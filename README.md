# efreightsuite

This application is built using Spring Boot, MySQL, and Redis. Frontend of the application is written using Angular 1.5.3. 

## Recommended tools

1. Docker
2. Git bash
3. MySQL
4. Redis
5. Intellij Idea Community edition

## Building the application

Before you build the application, set two environment variables.

```
EFS_RESOURCE_HOST_PATH=<path_of_efs_resource_directory>
EFS_RESOURCE_TEST_PATH=<path_of_efs_resource_directory>
```
The value of both these environment variables is equal to absolute path of `efs-resource` directory. The `efs-directory` exits inside the root of Git repository.

Now, you can build the application using following command.

```
./gradlew clean build
```

## Building the Docker image

Download and install Docker for your operating system. There are installers form Mac and Windows. Please refer to following [link](https://docs.docker.com/engine/installation/). Windows users note that _**Windows for Docker**_ only works if you are Windows 10. If you are not running Windows 10, then you can install Docker Machine by [referring to this link](https://docs.docker.com/machine/install-machine/#install-machine-directly). This document will describe Docker machine steps as well. We suggest [reading about Docker Machine](https://docs.docker.com/machine/get-started/#prerequisite-information) to help you gain knowledge of the same.

If Docker installer is not available for your OS, then please use Docker Machine. Read the [setup here](https://github.com/newage-git/newage/wiki/Running-Application-with-MySQL-Docker-Setup#step-0-docker-machine-setup).

To build the Docker image, run the following command.

```
./gradlew clean build docker
```

## Running the applicaton

To run the application, we will use `docker-compose`. Make sure you are inside the root directory of the project.

```
docker-compose up
```

This will start the application on port 9090. If you are using Docker for Mac or Docker for Windows then application will be available at http://localhost:9090/index.html. If you are using Docker for machine, then you should use ip address of your machine instead of localhost.
