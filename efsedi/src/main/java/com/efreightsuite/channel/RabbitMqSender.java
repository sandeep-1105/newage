package com.efreightsuite.channel;

import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessagePostProcessor;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.efreightsuite.dto.MessagingDto;

import lombok.extern.log4j.Log4j2;

@Log4j2
@Service
public class RabbitMqSender {

	@Value("${queue.response.name}")
	String responseQueueName;

	@Autowired
	RabbitTemplate rabbitTemplate;

	public void sendtoMQ(MessagingDto request) {
		try {

			rabbitTemplate.convertAndSend(responseQueueName, request, new MessagePostProcessor() {
				@Override
				public Message postProcessMessage(Message message) {

					message.getMessageProperties().setHeader("SaasId", request.getSaasId());

					message.getMessageProperties().setContentType("application/json");

					return message;
				}
			});
		} catch (Exception exception) {
			log.error("Exception in RabbitMqSender :", exception);
		}
	}
}
