package com.efreightsuite.channel;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import com.efreightsuite.channel.stdin.AbstractMessagingService;
import com.efreightsuite.configuration.TenantContext;
import com.efreightsuite.configuration.TenantService;
import com.efreightsuite.dto.MessagingDto;
import com.efreightsuite.enumeration.EdiMessagingFor;
import com.efreightsuite.service.EdiResponseService;
import com.efreightsuite.service.WinWebResponseService;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.log4j.Log4j2;
import org.apache.log4j.MDC;

import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageListener;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class RabbitMqListener implements MessageListener {

	@Autowired
	MessageConverter convertor;

	@Autowired
	RabbitTemplate rabbitTemplate;

	@Autowired
	WinWebResponseService winWebResponseService;
	
	@Autowired
	List<AbstractMessagingService> abstractMessagingServiceList;

	@Autowired
	TenantService tenantService;

	Map<EdiMessagingFor, AbstractMessagingService> abstractMessagingServiceMap = new HashMap<>();

	@Autowired
	EdiResponseService ediResponseService;

	@PostConstruct
	public void init() {
		for (AbstractMessagingService ars : abstractMessagingServiceList) {
			abstractMessagingServiceMap.put(ars.getMessageFor(), ars);
		}
	}

	@Override
	public void onMessage(Message message) {
		try {

			// Receive Message and convert.
			String str = new String(message.getBody());
			ObjectMapper objectMapper = new ObjectMapper();
			objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
			MessagingDto request = objectMapper.readValue(str, MessagingDto.class);

			// Setting the SaasID
			String sassId = "";
			if (message.getMessageProperties() != null && message.getMessageProperties().getHeaders() != null) {
				sassId = message.getMessageProperties().getHeaders().get("SaasId").toString();
				TenantContext.setCurrentTenant(sassId);
			}

			// If message is a response then read all the directories based on the saasMap
			/*
			 * if (request.isResponse() && (request.getMessagingFor() == null ||
			 * !request.getMessagingFor().equals(EdiMessagingFor.AES_RESPONSE)))
			 * {
			 */
			if (request.isResponse()) {
				ediResponseService.readDirectory();
			} else {
				// If message is a request
				if (message.getMessageProperties() != null && message.getMessageProperties().getHeaders() != null) {
					if (message.getMessageProperties().getHeaders().get("TrackId") != null) {
						String trackId = message.getMessageProperties().getHeaders().get("TrackId").toString();
						MDC.put("TrackId", trackId);
					}
					TenantContext.setCurrentTenant(sassId);
					request.setSaasId(sassId);
				}
				abstractMessagingServiceMap.get(request.getMessagingFor()).process(request);
			}
			winWebResponseService.winResponeCheck();
		} catch (Exception e) {
			log.error("RabbitMqListener Exception", e);
		}
	}
}
