package com.efreightsuite.channel.stdin;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.efreightsuite.dto.MessagingDto;
import com.efreightsuite.enumeration.EdiMessagingFor;
import com.efreightsuite.repository.AesFileEdiRepository;
import com.efreightsuite.repository.AesFileEdiStatusRepository;
import com.efreightsuite.repository.AesFileRepository;
import com.efreightsuite.repository.AesFileStatusHistoryRepository;
import com.efreightsuite.repository.AesFilerMasterRepository;
import com.efreightsuite.repository.AirlineEdiRepository;
import com.efreightsuite.repository.AirlineEdiStatusRepository;
import com.efreightsuite.repository.DefaultMasterDataRepository;
import com.efreightsuite.repository.EdiConfigurationMasterRepository;
import com.efreightsuite.repository.LocationMasterRepository;
import com.efreightsuite.repository.UserProfileRepository;
import com.efreightsuite.service.FHLService;
import com.efreightsuite.service.FTPConfigurationService;
import com.efreightsuite.service.FWBService;
import com.efreightsuite.service.email.EdiMailService;
import com.efreightsuite.util.AppUtil;

@Service
public abstract class AbstractMessagingService {

	@Autowired
	protected AirlineEdiRepository airlineEdiRepository;

	@Autowired
	protected DefaultMasterDataRepository defaultMasterDataRepository;

	@Autowired
	protected AesFileEdiRepository aesFileEdiRepository;

	@Autowired
	protected AesFileEdiStatusRepository aesFileEdiStatusRepository;
	
	@Autowired
	protected AesFileRepository aesFileRepository;

	@Autowired
	protected AesFileStatusHistoryRepository aesFileStatusHistoryRepository;
	
	@Autowired
	protected AesFilerMasterRepository aesFilerMasterRepository;

	@Autowired
	protected AirlineEdiStatusRepository airlineEdiStatusRepository;

	@Autowired
	protected UserProfileRepository userProfileRepository;

	@Autowired
	protected LocationMasterRepository locationMasterRepository;
	
	@Autowired
	protected EdiConfigurationMasterRepository ediConfigurationMasterRepository;
	
	@Autowired
	protected AppUtil appUtil;

	
	@Autowired
	protected FTPConfigurationService ftpConfigurationService;
	
	public abstract void process(MessagingDto request);

	public abstract EdiMessagingFor getMessageFor();
	
	@Autowired
	protected FWBService fwbService;
	
	@Autowired
	protected FHLService fhlService;
	
	@Autowired
	protected EdiMailService ediMailService;

}
