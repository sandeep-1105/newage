package com.efreightsuite.channel.stdin;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.efreightsuite.enumeration.EdiMessagingFor;
import com.efreightsuite.repository.ConsolDocumentRepository;
import com.efreightsuite.repository.ConsolRepository;
import com.efreightsuite.repository.EdiConfigurationMasterRepository;
import com.efreightsuite.repository.EdiResponseStatusRepository;
import com.efreightsuite.service.FTPConfigurationService;
import com.efreightsuite.service.TranslateFSU;

@Service
public abstract class AbstractResponseService {

	@Autowired
	protected EdiConfigurationMasterRepository ediConfigurationMasterRepository;

	@Autowired
	protected EdiResponseStatusRepository ediResponseStatusRepository;
	
	@Autowired
	protected ConsolDocumentRepository consolDocumentRepository;
	
	@Autowired
	protected ConsolRepository consolRepository;
	
	@Autowired
	protected TranslateFSU translateFSU;
	
	@Autowired
	protected FTPConfigurationService ftpConfigurationService;

	public abstract void processor();

	public abstract EdiMessagingFor getMessageFor();

}
