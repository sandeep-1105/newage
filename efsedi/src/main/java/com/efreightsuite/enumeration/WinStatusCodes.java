package com.efreightsuite.enumeration;

public enum WinStatusCodes {

	RCS,RCT,RCF,BKD,MAN,DEP,PRE,TRM,TFD,NFD,AWB,CCD,DLV,DIS,AWR,ARR,DDL,CRC,TGC,FOH,DOC
}
