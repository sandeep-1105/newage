package com.efreightsuite;

import javax.persistence.EntityManagerFactory;

import com.efreightsuite.support.ApplicationPropertiesBindingPostProcessor;
import lombok.extern.log4j.Log4j2;

import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.amqp.rabbit.listener.adapter.MessageListenerAdapter;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.boot.web.support.ErrorPageFilter;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.web.client.RestTemplate;

import com.efreightsuite.channel.RabbitMqListener;

@SpringBootApplication
@Log4j2
@EnableCaching
@EnableAsync
@EnableScheduling
public class EfsEdiLauncher {

	@Value("${queue.name}")
	String queueName;
	
	@Value("${queue.response.name}")
	String responseQueueName;
	
	

	@Value("${queue.concurrentuser}")
	int concurrentUsers;

	@Value("${queue.maxconcurrent.user}")
	int maxConcurrentUsers;

	public static void main(String[] args) {
		log.info("Application started");
		SpringApplication.run(EfsEdiLauncher.class, args);
	}

	@Bean
	JpaTransactionManager transactionManager(EntityManagerFactory entityManagerFactory) {
		JpaTransactionManager transactionManager = new JpaTransactionManager();
		transactionManager.setEntityManagerFactory(entityManagerFactory);
		return transactionManager;
	}

	@Bean
	public ErrorPageFilter errorPageFilter() {
		return new ErrorPageFilter();
	}

	@Bean
	public FilterRegistrationBean disableSpringBootErrorFilter(ErrorPageFilter filter) {
		FilterRegistrationBean filterRegistrationBean = new FilterRegistrationBean();
		filterRegistrationBean.setFilter(filter);
		filterRegistrationBean.setEnabled(false);
		return filterRegistrationBean;
	}

	@Bean
	Queue ediRequestQueue() {
		return new Queue(queueName, false);
	}
	

	@Bean
	Queue ediResponseQueue() {
		return new Queue(responseQueueName, false);
	}
	
	@Bean
	MessageConverter messageConverter() {
		return new Jackson2JsonMessageConverter();
	}

	@Bean
	MessageListenerAdapter listenerAdapter(RabbitMqListener newAgeListener, MessageConverter converter) {
		return new MessageListenerAdapter(newAgeListener, converter);
	}

	@Bean
	SimpleMessageListenerContainer container(ConnectionFactory connectionFactory, MessageListenerAdapter listenerAdapter) {
		SimpleMessageListenerContainer container = new SimpleMessageListenerContainer(connectionFactory);
		container.setQueueNames(queueName, responseQueueName);
		container.setConcurrentConsumers(concurrentUsers);
		container.setMaxConcurrentConsumers(maxConcurrentUsers);
		container.setMessageListener(listenerAdapter);
		return container;
	}

	

	@Bean
	RabbitTemplate template(ConnectionFactory connectionFactory, MessageConverter converter) {

		RabbitTemplate template = new RabbitTemplate(connectionFactory);
		template.setMessageConverter(converter);
		return template;
	}

	@Bean
	public ThreadPoolTaskExecutor taskExecutor() {
		ThreadPoolTaskExecutor pool = new ThreadPoolTaskExecutor();
		pool.setCorePoolSize(1);
		pool.setMaxPoolSize(10);
		pool.setWaitForTasksToCompleteOnShutdown(true);
		return pool;
	}
	
	@Bean
	public RestTemplate restTemplate() {
	    return new RestTemplate();
	}

}
