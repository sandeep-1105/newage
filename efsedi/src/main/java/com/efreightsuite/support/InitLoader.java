
package com.efreightsuite.support;

import com.efreightsuite.common.component.CacheRepository;
import com.efreightsuite.configuration.TenantContext;
import com.efreightsuite.configuration.TenantService;
import com.efreightsuite.model.Language;
import com.efreightsuite.repository.LanguageDetailRepository;
import com.efreightsuite.repository.LanguageRepository;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Service;

@Log4j2
@Service
public class InitLoader implements CommandLineRunner {

	@Value("${newage.saas.based.application.enable}")
	boolean saasBased;
	
	@Value("${efs.push.api.server}")
	boolean pushApiServer;
	
	@Autowired
    TenantService tenantService;
	
	@Autowired
    LanguageDetailRepository languageDetailRepository;

	@Autowired
    CacheRepository cacheRepository;
	    
	@Autowired
    LanguageRepository languageRepo;
	

	private void initLoadCache(){
		
		if(pushApiServer==false){
			if(saasBased){
	    		cacheRepository.clearCache();
	    		for(String saas : tenantService.getSaasMap().keySet()){
	    			if(!saas.equals("COM")) {
	    				log.info("Init Cache Loading SaaS Id : "+saas);
	    				TenantContext.setCurrentTenant(saas);
		        		for(Language language : languageRepo.findAll()) {
		        			cacheRepository.removeNLSCache(saas,language);
		            		cacheRepository.pushNLS(saas, language, languageDetailRepository.findAllByLanguage(language));	
		            	}
	    			}
	        	}
	    	}else{
	    		cacheRepository.clearCache();
	    		TenantContext.setCurrentTenant(null);
	    		log.info("Init Cache Loading ");
	    		for(Language language : languageRepo.findAll()) {
	    			cacheRepository.removeNLSCache("",language);
	        		cacheRepository.pushNLS("", language, languageDetailRepository.findAllByLanguage(language));	
	        	}
	    	}
		}
	    	
	    }

	@Override
	public void run(String... args) throws Exception {
		initLoadCache();
		
	} 
}
