
package com.efreightsuite.common.component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import com.efreightsuite.enumeration.TemplateType;
import com.efreightsuite.enumeration.WhichTransactionDate;
import com.efreightsuite.model.DateConfiguration;
import com.efreightsuite.model.EmailTemplate;
import com.efreightsuite.model.Language;
import com.efreightsuite.model.LanguageDetail;
import com.efreightsuite.repository.DateConfigurationRepository;
import com.efreightsuite.repository.EmailTemplateRepository;
import com.efreightsuite.repository.LanguageDetailRepository;
import com.efreightsuite.repository.LanguageRepository;

import lombok.extern.log4j.Log4j2;

@Log4j2
@Service
public class CacheRepository {

    @Autowired
    LanguageRepository languageRepo;
    
    @Autowired
    LanguageDetailRepository languageDetailRepo;
    
	@SuppressWarnings("unchecked")
	@CachePut(value = "master", key = "#saasId+'-'+#object.id +'-'+#uniqueKey", unless = "#result == null")
	public <T> T save(String saasId, String uniqueKey, T object,
			@SuppressWarnings("rawtypes") JpaRepository objRepository) {
		if (object == null) {
			return null;
		}
		log.debug("Data saved from Database : id[" + saasId + "-" + uniqueKey + "]");
		return (T) objRepository.save(object);
	}
	
	
	
	
	public <T> T save(String saasId, String uniqueKey, List<T> object,
			@SuppressWarnings("rawtypes") JpaRepository objRepository) {
		
		if (object == null) {
			return null;
		}
		
		object = objRepository.save(object);
		
		for(T obj : object){
			saveEachItem(saasId, uniqueKey, obj);
		}
		
		log.debug("Data saved from Database : id[" + saasId + "-" + uniqueKey + "]");
		return (T) object;
	}
	
	@SuppressWarnings("unchecked")
	@CachePut(value = "master", key = "#saasId+'-'+#object.id +'-'+#uniqueKey", unless = "#result == null")
	public <T> T save(String saasId, String uniqueKey, T object) {
		if (object == null) {
			return null;
		}
		
		return (T) object;
	}
	
	@SuppressWarnings("unchecked")
	@CachePut(value = "master", key = "#saasId+'-'+#object.id +'-'+#uniqueKey", unless = "#result == null")
	public <T> T saveEachItem(String saasId, String uniqueKey, T object) {
		if (object == null) {
			return null;
		}
		
		return (T) object;
	}
	
	

	@SuppressWarnings("unchecked")
	@Cacheable(value = "master", key = "#saasId+'-'+#id +'-'+#uniqueKey", unless = "#result == null")
	public <T> T get(String saasId, String uniqueKey, Long id,
			@SuppressWarnings("rawtypes") JpaRepository objRepository) {
		T object = (T) objRepository.findOne(id);
		log.debug("Data fetched from Database : id[" + saasId + "-" + uniqueKey + "-" + id + "]");
		return object;
	}
	
	@SuppressWarnings("unchecked")
	@Cacheable(value = "configuration", key = "#saasId+'-'+#templateType +'-'+#uniqueKey", unless = "#result == null")
	public EmailTemplate get(String saasId, String uniqueKey, TemplateType templateType,
			 EmailTemplateRepository objRepository) {
		EmailTemplate object = objRepository.findByTemplateType(templateType);
		log.debug("Data fetched from Database : id[" + saasId + "-" + uniqueKey + "-" + templateType + "]");
		return object;
	}
	
	@SuppressWarnings("unchecked")
	@Cacheable(value = "configuration", key = "#saasId+'-'+#serviceId+'-'+#whichTransactionDate+'-'+#uniqueKey", unless = "#result == null")
	public DateConfiguration getByLogicDate(String saasId, String uniqueKey, Long serviceId, WhichTransactionDate whichTransactionDate,
			DateConfigurationRepository objRepository) {
		DateConfiguration object = objRepository.getByLogicDate(serviceId,whichTransactionDate);
		return object;
	}
	
	@SuppressWarnings("unchecked")
	@CachePut(value = "configuration",key = "#saasId+'-'+#object.serviceMaster.id+'-'+#object.whichTransactionDate+'-'+#uniqueKey", unless = "#result == null")
	public <T> T saveLogicDate(String saasId, String uniqueKey, T object) {
		if (object == null) {
			return null;
		}
		return (T) object;
	}
	
	@CacheEvict(value = { "master", "configuration", "NLS" }, allEntries=true)
	public void clearCache(){
		//Clear all redis cache
	}
	
	@CacheEvict(value = "configuration", key = "#saasId+'-'+#serviceId+'-'+#whichTransactionDate+'-'+#uniqueKey", allEntries = true)
	public void removeLogicDate(String saasId, String uniqueKey, Long serviceId, WhichTransactionDate whichTransactionDate) {
		log.debug("Removed from cache :" + saasId + uniqueKey);
	}
	
	@SuppressWarnings("unchecked")
	@Cacheable(value = "master", key = "#saasId+'-'+#id +'-'+#uniqueKey", unless = "#result == null")
	public <T> T get(String saasId, String uniqueKey, Long id) {
		
		return null;
	}
	

	@CacheEvict(value = "master", key = "#saasId+'-'+#id +'-'+#uniqueKey", allEntries = true)
	public void remove(String saasId, String uniqueKey, Long id) {
		log.debug("Removed from cache :" + saasId + uniqueKey + id);
	}

	@SuppressWarnings("unchecked")
	public void delete(String saasId, String uniqueKey, Long id,
			@SuppressWarnings("rawtypes") JpaRepository objRepository) {

		objRepository.delete(id);

		remove(saasId, uniqueKey, id);

	}

	
	@SuppressWarnings("unchecked")
	@CachePut(value = "master", key = "#saasId+'-'+#object.id +'-'+#uniqueKey", unless = "#result == null")
	public Map saveNLS(String saasId, String uniqueKey, LanguageDetail object,
			LanguageDetailRepository languageDetailRepository) {
		if (object == null) {
			return null;
		}
		log.debug("Data saved from Database : id[" + saasId + "-" + uniqueKey + "]");
		object = languageDetailRepository.save(object);
		return pushNLSitem(saasId, object.getLanguage().getLanguageName(), object);
	}
	
	
	//@SuppressWarnings("unchecked")
	//@CachePut(value = "master", key = "#saasId+'-'+#object.id +'-'+#uniqueKey", unless = "#result == null")
	public void saveNLS(String saasId, List<LanguageDetail> object,
			LanguageDetailRepository languageDetailRepository) {
		if (object == null) {
			return;
		}
		log.debug("Data saved from Database : id[" + saasId + "]");
		object = languageDetailRepository.save(object);
		
		for(LanguageDetail obj : object){
			saveEachItemNLS(saasId, obj.getLanguage(), obj);
		}
		
	}
	
	
	
	@SuppressWarnings("unchecked")
	@CachePut(value = "NLS", key = "#saasId+'-'+#language.languageName", unless = "#result == null")
	public <T> T saveEachItemNLS(String saasId, Language language, LanguageDetail object) {
		if (object == null) {
			return null;
		}
		
		return (T) object;
	}
	
	
	
	@SuppressWarnings("unchecked")
	@CachePut(value = "NLS", key = "#saasId+'-'+#language.languageName", unless = "#result == null")
	public Map pushNLS(String saasId, Language language, List<LanguageDetail> languageDetail) {

		Map<String, String> codeMap = new HashMap<>();
		for (LanguageDetail ld : languageDetail) {
			codeMap.put(ld.getLanguageCode(), ld.getLanguageDesc());
		}

		log.debug("Data saved from Database 'NLS' : " + language.getLanguageName());
		return codeMap;
	}
	
	@CacheEvict(value = "NLS", key = "#saasId+'-'+#language.languageName", allEntries = true)
	public void removeNLSCache(String saasId, Language language) {
		log.debug("Removed from NLS cache :" + saasId );
	}
	
	
	@SuppressWarnings("unchecked")
	@Cacheable(value = "NLS", key = "#saasId+'-'+#languageName", unless = "#result == null")
	public Map getNLS(String saasId, String languageName) {
	    Map<String,String> codeMap = new HashMap<>();
	    
	    Language lang = languageRepo.findByLanguageName(languageName);
	    for(LanguageDetail ld : languageDetailRepo.findByLanguage(lang)){
		    codeMap.put(ld.getLanguageCode(), ld.getLanguageDesc());
	    }
	    log.debug("Data get from Database 'NLS' : "+languageName);
		return codeMap;
	}
	

	
	@SuppressWarnings("unchecked")
	@CachePut(value = "master", key = "#saasId+'-'+#object.id +'-'+#uniqueKey", unless = "#result == null")
	public LanguageDetail saveAndReturnNLS(String saasId, String uniqueKey, LanguageDetail object,
			LanguageDetailRepository languageDetailRepository) {
		if (object == null) {
			return null;
		}
		log.debug("Data saved from Database : id[" + saasId + "-" + uniqueKey + "]");
		object = languageDetailRepository.save(object);
		return object;
	}
	
	@SuppressWarnings("unchecked")
	@CachePut(value = "NLS", key = "#saasId+'-'+#languageName", unless = "#result == null")
	public Map pushNLSitem(String saasId, String languageName, LanguageDetail languageDetail) {
	    Map<String,String> codeMap = getNLS(saasId, languageName);
	
	    codeMap.put(languageDetail.getLanguageCode(), languageDetail.getLanguageDesc());
	    log.debug("Data saved from Database 'NLS' : "+languageName);
	    return codeMap;
	}
	
}
