package com.efreightsuite.configuration;

import java.util.Map;

import org.springframework.jdbc.datasource.lookup.AbstractRoutingDataSource;

import lombok.extern.log4j.Log4j2;

@Log4j2
public class SessionRoutingDataSource extends AbstractRoutingDataSource {
	
	@Override
	protected Object determineCurrentLookupKey() {
		return TenantContext.getCurrentTenant();
	}

    public void setDataSources(Map<Object, Object> dataSources) {
        setTargetDataSources(dataSources);
        afterPropertiesSet();
	}
}
