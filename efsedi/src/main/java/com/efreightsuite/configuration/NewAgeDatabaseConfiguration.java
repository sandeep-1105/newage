package com.efreightsuite.configuration;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.commons.dbcp2.BasicDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.efreightsuite.configuration.MultitenancyConfigurationProperties.Tenant;

import lombok.extern.log4j.Log4j2;

@Configuration
// @EnableConfigurationProperties(MultitenancyConfigurationProperties.class)
@Log4j2
public class NewAgeDatabaseConfiguration {

	@Autowired
    MultitenancyConfigurationProperties multitenancyProperties;

	@Autowired
	TenantService tenantService;

	@Bean
	public DataSource dataSource() {

		SessionRoutingDataSource sessionRoutingDataSource = new SessionRoutingDataSource();

        HashMap<Object, Object> targetDataSources = new HashMap<>();
        String defaultTenant = null;

    	Map<String, String> saasMap = new HashMap<>();

        for(Tenant tenant : multitenancyProperties.getTenants()) {
        	log.info("####### Current Tenant : "+tenant.getName());
        	if(tenant.isDefault()) {
        		defaultTenant = tenant.getName();
        	}

        	BasicDataSource ds = new BasicDataSource();

        	ds.setDriverClassName(tenant.getDriverClassName());
    		ds.setUrl(tenant.getUrl());
    		ds.setUsername(tenant.getUsername());
    		ds.setPassword(tenant.getPassword());
    		ds.setMaxTotal(tenant.getMaxTotal());
    		ds.setMaxIdle(tenant.getMaxIdle());
    		ds.setMinIdle(tenant.getMinIdle());
    		ds.setValidationQuery(tenant.getValidationQuery());
    		ds.setValidationQueryTimeout(tenant.getValidationQueryTimeout());
        	targetDataSources.put(tenant.getName(), ds);

        	saasMap.put(tenant.getName(), tenant.getCompanyName());

        }


        sessionRoutingDataSource.setDefaultTargetDataSource(targetDataSources.get(defaultTenant));
        sessionRoutingDataSource.setTargetDataSources(targetDataSources);
        tenantService.setTempTargetDataSources(targetDataSources);
        tenantService.setTempdefaultDataSources(targetDataSources.get(defaultTenant));
        tenantService.setSaasMap(saasMap);
        sessionRoutingDataSource.afterPropertiesSet();

        tenantService.setSessionRoutingDataSource(sessionRoutingDataSource);

		return sessionRoutingDataSource;

	}

}