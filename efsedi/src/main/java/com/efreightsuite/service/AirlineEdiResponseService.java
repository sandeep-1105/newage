package com.efreightsuite.service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.efreightsuite.channel.stdin.AbstractResponseService;
import com.efreightsuite.communication.FTPHelper;
import com.efreightsuite.communication.FtpDto;
import com.efreightsuite.enumeration.EdiMessagingFor;
import com.efreightsuite.enumeration.ImportExport;
import com.efreightsuite.model.Consol;
import com.efreightsuite.model.EdiResponseStatus;
import com.efreightsuite.repository.ConsolRepository;
import com.efreightsuite.repository.EdiResponseStatusRepository;

import lombok.extern.log4j.Log4j2;

@Service
@Log4j2
public class AirlineEdiResponseService {
	
	@Autowired
	FTPConfigurationService ftpConfigurationService;
	
	@Autowired
	TranslateFSU translateFSU;

	@Autowired
	EdiResponseStatusRepository ediResponseStatusRepository;
	
	@Autowired
	ConsolRepository consolRepository;
	
	public void processor(EdiMessagingFor ediMessagingFor) {

		try {
		FtpDto ftpDto = ftpConfigurationService.getFtpData(ediMessagingFor);
		
		if (ftpDto != null && ftpDto.getServer() != null && ftpDto.getPort() != null && ftpDto.getUserName() != null
				&& ftpDto.getPassword() != null && ftpDto.getLocalFileInputLocation() != null
				&& ftpDto.getLocalFileOutputLocation() != null && ftpDto.getRemoteFileInputLocation() != null
				&& ftpDto.getRemoteFileOutputLocation() != null) {
			FTPHelper ftpHelper = new FTPHelper(ftpDto);

			ArrayList<String> fileNames = ftpHelper.getFileNames(ftpDto.getRemoteFileOutputLocation());

			if (fileNames != null && !fileNames.isEmpty()) {
				for (String fileName : fileNames) {
					
					try {
						
						ftpHelper.downloadFile(ftpDto.getRemoteFileOutputLocation() + fileName, ftpDto.getLocalFileOutputLocation() + fileName);
						log.info(fileName + " being downloaded...");
						ftpHelper.deleteFile(ftpDto.getRemoteFileOutputLocation() + fileName);
						log.info("Remote File " + ftpDto.getRemoteFileOutputLocation() + fileName + " being deleted");
						
						
						EdiResponseStatus ediResponseStatus = translateFSU.getObject(ftpDto.getLocalFileOutputLocation(), fileName);
					
						Consol consol = consolRepository.getConsolByMawbAndService(ediResponseStatus.getMasterNumber(), ImportExport.Export);
						ediResponseStatus.setConsol(consol);
						log.info("Consol " + consol.getConsolUid() + " has got the response!"); 
						ediResponseStatus = ediResponseStatusRepository.save(ediResponseStatus);
					}catch(Exception e) {
						log.error("Exception Occured while reading the file : " + e);
					}
				}
			}
			ftpHelper.disconnect();
		} else {
			log.error("FTP DATA is not configured............");
		}
		}catch(Exception exception) {
			log.error("Error while reading the EK EDI ", exception);
		}
	}


}
