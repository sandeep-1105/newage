package com.efreightsuite.service;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.efreightsuite.channel.stdin.AbstractMessagingService;
import com.efreightsuite.dto.MessagingDto;
import com.efreightsuite.enumeration.BooleanType;
import com.efreightsuite.enumeration.DimensionUnit;
import com.efreightsuite.enumeration.EdiMessagingFor;
import com.efreightsuite.enumeration.PPCC;
import com.efreightsuite.enumeration.YesNo;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.model.Consol;
import com.efreightsuite.model.ConsolConnection;
import com.efreightsuite.model.ConsolDocumentDimension;
import com.efreightsuite.model.EdiConfigurationMaster;
import com.efreightsuite.model.ShipmentLink;
import com.efreightsuite.model.WinResponseRemarks;
import com.efreightsuite.model.WinWebConnectResponse;
import com.efreightsuite.repository.ConsolRepository;
import com.efreightsuite.repository.EdiConfigurationMasterRepository;
import com.efreightsuite.repository.WinWebConnectResponseRepository;
import com.efreightsuite.winwebconnect.Address;
import com.efreightsuite.winwebconnect.Charges;
import com.efreightsuite.winwebconnect.ChargesDeclaration;
import com.efreightsuite.winwebconnect.ChargesSummary;
import com.efreightsuite.winwebconnect.City;
import com.efreightsuite.winwebconnect.Country;
import com.efreightsuite.winwebconnect.Dimension;
import com.efreightsuite.winwebconnect.Execution;
import com.efreightsuite.winwebconnect.Hawb;
import com.efreightsuite.winwebconnect.Locations;
import com.efreightsuite.winwebconnect.Mawb;
import com.efreightsuite.winwebconnect.Parties;
import com.efreightsuite.winwebconnect.Party;
import com.efreightsuite.winwebconnect.Port;
import com.efreightsuite.winwebconnect.Rates;
import com.efreightsuite.winwebconnect.Remarks;
import com.efreightsuite.winwebconnect.RoutingDetails;
import com.efreightsuite.winwebconnect.Weight;
import com.efreightsuite.winwebconnect.dto.PouchDTO;
import com.efreightsuite.winwebconnect.dto.PouchResponseDto;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;

//Request service which initiates from rabbit mq listner to Rest template request
@Service
@Log4j2
public class WinWebConnectMessagingService extends AbstractMessagingService {

	@Autowired
	ConsolRepository consolRepository;

	@Autowired
	WinWebConnectRestServices winWebConnectRestServices;

	@Autowired
	EdiConfigurationMasterRepository ediConfigurationMasterRepository;

	@Autowired
	WinWebConnectResponseRepository winWebConnectResponseRepository;

	public EdiMessagingFor getMessageFor() {
		return EdiMessagingFor.WIN;
	}

	SimpleDateFormat dateFormat = new SimpleDateFormat(
			"yyyy-MM-dd'T'HH:mm:ss'Z'");

	/*
	 * 
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.efreightsuite.channel.stdin.AbstractMessagingService#process(com.
	 * efreightsuite.dto.MessagingDto) class which is used for Process post/put
	 * request for win web connect
	 */
	public void process(MessagingDto request) {
		ResponseEntity<PouchResponseDto> pouchResponseDTo = null;
		try {
			if (request.getConsolUid() != null) {
				Consol consol = consolRepository.findByConsolUid(request
						.getConsolUid());
				PouchDTO prdto = new PouchDTO();
				createMawb(prdto, consol, request);
				if (consol.getDirectShipment().equals(YesNo.Yes)) {
					// only Mawb
				} else {
					createHawb(prdto, consol, request);
				}
				String token = winWebConnectRestServices.winWebLogin(consol);
				if (token != null) {
					pouchResponseDTo = winWebConnectRestServices
							.createRequestWinWeb(prdto, token, consol);
					saveResponse(pouchResponseDTo, request, null);
				} else {
					throw new RestException(ErrorCode.TOKENINVALID);
				}
			}
		} catch (HttpClientErrorException e) {
			try {
				saveHttpResponse(request, e);
			} catch (IOException e1) {
				saveExResponse(request, e1);
			}
		} catch (RestException re) {
			saveRestResponse(request, re);
		} catch (Exception e) {
			saveExResponse(request, e);
		}
	}

	// Response save for Exception occures
	private void saveExResponse(MessagingDto request, Exception e) {
		Consol consol = consolRepository
				.findByConsolUid(request.getConsolUid());
		if (e != null) {
			WinWebConnectResponse winWebConnectResponse = winWebConnectResponseRepository
					.findByConsolUid(request.getConsolUid());
			if (winWebConnectResponse != null
					&& winWebConnectResponse.getWinStatus() != null) {

			} else {
				winWebConnectResponse = new WinWebConnectResponse();
			}
			if (winWebConnectResponse.getAgentId() != 0L) {

			} else {
				winWebConnectResponse.setAgentId(0);
			}
			winWebConnectResponse.setTransactionID(0);
			if (winWebConnectResponse.getMawbId() != null
					&& winWebConnectResponse.getMawbId() != 0L) {

			} else {
				winWebConnectResponse.setMawbId(0L);
			}
			winWebConnectResponse.setConsolUid(request.getConsolUid());
			winWebConnectResponse.setAwbNumber(consol.getConsolDocument()
					.getMawbNo());
			//winWebConnectResponse.setWinStatus(ErrorCode.FAILED);
			//winWebConnectResponse.setWinStatus(ErrorCode.FAILED);
			consol.setWinErrorMessage(e.getMessage());
			if (consol.getWinAwbID() != null && consol.getWinAwbID() != 0L) {

			} else {
				consol.setWinAwbID(0L);
			}
			//consol.setWinStatus(ErrorCode.FAILED);
			winWebConnectResponse.setAgentName("");
			winWebConnectResponse.setContactId(0);
			winWebConnectResponse.setDatetime(dateFormat.format(new Date()));
			if (winWebConnectResponse.getRemarks() != null
					&& winWebConnectResponse.getRemarks().size() > 0) {
				List<WinResponseRemarks> responseRemarks = new ArrayList<>(
                        winWebConnectResponse.getRemarks().size());
				int i = 0;
				for (WinResponseRemarks remarks : winWebConnectResponse
						.getRemarks()) {
					WinResponseRemarks winResponseRemarks = new WinResponseRemarks();
					winResponseRemarks.setErrorCode(remarks.getErrorCode());
					consol.setWinErrorCode(remarks.getErrorCode());
					consol.setWinErrorMessage(remarks.getErrorMessage());
					winResponseRemarks.setErrorMessage(remarks
							.getErrorMessage());
					winResponseRemarks.setIdentifier(remarks.getIdentifier());
					winResponseRemarks.setReferenceNumber(remarks
							.getReferenceNumber());
					winResponseRemarks.setWinWebResponse(winWebConnectResponse);
					responseRemarks.add(winResponseRemarks);
					i++;
				}
				winWebConnectResponse.setRemarks(responseRemarks);
			} else {
				List<WinResponseRemarks> responseRemarks = new ArrayList<>(
                        0);
				WinResponseRemarks winResponseRemarks = new WinResponseRemarks();
				winResponseRemarks.setErrorCode(ErrorCode.FAILED);
				consol.setWinErrorCode(ErrorCode.FAILED);
				winResponseRemarks.setErrorMessage(e.getMessage());
				winResponseRemarks.setIdentifier("");
				winResponseRemarks.setReferenceNumber("");
				winResponseRemarks.setWinWebResponse(winWebConnectResponse);
				responseRemarks.add(winResponseRemarks);
				winWebConnectResponse.setRemarks(responseRemarks);
			}
			winWebConnectResponseRepository.save(winWebConnectResponse);
		}
		consolRepository.save(consol);
	}

	// Response save for Rest Exception occures
	private void saveRestResponse(MessagingDto request, RestException re) {
		Consol consol = consolRepository
				.findByConsolUid(request.getConsolUid());
		if (re != null) {
			WinWebConnectResponse winWebConnectResponse = winWebConnectResponseRepository
					.findByConsolUid(request.getConsolUid());
			if (winWebConnectResponse != null
					&& winWebConnectResponse.getWinStatus() != null) {

			} else {
				winWebConnectResponse = new WinWebConnectResponse();
			}
			if (String.valueOf(winWebConnectResponse.getAgentId()) != "null"
					&& winWebConnectResponse.getAgentId() != 0L) {

			} else {
				winWebConnectResponse.setAgentId(0);
			}
			winWebConnectResponse.setTransactionID(0);

			if (winWebConnectResponse.getMawbId() != null
					&& winWebConnectResponse.getMawbId() != 0L) {

			} else {
				winWebConnectResponse.setMawbId(0L);
			}
			winWebConnectResponse.setConsolUid(request.getConsolUid());
			winWebConnectResponse.setAwbNumber(consol.getConsolDocument()
					.getMawbNo());
			winWebConnectResponse.setWinStatus(ErrorCode.FAILED);
			if (consol.getWinAwbID() != null && consol.getWinAwbID() != 0L) {

			} else {
				consol.setWinAwbID(0L);
			}
			consol.setWinStatus(ErrorCode.FAILED);
			winWebConnectResponse.setAgentName("");
			winWebConnectResponse.setContactId(0);
			winWebConnectResponse.setDatetime(dateFormat.format(new Date()));
			if (winWebConnectResponse.getRemarks() != null
					&& winWebConnectResponse.getRemarks().size() > 0) {
				List<WinResponseRemarks> responseRemarks = new ArrayList<>(
                        winWebConnectResponse.getRemarks().size());
				int i = 0;
				for (WinResponseRemarks remarks : winWebConnectResponse
						.getRemarks()) {
					WinResponseRemarks winResponseRemarks = new WinResponseRemarks();
					winResponseRemarks.setErrorCode(remarks.getErrorCode());
					consol.setWinErrorCode(remarks.getErrorCode());
					consol.setWinErrorMessage(remarks.getErrorMessage());
					winResponseRemarks.setErrorMessage(remarks
							.getErrorMessage());
					winResponseRemarks.setIdentifier(remarks.getIdentifier());
					winResponseRemarks.setReferenceNumber(remarks
							.getReferenceNumber());
					winResponseRemarks.setWinWebResponse(winWebConnectResponse);
					responseRemarks.add(winResponseRemarks);
					i++;
				}
				winWebConnectResponse.setRemarks(responseRemarks);
			} else {
				List<WinResponseRemarks> responseRemarks = new ArrayList<>(
                        0);
				WinResponseRemarks winResponseRemarks = new WinResponseRemarks();
				winResponseRemarks.setErrorCode(ErrorCode.FAILED);
				consol.setWinErrorCode(ErrorCode.FAILED);
				winResponseRemarks.setErrorMessage(re.getMessage());
				winResponseRemarks.setIdentifier("");
				winResponseRemarks.setReferenceNumber("");
				winResponseRemarks.setWinWebResponse(winWebConnectResponse);
				responseRemarks.add(winResponseRemarks);
				winWebConnectResponse.setRemarks(responseRemarks);
			}
			winWebConnectResponseRepository.save(winWebConnectResponse);
		}
		consolRepository.save(consol);
	}

	// Response save for Http Response occures
	private void saveHttpResponse(MessagingDto request,
			HttpClientErrorException e) throws JsonParseException,
			JsonMappingException, IOException {
		String e1 = e.getResponseBodyAsString() != null ? e
				.getResponseBodyAsString() : e.getMessage();
		ObjectMapper mapper = new ObjectMapper();
		PouchResponseDto pouchResponseDto = (PouchResponseDto) mapper
				.readValue(e1, PouchResponseDto.class);
		Consol consol = consolRepository
				.findByConsolUid(request.getConsolUid());
		if (pouchResponseDto != null) {
			WinWebConnectResponse winWebConnectResponse = winWebConnectResponseRepository
					.findByConsolUid(request.getConsolUid());
			if (winWebConnectResponse != null
					&& winWebConnectResponse.getWinStatus() != null) {

			} else {
				winWebConnectResponse = new WinWebConnectResponse();
			}
			winWebConnectResponse.setAgentId(pouchResponseDto.getAgentId());
			winWebConnectResponse.setTransactionID(pouchResponseDto
					.getTransactionID());
			winWebConnectResponse.setMawbId(pouchResponseDto.getMawbId());
			winWebConnectResponse.setAwbNumber(pouchResponseDto.getAwbNumber());
			winWebConnectResponse.setWinStatus(pouchResponseDto.getWinStatus());
			winWebConnectResponse.setLocationId(consol.getLocation().getId());
			consol.setWinAwbID(pouchResponseDto.getAwbID());
			consol.setWinStatus(pouchResponseDto.getWinStatus());
			winWebConnectResponse.setAgentName(null);
			winWebConnectResponse.setContactId(0);
			winWebConnectResponse.setDatetime(dateFormat.format(new Date()));
			winWebConnectResponse.setConsolUid(request.getConsolUid());
			if (pouchResponseDto.getRemarks() != null
					&& pouchResponseDto.getRemarks().size() > 0) {
				List<Remarks> listremarks = new ArrayList<>(
                        pouchResponseDto.getRemarks().size());
				List<WinResponseRemarks> responseRemarks = new ArrayList<>(
                        pouchResponseDto.getRemarks().size());
				int i = 0;
				String errorMessage = "";
				for (Remarks remarks : pouchResponseDto.getRemarks()) {
					WinResponseRemarks winResponseRemarks = new WinResponseRemarks();
					winResponseRemarks.setErrorCode(pouchResponseDto
							.getRemarks().get(i).getErrorCode());
					consol.setWinErrorCode(pouchResponseDto.getRemarks().get(i)
							.getErrorCode());
					winResponseRemarks.setErrorMessage(pouchResponseDto
							.getRemarks().get(i).getErrorMessage());
					errorMessage = errorMessage
							+ pouchResponseDto.getRemarks().get(i)
									.getErrorMessage() + "#";
					winResponseRemarks.setIdentifier(pouchResponseDto
							.getRemarks().get(i).getIdentifier());
					winResponseRemarks.setReferenceNumber(pouchResponseDto
							.getRemarks().get(i).getReferenceNumber());
					winResponseRemarks.setWinWebResponse(winWebConnectResponse);
					responseRemarks.add(winResponseRemarks);
					i++;
				}
				consol.setWinErrorMessage(errorMessage);
				winWebConnectResponse.setRemarks(responseRemarks);
			} else {
				winWebConnectResponse.setRemarks(new ArrayList<>());
				List<WinResponseRemarks> responseRemarks = new ArrayList<>(
                        0);
				WinResponseRemarks winResponseRemarks = new WinResponseRemarks();
				winResponseRemarks.setErrorCode(ErrorCode.FAILED);
				winResponseRemarks.setErrorMessage("Failed");
				winResponseRemarks.setIdentifier("");
				winResponseRemarks.setReferenceNumber("");
				winResponseRemarks.setWinWebResponse(winWebConnectResponse);
				responseRemarks.add(winResponseRemarks);
			}
			winWebConnectResponseRepository.save(winWebConnectResponse);
		}
		consolRepository.save(consol);

	}

	// which is for only post request
	void saveResponse(ResponseEntity<PouchResponseDto> pouchResponseDTo,
			MessagingDto request, Exception e) {

		if (pouchResponseDTo != null && pouchResponseDTo.getBody() != null) {
			Consol consol = consolRepository.findByConsolUid(request
					.getConsolUid());
			WinWebConnectResponse winWebConnectResponse = winWebConnectResponseRepository
					.findByConsolUid(request.getConsolUid());
			if (winWebConnectResponse != null) {

			} else {
				winWebConnectResponse = new WinWebConnectResponse();
			}
			
			System.out.println(pouchResponseDTo.getBody());
			winWebConnectResponse.setAwbID(pouchResponseDTo.getBody()
					.getMawbId());
			winWebConnectResponse.setLocationId(consol.getLocation().getId());
			winWebConnectResponse.setAgentId(pouchResponseDTo.getBody()
					.getAgentId());
			winWebConnectResponse.setTransactionID(pouchResponseDTo.getBody()
					.getTransactionID());
			winWebConnectResponse.setMawbId(pouchResponseDTo.getBody()
					.getMawbId());
			winWebConnectResponse.setAwbNumber(pouchResponseDTo.getBody()
					.getAwbNumber());
			winWebConnectResponse.setWinStatus(pouchResponseDTo.getBody()
					.getWinStatus());
			consol.setWinAwbID(pouchResponseDTo.getBody().getMawbId());
			consol.setWinStatus(pouchResponseDTo.getBody().getWinStatus());
			consol.setWinErrorCode(null);
			consol.setWinErrorMessage(null);
			// winWebConnectResponse.setAgentName(pouchResponseDTo.getBody().getUpdated()!=null?pouchResponseDTo.getBody().getUpdated().getAgentName():null);
			// winWebConnectResponse.setContactId(pouchResponseDTo.getBody().getUpdated()!=null?pouchResponseDTo.getBody().getUpdated().getContactId():null);
			// winWebConnectResponse.setDatetime(dateFormat.format(new Date()));
			winWebConnectResponse.setConsolUid(request.getConsolUid());
			if (pouchResponseDTo.getBody().getRemarks() != null
					&& pouchResponseDTo.getBody().getRemarks().size() > 0) {
				List<Remarks> listremarks = new ArrayList<>(
                        pouchResponseDTo.getBody().getRemarks().size());
				List<WinResponseRemarks> responseRemarks = new ArrayList<>(
                        pouchResponseDTo.getBody().getRemarks().size());
				int i = 0;
				for (Remarks remarks : pouchResponseDTo.getBody().getRemarks()) {
					WinResponseRemarks winResponseRemarks = new WinResponseRemarks();
					winResponseRemarks.setErrorCode(pouchResponseDTo.getBody()
							.getRemarks().get(i).getErrorCode());
					winResponseRemarks.setErrorMessage(pouchResponseDTo
							.getBody().getRemarks().get(i).getErrorMessage());
					winResponseRemarks.setIdentifier(pouchResponseDTo.getBody()
							.getRemarks().get(i).getIdentifier());
					winResponseRemarks
							.setReferenceNumber(pouchResponseDTo.getBody()
									.getRemarks().get(i).getReferenceNumber());
					winResponseRemarks.setWinWebResponse(winWebConnectResponse);
					responseRemarks.add(winResponseRemarks);
					i++;
				}
				winWebConnectResponse.setRemarks(responseRemarks);
			} else {
				winWebConnectResponse.setRemarks(new ArrayList<>());
			}
			winWebConnectResponseRepository.save(winWebConnectResponse);
			consolRepository.save(consol);
		}
	}

	// which is used for creating hawb details
	private void createHawb(PouchDTO prdto, Consol consol, MessagingDto request) {
		List<Hawb> hawb = new ArrayList<>(consol.getShipmentLinkList()
                .size());
		for (ShipmentLink sl : consol.getShipmentLinkList()) {
			Hawb hawbItem = new Hawb();
			setHawbHeaderDetails(consol, hawbItem, sl);
			Parties party = new Parties();
			setHawbParties(consol, hawbItem, sl, party);
			hawbItem.setParties(party);
			SetHawbCharges(hawbItem, sl);
			SetCargo(hawbItem, sl);
			SetOptional(hawbItem, sl);
			hawb.add(hawbItem);
		}
		prdto.setHawb(hawb);
	}

	private void SetOptional(Hawb hawbItem, ShipmentLink sl) {
		// doubt
		// TODO Auto-generated method stub
		/*
		 * Optional optional=new Optional(); OtherCustoms otherCustoms =new
		 * OtherCustoms(); Country country=new Country();//c
		 * country.setCode("");//c country.setName("");
		 * otherCustoms.setCountry(country);
		 * otherCustoms.setInformationIdentifier("");//c
		 * otherCustoms.setControlIdentifier("");//c
		 * otherCustoms.setSupplementControlInformation("");//c
		 * optional.setOtherCustoms(otherCustoms);
		 * hawbItem.setOptional(optional);
		 */
		hawbItem.setOptional(null);
	}

	private void SetCargo(Hawb hawbItem, ShipmentLink sl) {
		// TODO Auto-generated method stub
		// doubt
		/* Cargo cargo=new Cargo(); */
		/*
		 * HSCode hsCode=new HSCode(); hsCode.setCode("");
		 * cargo.setHsCode(hsCode); cargo.setGoodsDescription("");
		 * SpecialHandling specialHandling=new SpecialHandling();
		 * specialHandling.setCode("");
		 * cargo.setSpecialHandling(specialHandling); hawbItem.setCargo(cargo);
		 */
		hawbItem.setCargo(null);
	}

	private void SetHawbCharges(Hawb hawbItem, ShipmentLink sl) {

		Charges charges = new Charges(); // mandatory
		charges.setCurrencyCode(sl.getService().getLocalCurrencyCode()); // mandatory
		if (sl.getService().getPpcc().equals(PPCC.Prepaid)) {
			charges.setWeightOrValuation("P"); // mandatory
			charges.setOther("P"); // mandatory
		} else {
			charges.setWeightOrValuation("C"); // mandatory
			charges.setOther("C"); // mandatory
		}
		charges.setValuesForCarriage(null);
		charges.setValuesForCustom(null);
		charges.setValuesForInsurance(null);
		hawbItem.setCharges(charges);
	}

	private void setHawbParties(Consol consol, Hawb hawbItem, ShipmentLink sl,
			Parties parties) {

		// shipper setting
		Party shipper = new Party(); // mandatory
		shipper.setWinID(null);
		shipper.setAccountNumber(null);
		shipper.setName((consol.getConsolDocument().getShipper().getPartyName())
				.length() > 35 ? (consol.getConsolDocument().getShipper()
				.getPartyName()).substring(0, 34) : (consol.getConsolDocument()
				.getShipper().getPartyName())); // mandatory
		shipper.setPhone(consol.getConsolDocument().getShipperAddress()
				.getPhone());
		shipper.setFax(consol.getConsolDocument().getShipperAddress().getFax());
		shipper.setEmail(consol.getConsolDocument().getShipperAddress()
				.getEmail());
		Address address = new Address(); // mandatory
		address.setLine1((consol.getConsolDocument().getShipperAddress()
				.getAddressLine1()).length() > 35 ? (consol.getConsolDocument()
				.getShipperAddress().getAddressLine1()).substring(0, 34)
				: (consol.getConsolDocument().getShipperAddress()
						.getAddressLine1())); // mandatory
		if (consol.getConsolDocument().getShipperAddress().getAddressLine2() != null
				&& consol.getConsolDocument().getShipperAddress()
						.getAddressLine2().trim() != null) {
			address.setLine2((consol.getConsolDocument().getShipperAddress()
					.getAddressLine2()).length() > 35 ? (consol
					.getConsolDocument().getShipperAddress().getAddressLine2())
					.substring(0, 34) : (consol.getConsolDocument()
					.getShipperAddress().getAddressLine2()));
		}
		address.setPlace(sl.getLocation().getLocationName().length() > 17 ? sl
				.getLocation().getLocationName().substring(0, 16) : sl
				.getLocation().getLocationName()); // DOUBT--Mandatory
		City city = new City();
		city.setCode(consol.getConsolDocument().getShipperAddress().getCity() != null ? consol
				.getConsolDocument().getShipperAddress().getCity()
				.getCityCode()
				: null);
		city.setName(consol.getConsolDocument().getShipperAddress().getCity() != null ? consol
				.getConsolDocument().getShipperAddress().getCity()
				.getCityName()
				: null);
		address.setCity(city);
		address.setPostalCode(consol.getConsolDocument().getShipperAddress()
				.getZipCode());
		address.setStateProvince(consol.getConsolDocument().getShipperAddress()
				.getState() != null ? consol.getConsolDocument()
				.getShipperAddress().getState().getStateName() : null);
		Country country = new Country(); // mandatory
		country.setCode(consol.getConsolDocument().getShipper()
				.getCountryMaster().getCountryCode()); // mandatory
		country.setName(consol.getConsolDocument().getShipper()
				.getCountryMaster().getCountryName());
		address.setCountry(country);
		shipper.setAddress(address);
		parties.setShipper(shipper);
		Party consignee = new Party(); // mandatory
		consignee.setWinID(null);
		consignee.setAccountNumber(null);
		consignee.setName(consol.getConsolDocument().getConsignee()
				.getPartyName().length() > 35 ? consol.getConsolDocument()
				.getConsignee().getPartyName().substring(0, 35) : consol
				.getConsolDocument().getConsignee().getPartyName()); // mandatory
		consignee.setPhone(consol.getConsolDocument().getConsigneeAddress()
				.getPhone());
		consignee.setFax(consol.getConsolDocument().getConsigneeAddress()
				.getFax());
		consignee.setEmail(consol.getConsolDocument().getConsigneeAddress()
				.getEmail());
		Address cAddress = new Address(); // mandatory
		cAddress.setLine1((consol.getConsolDocument().getConsigneeAddress()
				.getAddressLine1()).length() > 35 ? (consol.getConsolDocument()
				.getConsigneeAddress().getAddressLine1()).substring(0, 34)
				: (consol.getConsolDocument().getConsigneeAddress()
						.getAddressLine1()));
		if (consol.getConsolDocument().getConsigneeAddress().getAddressLine2() != null) {
			cAddress.setLine2((consol.getConsolDocument().getConsigneeAddress()
					.getAddressLine2()).length() > 35 ? (consol
					.getConsolDocument().getConsigneeAddress()
					.getAddressLine2()).substring(0, 34) : (consol
					.getConsolDocument().getConsigneeAddress()
					.getAddressLine2()));
		}
		cAddress.setPlace(consol.getLocation().getLocationName().length() > 17 ? consol
				.getLocation().getLocationName().substring(0, 16)
				: consol.getLocation().getLocationName()); // DOUBT--Mandatory
		City Ccity = new City();
		Ccity.setCode(consol.getConsolDocument().getConsigneeAddress()
				.getCity() != null ? consol.getConsolDocument()
				.getConsigneeAddress().getCity().getCityCode() : null);
		Ccity.setName(consol.getConsolDocument().getConsigneeAddress()
				.getCity() != null ? consol.getConsolDocument()
				.getConsigneeAddress().getCity().getCityName() : null);
		cAddress.setCity(Ccity);
		cAddress.setPostalCode(consol.getConsolDocument().getConsigneeAddress()
				.getZipCode());
		cAddress.setStateProvince(consol.getConsolDocument()
				.getConsigneeAddress().getState() != null ? consol
				.getConsolDocument().getConsigneeAddress().getState()
				.getStateName() : null);
		Country cCountry = new Country(); // mandatory
		cCountry.setCode(consol.getConsolDocument().getConsignee()
				.getCountryMaster().getCountryCode()); // mandatory
		cCountry.setName(consol.getConsolDocument().getConsignee()
				.getCountryMaster().getCountryName());
		cAddress.setCountry(cCountry);
		consignee.setAddress(cAddress);
		parties.setConsignee(consignee);
	}

	private void setHawbHeaderDetails(Consol consol, Hawb hawbItem,
			ShipmentLink sl) {
		hawbItem.setAwbNumber(((consol.getConsolDocument().getMawbNo())
				.substring(0, 3))
				+ "-"
				+ ((consol.getConsolDocument().getMawbNo()).substring(3, 11))); // mandatory
		hawbItem.setHawbNumber((sl.getService().getDocumentList().get(0)
				.getHawbNo()).length() > 12 ? (sl.getService()
				.getDocumentList().get(0).getHawbNo()).substring(0, 11) : (sl
				.getService().getDocumentList().get(0).getHawbNo())); // mandatory
		hawbItem.setHawbStatus("S"); // mandatory
		hawbItem.setNumberOfPieces(sl.getService().getDocumentList().get(0)
				.getNoOfPieces().intValue()); // mandatory
		Weight grossWeight = new Weight(); // mandatory
		grossWeight.setValue(sl.getService().getBookedGrossWeightUnitKg()); // mandatory
		grossWeight.setUom(sl.getService().getDimensionUnit()
				.equals(DimensionUnit.CENTIMETERORKILOS) ? "K" : "L"); // mandatory
		hawbItem.setGrossWeight(grossWeight);
		Locations locations = new Locations(); // mandatory
		Port portOfOrigin = new Port(); // mandatory
		portOfOrigin.setCode(sl.getService().getPol().getPortCode()); // mandatory
		Port portOfDestination = new Port(); // mandatory
		portOfDestination.setCode(sl.getService().getPod().getPortCode()); // mandatory
		hawbItem.setLocations(locations);
		if (sl.getService().getDocumentList().get(0).getCommodityDescription() != null) {
			hawbItem.setCommodity((sl.getService().getDocumentList().get(0)
					.getCommodityDescription()).length() > 15 ? (sl
					.getService().getDocumentList().get(0)
					.getCommodityDescription()).substring(0, 14) : (sl
					.getService().getDocumentList().get(0)
					.getCommodityDescription())); // mandatory
		}
		hawbItem.setSlac(null);
	}

	// which is used for creating mawb services
	private void createMawb(PouchDTO prdto, Consol consol, MessagingDto request) {

		Mawb mawb = new Mawb();
		createMawbHeader(mawb, consol);
		createMawbParties(mawb, consol, request);
		setAccounting(consol, mawb);
		setShipmentReference(consol, mawb);
		setRoutingDetails(consol, mawb);
		setChargesDeclartion(consol, mawb);
		setHandling(consol, mawb);
		setRates(consol, mawb);
		setChargesSummary(consol, mawb);
		setOtherCharges(consol, mawb);
		setOPtionalSection(consol, mawb);
		setEmailNotification(consol, mawb);
		setExecution(consol, mawb);
		prdto.setMawb(mawb);
		/*
		 * setCreated(consol,mawb); setUpdated(consol,mawb);
		 */

	}

	// MAWB HEADER
	private void createMawbHeader(Mawb mawb, Consol consol) {
		mawb.setAwbNumber(((consol.getConsolDocument().getMawbNo()).substring(
				0, 3))
				+ "-"
				+ ((consol.getConsolDocument().getMawbNo()).substring(3, 11)));
		if (consol.getWinStatus() == null || consol.getWinStatus() == "R") {
			mawb.setAwbStatus("D");
		} else {
			mawb.setAwbStatus(consol.getWinStatus());
		}
		mawb.setEAwb(consol.getWinEawb().equals(BooleanType.TRUE) ? true
				: false);
		mawb.setWithPaper(consol.getWinEawb().equals(BooleanType.TRUE) ? true
				: false);
		log.info("Win web connect--MAWB header setting complete..");
	}

	// create MAWB parties
	private void createMawbParties(Mawb mawb, Consol consol,
			MessagingDto request) {
		Parties party = new Parties();
		SetParties(party, consol, request);
		mawb.setParties(party);
	}

	// both hawb and mawb
	private Parties SetParties(Parties party, Consol consol,
			MessagingDto request) {
		Party agent = new Party();
		// need to fetch form default master based on location and document
		// agentt
		if (request != null) {
			List<EdiConfigurationMaster> ediConfigurationMaster = new ArrayList<>();
			ediConfigurationMaster = ediConfigurationMasterRepository.findByKey("win-web-connect-request-agent-id");
			// ediConfigurationMaster=ediConfigurationMasterRepository.findByLocationId(request.getSelectedLocationId());
			if (ediConfigurationMaster != null
					&& ediConfigurationMaster.get(0).getEdiConfigurationValue() != null) {
				agent.setWinID(Integer.parseInt(ediConfigurationMaster
						.get(0).getEdiConfigurationValue()));
			} else {
				throw new RestException(
						ErrorCode.AGENT_IS_NOT_MAPPED_FOR_WIN_WEB_CONNECT);
			}
		} else {
			throw new RestException(
					ErrorCode.AGENT_IS_NOT_MAPPED_FOR_WIN_WEB_CONNECT);
		}
		agent.setAccountNumber(null);
		party.setAgent(agent);
		Party shipper = new Party();
		shipper.setWinID(null);
		// shipper.setAccountNumber(consol.getConsolDocument().getShipper().getPartyDetail().getAccountNumber()
		// == null ? 0 :
		// Integer.parseInt(consol.getConsolDocument().getShipper().getPartyDetail().getAccountNumber()));
		shipper.setAccountNumber(null);
		shipper.setName(consol.getConsolDocument().getShipper().getPartyName()
				.length() > 35 ? consol.getConsolDocument().getShipper()
				.getPartyName().substring(0, 34) : consol.getConsolDocument()
				.getShipper().getPartyName()); // mandatory
		shipper.setPhone(consol.getConsolDocument().getShipperAddress()
				.getPhone());
		shipper.setFax(consol.getConsolDocument().getShipperAddress().getFax());
		shipper.setEmail(consol.getConsolDocument().getShipperAddress()
				.getEmail());
		Address address = new Address(); // mandatory
		address.setLine1(consol.getConsolDocument().getShipperAddress()
				.getAddressLine1().length() > 35 ? consol.getConsolDocument()
				.getShipperAddress().getAddressLine1().substring(0, 34)
				: consol.getConsolDocument().getShipperAddress()
						.getAddressLine1()); // mandatgetShipperAddressory
		if (consol.getConsolDocument().getShipperAddress().getAddressLine2() != null) {
			address.setLine2(consol.getConsolDocument().getShipperAddress()
					.getAddressLine2().length() > 35 ? consol
					.getConsolDocument().getShipperAddress().getAddressLine2()
					.substring(0, 35) : consol.getConsolDocument()
					.getShipperAddress().getAddressLine2());
		} else {
			address.setLine2(null);
		}
		// mandatory if city is not present
		address.setPlace(consol.getLocation().getLocationName().length() > 17 ? consol
				.getLocation().getLocationName().substring(0, 16)
				: consol.getLocation().getLocationName()); // DOUBT
		// --Mandatory
		// if city
		// not present
		City city = new City();
		// mandatory if place is not present
		city.setCode(consol.getConsolDocument().getShipperAddress().getCity() != null ? consol
				.getConsolDocument().getShipperAddress().getCity()
				.getCityCode()
				: null);
		city.setName(consol.getConsolDocument().getShipperAddress().getCity() != null ? consol
				.getConsolDocument().getShipperAddress().getCity()
				.getCityName()
				: null);
		address.setCity(city);
		address.setPostalCode(consol.getConsolDocument().getShipperAddress()
				.getZipCode());
		address.setStateProvince(consol.getConsolDocument().getShipperAddress()
				.getState() != null ? consol.getConsolDocument()
				.getShipperAddress().getState().getStateName() : null);
		Country country = new Country(); // mandatory
		country.setCode(consol.getConsolDocument().getShipper()
				.getCountryMaster().getCountryCode()); // mandatory
		country.setName(consol.getConsolDocument().getShipper()
				.getCountryMaster().getCountryName());
		address.setCountry(country);
		shipper.setAddress(address);
		party.setShipper(shipper);
		// Consignee setting
		Party consignee = new Party(); // mandatory
		consignee.setWinID(null);
		// consignee.setAccountNumber(consol.getConsolDocument().getConsignee().getPartyDetail().getAccountNumber()
		// == null ? 0 :
		// Integer.parseInt(consol.getConsolDocument().getConsignee().getPartyDetail().getAccountNumber()));
		consignee.setAccountNumber(null);
		consignee.setName(consol.getConsolDocument().getConsignee()
				.getPartyName().length() > 35 ? consol.getConsolDocument()
				.getConsignee().getPartyName().substring(0, 34) : consol
				.getConsolDocument().getConsignee().getPartyName()); // mandatory
		consignee.setPhone(consol.getConsolDocument().getConsigneeAddress()
				.getPhone());
		consignee.setFax(consol.getConsolDocument().getConsigneeAddress()
				.getFax());
		consignee.setEmail(consol.getConsolDocument().getConsigneeAddress()
				.getEmail());
		Address cAddress = new Address(); // mandatory
		cAddress.setLine1(consol.getConsolDocument().getConsigneeAddress()
				.getAddressLine1().length() > 35 ? consol.getConsolDocument()
				.getConsigneeAddress().getAddressLine1().substring(0, 34)
				: consol.getConsolDocument().getConsigneeAddress()
						.getAddressLine1()); // mandatory
		if (consol.getConsolDocument().getConsigneeAddress().getAddressLine2() != null) {
			cAddress.setLine2(consol.getConsolDocument().getConsigneeAddress()
					.getAddressLine2().length() > 35 ? consol
					.getConsolDocument().getConsigneeAddress()
					.getAddressLine2().substring(0, 34) : consol
					.getConsolDocument().getConsigneeAddress()
					.getAddressLine2());
		} else {
			cAddress.setLine2(null);
		}
		// mandatory if city is not present
		cAddress.setPlace(consol.getLocation().getLocationName().length() > 17 ? consol
				.getLocation().getLocationName().substring(0, 16)
				: consol.getLocation().getLocationName()); // DOUBT--Mandatory
		// if city
		// not present
		City Ccity = new City();
		// mandatory if city is not present
		Ccity.setCode(consol.getConsolDocument().getConsigneeAddress()
				.getCity() != null ? consol.getConsolDocument()
				.getConsigneeAddress().getCity().getCityCode() : null);
		Ccity.setName(consol.getConsolDocument().getConsigneeAddress()
				.getCity() != null ? consol.getConsolDocument()
				.getConsigneeAddress().getCity().getCityName() : null);
		cAddress.setCity(Ccity);
		cAddress.setPostalCode(consol.getConsolDocument().getConsigneeAddress()
				.getZipCode());
		cAddress.setStateProvince(consol.getConsolDocument()
				.getConsigneeAddress().getState() != null ? consol
				.getConsolDocument().getConsigneeAddress().getState()
				.getStateName() : null);
		Country cCountry = new Country(); // mandatory
		cCountry.setCode(consol.getConsolDocument().getConsignee()
				.getCountryMaster().getCountryCode()); // mandatory
		cCountry.setName(consol.getConsolDocument().getConsignee()
				.getCountryMaster().getCountryName());
		cAddress.setCountry(cCountry);
		consignee.setAddress(cAddress);
		party.setConsignee(consignee);
		// Notify setting
		if (consol.getConsolDocument().getFirstNotify() != null) {
			Party notify = new Party();
			notify.setWinID(null);
			// notify.setAccountNumber(consol.getConsolDocument().getFirstNotify().getPartyDetail().getAccountNumber());
			notify.setAccountNumber(null);
			notify.setName(consol.getConsolDocument().getFirstNotify()
					.getPartyName().length() > 35 ? consol.getConsolDocument()
					.getFirstNotify().getPartyName().substring(0, 34) : consol
					.getConsolDocument().getFirstNotify().getPartyName()); // Mandatory
																			// if
																			// Notify
																			// Party
																			// present
			notify.setPhone(consol.getConsolDocument().getFirstNotifyAddress()
					.getPhone());
			notify.setFax(consol.getConsolDocument().getFirstNotifyAddress()
					.getFax());
			notify.setEmail(consol.getConsolDocument().getFirstNotifyAddress()
					.getEmail());
			Address notifyAddress = new Address(); // Mandatory if Notify Party
			// present
			notifyAddress.setLine1(consol.getConsolDocument()
					.getFirstNotifyAddress().getAddressLine1()); // Mandatory if
			// Notify Party
			// present
			notifyAddress.setLine2(consol.getConsolDocument()
					.getFirstNotifyAddress().getAddressLine2());
			// mandatory if city is not present
			cAddress.setPlace(consol.getLocation().getLocationName().length() > 17 ? consol
					.getLocation().getLocationName().substring(0, 16)
					: consol.getLocation().getLocationName()); // DOUBT--Mandatory
			// if
			// city not present
			City notifyCity = new City();
			// mandatory if city is not present
			notifyCity.setCode(consol.getConsolDocument()
					.getFirstNotifyAddress().getCity() != null ? consol
					.getConsolDocument().getFirstNotifyAddress().getCity()
					.getCityCode() : null);
			notifyCity.setName(consol.getConsolDocument()
					.getFirstNotifyAddress().getCity() != null ? consol
					.getConsolDocument().getFirstNotifyAddress().getCity()
					.getCityName() : null);
			notifyAddress.setCity(notifyCity);
			notifyAddress.setPostalCode(consol.getConsolDocument()
					.getFirstNotifyAddress().getZipCode());
			notifyAddress.setStateProvince(consol.getConsolDocument()
					.getFirstNotifyAddress().getState() != null ? consol
					.getConsolDocument().getFirstNotifyAddress().getState()
					.getStateName() : null);
			Country notifyCountry = new Country(); // Mandatory if Notify Party
			// present
			notifyCountry.setCode(consol.getConsolDocument().getFirstNotify()
					.getCountryMaster().getCountryCode()); // Mandatory if
															// Notify
			// Party present
			notifyCountry.setName(consol.getConsolDocument().getFirstNotify()
					.getCountryMaster().getCountryName());
			notifyAddress.setCountry(notifyCountry);
			notify.setAddress(notifyAddress);
			party.setNotify(notify);
		} else {
			party.setNotify(null);
		}
		party.setCoLoader(null);
		// Co-loader
		// doubt
		/*
		 * Party coloader =new Party(); coloader.setWinID(0);
		 * coloader.setAccountNumber
		 * (consol.getConsolDocument().getFirstNotify().
		 * getPartyDetail()==null?null
		 * :Integer.parseInt(consol.getConsolDocument(
		 * ).getFirstNotify().getPartyDetail().getAccountNumber()));
		 * party.setCoLoader(coloader);
		 */
		return party;
	}

	private void setExecution(Consol consol, Mawb mawb) {
		// TODO Auto-generated method stub
		// doubt
		Execution execution = new Execution();
		if (consol.getConsolDocument().getIssCarrierAgentSign() != null) {
			execution.setCarrierSignature(consol.getConsolDocument()
					.getIssCarrierAgentSign().length() > 20 ? consol
					.getConsolDocument().getIssCarrierAgentSign()
					.substring(0, 19) : consol.getConsolDocument()
					.getIssCarrierAgentSign()); // mandatory
		} else {
			execution.setCarrierSignature(consol.getConsolDocument().getAgent()
					.getPartyName().length() > 20 ? consol.getConsolDocument()
					.getAgent().getPartyName().substring(0, 19) : consol
					.getConsolDocument().getAgent().getPartyName());
		}
		execution.setDate(dateFormat.format(new Date())); // mandatory
		execution
				.setPlace(consol.getLocation().getLocationName().length() > 17 ? consol
						.getLocation().getLocationName().substring(0, 16)
						: consol.getLocation().getLocationName()); // mandatory

		if (consol.getConsolDocument().getShipperSignature() != null
				&& consol.getConsolDocument().getShipperSignature().trim() != null) {
			execution.setShipperSignature(consol.getConsolDocument()
					.getShipperSignature().length() > 20 ? consol
					.getConsolDocument().getShipperSignature().substring(0, 19)
					: consol.getConsolDocument().getShipperSignature()); // mandatory
		} else {
			execution.setCarrierSignature(consol.getConsolDocument()
					.getShipper().getPartyName().length() > 20 ? consol
					.getConsolDocument().getShipper().getPartyName()
					.substring(0, 19) : consol.getConsolDocument().getShipper()
					.getPartyName());
		}
		mawb.setExecution(execution);
	}

	private void setEmailNotification(Consol consol, Mawb mawb) {
		mawb.setEmailNotifications(null);
	}

	private void setOPtionalSection(Consol consol, Mawb mawb) {
		mawb.setOptionalSection(null);
	}

	private void setOtherCharges(Consol consol, Mawb mawb) {
		/*
		 * List < OtherCharges > otherCharges = new ArrayList < OtherCharges >
		 * (); // Not for (ConsolCharge cc: consol.getChargeList()) {
		 * OtherCharges otherCharge = new OtherCharges();
		 * otherCharge.setDescription("FR"); otherCharge.setAmount(22.25); if
		 * (cc.getPpcc() != null) { if (cc.getPpcc().equals(PPCC.Prepaid)) {
		 * otherCharge.setPrepaidCollect("P");
		 * otherCharge.setChargeIdentifier("P"); } else {
		 * otherCharge.setPrepaidCollect("C");
		 * otherCharge.setChargeIdentifier("C"); } } else { // we continue need
		 * all field mandatory continue; } otherCharges.add(otherCharge); }
		 */
		mawb.setOtherCharges(null);
	}

	private void setChargesSummary(Consol consol, Mawb mawb) {
		// TODO Auto-generated method stub
		ChargesSummary chargesSummary = new ChargesSummary(); // mandatory
		if (consol.getIataRate() != null
				&& consol.getConsolDocument().getChargebleWeight() != null) {
			chargesSummary.setWeightCharge(consol.getIataRate()
					* consol.getConsolDocument().getChargebleWeight()); // mandatory
		} else {
			throw new RestException(ErrorCode.WEIGHT_CHARGE_MANDATORY);
		}
		chargesSummary.setValuationCharge(null);
		chargesSummary.setTaxes(null);
		mawb.setChargesSummary(chargesSummary);
	}

	private void setRates(Consol consol, Mawb mawb) {
		List<Rates> rates = new ArrayList<>(0);
		// doubt
		Rates rate = new Rates(); // mandatory
		rate.setNumberOfPieces(consol.getConsolDocument().getNoOfPieces()
				.intValue()); // mandatory
		Weight grossWeight = new Weight(); // mandatory
		if (consol.getConsolDocument().getGrossWeight() < 0.1
				|| consol.getConsolDocument().getGrossWeight() > 9999999) {
			throw new RestException(ErrorCode.GROSS_WEIGHT_IS_INVALID);
		} else {
			grossWeight.setValue(consol.getConsolDocument().getGrossWeight()); // mandatory
		}
		grossWeight.setUom((consol.getConsolDocument().getDimensionUnit()
				.equals(DimensionUnit.CENTIMETERORKILOS)) ? "K" : "L"); // mandatory
		rate.setGrossWeight(grossWeight);
		Weight volume = new Weight();
		volume.setValue(null);
		volume.setUom(null);
		rate.setVolume(null); // doubt
		rate.setSlac(null); // doubt
		rate.setRateClassCode(consol.getConsolDocument().getRateClass().name());
		rate.setCommodityItemNumber(consol.getConsolDocument().getCommodityNo());
		rate.setChargeableWeight(consol.getConsolDocument()
				.getChargebleWeight());
		rate.setRateOrCharge(consol.getIataRate());
		if (consol.getIataRate() != null
				&& consol.getConsolDocument().getChargebleWeight() != null) {
			rate.setChargeAmount(consol.getConsolDocument()
					.getChargebleWeight() * consol.getIataRate()); // mandatory)
		} else {
			rate.setChargeAmount(null);
		}
		if (consol.getCommodityDescription() != null) {
			rate.setNatureAndQuantityOfGoods(consol.getCommodityDescription()
					.length() > 100 ? consol.getCommodityDescription()
					.substring(0, 99) : consol.getCommodityDescription());
		}
		if (consol.getConsolDocument().getDimensionList() != null
				&& consol.getConsolDocument().getDimensionList().size() > 0) {
			List<Dimension> dims = new ArrayList<>(consol
                    .getConsolDocument().getDimensionList().size());
			for (ConsolDocumentDimension consolDocumentDimension : consol
					.getConsolDocument().getDimensionList()) {
				Dimension dim = new Dimension(); // Whole section mandatory if
				// any one element exists
				dim.setPcs(consolDocumentDimension.getNoOfPiece()); // c
				dim.setHeight(consolDocumentDimension.getHeight()); // c
				dim.setLength(consolDocumentDimension.getLength()); // c
				dim.setUom((consol.getConsolDocument().getDimensionUnit()
						.equals(DimensionUnit.CENTIMETERORKILOS)) ? "CMT"
						: "INH"); // c
				dim.setWidth(consolDocumentDimension.getWidth());
				dims.add(dim);
			}
			rate.setDims(dims);
		}
		rate.setUld(null); // doubt
		/*
		 * UnitLoadDevice uld = new UnitLoadDevice(); uld.setNumber(""); //
		 * Mandatory if ULD object present
		 */
		rates.add(rate);
		mawb.setRates(rates);
	}

	private void setHandling(Consol consol, Mawb mawb) {
		// TODO Auto-generated method stub
		// doubt
		/*
		 * Handling handling =new Handling();
		 * handling.setSpecialServiceInformation("ddd");
		 * handling.setOtherServiceInformation(""); handling.setSci("sci");
		 * SpecialHandling specialHandling =new SpecialHandling();
		 * specialHandling.setCode("code");
		 * handling.setSpecialHandling(specialHandling);
		 */
		mawb.setHandling(null);
	}

	private void setChargesDeclartion(Consol consol, Mawb mawb) {
		// doubt
		ChargesDeclaration chargesDeclartion = new ChargesDeclaration(); // mandatory
		chargesDeclartion.setCurrencyCode(consol.getLocalCurrency()
				.getCurrencyCode()); // mandatory
		if (consol.getPpcc() == PPCC.Prepaid) {
			chargesDeclartion.setChargeCode("PP");
		} else {
			chargesDeclartion.setChargeCode("CC"); // mandatory
		}
		chargesDeclartion.setWeightOrValuation((consol.getPpcc().name())
				.charAt(0)); // mandatory
		chargesDeclartion.setOther((consol.getPpcc().name()).charAt(0)); // mandatory
		chargesDeclartion.setValuesForCarriage(null);
		chargesDeclartion.setValuesForCustom(null);
		chargesDeclartion.setValuesForInsurance(null);
		mawb.setChargesDeclartion(chargesDeclartion);
	}

	private void setRoutingDetails(Consol consol, Mawb mawb) {
		// TODO Auto-generated method stub
		if (consol.getConnectionList() != null
				&& consol.getConnectionList().size() > 0) {
			List<RoutingDetails> routingDetails = new ArrayList<>(
                    consol.getConnectionList().size());
			for (ConsolConnection consolConnection : consol.getConnectionList()) {
				RoutingDetails rs = new RoutingDetails(); // Mandatory
				rs.setFromAirPortCode(consolConnection.getPol().getPortCode()); // Mandatory
				rs.setToAirPortCode(consolConnection.getPod().getPortCode()); // Mandatory
				rs.setCarrierCode(consolConnection.getCarrierMaster()
						.getCarrierCode()); // Mandatory
				rs.setFlightNumber(consolConnection.getFlightVoyageNo()
						.length() > 5 ? consolConnection.getFlightVoyageNo()
						.substring(0, 4) : consolConnection.getFlightVoyageNo());
				rs.setFlightDate(dateFormat.format(consolConnection.getEtd()));
				routingDetails.add(rs);
			}
			mawb.setRoutingDetails(routingDetails);
		} else {
			if (consol.getEta() != null && consol.getEtd() != null
					&& consol.getCarrier() != null
					&& consol.getConsolDocument() != null
					&& consol.getConsolDocument().getRouteNo() != null
					&& consol.getPod() != null && consol.getPol() != null) {
				List<RoutingDetails> routingDetails = new ArrayList<>(
                        0);
				RoutingDetails rs = new RoutingDetails();
				rs.setFromAirPortCode(consol.getPol().getPortCode()); // Mandatory
				rs.setToAirPortCode(consol.getPod().getPortCode()); // Mandatory
				rs.setCarrierCode(consol.getCarrier().getCarrierCode()); // Mandatory
				rs.setFlightNumber(consol.getConsolDocument().getRouteNo()
						.length() > 5 ? consol.getConsolDocument().getRouteNo()
						.substring(0, 4) : consol.getConsolDocument()
						.getRouteNo());
				rs.setFlightDate(dateFormat.format(consol.getEtd()));
				routingDetails.add(rs);
				mawb.setRoutingDetails(routingDetails);
			} else {
				throw new RestException(ErrorCode.CONNECTION_LIST_REQUIRED);
			}
		}
	}

	private void setShipmentReference(Consol consol, Mawb mawb) {

		/*
		 * ShipmentReference shipmentReference=new ShipmentReference(); //doubt
		 * shipmentReference.setAdditionalInformation("addInfo");
		 * shipmentReference.setReferenceNumber("reference number");
		 */
		mawb.setShipmentReference(null);
	}

	// set accounting
	private void setAccounting(Consol consol, Mawb mawb) {

		mawb.setAccounting(null);
		/*
		 * List<Accounting> accounting=new ArrayList<>(); Accounting acc=new
		 * Accounting(); //doubt acc.setIdentifier("identifier");//Mandatory if
		 * Information is present
		 * acc.setInformation(consol.getPpcc().toString());//Mandatory if
		 * Information is present accounting.add(acc);
		 * mawb.setAccounting(accounting);
		 */
	}

}
