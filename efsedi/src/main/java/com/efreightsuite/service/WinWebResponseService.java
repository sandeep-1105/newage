package com.efreightsuite.service;

import lombok.Data;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

@Service
@Data
public class WinWebResponseService {
	
	@Autowired
	WinWebResponseImplService winWebResponseImplService;
	
	
     public void winResponeCheck(){
		winWebResponseImplService.processor();
     }

}
