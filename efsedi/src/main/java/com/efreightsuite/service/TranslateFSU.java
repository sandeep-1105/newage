package com.efreightsuite.service;

import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;

import org.springframework.stereotype.Service;

import com.efreightsuite.enumeration.EdiResponseType;
import com.efreightsuite.enumeration.EdiStatusType;
import com.efreightsuite.model.EdiResponseStatus;
import com.efreightsuite.util.EdiReader;
import com.ibm.icu.text.SimpleDateFormat;

import lombok.extern.log4j.Log4j2;

@Service
@Log4j2
public class TranslateFSU  {
	
	public Date ediDateToDate(String dateString) {
		Date date = null;
		if (dateString != null && dateString.length() == 9) {
			String anotherDateString = dateString.substring(0, 5) + Calendar.getInstance().get(Calendar.YEAR)
					+ dateString.substring(5, dateString.length());
			SimpleDateFormat format = new SimpleDateFormat("ddMMMyyyyhhmm");

			try {
				date = format.parse(anotherDateString);
			} catch (ParseException e) {
				log.error("Date Parsing Exception", e);
			}
		} else if (dateString.length() == 5) {
			String anotherDateString = dateString + Calendar.getInstance().get(Calendar.YEAR);
			SimpleDateFormat format = new SimpleDateFormat("ddMMMyyyy");
			try {
				date = format.parse(anotherDateString);
			} catch (ParseException e) {
				log.error("Date Parsing Exception", e);
			}
		}
		return date;
	}
	
	public EdiResponseStatus getObject(String fileLocation, String fileName) {
		
		EdiReader reader = new EdiReader();
		reader.setFileLocation(fileLocation);
		reader.setFileName(fileName);
		reader.read();
		
		log.info("file Content : " + reader.getFileContent());
		log.info("Content : " + reader.getValue(0, 0));

		EdiResponseStatus ediResponseStatus = new EdiResponseStatus();

		log.info("FSU.............................................");
		ediResponseStatus.setEdiStatusType(EdiStatusType.FSU);

		String tempMasterNumber = reader.getValue(1, 0).substring(0, 12);
		String carrierNumber = tempMasterNumber.substring(0, 3);
		String mawbTemp = tempMasterNumber.substring(4, tempMasterNumber.length());
		String masterNumber = carrierNumber + mawbTemp;

		ediResponseStatus.setMasterNumber(masterNumber);
		
		
		log.info("================" + reader.getValue(2, 0));
		if (reader.getValue(2, 0).equals("DLV")) {
			ediResponseStatus.setEdiType(EdiResponseType.DLV);
			ediResponseStatus.setStatusName("Consignment delivered to the consignee or agent");

			String strDate = reader.getValue(2, 1);
			log.info("===============================>" + strDate);
			ediResponseStatus.setStatusDate(ediDateToDate(strDate));

		} else if (reader.getValue(2, 0).equals("BKD")) {
			ediResponseStatus.setEdiType(EdiResponseType.BKD);
			ediResponseStatus.setStatusName("Consignment booked on a specific flight");

			String carrierCodeAndFlightNumber = reader.getValue(2, 1);
			ediResponseStatus.setFlightNumber(carrierCodeAndFlightNumber);
			ediResponseStatus.setCarrierCode(carrierCodeAndFlightNumber.substring(0, 2));

			String strDate = reader.getValue(2, 2);
			ediResponseStatus.setStatusDate(ediDateToDate(strDate));

		} else if (reader.getValue(2, 0).equals("RCS")) {
			ediResponseStatus.setEdiType(EdiResponseType.RCS);
			ediResponseStatus.setStatusName("Consignment received from shipper or agent");

			String strDate = reader.getValue(2, 1);
			log.info("===============================>" + strDate);
			ediResponseStatus.setStatusDate(ediDateToDate(strDate));

		} else if (reader.getValue(2, 0).equals("MAN")) {
			ediResponseStatus.setEdiType(EdiResponseType.MAN);
			ediResponseStatus.setStatusName("Consignment manifested on a specific flight");

			String carrierCodeAndFlightNumber = reader.getValue(2, 1);
			ediResponseStatus.setFlightNumber(carrierCodeAndFlightNumber);
			ediResponseStatus.setCarrierCode(carrierCodeAndFlightNumber.substring(0, 2));

			String strDate = reader.getValue(2, 2);
			log.info("===============================>" + strDate);
			ediResponseStatus.setStatusDate(ediDateToDate(strDate));

		} else if (reader.getValue(2, 0).equals("DEP")) {
			ediResponseStatus.setEdiType(EdiResponseType.DEP);
			ediResponseStatus.setStatusName("Consignment departed on a specific flight");

			String carrierCodeAndFlightNumber = reader.getValue(2, 1);
			ediResponseStatus.setFlightNumber(carrierCodeAndFlightNumber);
			ediResponseStatus.setCarrierCode(carrierCodeAndFlightNumber.substring(0, 2));

			String strDate = reader.getValue(2, 2);
			log.info("===============================>" + strDate);
			ediResponseStatus.setStatusDate(ediDateToDate(strDate));

		} else if (reader.getValue(2, 0).equals("TFD")) {
			ediResponseStatus.setEdiType(EdiResponseType.TFD);
			ediResponseStatus.setStatusName("Consignment transferred to another airline");

			String carrierCodeAndFlightNumber = reader.getValue(2, 1);
			// ediResponseStatus.setFlightNumber(carrierCodeAndFlightNumber);
			ediResponseStatus.setCarrierCode(carrierCodeAndFlightNumber.substring(0, 2));

			String strDate = reader.getValue(2, 2);
			log.info("===============================>" + strDate);
			ediResponseStatus.setStatusDate(ediDateToDate(strDate));

		} else if (reader.getValue(2, 0).equals("RCT")) {
			ediResponseStatus.setEdiType(EdiResponseType.RCT);
			ediResponseStatus.setStatusName("Consignment received from another airline");

			String carrierCodeAndFlightNumber = reader.getValue(2, 1);
			// ediResponseStatus.setFlightNumber(carrierCodeAndFlightNumber);
			ediResponseStatus.setCarrierCode(carrierCodeAndFlightNumber.substring(0, 2));

			String strDate = reader.getValue(2, 2);
			log.info("===============================>" + strDate);
			ediResponseStatus.setStatusDate(ediDateToDate(strDate));
		} else if (reader.getValue(2, 0).equals("RCF")) {
			ediResponseStatus.setEdiType(EdiResponseType.RCF);
			ediResponseStatus.setStatusName("Consignment received from a given flight");

			String carrierCodeAndFlightNumber = reader.getValue(2, 1);
			ediResponseStatus.setFlightNumber(carrierCodeAndFlightNumber);
			ediResponseStatus.setCarrierCode(carrierCodeAndFlightNumber.substring(0, 2));

			String strDate = reader.getValue(2, 2);
			log.info("===============================>" + strDate);
			ediResponseStatus.setStatusDate(ediDateToDate(strDate));

		} else if (reader.getValue(2, 0).equals("NFD")) {
			ediResponseStatus.setEdiType(EdiResponseType.NFD);
			ediResponseStatus
					.setStatusName("Consignment arrived at destination and the consignee or agent has been informed");

			String strDate = reader.getValue(2, 1);
			log.info("===============================>" + strDate);
			ediResponseStatus.setStatusDate(ediDateToDate(strDate));
		} else if (reader.getValue(2, 0).equals("AWD")) {
			ediResponseStatus.setEdiType(EdiResponseType.AWD);
			ediResponseStatus.setStatusName("Consignment arrival documents delivered to the consignee or agent");

			String strDate = reader.getValue(2, 1);
			log.info("===============================>" + strDate);
			ediResponseStatus.setStatusDate(ediDateToDate(strDate));

		} else if (reader.getValue(2, 0).equals("TRM")) {
			ediResponseStatus.setEdiType(EdiResponseType.TRM);
			ediResponseStatus.setStatusName("Consignment to be transferred to another airline");

			String carrierCodeAndFlightNumber = reader.getValue(2, 1);
			// ediResponseStatus.setFlightNumber(carrierCodeAndFlightNumber);
			ediResponseStatus.setCarrierCode(carrierCodeAndFlightNumber.substring(0, 2));

		} else if (reader.getValue(2, 0).equals("CCD")) {
			ediResponseStatus.setEdiType(EdiResponseType.CCD);
			ediResponseStatus.setStatusName("Consignment cleared by Customs");

			String strDate = reader.getValue(2, 1);
			log.info("===============================>" + strDate);
			ediResponseStatus.setStatusDate(ediDateToDate(strDate));

		} else if (reader.getValue(2, 0).equals("DIS")) {
			ediResponseStatus.setEdiType(EdiResponseType.DIS);
			ediResponseStatus.setStatusName("Consignment with a discrepancy");

			String carrierCodeAndFlightNumber = reader.getValue(2, 1);
			ediResponseStatus.setFlightNumber(carrierCodeAndFlightNumber);
			ediResponseStatus.setCarrierCode(carrierCodeAndFlightNumber.substring(0, 2));

			String strDate = reader.getValue(2, 2);
			log.info("===============================>" + strDate);
			ediResponseStatus.setStatusDate(ediDateToDate(strDate));
		}

		log.info(ediResponseStatus);
		
		return ediResponseStatus;
	}
	
}
