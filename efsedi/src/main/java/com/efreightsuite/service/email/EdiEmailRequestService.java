package com.efreightsuite.service.email;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.imageio.ImageIO;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.util.ByteArrayDataSource;

import com.efreightsuite.common.component.BufferedDataSource;
import com.efreightsuite.enumeration.EmailStatus;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.model.EmailRequest;
import com.efreightsuite.model.EmailRequestAttachment;
import com.efreightsuite.model.LocationMaster;
import com.efreightsuite.model.LogoMaster;
import com.efreightsuite.model.UserProfile;
import com.efreightsuite.repository.EmailRequestRepository;
import com.efreightsuite.repository.LogoMasterRepository;
import com.efreightsuite.util.AppUtil;
import com.efreightsuite.util.TimeUtil;
import com.efreightsuite.util.ValidateUtil;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class EdiEmailRequestService {
	
	@Value("${efs.resource.path}")
	String appResourcePath;
	
	@Autowired
	LogoMasterRepository logoMasterRepository;

	@Autowired
	AppUtil appUtil;

	@Autowired
	EmailRequestRepository emailRequestRepository;

	public void saveAndSendEmail(EmailRequest emailRequest, LocationMaster location, Map<String, byte[]> logoMap) {

		EmailRequest email = beforeSend(emailRequest, null, location);
		if (email.getId() != null) {
			emailRequest.setEmailStatus(EmailStatus.PROGRESSING);
			TimeUtil.setUpdateSystemTrackWithLocation(emailRequest.getManualSystemTrack(), location);

			emailRequestRepository.save(emailRequest);

			sendEmail(emailRequest, logoMap);
		} else {
			log.error("Email Not send because of could not save or invalid");
		}

	}

	public void saveAndSendEmail(EmailRequest emailRequest, UserProfile userProfile) {

		EmailRequest email = beforeSend(emailRequest, userProfile, null);
		if (email.getId() != null) {
			emailRequest.setEmailStatus(EmailStatus.PROGRESSING);
			TimeUtil.setUpdateSystemTrack(emailRequest.getManualSystemTrack(), userProfile);
			emailRequestRepository.save(emailRequest);
			sendEmail(emailRequest, getLogo(userProfile.getSelectedUserLocation()));
		} else {
			log.error("Email Not send because of could not save or invalid");
		}
	}
	
	public Map<String,byte[]> getLogo(LocationMaster location) {
		Map<String,byte[]> logoMap = new HashMap<>();
		logoMap.put("#COMPANY_LOGO#", null);
		logoMap.put("#POWERED_BY#", null);
		
		try{
			
			LogoMaster logoMaster = logoMasterRepository.findByLocationMaster(location);
			
			if(logoMaster!=null && logoMaster.getLogo()!=null){
				logoMap.put("#COMPANY_LOGO#", logoMaster.getLogo());
			}else{
				logoMap.put("#COMPANY_LOGO#", null);
			}
			
			ByteArrayOutputStream baos=new ByteArrayOutputStream(1000);
			BufferedImage img=ImageIO.read(new File(appResourcePath,"/efs-master-data/newage_small.png"));
			ImageIO.write(img, "png", baos);
			baos.flush();
			baos.close();
			
			logoMap.put("#POWERED_BY#", baos.toByteArray());
			
		}catch(Exception e){
			log.error("Not able to read powered by image in resource path ",e);
			logoMap.put("#POWERED_BY#", null);
		}
        
		return logoMap;
	}


	private EmailRequest beforeSend(EmailRequest emailRequest, UserProfile userProfile, LocationMaster location) {

		try {
			emailRequest.setEmailStatus(EmailStatus.PENDING);
			ValidateUtil.notNull(emailRequest.getFromEmailId(), ErrorCode.EMAIL_FROM_NOTNULL);
			ValidateUtil.notNull(emailRequest.getFromEmailIdPassword(), ErrorCode.EMAIL_FROM_NOTNULL);
			ValidateUtil.notNull(emailRequest.getSubject(), ErrorCode.EMAIL_SUBJECT_NOTNULL);
			ValidateUtil.notNull(emailRequest.getEmailBody(), ErrorCode.EMAIL_BODY_NOTNULL);
			ValidateUtil.notNull(emailRequest.getSmtpServerAddress(), ErrorCode.EMAIL_SERVER_ADDRESS_REQUIRED);
			ValidateUtil.notNull(emailRequest.getPort(), ErrorCode.EMAIL_SERVER_PORT_REQUIRED);

			if (!validateCSVEmailid(emailRequest.getToEmailIdList(), ErrorCode.EMAIL_TO_NOTNULL)) {
				emailRequest.setEmailStatus(EmailStatus.FAILURE);
				emailRequest.setErrorDesc(appUtil.getDesc(ErrorCode.EMAIL_TO_NOTNULL));
			} else if (!validateCSVEmailid(emailRequest.getBccEmailIdList(), ErrorCode.EMAIL_BCC_NOTNULL)) {
				emailRequest.setEmailStatus(EmailStatus.FAILURE);
				emailRequest.setErrorDesc(appUtil.getDesc(ErrorCode.EMAIL_BCC_NOTNULL));
			} else if (!validateCSVEmailid(emailRequest.getCcEmailIdList(), ErrorCode.EMAIL_CC_NOTNULL)) {
				emailRequest.setEmailStatus(EmailStatus.FAILURE);
				emailRequest.setErrorDesc(appUtil.getDesc(ErrorCode.EMAIL_CC_NOTNULL));
			}

			if (userProfile != null) {
				emailRequest.setManualSystemTrack(TimeUtil.getCreateSystemTrack(userProfile));
			} else if (location != null) {
				emailRequest.setManualSystemTrack(TimeUtil.getCreateSystemTrackWithLocation(location));
			}

			emailRequest = emailRequestRepository.save(emailRequest);
			log.info("Created EmailRequest with id: " + emailRequest.getId() + " is Valid: "
					+ emailRequest.getEmailStatus());

			emailRequest.setId(emailRequest.getId());

		} catch (RestException re) {

			log.error("Validation Excepiont in email request", re);

			String errorCode = re.getMessage().split(" ")[0];

			emailRequest.setEmailStatus(EmailStatus.FAILURE);
			emailRequest.setErrorDesc(appUtil.getDesc(errorCode));

		} catch (Exception e) {
			emailRequest.setEmailStatus(EmailStatus.FAILURE);
			emailRequest.setErrorDesc(appUtil.getDesc(ErrorCode.INVALID_PARAMETER));
			log.error("Validation Excepiont in email request", e);
		}

		return emailRequest;
	}

	private void sendEmail(EmailRequest emailRequest, Map<String, byte[]> logoMap) {

		try {

			EmailRequest tmpObj = emailRequestRepository.findById(emailRequest.getId());
			emailRequest.setVersionLock(tmpObj.getVersionLock());

			Session session = setPropertiesAndGetSession(emailRequest);

			// creates a new e-mail message
			Message msg = new MimeMessage(session);

			msg.setFrom(new InternetAddress(emailRequest.getFromEmailId()));
			msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(emailRequest.getToEmailIdList()));
			msg.setSubject(emailRequest.getSubject());
			msg.setSentDate(new Date());

			if (emailRequest.getCcEmailIdList() != null) {
				log.info("sending email as cc to  : " + emailRequest.getCcEmailIdList());
				msg.addRecipients(Message.RecipientType.CC, InternetAddress.parse(emailRequest.getCcEmailIdList()));
			}
			if (emailRequest.getBccEmailIdList() != null) {
				log.info("sending email as Bcc to  : " + emailRequest.getBccEmailIdList());
				msg.addRecipients(Message.RecipientType.BCC, InternetAddress.parse(emailRequest.getBccEmailIdList()));
			}

			// creates message part
			MimeBodyPart messageBodyPart = new MimeBodyPart();
			messageBodyPart.setContent(emailRequest.getEmailBody(), "text/html");

			// creates multi-part
			Multipart multipart = new MimeMultipart();
			multipart.addBodyPart(messageBodyPart);

			if (emailRequest.getAttachmentList() != null && emailRequest.getAttachmentList().size() > 0) {
				checkAndAddAttachments(emailRequest, multipart);
			}

			if (logoMap != null && logoMap.size() != 0) {

				for (String contentId : logoMap.keySet()) {

					if (logoMap.get(contentId) == null) {
						continue;
					}
					if (contentId.equals("#COMPANY_LOGO#")) {
						BodyPart tmp = new MimeBodyPart();
						BufferedDataSource fds = new BufferedDataSource(logoMap.get(contentId), "companylogo.png");
						// DataSource fds = new
						// FileDataSource("/home/administrator/Desktop/FSL.png");
						tmp.setDataHandler(new DataHandler(fds));
						tmp.setHeader("Content-ID", "<companylogo>");
						multipart.addBodyPart(tmp);
					} else if (contentId.equals("#POWERED_BY#")) {
						BodyPart tmp = new MimeBodyPart();
						BufferedDataSource fds = new BufferedDataSource(logoMap.get(contentId), "poweredby.png");
						// DataSource fds = new
						// FileDataSource("/home/administrator/Desktop/FSL.png");
						tmp.setDataHandler(new DataHandler(fds));
						tmp.setHeader("Content-ID", "<poweredby>");
						multipart.addBodyPart(tmp);
					}

				}
			}
			msg.setContent(multipart);

			Transport.send(msg);
			emailRequest.setEmailStatus(EmailStatus.SUCCESS);
		} catch (MessagingException e) {
			log.error("Error in sending Email : " + emailRequest.getFromEmailId() + ", "
					+ emailRequest.getFromEmailIdPassword(), e);
			emailRequest.setEmailStatus(EmailStatus.FAILURE);

			emailRequest.setErrorDesc(ErrorCode.ERROR_IN_EMAIL_SENDING);
		} catch (Exception e) {
			log.error("Exception in sending Email : " + emailRequest.getFromEmailId() + ", "
					+ emailRequest.getFromEmailIdPassword(), e);
			emailRequest.setEmailStatus(EmailStatus.FAILURE);

			emailRequest.setErrorDesc(ErrorCode.ERROR_IN_EMAIL_SENDING);
		}

		emailRequestRepository.save(emailRequest);

	}

	private Session setPropertiesAndGetSession(EmailRequest emailRequest) {
		Properties props = new Properties();
		props.put("mail.smtp.host", emailRequest.getSmtpServerAddress());
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.port", emailRequest.getPort());

		Session session = Session.getInstance(props, new javax.mail.Authenticator() {
			@Override
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(emailRequest.getFromEmailId(), emailRequest.getFromEmailIdPassword());
			}
		});
		return session;
	}

	private void checkAndAddAttachments(EmailRequest emailRequest, Multipart multipart) throws MessagingException {

		for (EmailRequestAttachment attachmentDto : emailRequest.getAttachmentList()) {
			if (attachmentDto.getMimeType().equalsIgnoreCase("text/html")) {
				log.info("Adding text/html MimeBodyPart");
				MimeBodyPart messageBodyPart = new MimeBodyPart();
				messageBodyPart.setContent(new String(attachmentDto.getAttachement()), attachmentDto.getMimeType());
				messageBodyPart.setFileName(attachmentDto.getFileName());
				multipart.addBodyPart(messageBodyPart);
			} else {
				log.info("Adding other than text/html MimeBodyPart");
				MimeBodyPart messageBodyPart = new MimeBodyPart();
				DataSource dataSource = new ByteArrayDataSource(attachmentDto.getAttachement(),
						attachmentDto.getMimeType());
				messageBodyPart.setDataHandler(new DataHandler(dataSource));
				messageBodyPart.setFileName(attachmentDto.getFileName());
				multipart.addBodyPart(messageBodyPart);
			}
		}

	}

	private boolean validateCSVEmailid(String csvEmailIDs, String errorDesc) {
		boolean isValid = true;
		if (csvEmailIDs != null && csvEmailIDs.length() != 0) {
			String[] emailidArray = csvEmailIDs.split(",");
			try {
				for (String emailID : emailidArray) {
					log.info("Validating email:" + emailID);
					ValidateUtil.notNull(emailID, errorDesc);
				}
			} catch (Exception e) {
				isValid = false;
				log.error("Invalid Email ID in: " + appUtil.getDesc(errorDesc), e);
			}
			log.info("Validation Result for " + appUtil.getDesc(errorDesc) + " :" + csvEmailIDs + ":" + isValid);
			return isValid;
		} else {
			return true;
		}

	}

}
