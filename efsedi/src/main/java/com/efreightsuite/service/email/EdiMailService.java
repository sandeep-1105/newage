package com.efreightsuite.service.email;

import java.util.HashMap;
import java.util.Map;

import com.efreightsuite.common.component.CacheRepository;
import com.efreightsuite.enumeration.TemplateType;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.model.EmailRequest;
import com.efreightsuite.model.EmailTemplate;
import com.efreightsuite.model.UserProfile;
import com.efreightsuite.repository.EmailTemplateRepository;
import com.efreightsuite.util.SaaSUtil;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service
@Log4j2
public class EdiMailService {
	
	@Autowired
	CacheRepository cacheRepository;
	
	@Autowired
	EmailTemplateRepository emailTemplateRepository;
	
	@Autowired
	EdiEmailRequestService ediEmailRequestService;
	
	@Async
	@Transactional(propagation=Propagation.REQUIRES_NEW)
	public void sendEmail(String toEmail, String content, UserProfile userProfile) {
		log.info("Sending EDI email to [ " + toEmail + "]" );
		
		
		EmailTemplate emailTemplate = cacheRepository.get(SaaSUtil.getSaaSId(), EmailTemplate.class.getName(), TemplateType.AIRLINE_EDI_EMAIL, emailTemplateRepository);
		
		if (emailTemplate == null) {
			log.error("No email template available for EDI");
			return;
		}
		
		try {

			Map<String,Object> mapSubject = new HashMap<>();
			//mapSubject.put("#EDI_SUBJECT#", "EDI Subject");
			
			Map<String,Object> mapBody = new HashMap<>();
			mapBody.put("#EDI_CONTENT#", content);
			 
			// sending email

			EmailRequest emailRequest = new EmailRequest();
			emailRequest.setFromEmailId(emailTemplate.getFromEmailId());
			emailRequest.setFromEmailIdPassword(emailTemplate.getFromEmailPassword());
			emailRequest.setToEmailIdList(toEmail);
			//emailRequest.setCcEmailIdList(ccEmail);
			emailRequest.setEmailType(emailTemplate.getEmailType());
			emailRequest.setPort(emailTemplate.getPort());
			emailRequest.setSmtpServerAddress(emailTemplate.getSmtpServerAddress());
			emailRequest.setSubject(emailTemplate.getSubject());
			emailRequest.setEmailBody(emailTemplate.getTemplate());
			
			for(String key : mapSubject.keySet()){
				emailRequest.setSubject(emailRequest.getSubject().replaceAll(key, mapSubject.get(key)==null?"":mapSubject.get(key).toString()));
			}
			
			for(String key : mapBody.keySet()){
				emailRequest.setEmailBody(emailRequest.getEmailBody().replaceAll(key, mapBody.get(key)==null?"":mapBody.get(key).toString()));
			}
			
			ediEmailRequestService.saveAndSendEmail(emailRequest, userProfile);
		}
		catch (RestException re) {
			log.error("RestException in while sending the EDI Email ", re);
		} catch (Exception e) {
			log.error("Exception in while sending the EDI Email ", e);
		}
	}
	
}
