package com.efreightsuite.service;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.model.Consol;
import com.efreightsuite.model.EdiConfigurationMaster;
import com.efreightsuite.repository.ConsolRepository;
import com.efreightsuite.repository.EdiConfigurationMasterRepository;
import com.efreightsuite.winwebconnect.LogOnModel;
import com.efreightsuite.winwebconnect.dto.ApiTrackingStatusDTO;
import com.efreightsuite.winwebconnect.dto.PouchDTO;
import com.efreightsuite.winwebconnect.dto.PouchResponseDto;
import lombok.extern.log4j.Log4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

//Rest templates initiation
@Service
@Log4j
public class WinWebConnectRestServices {
	
	@Autowired
	EdiConfigurationMasterRepository ediConfigurationMasterRepository;
	
    @Autowired
    RestTemplate winRestTemplate;
    
    @Autowired
    ConsolRepository consolRepository;
  
	 List<EdiConfigurationMaster> ediConfigiurationMaster = null;
	
	 
   //For Login	
	String winWebLogin(Consol consol){
		String authTokenValue=null;
		winRestTemplate=new RestTemplate();
		LogOnModel LogOnModel=new LogOnModel();
		String uri="";
		log.info("login called wth win web connect...");
		ediConfigiurationMaster = ediConfigurationMasterRepository.findByKey("win-web-connect-login");
		if(ediConfigiurationMaster!=null){
			uri=ediConfigiurationMaster.get(0).getEdiConfigurationValue();
		}else{
   			throw new RestException(ErrorCode.EDI_CONFIGURATION_NOT_CONFIGURED);
   		}
		ediConfigiurationMaster = ediConfigurationMasterRepository.findByKey("win-web-connect-username");
		if(ediConfigiurationMaster!=null){
		   LogOnModel.setUsername(ediConfigiurationMaster.get(0).getEdiConfigurationValue());
		}else{
   			
   			throw new RestException(ErrorCode.EDI_CONFIGURATION_NOT_CONFIGURED);
   		}
		 ediConfigiurationMaster = ediConfigurationMasterRepository.findByKey("win-web-connect-password");
		if(ediConfigiurationMaster!=null){
			LogOnModel.setPassword(ediConfigiurationMaster.get(0).getEdiConfigurationValue());
   		}else{
   			throw new RestException(ErrorCode.EDI_CONFIGURATION_NOT_CONFIGURED);
   		}
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<LogOnModel> entity = new HttpEntity<>(LogOnModel, headers);
		ResponseEntity<PouchResponseDto> pouchResponseDto = winRestTemplate.exchange(uri, HttpMethod.POST, entity, PouchResponseDto.class);
		authTokenValue=getAuthToken(pouchResponseDto);
		return authTokenValue;
	}
	
	
	String getAuthToken(ResponseEntity<PouchResponseDto> pouchResponseDto){
		String[] cookies=pouchResponseDto.getHeaders().get("Set-Cookie").get(0).split(";");
		String[] cookies2=cookies[0].split("=");
		String finalToken=cookies2[1];
		return finalToken;
	}

	//POST AND PUT
	ResponseEntity<PouchResponseDto> createRequestWinWeb(PouchDTO pouchRequestDto,String authTokenValue,Consol consol) {
		 winRestTemplate=new RestTemplate();
		 ResponseEntity<PouchResponseDto> pouchResponseDto = null;
		 HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON);
			headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
			headers.add("Cookie", "authToken=" + authTokenValue);
			headers.add( "Cache-Control", "no-cache" ); 
			if(consol!=null && (consol.getWinAwbID()==null || consol.getWinAwbID()== 0L)) {
				ediConfigiurationMaster = ediConfigurationMasterRepository.findByKey("win-web-connect-request-post");
				if(ediConfigiurationMaster.get(0).getEdiConfigurationValue()!=null){
					final String uri = ediConfigiurationMaster.get(0).getEdiConfigurationValue();
				    HttpEntity<PouchDTO> entity = new HttpEntity<>(pouchRequestDto, headers);
				    pouchResponseDto= winRestTemplate.exchange(uri, HttpMethod.POST, entity, PouchResponseDto.class);
				}else{
					throw new RestException(ErrorCode.EDI_CONFIGURATION_NOT_CONFIGURED);
				}
			}else{
				  Map<String, Long> param = new HashMap<>();
				    param.put("AwbID",consol.getWinAwbID());
				ediConfigiurationMaster = ediConfigurationMasterRepository.findByKey("win-web-connect-request-update");
				if(ediConfigiurationMaster!=null){
					 final String uri = ediConfigiurationMaster.get(0).getEdiConfigurationValue();
					    HttpEntity<PouchDTO> entity = new HttpEntity<>(pouchRequestDto, headers);
					   pouchResponseDto = winRestTemplate.exchange(uri,HttpMethod.PUT, entity, PouchResponseDto.class,param);
				}else{
					throw new RestException(ErrorCode.EDI_CONFIGURATION_NOT_CONFIGURED);
				}
			}
		return pouchResponseDto;
	}

      //Requesting for Last status of CONSOL BASED on AGENT      
     ResponseEntity<List<ApiTrackingStatusDTO>> requestLastStatusForAgent(String authTokenValue){
    	 
    	 if(authTokenValue!=null){
    		 HttpHeaders headers = new HttpHeaders();
    		 ResponseEntity<List<ApiTrackingStatusDTO>> response;
    			headers.setContentType(MediaType.APPLICATION_JSON);
    			headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
    			headers.add("Cookie", "authToken=" + authTokenValue);
    			headers.add( "Cache-Control", "no-cache" );
    	        ediConfigiurationMaster = ediConfigurationMasterRepository.findByKey("win-web-connect-request-latest-status-agent");
    	        if(ediConfigiurationMaster!=null && ediConfigiurationMaster.get(0).getEdiConfigurationValue()!=null){
    	        	 String uri = ediConfigiurationMaster.get(0).getEdiConfigurationValue();
    	  	        ediConfigiurationMaster = ediConfigurationMasterRepository.findByKey("win-web-connect-request-agent-id");
    	  	        Map<String, String> params = new HashMap<>();
    	  	        if(ediConfigiurationMaster!=null && ediConfigiurationMaster.get(0).getEdiConfigurationValue()!=null ){
    	  	        	
    	  	        }else{
    	  	        	throw new RestException(ErrorCode.EDI_CONFIGURATION_NOT_CONFIGURED);	
    	  	        }
    	             params.put("AgentID",ediConfigiurationMaster.get(0).getEdiConfigurationValue());
    	             HttpEntity<HttpHeaders> entity = new HttpEntity<>(null,headers);
    	           response = winRestTemplate.exchange(uri, HttpMethod.GET, entity, new ParameterizedTypeReference<List<ApiTrackingStatusDTO>>(){},params);
    	           System.out.println("ee"+response.getBody());
    	           return (ResponseEntity<List<ApiTrackingStatusDTO>>) response;
    	        }
    	 }else{
    		 throw new RestException(ErrorCode.TOKENINVALID);
    	 }
      return null;           
}
	
	
	
	
	
}
