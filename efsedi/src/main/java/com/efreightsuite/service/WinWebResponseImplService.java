package com.efreightsuite.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.efreightsuite.enumeration.EdiMessagingFor;
import com.efreightsuite.enumeration.WinStatusCodes;
import com.efreightsuite.model.Consol;
import com.efreightsuite.model.EdiConfigurationMaster;
import com.efreightsuite.model.HawbWinResponse;
import com.efreightsuite.model.WinStatusResponse;
import com.efreightsuite.model.WinWebConnectResponse;
import com.efreightsuite.repository.ConsolRepository;
import com.efreightsuite.repository.EdiConfigurationMasterRepository;
import com.efreightsuite.repository.WinStatusResponseRepository;
import com.efreightsuite.repository.WinWebConnectResponseRepository;
import com.efreightsuite.util.TimeUtil;
import com.efreightsuite.winwebconnect.AirWayBill;
import com.efreightsuite.winwebconnect.HawbResponse;
import com.efreightsuite.winwebconnect.LogOnModel;
import com.efreightsuite.winwebconnect.Statuses;
import com.efreightsuite.winwebconnect.dto.ApiTrackingStatusDTO;
import com.efreightsuite.winwebconnect.dto.PouchResponseDto;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;

@Service
@Log4j2
public class WinWebResponseImplService // extends AbstractResponseService
{

	SimpleDateFormat dateFormat = new SimpleDateFormat ("yyyy-MM-dd'T'HH:mm:ss'Z'");
	
	@Autowired
    RestTemplate winRestTemplate;
	
	@Autowired
	WinWebConnectRestServices winWebConnectRestServices;
	
	@Autowired
	WinWebConnectMessagingService winWebConnectMessagingService;
	
	@Autowired
	EdiConfigurationMasterRepository ediConfigurationMasterRepository;
	
	@Autowired
	ConsolRepository consolRepository;
	
	@Autowired
	WinWebConnectResponseRepository winWebConnectResponseRepository;
	
	@Autowired
	WinStatusResponseRepository winStatusResponseRepository;

	 List<EdiConfigurationMaster> ediConfigiurationMaster = null;
	
	
	public EdiMessagingFor getMessageFor() {
		return EdiMessagingFor.WIN;
	}

	//@Override
	public void processor() {
		String uri = null;
		try{
			winRestTemplate=new RestTemplate();
			log.info("login called wth win web connect...");
			ediConfigiurationMaster = ediConfigurationMasterRepository.findByKey("win-web-connect-login");
			if(ediConfigiurationMaster != null && ediConfigiurationMaster.get(0).getEdiConfigurationValue()!=null){
				uri=ediConfigiurationMaster.get(0).getEdiConfigurationValue();
				LogOnModel LogOnModel=new LogOnModel();
				ediConfigiurationMaster = ediConfigurationMasterRepository.findByKey("win-web-connect-username");
				if(ediConfigiurationMaster != null && ediConfigiurationMaster.get(0).getEdiConfigurationValue()!=null){
					LogOnModel.setUsername(ediConfigiurationMaster.get(0).getEdiConfigurationValue());
				}
				ediConfigiurationMaster = ediConfigurationMasterRepository.findByKey("win-web-connect-password");
				if(ediConfigiurationMaster != null && ediConfigiurationMaster.get(0).getEdiConfigurationValue()!=null){
					LogOnModel.setPassword(ediConfigiurationMaster.get(0).getEdiConfigurationValue());
				}
				HttpHeaders headers = new HttpHeaders();
				headers.setContentType(MediaType.APPLICATION_JSON);
				HttpEntity<LogOnModel> entity = new HttpEntity<>(LogOnModel, headers);
				ResponseEntity<PouchResponseDto> pouchResponseDto = winRestTemplate.exchange(uri, HttpMethod.POST, entity, PouchResponseDto.class);
				String authTokenValue=getAuthToken(pouchResponseDto);
				responseForAwb(authTokenValue);
			}else{
				log.info("Uri not configured");
			}
				
		}catch(HttpStatusCodeException exception){
			log.info("Exception while processing for last status win request",exception);
		}catch(Exception e){
			log.info("Exception while processing for last status win request",e);
		}
	}
	
	String getAuthToken(ResponseEntity<PouchResponseDto> pouchResponseDto){
		String[] cookies=pouchResponseDto.getHeaders().get("Set-Cookie").get(0).split(";");
		String[] cookies2=cookies[0].split("=");
		String finalToken=cookies2[1];
		return finalToken;
	}

	void responseForAwb(String authTokenValue) {
		List<ApiTrackingStatusDTO> apiTrackingStatusDTOs=(winWebConnectRestServices.requestLastStatusForAgent(authTokenValue)).getBody();
      if(apiTrackingStatusDTOs!=null && apiTrackingStatusDTOs.size()>0){
    	  Consol consol=new Consol();
          for(ApiTrackingStatusDTO apiTrackingStatusDTO:apiTrackingStatusDTOs){
        	  AirWayBill airWayBill=new AirWayBill();
        	  airWayBill=apiTrackingStatusDTO.getAirwayBill();
        	  WinStatusResponse  winStatusResponse;
        	  winStatusResponse=winStatusResponseRepository.findByAwbId(airWayBill.getAwbID());
	            if(winStatusResponse!=null){
        			
        		}else{
        			winStatusResponse=new WinStatusResponse();
        		}
        	  if(airWayBill!=null && airWayBill.getStatuses()!=null && airWayBill.getStatuses().size()>0){
        		  for(Statuses status:airWayBill.getStatuses()){
        			  if(status.getDetails()!=null && status.getDetails().size()>0){
        				  airWayBill.setMawbStatus(status.getDetails().get(status.getDetails().size()-1).getStatusCode());
        				  break;
        			  }
            	  }
        	  }
              winStatusResponse.setStatus(airWayBill.getMawbStatus());
        	  winStatusResponse.setAwbId(airWayBill.getAwbID());
              winStatusResponse.setAwbNumber(airWayBill.getAwbNumber());
        		  //winStatusResponse.setDestination(pouchCardDTO.getDestination().getCode());
        		  //winStatusResponse.setOriginName(pouchCardDTO.getDestination().getCode());
        		  
        		 // winStatusResponse.setCreatedDate(arrayOfPouchCardDTO.getCreatedDate());
        		  //winStatusResponse.setUpdatedDate(arrayOfPouchCardDTO.getUpdatedDate());
        		  consol=consolRepository.findByAwbId(airWayBill.getAwbID());
        		  if(consol!=null){
        			  WinWebConnectResponse winWebConnectResponse=null;
        			  winWebConnectResponse= winWebConnectResponseRepository.findByConsolUid(consol.getConsolUid());
        			  if(winWebConnectResponse!=null){
        				  winStatusResponse.setConsolUid(consol.getConsolUid());
            			  winWebConnectResponse.setWinStatus(airWayBill.getMawbStatus());
            			  winWebConnectResponse.setDatetime(dateFormat.format(new Date()));
            			  winWebConnectResponse.setRemarks(new ArrayList<>());
            			  winWebConnectResponseRepository.save(winWebConnectResponse);
            		  }
        			  consol.setWinAwbID(airWayBill.getAwbID());
        			  consol.setWinStatus(airWayBill.getMawbStatus());
        			  consol.setWinErrorCode(null);
        			  consol.setWinErrorMessage(null);
        			  if(consol.getAtd()==null && airWayBill.getMawbStatus()!=null   && airWayBill.getMawbStatus().equalsIgnoreCase(WinStatusCodes.DEP.name())){
        				  consol.setAtd(TimeUtil.getCurrentLocationTime(consol.getLocation()));
        			  }
        			  if(consol.getAta()==null &&airWayBill.getMawbStatus()!=null   && airWayBill.getMawbStatus().equalsIgnoreCase(WinStatusCodes.ARR.name())){
        				  consol.setAta(TimeUtil.getCurrentLocationTime(consol.getLocation()));
        			  }
        			  consolRepository.save(consol);
        			  
        		  }
        		  if(airWayBill.getHawb()!=null && airWayBill.getHawb().size()>0){
            		  List<HawbWinResponse> hawbWinResponseList= new ArrayList<>(airWayBill.getHawb().size());
        			  for(HawbResponse H :airWayBill.getHawb()){
            			  HawbWinResponse hawbWinResponse=new HawbWinResponse();
            			  hawbWinResponse.setHawbNumber(H.getHawbNumber());
            			  hawbWinResponse.setStatus(H.getStatusCode());
            			  hawbWinResponse.setWinStatusResponse(winStatusResponse);
            			  hawbWinResponseList.add(hawbWinResponse);
            		  }
        			  winStatusResponse.setHawbs(hawbWinResponseList);
        		  }else{
        			  winStatusResponse.setHawbs(new ArrayList<>());
        		  }
        		  winStatusResponseRepository.save(winStatusResponse);
        	 
          }
      }
      }
}
