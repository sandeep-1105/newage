package com.efreightsuite.service;

import java.util.Date;

import org.springframework.stereotype.Service;

import com.efreightsuite.channel.stdin.AbstractMessagingService;
import com.efreightsuite.communication.FtpDto;
import com.efreightsuite.communication.FtpTransfer;
import com.efreightsuite.dto.MessagingDto;
import com.efreightsuite.enumeration.EdiMessagingFor;
import com.efreightsuite.enumeration.EdiStatus;
import com.efreightsuite.enumeration.YesNo;
import com.efreightsuite.exception.EDIGenerationException;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.model.AirlineEdi;
import com.efreightsuite.model.AirlineEdiHouse;
import com.efreightsuite.model.AirlineEdiStatus;
import com.efreightsuite.model.LocationMaster;
import com.efreightsuite.model.UserProfile;

import lombok.extern.log4j.Log4j2;

@Service
@Log4j2
public class EkEdiMessagingService extends AbstractMessagingService {
	
	public EdiMessagingFor getMessageFor() {
		return EdiMessagingFor.EK;
	}

	public void process(MessagingDto request) {

		log.info("=========================================EK EDI PROCESS BEGINS==================================================");
		String fileName = "";
		FtpDto ftpData = null;
		boolean success = false;
		FtpTransfer ftpTransfer = new FtpTransfer();
		UserProfile userProfile = userProfileRepository.findOne(request.getUserProfileId());
		AirlineEdi airlineEdi = airlineEdiRepository.findOne(request.getEdiId());
		LocationMaster selectedUserLocation = locationMasterRepository.findOne(request.getSelectedLocationId());

		userProfile.setSelectedUserLocation(selectedUserLocation);
		AirlineEdiStatus airlineEdiStatus = new AirlineEdiStatus();
		airlineEdiStatus.setCreateDate(new Date());
		airlineEdiStatus.setAirlineEdi(airlineEdi);

		if (airlineEdi != null && airlineEdi.getCarrierMaster() != null
				&& airlineEdi.getCarrierMaster().getMessagingType() != null) {

			ftpData = ftpConfigurationService.getFtpData(EdiMessagingFor.EK);

			if (ftpData != null && ftpData.getLocalFileInputLocation() != null && ftpData.getRemoteFileInputLocation() != null
					&& ftpData.getServer() != null && ftpData.getUserName() != null && ftpData.getPassword() != null) {
				
				// Generating FWB 
				try {
					fileName = fwbService.generate(airlineEdi, selectedUserLocation, ftpData, userProfile);
				} catch (EDIGenerationException ediGenerationException) {
					// FTP Configuration Error
					log.error("Error while generating edi ", ediGenerationException);
					airlineEdiStatus.setStatus(EdiStatus.LOCAL_FILE_CONFIGURATION_FAILURE);
					airlineEdiStatus.setStatusMessage(appUtil.getDesc(ErrorCode.EDI_STATUS_LOCAL_FILE_CONFIG_MISSING));
					airlineEdi.setStatus(EdiStatus.LOCAL_FILE_CONFIGURATION_FAILURE);
					airlineEdi.setStatusMessage(appUtil.getDesc(ErrorCode.EDI_STATUS_LOCAL_FILE_CONFIG_MISSING));
				}
				ftpData.setFileName(fileName);
				success = ftpTransfer.upload(ftpData);
				if (success) {
					airlineEdiStatus.setStatus(EdiStatus.EDI_GENERATION_SUCCESS);
					airlineEdiStatus.setStatusMessage(appUtil.getDesc(ErrorCode.EDI_STATUS_SUCCESS));
					airlineEdi.setStatus(EdiStatus.EDI_GENERATION_SUCCESS);
					airlineEdi.setStatusMessage(appUtil.getDesc(ErrorCode.EDI_STATUS_SUCCESS));
				} else {
					airlineEdiStatus.setStatus(EdiStatus.FTP_UPLOAD_FAILURE);
					airlineEdiStatus.setStatusMessage(appUtil.getDesc(ErrorCode.EDI_STATUS_FTP_UPLOAD_FAILURE));
					airlineEdi.setStatus(EdiStatus.FTP_UPLOAD_FAILURE);
					airlineEdi.setStatusMessage(appUtil.getDesc(ErrorCode.EDI_STATUS_FTP_UPLOAD_FAILURE));
				}
				success = false;
			
				
				
				
				// Generating FHL 
				
				if (airlineEdi.getDirectShipment().equals(YesNo.No)) {
					int count = 1;
					
					for (AirlineEdiHouse ediHouse : airlineEdi.getHouseList()) {
						try {
							fileName = fhlService.generate(airlineEdi, selectedUserLocation,ediHouse,ftpData,userProfile, count++);
						} catch (EDIGenerationException ediGenerationException) {
							// FTP Configuration Error
							log.error("Error while generating edi ", ediGenerationException);
							airlineEdiStatus.setStatus(EdiStatus.LOCAL_FILE_CONFIGURATION_FAILURE);
							airlineEdiStatus.setStatusMessage(appUtil.getDesc(ErrorCode.EDI_STATUS_LOCAL_FILE_CONFIG_MISSING));

							airlineEdi.setStatus(EdiStatus.LOCAL_FILE_CONFIGURATION_FAILURE);
							airlineEdi.setStatusMessage(appUtil.getDesc(ErrorCode.EDI_STATUS_LOCAL_FILE_CONFIG_MISSING));
						}
						ftpData.setFileName(fileName);
						success = ftpTransfer.upload(ftpData);

						if (success) {
							airlineEdiStatus.setStatus(EdiStatus.EDI_GENERATION_SUCCESS);
							airlineEdiStatus.setStatusMessage(appUtil.getDesc(ErrorCode.EDI_STATUS_SUCCESS));

							airlineEdi.setStatus(EdiStatus.EDI_GENERATION_SUCCESS);
							airlineEdi.setStatusMessage(appUtil.getDesc(ErrorCode.EDI_STATUS_SUCCESS));
						} else {
							airlineEdiStatus.setStatus(EdiStatus.FTP_UPLOAD_FAILURE);
							airlineEdiStatus.setStatusMessage(appUtil.getDesc(ErrorCode.EDI_STATUS_FTP_UPLOAD_FAILURE));

							airlineEdi.setStatus(EdiStatus.FTP_UPLOAD_FAILURE);
							airlineEdi.setStatusMessage(appUtil.getDesc(ErrorCode.EDI_STATUS_FTP_UPLOAD_FAILURE));
						}

					}
				}
				
			} else {

				airlineEdiStatus.setStatus(EdiStatus.FTP_CONFIGURATION_FAILURE);
				airlineEdiStatus.setStatusMessage(appUtil.getDesc(ErrorCode.EDI_STATUS_FTP_CONFIGURATION_MISSING));

				airlineEdi.setStatus(EdiStatus.FTP_CONFIGURATION_FAILURE);
				airlineEdi.setStatusMessage(appUtil.getDesc(ErrorCode.EDI_STATUS_FTP_CONFIGURATION_MISSING));
			}
			

			airlineEdiStatusRepository.save(airlineEdiStatus);

			airlineEdiRepository.updateEdiStatus(airlineEdi.getStatusMessage(), airlineEdi.getStatus(),airlineEdi.getId());
		}

	}
}
