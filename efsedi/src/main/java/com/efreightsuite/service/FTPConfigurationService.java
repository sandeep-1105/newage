package com.efreightsuite.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.efreightsuite.communication.FtpDto;
import com.efreightsuite.enumeration.EdiMessagingFor;
import com.efreightsuite.model.EdiConfigurationMaster;
import com.efreightsuite.repository.EdiConfigurationMasterRepository;

@Service
public class FTPConfigurationService {

	@Autowired
	EdiConfigurationMasterRepository ediConfigurationMasterRepository;

	public FtpDto getFtpData(EdiMessagingFor ediMessagingFor) {

		String searchString = ediMessagingFor.toString();

		FtpDto ftpData = null;

		List<EdiConfigurationMaster> ediConfiguration = ediConfigurationMasterRepository.findAllByKeyLike(searchString + "%");

		if (ediConfiguration == null || ediConfiguration.isEmpty()) {
			return null;
		} else {
			ftpData = new FtpDto();
			for (EdiConfigurationMaster configuration : ediConfiguration) {
				if (configuration.getEdiConfigurationKey().equals(searchString + "_FTP_IP_ADDRESS")) {
					ftpData.setServer(configuration.getEdiConfigurationValue());
				} else if (configuration.getEdiConfigurationKey().equals(searchString + "_FTP_PORT_NUMBER")) {
					ftpData.setPort(Integer.parseInt(configuration.getEdiConfigurationValue()));
				} else if (configuration.getEdiConfigurationKey().equals(searchString + "_FTP_USER_NAME")) {
					ftpData.setUserName(configuration.getEdiConfigurationValue());
				} else if (configuration.getEdiConfigurationKey().equals(searchString + "_FTP_PASSWORD")) {
					ftpData.setPassword(configuration.getEdiConfigurationValue());
				} else if (configuration.getEdiConfigurationKey().equals(searchString + "_FTP_LOCAL_INPUT_DIRECTORY")) {
					ftpData.setLocalFileInputLocation(configuration.getEdiConfigurationValue());
				} else if (configuration.getEdiConfigurationKey().equals(searchString + "_FTP_REMOTE_INPUT_DIRECTORY")) {
					ftpData.setRemoteFileInputLocation(configuration.getEdiConfigurationValue());
				} else if (configuration.getEdiConfigurationKey().equals(searchString + "_FTP_LOCAL_OUTPUT_DIRECTORY")) {
					ftpData.setLocalFileOutputLocation(configuration.getEdiConfigurationValue());
				} else if (configuration.getEdiConfigurationKey().equals(searchString + "_FTP_REMOTE_OUTPUT_DIRECTORY")) {
					ftpData.setRemoteFileOutputLocation(configuration.getEdiConfigurationValue());
				} else if (configuration.getEdiConfigurationKey().equals(searchString + "_FTP_TO_EMAIL")) {
					ftpData.setToEmail(configuration.getEdiConfigurationValue());
				} else if (configuration.getEdiConfigurationKey().equals(searchString + "_FTP_ENABLE_EMAIL")) {
					ftpData.setEnableEmail(configuration.getEdiConfigurationValue());
				}
			}
		}
		
		
		/*log.info("FTP DATA : " + ftpData);*/
		return ftpData;
	}
}
