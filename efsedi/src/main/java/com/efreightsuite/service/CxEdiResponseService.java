package com.efreightsuite.service;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.efreightsuite.channel.stdin.AbstractResponseService;
import com.efreightsuite.communication.FTPHelper;
import com.efreightsuite.communication.FtpDto;
import com.efreightsuite.enumeration.EdiMessagingFor;
import com.efreightsuite.enumeration.ImportExport;
import com.efreightsuite.model.Consol;
import com.efreightsuite.model.EdiResponseStatus;

import lombok.extern.log4j.Log4j2;

@Service
@Log4j2
public class CxEdiResponseService extends AbstractResponseService {

	public EdiMessagingFor getMessageFor() {
		return EdiMessagingFor.CX;
	}

	public void processor() {
		log.info("CX Process Started...........");

		try {
		FtpDto ftpDto = ftpConfigurationService.getFtpData(EdiMessagingFor.CX);
		
		log.info("FTP DTO : " + ftpDto);

		if (ftpDto != null && ftpDto.getServer() != null && ftpDto.getPort() != null && ftpDto.getUserName() != null
				&& ftpDto.getPassword() != null && ftpDto.getLocalFileInputLocation() != null
				&& ftpDto.getLocalFileOutputLocation() != null && ftpDto.getRemoteFileInputLocation() != null
				&& ftpDto.getRemoteFileOutputLocation() != null) {
			FTPHelper ftpHelper = new FTPHelper(ftpDto);

			ArrayList<String> fileNames = ftpHelper.getFileNames(ftpDto.getRemoteFileOutputLocation());

			if (fileNames != null && !fileNames.isEmpty()) {
				for (String fileName : fileNames) {
					
					try {
						log.info("Processing file " + fileName);
						
						ftpHelper.downloadFile(ftpDto.getRemoteFileOutputLocation() + fileName, ftpDto.getLocalFileOutputLocation() + fileName);
						System.out.println(fileName + " being downloaded...");
						ftpHelper.deleteFile(ftpDto.getRemoteFileOutputLocation() + fileName);
						log.info("Remote File " + ftpDto.getRemoteFileOutputLocation() + fileName + " being deleted");
						
						
						EdiResponseStatus ediResponseStatus = translateFSU.getObject(ftpDto.getLocalFileOutputLocation(), fileName);
					
						Consol consol = consolRepository.getConsolByMawbAndService(ediResponseStatus.getMasterNumber(), ImportExport.Export);
						log.info("Consol " + consol.getId()); 
						ediResponseStatus.setConsol(consol);
						
						ediResponseStatus = ediResponseStatusRepository.save(ediResponseStatus);
						log.info("");
					}catch(Exception e) {
						log.error("Exception Occured while reading the file : " + e);
					}
				}
			}

			ftpHelper.disconnect();
		} else {
			log.error("FTP DATA is not configured............");
		}
		}catch(Exception exception) {
			log.error("Error while reading the CX EDI ", exception);
		}
	}



}
