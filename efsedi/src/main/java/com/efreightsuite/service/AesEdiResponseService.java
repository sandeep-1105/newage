package com.efreightsuite.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.efreightsuite.channel.stdin.AbstractMessagingService;
import com.efreightsuite.dto.MessagingDto;
import com.efreightsuite.enumeration.EdiMessagingFor;
import com.efreightsuite.ibmmq.IbmMQProperties;
import com.efreightsuite.ibmmq.IbmMQServiceUtil;
import com.efreightsuite.model.AesFile;
import com.efreightsuite.model.AesFileEdiStatus;
import com.efreightsuite.model.AesFileStatusHistory;
import com.efreightsuite.model.AmsDispositionMaster;
import com.efreightsuite.repository.AesFileStatusHistoryRepository;
import com.efreightsuite.repository.AmsDispositionMasterRepository;
import com.efreightsuite.util.AesUtil;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class AesEdiResponseService extends AbstractMessagingService {

	@Autowired
	protected AmsDispositionMasterRepository amsDispositionMasterRepository;

	@Autowired
	protected AesFileStatusHistoryRepository aesFileStatusHistoryRepository;
	
	public EdiMessagingFor getMessageFor() {
		return EdiMessagingFor.AES_RESPONSE;
	}

	public void process(MessagingDto request) {
		mqServeReadOperation(new IbmMQProperties());
	}

	public void mqServeReadOperation(IbmMQProperties properties) {

		IbmMQServiceUtil service = new IbmMQServiceUtil();
		if (service.outBoundInit(properties, true)) {
			log.info("Queue Read Initialized.. ");
			if (service.openConnection()) {
				String val = null;

				while ((val = service.readAndConvert()) != null) {
					log.info("INBOUNDS ---> " + val);

					AesFileEdiStatus aesFileEdiStatus = new AesFileEdiStatus();
					aesFileEdiStatus.setAesFile(null);
					aesFileEdiStatus.setMessagingStatus("Message From MQ ");
					aesFileEdiStatus.setMessagingError(null);
					aesFileEdiStatus.setCreateDate(new Date());
					aesFileEdiStatus.setMqActionType("INBOUND");
					aesFileEdiStatus.setMqMessage(val);
					aesFileEdiStatus = aesFileEdiStatusRepository.save(aesFileEdiStatus);
					parseAndProcessResponse(aesFileEdiStatus);
				}

			} else {
				log.error("Read Queue cannot able to Open Connection");
			}
		} else {
			log.error("Read Queue cannot able to initialize");
		}
		service.closeSession();
	}

	public void parseAndProcessResponse(AesFileEdiStatus aesFileEdiStatus) {
		String arr[] = aesFileEdiStatus.getMqMessage().split("\\n");
		String lv_ace_m1 = null;
		String lv_message_type = null;
		Date lv_acr_date = null;
		Date lv_eta = null;
		String lv_acr_time = null;
		String lv_batch_no = null;
		String lv_aes_batch_no = null;

		String lv_carrier_code = null;
		String lv_transport_mode = null;
		String lv_vessel_country_code = null;
		String lv_vessel_country = null;
		String lv_vessel_name = null;
		String lv_voyage_no = null;
		String lv_manifest_no = null;
		String lv_manifest_type = null;
		String lv_isf_no = null;
		String lv_cbp_port = null;
		String lv_user_id = null;
		String lv_im = null;// = 'Y';
		String lv_type = null;
		String lv_house_no_im = null;// = substr(lv_read_line,5,14);
		String lv_entry_no = null;// = substr(lv_read_line,5,14);
		String lv_cbp_port_im = null;// = substr(lv_read_line,25,4);
		String lv_manifest_no_im = null;
		String lv_error_message = null;
		String lv_reference_type = null;
		String lv_reference_no = null;

		String lv_isf_house = null;
		String lv_isf_error_code = null;
		String lv_isf_error_desc = null;
		String lv_disposition = null;
		String lv_disposition_name = null;
		String lv_response_date = null;
		String lv_location_acr_date = null;
		String lv_location_acr_time = null;
		String lv_location_date = null;
		String lv_action_time = null;
		String lv_action_time_im = null;
		String lv_manifest_qty = null;
		String lv_issuer_code = null;
		String lv_job_reference = null;
		String lv_master_no = null;
		String lv_house_no = null;
		String lv_entry_type = null;
		Date lv_action_date = null;
		String lv_firms_code = null;
		String lv_transaction_port = null;
		String lv_us_destination_port = null;
		String lv_foreign_destination_port = null;
		String lv_container = null;
		String lv_r05_container = null;
		String lv_seal_no1 = null;
		String lv_seal_no2 = null;
		String lv_remarks = null;
		String lv_message_clob = null;
		String lv_carrier_code_im = null;
		Date lv_action_date_im = null;
		String lv_total_manifest = null;
		String lv_total_ports = null;
		String lv_total_bills = null;
		String lv_total_house_bills = null;
		String lv_total_ammendments = null;
		String lv_total_h01 = null;
		String lv_total_bills_rejected = null;
		String lv_total_bills_accepted = null;
		String lv_total_records = null;
		String lv_subjob_uid = null;
		String lv_subjob_no = null;
		String lv_isf_master = null;
		String lv_isf_reference = null;
		String lv_response_code = null;
		String lv_aes_disposition = null;
		String lv_aes_response = null;
		String lv_aes_itn = null;
		String lv_severity_indicator = null;
		String lv_aes_status = null;
		int k = 0;
		List<String> _I_F_List = new ArrayList<>();
		_I_F_List.add("I");
		_I_F_List.add("F");

		// Processing Line No - 1
		String lv_read_line = arr[0];

		if (AesUtil.substr(lv_read_line, 1, 3).equals("ACR")) {
			lv_ace_m1 = AesUtil.substr(lv_read_line, 4, 4);
			lv_message_type = AesUtil.substr(lv_read_line, 14, 2);
			lv_batch_no = AesUtil.substr(lv_read_line,28,5);

			if (!_I_F_List.contains(AesUtil.substr(lv_message_type, 2, 1).trim())) {
				if (AesUtil.substr(lv_read_line,16,6).trim().length() != 0) {
					lv_acr_date = AesUtil.convertStringToSpDateFormat(lv_read_line.substring(15, 21), "yyMMdd");// to_date(substr(lv_read_line,16,6),'YYMMDD');
					lv_acr_time = lv_read_line.substring(21, 27);// substr(lv_read_line,22,6);
				}
			}
		} else if (AesUtil.substr(lv_read_line,1,1).equals("A") && AesUtil.substr(lv_read_line,2,2).equals(" ")) {
			lv_message_type = AesUtil.substr(lv_read_line,22,2);
			lv_aes_batch_no = AesUtil.substr(lv_read_line,32,6);
		}
		AesFile aesFile = aesFileRepository.findByBatchNo(lv_aes_batch_no); 
		// Processing Line No - 2
		lv_read_line = arr[1];

		if (AesUtil.substr(lv_read_line,1,3).equals("M01")) {
			lv_carrier_code = AesUtil.substr(lv_read_line,4,4);
			lv_transport_mode =  AesUtil.substr(lv_read_line,8,2);
			lv_vessel_country_code =  AesUtil.substr(lv_read_line,10,2);
			lv_vessel_country = AesUtil.substr(lv_read_line,10,2); //TODO pk_country_master.get_name();
			lv_vessel_name = AesUtil.substr(lv_read_line,12,23);
			lv_voyage_no = AesUtil.substr(lv_read_line,35,5);
			lv_manifest_no = AesUtil.substr(lv_read_line,45,6);
			lv_manifest_type = AesUtil.substr(lv_read_line,60,1);
		}

		if (AesUtil.substr(lv_read_line,1,4).equals("SF10")) {
			lv_isf_no = AesUtil.substr(lv_read_line,39,15);
		}

		// Processing Line No - 3
		lv_read_line = arr[2];

		if (AesUtil.substr(lv_read_line,1,3).equals("R01")) {
			lv_cbp_port =AesUtil.substr(lv_read_line,8,4);
			if (AesUtil.substr(lv_read_line,46,6).trim().length() != 0) {
				lv_eta = AesUtil.convertStringToSpDateFormat(AesUtil.substr(lv_read_line,46,6), "yyMMdd");
			}
		}
		if (AesUtil.substr(lv_read_line,1,3).equals("M02")) {
			lv_job_reference = AesUtil.substr(lv_read_line,4,30);
		}

		if (AesUtil.substr(lv_read_line,-1,-1).equals("H01")) {
			lv_im = "Y";
			lv_entry_no = lv_house_no_im = AesUtil.substr(lv_read_line,5,14);
			lv_cbp_port_im = AesUtil.substr(lv_read_line,25,4);
		}
		if (AesUtil.substr(lv_read_line,-1,-1).equals("T01")) {
			lv_im = "Y";
			lv_house_no_im = AesUtil.substr(lv_read_line,4,12);
		}

		if (AesUtil.substr(lv_read_line,1,3).equals("W01")) {
			lv_im = "Y";
			lv_house_no_im = AesUtil.substr(lv_read_line,4,14);
			lv_cbp_port_im = AesUtil.substr(lv_read_line,30,4);
			lv_manifest_no_im = AesUtil.substr(lv_read_line,34,6);
			if (lv_error_message == null) {
				lv_error_message = lv_house_no_im + " - " + AesUtil.substr(lv_read_line,40,40);
			} else {
				lv_error_message = lv_house_no_im + " - " + AesUtil.substr(lv_read_line,40,40);
			}
		}

		if (AesUtil.substr(lv_read_line,-1,-1).equals("I01")) {
			lv_reference_type = "IB";
			lv_reference_no = AesUtil.stringNVL(AesUtil.substr(lv_read_line,50,11), lv_read_line, 8,9);
		}
		if (AesUtil.substr(lv_read_line,-1,-1).equals("H02")) {
			lv_reference_type = "IB";
			lv_reference_no = AesUtil.substr(lv_read_line,6,16);
		}
		if (AesUtil.substr(lv_read_line,-1,-1).equals("SF15")) {
			lv_isf_house = AesUtil.substr(lv_read_line,11,15);
		}
		if (AesUtil.substr(lv_read_line,-1,-1).equals("SF90")) {
			lv_isf_error_code = AesUtil.substr(lv_read_line,5,2);
			if (lv_isf_error_desc == null) {
				lv_isf_error_desc = lv_isf_error_code + " - " + AesUtil.substr(lv_read_line,10,70).trim();
			} else {
				lv_isf_error_desc = AesUtil.asciiTOString(10) + lv_isf_error_code + " - "
						+ AesUtil.substr(lv_read_line,10,70).trim();
			}
		}
		if (lv_read_line.substring(0, 3).equals("ES1")) {
			lv_response_code = AesUtil.substr(lv_read_line,4,3);
			lv_aes_disposition = AesUtil.substr(lv_read_line,8,1);
			lv_severity_indicator = AesUtil.substr(lv_read_line,9,1);
			lv_aes_response = AesUtil.substr(lv_read_line,4,3) + " - " + AesUtil.substr(lv_read_line,11,40);
			lv_aes_itn = AesUtil.substr(lv_read_line,51,15);
			if (lv_response_code != null) {
//	           TODO String array   lv_resp(k)            := lv_response_code;
				k = k + 1;
			}
		}

		// Processing Line No - 4
		lv_read_line = arr[3];
		
		if (AesUtil.substr(lv_read_line,1,3).equals("J01")) {
			lv_issuer_code = AesUtil.substr(lv_read_line,4,4);
		}
		if (AesUtil.substr(lv_read_line,1,3).equals("M02")) {
			lv_job_reference = AesUtil.substr(lv_read_line,4,30);
		}
		if (AesUtil.substr(lv_read_line,1,3).equals("H01")) {
			lv_im = "Y";
			lv_entry_no = lv_house_no_im = AesUtil.substr(lv_read_line,5,14);
			lv_cbp_port_im = AesUtil.substr(lv_read_line,25,4);
		}
		if (AesUtil.substr(lv_read_line,1,3).equals("T01")) {
			lv_im = "Y";
			lv_house_no_im = AesUtil.substr(lv_read_line,4,12);
		}
		if (AesUtil.substr(lv_read_line,1,3).equals("W01")) {
			lv_im = "Y";
			lv_house_no_im = AesUtil.substr(lv_read_line,4,14);
			lv_cbp_port_im = AesUtil.substr(lv_read_line,30,4);
			lv_manifest_no_im = AesUtil.substr(lv_read_line,34,6);
			if (lv_error_message == null) {
				lv_error_message = lv_house_no_im + " - " + AesUtil.substr(lv_read_line,40,40);// (lv_read_line,40,40);
			} else {
				lv_error_message = lv_error_message + AesUtil.asciiTOString(10) + lv_house_no_im + " - "
						+ AesUtil.substr(lv_read_line,40,40);
			}
		}

		if (AesUtil.substr(lv_read_line, 1, 3).equals("W02")) {
			lv_im = "Y";
			lv_carrier_code_im = AesUtil.substr(lv_read_line, 4, 4);
			if (AesUtil.substr(lv_read_line, 8, 6).trim().length() != 0) {
				lv_action_date_im = AesUtil.convertStringToSpDateFormat(AesUtil.substr(lv_read_line, 8, 6), "yyMMdd");
			}
			lv_action_time_im = AesUtil.substr(lv_read_line, 14, 6);
			lv_total_manifest = AesUtil.substr(lv_read_line, 20, 2);
			lv_total_ports = AesUtil.substr(lv_read_line, 22, 3);
			lv_total_bills = AesUtil.substr(lv_read_line, 25, 5);
			lv_total_house_bills = AesUtil.substr(lv_read_line, 30, 5);
			lv_total_ammendments = AesUtil.substr(lv_read_line, 35, 5);
			lv_total_h01 = AesUtil.substr(lv_read_line, 40, 5);
			lv_total_bills_rejected = AesUtil.substr(lv_read_line, 45, 5);
			lv_total_bills_accepted = AesUtil.substr(lv_read_line, 50, 5);
			lv_total_records = AesUtil.substr(lv_read_line, 55, 5);
		}

		if (AesUtil.substr(lv_read_line, 1, 3).equals("I01")) {
			lv_reference_type = "IB";
			lv_reference_no = AesUtil.stringNVL(AesUtil.substr(lv_read_line, 50, 11), lv_read_line, 8, 9);
		}

		if (AesUtil.substr(lv_read_line, 1, 3).equals("H02")) {
			lv_reference_type = "IB";
			lv_reference_no = AesUtil.substr(lv_read_line, 6, 16);
		}
		if (AesUtil.substr(lv_read_line, 1, 4).equals("SF20")) {
			if (AesUtil.substr(lv_read_line, 5, 2).equals("MB")) {
				lv_isf_master = AesUtil.substr(lv_read_line, 12, 15).trim();
			} else if (AesUtil.substr(lv_read_line, 5, 2).equals("CR")) {
				lv_isf_reference = AesUtil.substr(lv_read_line, 13, 20);
			}
		}

		if (AesUtil.substr(lv_read_line, 1, 4).equals("SF90")) {
			lv_isf_error_code = AesUtil.substr(lv_read_line, 5, 2);
			if (lv_isf_error_desc == null) {
				lv_isf_error_desc = lv_isf_error_code + " - " + AesUtil.substr(lv_read_line, 10, 70).trim();
			} else {
				lv_isf_error_desc = lv_isf_error_desc + AesUtil.asciiTOString(10) + lv_isf_error_code + " - "
						+ AesUtil.substr(lv_read_line, 10, 70);
			}
		}

		if (AesUtil.substr(lv_read_line, 1, 3).equals("ES1")) {
			lv_response_code = AesUtil.substr(lv_read_line, 4, 3);
			lv_aes_disposition = AesUtil.substr(lv_read_line, 8, 1);
			lv_severity_indicator = AesUtil.substr(lv_read_line, 9, 1);

			if (lv_aes_response == null) {
				lv_aes_response = AesUtil.substr(lv_read_line, 4, 3) + " - " + AesUtil.substr(lv_read_line, 11, 40);
			} else {
				lv_aes_response = lv_aes_response + AesUtil.asciiTOString(10) + AesUtil.substr(lv_read_line, 4, 3)
						+ " - " + AesUtil.substr(lv_read_line, 11, 40);
			}
			lv_aes_itn = AesUtil.substr(lv_read_line, 51, 15);
			if (lv_response_code != null) {
				/*
				 * TODO if lv_response_code is not null then lv_resp(k) :=
				 * lv_response_code; k := k + 1; end if;
				 */
			}
		}

		// Processing Line No - 5
		lv_read_line = arr[4];

		if (AesUtil.substr(lv_read_line, 1, 3).equals("J01")) {
			lv_issuer_code = AesUtil.substr(lv_read_line, 4, 4);
		}
		if (AesUtil.substr(lv_read_line, 1, 3).equals("M02")) {
			lv_job_reference = AesUtil.substr(lv_read_line, 4, 30);
		}
		if (AesUtil.substr(lv_read_line, 1, 3).equals("R02")) {
			lv_house_no = AesUtil.substr(lv_read_line, 4, 12);
			lv_disposition = AesUtil.substr(lv_read_line, 16, 2);
			lv_manifest_qty = AesUtil.substr(lv_read_line, 18, 10);
			lv_entry_type = AesUtil.substr(lv_read_line, 28, 2);
			lv_entry_no = AesUtil.substr(lv_read_line, 30, 15);
			if (AesUtil.substr(lv_read_line, 45, 6).trim().length() != 0) {
				lv_action_date = AesUtil.convertStringToSpDateFormat(AesUtil.substr(lv_read_line, 45, 6), "yyMMdd");
			}
			lv_action_time = AesUtil.substr(lv_read_line, 51, 4);
		}

		if (AesUtil.substr(lv_read_line, 1, 3).equals("H01")) {
			lv_im = "Y";
			lv_house_no_im = AesUtil.substr(lv_read_line, 5, 14);
			lv_entry_no = AesUtil.substr(lv_read_line, 5, 14);
			lv_cbp_port_im = AesUtil.substr(lv_read_line, 25, 4);
		}

		if (AesUtil.substr(lv_read_line, 1, 3).equals("T01")) {
			lv_im = "Y";
			lv_house_no_im = AesUtil.substr(lv_read_line, 4, 12);
		}

		if (AesUtil.substr(lv_read_line, 1, 3).equals("W01")) {
			lv_im = "Y";
			lv_house_no_im = AesUtil.substr(lv_read_line, 4, 14);
			lv_cbp_port_im = AesUtil.substr(lv_read_line, 30, 4);
			lv_manifest_no_im = AesUtil.substr(lv_read_line, 34, 6);
			if (lv_error_message == null) {
				lv_error_message = lv_house_no_im + " - " + AesUtil.substr(lv_read_line, 40, 40);
			} else {
				lv_error_message = lv_error_message + AesUtil.asciiTOString(10) + lv_house_no_im + " - "
						+ AesUtil.substr(lv_read_line, 40, 40);
			}
		}

		if (AesUtil.substr(lv_read_line, 1, 3).equals("W02")) {
			lv_im = "Y";
			lv_carrier_code_im = AesUtil.substr(lv_read_line, 4, 4);
			if (AesUtil.substr(lv_read_line, 8, 6).trim().length() > 0) {
				lv_action_date_im = AesUtil.convertStringToSpDateFormat(AesUtil.substr(lv_read_line, 8, 6), "yyMMdd");
			}

			lv_action_time_im = AesUtil.substr(lv_read_line, 14, 6);
			lv_total_manifest = AesUtil.substr(lv_read_line, 20, 2);
			lv_total_ports = AesUtil.substr(lv_read_line, 22, 3);
			lv_total_bills = AesUtil.substr(lv_read_line, 25, 5);
			lv_total_house_bills = AesUtil.substr(lv_read_line, 30, 5);
			lv_total_ammendments = AesUtil.substr(lv_read_line, 35, 5);
			lv_total_h01 = AesUtil.substr(lv_read_line, 40, 5);
			lv_total_bills_rejected = AesUtil.substr(lv_read_line, 45, 5);
			lv_total_bills_accepted = AesUtil.substr(lv_read_line, 50, 5);
			lv_total_records = AesUtil.substr(lv_read_line, 55, 5);
		}

		if (AesUtil.substr(lv_read_line, 1, 3).equals("I01")) {
			lv_reference_type = "IB";
			lv_reference_no = AesUtil.stringNVL(AesUtil.substr(lv_read_line, 50, 11), lv_read_line, 8, 9);
		}

		if (AesUtil.substr(lv_read_line, 1, 3).equals("H02")) {
			lv_reference_type = "IB";
			lv_reference_no = AesUtil.substr(lv_read_line, 6, 16);
		}

		if (AesUtil.substr(lv_read_line, 1, 4).equals("SF20")) {
			if (AesUtil.substr(lv_read_line, 5, 2).equals("MB")) {
				lv_isf_master = AesUtil.substr(lv_read_line, 12, 15).trim();
			} else if (AesUtil.substr(lv_read_line, 5, 2).equals("CR")) {
				lv_isf_reference = AesUtil.substr(lv_read_line, 13, 20).trim();
			}
		}

		if (AesUtil.substr(lv_read_line, 1, 4).equals("SF90")) {
			lv_isf_error_code = AesUtil.substr(lv_read_line, 5, 2);
			if (lv_isf_error_desc == null) {
				lv_isf_error_desc = lv_isf_error_code + " - " + AesUtil.substr(lv_read_line, 10, 70).trim();
			} else {
				lv_isf_error_desc = lv_isf_error_desc + AesUtil.asciiTOString(10) + lv_isf_error_code + " - "
						+ AesUtil.substr(lv_read_line, 10, 70).trim();
			}
		}

		if (AesUtil.substr(lv_read_line, 1, 3).equals("ES1")) {
			lv_response_code = AesUtil.substr(lv_read_line, 4, 3);
			lv_aes_disposition = AesUtil.substr(lv_read_line, 8, 1);
			lv_severity_indicator = AesUtil.substr(lv_read_line, 9, 1);
			if (lv_aes_response == null) {
				lv_aes_response = AesUtil.substr(lv_read_line, 4, 3) + " - " + AesUtil.substr(lv_read_line, 11, 40);
			} else {
				lv_aes_response = lv_aes_response + AesUtil.asciiTOString(10) + AesUtil.substr(lv_read_line, 4, 3)
						+ " - " + AesUtil.substr(lv_read_line, 11, 40);
			}
			lv_aes_itn = AesUtil.substr(lv_read_line, 51, 15);
			if (lv_response_code != null) {
				/*
				 * TODO lv_resp(k) = lv_response_code; k = k + 1;
				 */
			}
		}

		// Processing Line No - 6
		lv_read_line = arr[5];

		if (AesUtil.substr(lv_read_line, 1, 3).equals("M02")) {
			lv_job_reference = AesUtil.substr(lv_read_line, 4, 30);
		}

		if (AesUtil.substr(lv_read_line, 1, 3).equals("R02")) {
			lv_transaction_port = AesUtil.substr(lv_read_line, 4, 4);
			lv_firms_code = AesUtil.substr(lv_read_line, 8, 4);
			lv_us_destination_port = AesUtil.substr(lv_read_line, 12, 4);
			lv_foreign_destination_port = AesUtil.substr(lv_read_line, 16, 5);
			lv_container = AesUtil.substr(lv_read_line, 24, 14);
			lv_reference_type = AesUtil.substr(lv_read_line, 38, 3);
			lv_reference_no = AesUtil.substr(lv_read_line, 41, 30);
		}

		if (AesUtil.substr(lv_read_line, 1, 3).equals("H01")) {
			lv_im = "Y";
			lv_house_no_im = AesUtil.substr(lv_read_line, 5, 14);
			lv_entry_no = AesUtil.substr(lv_read_line, 5, 14);
			lv_cbp_port_im = AesUtil.substr(lv_read_line, 25, 4);
		}

		if (AesUtil.substr(lv_read_line, 1, 3).equals("T01")) {
			lv_im = "Y";
			lv_house_no_im = AesUtil.substr(lv_read_line, 4, 12);
		}

		if (AesUtil.substr(lv_read_line, 1, 3).equals("W01")) {
			lv_im = "Y";
			lv_house_no_im = AesUtil.substr(lv_read_line, 4, 14);
			lv_cbp_port_im = AesUtil.substr(lv_read_line, 30, 4);
			lv_manifest_no_im = AesUtil.substr(lv_read_line, 34, 6);
			if (lv_error_message == null) {
				lv_error_message = lv_house_no_im + " - " + AesUtil.substr(lv_read_line, 40, 40);
			} else {
				lv_error_message = lv_error_message + AesUtil.asciiTOString(10) + lv_house_no_im + " - "
						+ AesUtil.substr(lv_read_line, 40, 40);
			}
		}

		if (AesUtil.substr(lv_read_line, 1, 3).equals("W02")) {
			lv_im = "Y";
			lv_carrier_code_im = AesUtil.substr(lv_read_line, 4, 4);
			if (AesUtil.substr(lv_read_line, 8, 6).trim().length() > 0) {
				lv_action_date_im = AesUtil.convertStringToSpDateFormat(AesUtil.substr(lv_read_line, 8, 6), "yyMMdd");
			}
			lv_action_time_im = AesUtil.substr(lv_read_line, 14, 6);
			lv_total_manifest = AesUtil.substr(lv_read_line, 20, 2);
			lv_total_ports = AesUtil.substr(lv_read_line, 22, 3);
			lv_total_bills = AesUtil.substr(lv_read_line, 25, 5);
			lv_total_house_bills = AesUtil.substr(lv_read_line, 30, 5);
			lv_total_ammendments = AesUtil.substr(lv_read_line, 35, 5);
			lv_total_h01 = AesUtil.substr(lv_read_line, 40, 5);
			lv_total_bills_rejected = AesUtil.substr(lv_read_line, 45, 5);
			lv_total_bills_accepted = AesUtil.substr(lv_read_line, 50, 5);
			lv_total_records = AesUtil.substr(lv_read_line, 55, 5);
		}

		if (AesUtil.substr(lv_read_line, 1, 3).equals("I01")) {
			lv_reference_type = "IB";
			lv_reference_no = AesUtil.stringNVL(AesUtil.substr(lv_read_line, 50, 11), lv_read_line, 8, 9);
		}

		if (AesUtil.substr(lv_read_line, 1, 3).equals("H02")) {
			lv_reference_type = "IB";
			lv_reference_no = AesUtil.substr(lv_read_line, 6, 16);
		}
		if (AesUtil.substr(lv_read_line, 1, 3).equals("R03")) {
			lv_remarks = AesUtil.substr(lv_read_line, 4, 53);
		}

		if (AesUtil.substr(lv_read_line, 1, 4).equals("SF20")) {
			if (AesUtil.substr(lv_read_line, 5, 2).equals("MB")) {
				lv_isf_master = AesUtil.substr(lv_read_line, 12, 15).trim();
			} else if (AesUtil.substr(lv_read_line, 5, 2).equals("CR")) {
				lv_isf_reference = AesUtil.substr(lv_read_line, 13, 20).trim();
			}
		}

		if (AesUtil.substr(lv_read_line, 1, 4).equals("SF90")) {
			lv_isf_error_code = AesUtil.substr(lv_read_line, 5, 2);
			if (lv_isf_error_desc == null) {
				lv_isf_error_desc = lv_isf_error_code + " - " + AesUtil.substr(lv_read_line, 10, 70).trim();
			} else {
				lv_isf_error_desc = lv_isf_error_desc + AesUtil.asciiTOString(10) + lv_isf_error_code + " - "
						+ AesUtil.substr(lv_read_line, 10, 70).trim();
			}
		}

		if (AesUtil.substr(lv_read_line, 1, 3).equals("ES1")) {
			lv_response_code = AesUtil.substr(lv_read_line, 4, 3);
			lv_aes_disposition = AesUtil.substr(lv_read_line, 8, 1);
			lv_severity_indicator = AesUtil.substr(lv_read_line, 9, 1);
			if (lv_aes_response == null) {
				lv_aes_response = AesUtil.substr(lv_read_line, 4, 3) + " - " + AesUtil.substr(lv_read_line, 11, 40);
			} else {
				lv_aes_response = lv_aes_response + AesUtil.asciiTOString(10) + AesUtil.substr(lv_read_line, 4, 3)
						+ " - " + AesUtil.substr(lv_read_line, 11, 40);
			}
			lv_aes_itn = AesUtil.substr(lv_read_line, 51, 15);
			if (lv_response_code != null) {
				/*
				 * TODO lv_resp(k) = lv_response_code; k = k + 1;
				 */
			}
		}

		if (!AesUtil.substr(lv_message_type, 2, 1).equals("I")) {
			if (arr.length > 6) {
				for (int idx = 6; idx < arr.length; idx++) {
					lv_read_line = arr[idx];

	                if(AesUtil.substr(lv_read_line,1,3).equals("R03")) {
	                   if(lv_remarks == null) {
	                      lv_remarks            = AesUtil.substr(lv_read_line,4,53);
	                   } else {
	                      lv_remarks            = lv_remarks+" "+AesUtil.substr(lv_read_line,4,53);
	                   }
	                }

	                if(AesUtil.substr(lv_read_line,1,3).equals("H01")) {
	                   lv_im                    = "Y";
	                   lv_house_no_im           = AesUtil.substr(lv_read_line,5,14);
	                   lv_entry_no              = AesUtil.substr(lv_read_line,5,14);
	                   lv_cbp_port_im           = AesUtil.substr(lv_read_line,25,4);
	                }

	                if(AesUtil.substr(lv_read_line,1,3).equals("T01")) {
	                   lv_im                    = "Y";
	                   lv_house_no_im           = AesUtil.substr(lv_read_line,4,12);
	                }

	                if(AesUtil.substr(lv_read_line,1,3).equals("W01")) {
	                   lv_im                    = "Y";
	                   lv_house_no_im           = AesUtil.substr(lv_read_line,4,14);
	                   lv_cbp_port_im           = AesUtil.substr(lv_read_line,30,4);
	                   lv_manifest_no_im        = AesUtil.substr(lv_read_line,34,6);
	                   if(lv_error_message  == null) {
	                      lv_error_message      = lv_house_no_im+" - "+AesUtil.substr(lv_read_line,40,40);
	                   } else {
	                      lv_error_message      = lv_error_message+AesUtil.asciiTOString(10)+lv_house_no_im+" - "+AesUtil.substr(lv_read_line,40,40);
	                   }
	                }

	                if(AesUtil.substr(lv_read_line,1,3).equals("W02")) {
	                   lv_im                    = "Y";
	                   lv_carrier_code_im       = AesUtil.substr(lv_read_line,4,4);
	                   if (AesUtil.substr(lv_read_line, 8, 6).trim().length() > 0) {
	       					lv_action_date_im = AesUtil.convertStringToSpDateFormat(AesUtil.substr(lv_read_line, 8, 6), "yyMMdd");
	                   }
	                   lv_action_time_im        = AesUtil.substr(lv_read_line,14,6);
	                   lv_total_manifest        = AesUtil.substr(lv_read_line,20,2);
	                   lv_total_ports           = AesUtil.substr(lv_read_line,22,3);
	                   lv_total_bills           = AesUtil.substr(lv_read_line,25,5);
	                   lv_total_house_bills     = AesUtil.substr(lv_read_line,30,5);
	                   lv_total_ammendments     = AesUtil.substr(lv_read_line,35,5);
	                   lv_total_h01             = AesUtil.substr(lv_read_line,40,5);
	                   lv_total_bills_rejected  = AesUtil.substr(lv_read_line,45,5);
	                   lv_total_bills_accepted  = AesUtil.substr(lv_read_line,50,5);
	                   lv_total_records         = AesUtil.substr(lv_read_line,55,5);
	                }

	                if(AesUtil.substr(lv_read_line,1,4).equals("SF20")) {
	                    if(AesUtil.substr(lv_read_line,5,2).equals("MB")) {
	                        lv_isf_master       = AesUtil.substr(lv_read_line,12,15).trim();
	                    } else if(AesUtil.substr(lv_read_line,5,2).equals("CR")) {
	                        lv_isf_reference    = AesUtil.substr(lv_read_line,13,20).trim();
	                    }
	                }

	                if(AesUtil.substr(lv_read_line,1,4).equals("SF90")) {
	                   lv_isf_error_code        = AesUtil.substr(lv_read_line,5,2);
	                   if(lv_isf_error_desc == null) {
	                      lv_isf_error_desc     = lv_isf_error_code+" - "+AesUtil.substr(lv_read_line,10,70).trim();
	                   } else {
	                      lv_isf_error_desc     = lv_isf_error_desc+AesUtil.asciiTOString(10)+lv_isf_error_code+" - "+AesUtil.substr(lv_read_line,10,70).trim();
	                   }
	                }    
	        
	                if(AesUtil.substr(lv_read_line,1,3).equals("ES1")) {
	                   lv_response_code         = AesUtil.substr(lv_read_line,4,3);  
	                   lv_aes_disposition       = AesUtil.substr(lv_read_line,8,1); 
	                   lv_severity_indicator    = AesUtil.substr(lv_read_line,9,1);   
	                   if(lv_aes_response == null) {
	                      lv_aes_response       = AesUtil.substr(lv_read_line,4,3)+" - "+AesUtil.substr(lv_read_line,11,40);  
	                   } else {
	                      lv_aes_response       = lv_aes_response+AesUtil.asciiTOString(10)+AesUtil.substr(lv_read_line,4,3)+" - "+AesUtil.substr(lv_read_line,11,40) ;   
	                   }  
	                   lv_aes_itn               = AesUtil.substr(lv_read_line,51,15); 
	                   if(lv_response_code != null) {
//	                      lv_resp(k)            = lv_response_code;
	                      k                     = k + 1;
	                   }
	                } 

					if (AesUtil.substr(lv_read_line, 1, 3).equals("R05")) {
 

					}

				}
			}
		} else {
			if (arr.length > 6) {
				for (int idx = 6; idx < arr.length; idx++) {

	                if(AesUtil.substr(lv_read_line,1,3).equals("I01")) {
	                   lv_reference_type = "IB";
	                   lv_reference_no = AesUtil.stringNVL(AesUtil.substr(lv_read_line,50,11), lv_read_line,8,9);
	                }

	                if(AesUtil.substr(lv_read_line,1,3).equals("H02")) {
	                   lv_reference_type = "IB";
	                   lv_reference_no = AesUtil.substr(lv_read_line,6,16);
	                }
				}
			}

		}

		
		

        
        if(lv_job_reference == null) {
/*            open  cur_ams_job(trim(AesUtil.substr(lv_job_reference,3,14)));
            fetch cur_ams_job into rec_ams_job;
            close cur_ams_job;*/
        }

        if(lv_reference_type.equals("OB")) {
           lv_master_no = lv_reference_no.trim();
        }

        if(lv_acr_time != null) {
        	lv_acr_time         = AesUtil.substr(lv_acr_time,1,2)+":"+AesUtil.substr(lv_acr_time,3,2)+":"+ (AesUtil.substr(lv_acr_time,5,2).trim().length() ==0 ?"00":AesUtil.substr(lv_acr_time,5,2).trim());;
        	//nvl(AesUtil.substr(lv_acr_time,5,2),"00");
        } else {
//            lv_acr_time         = to_char(rec_ams.create_date,"HH24:MI:SS");
        }
        if(lv_action_time != null) {
            lv_action_time      = AesUtil.substr(lv_action_time,1,2)+":"+AesUtil.substr(lv_action_time,3,2)+":"+ (AesUtil.substr(lv_action_time,5,2).trim().length() ==0 ?"00":AesUtil.substr(lv_action_time,5,2).trim());
        }
        if(lv_action_time_im != null) {
            lv_action_time_im   = AesUtil.substr(lv_action_time_im,1,2)+":"+AesUtil.substr(lv_action_time_im,3,2)+":"+(AesUtil.substr(lv_action_time_im,5,2).trim().length() ==0 ?"00":AesUtil.substr(lv_action_time_im,5,2).trim());
        }
        
        
		if (lv_disposition == null) {
			AmsDispositionMaster amsDispositionMaster = amsDispositionMasterRepository.findByCode(lv_disposition);
			if (amsDispositionMaster != null) {
				lv_disposition_name = amsDispositionMaster.getName();
			}
		}
		/*
		 * TODO
		lv_acr_date = nvl(trim(lv_acr_date),trunc( rec_ams.create_date));
		lv_acr_time = nvl(trim(lv_acr_time),to_char( rec_ams.create_date,'HH24:MI:SS'));    
        
        lv_response_date =  lv_acr_date||' '||lv_acr_time;    
        
        lv_location_acr_date =  trunc( get_sysdate(fa_location_code));
        lv_location_acr_time =  to_char(get_sysdate(fa_location_code),'HH24:MI:SS') ;   
        lv_location_date =  lv_location_acr_date||' '||lv_location_acr_time;
            
        */
        
		List<String> _SF_SN_List = new ArrayList<>();
		_SF_SN_List.add("SF");
		_SF_SN_List.add("SN");
		List<String> _XP_XT_List = new ArrayList<>();
		_XP_XT_List.add("XP");
		_XP_XT_List.add("XT");
		if (_SF_SN_List.contains(lv_message_type)) {
			// insert into isf_status_history
		} else if (_XP_XT_List.contains(lv_message_type)) {

            if(lv_message_type.equals("XP")) {
               lv_user_id =  "Global User";
            } else {
               lv_user_id = "US Customs Response";
            }   
            
            if(lv_aes_disposition != null && lv_aes_disposition.trim().equals("A")) {
               if(lv_severity_indicator == null || lv_severity_indicator.trim().length() ==0) { 
                  lv_aes_status = "Unconditionally Accepted";
               } else if(lv_severity_indicator.trim().equals("V")) { 
                  lv_aes_status = "Accepted - Verification Requested"; 
               } else if(lv_severity_indicator.trim().equals("C")) { 
                  lv_aes_status = "Accepted - Compliance Alert";   
               } else if(lv_severity_indicator.trim().equals("I")) { 
                  lv_aes_status = "Accepted - Information all Notice";   
               } else if(lv_severity_indicator.trim().equals("M")) { 
                  lv_aes_status = "Accepted - Issue Manually Closed";    
               } else if(lv_severity_indicator.trim().equals("W")) { 
                  lv_aes_status = "Accepted with Warnings"; 
               }
            } else if(lv_aes_disposition != null && lv_aes_disposition.equals("R")) { 
               if(lv_severity_indicator.trim().equals("F")) { 
                  lv_aes_status = "Fatally Rejected"; 
               }  
            }
            
			/*
			 * FOR indx IN 1 .. lv_resp.COUNT rec_aes_response := null; open
			 * cur_aes_response(trim(lv_resp (indx))); fetch cur_aes_response
			 * into rec_aes_response; close cur_aes_response;
			 * 
			 * if rec_aes_response.response_code is not null then insert into
			 * aes_response_description
			 */

			// Then
			// insert into aes_status_history
		} else {
			if (lv_im == null || lv_im.equals("N")) {
				// insert into ams_rc_messages
				// then ->
				/*
				 * rec_subjob := null; open
				 * cur_subjob(nvl(trim(lv_house_no),rec_ams_job.bl_no)); fetch
				 * cur_subjob into rec_subjob; close cur_subjob;
				 * 
				 * begin
				 * 
				 * insert into ams_status_history
				 */
			} else {
				List<String> _MR_AR_List = new ArrayList<>();
				_MR_AR_List.add("MR");
				_MR_AR_List.add("AR");
				if (_MR_AR_List.contains(lv_message_type)) {
					lv_type = "BILLS";
				} else if (lv_message_type.equals("IR")) {
					lv_type = "INBONDS";
				} else if (lv_message_type.equals("HR")) {
					lv_type = "ARRIVALS";
				}

				// insert into ams_rc_messages (create_user ,

				/*
				 * rec_subjob := null; open cur_subjob(trim(lv_house_no_im));
				 * fetch cur_subjob into rec_subjob; close cur_subjob;
				 */
//				insert into ams_status_history  (create_user                ,
				AesFileStatusHistory aesFileStatusHistory = new AesFileStatusHistory();
				aesFileStatusHistory.setCreateDate(new Date());
//				aesFileStatusHistory.setCompany_code(company_code);
//				aesFileStatusHistory.setBranch_code(branch_code);
//				aesFileStatusHistory.setLocation_code(location_code);
				aesFileStatusHistory.setUser_id(lv_user_id);
//				aesFileStatusHistory.setSegment_code(segment_code);
//				aesFileStatusHistory.setJob_uid(job_uid);
//				aesFileStatusHistory.setSubjob_uid(subjob_uid);
//				aesFileStatusHistory.setJob_no(job_no);
//				aesFileStatusHistory.setSubjob_no(subjob_no);
				aesFileStatusHistory.setBatch_no(lv_aes_batch_no);
//				aesFileStatusHistory.setSl_no(lv_sl_no);
				aesFileStatusHistory.setMessage_type(lv_message_type);
				aesFileStatusHistory.setAcr_date(lv_acr_date);
//				aesFileStatusHistory.setAcr_date(lv_location_acr_date == null ? lv_acr_date : lv_location_acr_date);
//				aesFileStatusHistory.setAcr_time(lv_location_acr_time == null ? lv_acr_time: lv_location_acr_time);
				aesFileStatusHistory.setCustoms_response_date(lv_response_date);
//				aesFileStatusHistory.setHouse_no(house_no);
				aesFileStatusHistory.setResponse_code(lv_response_code);
				aesFileStatusHistory.setResponse_description(lv_aes_response);
				aesFileStatusHistory.setDisposition_code(lv_aes_disposition.trim());
				aesFileStatusHistory.setSeverity_level(lv_severity_indicator.trim());
				aesFileStatusHistory.setItn_no(lv_aes_itn.trim());
				aesFileStatusHistory.setStatus(lv_aes_status);
				aesFileStatusHistory.setRemarks(lv_aes_response.trim());
				aesFileStatusHistory.setMessage(aesFileEdiStatus.getMqMessage());
				aesFileStatusHistory.setAesFile(aesFile);
				aesFileStatusHistoryRepository.save(aesFileStatusHistory);
			}
		}

		/*
		  
		 */
	}

}
