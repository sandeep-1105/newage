package com.efreightsuite.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import com.efreightsuite.communication.FtpDto;
import com.efreightsuite.enumeration.PPCC;
import com.efreightsuite.enumeration.YesNo;
import com.efreightsuite.model.AirlineEdi;
import com.efreightsuite.model.LocationMaster;
import com.efreightsuite.model.UserProfile;
import com.efreightsuite.service.email.EdiMailService;
import com.efreightsuite.util.EdiWriter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class FWBService {
	
	
	EdiWriter ediWriter;
	
	AirlineEdi airlineEdi;
	
	@Autowired
	EdiMailService ediMailService;

	private void standardMessageIdentification() {
	
		/*
		 * 1.Standard Message Identification
		 * */

		// 1.1 Standard Message Identifier
		ediWriter.write("FWB");
		
		// 1.2 Separator : Slant
		ediWriter.addSeparator();
		
		// 1.3 Message Type Version Number
		ediWriter.write("16", 3);

		// 1.4 Separator : CRLF
		ediWriter.addLine();
	}
	
	
	private void awbConsignmentDetails() {
		
		/*
		 * 2. AWB Consignment Details
		 * */

		/* AWB Consignment Details */
		String mawbNumber = airlineEdi.getMawbNo();
		String airlineNumber = mawbNumber.substring(0, 3);
		String awbNumber = mawbNumber.substring(3);

		// 2.1 AWB Identification (M)
		// 2.1.1 Airline Prefix
		ediWriter.write(airlineNumber, 3);
		// 2.1.2 Separator : Hyphen
		ediWriter.write("-");
		// 2.1.3 AWB Serial Number
		ediWriter.write(awbNumber, 8);
		
		
		// 2.2 AWB Origin and Destination
		// 2.2.1 Airport/City Code (of Origin)
		ediWriter.write(airlineEdi.getOrigin().getPortCode(), 3);
		// 2.2.2 Airport/City Code (of Destination)
		ediWriter.write(airlineEdi.getDestination().getPortCode(), 3);

		
		// 2.3 Quantity Detail
		// 2.3.1 Separator : Slant
		ediWriter.addSeparator();
		// 2.3.2 Shipment Description Code (T)
		ediWriter.write("T");
		// 2.3.3 Number of Pieces
		ediWriter.write(airlineEdi.getNoOfPieces().toString(), 4);
		// 2.3.4 Weight Code
		ediWriter.write("K");
		//	2.3.5 Weight
		ediWriter.write(airlineEdi.getGrossWeight().toString(), 7);

		// 2.4 Volume Detail (O)
		// 2.4.1 Volume Code
		// 2.4.2 Volume Amount
		
		
		// 2.5 Density Group (O)
		// 2.5.1 Density Indicator (DG)
		// 2.5.2 Density Group
		
		// 2.6 Separator : CRLF (M)
		ediWriter.addLine();
		
	}
	
	
	
	private void flightBooking() {
		/*
		 * 3. Flight Bookings
		 * */
		
		
		// 3.1 Line Identifier (FLT)
		ediWriter.write("FLT");

		// 3.2 Flight Identification
		// 3.2.1 Separator : Slant
		ediWriter.addSeparator();

		// 3.2.2 Carrier Code
		ediWriter.write(airlineEdi.getCarrierMaster().getCarrierCode(), 2);
		// 3.2.3 Flight Number
		ediWriter.write(airlineEdi.getFlightNumber(), 5);
		// 3.2.4 Separator : Slant
		ediWriter.addSeparator();
		// 3.2.5 Day
		String date = String.valueOf(airlineEdi.getMasterDate().getDate());
		if(date.length() == 1) {
			date = "0" + date;
		}
		ediWriter.write(date, 2);
		// 3.3 Separator : CRLF
		ediWriter.addLine();
	}
	
	
	private void routing() {

		/*
		 * 4. Routing
		 * */
		
		
		// 4.1 Line Identifier (RTG)
		ediWriter.write("RTG");
		
		// 4.2 1st Destination/Carrier

		// 4.2.1 Separator : Slant
		ediWriter.addSeparator();
		// 4.2.2 Airport/City Code
		ediWriter.write(airlineEdi.getDestination().getPortCode(), 3);
		// 4.2.3 Carrier Code
		ediWriter.write(airlineEdi.getCarrierMaster().getCarrierCode(), 2);
		
		 
		// 4.3 Onward Destination/Carrier
		// 4.3.1 Separator : Slant
		// 4.3.2 Airport/City Code
		// 4.3.3 Carrier Code
		// 4.4 Separator : CRLF
		ediWriter.addLine();
	}
	
	
	
	private void shipperDetail() {
		/*
		 * 5. Shipper
		 * */
		
		// 5. Shipper
		// 5.1 Line Identifier (SHP)
		
		ediWriter.write("SHP");
		// 5.2 Account Detail (O)
		// 5.2.1 Separator : Slant
		// 5.2.2 Account Number
		
		// 5.3 Separator : CRLF
		ediWriter.addLine();
		
		// 5.4 Name
		// 5.4.1 Separator : Slant
		ediWriter.addSeparator();
		// 5.4.2 Name
		ediWriter.write(airlineEdi.getShipper().getPartyName(), 35);
		// 5.4.3 Separator : CRLF
		ediWriter.addLine();
		
		// 5.5 Street Address
		// 5.5.1 Separator : Slant
		ediWriter.addSeparator();
		// 5.5.2 Street Address
		ediWriter.write(airlineEdi.getShipperAddress().getAddressLine1() + airlineEdi.getShipperAddress().getAddressLine2(), 35);
		// 5.5.3 Separator : CRLF
		ediWriter.addLine();

		// 5.6 Location
		// 5.6.1 Separator : Slant
		ediWriter.addSeparator();
		// 5.6.2 Place
		ediWriter.write(airlineEdi.getShipperAddress().getCity().getCityName(), 17);
		// 5.6.3 Separator : Slant (C)
		// 5.6.4 State/Province (O)

		// 5.6.5 Separator : CRLF
		ediWriter.addLine();

		// 5.7 Coded Location

		// 5.7.1 Separator : Slant
		ediWriter.addSeparator();
		// 5.7.2 ISO Country Code
		ediWriter.write(airlineEdi.getShipperAddress().getCountry().getCountryCode(), 2);
		// 5.7.3 Separator : Slant (C)
		// 5.7.4 Post Code (O)

		// 5.8 Contact Detail (O)
		// 5.8.1 Separator : Slant
		// 5.8.2 Contact Identifier
		// 5.8.3 Separator : Slant
		// 5.8.4 Contact Number
		
		// 5.9 Separator : CRLF
		ediWriter.addLine();
	}
	
	
	private void consigneeDetail() {
		/*
		 * 6. Consignee
		 * */
		
		// 6. Consignee
		// 6.1 Line Identifier (CNE)
		
		ediWriter.write("CNE");
		// 6.2 Account Detail (O)
		// 6.2.1 Separator : Slant
		// 6.2.2 Account Number
		
		// 6.3 Separator : CRLF
		ediWriter.addLine();
		
		// 6.4 Name
		// 6.4.1 Separator : Slant
		ediWriter.addSeparator();
		// 6.4.2 Name
		ediWriter.write(airlineEdi.getConsignee().getPartyName(), 35);
		// 6.4.3 Separator : CRLF
		ediWriter.addLine();
		
		// 6.5 Street Address
		// 6.5.1 Separator : Slant
		ediWriter.addSeparator();
		// 6.5.2 Street Address
		ediWriter.write(airlineEdi.getConsigneeAddress().getAddressLine1() + airlineEdi.getConsigneeAddress().getAddressLine2(), 35);
		// 6.5.3 Separator : CRLF
		ediWriter.addLine();

		// 6.6 Location
		// 6.6.1 Separator : Slant
		ediWriter.addSeparator();
		// 6.6.2 Place
		ediWriter.write(airlineEdi.getConsigneeAddress().getCity().getCityName(), 17);
		// 6.6.3 Separator : Slant (C)
		// 6.6.4 State/Province (O)

		// 6.6.5 Separator : CRLF
		ediWriter.addLine();

		// 6.7 Coded Location

		// 6.7.1 Separator : Slant
		ediWriter.addSeparator();
		// 6.7.2 ISO Country Code
		ediWriter.write(airlineEdi.getConsignee().getCountryMaster().getCountryCode(), 2);
		// 6.7.3 Separator : Slant (C)
		// 6.7.4 Post Code (O)

		// 6.8 Contact Detail (O)
		// 6.8.1 Separator : Slant
		// 6.8.2 Contact Identifier
		// 6.8.3 Separator : Slant
		// 6.8.4 Contact Number
		
		// 6.9 Separator : CRLF
		ediWriter.addLine();
	}
	
	
	
	public void agentDetail() {

		/*
		 * 7. Agent Detail (C)
		 * */
		
		if (airlineEdi.getAgent() != null && airlineEdi.getAgentAddress() != null) {
			
			// 7.1 Line Identifier (AGT)	
			ediWriter.write("AGT");
			
			// 7.2 Account Detail
			// 7.2.1 Separator : Slant
			ediWriter.addSeparator();
			// 7.2.2 Account Number
			// 7.2.3 Separator : Slant
			ediWriter.addSeparator();			
			// 7.2.4 IATA Cargo Agent Numeric Code
			ediWriter.write(airlineEdi.getAgent().getPartyDetail().getIataCode(), 14);
			// 7.2.5 Separator : Slant
			// 7.2.6 IATA Cargo Agent CASS Address
			// 7.2.7 Separator : Slant
			// 7.2.8 Participant Identifier
			
			// 7.3 Separator : CRLF
			ediWriter.addLine();

			
			// 7.4 Name
			// 7.4.1 Separator : Slant
			ediWriter.addSeparator();
			// 7.4.2 Name
			ediWriter.write(airlineEdi.getAgent().getPartyName(), 35);
			// 7.4.3 Separator : CRLF
			ediWriter.addLine();
			// 7.5 Place
			// 7.5.1 Separator : Slant
			ediWriter.addSeparator();
			// 7.5.2 Place
			ediWriter.write(airlineEdi.getAgentAddress().getCity().getCityName(), 17);
			// 7.5.3 Separator : CRLF
			ediWriter.addLine();
		 
		}
	}
	
	
	
	private void specialServiceRequest() {
		/*
		 * 8. Special Service Request
		 * */
		
		// 8.1 Line Identifier (SSR)
		// 8.2 Separator : Slant
		// 8.3 Special Service Request
		// 8.4 Separator : CRLF
	}
	
	

	
	
	private void alsoNotify() {
		/*
		 * 9. Also Notify
		 * */
	}
	
	

	
	
	private void accountingInformation() {
		/*
		 * 10. Accounting Information
		 * */
	}
	
	
	
	
	
	private void chargeDeclarations(LocationMaster selectedLocationMaster) {

		/*
		 * 11. Charge Declarations
		 * */
		
		
		// 11. Charge Declarations
		
		// 11.1 Line Identifier (CVD)
		ediWriter.write("CVD", 3);

		// 11.2 Separator : Slant
		ediWriter.addSeparator();
		

		// 11.3 ISO Currency Code
		ediWriter.write(selectedLocationMaster.getCountryMaster().getCurrencyMaster().getCurrencyCode(), 3);
		// 11.4 Separator : Slant
		ediWriter.addSeparator();
		
		// 11.5 Charge Code (C) // As discussed
		ediWriter.write("PP", 2);	

		// 11.6 Separator : Slant
		ediWriter.addSeparator();
		
		// TODO
		// 11.7 Prepaid/Collect Charge Declarations (C)
		
		// 11.7.1 P/C Ind. (Weight/Valuation)
		if(airlineEdi.getPpcc().equals(PPCC.Prepaid)) {
			ediWriter.write("PP", 2);	
		} else {
			ediWriter.write("CC", 2);
		}
		
		
		// 11.7.2 P/C Ind. (Other Charges)
		
		// 11.8 Value for Carriage Declaration
		// 11.8.1 Separator : Slant
		ediWriter.addSeparator();
		// 11.8.2 Declared Value for Carriage
		ediWriter.write(airlineEdi.getValueForCarriage(), 3);

		// 11.9 Separator : Slant
		ediWriter.addSeparator();

		// 11.10 Value for Customs Declaration
		// 11.10.1 Declared Value for Customs
		ediWriter.write(airlineEdi.getValueForCustomDeclarations(), 3);


		
		
		// 11.11 Value for Insurance Declaration
		// 11.11.1 Separator : Slant
		ediWriter.addSeparator();
		// 11.11.2 Amount of Insurance
		ediWriter.write(airlineEdi.getValueForInsurance(), 3);
		
		
		
		// 11.12 Separator : CRLF
		ediWriter.addLine();
	}
	
	
	
	private void rateDescription() {
		/*
		 * 12. Rate Description
		 * */
		
		// 12.1 Line Identifier (RTD)
		ediWriter.write("RTD", 3);
		// 12.2 Charge Line Count
		
		// 12.2.1 Separator : Slant
		ediWriter.addSeparator();
		// 12.2.2 AWB Rate Line Number
		// TODO - 
		ediWriter.write("1", 2);
		
		// 12.3 Number of Pieces/RCP Details
		
		// 12.3.1 Separator : Slant
		ediWriter.addSeparator();
		// 12.3.2 AWB Column Identifier (P)
		ediWriter.write("P", 1);
		// 12.3.3 Number of Pieces
		ediWriter.write(airlineEdi.getNoOfPieces().toString(), 4);
		
		
		// 12.4 Gross Weight Details (O)
		// 12.4.1 Separator : Slant
		ediWriter.addSeparator();
		// 12.4.2 Weight Code
		ediWriter.write("K", 1);
		// 12.4.3 Weight
		ediWriter.write(airlineEdi.getGrossWeight().toString(), 7);

		
		
		
		// 12.5 Rate Class Details
		// 12.5.1 Separator : Slant
		ediWriter.addSeparator();
		// 12.5.2 AWB Column Identifier (C)
		ediWriter.write("C", 1);
		
		// 12.5.3 Rate Class Code
		/*if (airlineEdi.getRateClass() == null) {
			airlineEdi.setRateClass(RateClass.Q);
		}*/
		ediWriter.write("Q", 1);
		
	
		
		

		// 12.6 Commodity Item Number Details (O)
		// 12.6.1 Separator : Slant
		// 12.6.2 AWB Column Identifier (S)
		// 12.6.3 Commodity Item Number
		
		// 12.7 Chargeable Weight Details
		// 12.7.1 Separator : Slant
		ediWriter.addSeparator();
		// 12.7.2 AWB Column Identifier (W)
		ediWriter.write("W", 1);
		// 12.7.3 Weight
		ediWriter.write(airlineEdi.getChargebleWeight().toString(), 7);
		ediWriter.addLine();
		
		// 12.8 Rate/Charge Details (O)
		// 12.8.1 Separator : Slant
		// 12.8.2 AWB Column Identifier (R)
		// 12.8.3 Rate or Charge
		

		
		// 12.9 Total Details (O)
		// 12.9.1 Separator : Slant
		// 12.9.2 AWB Column Identifier (T)
		// 12.9.3 Charge Amount
		
		
		// 12.10 Separator : CRLF		
		// ediWriter.addLine();
		
		
		
		
		commodityDetails();
		
		
		// 12.14 Volume (C)
		// 12.14.1 Separator : Slant
		// 12.14.2 AWB Column Identifier (N)
		// 12.14.3 Goods Data Identifier (V)
		// 12.14.4 Separator : Slant
		// 12.14.5 Volume Code
		// 12.14.6 Volume Amount
		
		// 12.15 ULD Number (O)
		// 12.15.1 Separator : Slant
		// 12.15.2 AWB Column Identifier (N)
		// 12.15.3 Goods Data Identifier (U)
		// 12.15.4 Separator : Slant
		// 12.15.5 ULD Type
		// 12.15.6 ULD Serial Number
		// 12.15.7 ULD Owner Code
		
		
		
		// 12.16 Shipper’s Load and Count
		// 12.16.1 Separator : Slant
		// 12.16.2 AWB Column Identifier (N)
		// 12.16.3 Goods Data Identifier (S)
		// 12.16.4 Separator : Slant
		// 12.16.5 SLAC

		
		
		// 12.17 Harmonised Commodity Code (O)
		// 12.17.1 Separator : Slant
		// 12.17.2 AWB Column Identifier (N)
		// 12.17.3 Goods Data Identifier (H)
		// 12.17.4 Separator : Slant
		// 12.17.5 Harmonised Commodity Code
		
		
		// 12.18 Country of Origin of Goods
		// 12.18.1 Separator : Slant
		// 12.18.2 AWB Column Identifier (N)
		// 12.18.3 Goods Data Identifier (O)
		// 12.18.4 Separator : Slant
		// 12.18.5 ISO Country Code
		
		// 12.19 Service Code Details
		// 12.19.1 Separator : Slant
		// 12.19.2 Service Code
		// 12.20 Separator : CRLF
		ediWriter.addLine();
		
	}
	
	
	private void commodityDetails() {
		
		String description = airlineEdi.getCommodityDescription().trim();
		List<String> descriptions = new ArrayList<>();
		int index = 0;
		while (index < description.length()) {
		    descriptions.add(description.substring(index, Math.min(index + 20,description.length())));
		    index += 20;
		}
		
		
		int i = 1;
		for(String desc : descriptions) {
			
			if(i == 1) {
				ediWriter.addSeparator();
				ediWriter.write("N");
				ediWriter.write(airlineEdi.getDirectShipment().equals(YesNo.Yes)?"G":"C");
				ediWriter.addSeparator();
				ediWriter.write(desc,  20);
				ediWriter.addLine();
			} else {
				ediWriter.addSeparator();
				ediWriter.write(""+i, 1);
				ediWriter.addSeparator();
				ediWriter.write("N");
				ediWriter.write(airlineEdi.getDirectShipment().equals(YesNo.Yes)?"G":"C");
				ediWriter.addSeparator();
				ediWriter.write(desc,  20);
				ediWriter.addLine();
			}
			
			
			i++;
		}
		
		 
		

		// 12.11 Goods Description (O)
		// 12.11.1 Separator : Slant
		// 12.11.2 AWB Column Identifier (N)
		// 12.11.3 Goods Data Identifier (G)
		// 12.11.4 Separator : Slant
		// 12.11.5 Nature and Quantity of Goods
		
		// 12.12 Consolidation (O)
		// 12.12.1 Separator : Slant
		// 12.12.2 AWB Column Identifier (N)
		// 12.12.3 Goods Data Identifier (C)
		// 12.12.4 Separator : Slant
		// 12.12.5 Nature and Quantity of Goods
		
		
		ediWriter.addSeparator();
		ediWriter.write("" + i, 1);
		
		
		// 12.13 Dimensions
		// 12.13.1 Separator : Slant
		ediWriter.addSeparator();
		// 12.13.2 AWB Column Identifier (N)
		ediWriter.write("N", 1);
		// 12.13.3 Goods Data Identifier (D)
		ediWriter.write("D", 1);
		// 12.13.4 Separator : Slant
		ediWriter.addSeparator();
		// 12.13.5 Weight Code (O)
		
		// 12.13.6 Weight (C)

		// 12.13.7 Separator : Slant
		ediWriter.addSeparator();
		// 12.13.8 Measurement Unit Code
		ediWriter.write("NDA", 3); // No Dimensions Available (NDA)
		// 12.13.9 Length Dimension
		// 12.13.10 Separator : Hyphen
		// 12.13.11 Width Dimension
		// 12.13.12 Separator : Hyphen
		// 12.13.13 Height Dimension
		// 12.13.14 Separator : Slant
		// 12.13.15 Numbers of Pieces
		
	}
	

	private void carrierExecution(LocationMaster selectedLocationMaster) {
		/*
		 * 17. Carrier’s Execution
		 * */ 
		 
		// 17. Carrier’s Execution
		// 17.1 Line Identifier (ISU)
		ediWriter.write("ISU", 3);
		ediWriter.addSeparator();  // email reference
		// 17.2 AWB Issue Details
		// 17.2.2 Day
		// 17.2.3 Month
		// 17.2.4 Year
		SimpleDateFormat sdf = new SimpleDateFormat("ddMMMyy");
		String consolDate = sdf.format(airlineEdi.getMasterDate());
		ediWriter.write(consolDate);
		// 17.2.5 Separator : Slant
		ediWriter.addSeparator();
		// 17.2.6 Place
		ediWriter.write(airlineEdi.getOrigin().getPortName(), 17);
		
		// 17.3 Authorisation
		// 17.3.1 Separator : Slant
		ediWriter.addSeparator();
		// 17.3.2 Signature
		ediWriter.write(selectedLocationMaster.getLocationName(), 20);
		// 17.4 Separator : CRLF
		ediWriter.addLine();
	}
	
	
	private void senderReference() {
		/*
		 * 20. Sender Reference
		 * */
		
		// 20. Sender Reference
		// 20.1 Line Identifier (REF)
		ediWriter.write("REF", 3);
		// 20.2 Separator : Slant
		ediWriter.addSeparator();
		
		// 20.3 Sender Office Message Address (C)
		// 20.3.1 Airport/City Code
		ediWriter.write(airlineEdi.getOrigin().getPortCode(), 3);
		// 20.3.2 Office Function Designator
		ediWriter.write("FM", 2);
		// 20.3.3 Company Designator
		ediWriter.write(airlineEdi.getCarrierMaster().getCarrierCode(), 2);

	}
	
	
	
	
	
	
	public String generate(AirlineEdi airlineEdi, LocationMaster selectedLocationMaster, FtpDto ftpData, UserProfile userProfile) {
		
		this.airlineEdi = airlineEdi;
		this.ediWriter = new EdiWriter();
		
		
		ediWriter.setFileLocation(ftpData.getLocalFileInputLocation());

		// Line 1 : Standard Message Identification
		String fileName = "FWB" + airlineEdi.getMawbNo() + ".txt";
		ediWriter.setFileName(fileName);
		
		
		/*
		 * 1.Standard Message Identification
		 * */

		standardMessageIdentification();
		
		/*
		 * 2. AWB Consignment Details
		 * */
		
		awbConsignmentDetails();
		
		/*
		 * 3. Flight Bookings
		 * */
		
		flightBooking();

		
		/*
		 * 4. Routing
		 * */
		routing();
		

		/*
		 * 5. Shipper
		 * */
		shipperDetail();
		
		/*
		 * 6. Consignee
		 * */
		consigneeDetail();
		
		/*
		 * 7. Consignee
		 * */
		agentDetail();
		 
		/*
		 * 8. Special Service Request (O)
		 * */
		
		specialServiceRequest();

		
		/*
		 * 9. Also Notify (O)
		 * */
		
		alsoNotify();
		
		
		/*
		 * 10. Accounting Information
		 * */
		
		accountingInformation();
		
		/*
		 * 11. Charge Declarations
		 * */
		
		chargeDeclarations(selectedLocationMaster);

		/*
		 * 12. Rate Description
		 * */
		
		rateDescription();
		
		/*
		 * 17. Carrier’s Execution
		 * */ 
		
		carrierExecution(selectedLocationMaster);
		
		/*
		 * 20. Sender Reference
		 * */
		
		senderReference();
		
		
		ediWriter.createEdiFile();
		
		if(ftpData.getEnableEmail() != null && ftpData.getEnableEmail().toUpperCase().equals("YES")) {
			ediMailService.sendEmail(ftpData.getToEmail(), ediWriter.getMessageForEmail(), userProfile);
		}
		
		return fileName;
		
	}
}
