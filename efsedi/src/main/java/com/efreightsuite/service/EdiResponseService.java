package com.efreightsuite.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import com.efreightsuite.channel.stdin.AbstractResponseService;
import com.efreightsuite.enumeration.EdiMessagingFor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class EdiResponseService {


	@Autowired
	List<AbstractResponseService> abstractResponseServiceList;
	
	
	@Autowired
	AirlineEdiResponseService airlineEdiResponseService;

	Map<EdiMessagingFor, AbstractResponseService> abstractResponseServiceMap = new HashMap<>();

	@PostConstruct
	public void init() {
		for (AbstractResponseService ars : abstractResponseServiceList) {
			abstractResponseServiceMap.put(ars.getMessageFor(), ars);
		}
	}
	
	/*public void readDirectory() {
		
		for(EdiMessagingFor ediMessagingFor : EdiMessagingFor.values()) {
			if(abstractResponseServiceMap.get(ediMessagingFor) != null && ediMessagingFor.equals(ediMessagingFor.EK)) {
				abstractResponseServiceMap.get(ediMessagingFor).processor();
			}
		}
		
	}*/
	
	public void readDirectory() {
		
		airlineEdiResponseService.processor(EdiMessagingFor.EK);
		
		// airlineEdiResponseService.processor(EdiMessagingFor.CX);
		
		// airlineEdiResponseService.processor(EdiMessagingFor.EY);
		
	}
	
	
}
