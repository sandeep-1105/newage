package com.efreightsuite.service;

import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.efreightsuite.configuration.TenantService;
import com.efreightsuite.controller.EdiController;
import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.SaaSDto;


@Service
@Log4j2
public class EdiService {

	@Autowired
	TenantService tenantService;
	
	
	public BaseDto saasAdded(SaaSDto saasDto){
 		return tenantService.loadNewDataSource(saasDto.getSaasId(), saasDto.getCompanyName(), saasDto.getDriverName(), saasDto.getUrl(), saasDto.getUserName(), saasDto.getPassword());
	}
	
}
