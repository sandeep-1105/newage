package com.efreightsuite.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.efreightsuite.communication.FtpDto;
import com.efreightsuite.model.AirlineEdi;
import com.efreightsuite.model.AirlineEdiHouse;
import com.efreightsuite.model.LocationMaster;
import com.efreightsuite.model.UserProfile;
import com.efreightsuite.service.email.EdiMailService;
import com.efreightsuite.util.EdiWriter;

@Service
public class FHLService {

	AirlineEdi airlineEdi;
	
	
	EdiWriter ediWriter;
	
	@Autowired
	EdiMailService ediMailService;

		
	private void standardMessageIdentification(AirlineEdiHouse house) {
		/*
		 * 1. Standard Message Identification
		 * */
		// 1.1 Standard Message Identifier
		
		ediWriter.write("FHL", 3);
		// 1.2 Separator : Slant
		ediWriter.addSeparator();
		
		// 1.3 Message Type Version Number
		ediWriter.write("4", 3);
		// 1.4 Separator : CRLF
		ediWriter.addLine();
	}
	
	private void masterAWBConsignmentDetail(AirlineEdiHouse ediHouse) {
		/*
		 * 2. Master AWB Consignment Detail
		 * */
		
		// 2.1 Line Identifier
		ediWriter.write("MBI", 3);
		
		// 2.2 Separator
		ediWriter.addSeparator();
		
		// 2.3 Master AWB Identification
		String mawbNumber = airlineEdi.getMawbNo();
		String airlineNumber = mawbNumber.substring(0, 3);
		String awbNumber = mawbNumber.substring(3);
		
		// 2.3.1 Airline Prefix
		ediWriter.write(airlineNumber, 3);
		
		// 2.3.2 Separator : Hyphen
		ediWriter.write("-");
		
		// 2.3.3 AWB Serial Number
		ediWriter.write(awbNumber, 8);
		
		// 2.4 AWB Origin and Destination
		
		// 2.4.1 Airport/City Code (of Origin)
		ediWriter.write(airlineEdi.getOrigin().getPortCode(), 3);

		// 2.4.2 Airport/City Code (of Destination)
		ediWriter.write(airlineEdi.getDestination().getPortCode(), 3);
		
		
		// 2.5 Quantity Detail
		
		// 2.5.1 Separator : Slant
		ediWriter.addSeparator();
		
		// 2.5.2 Shipment Description Code (T)   T - Total/P - Part
		ediWriter.write("T", 1);
		
		// 2.5.3 Number of Pieces
		ediWriter.write(airlineEdi.getNoOfPieces().toString(), 4);
		
		// 2.5.4 Weight Code
		ediWriter.write("K");
		
		// 2.5.5 Weight
		ediWriter.write(airlineEdi.getGrossWeight().toString(), 7);
		// 2.6 Separator : CRLF
		ediWriter.addLine();
		
	}
	
	private void houseWayBillSummaryDetail(AirlineEdiHouse house) {
		/*
		 * 3. House Waybill Summary Details
		 * */
		
			// 3.1 Line Identifier
			ediWriter.write("HBS");
			
			// 3.2 Separator : Slant
			ediWriter.addSeparator();
			
			// 3.3 HWB Serial Number
			ediWriter.write(house.getHawbNo(), 12);
			// 3.4 Separator : Slant
			ediWriter.addSeparator();
			
			
			// 3.5 House Waybill Origin and Destination
			
			// 3.5.1 Airport/City Code (of Departure)
			ediWriter.write(house.getOrigin().getPortCode(), 3);
			// 3.5.2 Airport/City Code (of Destination)
			ediWriter.write(house.getDestination().getPortCode(), 3);
			
			// 3.6 Separator : Slant
			ediWriter.addSeparator();
			
			// 3.7 House Waybill Totals
			// 3.7.1 Number of Pieces
			ediWriter.write(house.getExternalNumberOfPieces().toString(), 3);
			// 3.7.2 Separator : Slant
			ediWriter.addSeparator();
			// 3.7.3 Weight Code
			ediWriter.write("K");
			// 3.7.4 Weight
			ediWriter.write(house.getGrossWeight().toString(), 7);
			// 3.7.5 Separator : Slant
			ediWriter.addSeparator();
			// 3.7.6 SLAC (Optional)
			ediWriter.addSeparator();
			
			
			// 3.8 Nature of Goods (M)
			// 3.8.1 Separator : Slant
			ediWriter.addSeparator();
			// 3.8.2 Manifest Description of Goods
			ediWriter.write(house.getCommodityDescription(), 15);
			
			// 3.9 Separator : CRLF (C)
			//ediWriter.addLine();
			
			
			// 3.10 Special Handling Requirements (O)
			// 3.10.1 Separator : Slant
			// 3.10.2 Special Handling Code
			// 3.11 Separator : CRLF
			ediWriter.addLine();

	}
	
	private void shipperNameAndAddress(AirlineEdiHouse ediHouse) {
		/*
		 * 7. Shipper Name and Address (O)
		 * */
		//7.1 Line Identifier
		ediWriter.write("SHP");
		//7.2 Separator : Slant
		ediWriter.addSeparator();
		//7.3 Name
		ediWriter.write(ediHouse.getShipper().getPartyName(), 35);
		//7.4 Separator : CRLF
		ediWriter.addLine();
		//7.5 Street Address
		
		//7.5.1 Separator : Slant
		ediWriter.addSeparator();
		//7.5.2 Street Address
		ediWriter.write(ediHouse.getShipperAddress().getAddressLine1() + " " + ediHouse.getShipperAddress().getAddressLine2(), 35);
		
		//7.5.3 Separator : CRLF
		ediWriter.addLine();
		
		//7.6 Location
		
		//7.6.1 Separator : Slant
		ediWriter.addSeparator();
		//7.6.2 Place
		ediWriter.write(ediHouse.getShipperAddress().getCity().getCityName(), 35);
		//7.6.3 Separator : Slant
		ediWriter.addSeparator();
		//7.6.4 State/Province
		//7.6.5 Separator : CRLF
		ediWriter.addLine();
		
		
		
		//7.7 Coded Location
		//7.7.1 Separator : Slant
		ediWriter.addSeparator();
		//7.7.2 ISO Country Code
		ediWriter.write(ediHouse.getShipperAddress().getCountry().getCountryCode(), 2);
		//7.7.3 Separator : Slant
		ediWriter.addSeparator();
		//7.7.4 Post Code
		if(ediHouse.getShipperAddress().getZipCode() != null && !ediHouse.getShipperAddress().getZipCode().isEmpty()) {
			ediWriter.write(ediHouse.getShipperAddress().getZipCode(), 9);
		}
		//7.8 Contact Detail
		//7.8.1 Separator : Slant
		//7.8.2 Contact Identifier
		//7.8.3 Separator : Slant
		//7.8.4 Contact Number
		//7.9 Separator : CRLF
		ediWriter.addLine();
	}
	
	private void consigneeNameAndAddress(AirlineEdiHouse house) {
		/*
		 * 8. Consignee Name and Address
		 * */
		
		// 8.1 Line Identifier
		ediWriter.write("CNE");
		
		
		//8.2 Separator : Slant
		ediWriter.addSeparator();
		
		//8.3 Name
		ediWriter.write(house.getConsignee().getPartyName(), 35);
		
		//8.4 Separator : CRLF
		ediWriter.addLine();

		
		// 8.5 Street Address (M)
		// 8.5.1 Separator : Slant
		ediWriter.addSeparator();
		// 8.5.2 Street Address
		ediWriter.write(house.getConsigneeAddress().getAddressLine1(), 35);
		// 8.5.3 Separator : CRLF
		ediWriter.addLine();
		
		// 8.6 Location (M)
		// 8.6.1 Separator : Slant
		ediWriter.addSeparator();
		ediWriter.write(house.getConsigneeAddress().getCity().getCityName(), 17);
		// 8.6.2 Place
		// 8.6.3 Separator : Slant
		ediWriter.addSeparator();
		// 8.6.4 State/Province
		//ediWriter.write(airlineEdi.getAgentAtDestinationAddress().getState().getStateName(), 9);
		// 8.6.5 Separator : CRLF
		ediWriter.addLine();
		
		
		// 8.7 Coded Location (M)
		// 8.7.1 Separator : Slant
		ediWriter.addSeparator();
		// 8.7.2 ISO Country Code
		ediWriter.write(house.getConsigneeAddress().getCountry().getCountryCode(), 2);
		// 8.7.3 Separator : Slant
		ediWriter.addSeparator();
		// 8.7.4 Post Code
		if(house.getConsigneeAddress().getZipCode() != null && !house.getConsigneeAddress().getZipCode().isEmpty()) {
			ediWriter.write(house.getConsigneeAddress().getZipCode(), 9);
		}
		
		// 8.8 Contact Detail (O)
		// 8.8.1 Separator : Slant
		// 8.8.2 Contact Identifier
		// 8.8.3 Separator : Slant
		// 8.8.4 Contact Number
		// 8.9 Separator : CRLF
		ediWriter.addLine();
	}
	
	private void chargeDeclaration(AirlineEdiHouse house) {

		
		// 9. Charge Declarations (O)
		// 9.1 Line Identifier
		// 9.2 Separator : Slant
		// 9.3 ISO Currency Code
		// 9.4 Separator : Slant
		// 9.5 Prepaid/Collect Charge DeclarationsStatus:
		// 9.5.1 P/C Ind. (Weight/Valuation)
		// 9.5.2 P/C Ind. (Other Charges)
		// 9.6 Value for Carriage Declaration
		// 9.6.1 Separator : Slant
		// 9.6.2 Declared Value for Carriage
		// 9.7 Value for Customs Declaration
		// 9.7.1 Separator : Slant
		// 9.7.2 Declared Value for Customs
		// 9.8 Value for Insurance Declaration
		// 9.8.1 Separator : Slant
		// 9.8.2 Declared Value for Insurance
		// 9.9 Separator : CRLF
	}
	

	public String generate(AirlineEdi airlineEdi, LocationMaster selectedLocationMaster, AirlineEdiHouse ediHouse, FtpDto ftpData, UserProfile userProfile, int count) {
		
		this.airlineEdi = airlineEdi;
		this.ediWriter = new EdiWriter();
		
		
		ediWriter.setFileLocation(ftpData.getLocalFileInputLocation());

		// Line 1 : Standard Message Identification
		String fileName = "FHL" + ediHouse.getAirlineEdi().getMawbNo() + "" + count + ".txt";
		ediWriter.setFileName(fileName);
		
		/*
		 * 1. Standard Message Identification
		 * */
	 
		standardMessageIdentification(ediHouse);
		
		
		/***********************************************************************************************************/
		
		/*
		 * 2. Master AWB Consignment Detail
		 * */
		
		masterAWBConsignmentDetail(ediHouse);
		
		/***********************************************************************************************************/
		
		
		/*
		 * 3. House Waybill Summary Details
		 * */
		
		houseWayBillSummaryDetail(ediHouse);
		
		
		/*************************************************************************************/
		
		/*
		 * 7. Shipper Name and Address (O)
		 * */
		
		shipperNameAndAddress(ediHouse);
		
		
		
		/**************************************************************************************/
		
		/*
		 * 8. Consignee Name and Address
		 * */
		
		consigneeNameAndAddress(ediHouse);
		
		
		
		
		/*****************************************************************************************/
		
		/*
		 * 9. Charge Declaration (O)
		 * */
		
		chargeDeclaration(ediHouse);
		
		ediWriter.createEdiFile();
		
		if(ftpData.getEnableEmail() != null && ftpData.getEnableEmail().toUpperCase().equals("YES")) {
			ediMailService.sendEmail(ftpData.getToEmail(), ediWriter.getMessageForEmail(), userProfile);
		}
		
		return fileName;
		
	}
	
}
