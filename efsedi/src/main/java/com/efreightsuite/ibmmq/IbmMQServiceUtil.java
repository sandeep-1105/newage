package com.efreightsuite.ibmmq;

import java.io.IOException;

import com.ibm.mq.MQC;
import com.ibm.mq.MQEnvironment;
import com.ibm.mq.MQException;
import com.ibm.mq.MQGetMessageOptions;
import com.ibm.mq.MQMessage;
import com.ibm.mq.MQPutMessageOptions;
import com.ibm.mq.MQQueue;
import com.ibm.mq.MQQueueManager;

import lombok.extern.log4j.Log4j2;

@Log4j2
public class IbmMQServiceUtil {

	private MQQueueManager qMgr = null;
	private MQQueue mqQueue = null;
	private String mqManager, queuName;

	public boolean outBoundInit(IbmMQProperties properties, boolean isInBound) {

		log.info("Initialize Queue properties and Queue Environment");
		MQEnvironment.hostname = properties.getHost();
		MQEnvironment.port = properties.getPort();
		MQEnvironment.channel = properties.getChannel();
		mqManager = properties.getManager();
		if(isInBound) {
			queuName = properties.getInboundQueue();
		} else {
			queuName = properties.getOutboundQueue();
		}
		try {
			log.info("An MQ Initialization parameter : Host Name " + properties.getHost() + " Port "
					+ properties.getPort() + " Queue Manager " + properties.getManager() + " Queue Name " + queuName);
			qMgr = new MQQueueManager(mqManager);
			return true;
		} catch (MQException e) {
			Object o = e.exceptionSource;
			log.error("MQException originated from object '" + o.toString() + "Completion code = " + e.completionCode
					+ e.getMessage());
		}
		return false;
	}

	public boolean openConnection() {
		log.info("Going to get Queue Access");
		if (qMgr == null) {
			log.error("MQ.openQueue - MQException occurred because of incorrect MQManager");
		} else {
			int openOptions = MQC.MQOO_INPUT_AS_Q_DEF | MQC.MQOO_OUTPUT;
			try {
				mqQueue = qMgr.accessQueue(queuName, openOptions);
				return true;
			} catch (MQException mqe) {
				if (mqe.reasonCode == 2045) {
					openOptions = MQC.MQOO_OUTPUT;
					try {
						mqQueue = qMgr.accessQueue(queuName, openOptions);
						return true;
					} catch (Exception ex) {
						ex.printStackTrace();
					}
				} else {
					log.error("MQ.openQueue - MQException occurred : Completion code " + mqe.completionCode
							+ " Reason code " + mqe.reasonCode);
				}
			}
		}

		return false;
	}

	public void convertAndSend(String msg) {
		if (qMgr == null) {
			return;
		}
		try {
			MQPutMessageOptions pmo = new MQPutMessageOptions();
			MQMessage mqMsg = new MQMessage();
			mqMsg.writeString(msg);
			log.info("Writing Message Length " + msg.length() + "\n" + msg);
			mqQueue.put(mqMsg, pmo);
		} catch (MQException mqe) {
			log.error("MQ.write - MQException occurred : Completion code " + mqe.completionCode + " Reason code " + mqe.reasonCode);
		} catch (IOException ex) {
			ex.printStackTrace();
			log.error("An error occurred whle writing to the message buffer: " , ex);
		} catch (Exception ex) {
			ex.printStackTrace();
			log.error("MQ Write Error ");
		}
	}

	public String readAndConvert() {
		String msgText = null;
		if (qMgr != null) {
			try {
				MQMessage mQMessage = new MQMessage();
				MQGetMessageOptions gmo = new MQGetMessageOptions();
				// Specify the wait interval for the message in milliseconds
				gmo.waitInterval = 300000;
				// Get a message from the queue
				mqQueue.get(mQMessage, gmo);
				int msglen = mQMessage.getMessageLength();
				msgText = mQMessage.readString(msglen);
				// msgText = mQMessage.readUTF();
				log.info("Reading Message Length " + msglen + "\n" + msgText);
			} catch (MQException mqe) {
				int reason = mqe.reasonCode;
				if (reason == 2033) { // No Message
					log.info("No message on the specified queue : " + queuName);
				} else {
					log.info("MQ Error occured : Completion Code " + mqe.completionCode + " Reason Code "
							+ mqe.reasonCode);
				}
			} catch (IOException e) {
				e.printStackTrace();
				log.error("MQ read Error ");
			}
		}
		return msgText;
	}
 
	public IbmMQWriteOperationResponse mqServeWriteOperation(IbmMQProperties properties, String message){

		IbmMQServiceUtil service = new IbmMQServiceUtil();
		if(service.outBoundInit(properties, false)) {
			log.info("Write Queue Initialized.. ");
			if(service.openConnection()) {
				service.convertAndSend(message);
				return new IbmMQWriteOperationResponse(true, "Message Sent TO MQ");
			} else {
				log.error("Write Queue cannot able to Open Connection");
				return new IbmMQWriteOperationResponse(false, "MQ Connection Failure");
			}
		} else {
			log.error("Write Queue cannot able to Open Connection");
			return new IbmMQWriteOperationResponse(false, "MQ Connection Failure");
		}
	}

	public void closeSession() {
		try {
			if (mqQueue != null)
				mqQueue.close();
			if (qMgr != null)
				qMgr.disconnect();

		} catch (MQException ex) {
			log.error("MQException occurred : Completion code " + ex.completionCode + ">MQStatus: Reason code "
					+ ex.reasonCode);
			ex.printStackTrace();
		}
	}
}
