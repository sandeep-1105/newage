package com.efreightsuite.ibmmq;

import lombok.Data;

@Data
public class IbmMQWriteOperationResponse {

	boolean success;
	
	String statusMessage;
	
	public IbmMQWriteOperationResponse() {}
	
	public IbmMQWriteOperationResponse(boolean success, String statusMessage) {
		this.statusMessage = statusMessage;
		this.success = success;
	}
}
