package com.efreightsuite.ibmmq;

import lombok.Data;

@Data
public class IbmMQProperties {
	 
	String host = "172.16.0.50";
	int port= 1531;
	String manager = "NA_QGC1";
	String channel = "SYSTEM.DEF.SVRCONN";
	String outboundQueue = "AES.NEW.OUTBOUND";
	String inboundQueue = "AES.NEW.INPUT";
	
	public IbmMQProperties(){}
	public IbmMQProperties(String host, int port, String manager, String channel, String outboundQueue, String inboundQueue ){
		this.host = host;
		this.port = port;
		this.manager = manager;
		this.channel = channel;
		this.outboundQueue = outboundQueue;
		this.inboundQueue = inboundQueue;
	}
}
