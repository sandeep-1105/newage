package com.efreightsuite.scheduler;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.efreightsuite.channel.RabbitMqSender;
import com.efreightsuite.configuration.SessionRoutingDataSource;
import com.efreightsuite.configuration.TenantService;
import com.efreightsuite.dto.MessagingDto;

import lombok.extern.log4j.Log4j2;

@Component
@Log4j2
public class SaaSFinder {

	final int value = 1000 * 60 * 2;
	
	@Value("${newage.saas.based.application.enable}")
	boolean saasBased;

	@Autowired
	TenantService tenantService;

	@Autowired
	RabbitMqSender rabbitMqSender;

	private void finder() {
		if (saasBased) {
			for (String saas : tenantService.getSaasMap().keySet()) {
				if (!saas.equals("COM")) {
					MessagingDto request = new MessagingDto();
					request = new MessagingDto();
					request.setResponse(true);
					request.setSaasId(saas);
					rabbitMqSender.sendtoMQ(request);
				}
			}
		} else {
			log.info("Not Saas Based ");
		}

	}

	@Scheduled(fixedDelay = value)
	public void run() {
		log.info("Saas Finder started : " + new Date());
		finder();
	}

}
