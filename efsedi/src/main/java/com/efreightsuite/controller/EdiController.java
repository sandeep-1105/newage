package com.efreightsuite.controller;

import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.SaaSDto;
import com.efreightsuite.service.EdiService;


@RestController
@Log4j2
@RequestMapping(value = "/api/v1/edi")
public class EdiController {

	@Autowired
	EdiService ediService;
	
	@RequestMapping(value = "/saas/added", method = RequestMethod.POST)
	public BaseDto process(@RequestBody SaaSDto data) {
		log.info("Edi controller Class -> process Mtd single ");
		return ediService.saasAdded(data);
	}
}
