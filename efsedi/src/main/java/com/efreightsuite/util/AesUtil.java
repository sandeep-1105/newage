package com.efreightsuite.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.lang3.StringUtils;

import com.efreightsuite.enumeration.AesAction;
import com.efreightsuite.enumeration.FillingType;
import com.efreightsuite.enumeration.YesNo;
import com.efreightsuite.model.AesTransportMode;
import com.efreightsuite.model.AesUsForeignPortMaster;
import com.efreightsuite.model.AesUsPortMaster;
import com.efreightsuite.model.AesUsStateCodeMaster;
import com.efreightsuite.model.CarrierMaster;
import com.efreightsuite.model.CountryMaster;
import com.efreightsuite.model.InBondTypeMaster;
import com.efreightsuite.model.PortMaster;
import com.efreightsuite.model.StateMaster;

public class AesUtil {

	public static boolean checkinteger(String str) {
		str = replace(translate(str));
		if(str.length() == 0) {
			return true;
		} else {
			return false;
		}
	}
	public static String replace(String replaceStr){
		if(replaceStr == null) {
			return "";
		}
		replaceStr = replaceStr.replace("x", "");
		return replaceStr;
	}
	public static String translate(String replaceStr){
		if(replaceStr == null) {
			return "";
		}
		  replaceStr = replaceStr.replace('0', 'x');
		  replaceStr = replaceStr.replace('1', 'x');
		  replaceStr = replaceStr.replace('2', 'x');
		  replaceStr = replaceStr.replace('3', 'x');
		  replaceStr = replaceStr.replace('4', 'x');
		  replaceStr = replaceStr.replace('5', 'x');
		  replaceStr = replaceStr.replace('6', 'x');
		  replaceStr = replaceStr.replace('7', 'x');
		  replaceStr = replaceStr.replace('8', 'x');
		  replaceStr = replaceStr.replace('9', 'x');
		  return replaceStr;
	}

    public static String nullOrAppendEmptyInRightPadd(String input, int length, String replacement) {
    	if(input == null) {
    		input = "";
    	}
    	input = StringUtils.rightPad(input,length,replacement);
    	if(input.length() > length) {
    		return input.substring(0, length);
    	} 
    	return input;
    }

    public static String nullOrAppendEmptyInLeftPadd(String input, int length, String replacement) {
    	if(input == null) {
    		input = "";
    	}
    	input = StringUtils.leftPad(input,length,replacement);
    	if(input.length() > length) {
    		return input.substring(0, length);
    	} 
    	return input;
    }
	public static String getCurrentDate(Date date) {
		if(date == null) {
			return null;	
		}
		SimpleDateFormat sdf = new SimpleDateFormat("YYYYMMdd");
		return sdf.format(date);
	}

    public static String customSubStringNullReturnEmptyString(String input, int startIndex, int endIndex, boolean appendExtraSpace) {
    	if(input == null || input.length() == 0) {
    		return "";
    	}
    	if(input.length() >= startIndex && input.length() >= endIndex) {
    		return input.substring(startIndex, endIndex);
    	} else {
    		if(appendExtraSpace) {
    			return nullOrAppendEmptyInRightPadd(input, endIndex, " ");
    		} else {
    			return input;
    		}
    	}
    }
    public static String customSubStringNullReturnNull(String input, int startIndex, int endIndex) {
    	if(input == null || input.length() == 0) {
    		return null;
    	}
    	if(input.length() >= startIndex && input.length() >= endIndex) {
    		return input.substring(startIndex, endIndex);
    	} else {
    		return nullOrAppendEmptyInRightPadd(input, endIndex, " ");
    	}
    }
    public static String modAesTransportMode(AesTransportMode aesTransportMode) {
    	if(aesTransportMode == null || aesTransportMode.getAesTransportName() == null) {
    		return nullOrAppendEmptyInRightPadd(null, 2, " ");
    	} else {
    		return customSubStringNullReturnEmptyString(aesTransportMode.getAesTransportName(), 0, 2, true);
    	}
    }
    public static String modAesFillingType(FillingType aesFilingType, String replacement) {
    	if(aesFilingType == null) {
    		return replacement;
    	} else {
    		return customSubStringNullReturnEmptyString(aesFilingType.toString(), 0, 1, true);
    	}
    }
    public static String modAesCountryCode(CountryMaster countryMaster) {
    	if(countryMaster == null || countryMaster.getCountryCode() == null) {
    		return nullOrAppendEmptyInRightPadd(null, 2, " ");
    	} else {
    		return customSubStringNullReturnEmptyString(countryMaster.getCountryCode(), 0, 2, true);
    	}
    }
    public static String modAesOriginState(AesUsStateCodeMaster master) {
    	if(master == null || master.getName() == null) {
    		return nullOrAppendEmptyInRightPadd(null, 2, " ");
    	} else {
    		return customSubStringNullReturnEmptyString(master.getStateCode(), 0, 2, true);
    	}
    }
    

	public static String rpad_nvl(String str, int length){
		return nullOrAppendEmptyInRightPadd(AesUtil.nvlReplace(str, ""), length, " ");
	}
	public static String lpad_nvl(String str, int length){
		return nullOrAppendEmptyInLeftPadd(AesUtil.nvlReplace(str, ""), length, " ");
	}
	
    public static String nvlReplace(String value, String alternate) {
    	if(value != null){
    		return value;
    	} else if(alternate != null) {
    		return alternate;
    	} else {
    		return null;
    	}
    }

    public static String modAesCarrierName(CarrierMaster object) {
    	if(object == null || object.getCarrierName() == null) {
    		return nullOrAppendEmptyInRightPadd(null, 23, " ");
    	} else {
    		return customSubStringNullReturnEmptyString(removeAscii(object.getCarrierName()), 0, 23, true);
    	}
    }
    public static String removeAscii (String input){
    	String result = "";
    	if(input == null) {
    		return result;
    	}
    	input = input.trim();
    	for(int i = 0; i < input.length(); i++) {

        	char character = input.charAt(i);    
        	int ascii = (int) character;
    		if(ascii >= 32 && ascii <= 126) {
    			result = result + character;
    		}
    		if(ascii == 10) {
    			result = result + " ";
    		}
     	}
    	return result;
    }

    public static String asciiTOString(int i) {

		return Character.toString ((char) i);
    }
    public static String modAesPort(AesUsForeignPortMaster object, int maxLength) {
    	if(object == null || object.getPortName() == null) {
    		return nullOrAppendEmptyInRightPadd(null, maxLength, " ");
    	} else {
    		return customSubStringNullReturnEmptyString(object.getPortName(), 0, maxLength, true);
    	}
    }

    public static String modAesPort(AesUsPortMaster object, int maxLength) {
    	if(object == null || object.getPortName() == null) {
    		return nullOrAppendEmptyInRightPadd(null, maxLength, " ");
    	} else {
    		return customSubStringNullReturnEmptyString(object.getPortName(), 0, maxLength, true);
    	}
    }

    
    
    public static String modAesYesNo(YesNo yesNo) {
    	if(yesNo == null) {
    		return "N";
    	} else if(yesNo.equals(YesNo.Yes)) {
    		return "Y";
    	} else {
    		return "N";
    	}
    }

    public static String modAesAction(AesAction aesAction) {
    	if(aesAction == null || aesAction.equals(AesAction.Add)) {
    		return "A";
    	} else if(aesAction.equals(AesAction.Cancel)) {
    		return "C";
    	} else if(aesAction.equals(AesAction.Change)) {
    		return "C";
    	/*} else if(aesAction.equals(AesAction.Delete)) {
    		return "DD";*/
    	} else if(aesAction.equals(AesAction.Replace)) {
    		return "R";
    	} else {
    		return "A";
    	}
    }

    public static String modAesInBondType(InBondTypeMaster object) {
    	if(object == null || object.getInbondType() == null) {
    		return "70";
    	} else {
    		return object.getInbondType();
    	}
    }

    public static String substr(String str, int beginIdx, int endIdx) {
    	int endsAt = beginIdx+endIdx-1;
    	int beginsAt = beginIdx-1;
    	if(str.length() < beginIdx) {
    		return "";
    	}
    	if(str.length() >= endsAt) {
    		return str.substring(beginIdx-1, (beginIdx+endIdx-1));
    	} else {
    		return str.substring(beginIdx-1, str.length()-1);
    	}
    }
    
    public static String stringNVL(String value, String input, int beginIdx, int endIdx){
    	if(value != null && value.trim().length() > 0) {
    		return value;
    	}
    	return substr(input, beginIdx, endIdx);
    }


	public static Date convertStringToSpDateFormat(String input, String dfStr){
		
		SimpleDateFormat sdf = new SimpleDateFormat(dfStr);
		Date output = null;
		try {
			output = sdf.parse(input);
			
		} catch (ParseException e) {
		}
		return output;
	}
}
