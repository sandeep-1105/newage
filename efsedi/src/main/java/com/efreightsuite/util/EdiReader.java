package com.efreightsuite.util;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.StringTokenizer;

import com.efreightsuite.exception.EDIReaderException;
import lombok.extern.log4j.Log4j2;

@Log4j2
public class EdiReader {

	private String fileName;

	private String fileLocation;

	private String separator;

	private String fileContent;

	private HashMap<Integer, ArrayList<String>> content;

	public EdiReader() {
		this.separator = "/";
		content = new HashMap<>();
	}

	public void read() {
		InputStream is = null;
		BufferedReader reader = null;
		try {
			is = new FileInputStream(this.getFileLocation() + this.getFileName());
			reader = new BufferedReader(new InputStreamReader(is));

			String line = reader.readLine();
			StringBuffer sb = new StringBuffer();
			int lineCount = 0;
			while (line != null) {
				sb.append(line).append("\n");
				

				StringTokenizer tokenizer = new StringTokenizer(line, this.getSeparator());
				ArrayList<String> tokens = new ArrayList<>();
				while (tokenizer.hasMoreTokens()) {
					tokens.add(tokenizer.nextToken());
				}

				content.put(lineCount, tokens);
				lineCount++;

				line = reader.readLine();
			}

			this.setFileContent(sb.toString());

		} catch (FileNotFoundException e) {
			throw new EDIReaderException("EDI file not found " + getFileLocation());
		} catch (IOException e) {
			throw new EDIReaderException("EDI file not read " + getFileLocation());
		} finally {
			if (is != null) {
				try {
					is.close();
				} catch (IOException e) {
					log.error("Exception while closing the InputStream", e);
				}
			}

			if (reader != null) {
				try {
					reader.close();
				} catch (IOException e) {
					log.error("Exception while closing the Reader", e);
				}
			}
		}
	}

	public String getValue(int line, int token) {
		String value = "";
		
		try {
			value = content.get(line).get(token);
		} catch(NullPointerException exception) {
			value = "";
		} catch(IndexOutOfBoundsException exception) {
			value = "";
		}
		
		return value;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getFileLocation() {
		return fileLocation;
	}

	public void setFileLocation(String fileLocation) {
		this.fileLocation = fileLocation;
	}

	public String getSeparator() {
		return separator;
	}

	public void setSeparator(String separator) {
		this.separator = separator;
	}

	public String getFileContent() {
		return fileContent;
	}

	public void setFileContent(String fileContent) {
		this.fileContent = fileContent;
	}

}
