package com.efreightsuite.util;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.efreightsuite.common.component.CacheRepository;
import com.efreightsuite.dto.BaseDto;

import lombok.extern.log4j.Log4j2;

@Service
@Log4j2
public class AppUtil {

	@Autowired
	CacheRepository cacheRepository;

	public BaseDto setDesc(BaseDto dto) {
		log.debug("Response Message being sent to the client : " + dto.getResponseCode() + " --> "
				+ getDesc(dto.getResponseCode()));
		dto.setResponseDescription(getDesc(dto.getResponseCode()));
		return dto;
	}

	public String getDesc(String code) {
		Map<String, String> codeMap = cacheRepository.getNLS(SaaSUtil.getSaaSId(), "English");
		return codeMap.get(code);
	}

}
