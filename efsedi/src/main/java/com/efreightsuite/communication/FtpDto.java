package com.efreightsuite.communication;

import lombok.Data;

@Data
public class FtpDto {

	String server;
	Integer port;
	String userName;
	String password;
	String localFileInputLocation;
	String remoteFileInputLocation;
	String fileName;
	String localFileOutputLocation;
	String remoteFileOutputLocation;
	String readDirInterval;
	String toEmail;
	String enableEmail;
	
	byte[] localFile;
	byte[] remoteFile;
	
}
