package com.efreightsuite.communication;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;

import lombok.extern.log4j.Log4j2;
 

public class FtpTransfer {

	/*public static void main(String[] args) {
		FtpTransfer obj = new FtpTransfer();
		FtpDto ftpDto = new FtpDto();
		ftpDto.setServer("localhost");
		ftpDto.setPort(21);
		ftpDto.setUserName("tanveer");
		ftpDto.setPassword("finatel123");
		ftpDto.setLocalFileInputLocation("/home/tanveer");
		ftpDto.setRemoteFileInputLocation("test");
		ftpDto.setFileName("abc.txt");
		obj.upload(ftpDto);
	}*/
	
	public boolean upload(FtpDto ftpDto) {

		FTPClient ftpClient = new FTPClient();

		try {
			ftpClient.connect(ftpDto.getServer(), ftpDto.getPort());
			ftpClient.login(ftpDto.getUserName(), ftpDto.getPassword());
			ftpClient.enterLocalPassiveMode();

			ftpClient.setFileType(FTP.BINARY_FILE_TYPE);

			File firstLocalFile = new File(ftpDto.getLocalFileInputLocation() + "/" + ftpDto.getFileName());
			InputStream inputStream = new FileInputStream(firstLocalFile);

			System.out.println("Start uploading file : " + ftpDto);
			boolean done = ftpClient.storeFile(ftpDto.getRemoteFileInputLocation() + "/" + ftpDto.getFileName(),
					inputStream);
			inputStream.close();

			if (done) {
				System.out.println("File is uploaded successfully.");
			}
			return done;
		} catch (Exception ex) {
			System.out.println("FTP file transfer upload error "+ ex);
		} finally {
			try {
				if (ftpClient.isConnected()) {
					ftpClient.logout();
					ftpClient.disconnect();
				}
			} catch (Exception ex) {
				System.out.println("FTP file transfer connection closing error "+ ex);
			}
		}
		return false;
	}

	public boolean download(FtpDto ftpDto) {

		FTPClient ftpClient = new FTPClient();
		try {

			ftpClient.connect(ftpDto.getServer(), ftpDto.getPort());
			ftpClient.login(ftpDto.getUserName(), ftpDto.getPassword());
			ftpClient.enterLocalPassiveMode();

			ftpClient.setFileType(FTP.BINARY_FILE_TYPE);

			File downloadFile1 = new File(ftpDto.getLocalFileOutputLocation() + "/" + ftpDto.getFileName());
			OutputStream outputStream = new BufferedOutputStream(new FileOutputStream(downloadFile1));
			boolean success = ftpClient.retrieveFile(ftpDto.getRemoteFileOutputLocation() + "/" + ftpDto.getFileName(),
					outputStream);
			outputStream.close();

			if (success) {
				System.out.println("File has been downloaded successfully.");
			}

			return success;

		} catch (Exception ex) {
			System.out.println("FTP file transfer download error "+ ex);
		} finally {
			try {
				if (ftpClient.isConnected()) {
					ftpClient.logout();
					ftpClient.disconnect();
				}
			} catch (Exception ex) {
				System.out.println("FTP file transfer connection closing error "+ ex);
			}
		}
		return false;
	}
}