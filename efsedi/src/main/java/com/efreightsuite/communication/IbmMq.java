package com.efreightsuite.communication;

import javax.jms.JMSException;
import javax.jms.Session;

import org.springframework.stereotype.Service;

import com.ibm.jms.JMSMessage;
import com.ibm.jms.JMSTextMessage;
import com.ibm.mq.jms.JMSC;
import com.ibm.mq.jms.MQQueue;
import com.ibm.mq.jms.MQQueueConnection;
import com.ibm.mq.jms.MQQueueConnectionFactory;
import com.ibm.mq.jms.MQQueueReceiver;
import com.ibm.mq.jms.MQQueueSender;
import com.ibm.mq.jms.MQQueueSession;
import com.ibm.mq.jms.MQTopic;
import com.ibm.mq.jms.MQTopicConnection;
import com.ibm.mq.jms.MQTopicConnectionFactory;
import com.ibm.mq.jms.MQTopicPublisher;
import com.ibm.mq.jms.MQTopicSession;
import com.ibm.mq.jms.MQTopicSubscriber;


import lombok.extern.log4j.Log4j2;



@Service
@Log4j2
public class IbmMq {
	
	/**
	 * SimplePTP: A minimal and simple test case for Point-to-point messaging (1.02 style).
	 *
	 * Assumes that the queue is empty before being run.
	 *
	 * Does not make use of JNDI for ConnectionFactory and/or Destination definitions.
	 */
  public void sendAndReceiveViaPTP(IbmMqDto dto){
	    try {
	      MQQueueConnectionFactory cf = new MQQueueConnectionFactory();

	      cf.setHostName(dto.getHostName());
	      cf.setPort(dto.getPort());
	      cf.setTransportType(JMSC.MQJMS_TP_CLIENT_MQ_TCPIP);
	      cf.setQueueManager(dto.getQueueManager());
	      cf.setChannel(dto.getChannel());

	      MQQueueConnection connection = (MQQueueConnection) cf.createQueueConnection();
	      MQQueueSession session = (MQQueueSession) connection.createQueueSession(false, Session.AUTO_ACKNOWLEDGE);
	      MQQueue queue = (MQQueue) session.createQueue(dto.getQueueName());
	      MQQueueSender sender =  (MQQueueSender) session.createSender(queue);
	      MQQueueReceiver receiver = (MQQueueReceiver) session.createReceiver(queue);      

	      JMSTextMessage message = (JMSTextMessage) session.createTextMessage(dto.getMessage());     

	      // Start the connection
	      connection.start();

	      sender.send(message);
	      
	      JMSMessage receivedMessage = (JMSMessage) receiver.receive(10000);
	      dto.setResponse(receivedMessage.getJMSMessageID());
	      
	      log.debug("IBM Queue Message Send : "+dto);

	      sender.close();
	      receiver.close();
	      session.close();
	      connection.close();

	      dto.setDone(true);
	    }
	    catch (JMSException jmsex) {
	    	log.error("JMSException for IBM Queue : ",jmsex);
	    	dto.setDone(false);
	    }
	    catch (Exception ex) {
	    	log.error("Exception for IBM Queue : ",ex);
	    	dto.setDone(false);
	    }
  }
  
  /**
   * SimplePubSub: A minimal and simple test case for Publish/Subscribe (1.02 style).
   *
   * Topics are dynamically created on the queue manager and need not be predefined.
   *
   * (The Broker must be enabled on the queue manager and the JMS Publish/Subscribe
   * queues must be defined manually.)
   *
   * Note: These samples are for WMQ Base Broker and incomplete for WebSphere Message Broker)
   *
   * Does not make use of JNDI for ConnectionFactory and/or Destination definitions.
   *
   */
  
  public void sendAndReceiveViaPubSub(IbmMqDto dto){
	 try{
		 
		 MQTopicConnectionFactory cf = new MQTopicConnectionFactory();

	      cf.setHostName(dto.getHostName());
	      cf.setPort(dto.getPort());
	      cf.setTransportType(JMSC.MQJMS_TP_CLIENT_MQ_TCPIP);
	      cf.setQueueManager(dto.getQueueManager());
	      cf.setChannel(dto.getChannel());

	      MQTopicConnection connection = (MQTopicConnection) cf.createTopicConnection();
	      MQTopicSession session = (MQTopicSession) connection.createTopicSession(false, Session.AUTO_ACKNOWLEDGE);
	      MQTopic topic = (MQTopic) session.createTopic(dto.getTopic());
	      MQTopicPublisher publisher =  (MQTopicPublisher) session.createPublisher(topic);
	      MQTopicSubscriber subscriber = (MQTopicSubscriber) session.createSubscriber(topic);      

	      JMSTextMessage message = (JMSTextMessage) session.createTextMessage(dto.getMessage());     

	      connection.start();

	      publisher.publish(message);

	      JMSMessage receivedMessage = (JMSMessage) subscriber.receive(10000);
	      dto.setResponse(receivedMessage.getJMSMessageID());

	      log.debug("IBM Queue Message Send : "+dto);
	      publisher.close();
	      subscriber.close();
	      session.close();
	      connection.close();

	      dto.setDone(true);
	 }catch (JMSException jmsex) {
	    	log.error("JMSException for IBM Queue : ",jmsex);
	    	dto.setDone(false);
	    }
	    catch (Exception ex) {
	    	log.error("Exception for IBM Queue : ",ex);
	    	dto.setDone(false);
	    }
	}
  
}
