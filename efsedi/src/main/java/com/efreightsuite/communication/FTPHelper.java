package com.efreightsuite.communication;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import lombok.extern.log4j.Log4j2;
import org.apache.commons.io.FileUtils;
import org.apache.commons.net.PrintCommandListener;
import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;

@Log4j2
public class FTPHelper {

	FTPClient ftpClient = null;
	
	FtpDto ftpDto = new FtpDto();

	public FTPHelper(FtpDto ftpDto) {

		ftpClient = new FTPClient();
		ftpClient.addProtocolCommandListener(new PrintCommandListener(new PrintWriter(System.out)));
		this.ftpDto = ftpDto;
		
		try {
			
			ftpClient.connect(ftpDto.getServer(), ftpDto.getPort());
			ftpClient.login(ftpDto.getUserName(), ftpDto.getPassword());
			ftpClient.setFileType(FTP.BINARY_FILE_TYPE);
			ftpClient.enterLocalPassiveMode();

		} catch (Exception exception) {
			log.error("Exception while connecting : ", exception);
		}

	}

	public void disconnect() {
		if (this.ftpClient.isConnected()) {
			try {
				this.ftpClient.logout();
				this.ftpClient.disconnect();
			} catch (IOException exception) {
				log.error("Exception while disconnecting : ", exception);
			}
		}
	}

	public boolean downloadFile(String remoteFilePath, String localFilePath) {
		boolean isSuccessful = true;
		try (FileOutputStream fileOutputStream = new FileOutputStream(localFilePath)) {
			isSuccessful = this.ftpClient.retrieveFile(remoteFilePath, fileOutputStream);
			log.info(localFilePath + " is downloaded from " + remoteFilePath);
		} catch (Exception exception) {
			log.error("Exception while downloading : ", exception);
			isSuccessful = false;
		}
		return isSuccessful;
	}
	
	
	public boolean listFiles(String remoteFilePath) {
		boolean isSuccessful = true;
		if(this.ftpClient.isConnected()) {
			try {
				String fileNames[] = ftpClient.listNames();
				for(String fileName : fileNames) {
					log.info(fileName);
				}
				isSuccessful = true;
			} catch (IOException e) {
				log.error("Exception in listFiles" + e);
				isSuccessful = false;
			}
		} 
		return isSuccessful;
	}
	
	
	public ArrayList<String> getFileNames(String remoteFilePath) {
		FTPFile[] ftpFiles;
		ArrayList<String> fileNames = new ArrayList<>();
		try {
			ftpFiles = ftpClient.listFiles(remoteFilePath);
			 for (FTPFile ftpFile : ftpFiles) {
		            // Check if FTPFile is a regular file
		            if (ftpFile.getType() == FTPFile.FILE_TYPE) {
		                // Check the file size, if the file size is not zero add it in to array.

		            	log.info("FTPFile: " + ftpFile.getName() + "; " + FileUtils.byteCountToDisplaySize(ftpFile.getSize()));
		            	
		            	if(ftpFile.getSize() > 0) {
		            		fileNames.add(ftpFile.getName());
		            	}
		            }
		        }
		} catch (IOException e) {
			log.error("Exception in getFileNames() method ", e);
		}
       
		return fileNames;
	}
	
	
	public boolean deleteFile(String remoteFilePath) {
		boolean isSuccessful = true;
		if(this.ftpClient.isConnected()) {
			try {
				isSuccessful = ftpClient.deleteFile(remoteFilePath);
			} catch (IOException e) {
				log.error("Exception in deleteFile() method ", e);
				isSuccessful = false;
			}
		}
		return isSuccessful;
	}
	
}
