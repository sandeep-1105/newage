package com.efreightsuite.winwebconnect;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class ElectronicSecurityDeclartion implements Serializable{/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	
	@JsonProperty("RegulatedAgentDetails")
	RegulatedAgentDetails regulatedAgentDetails;
	
	
	@JsonProperty("Screening")
	Screening screening;

	@JsonProperty("Exemption")
	Exemption  exemption;
	
	@JsonProperty("OtherRegulatedAgentDetails")
	OtherRegulatedAgentDetails otherRegulatedAgentDetails;

	@JsonProperty("AdditionalSecurityStatement")
	String[] additionalSecurityStatement;
	
}

