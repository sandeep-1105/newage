package com.efreightsuite.winwebconnect;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class HawbResponse implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@JsonProperty("HAWBNUMBER")
	String hawbNumber;

	@JsonProperty("StatusCode")
	String statusCode;
	
	@JsonProperty("Status")
	String status;
	
	@JsonProperty("Remarks")
	String remarks;
	
	
	

	
}
