package com.efreightsuite.winwebconnect;

import java.io.Serializable;

import lombok.Data;

import com.fasterxml.jackson.annotation.JsonProperty;

@Data
public class ReceivedStatus implements Serializable{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@JsonProperty("Received")
	String received;

	@JsonProperty("Carrier")
	String carrier;

	@JsonProperty("From")
	String from;

	@JsonProperty("Volume")
	Weight volume;

	@JsonProperty("Density")
	Density Density;

	

}
