package com.efreightsuite.winwebconnect;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class Cargo implements Serializable {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@JsonProperty("HSCode")
	HSCode hsCode;
	
	@JsonProperty("GoodsDescription")
	String goodsDescription;
	
	@JsonProperty("SpecialHandling")
	SpecialHandling specialHandling;
}
