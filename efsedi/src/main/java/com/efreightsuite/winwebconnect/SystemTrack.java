package com.efreightsuite.winwebconnect;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class SystemTrack implements Serializable{

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 1L;

	@JsonProperty("AgentId")
	int agentId;
	
	@JsonProperty("AgentName")
	String agentName;
	
	@JsonProperty("ContactId")
	int contactId;
	
	@JsonProperty("ContactName")
	String contactName;
	
	@JsonProperty("Datetime")
	Date datetime;
	
}
