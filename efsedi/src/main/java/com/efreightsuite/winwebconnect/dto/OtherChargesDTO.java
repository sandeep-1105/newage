package com.efreightsuite.winwebconnect.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

public class OtherChargesDTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
/*
*/

	@JsonProperty("Amount")
	private Double amount;

	@JsonProperty("Description")
	private String description;
	
	@JsonProperty("ChargeIdentifier")
	private String chargeIdentifier;
	
	@JsonProperty("PrepaidCollect")
	private String prepaidCollect;
	
}