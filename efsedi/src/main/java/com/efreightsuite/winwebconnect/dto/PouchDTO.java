package com.efreightsuite.winwebconnect.dto;

import java.io.Serializable;
import java.util.List;

import lombok.Data;

import com.efreightsuite.winwebconnect.Hawb;
import com.efreightsuite.winwebconnect.Mawb;

/*
 * Pouch Model will enable sending Master & Manifest 
 * (House Air Waybills) data to airline electronically
 * 
 */
@Data
public class PouchDTO implements Serializable{

	/**
	 * Serial version id
	 */
	private static final long serialVersionUID = 1L;

	//Maps  mawb -class 
	public Mawb mawb;
	
	
	//Maps  hawb-class 
	public List<Hawb> hawb;
	
	
	
}
