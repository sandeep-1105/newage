package com.efreightsuite.winwebconnect.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class RatesDTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@JsonProperty("NumberOfPieces")
	private int numberOfPieces;

	@JsonProperty("GrossWeight")
	private ValueUmoDTO grossWeight;
	
	@JsonProperty("Volume")
	private ValueUmoDTO volume;

	@JsonProperty("SLAC")
	private int slac;

	@JsonProperty("RateClassCode")
	private String rateClassCode;
	
	@JsonProperty("CommodityItemNumber")
	private String commodityItemNumber;
	
	@JsonProperty("ChargeableWeight")
	private Double chargeableWeight;
	
	@JsonProperty("RateOrCharge")
	private Double rateOrCharge;

	@JsonProperty("NatureAndQuantityOfGoods")
	private String natureAndQuantityOfGoods;
	
	@JsonProperty("Dims")
	private List<DimensionDTO> dims = new ArrayList<>();
	
	@JsonProperty("ULD")
	private List<StringNumberDTO> uld = new ArrayList<>();
	
	
	
	
	
}
