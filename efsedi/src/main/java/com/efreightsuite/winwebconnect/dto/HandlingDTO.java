package com.efreightsuite.winwebconnect.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class HandlingDTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
/*"Handling": {
	"SpecialServiceInformation": "03 pcs marked and
	addressed please inform consignee upon arrival, Docs
	attached",
	"OtherServiceInformation": null,
	"SCI": "IN",
	"SpecialHandling": [
		{
		"Code": "BUP"
		}
	]
	}*/
	
	@JsonProperty("SpecialServiceInformation")
	private String SpecialServiceInformation;
	
	@JsonProperty("OtherServiceInformation")
	private String OtherServiceInformation;
	
	@JsonProperty("SCI")
	private String SCI;
	
	@JsonProperty("SpecialHandling")
	private List<CodeDTO> SpecialHandling = new ArrayList<>();
 
}
