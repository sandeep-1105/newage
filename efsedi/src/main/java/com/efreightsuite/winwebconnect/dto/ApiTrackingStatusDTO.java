package com.efreightsuite.winwebconnect.dto;

import java.io.Serializable;

import lombok.Data;

import com.efreightsuite.winwebconnect.AirWayBill;
import com.fasterxml.jackson.annotation.JsonProperty;

@Data
public class ApiTrackingStatusDTO implements Serializable{/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@JsonProperty("AgentId")
	Long agentId;
	
	@JsonProperty("AirwayBill")
    AirWayBill airwayBill;
	
	
}
