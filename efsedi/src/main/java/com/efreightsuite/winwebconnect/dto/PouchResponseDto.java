package com.efreightsuite.winwebconnect.dto;

import java.io.Serializable;
import java.util.List;

import lombok.Data;

import com.efreightsuite.winwebconnect.Remarks;
import com.efreightsuite.winwebconnect.SystemTrack;
import com.fasterxml.jackson.annotation.JsonProperty;

@Data
public class PouchResponseDto implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	//unique WIN
	@JsonProperty("AwbId")
	Long  awbID;
	
	@JsonProperty("TransactionID")
	int transactionID;
	
	@JsonProperty("MawbID")
	Long mawbId;
	
	@JsonProperty("AgentID")
	int agentId;
	
	@JsonProperty("AWBNumber")
	String awbNumber;
	
	@JsonProperty("WINStatus")
	String winStatus;
	
	@JsonProperty("Remarks")
	List<Remarks> remarks;
	
	@JsonProperty("")
	String authToken;
	
	@JsonProperty("")
    SystemTrack  created;
	
	@JsonProperty("")
   SystemTrack updated;
}
