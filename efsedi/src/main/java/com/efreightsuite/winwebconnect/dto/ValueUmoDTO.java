package com.efreightsuite.winwebconnect.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ValueUmoDTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
/*"Handling": {
	"Value": 8.24,
"UOM": "MC"
	}*/
	
	@JsonProperty("Value")
	private String Value;
	
	@JsonProperty("UOM")
	private String UOM;
	
}
