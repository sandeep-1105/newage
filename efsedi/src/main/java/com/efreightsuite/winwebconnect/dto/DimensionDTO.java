package com.efreightsuite.winwebconnect.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

public class DimensionDTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
/*
 * {
"Pcs": 2,
"Length": 122,
"Width": 102,
"Height": 150,
"UOM": "CMT"
}
*/

	@JsonProperty("Pcs")
	private int pcs;
	
	@JsonProperty("Length")
	private int length;

	@JsonProperty("Width")
	private int width;

	@JsonProperty("Height")
	private int height;
	
	@JsonProperty("UOM")
	private int uom;
	
	
}
