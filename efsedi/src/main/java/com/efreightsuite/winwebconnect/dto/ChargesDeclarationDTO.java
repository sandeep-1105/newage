package com.efreightsuite.winwebconnect.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ChargesDeclarationDTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
/*
	"ChargesDeclaration": {
		"CurrencyCode": "INR",
		"ChargeCode": "PP",
		"WeightOrValuation": "P",
		"Other": "P",
		"ValuesForCarriage": 280000,
		"ValuesForCustom": 280000,
		"ValuesForInsurance": 280000
		}*/
	
	@JsonProperty("CurrencyCode")
	private String currencyCode;
	
	@JsonProperty("ChargeCode")
	private String chargeCode;
	
	@JsonProperty("WeightOrValuation")
	private String weightOrValuation;
	
	@JsonProperty("Other")
	private String other;
	
	@JsonProperty("ValuesForCarriage")
	private String valuesForCarriage;
	
	@JsonProperty("ValuesForCustom")
	private String valuesForCustom;
	
	@JsonProperty("ValuesForInsurance")
	private String valuesForInsurance;
 
}
