package com.efreightsuite.winwebconnect.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

public class OtherCustomsDTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
/*
*/

	@JsonProperty("Country")
	private CodeNameDTO country;

	@JsonProperty("InformationIdentifier")
	private String informationIdentifier;
	
	@JsonProperty("controlIdentifier")
	private String ControlIdentifier;
	
	@JsonProperty("SupplementControlInformation")
	private String supplementControlInformation;
	
	
	
}