package com.efreightsuite.winwebconnect.dto;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class ArrayOfApiTrackingStatusDTO implements Serializable{

	 
	/**
	 * serialVersionUID = 1L;
	 */
	private static final long serialVersionUID = 1L;
	
	
	@JsonProperty("ApiTrackingStatusDTO")
	public List<ApiTrackingStatusDTO> apiTrackingStatusDTO;
	
	
}
