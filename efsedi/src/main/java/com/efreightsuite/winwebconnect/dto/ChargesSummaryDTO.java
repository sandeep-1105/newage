package com.efreightsuite.winwebconnect.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ChargesSummaryDTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
/*
*/

	@JsonProperty("WeightCharge")
	private Double weightCharge;

	@JsonProperty("ValuationCharge")
	private Double valuationCharge;

	@JsonProperty("Taxes")
	private Double taxes;
	
}