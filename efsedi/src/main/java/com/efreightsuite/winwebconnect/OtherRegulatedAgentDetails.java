package com.efreightsuite.winwebconnect;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class OtherRegulatedAgentDetails implements Serializable{/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

@JsonProperty("Identifier")
	String identifier;
	
@JsonProperty("Country")
	Country country;
	
@JsonProperty("ReferenceInformation")
	String referenceInformation;
	
@JsonProperty("ExpiryDate")
	Date expiryDate;
	
	
}
