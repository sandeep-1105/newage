package com.efreightsuite.winwebconnect;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class Hawbs implements Serializable{
	
	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 1L;

	@JsonProperty("HawbId")
	int hawbId;
	
	@JsonProperty("HawbNumber")
	String hawbNumber;
	
	@JsonProperty("Status")
	String Status;
	
	
	
}
