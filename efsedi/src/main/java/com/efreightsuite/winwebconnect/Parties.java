package com.efreightsuite.winwebconnect;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class Parties implements Serializable{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

@JsonProperty("Agent")
	Party agent;

@JsonProperty("Shipper")
	Party shipper;

@JsonProperty("Consignee")
	Party consignee;

@JsonProperty("Notify")
	Party notify;
	
@JsonProperty("CoLoader")
	Party coLoader;
	
	
}
