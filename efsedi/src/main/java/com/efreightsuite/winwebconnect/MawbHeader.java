package com.efreightsuite.winwebconnect;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

/*
 * 
 * 
 */
@Data
public class MawbHeader implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@JsonProperty("AwbNumber")
	private String awbNumber; 

	@JsonProperty("AwbStatus")
	private String awbStatus; 

	@JsonProperty("eAwb")
	private Boolean eAwb; 

	@JsonProperty("WithPaper")
	private Boolean withPaper; 
	
}
