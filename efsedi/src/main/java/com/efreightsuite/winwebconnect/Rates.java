package com.efreightsuite.winwebconnect;

import java.io.Serializable;
import java.util.List;

import lombok.Data;

import com.fasterxml.jackson.annotation.JsonProperty;

@Data
public class Rates implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@JsonProperty("NumberOfPieces")
	int numberOfPieces;
	
	@JsonProperty("GrossWeight")
	Weight grossWeight;
	
	@JsonProperty("Volume")
	Weight volume;
	
	@JsonProperty("SLAC")
	Integer slac;

	@JsonProperty("RateClassCode")
	String rateClassCode;
	
	@JsonProperty("CommodityItemNumber")
	String commodityItemNumber;
	
	@JsonProperty("ChargeableWeight")
	Double chargeableWeight;

	@JsonProperty("RateOrCharge")
	Double  rateOrCharge;
	
	@JsonProperty("ChargeAmount")
    Double chargeAmount;
    
	@JsonProperty("NatureAndQuantityOfGoods")
    String natureAndQuantityOfGoods;

	@JsonProperty("Dims")
    List<com.efreightsuite.winwebconnect.Dimension>dims;
    
	@JsonProperty("ULD")
    UnitLoadDevice uld;
    
}


