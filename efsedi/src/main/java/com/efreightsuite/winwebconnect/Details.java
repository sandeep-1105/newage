package com.efreightsuite.winwebconnect;

import java.io.Serializable;
import java.util.Date;

import lombok.Data;

import com.fasterxml.jackson.annotation.JsonProperty;

@Data
public class Details implements Serializable{

	@JsonProperty("StatusCode")
	String statusCode;
	
	
	@JsonProperty("StatusDesc")
	String statusDesc;
	
	@JsonProperty("FlightNumber")
	String FlightNumber;
	
	@JsonProperty("NumberOfPieces")
	String noOfPieces;
	
	@JsonProperty("Weight")
	Weight weight;
	
	@JsonProperty("StatusDateTime")
	String statusDateTime;
	
	@JsonProperty("PortOfArrival")
	Port portOfArrival;
	
	@JsonProperty("PortOfDeparture")
	Port portOfDeparture;
	
	@JsonProperty("ReceivedStatus")
	ReceivedStatus receivedStatus;
	
	@JsonProperty("DepartureInfo")
	TravelInfo departureInfo;
	
	@JsonProperty("ArrivalInfo")
	TravelInfo arrivalInfo;
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
