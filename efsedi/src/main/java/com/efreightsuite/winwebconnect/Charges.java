package com.efreightsuite.winwebconnect;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class Charges implements Serializable{/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@JsonProperty("CurrencyCode")
	String currencyCode;
	
	@JsonProperty("WeightOrValuation")
	String weightOrValuation;
	
	@JsonProperty("Other")
	String other;
	
	@JsonProperty("ValuesForCarriage")
	String valuesForCarriage;
	
	@JsonProperty("ValuesForCustom")
	Double valuesForCustom;
	
	@JsonProperty("ValuesForInsurance")
	Double valuesForInsurance;
	
}
