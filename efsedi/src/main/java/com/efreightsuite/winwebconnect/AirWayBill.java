package com.efreightsuite.winwebconnect;

import java.io.Serializable;
import java.util.List;

import lombok.Data;

import com.fasterxml.jackson.annotation.JsonProperty;

@Data
public class AirWayBill implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@JsonProperty("AwbID")
	Long awbID;
	
	@JsonProperty("AwbNumber")
	String awbNumber;
	
	@JsonProperty("AirlineName")
	String airlineName;
	
	@JsonProperty("MAWBStatusCode")
	String mawbStatusCode;
	
	@JsonProperty("MAWBStatus")
	String mawbStatus;
	
	@JsonProperty("MAWBRemarks")
	String mawbRemarks;
	
	@JsonProperty("Hawb")
	List<HawbResponse> hawb;
	
	
	@JsonProperty("Statuses")
	List<Statuses> statuses;
	
	
	
	
	
	
	
	
}
