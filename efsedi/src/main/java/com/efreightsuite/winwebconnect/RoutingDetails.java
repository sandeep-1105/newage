package com.efreightsuite.winwebconnect;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class RoutingDetails implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@JsonProperty("FromAirPortCode")
	String fromAirPortCode;

	@JsonProperty("ToAirPortCode")
	String toAirPortCode;
	
	@JsonProperty("CarrierCode")
	String carrierCode;
	
	@JsonProperty("FlightNumber")
	String flightNumber;
	
	@JsonProperty("FlightDate")
	String flightDate;
	
}
