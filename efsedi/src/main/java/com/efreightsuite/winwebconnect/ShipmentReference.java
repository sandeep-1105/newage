package com.efreightsuite.winwebconnect;

import java.io.Serializable;

import lombok.Data;

@Data
public class ShipmentReference implements Serializable{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	String referenceNumber;
	
	String additionalInformation;
	
	
}
