package com.efreightsuite.winwebconnect;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class OtherCustoms implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@JsonProperty("Country")
	Country country;
	
	@JsonProperty("InformationIdentifier")
	String informationIdentifier;
	
	@JsonProperty("ControlIdentifier")
	String controlIdentifier;
	
	@JsonProperty("SupplementControlInformation")
	String supplementControlInformation;
	
}
