package com.efreightsuite.winwebconnect;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class Mawb implements Serializable{

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 1L;

	/*@JsonProperty("MawbHeader")
	 MawbHeader mawbHeader;
	*/
	@JsonProperty("Parties")
	 Parties parties;
	
	@JsonProperty("Accounting")
	 List<Accounting> accounting;
	
	@JsonProperty("ShipmentReference")
	 ShipmentReference shipmentReference;
	
	@JsonProperty("RoutingDetails")
	 List<RoutingDetails> routingDetails ;
	
	@JsonProperty("ChargesDeclaration")
	 ChargesDeclaration chargesDeclartion;
	
	@JsonProperty("Handling")
	 Handling handling;
	
	@JsonProperty("Rates")
	 List<Rates> rates;
	
	@JsonProperty("ChargesSummary")
	 ChargesSummary chargesSummary;

	 @JsonProperty("OtherCharges")
	 List<OtherCharges> otherCharges;
	
	@JsonProperty("OptionalSection")
	 OptionalSection optionalSection;
	
	@JsonProperty("EmailNotifications")
	 List<EmailNotifications> emailNotifications;
	
	@JsonProperty("Execution")
	 Execution execution;
	

	@JsonProperty("AwbNumber")
	private String awbNumber; 

	@JsonProperty("AwbStatus")
	private String awbStatus; 

	@JsonProperty("eAwb")
	private Boolean eAwb; 

	@JsonProperty("WithPaper")
	private Boolean withPaper; 
	
		
	
}
