package com.efreightsuite.winwebconnect;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;


@Data
public class Party implements Serializable{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@JsonProperty("WinId")
	Integer winID;
	
	@JsonProperty("AccountNumber")
	Integer accountNumber;
	
	@JsonProperty("Name")
	String name;

	@JsonProperty("Phone")
	String phone;
	
	@JsonProperty("Fax")
	String fax;

	@JsonProperty("Email")
	String email;

	@JsonProperty("Address")
	Address address;

}
