package com.efreightsuite.winwebconnect;

import java.io.Serializable;

import lombok.Data;

import com.fasterxml.jackson.annotation.JsonProperty;

@Data
public class Port implements Serializable{

	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@JsonProperty("Code")
	String code;
	
	@JsonProperty("Name")
	String name;
	
}
