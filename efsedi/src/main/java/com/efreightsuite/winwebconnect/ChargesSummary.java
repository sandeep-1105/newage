package com.efreightsuite.winwebconnect;

import java.io.Serializable;

import lombok.Data;

import com.fasterxml.jackson.annotation.JsonProperty;

@Data
public class ChargesSummary implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@JsonProperty("WeightCharge")
	Double weightCharge;
	
	@JsonProperty("ValuationCharge")
	Double valuationCharge;
	
	@JsonProperty("Taxes")
	Double taxes;
	
	
}
