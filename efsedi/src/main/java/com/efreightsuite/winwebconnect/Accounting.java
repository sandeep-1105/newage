package com.efreightsuite.winwebconnect;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class Accounting implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@JsonProperty("Identifier")
	String identifier;
	
	@JsonProperty("Information")
	String information;

}
