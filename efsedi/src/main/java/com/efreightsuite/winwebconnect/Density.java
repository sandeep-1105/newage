package com.efreightsuite.winwebconnect;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class Density implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;


	@JsonProperty("Indicator")
	String indicator;
	
	
	@JsonProperty("Group")
	String group;
}
