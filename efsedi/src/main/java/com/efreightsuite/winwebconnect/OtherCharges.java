package com.efreightsuite.winwebconnect;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class OtherCharges implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@JsonProperty("Description")
	String description;

	@JsonProperty("Amount")
    Double amount;
    
	@JsonProperty("ChargeIdentifier")
    String chargeIdentifier;
    
	@JsonProperty("PrepaidCollect")
    String prepaidCollect;
	 
}
