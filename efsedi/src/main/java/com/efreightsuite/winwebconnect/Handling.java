package com.efreightsuite.winwebconnect;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class Handling implements Serializable {

	
 /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@JsonProperty("SpecialServiceInformation")
String specialServiceInformation;
 
	@JsonProperty("OtherServiceInformation")
 String otherServiceInformation;
 
	@JsonProperty("SCI")
 String sci;
 
	@JsonProperty("SpecialHandling")
 SpecialHandling specialHandling;

}
