package com.efreightsuite.winwebconnect;

import java.io.Serializable;

import lombok.Data;

import com.fasterxml.jackson.annotation.JsonProperty;

@Data
public class Dimension implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@JsonProperty("Pcs")
	int pcs;
	
	@JsonProperty("Length")
	int length;
	
	@JsonProperty("Width")
	int width;
	
	@JsonProperty("Height")
	int height;
	
	@JsonProperty("UOM")
	String uom;
	
}
