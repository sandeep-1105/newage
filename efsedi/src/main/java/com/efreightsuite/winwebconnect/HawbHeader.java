package com.efreightsuite.winwebconnect;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class HawbHeader implements Serializable{/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@JsonProperty("AwbNumber")
	String awbNumber;
	
	@JsonProperty("HawbNumber")
	String hawbNumber;
	
	@JsonProperty("HawbStatus")
	String hawbStatus;
	
	@JsonProperty("NumberOfPieces")
	int numberOfPieces;
	
	@JsonProperty("GrossWeight")
	Weight grossWeight;

	@JsonProperty("Locations")
	Locations locations;
	
	@JsonProperty("Commodity")
	String commodity;
	
	@JsonProperty("SLAC")
    int slac;	
	
}
