package com.efreightsuite.winwebconnect;

import java.io.Serializable;

import lombok.Data;

import com.fasterxml.jackson.annotation.JsonProperty;

@Data
public class Address implements Serializable{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@JsonProperty("Line1")
	String line1;
	
	@JsonProperty("Line2")
	String line2;
	
	@JsonProperty("Place")
	String place;

	@JsonProperty("City")
	City city;
	
	@JsonProperty("Country")
	Country country;
	
	
	@JsonProperty("PostalCode")
	String postalCode;
	
	@JsonProperty("StateProvince")
	String stateProvince;
	
}
