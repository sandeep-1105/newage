package com.efreightsuite.winwebconnect;

import java.io.Serializable;

import lombok.Data;

import com.fasterxml.jackson.annotation.JsonProperty;

@Data
public class ChargesDeclaration implements Serializable{

   /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@JsonProperty("CurrencyCode")
String currencyCode;	
   
	@JsonProperty("ChargeCode")
   String chargeCode;
   
	@JsonProperty("WeightOrValuation")
    char weightOrValuation;
   
	@JsonProperty("Other")
    char other;
	
	@JsonProperty("ValuesForCarriage")
	String valuesForCarriage;

	@JsonProperty("ValuesForCustom")
    Double valuesForCustom;
   
	@JsonProperty("ValuesForInsurance")
   Double valuesForInsurance;
   
}

