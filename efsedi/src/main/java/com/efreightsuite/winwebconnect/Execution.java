package com.efreightsuite.winwebconnect;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class Execution implements Serializable{

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 1L;
	
@JsonProperty("Date")
	String date;
	
@JsonProperty("Place")
	String place;
	
@JsonProperty("ShipperSignature")
	String shipperSignature;
	
@JsonProperty("CarrierSignature")
	String carrierSignature;
	

}
