package com.efreightsuite.winwebconnect;

import java.io.Serializable;
import java.util.List;

import lombok.Data;

import com.fasterxml.jackson.annotation.JsonProperty;

@Data
public class Statuses implements Serializable{/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@JsonProperty("AirportCode")
	String airportCode;
	
	@JsonProperty("Details")
	List<Details> details;
	
	
	
}
