package com.efreightsuite.winwebconnect;

import java.io.Serializable;

import lombok.Data;

@Data
public class LogOnModel implements Serializable{

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 1L;

	String username;
	
	String password;
	
}
