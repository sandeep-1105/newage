package com.efreightsuite.winwebconnect;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class Screening implements Serializable{/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@JsonProperty("Method")
   String method;
	
	@JsonProperty("OtherScreeningMethod")
	String otherScreeningMethod;
	
	@JsonProperty("IssuedBy")
	String issuedBy;
	
	@JsonProperty("DateTime")
	Date dateTime;
	
	
}
