package com.efreightsuite.winwebconnect;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class HawbParties implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@JsonProperty("Shipper")
	Party shipper;
	
	@JsonProperty("Consignee")
	Party consignee;


}
