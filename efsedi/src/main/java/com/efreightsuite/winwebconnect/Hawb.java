package com.efreightsuite.winwebconnect;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class Hawb implements Serializable{

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 1L;
	
/*  @JsonProperty("HawbHeader")
	 HawbHeader hawbHeader;*/
  
  
  @JsonProperty("AwbNumber")
	String awbNumber;
	
	@JsonProperty("HawbNumber")
	String hawbNumber;
	
	@JsonProperty("HawbStatus")
	String hawbStatus;
	
	@JsonProperty("NumberOfPieces")
	int numberOfPieces;
	
	@JsonProperty("GrossWeight")
	Weight grossWeight;

	@JsonProperty("Locations")
	Locations locations;
	
	@JsonProperty("Commodity")
	String commodity;
	
	@JsonProperty("SLAC")
  Integer slac;	
	
  @JsonProperty("Parties")
	 Parties parties;

  @JsonProperty("Charges")
	 Charges charges;

  @JsonProperty("Cargo")
	 Cargo cargo;
	
  @JsonProperty("Optional")
	 Optional optional;


}
