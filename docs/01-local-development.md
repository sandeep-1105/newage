# Local Development

The first rule of software development is that you should be able to run application locally on your machine without any help. This is critical for software development. You can't build software if you find it hard to run application locally. 

## Tools

1. IntelliJ Idea Community Edition
2. Git
3. Java 8
4. MySQL
5. Redis: If you use Windows, then you can download Redis from [here](https://github.com/MicrosoftArchive/redis).

## Steps to run application locally

To run application locally, these are the steps you need to perform. 

**Step 1** Clone the repository on your local machine by running the following command.

```
$ git clone git@github.com:newage-git/efreightsuite.git
```

**Step 2**: Import project to Intellij as a Gradle project

Once import is finished, change the value of `efs.resource.path` in application.yml to the path of `efs-resource` directory on your machine.

**Step 3** Build the project using Gradle wrapper

If you are Mac or Ubuntu then you can run following command:

```
$ ./gradlew clean build
```

If you are Windows user, then you can run following command

```
$ gradlew.bat clean build
```

The above will download Gradle and use it for building the project. Gradle will be installed only once. The best part of using wrapper is that you can easily upgrade Gradle versions and keep Gradle versions in sync.

**Step 4** Create efsdev database in MySQL

The MySQL installation assumes MySQL is installed on your machine. You are using root user with no password. Login into MySQL cli and create a database named efsdev.

```
$ mysql -u root
mysql> create database efsdev;
```

**Step 5** Start Redis on your machine

```
$ redis-server
```

**Step 6** Run the application. You might need to change name of the jar.

```
$ java -jar backend/core/build/libs/core-0.1.0.SNAPSHOT.jar
```

Application will be up at http://localhost:8080.

## Changes made to run application locally

1. In development we make use of `local` profile.
2. We moved Spring Boot to latest version 1.5.7
3. Moved `efs-resource/efs-data-source/application-local-datasource.yml` to `src/main/resources/application-local.yml`. This was required because Spring has removed `location` attribute from the @ConfigurationProperties annotation.
4. In `application-local.yml`, we have replaced Oracle configuration with MySQL.
5. We had to set `ddl-auto` to `create-drop` so that schema is created.
6. We had to set `globally_quoted_identifiers` property to true. This is required because some of the column names uses reserved words so schema generation fails.
7. In ActivityBPMNDeployment, we set schema update to `create-drop`.