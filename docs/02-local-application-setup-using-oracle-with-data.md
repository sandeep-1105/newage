# Application setup using Oracle

Setting up the application using Oracle database with data. Once application is setup and running, then we can sign-up new customers, login in to the application etc.

eFrieghtSuite(EFS) is a cloud application which provides software as a service (SAAS) to its customers. Steps to enable SAAS for the customer:

- Customer to [signup](http://localhost:8080/public/user_registration/user_registration.html) in EFS application.
- Customer gets email with activation key to activate the account.
- Once customer activated the account, customer will get notification and can login to the application.

EFS has multi-tenants architecture where new schema will be created once customer is on-boarded onto EFS application. Customer has to provide SAAS id, username and password to login to application.

## Tools

1. IntelliJ Idea Community Edition
2. Git
3. Java 8
4. Oracle 12c: Use [docker](https://hub.docker.com/r/sath89/oracle-12c/) image to setup it in system
5. Redis: If you use Windows, then you can download Redis from [here](https://github.com/MicrosoftArchive/redis).
6. SQL developer

## Steps to setup application

To setup application with Oracle, these are the steps you need to perform. 

**Step 1** Clone the repository on your local machine by running the following command.

```
$ git clone git@github.com:xebiaww/newage.git
```

**Step 2**: Import project to Intellij as a Gradle project

Once import is finished, change the value of `efs.resource.path` in application.yml to the path of `efs-resource` directory on your machine.

**Step 3** Build the project using Gradle wrapper

If you are Mac or Ubuntu then you can run following command:

```
$ ./gradlew clean build
```

If you are Windows user, then you can run following command

```
$ gradlew.bat clean build
```

The above will download Gradle and use it for building the project. Gradle will be installed only once. The best part of using wrapper is that you can easily upgrade Gradle versions and keep Gradle versions in sync.

**Step 4** Create efsdev database in Oracle

Install Oracle on your machine using docker image. Installation steps is [here](https://hub.docker.com/r/sath89/oracle-12c/) with default schema. Connect  default schema through SQL Developer and create a schema named efsdev by executing below query.

```
CREATE USER efsdev IDENTIFIED BY efsdev;
GRANT CONNECT TO efsdev;
GRANT CONNECT, RESOURCE, DBA TO efsdev;
```

**Step 5** Start Redis on your machine

```
$ redis-server
```

**Step 6** Configure Oracles's efsdev schema details in application-local.yml

- Configure to oracle database under multitenancy.tenants & newage.saas.based.jdbc

  ```
  dbtype: Oracle
  driver: oracle.jdbc.driver.OracleDriver
  url: jdbc:oracle:thin:@192.168.99.100:1521/xe
  username: efsdev
  password: efsdev
  maxtotal: 25
  maxidle: 1
  minidle: 0
  validationquery: SELECT 1 FROM DUAL
  validationquerytimeout: 300
  ```


- Configure to use Oracle dialect

  ```
  database-platform: org.hibernate.dialect.Oracle12cDialect
  ```


- Make globally_quoted_identifiers to false.

- In ActivityBPMNDeployment, we set schema update to `create-drop`.

- We have to set `ddl-auto` to `create-drop` so that schema is created.

- Enable new id generator mapping to true under spring.jpa

  ```yaml
  hibernate:
      use-new-id-generator-mappings: true
  ```

**Step 7** Enable multi-tenants architecture

```yaml
newage:
    saas:
        based:
            application:
                enable: true
```

**Step 8** Uncomment code CommonDbFileUploaderService.java -> loadCommonData() method

**Step 9** Comment the code SaasService.java -> createSaasProcess()  -> Line number 268 to 276.

**Step 10** Start the application with heap settings -Xms512m -Xmx1024m

**Step 11** Once application started, we need to load data using REST API http://localhost:8080/public/api/v1/upload/common_db_data

**Step 12** Insert one row using SQL developer

INSERT INTO "EFSDEV"."COMMON_LANGUAGE_MASTER" (ID, LANGUAGE_CODE, LANGUAGE_NAME, STATUS) VALUES ('1', 'EN', 'English', 'Yes')

**Step 13** [Sign-up](http://localhost:8080/public/user_registration/user_registration.html) the customers in application with required details.

**Step 14** Activate the account manually using REST API activation link http://localhost:8080/public/account_activation/account_activation_form.html?<LOCATION_SETUP_ID>

- LOCATION_SETUP_ID can be retrieved using below query

  ```
  SELECT ID FROM "EFSDEV"."LOCATION_SETUP" WHERE COMPANY_REG_NAME='<Company name given while signup>'
  ```


- Open below activation link in browser by passing LOCATION_SETUP_ID

  ```
  http://localhost:8080/public/account_activation/account_activation_form.html?<LOCATION_SETUP_ID>
  ```


- Get activation key from query and enter in the form and submit.

  ```
  SELECT ACTIVATION_KEY FROM "EFSDEV"."LOCATION_SETUP" WHERE COMPANY_REG_NAME='<Company name given while signup>'
  ```

- Get SAAS id from query. It requires in login.

  ```
  SELECT SAAS_ID FROM "EFSDEV"."LOCATION_SETUP" WHERE COMPANY_REG_NAME='<Company name given while signup>'
  ```

**Step 15** Activation will ~1 hr as it creates new schema for the customer and loads all required data.

**Step 16** Try login with SAAS Id, username and password.



Application will be up at http://localhost:8080/index.html#/login