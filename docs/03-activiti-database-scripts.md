# Activiti Database Scripts

Activiti team maintains database scripts for different databases in their Github repository.

For example, activiti version 6.0.0 script are here https://github.com/Activiti/Activiti/tree/6.x/modules/activiti-engine/src/main/resources/org/activiti/db

The same scripts also exists in the activiti-engine-6.0.0.jar as well.
