#!/usr/bin/env bash
set -e
docker-machine create --driver amazonec2 --amazonec2-open-port 9090 --amazonec2-instance-type t2.medium --amazonec2-region ap-south-1 aws-newage-app
eval $(docker-machine env aws-newage-app)
docker-machine scp -r -d efs-resource/ aws-newage-app:/home/ubuntu/efs-resources
export EFS_RESOURCE_HOST_PATH=/home/ubuntu/efs-resources
./gradlew clean build docker
docker-compose -f docker-compose.yml -f docker-compose.init-mysql-db.yml up -d
