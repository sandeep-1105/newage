package com.efreightsuite.constants;

public class EnquiryLogConstants {

    public static final String CUSTOMER_NAME = "customerName";
    public static final String ENQUIRY_NO = "enquiryNo";
    public static final String QUOTE_BY = "quoteBy";
    public static final String RECEIVED_ON = "receivedOn";
    public static final String QUOTATION_NO = "quotationNo";
    public static final String SALES_COORDINATE_NAME = "salesCoOrdinatorName";
    public static final String SALES_MAN_NAME = "salesmanName";
    public static final String LOGGED_ON_DATE = "loggedOnDate";
    public static final String LOGGED_BY_NAME = "loggedByName";
    public static final String STATUS = "status";

    private EnquiryLogConstants() {
    }

}
