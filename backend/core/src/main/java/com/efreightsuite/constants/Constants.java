package com.efreightsuite.constants;

public class Constants {

    public static final String US = "US";

    public static final String QUATATION_ID = "QuotationId";
    public static final String QUATATION_NO = "QuotationNo";
    public static final String QUATATION_AIR_EXPORT_ID = "QuotationAirExportId";
    public static final String QUOTE_TYPE = "quoteType";
    public static final String QUOTE_STATUS = "quoteStatus";
    public static final String QUOTATION_CANCELLED = "quotationCancelled";

    public static final String SHIPMENT_ID = "ShipmentId";
    public static final String SHIPMENT_SERVICE_ID = "ShipmentServiceId";
    public static final String SHIPMENT_UID = "shipmentUid";
    public static final String SHIPMENT_DATE = "shipmentDate";
    public static final String SHIPMENT_TASK_TO = "shipmentTaskTo";

    public static final String ENQUIRY_ID = "EnquiryId";
    public static final String ENQUIRY_AIR_EXPORT_ID = "EnquiryAirExportId";
    public static final String ENQUIRY_NO = "EnquiryNo";
    public static final String ENQUIRY_CANCELLED = "enquiryCancelled";

    public static final String CUSTOMER = "customer";
    public static final String CUSTOMER_SERVICE = "customerService";
    public static final String CUSTOMER_NAME = "customerName";

    public static final String SERVICE_NAME = "serviceName";
    public static final String ORIGIN_NAME = "originName";
    public static final String DESTINATION_NAME = "destinationName";
    public static final String GROUP_NAME = "groupName";

    public static final String CONSOLE_ID = "ConsolId";
    public static final String DIFF_GROUP = "diffGroup";
    public static final String DESTINATION = "destination";
    public static final String CARRIER = "carrier";
    public static final String FLIGHT_NO = "flightNo";
    public static final String MAWB = "mawb";
    public static final String ETD = "etd";
    public static final String ETA = "eta";
    public static final String TRADE = "Trade";
    public static final String LOCATION = "Location";
    public static final String COMPANY = "Company";
    public static final String FIRST_STATE = "firstState";
    public static final String VALID_TO = "validTo";
    public static final String SALES_MAN = "salesman";
    public static final String OUR_PICK_UP = "ourpickup";
    public static final String PARENT_TASK = "parentTask";
    public static final String AES_GROUP = "aesGroup";
    public static final String PICK_UPDATE = "pickupdate";
    public static final String PICKUP_ACTUAL_DATE = "pickupActualDate";
    public static final String RECEIVED_ON_DATE = "receivedOnDate";
    public static final String QUOTE_BY_DATE = "quoteByDate";

    public static final String VALID_FROM = "validFrom";
    public static final String EXPIRES_ON = "expiresOn";
    public static final String REFERENCE_NUMBER = "referenceNumber";
    public static final String SHIPPER_NAME = "shipperName";
    public static final String SALES_COORDINATOR = "salesCoOrdinator";
    public static final String CREATED_ON = "createdOn";
    public static final String CREATED_BY = "createdBy";
    public static final String ORIGIN = "origin";
    public static final String APPROVED = "approved";

    public static final String QUOTATION_APPROVED_IN_WITH_OPEN_BRACET = " quotation.approved IN ('";
    public static final String CLOSING_BRACKET_WITH_INVERTED_COMMA = "') ";
    public static final String COMMA_SPACE = ", ";
    public static final String AND = " AND ";
    public static final String ORDER_BY = " ORDER BY ";
    public static final String _TRUE = "TRUE";
    public static final String _FALSE = "FALSE";

    public static final String ENQUIRY_LOWER_CASE = "enquiry";
    public static final String QUOTATION_LOWER_CASE = "quotation";
    public static final String SHIPMENT_LOWER_CASE = "shipment";
    public static final String PICKUP_LOWER_CASE = "pickup";
    public static final String DELIVERY_TO_CFS_LOWER_CASE = "deliverytocfs";
    public static final String AES_OR_CUSTOMS_LOWER_CASE = "aesorcustoms";
    public static final String LINK_TO_MASTER_LOWER_CASE = "linktomaster";
    public static final String SIGN_OFF_LOWER_CASE = "signoff";
    public static final String AIRLINE_SUB_LOWER_CASE = "airlinesubmission";
    public static final String JOB_COMPLETION_LOWER_CASE = "jobcompletion";
    public static final String INVOICE_LOWER_CASE = "invoice";
    public static final String ATD_CONF_LOWER_CASE = "atdconfirmation";

    public static final String COMMA = ",";

    public static final String ETA_START_DATE = "etaStartDate";
    public static final String ETA_END_DATE = "etaEndDate";
    public static final String ETD_START_DATE = "etdStartDate";
    public static final String ETD_END_DATE = "etdEndDate";
    public static final String SHIPMENT_MIN_CHRG_WEIGHT = "shipment.minimum.chargeable.weight";
    public static final String SERVICE_UID = "serviceUid";
    public static final String SERVICE_REQ_DATE = "serviceReqDate";
    public static final String CONSIGNEE_NAME = "consigneeName";
    public static final String LOCATION_NAME = "locationName";
    public static final String EXPORT_REF = "exportRef";
    public static final String HAWB_NO = "hawbNo";

    public static final String PROTECTED_FILE_NAME = "protectedFileName";
    public static final String UNPROTECTED_FILE_NAME = "unprotectedFileName";

    public static final String SAAS_ID_PREFIX = "EFS";
    public static final String ACTIVIT_ORACLE_SCRIPT_PATH_SUFFIX = "/efs-actitivi-script/oracle/";
    public static final String ACTIVIT_SQL_SCRIPT_PATH_SUFFIX="/efs-actitivi-script/mysql/";
    public static final String ACTIVIT_H2_SCRIPT_PATH_SUFFIX="/efs-actitivi-script/h2/";

}
