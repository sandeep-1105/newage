package com.efreightsuite.constants;


public class MatcherConstants {

    public static final String  CITY_CODE= "cityCode";
    public static final String  CITY_NAME= "cityName";
    public static final String  STATUS= "status";
    public static final String  VERSION_LOCK="versionLock";
    public static final String  LANGUAGE_NAME = "languageName";
    public static final String  LANGUAGE_CODE = "languageCode";
    public static final String  LANGUAGE_DESC = "languageDesc";
    public static final String  FLAGGED = "flagged";
    public static final String  GROUP_NAME = "groupName";
    public static final String  LOCATION_NAME = "locationName";
    public static final String  LOCATION_CODE = "locationCode";
    public static final String  BRANCH_NAME = "branchName";
}
