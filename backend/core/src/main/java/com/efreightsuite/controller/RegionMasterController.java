package com.efreightsuite.controller;

import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.RegionSearchDto;
import com.efreightsuite.dto.SearchRequest;
import com.efreightsuite.model.RegionMaster;
import com.efreightsuite.service.RegionMasterService;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/api/v1/regionmaster")
@Log4j2
public class RegionMasterController {


    @Autowired
    private
    RegionMasterService regionMasterService;

    /**
     * This method is used to get region details using ID.
     *
     * @param id This is the parameter to get details
     * @return region object .
     */
    @RequestMapping(value = "/get/id/{id}", method = RequestMethod.GET)
    @Secured("ROLE_MASTER.GENERAL.REGION_SECTOR.VIEW")
    public BaseDto getByPackId(@PathVariable Long id) {
        log.info("region controller get by id called" + id);
        return regionMasterService.getRegionById(id);
    }

    /**
     * This method is used to get entire region details for lov.
     *
     * @return region object .
     */
    @RequestMapping(value = "/get/search", method = RequestMethod.POST)
    @Secured("ROLE_MASTER.GENERAL.REGION_SECTOR.LIST")
    public BaseDto search(@RequestBody RegionSearchDto searchDto) {
        log.info("Search method is called...Search Dto: " + searchDto);
        return regionMasterService.search(searchDto);
    }

    /**
     * This method is used to get entire region details for lov.
     *
     * @return region object .
     */
    @RequestMapping(value = "/get/search/keyword", method = RequestMethod.POST)
    public BaseDto search(@RequestBody SearchRequest searchRequest) {
        log.info("Search method is called...Search Dto: " + searchRequest);
        return regionMasterService.search(searchRequest);
    }

    /**
     * This method is used to create region details .
     *
     * @return region object .
     */
    @RequestMapping(value = "/create", method = RequestMethod.POST)
    @Secured("ROLE_MASTER.GENERAL.REGION_SECTOR.CREATE")
    public BaseDto create(@RequestBody RegionMaster regionMaster) {
        log.info("Create method is called-RegionMaster");
        return regionMasterService.create(regionMaster);
    }

    /**
     * This method is used to update region details .
     *
     * @return region object .
     */
    @RequestMapping(value = "/update", method = RequestMethod.POST)
    @Secured("ROLE_MASTER.GENERAL.REGION_SECTOR.MODIFY")
    public BaseDto update(@RequestBody RegionMaster existingregionMaster) {

        log.info("Update method is called : " + existingregionMaster);

        return regionMasterService.update(existingregionMaster);
    }

    /**
     * This method is used to delete region details .
     *
     * @return .
     */

    @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
    @Secured("ROLE_MASTER.GENERAL.REGION_SECTOR.DELETE")
    public BaseDto delete(@PathVariable Long id) {

        log.info("Delete method is called.. : [" + id + "]");

        return regionMasterService.delete(id);
    }

}
