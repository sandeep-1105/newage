package com.efreightsuite.controller;

import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.CitySearchDto;
import com.efreightsuite.dto.SearchRequest;
import com.efreightsuite.model.CityMaster;
import com.efreightsuite.service.CityMasterService;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Log4j2
@RequestMapping(value = "/api/v1/citymaster")
public class CityMasterController {


    @Autowired
    private
    CityMasterService cityMasterService;

    @RequestMapping(value = "/get/search/keyword/{countryId}", method = RequestMethod.POST)
    public BaseDto getAll(@RequestBody SearchRequest searchRequest, @PathVariable Long countryId) {
        log.info("Search method called.");
        return cityMasterService.search(searchRequest, countryId);
    }

    @RequestMapping(value = "/get/search/keyword", method = RequestMethod.POST)
    @Secured("ROLE_MASTER.GENERAL.CITY.LIST")
    public BaseDto get(@RequestBody CitySearchDto citySearchDto) {
        log.info("Search method called.");
        return cityMasterService.search(citySearchDto);
    }


    @RequestMapping(value = "/get/id/{id}", method = RequestMethod.GET)
    @Secured("ROLE_MASTER.GENERAL.CITY.VIEW")
    public BaseDto get(@PathVariable Long id) {
        log.info("get method called." + id);
        return cityMasterService.get(id);
    }


    @RequestMapping(value = "/create", method = RequestMethod.POST)
    @Secured({"ROLE_MASTER.GENERAL.CITY.CREATE", "ROLE_MASTER.GENERAL.CITY.MODIFY"})
    public BaseDto create(@RequestBody CityMaster cityMaster) {
        log.info("Create method is called.");
        return cityMasterService.saveOrUpdate(cityMaster);
    }

    @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
    @Secured("ROLE_MASTER.GENERAL.CITY.DELETE")
    public BaseDto delete(@PathVariable Long id) {
        log.info("Delete method called....");
        return cityMasterService.delete(id);
    }
}
