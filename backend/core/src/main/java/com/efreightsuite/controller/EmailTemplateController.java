package com.efreightsuite.controller;

import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.EmailTemplateSearchDto;
import com.efreightsuite.model.EmailTemplate;
import com.efreightsuite.service.EmailTemplateService;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/api/v1/emailtemplate")
@Log4j2
public class EmailTemplateController {

    @Autowired
    private
    EmailTemplateService emailTemplateService;

    @RequestMapping(value = "/get/search", method = RequestMethod.POST)
    @Secured("ROLE_SETUP.EMAIL_TEMPLATE.LIST")
    public BaseDto search(@RequestBody EmailTemplateSearchDto dto) {
        return emailTemplateService.search(dto);
    }

    @RequestMapping(value = "/get", method = RequestMethod.GET)
    @Secured("ROLE_SETUP.EMAIL_TEMPLATE.LIST")
    public BaseDto getAll() {
        return emailTemplateService.getAll();
    }

    /**
     * Method - getById Returns EmailTemplate based on given pack id
     *
     * @return EmailTemplate
     */

    @RequestMapping(value = "/getbyid/{id}", method = RequestMethod.GET)
    public BaseDto getByEmailTemplateId(@PathVariable Long id) {

        log.info("getByEmailTemplateId method is called...ID = " + id);

        return emailTemplateService.getByEmailTemplateId(id);
    }

    /**
     * Method - create Creates and returns a new pack master
     *
     * @returns EmailTemplate
     */

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    @Secured("ROLE_SETUP.EMAIL_TEMPLATE.CREATE")
    public BaseDto create(@RequestBody EmailTemplate emailTemplate) {

        log.info("Create method is called.. EmailTemplate : " + emailTemplate);

        return emailTemplateService.create(emailTemplate);
    }

    /**
     * Method - update returns a updated pack master
     *
     * @param emailTemplate
     * @returns EmailTemplate
     */

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    @Secured("ROLE_SETUP.EMAIL_TEMPLATE.MODIFY")
    public BaseDto update(@RequestBody EmailTemplate emailTemplate) {

        log.info("Create method is called.. EmailTemplate : " + emailTemplate);

        return emailTemplateService.update(emailTemplate);
    }

}
