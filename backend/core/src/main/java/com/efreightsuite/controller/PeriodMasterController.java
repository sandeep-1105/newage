package com.efreightsuite.controller;

import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.model.PeriodMaster;
import com.efreightsuite.service.PeriodMasterService;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/api/v1/periodmaster")
@Log4j2
public class PeriodMasterController {

    @Autowired
    private
    PeriodMasterService periodMasterService;

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public BaseDto create(@RequestBody PeriodMaster periodMaster) {
        log.info("Create method is Invoked..");
        return periodMasterService.create(periodMaster);
    }


    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public BaseDto update(@RequestBody PeriodMaster existingPeriodMaster) {
        log.info("Create method is Invoked..: " + existingPeriodMaster);
        return periodMasterService.update(existingPeriodMaster);
    }

    @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
    public BaseDto delete(@PathVariable Long id) {
        log.info("Delete method is Invoked.. : [" + id + "]");
        return periodMasterService.delete(id);
    }

    @RequestMapping(value = "/getall", method = RequestMethod.GET)
    public BaseDto get() {
        log.info("getall method Invoked.");
        return periodMasterService.getAll();
    }

}
