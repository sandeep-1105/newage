package com.efreightsuite.controller;

import com.efreightsuite.dto.AesFilerSearchDto;
import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.model.AesFilerMaster;
import com.efreightsuite.service.AesFilerMasterService;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/api/v1/aesfilermaster")
@Log4j2
public class AesFilerMasterController {

    /**
     * AesFilerMasterService Object
     */

    @Autowired
    private
    AesFilerMasterService aesFilerMasterService;

    /**
     * Method - getById Returns AesFilerMaster based on given aes filer id
     *
     * @return AesFilerMaster
     */

    @RequestMapping(value = "/get/id/{id}", method = RequestMethod.GET)
    @Secured("ROLE_MASTER.GENERAL.AES_FILER.VIEW")
    public BaseDto getByAesFilerId(@PathVariable Long id) {

        log.info("getByAesFilerId method is called...ID = " + id);

        return aesFilerMasterService.getByAesFilerId(id);
    }

    /**
     * Method - search Returns Paginated Search Result based on Search Keyword
     *
     * @return SearchRespDto
     */

    @RequestMapping(value = "/get/search", method = RequestMethod.POST)
    @Secured("ROLE_MASTER.GENERAL.AES_FILER.LIST")
    public BaseDto search(@RequestBody AesFilerSearchDto searchDto) {

        log.info("Search method is called...Search Dto: " + searchDto);

        return aesFilerMasterService.search(searchDto);
    }

    /**
     * Method - create Creates and returns a new aes filer master
     *
     * @returns AesFilerMaster
     */

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    @Secured("ROLE_MASTER.GENERAL.AES_FILER.CREATE")
    public BaseDto create(@RequestBody AesFilerMaster aesFilerMaster) {

        log.info("Create method is called.. AesFilerMaster : " + aesFilerMaster);

        return aesFilerMasterService.create(aesFilerMaster);
    }

    /**
     * Method - update returns a updated aes filer master
     *
     * @param existingAesFilerMaster
     * @returns AesFilerMaster
     */

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    @Secured("ROLE_MASTER.GENERAL.AES_FILER.MODIFY")
    public BaseDto update(@RequestBody AesFilerMaster existingAesFilerMaster) {

        log.info("Create method is called.. AesFilerMaster : " + existingAesFilerMaster);

        return aesFilerMasterService.update(existingAesFilerMaster);
    }

    @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
    @Secured("ROLE_MASTER.GENERAL.AES_FILER.DELETE")
    public BaseDto delete(@PathVariable Long id) {

        log.info("Delete method is called.. : [" + id + "]");

        return aesFilerMasterService.delete(id);
    }
}
