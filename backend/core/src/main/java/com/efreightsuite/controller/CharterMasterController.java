package com.efreightsuite.controller;

import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.CharterSearchDto;
import com.efreightsuite.dto.SearchRequest;
import com.efreightsuite.model.CharterMaster;
import com.efreightsuite.service.CharterMasterService;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/api/v1/chartermaster")
@Log4j2
public class CharterMasterController {

    /**
     * CharterMasterService Object
     */

    @Autowired
    private
    CharterMasterService charterMasterService;

    /**
     * Method - getById Returns CharterMaster based on given charter id
     *
     * @return CharterMaster
     */

    @RequestMapping(value = "/get/id/{id}", method = RequestMethod.GET)
    public BaseDto getByCharterId(@PathVariable Long id) {

        log.info("getByCharterId method is called...ID = " + id);

        return charterMasterService.getByCharterId(id);
    }

    /**
     * Method - getByCharterCode Returns CharterMaster based on given charter code
     *
     * @return CharterMaster
     */

    @RequestMapping(value = "/get/code/{charterCode}", method = RequestMethod.GET)
    public BaseDto getByCharterCode(@PathVariable String charterCode) {

        log.info("getByCharterCode method is called...CODE = " + charterCode);

        return charterMasterService.getByCharterCode(charterCode);
    }

    /**
     * Method - search Returns Paginated Search Result based on Search Keyword
     *
     * @return SearchRespDto
     */

    @RequestMapping(value = "/get/search", method = RequestMethod.POST)
    public BaseDto search(@RequestBody CharterSearchDto searchDto) {

        log.info("Search method is called...Search Dto: " + searchDto);

        return charterMasterService.search(searchDto);
    }

    @RequestMapping(value = "/get/search/keyword", method = RequestMethod.POST)
    public BaseDto search(@RequestBody SearchRequest searchRequest) {

        log.info("Search method is called...Search Dto: " + searchRequest);

        return charterMasterService.search(searchRequest);
    }

    /**
     * Method - create Creates and returns a new charter master
     *
     * @returns CharterMaster
     */

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public BaseDto create(@RequestBody CharterMaster charterMaster) {

        log.info("Create method is called.. CharterMaster : " + charterMaster);

        return charterMasterService.create(charterMaster);
    }

    /**
     * Method - update returns a updated charter master
     *
     * @param existingCharterMaster
     * @returns CharterMaster
     */

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public BaseDto update(@RequestBody CharterMaster existingCharterMaster) {

        log.info("Create method is called.. CharterMaster : " + existingCharterMaster);

        return charterMasterService.update(existingCharterMaster);
    }

    @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
    public BaseDto delete(@PathVariable Long id) {

        log.info("Delete method is called.. : [" + id + "]");

        return charterMasterService.delete(id);
    }
}
