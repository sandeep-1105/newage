package com.efreightsuite.controller;

import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.BuyerConsolidationSearchDto;
import com.efreightsuite.model.BuyerConsolidationMaster;
import com.efreightsuite.service.BuyerConsolidationMasterService;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Sandhanapandian
 */

@RestController
@Log4j2
@RequestMapping(value = "/api/v1/bcmaster")

public class BuyerConsolidationMasterController {

    @Autowired
    private
    BuyerConsolidationMasterService buyerConsolidationMasterService;

    @RequestMapping(value = "/search", method = RequestMethod.POST)
    @Secured("ROLE_MASTER.CRM.BUYER_CONSOLIDATION.LIST")
    public BaseDto searchByCriteria(@RequestBody BuyerConsolidationSearchDto searchDto) {
        log.info("Search method called.");
        return buyerConsolidationMasterService.searchByCriteria(searchDto);
    }

    @RequestMapping(value = "/search", method = RequestMethod.PUT)
    public BaseDto searchByKeword(@RequestBody BuyerConsolidationSearchDto searchDto) {
        log.info("Search method called.");
        return buyerConsolidationMasterService.searchByKeword(searchDto);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @Secured("ROLE_MASTER.CRM.BUYER_CONSOLIDATION.VIEW")
    public BaseDto getById(@PathVariable Long id) {
        log.info("getByTosId method called..[" + id + "]");
        return buyerConsolidationMasterService.get(id);
    }

    @RequestMapping(method = RequestMethod.POST)
    @Secured("ROLE_MASTER.CRM.BUYER_CONSOLIDATION.CREATE")
    public BaseDto create(@RequestBody BuyerConsolidationMaster buyerConsolidationMaster) {
        log.info("Create method is called..");
        return buyerConsolidationMasterService.create(buyerConsolidationMaster);
    }

    @RequestMapping(method = RequestMethod.PUT)
    @Secured("ROLE_MASTER.CRM.BUYER_CONSOLIDATION.MODIFY")
    public BaseDto update(@RequestBody BuyerConsolidationMaster buyerConsolidationMaster) {
        log.info("Update method is called..[" + buyerConsolidationMaster.getId() + "]");
        return buyerConsolidationMasterService.update(buyerConsolidationMaster);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @Secured("ROLE_MASTER.CRM.BUYER_CONSOLIDATION.DELETE")
    public BaseDto delete(@PathVariable Long id) {
        log.info("Delete method called....[" + id + "]");
        return buyerConsolidationMasterService.delete(id);
    }

    @RequestMapping(value = "/getall", method = RequestMethod.GET)
    public BaseDto get() {
        log.info("getall method called.");
        return buyerConsolidationMasterService.getAll();
    }

}
