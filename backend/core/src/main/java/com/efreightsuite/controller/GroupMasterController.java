package com.efreightsuite.controller;

import java.util.List;

import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.GroupSubGroupDto;
import com.efreightsuite.model.GeneralLedgerGroup;
import com.efreightsuite.service.GroupMasterService;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Log4j2
@RequestMapping(value = "/api/v1/groupmaster")
public class GroupMasterController {

    @Autowired
    private
    GroupMasterService groupMasterService;


    @RequestMapping(value = "/get/id/{id}", method = RequestMethod.GET)
    @Secured("ROLE_MASTER.GENERAL.GROUP.VIEW")
    public BaseDto getByGroupId(@PathVariable Long id) {
        log.info("GroupMaster getID method Controller is called..");
        return groupMasterService.get(id);
    }

    @RequestMapping(value = "/get/search/keyword", method = RequestMethod.POST)
    @Secured("ROLE_MASTER.GENERAL.GROUP.LIST")
    public BaseDto search(@RequestBody GroupSubGroupDto searchDto) {
        log.info("Group Master Search method Controller is called.");
        return groupMasterService.search(searchDto);
    }

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    @Secured("ROLE_MASTER.GENERAL.GROUP.CREATE")
    public BaseDto create(@RequestBody List<GeneralLedgerGroup> GroupMaster) {
        log.info("Group Master Create method Controller is called..");
        return groupMasterService.create(GroupMaster);
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    @Secured("ROLE_MASTER.GENERAL.GROUP.MODIFY")
    public BaseDto update(@RequestBody List<GeneralLedgerGroup> GroupMaster) {
        log.info("Group Master Update method Controller is called..");
        return groupMasterService.update(GroupMaster);
    }

    @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
    @Secured("ROLE_MASTER.GENERAL.GROUP.DELETE")
    public BaseDto delete(@PathVariable Long id) {
        log.info("Group Master Delete method Controller is called..");
        return groupMasterService.delete(id);
    }


}
