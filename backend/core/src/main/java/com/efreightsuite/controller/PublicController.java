package com.efreightsuite.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.DocumentDetailSearchDto;
import com.efreightsuite.dto.FileUploadDto;
import com.efreightsuite.dto.QuotationApproveDto;
import com.efreightsuite.dto.SearchRequest;
import com.efreightsuite.model.Consol;
import com.efreightsuite.model.EnquiryLog;
import com.efreightsuite.model.InvoiceCreditNote;
import com.efreightsuite.model.PartyMaster;
import com.efreightsuite.model.Provisional;
import com.efreightsuite.model.Quotation;
import com.efreightsuite.model.RateRequest;
import com.efreightsuite.model.Shipment;
import com.efreightsuite.service.*;
import com.efreightsuite.service.common.CommonDbFileUploaderService;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/public/api/v1/")
@Log4j2
public class PublicController {

    @Autowired
    private
    QuotationService quotationService;

    @Autowired
    private
    PublicService publicService;

    @Autowired
    private
    EnquiryLogService enquiryLogService;

    @Autowired
    private
    ShipmentService shipmentService;

    @Autowired
    private
    ChargeMasterService chargeMasterService;

    @Autowired
    private
    UnitMasterService unitMasterService;

    @Autowired
    private
    CurrencyMasterService currencyMasterService;

    @Autowired
    private
    LanguageDetailService languageDetailService;

    @Autowired
    private
    CountryMasterService countryMasterService;

    @Autowired
    private
    CityMasterService cityMasterService;

    @Autowired
    private
    StateMasterService stateMasterService;

    @Autowired
    private
    ServiceMasterService serviceMasterService;

    @Autowired
    private
    LanguageService languageService;

    @Autowired
    private
    LocationMasterService locationMasterService;

    @Autowired
    private
    CategoryMasterService categoryMasterService;

    @Autowired
    private
    PartyMasterService partyMasterService;

    @Autowired
    private
    DaybookMasterService daybookMasterService;

    @Autowired
    private
    ConsolService consolService;

    @Autowired
    private
    InvoiceCreditNoteService invoiceCreditNoteService;

    @Autowired
    private
    ProvisionalService provisionalService;

    @Autowired
    private
    CommonDbFileUploaderService commonDbFileUploaderService;

    @RequestMapping(value = "/quotation/{quotationId}", method = RequestMethod.GET)
    public BaseDto quotationCustomerApproval(@PathVariable Long quotationId) {
        log.info("quotationCustomerApproval method is called.");
        return quotationService.get(quotationId, null);
    }

    @RequestMapping(value = "/document/{documentId}", method = RequestMethod.GET)
    public BaseDto getDocumentDetail(@PathVariable Long documentId) {
        log.info("getDocumentDetail method is called.");
        return publicService.getDocumentDetail(documentId);
    }

    @RequestMapping(value = "/shipmentservice/{serviceUid}", method = RequestMethod.GET)
    public BaseDto getShipmentServiceDetail(@PathVariable String serviceUid) {
        log.info("getShipmentServiceDetail method is called.");
        return shipmentService.getService(serviceUid);
    }

    @RequestMapping(value = "/quotation/party/approval", method = RequestMethod.POST)
    public BaseDto quotationCustomerApproval(@RequestBody QuotationApproveDto dto, HttpServletRequest request) {
        log.info("quotationCustomerApproval method is called.");
        return quotationService.quotationCustomerApproval(dto, request);
    }

    @RequestMapping(value = "/hawb/party/confirmation", method = RequestMethod.POST)
    public BaseDto confirmHawbByCustomer(@RequestBody DocumentDetailSearchDto dto, HttpServletRequest request) {
        log.info("confirmHawbByCustomer method is called.");
        return shipmentService.confirmHawbByCustomer(dto, request);
    }

    @RequestMapping(value = "/nls/{language}", method = RequestMethod.GET)
    public BaseDto getNLS(@PathVariable String language) {
        log.info("LanguageDetailController.createDefault is called.");
        return languageDetailService.getNls(language);
    }


    //Enquiry-Rate Request-Api

    @RequestMapping(value = "/enquiry/raterequest/{rateRequestId}", method = RequestMethod.GET)
    public BaseDto getRateRequest(@PathVariable Long rateRequestId) {
        log.info("RateRequest public controller method is called.");
        return publicService.getRateRequest(rateRequestId);
    }


    @RequestMapping(value = "/enquiry/enquirydetail/{rateRequestId}", method = RequestMethod.GET)
    public BaseDto getEnquiryDetail(@PathVariable Long rateRequestId) {
        log.info("RateRequest enquiry detail public controller method is called.");
        return publicService.getEnquiryDetail(rateRequestId);
    }

    @RequestMapping(value = "/enquiry/enquirystatus/{enquiryNo}", method = RequestMethod.GET)
    public BaseDto getEnquiryDetail(@PathVariable String enquiryNo) {
        log.info("rate request enquiry status checking.");
        return publicService.statusOfEnquiry(enquiryNo);
    }

    @RequestMapping(value = "/chargemaster/get/search/keyword", method = RequestMethod.POST)
    public BaseDto searchCharge(@RequestBody SearchRequest searchRequest) {
        log.info("search method called." + searchRequest.getRecordPerPage());
        return chargeMasterService.search(searchRequest);
    }

    @RequestMapping(value = "/unitmaster/get/search/keyword", method = RequestMethod.POST)
    public BaseDto searchUnit(@RequestBody SearchRequest searchRequest) {
        log.info("Search method called.");
        return unitMasterService.search(searchRequest);
    }

    @RequestMapping(value = "/currencymaster/get/search/keyword", method = RequestMethod.POST)
    public BaseDto searchCurrency(@RequestBody SearchRequest searchRequest) {
        log.info("Search method called.[" + searchRequest + "]");
        return currencyMasterService.search(searchRequest);
    }


    @RequestMapping(value = "/countrymaster/get/search/keyword", method = RequestMethod.POST)
    public BaseDto getAllCountries(@RequestBody SearchRequest searchRequest) {
        return countryMasterService.search(searchRequest);
    }

    @RequestMapping(value = "/common/countrymaster/get/search/keyword", method = RequestMethod.POST)
    public ResponseEntity<BaseDto> searchCountries(@RequestBody SearchRequest searchRequest) {
        return publicService.searchCommonCountries(searchRequest);
    }

    @RequestMapping(value = "/common/citymaster/get/search/keyword/{countryId}/{stateId}", method = RequestMethod.POST)
    public ResponseEntity<BaseDto> getAllCommonCities(@RequestBody SearchRequest searchRequest, @PathVariable Long countryId, @PathVariable Long stateId) {
        return publicService.searchCommonCities(searchRequest, countryId, stateId);
    }

    @RequestMapping(value = "/common/statemaster/get/search/keyword/{countryId}", method = RequestMethod.POST)
    public ResponseEntity<BaseDto> getAllCommonStates(@RequestBody SearchRequest searchRequest, @PathVariable Long countryId) {
        return publicService.searchCommonStates(searchRequest, countryId);
    }

    @RequestMapping(value = "/common/regionmaster/get/search/keyword", method = RequestMethod.POST)
    public ResponseEntity<BaseDto> getAllCommonRegions(@RequestBody SearchRequest searchRequest) {
        return publicService.searchCommonRegions(searchRequest);
    }

    @RequestMapping(value = "/common/documenttype/get/search/keyword", method = RequestMethod.POST)
    public BaseDto getAllCommonDocumentType(@RequestBody SearchRequest searchRequest) {
        return publicService.searchCommonDocumentType(searchRequest);
    }

    @RequestMapping(value = "/common/timezone/get/search/keyword", method = RequestMethod.POST)
    public BaseDto getAllCommonTimeZone(@RequestBody SearchRequest searchRequest) {
        return publicService.searchCommonTimeZone(searchRequest);
    }

    @RequestMapping(value = "/common/servicemaster/get/all", method = RequestMethod.GET)
    public BaseDto getAllCommonServices() {
        return publicService.getAllCommonServices();
    }

    @RequestMapping(value = "/common/servicemaster/get/search/exclude", method = RequestMethod.POST)
    public BaseDto searchServiceNotInList(@RequestBody SearchRequest searchRequest) {
        log.info("Search method called. :");
        return publicService.searchCommonServiceNotInList(searchRequest);
    }

    @RequestMapping(value = "/citymaster/get/search/keyword/{countryId}", method = RequestMethod.POST)
    public BaseDto getAllCities(@RequestBody SearchRequest searchRequest, @PathVariable Long countryId) {
        return cityMasterService.search(searchRequest, countryId);
    }


    @RequestMapping(value = "/statemaster/get/search/keyword/{countryId}", method = RequestMethod.POST)
    public BaseDto getAllStates(@RequestBody SearchRequest searchRequest, @PathVariable Long countryId) {
        return stateMasterService.search(searchRequest, countryId);
    }


    @RequestMapping(value = "/servicemaster/get/search/keyword", method = RequestMethod.POST)
    public BaseDto getAllService(@RequestBody SearchRequest searchRequest) {
        log.info("getAllService is called...");
        return serviceMasterService.search(searchRequest);
    }


    @RequestMapping(value = "/language/get/search/keyword", method = RequestMethod.POST)
    public BaseDto getAllLanguages(@RequestBody SearchRequest searchRequest) {
        log.info("getAllLanguages is called");
        return languageService.search(searchRequest);
    }

    @RequestMapping(value = "timezones/get", method = RequestMethod.GET)
    public BaseDto getTimeZones() {
        return locationMasterService.getTimeZoneList();
    }


    @RequestMapping(value = "/unitmaster/get/code/{unitCode}", method = RequestMethod.GET)
    public BaseDto getUnitCode(@PathVariable String unitCode) {
        log.info("get method called[" + unitCode + "]");
        return unitMasterService.getByUnitCode(unitCode);
    }

    @RequestMapping(value = "/enquiry/enquirydetail/save", method = RequestMethod.POST)
    public BaseDto getUnitCode(@RequestBody RateRequest rq, HttpServletRequest httpServletRequest) {
        return enquiryLogService.saveEnquiryDetail(rq, httpServletRequest);
    }


    @RequestMapping(value = "/logoByLocation/{locationId}", method = RequestMethod.GET)
    public BaseDto getLogoByLocation(@PathVariable Long locationId) {
        log.info("getLogoByLocation method is called.");
        return publicService.getLogoByLocation(locationId);
    }

    @RequestMapping(value = "/servicemaster/search/keyword", method = RequestMethod.POST)
    public BaseDto searchServiceMaster(@RequestBody SearchRequest searchRequest) {
        return serviceMasterService.search(searchRequest);
    }

    @RequestMapping(value = "/categoryMaster/search/keyword", method = RequestMethod.POST)
    public BaseDto searchCategoryMaster(@RequestBody SearchRequest searchRequest) {
        log.info("Search method called.");
        return categoryMasterService.search(searchRequest);
    }


    @RequestMapping(value = "/download/template/{type}", method = RequestMethod.GET)
    public void downloadTemplate(HttpServletResponse response, @PathVariable String type) {
        log.info("Download Template method called.");
        publicService.downloadTemplate(response, type);
    }

    @RequestMapping(value = "/daybook/search", method = RequestMethod.POST)
    public BaseDto daybookSearch(@RequestBody SearchRequest searchRequest) {
        log.info("daybookSearch method is called...Search Dto: " + searchRequest);
        return daybookMasterService.search(searchRequest);
    }

    @RequestMapping(value = "/common/currencymaster/get/search/keyword", method = RequestMethod.POST)
    public BaseDto searchCommonCurrency(@RequestBody SearchRequest searchRequest) {
        log.info("Search method called.[" + searchRequest + "]");
        return publicService.searchCommonCurrency(searchRequest);
    }


    @RequestMapping(value = "/common/language/get/search/keyword", method = RequestMethod.POST)
    public BaseDto getAllCommonLanguages(@RequestBody SearchRequest searchRequest) {
        log.info("getAllLanguages is called");
        return publicService.searchCommonLanguage(searchRequest);
    }

    @RequestMapping(value = "/chargemaster/bulkupload", method = RequestMethod.POST)
    public BaseDto agentPortMasterBulkUpload(@RequestBody FileUploadDto fileUploadDto) {
        log.info("Agent Port Master Bulk Upload method called.");
        return chargeMasterService.chargeBulkUpload(fileUploadDto);
    }

    @RequestMapping(value = "/download/chargetemplate/{type}", method = RequestMethod.GET)
    public void downloadChargeTemplate(HttpServletResponse response, @PathVariable String type) {
        log.info("Download Template method called.");
        publicService.downloadChargeTemplate(response, type);
    }

    @RequestMapping(value = "/enquiry/push/save", method = RequestMethod.POST)
    public BaseDto pushSave(@RequestBody EnquiryLog existingEnquiryLog) {
        return enquiryLogService.pushSave(existingEnquiryLog);
    }

    @RequestMapping(value = "/quotation/push/save", method = RequestMethod.POST)
    public BaseDto pushSave(@RequestBody Quotation existingQuotation) {
        return quotationService.pushSave(existingQuotation);
    }

    @RequestMapping(value = "/shipment/push/save", method = RequestMethod.POST)
    public BaseDto pushSave(@RequestBody Shipment existingShipment) {
        return shipmentService.pushSave(existingShipment);
    }

    @RequestMapping(value = "/consol/push/save", method = RequestMethod.POST)
    public BaseDto pushSave(@RequestBody Consol existingConsol) {
        return consolService.pushSave(existingConsol);
    }

    @RequestMapping(value = "/invoice/push/save", method = RequestMethod.POST)
    public BaseDto pushSave(@RequestBody InvoiceCreditNote existingInvoiceCreditNote) {
        return invoiceCreditNoteService.pushSave(existingInvoiceCreditNote);
    }

    @RequestMapping(value = "/provisional/push/save", method = RequestMethod.POST)
    public BaseDto pushSave(@RequestBody Provisional existing) {
        return provisionalService.pushSave(existing);
    }

    @RequestMapping(value = "/partymaster/push/save", method = RequestMethod.POST)
    public BaseDto pushSave(@RequestBody PartyMaster existing) {
        return partyMasterService.pushSave(existing);
    }

    @RequestMapping(value = "/upload/common_db_data", method = RequestMethod.GET)
    public void uploadCommonData() {
        log.info("Upload Common Data.");
        commonDbFileUploaderService.loadCommonData();
    }

}
