package com.efreightsuite.controller;

import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.CommoditySearchDto;
import com.efreightsuite.dto.SearchRequest;
import com.efreightsuite.model.CommodityMaster;
import com.efreightsuite.service.CommodityMasterService;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


@RestController
@Log4j2
@RequestMapping(value = "/api/v1/commoditymaster")
public class CommodityMasterController {
    /**
     * commodityMasterService
     */
    @Autowired
    private
    CommodityMasterService commodityMasterService;


    /**
     * Method get
     *
     */
    @RequestMapping(value = "/get/id/{id}", method = RequestMethod.GET)
    @Secured("ROLE_MASTER.GENERAL.COMMODITY.VIEW")
    public BaseDto get(@PathVariable Long id) {
        log.info("CommodityMasterControler get by id method called.");
        return commodityMasterService.get(id);
    }


    /**
     * Method Save
     *
     */
    @RequestMapping(value = "/create", method = RequestMethod.POST)
    @Secured("ROLE_MASTER.GENERAL.COMMODITY.CREATE")
    public BaseDto create(@RequestBody CommodityMaster commodityMaster) {
        log.info("CommodityMasterControler create method called.");
        return commodityMasterService.create(commodityMaster);
    }

    /**
     * Method Update
     *
     */
    @RequestMapping(value = "/update", method = RequestMethod.POST)
    @Secured("ROLE_MASTER.GENERAL.COMMODITY.MODIFY")
    public BaseDto update(@RequestBody CommodityMaster commodityMaster) {
        log.info("CommodityMasterControler update method called.");
        return commodityMasterService.update(commodityMaster);
    }

    /**
     * Method Delete
     *
     */
    @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
    @Secured("ROLE_MASTER.GENERAL.COMMODITY.DELETE")
    public BaseDto delete(@PathVariable("id") Long id) {
        log.info("CommodityMasterControler delete method called.");
        return commodityMasterService.delete(id);
    }


    @RequestMapping(value = "/get/search", method = RequestMethod.POST)
    @Secured("ROLE_MASTER.GENERAL.COMMODITY.LIST")
    public BaseDto search(@RequestBody CommoditySearchDto searchDto) {
        log.info("Search method called.");
        return commodityMasterService.search(searchDto);
    }

    @RequestMapping(value = "/get/search/keyword", method = RequestMethod.POST)
    public BaseDto search(@RequestBody SearchRequest searchRequest) {
        log.info("Search method called.");
        return commodityMasterService.search(searchRequest);
    }

}
