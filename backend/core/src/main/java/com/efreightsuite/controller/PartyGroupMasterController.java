package com.efreightsuite.controller;

import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.PartyGroupSearchDto;
import com.efreightsuite.dto.SearchRequest;
import com.efreightsuite.model.PartyGroupMaster;
import com.efreightsuite.service.PartyGroupMasterService;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Log4j2
@RequestMapping(value = "/api/v1/partygroupmaster")
public class PartyGroupMasterController {

    @Autowired
    private
    PartyGroupMasterService partyGroupMasterService;

    @RequestMapping(value = "/get/search", method = RequestMethod.POST)
    @Secured("ROLE_MASTER.CRM.PARTY_GROUP.LIST")
    public BaseDto search(@RequestBody PartyGroupSearchDto partyGroupSearchDto) {
        log.info("Search method called.");
        return partyGroupMasterService.search(partyGroupSearchDto);
    }

    @RequestMapping(value = "/get/search/keyword", method = RequestMethod.POST)
    public BaseDto search(@RequestBody SearchRequest searchRequest) {
        log.info("Search method called.");
        return partyGroupMasterService.search(searchRequest);
    }

    @RequestMapping(value = "/get/id/{id}", method = RequestMethod.GET)
    @Secured("ROLE_MASTER.CRM.PARTY_GROUP.VIEW")
    public BaseDto get(@PathVariable Long id) {
        log.info("get method is called. [ID = " + id + "]");
        return partyGroupMasterService.get(id);
    }

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    @Secured("ROLE_MASTER.CRM.PARTY_GROUP.CREATE")
    public BaseDto save(@RequestBody PartyGroupMaster partyGroupMaster) {
        log.info("Create method called.");
        return partyGroupMasterService.create(partyGroupMaster);
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    @Secured("ROLE_MASTER.CRM.PARTY_GROUP.MODIFY")
    public BaseDto update(@RequestBody PartyGroupMaster partyGroupMaster) {
        log.info("Update method called.");
        return partyGroupMasterService.update(partyGroupMaster);
    }

    @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
    @Secured("ROLE_MASTER.CRM.PARTY_GROUP.DELETE")
    public BaseDto delete(@PathVariable Long id) {
        log.info("Delete method called.");
        return partyGroupMasterService.delete(id);
    }

}
