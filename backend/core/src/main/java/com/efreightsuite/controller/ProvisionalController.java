package com.efreightsuite.controller;

import com.efreightsuite.client.PushApiService;
import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.ProvisionalSearchDto;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.model.Provisional;
import com.efreightsuite.service.ProvisionalService;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


@RestController
@Log4j2
@RequestMapping(value = "/api/v1/provisional")
public class ProvisionalController {

    @Autowired
    private
    ProvisionalService provisionalService;

    @Autowired
    private
    PushApiService pushApiService;

    @RequestMapping(value = "/get/id/{id}", method = RequestMethod.GET)
    @Secured("ROLE_FINANCE.PROVISIONAL.VIEW")
    public BaseDto get(@PathVariable Long id) {
        log.info("ProvisionalController get by id method called.");
        return provisionalService.get(id);
    }

    @RequestMapping(value = "/get/serviceuid/{serviceuid}", method = RequestMethod.GET)
    public BaseDto getByServiceUid(@PathVariable String serviceuid) {
        log.info("ProvisionalController getByServiceUid by id method called.");
        return provisionalService.getByServiceUid(serviceuid, false);
    }

    @RequestMapping(value = "/get/masteruid/{masteruid}", method = RequestMethod.GET)
    public BaseDto getByMasterUid(@PathVariable String masteruid) {
        log.info("ProvisionalController getByMasterUid method called.");
        return provisionalService.getByMasterUid(masteruid);
    }

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    @Secured("ROLE_FINANCE.PROVISIONAL.CREATE")
    public BaseDto create(@RequestBody Provisional provisional) {
        log.info("Create method called.");

        BaseDto baseDto = provisionalService.create(provisional);


        if (baseDto.getResponseCode().toUpperCase().equals(ErrorCode.SUCCESS)) {
            pushApiService.pushToProvisional(Long.parseLong(baseDto.getResponseObject().toString()), null, null);
        }

        return baseDto;
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    @Secured("ROLE_FINANCE.PROVISIONAL.MODIFY")
    public BaseDto update(@RequestBody Provisional provisional) {
        log.info("ProvisionalControler update method called.");

        BaseDto baseDto = provisionalService.update(provisional);

        if (baseDto.getResponseCode().toUpperCase().equals(ErrorCode.SUCCESS)) {
            pushApiService.pushToProvisional(Long.parseLong(baseDto.getResponseObject().toString()), null, null);
        }

        return baseDto;

    }

    @RequestMapping(value = "/search", method = RequestMethod.POST)
    @Secured("ROLE_FINANCE.PROVISIONAL.LIST")
    public BaseDto search(@RequestBody ProvisionalSearchDto flightPlanSearchDto) {
        log.info("Search method called.");
        return provisionalService.search(flightPlanSearchDto);
    }

}
