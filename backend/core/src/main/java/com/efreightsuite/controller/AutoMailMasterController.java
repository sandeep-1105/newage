package com.efreightsuite.controller;

import com.efreightsuite.dto.AutoMailMasterSearchDto;
import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.SearchRequest;
import com.efreightsuite.model.AutoMailMaster;
import com.efreightsuite.service.AutoMailMasterService;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Devendrachary M
 */

@RestController
@Log4j2
@RequestMapping(value = "/api/v1/automailmaster")
public class AutoMailMasterController {

    @Autowired
    private
    AutoMailMasterService autoMailMasterService;

    /**
     * This method Retrieves the Auto Mail Master
     *
     * @return
     */
    @RequestMapping(value = "/get/search", method = RequestMethod.POST)
    @Secured("ROLE_MASTER.GENERAL.AUTO_MAIL.LIST")
    public BaseDto search(@RequestBody AutoMailMasterSearchDto searchDto) {
        log.info("Search method called.[" + searchDto + "]");

        return autoMailMasterService.search(searchDto);
    }

    @RequestMapping(value = "/get/search/keyword", method = RequestMethod.POST)
    public BaseDto search(@RequestBody SearchRequest searchRequest) {
        log.info("Search method called.[" + searchRequest + "]");

        return autoMailMasterService.search(searchRequest);
    }

    @RequestMapping(value = "/get/searchByExternal/keyword", method = RequestMethod.POST)
    public BaseDto searchByExternal(@RequestBody SearchRequest searchRequest) {
        log.info("Search method called.[" + searchRequest + "]");

        return autoMailMasterService.searchByExternal(searchRequest);
    }

    /**
     * This method Retrieves the Auto Mail Master By Id
     *
     * @param id
     * @return
     */
    @RequestMapping(value = "/get/id/{id}", method = RequestMethod.GET)
    @Secured("ROLE_MASTER.GENERAL.AUTO_MAIL.VIEW")
    public BaseDto get(@PathVariable Long id) {
        log.info("Get by id method called..[" + id + "]");
        return autoMailMasterService.get(id);
    }

    /**
     * This method Persists the Auto Mail Master
     *
     * @param enquiryLog
     * @return
     */
    @RequestMapping(value = "/create", method = RequestMethod.POST)
    @Secured("ROLE_MASTER.GENERAL.AUTO_MAIL.CREATE")
    public BaseDto save(@RequestBody AutoMailMaster enquiryLog) {
        log.info("While sending AutoMailMaster  : " + enquiryLog);
        return autoMailMasterService.create(enquiryLog);
    }

    /**
     * This method Updates the Existing Auto Mail Master
     *
     * @param existingAutoMailMaster
     * @return
     */
    @RequestMapping(value = "/update", method = RequestMethod.POST)
    @Secured("ROLE_MASTER.GENERAL.AUTO_MAIL.MODIFY")
    public BaseDto update(@RequestBody AutoMailMaster existingAutoMailMaster) {
        log.info("Update method called.[" + existingAutoMailMaster.getId() + "]");
        return autoMailMasterService.update(existingAutoMailMaster);
    }


    @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
    @Secured("ROLE_MASTER.GENERAL.AUTO_MAIL.DELETE")
    public BaseDto delete(@PathVariable("id") Long id) {
        log.info("Delete method called.[" + id + "]");
        return autoMailMasterService.delete(id);
    }


}
