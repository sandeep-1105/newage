package com.efreightsuite.controller;

import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.SearchRequest;
import com.efreightsuite.dto.ServiceTaxCategoryMasterSearchDto;
import com.efreightsuite.model.ServiceTaxCategoryMaster;
import com.efreightsuite.service.ServiceTaxCategroyMasterService;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/api/v1/sertaxcatmaster")
@Log4j2
public class ServiceTaxCategoryMasterController {

    /**
     * ServiceTaxCategoryMasterService Object
     */

    @Autowired
    private
    ServiceTaxCategroyMasterService serviceTaxCategroyMasterService;


    /**
     * Method - getById Returns ServiceTaxCategoryMaster based on given serviceTaxCategoryMaster id
     *
     * @return ServiceTaxCategoryMaster
     */

    @RequestMapping(value = "/get/id/{id}", method = RequestMethod.GET)
    @Secured("ROLE_MASTER.FINANCE.SERVICE_TAX_CATEGORY.VIEW")
    public BaseDto getById(@PathVariable Long id) {

        log.info("getById method is called...ID = " + id);

        return serviceTaxCategroyMasterService.getById(id);
    }

    /**
     * Method - search Returns Paginated Search Result based on Search Keyword
     *
     * @return SearchRespDto
     */

    @RequestMapping(value = "/get/search", method = RequestMethod.POST)
    @Secured("ROLE_MASTER.FINANCE.SERVICE_TAX_CATEGORY.LIST")
    public BaseDto search(@RequestBody ServiceTaxCategoryMasterSearchDto searchDto) {

        log.info("Search method is called...Search Dto: " + searchDto);

        return serviceTaxCategroyMasterService.search(searchDto);
    }

    @RequestMapping(value = "/get/search/keyword", method = RequestMethod.POST)
    public BaseDto search(@RequestBody SearchRequest searchRequest) {

        log.info("Search method is called...Search Dto: " + searchRequest);

        return serviceTaxCategroyMasterService.search(searchRequest);
    }

    /**
     * Method - create Creates and returns a new ServiceTaxCategoryMaster
     *
     * @returns ServiceTaxCategoryMaster
     */

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    @Secured("ROLE_MASTER.FINANCE.SERVICE_TAX_CATEGORY.CREATE")
    public BaseDto create(@RequestBody ServiceTaxCategoryMaster serviceTaxCategoryMaster) {

        log.info("Create method is called.. serviceTaxCategoryMaster : " + serviceTaxCategoryMaster);

        return serviceTaxCategroyMasterService.create(serviceTaxCategoryMaster);
    }

    /**
     * Method - update returns a updated serviceTaxCategoryMaster
     *
     * @param existingserviceTaxCategoryMaster
     * @returns serviceTaxCategoryMaster
     */

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    @Secured("ROLE_MASTER.FINANCE.SERVICE_TAX_CATEGORY.MODIFY")
    public BaseDto update(@RequestBody ServiceTaxCategoryMaster existingserviceTaxCategoryMaster) {

        log.info("Update method is called.. serviceTaxCategoryMaster : " + existingserviceTaxCategoryMaster);

        return serviceTaxCategroyMasterService.update(existingserviceTaxCategoryMaster);
    }

    @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
    @Secured("ROLE_MASTER.FINANCE.SERVICE_TAX_CATEGORY.DELETE")
    public BaseDto delete(@PathVariable Long id) {

        log.info("Delete method is called.. : [" + id + "]");

        return serviceTaxCategroyMasterService.delete(id);
    }


}
