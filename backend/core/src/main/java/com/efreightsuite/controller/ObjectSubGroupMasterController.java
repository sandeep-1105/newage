package com.efreightsuite.controller;


import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.ObjectSubGroupMasterSearchDto;
import com.efreightsuite.dto.SearchRequest;
import com.efreightsuite.model.ObjectSubGroupMaster;
import com.efreightsuite.service.ObjectSubGroupMasterService;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


@RestController
@Log4j2
@RequestMapping(value = "/api/v1/objectsubgroupmaster")
public class ObjectSubGroupMasterController {

    @Autowired
    private
    ObjectSubGroupMasterService objectSubGroupMasterService;

    @RequestMapping(value = "/get/search/{groupId}", method = RequestMethod.POST)
    public BaseDto get(@RequestBody SearchRequest searchRequest, @PathVariable Long groupId) {
        log.info("getall method called.");
        return objectSubGroupMasterService.search(searchRequest, groupId);
    }

    @RequestMapping(value = "/get/search/keyword", method = RequestMethod.POST)
    @Secured("ROLE_MASTER.GENERAL.OBJECT_SUBGROUP.LIST")
    public BaseDto get(@RequestBody ObjectSubGroupMasterSearchDto searchDto) {
        log.info("getall method called.");
        return objectSubGroupMasterService.search(searchDto);
    }

    @RequestMapping(value = "/get/id/{id}", method = RequestMethod.GET)
    @Secured("ROLE_MASTER.GENERAL.OBJECT_SUBGROUP.VIEW")
    public BaseDto get(@PathVariable Long id) {
        log.info("get method called." + id);
        return objectSubGroupMasterService.get(id);
    }

    @RequestMapping(value = "/get/getall", method = RequestMethod.GET)
    public BaseDto getAll() {
        log.info("getall method called.");
        return objectSubGroupMasterService.getAll();
    }

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    @Secured({"ROLE_MASTER.GENERAL.OBJECT_SUBGROUP.CREATE", "ROLE_MASTER.GENERAL.OBJECT_SUBGROUP.MODIFY"})
    public BaseDto saveOrUpdate(@RequestBody ObjectSubGroupMaster objectSubGroupMaster) {
        log.info("SaveOrUpdate method is called.");
        return objectSubGroupMasterService.saveOrUpdate(objectSubGroupMaster);
    }

    @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
    @Secured("ROLE_MASTER.GENERAL.OBJECT_SUBGROUP.DELETE")
    public BaseDto delete(@PathVariable Long id) {
        log.info("Delete method called....");
        return objectSubGroupMasterService.delete(id);
    }


}
