package com.efreightsuite.controller;

import java.util.List;

import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.DocumentSearchDto;
import com.efreightsuite.dto.SearchRequest;
import com.efreightsuite.model.DocumentMaster;
import com.efreightsuite.service.DocumentMasterService;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author achyu Date - 23.06.2016 Updated - 23.06.2016
 */

@RestController
@RequestMapping(value = "/api/v1/documentmaster")
@Log4j2
public class DocumentMasterController {

    @Autowired
    private
    DocumentMasterService documentMasterService;


    @RequestMapping(value = "/get/id/{id}", method = RequestMethod.GET)
    @Secured("ROLE_MASTER.FINANCE.DOCUMENT_MASTER.VIEW")
    public BaseDto getByDocumentId(@PathVariable Long id) {

        log.info("getByDocumentId method is called...ID = " + id);

        return documentMasterService.getByDocumentId(id);
    }

    @RequestMapping(value = "/get/search", method = RequestMethod.POST)
    @Secured("ROLE_MASTER.FINANCE.DOCUMENT_MASTER.LIST")
    public BaseDto search(@RequestBody DocumentSearchDto searchDto) {

        log.info("Search method is called...Search Dto: " + searchDto);

        return documentMasterService.search(searchDto);
    }

    @RequestMapping(value = "/get/search/keyword", method = RequestMethod.POST)
    public BaseDto search(@RequestBody SearchRequest searchRequest) {

        log.info("Search method is called...Search Dto: " + searchRequest);

        return documentMasterService.search(searchRequest);
    }

    @RequestMapping(value = "/create")
    @Secured("ROLE_MASTER.FINANCE.DOCUMENT_MASTER.CREATE")
    public BaseDto create(@RequestBody List<DocumentMaster> documentMasterList) {

        log.info("Create method is called.. DocumentMaster : " + documentMasterList);

        return documentMasterService.create(documentMasterList);
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    @Secured("ROLE_MASTER.FINANCE.DOCUMENT_MASTER.MODIFY")
    public BaseDto update(@RequestBody DocumentMaster existingDocumentMaster) {

        log.info("Create method is called.. DocumentMaster : " + existingDocumentMaster);

        return documentMasterService.update(existingDocumentMaster);
    }

    @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
    @Secured("ROLE_MASTER.FINANCE.DOCUMENT_MASTER.DELETE")
    public BaseDto delete(@PathVariable Long id) {

        log.info("Delete method is called.. : [" + id + "]");

        return documentMasterService.delete(id);
    }
}
