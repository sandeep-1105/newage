package com.efreightsuite.controller;

import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.CompanySearchReqDto;
import com.efreightsuite.dto.SearchRequest;
import com.efreightsuite.model.CompanyMaster;
import com.efreightsuite.service.CompanyMasterService;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Log4j2
@RequestMapping(value = "/api/v1/companymaster")
public class CompanyMasterController {

    @Autowired
    private
    CompanyMasterService companyMasterService;


    @RequestMapping(value = "/get/search", method = RequestMethod.POST)
    public BaseDto search(@RequestBody CompanySearchReqDto searchDto) {
        log.info("CompanyMasterController search method called.");
        return companyMasterService.search(searchDto);
    }

    @RequestMapping(value = "/get/search/keyword", method = RequestMethod.POST)
    public BaseDto search(@RequestBody SearchRequest searchRequest) {
        log.info("CompanyMasterController search method called.");
        return companyMasterService.search(searchRequest);
    }

    @RequestMapping(value = "/get/id/{id}", method = RequestMethod.GET)
    public BaseDto get(@PathVariable Long id) {
        log.info("CompanyMasterController get by id method called.");
        return companyMasterService.get(id);
    }


    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public BaseDto save(@RequestBody CompanyMaster companyMaster) {
        log.info("CompanyMasterController create method called.");
        return companyMasterService.create(companyMaster);
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public BaseDto update(@RequestBody CompanyMaster companyMaster) {
        log.info("CompanyMasterController update method called.");
        return companyMasterService.update(companyMaster);
    }

    @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
    public BaseDto delete(@PathVariable Long id) {
        log.info("CompanyMasterController delete by id method called.");
        return companyMasterService.delete(id);
    }

    @RequestMapping(value = "/get/code/{companyCode}", method = RequestMethod.GET)
    public BaseDto getByCompanyCode(@PathVariable String companyCode) {
        log.info("getByCompanyCode method is called...CODE = " + companyCode);
        return companyMasterService.getByCompanyCode(companyCode);
    }

    @RequestMapping(value = "/getall", method = RequestMethod.GET)
    public BaseDto get() {
        log.info("getall method called.");
        return companyMasterService.getAll();
    }

}
