package com.efreightsuite.controller;

import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.LocationSearchDto;
import com.efreightsuite.dto.SearchRequest;
import com.efreightsuite.model.LocationMaster;
import com.efreightsuite.service.LocationMasterService;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author tanveer
 */
@RestController
@Log4j2
@RequestMapping(value = "/api/v1/locationmaster")
public class LocationMasterController {


    /**
     * LocationMasterService Object
     */

    @Autowired
    private
    LocationMasterService locationMasterService;


    /**
     * Method - getAll
     * API to get the location By Id
     *
     * @return BaseDto
     */

    @RequestMapping(value = "/get/search/keyword", method = RequestMethod.POST)
    public BaseDto search(@RequestBody SearchRequest searchRequest) {
        log.info("getall method called.");
        return locationMasterService.search(searchRequest);
    }


    @RequestMapping(value = "/get/search/exclude", method = RequestMethod.POST)
    public BaseDto searchServiceNotInList(@RequestBody SearchRequest searchRequest) {
        log.info("searchLocationNotInList method called. :");
        return locationMasterService.searchLocationNotInList(searchRequest);
    }

    @RequestMapping(value = "/get/search", method = RequestMethod.POST)
    @Secured("ROLE_SETUP.LOCATION_MASTER.LIST")
    public BaseDto search(@RequestBody LocationSearchDto searchDto) {
        log.info("getall method called.");
        return locationMasterService.search(searchDto);
    }

    @RequestMapping(value = "/get/search/keyword/{countryId}", method = RequestMethod.POST)
    public BaseDto search(@RequestBody SearchRequest searchRequest, @PathVariable Long countryId) {
        log.info("getall method called.");
        return locationMasterService.search(searchRequest, countryId);
    }

    @RequestMapping(value = "/get/searchByCompany/keyword/{companyId}", method = RequestMethod.POST)
    public BaseDto searchByCompany(@RequestBody SearchRequest searchRequest, @PathVariable Long companyId) {
        log.info("searchByCompany method called.");
        return locationMasterService.searchByCompany(searchRequest, companyId);
    }


    /**
     * Method - getByLocationId
     * API to get the location By Id
     *
     * @param id
     * @return BaseDto
     */

    @RequestMapping(value = "/get/id/{id}", method = RequestMethod.GET)
    @Secured("ROLE_SETUP.LOCATION_MASTER.VIEW")
    public BaseDto get(@PathVariable Long id) {
        log.info("get method called." + id);
        return locationMasterService.get(id);
    }


    @RequestMapping(value = "/get/timezones", method = RequestMethod.GET)
    public BaseDto getTimeZones() {
        return locationMasterService.getTimeZoneList();
    }


    /**
     * Method - Create
     * API to create the location
     *
     * @return BaseDto
     */
    @RequestMapping(value = "/create", method = RequestMethod.POST)
    @Secured({"ROLE_SETUP.LOCATION_MASTER.CREATE", "ROLE_SETUP.LOCATION_MASTER.MODIFY"})
    public BaseDto create(@RequestBody LocationMaster locationMaster) {
        log.info("Create method is called.");
        return locationMasterService.createOrUpdate(locationMaster);
    }


    /**
     * Method - Update
     * API to update the existing location master
     *
     * @param locationMaster
     * @return BaseDto
     */
    @RequestMapping(value = "/update", method = RequestMethod.POST)
    @Secured("ROLE_SETUP.LOCATION_MASTER.MODIFY")
    public BaseDto update(@RequestBody LocationMaster locationMaster) {
        log.info("Update method is called.");
        return locationMasterService.createOrUpdate(locationMaster);
    }

    /**
     * To Delete the locationMaster Object Based on ID
     *
     * @param id
     * @return BaseDto
     */
    @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
    @Secured("ROLE_SETUP.LOCATION_MASTER.DELETE")
    public BaseDto delete(@PathVariable Long id) {
        log.info("Delete method called....");
        return locationMasterService.delete(id);
    }
}
