package com.efreightsuite.controller;

import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.PreBookingMawbSearchDto;
import com.efreightsuite.dto.StockEnquiryDto;
import com.efreightsuite.dto.StockGenerationSearchDto;
import com.efreightsuite.model.StockGeneration;
import com.efreightsuite.service.StockGenerationService;
import com.efreightsuite.service.StockService;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Log4j2
@RequestMapping(value = "/api/v1/stock")
public class StockGenerationController {


    @Autowired
    private
    StockService stockService;

    @Autowired
    private
    StockGenerationService stockGenerationService;

    @RequestMapping(value = "/generate", method = RequestMethod.POST)
    public BaseDto create(@RequestBody StockGeneration stockGeneration) {
        log.info("StockController -> generate method called.");
        return stockService.generate(stockGeneration);
    }


    @RequestMapping(value = "/get/{carrierId}/{porId}", method = RequestMethod.GET)
    public BaseDto get(@PathVariable Long carrierId, @PathVariable Long porId) {
        return stockService.get(carrierId, porId);
    }

    @RequestMapping(value = "/search", method = RequestMethod.POST)
    @Secured("ROLE_AIR.MAWB_STOCK_GENERATION.LIST")
    public BaseDto search(@RequestBody StockGenerationSearchDto searchDto) {
        return stockService.search(searchDto);
    }

    @RequestMapping(value = "/enquirysearch", method = RequestMethod.POST)
    @Secured("ROLE_AIR.MAWB_STOCK_GENERATION.MAWB_STOCK_ENQUIRY.LIST")
    public BaseDto enquirySearch(@RequestBody StockEnquiryDto dto) {
        return stockService.enquirySearch(dto);
    }


    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public BaseDto update(@RequestBody StockGeneration stockGeneration) {
        log.info("StockController -> update method called.");
        return stockService.update(stockGeneration);
    }

    @RequestMapping(value = "/getMawb/{carrierId}/{portId}", method = RequestMethod.GET)
    public BaseDto getMawb(@PathVariable Long carrierId, @PathVariable Long portId) {
        return stockService.getMawb(carrierId, portId);
    }


    @RequestMapping(value = "/mawblist", method = RequestMethod.POST)
    public Object getMawbBasedOnCarrierAndPorAndCount(@RequestBody PreBookingMawbSearchDto data) {
        return stockGenerationService.getMawbBasedOnCarrierAndPorAndCount(data);
    }


}
