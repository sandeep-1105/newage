package com.efreightsuite.controller;

import java.util.List;

import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.CarrierRateSearchDto;
import com.efreightsuite.dto.FileUploadDto;
import com.efreightsuite.model.CarrierRateMaster;
import com.efreightsuite.service.CarrierRateMasterService;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Log4j2
@RequestMapping(value = "/api/v1/carrierratemaster")
public class CarrierRateMasterController {

    @Autowired
    private
    CarrierRateMasterService carrierRateMasterService;

    @RequestMapping(value = "/get/search/keyword", method = RequestMethod.POST)
    @Secured("ROLE_MASTER.EAIR.CARRIER_RATE.LIST")
    public BaseDto search(@RequestBody CarrierRateSearchDto searchDto) {
        log.info("Search method called.");
        return carrierRateMasterService.search(searchDto);
    }

    @RequestMapping(value = "/get/id/{id}", method = RequestMethod.GET)
    @Secured("ROLE_MASTER.EAIR.CARRIER_RATE.VIEW")
    public BaseDto get(@PathVariable Long id) {
        log.info("Get by id method called.[" + id + "]");
        return carrierRateMasterService.get(id);
    }

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    @Secured("ROLE_MASTER.EAIR.CARRIER_RATE.CREATE")
    public BaseDto save(@RequestBody List<CarrierRateMaster> carrierRateMaster) {
        log.info("Create method called.");
        return carrierRateMasterService.create(carrierRateMaster);
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    @Secured("ROLE_MASTER.EAIR.CARRIER_RATE.MODIFY")
    public BaseDto update(@RequestBody CarrierRateMaster carrierRateMaster) {
        log.info("Update method called.[" + carrierRateMaster.getId() + "]");
        return carrierRateMasterService.update(carrierRateMaster);
    }

    @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
    @Secured("ROLE_MASTER.EAIR.CARRIER_RATE.DELETE")
    public BaseDto delete(@PathVariable("id") Long id) {
        log.info("Delete method called.[" + id + "]");
        return carrierRateMasterService.delete(id);
    }


    @RequestMapping(value = "/upload", method = RequestMethod.POST)
    public BaseDto upload(@RequestBody FileUploadDto fileUploadDto) {
        log.info("carrier upload method called.");
        return carrierRateMasterService.upload(fileUploadDto);
    }


}
