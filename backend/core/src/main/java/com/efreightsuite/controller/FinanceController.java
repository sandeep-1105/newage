package com.efreightsuite.controller;

import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.JobLedgerReqDto;
import com.efreightsuite.service.FinanceService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/api/v1/finance")
public class FinanceController {

    @Autowired
    private
    FinanceService financeService;

    @RequestMapping(value = "/jobledger", method = RequestMethod.POST)
    public BaseDto create(@RequestBody JobLedgerReqDto requestDto) {
        return financeService.getJobLedger(requestDto);
    }


}
