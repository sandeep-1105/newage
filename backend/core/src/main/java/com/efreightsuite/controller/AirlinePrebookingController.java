package com.efreightsuite.controller;

import com.efreightsuite.dto.AirlinePrebookingSearchRequestDto;
import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.model.AirlinePrebooking;
import com.efreightsuite.service.AirlinePrebookingService;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Log4j2
@RequestMapping(value = "api/v1/airline_prebooking")
public class AirlinePrebookingController {

    @Autowired
    private
    AirlinePrebookingService airlinePrebookingService;

    @RequestMapping(value = "/get/id/{id}", method = RequestMethod.GET)
    @Secured("ROLE_AIR.AIRLINE_PREBOOKING_ENTRY.VIEW")
    public BaseDto get(@PathVariable Long id) {
        log.info("AirlinePrebookingController -> get method called.");
        return airlinePrebookingService.get(id);
    }

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    @Secured({"ROLE_AIR.AIRLINE_PREBOOKING_ENTRY.CREATE", "ROLE_AIR.AIRLINE_PREBOOKING_ENTRY.MODIFY"})
    public BaseDto create(@RequestBody AirlinePrebooking airlinePrebooking) {
        log.info("AirlinePrebookingController -> create method called." + airlinePrebooking);
        return airlinePrebookingService.create(airlinePrebooking);
    }

    @RequestMapping(value = "/search", method = RequestMethod.POST)
    @Secured("ROLE_AIR.AIRLINE_PREBOOKING_ENTRY.LIST")
    public BaseDto search(@RequestBody AirlinePrebookingSearchRequestDto requestDto) {
        log.info("AirlinePrebookingController -> search method called." + requestDto);
        return airlinePrebookingService.search(requestDto);
    }

}
