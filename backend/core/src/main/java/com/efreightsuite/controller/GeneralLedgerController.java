package com.efreightsuite.controller;

import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.SearchRequest;
import com.efreightsuite.enumeration.YesNo;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.service.GeneralLedgerService;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/api/v1/gl")
@Log4j2
public class GeneralLedgerController {

    @Autowired
    private
    GeneralLedgerService glService;

    @RequestMapping(value = "/account/get/search/keyword/{isSubledger}", method = RequestMethod.POST)
    public BaseDto search(@PathVariable String isSubledger, @RequestBody SearchRequest searchRequest) {

        BaseDto dto = new BaseDto();

        if (isSubledger != null && isSubledger.toUpperCase().equals("ALL")) {
            dto = glService.search(searchRequest, null, null);
        } else if (isSubledger != null && isSubledger.toUpperCase().equals("YES")) {
            dto = glService.search(searchRequest, YesNo.Yes, null);
        } else if (isSubledger != null && isSubledger.toUpperCase().equals("NO")) {
            dto = glService.search(searchRequest, YesNo.No, null);
        } else {
            dto.setResponseCode(ErrorCode.SUCCESS);
        }

        return dto;

    }

    @RequestMapping(value = "/account/get/search/keyword/{isSubledger}/{isBank}", method = RequestMethod.POST)
    public BaseDto search(@PathVariable String isSubledger, @PathVariable String isBank, @RequestBody SearchRequest searchRequest) {

        BaseDto dto = new BaseDto();

        if (isSubledger != null && isSubledger.toUpperCase().equals("ALL")) {
            if (isBank != null && isBank.toUpperCase().equals("YES")) {
                dto = glService.search(searchRequest, null, YesNo.Yes);
            }
            if (isBank != null && isBank.toUpperCase().equals("NO")) {
                dto = glService.search(searchRequest, null, YesNo.No);
            }
        } else if (isSubledger != null && isSubledger.toUpperCase().equals("YES")) {
            if (isBank != null && isBank.toUpperCase().equals("YES")) {
                dto = glService.search(searchRequest, YesNo.Yes, YesNo.Yes);
            }
            if (isBank != null && isBank.toUpperCase().equals("NO")) {
                dto = glService.search(searchRequest, YesNo.Yes, YesNo.No);
            }
        } else if (isSubledger != null && isSubledger.toUpperCase().equals("NO")) {
            if (isBank != null && isBank.toUpperCase().equals("YES")) {
                dto = glService.search(searchRequest, YesNo.No, YesNo.Yes);
            }
            if (isBank != null && isBank.toUpperCase().equals("NO")) {
                dto = glService.search(searchRequest, YesNo.No, YesNo.No);
            }
        } else {
            dto.setResponseCode(ErrorCode.SUCCESS);
        }

        return dto;

    }


}
