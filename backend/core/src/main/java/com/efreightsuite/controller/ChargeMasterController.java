package com.efreightsuite.controller;

import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.ChargeSearchDto;
import com.efreightsuite.dto.SearchRequest;
import com.efreightsuite.model.ChargeMaster;
import com.efreightsuite.service.ChargeMasterService;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Log4j2
@RequestMapping(value = "api/v1/chargemaster")
public class ChargeMasterController {

    @Autowired
    private
    ChargeMasterService chargeMasterService;

    @RequestMapping(value = "/get/search", method = RequestMethod.POST)
    @Secured("ROLE_MASTER.FINANCE.CHARGE.LIST")
    public BaseDto search(@RequestBody ChargeSearchDto searchDto) {
        log.info("search method called.");
        return chargeMasterService.search(searchDto);
    }

    @RequestMapping(value = "/get/search/keyword", method = RequestMethod.POST)
    public BaseDto search(@RequestBody SearchRequest searchRequest) {
        log.info("search method called.");
        return chargeMasterService.search(searchRequest);
    }

    @RequestMapping(value = "/get/id/{id}", method = RequestMethod.GET)
    @Secured("ROLE_MASTER.FINANCE.CHARGE.VIEW")
    public BaseDto get(@PathVariable Long id) {
        log.info("get method is called.[" + id + "]");
        return chargeMasterService.get(id);
    }

    @RequestMapping(value = "/get/code/{chargeCode}", method = RequestMethod.GET)
    @Secured("ROLE_MASTER.FINANCE.CHARGE.VIEW")
    public BaseDto getByCarrierCode(@PathVariable String chargeCode) {
        log.info("get by code method called.[" + chargeCode + "]");
        return chargeMasterService.getByChargeCode(chargeCode);
    }

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    @Secured("ROLE_MASTER.FINANCE.CHARGE.CREATE")
    public BaseDto save(@RequestBody ChargeMaster chargeMaster) {
        log.info("create method called.");
        return chargeMasterService.create(chargeMaster);
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    @Secured("ROLE_MASTER.FINANCE.CHARGE.MODIFY")
    public BaseDto update(@RequestBody ChargeMaster chargeMaster) {
        log.info("update method called.[" + chargeMaster.getId() + "]");
        return chargeMasterService.update(chargeMaster);
    }

    @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
    @Secured("ROLE_MASTER.FINANCE.CHARGE.DELETE")
    public BaseDto delete(@PathVariable Long id) {
        log.info("Delete method called....[" + id + "]");
        return chargeMasterService.delete(id);
    }

    @RequestMapping(value = "/get/search/exclude", method = RequestMethod.POST)
    public BaseDto searchChargeNotInList(@RequestBody SearchRequest searchRequest) {
        log.info("searchChargeNotInList method called. :");
        return chargeMasterService.searchChargeNotInList(searchRequest);
    }

}
