package com.efreightsuite.controller;

import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.DocumentPrefixSearchDto;
import com.efreightsuite.enumeration.DocPrefixDocumentType;
import com.efreightsuite.model.DocPrefixTypeMapping;
import com.efreightsuite.model.DocumentPrefixMaster;
import com.efreightsuite.service.DocumentPrefixMasterService;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author tanveer
 */

@RestController
@Log4j2
@RequestMapping(value = "/api/v1/documentprefixmaster")
public class DocumentPrefixMasterController {

    @Autowired
    private
    DocumentPrefixMasterService documentPrefixMasterService;

    /**
     * This method Retrieves the Document Prefix Master
     *
     * @return
     */
    @RequestMapping(value = "/get/search", method = RequestMethod.POST)
    @Secured("ROLE_MASTER.GENERAL.DOCUMENT_PREFIX.LIST")
    public BaseDto search(@RequestBody DocumentPrefixSearchDto searchDto) {
        log.info("Search method called.[" + searchDto + "]");

        BaseDto baseDto = documentPrefixMasterService.search(searchDto);
        log.info("Search method called." + baseDto);
        return baseDto;
    }

    /**
     * This method Retrieves the Document Prefix Master By Id
     *
     * @param id
     * @return
     */
    @RequestMapping(value = "/get/id/{id}", method = RequestMethod.GET)
    @Secured("ROLE_MASTER.GENERAL.DOCUMENT_PREFIX.VIEW")
    public BaseDto get(@PathVariable Long id) {
        log.info("Get by id method called..[" + id + "]");
        return documentPrefixMasterService.get(id);
    }


    @RequestMapping(value = "/get/formula/doctype/locationcode/{doctype}/{locationcode}", method = RequestMethod.GET)
    public DocPrefixTypeMapping get(@PathVariable DocPrefixDocumentType doctype, @PathVariable String locationcode) {
        return documentPrefixMasterService.getFormula(doctype, locationcode);
    }

    /**
     * This method Persists the Document Prefix Master
     *
     * @param documentPrefixMaster
     * @return
     */
    @RequestMapping(value = "/create", method = RequestMethod.POST)
    @Secured("ROLE_MASTER.GENERAL.DOCUMENT_PREFIX.CREATE")
    public BaseDto save(@RequestBody DocumentPrefixMaster documentPrefixMaster) {
        log.info("While sending DocumentPrefixMaster  : ");
        return documentPrefixMasterService.create(documentPrefixMaster);
    }

    /**
     * This method Updates the Existing Document Prefix Master
     *
     * @param existingDocumentPrefixMaster
     * @return
     */
    @RequestMapping(value = "/update", method = RequestMethod.POST)
    @Secured("ROLE_MASTER.GENERAL.DOCUMENT_PREFIX.MODIFY")
    public BaseDto update(@RequestBody DocumentPrefixMaster existingDocumentPrefixMaster) {
        log.info("Update method called.[" + existingDocumentPrefixMaster.getId() + "]");

        return documentPrefixMasterService.update(existingDocumentPrefixMaster);
    }

    /**
     * This method deletes the Document Prefix Master By Id
     *
     * @param id
     * @return
     */
    @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
    @Secured("ROLE_MASTER.GENERAL.DOCUMENT_PREFIX.DELETE")
    public BaseDto delete(@PathVariable("id") Long id) {
        log.info("Delete method called.[" + id + "]");
        return documentPrefixMasterService.delete(id);
    }

}
