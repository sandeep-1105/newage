package com.efreightsuite.controller;

import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.model.PartyEmailMessageMapping;
import com.efreightsuite.service.PartyEmailMessageMappingService;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Devendrachary M
 */

@RestController
@Log4j2
@RequestMapping(value = "/api/v1/partyemailmessagemapping")
public class PartyEmailMessageMappingController {

    @Autowired
    private
    PartyEmailMessageMappingService partyEmailMessageMappingService;

    /**
     * This method Retrieves the Party Email Message Mapping By Id
     *
     * @param id
     * @return
     */
    @RequestMapping(value = "/get/id/{id}", method = RequestMethod.GET)
    public BaseDto get(@PathVariable Long id) {
        log.info("Get by id method called..[" + id + "]");
        return partyEmailMessageMappingService.get(id);
    }

    /**
     * This method Persists the Party Email Message Mapping
     *
     * @param partyEmailMessageMapping
     * @return
     */
    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public BaseDto save(@RequestBody PartyEmailMessageMapping partyEmailMessageMapping) {
        log.info("While sending PartyEmailMessageMapping  : " + partyEmailMessageMapping);
        return partyEmailMessageMappingService.create(partyEmailMessageMapping);
    }

    /**
     * This method Updates the Existing Party Email Message Mapping
     *
     * @param existingPartyEmailMessageMapping
     * @return
     */
    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public BaseDto update(@RequestBody PartyEmailMessageMapping existingPartyEmailMessageMapping) {
        log.info("Update method called.[" + existingPartyEmailMessageMapping.getId() + "]");
        return partyEmailMessageMappingService.update(existingPartyEmailMessageMapping);
    }

    /**
     * To get the PartyEmailMessageMapping all
     *
     * @return BaseDto
     */
    @RequestMapping(value = "/getall", method = RequestMethod.GET)
    public BaseDto getall() {
        log.info("getall method called.");
        return partyEmailMessageMappingService.getAll();
    }

    @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
    public BaseDto delete(@PathVariable("id") Long id) {
        log.info("Delete method called.[" + id + "]");
        return partyEmailMessageMappingService.delete(id);
    }


}
