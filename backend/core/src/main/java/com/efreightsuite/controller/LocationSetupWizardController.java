package com.efreightsuite.controller;

import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.FileUploadDto;
import com.efreightsuite.model.common.LocationSetup;
import com.efreightsuite.service.CommonErrorMessageService;
import com.efreightsuite.service.LocationSetupWizardService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/public/api/v1/locationsetup")
public class LocationSetupWizardController {

    private Logger logger = LoggerFactory.getLogger(LocationSetupWizardController.class);

    @Autowired
    private
    LocationSetupWizardService locationSetupWizardService;

    @Autowired
    private
    CommonErrorMessageService commonErrorMessageService;

    @RequestMapping(value = "/errors", method = RequestMethod.GET)
    public BaseDto getAllErrorMessage() {
        logger.info("LocationSetupWizardController -> getAllErrorMessage method called");
        return commonErrorMessageService.findAll();
    }

    @RequestMapping(value = "/reg_exp", method = RequestMethod.GET)
    public BaseDto getAllCommonRegularExpression() {
        logger.info("LocationSetupWizardController -> getAllCommonRegularExpression method called");
        return locationSetupWizardService.getAllCommonRegularExpression();
    }


    @RequestMapping(value = "/configurations", method = RequestMethod.GET)
    public BaseDto getAllCommonConfigurations() {
        logger.info("LocationSetupWizardController -> getAllCommonConfigurations method called");
        return locationSetupWizardService.getAllCommonConfigurations();
    }


    @RequestMapping(value = "/create_company", method = RequestMethod.POST)
    public BaseDto createOrUpdate(@RequestBody LocationSetup locationSetup) {
        logger.info("Saving LocationSetup data for section {}", locationSetup.getCurrentSection());
        return locationSetupWizardService.createOrUpdate(locationSetup);
    }

    @RequestMapping(value = "/is_company_unique/{companyName}/{id}", method = RequestMethod.GET)
    public ResponseEntity<BaseDto> isCompanyUnique(@PathVariable("companyName") String companyName, @PathVariable("id") Long id) {
        return locationSetupWizardService.isCompanyUnique(companyName);
    }


    @RequestMapping(value = "/get/{id}", method = RequestMethod.GET)
    public BaseDto get(@PathVariable("id") Long id) {
        logger.info("LocationSetupWizardController -> get method called.[" + id + "]");
        return locationSetupWizardService.get(id);
    }


    @RequestMapping(value = "/activate/{key}/{id}", method = RequestMethod.GET)
    public BaseDto activate(@PathVariable("key") String key, @PathVariable("id") Long id) {
        logger.info("LocationSetupWizardController -> Activation Key : [" + key + "] Location Setup ID : " + id);
        return locationSetupWizardService.activate(key, id);
    }

    @RequestMapping(value = "/is_user_active/{id}", method = RequestMethod.GET)
    public BaseDto isUserActivated(@PathVariable("id") Long id) {
        logger.info("LocationSetupWizardController -> Location Setup ID : " + id);
        return locationSetupWizardService.isUserActivated(id);
    }

    @RequestMapping(value = "/is_phone_number_valid/{countryCode}/{phoneNumber}", method = RequestMethod.GET)
    public ResponseEntity<BaseDto> isPhoneNumberValid(@PathVariable("countryCode") String countryCode, @PathVariable("phoneNumber") String phoneNumber) {
        return locationSetupWizardService.isPhoneNumberValid(countryCode, phoneNumber);
    }

    @RequestMapping(value = "/phone_number_code/{countryCode}", method = RequestMethod.GET)
    public ResponseEntity<BaseDto> getCountryCallingCode(@PathVariable("countryCode") String countryCode) {
        return locationSetupWizardService.getCallingCode(countryCode);
    }


    @RequestMapping(value = "/validate_file", method = RequestMethod.POST)
    public BaseDto validateFiles(@RequestBody FileUploadDto fileUploadDto) {
        logger.info("validateFiles is called ");
        return locationSetupWizardService.validateFiles(fileUploadDto);
    }

    @RequestMapping(value = "/loadsequencefromexcel", method = RequestMethod.GET)
    public BaseDto loadSequenceFromExcel() {
        logger.info("loadSequenceFromExcel is called.");
        return locationSetupWizardService.loadSequenceFromExcel();
    }


}
