package com.efreightsuite.controller;

import javax.servlet.http.HttpServletResponse;

import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.PurchaseOrderSearchDto;
import com.efreightsuite.model.PurchaseOrder;
import com.efreightsuite.service.PurchaseOrderService;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Log4j2
@RequestMapping(value = "/api/v1/purchaseorder")
public class PurchaseOrderController {

    @Autowired
    private
    PurchaseOrderService purchaseOrderService;

    @RequestMapping(method = RequestMethod.POST)
    @Secured("ROLE_CRM.PURCHASE_ORDER.CREATE")
    public BaseDto create(@RequestBody PurchaseOrder purchaseOrder) {
        log.info("PurchaseOrderController -> create method called.");
        return purchaseOrderService.create(purchaseOrder);
    }

    @RequestMapping(method = RequestMethod.PUT)
    @Secured("ROLE_CRM.PURCHASE_ORDER.MODIFY")
    public BaseDto update(@RequestBody PurchaseOrder purchaseOrder) {
        log.info("PurchaseOrderController -> update method called.");
        return purchaseOrderService.update(purchaseOrder);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @Secured("ROLE_CRM.PURCHASE_ORDER.VIEW")
    public BaseDto getById(@PathVariable("id") Long id) {
        log.info("PurchaseOrderController -> getById method called.");
        return purchaseOrderService.getById(id);
    }

    @RequestMapping(value = "/getbypono/{poNo}", method = RequestMethod.GET)
    @Secured("ROLE_CRM.PURCHASE_ORDER.VIEW")
    public BaseDto getById(@PathVariable("poNo") String poNo) {
        log.info("PurchaseOrderController -> get by poNo method called.");
        return purchaseOrderService.getByPoNo(poNo);
    }


    @RequestMapping(value = "/search", method = RequestMethod.POST)
    @Secured("ROLE_CRM.PURCHASE_ORDER.LIST")
    public BaseDto searchByCriteria(@RequestBody PurchaseOrderSearchDto searchData) {
        log.info("PurchaseOrderController -> searchByCriteria method called.");
        return purchaseOrderService.searchByCriteria(searchData);
    }

    @RequestMapping(value = "/files/{id}/{type}/{swapped}", method = RequestMethod.GET)
    public void getFile(@PathVariable Long id, @PathVariable String type, @PathVariable("swapped") boolean swapped, HttpServletResponse response) {
        purchaseOrderService.downloadAttachment(id, type, swapped, response);
    }


}
