package com.efreightsuite.controller;

import java.util.List;

import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.ReportMasterSearchDto;
import com.efreightsuite.model.ReportMaster;
import com.efreightsuite.service.ReportMasterService;
import groovy.util.logging.Log4j;
import org.jfree.util.Log;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "api/v1/reportmaster")
@Log4j
public class ReportMasterController {

    @Autowired
    private
    ReportMasterService reportMasterService;

    @RequestMapping(value = "/get/search/keyword", method = RequestMethod.POST)
    @Secured("ROLE_SETUP.REPORT_CONFIGURATION_REPORT_MASTER.LIST")
    public BaseDto search(@RequestBody ReportMasterSearchDto searchDto) {
        return reportMasterService.search(searchDto);
    }


    @RequestMapping(value = "/get/all", method = RequestMethod.GET)
    public BaseDto getAll() {
        Log.info("report master get all called");
        return reportMasterService.getAll();
    }

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    @Secured("ROLE_SETUP.REPORT_CONFIGURATION_REPORT_MASTER.CREATE")
    public BaseDto save(@RequestBody List<ReportMaster> reportMaster) {
        return reportMasterService.create(reportMaster);
    }

    @Secured("ROLE_SETUP.REPORT_CONFIGURATION_REPORT_MASTER.MODIFY")
    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public BaseDto update(@RequestBody ReportMaster carrierRateMaster) {
        return reportMasterService.update(carrierRateMaster);
    }

    @Secured("ROLE_SETUP.REPORT_CONFIGURATION_REPORT_MASTER.DELETE")
    @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
    public BaseDto delete(@PathVariable("id") Long id) {
        return reportMasterService.delete(id);
    }


}
