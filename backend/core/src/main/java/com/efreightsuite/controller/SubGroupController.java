package com.efreightsuite.controller;

import java.util.List;

import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.GroupSubGroupDto;
import com.efreightsuite.model.GeneralLedgerSubGroup;
import com.efreightsuite.service.SubGroupMasterService;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


@RestController
@Log4j2
@RequestMapping(value = "/api/v1/subgroupmaster")
public class SubGroupController {

    @Autowired
    private
    SubGroupMasterService subGroupMasterService;

    @RequestMapping(value = "/get/search/keyword", method = RequestMethod.POST)
    //@Secured("ROLE_MASTER.GENERAL.SUBGROUP.LIST")
    public BaseDto search(@RequestBody GroupSubGroupDto searchDto) {
        log.info("Search method called.");
        return subGroupMasterService.search(searchDto);
    }

    @RequestMapping(value = "/get/id/{id}", method = RequestMethod.GET)
    @Secured("ROLE_MASTER.GENERAL.SUBGROUP.VIEW")
    public BaseDto getBySubGroupId(@PathVariable Long id) {
        log.info("SubGroupMaster get method called..");
        return subGroupMasterService.get(id);
    }


    @RequestMapping(value = "/create", method = RequestMethod.POST)
    @Secured("ROLE_MASTER.GENERAL.SUBGROUP.CREATE")
    public BaseDto create(@RequestBody List<GeneralLedgerSubGroup> SubGroupMaster) {
        log.info("Create method is called.subgroup.");
        return subGroupMasterService.create(SubGroupMaster);
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    @Secured("ROLE_MASTER.GENERAL.SUBGROUP.MODIFY")
    public BaseDto update(@RequestBody List<GeneralLedgerSubGroup> SubGroupMaster) {
        log.info("Update method is called..subgroupmaster");
        return subGroupMasterService.update(SubGroupMaster);
    }


    @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
    @Secured("ROLE_MASTER.GENERAL.SUBGROUP.DELETE")
    public BaseDto delete(@PathVariable Long id) {
        log.info("Delete method called sub group");
        return subGroupMasterService.delete(id);
    }


}
