package com.efreightsuite.controller;

import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.PartyTypeSearchDto;
import com.efreightsuite.dto.SearchRequest;
import com.efreightsuite.model.PartyTypeMaster;
import com.efreightsuite.service.PartyTypeMasterService;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Log4j2
@RequestMapping(value = "/api/v1/partytypemaster")
public class PartyTypeMasterController {

    @Autowired
    private
    PartyTypeMasterService partyTypeMasterService;


    @RequestMapping(value = "/get/all", method = RequestMethod.GET)
    public BaseDto getAll() {
        log.info("Get All method called.");
        return partyTypeMasterService.getAll();
    }

    @RequestMapping(value = "/get/search", method = RequestMethod.POST)
    @Secured("ROLE_MASTER.CRM.PARTY_TYPE.LIST")
    public BaseDto search(@RequestBody PartyTypeSearchDto partyTypeSearchDto) {
        log.info("Search method called.");
        return partyTypeMasterService.search(partyTypeSearchDto);
    }


    @RequestMapping(value = "/get/search/keyword", method = RequestMethod.POST)
    public BaseDto search(@RequestBody SearchRequest searchRequest) {
        return partyTypeMasterService.search(searchRequest);
    }

    @RequestMapping(value = "/get/id/{id}", method = RequestMethod.GET)
    @Secured("ROLE_MASTER.CRM.PARTY_TYPE.VIEW")
    public BaseDto getByPackId(@PathVariable Long id) {
        log.info("getByPackId method is called...ID = " + id);
        return partyTypeMasterService.get(id);
    }


    @RequestMapping(value = "/create", method = RequestMethod.POST)
    @Secured("ROLE_MASTER.CRM.PARTY_TYPE.CREATE")
    public BaseDto create(@RequestBody PartyTypeMaster partyTypeMaster) {
        log.info("Create method is called..");
        return partyTypeMasterService.create(partyTypeMaster);
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    @Secured("ROLE_MASTER.CRM.PARTY_TYPE.MODIFY")
    public BaseDto update(@RequestBody PartyTypeMaster existingPartyTypeMaster) {
        log.info("Update method is called.. " + existingPartyTypeMaster);
        return partyTypeMasterService.update(existingPartyTypeMaster);
    }

    @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
    @Secured("ROLE_MASTER.CRM.PARTY_TYPE.DELETE")
    public BaseDto delete(@PathVariable Long id) {
        log.info("Delete method is called.. : [" + id + "]");
        return partyTypeMasterService.delete(id);
    }


}
