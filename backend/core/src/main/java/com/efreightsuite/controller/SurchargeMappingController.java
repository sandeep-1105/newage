package com.efreightsuite.controller;

import java.util.List;

import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.SurchargeMappingSearchDto;
import com.efreightsuite.model.SurchargeMapping;
import com.efreightsuite.service.SurchargeMappingService;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/api/v1/surchargemapping")
@Log4j2
public class SurchargeMappingController {

    /**
     * SurchargeMappingService Object
     */

    @Autowired
    private
    SurchargeMappingService surchargeMappingService;

    /**
     * Method - getById Returns SurchargeMapping based on given pack id
     *
     * @return SurchargeMapping
     */

    @RequestMapping(value = "/get/id/{id}", method = RequestMethod.GET)
    public BaseDto getById(@PathVariable Long id) {

        log.info("getById method is called...ID = " + id);

        return surchargeMappingService.getById(id);
    }


    @RequestMapping(value = "/get/search/keyword", method = RequestMethod.POST)
    @Secured("ROLE_SETUP.APP_CONFIGURATION.SURCHARGE_MAPPING.LIST")
    public BaseDto search(@RequestBody SurchargeMappingSearchDto searchDto) {

        log.info("Search method is called...Search Dto: " + searchDto);

        return surchargeMappingService.search(searchDto);
    }

    /**
     * Method - create Creates and returns a new surcharge mapping
     *
     * @returns SurchargeMapping
     */

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    @Secured("ROLE_SETUP.APP_CONFIGURATION.SURCHARGE_MAPPING.CREATE")
    public BaseDto create(@RequestBody List<SurchargeMapping> surchargeMappingList) {
        log.info("Create method is called.");
        return surchargeMappingService.create(surchargeMappingList);
    }

    /**
     * Method - update returns a updated surcharge mapping
     *
     * @param existingSurchargeMapping
     * @returns SurchargeMapping
     */

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    @Secured("ROLE_SETUP.APP_CONFIGURATION.SURCHARGE_MAPPING.MODIFY")
    public BaseDto update(@RequestBody SurchargeMapping existingSurchargeMapping) {

        log.info("Create method is called.. SurchargeMapping : " + existingSurchargeMapping);

        return surchargeMappingService.update(existingSurchargeMapping);
    }

    @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
    @Secured("ROLE_SETUP.APP_CONFIGURATION.SURCHARGE_MAPPING.DELETE")
    public BaseDto delete(@PathVariable Long id) {

        log.info("Delete method is called.. : [" + id + "]");

        return surchargeMappingService.delete(id);
    }
}
