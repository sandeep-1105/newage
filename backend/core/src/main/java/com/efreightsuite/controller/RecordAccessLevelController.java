package com.efreightsuite.controller;

import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.RecordAccessLevelDTO;
import com.efreightsuite.dto.RecordAccessLevelSearchDto;
import com.efreightsuite.service.RecordAccessLevelService;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Log4j2
@RequestMapping(value = "/api/v1/recordaccesslevel")
public class RecordAccessLevelController {

    @Autowired
    private
    RecordAccessLevelService recordAccessLevelService;

	/*@RequestMapping(value = "/get/user/{id}", method = RequestMethod.GET)
	public BaseDto getRecordAccessLevelForAUser(@PathVariable Long id) {
		log.info("Get by id method called.[" + id + "]");
		return recordAccessLevelService.getRecordAccessLevelForAUser(id);
	}*/


    @RequestMapping(value = "/get/id/{id}", method = RequestMethod.GET)
    @Secured("ROLE_SETUP.RECORD_ACCESS_LEVEL.VIEW")
    public BaseDto get(@PathVariable Long id) {
        log.info("Get by id method called.[" + id + "]");
        return recordAccessLevelService.get(id);
    }

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    @Secured({"ROLE_SETUP.RECORD_ACCESS_LEVEL.CREATE", "ROLE_SETUP.RECORD_ACCESS_LEVEL.MODIFY"})
    public BaseDto save(@RequestBody RecordAccessLevelDTO recordAccessLevelDto) {
        log.info("Create method called.");
        return recordAccessLevelService.create(recordAccessLevelDto);
    }

    @RequestMapping(value = "/search", method = RequestMethod.POST)
    @Secured("ROLE_SETUP.RECORD_ACCESS_LEVEL.LIST")
    public BaseDto search(@RequestBody RecordAccessLevelSearchDto searchDto) {
        log.info("RecordAccessLevelController -> search method called.");
        return recordAccessLevelService.search(searchDto);
    }

    @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
    @Secured("ROLE_SETUP.RECORD_ACCESS_LEVEL.DELETE")
    public BaseDto delete(@PathVariable("id") Long id) {
        log.info("Delete method called.[" + id + "]");
        return recordAccessLevelService.delete(id);
    }


}
