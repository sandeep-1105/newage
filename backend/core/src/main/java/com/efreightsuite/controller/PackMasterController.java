package com.efreightsuite.controller;

import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.PackSearchDto;
import com.efreightsuite.dto.SearchRequest;
import com.efreightsuite.model.PackMaster;
import com.efreightsuite.service.PackMasterService;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/api/v1/packmaster")
@Log4j2
public class PackMasterController {

    /**
     * PackMasterService Object
     */

    @Autowired
    private
    PackMasterService packMasterService;

    /**
     * Method - getById Returns PackMaster based on given pack id
     *
     * @return PackMaster
     */

    @RequestMapping(value = "/get/id/{id}", method = RequestMethod.GET)
    @Secured("ROLE_MASTER.GENERAL.PACK.VIEW")
    public BaseDto getByPackId(@PathVariable Long id) {

        log.info("getByPackId method is called...ID = " + id);

        return packMasterService.getByPackId(id);
    }

    /**
     * Method - getByPackCode Returns PackMaster based on given pack code
     *
     * @return PackMaster
     */

    @RequestMapping(value = "/get/code/{packCode}", method = RequestMethod.GET)
    public BaseDto getByPackCode(@PathVariable String packCode) {

        log.info("getByPackCode method is called...CODE = " + packCode);

        return packMasterService.getByPackCode(packCode);
    }

    /**
     * Method - search Returns Paginated Search Result based on Search Keyword
     *
     * @return SearchRespDto
     */

    @RequestMapping(value = "/get/search", method = RequestMethod.POST)
    @Secured("ROLE_MASTER.GENERAL.PACK.LIST")
    public BaseDto search(@RequestBody PackSearchDto searchDto) {

        log.info("Search method is called...Search Dto: " + searchDto);

        return packMasterService.search(searchDto);
    }

    @RequestMapping(value = "/get/search/keyword", method = RequestMethod.POST)
    public BaseDto search(@RequestBody SearchRequest searchRequest) {

        log.info("Search method is called...Search Dto: " + searchRequest);

        return packMasterService.search(searchRequest);
    }

    /**
     * Method - create Creates and returns a new pack master
     *
     * @returns PackMaster
     */

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    @Secured("ROLE_MASTER.GENERAL.PACK.CREATE")
    public BaseDto create(@RequestBody PackMaster packMaster) {

        log.info("Create method is called.. PackMaster : " + packMaster);

        return packMasterService.create(packMaster);
    }

    /**
     * Method - update returns a updated pack master
     *
     * @param existingPackMaster
     * @returns PackMaster
     */

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    @Secured("ROLE_MASTER.GENERAL.PACK.MODIFY")
    public BaseDto update(@RequestBody PackMaster existingPackMaster) {

        log.info("Create method is called.. PackMaster : " + existingPackMaster);

        return packMasterService.update(existingPackMaster);
    }

    @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
    @Secured("ROLE_MASTER.GENERAL.PACK.DELETE")
    public BaseDto delete(@PathVariable Long id) {

        log.info("Delete method is called.. : [" + id + "]");

        return packMasterService.delete(id);
    }
}
