package com.efreightsuite.controller;

import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.ServiceTaxPercentageSearchDto;
import com.efreightsuite.model.ServiceTaxPercentage;
import com.efreightsuite.service.ServiceTaxPercentageService;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Praveen
 */
@RestController
@Log4j2
@RequestMapping(value = "/api/v1/servicetaxpercentage")
public class ServiceTaxPercentageController {


    /**
     * ServiceTaxPercentage Object
     */

    @Autowired
    private
    ServiceTaxPercentageService serviceTaxPercentageService;


    /**
     * Method - search
     * API to get the search ServiceTax Percentage
     *
     * @return BaseDto
     */
    @RequestMapping(value = "get/search", method = RequestMethod.POST)
    public BaseDto search(@RequestBody ServiceTaxPercentageSearchDto serviceTaxPercentageSearchDto) {
        log.info("search method called.");
        return serviceTaxPercentageService.search(serviceTaxPercentageSearchDto);
    }


    /**
     * Method - getAll
     * API to get the ServiceTax Percentage
     *
     * @return BaseDto
     */

    @RequestMapping(value = "/get/getall", method = RequestMethod.GET)
    public BaseDto getAll() {
        log.info("getall method called.");
        return serviceTaxPercentageService.getAll();
    }


    /**
     * Method - getById
     * API to get By Id
     *
     * @param id
     * @return BaseDto
     */

    @RequestMapping(value = "/get/id/{id}", method = RequestMethod.GET)
    public BaseDto get(@PathVariable Long id) {
        log.info("ServiceTaxPercentageController -> getById method is called....." + id);
        return serviceTaxPercentageService.getById(id);
    }


    /**
     * Method - Create
     * API to create the ServiceTaxPercentage
     *
     * @return BaseDto
     */

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public BaseDto create(@RequestBody ServiceTaxPercentage serviceTaxPercentage) {
        log.info("ServiceTaxPercentageController -> create method called.");
        return serviceTaxPercentageService.create(serviceTaxPercentage);
    }


    /**
     * Method - Update
     * API to update the existing ServiceTaxPercentage
     *
     * @return BaseDto
     */
    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public BaseDto update(@RequestBody ServiceTaxPercentage serviceTaxPercentage) {
        log.info("ServiceTaxPercentageController -> Update method is called.");
        return serviceTaxPercentageService.update(serviceTaxPercentage);
    }

    /**
     * To Delete the ServiceTaxPercentage Object Based on ID
     *
     * @param id
     * @return BaseDto
     */
    @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
    public BaseDto delete(@PathVariable Long id) {
        log.info("Delete method called....");
        return serviceTaxPercentageService.delete(id);
    }
}
