package com.efreightsuite.controller;

import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.model.ServiceChargeTaxCategoryMapping;
import com.efreightsuite.service.ServiceTaxChargeService;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Praveen
 */

@RestController
@Log4j2
@RequestMapping(value = "/api/v1/servicetaxcharge")
public class ServiceTaxChargeController {


    /**
     * ServiceTaxCharge Object
     */

    @Autowired
    private
    ServiceTaxChargeService serviceTaxChargeService;

    /**
     * Method - getAll
     * API to get the ServiceTax Charge
     *
     * @return BaseDto
     */

    @RequestMapping(value = "/get/getall", method = RequestMethod.GET)
    public BaseDto getAll() {
        log.info("getall method called.");
        return serviceTaxChargeService.getAll();
    }


    /**
     * Method - getById
     * API to get By Id
     *
     * @param id
     * @return BaseDto
     */

    @RequestMapping(value = "/get/id/{id}", method = RequestMethod.GET)
    public BaseDto get(@PathVariable Long id) {
        log.info("ServiceTaxChargeController -> getById method is called....." + id);
        return serviceTaxChargeService.getById(id);
    }


    /**
     * Method - Create
     * API to create the ServiceTaxChargeController
     *
     * @return BaseDto
     */

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public BaseDto create(@RequestBody ServiceChargeTaxCategoryMapping serviceChargeTaxCategoryMapping) {
        log.info("ServiceTaxChargeController -> create method called.");
        return serviceTaxChargeService.create(serviceChargeTaxCategoryMapping);
    }


    /**
     * Method - Update
     * API to update the existing ServiceTaxChargeController
     *
     * @return BaseDto
     */
    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public BaseDto update(@RequestBody ServiceChargeTaxCategoryMapping serviceChargeTaxCategoryMapping) {
        log.info("ServiceTaxChargeController -> Update method is called.");
        return serviceTaxChargeService.update(serviceChargeTaxCategoryMapping);
    }

    /**
     * To Delete the ServiceTaxCharge Object Based on ID
     *
     * @param id
     * @return BaseDto
     */
    @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
    public BaseDto delete(@PathVariable Long id) {
        log.info("Delete method called....");
        return serviceTaxChargeService.delete(id);
    }
}
