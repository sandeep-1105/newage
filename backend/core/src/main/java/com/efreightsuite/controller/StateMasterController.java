package com.efreightsuite.controller;

import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.SearchRequest;
import com.efreightsuite.dto.StateSearchDto;
import com.efreightsuite.model.StateMaster;
import com.efreightsuite.service.StateMasterService;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


@RestController
@Log4j2
@RequestMapping(value = "/api/v1/statemaster")
public class StateMasterController {

    @Autowired
    private
    StateMasterService stateMasterService;

    @RequestMapping(value = "/get/search/keyword/{countryId}", method = RequestMethod.POST)
    public BaseDto getAll(@RequestBody SearchRequest searchRequest, @PathVariable Long countryId) {
        log.info("Search method called.");
        return stateMasterService.search(searchRequest, countryId);
    }

    @RequestMapping(value = "/get/search/keyword/code/{countryCode}", method = RequestMethod.POST)
    public BaseDto getAll(@RequestBody SearchRequest searchRequest, @PathVariable String countryCode) {
        log.info("Search method called.");
        return stateMasterService.search(searchRequest, countryCode);
    }

    @RequestMapping(value = "/get/search", method = RequestMethod.POST)
    @Secured("ROLE_MASTER.GENERAL.STATE_PROVINCE.LIST")
    public BaseDto getAll(@RequestBody StateSearchDto searchDto) {
        log.info("Search method called.");
        return stateMasterService.search(searchDto);
    }

    @RequestMapping(value = "/get/id/{id}", method = RequestMethod.GET)
    @Secured("ROLE_MASTER.GENERAL.STATE_PROVINCE.VIEW")
    public BaseDto get(@PathVariable Long id) {
        log.info("get method called." + id);
        return stateMasterService.get(id);
    }

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    @Secured({"ROLE_MASTER.GENERAL.STATE_PROVINCE.CREATE", "ROLE_MASTER.GENERAL.STATE_PROVINCE.MODIFY"})
    public BaseDto create(@RequestBody StateMaster stateMaster) {
        log.info("Create method is called.");
        return stateMasterService.saveOrUpdate(stateMaster);
    }

    @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
    @Secured("ROLE_MASTER.GENERAL.STATE_PROVINCE.DELETE")
    public BaseDto delete(@PathVariable Long id) {
        log.info("Delete method called....");
        return stateMasterService.delete(id);
    }
}
