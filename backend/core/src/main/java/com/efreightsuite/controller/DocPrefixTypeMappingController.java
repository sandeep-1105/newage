package com.efreightsuite.controller;

import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.service.DocPrefixTypeMappingService;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


@Log4j2
@RestController
@RequestMapping(value = "/api/v1/docPrefixTypemappingmaster")
public class DocPrefixTypeMappingController {


    @Autowired
    private
    DocPrefixTypeMappingService docPrefixTypeMappingService;


    @RequestMapping(value = "/get/id/{id}")
    public BaseDto getByPortId(@PathVariable Long id) {
        log.info("getByPackId is Invoked..");
        return docPrefixTypeMappingService.getById(id);
    }


    @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
    public BaseDto delete(@PathVariable("id") Long id) {
        log.info("Delete method called.[" + id + "]");
        return docPrefixTypeMappingService.delete(id);
    }


}
