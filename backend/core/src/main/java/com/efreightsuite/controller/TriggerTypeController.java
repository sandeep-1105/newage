package com.efreightsuite.controller;

import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.SearchRequest;
import com.efreightsuite.dto.TriggerTypeDto;
import com.efreightsuite.model.TriggerTypeMaster;
import com.efreightsuite.service.TriggerTypeMasterService;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Log4j2
@RequestMapping(value = "/api/v1/triggertypemaster")
public class TriggerTypeController {

    @Autowired
    private
    TriggerTypeMasterService triggerTypeMasterService;


    @RequestMapping(value = "/get/search", method = RequestMethod.POST)
    @Secured("ROLE_MASTER.CRM.TRIGGER_TYPE.LIST")
    public BaseDto search(@RequestBody TriggerTypeDto searchDto) {
        log.info("Search method called.");
        return triggerTypeMasterService.search(searchDto);
    }

    @RequestMapping(value = "/get/search/keyword", method = RequestMethod.POST)
    public BaseDto search(@RequestBody SearchRequest searchRequest) {
        log.info("Search method called.");
        return triggerTypeMasterService.search(searchRequest);
    }

    /**
     * To get the Trigger Type Master by Trigger Type Id
     *
     * @return BaseDto
     */
    @RequestMapping(value = "/get/id/{id}", method = RequestMethod.GET)
    @Secured("ROLE_MASTER.CRM.TRIGGER_TYPE.VIEW")
    public BaseDto get(@PathVariable Long id) {
        log.info("get method called.");
        return triggerTypeMasterService.get(id);
    }


    /**
     * To create the new TriggerTypeMaster
     *
     * @param triggerTypeMaster entity
     * @return BaseDto
     */
    @RequestMapping(value = "/create", method = RequestMethod.POST)
    @Secured("ROLE_MASTER.CRM.TRIGGER_TYPE.CREATE")
    public BaseDto create(@RequestBody TriggerTypeMaster triggerTypeMaster) {
        log.info("Create method called.");
        return triggerTypeMasterService.create(triggerTypeMaster);
    }

    /**
     * To update the triggerTypeMaster
     *
     * @return BaseDto
     */

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    @Secured("ROLE_MASTER.CRM.TRIGGER_TYPE.MODIFY")
    public BaseDto update(@RequestBody TriggerTypeMaster triggerTypeMaster) {
        log.info("Update method called.");
        return triggerTypeMasterService.update(triggerTypeMaster);
    }

    /**
     * To Delete the triggerTypeMaster Object Based on ID
     *
     * @param id
     * @return BaseDto
     */
    @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
    @Secured("ROLE_MASTER.CRM.TRIGGER_TYPE.DELETE")
    public BaseDto delete(@PathVariable Long id) {
        log.info("Delete method called....");
        return triggerTypeMasterService.delete(id);
    }

    /**
     * To get the TriggerTypeMaster all
     *
     * @return BaseDto
     */
    @RequestMapping(value = "/getall", method = RequestMethod.GET)
    public BaseDto get() {
        log.info("getall method called.");
        return triggerTypeMasterService.getAll();
    }
}
