package com.efreightsuite.controller;

import java.util.List;

import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.DefaultMasterSearchDto;
import com.efreightsuite.dto.EdiConfigrationSearchDto;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.model.EdiConfigurationMaster;
import com.efreightsuite.repository.EdiConfigurationMasterRepository;
import com.efreightsuite.service.EdiConfigurationMasterService;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Log4j2
@RequestMapping(value = "/api/v1/ediconfiguration")
public class EdiConfigurationController {

    @Autowired
    private
    EdiConfigurationMasterRepository ediConfigurationMasterRepository;

    @Autowired
    private
    EdiConfigurationMasterService ediConfigurationMasterService;

    @RequestMapping(value = "/get/uri/laststatus", method = RequestMethod.POST)
    public BaseDto search(@RequestBody DefaultMasterSearchDto searchDto) {
        BaseDto baseDto = new BaseDto();
        List<EdiConfigurationMaster> uri = ediConfigurationMasterRepository.findByKey("win-web-connect-request-last-status");
        if (uri != null && uri.get(0).getEdiConfigurationValue() != null) {
            baseDto.setResponseObject(uri.get(0).getEdiConfigurationValue());
        } else {
            baseDto.setResponseObject(null);
        }
        baseDto.setResponseCode(ErrorCode.SUCCESS);
        return baseDto;
    }

    @RequestMapping(value = "/get/id/{id}", method = RequestMethod.GET)
    public BaseDto get(@PathVariable Long id) {
        log.info("get method is called... [" + id + "]");
        return ediConfigurationMasterService.get(id);
    }

    @RequestMapping(value = "/get/search/{locationId}", method = RequestMethod.POST)
    public BaseDto search(@RequestBody EdiConfigrationSearchDto searchDto, @PathVariable Long locationId) {
        log.info("Search method is called...Search Dto: " + searchDto);
        return ediConfigurationMasterService.search(searchDto, locationId);
    }

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    //@Secured("ROLE_SETUP.APP_CONFIGURATION.EDI_CONFIG.CREATE")
    public BaseDto create(@RequestBody List<EdiConfigurationMaster> ediConfigurationMasterList) {
        log.info("Create method is called..");
        return ediConfigurationMasterService.create(ediConfigurationMasterList);
    }


    @RequestMapping(value = "/update", method = RequestMethod.POST)
    //@Secured("ROLE_SETUP.APP_CONFIGURATION.EDI_CONFIG.UPDATE")
    public BaseDto create(@RequestBody EdiConfigurationMaster ediConfigurationMaster) {
        log.info("Update method is called..");
        return ediConfigurationMasterService.update(ediConfigurationMaster);
    }

}
