package com.efreightsuite.controller;

import java.util.List;

import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.CurrencyRateSearchDto;
import com.efreightsuite.model.CurrencyRateMaster;
import com.efreightsuite.service.CurrencyRateMasterService;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


@RestController
@Log4j2
@RequestMapping(value = "/api/v1/currencyratemaster")
public class CurrencyRateMasterController {


    @Autowired
    private
    CurrencyRateMasterService currencyRateMasterService;


    @RequestMapping(value = "/get/id/{id}", method = RequestMethod.GET)
    @Secured("ROLE_MASTER.FINANCE.CURRENCY_RATE.VIEW")
    public BaseDto getByCurrencyId(@PathVariable Long id) {
        log.info("getByCurrencyId is called." + id + "]");
        return currencyRateMasterService.getByCurrencyRateMasterId(id);
    }

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    @Secured("ROLE_MASTER.FINANCE.CURRENCY_RATE.CREATE")
    public BaseDto save(@RequestBody List<CurrencyRateMaster> currencyRateMasterList) {
        log.info("Create method called..");
        return currencyRateMasterService.create(currencyRateMasterList);
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    @Secured("ROLE_MASTER.FINANCE.CURRENCY_RATE.MODIFY")
    public BaseDto update(@RequestBody List<CurrencyRateMaster> currencyRateMasterList) {
        return currencyRateMasterService.update(currencyRateMasterList);
    }

    @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
    @Secured("ROLE_MASTER.FINANCE.CURRENCY_RATE.DELETE")
    public BaseDto delete(@PathVariable Long id) {
        log.info("Delete method is called...[" + id + "]");
        return currencyRateMasterService.delete(id);
    }

    @RequestMapping(value = "/get/search", method = RequestMethod.POST)
    @Secured("ROLE_MASTER.FINANCE.CURRENCY_RATE.LIST")
    public BaseDto search(@RequestBody CurrencyRateSearchDto searchDto) {
        return currencyRateMasterService.search(searchDto);
    }

    @RequestMapping(value = "/get/fromCurrency/{fromCurrencyId}", method = RequestMethod.GET)
    public BaseDto search(@PathVariable Long fromCurrencyId) {
        return currencyRateMasterService.search(fromCurrencyId);
    }

}
