package com.efreightsuite.controller;

import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.ConsolDto;
import com.efreightsuite.dto.WinConfigurationRequestDto;
import com.efreightsuite.service.WinConfigurationService;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Log4j2
@RequestMapping(value = "/api/v1/winconfiguration")
public class WinConfigurationController {

    @Autowired
    private
    WinConfigurationService winConfigurationService;


    @RequestMapping(value = "/winwebconnect", method = RequestMethod.POST)
    public BaseDto sendToWin(@RequestBody ConsolDto consolDto) {
        log.info("Win web connect sending ...");
        return winConfigurationService.sendToWin(consolDto);
    }


    @RequestMapping(value = "/get/search/keyword", method = RequestMethod.POST)
    public BaseDto search(@RequestBody WinConfigurationRequestDto searchDto) {
        log.info("Search method called.");
        return winConfigurationService.search(searchDto);
    }

}
