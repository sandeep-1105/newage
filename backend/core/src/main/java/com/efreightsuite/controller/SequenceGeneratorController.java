package com.efreightsuite.controller;

import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.SequenceGeneratorSearchDto;
import com.efreightsuite.enumeration.SequenceType;
import com.efreightsuite.model.LocationMaster;
import com.efreightsuite.model.SequenceGenerator;
import com.efreightsuite.service.SequenceGeneratorService;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


@RestController
@Log4j2
@RequestMapping(value = "/api/v1/sequencegenerator")
public class SequenceGeneratorController {

    @Autowired
    private
    SequenceGeneratorService sequenceGeneratorService;


    @RequestMapping(value = "/get/id/{id}", method = RequestMethod.GET)
    public BaseDto get(@PathVariable Long id) {
        log.info("SequenceGeneratorController get by id method called.");
        return sequenceGeneratorService.get(id);
    }

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public BaseDto create(@RequestBody SequenceGenerator sequenceGenerator) {
        log.info("SequenceGeneratorController create method called.");
        return sequenceGeneratorService.create(sequenceGenerator);
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public BaseDto update(@RequestBody SequenceGenerator sequenceGenerator) {
        log.info("SequenceGeneratorControler update method called.");
        return sequenceGeneratorService.update(sequenceGenerator);
    }

    @RequestMapping(value = "/get/search/impl", method = RequestMethod.POST)
    public BaseDto search(@RequestBody SequenceGeneratorSearchDto dto) {
        log.info("getAll method called.");
        return sequenceGeneratorService.search(dto);
    }

    @RequestMapping(value = "/get/sequence/{sequenceType}", method = RequestMethod.POST)
    public String getSequence(@PathVariable SequenceType sequenceType, @RequestBody LocationMaster locationMaster) {
        return sequenceGeneratorService.getSequence(sequenceType, locationMaster);
    }

}
