package com.efreightsuite.controller;

import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.DivisionMasterSearchDto;
import com.efreightsuite.dto.SearchRequest;
import com.efreightsuite.model.DivisionMaster;
import com.efreightsuite.service.DivisionMasterService;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author achyutananda
 */
@RestController
@Log4j2
@RequestMapping(value = "/api/v1/divisionmaster")
public class DivisionMasterController {


    /**
     * DivisionMasterService Object
     */

    @Autowired
    private
    DivisionMasterService divisionMasterService;


    /**
     * Method - getAll
     * API to get the state By Id
     *
     * @return BaseDto
     */

    @RequestMapping(value = "/get/search/keyword", method = RequestMethod.POST)
    public BaseDto search(@RequestBody SearchRequest searchRequest) {
        log.info("getall method called.");
        return divisionMasterService.search(searchRequest);
    }

    @RequestMapping(value = "/get/search/exclude", method = RequestMethod.POST)
    public BaseDto searchDivisionNotInList(@RequestBody SearchRequest searchRequest) {
        log.info("Search method called. :");
        return divisionMasterService.searchDivisionNotInList(searchRequest);
    }

    @RequestMapping(value = "/get/getallcompanywise/{companyId}", method = RequestMethod.POST)
    public BaseDto getAll(@PathVariable Long companyId, @RequestBody SearchRequest searchRequest) {
        log.info("getall companyId method called.");
        return divisionMasterService.search(companyId, searchRequest);
    }

    @RequestMapping(value = "/get/search", method = RequestMethod.POST)
    @Secured("ROLE_MASTER.GENERAL.DIVISION.LIST")
    public BaseDto search(@RequestBody DivisionMasterSearchDto searchDto) {
        return divisionMasterService.search(searchDto);
    }

    /**
     * Method - getByDivisionId
     * API to get the division By Id
     *
     * @param id
     * @return BaseDto
     */

    @RequestMapping(value = "/get/id/{id}", method = RequestMethod.GET)
    @Secured("ROLE_MASTER.GENERAL.DIVISION.VIEW")
    public BaseDto get(@PathVariable Long id) {
        log.info("get method called." + id);
        return divisionMasterService.get(id);
    }


    /**
     * Method - Create
     * API to create the division
     *
     * @return BaseDto
     */
    @RequestMapping(value = "/create", method = RequestMethod.POST)
    @Secured("ROLE_MASTER.GENERAL.DIVISION.CREATE")
    public BaseDto create(@RequestBody DivisionMaster divisionMaster) {
        log.info("Create method is called.");
        return divisionMasterService.create(divisionMaster);
    }


    /**
     * Method - Update
     * API to update the existing division master
     *
     * @param divisionMaster
     * @return BaseDto
     */
    @RequestMapping(value = "/update", method = RequestMethod.POST)
    @Secured("ROLE_MASTER.GENERAL.DIVISION.MODIFY")
    public BaseDto update(@RequestBody DivisionMaster divisionMaster) {
        log.info("Update method is called.");
        return divisionMasterService.update(divisionMaster);
    }

    /**
     * To Delete the divisionMaster Object Based on ID
     *
     * @param id
     * @return BaseDto
     */
    @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
    @Secured("ROLE_MASTER.GENERAL.DIVISION.DELETE")
    public BaseDto delete(@PathVariable Long id) {
        log.info("Delete method called....");
        return divisionMasterService.delete(id);
    }
}
