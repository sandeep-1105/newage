package com.efreightsuite.controller;

import com.efreightsuite.activiti.ActivitiAirExportSearchService;
import com.efreightsuite.activiti.ActivitiAirImportSearchService;
import com.efreightsuite.activiti.ActivitiUtilService;
import com.efreightsuite.activiti.dto.ActivitiSearchDto;
import com.efreightsuite.activiti.dto.AirExportImportTaskCountDto;
import com.efreightsuite.activiti.dto.AirExportTaskCountDto;
import com.efreightsuite.activiti.dto.AirImportTaskCountDto;
import com.efreightsuite.dto.BaseDto;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


@RestController
@Log4j2
@RequestMapping(value = "/api/v1/activiti")
public class ActivitiController {

    @Autowired
    private
    ActivitiAirExportSearchService airExportSearchService;

    @Autowired
    private
    ActivitiAirImportSearchService airImportSearchService;

    @Autowired
    ActivitiUtilService activitiUtilService;

    @RequestMapping(value = "/airexport/getcount", method = RequestMethod.GET)
    public AirExportTaskCountDto getAirExportCount() {
        log.info("Search method called.");
        return airExportSearchService.getAirExportCount();
    }

    @RequestMapping(value = "/airexport/group/{groupName}", method = RequestMethod.POST)
    public BaseDto getAirExportTaskListByGroup(@RequestBody ActivitiSearchDto searchDto, @PathVariable("groupName") String groupName) {
        log.info("Search method called." + searchDto);
        return airExportSearchService.getAirExportTaskListByGroup(searchDto, groupName);
    }

    @RequestMapping(value = "/airimport/getcount", method = RequestMethod.GET)
    public AirImportTaskCountDto getAirImportCount() {
        log.info("Search method called.");
        return airImportSearchService.getAirImportCount();
    }

    @RequestMapping(value = "/airimport/group/{groupName}", method = RequestMethod.POST)
    public BaseDto getAirImportTaskListByGroup(@RequestBody ActivitiSearchDto searchDto, @PathVariable("groupName") String groupName) {
        log.info("Search method called.");
        return airImportSearchService.getAirImportTaskListByGroup(searchDto, groupName);
    }

    @RequestMapping(value = "/airexportimport/getcount", method = RequestMethod.GET)
    public AirExportImportTaskCountDto getAirExportImportCount() {
        log.info("Search air export import method called.");
        AirExportTaskCountDto airExportCount = airExportSearchService.getAirExportCount();
        AirImportTaskCountDto airImportCount = airImportSearchService.getAirImportCount();
        AirExportImportTaskCountDto exportImportTaskCountDto = new AirExportImportTaskCountDto();
        exportImportTaskCountDto.setAirExportTaskCountDto(airExportCount);
        exportImportTaskCountDto.setAirImportTaskCountDto(airImportCount);
        return exportImportTaskCountDto;
    }

}
