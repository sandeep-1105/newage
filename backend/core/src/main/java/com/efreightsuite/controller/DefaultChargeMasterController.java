package com.efreightsuite.controller;

import java.util.List;

import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.DefaultChargeRequestDto;
import com.efreightsuite.dto.FileUploadDto;
import com.efreightsuite.model.DefaultChargeMaster;
import com.efreightsuite.service.DefaultChargeMasterService;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Log4j2
@RequestMapping(value = "/api/v1/defaultcharge")
public class DefaultChargeMasterController {

    @Autowired
    private
    DefaultChargeMasterService defaultChargeService;


    @RequestMapping(value = "/search", method = RequestMethod.POST)
    public BaseDto search(@RequestBody DefaultChargeRequestDto dcDto) {
        log.info("Default charge Controller Search method called.");
        return defaultChargeService.search(dcDto);
    }

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public BaseDto create(@RequestBody List<DefaultChargeMaster> defaultList) {
        log.info("DefaultChargeMaster Controller Create method called.");
        return defaultChargeService.createUpdate(defaultList);
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public BaseDto update(@RequestBody List<DefaultChargeMaster> defaultList) {
        log.info("DefaultChargeMaster Controller update method called.");
        return defaultChargeService.createUpdate(defaultList);
    }

    @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
    public BaseDto delete(@PathVariable Long id) {
        log.info("Delete method called....[" + id + "]");
        return defaultChargeService.delete(id);
    }

    @RequestMapping(value = "/get/id/{id}", method = RequestMethod.GET)
    public BaseDto getByTosId(@PathVariable Long id) {
        log.info("defaultChargeService Id method called..[" + id + "]");
        return defaultChargeService.get(id);
    }

    @RequestMapping(value = "/upload", method = RequestMethod.POST)
    public BaseDto upload(@RequestBody FileUploadDto fileUploadDto) {
        log.info("defaultChargeService upload method called.");
        return defaultChargeService.uploadInPricing(fileUploadDto);
    }


}
