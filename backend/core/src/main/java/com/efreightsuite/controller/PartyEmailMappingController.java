package com.efreightsuite.controller;

import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.model.PartyEmailMapping;
import com.efreightsuite.service.PartyEmailMappingService;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Devendrachary M
 */

@RestController
@Log4j2
@RequestMapping(value = "/api/v1/partyemailmappingcontroller")
public class PartyEmailMappingController {

    @Autowired
    private
    PartyEmailMappingService partyEmailMappingService;

    /**
     * This method Retrieves the Party Email Mapping By Id
     *
     * @param id
     * @return
     */
    @RequestMapping(value = "/get/id/{id}", method = RequestMethod.GET)
    public BaseDto get(@PathVariable Long id) {
        log.info("Get by id method called..[" + id + "]");
        return partyEmailMappingService.get(id);
    }

    /**
     * This method Persists the Party Email Mapping
     *
     * @param enquiryLog
     * @return
     */
    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public BaseDto save(@RequestBody PartyEmailMapping enquiryLog) {
        log.info("While sending PartyEmailMapping  : " + enquiryLog);
        return partyEmailMappingService.create(enquiryLog);
    }

    /**
     * This method Updates the Existing Party Email Mapping
     *
     * @param existingPartyEmailMapping
     * @return
     */
    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public BaseDto update(@RequestBody PartyEmailMapping existingPartyEmailMapping) {
        log.info("Update method called.[" + existingPartyEmailMapping.getId() + "]");
        return partyEmailMappingService.update(existingPartyEmailMapping);
    }

    /**
     * To get the PartyEmailMessageMapping all
     *
     * @return BaseDto
     */
    @RequestMapping(value = "/getall", method = RequestMethod.GET)
    public BaseDto getall() {
        log.info("getall method called.");
        return partyEmailMappingService.getAll();
    }

    @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
    public BaseDto delete(@PathVariable("id") Long id) {
        log.info("Delete method called.[" + id + "]");
        return partyEmailMappingService.delete(id);
    }


}
