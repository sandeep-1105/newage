package com.efreightsuite.controller;

import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.SearchRequest;
import com.efreightsuite.dto.ValueAddedServicesSearchDto;
import com.efreightsuite.model.ValueAddedServices;
import com.efreightsuite.service.ValueAddedServicesService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/api/v1/valueaddedservices")
public class ValueAddedServicesController {

    @Autowired
    private
    ValueAddedServicesService valueAddedServicesService;

    @RequestMapping(value = "/get/id/{id}", method = RequestMethod.GET)
    @Secured("ROLE_MASTER.GENERAL.VALUE_ADDED_SERVICE.VIEW")
    public BaseDto getById(@PathVariable Long id) {
        return valueAddedServicesService.getById(id);
    }

    @RequestMapping(value = "/get/search", method = RequestMethod.POST)
    public BaseDto search(@RequestBody SearchRequest searchRequest) {
        return valueAddedServicesService.search(searchRequest);
    }

    @RequestMapping(value = "/get/all", method = RequestMethod.GET)
    public BaseDto getAll() {
        return valueAddedServicesService.getAll();
    }

    @RequestMapping(value = "/get/search/keyword", method = RequestMethod.POST)
    @Secured("ROLE_MASTER.GENERAL.VALUE_ADDED_SERVICE.LIST")
    public BaseDto search(@RequestBody ValueAddedServicesSearchDto searchDto) {
        return valueAddedServicesService.search(searchDto);
    }

    @RequestMapping(value = "/save", method = RequestMethod.POST)
    @Secured({"ROLE_MASTER.GENERAL.VALUE_ADDED_SERVICE.CREATE", "ROLE_MASTER.GENERAL.VALUE_ADDED_SERVICE.MODIFY"})
    public BaseDto saveOrUpdate(@RequestBody ValueAddedServices valueAddedServices) {
        return valueAddedServicesService.saveOrUpdate(valueAddedServices);
    }

    @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
    @Secured("ROLE_MASTER.GENERAL.VALUE_ADDED_SERVICE.DELETE")
    public BaseDto delete(@PathVariable Long id) {
        return valueAddedServicesService.delete(id);
    }
}
