package com.efreightsuite.controller;

import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.EventSearchDto;
import com.efreightsuite.dto.SearchRequest;
import com.efreightsuite.model.EventMaster;
import com.efreightsuite.service.EventMasterService;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author tanveer
 */

@RestController
@Log4j2
@RequestMapping(value = "/api/v1/eventmaster")

public class EventMasterController {

    @Autowired
    private
    EventMasterService eventMasterService;

    @RequestMapping(value = "/get/search", method = RequestMethod.POST)
    @Secured("ROLE_MASTER.OCEAN.EVENT.LIST")
    public BaseDto search(@RequestBody EventSearchDto searchDto) {
        log.info("Search method called.");
        return eventMasterService.search(searchDto);
    }

    @RequestMapping(value = "/get/search/keyword", method = RequestMethod.POST)
    public BaseDto search(@RequestBody SearchRequest searchRequest) {
        log.info("Search method called.");
        return eventMasterService.search(searchRequest);
    }


    @RequestMapping(value = "/get/search/exclude", method = RequestMethod.POST)
    public BaseDto searchEventNotInList(@RequestBody SearchRequest searchRequest) {
        return eventMasterService.searchEventNotInList(searchRequest);
    }

    @RequestMapping(value = "/get/id/{id}", method = RequestMethod.GET)
    @Secured("ROLE_MASTER.OCEAN.EVENT.VIEW")
    public BaseDto getById(@PathVariable Long id) {
        return eventMasterService.get(id);
    }

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    @Secured("ROLE_MASTER.OCEAN.EVENT.CREATE")
    public BaseDto create(@RequestBody EventMaster eventMaster) {
        log.info("Create method is called..");
        return eventMasterService.create(eventMaster);
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    @Secured("ROLE_MASTER.OCEAN.EVENT.MODIFY")
    public BaseDto update(@RequestBody EventMaster eventMaster) {
        return eventMasterService.update(eventMaster);
    }

    @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
    @Secured("ROLE_MASTER.OCEAN.EVENT.DELETE")
    public BaseDto delete(@PathVariable Long id) {
        log.info("Delete method called....[" + id + "]");
        return eventMasterService.delete(id);
    }

    @RequestMapping(value = "/getall", method = RequestMethod.GET)
    public BaseDto get() {
        log.info("getall method called.");
        return eventMasterService.getAll();
    }

}
