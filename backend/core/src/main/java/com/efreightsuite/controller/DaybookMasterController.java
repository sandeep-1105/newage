package com.efreightsuite.controller;

import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.DaybookSearchDto;
import com.efreightsuite.dto.SearchRequest;
import com.efreightsuite.model.DaybookMaster;
import com.efreightsuite.service.DaybookMasterService;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/api/v1/daybookmaster")
@Log4j2
public class DaybookMasterController {

    /**
     * DaybookMasterService Object
     */

    @Autowired
    private
    DaybookMasterService daybookMasterService;

    /**
     * Method - getById Returns DaybookMaster based on given daybook id
     *
     * @return DaybookMaster
     */

    @RequestMapping(value = "/get/id/{id}", method = RequestMethod.GET)
    @Secured("ROLE_MASTER.FINANCE.DAY_BOOK.VIEW")
    public BaseDto getByDaybookId(@PathVariable Long id) {

        log.info("getByDaybookId method is called...ID = " + id);

        return daybookMasterService.getByDaybookId(id);
    }

    /**
     * Method - getByDaybookCode Returns DaybookMaster based on given daybook code
     *
     * @return DaybookMaster
     */

    @RequestMapping(value = "/get/code/{daybookCode}", method = RequestMethod.GET)
    public BaseDto getByDaybookCode(@PathVariable String daybookCode) {

        log.info("getByDaybookCode method is called...CODE = " + daybookCode);

        return daybookMasterService.getByDaybookCode(daybookCode);
    }

    /**
     * Method - search Returns Paginated Search Result based on Search Keyword
     *
     * @return SearchRespDto
     */

    @RequestMapping(value = "/get/search", method = RequestMethod.POST)
    @Secured("ROLE_MASTER.FINANCE.DAY_BOOK.LIST")
    public BaseDto search(@RequestBody DaybookSearchDto searchDto) {

        log.info("Search method is called...Search Dto: " + searchDto);

        return daybookMasterService.search(searchDto);
    }

    @RequestMapping(value = "/get/search/keyword", method = RequestMethod.POST)
    public BaseDto search(@RequestBody SearchRequest searchRequest) {

        log.info("Search method is called...Search Dto: " + searchRequest);

        return daybookMasterService.search(searchRequest);
    }


    @RequestMapping(value = "/getDaybookListByDocumentType/search/keyword/{documentTypeId}", method = RequestMethod.POST)
    public BaseDto searchDaybookListByDocumentType(@RequestBody SearchRequest searchRequest, @PathVariable Long documentTypeId) {

        log.info("searchDaybookListByDocumentType method is called...Search Dto: " + searchRequest);

        return daybookMasterService.searchDaybookListByDocumentType(searchRequest, documentTypeId);
    }

    /**
     * Method - create Creates and returns a new daybook master
     *
     * @returns DaybookMaster
     */

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    @Secured("ROLE_MASTER.FINANCE.DAY_BOOK.CREATE")
    public BaseDto create(@RequestBody DaybookMaster daybookMaster) {

        log.info("Create method is called.. DaybookMaster : " + daybookMaster);

        return daybookMasterService.create(daybookMaster);
    }

    /**
     * Method - update returns a updated daybook master
     *
     * @param existingDaybookMaster
     * @returns DaybookMaster
     */

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    @Secured("ROLE_MASTER.FINANCE.DAY_BOOK.MODIFY")
    public BaseDto update(@RequestBody DaybookMaster existingDaybookMaster) {

        log.info("Create method is called.. DaybookMaster : " + existingDaybookMaster);

        return daybookMasterService.update(existingDaybookMaster);
    }

    @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
    @Secured("ROLE_MASTER.FINANCE.DAY_BOOK.DELETE")
    public BaseDto delete(@PathVariable Long id) {

        log.info("Delete method is called.. : [" + id + "]");

        return daybookMasterService.delete(id);
    }
}
