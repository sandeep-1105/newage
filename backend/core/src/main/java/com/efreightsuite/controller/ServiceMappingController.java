package com.efreightsuite.controller;

import java.util.List;

import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.ServiceMappingSearchDto;
import com.efreightsuite.model.ServiceMapping;
import com.efreightsuite.service.ServiceMappingService;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@Log4j2
@RestController
@RequestMapping(value = "/api/v1/servicemapping")
public class ServiceMappingController {

    @Autowired
    private
    ServiceMappingService serviceMappingService;

    @RequestMapping(value = "/get/search/keyword", method = RequestMethod.POST)
    @Secured("ROLE_SETUP.APP_CONFIGURATION.SERVICE_MAPPING.LIST")
    public BaseDto get(@RequestBody ServiceMappingSearchDto searchDto) {
        log.info("getall method called.");
        return serviceMappingService.search(searchDto);
    }

    @RequestMapping(value = "/get/id/{id}", method = RequestMethod.GET)
    public BaseDto get(@PathVariable Long id) {
        log.info("get method called." + id);
        return serviceMappingService.get(id);
    }

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    @Secured("ROLE_SETUP.APP_CONFIGURATION.SERVICE_MAPPING.CREATE")
    public BaseDto update(@RequestBody List<ServiceMapping> enquiryServiceMapping) {
        log.info("save method is called.");
        return serviceMappingService.save(enquiryServiceMapping);
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    @Secured("ROLE_SETUP.APP_CONFIGURATION.SERVICE_MAPPING.MODIFY")
    public BaseDto update(@RequestBody ServiceMapping enquiryServiceMapping) {
        log.info("Update method is called.");
        return serviceMappingService.update(enquiryServiceMapping);
    }

    @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
    @Secured("ROLE_SETUP.APP_CONFIGURATION.SERVICE_MAPPING.DELETE")
    public BaseDto update(@PathVariable("id") Long id) {
        log.info("delete with id::-", id);
        return serviceMappingService.delete(id);
    }

}
