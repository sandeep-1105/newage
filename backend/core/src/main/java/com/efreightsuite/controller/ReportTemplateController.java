package com.efreightsuite.controller;

import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.ReportTemplateSearchDto;
import com.efreightsuite.model.ReportTemplate;
import com.efreightsuite.service.ReportTemplateService;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Log4j2
@RequestMapping(value = "/api/v1/reporttemplate")
public class ReportTemplateController {

    @Autowired
    private
    ReportTemplateService reportTemplateService;

    @RequestMapping(value = "/get/{id}", method = RequestMethod.GET)
//	@Secured("ROLE_MASTER.CRM.PARTY.VIEW")
    public BaseDto get(@PathVariable Long id) {
        log.info("get method called..." + id);
        return reportTemplateService.get(id);
    }

    @RequestMapping(value = "/save", method = RequestMethod.POST)
//	@Secured("ROLE_MASTER.CRM.PARTY.CREATE")
    public BaseDto create(@RequestBody ReportTemplate reportTemplate) {
        log.info("Create method is called....");
        return reportTemplateService.createOrUpdate(reportTemplate);
    }

    @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
//	@Secured("ROLE_MASTER.CRM.PARTY.DELETE")
    public BaseDto delete(@PathVariable Long id) {
        log.info("Delete method called...[" + id + "]");
        return reportTemplateService.delete(id);
    }

    @RequestMapping(value = "/search", method = RequestMethod.POST)
//	@Secured("ROLE_MASTER.CRM.PARTY.LIST")
    public BaseDto search(@RequestBody ReportTemplateSearchDto searchDto) {
        log.info("Search method called.[" + searchDto + "]");
        return reportTemplateService.search(searchDto);
    }

}
