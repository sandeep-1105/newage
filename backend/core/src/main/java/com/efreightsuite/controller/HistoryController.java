package com.efreightsuite.controller;

import com.efreightsuite.dto.KeyValueDto;
import com.efreightsuite.service.HistoryService;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author tanveer
 */

@RestController
@Log4j2
@RequestMapping(value = "/api/v1/history")

public class HistoryController {

    @Autowired
    private
    HistoryService historyService;

    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public Object put(@RequestBody KeyValueDto data) {
        log.info("Search method called.");
        return historyService.redisTest(data);
    }

    @RequestMapping(value = "/get", method = RequestMethod.POST)
    public Object search(@RequestBody KeyValueDto data) {
        log.info("Search method called.");
        return historyService.get(data);
    }


}
