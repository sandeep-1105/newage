package com.efreightsuite.controller;

import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.service.AppService;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Log4j2
@RequestMapping(value = "/api/v1/application")
public class AppController {

    @Autowired
    private
    AppService appService;

    @RequestMapping(value = "/getalltimezone", method = RequestMethod.GET)
    public BaseDto getAllTimeZones() {
        log.debug("getalltimezone get all method called.");

        return appService.getAllTimZones();
    }

    @RequestMapping(value = "/getallenums", method = RequestMethod.GET)
    public BaseDto getAllEnums() {
        log.debug("getAllEnums get all method called.");

        return appService.getAllEnums();
    }

}
