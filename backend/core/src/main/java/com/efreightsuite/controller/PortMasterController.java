package com.efreightsuite.controller;

import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.PortSearchDto;
import com.efreightsuite.dto.SearchRequest;
import com.efreightsuite.model.PortMaster;
import com.efreightsuite.service.PortMasterService;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@Log4j2
@RestController
@RequestMapping(value = "/api/v1/portmaster")

public class PortMasterController {

    @Autowired
    private
    PortMasterService portMasterService;

    @RequestMapping(value = "/get/id/{id}")
    @Secured("ROLE_MASTER.GENERAL.PORT.VIEW")
    public BaseDto get(@PathVariable Long id) {
        log.info("PortMasterController -> get is called..[ " + id + " ]");
        return portMasterService.get(id);
    }

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    @Secured("ROLE_MASTER.GENERAL.PORT.CREATE")
    public BaseDto create(@RequestBody PortMaster portMaster) {
        log.info("Create method called.");
        return portMasterService.saveOrUpdate(portMaster);
    }

    @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
    @Secured("ROLE_MASTER.GENERAL.PORT.DELETE")
    public BaseDto delete(@PathVariable("id") Long id) {
        log.info("Delete method called.[" + id + "]");
        return portMasterService.delete(id);
    }


    @RequestMapping(value = "/get/search", method = RequestMethod.POST)
    @Secured("ROLE_MASTER.GENERAL.PORT.LIST")
    public BaseDto search(@RequestBody PortSearchDto searchDto) {
        log.info("PortMasterController search method called.");
        return portMasterService.search(searchDto);
    }

    @RequestMapping(value = "/get/search/keyword", method = RequestMethod.POST)
    public BaseDto search(@RequestBody SearchRequest searchRequest) {
        log.info("PortMasterController search method called.");
        return portMasterService.search(searchRequest);
    }


    @RequestMapping(value = "/get/search/keyword/{transportMode}", method = RequestMethod.POST)
    public BaseDto search(@RequestBody SearchRequest searchRequest, @PathVariable String transportMode) {
        log.info("PortMasterController search method called... Transport Mode " + transportMode);
        return portMasterService.search(searchRequest, transportMode);
    }

    @RequestMapping(value = "/get/searchByCountryCode/keyword", method = RequestMethod.POST)
    public BaseDto searchByCountryCode(@RequestBody SearchRequest searchRequest) {
        log.info("PortMasterController searchByCountryCode method calld::::::::");
        return portMasterService.searchCountry(searchRequest, searchRequest.getParam1(), searchRequest.getParam2());
    }

    @RequestMapping(value = "/get/search/{groupId}/{transportMode}", method = RequestMethod.POST)
    public BaseDto searchByGroup(@RequestBody SearchRequest searchRequest, @PathVariable Long groupId, @PathVariable String transportMode) {
        log.info("PortMasterController search method called... groupId " + groupId);
        return portMasterService.searchByGroup(searchRequest, groupId, transportMode);
    }
}
