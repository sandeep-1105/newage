package com.efreightsuite.controller;

import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.ReferenceTypeSearchDto;
import com.efreightsuite.dto.SearchRequest;
import com.efreightsuite.enumeration.ReferenceTypeKey;
import com.efreightsuite.model.ReferenceTypeMaster;
import com.efreightsuite.service.ReferenceTypeMasterService;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/api/v1/referencetype")
@Log4j2
public class ReferenceTypeController {

    @Autowired
    private
    ReferenceTypeMasterService referenceTypeMasterService;

    @RequestMapping(value = "/get/search", method = RequestMethod.POST)
    @Secured("ROLE_MASTER.OCEAN.REFERENCE_TYPE.LIST")
    public BaseDto search(@RequestBody ReferenceTypeSearchDto searchDto) {
        log.info("ReferenceTypeController.Search method is called...");
        return referenceTypeMasterService.search(searchDto);
    }

    @RequestMapping(value = "/get/search/keyword", method = RequestMethod.POST)
    public BaseDto search(@RequestBody SearchRequest searchRequest) {
        log.info("ReferenceTypeController.Search method is called...");
        return referenceTypeMasterService.search(searchRequest);
    }

    @RequestMapping(value = "/get/id/{id}", method = RequestMethod.GET)
    @Secured("ROLE_MASTER.OCEAN.REFERENCE_TYPE.VIEW")
    public BaseDto get(@PathVariable Long id) {
        log.info("ReferenceTypeController.get method is called...");
        return referenceTypeMasterService.get(id);
    }

    @RequestMapping(value = "/get/{key}", method = RequestMethod.GET)
    public BaseDto getMandatoryReferenceTypeByKey(@PathVariable ReferenceTypeKey key) {
        log.info("ReferenceTypeController.get method is called...");
        return referenceTypeMasterService.getMandatoryReferenceTypeByKey(key);
    }

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    @Secured("ROLE_MASTER.OCEAN.REFERENCE_TYPE.CREATE")
    public BaseDto create(@RequestBody ReferenceTypeMaster referenceTypeMaster) {
        log.info("ReferenceTypeController.create method is called...");
        return referenceTypeMasterService.create(referenceTypeMaster);
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    @Secured("ROLE_MASTER.OCEAN.REFERENCE_TYPE.MODIFY")
    public BaseDto update(@RequestBody ReferenceTypeMaster referenceTypeMaster) {
        log.info("ReferenceTypeController.update method is called...");
        return referenceTypeMasterService.update(referenceTypeMaster);
    }

    @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
    @Secured("ROLE_MASTER.OCEAN.REFERENCE_TYPE.DELETE")
    public BaseDto delete(@PathVariable Long id) {
        log.info("ReferenceTypeController.delete method is called...");
        return referenceTypeMasterService.delete(id);
    }
}
