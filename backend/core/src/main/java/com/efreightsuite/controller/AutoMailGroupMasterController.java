package com.efreightsuite.controller;

import com.efreightsuite.dto.AutoMailGroupMasterSearchDto;
import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.SearchRequest;
import com.efreightsuite.model.AutoMailGroupMaster;
import com.efreightsuite.service.AutoMailGroupMasterService;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Devendrachary M
 */

@RestController
@Log4j2
@RequestMapping(value = "/api/v1/automailgroupmaster")
public class AutoMailGroupMasterController {

    @Autowired
    private
    AutoMailGroupMasterService autoMailGroupMasterService;

    /**
     * This method Retrieves the Auto Mail Group Master
     *
     * @return
     */
    @RequestMapping(value = "/get/search", method = RequestMethod.POST)
    public BaseDto search(@RequestBody AutoMailGroupMasterSearchDto searchDto) {
        log.info("Search method called.[" + searchDto + "]");

        return autoMailGroupMasterService.search(searchDto);
    }

    @RequestMapping(value = "/get/search/keyword", method = RequestMethod.POST)
    public BaseDto search(@RequestBody SearchRequest searchRequest) {
        log.info("Search method called.[" + searchRequest + "]");

        return autoMailGroupMasterService.search(searchRequest);
    }

    @RequestMapping(value = "/get/searchByExternal/keyword", method = RequestMethod.POST)
    public BaseDto searchByExternalAutoMail(@RequestBody SearchRequest searchRequest) {
        log.info("Search method called.[" + searchRequest + "]");

        return autoMailGroupMasterService.searchByExternalAutoMail(searchRequest);
    }

    /**
     * This method Retrieves the Auto Mail Group Master By Id
     *
     * @param id
     * @return
     */
    @RequestMapping(value = "/get/id/{id}", method = RequestMethod.GET)
    public BaseDto get(@PathVariable Long id) {
        log.info("Get by id method called..[" + id + "]");
        return autoMailGroupMasterService.get(id);
    }

    /**
     * This method Persists the Auto Mail Group Master
     *
     * @param autoMailGroupMaster
     * @return
     */
    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public BaseDto save(@RequestBody AutoMailGroupMaster autoMailGroupMaster) {
        log.info("While sending AutoMailGroupMaster  : " + autoMailGroupMaster);
        return autoMailGroupMasterService.createUpdate(autoMailGroupMaster);
    }

    /**
     * This method Updates the Existing Auto Mail Group Master
     *
     * @param existingAutoMailGroupMaster
     * @return
     */
    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public BaseDto update(@RequestBody AutoMailGroupMaster existingAutoMailGroupMaster) {
        log.info("Update method called.[" + existingAutoMailGroupMaster.getId() + "]");
        return autoMailGroupMasterService.update(existingAutoMailGroupMaster);
    }

    @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
    public BaseDto delete(@PathVariable("id") Long id) {
        log.info("Delete method called.[" + id + "]");
        return autoMailGroupMasterService.delete(id);
    }

}
