package com.efreightsuite.controller;

import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.model.PartyServiceMaster;
import com.efreightsuite.service.PartyServiceMasterService;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * ClassName             Purpose                    CreatedBy/ModifiedBy      Date          Version
 * <p>
 * PartyServiceMaster   Class handles all request&        Devendrachary M          25-02-15        0.0
 * responses of partyservice master
 */
@RestController
@Log4j2
@RequestMapping(value = "/api/v1/partyservicemaster")
public class PartyServiceMasterController {


    /**
     * Autowired PartyServiceMaster
     */
    @Autowired
    private
    PartyServiceMasterService partyServiceMasterService;

    /**
     * Method                      getAll
     * Description       RestAPI to get all CostCenter list
     */
    @RequestMapping(value = "/getall", method = RequestMethod.GET)
    public BaseDto getALl() {

        log.info("PartyServiceMasterController.getall method called");
        return partyServiceMasterService.getAll();

    }

    /**
     * Method                        get
     *
     */
    @RequestMapping(value = "/get/id/{id}", method = RequestMethod.GET)
    public BaseDto get(@PathVariable Long id) {
        log.info("PartyServiceMasterController.get method called");
        return partyServiceMasterService.get(id);
    }

    /**
     * Method                             create
     *
     */

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public BaseDto save(@RequestBody PartyServiceMaster partyServiceMaster) {
        log.info("PartyServiceMasterController.create method called.");
        return partyServiceMasterService.create(partyServiceMaster);
    }

    /**
     * Method                             update
     *
     */
    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public BaseDto update(@RequestBody PartyServiceMaster partyServiceMaster) {
        log.info("PartyServiceMasterController.update method called.");
        return partyServiceMasterService.update(partyServiceMaster);
    }


}
