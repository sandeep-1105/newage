package com.efreightsuite.controller;

import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.GeneralNoteMasterSearchDto;
import com.efreightsuite.enumeration.GeneralNoteCategory;
import com.efreightsuite.enumeration.GeneralNoteSubCategory;
import com.efreightsuite.model.GeneralNoteMaster;
import com.efreightsuite.service.GeneralNoteMasterService;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Log4j2
@RequestMapping(value = "/api/v1/generalnotemaster")
public class GeneralNoteMasterController {

    @Autowired
    private
    GeneralNoteMasterService generalNoteMasterService;

    @RequestMapping(value = "/get/generalnote/{locationId}/{category}/{subCategory}/{serviceMasterId}", method = RequestMethod.GET)
    public BaseDto getAllTimeZones(@PathVariable Long locationId, @PathVariable GeneralNoteCategory category, @PathVariable GeneralNoteSubCategory subCategory, @PathVariable Long serviceMasterId) {

        return generalNoteMasterService.getGeneralNote(locationId, category, subCategory, serviceMasterId);
    }


    /**
     * Method - getById Returns GeneralNoteMaster based on given generalNoteMaster id
     *
     * @return GeneralNoteMaster
     */

    @RequestMapping(value = "/get/id/{id}", method = RequestMethod.GET)
    public BaseDto getById(@PathVariable Long id) {

        log.info("getById method is called...ID = " + id);

        return generalNoteMasterService.getById(id);
    }

    /**
     * Method - search Returns Paginated Search Result based on Search Keyword
     *
     * @return SearchRespDto
     */

    @RequestMapping(value = "/get/search", method = RequestMethod.POST)
    public BaseDto search(@RequestBody GeneralNoteMasterSearchDto searchDto) {

        log.info("Search method is called...Search Dto: " + searchDto);

        return generalNoteMasterService.search(searchDto);
    }


    /**
     * Method - create Creates and returns a new GeneralNoteMaster
     *
     * @returns GeneralNoteMaster
     */

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public BaseDto create(@RequestBody GeneralNoteMaster generalNoteMaster) {

        log.info("Create method is called.. generalNoteMaster : " + generalNoteMaster);

        return generalNoteMasterService.create(generalNoteMaster);
    }

    /**
     * Method - update returns a updated generalNoteMaster
     *
     * @param existinggeneralNoteMaster
     * @returns generalNoteMaster
     */

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public BaseDto update(@RequestBody GeneralNoteMaster existinggeneralNoteMaster) {

        log.info("Update method is called.. generalNoteMaster : " + existinggeneralNoteMaster);

        return generalNoteMasterService.update(existinggeneralNoteMaster);
    }

    @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
    public BaseDto delete(@PathVariable Long id) {

        log.info("Delete method is called.. : [" + id + "]");

        return generalNoteMasterService.delete(id);
    }


}
