package com.efreightsuite.controller;

import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.ReasonSearchDto;
import com.efreightsuite.dto.SearchRequest;
import com.efreightsuite.model.ReasonMaster;
import com.efreightsuite.service.ReasonMasterService;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/api/v1/reasonmaster")
@Log4j2
public class ReasonMasterController {

    /**
     * ReasonMasterService Object
     */

    @Autowired
    private
    ReasonMasterService reasonMasterService;

    /**
     * Method - getById Returns ReasonMaster based on given reason id
     *
     * @return ReasonMaster
     */

    @RequestMapping(value = "/get/id/{id}", method = RequestMethod.GET)
    @Secured("ROLE_MASTER.FINANCE.REASON.VIEW")
    public BaseDto getByReasonId(@PathVariable Long id) {

        log.info("getByReasonId method is called...ID = " + id);

        return reasonMasterService.getByReasonId(id);
    }

    /**
     * Method - getByReasonCode Returns ReasonMaster based on given reason code
     *
     * @return ReasonMaster
     */

    @RequestMapping(value = "/get/code/{reasonCode}", method = RequestMethod.GET)
    public BaseDto getByReasonCode(@PathVariable String reasonCode) {

        log.info("getByReasonCode method is called...CODE = " + reasonCode);

        return reasonMasterService.getByReasonCode(reasonCode);
    }

    /**
     * Method - search Returns Paginated Search Result based on Search Keyword
     *
     * @return SearchRespDto
     */

    @RequestMapping(value = "/get/search", method = RequestMethod.POST)
    @Secured("ROLE_MASTER.FINANCE.REASON.LIST")
    public BaseDto search(@RequestBody ReasonSearchDto searchDto) {

        log.info("Search method is called...Search Dto: " + searchDto);

        return reasonMasterService.search(searchDto);
    }

    @RequestMapping(value = "/get/search/keyword", method = RequestMethod.POST)
    public BaseDto search(@RequestBody SearchRequest searchRequest) {

        log.info("Search method is called...Search Dto: " + searchRequest);

        return reasonMasterService.search(searchRequest);
    }

    /**
     * Method - create Creates and returns a new reason master
     *
     * @returns ReasonMaster
     */

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    @Secured("ROLE_MASTER.FINANCE.REASON.CREATE")
    public BaseDto create(@RequestBody ReasonMaster reasonMaster) {

        log.info("Create method is called.. ReasonMaster : " + reasonMaster);

        return reasonMasterService.create(reasonMaster);
    }

    /**
     * Method - update returns a updated reason master
     *
     * @param existingReasonMaster
     * @returns ReasonMaster
     */

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    @Secured("ROLE_MASTER.FINANCE.REASON.MODIFY")
    public BaseDto update(@RequestBody ReasonMaster existingReasonMaster) {

        log.info("Create method is called.. ReasonMaster : " + existingReasonMaster);

        return reasonMasterService.update(existingReasonMaster);
    }

    @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
    @Secured("ROLE_MASTER.FINANCE.REASON.DELETE")
    public BaseDto delete(@PathVariable Long id) {

        log.info("Delete method is called.. : [" + id + "]");

        return reasonMasterService.delete(id);
    }
}
