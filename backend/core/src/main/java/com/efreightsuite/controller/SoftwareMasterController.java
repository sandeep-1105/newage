package com.efreightsuite.controller;

import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.SearchRequest;
import com.efreightsuite.dto.SoftwareMasterSearchDto;
import com.efreightsuite.model.SoftwareMaster;
import com.efreightsuite.service.SoftwareMasterService;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


@RestController
@Log4j2
@RequestMapping(value = "/api/v1/softwaremaster")
public class SoftwareMasterController {

    @Autowired
    private
    SoftwareMasterService softwareMasterService;


    @RequestMapping(value = "/get/search", method = RequestMethod.POST)
    public BaseDto get(@RequestBody SearchRequest searchRequest) {
        log.info("getall method called.");
        return softwareMasterService.search(searchRequest);
    }


    @RequestMapping(value = "/get/search/keyword", method = RequestMethod.POST)
    @Secured("ROLE_MASTER.GENERAL.SOFTWARE.LIST")
    public BaseDto get(@RequestBody SoftwareMasterSearchDto searchDto) {
        log.info("getall method called.");
        return softwareMasterService.search(searchDto);
    }


    @RequestMapping(value = "/get/id/{id}", method = RequestMethod.GET)
    @Secured("ROLE_MASTER.GENERAL.SOFTWARE.VIEW")
    public BaseDto get(@PathVariable Long id) {
        log.info("get method called." + id);
        return softwareMasterService.get(id);
    }


    @RequestMapping(value = "/get/getall", method = RequestMethod.GET)
    public BaseDto getAll() {
        log.info("getall method called.");
        return softwareMasterService.getAll();
    }


    @RequestMapping(value = "/create", method = RequestMethod.POST)
    @Secured({"ROLE_MASTER.GENERAL.SOFTWARE.CREATE", "ROLE_MASTER.GENERAL.SOFTWARE.MODIFY"})
    public BaseDto saveOrUpdate(@RequestBody SoftwareMaster softwareMaster) {
        log.info("SaveOrUpdate method is called.");
        return softwareMasterService.saveOrUpdate(softwareMaster);
    }


    @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
    @Secured("ROLE_MASTER.GENERAL.SOFTWARE.DELETE")
    public BaseDto delete(@PathVariable Long id) {
        log.info("Delete method called....");
        return softwareMasterService.delete(id);
    }


}
