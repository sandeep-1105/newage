package com.efreightsuite.controller;

import java.util.List;

import javax.servlet.http.HttpServletResponse;

import com.efreightsuite.client.PushApiService;
import com.efreightsuite.dto.AirDocumentSearchDto;
import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.ConsolDto;
import com.efreightsuite.dto.ConsolSearchDto;
import com.efreightsuite.dto.DeliveryOrderDto;
import com.efreightsuite.dto.GenerateNumDto;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.model.AirlineEdi;
import com.efreightsuite.model.Consol;
import com.efreightsuite.model.ConsolSignOff;
import com.efreightsuite.repository.ConsolRepository;
import com.efreightsuite.service.AirlineEdiService;
import com.efreightsuite.service.ConsolService;
import com.efreightsuite.service.CopyConsolService;
import com.efreightsuite.service.EdiResponseStatusService;
import com.efreightsuite.service.WinConfigurationService;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Log4j2
@RequestMapping(value = "/api/v1/consol")
public class ConsolController {

    @Autowired
    private
    ConsolService consolService;

    @Autowired
    private
    CopyConsolService copyConsolService;

    @Autowired
    private
    AirlineEdiService airlineEdiService;

    @Autowired
    private
    EdiResponseStatusService ediResponseStatusService;


    @Autowired
    private
    ConsolRepository consolRepository;

    @Autowired
    WinConfigurationService winConfigurationService;

    @Autowired
    private
    PushApiService pushApiService;

    @RequestMapping(value = "/shipmentLink", method = RequestMethod.POST)
    public BaseDto shipmentLink(@RequestBody ConsolDto consolDto) {
        log.info("ConsolController -> search method called.");
        if (consolDto.getConsolUid() == null || consolDto.getConsolUid().trim().length() == 0
                || consolDto.getConsolUid() == "-1") {
            consolDto.setConsolUid(null);
        }
        return consolService.shipmentLink(consolDto);
    }

    @RequestMapping(value = "/search", method = RequestMethod.POST)
    public BaseDto search(@RequestBody ConsolSearchDto searchDto) {
        log.info("ConsolController -> search method called.");
        return consolService.search(searchDto);
    }

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public BaseDto create(@RequestBody Consol consol) {
        log.info("ConsolController -> create method called.");
        BaseDto baseDto = consolService.create(consol);
        if (baseDto.getResponseCode().toUpperCase().equals(ErrorCode.SUCCESS)) {
            Consol entity = (Consol) baseDto.getResponseObject();
            pushApiService.pushToMasterShipment(entity.getId(), null);
        }
        return baseDto;
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public BaseDto update(@RequestBody Consol consol) {
        log.info("ConsolController -> update method called.");
        BaseDto baseDto = consolService.update(consol);
        if (baseDto.getResponseCode().toUpperCase().equals(ErrorCode.SUCCESS)) {
            Consol entity = (Consol) baseDto.getResponseObject();
            pushApiService.pushToMasterShipment(entity.getId(), null);
        }
        return baseDto;
    }


    @RequestMapping(value = "/get/id/{id}", method = RequestMethod.GET)
    public BaseDto getById(@PathVariable Long id) {
        log.info("ConsolController -> get by id method called.");
        return consolService.getById(id);
    }

    @RequestMapping(value = "/edi/get/{id}/{isAgentChange}", method = RequestMethod.GET)
    public BaseDto getEDI(@PathVariable("id") Long id, @PathVariable("isAgentChange") Boolean isAgentChange) {
        log.info("ConsolController -> getEDI method called. ID = [" + id + "] + isAgentChange Flag = [" + isAgentChange + "]");
        return airlineEdiService.getEdi(id, isAgentChange);
    }

    @RequestMapping(value = "/edi/get/response/{id}", method = RequestMethod.GET)
    public BaseDto getEDIResponse(@PathVariable("id") Long id) {
        log.info("ConsolController -> getEDIResponse method called. ID = [" + id + "]");
        return ediResponseStatusService.getAllByConsolId(id);
    }


    @RequestMapping(value = "/edi/save", method = RequestMethod.POST)
    public BaseDto saveEdi(@RequestBody AirlineEdi airlineEdi) {
        log.info("ConsolController -> update method called.");
        BaseDto baseDto = airlineEdiService.saveEdi(airlineEdi);
        if (baseDto.getResponseCode().toUpperCase().equals(ErrorCode.SUCCESS)) {
            Consol entity = (Consol) baseDto.getResponseObject();
            pushApiService.pushToMasterShipment(entity.getId(), null);
        }
        return baseDto;
    }

    @RequestMapping(value = "/get/consolUid/{consolUid}", method = RequestMethod.GET)
    public BaseDto getByShipmentUid(@PathVariable String consolUid) {
        log.info("ConsolController -> get by Consol UID method called.");
        return consolService.getByConsolUid(consolUid);
    }

    @RequestMapping(value = "/statuschange", method = RequestMethod.POST)
    public BaseDto statuschange(@RequestBody ConsolDto consolDto) {
        log.info("ConsolController -> statuschange.");
        BaseDto baseDto = consolService.statusChange(consolDto);
        if (baseDto.getResponseCode().toUpperCase().equals(ErrorCode.SUCCESS)) {
            Consol entity = (Consol) baseDto.getResponseObject();
            pushApiService.pushToMasterShipment(entity.getId(), null);
        }
        return baseDto;
    }

    @RequestMapping(value = "/processautoimport", method = RequestMethod.POST)
    public BaseDto processAutoImport(@RequestBody ConsolDto consolDto) {
        log.info("ConsolController -> processAutoImport.");
        return consolService.executeAutoImportProcess(consolDto.getConsolId());
    }

    @RequestMapping(value = "/gennum", method = RequestMethod.POST)
    public BaseDto generateNUM(@RequestBody GenerateNumDto generateNumDto) {
        log.info("ConsolController ->Generate number controlled called.");
        return consolService.generateNUM(generateNumDto);
    }

    @RequestMapping(value = "/files/{id}/{type}/{swapped}", method = RequestMethod.GET)
    public void getFile(@PathVariable Long id, @PathVariable String type, @PathVariable("swapped") boolean swapped,
                        HttpServletResponse response) {
        consolService.downloadAttachment(id, type, swapped, response);
    }

    @RequestMapping(value = "/report/shipment/{serviceId}", method = RequestMethod.GET)
    public BaseDto commercialInvoice(@PathVariable("serviceId") Long serviceId) {
        return consolService.getConsolShipmentDocumentListByServiceId(serviceId);
    }


    @RequestMapping(value = "/copyconsol/copyid/{copyid}", method = RequestMethod.GET)
    public BaseDto copyConsol(@PathVariable Long copyid) {
        log.info("ConsolController -> copy consol method called.");
        Consol consolToCopy = consolRepository.findById(copyid);
        BaseDto baseDto = copyConsolService.copyConsol(consolToCopy);

        if (baseDto.getResponseCode().toUpperCase().equals(ErrorCode.SUCCESS)) {
            Consol entity = (Consol) baseDto.getResponseObject();
            pushApiService.pushToMasterShipment(entity.getId(), null);
        }
        return baseDto;
    }


    // documentSearch

    @RequestMapping(value = "/documentSearch", method = RequestMethod.POST)
    @Secured("ROLE_AIR.DOCUMENT.LIST")
    public BaseDto documentSearch(@RequestBody AirDocumentSearchDto searchDto) {
        log.info("ConsolController -> documentSearch method called.");
        return consolService.documentSearch(searchDto);
    }

    @RequestMapping(value = "/getdocument/{docType}/{id}", method = RequestMethod.GET)
    @Secured("ROLE_AIR.DOCUMENT.VIEW")
    public BaseDto getDocumentById(@PathVariable String docType, @PathVariable Long id) {
        log.info("ConsolController -> get by id method called.");
        return consolService.getDocumentById(docType, id);
    }

    @RequestMapping(value = "/consolIdBasedOnUid/{consolUid}", method = RequestMethod.GET)
    public BaseDto getConsolIdBasedOnUid(@PathVariable String consolUid) {
        log.info("ConsolController -> getConsolIdBasedOnUid method called.");
        return consolService.getConsolIdBasedOnUid(consolUid);
    }

    @RequestMapping(value = "/document/update", method = RequestMethod.POST)
    public BaseDto update(@RequestBody DeliveryOrderDto dto) {
        log.info("ConsolController -> update method called.");
        BaseDto baseDto = consolService.updateDocument(dto);
        if (baseDto.getResponseCode().toUpperCase().equals(ErrorCode.SUCCESS)) {
            pushApiService.pushToMasterShipment(Long.parseLong(baseDto.getResponseObject().toString()), null);
        }
        return baseDto;
    }

    @RequestMapping(value = "/signoff")
    public BaseDto saveConsolSignOff(@RequestBody ConsolSignOff consolSignOff) {
        log.info("ConsolController -> signOff method called.");
        BaseDto baseDto = consolService.saveConsolSignOff(consolSignOff);

        if (consolSignOff.getConsolUid() != null) {
            pushApiService.pushToMasterShipment(null, consolSignOff.getConsolUid());
        }

        return baseDto;

    }

    @RequestMapping(value = "/unsignoff")
    public BaseDto saveConsolUnSignOff(@RequestBody ConsolSignOff consolSignOff) {
        log.info("ConsolController -> signOff method called.");
        BaseDto baseDto = consolService.saveConsolUnSignOff(consolSignOff);
        if (consolSignOff.getConsolUid() != null) {
            pushApiService.pushToMasterShipment(null, consolSignOff.getConsolUid());
        }
        return baseDto;
    }

    @RequestMapping(value = "/doall", method = RequestMethod.POST)
    public BaseDto save(@RequestBody List<DeliveryOrderDto> serviceDetailId) {
        log.info("Do print all controller called.");
        return consolService.downloadAll(serviceDetailId);
    }

    @RequestMapping(value = "/get/signoff/status/{consolId}", method = RequestMethod.GET)
    public BaseDto getSignOffStatus(@PathVariable Long consolId) {
        log.info("shipmentService.getSignOffStatus");
        return consolService.getSignOffStatus(consolId);
    }

    @RequestMapping(value = "/getchargehistory/{consolUid}", method = RequestMethod.GET)
    public BaseDto getConsolChargeHistory(@PathVariable String consolUid) {
        log.info("GetConsol Charge History called");
        return consolService.getConsolChargeHistory(consolUid);
    }

    @RequestMapping(value = "/isconsolcreatedfromshipment/{serviceUid}", method = RequestMethod.GET)
    public BaseDto isConsolCreatedFromShipment(@PathVariable String serviceUid) {
        log.info("isConsolCreatedFromShipment method called..[" + serviceUid + "]");
        return consolService.isConsolCreatedFromShipment(serviceUid);
    }


}
