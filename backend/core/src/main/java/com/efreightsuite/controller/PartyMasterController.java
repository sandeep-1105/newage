package com.efreightsuite.controller;

import com.efreightsuite.client.PushApiService;
import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.PartySearchDto;
import com.efreightsuite.dto.SearchRequest;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.model.PartyMaster;
import com.efreightsuite.service.PartyMasterService;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Log4j2
@RequestMapping(value = "/api/v1/partymaster")
public class PartyMasterController {

    @Autowired
    private
    PartyMasterService partyMasterService;

    @Autowired
    private
    PushApiService pushApiService;

    @RequestMapping(value = "/get/id/{id}", method = RequestMethod.GET)
    @Secured("ROLE_MASTER.CRM.PARTY.VIEW")
    public BaseDto get(@PathVariable Long id) {
        log.info("get method called..." + id);
        return partyMasterService.get(id);
    }

    @RequestMapping(value = "/get/code/{code}", method = RequestMethod.GET)
    public BaseDto getByPartyCode(@PathVariable String code) {
        log.info("getByPartyCode method called..." + code);
        return partyMasterService.getByPartyCode(code);
    }

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    @Secured("ROLE_MASTER.CRM.PARTY.CREATE")
    public BaseDto create(@RequestBody PartyMaster partyMaster) {
        log.info("Create method is called....");
        BaseDto baseDto = partyMasterService.create(partyMaster);

        if (ErrorCode.SUCCESS.equalsIgnoreCase(baseDto.getResponseCode())) {
            PartyMaster entity = (PartyMaster) baseDto.getResponseObject();
            pushApiService.pushToPartyMaster(entity.getId());
        }

        return baseDto;
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    @Secured("ROLE_MASTER.CRM.PARTY.MODIFY")
    public BaseDto update(@RequestBody PartyMaster partyMaster) {
        log.info("Update method is called...[" + partyMaster.getId() + "]");
        BaseDto baseDto = partyMasterService.update(partyMaster);
        if (ErrorCode.SUCCESS.equalsIgnoreCase(baseDto.getResponseCode())) {
            PartyMaster entity = (PartyMaster) baseDto.getResponseObject();
            pushApiService.pushToPartyMaster(entity.getId());
        }

        return baseDto;
    }

    @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
    @Secured("ROLE_MASTER.CRM.PARTY.DELETE")
    public BaseDto delete(@PathVariable Long id) {
        log.info("Delete method called...[" + id + "]");
        return partyMasterService.delete(id);
    }

    @RequestMapping(value = "/get/search", method = RequestMethod.POST)
    @Secured("ROLE_MASTER.CRM.PARTY.LIST")
    public BaseDto search(@RequestBody PartySearchDto searchDto) {
        log.info("Search method called.[" + searchDto + "]");
        return partyMasterService.search(searchDto);
    }


    @RequestMapping(value = "/get/search/keyword", method = RequestMethod.POST)
    public BaseDto search(@RequestBody SearchRequest searchRequest) {
        log.info("Search method called with keyword.[" + searchRequest.getKeyword() + "]");
        return partyMasterService.search(searchRequest);
    }

    @RequestMapping(value = "/get/search/agent", method = RequestMethod.POST)
    public BaseDto searchByAgent(@RequestBody SearchRequest searchRequest) {
        log.info("searchByAgent method called.[" + searchRequest.getKeyword() + "]");
        return partyMasterService.searchByAgent(searchRequest);
    }


    @RequestMapping(value = "/get/search/exclude", method = RequestMethod.POST)
    public BaseDto searchPartyNotInList(@RequestBody SearchRequest searchRequest) {
        log.info("searchPartyNotInList method called. :");
        return partyMasterService.searchPartyNotInList(searchRequest);
    }


    @RequestMapping(value = "/get/search/keyword/partytype/{partyType}", method = RequestMethod.POST)
    public BaseDto search(@RequestBody SearchRequest searchRequest, @PathVariable String partyType) {
        log.info("Search method called with party type.[" + searchRequest + "]");
        return partyMasterService.search(searchRequest, partyType);
    }

    @RequestMapping(value = "/get/search/keyword/partyaccount/{partyId}", method = RequestMethod.POST)
    public BaseDto searchPartyAccount(@RequestBody SearchRequest searchRequest, @PathVariable String partyId) {
        log.info("searchPartyAccount method called.[" + searchRequest + "]");
        return partyMasterService.searchPartyAccount(searchRequest, partyId);
    }

    @RequestMapping(value = "/get/search/keyword/partyaccount/notin/{partyId}", method = RequestMethod.POST)
    public BaseDto getPartyAcount(@RequestBody SearchRequest searchRequest, @PathVariable Long partyId) {
        return partyMasterService.searchPartyAccountNotInParty(searchRequest, partyId);
    }

}
