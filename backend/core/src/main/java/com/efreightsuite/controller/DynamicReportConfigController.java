package com.efreightsuite.controller;

import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.ReportSearchDto;
import com.efreightsuite.model.ReportsFormData;
import com.efreightsuite.service.ReportService;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "api/v1/report")
@Log4j2
public class DynamicReportConfigController {

    @Autowired
    private
    ReportService reportService;


    @RequestMapping(value = "/reportSearch", method = RequestMethod.POST)
    public BaseDto reportSearch(@RequestBody ReportSearchDto dto) {
        log.info("ReportsController  reportsSearch method called.");
        return reportService.reportSearch(dto);
    }

    @RequestMapping(value = "/get/search/keyword", method = RequestMethod.POST)
    @Secured("ROLE_REPORTS.LIST")
    public BaseDto search(@RequestBody ReportSearchDto searchDto) {

        log.info("Search method is called...ReportSearchDto Dto: " + searchDto);

        return reportService.search(searchDto);
    }

    @RequestMapping(value = "/getReports/id/{id}", method = RequestMethod.GET)
    public BaseDto getAllById(@PathVariable Long id) {

        log.info("getByReportsId method is called...ID = " + id);

        return reportService.getAllById(id);
    }

    @RequestMapping(value = "/createReport", method = RequestMethod.POST)
    public BaseDto save(@RequestBody ReportsFormData reportsFormData) {
        log.info("ReportsController create method called.");
        return reportService.create(reportsFormData);
    }

}
