package com.efreightsuite.controller;

import java.util.Objects;

import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.SearchRequest;
import com.efreightsuite.dto.UserProfileDto;
import com.efreightsuite.dto.UserProfileSearchDto;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.model.UserProfile;
import com.efreightsuite.service.UserProfileService;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;


@RestController
@Log4j2
@RequestMapping(value = "/api/v1/userprofile")
public class UserProfileController {

    @Autowired
    private
    UserProfileService userProfileService;

    @RequestMapping(value = "/get/search", method = RequestMethod.POST)
    @Secured("ROLE_SETUP.USER_CREATION.LIST")
    public BaseDto search(@RequestBody UserProfileSearchDto userProfileSearchDto) {
        log.info("Search method called.");
        return userProfileService.search(userProfileSearchDto);
    }

    @RequestMapping(value = "/get/search/keyword", method = RequestMethod.POST)
    public BaseDto search(@RequestBody SearchRequest searchRequest) {
        log.info("Search method called.");
        return userProfileService.search(searchRequest);
    }

    @RequestMapping(value = "/get/search/exclude", method = RequestMethod.POST)
    public BaseDto searchUserNotInList(@RequestBody SearchRequest searchRequest) {
        log.info("Search method called. :");
        return userProfileService.searchUserNotInList(searchRequest);
    }

    @RequestMapping(value = "/get/search/list", method = RequestMethod.POST)
    public BaseDto searchUserList(@RequestBody SearchRequest searchRequest) {
        log.info("Search method called.");
        return userProfileService.searchUserList(searchRequest);
    }


    @RequestMapping(value = "/get/id/{id}", method = RequestMethod.GET)
    @Secured("ROLE_SETUP.USER_CREATION.VIEW")
    public BaseDto get(@PathVariable Long id) {
        log.info("Get by id method called.[" + id + "]");
        return userProfileService.get(id);
    }

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    @Secured("ROLE_SETUP.USER_CREATION.CREATE")
    public ResponseEntity<BaseDto> create(@RequestBody UserProfileDto userProfile, UriComponentsBuilder uriBuilder) {
        log.info("Create method called.");
        BaseDto response = userProfileService.saveUser(userProfile);
        if (Objects.equals(response.getResponseCode(), ErrorCode.SUCCESS)) {
            return ResponseEntity
                    .created(uriBuilder.path("/api/v1/userprofile/get/id/{id}").buildAndExpand(((UserProfile) response.getResponseObject()).getId())
                            .toUri()
                    ).body(response);
        }
        return ResponseEntity.ok(response);
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    @Secured("ROLE_SETUP.USER_CREATION.MODIFY")
    public BaseDto update(@RequestBody UserProfileDto userProfileDto) {
        log.info("Update method called.[" + userProfileDto.getId() + "]");
        return userProfileService.updateUser(userProfileDto);
    }

    @RequestMapping(value = "/{id}/activate", method = RequestMethod.POST)
    @Secured("ROLE_SETUP.USER_CREATION.MODIFY")
    public BaseDto activate(@PathVariable("id") Long userId) {
        return userProfileService.activate(userId);
    }

    @RequestMapping(value = "/{id}/deactivate", method = RequestMethod.POST)
    @Secured("ROLE_SETUP.USER_CREATION.MODIFY")
    public BaseDto deactivate(@PathVariable("id") Long userId) {
        return userProfileService.deactivate(userId);
    }

    @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
    @Secured("ROLE_SETUP.USER_CREATION.DELETE")
    public BaseDto delete(@PathVariable("id") Long id) {
        log.info("Delete method called.[" + id + "]");
        return userProfileService.delete(id);
    }

    /**
     * To get the user profile all
     *
     * @return BaseDto
     */

    @RequestMapping(value = "/get/search/all", method = RequestMethod.GET)
    public BaseDto getAll() {

        return userProfileService.getAll();
    }

    @RequestMapping(value = "/get/currentuser", method = RequestMethod.GET)
    public UserProfile getCurrentUser() {
        return userProfileService.getCurrentUser();
    }


    @RequestMapping(value = "/changeprofile/{locationId}", method = RequestMethod.GET)
    public BaseDto changeprofile(@PathVariable Long locationId) {

        return userProfileService.changeprofile(locationId);
    }


    @RequestMapping(value = "/companyaddress/{companyId}", method = RequestMethod.GET)
    public BaseDto getCompanyAdress(@PathVariable Long companyId) {
        return userProfileService.getCompanyAdress(companyId);
    }


}
