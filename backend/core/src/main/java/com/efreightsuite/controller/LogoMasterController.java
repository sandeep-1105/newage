package com.efreightsuite.controller;

import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.LogoMasterSearchDto;
import com.efreightsuite.model.LogoMaster;
import com.efreightsuite.service.LogoMasterService;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Devendrachary M
 */

@RestController
@Log4j2
@RequestMapping(value = "/api/v1/logomaster")
public class LogoMasterController {

    @Autowired
    private
    LogoMasterService logoMasterService;

    /**
     * This method Retrieves the Logo Master
     *
     * @return
     */
    @RequestMapping(value = "/get/search", method = RequestMethod.POST)
    @Secured("ROLE_SETUP.LOGO_MASTER.LIST")
    public BaseDto search(@RequestBody LogoMasterSearchDto searchDto) {
        log.info("Search method called.[" + searchDto + "]");

        return logoMasterService.search(searchDto);
    }

    /**
     * This method Retrieves the Logo Master By Id
     *
     * @param id
     * @return
     */
    @RequestMapping(value = "/get/id/{id}", method = RequestMethod.GET)
    @Secured("ROLE_SETUP.LOGO_MASTER.VIEW")
    public BaseDto get(@PathVariable Long id) {
        log.info("Get by id method called..[" + id + "]");
        return logoMasterService.get(id);
    }

    /**
     * This method Persists the Logo Master
     *
     * @param enquiryLog
     * @return
     */
    @RequestMapping(value = "/create", method = RequestMethod.POST)
    @Secured("ROLE_SETUP.LOGO_MASTER.CREATE")
    public BaseDto save(@RequestBody LogoMaster enquiryLog) {
        log.info("While sending LogoMaster  : " + enquiryLog);
        return logoMasterService.create(enquiryLog);
    }

    /**
     * This method Updates the Existing Logo Master
     *
     * @param existingLogoMaster
     * @return
     */
    @RequestMapping(value = "/update", method = RequestMethod.POST)
    @Secured("ROLE_SETUP.LOGO_MASTER.MODIFY")
    public BaseDto update(@RequestBody LogoMaster existingLogoMaster) {
        log.info("Update method called.[" + existingLogoMaster.getId() + "]");
        return logoMasterService.update(existingLogoMaster);
    }

    /**
     * To get the PartyEmailMessageMapping all
     *
     * @return BaseDto
     */
    @RequestMapping(value = "/getall", method = RequestMethod.GET)
    public BaseDto getall() {
        log.info("getall method called.");
        return logoMasterService.getAll();
    }

    @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
    @Secured("ROLE_SETUP.LOGO_MASTER.DELETE")
    public BaseDto delete(@PathVariable("id") Long id) {
        log.info("Delete method called.[" + id + "]");
        return logoMasterService.delete(id);
    }

    @RequestMapping(value = "/get/bylocation/{locationid}", method = RequestMethod.GET)
    public BaseDto getlogoBylocation(@PathVariable Long locationid) {
        return logoMasterService.getlogoBylocation(locationid);
    }


}
