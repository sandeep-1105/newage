package com.efreightsuite.controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.ForgotPasswordDto;
import com.efreightsuite.dto.LoginRequest;
import com.efreightsuite.service.AuthService;
import com.efreightsuite.service.InitSetupService;
import com.efreightsuite.service.LanguageDetailService;
import lombok.extern.log4j.Log4j2;
import org.activiti.engine.TaskService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/*
 * Login controller class for
 */
@RestController
@RequestMapping("/api/v1/auth")
@Log4j2
public class AuthController {

    @Autowired
    private AuthService authService;

    @Autowired
    private LanguageDetailService languageDetailService;

    @Autowired
    private InitSetupService initSetupService;

    @Autowired
    private TaskService taskService;

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public ResponseEntity<BaseDto> login(
            @Valid @RequestBody LoginRequest loginRequest,
            HttpServletRequest request) {

        return authService.login(loginRequest, request);
    }

    @RequestMapping(value = "/logout", method = RequestMethod.GET)
    public void logout(HttpServletRequest request) {
        authService.logout(request);
    }

    @RequestMapping(value = "/defaultcurrency", method = RequestMethod.GET)
    public void usercurrency() {
        /*userProfileService.defaulcurrency();*/
    }

    @RequestMapping(value = "/loadcache", method = RequestMethod.GET)
    public BaseDto loadCache() {
        return languageDetailService.loadCache();
    }

    @RequestMapping(value = "/task/{taskId}/{taskKey}/{taskValue}", method = RequestMethod.GET)
    public void loadTask(@PathVariable("taskId") String taskId, @PathVariable("taskKey") String taskKey,
                         @PathVariable("taskValue") String taskValue) {
        Map<String, Object> var = new HashMap<>();
        var.put(taskKey, taskValue);
        taskService.complete(taskId, var);
    }


    @RequestMapping(value = "/loadbasicdata", method = RequestMethod.GET)
    public BaseDto loadBasicData() {
        return initSetupService.insertBaseData();
    }

    @RequestMapping(value = "/sendforgotpasswordlink", method = RequestMethod.POST)
    public BaseDto sendForgotPasswordLink(@RequestBody ForgotPasswordDto forgotPasswordDto,
                                          HttpServletRequest request) {
        return authService.sendForgotPasswordLink(forgotPasswordDto);
    }

    @RequestMapping(value = "/updatepassword", method = RequestMethod.POST)
    public BaseDto updatePassword(@RequestBody ForgotPasswordDto forgotPasswordDto, HttpServletRequest request) {
        return authService.updatePassword(forgotPasswordDto);
    }

	/*@RequestMapping(value = "/get/user/{id}", method = RequestMethod.GET)
    public BaseDto getRecordAccessLevelForAUser(@PathVariable Long id) {
		log.info("Get by id method called.[" + id + "]");
		return recordAccessLevelService.getRecordAccessLevelForAUser(id);
	}*/

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<BaseDto> handleMethodArgumentNotValidException(MethodArgumentNotValidException error) {
        return ResponseEntity.badRequest().build();
    }

}
