package com.efreightsuite.controller;

import java.util.List;

import javax.servlet.http.HttpServletResponse;

import com.efreightsuite.client.PushApiService;
import com.efreightsuite.dto.*;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.model.DocumentDetail;
import com.efreightsuite.model.Shipment;
import com.efreightsuite.model.ShipmentServiceSignOff;
import com.efreightsuite.model.StockGeneration;
import com.efreightsuite.repository.ShipmentServiceDetailRepository;
import com.efreightsuite.service.ShipmentInvoiceValidationService;
import com.efreightsuite.service.ShipmentService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

@RestController
@RequestMapping(value = "/api/v1/shipment")
public class ShipmentController {

    private static final String LOGGER_PREFIX = "ShipmentController -> ";
    private static final String LOGGER_SUFFIX = "method called.";

    private final Logger logger = LoggerFactory.getLogger(ShipmentController.class);

    @Autowired
    private
    ShipmentService shipmentService;


    @Autowired
    ShipmentServiceDetailRepository shipmentServiceDetailRepository;


    @Autowired
    private
    ShipmentInvoiceValidationService shipmentInvoiceValidationService;

    @Autowired
    private
    PushApiService pushApiService;


    @RequestMapping(value = "/search", method = RequestMethod.POST)
    @Secured("ROLE_CRM.SHIPMENT.LIST")
    public BaseDto search(@RequestBody ShipmentSearchRequestDto searchDto) {
        logger.info(LOGGER_PREFIX + " search " + LOGGER_SUFFIX);
        return shipmentService.search(searchDto);
    }

    @RequestMapping(value = "/createexport", method = RequestMethod.POST)
    public BaseDto createExportService(@RequestBody ImportToExportDto importToExportDto) {
        logger.info(LOGGER_PREFIX + " create " + LOGGER_SUFFIX);
        BaseDto baseDto = shipmentService.createExportService(importToExportDto);

        if (ErrorCode.SUCCESS.equalsIgnoreCase(baseDto.getResponseCode())) {
            Shipment entity = (Shipment) baseDto.getResponseObject();
            pushApiService.pushToShipment(entity.getId(), null);
        }

        return baseDto;
    }

    @RequestMapping(value = "/copy_shipment_search", method = RequestMethod.POST)
    public BaseDto search(@RequestBody CopyShipmentSearchDto copyShipmentSearchDto) {
        logger.info(LOGGER_PREFIX + " search " + LOGGER_SUFFIX);
        return shipmentService.search(copyShipmentSearchDto);
    }

    @RequestMapping(value = "/importExportSearch", method = RequestMethod.POST)
    @Secured("ROLE_CRM.IMPORT_TO_EXPORT.LIST")
    public BaseDto importExportSearch(@RequestBody ImportExportSearchDto searchDto) {
        logger.info(LOGGER_PREFIX + " importExportSearch " + LOGGER_SUFFIX);
        return shipmentService.importExportSearch(searchDto);
    }

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    @Secured("ROLE_CRM.SHIPMENT.CREATE")
    public ResponseEntity<BaseDto> create(@RequestBody Shipment shipment, UriComponentsBuilder uriBuilder) {
        logger.info("Creating shipment for request dated {}", shipment.getShipmentReqDate());
        BaseDto baseDto = shipmentService.create(shipment);
        if (ErrorCode.SUCCESS.equalsIgnoreCase(baseDto.getResponseCode())) {
            Shipment entity = (Shipment) baseDto.getResponseObject();
            pushApiService.pushToShipment(entity.getId(), null);
            return ResponseEntity.created(uriBuilder.path("/api/v1/shipment/get/id/{id}").buildAndExpand(entity.getId()).toUri()).body(baseDto);
        }
        return ResponseEntity.ok().body(baseDto);
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    @Secured("ROLE_CRM.SHIPMENT.MODIFY")
    public BaseDto update(@RequestBody Shipment shipment) {
        logger.info(LOGGER_PREFIX + " update " + LOGGER_SUFFIX);
        BaseDto baseDto = shipmentService.update(shipment);
        if (ErrorCode.SUCCESS.equalsIgnoreCase(baseDto.getResponseCode())) {
            Shipment entity = (Shipment) baseDto.getResponseObject();
            pushApiService.pushToShipment(entity.getId(), null);
        }
        return baseDto;
    }

    @RequestMapping(value = "/get/id/{id}", method = RequestMethod.GET)
    @Secured("ROLE_CRM.SHIPMENT.VIEW")
    public BaseDto getById(@PathVariable Long id) {
        logger.info(LOGGER_PREFIX + " get by id " + LOGGER_SUFFIX);
        return shipmentService.getById(id);
    }

    @RequestMapping(value = "/service/get/id/{id}", method = RequestMethod.GET)
    public BaseDto getServiceById(@PathVariable Long id) {
        logger.info(LOGGER_PREFIX + " getServiceById " + LOGGER_SUFFIX);
        return shipmentService.getServiceById(id);
    }

    @RequestMapping(value = "/get/{poId}", method = RequestMethod.GET)
    public BaseDto getPurchaseOrderById(@PathVariable Long poId) {
        logger.info(LOGGER_PREFIX + " getPurchaseOrderById " + LOGGER_SUFFIX);
        return shipmentService.getPurchaseOrderById(poId);
    }

    @RequestMapping(value = "/getid/{serviceId}", method = RequestMethod.GET)
    public BaseDto getShipmentId(@PathVariable Long serviceId) {
        logger.info(LOGGER_PREFIX + " get by Shipment UID " + LOGGER_SUFFIX);
        return shipmentService.getShipmentId(serviceId);
    }

    @RequestMapping(value = "/getshipmentid/byserviceuid/{serviceuid}", method = RequestMethod.GET)
    public BaseDto getShipmentIdByServiceUid(@PathVariable String serviceuid) {
        logger.info(LOGGER_PREFIX + " get Shipment Id By ServiceUid " + LOGGER_SUFFIX);
        return shipmentService.getShipmentIdByServiceUid(serviceuid);
    }


    @RequestMapping(value = "/getserviceid/byserviceuid/{serviceuid}", method = RequestMethod.GET)
    public BaseDto getServiceIdByServiceUid(@PathVariable String serviceuid) {
        logger.info(LOGGER_PREFIX + " get Service Id By ServiceUid " + LOGGER_SUFFIX);
        return shipmentService.getServiceIdByServiceUid(serviceuid);
    }


    @RequestMapping(value = "/get/serviceUid/{serviceUid}", method = RequestMethod.GET)
    public BaseDto getService(@PathVariable String serviceUid) {
        logger.info(LOGGER_PREFIX + " get Shipment Id By ServiceUid " + LOGGER_SUFFIX);
        return shipmentService.getService(serviceUid);
    }


    @RequestMapping(value = "/get/shipmentuid/{shipmentUid}", method = RequestMethod.GET)
    public BaseDto getByShipmentUid(@PathVariable String shipmentUid) {
        logger.info(LOGGER_PREFIX + " get by Shipment UID " + LOGGER_SUFFIX);
        return shipmentService.getByShipmentUid(shipmentUid);
    }

    //for stockservice
    @RequestMapping(value = "/updatebyShipmentuid", method = RequestMethod.POST)
    public BaseDto updateShipmentUid(@RequestBody StockGeneration stockGeneration) {
        logger.info(LOGGER_PREFIX + " get by Shipment UID " + LOGGER_SUFFIX);
        return shipmentService.updatebyShipmentuid(stockGeneration);

    }


    @RequestMapping(value = "/statuschange", method = RequestMethod.POST)
    public BaseDto statuschange(@RequestBody ShipmentDto shipmentSto) {
        logger.info(LOGGER_PREFIX + " statuschange.");

        BaseDto baseDto = shipmentService.statusChange(shipmentSto);
        if (ErrorCode.SUCCESS.equalsIgnoreCase(baseDto.getResponseCode())) {
            Shipment entity = (Shipment) baseDto.getResponseObject();
            pushApiService.pushToShipment(entity.getId(), null);
        }
        return baseDto;
    }

    @RequestMapping(value = "/files/{id}/{type}/{swapped}", method = RequestMethod.GET)
    public void getFile(@PathVariable Long id, @PathVariable String type, @PathVariable("swapped") boolean swapped, HttpServletResponse response) {
        shipmentService.downloadAttachment(id, type, swapped, response);
    }


    @RequestMapping(value = "/report/shipment/{serviceId}", method = RequestMethod.GET)
    public BaseDto commercialInvoice(@PathVariable("serviceId") Long serviceId) {
        return shipmentService.documentListByServiceId(serviceId);
    }

    @RequestMapping(value = "/autoimport/serviceId/{serviceId}", method = RequestMethod.GET)
    public BaseDto autoImport(@PathVariable Long serviceId) {
        logger.info(LOGGER_PREFIX + " autoImport " + LOGGER_SUFFIX);
        return shipmentService.executeAutoImportProcess(serviceId);
    }

    @RequestMapping(value = "/invoicechecking/{serviceDetailId}", method = RequestMethod.GET)
    public BaseDto shipmentInvoicCheckingByServiceDetailId(@PathVariable("serviceDetailId") List<Long> serviceDetailId) {
        logger.info(LOGGER_PREFIX + " shipmentInvoicCheckingByServiceDetailId method called. - " + serviceDetailId);
        return shipmentInvoiceValidationService.shipmentInvoicCheckingByServiceDetailId(serviceDetailId);
    }

    @RequestMapping(value = "/get/search/keyword", method = RequestMethod.POST)
    public BaseDto search(@RequestBody SearchRequest searchRequest) {
        logger.info("Search " + LOGGER_SUFFIX);
        return shipmentService.search(searchRequest);
    }


    @RequestMapping(value = "/service/get/search/keyword", method = RequestMethod.POST)
    public BaseDto searchService(@RequestBody SearchRequest searchRequest) {
        logger.info("Search " + LOGGER_SUFFIX);
        return shipmentService.searchService(searchRequest);
    }

    @RequestMapping(value = "/shipmentIdBasedOnUid/{shipmentUid}", method = RequestMethod.GET)
    public BaseDto getShipmentIdBasedOnUid(@PathVariable String shipmentUid) {
        logger.info(LOGGER_PREFIX + " getShipmentIdBasedOnUid " + LOGGER_SUFFIX);
        return shipmentService.getShipmentIdBasedOnUid(shipmentUid);
    }

    @RequestMapping(value = "/document/update", method = RequestMethod.POST)
    public BaseDto update(@RequestBody DocumentDetail documentDetail) {
        logger.info("ConsolController -> update " + LOGGER_SUFFIX);
        BaseDto baseDto = shipmentService.updateDocument(documentDetail);
        if (ErrorCode.SUCCESS.equalsIgnoreCase(baseDto.getResponseCode())) {
            pushApiService.pushToShipment(Long.parseLong(baseDto.getResponseObject().toString()), null);
        }
        return baseDto;
    }

    @RequestMapping(value = "/genDoNum/{documentId}", method = RequestMethod.GET)
    public BaseDto generateDoNuM(@PathVariable Long documentId) {
        logger.info("shipmentService");
        return shipmentService.toBeGenerateDONumber(documentId);
    }

    @RequestMapping(value = "/genCanNum/{documentId}", method = RequestMethod.GET)
    public BaseDto generateCanNuM(@PathVariable Long documentId) {
        logger.info("shipmentService");
        return shipmentService.toBeGenerateCanNumber(documentId);
    }

    @RequestMapping(value = "/service/signoff", method = RequestMethod.POST)
    public BaseDto saveShipmentServiceSignOff(@RequestBody ShipmentServiceSignOff shipmentServiceSignOff) {
        logger.info("saveShipmentServiceSignOff");
        BaseDto baseDto = shipmentService.saveShipmentServiceSignOff(shipmentServiceSignOff);

        if (shipmentServiceSignOff.getShipmentUid() != null) {
            pushApiService.pushToShipment(null, shipmentServiceSignOff.getShipmentUid());
        }

        return baseDto;
    }

    @RequestMapping(value = "/service/unsignoff", method = RequestMethod.POST)
    public BaseDto saveShipmentServiceUnSignOff(@RequestBody ShipmentServiceSignOff shipmentServiceSignOff) {
        logger.info("saveShipmentServiceSignOff");
        BaseDto baseDto = shipmentService.saveShipmentServiceUnSignOff(shipmentServiceSignOff);
        if (shipmentServiceSignOff.getShipmentUid() != null) {
            pushApiService.pushToShipment(null, shipmentServiceSignOff.getShipmentUid());
        }
        return baseDto;
    }

    @RequestMapping(value = "/service/updateAttachEvent", method = RequestMethod.POST)
    public BaseDto updateAE(@RequestBody List<AttachEventDto> aeDto) {
        logger.info("Do print all controller called.");
        return shipmentService.updateAE(aeDto);
    }

    @RequestMapping(value = "/getshipmentchargehistory/{shipmentUid}", method = RequestMethod.GET)
    public BaseDto getShipmentChargeHistory(@PathVariable String shipmentUid) {
        logger.info("getShipmentChargeHistory called");
        return shipmentService.getShipmentChargeHistory(shipmentUid);
    }

    @RequestMapping(value = "/trackShipment/{shipmentId}", method = RequestMethod.GET)
    public BaseDto trackShipment(@PathVariable Long shipmentId) {
        logger.info("shipmentService.trackShipment");
        return shipmentService.trackShipment(shipmentId);
    }

    @RequestMapping(value = "/get/service/signoff/status/{serviceId}", method = RequestMethod.GET)
    public BaseDto getSignOffStatus(@PathVariable Long serviceId) {
        logger.info("shipmentService.getSignOffStatus");
        return shipmentService.getSignOffStatus(serviceId);
    }

    @RequestMapping(value = "/isshipmentcreatedfromquotation/{quotationNo}", method = RequestMethod.GET)
    public BaseDto isShipmentCreatedFromQuotation(@PathVariable String quotationNo) {
        logger.info("isShipmentCreatedFromQuotation method called..[" + quotationNo + "]");
        return shipmentService.isShipmentCreatedFromQuotation(quotationNo);
    }

    @RequestMapping(value = "/getShipmentbymawb/{mawbNo}/{serviceId}", method = RequestMethod.GET)
    public BaseDto getShipmentByMawbNo(@PathVariable String mawbNo, @PathVariable Long serviceId) {
        return shipmentService.getShipmentByMawbNo(mawbNo, serviceId);
    }

    @RequestMapping(value = "/sendTrackShipmentStatusByMail", method = RequestMethod.POST)
    public BaseDto postTrackShipmentMail(@RequestBody TrackingShipmentRequest trackingShipmentRequest){
        return shipmentService.triggerMail(trackingShipmentRequest.getMailData(),trackingShipmentRequest.getShipmentUid());
    }
}
