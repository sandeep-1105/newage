package com.efreightsuite.controller;

import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.CountryMasterSearchDto;
import com.efreightsuite.dto.SearchRequest;
import com.efreightsuite.model.CountryMaster;
import com.efreightsuite.service.CountryMasterService;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Log4j2
@RequestMapping(value = "/api/v1/countrymaster")
public class CountryMasterController {

    @Autowired
    private CountryMasterService countryMasterService;

    @RequestMapping(value = "/get/id/{id}", method = RequestMethod.GET)
    @Secured("ROLE_MASTER.GENERAL.COUNTRY.VIEW")
    public BaseDto get(@PathVariable Long id) {
        log.info("get method called.");
        return countryMasterService.get(id);
    }

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    @Secured("ROLE_MASTER.GENERAL.COUNTRY.CREATE")
    public BaseDto create(@RequestBody CountryMaster countryMaster) {
        log.info("Create method called.");
        return countryMasterService.create(countryMaster);
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    @Secured("ROLE_MASTER.GENERAL.COUNTRY.MODIFY")
    public BaseDto update(@RequestBody CountryMaster countryMaster) {
        log.info("Update method called.");
        return countryMasterService.update(countryMaster);
    }

    @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
    @Secured("ROLE_MASTER.GENERAL.COUNTRY.DELETE")
    public BaseDto delete(@PathVariable Long id) {
        log.info("Delete method called....");
        return countryMasterService.delete(id);
    }

    @RequestMapping(value = "/get/search", method = RequestMethod.POST)
    @Secured("ROLE_MASTER.GENERAL.COUNTRY.LIST")
    public BaseDto get(@RequestBody CountryMasterSearchDto searchDto) {
        log.info("getall method called.");
        return countryMasterService.search(searchDto);
    }

    @RequestMapping(value = "/get/search/keyword", method = RequestMethod.POST)
    public BaseDto get(@RequestBody SearchRequest searchRequest) {
        log.info("getall method called.");
        return countryMasterService.search(searchRequest);
    }

    @RequestMapping(value = "/get/search/exclude", method = RequestMethod.POST)
    public BaseDto searchCountryNotInList(@RequestBody SearchRequest searchRequest) {
        log.info("searchCountryNotInList method called. :");
        return countryMasterService.searchCountryNotInList(searchRequest);
    }
}
