package com.efreightsuite.controller;

import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.CountryReportConfigSearchDto;
import com.efreightsuite.model.CountryReportConfigMaster;
import com.efreightsuite.service.CountryReportConfigService;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping(value = "/api/v1/cntryrprtcnfg")
@Log4j2
public class CountryReportConfigController {
    @Autowired
    private
    CountryReportConfigService pageCountryService;

    /**
     * Method - search Returns Paginated Search Result based on Search Keyword
     *
     * @return SearchRespDto
     */

    @RequestMapping(value = "/get/search", method = RequestMethod.POST)
    public BaseDto search(@RequestBody CountryReportConfigSearchDto searchDto) {
        log.info("Search method is called...Search Dto: " + searchDto);
        return pageCountryService.search(searchDto);
    }

    /**
     * Method - create Creates and returns a new PageCountryMaster
     *
     * @returns PageCountryMaster
     */

    @Secured("ROLE_SETUP.REPORT_CONFIGURATION_COUNTRYMASTER_REPORT.CREATE")
    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public BaseDto create(@RequestBody CountryReportConfigMaster pageCountryMaster) {
        log.info("create method is called...Search Dto: " + pageCountryMaster);
        return pageCountryService.create(pageCountryMaster);
    }

    /**
     * Method - getById Returns PageCountryMaster based on given reason id
     *
     * @return PageCountryMaster
     */

    @RequestMapping(value = "/get/id/{id}", method = RequestMethod.GET)
    public BaseDto getById(@PathVariable Long id) {
        log.info("getById method called...." + id);
        return pageCountryService.getById(id);
    }

    /**
     * Method - update returns a updated PageCountryMaster
     *
     * @returns PageCountryMaster
     */

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    @Secured("ROLE_SETUP.REPORT_CONFIGURATION_COUNTRYMASTER_REPORT.MODIFY")
    public BaseDto update(@RequestBody CountryReportConfigMaster exsistingpPageCountryMaster) {
        log.info("Update method called...." + exsistingpPageCountryMaster);
        return pageCountryService.update(exsistingpPageCountryMaster);
    }

    /**
     * This method is used to delete PageCountryMaster details .
     *
     * @return .
     */

    @Secured("ROLE_SETUP.REPORT_CONFIGURATION_COUNTRYMASTER_REPORT.DELETE")
    @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
    public BaseDto delete(@PathVariable Long id) {
        log.info("Delete method called...." + id);
        return pageCountryService.delete(id);
    }


}
