package com.efreightsuite.controller;

import java.util.List;

import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.DateConfigurationDto;
import com.efreightsuite.model.DateConfiguration;
import com.efreightsuite.service.DateConfigurationService;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Log4j2
@RequestMapping(value = "/api/v1/dateconfiguration")
public class DateConfigurationController {

    @Autowired
    private
    DateConfigurationService dateConfigurationService;

    @RequestMapping(value = "/get/search", method = RequestMethod.POST)
    @Secured("ROLE_SETUP.APP_CONFIGURATION.JOB_DATE.LIST")
    public BaseDto getAll(@RequestBody DateConfigurationDto searchDto) {
        log.info("Search method called.");
        return dateConfigurationService.search(searchDto);
    }

    @RequestMapping(value = "/get/{id}", method = RequestMethod.POST)
    public BaseDto get(@RequestBody DateConfigurationDto searchDto) {
        return dateConfigurationService.get(searchDto);
    }

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    @Secured("ROLE_SETUP.APP_CONFIGURATION.JOB_DATE.CREATE")
    public BaseDto create(@RequestBody List<DateConfiguration> dateConfigurationList) {
        return dateConfigurationService.create(dateConfigurationList);
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    @Secured("ROLE_SETUP.APP_CONFIGURATION.JOB_DATE.MODIFY")
    public BaseDto update(@RequestBody DateConfiguration dateConfiguration) {
        return dateConfigurationService.update(dateConfiguration);
    }

    @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
    @Secured("ROLE_SETUP.APP_CONFIGURATION.JOB_DATE.DELETE")
    public BaseDto delete(@PathVariable Long id) {

        log.info("Delete method is called.. : [" + id + "]");

        return dateConfigurationService.delete(id);
    }


}
