package com.efreightsuite.controller;

import javax.servlet.http.HttpServletResponse;

import com.efreightsuite.client.PushApiService;
import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.InvoiceChargeDto;
import com.efreightsuite.dto.InvoiceSearchDto;
import com.efreightsuite.dto.SearchRequest;
import com.efreightsuite.dto.ServiceTaxCategoryMasterSearchDto;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.model.InvoiceCreditNote;
import com.efreightsuite.service.InvoiceCreditNoteService;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Log4j2
@RequestMapping(value = "/api/v1/invoicecreditnote")
public class InvoiceCreditNoteController {
    @Autowired
    private
    InvoiceCreditNoteService invoiceCreditNoteService;

    @Autowired
    private
    PushApiService pushApiService;


    @RequestMapping(value = "/search", method = RequestMethod.POST)
    @Secured({"ROLE_FINANCE.CREDIT_NOTE_COST.LIST", "ROLE_FINANCE.CREDIT_NOTE_REVENUE.LIST", "ROLE_FINANCE.INVOICE.LIST"})
    public BaseDto search(@RequestBody InvoiceSearchDto searchDto) {
        log.info("InvoiceController -> search method called.");
        return invoiceCreditNoteService.search(searchDto);
    }

    @RequestMapping(value = "/get/invoice/count", method = RequestMethod.POST)
    public BaseDto getInvoiceCount(@RequestBody InvoiceSearchDto searchDto) {
        log.info("InvoiceController 123 -> search method called." + searchDto.getMasterUid());
        return invoiceCreditNoteService.getInvoiceCount(searchDto);
    }

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    @Secured({"ROLE_FINANCE.CREDIT_NOTE_COST.CREATE", "ROLE_FINANCE.CREDIT_NOTE_REVENUE.CREATE", "ROLE_FINANCE.INVOICE.CREATE"})
    public BaseDto create(@RequestBody InvoiceCreditNote invoiceCreditNote) {
        log.info("InvoiceController -> create method called.");
        BaseDto baseDto = invoiceCreditNoteService.create(invoiceCreditNote);
        if (baseDto.getResponseCode().toUpperCase().equals(ErrorCode.SUCCESS)) {
            InvoiceCreditNote entity = (InvoiceCreditNote) baseDto.getResponseObject();
            pushApiService.pushToInvoice(entity.getId());
        }
        return baseDto;
    }

    @RequestMapping(value = "/get/id/{id}", method = RequestMethod.GET)
    @Secured({"ROLE_FINANCE.CREDIT_NOTE_COST.VIEW", "ROLE_FINANCE.CREDIT_NOTE_REVENUE.VIEW", "ROLE_FINANCE.INVOICE.VIEW"})
    public BaseDto getById(@PathVariable Long id) {
        log.info("InvoiceController -> get by id method called.");
        return invoiceCreditNoteService.getById(id);
    }

    @RequestMapping(value = "/get/invoiceNo/{invoiceNo}/{daybookId}", method = RequestMethod.GET)
    public BaseDto getByInvoiceNo(@PathVariable String invoiceNo, @PathVariable Long daybookId) {
        log.info("InvoiceController -> getByInvoiceNo method called.");
        return invoiceCreditNoteService.getByInvoiceNo(invoiceNo, daybookId);
    }

    @RequestMapping(value = "/getServiceTaxPercentage/{serviceId}/{chargeId}/{countryId}/{companyId}", method = RequestMethod.GET)
    public BaseDto getServiceTaxPercentage(@PathVariable("serviceId") Long serviceId, @PathVariable("chargeId") Long chargeId, @PathVariable("countryId") Long countryId, @PathVariable("companyId") Long companyId) {
        log.info("InvoiceController -> get ServiceTax Percentage method called.");
        return invoiceCreditNoteService.getServiceTaxPercentage(serviceId, chargeId, countryId, companyId);
    }

    @RequestMapping(value = "/getShipmentServiceInvoiceList", method = RequestMethod.POST)
    public BaseDto getShipmentServiceInvoiceList(@RequestBody SearchRequest searchRequest) {
        log.info("InvoiceController ->getShipmentServiceInvoiceList method called.");
        return invoiceCreditNoteService.getShipmentServiceInvoiceList(searchRequest.getParam1(), searchRequest.getParam2());
    }

    @RequestMapping(value = "/getChargeListWithInvoiceNo", method = RequestMethod.POST)
    public BaseDto getChargeListWithInvoiceNo(@RequestBody InvoiceChargeDto invoiceChargeDto) {
        log.info("InvoiceController ->getChargeListWithInvoiceNo method called.");
        return invoiceCreditNoteService.getChargeListWithInvoiceNo(invoiceChargeDto);
    }

    @RequestMapping(value = "/getConsolInvoiceList/{consolUid}", method = RequestMethod.GET)
    public BaseDto getConsolInvoiceList(@PathVariable String consolUid) {
        log.info("InvoiceController ->getConsolInvoiceList method called.");
        return invoiceCreditNoteService.getConsolInvoiceList(consolUid);
    }


    @RequestMapping(value = "/get/attachment/{id}", method = RequestMethod.GET)
    public BaseDto getAttachmentById(@PathVariable Long id) {
        log.info("InvoiceController getAttachmentById is called. [ " + id + "]");
        return invoiceCreditNoteService.getAttachmentById(id);
    }


    @RequestMapping(value = "/files/{id}", method = RequestMethod.GET)
    public void getFile(@PathVariable Long id, HttpServletResponse response) {
        invoiceCreditNoteService.downloadAttachment(id, response);
    }

    /**
     * To get the all Service Tax Catogory Master
     *
     * @return BaseDto
     */
    @RequestMapping(value = "/get/servicetaxcategory/search/keyword", method = RequestMethod.POST)
    public BaseDto getallServiceTaxCategory(@RequestBody ServiceTaxCategoryMasterSearchDto searchDto) {
        log.info("getall method called.");
        return invoiceCreditNoteService.getallServiceTaxCategory(searchDto);
    }

    @RequestMapping(value = "/get/servicetaxcategory/id/{id}", method = RequestMethod.GET)
    public BaseDto getServiceTaxCategoryById(@PathVariable Long id) {
        log.info("InvoiceController -> getServiceTaxCategoryById method called.");
        return invoiceCreditNoteService.getServiceTaxCategoryById(id);
    }

    @RequestMapping(value = "/get/searchglchargemapping/keyword/{serviceId}", method = RequestMethod.POST)
    public BaseDto searchGlChargeMapping(@RequestBody SearchRequest searchRequest, @PathVariable Long serviceId) {
        log.info("Search method called.[" + searchRequest + "]");
        return invoiceCreditNoteService.searchGlChargeMapping(searchRequest, serviceId);
    }


    @RequestMapping(value = "/get/search/keyword/{daybookId}", method = RequestMethod.POST)
    public BaseDto searchByDaybook(@RequestBody SearchRequest searchRequest, @PathVariable Long daybookId) {
        log.info("Search method called.[" + searchRequest + "]");
        return invoiceCreditNoteService.searchByDaybook(searchRequest, daybookId, searchRequest.getParam1(), searchRequest.getParam2(), searchRequest.getParam3());
    }

    @RequestMapping(value = "/get/getratesfromprovision", method = RequestMethod.POST)
    public BaseDto getRatesFromProvision(@RequestBody SearchRequest searchRequest) {
        log.info("InvoiceController -> getRatesFromProvision method called.");
        return invoiceCreditNoteService.getRatesFromProvision(searchRequest);
    }

}
