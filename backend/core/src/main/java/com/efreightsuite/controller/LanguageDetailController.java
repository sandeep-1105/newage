package com.efreightsuite.controller;

import java.util.List;

import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.LanguageDetailSearchReqDto;
import com.efreightsuite.dto.SearchRequest;
import com.efreightsuite.model.LanguageDetail;
import com.efreightsuite.service.LanguageDetailService;
import com.efreightsuite.service.LanguageService;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Log4j2
@RequestMapping(value = "/api/v1/language")
public class LanguageDetailController {


    @Autowired
    private
    LanguageDetailService languageDetailService;

    @Autowired
    private
    LanguageService languageService;

    @RequestMapping(value = "/nls/{language}", method = RequestMethod.GET)
    public BaseDto getNLS(@PathVariable String language) {
        log.info("LanguageDetailController.createDefault is called.");
        return languageDetailService.getNls(language);
    }

    @RequestMapping(value = "/get/search/keyword", method = RequestMethod.POST)
    public BaseDto search(@RequestBody SearchRequest searchRequest) {
        log.info("LanguageDetailController.Search method is called...Search Dto: " + searchRequest);
        return languageService.search(searchRequest);
    }


    @RequestMapping(value = "/get/search", method = RequestMethod.POST)
    @Secured("ROLE_SETUP.ERROR_MESSAGE.LIST")
    public BaseDto search(@RequestBody LanguageDetailSearchReqDto searchDto) {
        log.info("LanguageDetailController.Search method called.");
        return languageDetailService.search(searchDto);
    }

    @RequestMapping(value = "/get/id/{id}", method = RequestMethod.GET)
    public BaseDto get(@PathVariable Long id) {
        log.info("LanguageDetailController.get method is called." + id + "]");
        return languageDetailService.get(id);
    }

    @RequestMapping(value = "/get/errorCode/{errorCode}", method = RequestMethod.GET)
    public BaseDto get(@PathVariable String errorCode) {
        log.info("LanguageDetailController.get is called." + errorCode + "]");
        return languageDetailService.get(errorCode);
    }

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    @Secured("ROLE_SETUP.ERROR_MESSAGE.CREATE")
    public BaseDto create(@RequestBody List<LanguageDetail> languageDetailList) {
        log.info("LanguageDetailController.create method called..");
        return languageDetailService.create(languageDetailList);
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    @Secured("ROLE_SETUP.ERROR_MESSAGE.MODIFY")
    public BaseDto update(@RequestBody LanguageDetail languageDetail) {
        log.info("LanguageDetailController.Update method called...");
        return languageDetailService.update(languageDetail);
    }

    @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
    public BaseDto delete(@PathVariable Long id) {
        log.info("LanguageDetailController.delete method is called.. : [" + id + "]");
        return languageDetailService.delete(id);
    }


}

