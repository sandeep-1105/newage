package com.efreightsuite.controller.representations;

import java.util.Date;
import java.util.List;

import javax.validation.constraints.NotNull;

import com.efreightsuite.enumeration.EnquiryStatus;
import com.efreightsuite.model.CompanyMaster;
import com.efreightsuite.model.CountryMaster;
import com.efreightsuite.model.EmployeeMaster;
import com.efreightsuite.model.EnquiryDetail;
import com.efreightsuite.model.EnquiryLog;
import com.efreightsuite.model.LocationMaster;
import com.efreightsuite.model.PartyMaster;
import com.efreightsuite.util.JsonDateDeserializer;
import com.efreightsuite.util.JsonDateSerializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonDeserialize(builder = EnquiryLogRequest.Builder.class)
public class EnquiryLogRequest {

    private final Long id;

    private final Date quoteBy;

    private final Date receivedOn;

    private final EnquiryStatus status;

    private final PartyMaster partyMaster;

    private final EmployeeMaster salesman;

    private final EmployeeMaster loggedBy;

    private final EmployeeMaster salesCoOrdinator;

    private final CompanyMaster companyMaster;

    private final LocationMaster locationMaster;

    private final CountryMaster countryMaster;

    private final List<EnquiryDetail> enquiryDetailList;

    private EnquiryLogRequest(Builder builder) {
        id = builder.id;
        quoteBy = builder.quoteBy;
        receivedOn = builder.receivedOn;
        status = builder.status;
        partyMaster = builder.partyMaster;
        salesman = builder.salesman;
        loggedBy = builder.loggedBy;
        salesCoOrdinator = builder.salesCoOrdinator;
        companyMaster = builder.companyMaster;
        locationMaster = builder.locationMaster;
        countryMaster = builder.countryMaster;
        enquiryDetailList = builder.enquiryDetailList;
    }

    public Long getId() {
        return this.id;
    }

    public Date getQuoteBy() {
        return this.quoteBy;
    }

    public Date getReceivedOn() {
        return this.receivedOn;
    }

    public EnquiryStatus getStatus() {
        return this.status;
    }

    public PartyMaster getPartyMaster() {
        return this.partyMaster;
    }

    private EmployeeMaster getSalesman() {
        return this.salesman;
    }

    public EmployeeMaster getLoggedBy() {
        return this.loggedBy;
    }

    public EmployeeMaster getSalesCoOrdinator() {
        return this.salesCoOrdinator;
    }

    public CompanyMaster getCompanyMaster() {
        return this.companyMaster;
    }

    public LocationMaster getLocationMaster() {
        return this.locationMaster;
    }

    public CountryMaster getCountryMaster() {
        return this.countryMaster;
    }

    public List<EnquiryDetail> getEnquiryDetailList() {
        return this.enquiryDetailList;
    }

    public EnquiryLog toEnquiryLog() {
        return toEnquiryLog(null);
    }

    public EnquiryLog toEnquiryLog(EnquiryLog existingEnquiryLog) {
        EnquiryLog enquiryLog = existingEnquiryLog == null ? new EnquiryLog() : existingEnquiryLog;
        enquiryLog.setPartyMaster(this.partyMaster);
        enquiryLog.setQuoteBy(this.quoteBy);
        enquiryLog.setReceivedOn(this.receivedOn);
        enquiryLog.setStatus(this.status);
        enquiryLog.setSalesCoOrdinator(this.salesCoOrdinator);
        enquiryLog.setLoggedBy(this.loggedBy);
        enquiryLog.setCompanyMaster(this.companyMaster);
        enquiryLog.setLocationMaster(this.locationMaster);
        enquiryLog.setCountryCode(this.countryMaster.getCountryCode());
        enquiryLog.setLocationCode(this.locationMaster.getLocationCode());
        enquiryLog.setCompanyCode(this.companyMaster.getCompanyCode());
        enquiryLog.setEnquiryDetailList(this.enquiryDetailList);
        enquiryLog.setSalesman(this.salesman);
        enquiryLog.setLoggedOn(new Date());

        if (this.getLoggedBy() != null) {
            enquiryLog.setLoggedByCode(enquiryLog.getLoggedBy().getEmployeeCode());
        }
        if (this.getPartyMaster() != null) {
            enquiryLog.setPartyMasterCode(enquiryLog.getPartyMaster().getPartyCode());
        }
        if (this.getSalesCoOrdinator() != null) {
            enquiryLog.setSalesCoOrdinatorCode(enquiryLog.getSalesCoOrdinator().getEmployeeCode());
        }
        if (this.getSalesman() != null) {
            enquiryLog.setSalesmanCode(enquiryLog.getSalesman().getEmployeeCode());
        }
        return enquiryLog;
    }

    @JsonPOJOBuilder()
    public static final class Builder {
        private Long id;
        @JsonSerialize(using = JsonDateSerializer.class)
        @JsonDeserialize(using = JsonDateDeserializer.class)
        private Date quoteBy;
        @JsonSerialize(using = JsonDateSerializer.class)
        @JsonDeserialize(using = JsonDateDeserializer.class)
        private Date receivedOn;
        private EnquiryStatus status;
        private PartyMaster partyMaster;
        @NotNull
        private EmployeeMaster salesman;
        @NotNull
        private EmployeeMaster loggedBy;
        @NotNull
        private EmployeeMaster salesCoOrdinator;
        @NotNull
        private CompanyMaster companyMaster;
        @NotNull
        private LocationMaster locationMaster;
        @NotNull
        private CountryMaster countryMaster;
        @NotNull
        private List<EnquiryDetail> enquiryDetailList;

        public Builder withId(Long val) {
            this.id = val;
            return this;
        }

        public Builder withQuoteBy(Date val) {
            quoteBy = val;
            return this;
        }

        public Builder withReceivedOn(Date val) {
            receivedOn = val;
            return this;
        }

        public Builder withStatus(EnquiryStatus val) {
            status = val;
            return this;
        }

        public Builder withPartyMaster(PartyMaster val) {
            partyMaster = val;
            return this;
        }

        public Builder withSalesman(EmployeeMaster val) {
            salesman = val;
            return this;
        }

        public Builder withLoggedBy(EmployeeMaster val) {
            loggedBy = val;
            return this;
        }

        public Builder withSalesCoOrdinator(EmployeeMaster val) {
            salesCoOrdinator = val;
            return this;
        }

        public Builder withCompanyMaster(CompanyMaster val) {
            companyMaster = val;
            return this;
        }

        public Builder withLocationMaster(LocationMaster val) {
            locationMaster = val;
            return this;
        }

        public Builder withCountryMaster(CountryMaster val) {
            countryMaster = val;
            return this;
        }

        public Builder withEnquiryDetailList(List<EnquiryDetail> val) {
            enquiryDetailList = val;
            return this;
        }

        public EnquiryLogRequest build() {
            return new EnquiryLogRequest(this);
        }
    }
}
