package com.efreightsuite.controller;

import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.service.RegularExpressionService;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Log4j2
@RequestMapping(value = "/api/v1/regularexpression")
public class RegularExpressionController {

    @Autowired
    private
    RegularExpressionService regularExpressionService;

    @RequestMapping(value = "/get/all", method = RequestMethod.GET)
    public BaseDto getall() {
        return regularExpressionService.getAll();
    }
}
