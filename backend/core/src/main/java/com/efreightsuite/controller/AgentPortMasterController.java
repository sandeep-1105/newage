package com.efreightsuite.controller;

import java.util.List;

import com.efreightsuite.dto.AgentPortMasterSearchDto;
import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.model.AgentPortMaster;
import com.efreightsuite.service.AgentPortMasterService;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@Log4j2
@RestController
@RequestMapping(value = "/api/v1/agentportmaster")
public class AgentPortMasterController {

    @Autowired
    private
    AgentPortMasterService agentPortMasterService;

    @RequestMapping(value = "/get/search/{serviceId}/{portId}", method = RequestMethod.GET)
    public BaseDto searchByServicePort(@PathVariable("serviceId") Long serviceId, @PathVariable("portId") Long portId) {
        log.info("get by service and port id" + serviceId + "---" + portId);
        return agentPortMasterService.searchByService(serviceId, portId);
    }


    @RequestMapping(value = "/get/search/keyword", method = RequestMethod.POST)
    @Secured("ROLE_SETUP.APP_CONFIGURATION.AGENT_PORT.LIST")
    public BaseDto get(@RequestBody AgentPortMasterSearchDto searchDto) {
        log.info("getall method called.");
        return agentPortMasterService.search(searchDto);
    }


    @RequestMapping(value = "/get/id/{id}", method = RequestMethod.GET)
    public BaseDto get(@PathVariable Long id) {
        log.info("get method called." + id);
        return agentPortMasterService.get(id);
    }

    @RequestMapping(value = "/get/agent/{portCode}", method = RequestMethod.GET)
    public BaseDto getAgent(@PathVariable String portCode) {
        log.info("getAgent method called." + portCode);
        return agentPortMasterService.getAgentByPortCode(portCode);
    }


    @RequestMapping(value = "/create", method = RequestMethod.POST)
    @Secured("ROLE_SETUP.APP_CONFIGURATION.AGENT_PORT.CREATE")
    public BaseDto create(@RequestBody List<AgentPortMaster> agentPortMaster) {
        log.info("Create method is called.");
        return agentPortMasterService.create(agentPortMaster);
    }


    @RequestMapping(value = "/update", method = RequestMethod.POST)
    @Secured("ROLE_SETUP.APP_CONFIGURATION.AGENT_PORT.MODIFY")
    public BaseDto update(@RequestBody AgentPortMaster agentPortMaster) {
        log.info("Update method is called.");
        return agentPortMasterService.update(agentPortMaster);
    }

    @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
    @Secured("ROLE_SETUP.APP_CONFIGURATION.AGENT_PORT.DELETE")
    public BaseDto delete(@PathVariable Long id) {
        log.info("Delete method called....");
        return agentPortMasterService.delete(id);
    }
}
