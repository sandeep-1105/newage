package com.efreightsuite.controller;

import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.GradeMasterSearchDto;
import com.efreightsuite.dto.SearchRequest;
import com.efreightsuite.model.GradeMaster;
import com.efreightsuite.service.GradeMasterService;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


@RestController
@Log4j2
@RequestMapping(value = "/api/v1/grademaster")
public class GradeMasterController {

    @Autowired
    private
    GradeMasterService gradeMasterService;

    @RequestMapping(value = "/get/search", method = RequestMethod.POST)
    public BaseDto get(@RequestBody SearchRequest searchRequest) {
        log.info("getall method called.");
        return gradeMasterService.search(searchRequest);
    }

    @RequestMapping(value = "/get/search/keyword", method = RequestMethod.POST)
    @Secured("ROLE_MASTER.GENERAL.GRADE.LIST")
    public BaseDto get(@RequestBody GradeMasterSearchDto searchDto) {
        log.info("getall method called.");
        return gradeMasterService.search(searchDto);
    }

    @RequestMapping(value = "/get/id/{id}", method = RequestMethod.GET)
    @Secured("ROLE_MASTER.GENERAL.GRADE.VIEW")
    public BaseDto get(@PathVariable Long id) {
        log.info("get method called." + id);
        return gradeMasterService.get(id);
    }

    @RequestMapping(value = "/save", method = RequestMethod.POST)
    @Secured("ROLE_MASTER.GENERAL.GRADE.MODIFY")
    public BaseDto saveOrUpdate(@RequestBody GradeMaster gradeMaster) {
        log.info("save or update method is called.");
        return gradeMasterService.saveOrUpdate(gradeMaster);
    }

    @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
    @Secured("ROLE_MASTER.GENERAL.GRADE.DELETE")
    public BaseDto delete(@PathVariable Long id) {
        log.info("Delete method called....");
        return gradeMasterService.delete(id);
    }
}
