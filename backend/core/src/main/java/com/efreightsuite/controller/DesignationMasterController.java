package com.efreightsuite.controller;

import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.DesignationSearchDto;
import com.efreightsuite.dto.SearchRequest;
import com.efreightsuite.model.DesignationMaster;
import com.efreightsuite.service.DesignationMasterService;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/api/v1/designationmaster")
@Log4j2
public class DesignationMasterController {

    @Autowired
    private
    DesignationMasterService designationMasterService;

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    @Secured({"ROLE_MASTER.HRMS.DESIGNATION.CREATE", "ROLE_MASTER.HRMS.DESIGNATION.MODIFY"})
    public BaseDto create(@RequestBody DesignationMaster Master) {
        log.info("Create method is Invoked..");
        return designationMasterService.saveOrUpdate(Master);
    }

    @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
    @Secured("ROLE_MASTER.HRMS.DESIGNATION.DELETE")
    public BaseDto delete(@PathVariable Long id) {
        log.info("Delete method is Invoked.. : [" + id + "]");
        return designationMasterService.delete(id);
    }

    @RequestMapping(value = "/get/id/{id}")
    @Secured("ROLE_MASTER.HRMS.DESIGNATION.LIST")
    public BaseDto get(@PathVariable Long id) {
        log.info("get is called..[ " + id + " ]");
        return designationMasterService.get(id);
    }

    @RequestMapping(value = "/get/search", method = RequestMethod.POST)
    @Secured("ROLE_MASTER.HRMS.DESIGNATION.VIEW")
    public BaseDto search(@RequestBody DesignationSearchDto searchDto) {
        log.info("search method called.");
        return designationMasterService.search(searchDto);
    }

    @RequestMapping(value = "/get/search/keyword", method = RequestMethod.POST)
    public BaseDto search(@RequestBody SearchRequest searchRequest) {
        log.info("search method called.");
        return designationMasterService.search(searchRequest);
    }

}
