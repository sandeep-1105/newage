package com.efreightsuite.controller;

import javax.servlet.http.HttpServletResponse;

import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.FetchQuotationChargeSearchDto;
import com.efreightsuite.dto.FileUploadDto;
import com.efreightsuite.dto.PricingAirSearchDto;
import com.efreightsuite.dto.PricingMasterListDto;
import com.efreightsuite.dto.SearchRequest;
import com.efreightsuite.model.PricingMaster;
import com.efreightsuite.model.PricingPortPair;
import com.efreightsuite.service.PricingAirService;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Log4j2
@RequestMapping(value = "/api/v1/pricing")
public class PricingAirController {

    @Autowired
    private
    PricingAirService pricingAirService;

    @RequestMapping(value = "/search", method = RequestMethod.POST)
    @Secured("ROLE_CRM.PRICING_AIR.LIST")
    public BaseDto search(@RequestBody PricingAirSearchDto searchDto) {
        log.info("PricingAirController -> search method called.");
        return pricingAirService.search(searchDto);
    }

    @RequestMapping(value = "/defaultchargesearch", method = RequestMethod.POST)
    @Secured("ROLE_CRM.DEFAULT_CHARGES.LIST")
    public BaseDto defaultChargeSearch(@RequestBody PricingAirSearchDto searchDto) {
        log.info("PricingAirController -> search method called.");
        return pricingAirService.defaultChargeSearch(searchDto);
    }

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    @Secured("ROLE_CRM.PRICING_AIR.CREATE")
    public BaseDto create(@RequestBody PricingMaster pricingMaster) {
        log.info("PricingAirController -> create method called.");
        return pricingAirService.create(pricingMaster);
    }


    @RequestMapping(value = "/createlist", method = RequestMethod.POST)
    @Secured("ROLE_CRM.PRICING_AIR.CREATE")
    public BaseDto create(@RequestBody PricingMasterListDto pricingMasterListDto) {
        log.info("PricingAirController -> create list method called.");
        return pricingAirService.createList(pricingMasterListDto);
    }


    @RequestMapping(value = "/update", method = RequestMethod.POST)
    @Secured("ROLE_CRM.PRICING_AIR.MODIFY")
    public BaseDto update(@RequestBody PricingMaster pricingMaster) {
        log.info("PricingAirController -> update method called.");
        return pricingAirService.update(pricingMaster);
    }


    @RequestMapping(value = "pricingportpair/update", method = RequestMethod.POST)
    public BaseDto update(@RequestBody PricingPortPair pricingPortPair) {
        log.info("PricingAirController -> update method called.");
        return pricingAirService.updatePricingPortPair(pricingPortPair);
    }


    @RequestMapping(value = "/get/id/{id}", method = RequestMethod.GET)
    @Secured("ROLE_CRM.PRICING_AIR.VIEW")
    public BaseDto getById(@PathVariable Long id) {
        log.info("PricingAirController -> get by id method called.");
        return pricingAirService.getById(id);
    }


    @RequestMapping(value = "pricingport/get/id/{id}", method = RequestMethod.GET)
    public BaseDto getPricingPort(@PathVariable Long id) {
        log.info("PricingAirController -> get by pricing port id method called.");
        return pricingAirService.getPricingPort(id);
    }

    @RequestMapping(value = "/files/{id}", method = RequestMethod.GET)
    public void getFile(@PathVariable Long id, HttpServletResponse response) {
        log.info("PricingAirController -> getFile method cacinglled.");
        pricingAirService.downloadAttachment(id, response);
    }

    @RequestMapping(value = "/getcharge/{enquiryId}/{originId}/{destinationId}", method = RequestMethod.GET)
    public BaseDto findByOriginAndDestination(@PathVariable("originId") Long originId,
                                              @PathVariable("destinationId") Long destinationId, @PathVariable("enquiryId") Long enquiryId) {
        return pricingAirService.findByOriginAndDestination(originId, destinationId, enquiryId);
    }

    @RequestMapping(value = "/upload", method = RequestMethod.POST)
    public BaseDto upload(@RequestBody FileUploadDto fileUploadDto) {
        log.info("Pricing Air upload method called.");
        return pricingAirService.upload(fileUploadDto);
    }

    @RequestMapping(value = "/download/template/{id}", method = RequestMethod.GET)
    @Secured("ROLE_CRM.PRICING_AIR.DOWNLOAD")
    public void upload(@PathVariable("id") Long id, HttpServletResponse response) {
        log.info("Pricing Air upload method called.");
        pricingAirService.download(id, response);
    }

    @RequestMapping(value = "/getquotationcharge", method = RequestMethod.POST)
    public BaseDto fetchQuotationCharges(@RequestBody FetchQuotationChargeSearchDto data) {
        log.info("Pricing Air upload method called.");
        return pricingAirService.fetchQuotationCharges(data);
    }

    @RequestMapping(value = "/prepareShipment", method = RequestMethod.POST)
    public BaseDto prepareShipment(@RequestBody SearchRequest data) {
        log.info("prepareShipment method called.");
        return pricingAirService.prepareShipment(data);
    }

}
