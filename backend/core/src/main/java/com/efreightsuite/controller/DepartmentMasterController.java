package com.efreightsuite.controller;

import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.DepartmentSearchDto;
import com.efreightsuite.dto.SearchRequest;
import com.efreightsuite.model.DepartmentMaster;
import com.efreightsuite.service.DepartmentMasterService;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/api/v1/departmentmaster")
@Log4j2
public class DepartmentMasterController {

    @Autowired
    private
    DepartmentMasterService departmentMasterService;

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    @Secured({"ROLE_MASTER.HRMS.DEPARTMENT.CREATE", "ROLE_MASTER.HRMS.DEPARTMENT.MODIFY"})
    public BaseDto create(@RequestBody DepartmentMaster departmentMaster) {
        log.info("Create method is Invoked..");
        return departmentMasterService.saveOrUpdate(departmentMaster);
    }

    @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
    @Secured("ROLE_MASTER.HRMS.DEPARTMENT.DELETE")
    public BaseDto delete(@PathVariable Long id) {
        log.info("Delete method is Invoked.. : [" + id + "]");
        return departmentMasterService.delete(id);
    }

    @RequestMapping(value = "/get/id/{id}")
    @Secured("ROLE_MASTER.HRMS.DEPARTMENT.VIEW")
    public BaseDto get(@PathVariable Long id) {
        log.info("get is called..[ " + id + " ]");
        return departmentMasterService.get(id);
    }

    @RequestMapping(value = "/get/search", method = RequestMethod.POST)
    @Secured("ROLE_MASTER.HRMS.DEPARTMENT.LIST")
    public BaseDto search(@RequestBody DepartmentSearchDto searchDto) {
        log.info("search method called.");
        return departmentMasterService.search(searchDto);
    }

    @RequestMapping(value = "/get/search/keyword", method = RequestMethod.POST)
    public BaseDto search(@RequestBody SearchRequest searchRequest) {
        log.info("search method called.");
        return departmentMasterService.search(searchRequest);
    }

}
