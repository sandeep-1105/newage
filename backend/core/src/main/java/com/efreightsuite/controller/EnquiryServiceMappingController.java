package com.efreightsuite.controller;

import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.EnquiryServiceMappingSearchDto;
import com.efreightsuite.model.EnquiryServiceMapping;
import com.efreightsuite.service.EnquiryServiceMappingService;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@Log4j2
@RestController
@RequestMapping(value = "/api/v1/enquiryservicemapping")
public class EnquiryServiceMappingController {

    @Autowired
    private
    EnquiryServiceMappingService enquiryServiceMappingService;

    @RequestMapping(value = "/get/search/keyword", method = RequestMethod.POST)
    @Secured("ROLE_SETUP.APP_CONFIGURATION.ENQUIRY_SERVICE_MAPPING.LIST")
    public BaseDto get(@RequestBody EnquiryServiceMappingSearchDto searchDto) {
        log.info("getall method called.");
        return enquiryServiceMappingService.search(searchDto);
    }

    @RequestMapping(value = "/get/id/{id}", method = RequestMethod.GET)
    public BaseDto get(@PathVariable Long id) {
        log.info("get method called." + id);
        return enquiryServiceMappingService.get(id);
    }

    @RequestMapping(value = "/save", method = RequestMethod.POST)
    @Secured("ROLE_SETUP.APP_CONFIGURATION.ENQUIRY_SERVICE_MAPPING.MODIFY")
    public BaseDto update(@RequestBody EnquiryServiceMapping enquiryServiceMapping) {
        log.info("Update method is called.");
        return enquiryServiceMappingService.saveOrupdate(enquiryServiceMapping);
    }

}
