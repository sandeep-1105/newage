package com.efreightsuite.controller;

import java.util.List;

import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.EssentialChargeSearchDto;
import com.efreightsuite.model.EssentialChargeMaster;
import com.efreightsuite.service.EssentialChargeMasterService;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/api/v1/essentialcharge")
@Log4j2
public class EssentialChargeMasterController {

    @Autowired
    private
    EssentialChargeMasterService essentialChargeMasterService;

    @RequestMapping(value = "/get/search", method = RequestMethod.POST)
    @Secured("ROLE_MASTER.GENERAL.ESSENTIAL_CHARGE.LIST")
    public BaseDto search(@RequestBody EssentialChargeSearchDto searchDto) {
        log.info("EssentialChargeController.Search method is called...");
        return essentialChargeMasterService.search(searchDto);
    }

    @RequestMapping(value = "/get/id/{id}", method = RequestMethod.GET)
    @Secured("ROLE_MASTER.GENERAL.ESSENTIAL_CHARGE.VIEW")
    public BaseDto get(@PathVariable Long id) {
        log.info("EssentialChargeController.get method is called...");
        return essentialChargeMasterService.get(id);
    }

    @RequestMapping(value = "/get/serviceId/{serviceId}", method = RequestMethod.GET)
    public BaseDto getChare(@PathVariable Long serviceId) {
        log.info("EssentialChargeController.get method is called...");
        return essentialChargeMasterService.getCharge(serviceId);
    }

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    @Secured("ROLE_MASTER.GENERAL.ESSENTIAL_CHARGE.CREATE")
    public BaseDto create(@RequestBody List<EssentialChargeMaster> essentialChargeMasterList) {
        log.info("EssentialChargeController.create method is called...");
        return essentialChargeMasterService.create(essentialChargeMasterList);
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    @Secured("ROLE_MASTER.GENERAL.ESSENTIAL_CHARGE.MODIFY")
    public BaseDto update(@RequestBody EssentialChargeMaster essentialChargeMaster) {
        log.info("EssentialChargeController.update method is called...");
        return essentialChargeMasterService.update(essentialChargeMaster);
    }

    @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
    @Secured("ROLE_MASTER.GENERAL.ESSENTIAL_CHARGE.DELETE")
    public BaseDto delete(@PathVariable Long id) {
        log.info("EssentialChargeController.delete method is called...");
        return essentialChargeMasterService.delete(id);
    }

    @RequestMapping(value = "/get/essentialcharges", method = RequestMethod.POST)
    public BaseDto essentialcharges(@RequestBody EssentialChargeSearchDto searchDto) {
        log.info("EssentialChargeController.Search method is called...");
        return essentialChargeMasterService.essentialcharges(searchDto);
    }
}
