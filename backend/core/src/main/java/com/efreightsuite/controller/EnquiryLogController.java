package com.efreightsuite.controller;

import java.util.List;

import javax.servlet.http.HttpServletResponse;

import com.efreightsuite.client.PushApiService;
import com.efreightsuite.controller.representations.EnquiryLogRequest;
import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.EnquiryDto;
import com.efreightsuite.dto.EnquiryLogSearchDto;
import com.efreightsuite.dto.RecentRecordsRequest;
import com.efreightsuite.dto.SearchRespDto;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.model.EnquiryLog;
import com.efreightsuite.service.EnquiryLogService;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

@RestController
@Log4j2
@RequestMapping(value = "/api/v1/enquirylog")
public class EnquiryLogController {

    @Autowired
    private
    EnquiryLogService enquiryLogService;

    @Autowired
    private
    PushApiService pushApiService;

    @PostMapping(path = "/get/search/{searchFlag}")
    @Secured("ROLE_SALE.ENQUIRY.LIST")
    public BaseDto search(
            @PathVariable String searchFlag,
            @RequestBody EnquiryLogSearchDto searchDto) {
        return enquiryLogService.search(searchDto, searchFlag);
    }

    @RequestMapping(value = "/get/search/recent", method = RequestMethod.POST)
    public SearchRespDto searchRecentRecords(@RequestBody RecentRecordsRequest request) {
        return enquiryLogService.searchRecentRecords(request);
    }

    @RequestMapping(value = "/get/id/{id}", method = RequestMethod.GET)
    @Secured("ROLE_SALE.ENQUIRY.VIEW")
    public BaseDto get(@PathVariable Long id) {
        return enquiryLogService.get(id);
    }

    @RequestMapping(value = "/get/enquiryNo/{enquiryNo}", method = RequestMethod.GET)
    public BaseDto getByEnquiryNo(@PathVariable String enquiryNo) {
        return enquiryLogService.getByEnquiryNo(enquiryNo);
    }

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    @Secured("ROLE_SALE.ENQUIRY.CREATE")
    public ResponseEntity<BaseDto> save(
            @RequestBody EnquiryLogRequest enquiryLog,
            UriComponentsBuilder uriBuilder) {

        BaseDto baseDto = enquiryLogService.create(enquiryLog);

        if (ErrorCode.SUCCESS.equalsIgnoreCase(baseDto.getResponseCode())) {
            EnquiryLog entity = (EnquiryLog) baseDto.getResponseObject();
            pushApiService.pushToEnquiry(entity.getId());
            return ResponseEntity
                    .created(uriBuilder.path("/api/v1/enquirylog/get/id/{id}")
                            .buildAndExpand(entity.getId()).toUri())
                    .body(baseDto);
        }

        return ResponseEntity.ok(baseDto);
    }

    @RequestMapping(value = "/{id}/update", method = RequestMethod.POST)
    @Secured("ROLE_SALE.ENQUIRY.MODIFY")
    public BaseDto update(@PathVariable("id") Long enquiryId, @RequestBody EnquiryLogRequest updateRequest) {
        BaseDto baseDto = enquiryLogService.update(enquiryId, updateRequest);

        if (ErrorCode.SUCCESS.equalsIgnoreCase(baseDto.getResponseCode())) {
            EnquiryLog entity = (EnquiryLog) baseDto.getResponseObject();
            pushApiService.pushToEnquiry(entity.getId());
        }

        return baseDto;
    }

    @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
    @Secured("ROLE_SALE.ENQUIRY.DELETE")
    public BaseDto delete(@PathVariable Long id) {
        return enquiryLogService.delete(id);
    }

    @RequestMapping(value = "/files/{id}", method = RequestMethod.GET)
    @Secured("ROLE_SALE.ENQUIRY.ATTACHMENT.DOWNLOAD")
    public void getFile(@PathVariable Long id, HttpServletResponse response) {
        enquiryLogService.downloadAttachment(id, response);
    }

    @RequestMapping(value = "/files/all", method = RequestMethod.POST)
    public BaseDto getAttachmentList(@RequestBody List<Long> list) {
        return enquiryLogService.getAttachmentList(list);
    }

    @RequestMapping(value = "/statuschange", method = RequestMethod.POST)
    public BaseDto statuschange(@RequestBody EnquiryDto dto) {
        BaseDto baseDto = enquiryLogService.statusChange(dto);
        if (ErrorCode.SUCCESS.equalsIgnoreCase(baseDto.getResponseCode())) {
            EnquiryLog entity = (EnquiryLog) baseDto.getResponseObject();
            pushApiService.pushToEnquiry(entity.getId());
        }
        return baseDto;
    }

    @RequestMapping(value = "/create/customer/{id}", method = RequestMethod.GET)
    public BaseDto createCustomer(@PathVariable Long id) {
        BaseDto baseDto = enquiryLogService.createCustomer(id);
        if (ErrorCode.SUCCESS.equalsIgnoreCase(baseDto.getResponseCode())) {
            EnquiryLog entity = (EnquiryLog) baseDto.getResponseObject();
            pushApiService.pushToEnquiry(entity.getId());
        }
        return baseDto;
    }

}
