package com.efreightsuite.controller;

import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.SearchRequest;
import com.efreightsuite.dto.UnitSearchDto;
import com.efreightsuite.model.UnitMaster;
import com.efreightsuite.service.UnitMasterService;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author tanveer
 */
@RestController
@Log4j2
@RequestMapping(value = "/api/v1/unitmaster")
public class UnitMasterController {

    /**
     * UnitMasterService Object
     */

    @Autowired
    private
    UnitMasterService unitMasterService;


    /**
     * Method - getByUnitId API to get the unit By Id
     *
     * @param id
     * @return BaseDto
     */

    @RequestMapping(value = "/get/id/{id}", method = RequestMethod.GET)
    @Secured("ROLE_MASTER.GENERAL.UNIT.VIEW")
    public BaseDto get(@PathVariable Long id) {
        log.info("get method called[" + id + "]");
        return unitMasterService.get(id);
    }


    @RequestMapping(value = "/get/code/{unitCode}", method = RequestMethod.GET)
    public BaseDto get(@PathVariable String unitCode) {
        log.info("get method called[" + unitCode + "]");
        return unitMasterService.getByUnitCode(unitCode);
    }


    /**
     * Method - Create API to create the unit
     *
     * @return BaseDto
     */
    @RequestMapping(value = "/create", method = RequestMethod.POST)
    @Secured("ROLE_MASTER.GENERAL.UNIT.CREATE")
    public BaseDto create(@RequestBody UnitMaster unitMaster) {
        log.info("Create method is called.");
        return unitMasterService.create(unitMaster);
    }

    /**
     * Method - Update API to update the existing unit master
     *
     * @return BaseDto
     */
    @RequestMapping(value = "/update", method = RequestMethod.POST)
    @Secured("ROLE_MASTER.GENERAL.UNIT.MODIFY")
    public BaseDto update(@RequestBody UnitMaster unitMaster) {
        log.info("Update method is called.");
        return unitMasterService.update(unitMaster);
    }

    /**
     * Method delete - To delete the unitMaster based on the id
     *
     * @param id
     * @return BaseDto
     */

    @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
    @Secured("ROLE_MASTER.GENERAL.UNIT.DELETE")
    public BaseDto delete(@PathVariable Long id) {
        log.info("Delete method called....");
        return unitMasterService.delete(id);
    }


    @RequestMapping(value = "/get/search", method = RequestMethod.POST)
    @Secured("ROLE_MASTER.GENERAL.UNIT.LIST")
    public BaseDto search(@RequestBody UnitSearchDto searchDto) {
        log.info("Search method called.");
        return unitMasterService.search(searchDto);
    }

    @RequestMapping(value = "/get/search/keyword", method = RequestMethod.POST)
    public BaseDto search(@RequestBody SearchRequest searchRequest) {
        log.info("Search method called.");
        return unitMasterService.search(searchRequest);
    }


}
