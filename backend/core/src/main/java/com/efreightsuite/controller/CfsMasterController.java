package com.efreightsuite.controller;

import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.CfsSearchDto;
import com.efreightsuite.dto.SearchRequest;
import com.efreightsuite.model.CFSMaster;
import com.efreightsuite.service.CfsMasterService;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Log4j2
@RequestMapping(value = "/api/v1/cfsmaster")

public class CfsMasterController {

    @Autowired
    private
    CfsMasterService cfsMasterService;

    @RequestMapping(value = "/get/search", method = RequestMethod.POST)
    @Secured("ROLE_MASTER.OCEAN.CFS.LIST")
    public BaseDto search(@RequestBody CfsSearchDto searchDto) {
        log.info("Search method called.");
        return cfsMasterService.search(searchDto);
    }

    @RequestMapping(value = "/get/search/keyword", method = RequestMethod.POST)
    public BaseDto search(@RequestBody SearchRequest searchRequest) {
        log.info("Search method called.");
        return cfsMasterService.search(searchRequest, null);
    }

    @RequestMapping(value = "/get/search/keyword/{portId}", method = RequestMethod.POST)
    public BaseDto search(@RequestBody SearchRequest searchRequest, @PathVariable Long portId) {
        log.info("Search method called.");
        return cfsMasterService.search(searchRequest, portId);
    }

    @RequestMapping(value = "/get/id/{id}", method = RequestMethod.GET)
    @Secured("ROLE_MASTER.OCEAN.CFS.VIEW")
    public BaseDto getByTosId(@PathVariable Long id) {
        return cfsMasterService.get(id);
    }

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    @Secured("ROLE_MASTER.OCEAN.CFS.CREATE")
    public BaseDto create(@RequestBody CFSMaster cfsMaster) {
        log.info("Create method is called..");
        return cfsMasterService.create(cfsMaster);
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    @Secured("ROLE_MASTER.OCEAN.CFS.MODIFY")
    public BaseDto update(@RequestBody CFSMaster cfsMaster) {
        return cfsMasterService.update(cfsMaster);
    }

    @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
    @Secured("ROLE_MASTER.OCEAN.CFS.DELETE")
    public BaseDto delete(@PathVariable Long id) {
        log.info("Delete method called....[" + id + "]");
        return cfsMasterService.delete(id);
    }


}
