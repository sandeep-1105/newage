package com.efreightsuite.controller;

import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.DefaultMasterSearchDto;
import com.efreightsuite.dto.SearchRequest;
import com.efreightsuite.model.DefaultMasterData;
import com.efreightsuite.service.DefaultMasterDataService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/api/v1/defaultmaster")
public class DefaultMasterDataController {

    @Autowired
    private
    DefaultMasterDataService defaultMasterDataService;

    @RequestMapping(value = "/get/id/{id}", method = RequestMethod.GET)
    @Secured("ROLE_MASTER.GENERAL.DEFAULT.VIEW")
    public BaseDto getById(@PathVariable Long id) {
        return defaultMasterDataService.getById(id);
    }

    @RequestMapping(value = "/get/search", method = RequestMethod.POST)
    public BaseDto search(@RequestBody SearchRequest searchRequest) {
        return defaultMasterDataService.search(searchRequest);
    }

    @RequestMapping(value = "/get/search/keyword", method = RequestMethod.POST)
    @Secured("ROLE_MASTER.GENERAL.DEFAULT.LIST")
    public BaseDto search(@RequestBody DefaultMasterSearchDto searchDto) {
        return defaultMasterDataService.search(searchDto);
    }

    @RequestMapping(value = "/save", method = RequestMethod.POST)
    @Secured({"ROLE_MASTER.GENERAL.DEFAULT.CREATE", "ROLE_MASTER.GENERAL.DEFAULT.MODIFY"})
    public BaseDto saveOrUpdate(@RequestBody DefaultMasterData defaultMasterData) {
        return defaultMasterDataService.saveOrUpdate(defaultMasterData);
    }

    @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
    @Secured("ROLE_MASTER.GENERAL.DEFAULT.DELETE")
    public BaseDto delete(@PathVariable Long id) {
        return defaultMasterDataService.delete(id);
    }
}
