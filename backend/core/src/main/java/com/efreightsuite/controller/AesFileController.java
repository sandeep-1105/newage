package com.efreightsuite.controller;

import java.util.ArrayList;

import com.efreightsuite.dto.AesFileSearchDto;
import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.SearchRequest;
import com.efreightsuite.dto.ShipmentSearchDto;
import com.efreightsuite.model.AesFile;
import com.efreightsuite.service.AesFileEdiService;
import com.efreightsuite.service.AesFileService;
import com.efreightsuite.service.AesMasterService;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Log4j2
@RequestMapping(value = "/api/v1/aesfile")
public class AesFileController {

    @Autowired
    private
    AesFileService aesFileService;

    @Autowired
    private
    AesFileEdiService aesFileEdiService;

    @Autowired
    private
    AesMasterService aesMasterService;


    @RequestMapping(value = "/get/id/{id}", method = RequestMethod.GET)
    public BaseDto getById(@PathVariable Long id) {
        log.info("AesFile Controller -> get by id method called.");
        return aesFileService.getById(id);
    }


    @RequestMapping(value = "/get/shipmentUid/{shipmentUid}", method = RequestMethod.GET)
    public BaseDto getByShipmentUid(@PathVariable String shipmentUid) {
        log.info("AesFile Controller getByShipmentUid method called.");
        return aesFileService.getByShipmentUid(shipmentUid);
    }


    @RequestMapping(value = "/get/serviceuid/{serviceuid}", method = RequestMethod.GET)
    public BaseDto getByServiceUid(@PathVariable String serviceuid) {
        log.info("AesFile Controller getByServiceUid by id method called.");
        return aesFileService.getByServiceUid(serviceuid);
    }

    @RequestMapping(value = "/get/masteruid/{masteruid}", method = RequestMethod.GET)
    public BaseDto getByMasterUid(@PathVariable String masteruid) {
        log.info("AesFile Controller getByMasterUid method called.");
        return aesFileService.getByMasterUid(masteruid);
    }

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public BaseDto create(@RequestBody AesFile aesFile) {
        log.info("AesFile Controller Create method called.");
        return aesFileService.create(aesFile);
    }

    @RequestMapping(value = "/createall", method = RequestMethod.POST)
    public BaseDto createAll(@RequestBody ArrayList<AesFile> aesFiles) {
        log.info("AesFile Controller createAll method called.");
        return aesFileService.createAll(aesFiles);
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public BaseDto update(@RequestBody AesFile aesFile) {
        log.info("AesFile Controller update method called.");
        return aesFileService.update(aesFile);
    }

    @RequestMapping(value = "/getallaeslist/masteruid/{masteruid}", method = RequestMethod.GET)
    public BaseDto getAesFileListByMasterUid(@PathVariable String masteruid) {
        log.info("AesFile Controller Search method called.");
        return aesFileService.getAesFileListByMasterUid(masteruid);
    }

    @RequestMapping(value = "/search", method = RequestMethod.POST)
    @Secured("ROLE_CRM.AUTOMATED_EXPORT_SYSTEM.LIST")
    public BaseDto search(@RequestBody AesFileSearchDto aesDto) {
        log.info("AesFile Controller Search method called.");
        return aesFileService.search(aesDto);
    }

    @RequestMapping(value = "/aespendingsearch", method = RequestMethod.POST)
    @Secured("ROLE_CRM.AUTOMATED_EXPORT_SYSTEM.LIST")
    public BaseDto search(@RequestBody ShipmentSearchDto serviceDto) {
        log.info("AesFile Controller Search method called.");
        return aesFileService.pendingSearch(serviceDto);
    }


    @RequestMapping(value = "/edi/save/{id}", method = RequestMethod.GET)
    public BaseDto saveEdi(@PathVariable("id") Long aesFileId) {
        log.info("AesFileController -> save Edi method called." + aesFileId);
        return aesFileEdiService.save(aesFileId);
    }


    @RequestMapping(value = "/portmaster/search", method = RequestMethod.POST)
    public BaseDto searchUsPortmaster(@RequestBody SearchRequest search) {
        log.info("AesFileController -> searchUsPortmaster method called." + search.getKeyword());
        return aesMasterService.getPortMaster(search.getKeyword());
    }

    @RequestMapping(value = "/zipcodemaster/search", method = RequestMethod.POST)
    public BaseDto searchUsZipCodeMaster(@RequestBody SearchRequest search) {
        log.info("AesFileController -> searchUsPortmaster method called." + search.getKeyword());
        return aesMasterService.getUsZipCodeMaster(search.getKeyword());
    }

    @RequestMapping(value = "/statecodemaster/search", method = RequestMethod.POST)
    public BaseDto searchUsStateCodeMaster(@RequestBody SearchRequest search) {
        log.info("AesFileController -> searchUsPortmaster method called." + search.getKeyword());
        return aesMasterService.getUsStateCodeMaster(search.getKeyword());
    }
    /*
	@RequestMapping(value = "/portmaster/save", method = RequestMethod.POST)
	public void searchUsPortmaster(@RequestBody List<AesUsStateCodeMaster> entityList) {
		log.info("AesFileController -> searchUsPortmaster method called.");
		aesMasterService.savePortMaster(entityList);
	}
	
	

	
	@RequestMapping(value = "/zip/save", method = RequestMethod.POST)
	public void zip(@RequestBody List<AesUsZipCodeMaster> entityList) {
		log.info("AesFileController -> searchUsPortmaster method called.");
		aesMasterService.saveZip(entityList);
	}
	*/


    @RequestMapping(value = "/get/status/history/{id}", method = RequestMethod.GET)
    public BaseDto getStatusHistory(@PathVariable Long id) {
        log.info("AesFile Controller -> get Status History method called.");
        return aesFileService.getStatusHistory(id);
    }

    @RequestMapping(value = "/foreign/portmaster/search", method = RequestMethod.POST)
    public BaseDto searchUsForeignPortmaster(@RequestBody SearchRequest search) {
        log.info("AesFileController -> searchUsForeignPortmaster method called." + search.getKeyword());
        return aesMasterService.getForeignPortMaster(search.getKeyword());
    }


}
