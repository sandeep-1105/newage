package com.efreightsuite.controller;

import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.SearchRequest;
import com.efreightsuite.dto.TosSearchDto;
import com.efreightsuite.model.TosMaster;
import com.efreightsuite.service.TosMasterService;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author tanveer
 */

@RestController
@Log4j2
@RequestMapping(value = "/api/v1/tosmaster")

public class TosMasterController {

    @Autowired
    private
    TosMasterService tosMasterService;

    @RequestMapping(value = "/get/search", method = RequestMethod.POST)
    @Secured("ROLE_MASTER.GENERAL.TOS.LIST")
    public BaseDto search(@RequestBody TosSearchDto searchDto) {
        log.info("Search method called.");
        return tosMasterService.search(searchDto);
    }

    @RequestMapping(value = "/get/search/keyword", method = RequestMethod.POST)
    public BaseDto search(@RequestBody SearchRequest searchRequest) {
        log.info("Search method called.");
        return tosMasterService.search(searchRequest);
    }

    @RequestMapping(value = "/get/id/{id}", method = RequestMethod.GET)
    @Secured("ROLE_MASTER.GENERAL.TOS.VIEW")
    public BaseDto getByTosId(@PathVariable Long id) {
        log.info("getByTosId method called..[" + id + "]");
        return tosMasterService.get(id);
    }

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    @Secured("ROLE_MASTER.GENERAL.TOS.CREATE")
    public BaseDto create(@RequestBody TosMaster tosMaster) {
        log.info("Create method is called..");
        return tosMasterService.create(tosMaster);
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    @Secured("ROLE_MASTER.GENERAL.TOS.MODIFY")
    public BaseDto update(@RequestBody TosMaster tosMaster) {
        log.info("Update method is called..[" + tosMaster.getId() + "]");
        return tosMasterService.update(tosMaster);
    }

    @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
    @Secured("ROLE_MASTER.GENERAL.TOS.DELETE")
    public BaseDto delete(@PathVariable Long id) {
        log.info("Delete method called....[" + id + "]");
        return tosMasterService.delete(id);
    }

    @RequestMapping(value = "/getall", method = RequestMethod.GET)
    public BaseDto get() {
        log.info("getall method called.");
        return tosMasterService.getAll();
    }

}
