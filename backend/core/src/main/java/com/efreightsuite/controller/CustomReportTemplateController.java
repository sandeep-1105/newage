package com.efreightsuite.controller;

import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.model.CustomReportTemplate;
import com.efreightsuite.service.CustomReportTemplateService;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/api/v1/custom_report_template")
@Log4j2
public class CustomReportTemplateController {

    @Autowired
    private
    CustomReportTemplateService customReportTemplateService;

    @RequestMapping(value = "/get/id/{id}", method = RequestMethod.GET)
    public BaseDto getById(@PathVariable Long id) {
        log.info("getById method is called...ID = " + id);
        return customReportTemplateService.getById(id);
    }

    @RequestMapping(value = "/get/all", method = RequestMethod.GET)
    public BaseDto getAll(@PathVariable Long id) {
        log.info("getAll method is called...ID = " + id);
        return customReportTemplateService.getAll();
    }

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public BaseDto create(@RequestBody CustomReportTemplate customReportTemplate) {
        log.info("Create method is called.. : " + customReportTemplate);
        return customReportTemplateService.createOrUpdate(customReportTemplate);
    }

    @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
    public BaseDto delete(@PathVariable Long id) {
        log.info("Delete method is called.. : [" + id + "]");
        return customReportTemplateService.delete(id);
    }
}
