package com.efreightsuite.controller;

import java.util.List;

import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.DynamicFieldSearchDto;
import com.efreightsuite.model.DynamicFields;
import com.efreightsuite.service.DynamicFieldService;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/api/v1/dynamicfield")
@Log4j2
public class DynamicFieldController {

    /**
     * dynamicFieldService Object
     */

    @Autowired
    private
    DynamicFieldService dynamicFieldService;


    /**
     * Method - create or update DynamicFields and returns a new DynamicFields
     *
     * @returns DynamicFields
     */
    @RequestMapping(method = RequestMethod.POST)
    @Secured({"ROLE_SETUP.COUNTRY_CONFIGURATION.CREATE", "ROLE_SETUP.COUNTRY_CONFIGURATION.MODIFY"})
    public BaseDto modifyDynamicFields(@RequestBody DynamicFields entity) {
        log.info("Create or Update method is called.. DynamicFields : ");
        return dynamicFieldService.modifyDynamicFields(entity);
    }


    /**
     * Method - search By Id and returns a requested resource DynamicFields
     *
     * @returns DynamicFields
     */
    @RequestMapping(value = "/search", method = RequestMethod.POST)
    @Secured("ROLE_SETUP.COUNTRY_CONFIGURATION.LIST")
    public BaseDto searchByCriteria(@RequestBody DynamicFieldSearchDto searchData) {
        log.info("Search By Id is called.. PageDynamicFields : ");
        return dynamicFieldService.searchByCriteria(searchData);
    }

    /**
     * Method - search By Id and returns a requested resource DynamicFields
     *
     * @returns DynamicFields
     */
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @Secured("ROLE_SETUP.COUNTRY_CONFIGURATION.VIEW")
    public BaseDto searchById(@PathVariable("id") Long id) {
        log.info("Search By Id is called.. DynamicFields : " + id);
        return dynamicFieldService.searchById(id);
    }


    /*
    @RequestMapping(value="/country", method = RequestMethod.POST)
	public BaseDto modifyCountryDynamicFields(@RequestBody CountryDynamicField entity) {
		log.info("Create or Update method is called.. DynamicFields : ");
		return dynamicFieldService.modifyCountryDynamicFields(entity);
	}
	

	 */

    /**
     * Method - Get FieldName and show status
     *
     * @param countryId
     * @returns BaseDto
     */
    @RequestMapping(value = "/country/{countryId}", method = RequestMethod.GET)
    public BaseDto getFeildsByCountry(@PathVariable("countryId") Long countryId) {
        log.info("Create or Update method is called.. DynamicFields : ");
        return dynamicFieldService.getFeildsByCountry(countryId);
    }

    /*For Development Team - Lazy Upload All data*/
    @RequestMapping(value = "/uploadlist", method = RequestMethod.POST)
    public BaseDto modifyDynamicFieldList(@RequestBody List<DynamicFields> entityList) {
        log.info("Create or Update method is called.. DynamicFields : ");
        return dynamicFieldService.modifyDynamicFieldList(entityList);
    }

}
