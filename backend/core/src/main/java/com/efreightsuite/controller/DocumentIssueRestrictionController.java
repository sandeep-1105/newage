package com.efreightsuite.controller;

import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.DocumentIssueRestrictionSearchDto;
import com.efreightsuite.model.DocumentIssueRestriction;
import com.efreightsuite.service.DocumentIssueRestrictionService;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/api/v1/documentissuerestriction")
@Log4j2
public class DocumentIssueRestrictionController {

    /**
     * documentIssueRestrictionService Object
     */

    @Autowired
    private
    DocumentIssueRestrictionService documentIssueRestrictionService;


    /**
     * Method - create or update DocumentIssueRestriction and returns a new DocumentIssueRestriction
     *
     * @returns DocumentIssueRestriction
     */
    @RequestMapping(method = RequestMethod.POST)
    @Secured({"ROLE_SETUP.DOCUMENT_ISSUE_RESTRICTION.CREATE", "ROLE_SETUP.DOCUMENT_ISSUE_RESTRICTION.MODIFY"})
    public BaseDto modifyDocumentIssueRestriction(@RequestBody DocumentIssueRestriction entity) {
        log.info("Create or Update method is called.. DocumentIssueRestriction : ");
        return documentIssueRestrictionService.modifyDocumentIssueRestriction(entity);
    }


    /**
     * Method - search By Id and returns a requested resource DocumentIssueRestriction
     *
     * @returns DocumentIssueRestriction
     */
    @RequestMapping(value = "/search", method = RequestMethod.POST)
    @Secured("ROLE_SETUP.DOCUMENT_ISSUE_RESTRICTION.LIST")
    public BaseDto searchByCriteria(@RequestBody DocumentIssueRestrictionSearchDto searchData) {
        log.info("Search By Id is called.. PageDocumentIssueRestriction : ");
        return documentIssueRestrictionService.searchByCriteria(searchData);
    }

    /**
     * Method - search By Id and returns a requested resource DocumentIssueRestriction
     *
     * @returns DocumentIssueRestriction
     */
    @RequestMapping(value = "/search/{id}", method = RequestMethod.GET)
    public BaseDto searchById(@PathVariable("id") Long id) {
        log.info("Search By Id is called.. DocumentIssueRestriction : " + id);
        return documentIssueRestrictionService.searchById(id);
    }


}
