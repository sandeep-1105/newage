package com.efreightsuite.controller;

import com.efreightsuite.dto.BankSearchDto;
import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.SearchRequest;
import com.efreightsuite.model.BankMaster;
import com.efreightsuite.service.BankMasterService;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/api/v1/bankmaster")
@Log4j2
public class BankMasterController {

    @Autowired
    private
    BankMasterService bankMasterService;

    /**
     * This method is used to get bank details using ID.
     *
     * @param id This is the parameter to get details
     * @return bank object .
     */
    @RequestMapping(value = "/get/id/{id}", method = RequestMethod.GET)
    @Secured("ROLE_MASTER.FINANCE.BANK.VIEW")
    public BaseDto getByPackId(@PathVariable Long id) {
        log.info("bank controller get by id called" + id);
        return bankMasterService.getBankById(id);
    }

    /**
     * This method is used to get entire bank details for lov.
     *
     * @return bank object .
     */
    @RequestMapping(value = "/get/search", method = RequestMethod.POST)
    @Secured("ROLE_MASTER.FINANCE.BANK.LIST")
    public BaseDto search(@RequestBody BankSearchDto searchDto) {
        log.info("Search method is called...Search Dto: " + searchDto);
        return bankMasterService.search(searchDto);
    }

    /**
     * This method is used to get entire bank details for lov.
     *
     * @return bank object .
     */
    @RequestMapping(value = "/get/search/keyword", method = RequestMethod.POST)
    public BaseDto search(@RequestBody SearchRequest searchRequest) {
        log.info("Search method is called...Search Dto: " + searchRequest);
        return bankMasterService.search(searchRequest);
    }

    /**
     * This method is used to create bank details .
     *
     * @return bank object .
     */
    @RequestMapping(value = "/create", method = RequestMethod.POST)
    @Secured("ROLE_MASTER.FINANCE.BANK.CREATE")
    public BaseDto create(@RequestBody BankMaster bankMaster) {
        log.info("Create method is called-BankMaster");
        return bankMasterService.create(bankMaster);
    }

    /**
     * This method is used to update bank details .
     *
     * @return bank object .
     */
    @RequestMapping(value = "/update", method = RequestMethod.POST)
    @Secured("ROLE_MASTER.FINANCE.BANK.MODIFY")
    public BaseDto update(@RequestBody BankMaster existingbankMaster) {

        log.info("Update method is called : " + existingbankMaster);

        return bankMasterService.update(existingbankMaster);
    }

    /**
     * This method is used to delete bank details .
     *
     * @return .
     */

    @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
    @Secured("ROLE_MASTER.FINANCE.BANK.DELETE")
    public BaseDto delete(@PathVariable Long id) {

        log.info("Delete method is called.. : [" + id + "]");

        return bankMasterService.delete(id);
    }


}
