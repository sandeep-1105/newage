package com.efreightsuite.controller;

import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.RoleDTO;
import com.efreightsuite.dto.RoleFormDto;
import com.efreightsuite.dto.RoleSearchDto;
import com.efreightsuite.dto.SearchRequest;
import com.efreightsuite.service.RoleService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/role")
public class RoleController {

    @Autowired
    private RoleService roleService;


    @RequestMapping(value = "/get/feature/all", method = RequestMethod.GET)
    public BaseDto getAllFeatures() {

        return roleService.getAllFeatures(null);
    }

    @RequestMapping(value = "/add/{id}", method = RequestMethod.GET)
    public BaseDto addRole(@PathVariable Long id) {

        return roleService.getAllFeatures(id);
    }

    @RequestMapping(value = "/roleform", method = RequestMethod.POST)
    public BaseDto roleForm(@RequestBody RoleFormDto data) {

        return roleService.roleForm(data);

    }

    @RequestMapping(value = "/get/id/{id}", method = RequestMethod.GET)
    @Secured("ROLE_SETUP.ROLES_AND_PRIVILEGES.VIEW")
    public BaseDto getAllFeatures(@PathVariable Long id) {

        return roleService.getAllFeatures(id);
    }

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    @Secured("ROLE_SETUP.ROLES_AND_PRIVILEGES.CREATE")
    public BaseDto create(@RequestBody RoleDTO data) {

        return roleService.saveOrUpdate(data);

    }

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    @Secured("ROLE_SETUP.ROLES_AND_PRIVILEGES.MODIFY")
    public BaseDto update(@RequestBody RoleDTO data) {
        return roleService.saveOrUpdate(data);
    }

    @RequestMapping(value = "/get/search/keyword", method = RequestMethod.POST)
    public BaseDto searchKeyword(@RequestBody SearchRequest searchRequest) {

        return roleService.search(searchRequest);
    }

    @RequestMapping(value = "/get/search/all", method = RequestMethod.GET)
    public BaseDto getAll() {

        return roleService.search(null);
    }

    @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
    @Secured("ROLE_SETUP.ROLES_AND_PRIVILEGES.DELETE")
    public BaseDto getRoleWithFeature(@PathVariable Long id) {

        return roleService.delete(id);
    }

    @RequestMapping(value = "/get/search", method = RequestMethod.POST)
    @Secured("ROLE_SETUP.ROLES_AND_PRIVILEGES.LIST")
    public BaseDto search(@RequestBody RoleSearchDto searchDto) {

        return roleService.roleSearch(searchDto);
    }

}
