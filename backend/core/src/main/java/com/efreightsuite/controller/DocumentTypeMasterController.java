package com.efreightsuite.controller;

import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.DocumentTypeSearchDto;
import com.efreightsuite.dto.SearchRequest;
import com.efreightsuite.enumeration.DocumentType;
import com.efreightsuite.model.DocumentTypeMaster;
import com.efreightsuite.service.DocumentTypeMasterService;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/api/v1/documenttypemaster")
@Log4j2
public class DocumentTypeMasterController {

    /**
     * DocumentTypeMasterService Object
     */

    @Autowired
    private
    DocumentTypeMasterService documentTypeMasterService;

    /**
     * Method - getById Returns DocumentTypeMaster based on given document type id
     *
     * @return DocumentTypeMaster
     */

    @RequestMapping(value = "/get/id/{id}", method = RequestMethod.GET)
    @Secured("ROLE_MASTER.FINANCE.DOCUMENT_TYPE.VIEW")
    public BaseDto getByDocumentTypeId(@PathVariable Long id) {

        log.info("getByDocumentTypeId method is called...ID = " + id);

        return documentTypeMasterService.getByDocumentTypeId(id);
    }

    /**
     * Method - getByDocumentTypeCode Returns DocumentTypeMaster based on given document type code
     *
     * @return DocumentTypeMaster
     */

    @RequestMapping(value = "/get/code/{documentTypeCode}", method = RequestMethod.GET)
    public BaseDto getByDocumentTypeCode(@PathVariable DocumentType documentTypeCode) {

        log.info("getByDocumentTypeCode method is called...CODE = " + documentTypeCode);

        return documentTypeMasterService.getByDocumentTypeCode(documentTypeCode);
    }

    /**
     * Method - search Returns Paginated Search Result based on Search Keyword
     *
     * @return SearchRespDto
     */

    @RequestMapping(value = "/get/search", method = RequestMethod.POST)
    @Secured("ROLE_MASTER.FINANCE.DOCUMENT_TYPE.LIST")
    public BaseDto search(@RequestBody DocumentTypeSearchDto searchDto) {

        log.info("Search method is called...Search Dto: " + searchDto);

        return documentTypeMasterService.search(searchDto);
    }

    @RequestMapping(value = "/get/search/keyword", method = RequestMethod.POST)
    public BaseDto search(@RequestBody SearchRequest searchRequest) {

        log.info("Search method is called...Search Dto: " + searchRequest);

        return documentTypeMasterService.search(searchRequest);
    }

    /**
     * Method - create Creates and returns a new document type master
     *
     * @returns DocumentTypeMaster
     */

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    @Secured("ROLE_MASTER.FINANCE.DOCUMENT_TYPE.CREATE")
    public BaseDto create(@RequestBody DocumentTypeMaster documentTypeMaster) {

        log.info("Create method is called.. DocumentTypeMaster : " + documentTypeMaster);

        return documentTypeMasterService.create(documentTypeMaster);
    }

    /**
     * Method - update returns a updated document type master
     *
     * @param existingDocumentTypeMaster
     * @returns DocumentTypeMaster
     */

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    @Secured("ROLE_MASTER.FINANCE.DOCUMENT_TYPE.MODIFY")
    public BaseDto update(@RequestBody DocumentTypeMaster existingDocumentTypeMaster) {

        log.info("Create method is called.. DocumentTypeMaster : " + existingDocumentTypeMaster);

        return documentTypeMasterService.update(existingDocumentTypeMaster);
    }

    @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
    @Secured("ROLE_MASTER.FINANCE.DOCUMENT_TYPE.DELETE")
    public BaseDto delete(@PathVariable Long id) {

        log.info("Delete method is called.. : [" + id + "]");

        return documentTypeMasterService.delete(id);
    }
}
