package com.efreightsuite.controller;

import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.SearchRequest;
import com.efreightsuite.dto.ZoneMasterSearchDto;
import com.efreightsuite.model.ZoneMaster;
import com.efreightsuite.service.ZoneMasterService;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


@RestController
@Log4j2
@RequestMapping(value = "/api/v1/zonemaster")
public class ZoneMasterController {


    @Autowired
    private
    ZoneMasterService zoneMasterService;

    /**
     * To get the lov
     *
     * @return BaseDto
     */

    @RequestMapping(value = "/get/search", method = RequestMethod.POST)
    public BaseDto get(@RequestBody SearchRequest searchRequest) {
        log.info("getall method called.");
        return zoneMasterService.search(searchRequest);
    }

    /**
     * To get the Zone all
     *
     * @return BaseDto
     */

    @RequestMapping(value = "/get/search/keyword", method = RequestMethod.POST)
    @Secured("ROLE_MASTER.GENERAL.ZONE.LIST")
    public BaseDto get(@RequestBody ZoneMasterSearchDto searchDto) {
        log.info("getall method called.");
        return zoneMasterService.search(searchDto);
    }

    /**
     * Method - getAll
     * API to get the state By Id
     *
     * @return BaseDto
     */

    @RequestMapping(value = "/get/getall", method = RequestMethod.GET)
    public BaseDto getAll() {
        log.info("getall method called.");
        return zoneMasterService.getAll();
    }


    /**
     * Method - getByZoneId
     * API to get the zone By Id
     *
     * @param id
     * @return BaseDto
     */

    @RequestMapping(value = "/get/id/{id}", method = RequestMethod.GET)
    @Secured("ROLE_MASTER.GENERAL.ZONE.VIEW")
    public BaseDto get(@PathVariable Long id) {
        log.info("get method called." + id);
        return zoneMasterService.get(id);
    }


    /**
     * Method - Create
     * API to create the zone
     *
     * @return BaseDto
     */
    @RequestMapping(value = "/create", method = RequestMethod.POST)
    @Secured("ROLE_MASTER.GENERAL.ZONE.CREATE")
    public BaseDto create(@RequestBody ZoneMaster zoneMaster) {
        log.info("Create method is called.");
        return zoneMasterService.create(zoneMaster);
    }


    /**
     * Method - Update
     * API to update the existing zone master
     *
     * @param zoneMaster
     * @return BaseDto
     */
    @RequestMapping(value = "/update", method = RequestMethod.POST)
    @Secured("ROLE_MASTER.GENERAL.ZONE.MODIFY")
    public BaseDto update(@RequestBody ZoneMaster zoneMaster) {
        log.info("Update method is called.");
        return zoneMasterService.update(zoneMaster);
    }

    /**
     * To Delete the zoneMaster Object Based on ID
     *
     * @param id
     * @return BaseDto
     */
    @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
    @Secured("ROLE_MASTER.GENERAL.ZONE.DELETE")
    public BaseDto delete(@PathVariable Long id) {
        log.info("Delete method called....");
        return zoneMasterService.delete(id);
    }
}
