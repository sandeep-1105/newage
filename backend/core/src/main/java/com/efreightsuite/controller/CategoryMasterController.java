package com.efreightsuite.controller;

import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.CategorySearchDto;
import com.efreightsuite.dto.SearchRequest;
import com.efreightsuite.model.CategoryMaster;
import com.efreightsuite.service.CategoryMasterService;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Log4j2
@RequestMapping(value = "/api/v1/categorymaster")
public class CategoryMasterController {

    @Autowired
    private
    CategoryMasterService categoryMasterService;

    @RequestMapping(value = "/get/id/{id}", method = RequestMethod.GET)
    @Secured("ROLE_MASTER.FINANCE.VERTICAL.VIEW")
    public BaseDto getByCategoryId(@PathVariable Long id) {
        log.info("getByCategoryId method called. [ " + id + "]");
        return categoryMasterService.getByCategoryId(id);
    }

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    @Secured("ROLE_MASTER.FINANCE.VERTICAL.CREATE")
    public BaseDto create(@RequestBody CategoryMaster catgeoryMaster) {
        log.info("Create method called.");
        return categoryMasterService.create(catgeoryMaster);
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    @Secured("ROLE_MASTER.FINANCE.VERTICAL.MODIFY")
    public BaseDto update(@RequestBody CategoryMaster categoryMaster) {
        log.info("Update method called.[" + categoryMaster.getId() + "]");
        return categoryMasterService.update(categoryMaster);
    }

    @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
    @Secured("ROLE_MASTER.FINANCE.VERTICAL.DELETE")
    public BaseDto delete(@PathVariable Long id) {
        log.info("Delete method called.[" + id + "]");
        return categoryMasterService.delete(id);
    }

    @RequestMapping(value = "/get/search", method = RequestMethod.POST)
    @Secured("ROLE_MASTER.FINANCE.VERTICAL.LIST")
    public BaseDto search(@RequestBody CategorySearchDto searchDto) {
        log.info("Search method called.");
        return categoryMasterService.search(searchDto);
    }

    @RequestMapping(value = "/get/search/keyword", method = RequestMethod.POST)
    public BaseDto search(@RequestBody SearchRequest searchRequest) {
        log.info("Search method called.");
        return categoryMasterService.search(searchRequest);
    }
}
