package com.efreightsuite.controller;

import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.PortGroupSearchDto;
import com.efreightsuite.dto.SearchRequest;
import com.efreightsuite.model.PortGroupMaster;
import com.efreightsuite.service.PortGroupMasterService;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Log4j2
@RequestMapping(value = "/api/v1/portgroupmaster")
public class PortGroupMasterController {

    @Autowired
    private
    PortGroupMasterService portGroupMasterService;

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    @Secured({"ROLE_MASTER.GENERAL.PORT_GROUP.CREATE", "ROLE_MASTER.GENERAL.PORT_GROUP.MODIFY"})
    public BaseDto save(@RequestBody PortGroupMaster portGroupMaster) {
        log.info("PortGroupMasterController -> create method called.");
        return portGroupMasterService.saveOrUpdate(portGroupMaster);
    }

    @RequestMapping(value = "/get/id/{id}", method = RequestMethod.GET)
    @Secured("ROLE_MASTER.GENERAL.PORT_GROUP.VIEW")
    public BaseDto get(@PathVariable Long id) {
        log.info("PortGroupMasterController -> get method called. [ " + id + " ]");
        return portGroupMasterService.get(id);
    }

    @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
    @Secured("ROLE_MASTER.GENERAL.PORT_GROUP.DELETE")
    public BaseDto delete(@PathVariable Long id) {
        log.info("PortGroupMasterController -> delete method called...[" + id + "]");
        return portGroupMasterService.delete(id);
    }

    @RequestMapping(value = "/get/search/keyword", method = RequestMethod.POST)
    public BaseDto search(@RequestBody SearchRequest searchRequest) {
        log.info("PortGroupMasterController -> search method called.");
        return portGroupMasterService.search(searchRequest);
    }

    @RequestMapping(value = "/get/search", method = RequestMethod.POST)
    @Secured("ROLE_MASTER.GENERAL.PORT_GROUP.LIST")
    public BaseDto search(@RequestBody PortGroupSearchDto portGroupSearchDto) {
        log.info("PortGroupMasterController -> search method called.");
        return portGroupMasterService.search(portGroupSearchDto);
    }
}
