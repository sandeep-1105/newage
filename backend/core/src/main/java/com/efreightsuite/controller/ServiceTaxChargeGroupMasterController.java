package com.efreightsuite.controller;

import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.ServiceTaxChargeGroupDto;
import com.efreightsuite.model.ServiceTaxChargeGroupMaster;
import com.efreightsuite.service.ServiceTaxChargeGroupMasterService;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/api/v1/stcgroupmaster")
@Log4j2
public class ServiceTaxChargeGroupMasterController {

    @Autowired
    private
    ServiceTaxChargeGroupMasterService serviceTaxChargeGroupMasterService;

    /**
     * Method - search Returns Paginated Search Result based on Search Keyword
     *
     * @return SearchRespDto
     */

    @RequestMapping(value = "/get/search", method = RequestMethod.POST)
    @Secured("ROLE_MASTER.FINANCE.SERVICE_CHARGE_MAPPING.LIST")
    public BaseDto search(@RequestBody ServiceTaxChargeGroupDto searchDto) {
        log.info("Search method is called...Search Dto: " + searchDto);
        return serviceTaxChargeGroupMasterService.search(searchDto);
    }

    /**
     * Method - create Creates and returns a new ServiceTaxChargeGroupMaster
     *
     * @returns ServiceTaxChargeGroupMaster
     */

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    @Secured("ROLE_MASTER.FINANCE.SERVICE_CHARGE_MAPPING.CREATE")
    public BaseDto create(@RequestBody ServiceTaxChargeGroupMaster stcGroupMaster) {
        log.info("create method is called...Search Dto: " + stcGroupMaster);
        return serviceTaxChargeGroupMasterService.create(stcGroupMaster);
    }

    /**
     * Method - getById Returns ServiceTaxChargeGoupMaster based on given reason id
     *
     * @return ServiceTaxChargeGoupMaster
     */

    @RequestMapping(value = "/get/id/{id}", method = RequestMethod.GET)
    @Secured("ROLE_MASTER.FINANCE.SERVICE_CHARGE_MAPPING.VIEW")
    public BaseDto getById(@PathVariable Long id) {
        log.info("getById method called...." + id);
        return serviceTaxChargeGroupMasterService.getById(id);
    }


    /**
     * Method - update returns a updated ServiceTaxChargeGoupMaster
     *
     * @param existingStcGroupMaster
     * @returns ServiceTaxChargeGoupMaster
     */


    @RequestMapping(value = "/update", method = RequestMethod.POST)
    @Secured("ROLE_MASTER.FINANCE.SERVICE_CHARGE_MAPPING.MODIFY")
    public BaseDto update(@RequestBody ServiceTaxChargeGroupMaster existingStcGroupMaster) {
        log.info("Update method called...." + existingStcGroupMaster);
        return serviceTaxChargeGroupMasterService.update(existingStcGroupMaster);
    }

    /**
     * This method is used to delete ServiceTaxChargeGoupMaster details .
     *
     * @return .
     */

    @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
    @Secured("ROLE_MASTER.FINANCE.SERVICE_CHARGE_MAPPING.DELETE")
    public BaseDto delete(@PathVariable Long id) {
        log.info("Delete method called...." + id);
        return serviceTaxChargeGroupMasterService.delete(id);
    }
}
