package com.efreightsuite.controller;

import javax.servlet.http.HttpServletResponse;

import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.IataRateMasterSearchDto;
import com.efreightsuite.model.IataRateMaster;
import com.efreightsuite.service.IataRateMasterService;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Log4j2
@RequestMapping(value = "/api/v1/iatarate")
public class IataRateMasterController {

    @Autowired
    private
    IataRateMasterService iataRateMasterService;

    @RequestMapping(value = "/search", method = RequestMethod.POST)
    @Secured("ROLE_CRM.IATA_RATES_AIR.LIST")
    public BaseDto search(@RequestBody IataRateMasterSearchDto searchDto) {
        log.info("IataRateMasterController -> search method called.");
        return iataRateMasterService.search(searchDto);
    }

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    @Secured("ROLE_CRM.IATA_RATES_AIR.CREATE")
    public BaseDto create(@RequestBody IataRateMaster iataRateMaster) {
        log.info("IataRateMasterController -> create method called.");
        return iataRateMasterService.create(iataRateMaster);
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    @Secured("ROLE_CRM.IATA_RATES_AIR.MODIFY")
    public BaseDto update(@RequestBody IataRateMaster iataRateMaster) {
        log.info("IataRateMasterController -> update method called.");
        return iataRateMasterService.update(iataRateMaster);
    }

    @RequestMapping(value = "/get/id/{id}", method = RequestMethod.GET)
    @Secured("ROLE_CRM.IATA_RATES_AIR.VIEW")
    public BaseDto getById(@PathVariable Long id) {
        log.info("IataRateMasterController -> get by id method called.");
        return iataRateMasterService.getById(id);
    }

    @RequestMapping(value = "/delete/id/{id}", method = RequestMethod.DELETE)
    @Secured("ROLE_CRM.IATA_RATES_AIR.DELETE")
    public BaseDto delete(@PathVariable Long id) {
        log.info("IataRateMasterController -> delete method called.");
        return iataRateMasterService.delete(id);
    }

    @RequestMapping(value = "/files/{id}", method = RequestMethod.GET)
    public void getFile(@PathVariable Long id, HttpServletResponse response) {
        iataRateMasterService.downloadAttachment(id, response);
    }
}
