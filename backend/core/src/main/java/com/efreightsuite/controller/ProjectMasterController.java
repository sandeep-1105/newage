package com.efreightsuite.controller;

import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.SearchRequest;
import com.efreightsuite.model.ProjectMaster;
import com.efreightsuite.service.ProjectMasterService;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/api/v1/projectmaster")
@Log4j2
public class ProjectMasterController {

    @Autowired
    private
    ProjectMasterService projectMasterService;

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public BaseDto create(@RequestBody ProjectMaster projectMaster) {
        log.info("Create method is Invoked..");
        return projectMasterService.create(projectMaster);
    }


    @RequestMapping(value = "/get/search/keyword", method = RequestMethod.POST)
    public BaseDto search(@RequestBody SearchRequest searchRequest) {
        log.info("Search method called.");
        return projectMasterService.search(searchRequest);
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public BaseDto update(@RequestBody ProjectMaster existingPeriodMaster) {
        log.info("Create method is Invoked..: " + existingPeriodMaster);
        return projectMasterService.update(existingPeriodMaster);
    }

    @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
    public BaseDto delete(@PathVariable Long id) {
        log.info("Delete method is Invoked.. : [" + id + "]");
        return projectMasterService.delete(id);
    }

    @RequestMapping(value = "/getall", method = RequestMethod.GET)
    public BaseDto get() {
        log.info("getall method Invoked.");
        return projectMasterService.getAll();
    }

}
