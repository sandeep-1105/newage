package com.efreightsuite.controller;

import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.SearchRequest;
import com.efreightsuite.dto.ServiceTypeSearchDto;
import com.efreightsuite.model.ServiceTypeMaster;
import com.efreightsuite.service.ServiceTypeMasterService;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Log4j2
@RequestMapping(value = "/api/v1/servicetypemaster")
public class ServiceTypeMasterController {

    @Autowired
    private
    ServiceTypeMasterService serviceTypeMasterService;


    @RequestMapping(value = "/get/search", method = RequestMethod.POST)
    @Secured("ROLE_MASTER.GENERAL.SERVICE_TYPE.LIST")
    public BaseDto search(@RequestBody ServiceTypeSearchDto serviceTypeSearchDto) {
        log.info("Search method called.");
        return serviceTypeMasterService.search(serviceTypeSearchDto);
    }


    @RequestMapping(value = "/get/search/keyword/{transportMode}", method = RequestMethod.POST)
    public BaseDto search(@RequestBody SearchRequest searchRequest, @PathVariable String transportMode) {
        log.info("Search method called for the transport mode " + transportMode);
        return serviceTypeMasterService.search(searchRequest, transportMode);
    }


    @RequestMapping(value = "/get/id/{id}", method = RequestMethod.GET)
    @Secured("ROLE_MASTER.GENERAL.SERVICE_TYPE.VIEW")
    public BaseDto getByPackId(@PathVariable Long id) {
        log.info("getByPackId method is called...ID = " + id);
        return serviceTypeMasterService.get(id);
    }


    @RequestMapping(value = "/create", method = RequestMethod.POST)
    @Secured("ROLE_MASTER.GENERAL.SERVICE_TYPE.CREATE")
    public BaseDto create(@RequestBody ServiceTypeMaster serviceTypeMaster) {
        log.info("Create method is called..");
        return serviceTypeMasterService.create(serviceTypeMaster);
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    @Secured("ROLE_MASTER.GENERAL.SERVICE_TYPE.MODIFY")
    public BaseDto update(@RequestBody ServiceTypeMaster existingServiceTypeMaster) {
        log.info("Update method is called.. " + existingServiceTypeMaster);
        return serviceTypeMasterService.update(existingServiceTypeMaster);
    }

    @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
    @Secured("ROLE_MASTER.GENERAL.SERVICE_TYPE.DELETE")
    public BaseDto delete(@PathVariable Long id) {
        log.info("Delete method is called.. : [" + id + "]");
        return serviceTypeMasterService.delete(id);
    }


}
