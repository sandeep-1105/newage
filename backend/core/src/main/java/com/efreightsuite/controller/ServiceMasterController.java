package com.efreightsuite.controller;

import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.SearchRequest;
import com.efreightsuite.dto.ServiceSearchDto;
import com.efreightsuite.model.ServiceMaster;
import com.efreightsuite.service.AuthService;
import com.efreightsuite.service.ServiceMasterService;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


@RestController
@Log4j2
@RequestMapping(value = "/api/v1/servicemaster")
public class ServiceMasterController {

    @Autowired
    private
    ServiceMasterService serviceMasterService;


    @RequestMapping(value = "/get/search", method = RequestMethod.POST)
    @Secured("ROLE_MASTER.GENERAL.SERVICE.LIST")
    public BaseDto search(@RequestBody ServiceSearchDto serviceSearchDto) {
        log.info("Search method called.");
        return serviceMasterService.search(serviceSearchDto);
    }

    @RequestMapping(value = "/get/search/keyword", method = RequestMethod.POST)
    public BaseDto search(@RequestBody SearchRequest searchRequest) {
        log.info("Search method called. :" + AuthService.getCurrentUser().getSelectedCompany().getCompanyName());
        return serviceMasterService.search(searchRequest);
    }


    @RequestMapping(value = "/get/search/exclude", method = RequestMethod.POST)
    public BaseDto searchServiceNotInList(@RequestBody SearchRequest searchRequest) {
        log.info("Search method called. :");
        return serviceMasterService.searchServiceNotInList(searchRequest);
    }

    @RequestMapping(value = "/get/search/keyword/{transportMode}", method = RequestMethod.POST)
    public BaseDto search(@RequestBody SearchRequest searchRequest, @PathVariable String transportMode) {
        log.info("Search method called for the transport mode " + transportMode);
        return serviceMasterService.search(searchRequest, transportMode);
    }

    @RequestMapping(value = "/get/airexportservice", method = RequestMethod.GET)
    public BaseDto getAirExportService() {
        log.info("Search method called for the transport mode ");
        return serviceMasterService.getAirExportService();
    }


    @RequestMapping(value = "/get/id/{id}", method = RequestMethod.GET)
    @Secured("ROLE_MASTER.GENERAL.SERVICE.VIEW")
    public BaseDto get(@PathVariable Long id) {
        log.info("Get by id method called.[" + id + "]");
        return serviceMasterService.get(id);
    }

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    @Secured("ROLE_MASTER.GENERAL.SERVICE.CREATE")
    public BaseDto create(@RequestBody ServiceMaster serviceMaster) {
        log.info("Create method called.");
        return serviceMasterService.create(serviceMaster);
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    @Secured("ROLE_MASTER.GENERAL.SERVICE.MODIFY")
    public BaseDto update(@RequestBody ServiceMaster serviceMaster) {
        log.info("Update method called.[" + serviceMaster.getId() + "]");
        return serviceMasterService.update(serviceMaster);
    }

    @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
    @Secured("ROLE_MASTER.GENERAL.SERVICE.DELETE")
    public BaseDto delete(@PathVariable("id") Long id) {
        log.info("Delete method called.[" + id + "]");
        return serviceMasterService.delete(id);
    }

    /**
     * To get the service all
     *
     * @return BaseDto
     */
    @RequestMapping(value = "/getall", method = RequestMethod.GET)
    public BaseDto get() {
        log.info("getall method called.");
        return serviceMasterService.getAll();
    }
}
