package com.efreightsuite.controller;

import javax.servlet.http.HttpServletResponse;

import com.efreightsuite.client.PushApiService;
import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.CopyDto;
import com.efreightsuite.dto.QuotationApproveDto;
import com.efreightsuite.dto.QuotationSearchDto;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.model.Quotation;
import com.efreightsuite.service.QuotationService;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

@RestController
@Log4j2
@RequestMapping(value = "/api/v1/quotation")
public class QuotationController {

    @Autowired
    private
    QuotationService quotationService;

    @Autowired
    private
    PushApiService pushApiService;

    @RequestMapping(value = "/get/search", method = RequestMethod.POST)
    @Secured("ROLE_SALE.QUOTATION.LIST")
    public BaseDto search(@RequestBody QuotationSearchDto searchDto) {
        log.info("Search method called.[" + searchDto + "]");
        return quotationService.search(searchDto);
    }

    @RequestMapping(value = "/get/id/{id}", method = RequestMethod.GET)
    @Secured("ROLE_SALE.QUOTATION.VIEW")
    public BaseDto get(@PathVariable Long id) {
        log.info("Get by id method called..[" + id + "]");
        return quotationService.get(id, null);
    }

    @RequestMapping(value = "/get/quotationNo/{quotationNo}", method = RequestMethod.GET)
    public BaseDto get(@PathVariable String quotationNo) {
        return quotationService.get(null, quotationNo);
    }

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    @Secured("ROLE_SALE.QUOTATION.CREATE")
    public ResponseEntity<BaseDto> save(
            @RequestBody Quotation quotation,
            UriComponentsBuilder uriBuilder) {
        log.info("Save method is called.");
        BaseDto baseDto = quotationService.create(quotation);

        if (ErrorCode.SUCCESS.equalsIgnoreCase(baseDto.getResponseCode())) {
            Quotation entity = (Quotation) baseDto.getResponseObject();
            pushApiService.pushToQuotation(entity.getId());
            return ResponseEntity.created(uriBuilder.path("/api/v1/quotation/get/id/{id}").buildAndExpand(entity.getId()).toUri()).body(baseDto);
        }

        return ResponseEntity.ok(baseDto);
    }

    @RequestMapping(value = "/update/group", method = RequestMethod.POST)
    @Secured("ROLE_SALE.QUOTATION.MODIFY")
    public BaseDto group(@RequestBody Quotation existingQuotation) {
        BaseDto baseDto = quotationService.update(existingQuotation, true);

        if (ErrorCode.SUCCESS.equalsIgnoreCase(baseDto.getResponseCode())) {
            Quotation entity = (Quotation) baseDto.getResponseObject();
            pushApiService.pushToQuotation(entity.getId());
        }

        return baseDto;
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    @Secured("ROLE_SALE.QUOTATION.MODIFY")
    public BaseDto update(@RequestBody Quotation existingQuotation) {
        BaseDto baseDto = quotationService.update(existingQuotation, false);

        if (ErrorCode.SUCCESS.equalsIgnoreCase(baseDto.getResponseCode())) {
            Quotation entity = (Quotation) baseDto.getResponseObject();
            pushApiService.pushToQuotation(entity.getId());
        }

        return baseDto;
    }

    @RequestMapping(value = "/statusupdate", method = RequestMethod.POST)
    @Secured({"ROLE_SALE.QUOTATION.APPROVE.MODIFY", "ROLE_SALE.QUOTATION.LOST.MODIFY"})
    public BaseDto updateStatus(@RequestBody QuotationApproveDto dto) {

        BaseDto baseDto = quotationService.updateStatus(dto);

        if (ErrorCode.SUCCESS.equalsIgnoreCase(baseDto.getResponseCode())) {
            pushApiService.pushToQuotation(Long.parseLong(baseDto.getResponseObject().toString()));
        }

        return baseDto;
    }

    @RequestMapping(value = "/copyquote", method = RequestMethod.POST)
    @Secured("ROLE_SALE.QUOTATION.COPY_QUOTE.CREATE")
    public BaseDto copyquote(@RequestBody CopyDto copyDto) {
        log.info("copyquote method is called.");
        BaseDto baseDto = quotationService.create(copyDto);

        if (ErrorCode.SUCCESS.equalsIgnoreCase(baseDto.getResponseCode())) {
            pushApiService.pushToQuotation(Long.parseLong(baseDto.getResponseObject().toString()));
        }

        return baseDto;
    }

    @RequestMapping(value = "/files/{id}", method = RequestMethod.GET)
    @Secured("ROLE_SALE.QUOTATION.ATTACHMENT.DOWNLOAD")
    public void getFile(@PathVariable Long id, HttpServletResponse response) {
        quotationService.downloadAttachment(id, response);
    }

	/*@RequestMapping(value = "/report/quotation", method = RequestMethod.POST)
    public void quotationWithOutEstimation(@RequestBody QuotationReportDownloadRequestDto data, HttpServletResponse response) {
		reportService.quotationWithOutEstimation(data,response);
	}*/

	/*@RequestMapping(value = "/report/quotation/withestimation", method = RequestMethod.POST)
    public void quotationWithExtimation(@RequestBody QuotationReportDownloadRequestDto data, HttpServletResponse response) {
		reportService.quotationWithEstimation(data, response);
	}*/

	/*@RequestMapping(value = "/report/quotation/print", method = RequestMethod.POST)
    public void sendEmailQuotationWithOutEstimation(@RequestBody QuotationReportEmailRequestDto data) {
		reportService.sendEmailQuotationWithOutEstimation(data);
	}*/

    @RequestMapping(value = "/getsurcharge/chargeid/{chargeId}", method = RequestMethod.GET)
    public BaseDto getSurchargeMapping(@PathVariable Long chargeId) {
        log.info("getSurchargeMapping method called..[" + chargeId + "]");
        return quotationService.getSurchargeMapping(chargeId);
    }

    @RequestMapping(value = "/getquotation/chargehistory/{id}", method = RequestMethod.GET)
    public BaseDto getConsolChargeHistory(@PathVariable Long id) {
        log.info("Quotation Charge History called");
        return quotationService.getQuotationChargeHistory(id);
    }

    @RequestMapping(value = "/isquotationcreatedfromenquiry/{enquiryNo}", method = RequestMethod.GET)
    public BaseDto isQuotationCreatedFromEnquiry(@PathVariable String enquiryNo) {
        log.info("checkQuotationNotAssociatedWithEnquiry method called..[" + enquiryNo + "]");
        return quotationService.isQuotationCreatedFromEnquiry(enquiryNo);
    }
    @RequestMapping(value = "/get/isClientApproved", method = RequestMethod.GET)
    public BaseDto isClientApproved() {
        log.info("isClientApproved method called..");
        return quotationService.isClientApproved();
    }


}
