package com.efreightsuite.controller;

import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.FlightPlanCopyDto;
import com.efreightsuite.dto.FlightPlanSearchDto;
import com.efreightsuite.model.FlightPlan;
import com.efreightsuite.service.FlightPlanService;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Log4j2
@RequestMapping(value = "/api/v1/flightplan")
public class FlightPlanController {

    @Autowired
    private
    FlightPlanService flightPlanService;

    @RequestMapping(value = "/search", method = RequestMethod.POST)
    @Secured("ROLE_AIR.FLIGHT_SCHEDULE.LIST")
    public BaseDto search(@RequestBody FlightPlanSearchDto flightPlanSearchDto) {
        log.info("Search method called.");
        return flightPlanService.search(flightPlanSearchDto);
    }

    @RequestMapping(value = "/shipment/search", method = RequestMethod.POST)
    public BaseDto shipmentFlightPlanSearch(@RequestBody FlightPlanSearchDto flightPlanSearchDto) {
        log.info("Search method called.", flightPlanSearchDto);
        return flightPlanService.shipmentFlightPlanSearch(flightPlanSearchDto);
    }

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    @Secured("ROLE_AIR.FLIGHT_SCHEDULE.CREATE")
    public BaseDto create(@RequestBody FlightPlan flightPlan) {
        log.info("Create method called.");
        return flightPlanService.saveOrUpdate(flightPlan);
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public BaseDto update(@RequestBody FlightPlan flightPlan) {
        log.info("Update method called.");
        return flightPlanService.saveOrUpdate(flightPlan);
    }

    @RequestMapping(value = "/get/id/{id}", method = RequestMethod.GET)
    @Secured("ROLE_AIR.FLIGHT_SCHEDULE.VIEW")
    public BaseDto get(@PathVariable Long id) {
        log.info("get method called.");
        return flightPlanService.get(id);
    }

    @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
    public BaseDto delete(@PathVariable Long id) {
        log.info("Delete method is called.. : [" + id + "]");
        return flightPlanService.delete(id);
    }

    @RequestMapping(value = "/copy", method = RequestMethod.POST)
    @Secured("ROLE_AIR.FLIGHT_SCHEDULE.COPY_SCHEDULE.CREATE")
    public BaseDto copy(@RequestBody FlightPlanCopyDto flightPlanCopyDto) {
        log.info("Copy method is called.");
        return flightPlanService.copy(flightPlanCopyDto);
    }


    @RequestMapping(value = "/getschedule", method = RequestMethod.POST)
    public Object getFlightPlanSchedulebyCarrierAndDate(@RequestBody FlightPlanSearchDto data) {
        return flightPlanService.getFlightPlanSchedulebyCarrierAndDate(data);
    }


}
