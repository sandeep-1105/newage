package com.efreightsuite.controller;

import com.efreightsuite.dto.AutoAttachmentDto;
import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.report.service.AutoAttachService;
import com.efreightsuite.util.CryptoException;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Log4j2
@RequestMapping(value = "/api/v1/attachment")
public class AutoAttachController {

    @Autowired
    private
    AutoAttachService autoAttachService;

    @RequestMapping(value = "/auto", method = RequestMethod.POST)
    public BaseDto autoAttachDocument(@RequestBody AutoAttachmentDto autoAttachmentDto) throws CryptoException {
        log.info("autoAttachController -> auto Attach called.");
        return autoAttachService.attach(autoAttachmentDto);
    }

}
