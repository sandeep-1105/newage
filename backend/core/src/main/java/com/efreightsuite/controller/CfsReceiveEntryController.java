package com.efreightsuite.controller;

import javax.servlet.http.HttpServletResponse;

import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.CfsReceiveSearchRequestDto;
import com.efreightsuite.model.CfsReceiveEntry;
import com.efreightsuite.service.CfsReceiveEntryService;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


@RestController
@Log4j2
@RequestMapping(value = "/api/v1/cfsreceive")
public class CfsReceiveEntryController {

    @Autowired
    private
    CfsReceiveEntryService cfsReceiveEntryService;

    @RequestMapping(value = "/get/search", method = RequestMethod.POST)
    @Secured("ROLE_AIR.CFS_RECIEVE_ENTRY.LIST")
    public BaseDto search(@RequestBody CfsReceiveSearchRequestDto cfsReceiveSearchDto) {
        log.info("CfsReceiveEntryController.search() method called.");
        return cfsReceiveEntryService.search(cfsReceiveSearchDto);
    }

    @RequestMapping(value = "/get/id/{id}", method = RequestMethod.GET)
    @Secured("ROLE_AIR.CFS_RECIEVE_ENTRY.VIEW")
    public BaseDto get(@PathVariable Long id) {
        log.info("CfsReceiveEntryController.get method is called.[ " + id + " ]");
        return cfsReceiveEntryService.get(id);
    }


    @RequestMapping(value = "/create", method = RequestMethod.POST)
    @Secured("ROLE_AIR.CFS_RECIEVE_ENTRY.CREATE")
    public BaseDto save(@RequestBody CfsReceiveEntry cfsReceiveEntry) {
        log.info("CfsReceiveEntryController.create method is called.");
        return cfsReceiveEntryService.create(cfsReceiveEntry);
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    @Secured("ROLE_AIR.CFS_RECIEVE_ENTRY.MODIFY")
    public BaseDto update(@RequestBody CfsReceiveEntry cfsReceiveEntry) {
        log.info("CfsReceiveEntryController.Update method called.");
        return cfsReceiveEntryService.update(cfsReceiveEntry);
    }

    @RequestMapping(value = "/files/{id}", method = RequestMethod.GET)
    public void getFile(@PathVariable Long id, HttpServletResponse response) {
        cfsReceiveEntryService.downloadAttachment(id, response);
    }

}
