package com.efreightsuite.controller;

import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.model.RecentHistory;
import com.efreightsuite.service.RecentHistoryService;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


@RestController
@Log4j2
@RequestMapping(value = "/api/v1/recenthistory")
public class RecentHistoryController {

    @Autowired
    private
    RecentHistoryService recentHistoryService;

    @RequestMapping(method = RequestMethod.POST)
    public BaseDto saveRecentHistory(@RequestBody RecentHistory recentHistory) {
        log.info("Class - RecentHistoryController | MTD - saveRecentHistory(recentHistory) | REQTYPE - POST called");
        return recentHistoryService.saveRecentHistory(recentHistory);
    }

    @RequestMapping(method = RequestMethod.GET)
    public BaseDto getAllRecentHistoryByUserAndLocation() {
        log.info("Class - RecentHistoryController | MTD - getAllRecentHistoryByUserAndLocation() | REQTYPE - GET called");
        return recentHistoryService.getAllRecentHistoryByUserAndLocation();
    }

    @RequestMapping(value = "/group", method = RequestMethod.GET)
    public BaseDto getGroupByRecentHistoryByUserAndLocation() {
        log.info("Class - RecentHistoryController | MTD - getGroupByRecentHistoryByUserAndLocation() | REQTYPE - GET called");
        return recentHistoryService.getGroupByRecentHistoryByUserAndLocation();
    }

    /*@RequestMapping(value = "/delete", method = RequestMethod.GET)*/
    @RequestMapping(method = RequestMethod.DELETE)
    public BaseDto deleteAllRecentHistoryByUserAndLocation() {
        log.info("Class - RecentHistoryController | MTD - deleteAllRecentHistoryByUserAndLocation() | REQTYPE - DELETE called");
        return recentHistoryService.deleteAllRecentHistoryByUserAndLocation();
    }

}
