package com.efreightsuite.controller;

import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.CarrierSearchDto;
import com.efreightsuite.dto.SearchRequest;
import com.efreightsuite.model.CarrierMaster;
import com.efreightsuite.service.CarrierMasterService;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Log4j2
@RequestMapping(value = "/api/v1/carriermaster")
public class CarrierMasterController {

    @Autowired
    private
    CarrierMasterService carrierMasterService;

    @RequestMapping(value = "/get/search", method = RequestMethod.POST)
    @Secured("ROLE_MASTER.GENERAL.CARRIER.LIST")
    public BaseDto search(@RequestBody CarrierSearchDto searchDto) {
        log.info("Search method called.");
        return carrierMasterService.search(searchDto);
    }

    @RequestMapping(value = "/get/search/keyword", method = RequestMethod.POST)
    public BaseDto search(@RequestBody SearchRequest searchRequest) {
        log.info("Search method called.");
        return carrierMasterService.search(searchRequest);
    }

    @RequestMapping(value = "/get/id/{id}", method = RequestMethod.GET)
    @Secured("ROLE_MASTER.GENERAL.CARRIER.VIEW")
    public BaseDto get(@PathVariable Long id) {
        log.info("Get by id method called.[" + id + "]");
        return carrierMasterService.get(id);
    }

    @RequestMapping(value = "/getCarrier/{carrierNo}", method = RequestMethod.GET)
    public BaseDto get(@PathVariable String carrierNo) {
        return carrierMasterService.getByCarrierNo(carrierNo);
    }


    @RequestMapping(value = "/create", method = RequestMethod.POST)
    @Secured("ROLE_MASTER.GENERAL.CARRIER.CREATE")
    public BaseDto save(@RequestBody CarrierMaster carrierMaster) {
        log.info("Create method called.");
        return carrierMasterService.create(carrierMaster);
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    @Secured("ROLE_MASTER.GENERAL.CARRIER.MODIFY")
    public BaseDto update(@RequestBody CarrierMaster carrierMaster) {
        log.info("Update method called.[" + carrierMaster.getId() + "]");
        return carrierMasterService.update(carrierMaster);
    }

    @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
    @Secured("ROLE_MASTER.GENERAL.CARRIER.DELETE")
    public BaseDto delete(@PathVariable("id") Long id) {
        log.info("Delete method called.[" + id + "]");
        return carrierMasterService.delete(id);
    }

    @RequestMapping(value = "/get/search/keyword/{transportMode}", method = RequestMethod.POST)
    public BaseDto search(@RequestBody SearchRequest searchRequest, @PathVariable String transportMode) {
        log.info("Search method called for the transport mode " + transportMode);
        return carrierMasterService.search(searchRequest, transportMode);
    }
}
