package com.efreightsuite.controller;

import javax.servlet.http.HttpServletResponse;

import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.EmployeeSearchRequestDto;
import com.efreightsuite.dto.SearchRequest;
import com.efreightsuite.model.EmployeeMaster;
import com.efreightsuite.service.EmployeeMasterService;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Log4j2
@RequestMapping(value = "/api/v1/employeemaster")
public class EmployeeMasterController {

    @Autowired
    private
    EmployeeMasterService employeeMasterService;

    @RequestMapping(value = "/get/search/keyword", method = RequestMethod.POST)
    public BaseDto search(@RequestBody SearchRequest searchRequest) {
        log.info("Search method called.");
        return employeeMasterService.search(searchRequest, false);
    }

    @RequestMapping(value = "/get/search/keyword/salesman", method = RequestMethod.POST)
    public BaseDto salesMan(@RequestBody SearchRequest searchRequest) {
        log.info("Search method called.");
        return employeeMasterService.search(searchRequest, true);
    }

    @RequestMapping(value = "/get/search", method = RequestMethod.POST)
    @Secured("ROLE_MASTER.HRMS.EMPLOYEE.VIEW")
    public BaseDto search(@RequestBody EmployeeSearchRequestDto dto) {
        log.info("List Page Search method called.");
        return employeeMasterService.search(dto);
    }

    @RequestMapping(value = "/get/id/{id}", method = RequestMethod.GET)
    @Secured("ROLE_MASTER.HRMS.EMPLOYEE.LIST")
    public BaseDto get(@PathVariable Long id) {
        log.info("get method called." + id);
        return employeeMasterService.get(id);
    }

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    @Secured("ROLE_MASTER.HRMS.EMPLOYEE.CREATE")
    public BaseDto create(@RequestBody EmployeeMaster employeeMaster) {
        log.info("Create method is called.");
        return employeeMasterService.create(employeeMaster);
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    @Secured("ROLE_MASTER.HRMS.EMPLOYEE.MODIFY")
    public BaseDto update(@RequestBody EmployeeMaster employeeMaster) {
        log.info("Update method is called.");
        return employeeMasterService.create(employeeMaster);
    }

    @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
    @Secured("ROLE_MASTER.HRMS.EMPLOYEE.DELETE")
    public BaseDto delete(@PathVariable Long id) {
        log.info("Delete method called....[" + id + "]");
        return employeeMasterService.delete(id);
    }

    @RequestMapping(value = "/get/searchByCompany/keyword/{companyId}", method = RequestMethod.POST)
    public BaseDto searchByCompany(@RequestBody SearchRequest searchRequest, @PathVariable Long companyId) {
        log.info("searchByCompany method called.");
        return employeeMasterService.searchByCompany(searchRequest, companyId);
    }

    @RequestMapping(value = "/get/search/keyword/{countryId}", method = RequestMethod.POST)
    public BaseDto getAll(@RequestBody SearchRequest searchRequest, @PathVariable Long countryId) {
        log.info("Search method called.");
        return employeeMasterService.search(searchRequest, countryId, false);
    }

    @RequestMapping(value = "/get/search/keyword/salesman/{countryId}", method = RequestMethod.POST)
    public BaseDto getAllSales(@RequestBody SearchRequest searchRequest, @PathVariable Long countryId) {
        log.info("Search method called.");
        return employeeMasterService.search(searchRequest, countryId, true);
    }

    @RequestMapping(value = "/files/{id}", method = RequestMethod.GET)
    public void getFile(@PathVariable Long id, HttpServletResponse response) {
        employeeMasterService.downloadAttachment(id, response);
    }
}
