package com.efreightsuite.controller;

import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.CurrencySearchDto;
import com.efreightsuite.dto.SearchRequest;
import com.efreightsuite.model.CurrencyMaster;
import com.efreightsuite.service.CurrencyMasterService;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Log4j2
@RequestMapping(value = "/api/v1/currencymaster")
public class CurrencyMasterController {


    @Autowired
    private
    CurrencyMasterService currencyMasterService;


    @RequestMapping(value = "/get/search", method = RequestMethod.POST)
    @Secured("ROLE_MASTER.GENERAL.CURRENCY.LIST")
    public BaseDto search(@RequestBody CurrencySearchDto searchDto) {
        log.info("Search method called.[" + searchDto + "]");
        return currencyMasterService.search(searchDto);
    }

    @RequestMapping(value = "/get/search/keyword", method = RequestMethod.POST)
    public BaseDto search(@RequestBody SearchRequest searchRequest) {
        log.info("Search method called.[" + searchRequest + "]");
        return currencyMasterService.search(searchRequest);
    }

    @RequestMapping(value = "/get/search/exclude/{excludeCurrencyCode}", method = RequestMethod.POST)
    public BaseDto search(@PathVariable String excludeCurrencyCode, @RequestBody SearchRequest searchRequest) {
        log.info("Search method called. exculdeCurrencyCode " + excludeCurrencyCode + "]");
        return currencyMasterService.search(searchRequest, excludeCurrencyCode);
    }

    @RequestMapping(value = "/get/id/{id}", method = RequestMethod.GET)
    @Secured("ROLE_MASTER.GENERAL.CURRENCY.VIEW")
    public BaseDto getByCurrencyId(@PathVariable Long id) {
        log.info("getByCurrencyId is called." + id + "]");
        return currencyMasterService.getByCurrencyId(id);
    }

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    @Secured("ROLE_MASTER.GENERAL.CURRENCY.CREATE")
    public BaseDto save(@RequestBody CurrencyMaster currencyMaster) {
        log.info("Create method called..");
        return currencyMasterService.create(currencyMaster);
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    @Secured("ROLE_MASTER.GENERAL.CURRENCY.MODIFY")
    public BaseDto update(@RequestBody CurrencyMaster currencyMaster) {
        log.info("Update method called...[" + currencyMaster.getId() + "]");
        return currencyMasterService.update(currencyMaster);
    }

    @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
    @Secured("ROLE_MASTER.GENERAL.CURRENCY.DELETE")
    public BaseDto delete(@PathVariable Long id) {
        log.info("Delete method is called...[" + id + "]");
        return currencyMasterService.delete(id);
    }

}
