package com.efreightsuite.controller;

import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.SearchRequest;
import com.efreightsuite.dto.TriggerMasterSearchDto;
import com.efreightsuite.model.TriggerMaster;
import com.efreightsuite.service.TriggerMasterService;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Log4j2
@RequestMapping(value = "/api/v1/triggermaster")
public class TriggerMasterController {

    /**
     * TriggerTypeMasterService object
     */

    @Autowired
    private
    TriggerMasterService triggerMasterService;


    @RequestMapping(value = "/get/search", method = RequestMethod.POST)
    @Secured("ROLE_MASTER.CRM.TRIGGER.LIST")
    public BaseDto search(@RequestBody TriggerMasterSearchDto searchDto) {
        log.info("Search method called.");
        return triggerMasterService.search(searchDto);
    }

    @RequestMapping(value = "/get/search/keyword", method = RequestMethod.POST)
    public BaseDto search(@RequestBody SearchRequest searchRequest) {
        log.info("Search method called.");
        return triggerMasterService.search(searchRequest, null);
    }

    @RequestMapping(value = "/get/search/keyword/{triggerType}", method = RequestMethod.POST)
    public BaseDto search(@RequestBody SearchRequest searchRequest, @PathVariable String triggerType) {
        log.info("Search method called.");
        return triggerMasterService.search(searchRequest, triggerType);
    }

    /**
     * To get the Trigger Type Master by Trigger Type Id
     *
     * @return BaseDto
     */
    @RequestMapping(value = "/get/id/{id}", method = RequestMethod.GET)
    @Secured("ROLE_MASTER.CRM.TRIGGER.VIEW")
    public BaseDto get(@PathVariable Long id) {
        log.info("get method called.");
        return triggerMasterService.get(id);
    }


    /**
     * To create the new TriggerTypeMaster
     *
     * @param triggerTypeMaster entity
     * @return BaseDto
     */
    @RequestMapping(value = "/create", method = RequestMethod.POST)
    @Secured("ROLE_MASTER.CRM.TRIGGER.CREATE")
    public BaseDto create(@RequestBody TriggerMaster triggerTypeMaster) {
        log.info("Create method called.");
        return triggerMasterService.create(triggerTypeMaster);
    }

    /**
     * To update the triggerTypeMaster
     *
     * @return BaseDto
     */

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    @Secured("ROLE_MASTER.CRM.TRIGGER.MODIFY")
    public BaseDto update(@RequestBody TriggerMaster triggerTypeMaster) {
        log.info("Update method called.");
        return triggerMasterService.update(triggerTypeMaster);
    }

    /**
     * To Delete the triggerTypeMaster Object Based on ID
     *
     * @param id
     * @return BaseDto
     */
    @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
    @Secured("ROLE_MASTER.CRM.TRIGGER.DELETE")
    public BaseDto delete(@PathVariable Long id) {
        log.info("Delete method called....");
        return triggerMasterService.delete(id);
    }

    /**
     * To get the TriggerTypeMaster all
     *
     * @return BaseDto
     */
    @RequestMapping(value = "/getall", method = RequestMethod.GET)
    public BaseDto get() {
        log.info("getall method called.");
        return triggerMasterService.getAll();
    }
}
