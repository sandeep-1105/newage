package com.efreightsuite.controller;

import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.ObjectGroupMasterSearchDto;
import com.efreightsuite.dto.SearchRequest;
import com.efreightsuite.model.ObjectGroupMaster;
import com.efreightsuite.service.ObjectGroupMasterService;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


@RestController
@Log4j2
@RequestMapping(value = "/api/v1/objectgroupmaster")
public class ObjectGroupMasterController {

    @Autowired
    private
    ObjectGroupMasterService objectGroupMasterService;

    @RequestMapping(value = "/get/search", method = RequestMethod.POST)
    public BaseDto get(@RequestBody SearchRequest searchRequest) {
        log.info("getall method called.");
        return objectGroupMasterService.search(searchRequest);
    }

    @RequestMapping(value = "/get/search/keyword", method = RequestMethod.POST)
    @Secured("ROLE_MASTER.GENERAL.OBJECT_GROUP.LIST")
    public BaseDto get(@RequestBody ObjectGroupMasterSearchDto searchDto) {
        log.info("getall method called.");
        return objectGroupMasterService.search(searchDto);
    }

    @RequestMapping(value = "/get/id/{id}", method = RequestMethod.GET)
    @Secured("ROLE_MASTER.GENERAL.OBJECT_GROUP.VIEW")
    public BaseDto get(@PathVariable Long id) {
        log.info("get method called." + id);
        return objectGroupMasterService.get(id);
    }

    @RequestMapping(value = "/get/getall", method = RequestMethod.GET)
    public BaseDto getAll() {
        log.info("getall method called.");
        return objectGroupMasterService.getAll();
    }

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    @Secured({"ROLE_MASTER.GENERAL.OBJECT_GROUP.CREATE", "ROLE_MASTER.GENERAL.OBJECT_GROUP.MODIFY"})
    public BaseDto saveOrUpdate(@RequestBody ObjectGroupMaster objectGroupMaster) {
        log.info("SaveOrUpdate method is called.");
        return objectGroupMasterService.saveOrUpdate(objectGroupMaster);
    }

    @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
    @Secured("ROLE_MASTER.GENERAL.OBJECT_GROUP.DELETE")
    public BaseDto delete(@PathVariable Long id) {
        log.info("Delete method called....");
        return objectGroupMasterService.delete(id);
    }


}
