package com.efreightsuite.controller;

import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.CostCenterSearchDto;
import com.efreightsuite.dto.SearchRequest;
import com.efreightsuite.model.CostCenterMaster;
import com.efreightsuite.service.CostCenterMasterService;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


/**
 * ClassName             Purpose                    CreatedBy/ModifiedBy      Date          Version
 * <p>
 * CostCenterMaster   Class handles all request&        SathishKumar          25-02-15        0.0
 * responses of costcenter master
 */
@RestController
@Log4j2
@RequestMapping(value = "/api/v1/costcentermaster")
public class CostCenterMasterController {
    /**
     * Autowired CostCenterMaster
     */
    @Autowired
    private
    CostCenterMasterService costCenterMasterService;

    @RequestMapping(value = "/get/search", method = RequestMethod.POST)
    @Secured("ROLE_MASTER.FINANCE.COST_CENTER.LIST")
    public BaseDto search(@RequestBody CostCenterSearchDto costCenterSearchDto) {
        log.info("Search method called.");
        return costCenterMasterService.search(costCenterSearchDto);
    }

    /**
     * Method                       Search
     *
     */
    @RequestMapping(value = "/get/search/keyword", method = RequestMethod.POST)
    public BaseDto search(@RequestBody SearchRequest searchRequest) {
        log.info("CostCenterMasterController search method called.");
        return costCenterMasterService.search(searchRequest);
    }

    /**
     * Method                        get
     *
     */
    @RequestMapping(value = "/get/id/{id}", method = RequestMethod.GET)
    @Secured("ROLE_MASTER.FINANCE.COST_CENTER.VIEW")
    public BaseDto get(@PathVariable Long id) {
        log.info("CostCenterMasterController get by id method called.");
        return costCenterMasterService.get(id);
    }

    /**
     * Method                             get
     *
     */
    @RequestMapping(value = "/get/code/{costCenterCode}", method = RequestMethod.GET)
    @Secured("ROLE_MASTER.FINANCE.COST_CENTER.VIEW")
    public BaseDto getBYCostCenterCode(@PathVariable String costCenterCode) {
        log.info("CostCenterMasterController get by code method called.");
        return costCenterMasterService.get(costCenterCode);
    }

    /**
     * Method                             get
     *
     */

    @RequestMapping(value = "/get/name/{costCenterName}", method = RequestMethod.GET)
    @Secured("ROLE_MASTER.FINANCE.COST_CENTER.VIEW")
    public BaseDto getByCostCenterName(@PathVariable String costCenterName) {
        log.info("CostCenterMasterController get by name method called.");
        return costCenterMasterService.get(costCenterName);
    }

    /**
     * Method                             save
     *
     */

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    @Secured("ROLE_MASTER.FINANCE.COST_CENTER.CREATE")
    public BaseDto save(@RequestBody CostCenterMaster costCenterMaster) {
        log.info("CostCenterMasterController create method called.");
        return costCenterMasterService.create(costCenterMaster);
    }

    /**
     * Method                             update
     *
     */
    @RequestMapping(value = "/update", method = RequestMethod.POST)
    @Secured("ROLE_MASTER.FINANCE.COST_CENTER.MODIFY")
    public BaseDto update(@RequestBody CostCenterMaster costCenterMaster) {
        log.info("CostCenterMasterController update method called.");
        return costCenterMasterService.update(costCenterMaster);
    }


    @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
    @Secured("ROLE_MASTER.FINANCE.COST_CENTER.DELETE")
    public BaseDto delete(@PathVariable Long id) {
        log.info("Delete method called....[" + id + "]");
        return costCenterMasterService.delete(id);
    }


}
