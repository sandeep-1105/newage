package com.efreightsuite.controller;

import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.CommentSearchDto;
import com.efreightsuite.dto.SearchRequest;
import com.efreightsuite.model.CommentMaster;
import com.efreightsuite.service.CommentMasterService;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Log4j2
@RequestMapping(value = "/api/v1/commentmaster")
public class CommentMasterController {
    @Autowired
    private
    CommentMasterService commentMasterService;

    @RequestMapping(value = "/get/search/keyword", method = RequestMethod.POST)
    public BaseDto search(@RequestBody SearchRequest searchRequest) {
        log.info("Search method called.....");
        return commentMasterService.search(searchRequest);
    }

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public BaseDto create(@RequestBody CommentMaster commentMaster) {

        log.info("Create method is called.. commentMaster : " + commentMaster);

        return commentMasterService.create(commentMaster);
    }


    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public BaseDto update(@RequestBody CommentMaster existingCommentMaster) {

        log.info("Create method is called.. CommentMaster : " + existingCommentMaster);

        return commentMasterService.update(existingCommentMaster);
    }

    @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
    public BaseDto delete(@PathVariable Long id) {

        log.info("Delete method is called.. : [" + id + "]");

        return commentMasterService.delete(id);
    }

    @RequestMapping(value = "/get/search", method = RequestMethod.POST)
    public BaseDto search(@RequestBody CommentSearchDto searchDto) {

        log.info("Search method is called...Search Dto: " + searchDto);

        return commentMasterService.search(searchDto);
    }

    @RequestMapping(value = "/get/id/{id}", method = RequestMethod.GET)
    public BaseDto getByReasonId(@PathVariable Long id) {

        log.info("getByCommentMasterId method is called...ID = " + id);

        return commentMasterService.getByCommentId(id);
    }
}
