package com.efreightsuite.model;

import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.envers.DefaultRevisionEntity;
import org.hibernate.envers.RevisionEntity;

/**
 * Stores the basic revision information (revision numnber, timestamp) with a custom value of the username who was executing the change.
 *
 * @author GHajba
 */

@Entity
@Table(name = "efs_newage_audit_info")
@RevisionEntity(AuditRevisionListener.class)
public class AuditEntity extends DefaultRevisionEntity {

    /**
     *
     */
    private static final long serialVersionUID = 1L;


}
