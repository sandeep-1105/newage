package com.efreightsuite.model;

import java.util.Date;

import com.efreightsuite.service.AuthService;
import com.efreightsuite.util.TimeUtil;
import lombok.extern.log4j.Log4j;
import org.hibernate.envers.RevisionListener;


@Log4j
class AuditRevisionListener implements RevisionListener {

    public void newRevision(Object revisionEntity) {
        log.info("PersistanceConfig called..");
        AuditEntity exampleRevEntity = (AuditEntity) revisionEntity;

        if (AuthService.getCurrentUser() == null) {
            exampleRevEntity.setTimestamp(new Date().getTime());
        } else {
            exampleRevEntity.setTimestamp(TimeUtil.getCurrentLocationTime().getTime());
        }


    }
}
