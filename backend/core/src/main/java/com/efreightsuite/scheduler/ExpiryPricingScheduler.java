package com.efreightsuite.scheduler;

import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.efreightsuite.common.component.EmailRequestService;
import com.efreightsuite.enumeration.TemplateType;
import com.efreightsuite.model.EmailRequest;
import com.efreightsuite.model.EmailTemplate;
import com.efreightsuite.model.LocationMaster;
import com.efreightsuite.repository.DefaultMasterDataRepository;
import com.efreightsuite.repository.EmailTemplateRepository;
import com.efreightsuite.repository.EmployeeMasterRepository;
import com.efreightsuite.repository.LocationMasterRepository;
import com.efreightsuite.repository.UserProfileRepository;
import com.efreightsuite.service.mailer.MailerUtil;
import com.efreightsuite.util.AppUtil;
import com.efreightsuite.util.TimeUtil;
import lombok.extern.log4j.Log4j2;
import org.joda.time.DateTime;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

/**
 * Scheduler runs on bases of provided cron expression
 *
 * @author Kishore Phanindra
 */

@Component
@Log4j2
public class ExpiryPricingScheduler {

    @Autowired
    private
    EntityManager em;

    @Autowired
    private
    AppUtil appUtil;

    @Autowired
    DefaultMasterDataRepository defaultMasterDataRepository;

    @Autowired
    private
    EmailRequestService emailRequestService;

    @Autowired
    private
    MailerUtil mailerUtil;

    @Autowired
    UserProfileRepository userProfileRepository;

    @Autowired
    EmployeeMasterRepository employeeMasterRepository;

    @Autowired
    private
    LocationMasterRepository locationMasterRepository;

    @Autowired
    private
    EmailTemplateRepository emailTemplateRepository;

    //@Scheduled(cron = "* */10 * * * *") // cron expression {The pattern is a
    // list of six single space-separated
    // fields: representing second, minute,
    // hour, day, month, weekday. Month and
    // weekday names can be given as the
    // first three letters of the English
    // names}-CronSequenceGenerator
    public void checkExpiryDate() {

        log.info("Scheduler is running on " + new DateTime());


        List<LocationMaster> tmpList = locationMasterRepository.findAll();

        for (LocationMaster location : tmpList) {

            String toEmailId;

            int aboutToExpiry;

            aboutToExpiry = Integer.parseInt(appUtil.getLocationConfig("pricing.expiry.days", location, false));

            List pricingPortPairList = resultListOfPricingPortPairHql(aboutToExpiry, location);

            List pricingCarrierList = resultListOfPricingCarrierHql(aboutToExpiry, location);

            //List<ExpiryPricingDto> resultList = new ArrayList<ExpiryPricingDto>();

            if ((pricingPortPairList != null && pricingPortPairList.size() != 0)
                    || (pricingCarrierList != null && pricingCarrierList.size() != 0)) {

                log.info("No of to be expiry records found for port: " + (pricingPortPairList == null ? 0 : pricingPortPairList.size()));

                log.info("No of to be expiry records found for carrier: " + (pricingCarrierList == null ? 0 : pricingCarrierList.size()));

                toEmailId = appUtil.getLocationConfig("pricing.expiry.email", location, false);


                EmailTemplate emailTemplate = emailTemplateRepository.findByTemplateType(TemplateType.EXPIRY_PRICING_BILL);

                try {
                    processEmail(formEmailRequest(location, emailTemplate, toEmailId, pricingPortPairList, pricingCarrierList), location);
                } catch (Exception e) {
                    //log.error("Expiry remainder email has not send for "+object[7].toString()+" "+object[8].toString());
                }
            } else {
                log.info("No records found for " + new DateTime().plus(aboutToExpiry));
            }

        }


    }

    private List resultListOfPricingPortPairHql(int aboutToExpiry, LocationMaster locationMaster) {

        // hql query to pull the records from pricingportpair and charge master
        String hql = "Select "
                + " pp.validToDate,"
                + " pp.minSelInMinimum, "
                + " pp.minSelInAmount, "
                + " pp.stdSelInMinimum, "
                + " pp.costInMinimum, "
                + " pp.costInAmount, "
                + " pp.stdSelInAmount,"
                + " cc.chargeCode,"
                + " cc.chargeName"
                + " From PricingPortPair pp"
                + " LEFT JOIN pp.chargeMaster cc"
                + " WHERE pp.validToDate >=:currentDate "
                + "AND pp.locationMaster.id = :locationId "
                + "pp.validToDate <= :expiryDate ";
        Query query = em.createQuery(hql);
        query.setParameter("currentDate", getCurrentDate(locationMaster));
        query.setParameter("expiryDate", getExpirtyDate(aboutToExpiry, locationMaster));
        query.setParameter("locationId", locationMaster.getId());
        return query.getResultList();
    }

    private List resultListOfPricingCarrierHql(int aboutToExpiry, LocationMaster locationMaster) {

        // hql query to pull the records from pricingportpair and charge master
        String hql = "Select "
                + " pp.validTo,"
                + " pp.fuelSurcharge, "
                + " pp.securitySurcharge, "
                + " pcs.minus45, "
                + " pcs.plus45, "
                + " pcs.plus100, "
                + " pcs.plus250,"
                + " pcs.plus300,"
                + " pcs.plus500,"
                + " pcs.plus1000,"
                + " cc.chargeCode,"
                + " cc.chargeName"
                + " From PricingCarrier pp"
                + " LEFT JOIN pp.chargeMaster cc"
                + " LEFT JOIN pp.standardSellPrice pcs"
                + " WHERE pp.validTo >=:currentDate "
                + "AND pp.locationMaster.id = :locationId "
                + "pp.validTo <= :expiryDate ";
        Query query = em.createQuery(hql);
        query.setParameter("currentDate", getCurrentDate(locationMaster));
        query.setParameter("expiryDate", getExpirtyDate(aboutToExpiry, locationMaster));
        query.setParameter("locationId", locationMaster.getId());
        return query.getResultList();
    }

    private EmailRequest formEmailRequest(LocationMaster location, EmailTemplate emailTemplate, String toEmailId,
                                          List<Object[]> pricingPortPairList, List<Object[]> pricingCarrierList) {
        // StringBuilder portPricingTable = null,pricingCarrierTable=null;
        StringBuilder tableContent = formTableContent(pricingPortPairList, pricingCarrierList);
        // StringBuilder
        // portPricingTable=formPortPricingTable(pricingPortPairList);
        // StringBuilder pricingCarrierTable
        // =formPortPricingTable(pricingCarrierList);
        EmailRequest emailRequest = new EmailRequest();
        emailRequest.setFromEmailId(emailTemplate.getFromEmailId());
        emailRequest.setFromEmailIdPassword(emailTemplate.getFromEmailPassword());
        emailRequest.setToEmailIdList(toEmailId);
        emailRequest.setSubject(emailTemplate.getSubject());
        emailRequest.setEmailBody(emailTemplate.getTemplate());


        emailRequest.setManualSystemTrack(TimeUtil.getCreateSystemTrackWithLocation(location));
        emailRequest.setEmailType(emailTemplate.getEmailType());
        emailRequest.setPort(emailTemplate.getPort());
        emailRequest.setSmtpServerAddress(emailTemplate.getSmtpServerAddress());
        // emailRequest.setEmailBody(emailRequest.getEmailBody().replace("#PORT_PRICING_DETAILS#",
        // "<table cellpadding='' cellspacing='5'
        // width='100%'>"+portPricingTable+"</table>"));
        // emailRequest.setEmailBody(emailRequest.getEmailBody().replace("#PRCING_CARRIER_DETAILS#",
        // "<table cellpadding='' cellspacing='5'
        // width='100%'>"+pricingCarrierTable+"</table>"));
        emailRequest.setEmailBody(emailRequest.getEmailBody().replace("#EMAIL_PRICING_DETAILS#", tableContent));
        return emailRequest;
    }

    private StringBuilder formPortPricingTable(List<Object[]> pricingPortPairList) {
        String portPricingTableRowTemplate = "<tr style='font-size: 13px; font-weight: bold;'> <td width='8%' align='left'>#CHARGE_NAME#</td> <td width='8%' align='left'>#EXPIRY_DATE#</td> <td width='8%' align='left'>#MIN_SEL_IN_MINIMUM#</td> <td width='8%' align='left'>#MIN_SEL_IN_AMOUNT#</td> <td width='8%' align='left'>#STD_SEL_IN_MINIMUM#</td> <td width='8%' align='left'>#STD_SEL_IN_AMOUNT#</td> <td width='8%' align='left'>#COST_IN_MINIMUM#</td> <td width='8%' align='left'>#COST_IN_AMOUNT#</td> </tr>";
        StringBuilder portPricingTable = new StringBuilder();
        for (Object[] object : pricingPortPairList) {
            String row = portPricingTableRowTemplate;
            row = row.replace("#CHARGE_CODE#", object[7] == null ? "" : object[7].toString());
            row = row.replace("#CHARGE_NAME#", object[8] == null ? "" : object[8].toString());
            row = row.replace("#EXPIRY_DATE#", object[0] == null ? "" : object[0].toString());
            row = row.replace("#MIN_SEL_IN_MINIMUM#", object[1] == null ? "" : object[1].toString());
            row = row.replace("#MIN_SEL_IN_AMOUNT#", object[2] == null ? "" : object[2].toString());
            row = row.replace("#STD_SEL_IN_MINIMUM#", object[3] == null ? "" : object[3].toString());
            row = row.replace("#STD_SEL_IN_AMOUNT#", object[6] == null ? "" : object[6].toString());
            row = row.replace("#COST_IN_MINIMUM#", object[4] == null ? "" : object[4].toString());
            row = row.replace("#COST_IN_AMOUNT#", object[5] == null ? "" : object[5].toString());
            log.info(row);
            portPricingTable.append(row);
            log.info(portPricingTable);
        }
        return portPricingTable;
    }


    private StringBuilder formPricingCarrierTable(List<Object[]> pricingCarrierList) {
        String portPricingTableRowTemplate = "<tr style='font-size: 13px; font-weight: bold;'><td width='8%' align='left'>#CHARGE_NAME#</td><td width='8%' align='left'>#VALID_TO#</td><td width='8%' align='left'>#FUEL_SURCHARGE#</td><td width='8%' align='left'>#SECURITY_SURCHARGE#</td><td width='8%' align='left'>#MINUS45#/ #PLUS45#/ #PLUS100#/ #PLUS250#/ #PLUS300#/ #PLUS500#/ #PLUS1000#</td></tr>";
        StringBuilder portPricingTable = new StringBuilder();
        for (Object[] object : pricingCarrierList) {
            String row = portPricingTableRowTemplate;
            row = row.replace("#VALID_TO#", object[0] == null ? "" : object[0].toString());
            row = row.replace("#FUEL_SURCHARGE#", object[1] == null ? "" : object[1].toString());
            row = row.replace("#SECURITY_SURCHARGE#", object[2] == null ? "" : object[2].toString());
            row = row.replace("#MINUS45#", object[3] == null ? "" : object[3].toString());
            row = row.replace("#PLUS45#", object[4] == null ? "" : object[4].toString());
            row = row.replace("#PLUS100#", object[5] == null ? "" : object[5].toString());
            row = row.replace("#PLUS250#", object[6] == null ? "" : object[6].toString());
            row = row.replace("#PLUS300#", object[7] == null ? "" : object[7].toString());
            row = row.replace("#PLUS500#", object[8] == null ? "" : object[8].toString());
            row = row.replace("#PLUS1000#", object[9] == null ? "" : object[9].toString());
            row = row.replace("#CHARGE_NAME#", object[10] == null ? "" : object[10].toString());
            row = row.replace("#CHARGE_CODE#", object[11] == null ? "" : object[11].toString());
            log.info(row);
            portPricingTable.append(row);
            log.info(portPricingTable);
        }
        return portPricingTable;
    }

    private StringBuilder formTableContent(List<Object[]> pricingPortPairList, List<Object[]> pricingCarrierList) {
        EmailTemplate headerEmailTemplate = null;
        StringBuilder table = new StringBuilder();
        if (pricingPortPairList != null && pricingPortPairList.size() != 0) {
            headerEmailTemplate = emailTemplateRepository.findByTemplateType(TemplateType.PORT_PRICING_TABLE_HEADER);
            table.append(headerEmailTemplate.getTemplate().replace("#PORT_PRICING_DETAILS#",
                    formPortPricingTable(pricingPortPairList)));
        }
        if (pricingCarrierList != null && pricingCarrierList.size() != 0) {
            headerEmailTemplate = emailTemplateRepository.findByTemplateType(TemplateType.CARRIER_PRICING_TABLE_HEADER);
            table.append(headerEmailTemplate.getTemplate().replace("#PRCING_CARRIER_DETAILS#",
                    formPricingCarrierTable(pricingCarrierList)));
        }
        return table;
    }

    private Date getCurrentDate(LocationMaster location) {
        Date tempDate = TimeUtil.getCurrentLocationTime(location);

        tempDate.setHours(0);
        tempDate.setMinutes(0);
        tempDate.setSeconds(0);

        return tempDate;
    }

    private Date getExpirtyDate(int aboutToExpiry, LocationMaster locationMaster) {
        DateTime expiryDate = null;
        expiryDate = new DateTime(getCurrentDate(locationMaster));
        expiryDate = expiryDate.plusDays(aboutToExpiry);
        Date tmpDate = expiryDate.toDate();

        tmpDate.setHours(23);
        tmpDate.setMinutes(59);
        tmpDate.setSeconds(59);

        return tmpDate;
    }

    @Async
    private void processEmail(EmailRequest emailRequest, LocationMaster locationMaster) {
        emailRequestService.saveAndSendEmail(emailRequest, locationMaster, mailerUtil.getLogo(locationMaster));
    }

}
