package com.efreightsuite.aspect;

import javax.servlet.http.HttpServletRequest;

import com.efreightsuite.configuration.TenantContext;
import com.efreightsuite.configuration.TenantService;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.RestException;
import com.fasterxml.jackson.annotation.JsonAutoDetect;
import lombok.extern.log4j.Log4j2;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

@JsonAutoDetect
@Aspect
@Component
@Log4j2
public class LoggingUserActivityAspect {

    @Value("${newage.saas.based.application.enable}")
    private
    boolean saasBased;

    @Autowired
    private
    TenantService tenantService;
    /*
	@Autowired
	AuditLogRepository auditLogRepository;

	@Autowired
	private HttpSession httpSession;
	
	@Autowired
	private HttpServletResponse httpServletResponse;

	@Autowired
	private AuditLogComponent auditLogComponent;*/


    @Pointcut("execution(* com.efreightsuite.controller.*.*(..))")
    private void pointCut() {
        // Nothing to be implement
    }

	/*@Pointcut("execution(* com.efreightsuite.service.*.*(..))")
	private void pointCutAfterReturning() {
		// Nothing to be implement
	}

	@Pointcut("execution(* *(..)) &&(within(com.efreightsuite.controller..*) || within(com.efreightsuite.service..*))")
	private void auditLogExceptionAdvicePointCut() {
		// Nothing to be implement
	}*/

    @Before("pointCut()")
    public void beforeInit() {

        ServletRequestAttributes servletContainer = (ServletRequestAttributes) RequestContextHolder
                .getRequestAttributes();

        String saas = null;
        HttpServletRequest request = servletContainer.getRequest();

        if (saasBased && request.getHeader("SAAS_ID") != null) {

            saas = request.getHeader("SAAS_ID");

            if (saas == null || saas.trim().length() == 0) {
                log.error("SAAS Id is Empty");
                throw new RestException(ErrorCode.SAAS_USER_EMPTY);
            }

            if (tenantService.getSaasMap().get(saas) != null) {
                TenantContext.setCurrentTenant(saas);
            } else {
                log.error("Invaild SAAS Id");
                throw new RestException(ErrorCode.SAAS_INVAILD_USER);
            }

        } else {
            TenantContext.setCurrentTenant(saas);
        }

    }

	/*@Before("pointCut()")
	public void insertAuditLogAdvice(JoinPoint joinPoint) {
		log.info("LoggingUserActivityAspect.insertAuditLog start.");

		try {

			if (httpSession != null) {

				String className = joinPoint.getTarget().getClass().getName();

				String methodName = joinPoint.getSignature().getName();

				AuditLog auditLog = new AuditLog();

				auditLog.setCreateDate(new Date());
				auditLog.setMethodName(methodName);
				auditLog.setClassName(className);
				auditLog.setUserName(AuthService.getCurrentUser().getUserName());

				AuditLog auditLogResponse = auditLogComponent.create(auditLog);

				// Storing AuditLog id in session
				httpSession.setAttribute(methodName, auditLogResponse.getId());

			} else {
				log.info("No session for the operation in ");
			}

		} catch (Exception e) {
			log.debug("Exception in loggingUserActivityAspect.insertAuditLog : " + e);
		}
		log.info("LoggingUserActivityAspect.insertAuditLog end.");
	}

	@AfterThrowing(value = "auditLogExceptionAdvicePointCut()", throwing = "error")
	public void auditLogExceptionAdvice(JoinPoint joinPoint, Throwable error) {

		log.info("LoggingUserActivityAspect.auditLogExceptionAdvice start.");

		log.info("Exception inside method name : " + joinPoint.getSignature().getName());

		log.debug("Exception : " + error);

		log.info("LoggingUserActivityAspect.auditLogExceptionAdvice end.");
	}

	@After("pointCut()")
	public void updateAuditLogAdvice(JoinPoint joinPoint) {
		log.info("LoggingUserActivityAspect.updateAuditLog start.");

		try {

			log.info("joinPoint.getTarget().getClass() : " + joinPoint.getTarget().getClass());

			if (httpSession != null) {

				String methodName = joinPoint.getSignature().getName();

				// Getting AuditLog id in session
				Long auditLogId = (Long) httpSession.getAttribute(methodName);
				int ststusCode = (int) httpSession.getAttribute(methodName + "StatusCode");
				httpSession.removeAttribute(methodName);
				httpSession.removeAttribute(methodName + "StatusCode");
				AuditLog auditLogToUpdate = auditLogRepository.getOne(auditLogId);

				log.info("auditLogResponse.getId() : " + auditLogId);
				
				auditLogToUpdate.setStatusCode(ststusCode);
				auditLogToUpdate.setHttpResponseCode(httpServletResponse.getStatus());
				auditLogToUpdate.setLastUpdatedDate(new Date());
				auditLogToUpdate.setLastUpdatedUserName(AuthService.getCurrentUser().getUserName());

				log.info("auditLogToUpdate : " + auditLogToUpdate);

				auditLogComponent.update(auditLogToUpdate);
				

			} else {
				log.info("No session for the operation.");
			}
		} catch (Exception e) {
			log.debug("Exception in loggingUserActivityAspect.updateAuditLogAdvice : ", e);
		}
		log.info("LoggingUserActivityAspect.updateAuditLog end.");
	}

	@AfterReturning(value = "pointCutAfterReturning()", returning = "baseDto")
	public void afterReturningAuditLogAdvice(JoinPoint joinPoint, com.efreightsuite.dto.BaseDto baseDto) {
		log.info("LoggingUserActivityAspect.afterReturningAuditLogAdvice start.");

		String methodName = joinPoint.getSignature().getName();

		log.info("The Method Name is : " + methodName);

		httpSession.setAttribute(methodName + "StatusCode", baseDto.getResponseCode());

		log.info("LoggingUserActivityAspect.afterReturningAuditLogAdvice end.");
	}*/
}
