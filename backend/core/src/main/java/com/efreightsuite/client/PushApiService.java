package com.efreightsuite.client;

import java.util.ArrayList;
import java.util.List;

import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.enumeration.PushApi;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.model.CfsAttachment;
import com.efreightsuite.model.CfsReceiveEntry;
import com.efreightsuite.model.Consol;
import com.efreightsuite.model.ConsolAttachment;
import com.efreightsuite.model.EnquiryAttachment;
import com.efreightsuite.model.EnquiryDetail;
import com.efreightsuite.model.EnquiryLog;
import com.efreightsuite.model.InvoiceCreditNote;
import com.efreightsuite.model.InvoiceCreditNoteAttachment;
import com.efreightsuite.model.PartyMaster;
import com.efreightsuite.model.Provisional;
import com.efreightsuite.model.PurchaseOrderAttachment;
import com.efreightsuite.model.Quotation;
import com.efreightsuite.model.QuotationAttachment;
import com.efreightsuite.model.Shipment;
import com.efreightsuite.model.ShipmentAttachment;
import com.efreightsuite.model.ShipmentLink;
import com.efreightsuite.model.ShipmentServiceDetail;
import com.efreightsuite.repository.*;
import com.efreightsuite.service.ConsolService;
import com.efreightsuite.service.EnquiryLogService;
import com.efreightsuite.service.InvoiceCreditNoteService;
import com.efreightsuite.service.PartyMasterService;
import com.efreightsuite.service.ProvisionalService;
import com.efreightsuite.service.QuotationService;
import com.efreightsuite.service.ShipmentService;
import com.efreightsuite.util.SaaSUtil;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.collections4.CollectionUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class PushApiService {

    @Value("${efs.push.api.required}")
    private
    boolean pushApiRequired;

    @Value("${efs.push.api.enquiry.url}")
    private
    String enquiryUrl;

    @Value("${efs.push.api.quotation.url}")
    private
    String quotationUrl;

    @Value("${efs.push.api.shipment.url}")
    private
    String shipmentUrl;

    @Value("${efs.push.api.mastershipment.url}")
    private
    String mastershipmentUrl;

    @Value("${efs.push.api.invoice.url}")
    private
    String invoiceUrl;

    @Value("${efs.push.api.provisional.url}")
    private
    String provisionalUrl;

    @Value("${efs.push.api.partymaster.url}")
    private
    String partymasterUrl;

    @Autowired
    private
    RestAPIClient restAPIClient;

    @Autowired
    private
    EnquiryLogService enquiryLogService;

    @Autowired
    private
    EnquiryLogRepository enquiryLogRepository;

    @Autowired
    private
    EnquiryAttachmentRepository enquiryAttachmentRepository;

    @Autowired
    private
    EnquiryValueAddedServiceRepository enquiryValueAddedServiceRepository;

    @Autowired
    private
    QuotationService quotationService;

    @Autowired
    private
    QuotationRepository quotationRepository;

    @Autowired
    private
    QuotationAttachmentRepository quotationAttachmentRepository;

    @Autowired
    private
    ShipmentService shipmentService;

    @Autowired
    private
    CfsAttachmentRepository cfsAttachmentRepo;

    @Autowired
    private
    ShipmentAttachmentRepository shipmentAttachmentRepo;

    @Autowired
    private
    PurchaseOrderAttachmentRepository purchaseOrderRepo;

    @Autowired
    private
    ConsolService consolService;

    @Autowired
    private
    ConsolAttachmentRepository consolAttachmentRepo;

    @Autowired
    private
    ProvisionalService provisionalService;

    @Autowired
    private
    InvoiceCreditNoteService invoiceService;

    @Autowired
    private
    InvoiceCreditNoteRepository invoiceCreditNoteRepository;

    @Autowired
    private
    PartyMasterService partyMasterService;

    @Autowired
    private
    ShipmentServiceDetailRepository shipmentServiceDetailRepository;


    public void pushToEnquiry(Long id) {
        if (!pushApiRequired) {
            return;
        }


        BaseDto response = enquiryLogService.get(id);

        if (ErrorCode.SUCCESS.equalsIgnoreCase(response.getResponseCode())) {

            EnquiryLog sendData = (EnquiryLog) response.getResponseObject();
            for (EnquiryDetail ed : sendData.getEnquiryDetailList()) {
                if (CollectionUtils.isNotEmpty(ed.getEnquiryAttachmentList())) {
                    for (EnquiryAttachment ea : ed.getEnquiryAttachmentList()) {
                        byte[] arr = enquiryAttachmentRepository.getByteArray(ea.getId());
                        if (arr != null) {
                            ea.setFile(arr);
                        }
                    }
                }

                if (CollectionUtils.isNotEmpty(ed.getEnquiryValueAddedServiceList())) {
                    ed.setEnquiryValueAddedServiceList(enquiryValueAddedServiceRepository.getValueAddedlist(ed.getId()));
                }
            }
            push(SaaSUtil.getSaaSId(), sendData, PushApi.Enquiry);
        }


    }

    public void pushToQuotation(Long id) {
        if (!pushApiRequired) {
            return;
        }

        BaseDto response = quotationService.get(id, null);

        if (ErrorCode.SUCCESS.equalsIgnoreCase(response.getResponseCode())) {
            Quotation sendData = (Quotation) response.getResponseObject();


            if (CollectionUtils.isNotEmpty(sendData.getQuotationAttachementList())) {
                for (QuotationAttachment att : sendData.getQuotationAttachementList()) {
                    byte[] arr = quotationAttachmentRepository.getByteArray(att.getId());
                    if (arr != null) {
                        att.setFile(arr);
                    }
                }
            }

            push(SaaSUtil.getSaaSId(), sendData, PushApi.Quotation);

            if (sendData.getEnquiryNo() != null) {
                EnquiryLog log = enquiryLogRepository.findByEnquiryNo(sendData.getEnquiryNo());
                pushToEnquiry(log.getId());
            }
        }


    }

    public void pushToShipment(Long id, String shipmentUid) {

        if (!pushApiRequired) {
            return;
        }

        BaseDto response = null;

        if (id != null) {
            response = shipmentService.getById(id);
        } else if (shipmentUid != null) {
            response = shipmentService.getByShipmentUid(shipmentUid);
        } else {
            return;
        }


        List<Long> quotationids = new ArrayList<>();
        List<String> serviceUids = new ArrayList<>();

        if (ErrorCode.SUCCESS.equalsIgnoreCase(response.getResponseCode())) {
            Shipment sendData = (Shipment) response.getResponseObject();
            sendData.setLastUpdatedStatus(null);
            for (ShipmentServiceDetail service : sendData.getShipmentServiceList()) {
                serviceUids.add(service.getServiceUid());
                service.setLastUpdatedStatus(null);
                if (CollectionUtils.isNotEmpty(service.getShipmentAttachmentList())) {
                    for (ShipmentAttachment att : service.getShipmentAttachmentList()) {
                        byte[] arr = shipmentAttachmentRepo.getByteArray(att.getId());
                        if (arr != null) {
                            att.setFile(arr);
                        }
                    }
                }

                if (CollectionUtils.isNotEmpty(service.getCfsReceiveEntryList())) {
                    for (CfsReceiveEntry cfs : service.getCfsReceiveEntryList()) {
                        if (CollectionUtils.isNotEmpty(cfs.getCfsAttachementList())) {
                            for (CfsAttachment att : cfs.getCfsAttachementList()) {
                                byte[] arr = cfsAttachmentRepo.getByteArray(att.getId());
                                if (arr != null) {
                                    att.setFile(arr);
                                }
                            }
                        }
                    }
                }

                if (service.getQuotationUid() != null) {
                    Quotation log = quotationRepository.findByQuotationNo(service.getQuotationUid());
                    quotationids.add(log.getId());
                }

                if (service.getPurchaseOrder() != null && CollectionUtils.isNotEmpty(service.getPurchaseOrder().getPurchaseOrderAttachmentList())) {
                    for (PurchaseOrderAttachment att : service.getPurchaseOrder().getPurchaseOrderAttachmentList()) {
                        byte[] arr = purchaseOrderRepo.getByteArrayProtected(att.getId());
                        if (arr != null) {
                            att.setProtectedFile(arr);
                        }

                        byte[] unarr = purchaseOrderRepo.getByteArrayUnProtected(att.getId());
                        if (unarr != null) {
                            att.setUnprotectedFile(unarr);
                        }

                    }
                }


            }

            push(SaaSUtil.getSaaSId(), sendData, PushApi.Shipment);


            if (CollectionUtils.isNotEmpty(serviceUids)) {
                for (String serviceUid : serviceUids) {
                    pushToProvisional(null, null, serviceUid);
                }
            }

            if (CollectionUtils.isNotEmpty(quotationids)) {
                for (Long quotationid : quotationids) {
                    pushToQuotation(quotationid);
                }
            }

        }


    }

    public void pushToMasterShipment(Long id, String consolUid) {

        if (!pushApiRequired) {
            return;
        }

        BaseDto response = null;

        if (id != null) {
            response = consolService.getById(id);
        } else if (consolUid != null) {
            response = consolService.getByConsolUid(consolUid);
        } else {
            return;
        }


        if (ErrorCode.SUCCESS.equalsIgnoreCase(response.getResponseCode())) {
            Consol sendData = (Consol) response.getResponseObject();

            if (CollectionUtils.isNotEmpty(sendData.getConsolAttachmentList())) {
                for (ConsolAttachment att : sendData.getConsolAttachmentList()) {
                    byte[] arr = consolAttachmentRepo.getByteArray(att.getId());
                    if (arr != null) {
                        att.setFile(arr);
                    }
                }
            }

            push(SaaSUtil.getSaaSId(), sendData, PushApi.MasterShipment);

            pushToProvisional(null, sendData.getConsolUid(), null);

            for (ShipmentLink sl : sendData.getShipmentLinkList()) {
                ShipmentServiceDetail shipmentServiceDetail = shipmentServiceDetailRepository.getOne(sl.getService().getId());
                pushToShipment(shipmentServiceDetail.getShipment().getId(), null);
            }

        }

    }

    public void pushToProvisional(Long id, String consolUid, String serviceUid) {

        if (!pushApiRequired) {
            return;
        }

        BaseDto response = null;

        if (consolUid != null) {
            response = provisionalService.getByMasterUid(consolUid);
        } else if (serviceUid != null) {
            response = provisionalService.getByServiceUid(serviceUid, true);
        } else if (id != null) {
            response = provisionalService.get(id);
        }

        if (response == null) {
            return;
        }


        if (ErrorCode.SUCCESS.equalsIgnoreCase(response.getResponseCode())) {
            Provisional sendData = (Provisional) response.getResponseObject();

            push(SaaSUtil.getSaaSId(), sendData, PushApi.Provisional);
        }


    }

    public void pushToInvoice(Long id) {

        if (!pushApiRequired) {
            return;
        }

        BaseDto response = invoiceService.getById(id);

        if (ErrorCode.SUCCESS.equalsIgnoreCase(response.getResponseCode())) {
            InvoiceCreditNote sendData = (InvoiceCreditNote) response.getResponseObject();

            for (InvoiceCreditNoteAttachment att : sendData.getInvoiceCreditNoteAttachmentList()) {
                byte[] arr = invoiceCreditNoteRepository.getByteArray(att.getId());
                if (arr != null) {
                    att.setFile(arr);
                }
            }

            push(SaaSUtil.getSaaSId(), sendData, PushApi.Invoice);
        }


    }

    public void pushToPartyMaster(Long id) {

        if (!pushApiRequired) {
            return;
        }

        BaseDto response = partyMasterService.get(id);

        if (ErrorCode.SUCCESS.equalsIgnoreCase(response.getResponseCode())) {
            PartyMaster sendData = (PartyMaster) response.getResponseObject();

            push(SaaSUtil.getSaaSId(), sendData, PushApi.PartyMaster);
        }


    }

    @Async
    private void push(String saasId, Object object, PushApi pushApi) {

        if (!pushApiRequired) {
            return;
        }

        sendRequest(saasId, pushApi, object);
    }

    private void sendRequest(String saasId, PushApi pushApi, Object object) {

        try {

            String responseCode = null;

            if (pushApi == PushApi.Enquiry) {
                restAPIClient.postAsync(enquiryUrl, object, saasId);
            } else if (pushApi == PushApi.Quotation) {
                restAPIClient.postAsync(quotationUrl, object, saasId);
            } else if (pushApi == PushApi.Shipment) {
                restAPIClient.postAsync(shipmentUrl, object, saasId);
            } else if (pushApi == PushApi.MasterShipment) {
                restAPIClient.postAsync(mastershipmentUrl, object, saasId);
            } else if (pushApi == PushApi.Invoice) {
                restAPIClient.postAsync(invoiceUrl, object, saasId);
            } else if (pushApi == PushApi.Provisional) {
                restAPIClient.postAsync(provisionalUrl, object, saasId);
            } else if (pushApi == PushApi.PartyMaster) {
                restAPIClient.postAsync(partymasterUrl, object, saasId);
            }

        } catch (Exception e) {
            log.error("Push API error message, ", e);
        }
    }
}
