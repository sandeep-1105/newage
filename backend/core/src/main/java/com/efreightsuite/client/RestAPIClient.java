package com.efreightsuite.client;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.LoginRequest;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.AsyncRestTemplate;
import org.springframework.web.client.RestTemplate;

@Log4j
@Service
@Getter
@Setter
public class RestAPIClient {

    RestTemplate restTemplate = new RestTemplate();
    AsyncRestTemplate asyncRestTemplate = new AsyncRestTemplate();
    Map<String, String> sessions = new HashMap<>();

    private HttpEntity<String> getAuthHeader(String saasId) {
        HttpHeaders requestHeaders = new HttpHeaders();
        requestHeaders.add("Cookie", "SESSION=" + sessions.get(saasId));
        requestHeaders.set("SAAS_ID", saasId);
        return new HttpEntity<>(requestHeaders);


    }

    private HttpEntity<Object> getPostBody(Object objectToPost, String saasId) {
        MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
        headers.add("Cookie", "SESSION=" + sessions.get(saasId));
        headers.set("SAAS_ID", saasId);
        return new HttpEntity<>(objectToPost, headers);
    }

    public BaseDto get(String url, String saasId) {
        ResponseEntity<BaseDto> responseEntity = restTemplate.exchange(url, HttpMethod.GET, getAuthHeader(saasId), BaseDto.class);
        log.info("Response Entity : " + responseEntity);

        return responseEntity.getBody();
    }

    public BaseDto post(String url, Object objectToPost, String saasId) {

        HttpHeaders requestHeaders = new HttpHeaders();
        //\\requestHeaders.add("Cookie", "SESSION=" + sessions.get(saasId));
        requestHeaders.set("SAAS_ID", saasId);
        requestHeaders.setContentType(MediaType.APPLICATION_JSON);
        requestHeaders.add("Cache-Control", "no-cache");
        HttpEntity<Object> entity = new HttpEntity<>(objectToPost, requestHeaders);
        ResponseEntity<BaseDto> responseEntity = restTemplate.exchange(url, HttpMethod.POST, entity, BaseDto.class);


        //ResponseEntity<BaseDto> responseEntity = restTemplate.exchange(url, HttpMethod.POST, getPostBody(objectToPost, saasId), BaseDto.class);

        log.info("Response Entity : " + responseEntity);

        return responseEntity.getBody();
    }

    public void postAsync(String url, Object objectToPost, String saasId) {
        HttpHeaders requestHeaders = new HttpHeaders();
        requestHeaders.set("SAAS_ID", saasId);
        requestHeaders.setContentType(MediaType.APPLICATION_JSON);
        requestHeaders.add("Cache-Control", "no-cache");
        HttpEntity<Object> entity = new HttpEntity<>(objectToPost, requestHeaders);
        asyncRestTemplate.exchange(url, HttpMethod.POST, entity, BaseDto.class);
    }


    public BaseDto delete(String url, Object objectToPost, String saasId) {
        ResponseEntity<BaseDto> responseEntity = restTemplate.exchange(url, HttpMethod.DELETE,
                getPostBody(objectToPost, saasId), BaseDto.class);
        log.info("Response Entity : " + responseEntity);
        return responseEntity.getBody();
    }

    public BaseDto login(String url, String saasId, String userName, String password) {

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set("SAAS_ID", saasId);
        LoginRequest loginRequest = new LoginRequest(userName, password);
        HttpEntity<LoginRequest> entity = new HttpEntity<>(loginRequest, headers);
        ResponseEntity<BaseDto> responseEntity = restTemplate.exchange(url, HttpMethod.POST, entity, BaseDto.class);
        log.info("Response Entity : " + responseEntity);
        HttpHeaders httpHeaders = responseEntity.getHeaders();
        Set<String> keys = httpHeaders.keySet();
        String cookie = "";
        for (String header : keys) {
            if (header.equals("Set-Cookie")) {
                cookie = httpHeaders.get(header).get(0);
            }
        }
        String jsessionid = (cookie.split(";")[0]).split("=", 2)[1];
        sessions.put(saasId, jsessionid);
        log.info("Session ID : " + jsessionid);
        return responseEntity.getBody();
    }

}
