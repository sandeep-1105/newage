package com.efreightsuite.common.component;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.activation.DataSource;

class BufferedDataSource implements DataSource {

    private final byte[] _data;
    private final java.lang.String _name;

    public BufferedDataSource(byte[] data, String name) {
        _data = data;
        _name = name;
    }

    public String getContentType() {
        return "application/octet-stream";
    }

    public InputStream getInputStream() throws IOException {
        return new ByteArrayInputStream(_data);
    }

    public String getName() {
        return _name;
    }

    /**
     * Returns an OutputStream from the DataSource
     *
     * @returns OutputStream Array of bytes converted into an OutputStream
     */
    public OutputStream getOutputStream() throws IOException {
        OutputStream out = new ByteArrayOutputStream();
        out.write(_data);
        return out;
    }

}
