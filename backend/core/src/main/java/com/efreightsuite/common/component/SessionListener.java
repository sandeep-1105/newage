package com.efreightsuite.common.component;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSessionEvent;

import com.efreightsuite.enumeration.SessionStatus;
import com.efreightsuite.model.LoginHistory;
import com.efreightsuite.repository.LoginHistoryRepository;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.web.session.HttpSessionEventPublisher;
import org.springframework.stereotype.Service;

@Log4j2
@Configuration
@Service
public class SessionListener extends HttpSessionEventPublisher {

    @Autowired
    private
    LoginHistoryRepository loginHistoryRepo;

    @Override
    public void sessionCreated(HttpSessionEvent event) {
        log.info("Session created " + event.getSession().getId());
    }

    @Override
    public void sessionDestroyed(HttpSessionEvent event) {
        log.info("Session destroyed " + event.getSession().getId());

        List<LoginHistory> loginHistoryList = loginHistoryRepo.findBySessionIdAndSessionStatus(event.getSession().getId(), SessionStatus.CREATED);

        if (loginHistoryList != null && loginHistoryList.size() != 0) {

            for (LoginHistory loginHistory : loginHistoryList) {
                loginHistory.setTimeoutTime(new Date());
                loginHistory.setSessionStatus(SessionStatus.EXPIRED);
            }

            loginHistoryRepo.save(loginHistoryList);
        }
        event.getSession().invalidate();
    }

}
