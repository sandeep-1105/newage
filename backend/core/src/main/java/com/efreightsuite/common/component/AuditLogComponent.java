package com.efreightsuite.common.component;

import com.efreightsuite.model.AuditLog;
import com.efreightsuite.repository.AuditLogRepository;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * ClassName Purpose CreatedBy/ModifiedBy Date Version
 * AuditLogComponent : Class handles insert and update operation on AuditLog : Devendracahry M 26-02-15 0.0
 */

@Service
@Log4j2
public class AuditLogComponent {

    /*
     * AuditLogRepository autowired
     */
    @Autowired
    private
    AuditLogRepository auditLogRepository;

    /**
     * create - This method creates a new carrier
     *
     * @Param AuditLog entity
     * @Return AuditLog
     */

    public AuditLog create(AuditLog auditLog) {

        log.info("AuditLogComponent.create method start");

        AuditLog auditLogResponse = new AuditLog();

        try {

            auditLogResponse = auditLogRepository.save(auditLog);
            log.info("AuditLog Saved successfully.");

        } catch (Exception e) {
            log.error("Exception Occured while saving the AuditLog in AuditLogComponent.create ", e);
            return null;
        }

        log.info("AuditLogComponent.create method end");
        return auditLogResponse;

    }
    /*
	 * Update- This method updates a existing AuditLog
	 * @Param AuditLog entity
	 */

    public AuditLog update(AuditLog existingAuditLog) {


        log.info("AuditLogComponent.update method start");

        AuditLog auditLogUpdateResponse = new AuditLog();

        try {

            auditLogUpdateResponse = auditLogRepository.save(existingAuditLog);

            log.info("AuditLog  updated successfully...");

        } catch (Exception e) {
            log.error("Exception occured while updating the AuditLog in AuditLogComponent.update method: ", e);
            return null;
        }

        log.info("AuditLogComponent.update method end");

        return auditLogUpdateResponse;
    }

}
