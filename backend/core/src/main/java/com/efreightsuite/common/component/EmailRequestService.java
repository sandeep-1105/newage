package com.efreightsuite.common.component;

import com.efreightsuite.dto.LocationSetupDTO;
import com.efreightsuite.enumeration.EmailStatus;
import com.efreightsuite.enumeration.common.LocationSetupSection;
import com.efreightsuite.enumeration.common.TemplateType;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.model.EmailRequest;
import com.efreightsuite.model.EmailRequestAttachment;
import com.efreightsuite.model.LocationMaster;
import com.efreightsuite.model.UserProfile;
import com.efreightsuite.model.common.CommonEmailRequest;
import com.efreightsuite.model.common.CommonEmailTemplate;
import com.efreightsuite.repository.EmailRequestRepository;
import com.efreightsuite.repository.common.CommonEmailRequestRepository;
import com.efreightsuite.repository.common.CommonEmailTemplateRepository;
import com.efreightsuite.util.AppUtil;
import com.efreightsuite.util.TimeUtil;
import com.efreightsuite.util.ValidateUtil;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.util.ByteArrayDataSource;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

@Service
@Log4j2
public class EmailRequestService {

    @Autowired
    private
    AppUtil appUtil;

    @Autowired
    private
    EmailRequestRepository emailRequestRepository;

    @Value("${efs.application.base.url}")
    private
    String applicationBaseUrl;

    @Autowired
    private
    CommonEmailRequestRepository commonEmailRequestRepository;

    @Autowired
    private
    CommonEmailTemplateRepository commonEmailTemplateRepository;

    @Autowired
    private
    CommonAsyncMailer commonAsyncMailer;

    public void saveAndSendEmail(EmailRequest emailRequest, LocationMaster location, Map<String, byte[]> logoMap) {

        EmailRequest email = beforeSend(emailRequest, null, location);
        if (email.getId() != null) {
            emailRequest.setEmailStatus(EmailStatus.PROGRESSING);
            TimeUtil.setUpdateSystemTrackWithLocation(emailRequest.getManualSystemTrack(), location);

            emailRequestRepository.save(emailRequest);

            sendEmail(emailRequest, logoMap);
        } else {
            log.error("Email Not send because of could not save or invalid");
        }

    }

    public void saveAndSendEmail(EmailRequest emailRequest, UserProfile userProfile, Map<String, byte[]> logoMap) {

        EmailRequest email = beforeSend(emailRequest, userProfile, null);
        if (email.getId() != null) {
            emailRequest.setEmailStatus(EmailStatus.PROGRESSING);
            TimeUtil.setUpdateSystemTrack(emailRequest.getManualSystemTrack(), userProfile);

            emailRequestRepository.save(emailRequest);

            sendEmail(emailRequest, logoMap);
        } else {
            log.error("Email Not send because of could not save or invalid");
        }

    }

    public void saveAndSendEmailForTrackShipment(byte[] imageBytes, EmailRequest emailRequest, UserProfile userProfile, Map<String, byte[]> logoMap) {

        EmailRequest email = beforeSend(emailRequest, userProfile, null);
        if (email.getId() != null) {
            emailRequest.setEmailStatus(EmailStatus.PROGRESSING);
            TimeUtil.setUpdateSystemTrack(emailRequest.getManualSystemTrack(), userProfile);

            emailRequestRepository.save(emailRequest);

            sendEmailForTrackShipment(imageBytes, emailRequest, logoMap);
        } else {
            log.error("Email Not send because of could not save or invalid");
        }

    }

    private EmailRequest beforeSend(EmailRequest emailRequest, UserProfile userProfile, LocationMaster location) {

        try {
            emailRequest.setEmailStatus(EmailStatus.PENDING);
            ValidateUtil.notNull(emailRequest.getFromEmailId(), ErrorCode.EMAIL_FROM_NOTNULL);
            ValidateUtil.notNull(emailRequest.getFromEmailIdPassword(), ErrorCode.EMAIL_FROM_NOTNULL);
            ValidateUtil.notNull(emailRequest.getSubject(), ErrorCode.EMAIL_SUBJECT_NOTNULL);
            ValidateUtil.notNull(emailRequest.getEmailBody(), ErrorCode.EMAIL_BODY_NOTNULL);
            ValidateUtil.notNull(emailRequest.getSmtpServerAddress(), ErrorCode.EMAIL_SERVER_ADDRESS_REQUIRED);
            ValidateUtil.notNull(emailRequest.getPort(), ErrorCode.EMAIL_SERVER_PORT_REQUIRED);

            if (!validateCSVEmailid(emailRequest.getToEmailIdList(), ErrorCode.EMAIL_TO_NOTNULL)) {
                emailRequest.setEmailStatus(EmailStatus.FAILURE);
                emailRequest.setErrorDesc(appUtil.getDesc(ErrorCode.EMAIL_TO_NOTNULL));
            } else if (!validateCSVEmailid(emailRequest.getBccEmailIdList(), ErrorCode.EMAIL_BCC_NOTNULL)) {
                emailRequest.setEmailStatus(EmailStatus.FAILURE);
                emailRequest.setErrorDesc(appUtil.getDesc(ErrorCode.EMAIL_BCC_NOTNULL));
            } else if (!validateCSVEmailid(emailRequest.getCcEmailIdList(), ErrorCode.EMAIL_CC_NOTNULL)) {
                emailRequest.setEmailStatus(EmailStatus.FAILURE);
                emailRequest.setErrorDesc(appUtil.getDesc(ErrorCode.EMAIL_CC_NOTNULL));
            }

            if (userProfile != null) {
                emailRequest.setManualSystemTrack(TimeUtil.getCreateSystemTrack(userProfile));
            } else if (location != null) {
                emailRequest.setManualSystemTrack(TimeUtil.getCreateSystemTrackWithLocation(location));
            }

            emailRequest = emailRequestRepository.save(emailRequest);
            log.info("Created EmailRequest with id: " + emailRequest.getId() + " is Valid: "
                    + emailRequest.getEmailStatus());

            emailRequest.setId(emailRequest.getId());

        } catch (RestException re) {

            log.error("Validation Exception in email request", re);

            String errorCode = re.getMessage().split(" ")[0];

            emailRequest.setEmailStatus(EmailStatus.FAILURE);
            emailRequest.setErrorDesc(appUtil.getDesc(errorCode));

        } catch (Exception e) {
            emailRequest.setEmailStatus(EmailStatus.FAILURE);
            emailRequest.setErrorDesc(appUtil.getDesc(ErrorCode.INVALID_PARAMETER));
            log.error("Validation Excepiont in email request", e);
        }

        return emailRequest;
    }

    private void sendEmail(EmailRequest emailRequest, Map<String, byte[]> logoMap) {

        try {

            EmailRequest tmpObj = emailRequestRepository.findById(emailRequest.getId());
            emailRequest.setVersionLock(tmpObj.getVersionLock());

            Session session = setPropertiesAndGetSession(emailRequest);

            // creates a new e-mail message
            Message msg = new MimeMessage(session);

            msg.setFrom(new InternetAddress(emailRequest.getFromEmailId()));
            if (emailRequest.getToEmailIdList() != null) {
                msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(emailRequest.getToEmailIdList()));
            }
            msg.setSubject(emailRequest.getSubject());
            msg.setSentDate(new Date());

            if (emailRequest.getCcEmailIdList() != null) {
                log.info("sending email as cc to  : " + emailRequest.getCcEmailIdList());
                msg.addRecipients(Message.RecipientType.CC, InternetAddress.parse(emailRequest.getCcEmailIdList()));
            }
            if (emailRequest.getBccEmailIdList() != null) {
                log.info("sending email as Bcc to  : " + emailRequest.getBccEmailIdList());
                msg.addRecipients(Message.RecipientType.BCC, InternetAddress.parse(emailRequest.getBccEmailIdList()));
            }

            // creates message part
            MimeBodyPart messageBodyPart = new MimeBodyPart();
            messageBodyPart.setContent(emailRequest.getEmailBody(), "text/html");

            // creates multi-part
            Multipart multipart = new MimeMultipart();
            multipart.addBodyPart(messageBodyPart);

            if (emailRequest.getAttachmentList() != null && emailRequest.getAttachmentList().size() > 0) {
                checkAndAddAttachments(emailRequest, multipart);
            }

            if (logoMap != null && logoMap.size() != 0) {

                for (String contentId : logoMap.keySet()) {

                    if (logoMap.get(contentId) == null) {
                        continue;
                    }
                    if (contentId.equals("#COMPANY_LOGO#")) {
                        BodyPart tmp = new MimeBodyPart();
                        BufferedDataSource fds = new BufferedDataSource(logoMap.get(contentId), "companylogo.png");
                        // DataSource fds = new
                        // FileDataSource("/home/administrator/Desktop/FSL.png");
                        tmp.setDataHandler(new DataHandler(fds));
                        tmp.setHeader("Content-ID", "<companylogo>");
                        multipart.addBodyPart(tmp);
                    } else if (contentId.equals("#POWERED_BY#")) {
                        BodyPart tmp = new MimeBodyPart();
                        BufferedDataSource fds = new BufferedDataSource(logoMap.get(contentId), "poweredby.png");
                        // DataSource fds = new
                        // FileDataSource("/home/administrator/Desktop/FSL.png");
                        tmp.setDataHandler(new DataHandler(fds));
                        tmp.setHeader("Content-ID", "<poweredby>");
                        multipart.addBodyPart(tmp);
                    }

                }
            }
            msg.setContent(multipart);
            log.info("Mail is sent to : " + emailRequest.getToEmailIdList());
            Transport.send(msg);
            emailRequest.setEmailStatus(EmailStatus.SUCCESS);
        } catch (MessagingException e) {
            log.error("Error in sending Email : " + emailRequest.getFromEmailId() + ", "
                    + emailRequest.getFromEmailIdPassword(), e);
            emailRequest.setEmailStatus(EmailStatus.FAILURE);

            emailRequest.setErrorDesc(ErrorCode.ERROR_IN_EMAIL_SENDING);
        } catch (Exception e) {
            log.error("Exception in sending Email : " + emailRequest.getFromEmailId() + ", "
                    + emailRequest.getFromEmailIdPassword(), e);
            emailRequest.setEmailStatus(EmailStatus.FAILURE);

            emailRequest.setErrorDesc(ErrorCode.ERROR_IN_EMAIL_SENDING);
        }

        emailRequestRepository.save(emailRequest);

    }

    private void sendEmailForTrackShipment(byte[] byteArr, EmailRequest emailRequest, Map<String, byte[]> logoMap) {

        try {

            EmailRequest tmpObj = emailRequestRepository.findById(emailRequest.getId());
            emailRequest.setVersionLock(tmpObj.getVersionLock());

            Session session = setPropertiesAndGetSession(emailRequest);

            // creates a new e-mail message
            Message msg = new MimeMessage(session);

            msg.setFrom(new InternetAddress(emailRequest.getFromEmailId()));
            if (emailRequest.getToEmailIdList() != null) {
                msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(emailRequest.getToEmailIdList()));
            }
            msg.setSubject(emailRequest.getSubject());
            msg.setSentDate(new Date());

            if (emailRequest.getCcEmailIdList() != null) {
                log.info("sending email as cc to  : " + emailRequest.getCcEmailIdList());
                msg.addRecipients(Message.RecipientType.CC, InternetAddress.parse(emailRequest.getCcEmailIdList()));
            }
            if (emailRequest.getBccEmailIdList() != null) {
                log.info("sending email as Bcc to  : " + emailRequest.getBccEmailIdList());
                msg.addRecipients(Message.RecipientType.BCC, InternetAddress.parse(emailRequest.getBccEmailIdList()));
            }

            // creates message part
            MimeBodyPart messageBodyPart = new MimeBodyPart();
            //messageBodyPart.setContent("<H1>Hello</H1><img src=\"cid:trackShipment\">","text/html");
            messageBodyPart.setContent(emailRequest.getEmailBody(), "text/html");


            BodyPart trackShipmentImagePart = new MimeBodyPart();
            BufferedDataSource bds = new BufferedDataSource(byteArr, "trackShipment.png");
            trackShipmentImagePart.setDataHandler(new DataHandler(bds));
            trackShipmentImagePart.setHeader("Content-ID", "<trackShipment>");


            // creates multi-part
            Multipart multipart = new MimeMultipart("related");
            multipart.addBodyPart(messageBodyPart);
            multipart.addBodyPart(trackShipmentImagePart);

            if (emailRequest.getAttachmentList() != null && emailRequest.getAttachmentList().size() > 0) {
                checkAndAddAttachments(emailRequest, multipart);
            }

            if (logoMap != null && logoMap.size() != 0) {

                for (String contentId : logoMap.keySet()) {

                    if (logoMap.get(contentId) == null) {
                        continue;
                    }
                    if (contentId.equals("#COMPANY_LOGO#")) {
                        BodyPart tmp = new MimeBodyPart();
                        BufferedDataSource fds = new BufferedDataSource(logoMap.get(contentId), "companylogo.png");
                        // DataSource fds = new
                        // FileDataSource("/home/administrator/Desktop/FSL.png");
                        tmp.setDataHandler(new DataHandler(fds));
                        tmp.setHeader("Content-ID", "<companylogo>");
                        multipart.addBodyPart(tmp);
                    } else if (contentId.equals("#POWERED_BY#")) {
                        BodyPart tmp = new MimeBodyPart();
                        BufferedDataSource fds = new BufferedDataSource(logoMap.get(contentId), "poweredby.png");
                        // DataSource fds = new
                        // FileDataSource("/home/administrator/Desktop/FSL.png");
                        tmp.setDataHandler(new DataHandler(fds));
                        tmp.setHeader("Content-ID", "<poweredby>");
                        multipart.addBodyPart(tmp);
                    }

                }
            }
            msg.setContent(multipart);
            log.info("Mail is sent to : " + emailRequest.getToEmailIdList());
            Transport.send(msg);
            emailRequest.setEmailStatus(EmailStatus.SUCCESS);
        } catch (MessagingException e) {
            log.error("Error in sending Email : " + emailRequest.getFromEmailId() + ", "
                    + emailRequest.getFromEmailIdPassword(), e);
            emailRequest.setEmailStatus(EmailStatus.FAILURE);

            emailRequest.setErrorDesc(ErrorCode.ERROR_IN_EMAIL_SENDING);
        } catch (Exception e) {
            log.error("Exception in sending Email : " + emailRequest.getFromEmailId() + ", "
                    + emailRequest.getFromEmailIdPassword(), e);
            emailRequest.setEmailStatus(EmailStatus.FAILURE);

            emailRequest.setErrorDesc(ErrorCode.ERROR_IN_EMAIL_SENDING);
        }

        emailRequestRepository.save(emailRequest);

    }

    private Session setPropertiesAndGetSession(EmailRequest emailRequest) {
        Properties props = new Properties();
        props.put("mail.smtp.host", emailRequest.getSmtpServerAddress());
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.port", emailRequest.getPort());

        return Session.getInstance(props, new javax.mail.Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(emailRequest.getFromEmailId(), emailRequest.getFromEmailIdPassword());
            }
        });
    }

    private void checkAndAddAttachments(EmailRequest emailRequest, Multipart multipart) throws MessagingException {

        for (EmailRequestAttachment attachmentDto : emailRequest.getAttachmentList()) {
            if (attachmentDto.getMimeType().equalsIgnoreCase("text/html")) {
                log.info("Adding text/html MimeBodyPart");
                MimeBodyPart messageBodyPart = new MimeBodyPart();
                messageBodyPart.setContent(new String(attachmentDto.getAttachement()), attachmentDto.getMimeType());
                messageBodyPart.setFileName(attachmentDto.getFileName());
                multipart.addBodyPart(messageBodyPart);
            } else {
                log.info("Adding other than text/html MimeBodyPart");
                MimeBodyPart messageBodyPart = new MimeBodyPart();
                DataSource dataSource = new ByteArrayDataSource(attachmentDto.getAttachement(),
                        attachmentDto.getMimeType());
                messageBodyPart.setDataHandler(new DataHandler(dataSource));
                messageBodyPart.setFileName(attachmentDto.getFileName());
                multipart.addBodyPart(messageBodyPart);
            }
        }

    }

    private boolean validateCSVEmailid(String csvEmailIDs, String errorDesc) {
        boolean isValid = true;
        if (csvEmailIDs != null && csvEmailIDs.length() != 0) {
            String[] emailidArray = csvEmailIDs.split(",");
            try {
                for (String emailID : emailidArray) {
                    log.info("Validating email:" + emailID);
                    ValidateUtil.notNull(emailID, errorDesc);
                }
            } catch (Exception e) {
                isValid = false;
                log.error("Invalid Email ID in: " + appUtil.getDesc(errorDesc), e);
            }
            log.info("Validation Result for " + appUtil.getDesc(errorDesc) + " :" + csvEmailIDs + ":" + isValid);
            return isValid;
        } else {
            return true;
        }

    }

    public void sendTrailMail(Long locationSetupId, String otp, String toEmail, String ccEmail,
                              LocationSetupSection currentSection, String saasId) {

        if (locationSetupId == null || toEmail == null) {
            log.error("Required Data is missing to send an email.");
            return;
        }

        log.info("Sending Trail version Mail started............");

        CommonEmailTemplate emailTemplate = commonEmailTemplateRepository
                .findByTemplateType(TemplateType.USER_TRAIL_MAIL);

        if (emailTemplate == null) {
            log.error("Template for the Account Activation Mail is not present");
            return;
        }

        try {

            Map<String, Object> mapSubject = new HashMap<>();
            Map<String, Object> mapBody = new HashMap<>();
            mapBody.put("###ACTIVATION_LINK###", applicationBaseUrl
                    + "/public/user_registration/user_registration.html#/company/" + locationSetupId);
            mapBody.put("###SAAS_ID###", saasId);
            // sending email
            String emailSeprate = ";";
            CommonEmailRequest emailRequest = new CommonEmailRequest();
            emailRequest.setFromEmailId(emailTemplate.getFromEmailId());
            emailRequest.setFromEmailIdPassword(emailTemplate.getFromEmailPassword());
            emailRequest.setToEmailIdList(toEmail.trim().replaceAll(emailSeprate, ","));
            emailRequest.setCcEmailIdList(ccEmail != null ? ccEmail.trim().replaceAll(emailSeprate, ",") : ccEmail);
            emailRequest.setEmailType(emailTemplate.getEmailType());
            emailRequest.setPort(emailTemplate.getPort());
            emailRequest.setSmtpServerAddress(emailTemplate.getSmtpServerAddress());
            emailRequest.setSubject(emailTemplate.getSubject());
            emailRequest.setEmailBody(emailTemplate.getTemplate());

            for (String key : mapSubject.keySet()) {
                emailRequest.setSubject(emailRequest.getSubject().replaceAll(key,
                        mapSubject.get(key) == null ? "" : mapSubject.get(key).toString()));
            }

            for (String key : mapBody.keySet()) {
                emailRequest.setEmailBody(emailRequest.getEmailBody().replaceAll(key,
                        mapBody.get(key) == null ? "" : mapBody.get(key).toString()));
            }

            saveAndSendEmail(emailRequest);
        } catch (Exception e) {
            log.error("Exception in sendAccountActivationMail  ", e);
        }
        log.info("Sending Account Activation Mail Finished............");

    }

    public void sendAccountActivationMail(Long locationSetupId, String activationKey, String toEmail, String ccEmail) {

        if (locationSetupId == null || toEmail == null || activationKey == null) {
            log.error("Required Data is missing to send an email.");
            return;
        }

        log.info("Sending Account Activation Mail started............");

        CommonEmailTemplate emailTemplate = commonEmailTemplateRepository
                .findByTemplateType(TemplateType.ACTIVATION_MAIL);

        if (emailTemplate == null) {
            log.error("Template for the Account Activation Mail is not present");
            return;
        }

        try {

            Map<String, Object> mapBody = new HashMap<>();
            mapBody.put("###ACTIVATION_LINK###",
                    applicationBaseUrl + "/public/account_activation/account_activation_form.html?" + locationSetupId);
            mapBody.put("###ACTIVATION_CODE###", activationKey);

            // sending email
            CommonEmailRequest emailRequest = new CommonEmailRequest();

            String emailSeprate = ";";

            emailRequest.setFromEmailId(emailTemplate.getFromEmailId());
            emailRequest.setFromEmailIdPassword(emailTemplate.getFromEmailPassword());
            emailRequest.setToEmailIdList(toEmail.trim().replaceAll(emailSeprate, ","));
            emailRequest.setCcEmailIdList(ccEmail != null ? ccEmail.trim().replaceAll(emailSeprate, ",") : ccEmail);
            emailRequest.setEmailType(emailTemplate.getEmailType());
            emailRequest.setPort(emailTemplate.getPort());
            emailRequest.setSmtpServerAddress(emailTemplate.getSmtpServerAddress());
            emailRequest.setSubject(emailTemplate.getSubject());
            emailRequest.setEmailBody(emailTemplate.getTemplate());
            

            for (String key : mapBody.keySet()) {
                emailRequest.setEmailBody(emailRequest.getEmailBody().replaceAll(key,
                        mapBody.get(key) == null ? "" : mapBody.get(key).toString()));
            }

            saveAndSendEmail(emailRequest);
        } catch (Exception e) {
            log.error("Exception in sendAccountActivationMail  ", e);
        }
        log.info("Sending Account Activation Mail Finished............");
    }

    public void sendSaasIdProcessCompletedMail(LocationSetupDTO locationSetupDTO ) {

        if (locationSetupDTO.getSaasId() == null || locationSetupDTO.getEmail() == null) {
            log.error("Required Data is missing to send SaasId Process Completed Mail.");
            return;
        }

        log.info("Sending SaasId Process Completed Mail started............");

        CommonEmailTemplate emailTemplate = commonEmailTemplateRepository
                .findByTemplateType(TemplateType.ACCOUNT_CREATION_PROCESS_COMPLETED_MAIL);

        if (emailTemplate == null) {
            log.error("Template for the SaasId Process Completed Mail is not present");
            return;
        }

        try {

            Map<String, Object> mapBody = new HashMap<>();
            mapBody.put("###LOGIN_LINK###", applicationBaseUrl + "/index.html");
            mapBody.put("###USER_NAME###", locationSetupDTO.getUsername());
            mapBody.put("###PASSWORD###", locationSetupDTO.getPassword());
            mapBody.put("###SAAS_ID###", locationSetupDTO.getSaasId());

            CommonEmailRequest emailRequest = new CommonEmailRequest();
            emailRequest.setFromEmailId(emailTemplate.getFromEmailId());
            emailRequest.setFromEmailIdPassword(emailTemplate.getFromEmailPassword());
            emailRequest.setToEmailIdList(locationSetupDTO.getCommaSeperatedEmails());
            emailRequest.setEmailType(emailTemplate.getEmailType());
            emailRequest.setPort(emailTemplate.getPort());
            emailRequest.setSmtpServerAddress(emailTemplate.getSmtpServerAddress());
            emailRequest.setSubject(emailTemplate.getSubject());
            emailRequest.setEmailBody(emailTemplate.getTemplate());

            for (String key : mapBody.keySet()) {
                emailRequest.setEmailBody(emailRequest.getEmailBody().replace(key,
                        mapBody.get(key) == null ? "" : mapBody.get(key).toString()));
            }

            saveAndSendEmail(emailRequest);
        } catch (Exception e) {
            log.error("Exception in send SaasId Process Completed Mail  ", e);
        }
        log.info("Sending SaasId Process Completed Mail Finished............");
    }

    private void saveAndSendEmail(CommonEmailRequest emailRequest) {

        try {

            ValidateUtil.notNull(emailRequest.getFromEmailId(), ErrorCode.EMAIL_FROM_NOTNULL);
            ValidateUtil.notNull(emailRequest.getFromEmailIdPassword(), ErrorCode.EMAIL_FROM_NOTNULL);
            ValidateUtil.notNull(emailRequest.getSubject(), ErrorCode.EMAIL_SUBJECT_NOTNULL);
            ValidateUtil.notNull(emailRequest.getEmailBody(), ErrorCode.EMAIL_BODY_NOTNULL);
            ValidateUtil.notNull(emailRequest.getSmtpServerAddress(), ErrorCode.EMAIL_SERVER_ADDRESS_REQUIRED);
            ValidateUtil.notNull(emailRequest.getPort(), ErrorCode.EMAIL_SERVER_PORT_REQUIRED);

            log.info("Saving Email Request Object......");
            emailRequest = commonEmailRequestRepository.save(emailRequest);

            if (emailRequest.getId() != null) {
                commonAsyncMailer.sendEmail(emailRequest);
            } else {
                log.error("Email Not send because of could not save or invalid");
            }
        } catch (Exception e) {
            log.error("Validation Excepiont in email request", e);
        }
    }

    public void sendEmailToSalesForRatesUpdate(EmailRequest emailRequest, Map<String, byte[]> logoMap) {
        try {

            Session session = setPropertiesAndGetSession(emailRequest);

            // creates a new e-mail message
            Message msg = new MimeMessage(session);

            msg.setFrom(new InternetAddress(emailRequest.getFromEmailId()));
            if (emailRequest.getToEmailIdList() != null) {
                msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(emailRequest.getToEmailIdList()));
            }
            msg.setSubject(emailRequest.getSubject());
            msg.setSentDate(new Date());

            if (emailRequest.getCcEmailIdList() != null) {
                log.info("sending email as cc to  : " + emailRequest.getCcEmailIdList());
                msg.addRecipients(Message.RecipientType.CC, InternetAddress.parse(emailRequest.getCcEmailIdList()));
            }
            if (emailRequest.getBccEmailIdList() != null) {
                log.info("sending email as Bcc to  : " + emailRequest.getBccEmailIdList());
                msg.addRecipients(Message.RecipientType.BCC, InternetAddress.parse(emailRequest.getBccEmailIdList()));
            }

            // creates message part
            MimeBodyPart messageBodyPart = new MimeBodyPart();
            messageBodyPart.setContent(emailRequest.getEmailBody(), "text/html");

            // creates multi-part
            Multipart multipart = new MimeMultipart();
            multipart.addBodyPart(messageBodyPart);

            if (logoMap != null && logoMap.size() != 0) {

                for (String contentId : logoMap.keySet()) {

                    if (logoMap.get(contentId) == null) {
                        continue;
                    }
                    if (contentId.equals("#COMPANY_LOGO#")) {
                        BodyPart tmp = new MimeBodyPart();
                        BufferedDataSource fds = new BufferedDataSource(logoMap.get(contentId), "companylogo.png");
                        // DataSource fds = new
                        // FileDataSource("/home/administrator/Desktop/FSL.png");
                        tmp.setDataHandler(new DataHandler(fds));
                        tmp.setHeader("Content-ID", "<companylogo>");
                        multipart.addBodyPart(tmp);
                    } else if (contentId.equals("#POWERED_BY#")) {
                        BodyPart tmp = new MimeBodyPart();
                        BufferedDataSource fds = new BufferedDataSource(logoMap.get(contentId), "poweredby.png");
                        // DataSource fds = new
                        // FileDataSource("/home/administrator/Desktop/FSL.png");
                        tmp.setDataHandler(new DataHandler(fds));
                        tmp.setHeader("Content-ID", "<poweredby>");
                        multipart.addBodyPart(tmp);
                    }

                }
            }
            msg.setContent(multipart);
            log.info("Mail is sent to : " + emailRequest.getToEmailIdList());
            Transport.send(msg);
            emailRequest.setEmailStatus(EmailStatus.SUCCESS);
        } catch (MessagingException e) {
            log.error("Error in sending Email : " + emailRequest.getFromEmailId() + ", "
                    + emailRequest.getFromEmailIdPassword(), e);
            emailRequest.setEmailStatus(EmailStatus.FAILURE);

            emailRequest.setErrorDesc(ErrorCode.ERROR_IN_EMAIL_SENDING);
        } catch (Exception e) {
            log.error("Exception in sending Email : " + emailRequest.getFromEmailId() + ", "
                    + emailRequest.getFromEmailIdPassword(), e);
            emailRequest.setEmailStatus(EmailStatus.FAILURE);

            emailRequest.setErrorDesc(ErrorCode.ERROR_IN_EMAIL_SENDING);
        }
    }

}
