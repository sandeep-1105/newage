package com.efreightsuite.common.component;

import com.efreightsuite.model.HttpMessage;
import com.efreightsuite.repository.HttpMessageRepository;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * ClassName Purpose CreatedBy/ModifiedBy Date Version HttpMessageComponent :
 * Class handles insert and update operation on HttpMessage : Devendracahry M
 * 26-02-15 0.0
 */

@Service
@Log4j2
public class HttpMessageComponent {

    /*
     * HttpMessageRepository autowired
     */
    @Autowired
    private
    HttpMessageRepository httpMessageRepository;

    /**
     * create - This method creates a new HttpMessage
     *
     * @Param HttpMessage entity
     * @Return HttpMessage
     */

    public HttpMessage create(HttpMessage httpRequest) {

        log.info("HttpMessageComponent.create method start");

        HttpMessage httpRequestResponse = new HttpMessage();

        try {

            httpRequestResponse = httpMessageRepository.save(httpRequest);
            log.info("HttpMessage Saved successfully.");

        } catch (Exception e) {
            log.error("Exception Occured while saving the HttpMessage in HttpMessageComponent.create ", e);
            return null;
        }

        log.info("HttpMessageComponent.create method end");
        return httpRequestResponse;

    }
    /*
	 * Update- This method updates a existing HttpMessage
	 * 
	 * @Param HttpMessage entity
	 */

    public void update(HttpMessage existingHttpMessage) {

        log.info("HttpMessageComponent.update method start");

        HttpMessage httpRequestUpdateResponse = new HttpMessage();

        try {

            httpRequestUpdateResponse = httpMessageRepository.save(existingHttpMessage);

            log.info("HttpMessage  updated successfully...");

        } catch (Exception e) {
            log.error("Exception occured while updating the HttpMessage in HttpMessageComponent.update method: ", e);
            return;
        }

        log.info("HttpMessageComponent.update method end");

    }

    /**
     * findById - This method gets the HttpMessage by given ID
     *
     * @Param HttpMessage entity
     * @Return HttpMessage
     */

    public HttpMessage findById(Long httpMessageId) {

        log.info("HttpMessageComponent.findById method start");
        HttpMessage httpMessage = null;
        try {

            httpMessage = httpMessageRepository.findById(httpMessageId);

            log.info("HttpMessage getting successfull.");

        } catch (Exception e) {
            log.error("Exception Occured while getting the HttpMessage in HttpMessageComponent.findById ", e);
            return null;
        }

        log.info("HttpMessageComponent.findById method end");
        return httpMessage;

    }

}
