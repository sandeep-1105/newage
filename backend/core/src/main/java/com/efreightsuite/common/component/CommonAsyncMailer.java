package com.efreightsuite.common.component;

import java.util.Date;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import com.efreightsuite.model.common.CommonEmailRequest;
import lombok.extern.log4j.Log4j2;

import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

@Log4j2
@Service
public class CommonAsyncMailer {

    private Session setPropertiesAndGetSession(CommonEmailRequest emailRequest) {
        Properties props = new Properties();
        props.put("mail.smtp.host", emailRequest.getSmtpServerAddress());
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.port", emailRequest.getPort());

        return Session.getInstance(props, new javax.mail.Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(emailRequest.getFromEmailId(), emailRequest.getFromEmailIdPassword());
            }
        });
    }

    @Async
    public void sendEmail(CommonEmailRequest emailRequest) {

        try {

            Session session = setPropertiesAndGetSession(emailRequest);
            Message msg = new MimeMessage(session);
            msg.setFrom(new InternetAddress(emailRequest.getFromEmailId()));
            if (emailRequest.getToEmailIdList() != null) {
                msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(emailRequest.getToEmailIdList()));
            }
            msg.setSubject(emailRequest.getSubject());
            msg.setSentDate(new Date());

            if (emailRequest.getCcEmailIdList() != null) {
                log.info("sending email as cc to  : " + emailRequest.getCcEmailIdList());
                msg.addRecipients(Message.RecipientType.CC, InternetAddress.parse(emailRequest.getCcEmailIdList()));
            }
            if (emailRequest.getBccEmailIdList() != null) {
                log.info("sending email as Bcc to  : " + emailRequest.getBccEmailIdList());
                msg.addRecipients(Message.RecipientType.BCC, InternetAddress.parse(emailRequest.getBccEmailIdList()));
            }

            // creates message part
            MimeBodyPart messageBodyPart = new MimeBodyPart();
            messageBodyPart.setContent(emailRequest.getEmailBody(), "text/html");

            // creates multi-part
            Multipart multipart = new MimeMultipart();
            multipart.addBodyPart(messageBodyPart);
            msg.setContent(multipart);

            Transport.send(msg);
            log.info("Email Message Sent......");
        } catch (MessagingException e) {
            log.error("Error in sending Email : " + emailRequest.getFromEmailId() + ", "
                    + emailRequest.getFromEmailIdPassword(), e);
        } catch (Exception e) {
            log.error("Exception in sending Email : " + emailRequest.getFromEmailId() + ", "
                    + emailRequest.getFromEmailIdPassword(), e);
        }

    }

}
