package com.efreightsuite;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManagerFactory;

import com.efreightsuite.common.component.HttpMessageComponent;
import com.efreightsuite.security.HttpMessageFilter;
import com.efreightsuite.security.NewAgeFilter;
import com.efreightsuite.security.TimeTrackingFilter;
import com.efreightsuite.support.ApplicationPropertiesBindingPostProcessor;
import com.efreightsuite.util.TrackIdGenerator;
import lombok.extern.log4j.Log4j2;

import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.boot.web.support.ErrorPageFilter;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ImportResource;
import org.springframework.context.annotation.Profile;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.session.HttpSessionEventPublisher;
import org.springframework.session.ExpiringSession;
import org.springframework.session.MapSessionRepository;
import org.springframework.session.SessionRepository;
import org.springframework.session.config.annotation.web.http.EnableSpringHttpSession;
import org.springframework.session.data.redis.config.annotation.web.http.EnableRedisHttpSession;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@SpringBootApplication
@ImportResource("classpath:security.xml")
@Log4j2
@EnableCaching
@EnableAsync
@EnableScheduling
@EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true, jsr250Enabled = true)
public class EfreightsuiteApplication {

    @Value("${queue.name}")
    String queueName;

    @Value("${queue.concurrentuser}")
    int concurrentUsers;

    @Value("${queue.maxconcurrent.user}")
    int maxConcurrentUsers;

    @Value("${request.response.track.required}")
    boolean requestResponseTrackRequired;

    @Value("${url.to.track.api.execution.time}")
    String urlToTrack;

    public static void main(String[] args) {
        log.info("Application started");
        SpringApplication.run(EfreightsuiteApplication.class, args);
    }

    @Bean
    public ApplicationPropertiesBindingPostProcessor applicationPropertiesBindingPostProcessor() {
        return new ApplicationPropertiesBindingPostProcessor();
    }

    @Bean
    public WebMvcConfigurer corsConfigurer() {
        return new WebMvcConfigurerAdapter() {
            @Override
            public void addCorsMappings(CorsRegistry registry) {
                registry.addMapping("/**")
                        .allowedOrigins("http://localhost:3000")
                        .allowedHeaders("*")
                        .allowedMethods("*");
            }
        };
    }

    @Bean
    JpaTransactionManager transactionManager(EntityManagerFactory entityManagerFactory) {
        JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(entityManagerFactory);
        return transactionManager;
    }

    @Bean
    public ErrorPageFilter errorPageFilter() {
        return new ErrorPageFilter();
    }

    @Bean
    public FilterRegistrationBean disableSpringBootErrorFilter(ErrorPageFilter filter) {
        FilterRegistrationBean filterRegistrationBean = new FilterRegistrationBean();
        filterRegistrationBean.setFilter(filter);
        filterRegistrationBean.setEnabled(false);
        return filterRegistrationBean;
    }

    @Bean
    public FilterRegistrationBean authorizationFilter() {

        NewAgeFilter newAgeFilter = new NewAgeFilter();

        newAgeFilter.setTrackIdGenerator(getTrackIdGenerator());

        FilterRegistrationBean filterRegBean = new FilterRegistrationBean();
        filterRegBean.setFilter(newAgeFilter);
        List<String> urlPatterns = new ArrayList<>();
        urlPatterns.add("/*");
        filterRegBean.setUrlPatterns(urlPatterns);

        return filterRegBean;
    }

    @Bean
    public TrackIdGenerator getTrackIdGenerator() {
        return new TrackIdGenerator();
    }

    @Bean
    public HttpSessionEventPublisher httpSessionEventPublisher() {
        return new HttpSessionEventPublisher();
    }

    @Bean
    @Profile("!test")
    Queue queue() {
        return new Queue(queueName, false);
    }

    @Bean
    MessageConverter messageConverter() {
        return new Jackson2JsonMessageConverter();
    }

    @Bean
    @Profile("!test")
    RabbitTemplate template(ConnectionFactory connectionFactory, MessageConverter converter) {
        RabbitTemplate template = new RabbitTemplate(connectionFactory);
        template.setMessageConverter(converter);
        return template;
    }

    @Bean
    public ThreadPoolTaskExecutor taskExecutor() {
        ThreadPoolTaskExecutor pool = new ThreadPoolTaskExecutor();
        pool.setCorePoolSize(1);
        pool.setMaxPoolSize(10);
        pool.setWaitForTasksToCompleteOnShutdown(true);
        return pool;
    }

    @Bean
    public FilterRegistrationBean timeTrackingFilter() {
        TimeTrackingFilter filter = new TimeTrackingFilter();
        FilterRegistrationBean filterRegistrationBean = new FilterRegistrationBean();
        filterRegistrationBean.setFilter(filter);

        if (urlToTrack != null) {
            List<String> urlPatterns = new ArrayList<>();
            urlPatterns.add(urlToTrack);
            filterRegistrationBean.setUrlPatterns(urlPatterns);
        } else {
            log.info("URL to track the time is not given in property file....");
        }

        return filterRegistrationBean;
    }


    @Bean
    public FilterRegistrationBean loggingFilter() {

        HttpMessageFilter loggerFilter = new HttpMessageFilter();
        loggerFilter.setFilterExecute(requestResponseTrackRequired);
        loggerFilter.setHttpMessageComponent(getHttpMessageComponent());
        FilterRegistrationBean filterRegBean = new FilterRegistrationBean();
        filterRegBean.setFilter(loggerFilter);
        List<String> urlPatterns = new ArrayList<>();
        urlPatterns.add("/*");
        filterRegBean.setUrlPatterns(urlPatterns);

        return filterRegBean;
    }

    @Bean
    public HttpMessageComponent getHttpMessageComponent() {
        return new HttpMessageComponent();
    }

    @Profile("!test")
    @EnableRedisHttpSession
    public static class RedisSessionConfig {
    }

    @Profile("test")
    @EnableSpringHttpSession
    public static class MapSessionConfig {
        @Bean
        public SessionRepository<ExpiringSession> sessionRepository() {
            return new MapSessionRepository();
        }
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
}
