package com.efreightsuite.util;

import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;

import lombok.extern.log4j.Log4j2;

/**
 * A utility class that encrypts or decrypts a file.
 *
 * @author www.codejava.net
 */
@Log4j2
public class CryptoUtils {
    private static final String ALGORITHM = "AES";
    private static final String TRANSFORMATION = "AES";

    public static byte[] encrypt(String key, byte[] inputByte)
            throws CryptoException {
        return doCrypto(Cipher.ENCRYPT_MODE, key, inputByte);
    }

    public static byte[] decrypt(String key, byte[] inputByte)
            throws CryptoException {
        return doCrypto(Cipher.DECRYPT_MODE, key, inputByte);
    }

    private static byte[] doCrypto(int cipherMode, String key, byte[] inputByte) throws CryptoException {
        try {
            Key secretKey = new SecretKeySpec(key.getBytes(), ALGORITHM);
            Cipher cipher = Cipher.getInstance(TRANSFORMATION);
            cipher.init(cipherMode, secretKey);


            return cipher.doFinal(inputByte);
        } catch (NoSuchPaddingException | NoSuchAlgorithmException
                | InvalidKeyException | BadPaddingException
                | IllegalBlockSizeException ex) {
            throw new CryptoException("Error encrypting/decrypting file", ex);
        }
    }
}
