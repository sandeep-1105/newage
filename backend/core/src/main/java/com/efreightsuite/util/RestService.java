package com.efreightsuite.util;

import com.efreightsuite.dto.ReportDownloadRequestDto;
import com.efreightsuite.model.UserProfile;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
@Log4j2
public class RestService {

    @Value("${efs.report.application.base.url}")
    private
    String reportServerBasePath;

    @Autowired
    AppUtil appUtil;


    public byte[] getPdfReportByteArray(ReportDownloadRequestDto data, UserProfile userProfile, String saasId) {
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders requestHeaders = new HttpHeaders();
        requestHeaders.add("Cookie", "SESSION=" + userProfile.getSessionId());
        requestHeaders.add("SAAS_ID", saasId.toUpperCase());

        HttpEntity requestEntity = new HttpEntity(data, requestHeaders);
        ResponseEntity responseEntity = restTemplate.exchange(reportServerBasePath + "/api/v1/report/download",
                HttpMethod.POST, requestEntity, byte[].class);
        byte[] response = (byte[]) responseEntity.getBody();
        log.info("Gettin Response :: " + response);
        return response;
    }

}
