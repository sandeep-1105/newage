package com.efreightsuite.util;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.efreightsuite.enumeration.UnitCalculationType;
import com.efreightsuite.report.service.AutoAttachService;
import com.efreightsuite.repository.ReportMasterRepository;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class ReportUtil {

    @Autowired
    AutoAttachService as;
    @Autowired
    ReportMasterRepository reportMasterRepository;


    //Date format Util Func
    public String dateToReportDate(Date date, String dateFormatString) {
        log.info("In dateToReportDate FUN input parameter date - " + date + " ; DateFormat -- " + dateFormatString);
        DateFormat dateFormat = null;
        if (date != null) {
            try {
                if (dateFormatString == null || dateFormatString.length() == 0) {
                    log.info("Request Date format is null So default date format 'dd-MMM-yy' is assigned");
                    dateFormat = new SimpleDateFormat("dd-MMM-yy");
                } else {
                    dateFormat = new SimpleDateFormat(dateFormatString);
                }
                return dateFormat.format(date).toUpperCase();
            } catch (Exception ex) {
                log.error("Exception on converting date --- ", ex);
                ex.printStackTrace();
            }
        }
        return "";
    }


    @SuppressWarnings("static-access")
    public Double mappingUnitCalculate(UnitCalculationType calculationType, Long calculationValue, Double actualValue) {

        Double retvalue = 1.0;

        if (calculationType.equals(calculationType.ADDITION)) {
            retvalue = actualValue + calculationValue;
        } else if (calculationType.equals(calculationType.SUBTRACTION)) {
            retvalue = actualValue - calculationValue;
        } else if (calculationType.equals(calculationType.MULTIPLICATION)) {
            retvalue = actualValue * calculationValue;
        } else if (calculationType.equals(calculationType.DIVISION)) {
            retvalue = actualValue / calculationValue;
        }
        return retvalue;

    }

    public String twoDecimalDoubleValueFormat(Double value) {
        DecimalFormat twoDecFormat = new DecimalFormat("#0.00#");
        return twoDecFormat.format(value);
    }


}
