package com.efreightsuite.util;

import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.efreightsuite.enumeration.EmploymentStatus;
import com.efreightsuite.enumeration.EventMasterType;
import com.efreightsuite.enumeration.LovStatus;
import com.efreightsuite.enumeration.TransportMode;
import com.efreightsuite.enumeration.YesNo;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.model.PartyMaster;
import com.efreightsuite.model.ShipmentServiceDetail;
import com.efreightsuite.model.ShipmentServiceEvent;
import lombok.extern.log4j.Log4j2;

/**
 * The Class Validate.
 */

@Log4j2
public class ValidateUtil {

    public static final String regularExp = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$";

    public static <E> void checkPattern(E object, String parameterName, boolean checkPattern) throws RestException {
        if (checkPattern) {
            checkPattern(object, parameterName);
        }
    }

    public static void notNull(Object object, String errorCode) throws RestException {
        if (object == null) {
            throw new RestException(errorCode);
        }
    }

    public static void isStringEquals(String value1, String value2, String errorCode) throws RestException {
        if (value1 == null || value2 == null || !value1.trim().equals(value2.trim())) {
            throw new RestException(errorCode);
        }
    }

    public static void notEquals(Object src, Object target, String errorCode) throws RestException {
        if (!src.equals(target)) {
            throw new RestException(errorCode);
        }
    }


    public static void assertTrue(boolean flag, String errorCode) throws RestException {
        if (!flag)
            throw new RestException(errorCode);
    }

    private static <E> void checkPattern(E object, String parameterName) throws RestException {
        if (object == null) {
            return;
        }
        String[] patternAndErrorMessage = ParameterFormat.getPatternAndErrorMessage(parameterName);

        if (patternAndErrorMessage == null) {
            return;
        }
        String parameterPattern = patternAndErrorMessage[0];
        String errorMessage = patternAndErrorMessage[1];
        Pattern pattern = Pattern.compile(parameterPattern);
        Matcher matcher = pattern.matcher(object.toString());
        boolean matchFound = matcher.matches();
        if (!matchFound) {
            throw new RestException(errorMessage);
        }
    }

    public static <E> void checkPattern(E object, String regExp, String errorCode) throws RestException {
        if (object == null) {
            return;
        }

        if (regExp == null) {
            return;
        }

        Pattern pattern = Pattern.compile(regExp);
        Matcher matcher = pattern.matcher(object.toString());
        boolean matchFound = matcher.matches();
        if (!matchFound) {
            throw new RestException(errorCode);
        }
    }

    public static void notEmpty(@SuppressWarnings("rawtypes") Collection list, String errorCode) throws RestException {
        if (list == null || list.size() == 0) {
            throw new RestException(errorCode);
        }
    }

    public static void notNullOrEmpty(String object, String errorCode) throws RestException {
        if (object == null || object.trim().length() == 0) {
            throw new RestException(errorCode);
        }
    }


    public static void validateLength(Object object, int min, int max, String errorCode) throws RestException {

        if (object != null) {
            if (object.toString().length() > max) {
                throw new RestException(errorCode);
            }

            if (object.toString().length() < min) {
                throw new RestException(errorCode);
            }
        }
    }


    public static void validateEnum(Class<?> enumType, Object object, String errorCode) throws RestException {
        boolean flag = false;

        if (object != null) {
            for (Object obj : enumType.getEnumConstants()) {
                if (obj.equals(object)) {
                    flag = true;
                    break;
                }
            }
        }
        if (!flag) {
            throw new RestException(errorCode);
        }
    }

    public static void checkDate(Date input, String errorCode) throws RestException {
        Date date = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        if (!sdf.format(date).equals(sdf.format(input))) {
            throw new RestException(errorCode);
        }

    }

    public static void dateNotNull(Date date, String errorCode) throws RestException {
        if (date == null) {
            throw new RestException(errorCode);
        }
    }

    public static void dateEmpty(Date date, String errorCode) throws RestException {
        if (date.toString().equals("")) {
            throw new RestException(errorCode);
        }
    }

    public static void checkDecimalLength(Double value, String errorCode) {
        String text = Double.toString(Math.abs(value));
        int integerPlaces = text.indexOf('.');
        int decimalPlaces = text.length() - integerPlaces - 1;
        if (integerPlaces > 5 || decimalPlaces > 6) {
            throw new RestException(errorCode);
        }
    }

    public static void nonZero(Double value, String errorCode) throws RestException {
        if (value == 0) {
            throw new RestException(errorCode);
        }
    }

    public static void nonZero(Long value, String errorCode) throws RestException {
        if (value == 0) {
            throw new RestException(errorCode);
        }
    }

    public static void nonZero(int value, String errorCode) throws RestException {
        if (value == 0) {
            throw new RestException(errorCode);
        }
    }

    public static void isValidEmail(String enteredEmail, String errorCode) throws RestException {
        log.info("enteredEmail:" + enteredEmail);
        String EMAIL_REGIX = "^[\\\\w!#$%&’*+/=?`{|}~^-]+(?:\\\\.[\\\\w!#$%&’*+/=?`{|}~^-]+)*@(?:[a-zA-Z0-9-]+\\\\.)+[a-zA-Z]{2,6}$";
        Pattern pattern = Pattern.compile(EMAIL_REGIX);
        Matcher matcher = pattern.matcher(enteredEmail);
        if (!enteredEmail.isEmpty() && enteredEmail != null && matcher.matches()) {
            throw new RestException(errorCode);
        }
    }


    public static void isDefaulter(PartyMaster partyMaster, String errorCode) {
        if (partyMaster.getIsDefaulter() != null && partyMaster.getIsDefaulter().equals(YesNo.Yes)) {
            throw new RestException(errorCode);
        }
    }

    public static void isStatusBlocked(LovStatus status, String errorCode) {
        if (status.equals(LovStatus.Block)) {
            log.info("Status is Blocked. Throwing error code : " + errorCode);
            throw new RestException(errorCode);
        }
    }

    public static void isStatusHidden(LovStatus status, String errorCode) {
        if (status.equals(LovStatus.Hide)) {
            log.info("Status is Hidden. Throwing error code : " + errorCode);
            throw new RestException(errorCode);
        }
    }

    public static void isAirTransportMode(TransportMode transportMode, String errorCode) {

        log.info(" Air Transport. transportMode : " + transportMode);
        if (!transportMode.equals(TransportMode.Air)) {
            log.info("Not Air Transport. Throwing error code : " + errorCode);
            throw new RestException(errorCode);
        }
    }

    public static void isNotEqualTransportMode(TransportMode transportMode, TransportMode serviceTransportMode, String errorCode) {

        if (!transportMode.equals(serviceTransportMode)) {
            log.info("Invaild isNotEqualTransportMode. Throwing error code : " + errorCode);
            throw new RestException(errorCode);
        }
    }


    public static void isEmployeeResigned(EmploymentStatus employementStatus, String errorCode) {
        if (employementStatus.equals(EmploymentStatus.RESIGNED)) {
            throw new RestException(errorCode);
        }
    }

    public static void isEmployeeTerminated(EmploymentStatus employementStatus, String errorCode) {
        if (employementStatus.equals(EmploymentStatus.TERMINATED)) {
            throw new RestException(errorCode);
        }
    }

    public static void isSalesman(YesNo isSalesman, String errorCode) {
        if (isSalesman.equals(YesNo.No)) {
            throw new RestException(errorCode);
        }
    }

    public static void belowRange(double value, double minValue, String errorCode) {
        if (value < minValue) {
            throw new RestException(errorCode);
        }
    }

    public static void aboveRange(double value, double maxValue, String errorCode) {
        if (value > maxValue) {
            throw new RestException(errorCode);
        }
    }

    public static void decimalLength(double object, int min, int max, String errorCode) {

        if (object > max) {

            throw new RestException(errorCode);
        }
        if (object < min) {

            throw new RestException(errorCode);
        }

    }


    public static Boolean isCargoRecived(ShipmentServiceDetail service) {

        boolean isReceived = false;

        if (service != null && service.getId() != null && service.getEventList() != null && service.getEventList().size() > 0) {

            for (ShipmentServiceEvent shipmentServiceEvent : service.getEventList()) {

                if (shipmentServiceEvent.getEventMaster() != null && shipmentServiceEvent.getEventMaster().getEventMasterType() != null && ((shipmentServiceEvent.getEventMaster().getEventMasterType() == EventMasterType.CARGO_RECEIVED) &&
                        (shipmentServiceEvent.getEventMaster().getEventMasterType().equals(EventMasterType.CARGO_RECEIVED)))) {
                    isReceived = true;
                    break;
                }
            }
        }
        return isReceived;
    }


	/*public static void checkDecimalPattern(double object, int intvalue, int decimalvalue,String errorCode) {
	
		int integerLength = (int) Math.log10((int)object) + 1; 
		
		if(integerLength>intvalue){
			
			throw new RestException(errorCode);
		}
		
		String temp = "." + String.valueOf(object).split( "\\." )[1];
		double decimalPart = Double.parseDouble( temp );
		
		int decimalLength = (int) Math.log10((int)decimalPart) + 1; 
		
      if(decimalLength>decimalvalue){
			
			throw new RestException(errorCode);
		}
		
	}*/

}
