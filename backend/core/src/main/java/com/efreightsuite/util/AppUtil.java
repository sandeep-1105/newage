package com.efreightsuite.util;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import com.efreightsuite.common.component.CacheRepository;
import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.enumeration.*;
import com.efreightsuite.model.Consol;
import com.efreightsuite.model.ConsolCharge;
import com.efreightsuite.model.DefaultMasterData;
import com.efreightsuite.model.DocumentDetail;
import com.efreightsuite.model.LocationMaster;
import com.efreightsuite.model.PartyAddressMaster;
import com.efreightsuite.model.PartyAssociateToPartyTypeMaster;
import com.efreightsuite.model.PartyMaster;
import com.efreightsuite.model.PartyServiceMaster;
import com.efreightsuite.model.ShipmentCharge;
import com.efreightsuite.model.ShipmentLink;
import com.efreightsuite.model.ShipmentServiceDetail;
import com.efreightsuite.repository.DefaultMasterDataRepository;
import lombok.extern.log4j.Log4j2;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class AppUtil {

    @Autowired
    private
    CacheRepository cacheRepository;

    @Autowired
    private
    DefaultMasterDataRepository defaultMasterDataRepository;

    public static void setPartyToNull(PartyMaster partyMaster) {
        if (partyMaster != null) {
            if (partyMaster.getPartyAddressList() != null && !partyMaster.getPartyAddressList().isEmpty()) {
                for (PartyAddressMaster pam : partyMaster.getPartyAddressList()) {
                    pam.setPartyMaster(null);
                }
            } else {
                partyMaster.setPartyAddressList(null);
            }

            if (partyMaster.getPartyTypeList() != null && !partyMaster.getPartyTypeList().isEmpty()) {
                for (PartyAssociateToPartyTypeMaster pam : partyMaster.getPartyTypeList()) {
                    pam.setPartyMaster(null);
                }
            } else {
                partyMaster.setPartyTypeList(null);
            }

            if (partyMaster.getPartyServiceList() != null && !partyMaster.getPartyServiceList().isEmpty()) {
                for (PartyServiceMaster pam : partyMaster.getPartyServiceList()) {
                    pam.setPartyMaster(null);
                }
            } else {
                partyMaster.setPartyServiceList(null);
            }

            partyMaster.setPartyBusinessDetailList(null);
            partyMaster.setPartyAccountList(null);
            partyMaster.setPartyCreditLimitList(null);
            partyMaster.setPartyEmailMappingList(null);
            partyMaster.setPartyCompanyAssociateList(null);
            partyMaster.setContactList(null);
        }
    }

    public static void setPartyListToNull(List<PartyMaster> partyList) {

        for (PartyMaster partyMaster : partyList) {
            setPartyToNull(partyMaster);
        }

    }

    public static void setService(ShipmentServiceDetail service) {

        if (service == null) {
            return;
        }

        if (service.getLocation() != null && service.getLocation().getPartyMaster() != null) {
            setPartyToNull(service.getLocation().getPartyMaster());
        }


        setPartyToNull(service.getParty());
        setPartyToNull(service.getAgent());
        setPartyToNull(service.getCoLoader());
        setPartyToNull(service.getBrokerageParty());

        if (service.getCarrier() != null) {
            service.getCarrier().setAddressList(null);
            service.getCarrier().setEdiList(null);
        }

        if (service.getDocumentList() != null) {
            for (DocumentDetail dd : service.getDocumentList()) {
                if (dd.getLocation() != null && dd.getLocation().getPartyMaster() != null) {
                    setPartyToNull(dd.getLocation().getPartyMaster());
                }
                setPartyToNull(dd.getShipper());
                setPartyToNull(dd.getConsignee());
                setPartyToNull(dd.getFirstNotify());
                setPartyToNull(dd.getSecondNotify());
                setPartyToNull(dd.getForwarder());
                setPartyToNull(dd.getAgent());
                setPartyToNull(dd.getIssuingAgent());
                setPartyToNull(dd.getChaAgent());
                if (dd.getCarrier() != null) {
                    dd.getCarrier().setAddressList(null);
                    dd.getCarrier().setEdiList(null);
                }
            }
        }


        if (service.getPickUpDeliveryPoint() != null) {
            setPartyToNull(service.getPickUpDeliveryPoint().getTransporter());
            setPartyToNull(service.getPickUpDeliveryPoint().getPickupPoint());
        }
        if (service.getShipmentChargeList() != null && !service.getShipmentChargeList().isEmpty()) {
            for (ShipmentCharge dd : service.getShipmentChargeList()) {
                setPartyToNull(dd.getVendor());
                if (dd.getLocation() != null) {
                    setPartyToNull(dd.getLocation().getPartyMaster());
                }
            }

        }


    }

    public static void setMaster(Consol consol) {

        if (consol.getCreatedBy() != null) {
            consol.getCreatedBy().setLocationMaster(null);
        }

        setPartyToNull(consol.getLocation().getPartyMaster());
        setPartyToNull(consol.getAgent());
        if (consol.getCarrier() != null) {
            consol.getCarrier().setAddressList(null);
            consol.getCarrier().setEdiList(null);
        }
        setPartyToNull(consol.getCarrierAgent());
        setPartyToNull(consol.getCoLoader());
        setPartyToNull(consol.getDocumentIssuingAgent());
        setPartyToNull(consol.getFirstNotify());
        setPartyToNull(consol.getHandlingAgent());
        setPartyToNull(consol.getRoutedAgent());


        if (consol.getConsolDocument() != null) {
            if (consol.getConsolDocument().getLocation() != null && consol.getConsolDocument().getLocation().getPartyMaster() != null) {
                setPartyToNull(consol.getConsolDocument().getLocation().getPartyMaster());
            }
            setPartyToNull(consol.getConsolDocument().getShipper());
            setPartyToNull(consol.getConsolDocument().getConsignee());
            setPartyToNull(consol.getConsolDocument().getFirstNotify());
            setPartyToNull(consol.getConsolDocument().getSecondNotify());
            setPartyToNull(consol.getConsolDocument().getForwarder());
            setPartyToNull(consol.getConsolDocument().getAgent());
            setPartyToNull(consol.getConsolDocument().getIssuingAgent());
            if (consol.getConsolDocument().getLocation() != null) {
                setPartyToNull(consol.getConsolDocument().getLocation().getPartyMaster());
            }
        }

        if (consol.getPickUpDeliveryPoint() != null) {
            setPartyToNull(consol.getPickUpDeliveryPoint().getTransporter());
            setPartyToNull(consol.getPickUpDeliveryPoint().getPickupPoint());
        }

        if (consol.getChargeList() != null && !consol.getChargeList().isEmpty()) {
            for (ConsolCharge dd : consol.getChargeList()) {
                if (dd.getLocation() != null) {
                    setPartyToNull(dd.getLocation().getPartyMaster());
                }

            }
        }

        if (consol.getShipmentLinkList() != null && !consol.getShipmentLinkList().isEmpty()) {
            for (ShipmentLink dd : consol.getShipmentLinkList()) {
                setPartyToNull(dd.getLocation().getPartyMaster());
                setService(dd.getService());
            }
        }

    }

    public static String getDateTimeZone(Date date, String timeZone) {

        String tmpObj = null;

        try {

            if (date != null) {

                TimeZone zone = TimeZone.getTimeZone(timeZone);

                if (zone.useDaylightTime()) {
                    tmpObj = zone.getDisplayName(zone.inDaylightTime(date), TimeZone.SHORT);
                } else {
                    tmpObj = zone.getDisplayName(false, TimeZone.SHORT);
                }
            }

        } catch (Exception e) {
            log.error("Time Zone error ", e);
        }

        return tmpObj;
    }

    public String getLocationConfig(String key, LocationMaster location, boolean upperCase) {

        DefaultMasterData data = null;
        if (location != null)
            data = defaultMasterDataRepository.getCodeAndLocation(key, location.getId());
        if (data == null || data.getValue() == null) {
            return "";
        }

        if (upperCase) {
            return data.getValue().toUpperCase().trim();
        } else {
            return data.getValue().trim();
        }

    }

    public BaseDto setDesCommon(BaseDto dto) {
        dto.setResponseDescription(dto.getResponseCode());
        return dto;
    }

    public BaseDto setDesc(BaseDto dto) {
        log.debug("Response Message being sent to the client : " + dto.getResponseCode() + " --> " + getDesc(dto.getResponseCode()));
        dto.setResponseDescription(getDesc(dto.getResponseCode()));
        return dto;
    }

    public String getDesc(String code) {
        Map<String, String> codeMap = cacheRepository.getNLS(SaaSUtil.getSaaSId(), "English");
        return codeMap.get(code);
    }

    public List<Class> getAllClasses(String pckgname) {
        try {
            List<Class> classes = new ArrayList();
            /*
             * // Get a File object for the package File directory = null; try {
			 * directory = new File(URLDecoder.decode(Thread.currentThread().
			 * getContextClassLoader() .getResource(pckgname.replace('.',
			 * '/')).getFile(), "UTF-8"));
			 *
			 * } catch (NullPointerException x) { log.error("Nullpointer :", x);
			 * throw new ClassNotFoundException(pckgname +
			 * " does not appear to be a valid package"); }
			 *
			 * log.info("ENUM PATH :" + directory.getAbsolutePath());
			 *
			 * if (directory.exists()) { // Get the list of the files contained
			 * in the package String[] files = directory.list(); for (int i = 0;
			 * i < files.length; i++) { // we are only interested in .class
			 * files if (files[i].endsWith(".class")) { // removes the .class
			 * extension classes.add(Class.forName(pckgname + '.' +
			 * files[i].substring(0, files[i].length() - 6))); } } } else {
			 * log.error("Directory does not exist"); throw new
			 * ClassNotFoundException(pckgname +
			 * " does not appear to be a valid package"); }
			 */

            classes.add(AddressType.class);
            classes.add(TransactionType.class);
            classes.add(BillToType.class);
            classes.add(BooleanType.class);
            classes.add(ChargeCalculationType.class);
            classes.add(ChargeType.class);
            classes.add(CorporateNonCorporate.class);
            classes.add(EmailStatus.class);
            classes.add(FullGroupage.class);
            classes.add(ActualChargeable.class);
            classes.add(ImportExport.class);
            classes.add(JobStatus.class);
            classes.add(LovStatus.class);
            classes.add(MessageSendType.class);
            classes.add(PPCC.class);
            classes.add(ServiceCode.class);
            classes.add(ServiceStatus.class);
            classes.add(EmploymentStatus.class);

            classes.add(Status.class);
            classes.add(StatusOfShipment.class);
            classes.add(TermCode.class);
            classes.add(TradeCode.class);
            classes.add(TransitMode.class);
            classes.add(TransportMode.class);
            classes.add(UnitCalculationType.class);
            classes.add(WhoRouted.class);
            classes.add(ReferenceType.class);
            classes.add(ReferenceTypeKey.class);
            classes.add(SessionStatus.class);
            classes.add(UnitType.class);
            classes.add(MappingUnit.class);
            classes.add(UnitDecimals.class);
            classes.add(SequenceType.class);
            classes.add(SequenceFormat.class);
            classes.add(SalutationType.class);
            classes.add(IdType.class);
            classes.add(BondType.class);
            classes.add(FillingType.class);
            classes.add(FillingBy.class);
            classes.add(ConsigneeType.class);
            classes.add(BondActivityCode.class);
            classes.add(QuoteType.class);
            classes.add(Approved.class);
            classes.add(Frequency.class);
            classes.add(MailEventType.class);
            classes.add(TermCode.class);
            classes.add(CurrentPotential.class);
            classes.add(PartyBusinessStatus.class);
            classes.add(TaxExempted.class);
            classes.add(Reason.class);
            classes.add(ConnectionStatus.class);
            classes.add(DUE.class);
            classes.add(CRN.class);
            classes.add(UserStatus.class);
            classes.add(FlightPlanStatus.class);
            classes.add(StockStatus.class);
            classes.add(CustomerAgent.class);
            classes.add(InvoiceCreditNoteStatus.class);
            classes.add(DebitCredit.class);
            classes.add(RateClass.class);
            classes.add(EventMasterType.class);
            classes.add(DocumentType.class);
            classes.add(PersonalEvent.class);
            classes.add(RelationShip.class);
            classes.add(SignOffUnsignOff.class);
            classes.add(DocPrefixDocumentType.class);
            classes.add(GeneralNoteCategory.class);
            classes.add(GeneralNoteSubCategory.class);
            classes.add(AesAction.class);
            classes.add(AesInBondType.class);
            classes.add(OriginOfGoods.class);
            classes.add(RemarkType.class);
            classes.add(YesNo.class);
            classes.add(DateLogic.class);
            classes.add(WhichTransactionDate.class);
            classes.add(EdiMessagingFor.class);
            classes.add(ServiceType.class);
            classes.add(Gender.class);
            classes.add(MaritalStatus.class);
            classes.add(ProbationStatus.class);
            classes.add(ScopeFlag.class);
            classes.add(Priority.class);
            classes.add(ReportName.class);
            classes.add(PageCode.class);
            classes.add(ShipmentMovement.class);
            classes.add(com.efreightsuite.enumeration.Service.class);
            classes.add(SurchargeType.class);
            classes.add(GlHead.class);
            return classes;

        } catch (Exception e) {
            log.error("Exception getting all enum class:", e);
            return null;
        }
    }

    public static Date removeTime(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return cal.getTime();
    }

    public XSSFWorkbook csvToXLSX(byte[] file) {

        InputStream is = null;
        BufferedReader bfReader = null;

        is = new ByteArrayInputStream(file);
        bfReader = new BufferedReader(new InputStreamReader(is));

        try (XSSFWorkbook workBook = new XSSFWorkbook()) {
            XSSFSheet sheet = workBook.createSheet("sheet1");
            String currentLine = null;
            int RowNum = 0;

            while ((currentLine = bfReader.readLine()) != null) {
                String str[] = currentLine.split(",");
                RowNum++;
                XSSFRow currentRow = sheet.createRow(RowNum);
                for (int i = 0; i < str.length; i++) {
                    currentRow.createCell(i).setCellValue(str[i]);
                }
            }

            return workBook;

        } catch (Exception ex) {
            log.error("CSV to XLSX conversion error", ex);
        }

        return null;
    }
}
