package com.efreightsuite.util;

import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import com.efreightsuite.model.LocationMaster;
import com.efreightsuite.model.SystemTrackManual;
import com.efreightsuite.model.UserProfile;
import com.efreightsuite.service.AuthService;
import lombok.extern.log4j.Log4j2;

@Log4j2
public class TimeUtil {


    private TimeUtil() {

    }

    public static Date getCurrentLocationTime() {

        //TODO - We need to change after User profile screen development
        LocationMaster selectedUserLocation = AuthService.getCurrentUser().getSelectedUserLocation();
        if (selectedUserLocation == null) {
            return new Date();
        }
        String timeZone = selectedUserLocation.getTimeZone();
        LocalDateTime ldt = LocalDateTime.now(ZoneId.of(timeZone, ZoneId.SHORT_IDS));
        Instant instant = ldt.atZone(ZoneId.systemDefault()).toInstant();
        return Date.from(instant);

    }

    public static Date getCurrentLocationTime(LocationMaster locationMaster) {
        LocalDateTime ldt = LocalDateTime.now(ZoneId.of(locationMaster.getTimeZone(), ZoneId.SHORT_IDS));
        Instant instant = ldt.atZone(ZoneId.systemDefault()).toInstant();
        return Date.from(instant);
    }

    private static Date getCurrentLocationTime(UserProfile userProfile) {

        Date date = null;
        if (null != userProfile && null != userProfile.getSelectedUserLocation()) {
            String timeZone = userProfile.getSelectedUserLocation().getTimeZone();

            LocalDateTime ldt = LocalDateTime.now(ZoneId.of(timeZone, ZoneId.SHORT_IDS));
            Instant instant = ldt.atZone(ZoneId.systemDefault()).toInstant();
            date = Date.from(instant);
        }
        return date;

    }

/*	public static SystemTrack getCreateSystemTrack() {

		SystemTrack systemTrack = new SystemTrack();

		systemTrack.setCreateDate(getCurrentLocationTime());
		systemTrack.setCreateUser(AuthService.getCurrentUser().getUserName());

		systemTrack.setLastUpdatedDate(getCurrentLocationTime());
		systemTrack.setLastUpdatedUser(AuthService.getCurrentUser().getUserName());

		return systemTrack;
	}

	public static void setUpdateSystemTrack(SystemTrack systemTrack) {

		if (systemTrack == null) {
			systemTrack = new SystemTrack();
			systemTrack.setCreateDate(getCurrentLocationTime());
			systemTrack.setCreateUser(AuthService.getCurrentUser().getUserName());
		}
		systemTrack.setLastUpdatedDate(getCurrentLocationTime());
		systemTrack.setLastUpdatedUser(AuthService.getCurrentUser().getUserName());

	}*/

    public static void setUpdateSystemTrackWithLocation(SystemTrackManual systemTrack, LocationMaster location) {

        if (systemTrack == null) {
            systemTrack = new SystemTrackManual();
            systemTrack.setCreateDate(getCurrentLocationTime(location));
            systemTrack.setCreateUser("admin");
        }
        systemTrack.setLastUpdatedDate(getCurrentLocationTime(location));
        systemTrack.setLastUpdatedUser("admin");

    }

    public static SystemTrackManual getCreateSystemTrackWithLocation(LocationMaster location) {

        SystemTrackManual systemTrack = new SystemTrackManual();

        systemTrack.setCreateDate(getCurrentLocationTime(location));
        systemTrack.setCreateUser("admin");

        systemTrack.setLastUpdatedDate(getCurrentLocationTime(location));
        systemTrack.setLastUpdatedUser("admin");

        return systemTrack;
    }

    public static SystemTrackManual getCreateSystemTrack(UserProfile userProfile) {

        SystemTrackManual systemTrack = new SystemTrackManual();
        if (null != userProfile) {
            systemTrack.setCreateDate(getCurrentLocationTime(userProfile));
            systemTrack.setCreateUser(userProfile.getUserName());

            systemTrack.setLastUpdatedDate(getCurrentLocationTime(userProfile));
            systemTrack.setLastUpdatedUser(userProfile.getUserName());
        }
        return systemTrack;
    }

    public static void setUpdateSystemTrack(SystemTrackManual systemTrack, UserProfile userProfile) {

        if (systemTrack == null) {
            systemTrack = new SystemTrackManual();
            systemTrack.setCreateDate(getCurrentLocationTime(userProfile));
            systemTrack.setCreateUser(userProfile.getUserName());
        }
        systemTrack.setLastUpdatedDate(getCurrentLocationTime(userProfile));
        systemTrack.setLastUpdatedUser(userProfile.getUserName());

    }

    public static Date addMinutesToDate(int minutes, Date beforeTime) {
        final long ONE_MINUTE_IN_MILLIS = 60000;
        long curTimeInMs = beforeTime.getTime();
        return new Date(curTimeInMs + (minutes * ONE_MINUTE_IN_MILLIS));
    }

    public static String getLocationDate(LocationMaster locationMaster, Date date) {
        if (locationMaster != null && locationMaster.getJavaDateFormat() != null) {
            return new SimpleDateFormat(locationMaster.getJavaDateFormat()).format(date);
        } else {
            return new SimpleDateFormat("dd-MMM-yyyy").format(date);
        }
    }

    public static Date addDate(Date date, Integer day) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.DAY_OF_YEAR, day);
        return calendar.getTime();
    }

    public static boolean validateFromAndToDateWithNow(Date fromDate, Date toDate) {

        Date now = TimeUtil.getCurrentLocationTime();
        return (fromDate.equals(now) || fromDate.before(now)) && (toDate.equals(now) || toDate.after(now));

    }


    public static List<String> getAllTimeZones() {
        String[] ids = TimeZone.getAvailableIDs();
        List<String> timeZones = new ArrayList<>();
        for (String id : ids) {
            timeZones.add(TimeZone.getTimeZone(id).getID());
        }
        return timeZones;
    }
}
