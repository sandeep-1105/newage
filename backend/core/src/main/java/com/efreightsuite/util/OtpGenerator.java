package com.efreightsuite.util;

import org.apache.commons.lang3.RandomStringUtils;

public abstract class OtpGenerator {

    public static String generateAlphanumeric(int length) {
        return RandomStringUtils.randomAlphanumeric(length).toUpperCase();

    }

    public static String generateAlphabetic(int length) {
        return RandomStringUtils.randomAlphabetic(length).toUpperCase();
    }

}
