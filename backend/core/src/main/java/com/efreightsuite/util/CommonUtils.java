package com.efreightsuite.util;

import java.util.Set;

import org.apache.commons.lang3.StringUtils;

public class CommonUtils {

    public static String commaSeparatedIds(Set<Long> ids) {
        return StringUtils.join(ids, ",");
    }

}
