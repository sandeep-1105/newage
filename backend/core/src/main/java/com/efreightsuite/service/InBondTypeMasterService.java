package com.efreightsuite.service;

import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.SearchRequest;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.repository.InbondTypeRepository;
import com.efreightsuite.search.InBondTypeMasterSearchImpl;
import com.efreightsuite.util.AppUtil;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class InBondTypeMasterService {


    @Autowired
    private
    AppUtil appUtil;

    @Autowired
    private
    InBondTypeMasterSearchImpl inBondSearchImpl;


    @Autowired
    InbondTypeRepository inbondTypeRepository;

    public BaseDto search(SearchRequest searchRequest) {

        log.info("Search method is called...." + searchRequest);

        BaseDto baseDto = new BaseDto();

        try {

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(inBondSearchImpl.search(searchRequest));

            log.info("Successfully Searching inbond...");

        } catch (Exception exception) {

            log.error("Exception in Search method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);

    }


}
