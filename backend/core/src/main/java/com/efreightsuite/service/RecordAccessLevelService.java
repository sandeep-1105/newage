package com.efreightsuite.service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.RecordAccessLevelDTO;
import com.efreightsuite.dto.RecordAccessLevelProcessDTO;
import com.efreightsuite.dto.RecordAccessLevelSearchDto;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.ExceptionHelper;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.exception.UniqueKey;
import com.efreightsuite.model.RecordAccessLevel;
import com.efreightsuite.model.RecordAccessLevelCountry;
import com.efreightsuite.model.RecordAccessLevelCustomer;
import com.efreightsuite.model.RecordAccessLevelDivision;
import com.efreightsuite.model.RecordAccessLevelLocation;
import com.efreightsuite.model.RecordAccessLevelSalesman;
import com.efreightsuite.model.RecordAccessLevelUser;
import com.efreightsuite.repository.RecordAccessLevelRepository;
import com.efreightsuite.repository.RecordAccessLevelUserRepository;
import com.efreightsuite.search.RecordAccessLevelSearchImpl;
import com.efreightsuite.util.AppUtil;
import com.efreightsuite.validation.RecordAccessLevelValidator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.exception.ExceptionUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.orm.ObjectOptimisticLockingFailureException;
import org.springframework.orm.jpa.JpaObjectRetrievalFailureException;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class RecordAccessLevelService {

    @Autowired
    private
    AppUtil appUtil;

    @Autowired
    private
    RecordAccessLevelRepository recordAccessLevelRepository;

    @Autowired
    private
    RecordAccessLevelSearchImpl recordAccessLevelSearchImpl;

    @Autowired
    private
    RecordAccessLevelValidator recordAccessLevelValidator;

    @Autowired
    private
    RecordAccessLevelUserRepository recordAccessLevelUserRepository;


    public RecordAccessLevelProcessDTO getRecordAccessLevelForAUser(Long userId) {
        log.info("RecordAccessLevelForAUser method is called....." + userId);

        RecordAccessLevelProcessDTO dto = null;
        Set<Long> countryList = new HashSet<>();
        Set<Long> locationList = new HashSet<>();
        Set<Long> divisionList = new HashSet<>();
        Set<Long> serviceList = new HashSet<>();
        Set<Long> salesmanList = new HashSet<>();
        Set<Long> customerList = new HashSet<>();

        try {
            List<RecordAccessLevelUser> userList = recordAccessLevelUserRepository.findAllByUserProfile(userId);

            for (RecordAccessLevelUser user : userList) {
                if (user != null && user.getRecordAccessLevel() != null) {

                    if (user.getRecordAccessLevel().getCountryList() != null) {
                        for (RecordAccessLevelCountry country : user.getRecordAccessLevel().getCountryList()) {
                            if (country != null && country.getCountryMaster() != null) {
                                countryList.add(country.getCountryMaster().getId());
                            }
                        }
                    }


                    if (user.getRecordAccessLevel().getLocationList() != null) {
                        for (RecordAccessLevelLocation location : user.getRecordAccessLevel().getLocationList()) {
                            if (location != null && location.getLocationMaster() != null) {
                                locationList.add(location.getLocationMaster().getId());
                            }
                        }
                    }


                    if (user.getRecordAccessLevel().getDivisionList() != null) {
                        for (RecordAccessLevelDivision division : user.getRecordAccessLevel().getDivisionList()) {
                            if (division != null && division.getDivisionMaster() != null) {
                                divisionList.add(division.getDivisionMaster().getId());
                            }
                        }
                    }


                    if (user.getRecordAccessLevel().getServiceList() != null) {
                        for (com.efreightsuite.model.RecordAccessLevelService service : user.getRecordAccessLevel().getServiceList()) {
                            if (service != null && service.getServiceMaster() != null) {
                                serviceList.add(service.getServiceMaster().getId());
                            }
                        }
                    }


                    if (user.getRecordAccessLevel().getSalesmanList() != null) {
                        for (RecordAccessLevelSalesman salesman : user.getRecordAccessLevel().getSalesmanList()) {
                            if (salesman != null && salesman.getEmployeeMaster() != null) {
                                salesmanList.add(salesman.getEmployeeMaster().getId());
                            }
                        }
                    }

                    if (user.getRecordAccessLevel().getCustomerList() != null) {
                        for (RecordAccessLevelCustomer customer : user.getRecordAccessLevel().getCustomerList()) {
                            if (customer != null && customer.getPartyMaster() != null) {
                                customerList.add(customer.getPartyMaster().getId());
                            }
                        }
                    }

                }
            }

            dto = new RecordAccessLevelProcessDTO(userId, countryList, locationList, divisionList, serviceList, salesmanList, customerList);


        } catch (Exception e) {
            log.error("Exception in RecordAccessLevelService -> recordAccessLevelForAUser method ", e);
        }
        return dto;
    }

    public BaseDto create(RecordAccessLevelDTO recordAccessLevelDto) {

        log.info("Create method is called.....");
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            String json = objectMapper.writeValueAsString(recordAccessLevelDto);
            log.info("Json Request DTO :: " + json);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        RecordAccessLevel recordAccessLevel = new RecordAccessLevel(recordAccessLevelDto);

        try {
            String json = objectMapper.writeValueAsString(recordAccessLevel);
            log.info("Json Request :: " + json);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        BaseDto baseDto = new BaseDto();

        try {

            recordAccessLevelValidator.validate(recordAccessLevel);

            if (recordAccessLevel.getUserList() != null && !recordAccessLevel.getUserList().isEmpty()) {
                for (RecordAccessLevelUser user : recordAccessLevel.getUserList()) {
                    user.setRecordAccessLevel(recordAccessLevel);
                }
            } else {
                recordAccessLevel.setUserList(new ArrayList<>());
            }

            if (recordAccessLevel.getCountryList() != null && !recordAccessLevel.getCountryList().isEmpty()) {
                for (RecordAccessLevelCountry country : recordAccessLevel.getCountryList()) {
                    country.setRecordAccessLevel(recordAccessLevel);
                }
            } else {
                recordAccessLevel.setCountryList(new ArrayList<>());
            }

            if (recordAccessLevel.getLocationList() != null && !recordAccessLevel.getLocationList().isEmpty()) {
                for (RecordAccessLevelLocation location : recordAccessLevel.getLocationList()) {
                    location.setRecordAccessLevel(recordAccessLevel);
                }
            } else {
                recordAccessLevel.setLocationList(new ArrayList<>());
            }

            if (recordAccessLevel.getServiceList() != null && !recordAccessLevel.getServiceList().isEmpty()) {
                for (RecordAccessLevelCountry country : recordAccessLevel.getCountryList()) {
                    country.setRecordAccessLevel(recordAccessLevel);
                }
            } else {
                recordAccessLevel.setServiceList(new ArrayList<>());
            }

            if (recordAccessLevel.getDivisionList() != null && !recordAccessLevel.getDivisionList().isEmpty()) {
                for (RecordAccessLevelDivision division : recordAccessLevel.getDivisionList()) {
                    division.setRecordAccessLevel(recordAccessLevel);
                }
            } else {
                recordAccessLevel.setDivisionList(new ArrayList<>());
            }

            if (recordAccessLevel.getSalesmanList() != null && !recordAccessLevel.getSalesmanList().isEmpty()) {
                for (RecordAccessLevelSalesman salesman : recordAccessLevel.getSalesmanList()) {
                    salesman.setRecordAccessLevel(recordAccessLevel);
                }
            } else {
                recordAccessLevel.setSalesmanList(new ArrayList<>());
            }

            if (recordAccessLevel.getCustomerList() != null && !recordAccessLevel.getCustomerList().isEmpty()) {
                for (RecordAccessLevelCustomer customer : recordAccessLevel.getCustomerList()) {
                    customer.setRecordAccessLevel(recordAccessLevel);
                }
            } else {
                recordAccessLevel.setCustomerList(new ArrayList<>());
            }

            if (recordAccessLevel.getId() != null) {
                recordAccessLevelRepository.getOne(recordAccessLevel.getId());
            }
            recordAccessLevel = recordAccessLevelRepository.save(recordAccessLevel);

            log.info("Record Access Level Saved successfully.....[" + recordAccessLevel.getId() + "]");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(ErrorCode.SUCCESS);

        } catch (RestException exception) {

            log.error("RestException in Create method : ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (ObjectOptimisticLockingFailureException e) {

            log.error("Error in editing Service", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_LOCKED_RECORD);

        } catch (JpaObjectRetrievalFailureException e) {

            log.error("Error in editing Service", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_DELETED_RECORD);

        } catch (Exception exception) {

            log.error("Exception in Create method : ", exception);

            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Exception Cause 1 ::: " + exceptionCause1);

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            if (exceptionCause1.contains(UniqueKey.UK_RAL_NAME)) {
                baseDto.setResponseCode(ErrorCode.RAL_NAME_ALREADY_EXIST);
            }
        }

        return appUtil.setDesc(baseDto);
    }

    public BaseDto get(Long id) {

        log.info("get method is called....[" + id + "]");

        BaseDto baseDto = new BaseDto();

        try {

            RecordAccessLevel recordAccessLevel = recordAccessLevelRepository.findById(id);
            log.info("recordAccessLevel " + recordAccessLevel.getId());
            /*try {
				ObjectMapper objectMapper = new ObjectMapper();
				String json = objectMapper.writeValueAsString(recordAccessLevel);
				log.info("Json Request :: " + json);
			} catch (JsonProcessingException e) {
				e.printStackTrace();
			}*/

            RecordAccessLevelDTO recordAccessLevelDTO = new RecordAccessLevelDTO(recordAccessLevel);

            log.info("Successfully getting Record Access Level by id...[" + recordAccessLevel.getId() + "]");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(recordAccessLevelDTO);

        } catch (Exception exception) {

            log.error("Exception in get method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);
    }

    public BaseDto search(RecordAccessLevelSearchDto searchDto) {

        log.info("RecordAccessLevelService -> search method is called....");

        BaseDto baseDto = new BaseDto();

        try {

            baseDto.setResponseCode(ErrorCode.SUCCESS);
            baseDto.setResponseObject(recordAccessLevelSearchImpl.search(searchDto));

        } catch (Exception e) {
            log.error("Exception in RecordAccessLevelService -> search method ", e);
            baseDto.setResponseCode(ErrorCode.FAILED);

        }
        return appUtil.setDesc(baseDto);
    }

    public BaseDto delete(Long id) {

        log.info("delete method is called....[" + id + "]");

        BaseDto baseDto = new BaseDto();

        try {

            recordAccessLevelRepository.delete(id);

            log.info("Successfully deleted by id...");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

        } catch (DataIntegrityViolationException exception) {

            log.error("Exception occured : ", exception);

            ExceptionHelper helper = new ExceptionHelper(exception);

            log.error("Foreign key reference : " + helper.getFkReference());

            baseDto.setResponseCode(ErrorCode.ERROR_FK_CONSTRAINT);

        } catch (Exception exception) {

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            log.error("Exception in Delete method : ", exception);

            String rootCause = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Root Cause of Exception : " + rootCause);

            if (rootCause.contains("FK_")) {
                baseDto.setResponseCode(ErrorCode.CANNOT_DELETE_FK_RECORD);
            }
        }

        return appUtil.setDesc(baseDto);

    }

}
