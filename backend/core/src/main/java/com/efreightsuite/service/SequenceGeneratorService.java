package com.efreightsuite.service;

import java.util.Date;

import javax.transaction.Transactional;
import javax.transaction.Transactional.TxType;

import com.efreightsuite.common.component.CacheRepository;
import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.SequenceGeneratorSearchDto;
import com.efreightsuite.enumeration.SequenceType;
import com.efreightsuite.enumeration.YesNo;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.exception.UniqueKey;
import com.efreightsuite.model.LocationMaster;
import com.efreightsuite.model.SequenceGenerator;
import com.efreightsuite.repository.SequenceGeneratorRepository;
import com.efreightsuite.search.SequenceGeneratorSearchImpl;
import com.efreightsuite.util.AppUtil;
import com.efreightsuite.util.SaaSUtil;
import com.efreightsuite.util.TimeUtil;
import com.efreightsuite.validation.SequenceGeneratorValidator;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.exception.ExceptionUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class SequenceGeneratorService {

    @Autowired
    private
    SequenceGeneratorRepository sequenceGeneratorRepository;

    @Autowired
    private
    CacheRepository cacheRepository;

    @Autowired
    private
    SequenceGeneratorSearchImpl sequenceGeneratorSearchImpl;

    @Autowired
    private
    SequenceGeneratorValidator sequenceGeneratorValidator;

    @Autowired
    private
    AppUtil appUtil;


    @Transactional(value = TxType.REQUIRES_NEW)
    public String getSequenceValueByType(SequenceType sequenceType) {

        return getSequence(sequenceType);
    }

    public String getSequence(SequenceType sequenceType, LocationMaster locationMaster) {
        log.info("SequenceGeneratorService.getCurrentSequenceValueByType start");

        String genaratedSequence = nextSequence(sequenceType, locationMaster);

        log.info("Current Sequence Value : " + genaratedSequence);

        return genaratedSequence;

    }

    public String getSequence(SequenceType sequenceType) {
        return nextSequence(sequenceType, null);
    }


    private String nextSequence(SequenceType sequenceType, LocationMaster locationMaster) {

		/*Long companyId = null;
		
		
		if(locationMaster==null){
			companyId = AuthService.getCurrentUser().getSelectedCompany().getId();
		}else{
			companyId = locationMaster.getCompanyMaster().getId();
		}
		
		SequenceGenerator sequence = sequenceGeneratorRepository.getByTypeAndCompany(sequenceType,companyId);*/

        SequenceGenerator sequence = sequenceGeneratorRepository.findBySequenceType(sequenceType);

        if (sequence == null) {
            throw new RestException(ErrorCode.SEQUENCE_NOT_PRESENT);
        }

        String genaratedSequence = "";

        String separator = "";

        if (sequence.getSeparator() != null && sequence.getSeparator().trim().length() != 0) {
            separator = sequence.getSeparator().trim();
        }

        if (sequence.getPrefix() != null && sequence.getPrefix().trim().length() != 0) {
            if (genaratedSequence.trim().length() == 0) {
                genaratedSequence = genaratedSequence + sequence.getPrefix().trim();
            } else {
                genaratedSequence = genaratedSequence + separator + sequence.getPrefix().trim();
            }
        }

        if (sequence.getIsYear() != null && sequence.getIsYear() == YesNo.Yes) {

            Date dt = null;

            if (locationMaster == null) {
                dt = TimeUtil.getCurrentLocationTime();
            } else {
                dt = TimeUtil.getCurrentLocationTime(locationMaster);
            }


            if (genaratedSequence.trim().length() == 0) {
                genaratedSequence = genaratedSequence + (dt.getYear() + 1900);
            } else {
                genaratedSequence = genaratedSequence + separator + (dt.getYear() + 1900);
            }
        }

        if (sequence.getIsMonth() != null && sequence.getIsMonth() == YesNo.Yes) {

            Date dt = null;

            if (locationMaster == null) {
                dt = TimeUtil.getCurrentLocationTime();
            } else {
                dt = TimeUtil.getCurrentLocationTime(locationMaster);
            }

            if (genaratedSequence.trim().length() == 0) {
                genaratedSequence = genaratedSequence + (dt.getMonth() + 1);
            } else {
                genaratedSequence = genaratedSequence + separator + (dt.getMonth() + 1);
            }
        }

        if (genaratedSequence.trim().length() == 0) {
            genaratedSequence = genaratedSequence + String.format(sequence.getFormat().getFormat(), sequence.getCurrentSequenceValue());
        } else {
            genaratedSequence = genaratedSequence + separator + String.format(sequence.getFormat().getFormat(), sequence.getCurrentSequenceValue());
        }

        if (sequence.getSuffix() != null && sequence.getSuffix().trim().length() != 0) {
            if (genaratedSequence.trim().length() == 0) {
                genaratedSequence = genaratedSequence + sequence.getSuffix().trim();
            } else {
                genaratedSequence = genaratedSequence + separator + sequence.getSuffix().trim();
            }
        }

        sequence.setCurrentSequenceValue(sequence.getCurrentSequenceValue() + 1);
        sequenceGeneratorRepository.save(sequence);

        return genaratedSequence;
    }

    public BaseDto get(Long id) {
        log.info("get method is called....");

        BaseDto baseDto = new BaseDto();

        try {
            SequenceGenerator sequenceGenerator = cacheRepository.get(SaaSUtil.getSaaSId(),
                    SequenceGenerator.class.getName(), id, sequenceGeneratorRepository);

            log.info("Successfully getting SequenceGenerator by id...");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(sequenceGenerator);

        } catch (RestException exception) {

            log.error("RestException in get method : ", exception);

            baseDto.setResponseCode(exception.getMessage());
        } catch (Exception exception) {

            log.error("Exception in get method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);

        }

        return appUtil.setDesc(baseDto);
    }

    public BaseDto create(SequenceGenerator sequenceGenerator) {

        log.info("Create method is called.....");

        BaseDto baseDto = new BaseDto();

        try {

            sequenceGeneratorValidator.validator(sequenceGenerator);


            sequenceGenerator = cacheRepository.save(SaaSUtil.getSaaSId(), SequenceGenerator.class.getName(),
                    sequenceGenerator, sequenceGeneratorRepository);

            log.info(sequenceGenerator.getSequenceType() + " SequenceGenerator Saved successfully.....");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(sequenceGenerator);

        } catch (RestException exception) {

            log.error("RestException in Create method : ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (Exception e) {

            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(e);

            log.error("Exception Cause 1 ::: " + exceptionCause1);

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            if (exceptionCause1.contains(UniqueKey.UK_SEQUENCE_TYPE)) {
                baseDto.setResponseCode(ErrorCode.SEQUENCE_TYPE_ALREADY_EXIST);
            }

            if (exceptionCause1.contains(UniqueKey.UK_SEQUENCE_NAME)) {
                baseDto.setResponseCode(ErrorCode.SEQUENCE_NAME_ALREADY_EXIST);
            }

            if (exceptionCause1.contains(UniqueKey.UK_SEQUENCE_PREFIX)) {
                baseDto.setResponseCode(ErrorCode.SEQUENCE_PREFIX_ALREADY_EXIST);
            }
        }
        return appUtil.setDesc(baseDto);
    }


    /**
     * Method - search Returns list of GeneralNoteMaster based on the search
     *
     * @return BaseDto
     */

    public BaseDto search(SequenceGeneratorSearchDto searchDto) {

        log.info("Search method is called...." + searchDto);

        BaseDto baseDto = new BaseDto();

        try {

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(sequenceGeneratorSearchImpl.search(searchDto));

            log.info("Successfully Searching GeneralNoteMaster...");

        } catch (Exception exception) {

            log.error("Exception in Search method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);

    }


    public BaseDto update(SequenceGenerator newMaster) {

        log.info("update method is called.... : " + newMaster.getId());

        BaseDto baseDto = new BaseDto();

        try {

            sequenceGeneratorValidator.validator(newMaster);

            SequenceGenerator oldMaster = cacheRepository.get(SaaSUtil.getSaaSId(), SequenceGenerator.class.getName(),
                    newMaster.getId(), sequenceGeneratorRepository);

            oldMaster.setCurrentSequenceValue(newMaster.getCurrentSequenceValue());

            oldMaster = cacheRepository.save(SaaSUtil.getSaaSId(), SequenceGenerator.class.getName(), oldMaster,
                    sequenceGeneratorRepository);

            log.info("SequenceGenerator updated successfully...[" + oldMaster.getId() + "]");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(oldMaster);

        } catch (RestException exception) {

            log.error("RestException in Create method : ", exception);

            baseDto.setResponseCode(exception.getMessage());
        } catch (Exception e) {
            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(e);

            log.error("Exception Cause 1 ::: " + exceptionCause1);

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            if (exceptionCause1.contains(UniqueKey.UK_SEQUENCE_TYPE)) {
                baseDto.setResponseCode(ErrorCode.SEQUENCE_TYPE_ALREADY_EXIST);
            }

            if (exceptionCause1.contains(UniqueKey.UK_SEQUENCE_NAME)) {
                baseDto.setResponseCode(ErrorCode.SEQUENCE_NAME_ALREADY_EXIST);
            }

            if (exceptionCause1.contains(UniqueKey.UK_SEQUENCE_PREFIX)) {
                baseDto.setResponseCode(ErrorCode.SEQUENCE_PREFIX_ALREADY_EXIST);
            }

        }
        return appUtil.setDesc(baseDto);
    }
}
