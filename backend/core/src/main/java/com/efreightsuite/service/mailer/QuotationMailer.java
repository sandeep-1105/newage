package com.efreightsuite.service.mailer;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import com.efreightsuite.common.component.CacheRepository;
import com.efreightsuite.common.component.EmailRequestService;
import com.efreightsuite.dto.ReportDownloadRequestDto;
import com.efreightsuite.enumeration.AddressType;
import com.efreightsuite.enumeration.ReportDownloadFileType;
import com.efreightsuite.enumeration.ReportName;
import com.efreightsuite.enumeration.TemplateType;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.model.EmailRequest;
import com.efreightsuite.model.EmailRequestAttachment;
import com.efreightsuite.model.EmailTemplate;
import com.efreightsuite.model.PartyMaster;
import com.efreightsuite.model.Quotation;
import com.efreightsuite.model.UserProfile;
import com.efreightsuite.repository.DefaultMasterDataRepository;
import com.efreightsuite.repository.EmailTemplateRepository;
import com.efreightsuite.repository.QuotationRepository;
import com.efreightsuite.util.AppUtil;
import com.efreightsuite.util.RestService;
import com.efreightsuite.util.TimeUtil;
import com.efreightsuite.util.ValidateUtil;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service
@Log4j2
public class QuotationMailer {

    @Autowired
    private
    AppUtil appUtil;

    @Autowired
    private
    MailerUtil mailerUtil;

    @Autowired
    DefaultMasterDataRepository defaultMasterDataRepository;

    @Autowired
    private
    EmailTemplateRepository emailTemplateRepository;

    @Autowired
    private
    EmailRequestService emailRequestService;

    @Autowired
    private
    QuotationRepository quotationRepository;

    @Autowired
    private
    CacheRepository cacheRepository;

    @Autowired
    private
    RestService restService;


    private byte[] getQuotationByte(Long quotationId, UserProfile userProfile, String saasId) {
        ReportDownloadRequestDto reportDownloadRequestDto = new ReportDownloadRequestDto();
        reportDownloadRequestDto.setResourceId(quotationId);
        reportDownloadRequestDto.setReportName(ReportName.QUOTATION_WITHOUT_ESTIMATION);
        reportDownloadRequestDto.setDownloadFileType(ReportDownloadFileType.PDF);
        return restService.getPdfReportByteArray(reportDownloadRequestDto, userProfile, saasId);
    }

    @Async
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void sendQuotationMailToCustomer(Long quotationId, UserProfile userProfile, String saasId) {
        log.info("sendQuotationMailToCustomer start: ");

        EmailTemplate emailTemplate = cacheRepository.get(saasId, EmailTemplate.class.getName(), TemplateType.QUOTATION_APPROVING_CONFIRMATION, emailTemplateRepository);

        if (emailTemplate == null) {
            log.error("Quotation Approval Template is not available");
            return;
        }

        Quotation quotation = quotationRepository.findById(quotationId);

        ValidateUtil.notNull(getPartyEmailList(quotation.getCustomer()), ErrorCode.QUOTATION_CUSTOMER_EMAIL_REQUIRED);

        ReportDownloadRequestDto reportDownloadRequestDto = new ReportDownloadRequestDto();
        reportDownloadRequestDto.setResourceId(quotationId);
        reportDownloadRequestDto.setDownloadFileType(ReportDownloadFileType.PDF);

        byte[] attachmentArr = getQuotationByte(quotationId, userProfile, saasId);

        if (attachmentArr == null) {
            log.error("Quotation generation failed");
            return;
        }

        try {

            Map<String, Object> mapSubject = new HashMap<>();
            mapSubject.put("#QUOTATION_NO#", quotation.getQuotationNo());

            Map<String, Object> mapBody = new HashMap<>();

            mapBody.put("#COMPANY_NAME#", quotation.getCompanyMaster().getCompanyName());
            mapBody.put("#EMP_NAME#", quotation.getLoggedBy().getEmployeeName());
            mapBody.put("#EMP_EMAIL#", quotation.getLoggedBy().getEmail());
            mapBody.put("#EMP_TELNO#", quotation.getLoggedBy().getEmployeePhoneNo());
            // sending email

            EmailRequest emailRequest = new EmailRequest();
            emailRequest.setFromEmailId(emailTemplate.getFromEmailId());
            emailRequest.setFromEmailIdPassword(emailTemplate.getFromEmailPassword());

            String emailSeprate = appUtil.getLocationConfig("single.text.split.by.multiple.email", quotation.getLocationMaster(), false);


            String toEmail = getPartyEmailList(quotation.getCustomer()).replaceAll(emailSeprate, ",");
            emailRequest.setToEmailIdList(toEmail);

            String ccEmail = quotation.getLoggedBy().getEmail().replaceAll(emailSeprate, ",");
            emailRequest.setCcEmailIdList(ccEmail);
            emailRequest.setEmailType(emailTemplate.getEmailType());
            emailRequest.setPort(emailTemplate.getPort());
            emailRequest.setSmtpServerAddress(emailTemplate.getSmtpServerAddress());

            emailRequest.setSubject(emailTemplate.getSubject());
            emailRequest.setEmailBody(emailTemplate.getTemplate());

            for (String key : mapSubject.keySet()) {
                emailRequest.setSubject(emailRequest.getSubject().replaceAll(key, mapSubject.get(key) == null ? "" : mapSubject.get(key).toString()));
            }

            for (String key : mapBody.keySet()) {
                emailRequest.setEmailBody(emailRequest.getEmailBody().replaceAll(key, mapBody.get(key) == null ? "" : mapBody.get(key).toString()));
            }

            // quotation report pdf setting to attachment
            Set<EmailRequestAttachment> attachmentList = new HashSet<>();
            EmailRequestAttachment emailRequestAttachment = new EmailRequestAttachment();
            emailRequestAttachment.setAttachement(attachmentArr);
            emailRequestAttachment.setFileName("quotation" + "_" + quotation.getQuotationNo() + "." + "pdf");
            emailRequestAttachment.setMimeType("application/pdf");
            emailRequestAttachment.setManualSystemTrack(TimeUtil.getCreateSystemTrack(userProfile));
            attachmentList.add(emailRequestAttachment);
            emailRequest.setAttachmentList(attachmentList);

            emailRequestService.saveAndSendEmail(emailRequest, userProfile, mailerUtil.getLogo(quotation.getLocationMaster()));
        } catch (RestException re) {
            log.error("RestException in EmailAsyncService.sendQuotationMailToCustomer : ", re);

        } catch (Exception e) {
            log.error("Exception in EmailAsyncService.sendQuotationMailToCustomer : ", e);
        }

    }

    private String getPartyEmailList(PartyMaster partyMaster) {

        String emailList = null;
        if (partyMaster == null || partyMaster.getId() == null) {
            return null;
        } else {
            if (partyMaster.getPartyAddressList() != null && partyMaster.getPartyAddressList().size() > 0) {
                for (int i = 0; i < partyMaster.getPartyAddressList().size(); i++) {
                    if (partyMaster.getPartyAddressList().get(i).getAddressType().equals(AddressType.Primary)) {
                        emailList = partyMaster.getPartyAddressList().get(i).getEmail();
                    }
                }
            }
        }
        return emailList;
    }

}

