package com.efreightsuite.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.persistence.EntityManager;
import javax.servlet.http.HttpServletResponse;

import com.efreightsuite.dto.AirDocumentSearchDto;
import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.ConsolDto;
import com.efreightsuite.dto.ConsolSearchDto;
import com.efreightsuite.dto.DeliveryOrderDto;
import com.efreightsuite.dto.GenerateNumDto;
import com.efreightsuite.dto.SearchRespDto;
import com.efreightsuite.enumeration.EventMasterType;
import com.efreightsuite.enumeration.ImportExport;
import com.efreightsuite.enumeration.LovStatus;
import com.efreightsuite.enumeration.TransportMode;
import com.efreightsuite.enumeration.YesNo;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.exception.UniqueKey;
import com.efreightsuite.model.AuditEntity;
import com.efreightsuite.model.CommentMaster;
import com.efreightsuite.model.Consol;
import com.efreightsuite.model.ConsolAttachment;
import com.efreightsuite.model.ConsolCharge;
import com.efreightsuite.model.ConsolDocument;
import com.efreightsuite.model.ConsolSignOff;
import com.efreightsuite.model.DocumentDetail;
import com.efreightsuite.model.EventMaster;
import com.efreightsuite.model.ShipmentLink;
import com.efreightsuite.model.ShipmentServiceDetail;
import com.efreightsuite.model.ShipmentServiceEvent;
import com.efreightsuite.model.UserProfile;
import com.efreightsuite.repository.CommentMasterRepository;
import com.efreightsuite.repository.ConsolAttachmentRepository;
import com.efreightsuite.repository.ConsolDocumentRepository;
import com.efreightsuite.repository.ConsolRepository;
import com.efreightsuite.repository.ConsolSignOffRepository;
import com.efreightsuite.repository.DefaultMasterDataRepository;
import com.efreightsuite.repository.DocumentDetailRepository;
import com.efreightsuite.repository.EventMasterRepository;
import com.efreightsuite.repository.LocationMasterRepository;
import com.efreightsuite.repository.ShipmentServiceDetailRepository;
import com.efreightsuite.repository.ShipmentServiceEventRepository;
import com.efreightsuite.search.ConsolSearchImplNew;
import com.efreightsuite.search.ShipmentSearchImpl;
import com.efreightsuite.service.mailer.ShipmentMailer;
import com.efreightsuite.util.AppUtil;
import com.efreightsuite.util.CryptoUtils;
import com.efreightsuite.util.TimeUtil;
import com.efreightsuite.validation.ConsolValidator;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.hibernate.envers.AuditReader;
import org.hibernate.envers.AuditReaderFactory;
import org.hibernate.envers.RevisionType;
import org.hibernate.envers.query.AuditQuery;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.ObjectOptimisticLockingFailureException;
import org.springframework.orm.jpa.JpaObjectRetrievalFailureException;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class ConsolService {

    @Autowired
    private
    AppUtil appUtil;

    @Autowired
    private
    AutoImportProcessService autoImportProcessService;

    @Autowired
    private
    ConsolRepository consolRepository;

    @Autowired
    private
    ShipmentInvoiceValidationService shipmentInvoiceValidationService;

    @Autowired
    private
    ShipmentMailer shipmentMailer;

    @Autowired
    private
    ConsolSearchImplNew consolSearchImplNew;

    @Autowired
    private
    ConsolValidator consolValidator;

    @Autowired
    SequenceGeneratorService sequenceGeneratorService;

    @Autowired
    private
    ShipmentServiceDetailRepository shipmentServiceDetailRepository;

    @Autowired
    DefaultMasterDataRepository defaultMasterDataRepository;

    @Autowired
    private
    ConsolAttachmentRepository consolAttachmentRepository;

    @Autowired
    private
    ShipmentSearchImpl shipmentSearchImpl;

    @Autowired
    LocationMasterRepository locationMasterRepository;

    @Autowired
    private
    DocumentDetailRepository documentDetailRepository;

    @Autowired
    private
    ConsolDocumentRepository consolDocumentRepository;

    @Autowired
    private
    ConsolSignOffRepository consolSignOffRepository;

    @Autowired
    private
    EventMasterRepository eventMasterRepository;

    @Autowired
    private
    EntityManager entityManger;

    @Autowired
    private
    ShipmentServiceEventRepository shipmentServiceEventRepository;

    @Autowired
    private
    CommentMasterRepository commentMasterRepository;

    public BaseDto getById(Long id) {

        log.info("ConsolService -> getById method is called....");

        BaseDto baseDto = new BaseDto();

        try {

            Consol consol = consolRepository.findById(id);

            afterGet(consol);

            baseDto.setResponseCode(ErrorCode.SUCCESS);
            baseDto.setResponseObject(consol);
        } catch (Exception e) {
            log.error("Exception in ConsolService -> getById method ", e);
            baseDto.setResponseCode(ErrorCode.FAILED);

        }
        return appUtil.setDesc(baseDto);
    }

    private void afterGet(Consol consol) {

        AppUtil.setMaster(consol);

        // making the file null
        if (consol.getConsolAttachmentList() != null && consol.getConsolAttachmentList().size() != 0) {
            for (ConsolAttachment attachment : consol.getConsolAttachmentList()) {
                attachment.setFile(null);
            }
        }

        if (consol.getShipmentLinkList() != null && consol.getShipmentLinkList().size() != 0) {
            for (ShipmentLink link : consol.getShipmentLinkList()) {
                link.getService().touch();
            }
        }
    }

    public BaseDto shipmentLink(ConsolDto consolDto) {

        log.info("ConsolService -> getById method is called....");

        BaseDto baseDto = new BaseDto();

        try {

            List<ShipmentServiceDetail> list = shipmentSearchImpl.shipmentLink(consolDto);

            if (list == null) {
                list = new ArrayList<>();
            }

            SearchRespDto dto = new SearchRespDto(Long.parseLong(list.size() + ""), list);
            baseDto.setResponseCode(ErrorCode.SUCCESS);
            baseDto.setResponseObject(dto);

        } catch (RestException re) {
            log.error("Exception in ConsolService -> getById method ", re);
            baseDto.setResponseCode(re.getMessage());

        } catch (Exception e) {
            log.error("Exception in ConsolService -> getById method ", e);
            baseDto.setResponseCode(ErrorCode.FAILED);

        }
        return appUtil.setDesc(baseDto);
    }

    public BaseDto getByConsolUid(String consolUid) {

        log.info("ConsolService -> getByConsolUid method is called....");

        BaseDto baseDto = new BaseDto();

        try {

            Consol consol = consolRepository.findByConsolUid(consolUid);

            afterGet(consol);

            baseDto.setResponseCode(ErrorCode.SUCCESS);
            baseDto.setResponseObject(consol);

        } catch (RestException re) {
            log.error("Exception in ConsolService -> getByConsolUid method ", re);
            baseDto.setResponseCode(re.getMessage());

        } catch (Exception e) {
            log.error("Exception in ConsolService -> getByConsolUid method ", e);
            baseDto.setResponseCode(ErrorCode.FAILED);

        }
        return appUtil.setDesc(baseDto);
    }

    public BaseDto search(ConsolSearchDto searchDto) {

        log.info("ConsolService -> search method is called....");

        BaseDto baseDto = new BaseDto();

        try {

            baseDto.setResponseCode(ErrorCode.SUCCESS);
            baseDto.setResponseObject(consolSearchImplNew.search(searchDto));

        } catch (RestException re) {
            log.error("Exception in ConsolService -> search method ", re);
            baseDto.setResponseCode(re.getMessage());

        } catch (Exception e) {
            log.error("Exception in ConsolService -> search method ", e);
            baseDto.setResponseCode(ErrorCode.FAILED);

        }
        return appUtil.setDesc(baseDto);
    }

    public BaseDto create(Consol consol) {

        log.info("ConsolService -> create method is called....");

        BaseDto baseDto = new BaseDto();

        try {
            consolSearchImplNew.processCreate(baseDto, consol);
        } catch (RestException re) {
            log.error("Exception in ConsolService -> create method ", re);
            if (re.getParams() != null && !re.getParams().isEmpty()) {
                for (String param : re.getParams()) {
                    baseDto.getParams().add(param);
                }
            }
            baseDto.setResponseCode(re.getMessage());

        } catch (ObjectOptimisticLockingFailureException e) {
            log.error("Error in editing Service", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_LOCKED_RECORD);
        } catch (JpaObjectRetrievalFailureException e) {
            log.error("Error in editing Service", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_DELETED_RECORD);
        } catch (Exception exception) {
            log.error("Exception in ConsolService -> create method ", exception);

            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(exception);
            log.error("Exception Cause 1 ::: " + exceptionCause1);

            baseDto.setResponseCode(ErrorCode.FAILED);

            if (exceptionCause1.contains(UniqueKey.UK_CONSOL_CONSOLUID)) {
                baseDto.setResponseCode(ErrorCode.CONSOL_UNIQUE);
            }

            if (exceptionCause1.contains(UniqueKey.UK_CONSLATTCH_REF)) {
                baseDto.setResponseCode(ErrorCode.ATTACHMENT_REFERENCE_NUMBER_DUPLICATED);
            }

            if (exceptionCause1.contains("FK_")) {
                baseDto.setResponseCode(ErrorCode.SAVE_UPDATE_FK_RECORD);
            }

        }

        if (baseDto.getResponseCode() == ErrorCode.SUCCESS || baseDto.getResponseCode().equals(ErrorCode.SUCCESS)) {
            if (baseDto.getResponseObject() != null) {
                mailTrigger((Consol) baseDto.getResponseObject(), AuthService.getCurrentUser());
            }
        }

        return appUtil.setDesc(baseDto);
    }

    private void mailTrigger(Consol consolObj, UserProfile userProfile) {
        try {
            Consol consol = consolRepository.findById(consolObj.getId());
            if (consol.getServiceMaster().getTransportMode() == TransportMode.Air
                    && consol.getServiceMaster().getImportExport() == ImportExport.Export) {
                shipmentMailer.cargoLoadingConfirmationMail(consol, userProfile);
            }
        } catch (Exception e) {
            log.error("Error occur in Cargo Loading confirmation mail : ", e);
        }
    }

    public BaseDto update(Consol consol) {

        log.info("ConsolService -> update method is called....");

        BaseDto baseDto = new BaseDto();

        try {
            consolSearchImplNew.processUpdate(baseDto, consol);
        } catch (RestException re) {
            log.error("Exception in ConsolService -> update method ", re);
            if (re.getParams() != null && !re.getParams().isEmpty()) {
                for (String param : re.getParams()) {
                    baseDto.getParams().add(param);
                }
            }
            baseDto.setResponseCode(re.getMessage());

        } catch (ObjectOptimisticLockingFailureException e) {

            log.error("Error in editing Consol", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_LOCKED_RECORD);

        } catch (JpaObjectRetrievalFailureException e) {

            log.error("Error in editing Consol", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_DELETED_RECORD);

        } catch (Exception exception) {
            log.error("Exception in ConsolService -> update method ", exception);

            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(exception);
            log.error("Exception Cause 1 ::: " + exceptionCause1);

            baseDto.setResponseCode(ErrorCode.FAILED);

            if (exceptionCause1.contains(UniqueKey.UK_CONSOL_CONSOLUID)) {
                baseDto.setResponseCode(ErrorCode.CONSOL_UNIQUE);
            }

            if (exceptionCause1.contains("FK_")) {
                baseDto.setResponseCode(ErrorCode.SAVE_UPDATE_FK_RECORD);
            }

        }

        if (baseDto.getResponseCode() == ErrorCode.SUCCESS || baseDto.getResponseCode().equals(ErrorCode.SUCCESS)) {
            if (baseDto.getResponseObject() != null) {
                mailTrigger((Consol) baseDto.getResponseObject(), AuthService.getCurrentUser());
            }
        }

        return appUtil.setDesc(baseDto);
    }

    public BaseDto statusChange(ConsolDto consolDto) {

        log.info("ConsolService -> update method is called....");

        BaseDto baseDto = new BaseDto();

        try {
            consolSearchImplNew.processStatusChange(consolDto);

            Consol consol = consolRepository.findById(consolDto.getConsolId());

            afterGet(consol);

            baseDto.setResponseCode(ErrorCode.SUCCESS);
            baseDto.setResponseObject(consol);
        } catch (RestException re) {
            log.error("Exception in ConsolService -> update method ", re);
            baseDto.setResponseCode(re.getMessage());

        } catch (Exception exception) {
            log.error("Exception in ConsolService -> update method ", exception);

            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(exception);
            log.error("Exception Cause 1 ::: " + exceptionCause1);

            baseDto.setResponseCode(ErrorCode.FAILED);

            if (exceptionCause1.contains(UniqueKey.UK_CONSOL_CONSOLUID)) {
                baseDto.setResponseCode(ErrorCode.CONSOL_UNIQUE);
            }

            if (exceptionCause1.contains("FK_")) {
                baseDto.setResponseCode(ErrorCode.SAVE_UPDATE_FK_RECORD);
            }

        }
        return appUtil.setDesc(baseDto);
    }

    public void downloadAttachment(Long attachmentId, String type, boolean swapped, HttpServletResponse response) {

        try {
            ConsolAttachment attachment = consolAttachmentRepository.findOne(attachmentId);
            String KEY = appUtil.getLocationConfig("shipment.attach.file.encrypt.key",
                    attachment.getConsol().getLocation(), false);
            if (attachment.getIsProtected() != null && attachment.getIsProtected().equals(YesNo.Yes)) {
                log.info("Its a protected File... ");
                attachment.setFile(CryptoUtils.decrypt(KEY, attachment.getFile()));
            }
            response.getOutputStream().write(attachment.getFile());
            response.setContentType(attachment.getFileContentType());
            response.setContentLength(attachment.getFile().length);
            response.setHeader("Content-Disposition", "attachment; filename=" + attachment.getFileName());
            response.getOutputStream().flush();
            response.getOutputStream().close();
        } catch (Exception exception) {
            log.error("Error occurred while downloading attachment file : " + attachmentId + ", ", exception);
        }
    }

    public BaseDto getDocumentById(String docType, Long id) {

        log.info("ConsolService -> getDocumentById method is called....");

        BaseDto baseDto = new BaseDto();

        try {

            if (docType.toUpperCase().equals("HAWB") || docType.toUpperCase().equals("DO")) {
                DocumentDetail dd = documentDetailRepository.findById(id);
                if (docType.toUpperCase().equals("DO") && dd.getShipmentServiceDetail().getConsolUid() != null) {
                    Consol consol = consolRepository.findByConsolUid(dd.getShipmentServiceDetail().getConsolUid());
                    consol.setShipmentLinkList(null);
                    AppUtil.setMaster(consol);
                    dd.setConsol(consol);
                }
                dd.setServiceId(dd.getShipmentServiceDetail().getId());
                AppUtil.setService(dd.getShipmentServiceDetail());
                baseDto.setResponseObject(dd);
            } else if (docType.toUpperCase().equals("MAWB")) {
                ConsolDocument cs = consolDocumentRepository.findById(id);
                Consol consol = consolRepository.findByConsolUid(cs.getConsolUid());
                consol.setShipmentLinkList(null);
                AppUtil.setMaster(consol);
                baseDto.setResponseObject(consol);
            }

            baseDto.setResponseCode(ErrorCode.SUCCESS);

        } catch (Exception e) {
            log.error("Exception in ConsolService -> getDocumentById method ", e);
            baseDto.setResponseCode(ErrorCode.FAILED);

        }
        return appUtil.setDesc(baseDto);
    }

    public BaseDto documentSearch(AirDocumentSearchDto searchDto) {

        log.info("ConsolService -> documentSearch method is called....");

        BaseDto baseDto = new BaseDto();

        try {
            baseDto.setResponseCode(ErrorCode.SUCCESS);

            if (searchDto.getWhichReport().trim().toUpperCase().equals("HAWB")) {
                baseDto.setResponseObject(consolSearchImplNew.hawbSearchInDocument(searchDto));
            } else if (searchDto.getWhichReport().trim().toUpperCase().equals("MAWB")) {
                baseDto.setResponseObject(consolSearchImplNew.mawbSearchInDocument(searchDto));
            } else if (searchDto.getWhichReport().trim().toUpperCase().equals("DO")) {
                baseDto.setResponseObject(consolSearchImplNew.doSearchInDocument(searchDto));
            } else {
                baseDto.setResponseObject(null);
            }

        } catch (RestException re) {
            log.error("Exception in ConsolService -> documentSearch method ", re);
            baseDto.setResponseCode(re.getMessage());

        } catch (Exception e) {
            log.error("Exception in ConsolService -> documentSearch method ", e);
            baseDto.setResponseCode(ErrorCode.FAILED);

        }

        log.info("ConsolService -> documentSearch method is end");
        return appUtil.setDesc(baseDto);
    }

    public BaseDto getConsolIdBasedOnUid(String consolUid) {

        log.info("ShipmentService -> getConsolIdBasedOnUid method is called....");

        BaseDto baseDto = new BaseDto();

        try {

            Long consolId = consolRepository.getConsolIdBasedOnUid(consolUid);

            baseDto.setResponseCode(ErrorCode.SUCCESS);
            baseDto.setResponseObject(consolId);

        } catch (RestException re) {
            log.error("Exception in ShipmentService -> getConsolIdBasedOnUid method ", re);
            baseDto.setResponseCode(re.getMessage());

        } catch (Exception e) {
            log.error("Exception in ShipmentService -> getConsolIdBasedOnUid method ", e);
            baseDto.setResponseCode(ErrorCode.FAILED);

        }
        return appUtil.setDesc(baseDto);
    }

    public BaseDto updateDocument(DeliveryOrderDto dto) {

        Consol cd = dto.getConsolCloneDO();
        DocumentDetail dc = dto.getDocumentDetailForDO();
        ShipmentServiceDetail sd = shipmentServiceDetailRepository.findByServiceUid(dc.getServiceUid());

        BaseDto baseDto = new BaseDto();

        try {
            if (dto.getIsDownloadPrintFor() != null
                    && (dto.getIsDownloadPrintFor() == "DO" || dto.getIsDownloadPrintFor().equals("DO"))) {

                if (sd != null && sd.getId() != null) {

                    if (sd.getEventList() != null && !sd.getEventList().isEmpty()) {

                        EventMaster event = new EventMaster();
                        ShipmentServiceEvent se = null;

                        if (Objects.equals(dto.getIsDownloadPrintFor(), "DO") || dto.getIsDownloadPrintFor().equals("DO")) {
                            event = eventMasterRepository.findByEventMasterType(EventMasterType.DO_RECEIVED);
                            se = shipmentServiceEventRepository.findByEventId(event.getId(), sd.getId());
                        } else {
                            log.info("no documnets");
                        }
                        if (se == null && event != null && event.getStatus().equals(LovStatus.Active)) {
                            se = new ShipmentServiceEvent();
                            se.setShipmentServiceDetail(sd);
                            se.setEventMaster(event);
                            se.setEventDate(TimeUtil.getCurrentLocationTime(sd.getLocation()));
                            se.setFollowUpRequired(YesNo.No);
                            se.setIsCompleted(YesNo.Yes);
                            sd.getEventList().add(se);
                            sd = shipmentServiceDetailRepository.save(sd);
                        }
                    }

                }
            }
            baseDto = saveDocument(cd, sd, dc, baseDto);

        } catch (RestException re) {
            log.error("Exception in ConsolService  method ", re);
            baseDto.setResponseCode(re.getMessage());
        } catch (Exception exception) {
            log.error("Exception in ConsolService  method ", exception);
            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(exception);
            log.error("Exception Cause 1 ::: " + exceptionCause1);
            baseDto.setResponseCode(ErrorCode.FAILED);
        }
        return appUtil.setDesc(baseDto);
    }

    private BaseDto saveDocument(Consol cd, ShipmentServiceDetail sd, DocumentDetail dc, BaseDto baseDto) {
        return consolSearchImplNew.saveDocument(cd, sd, dc, baseDto);
    }

    private ConsolSignOff signOff(ConsolSignOff consolSignOff) {
        return consolSearchImplNew.signOff(consolSignOff);
    }

    private ConsolSignOff unsignOff(ConsolSignOff consolSignOff) {

        Consol consol = consolRepository.findByConsolUid(consolSignOff.getConsolUid());

        if (appUtil.getLocationConfig("signoff.enabled", consol.getLocation(), true).equals("FALSE")) {
            throw new RestException(ErrorCode.SHIPMENT_SIGN_OFF_NOT_ENABLED);
        }

        if (AuthService.getCurrentUser().getFeatureMap().get("AIR.MASTER_SHIPMENT.UNSIGNOFF.MODIFY") != null
                && AuthService.getCurrentUser().getFeatureMap().get("AIR.MASTER_SHIPMENT.UNSIGNOFF.MODIFY")) {
            ConsolSignOff signOff = consolSignOffRepository.getOne(consolSignOff.getId());

            if (signOff.getIsSignOff() == YesNo.No) {
                throw new RestException(ErrorCode.SHIPMENT_UISIGN_OFF_ALREADY);
            }

            if (consolSignOff.getCommentMaster() == null || consolSignOff.getCommentMaster().getId() == null) {
                String code = appUtil.getLocationConfig("unsignoff.default.comment", consol.getLocation(), true);

                if (code != null && code.trim().length() != 0) {
                    CommentMaster commentMaster = commentMasterRepository.findByCommentCode(code);
                    if (commentMaster != null) {
                        consolSignOff.setCommentMaster(commentMaster);
                        consolSignOff.setCommentMasterCode(commentMaster.getCommentCode());
                        if (consolSignOff.getDescription() == null) {
                            consolSignOff.setDescription(commentMaster.getDescription());
                        }
                    }
                }

            }
            consolSignOff.setSignOffByCode(consolSignOff.getSignOffBy().getEmployeeCode());
            consolSignOff.setIsSignOff(YesNo.No);

            consolSignOff = consolSignOffRepository.save(consolSignOff);

            return consolSignOff;

        } else {
            throw new RestException(ErrorCode.SHIPMENT_UNSIGN_OFF_INVALID_USER);
        }

    }

    public BaseDto saveConsolSignOff(ConsolSignOff consolSignOff) {

        BaseDto baseDto = new BaseDto();

        try {

            baseDto.setResponseObject(signOff(consolSignOff));
            baseDto.setResponseCode(ErrorCode.SUCCESS);

        } catch (RestException re) {
            log.error("Exception in ConsolService -> create method ", re);
            baseDto.setResponseCode(re.getMessage());

        } catch (Exception exception) {

            log.error("Exception in ConsolService  method ", exception);

            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Exception Cause 1 ::: " + exceptionCause1);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return baseDto;
    }

    public BaseDto saveConsolUnSignOff(ConsolSignOff consolSignOff) {

        BaseDto baseDto = new BaseDto();

        try {

            baseDto.setResponseObject(unsignOff(consolSignOff));
            baseDto.setResponseCode(ErrorCode.SUCCESS);

        } catch (RestException re) {
            log.error("Exception in ConsolService -> create method ", re);
            baseDto.setResponseCode(re.getMessage());

        } catch (Exception exception) {

            log.error("Exception in ConsolService  method ", exception);

            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Exception Cause 1 ::: " + exceptionCause1);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return baseDto;
    }

    /*
     * Method is used for download do for all selected and updating the
     * respective services with do number
     */
    public BaseDto downloadAll(List<DeliveryOrderDto> serviceDetailId) {
        BaseDto bd = new BaseDto();
        try {
            if (serviceDetailId != null && serviceDetailId.size() > 0) {
                // step -1 invoice checking for all services
                List<Long> invoiceCheckID = new ArrayList<>();
                for (DeliveryOrderDto deliveryOrderDto : serviceDetailId) {
                    invoiceCheckID.add(deliveryOrderDto.getSid());
                }
                shipmentInvoiceValidationService.shipmentInvoicCheckingByServiceDetailId(invoiceCheckID);
                // step -2 update do values
                bd = updateDoValues(serviceDetailId, bd);
            } else {
                throw new RestException(ErrorCode.FAILED);
            }
        } catch (RestException re) {
            bd.setResponseCode(re.getMessage());
        } catch (Exception e) {
            log.info("exception ", e);
            bd.setResponseCode(ErrorCode.FAILED);
        }
        return appUtil.setDesc(bd);
    }

    private BaseDto updateDoValues(List<DeliveryOrderDto> serviceDetailIdList, BaseDto bd) {
        return consolSearchImplNew.updateDoValues(serviceDetailIdList, bd);
    }

    public BaseDto getConsolChargeHistory(String consolUid) {

        log.info("getConsolChargeHistor is called");

        BaseDto baseDto = new BaseDto();
        try {

            AuditReader reader = AuditReaderFactory.get(entityManger);

            AuditQuery query = reader.createQuery().forRevisionsOfEntity(ConsolCharge.class, false, true);

            // query.setFirstResult(searchDto.getSelectedPageNumber());

            // query.setMaxResults(searchDto.getRecordPerPage());

            query.add(org.hibernate.envers.query.AuditEntity.property("consolUid").eq(consolUid));

            List<Number> revList = query.getResultList();

            List<ConsolCharge> list = new ArrayList<>();

            if (revList != null && revList.size() >= 1) {
                for (Object obj : revList) {
                    Object[] objects = (Object[]) obj;
                    ConsolCharge consolCharge = (ConsolCharge) objects[0];
                    AuditEntity auditEntity = (AuditEntity) objects[1];
                    RevisionType revisonType = (RevisionType) objects[2];
                    consolCharge.setAuditDate(auditEntity.getRevisionDate());
                    consolCharge.setRevisionType(revisonType.name());
                    list.add(consolCharge);
                }
                baseDto.setResponseCode(ErrorCode.SUCCESS);
                baseDto.setResponseObject(list);
            } else {
                baseDto.setResponseCode(ErrorCode.NO_AUDIT_HISTORY_FOUND);
            }
        } catch (Exception exception) {
            log.error("Exception occured at Audit Histyory of Quotation service" + exception);
            baseDto.setResponseCode(ErrorCode.FAILED);
        }
        return baseDto;
    }

    public BaseDto getConsolShipmentDocumentListByServiceId(Long serviceId) {
        log.info("ConsolDocumentList is invoked.");
        BaseDto baseDto = new BaseDto();
        baseDto.setResponseCode(ErrorCode.SUCCESS);
        baseDto.setResponseObject(consolDocumentRepository.findById(serviceId));
        return appUtil.setDesc(baseDto);
    }

    public BaseDto getSignOffStatus(Long consolId) {
        BaseDto baseDto = new BaseDto();
        try {

            Consol consol = consolRepository.findById(consolId);

            if (consol.getConsolSignOff() == null) {
                baseDto.setResponseObject(null);
            } else {
                baseDto.setResponseObject(consol.getConsolSignOff());
            }

            baseDto.setResponseCode(ErrorCode.SUCCESS);

        } catch (Exception e) {
            log.error("Exception occured at Service sign off", e);
            baseDto.setResponseCode(ErrorCode.FAILED);
        }
        return baseDto;
    }

    public BaseDto executeAutoImportProcess(Long consolId) {
        log.info("Start AutoImportProcessService.executeAutoImportProcess with service id : " + consolId);

        BaseDto baseDto = new BaseDto();
        try {

            autoImportProcessService.markAsComplete(consolId, baseDto);

        } catch (RestException re) {
            log.error("Exception in AutoImportProcessService -> executeAutoImportProcess method ", re);
            baseDto.setResponseCode(re.getMessage());

        } catch (Exception exception) {
            log.error("Exception in AutoImportProcessService -> executeAutoImportProcess method ", exception);

            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(exception);
            log.error("Exception Cause 1 ::: " + exceptionCause1);

            baseDto.setResponseCode(ErrorCode.FAILED);

            if (exceptionCause1.contains(UniqueKey.UK_SHIPMENTATT_REF)) {
                baseDto.setResponseCode(ErrorCode.ATTACHMENT_REFERENCE_NUMBER_DUPLICATED);
            }

            if (exceptionCause1.contains(UniqueKey.UK_CONSOL_CONSOLUID)) {
                baseDto.setResponseCode(ErrorCode.CONSOL_UNIQUE);
            }

            if (exceptionCause1.contains(UniqueKey.UK_CONSLATTCH_REF)) {
                baseDto.setResponseCode(ErrorCode.ATTACHMENT_REFERENCE_NUMBER_DUPLICATED);
            }

            if (exceptionCause1.contains("FK_")) {
                baseDto.setResponseCode(ErrorCode.SAVE_UPDATE_FK_RECORD);
            }

        }

        log.info("End AutoImportProcessService.executeAutoImportProcess with service id : ");

        return appUtil.setDesc(baseDto);

    }

    public BaseDto isConsolCreatedFromShipment(String serviceUid) {
        BaseDto baseDto = new BaseDto();
        try {
            consolValidator.isConsolCreatedFromShipment(serviceUid);
            baseDto.setResponseCode(ErrorCode.SUCCESS);
        } catch (RestException re) {
            log.error("RestException in isConsolCreatedFromShipment method of consol service : " + re);
            baseDto.setResponseCode(re.getMessage());
        } catch (Exception e) {
            log.error("Exception in isConsolCreatedFromShipment method of consol service : " + e);
            baseDto.setResponseCode(ErrorCode.FAILED);
        }
        return baseDto;
    }

    public BaseDto pushSave(Consol entity) {

        BaseDto baseDto = new BaseDto();

        try {
            consolRepository.save(entity);
            baseDto.setResponseCode(ErrorCode.SUCCESS);
        } catch (Exception e) {
            // TODO: handle exception
            baseDto.setResponseCode(ErrorCode.FAILED);
            log.info("exception in push api save", e);

        }
        return appUtil.setDesc(baseDto);
    }

    // used to create Sequence numbers based on report
    public BaseDto generateNUM(GenerateNumDto generateNumDto) {

        BaseDto baseDto = new BaseDto();

        Consol c = null;
        try {
            if (generateNumDto != null) {

                if (generateNumDto.getIsMultiple()) {

                    if (generateNumDto.getConsolId() != null) {
                        c = consolRepository.findById(generateNumDto.getConsolId());
                        if (c != null) {
                            consolSearchImplNew.generateNumbers(generateNumDto, c);
                        } else {
                            throw new RestException(ErrorCode.CONSOL_NEW_NOT_NULL);
                        }
                    } else {
                        log.info("Nothing will generate");
                    }
                }
            }

            baseDto.setResponseCode(ErrorCode.SUCCESS);

        } catch (RestException e) {

        }

        return baseDto;
    }

}
