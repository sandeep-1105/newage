package com.efreightsuite.service;

import com.efreightsuite.common.component.CacheRepository;
import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.ReasonSearchDto;
import com.efreightsuite.dto.SearchRequest;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.ExceptionHelper;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.exception.UniqueKey;
import com.efreightsuite.model.ReasonMaster;
import com.efreightsuite.repository.ReasonMasterRepository;
import com.efreightsuite.search.ReasonSearchImpl;
import com.efreightsuite.util.AppUtil;
import com.efreightsuite.util.SaaSUtil;
import com.efreightsuite.util.ValidateUtil;
import com.efreightsuite.validation.ReasonMasterValidator;
import com.efreightsuite.validation.SearchDtoValidator;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.exception.ExceptionUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.orm.ObjectOptimisticLockingFailureException;
import org.springframework.orm.jpa.JpaObjectRetrievalFailureException;
import org.springframework.stereotype.Service;

/**
 * @author tanveer Date - 07.03.2016 Updated - 11.03.2016
 */

@Service
@Log4j2
public class ReasonMasterService {

    @Autowired
    private
    AppUtil appUtil;

    /**
     * ReasonMasterRepository Object
     */

    @Autowired
    private
    ReasonMasterRepository reasonMasterRepository;

    @Autowired
    SearchDtoValidator searchDtoValidator;

    @Autowired
    private
    ReasonMasterValidator reasonMasterValidator;

    @Autowired
    private
    CacheRepository cacheRepository;

    @Autowired
    private
    ReasonSearchImpl reasonSearchImpl;

    /**
     * Method - getByReasonId Returns ReasonMaster based on given reason id
     *
     * @return BaseDto
     */

    public BaseDto getByReasonId(Long id) {

        log.info("getByReasonId method is called...[" + id + "]");

        final BaseDto baseDto = new BaseDto();

        try {

            ReasonMaster reasonMaster = cacheRepository.get(SaaSUtil.getSaaSId(), ReasonMaster.class.getName(), id,
                    reasonMasterRepository);

            ValidateUtil.notNull(reasonMaster, ErrorCode.REASON_ID_NOT_FOUND);

            log.info("getByReasonId is successfully executed....[" + reasonMaster.getId() + "]");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(reasonMaster);

        } catch (final RestException exception) {

            log.error("Exception in getByReasonId method : ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (final Exception exception) {

            log.error("Exception in getByReasonId method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);
    }

    /**
     * Method - getByReasonCode Returns ReasonMaster based on given reason code
     *
     * @return BaseDto
     */

    public BaseDto getByReasonCode(String reasonCode) {

        log.info("getByReasonCode method is called..." + reasonCode);

        final BaseDto baseDto = new BaseDto();

        try {

            final ReasonMaster reasonMaster = reasonMasterRepository.findByReasonCode(reasonCode);

            ValidateUtil.notNull(reasonMaster, ErrorCode.REASON_CODE_NOT_FOUND);

            log.info("Reason Found Based on the given code : [" + reasonMaster.getReasonCode() + "]");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(reasonMaster);

        } catch (RestException exception) {

            log.error("Exception in getByReasonCode method : ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (Exception exception) {

            log.error("Exception in getByReasonCode method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);
    }

    /**
     * Method - search Returns list of ReasonMaster based on the search keyword
     *
     * @return BaseDto
     */

    public BaseDto search(ReasonSearchDto searchDto) {

        log.info("Search method is called...." + searchDto);

        BaseDto baseDto = new BaseDto();

        try {

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(reasonSearchImpl.search(searchDto));

            log.info("Successfully Searching Reason...");

        } catch (Exception exception) {

            log.error("Exception in Search method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);

    }

    public BaseDto search(SearchRequest searchRequest) {

        log.info("Search method is called...." + searchRequest);

        BaseDto baseDto = new BaseDto();

        try {

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(reasonSearchImpl.search(searchRequest));

            log.info("Successfully Searching Reason...");

        } catch (Exception exception) {

            log.error("Exception in Search method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);

    }

    /**
     * Method - create Creates and returns a new reason master
     *
     * @returns ReasonMaster
     */

    public BaseDto create(ReasonMaster reasonMaster) {

        log.info("Create method is called - reasonMasterId ..");

        final BaseDto baseDto = new BaseDto();

        try {

            reasonMasterValidator.validate(reasonMaster);

            cacheRepository.save(SaaSUtil.getSaaSId(), ReasonMaster.class.getName(), reasonMaster, reasonMasterRepository);

            log.info("Reason Saved successfully.....");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(reasonMaster);

        } catch (RestException exception) {

            log.error("Exception Cause 1 ::: ", exception);
            baseDto.setResponseCode(exception.getMessage());


        } catch (Exception exception) {

            log.error("Exception occured : ", exception);


            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Exception Cause 1 : " + exceptionCause1);

            // General error condition
            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            // Duplicate reason Code

            if (exceptionCause1.contains(UniqueKey.UK_REASON_REASONCODE)) {
                baseDto.setResponseCode(ErrorCode.REASON_CODE_ALREADY_EXIST);

            }

            if (exceptionCause1.contains(UniqueKey.UK_REASON_REASONNAME)) {
                baseDto.setResponseCode(ErrorCode.REASON_NAME_ALREADY_EXIST);
            }


        }

        return appUtil.setDesc(baseDto);
    }

    /**
     * Method - update Updates and returns a updated reason master
     *
     * @param existingReasonMaster
     * @returns ReasonMaster
     */

    public BaseDto update(ReasonMaster existingReasonMaster) {

        log.info("Update method is called - existingReasonMaster : [" + existingReasonMaster.getId() + "]");

        BaseDto baseDto = new BaseDto();

        try {

            ValidateUtil.notNull(existingReasonMaster.getId(), ErrorCode.REASON_ID_NOT_NULL);

            reasonMasterValidator.validate(existingReasonMaster);

            reasonMasterRepository.getOne(existingReasonMaster.getId());

            existingReasonMaster = cacheRepository.save(SaaSUtil.getSaaSId(), ReasonMaster.class.getName(), existingReasonMaster,
                    reasonMasterRepository);

            log.info("ReasonMaster updated successfully....[" + existingReasonMaster.getId() + "]");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(existingReasonMaster);

        } catch (RestException exception) {

            log.error("Exception occured with error code ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (ObjectOptimisticLockingFailureException e) {

            log.error("Error in editing", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_LOCKED_RECORD);

        } catch (JpaObjectRetrievalFailureException e) {

            log.error("Error in editing", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_DELETED_RECORD);

        } catch (Exception exception) {

            log.error("Exception occured : ", exception);

            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Exception Cause 1 ::: " + exceptionCause1);


            // General error condition
            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            // Duplicate reason Code

            if (exceptionCause1.contains(UniqueKey.UK_REASON_REASONCODE)) {
                baseDto.setResponseCode(ErrorCode.REASON_CODE_ALREADY_EXIST);

            }

            if (exceptionCause1.contains(UniqueKey.UK_REASON_REASONNAME)) {
                baseDto.setResponseCode(ErrorCode.REASON_NAME_ALREADY_EXIST);
            }


        }

        return appUtil.setDesc(baseDto);
    }

    /**
     * Deletes the Existing ReasonMaster Based on the Given Id
     *
     * @Returns BaseDto
     */
    public BaseDto delete(Long id) {

        log.info("Delete method is Invoked...[" + id + "]");

        final BaseDto baseDto = new BaseDto();

        try {

            cacheRepository.delete(SaaSUtil.getSaaSId(), ReasonMaster.class.getName(), id, reasonMasterRepository);

            log.info("ReasonMaster Deleted Successfully....[" + id + "]");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

        } catch (RestException exception) {

            log.error("Exception in Delete method : ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (DataIntegrityViolationException exception) {

            log.error("Exception occured : ", exception);

            ExceptionHelper helper = new ExceptionHelper(exception);

            log.error("Foreign key reference : " + helper.getFkReference());

            baseDto.setResponseCode(ErrorCode.ERROR_FK_CONSTRAINT);

        } catch (Exception exception) {

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            log.error("Exception in Delete method : ", exception);

            String rootCause = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Root Cause of Exception : " + rootCause);

            if (rootCause.contains("FK_")) {
                baseDto.setResponseCode(ErrorCode.CANNOT_DELETE_FK_RECORD);
            }
        }

        return appUtil.setDesc(baseDto);
    }

}
