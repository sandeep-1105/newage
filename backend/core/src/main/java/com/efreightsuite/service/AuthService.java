package com.efreightsuite.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.transaction.Transactional;

import com.efreightsuite.common.component.EmailRequestService;
import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.ForgotPasswordDto;
import com.efreightsuite.dto.LoginRequest;
import com.efreightsuite.enumeration.LoginStatus;
import com.efreightsuite.enumeration.SessionStatus;
import com.efreightsuite.enumeration.YesNo;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.model.AesTransportMode;
import com.efreightsuite.model.DefaultMasterData;
import com.efreightsuite.model.EmailRequest;
import com.efreightsuite.model.LocationMaster;
import com.efreightsuite.model.LoginHistory;
import com.efreightsuite.model.ResetPasswordHistory;
import com.efreightsuite.model.Role;
import com.efreightsuite.model.RoleHasFeature;
import com.efreightsuite.model.UserCompany;
import com.efreightsuite.model.UserCompanyLocation;
import com.efreightsuite.model.UserProfile;
import com.efreightsuite.repository.AesTransportModeRepository;
import com.efreightsuite.repository.DefaultMasterDataRepository;
import com.efreightsuite.repository.LocationMasterRepository;
import com.efreightsuite.repository.LoginHistoryRepository;
import com.efreightsuite.repository.ResetPasswordHistoryRepository;
import com.efreightsuite.repository.UserProfileRepository;
import com.efreightsuite.service.mailer.MailerUtil;
import com.efreightsuite.util.AppUtil;
import com.efreightsuite.util.AuthUtil;
import com.efreightsuite.util.SaaSUtil;
import com.efreightsuite.util.TimeUtil;
import com.efreightsuite.util.ValidateUtil;
import eu.bitwalker.useragentutils.UserAgent;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class AuthService {

    @Value("${session.timeout}")
    private
    int sessionTimeout;

    @Value("${forgot.password.linkExpire}")
    private
    int forgotPasswordLinkExpire;

    @Value("${forgot.password.url}")
    private
    String forgotPasswordURL;

    @Value("${forgot.password.fromEmailId}")
    private
    String fromEmailId;

    @Value("${forgot.password.fromEmailPassword}")
    private
    String fromEmailPassword;

    @Value("${forgot.password.subject}")
    private
    String subject;

    @Value("${forgot.password.emailBodyMessage}")
    private
    String emailBodyMessage;

    @Value("${forgot.password.smtp.port}")
    private
    int emailSMTPPort;

    @Value("${forgot.password.smtp.host}")
    private
    String emailSMTPHost;

    @Value("${efs.report.application.base.url}")
    private
    String reportServerBasePath;

    @Autowired
    private
    MailerUtil mailerUtil;

    @Autowired
    private
    DefaultMasterDataRepository defaultMasterDataRepository;

    @Autowired
    private
    LoginHistoryRepository loginHistoryRepo;

    @Autowired
    private
    EmailRequestService emailRequestService;

    @Autowired
    private
    AppUtil appUtil;

    @Autowired
    private
    UserProfileRepository userProfileRepository;

    @Autowired
    private
    ResetPasswordHistoryRepository resetPasswordHistoryRepository;

    @Autowired
    private
    LocationMasterRepository locationMasterRepository;

    @Autowired
    private
    RecordAccessLevelService recordAccessLevelService;

    @Autowired
    private
    AesTransportModeRepository aesTransportModeRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    public static UserProfile getCurrentUser() {

        if (SecurityContextHolder.getContext().getAuthentication() == null) {
            return null;
        }

        Object authObject = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (authObject == null || authObject.equals("anonymousUser")) {
            return null;
        } else {
            if (authObject instanceof UserProfile) {
                return (UserProfile) authObject;
            } else {
                User user = (User) authObject;
                return new UserProfile(user);
            }
        }
    }

    public ResponseEntity<BaseDto> login(LoginRequest loginRequest, HttpServletRequest request) {
        try {
            UserProfile userProfile = userProfileRepository.findByUserName(loginRequest.getUserName());
            if (userProfile == null) {
                BaseDto response = new BaseDto(ErrorCode.UNAUTHORIZED_USER, appUtil.getDesc(ErrorCode.UNAUTHORIZED_USER));
                return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(response);
            }

            if (userProfile.getUserCompanyList() == null
                    || userProfile.getUserCompanyList().isEmpty()) {
                BaseDto response = new BaseDto(ErrorCode.USER_LOCATION_NOT_ASSOCIATE, appUtil.getDesc(ErrorCode.USER_LOCATION_NOT_ASSOCIATE));
                return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(response);
            }

            setUserProfile(userProfile);


            if (loginRequest.getUserName().equals(userProfile.getUserName())
                    && passwordEncoder.matches(loginRequest.getPassword(), userProfile.getPassword())
                    && userProfile.getResetTokenKey() == null) {
                return setupUserOnSuccessfulLogin(request, userProfile);
            } else {
                log.info("Login failed....");
                BaseDto response = new BaseDto(ErrorCode.UNAUTHORIZED_USER, appUtil.getDesc(ErrorCode.UNAUTHORIZED_USER));

                if (userProfile.getEmployee() != null) {
                    createLoginHistory(userProfile, request, LoginStatus.FAILED, null);
                }
                return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(response);
            }
        } catch (Exception e) {
            log.error("Exception ", e);
            String errorCode = e instanceof RestException
                    ? e.getMessage()
                    : ErrorCode.ERROR_GENERIC;
            BaseDto response = new BaseDto(errorCode, appUtil.getDesc(errorCode));
            HttpStatus status = e instanceof RestException
                    ? HttpStatus.UNAUTHORIZED
                    : HttpStatus.INTERNAL_SERVER_ERROR;
            return ResponseEntity.status(status).body(response);
        }


    }

    private ResponseEntity<BaseDto> setupUserOnSuccessfulLogin(HttpServletRequest request, UserProfile userProfile) {
        createLoginHistory(userProfile, request, LoginStatus.SUCCESS, SessionStatus.CREATED);

        userProfile.setPassword(null);

        Map<String, Object> map = new HashMap<>();

        Long selectedLocationId = userProfile.getSelectedUserLocation().getId();
        for (DefaultMasterData data : defaultMasterDataRepository.getByLocation(selectedLocationId)) {
            map.put(data.getCode(), data.getValue());
        }
        //for aes transport mode
        AesTransportMode aesTransportMode = aesTransportModeRepository.findByTransportName("40-Air,Non Containerized");
        map.put("aes.transportmode.default", aesTransportMode);

        userProfile.setAppMasterData(map);

        LocationMaster location = locationMasterRepository.findById(selectedLocationId);
        AppUtil.setPartyToNull(location.getPartyMaster());
        userProfile.setSelectedUserLocation(location);
        AppUtil.setPartyToNull(userProfile.getSelectedUserLocation().getPartyMaster());
        userProfile.getSelectedUserLocation().setTmpPartyMaster(userProfile.getSelectedUserLocation().getPartyMaster());

        getFeatureList(userProfile);

        // Create a session id if login is successful
        HttpSession session = request.getSession(true);
        session.setMaxInactiveInterval(sessionTimeout);

        userProfile.setSessionId(session.getId());

        userProfile.setRecordAccessLevelProcessDTO(recordAccessLevelService.getRecordAccessLevelForAUser(userProfile.getId()));
        userProfile.setReportServerBasePath(reportServerBasePath);
        session.setAttribute("userProfile", userProfile);

        List<GrantedAuthority> authorities = new ArrayList<>();

        for (String key : userProfile.getFeatureMap().keySet()) {
            authorities.add(new SimpleGrantedAuthority("ROLE_" + key));
        }

        Authentication authentication = new UsernamePasswordAuthenticationToken(userProfile,
                SaaSUtil.getSaaSId(), authorities);

        SecurityContextHolder.getContext().setAuthentication(authentication);

        log.info("Login successful.");
        BaseDto response = new BaseDto(ErrorCode.SUCCESS, appUtil.getDesc(ErrorCode.SUCCESS), userProfile);
        return ResponseEntity.ok(response);
    }

    private void setUserProfile(UserProfile userProfile) {
        if (userProfile.getUserCompanyList().size() == 1) {
            setUserProfileSingleCompany(userProfile);
        } else {
            setUserProfileMultipleCompany(userProfile);
        }
    }

    private void setUserProfileMultipleCompany(UserProfile userProfile) {

        boolean isFound = false;
        for (UserCompany ucl : userProfile.getUserCompanyList()) {
            if (ucl.getYesNo() == YesNo.Yes || ucl.getYesNo().equals(YesNo.Yes)) {
                userProfile.setEmployee(ucl.getEmployee());
                userProfile.setSelectedCompany(ucl.getCompanyMaster());
                if (ucl.getUserCompanyLocationList() == null || ucl.getUserCompanyLocationList().size() == 0) {
                    throw new RestException(ErrorCode.USER_LOCATION_NOT_ASSOCIATE);
                }

                isFound = setUserProfileSelectedUserLocation(ucl, userProfile);

                break;
            }
        }

        if (!isFound) {
            throw new RestException(ErrorCode.USER_LOCATION_NOT_ASSOCIATE);
        }
    }

    private boolean setUserProfileSelectedUserLocation(UserCompany ucl, UserProfile userProfile) {
        boolean isFound = false;
        if (ucl.getUserCompanyLocationList().size() == 1) {
            userProfile.setSelectedUserLocation(ucl.getUserCompanyLocationList().get(0).getLocationMaster());
            isFound = true;
        } else {
            for (UserCompanyLocation uclList : ucl.getUserCompanyLocationList()) {
                if (uclList.getYesNo() == YesNo.Yes || uclList.getYesNo().equals(YesNo.Yes)) {
                    userProfile.setSelectedUserLocation(uclList.getLocationMaster());
                    isFound = true;
                    break;
                }
            }
        }
        return isFound;
    }

    private void setUserProfileSingleCompany(UserProfile userProfile) {
        UserCompany userCompany = userProfile.getUserCompanyList().get(0);
        userProfile.setEmployee(userCompany.getEmployee());
        userProfile.setSelectedCompany(userCompany.getCompanyMaster());
        if (userCompany.getUserCompanyLocationList() == null ||
                userCompany.getUserCompanyLocationList().isEmpty()) {
            throw new RestException(ErrorCode.USER_LOCATION_NOT_ASSOCIATE);
        }

        if (userCompany.getUserCompanyLocationList().size() == 1) {
            userProfile.setSelectedUserLocation(userCompany.getUserCompanyLocationList().get(0).getLocationMaster());
        } else {
            boolean isFound = false;
            for (UserCompanyLocation ucl : userCompany.getUserCompanyLocationList()) {
                if (ucl.getYesNo() == YesNo.Yes || ucl.getYesNo().equals(YesNo.Yes)) {
                    userProfile.setSelectedUserLocation(ucl.getLocationMaster());
                    isFound = true;
                    break;
                }
            }
            if (!isFound) {
                throw new RestException(ErrorCode.USER_LOCATION_NOT_ASSOCIATE);
            }
        }
    }

    private void getFeatureList(UserProfile userProfile) {

        Map<String, Boolean> featureMap = new HashMap<>();

        for (Role role : userProfile.getRoleList()) {
            if (role.getFeatureList() == null || role.getFeatureList().isEmpty()) {
                continue;
            }
            buildFeatureMap(role, featureMap);
        }
        userProfile.setFeatureMap(featureMap);
        userProfile.setRoleList(null);
    }

    private void buildFeatureMap(Role role, Map<String, Boolean> featureMap) {

        for (RoleHasFeature rhf : role.getFeatureList()) {
            if (rhf.getValList() != null) {
                featureMap.put(rhf.getValList(), true);
            }
            if (rhf.getValCreate() != null) {
                featureMap.put(rhf.getValCreate(), true);
            }
            if (rhf.getValModify() != null) {
                featureMap.put(rhf.getValModify(), true);
            }
            if (rhf.getValView() != null) {
                featureMap.put(rhf.getValView(), true);
            }
            if (rhf.getValDelete() != null) {
                featureMap.put(rhf.getValDelete(), true);
            }
            if (rhf.getValDownload() != null) {
                featureMap.put(rhf.getValDownload(), true);
            }
        }
    }

    public void logout(HttpServletRequest request) {

        log.debug("Logout Method is Invoked");

        HttpSession session = request.getSession(false);

        if (session != null) {

            List<LoginHistory> loginHistoryList = loginHistoryRepo
                    .findBySessionIdAndSessionStatus(request.getSession().getId(), SessionStatus.CREATED);

            if (loginHistoryList != null && !loginHistoryList.isEmpty()) {

                for (LoginHistory loginHistory : loginHistoryList) {

                    loginHistory.setSessionStatus(SessionStatus.INVALIDATED);

                    loginHistory.setLogoutTime(new Date());

                }

                loginHistoryRepo.save(loginHistoryList);
            }

            session.setAttribute("LOGOUTSTATUS", "USER_LOGOUT");

            session.invalidate();

        } else {

            log.debug("Session is already invalidated");
        }
    }

    private void createLoginHistory(UserProfile userProfile, HttpServletRequest request,
                                    LoginStatus loginStatus, SessionStatus sessionStatus) {

        LoginHistory loginHistory = new LoginHistory();

        loginHistory.setUserProfile(userProfile);
        loginHistory.setUserName(userProfile.getUserName());
        loginHistory.setEmployee(userProfile.getEmployee());
        loginHistory.setEmployeeCode(userProfile.getEmployee().getEmployeeCode());
        loginHistory.setSessionId(request.getSession().getId());
        loginHistory.setLoginTime(new Date());

        UserAgent userAgent = UserAgent.parseUserAgentString(request.getHeader("User-Agent"));
        if (userAgent != null) {
            if (userAgent.getBrowser() != null) {
                loginHistory.setBrowser(userAgent.getBrowser().getName());
            }

            if (userAgent.getBrowserVersion() != null) {
                loginHistory.setBrowserVersion(userAgent.getBrowserVersion().getVersion());
            }

            if (userAgent.getOperatingSystem() != null) {
                loginHistory.setPlatform(userAgent.getOperatingSystem().getName());
            }

            if (userAgent.getOperatingSystem().getDeviceType() != null) {
                loginHistory.setDeviceName(userAgent.getOperatingSystem().getDeviceType().getName());
            }
        }

        String ipAddress = request.getHeader("X-FORWARDED-FOR");

        if (ipAddress == null) {
            ipAddress = request.getRemoteAddr();
        }

        loginHistory.setSourceIp(ipAddress);
        loginHistory
                .setSaasId(SaaSUtil.getSaaSId() == "" || SaaSUtil.getSaaSId() == null ? null : SaaSUtil.getSaaSId());
        loginHistory.setSessionStatus(sessionStatus);
        loginHistory.setLoginStatus(loginStatus);

        loginHistoryRepo.save(loginHistory);

        log.debug("Login History Created [ " + userProfile.getUserName() + " ]");

    }

    @Transactional
    public BaseDto sendForgotPasswordLink(ForgotPasswordDto forgotPasswordDto) {

        log.debug("SendForgotPasswordLink Method Invoked.");

        BaseDto baseDto = new BaseDto();

        try {

            ValidateUtil.notNull(forgotPasswordDto.getEmailId(), ErrorCode.EMAILID_NOT_NULL);
            ValidateUtil.isValidEmail(forgotPasswordDto.getEmailId(), ErrorCode.INVALID_EMAILID);

            UserProfile userProfile = userProfileRepository.findByEmployee(forgotPasswordDto.getEmailId()).get(0);
            ValidateUtil.notNull(userProfile, ErrorCode.EMAILID_NOT_PRESENT);

            // Update ResetTokenKey
            String resetTokenKey = AuthUtil.genarateResetTokenKey();
            userProfile.setResetTokenKey(resetTokenKey);
            userProfile.setResetTokenDate(new Date());
            userProfileRepository.save(userProfile);
            log.debug("ResetTokenKey and ResetTokenDate updated in User Profile");

            // insert record into reset_password_history
            ResetPasswordHistory resetPasswordHistory = new ResetPasswordHistory();

            resetPasswordHistory.setCreateDate(new Date());
            resetPasswordHistory.setCreateUser(userProfile.getUserName());
            resetPasswordHistory.setLastUpdatedDate(new Date());
            resetPasswordHistory.setLastUpdatedUser(userProfile.getUserName());

            resetPasswordHistory.setExpiryDate(new Date());
            resetPasswordHistory.setUrl(forgotPasswordURL);
            resetPasswordHistory.setStatus("PENDING");
            resetPasswordHistory.setEmailId(forgotPasswordDto.getEmailId());
            resetPasswordHistory.setErrorCode(ErrorCode.FORGOT_PASSWORD_EMAIL_SENDING);
            resetPasswordHistory.setErrorDescription(appUtil.getDesc(ErrorCode.FORGOT_PASSWORD_EMAIL_SENDING));
            resetPasswordHistory = resetPasswordHistoryRepository.save(resetPasswordHistory);

            // To form Reset URL
            StringBuilder forgotPasswordURLBuilder = new StringBuilder();
            forgotPasswordURLBuilder.append(forgotPasswordURL);
            forgotPasswordURLBuilder.append("?rpt=");
            forgotPasswordURLBuilder.append(resetTokenKey);
            forgotPasswordURLBuilder.append("&rphid=");
            forgotPasswordURLBuilder.append(resetPasswordHistory.getId());
            forgotPasswordURLBuilder.append("&sid=");
            forgotPasswordURLBuilder.append(userProfile.getId());

            // To build Email Body
            String emailBody = emailBodyMessage +
                    " <a href='" +
                    forgotPasswordURLBuilder.toString() +
                    "'>Reset Password</a>";

            EmailRequest emailRequest = new EmailRequest();
            emailRequest.setFromEmailId(fromEmailId);
            emailRequest.setFromEmailIdPassword(fromEmailPassword);
            emailRequest.setToEmailIdList(userProfile.getEmployeeEmail());
            emailRequest.setSubject(subject);
            emailRequest.setEmailBody(emailBody);
            emailRequest.setManualSystemTrack(TimeUtil.getCreateSystemTrack(userProfile));
            emailRequest.setEmailType("text/html");
            emailRequest.setSmtpServerAddress(emailSMTPHost);
            emailRequest.setPort(emailSMTPPort);

            processEmail(emailRequest, userProfile);

            log.debug("Email sent successfully.. ");

            // Update reset_password_history
            resetPasswordHistory.setUrl(forgotPasswordURLBuilder.toString());
            resetPasswordHistory.setErrorCode(ErrorCode.FORGOT_PASSWORD_EMAIL_SENT_SUCCESS);
            resetPasswordHistory.setErrorDescription(appUtil.getDesc(ErrorCode.FORGOT_PASSWORD_EMAIL_SENT_SUCCESS));
            resetPasswordHistory = resetPasswordHistoryRepository.save(resetPasswordHistory);

            baseDto.setResponseCode(ErrorCode.SUCCESS);

        } catch (RestException ipfe) {
            log.error("RestException Occured. ", ipfe);
            baseDto.setResponseCode(ipfe.getMessage());
            saveResetPasswordHistory("FAILED", ipfe.getMessage(), forgotPasswordDto.getEmailId());

        } catch (Exception e) {
            log.error("Exception Occured.", e);
            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);
            saveResetPasswordHistory("FAILED", ErrorCode.ERROR_GENERIC, forgotPasswordDto.getEmailId());
        }
        log.debug("AuthService.sendForgotPasswordLink ended");
        return appUtil.setDesc(baseDto);
    }

    @Transactional
    public BaseDto updatePassword(ForgotPasswordDto forgotPasswordDto) {
        BaseDto respDto = new BaseDto();
        ResetPasswordHistory resetPasswordHistory = null;
        UserProfile userProfile = null;
        try {

            ValidateUtil.notNull(forgotPasswordDto, ErrorCode.PASSWORD_NOT_NULL);
            ValidateUtil.notNull(forgotPasswordDto.getPassword(), ErrorCode.PASSWORD_NOT_NULL);

            resetPasswordHistory = resetPasswordHistoryRepository
                    .getOne(Long.parseLong(forgotPasswordDto.getResetPasswordId()));

            ValidateUtil.notNull(resetPasswordHistory, ErrorCode.INVALID_EMAILID);

            userProfile = userProfileRepository.findOne(Long.parseLong(forgotPasswordDto.getResetPasswordSid()));

            if (userProfile == null) {
                resetPasswordHistory.setErrorCode(ErrorCode.INVALID_EMAILID);
                resetPasswordHistory.setErrorDescription("UserProfile is not found");
                resetPasswordHistoryRepository.save(resetPasswordHistory);
                respDto.setResponseCode(ErrorCode.INVALID_EMAILID);
                log.debug("UserProfile is not found");
                return appUtil.setDesc(respDto);

            }

            Date expiryDate = TimeUtil.addMinutesToDate(forgotPasswordLinkExpire, resetPasswordHistory.getExpiryDate());
            Date localDateTime = new Date();

            if (localDateTime.before(expiryDate) && userProfile.getResetTokenKey() != null
                    && userProfile.getResetTokenDate() != null
                    && userProfile.getResetTokenKey().equals(forgotPasswordDto.getResetPasswordToken())) {

                userProfile.setResetTokenKey(null);
                userProfile.setResetTokenDate(null);
                userProfile.setPassword(passwordEncoder.encode(forgotPasswordDto.getPassword()));
                userProfileRepository.save(userProfile);

                resetPasswordHistory.setStatus("SUCCESS");
                resetPasswordHistory.setErrorCode(ErrorCode.SUCCESS);

                resetPasswordHistory.setLastUpdatedDate(new Date());
                resetPasswordHistory.setLastUpdatedUser(userProfile.getUserName());

                resetPasswordHistory.setErrorDescription("New password updated successfully");

                resetPasswordHistoryRepository.save(resetPasswordHistory);

                respDto.setResponseCode(ErrorCode.SUCCESS);
                log.debug("New password updated successfully");

            } else {
                resetPasswordHistory.setErrorCode(ErrorCode.RESET_EMAIL_EXPIRED);
                resetPasswordHistory.setErrorDescription("Password reset link Experied");
                resetPasswordHistory.setStatus("FAILED");

                resetPasswordHistory.setLastUpdatedDate(new Date());
                resetPasswordHistory.setLastUpdatedUser(userProfile.getUserName());

                resetPasswordHistoryRepository.save(resetPasswordHistory);

                respDto.setResponseCode(ErrorCode.RESET_EMAIL_EXPIRED);
                log.debug("Password reset link Expired");

            }

        } catch (RestException ipfe) {
            log.error("BadRequestException Occured. ", ipfe);
            String errorCode = ipfe.getMessage().split(" ")[0];
            respDto.setResponseCode(errorCode);
            if (null != resetPasswordHistory) {
                resetPasswordHistory.setStatus("FAILED");
                resetPasswordHistory.setErrorCode(errorCode);
                resetPasswordHistory.setErrorDescription("Bad client request");
                resetPasswordHistory.setLastUpdatedDate(new Date());
            }
            if (userProfile != null) {
                resetPasswordHistory.setLastUpdatedUser(userProfile.getUserName());
            }
            resetPasswordHistoryRepository.save(resetPasswordHistory);

        } catch (Exception e) {
            log.error("Exception Occured.", e);
            respDto.setResponseCode(ErrorCode.ERROR_GENERIC);
            if (null != resetPasswordHistory) {
                resetPasswordHistory.setStatus("FAILED");
                resetPasswordHistory.setErrorCode(ErrorCode.ERROR_GENERIC);
                resetPasswordHistory.setErrorDescription(appUtil.getDesc(ErrorCode.ERROR_GENERIC));

                resetPasswordHistory.setLastUpdatedDate(new Date());
            }
            if (userProfile != null) {
                resetPasswordHistory.setLastUpdatedUser(userProfile.getUserName());
            }

            resetPasswordHistoryRepository.save(resetPasswordHistory);
        }
        log.debug("AuthService.updatePassword ended.");
        return appUtil.setDesc(respDto);

    }

    private void saveResetPasswordHistory(String status, String errorCode, String emailId) {
        ResetPasswordHistory resetPasswordHistory = new ResetPasswordHistory();

        resetPasswordHistory.setExpiryDate(new Date());
        resetPasswordHistory.setUrl(forgotPasswordURL);
        resetPasswordHistory.setStatus(status);
        resetPasswordHistory.setErrorCode(errorCode);
        resetPasswordHistory.setErrorDescription(appUtil.getDesc(errorCode));
        resetPasswordHistory.setEmailId(emailId);
        resetPasswordHistory.setCreateDate(new Date());
        resetPasswordHistory.setLastUpdatedDate(new Date());

        resetPasswordHistoryRepository.save(resetPasswordHistory);
    }

    @Async
    private void processEmail(EmailRequest emailRequest, UserProfile userProfile) {
        emailRequestService.saveAndSendEmail(emailRequest, userProfile, mailerUtil.getLogo(userProfile.getSelectedUserLocation()));
    }
}
