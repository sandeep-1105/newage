package com.efreightsuite.service;

import java.util.List;

import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.enumeration.YesNo;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.model.DocumentIssueRestriction;
import com.efreightsuite.model.InvoiceCreditNote;
import com.efreightsuite.model.ShipmentServiceDetail;
import com.efreightsuite.model.TosMaster;
import com.efreightsuite.repository.DocumentIssueRestrictionRepository;
import com.efreightsuite.repository.InvoiceCreditNoteRepository;
import com.efreightsuite.repository.ShipmentRepository;
import com.efreightsuite.repository.ShipmentServiceDetailRepository;
import com.efreightsuite.repository.StockRepository;
import com.efreightsuite.util.AppUtil;
import com.efreightsuite.util.ValidateUtil;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class ShipmentInvoiceValidationService {

    @Autowired
    private
    AppUtil appUtil;

    @Autowired
    ShipmentRepository shipmentRepository;

    @Autowired
    StockRepository stockRepository;

    @Autowired
    private
    ShipmentServiceDetailRepository shipmentServiceDetailRepository;

    @Autowired
    private
    DocumentIssueRestrictionRepository documentIssueRestrictionRepository;

    @Autowired
    private
    InvoiceCreditNoteRepository invoiceCreditNoteRepository;

    public BaseDto shipmentInvoicCheckingByServiceDetailId(List<Long> serviceDetailId) {

        log.info("ShipmentService -> getById method is called....");

        BaseDto baseDto = new BaseDto();
        List<InvoiceCreditNote> invoiceList = null;
        StringBuilder serviceId = new StringBuilder();

        for (int i = 0; i < serviceDetailId.size(); i++) {

            try {
                ShipmentServiceDetail shipmentServiceDetail = shipmentServiceDetailRepository.findById(serviceDetailId.get(i));
                ValidateUtil.notNull(shipmentServiceDetail, ErrorCode.SHIPMENT_INVOICE_CHECKING_INVALID_SHIPMENT_REQUESTED);
                ValidateUtil.notNull(shipmentServiceDetail.getCompany(), ErrorCode.SHIPMENT_INVOICE_CHECKING_INVALID_SHIPMENT_COMPANY);
                ValidateUtil.notNull(shipmentServiceDetail.getCompany().getId(), ErrorCode.SHIPMENT_INVOICE_CHECKING_INVALID_SHIPMENT_COMPANY);
                ValidateUtil.notNull(shipmentServiceDetail.getLocation(), ErrorCode.SHIPMENT_INVOICE_CHECKING_INVALID_SHIPMENT_LOCATION);
                ValidateUtil.notNull(shipmentServiceDetail.getLocation().getId(), ErrorCode.SHIPMENT_INVOICE_CHECKING_INVALID_SHIPMENT_LOCATION);
        /*
		 * //NEW SHIPMENT COMMENTS
		 * ValidateUtil.notNull(shipmentServiceDetail.getShipment().getTosCode(), ErrorCode.SHIPMENT_INVOICE_CHECKING_INVALID_SHIPMENT_TERM);
		ValidateUtil.notNull(shipmentServiceDetail.getShipment().getTosMaster(), ErrorCode.SHIPMENT_INVOICE_CHECKING_INVALID_SHIPMENT_TERM);
		ValidateUtil.notNull(shipmentServiceDetail.getShipment().getTosMaster().getId(), ErrorCode.SHIPMENT_INVOICE_CHECKING_INVALID_SHIPMENT_TERM);
		Long[] tosIds = {
				shipmentServiceDetail.getShipment().getTosMaster().getId()
		};*/

//		log.info("Location Id - ["+shipmentServiceDetail.getLocation().getId()+"] Service Id - ["+shipmentServiceDetail.getServiceMaster().getId()+"]"+"Company Id - ["+shipmentServiceDetail.getCompany().getId()+"]"+"TOS ID - ["+tosIds+"]");

                DocumentIssueRestriction documentIssueRestriction = documentIssueRestrictionRepository.findExceptionalTermByCompanyMasterAndLocationMasterAndServiceMaster(
                        shipmentServiceDetail.getCompany().getId(), shipmentServiceDetail.getLocation().getId(), shipmentServiceDetail.getServiceMaster().getId());
                log.info("documentIssueRestriction Id - [" + documentIssueRestriction + "]");

                boolean chkInvoice = true;
                if (documentIssueRestriction == null || documentIssueRestriction.getId() == null
                        || documentIssueRestriction.getEnableIssueRestriction() == null) {
                    chkInvoice = true;
                } else if (documentIssueRestriction.getEnableIssueRestriction().equals(YesNo.No)) {
                    chkInvoice = false;
                } else if (documentIssueRestriction.getEnableIssueRestriction().equals(YesNo.Yes)) {
                    log.info("IssueRestriction Enabled");
                    if (documentIssueRestriction.getEnableInvoiceChecking() == null) {
                        chkInvoice = true;
                    } else if (documentIssueRestriction.getEnableInvoiceChecking().equals(YesNo.Yes)) {
                        log.info("InvoiceChecking Enabled");
                        if (documentIssueRestriction.getTosList() != null && !documentIssueRestriction.getTosList().isEmpty()) {
                            for (TosMaster tos : documentIssueRestriction.getTosList()) {
						/*//NEW SHIPMENT COMMENTS
						if(shipmentServiceDetail.getShipment().getTosMaster().getId().equals(tos.getId())) {
							log.info("Is Exceptional Terms of Shipmemnt - "+shipmentServiceDetail.getShipment().getTosMaster().getId() );
							chkInvoice = false;
						}
						*/
                            }
                        }
                    } else if (documentIssueRestriction.getEnableInvoiceChecking().equals(YesNo.No)) {
                        chkInvoice = false;
                    }
                }
                if (chkInvoice) {
                    invoiceList = invoiceCreditNoteRepository.findByShipmentServiceDetail(serviceDetailId.get(i));
                    if (invoiceList == null || invoiceList.isEmpty()) {
                        serviceId.append(shipmentServiceDetail.getServiceUid()).append(",");
                    }
                }
                if (serviceId != null && serviceId.toString() != "" && i == serviceDetailId.size() - 1) {
                    serviceId = new StringBuilder(StringUtils.stripEnd(serviceId.toString(), ","));
                    baseDto.getParams().add(serviceId.toString());
                   // throw new RestException(ErrorCode.SHIPMENT_INVOICE_CHECKING_INVOICE_NOT_CREATED);
                }
                baseDto.setResponseCode(ErrorCode.SUCCESS);
                baseDto.setResponseObject(null);
            } catch (RestException re) {
                log.error("Exception in ShipmentService -> getById method ", re);
                baseDto.setResponseCode(re.getMessage());

            } catch (Exception e) {
                log.error("Exception in ShipmentService -> getById method ", e);
                baseDto.setResponseCode(ErrorCode.FAILED);
            }
        }
        return appUtil.setDesc(baseDto);
    }
}


