package com.efreightsuite.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.model.RecentHistory;
import com.efreightsuite.model.UserProfile;
import com.efreightsuite.repository.RecentHistoryRepository;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class RecentHistoryService {

    @Autowired
    private
    RecentHistoryRepository recentHistoryRepository;

    @Value("${recent.historyMaxRecords}")
    private
    int maxSize;

    public BaseDto getAllRecentHistoryByUserAndLocation() {
        log.info("Get All RecentHistory By User And Location method is called");
        BaseDto baseDto = new BaseDto();
        UserProfile userPrincipal = null;
        try {
            userPrincipal = AuthService.getCurrentUser();
            if (userPrincipal != null) {
                Long userId = userPrincipal.getId();
                Long locationId = userPrincipal.getSelectedUserLocation().getId();
                log.info("Requested User Id : " + userId + " ; Location Id : " + locationId);
                Page<RecentHistory> page = recentHistoryRepository.findAllByLocationIdAndUserId(locationId, userId, new PageRequest(0, maxSize, new Sort(Direction.DESC, "createdDate")));
                baseDto.setResponseObject(page);
                baseDto.setResponseCode(ErrorCode.SUCCESS);
            }
        } catch (Exception ex) {
            log.error("Exception on Getting Recent History :: ", ex);
            baseDto.setResponseCode(ErrorCode.FAILED);
        }
        return baseDto;
    }


    public BaseDto getGroupByRecentHistoryByUserAndLocation() {
        log.info("Get All RecentHistory By User And Location method is called");
        BaseDto baseDto = new BaseDto();
        UserProfile userPrincipal = null;
        try {
            userPrincipal = AuthService.getCurrentUser();
            if (userPrincipal != null) {
                Long userId = userPrincipal.getId();
                Long locationId = userPrincipal.getSelectedUserLocation().getId();
                log.info("Requested User Id : " + userId + " ; Location Id : " + locationId);

                List<String> categories = Arrays.asList("Master Shipment", "Shipment", "Invoice", "Quotation", "Enquiry");
                List<RecentHistory> recentHistoryList = new ArrayList<>();
                for (String cat : categories) {
                    Page<RecentHistory> page = recentHistoryRepository.findAllByLocationIdAndUserIdAndStateCategory(locationId, userId, cat, new PageRequest(0, 5, new Sort(Direction.DESC, "createdDate")));
                    if (page != null && page.getNumberOfElements() > 0) {
                        recentHistoryList.addAll(page.getContent());
                    }
                }
                baseDto.setResponseObject(recentHistoryList);
                baseDto.setResponseCode(ErrorCode.SUCCESS);
            }
        } catch (Exception ex) {
            log.error("Exception on Getting Recent History :: ", ex);
            baseDto.setResponseCode(ErrorCode.FAILED);
        }
        return baseDto;
    }

    public BaseDto saveRecentHistory(RecentHistory recentHistory) {
        log.info("Save RecentHistory for User By Location method is called");
        UserProfile userPrincipal = null;
        BaseDto baseDto = new BaseDto();
        try {
            userPrincipal = AuthService.getCurrentUser();
            if (userPrincipal != null) {
                Long userId = userPrincipal.getId();
                Long locationId = userPrincipal.getSelectedUserLocation().getId();
                log.info("Requested User Id : " + userId + " ; Location Id : " + locationId);
                if (userId != null && locationId != null) {
                    recentHistory.setUserId(userId);
                    recentHistory.setLocationId(locationId);
                    recentHistory.setCreatedDate(new Date());
                    RecentHistory recentHistoryResponse = recentHistoryRepository.save(recentHistory);
                    baseDto.setResponseObject(recentHistoryResponse);
                    baseDto.setResponseCode(ErrorCode.SUCCESS);
                }
            }
        } catch (Exception ex) {
            log.error("Exception on Saving Recent History :: ", ex);
            baseDto.setResponseCode(ErrorCode.FAILED);
        }
        return baseDto;
    }


    public BaseDto deleteAllRecentHistoryByUserAndLocation() {
        log.info("Delete All RecentHistory By User And Location method is called");
        BaseDto baseDto = new BaseDto();
        UserProfile userPrincipal = null;
        try {
            userPrincipal = AuthService.getCurrentUser();
            if (userPrincipal != null) {
                Long userId = userPrincipal.getId();
                Long locationId = userPrincipal.getSelectedUserLocation().getId();
                log.info("Requested User Id : " + userId + " ; Location Id : " + locationId);
                int count = recentHistoryRepository.deleteAllByLocationIdAndUserId(locationId, userId);
                log.info("Total Delete Count  : " + count);
                baseDto.setResponseCode(ErrorCode.SUCCESS);
            }
        } catch (Exception ex) {
            log.error("Exception on Deleting Recent History :: ", ex);
            baseDto.setResponseCode(ErrorCode.FAILED);
        }
        return baseDto;
    }
}
