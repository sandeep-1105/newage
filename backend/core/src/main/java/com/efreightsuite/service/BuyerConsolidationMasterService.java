package com.efreightsuite.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.efreightsuite.common.component.CacheRepository;
import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.BuyerConsolidationSearchDto;
import com.efreightsuite.dto.SearchRespDto;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.ExceptionHelper;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.exception.UniqueKey;
import com.efreightsuite.model.BuyerConsolidationMaster;
import com.efreightsuite.repository.BuyerConsolidationRepository;
import com.efreightsuite.util.AppUtil;
import com.efreightsuite.util.SaaSUtil;
import com.efreightsuite.util.StaticSearchQuery;
import com.efreightsuite.util.ValidateUtil;
import com.efreightsuite.validation.BuyerConsolidationMasterValidator;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.exception.ExceptionUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.orm.ObjectOptimisticLockingFailureException;
import org.springframework.orm.jpa.JpaObjectRetrievalFailureException;
import org.springframework.stereotype.Service;

/**
 * @author Sandhanapandian
 */

@Log4j2
@Service
public class BuyerConsolidationMasterService {

    @Autowired
    private
    AppUtil appUtil;

    @Autowired
    private
    BuyerConsolidationRepository buyerConsolidationMasterRepository;

    @Autowired
    private
    CacheRepository cacheRepository;

    @Autowired
    private
    BuyerConsolidationMasterValidator buyerConsolidationMasterValidator;
/*
    @Autowired
	TosSearchImpl tosSearchImpl;*/


    @Autowired
    private
    EntityManager entityManager;

    public BaseDto get(Long id) {

        log.info("get method is called.....[" + id + "]");

        BaseDto baseDto = new BaseDto();

        try {

            ValidateUtil.notNull(id, ErrorCode.BUYER_CONSOLIDATION_ID_NOT_NULL);

            BuyerConsolidationMaster buyerConsolidationMaster = cacheRepository.get(SaaSUtil.getSaaSId(), BuyerConsolidationMaster.class.getName(), id, buyerConsolidationMasterRepository);

            ValidateUtil.notNull(buyerConsolidationMaster, ErrorCode.BUYER_CONSOLIDATION_ID_NOT_FOUND);

            log.info("Successfully getting BUYER_CONSOLIDATION by id.....[" + buyerConsolidationMaster.getId() + "]");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(buyerConsolidationMaster);

        } catch (RestException ex) {

            log.error("Exception in get method ", ex);

            baseDto.setResponseCode(ex.getMessage());

        } catch (Exception e) {

            log.error("Exception in get method ", e);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);
    }


    public BaseDto create(BuyerConsolidationMaster buyerConsolidationMaster) {

        log.info("Create method is called.....");

        BaseDto baseDto = new BaseDto();

        try {

            buyerConsolidationMasterValidator.validate(buyerConsolidationMaster);

            // Setting track time & user...

            // Saving in the cache...
            BuyerConsolidationMaster buyerConsolidationMasterCreated = cacheRepository.save(SaaSUtil.getSaaSId(), BuyerConsolidationMaster.class.getName(), buyerConsolidationMaster, buyerConsolidationMasterRepository);

            log.info("BuyerConsolidationMaster Saved successfully.....[" + buyerConsolidationMasterCreated.getId() + "]");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(buyerConsolidationMasterCreated);

        } catch (RestException exception) {

            log.error("BadRequestException occured with error code ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (Exception exception) {

            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Exception Cause 1 ::: " + exceptionCause1);

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            if (exceptionCause1.contains(UniqueKey.UK_BUYERS_CONSOLD_BCCODE)) {
                baseDto.setResponseCode(ErrorCode.BUYER_CONSOLIDATION_CODE_ALREADY_EXIST);
            }

            if (exceptionCause1.contains(UniqueKey.UK_BUYERS_CONSOLD_BCNAME)) {
                baseDto.setResponseCode(ErrorCode.BUYER_CONSOLIDATION_NAME_ALREADY_EXIST);
            }
        }
        return appUtil.setDesc(baseDto);
    }


    public BaseDto update(BuyerConsolidationMaster existingBuyerConsolidationMaster) {

        log.info("update method is called....ID [ " + existingBuyerConsolidationMaster.getId() + "]");

        BaseDto baseDto = new BaseDto();

        try {

            if (existingBuyerConsolidationMaster.getId() != null) {
                buyerConsolidationMasterRepository.getOne(existingBuyerConsolidationMaster.getId());
            }
            buyerConsolidationMasterValidator.validate(existingBuyerConsolidationMaster);

            buyerConsolidationMasterRepository.save(existingBuyerConsolidationMaster);

            cacheRepository.remove(SaaSUtil.getSaaSId(), BuyerConsolidationMaster.class.getName(), existingBuyerConsolidationMaster.getId());

            log.info("BuyerConsolidationMaster Updated successfully.....[" + existingBuyerConsolidationMaster.getId() + "]");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(existingBuyerConsolidationMaster);

        } catch (RestException exception) {

            log.error("Exception occured in update method ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (ObjectOptimisticLockingFailureException e) {

            log.error("Optimistic Lock Exception occured ", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_LOCKED_RECORD);

        } catch (JpaObjectRetrievalFailureException e) {

            log.error("Object Already Deleted................", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_DELETED_RECORD);

        } catch (Exception exception) {

            log.error("Exception occurred " + exception);

            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Exception Cause 1 ::: " + exceptionCause1);

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            if (exceptionCause1.contains(UniqueKey.UK_BUYERS_CONSOLD_BCCODE)) {
                baseDto.setResponseCode(ErrorCode.BUYER_CONSOLIDATION_CODE_ALREADY_EXIST);
            }

            if (exceptionCause1.contains(UniqueKey.UK_BUYERS_CONSOLD_BCNAME)) {
                baseDto.setResponseCode(ErrorCode.BUYER_CONSOLIDATION_NAME_ALREADY_EXIST);
            }
        }

        return appUtil.setDesc(baseDto);
    }


    public BaseDto delete(Long id) {

        log.info("Delete method is Invoked...[" + id + "]");

        BaseDto baseDto = new BaseDto();

        try {

            cacheRepository.delete(SaaSUtil.getSaaSId(), BuyerConsolidationMaster.class.getName(), id, buyerConsolidationMasterRepository);

            log.info("BuyerConsolidationMaster Deleted Successfully....[" + id + "]");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

        } catch (DataIntegrityViolationException exception) {

            log.error("Exception occured : ", exception);

            ExceptionHelper helper = new ExceptionHelper(exception);

            log.error("Foreign key reference : " + helper.getFkReference());

            baseDto.setResponseCode(ErrorCode.ERROR_FK_CONSTRAINT);

        } catch (Exception exception) {

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            log.error("Exception in Delete method : ", exception);

            String rootCause = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Root Cause of Exception : " + rootCause);

            if (rootCause.contains("FK_")) {
                baseDto.setResponseCode(ErrorCode.CANNOT_DELETE_FK_RECORD);
            }
        }

        return appUtil.setDesc(baseDto);
    }


    public BaseDto getAll() {

        log.info("getAll method is called....");

        BaseDto baseDto = new BaseDto();

        try {

            List<BuyerConsolidationMaster> buyerConsolidationMasterList = buyerConsolidationMasterRepository.findAll();

            ValidateUtil.notEmpty(buyerConsolidationMasterList, ErrorCode.BUYER_CONSOLIDATION_LIST_EMPTY);

            log.info("Successfully getting list of tos...");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(buyerConsolidationMasterList);

        } catch (RestException re) {

            log.error("Exception in getAll method ", re);

            baseDto.setResponseCode(re.getMessage());

        } catch (Exception e) {

            log.error("Exception in getAll method ", e);

            baseDto.setResponseCode(ErrorCode.FAILED);

        }
        return appUtil.setDesc(baseDto);
    }

    public BaseDto searchByCriteria(BuyerConsolidationSearchDto searchData) {

        log.info("DocumentIssueRestrictionService -> searchByCriteria method is called....");

        Map<String, Object> params = new HashMap<>();
        BaseDto baseDto = new BaseDto();

        try {
            String hql = "";

            boolean isWhereAdded = false;
            if (searchData.getBcName() != null && searchData.getBcName().trim().length() != 0) {
                hql = hql + " where ";
                isWhereAdded = true;
                hql = hql + "UPPER(bcm.bcName) like '" + searchData.getBcName().trim().toUpperCase() + "%' ";
            }
            if (searchData.getBcCode() != null && searchData.getBcCode().trim().length() != 0) {
                if (isWhereAdded) {
                    hql = hql + " and ";
                    isWhereAdded = true;
                } else {
                    hql = hql + " where ";
                }
                hql = hql + " UPPER(bcm.bcCode) like '" + searchData.getBcCode().trim().toUpperCase() + "%' ";
            }
            if (searchData.getStatus() != null) {
                if (isWhereAdded) {
                    hql = hql + " and ";
                    isWhereAdded = true;
                } else {
                    hql = hql + " where ";
                }
                hql = hql + "  bcm.status = '" + searchData.getStatus() + "' ";
            }

            if (searchData.getSortByColumn() != null && searchData.getOrderByType() != null && searchData.getSortByColumn().trim().length() != 0
                    && searchData.getOrderByType().trim().length() != 0) {
                hql = hql + " order by " + searchData.getSortByColumn().trim() + " " + searchData.getOrderByType().trim();
            } else {
                hql = hql + " order by bcm.id DESC";
            }
			/* DateRange poDate; String sortByColumn; String orderByType; */
            log.info("Search HQL : [" + hql + "]");
            Query query = entityManager.createQuery(StaticSearchQuery.BuyerConsolidationSearch + hql);
            for (Entry<String, Object> e : params.entrySet()) {
                query.setParameter(e.getKey(), e.getValue());
            }
            query.setFirstResult(searchData.getSelectedPageNumber() * searchData.getRecordPerPage());
            query.setMaxResults(searchData.getRecordPerPage());


            String countQ = "Select count (bcm.id) from BuyerConsolidationMaster bcm " + hql;
            Query countQuery = entityManager.createQuery(countQ);
            for (Entry<String, Object> e : params.entrySet()) {
                countQuery.setParameter(e.getKey(), e.getValue());
            }
            Long countTotal = (Long) countQuery.getSingleResult();

            SearchRespDto searchRespDto = new SearchRespDto(countTotal, query.getResultList());
            searchRespDto.setRecordPerPage(searchData.getRecordPerPage());
            searchRespDto.setSelectedPageNumber(searchData.getSelectedPageNumber());
            baseDto.setResponseCode(ErrorCode.SUCCESS);
            baseDto.setResponseObject(searchRespDto);

        } catch (RestException re) {
            log.error("Exception in BuyerConsolidationMasterService -> searchByCriteria method ", re);
            baseDto.setResponseCode(re.getMessage());
        } catch (Exception e) {
            log.error("Exception in BuyerConsolidationMasterService -> searchByCriteria method ", e);
            baseDto.setResponseCode(ErrorCode.FAILED);
        }
        return appUtil.setDesc(baseDto);
    }


    public BaseDto searchByKeword(BuyerConsolidationSearchDto searchData) {

        log.info("Search method is called....");

        BaseDto baseDto = new BaseDto();

        try {

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            String hql = " where status!='Hide' ";

            if (searchData.getKeyword() != null && searchData.getKeyword().trim().length() != 0) {

                hql = hql + " AND ( ";

                hql = hql + " UPPER(bcm.bcCode) like '" + searchData.getKeyword().trim().toUpperCase() + "%' ";

                hql = hql + " OR ";

                hql = hql + " UPPER(bcm.bcName) like '" + searchData.getKeyword().trim().toUpperCase() + "%' ";

                hql = hql + " ) ";
            }

            hql = hql + " order by bcm.bcName ASC";

            Query query = entityManager.createQuery(StaticSearchQuery.BuyerConsolidationSearch + hql);
            query.setFirstResult(searchData.getSelectedPageNumber() * searchData.getRecordPerPage());
            query.setMaxResults(searchData.getRecordPerPage());

            SearchRespDto searchRespDto = new SearchRespDto(query.getResultList());
            baseDto.setResponseCode(ErrorCode.SUCCESS);
            baseDto.setResponseObject(searchRespDto);
            log.info("Successfully Searching Party...");

        } catch (Exception exception) {

            log.error("Exception in Search method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);

    }

}
