package com.efreightsuite.service;

import java.util.List;

import com.efreightsuite.common.component.CacheRepository;
import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.SearchRequest;
import com.efreightsuite.dto.TosSearchDto;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.ExceptionHelper;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.exception.UniqueKey;
import com.efreightsuite.model.TosMaster;
import com.efreightsuite.repository.TosMasterRepository;
import com.efreightsuite.search.TosSearchImpl;
import com.efreightsuite.util.AppUtil;
import com.efreightsuite.util.SaaSUtil;
import com.efreightsuite.util.ValidateUtil;
import com.efreightsuite.validation.TosMasterValidator;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.exception.ExceptionUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.orm.ObjectOptimisticLockingFailureException;
import org.springframework.orm.jpa.JpaObjectRetrievalFailureException;
import org.springframework.stereotype.Service;

/**
 * @author tanveer
 */

@Log4j2
@Service
public class TosMasterService {

    @Autowired
    private
    AppUtil appUtil;

    @Autowired
    private
    TosMasterRepository tosMasterRepository;

    @Autowired
    private
    CacheRepository cacheRepository;

    @Autowired
    private
    TosMasterValidator tosMasterValidator;

    @Autowired
    private
    TosSearchImpl tosSearchImpl;


    public BaseDto get(Long id) {

        log.info("get method is called.....[" + id + "]");

        BaseDto baseDto = new BaseDto();

        try {

            ValidateUtil.notNull(id, ErrorCode.TOS_ID_NOT_NULL);

            TosMaster tosMaster = cacheRepository.get(SaaSUtil.getSaaSId(), TosMaster.class.getName(), id, tosMasterRepository);

            ValidateUtil.notNull(tosMaster, ErrorCode.TOS_ID_NOT_FOUND);

            log.info("Successfully getting TOS by id.....[" + tosMaster.getId() + "]");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(tosMaster);

        } catch (RestException ex) {

            log.error("Exception in get method ", ex);

            baseDto.setResponseCode(ex.getMessage());

        } catch (Exception e) {

            log.error("Exception in get method ", e);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);
    }


    public BaseDto create(TosMaster tosMaster) {

        log.info("Create method is called.....");

        BaseDto baseDto = new BaseDto();

        try {

            tosMasterValidator.validate(tosMaster);

            TosMaster tosMasterCreated = cacheRepository.save(SaaSUtil.getSaaSId(), TosMaster.class.getName(), tosMaster, tosMasterRepository);

            log.info("TosMaster Saved successfully.....[" + tosMasterCreated.getId() + "]");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(tosMasterCreated);

        } catch (RestException exception) {

            log.error("BadRequestException occured with error code ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (Exception exception) {

            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Exception Cause 1 ::: " + exceptionCause1);

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            if (exceptionCause1.contains(UniqueKey.UK_TOSMASTER_TOSCODE)) {
                baseDto.setResponseCode(ErrorCode.TOS_CODE_ALREADY_EXIST);
            }

            if (exceptionCause1.contains(UniqueKey.UK_TOSMASTER_TOSNAME)) {
                baseDto.setResponseCode(ErrorCode.TOS_NAME_ALREADY_EXIST);
            }
        }
        return appUtil.setDesc(baseDto);
    }


    public BaseDto update(TosMaster existingTosMaster) {

        log.info("update method is called....ID [ " + existingTosMaster.getId() + "]");

        BaseDto baseDto = new BaseDto();

        try {

            tosMasterValidator.validate(existingTosMaster);

            tosMasterRepository.getOne(existingTosMaster.getId());

            existingTosMaster = cacheRepository.save(SaaSUtil.getSaaSId(), TosMaster.class.getName(), existingTosMaster, tosMasterRepository);

            log.info("TosMaster Updated successfully.....[" + existingTosMaster.getId() + "]");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(existingTosMaster);

        } catch (RestException exception) {

            log.error("Exception occured in update method ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (ObjectOptimisticLockingFailureException e) {

            log.error("Optimistic Lock Exception occured ", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_LOCKED_RECORD);

        } catch (JpaObjectRetrievalFailureException e) {

            log.error("Object Already Deleted................", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_DELETED_RECORD);

        } catch (Exception exception) {

            log.error("Exception occurred " + exception);

            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Exception Cause 1 ::: " + exceptionCause1);

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            if (exceptionCause1.contains(UniqueKey.UK_TOSMASTER_TOSCODE)) {
                baseDto.setResponseCode(ErrorCode.TOS_CODE_ALREADY_EXIST);
            }

            if (exceptionCause1.contains(UniqueKey.UK_TOSMASTER_TOSNAME)) {
                baseDto.setResponseCode(ErrorCode.TOS_NAME_ALREADY_EXIST);
            }
        }

        return appUtil.setDesc(baseDto);
    }


    public BaseDto delete(Long id) {

        log.info("Delete method is Invoked...[" + id + "]");

        BaseDto baseDto = new BaseDto();

        try {

            cacheRepository.delete(SaaSUtil.getSaaSId(), TosMaster.class.getName(), id, tosMasterRepository);

            log.info("TosMaster Deleted Successfully....[" + id + "]");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

        } catch (DataIntegrityViolationException exception) {

            log.error("Exception occured : ", exception);

            ExceptionHelper helper = new ExceptionHelper(exception);

            log.error("Foreign key reference : " + helper.getFkReference());

            baseDto.setResponseCode(ErrorCode.ERROR_FK_CONSTRAINT);

        } catch (Exception exception) {

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            log.error("Exception in Delete method : ", exception);

            String rootCause = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Root Cause of Exception : " + rootCause);

            if (rootCause.contains("FK_")) {
                baseDto.setResponseCode(ErrorCode.CANNOT_DELETE_FK_RECORD);
            }
        }

        return appUtil.setDesc(baseDto);
    }

    public BaseDto search(TosSearchDto searchDto) {

        log.info("Search method is called...." + searchDto);

        BaseDto baseDto = new BaseDto();

        try {

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(tosSearchImpl.search(searchDto));

            log.info("Successfully Searching Tos...");

        } catch (Exception exception) {

            log.error("Exception in Search method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);

    }

    public BaseDto search(SearchRequest searchRequest) {

        log.info("Search method is called...." + searchRequest);

        BaseDto baseDto = new BaseDto();

        try {

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(tosSearchImpl.search(searchRequest));

            log.info("Successfully Searching Tos...");

        } catch (Exception exception) {

            log.error("Exception in Search method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);

    }

    public BaseDto getAll() {

        log.info("getAll method is called....");

        BaseDto baseDto = new BaseDto();

        try {

            List<TosMaster> tosMasterList = tosMasterRepository.findAll();

            ValidateUtil.notEmpty(tosMasterList, ErrorCode.TOS_LIST_EMPTY);

            log.info("Successfully getting list of tos...");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(tosMasterList);

        } catch (RestException re) {

            log.error("Exception in getAll method ", re);

            baseDto.setResponseCode(re.getMessage());

        } catch (Exception e) {

            log.error("Exception in getAll method ", e);

            baseDto.setResponseCode(ErrorCode.FAILED);

        }
        return appUtil.setDesc(baseDto);
    }

}
