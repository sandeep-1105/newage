package com.efreightsuite.service;

import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.CountryReportConfigSearchDto;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.ExceptionHelper;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.exception.UniqueKey;
import com.efreightsuite.model.CountryReportConfigMaster;
import com.efreightsuite.model.PageMaster;
import com.efreightsuite.repository.CountryReportConfigRepository;
import com.efreightsuite.search.CountryReportConfigSearchImpl;
import com.efreightsuite.util.AppUtil;
import com.efreightsuite.util.ValidateUtil;
import com.efreightsuite.validation.CountryReportConfigValidator;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.exception.ExceptionUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.orm.ObjectOptimisticLockingFailureException;
import org.springframework.orm.jpa.JpaObjectRetrievalFailureException;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class CountryReportConfigService {
    @Autowired
    private
    CountryReportConfigValidator pageCountryValidator;

    @Autowired
    private
    CountryReportConfigRepository pageCountryRepository;

    @Autowired
    private
    CountryReportConfigSearchImpl pageCountrySearchImpl;

    @Autowired
    private
    AppUtil appUtil;


    public BaseDto search(CountryReportConfigSearchDto searchDto) {

        log.info("Search method is called...." + searchDto);

        BaseDto baseDto = new BaseDto();

        try {

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(pageCountrySearchImpl.search(searchDto));

            log.info("Successfully Searching Pack...");

        } catch (Exception exception) {

            log.error("Exception in Search method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);

    }

    public BaseDto create(CountryReportConfigMaster pageCountryMaster) {

        log.info("Create Method is called......" + pageCountryMaster);

        BaseDto baseDto = new BaseDto();
        try {

            if (pageCountryMaster.getPageMasterList() != null
                    && pageCountryMaster.getPageMasterList().size() != 0) {
                pageCountryMaster.setCopies((long) pageCountryMaster.getPageMasterList().size());
                for (PageMaster pageMaster : pageCountryMaster.getPageMasterList()) {
                    pageMaster.setPageCountryMaster(pageCountryMaster);
                }
            } else {
                pageCountryMaster.setCopies(0L);
            }
            pageCountryValidator.validate(pageCountryMaster);
            log.info("saving pageCountryMaster   ->" + pageCountryMaster);

            pageCountryRepository.save(pageCountryMaster);

            baseDto.setResponseCode(ErrorCode.SUCCESS);
            baseDto.setResponseObject(pageCountryMaster);
        } catch (RestException exception) {
            log.error("Exception Cause 1 ::: ", exception);
            baseDto.setResponseCode(exception.getMessage());

        } catch (Exception exception) {

            log.error("Exception occured : ", exception);
            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(exception);
            log.error("Exception Cause 1 : " + exceptionCause1);
            // General error condition
            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            if (exceptionCause1.contains(UniqueKey.UK_PAGECOUNTRY_KEY)) {
                baseDto.setResponseCode(ErrorCode.COUNTRYREPORT_ALREADY_EXIST);
            }

            if (exceptionCause1.contains(UniqueKey.UK_PAGE_CODE)) {
                baseDto.setResponseCode(ErrorCode.COUNTRYREPORT_PAGE_CODE_ALREADY_EXIST);
            }

            if (exceptionCause1.contains(UniqueKey.UK_PAGE_NAME)) {
                baseDto.setResponseCode(ErrorCode.COUNTRYREPORT_PAGE_NAME_ALREADY_EXIST);
            }
        }
        return appUtil.setDesc(baseDto);
    }

    public BaseDto getById(Long id) {
        log.info("getById method is called...." + id);
        BaseDto baseDto = new BaseDto();

        try {
            CountryReportConfigMaster pageCountryMaster = pageCountryRepository.getOne(id);

            ValidateUtil.notNull(pageCountryMaster, ErrorCode.COUNTRYREPORT_ID_NOT_FOUND);
            log.info("sucessfully fected the pageCountry master by id: " + id);

            baseDto.setResponseCode(ErrorCode.SUCCESS);
            baseDto.setResponseObject(pageCountryMaster);
        } catch (RestException exception) {
            log.info("Exception cause-1 in getById :: " + exception);
            baseDto.setResponseCode(exception.getMessage());
        } catch (Exception exception) {
            log.info("Exception cause-2 in getById :: " + exception);
            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);
    }

    public BaseDto update(CountryReportConfigMaster pageCountryMaster) {

        log.info("Update Method is called......" + pageCountryMaster);
        BaseDto baseDto = new BaseDto();
        try {
            if (pageCountryMaster.getPageMasterList() != null
                    && pageCountryMaster.getPageMasterList().size() != 0) {
                pageCountryMaster.setCopies((long) pageCountryMaster.getPageMasterList().size());
                for (PageMaster pageMaster : pageCountryMaster.getPageMasterList()) {
                    pageMaster.setPageCountryMaster(pageCountryMaster);
                }
            } else {
                pageCountryMaster.setCopies(0L);
            }
            pageCountryValidator.validate(pageCountryMaster);
            ValidateUtil.notNull(pageCountryMaster.getId(), ErrorCode.COUNTRYREPORT_ID_NOT_FOUND);
            pageCountryRepository.getOne(pageCountryMaster.getId());

            //	existingStcGroupMaster.setServiceCode(existingStcGroupMaster.getServiceMaster().getServiceCode().trim());
            //	existingStcGroupMaster.setServiceName(existingStcGroupMaster.getServiceMaster().getServiceName().trim());
            //	existingStcGroupMaster.setChargeCode(existingStcGroupMaster.getChargeMaster().getChargeCode().trim());
            //	existingStcGroupMaster.setChargeName(existingStcGroupMaster.getChargeMaster().getChargeName().trim());
            //	existingStcGroupMaster.setCategoryCode(existingStcGroupMaster.getCategoryMaster().getCategoryCode().trim());
            //	existingStcGroupMaster.setCategoryName(existingStcGroupMaster.getCategoryMaster().getCategoryName().trim());

            pageCountryRepository.save(pageCountryMaster);

            log.info("pageCountryMaster updated successfully....[" + pageCountryMaster.getId() + "]");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(pageCountryMaster);
        } catch (RestException exception) {

            log.error("Exception occured with error code ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (ObjectOptimisticLockingFailureException e) {

            log.error("Error in editing", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_LOCKED_RECORD);

        } catch (JpaObjectRetrievalFailureException e) {

            log.error("Error in editing", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_DELETED_RECORD);

        } catch (Exception exception) {

            log.error("Exception occured : ", exception);

            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Exception Cause 1 ::: " + exceptionCause1);


            // General error condition
            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            // Duplicate reason Code

            if (exceptionCause1.contains(UniqueKey.UK_PAGECOUNTRY_KEY)) {
                baseDto.setResponseCode(ErrorCode.COUNTRYREPORT_ALREADY_EXIST);
            }
            if (exceptionCause1.contains(UniqueKey.UK_PAGE_CODE)) {
                baseDto.setResponseCode(ErrorCode.COUNTRYREPORT_PAGE_CODE_ALREADY_EXIST);
            }

            if (exceptionCause1.contains(UniqueKey.UK_PAGE_NAME)) {
                baseDto.setResponseCode(ErrorCode.COUNTRYREPORT_PAGE_NAME_ALREADY_EXIST);
            }
        }

        return appUtil.setDesc(baseDto);
    }

    public BaseDto delete(Long id) {

        log.info("Delete Method is called......" + id);

        BaseDto baseDto = new BaseDto();

        try {

            pageCountryRepository.delete(id);

            log.info("PageCountryMaster Deleted Successfully....[" + id + "]");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

        } catch (RestException exception) {

            log.error("Exception in Delete method : ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (DataIntegrityViolationException exception) {

            log.error("Exception occured : ", exception);

            ExceptionHelper helper = new ExceptionHelper(exception);

            log.error("Foreign key reference : " + helper.getFkReference());

            baseDto.setResponseCode(ErrorCode.ERROR_FK_CONSTRAINT);

        } catch (Exception exception) {

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            log.error("Exception in Delete method : ", exception);

            String rootCause = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Root Cause of Exception : " + rootCause);

            if (rootCause.contains("FK_")) {
                baseDto.setResponseCode(ErrorCode.CANNOT_DELETE_FK_RECORD);
            }
        }

        return appUtil.setDesc(baseDto);
    }


}
