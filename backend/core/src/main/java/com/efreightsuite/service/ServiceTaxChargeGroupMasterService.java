package com.efreightsuite.service;

import com.efreightsuite.common.component.CacheRepository;
import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.ServiceTaxChargeGroupDto;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.ExceptionHelper;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.exception.UniqueKey;
import com.efreightsuite.model.ServiceTaxChargeGroupMaster;
import com.efreightsuite.repository.ServiceTaxChargeGroupMasterRepository;
import com.efreightsuite.search.ServiceTaxChargeGroupSearchImpl;
import com.efreightsuite.util.AppUtil;
import com.efreightsuite.util.SaaSUtil;
import com.efreightsuite.util.ValidateUtil;
import com.efreightsuite.validation.ServiceTaxChargeGroupMasterValidator;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.exception.ExceptionUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.orm.ObjectOptimisticLockingFailureException;
import org.springframework.orm.jpa.JpaObjectRetrievalFailureException;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class ServiceTaxChargeGroupMasterService {

    @Autowired
    private
    ServiceTaxChargeGroupMasterValidator serviceTaxChargeGroupMasterValidator;

    @Autowired
    private
    CacheRepository cacheRepository;

    @Autowired
    private
    ServiceTaxChargeGroupMasterRepository serviceTaxChargeGroupMasterRepository;

    @Autowired
    private
    ServiceTaxChargeGroupSearchImpl serviceTaxChargeGroupSearchImpl;

    @Autowired
    private
    AppUtil appUtil;


    public BaseDto search(ServiceTaxChargeGroupDto searchDto) {

        log.info("Search method is called...." + searchDto);

        BaseDto baseDto = new BaseDto();

        try {

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(serviceTaxChargeGroupSearchImpl.search(searchDto));

            log.info("Successfully Searching Pack...");

        } catch (Exception exception) {

            log.error("Exception in Search method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);

    }

    public BaseDto create(ServiceTaxChargeGroupMaster stcGroupMaster) {

        log.info("Create Method is called......" + stcGroupMaster);

        BaseDto baseDto = new BaseDto();

        try {
            serviceTaxChargeGroupMasterValidator.validate(stcGroupMaster);

            //setting serviceCode ChargeCode & CategoryCode

            //stcGroupMaster.setServiceCode(stcGroupMaster.getServiceMaster().getServiceCode().trim());
            //stcGroupMaster.setServiceName(stcGroupMaster.getServiceMaster().getServiceName().trim());
            //stcGroupMaster.setChargeCode(stcGroupMaster.getChargeMaster().getChargeCode().trim());
            //stcGroupMaster.setChargeName(stcGroupMaster.getChargeMaster().getChargeName().trim());
            //stcGroupMaster.setCategoryCode(stcGroupMaster.getCategoryMaster().getCategoryCode().trim());
            //stcGroupMaster.setCategoryName(stcGroupMaster.getCategoryMaster().getCategoryName().trim());

            log.info("saving servicetaxCharegGroupMaster   ->" + stcGroupMaster);

            serviceTaxChargeGroupMasterRepository.save(stcGroupMaster);


            baseDto.setResponseCode(ErrorCode.SUCCESS);
            baseDto.setResponseObject(stcGroupMaster);
        } catch (RestException exception) {
            log.error("Exception Cause 1 ::: ", exception);
            baseDto.setResponseCode(exception.getMessage());

        } catch (Exception exception) {

            log.error("Exception occured : ", exception);
            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(exception);
            log.error("Exception Cause 1 : " + exceptionCause1);
            // General error condition
            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            if (exceptionCause1.contains(UniqueKey.UK_SVC_CHRG_CAT)) {
                baseDto.setResponseCode(ErrorCode.STCGROUP_ALREADY_EXIST);
            }
        }
        return appUtil.setDesc(baseDto);
    }

    public BaseDto getById(Long id) {
        log.info("getById method is called...." + id);
        BaseDto baseDto = new BaseDto();

        try {
            ServiceTaxChargeGroupMaster stcGroupMaster = serviceTaxChargeGroupMasterRepository.findOne(id);
            if (stcGroupMaster.getChargeMaster() != null) {
                stcGroupMaster.getChargeMaster().setEdiList(null);
            }
            ValidateUtil.notNull(stcGroupMaster, ErrorCode.STCGROUP_ID_NOT_FOUND);
            log.info("sucessfully fected the servicetaxchargeGroup master by id: " + id);

            baseDto.setResponseCode(ErrorCode.SUCCESS);
            baseDto.setResponseObject(stcGroupMaster);
        } catch (RestException exception) {
            log.info("Exception cause-1 in getById :: " + exception);
            baseDto.setResponseCode(exception.getMessage());
        } catch (Exception exception) {
            log.info("Exception cause-2 in getById :: " + exception);
            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);
    }

    public BaseDto update(ServiceTaxChargeGroupMaster existingStcGroupMaster) {

        log.info("Update Method is called......" + existingStcGroupMaster);

        BaseDto baseDto = new BaseDto();

        try {
            serviceTaxChargeGroupMasterValidator.validate(existingStcGroupMaster);


            //	existingStcGroupMaster.setServiceCode(existingStcGroupMaster.getServiceMaster().getServiceCode().trim());
            //	existingStcGroupMaster.setServiceName(existingStcGroupMaster.getServiceMaster().getServiceName().trim());
            //	existingStcGroupMaster.setChargeCode(existingStcGroupMaster.getChargeMaster().getChargeCode().trim());
            //	existingStcGroupMaster.setChargeName(existingStcGroupMaster.getChargeMaster().getChargeName().trim());
            //	existingStcGroupMaster.setCategoryCode(existingStcGroupMaster.getCategoryMaster().getCategoryCode().trim());
            //	existingStcGroupMaster.setCategoryName(existingStcGroupMaster.getCategoryMaster().getCategoryName().trim());

            serviceTaxChargeGroupMasterRepository.save(existingStcGroupMaster);

            log.info("stcGroupMaster updated successfully....[" + existingStcGroupMaster.getId() + "]");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(existingStcGroupMaster);
        } catch (RestException exception) {

            log.error("Exception occured with error code ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (ObjectOptimisticLockingFailureException e) {

            log.error("Error in editing", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_LOCKED_RECORD);

        } catch (JpaObjectRetrievalFailureException e) {

            log.error("Error in editing", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_DELETED_RECORD);

        } catch (Exception exception) {

            log.error("Exception occured : ", exception);

            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Exception Cause 1 ::: " + exceptionCause1);


            // General error condition
            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            // Duplicate reason Code

            if (exceptionCause1.contains(UniqueKey.UK_SVC_CHRG_CAT)) {
                baseDto.setResponseCode(ErrorCode.STCGROUP_ALREADY_EXIST);
            }
        }

        return appUtil.setDesc(baseDto);
    }

    public BaseDto delete(Long id) {

        log.info("Delete Method is called......" + id);

        BaseDto baseDto = new BaseDto();

        try {

            cacheRepository.delete(SaaSUtil.getSaaSId(), ServiceTaxChargeGroupMaster.class.getName(), id, serviceTaxChargeGroupMasterRepository);

            log.info("ReasonMaster Deleted Successfully....[" + id + "]");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

        } catch (RestException exception) {

            log.error("Exception in Delete method : ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (DataIntegrityViolationException exception) {

            log.error("Exception occured : ", exception);

            ExceptionHelper helper = new ExceptionHelper(exception);

            log.error("Foreign key reference : " + helper.getFkReference());

            baseDto.setResponseCode(ErrorCode.ERROR_FK_CONSTRAINT);

        } catch (Exception exception) {

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            log.error("Exception in Delete method : ", exception);

            String rootCause = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Root Cause of Exception : " + rootCause);

            if (rootCause.contains("FK_")) {
                baseDto.setResponseCode(ErrorCode.CANNOT_DELETE_FK_RECORD);
            }
        }

        return appUtil.setDesc(baseDto);
    }

}

		
	
	


