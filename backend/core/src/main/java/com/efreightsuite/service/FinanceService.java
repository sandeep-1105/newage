package com.efreightsuite.service;

import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.JobLedgerReqDto;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.search.FinanceImpl;
import com.efreightsuite.util.AppUtil;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class FinanceService {

    @Autowired
    private
    AppUtil appUtil;

    @Autowired
    private
    FinanceImpl financeImpl;

    public BaseDto getJobLedger(JobLedgerReqDto requestDto) {

        BaseDto baseDto = new BaseDto();

        try {

            baseDto.setResponseObject(financeImpl.getJobLedger(requestDto));
            baseDto.setResponseCode(ErrorCode.SUCCESS);

        } catch (RestException ex) {

            log.error("Exception in get method ", ex);

            baseDto.setResponseCode(ex.getMessage());

        } catch (Exception e) {

            log.error("Exception in get method ", e);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);
    }

}
