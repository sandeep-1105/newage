package com.efreightsuite.service;

import com.efreightsuite.common.component.CacheRepository;
import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.ReferenceTypeSearchDto;
import com.efreightsuite.dto.SearchRequest;
import com.efreightsuite.enumeration.ReferenceTypeKey;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.exception.UniqueKey;
import com.efreightsuite.model.ReferenceTypeMaster;
import com.efreightsuite.repository.ReferenceTypeMasterRepository;
import com.efreightsuite.search.ReferenceTypeSearchImpl;
import com.efreightsuite.util.AppUtil;
import com.efreightsuite.util.SaaSUtil;
import com.efreightsuite.validation.ReferenceTypeMasterValidator;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.exception.ExceptionUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.ObjectOptimisticLockingFailureException;
import org.springframework.orm.jpa.JpaObjectRetrievalFailureException;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;

@Service
@Log4j2
public class ReferenceTypeMasterService {

    @Autowired
    private
    AppUtil appUtil;

    @Autowired
    private
    CacheRepository cacheRepository;

    @Autowired
    private
    ReferenceTypeMasterRepository referenceTypeMasterRepository;

    @Autowired
    private
    ReferenceTypeSearchImpl referenceTypeSearchImpl;

    @Autowired
    private
    ReferenceTypeMasterValidator referenceTypeMasterValidator;

    public BaseDto search(ReferenceTypeSearchDto searchDto) {

        log.info("Search method is called...." + searchDto);

        BaseDto baseDto = new BaseDto();

        try {

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(referenceTypeSearchImpl.search(searchDto));

            log.info("Successfully Searching ReferenceType...");

        } catch (Exception exception) {

            log.error("Exception in Search method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);

    }

    public BaseDto search(SearchRequest searchRequest) {

        log.info("Search method is called...." + searchRequest);

        BaseDto baseDto = new BaseDto();

        try {

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(referenceTypeSearchImpl.search(searchRequest));

            log.info("Successfully Searching ReferenceType...");

        } catch (Exception exception) {

            log.error("Exception in Search method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);

    }

    public BaseDto get(Long id) {

        log.info("get method is called...[" + id + "]");

        BaseDto baseDto = new BaseDto();

        try {

            ReferenceTypeMaster referenceTypeMaster = cacheRepository.get(SaaSUtil.getSaaSId(), ReferenceTypeMaster.class.getName(), id, referenceTypeMasterRepository);

            log.info("get is successfully executed....[" + referenceTypeMaster.getId() + "]");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(referenceTypeMaster);

        } catch (Exception exception) {

            log.error("Exception in get method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);
    }

    public BaseDto create(ReferenceTypeMaster referenceTypeMaster) {

        log.info("Create method is called");

        BaseDto baseDto = new BaseDto();

        try {

            referenceTypeMasterValidator.validate(referenceTypeMaster);

            cacheRepository.save(SaaSUtil.getSaaSId(), ReferenceTypeMaster.class.getName(), referenceTypeMaster, referenceTypeMasterRepository);

            log.info("ReferenceTypeMaster Saved successfully.....[ " + referenceTypeMaster.getId() + "]");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(referenceTypeMaster);

        } catch (RestException exception) {

            log.error("Exception in ReferenceTypeMaster.create method ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (Exception exception) {

            log.error("Exception in ReferenceTypeMaster.create method ", exception);

            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Exception Cause 1 : " + exceptionCause1);

            // General error condition
            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            if (exceptionCause1.contains(UniqueKey.UK_REFERENCE_REFERENCECODE)) {
                baseDto.setResponseCode(ErrorCode.REFERENCE_TYPE_CODE_ALREADY_EXIST);
            }

            if (exceptionCause1.contains(UniqueKey.UK_REFERENCE_REFERENCENAME)) {
                baseDto.setResponseCode(ErrorCode.REFERENCE_TYPE_NAME_ALREADY_EXIST);
            }
        }

        return appUtil.setDesc(baseDto);
    }

    public BaseDto update(ReferenceTypeMaster existingReferenceTypeMaster) {

        log.info("Update method is called - existingReferenceTypeMaster : [" + existingReferenceTypeMaster.getId() + "]");

        BaseDto baseDto = new BaseDto();

        try {

            referenceTypeMasterValidator.validate(existingReferenceTypeMaster);

            referenceTypeMasterRepository.getOne(existingReferenceTypeMaster.getId());

            existingReferenceTypeMaster = cacheRepository.save(SaaSUtil.getSaaSId(), ReferenceTypeMaster.class.getName(), existingReferenceTypeMaster, referenceTypeMasterRepository);

            log.info("ReferenceTypeMaster updated successfully....[" + existingReferenceTypeMaster.getId() + "]");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(existingReferenceTypeMaster);

        } catch (RestException exception) {

            log.error("Exception in ReferenceTypeMaster.update method ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (ObjectOptimisticLockingFailureException exception) {

            log.error("Exception in ReferenceTypeMaster.update method ", exception);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_LOCKED_RECORD);

        } catch (JpaObjectRetrievalFailureException exception) {

            log.error("Exception in ReferenceTypeMaster.update method ", exception);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_DELETED_RECORD);

        } catch (Exception exception) {

            log.error("Exception in ReferenceTypeMaster.update method ", exception);

            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Exception Cause 1 ::: " + exceptionCause1);

            // General error condition
            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            if (exceptionCause1.contains(UniqueKey.UK_REFERENCE_REFERENCECODE)) {
                baseDto.setResponseCode(ErrorCode.REFERENCE_TYPE_CODE_ALREADY_EXIST);
            }

            if (exceptionCause1.contains(UniqueKey.UK_REFERENCE_REFERENCENAME)) {
                baseDto.setResponseCode(ErrorCode.REFERENCE_TYPE_NAME_ALREADY_EXIST);
            }
        }

        return appUtil.setDesc(baseDto);
    }

    public BaseDto delete(Long id) {

        log.info("Delete method is Invoked...[" + id + "]");

        BaseDto baseDto = new BaseDto();

        try {

            cacheRepository.delete(SaaSUtil.getSaaSId(), ReferenceTypeMaster.class.getName(), id, referenceTypeMasterRepository);

            log.info("ReferenceTypeMaster Deleted Successfully....[" + id + "]");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

        } catch (RestException exception) {

            log.error("Exception in ReferenceTypeMaster.delete method ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (Exception exception) {

            log.error("Exception in ReferenceTypeMaster.delete method ", exception);

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            String rootCause = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Root Cause of Exception : " + rootCause);

            if (rootCause.contains("FK_")) {
                baseDto.setResponseCode(ErrorCode.CANNOT_DELETE_FK_RECORD);
            }
        }

        return appUtil.setDesc(baseDto);
    }


    public BaseDto getMandatoryReferenceTypeByKey(@PathVariable ReferenceTypeKey key) {

        log.info("getMandatoryReferenceTypeByKey : [" + key + "]");

        BaseDto baseDto = new BaseDto();

        try {


            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(referenceTypeMasterRepository.findAllByReferenceTypeKey(key));

        } catch (Exception exception) {

            log.error("Exception in get method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);
    }
}
