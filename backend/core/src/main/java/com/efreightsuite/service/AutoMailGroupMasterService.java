package com.efreightsuite.service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.efreightsuite.common.component.CacheRepository;
import com.efreightsuite.dto.AutoMailGroupMasterSearchDto;
import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.SearchRequest;
import com.efreightsuite.dto.SearchRespDto;
import com.efreightsuite.enumeration.MessageSendType;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.ExceptionHelper;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.exception.UniqueKey;
import com.efreightsuite.model.AutoMailGroupMaster;
import com.efreightsuite.model.AutoMailMaster;
import com.efreightsuite.model.TosMaster;
import com.efreightsuite.repository.AutoMailGroupMasterRepository;
import com.efreightsuite.repository.AutoMailMasterRepository;
import com.efreightsuite.search.AutoMailGroupMasterSearchService;
import com.efreightsuite.util.AppUtil;
import com.efreightsuite.util.SaaSUtil;
import com.efreightsuite.util.ValidateUtil;
import com.efreightsuite.validation.AutoMailGroupMasterValidator;
import com.efreightsuite.validation.AutoMailMasterValidator;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.exception.ExceptionUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.orm.ObjectOptimisticLockingFailureException;
import org.springframework.orm.jpa.JpaObjectRetrievalFailureException;
import org.springframework.stereotype.Service;

/**
 * @author Devendrachary M
 */
@Service
@Log4j2
public class AutoMailGroupMasterService {

    @Autowired
    private
    AutoMailGroupMasterRepository autoMailGroupMasterRepository;

    @Autowired
    AutoMailMasterRepository autoMailMasterRepository;


    @Autowired
    private
    AppUtil appUtil;

    @Autowired
    private
    CacheRepository cacheRepository;

    @Autowired
    private
    AutoMailMasterValidator autoMailMasterValidator;

    @Autowired
    private
    AutoMailGroupMasterValidator autoMailGroupMasterValidator;

    @Autowired
    private
    AutoMailGroupMasterSearchService autoMailGroupMasterSearchService;

    @Autowired
    SequenceGeneratorService sequenceGeneratorService;

    /**
     * Method - get
     *
     * @return baseDto
     */

    public AutoMailGroupMaster putCache(AutoMailGroupMaster obj, Long id) {
        AutoMailGroupMaster autoMailGroupMaster = obj != null ? obj : autoMailGroupMasterRepository.findById(id);

        if (autoMailGroupMaster.getAutoMailMasterList() != null && autoMailGroupMaster.getAutoMailMasterList().size() != 0) {
            for (AutoMailMaster po : autoMailGroupMaster.getAutoMailMasterList()) {
                po.setAutoMailGroupMaster(null);
                log.info("LOck :: " + po.getVersionLock());
            }
        } else {
            autoMailGroupMaster.setAutoMailMasterList(null);
        }

        return cacheRepository.saveEachItem(SaaSUtil.getSaaSId(), AutoMailGroupMaster.class.getName(), autoMailGroupMaster);
    }

    public BaseDto get(Long id) {
        log.info("AutoMailGroupMasterService.get method started.");

        BaseDto baseDto = new BaseDto();
        try {

            ValidateUtil.notNull(id, ErrorCode.AUTOMAILGROUP_ID_INVALID);

            AutoMailGroupMaster autoMailGroupMaster = cacheRepository.get(SaaSUtil.getSaaSId(), AutoMailGroupMaster.class.getName(), id);
            if (autoMailGroupMaster == null) {
                autoMailGroupMaster = putCache(null, id);
            }

            log.info("Successfully getting AutoMailGroupMaster by id...");

            baseDto.setResponseCode(ErrorCode.SUCCESS);
            baseDto.setResponseObject(autoMailGroupMaster);

        } catch (RestException re) {
            log.error("RestException in AutoMailGroupMasterService.get method while getting the AutoMailGroupMaster : "
                    + re);
            baseDto.setResponseCode(ErrorCode.AUTOMAILGROUP_ID_INVALID);
        } catch (Exception e) {
            log.error("Exception in AutoMailGroupMasterService.get method while getting the AutoMailGroupMaster : ", e);
            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        log.info("AutoMailGroupMasterService.get method ended.");
        return appUtil.setDesc(baseDto);
    }

	/*
     * create - This method creates a new AutoMailGroupMaster
	 * 
	 * @Param AutoMailGroupMaster master entity
	 * 
	 * @Return BaseDto
	 */

    public BaseDto createUpdate(AutoMailGroupMaster autoMailGroupMaster) {
        log.info("AutoMailGroupMasterService.create method is started.");
        ValidateUtil.notNull(autoMailGroupMaster, ErrorCode.AUTOMAILGROUP_NOT_NULL);
        autoMailGroupMasterValidator.validateToCreate(autoMailGroupMaster);
        BaseDto baseDto = new BaseDto();
        try {
            log.info("parent version lock" + autoMailGroupMaster.getVersionLock());
            if (autoMailGroupMaster.getAutoMailMasterList() != null && autoMailGroupMaster.getAutoMailMasterList().size() != 0) {
            } else {
                autoMailGroupMaster.setAutoMailMasterList(new ArrayList<>());
                autoMailGroupMaster.getAutoMailMasterList().clear();
            }
            autoMailGroupMaster = autoMailGroupMasterRepository.save(autoMailGroupMaster);
            log.info("autoMailGroupMaster.getId() --- " + autoMailGroupMaster.getId());
            for (AutoMailMaster autoMail : autoMailGroupMaster.getAutoMailMasterList()) {
                log.info("LOck :: " + autoMail.getVersionLock());
            }
//			putCache(null, AUTOMAILGROUPMASTER.GETID());

            cacheRepository.remove(SaaSUtil.getSaaSId(), AutoMailGroupMaster.class.getName(), autoMailGroupMaster.getId());
            log.info("AutoMailGroupMaster Saved successfully.....");
            baseDto.setResponseCode(ErrorCode.SUCCESS);
            baseDto.setResponseObject(ErrorCode.SUCCESS);
        } catch (RestException re) {
            log.error("Exception in AutoMailGroupMasterService.create ", re);
            baseDto.setResponseCode(re.getMessage());
        } catch (Exception exception) {

            log.error("Failed to save the Auto Mail Group Master..", exception);

            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Exception Cause 1 ::: " + exceptionCause1);

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            // Duplicate Auto Mail Group code
            if (exceptionCause1.contains(UniqueKey.UK_AUTOMAIL_GROUP_MSGGRPCODE)) {
                baseDto.setResponseCode(ErrorCode.AUTOMAILGROUP_MESSAGEGROUPCODE_ALREADY_EXIST);
            }

            // Duplicate Auto Mail Group name
            if (exceptionCause1.contains(UniqueKey.UK_AUTOMAIL_GROUP_MSGGRPNAME)) {
                baseDto.setResponseCode(ErrorCode.AUTOMAILGROUP_MESSAGEGROUPNAME_ALREADY_EXIST);
            }

            if (exceptionCause1.contains(UniqueKey.UK_AUTOMAIL_MESSAGECODE)) {
                baseDto.setResponseCode(ErrorCode.AUTOMAIL_MESSAGECODE_ALREADY_EXIST);
            }

            if (exceptionCause1.contains(UniqueKey.UK_AUTOMAIL_MESSAGENAME)) {
                baseDto.setResponseCode(ErrorCode.AUTOMAIL_MESSAGENAME_ALREADY_EXIST);
            }

        }

        log.info("AutoMailGroupMasterService.create method is ended.");
        return appUtil.setDesc(baseDto);
    }


    /*
     * search - This method Retrieves the Enquiry Log
     *
     * @Param AutoMailGroupMasterSearchDto
     *
     * @Return BaseDto
     */
    public BaseDto search(AutoMailGroupMasterSearchDto searchDto) {
        log.info("AutoMailGroupMasterService.search method is started.[" + searchDto + "]");

        BaseDto baseDto = new BaseDto();

        try {

            baseDto.setResponseObject(autoMailGroupMasterSearchService.search(searchDto));

            log.info("Successfully Searching AutoMailGroupMaster...");

        } catch (Exception exception) {

            log.info("Exception in Search method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }
        log.info("AutoMailGroupMasterService.search method is ended.");
        return appUtil.setDesc(baseDto);
    }

    public BaseDto search(SearchRequest searchRequest) {
        log.info("AutoMailGroupMasterService.search method is started.[" + searchRequest + "]");

        BaseDto baseDto = new BaseDto();

        try {

            baseDto.setResponseObject(autoMailGroupMasterSearchService.search(searchRequest));
            baseDto.setResponseCode(ErrorCode.SUCCESS);

            log.info("Successfully Searching AutoMailGroupMaster...");

        } catch (Exception exception) {

            log.info("Exception in Search method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }
        log.info("AutoMailGroupMasterService.search method is ended.");
        return appUtil.setDesc(baseDto);
    }

    public BaseDto searchByExternalAutoMail(SearchRequest searchRequest) {
        log.info("AutoMailGroupMasterService.searchByExternalAutoMail method is started.[" + searchRequest + "]");

        BaseDto baseDto = new BaseDto();

        try {


            SearchRespDto searchRespDto = autoMailGroupMasterSearchService.search(searchRequest);


            List<AutoMailGroupMaster> automailGroups = (List<AutoMailGroupMaster>) searchRespDto.getSearchResult();

            if (automailGroups != null) {
                for (AutoMailGroupMaster autoMailGroupMaster : automailGroups) {
                    if (autoMailGroupMaster.getAutoMailMasterList() != null) {

                        autoMailGroupMaster.getAutoMailMasterList().removeIf(autoMailMaster -> autoMailMaster.getMessageSendType() != null && autoMailMaster.getMessageSendType().equals(MessageSendType.Internal));

                    }
                }
            }
			
			/*if(automailGroups !=null)
			{
				for(AutoMailGroupMaster autoMailGroupMaster:automailGroups)
				{
					if(autoMailGroupMaster.getAutoMailMasterList()!=null)
					{
						for(AutoMailMaster autoMailMaster:autoMailGroupMaster.getAutoMailMasterList())
						{
							if(autoMailMaster.getMessageSendType().equals(MessageSendType.Internal))
							{
								autoMailGroupMaster.getAutoMailMasterList().remove(autoMailMaster);
							}
						}
					}
				}
			}*/

            searchRespDto.setSearchResult(automailGroups);
            baseDto.setResponseObject(searchRespDto);
            baseDto.setResponseCode(ErrorCode.SUCCESS);

            log.info("Successfully Searching AutoMailGroupMaster...");

        } catch (Exception exception) {

            log.info("Exception in Search method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }
        log.info("AutoMailGroupMasterService.searchByExternalAutoMail method is ended.");
        return appUtil.setDesc(baseDto);
    }

    public BaseDto update(AutoMailGroupMaster existingAutoMailGroupMaster) {
        log.info("AutoMailGroupMasterService.update method is started.");
        BaseDto baseDto = new BaseDto();
        log.info("existingAutoMailGroupMaster.getAutoMailMasterList() : " + existingAutoMailGroupMaster.getAutoMailMasterList().size());

        AutoMailGroupMaster aMgObjForNewAutoMail = null;
        for (AutoMailMaster autoMailMaster : existingAutoMailGroupMaster.getAutoMailMasterList()) {
            //Getting AutoMailGroupMaster and setting to EnquiryDetail
            autoMailMasterValidator.validateToUpdate(autoMailMaster);
            AutoMailGroupMaster autoMailGroupMaster = autoMailMaster.getAutoMailGroupMaster();
            if (autoMailGroupMaster != null) {
                AutoMailGroupMaster autoMailGroupMasterFromDb = cacheRepository.get(SaaSUtil.getSaaSId(), TosMaster.class.getName(),
                        autoMailGroupMaster.getId(), autoMailGroupMasterRepository);
                aMgObjForNewAutoMail = autoMailGroupMasterFromDb;
                autoMailMaster.setAutoMailGroupMaster(autoMailGroupMasterFromDb);
            }

            if (autoMailGroupMaster == null) {

                autoMailMaster.setAutoMailGroupMaster(aMgObjForNewAutoMail);

            }


        }

        try {

            autoMailGroupMasterValidator.validateToUpdate(existingAutoMailGroupMaster);

            autoMailGroupMasterRepository.getOne(existingAutoMailGroupMaster.getId());


            if (existingAutoMailGroupMaster.getAutoMailMasterList() != null && existingAutoMailGroupMaster.getAutoMailMasterList().size() != 0) {
                for (AutoMailMaster autoMailMaster : existingAutoMailGroupMaster.getAutoMailMasterList()) {

                    autoMailMaster.setAutoMailGroupMaster(existingAutoMailGroupMaster);

                }
            } else {
                existingAutoMailGroupMaster.setAutoMailMasterList(new ArrayList<>());
                existingAutoMailGroupMaster.getAutoMailMasterList().clear();
            }


            existingAutoMailGroupMaster = autoMailGroupMasterRepository.save(existingAutoMailGroupMaster);
            putCache(existingAutoMailGroupMaster, null);

            log.info("PartyService updated successfully...");

            baseDto.setResponseCode(ErrorCode.SUCCESS);
            baseDto.setResponseObject(ErrorCode.SUCCESS);

        } catch (RestException exception) {

            log.error("Exception in Delete method : ", exception);

            baseDto.setResponseCode(exception.getMessage());
        } catch (ObjectOptimisticLockingFailureException e) {
            log.error("Error in editing Service", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_LOCKED_RECORD);
        } catch (JpaObjectRetrievalFailureException e) {
            log.error("Error in editing Service", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_DELETED_RECORD);
        } catch (Exception exception) {

            log.error("Failed to update the Auto Mail Group Master..", exception);

            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Exception Cause 1 ::: " + exceptionCause1);

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            // Duplicate Auto Mail Group code
            if (exceptionCause1.contains(UniqueKey.UK_AUTOMAIL_GROUP_MSGGRPCODE)) {
                baseDto.setResponseCode(ErrorCode.AUTOMAILGROUP_MESSAGEGROUPCODE_ALREADY_EXIST);
            }

            // Duplicate Auto Mail Group name
            if (exceptionCause1.contains(UniqueKey.UK_AUTOMAIL_GROUP_MSGGRPNAME)) {
                baseDto.setResponseCode(ErrorCode.AUTOMAILGROUP_MESSAGEGROUPNAME_ALREADY_EXIST);
            }

            for (AutoMailMaster autoMailMaster : existingAutoMailGroupMaster.getAutoMailMasterList()) {
                // Duplicate Auto Mail code
                if (exceptionCause1.contains(UniqueKey.UK_AUTOMAIL_MESSAGECODE)) {
                    baseDto.setResponseCode(ErrorCode.AUTOMAIL_MESSAGECODE_ALREADY_EXIST);
                }

                if (exceptionCause1.contains(UniqueKey.UK_AUTOMAIL_MESSAGENAME)) {
                    baseDto.setResponseCode(ErrorCode.AUTOMAIL_MESSAGENAME_ALREADY_EXIST);
                }


            }
        }

        log.info("AutoMailGroupMasterService.update method is ended.");
        return appUtil.setDesc(baseDto);

    }

    /*
     * Deletes the Existing Party Based on the Given Id
     *
     * @param Id
     *
     * @Returns BaseDto
     */
    public BaseDto delete(Long id) {
        log.info("AutoMailGroupMasterService.delete method is started.[" + id + "]");

        BaseDto baseDto = new BaseDto();

        try {

            ValidateUtil.notNull(id, ErrorCode.AUTOMAILGROUP_ID_NOT_NULL);

            cacheRepository.delete(SaaSUtil.getSaaSId(), AutoMailGroupMaster.class.getName(), id,
                    autoMailGroupMasterRepository);

            log.info("AutoMailGroupMaster Deleted Successfully....[" + id + "]");

            baseDto.setResponseCode(ErrorCode.SUCCESS);
            return appUtil.setDesc(baseDto);

        } catch (RestException exception) {

            log.error("Exception in Delete method : ", exception);

            baseDto.setResponseCode(exception.getMessage());
        } catch (DataIntegrityViolationException exception) {

            log.error("Exception occured : ", exception);

            ExceptionHelper helper = new ExceptionHelper(exception);

            log.error("Foreign key reference : " + helper.getFkReference());

            baseDto.setResponseCode(ErrorCode.ERROR_FK_CONSTRAINT);
        } catch (Exception exception) {

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            log.error("Exception in Delete method : ", exception);

            String rootCause = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Root Cause of Exception : " + rootCause);

            if (rootCause.contains("FK_")) {
                baseDto.setResponseCode(ErrorCode.CANNOT_DELETE_FK_RECORD);
            }
        }
        log.info("AutoMailGroupMasterService.delete method is ended");
        return appUtil.setDesc(baseDto);

    }

}
