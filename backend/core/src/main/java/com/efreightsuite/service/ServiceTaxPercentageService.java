package com.efreightsuite.service;

import java.util.List;

import com.efreightsuite.common.component.CacheRepository;
import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.ServiceTaxPercentageSearchDto;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.ExceptionHelper;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.model.ServiceTaxPercentage;
import com.efreightsuite.repository.ServiceTaxPercentageRepository;
import com.efreightsuite.search.ServiceTaxPercentageSearchImpl;
import com.efreightsuite.util.AppUtil;
import com.efreightsuite.util.SaaSUtil;
import com.efreightsuite.util.ValidateUtil;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.exception.ExceptionUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.orm.ObjectOptimisticLockingFailureException;
import org.springframework.orm.jpa.JpaObjectRetrievalFailureException;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class ServiceTaxPercentageService {

    @Autowired
    private
    AppUtil appUtil;

    @Autowired
    private
    CacheRepository cacheRepository;

    @Autowired
    private
    ServiceTaxPercentageRepository serviceTaxPercentageRepository;

    @Autowired
    private
    ServiceTaxPercentageSearchImpl serviceTaxPercentageSearchImpl;

    public BaseDto search(ServiceTaxPercentageSearchDto searchDto) {
        log.info("Search method is called...." + searchDto);
        BaseDto baseDto = new BaseDto();
        try {
            baseDto.setResponseCode(ErrorCode.SUCCESS);
            baseDto.setResponseObject(serviceTaxPercentageSearchImpl.search(searchDto));
            log.info("Successfully Searching ServiceTax Percentage...");
        } catch (Exception exception) {
            log.error("Exception in Search method : ", exception);
            baseDto.setResponseCode(ErrorCode.FAILED);
        }
        return appUtil.setDesc(baseDto);
    }


    public BaseDto getAll() {

        log.info("getAll method is called..");

        BaseDto baseDto = new BaseDto();

        try {

            List<ServiceTaxPercentage> servicetaxPercentageList = serviceTaxPercentageRepository.findAll();
            ValidateUtil.notEmpty(servicetaxPercentageList, ErrorCode.SERVICE_TAX_PERCENTAGE_LIST_EMPTY);
            baseDto.setResponseCode(ErrorCode.SUCCESS);
            baseDto.setResponseObject(servicetaxPercentageList);

            log.info("getAll method is executed successfully..");

        } catch (RestException exception) {

            log.error("Exception in getAll method : ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (Exception exception) {

            log.error("Exception in getAll method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);
    }


    public BaseDto getById(Long id) {
        log.info("ServiceTaxPercentageService -> getById method is called.... " + id);
        BaseDto baseDto = new BaseDto();
        try {
            ServiceTaxPercentage serviceTaxPercentage = cacheRepository.get(SaaSUtil.getSaaSId(), ServiceTaxPercentage.class.getName(), id, serviceTaxPercentageRepository);
            ValidateUtil.notNull(serviceTaxPercentage, ErrorCode.SERVICE_TAX_PERCENTAGE_ID_NOT_FOUND);
            log.info("Successfully getting Service Tax Percentage by id....[" + id + "]");
            baseDto.setResponseCode(ErrorCode.SUCCESS);
            baseDto.setResponseObject(serviceTaxPercentage);
        } catch (RestException re) {
            log.error("Exception in ServiceTaxPercentageService -> getById method ", re);
            baseDto.setResponseCode(re.getMessage());
        } catch (Exception e) {
            log.error("Exception in ServiceTaxPercentageService -> getById method ", e);
            baseDto.setResponseCode(ErrorCode.FAILED);
        }
        return appUtil.setDesc(baseDto);
    }


    public BaseDto create(ServiceTaxPercentage serviceTaxPercentage) {
        log.info("ServiceTaxPercentageService -> create method start..");
        BaseDto baseDto = new BaseDto();
        try {
            serviceTaxPercentage = cacheRepository.save(SaaSUtil.getSaaSId(), ServiceTaxPercentage.class.getName(), serviceTaxPercentage, serviceTaxPercentageRepository);
            log.info(" ServiceTaxPercentage Saved Successfully....[" + serviceTaxPercentage.getId() + "]");
            baseDto.setResponseCode(ErrorCode.SUCCESS);
            baseDto.setResponseObject(serviceTaxPercentage);
        } catch (RestException re) {
            log.error("Exception in ServiceTaxPercentageService -> create method ", re);
            baseDto.setResponseCode(re.getMessage());
        } catch (Exception e) {
            log.error("Exception in ServiceTaxPercentageService -> create method ", e);
            String exceptionCause = ExceptionUtils.getRootCauseMessage(e);
            log.error("Exception ", e);
            log.info("Exception Cause ::: " + exceptionCause);
            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }
        return appUtil.setDesc(baseDto);

    }

    public BaseDto update(ServiceTaxPercentage existingServiceTaxPercentage) {
        log.info("ServiceTaxPercentageService -> update method start..");
        BaseDto baseDto = new BaseDto();
        try {
            ValidateUtil.notNull(existingServiceTaxPercentage.getId(), ErrorCode.SERVICE_TAX_PERCENTAGE_ID_NOT_NULL);
            serviceTaxPercentageRepository.getOne(existingServiceTaxPercentage.getId());
            existingServiceTaxPercentage = cacheRepository.save(SaaSUtil.getSaaSId(), ServiceTaxPercentage.class.getName(), existingServiceTaxPercentage, serviceTaxPercentageRepository);
            log.info(" ServiceTaxPercentage updated Successfully....[" + existingServiceTaxPercentage.getId() + "]");
            baseDto.setResponseCode(ErrorCode.SUCCESS);
            baseDto.setResponseObject(existingServiceTaxPercentage);

        } catch (RestException re) {
            log.error("Exception in ServiceTaxPercentageService -> update method ", re);
            baseDto.setResponseCode(re.getMessage());
        } catch (ObjectOptimisticLockingFailureException e) {

            log.error("Error in editing Service", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_LOCKED_RECORD);

        } catch (JpaObjectRetrievalFailureException e) {

            log.error("Error in editing Service", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_DELETED_RECORD);

        } catch (Exception e) {
            log.error("Exception in ServiceTaxPercentageService -> update method ", e);
            String exceptionCause = ExceptionUtils.getRootCauseMessage(e);
            log.error("Exception ", e);
            log.info("Exception Cause ::: " + exceptionCause);
            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);
                /*
				 * if (exceptionCause.contains(UniqueKey.UK_PRICMASTER_ORI_DEST)) {
					dto.setResponseCode(ErrorCode.UK_PRICING_ALREADY_EXIST);
				   }
				*/
            baseDto.setResponseCode(ErrorCode.FAILED);
        }
        return appUtil.setDesc(baseDto);
    }

    public BaseDto delete(Long id) {
        log.info("Delete method is invoked : [ " + id + "]");
        BaseDto baseDto = new BaseDto();
        try {
            cacheRepository.delete(SaaSUtil.getSaaSId(), ServiceTaxPercentage.class.getName(), id, serviceTaxPercentageRepository);
            log.info("ServiceTaxPercentage Deleted Successfully.... [ " + id + "]");
            baseDto.setResponseCode(ErrorCode.SUCCESS);
        } catch (DataIntegrityViolationException exception) {
            log.error("Exception occured : ", exception);
            ExceptionHelper helper = new ExceptionHelper(exception);
            log.error("Foreign key reference : " + helper.getFkReference());
            baseDto.setResponseCode(ErrorCode.ERROR_FK_CONSTRAINT);
        } catch (Exception exception) {
            log.error("Exception in Delete method : ", exception);
            baseDto.setResponseCode(ErrorCode.FAILED);
        }
        return appUtil.setDesc(baseDto);
    }

}
