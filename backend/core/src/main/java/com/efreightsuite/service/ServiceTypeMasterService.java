package com.efreightsuite.service;

import com.efreightsuite.common.component.CacheRepository;
import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.SearchRequest;
import com.efreightsuite.dto.ServiceTypeSearchDto;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.ExceptionHelper;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.exception.UniqueKey;
import com.efreightsuite.model.ServiceTypeMaster;
import com.efreightsuite.repository.ServiceTypeMasterRepository;
import com.efreightsuite.search.ServiceTypeSearchImpl;
import com.efreightsuite.util.AppUtil;
import com.efreightsuite.util.RegExpName;
import com.efreightsuite.util.SaaSUtil;
import com.efreightsuite.util.ValidateUtil;
import com.efreightsuite.validation.ServiceTypeMasterValidator;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.exception.ExceptionUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.orm.ObjectOptimisticLockingFailureException;
import org.springframework.orm.jpa.JpaObjectRetrievalFailureException;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class ServiceTypeMasterService {

    @Autowired
    private
    AppUtil appUtil;

    @Autowired
    private
    ServiceTypeMasterRepository serviceTypeMasterRepository;

    @Autowired
    ServiceTypeMasterValidator serviceTypeMasterValidator;

    @Autowired
    private
    CacheRepository cacheRepository;


    @Autowired
    private
    ServiceTypeSearchImpl serviceTypeSearchImpl;

    @Autowired
    private
    RegularExpressionService regExpService;


    /*
     * Returns all the CostCenter object as a list based on search keyword
     *
     * @Returns BaseDto
     */
    public BaseDto search(SearchRequest searchRequest, String transportMode) {

        log.info("Search method is called....");

        BaseDto baseDto = new BaseDto();

        try {
            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(serviceTypeSearchImpl.search(searchRequest, transportMode));

            return appUtil.setDesc(baseDto);

        } catch (Exception e) {

            log.error("Exception in Search method ", e);

            baseDto.setResponseCode(ErrorCode.FAILED);

            return appUtil.setDesc(baseDto);
        }

    }


    public BaseDto search(ServiceTypeSearchDto serviceTypeSearchDto) {

        log.info("Search method is called....");

        BaseDto baseDto = new BaseDto();

        try {

            baseDto.setResponseObject(serviceTypeSearchImpl.search(serviceTypeSearchDto));

            baseDto.setResponseCode(ErrorCode.SUCCESS);

        } catch (Exception exception) {

            log.error("Exception in Search method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);
    }


    public BaseDto create(ServiceTypeMaster serviceTypeMaster) {

        log.info("Create method is called..");

        BaseDto baseDto = new BaseDto();

        try {

            //packMasterValidator.validate(packMaster);

            ValidateUtil.notNull(serviceTypeMaster.getServiceTypeCode(), ErrorCode.SERVICETYPE_CODE_NOT_NULL);
            regExpService.validate(serviceTypeMaster.getServiceTypeCode(), RegExpName.Reg_Exp_Master_SERVICE_TYPE_CODE, ErrorCode.SERVICETYPE_CODE_INVALID);
            // Validating CosTCenter code for not null

            log.info("CostCenterCode Validated...");
            ValidateUtil.notNull(serviceTypeMaster.getServiceTypeName(), ErrorCode.SERVICETYPE_NAME_NOT_NULL);
            // Validating Costcenter name pattern
            regExpService.validate(serviceTypeMaster.getServiceTypeName(), RegExpName.Reg_Exp_Master_SERVICE_TYPE_NAME, ErrorCode.SERVICETYPE_NAME_INVALID);
            // Validating Costcenter name for not null


            serviceTypeMaster.setServiceTypeCode(serviceTypeMaster.getServiceTypeCode().trim());

            serviceTypeMaster.setServiceTypeName(serviceTypeMaster.getServiceTypeName().trim());


            serviceTypeMaster = cacheRepository.save(SaaSUtil.getSaaSId(), ServiceTypeMaster.class.getName(), serviceTypeMaster, serviceTypeMasterRepository);

            log.info("Service Type Master Saved successfully.....[" + serviceTypeMaster.getId() + "]");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(serviceTypeMaster);

        } catch (RestException exception) {

            log.error("Exception Cause 1 ::: ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (Exception exception) {

            log.error("Exception occured : ", exception);

            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Exception Cause 1 : " + exceptionCause1);

            // General error condition
            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);


            if (exceptionCause1.contains(UniqueKey.UK_SERVICETYPE_SERVICECODE)) {
                baseDto.setResponseCode(ErrorCode.SERVICE_TYPE_CODE_ALREADY_EXIST);
            }

            if (exceptionCause1.contains(UniqueKey.UK_SERVICETYPE_SERVICENAME)) {
                baseDto.setResponseCode(ErrorCode.SERVICE_TYPE_NAME_ALREADY_EXIST);
            }

        }

        return appUtil.setDesc(baseDto);
    }

    public BaseDto update(ServiceTypeMaster serviceTypeMaster) {

        log.info("Update method is called - ServiceTypeMaster : [" + serviceTypeMaster.getId() + "]");

        BaseDto baseDto = new BaseDto();

        try {

            ValidateUtil.notNull(serviceTypeMaster.getId(), ErrorCode.SERVICE_TYPE_ID_NOT_NULL);
            ValidateUtil.notNull(serviceTypeMaster.getServiceTypeCode(), ErrorCode.SERVICETYPE_CODE_NOT_NULL);
            regExpService.validate(serviceTypeMaster.getServiceTypeCode(), RegExpName.Reg_Exp_Master_SERVICE_TYPE_CODE, ErrorCode.SERVICETYPE_CODE_INVALID);
            // Validating CosTCenter code for not null

            log.info("CostCenterCode Validated...");
            ValidateUtil.notNull(serviceTypeMaster.getServiceTypeName(), ErrorCode.SERVICETYPE_NAME_NOT_NULL);
            // Validating Costcenter name pattern
            regExpService.validate(serviceTypeMaster.getServiceTypeName(), RegExpName.Reg_Exp_Master_SERVICE_TYPE_NAME, ErrorCode.SERVICETYPE_NAME_INVALID);
            // Validating Costcenter name for not null
            //packMasterValidator.validate(serviceTypeMaster);


            serviceTypeMasterRepository.getOne(serviceTypeMaster.getId());

            serviceTypeMaster = cacheRepository.save(SaaSUtil.getSaaSId(), ServiceTypeMaster.class.getName(), serviceTypeMaster, serviceTypeMasterRepository);

            log.info("ServiceTypeMaster updated successfully....[" + serviceTypeMaster.getId() + "]");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(serviceTypeMaster);

        } catch (RestException exception) {

            log.error("Exception occured with error code ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (ObjectOptimisticLockingFailureException e) {

            log.error("Error in editing serviceTypeMaster", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_LOCKED_RECORD);

        } catch (JpaObjectRetrievalFailureException e) {

            log.error("Error in editing serviceTypeMaster", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_DELETED_RECORD);

        } catch (Exception exception) {

            log.error("Exception occured : ", exception);

            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Exception Cause 1 ::: " + exceptionCause1);


            // General error condition
            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);


            if (exceptionCause1.contains(UniqueKey.UK_SERVICETYPE_SERVICECODE)) {
                baseDto.setResponseCode(ErrorCode.SERVICE_TYPE_CODE_ALREADY_EXIST);
            }

            if (exceptionCause1.contains(UniqueKey.UK_SERVICETYPE_SERVICENAME)) {
                baseDto.setResponseCode(ErrorCode.SERVICE_TYPE_NAME_ALREADY_EXIST);
            }


        }
        return appUtil.setDesc(baseDto);
    }


    public BaseDto delete(Long id) {

        log.info("Delete method is Invoked...[" + id + "]");

        BaseDto baseDto = new BaseDto();

        try {

            cacheRepository.delete(SaaSUtil.getSaaSId(), ServiceTypeMaster.class.getName(), id, serviceTypeMasterRepository);

            log.info("ServiceTypeMaster Deleted Successfully....[" + id + "]");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

        } catch (RestException exception) {

            log.error("Exception in Delete method : ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (DataIntegrityViolationException exception) {

            log.error("Exception occured : ", exception);

            ExceptionHelper helper = new ExceptionHelper(exception);

            log.error("Foreign key reference : " + helper.getFkReference());

            baseDto.setResponseCode(ErrorCode.ERROR_FK_CONSTRAINT);

        } catch (Exception exception) {

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            log.error("Exception in Delete method : ", exception);

            String rootCause = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Root Cause of Exception : " + rootCause);

            if (rootCause.contains("FK_")) {
                baseDto.setResponseCode(ErrorCode.CANNOT_DELETE_FK_RECORD);
            }
        }

        return appUtil.setDesc(baseDto);
    }

    public BaseDto get(Long id) {

        log.info("get method is called...[" + id + "]");

        BaseDto baseDto = new BaseDto();

        try {

            ServiceTypeMaster serviceTypeMaster = cacheRepository.get(SaaSUtil.getSaaSId(), ServiceTypeMaster.class.getName(), id, serviceTypeMasterRepository);

            ValidateUtil.notNull(serviceTypeMaster, ErrorCode.SERVICE_TYPE_ID_NOT_FOUND);

            log.info("get is successfully executed....[" + serviceTypeMaster.getId() + "]");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(serviceTypeMaster);

        } catch (RestException exception) {

            log.error("Exception in get method : ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (Exception exception) {

            log.error("Exception in get method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);
    }

}
