package com.efreightsuite.service;

import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.KeyValueDto;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.util.AppUtil;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Service;

@Service
public class HistoryService {


    @Autowired
    private
    AppUtil appUtil;

    @Autowired
    private StringRedisTemplate template;

    public Object redisTest(KeyValueDto data) {

        ValueOperations<String, String> ops = this.template.opsForValue();
        ops.set(data.getKey(), data.getValue());
        System.out.println("Found key " + data.getKey());

        BaseDto baseDto = new BaseDto();

        baseDto.setResponseCode(ErrorCode.SUCCESS);

        baseDto.setResponseObject(null);

        return appUtil.setDesc(baseDto);
    }

    public Object get(KeyValueDto data) {

        BaseDto baseDto = new BaseDto();

        baseDto.setResponseCode(ErrorCode.SUCCESS);

        if (this.template.hasKey(data.getKey())) {
            baseDto.setResponseObject(this.template.opsForValue().get(data.getKey()));
            this.template.delete(data.getKey());
        }

        return appUtil.setDesc(baseDto);
    }


}
