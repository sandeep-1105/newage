package com.efreightsuite.service;

import com.efreightsuite.communication.RabbitMQMessageSender;
import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.ConsolDto;
import com.efreightsuite.dto.MessagingDto;
import com.efreightsuite.dto.WinConfigurationRequestDto;
import com.efreightsuite.enumeration.EdiMessagingFor;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.search.WinConfigurationImpl;
import com.efreightsuite.util.AppUtil;
import com.efreightsuite.util.SaaSUtil;
import com.efreightsuite.util.ValidateUtil;
import org.apache.commons.lang3.exception.ExceptionUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@lombok.extern.log4j.Log4j2
public class WinConfigurationService {

    @Autowired
    private
    WinConfigurationImpl winConfigurationImpl;

    @Autowired
    private
    AppUtil appUtil;

    @Autowired
    private
    RabbitMQMessageSender rabbitMQMessageSender;

    @Autowired
    AuthService authService;


    //create ..which sends hawb and mawb data to win service
    public BaseDto sendToWin(ConsolDto consolDto) {
        log.info("send to win method called...");
        BaseDto baseDto = new BaseDto();
        try {
            if (consolDto.getConsolUid() != null) {
                MessagingDto data = new MessagingDto();
                data.setConsolUid(consolDto.getConsolUid());
                data.setMessagingFor(EdiMessagingFor.WIN);
                data.setResponse(false);
                data.setUserProfileId(AuthService.getCurrentUser().getId());
                data.setSelectedLocationId(AuthService.getCurrentUser().getSelectedUserLocation().getId());
                rabbitMQMessageSender.sendtoMQ(data, SaaSUtil.getSaaSId());
                baseDto.setResponseCode(ErrorCode.SUCCESS);
                baseDto.setResponseObject(ErrorCode.SUCCESS);
            } else {
                log.error("Win web connect..sending");
            }
        } catch (Exception exception) {

            log.error("Exception in saveEdi method ", exception);

            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Exception Cause 1 ::: " + exceptionCause1);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return baseDto;

    }


    public BaseDto search(WinConfigurationRequestDto searchDto) {

        log.info("Win -> search method is called....");

        BaseDto baseDto = new BaseDto();

        try {

            ValidateUtil.notNull(searchDto, ErrorCode.AES_CANNOT_NULL);

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(winConfigurationImpl.search(searchDto));

        } catch (RestException re) {

            log.error("Exception in Aes service -> search method ", re);

            baseDto.setResponseCode(re.getMessage());

        } catch (Exception e) {

            log.error("Exception in Win service -> search method ", e);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);
    }


}
