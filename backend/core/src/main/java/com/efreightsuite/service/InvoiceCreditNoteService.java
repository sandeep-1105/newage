package com.efreightsuite.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.FromToCurrency;
import com.efreightsuite.dto.InvoiceChargeDto;
import com.efreightsuite.dto.InvoiceSearchDto;
import com.efreightsuite.dto.SearchRequest;
import com.efreightsuite.dto.ServiceTaxCategoryMasterSearchDto;
import com.efreightsuite.enumeration.CRN;
import com.efreightsuite.enumeration.ChargeCalculationType;
import com.efreightsuite.enumeration.CustomerAgent;
import com.efreightsuite.enumeration.DebitCredit;
import com.efreightsuite.enumeration.ImportExport;
import com.efreightsuite.enumeration.PPCC;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.model.Consol;
import com.efreightsuite.model.GeneralLedgerChargeMappingMaster;
import com.efreightsuite.model.InvoiceCreditNote;
import com.efreightsuite.model.InvoiceCreditNoteAttachment;
import com.efreightsuite.model.InvoiceCreditNoteDetail;
import com.efreightsuite.model.ProvisionItem;
import com.efreightsuite.model.Provisional;
import com.efreightsuite.model.ServiceChargeTaxCategoryMapping;
import com.efreightsuite.model.ServiceTaxCategoryMaster;
import com.efreightsuite.model.ShipmentCharge;
import com.efreightsuite.model.ShipmentLink;
import com.efreightsuite.model.ShipmentServiceDetail;
import com.efreightsuite.repository.ConsolRepository;
import com.efreightsuite.repository.DefaultMasterDataRepository;
import com.efreightsuite.repository.GeneralLedgerChargeMappingRepository;
import com.efreightsuite.repository.InvoiceAttachmentRepository;
import com.efreightsuite.repository.InvoiceCreditNoteRepository;
import com.efreightsuite.repository.ProvisionalRepository;
import com.efreightsuite.repository.ServiceChargeTaxCategoryMappingRepository;
import com.efreightsuite.repository.ServiceTaxCategoryMasterRepository;
import com.efreightsuite.repository.ServiceTaxPercentageRepository;
import com.efreightsuite.repository.ShipmentServiceDetailRepository;
import com.efreightsuite.search.CurrencyRateSearchImpl;
import com.efreightsuite.search.GeneralLedgerChargeMappingSearchImpl;
import com.efreightsuite.search.InvoiceCreditNoteSearchImpl;
import com.efreightsuite.search.ServiceTaxCategorySearchImpl;
import com.efreightsuite.util.AppUtil;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.exception.ExceptionUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class InvoiceCreditNoteService {

    @Autowired
    private
    AppUtil appUtil;

    @Autowired
    private
    InvoiceCreditNoteRepository invoiceCreditNoteRepository;

    @Autowired
    private
    InvoiceCreditNoteSearchImpl invoiceCreditNoteSearchImpl;

    @Autowired
    private
    ProvisionalRepository provisionalRepository;

    @Autowired
    private
    GeneralLedgerChargeMappingSearchImpl generalLedgerChargeMappingSearchImpl;

    @Autowired
    private
    ServiceTaxCategorySearchImpl serviceTaxCategorySearchImpl;

    @Autowired
    private
    ServiceTaxCategoryMasterRepository serviceTaxCategoryMasterRepository;

    @Autowired
    private
    ServiceTaxPercentageRepository serviceTaxPercentageRepository;

    @Autowired
    private
    ServiceChargeTaxCategoryMappingRepository serviceChargeTaxCategoryMappingRepository;

    @Autowired
    private
    InvoiceAttachmentRepository invoiceAttachmentRepository;

    @Autowired
    DefaultMasterDataRepository defaultMasterDataRepository;

    @Autowired
    private
    ShipmentServiceDetailRepository shipmentServiceDetailRepository;

    @Autowired
    private
    ConsolRepository consolRepository;

    @Autowired
    private
    CurrencyRateSearchImpl currencyRateSearchImpl;

    @Autowired
    private
    GeneralLedgerChargeMappingRepository glChargeMappingRepository;


    private void afterGet(InvoiceCreditNote obj) {

        AppUtil.setPartyToNull(obj.getParty());
        AppUtil.setService(obj.getShipmentServiceDetail());

        for (InvoiceCreditNoteDetail dd : obj.getInvoiceCreditNoteDetailList()) {
            AppUtil.setService(dd.getShipmentServiceDetail());
        }

        if (obj.getInvoiceCreditNoteAttachmentList() != null
                && obj.getInvoiceCreditNoteAttachmentList().size() != 0) {
            for (InvoiceCreditNoteAttachment attachment : obj.getInvoiceCreditNoteAttachmentList()) {
                attachment.setFile(null);

            }
        }

    }

    public BaseDto getById(Long id) {

        log.info("InvoiceService -> getById method is called....");

        BaseDto baseDto = new BaseDto();

        try {

            InvoiceCreditNote invoiceCreditNote = invoiceCreditNoteRepository.findById(id);

            afterGet(invoiceCreditNote);

            baseDto.setResponseCode(ErrorCode.SUCCESS);
            baseDto.setResponseObject(invoiceCreditNote);

        } catch (RestException re) {
            log.error("Exception in InvoiceService -> getById method ", re);
            baseDto.setResponseCode(re.getMessage());

        } catch (Exception e) {
            log.error("Exception in InvoiceService -> getById method ", e);
            baseDto.setResponseCode(ErrorCode.FAILED);

        }
        return appUtil.setDesc(baseDto);
    }

    public BaseDto getByInvoiceNo(String invoiceNo, Long daybookId) {

        log.info("InvoiceService -> getByInvoiceNo method is called....");

        BaseDto baseDto = new BaseDto();

        try {

            InvoiceCreditNote invoiceCreditNote = invoiceCreditNoteRepository
                    .findByInvoiceCreditNoteNoAndDaybook(invoiceNo, daybookId);

            afterGet(invoiceCreditNote);

            baseDto.setResponseCode(ErrorCode.SUCCESS);
            baseDto.setResponseObject(invoiceCreditNote);

        } catch (RestException re) {
            log.error("Exception in InvoiceService -> getByInvoiceNo method ", re);
            baseDto.setResponseCode(re.getMessage());

        } catch (Exception e) {
            log.error("Exception in InvoiceService -> getByInvoiceNo method ", e);
            baseDto.setResponseCode(ErrorCode.FAILED);

        }
        return appUtil.setDesc(baseDto);
    }

    public BaseDto search(InvoiceSearchDto searchDto) {

        log.info("InvoiceService -> search method is called....");

        BaseDto baseDto = new BaseDto();

        try {
            searchDto.setIsServiceNull(false);
            baseDto.setResponseCode(ErrorCode.SUCCESS);
            baseDto.setResponseObject(invoiceCreditNoteSearchImpl.search(searchDto));

        } catch (RestException re) {
            log.error("Exception in InvoiceService -> search method ", re);
            baseDto.setResponseCode(re.getMessage());

        } catch (Exception e) {
            log.error("Exception in InvoiceService -> search method ", e);
            baseDto.setResponseCode(ErrorCode.FAILED);

        }
        return appUtil.setDesc(baseDto);
    }

    public BaseDto searchByDaybook(SearchRequest searchRequest, Long daybookId, String shipmentUid, String consolUid,
                                   String shipmentServiceId) {

        log.info("InvoiceService -> search method is called....");

        BaseDto baseDto = new BaseDto();

        try {

            baseDto.setResponseCode(ErrorCode.SUCCESS);
            baseDto.setResponseObject(invoiceCreditNoteSearchImpl.searchByDaybook(searchRequest, daybookId, shipmentUid,
                    consolUid, shipmentServiceId));

        } catch (RestException re) {
            log.error("Exception in InvoiceService -> search method ", re);
            baseDto.setResponseCode(re.getMessage());

        } catch (Exception e) {
            log.error("Exception in InvoiceService -> search method ", e);
            baseDto.setResponseCode(ErrorCode.FAILED);

        }
        return appUtil.setDesc(baseDto);
    }


    public BaseDto create(InvoiceCreditNote invoiceCreditNote) {

        log.info("InvoiceService -> create method is called....");

        BaseDto baseDto = new BaseDto();

        try {
            invoiceCreditNoteSearchImpl.create(baseDto, invoiceCreditNote);
        } catch (RestException re) {
            log.error("Exception in InvoiceService -> create method ", re);
            baseDto.setResponseCode(re.getMessage());

        } catch (Exception exception) {
            log.error("Exception in InvoiceService -> create method ", exception);

            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(exception);
            log.error("Exception Cause 1 ::: " + exceptionCause1);

            baseDto.setResponseCode(ErrorCode.FAILED);

            if (exceptionCause1.contains("FK_")) {
                baseDto.setResponseCode(ErrorCode.SAVE_UPDATE_FK_RECORD);
            }

        }
        return appUtil.setDesc(baseDto);
    }

    public void downloadAttachment(Long attachmentId, HttpServletResponse response) {

        InvoiceCreditNoteAttachment attachment = invoiceAttachmentRepository.getOne(attachmentId);

        if (attachment != null && attachment.getFile() != null && attachment.getFileName() != null) {

            try {

                response.setContentType(attachment.getFileContentType());

                response.setHeader("Content-Disposition", "attachment; filename=" + attachment.getFileName());

                response.setContentLength(attachment.getFile().length);

                response.getOutputStream().write(attachment.getFile());

                response.getOutputStream().flush();

                response.getOutputStream().close();

            } catch (Exception e) {
                log.error("Error occur during download attachment file : " + attachmentId + ", ", e);
            }

        } else {
            log.info("Attachment document is not available : " + attachmentId);
        }

    }

    public BaseDto getAttachmentById(Long attachmentId) {

        log.info("getAttachment method is called........Attachment ID : [" + attachmentId + "]");

        BaseDto baseDto = new BaseDto();

        try {

            InvoiceCreditNoteAttachment attachment = invoiceAttachmentRepository.findOne(attachmentId);

            if (attachment != null) {
                baseDto.setResponseObject(attachment);
                baseDto.setResponseCode(ErrorCode.SUCCESS);
            }
        } catch (Exception e) {

            log.error("Exception in getAttachment", e);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);
    }

    public BaseDto getallServiceTaxCategory(ServiceTaxCategoryMasterSearchDto searchDto) {

        log.info("InvoiceService -> search method is called....");

        BaseDto baseDto = new BaseDto();

        try {

            baseDto.setResponseCode(ErrorCode.SUCCESS);
            baseDto.setResponseObject(serviceTaxCategorySearchImpl.search(searchDto));

        } catch (RestException re) {
            log.error("Exception in InvoiceService -> search method ", re);
            baseDto.setResponseCode(re.getMessage());

        } catch (Exception e) {
            log.error("Exception in InvoiceService -> search method ", e);
            baseDto.setResponseCode(ErrorCode.FAILED);

        }
        return appUtil.setDesc(baseDto);

    }

    public BaseDto getServiceTaxCategoryById(Long id) {

        log.info("InvoiceService -> getServiceTaxCategoryById method is called....");

        BaseDto baseDto = new BaseDto();

        try {

            ServiceTaxCategoryMaster serviceTaxCategoryMaster = serviceTaxCategoryMasterRepository.findById(id);
            serviceTaxCategoryMaster.setTaxPercentageList(
                    serviceTaxPercentageRepository.findByServiceTaxCategoryMaster(serviceTaxCategoryMaster.getId()));

            baseDto.setResponseCode(ErrorCode.SUCCESS);
            baseDto.setResponseObject(serviceTaxCategoryMaster);

        } catch (RestException re) {
            log.error("Exception in InvoiceService -> getServiceTaxCategoryById method ", re);
            baseDto.setResponseCode(re.getMessage());

        } catch (Exception e) {
            log.error("Exception in InvoiceService -> getServiceTaxCategoryById method ", e);
            baseDto.setResponseCode(ErrorCode.FAILED);

        }
        return appUtil.setDesc(baseDto);
    }

    public BaseDto getServiceTaxPercentage(Long serviceId, Long chargeId, Long countryId, Long companyId) {

        log.info("InvoiceService -> get ServiceTax Percentage method is called....");

        BaseDto baseDto = new BaseDto();

        try {

            ServiceChargeTaxCategoryMapping serviceChargeTaxCategoryMapping = serviceChargeTaxCategoryMappingRepository
                    .findByServiceMasterAndChargeMaster(serviceId, chargeId);

            ServiceTaxCategoryMaster serviceTaxCategoryMaster = new ServiceTaxCategoryMaster();

            if (serviceChargeTaxCategoryMapping != null
                    && serviceChargeTaxCategoryMapping.getServiceTaxCategoryMaster() != null) {
                serviceTaxCategoryMaster = serviceTaxCategoryMasterRepository
                        .findById(serviceChargeTaxCategoryMapping.getServiceTaxCategoryMaster().getId());

                if (serviceTaxCategoryMaster != null && serviceTaxCategoryMaster.getId() != null)
                    serviceTaxCategoryMaster.setTaxPercentageList(serviceTaxPercentageRepository
                            .findServiceTaxPercentage(serviceTaxCategoryMaster.getId(), countryId, companyId));
            }
            baseDto.setResponseCode(ErrorCode.SUCCESS);
            baseDto.setResponseObject(serviceTaxCategoryMaster);

        } catch (RestException re) {
            log.error("Exception in InvoiceService -> getServiceTaxPercentage method ", re);
            baseDto.setResponseCode(re.getMessage());

        } catch (Exception e) {
            log.error("Exception in InvoiceService -> getServiceTaxPercentage method ", e);
            baseDto.setResponseCode(ErrorCode.FAILED);

        }
        return appUtil.setDesc(baseDto);
    }

    public BaseDto getShipmentServiceInvoiceList(String shipmentServiceId, String consolUid) {

        log.info("InvoiceService -> getShipmentServiceInvoiceList is called....");

        BaseDto baseDto = new BaseDto();

        try {
            List<InvoiceSearchDto> invoiceCreditNotes = new ArrayList<>();
            if (consolUid != null) {
                invoiceCreditNotes = invoiceCreditNoteSearchImpl
                        .findByShipmentServiceAndConsol(Long.parseLong(shipmentServiceId), consolUid);
            } else {
                invoiceCreditNotes = invoiceCreditNoteSearchImpl
                        .findByShipmentServiceDetail(Long.parseLong(shipmentServiceId));
            }
            baseDto.setResponseCode(ErrorCode.SUCCESS);
            baseDto.setResponseObject(invoiceCreditNotes);

        } catch (RestException re) {
            log.error("Exception in InvoiceService -> getShipmentServiceInvoiceList method ", re);
            baseDto.setResponseCode(re.getMessage());

        } catch (Exception e) {
            log.error("Exception in InvoiceService -> getShipmentServiceInvoiceList method ", e);
            baseDto.setResponseCode(ErrorCode.FAILED);

        }
        return appUtil.setDesc(baseDto);
    }

    public BaseDto getConsolInvoiceList(String consolUid) {

        log.info("InvoiceService -> getConsolInvoiceList is called....");

        BaseDto baseDto = new BaseDto();

        try {

            List<InvoiceSearchDto> invoiceCreditNotes = invoiceCreditNoteSearchImpl
                    .findByShipmentServiceAndConsol(null, consolUid);

            baseDto.setResponseCode(ErrorCode.SUCCESS);
            baseDto.setResponseObject(invoiceCreditNotes);

        } catch (RestException re) {
            log.error("Exception in InvoiceService -> getConsolInvoiceList method ", re);
            baseDto.setResponseCode(re.getMessage());

        } catch (Exception e) {
            log.error("Exception in InvoiceService -> getConsolInvoiceList method ", e);
            baseDto.setResponseCode(ErrorCode.FAILED);

        }
        return appUtil.setDesc(baseDto);
    }

    public BaseDto getInvoiceCount(InvoiceSearchDto searchDto) {

        log.info("InvoiceService -> getInvoiceCount is called...." + searchDto.getMasterUid() + "---" + searchDto.getServiceUidList());

        BaseDto baseDto = new BaseDto();

        try {

            Long count = invoiceCreditNoteRepository.countByConsolUidAndServiceUid(searchDto.getMasterUid(),
                    searchDto.getServiceUidList());
            baseDto.setResponseCode(ErrorCode.SUCCESS);
            baseDto.setResponseObject(count);

        } catch (Exception e) {
            log.error("Exception in InvoiceService -> getInvoiceCount method ", e);
            baseDto.setResponseCode(ErrorCode.FAILED);
        }
        return appUtil.setDesc(baseDto);
    }


    public BaseDto searchGlChargeMapping(SearchRequest searchRequest, Long serviceId) {

        log.info("Search method is called...." + searchRequest);

        BaseDto baseDto = new BaseDto();

        try {

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(generalLedgerChargeMappingSearchImpl.search(searchRequest, serviceId));

            log.info("Successfully Searching generalLedgerChargeMapping...");

        } catch (Exception exception) {

            log.error("Exception in Search method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);

    }

    public BaseDto getChargeListWithInvoiceNo(InvoiceChargeDto invoiceChargeDto) {
        BaseDto baseDto = new BaseDto();
        try {
            List<InvoiceCreditNote> invoiceCreditNotes = new ArrayList<>();
            List<InvoiceCreditNoteDetail> invoiceCreditNoteDetailList = invoiceChargeDto.getInvoiceCreditNoteDetailList();
            if (invoiceCreditNoteDetailList != null) {
                if (invoiceChargeDto.getConsolUid() != null) {
                    invoiceCreditNotes = invoiceCreditNoteRepository
                            .findByConsolUid(invoiceChargeDto.getConsolUid());
                } else {
                    invoiceCreditNotes = invoiceCreditNoteRepository
                            .findByShipmentServiceDetail(invoiceChargeDto.getServiceId());
                }

                for (InvoiceCreditNoteDetail invoiceCreditNoteDetail : invoiceCreditNoteDetailList) {

                    for (InvoiceCreditNote invoiceCreditNote : invoiceCreditNotes) {

                        for (InvoiceCreditNoteDetail invoiceDetail : invoiceCreditNote.getInvoiceCreditNoteDetailList()) {

                            if (invoiceDetail.getChargeMaster().getId().equals(invoiceCreditNoteDetail.getChargeMaster().getId())) {

                                invoiceCreditNoteDetail.setInvoiceNo(invoiceCreditNote.getInvoiceCreditNoteNo());
                                break;
                            }
                        }
                    }


                }

                baseDto.setResponseObject(invoiceCreditNoteDetailList);
                baseDto.setResponseCode(ErrorCode.SUCCESS);
                log.info("Successfully Searching ChargeListWithInvoiceNo...");
            } else {
                log.info("invoiceCreditNoteDetailList is empty");
            }

        } catch (Exception exception) {

            log.error("Exception in Search method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);
    }

    public BaseDto pushSave(InvoiceCreditNote entity) {


        BaseDto baseDto = new BaseDto();

        try {
            invoiceCreditNoteRepository.save(entity);
            baseDto.setResponseCode(ErrorCode.SUCCESS);
        } catch (Exception e) {
            // TODO: handle exception
            baseDto.setResponseCode(ErrorCode.FAILED);
            log.info("exception in push api save", e);

        }
        return appUtil.setDesc(baseDto);
    }


    public BaseDto getRatesFromProvision(SearchRequest searchRequest) {
        BaseDto baseDto = new BaseDto();
        try {
            if (searchRequest == null) {
                log.info("searchRequest is empty in invoicecreditnoteservice.getRateFromProvision ");
                baseDto.setResponseCode(ErrorCode.FAILED);
            } else {
                String serviceUid = searchRequest.getParam1();
                String consolUid = searchRequest.getParam2();
                String documentType = searchRequest.getParam3();
                String customerAgent = searchRequest.getParam4();

                List<InvoiceCreditNoteDetail> detailList = new ArrayList<>();

                if (serviceUid != null) {
                    prepareRatesData(serviceUid, null, detailList, customerAgent, documentType);
                } else if (consolUid != null) {
                    Consol consol = consolRepository.findByConsolUid(consolUid);
                    for (ShipmentLink shipmentLink : consol.getShipmentLinkList()) {
                        prepareRatesData(shipmentLink.getService().getServiceUid(), consolUid, detailList, customerAgent, documentType);
                    }
                }
                List<String> params = new ArrayList<>();

                for (InvoiceCreditNoteDetail key : detailList) {
                    if (key.getGlChargeMappingId() == null) {
                        params.add(key.getChargeCode());
                    }
                }
                if (params.size() > 0) {
                    baseDto.setParams(params);
                }

                baseDto.setResponseObject(detailList);
                baseDto.setResponseCode(ErrorCode.SUCCESS);
            }
        } catch (Exception e) {
            // TODO: handle exception
            baseDto.setResponseCode(ErrorCode.FAILED);
            log.info("exception in getRateFromProvision ", e);
        }
        return appUtil.setDesc(baseDto);
    }


    private void prepareRatesData(String serviceUid, String consolUid, List<InvoiceCreditNoteDetail> mapList, String customerAgent, String documentType) {

        if (serviceUid != null) {
            rateFromShipmentProvision(serviceUid, mapList, customerAgent, documentType);
            rateFromShipment(serviceUid, mapList, customerAgent, documentType);
        } else if (consolUid != null) {
            rateFromConsolProvision(consolUid, mapList, customerAgent, documentType);
        }
    }

    private void rateFromShipmentProvision(String serviceUid, List<InvoiceCreditNoteDetail> mapList, String customerAgent, String documentType) {

        Provisional provision = provisionalRepository.findByServiceUid(serviceUid);

        if (provision == null ||
                (provision != null && provision.getProvisionalItemList() != null && provision.getProvisionalItemList().size() == 0)) {
            return;
        }

        for (ProvisionItem item : provision.getProvisionalItemList()) {

            InvoiceCreditNoteDetail invoiceCreditNoteDetail = new InvoiceCreditNoteDetail();
            if (item.getShipmentService() == null) {
                continue;
            }
            invoiceCreditNoteDetail.setShipmentServiceDetail(new ShipmentServiceDetail(item.getShipmentService()));

            GeneralLedgerChargeMappingMaster glChargeMapping = glChargeMappingRepository.findByChargeAndService(item.getChargeMaster().getId(), item.getShipmentService().getServiceMaster().getId());

            if (glChargeMapping != null && glChargeMapping.getId() != null) {
                invoiceCreditNoteDetail.setGlChargeMappingId(glChargeMapping.getId());
            } else {
                invoiceCreditNoteDetail.setGlChargeMappingId(null);
            }
            invoiceCreditNoteDetail.setChargeMaster(item.getChargeMaster());
            invoiceCreditNoteDetail.setChargeName(item.getChargeMaster().getChargeName());
            invoiceCreditNoteDetail.setChargeCode(item.getChargeMaster().getChargeCode());

            if (item.getNoOfUnits() != null) {
                invoiceCreditNoteDetail.setUnit(item.getNoOfUnits());
            }

            if (documentType.equals("CRN-COST")) {
                invoiceCreditNoteDetail.setCrn(CRN.Cost);
                invoiceCreditNoteDetail.setDebitCredit(DebitCredit.Debit);
            } else {
                invoiceCreditNoteDetail.setCrn(CRN.Revenue);
                invoiceCreditNoteDetail.setDebitCredit(DebitCredit.Credit);
            }

            invoiceCreditNoteDetail.setCurrencyMaster(item.getSellCurrency());
            invoiceCreditNoteDetail.setAmountPerUnit(item.getSellAmountPerUnit());
            invoiceCreditNoteDetail.setRoe(item.getSellRoe());

            mapList.add(invoiceCreditNoteDetail);
        }
    }

    private void rateFromShipment(String serviceUid, List<InvoiceCreditNoteDetail> mapList, String customerAgent, String documentType) {

        Map<String, String> excludes = new HashMap<>();

        for (InvoiceCreditNoteDetail in : mapList) {
            excludes.put(in.getChargeMaster().getChargeCode(), in.getChargeMaster().getChargeCode());
        }

        ShipmentServiceDetail service = shipmentServiceDetailRepository.findByServiceUid(serviceUid);

        String rateMerged = appUtil.getLocationConfig("booking.rates.merged", service.getLocation(), true);

        for (ShipmentCharge charge : service.getShipmentChargeList()) {

            if (excludes.get(charge.getChargeMaster().getChargeCode()) != null) {
                continue;
            }

            boolean flag = false;

            if (customerAgent.equals(CustomerAgent.Agent.toString())) {
                if (isExportAndPpccCollect(service, charge) || isImportAndPpccCollect(service, charge)) {
                    flag = true;
                }
            } else if (customerAgent.equals(CustomerAgent.Customer.toString())) {
                if (isExportAndPpccPrepaid(service, charge) || isImportAndPpccCollect(service, charge)) {
                    flag = true;
                }
            }

            if (!flag) {
                continue;
            }

            InvoiceCreditNoteDetail invoiceCreditNoteDetail = new InvoiceCreditNoteDetail();

            invoiceCreditNoteDetail.setShipmentServiceDetail(new ShipmentServiceDetail(service));

            GeneralLedgerChargeMappingMaster glChargeMapping = glChargeMappingRepository.findByChargeAndService(charge.getChargeMaster().getId(), service.getServiceMaster().getId());

            if (glChargeMapping != null && glChargeMapping.getId() != null) {
                invoiceCreditNoteDetail.setGlChargeMappingId(glChargeMapping.getId());
            } else {
                invoiceCreditNoteDetail.setGlChargeMappingId(null);
            }
            invoiceCreditNoteDetail.setChargeMaster(charge.getChargeMaster());
            invoiceCreditNoteDetail.setChargeName(charge.getChargeName());
            invoiceCreditNoteDetail.setChargeCode(charge.getChargeCode());
            invoiceCreditNoteDetail.setShipmentRateId(charge.getId());

            if (invoiceCreditNoteDetail.getChargeMaster().getCalculationType().equals(ChargeCalculationType.Unit)) {
                invoiceCreditNoteDetail.setUnit(service.getBookedChargeableUnit());
            } else if (invoiceCreditNoteDetail.getChargeMaster().getCalculationType().equals(ChargeCalculationType.Shipment)) {
                invoiceCreditNoteDetail.setUnit((double) 1);
            } else if (invoiceCreditNoteDetail.getChargeMaster().getCalculationType().equals(ChargeCalculationType.Document)) {
                invoiceCreditNoteDetail.setUnit((double) service.getDocumentList().size());
            }
            if (documentType.equals("CRN-COST")) {
                invoiceCreditNoteDetail.setCrn(CRN.Cost);
                invoiceCreditNoteDetail.setDebitCredit(DebitCredit.Debit);
            } else {
                invoiceCreditNoteDetail.setCrn(CRN.Revenue);
                invoiceCreditNoteDetail.setDebitCredit(DebitCredit.Credit);
            }

            if (rateMerged.equals("TRUE") || rateMerged == "TRUE") {
                if (documentType.equals("CRN-COST")) {
                    invoiceCreditNoteDetail.setCurrencyMaster(charge.getCostCurrency());
                    invoiceCreditNoteDetail.setAmountPerUnit(charge.getCostAmount());
                } else {
                    invoiceCreditNoteDetail.setCurrencyMaster(charge.getRateCurrency());
                    invoiceCreditNoteDetail.setAmountPerUnit(charge.getRateAmount());
                }
            } else {
                if (documentType.equals("CRN-COST")) {
                    invoiceCreditNoteDetail.setCurrencyMaster(charge.getCostCurrency());
                    invoiceCreditNoteDetail.setAmountPerUnit(charge.getCostAmount());
                } else {
                    invoiceCreditNoteDetail.setCurrencyMaster(charge.getNetSaleCurrency());
                    invoiceCreditNoteDetail.setAmountPerUnit(charge.getNetSaleAmount());
                }
            }

            if (invoiceCreditNoteDetail.getCurrencyMaster() != null) {
                Map<Long, FromToCurrency> map = (Map<Long, FromToCurrency>) currencyRateSearchImpl.search(invoiceCreditNoteDetail.getCurrencyMaster().getId()).getSearchResult();

                if (invoiceCreditNoteDetail.getCurrencyMaster() != null && map.get(invoiceCreditNoteDetail.getCurrencyMaster().getId()) != null) {
                    invoiceCreditNoteDetail.setRoe(map.get(invoiceCreditNoteDetail.getCurrencyMaster().getId()).getCSell());
                }
                if (AuthService.getCurrentUser().getSelectedUserLocation().getCurrencyMaster().getId().equals(invoiceCreditNoteDetail.getCurrencyMaster().getId())) {
                    invoiceCreditNoteDetail.setRoe((double) 1);
                }
            }
            mapList.add(invoiceCreditNoteDetail);
        }
    }

    private boolean isExportAndPpccPrepaid(ShipmentServiceDetail service, ShipmentCharge charge) {
        return service.getServiceMaster().getImportExport() == ImportExport.Export
                && charge.getPpcc() == PPCC.Prepaid;
    }

    private boolean isImportAndPpccCollect(ShipmentServiceDetail service, ShipmentCharge charge) {
        return service.getServiceMaster().getImportExport() == ImportExport.Import
                && charge.getPpcc() == PPCC.Collect;
    }

    private boolean isExportAndPpccCollect(ShipmentServiceDetail service, ShipmentCharge charge) {
        return service.getServiceMaster().getImportExport() == ImportExport.Export
                && charge.getPpcc() == PPCC.Collect;
    }

    private void rateFromConsolProvision(String consolUid, List<InvoiceCreditNoteDetail> mapList, String customerAgent, String documentType) {

        Provisional provision = provisionalRepository.findByMasterUid(consolUid);

        if (provision == null ||
                (provision != null && provision.getProvisionalItemList() != null && provision.getProvisionalItemList().size() == 0)) {
            return;
        }

        for (ProvisionItem item : provision.getProvisionalItemList()) {

            InvoiceCreditNoteDetail invoiceCreditNoteDetail = new InvoiceCreditNoteDetail();

            invoiceCreditNoteDetail.setShipmentServiceDetail(null);

            if (item.getShipmentService() == null) {
                continue;
            }

            GeneralLedgerChargeMappingMaster glChargeMapping = glChargeMappingRepository.findByChargeAndService(item.getChargeMaster().getId(), item.getShipmentService().getServiceMaster().getId());

            if (glChargeMapping != null && glChargeMapping.getId() != null) {
                invoiceCreditNoteDetail.setGlChargeMappingId(glChargeMapping.getId());
            } else {
                invoiceCreditNoteDetail.setGlChargeMappingId(null);
            }
            invoiceCreditNoteDetail.setChargeMaster(item.getChargeMaster());
            invoiceCreditNoteDetail.setChargeName(item.getChargeMaster().getChargeName());
            invoiceCreditNoteDetail.setChargeCode(item.getChargeMaster().getChargeCode());

            if (item.getNoOfUnits() != null) {
                invoiceCreditNoteDetail.setUnit(item.getNoOfUnits());
            }

            if (documentType.equals("CRN-COST")) {
                invoiceCreditNoteDetail.setCrn(CRN.Cost);
                invoiceCreditNoteDetail.setDebitCredit(DebitCredit.Debit);
            } else {
                invoiceCreditNoteDetail.setCrn(CRN.Revenue);
                invoiceCreditNoteDetail.setDebitCredit(DebitCredit.Credit);
            }

            invoiceCreditNoteDetail.setCurrencyMaster(item.getSellCurrency());
            invoiceCreditNoteDetail.setAmountPerUnit(item.getSellAmountPerUnit());
            invoiceCreditNoteDetail.setRoe(item.getSellRoe());

            mapList.add(invoiceCreditNoteDetail);
        }
    }

}
