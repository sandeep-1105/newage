package com.efreightsuite.service;


import com.efreightsuite.common.component.CacheRepository;
import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.DivisionMasterSearchDto;
import com.efreightsuite.dto.SearchRequest;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.ExceptionHelper;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.exception.UniqueKey;
import com.efreightsuite.model.DivisionMaster;
import com.efreightsuite.repository.CompanyMasterRepository;
import com.efreightsuite.repository.DivisionMasterRepository;
import com.efreightsuite.search.CommonSearchImpl;
import com.efreightsuite.search.DivisionMasterSearchImpl;
import com.efreightsuite.util.AppUtil;
import com.efreightsuite.util.SaaSUtil;
import com.efreightsuite.util.ValidateUtil;
import com.efreightsuite.validation.DivisionMasterValidator;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.exception.ExceptionUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

/**
 * @author achyutananda
 */

@Service
@Log4j2
public class DivisionMasterService {


    @Autowired
    private
    AppUtil appUtil;


    @Autowired
    private
    DivisionMasterRepository divisionMasterRepository;


    @Autowired
    CompanyMasterRepository companyMasterRepository;


    @Autowired
    private
    CacheRepository cacheRepository;


    @Autowired
    private
    DivisionMasterValidator divisionMasterValidator;

    @Autowired
    private
    DivisionMasterSearchImpl divisionMasterSearchImpl;

    @Autowired
    private
    CommonSearchImpl commonSearchImpl;


    /**
     * Method - getAll() Returns all the division in a list format
     *
     * @return BaseDto
     */

    public BaseDto search(SearchRequest searchRequest) {

        log.info("getAll method is called..");

        BaseDto baseDto = new BaseDto();

        try {

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(divisionMasterSearchImpl.search(searchRequest, null));

            log.info("getAll method is executed successfully..");

        } catch (RestException exception) {

            log.error("Exception in getAll method : ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (Exception exception) {

            log.error("Exception in getAll method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);
    }


    public BaseDto searchDivisionNotInList(SearchRequest searchRequest) {

        log.info("searchDivisionNotInList method is called....");

        BaseDto baseDto = new BaseDto();

        try {

            baseDto.setResponseObject(commonSearchImpl.searchDivisionNotInList(searchRequest));

            baseDto.setResponseCode(ErrorCode.SUCCESS);

        } catch (Exception exception) {

            log.error("Exception in searchDivisionNotInList method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);
    }

    public BaseDto search(Long companyId, SearchRequest searchRequest) {

        log.info("getAll method is called..");

        BaseDto baseDto = new BaseDto();

        try {

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(divisionMasterSearchImpl.search(searchRequest, companyId));

            log.info("getAll method is executed successfully..");

        } catch (RestException exception) {

            log.error("Exception in getAll method : ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (Exception exception) {

            log.error("Exception in getAll method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);
    }


    public BaseDto search(DivisionMasterSearchDto dto) {
        BaseDto baseDto = new BaseDto();
        try {
            baseDto.setResponseCode(ErrorCode.SUCCESS);
            baseDto.setResponseObject(divisionMasterSearchImpl.search(dto));

        } catch (RestException exception) {
            log.error("Exception in getAll method : ", exception);
            baseDto.setResponseCode(exception.getMessage());
        } catch (Exception exception) {
            log.error("Exception in getAll method : ", exception);
            baseDto.setResponseCode(ErrorCode.FAILED);
        }
        return appUtil.setDesc(baseDto);
    }


    /**
     * Method - getByDivisionId Returns DivisionMaster Object Based on the division Id
     *
     * @return baseDto
     */

    public BaseDto get(Long id) {

        log.info("get method is called [" + id + "]");

        BaseDto baseDto = new BaseDto();

        try {

            DivisionMaster divisionMaster = cacheRepository.get(SaaSUtil.getSaaSId(), DivisionMaster.class.getName(), id, divisionMasterRepository);

            // Validating DivisionMaster for not null
            ValidateUtil.notNull(divisionMaster, ErrorCode.DIVISION_ID_NOT_FOUND);

            log.info("Successfully getting Division by id.....[" + id + "]");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(divisionMaster);


        } catch (RestException exception) {

            log.error("Exception in get method ", exception);

            baseDto.setResponseCode(exception.getMessage());
        } catch (Exception e) {

            log.error("Exception in get method", e);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);
    }

    /**
     * Method - Create This method create a new Division
     *
     * @param divisionMaster
     * @return BaseDto
     */

    public BaseDto create(DivisionMaster divisionMaster) {

        log.info("Create method is Invoked.....");

        BaseDto baseDto = new BaseDto();

        try {

            divisionMasterValidator.validate(divisionMaster);

            divisionMaster = cacheRepository.save(SaaSUtil.getSaaSId(), DivisionMaster.class.getName(), divisionMaster, divisionMasterRepository);

            log.info("Division Successfully Saved....[" + divisionMaster.getId() + "]");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(divisionMaster);

        } catch (RestException exception) {

            log.error("Rest Exception occured with error code ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (Exception exception) {

            log.error("Exception occured ", exception);

            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Exception Cause 1 : " + exceptionCause1);

            // General error condition
            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            // Duplicate Division Code
            if (exceptionCause1.contains(UniqueKey.UK_DIVISION_DIVISIONCODE)) {
                baseDto.setResponseCode(ErrorCode.DIVISION_CODE_ALREADY_EXIST);
            }

            // Duplicate Division Name
            if (exceptionCause1.contains(UniqueKey.UK_DIVISION_DIVISIONNAME)) {
                baseDto.setResponseCode(ErrorCode.DIVISION_NAME_ALREADY_EXIST);
            }
        }

        return appUtil.setDesc(baseDto);
    }


    /**
     * Method - Update This method updates the existing Division
     *
     * @return BaseDto
     */

    public BaseDto update(DivisionMaster divisionMaster) {

        log.info("update method is Invoked....[ " + divisionMaster.getId() + "]");

        BaseDto baseDto = new BaseDto();

        try {

            divisionMasterValidator.validate(divisionMaster);

            divisionMasterRepository.save(divisionMaster);

            cacheRepository.remove(SaaSUtil.getSaaSId(), DivisionMaster.class.getName(), divisionMaster.getId());

            log.info("DivisionMaster Successfully Updated...[" + divisionMaster.getId() + "]");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(divisionMaster);

        } catch (RestException exception) {

            log.error("Rest Exception Occured with error code ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (Exception exception) {

            log.error("Exception occured ", exception);

            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Exception Cause 1 : " + exceptionCause1);

            // General error condition
            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            // Duplicate Division Code
            if (exceptionCause1.contains(UniqueKey.UK_DIVISION_DIVISIONCODE)) {
                baseDto.setResponseCode(ErrorCode.DIVISION_CODE_ALREADY_EXIST);
            }

            // Duplicate Division Name
            if (exceptionCause1.contains(UniqueKey.UK_DIVISION_DIVISIONNAME)) {
                baseDto.setResponseCode(ErrorCode.DIVISION_NAME_ALREADY_EXIST);
            }
        }

        return appUtil.setDesc(baseDto);
    }


    /**
     * Deletes the Existing Division Based on the Given Id
     *
     * @Returns BaseDto
     */

    public BaseDto delete(Long id) {

        log.info("Delete method is Invoked...[" + id + "]");

        BaseDto baseDto = new BaseDto();

        try {

            cacheRepository.delete(SaaSUtil.getSaaSId(), DivisionMaster.class.getName(), id, divisionMasterRepository);

            log.info("DivisionMaster Deleted Successfully....[" + id + "]");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

        } catch (DataIntegrityViolationException exception) {

            log.error("Exception occured : ", exception);

            ExceptionHelper helper = new ExceptionHelper(exception);

            log.error("Foreign key reference : " + helper.getFkReference());

            baseDto.setResponseCode(ErrorCode.ERROR_FK_CONSTRAINT);

        } catch (Exception exception) {

            log.error("Exception in Delete method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);
    }
}
