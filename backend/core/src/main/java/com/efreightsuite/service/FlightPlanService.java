package com.efreightsuite.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.FlightPlanCopyDto;
import com.efreightsuite.dto.FlightPlanSearchDto;
import com.efreightsuite.enumeration.DirectStopOver;
import com.efreightsuite.enumeration.FlightPlanStatus;
import com.efreightsuite.enumeration.SequenceType;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.exception.UniqueKey;
import com.efreightsuite.model.FlightPlan;
import com.efreightsuite.model.FlightPlanConnection;
import com.efreightsuite.repository.FlightPlanRepository;
import com.efreightsuite.search.FlightPlanSearchImpl;
import com.efreightsuite.util.AppUtil;
import com.efreightsuite.util.TimeUtil;
import com.efreightsuite.validation.FlightPlanValidator;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.exception.ExceptionUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.ObjectOptimisticLockingFailureException;
import org.springframework.orm.jpa.JpaObjectRetrievalFailureException;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class FlightPlanService {

    @Autowired
    private
    AppUtil appUtil;

    @Autowired
    private
    FlightPlanRepository flightPlanRepository;

    @Autowired
    private
    FlightPlanSearchImpl flightPlanSearchImpl;

    @Autowired
    private
    FlightPlanValidator flightPlanValidator;

    @Autowired
    private
    SequenceGeneratorService sequenceGeneratorService;


    public BaseDto get(Long id) {

        log.info("Get method is called....");

        BaseDto baseDto = new BaseDto();

        try {

            FlightPlan flightPlan = flightPlanRepository.findById(id);

            baseDto.setResponseCode(ErrorCode.SUCCESS);
            baseDto.setResponseObject(flightPlan);

        } catch (RestException re) {
            log.error("Exception in get method ", re);
            baseDto.setResponseCode(re.getMessage());

        } catch (Exception e) {
            log.error("Exception in get method ", e);
            baseDto.setResponseCode(ErrorCode.FAILED);

        }
        return appUtil.setDesc(baseDto);
    }


    public BaseDto search(FlightPlanSearchDto flightPlanSearchDto) {

        log.info("Search method is called " + flightPlanSearchDto);

        BaseDto baseDto = new BaseDto();

        try {
            baseDto.setResponseObject(flightPlanSearchImpl.search(flightPlanSearchDto));
            baseDto.setResponseCode(ErrorCode.SUCCESS);

        } catch (RestException re) {

            log.error("Search method ", re);

            baseDto.setResponseCode(re.getMessage());

        } catch (Exception e) {

            log.error("Search method ", e);

            baseDto.setResponseCode(ErrorCode.FAILED);

        }
        return appUtil.setDesc(baseDto);
    }

    public BaseDto shipmentFlightPlanSearch(FlightPlanSearchDto flightPlanSearchDto) {

        log.info("Search method is called " + flightPlanSearchDto);

        BaseDto baseDto = new BaseDto();

        try {
            baseDto.setResponseObject(flightPlanSearchImpl.flightPlanSearch(flightPlanSearchDto));
            baseDto.setResponseCode(ErrorCode.SUCCESS);

        } catch (RestException re) {

            log.error("Search method ", re);

            baseDto.setResponseCode(re.getMessage());

        } catch (Exception e) {

            log.error("Search method ", e);

            baseDto.setResponseCode(ErrorCode.FAILED);

        }
        return appUtil.setDesc(baseDto);
    }

    @Transactional
    private List<FlightPlan> generateSchulde(FlightPlanCopyDto flightPlanCopyDto) {

        FlightPlan flightPlan = flightPlanCopyDto.getFlightPlan();

        flightPlanValidator.validate(flightPlan);

        parentObjectAssigned(flightPlan);

        Date startDate = flightPlan.getEtd();
        Integer frequency = 0;

        List<FlightPlan> list = new ArrayList<>();
        do {
            FlightPlan fp = new FlightPlan();
            fp.setScheduleId(sequenceGeneratorService.getSequence(SequenceType.FLIGHTPLAN));
            fp.setCapacityReserved(flightPlan.getCapacityReserved());
            fp.setFlightPlanStatus(FlightPlanStatus.Planned);
            fp.setFlightFrequency(flightPlan.getFlightFrequency());
            fp.setEffectiveUpto(flightPlan.getEffectiveUpto());

            fp.setConnectionList(new ArrayList<>());

            for (FlightPlanConnection fpc : flightPlan.getConnectionList()) {

                FlightPlanConnection fc = new FlightPlanConnection();
                fc.setFlightPlan(fp);
                fc.setCarrierMaster(fpc.getCarrierMaster());
                fc.setFlightNo(fpc.getFlightNo());
                fc.setPol(fpc.getPol());
                fc.setPod(fpc.getPod());

                if (frequency == 0) {
                    fc.setEtd(fpc.getEtd());
                    fc.setEta(fpc.getEta());
                } else {

                    fc.setEtd(fpc.getEtd());
                    fc.setEta(fpc.getEta());

                    fc.setEtd(TimeUtil.addDate(fpc.getEtd(), frequency));
                    fc.setEta(TimeUtil.addDate(fpc.getEta(), frequency));
                }
                fp.getConnectionList().add(fc);

            }

            parentObjectAssigned(fp);

            list.add(fp);

            if (flightPlan.getFlightFrequency() <= 0) {
                break;
            }

            frequency = frequency + flightPlan.getFlightFrequency();
            startDate = TimeUtil.addDate(flightPlan.getEtd(), frequency);
            log.info("frequency : " + frequency + ", startDate : " + startDate + ", Eff:" + flightPlan.getEffectiveUpto());

        } while (startDate.before(flightPlan.getEffectiveUpto()));

        if (flightPlanCopyDto.getCopyFrom() != null && flightPlanCopyDto.getCopyFrom().equals("VIEW")) {
            list.remove(0);
        }

        list = flightPlanRepository.save(list);

        return list;

    }

    private BaseDto copySchulde(FlightPlanCopyDto flightPlanCopyDto) {

        log.info("Create method is called....");

        BaseDto baseDto = new BaseDto();

        try {

            baseDto.setResponseObject(generateSchulde(flightPlanCopyDto));
            baseDto.setResponseCode(ErrorCode.SUCCESS);

        } catch (RestException re) {

            log.error("Exception in FlightPlanService -> create method ", re);

            baseDto.setResponseCode(re.getMessage());

        } catch (Exception exception) {

            log.error("Exception in FlightPlanService -> create method ", exception);

            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Exception Cause 1 ::: " + exceptionCause1);

            baseDto.setResponseCode(ErrorCode.FAILED);

            if (exceptionCause1.contains(UniqueKey.UK_FLIGHTP_SCHEDULEID)) {
                baseDto.setResponseCode(ErrorCode.SCHEDULE_ID_ALREADY_EXIST);
            }

            if (exceptionCause1.contains(UniqueKey.UK_FLIGHTP_FLIGHTCON)) {
                baseDto.setResponseCode(ErrorCode.FLIGHTPLAN_DUPLICATED);
            }


            if (exceptionCause1.contains("FK_")) {
                baseDto.setResponseCode(ErrorCode.SAVE_UPDATE_FK_RECORD);
            }

        }
        return appUtil.setDesc(baseDto);
    }

    @Transactional
    private FlightPlan saveTransactionally(FlightPlan flightPlan) {
        flightPlanValidator.validate(flightPlan);

        if (flightPlan.getId() == null) {
            flightPlan.setScheduleId(sequenceGeneratorService.getSequenceValueByType(SequenceType.FLIGHTPLAN));
        }

        for (FlightPlanConnection flightPlanConnection : flightPlan.getConnectionList()) {
            flightPlanConnection.setFlightPlan(flightPlan);
        }


        parentObjectAssigned(flightPlan);

        flightPlan = flightPlanRepository.save(flightPlan);

        return flightPlan;
    }


    public BaseDto saveOrUpdate(FlightPlan flightPlan) {

        log.info("saveOrUpdate method is called....");

        BaseDto baseDto = new BaseDto();

        try {

            baseDto.setResponseCode(ErrorCode.SUCCESS);
            baseDto.setResponseObject(saveTransactionally(flightPlan));

        } catch (RestException re) {

            log.error("Exception in FlightPlanService -> update method ", re);

            baseDto.setResponseCode(re.getMessage());

        } catch (ObjectOptimisticLockingFailureException e) {

            log.error("Error in editing FlightPlan", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_LOCKED_RECORD);

        } catch (JpaObjectRetrievalFailureException e) {

            log.error("Error in editing FlightPlan", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_DELETED_RECORD);

        } catch (Exception exception) {
            log.error("Exception in FlightPlanService -> update method ", exception);

            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(exception);
            log.error("Exception Cause 1 ::: " + exceptionCause1);

            baseDto.setResponseCode(ErrorCode.FAILED);

            if (exceptionCause1.contains(UniqueKey.UK_FLIGHTP_SCHEDULEID)) {
                baseDto.setResponseCode(ErrorCode.SCHEDULE_ID_ALREADY_EXIST);
            }

            if (exceptionCause1.contains(UniqueKey.UK_FLIGHTP_FLIGHTCON)) {
                baseDto.setResponseCode(ErrorCode.FLIGHTPLAN_DUPLICATED);
            }

            if (exceptionCause1.contains("FK_")) {
                baseDto.setResponseCode(ErrorCode.SAVE_UPDATE_FK_RECORD);
            }

        }
        return appUtil.setDesc(baseDto);
    }

    private void parentObjectAssigned(FlightPlan fp) {

        fp.setCarrierMaster(fp.getConnectionList().get(0).getCarrierMaster());
        fp.setFlightNo(fp.getConnectionList().get(0).getFlightNo());
        fp.setPol(fp.getConnectionList().get(0).getPol());
        fp.setPod(fp.getConnectionList().get(fp.getConnectionList().size() - 1).getPod());
        fp.setEtd(fp.getConnectionList().get(0).getEtd());
        fp.setEta(fp.getConnectionList().get(fp.getConnectionList().size() - 1).getEta());

        if (fp.getConnectionList().size() == 1) {
            fp.setDirectStopOver(DirectStopOver.Direct);
        } else {
            fp.setDirectStopOver(DirectStopOver.Stop_Over);
        }


    }

    public BaseDto delete(Long id) {

        log.info("Delete method is Invoked...[" + id + "]");

        BaseDto baseDto = new BaseDto();

        try {

            flightPlanRepository.delete(id);

            log.info("Deleted Successfully....[" + id + "]");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

        } catch (Exception exception) {

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            log.error("Exception in Delete method : ", exception);

            String rootCause = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Root Cause of Exception : " + rootCause);

            if (rootCause.contains("FK_")) {
                baseDto.setResponseCode(ErrorCode.CANNOT_DELETE_FK_RECORD);
            }
        }

        return appUtil.setDesc(baseDto);
    }


    public BaseDto copy(FlightPlanCopyDto flightPlanCopyDto) {

        log.info("Copy method is called " + flightPlanCopyDto.getFlightPlan().getId());

        flightPlanCopyDto.getFlightPlan().setEffectiveUpto(flightPlanCopyDto.getEffectiveUpto());

        return copySchulde(flightPlanCopyDto);
    }


    public BaseDto getFlightPlanSchedulebyCarrierAndDate(FlightPlanSearchDto data) {
        BaseDto baseDto = new BaseDto();
        baseDto.setResponseCode(ErrorCode.SUCCESS);
        List<FlightPlan> dataList = null;
        data.setDate(TimeUtil.getCurrentLocationTime());
        if (data.getIds() != null && data.getIds().length > 0) {
            log.info("data.getIds() " + data.getIds());
            baseDto.setResponseObject(flightPlanSearchImpl.search(data));
            //dataList = flightPlanRepository.findFlightPlanSchedule(data.getCarrierId(), data.getPorId(), TimeUtil.getCurrentLocationTime(), data.getIds());
        } else {
            log.info("data.getMawbList() " + data.getCarrierId() + "  ---   " + data.getPorId());
            baseDto.setResponseObject(flightPlanSearchImpl.search(data));
            //dataList = flightPlanRepository.findFlightPlanSchedule(data.getCarrierId(), data.getPorId(), TimeUtil.getCurrentLocationTime());

        }
        return appUtil.setDesc(baseDto);
    }

}
