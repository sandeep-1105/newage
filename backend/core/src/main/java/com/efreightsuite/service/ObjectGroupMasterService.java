package com.efreightsuite.service;

import java.util.List;

import com.efreightsuite.common.component.CacheRepository;
import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.ObjectGroupMasterSearchDto;
import com.efreightsuite.dto.SearchRequest;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.ExceptionHelper;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.exception.UniqueKey;
import com.efreightsuite.model.ObjectGroupMaster;
import com.efreightsuite.repository.ObjectGroupMasterRepository;
import com.efreightsuite.search.ObjectGroupSearchImpl;
import com.efreightsuite.util.AppUtil;
import com.efreightsuite.util.SaaSUtil;
import com.efreightsuite.util.ValidateUtil;
import com.efreightsuite.validation.ObjectGroupMasterValidator;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.exception.ExceptionUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class ObjectGroupMasterService {

    @Autowired
    private
    AppUtil appUtil;

    @Autowired
    private
    ObjectGroupMasterRepository objectGroupMasterRepository;
    @Autowired
    private
    CacheRepository cacheRepository;

    @Autowired
    private
    ObjectGroupSearchImpl objectGroupSearchImpl;

    @Autowired
    private
    ObjectGroupMasterValidator objectGroupMasterValidator;

    public BaseDto search(SearchRequest searchRequest) {

        log.info("getAll method is called....");

        BaseDto baseDto = new BaseDto();

        try {

            baseDto.setResponseCode(ErrorCode.SUCCESS);
            baseDto.setResponseObject(objectGroupSearchImpl.search(searchRequest));

        } catch (RestException re) {
            log.error("Exception in getAll method ", re);
            baseDto.setResponseCode(re.getMessage());

        } catch (Exception e) {
            log.error("Exception in getAll method ", e);
            baseDto.setResponseCode(ErrorCode.FAILED);

        }
        return appUtil.setDesc(baseDto);
    }

    public BaseDto search(ObjectGroupMasterSearchDto searchDto) {

        log.info("getAll method is called....");

        BaseDto baseDto = new BaseDto();

        try {

            log.info("Successfully getting list of zone...");
            baseDto.setResponseCode(ErrorCode.SUCCESS);
            baseDto.setResponseObject(objectGroupSearchImpl.search(searchDto));

        } catch (RestException re) {
            log.error("Exception in getAll method ", re);
            baseDto.setResponseCode(re.getMessage());

        } catch (Exception e) {
            log.error("Exception in getAll method ", e);
            baseDto.setResponseCode(ErrorCode.FAILED);

        }
        return appUtil.setDesc(baseDto);
    }

    public BaseDto get(Long id) {

        log.info("get method is called [" + id + "]");

        BaseDto baseDto = new BaseDto();

        try {

            //ObjectGroupMaster objectGroupMaster = cacheRepository.get(SaaSUtil.getSaaSId(), ObjectGroupMaster.class.getName(),id, objectGroupMasterRepository);

            ObjectGroupMaster objectGroupMaster = objectGroupMasterRepository.getOne(id);

            ValidateUtil.notNull(objectGroupMaster, ErrorCode.OBJECTGROUP_ID_NOT_FOUND);

            log.info("Successfully getting object Group by id.....[" + id + "]");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(objectGroupMaster);

        } catch (RestException exception) {

            log.error("Exception in get method ", exception);

            baseDto.setResponseCode(exception.getMessage());
        } catch (Exception e) {

            log.error("Exception in get method", e);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);
    }

    public BaseDto saveOrUpdate(ObjectGroupMaster objectGroupMaster) {


        BaseDto baseDto = new BaseDto();

        try {

            log.info("update method is Invoked....[ " + objectGroupMaster.getId() + "]");

            objectGroupMasterValidator.validate(objectGroupMaster);

            objectGroupMasterRepository.save(objectGroupMaster);

            log.info("ObjectGroupMaster Successfully Updated...[" + objectGroupMaster.getId() + "]");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(objectGroupMaster);


        } catch (RestException exception) {

            log.error("Rest Exception Occured with error code ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (Exception exception) {

            log.error("Exception occured ", exception);

            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Exception Cause 1 : " + exceptionCause1);

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            if (exceptionCause1.contains(UniqueKey.UK_OBJECTGROUP_OBJECTGROUPCODE)) {
                baseDto.setResponseCode(ErrorCode.OBJECTGROUP_CODE_ALREADY_EXIST);
            }

            if (exceptionCause1.contains(UniqueKey.UK_OBJECTGROUP_OBJECTGROUPNAME)) {
                baseDto.setResponseCode(ErrorCode.OBJECTGROUP_NAME_ALREADY_EXIST);
            }
        }

        return appUtil.setDesc(baseDto);
    }

    public BaseDto delete(Long id) {

        log.info("Delete method is Invoked...[" + id + "]");

        BaseDto baseDto = new BaseDto();

        try {

            cacheRepository.delete(SaaSUtil.getSaaSId(), ObjectGroupMaster.class.getName(), id, objectGroupMasterRepository);

            log.info("ObjectGroupMaster Deleted Successfully....[" + id + "]");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

        } catch (DataIntegrityViolationException exception) {

            log.error("Exception occured : ", exception);

            ExceptionHelper helper = new ExceptionHelper(exception);

            log.error("Foreign key reference : " + helper.getFkReference());

            baseDto.setResponseCode(ErrorCode.ERROR_FK_CONSTRAINT);

        } catch (Exception exception) {

            log.error("Exception in Delete method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);
    }

    public BaseDto getAll() {

        log.info("getAll method is called..");

        BaseDto baseDto = new BaseDto();

        try {

            List<ObjectGroupMaster> objectGroupMasterList = objectGroupMasterRepository.findAll();

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(objectGroupMasterList);

            log.info("getAll method is executed successfully..");

        } catch (RestException exception) {

            log.error("Exception in getAll method : ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (Exception exception) {

            log.error("Exception in getAll method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);
    }

}
