package com.efreightsuite.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.servlet.http.HttpServletResponse;

import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.PurchaseOrderSearchDto;
import com.efreightsuite.dto.SearchRespDto;
import com.efreightsuite.enumeration.SequenceType;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.exception.UniqueKey;
import com.efreightsuite.model.CompanyMaster;
import com.efreightsuite.model.PurchaseOrder;
import com.efreightsuite.model.PurchaseOrderAttachment;
import com.efreightsuite.model.PurchaseOrderItem;
import com.efreightsuite.model.PurchaseOrderRemarkFollowUp;
import com.efreightsuite.model.PurchaseOrderVehicleInfo;
import com.efreightsuite.model.UserProfile;
import com.efreightsuite.repository.DefaultMasterDataRepository;
import com.efreightsuite.repository.PurchaseOrderAttachmentRepository;
import com.efreightsuite.repository.PurchaseOrderRepository;
import com.efreightsuite.util.AppUtil;
import com.efreightsuite.util.CryptoUtils;
import com.efreightsuite.util.StaticSearchQuery;
import com.efreightsuite.validation.PurchaseOrderValidator;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.exception.ExceptionUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import static com.efreightsuite.constants.Constants.PROTECTED_FILE_NAME;
import static com.efreightsuite.constants.Constants.UNPROTECTED_FILE_NAME;

@Service
@Log4j2
public class PurchaseOrderService {

    @Autowired
    private
    AppUtil appUtil;

    @Autowired
    private
    PurchaseOrderAttachmentRepository purchaseOrderAttachmentRepository;

    @Autowired
    private
    PurchaseOrderRepository purchaseOrderRepository;

    @Autowired
    DefaultMasterDataRepository defaultMasterDataRepository;

    @Autowired
    private
    SequenceGeneratorService sequenceGeneratorService;

    @Autowired
    private
    PurchaseOrderValidator purchaseOrderValidator;

    @Autowired
    private
    EntityManager entityManager;

    private final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:SS");

    public BaseDto create(PurchaseOrder purchaseOrder) {

        String KEY = appUtil.getLocationConfig("shipment.attach.file.encrypt.key", purchaseOrder.getLocationMaster(), false);

        log.info("PurchaseOrderService -> create method is called....");

        BaseDto baseDto = new BaseDto();
        boolean exceptioned = false;

        try {

            purchaseOrderValidator.validate(purchaseOrder);

            if (purchaseOrder.getPurchaseOrderItemList() != null && !purchaseOrder.getPurchaseOrderItemList().isEmpty()) {
                for (PurchaseOrderItem purchaseOrderItem : purchaseOrder.getPurchaseOrderItemList()) {
                    purchaseOrderItem.setPurchaseOrder(purchaseOrder);
                }
            } else {
                purchaseOrder.setPurchaseOrderItemList(null);
            }
            if (purchaseOrder.getPurchaseOrderAttachmentList() != null && !purchaseOrder.getPurchaseOrderAttachmentList().isEmpty()) {
                for (PurchaseOrderAttachment attachment : purchaseOrder.getPurchaseOrderAttachmentList()) {

                    if (attachment.getProtectedFile() != null) {
                        byte[] encryptedFile = CryptoUtils.encrypt(KEY, attachment.getProtectedFile());
                        attachment.setProtectedFile(encryptedFile);
                        if (!Arrays.toString(attachment.getProtectedFile()).equals(Arrays.toString(encryptedFile))) {
                            log.info("Protected File has been encrypted............");
                        }
                    }
                    attachment.setPurchaseOrder(purchaseOrder);
                }
            } else {
                purchaseOrder.setPurchaseOrderAttachmentList(null);
            }


            if (purchaseOrder.getPurchaseOrderVehicleInfoList() != null && !purchaseOrder.getPurchaseOrderVehicleInfoList().isEmpty()) {
                for (PurchaseOrderVehicleInfo purchaseOrderVehicleInfo : purchaseOrder.getPurchaseOrderVehicleInfoList()) {
                    purchaseOrderVehicleInfo.setPurchaseOrder(purchaseOrder);
                }
            } else {
                purchaseOrder.setPurchaseOrderVehicleInfoList(null);
            }

            if (purchaseOrder.getPurchaseOrderRemarkFollowUpList() != null && !purchaseOrder.getPurchaseOrderRemarkFollowUpList().isEmpty()) {
                for (PurchaseOrderRemarkFollowUp remarkAndFollowUp : purchaseOrder.getPurchaseOrderRemarkFollowUpList()) {
                    remarkAndFollowUp.setPurchaseOrder(purchaseOrder);
                }
            } else {
                purchaseOrder.setPurchaseOrderRemarkFollowUpList(null);
            }


        } catch (RestException re) {
            log.error("Exception On Save Purcahse Order in Service ->", re);
            baseDto.setResponseCode(re.getMessage());
            exceptioned = true;
        } catch (Exception exception) {
            log.error("Exception in ShipmentService -> create method ", exception);
            baseDto.setResponseCode(ErrorCode.FAILED);
            exceptioned = true;
        }
        if (!exceptioned) {
            log.info("I am going to save Purchase Order");

            baseDto = savePurchaseOrder(purchaseOrder, baseDto);
        }
        return appUtil.setDesc(baseDto);
    }


    public BaseDto update(PurchaseOrder purchaseOrder) {

        String KEY = appUtil.getLocationConfig("shipment.attach.file.encrypt.key", purchaseOrder.getLocationMaster(), false);

        log.info("PurchaseOrderService -> create method is called....");

        BaseDto baseDto = new BaseDto();
        boolean exceptioned = false;
        try {

            purchaseOrderValidator.validate(purchaseOrder);


			/*Order Item Starts Here*/
            if (purchaseOrder.getPurchaseOrderItemList() != null && !purchaseOrder.getPurchaseOrderItemList().isEmpty()) {

                for (PurchaseOrderItem orderItem : purchaseOrder.getPurchaseOrderItemList()) {
                    orderItem.setPurchaseOrder(purchaseOrder);
                }
            } else {
                purchaseOrder.setPurchaseOrderItemList(new ArrayList<>());
            }
            /*Order Item Ends Here*/
			
			
			/*Attachment Starts Here*/
            if (purchaseOrder.getPurchaseOrderAttachmentList() != null && !purchaseOrder.getPurchaseOrderAttachmentList().isEmpty()) {
                for (PurchaseOrderAttachment attachment : purchaseOrder.getPurchaseOrderAttachmentList()) {

                    attachment.setPurchaseOrder(purchaseOrder);

                    if (attachment.getId() != null) {
                        PurchaseOrderAttachment a = purchaseOrderAttachmentRepository.getOne(attachment.getId());
                        log.info("Attament is Swapped ? " + attachment.isSwapped());
                        if (attachment.isSwapped()) {
                            byte[] tempUnProtectedFile = attachment.getUnprotectedFile();
                            byte[] tempProtectedFile = attachment.getProtectedFile();
                            if (tempProtectedFile == null && a.getProtectedFile() != null) {
                                attachment.setUnprotectedFile(CryptoUtils.decrypt(KEY, a.getProtectedFile()));
                            } else if (tempProtectedFile != null) {
                                attachment.setProtectedFile(CryptoUtils.encrypt(KEY, tempProtectedFile));
                            }

                            if (tempUnProtectedFile == null && a.getUnprotectedFile() != null) {
                                attachment.setProtectedFile(CryptoUtils.encrypt(KEY, a.getUnprotectedFile()));
                            } else if (tempUnProtectedFile != null) {
                                attachment.setUnprotectedFile(CryptoUtils.encrypt(KEY, tempUnProtectedFile));
                            }
                        } else {
                            if (attachment.getProtectedFile() == null && a.getProtectedFile() != null) {
                                attachment.setProtectedFile(a.getProtectedFile());
                            } else if (attachment.getProtectedFile() != null) {
                                attachment.setProtectedFile(CryptoUtils.encrypt(KEY, attachment.getProtectedFile()));
                            }
                            if (attachment.getUnprotectedFile() == null && a.getUnprotectedFile() != null) {
                                attachment.setUnprotectedFile(a.getUnprotectedFile());
                            }
                        }
                    } else {
                        log.info("New Attachment While Updating......");
                        if (attachment.getProtectedFile() != null) {
                            log.info("Protected File Encryption......");
                            attachment.setProtectedFile(CryptoUtils.encrypt(KEY, attachment.getProtectedFile()));
                        }
                    }
                }
            } else {
                purchaseOrder.setPurchaseOrderAttachmentList(new ArrayList<>());
            }
			/*Attachment Ends Here*/
			
			/*Vehicle Info Starts Here*/
            if (purchaseOrder.getPurchaseOrderVehicleInfoList() != null && !purchaseOrder.getPurchaseOrderVehicleInfoList().isEmpty()) {

                for (PurchaseOrderVehicleInfo vehicleInfo : purchaseOrder.getPurchaseOrderVehicleInfoList()) {

                    vehicleInfo.setPurchaseOrder(purchaseOrder);
                }
            } else {
                purchaseOrder.setPurchaseOrderVehicleInfoList(new ArrayList<>());
            }
			/*Vehicle Info Ends Here*/
			
			/*OrderRemark And FollowUp Info Starts Here*/
            if (purchaseOrder.getPurchaseOrderRemarkFollowUpList() != null && !purchaseOrder.getPurchaseOrderRemarkFollowUpList().isEmpty()) {

                for (PurchaseOrderRemarkFollowUp remarkAndFollowUp : purchaseOrder.getPurchaseOrderRemarkFollowUpList()) {

                    remarkAndFollowUp.setPurchaseOrder(purchaseOrder);

                }
            } else {
                purchaseOrder.setPurchaseOrderRemarkFollowUpList(new ArrayList<>());
            }
			/*OrderRemark And FollowUp Info Ends Here*/
        } catch (RestException re) {
            log.error("Exception On Save Purcahse Order in Service ->", re);
            baseDto.setResponseCode(re.getMessage());
            exceptioned = true;
        } catch (Exception exception) {
            log.error("Exception in ShipmentService -> create method ", exception);
            baseDto.setResponseCode(ErrorCode.FAILED);
            exceptioned = true;
        }
        if (!exceptioned) {
            log.info("I am going to save Purchase Order");
            baseDto = savePurchaseOrder(purchaseOrder, baseDto);
        }
        return appUtil.setDesc(baseDto);
    }


    //getById
    public BaseDto getById(Long id) {

        log.info("PurchaseOrder Service -> getById method is called....[" + id + "]");

        BaseDto baseDto = new BaseDto();

        try {

            PurchaseOrder purchaseOrder = purchaseOrderRepository.findById(id);

            if (purchaseOrder != null && purchaseOrder.getPurchaseOrderAttachmentList() != null && purchaseOrder.getPurchaseOrderAttachmentList().size() != 0) {
                for (PurchaseOrderAttachment attachment : purchaseOrder.getPurchaseOrderAttachmentList()) {
                    attachment.setProtectedFile(null);
                    attachment.setUnprotectedFile(null);
                }
            }

            baseDto.setResponseCode(ErrorCode.SUCCESS);
            baseDto.setResponseObject(purchaseOrder);

        } catch (RestException exception) {
            log.error("Exception in PurchaseOrder Service -> getById method ", exception);
            baseDto.setResponseCode(exception.getMessage());
        } catch (Exception exception) {
            log.error("Exception in PurchaseOrder Service -> getById method ", exception);
            baseDto.setResponseCode(ErrorCode.FAILED);
        }
        return appUtil.setDesc(baseDto);
    }

    //getByPoNo
    public BaseDto getByPoNo(String poNo) {

        log.info("PurchaseOrder Service -> getBypoNo method is called....[" + poNo + "]");

        BaseDto baseDto = new BaseDto();

        try {

            PurchaseOrder purchaseOrder = purchaseOrderRepository.findByPONo(poNo);

            if (purchaseOrder != null && purchaseOrder.getPurchaseOrderAttachmentList() != null && purchaseOrder.getPurchaseOrderAttachmentList().size() != 0) {
                for (PurchaseOrderAttachment attachment : purchaseOrder.getPurchaseOrderAttachmentList()) {
                    attachment.setProtectedFile(null);
                    attachment.setUnprotectedFile(null);
                }
            }
            baseDto.setResponseCode(ErrorCode.SUCCESS);
            baseDto.setResponseObject(purchaseOrder);
        } catch (RestException exception) {
            log.error("Exception in PurchaseOrder Service -> getById method ", exception);
            baseDto.setResponseCode(exception.getMessage());
        } catch (Exception exception) {
            log.error("Exception in PurchaseOrder Service -> getById method ", exception);
            baseDto.setResponseCode(ErrorCode.FAILED);
        }
        return appUtil.setDesc(baseDto);
    }


    private BaseDto savePurchaseOrder(PurchaseOrder purchaseOrder, BaseDto baseDto) {
        try {
            if (purchaseOrder.getId() == null) {
                purchaseOrder.setPoNo(sequenceGeneratorService.getSequenceValueByType(SequenceType.PURCHASE_ORDER));
            }
            purchaseOrder = purchaseOrderRepository.save(purchaseOrder);
            baseDto.setResponseCode(ErrorCode.SUCCESS);
            baseDto.setResponseObject(purchaseOrder);
        } catch (Exception exception) {
            log.error("Exception in Saving Purchase Order -", exception);

            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(exception);
            baseDto.setResponseCode(ErrorCode.FAILED);

            if (exceptionCause1.contains(UniqueKey.UK_PURCHASE_ORDER_NO)) {
                baseDto.setResponseCode(ErrorCode.PURCHASE_UK_PURCHASE_ORDER_NO);
            } else if (exceptionCause1.contains(UniqueKey.FK_PURCHASE_ORDER_BUYER)) {
                baseDto.setResponseCode(ErrorCode.PURCHASE_FK_PURCHASE_ORDER_BUYER);
            } else if (exceptionCause1.contains(UniqueKey.FK_PURCHASE_ORDER_SUPPLIER)) {
                baseDto.setResponseCode(ErrorCode.PURCHASE_FK_PURCHASE_ORDER_SUPPLIER);
            } else if (exceptionCause1.contains(UniqueKey.FK_PURCHASE_ORDER_ORIGIN)) {
                baseDto.setResponseCode(ErrorCode.PURCHASE_FK_PURCHASE_ORDER_ORIGIN);
            } else if (exceptionCause1.contains(UniqueKey.FK_PURCHASE_ORDER_DESTINATION)) {
                baseDto.setResponseCode(ErrorCode.PURCHASE_FK_PURCHASE_ORDER_DESTINATION);
            } else if (exceptionCause1.contains(UniqueKey.FK_PURCHASE_ORDER_ORIGIN_AGENT)) {
                baseDto.setResponseCode(ErrorCode.PURCHASE_FK_PURCHASE_ORDER_ORIGIN_AGENT);
            } else if (exceptionCause1.contains(UniqueKey.FK_PURCHASE_ORDER_DEST_AGENT)) {
                baseDto.setResponseCode(ErrorCode.PURCHASE_FK_PURCHASE_ORDER_DEST_AGENT);
            } else if (exceptionCause1.contains(UniqueKey.FK_PURCHASE_ORDER_TOS)) {
                baseDto.setResponseCode(ErrorCode.PURCHASE_FK_PURCHASE_ORDER_TOS);
            } else if (exceptionCause1.contains("consol_or_direct")) {
                baseDto.setResponseCode(ErrorCode.PO_DIRECT_OR_CONSOL_REQUIRED);
            } else if (exceptionCause1.contains("supplier_code")) {
                baseDto.setResponseCode(ErrorCode.PO_SUPPLIER);
            } else if (exceptionCause1.contains("buyer_code")) {
                baseDto.setResponseCode(ErrorCode.PO_BUYER);
            } else if (exceptionCause1.contains("po_no")) {
                baseDto.setResponseCode(ErrorCode.PO_NUMBER);
            } else if (exceptionCause1.contains("po_date")) {
                baseDto.setResponseCode(ErrorCode.PO_DATE);
            } else if (exceptionCause1.contains("status")) {
                baseDto.setResponseCode(ErrorCode.PO_STATUS);
            } else if (exceptionCause1.contains("transport_mode")) {
                baseDto.setResponseCode(ErrorCode.PO_TRANSPORT_MODE_REQUIRED);
            } else if (exceptionCause1.contains(UniqueKey.UK_PO_ATTACHMENT_REF)) {
                baseDto.setResponseCode(ErrorCode.PO_UK_PO_ATTACHMENT_REF);
            } else if (exceptionCause1.contains(UniqueKey.FK_PO_ATTACHMENT_PO)) {
                baseDto.setResponseCode(ErrorCode.PO_FK_PO_ATTACHMENT_PO);
            } else if (exceptionCause1.contains(UniqueKey.FK_PO_ITEM_PO)) {
                baseDto.setResponseCode(ErrorCode.PO_FK_PO_ITEM_PO);
            } else if (exceptionCause1.contains(UniqueKey.FK_PO_REMARK_PO)) {
                baseDto.setResponseCode(ErrorCode.PO_FK_PO_REMARK_PO);
            } else if (exceptionCause1.contains(UniqueKey.FK_PO_VEHICLE_INFO_PO)) {
                baseDto.setResponseCode(ErrorCode.PO_FK_PO_VEHICLE_INFO_PO);
            } else if (exceptionCause1.contains("company_id")) {
                baseDto.setResponseCode(ErrorCode.PO_FK_PO_COMPANY);
            } else if (exceptionCause1.contains("location_id")) {
                baseDto.setResponseCode(ErrorCode.PO_FK_PO_LOCATION);
            }


        }
        return baseDto;
    }


    public BaseDto searchByCriteria(PurchaseOrderSearchDto searchData) {

        log.info("DocumentIssueRestrictionService -> searchByCriteria method is called....");

        Map<String, Object> params = new HashMap<>();
        BaseDto baseDto = new BaseDto();

        try {
            UserProfile loggedInUser = AuthService.getCurrentUser();
            CompanyMaster selectedCompany = loggedInUser.getSelectedCompany();
            String hql = " where po.companyMaster.id=" + selectedCompany.getId() + " and po.locationMaster.id=" + loggedInUser.getSelectedUserLocation().getId();
            if (searchData.getBuyerName() != null && searchData.getBuyerName().trim().length() != 0) {
                hql = hql + " and UPPER(po.buyer.bcName) like '" + searchData.getBuyerName().trim().toUpperCase() + "%' ";
            }
            if (searchData.getSupplierName() != null && searchData.getSupplierName().trim().length() != 0) {
                hql = hql + " and UPPER(po.supplier.partyName) like '" + searchData.getSupplierName().trim().toUpperCase() + "%' ";
            }
            if (searchData.getPoNo() != null && searchData.getPoNo().trim().length() != 0) {
                hql = hql + " and UPPER(po.poNo) like '" + searchData.getPoNo().trim().toUpperCase() + "%' ";
            }
            if (searchData.getOrigin() != null && searchData.getOrigin().trim().length() != 0) {
                hql = hql + " and UPPER(po.origin.portName) like '" + searchData.getOrigin().trim().toUpperCase() + "%' ";
            }
            if (searchData.getDestination() != null && searchData.getDestination().trim().length() != 0) {
                hql = hql + " and UPPER(po.destination.portName) like '" + searchData.getDestination().trim().toUpperCase() + "%' ";
            }
            if (searchData.getTransportMode() != null) {
                hql = hql + " and po.transportMode = '" + searchData.getTransportMode() + "' ";
            }
            if (searchData.getStatus() != null) {
                hql = hql + " and po.status = '" + searchData.getStatus() + "' ";
            }
            if (searchData.getTos() != null && searchData.getTos().trim().length() != 0) {
                hql = hql + " and UPPER(po.tosMaster.tosName) like '" + searchData.getTos().trim().toUpperCase() + "%' ";
            }


            if (searchData.getPoDate() != null && searchData.getPoDate().getStartDate() != null && searchData.getPoDate().getEndDate() != null) {

                try {
                    Date dt1 = sdf.parse(searchData.getPoDate().getStartDate());
                    Date dt2 = sdf.parse(searchData.getPoDate().getEndDate());

                    if (hql.trim().length() != 0) {
                        hql = hql + " AND ";
                    }

                    params.put("fromStartDate", dt1);
                    params.put("fromEndDate", dt2);
                    hql = hql + " ( po.poDate >= :fromStartDate AND po.poDate <= :fromEndDate ) ";

                } catch (ParseException e1) {
                    log.error("Date Conversion error in shipmentReqDate: ", e1);
                }


            }
            if (searchData.getSortByColumn() != null && searchData.getOrderByType() != null && searchData.getSortByColumn().trim().length() != 0
                    && searchData.getOrderByType().trim().length() != 0) {
                hql = hql + " order by " + searchData.getSortByColumn().trim() + " " + searchData.getOrderByType().trim();
            } else {
                hql = hql + " order by po.id DESC";
            }
			/* DateRange poDate; String sortByColumn; String orderByType; */
            log.info("Search HQL : [" + hql + "]");
            Query query = entityManager.createQuery(StaticSearchQuery.PurchaseOrderSearch + hql);
            for (Entry<String, Object> e : params.entrySet()) {
                query.setParameter(e.getKey(), e.getValue());
            }
            query.setFirstResult(searchData.getSelectedPageNumber() * searchData.getRecordPerPage());
            query.setMaxResults(searchData.getRecordPerPage());


            String countQ = "Select count (po.id) from PurchaseOrder po  " + hql;
            Query countQuery = entityManager.createQuery(countQ);
            for (Entry<String, Object> e : params.entrySet()) {
                countQuery.setParameter(e.getKey(), e.getValue());
            }
            Long countTotal = (Long) countQuery.getSingleResult();

            SearchRespDto searchRespDto = new SearchRespDto(countTotal, query.getResultList());
            searchRespDto.setRecordPerPage(searchData.getRecordPerPage());
            searchRespDto.setSelectedPageNumber(searchData.getSelectedPageNumber());
            baseDto.setResponseCode(ErrorCode.SUCCESS);
            baseDto.setResponseObject(searchRespDto);

        } catch (RestException re) {
            log.error("Exception in DocumentIssueRestrictionService -> searchById method ", re);
            baseDto.setResponseCode(re.getMessage());
        } catch (Exception e) {
            log.error("Exception in DocumentIssueRestrictionService -> searchById method ", e);
            baseDto.setResponseCode(ErrorCode.FAILED);
        }
        return appUtil.setDesc(baseDto);
    }


    public void downloadAttachment(Long attachmentId, String type, boolean swapped, HttpServletResponse response) {

        PurchaseOrderAttachment attachment = purchaseOrderAttachmentRepository.findOne(attachmentId);

        String key = appUtil.getLocationConfig("shipment.attach.file.encrypt.key", attachment.getPurchaseOrder().getLocationMaster(), false);

        if (swapped) {
            if (type.equals(PROTECTED_FILE_NAME)) {
                type = UNPROTECTED_FILE_NAME;
            } else {
                type = PROTECTED_FILE_NAME;
            }
        }

        if (PROTECTED_FILE_NAME.equals(type)) {

            log.info("It's a protected File............");

            if (attachment.getProtectedFile() != null && attachment.getProtectedFileName() != null) {
                try {
                    log.info("Decrypting the protected file...............");

                    // Decrypting the protected file..
                    byte[] outputBytes = CryptoUtils.decrypt(key, attachment.getProtectedFile());
                    log.info("Decrypted File length :  " + outputBytes.length);

                    response.getOutputStream().write(outputBytes);

                    response.setContentType(attachment.getProtectedFileContentType());

                    response.setContentLength(outputBytes.length);

                    response.setHeader("Content-Disposition", "attachment; filename=" + attachment.getProtectedFileName());

                    log.info("Decrypting the protected file finished...............");

                    response.getOutputStream().flush();

                    response.getOutputStream().close();

                    log.info("Successfully downloading protectedFile....");

                } catch (Exception exception) {

                    log.error("Error occur while downloading protected attachment file : " + attachmentId + ", ", exception);

                }
            }
        } else if (UNPROTECTED_FILE_NAME.equals(type)) {

            log.info("It's a unprotected File............");

            if (attachment.getUnprotectedFile() != null && attachment.getUnprotectedFileName() != null) {

                log.info("UnprotectedFile : " + attachment.getUnprotectedFile());

                log.info("UnprotectedFileName : " + attachment.getUnprotectedFileName());

                try {

                    response.setContentType(attachment.getUnprotectedFileContentType());

                    response.setContentLength(attachment.getUnprotectedFile().length);

                    response.setHeader("Content-Disposition", "attachment; filename=" + attachment.getUnprotectedFileName());

                    response.getOutputStream().write(attachment.getUnprotectedFile());

                    response.getOutputStream().flush();

                    response.getOutputStream().close();

                    log.info("Successfully downloading unprotectedFile....");

                } catch (Exception exception) {
                    log.error("Error occur while downloading unprotected attachment file : " + attachmentId + ", ", exception);
                }
            }
        }

    }


}
