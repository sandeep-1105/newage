package com.efreightsuite.service;

import java.util.List;

import com.efreightsuite.common.component.CacheRepository;
import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.SurchargeMappingSearchDto;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.ExceptionHelper;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.exception.UniqueKey;
import com.efreightsuite.model.AgentPortMaster;
import com.efreightsuite.model.SurchargeMapping;
import com.efreightsuite.repository.SurchargeMappingRepository;
import com.efreightsuite.search.SurchargeMappingSearchImpl;
import com.efreightsuite.util.AppUtil;
import com.efreightsuite.util.SaaSUtil;
import com.efreightsuite.util.ValidateUtil;
import com.efreightsuite.validation.SurchargeMappingValidator;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.exception.ExceptionUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

/**
 * @author tanveer Date - 07.03.2016 Updated - 11.03.2016
 */

@Service
@Log4j2
public class SurchargeMappingService {

    @Autowired
    private
    AppUtil appUtil;

    /**
     * SurchargeMappingRepository Object
     */

    @Autowired
    private
    SurchargeMappingRepository surchargeMappingRepository;

    @Autowired
    private
    SurchargeMappingValidator surchargeMappingValidator;

    @Autowired
    private
    CacheRepository cacheRepository;

    @Autowired
    private
    SurchargeMappingSearchImpl surchargeMappingSearchImpl;

    /**
     * Method - getByPackId Returns SurchargeMapping based on given pack id
     *
     * @return BaseDto
     */

    public BaseDto getById(Long id) {

        log.info("getByPackId method is called...[" + id + "]");

        final BaseDto baseDto = new BaseDto();

        try {

            SurchargeMapping surchargeMapping = cacheRepository.get(SaaSUtil.getSaaSId(), SurchargeMapping.class.getName(), id,
                    surchargeMappingRepository);

            ValidateUtil.notNull(surchargeMapping, ErrorCode.SURCHARGE_MAPPING_ID_NOT_FOUND);

            log.info("getByPackId is successfully executed....[" + surchargeMapping.getId() + "]");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(surchargeMapping);

        } catch (final RestException exception) {

            log.error("Exception in getByPackId method : ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (final Exception exception) {

            log.error("Exception in getByPackId method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);
    }


    /**
     * Method - search Returns list of SurchargeMapping based on the search keyword
     *
     * @return BaseDto
     */

    public BaseDto search(SurchargeMappingSearchDto searchDto) {

        log.info("Search method is called...." + searchDto);

        BaseDto baseDto = new BaseDto();

        try {

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(surchargeMappingSearchImpl.search(searchDto));

            log.info("Successfully Searching Surcharge Mapping...");

        } catch (Exception exception) {

            log.error("Exception in Search method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);

    }

    /**
     * Method - create Creates and returns a new surcharge mapping
     *
     * @returns SurchargeMapping
     */

    public BaseDto create(List<SurchargeMapping> surchargeMappingList) {

        log.info("create method is Invoked.....");

        BaseDto baseDto = new BaseDto();

        try {

            for (SurchargeMapping surchargeMapping : surchargeMappingList) {
                surchargeMappingValidator.validate(surchargeMapping);
            }

            surchargeMappingRepository.save(surchargeMappingList);

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(surchargeMappingList);

        } catch (RestException exception) {

            log.error("Rest Exception occured with error code ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (Exception exception) {

            log.error("Exception occured ", exception);

            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Exception Cause 1 : " + exceptionCause1);

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            if (exceptionCause1.contains(UniqueKey.UK_SURCHARGE_CHARGEID)) {
                baseDto.setResponseCode(ErrorCode.SURCHARGE_MAPPING_ALREADY_EXIST);
            }

            if (exceptionCause1.contains(UniqueKey.UK_SURCHARGE_SURCHARGE)) {
                baseDto.setResponseCode(ErrorCode.SURCHARGE_MAPPING_ALREADY_EXIST);
            }

        }

        return appUtil.setDesc(baseDto);
    }

    /**
     * Method - update Updates and returns a updated surcharge mapping
     *
     * @param existingSurchargeMapping
     * @returns SurchargeMapping
     */

    public BaseDto update(SurchargeMapping existingSurchargeMapping) {

        log.info("update method is Invoked....[ " + existingSurchargeMapping.getId() + "]");

        BaseDto baseDto = new BaseDto();

        try {

            if (existingSurchargeMapping.getId() != null) {
                surchargeMappingRepository.getOne(existingSurchargeMapping.getId());
            }


            surchargeMappingValidator.validate(existingSurchargeMapping);

            surchargeMappingRepository.save(existingSurchargeMapping);

            cacheRepository.remove(SaaSUtil.getSaaSId(), AgentPortMaster.class.getName(), existingSurchargeMapping.getId());

            log.info("Surcharge Mapping Successfully Updated...[" + existingSurchargeMapping.getId() + "]");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(existingSurchargeMapping);

        } catch (RestException exception) {

            log.error("Rest Exception occured with error code ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (Exception exception) {

            log.error("Exception occured ", exception);

            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Exception Cause 1 : " + exceptionCause1);

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            if (exceptionCause1.contains(UniqueKey.UK_SURCHARGE_CHARGEID)) {
                baseDto.setResponseCode(ErrorCode.SURCHARGE_MAPPING_ALREADY_EXIST);
            }

            if (exceptionCause1.contains(UniqueKey.UK_SURCHARGE_SURCHARGE)) {
                baseDto.setResponseCode(ErrorCode.SURCHARGE_MAPPING_ALREADY_EXIST);
            }

        }

        return appUtil.setDesc(baseDto);
    }

    /**
     * Deletes the Existing SurchargeMapping Based on the Given Id
     *
     * @Returns BaseDto
     */
    public BaseDto delete(Long id) {

        log.info("Delete method is Invoked...[" + id + "]");

        final BaseDto baseDto = new BaseDto();

        try {

            cacheRepository.delete(SaaSUtil.getSaaSId(), SurchargeMapping.class.getName(), id, surchargeMappingRepository);

            log.info("SurchargeMapping Deleted Successfully....[" + id + "]");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

        } catch (RestException exception) {

            log.error("Exception in Delete method : ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (DataIntegrityViolationException exception) {

            log.error("Exception occured : ", exception);

            ExceptionHelper helper = new ExceptionHelper(exception);

            log.error("Foreign key reference : " + helper.getFkReference());

            baseDto.setResponseCode(ErrorCode.ERROR_FK_CONSTRAINT);

        } catch (Exception exception) {

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            log.error("Exception in Delete method : ", exception);

            String rootCause = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Root Cause of Exception : " + rootCause);

            if (rootCause.contains("FK_")) {
                baseDto.setResponseCode(ErrorCode.CANNOT_DELETE_FK_RECORD);
            }
        }

        return appUtil.setDesc(baseDto);
    }

}
