package com.efreightsuite.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.model.common.CommonErrorMessage;
import com.efreightsuite.repository.common.CommonErrorMessageRepository;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class CommonErrorMessageService {

    @Autowired
    private
    CommonErrorMessageRepository commonErrorMessageRepository;

    public BaseDto findAll() {

        BaseDto baseDto = new BaseDto();
        Map<String, String> errorMap = new HashMap<>();
        try {
            List<CommonErrorMessage> listOfCommonErrorMessage = commonErrorMessageRepository.findAll();
            if (listOfCommonErrorMessage != null) {
                log.info("Successfully getting list of error messages " + listOfCommonErrorMessage.size());


                for (CommonErrorMessage cem : listOfCommonErrorMessage) {
                    errorMap.put(cem.getErrorCode(), cem.getErrorDesc());
                }
            }
            baseDto.setResponseCode(ErrorCode.SUCCESS);
            baseDto.setResponseObject(errorMap);
        } catch (Exception exception) {
            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return baseDto;
    }

}
