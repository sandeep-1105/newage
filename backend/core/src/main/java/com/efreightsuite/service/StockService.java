package com.efreightsuite.service;

import java.util.List;

import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.StockEnquiryDto;
import com.efreightsuite.dto.StockGenerationSearchDto;
import com.efreightsuite.enumeration.StockStatus;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.exception.UniqueKey;
import com.efreightsuite.model.StockGeneration;
import com.efreightsuite.repository.StockRepository;
import com.efreightsuite.search.StockSearchImpl;
import com.efreightsuite.util.AppUtil;
import com.efreightsuite.util.ValidateUtil;
import com.efreightsuite.validation.StockValidator;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.exception.ExceptionUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.ObjectOptimisticLockingFailureException;
import org.springframework.orm.jpa.JpaObjectRetrievalFailureException;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class StockService {


    @Autowired
    private
    StockValidator stockValidator;


    @Autowired
    private
    AppUtil appUtil;

    @Autowired
    SequenceGeneratorService sequenceGeneratorService;

    @Autowired
    private
    StockGenerationService stockGenerationService;

    @Autowired
    private
    StockSearchImpl stockSearchImpl;

    @Autowired
    private
    StockRepository stockRepository;

    //Stock Generation Method

    public BaseDto generate(StockGeneration stockGeneration) {
        BaseDto baseDto = new BaseDto();

        try {
            stockValidator.validateStock(stockGeneration);
            baseDto.setResponseObject(stockGenerationService.generateStock(stockGeneration));
            baseDto.setResponseCode(ErrorCode.SUCCESS);
            log.info("Stock Generation generated successfully");
        } catch (RestException re) {
            log.error("Exception Cause  ::: ", re);
            baseDto.setResponseCode(re.getMessage());

        } catch (Exception e) {
            log.error("Exception ::: ", e);

            String exceptionCause = ExceptionUtils.getRootCauseMessage(e);

            log.error("Exception Cause  ::: " + exceptionCause);

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            if (exceptionCause.contains(UniqueKey.UK_STOCK_UNIQUE)) {
                baseDto.setResponseCode(ErrorCode.STOCK_ALREADY_CREATED);
            }
            if (exceptionCause.contains(UniqueKey.UK_STOCK_MAWB_UNIQUE)) {
                baseDto.setResponseCode(ErrorCode.STOCK_MAWB_ALREADY_CREATED);
            }

        }

        return appUtil.setDesc(baseDto);
    }


    public BaseDto update(StockGeneration stockGeneration) {

        log.info("update method is called....");

        BaseDto baseDto = new BaseDto();

        try {

            ValidateUtil.notNull(stockGeneration.getId(), ErrorCode.STOCK_GENERATION_MANDATORY);

            if (stockGeneration.getStatusChange().equals("Available")) {
                stockGeneration.setStockStatus(StockStatus.Available);
                stockGeneration.setServiceCode(null);
                stockGeneration.setShipment(null);
                stockGeneration.setShipmentUid(null);
            }
            if (stockGeneration.getStatusChange().equals("Cancelled")) {
                stockGeneration.setStockStatus(StockStatus.Cancelled);
                stockGeneration.setServiceCode(null);
                stockGeneration.setShipment(null);
                stockGeneration.setShipmentUid(null);
            }
            stockGeneration = stockRepository.save(stockGeneration);
            baseDto.setResponseCode(ErrorCode.SUCCESS);
            baseDto.setResponseObject(stockGeneration);

        } catch (RestException exception) {

            log.error("RestException in Create method : ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (ObjectOptimisticLockingFailureException e) {


            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_LOCKED_RECORD);

        } catch (JpaObjectRetrievalFailureException e) {

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_DELETED_RECORD);

        } catch (Exception e) {
            log.error("Exception ::: " + e);

            String exceptionCause = ExceptionUtils.getRootCauseMessage(e);

            log.error("Exception Cause  ::: " + exceptionCause);

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

        }
        return appUtil.setDesc(baseDto);
    }


    public BaseDto get(Long carrierId, Long porId) {
        BaseDto baseDto = new BaseDto();
        try {
            List<StockGeneration> stocklist = stockRepository.getStock(carrierId, porId);
            baseDto.setResponseCode(ErrorCode.SUCCESS);
            baseDto.setResponseObject(stocklist);
        } catch (Exception e) {
            baseDto.setResponseCode(e.getMessage());
        }
        return appUtil.setDesc(baseDto);
    }


    public BaseDto search(StockGenerationSearchDto searchDto) {

        log.info("Stock service -> search method is called....");

        BaseDto baseDto = new BaseDto();

        try {

            baseDto.setResponseCode(ErrorCode.SUCCESS);
            baseDto.setResponseObject(stockSearchImpl.search(searchDto));

        } catch (RestException re) {
            log.info("exception message", re);
            baseDto.setResponseCode(re.getMessage());

        } catch (Exception e) {
            log.info("exception message", e);
            baseDto.setResponseCode(ErrorCode.FAILED);

        }
        return appUtil.setDesc(baseDto);
    }

    public BaseDto enquirySearch(StockEnquiryDto dto) {

        log.info("enquirySearch -> search method is called....");

        BaseDto baseDto = new BaseDto();

        try {

            baseDto.setResponseCode(ErrorCode.SUCCESS);
            baseDto.setResponseObject(stockSearchImpl.stockEnquiry(dto));

        } catch (RestException re) {
            log.info("exception message", re);
            baseDto.setResponseCode(re.getMessage());

        } catch (Exception e) {
            log.info("exception message", e);
            baseDto.setResponseCode(ErrorCode.FAILED);

        }
        return appUtil.setDesc(baseDto);
    }


    public BaseDto getMawb(Long carrierId, Long porid) {

        log.info("Stock service -> search method is called....");

        BaseDto baseDto = new BaseDto();

        try {
            baseDto.setResponseCode(ErrorCode.SUCCESS);
            baseDto.setResponseObject(stockSearchImpl.getMawb(carrierId, porid));

        } catch (RestException re) {
            log.info("exception message", re);
            baseDto.setResponseCode(re.getMessage());

        } catch (Exception e) {
            log.info("exception message", e);
            baseDto.setResponseCode(ErrorCode.FAILED);

        }
        return appUtil.setDesc(baseDto);
    }


}
