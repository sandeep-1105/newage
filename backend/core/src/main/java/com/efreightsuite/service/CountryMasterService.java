package com.efreightsuite.service;

import javax.xml.bind.DatatypeConverter;

import com.efreightsuite.common.component.CacheRepository;
import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.CountryMasterSearchDto;
import com.efreightsuite.dto.SearchRequest;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.ExceptionHelper;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.model.CountryMaster;
import com.efreightsuite.model.ZoneMaster;
import com.efreightsuite.repository.CountryMasterRepository;
import com.efreightsuite.search.CommonSearchImpl;
import com.efreightsuite.search.CountrySearchService;
import com.efreightsuite.util.AppUtil;
import com.efreightsuite.util.SaaSUtil;
import com.efreightsuite.util.ValidateUtil;
import com.efreightsuite.validation.CountryMasterValidator;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.exception.ExceptionUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;


@Service
@Log4j2
public class CountryMasterService {

    @Autowired
    private
    AppUtil appUtil;

    @Autowired
    private
    CountryMasterRepository countryMasterRepository;

    @Autowired
    private
    CountryMasterValidator countryMasterValidator;

    @Autowired
    private
    CacheRepository cacheRepository;

    @Autowired
    private
    CountrySearchService countrySearchService;

    @Autowired
    private
    CommonSearchImpl commonSearchImpl;

    /*
     * Returns all the Country object
     *
     * @Returns BaseDto
     */
    public BaseDto search(CountryMasterSearchDto searchDto) {

        log.info("getAll method is called....");

        BaseDto baseDto = new BaseDto();

        try {

            log.info("Successfully getting list of country...");
            baseDto.setResponseCode(ErrorCode.SUCCESS);
            baseDto.setResponseObject(countrySearchService.search(searchDto));

        } catch (RestException re) {
            log.error("Exception in getAll method ", re);
            baseDto.setResponseCode(re.getMessage());

        } catch (Exception e) {
            log.error("Exception in getAll method ", e);
            baseDto.setResponseCode(ErrorCode.FAILED);

        }
        return appUtil.setDesc(baseDto);
    }

    /**
     * Method - get
     *
     * @return baseDto
     */

    public BaseDto get(Long id) {

        log.info("get method is called [" + id + "]");

        BaseDto baseDto = new BaseDto();

        try {

            ValidateUtil.notNull(id, ErrorCode.COUNTRY_ID_NOT_NULL);

            CountryMaster countryMaster = cacheRepository.get(SaaSUtil.getSaaSId(), CountryMaster.class.getName(), id, countryMasterRepository);

            ValidateUtil.notNull(countryMaster, ErrorCode.COUNTRY_ID_NOT_FOUND);

            if (countryMaster.getImage() != null) {
                countryMaster.setEncodedImage(DatatypeConverter.printBase64Binary(countryMaster.getImage()));
            }

            countryMaster.setImage(null);

            log.info("Successfully getting Country By Given Id.....[" + countryMaster.getId() + "]");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(countryMaster);

        } catch (RestException exception) {

            log.error("Exception in get method...", exception);


            baseDto.setResponseCode(exception.getMessage());
        } catch (Exception exception) {

            log.error("Exception in get method", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);

    }

	/*
     * create - This method creates a new country
	 *
	 * @Param CountryMaster entity
	 *
	 * @Return BaseDto
	 */

    public BaseDto create(CountryMaster countryMaster) {

        log.info("Create method is called....");

        BaseDto baseDto = new BaseDto();

        try {

            countryMasterValidator.validate(countryMaster);

            countryMaster = cacheRepository.save(SaaSUtil.getSaaSId(), CountryMaster.class.getName(), countryMaster, countryMasterRepository);

            log.info("CountryMaster Saved successfully.....[ " + countryMaster.getId() + "]");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(countryMaster);

        } catch (RestException exception) {

            log.error("CountryMasterService->Exception in Create method : ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (DataIntegrityViolationException exception) {

            log.error("DataIntegrityViolationException occured", exception);

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Exception Cause 1 ::: " + exceptionCause1);

            // General error condition
            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            if (exceptionCause1.contains("UK_COUNTRY_COUNTRYCODE")) {
                log.error("Duplicate Country Code");
                baseDto.setResponseCode(ErrorCode.COUNTRY_CODE_ALREADY_EXIST);
            }
            if (exceptionCause1.contains("UK_COUNTRY_COUNTRYNAME")) {
                log.error("Duplicate Country name");
                baseDto.setResponseCode(ErrorCode.COUNTRY_NAME_ALREADY_EXIST);
            }

        } catch (Exception e) {

            log.error("Failed to save the country..", e);

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);
        }

        return appUtil.setDesc(baseDto);
    }

	/*
	 * update - This method updates the Existing Country
	 *
	 * @Param ExistingCountryMaster entity
	 *
	 * @Return BaseDto
	 */

    public BaseDto update(CountryMaster existingCountryMaster) {

        log.info("update method is called....[" + existingCountryMaster.getId() + "]");

        BaseDto baseDto = new BaseDto();

        try {

            ValidateUtil.notNull(existingCountryMaster.getId(), ErrorCode.COUNTRY_ID_NOT_NULL);

            if (existingCountryMaster.getId() != null) {
                countryMasterRepository.getOne(existingCountryMaster.getId());
            }

            countryMasterValidator.validate(existingCountryMaster);

            countryMasterRepository.save(existingCountryMaster);

            cacheRepository.remove(SaaSUtil.getSaaSId(), ZoneMaster.class.getName(), existingCountryMaster.getId());

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(existingCountryMaster);

        } catch (RestException exception) {

            log.error("Exception in Delete method : ", exception);


            baseDto.setResponseCode(exception.getMessage());

        } catch (DataIntegrityViolationException exception) {

            log.error("DataIntegrityViolationException occured", exception);


            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Exception Cause 1 ::: " + exceptionCause1);

            // General error condition
            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            if (exceptionCause1.contains("UK_COUNTRY_COUNTRYCODE")) {
                log.error("Duplicate Country Code");
                baseDto.setResponseCode(ErrorCode.COUNTRY_CODE_ALREADY_EXIST);

            }
            if (exceptionCause1.contains("UK_COUNTRY_COUNTRYNAME")) {
                log.error("Duplicate Country name");
                baseDto.setResponseCode(ErrorCode.COUNTRY_NAME_ALREADY_EXIST);
            }

        } catch (Exception e) {
            log.error("Failed to save the Country..", e);
            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);
        }

        return appUtil.setDesc(baseDto);
    }

    /*
     * Deletes the Existing Country Based on the Given Id
     *
     * @param Id
     *
     * @Returns BaseDto
     */
    public BaseDto delete(Long id) {

        log.info("Delete method is Invoked...[" + id + "]");

        BaseDto baseDto = new BaseDto();

        try {

            ValidateUtil.notNull(id, ErrorCode.COUNTRY_ID_NOT_NULL);

            cacheRepository.delete(SaaSUtil.getSaaSId(), CountryMaster.class.getName(), id, countryMasterRepository);

            log.info("CountryMaster Deleted Successfully....[" + id + "]");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

        } catch (RestException exception) {

            log.error("Exception in Delete method : ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (DataIntegrityViolationException exception) {

            log.error("Exception occured : ", exception);

            ExceptionHelper helper = new ExceptionHelper(exception);

            log.error("Foreign key reference : " + helper.getFkReference());

            baseDto.setResponseCode(ErrorCode.ERROR_FK_CONSTRAINT);

        } catch (Exception exception) {
            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            log.error("Exception in Delete method : ", exception);

            String rootCause = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Root Cause of Exception : " + rootCause);

            if (rootCause.contains("FK_")) {
                baseDto.setResponseCode(ErrorCode.CANNOT_DELETE_FK_RECORD);
            }
        }

        return appUtil.setDesc(baseDto);
    }


    public BaseDto search(SearchRequest searchRequest) {

        log.info("getAll method is called....");

        BaseDto baseDto = new BaseDto();

        try {

            log.info("Successfully getting list of country...");
            baseDto.setResponseCode(ErrorCode.SUCCESS);
            baseDto.setResponseObject(countrySearchService.search(searchRequest));

        } catch (RestException re) {
            log.error("Exception in getAll method ", re);
            baseDto.setResponseCode(re.getMessage());

        } catch (Exception e) {
            log.error("Exception in getAll method ", e);
            baseDto.setResponseCode(ErrorCode.FAILED);

        }
        return appUtil.setDesc(baseDto);
    }


    public BaseDto searchCountryNotInList(SearchRequest searchRequest) {

        log.info("searchCountryNotInList method is called....");

        BaseDto baseDto = new BaseDto();

        try {

            baseDto.setResponseObject(commonSearchImpl.searchCountryNotInList(searchRequest));

            baseDto.setResponseCode(ErrorCode.SUCCESS);

        } catch (Exception exception) {

            log.error("Exception in searchCountryNotInList method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);
    }

}
