package com.efreightsuite.service;

import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.ProvisionalSearchDto;
import com.efreightsuite.enumeration.ActualChargeable;
import com.efreightsuite.enumeration.UnitType;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.model.ProvisionItem;
import com.efreightsuite.model.Provisional;
import com.efreightsuite.model.ShipmentCharge;
import com.efreightsuite.model.ShipmentServiceDetail;
import com.efreightsuite.repository.DefaultMasterDataRepository;
import com.efreightsuite.repository.ProvisionalRepository;
import com.efreightsuite.repository.ShipmentServiceDetailRepository;
import com.efreightsuite.search.ProvisionalSearchImpl;
import com.efreightsuite.util.AppUtil;
import com.efreightsuite.validation.ProvisionalValidator;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.exception.ExceptionUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.ObjectOptimisticLockingFailureException;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class ProvisionalService {

    @Autowired
    private
    ProvisionalRepository provisionalRepository;

    @Autowired
    private
    ProvisionalValidator provisionalValidator;

    @Autowired
    private
    ProvisionalSearchImpl provisionalSearchImpl;

    @Autowired
    private
    ShipmentServiceDetailRepository shipmentServiceDetailRepository;

    @Autowired
    DefaultMasterDataRepository defaultMasterDataRepository;

    @Autowired
    private
    AppUtil appUtil;

    private void afterGet(Provisional provisional) {
        if (provisional != null && provisional.getProvisionalItemList() != null) {
            for (ProvisionItem item : provisional.getProvisionalItemList()) {
                AppUtil.setService(item.getShipmentService());
                AppUtil.setPartyToNull(item.getBillToParty());
                AppUtil.setPartyToNull(item.getVendorSubledger());
            }
        }

    }

    public BaseDto get(Long id) {
        log.info("get method is called....");

        BaseDto baseDto = new BaseDto();

        try {

            Provisional provisional = provisionalRepository.findOne(id);
            afterGet(provisional);

            log.info("Successfully getting Provisional by id...");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(provisional);

        } catch (RestException exception) {

            log.error("RestException in get method : ", exception);

            baseDto.setResponseCode(exception.getMessage());
        } catch (Exception exception) {

            log.error("Exception in get method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);

        }

        return appUtil.setDesc(baseDto);
    }

    public BaseDto create(Provisional provisional) {

        log.info("saveOrUpdate method is called....");

        BaseDto baseDto = new BaseDto();

        try {

            provisionalValidator.validate(provisional);

			/* Provisional item start */
            for (ProvisionItem provisionalItem : provisional.getProvisionalItemList()) {

                ShipmentServiceDetail shipmentServiceDetail = new ShipmentServiceDetail();
                if (provisionalItem.getShipmentService() != null
                        && provisionalItem.getShipmentService().getId() != null) {
                    shipmentServiceDetail.setId(provisionalItem.getShipmentService().getId());
                } else {
                    shipmentServiceDetail = null;
                }

                provisionalItem.setShipmentService(shipmentServiceDetail);

                provisionalItem.setProvisional(provisional);
            }
            /* Provisional Item end */

            Provisional provisionalRes = provisionalRepository.save(provisional);

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(provisionalRes.getId());

        } catch (RestException re) {

            log.error("Search method ", re);

            baseDto.setResponseCode(re.getMessage());

        } catch (Exception exception) {
            log.error("Exception in ProvisionalService -> update method ", exception);

            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(exception);
            log.error("Exception Cause 1 ::: " + exceptionCause1);

            baseDto.setResponseCode(ErrorCode.FAILED);

        }
        return appUtil.setDesc(baseDto);
    }

    public BaseDto search(ProvisionalSearchDto provisionalSearchDto) {

        log.info("Search method is called " + provisionalSearchDto);

        BaseDto baseDto = new BaseDto();

        try {

            baseDto.setResponseObject(provisionalSearchImpl.search(provisionalSearchDto));

            baseDto.setResponseCode(ErrorCode.SUCCESS);
        } catch (RestException re) {

            log.error("Search method ", re);

            baseDto.setResponseCode(re.getMessage());

        } catch (Exception e) {

            log.error("Search method ", e);

            baseDto.setResponseCode(ErrorCode.FAILED);

        }
        return appUtil.setDesc(baseDto);
    }


    public BaseDto update(Provisional provisional) {

        log.info("update method is called.... : " + provisional.getId());

        BaseDto baseDto = new BaseDto();

        try {

            provisionalValidator.validate(provisional);

            for (ProvisionItem provisionalItem : provisional.getProvisionalItemList()) {

                if (provisionalItem.getId() == null) {
                    provisionalItem.setProvisional(provisional);

                    ShipmentServiceDetail shipmentServiceDetail = new ShipmentServiceDetail();
                    if (provisionalItem.getShipmentService() != null
                            && provisionalItem.getShipmentService().getId() != null) {
                        shipmentServiceDetail.setId(provisionalItem.getShipmentService().getId());
                    } else {
                        shipmentServiceDetail = null;
                    }

                    provisionalItem.setShipmentService(shipmentServiceDetail);

                }
            }

            Provisional provisionalRes = provisionalRepository.save(provisional);

            log.info("Provisional updated successfully...[" + provisionalRes.getId() + "]");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(provisionalRes.getId());

        } catch (RestException exception) {
            log.error("RestException in Create method : ", exception);
            baseDto.setResponseCode(exception.getMessage());
        } catch (ObjectOptimisticLockingFailureException e) {

            log.error("Error in editing Provisional", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_LOCKED_RECORD);

        } catch (Exception e) {
            log.error("Exception in update method ::: ", e);
            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

        }
        return appUtil.setDesc(baseDto);
    }

    public BaseDto getByServiceUid(String serviceUid, boolean onlyGet) {
        log.info("get method is called....");

        BaseDto baseDto = new BaseDto();

        try {

            Provisional provisional = provisionalRepository.findByServiceUid(serviceUid);

            if (provisional == null && !onlyGet) {
                ShipmentServiceDetail sd = shipmentServiceDetailRepository.findByServiceUid(serviceUid);

                if (sd.getConsolUid() != null && sd.getShipmentChargeList() != null && sd.getShipmentChargeList().size() != 0) {

                    provisional = new Provisional();
                    provisional.setMasterUid(sd.getConsolUid());
                    provisional.setShipmentUid(sd.getShipmentUid());
                    provisional.setServiceUid(sd.getServiceUid());
                    provisional.setServiceName(sd.getServiceMaster().getServiceName());
                    provisional.setLocalCurrency(sd.getCountry().getCurrencyMaster());

                    double rAmt = 0.0, cAmt = 0.0;

                    for (ShipmentCharge sc : sd.getShipmentChargeList()) {

                        if (sc.getUnitMaster().getUnitType() == UnitType.Unit && sc.getNumberOfUnit() != null) {
                            if (sc.getNumberOfUnit().equals("+1000") && sd.getBookedChargeableUnit() > 1000) {
                            } else if (sc.getNumberOfUnit().equals("+500")
                                    && sd.getBookedChargeableUnit() > 500 && sd.getBookedChargeableUnit() <= 1000) {
                            } else if (sc.getNumberOfUnit().equals("+300")
                                    && sd.getBookedChargeableUnit() > 100 && sd.getBookedChargeableUnit() <= 300) {
                            } else if (sc.getNumberOfUnit().equals("+250")
                                    && sd.getBookedChargeableUnit() > 100 && sd.getBookedChargeableUnit() <= 250) {
                            } else if (sc.getNumberOfUnit().equals("+45")
                                    && sd.getBookedChargeableUnit() > 0 && sd.getBookedChargeableUnit() <= 45) {
                            } else if (sc.getNumberOfUnit().equals("-45") && sd.getBookedChargeableUnit() <= 0) {
                            } else {
                                continue;
                            }
                        }

                        ProvisionItem pi = new ProvisionItem();
                        pi.setShipmentService(sd);
                        pi.setServiceUid(sd.getServiceUid());
                        pi.setChargeMaster(sc.getChargeMaster());
                        pi.setUnitMaster(sc.getUnitMaster());
                        if (sc.getUnitMaster().getUnitType() == UnitType.Shipment) {
                            pi.setNoOfUnits(1.0);
                        } else if (sc.getUnitMaster().getUnitType() == UnitType.Unit) {
                            if (sc.getActualChargeable() == ActualChargeable.ACTUAL) {
                                pi.setNoOfUnits(sd.getBookedGrossWeightUnitKg());
                            } else {
                                pi.setNoOfUnits(sd.getBookedChargeableUnit());
                            }
                        }

                        String key = appUtil.getLocationConfig("booking.rates.merged", sd.getLocation(), true);

                        if (key.equals("TRUE") || key == "TRUE") {
                            pi.setSellCurrency(sc.getRateCurrency());
                            pi.setSellRoe(sc.getLocalRateAmountRoe());
                            pi.setSellAmountPerUnit(sc.getRateAmount());
                            pi.setSellAmount(sc.getTotalRateAmount());
                            pi.setSellLocalAmount(sc.getLocalTotalRateAmount());

                            pi.setBuyCurrency(sc.getCostCurrency());
                            pi.setBuyRoe(sc.getLocalCostAmountRoe());
                            pi.setBuyAmountPerUnit(sc.getCostAmount());
                            pi.setBuyAmount(sc.getTotalCostAmount());
                            pi.setBuyLocalAmount(sc.getLocalTotalCostAmount());
                        } else {
                            pi.setSellCurrency(sc.getNetSaleCurrency());
                            pi.setSellRoe(sc.getLocalNetSaleRoe());
                            pi.setSellAmountPerUnit(sc.getNetSaleAmount());
                            pi.setSellAmount(sc.getTotalNetSale());
                            pi.setSellLocalAmount(sc.getLocalTotalNetSale());

                            pi.setBuyCurrency(sc.getCostCurrency());
                            pi.setBuyRoe(sc.getLocalCostAmountRoe());
                            pi.setBuyAmountPerUnit(sc.getCostAmount());
                            pi.setBuyAmount(sc.getTotalCostAmount());
                            pi.setBuyLocalAmount(sc.getLocalTotalCostAmount());
                        }

                        if (pi.getSellLocalAmount() != null && pi.getSellLocalAmount() > 0) {
                            rAmt = rAmt + pi.getSellLocalAmount();
                        }

                        if (pi.getBuyLocalAmount() != null && pi.getBuyLocalAmount() > 0) {
                            cAmt = cAmt + pi.getBuyLocalAmount();
                        }

                        pi.setVendorSubledger(sc.getVendor());
                        pi.setBillToParty(sd.getParty());

                        pi.setProvisional(provisional);
                        provisional.getProvisionalItemList().add(pi);
                    }
                    provisional.setTotalRevenue(rAmt);
                    provisional.setTotalCost(cAmt);
                    provisional.setNetAmount(rAmt - cAmt);

                    provisionalValidator.validate(provisional);

                    provisional = provisionalRepository.save(provisional);
                }
            }

            log.info("Successfully getting Provisional by id...");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            afterGet(provisional);
            baseDto.setResponseObject(provisional);

        } catch (RestException exception) {

            log.error("RestException in getByShipmentUid method : ", exception);

            baseDto.setResponseCode(exception.getMessage());
        } catch (Exception exception) {

            log.error("Exception in getByShipmentUid method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);

        }

        return appUtil.setDesc(baseDto);
    }


    public BaseDto getByMasterUid(String masterUid) {
        log.info("get method is called....");

        BaseDto baseDto = new BaseDto();

        try {

            Provisional provisional = provisionalRepository.findByMasterUid(masterUid);
            afterGet(provisional);


            log.info("Successfully getting Provisional by id...");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(provisional);

        } catch (RestException exception) {

            log.error("RestException in getByMasterUid method : ", exception);

            baseDto.setResponseCode(exception.getMessage());
        } catch (Exception exception) {

            log.error("Exception in getByMasterUid method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);

        }

        return appUtil.setDesc(baseDto);
    }


    public BaseDto pushSave(Provisional entity) {

        BaseDto baseDto = new BaseDto();

        try {
            provisionalRepository.save(entity);
            baseDto.setResponseCode(ErrorCode.SUCCESS);
        } catch (Exception e) {
            // TODO: handle exception
            baseDto.setResponseCode(ErrorCode.FAILED);
            log.info("exception in push api save", e);

        }
        return appUtil.setDesc(baseDto);
    }
}
