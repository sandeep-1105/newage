package com.efreightsuite.service;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.CountryDynamicFieldDto;
import com.efreightsuite.dto.DynamicFieldSearchDto;
import com.efreightsuite.dto.SearchRespDto;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.model.DynamicFields;
import com.efreightsuite.repository.CountryDynamicFieldRepository;
import com.efreightsuite.repository.CountryMasterRepository;
import com.efreightsuite.repository.DynamicFieldRepository;
import com.efreightsuite.util.AppUtil;
import com.efreightsuite.util.StaticSearchQuery;
import com.efreightsuite.util.ValidateUtil;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.exception.ExceptionUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class DynamicFieldService {

    @Autowired
    private
    AppUtil appUtil;

    @Autowired
    private
    EntityManager em;

    @Autowired
    private
    DynamicFieldRepository dynamicFieldRepository;

    @Autowired
    CountryDynamicFieldRepository countryDynamicFieldRepository;

    @Autowired
    CountryMasterRepository countryMasterRepository;


    public BaseDto modifyDynamicFieldList(List<DynamicFields> entityList) {
        BaseDto baseDto = new BaseDto();
        for (DynamicFields entity : entityList) {
            modifyDynamicFields(entity);
        }

        baseDto.setResponseCode(ErrorCode.SUCCESS);
        return appUtil.setDesc(baseDto);
    }

    public BaseDto modifyDynamicFields(DynamicFields entity) {
        log.info("ShipmentService -> create method is called....");
        BaseDto baseDto = new BaseDto();
        try {
            ValidateUtil.notNull(entity, ErrorCode.DYNAMIC_FIELD_DATA_IS_INVALID);
            ValidateUtil.notNull(entity.getFieldName(), ErrorCode.DYNAMIC_FIELD_NAME_IS_REQUIRED);
            boolean isUpdate = false;
            if (entity.getId() != null) {
                isUpdate = true;
            }
            entity = dynamicFieldRepository.save(entity);
            /*if(!isUpdate) {
				List<CountryMaster> countryList = countryMasterRepository.findAllCountryObjectWithIdAlone();
				List<CountryDynamicField> countryDynamicFieldEntityList = new ArrayList<CountryDynamicField>();
				if(countryList != null && !countryList.isEmpty()) {
					for(CountryMaster country : countryList) {
						CountryDynamicField countryDynamicFieldEntity = new CountryDynamicField();
						countryDynamicFieldEntity.setCountryMaster(country);
						countryDynamicFieldEntity.setDynamicField(entity);
						countryDynamicFieldEntity.setShowField(YesNo.Yes);
						countryDynamicFieldEntity.setSystemTrack(TimeUtil.getCreateSystemTrack());
						countryDynamicFieldEntityList.add(countryDynamicFieldEntity);
					}
				}
				if(!countryDynamicFieldEntityList.isEmpty()) {
					countryDynamicFieldRepository.save(countryDynamicFieldEntityList);
				}
			}*/
            baseDto.setResponseCode(ErrorCode.SUCCESS);
            baseDto.setResponseObject(searchById(entity.getId()).getResponseObject());
        } catch (RestException re) {
            log.error("Exception in ShipmentService -> create method ", re);
            baseDto.setResponseCode(re.getMessage());
        } catch (DataIntegrityViolationException divEX) {
            log.error("Exception in DynamicFieldsService -> create or update method ", divEX);
            baseDto.setResponseCode(ErrorCode.FAILED);

            if (divEX.getMostSpecificCause().toString().contains("UK_DYNAMIC_FIELD_NAME")) {
                baseDto.setResponseCode(ErrorCode.UK_DYNAMIC_FIELD_NAME);
            } else if (divEX.getMostSpecificCause().toString().contains("UK_DYNAMIC_FIELD_ID")) {
                baseDto.setResponseCode(ErrorCode.UK_DYNAMIC_FIELD_ID);
            } else if (divEX.getMostSpecificCause().toString().contains("UK_DFC_FIELD_COUNTRY")) {
                baseDto.setResponseCode(ErrorCode.COUNTRY_UK_DYNAMIC_FIELD);
            }
        } catch (Exception exception) {
            log.error("Exception in DynamicFieldsService -> create or update method ", exception);
            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(exception);
            log.error("Exception Cause 1 ::: " + exceptionCause1);
            baseDto.setResponseCode(ErrorCode.FAILED);
        }
        return appUtil.setDesc(baseDto);
    }

    public BaseDto searchById(Long id) {

        log.info("DynamicFieldsService -> searchById method is called....");

        BaseDto baseDto = new BaseDto();

        try {

            DynamicFields entity = dynamicFieldRepository.findById(id);
            baseDto.setResponseCode(ErrorCode.SUCCESS);
            baseDto.setResponseObject(entity);

        } catch (RestException re) {
            log.error("Exception in DynamicFieldsService -> searchById method ", re);
            baseDto.setResponseCode(re.getMessage());
        } catch (Exception e) {
            log.error("Exception in DynamicFieldsService -> searchById method ", e);
            baseDto.setResponseCode(ErrorCode.FAILED);
        }
        return appUtil.setDesc(baseDto);
    }

    public BaseDto searchByCriteria(DynamicFieldSearchDto searchData) {

        log.info("DynamicFieldsService -> searchByCriteria method is called....");

        BaseDto baseDto = new BaseDto();

        try {
			/*UserProfile loggedInUser = AuthService.getCurrentUser();
			CompanyMaster selectedCompany = loggedInUser.getSelectedCompany();*/
            String hql = "";

            if (searchData.getCountryId() != null) {
                hql = hql + " join df.countryMasterList cm  where cm.id in (" + searchData.getCountryId() + ") ";
            }
            if (searchData.getFieldName() != null && searchData.getFieldName().trim().length() > 0) {
                if (hql.length() == 0) {
                    hql = hql + " where ";
                } else {
                    hql = hql + " and ";
                }
                hql = hql + " UPPER(df.fieldName) like '" + searchData.getFieldName().trim().toUpperCase() + "%' ";
            }

            if (searchData.getScreen() != null && searchData.getScreen().trim().length() > 0) {
                if (hql.length() == 0) {
                    hql = hql + " where ";
                } else {
                    hql = hql + " and ";
                }
                hql = hql + " UPPER(df.screen) like '" + searchData.getScreen().trim().toUpperCase() + "%' ";
            }

            if (searchData.getSection() != null && searchData.getSection().trim().length() > 0) {
                if (hql.length() == 0) {
                    hql = hql + " where ";
                } else {
                    hql = hql + " and ";
                }
                hql = hql + " UPPER(df.screentab) like '" + searchData.getSection().trim().toUpperCase() + "%' ";
            }

            if (searchData.getSortByColumn() != null && searchData.getOrderByType() != null && searchData.getSortByColumn().trim().length() != 0
                    && searchData.getOrderByType().trim().length() != 0) {
                hql = hql + " order by " + searchData.getSortByColumn().trim() + " " + searchData.getOrderByType().trim();
            } else {
                hql = hql + " order by df.id DESC";
            }
            log.info("Search HQL : [" + hql + "]");
            Query query = em.createQuery(StaticSearchQuery.DynamicFieldSearch + hql);
            query.setFirstResult(searchData.getSelectedPageNumber() * searchData.getRecordPerPage());
            query.setMaxResults(searchData.getRecordPerPage());


            String countQ = "Select count (df.id) from DynamicFields df  " + hql;
            Query countQuery = em.createQuery(countQ);
            Long countTotal = (Long) countQuery.getSingleResult();

            SearchRespDto searchRespDto = new SearchRespDto(countTotal, query.getResultList());
            searchRespDto.setRecordPerPage(searchData.getRecordPerPage());
            searchRespDto.setSelectedPageNumber(searchData.getSelectedPageNumber());
            baseDto.setResponseCode(ErrorCode.SUCCESS);
            baseDto.setResponseObject(searchRespDto);

        } catch (RestException re) {
            log.error("Exception in DynamicFieldsService -> searchById method ", re);
            baseDto.setResponseCode(re.getMessage());
        } catch (Exception e) {
            log.error("Exception in DynamicFieldsService -> searchById method ", e);
            baseDto.setResponseCode(ErrorCode.FAILED);
        }
        return appUtil.setDesc(baseDto);
    }


    /*
        public BaseDto modifyCountryDynamicFields(CountryDynamicField entity) {
            log.info("ShipmentService -> create method is called....");
            BaseDto baseDto = new BaseDto();
            try {
                ValidateUtil.notNull(entity, ErrorCode.COUNTRY_DYNAMIC_FIELD_DATA_IS_INVALID);
                ValidateUtil.notNull(entity.getCountryMaster(), ErrorCode.COUNTRY_DYNAMIC_FIELD_COUNTRY_IS_REQUIRED);
                ValidateUtil.notNull(entity.getCountryMaster().getId(), ErrorCode.COUNTRY_DYNAMIC_FIELD_COUNTRY_IS_REQUIRED);
                ValidateUtil.notNull(entity.getDynamicField(), ErrorCode.COUNTRY_DYNAMIC_FIELD_FIELD_IS_REQUIRED);
                ValidateUtil.notNull(entity.getDynamicField().getId(), ErrorCode.COUNTRY_DYNAMIC_FIELD_FIELD_IS_REQUIRED);

                entity.setSystemTrack(TimeUtil.getCreateSystemTrack());
                entity = countryDynamicFieldRepository.save(entity);
                baseDto.setResponseCode(ErrorCode.SUCCESS);
                baseDto.setResponseObject(searchById(entity.getId()).getResponseObject());
            } catch (RestException re) {
                log.error("Exception in ShipmentService -> create method ", re);
                baseDto.setResponseCode(re.getMessage());
            } catch(DataIntegrityViolationException divEX){
                log.error("Exception in DynamicFieldsService -> create or update method ", divEX);
                baseDto.setResponseCode(ErrorCode.FAILED);

                if(divEX.getMostSpecificCause().toString().contains("UK_COMPDYNFIELDCOUNTRY_FIELD")){
                    baseDto.setResponseCode(ErrorCode.COUNTRY_UK_DYNAMIC_FIELD);
                }
            } catch (Exception exception) {
                log.error("Exception in DynamicFieldsService -> create or update method ", exception);
                String exceptionCause1 = ExceptionUtils.getRootCauseMessage(exception);
                log.error("Exception Cause 1 ::: " + exceptionCause1);
                baseDto.setResponseCode(ErrorCode.FAILED);
            }
            return appUtil.setDesc(baseDto);
        }

    */
    public BaseDto getFeildsByCountry(Long countryId) {

        log.info("DynamicFieldsService -> searchById method is called....");

        BaseDto baseDto = new BaseDto();

        try {
            Long[] ids = new Long[]{countryId};
            List<CountryDynamicFieldDto> showMap = dynamicFieldRepository.findAllByCountryId(ids);
            //countryDynamicFieldRepository.findAllByCountryId(countryId);
            baseDto.setResponseCode(ErrorCode.SUCCESS);
            baseDto.setResponseObject(showMap);

        } catch (RestException re) {
            log.error("Exception in DynamicFieldsService -> searchById method ", re);
            baseDto.setResponseCode(re.getMessage());
        } catch (Exception e) {
            log.error("Exception in DynamicFieldsService -> searchById method ", e);
            baseDto.setResponseCode(ErrorCode.FAILED);
        }
        return appUtil.setDesc(baseDto);
    }


}
