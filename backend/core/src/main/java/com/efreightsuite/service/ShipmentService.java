package com.efreightsuite.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import javax.persistence.EntityManager;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.transaction.Transactional;

import com.efreightsuite.dto.AttachEventDto;
import com.efreightsuite.dto.AutoAttachmentDto;
import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.CopyShipmentSearchDto;
import com.efreightsuite.dto.DocumentDetailSearchDto;
import com.efreightsuite.dto.ImportExportSearchDto;
import com.efreightsuite.dto.ImportToExportDto;
import com.efreightsuite.dto.SearchRequest;
import com.efreightsuite.dto.ShipmentDto;
import com.efreightsuite.dto.ShipmentSearchRequestDto;
import com.efreightsuite.enumeration.EventMasterType;
import com.efreightsuite.enumeration.ReportName;
import com.efreightsuite.enumeration.YesNo;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.exception.UniqueKey;
import com.efreightsuite.model.AuditEntity;
import com.efreightsuite.model.DocumentDetail;
import com.efreightsuite.model.EventMaster;
import com.efreightsuite.model.PartyMaster;
import com.efreightsuite.model.PurchaseOrder;
import com.efreightsuite.model.ReportMaster;
import com.efreightsuite.model.Shipment;
import com.efreightsuite.model.ShipmentAttachment;
import com.efreightsuite.model.ShipmentCharge;
import com.efreightsuite.model.ShipmentServiceDetail;
import com.efreightsuite.model.ShipmentServiceEvent;
import com.efreightsuite.model.ShipmentServiceSignOff;
import com.efreightsuite.model.StockGeneration;
import com.efreightsuite.report.service.AutoAttachService;
import com.efreightsuite.repository.DefaultMasterDataRepository;
import com.efreightsuite.repository.DocumentDetailRepository;
import com.efreightsuite.repository.EventMasterRepository;
import com.efreightsuite.repository.LocationMasterRepository;
import com.efreightsuite.repository.PartyMasterRepository;
import com.efreightsuite.repository.ReportMasterRepository;
import com.efreightsuite.repository.ShipmentAttachmentRepository;
import com.efreightsuite.repository.ShipmentRepository;
import com.efreightsuite.repository.ShipmentServiceDetailRepository;
import com.efreightsuite.repository.ShipmentServiceEventRepository;
import com.efreightsuite.search.ShipmentSearchImpl;
import com.efreightsuite.service.mailer.ShipmentMailer;
import com.efreightsuite.util.AppUtil;
import com.efreightsuite.util.CryptoException;
import com.efreightsuite.util.CryptoUtils;
import com.efreightsuite.util.TimeUtil;
import com.efreightsuite.util.ValidateUtil;
import com.efreightsuite.validation.ShipmentServiceDetailValidator;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.hibernate.envers.AuditReader;
import org.hibernate.envers.AuditReaderFactory;
import org.hibernate.envers.RevisionType;
import org.hibernate.envers.query.AuditQuery;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.ObjectOptimisticLockingFailureException;
import org.springframework.orm.jpa.JpaObjectRetrievalFailureException;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class ShipmentService {

    @Autowired
    private
    AppUtil appUtil;

    @Autowired
    private
    AutoImportProcessService autoImportProcessService;

    @Autowired
    private
    ShipmentMilestone shipmentMilestone;

    @Autowired
    private
    ShipmentServiceDetailValidator shipmentServiceDetailValidator;

    @Autowired
    private
    ShipmentRepository shipmentRepository;

    @Autowired
    private
    ShipmentServiceDetailRepository shipmentServiceDetailRepository;

    @Autowired
    private
    ReportMasterRepository reportMasterRepository;

    @Autowired
    private
    ShipmentSearchImpl shipmentSearchImpl;

    @Autowired
    SequenceGeneratorService sequenceGeneratorService;

    @Autowired
    private
    ShipmentAttachmentRepository shipmentAttachmentRepository;

    @Autowired
    DefaultMasterDataRepository defaultMasterDataRepository;

    @Autowired
    private
    DocumentDetailRepository documentDetailRepository;
    @Autowired
    private
    ShipmentMailer shipmentMailer;

    @Autowired
    private
    AutoAttachService autoAttachService;

    @Autowired
    private
    PartyMasterRepository partyMasterRepository;

    @Autowired
    LocationMasterRepository locationMasterRepository;

    @Autowired
    private
    EventMasterRepository eventMasterRepository;

    @Autowired
    private
    ShipmentServiceEventRepository shipmentServiceEventRepository;

    @Autowired
    private
    EntityManager entityManager;

    public BaseDto getById(Long id) {

        log.info("ShipmentService -> getById method is called....");

        BaseDto baseDto = new BaseDto();

        try {

            Shipment shipment = shipmentRepository.findById(id);

            shipmentSearchImpl.afterGet(shipment);

            baseDto.setResponseCode(ErrorCode.SUCCESS);
            baseDto.setResponseObject(shipment);

        } catch (RestException re) {
            log.error("Exception in ShipmentService -> getById method ", re);
            baseDto.setResponseCode(re.getMessage());
        } catch (Exception e) {
            log.error("Exception in ShipmentService -> getById method ", e);
            baseDto.setResponseCode(ErrorCode.FAILED);
        }
        return appUtil.setDesc(baseDto);
    }

    public BaseDto getServiceById(Long id) {

        log.info("ShipmentService -> getServiceById method is called....");

        BaseDto baseDto = new BaseDto();

        try {

            ShipmentServiceDetail serviceImport = shipmentServiceDetailRepository.findById(id);

            ImportToExportDto dto = new ImportToExportDto();
            AppUtil.setService(serviceImport);
            dto.setImportService(serviceImport);

            ShipmentServiceDetail serviceExport = shipmentServiceDetailRepository.findByServiceUid(serviceImport.getExportRef());
            AppUtil.setService(serviceExport);
            dto.setExportService(serviceExport);

            baseDto.setResponseCode(ErrorCode.SUCCESS);
            baseDto.setResponseObject(dto);

        } catch (RestException re) {
            log.error("Exception in ShipmentService -> getServiceById method ", re);
            baseDto.setResponseCode(re.getMessage());

        } catch (Exception e) {
            log.error("Exception in ShipmentService -> getServiceById method ", e);
            baseDto.setResponseCode(ErrorCode.FAILED);

        }
        return appUtil.setDesc(baseDto);
    }

    public BaseDto getPurchaseOrderById(Long poId) {

        log.info("ShipmentService -> getServiceById method is called....");

        BaseDto baseDto = new BaseDto();

        try {

            PurchaseOrder po = shipmentServiceDetailRepository.findByPOId(poId);


            baseDto.setResponseCode(ErrorCode.SUCCESS);
            baseDto.setResponseObject(po);

        } catch (RestException re) {
            log.error("Exception in ShipmentService -> getServiceById method ", re);
            baseDto.setResponseCode(re.getMessage());

        } catch (Exception e) {
            log.error("Exception in ShipmentService -> getServiceById method ", e);
            baseDto.setResponseCode(ErrorCode.FAILED);

        }
        return appUtil.setDesc(baseDto);
    }


    public BaseDto getShipmentId(Long serviceUid) {

        log.info("ShipmentService -> getByShipmentUid method is called....");

        BaseDto baseDto = new BaseDto();

        try {

            baseDto.setResponseObject(shipmentRepository.getByService(serviceUid).getShipment().getId());
            baseDto.setResponseCode(ErrorCode.SUCCESS);
        } catch (RestException re) {
            log.error("Exception in ShipmentService -> getByShipmentUid method ", re);
            baseDto.setResponseCode(re.getMessage());

        } catch (Exception e) {
            log.error("Exception in ShipmentService -> getByShipmentUid method ", e);
            baseDto.setResponseCode(ErrorCode.FAILED);

        }
        return appUtil.setDesc(baseDto);
    }

    public BaseDto getShipmentIdByServiceUid(String serviceUid) {

        log.info("ShipmentService -> getShipmentIdByServiceUid method is called....");

        BaseDto baseDto = new BaseDto();

        try {

            baseDto.setResponseObject(shipmentServiceDetailRepository.findByServiceUid(serviceUid).getShipment().getId());
            baseDto.setResponseCode(ErrorCode.SUCCESS);
        } catch (RestException re) {
            log.error("Exception in ShipmentService -> getShipmentIdByServiceUid method ", re);
            baseDto.setResponseCode(re.getMessage());

        } catch (Exception e) {
            log.error("Exception in ShipmentService -> getShipmentIdByServiceUid method ", e);
            baseDto.setResponseCode(ErrorCode.FAILED);

        }
        return appUtil.setDesc(baseDto);
    }

    public BaseDto getServiceIdByServiceUid(String serviceUid) {

        log.info("ShipmentService -> getServiceIdByServiceUid method is called....");

        BaseDto baseDto = new BaseDto();

        try {

            baseDto.setResponseObject(shipmentServiceDetailRepository.findByServiceUid(serviceUid).getId());
            baseDto.setResponseCode(ErrorCode.SUCCESS);
        } catch (RestException re) {
            log.error("Exception in ShipmentService -> getServiceIdByServiceUid method ", re);
            baseDto.setResponseCode(re.getMessage());

        } catch (Exception e) {
            log.error("Exception in ShipmentService -> getServiceIdByServiceUid method ", e);
            baseDto.setResponseCode(ErrorCode.FAILED);

        }
        return appUtil.setDesc(baseDto);
    }


    // service returns
    public BaseDto getService(String serviceUid) {

        log.info("ShipmentService -> getShipmentIdByServiceUid method is called....");

        BaseDto baseDto = new BaseDto();

        try {

            baseDto.setResponseObject(shipmentServiceDetailRepository.findService(serviceUid));
            baseDto.setResponseCode(ErrorCode.SUCCESS);
        } catch (RestException re) {
            log.error("Exception in ShipmentService -> getShipmentIdByServiceUid method ", re);
            baseDto.setResponseCode(re.getMessage());

        } catch (Exception e) {
            log.error("Exception in ShipmentService -> getShipmentIdByServiceUid method ", e);
            baseDto.setResponseCode(ErrorCode.FAILED);

        }
        return appUtil.setDesc(baseDto);
    }


    public BaseDto getByShipmentUid(String shipmentUid) {

        log.info("ShipmentService -> getByShipmentUid method is called....");

        BaseDto baseDto = new BaseDto();

        try {


            Shipment shipment = shipmentRepository.findByShipmentUid(shipmentUid);

            shipmentSearchImpl.afterGet(shipment);
            baseDto.setResponseCode(ErrorCode.SUCCESS);
            baseDto.setResponseObject(shipment);

        } catch (RestException re) {
            log.error("Exception in ShipmentService -> getByShipmentUid method ", re);
            baseDto.setResponseCode(re.getMessage());

        } catch (Exception e) {
            log.error("Exception in ShipmentService -> getByShipmentUid method ", e);
            baseDto.setResponseCode(ErrorCode.FAILED);

        }
        return appUtil.setDesc(baseDto);
    }

    public BaseDto search(ShipmentSearchRequestDto searchDto) {

        log.info("ShipmentService -> search method is called....");

        BaseDto baseDto = new BaseDto();

        try {

            baseDto.setResponseCode(ErrorCode.SUCCESS);
            baseDto.setResponseObject(shipmentSearchImpl.search(searchDto));

        } catch (RestException re) {
            log.error("Exception in ShipmentService -> search method ", re);
            baseDto.setResponseCode(re.getMessage());

        } catch (Exception e) {
            log.error("Exception in ShipmentService -> search method ", e);
            baseDto.setResponseCode(ErrorCode.FAILED);

        }

        return appUtil.setDesc(baseDto);
    }

    public BaseDto search(CopyShipmentSearchDto copyShipmentSearchDto) {

        log.info("ShipmentService -> search method is called....");

        BaseDto baseDto = new BaseDto();

        try {

            ValidateUtil.notNull(copyShipmentSearchDto.getServiceId(), ErrorCode.SHIPMENT_COPY_SERVICE_REQUIRED);

            ValidateUtil.notNull(copyShipmentSearchDto.getPartyId(), ErrorCode.SHIPMENT_COPY_PARTY_REQUIRED);

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(shipmentSearchImpl.search(copyShipmentSearchDto));

        } catch (RestException re) {

            log.error("Exception in ShipmentService -> search method ", re);

            baseDto.setResponseCode(re.getMessage());

        } catch (Exception e) {

            log.error("Exception in ShipmentService -> search method ", e);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);
    }

    public BaseDto importExportSearch(ImportExportSearchDto searchDto) {

        log.info("ShipmentService -> search method is called....");

        BaseDto baseDto = new BaseDto();

        try {

            baseDto.setResponseCode(ErrorCode.SUCCESS);
            baseDto.setResponseObject(shipmentSearchImpl.importExportSearch(searchDto));

        } catch (RestException re) {
            log.error("Exception in ShipmentService -> search method ", re);
            baseDto.setResponseCode(re.getMessage());

        } catch (Exception e) {
            log.error("Exception in ShipmentService -> search method ", e);
            baseDto.setResponseCode(ErrorCode.FAILED);

        }

        return appUtil.setDesc(baseDto);
    }


    public BaseDto create(Shipment shipment) {
        BaseDto baseDto = new BaseDto();
        try {
            Map<String, String> newServiceMap = new HashMap<>();
            baseDto.setResponseCode(ErrorCode.SUCCESS);

            Shipment persistedShipment = shipmentSearchImpl.createShipment(shipment, newServiceMap);
            shipmentSearchImpl.afterGet(persistedShipment);
            // Shekhar: No need to send the full object as the frontend makes get request
//            baseDto.setResponseObject(persistedShipment);
            Shipment shipmentResponse = new Shipment(persistedShipment.getId(), persistedShipment.getShipmentUid());
            baseDto.setResponseObject(shipmentResponse);

            if (Objects.equals(baseDto.getResponseCode(), ErrorCode.SUCCESS)) {
                if (baseDto.getResponseObject() != null) {
                    shipmentMailer.shipmentMailTrigger((Shipment) baseDto.getResponseObject(), AuthService.getCurrentUser(), newServiceMap);
                }
            }

        } catch (RestException re) {
            log.error("Exception in ShipmentService -> create method ", re);
            if (re.getParams() != null && !re.getParams().isEmpty()) {
                for (String param : re.getParams()) {
                    baseDto.getParams().add(param);
                }
            }
            baseDto.setResponseCode(re.getMessage());

        } catch (Exception exception) {
            log.error("Exception in ShipmentService -> create method ", exception);

            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(exception);
            log.error("Exception Cause 1 ::: " + exceptionCause1);

            baseDto.setResponseCode(ErrorCode.FAILED);

            if (exceptionCause1.contains(UniqueKey.UK_SHIPMENT_SHIPMENTUID)) {
                baseDto.setResponseCode(ErrorCode.SHIPMENT_UK_SHIPMENT_SHIPMENTUID);
            }

            if (exceptionCause1.contains(UniqueKey.UK_SHIPMENTATT_REF)) {
                baseDto.setResponseCode(ErrorCode.ATTACHMENT_REFERENCE_NUMBER_DUPLICATED);
            }
            if (exceptionCause1.contains(UniqueKey.UK_SERVICE_DUPLICATE)) {
                baseDto.setResponseCode(ErrorCode.SHIPMENT_SERVICE_EXIST);
            }

            if (exceptionCause1.contains(UniqueKey.UK_DOCUMENTDETA_DOCUMENTNO)) {
                baseDto.setResponseCode(ErrorCode.DOCUMENT_NO_DUPLICATED);
            }
            if (exceptionCause1.contains(UniqueKey.UK_DOCUMENTDETA_HAWBNO)) {
                baseDto.setResponseCode(ErrorCode.HAWB_NO_DUPLICATED);
            }

            if (exceptionCause1.contains("FK_")) {
                baseDto.setResponseCode(ErrorCode.SAVE_UPDATE_FK_RECORD);
            }

        }
        return appUtil.setDesc(baseDto);
    }


    public BaseDto update(Shipment shipment) {

        log.info("ShipmentService -> update method is called....");

        BaseDto baseDto = new BaseDto();
        long timeBeforeUpdatingShipment = System.currentTimeMillis();

        try {
            Map<String, String> newServiceMap = new HashMap<>();
            baseDto.setResponseCode(ErrorCode.SUCCESS);
            Shipment updatedShipment = shipmentSearchImpl.updateShipment(shipment, newServiceMap);
            long timeAfterUpdatingShipment = System.currentTimeMillis();
            log.info("Total Time taken in Updating Shipment is:(in Sec) ", timeAfterUpdatingShipment - timeBeforeUpdatingShipment);
            shipmentSearchImpl.afterGet(updatedShipment);
            long timeAfterGet = System.currentTimeMillis();
            log.info("Total Time taken in calling afterGet Method:(in Sec) ", timeAfterGet - timeAfterUpdatingShipment);
            baseDto.setResponseObject(new Shipment(updatedShipment.getId(), updatedShipment.getShipmentUid()));
            long timeAfterSettingResponseObj = System.currentTimeMillis();
            log.info("Time taken in setting ResponseObject in baseDTO, Calling shipmentRepository.findById: (in Sec)", timeAfterSettingResponseObj - timeAfterGet);
            if (Objects.equals(baseDto.getResponseCode(), ErrorCode.SUCCESS) || baseDto.getResponseCode().equals(ErrorCode.SUCCESS)) {
                if (baseDto.getResponseObject() != null) {
                    shipmentMailer.shipmentMailTrigger((Shipment) baseDto.getResponseObject(), AuthService.getCurrentUser(), newServiceMap);
                    log.info("Time taken in Shipment MailTrigger: (in Sec)", System.currentTimeMillis() - timeAfterSettingResponseObj);


                }
            }
        } catch (RestException re) {
            log.error("Exception in ShipmentService -> update method ", re);
            if (re.getParams() != null && !re.getParams().isEmpty()) {
                for (String param : re.getParams()) {
                    baseDto.getParams().add(param);
                }
            }
            baseDto.setResponseCode(re.getMessage());
        } catch (ObjectOptimisticLockingFailureException e) {

            log.error("Error in editing Shipment", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_LOCKED_RECORD);

        } catch (JpaObjectRetrievalFailureException e) {

            log.error("Error in editing Shipment", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_DELETED_RECORD);

        } catch (Exception exception) {
            log.error("Exception in ShipmentService -> update method ", exception);

            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(exception);
            log.error("Exception Cause 1 ::: " + exceptionCause1);

            baseDto.setResponseCode(ErrorCode.FAILED);

            if (exceptionCause1.contains(UniqueKey.UK_SHIPMENT_SHIPMENTUID)) {
                baseDto.setResponseCode(ErrorCode.SHIPMENT_UK_SHIPMENT_SHIPMENTUID);
            }

            if (exceptionCause1.contains(UniqueKey.UK_SHIPMENTATT_REF)) {
                baseDto.setResponseCode(ErrorCode.ATTACHMENT_REFERENCE_NUMBER_DUPLICATED);
            }
            if (exceptionCause1.contains(UniqueKey.UK_SERVICE_DUPLICATE)) {
                baseDto.setResponseCode(ErrorCode.SHIPMENT_SERVICE_EXIST);
            }

            if (exceptionCause1.contains(UniqueKey.UK_DOCUMENTDETA_DOCUMENTNO)) {
                baseDto.setResponseCode(ErrorCode.DOCUMENT_NO_DUPLICATED);
            }
            if (exceptionCause1.contains(UniqueKey.UK_DOCUMENTDETA_HAWBNO)) {
                baseDto.setResponseCode(ErrorCode.HAWB_NO_DUPLICATED);
            }

            if (exceptionCause1.contains("FK_")) {
                baseDto.setResponseCode(ErrorCode.SAVE_UPDATE_FK_RECORD);
            }

        }

        log.info("Total Time taken in Updating final Shipment is :(in Sec)", System.currentTimeMillis() - timeBeforeUpdatingShipment);
        return appUtil.setDesc(baseDto);
    }


    public BaseDto statusChange(ShipmentDto shipmentDto) {

        log.info("ShipmentService -> update method is called....");

        BaseDto baseDto = new BaseDto();

        try {
            shipmentSearchImpl.cancelProcess(shipmentDto);
            baseDto.setResponseCode(ErrorCode.SUCCESS);
            baseDto.setResponseObject(getById(shipmentDto.getShipmentId()).getResponseObject());

        } catch (RestException re) {
            log.error("Exception in ShipmentService -> update method ", re);
            baseDto.setResponseCode(re.getMessage());

        } catch (Exception exception) {
            log.error("Exception in ShipmentService -> update method ", exception);

            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(exception);
            log.error("Exception Cause 1 ::: " + exceptionCause1);

            baseDto.setResponseCode(ErrorCode.FAILED);

            if (exceptionCause1.contains("FK_")) {
                baseDto.setResponseCode(ErrorCode.SAVE_UPDATE_FK_RECORD);
            }

        }
        return appUtil.setDesc(baseDto);
    }

    public void downloadAttachment(Long attachmentId, String type, boolean swapped, HttpServletResponse response) {


        try {

            ShipmentAttachment attachment = shipmentAttachmentRepository.findOne(attachmentId);


            if (attachment != null) {
                String KEY = appUtil.getLocationConfig("shipment.attach.file.encrypt.key", attachment.getShipmentServiceDetail().getLocation(), false);


                if (attachment.getIsProtected().equals(YesNo.Yes)) {

                    log.info("Its a protected File... ");

                    attachment.setFile(CryptoUtils.decrypt(KEY, attachment.getFile()));
                }
                response.getOutputStream().write(attachment.getFile());

                response.setContentType(attachment.getFileContentType());

                response.setContentLength(attachment.getFile().length);

                response.setHeader("Content-Disposition", "attachment; filename=" + attachment.getFileName());

                response.getOutputStream().flush();

                response.getOutputStream().close();
            } else {
                log.info("Attachment is not available : " + attachmentId);
            }
        } catch (Exception exception) {
            log.error("Error occured while downloading attachment file : " + attachmentId + ", ", exception);
        }

    }


    public BaseDto createExportService(ImportToExportDto importToExportDto) {

        BaseDto baseDto = new BaseDto();
        log.info("createExportService start");

        try {
            shipmentSearchImpl.importToExport(importToExportDto, baseDto);
        } catch (RestException re) {
            log.error("Exception in ShipmentService -> createExportService method ", re);
            baseDto.setResponseCode(re.getMessage());

        } catch (Exception exception) {
            log.error("Exception in ShipmentService -> createExportService method ", exception);

            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(exception);
            log.error("Exception Cause 1 ::: " + exceptionCause1);

            baseDto.setResponseCode(ErrorCode.FAILED);

            if (exceptionCause1.contains(UniqueKey.UK_SHIPMENT_SHIPMENTUID)) {
                baseDto.setResponseCode(ErrorCode.SHIPMENT_UK_SHIPMENT_SHIPMENTUID);
            }

            if (exceptionCause1.contains(UniqueKey.UK_SHIPMENTATT_REF)) {
                baseDto.setResponseCode(ErrorCode.ATTACHMENT_REFERENCE_NUMBER_DUPLICATED);
            }
            if (exceptionCause1.contains(UniqueKey.UK_SERVICE_DUPLICATE)) {
                baseDto.setResponseCode(ErrorCode.SHIPMENT_SERVICE_EXIST);
            }

            if (exceptionCause1.contains(UniqueKey.UK_DOCUMENTDETA_DOCUMENTNO)) {
                baseDto.setResponseCode(ErrorCode.DOCUMENT_NO_DUPLICATED);
            }
            if (exceptionCause1.contains(UniqueKey.UK_DOCUMENTDETA_HAWBNO)) {
                baseDto.setResponseCode(ErrorCode.HAWB_NO_DUPLICATED);
            }

            if (exceptionCause1.contains("FK_")) {
                baseDto.setResponseCode(ErrorCode.SAVE_UPDATE_FK_RECORD);
            }

        }
        return appUtil.setDesc(baseDto);
    }


    // update for stocks in shipment

    @Transactional
    public BaseDto updatebyShipmentuid(StockGeneration stockGeneration) {

        BaseDto baseDto = new BaseDto();

        List<ShipmentServiceDetail> tmpList = shipmentServiceDetailRepository.getByServiceByMawbNo(stockGeneration.getMawbNo());
        if (tmpList != null && tmpList.size() != 0) {
            for (ShipmentServiceDetail service : tmpList) {
                service.setMawbNo(null);
                service.setMawbDate(null);

                for (DocumentDetail dd : service.getDocumentList()) {
                    if (dd.getMawbNo().equals(stockGeneration.getMawbNo())
                            || dd.getMawbNo() == stockGeneration.getMawbNo()) {
                        dd.setMawbNo(null);
                    }
                }
            }
            shipmentServiceDetailRepository.save(tmpList);
        }
        baseDto.setResponseCode(ErrorCode.SUCCESS);
        return appUtil.setDesc(baseDto);
    }


    public BaseDto search(SearchRequest searchRequest) {

        log.info("Search method is called...." + searchRequest);

        BaseDto baseDto = new BaseDto();

        try {

            baseDto.setResponseObject(shipmentSearchImpl.search(searchRequest));

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            log.info("Successfully Searching Shipment...");

        } catch (Exception exception) {

            log.info("Exception in Search method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);
    }


    public BaseDto searchService(SearchRequest searchRequest) {

        log.info("Search Service method is called...." + searchRequest);

        BaseDto baseDto = new BaseDto();

        try {

            baseDto.setResponseObject(shipmentSearchImpl.searchService(searchRequest));

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            log.info("Successfully Searching Shipment Service...");

        } catch (Exception exception) {

            log.info("Exception in Search method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);
    }

    public BaseDto getShipmentIdBasedOnUid(String shipmentUid) {

        log.info("ShipmentService -> getShipmentIdBasedOnUid method is called....");

        BaseDto baseDto = new BaseDto();

        try {

            Long shipmentId = shipmentRepository.getShipmentIdBasedOnUid(shipmentUid);

            baseDto.setResponseCode(ErrorCode.SUCCESS);
            baseDto.setResponseObject(shipmentId);

        } catch (RestException re) {
            log.error("Exception in ShipmentService -> getShipmentIdBasedOnUid method ", re);
            baseDto.setResponseCode(re.getMessage());

        } catch (Exception e) {
            log.error("Exception in ShipmentService -> getShipmentIdBasedOnUid method ", e);
            baseDto.setResponseCode(ErrorCode.FAILED);

        }
        return appUtil.setDesc(baseDto);
    }


    public BaseDto toBeGenerateDONumber(Long doumentId) {

        log.info("shipment service -> toBeGenerateDONumber method is called....");

        BaseDto baseDto = new BaseDto();

        try {

            baseDto.setResponseObject(shipmentSearchImpl.toBeGenerateDONumber(doumentId));
            baseDto.setResponseCode(ErrorCode.SUCCESS);

        } catch (RestException re) {
            log.error("Exception in ConsolService -> toBeGenerateDONumber method ", re);
            baseDto.setResponseCode(re.getMessage());

        } catch (Exception exception) {
            log.error("Exception in ConsolService -> toBeGenerateDONumber method ", exception);

            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(exception);
            log.error("Exception Cause 1 ::: " + exceptionCause1);

            baseDto.setResponseCode(ErrorCode.FAILED);

        }
        return appUtil.setDesc(baseDto);
    }


    public BaseDto toBeGenerateCanNumber(Long doumentId) {

        log.info("ConsolService -> toBeGenerateCanNumber method is called....");

        BaseDto baseDto = new BaseDto();

        try {

            baseDto.setResponseObject(shipmentSearchImpl.toBeGenerateCanNumber(doumentId));
            baseDto.setResponseCode(ErrorCode.SUCCESS);


        } catch (RestException re) {
            log.error("Exception in ConsolService -> toBeGenerateCanNumber method ", re);
            baseDto.setResponseCode(re.getMessage());

        } catch (Exception exception) {
            log.error("Exception in ConsolService -> toBeGenerateCanNumber method ", exception);

            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(exception);
            log.error("Exception Cause 1 ::: " + exceptionCause1);

            baseDto.setResponseCode(ErrorCode.FAILED);

        }
        return appUtil.setDesc(baseDto);
    }


    public BaseDto updateDocument(DocumentDetail documentDetail) {

        BaseDto baseDto = new BaseDto();

        try {

            DocumentDetail dc = documentDetailRepository.save(documentDetail);
            baseDto.setResponseObject(dc.getShipmentServiceDetail().getShipment().getId());
        } catch (RestException re) {
            log.error("Exception in ConsolService -> toBeGenerateDONumber method ", re);
            baseDto.setResponseCode(re.getMessage());
        } catch (Exception exception) {
            log.error("Exception in ConsolService -> toBeGenerateDONumber method ", exception);
            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(exception);
            log.error("Exception Cause 1 ::: " + exceptionCause1);
            baseDto.setResponseCode(ErrorCode.FAILED);
        }
        return appUtil.setDesc(baseDto);
    }

    @Transactional
    private ShipmentServiceSignOff signOff(ShipmentServiceSignOff shipmentServiceSignOff) {

        ShipmentServiceDetail service = shipmentServiceDetailRepository.findByServiceUid(shipmentServiceSignOff.getServiceUid());

        return shipmentServiceDetailValidator.signOff(shipmentServiceSignOff, service);
    }

    private ShipmentServiceSignOff unSignOff(ShipmentServiceSignOff shipmentServiceSignOff) {
        ShipmentServiceDetail service = shipmentServiceDetailRepository.findByServiceUid(shipmentServiceSignOff.getServiceUid());
        return shipmentServiceDetailValidator.unSignOff(shipmentServiceSignOff, service);
    }


    public BaseDto saveShipmentServiceSignOff(ShipmentServiceSignOff shipmentServiceSignOff) {

        BaseDto baseDto = new BaseDto();

        try {

            baseDto.setResponseObject(signOff(shipmentServiceSignOff));
            baseDto.setResponseCode(ErrorCode.SUCCESS);


        } catch (RestException re) {
            log.error("Exception in ShipmentService -> saveShipmentServiceSignOff method ", re);
            baseDto.setResponseCode(re.getMessage());
        } catch (Exception exception) {
            log.error("Exception in ShipmentService -> saveShipmentServiceSignOff method ", exception);
            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(exception);
            log.error("Exception Cause 1 ::: " + exceptionCause1);
            baseDto.setResponseCode(ErrorCode.FAILED);
        }
        return appUtil.setDesc(baseDto);
    }

    public BaseDto saveShipmentServiceUnSignOff(ShipmentServiceSignOff shipmentServiceSignOff) {

        BaseDto baseDto = new BaseDto();

        try {
            baseDto.setResponseObject(unSignOff(shipmentServiceSignOff));
            baseDto.setResponseCode(ErrorCode.SUCCESS);


        } catch (RestException re) {
            log.error("Exception in ShipmentService -> saveShipmentServiceSignOff method ", re);
            baseDto.setResponseCode(re.getMessage());
        } catch (Exception exception) {
            log.error("Exception in ShipmentService -> saveShipmentServiceSignOff method ", exception);
            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(exception);
            log.error("Exception Cause 1 ::: " + exceptionCause1);
            baseDto.setResponseCode(ErrorCode.FAILED);
        }
        return appUtil.setDesc(baseDto);
    }

    public BaseDto confirmHawbByCustomer(DocumentDetailSearchDto documentDetailSearchDto, HttpServletRequest request) {
        BaseDto baseDto = new BaseDto();
        try {
            DocumentDetail documentDetail = documentDetailRepository
                    .getOne(documentDetailSearchDto.getDocumentId());

            if (documentDetail == null) {
                baseDto.setResponseCode(ErrorCode.HAWB_CUSTOMER_CONFORMATION_DOCUMENT_REQUIRED);
                return appUtil.setDesc(baseDto);
            }


            //ShipmentServiceDetail shipmentServiceDetail=shipmentServiceDetailRepository.getOne(documentDetail.getShipmentServiceDetail().getId());

            PartyMaster partyMaster = partyMasterRepository.getOne(documentDetail.getShipmentServiceDetail().getParty().getId());

            ValidateUtil.notNull(partyMaster, ErrorCode.HAWB_CUSTOMER_CONFORMATION_PARTY_REQUIRED);

            ValidateUtil.isStringEquals(documentDetailSearchDto.getPin(), partyMaster.getPartyDetail().getPin(), ErrorCode.HAWB_CUSTOMER_CONFORMATION_PIN_INVALID);

            documentDetailRepository.updateIsCustomerConform(request.getRemoteAddr(), YesNo.Yes, documentDetail.getId());

            baseDto.setResponseObject(YesNo.Yes);
            baseDto.setResponseCode(ErrorCode.SUCCESS);

        } catch (RestException re) {
            log.error("Exception in ShipmentService -> confirmHawbByCustomer method ", re);
            baseDto.setResponseCode(re.getMessage());
        } catch (Exception exception) {
            log.error("Exception in ShipmentService -> confirmHawbByCustomer method ", exception);
            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(exception);
            log.error("Exception Cause 1 ::: " + exceptionCause1);
            baseDto.setResponseCode(ErrorCode.FAILED);
        }
        return appUtil.setDesc(baseDto);
    }


    //update attachment and event after do download all
    public BaseDto updateAE(List<AttachEventDto> aeDto) {

        BaseDto bd = new BaseDto();
        if (aeDto != null && aeDto.size() > 0) {
            try {
                addEvent(aeDto);
                addAttachment(aeDto);
                bd.setResponseCode(ErrorCode.SUCCESS);
            } catch (RestException re) {
                log.error("Exception in ShipmentService -> updateAE method ", re);
                bd.setResponseCode(re.getMessage());
            } catch (Exception e) {
                log.error("Exception in ShipmentService -> updateAE method ", e);
                bd.setResponseCode(ErrorCode.FAILED);
            }
        } else {
            throw new RestException(ErrorCode.FAILED);
        }
        return appUtil.setDesc(bd);
    }

    @Transactional
    private void addAttachment(List<AttachEventDto> aeDtoList) throws CryptoException {

        AutoAttachmentDto autoAttachmentDto = new AutoAttachmentDto();

        ReportMaster reportMaster = reportMasterRepository.findByReportKeyAndIsReportEnable(ReportName.DELIVERY_ORDER, YesNo.Yes);
        if (reportMaster != null && reportMaster.getIsReportEnable() == YesNo.Yes) {
            for (AttachEventDto attachEventDto : aeDtoList) {
                ShipmentServiceDetail sd = shipmentServiceDetailRepository.findByServiceUid(attachEventDto.getServiceUid());
                AutoAttachmentDto autoAttachDto = new AutoAttachmentDto();
                autoAttachDto.setTransactionFor("Shipment");
                autoAttachDto.setDocumentName(ReportName.DELIVERY_ORDER.name());
                autoAttachDto.setDocumentObj(attachEventDto.getDocumentObj());
                autoAttachDto.setFormat("application/pdf");
                autoAttachDto.setChildId(sd.getId());
                autoAttachService.attach(autoAttachDto);
            }
        }

    }

    @Transactional
    private void addEvent(List<AttachEventDto> aeDtoList) {

        for (AttachEventDto attachEventDto : aeDtoList) {

            ShipmentServiceDetail sd = shipmentServiceDetailRepository.findByServiceUid(attachEventDto.getServiceUid());
            if (null != sd) {
                if (CollectionUtils.isNotEmpty(sd.getEventList()))
                    log.info("addEvent called");
                else
                    sd.setEventList(new ArrayList<>());

                ShipmentServiceEvent se = null;
                EventMaster event = eventMasterRepository.findByEventMasterType(EventMasterType.DO_RECEIVED);
                se = shipmentServiceEventRepository.findByEventId(event.getId(), sd.getId());
                if (se == null) {
                    se = new ShipmentServiceEvent();
                    se.setShipmentServiceDetail(sd);
                    se.setEventMaster(event);
                    se.setEventDate(TimeUtil.getCurrentLocationTime(sd.getLocation()));
                    se.setFollowUpRequired(YesNo.No);
                    se.setIsCompleted(YesNo.Yes);
                    sd.getEventList().add(se);
                    shipmentServiceDetailRepository.save(sd);
                }
            }
        }
    }

    public BaseDto getShipmentChargeHistory(String shipmentUid) {
        log.info("getShipmentChargeHistory called");

        BaseDto baseDto = new BaseDto();
        try {

            AuditReader reader = AuditReaderFactory.get(entityManager);

            AuditQuery query = reader.createQuery()
                    .forRevisionsOfEntity(ShipmentCharge.class, false, true);

            query.add(org.hibernate.envers.query.AuditEntity.property("shipmentUid").eq(shipmentUid));

            List<Number> revList = query.getResultList();

            List<ShipmentCharge> list = new ArrayList<>();

            if (CollectionUtils.isNotEmpty(revList)) {
                for (Object obj : revList) {
                    Object[] objects = (Object[]) obj;
                    ShipmentCharge shipmentCharge = (ShipmentCharge) objects[0];
                    AuditEntity auditEntity = (AuditEntity) objects[1];
                    RevisionType revisonType = (RevisionType) objects[2];
                    shipmentCharge.setAuditDate(auditEntity.getRevisionDate());
                    shipmentCharge.setRevisionType(revisonType.name());
                    list.add(shipmentCharge);
                }
                baseDto.setResponseCode(ErrorCode.SUCCESS);
                baseDto.setResponseObject(list);
            } else {
                baseDto.setResponseCode(ErrorCode.NO_AUDIT_HISTORY_FOUND);
            }
        } catch (Exception exception) {
            log.error("Exception occured at Audit Histyory of Quotation service" + exception);
            baseDto.setResponseCode(ErrorCode.FAILED);
        }
        return baseDto;
    }


    public BaseDto documentListByServiceId(Long serviceId) {
        log.info("Document List Method is Invoked.");
        BaseDto baseDto = new BaseDto();
        baseDto.setResponseCode(ErrorCode.SUCCESS);
        baseDto.setResponseObject(documentDetailRepository.findAllDocumentById(serviceId));
        return appUtil.setDesc(baseDto);
    }

    public BaseDto trackShipment(Long shipmentId) {
        BaseDto baseDto = new BaseDto();
        try {
            if (shipmentId == null) {
                log.error("Shipment id is not provided");
            } else {
                baseDto.setResponseObject(shipmentMilestone.milestone(shipmentId));
                baseDto.setResponseCode(ErrorCode.SUCCESS);
            }
        } catch (RestException re) {
            log.error("Exception in ShipmentService -> trackShipment method ", re);
            baseDto.setResponseCode(re.getMessage());

        } catch (Exception e) {
            log.error("Exception occured at trackShipment", e);
            baseDto.setResponseCode(ErrorCode.FAILED);
        }
        return baseDto;
    }

    public BaseDto getSignOffStatus(Long serviceId) {
        BaseDto baseDto = new BaseDto();
        try {

            ShipmentServiceDetail service = shipmentRepository.getByService(serviceId);

            if (service.getShipmentServiceSignOff() == null) {
                baseDto.setResponseObject(null);
            } else {
                baseDto.setResponseObject(service.getShipmentServiceSignOff());
            }

            baseDto.setResponseCode(ErrorCode.SUCCESS);

        } catch (Exception e) {
            log.error("Exception occured at Service sign off", e);
            baseDto.setResponseCode(ErrorCode.FAILED);
        }
        return baseDto;
    }


    public BaseDto executeAutoImportProcess(Long consolId) {
        log.info("Start AutoImportProcessService.executeAutoImportProcess with service id : " + consolId);

        BaseDto baseDto = new BaseDto();
        try {

            autoImportProcessService.markAsComplete(consolId, baseDto);

        } catch (RestException re) {
            log.error("Exception in AutoImportProcessService -> executeAutoImportProcess method ", re);
            baseDto.setResponseCode(re.getMessage());

        } catch (Exception exception) {
            log.error("Exception in AutoImportProcessService -> executeAutoImportProcess method ", exception);

            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(exception);
            log.error("Exception Cause 1 ::: " + exceptionCause1);

            baseDto.setResponseCode(ErrorCode.FAILED);

            if (exceptionCause1.contains(UniqueKey.UK_SHIPMENTATT_REF)) {
                baseDto.setResponseCode(ErrorCode.ATTACHMENT_REFERENCE_NUMBER_DUPLICATED);
            }

            if (exceptionCause1.contains(UniqueKey.UK_CONSOL_CONSOLUID)) {
                baseDto.setResponseCode(ErrorCode.CONSOL_UNIQUE);
            }

            if (exceptionCause1.contains(UniqueKey.UK_CONSLATTCH_REF)) {
                baseDto.setResponseCode(ErrorCode.ATTACHMENT_REFERENCE_NUMBER_DUPLICATED);
            }

            if (exceptionCause1.contains("FK_")) {
                baseDto.setResponseCode(ErrorCode.SAVE_UPDATE_FK_RECORD);
            }

        }

        log.info("End AutoImportProcessService.executeAutoImportProcess with service id : ");

        return appUtil.setDesc(baseDto);

    }

    public BaseDto isShipmentCreatedFromQuotation(String quotationNo) {
        BaseDto baseDto = new BaseDto();
        try {
            shipmentServiceDetailValidator.isShipmentCreatedFromQuotation(quotationNo);
            baseDto.setResponseCode(ErrorCode.SUCCESS);
        } catch (RestException re) {
            log.error("RestException in isShipmentCreatedFromQuotation method of shipment service : "
                    + re);
            baseDto.setResponseCode(re.getMessage());
        } catch (Exception e) {
            log.error("Exception in isShipmentCreatedFromQuotation method of shipment service : " + e);
            baseDto.setResponseCode(ErrorCode.FAILED);
        }
        return baseDto;
    }


    /*
     * Method is used for giving service details based on mawb and service
     *
     *
     */
    public BaseDto getShipmentByMawbNo(String mawbNo, Long serviceId) {
        // TODO Auto-generated method stub
        BaseDto baseDto = new BaseDto();
        try {
            ValidateUtil.notNullOrEmpty(mawbNo, ErrorCode.CONSOL_MAWB_REQUIRED);
            ValidateUtil.notNull(serviceId, ErrorCode.CONSOL_SERVICE);
            List<ShipmentServiceDetail> service = shipmentServiceDetailRepository.findByMawbNo(mawbNo, serviceId);
            if (CollectionUtils.isNotEmpty(service)) {
                baseDto.setResponseObject(service.get(0));
                baseDto.setResponseCode(ErrorCode.SUCCESS);
            } else {
                throw new RestException(ErrorCode.SHIPMENT_NOT_FOUND);
            }
        } catch (RestException re) {
            log.error("RestException in getShipmentByMawbNo method of shipment service : "
                    + re);
            baseDto.setResponseCode(re.getMessage());
        } catch (Exception e) {
            log.error("Exception in getShipmentByMawbNo method of shipment service : " + e);
            baseDto.setResponseCode(ErrorCode.FAILED);
        }
        return baseDto;
    }

    public BaseDto pushSave(Shipment entity) {


        BaseDto baseDto = new BaseDto();

        try {
            shipmentRepository.save(entity);
            baseDto.setResponseCode(ErrorCode.SUCCESS);
        } catch (Exception e) {
            // TODO: handle exception
            baseDto.setResponseCode(ErrorCode.FAILED);
            log.info("exception in push api save", e);

        }
        return appUtil.setDesc(baseDto);
    }

    public BaseDto triggerMail(String mailContent,String shipmentUid){
        BaseDto baseDto = new BaseDto();

        try {
            shipmentMailer.sendTrackShipmentEmailToCustomer(mailContent,shipmentUid);
            baseDto.setResponseCode(ErrorCode.SUCCESS);
        } catch (Exception e) {
            // TODO: handle exception
            baseDto.setResponseCode(ErrorCode.FAILED);
            log.info("exception while sending tracking shipment mail", e);

        }
        return appUtil.setDesc(baseDto);
    }

}


