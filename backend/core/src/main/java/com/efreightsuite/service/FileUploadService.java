package com.efreightsuite.service;

import java.io.ByteArrayInputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Scanner;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.enumeration.ChargeType;
import com.efreightsuite.enumeration.LovStatus;
import com.efreightsuite.enumeration.TransportMode;
import com.efreightsuite.enumeration.WhoRouted;
import com.efreightsuite.enumeration.YesNo;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.model.CarrierMaster;
import com.efreightsuite.model.ChargeMaster;
import com.efreightsuite.model.CurrencyMaster;
import com.efreightsuite.model.PortMaster;
import com.efreightsuite.model.PricingCarrier;
import com.efreightsuite.model.PricingCarrierSlap;
import com.efreightsuite.model.PricingMaster;
import com.efreightsuite.model.PricingPortPair;
import com.efreightsuite.model.UnitMaster;
import com.efreightsuite.repository.CarrierMasterRepository;
import com.efreightsuite.repository.ChargeMasterRepository;
import com.efreightsuite.repository.CurrencyMasterRepository;
import com.efreightsuite.repository.PortMasterRepository;
import com.efreightsuite.repository.PricingMasterRepository;
import com.efreightsuite.repository.UnitMasterRepository;
import com.efreightsuite.util.CSVFileConverter;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.collections.map.MultiKeyMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class FileUploadService {

    @Autowired
    private
    PortMasterRepository portMasterRepository;

    @Autowired
    private
    ChargeMasterRepository chargeMasterRepository;

    @Autowired
    private
    UnitMasterRepository unitMasterRepository;

    @Autowired
    private
    CurrencyMasterRepository currencyMasterRepository;

    @Autowired
    private
    CarrierMasterRepository carrierMasterRepository;

    @Autowired
    private
    PricingMasterRepository pricingMasterRepository;

    private Map<String, PortMaster> portMap = new HashMap<>();
    private Map<String, ChargeMaster> chargeMap = new HashMap<>();
    private Map<String, UnitMaster> unitMap = new HashMap<>();
    private Map<String, CurrencyMaster> currencyMap = new HashMap<>();
    private Map<String, CarrierMaster> carrierMap = new HashMap<>();
    private MultiKeyMap multiKey = new MultiKeyMap();

    /*int cOrgin=0, cDestination=1, cTransit=2, cHazardous=3, cCharge=4, cUnit=5, cCurrency=6, cCarrier=7,
            cValidFrom=8, cValidTo=9, cMinSellPriceMin=10,cMinSellPriceAmt=11,cStdSellPriceMin=19,cStdSellPriceAmt=20,
            cCostMin=28,cCostAmt=29, ccMinSellPriceMinus45=12,ccMinSellPricePlus45=13,ccMinSellPricePlus100=14,
            ccMinSellPricePlus250=15,ccMinSellPricePlus300=16,ccMinSellPricePlus500=17,ccMinSellPricePlus1000=18,
            ccStdSellPriceMinus45=21, ccStdSellPricePlus45=22,ccStdSellPricePlus100=23, ccStdSellPricePlus250=24,
            ccStdSellPricePlus300=25,ccStdSellPricePlus500=26,ccStdSellPricePlus1000=27,ccCostMinus45=30,
            ccCostPlus45=31,ccCostPlus100=32, ccCostPlus250=33, ccCostPlus300=34,ccCostPlus500=35,
            ccCostPlus1000=36, ccFuelValidFrom=37, ccFuelValidTo=38, ccFuel=39, ccSecurityValidFrom=40,
            ccSecurityValidTo=41, ccSecurity=42;*/
    private final int totalColumn = 43;
    private final int cOrgin = 0;
    private final int cDestination = 1;
    private final int cTransit = 2;
    private final int cRouted = 3;
    private final int cCharge = 4;
    private final int cUnit = 5;
    private final int cCurrency = 6;
    private final int cCarrier = 7;
    private final int cValidFrom = 8;
    private final int cValidTo = 9;
    private final int cMinSellPriceMin = 10;
    private final int cMinSellPriceAmt = 11;
    private final int ccMinSellPriceMinus45 = 12;
    private final int ccMinSellPricePlus45 = 13;
    private final int ccMinSellPricePlus100 = 14;
    private final int ccMinSellPricePlus250 = 15;
    private final int ccMinSellPricePlus300 = 16;
    private final int ccMinSellPricePlus500 = 17;
    private final int ccMinSellPricePlus1000 = 18;
    private final int cStdSellPriceMin = 19;
    private final int cStdSellPriceAmt = 20;
    private final int ccStdSellPriceMinus45 = 21;
    private final int ccStdSellPricePlus45 = 22;
    private final int ccStdSellPricePlus100 = 23;
    private final int ccStdSellPricePlus250 = 24;
    private final int ccStdSellPricePlus300 = 25;
    private final int ccStdSellPricePlus500 = 26;
    private final int ccStdSellPricePlus1000 = 27;
    private final int cCostMin = 28;
    private final int cCostAmt = 29;
    private final int ccCostMinus45 = 30;
    private final int ccCostPlus45 = 31;
    private final int ccCostPlus100 = 32;
    private final int ccCostPlus250 = 33;
    private final int ccCostPlus300 = 34;
    private final int ccCostPlus500 = 35;
    private final int ccCostPlus1000 = 36;
    private final int ccFuelValidFrom = 37;
    private final int ccFuelValidTo = 38;
    private final int ccFuel = 39;
    private final int ccSecurityValidFrom = 40;
    private final int ccSecurityValidTo = 41;
    private final int ccSecurity = 42;

    public void download(Long id, HttpServletResponse response) {

        try {

            //StringBuilder bw = new StringBuilder("Origin Code,Destination Code,Transit Port Code,Hazardous,Charge Code,Unit Code,Currency Code,Carrier Code,Valid From,Valid To,Min.Sell.Price.Min ,Min.Sell.Price.Amt,Min.Sell.Price.-45,Min.Sell.Price.+45,Min.Sell.Price.+100,Min.Sell.Price.+250,Min.Sell.Price.+300,Min.Sell.Price.+500,Min.Sell.Price.+1000,Std.Sell.Price.Min,Std.Sell.Price.Amt,Std.Sell.Price.-45,Std.Sell.Price.+45,Std.Sell.Price.+100,Std.Sell.Price.+250,Std.Sell.Price.+300,Std.Sell.Price.+500,Std.Sell.Price.+1000,Cost.Min,Cost.Amt,Cost.-45,Cost.+45,Cost.+100,Cost.+250,Cost.+300,Cost.+500,Cost.+1000,Fuel.Valid.From,Fuel.Valid.To,Fuel Surcharge,Security.Valid.From,Security.Valid.To,Security Surcharge");
            StringBuilder bw = new StringBuilder("Origin Code,Destination Code,Transit Port Code,Routed,Charge Code,Unit Code,Currency Code,Carrier Code,Valid From,Valid To,Min.Sell.Price.Min ,Min.Sell.Price.Amt,Min.Sell.Price.-45,Min.Sell.Price.+45,Min.Sell.Price.+100,Min.Sell.Price.+250,Min.Sell.Price.+300,Min.Sell.Price.+500,Min.Sell.Price.+1000,Std.Sell.Price.Min,Std.Sell.Price.Amt,Std.Sell.Price.-45,Std.Sell.Price.+45,Std.Sell.Price.+100,Std.Sell.Price.+250,Std.Sell.Price.+300,Std.Sell.Price.+500,Std.Sell.Price.+1000,Cost.Min,Cost.Amt,Cost.-45,Cost.+45,Cost.+100,Cost.+250,Cost.+300,Cost.+500,Cost.+1000,Fuel.Valid.From,Fuel.Valid.To,Fuel Surcharge,Security.Valid.From,Security.Valid.To,Security Surcharge");
            String fileName = "air_pricing_template.csv";

            if (id != -1L) {
                PricingMaster pricingMaster = pricingMasterRepository.getOne(id);

                for (PricingPortPair pp : pricingMaster.getGeneralOriginPriceList()) {
                    writeLine(bw, pricingMaster, pp);
                }

                for (PricingPortPair pp : pricingMaster.getGeneralDestinationPriceList()) {
                    writeLine(bw, pricingMaster, pp);
                }

                for (PricingCarrier pp : pricingMaster.getGeneralFreightPriceList()) {
                    writeLine(bw, pricingMaster, pp);
                }

		        /*for(PricingPortPair pp : pricingMaster.getHazardousOriginPriceList()){
		        	writeLine(bw, pricingMaster, pp);
		        }
		        
		        for(PricingPortPair pp : pricingMaster.getHazardousDestinationPriceList()){
		        	writeLine(bw, pricingMaster, pp);
		        }
		        
		        for(PricingCarrier pp : pricingMaster.getHazardousFreightPriceList()){
		        	writeLine(bw, pricingMaster, pp);
		        }*/

                fileName = pricingMaster.getOrigin().getPortCode() + "_" + pricingMaster.getDestination().getPortCode() + (pricingMaster.getTransit() == null ? "" : ("_" + pricingMaster.getTransit().getPortCode())) + ".csv";
            }


            response.setContentType("text/csv");

            response.setHeader("Content-Disposition", "attachment; filename= " + fileName);

            response.setContentLength(String.valueOf(bw).getBytes().length);

            ServletOutputStream servletOutputStream = response.getOutputStream();

            servletOutputStream.write(String.valueOf(bw).getBytes());

            servletOutputStream.flush();

            servletOutputStream.close();

        } catch (Exception exception) {
            log.error("Error occured during download attachment file : " + id + ", ", exception);
        }

    }

    @SuppressWarnings("resource")
    public List<PricingMaster> getAirPricing(BaseDto baseDto, byte[] file, Long pricingMasterId) {

        clearMap();

        Scanner scanner = new Scanner(new ByteArrayInputStream(file));

        int row = 0;


        PricingMaster oldPricingMaster = null;

        if (pricingMasterId != null && pricingMasterId != -1L) {
            oldPricingMaster = pricingMasterRepository.findById(pricingMasterId);
        }


        while (scanner.hasNext()) {

            List<String> record = CSVFileConverter.parseLine(scanner.nextLine());

            if (record.size() != totalColumn) {
                throwError(baseDto, row, record.size(), totalColumn + "", ErrorCode.CSV_MISMATCH_RECORD_INVALID);
            }

            if (row == 0) {
                row++;
                continue;
            }

            //get By Origin
            PortMaster origin = getPort(record, row, cOrgin, baseDto, false);

            //get By Destination
            PortMaster destination = getPort(record, row, cDestination, baseDto, false);

            //get By Transit
            PortMaster transit = getPort(record, row, cTransit, baseDto, false);

            if (oldPricingMaster != null) {
                if (!Objects.equals(oldPricingMaster.getOrigin().getId(), origin.getId())) {
                    throwError(baseDto, row, cOrgin, origin.getPortCode(), ErrorCode.CSV_DATA_INVAILD_PRICING);
                }

                if (!Objects.equals(oldPricingMaster.getDestination().getId(), destination.getId())) {
                    throwError(baseDto, row, cOrgin, destination.getPortCode(), ErrorCode.CSV_DATA_INVAILD_PRICING);
                }

                if (oldPricingMaster.getTransit() == null && transit != null) {
                    throwError(baseDto, row, cOrgin, transit.getPortCode(), ErrorCode.CSV_DATA_INVAILD_PRICING);
                } else if (oldPricingMaster.getTransit() != null && transit == null) {
                    throwError(baseDto, row, cOrgin, oldPricingMaster.getTransit().getPortCode(), ErrorCode.CSV_DATA_INVAILD_PRICING);
                } else if (oldPricingMaster.getTransit() == null && transit == null) {
                    //No error
                } else if (!Objects.equals(oldPricingMaster.getTransit().getId(), transit.getId())) {
                    throwError(baseDto, row, cOrgin, transit.getPortCode(), ErrorCode.CSV_DATA_INVAILD_PRICING);
                }
            }

            WhoRouted whoRouted = getWhoRouted(record, row, cRouted, baseDto, true);

            //get By PricingMaster
            PricingMaster pricingMaster = getPricingMaster(origin, destination, transit, whoRouted, TransportMode.Air);
			
			/*YesNo hazardous = isHazardous(record, row, cHazardous, baseDto);*/
            YesNo hazardous = YesNo.No;

            ChargeMaster chargeMaser = getCharge(record, row, cCharge, baseDto, true);

            UnitMaster unitMaster = getUnit(record, row, cUnit, baseDto, true);

            CurrencyMaster currencyMaster = getCurrency(record, row, cCurrency, baseDto, true);

            CarrierMaster carrierMaster = null;
            if (chargeMaser.getChargeType() == ChargeType.Freight) {
                carrierMaster = getCarrier(record, row, cCarrier, baseDto, true);
            }

            Date validFrom = getDate(record, row, cValidFrom, baseDto, true, true);

            Date validTo = getDate(record, row, cValidTo, baseDto, true, false);


            PricingPortPair portPair = null;
            PricingCarrier carrierPrice = null;

            if (hazardous == YesNo.No && chargeMaser.getChargeType() == ChargeType.Origin) {
                portPair = getPricingPortPair(pricingMaster.getGOriginMap(), chargeMaser.getChargeCode(), validFrom, validTo, baseDto, row, cValidFrom);
            } else if (hazardous == YesNo.No && chargeMaser.getChargeType() == ChargeType.Destination) {
                portPair = getPricingPortPair(pricingMaster.getGDestinationMap(), chargeMaser.getChargeCode(), validFrom, validTo, baseDto, row, cValidFrom);
            } else if (hazardous == YesNo.No && chargeMaser.getChargeType() == ChargeType.Freight) {
                String key = carrierMaster.getCarrierCode() + "#" + chargeMaser.getChargeCode();
                carrierPrice = getPricingCarrier(pricingMaster.getGFreightMap(), key, validFrom, validTo, baseDto, row, cValidFrom);
            } else if (hazardous == YesNo.Yes && chargeMaser.getChargeType() == ChargeType.Origin) {
                portPair = getPricingPortPair(pricingMaster.getHOriginMap(), chargeMaser.getChargeCode(), validFrom, validTo, baseDto, row, cValidFrom);
            } else if (hazardous == YesNo.Yes && chargeMaser.getChargeType() == ChargeType.Destination) {
                portPair = getPricingPortPair(pricingMaster.getHDestinationMap(), chargeMaser.getChargeCode(), validFrom, validTo, baseDto, row, cValidFrom);
            } else if (hazardous == YesNo.Yes && chargeMaser.getChargeType() == ChargeType.Freight) {
                String key = carrierMaster.getCarrierCode() + "#" + chargeMaser.getChargeCode();
                carrierPrice = getPricingCarrier(pricingMaster.getHFreightMap(), key, validFrom, validTo, baseDto, row, cValidFrom);
            }

            if (chargeMaser.getChargeType() == ChargeType.Freight) {

                if (carrierPrice == null) {
                    row++;
                    continue;
                }

                carrierPrice.setHazardous(hazardous);
                carrierPrice.setChargeMaster(chargeMaser);
                carrierPrice.setUnitMaster(unitMaster);
                carrierPrice.setCurrencyMaster(currencyMaster);
                carrierPrice.setCarrierMaster(carrierMaster);
                carrierPrice.setValidFrom(validFrom);
                carrierPrice.setValidTo(validTo);

                if (carrierPrice.getMinimumSellPrice() == null) {
                    carrierPrice.setMinimumSellPrice(new PricingCarrierSlap());
                }
                carrierPrice.getMinimumSellPrice().setMinimum(getDouble(record, row, cMinSellPriceMin, baseDto, false));
                carrierPrice.getMinimumSellPrice().setMinus45(getDouble(record, row, ccMinSellPriceMinus45, baseDto, false));
                carrierPrice.getMinimumSellPrice().setPlus45(getDouble(record, row, ccMinSellPricePlus45, baseDto, false));
                carrierPrice.getMinimumSellPrice().setPlus100(getDouble(record, row, ccMinSellPricePlus100, baseDto, false));
                carrierPrice.getMinimumSellPrice().setPlus250(getDouble(record, row, ccMinSellPricePlus250, baseDto, false));
                carrierPrice.getMinimumSellPrice().setPlus300(getDouble(record, row, ccMinSellPricePlus300, baseDto, false));
                carrierPrice.getMinimumSellPrice().setPlus500(getDouble(record, row, ccMinSellPricePlus500, baseDto, false));
                carrierPrice.getMinimumSellPrice().setPlus1000(getDouble(record, row, ccMinSellPricePlus1000, baseDto, false));

                if (carrierPrice.getStandardSellPrice() == null) {
                    carrierPrice.setStandardSellPrice(new PricingCarrierSlap());
                }
                carrierPrice.getStandardSellPrice().setMinimum(getDouble(record, row, cStdSellPriceMin, baseDto, false));
                carrierPrice.getStandardSellPrice().setMinus45(getDouble(record, row, ccStdSellPriceMinus45, baseDto, false));
                carrierPrice.getStandardSellPrice().setPlus45(getDouble(record, row, ccStdSellPricePlus45, baseDto, false));
                carrierPrice.getStandardSellPrice().setPlus100(getDouble(record, row, ccStdSellPricePlus100, baseDto, false));
                carrierPrice.getStandardSellPrice().setPlus250(getDouble(record, row, ccStdSellPricePlus250, baseDto, false));
                carrierPrice.getStandardSellPrice().setPlus300(getDouble(record, row, ccStdSellPricePlus300, baseDto, false));
                carrierPrice.getStandardSellPrice().setPlus500(getDouble(record, row, ccStdSellPricePlus500, baseDto, false));
                carrierPrice.getStandardSellPrice().setPlus1000(getDouble(record, row, ccStdSellPricePlus1000, baseDto, false));


                if (carrierPrice.getCost() == null) {
                    carrierPrice.setCost(new PricingCarrierSlap());
                }
                carrierPrice.getCost().setMinimum(getDouble(record, row, cCostMin, baseDto, false));
                carrierPrice.getCost().setMinus45(getDouble(record, row, ccCostMinus45, baseDto, false));
                carrierPrice.getCost().setPlus45(getDouble(record, row, ccCostPlus45, baseDto, false));
                carrierPrice.getCost().setPlus100(getDouble(record, row, ccCostPlus100, baseDto, false));
                carrierPrice.getCost().setPlus250(getDouble(record, row, ccCostPlus250, baseDto, false));
                carrierPrice.getCost().setPlus300(getDouble(record, row, ccCostPlus300, baseDto, false));
                carrierPrice.getCost().setPlus500(getDouble(record, row, ccCostPlus500, baseDto, false));
                carrierPrice.getCost().setPlus1000(getDouble(record, row, ccCostPlus1000, baseDto, false));

                carrierPrice.setFuelValidFrom(getDate(record, row, ccFuelValidFrom, baseDto, false, true));
                carrierPrice.setFuelValidTo(getDate(record, row, ccFuelValidTo, baseDto, false, false));
                carrierPrice.setFuelSurcharge(getDouble(record, row, ccFuel, baseDto, false));

                carrierPrice.setSecurityChargeValidFrom(getDate(record, row, ccSecurityValidFrom, baseDto, false, true));
                carrierPrice.setSecurityChargeValidTo(getDate(record, row, ccSecurityValidTo, baseDto, false, false));
                carrierPrice.setSecuritySurcharge(getDouble(record, row, ccSecurity, baseDto, false));


            } else if (chargeMaser.getChargeType() == ChargeType.Origin || chargeMaser.getChargeType() == ChargeType.Destination) {

                if (portPair == null) {
                    row++;
                    continue;
                }

                portPair.setHazardous(hazardous);
                portPair.setChargeMaster(chargeMaser);
                portPair.setUnitMaster(unitMaster);
                portPair.setCurrencyMaster(currencyMaster);
                portPair.setValidFromDate(validFrom);
                portPair.setValidToDate(validTo);

                portPair.setMinSelInMinimum(getDouble(record, row, cMinSellPriceMin, baseDto, false));
                portPair.setMinSelInAmount(getDouble(record, row, cMinSellPriceAmt, baseDto, false));

                portPair.setStdSelInMinimum(getDouble(record, row, cStdSellPriceMin, baseDto, true));
                portPair.setStdSelInAmount(getDouble(record, row, cStdSellPriceAmt, baseDto, true));

                portPair.setCostInMinimum(getDouble(record, row, cCostMin, baseDto, false));
                portPair.setCostInAmount(getDouble(record, row, cCostAmt, baseDto, false));
            }


            row = row + 1;
        }

        scanner.close();

        List<PricingMaster> list = new ArrayList<>();

        for (Object obj : multiKey.values()) {
            list.add((PricingMaster) obj);
        }

        for (PricingMaster pp : list) {

            pp.getGeneralOriginPriceList().clear();
            for (Object obj : pp.getGOriginMap().values()) {
                List<PricingPortPair> tmpList = (List<PricingPortPair>) obj;
                pp.getGeneralOriginPriceList().addAll(tmpList);
            }

            pp.getGeneralDestinationPriceList().clear();
            for (Object obj : pp.getGDestinationMap().values()) {
                List<PricingPortPair> tmpList = (List<PricingPortPair>) obj;
                pp.getGeneralDestinationPriceList().addAll(tmpList);
            }

            pp.getGeneralFreightPriceList().clear();
            for (Object obj : pp.getGFreightMap().values()) {
                List<PricingCarrier> tmpList = (List<PricingCarrier>) obj;
                pp.getGeneralFreightPriceList().addAll(tmpList);
            }

            pp.getHazardousOriginPriceList().clear();
            for (Object obj : pp.getHOriginMap().values()) {
                List<PricingPortPair> tmpList = (List<PricingPortPair>) obj;
                pp.getHazardousOriginPriceList().addAll(tmpList);
            }

            pp.getHazardousDestinationPriceList().clear();
            for (Object obj : pp.getHDestinationMap().values()) {
                List<PricingPortPair> tmpList = (List<PricingPortPair>) obj;
                pp.getHazardousDestinationPriceList().addAll(tmpList);
            }

            pp.getHazardousFreightPriceList().clear();
            for (Object obj : pp.getHFreightMap().values()) {
                List<PricingCarrier> tmpList = (List<PricingCarrier>) obj;
                pp.getHazardousFreightPriceList().addAll(tmpList);
            }
        }

        return list;
    }

    private boolean rangeCheck(Date testDate, Date startDate, Date endDate) {

        return testDate.after(startDate) && testDate.before(endDate);
    }

    private PricingPortPair getPricingPortPair(Map<String, List<PricingPortPair>> map, String chargeCode, Date validFrom, Date validTo, BaseDto baseDto, int row, int col) {

        if (map.get(chargeCode) != null) {

            boolean isFound = false;

            for (PricingPortPair pp : map.get(chargeCode)) {

                if (validFrom.equals(pp.getValidFromDate()) && validTo.equals(pp.getValidToDate())) {
                    return pp;
                }

                if (rangeCheck(validFrom, pp.getValidFromDate(), pp.getValidToDate())) {
                    isFound = true;
                    break;
                }

                if (rangeCheck(validTo, pp.getValidFromDate(), pp.getValidToDate())) {
                    isFound = true;
                    break;
                }
            }

            if (isFound) {
                SimpleDateFormat sdf = new SimpleDateFormat(AuthService.getCurrentUser().getSelectedUserLocation().getJavaDateFormat());
                String errorMsg = sdf.format(validFrom) + ", " + sdf.format(validTo);
                throwError(baseDto, row, col, errorMsg, ErrorCode.CSV_DATA_WIHT_IN_DATE_RANGE);
            } else {
                List<PricingPortPair> tmpList = map.get(chargeCode);
                tmpList.add(new PricingPortPair());
                map.put(chargeCode, tmpList);
                return tmpList.get(tmpList.size() - 1);
            }

        } else {
            List<PricingPortPair> tmpList = new ArrayList<>();
            tmpList.add(new PricingPortPair());
            map.put(chargeCode, tmpList);
            return tmpList.get(0);
        }

        return null;
    }

    private PricingCarrier getPricingCarrier(Map<String, List<PricingCarrier>> map, String key, Date validFrom, Date validTo, BaseDto baseDto, int row, int col) {

        if (map.get(key) != null) {

            boolean isFound = false;
            for (PricingCarrier pp : map.get(key)) {

                if (validFrom.equals(pp.getValidFrom()) && validTo.equals(pp.getValidTo())) {
                    return pp;
                }

                if (rangeCheck(validFrom, pp.getValidFrom(), pp.getValidTo())) {
                    isFound = true;
                    break;
                }

                if (rangeCheck(validTo, pp.getValidFrom(), pp.getValidTo())) {
                    isFound = true;
                    break;
                }
            }

            if (isFound) {
                SimpleDateFormat sdf = new SimpleDateFormat(AuthService.getCurrentUser().getSelectedUserLocation().getJavaDateFormat());
                String errorMsg = sdf.format(validFrom) + ", " + sdf.format(validTo);
                throwError(baseDto, row, col, errorMsg, ErrorCode.CSV_DATA_WIHT_IN_DATE_RANGE);
            } else {
                List<PricingCarrier> tmpList = map.get(key);
                tmpList.add(new PricingCarrier());
                map.put(key, tmpList);
                return tmpList.get(tmpList.size() - 1);
            }

        } else {
            List<PricingCarrier> tmpList = new ArrayList<>();
            tmpList.add(new PricingCarrier());
            map.put(key, tmpList);
            return tmpList.get(0);
        }
        return null;
    }

    private Double getDouble(List<String> record, int row, int col, BaseDto baseDto, boolean isMandatory) {

        String value = record.get(col);
        if (value == null || value.trim().length() == 0) {
            if (!isMandatory) {
                return null;
            }
            throwError(baseDto, row, col, value, ErrorCode.CSV_FILE_INVALID);
        } else {
            value = value.toUpperCase();
        }

        Double doubleVal = null;
        try {
            doubleVal = Double.parseDouble(value);
        } catch (Exception e) {
            log.error("Double value conversion Error, ", e);
            throwError(baseDto, row, col, value, ErrorCode.CSV_DOUBLE_VAL_CONVERSION);
        }
        return doubleVal;
    }

    private Date getDate(List<String> record, int row, int col, BaseDto baseDto, boolean isMandatory, boolean isStartTime) {

        String value = record.get(col);
        if (value == null || value.trim().length() == 0) {
            if (!isMandatory) {
                return null;
            }
            throwError(baseDto, row, col, value, ErrorCode.CSV_FILE_INVALID);
        } else {
            value = value.toUpperCase();
        }

        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        Date dt = null;
        try {
            dt = sdf.parse(value);
            if (isStartTime) {
                dt.setHours(0);
                dt.setMinutes(0);
                dt.setSeconds(0);
            } else {
                dt.setHours(23);
                dt.setMinutes(59);
                dt.setSeconds(59);
            }
        } catch (Exception e) {
            log.error("Date conversion Error, ", e);
            throwError(baseDto, row, col, value, ErrorCode.CSV_DATE_CONVERSION);
        }


        return dt;
    }

    private CarrierMaster getCarrier(List<String> record, int row, int col, BaseDto baseDto, boolean isMandatory) {

        String value = record.get(col);
        if (value == null || value.trim().length() == 0) {
            if (!isMandatory) {
                return null;
            }
            throwError(baseDto, row, col, value, ErrorCode.CSV_FILE_INVALID);
        } else {
            value = value.toUpperCase();
        }

        if (carrierMap.get(value) == null) {
            CarrierMaster object = carrierMasterRepository.findByCarrierCode(value);
            if (object == null) {
                throwError(baseDto, row, col, value, ErrorCode.CSV_CARRIER_IS_NOT_FOUND);
            } else {
                if (object.getStatus() == LovStatus.Block) {
                    throwError(baseDto, row, col, value, ErrorCode.CSV_CARRIER_BLOCKED);
                } else if (object.getStatus() == LovStatus.Hide) {
                    throwError(baseDto, row, col, value, ErrorCode.CSV_CARRIER_HIDDEN);
                } else {
                    carrierMap.put(value, object);
                }
            }
        }

        return carrierMap.get(value);
    }

    private CurrencyMaster getCurrency(List<String> record, int row, int col, BaseDto baseDto, boolean isMandatory) {

        String value = record.get(col);
        if (value == null || value.trim().length() == 0) {
            if (!isMandatory) {
                return null;
            }
            throwError(baseDto, row, col, value, ErrorCode.CSV_FILE_INVALID);
        } else {
            value = value.toUpperCase();
        }

        if (currencyMap.get(value) == null) {
            CurrencyMaster object = currencyMasterRepository.findByCurrencyCode(value);
            if (object == null) {
                throwError(baseDto, row, col, value, ErrorCode.CSV_CURRENCY_IS_NOT_FOUND);
            } else {
                if (object.getStatus() == LovStatus.Block) {
                    throwError(baseDto, row, col, value, ErrorCode.CSV_CURRENCY_BLOCKED);
                } else if (object.getStatus() == LovStatus.Hide) {
                    throwError(baseDto, row, col, value, ErrorCode.CSV_CURRENCY_HIDDEN);
                } else {
                    currencyMap.put(value, object);
                }
            }
        }

        return currencyMap.get(value);
    }

    private UnitMaster getUnit(List<String> record, int row, int col, BaseDto baseDto, boolean isMandatory) {

        String value = record.get(col);
        if (value == null || value.trim().length() == 0) {
            if (!isMandatory) {
                return null;
            }
            throwError(baseDto, row, col, value, ErrorCode.CSV_FILE_INVALID);
        } else {
            value = value.toUpperCase();
        }

        if (unitMap.get(value) == null) {
            UnitMaster object = unitMasterRepository.findByUnitCode(value);
            if (object == null) {
                throwError(baseDto, row, col, value, ErrorCode.CSV_UNIT_IS_NOT_FOUND);
            } else {
                if (object.getStatus() == LovStatus.Block) {
                    throwError(baseDto, row, col, value, ErrorCode.CSV_UNIT_BLOCKED);
                } else if (object.getStatus() == LovStatus.Hide) {
                    throwError(baseDto, row, col, value, ErrorCode.CSV_UNIT_HIDDEN);
                } else {
                    unitMap.put(value, object);
                }
            }
        }

        return unitMap.get(value);
    }

    private ChargeMaster getCharge(List<String> record, int row, int col, BaseDto baseDto, boolean isMandatory) {

        String value = record.get(col);
        if (value == null || value.trim().length() == 0) {
            if (!isMandatory) {
                return null;
            }
            throwError(baseDto, row, col, value, ErrorCode.CSV_FILE_INVALID);
        } else {
            value = value.toUpperCase();
        }

        if (chargeMap.get(value) == null) {
            ChargeMaster object = chargeMasterRepository.findByChargeCode(value);
            if (object == null) {
                throwError(baseDto, row, col, value, ErrorCode.CSV_CHARGE_IS_NOT_FOUND);
            } else {
                if (object.getStatus() == LovStatus.Block) {
                    throwError(baseDto, row, col, value, ErrorCode.CSV_CHARGE_BLOCKED);
                } else if (object.getStatus() == LovStatus.Hide) {
                    throwError(baseDto, row, col, value, ErrorCode.CSV_CHARGE_HIDDEN);
                } else {
                    chargeMap.put(value, object);
                }
            }
        }

        return chargeMap.get(value);
    }

    private PortMaster getPort(List<String> record, int row, int col, BaseDto baseDto, boolean isMandatory) {

        String value = record.get(col);
        if (value == null || value.trim().length() == 0) {
            if (!isMandatory) {
                return null;
            }
            throwError(baseDto, row, col, value, ErrorCode.CSV_FILE_INVALID);
        } else {
            value = value.toUpperCase();
        }

        if (portMap.get(value) == null) {
            PortMaster object = portMasterRepository.findByPortCode(value);
            if (object == null) {
                throwError(baseDto, row, col, value, ErrorCode.CSV_PORT_IS_NOT_FOUND);
            } else {
                if (object.getStatus() == LovStatus.Block) {
                    throwError(baseDto, row, col, value, ErrorCode.CSV_PORT_BLOCKED);
                } else if (object.getStatus() == LovStatus.Hide) {
                    throwError(baseDto, row, col, value, ErrorCode.CSV_PORT_HIDDEN);
                } else {
                    portMap.put(value, object);
                }
            }
        }

        return portMap.get(value);
    }

    private WhoRouted getWhoRouted(List<String> record, int row, int col, BaseDto baseDto, boolean isMandatory) {

        WhoRouted whoRouted = WhoRouted.Self;
        String value = record.get(col);
        if (value == null || value.trim().length() == 0) {
            if (!isMandatory) {
                return null;
            }
            throwError(baseDto, row, col, value, ErrorCode.CSV_FILE_INVALID);
        } else {
            value = value.toUpperCase();
            if (value.equals(WhoRouted.Agent.toString().toUpperCase())) {
                whoRouted = WhoRouted.Agent;
            }
        }
        return whoRouted;
    }

    private PricingMaster getPricingMaster(PortMaster origin, PortMaster destination, PortMaster transit, WhoRouted whoRouted, TransportMode transportMode) {
        if (multiKey.get(origin, destination, transit) == null) {
            PricingMaster object = null;
            if (transit == null) {
                object = pricingMasterRepository.getByOriginAndDestAndRoutedAndTrasitIsNullAndTransportMode(origin.getPortCode(), destination.getPortCode(), whoRouted, transportMode);
            } else {
                object = pricingMasterRepository.getByOriginAndDestAndTrasitAndRoutedAndTransportMode(origin.getPortCode(), destination.getPortCode(), transit.getPortCode(), whoRouted, transportMode);
            }
            if (object == null) {
                object = new PricingMaster();
                object.setOrigin(origin);
                object.setDestination(destination);
                object.setTransit(transit);
                object.setWhoRouted(whoRouted);
            }

            object.loadMap();

            multiKey.put(origin, destination, transit, object);
        }

        return (PricingMaster) multiKey.get(origin, destination, transit);
    }

    private void clearMap() {
        portMap = new HashMap<>();
        chargeMap = new HashMap<>();
        unitMap = new HashMap<>();
        currencyMap = new HashMap<>();
        carrierMap = new HashMap<>();
        multiKey = new MultiKeyMap();
    }


    private void throwError(BaseDto baseDto, int row, int col, String value, String errorCode) {
        baseDto.getParams().add(value);
        baseDto.getParams().add((row + 1) + "");
        baseDto.getParams().add((col + 1) + "");
        throw new RestException(errorCode);
    }


    private void writeLine(StringBuilder writer, PricingMaster pricingMaster, PricingPortPair pp) {

        SimpleDateFormat sdf = new SimpleDateFormat(AuthService.getCurrentUser().getSelectedUserLocation().getJavaDateFormat());
        try {

            writer.append("\n");
            writer.append(pricingMaster.getOrigin().getPortCode() + "," +
                    pricingMaster.getDestination().getPortCode() + "," +
                    (pricingMaster.getTransit() == null ? "" : pricingMaster.getTransit().getPortCode()) + "," +
				/*pp.getHazardous()+","+*/
                    pricingMaster.getWhoRouted() + "," +
                    (pp.getChargeMaster() == null ? "" : pp.getChargeMaster().getChargeCode()) + "," +
                    (pp.getUnitMaster() == null ? "" : pp.getUnitMaster().getUnitCode()) + "," +
                    (pp.getCurrencyMaster() == null ? "" : pp.getCurrencyMaster().getCurrencyCode()) + "," +
                    "" + "," +
                    sdf.format(pp.getValidFromDate()) + "," +
                    sdf.format(pp.getValidToDate()) + "," +
                    (pp.getMinSelInMinimum() == null ? "" : pp.getMinSelInMinimum()) + "," +
                    (pp.getMinSelInAmount() == null ? "" : pp.getMinSelInAmount()) + "," +
                    "" + "," +
                    "" + "," +
                    "" + "," +
                    "" + "," +
                    "" + "," +
                    "" + "," +
                    "" + "," +
                    (pp.getStdSelInMinimum() == null ? "" : pp.getStdSelInMinimum()) + "," +
                    (pp.getStdSelInAmount() == null ? "" : pp.getStdSelInAmount()) + "," +
                    "" + "," +
                    "" + "," +
                    "" + "," +
                    "" + "," +
                    "" + "," +
                    "" + "," +
                    "" + "," +
                    (pp.getCostInMinimum() == null ? "" : pp.getCostInMinimum()) + "," +
                    (pp.getCostInAmount() == null ? "" : pp.getCostInAmount()) + "," +
                    "" + "," +
                    "" + "," +
                    "" + "," +
                    "" + "," +
                    "" + "," +
                    "" + "," +
                    "" + "," +
                    "" + "," +
                    "" + "," +
                    "" + "," +
                    "" + "," +
                    "" + "," +
                    "");

        } catch (Exception e) {
            log.error("Error occur during download template :", e);
        }
    }

    private void writeLine(StringBuilder writer, PricingMaster pricingMaster, PricingCarrier pp) {

        SimpleDateFormat sdf = new SimpleDateFormat(AuthService.getCurrentUser().getSelectedUserLocation().getJavaDateFormat());
        try {
            writer.append("\n");
            writer.append(pricingMaster.getOrigin().getPortCode() + "," +
                    pricingMaster.getDestination().getPortCode() + "," +
                    (pricingMaster.getTransit() == null ? "" : pricingMaster.getTransit().getPortCode()) + "," +
    			/*pp.getHazardous()+","+*/
                    pricingMaster.getWhoRouted() + "," +
                    (pp.getChargeMaster() == null ? "" : pp.getChargeMaster().getChargeCode()) + "," +
                    (pp.getUnitMaster() == null ? "" : pp.getUnitMaster().getUnitCode()) + "," +
                    (pp.getCurrencyMaster() == null ? "" : pp.getCurrencyMaster().getCurrencyCode()) + "," +
                    (pp.getCarrierMaster() == null ? "" : pp.getCarrierMaster().getCarrierCode()) + "," +
                    sdf.format(pp.getValidFrom()) + "," +
                    sdf.format(pp.getValidTo()) + "," +
                    (pp.getMinimumSellPrice().getMinimum() == null ? "" : pp.getMinimumSellPrice().getMinimum()) + "," +
                    "" + "," +
                    (pp.getMinimumSellPrice().getMinus45() == null ? "" : pp.getMinimumSellPrice().getMinus45()) + "," +
                    (pp.getMinimumSellPrice().getPlus45() == null ? "" : pp.getMinimumSellPrice().getPlus45()) + "," +
                    (pp.getMinimumSellPrice().getPlus100() == null ? "" : pp.getMinimumSellPrice().getPlus100()) + "," +
                    (pp.getMinimumSellPrice().getPlus250() == null ? "" : pp.getMinimumSellPrice().getPlus250()) + "," +
                    (pp.getMinimumSellPrice().getPlus300() == null ? "" : pp.getMinimumSellPrice().getPlus300()) + "," +
                    (pp.getMinimumSellPrice().getPlus500() == null ? "" : pp.getMinimumSellPrice().getPlus500()) + "," +
                    (pp.getMinimumSellPrice().getPlus1000() == null ? "" : pp.getMinimumSellPrice().getPlus1000()) + "," +
                    (pp.getStandardSellPrice().getMinimum() == null ? "" : pp.getStandardSellPrice().getMinimum()) + "," +
                    "" + "," +
                    (pp.getStandardSellPrice().getMinus45() == null ? "" : pp.getStandardSellPrice().getMinus45()) + "," +
                    (pp.getStandardSellPrice().getPlus45() == null ? "" : pp.getStandardSellPrice().getPlus45()) + "," +
                    (pp.getStandardSellPrice().getPlus100() == null ? "" : pp.getStandardSellPrice().getPlus100()) + "," +
                    (pp.getStandardSellPrice().getPlus250() == null ? "" : pp.getStandardSellPrice().getPlus250()) + "," +
                    (pp.getStandardSellPrice().getPlus300() == null ? "" : pp.getStandardSellPrice().getPlus300()) + "," +
                    (pp.getStandardSellPrice().getPlus500() == null ? "" : pp.getStandardSellPrice().getPlus500()) + "," +
                    (pp.getStandardSellPrice().getPlus1000() == null ? "" : pp.getStandardSellPrice().getPlus1000()) + "," +
                    (pp.getCost().getMinimum() == null ? "" : pp.getCost().getMinimum()) + "," +
                    "" + "," +
                    (pp.getCost().getMinus45() == null ? "" : pp.getCost().getMinus45()) + "," +
                    (pp.getCost().getPlus45() == null ? "" : pp.getCost().getPlus45()) + "," +
                    (pp.getCost().getPlus100() == null ? "" : pp.getCost().getPlus100()) + "," +
                    (pp.getCost().getPlus250() == null ? "" : pp.getCost().getPlus250()) + "," +
                    (pp.getCost().getPlus300() == null ? "" : pp.getCost().getPlus300()) + "," +
                    (pp.getCost().getPlus500() == null ? "" : pp.getCost().getPlus500()) + "," +
                    (pp.getCost().getPlus1000() == null ? "" : pp.getCost().getPlus1000()) + "," +
                    (pp.getFuelValidFrom() == null ? "" : sdf.format(pp.getFuelValidFrom())) + "," +
                    (pp.getFuelValidTo() == null ? "" : sdf.format(pp.getFuelValidTo())) + "," +
                    (pp.getFuelSurcharge() == null ? "" : pp.getFuelSurcharge()) + "," +
                    (pp.getSecurityChargeValidFrom() == null ? "" : sdf.format(pp.getSecurityChargeValidFrom())) + "," +
                    (pp.getSecurityChargeValidTo() == null ? "" : sdf.format(pp.getSecurityChargeValidTo())) + "," +
                    (pp.getSecuritySurcharge() == null ? "" : pp.getSecuritySurcharge()));
        } catch (Exception e) {
            log.error("Error occur during download template :", e);
        }
    }
}
