package com.efreightsuite.service;

import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.LocationSearchDto;
import com.efreightsuite.dto.SearchRequest;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.ExceptionHelper;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.exception.UniqueKey;
import com.efreightsuite.model.LocationMaster;
import com.efreightsuite.repository.LocationMasterRepository;
import com.efreightsuite.search.CommonSearchImpl;
import com.efreightsuite.search.LocationSearchService;
import com.efreightsuite.util.AppUtil;
import com.efreightsuite.util.TimeUtil;
import com.efreightsuite.util.ValidateUtil;
import com.efreightsuite.validation.LocationMasterValidator;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.exception.ExceptionUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.orm.ObjectOptimisticLockingFailureException;
import org.springframework.orm.jpa.JpaObjectRetrievalFailureException;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class LocationMasterService {

    @Autowired
    private
    AppUtil appUtil;

    @Autowired
    private
    LocationMasterRepository locationMasterRepository;

    @Autowired
    private
    LocationSearchService locationSearchService;

    @Autowired
    private
    LocationMasterValidator locationMasterValidator;

    @Autowired
    private
    CommonSearchImpl commonSearchImpl;

    public BaseDto getTimeZoneList() {

        BaseDto baseDto = new BaseDto();

        baseDto.setResponseCode(ErrorCode.SUCCESS);

        baseDto.setResponseObject(TimeUtil.getAllTimeZones());

        return baseDto;
    }

    public BaseDto search(SearchRequest searchRequest) {

        log.info("getAll method is called....");

        BaseDto baseDto = new BaseDto();

        try {

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(locationSearchService.search(searchRequest, null));

        } catch (RestException exception) {

            log.error("Exception in getAll method ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (Exception exception) {

            log.error("Exception in getAll method ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);

        }

        return appUtil.setDesc(baseDto);
    }


    public BaseDto searchLocationNotInList(SearchRequest searchRequest) {

        log.info("searchLocationNotInList method is called....");

        BaseDto baseDto = new BaseDto();

        try {

            baseDto.setResponseObject(commonSearchImpl.searchLocationNotInList(searchRequest));

            baseDto.setResponseCode(ErrorCode.SUCCESS);

        } catch (Exception exception) {

            log.error("Exception in searchLocationNotInList method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);
    }

    public BaseDto search(LocationSearchDto searchDto) {

        log.info("getAll method is called....");

        BaseDto baseDto = new BaseDto();

        try {

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(locationSearchService.search(searchDto));

        } catch (RestException exception) {

            log.error("Exception in getAll method ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (Exception exception) {

            log.error("Exception in getAll method ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);

        }

        return appUtil.setDesc(baseDto);
    }

    public BaseDto search(SearchRequest searchRequest, Long countryId) {

        log.info("getAll method is called....");

        BaseDto baseDto = new BaseDto();

        try {

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(locationSearchService.search(searchRequest, countryId));

        } catch (RestException exception) {

            log.error("Exception in getAll method ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (Exception exception) {

            log.error("Exception in getAll method ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);

        }

        return appUtil.setDesc(baseDto);
    }

    public BaseDto searchByCompany(SearchRequest searchRequest, Long companyId) {

        log.info("searchByCompany method is called....");

        BaseDto baseDto = new BaseDto();

        try {

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(locationSearchService.searchByCompany(searchRequest, companyId));

        } catch (RestException exception) {

            log.error("Exception in getAll method ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (Exception exception) {

            log.error("Exception in getAll method ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);

        }

        return appUtil.setDesc(baseDto);
    }

    public BaseDto get(Long id) {

        log.info("get method is called. [" + id + "]");

        BaseDto baseDto = new BaseDto();

        try {

            ValidateUtil.notNull(id, ErrorCode.LOCATION_ID_NOT_NULL);

            LocationMaster locationMaster = locationMasterRepository.findById(id);


            if (locationMaster.getPartyMaster() != null) {
                AppUtil.setPartyToNull(locationMaster.getPartyMaster());
                locationMaster.setTmpPartyMaster(locationMaster.getPartyMaster());
            }

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(locationMaster);

        } catch (RestException exception) {

            log.error("Rest Exception occured ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (Exception exception) {

            log.error("Exception in get method", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);
    }

    public BaseDto createOrUpdate(LocationMaster locationMaster) {

        log.info("Create Method is called.");

        BaseDto baseDto = new BaseDto();

        try {
            locationMasterValidator.validate(locationMaster);

            if (locationMaster.getDateFormat() != null) {
                locationMaster.setDateTimeFormat(locationMaster.getDateFormat().concat(" HH:mm"));
                log.info("date time format : " + locationMaster.getDateTimeFormat());

            }

            if (locationMaster.getId() != null) {
                locationMasterRepository.getOne(locationMaster.getId());
            }

            locationMaster = locationMasterRepository.save(locationMaster);

            log.info("Location Master Saved Successfully....[" + locationMaster.getId() + "]");

            baseDto.setResponseCode(ErrorCode.SUCCESS);
            AppUtil.setPartyToNull(locationMaster.getPartyMaster());
            baseDto.setResponseObject(locationMaster);

        } catch (RestException exception) {

            log.error("Exception in create method : ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (ObjectOptimisticLockingFailureException e) {

            log.error("Optimistic Lock Exception occured ", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_LOCKED_RECORD);

        } catch (JpaObjectRetrievalFailureException e) {

            log.error("Object Already Deleted................", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_DELETED_RECORD);

        } catch (Exception exception) {

            String exceptionCause = ExceptionUtils.getRootCauseMessage(exception);
            log.error("Exception Cause  ::: ", exception);

            log.error("Exception Cause  ::: ", exceptionCause);

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            if (exceptionCause.contains(UniqueKey.UK_LOCATION_LOCATIONCODE)) {
                baseDto.setResponseCode(ErrorCode.LOCATION_CODE_ALREADY_EXIST);
            }

            if (exceptionCause.contains(UniqueKey.UK_LOCATION_LOCATIONNAME)) {
                baseDto.setResponseCode(ErrorCode.LOCATION_NAME_ALREADY_EXIST);
            }

        }

        return appUtil.setDesc(baseDto);

    }

    public BaseDto delete(Long id) {

        log.info("Delete method is invoked : [ " + id + "]");

        BaseDto baseDto = new BaseDto();

        try {
            locationMasterRepository.delete(id);

            log.info("LocationMaster Deleted Successfully.... [ " + id + "]");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

        } catch (DataIntegrityViolationException exception) {

            log.error("Exception occured : ", exception);

            ExceptionHelper helper = new ExceptionHelper(exception);

            log.error("Foreign key reference : " + helper.getFkReference());

            baseDto.setResponseCode(ErrorCode.ERROR_FK_CONSTRAINT);

        } catch (Exception exception) {

            log.error("Exception in Delete method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);
    }
}
