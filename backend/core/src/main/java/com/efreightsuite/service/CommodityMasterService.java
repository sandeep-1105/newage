package com.efreightsuite.service;

import com.efreightsuite.common.component.CacheRepository;
import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.CommoditySearchDto;
import com.efreightsuite.dto.SearchRequest;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.ExceptionHelper;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.exception.UniqueKey;
import com.efreightsuite.model.CommodityMaster;
import com.efreightsuite.repository.CommodityMasterRepository;
import com.efreightsuite.search.CommoditySearchImpl;
import com.efreightsuite.util.AppUtil;
import com.efreightsuite.util.SaaSUtil;
import com.efreightsuite.util.ValidateUtil;
import com.efreightsuite.validation.CommodityMasterValidator;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.exception.ExceptionUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.orm.ObjectOptimisticLockingFailureException;
import org.springframework.orm.jpa.JpaObjectRetrievalFailureException;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class CommodityMasterService {

    @Autowired
    private
    CommodityMasterRepository commodityMasterRepository;

    @Autowired
    private
    AppUtil appUtil;

    @Autowired
    private
    CacheRepository cacheRepository;

    @Autowired
    private
    CommodityMasterValidator commodityValidator;

    @Autowired
    private
    CommoditySearchImpl commoditySearchImpl;

    public BaseDto get(Long id) {

        log.info("get method is called....");

        BaseDto baseDto = new BaseDto();

        try {
            CommodityMaster commodityMaster = cacheRepository.get(SaaSUtil.getSaaSId(), CommodityMaster.class.getName(),
                    id, commodityMasterRepository);

            ValidateUtil.notNull(commodityMaster, ErrorCode.COMMODITY_ID_NOT_FOUND);

            log.info("Successfully getting Commodity by id...");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(commodityMaster);

        } catch (RestException exception) {

            log.error("RestException in get method : ", exception);

            baseDto.setResponseCode(exception.getMessage());
        } catch (Exception exception) {

            log.error("Exception in get method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);

        }

        return appUtil.setDesc(baseDto);
    }

    public BaseDto create(CommodityMaster commodityMaster) {

        log.info("Create method is called.....");

        BaseDto baseDto = new BaseDto();

        try {

            commodityValidator.commodityValidator(commodityMaster);

            CommodityMaster commodityMaster2 = cacheRepository.save(SaaSUtil.getSaaSId(),
                    CommodityMaster.class.getName(), commodityMaster, commodityMasterRepository);

            log.info("Commodity Saved successfully.....");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(commodityMaster2);

        } catch (RestException exception) {

            log.error("RestException in Create method : ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (Exception e) {

            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(e);

            log.error("Exception Cause 1 ::: " + exceptionCause1);

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            if (exceptionCause1.contains(UniqueKey.UK_COMMODITY_HSCODE)) {
                baseDto.setResponseCode(ErrorCode.COMMODITY_CODE_ALREADY_EXIST);
            }

            if (exceptionCause1.contains(UniqueKey.UK_COMMODITY_HSNAME)) {
                baseDto.setResponseCode(ErrorCode.COMMODITY_NAME_ALREADY_EXIST);
            }
        }
        return appUtil.setDesc(baseDto);
    }

    public BaseDto update(CommodityMaster existingCommodity) {

        log.info("update method is called.... : " + existingCommodity.getHsCode());

        BaseDto baseDto = new BaseDto();

        try {

            commodityValidator.commodityValidator(existingCommodity);

            commodityMasterRepository.getOne(existingCommodity.getId());

            existingCommodity = cacheRepository.save(SaaSUtil.getSaaSId(), CommodityMaster.class.getName(),
                    existingCommodity, commodityMasterRepository);

            log.info("Commodity  updated successfully...[" + existingCommodity.getId() + "]");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(existingCommodity);

        } catch (RestException exception) {

            log.error("RestException in Create method : ", exception);

            baseDto.setResponseCode(exception.getMessage());
        } catch (ObjectOptimisticLockingFailureException e) {

            log.error("Error in editing Unit", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_LOCKED_RECORD);

        } catch (JpaObjectRetrievalFailureException e) {

            log.error("Error in editing Unit", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_DELETED_RECORD);

        } catch (Exception e) {
            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(e);

            log.error("Exception Cause 1 ::: " + exceptionCause1);

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            if (exceptionCause1.contains(UniqueKey.UK_COMMODITY_HSCODE)) {
                baseDto.setResponseCode(ErrorCode.COMMODITY_CODE_ALREADY_EXIST);
            }

            if (exceptionCause1.contains(UniqueKey.UK_COMMODITY_HSNAME)) {
                baseDto.setResponseCode(ErrorCode.COMMODITY_NAME_ALREADY_EXIST);
            }
        }
        return appUtil.setDesc(baseDto);
    }
    /*
	 * Delete- This method deletes a existing commodity
	 *
	 * @Param commodity master entity
	 *
	 * @Return BaseDto
	 */

    public BaseDto delete(Long id) {
        log.info("delete  method is called....[ " + id + " ] ");

        BaseDto baseDto = new BaseDto();

        try {

            cacheRepository.delete(SaaSUtil.getSaaSId(), CommodityMaster.class.getName(), id,
                    commodityMasterRepository);

            log.info("Successfully deleted commodity by id...[ " + id + " ] ");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

        } catch (DataIntegrityViolationException exception) {

            log.error("Exception occured : ", exception);

            ExceptionHelper helper = new ExceptionHelper(exception);

            log.error("Foreign key reference : " + helper.getFkReference());

            baseDto.setResponseCode(ErrorCode.ERROR_FK_CONSTRAINT);

        } catch (Exception exception) {

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            log.error("Exception in Delete method : ", exception);

            String rootCause = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Root Cause of Exception : " + rootCause);

            if (rootCause.contains("FK_")) {
                baseDto.setResponseCode(ErrorCode.CANNOT_DELETE_FK_RECORD);
            }
        }
        return appUtil.setDesc(baseDto);

    }

    public BaseDto search(CommoditySearchDto searchDto) {

        log.info("Search method is called...." + searchDto);

        BaseDto baseDto = new BaseDto();

        try {

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(commoditySearchImpl.search(searchDto));

            log.info("Successfully Searching Tos...");

        } catch (Exception exception) {

            log.error("Exception in Search method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);

    }

    public BaseDto search(SearchRequest searchRequest) {

        log.info("Search method is called...." + searchRequest);

        BaseDto baseDto = new BaseDto();

        try {

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(commoditySearchImpl.search(searchRequest));

            log.info("Successfully Searching Commodity...");

        } catch (Exception exception) {

            log.error("Exception in Search method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);

    }

}
