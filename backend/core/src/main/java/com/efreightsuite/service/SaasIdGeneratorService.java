package com.efreightsuite.service;

import java.util.Set;
import java.util.stream.Stream;

import com.efreightsuite.model.common.LocationSetup;
import com.efreightsuite.repository.common.LocationSetupRepository;
import com.efreightsuite.util.OtpGenerator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import static com.efreightsuite.constants.Constants.SAAS_ID_PREFIX;

@Service
public class SaasIdGeneratorService {

    @Autowired
    private LocationSetupRepository locationSetupRepository;

    public String generateSaasId(LocationSetup locationSetup) {
        if (locationSetup.getSaasId() != null) {
            return locationSetup.getSaasId();
        }
        return generateNextSaasId();
    }

    private String generateNextSaasId() {
        Set<String> saasIds = locationSetupRepository.findAllSaasIds();
        // It is safe to call get on an Optional as eventually it will find a unique SaaS Id.
        return Stream.generate(() -> SAAS_ID_PREFIX + OtpGenerator.generateAlphabetic(3))
                .filter(next -> !saasIds.contains(next))
                .findFirst()
                .orElseThrow(() -> new RuntimeException("Unable to generate SaaS Id"));
    }
}
