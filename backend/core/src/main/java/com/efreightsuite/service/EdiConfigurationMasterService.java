package com.efreightsuite.service;

import java.util.List;

import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.EdiConfigrationSearchDto;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.exception.UniqueKey;
import com.efreightsuite.model.EdiConfigurationMaster;
import com.efreightsuite.repository.EdiConfigurationMasterRepository;
import com.efreightsuite.search.EdiConfigurationSearchImpl;
import com.efreightsuite.util.AppUtil;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.exception.ExceptionUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.ObjectOptimisticLockingFailureException;
import org.springframework.orm.jpa.JpaObjectRetrievalFailureException;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class EdiConfigurationMasterService {

    @Autowired
    private
    AppUtil appUtil;

    @Autowired
    private
    EdiConfigurationMasterRepository repository;

    @Autowired
    private
    EdiConfigurationSearchImpl searchImpl;

    public BaseDto get(Long id) {

        log.info("get method is called...[" + id + "]");

        BaseDto baseDto = new BaseDto();

        try {

            EdiConfigurationMaster ediConfigurationMaster = repository.findOne(id);

            log.info("Edi Configuration Master [" + ediConfigurationMaster.getId() + "]");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(ediConfigurationMaster);

        } catch (Exception exception) {

            log.error("Exception in get method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);
    }

    public BaseDto search(EdiConfigrationSearchDto searchDto, Long locationId) {

        log.info("Search method is called...." + searchDto);

        BaseDto baseDto = new BaseDto();

        try {


            baseDto.setResponseObject(searchImpl.search(searchDto, locationId));
            baseDto.setResponseCode(ErrorCode.SUCCESS);

        } catch (Exception exception) {

            log.error("Exception in Search method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);

    }

    public BaseDto update(EdiConfigurationMaster ediConfigurationMaster) {

        log.info("Create method is called");

        final BaseDto baseDto = new BaseDto();

        try {

            if (ediConfigurationMaster.getId() != null) {
                repository.getOne(ediConfigurationMaster.getId());
            }

            ediConfigurationMaster = repository.save(ediConfigurationMaster);

            log.info("Saved successfully.....[" + ediConfigurationMaster.getId() + "]");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(ediConfigurationMaster);

        } catch (RestException exception) {

            log.error("Exception Cause 1 ::: ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (ObjectOptimisticLockingFailureException e) {

            log.error("Error in editing", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_LOCKED_RECORD);

        } catch (JpaObjectRetrievalFailureException e) {

            log.error("Error in editing", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_DELETED_RECORD);

        } catch (Exception exception) {

            log.error("Exception occured : ", exception);

            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Exception Cause 1 : " + exceptionCause1);

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            if (exceptionCause1.contains(UniqueKey.UK_EDI_CONFIGURATION_KEY)) {
                baseDto.setResponseCode(ErrorCode.EDI_CONFIG_KEY_DUPLICATE);

            }
        }

        return appUtil.setDesc(baseDto);
    }

    public BaseDto create(
            List<EdiConfigurationMaster> ediConfigurationMasterList) {

        final BaseDto baseDto = new BaseDto();

        repository.save(ediConfigurationMasterList);

        baseDto.setResponseCode(ErrorCode.SUCCESS);
        return baseDto;
    }
}
