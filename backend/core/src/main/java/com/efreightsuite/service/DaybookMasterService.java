package com.efreightsuite.service;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.time.LocalDate;

import com.efreightsuite.common.component.CacheRepository;
import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.DaybookSearchDto;
import com.efreightsuite.dto.SearchRequest;
import com.efreightsuite.enumeration.SequenceFormat;
import com.efreightsuite.enumeration.YesNo;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.ExceptionHelper;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.exception.UniqueKey;
import com.efreightsuite.model.DaybookMaster;
import com.efreightsuite.repository.DaybookMasterRepository;
import com.efreightsuite.search.DaybookSearchImpl;
import com.efreightsuite.util.AppUtil;
import com.efreightsuite.util.ValidateUtil;
import com.efreightsuite.validation.DaybookMasterValidator;
import com.efreightsuite.validation.SearchDtoValidator;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.exception.ExceptionUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.orm.ObjectOptimisticLockingFailureException;
import org.springframework.orm.jpa.JpaObjectRetrievalFailureException;
import org.springframework.stereotype.Service;

/**
 * @author tanveer Date - 07.03.2016 Updated - 11.03.2016
 */

@Service
@Log4j2
public class DaybookMasterService {

    @Autowired
    private
    AppUtil appUtil;

    /**
     * DaybookMasterRepository Object
     */

    @Autowired
    private
    DaybookMasterRepository daybookMasterRepository;

    @Autowired
    SearchDtoValidator searchDtoValidator;

    @Autowired
    private
    DaybookMasterValidator daybookMasterValidator;

    @Autowired
    CacheRepository cacheRepository;

    @Autowired
    private
    DaybookSearchImpl daybookSearchImpl;

    /**
     * Method - getByDaybookId Returns DaybookMaster based on given pack id
     *
     * @return BaseDto
     */

    public BaseDto getByDaybookId(Long id) {

        log.info("getByDaybookId method is called...[" + id + "]");

        final BaseDto baseDto = new BaseDto();

        try {

            DaybookMaster daybookMaster = daybookMasterRepository.findById(id);

            ValidateUtil.notNull(daybookMaster, ErrorCode.DAYBOOK_ID_NOT_FOUND);

            if (daybookMaster.getLocationMaster() != null) {
                AppUtil.setPartyToNull(daybookMaster.getLocationMaster().getPartyMaster());
            }

            log.info("getByDaybookId is successfully executed....[" + daybookMaster.getId() + "]");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(daybookMaster);

        } catch (final RestException exception) {

            log.error("Exception in getByDaybookId method : ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (final Exception exception) {

            log.error("Exception in getByDaybookId method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);
    }

    /**
     * Method - getByDaybookCode Returns DaybookMaster based on given pack code
     *
     * @return BaseDto
     */

    public BaseDto getByDaybookCode(String packCode) {

        log.info("getByDaybookCode method is called..." + packCode);

        final BaseDto baseDto = new BaseDto();

        try {

            final DaybookMaster daybookMaster = daybookMasterRepository.findByDaybookCode(packCode);

            ValidateUtil.notNull(daybookMaster, ErrorCode.DAYBOOK_CODE_NOT_FOUND);

            log.info("Daybook Found Based on the given code : [" + daybookMaster.getDaybookCode() + "]");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(daybookMaster);

        } catch (RestException exception) {

            log.error("Exception in getByDaybookCode method : ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (Exception exception) {

            log.error("Exception in getByDaybookCode method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);
    }

    /**
     * Method - search Returns list of DaybookMaster based on the search keyword
     *
     * @return BaseDto
     */

    public BaseDto search(DaybookSearchDto searchDto) {

        log.info("Search method is called...." + searchDto);

        BaseDto baseDto = new BaseDto();

        try {

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(daybookSearchImpl.search(searchDto));

            log.info("Successfully Searching Daybook...");

        } catch (Exception exception) {

            log.error("Exception in Search method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);

    }

    public BaseDto search(SearchRequest searchRequest) {

        log.info("Search method is called...." + searchRequest);

        BaseDto baseDto = new BaseDto();

        try {

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(daybookSearchImpl.search(searchRequest));

            log.info("Successfully Searching Daybook...");

        } catch (Exception exception) {

            log.error("Exception in Search method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);

    }


    public BaseDto searchDaybookListByDocumentType(SearchRequest searchRequest, Long documentTypeId) {

        log.info("Search method is called...." + searchRequest);

        BaseDto baseDto = new BaseDto();

        try {

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(daybookSearchImpl.searchByDocumentType(searchRequest, documentTypeId));

            log.info("Successfully Searching Daybook...");

        } catch (Exception exception) {

            log.error("Exception in Search method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);

    }

    /**
     * Method - create Creates and returns a new pack master
     *
     * @returns DaybookMaster
     */

    public BaseDto create(DaybookMaster daybookMaster) {

        log.info("Create method is called - daybookMasterId ..");

        final BaseDto baseDto = new BaseDto();

        try {

            daybookMasterValidator.validate(daybookMaster);

            String prefix = "";
            String lastOneDigitsOfYear = String.valueOf(LocalDate.now().getYear()).substring(3, 4);
            String twoDigitsOfMonth = String.valueOf(LocalDate.now().getMonthValue());

            if (LocalDate.now().getMonthValue() < 10) {
                NumberFormat f = new DecimalFormat("00");
                twoDigitsOfMonth = String.valueOf(f.format(LocalDate.now().getMonthValue()));
            }
            String firstTwoDigitLocationCode = daybookMaster.getLocationMaster().getLocationCode().substring(0, 2);

            prefix = lastOneDigitsOfYear + twoDigitsOfMonth + firstTwoDigitLocationCode;

            daybookMaster.setPrefix(prefix);

            daybookMaster.setFormat(SequenceFormat.SIX);

            daybookMaster.setCurrentSequenceValue(1);

            daybookMaster.setIsMonth(YesNo.Yes);

            daybookMaster.setIsYear(YesNo.Yes);

            daybookMasterRepository.save(daybookMaster);

            log.info("Daybook Saved successfully.....");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(daybookMaster);

        } catch (RestException exception) {

            log.error("Exception Cause 1 ::: ", exception);
            baseDto.setResponseCode(exception.getMessage());


        } catch (Exception exception) {

            log.error("Exception occured : ", exception);


            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Exception Cause 1 : " + exceptionCause1);

            // General error condition
            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            // Duplicate pack Code

            if (exceptionCause1.contains(UniqueKey.UK_DAYBOOK_DAYBOOKCODE)) {
                baseDto.setResponseCode(ErrorCode.DAYBOOK_CODE_ALREADY_EXIST);

            }

            if (exceptionCause1.contains(UniqueKey.UK_DAYBOOK_DAYBOOKNAME)) {
                baseDto.setResponseCode(ErrorCode.DAYBOOK_NAME_ALREADY_EXIST);
            }


        }

        return appUtil.setDesc(baseDto);
    }

    /**
     * Method - update Updates and returns a updated pack master
     *
     * @param existingDaybookMaster
     * @returns DaybookMaster
     */

    public BaseDto update(DaybookMaster existingDaybookMaster) {

        log.info("Update method is called - existingDaybookMaster : [" + existingDaybookMaster.getId() + "]");

        BaseDto baseDto = new BaseDto();

        try {

            ValidateUtil.notNull(existingDaybookMaster.getId(), ErrorCode.DAYBOOK_ID_NOT_NULL);

            daybookMasterValidator.validate(existingDaybookMaster);

            daybookMasterRepository.getOne(existingDaybookMaster.getId());

            daybookMasterRepository.save(existingDaybookMaster);

            log.info("DaybookMaster updated successfully....[" + existingDaybookMaster.getId() + "]");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(existingDaybookMaster);

        } catch (RestException exception) {

            log.error("Exception occured with error code ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (ObjectOptimisticLockingFailureException e) {

            log.error("Error in editing", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_LOCKED_RECORD);

        } catch (JpaObjectRetrievalFailureException e) {

            log.error("Error in editing", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_DELETED_RECORD);

        } catch (Exception exception) {

            log.error("Exception occured : ", exception);

            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Exception Cause 1 ::: " + exceptionCause1);


            // General error condition
            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            // Duplicate pack Code

            if (exceptionCause1.contains(UniqueKey.UK_DAYBOOK_DAYBOOKCODE)) {
                baseDto.setResponseCode(ErrorCode.DAYBOOK_CODE_ALREADY_EXIST);

            }

            if (exceptionCause1.contains(UniqueKey.UK_DAYBOOK_DAYBOOKNAME)) {
                baseDto.setResponseCode(ErrorCode.DAYBOOK_NAME_ALREADY_EXIST);
            }


        }

        return appUtil.setDesc(baseDto);
    }

    /**
     * Deletes the Existing DaybookMaster Based on the Given Id
     *
     * @Returns BaseDto
     */
    public BaseDto delete(Long id) {

        log.info("Delete method is Invoked...[" + id + "]");

        final BaseDto baseDto = new BaseDto();

        try {
            daybookMasterRepository.delete(id);
            log.info("DaybookMaster Deleted Successfully....[" + id + "]");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

        } catch (RestException exception) {

            log.error("Exception in Delete method : ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (DataIntegrityViolationException exception) {

            log.error("Exception occured : ", exception);

            ExceptionHelper helper = new ExceptionHelper(exception);

            log.error("Foreign key reference : " + helper.getFkReference());

            baseDto.setResponseCode(ErrorCode.ERROR_FK_CONSTRAINT);

        } catch (Exception exception) {

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            log.error("Exception in Delete method : ", exception);

            String rootCause = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Root Cause of Exception : " + rootCause);

            if (rootCause.contains("FK_")) {
                baseDto.setResponseCode(ErrorCode.CANNOT_DELETE_FK_RECORD);
            }
        }

        return appUtil.setDesc(baseDto);
    }


    public String generateDocumentNo(Long daybookId) {

        log.info("DaybookMasterService.generateDocumentNo start");

        String genaratedSequence = "";

        String prefix = "";

        DaybookMaster daybookMasterToUpdate = daybookMasterRepository.getOne(daybookId);

        if (daybookMasterToUpdate == null) {
            throw new RestException(ErrorCode.SEQUENCE_NOT_PRESENT);
        }

        String lastOneDigitsOfYear = String.valueOf(LocalDate.now().getYear()).substring(3, 4);

        String twoDigitsOfMonth = String.valueOf(LocalDate.now().getMonthValue());

        if (LocalDate.now().getMonthValue() < 10) {
            NumberFormat f = new DecimalFormat("00");
            twoDigitsOfMonth = String.valueOf(f.format(LocalDate.now().getMonthValue()));
        }
        String firstTwoDigitLocationCode = daybookMasterToUpdate.getLocationMaster().getLocationCode().substring(0, 2);

        if (daybookMasterToUpdate.getIsYear().equals(YesNo.Yes)) {
            prefix = lastOneDigitsOfYear;
        }
        if (daybookMasterToUpdate.getIsMonth().equals(YesNo.Yes)) {
            prefix = prefix + twoDigitsOfMonth;
        }
        prefix = prefix + firstTwoDigitLocationCode;

        if (!prefix.equals(daybookMasterToUpdate.getPrefix())) {
            daybookMasterToUpdate.setPrefix(prefix);
            daybookMasterToUpdate = daybookMasterRepository.save(daybookMasterToUpdate);
        }

        prefix = daybookMasterToUpdate.getPrefix();

        daybookMasterToUpdate.setPrefix(prefix);
        if (daybookMasterToUpdate.getPrefix() != null) {
            genaratedSequence = String.format(
                    daybookMasterToUpdate.getPrefix().toUpperCase()
                            + daybookMasterToUpdate.getFormat().getFormat(),
                    daybookMasterToUpdate.getCurrentSequenceValue());
        } else {
            genaratedSequence = String.format(daybookMasterToUpdate.getFormat().getFormat(),
                    daybookMasterToUpdate.getCurrentSequenceValue());
        }
        log.info("DaybookMasterService.generateDocumentNo Current Sequence Value : " + genaratedSequence);

        daybookMasterToUpdate.setCurrentSequenceValue(daybookMasterToUpdate.getCurrentSequenceValue() + 1);

        daybookMasterRepository.save(daybookMasterToUpdate);

        log.info("DaybookMasterService.generateDocumentNo end");

        return genaratedSequence;
    }


}
