package com.efreightsuite.service;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.enumeration.SequenceType;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.exception.UniqueKey;
import com.efreightsuite.model.Consol;
import com.efreightsuite.model.ConsolAttachment;
import com.efreightsuite.model.ConsolCharge;
import com.efreightsuite.model.ConsolConnection;
import com.efreightsuite.model.ConsolDocument;
import com.efreightsuite.model.ConsolDocumentDimension;
import com.efreightsuite.model.ConsolEvent;
import com.efreightsuite.model.PickUpDeliveryPoint;
import com.efreightsuite.repository.ConsolAttachmentRepository;
import com.efreightsuite.repository.ConsolRepository;
import com.efreightsuite.repository.DefaultMasterDataRepository;
import com.efreightsuite.repository.ShipmentRepository;
import com.efreightsuite.util.AppUtil;
import com.efreightsuite.validation.ConsolValidator;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.exception.ExceptionUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class CopyConsolService {

    @Autowired
    private
    AppUtil appUtil;

    @Autowired
    private
    ConsolRepository consolRepository;

    @Autowired
    ShipmentRepository shipmentRepository;

    @Autowired
    private
    ConsolValidator consolValidator;

    @Autowired
    private
    SequenceGeneratorService sequenceGeneratorService;

    @Autowired
    DefaultMasterDataRepository defaultMasterDataRepository;

    @Autowired
    ConsolAttachmentRepository consolAttachmentRepository;


    @Transactional
    public BaseDto copyConsol(Consol consol) {

        BaseDto baseDto = new BaseDto();

        if (consol != null) {

            try {

                Consol consolNew = new Consol();

                log.info("ConsolService -> copyConsol method is called....");

                consolNew.setCreatedBy(consol.getCreatedBy());
                consolNew.setCreatedByCode(consol.getCreatedByCode());
                consolNew.setCompany(consol.getCompany());
                consolNew.setCompanyCode(consol.getCompanyCode());
                consolNew.setLocation(consol.getLocation());
                consolNew.setLocationCode(consol.getLocationCode());
                consolNew.setCountry(consol.getCountry());
                consolNew.setCountryCode(consol.getCountryCode());
                consolNew.setConsolUid(consol.getConsolUid());
                consolNew.setServiceMaster(consol.getServiceMaster());
                consolNew.setServiceCode(consol.getServiceCode());
                consolNew.setAgent(consol.getAgent());
                consolNew.setAgentCode(consol.getAgentCode());
                consolNew.setAgentManifestName(consol.getAgentManifestName());
                consolNew.setAgentAddress(consol.getAgentAddress());
                consolNew.setPpcc(consol.getPpcc());
                consolNew.setJobStatus(consol.getJobStatus());
                consolNew.setIsJobCompleted(consol.getIsJobCompleted());
                consolNew.setOrigin(consol.getOrigin());
                consolNew.setOriginCode(consol.getOriginCode());
                consolNew.setPol(consol.getPol());
                consolNew.setPolCode(consol.getPolCode());
                consolNew.setPod(consol.getPod());
                consolNew.setPodCode(consol.getPodCode());
                consolNew.setDestination(consol.getDestination());
                consolNew.setDestinationCode(consol.getDestinationCode());
                consolNew.setCurrency(consol.getCurrency());
                consolNew.setCurrencyCode(consol.getCurrencyCode());
                consolNew.setCurrencyRate(consol.getCurrencyRate());
                consolNew.getConsolDocument().setVolume(consol.getConsolDocument().getVolume());

                consolNew.getConsolDocument().setRatePerCharge(consol.getConsolDocument().getRatePerCharge());
                consolNew.setCarrier(consol.getCarrier());
                consolNew.setCarrierCode(consol.getCarrierCode());
                consolNew.setEtd(consol.getEtd());
                consolNew.setEta(consol.getEta());
                consolNew.setAtd(consol.getAtd());
                consolNew.setAta(consol.getAta());
                consolNew.getConsolDocument().setRouteNo(consol.getConsolDocument().getRouteNo());
                consolNew.getConsolDocument().setMawbNo(consol.getConsolDocument().getMawbNo());
                consolNew.getConsolDocument().setMawbIssueDate(consol.getConsolDocument().getMawbIssueDate());
                consolNew.setTotalAmount(consol.getTotalAmount());
                consolValidator.updateJobDate(consolNew);


                // Validate Old consol
                consolValidator.validate(consol);

                consolNew.setConsolUid(sequenceGeneratorService.getSequenceValueByType(SequenceType.CONSOL));

		
				/*Consol connections start here*/
                List<ConsolConnection> consolConnectionList = new ArrayList<>();
                if (consol.getConnectionList() != null && consol.getConnectionList().size() != 0) {
                    for (ConsolConnection cc : consol.getConnectionList()) {
                        ConsolConnection ccNew = new ConsolConnection();

                        ccNew.setConsol(consolNew);

                        ccNew.setMove(cc.getMove());
                        ccNew.setEtd(cc.getEtd());
                        ccNew.setEta(cc.getEta());
                        ccNew.setPol(cc.getPol());
                        ccNew.setPolCode(cc.getPolCode());
                        ccNew.setPod(cc.getPod());
                        ccNew.setPodCode(cc.getPodCode());
                        ccNew.setCarrierMaster(cc.getCarrierMaster());
                        ccNew.setFlightVoyageNo(cc.getFlightVoyageNo());
                        ccNew.setConnectionStatus(cc.getConnectionStatus());

                        consolConnectionList.add(ccNew);
                    }
                    consolNew.setConnectionList(consolConnectionList);
                } else {
                    consolNew.setConnectionList(null);
                }
				/*Consol connections end here*/
				
				
				/*Consol Charge Start here*/
                List<ConsolCharge> consolChargeList = new ArrayList<>();
                if (consol.getChargeList() != null && consol.getChargeList().size() != 0) {
                    for (ConsolCharge cc : consol.getChargeList()) {
                        ConsolCharge cchNew = new ConsolCharge();
                        cchNew.setConsol(consolNew);
                        cchNew.setConsolUid(consolNew.getConsolUid());

                        cchNew.setLocation(cc.getLocation());
                        cchNew.setLocationCode(cc.getLocationCode());
                        cchNew.setCompany(cc.getCompany());
                        cchNew.setCountry(cc.getCountry());
                        cchNew.setCountryCode(cc.getCountryCode());
                        cchNew.setChargeMaster(cc.getChargeMaster());
                        cchNew.setChargeCode(cc.getChargeCode());
                        cchNew.setChargeName(cc.getChargeName());
                        cchNew.setCrn(cc.getCrn());
                        cchNew.setPpcc(cc.getPpcc());
                        cchNew.setCurrency(cc.getCurrency());
                        cchNew.setCurrencyCode(cc.getCurrencyCode());
                        cchNew.setCurrencyRoe(cc.getCurrencyRoe());
                        cchNew.setUnit(cc.getUnit());
                        cchNew.setAmountPerUnit(cc.getAmountPerUnit());
                        cchNew.setAmount(cc.getAmount());
                        cchNew.setLocalAmount(cc.getLocalAmount());
                        cchNew.setDue(cc.getDue());

                        consolChargeList.add(cchNew);
                    }
                    consolNew.setChargeList(consolChargeList);
                } else {
                    consolNew.setChargeList(null);
                }
				/*Consol Charge End here*/
				
				
				/*Document code start here*/


                consolNew.setConsolDocument(new ConsolDocument());


                consolNew.getConsolDocument().setConsolUid(consolNew.getConsolUid());
                consolNew.getConsolDocument().setDocumentNo(sequenceGeneratorService.getSequenceValueByType(SequenceType.DOCUMENT));

                consolNew.getConsolDocument().setDocumentReqDate(consol.getConsolDocument().getDocumentReqDate());
                consolNew.getConsolDocument().setShipper(consol.getConsolDocument().getShipper());
                consolNew.getConsolDocument().setShipperCode(consol.getConsolDocument().getShipperCode());
                consolNew.getConsolDocument().setShipperAddress(consol.getConsolDocument().getShipperAddress());
                consolNew.getConsolDocument().setShipperManifestName(consol.getConsolDocument().getShipperManifestName());

                consolNew.getConsolDocument().setForwarder(consol.getConsolDocument().getForwarder());
                consolNew.getConsolDocument().setForwarderCode(consol.getConsolDocument().getForwarderCode());
                consolNew.getConsolDocument().setForwarderManifestName(consol.getConsolDocument().getForwarderManifestName());
                consolNew.getConsolDocument().setForwarderAddress(consol.getConsolDocument().getForwarderAddress());

                consolNew.getConsolDocument().setConsignee(consol.getConsolDocument().getConsignee());
                consolNew.getConsolDocument().setConsigneeCode(consol.getConsolDocument().getConsigneeCode());
                consolNew.getConsolDocument().setConsigneeManifestName(consol.getConsolDocument().getConsigneeManifestName());
                consolNew.getConsolDocument().setConsigneeAddress(consol.getConsolDocument().getConsigneeAddress());

                consolNew.getConsolDocument().setFirstNotify(consol.getConsolDocument().getFirstNotify());
                consolNew.getConsolDocument().setFirstNotifyCode(consol.getConsolDocument().getFirstNotifyCode());
                consolNew.getConsolDocument().setFirstNotifyManifestName(consol.getConsolDocument().getFirstNotifyManifestName());
                consolNew.getConsolDocument().setFirstNotifyAddress(consol.getConsolDocument().getFirstNotifyAddress());

                consolNew.getConsolDocument().setSecondNotify(consol.getConsolDocument().getSecondNotify());
                consolNew.getConsolDocument().setSecondNotifyCode(consol.getConsolDocument().getSecondNotifyCode());
                consolNew.getConsolDocument().setSecondNotifyManifestName(consol.getConsolDocument().getSecondNotifyManifestName());
                consolNew.getConsolDocument().setSecondNotifyAddress(consol.getConsolDocument().getSecondNotifyAddress());

                consolNew.getConsolDocument().setAgent(consol.getConsolDocument().getAgent());
                consolNew.getConsolDocument().setAgentCode(consol.getConsolDocument().getAgentCode());
                consolNew.getConsolDocument().setAgentManifestName(consol.getConsolDocument().getAgentManifestName());
                consolNew.getConsolDocument().setAgentAddress(consol.getConsolDocument().getAgentAddress());

                consolNew.getConsolDocument().setIssuingAgent(consol.getConsolDocument().getIssuingAgent());
                consolNew.getConsolDocument().setIssuingAgentCode(consol.getConsolDocument().getIssuingAgentCode());
                consolNew.getConsolDocument().setIssuingAgentName(consol.getConsolDocument().getIssuingAgentName());
                consolNew.getConsolDocument().setIssuingAgentAddress(consol.getConsolDocument().getIssuingAgentAddress());


                consolNew.getConsolDocument().setPackMaster(consol.getConsolDocument().getPackMaster());
                consolNew.getConsolDocument().setPackCode(consol.getConsolDocument().getPackCode());
                consolNew.getConsolDocument().setPackDescription(consol.getConsolDocument().getPackDescription());

                consolNew.getConsolDocument().setNoOfPieces(consol.getConsolDocument().getNoOfPieces());
                consolNew.getConsolDocument().setDimensionUnit(consol.getConsolDocument().getDimensionUnit());
                consolNew.getConsolDocument().setDimensionUnitValue(consol.getConsolDocument().getDimensionUnitValue());
                consolNew.getConsolDocument().setChargebleWeight(consol.getConsolDocument().getChargebleWeight());
                consolNew.getConsolDocument().setGrossWeight(consol.getConsolDocument().getGrossWeight());
                consolNew.getConsolDocument().setVolumeWeight(consol.getConsolDocument().getVolumeWeight());
                consolNew.getConsolDocument().setVolumeWeightInPound(consol.getConsolDocument().getVolumeWeightInPound());
                consolNew.getConsolDocument().setGrossWeightInPound(consol.getConsolDocument().getGrossWeightInPound());
                consolNew.getConsolDocument().setHandlingInformation(consol.getConsolDocument().getHandlingInformation());
                consolNew.getConsolDocument().setCommodityDescription(consol.getConsolDocument().getCommodityDescription());
                consolNew.getConsolDocument().setMarksAndNo(consol.getConsolDocument().getMarksAndNo());
                consolNew.getConsolDocument().setRateClass(consol.getConsolDocument().getRateClass());
                consolNew.getConsolDocument().setBlReceivedPerson(consol.getConsolDocument().getBlReceivedPerson());
                consolNew.getConsolDocument().setBlReceivedPersonCode(consol.getConsolDocument().getBlReceivedPersonCode());
                consolNew.getConsolDocument().setBlReceivedDate(consol.getConsolDocument().getBlReceivedDate());

                consolNew.getConsolDocument().setCompany(consol.getConsolDocument().getCompany());
                consolNew.getConsolDocument().setCountry(consol.getConsolDocument().getCountry());
                consolNew.getConsolDocument().setCountryCode(consol.getConsolDocument().getCountryCode());

                consolNew.getConsolDocument().setLocation(consol.getConsolDocument().getLocation());
                consolNew.getConsolDocument().setDocPrefixMaster(consol.getConsolDocument().getDocPrefixMaster());
                consolNew.getConsolDocument().setDocPrefixCode(consol.getConsolDocument().getDocPrefixCode());

                consolNew.getConsolDocument().setPickUpDeliveryPoint(consol.getConsolDocument().getPickUpDeliveryPoint());

                consolNew.getConsolDocument().setServiceCode(consol.getConsolDocument().getServiceCode());
                consolNew.getConsolDocument().setBookingVolumeUnit(consol.getConsolDocument().getBookingVolumeUnit());
                consolNew.getConsolDocument().setBookingVolumeUnitCode(consol.getConsolDocument().getBookingVolumeUnitCode());
                consolNew.getConsolDocument().setBookingVolumeUnitValue(consol.getConsolDocument().getBookingVolumeUnitValue());
                consolNew.getConsolDocument().setBookingVolume(consol.getConsolDocument().getBookingVolume());

                consolNew.getConsolDocument().setBookingGrossWeightUnit(consol.getConsolDocument().getBookingGrossWeightUnit());
                consolNew.getConsolDocument().setBookingGrossWeightUnitCode(consol.getConsolDocument().getBookingGrossWeightUnitCode());
                consolNew.getConsolDocument().setBookingGrossWeightUnitValue(consol.getConsolDocument().getBookingGrossWeightUnitValue());
                consolNew.getConsolDocument().setBookingGrossWeight(consol.getConsolDocument().getBookingGrossWeight());

                consolNew.getConsolDocument().setBookingVolumeWeightUnit(consol.getConsolDocument().getBookingVolumeWeightUnit());
                consolNew.getConsolDocument().setBookingVolumeWeightUnitCode(consol.getConsolDocument().getBookingVolumeWeightUnitCode());
                consolNew.getConsolDocument().setBookingVolumeWeightUnitValue(consol.getConsolDocument().getBookingVolumeWeightUnitValue());
                consolNew.getConsolDocument().setBookingVolumeWeight(consol.getConsolDocument().getBookingVolumeWeight());

                consolNew.getConsolDocument().setBookingChargeableUnit(consol.getConsolDocument().getBookingChargeableUnit());

                consolNew.getConsolDocument().setVesselId(consol.getConsolDocument().getVesselId());
                consolNew.getConsolDocument().setVesselCode(consol.getConsolDocument().getVesselCode());

                consolNew.getConsolDocument().setRouteNo(consol.getConsolDocument().getRouteNo());

                consolNew.getConsolDocument().setTransitPort(consol.getConsolDocument().getTransitPort());
                consolNew.getConsolDocument().setTransitPortCode(consol.getConsolDocument().getTransitPortCode());
                consolNew.getConsolDocument().setTransitEta(consol.getConsolDocument().getTransitEta());
                consolNew.getConsolDocument().setTransitScheduleUid(consol.getConsolDocument().getTransitScheduleUid());

                consolNew.getConsolDocument().setMawbNo(consol.getConsolDocument().getMawbNo());
                consolNew.getConsolDocument().setMawbGeneratedDate(consol.getConsolDocument().getMawbGeneratedDate());


                consolNew.getConsolDocument().setCommodity(consol.getConsolDocument().getCommodity());
                consolNew.getConsolDocument().setCommodityCode(consol.getConsolDocument().getCommodityCode());


                consolNew.getConsolDocument().setDocumentSubDetail(consol.getConsolDocument().getDocumentSubDetail());

                List<ConsolDocumentDimension> consolDocumentDimensionList = new ArrayList<>();
                if (consol.getConsolDocument().getDimensionList() != null && consol.getConsolDocument().getDimensionList().size() != 0) {
                    for (ConsolDocumentDimension cdd : consol.getConsolDocument().getDimensionList()) {
                        ConsolDocumentDimension cddNew = new ConsolDocumentDimension();

                        cddNew.setDocumentDetail(consolNew.getConsolDocument());

                        cddNew.setNoOfPiece(cdd.getNoOfPiece());
                        cddNew.setLength(cdd.getLength());
                        cddNew.setWidth(cdd.getWidth());
                        cddNew.setHeight(cdd.getHeight());
                        cddNew.setVolWeight(cdd.getVolWeight());
                        cddNew.setGrossWeight(cdd.getGrossWeight());

                        consolDocumentDimensionList.add(cddNew);
                    }

                    consolNew.getConsolDocument().setDimensionList(consolDocumentDimensionList);
                } else {
                    consolNew.getConsolDocument().setDimensionList(null);
                }
				
				
							
	            /*Document code start here*/

				

				
				
				/*Shipment Attachment Code*/

                log.info("Attachment Processing Begins....");
                List<ConsolAttachment> consolAttachmentList = new ArrayList<>();
                if (consol.getConsolAttachmentList() != null && consol.getConsolAttachmentList().size() != 0) {
                    log.info("There are " + consol.getConsolAttachmentList().size() + " number of attachments....");

                    for (ConsolAttachment attachment : consol.getConsolAttachmentList()) {
                        ConsolAttachment caNew = new ConsolAttachment();
                        caNew.setConsol(consolNew);

                        caNew.setRefNo(attachment.getRefNo());
                        caNew.setDocumentMaster(attachment.getDocumentMaster());
                        caNew.setIsCustoms(attachment.getIsCustoms());
                        caNew.setIsProtected(attachment.getIsProtected());
                        caNew.setFileName(attachment.getFileName());
                        caNew.setFileContentType(attachment.getFileContentType());
                        caNew.setFile(attachment.getFile());

                        consolAttachmentList.add(caNew);
                    }
                    consolNew.setConsolAttachmentList(consolAttachmentList);
                } else {
                    log.info("There are no attachment for this consol " + consol.getConsolUid());
                    consolNew.setConsolAttachmentList(null);
                }
                log.info("Attachment Processing Finished...");
				
				/*Consol Event Code Start here*/
                List<ConsolEvent> consolEventList = new ArrayList<>();
                if (consol.getEventList() != null && consol.getEventList().size() != 0) {
                    for (ConsolEvent event : consol.getEventList()) {

                        ConsolEvent ceNew = new ConsolEvent();

                        ceNew.setConsol(consolNew);

                        ceNew.setEventMaster(event.getEventMaster());
                        ceNew.setEventDate(event.getEventDate());
                        ceNew.setFollowUpRequired(event.getFollowUpRequired());
                        ceNew.setAssignedTo(event.getAssignedTo());
                        ceNew.setFollowUpDate(event.getFollowUpDate());
                        ceNew.setIsCompleted(event.getIsCompleted());

                        consolEventList.add(ceNew);
                    }
                    consolNew.setEventList(consolEventList);
                } else {
                    consolNew.setEventList(null);
                }
				
				/*Consol Event Code End here*/

                //Consol pICK UP
                PickUpDeliveryPoint pickUpDeliveryPointNew = new PickUpDeliveryPoint();
                if (consol.getPickUpDeliveryPoint() != null) {

                    PickUpDeliveryPoint pickUpDeliveryPointOld = consol.getPickUpDeliveryPoint();


                    pickUpDeliveryPointNew.setTransporter(pickUpDeliveryPointOld.getTransporter());
                    pickUpDeliveryPointNew.setTransporterCode(pickUpDeliveryPointOld.getTransporterCode());
                    pickUpDeliveryPointNew.setTransportermManifestName(pickUpDeliveryPointOld.getTransportermManifestName());
                    pickUpDeliveryPointNew.setTransportAddress(pickUpDeliveryPointOld.getTransportAddress());
                    pickUpDeliveryPointNew.setTransporterAddress(pickUpDeliveryPointOld.getTransporterAddress());
                    pickUpDeliveryPointNew.setIsOurPickUp(pickUpDeliveryPointOld.getIsOurPickUp());
                    pickUpDeliveryPointNew.setPickUpClientRefNo(pickUpDeliveryPointOld.getPickUpClientRefNo());
                    pickUpDeliveryPointNew.setPickupPoint(pickUpDeliveryPointOld.getPickupPoint());
                    pickUpDeliveryPointNew.setPickupPointCode(pickUpDeliveryPointOld.getPickupPointCode());
                    pickUpDeliveryPointNew.setPickupFrom(pickUpDeliveryPointOld.getPickupFrom());
                    pickUpDeliveryPointNew.setPickupFromCode(pickUpDeliveryPointOld.getPickupFromCode());
                    pickUpDeliveryPointNew.setPickUpPlace(pickUpDeliveryPointOld.getPickUpPlace());
                    pickUpDeliveryPointNew.setPickUpContactPerson(pickUpDeliveryPointOld.getPickUpContactPerson());
                    pickUpDeliveryPointNew.setPickUpMobileNo(pickUpDeliveryPointOld.getPickUpMobileNo());
                    pickUpDeliveryPointNew.setPickUpPhoneNo(pickUpDeliveryPointOld.getPickUpPhoneNo());
                    pickUpDeliveryPointNew.setPickUpEmail(pickUpDeliveryPointOld.getPickUpEmail());
                    pickUpDeliveryPointNew.setPickUpFax(pickUpDeliveryPointOld.getPickUpFax());
                    pickUpDeliveryPointNew.setPickUpFollowUp(pickUpDeliveryPointOld.getPickUpFollowUp());
                    pickUpDeliveryPointNew.setPickUpPlanned(pickUpDeliveryPointOld.getPickUpPlanned());
                    pickUpDeliveryPointNew.setPickUpActual(pickUpDeliveryPointOld.getPickUpActual());
                    pickUpDeliveryPointNew.setDeliveryPoint(pickUpDeliveryPointOld.getDeliveryPoint());
                    pickUpDeliveryPointNew.setDeliveryPointCode(pickUpDeliveryPointOld.getDeliveryPointCode());
                    pickUpDeliveryPointNew.setDeliveryFrom(pickUpDeliveryPointOld.getDeliveryFrom());
                    pickUpDeliveryPointNew.setDeliveryFromCode(pickUpDeliveryPointOld.getDeliveryFromCode());
                    pickUpDeliveryPointNew.setDeliveryPlace(pickUpDeliveryPointOld.getDeliveryPlace());
                    pickUpDeliveryPointNew.setDeliveryContactPerson(pickUpDeliveryPointOld.getDeliveryContactPerson());
                    pickUpDeliveryPointNew.setDoorDeliveryMobileNo(pickUpDeliveryPointOld.getDoorDeliveryMobileNo());
                    pickUpDeliveryPointNew.setDoorDeliveryPhoneNo(pickUpDeliveryPointOld.getDoorDeliveryPhoneNo());
                    pickUpDeliveryPointNew.setDoorDeliveryEmail(pickUpDeliveryPointOld.getDoorDeliveryEmail());
                    pickUpDeliveryPointNew.setDoorDeliveryExpected(pickUpDeliveryPointOld.getDoorDeliveryExpected());
                    pickUpDeliveryPointNew.setDoorDeliveryActual(pickUpDeliveryPointOld.getDoorDeliveryActual());

                    consolNew.setPickUpDeliveryPoint(pickUpDeliveryPointNew);
                } else {
                    consolNew.setPickUpDeliveryPoint(null);
                }

                consolValidator.validate(consolNew);

                Consol consolNewRes = consolRepository.save(consolNew);

                log.info("Consol copied successfull with id :  " + consolNewRes.getId());

                baseDto.setResponseCode(ErrorCode.SUCCESS);
                baseDto.setResponseObject(consolNewRes);
            } catch (RestException re) {
                log.error("Exception in ConsolService -> copyConsol method ", re);
                baseDto.setResponseCode(re.getMessage());

            } catch (Exception exception) {
                log.error("Exception in ConsolService -> copyConsol method ", exception);

                String exceptionCause1 = ExceptionUtils.getRootCauseMessage(exception);
                log.error("Exception Cause 1 ::: " + exceptionCause1);

                baseDto.setResponseCode(ErrorCode.FAILED);

                if (exceptionCause1.contains(UniqueKey.UK_CONSOL_CONSOLUID)) {
                    baseDto.setResponseCode(ErrorCode.CONSOL_UNIQUE);
                }

                if (exceptionCause1.contains(UniqueKey.UK_CONSLATTCH_REF)) {
                    baseDto.setResponseCode(ErrorCode.ATTACHMENT_REFERENCE_NUMBER_DUPLICATED);
                }

                if (exceptionCause1.contains("FK_")) {
                    baseDto.setResponseCode(ErrorCode.SAVE_UPDATE_FK_RECORD);
                }

            }
            return appUtil.setDesc(baseDto);

        } else {
            log.error("Consol should no be null");
            baseDto.setResponseCode(ErrorCode.FAILED);
            baseDto.setResponseObject(ErrorCode.FAILED);
            return appUtil.setDesc(baseDto);

        }

    }
}
