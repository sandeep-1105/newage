package com.efreightsuite.service;

import com.efreightsuite.common.component.CacheRepository;
import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.CfsSearchDto;
import com.efreightsuite.dto.SearchRequest;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.ExceptionHelper;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.exception.UniqueKey;
import com.efreightsuite.model.CFSMaster;
import com.efreightsuite.repository.CfsMasterRepository;
import com.efreightsuite.search.CfsSearchImpl;
import com.efreightsuite.util.AppUtil;
import com.efreightsuite.util.SaaSUtil;
import com.efreightsuite.util.ValidateUtil;
import com.efreightsuite.validation.CfsMasterValidator;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.exception.ExceptionUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.orm.ObjectOptimisticLockingFailureException;
import org.springframework.orm.jpa.JpaObjectRetrievalFailureException;
import org.springframework.stereotype.Service;

@Log4j2
@Service
public class CfsMasterService {

    @Autowired
    private
    AppUtil appUtil;

    @Autowired
    private
    CfsMasterRepository cfsMasterRepository;

    @Autowired
    private
    CacheRepository cacheRepository;

    @Autowired
    private
    CfsMasterValidator cfsMasterValidator;

    @Autowired
    private
    CfsSearchImpl cfsSearchImpl;


    public BaseDto get(Long id) {

        log.info("get method is called.....[" + id + "]");

        BaseDto baseDto = new BaseDto();

        try {

            CFSMaster cfsmaster = cacheRepository.get(SaaSUtil.getSaaSId(), CFSMaster.class.getName(), id, cfsMasterRepository);

            if (cfsmaster.getPortMaster() != null) {
                cfsmaster.getPortMaster().setEdiList(null);
            }
            ValidateUtil.notNull(cfsmaster, ErrorCode.MASTER_CFS_NOT_FOUND);

            log.info("Successfully getting CFS by id.....[" + cfsmaster.getId() + "]");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(cfsmaster);

        } catch (RestException ex) {

            log.error("Exception in get method ", ex);

            baseDto.setResponseCode(ex.getMessage());

        } catch (Exception e) {

            log.error("Exception in get method ", e);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);
    }


    public BaseDto create(CFSMaster cfsMaster) {

        log.info("Create method is called.....");

        BaseDto baseDto = new BaseDto();

        try {

            cfsMasterValidator.validate(cfsMaster);


            // Saving in the cache...
            cacheRepository.save(SaaSUtil.getSaaSId(), CFSMaster.class.getName(), cfsMaster, cfsMasterRepository);

            log.info("CFS Saved successfully.....");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(ErrorCode.SUCCESS);

        } catch (RestException exception) {

            log.error("RestException occured with error code ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (Exception exception) {

            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Exception Cause 1 ::: " + exceptionCause1);

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            if (exceptionCause1.contains(UniqueKey.UK_CFSMAS_CFSCODE)) {
                baseDto.setResponseCode(ErrorCode.MASTER_CFS_ALREADY_CODE);
            }

            if (exceptionCause1.contains(UniqueKey.UK_CFSMAS_CFSNAME)) {
                baseDto.setResponseCode(ErrorCode.MASTER_CFS_ALREADY_NAME);
            }
        }
        return appUtil.setDesc(baseDto);
    }


    public BaseDto update(CFSMaster cfsMaster) {


        BaseDto baseDto = new BaseDto();

        try {

            cfsMasterValidator.validate(cfsMaster);

            cfsMasterRepository.getOne(cfsMaster.getId());

            cacheRepository.save(SaaSUtil.getSaaSId(), CFSMaster.class.getName(), cfsMaster, cfsMasterRepository);

            log.info("CFS Updated successfully.....");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(ErrorCode.SUCCESS);

        } catch (RestException exception) {

            log.error("Exception occured in update method ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (ObjectOptimisticLockingFailureException e) {

            log.error("Optimistic Lock Exception occured ", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_LOCKED_RECORD);

        } catch (JpaObjectRetrievalFailureException e) {

            log.error("Object Already Deleted................", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_DELETED_RECORD);

        } catch (Exception exception) {

            log.error("Exception occurred " + exception);

            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Exception Cause 1 ::: " + exceptionCause1);

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            if (exceptionCause1.contains(UniqueKey.UK_CFSMAS_CFSCODE)) {
                baseDto.setResponseCode(ErrorCode.MASTER_CFS_ALREADY_CODE);
            }

            if (exceptionCause1.contains(UniqueKey.UK_CFSMAS_CFSNAME)) {
                baseDto.setResponseCode(ErrorCode.MASTER_CFS_ALREADY_NAME);
            }
        }

        return appUtil.setDesc(baseDto);
    }


    public BaseDto delete(Long id) {

        log.info("Delete method is Invoked...[" + id + "]");

        BaseDto baseDto = new BaseDto();

        try {

            cacheRepository.delete(SaaSUtil.getSaaSId(), CFSMaster.class.getName(), id, cfsMasterRepository);

            log.info("CFS Deleted Successfully....[" + id + "]");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

        } catch (DataIntegrityViolationException exception) {

            log.error("Exception occured : ", exception);

            ExceptionHelper helper = new ExceptionHelper(exception);

            log.error("Foreign key reference : " + helper.getFkReference());

            baseDto.setResponseCode(ErrorCode.ERROR_FK_CONSTRAINT);

        } catch (Exception exception) {

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            log.error("Exception in Delete method : ", exception);

            String rootCause = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Root Cause of Exception : " + rootCause);

            if (rootCause.contains("FK_")) {
                baseDto.setResponseCode(ErrorCode.CANNOT_DELETE_FK_RECORD);
            }
        }

        return appUtil.setDesc(baseDto);
    }

    public BaseDto search(CfsSearchDto searchDto) {

        log.info("Search method is called...." + searchDto);

        BaseDto baseDto = new BaseDto();

        try {

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(cfsSearchImpl.search(searchDto));

            log.info("Successfully Searching CFS...");

        } catch (Exception exception) {

            log.error("Exception in Search method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);

    }

    public BaseDto search(SearchRequest searchRequest, Long portId) {

        log.info("Search method is called...." + searchRequest);

        BaseDto baseDto = new BaseDto();

        try {

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(cfsSearchImpl.search(searchRequest, portId));

            log.info("Successfully Searching CFS Master...");

        } catch (Exception exception) {

            log.error("Exception in Search method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);

    }


}
