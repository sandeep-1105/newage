package com.efreightsuite.service;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.DocumentIssueRestrictionSearchDto;
import com.efreightsuite.dto.SearchRespDto;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.model.CompanyMaster;
import com.efreightsuite.model.DocumentIssueRestriction;
import com.efreightsuite.model.UserProfile;
import com.efreightsuite.repository.DocumentIssueRestrictionRepository;
import com.efreightsuite.util.AppUtil;
import com.efreightsuite.util.StaticSearchQuery;
import com.efreightsuite.validation.DocumentIssueRestrictionValidator;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.exception.ExceptionUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class DocumentIssueRestrictionService {

    @Autowired
    private
    AppUtil appUtil;

    @Autowired
    private
    EntityManager em;

    @Autowired
    private
    DocumentIssueRestrictionRepository documentIssueRestrictionRepository;

    @Autowired
    private
    DocumentIssueRestrictionValidator documentIssueRestrictionValidator;

    public BaseDto modifyDocumentIssueRestriction(DocumentIssueRestriction entity) {
        log.info("ShipmentService -> create method is called....");
        BaseDto baseDto = new BaseDto();
        try {
            documentIssueRestrictionValidator.validate(entity);
            entity = documentIssueRestrictionRepository.save(entity);
            baseDto.setResponseCode(ErrorCode.SUCCESS);
            baseDto.setResponseObject(searchById(entity.getId()).getResponseObject());
        } catch (RestException re) {
            log.error("Exception in ShipmentService -> create method ", re);
            baseDto.setResponseCode(re.getMessage());
        } catch (DataIntegrityViolationException divEX) {
            log.error("Exception in DocumentIssueRestrictionService -> create or update method ", divEX);
            baseDto.setResponseCode(ErrorCode.FAILED);

            if (divEX.getMostSpecificCause().toString().contains("UK_DOC_ISS_RES_COM_LOC_SER")) {
                baseDto.setResponseCode(ErrorCode.DOCUMENT_ISSUE_RESTRICTION_SERVICE_COMP_DUPLICATE);
            } else if (divEX.getMostSpecificCause().toString().contains("FK_DOC_ISSUE_RES_SERVICE_ID")) {
                baseDto.setResponseCode(ErrorCode.DOCUMENT_ISSUE_RESTRICTION_SERVICE_INVALID);
            } else if (divEX.getMostSpecificCause().toString().contains("FK_DOC_ISSUE_RES_LOCATION_ID")) {
                baseDto.setResponseCode(ErrorCode.DOCUMENT_ISSUE_RESTRICTION_LOCATION_INVALID);
            } else if (divEX.getMostSpecificCause().toString().contains("FK_DOC_ISSUE_RES_COMPANY_ID")) {
                baseDto.setResponseCode(ErrorCode.DOCUMENT_ISSUE_RESTRICTION_COMPANY_INVALID);
            } else if (divEX.getMostSpecificCause().toString().contains("COMP_UK_DOC_ISSUE_RES_COMPANY_lOC_SERVICE")) {
                baseDto.setResponseCode(ErrorCode.COMP_UK_DOC_ISSUE_RES_COMPANY_lOC_SERVICE);
            } else if (divEX.getMostSpecificCause().toString().contains("FK_DOC_ISS_RES_DOC_ID")) {
                baseDto.setResponseCode(ErrorCode.FK_DOC_ISS_RES_DOC_ID);
            } else if (divEX.getMostSpecificCause().toString().contains("FK_DOC_ISS_RES_TOS_ID")) {
                baseDto.setResponseCode(ErrorCode.FK_DOC_ISS_RES_TOS_ID);
            } else if (divEX.getMostSpecificCause().toString().contains("UK_DOC_ISS_RES_DOC_TOS")) {
                baseDto.setResponseCode(ErrorCode.UK_DOC_ISS_RES_DOC_TOS);
            }
        } catch (Exception exception) {
            log.error("Exception in DocumentIssueRestrictionService -> create or update method ", exception);
            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(exception);
            log.error("Exception Cause 1 ::: " + exceptionCause1);
            baseDto.setResponseCode(ErrorCode.FAILED);

            if (exceptionCause1.contains("UK_DOC_ISS_RES_COM_lOC_SER")) {
                baseDto.setResponseCode(ErrorCode.DOCUMENT_ISSUE_RESTRICTION_SERVICE_COMP_DUPLICATE);
            }
            if (exceptionCause1.contains("FK_DOC_ISSUE_RES_SERVICE_ID")) {
                baseDto.setResponseCode(ErrorCode.DOCUMENT_ISSUE_RESTRICTION_SERVICE_INVALID);
            }
            if (exceptionCause1.contains("FK_DOC_ISSUE_RES_LOCATION_ID")) {
                baseDto.setResponseCode(ErrorCode.DOCUMENT_ISSUE_RESTRICTION_LOCATION_INVALID);
            }
            if (exceptionCause1.contains("FK_DOC_ISSUE_RES_COMPANY_ID")) {
                baseDto.setResponseCode(ErrorCode.DOCUMENT_ISSUE_RESTRICTION_COMPANY_INVALID);
            }

        }
        return appUtil.setDesc(baseDto);
    }

    public BaseDto searchById(Long id) {

        log.info("DocumentIssueRestrictionService -> searchById method is called....");

        BaseDto baseDto = new BaseDto();

        try {

            DocumentIssueRestriction entity = documentIssueRestrictionRepository.findById(id);
            baseDto.setResponseCode(ErrorCode.SUCCESS);
            baseDto.setResponseObject(entity);

        } catch (RestException re) {
            log.error("Exception in DocumentIssueRestrictionService -> searchById method ", re);
            baseDto.setResponseCode(re.getMessage());
        } catch (Exception e) {
            log.error("Exception in DocumentIssueRestrictionService -> searchById method ", e);
            baseDto.setResponseCode(ErrorCode.FAILED);
        }
        return appUtil.setDesc(baseDto);
    }

    public BaseDto searchByCriteria(DocumentIssueRestrictionSearchDto searchData) {

        log.info("DocumentIssueRestrictionService -> searchByCriteria method is called....");

        BaseDto baseDto = new BaseDto();

        try {
            UserProfile loggedInUser = AuthService.getCurrentUser();
            CompanyMaster selectedCompany = loggedInUser.getSelectedCompany();
            String hql = " where di.companyMaster.id=" + selectedCompany.getId();

            if (searchData.getServiceMaster() != null && searchData.getServiceMaster().trim().length() != 0) {
                hql = hql + " and UPPER(di.serviceMaster.serviceName) like '" + searchData.getServiceMaster().trim().toUpperCase() + "%' ";
            }
            if (searchData.getLocationMaster() != null && searchData.getLocationMaster().trim().length() != 0) {
                hql = hql + " and UPPER(di.locationMaster.locationName) like '" + searchData.getLocationMaster().trim().toUpperCase() + "%' ";
            }
            if (searchData.getSortByColumn() != null && searchData.getOrderByType() != null && searchData.getSortByColumn().trim().length() != 0
                    && searchData.getOrderByType().trim().length() != 0) {
                hql = hql + " order by " + searchData.getSortByColumn().trim() + " " + searchData.getOrderByType().trim();
            } else {
                hql = hql + " order by di.id DESC";
            }
            log.info("Search HQL : [" + hql + "]");
            Query query = em.createQuery(StaticSearchQuery.DocumentIssueRestrictionSearch + hql);
            query.setFirstResult(searchData.getSelectedPageNumber() * searchData.getRecordPerPage());
            query.setMaxResults(searchData.getRecordPerPage());


            String countQ = "Select count (di.id) from DocumentIssueRestriction di  " + hql;
            Query countQuery = em.createQuery(countQ);
            Long countTotal = (Long) countQuery.getSingleResult();

            SearchRespDto searchRespDto = new SearchRespDto(countTotal, query.getResultList());
            searchRespDto.setRecordPerPage(searchData.getRecordPerPage());
            searchRespDto.setSelectedPageNumber(searchData.getSelectedPageNumber());
            baseDto.setResponseCode(ErrorCode.SUCCESS);
            baseDto.setResponseObject(searchRespDto);

        } catch (RestException re) {
            log.error("Exception in DocumentIssueRestrictionService -> searchById method ", re);
            baseDto.setResponseCode(re.getMessage());
        } catch (Exception e) {
            log.error("Exception in DocumentIssueRestrictionService -> searchById method ", e);
            baseDto.setResponseCode(ErrorCode.FAILED);
        }
        return appUtil.setDesc(baseDto);
    }

}

