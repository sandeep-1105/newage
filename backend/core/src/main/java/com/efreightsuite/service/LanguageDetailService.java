package com.efreightsuite.service;

import java.util.List;

import com.efreightsuite.common.component.CacheRepository;
import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.LanguageDetailSearchReqDto;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.exception.UniqueKey;
import com.efreightsuite.model.Language;
import com.efreightsuite.model.LanguageDetail;
import com.efreightsuite.repository.DefaultMasterDataRepository;
import com.efreightsuite.repository.LanguageDetailRepository;
import com.efreightsuite.repository.LanguageRepository;
import com.efreightsuite.search.LanguageDetailSearchService;
import com.efreightsuite.util.AppUtil;
import com.efreightsuite.util.SaaSUtil;
import com.efreightsuite.validation.LanguageDetailValidator;
import com.efreightsuite.validation.SearchDtoValidator;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.exception.ExceptionUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.ObjectOptimisticLockingFailureException;
import org.springframework.orm.jpa.JpaObjectRetrievalFailureException;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class LanguageDetailService {

    @Autowired
    private
    AppUtil appUtil;

    @Autowired
    private
    LanguageDetailRepository languageDetailRepository;

    @Autowired
    private
    CacheRepository cacheRepository;

    @Autowired
    SearchDtoValidator searchDtoValidator;

    @Autowired
    private
    LanguageDetailSearchService languageDetailSearchService;

    @Autowired
    private
    LanguageRepository languageRepo;

    @Autowired
    private
    LanguageDetailValidator languageDetailValidator;

    @Autowired
    DefaultMasterDataRepository defaultMasterDataRepository;


    public BaseDto search(LanguageDetailSearchReqDto searchDto) {


        log.info("search method is called....");

        BaseDto baseDto = new BaseDto();

        try {

            baseDto.setResponseObject(languageDetailSearchService.search(searchDto));
            baseDto.setResponseCode(ErrorCode.SUCCESS);

            log.info("Successfully getting list of Language code ...");

        } catch (RestException exception) {

            log.error("Exception in search method ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (Exception e) {

            log.error("Exception in search method ", e);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);
    }

    public BaseDto get(Long id) {

        log.info("LanguageDetailService.get method is called [ " + id + " ]");

        BaseDto baseDto = new BaseDto();

        try {

            LanguageDetail languageDetail = cacheRepository.get(SaaSUtil.getSaaSId(), LanguageDetail.class.getName(), id, languageDetailRepository);

            log.info("Successfully getting language detail based on id [ " + id + " ]");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(languageDetail);

        } catch (Exception e) {

            log.error("Exception in LanguageDetailService.get method", e);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);
    }

    public BaseDto get(String languageCode) {

        log.info("LanguageDetailService.get method is called....[" + languageCode + "]");

        BaseDto baseDto = new BaseDto();

        try {

            LanguageDetail languageDetail = languageDetailRepository.findByLanguageCode(languageCode);

            log.info("Successfully getting Language by code...[" + languageCode + "]");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(languageDetail);

        } catch (Exception e) {

            log.error("Exception in LanguageDetailService.get method", e);

            baseDto.setResponseCode(ErrorCode.FAILED);

        }

        return appUtil.setDesc(baseDto);
    }

    public BaseDto create(List<LanguageDetail> languageDetailList) {

        log.info("LanguageDetailService.create method is called.....");

        BaseDto baseDto = new BaseDto();

        try {

            if (languageDetailList != null && languageDetailList.size() != 0) {

                for (LanguageDetail languageDetail : languageDetailList) {
                    languageDetailValidator.validate(languageDetail);
                }

                languageDetailRepository.save(languageDetailList);

                asyncLoadCache();
                log.info("Language Detail List Saved successfully..... ");

                baseDto.setResponseCode(ErrorCode.SUCCESS);

                baseDto.setResponseObject(ErrorCode.SUCCESS);

            } else {
                log.info("There is no language detail given....");
            }


        } catch (RestException exception) {

            log.error("Exception in LanguageDetailService.create method ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (Exception exception) {

            log.error("Exception in LanguageDetailService.create method ", exception);

            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Exception Cause 1 : " + exceptionCause1);

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            if (exceptionCause1.contains(UniqueKey.UK_LANGDETAIL_CODE)) {
                baseDto.setResponseCode(ErrorCode.LANGUAGE_CODE_EXIT);
            }
        }

        return appUtil.setDesc(baseDto);
    }

    @Async
    private void asyncLoadCache() {

        loadCache();
    }

    public BaseDto update(LanguageDetail languageDetail) {

        log.info("LanguageDetailService.update method is called....");

        BaseDto baseDto = new BaseDto();

        try {


            LanguageDetail existingLanguageDetail = languageDetailRepository.getOne(languageDetail.getId());

            existingLanguageDetail.setLanguageDesc(languageDetail.getLanguageDesc());
            existingLanguageDetail.setFlagged(languageDetail.getFlagged());

            existingLanguageDetail = languageDetailRepository.save(existingLanguageDetail);
            asyncLoadCache();
            baseDto.setResponseObject(existingLanguageDetail);

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            log.info("Language Detail updated successfully...");

        } catch (RestException exception) {

            log.error("Exception LanguageDetailService.update method ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (ObjectOptimisticLockingFailureException exception) {

            log.error("Exception in LanguageDetailService.update method ", exception);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_LOCKED_RECORD);

        } catch (JpaObjectRetrievalFailureException exception) {

            log.error("Exception in LanguageDetailService.update method ", exception);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_DELETED_RECORD);

        } catch (Exception exception) {

            log.error("Exception in LanguageDetailService.update method ", exception);


            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Exception Cause 1 : " + exceptionCause1);

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            if (exceptionCause1.contains(UniqueKey.UK_LANGDETAIL_CODE)) {
                baseDto.setResponseCode(ErrorCode.LANGUAGE_CODE_EXIT);
            }
        }

        return appUtil.setDesc(baseDto);
    }

    public BaseDto getNls(String language) {
        BaseDto baseDto = new BaseDto();

        baseDto.setResponseObject(cacheRepository.getNLS(SaaSUtil.getSaaSId(), language));
        baseDto.setResponseCode(ErrorCode.SUCCESS);

        return appUtil.setDesc(baseDto);
    }


    public BaseDto loadCache() {

        BaseDto baseDto = new BaseDto();
        int count = 0;

        for (Language language : languageRepo.findAll()) {
            cacheRepository.pushNLS(SaaSUtil.getSaaSId(), language, languageDetailRepository.findAllByLanguage(language));
            count++;
        }

        baseDto.setResponseCode(ErrorCode.SUCCESS);
        baseDto.setResponseObject(count + " langauge are loaded in the cache.");
        return appUtil.setDesc(baseDto);
    }


    public BaseDto delete(Long id) {

        log.info("Delete method is Invoked...[" + id + "]");

        BaseDto baseDto = new BaseDto();

        try {

            cacheRepository.delete(SaaSUtil.getSaaSId(), LanguageDetail.class.getName(), id, languageDetailRepository);

            log.info("Language Detail Error Code Deleted Successfully....[" + id + "]");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

        } catch (Exception exception) {

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            log.error("Exception in Delete method : ", exception);

            String rootCause = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Root Cause of Exception : " + rootCause);

            if (rootCause.contains("FK_")) {
                baseDto.setResponseCode(ErrorCode.CANNOT_DELETE_FK_RECORD);
            }
        }

        return appUtil.setDesc(baseDto);
    }

}
