package com.efreightsuite.service;

import java.util.List;

import com.efreightsuite.common.component.CacheRepository;
import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.model.EmployeeMaster;
import com.efreightsuite.model.LocationMaster;
import com.efreightsuite.model.PartyMaster;
import com.efreightsuite.model.PartyServiceMaster;
import com.efreightsuite.model.ServiceMaster;
import com.efreightsuite.model.TosMaster;
import com.efreightsuite.repository.CompanyMasterRepository;
import com.efreightsuite.repository.CountryMasterRepository;
import com.efreightsuite.repository.EmployeeMasterRepository;
import com.efreightsuite.repository.LocationMasterRepository;
import com.efreightsuite.repository.PartyMasterRepository;
import com.efreightsuite.repository.PartyServiceMasterRepository;
import com.efreightsuite.repository.ServiceMasterRepository;
import com.efreightsuite.repository.TosMasterRepository;
import com.efreightsuite.util.AppUtil;
import com.efreightsuite.util.SaaSUtil;
import com.efreightsuite.util.ValidateUtil;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.exception.ExceptionUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.orm.ObjectOptimisticLockingFailureException;
import org.springframework.orm.jpa.JpaObjectRetrievalFailureException;
import org.springframework.stereotype.Service;

/**
 * ClassName Purpose CreatedBy/ModifiedBy Date Version
 * <p>
 * PartyServiceMasterService CRUD operation for PartyServiceMaster Devendrachary
 * M 25-02-15 0.0
 */
@Service
@Log4j2
public class PartyServiceMasterService {

    @Autowired
    private
    PartyServiceMasterRepository partyServiceMasterRepository;

    @Autowired
    private
    AppUtil appUtil;

    @Autowired
    private
    CacheRepository cacheRepository;

    @Autowired
    private
    PartyMasterRepository partyMasterRepository;

    @Autowired
    private
    ServiceMasterRepository serviceMasterRepository;

    @Autowired
    private
    EmployeeMasterRepository employeeMasterRepository;

    @Autowired
    CompanyMasterRepository companyMasterRepository;

    @Autowired
    CountryMasterRepository countryMasterRepository;

    @Autowired
    private
    LocationMasterRepository locationMasterRepository;

    @Autowired
    private
    TosMasterRepository tosMasterRepository;

    /*
     * Returns all the CostCenter object as a list
     *
     * @Returns BaseDto
     */
    public BaseDto getAll() {

        log.info("PartyServiceMasterService.getAll method is start.");

        BaseDto baseDto = new BaseDto();

        try {

            List<PartyServiceMaster> partyServiceMasterList = partyServiceMasterRepository.findAll();

            ValidateUtil.notEmpty(partyServiceMasterList, ErrorCode.PARTYSERVICE_LIST_EMPTY);

            baseDto.setResponseCode(ErrorCode.SUCCESS);
            baseDto.setResponseObject(partyServiceMasterList);

        } catch (RestException re) {
            log.error("Exception in getAll method ", re);
            baseDto.setResponseCode(re.getMessage());

        } catch (Exception e) {
            log.error("Exception in PartyServiceMasterService.getAll method : ", e);
            baseDto.setResponseCode(ErrorCode.FAILED);

        }
        log.info("PartyServiceMasterService.getAll method is end.");
        return appUtil.setDesc(baseDto);
    }

    /**
     * Method - get
     *
     * @param id
     * @return baseDto
     */

    public BaseDto get(Long id) {
        log.info("PartyServiceMasterService.get method is start.");

        BaseDto baseDto = new BaseDto();
        try {

            PartyServiceMaster partyServiceMaster = partyServiceMasterRepository.findById(id);

            ValidateUtil.notNull(partyServiceMaster, ErrorCode.PARTYSERVICE_ID_INVALID);

            log.info("Successfully getting PartyServiceMaster by id...");

            baseDto.setResponseCode(ErrorCode.SUCCESS);
            baseDto.setResponseObject(partyServiceMaster);

        } catch (RestException re) {
            log.error("RestException in PartyServiceMasterService.get method while getting the PartyServiceMaster : "
                    + re);
            baseDto.setResponseCode(ErrorCode.PARTYSERVICE_ID_INVALID);
        } catch (Exception e) {
            log.error("Exception in PartyServiceMasterService.get method while getting the PartyServiceMaster : ", e);
            baseDto.setResponseCode(ErrorCode.FAILED);
        }
        log.info("PartyServiceMasterService.get method is end.");
        return appUtil.setDesc(baseDto);
    }

	/*
     * create - This method creates a new PartyServiceMaster
	 *
	 * @Param PartyServiceMaster master entity
	 *
	 * @Return BaseDto
	 */

    public BaseDto create(PartyServiceMaster partyServiceMaster) {

        log.info("PartyServiceMasterService.create method is start.");
        BaseDto baseDto = new BaseDto();
        try {


            // PartyService Party
            PartyMaster partyMaster = cacheRepository.get(SaaSUtil.getSaaSId(), PartyMaster.class.getName(),
                    partyServiceMaster.getPartyMaster().getId(), partyMasterRepository);
            partyServiceMaster.setPartyMaster(partyMaster);

            // PartyService Service
            ServiceMaster serviceMaster = cacheRepository.get(SaaSUtil.getSaaSId(), ServiceMaster.class.getName(),
                    partyServiceMaster.getServiceMaster().getId(), serviceMasterRepository);
            partyServiceMaster.setServiceMaster(serviceMaster);

            // PartyService Salesman
            EmployeeMaster employeeMaster = cacheRepository.get(SaaSUtil.getSaaSId(), EmployeeMaster.class.getName(),
                    partyServiceMaster.getSalesman().getId(), employeeMasterRepository);
            partyServiceMaster.setSalesman(employeeMaster);

            // PartyService Customer Service
            EmployeeMaster customerService = cacheRepository.get(SaaSUtil.getSaaSId(), EmployeeMaster.class.getName(),
                    partyServiceMaster.getCustomerService().getId(), employeeMasterRepository);
            partyServiceMaster.setCustomerService(customerService);

            // PartyService Location
            LocationMaster locationMaster = cacheRepository.get(SaaSUtil.getSaaSId(), LocationMaster.class.getName(),
                    partyServiceMaster.getLocationMaster().getId(), locationMasterRepository);
            partyServiceMaster.setLocationMaster(locationMaster);

            // PartyService Tos
            TosMaster tosMaster = cacheRepository.get(SaaSUtil.getSaaSId(), TosMaster.class.getName(),
                    partyServiceMaster.getTosMaster().getId(), tosMasterRepository);
            partyServiceMaster.setTosMaster(tosMaster);

            partyServiceMaster = cacheRepository.save(SaaSUtil.getSaaSId(), PartyServiceMaster.class.getName(),
                    partyServiceMaster, partyServiceMasterRepository);

            log.info("PartyServiceMaster Saved successfully.....");
            baseDto.setResponseCode(ErrorCode.SUCCESS);
            baseDto.setResponseObject(partyServiceMaster);

        } catch (RestException re) {

            log.error("Exception in PartyServiceMasterService.create ", re);

            baseDto.setResponseCode(re.getMessage());
        } catch (DataIntegrityViolationException exception) {

            log.error(
                    "DataIntegrityViolationException in PartyServiceMasterService.create method while saving the PartyServiceMaster",
                    exception);

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Exception Cause 1 ::: " + exceptionCause1);

            // General error condition
            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

        } catch (Exception e) {

            log.error("Exception PartyServiceMasterService.create method while saving the PartyServiceMaster : ", e);

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);
        }

        log.info("PartyServiceMasterService.create method is end.");
        return appUtil.setDesc(baseDto);
    }

    public BaseDto update(PartyServiceMaster existingPartyServiceMaster) {
        log.info("PartyServiceMasterService.update method is start.");
        BaseDto baseDto = new BaseDto();

        try {

            partyServiceMasterRepository.getOne(existingPartyServiceMaster.getId());

            PartyServiceMaster partyServiceMasterUpdateResponse = cacheRepository.save(SaaSUtil.getSaaSId(),
                    PartyServiceMaster.class.getName(), existingPartyServiceMaster, partyServiceMasterRepository);

            log.info("PartyService updated successfully...");

            baseDto.setResponseCode(ErrorCode.SUCCESS);
            baseDto.setResponseObject(partyServiceMasterUpdateResponse);

        } catch (RestException exception) {

            log.error("RestException in Delete method : ", exception);

            baseDto.setResponseCode(exception.getMessage());
        } catch (DataIntegrityViolationException exception) {
            log.error(
                    "DataIntegrityViolationException in PartyServiceMasterService.update method while updating the PartyServiceMaster : ",
                    exception);
            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Exception Cause 1 ::: " + exceptionCause1);
            // General error condition
            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

        } catch (ObjectOptimisticLockingFailureException e) {
            log.error("Error in editing Service", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_LOCKED_RECORD);
        } catch (JpaObjectRetrievalFailureException e) {
            log.error("Error in editing Service", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_DELETED_RECORD);
        } catch (Exception e) {

            log.error("Failed to save the PartyService ..", e);

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

        }

        log.info("PartyServiceMasterService.update method is end.");
        return appUtil.setDesc(baseDto);
    }

}
