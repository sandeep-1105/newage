package com.efreightsuite.service.mailer;

import com.efreightsuite.common.component.EmailRequestService;
import com.efreightsuite.enumeration.DimensionUnit;
import com.efreightsuite.enumeration.TemplateType;
import com.efreightsuite.enumeration.TransportMode;
import com.efreightsuite.enumeration.YesNo;
import com.efreightsuite.model.*;
import com.efreightsuite.repository.*;
import com.efreightsuite.util.AppUtil;
import com.efreightsuite.util.ReportUtil;
import com.efreightsuite.util.SaaSUtil;
import com.efreightsuite.util.TimeUtil;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static java.util.stream.Collectors.toList;

@Service
@Log4j2
public class EnquiryMailer {

    @Value("${efs.application.base.url}")
    private
    String applicationBaseUrl;

    @Autowired
    private
    MailerUtil mailerUtil;

    @Autowired
    private
    AppUtil appUtil;

    @Autowired
    private
    ReportUtil reportUtil;

    @Autowired
    private
    EmailRequestService emailRequestService;

    @Autowired
    private
    EmailTemplateRepository emailTemplateRepository;

    @Autowired
    private
    LocationMasterRepository locationMasterRepository;

    @Autowired
    private
    RateRequestRepository rateRequestRepository;

    @Autowired
    private
    EnquiryDetailRepository enquiryDetailRepository;

    @Autowired
    private
    EmployeeMasterRepository employeeMasterRepository;

    @Autowired
    private
    EnquiryLogRepository enquiryLogRepository;

    @Async
    public void sendEnquiryMailToSalesDepo(EnquiryLog enquiryLog, UserProfile userProfile) {
        log.info("sendEnquiryAutoMail start: ");

        Map<Long, List<EnquiryDetail>> groupService = edListGroupByServiceName(enquiryLog.getEnquiryDetailList());

        if (groupService == null || groupService.size() == 0) {
            log.info("New Service is not available");
            return;
        }

        EmailTemplate emailTemplate = emailTemplateRepository.findByTemplateType(TemplateType.ENQUIRYCONFTOSALSE);

        if (emailTemplate == null) {
            log.error("Enquiry confirmation to sales man mail is not available");
            return;
        }

        populateEnquiryLogObj(enquiryLog);

        Map<String, Object> mapSubject = new HashMap<>();
        mapSubject.put("#ENQUIRY_NO#", enquiryLog.getEnquiryNo());

        Map<String, Object> mapBody = new HashMap<>();

        mapBody.put("#COMPANY_NAME#", getCompanyName(enquiryLog.getCompanyMaster()));
        mapBody.put("#ENQUIRY_NO#", enquiryLog.getEnquiryNo());
        mapBody.put("#CUST_NAME#", enquiryLog.getPartyMaster().getPartyName());
        mapBody.put("#QUOTE_BY#", TimeUtil.getLocationDate(enquiryLog.getLocationMaster(), enquiryLog.getQuoteBy()));
        mapBody.put("#SERVICES_TABLES#", airServiceTemplate(groupService));
        mapBody.put("#EMP_NAME#", enquiryLog.getLoggedBy().getEmployeeName());
        mapBody.put("#EMP_EMAIL#", enquiryLog.getLoggedBy().getEmail());
        mapBody.put("#EMP_TELNO#", enquiryLog.getLoggedBy().getEmployeePhoneNo());

        // sending email
        EmailRequest emailRequest = new EmailRequest();
        emailRequest.setFromEmailId(emailTemplate.getFromEmailId());
        emailRequest.setFromEmailIdPassword(emailTemplate.getFromEmailPassword());
        emailRequest.setToEmailIdList(enquiryLog.getSalesCoOrdinator().getEmail());
        if (enquiryLog.getSalesman() != null) {
            emailRequest.setCcEmailIdList(enquiryLog.getSalesman().getEmail());
        }
        emailRequest.setEmailType(emailTemplate.getEmailType());
        emailRequest.setPort(emailTemplate.getPort());
        emailRequest.setSmtpServerAddress(emailTemplate.getSmtpServerAddress());

        emailRequest.setSubject(emailTemplate.getSubject());
        emailRequest.setEmailBody(emailTemplate.getTemplate());

        for (String key : mapSubject.keySet()) {
            emailRequest.setSubject(emailRequest.getSubject().replaceAll(key, mapSubject.get(key) == null ? "" : mapSubject.get(key).toString()));
        }

        for (String key : mapBody.keySet()) {
            emailRequest.setEmailBody(emailRequest.getEmailBody().replaceAll(key, mapBody.get(key) == null ? "" : mapBody.get(key).toString()));
        }

        emailRequestService.saveAndSendEmail(emailRequest, userProfile, mailerUtil.getLogo(enquiryLog.getSalesCoOrdinator().getLocationMaster()));


        log.info("sendEnquiryAutoMail  end");
    }

    private String airServiceTemplate(Map<Long, List<EnquiryDetail>> groupService) {

        StringBuilder allServicesTables = new StringBuilder();
        String cssClass = "border: 1px solid #ba68c8;padding: 4px;border-collapse:collapse;";
        String cssClassDouble = "border: 1px solid #ba68c8;padding: 4px;border-collapse:collapse;text-align: right;";


        for (Long serviceId : groupService.keySet()) {
            List<EnquiryDetail> arrayList = groupService.get(serviceId);
            String tableHeading = getTableHdrHtmlString(groupService.get(serviceId).get(0).getServiceMaster());

            int srNo = 1;
            String sNoTd = "";
            String tosTd = "";
            String commo_groupTd = "";
            String originTd = "";
            String destinationTd = "";
            String grossWtTd = "";
            String volumeWtTd = "";
            String remarksTd = "";

            StringBuilder row = new StringBuilder();

            for (EnquiryDetail ed : arrayList) {

                sNoTd = "<td style=" + "'" + cssClass + "'" + ">" + srNo + "</td>";

                if (ed.getTosMaster() != null) {
                    tosTd = "<td style=" + "'" + cssClass + "'" + ">" + ed.getTosMaster().getTosName() + "</td>";
                } else {
                    tosTd = "<td style=" + "'" + cssClass + "'" + ">-</td>";
                }

                if (ed.getCommodityMaster() != null) {
                    commo_groupTd = "<td style=" + "'" + cssClass + "'" + ">" + ed.getCommodityMaster().getHsName() + "</td>";
                } else {
                    commo_groupTd = "<td style=" + "'" + cssClass + "'" + ">-</td>";
                }
                if (ed.getFromPortMaster() != null) {
                    originTd = "<td style=" + "'" + cssClass + "'" + ">" + ed.getFromPortMaster().getPortName() + "</td>";
                } else {
                    originTd = "<td style=" + "'" + cssClass + "'" + ">-</td>";
                }

                if (ed.getGrossWeight() == 0) {
                    grossWtTd = "<td style=" + "'" + cssClassDouble + "'" + ">0.00</td>";
                } else {
                    grossWtTd = "<td style=" + "'" + cssClassDouble + "'" + ">" + reportUtil.twoDecimalDoubleValueFormat(ed.getGrossWeight()) + "</td>";
                }

                if (ed.getVolWeight() == 0) {
                    volumeWtTd = "<td style=" + "'" + cssClassDouble + "'" + ">0.00</td>";
                } else {
                    volumeWtTd = "<td style=" + "'" + cssClassDouble + "'" + ">" + reportUtil.twoDecimalDoubleValueFormat(ed.getVolWeight()) + "</td>";
                }

                if (ed.getToPortMaster() != null) {
                    destinationTd = "<td style=" + "'" + cssClass + "'" + ">" + ed.getToPortMaster().getPortName() + "</td>";
                } else {
                    destinationTd = "<td style=" + "'" + cssClass + "'" + ">-</td>";
                }

                if (ed.getNotes() != null) {
                    remarksTd = "<td style=" + "'" + cssClass + "'" + ">" + ed.getNotes() + "</td>";
                } else {
                    remarksTd = "<td style=" + "'" + cssClass + "'" + ">-</td>";
                }

                String tr = "<tr style=" + "'border-collapse:collapse;" + "'>\n" + sNoTd + "\n" + originTd + "\n" + destinationTd + "\n" + tosTd + "\n" + commo_groupTd + "\n" + grossWtTd + "\n" + volumeWtTd + "\n" + remarksTd + "\n" + "</tr>";

                row.append(tr).append("\n");
                srNo++;
            }

            tableHeading += row + "</table>\n";
            allServicesTables.append(tableHeading);

        }

        return allServicesTables.toString();
    }

    private Map<Long, List<EnquiryDetail>> edListGroupByServiceName(List<EnquiryDetail> detailList) {
        Map<Long, List<EnquiryDetail>> groupService = new HashMap<>();

        for (EnquiryDetail ed : detailList) {

            if (ed.getServiceMaster().getTransportMode() != TransportMode.Air
                    || !ed.getServiceMaster().getTransportMode().equals(TransportMode.Air)) {
                continue;
            }

            if (groupService.get(ed.getServiceMaster().getId()) == null) {
                List<EnquiryDetail> tmpList = new ArrayList<>();
                tmpList.add(ed);
                groupService.put(ed.getServiceMaster().getId(), tmpList);
            } else {
                List<EnquiryDetail> tmpList = groupService.get(ed.getServiceMaster().getId());
                tmpList.add(ed);
                groupService.put(ed.getServiceMaster().getId(), tmpList);
            }
        }

        return groupService;
    }

    private String getTableHdrHtmlString(ServiceMaster serviceMaster) {

        String tableCss = "font-weight:normal;padding:40px;border: 1px #ba68c8;margin-left:20px;width: 750px;margin-top: 20px;text-align: left;font-size: 11px;font-family:Helvetica,arial,sans-serif; border-collapse:collapse;";

        String thHdrCss = "font-weight:normal;border: 1px solid #ba68c8;width: 24px;padding: 4px;border-collapse:collapse;";
        String thHdrCssOrigin = "font-weight:normal;border: 1px solid #ba68c8;width: 120px;padding: 4px;border-collapse:collapse;";
        String thHdrCssDestination = "font-weight:normal;border: 1px solid #ba68c8;width: 120px;padding: 4px;border-collapse:collapse;";
        String thHdrCssTos = "font-weight:normal;border: 1px solid #ba68c8;width: 40px;padding: 4px;border-collapse:collapse;";
        String thHdrCssCommodity = "font-weight:normal;border: 1px solid #ba68c8;width: 140px;padding: 4px;border-collapse:collapse;";
        String thHdrCssGrossWt = "font-weight:normal;border: 1px solid #ba68c8;width: 90px;padding: 4px;border-collapse:collapse;text-align: right;";
        String thHdrCssVolumeWt = "font-weight:normal;border: 1px solid #ba68c8;width: 90px;padding: 4px;border-collapse:collapse;text-align: right;";
        String thHdrCssNoOfContainers = "font-weight:normal;border: 1px solid #ba68c8;width: 180px;padding: 4px;border-collapse:collapse;";


        String thHdrRemarksCss = "font-weight:normal;border: 1px solid #ba68c8;padding: 4px;border-collapse:collapse;width: 88px;";

        String trHdrCss = "background: #d68ee2;color: white;border-collapse:collapse;";

        String serviceTr = "background-color: #d68ee2;color: white;font-weight: 400; ";
        String serviceTh = "padding: -10px;border: 1px solid #ba68c8;border-collapse:collapse;";
        String serviceCol = "";


        if (serviceMaster.getTransportMode().equals(TransportMode.Ocean)) {
            serviceCol = "colspan='7'";
        } else {
            serviceCol = "colspan='8'";
        }


        String serviceP = "font-weight: 500;margin-left: 4px;font-size: 12px;font-family:'Helvetica Neue',Helvetica,arial,sans-serif;";

        String serviceSpan = "font-weight: 600;font-size: 12px;font-family:'Helvetica Neue',Helvetica,arial,sans-serif;";


        String table = "<table style=" + "'" + tableCss + "'" + ">\n";


        table += "<tr style=" + "'" + serviceTr + "'" + ">\n";
        table += "<th " + serviceCol + " style=" + "'" + serviceTh + "'" + ">\n";
        table += "<p style=" + "'" + serviceP + "'" + ">Service :\n";
        table += "<span style=" + "'" + serviceSpan + "'> " + serviceMaster.getServiceName() + " </span></p></th></tr>\n";

        table += "<tr style=" + "'" + trHdrCss + "'" + ">\n";
        table += "<th style=" + "'" + thHdrCss + "'" + ">No</th>\n";
        table += "<th style=" + "'" + thHdrCssOrigin + "'" + ">Origin</th>\n";
        table += "<th style=" + "'" + thHdrCssDestination + "'" + ">Destination</th>\n";
        table += "<th style=" + "'" + thHdrCssTos + "'" + ">TOS</th>\n";
        table += "<th style=" + "'" + thHdrCssCommodity + "'" + ">Commodity Group</th>\n";

        // No. of Containers & Container Type
        if (serviceMaster.getTransportMode().equals(TransportMode.Ocean)) {

            table += "<th style=" + "'" + thHdrCssNoOfContainers + "'" + ">No. of Containers & Container Type</th>\n";

        } else {

            table += "<th style=" + "'" + thHdrCssGrossWt + "'" + ">Gross Wt.(KG)</th>\n";
            table += "<th style=" + "'" + thHdrCssVolumeWt + "'" + ">Volume Wt.(KG)</th>\n";

        }

        table += "<th style=" + "'" + thHdrRemarksCss + "'" + ">Remarks</th>\n<tr/>";
        return table;
    }

    private String getCompanyName(CompanyMaster companyMaster) {
        if (companyMaster == null || companyMaster.getCompanyName() == null) {
            return "";
        }
        return companyMaster.getCompanyName().trim();
    }


    @Async
    @Transactional
    public void sendEnquiryMailToRateRequest(EnquiryLog enquiryLog,
                                             UserProfile userProfile,
                                             String saasId) {

        EmailTemplate emailTemplate = emailTemplateRepository.findByTemplateType(TemplateType.ENQUIRY_RATE_REQUEST);

        if (emailTemplate == null) {
            log.error("Enquiry confirmation to sales man mail is not available");
            return;
        }

        populateEnquiryLogObj(enquiryLog);

        if (enquiryLog != null && enquiryLog.getEnquiryDetailList() != null && enquiryLog.getEnquiryDetailList().size() > 0) {

            for (int i = 0; i < enquiryLog.getEnquiryDetailList().size(); i++) {

                if (enquiryLog.getEnquiryDetailList().get(i).getRateRequestList() != null && enquiryLog.getEnquiryDetailList().get(i).getRateRequestList().size() > 0) {

                    for (int j = 0; j < enquiryLog.getEnquiryDetailList().get(i).getRateRequestList().size(); j++) {

                        if (enquiryLog.getEnquiryDetailList().get(i).getRateRequestList().get(j).getIsMailSent() == null || enquiryLog.getEnquiryDetailList().get(i).getRateRequestList().get(j).getIsMailSent() == YesNo.No) {

                            rateRequestRepository.UpdateMailFalg(enquiryLog.getEnquiryDetailList().get(i).getRateRequestList().get(j).getId());
                            Map<String, Object> mapSubject = new HashMap<>();
                            mapSubject.put("#ENQUIRYNO#", enquiryLog.getEnquiryNo());
                            Map<String, Object> mapBody = new HashMap<>();
                            log.info("Date" + enquiryLog.getLoggedOn());
                            mapBody.put("#DATE#", TimeUtil.getLocationDate(enquiryLog.getLocationMaster(), enquiryLog.getLoggedOn()));
                            mapBody.put("#VENDORNAME#", enquiryLog.getEnquiryDetailList().get(i).getRateRequestList().get(j).getVendor().getPartyName());
                            mapBody.put("#ENQUIRYNO#", enquiryLog.getEnquiryNo());
                            mapBody.put("#ORIGIN#", enquiryLog.getEnquiryDetailList().get(i).getFromPortMaster().getPortName());
                            mapBody.put("#DESTINATION#", enquiryLog.getEnquiryDetailList().get(i).getToPortMaster().getPortName());
                            mapBody.put("#COMMODITY#", enquiryLog.getEnquiryDetailList().get(i).getCommodityMaster() != null ? enquiryLog.getEnquiryDetailList().get(i).getCommodityMaster().getHsName() : "");
                            mapBody.put("#TOS#", enquiryLog.getEnquiryDetailList().get(i).getTosMaster() != null ? enquiryLog.getEnquiryDetailList().get(i).getTosMaster().getTosName() : "");
                            mapBody.put("#REMARKS#", enquiryLog.getEnquiryDetailList().get(i).getNotes());

                            populateDimensions(enquiryLog, i, mapBody);

                            String pickUp = "";
                            String delivery = "";

                            pickUp = (enquiryLog.getEnquiryDetailList().get(i).getPickupAddress() != null ?
                                    enquiryLog.getEnquiryDetailList().get(i).getPickupAddress().getAddressLine1() + "," + (enquiryLog.getEnquiryDetailList().get(i).getPickupAddress().getAddressLine2() != null ? (enquiryLog.getEnquiryDetailList().get(i).getPickupAddress().getAddressLine2() + ",") : " ") + (enquiryLog.getEnquiryDetailList().get(i).getPickupAddress().getAddressLine3() != null ? (enquiryLog.getEnquiryDetailList().get(i).getPickupAddress().getAddressLine3() + ",") : "") + (enquiryLog.getEnquiryDetailList().get(i).getPickupAddress().getCity() != null ? (enquiryLog.getEnquiryDetailList().get(i).getPickupAddress().getCity().getCityName() + ",") : "") + (enquiryLog.getEnquiryDetailList().get(i).getPickupAddress().getState() != null ? (enquiryLog.getEnquiryDetailList().get(i).getPickupAddress().getState().getStateName() + ",") : "") + (enquiryLog.getEnquiryDetailList().get(i).getPickupAddress().getPoBox() != null ? enquiryLog.getEnquiryDetailList().get(i).getPickupAddress().getPoBox() : "") : "");
                            if (pickUp != null && pickUp != "" && pickUp.trim().length() > 0) {
                                pickUp = pickUp.substring(0, pickUp.length() - 1);
                            }
                            delivery = (enquiryLog.getEnquiryDetailList().get(i).getDeliveryAddress() != null ?
                                    enquiryLog.getEnquiryDetailList().get(i).getDeliveryAddress().getAddressLine1() + "," + (enquiryLog.getEnquiryDetailList().get(i).getDeliveryAddress().getAddressLine2() != null ? (enquiryLog.getEnquiryDetailList().get(i).getDeliveryAddress().getAddressLine2() + ",") : "") + (enquiryLog.getEnquiryDetailList().get(i).getDeliveryAddress().getAddressLine3() != null ? (enquiryLog.getEnquiryDetailList().get(i).getDeliveryAddress().getAddressLine3() + ",") : "") + (enquiryLog.getEnquiryDetailList().get(i).getDeliveryAddress().getCity() != null ? (enquiryLog.getEnquiryDetailList().get(i).getDeliveryAddress().getCity().getCityName() + ",") : "") + (enquiryLog.getEnquiryDetailList().get(i).getDeliveryAddress().getState() != null ? (enquiryLog.getEnquiryDetailList().get(i).getDeliveryAddress().getState().getStateName() + ",") : "") + (enquiryLog.getEnquiryDetailList().get(i).getDeliveryAddress().getPoBox() != null ? enquiryLog.getEnquiryDetailList().get(i).getDeliveryAddress().getPoBox() : "") : "");

                            if (delivery != null && delivery != "" && delivery.trim().length() > 0) {
                                delivery = delivery.substring(0, delivery.length() - 1);
                            }

                            mapBody.put("#PICKUPPLACE#", pickUp);
                            mapBody.put("#DELIVERYPLACE#", delivery);


                            mapBody.put("#PHONENO#", enquiryLog.getLoggedBy().getEmployeePhoneNo());
                            mapBody.put("#SENDEREMAIL#", enquiryLog.getLoggedBy().getEmail());
                            mapBody.put("#SALESCORDINATOR#", enquiryLog.getLoggedBy().getEmployeeName());
                            mapBody.put("#LOGINCOMPANY#", enquiryLog.getLoggedBy().getCompanyMaster().getCompanyName());
                            mapBody.put("#COMPANY_NAME#", enquiryLog.getLoggedBy().getCompanyMaster().getCompanyName());
                            mapBody.put("#URLLINK#", String.format(applicationBaseUrl + "/public/enquiryRateRequest/enquiry_rate_request.html?saasId=" + "%1$s" + "&rateRequestId=" + "%2$d", saasId, enquiryLog.getEnquiryDetailList().get(i).getRateRequestList().get(j).getId()));

                            // sending email
                            EmailRequest emailRequest = new EmailRequest();

                            String emailSeprate = appUtil.getLocationConfig("single.text.split.by.multiple.email", enquiryLog.getLocationMaster(), false);

                            if (enquiryLog.getEnquiryDetailList().get(i).getRateRequestList().get(j).getEmailId() != null) {
                                String toEmail = (enquiryLog.getEnquiryDetailList().get(i).getRateRequestList().get(j).getEmailId().replaceAll(emailSeprate, ","));
                                emailRequest.setToEmailIdList(toEmail);
                            }

                            emailRequest.setFromEmailId(emailTemplate.getFromEmailId());
                            emailRequest.setFromEmailIdPassword(emailTemplate.getFromEmailPassword());
                            emailRequest.setCcEmailIdList(emailTemplate.getCcEmailAddress());
                            emailRequest.setBccEmailIdList(emailTemplate.getBccEmailAddress());
                            emailRequest.setEmailType(emailTemplate.getEmailType());
                            emailRequest.setPort(emailTemplate.getPort());
                            emailRequest.setAttachmentList(null);
                            emailRequest.setSmtpServerAddress(emailTemplate.getSmtpServerAddress());
                            emailRequest.setSubject(emailTemplate.getSubject());
                            emailRequest.setEmailBody(emailTemplate.getTemplate());

                            for (String key : mapSubject.keySet()) {
                                emailRequest.setSubject(emailRequest.getSubject().replaceAll(key, mapSubject.get(key) == null ? "" : mapSubject.get(key).toString()));
                            }

                            for (String key : mapBody.keySet()) {
                                emailRequest.setEmailBody(emailRequest.getEmailBody().replaceAll(key, mapBody.get(key) == null ? "" : mapBody.get(key).toString()));
                            }
                            emailRequestService.saveAndSendEmail(emailRequest, userProfile, mailerUtil.getLogo(enquiryLog.getSalesCoOrdinator().getLocationMaster()));

                        }
                    }
                }
            }
        }
    }

    private void populateEnquiryLogObj(EnquiryLog enquiryLog) {
        populateEnquiryDetailList(enquiryLog);
        populateLocationMaster(enquiryLog);
        populateEmployeeMaster(enquiryLog);
    }

    private void populateEmployeeMaster(EnquiryLog enquiryLog) {
        if(enquiryLog.getLoggedBy() != null) {
            enquiryLog.setLoggedBy(employeeMasterRepository.findById(enquiryLog.getLoggedBy().getId()));
        }
    }

    private void populateEnquiryDetailList(EnquiryLog enquiryLog) {
        List<Long> idList = enquiryLog.getEnquiryDetailList().stream().map(EnquiryDetail::getId).collect(toList());
        enquiryLog.setEnquiryDetailList(enquiryDetailRepository.findAllByIds(idList));
    }

    private void populateLocationMaster(EnquiryLog enquiryLog) {
        if(enquiryLog.getLocationMaster() != null) {
            enquiryLog.setLocationMaster(locationMasterRepository.findById(enquiryLog.getLocationMaster().getId()));
        }
    }


    private void populateDimensions(EnquiryLog enquiryLog, int i, Map<String, Object> mapBody) {

        boolean isCmKG = (enquiryLog.getEnquiryDetailList().get(i).getDimensionUnit()).equals(DimensionUnit.CENTIMETERORKILOS);
        String dimUnit = " cm";
        String wtUnit = " KG";
        if (!isCmKG) {
            dimUnit = " in";
            wtUnit = " LBS";
        }
        StringBuilder dimension = null;

        for (EnquiryDimension enquiryDimension : enquiryLog.getEnquiryDetailList().get(i).getEnquiryDimensionList()) {
            if(dimension == null) {
                dimension = new StringBuilder();
            }
            dimension.append(
                            "<span>" + "No Of Pieces: " + "</span>" +
                            "<span>" + enquiryDimension.getNoOfPiece() +"</span>" +
                            "<span>" + "  Length: " + "</span>" +
                            "<span>"+ enquiryDimension.getLength() + dimUnit +"</span>" +
                            "<span>" + "  Width: " + "</span>" +
                            "<span>" + enquiryDimension.getWidth() + dimUnit +"</span>" +
                            "<span>" + "  Height: " + "</span>" +
                            "<span>" + enquiryDimension.getHeight() + dimUnit +"</span>" +
                            "<span>" + "  Gross Weight: " + "</span>" +
                            "<span>" + enquiryDimension.getGrossWeight() + wtUnit +"</span>" +
                            "<span>" + "  Volume Weight: " + "</span>" +
                            "<span>" + enquiryDimension.getVolWeight() + wtUnit +"</span>"
            );
            dimension.append("<br>");
        }
        if(dimension != null) {
            mapBody.put("#Dimension#", dimension);
        } else {
            for (EnquiryDetail enquiryDetail : enquiryLog.getEnquiryDetailList()) {
                dimension = new StringBuilder();
                if(enquiryDetail.getGrossWeight() > 0) {
                    dimension.append("<span>" + " Gross Weight: " + "</span>" +
                                    "<span>" + enquiryDetail.getGrossWeight() + wtUnit + "</span>");
                }
                if(enquiryDetail.getVolWeight() > 0) {
                    dimension.append("<span>" + "  Volume Weight: " + "</span>" +
                            "<span>" + enquiryDetail.getVolWeight() + wtUnit + "</span>");
                }
                dimension.append("<br>");
            }
            if(dimension != null) {
                mapBody.put("#Dimension#", dimension);
            }
        }
    }


    @Async
    @Transactional
    public void sendEnquiryUpdateMailToSales(String enquiryNo) {

        EnquiryLog enquiryLog = enquiryLogRepository.findByEnquiryNo(enquiryNo);

        EmailTemplate emailTemplate = emailTemplateRepository.findByTemplateType(TemplateType.ENQUIRY_RATE_REQUEST_TO_SALES);

        if (emailTemplate == null) {
            log.error("Rates update email not sent to sales dept");
            return;
        }

        if (enquiryLog != null && enquiryLog.getEnquiryDetailList() != null && enquiryLog.getEnquiryDetailList().size() > 0) {

            for (int i = 0; i < enquiryLog.getEnquiryDetailList().size(); i++) {

                if (enquiryLog.getEnquiryDetailList().get(i).getRateRequestList() != null && enquiryLog.getEnquiryDetailList().get(i).getRateRequestList().size() > 0) {

                    for (int j = 0; j < enquiryLog.getEnquiryDetailList().get(i).getRateRequestList().size(); j++) {

                        Map<String, Object> mapSubject = new HashMap<>();
                        mapSubject.put("#ENQUIRYNO#", enquiryLog.getEnquiryNo());
                        Map<String, Object> mapBody = new HashMap<>();
                        log.info("Date" + enquiryLog.getLoggedOn());
                        mapBody.put("#DATE#", TimeUtil.getLocationDate(enquiryLog.getLocationMaster(), enquiryLog.getLoggedOn()));
                        mapBody.put("#VENDORNAME#", enquiryLog.getEnquiryDetailList().get(i).getRateRequestList().get(j).getVendor().getPartyName());
                        mapBody.put("#ENQUIRYNO#", enquiryLog.getEnquiryNo());
                        mapBody.put("#ORIGIN#", enquiryLog.getEnquiryDetailList().get(i).getFromPortMaster().getPortName());
                        mapBody.put("#DESTINATION#", enquiryLog.getEnquiryDetailList().get(i).getToPortMaster().getPortName());
                        mapBody.put("#COMMODITY#", enquiryLog.getEnquiryDetailList().get(i).getCommodityMaster() != null ? enquiryLog.getEnquiryDetailList().get(i).getCommodityMaster().getHsName() : "");
                        mapBody.put("#TOS#", enquiryLog.getEnquiryDetailList().get(i).getTosMaster() != null ? enquiryLog.getEnquiryDetailList().get(i).getTosMaster().getTosName() : "");
                        mapBody.put("#REMARKS#", enquiryLog.getEnquiryDetailList().get(i).getNotes());

                        String pickUp = "";
                        String delivery = "";

                        pickUp = (enquiryLog.getEnquiryDetailList().get(i).getPickupAddress() != null ?
                                enquiryLog.getEnquiryDetailList().get(i).getPickupAddress().getAddressLine1() + "," + (enquiryLog.getEnquiryDetailList().get(i).getPickupAddress().getAddressLine2() != null ? (enquiryLog.getEnquiryDetailList().get(i).getPickupAddress().getAddressLine2() + ",") : " ") + (enquiryLog.getEnquiryDetailList().get(i).getPickupAddress().getAddressLine3() != null ? (enquiryLog.getEnquiryDetailList().get(i).getPickupAddress().getAddressLine3() + ",") : "") + (enquiryLog.getEnquiryDetailList().get(i).getPickupAddress().getCity() != null ? (enquiryLog.getEnquiryDetailList().get(i).getPickupAddress().getCity().getCityName() + ",") : "") + (enquiryLog.getEnquiryDetailList().get(i).getPickupAddress().getState() != null ? (enquiryLog.getEnquiryDetailList().get(i).getPickupAddress().getState().getStateName() + ",") : "") + (enquiryLog.getEnquiryDetailList().get(i).getPickupAddress().getPoBox() != null ? enquiryLog.getEnquiryDetailList().get(i).getPickupAddress().getPoBox() : "") : "");
                        if (pickUp != null && pickUp != "" && pickUp.trim().length() > 0) {
                            pickUp = pickUp.substring(0, pickUp.length() - 1);
                        }
                        delivery = (enquiryLog.getEnquiryDetailList().get(i).getDeliveryAddress() != null ?
                                enquiryLog.getEnquiryDetailList().get(i).getDeliveryAddress().getAddressLine1() + "," + (enquiryLog.getEnquiryDetailList().get(i).getDeliveryAddress().getAddressLine2() != null ? (enquiryLog.getEnquiryDetailList().get(i).getDeliveryAddress().getAddressLine2() + ",") : "") + (enquiryLog.getEnquiryDetailList().get(i).getDeliveryAddress().getAddressLine3() != null ? (enquiryLog.getEnquiryDetailList().get(i).getDeliveryAddress().getAddressLine3() + ",") : "") + (enquiryLog.getEnquiryDetailList().get(i).getDeliveryAddress().getCity() != null ? (enquiryLog.getEnquiryDetailList().get(i).getDeliveryAddress().getCity().getCityName() + ",") : "") + (enquiryLog.getEnquiryDetailList().get(i).getDeliveryAddress().getState() != null ? (enquiryLog.getEnquiryDetailList().get(i).getDeliveryAddress().getState().getStateName() + ",") : "") + (enquiryLog.getEnquiryDetailList().get(i).getDeliveryAddress().getPoBox() != null ? enquiryLog.getEnquiryDetailList().get(i).getDeliveryAddress().getPoBox() : "") : "");

                        if (delivery != null && delivery != "" && delivery.trim().length() > 0) {
                            delivery = delivery.substring(0, delivery.length() - 1);
                        }

                        mapBody.put("#PICKUPPLACE#", pickUp);
                        mapBody.put("#DELIVERYPLACE#", delivery);


                        mapBody.put("#PHONENO#", enquiryLog.getLoggedBy().getEmployeePhoneNo());
                        mapBody.put("#SENDEREMAIL#", enquiryLog.getLoggedBy().getEmail());
                        mapBody.put("#SALESCORDINATOR#", enquiryLog.getLoggedBy().getEmployeeName());
                        mapBody.put("#LOGINCOMPANY#", enquiryLog.getLoggedBy().getCompanyMaster().getCompanyName());
                        mapBody.put("#COMPANY_NAME#", enquiryLog.getLoggedBy().getCompanyMaster().getCompanyName());

                        // sending email
                        EmailRequest emailRequest = new EmailRequest();

                        String emailSeprate = appUtil.getLocationConfig("single.text.split.by.multiple.email", enquiryLog.getLocationMaster(), false);

                        if (enquiryLog.getSalesman() != null) {
                            emailRequest.setToEmailIdList(enquiryLog.getSalesman().getEmail());
                        }
                        if (enquiryLog.getSalesCoOrdinator() != null) {
                            emailRequest.setCcEmailIdList(enquiryLog.getSalesCoOrdinator().getEmail());
                        }

                        emailRequest.setFromEmailId(emailTemplate.getFromEmailId());
                        emailRequest.setFromEmailIdPassword(emailTemplate.getFromEmailPassword());
                        emailRequest.setEmailType(emailTemplate.getEmailType());
                        emailRequest.setPort(emailTemplate.getPort());
                        emailRequest.setAttachmentList(null);
                        emailRequest.setSmtpServerAddress(emailTemplate.getSmtpServerAddress());
                        emailRequest.setSubject(emailTemplate.getSubject());
                        emailRequest.setEmailBody(emailTemplate.getTemplate());

                        for (String key : mapSubject.keySet()) {
                            emailRequest.setSubject(emailRequest.getSubject().replaceAll(key, mapSubject.get(key) == null ? "" : mapSubject.get(key).toString()));
                        }

                        for (String key : mapBody.keySet()) {
                            emailRequest.setEmailBody(emailRequest.getEmailBody().replaceAll(key, mapBody.get(key) == null ? "" : mapBody.get(key).toString()));
                        }
                        emailRequestService.sendEmailToSalesForRatesUpdate(emailRequest, mailerUtil.getLogo(enquiryLog.getSalesCoOrdinator().getLocationMaster()));

                    }
                }
            }
        }
    }

}
