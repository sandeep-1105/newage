package com.efreightsuite.service;

import java.util.List;

import com.efreightsuite.common.component.CacheRepository;
import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.CurrencyRateSearchDto;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.exception.UniqueKey;
import com.efreightsuite.model.CurrencyRateMaster;
import com.efreightsuite.repository.CountryMasterRepository;
import com.efreightsuite.repository.CurrencyMasterRepository;
import com.efreightsuite.repository.CurrencyRateMasterRepository;
import com.efreightsuite.search.CurrencyRateSearchImpl;
import com.efreightsuite.util.AppUtil;
import com.efreightsuite.util.SaaSUtil;
import com.efreightsuite.util.ValidateUtil;
import com.efreightsuite.validation.CurrencyRateMasterValidator;
import com.efreightsuite.validation.SearchDtoValidator;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.exception.ExceptionUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.ObjectOptimisticLockingFailureException;
import org.springframework.orm.jpa.JpaObjectRetrievalFailureException;
import org.springframework.stereotype.Service;

@Log4j2
@Service
public class CurrencyRateMasterService {

    @Autowired
    private
    AppUtil appUtil;

    @Autowired
    private
    CurrencyRateMasterRepository currencyRateMasterRepository;

    @Autowired
    private
    CacheRepository cacheRepository;

    @Autowired
    private
    CurrencyRateSearchImpl currencyRateSearchImpl;

    @Autowired
    CountryMasterRepository countryMasterRepository;

    @Autowired
    SearchDtoValidator searchDtoValidator;

    @Autowired
    CurrencyMasterRepository currencyMasterRepository;

    @Autowired
    private
    CurrencyRateMasterValidator currencyRateMasterValidator;

    public BaseDto getByCurrencyRateMasterId(Long id) {

        log.info("getByCurrencyRateMasterId method is Invoked....[" + id + "]");

        BaseDto baseDto = new BaseDto();

        try {

            ValidateUtil.notNull(id, ErrorCode.CURRENCY_RATE_ID_NOT_NULL);

            CurrencyRateMaster currencyRateMaster = currencyRateMasterRepository.findOne(id);

            //CurrencyRateMaster currencyRateMaster = cacheRepository.get(SaaSUtil.getSaaSId(),CurrencyRateMaster.class.getName(), id, currencyRateMasterRepository);

            ValidateUtil.notNull(currencyRateMaster, ErrorCode.CURRENCY_RATE_ID_NOT_FOUND);

            log.info("Successfully getting CurrencyMaster by id...[" + currencyRateMaster.getId() + "]");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(currencyRateMaster);

        } catch (RestException exception) {

            log.error("Exception in getByCurrencyId method : ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (Exception exception) {

            log.error("Exception in getByCurrencyId method", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);
    }

    public BaseDto create(List<CurrencyRateMaster> currencyRateMasterList) {

        log.info("Create Method is Called...");

        BaseDto baseDto = new BaseDto();

        try {

            for (CurrencyRateMaster currencyRateMaster : currencyRateMasterList) {

                ValidateUtil.notNull(currencyRateMaster, ErrorCode.CURRENCY_RATE_NOT_NULL);

                currencyRateMasterValidator.validate(currencyRateMaster);

				/*
                 * CountryMaster countryMaster =
				 * cacheRepository.get(SaaSUtil.getSaaSId(),
				 * CountryMaster.class.getName(),
				 * currencyRateMaster.getCountryMaster().getId(),
				 * countryMasterRepository);
				 * 
				 * CurrencyMaster fromCurrency =
				 * cacheRepository.get(SaaSUtil.getSaaSId(),
				 * CurrencyMaster.class.getName(),
				 * currencyRateMaster.getFromCurrency().getId(),
				 * currencyMasterRepository);
				 */
				/*
				 * ValidateUtil.isStatusBlocked(fromCurrency.getStatus(),
				 * ErrorCode.CURRENCYRATE_FROMCURRENCY_BLOCK);
				 * ValidateUtil.isStatusHidden(fromCurrency.getStatus(),
				 * ErrorCode.CURRENCYRATE_FROMCURRENCY_HIDE);
				 */

                // Set FromCurrency to CurrencyRateMaster
                currencyRateMaster.setFromCurrencyCode(currencyRateMaster.getFromCurrency().getCurrencyCode());

                // Set Country to CurrencyRateMaster
                currencyRateMaster.setCountryCode(currencyRateMaster.getCountryMaster().getCountryCode());

				/*
				 * // Get ToCurrency From Cache CurrencyMaster toCurrency =
				 * cacheRepository.get(SaaSUtil.getSaaSId(),
				 * CurrencyMaster.class.getName(),
				 * currencyRateMaster.getToCurrency().getId(),
				 * currencyMasterRepository);
				 * 
				 * // Set ToCurrency to CurrencyRateMaster
				 * currencyRateMaster.setToCurrency(toCurrency);
				 */

                ValidateUtil.isStatusBlocked(currencyRateMaster.getToCurrency().getStatus(),
                        ErrorCode.CURRENCYRATE_TOCURRENCY_BLOCK);
                ValidateUtil.isStatusHidden(currencyRateMaster.getToCurrency().getStatus(),
                        ErrorCode.CURRENCYRATE_TOCURRENCY_HIDE);

                currencyRateMaster.setToCurrencyCode(currencyRateMaster.getToCurrency().getCurrencyCode());

            }

            // Save Currency Rate Master to the Database & Cache
            cacheRepository.save(SaaSUtil.getSaaSId(), CurrencyRateMaster.class.getName(), currencyRateMasterList, currencyRateMasterRepository);

            log.info("Currency Rate Master Saved Successfully...");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(currencyRateMasterList);
        } catch (RestException exception) {

            log.error("Exception in create method : ", exception);

            baseDto.setResponseCode(exception.getMessage());
        } catch (Exception e) {

            log.error("Exception occured", e);

            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(e);

            log.error("Exception Cause 1 ::: " + exceptionCause1);

            // General error condition
            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            if (exceptionCause1.contains(UniqueKey.UK_CURRENCY_FROMTODATE)) {
                baseDto.setResponseCode(ErrorCode.CURRENCY_RATE_CURRENCY_DATE_ALREADY_EXIST);
            }
        }

        return appUtil.setDesc(baseDto);
    }

    public BaseDto update(List<CurrencyRateMaster> currencyRateMasterList) {
        log.info("	Update Method is Called...");

        BaseDto baseDto = new BaseDto();

        try {
            // Server Side Validations
            CurrencyRateMaster existingCurrencyRateMaster = currencyRateMasterList.get(0);

            ValidateUtil.notNull(existingCurrencyRateMaster, ErrorCode.CURRENCY_RATE_NOT_NULL);

            currencyRateMasterValidator.validate(existingCurrencyRateMaster);

            currencyRateMasterRepository.getOne(existingCurrencyRateMaster.getId());

            ValidateUtil.isStatusBlocked(existingCurrencyRateMaster.getToCurrency().getStatus(),
                    ErrorCode.CURRENCYRATE_TOCURRENCY_BLOCK);
            ValidateUtil.isStatusHidden(existingCurrencyRateMaster.getToCurrency().getStatus(),
                    ErrorCode.CURRENCYRATE_TOCURRENCY_HIDE);

            // Save Currency Rate Master to the Database & Cache
            //	cacheRepository.save(SaaSUtil.getSaaSId(), CurrencyRateMaster.class.getName(), existingCurrencyRateMaster,currencyRateMasterRepository);

            currencyRateMasterRepository.save(existingCurrencyRateMaster);

            log.info("Currency Rate Master Saved Successfully...[" + existingCurrencyRateMaster.getId() + "]");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(existingCurrencyRateMaster);
        } catch (RestException exception) {

            log.error("Exception in create method : ", exception);

            baseDto.setResponseCode(exception.getMessage());
        } catch (ObjectOptimisticLockingFailureException e) {

            log.error("Error in editing TOS", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_LOCKED_RECORD);

        } catch (JpaObjectRetrievalFailureException e) {

            log.error("Error in editing TOS", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_DELETED_RECORD);

        } catch (Exception e) {

            log.error("Exception occured", e);

            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(e);

            log.error("Exception Cause 1 ::: " + exceptionCause1);

            // General error condition
            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            if (exceptionCause1.contains(UniqueKey.UK_CURRENCY_FROMTODATE)) {
                baseDto.setResponseCode(ErrorCode.CURRENCY_RATE_CURRENCY_DATE_ALREADY_EXIST);
            }
        }

        return appUtil.setDesc(baseDto);
    }

    public BaseDto delete(Long id) {
        log.info("Delete method is invoked : [ " + id + "]");

        BaseDto baseDto = new BaseDto();

        try {
            cacheRepository.delete(SaaSUtil.getSaaSId(), CurrencyRateMaster.class.getName(), id,
                    currencyRateMasterRepository);

            log.info("CurrencyRateMaster Deleted Successfully.... [ " + id + "]");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

        } catch (RestException exception) {

            log.error("Exception in Delete method : ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (Exception exception) {

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            log.error("Exception in Delete method : ", exception);

            String rootCause = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Root Cause of Exception : " + rootCause);

            if (rootCause.contains("FK_")) {
                baseDto.setResponseCode(ErrorCode.CANNOT_DELETE_FK_RECORD);
            }
        }

        return appUtil.setDesc(baseDto);
    }

    public BaseDto search(CurrencyRateSearchDto searchDto) {

        log.info("Search method is called...." + searchDto);

        BaseDto baseDto = new BaseDto();

        try {

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(currencyRateSearchImpl.search(searchDto));

            log.info("Successfully Searching ...");

        } catch (Exception exception) {

            log.error("Exception in Search method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);

    }

    public BaseDto search(Long fromCurrencyId) {

        log.info("Search method is called...." + fromCurrencyId);

        BaseDto baseDto = new BaseDto();

        try {

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(currencyRateSearchImpl.search(fromCurrencyId));

            log.info("Successfully Searching ...");

        } catch (Exception exception) {

            log.error("Exception in Search method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);

    }

}
