package com.efreightsuite.service;

import java.util.List;

import com.efreightsuite.common.component.CacheRepository;
import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.SearchRequest;
import com.efreightsuite.dto.ZoneMasterSearchDto;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.ExceptionHelper;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.exception.UniqueKey;
import com.efreightsuite.model.ZoneMaster;
import com.efreightsuite.repository.CountryMasterRepository;
import com.efreightsuite.repository.ZoneMasterRepository;
import com.efreightsuite.search.ZoneSearchImpl;
import com.efreightsuite.util.AppUtil;
import com.efreightsuite.util.SaaSUtil;
import com.efreightsuite.util.ValidateUtil;
import com.efreightsuite.validation.ZoneMasterValidator;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.exception.ExceptionUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;


@Service
@Log4j2
public class ZoneMasterService {


    @Autowired
    private
    AppUtil appUtil;


    @Autowired
    private
    ZoneMasterRepository zoneMasterRepository;


    @Autowired
    CountryMasterRepository countryMasterRepository;


    @Autowired
    private
    CacheRepository cacheRepository;


    @Autowired
    private
    ZoneMasterValidator zoneMasterValidator;


    @Autowired
    private
    ZoneSearchImpl zoneSearchImpl;

    /**
     * Returns all the Zone object
     *
     * @Returns BaseDto
     */
    public BaseDto search(SearchRequest searchRequest) {

        log.info("getAll method is called....");

        BaseDto baseDto = new BaseDto();

        try {

            baseDto.setResponseCode(ErrorCode.SUCCESS);
            baseDto.setResponseObject(zoneSearchImpl.search(searchRequest));

        } catch (RestException re) {
            log.error("Exception in getAll method ", re);
            baseDto.setResponseCode(re.getMessage());

        } catch (Exception e) {
            log.error("Exception in getAll method ", e);
            baseDto.setResponseCode(ErrorCode.FAILED);

        }
        return appUtil.setDesc(baseDto);
    }


    /**
     * Returns all the Zone object
     *
     * @Returns BaseDto
     */
    public BaseDto search(ZoneMasterSearchDto searchDto) {

        log.info("getAll method is called....");

        BaseDto baseDto = new BaseDto();

        try {

            log.info("Successfully getting list of zone...");
            baseDto.setResponseCode(ErrorCode.SUCCESS);
            baseDto.setResponseObject(zoneSearchImpl.search(searchDto));

        } catch (RestException re) {
            log.error("Exception in getAll method ", re);
            baseDto.setResponseCode(re.getMessage());

        } catch (Exception e) {
            log.error("Exception in getAll method ", e);
            baseDto.setResponseCode(ErrorCode.FAILED);

        }
        return appUtil.setDesc(baseDto);
    }


    /**
     * Method - getAll() Returns all the zone in a list format
     *
     * @return BaseDto
     */

    public BaseDto getAll() {

        log.info("getAll method is called..");

        BaseDto baseDto = new BaseDto();

        try {

            // Retrieving ZoneMaster List
            List<ZoneMaster> zoneMasterList = zoneMasterRepository.findAll();

            // Validating ZoneMaster List
            ValidateUtil.notEmpty(zoneMasterList, ErrorCode.ZONE_LIST_EMPTY);

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(zoneMasterList);

            log.info("getAll method is executed successfully..");

        } catch (RestException exception) {

            log.error("Exception in getAll method : ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (Exception exception) {

            log.error("Exception in getAll method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);
    }


    /**
     * Method - get Returns ZoneMaster Object Based on the Id
     *
     * @return baseDto
     */

    public BaseDto get(Long id) {

        log.info("get method is called [" + id + "]");

        BaseDto baseDto = new BaseDto();

        try {

            ZoneMaster zoneMaster = cacheRepository.get(SaaSUtil.getSaaSId(), ZoneMaster.class.getName(), id, zoneMasterRepository);
            log.info("status::**********" + zoneMaster.getStatus());

            // Validating ZoneMaster for not null
            ValidateUtil.notNull(zoneMaster, ErrorCode.ZONE_ID_NOT_FOUND);

            log.info("Successfully getting Zone by id.....[" + id + "]");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(zoneMaster);


        } catch (RestException exception) {

            log.error("Exception in get method ", exception);

            baseDto.setResponseCode(exception.getMessage());
        } catch (Exception e) {

            log.error("Exception in get method", e);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);
    }

    /**
     * Method - Create This method create a new Zone
     *
     * @param zoneMaster
     * @return BaseDto
     */

    public BaseDto create(ZoneMaster zoneMaster) {

        log.info("Create method is Invoked.....");

        BaseDto baseDto = new BaseDto();

        try {

            zoneMasterValidator.validate(zoneMaster);

            zoneMaster = cacheRepository.save(SaaSUtil.getSaaSId(), ZoneMaster.class.getName(), zoneMaster, zoneMasterRepository);

            log.info("Zone Successfully Saved....[" + zoneMaster.getId() + "]");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(zoneMaster);

        } catch (RestException exception) {

            log.error("Rest Exception occured with error code ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (Exception exception) {

            log.error("Exception occured ", exception);

            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Exception Cause 1 : " + exceptionCause1);

            // General error condition
            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            // Duplicate Zone Code
            if (exceptionCause1.contains(UniqueKey.UK_ZONE_ZONECODE)) {
                baseDto.setResponseCode(ErrorCode.ZONE_CODE_ALREADY_EXIST);
            }

            // Duplicate Zone Name
            if (exceptionCause1.contains(UniqueKey.UK_ZONE_ZONENAME)) {
                baseDto.setResponseCode(ErrorCode.ZONE_NAME_ALREADY_EXIST);
            }
        }

        return appUtil.setDesc(baseDto);
    }


    /**
     * Method - Update This method updates the existing Zone
     *
     * @return BaseDto
     */

    public BaseDto update(ZoneMaster existingZoneMaster) {

        log.info("update method is Invoked....[ " + existingZoneMaster.getId() + "]");

        BaseDto baseDto = new BaseDto();

        try {
            ValidateUtil.notNull(existingZoneMaster.getId(), ErrorCode.ZONE_ID_NOT_NULL);

            if (existingZoneMaster.getId() != null) {
                zoneMasterRepository.getOne(existingZoneMaster.getId());
            }

            zoneMasterValidator.validate(existingZoneMaster);

            zoneMasterRepository.save(existingZoneMaster);

            cacheRepository.remove(SaaSUtil.getSaaSId(), ZoneMaster.class.getName(), existingZoneMaster.getId());

            log.info("ZoneMaster Successfully Updated...[" + existingZoneMaster.getId() + "]");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(existingZoneMaster);

        } catch (RestException exception) {

            log.error("Rest Exception Occured with error code ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (Exception exception) {

            log.error("Exception occured ", exception);

            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Exception Cause 1 : " + exceptionCause1);

            // General error condition
            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            // Duplicate Zone Code
            if (exceptionCause1.contains(UniqueKey.UK_ZONE_ZONECODE)) {
                baseDto.setResponseCode(ErrorCode.ZONE_CODE_ALREADY_EXIST);
            }

            // Duplicate Zone Name
            if (exceptionCause1.contains(UniqueKey.UK_ZONE_ZONENAME)) {
                baseDto.setResponseCode(ErrorCode.ZONE_NAME_ALREADY_EXIST);
            }
        }

        return appUtil.setDesc(baseDto);
    }


    /**
     * Deletes the Existing Zone Based on the Given Id
     *
     * @Returns BaseDto
     */

    public BaseDto delete(Long id) {

        log.info("Delete method is Invoked...[" + id + "]");

        BaseDto baseDto = new BaseDto();

        try {

            cacheRepository.delete(SaaSUtil.getSaaSId(), ZoneMaster.class.getName(), id, zoneMasterRepository);

            log.info("ZoneMaster Deleted Successfully....[" + id + "]");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

        } catch (DataIntegrityViolationException exception) {

            log.error("Exception occured : ", exception);

            ExceptionHelper helper = new ExceptionHelper(exception);

            log.error("Foreign key reference : " + helper.getFkReference());

            baseDto.setResponseCode(ErrorCode.ERROR_FK_CONSTRAINT);

        } catch (Exception exception) {

            log.error("Exception in Delete method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);
    }
}
