package com.efreightsuite.service;

import java.util.ArrayList;
import java.util.List;

import com.efreightsuite.activiti.ActivitiTransactionService;
import com.efreightsuite.dto.AesFileSearchDto;
import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.ShipmentSearchDto;
import com.efreightsuite.enumeration.SequenceType;
import com.efreightsuite.enumeration.ServiceStatus;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.model.AesCommodity;
import com.efreightsuite.model.AesFile;
import com.efreightsuite.model.AesFileStatusHistory;
import com.efreightsuite.model.DtdcVehicle;
import com.efreightsuite.model.ShipmentLink;
import com.efreightsuite.model.ShipmentServiceDetail;
import com.efreightsuite.repository.AesFileRepository;
import com.efreightsuite.repository.AesFileStatusHistoryRepository;
import com.efreightsuite.repository.ConsolRepository;
import com.efreightsuite.repository.ShipmentLinkRepository;
import com.efreightsuite.repository.ShipmentServiceDetailRepository;
import com.efreightsuite.search.AesFIleSearchImpl;
import com.efreightsuite.util.AppUtil;
import com.efreightsuite.util.ValidateUtil;
import com.efreightsuite.validators.AesFileValidator;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author user
 *
 */
@Service
@Log4j2
public class AesFileService {


    @Autowired
    private
    AesFileRepository aesFileRepository;
    @Autowired
    private
    SequenceGeneratorService sequenceGeneratorService;
    @Autowired
    private
    ActivitiTransactionService activitiTransactionService;

    @Autowired
    private
    AppUtil appUtil;

    @Autowired
    private
    AesFIleSearchImpl aesFIleSearchImpl;

    @Autowired
    private
    AesFileValidator aesFileValidator;

    @Autowired
    ConsolRepository consolRepository;


    @Autowired
    private
    ShipmentServiceDetailRepository shipmentServiceDetailRepository;

    @Autowired
    private
    ShipmentLinkRepository shipmentLinkRepository;

    @Autowired
    private
    AesFileStatusHistoryRepository aesFileStatusHistoryRepository;

    @Autowired
    AesFileEdiService aesFileEdiService;


    /**
     * Method to get aes files with the id of shipmentUid
     * @param shipmentUid
     * @return
     */
    public BaseDto getByShipmentUid(String shipmentUid) {

        log.info("AesFile Service getByShipmentUid method called");

        BaseDto baseDto = new BaseDto();
        try {

            ValidateUtil.notNullOrEmpty(shipmentUid, ErrorCode.SHIPMENT_UID_CANNOT_NULL);

            AesFile aesFile = aesFileRepository.findByShipmentUid(shipmentUid);

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(aesFile);
        } catch (RestException exception) {

            log.error("RestException in getByShipmentUid method : ", exception);

            baseDto.setResponseCode(exception.getMessage());
        } catch (Exception exception) {

            log.error("Exception in getByShipmentUid method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);

    }

    /**
     *  Method to get aes files with the id of serviceuid
     * @param serviceuid
     * @return
     */
    public BaseDto getByServiceUid(String serviceuid) {
        log.info("AesFile Service getByServiceUid method called");

        BaseDto baseDto = new BaseDto();
        try {

            ValidateUtil.notNullOrEmpty(serviceuid, ErrorCode.SERVICE_UID_CANNOT_NULL);

            AesFile aesFile = aesFileRepository.findByServiceUid(serviceuid);

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(aesFile);
        } catch (RestException exception) {

            log.error("RestException in getByServiceUid method : ", exception);

            baseDto.setResponseCode(exception.getMessage());
        } catch (Exception exception) {

            log.error("Exception in getByServiceUid method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);
    }

    /**
     *  Method to get aes files with the id of masteruid
     * @param masteruid
     * @return
     */
    public BaseDto getByMasterUid(String masteruid) {

        log.info("AesFile Service getByMasterUid method called");

        BaseDto baseDto = new BaseDto();
        try {

            ValidateUtil.notNullOrEmpty(masteruid, ErrorCode.MASTER_UID_CANNOT_NULL);

            AesFile aesFile = aesFileRepository.findByMasterUid(masteruid);

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(aesFile);
        } catch (RestException exception) {

            log.error("RestException in getByMasterUid method : ", exception);

            baseDto.setResponseCode(exception.getMessage());
        } catch (Exception exception) {

            log.error("Exception in getByMasterUid method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);

    }


    public BaseDto pendingSearch(ShipmentSearchDto serviceDto) {


        log.info("Aes service -> search method is called....");

        BaseDto baseDto = new BaseDto();

        try {

            ValidateUtil.notNull(serviceDto, ErrorCode.AES_CANNOT_NULL);

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(aesFIleSearchImpl.searchPending(serviceDto));

        } catch (RestException re) {

            log.error("Exception in Aes service -> search method ", re);

            baseDto.setResponseCode(re.getMessage());

        } catch (Exception e) {

            log.error("Exception in Aes service -> search method ", e);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);

    }


    public BaseDto search(AesFileSearchDto aesDto) {


        log.info("Aes service -> search method is called....");

        BaseDto baseDto = new BaseDto();

        try {

            ValidateUtil.notNull(aesDto, ErrorCode.AES_CANNOT_NULL);

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(aesFIleSearchImpl.search(aesDto));

        } catch (RestException re) {

            log.error("Exception in Aes service -> search method ", re);

            baseDto.setResponseCode(re.getMessage());

        } catch (Exception e) {

            log.error("Exception in Aes service -> search method ", e);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);

    }


    public BaseDto getAesFileListByMasterUid(String masteruid) {


        log.info("Aes service -> search method is called...." + masteruid);

        BaseDto baseDto = new BaseDto();

        try {
            boolean flag = false;
            List<AesFile> allAes = new ArrayList<>();
            //ValidateUtil.notNull(aesDto, ErrorCode.AES_CANNOT_NULL);

            List<ShipmentLink> shipmentLinkList = shipmentLinkRepository.findAllByConsolUid(masteruid);

            List<AesFile> aesFileList = aesFileRepository.findAllByMasterUid(masteruid);

            log.info("shipment link list" + shipmentLinkList.size());
            log.info("aes  list" + aesFileList.size());

            if (aesFileList != null && aesFileList.size() != 0) {

                allAes.addAll(aesFileList);

                for (ShipmentLink aShipmentLinkList : shipmentLinkList) {

                    for (AesFile anAesFileList : aesFileList) {

                        if (!aShipmentLinkList.getServiceUid().equals(anAesFileList.getServiceUid())) {
                            flag = true;
                        } else {
                            flag = false;
                            break;
                        }
                    }

                    if (flag) {

                        AesFile aesFile = new AesFile();
                        if (aShipmentLinkList.getConsolUid() != null) {
                            aesFile.setServiceUid(aShipmentLinkList.getServiceUid());
                            allAes.add(aesFile);
                        }

                    }
                }

            } else {

                for (ShipmentLink aShipmentLinkList : shipmentLinkList) {
                    AesFile aesFile = new AesFile();
                    aesFile.setServiceUid(aShipmentLinkList.getServiceUid());
                    allAes.add(aesFile);

                }

            }
            baseDto.setResponseCode(ErrorCode.SUCCESS);
            baseDto.setResponseObject(allAes);


        } catch (RestException re) {

            log.error("Exception in Aes service -> search method ", re);

            baseDto.setResponseCode(re.getMessage());

        } catch (Exception e) {

            log.error("Exception in Aes service -> search method ", e);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);

    }

    private AesFile aesCreate(AesFile aesFile) {

        ValidateUtil.notNull(aesFile, ErrorCode.AES_CANNOT_NULL);

        ShipmentServiceDetail sd = null;

        if (aesFile.getServiceUid() != null) {
            sd = shipmentServiceDetailRepository.findService(aesFile.getServiceUid());
            if (sd != null && sd.getLastUpdatedStatus().getServiceStatus().equals(ServiceStatus.Cancelled)) {
                throw new RestException(ErrorCode.AES_CANNOT_CREATE_FOR_CANCEL_SERVICE);
            }
        }
        aesFileValidator.validateInfo(aesFile);
        if (aesFile.getAesUsppi() != null) {
            aesFileValidator.verifyUppsi(aesFile);
        }
        if (aesFile.getAesUltimateConsignee() != null) {
            aesFileValidator.verifyultimateConsignee(aesFile.getAesUltimateConsignee());

        }
        if (aesFile.getAesIntermediateConsignee() != null) {
            aesFileValidator.verifyintermediateConsignee(aesFile);

        }
        if (aesFile.getAesFreightForwarder() != null) {
            aesFileValidator.verifyFreightForwarder(aesFile);
        }
        if (aesFile.getAesCommodityList() != null && aesFile.getAesCommodityList().size() > 0) {

            for (AesCommodity aesCommodity : aesFile.getAesCommodityList()) {
                aesFileValidator.verifyCommodity(aesCommodity);
                if (aesCommodity.getDtdcVehicleList() != null && aesCommodity.getDtdcVehicleList().size() > 0) {
                    for (DtdcVehicle dtdcVehicle : aesCommodity.getDtdcVehicleList()) {
                        aesFileValidator.verifyDtdcVehicle(dtdcVehicle);
                    }
                }
            }
        } else {
            aesFile.setAesCommodityList(null);
        }
        aesFile.setStatus("Update");
        aesFile.setBatchNo(sequenceGeneratorService.getSequence(SequenceType.AES_BATCH));
        AesFile savedAesFile = aesFileRepository.save(aesFile);
        sd.setAesFile(savedAesFile);
        shipmentServiceDetailRepository.save(sd);

        activitiTransactionService.aesOrCustoms(sd.getShipment().getId(), sd);

        return savedAesFile;
    }

    /**
     * Create a new Aes File
     * @param aesFile
     * @return
     */
    public BaseDto create(AesFile aesFile) {


        BaseDto baseDto = new BaseDto();
        log.info("aesfile create method is called");

        try {

            baseDto.setResponseCode(ErrorCode.SUCCESS);
            baseDto.setResponseDescription(ErrorCode.SUCCESS);
            baseDto.setResponseObject(aesCreate(aesFile));

        } catch (RestException re) {
            log.error("Exception in aes service -> create method ", re);
            baseDto.setResponseCode(re.getMessage());

        } catch (Exception e) {
            log.info("exception in aes creation", e);
            baseDto.setResponseCode(e.getMessage());
        }

        return appUtil.setDesc(baseDto);

    }

    /**
     * Updating an aesfile record
     * @param aesFile
     * @return
     */
    public BaseDto update(AesFile aesFile) {

        BaseDto baseDto = new BaseDto();
        log.info("aesfile create method is called");

        ShipmentServiceDetail sd = null;
        try {

            ValidateUtil.notNull(aesFile, ErrorCode.AES_CANNOT_NULL);

            if (aesFile.getServiceUid() != null) {
                sd = shipmentServiceDetailRepository.findService(aesFile.getServiceUid());
                if (sd != null && sd.getLastUpdatedStatus().getServiceStatus().equals(ServiceStatus.Cancelled)) {
                    throw new RestException(ErrorCode.AES_CANNOT_CREATE_FOR_CANCEL_SERVICE);
                }
            }

            aesFileValidator.validateInfo(aesFile);

            if (aesFile.getAesUsppi() != null) {
                aesFileValidator.verifyUppsi(aesFile);
            } else {
                if (aesFile.getAesUsppi() != null && aesFile.getAesUsppi().getId() != null) {
                    aesFileValidator.verifyUppsi(aesFile);
                }
            }

            if (aesFile.getAesUltimateConsignee() != null) {
                aesFileValidator.verifyultimateConsignee(aesFile.getAesUltimateConsignee());

            } else {
                if (aesFile.getAesUltimateConsignee() != null && aesFile.getAesUltimateConsignee().getId() != null) {
                    aesFileValidator.verifyultimateConsignee(aesFile.getAesUltimateConsignee());
                }

            }


            if (aesFile.getAesIntermediateConsignee() != null) {
                aesFileValidator.verifyintermediateConsignee(aesFile);

            } else {
                if (aesFile.getAesIntermediateConsignee() != null && aesFile.getAesIntermediateConsignee().getId() != null) {
                    aesFileValidator.verifyintermediateConsignee(aesFile);
                }
            }

            if (aesFile.getAesFreightForwarder() != null) {
                aesFileValidator.verifyFreightForwarder(aesFile);

            } else {

                if (aesFile.getAesFreightForwarder() != null && aesFile.getAesFreightForwarder().getId() != null) {
                    aesFileValidator.verifyFreightForwarder(aesFile);
                }
            }

            if (aesFile.getAesCommodityList() != null && aesFile.getAesCommodityList().size() > 0) {

                for (AesCommodity aesCommodity : aesFile.getAesCommodityList()) {

                    aesFileValidator.verifyCommodity(aesCommodity);

                    if (aesCommodity.getDtdcVehicleList() != null && aesCommodity.getDtdcVehicleList().size() > 0) {
                        for (DtdcVehicle dtdcVehicle : aesCommodity.getDtdcVehicleList()) {
                            aesFileValidator.verifyDtdcVehicle(dtdcVehicle);
                        }
                    } else {
                        aesCommodity.setDtdcVehicleList(new ArrayList<>());
                    }

                }
            } else {
                aesFile.setAesCommodityList(new ArrayList<>());
            }


            AesFile updatedAesFile = aesFileRepository.save(aesFile);
            sd.setAesFile(updatedAesFile);
            shipmentServiceDetailRepository.save(sd);
            baseDto.setResponseCode(ErrorCode.SUCCESS);
            baseDto.setResponseDescription(ErrorCode.SUCCESS);
            baseDto.setResponseObject(updatedAesFile);
        } catch (RestException re) {
            log.error("Exception in aes service -> update method ", re);
            baseDto.setResponseCode(re.getMessage());
        } catch (Exception e) {
            log.info("exception in aes creation", e);
            baseDto.setResponseCode(e.getMessage());
        }
        return appUtil.setDesc(baseDto);
    }

    /**
     * getById
     * @param id
     * @return
     */
    public BaseDto getById(Long id) {
        log.info("ShipmentService -> getById method is called....");

        BaseDto baseDto = new BaseDto();

        try {

            AesFile aesFile = aesFileRepository.findById(id);

            baseDto.setResponseCode(ErrorCode.SUCCESS);
            baseDto.setResponseObject(aesFile);

        } catch (RestException re) {
            log.error("Exception in AESService -> getById method ", re);
            baseDto.setResponseCode(re.getMessage());

        } catch (Exception e) {
            log.error("Exception in AESService -> getById method ", e);
            baseDto.setResponseCode(ErrorCode.FAILED);

        }
        return appUtil.setDesc(baseDto);
    }

    /**
     * getById
     * @param id
     * @return
     */
    public BaseDto getStatusHistory(Long id) {
        log.info("ShipmentService -> getStatusHistory method is called....");

        BaseDto baseDto = new BaseDto();

        try {

            List<AesFileStatusHistory> historyList = aesFileStatusHistoryRepository.findByAesFile(id);

            baseDto.setResponseCode(ErrorCode.SUCCESS);
            baseDto.setResponseObject(historyList);

        } catch (Exception e) {
            log.error("Exception in AESService -> getStatusHistory method ", e);
            baseDto.setResponseCode(ErrorCode.FAILED);

        }
        return appUtil.setDesc(baseDto);
    }

    public BaseDto createAll(ArrayList<AesFile> aesFiles) {
        BaseDto baseDto = new BaseDto();
        try {
            /*for(AesFile aesFile:aesFiles) {
		    aesFileRepository.save(aesFile);	
		      }*/
            baseDto.setResponseCode(ErrorCode.SUCCESS);
        } catch (RestException re) {
            log.error("Exception in AESService -> createAll method ", re);
            baseDto.setResponseCode(re.getMessage());

        } catch (Exception e) {
            log.error("Exception in AESService -> createAll method ", e);
            baseDto.setResponseCode(ErrorCode.FAILED);
        }
        return appUtil.setDesc(baseDto);
    }


}
