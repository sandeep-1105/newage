package com.efreightsuite.service;

import java.util.List;

import com.efreightsuite.common.component.CacheRepository;
import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.ExceptionHelper;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.model.PartyEmailMessageMapping;
import com.efreightsuite.repository.PartyEmailMappingRepository;
import com.efreightsuite.repository.PartyEmailMessageMappingRepository;
import com.efreightsuite.util.AppUtil;
import com.efreightsuite.util.SaaSUtil;
import com.efreightsuite.util.ValidateUtil;
import com.efreightsuite.validation.PartyEmailMessageMappingValidator;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.exception.ExceptionUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.orm.ObjectOptimisticLockingFailureException;
import org.springframework.orm.jpa.JpaObjectRetrievalFailureException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author Devendrachary M
 */
@Service
@Log4j2
public class PartyEmailMessageMappingService {

    @Autowired
    private
    PartyEmailMessageMappingRepository partyEmailMessageMappingRepository;

    @Autowired
    PartyEmailMappingRepository partyEmailMappingRepository;

    @Autowired
    private
    AppUtil appUtil;

    @Autowired
    private
    CacheRepository cacheRepository;

    @Autowired
    private
    PartyEmailMessageMappingValidator partyEmailMessageMappingValidator;

    @Autowired
    SequenceGeneratorService sequenceGeneratorService;

    /**
     * Method - get
     *
     * @return baseDto
     */

    public BaseDto get(Long id) {
        log.info("PartyEmailMessageMappingService.get method started.");

        BaseDto baseDto = new BaseDto();
        try {

            ValidateUtil.notNull(id, ErrorCode.AUTOMAILGROUP_ID_INVALID);

            PartyEmailMessageMapping partyEmailMessageMapping = partyEmailMessageMappingRepository.findById(id);

            log.info("Successfully getting PartyEmailMessageMapping by id...");

            baseDto.setResponseCode(ErrorCode.SUCCESS);
            baseDto.setResponseObject(partyEmailMessageMapping);

        } catch (RestException re) {
            log.error(
                    "RestException in PartyEmailMessageMappingService.get method while getting the PartyEmailMessageMapping : "
                            + re);
            baseDto.setResponseCode(ErrorCode.AUTOMAILGROUP_ID_INVALID);
        } catch (Exception e) {
            log.error(
                    "Exception in PartyEmailMessageMappingService.get method while getting the PartyEmailMessageMapping : ",
                    e);
            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        log.info("PartyEmailMessageMappingService.get method ended.");
        return appUtil.setDesc(baseDto);
    }

    /*
     * create - This method creates a new PartyEmailMessageMapping
     *
     * @Param PartyEmailMessageMapping master entity
     *
     * @Return BaseDto
     */
    @Transactional
    public BaseDto create(PartyEmailMessageMapping partyEmailMessageMapping) {
        log.info("PartyEmailMessageMappingService.create method is started.");

        log.info("partyEmailMessageMapping : " + partyEmailMessageMapping);

        BaseDto baseDto = new BaseDto();
        try {

            partyEmailMessageMappingValidator.validate(partyEmailMessageMapping);

            cacheRepository.save(SaaSUtil.getSaaSId(), PartyEmailMessageMapping.class.getName(),
                    partyEmailMessageMapping, partyEmailMessageMappingRepository);
            log.info("PartyEmailMessageMapping Saved successfully.....");
            baseDto.setResponseCode(ErrorCode.SUCCESS);
            baseDto.setResponseObject(null);

        } catch (RestException re) {

            log.error("Exception in PartyEmailMessageMappingService.create ", re);

            baseDto.setResponseCode(re.getMessage());
        } catch (Exception exception) {

            log.error("Failed to save the Party Email Message Mapping..", exception);

            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Exception Cause 1 ::: " + exceptionCause1);

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

        }

        log.info("PartyEmailMessageMappingService.create method is ended.");
        return appUtil.setDesc(baseDto);
    }

    public BaseDto update(PartyEmailMessageMapping existingPartyEmailMessageMapping) {
        log.info("PartyEmailMessageMappingService.update method is started.");
        BaseDto baseDto = new BaseDto();

        try {

            partyEmailMessageMappingValidator.validate(existingPartyEmailMessageMapping);

            partyEmailMessageMappingRepository.getOne(existingPartyEmailMessageMapping.getId());

            PartyEmailMessageMapping partyEmailMessageMappingUpdateResponse = cacheRepository.save(SaaSUtil.getSaaSId(),
                    PartyEmailMessageMapping.class.getName(), existingPartyEmailMessageMapping,
                    partyEmailMessageMappingRepository);

            log.info("Service  updated successfully...[" + partyEmailMessageMappingUpdateResponse.getId() + "]");

            log.info("PartyService updated successfully...");

            baseDto.setResponseCode(ErrorCode.SUCCESS);
            baseDto.setResponseObject(partyEmailMessageMappingUpdateResponse);

        } catch (RestException exception) {

            log.error("RestException in Delete method : ", exception);

            baseDto.setResponseCode(exception.getMessage());
        } catch (ObjectOptimisticLockingFailureException e) {
            log.error("Error in editing Service", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_LOCKED_RECORD);
        } catch (JpaObjectRetrievalFailureException e) {
            log.error("Error in editing Service", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_DELETED_RECORD);
        } catch (Exception exception) {

            log.error("Failed to update the Party Email Message Mapping..", exception);

            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Exception Cause 1 ::: " + exceptionCause1);

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

        }

        log.info("PartyEmailMessageMappingService.update method is ended.");
        return appUtil.setDesc(baseDto);

    }

    /*
     * Deletes the Existing Party Based on the Given Id
     *
     * @param Id
     *
     * @Returns BaseDto
     */
    public BaseDto delete(Long id) {
        log.info("PartyEmailMessageMappingService.delete method is started.[" + id + "]");

        BaseDto baseDto = new BaseDto();

        try {

            ValidateUtil.notNull(id, ErrorCode.AUTOMAILGROUP_ID_NOT_NULL);

            cacheRepository.delete(SaaSUtil.getSaaSId(), PartyEmailMessageMapping.class.getName(), id,
                    partyEmailMessageMappingRepository);

            log.info("PartyEmailMessageMapping Deleted Successfully....[" + id + "]");

            baseDto.setResponseCode(ErrorCode.SUCCESS);
            return appUtil.setDesc(baseDto);

        } catch (RestException exception) {

            log.error("Exception in Delete method : ", exception);

            baseDto.setResponseCode(exception.getMessage());
        } catch (DataIntegrityViolationException exception) {

            log.error("Exception occured : ", exception);

            ExceptionHelper helper = new ExceptionHelper(exception);

            log.error("Foreign key reference : " + helper.getFkReference());

            baseDto.setResponseCode(ErrorCode.ERROR_FK_CONSTRAINT);
        } catch (Exception exception) {

            log.error("Exception in Delete method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }
        log.info("PartyEmailMessageMappingService.delete method is ended");
        return appUtil.setDesc(baseDto);

    }

    /*
     * Gives the all PartyEmailMessageMapping
     *
     *
     * @Returns List<PartyEmailMessageMapping>
     */
    public BaseDto getAll() {

        log.info("PartyEmailMessageMappingService.getAll method is started");

        BaseDto baseDto = new BaseDto();

        try {

            List<PartyEmailMessageMapping> partyEmailMessageMappingList = partyEmailMessageMappingRepository.findAll();

            log.info("Successfully getting list of partyEmailMessageMapping...");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(partyEmailMessageMappingList);

        } catch (RestException exception) {

            log.error("Exception in getAll method ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (Exception exception) {

            log.error("Exception in getAll method ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }
        log.info("PartyEmailMessageMappingService.getAll method is ended");
        return appUtil.setDesc(baseDto);
    }

}
