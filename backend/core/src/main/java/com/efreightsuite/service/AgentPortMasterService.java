package com.efreightsuite.service;

import java.util.List;

import com.efreightsuite.common.component.CacheRepository;
import com.efreightsuite.dto.AgentPortMasterSearchDto;
import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.ExceptionHelper;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.exception.UniqueKey;
import com.efreightsuite.model.AgentPortMaster;
import com.efreightsuite.model.PartyMaster;
import com.efreightsuite.repository.AgentPortMasterRepository;
import com.efreightsuite.search.AgentPortMasterSearchImpl;
import com.efreightsuite.util.AppUtil;
import com.efreightsuite.util.SaaSUtil;
import com.efreightsuite.validation.AgentPortValidator;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.exception.ExceptionUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;


@Service
@Log4j2
public class AgentPortMasterService {

    @Autowired
    private
    AppUtil appUtil;

    @Autowired
    private
    AgentPortMasterRepository agentPortMasterRepository;

    @Autowired
    private
    AgentPortMasterSearchImpl agentPortMasterSearchImpl;

    @Autowired
    private
    CacheRepository cacheRepository;

    @Autowired
    private
    AgentPortValidator agentPortValidator;

    @Autowired
    MetaConfigurationService metaConfigurationService;

    public BaseDto searchByService(Long serviceId, Long portId) {

        BaseDto baseDto = new BaseDto();

        try {
            AgentPortMaster pe = agentPortMasterRepository.serachByServicePort(serviceId, portId);
            if (pe != null) {
                pe.getAgent();
            }
            baseDto.setResponseCode(ErrorCode.SUCCESS);
            baseDto.setResponseObject(pe);
        } catch (Exception exception) {

            baseDto.setResponseCode(ErrorCode.FAILED);
        }
        return appUtil.setDesc(baseDto);
    }

    public BaseDto search(AgentPortMasterSearchDto searchDto) {

        log.info("getAll method is called....");

        BaseDto baseDto = new BaseDto();

        try {

            log.info("Successfully getting list of Agent port...");
            baseDto.setResponseCode(ErrorCode.SUCCESS);
            baseDto.setResponseObject(agentPortMasterSearchImpl.search(searchDto));

        } catch (RestException re) {
            log.error("Exception in getAll method ", re);
            baseDto.setResponseCode(re.getMessage());

        } catch (Exception e) {
            log.error("Exception in getAll method ", e);
            baseDto.setResponseCode(ErrorCode.FAILED);

        }
        return appUtil.setDesc(baseDto);
    }


    public BaseDto get(Long id) {

        log.info("get method is called [" + id + "]");

        BaseDto baseDto = new BaseDto();

        try {

            AgentPortMaster agentPortMaster = cacheRepository.get(SaaSUtil.getSaaSId(), AgentPortMaster.class.getName(), id, agentPortMasterRepository);


            log.info("Successfully getting Agent Port by id.....[" + id + "]");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(agentPortMaster);


        } catch (RestException exception) {

            log.error("Exception in get method ", exception);

            baseDto.setResponseCode(exception.getMessage());
        } catch (Exception e) {

            log.error("Exception in get method", e);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);
    }

    public BaseDto getAgentByPortCode(String portCode) {

        log.info("getAgentByPortCode method is called [" + portCode + "]");

        BaseDto baseDto = new BaseDto();

        try {
            AgentPortMaster agentPortMaster = null;
            List<AgentPortMaster> agentPortMasterList = agentPortMasterRepository.searchByPortCode(portCode);
            if(agentPortMasterList!=null && agentPortMasterList.size()>0){
                agentPortMaster = agentPortMasterList.get(0);
            }

            log.info("Successfully getting AgentPortMaster by portCode.....[" + portCode + "]");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            if(agentPortMaster!=null)
                baseDto.setResponseObject(agentPortMaster.getAgent());
            else 
                baseDto.setResponseObject("Agent not Available corresponding to the given port code");


        } catch (RestException exception) {

            log.error("Exception in getAgentByPortCode method ", exception);

            baseDto.setResponseCode(exception.getMessage());
        } catch (Exception e) {

            log.error("Exception in getAgentByPortCode method", e);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);
    }


    public BaseDto create(List<AgentPortMaster> agentPortMaster) {

        log.info("create method is Invoked.....");

        BaseDto baseDto = new BaseDto();

        try {

            for (AgentPortMaster apm : agentPortMaster) {
                agentPortValidator.validate(apm);
            }

            agentPortMasterRepository.save(agentPortMaster);

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(ErrorCode.SUCCESS);

        } catch (RestException exception) {

            log.error("Rest Exception occured with error code ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (Exception exception) {

            log.error("Exception occured ", exception);

            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Exception Cause 1 : " + exceptionCause1);

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            if (exceptionCause1.contains(UniqueKey.UK_SVC_PORT_AGENTID)) {
                baseDto.setResponseCode(ErrorCode.PORT_AGENT_ALREADY_EXIST);
            }
        }

        return appUtil.setDesc(baseDto);
    }

    public BaseDto update(AgentPortMaster existingAgentPort) {

        log.info("update method is Invoked....[ " + existingAgentPort.getId() + "]");

        BaseDto baseDto = new BaseDto();

        try {

            if (existingAgentPort.getId() != null) {
                agentPortMasterRepository.getOne(existingAgentPort.getId());
            }


            agentPortValidator.validate(existingAgentPort);

            agentPortMasterRepository.save(existingAgentPort);

            cacheRepository.remove(SaaSUtil.getSaaSId(), AgentPortMaster.class.getName(), existingAgentPort.getId());

            log.info("Agent Port Master Successfully Updated...[" + existingAgentPort.getId() + "]");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(ErrorCode.SUCCESS);

        } catch (RestException exception) {

            log.error("Rest Exception occured with error code ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (Exception exception) {

            log.error("Exception occured ", exception);

            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Exception Cause 1 : " + exceptionCause1);

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            if (exceptionCause1.contains(UniqueKey.UK_SVC_PORT_AGENTID)) {
                baseDto.setResponseCode(ErrorCode.PORT_AGENT_ALREADY_EXIST);
            }
        }

        return appUtil.setDesc(baseDto);
    }


    public BaseDto delete(Long id) {

        log.info("Delete method is Invoked...[" + id + "]");

        BaseDto baseDto = new BaseDto();

        try {

            cacheRepository.delete(SaaSUtil.getSaaSId(), AgentPortMaster.class.getName(), id, agentPortMasterRepository);

            log.info("ZoneMaster Deleted Successfully....[" + id + "]");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

        } catch (DataIntegrityViolationException exception) {

            log.error("Exception occured : ", exception);

            ExceptionHelper helper = new ExceptionHelper(exception);

            log.error("Foreign key reference : " + helper.getFkReference());

            baseDto.setResponseCode(ErrorCode.ERROR_FK_CONSTRAINT);

        } catch (Exception exception) {

            log.error("Exception in Delete method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);
    }

}
