package com.efreightsuite.service;

import java.util.List;

import com.efreightsuite.common.component.CacheRepository;
import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.SearchRequest;
import com.efreightsuite.dto.ServiceSearchDto;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.ExceptionHelper;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.exception.UniqueKey;
import com.efreightsuite.model.ServiceMaster;
import com.efreightsuite.repository.ServiceMappingRepository;
import com.efreightsuite.repository.ServiceMasterRepository;
import com.efreightsuite.search.CommonSearchImpl;
import com.efreightsuite.search.ServiceSearchImpl;
import com.efreightsuite.util.AppUtil;
import com.efreightsuite.util.SaaSUtil;
import com.efreightsuite.util.ValidateUtil;
import com.efreightsuite.validation.ServiceMasterValidator;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.exception.ExceptionUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.orm.ObjectOptimisticLockingFailureException;
import org.springframework.orm.jpa.JpaObjectRetrievalFailureException;
import org.springframework.stereotype.Service;


@Service
@Log4j2
public class ServiceMasterService {

    @Autowired
    private
    AppUtil appUtil;

    @Autowired
    private
    ServiceMasterRepository serviceMasterRepository;

    @Autowired
    private
    CacheRepository cacheRepository;

    @Autowired
    private
    ServiceMasterValidator serviceMasterValidator;

    @Autowired
    private
    ServiceSearchImpl serviceSearchImpl;

    @Autowired
    ServiceMappingRepository serviceMappingRepository;

    @Autowired
    private
    CommonSearchImpl commonSearchImpl;

    public BaseDto search(ServiceSearchDto serviceSearchDto) {

        log.info("Search method is called....");

        BaseDto baseDto = new BaseDto();

        try {

            baseDto.setResponseObject(serviceSearchImpl.search(serviceSearchDto));

            baseDto.setResponseCode(ErrorCode.SUCCESS);

        } catch (Exception exception) {

            log.error("Exception in Search method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);
    }

    public BaseDto search(SearchRequest searchRequest) {

        log.info("Search method is called....");

        BaseDto baseDto = new BaseDto();

        try {

            baseDto.setResponseObject(serviceSearchImpl.search(searchRequest));

            baseDto.setResponseCode(ErrorCode.SUCCESS);

        } catch (Exception exception) {

            log.error("Exception in Search method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);
    }

    public BaseDto searchServiceNotInList(SearchRequest searchRequest) {

        log.info("searchServiceNotInList method is called....");

        BaseDto baseDto = new BaseDto();

        try {

            baseDto.setResponseObject(commonSearchImpl.searchServiceNotInList(searchRequest));

            baseDto.setResponseCode(ErrorCode.SUCCESS);

        } catch (Exception exception) {

            log.error("Exception in searchServiceNotInList method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);
    }


    public BaseDto search(SearchRequest searchRequest, String transportMode) {

        log.info("Search method is called....Transport Mode " + transportMode);

        BaseDto baseDto = new BaseDto();

        try {

            baseDto.setResponseObject(serviceSearchImpl.search(searchRequest, transportMode));

            baseDto.setResponseCode(ErrorCode.SUCCESS);

        } catch (Exception exception) {

            log.error("Exception in Search method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);
    }


    public BaseDto get(Long id) {

        log.info("Get method is called....[" + id + "]");

        BaseDto baseDto = new BaseDto();

        try {

            ServiceMaster serviceMaster = cacheRepository.get(SaaSUtil.getSaaSId(), ServiceMaster.class.getName(), id, serviceMasterRepository);

            ValidateUtil.notNull(serviceMaster, ErrorCode.SERVICE_ID_NOT_FOUND);

            log.info("Successfully getting Service by id....[" + id + "]");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(serviceMaster);

        } catch (RestException exception) {

            log.error("RestException in get method : ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (Exception exception) {

            log.error("Exception in get method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);

        }

        return appUtil.setDesc(baseDto);
    }

    public BaseDto create(ServiceMaster serviceMaster) {

        log.info("Save method is called.....");

        BaseDto baseDto = new BaseDto();

        try {

            serviceMasterValidator.validate(serviceMaster);

            ServiceMaster serviceMasterCreated = cacheRepository.save(SaaSUtil.getSaaSId(), ServiceMaster.class.getName(), serviceMaster, serviceMasterRepository);

            log.info("Service Saved successfully.....  " + serviceMaster.getId() + "]");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(serviceMasterCreated);

        } catch (RestException exception) {

            log.error("RestException in Create method : ", exception);


            baseDto.setResponseCode(exception.getMessage());

        } catch (Exception e) {

            String exceptionCause = ExceptionUtils.getRootCauseMessage(e);

            log.error("Exception Cause  ::: " + exceptionCause);

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            if (exceptionCause.contains(UniqueKey.UK_SERVICE_SERVICECODE)) {
                baseDto.setResponseCode(ErrorCode.SERVICE_CODE_ALREADY_EXIST);
            }
            if (exceptionCause.contains(UniqueKey.UK_SERVICE_SERVICENAME)) {
                baseDto.setResponseCode(ErrorCode.SERVICE_NAME_ALREADY_EXIST);
            }
        }
        return appUtil.setDesc(baseDto);
    }

    public BaseDto update(ServiceMaster existingServiceMaster) {

        log.info("update method is called....");

        BaseDto baseDto = new BaseDto();

        try {

            serviceMasterValidator.validate(existingServiceMaster);

            ValidateUtil.notNull(existingServiceMaster.getId(), ErrorCode.SERVICE_ID_NOT_NULL);

            serviceMasterRepository.getOne(existingServiceMaster.getId());

            serviceMasterRepository.save(existingServiceMaster);

            cacheRepository.remove(SaaSUtil.getSaaSId(), ServiceMaster.class.getName(), existingServiceMaster.getId());

            log.info("Service  updated successfully...[" + existingServiceMaster.getId() + "]");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(existingServiceMaster);

        } catch (RestException exception) {

            log.error("RestException in Create method : ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (ObjectOptimisticLockingFailureException e) {

            log.error("Error in editing Service", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_LOCKED_RECORD);

        } catch (JpaObjectRetrievalFailureException e) {

            log.error("Error in editing Service", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_DELETED_RECORD);

        } catch (Exception e) {
            log.error("Exception ::: " + e);

            String exceptionCause = ExceptionUtils.getRootCauseMessage(e);

            log.error("Exception Cause  ::: " + exceptionCause);

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            if (exceptionCause.contains(UniqueKey.UK_SERVICE_SERVICECODE)) {
                baseDto.setResponseCode(ErrorCode.SERVICE_CODE_ALREADY_EXIST);
            }
            if (exceptionCause.contains(UniqueKey.UK_SERVICE_SERVICENAME)) {
                baseDto.setResponseCode(ErrorCode.SERVICE_NAME_ALREADY_EXIST);
            }
        }
        return appUtil.setDesc(baseDto);
    }


    public BaseDto delete(Long id) {

        log.info("delete  method is called....[ " + id + " ] ");

        BaseDto baseDto = new BaseDto();

        try {

            cacheRepository.delete(SaaSUtil.getSaaSId(), ServiceMaster.class.getName(), id, serviceMasterRepository);

            log.info("Successfully deleted Service by id...[ " + id + " ] ");

            baseDto.setResponseCode(ErrorCode.SUCCESS);


        } catch (DataIntegrityViolationException exception) {

            log.error("Exception occured : ", exception);

            ExceptionHelper helper = new ExceptionHelper(exception);

            log.error("Foreign key reference : " + helper.getFkReference());

            baseDto.setResponseCode(ErrorCode.ERROR_FK_CONSTRAINT);

        } catch (Exception exception) {

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            log.error("Exception in Delete method : ", exception);

            String rootCause = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Root Cause of Exception : " + rootCause);

            if (rootCause.contains("FK_")) {
                baseDto.setResponseCode(ErrorCode.CANNOT_DELETE_FK_RECORD);
            }
        }
        return appUtil.setDesc(baseDto);

    }

    /*
     * Returns all the service object as a list
     *
     * @Returns BaseDto
     */
    public BaseDto getAll() {

        log.info("getAll method is called....");

        BaseDto baseDto = new BaseDto();

        try {

            List<ServiceMaster> serviceMasterList = serviceMasterRepository.findAll();

            // Validating service list is empty or not
            ValidateUtil.notEmpty(serviceMasterList, ErrorCode.SERVICE_LIST_EMPTY);
            log.info("Successfully getting list of service...");
            baseDto.setResponseCode(ErrorCode.SUCCESS);
            baseDto.setResponseObject(serviceMasterList);

        } catch (RestException re) {
            log.error("Exception in getAll method ", re);
            baseDto.setResponseCode(re.getMessage());

        } catch (Exception e) {
            log.error("Exception in getAll method ", e);
            baseDto.setResponseCode(ErrorCode.FAILED);

        }
        return appUtil.setDesc(baseDto);
    }

    public BaseDto getAirExportService() {

        log.info("getAll method is called....");

        BaseDto baseDto = new BaseDto();

        try {

            ServiceMaster serviceMaster = serviceMasterRepository.getAirExportService();
            log.info("Successfully getting service...");
            baseDto.setResponseCode(ErrorCode.SUCCESS);
            baseDto.setResponseObject(serviceMaster);

        } catch (RestException re) {
            log.error("Exception in getAll method ", re);
            baseDto.setResponseCode(re.getMessage());

        } catch (Exception e) {
            log.error("Exception in getAll method ", e);
            baseDto.setResponseCode(ErrorCode.FAILED);

        }
        return appUtil.setDesc(baseDto);
    }


}

