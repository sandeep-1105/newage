package com.efreightsuite.service;

import java.util.List;

import com.efreightsuite.common.component.CacheRepository;
import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.ReportSearchDto;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.model.ReportsFormData;
import com.efreightsuite.repository.ReportsRepository;
import com.efreightsuite.search.ReportSearchImpl;
import com.efreightsuite.util.AppUtil;
import com.efreightsuite.util.SaaSUtil;
import com.efreightsuite.validation.SearchDtoValidator;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.exception.ExceptionUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author mani Date - 07.03.2016 Updated - 11.03.2016
 */
@Service
@Log4j2
public class ReportService {

    @Autowired
    private
    AppUtil appUtil;

    /**
     * ReportsRepository Object
     */

    @Autowired
    private
    ReportsRepository reportsRepository;

    @Autowired
    SearchDtoValidator searchDtoValidator;

    @Autowired
    private
    CacheRepository cacheRepository;

    @Autowired
    private
    ReportSearchImpl reportSearchImpl;

    public BaseDto search(ReportSearchDto searchDto) {
        log.info("Report Search Key method is called...." + searchDto);
        BaseDto baseDto = new BaseDto();
        try {
            baseDto.setResponseCode(ErrorCode.SUCCESS);
            baseDto.setResponseObject(reportSearchImpl.searchKey(searchDto));
            log.info("Successfully Searching Report...");
        } catch (Exception exception) {
            log.error("Exception in Report Search Key method : ", exception);
            baseDto.setResponseCode(ErrorCode.FAILED);
        }
        return appUtil.setDesc(baseDto);
    }

    public BaseDto create(ReportsFormData reportsFormData) {
        log.info("Create method is called.....");
        log.info("Create method is called - reportsId ..");

        final BaseDto baseDto = new BaseDto();
        try {
            cacheRepository.save(SaaSUtil.getSaaSId(), ReportsFormData.class.getName(), reportsFormData, reportsRepository);
            log.info("Reports Saved successfully.....");
            baseDto.setResponseCode(ErrorCode.SUCCESS);
            baseDto.setResponseObject(reportsFormData);

        } catch (RestException exception) {
            log.error("Exception Cause 1 ::: ", exception);
            baseDto.setResponseCode(exception.getMessage());

        } catch (Exception exception) {
            log.error("Exception occured : ", exception);
            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(exception);
            log.error("Exception Cause 1 : " + exceptionCause1);
            // General error condition
            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);
        }
        return appUtil.setDesc(baseDto);
    }

    public BaseDto getAllById(Long id) {
        log.info("getByReportId method is called...[" + id + "]");
        final BaseDto baseDto = new BaseDto();
        if (id != null) {
            try {
                log.info("getByIdReportsSearch before try method is called...[" + id + "]");
                List<ReportsFormData> reportsForm = reportsRepository.findById(id);
                log.info("getByIdReportsSearch afterList try method is called...[" + id + "]");
                for (ReportsFormData form : reportsForm) {
                    String labelName = form.getLabelName();
                    log.info("labelName:::" + labelName);
                }
                cacheRepository.get(SaaSUtil.getSaaSId(), ReportsFormData.class.getName(), id,
                        reportsRepository);
                baseDto.setResponseCode(ErrorCode.SUCCESS);
                baseDto.setResponseObject(reportsForm);

            } catch (RestException exception) {
                log.error("Exception in getByIdRepor method : ", exception);
                baseDto.setResponseCode(exception.getMessage());

            } catch (Exception exception) {
                log.error("Exception in getByIdReport method : ", exception);
                baseDto.setResponseCode(ErrorCode.FAILED);
            }
        }
        return appUtil.setDesc(baseDto);
    }

    public BaseDto reportSearch(ReportSearchDto dto) {
        log.info("ReportsSearch -> search method is called....");
        BaseDto baseDto = new BaseDto();

        try {
            baseDto.setResponseCode(ErrorCode.SUCCESS);
            baseDto.setResponseObject(reportSearchImpl.reportsEnquiry(dto));

        } catch (RestException re) {
            log.info("exception message", re);
            baseDto.setResponseCode(re.getMessage());

        } catch (Exception e) {
            log.info("exception message", e);
            baseDto.setResponseCode(ErrorCode.FAILED);

        }
        log.info("ReportsSearch -> search method is completed....");
        return appUtil.setDesc(baseDto);
    }
}
