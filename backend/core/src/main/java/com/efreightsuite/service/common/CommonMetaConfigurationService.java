package com.efreightsuite.service.common;

import java.io.ByteArrayInputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.FileUploadDto;
import com.efreightsuite.enumeration.AddressType;
import com.efreightsuite.enumeration.CorporateNonCorporate;
import com.efreightsuite.enumeration.LovStatus;
import com.efreightsuite.enumeration.YesNo;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.model.PartyAddressMaster;
import com.efreightsuite.model.PartyMasterDetail;
import com.efreightsuite.model.common.CommonCityMaster;
import com.efreightsuite.model.common.CommonConfigurationMaster;
import com.efreightsuite.model.common.CommonCountryMaster;
import com.efreightsuite.model.common.CommonCurrencyMaster;
import com.efreightsuite.model.common.CommonStateMaster;
import com.efreightsuite.repository.common.CommonCityMasterRepository;
import com.efreightsuite.repository.common.CommonConfigurationMasterRepository;
import com.efreightsuite.repository.common.CommonCountryMasterRepository;
import com.efreightsuite.repository.common.CommonCurrencyMasterRepository;
import com.efreightsuite.repository.common.CommonStateMasterRepository;
import com.efreightsuite.util.AppUtil;
import com.efreightsuite.util.ValidateUtil;
import com.efreightsuite.validation.PartyAddressMasterValidator;
import com.efreightsuite.validation.PartyMasterDetailValidator;
import lombok.extern.log4j.Log4j2;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class CommonMetaConfigurationService {

    @Autowired
    private
    CommonCountryMasterRepository commonCountryMasterRepository;

    @Autowired
    private
    CommonCityMasterRepository commonCityMasterRepository;

    @Autowired
    private
    CommonStateMasterRepository commonStateMasterRepository;

    @Autowired
    private
    CommonCurrencyMasterRepository commonCurrencyMasterRepository;

    @Autowired
    private
    PartyAddressMasterValidator partyAddressMasterValidator;

    @Autowired
    private
    PartyMasterDetailValidator partyMasterDetailValidator;

    @Autowired
    private
    CommonConfigurationMasterRepository commonConfigurationMasterRepository;

    @Autowired
    private
    AppUtil appUtil;

    private final Map<String, CommonCountryMaster> countryMap = new HashMap<>();
    private final Map<String, CommonCityMaster> cityMap = new HashMap<>();
    private final Map<String, CommonStateMaster> stateMap = new HashMap<>();
    private final Map<String, CommonCurrencyMaster> currencyMap = new HashMap<>();


    // Column index for agent or customer master template
    private final int cName = 0;
    private final int cCountryCode = 1;
    private final int cAddress1 = 2;
    private final int cAddress2 = 3;
    private final int cAddress3 = 4;
    private final int cCity = 5;
    private final int cState = 6;
    private final int cZipCode = 7;
    private final int cPoBox = 8;
    private final int cPhoneNo = 9;
    private final int cFaxNo = 10;
    private final int cContactPerson = 11;
    private final int cMobileNo = 12;
    private final int cEmail = 13;
    private final int cIsCustomer = 14;
    int cIsShipper = 15;
    int cIsConsignee = 16;
    int cIsForwarder = 17;
    int cIsNotify = 18;
    int cIsLocalAgent = 19;
    int cIsTrader = 20;
    int cIsBank = 21;
    private final int cIsOther = 22;
    private final int cBaseCurrencyCode = 23;
    private final int cPanNo = 24;
    private final int cTdsPercentage = 25;
    private final int cValueAddedTaxNo = 26;
    private final int cGstNo = 27;
    private final int cCcPercentage = 28;
    private final int cCafPercentage = 29;
    int cExporterCode = 30;
    int cImporterCode = 31;

    private final int partyMastertotalColumn = 32;


    private Map<String, String> gstNoMap;
    private Map<String, String> vatNoMap;
    private Map<String, String> panNoMap;
    private Map<String, String> partyNameMap;

    public void validatePartyList(BaseDto baseDto, FileUploadDto fileUploadDto) {
        gstNoMap = new HashMap<>();
        vatNoMap = new HashMap<>();
        panNoMap = new HashMap<>();
        partyNameMap = new HashMap<>();

        XSSFWorkbook workBook = null;

        if (fileUploadDto.getFileName().contains(".xlsx") || fileUploadDto.getFileName().contains(".xls")) {
            List<List<String>> records = workBookToRecord(fileUploadDto.getFile(), partyMastertotalColumn, workBook);
            int row = 0;
            for (List<String> record : records) {
                findDuplicateValues(baseDto, record, row);
                validatePartyMaster(baseDto, record, row);
                row++;
            }
        } else if (fileUploadDto.getFileName().contains(".csv")) {

            XSSFWorkbook workBookCsv = appUtil.csvToXLSX(fileUploadDto.getFile());

            List<List<String>> records = workBookToRecord(fileUploadDto.getFile(), partyMastertotalColumn, workBookCsv);

            if (records != null && records.size() > 1) {
                int row = 0;
                for (List<String> record : records.subList(1, records.size())) {
                    findDuplicateValues(baseDto, record, row);
                    validatePartyMaster(baseDto, record, row);
                    row++;
                }

            } else {
                throw new RestException(ErrorCode.META_CONF_XLSX_FILE_EMPTY);
            }
        }
    }

    private void findDuplicateValues(BaseDto baseDto, List<String> record, int row) {
        findPartyNameDuplicate(record, row, cName, baseDto);
        findPanNoDuplicate(record, row, cPanNo, baseDto);
        findVatNoDuplicate(record, row, cValueAddedTaxNo, baseDto);
        findGstNoDuplicate(record, row, cGstNo, baseDto);
    }

    private void findPartyNameDuplicate(List<String> record, int row, int col, BaseDto baseDto) {
        String value = record.get(col);
        if (value == null || value.trim().length() == 0) {
            return;
        } else {
            value = value.trim().toUpperCase();
        }
        if (partyNameMap.get(value) == null) {
            partyNameMap.put(value, value);
        } else {
            log.info("Party name duplicate found : " + value);
            throwError(baseDto, row, cName, record.get(cName), ErrorCode.META_CONF_FILE_DUPLICATE_FOUND);
        }
    }

    private void findPanNoDuplicate(List<String> record, int row, int col, BaseDto baseDto) {
        String value = record.get(col);
        if (value == null || value.trim().length() == 0) {
            return;
        } else {
            value = value.trim().toUpperCase();
        }
        if (panNoMap.get(value) == null) {
            panNoMap.put(value, value);
        } else {
            log.info("PAN number duplicate found : " + value);
            throwError(baseDto, row, cPanNo, record.get(cPanNo), ErrorCode.META_CONF_FILE_DUPLICATE_FOUND);
        }
    }

    private void findVatNoDuplicate(List<String> record, int row, int col, BaseDto baseDto) {
        String value = record.get(col);
        if (value == null || value.trim().length() == 0) {
            return;
        } else {
            value = value.trim().toUpperCase();
        }
        if (vatNoMap.get(value) == null) {
            vatNoMap.put(value, value);
        } else {
            log.info("VAT number duplicate found : " + value);
            throwError(baseDto, row, cValueAddedTaxNo, record.get(cValueAddedTaxNo), ErrorCode.META_CONF_FILE_DUPLICATE_FOUND);
        }
    }

    private void findGstNoDuplicate(List<String> record, int row, int col, BaseDto baseDto) {
        String value = record.get(col);
        if (value == null || value.trim().length() == 0) {
            return;
        } else {
            value = value.trim().toUpperCase();
        }
        if (gstNoMap.get(value) == null) {
            gstNoMap.put(value, value);
        } else {
            log.info("GST number duplicate found : " + value);
            throwError(baseDto, row, cGstNo, record.get(cGstNo), ErrorCode.META_CONF_FILE_DUPLICATE_FOUND);
        }
    }

    private void validatePartyMaster(BaseDto baseDto, List<String> record, int row) {

        validatePartyName(record, row, cName, baseDto, true);

        validateCountryField(record, row, cCountryCode, baseDto, true);

        validatePartyAdressList(record, row, baseDto, true);

        validateState(record, row, cState, baseDto, false, null);

        validateCity(record, row, cCity, baseDto, false, null, null);

        validatePartyMasterDetail(record, row, baseDto);

        validatePartType(baseDto, record, row);

        validateCurrency(record, row, cBaseCurrencyCode, baseDto, false);

    }

    private void validatePartType(BaseDto baseDto, List<String> record, int row) {
        for (int i = cIsCustomer; i <= cIsOther; i++) {
            if (record.get(i) != null && record.get(i).trim().length() != 0) {
                if (!record.get(i).equalsIgnoreCase("y")) {
                    throwError(baseDto, row, i, record.get(i), ErrorCode.META_CONF_CUSTOMER_TYPE_FORMAT);
                }
            }
        }
        log.info("Party type validated");
    }

    private void validateCountryField(List<String> record, int row, int col, BaseDto baseDto, boolean isMandatory) {

        String value = record.get(col);
        if (value == null || value.trim().length() == 0) {
            if (isMandatory) {
                throwError(baseDto, row, col, "", ErrorCode.META_CONF_DATA_IS_MANDATORY);
            }
        } else {
            value = value.trim().toUpperCase();
        }

        if (value != null && value.trim().length() != 0) {
            if (countryMap.get(value) == null) {
                CommonCountryMaster object = commonCountryMasterRepository.findByCountryCode(value);
                if (object == null) {
                    throwError(baseDto, row, col, value, ErrorCode.META_CONF_DATA_IS_NOT_FOUND);
                } else {
                    if (object.getStatus() == LovStatus.Block) {
                        throwError(baseDto, row, col, value, ErrorCode.META_CONF_DATA_BLOCKED);
                    } else if (object.getStatus() == LovStatus.Hide) {
                        throwError(baseDto, row, col, value, ErrorCode.META_CONF_DATA_HIDDEN);
                    } else {
                        validateState(record, row, cState, baseDto, false, object.getId());
                        countryMap.put(value, object);
                    }
                }
            } else {
                validateState(record, row, cState, baseDto, false, countryMap.get(value).getId());
            }
        }
        log.info("Country code validated : " + value);
    }

    private void validatePartyName(List<String> record, int row, int col, BaseDto baseDto, boolean isMandatory) {

        String value = record.get(col);
        if (value == null || value.trim().length() == 0 || value.equals("0")) {
            if (isMandatory) {
                throwError(baseDto, row, col, "", ErrorCode.META_CONF_DATA_IS_MANDATORY);
            }

        } else {
            if (value.length() > 100) {
                throwError(baseDto, row, col, value, ErrorCode.META_CONF_FILE_INVALID);
            }
        }
        log.info("Party name validated : " + value);
    }

    private void validateCurrency(List<String> record, int row, int col, BaseDto baseDto, boolean isMandatory) {

        String value = record.get(col);
        if (value == null || value.trim().length() == 0) {
            if (isMandatory) {
                throwError(baseDto, row, col, "", ErrorCode.META_CONF_DATA_IS_MANDATORY);
            }

        } else {
            value = value.trim().toUpperCase();
        }

        if (value != null && value.trim().length() != 0 && currencyMap.get(value) == null) {
            CommonCurrencyMaster object = commonCurrencyMasterRepository.findByCurrencyCode(value);
            if (object == null) {
                throwError(baseDto, row, col, value, ErrorCode.META_CONF_DATA_IS_NOT_FOUND);
            } else {
                if (object.getStatus() == LovStatus.Block) {
                    throwError(baseDto, row, col, value, ErrorCode.META_CONF_DATA_BLOCKED);
                } else if (object.getStatus() == LovStatus.Hide) {
                    throwError(baseDto, row, col, value, ErrorCode.META_CONF_DATA_HIDDEN);
                } else {
                    currencyMap.put(value, object);
                }
            }
        }
        log.info("Currency code validated : " + value);
    }

    private void validateCity(List<String> record, int row, int col, BaseDto baseDto, boolean isMandatory,
                              Long CountryId, Long stateId) {

        String value = record.get(col);
        if (value == null || value.trim().length() == 0) {
            if (isMandatory) {
                throwError(baseDto, row, col, "", ErrorCode.META_CONF_DATA_IS_MANDATORY);
            }
        } else {
            value = value.trim().toUpperCase();
        }

        if (value != null && value.trim().length() != 0 && cityMap.get(value) == null) {
            CommonCityMaster object = commonCityMasterRepository.findByCityName(value);
            if (object == null) {
                throwError(baseDto, row, col, value, ErrorCode.META_CONF_DATA_IS_NOT_FOUND);
            } else {
                if (object.getStatus() == LovStatus.Block) {
                    throwError(baseDto, row, col, value, ErrorCode.META_CONF_DATA_BLOCKED);
                } else if (object.getStatus() == LovStatus.Hide) {
                    throwError(baseDto, row, col, value, ErrorCode.META_CONF_DATA_HIDDEN);
                } else {
                    cityMap.put(value, object);
                }
            }
        }

        if (CountryId != null) {
            if (cityMap.get(value).getCountryMaster() != null && !cityMap.get(value).getCountryMaster().getId().equals(CountryId)) {
                throwError(baseDto, row, col, value, ErrorCode.META_CONF_CITY_IS_NOT_IN_STATE_COUNTRY);
            }
        }

        if (stateId != null) {
            if (cityMap.get(value).getStateMaster() != null && !cityMap.get(value).getStateMaster().getId().equals(stateId)) {
                throwError(baseDto, row, col, value, ErrorCode.META_CONF_CITY_IS_NOT_IN_STATE_COUNTRY);
            }
        }

        log.info("City name validated : " + value);
    }

    private void validateState(List<String> record, int row, int col, BaseDto baseDto, boolean isMandatory,
                               Long countryId) {

        String value = record.get(col);
        if (value == null || value.trim().length() == 0) {
            if (isMandatory) {
                throwError(baseDto, row, col, "", ErrorCode.META_CONF_DATA_IS_MANDATORY);
            }
        } else {
            value = value.trim().toUpperCase();
        }
        if (value != null && value.trim().length() != 0) {
            if (stateMap.get(value) == null) {
                CommonStateMaster object = commonStateMasterRepository.findByStateName(value);
                if (object == null) {
                    throwError(baseDto, row, col, value, ErrorCode.META_CONF_DATA_IS_NOT_FOUND);
                } else {
                    if (object.getStatus() == LovStatus.Block) {
                        throwError(baseDto, row, col, value, ErrorCode.META_CONF_DATA_BLOCKED);
                    } else if (object.getStatus() == LovStatus.Hide) {
                        throwError(baseDto, row, col, value, ErrorCode.META_CONF_DATA_HIDDEN);
                    } else {
                        validateCity(record, row, cCity, baseDto, false, countryId, object.getId());
                        stateMap.put(value, object);
                    }
                }
            } else {
                validateCity(record, row, cCity, baseDto, false, countryId, stateMap.get(value).getId());
            }
        }

        if (stateMap.get(value) != null && countryId != null && stateMap.get(value).getCountryMaster() != null && !stateMap.get(value).getCountryMaster().getId().equals(countryId)) {
            throwError(baseDto, row, cState, value, ErrorCode.META_CONF_STATE_IS_NOT_IN_COUNTRY);
        }
        log.info("State name validated : " + value);
    }

    private void validatePartyAdressList(List<String> record, int row, BaseDto baseDto, boolean isMandatory) {

        PartyAddressMaster partyAddressMaster = new PartyAddressMaster();

        if (record.get(cAddress1) == null || record.get(cAddress1).equals("") || record.get(cAddress1).length() == 0) {
            throwError(baseDto, row, cAddress1, "", ErrorCode.META_CONF_DATA_IS_MANDATORY);
        } else {
            partyAddressMaster.setAddressLine1(record.get(cAddress1));
            try {
                partyAddressMasterValidator.verifyAddressLine1(partyAddressMaster);
            } catch (RestException e) {
                throwError(baseDto, row, cAddress1, record.get(cAddress1), ErrorCode.META_CONF_FILE_INVALID);
            }
        }

        try {

            partyAddressMaster.setAddressLine2(record.get(cAddress2));

            partyAddressMasterValidator.verifyAddressLine2(partyAddressMaster);

        } catch (RestException e) {

            throwError(baseDto, row, cAddress2, record.get(cAddress2), ErrorCode.META_CONF_FILE_INVALID);

        }

        try {
            partyAddressMaster.setAddressLine3(record.get(cAddress3));
            partyAddressMasterValidator.verifyAddressLine3(partyAddressMaster);
        } catch (RestException e) {
            throwError(baseDto, row, cAddress3, record.get(cAddress3), ErrorCode.META_CONF_FILE_INVALID);
        }

        partyAddressMaster.setAddressType(AddressType.Primary);
        partyAddressMaster.setCorporate(YesNo.Yes);

        partyAddressMaster.setZipCode(record.get(cZipCode));

        try {
            partyAddressMasterValidator.verifyZipCode(partyAddressMaster);
        } catch (RestException e) {
            throwError(baseDto, row, cZipCode, record.get(cZipCode), ErrorCode.META_CONF_FILE_INVALID);
        }
        partyAddressMaster.setPoBox(record.get(cPoBox));

        try {
            partyAddressMasterValidator.verifyPoBoxNumber(partyAddressMaster);
        } catch (RestException e) {
            throwError(baseDto, row, cPoBox, record.get(cPoBox), ErrorCode.META_CONF_FILE_INVALID);
        }
        partyAddressMaster.setPhone(record.get(cPhoneNo));

        try {
            partyAddressMasterValidator.verifyPhoneNumber(partyAddressMaster);
        } catch (RestException e) {
            throwError(baseDto, row, cPhoneNo, record.get(cPhoneNo), ErrorCode.META_CONF_FILE_INVALID);
        }
        partyAddressMaster.setFax(record.get(cFaxNo));

        try {
            partyAddressMasterValidator.verifyFax(partyAddressMaster);
        } catch (RestException e) {
            throwError(baseDto, row, cFaxNo, record.get(cFaxNo), ErrorCode.META_CONF_FILE_INVALID);
        }
        partyAddressMaster.setContactPerson(record.get(cContactPerson));

        try {
            partyAddressMasterValidator.verifyContactPerson(partyAddressMaster);
        } catch (RestException e) {
            throwError(baseDto, row, cContactPerson, record.get(cContactPerson), ErrorCode.META_CONF_FILE_INVALID);
        }
        partyAddressMaster.setMobileNo(record.get(cMobileNo));

        try {
            partyAddressMasterValidator.verifyMobileNumber(partyAddressMaster);
        } catch (RestException e) {
            throwError(baseDto, row, cMobileNo, record.get(cMobileNo), ErrorCode.META_CONF_FILE_INVALID);
        }
        if (record.get(cEmail) != null && record.get(cEmail).trim() != "") {
            partyAddressMaster.setEmail(record.get(cEmail));

            if (!isValidEmailId(record.get(cEmail))) {
                throwError(baseDto, row, cEmail, record.get(cEmail), ErrorCode.META_CONF_FILE_INVALID);
            }
        }
        log.info("Party address list validated");
    }

    private void validatePartyMasterDetail(List<String> record, int row, BaseDto baseDto) {
        int count = 0;
        for (int i = cPanNo; i <= cCafPercentage; i++) {
            if (record.get(i) != null && !record.get(i).equals("")) {
                count++;
            }
        }

        PartyMasterDetail partyMasterDetail = new PartyMasterDetail();
        if (count != 0) {

            partyMasterDetail.setPanNo(record.get(cPanNo));
            partyMasterDetail.setValueAddedTaxNo(record.get(cValueAddedTaxNo));
            partyMasterDetail.setGstNo(record.get(cGstNo));

            try {
                partyMasterDetailValidator.verifyPanNumber(partyMasterDetail);
            } catch (RestException e) {
                throwError(baseDto, row, cPanNo, record.get(cPanNo), ErrorCode.META_CONF_FILE_INVALID);
            }

            try {
                partyMasterDetailValidator.verifyVatNumber(partyMasterDetail);
            } catch (RestException e) {
                throwError(baseDto, row, cValueAddedTaxNo, record.get(cValueAddedTaxNo),
                        ErrorCode.META_CONF_FILE_INVALID);
            }

            try {
                partyMasterDetailValidator.verifyGstNumber(partyMasterDetail);
            } catch (RestException e) {
                throwError(baseDto, row, cGstNo, record.get(cGstNo), ErrorCode.META_CONF_FILE_INVALID);
            }

            if (record.get(cTdsPercentage) != null && !record.get(cTdsPercentage).equals("")
                    && record.get(cTdsPercentage).length() != 0) {
                partyMasterDetail.setTdsPercentage(cTdsPercentage);
                try {
                    partyMasterDetailValidator.verifyTDSPercentage(partyMasterDetail);
                } catch (RestException e) {
                    throwError(baseDto, row, cTdsPercentage, record.get(cTdsPercentage),
                            ErrorCode.META_CONF_FILE_INVALID);
                }
            }

            if (record.get(cCcPercentage) != null && !record.get(cCcPercentage).equals("")
                    && record.get(cCcPercentage).length() != 0) {
                partyMasterDetail.setCcPercentage(record.get(cCcPercentage) != null
                        ? record.get(cCcPercentage).equals("") ? null : new Double(record.get(cCcPercentage)) : null);
                try {
                    partyMasterDetailValidator.verifyCCPercentage(partyMasterDetail);
                } catch (RestException e) {
                    throwError(baseDto, row, cCcPercentage, record.get(cCcPercentage),
                            ErrorCode.META_CONF_FILE_INVALID);
                }
            }

            if (record.get(cCafPercentage) != null && !record.get(cCafPercentage).equals("")
                    && record.get(cCafPercentage).length() != 0) {
                partyMasterDetail.setCafPercentage(record.get(cCafPercentage) != null
                        ? record.get(cCafPercentage).equals("") ? null : new Double(record.get(cCafPercentage)) : null);
                try {
                    partyMasterDetailValidator.verifyCafPercentage(partyMasterDetail);
                } catch (RestException e) {
                    throwError(baseDto, row, cCafPercentage, record.get(cCafPercentage),
                            ErrorCode.META_CONF_FILE_INVALID);
                }
            }

            partyMasterDetail.setCorporate(CorporateNonCorporate.CORPORATE);
        } else {
            partyMasterDetail = null;
        }
        log.info("Party detail validated ");
    }

    private List<List<String>> workBookToRecord(byte[] file, int totalColumn, XSSFWorkbook workBook) {
        List<List<String>> rowRecords = new ArrayList<>();

        try {

            //Create an object of FileInputStream class to read excel file

            ByteArrayInputStream bis = new ByteArrayInputStream(file);

            if (workBook == null) {
                workBook = new XSSFWorkbook(bis);
            }

            XSSFSheet sheet = workBook.getSheetAt(0);

            for (int i = 1; i <= sheet.getLastRowNum(); i++) {
                rowRecords.add(rowToRecord(sheet.getRow(i), totalColumn));
            }

            workBook.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return rowRecords;

    }

    private void throwError(BaseDto baseDto, int row, int col, String value, String errorCode) {
        baseDto.getParams().add(value);
        baseDto.getParams().add((row + 1) + "");
        baseDto.getParams().add((col + 1) + "");
        throw new RestException(errorCode);
    }

    private List<String> rowToRecord(XSSFRow row, int totalColumn) {
        List<String> record = new ArrayList<>();
        for (int i = 0; i < totalColumn; i++) {
            if (row != null && row.getCell(i) != null) {
                if (row.getCell(i).getCellType() == Cell.CELL_TYPE_STRING) {
                    record.add(row.getCell(i).getStringCellValue());
                } else {
                    Double numeric = row.getCell(i).getNumericCellValue();
                    String str = Double.toString(numeric);
                    record.add(new BigDecimal(str).stripTrailingZeros().toPlainString());

                }

            } else {
                record.add("");
            }
        }
        return record;
    }

    private boolean isValidEmailId(String email) {
        String emailPattern = "^[\\w!#$%&’*+/=?`{|}~^-]+(?:\\.[\\w!#$%&’*+/=?`{|}~^-]+)*@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$";
        Pattern p = Pattern.compile(emailPattern);
        Matcher m = p.matcher(email);
        return m.matches();
    }

    public void validateCustomerOrAgentBulkUpload(FileUploadDto fileUploadDto) {

        ValidateUtil.notNull(fileUploadDto.getFile(), ErrorCode.META_CONF_XLSX_FILE_REQUIRED);

        ValidateUtil.nonZero(fileUploadDto.getFile().length, ErrorCode.META_CONF_XLSX_FILE_EMPTY);

        CommonConfigurationMaster commonConfigurationMaster = commonConfigurationMasterRepository
                .findByCode("file.size.party.master.bulk.upload");

        if (commonConfigurationMaster != null && commonConfigurationMaster.getValue() != null
                && commonConfigurationMaster.getValue().trim() != "") {

            long uploadedFileSize = fileUploadDto.getFile().length;

            long allowedSize = Long.parseLong(commonConfigurationMaster.getValue().trim()) * 1024 * 1024;

            ValidateUtil.aboveRange(uploadedFileSize, allowedSize, ErrorCode.META_CONF_XLSX_FILE_LENGTH_EXCEED);
        }

    }

}
