package com.efreightsuite.service;

import com.efreightsuite.common.component.CacheRepository;
import com.efreightsuite.dto.BankSearchDto;
import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.SearchRequest;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.ExceptionHelper;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.exception.UniqueKey;
import com.efreightsuite.model.BankMaster;
import com.efreightsuite.repository.BankMasterRepository;
import com.efreightsuite.search.BankSearchService;
import com.efreightsuite.util.AppUtil;
import com.efreightsuite.util.SaaSUtil;
import com.efreightsuite.util.ValidateUtil;
import com.efreightsuite.validation.BankMasterValidator;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.exception.ExceptionUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.orm.ObjectOptimisticLockingFailureException;
import org.springframework.orm.jpa.JpaObjectRetrievalFailureException;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class BankMasterService {


    @Autowired
    private
    BankMasterValidator bankMasterValidator;

    @Autowired
    private
    CacheRepository cacheRepository;

    @Autowired
    private
    BankMasterRepository bankMasterRepository;

    @Autowired
    private
    BankSearchService bankSearchService;


    @Autowired
    private
    AppUtil appUtil;

    /**
     * This service method is used to get bank details using ID.
     *
     * @param id This is the parameter to get details
     * @return bank object .
     */
    public BaseDto getBankById(Long id) {

        final BaseDto baseDto = new BaseDto();

        try {

            BankMaster bankMaster = cacheRepository.get(SaaSUtil.getSaaSId(), BankMaster.class.getName(), id,
                    bankMasterRepository);

            ValidateUtil.notNull(bankMaster, ErrorCode.BANK_ID_NOT_FOUND);

            log.info("Getting bank successfully");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(bankMaster);

        } catch (final RestException exception) {

            log.error("Exception in getBybank method : ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (final Exception exception) {

            log.error("Exception in getBybank method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);
    }

    /**
     * This service method is used to get bank details .
     *
     * @param searchDto This is the parameter to get details
     * @return bank object .
     */
    public BaseDto search(BankSearchDto searchDto) {

        log.info("Search method is called...." + searchDto);

        BaseDto baseDto = new BaseDto();

        try {

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(bankSearchService.search(searchDto));

            log.info("Successfully Searching Pack...");

        } catch (Exception exception) {

            log.error("Exception in Search method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);

    }

    /**
     * This service method is used to get bank details.
     *
     * @param searchRequest This is the parameter to get details
     * @return bank object .
     */
    public BaseDto search(SearchRequest searchRequest) {
        log.info("Search method is called...." + searchRequest);

        BaseDto baseDto = new BaseDto();

        try {

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(bankSearchService.search(searchRequest));

            log.info("Successfully Searching Bank...");

        } catch (Exception exception) {

            log.error("Exception in Search method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);
    }


    /**
     * This service method is used to Create bankMaster .
     *
     * @return bank object .
     */
    public BaseDto create(BankMaster newBankMaster) {


        log.info("Save method is called.....");

        BaseDto baseDto = new BaseDto();

        try {

            bankMasterValidator.validate(newBankMaster);

            BankMaster bankMasterCreated = cacheRepository.save(SaaSUtil.getSaaSId(), BankMaster.class.getName(), newBankMaster, bankMasterRepository);

            log.info("Bank created successfully...[" + newBankMaster.getId() + "]");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(bankMasterCreated);

        } catch (RestException exception) {

            log.error("RestException in Create method : ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (Exception e) {

            String exceptionCause = ExceptionUtils.getRootCauseMessage(e);

            log.error("Exception Cause  ::: " + exceptionCause);

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            if (exceptionCause.contains(UniqueKey.UK_BANK_BANKCODE)) {
                baseDto.setResponseCode(ErrorCode.BANK_CODE_ALREADY_EXIST);
            }
            if (exceptionCause.contains(UniqueKey.UK_BANK_BANKNAME)) {
                baseDto.setResponseCode(ErrorCode.BANK_NAME_ALREADY_EXIST);
            }
        }
        return appUtil.setDesc(baseDto);


    }

    /**
     * This service method is used to get bank details using ID.
     *
     * @return bank object .
     */

    public BaseDto update(BankMaster existingBankMaster) {


        BaseDto baseDto = new BaseDto();

        try {

            bankMasterValidator.validate(existingBankMaster);

            bankMasterRepository.save(existingBankMaster);

            cacheRepository.remove(SaaSUtil.getSaaSId(), BankMaster.class.getName(), existingBankMaster.getId());

            log.info("Bank updated successfully...[" + existingBankMaster.getId() + "]");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(existingBankMaster);

        } catch (RestException exception) {

            log.error("RestException in Create method : ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (ObjectOptimisticLockingFailureException e) {

            log.error("Error in editing Service", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_LOCKED_RECORD);

        } catch (JpaObjectRetrievalFailureException e) {

            log.error("Error in editing Service", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_DELETED_RECORD);

        } catch (Exception e) {
            log.error("Exception ::: " + e);

            String exceptionCause = ExceptionUtils.getRootCauseMessage(e);

            log.error("Exception Cause  ::: " + exceptionCause);

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            if (exceptionCause.contains(UniqueKey.UK_BANK_BANKCODE)) {
                baseDto.setResponseCode(ErrorCode.BANK_CODE_ALREADY_EXIST);
            }
            if (exceptionCause.contains(UniqueKey.UK_BANK_BANKNAME)) {
                baseDto.setResponseCode(ErrorCode.BANK_NAME_ALREADY_EXIST);
            }
        }
        return appUtil.setDesc(baseDto);

    }

    /**
     * This service method is used to delete bank object from db.
     *
     * @return bank object .
     */
    public BaseDto delete(Long id) {

        BaseDto baseDto = new BaseDto();

        try {

            log.info("delete  method is called....[ " + id + " ] ");

            cacheRepository.delete(SaaSUtil.getSaaSId(), BankMaster.class.getName(), id, bankMasterRepository);

            log.info("Successfully deleted Service by id...[ " + id + " ] ");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

        } catch (DataIntegrityViolationException exception) {

            log.error("Exception occured : ", exception);

            ExceptionHelper helper = new ExceptionHelper(exception);

            log.error("Foreign key reference : " + helper.getFkReference());

            baseDto.setResponseCode(ErrorCode.ERROR_FK_CONSTRAINT);

        } catch (Exception exception) {

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            log.error("Exception in Delete method : ", exception);

            String rootCause = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Root Cause of Exception : " + rootCause);

            if (rootCause.contains("FK_")) {
                baseDto.setResponseCode(ErrorCode.CANNOT_DELETE_FK_RECORD);
            }
        }
        return appUtil.setDesc(baseDto);
    }

}
