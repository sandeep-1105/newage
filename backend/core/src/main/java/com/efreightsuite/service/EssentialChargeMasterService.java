package com.efreightsuite.service;

import java.util.List;

import com.efreightsuite.common.component.CacheRepository;
import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.EssentialChargeSearchDto;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.exception.UniqueKey;
import com.efreightsuite.model.EssentialChargeMaster;
import com.efreightsuite.repository.EssentialChargeMasterRepository;
import com.efreightsuite.search.EssentialChargeSearchImpl;
import com.efreightsuite.util.AppUtil;
import com.efreightsuite.util.ValidateUtil;
import com.efreightsuite.validation.EssentialChargeMasterValidator;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.exception.ExceptionUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.ObjectOptimisticLockingFailureException;
import org.springframework.orm.jpa.JpaObjectRetrievalFailureException;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class EssentialChargeMasterService {

    @Autowired
    private
    AppUtil appUtil;

    @Autowired
    CacheRepository cacheRepository;

    @Autowired
    private
    EssentialChargeMasterRepository essentialChargeMasterRepository;

    @Autowired
    private
    EssentialChargeSearchImpl essentialChargeSearchImpl;

    @Autowired
    private
    EssentialChargeMasterValidator essentialChargeMasterValidator;

    public BaseDto search(EssentialChargeSearchDto searchDto) {

        log.info("Search method is called...." + searchDto);

        BaseDto baseDto = new BaseDto();
        try {
            baseDto.setResponseCode(ErrorCode.SUCCESS);
            baseDto.setResponseObject(essentialChargeSearchImpl.search(searchDto));
            log.info("Successfully Searching EssentialCharge...");

        } catch (Exception exception) {

            log.error("Exception in Search method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);

    }

    public BaseDto essentialcharges(EssentialChargeSearchDto searchDto) {

        log.info("Search method is called...." + searchDto);

        BaseDto baseDto = new BaseDto();
        try {
            baseDto.setResponseCode(ErrorCode.SUCCESS);
            baseDto.setResponseObject(essentialChargeSearchImpl.essentialcharges(searchDto));
            log.info("Successfully Searching EssentialCharge...");

        } catch (Exception exception) {

            log.error("Exception in Search method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);

    }

    public BaseDto get(Long id) {

        log.info("get method is called...[" + id + "]");

        BaseDto baseDto = new BaseDto();

        try {

            EssentialChargeMaster essentialChargeMaster = essentialChargeMasterRepository.getOne(id);

            log.info("get is successfully executed....[" + essentialChargeMaster.getId() + "]");

            baseDto.setResponseCode(ErrorCode.SUCCESS);
            baseDto.setResponseObject(essentialChargeMaster);

        } catch (Exception exception) {

            log.error("Exception in get method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);
    }

    public BaseDto create(List<EssentialChargeMaster> essentialChargeMasterList) {

        log.info("Create method is called");

        BaseDto baseDto = new BaseDto();

        try {

            for (EssentialChargeMaster essentialChargeMaster : essentialChargeMasterList) {

                essentialChargeMasterValidator.validate(essentialChargeMaster);

            }

            essentialChargeMasterRepository.save(essentialChargeMasterList);

            log.info("EssentialChargeMaster list Saved successfully");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(null);

        } catch (RestException exception) {

            log.error("Exception in EssentialChargeMaster.create method ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (Exception exception) {

            log.error("Exception in EssentialChargeMaster.create method ", exception);

            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(exception);

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            log.error("Exception Cause 1 : " + exceptionCause1);

            if (exceptionCause1.contains(UniqueKey.UK_ESSENTIAL_CHARGE_MASTER)) {
                baseDto.setResponseCode(ErrorCode.ESSENTIAL_ALREADY_EXIST);
            }

            // General error condition


        }

        return appUtil.setDesc(baseDto);
    }

    public BaseDto update(EssentialChargeMaster existingEssentialChargeMaster) {

        log.info("Update method is called - existingEssentialChargeMaster : [" + existingEssentialChargeMaster.getId() + "]");

        BaseDto baseDto = new BaseDto();

        try {

            essentialChargeMasterValidator.validate(existingEssentialChargeMaster);

            ValidateUtil.notNull(existingEssentialChargeMaster.getId(), ErrorCode.ESSENTIAL_ID_NOT_NULL);

            essentialChargeMasterRepository.getOne(existingEssentialChargeMaster.getId());

            essentialChargeMasterRepository.save(existingEssentialChargeMaster);

            log.info("EssentialChargeMaster updated successfully....");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(null);

        } catch (RestException exception) {

            log.error("Exception in EssentialChargeMaster.update method ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (ObjectOptimisticLockingFailureException exception) {

            log.error("Exception in EssentialChargeMaster.update method ", exception);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_LOCKED_RECORD);

        } catch (JpaObjectRetrievalFailureException exception) {

            log.error("Exception in EssentialChargeMaster.update method ", exception);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_DELETED_RECORD);

        } catch (Exception exception) {

            log.error("Exception in EssentialChargeMaster.update method ", exception);

            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Exception Cause 1 ::: " + exceptionCause1);

            // General error condition
            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            if (exceptionCause1.contains(UniqueKey.UK_ESSENTIAL_CHARGE_MASTER)) {
                baseDto.setResponseCode(ErrorCode.ESSENTIAL_ALREADY_EXIST);
            }

        }

        return appUtil.setDesc(baseDto);
    }

    public BaseDto delete(Long id) {

        log.info("Delete method is Invoked...[" + id + "]");

        BaseDto baseDto = new BaseDto();

        try {

            //cacheRepository.delete(SaaSUtil.getSaaSId(), EssentialChargeMaster.class.getName(), id, essentialChargeMasterRepository);

            essentialChargeMasterRepository.delete(id);

            log.info("EssentialChargeMaster Deleted Successfully....[" + id + "]");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

        } catch (RestException exception) {

            log.error("Exception in EssentialChargeMaster.delete method ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (Exception exception) {

            log.error("Exception in EssentialChargeMaster.delete method ", exception);

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            String rootCause = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Root Cause of Exception : " + rootCause);

            if (rootCause.contains("FK_")) {
                baseDto.setResponseCode(ErrorCode.CANNOT_DELETE_FK_RECORD);
            }
        }

        return appUtil.setDesc(baseDto);
    }

    public BaseDto getCharge(Long id) {

        log.info("get Charge based on service id essential charge master" + id);

        BaseDto baseDto = new BaseDto();

        try {

            List<EssentialChargeMaster> essentialChargeList = essentialChargeMasterRepository.findByService(id);

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(essentialChargeList);

        } catch (Exception exception) {

            log.error("Exception in get method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);


    }
}
