package com.efreightsuite.service;


import com.efreightsuite.common.component.CacheRepository;
import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.GeneralNoteMasterSearchDto;
import com.efreightsuite.enumeration.GeneralNoteCategory;
import com.efreightsuite.enumeration.GeneralNoteSubCategory;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.ExceptionHelper;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.model.GeneralNoteMaster;
import com.efreightsuite.repository.GeneralNoteMasterRepository;
import com.efreightsuite.search.GeneralNoteSearchImpl;
import com.efreightsuite.util.AppUtil;
import com.efreightsuite.util.SaaSUtil;
import com.efreightsuite.util.ValidateUtil;
import com.efreightsuite.validation.GeneralNoteMasterValidator;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.exception.ExceptionUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.orm.ObjectOptimisticLockingFailureException;
import org.springframework.orm.jpa.JpaObjectRetrievalFailureException;
import org.springframework.stereotype.Service;


@Service
@Log4j2
public class GeneralNoteMasterService {
    @Autowired
    private
    GeneralNoteMasterRepository generalNoteMasterRepository;

    @Autowired
    private
    GeneralNoteMasterValidator generalNoteMasterValidator;

    @Autowired
    private
    CacheRepository cacheRepository;

    @Autowired
    private
    AppUtil appUtil;

    @Autowired
    private
    GeneralNoteSearchImpl generalNoteSearchImpl;

    public BaseDto getGeneralNote(Long locationId, GeneralNoteCategory category, GeneralNoteSubCategory subCategory, Long serviceMasterId) {

        BaseDto baseDto = new BaseDto();

        try {
            baseDto.setResponseCode(ErrorCode.SUCCESS);

            if (serviceMasterId == null || serviceMasterId <= -1) {
                baseDto.setResponseObject(generalNoteMasterRepository.getGeneralNote(category, subCategory, locationId));
            } else {
                baseDto.setResponseObject(generalNoteMasterRepository.getGeneralNote(category, subCategory, serviceMasterId, locationId));
            }

        } catch (RestException exception) {

            log.error("Exception in get method... ", exception);


            baseDto.setResponseCode(exception.getMessage());

        } catch (Exception exception) {

            log.error("Exception in get method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);
    }


    /**
     * Method - create Creates and returns a new GeneralNoteMaster
     *
     * @returns BaseDto
     */

    public BaseDto create(GeneralNoteMaster gneralNoteMaster) {

        log.info("Create method is called - GeneralNoteMasterId ..");


        final BaseDto baseDto = new BaseDto();

        try {

            generalNoteMasterValidator.validate(gneralNoteMaster);

            cacheRepository.save(SaaSUtil.getSaaSId(), GeneralNoteMaster.class.getName(), gneralNoteMaster, generalNoteMasterRepository);

            log.info("gneralNoteMaster Saved successfully.....");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(gneralNoteMaster);

        } catch (RestException exception) {

            log.error("Exception Cause 1 ::: ", exception);
            baseDto.setResponseCode(exception.getMessage());


        } catch (Exception exception) {

            log.error("Exception occured : ", exception);


            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Exception Cause 1 : " + exceptionCause1);

            // General error condition
            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

			
			/*if (exceptionCause1.contains(UniqueKey.UK_SERVTAXCATE_CODE)) {
                baseDto.setResponseCode(ErrorCode.SERVICETAXCATEGORY_CODE_ALREADY_EXIST);

			}

			if (exceptionCause1.contains(UniqueKey.UK_SERVTAXCATE_NAME)) {
				baseDto.setResponseCode(ErrorCode.SERVICETAXCATEGORY_NAME_ALREADY_EXIST);
			}
*/
        }

        return appUtil.setDesc(baseDto);
    }


    /**
     * Method - update Updates and returns a updated GeneralNoteMaster
     *
     * @returns GeneralNoteMaster
     */

    public BaseDto update(GeneralNoteMaster exsistingGeneralNoteMaster) {

        log.info("Update method is called - exsistingGeneralNoteMaster : [" + exsistingGeneralNoteMaster.getId() + "]");

        BaseDto baseDto = new BaseDto();


        try {

            ValidateUtil.notNull(exsistingGeneralNoteMaster.getId(), ErrorCode.GENERALNOTE_ID_NOT_NULL);

            generalNoteMasterValidator.validate(exsistingGeneralNoteMaster);

            generalNoteMasterRepository.getOne(exsistingGeneralNoteMaster.getId());

            exsistingGeneralNoteMaster = cacheRepository.save(SaaSUtil.getSaaSId(), GeneralNoteMaster.class.getName(), exsistingGeneralNoteMaster,
                    generalNoteMasterRepository);

            log.info("GeneralNoteMaster updated successfully....[" + exsistingGeneralNoteMaster.getId() + "]");

            baseDto.setResponseCode(ErrorCode.SUCCESS);


            baseDto.setResponseObject(exsistingGeneralNoteMaster);

        } catch (RestException exception) {

            log.error("Exception occured with error code ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (ObjectOptimisticLockingFailureException e) {

            log.error("Error in editing", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_LOCKED_RECORD);

        } catch (JpaObjectRetrievalFailureException e) {

            log.error("Error in editing", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_DELETED_RECORD);

        } catch (Exception exception) {

            log.error("Exception occured : ", exception);

            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Exception Cause 1 ::: " + exceptionCause1);


            // General error condition
            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

			/*// Duplicate GeneralNoteMaster Code
			
			if (exceptionCause1.contains(UniqueKey.UK_SERVTAXCATE_CODE)) {
				baseDto.setResponseCode(ErrorCode.SERVICETAXCATEGORY_CODE_ALREADY_EXIST);

			}

			if (exceptionCause1.contains(UniqueKey.UK_SERVTAXCATE_NAME)) {
				baseDto.setResponseCode(ErrorCode.SERVICETAXCATEGORY_NAME_ALREADY_EXIST);
			}
*/
        }

        return appUtil.setDesc(baseDto);
    }

    /**
     * Deletes the Existing GeneralNoteMaster Based on the Given Id
     *
     * @Returns BaseDto
     */
    public BaseDto delete(Long id) {

        log.info("Delete method is Invoked...[" + id + "]");

        final BaseDto baseDto = new BaseDto();

        try {

            cacheRepository.delete(SaaSUtil.getSaaSId(), GeneralNoteMaster.class.getName(), id, generalNoteMasterRepository);

            log.info("GeneralNoteMaster Deleted Successfully....[" + id + "]");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

        } catch (RestException exception) {

            log.error("Exception in Delete method : ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (DataIntegrityViolationException exception) {

            log.error("Exception occured : ", exception);

            ExceptionHelper helper = new ExceptionHelper(exception);

            log.error("Foreign key reference : " + helper.getFkReference());

            baseDto.setResponseCode(ErrorCode.ERROR_FK_CONSTRAINT);

        } catch (Exception exception) {

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            log.error("Exception in Delete method : ", exception);

            String rootCause = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Root Cause of Exception : " + rootCause);

            if (rootCause.contains("FK_")) {
                baseDto.setResponseCode(ErrorCode.CANNOT_DELETE_FK_RECORD);
            }
        }

        return appUtil.setDesc(baseDto);
    }


    /**
     * Method - getById Returns GeneralNoteMaster based on given id
     *
     * @return BaseDto
     */

    public BaseDto getById(Long id) {

        log.info("getById method is called...[" + id + "]");

        final BaseDto baseDto = new BaseDto();

        try {

            GeneralNoteMaster generalNoteMaster = cacheRepository.get(SaaSUtil.getSaaSId(), GeneralNoteMaster.class.getName(), id,
                    generalNoteMasterRepository);

            //	GeneralNoteMaster generalNoteMaster = generalNoteMasterRepository.getOne(id);


            ValidateUtil.notNull(generalNoteMaster, ErrorCode.GENERALNOTE_ID_NOT_NULL);

            log.info("getById is successfully executed....[" + generalNoteMaster.getId() + "]");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            //generalNoteMaster.getLocationMaster().setPartyMaster(null);;

            baseDto.setResponseObject(generalNoteMaster);

        } catch (final RestException exception) {

            log.error("Exception in getById method : ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (final Exception exception) {

            log.error("Exception in getId method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);
    }

    /**
     * Method - search Returns list of GeneralNoteMaster based on the search
     *
     * @return BaseDto
     */

    public BaseDto search(GeneralNoteMasterSearchDto searchDto) {

        log.info("Search method is called...." + searchDto);

        BaseDto baseDto = new BaseDto();

        try {

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(generalNoteSearchImpl.search(searchDto));

            log.info("Successfully Searching GeneralNoteMaster...");

        } catch (Exception exception) {

            log.error("Exception in Search method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);

    }


}
