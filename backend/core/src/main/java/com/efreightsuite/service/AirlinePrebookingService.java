package com.efreightsuite.service;

import com.efreightsuite.dto.AirlinePrebookingSearchRequestDto;
import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.model.AirlinePrebooking;
import com.efreightsuite.model.AirlinePrebookingSchedule;
import com.efreightsuite.repository.AirlinePrebookingRepository;
import com.efreightsuite.search.AirlinePrebookingSearchImpl;
import com.efreightsuite.util.AppUtil;
import com.efreightsuite.validation.AirlinePrebookingEntryValidator;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class AirlinePrebookingService {

    @Autowired
    private
    AppUtil appUtil;

    @Autowired
    private
    AirlinePrebookingRepository airlinePrebookingRepository;

    @Autowired
    private
    AirlinePrebookingSearchImpl airlinePrebookingSearchImpl;


    @Autowired
    private
    AirlinePrebookingEntryValidator airlinePrebookingEntryValidator;


    public BaseDto get(Long id) {

        log.info("AirlinePrebookingService -> get method starts.");

        BaseDto baseDto = new BaseDto();

        try {

            AirlinePrebooking airlinePrebooking = airlinePrebookingRepository.findById(id);

            log.info("Successfully getting AirlinePrebooking by id...");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(airlinePrebooking);

        } catch (Exception e) {

            log.error("Exception in AirlinePrebookingService -> get method ", e);

            baseDto.setResponseCode(ErrorCode.FAILED);

        }

        log.info("AirlinePrebookingService -> get method ends.");

        return appUtil.setDesc(baseDto);

    }


    public BaseDto create(AirlinePrebooking airlinePrebooking) {

        log.info("AirlinePrebookingService -> create method is start.");

        BaseDto baseDto = new BaseDto();

        try {

            airlinePrebookingEntryValidator.validate(airlinePrebooking);
            if (airlinePrebooking.getId() == null) {
                airlinePrebooking.setCompanyMaster(AuthService.getCurrentUser().getSelectedUserLocation().getCompanyMaster());
                airlinePrebooking.setCompanyCode(AuthService.getCurrentUser().getSelectedUserLocation().getCompanyMaster().getCompanyCode());
                airlinePrebooking.setLocationMaster(AuthService.getCurrentUser().getSelectedUserLocation());
                airlinePrebooking.setLocationCode(AuthService.getCurrentUser().getSelectedUserLocation().getLocationCode());
                airlinePrebooking.setCountryMaster(AuthService.getCurrentUser().getSelectedUserLocation().getCountryMaster());
                airlinePrebooking.setCountryCode(AuthService.getCurrentUser().getSelectedUserLocation().getCountryMaster().getCountryCode());
            }

            for (AirlinePrebookingSchedule schedule : airlinePrebooking.getScheduleList()) {
                schedule.setAirlinePrebooking(airlinePrebooking);
            }


            airlinePrebooking = airlinePrebookingSearchImpl.saveTransactional(airlinePrebooking);

            log.info("AirlinePrebooking Saved successfully.....[ " + airlinePrebooking.getId() + " ]");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(airlinePrebooking.getId());

        } catch (RestException re) {

            log.error("Exception in AirlinePrebookingService -> create method ", re);

            baseDto.setResponseCode(re.getMessage());

        } catch (Exception e) {

            log.error("Exception AirlinePrebookingService -> create method ", e);

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

        }

        log.info("AirlinePrebookingService -> create method is end.");

        return appUtil.setDesc(baseDto);
    }


    public BaseDto search(AirlinePrebookingSearchRequestDto requestDto) {

        log.info("AirlinePrebookingService -> search method is called....");

        BaseDto baseDto = new BaseDto();

        try {

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(airlinePrebookingSearchImpl.search(requestDto));

        } catch (Exception e) {

            log.error("Exception in Aes service -> search method ", e);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);

    }

}
