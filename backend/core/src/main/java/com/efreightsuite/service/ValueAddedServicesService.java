package com.efreightsuite.service;

import java.util.List;

import com.efreightsuite.common.component.CacheRepository;
import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.SearchRequest;
import com.efreightsuite.dto.SearchRespDto;
import com.efreightsuite.dto.ValueAddedServicesSearchDto;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.ExceptionHelper;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.exception.UniqueKey;
import com.efreightsuite.model.ValueAddedCharge;
import com.efreightsuite.model.ValueAddedServices;
import com.efreightsuite.repository.ValueAddedServicesRepository;
import com.efreightsuite.search.ValueAddedServicesSearchImpl;
import com.efreightsuite.util.AppUtil;
import com.efreightsuite.util.SaaSUtil;
import com.efreightsuite.validation.ValueAddedServicesValidator;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.exception.ExceptionUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.orm.ObjectOptimisticLockingFailureException;
import org.springframework.orm.jpa.JpaObjectRetrievalFailureException;
import org.springframework.stereotype.Service;


@Service
@Log4j2
public class ValueAddedServicesService {

    @Autowired
    private
    AppUtil appUtil;

    @Autowired
    private
    ValueAddedServicesRepository valueAddedServicesRepository;

    @Autowired
    private
    ValueAddedServicesValidator valueAddedServicesValidator;

    @Autowired
    private
    CacheRepository cacheRepository;

    @Autowired
    private
    ValueAddedServicesSearchImpl valueAddedServicesSearchImpl;

    public BaseDto getById(Long id) {

        BaseDto baseDto = new BaseDto();

        try {
            ValueAddedServices valueAddedServices = valueAddedServicesRepository.findOne(id);
            baseDto.setResponseCode(ErrorCode.SUCCESS);
            baseDto.setResponseObject(valueAddedServices);

        } catch (RestException exception) {
            log.error("Exception in getById method : ", exception);
            baseDto.setResponseCode(exception.getMessage());
        } catch (Exception exception) {
            log.error("Exception in getById method : ", exception);
            baseDto.setResponseCode(ErrorCode.FAILED);
        }
        return appUtil.setDesc(baseDto);
    }

    public BaseDto getAll() {
        log.info("getAll method is called....");
        BaseDto baseDto = new BaseDto();
        try {
            List<ValueAddedServices> tmpList = valueAddedServicesRepository.getAll();
            SearchRespDto searchRespDto = new SearchRespDto(0L, tmpList);
            baseDto.setResponseCode(ErrorCode.SUCCESS);
            baseDto.setResponseObject(searchRespDto);
            return appUtil.setDesc(baseDto);

        } catch (RestException exception) {

            log.error("Exception in getAll method " + exception);

            baseDto.setResponseCode(exception.getMessage());

            return appUtil.setDesc(baseDto);

        } catch (Exception e) {

            log.error("Exception in getAll method ", e);

            baseDto.setResponseCode(ErrorCode.FAILED);
            return appUtil.setDesc(baseDto);
        }

    }

    public BaseDto search(SearchRequest searchRequest) {

        BaseDto baseDto = new BaseDto();

        try {

            baseDto.setResponseObject(valueAddedServicesSearchImpl.search(searchRequest));
            baseDto.setResponseCode(ErrorCode.SUCCESS);
        } catch (Exception exception) {

            log.error("Exception in Search method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);

    }

    public BaseDto search(ValueAddedServicesSearchDto searchDto) {

        BaseDto baseDto = new BaseDto();

        try {

            baseDto.setResponseObject(valueAddedServicesSearchImpl.search(searchDto));
            baseDto.setResponseCode(ErrorCode.SUCCESS);
        } catch (Exception exception) {

            log.error("Exception in Search method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);

    }


    public BaseDto saveOrUpdate(ValueAddedServices valueAddedServices) {

        BaseDto baseDto = new BaseDto();

        try {

            log.info("saveOrUpdate method is Invoked....[ " + valueAddedServices.getId() + "]");

            valueAddedServicesValidator.validate(valueAddedServices);

            for (ValueAddedCharge valueAddedCharge : valueAddedServices.getValueAddedChargeList()) {

                valueAddedCharge.setValueAddedServices(valueAddedServices);

            }


            valueAddedServicesRepository.save(valueAddedServices);

            log.info("ValueAddedServices Successfully Updated...[" + valueAddedServices.getId() + "]");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(valueAddedServices);

        } catch (RestException exception) {

            log.error("Exception occured with error code ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (ObjectOptimisticLockingFailureException e) {

            log.error("Error in editing", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_LOCKED_RECORD);

        } catch (JpaObjectRetrievalFailureException e) {

            log.error("Error in editing", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_DELETED_RECORD);

        } catch (Exception exception) {

            log.error("Exception occured : ", exception);

            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Exception Cause 1 ::: ", exceptionCause1);

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            if (exceptionCause1.contains(UniqueKey.UK_VAS_VASNAME)) {
                baseDto.setResponseCode(ErrorCode.VAS_NAME_ALREADY_EXIST);
            } else if (exceptionCause1.contains(UniqueKey.UK_VAS_VASCODE)) {
                baseDto.setResponseCode(ErrorCode.VAS_CODE_ALREADY_EXIST);
            }


        }

        return appUtil.setDesc(baseDto);
    }

    public BaseDto delete(Long id) {

        BaseDto baseDto = new BaseDto();

        try {

            cacheRepository.delete(SaaSUtil.getSaaSId(), ValueAddedServices.class.getName(), id, valueAddedServicesRepository);

            baseDto.setResponseCode(ErrorCode.SUCCESS);

        } catch (RestException exception) {

            log.error("Exception in Delete method : ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (DataIntegrityViolationException exception) {

            log.error("Exception occured : ", exception);

            ExceptionHelper helper = new ExceptionHelper(exception);

            log.error("Foreign key reference : " + helper.getFkReference());

            baseDto.setResponseCode(ErrorCode.ERROR_FK_CONSTRAINT);

        } catch (Exception exception) {

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            log.error("Exception in Delete method : ", exception);

            String rootCause = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Root Cause of Exception : " + rootCause);

            if (rootCause.contains("FK_")) {
                baseDto.setResponseCode(ErrorCode.CANNOT_DELETE_FK_RECORD);
            }
        }

        return appUtil.setDesc(baseDto);
    }

}
