package com.efreightsuite.service;

import java.util.ArrayList;

import javax.servlet.http.HttpServletResponse;

import com.efreightsuite.common.component.CacheRepository;
import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.IataRateMasterSearchDto;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.ExceptionHelper;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.model.IataRateAttachment;
import com.efreightsuite.model.IataRateCharge;
import com.efreightsuite.model.IataRateMaster;
import com.efreightsuite.repository.IataRateAttachmentRepository;
import com.efreightsuite.repository.IataRateMasterRepository;
import com.efreightsuite.search.IataRateSearchImpl;
import com.efreightsuite.util.AppUtil;
import com.efreightsuite.util.SaaSUtil;
import com.efreightsuite.validation.IataRateMasterValidator;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.exception.ExceptionUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.orm.ObjectOptimisticLockingFailureException;
import org.springframework.orm.jpa.JpaObjectRetrievalFailureException;
import org.springframework.stereotype.Service;

@Log4j2
@Service
public class IataRateMasterService {

    @Autowired
    private
    AppUtil appUtil;

    @Autowired
    private
    IataRateMasterRepository iataRateMasterRepository;

    @Autowired
    private
    IataRateAttachmentRepository iataRateAttachmentRepository;

    @Autowired
    private
    IataRateSearchImpl iataRateSearchImpl;

    @Autowired
    private
    IataRateMasterValidator iataRateMasterValidator;

    @Autowired
    private
    CacheRepository cacheRepository;


    public BaseDto search(IataRateMasterSearchDto searchDto) {

        log.info("Search method is callqd...." + searchDto);

        BaseDto baseDto = new BaseDto();

        try {

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(iataRateSearchImpl.search(searchDto));

            log.info("Successfully Searching IataRateMaster...");

        } catch (Exception exception) {

            log.error("Exception in Search method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);

    }

    public BaseDto create(IataRateMaster iataRateMaster) {

        log.info("Create method is start.");
        BaseDto baseDto = new BaseDto();
        try {
            iataRateMasterValidator.validate(iataRateMaster);
            iataRateSearchImpl.checkIataRateUnique(iataRateMaster);

            if (iataRateMaster.getIataRateChargeList() != null && iataRateMaster.getIataRateChargeList().size() > 0) {

                for (IataRateCharge iataRateCharge : iataRateMaster.getIataRateChargeList()) {
                    iataRateMasterValidator.iataRateChargeValidate(iataRateCharge);
                    iataRateCharge.setIataRateMaster(iataRateMaster);
                }

            } else {
                iataRateMaster.setIataRateChargeList(new ArrayList<>());
            }

            if (iataRateMaster.getAttachmentList() != null && iataRateMaster.getAttachmentList().size() > 0) {

                for (IataRateAttachment iataRateAttachment : iataRateMaster.getAttachmentList()) {
                    iataRateMasterValidator.iataRateAttachmentValidate(iataRateAttachment);
                    iataRateAttachment.setIataRateMaster(iataRateMaster);
                }

            } else {
                iataRateMaster.setAttachmentList(new ArrayList<>());
            }

            iataRateMaster = iataRateMasterRepository.save(iataRateMaster);

            log.info("PricngAir Saved successfully.....");

            baseDto.setResponseCode(ErrorCode.SUCCESS);
            baseDto.setResponseObject(ErrorCode.SUCCESS);

        } catch (RestException re) {
            log.error("Exception in iataRateMasterService -> create method ", re);
            baseDto.setResponseCode(re.getMessage());

        } catch (Exception exception) {
            log.error("Exception in iataRateMasterService -> create method ", exception);

            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(exception);
            log.error("Exception Cause 1 ::: " + exceptionCause1);

            baseDto.setResponseCode(ErrorCode.FAILED);


            if (exceptionCause1.contains("FK_")) {
                baseDto.setResponseCode(ErrorCode.SAVE_UPDATE_FK_RECORD);
            }

        }

        log.info("Create method is end.");

        return appUtil.setDesc(baseDto);
    }

    public BaseDto update(IataRateMaster iataRateMaster) {

        log.info("IataRateMasterService -> update method is called....");

        BaseDto baseDto = new BaseDto();

        try {
            iataRateMasterValidator.validate(iataRateMaster);
            iataRateSearchImpl.checkIataRateUnique(iataRateMaster);

            if (iataRateMaster.getIataRateChargeList() != null && iataRateMaster.getIataRateChargeList().size() > 0) {

                for (IataRateCharge iataRateCharge : iataRateMaster.getIataRateChargeList()) {
                    iataRateMasterValidator.iataRateChargeValidate(iataRateCharge);
                    iataRateCharge.setIataRateMaster(iataRateMaster);
                }

            } else {
                iataRateMaster.setIataRateChargeList(new ArrayList<>());
            }

            if (iataRateMaster.getAttachmentList() != null && iataRateMaster.getAttachmentList().size() > 0) {

                for (IataRateAttachment iataRateAttachment : iataRateMaster.getAttachmentList()) {
                    iataRateMasterValidator.iataRateAttachmentValidate(iataRateAttachment);
                    iataRateAttachment.setIataRateMaster(iataRateMaster);
                }

            } else {
                iataRateMaster.setAttachmentList(new ArrayList<>());
            }

            iataRateMaster = iataRateMasterRepository.save(iataRateMaster);

            log.info("PricngAir Saved successfully.....");

            baseDto.setResponseCode(ErrorCode.SUCCESS);
            baseDto.setResponseObject(ErrorCode.SUCCESS);

        } catch (RestException re) {
            log.error("Exception in iataRateMasterService -> update method ", re);
            baseDto.setResponseCode(re.getMessage());

        } catch (ObjectOptimisticLockingFailureException e) {

            log.error("Error in editing iataRateMaster", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_LOCKED_RECORD);

        } catch (JpaObjectRetrievalFailureException e) {

            log.error("Error in editing iataRateMaster", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_DELETED_RECORD);

        } catch (Exception exception) {
            log.error("Exception in iataRateMasterService -> update method ", exception);

            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(exception);
            log.error("Exception Cause 1 ::: " + exceptionCause1);

            baseDto.setResponseCode(ErrorCode.FAILED);


            if (exceptionCause1.contains("FK_")) {
                baseDto.setResponseCode(ErrorCode.SAVE_UPDATE_FK_RECORD);
            }

        }

        return appUtil.setDesc(baseDto);

    }

    public BaseDto getById(Long id) {

        log.info("IataRateMasterService -> getById method is called....");

        BaseDto baseDto = new BaseDto();
        try {

            IataRateMaster iataRateMaster = iataRateMasterRepository.getOne(id);

            // making the file null

            if (iataRateMaster.getAttachmentList() != null && iataRateMaster.getAttachmentList().size() != 0) {
                for (IataRateAttachment attachment : iataRateMaster.getAttachmentList()) {
                    attachment.setFile(null);

                }
            }

            baseDto.setResponseCode(ErrorCode.SUCCESS);
            baseDto.setResponseObject(iataRateMaster);

        } catch (RestException re) {
            log.error("Exception in IataRateMasterService -> getById method ", re);
            baseDto.setResponseCode(re.getMessage());

        } catch (Exception e) {
            log.error("Exception in IataRateMasterService -> getById method ", e);
            baseDto.setResponseCode(ErrorCode.FAILED);

        }
        return appUtil.setDesc(baseDto);
    }

    public BaseDto delete(Long id) {

        log.info("Delete method is Invoked...[" + id + "]");

        final BaseDto baseDto = new BaseDto();

        try {

            cacheRepository.delete(SaaSUtil.getSaaSId(), IataRateMaster.class.getName(), id, iataRateMasterRepository);

            log.info("IataRateMaster Deleted Successfully....[" + id + "]");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

        } catch (RestException exception) {

            log.error("Exception in Delete method : ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (DataIntegrityViolationException exception) {

            log.error("Exception occured : ", exception);

            ExceptionHelper helper = new ExceptionHelper(exception);

            log.error("Foreign key reference : " + helper.getFkReference());

            baseDto.setResponseCode(ErrorCode.ERROR_FK_CONSTRAINT);

        } catch (Exception exception) {

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            log.error("Exception in Delete method : ", exception);

            String rootCause = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Root Cause of Exception : " + rootCause);

            if (rootCause.contains("FK_")) {
                baseDto.setResponseCode(ErrorCode.CANNOT_DELETE_FK_RECORD);
            }
        }

        return appUtil.setDesc(baseDto);
    }

    public void downloadAttachment(Long attachmentId, HttpServletResponse response) {

        IataRateAttachment attachment = iataRateAttachmentRepository.getOne(attachmentId);


        if (attachment != null && attachment.getFile() != null && attachment.getFileName() != null) {

            try {

                response.setContentType(attachment.getFileContentType());

                response.setHeader("Content-Disposition", "attachment; filename=" + attachment.getFileName());

                response.setContentLength(attachment.getFile().length);

                response.getOutputStream().write(attachment.getFile());

                response.getOutputStream().flush();

                response.getOutputStream().close();

            } catch (Exception e) {
                log.error("Error occur during download attachment file : " + attachmentId + ", ", e);
            }


        } else {
            log.info("Attachment document is not available : " + attachmentId);
        }

    }
}
