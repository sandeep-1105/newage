package com.efreightsuite.service;

import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.CharterSearchDto;
import com.efreightsuite.dto.SearchRequest;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.ExceptionHelper;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.exception.UniqueKey;
import com.efreightsuite.model.CharterMaster;
import com.efreightsuite.repository.CharterMasterRepository;
import com.efreightsuite.search.CharterSearchImpl;
import com.efreightsuite.util.AppUtil;
import com.efreightsuite.util.ValidateUtil;
import com.efreightsuite.validation.CharterMasterValidator;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.exception.ExceptionUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.orm.ObjectOptimisticLockingFailureException;
import org.springframework.orm.jpa.JpaObjectRetrievalFailureException;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class CharterMasterService {

    @Autowired
    private
    AppUtil appUtil;

    @Autowired
    private
    CharterMasterRepository charterMasterRepository;

    @Autowired
    private
    CharterMasterValidator charterMasterValidator;

    @Autowired
    private
    CharterSearchImpl charterSearchImpl;


    public BaseDto getByCharterId(Long id) {

        log.info("getByCharterId method is called...[" + id + "]");

        BaseDto baseDto = new BaseDto();

        try {

            CharterMaster charterMaster = charterMasterRepository.findById(id);

            log.info("getByCharterId is successfully executed....[" + charterMaster.getId() + "]");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(charterMaster);

        } catch (final Exception exception) {

            log.error("Exception in getByCharterId method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);
    }


    public BaseDto getByCharterCode(String charterCode) {

        log.info("getByCharterCode method is called..." + charterCode);

        final BaseDto baseDto = new BaseDto();

        try {

            CharterMaster charterMaster = charterMasterRepository.findByCharterCode(charterCode);

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(charterMaster);

        } catch (Exception exception) {

            log.error("Exception in getByCharterCode method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);
    }


    public BaseDto search(CharterSearchDto searchDto) {

        log.info("Search method is called...." + searchDto);

        BaseDto baseDto = new BaseDto();

        try {

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(charterSearchImpl.search(searchDto));

            log.info("Successfully Searching Charter...");

        } catch (Exception exception) {

            log.error("Exception in Search method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);

    }

    public BaseDto search(SearchRequest searchRequest) {

        log.info("Search method is called...." + searchRequest);

        BaseDto baseDto = new BaseDto();

        try {

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(charterSearchImpl.search(searchRequest));

            log.info("Successfully Searching Charter...");

        } catch (Exception exception) {

            log.error("Exception in Search method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);

    }


    public BaseDto create(CharterMaster charterMaster) {

        log.info("Create method is called - charterMasterId ..");

        BaseDto baseDto = new BaseDto();

        try {

            charterMasterValidator.validate(charterMaster);

            charterMaster = charterMasterRepository.save(charterMaster);

            log.info("Charter Saved successfully.....[" + charterMaster.getId() + "]");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(charterMaster);

        } catch (RestException exception) {

            log.error("Exception Cause 1 ::: ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (Exception exception) {

            log.error("Exception occured : ", exception);


            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Exception Cause 1 : " + exceptionCause1);

            // General error condition
            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            // Duplicate charter Code

            if (exceptionCause1.contains(UniqueKey.UK_CHARTER_CHARTERCODE)) {
                baseDto.setResponseCode(ErrorCode.CHARTER_CODE_ALREADY_EXIST);
            }

            if (exceptionCause1.contains(UniqueKey.UK_CHARTER_CHARTERNAME)) {
                baseDto.setResponseCode(ErrorCode.CHARTER_NAME_ALREADY_EXIST);
            }
        }

        return appUtil.setDesc(baseDto);
    }


    public BaseDto update(CharterMaster existingCharterMaster) {

        log.info("Update method is called - existingCharterMaster : [" + existingCharterMaster.getId() + "]");

        BaseDto baseDto = new BaseDto();

        try {

            ValidateUtil.notNull(existingCharterMaster.getId(), ErrorCode.CHARTER_ID_NOT_NULL);

            charterMasterValidator.validate(existingCharterMaster);

            charterMasterRepository.getOne(existingCharterMaster.getId());

            charterMasterRepository.save(existingCharterMaster);

            log.info("CharterMaster updated successfully....[" + existingCharterMaster.getId() + "]");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(existingCharterMaster);

        } catch (RestException exception) {

            log.error("Exception occured with error code ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (ObjectOptimisticLockingFailureException e) {

            log.error("Error in editing", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_LOCKED_RECORD);

        } catch (JpaObjectRetrievalFailureException e) {

            log.error("Error in editing", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_DELETED_RECORD);

        } catch (Exception exception) {

            log.error("Exception occured : ", exception);

            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Exception Cause 1 ::: " + exceptionCause1);

            // General error condition
            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            // Duplicate charter Code

            if (exceptionCause1.contains(UniqueKey.UK_CHARTER_CHARTERCODE)) {
                baseDto.setResponseCode(ErrorCode.CHARTER_CODE_ALREADY_EXIST);
            }

            if (exceptionCause1.contains(UniqueKey.UK_CHARTER_CHARTERNAME)) {
                baseDto.setResponseCode(ErrorCode.CHARTER_NAME_ALREADY_EXIST);
            }

        }

        return appUtil.setDesc(baseDto);
    }

    public BaseDto delete(Long id) {

        log.info("Delete method is Invoked...[" + id + "]");

        final BaseDto baseDto = new BaseDto();

        try {

            charterMasterRepository.delete(id);

            log.info("CharterMaster Deleted Successfully....[" + id + "]");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

        } catch (DataIntegrityViolationException exception) {

            log.error("Exception occured : ", exception);

            ExceptionHelper helper = new ExceptionHelper(exception);

            log.error("Foreign key reference : " + helper.getFkReference());

            baseDto.setResponseCode(ErrorCode.ERROR_FK_CONSTRAINT);

        } catch (Exception exception) {

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            log.error("Exception in Delete method : ", exception);

            String rootCause = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Root Cause of Exception : " + rootCause);

            if (rootCause.contains("FK_")) {
                baseDto.setResponseCode(ErrorCode.CANNOT_DELETE_FK_RECORD);
            }
        }

        return appUtil.setDesc(baseDto);
    }

}
