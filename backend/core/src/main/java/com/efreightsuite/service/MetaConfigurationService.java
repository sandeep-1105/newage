package com.efreightsuite.service;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import com.efreightsuite.configuration.SessionRoutingDataSource;
import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.FileUploadDto;
import com.efreightsuite.enumeration.AddressType;
import com.efreightsuite.enumeration.ChargeCalculationType;
import com.efreightsuite.enumeration.ChargeType;
import com.efreightsuite.enumeration.CorporateNonCorporate;
import com.efreightsuite.enumeration.EmploymentStatus;
import com.efreightsuite.enumeration.LovStatus;
import com.efreightsuite.enumeration.YesNo;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.model.AgentPortMaster;
import com.efreightsuite.model.ChargeMaster;
import com.efreightsuite.model.CityMaster;
import com.efreightsuite.model.CountryMaster;
import com.efreightsuite.model.CurrencyMaster;
import com.efreightsuite.model.EmployeeMaster;
import com.efreightsuite.model.GeneralLedgerAccount;
import com.efreightsuite.model.LocationMaster;
import com.efreightsuite.model.PartyAddressMaster;
import com.efreightsuite.model.PartyAssociateToPartyTypeMaster;
import com.efreightsuite.model.PartyMaster;
import com.efreightsuite.model.PartyMasterDetail;
import com.efreightsuite.model.PartyServiceMaster;
import com.efreightsuite.model.PartyTypeMaster;
import com.efreightsuite.model.PortMaster;
import com.efreightsuite.model.ServiceMaster;
import com.efreightsuite.model.StateMaster;
import com.efreightsuite.repository.CityMasterRepository;
import com.efreightsuite.repository.CountryMasterRepository;
import com.efreightsuite.repository.CurrencyMasterRepository;
import com.efreightsuite.repository.DefaultMasterDataRepository;
import com.efreightsuite.repository.EmployeeMasterRepository;
import com.efreightsuite.repository.GeneralLedgerRepository;
import com.efreightsuite.repository.LocationMasterRepository;
import com.efreightsuite.repository.PartyAssociateToPartyTypeMasterRepository;
import com.efreightsuite.repository.PartyMasterRepository;
import com.efreightsuite.repository.PartyTypeMasterRepository;
import com.efreightsuite.repository.PortMasterRepository;
import com.efreightsuite.repository.ServiceMasterRepository;
import com.efreightsuite.repository.StateMasterRepository;
import com.efreightsuite.repository.UserProfileRepository;
import com.efreightsuite.util.AppUtil;
import com.efreightsuite.validation.PartyAddressMasterValidator;
import com.efreightsuite.validation.PartyMasterDetailValidator;
import lombok.extern.log4j.Log4j2;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class MetaConfigurationService {

    @Autowired
    private
    CountryMasterRepository countryMasterRepository;

    @Autowired
    private
    CityMasterRepository cityMasterRepository;

    @Autowired
    private
    StateMasterRepository stateMasterRepository;

    @Autowired
    LocationMasterRepository locationMasterRepository;

    @Autowired
    PartyAssociateToPartyTypeMasterRepository partyAssociateToPartyTypeMasterRepository;

    @Autowired
    private
    PartyTypeMasterRepository partyTypeMasterRepository;

    @Autowired
    SequenceGeneratorService sequenceGeneratorService;

    @Autowired
    DefaultMasterDataRepository defaultMasterDataRepository;

    @Autowired
    RegularExpressionService regExpService;

    @Autowired
    private
    PartyMasterRepository partyMasterRepository;

    @Autowired
    EmployeeMasterRepository employeeMasterRepository;

    @Autowired
    private
    ServiceMasterRepository serviceMasterRepository;


    @Autowired
    CurrencyMasterRepository currencyMasterRepository;

    @Autowired
    private
    PartyAddressMasterValidator partyAddressMasterValidator;

    @Autowired
    private
    PartyMasterDetailValidator partyMasterDetailValidator;

    @Autowired
    SessionRoutingDataSource sessionRoutingDataSource;

    @Autowired
    private
    PortMasterRepository portMasterRepository;

    @Autowired
    UserProfileRepository userProfileRepository;

    @Autowired
    GeneralLedgerRepository generalLedgerRepository;

    @Autowired
    private
    AppUtil appUtil;

    private final Map<String, CountryMaster> countryMap = new HashMap<>();
    private final Map<String, CityMaster> cityMap = new HashMap<>();
    private final Map<String, StateMaster> stateMap = new HashMap<>();
    Map<String, LocationMaster> locMap = new HashMap<>();
    Map<String, CurrencyMaster> currencyMap = new HashMap<>();
    Map<String, GeneralLedgerAccount> accountMasterMap = new HashMap<>();
    private final Map<Integer, String> partyTypeMap = new HashMap<>();
    private final Map<String, PortMaster> portMap = new HashMap<>();
    private final Map<String, PartyMaster> agentMap = new HashMap<>();
    private final Map<String, ServiceMaster> serviceMap = new HashMap<>();


    // Column index for agent or customer master template
    private final int cName = 0;
    private final int cCountryCode = 1;
    private final int cAddress1 = 2;
    private final int cAddress2 = 3;
    private final int cAddress3 = 4;
    private final int cCity = 5;
    private final int cState = 6;
    private final int cZipCode = 7;
    private final int cPoBox = 8;
    private final int cPhoneNo = 9;
    private final int cFaxNo = 10;
    private final int cContactPerson = 11;
    private final int cMobileNo = 12;
    private final int cEmail = 13;
    private final int cIsCustomer = 14;
    int cIsShipper = 15;
    int cIsConsignee = 16;
    int cIsForwarder = 17;
    int cIsNotify = 18;
    private final int cIsLocalAgent = 19;
    int cIsTrader = 20;
    int cIsBank = 21;
    private final int cIsOther = 22;
    int cBaseCurrencyCode = 23;
    private final int cPanNo = 24;
    private final int cTdsPercentage = 25;
    private final int cValueAddedTaxNo = 26;
    private final int cGstNo = 27;
    private final int cCcPercentage = 28;
    private final int cCafPercentage = 29;
    private final int cExporterCode = 30;
    private final int cImporterCode = 31;

    private final int partyMastertotalColumn = 32;

    //Column index for agent port master template
    private final int cServiceCode = 0;
    private final int cAgentCode = 1;
    private final int cPortCode = 2;
    private final int agentPortMastertotalColumn = 3;

    //Column index for charge master template
    private final int cChargeCode = 0;
    private final int cChargeName = 1;
    private final int cChargeType = 2;
    private final int cChargeCaluculationType = 3;
    private final int chargeMasterTotalColumn = 4;


    public void downloadTemplate(HttpServletResponse response, String type) {

        // Blank workbook
        try (XSSFWorkbook workbook = new XSSFWorkbook()) {
            XSSFSheet sheet = workbook.createSheet(type + " DATA");

            // This data needs to be written (Object[])
            Map<String, Object[]> data = new TreeMap<>();

            // Create a empty row
            XSSFRow emptyRow = null;

            type = type.toUpperCase();

            if (type.equals("CUSTOMER") || type.equals("AGENT")) {
                data.put("1",
                        new Object[]{"Name", "Country Code", "Address Line 1", "Address Line 2", "Address Line 3",
                                "City Name", "State Name", "Zip Code", "Po Box", "Phone No", "Fax No",
                                "Contact Person", "Mobile No", "eMail",
                                "Customer", "Shipper", "Consignee", "Forwarder", "Notify Customer", "Local Agent",
                                "Trader", "Bank", "Other", "Base Currency Code", "PAN No",
                                "TDS Percentage", "Value Added Tax No", "GST No",
                                "CC Percentage", "CAF Percentage", "Exporter Code", "Importer Code"});

                emptyRow = sheet.createRow(1);
                for (int column = 0; column < partyMastertotalColumn; column++) {
                    sheet.getRow(1).createCell(column);
                }
            } else if (type.equals("AGENT PORT")) {
                data.put("1", new Object[]{"Service Code", "Agent Code", "Port Code"});
            }

            // Iterate over data and write to sheet
            Set<String> keyset = data.keySet();
            int rownum = 0;
            for (String key : keyset) {
                XSSFRow row = sheet.createRow(rownum++);
                Object[] objArr = data.get(key);
                int cellnum = 0;
                for (Object obj : objArr) {
                    Cell cell = row.createCell(cellnum++);
                    if (obj instanceof String) {
                        cell.setCellValue((String) obj);
                    } else if (obj instanceof Integer) {
                        cell.setCellValue((Integer) obj);
                    }
                }
            }

            //Update the default data for customer and agent
            if (type.equals("CUSTOMER")) {
                updateColumnData(emptyRow, 14, "Y");
            } else if (type.equals("AGENT")) {
                updateColumnData(emptyRow, 19, "Y");
            }

            //Resize the column width
            autoSizeColumns(workbook);

            response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
            response.setHeader("Content-Disposition", "attachment;");
            ServletOutputStream servletOutputStream = response.getOutputStream();
            workbook.write(servletOutputStream);
            servletOutputStream.flush();
            servletOutputStream.close();
            log.info("template downloaded successfully on disk.");
        } catch (IOException e) {
            log.error("Error occured during download template ", e);
        }

        // Create a blank sheet

    }

    public List<PartyMaster> getPartyList(BaseDto baseDto, byte[] file, LocationMaster locationMaster, EmployeeMaster employeeMaster, String uploadType, String fileName) {

        List<PartyMaster> partyMasterList = new ArrayList<>();

        XSSFWorkbook workBook = null;
        if (fileName.contains(".xlsx") || fileName.contains(".xls")) {
            List<List<String>> records = workBookToRecord(file, partyMastertotalColumn, workBook);
            for (List<String> record : records) {
                int row = 0;
                partyMasterList.add(formPartyMaster(baseDto, record, row, locationMaster, employeeMaster, uploadType));
                row++;
            }
        } else if (fileName.contains(".csv")) {

            XSSFWorkbook workBookCsv = appUtil.csvToXLSX(file);

            List<List<String>> records = workBookToRecord(file, partyMastertotalColumn, workBookCsv);

            if (records != null && records.size() > 1) {
                for (List<String> record : records.subList(1, records.size())) {
                    int row = 0;
                    partyMasterList.add(formPartyMaster(baseDto, record, row, locationMaster, employeeMaster, uploadType));
                    row++;
                }

            } else {
                throw new RestException(ErrorCode.META_CONF_XLSX_FILE_EMPTY);
            }
        }
        return partyMasterList;

    }


    private PartyMaster formPartyMaster(BaseDto baseDto, List<String> record, int row, LocationMaster locationMaster, EmployeeMaster employeeMaster, String uploadType) {
        PartyMaster partyMaster = new PartyMaster();

        partyMaster.setPartyName(getPartyName(record, row, cName, baseDto, true));

        log.info("Party name validated : " + partyMaster.getPartyName());

        partyMaster.setCountryMaster(getCountry(record, row, cCountryCode, baseDto, true));

        log.info("Country master validated : " + partyMaster.getCountryMaster().getCountryCode());

        partyMaster.setCountryCode(partyMaster.getCountryMaster().getCountryCode());

        partyMaster.setPartyTypeList(getPartyAssociateToPartyTypeMaster(record, row, baseDto, partyMaster, uploadType));

        log.info("Party type list validated");

        partyMaster.setPartyAddressList(getPartyAdressList(record, row, baseDto, true, partyMaster));

        log.info("Party address list validated");

        partyMaster.setPartyDetail(getPartyMasterDetail(record, row, baseDto));

        log.info("Party detail validated");

        partyMaster.setPartyServiceList(getPartyServiceMaster(record, row, baseDto, true, partyMaster, locationMaster, employeeMaster));

        log.info("Party service list validated");

        //partyMaster.setPartyAccountList(getPartyAccountMaster(record, row, baseDto, true,partyMaster,locationMaster,employeeMaster));

        log.info("Party account list validated");

        partyMaster.setStatus(LovStatus.Active);


        return partyMaster;
    }

    private CountryMaster getCountry(List<String> record, int row, int col, BaseDto baseDto, boolean isMandatory) {

        String value = record.get(col);
        if (value == null || value.trim().length() == 0) {
            if (!isMandatory) {
                return null;
            }
            throwError(baseDto, row, col, null, ErrorCode.META_CONF_DATA_IS_MANDATORY);
        } else {
            value = value.trim().toUpperCase();
        }

        if (countryMap.get(value) == null) {
            CountryMaster object = countryMasterRepository.findByCountryCode(value);
            if (object == null) {
                throwError(baseDto, row, col, value, ErrorCode.META_CONF_DATA_IS_NOT_FOUND);
            } else {
                if (object.getStatus() == LovStatus.Block) {
                    throwError(baseDto, row, col, value, ErrorCode.META_CONF_DATA_BLOCKED);
                } else if (object.getStatus() == LovStatus.Hide) {
                    throwError(baseDto, row, col, value, ErrorCode.META_CONF_DATA_HIDDEN);
                } else {
                    countryMap.put(value, object);
                }
            }
        }
        return countryMap.get(value);
    }

    private String getPartyName(List<String> record, int row, int col, BaseDto baseDto, boolean isMandatory) {

        String value = record.get(col);
        if (value == null || value.trim().length() == 0 || value.equals("0")) {
            if (!isMandatory) {
                return null;
            }
            throwError(baseDto, row, col, null, ErrorCode.META_CONF_DATA_IS_MANDATORY);
        } else {
            if (value.length() > 100) {
                throwError(baseDto, row, col, value, ErrorCode.META_CONF_FILE_INVALID);
            }
        }
        return value;
    }


    private CityMaster getCity(List<String> record, int row, int col, BaseDto baseDto, boolean isMandatory, Long CountryId, Long stateId) {

        String value = record.get(col);
        if (value == null || value.trim().length() == 0) {
            if (!isMandatory) {
                return null;
            }
            throwError(baseDto, row, col, "", ErrorCode.META_CONF_DATA_IS_MANDATORY);
        } else {
            value = value.trim().toUpperCase();
        }

        if (cityMap.get(value) == null) {
            CityMaster object = cityMasterRepository.findByCityName(value);
            if (object == null) {
                throwError(baseDto, row, col, value, ErrorCode.META_CONF_DATA_IS_NOT_FOUND);
            } else {
                if (object.getStatus() == LovStatus.Block) {
                    throwError(baseDto, row, col, value, ErrorCode.META_CONF_DATA_BLOCKED);
                } else if (object.getStatus() == LovStatus.Hide) {
                    throwError(baseDto, row, col, value, ErrorCode.META_CONF_DATA_HIDDEN);
                } else {
                    cityMap.put(value, object);
                }
            }
        }
        if (CountryId != null) {
            if (cityMap.get(value).getCountryMaster() != null && !cityMap.get(value).getCountryMaster().getId().equals(CountryId)) {
                throwError(baseDto, row, col, value, ErrorCode.META_CONF_CITY_IS_NOT_IN_STATE_COUNTRY);
            }
        }

        if (stateId != null) {
            if (cityMap.get(value).getStateMaster() != null && !cityMap.get(value).getStateMaster().getId().equals(stateId)) {
                throwError(baseDto, row, col, value, ErrorCode.META_CONF_CITY_IS_NOT_IN_STATE_COUNTRY);
            }
        }

        return cityMap.get(value);
    }

    private StateMaster getState(List<String> record, int row, int col, BaseDto baseDto, boolean isMandatory, Long countryId) {

        String value = record.get(col);
        if (value == null || value.trim().length() == 0) {
            if (!isMandatory) {
                return null;
            }
            throwError(baseDto, row, col, "", ErrorCode.META_CONF_DATA_IS_MANDATORY);
        } else {
            value = value.trim().toUpperCase();
        }
        if (stateMap.get(value) == null) {
            StateMaster object = stateMasterRepository.findByStateName(value);
            if (object == null) {
                throwError(baseDto, row, col, value, ErrorCode.META_CONF_DATA_IS_NOT_FOUND);
            } else {
                if (object.getStatus() == LovStatus.Block) {
                    throwError(baseDto, row, col, value, ErrorCode.META_CONF_DATA_BLOCKED);
                } else if (object.getStatus() == LovStatus.Hide) {
                    throwError(baseDto, row, col, value, ErrorCode.META_CONF_DATA_HIDDEN);
                } else {
                    stateMap.put(value, object);
                }
            }
        }

        if (stateMap.get(value) != null && countryId != null && stateMap.get(value).getCountryMaster() != null && !stateMap.get(value).getCountryMaster().getId().equals(countryId)) {
            throwError(baseDto, row, cState, value, ErrorCode.META_CONF_STATE_IS_NOT_IN_COUNTRY);
        }
        return stateMap.get(value);
    }


    private List<PartyAssociateToPartyTypeMaster> getPartyAssociateToPartyTypeMaster(List<String> record, int row,
                                                                                     BaseDto baseDto, PartyMaster partyMaster, String uploadType) {
        int count = 0;
        List<PartyAssociateToPartyTypeMaster> list = new ArrayList<>();
        for (int i = cIsCustomer; i <= cIsOther; i++) {
            if (record.get(i) != null && record.get(i).trim().length() != 0) {
                if (record.get(i).equalsIgnoreCase("y")) {
                    PartyAssociateToPartyTypeMaster partyAssociateToPartyTypeMaster = new PartyAssociateToPartyTypeMaster();
                    if (partyTypeMap.get(i) != "" || partyTypeMap.get(i) != null) {
                        PartyTypeMaster partyTypeMaster = partyTypeMasterRepository.findByPartyTypeName(partyTypeMap.get(i).trim().toUpperCase());
                        if (partyTypeMaster != null) {
                            if (partyTypeMaster.getStatus() == LovStatus.Block) {
                                throwError(baseDto, row, i, record.get(i), ErrorCode.META_CONF_DATA_BLOCKED);
                            } else if (partyTypeMaster.getStatus() == LovStatus.Hide) {
                                throwError(baseDto, row, i, record.get(i), ErrorCode.META_CONF_DATA_HIDDEN);
                            }
                            partyAssociateToPartyTypeMaster.setPartyTypeMaster(partyTypeMaster);

                            partyAssociateToPartyTypeMaster.setPartyMaster(partyMaster);
                            list.add(partyAssociateToPartyTypeMaster);
                            count++;
                        }
                    }
                } else {
                    throwError(baseDto, row, i, record.get(i), ErrorCode.META_CONF_CUSTOMER_TYPE_FORMAT);
                }
            }
        }
        if (count == 0) {
            PartyAssociateToPartyTypeMaster partyAssociateToPartyTypeMaster = new PartyAssociateToPartyTypeMaster();
            PartyTypeMaster partyTypeMaster = null;
            if (uploadType.equals("agent")) {
                partyTypeMaster = partyTypeMasterRepository.findByPartyTypeName(partyTypeMap.get(cIsLocalAgent).trim().toUpperCase());
            } else if (uploadType.equals("customer")) {
                partyTypeMaster = partyTypeMasterRepository.findByPartyTypeName(partyTypeMap.get(cIsCustomer).trim().toUpperCase());
            }

            if (partyTypeMaster != null) {
                if (partyTypeMaster.getStatus() == LovStatus.Block) {
                    throwError(baseDto, row, cIsCustomer, record.get(cIsCustomer), ErrorCode.META_CONF_DATA_BLOCKED);
                } else if (partyTypeMaster.getStatus() == LovStatus.Hide) {
                    throwError(baseDto, row, cIsCustomer, record.get(cIsCustomer), ErrorCode.META_CONF_DATA_HIDDEN);
                }
                partyAssociateToPartyTypeMaster.setPartyTypeMaster(partyTypeMaster);

                partyAssociateToPartyTypeMaster.setPartyMaster(partyMaster);
                list.add(partyAssociateToPartyTypeMaster);
            }

			/*baseDto.getParams().add(null);
            baseDto.getParams().add((row + 1) + "");
			baseDto.getParams().add(cIsCustomer+" to "+cIsOther);
			throw new RestException(ErrorCode.META_CONF_FILE_INVALID);*/
        }
        return list;
    }

    private List<PartyAddressMaster> getPartyAdressList(List<String> record, int row, BaseDto baseDto, boolean isMandatory, PartyMaster partyMaster) {

        PartyAddressMaster partyAddressMaster = new PartyAddressMaster();

        if (record.get(cAddress1) == null || record.get(cAddress1).equals("") || record.get(cAddress1).length() == 0) {
            throwError(baseDto, row, cAddress1, "", ErrorCode.META_CONF_DATA_IS_MANDATORY);
        } else {
            partyAddressMaster.setAddressLine1(record.get(cAddress1));
            try {
                partyAddressMasterValidator.verifyAddressLine1(partyAddressMaster);
            } catch (RestException e) {
                throwError(baseDto, row, cAddress1, record.get(cAddress1), ErrorCode.META_CONF_FILE_INVALID);
            }
        }

        try {
            partyAddressMaster.setAddressLine2(record.get(cAddress2));
            partyAddressMasterValidator.verifyAddressLine2(partyAddressMaster);
        } catch (RestException e) {
            throwError(baseDto, row, cAddress2, record.get(cAddress2), ErrorCode.META_CONF_FILE_INVALID);
        }

        try {
            partyAddressMaster.setAddressLine3(record.get(cAddress3));
            partyAddressMasterValidator.verifyAddressLine3(partyAddressMaster);
        } catch (RestException e) {
            throwError(baseDto, row, cAddress3, record.get(cAddress3), ErrorCode.META_CONF_FILE_INVALID);
        }

        partyAddressMaster.setAddressType(AddressType.Primary);
        partyAddressMaster.setCorporate(YesNo.Yes);
        partyAddressMaster.setPartyMaster(partyMaster);

        if (partyMaster.getCountryMaster() != null) {
            partyAddressMaster.setStateMaster(getState(record, row, cState, baseDto, false, partyMaster.getCountryMaster().getId()));
        }
        if (partyMaster.getCountryMaster() != null && partyAddressMaster.getStateMaster() != null) {
            partyAddressMaster.setCityMaster(getCity(record, row, cCity, baseDto, false, partyMaster.getCountryMaster().getId(), partyAddressMaster.getStateMaster().getId()));
        }
        partyAddressMaster.setZipCode(record.get(cZipCode));

        try {
            partyAddressMasterValidator.verifyZipCode(partyAddressMaster);
        } catch (RestException e) {
            throwError(baseDto, row, cZipCode, record.get(cZipCode), ErrorCode.META_CONF_FILE_INVALID);
        }
        partyAddressMaster.setPoBox(record.get(cPoBox));

        try {
            partyAddressMasterValidator.verifyPoBoxNumber(partyAddressMaster);
        } catch (RestException e) {
            throwError(baseDto, row, cPoBox, record.get(cPoBox), ErrorCode.META_CONF_FILE_INVALID);
        }
        partyAddressMaster.setPhone(record.get(cPhoneNo));

        try {
            partyAddressMasterValidator.verifyPhoneNumber(partyAddressMaster);
        } catch (RestException e) {
            throwError(baseDto, row, cPhoneNo, record.get(cPhoneNo), ErrorCode.META_CONF_FILE_INVALID);
        }
        partyAddressMaster.setFax(record.get(cFaxNo));

        try {
            partyAddressMasterValidator.verifyFax(partyAddressMaster);
        } catch (RestException e) {
            throwError(baseDto, row, cFaxNo, record.get(cFaxNo), ErrorCode.META_CONF_FILE_INVALID);
        }
        partyAddressMaster.setContactPerson(record.get(cContactPerson));

        try {
            partyAddressMasterValidator.verifyContactPerson(partyAddressMaster);
        } catch (RestException e) {
            throwError(baseDto, row, cContactPerson, record.get(cContactPerson), ErrorCode.META_CONF_FILE_INVALID);
        }
        partyAddressMaster.setMobileNo(record.get(cMobileNo));

        try {
            partyAddressMasterValidator.verifyMobileNumber(partyAddressMaster);
        } catch (RestException e) {
            throwError(baseDto, row, cMobileNo, record.get(cMobileNo), ErrorCode.META_CONF_FILE_INVALID);
        }
        if (record.get(cEmail) != null && record.get(cEmail).trim() != "") {
            partyAddressMaster.setEmail(record.get(cEmail));

            if (!isValidEmailId(record.get(cEmail))) {
                throwError(baseDto, row, cEmail, record.get(cEmail), ErrorCode.META_CONF_FILE_INVALID);
            }
        }

        List<PartyAddressMaster> list = new ArrayList<>();
        list.add(partyAddressMaster);
        return list;

    }

    private PartyMasterDetail getPartyMasterDetail(List<String> record, int row, BaseDto baseDto) {
        int count = 0;
        for (int i = cPanNo; i <= cCafPercentage; i++) {
            if (record.get(i) != null && !record.get(i).equals("")) {
                count++;
            }
        }

        PartyMasterDetail partyMasterDetail = new PartyMasterDetail();
        if (count != 0) {

            partyMasterDetail.setPanNo(record.get(cPanNo));
            partyMasterDetail.setValueAddedTaxNo(record.get(cValueAddedTaxNo));
            partyMasterDetail.setGstNo(record.get(cGstNo));

            try {
                partyMasterDetailValidator.verifyPanNumber(partyMasterDetail);
            } catch (RestException e) {
                throwError(baseDto, row, cPanNo, record.get(cPanNo), ErrorCode.META_CONF_FILE_INVALID);
            }

            try {
                partyMasterDetailValidator.verifyVatNumber(partyMasterDetail);
            } catch (RestException e) {
                throwError(baseDto, row, cValueAddedTaxNo, record.get(cValueAddedTaxNo), ErrorCode.META_CONF_FILE_INVALID);
            }

            try {
                partyMasterDetailValidator.verifyGstNumber(partyMasterDetail);
            } catch (RestException e) {
                throwError(baseDto, row, cGstNo, record.get(cGstNo), ErrorCode.META_CONF_FILE_INVALID);
            }


            if (record.get(cTdsPercentage) != null && !record.get(cTdsPercentage).equals("") && record.get(cTdsPercentage).length() != 0) {
                partyMasterDetail.setTdsPercentage(cTdsPercentage);
                try {
                    partyMasterDetailValidator.verifyTDSPercentage(partyMasterDetail);
                } catch (RestException e) {
                    throwError(baseDto, row, cTdsPercentage, record.get(cTdsPercentage), ErrorCode.META_CONF_FILE_INVALID);
                }
            }


            if (record.get(cCcPercentage) != null && !record.get(cCcPercentage).equals("") && record.get(cCcPercentage).length() != 0) {
                partyMasterDetail.setCcPercentage(record.get(cCcPercentage) != null ? record.get(cCcPercentage).equals("") ? null : new Double(record.get(cCcPercentage)) : null);
                try {
                    partyMasterDetailValidator.verifyCCPercentage(partyMasterDetail);
                } catch (RestException e) {
                    throwError(baseDto, row, cCcPercentage, record.get(cCcPercentage), ErrorCode.META_CONF_FILE_INVALID);
                }
            }

            if (record.get(cCafPercentage) != null && !record.get(cCafPercentage).equals("") && record.get(cCafPercentage).length() != 0) {
                partyMasterDetail.setCafPercentage(record.get(cCafPercentage) != null ? record.get(cCafPercentage).equals("") ? null : new Double(record.get(cCafPercentage)) : null);
                try {
                    partyMasterDetailValidator.verifyCafPercentage(partyMasterDetail);
                } catch (RestException e) {
                    throwError(baseDto, row, cCafPercentage, record.get(cCafPercentage), ErrorCode.META_CONF_FILE_INVALID);
                }
            }

            partyMasterDetail.setCorporate(CorporateNonCorporate.CORPORATE);
        } else {
            partyMasterDetail = null;
        }

        return partyMasterDetail;
    }


    private List<PartyServiceMaster> getPartyServiceMaster(List<String> record, int row, BaseDto baseDto, boolean isMandatory, PartyMaster partyMaster, LocationMaster locationMaster, EmployeeMaster employeeMaster) {
        List<PartyServiceMaster> partyServiceMasterList = new ArrayList<>();
        if ((record.get(cImporterCode).equals("") || record.get(cImporterCode).trim().length() == 0) && (record.get(cExporterCode).equals("") || record.get(cExporterCode).trim().length() == 0)) {
            return null;
        } else {
            PartyServiceMaster partyServiceMaster = new PartyServiceMaster();
            ServiceMaster serviceMaster = new ServiceMaster();
            serviceMaster = serviceMasterRepository.findByServiceCode(record.get(cImporterCode).equals("") ? record.get(cExporterCode) : record.get(cImporterCode));
            if (serviceMaster.getStatus() == LovStatus.Hide || serviceMaster.getStatus() == LovStatus.Block || employeeMaster.getEmployementStatus() == EmploymentStatus.RESIGNED || employeeMaster.getEmployementStatus() == EmploymentStatus.TERMINATED) {
                return null;
            } else {
                partyServiceMaster.setLocationMaster(locationMaster);
                partyServiceMaster.setServiceMaster(serviceMaster);
                partyServiceMaster.setSalesman(employeeMaster);
                partyServiceMaster.setPartyMaster(partyMaster);
                partyServiceMasterList.add(partyServiceMaster);
            }
        }
        return partyServiceMasterList;
    }

    /*private List<PartyAccountMaster> getPartyAccountMaster(List<String> record, int row, BaseDto baseDto, boolean isMandatory,PartyMaster partyMaster,LocationMaster locationMaster,EmployeeMaster employeeMaster){

        List<PartyAccountMaster> partyAccountMasterList= new ArrayList<PartyAccountMaster>();
        if(record.get(cBaseCurrencyCode).equals("") || record.get(cBaseCurrencyCode).trim().length()==0){
            return null;
        }else{
            int count=0;
            for(int i=cAccount1;i<cAccount4;i++){
                if(record.get(i)!=null && !record.get(i).equals("")){
                    count++;
                    PartyAccountMaster partyAccountMaster = new PartyAccountMaster();
                    partyAccountMaster.setLocationMaster(locationMaster);
                    partyAccountMaster.setAccountMaster(getAccountMaster(record, row, i, baseDto, false));
                    partyAccountMaster.setAccountCode(accountMasterMap.get(record.get(i).toUpperCase()).getGlAccountCode());

                    if(!record.get(cTermCode).equals("")){
                        if(record.get(cTermCode).equalsIgnoreCase("CASH")){
                            partyAccountMaster.setTermCode(TermCode.CASH);
                        }else if(record.get(cTermCode).equalsIgnoreCase("CREDIT")){
                            partyAccountMaster.setTermCode(TermCode.CREDIT);
                        }else if(record.get(cTermCode).equalsIgnoreCase("POSTDATEDCHEQUE")){
                            partyAccountMaster.setTermCode(TermCode.POSTDATEDCHEQUE);
                        }
                    }
                    partyAccountMaster.setCurrencyMaster(getCurrency(record, row, cBaseCurrencyCode, baseDto, false));
                    partyAccountMaster.setStatus(LovStatus.Active);
                    partyAccountMaster.setPartyMaster(partyMaster);
                    partyAccountMasterList.add(partyAccountMaster);
                }
            }
            if(count==0){
                return null;
            }
        }
            return partyAccountMasterList;
    }
*/
    private List<List<String>> workBookToRecord(byte[] file, int totalColumn, XSSFWorkbook workBook) {
        List<List<String>> rowRecords = new ArrayList<>();

        try {

            //Create an object of FileInputStream class to read excel file

            ByteArrayInputStream bis = new ByteArrayInputStream(file);

            if (workBook == null) {
                workBook = new XSSFWorkbook(bis);
            }

            XSSFSheet sheet = workBook.getSheetAt(0);

            for (int i = 1; i <= sheet.getLastRowNum(); i++) {
                rowRecords.add(rowToRecord(sheet.getRow(i), totalColumn));
            }

            workBook.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return rowRecords;

    }

    private void throwError(BaseDto baseDto, int row, int col, String value, String errorCode) {
        baseDto.getParams().add(value);
        baseDto.getParams().add((row + 1) + "");
        baseDto.getParams().add((col + 1) + "");
        throw new RestException(errorCode);
    }

    private List<String> rowToRecord(XSSFRow row, int totalColumn) {
        List<String> record = new ArrayList<>();
        for (int i = 0; i < totalColumn; i++) {
            if (row != null && row.getCell(i) != null) {
                if (row.getCell(i).getCellType() == Cell.CELL_TYPE_STRING) {
                    record.add(row.getCell(i).getStringCellValue());
                } else {
                    Double numeric = row.getCell(i).getNumericCellValue();
                    String str = Double.toString(numeric);
                    record.add(new BigDecimal(str).stripTrailingZeros().toPlainString());

                }

            } else {
                record.add("");
            }
        }
        return record;
    }

    private void updateColumnData(XSSFRow emptyRow, int cellNo, String value) {
        Cell column = emptyRow.getCell(cellNo);
        column.setCellValue(value);
    }

    private void autoSizeColumns(XSSFWorkbook workbook) {
        int numberOfSheets = workbook.getNumberOfSheets();
        for (int i = 0; i < numberOfSheets; i++) {
            XSSFSheet sheet = workbook.getSheetAt(i);
            if (sheet.getPhysicalNumberOfRows() > 0) {
                XSSFRow row = sheet.getRow(0);
                Iterator<Cell> cellIterator = row.cellIterator();
                while (cellIterator.hasNext()) {
                    Cell cell = cellIterator.next();
                    int columnIndex = cell.getColumnIndex();
                    sheet.autoSizeColumn(columnIndex);
                }
            }
        }
    }

    private boolean isValidEmailId(String email) {
        String emailPattern = "^[\\w!#$%&’*+/=?`{|}~^-]+(?:\\.[\\w!#$%&’*+/=?`{|}~^-]+)*@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$";
        Pattern p = Pattern.compile(emailPattern);
        Matcher m = p.matcher(email);
        return m.matches();
    }


    public List<AgentPortMaster> getAgentPortList(BaseDto baseDto, byte[] file) {
        List<AgentPortMaster> agentPortMasterList = new ArrayList<>();

        XSSFWorkbook workBook = null;

        List<List<String>> records = workBookToRecord(file, agentPortMastertotalColumn, workBook);
        for (List<String> record : records) {
            int row = 0;
            agentPortMasterList.add(formAgentPortMaster(baseDto, record, row));
            row++;
        }


        return agentPortMasterList;
    }


    private AgentPortMaster formAgentPortMaster(BaseDto baseDto, List<String> record, int row) {
        AgentPortMaster agentPortMaster = new AgentPortMaster();

        agentPortMaster.setAgent(getAgent(record, row, cAgentCode, baseDto, true));

        agentPortMaster.setPort(getPort(record, row, cPortCode, baseDto, true));

        agentPortMaster.setServiceMaster(getService(record, row, cServiceCode, baseDto, true));

        agentPortMaster.setServiceCode(agentPortMaster.getServiceMaster().getServiceCode());

        agentPortMaster.setLocationMaster(AuthService.getCurrentUser().getSelectedUserLocation());

        agentPortMaster.setCountryMaster(agentPortMaster.getPort().getPortGroupMaster().getCountryMaster());

        return agentPortMaster;
    }

    private PortMaster getPort(List<String> record, int row, int col, BaseDto baseDto, boolean isMandatory) {

        String value = record.get(col);
        if (value == null || value.trim().length() == 0) {
            if (!isMandatory) {
                return null;
            }
            throwError(baseDto, row, col, "", ErrorCode.META_CONF_DATA_IS_MANDATORY);
        } else {
            value = value.trim().toUpperCase();
        }

        if (portMap.get(value) == null) {
            PortMaster object = portMasterRepository.findByPortCode(value);
            if (object == null) {
                //throwError(baseDto, row, col, value, ErrorCode.META_CONF_DATA_IS_NOT_FOUND);
            } else {
                if (object.getStatus() == LovStatus.Block) {
                    throwError(baseDto, row, col, value, ErrorCode.META_CONF_DATA_BLOCKED);
                } else if (object.getStatus() == LovStatus.Hide) {
                    throwError(baseDto, row, col, value, ErrorCode.META_CONF_DATA_HIDDEN);
                } else {
                    portMap.put(value, object);
                }
            }
        }
        return portMap.get(value);
    }


    private ServiceMaster getService(List<String> record, int row, int col, BaseDto baseDto, boolean isMandatory) {

        String value = record.get(col);
        if (value == null || value.trim().length() == 0) {
            if (!isMandatory) {
                return null;
            }
            throwError(baseDto, row, col, "", ErrorCode.META_CONF_DATA_IS_MANDATORY);
        } else {
            value = value.trim().toUpperCase();
        }

        if (serviceMap.get(value) == null) {
            ServiceMaster object = serviceMasterRepository.findByServiceCode(value);
            if (object == null) {
                //throwError(baseDto, row, col, value, ErrorCode.META_CONF_DATA_IS_NOT_FOUND);
            } else {
                if (object.getStatus() == LovStatus.Block) {
                    throwError(baseDto, row, col, value, ErrorCode.META_CONF_DATA_BLOCKED);
                } else if (object.getStatus() == LovStatus.Hide) {
                    throwError(baseDto, row, col, value, ErrorCode.META_CONF_DATA_HIDDEN);
                } else {
                    serviceMap.put(value, object);
                }
            }
        }
        return serviceMap.get(value);
    }


    private PartyMaster getAgent(List<String> record, int row, int col, BaseDto baseDto, boolean isMandatory) {

        String value = record.get(col);
        if (value == null || value.trim().length() == 0) {
            if (!isMandatory) {
                return null;
            }
            throwError(baseDto, row, col, "", ErrorCode.META_CONF_DATA_IS_MANDATORY);
        } else {
            value = value.trim().toUpperCase();
        }

        if (agentMap.get(value) == null) {
            PartyMaster object = partyMasterRepository.getByPartyCode(value);
            if (object == null) {
                //throwError(baseDto, row, col, value, ErrorCode.META_CONF_DATA_IS_NOT_FOUND);
            } else {
                if (object.getStatus() == LovStatus.Block) {
                    throwError(baseDto, row, col, value, ErrorCode.META_CONF_DATA_BLOCKED);
                } else if (object.getStatus() == LovStatus.Hide) {
                    throwError(baseDto, row, col, value, ErrorCode.META_CONF_DATA_HIDDEN);
                } else {
                    agentMap.put(value, object);
                }
            }
        }
        return agentMap.get(value);
    }


    public void downloadChargeTemplate(HttpServletResponse response, String type) {

        if (type != null && type.trim().length() != 0) {

            // Blank workbook
            XSSFWorkbook workbook = new XSSFWorkbook();

            // Create a blank sheet
            XSSFSheet sheet = workbook.createSheet(type + " DATA");

            // This data needs to be written (Object[])
            Map<String, Object[]> data = new TreeMap<>();


            if (type.equals("charge")) {
                data.put("1",
                        new Object[]{"Charge Code", "Charge Name", "Charge Type", "Calculation Type"});

                sheet.createRow(1);
                for (int column = 0; column < 4; column++) {
                    sheet.getRow(1).createCell(column);
                }
            }


            // Iterate over data and write to sheet
            Set<String> keyset = data.keySet();
            int rownum = 0;
            for (String key : keyset) {
                XSSFRow row = sheet.createRow(rownum++);
                Object[] objArr = data.get(key);
                int cellnum = 0;
                for (Object obj : objArr) {
                    Cell cell = row.createCell(cellnum++);
                    if (obj instanceof String) {
                        cell.setCellValue((String) obj);
                    } else if (obj instanceof Integer) {
                        cell.setCellValue((Integer) obj);
                    }
                }
            }

            //Resize the column width
            autoSizeColumns(workbook);

            try {
                response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
                response.setHeader("Content-Disposition", "attachment;");
                ServletOutputStream servletOutputStream = response.getOutputStream();
                workbook.write(servletOutputStream);
                servletOutputStream.flush();
                servletOutputStream.close();
                log.info("template downloaded successfully on disk.");
            } catch (Exception exception) {
                log.error("Error occured during download template ", exception);
            }


        }

    }


    private ChargeMaster formChargeMaster(BaseDto baseDto, List<String> record, int row) {

        ChargeMaster chargeMaster = new ChargeMaster();

        if (record.get(cChargeCode) == null || record.get(cChargeCode).equals("0") || record.get(cChargeCode).trim().length() == 0) {
            throwError(baseDto, row, cChargeCode, null, ErrorCode.CSV_FILE_INVALID);
        } else {
            if (record.get(cChargeCode).length() > 10) {
                throwError(baseDto, row, cChargeCode, record.get(cChargeCode), ErrorCode.CSV_FILE_INVALID);
            }
        }
        chargeMaster.setChargeCode(record.get(cChargeCode));
        if (record.get(cChargeName) == null || record.get(cChargeName).equals("0") || record.get(cChargeName).trim().length() == 0) {
            throwError(baseDto, row, cChargeName, null, ErrorCode.CSV_FILE_INVALID);
        } else {
            if (record.get(cChargeName).length() > 100) {
                throwError(baseDto, row, cChargeName, record.get(cChargeName), ErrorCode.CSV_FILE_INVALID);
            }
        }

        chargeMaster.setChargeName(record.get(cChargeName));


        String chargeType = record.get(cChargeType);


        if (chargeType != null && ("Origin".equalsIgnoreCase(chargeType) || "Destination".equalsIgnoreCase(chargeType) || "Freight".equalsIgnoreCase(chargeType) || "Other".equalsIgnoreCase(chargeType))) {
            chargeMaster.setChargeType(ChargeType.valueOf(record.get(cChargeType)));
        } else {
            throwError(baseDto, row, cChargeType, chargeType, ErrorCode.CSV_FILE_INVALID);
        }


        String calculationType = record.get(cChargeCaluculationType);
        if (calculationType != null && ("Percentage".equalsIgnoreCase(calculationType) ||
                "Document".equalsIgnoreCase(calculationType) ||
                "Shipment".equalsIgnoreCase(calculationType) ||
                "Unit".equalsIgnoreCase(calculationType))) {
            chargeMaster.setCalculationType(ChargeCalculationType.valueOf(calculationType));
        } else {
            throwError(baseDto, row, cChargeCaluculationType, calculationType, ErrorCode.CSV_FILE_INVALID);
        }

        chargeMaster.setStatus(LovStatus.Active);

        return chargeMaster;
    }


    public List<ChargeMaster> getChargeList(BaseDto baseDto, FileUploadDto fileUploadDto) {

        List<ChargeMaster> chargeMasterList = new ArrayList<>();

        String fileExtensionName = fileUploadDto.getFileName().substring(fileUploadDto.getFileName().indexOf("."));
        XSSFWorkbook workBook = null;
        if (fileExtensionName.equals(".xlsx") || fileExtensionName.equals(".xls")) {
            List<List<String>> records = workBookToRecord(fileUploadDto.getFile(), chargeMasterTotalColumn, workBook);
            int row = 0;
            for (List<String> record : records) {
                chargeMasterList.add(formChargeMaster(baseDto, record, row));
                row++;
            }
        } else if (fileExtensionName.equals(".csv")) {

            XSSFWorkbook workBookCsv = appUtil.csvToXLSX(fileUploadDto.getFile());

            List<List<String>> records = workBookToRecord(fileUploadDto.getFile(), chargeMasterTotalColumn, workBookCsv);

            if (records != null && records.size() > 1) {
                int row = 0;
                for (List<String> record : records.subList(1, records.size())) {
                    chargeMasterList.add(formChargeMaster(baseDto, record, row));
                    row++;
                }

            } else {
                throw new RestException(ErrorCode.META_CONF_XLSX_FILE_EMPTY);
            }
        }
        return chargeMasterList;
    }


}

