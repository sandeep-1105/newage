package com.efreightsuite.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import javax.transaction.Transactional;

import com.efreightsuite.activiti.ActivitiTransactionService;
import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.FromToCurrency;
import com.efreightsuite.enumeration.CRN;
import com.efreightsuite.enumeration.JobStatus;
import com.efreightsuite.enumeration.SequenceType;
import com.efreightsuite.enumeration.YesNo;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.model.*;
import com.efreightsuite.repository.ConsolAttachmentRepository;
import com.efreightsuite.repository.ConsolRepository;
import com.efreightsuite.repository.DefaultMasterDataRepository;
import com.efreightsuite.repository.EssentialChargeMasterRepository;
import com.efreightsuite.repository.LocationMasterRepository;
import com.efreightsuite.repository.ProvisionalRepository;
import com.efreightsuite.repository.ServiceMappingRepository;
import com.efreightsuite.repository.ShipmentLinkRepository;
import com.efreightsuite.repository.ShipmentRepository;
import com.efreightsuite.repository.ShipmentServiceDetailRepository;
import com.efreightsuite.search.CurrencyRateSearchImpl;
import com.efreightsuite.util.AppUtil;
import com.efreightsuite.util.TimeUtil;
import com.efreightsuite.util.ValidateUtil;
import com.efreightsuite.validation.ConsolValidator;
import com.efreightsuite.validation.ShipmentServiceDetailValidator;
import com.efreightsuite.validation.ShipmentValidator;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class AutoImportProcessService {

    @Autowired
    private
    AppUtil appUtil;

    @Autowired
    private
    ActivitiTransactionService activitiTransactionService;

    @Autowired
    ShipmentRepository shipmentRepository;

    @Autowired
    private
    ServiceMappingRepository serviceMappingRepository;

    @Autowired
    private
    ShipmentServiceDetailValidator shipmentServiceDetailValidator;

    @Autowired
    private
    ShipmentServiceDetailRepository shipmentServiceDetailRepository;

    @Autowired
    private
    SequenceGeneratorService sequenceGeneratorService;

    @Autowired
    DefaultMasterDataRepository defaultMasterDataRepository;

    @Autowired
    ShipmentLinkRepository shipmentLinkRepository;

    @Autowired
    ServiceMasterService serviceMasterService;

    @Autowired
    CopyConsolService copyConsolService;

    @Autowired
    ShipmentValidator shipmentValidator;

    @Autowired
    private
    ConsolRepository consolRepository;

    @Autowired
    private
    ConsolValidator consolValidator;

    @Autowired
    private
    ProvisionalRepository provisionalRepository;

    @Autowired
    private
    EssentialChargeMasterRepository essentialChargeMasterRepository;

    @Autowired
    ConsolAttachmentRepository consolAttachmentRepository;

    @Autowired
    private
    CurrencyRateSearchImpl currencyRateSearchImpl;

    @Autowired
    private
    LocationMasterRepository locationMasterRepository;

    String importServiceUidToLink = null;

    String exoprtServiceUid = null;

    BaseDto baseDto = new BaseDto();

	
/*	@Author         Version         Date                 Remarks

	 Deva             1.0                            Auto import process
	
	 Sathish Kumar    1.1         27-03-17           validating essential charges validation
	 
*/


    private void validateCharge(Consol consol) {

        String value = appUtil.getLocationConfig("essential.charge.validate", consol.getLocation(), true);

        if (value != null && value.equals("YES")) {

            List<EssentialChargeMaster> essentialChargeMasterList = essentialChargeMasterRepository.findByService(consol.getServiceMaster().getId());

            if (essentialChargeMasterList != null && essentialChargeMasterList.size() > 0) {

                //check essential charges with consol level as per Robert and Achu discussion
                if (consol.getShipmentLinkList() != null && consol.getShipmentLinkList().size() > 0) {

                    for (ShipmentLink sl : consol.getShipmentLinkList()) {

                        if (sl.getService() != null && sl.getService().getIsAllInclusiveCharges() == YesNo.No) {

                            Provisional pc = provisionalRepository.findByServiceUid(sl.getService().getServiceUid());

                            if (pc != null && pc.getProvisionalItemList() != null && pc.getProvisionalItemList().size() > 0) {

                                if (validateEssentailList(essentialChargeMasterList, pc.getProvisionalItemList())) {
                                    log.info("essential validaton ended...success");
                                } else {
                                    throw new RestException(ErrorCode.ESSENTIAL_CHARGE_MISMATCHED);
                                }
                            } else {
                                throw new RestException(ErrorCode.PROVISIONAL_CHARGES_NOT_CREATED);
                            }
                        }
                    }
                } else {
                    throw new RestException(ErrorCode.CONSOL_SERVICE_DOCUMENT_MANDATORY);
                }

            } else {
                throw new RestException(ErrorCode.ESSENTIAL_CHARGE_NOT_EXIST);
            }


        }

    }

    private boolean validateEssentailList(
            List<EssentialChargeMaster> essentialChargeMasterList,
            List<ProvisionItem> provisionalItemList) {

        for (EssentialChargeMaster anEssentialChargeMasterList : essentialChargeMasterList) {
            boolean isValid = false;
            for (ProvisionItem aProvisionalItemList : provisionalItemList) {
                //for cost part
                if (anEssentialChargeMasterList.getCrn() == CRN.Cost) {
                    if (Objects.equals(anEssentialChargeMasterList.getChargeMaster().getId(), aProvisionalItemList.getChargeMaster().getId()) &&
                            aProvisionalItemList.getBuyAmount() != null) {
                        isValid = true;
                        break;
                    }
                } else {
                    //for revenue part
                    if ((Objects.equals(anEssentialChargeMasterList.getChargeMaster().getId(), aProvisionalItemList.getChargeMaster().getId()) && aProvisionalItemList.getSellAmount() != null)) {
                        isValid = true;
                        break;
                    }
                }
            }
            if (!isValid) {
                return false;
            }
        }
        return true;
    }

    @Transactional
    public void markAsComplete(Long consolId, BaseDto baseDto) {

        List<Long> serviceIds = new ArrayList<>();

        Consol oldConsol = consolRepository.findById(consolId);

        //To be validate esstional charge
        validateCharge(oldConsol);

        if (oldConsol.getAirlineEdi() == null) {

            log.error("Without EDI cannot complete the mark as complete process...............");
            throw new RestException(ErrorCode.CANNOT_COMPLETE_IMPORT_PROCESS_EDI_REQUIRED);

        } else {

            Long destId = oldConsol.getDestination().getPortGroupMaster().getCountryMaster().getId();
            Long agentId = oldConsol.getConsolDocument().getAgent().getId();

            LocationMaster locationMaster = locationMasterRepository.getByCountryAndAgent(destId, agentId);

            if (locationMaster != null) {

                log.info("Location found for the agent auto import process started: ");

                Consol consolNew = getNewConsol(oldConsol, locationMaster);

                List<ShipmentLink> shipmentLinkList = new ArrayList<>();

                if (oldConsol.getShipmentLinkList() != null && oldConsol.getShipmentLinkList().size() != 0) {
                    int subJobSequenceNo = 1;
                    for (ShipmentLink oldShipmentLink : oldConsol.getShipmentLinkList()) {

                        serviceIds.add(oldShipmentLink.getService().getId());

                        ShipmentServiceDetail shipmentServiceDetailToLink = null;

                        if (oldShipmentLink.getService().getImportRef() != null) {
                            log.info("Service is  a namination shipment");
                            shipmentServiceDetailToLink = shipmentServiceDetailRepository.findByServiceUid(oldShipmentLink.getService().getImportRef());
                            shipmentServiceDetailToLink.setConsolUid(consolNew.getConsolUid());
                            shipmentServiceDetailToLink = shipmentServiceDetailRepository.save(shipmentServiceDetailToLink);
                        } else {
                            log.info("Service is not a namination shipment");

                            String serviceUidExport = oldShipmentLink.getServiceUid();

                            ShipmentServiceDetail serviceDetailExoprt = shipmentServiceDetailRepository
                                    .findByServiceUid(serviceUidExport);
                            ValidateUtil.notNull(serviceDetailExoprt, ErrorCode.SERVICEDETAIL_EXOPRT_OLD_NOT_NULL);

                            shipmentServiceDetailToLink = getNewImportShipmentServiceDetail(
                                    serviceDetailExoprt, consolNew);

                            ValidateUtil.notNull(shipmentServiceDetailToLink, ErrorCode.SERVICEDETAIL_IMPORT_NEW_NOT_NULL);


                            shipmentServiceDetailToLink.setAutoImport(YesNo.Yes);
                            shipmentServiceDetailToLink.setExportRef(serviceDetailExoprt.getServiceUid());
                            shipmentServiceDetailRepository.save(shipmentServiceDetailToLink);

                            serviceDetailExoprt.setImportRef(shipmentServiceDetailToLink.getServiceUid());

                            shipmentServiceDetailRepository.save(serviceDetailExoprt);

                            log.info("shipmentServiceDetailToLink UID " + shipmentServiceDetailToLink.getServiceUid());
                        }


                        ShipmentLink shipmentLink = new ShipmentLink();

                        shipmentLink.setService(shipmentServiceDetailToLink);
                        shipmentLink.setConsol(consolNew);

                        shipmentLink.setCompany(consolNew.getCompany());
                        shipmentLink.setCompanyCode(consolNew.getCompanyCode());
                        shipmentLink.setConsolUid(consolNew.getConsolUid());
                        shipmentLink.setCountry(consolNew.getCountry());
                        shipmentLink.setCountryCode(consolNew.getCountryCode());
                        shipmentLink.setLocation(consolNew.getLocation());
                        shipmentLink.setLocationCode(consolNew.getLocationCode());
                        shipmentLink.setServiceUid(shipmentServiceDetailToLink.getServiceUid());
                        shipmentLink.setShipmentUid(shipmentServiceDetailToLink.getShipmentUid());
                        shipmentLink.setSubJobSequence(subJobSequenceNo);
                        subJobSequenceNo++;
                        shipmentLinkList.add(shipmentLink);

                    }
                    consolNew.setShipmentLinkList(shipmentLinkList);
                } else {
                    consolNew.setShipmentLinkList(null);
                }
                consolValidator.validate(consolNew);

                Consol consol = consolRepository.findById(oldConsol.getId());
                consol.setIsJobCompleted(YesNo.Yes);
                consolRepository.save(consol);

                Consol consolNewRes = consolRepository.save(consolNew);
                // set status to old consol

                activitiTransactionService.shipmentJobCompletion(consol.getServiceMaster(), serviceIds);

                baseDto.setResponseCode(ErrorCode.SUCCESS);
                baseDto.setResponseObject(consolNewRes.getConsolUid());

            } else {
                log.info("Location not found for the agent : ");
                throw new RestException(ErrorCode.LOCATION_NOT_FOR_THE_AGENT);

            }
        }


    }

    private Consol getNewConsol(Consol consolOld, LocationMaster locationMaster) {

        Consol consolNew = new Consol();

        consolNew.setCreatedBy(consolOld.getCreatedBy());
        consolNew.setCreatedByCode(consolOld.getCreatedByCode());

        // From the location master
        consolNew.setCompany(locationMaster.getCompanyMaster());
        consolNew.setCompanyCode(locationMaster.getCompanyCode());
        consolNew.setLocation(locationMaster);
        consolNew.setLocationCode(locationMaster.getLocationCode());
        consolNew.setCountry(locationMaster.getCountryMaster());
        consolNew.setCountryCode(locationMaster.getCountryCode());

        // Current date

        ServiceMapping serviceMapping = serviceMappingRepository.findByImport(consolOld.getServiceMaster().getId());

        ValidateUtil.notNull(serviceMapping, ErrorCode.IMPORT_SERVICE_MASTER_NOT_NULL);

        consolNew.setServiceMaster(serviceMapping.getImportService());
        consolNew.setServiceCode(serviceMapping.getImportService().getServiceCode());

        if (consolOld.getAgent() != null) {
            consolNew.setAgent(consolOld.getAgent());
            consolNew.setAgentCode(consolOld.getAgentCode());
            consolNew.setAgentManifestName(consolOld.getAgentManifestName());
            consolNew.setAgentAddress(addressCopy(consolOld.getAgentAddress()));
        } else {
            consolNew.setAgent(null);
            consolNew.setAgentCode(null);
            consolNew.setAgentManifestName(null);
            consolNew.setAgentAddress(null);
        }

        consolNew.setPpcc(consolOld.getPpcc());
        consolNew.setJobStatus(JobStatus.Active);
        consolNew.setIsJobCompleted(YesNo.No);
        consolNew.setOrigin(consolOld.getOrigin());
        consolNew.setOriginCode(consolOld.getOriginCode());
        consolNew.setPol(consolOld.getPol());
        consolNew.setPolCode(consolOld.getPolCode());
        consolNew.setPod(consolOld.getPod());
        consolNew.setPodCode(consolOld.getPodCode());
        consolNew.setDestination(consolOld.getDestination());
        consolNew.setDestinationCode(consolOld.getDestinationCode());

        consolNew.setCurrency(locationMaster.getCountryMaster().getCurrencyMaster());
        consolNew.setCurrencyCode(locationMaster.getCountryMaster().getCurrencyMaster().getCurrencyCode());
        consolNew.setCurrencyRate(1.0);
        consolNew.setLocalCurrency(locationMaster.getCountryMaster().getCurrencyMaster());

        consolNew.setCarrier(consolOld.getCarrier());
        consolNew.setCarrierCode(consolOld.getCarrierCode());
        consolNew.setEtd(consolOld.getEtd());
        consolNew.setEta(consolOld.getEta());
        consolNew.setAtd(consolOld.getAtd());
        consolNew.setAta(consolOld.getAta());
        consolNew.setDirectShipment(consolOld.getDirectShipment());


        consolNew.setConsolUid(sequenceGeneratorService.getSequenceValueByType(SequenceType.CONSOL));

        consolValidator.updateJobDate(consolNew);
		
		/* Consol connections start here */
        List<ConsolConnection> consolConnectionList = new ArrayList<>();
        if (consolOld.getConnectionList() != null && consolOld.getConnectionList().size() != 0) {
            for (ConsolConnection cc : consolOld.getConnectionList()) {
                ConsolConnection ccNew = new ConsolConnection();

                ccNew.setConsol(consolNew);

                ccNew.setMove(cc.getMove());
                ccNew.setEtd(cc.getEtd());
                ccNew.setEta(cc.getEta());
                ccNew.setPol(cc.getPol());
                ccNew.setPolCode(cc.getPolCode());
                ccNew.setPod(cc.getPod());
                ccNew.setPodCode(cc.getPodCode());
                ccNew.setCarrierMaster(cc.getCarrierMaster());
                ccNew.setFlightVoyageNo(cc.getFlightVoyageNo());
                ccNew.setConnectionStatus(cc.getConnectionStatus());

                consolConnectionList.add(ccNew);
            }
            consolNew.setConnectionList(consolConnectionList);
        } else {
            consolNew.setConnectionList(null);
        }
		/* Consol connections end here */

		/* Consol Charge Start here */
        Map<Long, FromToCurrency> map = (Map<Long, FromToCurrency>) currencyRateSearchImpl.search(locationMaster.getCountryMaster().getCurrencyMaster().getId()).getSearchResult();
        List<ConsolCharge> consolChargeList = new ArrayList<>();
        if (consolOld.getChargeList() != null && consolOld.getChargeList().size() != 0) {
            for (ConsolCharge cc : consolOld.getChargeList()) {
                ConsolCharge cchNew = new ConsolCharge();
                cchNew.setConsol(consolNew);
                cchNew.setConsolUid(consolNew.getConsolUid());

                cchNew.setLocation(cc.getLocation());
                cchNew.setLocationCode(cc.getLocationCode());
                cchNew.setCompany(cc.getCompany());
                cchNew.setCountry(cc.getCountry());
                cchNew.setCountryCode(cc.getCountryCode());
                cchNew.setChargeMaster(cc.getChargeMaster());
                cchNew.setChargeCode(cc.getChargeCode());
                cchNew.setChargeName(cc.getChargeName());
                cchNew.setCrn(cc.getCrn());
                cchNew.setPpcc(cc.getPpcc());
                cchNew.setCurrency(cc.getCurrency());
                cchNew.setCurrencyCode(cc.getCurrencyCode());

                cchNew.setUnit(cc.getUnit());
                cchNew.setAmountPerUnit(cc.getAmountPerUnit());
                cchNew.setAmount(cc.getAmount());

                if (map.get(cc.getCurrency().getId()) != null) {
                    cchNew.setCurrencyRoe(map.get(cc.getCurrency().getId()).getCSell());
                    cchNew.setLocalAmount(cc.getLocalAmount() * (1 / cchNew.getCurrencyRoe()));
                    if (consolNew.getTotalAmount() == null) {
                        consolNew.setTotalAmount(0.0);
                    }
                    consolNew.setTotalAmount(consolNew.getTotalAmount() + cchNew.getLocalAmount());
                }

                cchNew.setDue(cc.getDue());

                consolChargeList.add(cchNew);
            }
            consolNew.setChargeList(consolChargeList);
        } else {
            consolNew.setChargeList(null);
        }
		/* Consol Charge End here */

		/* Document code start here */


        consolNew.setConsolDocument(new ConsolDocument());

        consolNew.getConsolDocument().setVolume(consolOld.getConsolDocument().getVolume());
        consolNew.getConsolDocument().setRatePerCharge(consolOld.getConsolDocument().getRatePerCharge());
        consolNew.getConsolDocument().setRouteNo(consolOld.getConsolDocument().getRouteNo());
        consolNew.getConsolDocument().setMawbNo(consolOld.getConsolDocument().getMawbNo());
        consolNew.getConsolDocument().setMawbIssueDate(consolOld.getConsolDocument().getMawbIssueDate());


        consolNew.getConsolDocument().setConsolUid(consolNew.getConsolUid());


        consolNew.getConsolDocument().setDocumentReqDate(TimeUtil.getCurrentLocationTime(consolNew.getLocation()));

        if (consolOld.getConsolDocument().getShipper() != null) {
            consolNew.getConsolDocument().setShipper(consolOld.getConsolDocument().getShipper());
            consolNew.getConsolDocument().setShipperCode(consolOld.getConsolDocument().getShipperCode());
            consolNew.getConsolDocument().setShipperAddress(addressCopy(consolOld.getConsolDocument().getShipperAddress()));
            consolNew.getConsolDocument().setShipperManifestName(consolOld.getConsolDocument().getShipperManifestName());
        } else {
            consolNew.getConsolDocument().setShipper(null);
            consolNew.getConsolDocument().setShipperCode(null);
            consolNew.getConsolDocument().setShipperAddress(null);
            consolNew.getConsolDocument().setShipperManifestName(null);
        }

        if (consolOld.getConsolDocument().getForwarder() != null) {
            consolNew.getConsolDocument().setForwarder(consolOld.getConsolDocument().getForwarder());
            consolNew.getConsolDocument().setForwarderCode(consolOld.getConsolDocument().getForwarderCode());
            consolNew.getConsolDocument().setForwarderManifestName(consolOld.getConsolDocument().getForwarderManifestName());
            consolNew.getConsolDocument().setForwarderAddress(addressCopy(consolOld.getConsolDocument().getForwarderAddress()));
        } else {
            consolNew.getConsolDocument().setForwarder(null);
            consolNew.getConsolDocument().setForwarderCode(null);
            consolNew.getConsolDocument().setForwarderManifestName(null);
            consolNew.getConsolDocument().setForwarderAddress(null);
        }

        if (consolOld.getConsolDocument().getConsignee() != null) {
            consolNew.getConsolDocument().setConsignee(consolOld.getConsolDocument().getConsignee());
            consolNew.getConsolDocument().setConsigneeCode(consolOld.getConsolDocument().getConsigneeCode());
            consolNew.getConsolDocument().setConsigneeManifestName(consolOld.getConsolDocument().getConsigneeManifestName());
            consolNew.getConsolDocument().setConsigneeAddress(addressCopy(consolOld.getConsolDocument().getConsigneeAddress()));
        } else {
            consolNew.getConsolDocument().setConsignee(null);
            consolNew.getConsolDocument().setConsigneeCode(null);
            consolNew.getConsolDocument().setConsigneeManifestName(null);
            consolNew.getConsolDocument().setConsigneeAddress(null);
        }


        if (consolOld.getConsolDocument().getFirstNotify() != null) {
            consolNew.getConsolDocument().setFirstNotify(consolOld.getConsolDocument().getFirstNotify());
            consolNew.getConsolDocument().setFirstNotifyCode(consolOld.getConsolDocument().getFirstNotifyCode());
            consolNew.getConsolDocument().setFirstNotifyManifestName(consolOld.getConsolDocument().getFirstNotifyManifestName());
            consolNew.getConsolDocument().setFirstNotifyAddress(addressCopy(consolOld.getConsolDocument().getFirstNotifyAddress()));
        } else {
            consolNew.getConsolDocument().setFirstNotify(null);
            consolNew.getConsolDocument().setFirstNotifyCode(null);
            consolNew.getConsolDocument().setFirstNotifyManifestName(null);
            consolNew.getConsolDocument().setFirstNotifyAddress(null);
        }

        if (consolOld.getConsolDocument().getSecondNotify() != null) {
            consolNew.getConsolDocument().setSecondNotify(consolOld.getConsolDocument().getSecondNotify());
            consolNew.getConsolDocument().setSecondNotifyCode(consolOld.getConsolDocument().getSecondNotifyCode());
            consolNew.getConsolDocument().setSecondNotifyManifestName(consolOld.getConsolDocument().getSecondNotifyManifestName());
            consolNew.getConsolDocument().setSecondNotifyAddress(addressCopy(consolOld.getConsolDocument().getSecondNotifyAddress()));
        } else {
            consolNew.getConsolDocument().setSecondNotify(null);
            consolNew.getConsolDocument().setSecondNotifyCode(null);
            consolNew.getConsolDocument().setSecondNotifyManifestName(null);
            consolNew.getConsolDocument().setSecondNotifyAddress(null);
        }

        if (consolOld.getConsolDocument().getAgent() != null) {
            consolNew.getConsolDocument().setAgent(consolOld.getConsolDocument().getAgent());
            consolNew.getConsolDocument().setAgentCode(consolOld.getConsolDocument().getAgentCode());
            consolNew.getConsolDocument().setAgentManifestName(consolOld.getConsolDocument().getAgentManifestName());
            consolNew.getConsolDocument().setAgentAddress(addressCopy(consolOld.getConsolDocument().getAgentAddress()));
        } else {
            consolNew.getConsolDocument().setAgent(null);
            consolNew.getConsolDocument().setAgentCode(null);
            consolNew.getConsolDocument().setAgentManifestName(null);
            consolNew.getConsolDocument().setAgentAddress(null);
        }

        if (consolOld.getConsolDocument().getIssuingAgent() != null) {
            consolNew.getConsolDocument().setIssuingAgent(consolOld.getConsolDocument().getIssuingAgent());
            consolNew.getConsolDocument().setIssuingAgentCode(consolOld.getConsolDocument().getIssuingAgentCode());
            consolNew.getConsolDocument().setIssuingAgentName(consolOld.getConsolDocument().getIssuingAgentName());
            consolNew.getConsolDocument().setIssuingAgentAddress(addressCopy(consolOld.getConsolDocument().getIssuingAgentAddress()));
        } else {
            consolNew.getConsolDocument().setIssuingAgent(null);
            consolNew.getConsolDocument().setIssuingAgentCode(null);
            consolNew.getConsolDocument().setIssuingAgentName(null);
            consolNew.getConsolDocument().setIssuingAgentAddress(null);
        }

        consolNew.getConsolDocument().setPackMaster(consolOld.getConsolDocument().getPackMaster());
        consolNew.getConsolDocument().setPackCode(consolOld.getConsolDocument().getPackCode());
        consolNew.getConsolDocument().setPackDescription(consolOld.getConsolDocument().getPackDescription());

        consolNew.getConsolDocument().setNoOfPieces(consolOld.getConsolDocument().getNoOfPieces());
        consolNew.getConsolDocument().setDimensionUnit(consolOld.getConsolDocument().getDimensionUnit());
        consolNew.getConsolDocument().setDimensionUnitValue(consolOld.getConsolDocument().getDimensionUnitValue());
        consolNew.getConsolDocument().setChargebleWeight(consolOld.getConsolDocument().getChargebleWeight());
        consolNew.getConsolDocument().setGrossWeight(consolOld.getConsolDocument().getGrossWeight());
        consolNew.getConsolDocument().setVolumeWeight(consolOld.getConsolDocument().getVolumeWeight());
        consolNew.getConsolDocument().setVolumeWeightInPound(consolOld.getConsolDocument().getVolumeWeightInPound());
        consolNew.getConsolDocument().setGrossWeightInPound(consolOld.getConsolDocument().getGrossWeightInPound());
        consolNew.getConsolDocument().setHandlingInformation(consolOld.getConsolDocument().getHandlingInformation());
        consolNew.getConsolDocument().setCommodityDescription(consolOld.getConsolDocument().getCommodityDescription());
        consolNew.getConsolDocument().setMarksAndNo(consolOld.getConsolDocument().getMarksAndNo());
        consolNew.getConsolDocument().setRateClass(consolOld.getConsolDocument().getRateClass());
        consolNew.getConsolDocument().setBlReceivedPerson(consolOld.getConsolDocument().getBlReceivedPerson());
        consolNew.getConsolDocument().setBlReceivedPersonCode(consolOld.getConsolDocument().getBlReceivedPersonCode());
        consolNew.getConsolDocument().setBlReceivedDate(consolOld.getConsolDocument().getBlReceivedDate());

        consolNew.getConsolDocument().setCompany(consolOld.getConsolDocument().getCompany());
        consolNew.getConsolDocument().setCountry(consolOld.getConsolDocument().getCountry());
        consolNew.getConsolDocument().setCountryCode(consolOld.getConsolDocument().getCountryCode());

        consolNew.getConsolDocument().setLocation(consolOld.getConsolDocument().getLocation());
        consolNew.getConsolDocument().setDocPrefixMaster(consolOld.getConsolDocument().getDocPrefixMaster());
        consolNew.getConsolDocument().setDocPrefixCode(consolOld.getConsolDocument().getDocPrefixCode());

        consolNew.getConsolDocument().setPickUpDeliveryPoint(consolOld.getConsolDocument().getPickUpDeliveryPoint());

        consolNew.getConsolDocument().setServiceCode(consolOld.getConsolDocument().getServiceCode());
        consolNew.getConsolDocument().setBookingVolumeUnit(consolOld.getConsolDocument().getBookingVolumeUnit());
        consolNew.getConsolDocument().setBookingVolumeUnitCode(consolOld.getConsolDocument().getBookingVolumeUnitCode());
        consolNew.getConsolDocument().setBookingVolumeUnitValue(consolOld.getConsolDocument().getBookingVolumeUnitValue());
        consolNew.getConsolDocument().setBookingVolume(consolOld.getConsolDocument().getBookingVolume());

        consolNew.getConsolDocument().setBookingGrossWeightUnit(consolOld.getConsolDocument().getBookingGrossWeightUnit());
        consolNew.getConsolDocument().setBookingGrossWeightUnitCode(consolOld.getConsolDocument().getBookingGrossWeightUnitCode());
        consolNew.getConsolDocument().setBookingGrossWeightUnitValue(consolOld.getConsolDocument().getBookingGrossWeightUnitValue());
        consolNew.getConsolDocument().setBookingGrossWeight(consolOld.getConsolDocument().getBookingGrossWeight());

        consolNew.getConsolDocument().setBookingVolumeWeightUnit(consolOld.getConsolDocument().getBookingVolumeWeightUnit());
        consolNew.getConsolDocument().setBookingVolumeWeightUnitCode(consolOld.getConsolDocument().getBookingVolumeWeightUnitCode());
        consolNew.getConsolDocument().setBookingVolumeWeightUnitValue(consolOld.getConsolDocument().getBookingVolumeWeightUnitValue());
        consolNew.getConsolDocument().setBookingVolumeWeight(consolOld.getConsolDocument().getBookingVolumeWeight());

        consolNew.getConsolDocument().setBookingChargeableUnit(consolOld.getConsolDocument().getBookingChargeableUnit());

        consolNew.getConsolDocument().setDimGrossWeight(consolOld.getConsolDocument().getDimGrossWeight());
        consolNew.getConsolDocument().setDimGrossWeightInPound(consolOld.getConsolDocument().getDimGrossWeightInPound());
        consolNew.getConsolDocument().setDimNoOfPieces(consolOld.getConsolDocument().getDimNoOfPieces());
        consolNew.getConsolDocument().setDimVolumeWeight(consolOld.getConsolDocument().getDimVolumeWeight());
        consolNew.getConsolDocument().setDimVolumeWeightInPound(consolOld.getConsolDocument().getDimVolumeWeightInPound());

        consolNew.getConsolDocument().setVesselId(consolOld.getConsolDocument().getVesselId());
        consolNew.getConsolDocument().setVesselCode(consolOld.getConsolDocument().getVesselCode());

        consolNew.getConsolDocument().setRouteNo(consolOld.getConsolDocument().getRouteNo());

        consolNew.getConsolDocument().setTransitPort(consolOld.getConsolDocument().getTransitPort());
        consolNew.getConsolDocument().setTransitPortCode(consolOld.getConsolDocument().getTransitPortCode());
        consolNew.getConsolDocument().setTransitEta(consolOld.getConsolDocument().getTransitEta());
        consolNew.getConsolDocument().setTransitScheduleUid(consolOld.getConsolDocument().getTransitScheduleUid());

        consolNew.getConsolDocument().setMawbNo(consolOld.getConsolDocument().getMawbNo());
        consolNew.getConsolDocument().setMawbIssueDate(consolOld.getConsolDocument().getMawbIssueDate());
        consolNew.getConsolDocument().setMawbGeneratedDate(consolOld.getConsolDocument().getMawbGeneratedDate());

        consolNew.getConsolDocument().setCommodity(consolOld.getConsolDocument().getCommodity());
        consolNew.getConsolDocument().setCommodityCode(consolOld.getConsolDocument().getCommodityCode());


        consolNew.getConsolDocument().setDocumentSubDetail(consolOld.getConsolDocument().getDocumentSubDetail());

        List<ConsolDocumentDimension> consolDocumentDimensionList = new ArrayList<>();
        if (consolOld.getConsolDocument().getDimensionList() != null && consolOld.getConsolDocument().getDimensionList().size() != 0) {
            for (ConsolDocumentDimension cdd : consolOld.getConsolDocument().getDimensionList()) {
                ConsolDocumentDimension cddNew = new ConsolDocumentDimension();

                cddNew.setDocumentDetail(consolNew.getConsolDocument());

                cddNew.setNoOfPiece(cdd.getNoOfPiece());
                cddNew.setLength(cdd.getLength());
                cddNew.setWidth(cdd.getWidth());
                cddNew.setHeight(cdd.getHeight());
                cddNew.setVolWeight(cdd.getVolWeight());
                cddNew.setGrossWeight(cdd.getGrossWeight());

                consolDocumentDimensionList.add(cddNew);
            }

            consolNew.getConsolDocument().setDimensionList(consolDocumentDimensionList);
        } else {
            consolNew.getConsolDocument().setDimensionList(null);
        }

        consolNew.getConsolDocument().setDocumentNo(sequenceGeneratorService.getSequenceValueByType(SequenceType.DOCUMENT));
		
		/* Document code start here */

		
		/* Shipment Attachment Code */

        log.info("Attachment Processing Begins....");
        List<ConsolAttachment> consolAttachmentList = new ArrayList<>();
        if (consolOld.getConsolAttachmentList() != null && consolOld.getConsolAttachmentList().size() != 0) {
            log.info("There are " + consolOld.getConsolAttachmentList().size() + " number of attachments....");

            for (ConsolAttachment attachment : consolOld.getConsolAttachmentList()) {
                ConsolAttachment caNew = new ConsolAttachment();


                caNew.setConsol(consolNew);

                caNew.setRefNo(attachment.getRefNo());
                caNew.setDocumentMaster(attachment.getDocumentMaster());
                caNew.setIsCustoms(attachment.getIsCustoms());
                caNew.setIsProtected(attachment.getIsProtected());
                caNew.setFileName(attachment.getFileName());
                caNew.setFileContentType(attachment.getFileContentType());
                caNew.setFile(attachment.getFile());

                consolAttachmentList.add(caNew);
            }
            consolNew.setConsolAttachmentList(consolAttachmentList);
        } else {
            log.info("There are no attachment for this consol " + consolOld.getConsolUid());
            consolNew.setConsolAttachmentList(null);
        }
        log.info("Attachment Processing Finished...");

		/* Consol Event Code Start here */
        List<ConsolEvent> consolEventList = new ArrayList<>();
        if (consolOld.getEventList() != null && consolOld.getEventList().size() != 0) {
            for (ConsolEvent event : consolOld.getEventList()) {

                ConsolEvent ceNew = new ConsolEvent();

                ceNew.setConsol(consolNew);

                ceNew.setEventMaster(event.getEventMaster());
                ceNew.setEventDate(event.getEventDate());
                ceNew.setFollowUpRequired(event.getFollowUpRequired());
                ceNew.setAssignedTo(event.getAssignedTo());
                ceNew.setFollowUpDate(event.getFollowUpDate());
                ceNew.setIsCompleted(event.getIsCompleted());

                consolEventList.add(ceNew);
            }
            consolNew.setEventList(consolEventList);
        } else {
            consolNew.setEventList(null);
        }

		/* Consol Event Code End here */

        // Consol pICK UP
        PickUpDeliveryPoint pickUpDeliveryPointNew = new PickUpDeliveryPoint();
        if (consolOld.getPickUpDeliveryPoint() != null) {

            PickUpDeliveryPoint pickUpDeliveryPointOld = consolOld.getPickUpDeliveryPoint();


            pickUpDeliveryPointNew.setTransporter(pickUpDeliveryPointOld.getTransporter());
            pickUpDeliveryPointNew.setTransporterCode(pickUpDeliveryPointOld.getTransporterCode());
            pickUpDeliveryPointNew.setTransportermManifestName(pickUpDeliveryPointOld.getTransportermManifestName());

            pickUpDeliveryPointNew.setTransportAddress(pickUpDeliveryPointOld.getTransportAddress());
            if (pickUpDeliveryPointNew.getTransportAddress() != null) {
                pickUpDeliveryPointNew.getTransportAddress().setId(null);
            }

            pickUpDeliveryPointNew.setTransporterAddress(pickUpDeliveryPointOld.getTransporterAddress());
            pickUpDeliveryPointNew.setIsOurPickUp(pickUpDeliveryPointOld.getIsOurPickUp());
            pickUpDeliveryPointNew.setPickUpClientRefNo(pickUpDeliveryPointOld.getPickUpClientRefNo());
            pickUpDeliveryPointNew.setPickupPoint(pickUpDeliveryPointOld.getPickupPoint());
            pickUpDeliveryPointNew.setPickupPointCode(pickUpDeliveryPointOld.getPickupPointCode());
            pickUpDeliveryPointNew.setPickupFrom(pickUpDeliveryPointOld.getPickupFrom());
            pickUpDeliveryPointNew.setPickupFromCode(pickUpDeliveryPointOld.getPickupFromCode());
            pickUpDeliveryPointNew.setPickUpPlace(pickUpDeliveryPointOld.getPickUpPlace());
            pickUpDeliveryPointNew.setPickUpContactPerson(pickUpDeliveryPointOld.getPickUpContactPerson());
            pickUpDeliveryPointNew.setPickUpMobileNo(pickUpDeliveryPointOld.getPickUpMobileNo());
            pickUpDeliveryPointNew.setPickUpPhoneNo(pickUpDeliveryPointOld.getPickUpPhoneNo());
            pickUpDeliveryPointNew.setPickUpEmail(pickUpDeliveryPointOld.getPickUpEmail());
            pickUpDeliveryPointNew.setPickUpFax(pickUpDeliveryPointOld.getPickUpFax());
            pickUpDeliveryPointNew.setPickUpFollowUp(pickUpDeliveryPointOld.getPickUpFollowUp());
            pickUpDeliveryPointNew.setPickUpPlanned(pickUpDeliveryPointOld.getPickUpPlanned());
            pickUpDeliveryPointNew.setPickUpActual(pickUpDeliveryPointOld.getPickUpActual());
            pickUpDeliveryPointNew.setDeliveryPoint(pickUpDeliveryPointOld.getDeliveryPoint());
            pickUpDeliveryPointNew.setDeliveryPointCode(pickUpDeliveryPointOld.getDeliveryPointCode());
            pickUpDeliveryPointNew.setDeliveryFrom(pickUpDeliveryPointOld.getDeliveryFrom());
            pickUpDeliveryPointNew.setDeliveryFromCode(pickUpDeliveryPointOld.getDeliveryFromCode());
            pickUpDeliveryPointNew.setDeliveryPlace(pickUpDeliveryPointOld.getDeliveryPlace());
            pickUpDeliveryPointNew.setDeliveryContactPerson(pickUpDeliveryPointOld.getDeliveryContactPerson());
            pickUpDeliveryPointNew.setDoorDeliveryMobileNo(pickUpDeliveryPointOld.getDoorDeliveryMobileNo());
            pickUpDeliveryPointNew.setDoorDeliveryPhoneNo(pickUpDeliveryPointOld.getDoorDeliveryPhoneNo());
            pickUpDeliveryPointNew.setDoorDeliveryEmail(pickUpDeliveryPointOld.getDoorDeliveryEmail());
            pickUpDeliveryPointNew.setDoorDeliveryExpected(pickUpDeliveryPointOld.getDoorDeliveryExpected());
            pickUpDeliveryPointNew.setDoorDeliveryActual(pickUpDeliveryPointOld.getDoorDeliveryActual());

            consolNew.setPickUpDeliveryPoint(pickUpDeliveryPointNew);
        } else {
            consolNew.setPickUpDeliveryPoint(null);
        }

        log.info("Consol copied successfull with the consol uid" + consolNew.getConsolUid());
        log.info("End AutoImportProcessService.getNewConsol ");
        return consolNew;

    }

    private ShipmentServiceDetail getNewImportShipmentServiceDetail(ShipmentServiceDetail service, Consol consol) {

        log.info("Start AutoImportProcessservice.executeAutoImportProcess");

        ShipmentServiceDetail shipmentServiceDetail = null;

        if (service != null) {

            Shipment shipment = service.getShipment();

            ValidateUtil.notNull(service.getIsProcessCompleted(), ErrorCode.IS_PROCESS_COMPLETED_NOT_NULL);
            ValidateUtil.notEquals(YesNo.No, service.getIsProcessCompleted(), ErrorCode.IS_PROCESS_COMPLETED_NOT_EQUAL);

            if (service.getIsProcessCompleted().equals(YesNo.No)) {

                log.info("Found the service to import : " + service.getId());

                shipmentServiceDetail = new ShipmentServiceDetail();

                String serviceUid = sequenceGeneratorService.getSequenceValueByType(SequenceType.SERVICE);
                shipmentServiceDetail.setServiceUid(serviceUid);
                log.info("shipmentServiceDetailToLink UID " + shipmentServiceDetail.getServiceUid());

                ServiceStatus serviceStatus = new ServiceStatus();
                serviceStatus.setShipmentServiceDetail(shipmentServiceDetail);
                serviceStatus.setServiceStatus(com.efreightsuite.enumeration.ServiceStatus.Generated);
                shipmentServiceDetail.getServiceStatusList().add(serviceStatus);

                shipmentServiceDetail.setLastUpdatedStatus(serviceStatus);

                // Getting and Setting the values for
                shipmentServiceDetail.setShipment(shipment);
                shipmentServiceDetail.setShipmentUid(service.getShipment().getShipmentUid());
                shipmentServiceDetail.setConsolUid(consol.getConsolUid());
                shipmentServiceDetail.setIsTranshipment(service.getIsTranshipment());

                if (shipmentServiceDetail.getIsTranshipment() != null && shipmentServiceDetail.getIsTranshipment() == YesNo.Yes) {
                    shipmentServiceDetail.setTranshipIsDoneLocation(YesNo.No);
                }

                shipmentServiceDetail.setServiceMaster(consol.getServiceMaster());
                shipmentServiceDetail.setServiceCode(consol.getServiceMaster().getServiceCode());

                shipmentServiceDetail.setCustomerService(service.getCustomerService());
                shipmentServiceDetail.setCustomerServiceCode(service.getCustomerServiceCode());
                shipmentServiceDetail.setCustomerServicePersonEmail(service.getCustomerServicePersonEmail());
                shipmentServiceDetail.setPpcc(service.getPpcc());
                shipmentServiceDetail.setOrigin(service.getOrigin());
                shipmentServiceDetail.setOriginCode(service.getOriginCode());
                shipmentServiceDetail.setPol(service.getPol());
                shipmentServiceDetail.setPolCode(service.getPolCode());

                shipmentServiceDetail.setPod(service.getPod());
                shipmentServiceDetail.setPodCode(service.getPodCode());

                shipmentServiceDetail.setDestination(service.getDestination());
                shipmentServiceDetail.setDestinationCode(service.getDestinationCode());

                shipmentServiceDetail.setWhoRouted(service.getWhoRouted());
                shipmentServiceDetail.setSalesman(service.getSalesman());
                shipmentServiceDetail.setSalesmanCode(service.getSalesmanCode());
                shipmentServiceDetail.setAgent(service.getAgent());
                shipmentServiceDetail.setAgentCode(service.getAgentCode());
                shipmentServiceDetail.setDivision(service.getDivision());
                shipmentServiceDetail.setDivisionCode(service.getDivisionCode());
                shipmentServiceDetail.setCompany(service.getCompany());
                shipmentServiceDetail.setCompanyCode(service.getCompanyCode());
                shipmentServiceDetail.setLocation(consol.getLocation());
                shipmentServiceDetail.setLocationCode(consol.getLocation().getLocationCode());
                shipmentServiceDetail.setParty(service.getParty());
                shipmentServiceDetail.setPartyCode(service.getPartyCode());
                shipmentServiceDetail.setCoLoader(service.getCoLoader());
                shipmentServiceDetail.setCoLoaderCode(service.getCoLoaderCode());
                shipmentServiceDetail.setPackMaster(service.getPackMaster());
                shipmentServiceDetail.setPackCode(service.getPackCode());
                shipmentServiceDetail.setBookedPieces(service.getBookedPieces());
                shipmentServiceDetail.setBookedGrossWeightUnitKg(service.getBookedGrossWeightUnitKg());
                shipmentServiceDetail.setBookedVolumeWeightUnitKg(service.getBookedVolumeWeightUnitKg());
                shipmentServiceDetail.setBookedChargeableUnit(service.getBookedChargeableUnit());
                shipmentServiceDetail.setBookedGrossWeightUnitPound(service.getBookedGrossWeightUnitPound());
                shipmentServiceDetail.setBookedVolumeWeightUnitPound(service.getBookedVolumeWeightUnitPound());
                shipmentServiceDetail.setCarrier(service.getCarrier());
                shipmentServiceDetail.setCarrierCode(service.getCarrierCode());
                shipmentServiceDetail.setEtd(service.getEtd());
                shipmentServiceDetail.setEta(service.getEta());
                shipmentServiceDetail.setRouteNo(service.getRouteNo());
                shipmentServiceDetail.setMawbNo(service.getMawbNo());
                shipmentServiceDetail.setLocalCurrency(service.getLocalCurrency());
                shipmentServiceDetail.setLocalCurrencyCode(service.getLocalCurrencyCode());
                shipmentServiceDetail.setClientGrossRate(service.getClientGrossRate());
                shipmentServiceDetail.setClientNetRate(service.getClientNetRate());
                shipmentServiceDetail.setDeclaredCost(service.getDeclaredCost());
                shipmentServiceDetail.setCost(service.getCost());
                shipmentServiceDetail.setRate(service.getRate());
                shipmentServiceDetail.setCarrierBookingNumber(service.getCarrierBookingNumber());
                shipmentServiceDetail.setYard(service.getYard());
                shipmentServiceDetail.setLoading(service.getLoading());
                shipmentServiceDetail.setCfsRemark(service.getCfsRemark());
                shipmentServiceDetail.setCfsReceiptNo(service.getCfsReceiptNo());
                shipmentServiceDetail.setDnNo(service.getDnNo());
                shipmentServiceDetail.setIsAgreed(service.getIsAgreed());

				/*ServiceMasterDetail segment = service.getSegment();
				shipmentServiceDetail.setSegment(segment);*/

                shipmentServiceDetail.setCountry(consol.getLocation().getCountryMaster());
                shipmentServiceDetail.setCountryCode(consol.getLocation().getCountryMaster().getCountryCode());
                shipmentServiceDetail.setCustomerManifestName(service.getCustomerManifestName());
                shipmentServiceDetail.setQuotationUid(service.getQuotationUid());
                shipmentServiceDetail.setBookedVolumeUnit(service.getBookedVolumeUnit());
                shipmentServiceDetail.setBookedVolumeUnitCode(service.getBookedVolumeUnitCode());
                shipmentServiceDetail.setBookedVolumeUnitValue(service.getBookedVolumeUnitValue());
                shipmentServiceDetail.setBookedVolumeUnitCbm(service.getBookedVolumeUnitCbm());
                shipmentServiceDetail.setBookedGrossWeightUnit(service.getBookedGrossWeightUnit());
                shipmentServiceDetail.setBookedGrossWeightUnitCode(service.getBookedGrossWeightUnitCode());
                shipmentServiceDetail.setBookedGrossWeightUnitValue(service.getBookedGrossWeightUnitValue());
                shipmentServiceDetail.setVesselId(service.getVesselId());
                shipmentServiceDetail.setServiceDetail(service.getServiceDetail());
                shipmentServiceDetail.setTradeCode(service.getTradeCode());
                shipmentServiceDetail.setIsServiceJob(service.getIsServiceJob());
                shipmentServiceDetail.setTransitMode(service.getTransitMode());
                shipmentServiceDetail.setTransitPort(service.getTransitPort());
                shipmentServiceDetail.setTransitPortCode(service.getTransitPortCode());
                shipmentServiceDetail.setTosMaster(service.getTosMaster());
                shipmentServiceDetail.setTosCode(service.getTosCode());
                shipmentServiceDetail.setProject(service.getProject());
                shipmentServiceDetail.setCommodity(service.getCommodity());
                shipmentServiceDetail.setCommodityCode(service.getCommodityCode());
                shipmentServiceDetail.setDirectShipment(service.getDirectShipment());
                shipmentServiceDetail.setHoldShipment(service.getHoldShipment());
                shipmentServiceDetail.setHazardous(service.getHazardous());
                shipmentServiceDetail.setOverDimension(service.getOverDimension());

                shipmentServiceDetail.setShipmentChargeList(null);
                shipmentServiceDetail.setShipmentServiceTriggerList(null);
                shipmentServiceDetail.setEventList(null);
                shipmentServiceDetail.setPickUpDeliveryPoint(null);

                shipmentServiceDetailValidator.updateServiceDate(shipmentServiceDetail);

                List<DocumentDetail> documentList = new ArrayList<>();

                if (service.getDocumentList() != null && service.getDocumentList().size() != 0) {

                    for (DocumentDetail dd : service.getDocumentList()) {

                        DocumentDetail ddNew = new DocumentDetail();

                        ddNew.setShipmentServiceDetail(shipmentServiceDetail);
                        ddNew.setShipmentUid(shipment.getShipmentUid());
                        ddNew.setServiceUid(shipmentServiceDetail.getServiceUid());
                        ddNew.setDocumentNo(sequenceGeneratorService.getSequenceValueByType(SequenceType.DOCUMENT));
                        ddNew.setDocumentReqDate(TimeUtil.getCurrentLocationTime(consol.getLocation()));

                        if (dd.getShipper() != null) {
                            ddNew.setShipper(dd.getShipper());
                            ddNew.setShipperCode(dd.getShipperCode());
                            ddNew.setShipperManifestName(dd.getShipperManifestName());
                            ddNew.setShipperAddress(addressCopy(dd.getShipperAddress()));
                        }

                        if (dd.getForwarder() != null) {
                            ddNew.setForwarder(dd.getForwarder());
                            ddNew.setForwarderCode(dd.getForwarderCode());
                            ddNew.setForwarderManifestName(dd.getForwarderManifestName());
                            ddNew.setForwarderAddress(addressCopy(dd.getForwarderAddress()));
                        }

                        if (dd.getConsignee() != null) {
                            ddNew.setConsignee(dd.getConsignee());
                            ddNew.setConsigneeCode(dd.getConsigneeCode());
                            ddNew.setConsigneeManifestName(dd.getConsigneeManifestName());
                            ddNew.setConsigneeAddress(addressCopy(dd.getConsigneeAddress()));
                        }

                        if (dd.getFirstNotify() != null) {
                            ddNew.setFirstNotify(dd.getFirstNotify());
                            ddNew.setFirstNotifyCode(dd.getFirstNotifyCode());
                            ddNew.setFirstNotifyManifestName(dd.getFirstNotifyManifestName());
                            ddNew.setFirstNotifyAddress(addressCopy(dd.getFirstNotifyAddress()));
                        }

                        if (dd.getSecondNotify() != null) {
                            ddNew.setSecondNotify(dd.getSecondNotify());
                            ddNew.setSecondNotifyCode(dd.getSecondNotifyCode());
                            ddNew.setSecondNotifyManifestName(dd.getSecondNotifyManifestName());
                            ddNew.setSecondNotifyAddress(addressCopy(dd.getSecondNotifyAddress()));
                        }

                        if (dd.getAgent() != null) {
                            ddNew.setAgent(dd.getAgent());
                            ddNew.setAgentCode(dd.getAgentCode());
                            ddNew.setAgentManifestName(dd.getAgentManifestName());
                            ddNew.setAgentAddress(addressCopy(dd.getAgentAddress()));
                        }

                        if (dd.getIssuingAgent() != null) {
                            ddNew.setIssuingAgent(dd.getIssuingAgent());
                            ddNew.setIssuingAgentCode(dd.getIssuingAgentCode());
                            ddNew.setIssuingAgentName(dd.getIssuingAgentName());
                            ddNew.setIssuingAgentAddress(addressCopy(dd.getIssuingAgentAddress()));
                        }


                        ddNew.setOrigin(dd.getOrigin());
                        ddNew.setOriginCode(dd.getOriginCode());

                        ddNew.setPol(dd.getPol());
                        ddNew.setPolCode(dd.getPolCode());

                        ddNew.setPod(dd.getPod());
                        ddNew.setPodCode(dd.getPodCode());

                        ddNew.setDestination(dd.getDestination());
                        ddNew.setDestinationCode(dd.getDestinationCode());

                        ddNew.setPackMaster(dd.getPackMaster());
                        ddNew.setPackCode(dd.getPackCode());
                        ddNew.setPackDescription(dd.getPackDescription());

                        ddNew.setNoOfPieces(dd.getNoOfPieces());
                        ddNew.setDimensionUnit(dd.getDimensionUnit());
                        ddNew.setDimensionUnitValue(dd.getDimensionUnitValue());

                        ddNew.setChargebleWeight(dd.getChargebleWeight());
                        ddNew.setGrossWeight(dd.getGrossWeight());
                        ddNew.setVolumeWeight(dd.getVolumeWeight());

                        ddNew.setVolumeWeightInPound(dd.getVolumeWeightInPound());
                        ddNew.setGrossWeightInPound(dd.getGrossWeightInPound());

                        ddNew.setBookedVolumeUnitCbm(dd.getBookedVolumeUnitCbm());

                        ddNew.setDimensionGrossWeight(dd.getDimensionGrossWeight());
                        ddNew.setDimensionGrossWeightInKg(dd.getDimensionGrossWeightInKg());
                        ddNew.setDimensionVolumeWeight(dd.getDimensionVolumeWeight());

                        ddNew.setRatePerCharge(dd.getRatePerCharge());

                        ddNew.setHandlingInformation(dd.getHandlingInformation());
                        ddNew.setAccoutingInformation(dd.getAccoutingInformation());

                        ddNew.setCommodityDescription(dd.getCommodityDescription());

                        ddNew.setMarksAndNo(dd.getMarksAndNo());

                        ddNew.setRateClass(dd.getRateClass());

                        ddNew.setBlReceivedPerson(dd.getBlReceivedPerson());
                        ddNew.setBlReceivedPersonCode(dd.getBlReceivedPersonCode());
                        ddNew.setBlReceivedDate(dd.getBlReceivedDate());

                        ddNew.setCompany(service.getCompany());
                        ddNew.setCountry(consol.getLocation().getCountryMaster());
                        ddNew.setCountryCode(consol.getLocation().getCountryMaster().getCountryCode());

                        ddNew.setLocation(consol.getLocation());

                        ddNew.setDocPrefixMaster(dd.getDocPrefixMaster());
                        ddNew.setDocPrefixCode(dd.getDocPrefixCode());

                        ddNew.setPickUpDeliveryPoint(dd.getPickUpDeliveryPoint());

                        ddNew.setServiceCode(dd.getServiceCode());

                        ddNew.setVesselId(dd.getVesselId());
                        ddNew.setVesselCode(dd.getVesselCode());
                        ddNew.setRouteNo(dd.getRouteNo());
                        ddNew.setTransitPort(dd.getTransitPort());
                        ddNew.setTransitPortCode(dd.getTransitPortCode());
                        ddNew.setTransitEta(dd.getTransitEta());
                        ddNew.setTransitScheduleUid(dd.getTransitScheduleUid());
                        ddNew.setMawbNo(dd.getMawbNo());
                        ddNew.setCarrier(dd.getCarrier());
                        ddNew.setEtd(dd.getEtd());
                        ddNew.setEta(dd.getEta());
                        ddNew.setCommodity(dd.getCommodity());
                        ddNew.setCommodityCode(dd.getCommodityCode());
                        ddNew.setDocumentSubDetail(dd.getDocumentSubDetail());

                        List<ServiceDocumentDimension> documentDimensionList = new ArrayList<>();
                        if (dd.getDimensionList() != null && dd.getDimensionList().size() != 0) {
                            for (ServiceDocumentDimension sdd : dd.getDimensionList()) {
                                // get and set the values
                                ServiceDocumentDimension sddNew = new ServiceDocumentDimension();

                                sddNew.setDocumentDetail(ddNew);
                                sddNew.setNoOfPiece(sdd.getNoOfPiece());
                                sddNew.setLength(sdd.getLength());
                                sddNew.setWidth(sdd.getWidth());
                                sddNew.setHeight(sdd.getHeight());
                                sddNew.setVolWeight(sdd.getVolWeight());
                                sddNew.setGrossWeight(sdd.getGrossWeight());

                                documentDimensionList.add(sddNew);
                            }
                            ddNew.setDimensionList(documentDimensionList);
                        } else {
                            ddNew.setDimensionList(null);
                        }
                        documentList.add(ddNew);
                    }
                    shipmentServiceDetail.setDocumentList(documentList);
                } else {
                    shipmentServiceDetail.setDocumentList(null);
                }
                List<ServiceConnection> serviceConnectionList = new ArrayList<>();
                if (service.getConnectionList() != null && service.getConnectionList().size() != 0) {
                    for (ServiceConnection sc : service.getConnectionList()) {

                        ServiceConnection scNew = new ServiceConnection();

                        scNew.setShipmentServiceDetail(shipmentServiceDetail);
                        scNew.setShipmentUid(shipment.getShipmentUid());
                        scNew.setServiceUid(shipmentServiceDetail.getServiceUid());

                        scNew.setMove(sc.getMove());
                        scNew.setEtd(sc.getEtd());
                        scNew.setEta(sc.getEta());
                        scNew.setPol(sc.getPol());
                        scNew.setPolCode(sc.getPolCode());
                        scNew.setPod(sc.getPod());
                        scNew.setPodCode(sc.getPodCode());
                        scNew.setCarrierMaster(sc.getCarrierMaster());
                        scNew.setFlightVoyageNo(sc.getFlightVoyageNo());
                        scNew.setConnectionStatus(sc.getConnectionStatus());

                        serviceConnectionList.add(scNew);
                    }
                    shipmentServiceDetail.setConnectionList(serviceConnectionList);
                } else {
                    shipmentServiceDetail.setConnectionList(null);
                }

            }

        }

        return shipmentServiceDetail;
    }

    private AddressMaster addressCopy(AddressMaster oldAddress) {

        if (oldAddress == null)
            return null;

        AddressMaster newAddress = new AddressMaster();
        newAddress.setAddressLine1(oldAddress.getAddressLine1());
        newAddress.setAddressLine2(oldAddress.getAddressLine2());
        newAddress.setAddressLine3(oldAddress.getAddressLine3());
        newAddress.setAddressLine4(oldAddress.getAddressLine4());
        newAddress.setEmail(oldAddress.getEmail());
        newAddress.setPhone(oldAddress.getPhone());
        newAddress.setMobile(oldAddress.getMobile());
        return newAddress;
    }


}
