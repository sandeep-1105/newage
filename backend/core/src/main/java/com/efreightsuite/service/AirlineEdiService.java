package com.efreightsuite.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.efreightsuite.activiti.ActivitiTransactionService;
import com.efreightsuite.communication.RabbitMQMessageSender;
import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.MessagingDto;
import com.efreightsuite.enumeration.AddressType;
import com.efreightsuite.enumeration.EdiStatus;
import com.efreightsuite.enumeration.YesNo;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.model.*;
import com.efreightsuite.repository.AirlineEdiRepository;
import com.efreightsuite.repository.AirlineEdiStatusRepository;
import com.efreightsuite.repository.CarrierMasterRepository;
import com.efreightsuite.repository.ConsolRepository;
import com.efreightsuite.util.AppUtil;
import com.efreightsuite.util.SaaSUtil;
import com.efreightsuite.util.TimeUtil;
import com.efreightsuite.util.ValidateUtil;
import com.efreightsuite.validation.AirlineEdiValidator;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.exception.ExceptionUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class AirlineEdiService {

    @Autowired
    private
    ActivitiTransactionService activitiTransactionService;

    @Autowired
    private
    ConsolRepository consolRepository;

    @Autowired
    private
    AirlineEdiRepository airlineEdiRepository;

    @Autowired
    private
    AirlineEdiStatusRepository airlineEdiStatusRepository;

    @Autowired
    private
    AppUtil appUtil;

    @Autowired
    private
    AirlineEdiValidator airlineEdiValidator;

    @Autowired
    private
    RabbitMQMessageSender rabbitMqSendMessage;

    @Autowired
    private
    CarrierMasterRepository carrierMasterRepository;

    public BaseDto getEdi(Long id, Boolean isAgentChange) {

        log.info("ConsolService -> getEdi method is called....");

        BaseDto baseDto = new BaseDto();

        try {
            AirlineEdi airlineEdi = null;

            Consol consol = consolRepository.findById(id);

            log.info("Consol UID : " + consol.getConsolUid());

            airlineEdi = copyEdiFromConsol(consol, isAgentChange);

            if (airlineEdi != null && airlineEdi.getStatusList() != null && !airlineEdi.getStatusList().isEmpty()) {
                if (airlineEdi.getStatusList() != null) {
                    airlineEdi.getStatusList().sort(AirlineEdiStatus.IdComparator);
                }
            }

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(airlineEdi);

        } catch (RestException re) {

            log.error("Exception in getEDI method ", re);

            baseDto.setResponseCode(re.getMessage());

        } catch (Exception e) {

            log.error("Exception in ConsolService -> getEdi method ", e);

            baseDto.setResponseCode(ErrorCode.FAILED);

        }
        return appUtil.setDesc(baseDto);
    }

    public BaseDto saveEdi(AirlineEdi airlineEdi) {

        log.info("saveEdi is called ");

        BaseDto baseDto = new BaseDto();

        Consol consol = null;

        AirlineEdiStatus airlineEdiStatus = new AirlineEdiStatus();
        airlineEdiStatus.setAirlineEdi(airlineEdi);
        airlineEdiStatus.setCreateDate(TimeUtil.getCurrentLocationTime());

        try {

            boolean airLineSubmissionTask = false;

            if (airlineEdi != null && airlineEdi.getId() == null) {
                airLineSubmissionTask = true;
            }

            airlineEdi.setLocationMaster(AuthService.getCurrentUser().getSelectedUserLocation());
            airlineEdi.setLocationCode(AuthService.getCurrentUser().getSelectedUserLocation().getLocationCode());

            airlineEdi.setCompanyMaster(AuthService.getCurrentUser().getSelectedUserLocation().getCompanyMaster());
            airlineEdi.setCompanyCode(
                    AuthService.getCurrentUser().getSelectedUserLocation().getCompanyMaster().getCompanyCode());

            airlineEdi.setCountryMaster(AuthService.getCurrentUser().getSelectedUserLocation().getCountryMaster());
            airlineEdi.setCountryCode(
                    AuthService.getCurrentUser().getSelectedUserLocation().getCountryMaster().getCountryCode());

            if (airlineEdi.getConnectionList() != null && !airlineEdi.getConnectionList().isEmpty()) {
                for (AirlineEdiConnection airlineEdiConnection : airlineEdi.getConnectionList()) {

                    airlineEdiConnection.setAirlineEdi(airlineEdi);

                    airlineEdiConnection.setLocationMaster(AuthService.getCurrentUser().getSelectedUserLocation());
                    airlineEdiConnection
                            .setLocationCode(AuthService.getCurrentUser().getSelectedUserLocation().getLocationCode());

                    airlineEdiConnection.setCompanyMaster(
                            AuthService.getCurrentUser().getSelectedUserLocation().getCompanyMaster());
                    airlineEdiConnection.setCompanyCode(
                            AuthService.getCurrentUser().getSelectedUserLocation().getCompanyMaster().getCompanyCode());

                    airlineEdiConnection.setCountryMaster(
                            AuthService.getCurrentUser().getSelectedUserLocation().getCountryMaster());
                    airlineEdiConnection.setCountryCode(
                            AuthService.getCurrentUser().getSelectedUserLocation().getCountryMaster().getCountryCode());
                }
            }

            if (airlineEdi.getDimensionList() != null && !airlineEdi.getDimensionList().isEmpty()) {
                for (AirlineEdiDimension airlineEdiDimension : airlineEdi.getDimensionList()) {
                    airlineEdiDimension.setAirlineEdi(airlineEdi);

                    airlineEdiDimension.setLocationMaster(AuthService.getCurrentUser().getSelectedUserLocation());
                    airlineEdiDimension
                            .setLocationCode(AuthService.getCurrentUser().getSelectedUserLocation().getLocationCode());

                    airlineEdiDimension.setCompanyMaster(
                            AuthService.getCurrentUser().getSelectedUserLocation().getCompanyMaster());
                    airlineEdiDimension.setCompanyCode(
                            AuthService.getCurrentUser().getSelectedUserLocation().getCompanyMaster().getCompanyCode());

                    airlineEdiDimension.setCountryMaster(
                            AuthService.getCurrentUser().getSelectedUserLocation().getCountryMaster());
                    airlineEdiDimension.setCountryCode(
                            AuthService.getCurrentUser().getSelectedUserLocation().getCountryMaster().getCountryCode());

                }
            }

            if (airlineEdi.getHouseList() != null && !airlineEdi.getHouseList().isEmpty()) {
                for (AirlineEdiHouse airlineEdiHouse : airlineEdi.getHouseList()) {
                    airlineEdiHouse.setAirlineEdi(airlineEdi);

                    airlineEdiHouse.setLocationMaster(AuthService.getCurrentUser().getSelectedUserLocation());
                    airlineEdiHouse
                            .setLocationCode(AuthService.getCurrentUser().getSelectedUserLocation().getLocationCode());

                    airlineEdiHouse.setCompanyMaster(
                            AuthService.getCurrentUser().getSelectedUserLocation().getCompanyMaster());
                    airlineEdiHouse.setCompanyCode(
                            AuthService.getCurrentUser().getSelectedUserLocation().getCompanyMaster().getCompanyCode());

                    airlineEdiHouse.setCountryMaster(
                            AuthService.getCurrentUser().getSelectedUserLocation().getCountryMaster());
                    airlineEdiHouse.setCountryCode(
                            AuthService.getCurrentUser().getSelectedUserLocation().getCountryMaster().getCountryCode());

                }
            }

            airlineEdi = airlineEdiRepository.save(airlineEdi);

            if (airlineEdi.getId() != null) {
                consol = initiateAirlineEdi(airlineEdi);
            } else {
                log.error("Edi is not created");
            }

			/* Edi Status Update Code */

            airlineEdiStatus.setStatus(EdiStatus.INITIATE_SUCCESS);
            airlineEdiStatus.setStatusMessage(appUtil.getDesc(ErrorCode.EDI_STATUS_PROCESS_INITIATED));

            airlineEdi.setStatus(EdiStatus.INITIATE_SUCCESS);
            airlineEdi.setStatusMessage(appUtil.getDesc(ErrorCode.EDI_STATUS_PROCESS_INITIATED));

            if (consol == null) {
                consol = consolRepository.findByConsolUid(airlineEdi.getMasterShipmentUid());
            }
            afterGet(consol);

            if (airLineSubmissionTask) {
                List<Long> serviceIds = new ArrayList<>();

                for (ShipmentLink sk : consol.getShipmentLinkList()) {
                    serviceIds.add(sk.getService().getId());
                }

                activitiTransactionService.consolAirline(consol.getServiceMaster(), serviceIds);
            }

            baseDto.setResponseCode(ErrorCode.SUCCESS);
            baseDto.setResponseObject(consol);

        } catch (Exception exception) {

            log.error("Exception in saveEdi method ", exception);

            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Exception Cause 1 ::: " + exceptionCause1);

            airlineEdiStatus.setStatus(EdiStatus.INITIATE_FAILURE);
            airlineEdiStatus.setStatusMessage(appUtil.getDesc(ErrorCode.EDI_STATUS_PROCESS_INITIATED_FAILED));

            airlineEdi.setStatus(EdiStatus.INITIATE_FAILURE);
            airlineEdi.setStatusMessage(appUtil.getDesc(ErrorCode.EDI_STATUS_PROCESS_INITIATED_FAILED));

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        airlineEdiRepository.updateEdiStatus(airlineEdi.getStatusMessage(), airlineEdi.getStatus(), airlineEdi.getId());
        // airlineEdiRepository.save(airlineEdi);
        airlineEdiStatusRepository.save(airlineEdiStatus);

        return baseDto;

    }

    private Consol initiateAirlineEdi(AirlineEdi airlineEdi) {
        log.info(".................................Initiating the EDI process........................");

        // Message to Send to the Rabbit MQ
        MessagingDto requestData = new MessagingDto();

        requestData.setEdiId(airlineEdi.getId());

        requestData.setUserProfileId(AuthService.getCurrentUser().getId());

        requestData.setIsDirect(airlineEdi.getDirectShipment());

        CarrierMaster carrierMaster = carrierMasterRepository.getOne(airlineEdi.getCarrierMaster().getId());
        if (carrierMaster.getMessagingType() != null) {
            requestData.setMessagingFor(carrierMaster.getMessagingType());
        }

        requestData.setSelectedLocationId(AuthService.getCurrentUser().getSelectedUserLocation().getId());

        requestData.setAirlineEDI(true);

        log.info("Request Data Before Sending " + requestData);

        rabbitMqSendMessage.sendtoMQ(requestData, SaaSUtil.getSaaSId());

        log.info("Trying to get the consol by UID : " + airlineEdi.getMasterShipmentUid());

        Consol consol = consolRepository.findByConsolUid(airlineEdi.getMasterShipmentUid());
        consol.setAirlineEdi(new AirlineEdi(airlineEdi.getId()));
        consol = consolRepository.save(consol);

        return consol;
    }

    private void afterGet(Consol consol) {

        // making the file null
        /*if (consol.getConsolAttachmentList() != null && consol.getConsolAttachmentList().size() != 0) {
			for (ConsolAttachment attachment : consol.getConsolAttachmentList()) {
				attachment.setFile(null);
			}
		}*/

        if (consol.getShipmentLinkList() != null && consol.getShipmentLinkList().size() != 0) {
            for (ShipmentLink link : consol.getShipmentLinkList()) {
                link.getService().touch();
            }
        }
    }

    private AirlineEdi copyEdiFromConsol(Consol consol, Boolean isAgentChange) {

        AirlineEdi airlineEdi = new AirlineEdi();

        if (consol.getAirlineEdi() != null) {
            log.info("Airline Edi is already generated for this consol.");
            airlineEdi = consol.getAirlineEdi();
            airlineEdi.setConnectionList(new ArrayList<>());
            airlineEdi.setDimensionList(new ArrayList<>());
            airlineEdi.setHouseList(new ArrayList<>());
        }

        airlineEdi.setEdiGenerateDate(TimeUtil.getCurrentLocationTime());

        airlineEdi.setMasterShipmentUid(consol.getConsolUid());
        airlineEdi.setMawbNo(consol.getConsolDocument().getMawbNo());
        airlineEdi.setAwbDate(consol.getConsolDocument().getMawbGeneratedDate());
        airlineEdi.setMasterDate(consol.getMasterDate());
		
		/* Routing and Carrier Info */
        airlineEdi.setOrigin(consol.getOrigin());
        airlineEdi.setPol(consol.getPol());
        airlineEdi.setPod(consol.getPod());
        airlineEdi.setDestination(consol.getDestination());
        airlineEdi.setCarrierMaster(consol.getCarrier());
        airlineEdi.setFlightNumber(consol.getConsolDocument().getRouteNo());
        airlineEdi.setDirectShipment(consol.getDirectShipment());
        if (consol.getConnectionList() != null && consol.getConnectionList().size() > 0) {
            for (ConsolConnection connection : consol.getConnectionList()) {

                AirlineEdiConnection ediConnection = new AirlineEdiConnection();

                ediConnection.setPol(connection.getPol());
                ediConnection.setPod(connection.getPod());
                ediConnection.setEta(connection.getEta());
                ediConnection.setEtd(connection.getEtd());
                ediConnection.setCarrierMaster(connection.getCarrierMaster());
                ediConnection.setFlightNumber(connection.getFlightVoyageNo());
                ediConnection.setAirlineEdi(airlineEdi);

                airlineEdi.getConnectionList().add(ediConnection);
            }
        }


        if (consol.getDirectShipment().equals(YesNo.Yes)) {

            if (isAgentChange) {

                airlineEdi.setShipper(consol.getConsolDocument().getShipper());
                airlineEdi.setShipperAddress(assignPartyAddress(consol.getConsolDocument().getShipper()));

                airlineEdi.setConsignee(consol.getConsolDocument().getConsignee());
                airlineEdi.setConsigneeAddress(assignPartyAddress(consol.getConsolDocument().getConsignee()));

                airlineEdi.setAgent(consol.getConsolDocument().getConsignee());
                airlineEdi.setAgentAddress(assignPartyAddress(consol.getConsolDocument().getConsignee()));
                ValidateUtil.notNull(consol.getConsolDocument().getConsignee(), ErrorCode.AIRLINE_EDI_IATA_CONS_REQUIRED);
                airlineEdi.setAgentIataCode(consol.getConsolDocument().getConsignee().getPartyDetail().getIataCode());
            } else {
                if (consol.getConsolDocument().getShipper() != null && consol.getConsolDocument().getShipper().getId() != null) {
                    airlineEdi.setShipper(consol.getConsolDocument().getShipper());
                    airlineEdi.setShipperAddress(assignPartyAddress(consol.getConsolDocument().getShipper()));
                }
                ValidateUtil.notNull(consol.getConsolDocument().getConsignee(), ErrorCode.AIRLINE_EDI_CONSIGNEE_REQUIRED);
                ValidateUtil.notNull(consol.getConsolDocument().getConsignee().getId(), ErrorCode.AIRLINE_EDI_CONSIGNEE_REQUIRED);

                ValidateUtil.notNull(consol.getConsolDocument().getAgent(), ErrorCode.AIRLINE_EDI_AGENT_REQUIRED);
                ValidateUtil.notNull(consol.getConsolDocument().getAgent().getId(), ErrorCode.AIRLINE_EDI_AGENT_REQUIRED);

                if (consol.getConsolDocument().getConsignee() != null && consol.getConsolDocument().getConsignee().getId() != null) {
                    airlineEdi.setConsignee(consol.getConsolDocument().getConsignee());
                    airlineEdi.setConsigneeAddress(assignPartyAddress(consol.getConsolDocument().getConsignee()));

                    airlineEdi.setAgent(consol.getConsolDocument().getAgent());
                    airlineEdi.setAgentAddress(assignPartyAddress(consol.getConsolDocument().getAgent()));
                    ValidateUtil.notNull(consol.getConsolDocument().getAgent().getPartyDetail().getIataCode(), ErrorCode.AIRLINE_EDI_IATA_AGENT_REQUIRED);
                    airlineEdi.setAgentIataCode(consol.getConsolDocument().getAgent().getPartyDetail().getIataCode());
                }
            }

        } else {
//			ValidateUtil.notNull(consol.getConsolDocument().getIssuingAgent(), ErrorCode.AIRLINE_EDI_ISSAGENT_REQUIRED);
//			ValidateUtil.notNull(consol.getConsolDocument().getIssuingAgent().getId(), ErrorCode.AIRLINE_EDI_ISSAGENT_REQUIRED);
            if (consol.getConsolDocument().getIssuingAgent() != null && consol.getConsolDocument().getIssuingAgent().getId() != null) {
                airlineEdi.setShipper(consol.getConsolDocument().getIssuingAgent());
                airlineEdi.setShipperAddress(assignPartyAddress(consol.getConsolDocument().getIssuingAgent()));
            }


            // if issuing agent is not there, assigning shipper as shipper (As Discussed 08.08.2017)
            if (airlineEdi.getShipper() == null) {
                airlineEdi.setShipper(consol.getConsolDocument().getShipper());
                airlineEdi.setShipperAddress(assignPartyAddress(consol.getConsolDocument().getShipper()));
            }

            ValidateUtil.notNull(consol.getConsolDocument().getAgent(), ErrorCode.AIRLINE_EDI_AGENT_REQUIRED);
            ValidateUtil.notNull(consol.getConsolDocument().getAgent().getId(), ErrorCode.AIRLINE_EDI_AGENT_REQUIRED);
            if (consol.getConsolDocument().getAgent() != null && consol.getConsolDocument().getAgent().getId() != null) {
                airlineEdi.setConsignee(consol.getConsolDocument().getAgent());
                airlineEdi.setConsigneeAddress(assignPartyAddress(consol.getConsolDocument().getAgent()));
                airlineEdi.setAgent(consol.getConsolDocument().getAgent());
                airlineEdi.setAgentAddress(assignPartyAddress(consol.getConsolDocument().getAgent()));
                ValidateUtil.notNull(consol.getConsolDocument().getAgent().getPartyDetail().getIataCode(), ErrorCode.AIRLINE_EDI_IATA_AGENT_REQUIRED);
                airlineEdi.setAgentIataCode(consol.getConsolDocument().getAgent().getPartyDetail().getIataCode());
            }
        }
        airlineEdi.setCommodityMaster(consol.getConsolDocument().getCommodity());
        airlineEdi.setCommodityDescription(consol.getConsolDocument().getCommodityDescription());
        airlineEdi.setCurrencyMaster(consol.getCurrency());
        airlineEdi.setPpcc(consol.getPpcc());
        airlineEdi.setRateClass(consol.getConsolDocument().getRateClass());

		/* Packs & Dimensions */
        airlineEdi.setNoOfPieces(consol.getConsolDocument().getNoOfPieces());
        airlineEdi.setVolumeWeight(consol.getConsolDocument().getVolumeWeight());
        airlineEdi.setGrossWeight(consol.getConsolDocument().getGrossWeight());
        airlineEdi.setChargebleWeight(consol.getConsolDocument().getChargebleWeight());
        airlineEdi.setDimensionUnit(consol.getConsolDocument().getDimensionUnit());
        airlineEdi.setDimensionUnitValue(consol.getConsolDocument().getDimensionUnitValue());
        airlineEdi.setDimNoOfPieces(consol.getConsolDocument().getDimNoOfPieces());
        airlineEdi.setDimGrossWeight(consol.getConsolDocument().getDimGrossWeight());
        airlineEdi.setDimGrossWeightInPound(consol.getConsolDocument().getDimGrossWeightInPound());
        airlineEdi.setDimVolumeWeight(consol.getConsolDocument().getDimVolumeWeight());
        airlineEdi.setDimVolumeWeightInPound(consol.getConsolDocument().getDimVolumeWeightInPound());

        if (consol.getConsolDocument().getDimensionList() != null && consol.getConsolDocument().getDimensionList().size() > 0) {
            for (ConsolDocumentDimension documentDimension : consol.getConsolDocument().getDimensionList()) {
                AirlineEdiDimension dimension = new AirlineEdiDimension();
                dimension.setNoOfPiece(documentDimension.getNoOfPiece());
                dimension.setLength(documentDimension.getLength());
                dimension.setWidth(documentDimension.getWidth());
                dimension.setHeight(documentDimension.getHeight());
                dimension.setVolWeight(documentDimension.getVolWeight());
                dimension.setGrossWeight(documentDimension.getGrossWeight());
                dimension.setGrossWeightKg(documentDimension.getGrossWeightKg());
                dimension.setAirlineEdi(airlineEdi);
                airlineEdi.getDimensionList().add(dimension);
            }
        }

        if (consol.getShipmentLinkList() != null && consol.getShipmentLinkList().size() > 0) {
            for (ShipmentLink shipmentLink : consol.getShipmentLinkList()) {

                AirlineEdiHouse house = new AirlineEdiHouse();
                house.setShipmentUid(shipmentLink.getService().getShipmentUid());
                house.setServiceUid(shipmentLink.getService().getServiceUid());
                house.setOrigin(shipmentLink.getService().getOrigin());
                house.setDestination(shipmentLink.getService().getDestination());
                house.setShipper(shipmentLink.getService().getShipper());
                house.setShipperAddress(assignPartyAddress(shipmentLink.getService().getShipper()));
                house.setConsignee(shipmentLink.getService().getConsignee());
                house.setConsigneeAddress(assignPartyAddress(shipmentLink.getService().getConsignee()));
                house.setAgent(shipmentLink.getService().getAgent());
                house.setAgentAddress((assignPartyAddress(shipmentLink.getService().getAgent())));
                house.setTosMaster(shipmentLink.getService().getTosMaster());
                house.setCommodityMaster(shipmentLink.getService().getCommodity());
                house.setExternalNumberOfPieces(shipmentLink.getService().getBookedPieces());
                house.setVolumeWeight(shipmentLink.getService().getBookedVolumeWeightUnitKg());
                house.setGrossWeight(shipmentLink.getService().getBookedGrossWeightUnitKg());
                house.setChargebleWeight(shipmentLink.getService().getBookedChargeableUnit());
                house.setDocumentNo(shipmentLink.getService().getHawbNo());
                house.setHawbNo(shipmentLink.getService().getDocumentList().get(0).getHawbNo());
                house.setCommodityDescription(shipmentLink.getService().getDocumentList().get(0).getCommodityDescription());
                house.setAirlineEdi(airlineEdi);
                airlineEdi.getHouseList().add(house);
            }
        }


        if (consol.getConsolDocument().getDeclaredValueForCarriage() != null && consol.getConsolDocument().getDeclaredValueForCarriage().length() != 0) {
            airlineEdi.setValueForCarriage(consol.getConsolDocument().getDeclaredValueForCarriage());
        } else {
            airlineEdi.setValueForCarriage("NVD");
        }

        if (consol.getConsolDocument().getDeclaredValueForCustoms() != null && consol.getConsolDocument().getDeclaredValueForCustoms().length() != 0) {
            airlineEdi.setValueForCustomDeclarations(consol.getConsolDocument().getDeclaredValueForCustoms());
        } else {
            airlineEdi.setValueForCustomDeclarations("NCV");
        }

        airlineEdi.setValueForInsurance("XXX");
        airlineEdi.setHandlingInformation(consol.getConsolDocument().getHandlingInformation());

        airlineEdiValidator.validate(airlineEdi);

        return airlineEdi;
    }

    private AddressMaster assignPartyAddress(PartyMaster partyMaster) {
        if (partyMaster != null && partyMaster.getPartyAddressList() != null && !partyMaster.getPartyAddressList().isEmpty()) {
            if (partyMaster.getPartyAddressList().size() == 1) {
                return getAddressMasterFromPartyAddress(partyMaster.getPartyAddressList().get(0), partyMaster);
            } else {
                for (PartyAddressMaster partyAddressMaster : partyMaster.getPartyAddressList()) {
                    if (partyAddressMaster.getAddressType().equals(AddressType.Primary)) {
                        return getAddressMasterFromPartyAddress(partyAddressMaster, partyMaster);
                    }
                }
            }
        }
        return null;
    }

    private AddressMaster getAddressMasterFromPartyAddress(PartyAddressMaster partyAddressMaster, PartyMaster partyMaster) {

        AddressMaster addressMaster = new AddressMaster();

        addressMaster.setAddressLine1(partyAddressMaster.getAddressLine1());
        addressMaster.setAddressLine2(partyAddressMaster.getAddressLine2());
        addressMaster.setAddressLine3(partyAddressMaster.getAddressLine3());

        addressMaster.setCountry(partyMaster.getCountryMaster());
        addressMaster.setCity(partyAddressMaster.getCityMaster());
        addressMaster.setState(partyAddressMaster.getStateMaster());

        addressMaster.setEmail(partyAddressMaster.getEmail());
        addressMaster.setMobile(partyAddressMaster.getMobileNo());
        addressMaster.setPhone(partyAddressMaster.getPhone());

        return addressMaster;
    }

}
