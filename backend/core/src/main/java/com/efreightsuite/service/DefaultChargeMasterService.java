package com.efreightsuite.service;

import java.io.ByteArrayInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.DefaultChargeRequestDto;
import com.efreightsuite.dto.FileUploadDto;
import com.efreightsuite.dto.PricingMasterDto;
import com.efreightsuite.dto.PricingMasterListDto;
import com.efreightsuite.enumeration.PPCC;
import com.efreightsuite.enumeration.WhoRouted;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.ExceptionHelper;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.model.ChargeMaster;
import com.efreightsuite.model.CurrencyMaster;
import com.efreightsuite.model.DefaultChargeMaster;
import com.efreightsuite.model.PortMaster;
import com.efreightsuite.model.PricingPortPair;
import com.efreightsuite.model.ServiceMaster;
import com.efreightsuite.model.TosMaster;
import com.efreightsuite.model.UnitMaster;
import com.efreightsuite.repository.CarrierMasterRepository;
import com.efreightsuite.repository.ChargeMasterRepository;
import com.efreightsuite.repository.CommodityMasterRepository;
import com.efreightsuite.repository.CurrencyMasterRepository;
import com.efreightsuite.repository.DefaultChargeMasterRepository;
import com.efreightsuite.repository.DefaultMasterDataRepository;
import com.efreightsuite.repository.DivisionMasterRepository;
import com.efreightsuite.repository.PartyMasterRepository;
import com.efreightsuite.repository.PortMasterRepository;
import com.efreightsuite.repository.ServiceMasterRepository;
import com.efreightsuite.repository.TosMasterRepository;
import com.efreightsuite.repository.UnitMasterRepository;
import com.efreightsuite.search.DefaultChargeSearchImpl;
import com.efreightsuite.util.AppUtil;
import com.efreightsuite.util.CSVFileConverter;
import com.efreightsuite.util.ValidateUtil;
import com.efreightsuite.validation.DefaultChargeMasterValidator;
import com.ibm.icu.text.SimpleDateFormat;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.lang3.exception.ExceptionUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class DefaultChargeMasterService {

    private static final String[] FILE_HEADER_MAPPING = {"ServiceCode", "ChargeCode", "CurrencyCode", "UnitCode"};
    private static final String SERVICE_CODE = "ServiceCode";
    private static final String CHARGE_CODE = "ChargeCode";
    private static final String CURRENCY_CODE = "CurrencyCode";
    private static final String UNIT_CODE = "UnitCode";
    private static final String AMOUNT_UNIT = "Amount";
    private static final String std_Sel_In_Minimum = "Revenu Min Amount";
    private static final String std_Sel_In_Amount = "Revenue Amount per unit";
    private static final String cost_In_Minimum = "Default Min Amount";
    private static final String cost_In_Amount = "Defalut amount per Unit";
    private static final String ORIGIN = "Origin";
    private static final String TRANIST_PORT = "Transit Port";
    private static final String DESTINATION = "Destination";
    private static final String TOS = "Tos";
    private static final String PREPAID_COLLECT = "Prepaid/Collect";
    private static final String SELF_AGENT = "Self/Agent";
    private static final String VALID_FROM = "Valid From";
    private static final String VALID_TO = "Valid To";

    //CSV file header
    private static final String DIVISION = "Division";
    private static final String IS_IMCO = "Is Imco";
    private static final String IS_CLEARANCE = "Is Clearance";
    private static final String FORWARDER_DIRECTOR = "Forwarder/Director";
    private static final String AGENT = "Agent Name";
    private static final String PERSONAL_EFFECT = "Personal Effect";
    private static final String IS_COLOAD = "Is Colaod";
    private static final String IS_TRANSIT = "Is Transit";
    private static final String IS_OUR_TRANSPORT = "Our Transport";
    private static final String COMMODITY_GROUP = "Commodity Code";
    private static final String CARRIER_CODE = "Carrier Code";
    private static final String SHIPPER = "Shipper Name";
    private static final String CONSIGNEE = "Consignee Name";
    @Autowired
    private
    AppUtil appUtil;
    @Autowired
    private
    DefaultChargeSearchImpl defaultChargeImpl;
    @Autowired
    private
    DefaultChargeMasterValidator defaultChargeValidator;
    @Autowired
    private
    DefaultChargeMasterRepository defaultChargeMasterRepository;
    @Autowired
    private
    ServiceMasterRepository serviceMasterRepository;
    @Autowired
    private
    ChargeMasterRepository chargeMasterRepository;
    @Autowired
    private
    CurrencyMasterRepository currencyMasterRepository;
    @Autowired
    private
    UnitMasterRepository unitMasterRepository;
    @Autowired
    private
    PortMasterRepository portMasterRepository;
    @Autowired
    PartyMasterRepository partyMasterRepository;
    @Autowired
    CarrierMasterRepository carrierMasterRepository;
    @Autowired
    CommodityMasterRepository commodityMasterRepository;
    @Autowired
    DivisionMasterRepository divsioMasterRepository;
    @Autowired
    private
    TosMasterRepository tosMasterRepository;
    @Autowired
    DefaultMasterDataRepository defaultMasterDataRepository;
    @Autowired
    PricingAirService pricingAirService;
    @Autowired
    FileUploadService fileUploadService;
    @Value("${efs.resource.path}")
    String appResourcePath;

    public BaseDto search(DefaultChargeRequestDto dcDto) {

        log.info("serach method called in default charge");

        BaseDto baseDto = new BaseDto();
        try {

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(defaultChargeImpl.search(dcDto));

        } catch (Exception e) {

            log.error("Exception in Default Master service -> search method ", e);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);
    }


    public BaseDto createUpdate(List<DefaultChargeMaster> defaultList) {
        BaseDto baseDto = new BaseDto();
        try {
            ValidateUtil.notNull(defaultList, ErrorCode.DEFAULT_CHARGE_DATA_MANDATORY);

            for (DefaultChargeMaster dcm : defaultList) {
                defaultChargeValidator.validate(dcm);
            }
            defaultChargeMasterRepository.save(defaultList);
            baseDto.setResponseCode(ErrorCode.SUCCESS);
        } catch (Exception e) {
            log.info("exception in create/update default charge creation", e);
            baseDto.setResponseCode(e.getMessage());
        }
        return appUtil.setDesc(baseDto);
    }


    public BaseDto delete(Long id) {
        log.info("Delete method is Invoked...[" + id + "]");

        BaseDto baseDto = new BaseDto();

        try {

            ValidateUtil.notNull(id, ErrorCode.DEFAULT_CHARGE_DATA_MANDATORY);

            defaultChargeMasterRepository.delete(id);

            baseDto.setResponseCode(ErrorCode.SUCCESS);

        } catch (DataIntegrityViolationException exception) {

            log.error("Exception occured : ", exception);

            ExceptionHelper helper = new ExceptionHelper(exception);

            log.error("Foreign key reference : " + helper.getFkReference());

            baseDto.setResponseCode(ErrorCode.ERROR_FK_CONSTRAINT);

        } catch (Exception exception) {

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            log.error("Exception in Delete method : ", exception);

            String rootCause = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Root Cause of Exception : " + rootCause);

            if (rootCause.contains("FK_")) {
                baseDto.setResponseCode(ErrorCode.CANNOT_DELETE_FK_RECORD);
            }
        }

        return appUtil.setDesc(baseDto);
    }


    public BaseDto get(Long id) {
        BaseDto baseDto = new BaseDto();

        try {

            ValidateUtil.notNull(id, ErrorCode.DEFAULT_CHARGE_DATA_MANDATORY);

            DefaultChargeMaster defaultMaster = defaultChargeMasterRepository.getOne(id);

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(defaultMaster);

        } catch (RestException ex) {

            log.error("Exception in get method ", ex);

            baseDto.setResponseCode(ex.getMessage());

        } catch (Exception e) {

            log.error("Exception in get method ", e);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);
    }


    public BaseDto uploadInPricing(FileUploadDto fileUploadDto) {

        BaseDto baseDto = new BaseDto();

        //FileReader fileReader = null;

        CSVParser csvFileParser = null;

        SimpleDateFormat dateFormat = new SimpleDateFormat(
                AuthService.getCurrentUser().getSelectedUserLocation().getJavaDateFormat());

        PricingMasterListDto pricingMasterListDto = new PricingMasterListDto();

        List<PricingMasterDto> dcList = new ArrayList<>();

        try {

            Scanner scanner = new Scanner(new ByteArrayInputStream(fileUploadDto.getFile()));

            List<String> records = CSVFileConverter.parseLine(scanner.nextLine());

            CSVFormat csvFileFormat = CSVFormat.DEFAULT.withHeader(FILE_HEADER_MAPPING);

            if (records != null && records.get(0).contains(";")) {
                csvFileFormat = CSVFormat.newFormat(';');
            } else {

            }
            csvFileParser = new CSVParser(new StringReader(new String(fileUploadDto.getFile())), csvFileFormat);

            List<CSVRecord> csvRecords = csvFileParser.getRecords();

            if (csvRecords.size() >= 2) {

                for (int i = 1; i < csvRecords.size(); i++) {

                    PricingMasterDto pricingMasterDto = new PricingMasterDto();

                    PricingPortPair pricingPortPair = new PricingPortPair();

                    CSVRecord record = csvRecords.get(i);


                    if (record != null) {

                        if (record.get(0) != null && record.get(0).trim() != null && record.get(0).length() > 0) {

                            ServiceMaster sm = serviceMasterRepository.findByServiceCode(record.get(0).trim());
                            if (sm != null) {
                                pricingPortPair.setServiceMaster(sm);
                            } else {
                                baseDto.getParams().add(record.get(0));
                                baseDto.getParams().add(String.valueOf(i));
                                baseDto.getParams().add(SERVICE_CODE);
                                baseDto.getParams().add(record.get(SERVICE_CODE));
                                throw new RestException(ErrorCode.CSV_FILE_INVALID);
                            }

                        } else {
                            baseDto.getParams().add(record.get(0));
                            baseDto.getParams().add(String.valueOf(i));
                            baseDto.getParams().add(SERVICE_CODE);
                            baseDto.getParams().add(record.get(SERVICE_CODE));
                            throw new RestException(ErrorCode.CSV_FILE_INVALID);
                        }


                        if (record.get(1) != null && record.get(1).trim() != null && record.get(1).length() > 0) {

                            ChargeMaster cm = chargeMasterRepository.findByChargeCode(record.get(1).trim());
                            if (cm != null) {
                                pricingPortPair.setChargeMaster(cm);
                            } else {
                                baseDto.getParams().add(record.get(1));
                                baseDto.getParams().add(String.valueOf(i));
                                baseDto.getParams().add(CHARGE_CODE);
                                baseDto.getParams().add(record.get(CHARGE_CODE));
                                throw new RestException(ErrorCode.CSV_FILE_INVALID);
                            }
                        } else {
                            baseDto.getParams().add(record.get(1));
                            baseDto.getParams().add(String.valueOf(i));
                            baseDto.getParams().add(CHARGE_CODE);
                            baseDto.getParams().add(record.get(CHARGE_CODE));
                            throw new RestException(ErrorCode.CSV_FILE_INVALID);
                        }


                        if (record.get(2) != null && record.get(2).trim() != null && record.get(2).length() > 0) {

                            CurrencyMaster currmas = currencyMasterRepository.findByCurrencyCode(record.get(2).trim());

                            if (currmas != null) {
                                pricingPortPair.setCurrencyMaster(currmas);
                            } else {
                                baseDto.getParams().add(record.get(2));
                                baseDto.getParams().add(String.valueOf(i));
                                baseDto.getParams().add(CURRENCY_CODE);
                                baseDto.getParams().add(record.get(CURRENCY_CODE));
                                throw new RestException(ErrorCode.CSV_FILE_INVALID);
                            }

                        } else {

                            baseDto.getParams().add(record.get(2));
                            baseDto.getParams().add(String.valueOf(i));
                            baseDto.getParams().add(CURRENCY_CODE);
                            baseDto.getParams().add(record.get(CURRENCY_CODE));
                            throw new RestException(ErrorCode.CSV_FILE_INVALID);

                        }


                        if (record.get(3) != null && record.get(3).trim() != null && record.get(3).length() > 0) {
                            UnitMaster um = unitMasterRepository.findByUnitCode(record.get(3).trim());
                            if (um != null) {
                                pricingPortPair.setUnitMaster(um);
                            } else {
                                baseDto.getParams().add(record.get(3));
                                baseDto.getParams().add(String.valueOf(i));
                                baseDto.getParams().add(UNIT_CODE);
                                baseDto.getParams().add(record.get(UNIT_CODE));
                                throw new RestException(ErrorCode.CSV_FILE_INVALID);
                            }
                        } else {

                            baseDto.getParams().add(record.get(3));
                            baseDto.getParams().add(String.valueOf(i));
                            baseDto.getParams().add(UNIT_CODE);
                            baseDto.getParams().add(record.get(UNIT_CODE));
                            throw new RestException(ErrorCode.CSV_FILE_INVALID);
                        }


                        if (record.get(4) != null && record.get(4).trim() != null && record.get(4).length() > 0) {
                            pricingPortPair.setStdSelInMinimum(Double.parseDouble(record.get(4).trim()));
                        } else {/*
                            baseDto.getParams().add(record.get(4));
							baseDto.getParams().add(String.valueOf(i));
							baseDto.getParams().add(std_Sel_In_Minimum);
							baseDto.getParams().add(record.get(std_Sel_In_Minimum));
							throw new RestException(ErrorCode.CSV_FILE_INVALID);
						*/
                        }

                        if (record.get(5) != null && record.get(5).trim() != null && record.get(5).length() > 0) {
                            pricingPortPair.setStdSelInAmount(Double.parseDouble(record.get(5).trim()));
                        } else {
                            baseDto.getParams().add(record.get(5));
                            baseDto.getParams().add(String.valueOf(i));
                            baseDto.getParams().add(std_Sel_In_Amount);
                            baseDto.getParams().add(record.get(std_Sel_In_Amount));
                            throw new RestException(ErrorCode.CSV_FILE_INVALID);
                        }


                        if (record.get(6) != null && record.get(6).trim() != null && record.get(6).length() > 0) {
                            pricingPortPair.setCostInMinimum(Double.parseDouble(record.get(6).trim()));
                        } else {/*
							baseDto.getParams().add(record.get(6));
							baseDto.getParams().add(String.valueOf(i));
							baseDto.getParams().add(cost_In_Minimum);
							baseDto.getParams().add(record.get(cost_In_Minimum));
							throw new RestException(ErrorCode.CSV_FILE_INVALID);
						*/
                        }

                        if (record.get(7) != null && record.get(7).trim() != null && record.get(7).length() > 0) {
                            pricingPortPair.setCostInAmount(Double.parseDouble(record.get(7).trim()));
                        } else {
                            baseDto.getParams().add(record.get(7));
                            baseDto.getParams().add(String.valueOf(i));
                            baseDto.getParams().add(cost_In_Amount);
                            baseDto.getParams().add(record.get(cost_In_Amount));
                            throw new RestException(ErrorCode.CSV_FILE_INVALID);
                        }


                        if (record.get(8) != null && record.get(8).trim() != null && record.get(8).length() > 0) {
                            PortMaster pm = portMasterRepository.findByPortCode(record.get(8).trim());
                            if (pm != null) {
                                pricingMasterDto.setOrigin(pm);
                            } else {
                                baseDto.getParams().add(record.get(8));
                                baseDto.getParams().add(String.valueOf(i));
                                baseDto.getParams().add(ORIGIN);
                                baseDto.getParams().add(record.get(ORIGIN));
                                throw new RestException(ErrorCode.CSV_FILE_INVALID);

                            }
                        }

                        if (record.get(9) != null && record.get(9).trim() != null && record.get(9).length() > 0) {
                            PortMaster tpm = portMasterRepository.findByPortCode(record.get(9).trim());
                            if (tpm != null) {
                                pricingMasterDto.setTransit(tpm);
                            } else {
                                baseDto.getParams().add(record.get(9));
                                baseDto.getParams().add(String.valueOf(i));
                                baseDto.getParams().add(TRANIST_PORT);
                                baseDto.getParams().add(record.get(TRANIST_PORT));
                                throw new RestException(ErrorCode.CSV_FILE_INVALID);

                            }
                        }

                        if (record.get(10) != null && record.get(10).trim() != null && record.get(10).length() > 0) {
                            PortMaster dpm = portMasterRepository.findByPortCode(record.get(10).trim());
                            if (dpm != null) {
                                pricingMasterDto.setDestination(dpm);
                            } else {
                                baseDto.getParams().add(record.get(10));
                                baseDto.getParams().add(String.valueOf(i));
                                baseDto.getParams().add(DESTINATION);
                                baseDto.getParams().add(record.get(DESTINATION));
                                throw new RestException(ErrorCode.CSV_FILE_INVALID);

                            }
                        }

                        if (record.get(11) != null && record.get(11).trim() != null && record.get(11).length() > 0) {
                            TosMaster tm = tosMasterRepository.findByTosCode(record.get(11).trim());
                            if (tm != null) {
                                pricingPortPair.setTosMaster(tm);
                            } else {
                                baseDto.getParams().add(record.get(11));
                                baseDto.getParams().add(String.valueOf(i));
                                baseDto.getParams().add(TOS);
                                baseDto.getParams().add(record.get(TOS));
                                throw new RestException(ErrorCode.CSV_FILE_INVALID);
                            }
                        }

                        if (record.get(12) != null && record.get(12).trim() != null && record.get(12).length() > 0) {

                            if (record.get(12).trim().equals(PPCC.Prepaid.toString())) {

                                pricingPortPair.setPpcc(PPCC.Prepaid);

                            } else if (record.get(12).trim().equals(PPCC.Collect.toString())) {
                                pricingPortPair.setPpcc(PPCC.Collect);
                            } else {
                                baseDto.getParams().add(record.get(12));
                                baseDto.getParams().add(String.valueOf(i));
                                baseDto.getParams().add(PREPAID_COLLECT);
                                baseDto.getParams().add(record.get(PREPAID_COLLECT));
                                throw new RestException(ErrorCode.CSV_FILE_INVALID);
                            }
                        }

                        if (record.get(13) != null && record.get(13).trim() != null && record.get(13).length() > 0) {

                            if (record.get(13).trim().equals(WhoRouted.Self.toString())) {

                                pricingMasterDto.setWhoRouted(WhoRouted.Self);

                            } else if (record.get(13).trim().equals(WhoRouted.Agent.toString())) {
                                pricingMasterDto.setWhoRouted(WhoRouted.Agent);
                            } else {
                                baseDto.getParams().add(record.get(13));
                                baseDto.getParams().add(String.valueOf(i));
                                baseDto.getParams().add(SELF_AGENT);
                                baseDto.getParams().add(record.get(SELF_AGENT));
                                throw new RestException(ErrorCode.CSV_FILE_INVALID);
                            }
                        } else {
                            pricingMasterDto.setWhoRouted(WhoRouted.Self);
                        }

					/*	if (record.get(14)!=null && record.get(14).trim() != null && record.get(14).length()>0) {

							try {
								pricingPortPair.setValidFromDate(dateFormat.parse(record.get(14)));

							} catch (Exception exception) {
								baseDto.getParams().add(record.get(14));
								baseDto.getParams().add(String.valueOf(i));
								baseDto.getParams().add(VALID_FROM);
								baseDto.getParams().add(record.get(VALID_FROM));
								throw new RestException(ErrorCode.CSV_FILE_INVALID);
							}
						}else{
							//logic to set todays date
						}

						if (record.get(15)!=null && record.get(15).trim() != null && record.get(15).length()>0) {

							try {
								pricingPortPair.setValidToDate(dateFormat.parse(record.get(15)));

							} catch (Exception exception) {
								baseDto.getParams().add(record.get(15));
								baseDto.getParams().add(String.valueOf(i));
								baseDto.getParams().add(record.get(VALID_TO));
								baseDto.getParams().add(record.get(VALID_TO));
								throw new RestException(ErrorCode.CSV_FILE_INVALID);
							}
						}else{
							//logic to set the value from defulte master
						}*/
                    }
                    pricingMasterDto.setPricingPortPairList(new ArrayList<>());
                    pricingMasterDto.getPricingPortPairList().add(pricingPortPair);
                    dcList.add(pricingMasterDto);
                    pricingMasterListDto.setPricingMasterList(new ArrayList<>());
                    pricingMasterListDto.getPricingMasterList().addAll(dcList);

                }
            } else {
                throw new RestException(ErrorCode.DEFAULT_CHARGE_ATLEAST_ONE_ROW);
            }
            baseDto.setResponseCode(ErrorCode.SUCCESS);
            baseDto.setResponseObject(dcList);
        } catch (FileNotFoundException exception) {
            log.info("File Not Found Exception occurred while converting ", exception);
            baseDto.setResponseCode(ErrorCode.CARRIER_RATE_CSV_FILE_REQUIRED);
        } catch (RestException exception) {
            log.info("Exception occured in upload method ", exception);
            baseDto.setResponseCode(exception.getMessage());
        } catch (Exception e) {
            log.info("Exception occured in upload method ", e);
            baseDto.setResponseCode(ErrorCode.CSV_FILE_INVALID);
        } finally {

            try {
                //fileReader.close();
                csvFileParser.close();
            } catch (IOException e) {
                e.printStackTrace();
                log.info("exception in file close", e);
            }

        }
        return appUtil.setDesc(baseDto);

    }

}
