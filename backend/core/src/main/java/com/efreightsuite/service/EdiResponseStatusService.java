package com.efreightsuite.service;

import java.util.List;

import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.model.EdiResponseStatus;
import com.efreightsuite.repository.EdiResponseStatusRepository;
import com.efreightsuite.util.AppUtil;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class EdiResponseStatusService {

    @Autowired
    private
    AppUtil appUtil;

    @Autowired
    private
    EdiResponseStatusRepository ediResponseStatusRepository;

    public BaseDto getAllByConsolId(Long consolId) {

        log.info("EdiResponseStatusService.getAllByConsolId method is Invoked....[" + consolId + "]");

        BaseDto baseDto = new BaseDto();

        try {

            List<EdiResponseStatus> ediResponseStatusList = ediResponseStatusRepository.findAllByConsolId(consolId);


            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(ediResponseStatusList);

        } catch (Exception exception) {

            log.error("Exception in EdiResponseStatusService.getAllByConsolId method", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);
    }

}
