package com.efreightsuite.service;

import java.util.List;

import com.efreightsuite.common.component.CacheRepository;
import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.DocumentSearchDto;
import com.efreightsuite.dto.SearchRequest;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.exception.UniqueKey;
import com.efreightsuite.model.DocumentMaster;
import com.efreightsuite.repository.DocumentMasterRepository;
import com.efreightsuite.search.DocumentSearchImpl;
import com.efreightsuite.util.AppUtil;
import com.efreightsuite.util.SaaSUtil;
import com.efreightsuite.util.ValidateUtil;
import com.efreightsuite.validation.DocumentMasterValidator;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.exception.ExceptionUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.ObjectOptimisticLockingFailureException;
import org.springframework.orm.jpa.JpaObjectRetrievalFailureException;
import org.springframework.stereotype.Service;

/**
 * @author achyu Date - 23.06.2016 Updated - 23.06.2016
 */

@Service
@Log4j2
public class DocumentMasterService {

    @Autowired
    private
    AppUtil appUtil;

    /**
     * DocumentMasterRepository Object
     */

    @Autowired
    private
    DocumentMasterRepository documentMasterRepository;

    @Autowired
    private
    DocumentMasterValidator documentMasterValidator;

    @Autowired
    private
    CacheRepository cacheRepository;

    @Autowired
    private
    DocumentSearchImpl documentSearchImpl;


    public BaseDto getByDocumentId(Long id) {

        log.info("getByDocumentId method is called...[" + id + "]");

        BaseDto baseDto = new BaseDto();

        try {

            DocumentMaster documentMaster = cacheRepository.get(SaaSUtil.getSaaSId(), DocumentMaster.class.getName(), id, documentMasterRepository);

            ValidateUtil.notNull(documentMaster, ErrorCode.DOCUMENT_ID_NOT_FOUND);

            log.info("getByDocumentId is successfully executed....[" + documentMaster.getId() + "]");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(documentMaster);

        } catch (RestException exception) {

            log.error("Exception in getByDocumentId method : ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (Exception exception) {

            log.error("Exception in getByDocumentId method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);
    }


    public BaseDto search(DocumentSearchDto searchDto) {

        log.info("Search method is called...." + searchDto);

        BaseDto baseDto = new BaseDto();

        try {

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(documentSearchImpl.search(searchDto));

            log.info("Successfully Searching Document...");

        } catch (Exception exception) {

            log.error("Exception in Search method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);

    }

    public BaseDto search(SearchRequest searchRequest) {

        log.info("Search method is called...." + searchRequest);

        BaseDto baseDto = new BaseDto();

        try {

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(documentSearchImpl.search(searchRequest));

            log.info("Successfully Searching Document...");

        } catch (Exception exception) {

            log.error("Exception in Search method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);

    }


    public BaseDto create(List<DocumentMaster> documentMasterList) {

        log.info("Create method is called - documentMasterId ..");

        final BaseDto baseDto = new BaseDto();

        try {

            for (DocumentMaster documentMaster : documentMasterList) {
                documentMasterValidator.validate(documentMaster);
                documentMaster.setDocumentCode(documentMaster.getDocumentCode().toUpperCase());
            }

            cacheRepository.save(SaaSUtil.getSaaSId(), DocumentMaster.class.getName(), documentMasterList, documentMasterRepository);

            log.info("Document Saved successfully.....");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(documentMasterList);

        } catch (RestException exception) {

            log.error("Exception Cause ", exception);

            baseDto.setResponseCode(exception.getMessage());


        } catch (Exception exception) {

            log.error("Exception occured : ", exception);

            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Exception Cause 1 : " + exceptionCause1);

            // General error condition
            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            // Duplicate document Code

            if (exceptionCause1.contains(UniqueKey.UK_DOCUMENT_DOCUMENTCODE)) {
                baseDto.setResponseCode(ErrorCode.DOCUMENT_CODE_ALREADY_EXIST);

            }

            if (exceptionCause1.contains(UniqueKey.UK_DOCUMENT_DOCUMENTNAME)) {
                baseDto.setResponseCode(ErrorCode.DOCUMENT_NAME_ALREADY_EXIST);
            }

        }

        return appUtil.setDesc(baseDto);
    }


    public BaseDto update(DocumentMaster existingDocumentMaster) {

        log.info("Update method is called - existingDocumentMaster : [" + existingDocumentMaster.getId() + "]");

        BaseDto baseDto = new BaseDto();

        try {

            ValidateUtil.notNull(existingDocumentMaster.getId(), ErrorCode.DOCUMENT_ID_NOT_NULL);

            documentMasterValidator.validate(existingDocumentMaster);

            documentMasterRepository.getOne(existingDocumentMaster.getId());

            existingDocumentMaster.setDocumentCode(existingDocumentMaster.getDocumentCode().toUpperCase());

            existingDocumentMaster = cacheRepository.save(SaaSUtil.getSaaSId(), DocumentMaster.class.getName(), existingDocumentMaster, documentMasterRepository);

            log.info("DocumentMaster updated successfully....[" + existingDocumentMaster.getId() + "]");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(existingDocumentMaster);

        } catch (RestException exception) {

            log.error("Exception occured with error code ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (ObjectOptimisticLockingFailureException e) {

            log.error("Error in editing", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_LOCKED_RECORD);

        } catch (JpaObjectRetrievalFailureException e) {

            log.error("Error in editing", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_DELETED_RECORD);

        } catch (Exception exception) {

            log.error("Exception occured : ", exception);

            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Exception Cause 1 ::: " + exceptionCause1);

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            if (exceptionCause1.contains(UniqueKey.UK_DOCUMENT_DOCUMENTCODE)) {
                baseDto.setResponseCode(ErrorCode.DOCUMENT_CODE_ALREADY_EXIST);
            }

            if (exceptionCause1.contains(UniqueKey.UK_DOCUMENT_DOCUMENTNAME)) {
                baseDto.setResponseCode(ErrorCode.DOCUMENT_NAME_ALREADY_EXIST);
            }
        }

        return appUtil.setDesc(baseDto);
    }

    public BaseDto delete(Long id) {

        log.info("Delete method is Invoked...[" + id + "]");

        BaseDto baseDto = new BaseDto();

        try {

            cacheRepository.delete(SaaSUtil.getSaaSId(), DocumentMaster.class.getName(), id, documentMasterRepository);

            log.info("DocumentMaster Deleted Successfully....[" + id + "]");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

        } catch (RestException exception) {

            log.error("Exception in Delete method : ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (Exception exception) {

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            log.error("Exception in Delete method : ", exception);

            String rootCause = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Root Cause of Exception : " + rootCause);

            if (rootCause.contains("FK_")) {
                baseDto.setResponseCode(ErrorCode.CANNOT_DELETE_FK_RECORD);
            }
        }

        return appUtil.setDesc(baseDto);
    }

}

