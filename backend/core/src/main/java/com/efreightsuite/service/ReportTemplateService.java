package com.efreightsuite.service;

import javax.transaction.Transactional;

import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.ReportTemplateSearchDto;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.ExceptionHelper;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.exception.UniqueKey;
import com.efreightsuite.model.ReportTemplate;
import com.efreightsuite.repository.ReportTemplateRepository;
import com.efreightsuite.search.CommonSearchImpl;
import com.efreightsuite.search.ReportTemplateSearchImpl;
import com.efreightsuite.util.AppUtil;
import com.efreightsuite.validation.ReportTemplateValidator;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.exception.ExceptionUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;


@Service
@Log4j2
public class ReportTemplateService {

    @Autowired
    private
    AppUtil appUtil;

    @Autowired
    private
    ReportTemplateRepository reportTemplateRepository;

    @Autowired
    private
    ReportTemplateSearchImpl reportTemplateSearchImpl;
    @Autowired
    MetaConfigurationService metaConfigurationService;

    @Autowired
    private
    ReportTemplateValidator reportTemplateValidator;

    @Autowired
    CommonSearchImpl commonSearchImpl;

    public BaseDto get(Long id) {

        log.info("get method is called [" + id + "]");

        BaseDto baseDto = new BaseDto();

        try {
            ReportTemplate reportTemplate = reportTemplateRepository.findById(id);
            baseDto.setResponseCode(ErrorCode.SUCCESS);
            baseDto.setResponseObject(reportTemplate);

        } catch (RestException exception) {

            log.error("Exception in get method ", exception);

            baseDto.setResponseCode(exception.getMessage());
        } catch (Exception e) {

            log.error("Exception in get method", e);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);
    }

    public BaseDto createOrUpdate(ReportTemplate reportTemplate) {

        BaseDto baseDto = new BaseDto();

        try {

            reportTemplateValidator.validate(reportTemplate);

            synchronized (this) {
                saveReportTemplate(reportTemplate);
            }

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(null);

        } catch (RestException exception) {

            log.error("Rest Exception occured with error code ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (Exception exception) {

            log.error("Exception occured ", exception);

            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Exception Cause 1 : " + exceptionCause1);

            // General error condition
            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            // Duplicate Party Code
            if (exceptionCause1.contains(UniqueKey.COMP_UNQ_R_TEMPLATE_CAT)) {
                baseDto.setResponseCode(ErrorCode.REPORT_TEMPLATE_COMP_UK_CATEGORY_AND_NAME);
            }

        }

        return appUtil.setDesc(baseDto);
    }

    @Transactional
    private void saveReportTemplate(ReportTemplate reportTemplate) {
        reportTemplate = reportTemplateRepository.save(reportTemplate);
    }

    public BaseDto delete(Long id) {

        log.info("Delete method is Invoked...[" + id + "]");

        BaseDto baseDto = new BaseDto();

        try {

            reportTemplateRepository.delete(id);

            log.info("Party Master Deleted Successfully....[" + id + "]");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

        } catch (DataIntegrityViolationException exception) {

            log.error("Exception occured : ", exception);

            ExceptionHelper helper = new ExceptionHelper(exception);

            log.error("Foreign key reference : " + helper.getFkReference());

            baseDto.setResponseCode(ErrorCode.ERROR_FK_CONSTRAINT);

        } catch (Exception exception) {

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            log.error("Exception in Delete method : ", exception);

            String rootCause = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Root Cause of Exception : " + rootCause);

            if (rootCause.contains("FK_")) {
                baseDto.setResponseCode(ErrorCode.CANNOT_DELETE_FK_RECORD);
            }
        }

        return appUtil.setDesc(baseDto);
    }

    public BaseDto search(ReportTemplateSearchDto searchDto) {

        log.info("Search method is called....");

        BaseDto baseDto = new BaseDto();

        try {

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(reportTemplateSearchImpl.search(searchDto));

            log.info("Successfully Searching Report Template...");

        } catch (Exception exception) {

            log.error("Exception in Search method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);

    }

}

