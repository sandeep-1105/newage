package com.efreightsuite.service;

import com.efreightsuite.common.component.CacheRepository;
import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.PartySearchDto;
import com.efreightsuite.dto.SearchRequest;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.ExceptionHelper;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.exception.UniqueKey;
import com.efreightsuite.model.PartyMaster;
import com.efreightsuite.repository.CountryMasterRepository;
import com.efreightsuite.repository.PartyAccountMasterRepository;
import com.efreightsuite.repository.PartyAddressMasterRepository;
import com.efreightsuite.repository.PartyAssociateToPartyTypeMasterRepository;
import com.efreightsuite.repository.PartyBusinessDetailRepository;
import com.efreightsuite.repository.PartyContactRepository;
import com.efreightsuite.repository.PartyCreditLimitRepository;
import com.efreightsuite.repository.PartyEmailMappingRepository;
import com.efreightsuite.repository.PartyMasterRepository;
import com.efreightsuite.repository.PartyServiceMasterRepository;
import com.efreightsuite.search.CommonSearchImpl;
import com.efreightsuite.search.PartyAccountSearchImpl;
import com.efreightsuite.search.PartySearchImpl;
import com.efreightsuite.util.AppUtil;
import com.efreightsuite.util.SaaSUtil;
import com.efreightsuite.validation.PartyMasterValidator;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.exception.ExceptionUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.orm.ObjectOptimisticLockingFailureException;
import org.springframework.orm.jpa.JpaObjectRetrievalFailureException;
import org.springframework.stereotype.Service;


@Service
@Log4j2
public class PartyMasterService {

    @Autowired
    private
    AppUtil appUtil;

    @Autowired
    private
    PartyMasterRepository partyMasterRepository;

    @Autowired
    CountryMasterRepository countryMasterRepository;

    @Autowired
    PartyServiceMasterRepository partyServiceMasterRepository;

    @Autowired
    PartyAccountMasterRepository partyAccountMasterRepository;


    @Autowired
    PartyCreditLimitRepository partyCreditLimitRepository;

    @Autowired
    PartyEmailMappingRepository partyEmailMappingRepository;

    @Autowired
    PartyBusinessDetailRepository partyBusinessDetailRepository;

    @Autowired
    PartyContactRepository partyContactRepository;

    @Autowired
    private
    CacheRepository cacheRepository;

    @Autowired
    private
    PartySearchImpl partySearchImpl;

    @Autowired
    private
    PartyAccountSearchImpl partyAccountSearchImpl;

    @Autowired
    PartyAssociateToPartyTypeMasterRepository partyAssociateToPartyTypeMasterRepository;

    @Autowired
    PartyAddressMasterRepository partyAddressMasterRepository;

    @Autowired
    SequenceGeneratorService sequenceGeneratorService;

    @Autowired
    PartyMasterValidator partyMasterValidator;

    @Autowired
    MetaConfigurationService metaConfigurationService;


    @Autowired
    private
    CommonSearchImpl commonSearchImpl;

    public BaseDto get(Long id) {

        log.info("get method is called [" + id + "]");

        BaseDto baseDto = new BaseDto();

        try {

            PartyMaster partyMaster = partyMasterRepository.findById(id);
            baseDto.setResponseCode(ErrorCode.SUCCESS);
            baseDto.setResponseObject(partyMaster);

        } catch (RestException exception) {

            log.error("Exception in get method ", exception);

            baseDto.setResponseCode(exception.getMessage());
        } catch (Exception e) {

            log.error("Exception in get method", e);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);
    }


    public BaseDto getByPartyCode(String partyCode) {

        BaseDto baseDto = new BaseDto();

        try {
            PartyMaster partyMaster = partyMasterRepository.getByPartyCode(partyCode);

            baseDto.setResponseCode(ErrorCode.SUCCESS);
            baseDto.setResponseObject(partyMaster);

        } catch (Exception e) {

            log.error("Exception in getByPartyCode method", e);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);
    }


    public BaseDto create(PartyMaster partyMaster) {

        log.info("Create method is Invoked....." + partyMaster);

        BaseDto baseDto = new BaseDto();

        try {
            partySearchImpl.create(baseDto, partyMaster);

        } catch (RestException exception) {

            log.error("Rest Exception occured with error code ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (Exception exception) {

            log.error("Exception occured ", exception);

            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Exception Cause 1 : " + exceptionCause1);

            // General error condition
            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);


            // Duplicate Party Code
            if (exceptionCause1.contains(UniqueKey.UK_PARTY_PARTYCODE)) {
                baseDto.setResponseCode(ErrorCode.PARTY_CODE_ALREADY_EXIST);
            }

            // Duplicate Party Name
            else if (exceptionCause1.contains(UniqueKey.UK_PARTY_PARTYNAME)) {
                baseDto.setResponseCode(ErrorCode.PARTY_NAME_ALREADY_EXIST);
            }

            // Duplicate Vat Number
            else if (exceptionCause1.contains(UniqueKey.UK_PARTYDETAIL_VALUEADDEDTAX)) {
                baseDto.setResponseCode(ErrorCode.PARTY_VAT_NUMBER_ALREADY_EXIST);
            }

            // Duplicate PAN Number
            else if (exceptionCause1.contains(UniqueKey.UK_PARTYDETAIL_PAN)) {
                baseDto.setResponseCode(ErrorCode.PARTY_PAN_NUMBER_ALREADY_EXIST);
            }

            // Duplicate CST Number
            else if (exceptionCause1.contains(UniqueKey.UK_PARTYDETAIL_CSTNO)) {
                baseDto.setResponseCode(ErrorCode.PARTY_CST_NUMBER_ALREADY_EXIST);
            }

            // Duplicate RAC Number
            else if (exceptionCause1.contains(UniqueKey.UK_PARTYDETAIL_RACNO)) {
                baseDto.setResponseCode(ErrorCode.PARTY_RAC_NUMBER_ALREADY_EXIST);
            }

            // Duplicate SVT Number
            else if (exceptionCause1.contains(UniqueKey.UK_PARTYDETAIL_SVTNO)) {
                baseDto.setResponseCode(ErrorCode.PARTY_SVT_NUMBER_ALREADY_EXIST);
            }

            // Duplicate SVAT Number
            else if (exceptionCause1.contains(UniqueKey.UK_PARTYDETAIL_SVATNO)) {
                baseDto.setResponseCode(ErrorCode.PARTY_SVAT_NUMBER_ALREADY_EXIST);
            }

            // Duplicate IATA CODE
            else if (exceptionCause1.contains(UniqueKey.UK_PARTYDETAIL_IATA_CODE)) {
                baseDto.setResponseCode(ErrorCode.PARTY_IATA_CODE_ALREADY_EXIST);
            }

            // Duplicate TIN number
            else if (exceptionCause1.contains(UniqueKey.UK_PARTYDETAIL_TINNO)) {
                baseDto.setResponseCode(ErrorCode.PARTY_TIN_NUMBER_ALREADY_EXIST);
            }

            // Duplicate Party Service
            else if (exceptionCause1.contains(UniqueKey.UK_PARTYSERVIE_PARTYSERVICE)) {
                log.info("Error Code while calling create method: " + ErrorCode.PARTY_PARTY_SERVICE_ALREADY_EXIST);
                baseDto.setResponseCode(ErrorCode.PARTY_PARTY_SERVICE_ALREADY_EXIST);
            }

            // Duplicate Party Company
            else if (exceptionCause1.contains(UniqueKey.UK_PARTYCOMPDIV_PARTYCOMPDIV)) {
                baseDto.setResponseCode(ErrorCode.PARTY_PARTY_COMPANY_ALREADY_EXIST);
            }

            // Duplicate Credit mapping
            else if (exceptionCause1.contains(UniqueKey.UK_PARTY_CREDIT)) {
                baseDto.setResponseCode(ErrorCode.PARTY_CREDIT_ALREADY_EXIST);
            }
            //Duplicate Account
            else if (exceptionCause1.contains(UniqueKey.UK_PARTY_ACCOUNT)) {
                baseDto.setResponseCode(ErrorCode.PARTY_ACCOUNT_ALREADY_EXIST);
            }

            // Duplicate Email
            else if (exceptionCause1.contains(UniqueKey.UK_PARTYEMAIL_PARTY)) {
                baseDto.setResponseCode(ErrorCode.PARTY_EMAIL_ALREADY_EXIST);
            }

        }

        return appUtil.setDesc(baseDto);
    }


    public BaseDto update(PartyMaster partyMaster) {

        log.info("Create method is Invoked....." + partyMaster.getId());

        BaseDto baseDto = new BaseDto();

        try {
            partySearchImpl.update(baseDto, partyMaster);
        } catch (RestException exception) {

            log.error("Rest Exception occured with error code ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (ObjectOptimisticLockingFailureException e) {

            log.error("Error in editing party", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_LOCKED_RECORD);

        } catch (JpaObjectRetrievalFailureException e) {

            log.error("Error in editing party", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_DELETED_RECORD);

        } catch (Exception exception) {

            log.error("Exception occured ", exception);

            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Exception Cause 1 : " + exceptionCause1);

            // General error condition
            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            // Duplicate Party Code
            if (exceptionCause1.contains(UniqueKey.UK_PARTY_PARTYCODE)) {
                baseDto.setResponseCode(ErrorCode.PARTY_CODE_ALREADY_EXIST);
            }

            // Duplicate Party Name
            else if (exceptionCause1.contains(UniqueKey.UK_PARTY_PARTYNAME)) {
                baseDto.setResponseCode(ErrorCode.PARTY_NAME_ALREADY_EXIST);
            }

            // Duplicate Vat Number
            else if (exceptionCause1.contains(UniqueKey.UK_PARTYDETAIL_VALUEADDEDTAX)) {
                baseDto.setResponseCode(ErrorCode.PARTY_VAT_NUMBER_ALREADY_EXIST);
            }

            // Duplicate PAN Number
            else if (exceptionCause1.contains(UniqueKey.UK_PARTYDETAIL_PAN)) {
                baseDto.setResponseCode(ErrorCode.PARTY_PAN_NUMBER_ALREADY_EXIST);
            }

            // Duplicate CST Number
            else if (exceptionCause1.contains(UniqueKey.UK_PARTYDETAIL_CSTNO)) {
                baseDto.setResponseCode(ErrorCode.PARTY_CST_NUMBER_ALREADY_EXIST);
            }

            // Duplicate RAC Number
            else if (exceptionCause1.contains(UniqueKey.UK_PARTYDETAIL_RACNO)) {
                baseDto.setResponseCode(ErrorCode.PARTY_RAC_NUMBER_ALREADY_EXIST);
            }

            // Duplicate SVT Number
            else if (exceptionCause1.contains(UniqueKey.UK_PARTYDETAIL_SVTNO)) {
                baseDto.setResponseCode(ErrorCode.PARTY_SVT_NUMBER_ALREADY_EXIST);
            }

            // Duplicate SVAT Number
            else if (exceptionCause1.contains(UniqueKey.UK_PARTYDETAIL_SVATNO)) {
                baseDto.setResponseCode(ErrorCode.PARTY_SVAT_NUMBER_ALREADY_EXIST);
            }

            // Duplicate IATA CODE
            else if (exceptionCause1.contains(UniqueKey.UK_PARTYDETAIL_IATA_CODE)) {
                baseDto.setResponseCode(ErrorCode.PARTY_IATA_CODE_ALREADY_EXIST);
            }

            // Duplicate TIN number
            else if (exceptionCause1.contains(UniqueKey.UK_PARTYDETAIL_TINNO)) {
                baseDto.setResponseCode(ErrorCode.PARTY_TIN_NUMBER_ALREADY_EXIST);
            }

            // Duplicate Party Service number
            else if (exceptionCause1.contains(UniqueKey.UK_PARTYSERVIE_PARTYSERVICE)) {
                log.info("Error Code : " + ErrorCode.PARTY_PARTY_SERVICE_ALREADY_EXIST);
                baseDto.setResponseCode(ErrorCode.PARTY_PARTY_SERVICE_ALREADY_EXIST);
            }

            // Duplicate Party Company
            else if (exceptionCause1.contains(UniqueKey.UK_PARTYCOMPDIV_PARTYCOMPDIV)) {
                log.info("Error Code : " + ErrorCode.PARTY_PARTY_COMPANY_ALREADY_EXIST);
                baseDto.setResponseCode(ErrorCode.PARTY_PARTY_COMPANY_ALREADY_EXIST);
            }

            // Duplicate Credit mapping
            else if (exceptionCause1.contains(UniqueKey.UK_PARTY_CREDIT)) {
                baseDto.setResponseCode(ErrorCode.PARTY_CREDIT_ALREADY_EXIST);
            } else if (exceptionCause1.contains(UniqueKey.UK_PARTY_ACCOUNT)) {
                baseDto.setResponseCode(ErrorCode.PARTY_ACCOUNT_ALREADY_EXIST);
            }

            // Duplicate Email
            else if (exceptionCause1.contains(UniqueKey.UK_PARTYEMAIL_PARTY)) {
                baseDto.setResponseCode(ErrorCode.PARTY_EMAIL_ALREADY_EXIST);
            }

        }

        return appUtil.setDesc(baseDto);
    }


    public BaseDto delete(Long id) {

        log.info("Delete method is Invoked...[" + id + "]");

        BaseDto baseDto = new BaseDto();

        try {

            cacheRepository.delete(SaaSUtil.getSaaSId(), PartyMaster.class.getName(), id, partyMasterRepository);

            log.info("Party Master Deleted Successfully....[" + id + "]");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

        } catch (DataIntegrityViolationException exception) {

            log.error("Exception occured : ", exception);

            ExceptionHelper helper = new ExceptionHelper(exception);

            log.error("Foreign key reference : " + helper.getFkReference());

            baseDto.setResponseCode(ErrorCode.ERROR_FK_CONSTRAINT);

        } catch (Exception exception) {

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            log.error("Exception in Delete method : ", exception);

            String rootCause = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Root Cause of Exception : " + rootCause);

            if (rootCause.contains("FK_")) {
                baseDto.setResponseCode(ErrorCode.CANNOT_DELETE_FK_RECORD);
            }
        }

        return appUtil.setDesc(baseDto);
    }


    public BaseDto search(PartySearchDto searchDto) {

        log.info("Search method is called....");

        BaseDto baseDto = new BaseDto();

        try {

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(partySearchImpl.search(searchDto));

            log.info("Successfully Searching (PartySearchDto) Party...");

        } catch (Exception exception) {

            log.error("Exception in (PartySearchDto) Search method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);

    }

    public BaseDto search(SearchRequest searchRequest) {

        log.info("Search method is called.... Changed one");

        BaseDto baseDto = new BaseDto();

        try {

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(partySearchImpl.search(searchRequest));

            log.info("Successfully Searching (SearchRequest)Party...");

        } catch (Exception exception) {

            log.error("Exception in (SearchRequest) Search method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);

    }

    public BaseDto searchByAgent(SearchRequest searchRequest) {

        BaseDto dto = new BaseDto();

        try {
            dto.setResponseCode(ErrorCode.SUCCESS);
            dto.setResponseObject(partySearchImpl.searchByAgent(searchRequest));


            log.info("Successfully Searching By Agent Party...");

        } catch (Exception exception) {

            log.error("Exception in Search By Agent P method : ", exception);

            dto.setResponseCode(ErrorCode.FAILED);
        }


        return appUtil.setDesc(dto);


    }


    public BaseDto searchPartyNotInList(SearchRequest searchRequest) {

        log.info("searchPartyNotInList method is called....");

        BaseDto baseDto = new BaseDto();

        try {

            baseDto.setResponseObject(commonSearchImpl.searchPartyNotInList(searchRequest));

            baseDto.setResponseCode(ErrorCode.SUCCESS);

        } catch (Exception exception) {

            log.error("Exception in searchPartyNotInList method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);
    }


    public BaseDto search(SearchRequest searchRequest, String partyType) {

        log.info("Search method is called....");

        BaseDto baseDto = new BaseDto();

        try {

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(partySearchImpl.search(searchRequest, partyType));

            log.info("Successfully Searching Party...");

        } catch (Exception exception) {

            log.error("Exception in Search method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);

    }


    public BaseDto searchPartyAccount(SearchRequest searchRequest, String partyId) {

        log.info("searchPartyAccount method is called....");

        BaseDto baseDto = new BaseDto();

        try {

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(partyAccountSearchImpl.search(searchRequest, partyId));

            log.info("Successfully Searching Party Account...");

        } catch (Exception exception) {

            log.error("Exception in searchPartyAccount method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);

    }

    public BaseDto searchPartyAccountNotInParty(SearchRequest searchRequest, Long partyId) {

        log.info("searchPartyAccount method is called....");

        BaseDto baseDto = new BaseDto();

        try {

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(partyAccountSearchImpl.searchPartyAccountNotInParty(searchRequest, partyId));

            log.info("Successfully Searching Party Account...");

        } catch (Exception exception) {

            log.error("Exception in searchPartyAccountNotInParty method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);

    }

    public BaseDto pushSave(PartyMaster entity) {

        BaseDto baseDto = new BaseDto();

        try {
            partyMasterRepository.save(entity);
            baseDto.setResponseCode(ErrorCode.SUCCESS);
        } catch (Exception e) {
            baseDto.setResponseCode(ErrorCode.FAILED);
            log.info("exception in push api save", e);

        }
        return appUtil.setDesc(baseDto);
    }
}

