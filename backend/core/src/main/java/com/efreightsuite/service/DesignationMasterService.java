package com.efreightsuite.service;

import com.efreightsuite.common.component.CacheRepository;
import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.DesignationSearchDto;
import com.efreightsuite.dto.SearchRequest;
import com.efreightsuite.enumeration.SequenceType;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.exception.UniqueKey;
import com.efreightsuite.model.DesignationMaster;
import com.efreightsuite.repository.DesignationMasterRepository;
import com.efreightsuite.search.DesignationSearchImpl;
import com.efreightsuite.util.AppUtil;
import com.efreightsuite.util.SaaSUtil;
import com.efreightsuite.validation.DesignationMasterValidator;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.exception.ExceptionUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.ObjectOptimisticLockingFailureException;
import org.springframework.orm.jpa.JpaObjectRetrievalFailureException;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class DesignationMasterService {

    @Autowired
    private
    AppUtil appUtil;

    @Autowired
    private
    DesignationMasterRepository designationMasterRepository;

    @Autowired
    private
    CacheRepository cacheRepository;

    @Autowired
    private
    DesignationSearchImpl designationSearchImpl;

    @Autowired
    private
    DesignationMasterValidator designationMasterValidator;

    @Autowired
    private
    SequenceGeneratorService sequenceGeneratorService;

    public BaseDto search(DesignationSearchDto searchDto) {

        log.info("Search method is called...." + searchDto);

        BaseDto baseDto = new BaseDto();

        try {

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(designationSearchImpl.search(searchDto));

            log.info("Successfully Searching ...");

        } catch (Exception exception) {

            log.error("Exception in Search method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);

    }

    public BaseDto search(SearchRequest searchRequest) {

        log.info("Search method is called...." + searchRequest);

        BaseDto baseDto = new BaseDto();

        try {

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(designationSearchImpl.search(searchRequest));

            log.info("Successfully Searching ...");

        } catch (Exception exception) {

            log.error("Exception in Search method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);

    }

    public BaseDto saveOrUpdate(DesignationMaster designationMaster) {

        log.info("DesignationMasterService -> saveOrUpdate method is called.....");

        BaseDto baseDto = new BaseDto();

        try {

            designationMasterValidator.validate(designationMaster);

            if (designationMaster.getId() != null) {
                designationMasterRepository.getOne(designationMaster.getId());
            } else {
                designationMaster.setDesignationCode(sequenceGeneratorService.getSequence(SequenceType.DESIGNATION));
            }


            designationMasterRepository.save(designationMaster);

            cacheRepository.remove(SaaSUtil.getSaaSId(), DesignationMaster.class.getName(), designationMaster.getId());

            log.info("Designation Master Saved successfully  :  [" + designationMaster.getId() + "]");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(ErrorCode.SUCCESS);

        } catch (RestException exception) {

            log.error("Exception in DesignationMasterService -> saveOrUpdate method ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (ObjectOptimisticLockingFailureException e) {

            log.error("Exception in DesignationMasterService -> saveOrUpdate method ", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_LOCKED_RECORD);

        } catch (JpaObjectRetrievalFailureException e) {

            log.error("Exception in DesignationMasterService -> saveOrUpdate method ", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_DELETED_RECORD);

        } catch (Exception exception) {

            log.error("Exception in DesignationMasterService -> saveOrUpdate method ", exception);

            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Exception Cause 1 ::: " + exceptionCause1);

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            if (exceptionCause1.contains("FK_")) {
                baseDto.setResponseCode(ErrorCode.SAVE_UPDATE_FK_RECORD);
            }

            if (exceptionCause1.contains(UniqueKey.UK_DESIGNATION_DESIGNATION_CODE)) {
                baseDto.setResponseCode(ErrorCode.DESIGNATION_CODE_DUPLICATED);
            }

            if (exceptionCause1.contains(UniqueKey.UK_DESIGNATION_DESIGNATION_NAME)) {
                baseDto.setResponseCode(ErrorCode.DESIGNATION_NAME_DUPLICATED);
            }
        }
        return appUtil.setDesc(baseDto);
    }

    public BaseDto delete(Long id) {

        log.info("DesignationMasterService -> delete  method called....[ " + id + " ] ");

        BaseDto baseDto = new BaseDto();

        try {

            cacheRepository.delete(SaaSUtil.getSaaSId(), DesignationMaster.class.getName(), id,
                    designationMasterRepository);

            log.info("Designation Master Deleted Successfully.... [ +" + id + "]");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

        } catch (Exception exception) {

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            log.error("Exception in DesignationMasterService -> delete method ", exception);

            String rootCause = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Root Cause of Exception : " + rootCause);

            if (rootCause.contains("FK_")) {
                baseDto.setResponseCode(ErrorCode.CANNOT_DELETE_FK_RECORD);
            }
        }
        return appUtil.setDesc(baseDto);

    }

    public BaseDto get(Long id) {

        log.info("DesignationMasterService -> get method is called....[" + id + "]");

        BaseDto baseDto = new BaseDto();

        try {

            DesignationMaster designationMaster = cacheRepository.get(SaaSUtil.getSaaSId(),
                    DesignationMaster.class.getName(), id, designationMasterRepository);

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(designationMaster);

        } catch (Exception exception) {

            log.error("Exception in DesignationMasterService -> get method", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);
    }

}
