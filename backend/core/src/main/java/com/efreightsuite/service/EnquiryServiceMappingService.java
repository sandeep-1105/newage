package com.efreightsuite.service;

import com.efreightsuite.common.component.CacheRepository;
import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.EnquiryServiceMappingSearchDto;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.exception.UniqueKey;
import com.efreightsuite.model.EnquiryServiceMapping;
import com.efreightsuite.repository.EnquiryServiceMappingRepository;
import com.efreightsuite.search.EnquiryServiceMappingSearchImpl;
import com.efreightsuite.util.AppUtil;
import com.efreightsuite.util.SaaSUtil;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.exception.ExceptionUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
@Log4j2
public class EnquiryServiceMappingService {

    @Autowired
    private
    AppUtil appUtil;

    @Autowired
    private
    CacheRepository cacheRepository;

    @Autowired
    private
    EnquiryServiceMappingSearchImpl enquiryServiceMappingSearchImpl;

    @Autowired
    private
    EnquiryServiceMappingRepository enquiryServiceMappingRepository;

    public BaseDto search(EnquiryServiceMappingSearchDto searchDto) {

        log.info("getAll method is called....");

        BaseDto baseDto = new BaseDto();

        try {

            log.info("Successfully getting list of serviceMapping...");
            baseDto.setResponseCode(ErrorCode.SUCCESS);
            baseDto.setResponseObject(enquiryServiceMappingSearchImpl.search(searchDto));

        } catch (RestException re) {
            log.error("Exception in getAll method ", re);
            baseDto.setResponseCode(re.getMessage());

        } catch (Exception e) {
            log.error("Exception in getAll method ", e);
            baseDto.setResponseCode(ErrorCode.FAILED);

        }
        return appUtil.setDesc(baseDto);
    }


    public BaseDto get(Long id) {
        log.info("get method is called [" + id + "]");
        BaseDto baseDto = new BaseDto();
        try {
            EnquiryServiceMapping enquiryServiceMapping = cacheRepository.get(SaaSUtil.getSaaSId(), EnquiryServiceMapping.class.getName(), id, enquiryServiceMappingRepository);
            log.info("Successfully getting EnquiryServiceMapping by id.....[" + id + "]");
            baseDto.setResponseCode(ErrorCode.SUCCESS);
            baseDto.setResponseObject(enquiryServiceMapping);
        } catch (RestException exception) {
            log.error("Exception in get method ", exception);
            baseDto.setResponseCode(exception.getMessage());
        } catch (Exception e) {
            log.error("Exception in get method", e);
            baseDto.setResponseCode(ErrorCode.FAILED);
        }
        return appUtil.setDesc(baseDto);
    }


    public BaseDto saveOrupdate(EnquiryServiceMapping enquiryServiceMapping) {
        BaseDto baseDto = new BaseDto();
        try {

            enquiryServiceMappingRepository.save(enquiryServiceMapping);
            cacheRepository.remove(SaaSUtil.getSaaSId(), EnquiryServiceMapping.class.getName(), enquiryServiceMapping.getId());

            baseDto.setResponseCode(ErrorCode.SUCCESS);
            baseDto.setResponseObject(enquiryServiceMapping);

        } catch (RestException exception) {
            log.error("Rest Exception occured with error code ", exception);
            baseDto.setResponseCode(exception.getMessage());
        } catch (Exception exception) {
            log.error("Exception occured ", exception);

            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Exception Cause 1 : " + exceptionCause1);

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            if (exceptionCause1.contains(UniqueKey.UK_ENQUIRY_SERVICE_MAPPING)) {
                baseDto.setResponseCode(ErrorCode.ENQUIRY_SERVICE_MAPPING_ALREADY_EXIST);
            }
            if (exceptionCause1.contains(UniqueKey.UK_ENQSERVICE_SERVICEID)) {
                baseDto.setResponseCode(ErrorCode.TRANSPORT_ALREADY_EXIST);
            }

        }

        return appUtil.setDesc(baseDto);
    }

}
