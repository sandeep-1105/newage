package com.efreightsuite.service;

import java.util.List;

import javax.persistence.EntityManager;

import com.efreightsuite.common.component.CacheRepository;
import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.DateConfigurationDto;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.ExceptionHelper;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.exception.UniqueKey;
import com.efreightsuite.model.DateConfiguration;
import com.efreightsuite.repository.DateConfigurationRepository;
import com.efreightsuite.repository.ServiceMasterRepository;
import com.efreightsuite.search.DateConfigurationSearchImpl;
import com.efreightsuite.util.AppUtil;
import com.efreightsuite.util.SaaSUtil;
import com.efreightsuite.validation.DateConfigurationValidator;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.exception.ExceptionUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class DateConfigurationService {

    @Autowired
    private
    AppUtil appUtil;

    @Autowired
    private
    CacheRepository cacheRepository;

    @Autowired
    ServiceMasterRepository serviceMasterRepository;

    @Autowired
    private
    DateConfigurationRepository dateConfigurationRepository;

    @Autowired
    EntityManager entityManager;

    @Autowired
    private
    DateConfigurationSearchImpl dateConfigurationSearchImpl;

    @Autowired
    private
    DateConfigurationValidator dateConfigurationValidator;


    public BaseDto search(DateConfigurationDto dto) {

        BaseDto baseDto = new BaseDto();

        try {
            baseDto.setResponseObject(dateConfigurationSearchImpl.search(dto));
            baseDto.setResponseCode(ErrorCode.SUCCESS);

        } catch (RestException exception) {
            log.error("Exception in get method ", exception);
            baseDto.setResponseCode(exception.getMessage());
        } catch (Exception e) {
            log.error("Exception in get method", e);
            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);

    }

    public BaseDto get(DateConfigurationDto dto) {

        BaseDto baseDto = new BaseDto();

        try {

            DateConfiguration dateConfiguration = dateConfigurationRepository.getByLogicDate(dto.getServiceId(), dto.getWhichTransactionDate());

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(dateConfiguration);

        } catch (RestException exception) {
            log.error("Exception in get method ", exception);
            baseDto.setResponseCode(exception.getMessage());
        } catch (Exception e) {
            log.error("Exception in get method", e);
            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);
    }

    public BaseDto create(List<DateConfiguration> dateConfigurationList) {

        BaseDto baseDto = new BaseDto();

        try {

            for (DateConfiguration dateConfiguration : dateConfigurationList) {
                dateConfigurationValidator.validate(dateConfiguration);
            }

            //Save or Update in DB with cache
            dateConfigurationList = dateConfigurationRepository.save(dateConfigurationList);
            for (DateConfiguration dateConfiguration : dateConfigurationList) {
                cacheRepository.saveLogicDate(SaaSUtil.getSaaSId(), DateConfiguration.class.getName(), dateConfiguration);
            }

            baseDto.setResponseCode(ErrorCode.SUCCESS);
            baseDto.setResponseObject(dateConfigurationList);

        } catch (RestException exception) {
            log.error("Rest Exception occured with error code ", exception);
            baseDto.setResponseCode(exception.getMessage());
        } catch (Exception exception) {
            log.error("Exception occured ", exception);
            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(exception);
            log.error("Exception Cause 1 : " + exceptionCause1);
            // General error condition
            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);
            // Duplicate City Code
            if (exceptionCause1.contains(UniqueKey.UK_DATECONFI_DATE)) {
                baseDto.setResponseCode(ErrorCode.DATE_CONFIG_UNIQUE);
            }
        }

        return appUtil.setDesc(baseDto);
    }


    public BaseDto update(DateConfiguration dateConfiguration) {

        BaseDto baseDto = new BaseDto();

        try {

            dateConfigurationValidator.validate(dateConfiguration);

            dateConfiguration = dateConfigurationRepository.save(dateConfiguration);
            cacheRepository.saveLogicDate(SaaSUtil.getSaaSId(), DateConfiguration.class.getName(), dateConfiguration);

            baseDto.setResponseCode(ErrorCode.SUCCESS);
            baseDto.setResponseObject(dateConfiguration);

        } catch (RestException exception) {
            log.error("Rest Exception occured with error code ", exception);
            baseDto.setResponseCode(exception.getMessage());
        } catch (Exception exception) {
            log.error("Exception occured ", exception);
            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(exception);
            log.error("Exception Cause 1 : " + exceptionCause1);
            // General error condition
            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);
            // Duplicate City Code
            if (exceptionCause1.contains(UniqueKey.UK_DATECONFI_DATE)) {
                baseDto.setResponseCode(ErrorCode.DATE_CONFIG_UNIQUE);
            }
        }

        return appUtil.setDesc(baseDto);
    }

    public BaseDto delete(Long id) {

        BaseDto baseDto = new BaseDto();

        try {

            dateConfigurationRepository.delete(id);

            baseDto.setResponseCode(ErrorCode.SUCCESS);

        } catch (DataIntegrityViolationException exception) {
            log.error("Exception occured : ", exception);
            ExceptionHelper helper = new ExceptionHelper(exception);
            log.error("Foreign key reference : " + helper.getFkReference());
            baseDto.setResponseCode(ErrorCode.ERROR_FK_CONSTRAINT);
        } catch (Exception exception) {
            log.error("Exception in Delete method : ", exception);
            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);
    }
}
