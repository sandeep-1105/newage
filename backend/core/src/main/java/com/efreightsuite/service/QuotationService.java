package com.efreightsuite.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import javax.persistence.EntityManager;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.efreightsuite.activiti.ActivitiTransactionService;
import com.efreightsuite.common.component.CacheRepository;
import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.CopyDto;
import com.efreightsuite.dto.QuotationApproveDto;
import com.efreightsuite.dto.QuotationSearchDto;
import com.efreightsuite.enumeration.AddressType;
import com.efreightsuite.enumeration.Approved;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.exception.UniqueKey;
import com.efreightsuite.model.*;
import com.efreightsuite.repository.DefaultMasterDataRepository;
import com.efreightsuite.repository.EmployeeMasterRepository;
import com.efreightsuite.repository.PartyMasterRepository;
import com.efreightsuite.repository.QuotationAttachmentRepository;
import com.efreightsuite.repository.QuotationDetailRepository;
import com.efreightsuite.repository.QuotationRepository;
import com.efreightsuite.repository.SurchargeMappingRepository;
import com.efreightsuite.search.QuotationSearchImpl;
import com.efreightsuite.service.mailer.QuotationMailer;
import com.efreightsuite.util.AppUtil;
import com.efreightsuite.util.SaaSUtil;
import com.efreightsuite.util.TimeUtil;
import com.efreightsuite.util.ValidateUtil;
import com.efreightsuite.validators.QuotationValidator;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.hibernate.envers.AuditReader;
import org.hibernate.envers.AuditReaderFactory;
import org.hibernate.envers.RevisionType;
import org.hibernate.envers.query.AuditQuery;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.ObjectOptimisticLockingFailureException;
import org.springframework.orm.jpa.JpaObjectRetrievalFailureException;
import org.springframework.stereotype.Service;

import static com.efreightsuite.constants.Constants.COMMA_SPACE;


@Service
@Log4j2
public class QuotationService {

    @Autowired
    private
    AppUtil appUtil;

    @Autowired
    CacheRepository cacheRepository;

    @Autowired
    private
    SurchargeMappingRepository surchargeMappingRepository;

    @Autowired
    private
    EmployeeMasterRepository employeeMasterRepository;

    @Autowired
    private
    QuotationMailer quotationEmailAsyncService;


    @Autowired
    private
    PartyMasterRepository partyMasterRepository;

    @Autowired
    private
    QuotationSearchImpl quotationSearchImpl;

    @Autowired
    private
    QuotationRepository quotationRepository;

    @Autowired
    private
    QuotationValidator quotationValidator;

    @Autowired
    SequenceGeneratorService sequenceGeneratorService;

    @Autowired
    private
    QuotationAttachmentRepository quotationAttachmentRepository;

    @Autowired
    DefaultMasterDataRepository defaultMasterDataRepository;

    @Autowired
    HttpServletResponse response;

    @Autowired
    private
    ActivitiTransactionService activitiTransactionService;

    @Autowired
    private
    QuotationDetailRepository quotationDetailRepository;

    @Autowired
    private
    EntityManager entityManager;

    public BaseDto search(QuotationSearchDto searchDto) {

        log.info("Search method is called...." + searchDto);

        BaseDto baseDto = new BaseDto();

        try {

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(quotationSearchImpl.search(searchDto));

            log.info("Successfully Searching Quotation...");

        } catch (Exception exception) {

            log.error("Exception in Search method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);

    }

    public BaseDto get(Long id, String quotationNo) {
        log.info("Quotation.get method start.[" + id + "]");

        BaseDto baseDto = new BaseDto();

        try {
            Quotation quotation = null;

            if (id != null) {
                quotation = quotationRepository.findById(id);
            } else if (quotationNo != null) {
                quotation = quotationRepository.findByQuotationNo(quotationNo);
            }

            if (quotation != null) {

                for (QuotationDetail qd : quotation.getQuotationDetailList()) {
                    qd.setQuotation(null);

                    for (QuotationDimension dim : qd.getQuotationDimensionList()) {
                        dim.setQuotationDetail(null);
                    }

                    for (QuotationCarrier dim : qd.getQuotationCarrierList()) {
                        dim.setQuotationDetail(null);
                        for (QuotationCharge charge : dim.getQuotationChargeList()) {
                            charge.setQuotationCarrier(null);
                        }
                    }

                    for (QuotationContainer dim : qd.getQuotationContainerList()) {
                        dim.setQuotationDetail(null);
                    }

                    for (QuotationDetailGeneralNote dim : qd.getServiceNoteList()) {
                        dim.setQuotationDetail(null);
                    }

                }


                AppUtil.setPartyToNull(quotation.getCustomer());
                AppUtil.setPartyToNull(quotation.getShipper());
                if (quotation.getLocationMaster() != null)
                    AppUtil.setPartyToNull(quotation.getLocationMaster().getPartyMaster());
                if (quotation.getApprovedBy() != null) {
                    quotation.getApprovedBy().setLocationMaster(null);
                    quotation.getApprovedBy().setAttachmentList(null);
                }
                if (quotation.getLoggedBy() != null) {
                    quotation.getLoggedBy().setLocationMaster(null);
                    quotation.getLoggedBy().setAttachmentList(null);
                }

                for (QuotationGeneralNote qd : quotation.getGeneralNoteList()) {
                    qd.setQuotation(null);
                }
                for (QuotationAttachment attachment : quotation.getQuotationAttachementList()) {
                    attachment.setFile(null);
                }
                baseDto.setResponseCode(ErrorCode.SUCCESS);
                baseDto.setResponseObject(quotation);

            } else {
                baseDto.setResponseCode(ErrorCode.QUOTATION_IS_NOT_AVAILABLE);
            }
        } catch (RestException re) {
            log.error("RestException in Quotation.get method while getting the quotation : "
                    + re);
            baseDto.setResponseCode(ErrorCode.QUOTATION_ID_INVALID);
        } catch (Exception e) {
            log.error(
                    "Exception in quotation.get method while getting the quotation : ",
                    e);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        log.info("quotation.get method end.");

        return appUtil.setDesc(baseDto);
    }

	/*
     * create - This method creates a new quotation
	 * 
	 * @Param Quotation master entity
	 * 
	 * @Return BaseDto
	 */

    public BaseDto create(Quotation quotation) {

        log.info("Create method is start.");
        BaseDto baseDto = new BaseDto();
        try {

            quotationValidator.validate(quotation);

            quotationSearchImpl.checkQuotationUnique(quotation);


            if (quotation.getQuotationDetailList() != null) {

                for (QuotationDetail detail : quotation.getQuotationDetailList()) {

                    detail.setQuotation(quotation);

                    if (detail.getQuotationValueAddedServiceList() != null) {
                        for (QuotationValueAddedService qv : detail.getQuotationValueAddedServiceList()) {
                            qv.setQuotationDetail(detail);
                        }
                    }


                    if (detail.getQuotationCarrierList() != null) {
                        for (QuotationCarrier carrier : detail.getQuotationCarrierList()) {
                            carrier.setQuotationDetail(detail);

                            if (carrier.getQuotationChargeList() != null) {
                                for (QuotationCharge charge : carrier.getQuotationChargeList()) {
                                    charge.setQuotationCarrier(carrier);
                                }
                            }

                        }
                    }

                    if (detail.getQuotationContainerList() != null) {
                        for (QuotationContainer container : detail.getQuotationContainerList()) {
                            container.setQuotationDetail(detail);
                        }
                    }

                    if (detail.getQuotationDimensionList() != null) {
                        for (QuotationDimension dimension : detail.getQuotationDimensionList()) {
                            dimension.setQuotationDetail(detail);
                        }
                    }

                    if (detail.getServiceNoteList() != null) {
                        for (QuotationDetailGeneralNote note : detail.getServiceNoteList()) {
                            note.setQuotationDetail(detail);
                        }
                    }
                }

            }

            if (quotation.getGeneralNoteList() != null) {
                for (QuotationGeneralNote generalNote : quotation.getGeneralNoteList()) {
                    generalNote.setQuotation(quotation);
                }
            }


            if (quotation.getQuotationAttachementList() != null) {
                for (QuotationAttachment quotationAttachment : quotation.getQuotationAttachementList()) {
                    quotationAttachment.setQuotation(quotation);
                }
            }

            Quotation createdQuotation = createQuotation(quotation);

            approveMailTrigger(createdQuotation);

            log.info("Quotation Saved successfully.....");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(new Quotation(createdQuotation.getId(), createdQuotation.getQuotationNo()));

        } catch (RestException re) {

            log.error("Exception in create ", re);

            baseDto.setResponseCode(re.getMessage());

        } catch (Exception e) {

            String exceptionCause = ExceptionUtils.getRootCauseMessage(e);

            log.error("Exception Cause  ::: ", e);

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            if (exceptionCause.contains(UniqueKey.UK_QUOTATION_NO)) {
                baseDto.setResponseCode(ErrorCode.QUOTATION_NUMBER_ALREADY_EXIST);
            }

            if (exceptionCause.contains(UniqueKey.UK_QUOTATION_PARTYVAILDTO)) {
                baseDto.setResponseCode(ErrorCode.QUOTATION_PARTY_VALID_FROM_TO);
            }

            if (exceptionCause.contains(UniqueKey.UK_QUOTATIONATT_REF)) {
                baseDto.setResponseCode(ErrorCode.QUOTATION_REFERENCE_ALREADY_EXIST);
            }
        }

        log.info("Create method is end.");

        return appUtil.setDesc(baseDto);
    }


    private Quotation createQuotation(Quotation quotation) {
        return quotationSearchImpl.createQuotation(quotation);
    }

    private void approveMailTrigger(Quotation quotation) {
        String manager = appUtil.getLocationConfig("quotation.client.approval.required", quotation.getLocationMaster(), true);
        if ("TRUE".equals(manager)) {
            quotationEmailAsyncService.sendQuotationMailToCustomer(quotation.getId(), AuthService.getCurrentUser(), SaaSUtil.getSaaSId());
        }
    }

    public BaseDto create(CopyDto dto) {

        log.info("Create method is start.");
        BaseDto baseDto = new BaseDto();
        try {

            Long quoteId = dto.getQuotation().getId();
            Quotation quotation = dto.getQuotation();
            quotation.setCopyQuotationNo(quotation.getQuotationNo());
            quotation.setId(null);
            quotation.setCustomer(dto.getParty());

            quotation.setCustomerAddress(null);
            quotation.setAttention(null);
            if (null != dto.getParty()) {
                for (PartyAddressMaster address : dto.getParty()
                        .getPartyAddressList()) {
                    if (AddressType.Primary.equals(address.getAddressType())) {

                        AddressMaster addMaster = new AddressMaster();
                        addMaster.setAddressLine1(address.getPoBox() != null ? address.getPoBox()
                                + COMMA_SPACE + address.getAddressLine1()
                                : address.getAddressLine1());
                        addMaster.setAddressLine2(address.getAddressLine2());
                        addMaster.setAddressLine3(address.getAddressLine3());

                        String addressLine4 = (address.getCityMaster() != null) ? address.getCityMaster().getCityName() : StringUtils.EMPTY;

                        String stateName = (address.getStateMaster() != null) ? address.getStateMaster().getStateName() : StringUtils.EMPTY;
                        addressLine4 = (StringUtils.isNotBlank(addressLine4)) ? addressLine4 + COMMA_SPACE + stateName : stateName;

                        String countryName = (dto.getParty() != null && dto.getParty().getCountryMaster() != null) ? dto.getParty().getCountryMaster().getCountryName() : StringUtils.EMPTY;
                        addressLine4 = (StringUtils.isNotBlank(addressLine4)) ? addressLine4 + COMMA_SPACE + countryName : countryName;

                        String zipCode = (StringUtils.isNoneBlank(address.getZipCode())) ? address.getZipCode() : StringUtils.EMPTY;
                        addressLine4 = (StringUtils.isNotBlank(addressLine4)) ? addressLine4 + COMMA_SPACE + zipCode : zipCode;

                        addMaster.setAddressLine4(addressLine4);

                        quotation.setCustomerAddress(addMaster);
                        quotation.setAttention(address.getContactPerson());
                        break;
                    }
                }
            }
            quotation.getShipperAddress().setId(null);

            quotation.setValidFrom(dto.getValidFrom());
            quotation.setValidTo(dto.getValidTo());
            quotation.setBookingNo(null);
            quotation.setEnquiryNo(null);

            quotation.setApproved(Approved.Pending);
            quotation.setApprovedBy(null);
            quotation.setApprovedDate(null);

            quotation.setLoggedBy(AuthService.getCurrentUser().getEmployee());
            Date date = TimeUtil.getCurrentLocationTime();
            date.setHours(0);
            date.setMinutes(0);
            date.setSeconds(0);
            quotation.setLoggedOn(date);


            if (quotation.getQuotationDetailList() != null) {

                for (QuotationDetail detail : quotation
                        .getQuotationDetailList()) {

                    if (detail.getPickupAddress() != null) {
                        detail.getPickupAddress().setId(null);
                        detail.getPickupAddress().setVersionLock(0);
                    }

                    if (detail.getDeliveryAddress() != null) {
                        detail.getDeliveryAddress().setId(null);
                        detail.getDeliveryAddress().setVersionLock(0);
                    }

                    detail.setProcessInstanceId(null);
                    detail.setTaskId(null);
                    detail.setId(null);
                    detail.setQuotation(quotation);
                    detail.setQuotationCarrierId(null);
                    if (detail.getQuotationCarrierList() != null) {
                        for (QuotationCarrier carrier : detail
                                .getQuotationCarrierList()) {
                            carrier.setQuotationDetail(detail);
                            carrier.setId(null);

                            if (carrier.getQuotationChargeList() != null) {
                                for (QuotationCharge charge : carrier.getQuotationChargeList()) {
                                    charge.setQuotationCarrier(carrier);
                                    charge.setId(null);
                                }
                            }
                        }
                    }

                    if (detail.getQuotationContainerList() != null) {
                        for (QuotationContainer container : detail
                                .getQuotationContainerList()) {
                            container.setQuotationDetail(detail);
                            container.setId(null);
                        }
                    }

                    if (detail.getQuotationDimensionList() != null) {
                        for (QuotationDimension dimension : detail
                                .getQuotationDimensionList()) {
                            dimension.setQuotationDetail(detail);
                            dimension.setId(null);
                        }
                    }

                    if (detail.getServiceNoteList() != null) {
                        for (QuotationDetailGeneralNote note : detail
                                .getServiceNoteList()) {
                            note.setQuotationDetail(detail);
                            note.setId(null);
                        }
                    }

                    if (detail.getQuotationValueAddedServiceList() != null) {
                        for (QuotationValueAddedService vat : detail
                                .getQuotationValueAddedServiceList()) {
                            vat.setQuotationDetail(detail);
                            vat.setId(null);
                            vat.setVersionLock(0);
                        }
                    }

                }

            }

            if (quotation.getGeneralNoteList() != null) {
                for (QuotationGeneralNote generalNote : quotation
                        .getGeneralNoteList()) {
                    generalNote.setQuotation(quotation);
                    generalNote.setId(null);
                }
            }


            if (quotation.getQuotationAttachementList() != null) {

                List<QuotationAttachment> quotationAttachments = new ArrayList<>();
                for (QuotationAttachment qa : quotationRepository.getAllAttachment(quoteId)) {
                    QuotationAttachment tmp = new QuotationAttachment();

                    tmp.setId(null);

                    tmp.setRefNo(qa.getRefNo());
                    tmp.setFile(qa.getFile());
                    tmp.setFileName(qa.getFileName());
                    tmp.setFileContentType(qa.getFileContentType());
                    tmp.setQuotation(quotation);
                    quotationAttachments.add(tmp);

                }
                quotation.setQuotationAttachementList(quotationAttachments);
            }

            quotationSearchImpl.checkQuotationUnique(quotation);

            quotation = createQuotation(quotation);

            approveMailTrigger(quotation);

            log.info("Quotation Copied successfully.....");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(quotation.getId());

        } catch (RestException re) {

            log.error("Exception in create ", re);

            baseDto.setResponseCode(re.getMessage());

        } catch (Exception e) {

            String exceptionCause = ExceptionUtils.getRootCauseMessage(e);

            log.error("Exception Cause  ::: ", e);

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            if (exceptionCause.contains(UniqueKey.UK_QUOTATION_NO)) {
                baseDto.setResponseCode(ErrorCode.QUOTATION_NUMBER_ALREADY_EXIST);
            }

            if (exceptionCause.contains(UniqueKey.UK_QUOTATION_PARTYVAILDTO)) {
                baseDto.setResponseCode(ErrorCode.QUOTATION_PARTY_VALID_FROM_TO);
            }

            if (exceptionCause.contains(UniqueKey.UK_QUOTATIONATT_REF)) {
                baseDto.setResponseCode(ErrorCode.QUOTATION_REFERENCE_ALREADY_EXIST);
            }
        }

        log.info("Create method is end.");

        return appUtil.setDesc(baseDto);
    }


    public BaseDto update(Quotation quotation, boolean isGroup) {

        BaseDto baseDto = new BaseDto();

        try {

            Quotation tmpQut = quotationRepository.findById(quotation.getId());
            boolean isRejected = false;
            if (Approved.Rejected.equals(tmpQut.getApproved())) {

                if (!AuthService.getCurrentUser().getEmployee().getId().equals(tmpQut.getApprovedBy().getId())) {
                    throw new RestException("");
                }
                isRejected = true;
            }

            Quotation updatedQuotation = quotationSearchImpl.updateQuotation(quotation, isRejected, isGroup);

            if (!isGroup && (isRejected || Approved.Approved.equals(updatedQuotation.getApproved()))) {
                quotationEmailAsyncService.sendQuotationMailToCustomer(updatedQuotation.getId(), AuthService.getCurrentUser(), SaaSUtil.getSaaSId());
            }

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(new Quotation(updatedQuotation.getId(), updatedQuotation.getQuotationNo()));

        } catch (RestException exception) {

            log.error("Validation Exception in Update method : ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (ObjectOptimisticLockingFailureException e) {

            log.error("Locking Exception", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_LOCKED_RECORD);

        } catch (JpaObjectRetrievalFailureException e) {

            log.error("Exception ", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_DELETED_RECORD);

        } catch (Exception e) {

            String exceptionCause = ExceptionUtils.getRootCauseMessage(e);

            log.error("Exception Cause  ::: ", e);

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            if (exceptionCause.contains(UniqueKey.UK_QUOTATION_NO)) {
                baseDto.setResponseCode(ErrorCode.QUOTATION_NUMBER_ALREADY_EXIST);
            }

            if (exceptionCause.contains(UniqueKey.UK_QUOTATION_PARTYVAILDTO)) {
                baseDto.setResponseCode(ErrorCode.QUOTATION_PARTY_VALID_FROM_TO);
            }

            if (exceptionCause.contains(UniqueKey.UK_QUOTATIONATT_REF)) {
                baseDto.setResponseCode(ErrorCode.QUOTATION_REFERENCE_ALREADY_EXIST);
            }
        }

        log.info("Update method is end.");

        return appUtil.setDesc(baseDto);
    }


    public void downloadAttachment(Long attachmentId, HttpServletResponse response) {

        QuotationAttachment attachment = quotationAttachmentRepository.getOne(attachmentId);


        if (attachment != null && attachment.getFile() != null && attachment.getFileName() != null) {

            try {

                response.setContentType(attachment.getFileContentType());

                response.setHeader("Content-Disposition", "attachment; filename=" + attachment.getFileName());

                response.setContentLength(attachment.getFile().length);

                response.getOutputStream().write(attachment.getFile());

                response.getOutputStream().flush();

                response.getOutputStream().close();

            } catch (Exception e) {
                log.error("Error occur during download attachment file : " + attachmentId + ", ", e);
            }


        } else {
            log.info("Attachment document is not available : " + attachmentId);
        }

    }


    private Long updateQuotationStatus(QuotationApproveDto dto) {


        Quotation quotation = quotationRepository.findById(dto.getQuotationId());
        EmployeeMaster employee = employeeMasterRepository.findById(dto.getEmployeeId());

        quotation.setApproved(dto.getApproved());
        quotation.setApprovedBy(employee);
        quotation.setApprovedDate(dto.getApprovedDate());

        String key = appUtil.getLocationConfig("quotation.client.approval.required", quotation.getLocationMaster(), true);

        if (quotation.getApproved() == Approved.Approved
                && key.equals("FALSE")) {
            quotation.setApproved(Approved.Approved);
            quotation.setCustomerApprovedDate(TimeUtil.getCurrentLocationTime(quotation.getLocationMaster()));
        }

        quotation = quotationRepository.save(quotation);

        if (quotation.getApproved() == Approved.Rejected) {
            activitiTransactionService.cancelQuotation(quotation);
        }


        return dto.getQuotationId();

    }

    public BaseDto updateStatus(QuotationApproveDto dto) {

        BaseDto baseDto = new BaseDto();
        try {

            baseDto.setResponseCode(ErrorCode.SUCCESS);
            baseDto.setResponseObject(updateQuotationStatus(dto));
        } catch (RestException re) {
            log.error("Exception in updateStatus ", re);
            baseDto.setResponseCode(re.getMessage());

        } catch (Exception e) {
            log.error("Exception Cause  ::: ", e);
            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);
        }

        if (dto.getApproved() == Approved.Approved && baseDto.getResponseCode() == ErrorCode.SUCCESS) {
            quotationEmailAsyncService.sendQuotationMailToCustomer(dto.getQuotationId(), AuthService.getCurrentUser(), SaaSUtil.getSaaSId());
        }

        return appUtil.setDesc(baseDto);
    }

    public BaseDto quotationCustomerApproval(QuotationApproveDto dto, HttpServletRequest request) {
        BaseDto baseDto = new BaseDto();
        try {

            Quotation quotation = quotationRepository.findByQuotationNo(dto.getQuotationNo());

            if (!quotation.getApproved().equals(Approved.Approved)) {
                throw new RestException(ErrorCode.QUOTATION_NOT_APPROVED_MANAGER);
            }

            ValidateUtil.notNull(quotation, ErrorCode.QUOTATION_APPROVAL_QUOTATION_NO_REQUIRED);


            PartyMaster partyMaster = partyMasterRepository.getOne(quotation.getCustomer().getId());

            ValidateUtil.notNull(partyMaster, ErrorCode.QUOTATION_APPROVAL_CUSTOMER_REQUIRED);

            ValidateUtil.isStringEquals(dto.getPin(), partyMaster.getPartyDetail().getPin(), ErrorCode.QUOTATION_APPROVAL_PIN_INVALID);


            if (dto.getApproved().equals(Approved.Approved)) {

                for (QuotationDetail qd : quotation.getQuotationDetailList()) {
                    for (QuotationCarrier qc : qd.getQuotationCarrierList()) {
                        if (Objects.equals(dto.getQuotationCarrierId(), qc.getId())) {
                            qd.setQuotationCarrierId(dto.getQuotationCarrierId());
                            break;
                        }
                    }
                }

                quotation.setApproved(Approved.ClientApproved);
                quotation.setCustomerApprovedIp(request.getRemoteAddr());
                quotation.setCustomerApprovedDate(TimeUtil.getCurrentLocationTime(quotation.getLocationMaster()));
                quotationRepository.save(quotation);

            } else if (dto.getApproved().equals(Approved.Rejected)) {

                for (QuotationDetail qd : quotation.getQuotationDetailList()) {
                    for (QuotationCarrier qc : qd.getQuotationCarrierList()) {
                        if (Objects.equals(dto.getQuotationCarrierId(), qc.getId())) {
                            qd.setQuotationCarrierId(null);
                            break;
                        }
                    }
                }

                quotation.setRejectReason(dto.getRejectReason());
                quotation.setApproved(Approved.Rejected);
                quotation.setCustomerApprovedIp(request.getRemoteAddr());
                quotation.setCustomerApprovedDate(TimeUtil.getCurrentLocationTime(quotation.getLocationMaster()));
                quotationRepository.save(quotation);

            }

            baseDto.setResponseObject(dto.getApproved());
            baseDto.setResponseCode(ErrorCode.SUCCESS);

        } catch (RestException re) {
            log.error("Exception in updateStatus ", re);
            baseDto.setResponseCode(re.getMessage());

        } catch (Exception e) {
            log.error("Exception Cause  ::: ", e);
            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);
        }
        return appUtil.setDesc(baseDto);
    }

    public BaseDto getSurchargeMapping(Long chargeId) {
        BaseDto baseDto = new BaseDto();
        baseDto.setResponseCode(ErrorCode.SUCCESS);
        try {
            SurchargeMapping surchargeMapping = surchargeMappingRepository.getByChargeMaster(chargeId);
            if (surchargeMapping == null || surchargeMapping.getId() == null) {
                baseDto.setResponseObject(false);
            } else {
                baseDto.setResponseObject(true);
            }

        } catch (RestException re) {
            log.error("Exception in getSurchargeMapping ", re);
            baseDto.setResponseCode(re.getMessage());

        } catch (Exception e) {
            log.error("Exception Cause  ::: ", e);
            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);
        }

        return appUtil.setDesc(baseDto);
    }


    public BaseDto getQuotationChargeHistory(Long id) {

        log.info("getQuotationChargeHistor is called");

        BaseDto baseDto = new BaseDto();

        try {

            AuditReader reader = AuditReaderFactory.get(entityManager);

            AuditQuery query = reader.createQuery().forRevisionsOfEntity(QuotationCharge.class, false, true);

            // query.setFirstResult(searchDto.getSelectedPageNumber());

            // query.setMaxResults(searchDto.getRecordPerPage());

            QuotationCarrier detail = quotationDetailRepository.getQuotationCarrier(id);

            query.add(org.hibernate.envers.query.AuditEntity.property("quotationCarrier").eq(detail));

            List<Number> revList = query.getResultList();

            List<QuotationCharge> list = new ArrayList<>();

            if (revList != null && revList.size() >= 1) {
                for (Object obj : revList) {
                    Object[] objects = (Object[]) obj;
                    QuotationCharge quotationCharge = (QuotationCharge) objects[0];
                    AuditEntity auditEntity = (AuditEntity) objects[1];
                    RevisionType revisonType = (RevisionType) objects[2];
                    quotationCharge.setAuditDate(auditEntity.getRevisionDate());
                    quotationCharge.setRevisionType(revisonType.name());
                    list.add(quotationCharge);
                }
                baseDto.setResponseCode(ErrorCode.SUCCESS);
                baseDto.setResponseObject(list);
            } else {
                baseDto.setResponseCode(ErrorCode.NO_AUDIT_HISTORY_FOUND);
            }
        } catch (Exception e) {
            log.error("Exception in Audit Histyory of Quotation service" + e);
            baseDto.setResponseCode(ErrorCode.FAILED);
        }
        return baseDto;
    }

    public BaseDto isQuotationCreatedFromEnquiry(String enquiryNo) {
        BaseDto baseDto = new BaseDto();
        try {
            quotationValidator.checkQuotationNotAssociatedWithEnquiry(enquiryNo);
            baseDto.setResponseCode(ErrorCode.SUCCESS);
        } catch (RestException re) {
            log.error("RestException in checkQuotationNotAssociatedWithEnquiry method : "
                    + re);
            baseDto.setResponseCode(re.getMessage());
        } catch (Exception e) {
            log.error("Exception in checkQuotationNotAssociatedWithEnquiry method of Quotation service" + e);
            baseDto.setResponseCode(ErrorCode.FAILED);
        }
        return baseDto;
    }

    public BaseDto isClientApproved() {
        BaseDto baseDto = new BaseDto();
        try {
            DefaultMasterData clientApproved = defaultMasterDataRepository.findByCodeAndLocationId("quotation.client.approval.required", AuthService.getCurrentUser().getSelectedUserLocation().getId());
            baseDto.setResponseCode(ErrorCode.SUCCESS);
            baseDto.setResponseObject(clientApproved);
        } catch (RestException re) {
            log.error("RestException in isClientApproved method : "
                    + re);
            baseDto.setResponseCode(re.getMessage());
        } catch (Exception e) {
            log.error("Exception in isClientApproved method of Quotation service" + e);
            baseDto.setResponseCode(ErrorCode.FAILED);
        }
        return baseDto;
    }


    public BaseDto pushSave(Quotation entity) {


        BaseDto baseDto = new BaseDto();

        try {
            quotationRepository.save(entity);
            baseDto.setResponseCode(ErrorCode.SUCCESS);
        } catch (Exception e) {
            // TODO: handle exception
            baseDto.setResponseCode(ErrorCode.FAILED);
            log.info("exception in push api save", e);

        }
        return appUtil.setDesc(baseDto);
    }
}
