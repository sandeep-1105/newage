package com.efreightsuite.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.efreightsuite.common.component.CacheRepository;
import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.ReportMasterSearchDto;
import com.efreightsuite.enumeration.ReportName;
import com.efreightsuite.enumeration.YesNo;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.exception.UniqueKey;
import com.efreightsuite.model.ReportMaster;
import com.efreightsuite.repository.ReportMasterRepository;
import com.efreightsuite.search.ReportMasterSearchImpl;
import com.efreightsuite.util.AppUtil;
import com.efreightsuite.validation.ReportMasterValidator;
import groovy.util.logging.Log4j2;
import org.apache.commons.lang3.exception.ExceptionUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.orm.ObjectOptimisticLockingFailureException;
import org.springframework.orm.jpa.JpaObjectRetrievalFailureException;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class ReportMasterService {

    @Autowired
    private
    AppUtil appUtil;

    @Autowired
    CacheRepository cacheRepository;

    @Autowired
    private
    ReportMasterRepository reportMasterRepository;

    @Autowired
    private
    ReportMasterSearchImpl reportMasterSearchImpl;

    @Autowired
    private
    ReportMasterValidator reportMasterValidator;

    private Map<Enum<ReportName>, Enum<YesNo>> localMap = new HashMap<>();

    public BaseDto search(ReportMasterSearchDto searchDto) {
        BaseDto baseDto = new BaseDto();
        try {
            baseDto.setResponseObject(reportMasterSearchImpl.search(searchDto));
            baseDto.setResponseCode(ErrorCode.SUCCESS);
        } catch (Exception exception) {
            baseDto.setResponseCode(ErrorCode.FAILED);
        }
        return appUtil.setDesc(baseDto);
    }


    public BaseDto create(List<ReportMaster> reportMasterList) {

        BaseDto baseDto = new BaseDto();

        try {


            if (reportMasterList == null || reportMasterList.size() <= 0) {
                throw new RestException(ErrorCode.FAILED);
            }

            for (ReportMaster reportMaster : reportMasterList) {
                reportMasterValidator.validate(reportMaster);
                reportMaster.setReportMasterCode(reportMaster.getReportMasterCode().toUpperCase());
            }

            List<ReportMaster> reportMasterCreated = reportMasterRepository.save(reportMasterList);

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(reportMasterCreated);

        } catch (RestException exception) {

            baseDto.setResponseCode(exception.getMessage());

        } catch (Exception exception) {

            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(exception);

            // General error condition
            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            // Duplicate report Code

            if (exceptionCause1.contains(UniqueKey.UK_REPORT_REPORTCODE)) {
                baseDto.setResponseCode(ErrorCode.REPORT_CODE_ALREADY_EXIST);

            }

            if (exceptionCause1.contains(UniqueKey.UK_REPORT_REPORTNAME)) {
                baseDto.setResponseCode(ErrorCode.REPORT_NAME_ALREADY_EXIST);
            }

            if (exceptionCause1.contains(UniqueKey.UK_REPORT_REPORTKEY)) {
                baseDto.setResponseCode(ErrorCode.REPORT_KEY_ALREADY_EXIST);
            }

        }
        return appUtil.setDesc(baseDto);

    }

    /**
     * Update Carrier Rate Master
     *
     * @return
     */
    public BaseDto update(ReportMaster existingReportMaster) {

        BaseDto baseDto = new BaseDto();

        try {

            if (existingReportMaster == null) {

                throw new RestException(ErrorCode.REPORT_NOT_FOUND);

            }

            existingReportMaster.setReportMasterCode(existingReportMaster.getReportMasterCode().toUpperCase());

            reportMasterRepository.save(existingReportMaster);

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(existingReportMaster);


        } catch (RestException exception) {


            baseDto.setResponseCode(exception.getMessage());

        } catch (ObjectOptimisticLockingFailureException e) {


            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_LOCKED_RECORD);

        } catch (JpaObjectRetrievalFailureException e) {


            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_DELETED_RECORD);

        } catch (Exception exception) {


            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(exception);


            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);


            if (exceptionCause1.contains(UniqueKey.UK_REPORT_REPORTCODE)) {
                baseDto.setResponseCode(ErrorCode.REPORT_CODE_ALREADY_EXIST);

            }

            if (exceptionCause1.contains(UniqueKey.UK_REPORT_REPORTNAME)) {
                baseDto.setResponseCode(ErrorCode.REPORT_NAME_ALREADY_EXIST);
            }

            if (exceptionCause1.contains(UniqueKey.UK_REPORT_REPORTKEY)) {
                baseDto.setResponseCode(ErrorCode.REPORT_KEY_ALREADY_EXIST);
            }
        }

        return appUtil.setDesc(baseDto);
    }

    /**
     * Delete Carrier Rate Master
     *
     * @param id
     * @return
     */
    public BaseDto delete(Long id) {
        BaseDto baseDto = new BaseDto();
        try {

            reportMasterRepository.delete(id);

            baseDto.setResponseCode(ErrorCode.SUCCESS);

        } catch (DataIntegrityViolationException exception) {


            baseDto.setResponseCode(ErrorCode.ERROR_FK_CONSTRAINT);

        } catch (Exception exception) {

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);


            String rootCause = ExceptionUtils.getRootCauseMessage(exception);


            if (rootCause.contains("FK_")) {
                baseDto.setResponseCode(ErrorCode.CANNOT_DELETE_FK_RECORD);
            }
        }

        return appUtil.setDesc(baseDto);
    }


    private Map<Enum<ReportName>, Enum<YesNo>> reportMasterDataMap() {


        List<ReportMaster> rm = reportMasterRepository.findAll();
        for (ReportMaster data : rm) {
            localMap.put(data.getReportKey(), data.getIsReportEnable());
        }
        return localMap;
    }


    @SuppressWarnings("unused")
    private void setReportMap(Map<Enum<ReportName>, Enum<YesNo>> localMap) {
        this.localMap = localMap;
    }


    //get all report config while user login
    public BaseDto getAll() {
        BaseDto bd = new BaseDto();
        try {
            Map<Enum<ReportName>, Enum<YesNo>> map = reportMasterDataMap();
            bd.setResponseCode(ErrorCode.SUCCESS);
            bd.setResponseObject(map);
        } catch (RestException exception) {
            bd.setResponseCode(exception.getMessage());
        } catch (Exception exception) {
            bd.setResponseCode(ErrorCode.ERROR_GENERIC);
        }
        return appUtil.setDesc(bd);
    }

}
