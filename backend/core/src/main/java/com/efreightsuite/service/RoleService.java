package com.efreightsuite.service;

import java.util.List;
import java.util.Objects;

import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.FeatureDTO;
import com.efreightsuite.dto.RoleDTO;
import com.efreightsuite.dto.RoleFormDto;
import com.efreightsuite.dto.RoleSearchDto;
import com.efreightsuite.dto.SearchRequest;
import com.efreightsuite.enumeration.YesNo;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.ExceptionHelper;
import com.efreightsuite.exception.UniqueKey;
import com.efreightsuite.model.Feature;
import com.efreightsuite.model.Role;
import com.efreightsuite.model.RoleHasFeature;
import com.efreightsuite.repository.FeatureRepository;
import com.efreightsuite.repository.RolesRepository;
import com.efreightsuite.repository.UserProfileRepository;
import com.efreightsuite.search.RoleSearchImpl;
import com.efreightsuite.util.AppUtil;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.exception.ExceptionUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class RoleService {

    @Autowired
    private
    AppUtil appUtil;

    @Autowired
    private
    FeatureRepository featureRepository;

    @Autowired
    private
    RolesRepository roleRepository;

    @Autowired
    private
    RoleSearchImpl roleSearchImpl;

    @Autowired
    UserProfileRepository userProfileRepository;

    public BaseDto getAllFeatures(Long id) {
        BaseDto respDto = new BaseDto();

        try {

            List<Feature> featureList = featureRepository.findByIsApplicableOrderByIdAsc(YesNo.Yes);
            List<FeatureDTO> features = FeatureDTO.entityListToDataList(featureList);


            if (id == null) {
                respDto.setResponseObject(features);
            } else if (id != null && id == -1) {
                RoleDTO roleDto = new RoleDTO();
                roleDto.setFeaturesList(features);
                respDto.setResponseObject(roleDto);
            } else if (id != null && id != -1) {
                Role role = roleRepository.findById(id);
                RoleDTO roleDto = new RoleDTO(role);

                for (RoleHasFeature rhf : role.getFeatureList()) {
                    for (FeatureDTO dto : features) {
                        if (!Objects.equals(dto.getId(), rhf.getFeature().getId())) {
                            continue;
                        }

                        if (rhf.getValList() != null) {
                            dto.setValList("Yes");
                        }

                        if (rhf.getValCreate() != null) {
                            dto.setValCreate("Yes");
                        }

                        if (rhf.getValModify() != null) {
                            dto.setValModify("Yes");
                        }

                        if (rhf.getValView() != null) {
                            dto.setValView("Yes");
                        }

                        if (rhf.getValDelete() != null) {
                            dto.setValDelete("Yes");
                        }

                        if (rhf.getValDownload() != null) {
                            dto.setValDownload("Yes");
                        }
                    }
                }
                roleDto.setFeaturesList(features);
                /*Have to Change
				if(role.getUserList()!=null && role.getUserList().size()>0){
					List<UserDto> tmpList = new ArrayList<UserDto>();
					for(UserProfile us : role.getUserList()){
						UserDto tt = new UserDto();
						tt.setId(us.getId());
						tt.setStatus(us.getStatus());
						tt.setUserName(us.getUserName());
						tmpList.add(tt);
					}
					roleDto.setUserList(tmpList);
				}*/

                respDto.setResponseObject(roleDto);
            }

            respDto.setResponseCode(ErrorCode.SUCCESS);


        } catch (Exception e) {
            log.error("Exception getting from get all feature :", e);
            respDto.setResponseCode(ErrorCode.FAILED);
        }


        return appUtil.setDesc(respDto);
    }

    public BaseDto roleForm(RoleFormDto roleFromDto) {
        BaseDto respDto = new BaseDto();

        try {

            for (RoleDTO rd : roleFromDto.getSelectedList()) {
                Role role = roleRepository.getOne(rd.getId());

                for (RoleHasFeature rhf : role.getFeatureList()) {
                    for (FeatureDTO dto : roleFromDto.getRoleDto().getFeaturesList()) {

                        if (!Objects.equals(dto.getId(), rhf.getFeature().getId())) {
                            continue;
                        }

                        if (rhf.getValList() != null) {
                            dto.setValList("Yes");
                        }

                        if (rhf.getValCreate() != null) {
                            dto.setValCreate("Yes");
                        }

                        if (rhf.getValModify() != null) {
                            dto.setValModify("Yes");
                        }

                        if (rhf.getValView() != null) {
                            dto.setValView("Yes");
                        }

                        if (rhf.getValDelete() != null) {
                            dto.setValDelete("Yes");
                        }

                        if (rhf.getValDownload() != null) {
                            dto.setValDownload("Yes");
                        }
                    }
                }

            }

            respDto.setResponseObject(roleFromDto.getRoleDto());
            respDto.setResponseCode(ErrorCode.SUCCESS);


        } catch (Exception e) {
            log.error("Exception getting from get all feature :", e);
            respDto.setResponseCode(ErrorCode.FAILED);
        }


        return appUtil.setDesc(respDto);
    }
/*	public BaseDto getRoleWithFeature(Long roleId)  {
		
		BaseDto respDto = new BaseDto();
		
		try{
						
			Role roleEntity = roleRepository.getOne(roleId);
			
			List<Long> availFeature = null;
			if(roleEntity.getFeature() != null && roleEntity.getFeature().size() >0) {
				availFeature = new ArrayList<Long>();
				for(Feature feature : roleEntity.getFeature()) {
					availFeature.add(feature.getId());
				}
			
			}
			log.info("Available feature L:ist :: "+availFeature);
			RoleDTO roleDto = new RoleDTO();
			roleDto.setId(roleEntity.getId());
			roleDto.setDescription(roleEntity.getDescription());
			roleDto.setRoleName(roleEntity.getRoleName());
			roleDto.setStatus(roleEntity.getStatus());
			roleDto.setSystemTrack(roleEntity.getSystemTrack());
			
			//roleDto.setFeaturesList(chkFeatures(availFeature));
			
			roleDto.setUserList(new ArrayList<>());
			for(UserProfile user:roleEntity.getUserList()){
				user.setFeatureMap(null);
				user.setRoleList(null);
				user.setUserCompanyList(null);
				roleDto.getUserList().add(user);
			}
			
			respDto.setResponseCode(ErrorCode.SUCCESS);
			respDto.setResponseObject(roleDto);
			
		}catch(Exception e){
			log.error("Exception getting from get role : ",e);
			respDto.setResponseCode(ErrorCode.FAILED);
		}
		
		
		
		return appUtil.setDesc(respDto);	
	}*/
	
	
/*    public BaseDto getRoleWithOutFeature(Long roleId)  {
		
		BaseDto respDto = new BaseDto();
		
		try{
						
			Role roleEntity = roleRepository.getOne(roleId);
					
			
			
			RoleDTO roleDto = new RoleDTO();
			roleDto.setId(roleEntity.getId());
			roleDto.setDescription(roleEntity.getDescription());
			roleDto.setRoleName(roleEntity.getRoleName());
			roleDto.setStatus(roleEntity.getStatus());
			roleDto.setSystemTrack(roleEntity.getSystemTrack());
			
			
			
			respDto.setResponseCode(ErrorCode.SUCCESS);
			respDto.setResponseObject(roleDto);
			
		}catch(Exception e){
			log.error("Exception getting from get role : ",e);
			respDto.setResponseCode(ErrorCode.FAILED);
		}
		
		
		
		return appUtil.setDesc(respDto);	
	}*/

    public BaseDto search(SearchRequest searchRequest) {

        BaseDto respDto = new BaseDto();

        try {

            respDto.setResponseObject(roleSearchImpl.search(searchRequest));
            respDto.setResponseCode(ErrorCode.SUCCESS);
        } catch (Exception e) {
            log.error("Exception getting from get role : ", e);
            respDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(respDto);

    }

    public BaseDto saveOrUpdate(RoleDTO data) {
        BaseDto respDto = new BaseDto();

        try {
            Role role = roleSearchImpl.saveUpdate(data);
            respDto.setResponseCode(ErrorCode.SUCCESS);
            respDto.setResponseObject(role);

        } catch (Exception e) {

            String exceptionCause = ExceptionUtils.getRootCauseMessage(e);

            log.error("Exception getting from save or update role :", e);

            respDto.setResponseCode(ErrorCode.ERROR_GENERIC);
            if (exceptionCause.contains(UniqueKey.UK_ROLE_ROLENAME)) {
                respDto.setResponseCode(ErrorCode.ROLE_NAME_ALREADY_EXIST);
            }
            if (exceptionCause.contains(UniqueKey.UK_USER_ROLE)) {
                respDto.setResponseCode(ErrorCode.ROLE_USER_ALREADY_EXIST);
            }
            if (exceptionCause.contains(UniqueKey.UK_ROLEHASFEATURE_UNQ)) {
                respDto.setResponseCode(ErrorCode.ROLE_FEATURE_ALREADY_EXIST);
            }

        }

        return appUtil.setDesc(respDto);
    }


    public BaseDto delete(Long id) {

        log.info("Delete method is Invoked...[" + id + "]");

        BaseDto baseDto = new BaseDto();

        try {

            roleRepository.delete(id);

            log.info("TosMaster Deleted Successfully....[" + id + "]");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

        } catch (DataIntegrityViolationException exception) {

            log.error("Exception occured : ", exception);

            ExceptionHelper helper = new ExceptionHelper(exception);

            log.error("Foreign key reference : " + helper.getFkReference());

            baseDto.setResponseCode(ErrorCode.ERROR_FK_CONSTRAINT);

        } catch (Exception exception) {

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            log.error("Exception in Delete method : ", exception);

            String rootCause = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Root Cause of Exception : " + rootCause);

            if (rootCause.contains("FK_")) {
                baseDto.setResponseCode(ErrorCode.CANNOT_DELETE_FK_RECORD);
            }
        }

        return appUtil.setDesc(baseDto);
    }
	
	/*public List<FeatureDTO > chkFeatures(List<Long> availFeature) {
		List<Feature> featureList = featureRepository.findAllByParentAndIsApplicable(null,YesNo.Yes);

		log.info("Fetaure List size :: "+featureList.size()); 

		log.info("Avail Feature List :: "+availFeature); 
		List<FeatureDTO> featList = FeatureDTO.entityListToDataList(featureList, availFeature);
		
		return featList;
	}*/

    public BaseDto roleSearch(RoleSearchDto searchDto) {

        log.info("Search method is called...." + searchDto);

        BaseDto baseDto = new BaseDto();

        try {

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(roleSearchImpl.search(searchDto));


            log.info("Successfully Searching .");

        } catch (Exception exception) {

            log.error("Exception in Search method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);

    }


}
