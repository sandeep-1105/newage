package com.efreightsuite.service;


import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.PartyGroupSearchDto;
import com.efreightsuite.dto.SearchRequest;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.ExceptionHelper;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.exception.UniqueKey;
import com.efreightsuite.model.PartyGroupMaster;
import com.efreightsuite.repository.PartyGroupMasterRepository;
import com.efreightsuite.search.PartyGroupSearchImpl;
import com.efreightsuite.util.AppUtil;
import com.efreightsuite.validation.PartyGroupMasterValidator;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.exception.ExceptionUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.orm.ObjectOptimisticLockingFailureException;
import org.springframework.orm.jpa.JpaObjectRetrievalFailureException;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class PartyGroupMasterService {

    @Autowired
    private
    PartyGroupMasterRepository partyGroupMasterRepository;

    @Autowired
    private
    PartyGroupMasterValidator partyGroupMasterValidator;

    @Autowired
    private
    AppUtil appUtil;

    @Autowired
    private
    PartyGroupSearchImpl partyGroupSearchImpl;

    public BaseDto search(PartyGroupSearchDto partyGroupSearchDto) {

        log.info("Search method is called....");

        BaseDto baseDto = new BaseDto();

        try {

            baseDto.setResponseObject(partyGroupSearchImpl.search(partyGroupSearchDto));

            baseDto.setResponseCode(ErrorCode.SUCCESS);

        } catch (Exception exception) {

            log.error("Exception in Search method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);
    }


    public BaseDto search(SearchRequest searchRequest) {

        log.info("Search method is called....");

        BaseDto baseDto = new BaseDto();

        try {

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(partyGroupSearchImpl.search(searchRequest));

        } catch (Exception e) {

            log.error("Exception in Search method ", e);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);

    }


    public BaseDto get(Long id) {

        BaseDto baseDto = new BaseDto();

        try {

            PartyGroupMaster partyGroupMaster = partyGroupMasterRepository.getOne(id);

            log.info("Successfully getting PartyGroupMaster by id..[" + partyGroupMaster.getId() + "]");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(partyGroupMaster);

        } catch (Exception exception) {

            log.error("Exception in get method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);
    }

    public BaseDto create(PartyGroupMaster partyGroupMaster) {

        log.info("save method is called.....");

        BaseDto baseDto = new BaseDto();

        try {

            partyGroupMasterValidator.validate(partyGroupMaster);

            partyGroupMaster = partyGroupMasterRepository.save(partyGroupMaster);

            log.info("PartyGroup Saved successfully.....[ " + partyGroupMaster.getId() + "]");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(partyGroupMaster);

        } catch (RestException exception) {

            log.error("Exception in Create method : ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (Exception exception) {

            log.error("Failed to save the party group master..", exception);

            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Exception Cause 1 ::: " + exceptionCause1);

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            if (exceptionCause1.contains(UniqueKey.UK_PARTYGRP_PARTYGROUPCODE)) {
                log.error("Duplicate PartyGroup Code");
                baseDto.setResponseCode(ErrorCode.PARTY_GROUP_CODE_ALREADY_EXIST);
            }
            if (exceptionCause1.contains(UniqueKey.UK_PARTYGRP_PARTYGROUPNAME)) {
                log.error("Duplicate PartyGroup name");
                baseDto.setResponseCode(ErrorCode.PARTY_GROUP_NAME_ALREADY_EXIST);
            }
            if (exceptionCause1.contains(UniqueKey.UK_PARTYGROUP_PARTY)) {
                log.error("Duplicate Party in Party Group name");
                baseDto.setResponseCode(ErrorCode.PARTY_GROUP_PARTY_ALREADY_EXIST);
            }

        }

        return appUtil.setDesc(baseDto);
    }

    public BaseDto update(PartyGroupMaster partyGroupMaster) {

        log.info("Update method is called - partyGroupMaster : [" + partyGroupMaster.getId() + "]");

        BaseDto baseDto = new BaseDto();

        try {

            partyGroupMasterValidator.validate(partyGroupMaster);

            partyGroupMasterRepository.getOne(partyGroupMaster.getId());

            partyGroupMaster = partyGroupMasterRepository.save(partyGroupMaster);

            log.info("PartyTypeMaster updated successfully....[" + partyGroupMaster.getId() + "]");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(partyGroupMaster);

        } catch (RestException exception) {

            log.error("Exception occured with error code ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (ObjectOptimisticLockingFailureException e) {

            log.error("Error in editing partyTypeMaster", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_LOCKED_RECORD);

        } catch (JpaObjectRetrievalFailureException e) {

            log.error("Error in editing partyTypeMaster", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_DELETED_RECORD);

        } catch (Exception exception) {

            log.error("Failed to Update the party group master..", exception);


            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Exception Cause 1 ::: " + exceptionCause1);

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            if (exceptionCause1.contains(UniqueKey.UK_PARTYGRP_PARTYGROUPCODE)) {
                log.error("Duplicate PartyGroup Code");
                baseDto.setResponseCode(ErrorCode.PARTY_GROUP_CODE_ALREADY_EXIST);
            }

            if (exceptionCause1.contains(UniqueKey.UK_PARTYGRP_PARTYGROUPNAME)) {
                log.error("Duplicate PartyGroup name");
                baseDto.setResponseCode(ErrorCode.PARTY_GROUP_NAME_ALREADY_EXIST);
            }

            if (exceptionCause1.contains(UniqueKey.UK_PARTYGROUP_PARTY)) {
                log.error("Duplicate Party in Party Group name");
                baseDto.setResponseCode(ErrorCode.PARTY_GROUP_NAME_ALREADY_EXIST);
            }


        }

        return appUtil.setDesc(baseDto);
    }

    public BaseDto delete(Long id) {

        log.info("Delete method is Called...[" + id + "]");

        BaseDto baseDto = new BaseDto();

        try {

            partyGroupMasterRepository.delete(id);

            log.info("PartyGroupMaster Deleted Successfully....[" + id + "]");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

        } catch (DataIntegrityViolationException exception) {

            log.error("Exception occured : ", exception);

            ExceptionHelper helper = new ExceptionHelper(exception);

            log.error("Foreign key reference : " + helper.getFkReference());

            baseDto.setResponseCode(ErrorCode.ERROR_FK_CONSTRAINT);

        } catch (Exception exception) {

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            log.error("Exception in Delete method : ", exception);

            String rootCause = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Root Cause of Exception : " + rootCause);

            if (rootCause.contains("FK_")) {
                baseDto.setResponseCode(ErrorCode.CANNOT_DELETE_FK_RECORD);
            }
        }

        return appUtil.setDesc(baseDto);
    }

}
