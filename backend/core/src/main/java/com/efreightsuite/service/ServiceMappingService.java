package com.efreightsuite.service;

import java.util.List;

import com.efreightsuite.common.component.CacheRepository;
import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.ServiceMappingSearchDto;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.ExceptionHelper;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.exception.UniqueKey;
import com.efreightsuite.model.ServiceMapping;
import com.efreightsuite.repository.ServiceMappingRepository;
import com.efreightsuite.search.ServiceMappingSearchImpl;
import com.efreightsuite.util.AppUtil;
import com.efreightsuite.util.SaaSUtil;
import com.efreightsuite.validation.ServiceMappingValidator;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.exception.ExceptionUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;


@Service
@Log4j2
public class ServiceMappingService {

    @Autowired
    private
    AppUtil appUtil;

    @Autowired
    private
    CacheRepository cacheRepository;

    @Autowired
    private
    ServiceMappingSearchImpl serviceMappingSearchImpl;

    @Autowired
    private
    ServiceMappingRepository serviceMappingRepository;

    @Autowired
    private
    ServiceMappingValidator serviceMappingValidator;

    public BaseDto search(ServiceMappingSearchDto searchDto) {

        log.info("getAll method is called....");

        BaseDto baseDto = new BaseDto();

        try {

            log.info("Successfully getting list of serviceMapping...");
            baseDto.setResponseCode(ErrorCode.SUCCESS);
            baseDto.setResponseObject(serviceMappingSearchImpl.search(searchDto));

        } catch (RestException re) {
            log.error("Exception in getAll method ", re);
            baseDto.setResponseCode(re.getMessage());

        } catch (Exception e) {
            log.error("Exception in getAll method ", e);
            baseDto.setResponseCode(ErrorCode.FAILED);

        }
        return appUtil.setDesc(baseDto);
    }


    public BaseDto get(Long id) {
        log.info("get method is called [" + id + "]");
        BaseDto baseDto = new BaseDto();
        try {
            ServiceMapping enquiryServiceMapping = cacheRepository.get(SaaSUtil.getSaaSId(), ServiceMapping.class.getName(), id, serviceMappingRepository);
            log.info("Successfully getting ServiceMapping by id.....[" + id + "]");
            baseDto.setResponseCode(ErrorCode.SUCCESS);
            baseDto.setResponseObject(enquiryServiceMapping);
        } catch (RestException exception) {
            log.error("Exception in get method ", exception);
            baseDto.setResponseCode(exception.getMessage());
        } catch (Exception e) {
            log.error("Exception in get method", e);
            baseDto.setResponseCode(ErrorCode.FAILED);
        }
        return appUtil.setDesc(baseDto);
    }


    public BaseDto save(List<ServiceMapping> serviceMappingList) {
        BaseDto baseDto = new BaseDto();
        try {
            for (ServiceMapping serviceMapping : serviceMappingList) {
                serviceMappingValidator.validate(serviceMapping);
            }
            serviceMappingRepository.save(serviceMappingList);
            baseDto.setResponseCode(ErrorCode.SUCCESS);
            baseDto.setResponseObject(serviceMappingList);

        } catch (RestException exception) {
            log.error("Rest Exception occured with error code ", exception);
            baseDto.setResponseCode(exception.getMessage());
        } catch (Exception exception) {
            log.error("Exception occured ", exception);

            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Exception Cause 1 : " + exceptionCause1);

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            if (exceptionCause1.contains(UniqueKey.UK_SERMAP_IMPID)) {
                baseDto.setResponseCode(ErrorCode.SERVICE_MAPPING_ALREADY_EXIST);
            }
            if (exceptionCause1.contains(UniqueKey.UK_SERMAP_EXPID)) {
                baseDto.setResponseCode(ErrorCode.SERVICE_MAPPING_ALREADY_EXIST);
            }


        }

        return appUtil.setDesc(baseDto);
    }

    public BaseDto update(ServiceMapping serviceMapping) {
        BaseDto baseDto = new BaseDto();
        try {
            serviceMappingValidator.validate(serviceMapping);
            serviceMappingRepository.save(serviceMapping);
            cacheRepository.remove(SaaSUtil.getSaaSId(), ServiceMapping.class.getName(), serviceMapping.getId());
            baseDto.setResponseCode(ErrorCode.SUCCESS);
            baseDto.setResponseObject(serviceMapping);

        } catch (RestException exception) {
            log.error("Rest Exception occured with error code ", exception);
            baseDto.setResponseCode(exception.getMessage());
        } catch (Exception exception) {
            log.error("Exception occured ", exception);

            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Exception Cause 1 : " + exceptionCause1);

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            if (exceptionCause1.contains(UniqueKey.UK_SERMAP_IMPID)) {
                baseDto.setResponseCode(ErrorCode.SERVICE_MAPPING_ALREADY_EXIST);
            }
            if (exceptionCause1.contains(UniqueKey.UK_SERMAP_EXPID)) {
                baseDto.setResponseCode(ErrorCode.SERVICE_MAPPING_ALREADY_EXIST);
            }
        }

        return appUtil.setDesc(baseDto);
    }


    public BaseDto delete(Long id) {
        BaseDto baseDto = new BaseDto();
        try {
            cacheRepository.delete(SaaSUtil.getSaaSId(), ServiceMapping.class.getName(), id, serviceMappingRepository);
            baseDto.setResponseCode(ErrorCode.SUCCESS);
        } catch (DataIntegrityViolationException exception) {
            log.error("Exception occured : ", exception);
            ExceptionHelper helper = new ExceptionHelper(exception);
            log.error("Foreign key reference : " + helper.getFkReference());
            baseDto.setResponseCode(ErrorCode.ERROR_FK_CONSTRAINT);
        } catch (Exception exception) {
            log.error("Exception in Delete method : ", exception);
            baseDto.setResponseCode(ErrorCode.FAILED);
        }
        return appUtil.setDesc(baseDto);
    }

}
