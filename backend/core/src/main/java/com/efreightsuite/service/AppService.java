package com.efreightsuite.service;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.enumeration.AddressType;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.repository.LanguageDetailRepository;
import com.efreightsuite.util.AppUtil;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class AppService {

    @Autowired
    private
    AppUtil appUtil;

    @Autowired
    private
    LanguageDetailRepository languageDetailRepository;

    public BaseDto getAllTimZones() {
        log.info("Get All Time Zone");

        String[] allTimeZones = TimeZone.getAvailableIDs();

        Arrays.sort(allTimeZones);

        BaseDto baseDto = new BaseDto();
        baseDto.setResponseCode(ErrorCode.SUCCESS);
        baseDto.setResponseObject(Arrays.asList(allTimeZones));

        return appUtil.setDesc(baseDto);
    }

    public BaseDto getAllEnums() {
        log.info("getAllEnums");

        Map<String, Object[]> map = new HashMap<>();

        for (Class className : appUtil
                .getAllClasses(AddressType.class.getName().replaceAll("." + AddressType.class.getSimpleName(), ""))) {

            List<String> values = new ArrayList<>();

            for (Field field : className.getDeclaredFields()) {
                if (!field.getName().contains("$")) {
                    values.add(field.getName());
                }

            }

            map.put(className.getSimpleName(), values.toArray());
        }

        List<String> groupList = languageDetailRepository.fetchAllGroupName();

        map.put("ErrorGroup", groupList.toArray());

        BaseDto baseDto = new BaseDto();
        baseDto.setResponseCode(ErrorCode.SUCCESS);
        baseDto.setResponseObject(map);

        return appUtil.setDesc(baseDto);
    }

}
