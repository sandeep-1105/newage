package com.efreightsuite.service;

import java.util.List;

import com.efreightsuite.common.component.CacheRepository;
import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.SearchRequest;
import com.efreightsuite.dto.SoftwareMasterSearchDto;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.ExceptionHelper;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.exception.UniqueKey;
import com.efreightsuite.model.SoftwareMaster;
import com.efreightsuite.repository.SoftwareMasterRepository;
import com.efreightsuite.search.SoftwareSearchImpl;
import com.efreightsuite.util.AppUtil;
import com.efreightsuite.util.SaaSUtil;
import com.efreightsuite.util.ValidateUtil;
import com.efreightsuite.validation.SoftwareMasterValidator;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.exception.ExceptionUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class SoftwareMasterService {

    @Autowired
    private
    AppUtil appUtil;

    @Autowired
    private
    SoftwareMasterRepository softwareMasterRepository;

    @Autowired
    private
    CacheRepository cacheRepository;

    @Autowired
    private
    SoftwareSearchImpl softwareSearchImpl;

    @Autowired
    private
    SoftwareMasterValidator softwareMasterValidator;

    public BaseDto search(SearchRequest searchRequest) {

        log.info("getAll method is called....");

        BaseDto baseDto = new BaseDto();

        try {

            baseDto.setResponseCode(ErrorCode.SUCCESS);
            baseDto.setResponseObject(softwareSearchImpl.search(searchRequest));

        } catch (RestException re) {
            log.info("Exception in getAll method ", re);
            baseDto.setResponseCode(re.getMessage());

        } catch (Exception e) {
            log.info("Exception in getAll method ", e);
            baseDto.setResponseCode(ErrorCode.FAILED);

        }
        return appUtil.setDesc(baseDto);
    }

    public BaseDto search(SoftwareMasterSearchDto searchDto) {

        log.info("getAll method is called....");

        BaseDto baseDto = new BaseDto();

        try {

            log.info("Successfully getting list of zone...");
            baseDto.setResponseCode(ErrorCode.SUCCESS);
            baseDto.setResponseObject(softwareSearchImpl.search(searchDto));

        } catch (RestException re) {
            log.info("Exception in getAll method ", re);
            baseDto.setResponseCode(re.getMessage());

        } catch (Exception e) {
            log.info("Exception in getAll method ", e);
            baseDto.setResponseCode(ErrorCode.FAILED);

        }
        return appUtil.setDesc(baseDto);
    }

    public BaseDto get(Long id) {

        log.info("get method is called [" + id + "]");

        BaseDto baseDto = new BaseDto();

        try {

            SoftwareMaster softwareMaster = cacheRepository.get(SaaSUtil.getSaaSId(), SoftwareMaster.class.getName(), id, softwareMasterRepository);

            ValidateUtil.notNull(softwareMaster, ErrorCode.SOFTWARE_ID_NOT_FOUND);

            log.info("Successfully getting Software by id.....[" + id + "]");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(softwareMaster);

        } catch (RestException exception) {

            log.info("Exception in get method ", exception);

            baseDto.setResponseCode(exception.getMessage());
        } catch (Exception e) {

            log.info("Exception in get method", e);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);
    }

    public BaseDto saveOrUpdate(SoftwareMaster softwareMaster) {

        BaseDto baseDto = new BaseDto();

        try {

            if (softwareMaster.getId() != null) {

                log.info("update method is Invoked....[ " + softwareMaster.getId() + "]");

                ValidateUtil.notNull(softwareMaster.getId(), ErrorCode.SOFTWARE_ID_NOT_FOUND);

                if (softwareMaster.getId() != null) {
                    softwareMasterRepository.getOne(softwareMaster.getId());
                }

                softwareMasterValidator.validate(softwareMaster);


                softwareMasterRepository.save(softwareMaster);

                cacheRepository.remove(SaaSUtil.getSaaSId(), SoftwareMaster.class.getName(), softwareMaster.getId());

                log.info("SoftwareMaster Updated Successfully...[" + softwareMaster.getId() + "]");

                baseDto.setResponseCode(ErrorCode.SUCCESS);

                baseDto.setResponseObject(softwareMaster);

            } else {

                log.info("Create method is Invoked....[ " + softwareMaster.getId() + "]");

                softwareMasterValidator.validate(softwareMaster);

                softwareMaster = cacheRepository.save(SaaSUtil.getSaaSId(), SoftwareMaster.class.getName(), softwareMaster, softwareMasterRepository);

                log.info("SoftwareMaster Saved Successfully...[" + softwareMaster.getId() + "]");

                baseDto.setResponseCode(ErrorCode.SUCCESS);

                baseDto.setResponseObject(softwareMaster);

            }

        } catch (RestException exception) {

            log.info("Rest Exception Occured with error code ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (Exception exception) {

            log.info("Exception occured ", exception);

            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(exception);

            log.info("Exception Cause 1 : " + exceptionCause1);

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            if (exceptionCause1.contains(UniqueKey.UK_SOFTWARE_SOFTWARECODE)) {
                baseDto.setResponseCode(ErrorCode.SOFTWARE_CODE_ALREADY_EXIST);
            }

            if (exceptionCause1.contains(UniqueKey.UK_SOFTWARE_SOFTWARENAME)) {
                baseDto.setResponseCode(ErrorCode.SOFTWARE_NAME_ALREADY_EXIST);
            }
        }

        return appUtil.setDesc(baseDto);
    }

    public BaseDto delete(Long id) {

        log.info("Delete method is Invoked...[" + id + "]");

        BaseDto baseDto = new BaseDto();

        try {

            cacheRepository.delete(SaaSUtil.getSaaSId(), SoftwareMaster.class.getName(), id, softwareMasterRepository);

            log.info("SoftwareMaster Deleted Successfully....[" + id + "]");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

        } catch (DataIntegrityViolationException exception) {

            log.info("Exception occured : ", exception);

            ExceptionHelper helper = new ExceptionHelper(exception);

            log.info("Foreign key reference : " + helper.getFkReference());

            baseDto.setResponseCode(ErrorCode.ERROR_FK_CONSTRAINT);

        } catch (Exception exception) {

            log.info("Exception in Delete method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);
    }

    public BaseDto getAll() {

        log.info("getAll method is called..");

        BaseDto baseDto = new BaseDto();

        try {

            List<SoftwareMaster> softwareMasterList = softwareMasterRepository.findAll();

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(softwareMasterList);

            log.info("getAll method is executed successfully..");

        } catch (RestException exception) {

            log.info("Exception in getAll method : ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (Exception exception) {

            log.info("Exception in getAll method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);
    }

}
