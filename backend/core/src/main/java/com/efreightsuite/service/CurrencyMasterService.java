package com.efreightsuite.service;

import com.efreightsuite.common.component.CacheRepository;
import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.CurrencySearchDto;
import com.efreightsuite.dto.SearchRequest;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.ExceptionHelper;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.exception.UniqueKey;
import com.efreightsuite.model.CountryMaster;
import com.efreightsuite.model.CurrencyMaster;
import com.efreightsuite.repository.CountryMasterRepository;
import com.efreightsuite.repository.CurrencyMasterRepository;
import com.efreightsuite.search.CurrencySearchImpl;
import com.efreightsuite.util.AppUtil;
import com.efreightsuite.util.SaaSUtil;
import com.efreightsuite.util.ValidateUtil;
import com.efreightsuite.validation.CountryMasterValidator;
import com.efreightsuite.validation.CurrencyMasterValidator;
import com.efreightsuite.validation.SearchDtoValidator;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.exception.ExceptionUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.orm.ObjectOptimisticLockingFailureException;
import org.springframework.orm.jpa.JpaObjectRetrievalFailureException;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class CurrencyMasterService {

    @Autowired
    private
    AppUtil appUtil;

    @Autowired
    private
    CacheRepository cacheRepository;

    @Autowired
    private
    CurrencyMasterRepository currencyMasterRepository;

    @Autowired
    private
    CountryMasterRepository countryMasterRepository;

    @Autowired
    private
    CurrencyMasterValidator currencyMasterValidator;

    @Autowired
    CountryMasterValidator countryMasterValidator;

    @Autowired
    SearchDtoValidator searchDtoValidator;

    @Autowired
    private
    CurrencySearchImpl currencySearchImpl;

    /**
     * Method - getByCurrencyId
     *
     * @param id
     * @return baseDto
     */

    public BaseDto getByCurrencyId(Long id) {

        log.info("getByCurrencyId method is Invoked....[" + id + "]");

        BaseDto baseDto = new BaseDto();

        try {

            ValidateUtil.notNull(id, ErrorCode.CURRENCY_ID_NOT_NULL);

            CurrencyMaster currencyMaster = cacheRepository.get(SaaSUtil.getSaaSId(), CurrencyMaster.class.getName(),
                    id, currencyMasterRepository);

            ValidateUtil.notNull(currencyMaster, ErrorCode.CURRENCY_ID_NOT_FOUND);

            log.info("Successfully getting CurrencyMaster by id...[" + currencyMaster.getId() + "]");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(currencyMaster);

        } catch (RestException exception) {

            log.error("Exception in getByCurrencyId method : ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (Exception exception) {

            log.error("Exception in getByCurrencyId method", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);
    }

    /**
     * Method - search Returns list of CurrencyMaster based on the search
     * keyword keyword
     *
     * @return BaseDto
     */

    public BaseDto search(CurrencySearchDto searchDto) {

        log.info("Search method is called...." + searchDto);

        BaseDto baseDto = new BaseDto();

        try {

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(currencySearchImpl.search(searchDto));

            log.info("Successfully Searching Currency...");

        } catch (Exception exception) {

            log.error("Exception in Search method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);

    }

    public BaseDto search(SearchRequest searchRequest) {

        log.info("Search method is called...." + searchRequest);

        BaseDto baseDto = new BaseDto();

        try {

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(currencySearchImpl.search(null, searchRequest));

            log.info("Successfully Searching Currency...");

        } catch (Exception exception) {

            log.error("Exception in Search method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);

    }


    public BaseDto search(SearchRequest searchRequest, String excludeCurrencyCode) {

        log.info("Search method is called..excludeCurrencyCode.." + excludeCurrencyCode);

        BaseDto baseDto = new BaseDto();

        try {

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(currencySearchImpl.search(excludeCurrencyCode, searchRequest));

            log.info("Successfully Searching ...");

        } catch (Exception exception) {

            log.error("Exception in Search method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);

    }


    /**
     * create - This method creates a new Currency Master
     *
     * @Param CurrencyMaster entity
     * @Return BaseDto
     */

    public BaseDto create(CurrencyMaster currencyMaster) {

        log.info("Create method is called.....");

        BaseDto baseDto = new BaseDto();

        try {

            currencyMasterValidator.validate(currencyMaster);

            CountryMaster countryMaster = cacheRepository.get(SaaSUtil.getSaaSId(), CountryMaster.class.getName(),
                    currencyMaster.getCountryMaster().getId(), countryMasterRepository);

            currencyMaster.setCountryMaster(countryMaster);
            currencyMaster.setCountryCode(countryMaster.getCountryCode());


            if (currencyMaster.getSymbol() != null) {
                currencyMaster.setSymbol(currencyMaster.getSymbol().trim());
            }


            // Persist the CurrencyMaster in database
            CurrencyMaster currencyMasterCreated = cacheRepository.save(SaaSUtil.getSaaSId(), CurrencyMaster.class.getName(), currencyMaster,
                    currencyMasterRepository);

            log.info("CurrencyMaster Saved successfully.....[" + currencyMasterCreated.getId() + "]");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(currencyMasterCreated);

        } catch (RestException exception) {

            log.error("Exception in create method : ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (Exception exception) {
            log.error("Exception ", exception);
            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Exception Cause 1 ::: " + exceptionCause1);

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            if (exceptionCause1.contains(UniqueKey.UK_CURRENCY_CURRENCYCODE)) {
                baseDto.setResponseCode(ErrorCode.CURRENCY_CODE_ALREADY_EXIST);
            }

            if (exceptionCause1.contains(UniqueKey.UK_CURRENCY_CURRENCYNAME)) {
                baseDto.setResponseCode(ErrorCode.CURRENCY_NAME_ALREADY_EXIST);
            }

        }
        return appUtil.setDesc(baseDto);
    }

    public BaseDto update(CurrencyMaster existingCurrencyMaster) {

        log.info("Update method is called.... [" + existingCurrencyMaster.getId() + "]");

        BaseDto baseDto = new BaseDto();

        try {

            currencyMasterValidator.validate(existingCurrencyMaster);

            ValidateUtil.notNull(existingCurrencyMaster.getId(), ErrorCode.CURRENCY_ID_NOT_NULL);

            currencyMasterRepository.getOne(existingCurrencyMaster.getId());

            if (existingCurrencyMaster.getSymbol() != null) {
                existingCurrencyMaster.setSymbol(existingCurrencyMaster.getSymbol().trim());
            }

            existingCurrencyMaster = cacheRepository.save(SaaSUtil.getSaaSId(), CurrencyMaster.class.getName(), existingCurrencyMaster,
                    currencyMasterRepository);

            log.info("Currency Master Updated successfully...[" + existingCurrencyMaster.getId() + "]");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(existingCurrencyMaster);

        } catch (RestException exception) {

            log.error("Exception in create method : ", exception);
            String errorCode = exception.getMessage();
            baseDto.setResponseCode(errorCode);
        } catch (ObjectOptimisticLockingFailureException e) {

            log.error("Error in editing Currency", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_LOCKED_RECORD);

        } catch (JpaObjectRetrievalFailureException e) {

            log.error("Error in editing Currency", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_DELETED_RECORD);

        } catch (Exception exception) {

            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Exception Cause 1 ::: " + exceptionCause1);

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            if (exceptionCause1.contains(UniqueKey.UK_CURRENCY_CURRENCYCODE)) {
                baseDto.setResponseCode(ErrorCode.CURRENCY_NAME_ALREADY_EXIST);
            }

            if (exceptionCause1.contains(UniqueKey.UK_CURRENCY_CURRENCYNAME)) {
                baseDto.setResponseCode(ErrorCode.CURRENCY_NAME_ALREADY_EXIST);
            }


        }
        return appUtil.setDesc(baseDto);
    }


    public BaseDto delete(Long id) {

        log.info("Delete method is invoked : [ " + id + "]");

        BaseDto baseDto = new BaseDto();

        try {
            cacheRepository.delete(SaaSUtil.getSaaSId(), CurrencyMaster.class.getName(), id, currencyMasterRepository);

            log.info("CurrencyMaster Deleted Successfully.... [ " + id + "]");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

        } catch (DataIntegrityViolationException exception) {

            log.error("Exception occured : ", exception);

            ExceptionHelper helper = new ExceptionHelper(exception);

            log.error("Foreign key reference : " + helper.getFkReference());

            baseDto.setResponseCode(ErrorCode.ERROR_FK_CONSTRAINT);

        } catch (Exception exception) {

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            log.error("Exception in Delete method : ", exception);

            String rootCause = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Root Cause of Exception : " + rootCause);

            if (rootCause.contains("FK_")) {
                baseDto.setResponseCode(ErrorCode.CANNOT_DELETE_FK_RECORD);
            }
        }

        return appUtil.setDesc(baseDto);
    }


}
