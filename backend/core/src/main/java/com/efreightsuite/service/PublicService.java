package com.efreightsuite.service;

import java.util.StringJoiner;
import java.util.function.Supplier;

import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.DatatypeConverter;

import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.SearchRequest;
import com.efreightsuite.dto.SearchRespDto;
import com.efreightsuite.enumeration.EnquiryStatus;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.model.DocumentDetail;
import com.efreightsuite.model.EnquiryDetail;
import com.efreightsuite.model.EnquiryLog;
import com.efreightsuite.model.LogoMaster;
import com.efreightsuite.model.RateRequest;
import com.efreightsuite.repository.DocumentDetailRepository;
import com.efreightsuite.repository.EnquiryLogRepository;
import com.efreightsuite.repository.LogoMasterRepository;
import com.efreightsuite.repository.RateRequestRepository;
import com.efreightsuite.repository.common.CommonCityMasterRepository;
import com.efreightsuite.repository.common.CommonCountryMasterRepository;
import com.efreightsuite.repository.common.CommonRegionMasterRepository;
import com.efreightsuite.repository.common.CommonServiceMasterRepository;
import com.efreightsuite.repository.common.CommonStateMasterRepository;
import com.efreightsuite.search.PublicSearchImpl;
import com.efreightsuite.util.AppUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class PublicService {

    private final Logger log = LoggerFactory.getLogger(PublicService.class);

    @Autowired
    private
    AppUtil appUtil;

    @Autowired
    private
    DocumentDetailRepository documentDetailRepository;

    @Autowired
    private
    EnquiryLogRepository enquiryLogRepository;

    @Autowired
    private
    RateRequestRepository rateRequestRepository;

    @Autowired
    private
    LogoMasterRepository logoMasterRepository;

    @Autowired
    private
    PublicSearchImpl publicSearchImpl;

    @Autowired
    private
    MetaConfigurationService metaConfigurationService;

    @Autowired
    private
    CommonServiceMasterRepository commonServiceMasterRepository;

    @Autowired
    private CommonCountryMasterRepository commonCountryMasterRepository;

    @Autowired
    private CommonCityMasterRepository commonCityMasterRepository;

    @Autowired
    private CommonStateMasterRepository commonStateMasterRepository;

    @Autowired
    private CommonRegionMasterRepository commonRegionMasterRepository;

    public ResponseEntity<BaseDto> searchCommonCountries(SearchRequest searchRequest) {
        log.info("Fetching countries for Keyword {}", searchRequest.getKeyword());
        return search(searchRequest, () -> commonCountryMasterRepository
                .searchCountries(searchRequest.getKeyword(),
                        new PageRequest(searchRequest.getSelectedPageNumber(), searchRequest.getRecordPerPage())));
    }

    public BaseDto getAllCommonServices() {

        log.info("getAllCommonServices method is called....");

        BaseDto baseDto = new BaseDto();

        try {

            log.info("Successfully getting list of country...");
            baseDto.setResponseCode(ErrorCode.SUCCESS);
            baseDto.setResponseObject(commonServiceMasterRepository.findAll());
        } catch (Exception e) {
            log.error("Exception in getAll method ", e);
            baseDto.setResponseCode(ErrorCode.FAILED);

        }
        return appUtil.setDesCommon(baseDto);
    }

    public BaseDto searchCommonCurrency(SearchRequest searchRequest) {

        log.info("searchCommonCurrency method is called....");

        BaseDto baseDto = new BaseDto();

        try {

            log.info("Successfully getting list of currency...");
            baseDto.setResponseCode(ErrorCode.SUCCESS);
            baseDto.setResponseObject(publicSearchImpl.searchCommonCurrency(searchRequest));
        } catch (Exception e) {
            log.error("Exception in getAll method ", e);
            baseDto.setResponseCode(ErrorCode.FAILED);

        }
        return appUtil.setDesCommon(baseDto);
    }


    public BaseDto searchCommonLanguage(SearchRequest searchRequest) {

        log.info("searchCommonLanguage method is called....");

        BaseDto baseDto = new BaseDto();

        try {

            log.info("Successfully getting list of language...");
            baseDto.setResponseCode(ErrorCode.SUCCESS);
            baseDto.setResponseObject(publicSearchImpl.searchCommonLanguage(searchRequest));
        } catch (Exception e) {
            log.error("Exception in getAll method ", e);
            baseDto.setResponseCode(ErrorCode.FAILED);

        }
        return appUtil.setDesCommon(baseDto);
    }


    public ResponseEntity<BaseDto> searchCommonCities(SearchRequest searchRequest, Long countryId, Long stateId) {
        log.info("Fetching cities for Keyword {}", searchRequest.getKeyword());
        return search(
                searchRequest,
                () -> commonCityMasterRepository
                        .searchCities(
                                searchRequest.getKeyword(),
                                new PageRequest(searchRequest.getSelectedPageNumber(), searchRequest.getRecordPerPage())));
    }

    public ResponseEntity<BaseDto> searchCommonStates(SearchRequest searchRequest, Long countryId) {
        log.info("Fetching states for Keyword {}", searchRequest.getKeyword());
        return search(
                searchRequest,
                () -> commonStateMasterRepository
                        .searchStates(
                                searchRequest.getKeyword(),
                                new PageRequest(searchRequest.getSelectedPageNumber(), searchRequest.getRecordPerPage())));

    }


    private <T> ResponseEntity<BaseDto> search(SearchRequest searchRequest, Supplier<Page<T>> supplier) {
        log.info("Searching for Keyword {}", searchRequest.getKeyword());
        try {
            Page<T> pageResult = supplier.get();
            SearchRespDto searchResults = new SearchRespDto(pageResult.getTotalElements(), pageResult.getContent());
            BaseDto baseDto = new BaseDto(ErrorCode.SUCCESS, appUtil.getDesc(ErrorCode.SUCCESS), searchResults);
            return ResponseEntity
                    .ok()
                    .body(baseDto);
        } catch (Exception e) {
            log.error("Exception in search ", e);
            BaseDto baseDto = new BaseDto(ErrorCode.FAILED, appUtil.getDesc(ErrorCode.FAILED));
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(baseDto);
        }
    }

    public ResponseEntity<BaseDto> searchCommonRegions(SearchRequest searchRequest) {
        log.info("Fetching region for Keyword {}", searchRequest.getKeyword());
        return search(
                searchRequest,
                () -> commonRegionMasterRepository
                        .searchRegions(
                                searchRequest.getKeyword(),
                                new PageRequest(searchRequest.getSelectedPageNumber(), searchRequest.getRecordPerPage())));

    }

    public BaseDto searchCommonDocumentType(SearchRequest searchRequest) {

        log.info("searchCommonDocumentType method is called....");

        BaseDto baseDto = new BaseDto();

        try {

            baseDto.setResponseCode(ErrorCode.SUCCESS);
            baseDto.setResponseObject(publicSearchImpl.searchCommonDocumentType(searchRequest));
            log.info("Successfully getting list of document type...");
        } catch (Exception e) {
            log.error("Exception in getAll method ", e);
            baseDto.setResponseCode(ErrorCode.FAILED);

        }
        return appUtil.setDesCommon(baseDto);
    }


    public BaseDto searchCommonTimeZone(SearchRequest searchRequest) {

        log.info("searchCommonTimeZone method is called....");

        BaseDto baseDto = new BaseDto();

        try {

            baseDto.setResponseCode(ErrorCode.SUCCESS);
            baseDto.setResponseObject(publicSearchImpl.searchCommonTimeZone(searchRequest));
            log.info("Successfully getting list of regions...");
        } catch (Exception e) {
            log.error("Exception in getAll method ", e);
            baseDto.setResponseCode(ErrorCode.FAILED);

        }
        return appUtil.setDesCommon(baseDto);
    }

    public BaseDto searchCommonServiceNotInList(SearchRequest searchRequest) {

        log.info("searchServiceNotInList method is called....");

        BaseDto baseDto = new BaseDto();

        try {

            baseDto.setResponseObject(publicSearchImpl.searchCommonServiceNotInList(searchRequest));

            baseDto.setResponseCode(ErrorCode.SUCCESS);

        } catch (Exception exception) {

            log.error("Exception in searchServiceNotInLpuist method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesCommon(baseDto);
    }


    public BaseDto getDocumentDetail(Long documentId) {
        log.info("getDocumentDetail method is called.");
        BaseDto baseDto = new BaseDto();
        DocumentDetail documentDetail = documentDetailRepository.getOne(documentId);
        baseDto.setResponseObject(documentDetail);
        baseDto.setResponseCode(ErrorCode.SUCCESS);
        return appUtil.setDesc(baseDto);
    }


    //geting Rate Request based on the id
    public BaseDto getRateRequest(Long rateRequestId) {
        log.info("Get Rate Request method is called.");
        BaseDto baseDto = new BaseDto();
        if (rateRequestId != null) {
            RateRequest rateRequest = rateRequestRepository.findOne(rateRequestId);
            if (rateRequest != null && rateRequest.getEnquiryDetail().getEnquiryLog().getLocationMaster() != null) {
                LogoMaster logoMaster = logoMasterRepository.findByLocationId(rateRequest.getEnquiryDetail().getEnquiryLog().getLocationMaster().getId());
                if (logoMaster != null && logoMaster.getLogo() != null) {
                    rateRequest.setEncodedLogo(DatatypeConverter.printBase64Binary(logoMaster.getLogo()));
                }

                String address = "";
                if (rateRequest.getEnquiryDetail().getEnquiryLog().getLocationMaster() != null) {
                    StringJoiner joiner = new StringJoiner(",");
                    joiner.add((rateRequest.getEnquiryDetail().getEnquiryLog().getCompanyMaster() != null ? rateRequest.getEnquiryDetail().getEnquiryLog().getCompanyMaster().getCompanyName() : "")).add((rateRequest.getEnquiryDetail().getEnquiryLog().getLocationMaster().getAddressLine1() != null ? rateRequest.getEnquiryDetail().getEnquiryLog().getLocationMaster().getAddressLine1() : "")).add((rateRequest.getEnquiryDetail().getEnquiryLog().getLocationMaster().getAddressLine2() != null ? rateRequest.getEnquiryDetail().getEnquiryLog().getLocationMaster().getAddressLine2() : "")).add((rateRequest.getEnquiryDetail().getEnquiryLog().getLocationMaster().getAddressLine3() != null ? rateRequest.getEnquiryDetail().getEnquiryLog().getLocationMaster().getAddressLine3() : "")).add((rateRequest.getEnquiryDetail().getEnquiryLog().getLocationMaster().getAddressLine4() != null ? rateRequest.getEnquiryDetail().getEnquiryLog().getLocationMaster().getAddressLine4() : ""));
                    address = joiner.toString();
                }
                if (address != null && address.trim().length() > 0) {
                    rateRequest.setCompanyAddress((address).substring(0, address.length() - 1));
                }
            }
            baseDto.setResponseObject(rateRequest);
            baseDto.setResponseCode(ErrorCode.SUCCESS);
        } else {
            baseDto.setResponseCode(ErrorCode.FAILED);
        }
        return appUtil.setDesc(baseDto);
    }


    public BaseDto getEnquiryDetail(Long rateRequestId) {
        log.info("Get Enquiry Detail method is called.");
        BaseDto baseDto = new BaseDto();
        EnquiryDetail ed = null;
        if (rateRequestId != null) {
            ed = rateRequestRepository.findEnquiryDetailId(rateRequestId);
            if (ed != null) {
                baseDto.setResponseObject(ed);
                baseDto.setResponseCode(ErrorCode.SUCCESS);
            }
        } else {
            baseDto.setResponseCode(ErrorCode.FAILED);
        }
        return appUtil.setDesc(baseDto);
    }


    public BaseDto statusOfEnquiry(String enquiryNo) {

        BaseDto baseDto = new BaseDto();
        if (enquiryNo != null) {
            log.info("Get Enquiry Detail method is called.");
            EnquiryLog el = enquiryLogRepository.findByEnquiryNo(enquiryNo);
            if (el != null) {
                if (el.getStatus().equals(EnquiryStatus.Active) && el.getQuotationNo() == null) {
                    baseDto.setResponseCode(ErrorCode.SUCCESS);
                } else {
                    baseDto.setResponseCode(ErrorCode.FAILED);
                }
            }
        }
        return appUtil.setDesc(baseDto);
    }


    public BaseDto getLogoByLocation(Long locationId) {
        BaseDto baseDto = new BaseDto();
        if (locationId != null) {
            log.info("get Logo ByLocation method is called.");
            LogoMaster logoMaster = logoMasterRepository.findByLocationId(locationId);
            baseDto.setResponseObject(logoMaster);
            baseDto.setResponseCode(ErrorCode.SUCCESS);
        }
        return appUtil.setDesc(baseDto);
    }


    public void downloadTemplate(HttpServletResponse response, String type) {
        metaConfigurationService.downloadTemplate(response, type);
    }

    public void downloadChargeTemplate(HttpServletResponse response, String type) {
        metaConfigurationService.downloadChargeTemplate(response, type);
    }

}
