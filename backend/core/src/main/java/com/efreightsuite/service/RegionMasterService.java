package com.efreightsuite.service;

import com.efreightsuite.common.component.CacheRepository;
import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.RegionSearchDto;
import com.efreightsuite.dto.SearchRequest;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.ExceptionHelper;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.exception.UniqueKey;
import com.efreightsuite.model.RegionMaster;
import com.efreightsuite.repository.RegionMasterRepository;
import com.efreightsuite.search.RegionSearchImpl;
import com.efreightsuite.util.AppUtil;
import com.efreightsuite.util.SaaSUtil;
import com.efreightsuite.util.ValidateUtil;
import com.efreightsuite.validation.RegionMasterValidator;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.exception.ExceptionUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.orm.ObjectOptimisticLockingFailureException;
import org.springframework.orm.jpa.JpaObjectRetrievalFailureException;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class RegionMasterService {

    @Autowired
    private
    RegionMasterValidator regionMasterValidator;

    @Autowired
    private
    CacheRepository cacheRepository;

    @Autowired
    private
    RegionMasterRepository regionMasterRepository;

    @Autowired
    private
    RegionSearchImpl regionSearchImpl;

    @Autowired
    private
    AppUtil appUtil;

    /**
     * This service method is used to get region details using ID.
     *
     * @param id This is the parameter to get details
     * @return region object .
     */

    public BaseDto getRegionById(Long id) {

        final BaseDto baseDto = new BaseDto();

        try {

            RegionMaster regionMaster = cacheRepository.get(SaaSUtil.getSaaSId(), RegionMaster.class.getName(), id,
                    regionMasterRepository);

            ValidateUtil.notNull(regionMaster, ErrorCode.REGION_ID_NOT_FOUND);

            log.info("Getting region successfully");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(regionMaster);

        } catch (final RestException exception) {

            log.error("Exception in getByRegion method : ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (final Exception exception) {

            log.error("Exception in getByRegion method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);
    }

    /**
     * This service method is used to get region details .
     *
     * @param searchDto This is the parameter to get details
     * @return region object .
     */
    public BaseDto search(RegionSearchDto searchDto) {

        log.info("Search method is called...." + searchDto);

        BaseDto baseDto = new BaseDto();

        try {

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(regionSearchImpl.search(searchDto));

            log.info("Successfully Searching Pack...");

        } catch (Exception exception) {

            log.error("Exception in Search method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);

    }

    /**
     * This service method is used to get region details.
     *
     * @param searchRequest This is the parameter to get details
     * @return region object .
     */
    public BaseDto search(SearchRequest searchRequest) {
        log.info("Search method is called...." + searchRequest);

        BaseDto baseDto = new BaseDto();

        try {

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(regionSearchImpl.search(searchRequest));

            log.info("Successfully Searching Region...");

        } catch (Exception exception) {

            log.error("Exception in Search method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);
    }

    /**
     * This service method is used to Create regionMaster .
     *
     * @return region object .
     */
    public BaseDto create(RegionMaster newRegionMaster) {

        log.info("Save method is called.....");

        BaseDto baseDto = new BaseDto();

        try {

            regionMasterValidator.validate(newRegionMaster);

            RegionMaster regionMasterCreated = cacheRepository.save(SaaSUtil.getSaaSId(), RegionMaster.class.getName(),
                    newRegionMaster, regionMasterRepository);

            log.info("Region created successfully...[" + newRegionMaster.getId() + "]");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(regionMasterCreated);

        } catch (RestException exception) {

            log.error("RestException in Create method : ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (Exception e) {

            String exceptionCause = ExceptionUtils.getRootCauseMessage(e);

            log.error("Exception Cause  ::: " + exceptionCause);

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            if (exceptionCause.contains(UniqueKey.UK_REGION_REGIONCODE)) {
                baseDto.setResponseCode(ErrorCode.REGION_CODE_ALREADY_EXIST);
            }
            if (exceptionCause.contains(UniqueKey.UK_REGION_REGIONNAME)) {
                baseDto.setResponseCode(ErrorCode.REGION_NAME_ALREADY_EXIST);
            }
        }
        return appUtil.setDesc(baseDto);

    }

    /**
     * This service method is used to get region details using ID.
     *
     * @return region object .
     */

    public BaseDto update(RegionMaster existingRegionMaster) {

        BaseDto baseDto = new BaseDto();

        try {

            regionMasterValidator.validate(existingRegionMaster);

            regionMasterRepository.save(existingRegionMaster);

            cacheRepository.remove(SaaSUtil.getSaaSId(), RegionMaster.class.getName(), existingRegionMaster.getId());

            log.info("Region updated successfully...[" + existingRegionMaster.getId() + "]");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(existingRegionMaster);

        } catch (RestException exception) {

            log.error("RestException in Create method : ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (ObjectOptimisticLockingFailureException e) {

            log.error("Error in editing region", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_LOCKED_RECORD);

        } catch (JpaObjectRetrievalFailureException e) {

            log.error("Error in editing Region", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_DELETED_RECORD);

        } catch (Exception e) {
            log.error("Exception ::: " + e);

            String exceptionCause = ExceptionUtils.getRootCauseMessage(e);

            log.error("Exception Cause  ::: " + exceptionCause);

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            if (exceptionCause.contains(UniqueKey.UK_REGION_REGIONCODE)) {
                baseDto.setResponseCode(ErrorCode.REGION_CODE_ALREADY_EXIST);
            }
            if (exceptionCause.contains(UniqueKey.UK_REGION_REGIONNAME)) {
                baseDto.setResponseCode(ErrorCode.REGION_NAME_ALREADY_EXIST);
            }
        }
        return appUtil.setDesc(baseDto);

    }

    /**
     * This service method is used to delete region object from db.
     *
     * @return region object .
     */
    public BaseDto delete(Long id) {

        BaseDto baseDto = new BaseDto();

        try {

            log.info("delete  method is called....[ " + id + " ] ");

            cacheRepository.delete(SaaSUtil.getSaaSId(), RegionMaster.class.getName(), id, regionMasterRepository);

            log.info("Successfully deleted region by id...[ " + id + " ] ");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

        } catch (DataIntegrityViolationException exception) {

            log.error("Exception occured : ", exception);

            ExceptionHelper helper = new ExceptionHelper(exception);

            log.error("Foreign key reference : " + helper.getFkReference());

            baseDto.setResponseCode(ErrorCode.ERROR_FK_CONSTRAINT);

        } catch (Exception exception) {

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            log.error("Exception in Delete method : ", exception);

            String rootCause = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Root Cause of Exception : " + rootCause);

            if (rootCause.contains("FK_")) {
                baseDto.setResponseCode(ErrorCode.CANNOT_DELETE_FK_RECORD);
            }
        }
        return appUtil.setDesc(baseDto);
    }

}
