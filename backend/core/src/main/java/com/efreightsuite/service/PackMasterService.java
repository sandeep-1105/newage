package com.efreightsuite.service;

import com.efreightsuite.common.component.CacheRepository;
import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.PackSearchDto;
import com.efreightsuite.dto.SearchRequest;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.ExceptionHelper;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.exception.UniqueKey;
import com.efreightsuite.model.PackMaster;
import com.efreightsuite.repository.PackMasterRepository;
import com.efreightsuite.search.PackSearchImpl;
import com.efreightsuite.util.AppUtil;
import com.efreightsuite.util.SaaSUtil;
import com.efreightsuite.util.ValidateUtil;
import com.efreightsuite.validation.PackMasterValidator;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.exception.ExceptionUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.orm.ObjectOptimisticLockingFailureException;
import org.springframework.orm.jpa.JpaObjectRetrievalFailureException;
import org.springframework.stereotype.Service;

/**
 * @author tanveer Date - 07.03.2016 Updated - 11.03.2016
 */

@Service
@Log4j2
public class PackMasterService {

    @Autowired
    private
    AppUtil appUtil;

    /**
     * PackMasterRepository Object
     */

    @Autowired
    private
    PackMasterRepository packMasterRepository;

    @Autowired
    private
    PackMasterValidator packMasterValidator;

    @Autowired
    private
    CacheRepository cacheRepository;

    @Autowired
    private
    PackSearchImpl packSearchImpl;

    /**
     * Method - getByPackId Returns PackMaster based on given pack id
     *
     * @return BaseDto
     */

    public BaseDto getByPackId(Long id) {

        log.info("getByPackId method is called...[" + id + "]");

        final BaseDto baseDto = new BaseDto();

        try {

            PackMaster packMaster = cacheRepository.get(SaaSUtil.getSaaSId(), PackMaster.class.getName(), id,
                    packMasterRepository);

            ValidateUtil.notNull(packMaster, ErrorCode.PACK_ID_NOT_FOUND);

            log.info("getByPackId is successfully executed....[" + packMaster.getId() + "]");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(packMaster);

        } catch (final RestException exception) {

            log.error("Exception in getByPackId method : ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (final Exception exception) {

            log.error("Exception in getByPackId method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);
    }

    /**
     * Method - getByPackCode Returns PackMaster based on given pack code
     *
     * @return BaseDto
     */

    public BaseDto getByPackCode(String packCode) {

        log.info("getByPackCode method is called..." + packCode);

        final BaseDto baseDto = new BaseDto();

        try {

            PackMaster packMaster = packMasterRepository.findByPackCode(packCode);

            baseDto.setResponseCode(ErrorCode.SUCCESS);


            baseDto.setResponseObject(packMaster);

        } catch (Exception exception) {

            log.error("Exception in getByPackCode method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);
    }

    /**
     * Method - search Returns list of PackMaster based on the search keyword
     *
     * @return BaseDto
     */

    public BaseDto search(PackSearchDto searchDto) {

        log.info("Search method is called...." + searchDto);

        BaseDto baseDto = new BaseDto();

        try {

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(packSearchImpl.search(searchDto));

            log.info("Successfully Searching Pack...");

        } catch (Exception exception) {

            log.error("Exception in Search method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);

    }

    public BaseDto search(SearchRequest searchRequest) {

        log.info("Search method is called...." + searchRequest);

        BaseDto baseDto = new BaseDto();

        try {

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(packSearchImpl.search(searchRequest));

            log.info("Successfully Searching Pack...");

        } catch (Exception exception) {

            log.error("Exception in Search method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);

    }

    /**
     * Method - create Creates and returns a new pack master
     *
     * @returns PackMaster
     */

    public BaseDto create(PackMaster packMaster) {

        log.info("Create method is called - packMasterId ..");

        final BaseDto baseDto = new BaseDto();

        try {

            packMasterValidator.validate(packMaster);


            cacheRepository.save(SaaSUtil.getSaaSId(), PackMaster.class.getName(), packMaster, packMasterRepository);

            log.info("Pack Saved successfully.....");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(packMaster);

        } catch (RestException exception) {

            log.error("Exception Cause 1 ::: ", exception);
            baseDto.setResponseCode(exception.getMessage());


        } catch (Exception exception) {

            log.error("Exception occured : ", exception);


            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Exception Cause 1 : " + exceptionCause1);

            // General error condition
            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            // Duplicate pack Code

            if (exceptionCause1.contains(UniqueKey.UK_PACK_PACKCODE)) {
                baseDto.setResponseCode(ErrorCode.PACK_CODE_ALREADY_EXIST);

            }

            if (exceptionCause1.contains(UniqueKey.UK_PACK_PACKNAME)) {
                baseDto.setResponseCode(ErrorCode.PACK_NAME_ALREADY_EXIST);
            }


        }

        return appUtil.setDesc(baseDto);
    }

    /**
     * Method - update Updates and returns a updated pack master
     *
     * @param existingPackMaster
     * @returns PackMaster
     */

    public BaseDto update(PackMaster existingPackMaster) {

        log.info("Update method is called - existingPackMaster : [" + existingPackMaster.getId() + "]");

        BaseDto baseDto = new BaseDto();

        try {

            ValidateUtil.notNull(existingPackMaster.getId(), ErrorCode.PACK_ID_NOT_NULL);

            packMasterValidator.validate(existingPackMaster);


            packMasterRepository.getOne(existingPackMaster.getId());

            existingPackMaster = cacheRepository.save(SaaSUtil.getSaaSId(), PackMaster.class.getName(), existingPackMaster,
                    packMasterRepository);

            log.info("PackMaster updated successfully....[" + existingPackMaster.getId() + "]");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(existingPackMaster);

        } catch (RestException exception) {

            log.error("Exception occured with error code ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (ObjectOptimisticLockingFailureException e) {

            log.error("Error in editing", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_LOCKED_RECORD);

        } catch (JpaObjectRetrievalFailureException e) {

            log.error("Error in editing", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_DELETED_RECORD);

        } catch (Exception exception) {

            log.error("Exception occured : ", exception);

            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Exception Cause 1 ::: " + exceptionCause1);


            // General error condition
            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            // Duplicate pack Code

            if (exceptionCause1.contains(UniqueKey.UK_PACK_PACKCODE)) {
                baseDto.setResponseCode(ErrorCode.PACK_CODE_ALREADY_EXIST);

            }

            if (exceptionCause1.contains(UniqueKey.UK_PACK_PACKNAME)) {
                baseDto.setResponseCode(ErrorCode.PACK_NAME_ALREADY_EXIST);
            }


        }

        return appUtil.setDesc(baseDto);
    }

    /**
     * Deletes the Existing PackMaster Based on the Given Id
     *
     * @Returns BaseDto
     */
    public BaseDto delete(Long id) {

        log.info("Delete method is Invoked...[" + id + "]");

        final BaseDto baseDto = new BaseDto();

        try {

            cacheRepository.delete(SaaSUtil.getSaaSId(), PackMaster.class.getName(), id, packMasterRepository);

            log.info("PackMaster Deleted Successfully....[" + id + "]");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

        } catch (RestException exception) {

            log.error("Exception in Delete method : ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (DataIntegrityViolationException exception) {

            log.error("Exception occured : ", exception);

            ExceptionHelper helper = new ExceptionHelper(exception);

            log.error("Foreign key reference : " + helper.getFkReference());

            baseDto.setResponseCode(ErrorCode.ERROR_FK_CONSTRAINT);

        } catch (Exception exception) {

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            log.error("Exception in Delete method : ", exception);

            String rootCause = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Root Cause of Exception : " + rootCause);

            if (rootCause.contains("FK_")) {
                baseDto.setResponseCode(ErrorCode.CANNOT_DELETE_FK_RECORD);
            }
        }

        return appUtil.setDesc(baseDto);
    }

}
