package com.efreightsuite.service;

import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.SearchRequest;
import com.efreightsuite.enumeration.YesNo;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.search.GeneralLedgerImpl;
import com.efreightsuite.util.AppUtil;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class GeneralLedgerService {

    @Autowired
    private
    AppUtil appUtil;

    @Autowired
    private
    GeneralLedgerImpl generalLedgerImpl;

    public BaseDto search(SearchRequest searchRequest, YesNo isSubLedger, YesNo isBank) {

        BaseDto baseDto = new BaseDto();

        try {

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(generalLedgerImpl.getGLAccounts(searchRequest, isSubLedger, isBank));

        } catch (Exception exception) {

            log.error("Exception in Search method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);
    }

}
