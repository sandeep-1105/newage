package com.efreightsuite.service;

import com.efreightsuite.common.component.CacheRepository;
import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.SearchRequest;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.repository.GeneralLedgerRepository;
import com.efreightsuite.search.GeneralLedgerSearchImpl;
import com.efreightsuite.util.AppUtil;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Log4j2
@Service
public class GeneralLedgerMasterService {


    @Autowired
    private
    AppUtil appUtil;

    @Autowired
    GeneralLedgerRepository generalLedgerMasterRepository;

    @Autowired
    CacheRepository cacheRepository;

    @Autowired
    private
    GeneralLedgerSearchImpl generalLedgerSearchImpl;


    /*
     * Returns all the Ledger object as a list
     *
     * @Returns BaseDto
     */
    public BaseDto search(SearchRequest searchRequest) {

        log.info("getAll method is called....");

        BaseDto baseDto = new BaseDto();

        try {

            //SearchRespDto resp = new SearchRespDto(0l, generalLedgerMasterRepository.findAll());

            baseDto.setResponseObject(generalLedgerSearchImpl.search(searchRequest));

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            //baseDto.setResponseObject(resp);

        } catch (RestException re) {

            log.error("Exception in getAll method ", re);

            baseDto.setResponseCode(re.getMessage());

        } catch (Exception e) {

            log.error("Exception in getAll method ", e);

            baseDto.setResponseCode(ErrorCode.FAILED);

        }
        return appUtil.setDesc(baseDto);
    }


}
