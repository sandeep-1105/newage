package com.efreightsuite.service;

import lombok.extern.log4j.Log4j2;

import org.springframework.stereotype.Service;

@Service
@Log4j2
public class GoogleRecaptchaService {

    private final static String httpUrl = "https://www.google.com/recaptcha/api/siteverify";
    private final static String secret = "6LfPuCcUAAAAAJUIfW5VPhrzrpkW_JQzpwDjL_N6";

}
