package com.efreightsuite.service;

import java.util.List;

import com.efreightsuite.common.component.CacheRepository;
import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.exception.UniqueKey;
import com.efreightsuite.model.PeriodMaster;
import com.efreightsuite.repository.PeriodMasterRepository;
import com.efreightsuite.util.AppUtil;
import com.efreightsuite.util.SaaSUtil;
import com.efreightsuite.validation.PeriodMasterValidator;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.exception.ExceptionUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.ObjectOptimisticLockingFailureException;
import org.springframework.orm.jpa.JpaObjectRetrievalFailureException;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class PeriodMasterService {

    @Autowired
    private
    AppUtil appUtil;

    @Autowired
    private
    PeriodMasterRepository periodMasterRepository;

    @Autowired
    private
    PeriodMasterValidator periodMasterValidator;

    @Autowired
    private
    CacheRepository cacheRepository;

    public BaseDto create(PeriodMaster periodMaster) {

        log.info("Create method is invoked.");

        BaseDto baseDto = new BaseDto();

        try {

            periodMasterValidator.validate(periodMaster);


            periodMaster = cacheRepository.save(SaaSUtil.getSaaSId(), PeriodMaster.class.getName(), periodMaster, periodMasterRepository);

            log.info("Period Master Saved Successfully.....[" + periodMaster.getId() + "]");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(periodMaster);

        } catch (RestException exception) {

            log.error("Exception occured ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (Exception exception) {

            log.error("Exception occured : ", exception);

            String exceptionCause = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Exception Cause  : " + exceptionCause);

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            if (exceptionCause.contains(UniqueKey.UK_PERIOD_FREQUENCY_CODE)) {
                baseDto.setResponseCode(ErrorCode.PERIOD_FREQUENCY_CODE_ALREADY_EXIST);
            }

            if (exceptionCause.contains(UniqueKey.UK_PERIOD_FREQUENCY_NAME)) {
                baseDto.setResponseCode(ErrorCode.PERIOD_FREQUENCY_NAME_ALREADY_EXIST);
            }

        }

        return appUtil.setDesc(baseDto);
    }


    public BaseDto update(PeriodMaster existingPeriodMaster) {

        log.info("Update method is invoked... [" + existingPeriodMaster.getId() + "]");

        BaseDto baseDto = new BaseDto();

        try {

            periodMasterValidator.validate(existingPeriodMaster);

            periodMasterRepository.getOne(existingPeriodMaster.getId());

            existingPeriodMaster = cacheRepository.save(SaaSUtil.getSaaSId(), PeriodMaster.class.getName(), existingPeriodMaster, periodMasterRepository);

            log.info("PeriodMaster updated successfully....[" + existingPeriodMaster.getId() + "]");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(existingPeriodMaster);

        } catch (RestException exception) {

            log.error("Exception occured with error code ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (ObjectOptimisticLockingFailureException e) {

            log.error("Error in editing", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_LOCKED_RECORD);

        } catch (JpaObjectRetrievalFailureException e) {

            log.error("Error in editing", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_DELETED_RECORD);

        } catch (Exception exception) {

            log.error("Exception occured : ", exception);

            String exceptionCause = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Exception Cause : " + exceptionCause);

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            if (exceptionCause.contains(UniqueKey.UK_PERIOD_FREQUENCY_CODE)) {
                baseDto.setResponseCode(ErrorCode.PERIOD_FREQUENCY_CODE_ALREADY_EXIST);
            }

            if (exceptionCause.contains(UniqueKey.UK_PERIOD_FREQUENCY_NAME)) {
                baseDto.setResponseCode(ErrorCode.PERIOD_FREQUENCY_NAME_ALREADY_EXIST);
            }
        }

        return appUtil.setDesc(baseDto);
    }


    public BaseDto delete(Long id) {

        log.info("Delete method is Invoked...[" + id + "]");

        BaseDto baseDto = new BaseDto();

        try {

            cacheRepository.delete(SaaSUtil.getSaaSId(), PeriodMaster.class.getName(), id, periodMasterRepository);

            log.info("PeriodMaster Deleted Successfully....[" + id + "]");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

        } catch (Exception exception) {

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            log.error("Exception in Delete method : ", exception);

            String rootCause = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Root Cause of Exception : " + rootCause);

            if (rootCause.contains("FK_")) {
                baseDto.setResponseCode(ErrorCode.CANNOT_DELETE_FK_RECORD);
            }
        }

        return appUtil.setDesc(baseDto);
    }


    public BaseDto getAll() {

        log.info("getAll method is Invoked....");

        BaseDto baseDto = new BaseDto();

        try {

            List<PeriodMaster> periodMasterList = periodMasterRepository.findAll();

            //ValidateUtil.notEmpty(periodMasterList, ErrorCode.PERIOD_LIST_EMPTY);

            log.info("Successfully getting list of period...");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(periodMasterList);

        } catch (RestException re) {

            log.error("Exception in getAll method ", re);

            baseDto.setResponseCode(re.getMessage());

        } catch (Exception e) {

            log.error("Exception in getAll method ", e);

            baseDto.setResponseCode(ErrorCode.FAILED);

        }
        return appUtil.setDesc(baseDto);
    }
}
