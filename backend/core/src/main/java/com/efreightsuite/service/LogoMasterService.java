package com.efreightsuite.service;

import java.util.List;

import javax.xml.bind.DatatypeConverter;

import com.efreightsuite.common.component.CacheRepository;
import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.LogoMasterSearchDto;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.ExceptionHelper;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.exception.UniqueKey;
import com.efreightsuite.model.LogoMaster;
import com.efreightsuite.repository.LocationMasterRepository;
import com.efreightsuite.repository.LogoMasterRepository;
import com.efreightsuite.search.LogoMasterSearchImpl;
import com.efreightsuite.util.AppUtil;
import com.efreightsuite.util.SaaSUtil;
import com.efreightsuite.util.ValidateUtil;
import com.efreightsuite.validation.LogoMasterValidator;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.exception.ExceptionUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.orm.ObjectOptimisticLockingFailureException;
import org.springframework.stereotype.Service;

/**
 * @author Devendrachary M
 */
@Service
@Log4j2
public class LogoMasterService {

    @Autowired
    private
    AppUtil appUtil;

    @Autowired
    private
    CacheRepository cacheRepository;

    @Autowired
    private
    LogoMasterRepository logoMasterRepository;

    @Autowired
    LocationMasterRepository locationMasterRepository;

    @Autowired
    private
    LogoMasterValidator logoMasterValidator;

    @Autowired
    private
    LogoMasterSearchImpl logoMasterSearchImpl;

    /**
     * Method - get
     *
     * @return baseDto
     */

    public BaseDto get(Long id) {
        log.info("LogoMasterService.get method started.");

        BaseDto baseDto = new BaseDto();
        try {

            ValidateUtil.notNull(id, ErrorCode.LOGOMASTER_NOT_NULL);

            LogoMaster logoMaster = logoMasterRepository.findById(id);


            //logoMaster.getLocationMaster().getPartyMaster().touch();

            if (logoMaster.getLogo() != null) {
                logoMaster.setEncodedLogo(DatatypeConverter.printBase64Binary(logoMaster.getLogo()));
            }
            logoMaster.setLogo(null);

            log.info("Successfully getting LogoMaster by id...");

            baseDto.setResponseCode(ErrorCode.SUCCESS);
            baseDto.setResponseObject(logoMaster);

        } catch (RestException re) {
            log.error("RestException in LogoMasterService.get method while getting the LogoMaster : " + re);
            baseDto.setResponseCode(ErrorCode.LOGOMASTER_NOT_NULL);
        } catch (Exception e) {
            log.error("Exception in LogoMasterService.get method while getting the LogoMaster : ", e);
            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        log.info("LogoMasterService.get method end.");
        return appUtil.setDesc(baseDto);
    }

    /*
     * create - This method creates a new LogoMaster
     *
     * @Param LogoMaster master entity
     *
     * @Return BaseDto
     */
    public BaseDto create(LogoMaster logoMaster) {

        log.info("LogoMasterService.create method is started.");
        BaseDto baseDto = new BaseDto();
        try {

            ValidateUtil.notNull(logoMaster, ErrorCode.LOGOMASTER_NOT_NULL);

            logoMasterValidator.validateToUpdate(logoMaster);

            LogoMaster logoMasterRes = logoMaster = logoMasterRepository.save(logoMaster);


            log.info("LogoMaster Saved successfully.....");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(logoMasterRes);

        } catch (RestException re) {

            log.error("Exception in LogoMasterService.create ", re);

            baseDto.setResponseCode(re.getMessage());
        } catch (Exception exception) {

            String exceptionCause = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Exception Cause  ::: " + exceptionCause);

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            if (exceptionCause.contains(UniqueKey.UK_LOGO_LOCATION)) {
                baseDto.setResponseCode(ErrorCode.UK_LOGO_LOCATION);
            }

        }

        log.info("LogoMasterService.create method is ended.");
        return appUtil.setDesc(baseDto);
    }

    /*
     * search - This method Retrieves the Logo Master
     *
     * @Param LogoMasterSearchDto
     *
     * @Return BaseDto
     */
    public BaseDto search(LogoMasterSearchDto searchDto) {
        log.info("LogoMasterService.search method is started.[" + searchDto + "]");

        BaseDto baseDto = new BaseDto();

        try {

            baseDto.setResponseObject(logoMasterSearchImpl.search(searchDto));

            log.info("Successfully Searching LogoMaster...");

        } catch (Exception exception) {

            log.info("Exception in Search method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }
        log.info("LogoMasterService.delete method is ended.");
        return appUtil.setDesc(baseDto);
    }

    public BaseDto update(LogoMaster existingLogoMaster) {
        log.info("LogoMasterService.update method is started.");
        BaseDto baseDto = new BaseDto();

        try {

            ValidateUtil.notNull(existingLogoMaster, ErrorCode.LOGOMASTER_NOT_NULL);

            logoMasterValidator.validateToUpdate(existingLogoMaster);

            logoMasterRepository.getOne(existingLogoMaster.getId());

            LogoMaster logoMasterUpdateResponse = logoMasterRepository.save(existingLogoMaster);

            cacheRepository.remove(SaaSUtil.getSaaSId(), LogoMaster.class.getName(), existingLogoMaster.getId());

            log.info("Service  updated successfully...[" + logoMasterUpdateResponse.getId() + "]");

            log.info("LogoMasterService updated successfully...");

            baseDto.setResponseCode(ErrorCode.SUCCESS);
            baseDto.setResponseObject(logoMasterUpdateResponse);

        } catch (RestException exception) {

            log.error("Exception in update method : ", exception);

            baseDto.setResponseCode(exception.getMessage());
        } catch (ObjectOptimisticLockingFailureException e) {
            log.error("Error in editing Service", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_LOCKED_RECORD);
        } catch (Exception ex) {

            log.error("Exception --- ", ex);

            String exceptionCause = ExceptionUtils.getRootCauseMessage(ex);

            log.error("Exception Cause  ::: " + exceptionCause);

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            if (exceptionCause.contains(UniqueKey.UK_LOGO_LOCATION)) {
                baseDto.setResponseCode(ErrorCode.UK_LOGO_LOCATION);
            }
        }

        log.info("LogoMasterService.update method is ended.");
        return appUtil.setDesc(baseDto);

    }

    /*
     * Deletes the Existing LogoMaster Based on the Given Id
     *
     * @param Id
     *
     * @Returns BaseDto
     */
    public BaseDto delete(Long id) {
        log.info("LogoMasterService.delete method is started.[" + id + "]");

        BaseDto baseDto = new BaseDto();

        try {

            ValidateUtil.notNull(id, ErrorCode.LOGOMASTER_NOT_NULL);

            cacheRepository.delete(SaaSUtil.getSaaSId(), LogoMaster.class.getName(), id, logoMasterRepository);

            log.info("LogoMaster Deleted Successfully....[" + id + "]");

            baseDto.setResponseCode(ErrorCode.SUCCESS);
            return appUtil.setDesc(baseDto);

        } catch (RestException exception) {

            log.error("Exception in Delete method : ", exception);

            baseDto.setResponseCode(exception.getMessage());
        } catch (DataIntegrityViolationException exception) {

            log.error("Exception occured : ", exception);

            ExceptionHelper helper = new ExceptionHelper(exception);

            log.error("Foreign key reference : " + helper.getFkReference());

            baseDto.setResponseCode(ErrorCode.ERROR_FK_CONSTRAINT);
        } catch (Exception exception) {

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            log.error("Exception in Delete method : ", exception);

            String rootCause = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Root Cause of Exception : " + rootCause);

            if (rootCause.contains("FK_")) {
                baseDto.setResponseCode(ErrorCode.CANNOT_DELETE_FK_RECORD);
            }
        }
        log.info("LogoMasterService.delete method is ended");
        return appUtil.setDesc(baseDto);

    }

    /*
     * Gives the all LogoMaster
     *
     * @Returns List<LogoMaster>
     */
    public BaseDto getAll() {

        log.info("LogoMasterService.getAll method is started");

        BaseDto baseDto = new BaseDto();

        try {

            List<LogoMaster> logoMasterList = logoMasterRepository.findAll();

            if (logoMasterList != null) {
                for (LogoMaster logoMaster : logoMasterList) {

                    logoMaster.getLocationMaster().setCompanyMaster(null);
                    logoMaster.getLocationMaster().setCountryMaster(null);
                    logoMaster.getLocationMaster().setZoneMaster(null);
                    logoMaster.getLocationMaster().setCityMaster(null);
                    logoMaster.getLocationMaster().setCostCenterMaster(null);
                    logoMaster.getLocationMaster().setPartyMaster(null);

                }

            }

            log.info("Successfully getting list of logoMaster...");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(logoMasterList);

        } catch (RestException exception) {

            log.error("Exception in getAll method ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (Exception exception) {

            log.error("Exception in getAll method ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }
        log.info("LogoMasterService.getAll method is ended");
        return appUtil.setDesc(baseDto);
    }


    //get by location


    public BaseDto getlogoBylocation(Long locationid) {
        log.info("LogoMasterService.get method started.");

        BaseDto baseDto = new BaseDto();
        try {

            ValidateUtil.notNull(locationid, ErrorCode.LOGOMASTER_LOCATION);

            LogoMaster logoMaster = logoMasterSearchImpl.getLogo(locationid);

            if (logoMaster != null) {
                logoMaster.getLocationMaster().setCompanyMaster(null);
                logoMaster.getLocationMaster().setCountryMaster(null);
                logoMaster.getLocationMaster().setZoneMaster(null);
                logoMaster.getLocationMaster().setCityMaster(null);
                logoMaster.getLocationMaster().setCostCenterMaster(null);
                logoMaster.getLocationMaster().setPartyMaster(null);
                if (logoMaster.getLogo() != null) {
                    logoMaster.setEncodedLogo(DatatypeConverter.printBase64Binary(logoMaster.getLogo()));
                }
                logoMaster.setLogo(null);
            }
            log.info("Successfully getting LogoMaster by id...");
            baseDto.setResponseCode(ErrorCode.SUCCESS);
            baseDto.setResponseObject(logoMaster);

        } catch (RestException re) {
            log.error("RestException in LogoMasterService.get method while getting the LogoMaster : " + re);
            baseDto.setResponseCode(ErrorCode.LOGOMASTER_NOT_NULL);
        } catch (Exception e) {
            log.error("Exception in LogoMasterService.get method while getting the LogoMaster : ", e);
            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        log.info("LogoMasterService.get method end.");
        return appUtil.setDesc(baseDto);
    }

}
