package com.efreightsuite.service;

import java.io.ByteArrayInputStream;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import com.efreightsuite.common.component.CacheRepository;
import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.CarrierRateSearchDto;
import com.efreightsuite.dto.FileUploadDto;
import com.efreightsuite.enumeration.ActualChargeable;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.ExceptionHelper;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.exception.UniqueKey;
import com.efreightsuite.model.CarrierMaster;
import com.efreightsuite.model.CarrierRateMaster;
import com.efreightsuite.model.ChargeMaster;
import com.efreightsuite.model.PortMaster;
import com.efreightsuite.model.UnitMaster;
import com.efreightsuite.repository.CarrierMasterRepository;
import com.efreightsuite.repository.CarrierRateMasterRepository;
import com.efreightsuite.repository.ChargeMasterRepository;
import com.efreightsuite.repository.PortMasterRepository;
import com.efreightsuite.repository.UnitMasterRepository;
import com.efreightsuite.search.CarrierRateSearchImpl;
import com.efreightsuite.util.AppUtil;
import com.efreightsuite.util.CSVFileConverter;
import com.efreightsuite.util.SaaSUtil;
import com.efreightsuite.util.ValidateUtil;
import com.efreightsuite.validation.CarrierRateMasterValidator;
import com.ibm.icu.text.SimpleDateFormat;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.exception.ExceptionUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.orm.ObjectOptimisticLockingFailureException;
import org.springframework.orm.jpa.JpaObjectRetrievalFailureException;
import org.springframework.stereotype.Service;

/**
 * @author user
 */
@Service
@Log4j2
public class CarrierRateMasterService {

    @Autowired
    private
    AppUtil appUtil;

    @Autowired
    private
    CacheRepository cacheRepository;

    @Autowired
    private
    CarrierRateMasterValidator carrierRateMasterValidator;

    @Autowired
    private
    CarrierRateSearchImpl carrierRateSearchImpl;

    @Autowired
    private
    CarrierRateMasterRepository carrierRateMasterRepository;

    @Autowired
    private
    CarrierMasterRepository carrierMasterRepository;

    @Autowired
    private
    ChargeMasterRepository chargeMasterRepository;

    @Autowired
    private
    UnitMasterRepository unitMasterRepository;

    @Autowired
    private
    PortMasterRepository portMasterRepository;

    @Value("${efs.resource.path}")
    String appResourcePath;


    public BaseDto search(CarrierRateSearchDto searchDto) {
        log.info("Carrier rate Search method is called");
        BaseDto baseDto = new BaseDto();
        try {
            baseDto.setResponseObject(carrierRateSearchImpl.search(searchDto));
            baseDto.setResponseCode(ErrorCode.SUCCESS);
            log.info("Successfully Searching Carrier...");

        } catch (Exception exception) {

            log.info("Exception in Search method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);
    }

    public BaseDto get(Long id) {
        log.info("get method is called....[" + id + "]");

        BaseDto baseDto = new BaseDto();

        try {

            CarrierRateMaster carrierRateMaster = carrierRateMasterRepository.getOne(id);
            /*
			 * if(carrierRateMaster!=null &&
			 * carrierRateMaster.getCarrierMaster()!=null &&
			 * carrierRateMaster.getCarrierMaster().getEdiList().size()>0){
			 * carrierRateMaster.getCarrierMaster().setEdiList(null);
			 */

            log.info("Successfully getting Carrier by id...[" + carrierRateMaster.getId() + "]");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(carrierRateMaster);

        } catch (Exception exception) {

            log.error("Exception in get method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);
    }

    public BaseDto create(List<CarrierRateMaster> carrierRateMasterList) {
        log.info("Create method is called.....");

        BaseDto baseDto = new BaseDto();

        try {

            for (CarrierRateMaster carrierRateMaster : carrierRateMasterList) {
                carrierRateMasterValidator.checkValidation(carrierRateMaster);
            }
            List<CarrierRateMaster> carrierRateMasterCreated = carrierRateMasterRepository.save(carrierRateMasterList);

			/*
			 * cacheRepository.save(SaaSUtil.getSaaSId(),
			 * CarrierRateMaster.class.getName(), carrierRateMasterList,
			 * carrierRateMasterRepository);
			 */
            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(carrierRateMasterCreated);

        } catch (RestException exception) {

            log.error("RestException in Create method : ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (Exception exception) {

            log.error("Exception in Create method : ", exception);

            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Exception Cause 1 ::: " + exceptionCause1);

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            if (exceptionCause1.contains(UniqueKey.UK_CARRIER_RATE)) {
                baseDto.setResponseCode(ErrorCode.CARRIER_RATE_ALREADY_EXIST);
            }

        }

        return appUtil.setDesc(baseDto);

    }

    /**
     * Update Carrier Rate Master
     *
     * @return
     */
    public BaseDto update(CarrierRateMaster existingcarrierRateMaster) {

        BaseDto baseDto = new BaseDto();

        try {

            ValidateUtil.notNull(existingcarrierRateMaster.getId(), ErrorCode.CARRIER_ID_NOT_NULL);

            if (existingcarrierRateMaster.getId() != null) {
                carrierRateMasterRepository.getOne(existingcarrierRateMaster.getId());
            }
            carrierRateMasterValidator.checkValidation(existingcarrierRateMaster);

            carrierRateMasterRepository.save(existingcarrierRateMaster);
            cacheRepository.remove(SaaSUtil.getSaaSId(), CarrierRateMaster.class.getName(), existingcarrierRateMaster.getId());

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(existingcarrierRateMaster);

            log.info("CarrierRateMaster Successfully Updated...[" + existingcarrierRateMaster.getId() + "]");

        } catch (RestException exception) {

            log.error("Exception occured in update method ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (ObjectOptimisticLockingFailureException e) {

            log.error("Optimistic Lock Exception occured ", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_LOCKED_RECORD);

        } catch (JpaObjectRetrievalFailureException e) {

            log.error("Object Already Deleted................", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_DELETED_RECORD);

        } catch (Exception exception) {

            log.error("Exception occurred ", exception);

            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Exception Cause 1 ::: ", exceptionCause1);

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            if (exceptionCause1.contains(UniqueKey.UK_CARRIER_RATE)) {
                baseDto.setResponseCode(ErrorCode.CARRIER_RATE_ALREADY_EXIST);
            }
        }

        return appUtil.setDesc(baseDto);
    }

    /**
     * Delete Carrier Rate Master
     *
     * @param id
     * @return
     */
    public BaseDto delete(Long id) {
        BaseDto baseDto = new BaseDto();
        try {
            cacheRepository.delete(SaaSUtil.getSaaSId(), CarrierRateMaster.class.getName(), id,
                    carrierRateMasterRepository);

            log.info("Successfully deleted Carrier by id...");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

        } catch (DataIntegrityViolationException exception) {

            log.error("Exception occured : ", exception);

            ExceptionHelper helper = new ExceptionHelper(exception);

            log.error("Foreign key reference : " + helper.getFkReference());

            baseDto.setResponseCode(ErrorCode.ERROR_FK_CONSTRAINT);

        } catch (Exception exception) {

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            log.error("Exception in Delete method : ", exception);

            String rootCause = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Root Cause of Exception : " + rootCause);

            if (rootCause.contains("FK_")) {
                baseDto.setResponseCode(ErrorCode.CANNOT_DELETE_FK_RECORD);
            }
        }

        return appUtil.setDesc(baseDto);
    }


    public BaseDto upload(FileUploadDto fileUploadDto) {

        BaseDto baseDto = new BaseDto();

        try {

            // Validation

            carrierRateMasterValidator.validateBulkUpload(fileUploadDto);

            // Reading the File

            SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

            List<CarrierRateMaster> list = new ArrayList<>();

            ByteArrayInputStream bis = new ByteArrayInputStream(fileUploadDto.getFile());

            Scanner scanner = new Scanner(bis);

            Integer lineNumber = 0;


            while (scanner.hasNext()) {

                List<String> line = CSVFileConverter.parseLine(scanner.nextLine());
                lineNumber = lineNumber + 1;

                if (lineNumber != 1) {

                    CarrierRateMaster carrierRateMaster = new CarrierRateMaster();

                    String carrierName = line.get(0);
                    CarrierMaster carrierMaster = carrierMasterRepository.findByCarrierName(carrierName);
                    if (carrierMaster == null) {
                        baseDto.getParams().add(carrierName);
                        baseDto.getParams().add(lineNumber.toString());
                        baseDto.getParams().add("1");
                        throw new RestException(ErrorCode.CSV_FILE_INVALID);
                    }
                    carrierRateMaster.setCarrierMaster(carrierMaster);


                    String chargeName = line.get(1);
                    ChargeMaster chargeMaster = chargeMasterRepository.findByChargeName(chargeName);
                    if (chargeMaster == null) {
                        baseDto.getParams().add(chargeName);
                        baseDto.getParams().add(lineNumber.toString());
                        baseDto.getParams().add("2");
                        throw new RestException(ErrorCode.CSV_FILE_INVALID);
                    }
                    carrierRateMaster.setChargeMaster(chargeMaster);

                    String unitName = line.get(2);
                    UnitMaster unitMaster = unitMasterRepository.findByUnitName(unitName);
                    if (unitMaster == null) {
                        baseDto.getParams().add(unitName);
                        baseDto.getParams().add(lineNumber.toString());
                        baseDto.getParams().add("3");
                        throw new RestException(ErrorCode.CSV_FILE_INVALID);
                    }
                    carrierRateMaster.setUnitMaster(unitMaster);


                    if (line.get(3).toUpperCase().equals("ACTUAL")) {
                        carrierRateMaster.setActualChargeable(ActualChargeable.ACTUAL);
                    } else if (line.get(3).toUpperCase().equals("CHARGEABLE") || line.get(3).toUpperCase().equals("CHARGEABLE WEIGHT")
                            || line.get(3).equals("Chargeable Weight")) {
                        carrierRateMaster.setActualChargeable(ActualChargeable.CHARGEABLE);
                    } else {
                        baseDto.getParams().add(line.get(3));
                        baseDto.getParams().add(lineNumber.toString());
                        baseDto.getParams().add("4");
                        throw new RestException(ErrorCode.CSV_FILE_INVALID);
                    }

                    String originCode = line.get(4);
                    PortMaster origin = portMasterRepository.findByPortCode(originCode);
                    if (origin == null) {
                        baseDto.getParams().add(originCode);
                        baseDto.getParams().add(lineNumber.toString());
                        baseDto.getParams().add("5");
                        throw new RestException(ErrorCode.CSV_FILE_INVALID);
                    }
                    carrierRateMaster.setOrigin(origin);

                    String destinationCode = line.get(5);
                    PortMaster destination = portMasterRepository.findByPortCode(destinationCode);
                    if (destination == null) {
                        baseDto.getParams().add(destinationCode);
                        baseDto.getParams().add(lineNumber.toString());
                        baseDto.getParams().add("6");
                        throw new RestException(ErrorCode.CSV_FILE_INVALID);
                    }
                    carrierRateMaster.setDestination(destination);

                    String firstFromDate = line.get(6);
                    try {
                        carrierRateMaster.setFirstFromDate(dateFormat.parse(firstFromDate));
                    } catch (ParseException exception) {
                        log.error("Exception while converting String to date ", exception);
                        baseDto.getParams().add(firstFromDate);
                        baseDto.getParams().add(lineNumber.toString());
                        baseDto.getParams().add("7");
                        throw new RestException(ErrorCode.CSV_FILE_INVALID);
                    }


                    String firstAmount = line.get(7);
                    try {
                        carrierRateMaster.setFirstAmount(Double.parseDouble(firstAmount));
                    } catch (NumberFormatException exception) {
                        log.error("Exception while converting Double value ", exception);
                        baseDto.getParams().add(firstAmount);
                        baseDto.getParams().add(lineNumber.toString());
                        baseDto.getParams().add("8");
                        throw new RestException(ErrorCode.CSV_FILE_INVALID);
                    }


                    String firstMinAmount = line.get(8);
                    try {
                        carrierRateMaster.setFirstMinAmount(Double.parseDouble(firstMinAmount));
                    } catch (NumberFormatException exception) {
                        log.error("Exception while converting Double value ", exception);
                        baseDto.getParams().add(firstMinAmount);
                        baseDto.getParams().add(lineNumber.toString());
                        baseDto.getParams().add("9");
                        throw new RestException(ErrorCode.CSV_FILE_INVALID);
                    }


                    String secondFromDate = line.get(9);
                    try {
                        carrierRateMaster.setSecondFromDate(dateFormat.parse(secondFromDate));
                    } catch (ParseException exception) {
                        baseDto.getParams().add(secondFromDate);
                        baseDto.getParams().add(lineNumber.toString());
                        baseDto.getParams().add("10");
                        throw new RestException(ErrorCode.CSV_FILE_INVALID);
                    }


                    String secondAmount = line.get(10);
                    try {
                        carrierRateMaster.setSecondAmount(Double.parseDouble(secondAmount));
                    } catch (NumberFormatException exception) {
                        log.error("Exception while converting Double value ", exception);
                        baseDto.getParams().add(secondAmount);
                        baseDto.getParams().add(lineNumber.toString());
                        baseDto.getParams().add("11");
                        throw new RestException(ErrorCode.CSV_FILE_INVALID);
                    }


                    String secondMinAmount = line.get(11);
                    try {
                        carrierRateMaster.setSecondMinAmount(Double.parseDouble(secondMinAmount));
                    } catch (NumberFormatException exception) {
                        log.error("Exception while converting Double value ", exception);
                        baseDto.getParams().add(secondMinAmount);
                        baseDto.getParams().add(lineNumber.toString());
                        baseDto.getParams().add("12");
                        throw new RestException(ErrorCode.CSV_FILE_INVALID);
                    }

                    list.add(carrierRateMaster);


                }

            }
            scanner.close();
            baseDto.setResponseCode(ErrorCode.SUCCESS);
            baseDto.setResponseObject(list);

        } catch (RestException exception) {
            log.error("Exception occured in upload method ", exception);
            baseDto.setResponseCode(ErrorCode.CSV_FILE_INVALID);
        } catch (Exception exception) {
            log.error("Exception occured in upload method ", exception);
            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);
        }
        return appUtil.setDesc(baseDto);
    }

}
