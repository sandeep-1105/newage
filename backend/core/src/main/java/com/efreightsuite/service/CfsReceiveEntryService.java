package com.efreightsuite.service;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import com.efreightsuite.activiti.ActivitiTransactionService;
import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.CfsReceiveSearchRequestDto;
import com.efreightsuite.enumeration.EventMasterType;
import com.efreightsuite.enumeration.ImportExport;
import com.efreightsuite.enumeration.SequenceType;
import com.efreightsuite.enumeration.TransportMode;
import com.efreightsuite.enumeration.YesNo;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.exception.UniqueKey;
import com.efreightsuite.model.CfsAttachment;
import com.efreightsuite.model.CfsDimension;
import com.efreightsuite.model.CfsReceiveEntry;
import com.efreightsuite.model.DocumentDetail;
import com.efreightsuite.model.DocumentOldEntry;
import com.efreightsuite.model.EventMaster;
import com.efreightsuite.model.ServiceDocumentDimension;
import com.efreightsuite.model.ServiceOldDimension;
import com.efreightsuite.model.ServiceOldEntry;
import com.efreightsuite.model.ServiceStatus;
import com.efreightsuite.model.Shipment;
import com.efreightsuite.model.ShipmentServiceDetail;
import com.efreightsuite.model.ShipmentServiceEvent;
import com.efreightsuite.repository.CfsAttachmentRepository;
import com.efreightsuite.repository.CfsReceiveEntryRepository;
import com.efreightsuite.repository.EventMasterRepository;
import com.efreightsuite.repository.ServiceOldEntryRepository;
import com.efreightsuite.repository.ShipmentRepository;
import com.efreightsuite.repository.ShipmentServiceDetailRepository;
import com.efreightsuite.search.CfsReceiveSearchImpl;
import com.efreightsuite.util.AppUtil;
import com.efreightsuite.util.TimeUtil;
import com.efreightsuite.validation.CfsReceiveEntryValidator;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.exception.ExceptionUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.ObjectOptimisticLockingFailureException;
import org.springframework.orm.jpa.JpaObjectRetrievalFailureException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Log4j2
public class CfsReceiveEntryService {

    @Autowired
    private
    AppUtil appUtil;

    @Autowired
    private
    ActivitiTransactionService activitiTransactionService;

    @Autowired
    private
    CfsReceiveEntryRepository cfsReceiveEntryRepository;

    @Autowired
    private
    SequenceGeneratorService sequenceGeneratorService;

    @Autowired
    private
    CfsReceiveSearchImpl cfsReceiveSearchImpl;

    @Autowired
    private
    CfsReceiveEntryValidator cfsReceiveEntryValidator;

    @Autowired
    private
    CfsAttachmentRepository cfsAttachmentRepository;

    @Autowired
    private
    ShipmentRepository shipmentRepository;

    @Autowired
    private
    ShipmentServiceDetailRepository shipmentServiceDetailRepository;

    @Autowired
    private
    EventMasterRepository eventMasterRepository;


    @Autowired
    ServiceOldEntryRepository serviceOldEntryRepository;

    public BaseDto get(Long id) {

        log.info("CfsReceiveEntryService.get() method is called....[ " + id + " ]");

        BaseDto baseDto = new BaseDto();

        try {
            CfsReceiveEntry cfsReceiveEntry = cfsReceiveEntryRepository.findById(id);
            if (cfsReceiveEntry.getShipmentServiceDetail() != null && cfsReceiveEntry.getShipmentServiceDetail().getId() != null) {
                cfsReceiveEntry.setShipmentUid(cfsReceiveEntry.getShipmentServiceDetail().getShipmentUid());
            }

            // Making the files Null

            if (cfsReceiveEntry != null) {
                if (cfsReceiveEntry.getCfsAttachementList() != null && cfsReceiveEntry.getCfsAttachementList().size() > 0) {
                    for (CfsAttachment attachment : cfsReceiveEntry.getCfsAttachementList()) {
                        attachment.setFile(null);
                    }
                }
            }


            log.info("Get By ID is successful....");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(cfsReceiveEntry);

        } catch (Exception exception) {

            log.error("Exception while get by id : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);
    }

    public BaseDto search(CfsReceiveSearchRequestDto cfsReceiveSearchDto) {

        log.info("CfsReceiveEntryService.search() method is called...." + cfsReceiveSearchDto);

        BaseDto baseDto = new BaseDto();

        try {

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(cfsReceiveSearchImpl.search(cfsReceiveSearchDto));

        } catch (Exception e) {

            log.error("Exception in CfsReceiveEntryService.search()  method ", e);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);
    }


    private void saveForServiceReceive(ShipmentServiceDetail service, String cfsNo) {

        if (service != null && service.getId() != null && service.getDocumentList() != null) {

            ServiceOldEntry serviceOldEntry = new ServiceOldEntry();
            serviceOldEntry.setBookedChargeableUnit(service.getBookedChargeableUnit());
            serviceOldEntry.setBookedGrossWeightUnitKg(service.getBookedGrossWeightUnitKg());
            serviceOldEntry.setBookedGrossWeightUnitPound(service.getBookedGrossWeightUnitPound());
            serviceOldEntry.setBookedPieces(service.getBookedPieces());
            serviceOldEntry.setBookedVolumeWeightUnitKg(service.getBookedVolumeWeightUnitKg());
            serviceOldEntry.setBookedVolumeWeightUnitPound(service.getBookedGrossWeightUnitPound());
            serviceOldEntry.setServiceUid(service.getServiceUid());
            serviceOldEntry.setShipmentUid(service.getShipmentUid());
            serviceOldEntry.setCfsReceiptNumber(cfsNo);
            serviceOldEntry.setDocumentOldList(new ArrayList<>());
            List<DocumentOldEntry> documentOldList = new ArrayList<>();
            for (DocumentDetail dd : service.getDocumentList()) {

                DocumentOldEntry doe = new DocumentOldEntry();
                doe.setDimensionGrossWeight(dd.getDimensionGrossWeight());
                doe.setDimensionGrossWeightInKg(dd.getDimensionGrossWeightInKg());
                doe.setDimensionVolumeWeight(dd.getDimensionVolumeWeight());
                doe.setDimensionNoOfPieces(dd.getDimensionNoOfPieces());
                doe.setDimensionUnit(dd.getDimensionUnit());
                doe.setDimensionUnitValue(dd.getDimensionUnitValue());
                doe.setServiceEntry(serviceOldEntry);
                doe.setDocumentNo(dd.getDocumentNo());
                doe.setChargebleWeight(dd.getChargebleWeight());
                doe.setGrossWeight(dd.getGrossWeight());
                doe.setGrossWeightInPound(dd.getGrossWeightInPound());
                doe.setVolumeWeight(dd.getVolumeWeightInPound());
                doe.setVolumeWeightInPound(dd.getVolumeWeightInPound());

                if (dd.getDimensionList() != null && dd.getDimensionList().size() > 0) {
                    List<ServiceOldDimension> serviceOldDimensionList = new ArrayList<>();
                    for (ServiceDocumentDimension sdimen : dd.getDimensionList()) {
                        ServiceOldDimension serviceReceiveDimension = new ServiceOldDimension();
                        serviceReceiveDimension.setGrossWeight(sdimen.getGrossWeight());
                        serviceReceiveDimension.setGrossWeightKg(sdimen.getGrossWeightKg());
                        serviceReceiveDimension.setHeight(sdimen.getHeight());
                        serviceReceiveDimension.setLength(sdimen.getLength());
                        serviceReceiveDimension.setNoOfPiece(sdimen.getNoOfPiece());
                        serviceReceiveDimension.setDocumentOldEntry(doe);
                        serviceReceiveDimension.setVolWeight(sdimen.getVolWeight());
                        serviceReceiveDimension.setWidth(sdimen.getWidth());
                        serviceReceiveDimension.setDocumentNo(dd.getDocumentNo());
                        serviceReceiveDimension.setDocumentOldEntry(doe);
                        serviceOldDimensionList.add(serviceReceiveDimension);
                    }
                    doe.setDimensionList(serviceOldDimensionList);
                } else {
                    doe.setDimensionList(null);
                }
                documentOldList.add(doe);
            }
            serviceOldEntry.setDocumentOldList(documentOldList);
            service.setServiceReceiveEntry(serviceOldEntry);
        }
    }

    @Transactional
    public CfsReceiveEntry save(CfsReceiveEntry cfsReceiveEntry) {

        if (cfsReceiveEntry.getId() == null) {
            cfsReceiveEntry.setCfsReceiptNumber(sequenceGeneratorService.getSequence(SequenceType.CFS));
        }

        CfsReceiveEntry cfsReceiveEntryRes = cfsReceiveEntryRepository.save(cfsReceiveEntry);

        ShipmentServiceDetail service = shipmentServiceDetailRepository.findByServiceUid(cfsReceiveEntry.getServiceUid());

        //which is used for saving dimension details of shipment in separate table
        if (service != null && service.getServiceReceiveEntry() == null) {
            saveForServiceReceive(service, cfsReceiveEntry.getCfsReceiptNumber());
        }
        if (service != null) {
            DocumentDetail document = service.getDocumentList().get(0);
            document.getDimensionList().clear();

            List<CfsReceiveEntry> listCfs = cfsReceiveEntryRepository.findByShipmentService(service.getServiceUid());

            {
                Long noOfPiece = 0L;

                Double volumeWeight = 0.0, grossWeight = 0.0, chargable = 0.0,
                        volumeInCbm = 0.0, volumeWeightInPound = 0.0, grossWeightInPound = 0.0,
                        dimVolumeWeight = 0.0, dimGrossWeight = 0.0, dimGrossWeightKg = 0.0;

                for (CfsReceiveEntry cfs : listCfs) {

                    if (cfs.getCfsDimensionList() != null) {
                        for (CfsDimension cfsDim : cfs.getCfsDimensionList()) {
                            ServiceDocumentDimension dim = new ServiceDocumentDimension();
                            dim.setDocumentDetail(document);
                            dim.setNoOfPiece(cfsDim.getNoOfPiece());
                            dim.setHeight(cfsDim.getHeight());
                            dim.setWidth(cfsDim.getWidth());
                            dim.setLength(cfsDim.getLength());
                            dim.setGrossWeight(cfsDim.getGrossWeight());
                            dim.setGrossWeightKg(cfsDim.getGrossWeightKg());
                            dim.setVolWeight(cfsDim.getVolWeight());
                            document.getDimensionList().add(dim);
                        }

                        noOfPiece = noOfPiece + (cfs.getNoOfPieces() == null ? 0 : cfs.getNoOfPieces());
                        volumeWeight = volumeWeight + (cfs.getVolumeWeight() == null ? 0.0 : cfs.getVolumeWeight());
                        grossWeight = grossWeight + (cfs.getGrossWeight() == null ? 0.0 : cfs.getGrossWeight());
                        chargable = chargable + (cfs.getChargebleWeight() == null ? 0.0 : cfs.getChargebleWeight());
                        volumeInCbm = volumeInCbm + (cfs.getBookedVolumeUnitCbm() == null ? 0.0 : cfs.getBookedVolumeUnitCbm());
                        volumeWeightInPound = volumeWeightInPound + (cfs.getVolumeWeightInPound() == null ? 0.0 : cfs.getVolumeWeightInPound());
                        grossWeightInPound = grossWeightInPound + (cfs.getGrossWeightInPound() == null ? 0.0 : cfs.getGrossWeightInPound());

                        dimVolumeWeight = dimVolumeWeight + (cfs.getDimensionVolumeWeight() == null ? 0.0 : cfs.getDimensionVolumeWeight());
                        dimGrossWeight = dimGrossWeight + (cfs.getDimensionGrossWeight() == null ? 0.0 : cfs.getDimensionGrossWeight());
                        dimGrossWeightKg = dimGrossWeightKg + (cfs.getDimensionGrossWeightInKg() == null ? 0.0 : cfs.getDimensionGrossWeightInKg());

                    }
                }

                document.setNoOfPieces(noOfPiece);
                document.setVolumeWeight(volumeWeight);
                document.setGrossWeight(grossWeight);
                document.setBookedVolumeUnitCbm(volumeInCbm);
                document.setVolumeWeightInPound(volumeWeightInPound);
                document.setGrossWeightInPound(grossWeightInPound);
                document.setChargebleWeight(chargable);
                document.setDimensionGrossWeight(dimGrossWeight);
                document.setDimensionGrossWeightInKg(dimGrossWeightKg);
                document.setDimensionVolumeWeight(dimVolumeWeight);
            }

            {
                Long noOfPiece = 0L;

                Double volumeWeight = 0.0, grossWeight = 0.0, chargable = 0.0,
                        volumeInCbm = 0.0, volumeWeightInPound = 0.0, grossWeightInPound = 0.0;

                for (DocumentDetail dd : service.getDocumentList()) {
                    noOfPiece = noOfPiece + (dd.getNoOfPieces() == null ? 0 : dd.getNoOfPieces());
                    volumeWeight = volumeWeight + (dd.getVolumeWeight() == null ? 0.0 : dd.getVolumeWeight());
                    grossWeight = grossWeight + (dd.getGrossWeight() == null ? 0.0 : dd.getGrossWeight());
                    chargable = chargable + (dd.getChargebleWeight() == null ? 0.0 : dd.getChargebleWeight());
                    volumeInCbm = volumeInCbm + (dd.getBookedVolumeUnitCbm() == null ? 0.0 : dd.getBookedVolumeUnitCbm());
                    volumeWeightInPound = volumeWeightInPound + (dd.getVolumeWeightInPound() == null ? 0.0 : dd.getVolumeWeightInPound());
                    grossWeightInPound = grossWeightInPound + (dd.getGrossWeightInPound() == null ? 0.0 : dd.getGrossWeightInPound());
                }

                service.setBookedPieces(noOfPiece);
                service.setBookedGrossWeightUnitPound(grossWeightInPound);
                service.setBookedVolumeWeightUnitPound(volumeWeightInPound);
                service.setBookedChargeableUnit(chargable);
                service.setBookedVolumeWeightUnitKg(volumeWeight);
                service.setBookedGrossWeightUnitKg(grossWeight);
                service.setBookedVolumeUnitCbm(volumeInCbm);
            }


            if (service.getServiceMaster().getTransportMode() == TransportMode.Air
                    && service.getServiceMaster().getImportExport() == ImportExport.Export
                    && service.getServiceMaster().getServiceType() == null) {

                if (service.getLastUpdatedStatus().getServiceStatus()
                        .equals(com.efreightsuite.enumeration.ServiceStatus.Booked)
                        || service.getLastUpdatedStatus()
                        .getServiceStatus() == com.efreightsuite.enumeration.ServiceStatus.Booked) {
                    ServiceStatus shStatus = new ServiceStatus();
                    shStatus.setShipmentServiceDetail(service);
                    shStatus.setServiceStatus(com.efreightsuite.enumeration.ServiceStatus.Received);
                    service.getServiceStatusList().add(shStatus);

                    service.setLastUpdatedStatus(shStatus);

				/*if(service.getEventList()==null || service.getEventList().size()==0){
					//service.setEventList(new ArrayList<>(service.getEventList()));
					service.getEventList().clear();
				}*/

                    ShipmentServiceEvent event = new ShipmentServiceEvent();
                    event.setEventDate(TimeUtil.getCurrentLocationTime());
                    event.setFollowUpRequired(YesNo.No);
                    event.setIsCompleted(YesNo.No);
                    EventMaster eventMaster = eventMasterRepository.findByEventMasterType(EventMasterType.CARGO_RECEIVED);
                    event.setEventMaster(eventMaster);

                    event.setShipmentServiceDetail(service);

                    service.getEventList().add(event);
                }

            }


            shipmentServiceDetailRepository.save(service);

            Shipment shipment = shipmentRepository.findById(service.getShipment().getId());
            activitiTransactionService.createShipmentTask(shipment);
        }
        return cfsReceiveEntryRes;

    }


    public BaseDto create(CfsReceiveEntry cfsReceiveEntry) {

        log.info("CfsReceiveEntryService.create method is called.");

        BaseDto baseDto = new BaseDto();

        try {

            cfsReceiveEntryValidator.validate(cfsReceiveEntry);


            cfsReceiveEntry.setLocation(AuthService.getCurrentUser().getSelectedUserLocation());
            cfsReceiveEntry.setCompanyMaster(AuthService.getCurrentUser().getSelectedUserLocation().getCompanyMaster());
            cfsReceiveEntry.setCountry(AuthService.getCurrentUser().getSelectedUserLocation().getCountryMaster());

            cfsReceiveEntry.setLocationCode(cfsReceiveEntry.getLocation().getLocationCode());
            cfsReceiveEntry.setCompanyCode(cfsReceiveEntry.getCompanyMaster().getCompanyCode());
            cfsReceiveEntry.setCountryCode(cfsReceiveEntry.getCountry().getCountryCode());


            if (cfsReceiveEntry.getShipmentServiceDetail() != null) {
                cfsReceiveEntry.setServiceUid(cfsReceiveEntry.getShipmentServiceDetail().getServiceUid());
            }


            if (cfsReceiveEntry.getCfsDimensionList() != null && cfsReceiveEntry.getCfsDimensionList().size() != 0) {

                for (CfsDimension dimension : cfsReceiveEntry.getCfsDimensionList()) {

                    dimension.setCfsReceiveEntry(cfsReceiveEntry);

                }

            } else {
                cfsReceiveEntry.setCfsDimensionList(null);
            }

            if (cfsReceiveEntry.getCfsAttachementList() != null
                    && cfsReceiveEntry.getCfsAttachementList().size() != 0) {

                for (CfsAttachment attachment : cfsReceiveEntry.getCfsAttachementList()) {


                    attachment.setCfsReceiveEntry(cfsReceiveEntry);

                }

            } else {
                cfsReceiveEntry.setCfsAttachementList(null);
            }

            // Save the Entity


            cfsReceiveEntry = save(cfsReceiveEntry);

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(cfsReceiveEntry.getId());

        } catch (RestException exception) {

            log.error("Exception in CfsReceiveEntryService.create method ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (Exception exception) {

            log.error("Exception Cause   ", exception);

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            String exceptionCause = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Exception Cause  ::: " + exceptionCause);

            if (exceptionCause.contains(UniqueKey.CFS_RECEIVE_ENTRY_ATTACHMENT_REFERENCE_NUMBER)) {
                // TODO : Need to change the error code
                baseDto.setResponseCode(ErrorCode.ENQUIRY_REFERENCE_ALREADY_EXIST);
            }
        }

        return appUtil.setDesc(baseDto);
    }

    public BaseDto update(CfsReceiveEntry cfsReceiveEntry) {

        log.info("CfsReceiveEntryService.update method is called.");

        BaseDto baseDto = new BaseDto();

        try {

            cfsReceiveEntryValidator.validate(cfsReceiveEntry);

            if (cfsReceiveEntry.getShipmentServiceDetail() != null) {
                cfsReceiveEntry.setServiceUid(cfsReceiveEntry.getShipmentServiceDetail().getServiceUid());
            }

            if (cfsReceiveEntry.getCfsDimensionList() != null && cfsReceiveEntry.getCfsDimensionList().size() != 0) {

                for (CfsDimension dimension : cfsReceiveEntry.getCfsDimensionList()) {

                    dimension.setCfsReceiveEntry(cfsReceiveEntry);


                }

            } else {
                cfsReceiveEntry.setCfsDimensionList(new ArrayList<>());
            }

            if (cfsReceiveEntry.getCfsAttachementList() != null
                    && cfsReceiveEntry.getCfsAttachementList().size() != 0) {

                for (CfsAttachment attachment : cfsReceiveEntry.getCfsAttachementList()) {
                    attachment.setCfsReceiveEntry(cfsReceiveEntry);
                }

            } else {
                cfsReceiveEntry.setCfsAttachementList(new ArrayList<>());
            }

            cfsReceiveEntryRepository.getOne(cfsReceiveEntry.getId());

            cfsReceiveEntry = save(cfsReceiveEntry);

            log.info("CFS Receive Entry updated successfully....[" + cfsReceiveEntry.getId() + "]");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(cfsReceiveEntry.getId());

        } catch (RestException exception) {

            log.error("Exception occured with error code ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (ObjectOptimisticLockingFailureException e) {

            log.error("Error in editing cfs Receive Entry", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_LOCKED_RECORD);

        } catch (JpaObjectRetrievalFailureException e) {

            log.error("Error in editing cfs Receive Entry", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_DELETED_RECORD);

        } catch (Exception exception) {

            log.error("Failed to Update the cfs Receive Entry..", exception);

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            String exceptionCause = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Exception Cause  ::: " + exceptionCause);

            if (exceptionCause.contains(UniqueKey.CFS_RECEIVE_ENTRY_ATTACHMENT_REFERENCE_NUMBER)) {
                // TODO : Need to change the error code
                baseDto.setResponseCode(ErrorCode.ENQUIRY_REFERENCE_ALREADY_EXIST);
            }

        }

        return appUtil.setDesc(baseDto);

    }

    public void downloadAttachment(Long attachmentId, HttpServletResponse response) {

        CfsAttachment attachment = cfsAttachmentRepository.getOne(attachmentId);

        if (attachment != null && attachment.getFile() != null && attachment.getFileName() != null) {

            try {

                response.setContentType(attachment.getFileContentType());

                response.setHeader("Content-Disposition", "attachment; filename=" + attachment.getFileName());

                response.setContentLength(attachment.getFile().length);

                response.getOutputStream().write(attachment.getFile());

                response.getOutputStream().flush();

                response.getOutputStream().close();

            } catch (Exception e) {
                log.error("Error occur during download attachment file : " + attachmentId + ", ", e);
            }

        } else {
            log.info("Attachment document is not available : " + attachmentId);
        }

    }

}
