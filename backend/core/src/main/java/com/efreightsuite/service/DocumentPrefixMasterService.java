package com.efreightsuite.service;

import java.util.ArrayList;

import javax.xml.bind.DatatypeConverter;

import com.efreightsuite.common.component.CacheRepository;
import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.DocumentPrefixSearchDto;
import com.efreightsuite.enumeration.DocPrefixDocumentType;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.exception.UniqueKey;
import com.efreightsuite.model.DocPrefixTypeMapping;
import com.efreightsuite.model.DocumentPrefixMaster;
import com.efreightsuite.repository.DocPrefixTypeMappingRepository;
import com.efreightsuite.repository.DocumentPrefixMasterRepository;
import com.efreightsuite.repository.LocationMasterRepository;
import com.efreightsuite.search.DocumentPrefixSearchImpl;
import com.efreightsuite.util.AppUtil;
import com.efreightsuite.util.SaaSUtil;
import com.efreightsuite.util.ValidateUtil;
import com.efreightsuite.validation.DocPrefixTypeMappingValidator;
import com.efreightsuite.validation.DocumentPrefixMasterValidator;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.exception.ExceptionUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.ObjectOptimisticLockingFailureException;
import org.springframework.orm.jpa.JpaObjectRetrievalFailureException;
import org.springframework.stereotype.Service;

@Log4j2
@Service
public class DocumentPrefixMasterService {

    byte[] image;

    @Autowired
    private
    AppUtil appUtil;

    @Autowired
    private
    DocumentPrefixMasterRepository documentPrefixMasterRepository;

    @Autowired
    private
    DocPrefixTypeMappingRepository docPrefixTypeMappingRepository;

    @Autowired
    LocationMasterRepository locationMasterRepository;

    @Autowired
    private
    CacheRepository cacheRepository;

    @Autowired
    private
    DocumentPrefixMasterValidator documentPrefixMasterValidator;

    @Autowired
    private
    DocPrefixTypeMappingValidator docPrefixTypeMappingValidator;

    @Autowired
    private
    DocumentPrefixSearchImpl documentPrefixSearchImpl;

    /**
     * This method lists all document based on the records per page
     *
     * @param id
     * @return
     */

    private DocumentPrefixMaster putCache(DocumentPrefixMaster obj, Long id) {
        DocumentPrefixMaster documentPrefixMaster = obj != null ? obj : documentPrefixMasterRepository.findById(id);

        if (documentPrefixMaster.getDocPrefixTypeMappingList() != null && documentPrefixMaster.getDocPrefixTypeMappingList().size() != 0) {
            for (DocPrefixTypeMapping po : documentPrefixMaster.getDocPrefixTypeMappingList()) {
                po.setDocumentPrefixMaster(null);
                po.setEncodedLogo(DatatypeConverter.printBase64Binary(po.getLogo()));
                po.setLogo(null);

            }
        } else {
            documentPrefixMaster.setDocPrefixTypeMappingList(null);
        }

        return cacheRepository.saveEachItem(SaaSUtil.getSaaSId(), DocPrefixTypeMapping.class.getName(), documentPrefixMaster);
    }


    public DocPrefixTypeMapping getFormula(DocPrefixDocumentType docType, String locationCode) {
        BaseDto baseDto = new BaseDto();
        ValidateUtil.notNull(docType, ErrorCode.DOCUMENT_TYPE_CODE_NOT_NULL);
        ValidateUtil.notNull(locationCode, ErrorCode.LOCATION_CODE_NOT_NULL);
        /*if(docPrefixTypeMapping!=null && docPrefixTypeMapping.getDocumentPrefixMaster()!=null){
			//String formula=docPrefixTypeMapping.getDocumentPrefixMaster().getFormula();
			return docPrefixTypeMapping;
		}else{
			return null;
		}*/
        return docPrefixTypeMappingRepository.getFormula(docType, locationCode);
    }

    /**
     * This method Retrieves the Document Prefix Master By Id
     *
     * @param id
     * @return
     */
    public BaseDto get(Long id) {

        log.info("get method is called....[" + id + "]");

        BaseDto baseDto = new BaseDto();

        try {

            DocumentPrefixMaster documentPrefixMaster = cacheRepository.get(SaaSUtil.getSaaSId(), DocumentPrefixMaster.class.getName(), id);
            if (documentPrefixMaster == null) {
                documentPrefixMaster = putCache(null, id);
            }

            ValidateUtil.notNull(documentPrefixMaster, ErrorCode.DOCUMENT_PREFIXM_ID_NOT_FOUND);

            log.info("Successfully getting Document Prefix by id...[" + documentPrefixMaster.getId() + "]");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(documentPrefixMaster);

        } catch (RestException exception) {

            log.error("Exception in get method... ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (Exception exception) {

            log.error("Exception in get method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);
    }

    /**
     * This method Persists the Document Prefix Master
     *
     * @param documentPrefixMaster
     * @return
     */
    public BaseDto create(DocumentPrefixMaster documentPrefixMaster) {

        log.info("Create method is called.....");

        BaseDto baseDto = new BaseDto();

        try {

            documentPrefixMasterValidator.validate(documentPrefixMaster);
            // Validating each port in portGroup

            if (documentPrefixMaster.getDocPrefixTypeMappingList() != null
                    && documentPrefixMaster.getDocPrefixTypeMappingList().size() != 0) {
                for (DocPrefixTypeMapping documentPrefixMapping : documentPrefixMaster.getDocPrefixTypeMappingList()) {
                    docPrefixTypeMappingValidator.validate(documentPrefixMapping);
                }
            }

            // Setting the port group for each port..
            if (documentPrefixMaster.getDocPrefixTypeMappingList() != null
                    && documentPrefixMaster.getDocPrefixTypeMappingList().size() != 0) {
                for (DocPrefixTypeMapping documentPrefixMapping : documentPrefixMaster.getDocPrefixTypeMappingList()) {
                    documentPrefixMapping.setLogo(documentPrefixMapping.getLogo());
                    documentPrefixMapping.setDocumentPrefixMaster(documentPrefixMaster);
                    documentPrefixMapping.setLocationMaster(documentPrefixMapping.getLocationMaster());
                    documentPrefixMapping.setLocationCode(documentPrefixMapping.getLocationMaster().getLocationCode());
                }
            }

            documentPrefixMaster = documentPrefixMasterRepository.save(documentPrefixMaster);

            putCache(documentPrefixMaster, null);

            log.info("Document Prefix Saved successfully.....[" + documentPrefixMaster.getId() + "]");
            baseDto.setResponseCode(ErrorCode.SUCCESS);
            baseDto.setResponseObject(ErrorCode.SUCCESS);
        } catch (RestException exception) {

            log.error("RestException in Create method : ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (Exception exception) {

            log.error("Exception in Create method : ", exception);

            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Exception Cause 1 ::: " + exceptionCause1);

            baseDto.setResponseCode(ErrorCode.FAILED);

            if (exceptionCause1.contains(UniqueKey.UK_DOCPREFIIX_DOCPREFIXCODE)) {
                baseDto.setResponseCode(ErrorCode.DOCUMENT_PREFIXM_CODE_ALREADY_EXIST);
            }

            if (exceptionCause1.contains(UniqueKey.UK_DOCPREFIIX_DOCPREFIXNAME)) {
                baseDto.setResponseCode(ErrorCode.DOCUMENT_PREFIXM_NAME_ALREADY_EXIST);
            }

            if (exceptionCause1.contains(UniqueKey.UK_DOCPREFIIXTYPE_LOCDOCTYPE)) {
                baseDto.setResponseCode(ErrorCode.DOCUMENT_DOCTYPE_ALREADY_EXIST);
            }
            if (exceptionCause1.contains(UniqueKey.UK_DOCPREFIIXTYPE_LOCREPORTTYPE)) {
                baseDto.setResponseCode(ErrorCode.DOCUMENT_REPORTNAME_ALREADY_EXIST);
            }

        }
        return appUtil.setDesc(baseDto);
    }

    /**
     * This method Updates the Existing Document Prefix Master
     *
     * @param existingDocumentPrefixMaster
     * @return
     */
    public BaseDto update(DocumentPrefixMaster existingDocumentPrefixMaster) {

        log.info("Updated method is called....[" + existingDocumentPrefixMaster.getId() + "]");

        BaseDto baseDto = new BaseDto();

        try {

            documentPrefixMasterRepository.getOne(existingDocumentPrefixMaster.getId());
            documentPrefixMasterValidator.validate(existingDocumentPrefixMaster);
            // Validating each docmapping in docprefix

            if (existingDocumentPrefixMaster.getDocPrefixTypeMappingList() != null
                    && existingDocumentPrefixMaster.getDocPrefixTypeMappingList().size() != 0) {
                for (DocPrefixTypeMapping documentPrefixMapping : existingDocumentPrefixMaster
                        .getDocPrefixTypeMappingList()) {
                    documentPrefixMapping.setEncodedLogo(null);
                    documentPrefixMapping.setLogo(documentPrefixMapping.getLogo());
                    documentPrefixMapping.setDocumentPrefixMaster(existingDocumentPrefixMaster);
                    documentPrefixMapping.setLocationMaster(documentPrefixMapping.getLocationMaster());
                    documentPrefixMapping.setLocationCode(documentPrefixMapping.getLocationMaster().getLocationCode());
                    docPrefixTypeMappingValidator.validate(documentPrefixMapping);
                }
            } else {
                existingDocumentPrefixMaster.setDocPrefixTypeMappingList(new ArrayList<>());
            }

            existingDocumentPrefixMaster = documentPrefixMasterRepository.save(existingDocumentPrefixMaster);

            putCache(existingDocumentPrefixMaster, null);


            log.info("Document Prefix Updated successfully...[" + existingDocumentPrefixMaster.getId() + "]");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(ErrorCode.SUCCESS);

        } catch (RestException exception) {

            log.error("Exception in Update method : ", exception);

            baseDto.setResponseCode(exception.getMessage());
        } catch (ObjectOptimisticLockingFailureException e) {

            log.error("Error in editing TOS", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_LOCKED_RECORD);

        } catch (JpaObjectRetrievalFailureException e) {

            log.error("Error in editing TOS", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_DELETED_RECORD);

        } catch (Exception exception) {

            log.error("Exception in Create method : ", exception);

            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Exception Cause 1 ::: " + exceptionCause1);

            baseDto.setResponseCode(ErrorCode.FAILED);

            if (exceptionCause1.contains(UniqueKey.UK_DOCPREFIIX_DOCPREFIXCODE)) {
                baseDto.setResponseCode(ErrorCode.DOCUMENT_PREFIXM_CODE_ALREADY_EXIST);
            }

            if (exceptionCause1.contains(UniqueKey.UK_DOCPREFIIX_DOCPREFIXNAME)) {
                baseDto.setResponseCode(ErrorCode.DOCUMENT_PREFIXM_NAME_ALREADY_EXIST);
            }

            if (exceptionCause1.contains(UniqueKey.UK_DOCPREFIIXTYPE_LOCDOCTYPE)) {
                baseDto.setResponseCode(ErrorCode.DOCUMENT_DOCTYPE_ALREADY_EXIST);
            }
            if (exceptionCause1.contains(UniqueKey.UK_DOCPREFIIXTYPE_LOCREPORTTYPE)) {
                baseDto.setResponseCode(ErrorCode.DOCUMENT_REPORTNAME_ALREADY_EXIST);
            }

        }
        return appUtil.setDesc(baseDto);
    }

    /**
     * This method deletes the Document Prefix Master By Id
     *
     * @param id
     * @return
     */
    public BaseDto delete(Long id) {

        log.info("Delete method is called....[" + id + "]");

        BaseDto baseDto = new BaseDto();

        try {

            cacheRepository.delete(SaaSUtil.getSaaSId(), DocumentPrefixMaster.class.getName(), id,
                    documentPrefixMasterRepository);

            log.info("Successfully deleted Carrier by id...[" + id + "]");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

        } catch (RestException exception) {

            log.error("Exception in Delete method : ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (Exception exception) {

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            log.error("Exception in Delete method : ", exception);

            String rootCause = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Root Cause of Exception : " + rootCause);

            if (rootCause.contains("FK_")) {
                baseDto.setResponseCode(ErrorCode.CANNOT_DELETE_FK_RECORD);
            }
        }

        return appUtil.setDesc(baseDto);

    }

    public BaseDto search(DocumentPrefixSearchDto searchDto) {

        log.info("Search method is called..keyword.." + searchDto);

        BaseDto baseDto = new BaseDto();

        try {

            baseDto.setResponseCode(ErrorCode.SUCCESS);
            baseDto.setResponseObject(documentPrefixSearchImpl.search(searchDto));

            log.info("Successfully Searching ...");

        } catch (Exception exception) {

            log.error("Exception in Search method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);

    }


}
