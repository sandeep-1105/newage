package com.efreightsuite.service;

import java.util.ArrayList;

import javax.xml.bind.DatatypeConverter;

import com.efreightsuite.common.component.CacheRepository;
import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.CarrierSearchDto;
import com.efreightsuite.dto.SearchRequest;
import com.efreightsuite.enumeration.TransportMode;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.ExceptionHelper;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.exception.UniqueKey;
import com.efreightsuite.model.CarrierAddressMapping;
import com.efreightsuite.model.CarrierEdiMapping;
import com.efreightsuite.model.CarrierMaster;
import com.efreightsuite.repository.CarrierMasterRepository;
import com.efreightsuite.search.CarrierSearchImpl;
import com.efreightsuite.util.AppUtil;
import com.efreightsuite.util.SaaSUtil;
import com.efreightsuite.util.ValidateUtil;
import com.efreightsuite.validation.CarrierMasterValidator;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.exception.ExceptionUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.orm.ObjectOptimisticLockingFailureException;
import org.springframework.orm.jpa.JpaObjectRetrievalFailureException;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class CarrierMasterService {

    @Autowired
    private
    CarrierMasterRepository carrierMasterRepository;

    @Autowired
    private
    AppUtil appUtil;

    @Autowired
    private
    CarrierMasterValidator carrierMasterValidator;

    @Autowired
    private
    CacheRepository cacheRepository;

    @Autowired
    private
    CarrierSearchImpl carrierSearchImpl;

    private CarrierMaster putCache(CarrierMaster obj, Long id) {
        CarrierMaster carrierMaster = obj != null ? obj : carrierMasterRepository.findById(id);
        if (carrierMaster.getAddressList() != null && carrierMaster.getAddressList().size() != 0) {
            for (CarrierAddressMapping cam : carrierMaster.getAddressList()) {
                cam.setCarrierMaster(null);
            }
        } else {
            carrierMaster.setAddressList(null);
        }

        if (carrierMaster.getEdiList() != null && carrierMaster.getEdiList().size() != 0) {
            for (CarrierEdiMapping cam : carrierMaster.getEdiList()) {
                cam.setCarrierMaster(null);
            }
        } else {
            carrierMaster.setEdiList(null);
        }

        return cacheRepository.saveEachItem(SaaSUtil.getSaaSId(), CarrierMaster.class.getName(), carrierMaster);
    }

    public BaseDto search(CarrierSearchDto searchDto) {

        log.info("Search method is called...." + searchDto);

        BaseDto baseDto = new BaseDto();

        try {

            baseDto.setResponseObject(carrierSearchImpl.search(searchDto));
            baseDto.setResponseCode(ErrorCode.SUCCESS);
            log.info("Successfully Searching Carrier...");

        } catch (Exception exception) {

            log.info("Exception in Search method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);
    }

    public BaseDto search(SearchRequest searchRequest) {

        log.info("Search method is called...." + searchRequest);

        BaseDto baseDto = new BaseDto();

        try {
            baseDto.setResponseObject(carrierSearchImpl.search(searchRequest));
            baseDto.setResponseCode(ErrorCode.SUCCESS);
            log.info("Successfully Searching Carrier...");

        } catch (Exception exception) {

            log.info("Exception in Search method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);
    }

    public BaseDto getByCarrierNo(String carrierNo) {
        BaseDto baseDto = new BaseDto();
        CarrierMaster carrierMaster = null;
        if (carrierNo != null && carrierNo.trim().length() > 0) {
            carrierMaster = carrierMasterRepository.getByCarrierNo(carrierNo);
        }
        baseDto.setResponseCode(ErrorCode.SUCCESS);
        baseDto.setResponseObject(carrierMaster);
        return appUtil.setDesc(baseDto);

    }

    public BaseDto get(Long id) {

        log.info("get method is called....[" + id + "]");

        BaseDto baseDto = new BaseDto();

        try {

            CarrierMaster carrierMaster = cacheRepository.get(SaaSUtil.getSaaSId(), CarrierMaster.class.getName(), id);
            if (carrierMaster == null) {
                carrierMaster = putCache(null, id);
            }

            if (carrierMaster.getImage() != null) {
                carrierMaster.setEncodedImage(DatatypeConverter.printBase64Binary(carrierMaster.getImage()));
            }

            carrierMaster.setImage(null);

            log.info("Successfully getting Carrier by id...["
                    + carrierMaster.getId() + "]");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(carrierMaster);

        } catch (RestException exception) {

            log.error("Exception in get method... ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (Exception exception) {

            log.error("Exception in get method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);
    }

    public BaseDto create(CarrierMaster carrierMaster) {

        log.info("Create method is called.....");

        BaseDto baseDto = new BaseDto();

        try {

            carrierMasterValidator.checkValidation(carrierMaster);

            if (carrierMaster.getTransportMode().equals(TransportMode.Ocean)) {
                carrierMaster.setIataCode(null);
            }

            if (carrierMaster.getTransportMode().equals(TransportMode.Air)) {
                carrierMaster.setScacCode(null);
            }


            if (carrierMaster.getEdiList() != null && carrierMaster.getEdiList().size() != 0) {
                for (CarrierEdiMapping cem : carrierMaster.getEdiList()) {
                    cem.setCarrierMaster(carrierMaster);
                    cem.setCarrierCode(carrierMaster.getCarrierCode());
                }
            } else {
                carrierMaster.setEdiList(null);
            }

            if (carrierMaster.getAddressList() != null && carrierMaster.getAddressList().size() != 0) {
                for (CarrierAddressMapping cam : carrierMaster.getAddressList()) {
                    cam.setCarrierMaster(carrierMaster);
                    cam.setCarrierCode(carrierMaster.getCarrierCode());
                    cam.setLocationCode(cam.getLocationMaster().getLocationCode());
                    cam.setPartyCode(cam.getPartyMaster().getPartyCode());

                }
            } else {
                carrierMaster.setAddressList(null);
            }


            carrierMaster = carrierMasterRepository.save(carrierMaster);

            //putCache(carrierMaster,null);
            cacheRepository.remove(SaaSUtil.getSaaSId(), CarrierMaster.class.getName(), carrierMaster.getId());

            log.info("Carrier Master Saved successfully.....["
                    + carrierMaster.getId() + "]");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(ErrorCode.SUCCESS);

        } catch (RestException exception) {

            log.error("RestException in Create method : ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (Exception exception) {

            log.error("Exception in Create method : ", exception);

            String exceptionCause1 = ExceptionUtils
                    .getRootCauseMessage(exception);

            log.error("Exception Cause 1 ::: " + exceptionCause1);

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            if (exceptionCause1.contains(UniqueKey.UK_CARRIER_CARRIERCODE)) {
                baseDto.setResponseCode(ErrorCode.CARRIER_CODE_ALREADY_EXIST);

            }

            if (exceptionCause1.contains(UniqueKey.UK_CARRIER_CARRIERNAME)) {
                baseDto.setResponseCode(ErrorCode.CARRIER_NAME_ALREADY_EXIST);
            }

            if (exceptionCause1.contains(UniqueKey.UK_CARRIER_CARRIERNO)) {
                baseDto.setResponseCode(ErrorCode.CARRIER_NUM_ALREADY_EXIST);
            }

            if (exceptionCause1.contains(UniqueKey.UK_CARRIER_IATACODE)) {
                baseDto.setResponseCode(ErrorCode.CARRIER_IATA_CODE_ALREADY_EXIST);
            }

            if (exceptionCause1.contains(UniqueKey.UK_CARRIER_SCACCODE)) {
                baseDto.setResponseCode(ErrorCode.CARRIER_SCAC_CODE_ALREADY_EXIST);
            }

            if (exceptionCause1.contains(UniqueKey.UK_CARRIEREDI_EDITYPE)) {
                baseDto.setResponseCode(ErrorCode.EDI_TYPE_ALREDY_EXIST);
            }
        }

        return appUtil.setDesc(baseDto);
    }

    public BaseDto update(CarrierMaster existingCarrierMaster) {

        log.info("update method is called....[" + existingCarrierMaster.getId()
                + "]");

        BaseDto baseDto = new BaseDto();

        try {


            carrierMasterValidator.checkValidation(existingCarrierMaster);
            ValidateUtil.notNull(existingCarrierMaster.getId(), ErrorCode.CARRIER_ID_NOT_NULL);

            log.info("existingTosMaster : " + existingCarrierMaster);

            carrierMasterRepository.getOne(existingCarrierMaster.getId());


            if (existingCarrierMaster.getEdiList() != null && existingCarrierMaster.getEdiList().size() != 0) {
                for (CarrierEdiMapping cem : existingCarrierMaster.getEdiList()) {
                    cem.setCarrierMaster(existingCarrierMaster);
                    cem.setCarrierCode(existingCarrierMaster.getCarrierCode());
                }
            } else {
                existingCarrierMaster.setEdiList(new ArrayList<>());
                existingCarrierMaster.getEdiList().clear();
            }


            if (existingCarrierMaster.getAddressList() != null && existingCarrierMaster.getAddressList().size() != 0) {
                for (CarrierAddressMapping cam : existingCarrierMaster.getAddressList()) {
                    cam.setCarrierMaster(existingCarrierMaster);
                    cam.setCarrierCode(existingCarrierMaster.getCarrierCode());
                    cam.setLocationCode(cam.getLocationMaster().getLocationCode());
                    cam.setPartyCode(cam.getPartyMaster().getPartyCode());

                }
            } else {
                existingCarrierMaster.setAddressList(new ArrayList<>());
                existingCarrierMaster.getAddressList().clear();
            }


            existingCarrierMaster = carrierMasterRepository.save(existingCarrierMaster);

            //putCache(existingCarrierMaster,null);
            cacheRepository.remove(SaaSUtil.getSaaSId(), CarrierMaster.class.getName(), existingCarrierMaster.getId());


            log.info("CarrierMaster Updated successfully.....[" + existingCarrierMaster.getId() + "]");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(ErrorCode.SUCCESS);

        } catch (RestException exception) {

            log.error("Exception in Update method : ", exception);

            baseDto.setResponseCode(exception.getMessage());
        } catch (ObjectOptimisticLockingFailureException e) {

            log.error("Error in editing Carrier", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_LOCKED_RECORD);

        } catch (JpaObjectRetrievalFailureException e) {

            log.error("Error in editing Carrier", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_DELETED_RECORD);

        } catch (Exception exception) {

            log.error("Exception in Update method : ", exception);

            String exceptionCause1 = ExceptionUtils
                    .getRootCauseMessage(exception);

            log.error("Exception Cause 1 ::: " + exceptionCause1);

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            if (exceptionCause1.contains(UniqueKey.UK_CARRIER_CARRIERCODE)) {
                baseDto.setResponseCode(ErrorCode.CARRIER_CODE_ALREADY_EXIST);
            }

            if (exceptionCause1.contains(UniqueKey.UK_CARRIER_CARRIERNAME)) {
                baseDto.setResponseCode(ErrorCode.CARRIER_NAME_ALREADY_EXIST);
            }

            if (exceptionCause1.contains(UniqueKey.UK_CARRIER_CARRIERNO)) {
                baseDto.setResponseCode(ErrorCode.CARRIER_NUM_ALREADY_EXIST);
            }

            if (exceptionCause1.contains(UniqueKey.UK_CARRIER_IATACODE)) {
                baseDto.setResponseCode(ErrorCode.CARRIER_IATA_CODE_ALREADY_EXIST);
            }

            if (exceptionCause1.contains(UniqueKey.UK_CARRIER_SCACCODE)) {
                baseDto.setResponseCode(ErrorCode.CARRIER_SCAC_CODE_ALREADY_EXIST);
            }

            if (exceptionCause1.contains(UniqueKey.UK_CARRIEREDI_EDITYPE)) {
                baseDto.setResponseCode(ErrorCode.EDI_TYPE_ALREDY_EXIST);
            }

        }

        return appUtil.setDesc(baseDto);
    }

	

	/*
     * Delete- This method deletes a existing carrier
	 * 
	 * @Param Carrier master entity
	 * 
	 * @Return BaseDto
	 */

    public BaseDto delete(Long id) {

        log.info("delete  method is called....[" + id + "]");

        BaseDto baseDto = new BaseDto();

        try {

            cacheRepository.delete(SaaSUtil.getSaaSId(),
                    CarrierMaster.class.getName(), id, carrierMasterRepository);

            log.info("Successfully deleted Carrier by id...");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

        } catch (DataIntegrityViolationException exception) {

            log.error("Exception occured : ", exception);

            ExceptionHelper helper = new ExceptionHelper(exception);

            log.error("Foreign key reference : " + helper.getFkReference());

            baseDto.setResponseCode(ErrorCode.ERROR_FK_CONSTRAINT);

        } catch (Exception exception) {

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            log.error("Exception in Delete method : ", exception);

            String rootCause = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Root Cause of Exception : " + rootCause);

            if (rootCause.contains("FK_")) {
                baseDto.setResponseCode(ErrorCode.CANNOT_DELETE_FK_RECORD);
            }
        }

        return appUtil.setDesc(baseDto);

    }


    public BaseDto search(SearchRequest searchRequest, String transportMode) {

        log.info("Search method is called....Transport Mode " + transportMode);

        BaseDto baseDto = new BaseDto();

        try {

            baseDto.setResponseObject(carrierSearchImpl.search(searchRequest, transportMode));

            baseDto.setResponseCode(ErrorCode.SUCCESS);

        } catch (Exception exception) {

            log.error("Exception in Search method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);
    }


}
