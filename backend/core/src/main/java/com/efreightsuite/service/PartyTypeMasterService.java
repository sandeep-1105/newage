package com.efreightsuite.service;

import java.util.List;

import com.efreightsuite.common.component.CacheRepository;
import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.PartyTypeSearchDto;
import com.efreightsuite.dto.SearchRequest;
import com.efreightsuite.dto.SearchRespDto;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.ExceptionHelper;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.exception.UniqueKey;
import com.efreightsuite.model.PartyTypeMaster;
import com.efreightsuite.repository.PartyTypeMasterRepository;
import com.efreightsuite.search.PartyTypeSearchImpl;
import com.efreightsuite.util.AppUtil;
import com.efreightsuite.util.SaaSUtil;
import com.efreightsuite.util.ValidateUtil;
import com.efreightsuite.validation.PartyTypeMasterValidator;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.exception.ExceptionUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.orm.ObjectOptimisticLockingFailureException;
import org.springframework.orm.jpa.JpaObjectRetrievalFailureException;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class PartyTypeMasterService {

    @Autowired
    private
    AppUtil appUtil;

    @Autowired
    private
    PartyTypeMasterRepository partyTypeMasterRepository;

    @Autowired
    private
    PartyTypeMasterValidator partyTypeMasterValidator;

    @Autowired
    private
    CacheRepository cacheRepository;


    @Autowired
    private
    PartyTypeSearchImpl partyTypeSearchImpl;


    /*
     * Returns all the CostCenter object as a list based on search keyword
     *
     * @Returns BaseDto
     */
    public BaseDto search(SearchRequest searchRequest) {

        log.info("getAll method is called....");

        BaseDto baseDto = new BaseDto();

        try {
            baseDto.setResponseCode(ErrorCode.SUCCESS);
            baseDto.setResponseObject(partyTypeSearchImpl.search(searchRequest));
            return appUtil.setDesc(baseDto);

        } catch (RestException exception) {

            log.error("RestException in getAll method " + exception);

            baseDto.setResponseCode(exception.getMessage());

            return appUtil.setDesc(baseDto);

        } catch (Exception e) {

            log.error("Exception in getAll method ", e);

            baseDto.setResponseCode(ErrorCode.FAILED);
            return appUtil.setDesc(baseDto);
        }

    }


    public BaseDto getAll() {

        log.info("getAll method is called....");

        BaseDto baseDto = new BaseDto();

        try {
            baseDto.setResponseCode(ErrorCode.SUCCESS);

            List<PartyTypeMaster> tmpList = partyTypeMasterRepository.getAll();

            SearchRespDto searchRespDto = new SearchRespDto(0L, tmpList);

            baseDto.setResponseObject(searchRespDto);
            return appUtil.setDesc(baseDto);

        } catch (RestException exception) {

            log.error("RestException in getAll method " + exception);

            baseDto.setResponseCode(exception.getMessage());

            return appUtil.setDesc(baseDto);

        } catch (Exception e) {

            log.error("Exception in getAll method ", e);

            baseDto.setResponseCode(ErrorCode.FAILED);
            return appUtil.setDesc(baseDto);
        }

    }

    public BaseDto search(PartyTypeSearchDto partyTypeSearchDto) {

        log.info("Search method is called....");

        BaseDto baseDto = new BaseDto();

        try {

            baseDto.setResponseObject(partyTypeSearchImpl.search(partyTypeSearchDto));

            baseDto.setResponseCode(ErrorCode.SUCCESS);

        } catch (Exception exception) {

            log.error("Exception in Search method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);
    }


    public BaseDto create(PartyTypeMaster partyTypeMaster) {

        log.info("Create method is called..");

        BaseDto baseDto = new BaseDto();

        try {

            partyTypeMasterValidator.validate(partyTypeMaster);


            partyTypeMaster = cacheRepository.save(SaaSUtil.getSaaSId(), PartyTypeMaster.class.getName(), partyTypeMaster, partyTypeMasterRepository);

            log.info("Party Type Master Saved successfully.....[" + partyTypeMaster.getId() + "]");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(partyTypeMaster);

        } catch (RestException exception) {

            log.error("Exception Cause 1 ::: ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (Exception exception) {

            log.error("Exception occured while creating party : ", exception);

            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Exception Cause 1 : " + exceptionCause1);

            // General error condition
            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);


            if (exceptionCause1.contains(UniqueKey.UK_PARTYTYPE_PARTYTYPECODE)) {
                baseDto.setResponseCode(ErrorCode.PARTY_TYPE_CODE_ALREADY_EXIST);
            }

            if (exceptionCause1.contains(UniqueKey.UK_PARTYTYPE_PARTYTYPENAME)) {
                baseDto.setResponseCode(ErrorCode.PARTY_TYPE_NAME_ALREADY_EXIST);
            }

        }

        return appUtil.setDesc(baseDto);
    }

    public BaseDto update(PartyTypeMaster partyTypeMaster) {

        log.info("Update method is called - PartyTypeMaster : [" + partyTypeMaster.getId() + "]");

        BaseDto baseDto = new BaseDto();

        try {

            partyTypeMasterValidator.validate(partyTypeMaster);


            partyTypeMasterRepository.getOne(partyTypeMaster.getId());

            partyTypeMaster = cacheRepository.save(SaaSUtil.getSaaSId(), PartyTypeMaster.class.getName(), partyTypeMaster, partyTypeMasterRepository);

            log.info("PartyTypeMaster updated successfully....[" + partyTypeMaster.getId() + "]");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(partyTypeMaster);

        } catch (RestException exception) {

            log.error("RestException occured with error code ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (ObjectOptimisticLockingFailureException e) {

            log.error("Error in editing partyTypeMaster", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_LOCKED_RECORD);

        } catch (JpaObjectRetrievalFailureException e) {

            log.error("Error in editing partyTypeMaster", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_DELETED_RECORD);

        } catch (Exception exception) {

            log.error("Exception occured : ", exception);

            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Exception Cause 1 ::: " + exceptionCause1);


            // General error condition
            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);


            if (exceptionCause1.contains(UniqueKey.UK_PARTYTYPE_PARTYTYPECODE)) {
                baseDto.setResponseCode(ErrorCode.PARTY_TYPE_CODE_ALREADY_EXIST);
            }

            if (exceptionCause1.contains(UniqueKey.UK_PARTYTYPE_PARTYTYPENAME)) {
                baseDto.setResponseCode(ErrorCode.PARTY_TYPE_NAME_ALREADY_EXIST);
            }


        }
        return appUtil.setDesc(baseDto);
    }


    public BaseDto delete(Long id) {

        log.info("Delete method is Invoked...[" + id + "]");

        BaseDto baseDto = new BaseDto();

        try {

            cacheRepository.delete(SaaSUtil.getSaaSId(), PartyTypeMaster.class.getName(), id, partyTypeMasterRepository);

            log.info("PartyTypeMaster Deleted Successfully....[" + id + "]");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

        } catch (RestException exception) {

            log.error("Exception in Delete method : ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (DataIntegrityViolationException exception) {

            log.error("Exception occured : ", exception);

            ExceptionHelper helper = new ExceptionHelper(exception);

            log.error("Foreign key reference : " + helper.getFkReference());

            baseDto.setResponseCode(ErrorCode.ERROR_FK_CONSTRAINT);

        } catch (Exception exception) {

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            log.error("Exception in Delete method : ", exception);

            String rootCause = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Root Cause of Exception : " + rootCause);

            if (rootCause.contains("FK_")) {
                baseDto.setResponseCode(ErrorCode.CANNOT_DELETE_FK_RECORD);
            }
        }

        return appUtil.setDesc(baseDto);
    }

    public BaseDto get(Long id) {

        log.info("get method is called...[" + id + "]");

        BaseDto baseDto = new BaseDto();

        try {

            PartyTypeMaster partyTypeMaster = cacheRepository.get(SaaSUtil.getSaaSId(), PartyTypeMaster.class.getName(), id, partyTypeMasterRepository);

            ValidateUtil.notNull(partyTypeMaster, ErrorCode.PARTY_TYPE_ID_NOT_FOUND);

            log.info("get is successfully executed....[" + partyTypeMaster.getId() + "]");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(partyTypeMaster);

        } catch (RestException exception) {

            log.error("Exception in get method : ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (Exception exception) {

            log.error("Exception in get method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);
    }

}
