package com.efreightsuite.service;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.enumeration.*;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.exception.UniqueKey;
import com.efreightsuite.model.*;
import com.efreightsuite.repository.*;
import com.efreightsuite.util.AppUtil;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.orm.ObjectOptimisticLockingFailureException;
import org.springframework.orm.jpa.JpaObjectRetrievalFailureException;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class InitSetupService {

    @Autowired
    private
    AppUtil appUtil;

    @Value("${efs.resource.path}")
    private
    String appResourcePath;

    @Autowired
    private
    ReportConfigurationMasterRepository reportConfigRepo;

    @Autowired
    private
    CountryMasterRepository countryRepo;

    @Autowired
    private
    StateMasterRepository stateRepo;

    @Autowired
    private
    CityMasterRepository cityRepo;

    @Autowired
    private
    CurrencyMasterRepository currencyRepo;

    @Autowired
    private
    PeriodMasterRepository periodRepo;

    @Autowired
    private
    PartyTypeMasterRepository partyTypeRepo;

    @Autowired
    private
    TriggerTypeMasterRepository triggerTypeRepo;

    @Autowired
    private
    TriggerMasterRepository triggerRepo;

    @Autowired
    private
    PortMasterRepository portRepo;

    @Autowired
    private
    PortGroupMasterRepository portGroupRepo;

    @Autowired
    private
    ServiceTypeMasterRepository serviceTypeRepo;

    @Autowired
    private
    ServiceMasterRepository serviceRepo;

    @Autowired
    private
    ServiceMappingRepository serviceMappingRepo;

    @Autowired
    private
    EnquiryServiceMappingRepository enquiryServiceMappingRepo;

    @Autowired
    private
    LanguageRepository languageRepo;

    @Autowired
    private
    LanguageDetailRepository languageDetailRepository;

    @Autowired
    private
    RegularExpressionRepository regularExpressionRepo;

    @Autowired
    private
    FeatureRepository featureRepo;

    @Autowired
    private
    DefaultMasterDataRepository defaultMasterDataRepo;

    @Autowired
    private
    SequenceGeneratorRepository sequenceGeneratorRepo;

    @Autowired
    private
    DocumentTypeMasterRepository documentTypeRepo;

    @Autowired
    private
    MetaConfigurationService metaConfigurationService;

    @Autowired
    private
    PartyMasterRepository partyMasterRepo;

    @Autowired
    AgentPortMasterRepository agentPortMasterRepository;

    @Autowired
    private
    EmailTemplateRepository emailTemplateRepository;

    @Autowired
    private
    TosMasterRepository tosMasterRepository;

    @Autowired
    private
    UnitMasterRepository unitMasterRepository;

    @Autowired
    private
    EdiConfigurationMasterRepository ediConfigurationMasterRepository;

    @Autowired
    private
    SequenceGeneratorService sequenceGeneratorService;

    @Autowired
    private
    DepartmentMasterRepository departmentMasterRepository;

    @Autowired
    private
    DesignationMasterRepository designationMasterRepository;

    @Autowired
    private
    CommodityMasterRepository commodityMasterRepository;

    @Autowired
    private
    CarrierMasterRepository carrierMasterRepo;


    @Autowired
    private
    EventMasterRepository eventMasterRepository;

    @Autowired
    private
    ReferenceTypeMasterRepository referenceTypeMasterRepository;

    @Autowired
    private
    ChargeMasterRepository chargeRepo;

    @Autowired
    private
    RegionMasterRepository regionRepo;

    @Autowired
    private
    GeneralLedgerGroupRepository glGroupRepo;

    @Autowired
    private
    GeneralLedgerSubGroupRepository glSubGroupRepo;

    @Autowired
    private
    GeneralLedgerMasterRepository glMasterRepo;

    @Autowired
    private
    GeneralLedgerRepository glAccountRepo;

    @Autowired
    private
    PackMasterRepository packRepo;

    @Autowired
    private
    GeneralLedgerChargeMappingRepository glChargeRepo;

    @Autowired
    private
    DynamicFieldRepository dynamicFieldRepository;

    public BaseDto insertBaseData() {

        BaseDto baseDto = new BaseDto();

		/*for(LocationMaster locationMaster : locationRepo.findAll()){
            if(locationMaster.getId()==1l){
				continue;
			}
			loadDefaultMaster(new BaseDto(), locationMaster);
			
			loadReportConfig(new BaseDto(), locationMaster);
			
			loadEdiConfiguration(new BaseDto(),locationMaster);
			
		}*/


        return appUtil.setDesc(baseDto);
    }

    private String firstCase(String str) {

        String[] strArray = str.split(" ");

        StringBuilder tmp = new StringBuilder();

        if (strArray.length == 1) {
            tmp = new StringBuilder(strArray[0].substring(0, 1).toUpperCase());
            tmp.append(strArray[0].substring(1).toLowerCase());
        } else {
            for (String st : strArray) {
                String tmp1 = st.substring(0, 1).toUpperCase();
                tmp1 = tmp1 + st.substring(1).toLowerCase();
                tmp.append(" ").append(tmp1);
            }
        }

        return tmp.toString().trim();
    }

    private void regionData(XSSFSheet firstSheet) {

        List<RegionMaster> entityList = new ArrayList<>();

        for (int i = 1; i <= firstSheet.getLastRowNum(); i++) {

            firstSheet.getRow(i).getCell(0).setCellType(Cell.CELL_TYPE_STRING);
            String code = firstSheet.getRow(i).getCell(0).getStringCellValue().trim().toUpperCase();
            RegionMaster tmpObj = regionRepo.findByRegionCode(code);

            if (tmpObj == null) {

                RegionMaster entity = new RegionMaster();
                entity.setStatus(LovStatus.Active);
                entity.setRegionCode(code);
                entity.setRegionName(firstSheet.getRow(i).getCell(1).getStringCellValue().trim());
                entityList.add(entity);

            }
            if (entityList.size() == 50) {
                regionRepo.save(entityList);
                regionRepo.flush();
                entityList = new ArrayList<>();
            }
        }

        if (entityList.size() != 0) {
            regionRepo.save(entityList);
        }
    }

    private void countryData(XSSFSheet firstSheet) {

        Map<String, RegionMaster> regionMap = new HashMap<>();
        List<CountryMaster> entityList = new ArrayList<>();

        for (int i = 1; i <= firstSheet.getLastRowNum(); i++) {

            firstSheet.getRow(i).getCell(3).setCellType(Cell.CELL_TYPE_STRING);
            String regionCode = firstSheet.getRow(i).getCell(3).getStringCellValue();

            if (regionCode != null && regionCode.trim().length() != 0
                    && regionMap.get(regionCode.trim().toUpperCase()) == null) {
                RegionMaster reg = regionRepo.findByRegionCode(regionCode.trim().toUpperCase());
                if (reg != null) {
                    regionMap.put(regionCode.trim().toUpperCase(), reg);
                }
            }

            CountryMaster tmpObj = countryRepo
                    .findByCountryCode(firstSheet.getRow(i).getCell(0).getStringCellValue().trim().toUpperCase());

            if (tmpObj == null) {

                CountryMaster entity = new CountryMaster();
                if (regionCode != null && regionCode.trim().length() != 0
                        && regionMap.get(regionCode.trim().toUpperCase()) != null) {
                    entity.setRegionMaster(regionMap.get(regionCode.trim().toUpperCase()));
                    entity.setRegionCode(entity.getRegionMaster().getRegionCode());
                }
                entity.setStatus(LovStatus.Active);
                entity.setCountryCode(firstSheet.getRow(i).getCell(0).getStringCellValue().trim().toUpperCase());
                entity.setCountryName(firstCase(firstSheet.getRow(i).getCell(1).getStringCellValue().trim()));
                entity.setLocale(firstSheet.getRow(i).getCell(2).getStringCellValue().trim());

                File logoFile = new File(appResourcePath + "/efs-master-data/country-logo/"
                        + entity.getCountryCode().trim().toLowerCase() + ".png");
                try (FileInputStream logo = new FileInputStream(logoFile)) {

                    if (logo != null) {
                        byte[] bytesArray = new byte[(int) logoFile.length()];
                        int read = logo.read(bytesArray);// read file into bytes[]
                        logo.close();
                        entity.setImage(bytesArray);
                    }

                } catch (Exception e) {
                    log.error("Logo fetch " + e);
                }

                entityList.add(entity);

            }
            if (entityList.size() == 50) {
                countryRepo.save(entityList);
                countryRepo.flush();
                entityList = new ArrayList<>();
            }
        }

        if (entityList.size() != 0) {
            countryRepo.save(entityList);
        }
    }

    public void loadCountry(BaseDto baseDto) {


        try (XSSFWorkbook workbook = new XSSFWorkbook(
                new FileInputStream(new File(appResourcePath + "/efs-master-data/country-master.xlsx")))) {


            regionData(workbook.getSheetAt(0));

            countryData(workbook.getSheetAt(1));

            workbook.close();


        } catch (RestException exception) {

            log.error("->Exception in Create method : ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (ObjectOptimisticLockingFailureException e) {

            log.error("Exception in  -> saveOrUpdate method ", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_LOCKED_RECORD);

        } catch (JpaObjectRetrievalFailureException e) {

            log.error("Exception in  -> saveOrUpdate method ", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_DELETED_RECORD);

        } catch (Exception exception) {
            log.error("Failed to save ..", exception);

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Exception Cause 1 ::: " + exceptionCause1);

            // General error condition
            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            if (exceptionCause1.contains("UK_COUNTRY_COUNTRYCODE")) {
                log.error("Duplicate Country Code");
                baseDto.setResponseCode(ErrorCode.COUNTRY_CODE_ALREADY_EXIST);
            }
            if (exceptionCause1.contains("UK_COUNTRY_COUNTRYNAME")) {
                log.error("Duplicate Country name");
                baseDto.setResponseCode(ErrorCode.COUNTRY_NAME_ALREADY_EXIST);
            }

            if (exceptionCause1.contains(UniqueKey.UK_REGION_REGIONCODE)) {
                baseDto.setResponseCode(ErrorCode.REGION_CODE_ALREADY_EXIST);
            }
            if (exceptionCause1.contains(UniqueKey.UK_REGION_REGIONNAME)) {
                baseDto.setResponseCode(ErrorCode.REGION_NAME_ALREADY_EXIST);
            }
        }

    }

    public void loadState(BaseDto baseDto) {


        try (XSSFWorkbook workbook = new XSSFWorkbook(
                new FileInputStream(new File(appResourcePath + "/efs-master-data/state-master.xlsx")))) {
            XSSFSheet firstSheet = workbook.getSheetAt(0);

            Map<String, CountryMaster> map = new HashMap<>();

            List<StateMaster> entityList = new ArrayList<>();

            for (int i = 1; i <= firstSheet.getLastRowNum(); i++) {

                String countryCode = firstSheet.getRow(i).getCell(2).getStringCellValue().trim().toUpperCase();

                if (map.get(countryCode) == null) {
                    CountryMaster tmpObj = countryRepo.findByCountryCode(countryCode);
                    map.put(countryCode, tmpObj);
                }

                firstSheet.getRow(i).getCell(0).setCellType(Cell.CELL_TYPE_STRING);
                String stateCode = firstSheet.getRow(i).getCell(0).getStringCellValue().trim().toUpperCase();
                StateMaster stateMaster = stateRepo.getState(stateCode, map.get(countryCode).getId());

                if (stateMaster == null) {

                    StateMaster entity = new StateMaster();
                    entity.setStatus(LovStatus.Active);
                    entity.setCountryCode(map.get(countryCode).getCountryCode());
                    entity.setCountryMaster(map.get(countryCode));
                    entity.setStateCode(stateCode);
                    entity.setStateName(firstCase(firstSheet.getRow(i).getCell(1).getStringCellValue().trim()));

                    entityList.add(entity);

                }

                if (entityList.size() == 50) {
                    stateRepo.save(entityList);
                    stateRepo.flush();
                    entityList = new ArrayList<>();
                }

            }

            if (entityList.size() != 0) {
                stateRepo.save(entityList);
            }

            workbook.close();


        } catch (RestException exception) {

            log.error("->Exception in Create method : ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (ObjectOptimisticLockingFailureException e) {

            log.error("Exception in  -> saveOrUpdate method ", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_LOCKED_RECORD);

        } catch (JpaObjectRetrievalFailureException e) {

            log.error("Exception in  -> saveOrUpdate method ", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_DELETED_RECORD);

        } catch (Exception exception) {
            log.error("Failed to save ..", exception);

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Exception Cause 1 ::: " + exceptionCause1);

            // General error condition
            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            if (exceptionCause1.contains("FK_")) {
                baseDto.setResponseCode(ErrorCode.SAVE_UPDATE_FK_RECORD);
            }

            if (exceptionCause1.contains(UniqueKey.UK_STATE_STATECODE)) {
                baseDto.setResponseCode(ErrorCode.STATE_CODE_ALREADY_EXIST);
            }

            if (exceptionCause1.contains(UniqueKey.UK_STATE_STATENAME)) {
                baseDto.setResponseCode(ErrorCode.STATE_NAME_ALREADY_EXIST);
            }

        }

    }

    public void loadCityMaster(BaseDto baseDto) {


        try (XSSFWorkbook workbook = new XSSFWorkbook(
                new FileInputStream(new File(appResourcePath + "/efs-master-data/city-master.xlsx")))) {
            XSSFSheet firstSheet = workbook.getSheetAt(0);

            Map<String, StateMaster> map = new HashMap<>();

            Map<String, CountryMaster> mapCountry = new HashMap<>();
            List<CityMaster> entityList = new ArrayList<>();

            for (int i = 1; i <= firstSheet.getLastRowNum(); i++) {

                String countryCode = firstSheet.getRow(i).getCell(3).getStringCellValue().trim().toUpperCase();
                String stateCode = firstSheet.getRow(i).getCell(2).getStringCellValue().trim().toUpperCase();
                if (stateCode == null || stateCode.length() == 0) {
                    stateCode = null;
                }
                String cityCode = firstSheet.getRow(i).getCell(0).getStringCellValue().trim().toUpperCase();

                if (map.get((stateCode + countryCode)) == null) {
                    StateMaster tmpObj = stateRepo.getState(stateCode, countryCode);
                    map.put((stateCode + countryCode), tmpObj);
                }

                if (mapCountry.get(countryCode) == null) {
                    CountryMaster tmpObj = countryRepo.findByCountryCode(countryCode);
                    mapCountry.put(countryCode, tmpObj);
                }

                CityMaster cityMaster = cityRepo.findByCityCodeAndCountryCodeAndStateCode(cityCode, countryCode,
                        stateCode);

                if (cityMaster == null) {

                    CityMaster entity = new CityMaster();

                    if (map.get((stateCode + countryCode)) != null) {
                        StateMaster st = map.get((stateCode + countryCode));
                        entity.setStateCode(st.getStateCode());
                        entity.setStateMaster(st);
                    }

                    if (mapCountry.get(countryCode) != null) {
                        CountryMaster st = mapCountry.get(countryCode);
                        entity.setCountryCode(st.getCountryCode());
                        entity.setCountryMaster(st);
                    }

                    entity.setCityCode(cityCode);
                    entity.setCityName(firstCase(firstSheet.getRow(i).getCell(1).getStringCellValue().trim()));

                    entity.setStatus(LovStatus.Active);

                    entityList.add(entity);
                }

                if (entityList.size() == 50) {
                    cityRepo.save(entityList);
                    cityRepo.flush();
                    entityList = new ArrayList<>();
                }

            }

            if (entityList.size() != 0) {
                cityRepo.save(entityList);
            }

            workbook.close();


        } catch (RestException exception) {

            log.error("->Exception in Create method : ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (ObjectOptimisticLockingFailureException e) {

            log.error("Exception in  -> saveOrUpdate method ", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_LOCKED_RECORD);

        } catch (JpaObjectRetrievalFailureException e) {

            log.error("Exception in  -> saveOrUpdate method ", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_DELETED_RECORD);

        } catch (Exception exception) {
            log.error("Failed to save ..", exception);

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Exception Cause 1 ::: " + exceptionCause1);

            // General error condition
            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            if (exceptionCause1.contains("FK_")) {
                baseDto.setResponseCode(ErrorCode.SAVE_UPDATE_FK_RECORD);
            }

            if (exceptionCause1.contains(UniqueKey.UK_CITY_CITYCODE)) {
                baseDto.setResponseCode(ErrorCode.CITY_CODE_ALREADY_EXIST);
            }

            if (exceptionCause1.contains(UniqueKey.UK_CITY_CITYNAME)) {
                baseDto.setResponseCode(ErrorCode.CITY_NAME_ALREADY_EXIST);
            }

        }

    }

    public void loadPeriod(BaseDto baseDto) {


        try (XSSFWorkbook workbook = new XSSFWorkbook(
                new FileInputStream(new File(appResourcePath + "/efs-master-data/period-master.xlsx")))) {
            XSSFSheet firstSheet = workbook.getSheetAt(0);

            List<PeriodMaster> entityList = new ArrayList<>();

            for (int i = 1; i <= firstSheet.getLastRowNum(); i++) {

                PeriodMaster tmpObj = periodRepo
                        .findByFrequencyCode(firstSheet.getRow(i).getCell(0).getStringCellValue().trim().toUpperCase());

                if (tmpObj == null) {

                    PeriodMaster entity = new PeriodMaster();
                    entity.setStatus(LovStatus.Active);
                    entity.setFrequencyCode(firstSheet.getRow(i).getCell(0).getStringCellValue().trim().toUpperCase());
                    entity.setFrequencyName(firstCase(firstSheet.getRow(i).getCell(1).getStringCellValue().trim()));
                    firstSheet.getRow(i).getCell(2).setCellType(Cell.CELL_TYPE_STRING);
                    entity.setNoOfDay(Long.parseLong(firstSheet.getRow(i).getCell(2).getStringCellValue()));

                    entityList.add(entity);

                }
                if (entityList.size() == 50) {
                    periodRepo.save(entityList);
                    periodRepo.flush();
                    entityList = new ArrayList<>();
                }
            }
            if (entityList.size() != 0) {
                periodRepo.save(entityList);
            }
            workbook.close();


        } catch (RestException exception) {

            log.error("->Exception in Create method : ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (ObjectOptimisticLockingFailureException e) {

            log.error("Exception in  -> saveOrUpdate method ", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_LOCKED_RECORD);

        } catch (JpaObjectRetrievalFailureException e) {

            log.error("Exception in  -> saveOrUpdate method ", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_DELETED_RECORD);

        } catch (Exception exception) {
            log.error("Failed to save ..", exception);

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Exception Cause 1 ::: " + exceptionCause1);

            // General error condition
            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            if (exceptionCause1.contains("FK_")) {
                baseDto.setResponseCode(ErrorCode.SAVE_UPDATE_FK_RECORD);
            }

            if (exceptionCause1.contains(UniqueKey.UK_PERIOD_FREQUENCY_CODE)) {
                baseDto.setResponseCode(ErrorCode.PERIOD_FREQUENCY_CODE_ALREADY_EXIST);
            }

            if (exceptionCause1.contains(UniqueKey.UK_PERIOD_FREQUENCY_NAME)) {
                baseDto.setResponseCode(ErrorCode.PERIOD_FREQUENCY_NAME_ALREADY_EXIST);
            }
        }

    }

    public void loadPartyType(BaseDto baseDto) {


        try (XSSFWorkbook workbook = new XSSFWorkbook(
                new FileInputStream(new File(appResourcePath + "/efs-master-data/party-type-master.xlsx")))) {
            XSSFSheet firstSheet = workbook.getSheetAt(0);

            List<PartyTypeMaster> entityList = new ArrayList<>();

            for (int i = 1; i <= firstSheet.getLastRowNum(); i++) {

                String code = firstSheet.getRow(i).getCell(0).getStringCellValue().trim().toUpperCase();
                PartyTypeMaster tmpObj = partyTypeRepo.findByPartyTypeCode(code);

                if (tmpObj == null) {

                    PartyTypeMaster entity = new PartyTypeMaster();
                    entity.setStatus(LovStatus.Active);
                    entity.setPartyTypeCode(firstSheet.getRow(i).getCell(0).getStringCellValue().trim().toUpperCase());
                    entity.setPartyTypeName(firstCase(firstSheet.getRow(i).getCell(1).getStringCellValue().trim()));

                    entityList.add(entity);

                }
                if (entityList.size() == 50) {
                    partyTypeRepo.save(entityList);
                    partyTypeRepo.flush();
                    entityList = new ArrayList<>();
                }
            }
            if (entityList.size() != 0) {
                partyTypeRepo.save(entityList);
            }
            workbook.close();


        } catch (RestException exception) {

            log.error("->Exception in Create method : ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (ObjectOptimisticLockingFailureException e) {

            log.error("Exception in  -> saveOrUpdate method ", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_LOCKED_RECORD);

        } catch (JpaObjectRetrievalFailureException e) {

            log.error("Exception in  -> saveOrUpdate method ", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_DELETED_RECORD);

        } catch (Exception exception) {
            log.error("Failed to save ..", exception);

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Exception Cause 1 ::: " + exceptionCause1);

            // General error condition
            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            if (exceptionCause1.contains("FK_")) {
                baseDto.setResponseCode(ErrorCode.SAVE_UPDATE_FK_RECORD);
            }

            if (exceptionCause1.contains(UniqueKey.UK_PARTYTYPE_PARTYTYPECODE)) {
                baseDto.setResponseCode(ErrorCode.PARTY_TYPE_CODE_ALREADY_EXIST);
            }

            if (exceptionCause1.contains(UniqueKey.UK_PARTYTYPE_PARTYTYPENAME)) {
                baseDto.setResponseCode(ErrorCode.PARTY_TYPE_NAME_ALREADY_EXIST);
            }
        }

    }

    public void loadTriggerType(BaseDto baseDto) {


        try (XSSFWorkbook workbook = new XSSFWorkbook(
                new FileInputStream(new File(appResourcePath + "/efs-master-data/trigger-type-master.xlsx")))) {
            XSSFSheet firstSheet = workbook.getSheetAt(0);

            List<TriggerTypeMaster> entityList = new ArrayList<>();

            for (int i = 1; i <= firstSheet.getLastRowNum(); i++) {

                TriggerTypeMaster tmpObj = triggerTypeRepo.findByTriggerTypeCode(
                        firstSheet.getRow(i).getCell(0).getStringCellValue().trim().toUpperCase());

                if (tmpObj == null) {

                    TriggerTypeMaster entity = new TriggerTypeMaster();
                    entity.setStatus(LovStatus.Active);
                    entity.setTriggerTypeCode(
                            firstSheet.getRow(i).getCell(0).getStringCellValue().trim().toUpperCase());
                    entity.setTriggerTypeName(firstCase(firstSheet.getRow(i).getCell(1).getStringCellValue().trim()));

                    entityList.add(entity);

                }
                if (entityList.size() == 50) {
                    triggerTypeRepo.save(entityList);
                    triggerTypeRepo.flush();
                    entityList = new ArrayList<>();
                }
            }
            if (entityList.size() != 0) {
                triggerTypeRepo.save(entityList);
            }
            workbook.close();


        } catch (RestException exception) {

            log.error("->Exception in Create method : ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (ObjectOptimisticLockingFailureException e) {

            log.error("Exception in  -> saveOrUpdate method ", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_LOCKED_RECORD);

        } catch (JpaObjectRetrievalFailureException e) {

            log.error("Exception in  -> saveOrUpdate method ", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_DELETED_RECORD);

        } catch (Exception exception) {
            log.error("Failed to save ..", exception);

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Exception Cause 1 ::: " + exceptionCause1);

            // General error condition
            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            if (exceptionCause1.contains("FK_")) {
                baseDto.setResponseCode(ErrorCode.SAVE_UPDATE_FK_RECORD);
            }

            // Duplicate Trigger Type Code
            if (exceptionCause1.contains(UniqueKey.UK_TRIGGERTYPE_TRIGGERTYPECODE)) {
                baseDto.setResponseCode(ErrorCode.TRIGGER_TYPE_CODE_ALREADY_EXIST);
            }

            // Duplicate Trigger Type Name
            if (exceptionCause1.contains(UniqueKey.UK_TRIGGERTYPE_TRIGGERTYPENAME)) {
                baseDto.setResponseCode(ErrorCode.TRIGGER_TYPE_NAME_ALREADY_EXIST);
            }

        }

    }

    public void loadTrigger(BaseDto baseDto) {


        try (XSSFWorkbook workbook = new XSSFWorkbook(
                new FileInputStream(new File(appResourcePath + "/efs-master-data/trigger-master.xlsx")))) {
            XSSFSheet firstSheet = workbook.getSheetAt(0);

            Map<String, TriggerTypeMaster> map = new HashMap<>();

            List<TriggerMaster> entityList = new ArrayList<>();

            for (int i = 1; i <= firstSheet.getLastRowNum(); i++) {

                firstSheet.getRow(i).getCell(0).setCellType(Cell.CELL_TYPE_STRING);
                String code = firstSheet.getRow(i).getCell(0).getStringCellValue().trim().toUpperCase();

                if (map.get(code) == null) {
                    TriggerTypeMaster tmpObj = triggerTypeRepo.findByTriggerTypeCode(code);
                    map.put(code, tmpObj);
                }

                TriggerMaster tmpObj = triggerRepo.findByTriggerCode(code);

                if (tmpObj == null) {

                    TriggerMaster entity = new TriggerMaster();
                    entity.setStatus(LovStatus.Active);
                    entity.setTriggerTypeMaster(map.get(code));

                    firstSheet.getRow(i).getCell(1).setCellType(Cell.CELL_TYPE_STRING);
                    entity.setTriggerCode(firstSheet.getRow(i).getCell(1).getStringCellValue().trim().toUpperCase());
                    entity.setTriggerName(firstCase(firstSheet.getRow(i).getCell(2).getStringCellValue().trim()));
                    entity.setNote(firstSheet.getRow(i).getCell(3).getStringCellValue().trim());

                    entityList.add(entity);

                }
                if (entityList.size() == 50) {
                    triggerRepo.save(entityList);
                    triggerRepo.flush();
                    entityList = new ArrayList<>();
                }
            }
            if (entityList.size() != 0) {
                triggerRepo.save(entityList);
            }
            workbook.close();

        } catch (RestException exception) {

            log.error("->Exception in Create method : ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (ObjectOptimisticLockingFailureException e) {

            log.error("Exception in  -> saveOrUpdate method ", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_LOCKED_RECORD);

        } catch (JpaObjectRetrievalFailureException e) {

            log.error("Exception in  -> saveOrUpdate method ", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_DELETED_RECORD);

        } catch (Exception exception) {
            log.error("Failed to save ..", exception);

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Exception Cause 1 ::: " + exceptionCause1);

            // General error condition
            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            if (exceptionCause1.contains("FK_")) {
                baseDto.setResponseCode(ErrorCode.SAVE_UPDATE_FK_RECORD);
            }

            if (exceptionCause1.contains(UniqueKey.UK_TRIGGER_TRIGGERCODE)) {
                baseDto.setResponseCode(ErrorCode.TRIGGER_CODE_ALREADY_EXIST);
            }

            if (exceptionCause1.contains(UniqueKey.UK_TRIGGER_TRIGGERNAME)) {
                baseDto.setResponseCode(ErrorCode.TRIGGER_NAME_ALREADY_EXIST);
            }

        }

    }

    public void loadPortGroup(BaseDto baseDto) {

        try {

            List<CountryMaster> countryList = countryRepo.findAll();

            List<PortGroupMaster> entityList = new ArrayList<>();

            for (CountryMaster cm : countryList) {

                PortGroupMaster po = new PortGroupMaster();
                po.setCountryMaster(cm);
                po.setCountryCode(cm.getCountryCode());
                po.setPortGroupCode(cm.getCountryCode());
                po.setPortGroupName(cm.getCountryName());
                po.setStatus(LovStatus.Active);

                entityList.add(po);

                if (entityList.size() == 50) {
                    portGroupRepo.save(entityList);
                    portGroupRepo.flush();
                    entityList = new ArrayList<>();
                }

            }
            if (entityList.size() != 0) {
                portGroupRepo.save(entityList);
            }

        } catch (RestException exception) {

            log.error("->Exception in Create method : ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (ObjectOptimisticLockingFailureException e) {

            log.error("Exception in  -> saveOrUpdate method ", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_LOCKED_RECORD);

        } catch (JpaObjectRetrievalFailureException e) {

            log.error("Exception in  -> saveOrUpdate method ", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_DELETED_RECORD);

        } catch (Exception exception) {
            log.error("Failed to save ..", exception);

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Exception Cause 1 ::: " + exceptionCause1);

            // General error condition
            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            if (exceptionCause1.contains("FK_")) {
                baseDto.setResponseCode(ErrorCode.SAVE_UPDATE_FK_RECORD);
            }

            // Duplicate Port Group code
            if (exceptionCause1.contains(UniqueKey.UK_PORTGROUP_PORTGROUPCODE)) {
                baseDto.setResponseCode(ErrorCode.PORT_GROUP_CODE_ALREADY_EXIST);
            }

            // Duplicate Port Group name
            if (exceptionCause1.contains(UniqueKey.UK_PORTGROUP_PORTGROUPNAME)) {
                baseDto.setResponseCode(ErrorCode.PORT_GROUP_NAME_ALREADY_EXIST);
            }
        }

    }

    public void loadPortMaster(BaseDto baseDto) {


        try (XSSFWorkbook workbook = new XSSFWorkbook(
                new FileInputStream(new File(appResourcePath + "/efs-master-data/port-master.xlsx")))) {
            XSSFSheet firstSheet = workbook.getSheetAt(0);

            Map<String, CountryMaster> tmpCountryMap = new HashMap<>();

            Map<String, PortGroupMaster> mapPortGroupMap = new HashMap<>();

            List<PortMaster> entityList = new ArrayList<>();

            for (int i = 1; i <= firstSheet.getLastRowNum(); i++) {

                String countryCode = firstSheet.getRow(i).getCell(2).getStringCellValue().trim().toUpperCase();

                if (tmpCountryMap.get(countryCode) == null) {
                    CountryMaster tmpObj = countryRepo.findByCountryCode(countryCode);
                    tmpCountryMap.put(countryCode, tmpObj);
                }

                if (mapPortGroupMap.get(countryCode) == null) {
                    PortGroupMaster tmpObj = portGroupRepo.findByCountry(countryCode);
                    mapPortGroupMap.put(countryCode, tmpObj);
                }

                PortMaster tmpObj = portRepo
                        .findByPortCode(firstSheet.getRow(i).getCell(0).getStringCellValue().trim().toUpperCase());

                if (tmpObj == null) {

                    PortMaster entity = new PortMaster();
                    entity.setStatus(LovStatus.Active);
                    entity.setPortName(firstCase(firstSheet.getRow(i).getCell(1).getStringCellValue().trim()));
                    entity.setPortCode(firstSheet.getRow(i).getCell(0).getStringCellValue().trim().toUpperCase());
                    entity.setPortGroupMaster(mapPortGroupMap.get(countryCode));
                    entity.setPortGroupCode(mapPortGroupMap.get(countryCode).getPortGroupCode());
                    entity.setCountryCode(countryCode);
                    if (firstSheet.getRow(i).getCell(3).getStringCellValue().trim().toUpperCase().equals("AIR")) {
                        entity.setTransportMode(TransportMode.Air);
                    } else if (firstSheet.getRow(i).getCell(3).getStringCellValue().trim().toUpperCase()
                            .equals("OCEAN")) {
                        entity.setTransportMode(TransportMode.Ocean);
                    } else if (firstSheet.getRow(i).getCell(3).getStringCellValue().trim().toUpperCase()
                            .equals("RAIL")) {
                        entity.setTransportMode(TransportMode.Rail);
                    } else if (firstSheet.getRow(i).getCell(3).getStringCellValue().trim().toUpperCase()
                            .equals("ROAD")) {
                        entity.setTransportMode(TransportMode.Road);
                    }

                    entityList.add(entity);

                }
                if (entityList.size() == 50) {
                    portRepo.save(entityList);
                    portRepo.flush();
                    entityList = new ArrayList<>();
                }

            }
            if (entityList.size() != 0) {
                portRepo.save(entityList);
            }
            workbook.close();


        } catch (RestException exception) {

            log.error("->Exception in Create method : ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (ObjectOptimisticLockingFailureException e) {

            log.error("Exception in  -> saveOrUpdate method ", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_LOCKED_RECORD);

        } catch (JpaObjectRetrievalFailureException e) {

            log.error("Exception in  -> saveOrUpdate method ", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_DELETED_RECORD);

        } catch (Exception exception) {
            log.error("Failed to save ..", exception);

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Exception Cause 1 ::: " + exceptionCause1);

            // General error condition
            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            if (exceptionCause1.contains("FK_")) {
                baseDto.setResponseCode(ErrorCode.SAVE_UPDATE_FK_RECORD);
            }

            if (exceptionCause1.contains(UniqueKey.UK_PORT_PORTCODE)) {
                baseDto.setResponseCode(ErrorCode.PORT_CODE_ALREADY_EXIST);
            }

            if (exceptionCause1.contains(UniqueKey.UK_PORT_PORTNAME)) {
                baseDto.setResponseCode(ErrorCode.PORT_NAME_ALREADY_EXIST);
            }

            if (exceptionCause1.contains(UniqueKey.UK_EDIPORT_EDITYPE)) {
                baseDto.setResponseCode(ErrorCode.EDI_TYPE_ALREDY_EXIST);
            }

        }

    }

    private void loadServiceType(XSSFSheet workSheet) {

        List<ServiceTypeMaster> entityList = new ArrayList<>();

        for (int i = 1; i < workSheet.getLastRowNum(); i++) {

            ServiceTypeMaster tmpObj = serviceTypeRepo
                    .findByServiceTypeCode(workSheet.getRow(i).getCell(0).getStringCellValue().trim().toUpperCase());

            if (tmpObj == null) {

                ServiceTypeMaster entity = new ServiceTypeMaster();
                entity.setStatus(LovStatus.Active);
                entity.setServiceTypeCode(workSheet.getRow(i).getCell(0).getStringCellValue().trim().toUpperCase());
                entity.setServiceTypeName(firstCase(workSheet.getRow(i).getCell(1).getStringCellValue().trim()));

                if (workSheet.getRow(i).getCell(2).getStringCellValue().trim().toUpperCase().equals("AIR")) {
                    entity.setTransportMode(TransportMode.Air);
                } else if (workSheet.getRow(i).getCell(2).getStringCellValue().trim().toUpperCase().equals("OCEAN")) {
                    entity.setTransportMode(TransportMode.Ocean);
                } else if (workSheet.getRow(i).getCell(2).getStringCellValue().trim().toUpperCase().equals("RAIL")) {
                    entity.setTransportMode(TransportMode.Rail);
                } else if (workSheet.getRow(i).getCell(2).getStringCellValue().trim().toUpperCase().equals("ROAD")) {
                    entity.setTransportMode(TransportMode.Road);
                }

                if (entity.getServiceTypeCode().equals("PROJECT")) {
                    entity.setServiceType(ServiceType.Project);
                } else if (entity.getServiceTypeCode().equals("CHA")) {
                    entity.setServiceType(ServiceType.Clearance);
                } else if (entity.getServiceTypeCode().equals("HAULAGE")) {
                    entity.setServiceType(ServiceType.Haulage);
                } else if (entity.getServiceTypeCode().equals("RORO")) {
                    entity.setServiceType(ServiceType.RoRo);
                } else if (entity.getServiceTypeCode().equals("TRANSPORTATION")) {
                    entity.setServiceType(ServiceType.Transportation);
                }

                entityList.add(entity);

            }
            if (entityList.size() == 50) {
                serviceTypeRepo.save(entityList);
                serviceTypeRepo.flush();
                entityList = new ArrayList<>();
            }
        }
        if (entityList.size() != 0) {
            serviceTypeRepo.save(entityList);
        }
    }

    private void loadServiceMaster(XSSFSheet workSheet) {
        Map<String, ServiceTypeMaster> map = new HashMap<>();

        List<ServiceMaster> entityList = new ArrayList<>();

        for (int i = 1; i < workSheet.getLastRowNum(); i++) {

            String code = workSheet.getRow(i).getCell(5).getStringCellValue().trim().toUpperCase();
            code = code.toUpperCase().equals("NA") ? null : code;
            if (code != null && map.get(code) == null) {
                ServiceTypeMaster tmpObj = serviceTypeRepo.findByServiceTypeCode(code);
                map.put(code, tmpObj);
            }

            ServiceMaster tmpObj = serviceRepo
                    .findByServiceCode(workSheet.getRow(i).getCell(0).getStringCellValue().trim().toUpperCase());

            if (tmpObj == null) {

                ServiceMaster entity = new ServiceMaster();
                entity.setStatus(LovStatus.Hide);
                entity.setServiceCode(workSheet.getRow(i).getCell(0).getStringCellValue().trim().toUpperCase());
                entity.setServiceName(firstCase(workSheet.getRow(i).getCell(1).getStringCellValue().trim()));

                if (workSheet.getRow(i).getCell(2).getStringCellValue().trim().toUpperCase().equals("AIR")) {
                    entity.setTransportMode(TransportMode.Air);
                } else if (workSheet.getRow(i).getCell(2).getStringCellValue().trim().toUpperCase().equals("OCEAN")) {
                    entity.setTransportMode(TransportMode.Ocean);
                } else if (workSheet.getRow(i).getCell(2).getStringCellValue().trim().toUpperCase().equals("RAIL")) {
                    entity.setTransportMode(TransportMode.Rail);
                } else if (workSheet.getRow(i).getCell(2).getStringCellValue().trim().toUpperCase().equals("ROAD")) {
                    entity.setTransportMode(TransportMode.Road);
                }

                if (workSheet.getRow(i).getCell(3).getStringCellValue().trim().toUpperCase().equals("FULL")) {
                    entity.setFullGroupage(FullGroupage.Full);
                } else if (workSheet.getRow(i).getCell(3).getStringCellValue().trim().toUpperCase()
                        .equals("GROUPAGE")) {
                    entity.setFullGroupage(FullGroupage.Groupage);
                } else if (workSheet.getRow(i).getCell(3).getStringCellValue().trim().toUpperCase().equals("NA")) {
                    entity.setFullGroupage(FullGroupage.NA);
                }

                if (workSheet.getRow(i).getCell(4).getStringCellValue().trim().toUpperCase().equals("EXPORT")) {
                    entity.setImportExport(ImportExport.Export);
                } else if (workSheet.getRow(i).getCell(4).getStringCellValue().trim().toUpperCase().equals("IMPORT")) {
                    entity.setImportExport(ImportExport.Import);
                }

                if (code == null) {
                    entity.setServiceType(null);
                } else if (code.equals("PROJECT")) {
                    entity.setServiceType(map.get(code));
                } else if (code.equals("CHA")) {
                    entity.setServiceType(map.get(code));
                } else if (code.equals("HAULAGE")) {
                    entity.setServiceType(map.get(code));
                } else if (code.equals("RORO")) {
                    entity.setServiceType(map.get(code));
                } else if (code.equals("TRANSPORTATION")) {
                    entity.setServiceType(map.get(code));
                }

                entityList.add(entity);

            }
            if (entityList.size() == 50) {
                serviceRepo.save(entityList);
                serviceRepo.flush();
                entityList = new ArrayList<>();
            }
        }
        if (entityList.size() != 0) {
            serviceRepo.save(entityList);
        }
    }

    private void loadImportToExportMapping(XSSFSheet workSheet) {

        Map<String, ServiceMaster> map = new HashMap<>();

        List<ServiceMapping> entityList = new ArrayList<>();

        for (int i = 1; i < workSheet.getLastRowNum(); i++) {

            if (map.get(workSheet.getRow(i).getCell(1).getStringCellValue().trim().toUpperCase()) == null) {
                ServiceMaster service = serviceRepo
                        .findByServiceCode(workSheet.getRow(i).getCell(1).getStringCellValue().trim().toUpperCase());
                map.put(service.getServiceCode(), service);

            }

            if (map.get(workSheet.getRow(i).getCell(2).getStringCellValue().trim().toUpperCase()) == null) {
                ServiceMaster service = serviceRepo
                        .findByServiceCode(workSheet.getRow(i).getCell(2).getStringCellValue().trim().toUpperCase());
                map.put(service.getServiceCode(), service);

            }

            ServiceMaster importService = map
                    .get(workSheet.getRow(i).getCell(1).getStringCellValue().trim().toUpperCase());
            ServiceMaster exportService = map
                    .get(workSheet.getRow(i).getCell(2).getStringCellValue().trim().toUpperCase());

            if (importService == null || exportService == null) {
                continue;
            }

            ServiceMapping mapping = serviceMappingRepo.findByImportAndExport(importService.getId(),
                    exportService.getId());

            if (mapping == null) {

                ServiceMapping entity = new ServiceMapping();
                entity.setExportCode(exportService.getServiceCode());
                entity.setExportService(exportService);
                entity.setImportCode(importService.getServiceCode());
                entity.setImportService(importService);

                entityList.add(entity);

            }
            if (entityList.size() == 50) {
                serviceMappingRepo.save(entityList);
                serviceMappingRepo.flush();
                entityList = new ArrayList<>();
            }
        }
        if (entityList.size() != 0) {
            serviceMappingRepo.save(entityList);
        }
    }

    private void loadEnquiryServiceMapping(XSSFSheet workSheet) {

        List<EnquiryServiceMapping> entityList = new ArrayList<>();

        for (int i = 1; i < workSheet.getLastRowNum(); i++) {

            com.efreightsuite.enumeration.Service transport = null;
            ImportExport trade = null;

            if (workSheet.getRow(i).getCell(1).getStringCellValue().trim().toUpperCase().equals("AIR")) {
                transport = com.efreightsuite.enumeration.Service.Air;
            } else if (workSheet.getRow(i).getCell(1).getStringCellValue().trim().toUpperCase().equals("LCL")) {
                transport = com.efreightsuite.enumeration.Service.LCL;
            } else if (workSheet.getRow(i).getCell(1).getStringCellValue().trim().toUpperCase().equals("FCL")) {
                transport = com.efreightsuite.enumeration.Service.FCL;
            }

            if (workSheet.getRow(i).getCell(2).getStringCellValue().trim().toUpperCase().equals("EXPORT")) {
                trade = ImportExport.Export;
            } else if (workSheet.getRow(i).getCell(2).getStringCellValue().trim().toUpperCase().equals("IMPORT")) {
                trade = ImportExport.Import;
            }

            ServiceMaster serviceObj = serviceRepo
                    .findByServiceCode(workSheet.getRow(i).getCell(3).getStringCellValue().trim().toUpperCase());

            if (transport == null || trade == null || serviceObj == null) {
                continue;
            }

            EnquiryServiceMapping tmpObj = enquiryServiceMappingRepo.findByServiceAndTransportMode(transport, trade);

            if (tmpObj == null) {

                EnquiryServiceMapping entity = new EnquiryServiceMapping();

                entity.setImportExport(trade);
                entity.setService(transport);
                entity.setServiceMaster(serviceObj);

                entityList.add(entity);

            }
            if (entityList.size() == 50) {
                enquiryServiceMappingRepo.save(entityList);
                enquiryServiceMappingRepo.flush();
                entityList = new ArrayList<>();
            }
        }
        if (entityList.size() != 0) {
            enquiryServiceMappingRepo.save(entityList);
        }
    }

    public void loadService(BaseDto baseDto) {


        try (XSSFWorkbook workbook = new XSSFWorkbook(
                new FileInputStream(new File(appResourcePath + "/efs-master-data/service-master.xlsx")))) {

            // load service type data
            loadServiceType(workbook.getSheetAt(0));

            // load service type data
            loadServiceMaster(workbook.getSheetAt(1));

            // load ImportToExport Mapping
            loadImportToExportMapping(workbook.getSheetAt(2));

            // Load loadEnquiryServiceMapping
            loadEnquiryServiceMapping(workbook.getSheetAt(3));

            workbook.close();


        } catch (RestException exception) {

            log.error("->Exception in Create method : ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (ObjectOptimisticLockingFailureException e) {

            log.error("Exception in  -> saveOrUpdate method ", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_LOCKED_RECORD);

        } catch (JpaObjectRetrievalFailureException e) {

            log.error("Exception in  -> saveOrUpdate method ", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_DELETED_RECORD);

        } catch (Exception exception) {
            log.error("Failed to save ..", exception);

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Exception Cause 1 ::: " + exceptionCause1);

            // General error condition
            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            if (exceptionCause1.contains("FK_")) {
                baseDto.setResponseCode(ErrorCode.SAVE_UPDATE_FK_RECORD);
            }
            if (exceptionCause1.contains(UniqueKey.UK_SERVICETYPE_SERVICECODE)) {
                baseDto.setResponseCode(ErrorCode.SERVICE_TYPE_CODE_ALREADY_EXIST);
            }

            if (exceptionCause1.contains(UniqueKey.UK_SERVICETYPE_SERVICENAME)) {
                baseDto.setResponseCode(ErrorCode.SERVICE_TYPE_NAME_ALREADY_EXIST);
            }

            if (exceptionCause1.contains(UniqueKey.UK_SERVICE_SERVICECODE)) {
                baseDto.setResponseCode(ErrorCode.SERVICE_CODE_ALREADY_EXIST);
            }
            if (exceptionCause1.contains(UniqueKey.UK_SERVICE_SERVICENAME)) {
                baseDto.setResponseCode(ErrorCode.SERVICE_NAME_ALREADY_EXIST);
            }

        }

    }

    public void loadCurrency(BaseDto baseDto) {


        try (XSSFWorkbook workbook = new XSSFWorkbook(
                new FileInputStream(new File(appResourcePath + "/efs-master-data/currency-master.xlsx")))) {
            XSSFSheet firstSheet = workbook.getSheetAt(0);

            Map<String, CountryMaster> map = new HashMap<>();

            for (int i = 1; i <= firstSheet.getLastRowNum(); i++) {

                String code = firstSheet.getRow(i).getCell(2).getStringCellValue().trim().toUpperCase();
                if (map.get(code) == null) {
                    CountryMaster tmpObj = countryRepo.findByCountryCode(code);
                    map.put(code, tmpObj);
                }

                CurrencyMaster tmpObj = currencyRepo
                        .findByCurrencyCode(firstSheet.getRow(i).getCell(0).getStringCellValue().trim().toUpperCase());

                if (tmpObj == null) {

                    CurrencyMaster entity = new CurrencyMaster();
                    entity.setStatus(LovStatus.Active);
                    entity.setCurrencyCode(firstSheet.getRow(i).getCell(0).getStringCellValue().trim().toUpperCase());
                    entity.setCurrencyName(firstCase(firstSheet.getRow(i).getCell(1).getStringCellValue().trim()));
                    entity.setCountryMaster(map.get(code));
                    entity.setCountryCode(map.get(code).getCountryCode());

                    firstSheet.getRow(i).getCell(3).setCellType(Cell.CELL_TYPE_STRING);
                    entity.setDecimalPoint(firstSheet.getRow(i).getCell(3).getStringCellValue());

                    String prefix = firstSheet.getRow(i).getCell(5).getStringCellValue();
                    if (prefix != null && prefix.trim().length() != 0) {
                        entity.setPrefix(firstCase(prefix.trim()));
                    }

                    String sufix = firstSheet.getRow(i).getCell(6).getStringCellValue();
                    if (sufix != null && sufix.trim().length() != 0) {
                        entity.setSuffix(firstCase(sufix.trim()));
                    }

                    entity = currencyRepo.save(entity);

                    CountryMaster tmpCountryObj = countryRepo.findByCountryCode(entity.getCountryMaster().getCountryCode());
                    tmpCountryObj.setCurrencyMaster(entity);
                    tmpCountryObj.setCurrencyCode(entity.getCurrencyCode());

                    countryRepo.save(tmpCountryObj);

                    if (i % 50 == 0) {
                        currencyRepo.flush();
                    }

                }

            }

            workbook.close();


        } catch (RestException exception) {

            log.error("->Exception in Create method : ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (ObjectOptimisticLockingFailureException e) {

            log.error("Exception in  -> saveOrUpdate method ", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_LOCKED_RECORD);

        } catch (JpaObjectRetrievalFailureException e) {

            log.error("Exception in  -> saveOrUpdate method ", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_DELETED_RECORD);

        } catch (Exception exception) {
            log.error("Failed to save ..", exception);

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Exception Cause 1 ::: " + exceptionCause1);

            // General error condition
            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            if (exceptionCause1.contains("FK_")) {
                baseDto.setResponseCode(ErrorCode.SAVE_UPDATE_FK_RECORD);
            }

            if (exceptionCause1.contains(UniqueKey.UK_CURRENCY_CURRENCYCODE)) {
                baseDto.setResponseCode(ErrorCode.CURRENCY_CODE_ALREADY_EXIST);
            }

            if (exceptionCause1.contains(UniqueKey.UK_CURRENCY_CURRENCYNAME)) {
                baseDto.setResponseCode(ErrorCode.CURRENCY_NAME_ALREADY_EXIST);
            }

        }

    }

    public void loadErrorMessage(BaseDto baseDto) {


        try (XSSFWorkbook workbook = new XSSFWorkbook(
                new FileInputStream(new File(appResourcePath + "/efs-master-data/error-message.xlsx")))) {

            XSSFSheet firstSheet = workbook.getSheetAt(0);

            Language language = languageRepo.findByLanguageNameAndStatus("English", YesNo.Yes);

            if (language == null) {
                language = new Language();
                language.setLanguageName("English");
                language.setStatus(YesNo.Yes);
                language = languageRepo.save(language);
            }

            List<LanguageDetail> entityList = new ArrayList<>();

            for (int i = 1; i <= firstSheet.getLastRowNum(); i++) {

                LanguageDetail ld = languageDetailRepository
                        .findByLanguageCode(firstSheet.getRow(i).getCell(0).getStringCellValue().trim().toUpperCase());

                if (ld == null) {
                    ld = new LanguageDetail();
                    ld.setLanguage(language);
                    ld.setLanguageCode(firstSheet.getRow(i).getCell(0).getStringCellValue().trim().toUpperCase());
                    ld.setLanguageDesc(firstSheet.getRow(i).getCell(1).getStringCellValue().trim());
                    ld.setGroupName(firstSheet.getRow(i).getCell(2).getStringCellValue().trim());

                    if (firstSheet.getRow(i).getCell(2).getStringCellValue().trim().toUpperCase().equals("Yes")) {
                        ld.setFlagged(YesNo.Yes);
                    } else {
                        ld.setFlagged(YesNo.No);
                    }

                    entityList.add(ld);

                }

                if (entityList.size() == 50) {
                    languageDetailRepository.save(entityList);
                    languageDetailRepository.flush();
                    entityList = new ArrayList<>();
                }

            }

            if (entityList.size() != 0) {
                languageDetailRepository.save(entityList);
            }

            workbook.close();


        } catch (RestException exception) {

            log.error("->Exception in Create method : ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (ObjectOptimisticLockingFailureException e) {

            log.error("Exception in  -> saveOrUpdate method ", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_LOCKED_RECORD);

        } catch (JpaObjectRetrievalFailureException e) {

            log.error("Exception in  -> saveOrUpdate method ", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_DELETED_RECORD);

        } catch (Exception exception) {
            log.error("Failed to save ..", exception);

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Exception Cause 1 ::: " + exceptionCause1);

            // General error condition
            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            if (exceptionCause1.contains("FK_")) {
                baseDto.setResponseCode(ErrorCode.SAVE_UPDATE_FK_RECORD);
            }

        }

    }

    public void loadRegularExpression(BaseDto baseDto) {


        try (XSSFWorkbook workbook = new XSSFWorkbook(
                new FileInputStream(new File(appResourcePath + "/efs-master-data/regular-expression.xlsx")))) {

            XSSFSheet firstSheet = workbook.getSheetAt(0);

            List<RegularExpression> entityList = new ArrayList<>();

            for (int i = 1; i <= firstSheet.getLastRowNum(); i++) {

                RegularExpression tmpObj = regularExpressionRepo
                        .findByRegExpName(firstSheet.getRow(i).getCell(0).getStringCellValue().trim());

                if (tmpObj == null) {
                    RegularExpression entity = new RegularExpression();

                    entity.setRegExpName(firstSheet.getRow(i).getCell(0).getStringCellValue().trim());
                    entity.setJavaRegExp(firstSheet.getRow(i).getCell(1).getStringCellValue().trim());
                    entity.setJsRegExp(firstSheet.getRow(i).getCell(2).getStringCellValue().trim());

                    entityList.add(entity);
                }

                if (entityList.size() == 50) {
                    regularExpressionRepo.save(entityList);
                    regularExpressionRepo.flush();
                    entityList = new ArrayList<>();
                }

            }

            if (entityList.size() != 0) {
                regularExpressionRepo.save(entityList);
            }

            workbook.close();


        } catch (RestException exception) {

            log.error("->Exception in Create method : ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (ObjectOptimisticLockingFailureException e) {

            log.error("Exception in  -> saveOrUpdate method ", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_LOCKED_RECORD);

        } catch (JpaObjectRetrievalFailureException e) {

            log.error("Exception in  -> saveOrUpdate method ", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_DELETED_RECORD);

        } catch (Exception exception) {
            log.error("Failed to save ..", exception);

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Exception Cause 1 ::: " + exceptionCause1);

            // General error condition
            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            if (exceptionCause1.contains("FK_")) {
                baseDto.setResponseCode(ErrorCode.SAVE_UPDATE_FK_RECORD);
            }

        }

    }

    public void loadFeature(BaseDto baseDto) {
        int i;

        try (XSSFWorkbook workbook = new XSSFWorkbook(
                new FileInputStream(new File(appResourcePath + "/efs-master-data/feature.xlsx")))) {

            XSSFSheet firstSheet = workbook.getSheetAt(0);

            Map<Long, Feature> mapObj = new HashMap<>();

            for (i = 1; i <= firstSheet.getLastRowNum(); i++) {

                firstSheet.getRow(i).getCell(0).setCellType(Cell.CELL_TYPE_STRING);

                Feature tmpObj = featureRepo
                        .findById(Long.parseLong(firstSheet.getRow(i).getCell(0).getStringCellValue()));

                if (tmpObj != null && mapObj.get(tmpObj.getFeatureValue()) == null) {
                    mapObj.put(tmpObj.getId(), tmpObj);
                }

                if (tmpObj == null) {
                    Feature entity = new Feature();

                    firstSheet.getRow(i).getCell(0).setCellType(Cell.CELL_TYPE_STRING);
                    entity.setId(Long.parseLong(firstSheet.getRow(i).getCell(0).getStringCellValue()));
                    entity.setFeatureName(firstCase(firstSheet.getRow(i).getCell(1).getStringCellValue().trim()));
                    entity.setFeatureValue(firstSheet.getRow(i).getCell(2).getStringCellValue().trim());

                    firstSheet.getRow(i).getCell(3).setCellType(Cell.CELL_TYPE_STRING);

                    if (firstSheet.getRow(i).getCell(3).getStringCellValue() != null
                            && firstSheet.getRow(i).getCell(3).getStringCellValue().trim().length() != 0) {
                        entity.setParent(
                                new Feature(Long.parseLong(firstSheet.getRow(i).getCell(3).getStringCellValue())));
                    } else {
                        entity.setParent(null);
                    }

                    if (firstSheet.getRow(i).getCell(4).getStringCellValue().trim().toUpperCase().equals("YES")) {
                        entity.setIsApplicable(YesNo.Yes);
                    } else {
                        entity.setIsApplicable(YesNo.No);
                    }

                    if (firstSheet.getRow(i).getCell(5).getStringCellValue().trim().toUpperCase().equals("YES")) {
                        entity.setList(YesNo.Yes);
                    } else {
                        entity.setList(YesNo.No);
                    }

                    if (firstSheet.getRow(i).getCell(6).getStringCellValue().trim().toUpperCase().equals("YES")) {
                        entity.setCreate(YesNo.Yes);
                    } else {
                        entity.setCreate(YesNo.No);
                    }

                    if (firstSheet.getRow(i).getCell(7).getStringCellValue().trim().toUpperCase().equals("YES")) {
                        entity.setModify(YesNo.Yes);
                    } else {
                        entity.setModify(YesNo.No);
                    }

                    if (firstSheet.getRow(i).getCell(8).getStringCellValue().trim().toUpperCase().equals("YES")) {
                        entity.setView(YesNo.Yes);
                    } else {
                        entity.setView(YesNo.No);
                    }

                    if (firstSheet.getRow(i).getCell(9).getStringCellValue().trim().toUpperCase().equals("YES")) {
                        entity.setDelete(YesNo.Yes);
                    } else {
                        entity.setDelete(YesNo.No);
                    }

                    if (firstSheet.getRow(i).getCell(10).getStringCellValue().trim().toUpperCase().equals("YES")) {
                        entity.setDownload(YesNo.Yes);
                    } else {
                        entity.setDownload(YesNo.No);
                    }

                    featureRepo.save(entity);
                }

                    /*
                     * if(i%50==0){ featureRepo.flush(); }
                     */
            }

            log.info("Number of features inserted......... : " + i);
            workbook.close();


        } catch (RestException exception) {

            log.error("->Exception in Create method : ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (ObjectOptimisticLockingFailureException e) {

            log.error("Exception in  -> saveOrUpdate method ", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_LOCKED_RECORD);

        } catch (JpaObjectRetrievalFailureException e) {

            log.error("Exception in  -> saveOrUpdate method ", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_DELETED_RECORD);

        } catch (Exception exception) {
            log.error("Failed to save ..", exception);

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Exception Cause 1 ::: " + exceptionCause1);

            // General error condition
            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            if (exceptionCause1.contains("FK_")) {
                baseDto.setResponseCode(ErrorCode.SAVE_UPDATE_FK_RECORD);
            }

        }

    }

    public void loadDefaultMaster(BaseDto baseDto, LocationMaster location) {


        try (XSSFWorkbook workbook = new XSSFWorkbook(
                new FileInputStream(new File(appResourcePath + "/efs-master-data/location-config.xlsx")))) {

            XSSFSheet firstSheet = workbook.getSheetAt(0);

            for (int i = 1; i <= firstSheet.getLastRowNum(); i++) {

                DefaultMasterData tmpObj = defaultMasterDataRepo.getCodeAndLocation(
                        firstSheet.getRow(i).getCell(1).getStringCellValue().trim(), location.getId());

                if (tmpObj == null) {
                    DefaultMasterData entity = new DefaultMasterData();

                    entity.setDefaultName(firstSheet.getRow(i).getCell(0).getStringCellValue().trim());
                    entity.setCode(firstSheet.getRow(i).getCell(1).getStringCellValue().trim());
                    firstSheet.getRow(i).getCell(2).setCellType(Cell.CELL_TYPE_STRING);
                    entity.setValue(firstSheet.getRow(i).getCell(2).getStringCellValue().trim());
                    entity.setDescription(firstSheet.getRow(i).getCell(3).getStringCellValue());
                    entity.setLocation(location);
                    entity.setLocationCode(location.getLocationCode());

                    defaultMasterDataRepo.save(entity);
                }

                if (i % 50 == 0) {
                    defaultMasterDataRepo.flush();
                }

            }

            workbook.close();


        } catch (RestException exception) {

            log.error("->Exception in Create method : ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (ObjectOptimisticLockingFailureException e) {

            log.error("Exception in  -> saveOrUpdate method ", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_LOCKED_RECORD);

        } catch (JpaObjectRetrievalFailureException e) {

            log.error("Exception in  -> saveOrUpdate method ", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_DELETED_RECORD);

        } catch (Exception exception) {
            log.error("Failed to save ..", exception);

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Exception Cause 1 ::: " + exceptionCause1);

            // General error condition
            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            if (exceptionCause1.contains("FK_")) {
                baseDto.setResponseCode(ErrorCode.SAVE_UPDATE_FK_RECORD);
            }

        }

    }

    public void loadEmailTemplate(BaseDto baseDto) {


        try (XSSFWorkbook workbook = new XSSFWorkbook(
                new FileInputStream(new File(appResourcePath + "/efs-master-data/email-template.xlsx")))) {
            XSSFSheet firstSheet = workbook.getSheetAt(0);

            List<EmailTemplate> entityList = new ArrayList<>();

            for (int i = 1; i <= firstSheet.getLastRowNum(); i++) {

                EmailTemplate emailTemplateObj = emailTemplateRepository.findByTemplateType(TemplateType
                        .valueOf(firstSheet.getRow(i).getCell(0).getStringCellValue().trim().toUpperCase()));

                if (emailTemplateObj == null) {

                    EmailTemplate emailTemplate = new EmailTemplate();
                    emailTemplate.setTemplateType(TemplateType
                            .valueOf(firstSheet.getRow(i).getCell(0).getStringCellValue().trim().toUpperCase()));
                    emailTemplate.setEmailType(firstCase(firstSheet.getRow(i).getCell(1).getStringCellValue().trim()));
                    emailTemplate.setFromEmailId(firstSheet.getRow(i).getCell(2).getStringCellValue().trim());
                    emailTemplate.setFromEmailPassword(firstSheet.getRow(i).getCell(3).getStringCellValue().trim());
                    emailTemplate.setPort((int) firstSheet.getRow(i).getCell(4).getNumericCellValue());
                    emailTemplate.setSmtpServerAddress(firstSheet.getRow(i).getCell(5).getStringCellValue().trim());
                    emailTemplate.setSubject(firstSheet.getRow(i).getCell(6).getStringCellValue().trim());
                    emailTemplate.setTemplate(firstSheet.getRow(i).getCell(7).getStringCellValue().trim());
                    entityList.add(emailTemplate);
                }
            }
            if (!entityList.isEmpty()) {
                emailTemplateRepository.save(entityList);
            }
            workbook.close();


        } catch (RestException exception) {

            log.error("->Exception in Create method : ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (ObjectOptimisticLockingFailureException e) {

            log.error("Exception in  -> saveOrUpdate method ", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_LOCKED_RECORD);

        } catch (JpaObjectRetrievalFailureException e) {

            log.error("Exception in  -> saveOrUpdate method ", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_DELETED_RECORD);

        } catch (Exception exception) {
            log.error("Failed to save ..", exception);

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Exception Cause 1 ::: " + exceptionCause1);

            // General error condition
            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            if (exceptionCause1.contains("FK_")) {
                baseDto.setResponseCode(ErrorCode.SAVE_UPDATE_FK_RECORD);
            }
        }
    }


    public void loadTos(BaseDto baseDto) {


        try (XSSFWorkbook workbook = new XSSFWorkbook(
                new FileInputStream(new File(appResourcePath + "/efs-master-data/tos-master.xlsx")))) {
            XSSFSheet firstSheet = workbook.getSheetAt(0);

            List<TosMaster> entityList = new ArrayList<>();

            for (int i = 1; i <= firstSheet.getLastRowNum(); i++) {

                TosMaster tmpObj = tosMasterRepository
                        .findByTosCode(firstSheet.getRow(i).getCell(0).getStringCellValue().trim().toUpperCase());

                if (tmpObj == null) {

                    TosMaster entity = new TosMaster();
                    entity.setStatus(LovStatus.Active);
                    entity.setTosCode(firstSheet.getRow(i).getCell(0).getStringCellValue().trim().toUpperCase());
                    entity.setTosName(firstCase(firstSheet.getRow(i).getCell(1).getStringCellValue().trim()));
                    if (firstCase(firstSheet.getRow(i).getCell(2).getStringCellValue().trim())
                            .equalsIgnoreCase("Prepaid")) {
                        entity.setFreightPPCC(PPCC.Prepaid);
                    } else if (firstCase(firstSheet.getRow(i).getCell(2).getStringCellValue().trim())
                            .equalsIgnoreCase("Collect")) {
                        entity.setFreightPPCC(PPCC.Collect);
                    }
                    if (firstSheet.getRow(i).getCell(3) != null && firstSheet.getRow(i).getCell(3).getStringCellValue() != null) {
                        entity.setDescription(firstSheet.getRow(i).getCell(3).getStringCellValue().trim());
                    }

                    entityList.add(entity);
                    if (i % 50 == 0) {
                        tosMasterRepository.flush();
                    }

                }
                if (entityList.size() == 50) {
                    tosMasterRepository.save(entityList);
                    tosMasterRepository.flush();
                    entityList = new ArrayList<>();
                }

            }

            if (entityList.size() != 0) {
                tosMasterRepository.save(entityList);
            }
            workbook.close();


        } catch (RestException exception) {

            log.error("->Exception in Create method : ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (ObjectOptimisticLockingFailureException e) {

            log.error("Exception in  -> saveOrUpdate method ", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_LOCKED_RECORD);

        } catch (JpaObjectRetrievalFailureException e) {

            log.error("Exception in  -> saveOrUpdate method ", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_DELETED_RECORD);

        } catch (Exception exception) {
            log.error("Failed to save ..", exception);

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Exception Cause 1 ::: " + exceptionCause1);

            // General error condition
            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            if (exceptionCause1.contains("FK_")) {
                baseDto.setResponseCode(ErrorCode.SAVE_UPDATE_FK_RECORD);
            }

            if (exceptionCause1.contains(UniqueKey.UK_TOSMASTER_TOSCODE)) {
                baseDto.setResponseCode(ErrorCode.TOS_CODE_ALREADY_EXIST);
            }

            if (exceptionCause1.contains(UniqueKey.UK_TOSMASTER_TOSNAME)) {
                baseDto.setResponseCode(ErrorCode.TOS_NAME_ALREADY_EXIST);
            }
        }
    }

    public void loadEdiConfiguration(BaseDto baseDto, LocationMaster location) {


        try (XSSFWorkbook workbook = new XSSFWorkbook(
                new FileInputStream(new File(appResourcePath + "/efs-master-data/edi_configuration.xlsx")))) {
            XSSFSheet firstSheet = workbook.getSheetAt(0);

            List<EdiConfigurationMaster> entityList = new ArrayList<>();

            for (int i = 1; i < firstSheet.getLastRowNum(); i++) {

                EdiConfigurationMaster tmpObj = ediConfigurationMasterRepository.findByKey(firstSheet.getRow(i).getCell(0).getStringCellValue().trim().toUpperCase(), location.getId());

                if (tmpObj == null) {
                    EdiConfigurationMaster entity = new EdiConfigurationMaster();

                    entity.setEdiConfigurationKey(firstSheet.getRow(i).getCell(0).getStringCellValue().trim().toUpperCase());
                    firstSheet.getRow(i).getCell(1).setCellType(Cell.CELL_TYPE_STRING);
                    entity.setEdiConfigurationValue(firstSheet.getRow(i).getCell(1).getStringCellValue().trim());
                    if (firstSheet.getRow(i).getCell(2) != null && firstSheet.getRow(i).getCell(2).getStringCellValue() != null && firstSheet.getRow(i).getCell(2).getStringCellValue() != "") {
                        entity.setDescription(firstSheet.getRow(i).getCell(2).getStringCellValue());
                    } else {
                        entity.setDescription(null);
                    }
                    entity.setLocationMaster(location);

                    entityList.add(entity);

                    if (i % 50 == 0) {
                        ediConfigurationMasterRepository.flush();
                    }
                }
                if (entityList.size() == 50) {
                    ediConfigurationMasterRepository.save(entityList);
                    ediConfigurationMasterRepository.flush();
                    entityList = new
                            ArrayList<>();
                }
            }

            if (entityList.size() != 0) {
                ediConfigurationMasterRepository.save(entityList);
            }
            workbook.close();


        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void loadUnit(BaseDto baseDto) {


        try (XSSFWorkbook workbook = new XSSFWorkbook(
                new FileInputStream(new File(appResourcePath + "/efs-master-data/unit-master.xlsx")))) {
            XSSFSheet firstSheet = workbook.getSheetAt(0);

            List<UnitMaster> entityList = new ArrayList<>();

            for (int i = 1; i <= firstSheet.getLastRowNum(); i++) {

                UnitMaster tmpObj = unitMasterRepository
                        .findByUnitCode(firstSheet.getRow(i).getCell(0).getStringCellValue().trim().toUpperCase());

                if (tmpObj == null) {

                    UnitMaster entity = new UnitMaster();
                    entity.setStatus(LovStatus.Active);
                    entity.setUnitCode(firstSheet.getRow(i).getCell(0).getStringCellValue().trim().toUpperCase());
                    entity.setUnitName(firstCase(firstSheet.getRow(i).getCell(1).getStringCellValue().trim()));
                    if (firstSheet.getRow(i).getCell(2).getStringCellValue() != null) {
                        if (firstCase(firstSheet.getRow(i).getCell(2).getStringCellValue().trim())
                                .equalsIgnoreCase("Container")) {
                            entity.setUnitType(UnitType.Container);
                        } else if (firstCase(firstSheet.getRow(i).getCell(2).getStringCellValue().trim())
                                .equalsIgnoreCase("Shipment")) {
                            entity.setUnitType(UnitType.Shipment);
                        } else if (firstCase(firstSheet.getRow(i).getCell(2).getStringCellValue().trim())
                                .equalsIgnoreCase("Unit")) {
                            entity.setUnitType(UnitType.Unit);
                        }
                    }


                    if (firstSheet.getRow(i).getCell(3) != null && firstSheet.getRow(i).getCell(3).getStringCellValue() != null) {
                        if (firstCase(firstSheet.getRow(i).getCell(3).getStringCellValue().trim()).equalsIgnoreCase("1")) {
                            entity.setDecimals(UnitDecimals.ONE);
                        } else if (firstCase(firstSheet.getRow(i).getCell(3).getStringCellValue().trim())
                                .equalsIgnoreCase("Shipment")) {
                            entity.setDecimals(UnitDecimals.TWO);
                        } else if (firstCase(firstSheet.getRow(i).getCell(3).getStringCellValue().trim())
                                .equalsIgnoreCase("Unit")) {
                            entity.setDecimals(UnitDecimals.THREE);
                        } else if (firstCase(firstSheet.getRow(i).getCell(3).getStringCellValue().trim())
                                .equalsIgnoreCase("Na")) {
                            entity.setDecimals(UnitDecimals.NA);
                        }
                    }


                    if (firstSheet.getRow(i).getCell(4) != null && firstSheet.getRow(i).getCell(4).getStringCellValue() != null && firstSheet.getRow(i).getCell(4).getStringCellValue().trim().length() != 0) {
                        if (firstCase(firstSheet.getRow(i).getCell(4).getStringCellValue().trim())
                                .equalsIgnoreCase("GROSS_WEIGHT")) {
                            entity.setMappingUnit1(MappingUnit.GROSS_WEIGHT);
                        } else if (firstCase(firstSheet.getRow(i).getCell(4).getStringCellValue().trim())
                                .equalsIgnoreCase("PIECES")) {
                            entity.setMappingUnit1(MappingUnit.PIECES);
                        } else if (firstCase(firstSheet.getRow(i).getCell(4).getStringCellValue().trim())
                                .equalsIgnoreCase("VOLUME")) {
                            entity.setMappingUnit1(MappingUnit.VOLUME);
                        } else if (firstCase(firstSheet.getRow(i).getCell(4).getStringCellValue().trim())
                                .equalsIgnoreCase("VOLUME_WEIGHT")) {
                            entity.setMappingUnit1(MappingUnit.VOLUME_WEIGHT);
                        } else if (firstCase(firstSheet.getRow(i).getCell(4).getStringCellValue().trim())
                                .equalsIgnoreCase("DAY")) {
                            entity.setMappingUnit1(MappingUnit.DAY);
                        } else if (firstCase(firstSheet.getRow(i).getCell(4).getStringCellValue().trim())
                                .equalsIgnoreCase("DOC")) {
                            entity.setMappingUnit1(MappingUnit.DOC);
                        } else if (firstCase(firstSheet.getRow(i).getCell(4).getStringCellValue().trim())
                                .equalsIgnoreCase("TEU")) {
                            entity.setMappingUnit1(MappingUnit.TEU);
                        } else if (firstCase(firstSheet.getRow(i).getCell(4).getStringCellValue().trim())
                                .equalsIgnoreCase("FEU")) {
                            entity.setMappingUnit1(MappingUnit.FEU);
                        } else if (firstCase(firstSheet.getRow(i).getCell(4).getStringCellValue().trim())
                                .equalsIgnoreCase("CHARGEABLE_UNIT")) {
                            entity.setMappingUnit1(MappingUnit.CHARGEABLE_UNIT);
                        } else if (firstCase(firstSheet.getRow(i).getCell(4).getStringCellValue().trim())
                                .equalsIgnoreCase("POUND")) {
                            entity.setMappingUnit1(MappingUnit.POUND);
                        } else if (firstCase(firstSheet.getRow(i).getCell(4).getStringCellValue().trim())
                                .equalsIgnoreCase("CFT")) {
                            entity.setMappingUnit1(MappingUnit.CFT);
                        }
                    }


                    if (firstSheet.getRow(i).getCell(5) != null && firstSheet.getRow(i).getCell(5).getStringCellValue() != null && firstSheet.getRow(i).getCell(5).getStringCellValue().trim().length() != 0) {
                        if (firstCase(firstSheet.getRow(i).getCell(5).getStringCellValue().trim())
                                .equalsIgnoreCase("ADDITION")) {
                            entity.setCalcType1(UnitCalculationType.ADDITION);
                        } else if (firstCase(firstSheet.getRow(i).getCell(5).getStringCellValue().trim())
                                .equalsIgnoreCase("MULTIPLICATION")) {
                            entity.setCalcType1(UnitCalculationType.MULTIPLICATION);
                        } else if (firstCase(firstSheet.getRow(i).getCell(5).getStringCellValue().trim())
                                .equalsIgnoreCase("SUBTRACTION")) {
                            entity.setCalcType1(UnitCalculationType.SUBTRACTION);
                        } else if (firstCase(firstSheet.getRow(i).getCell(5).getStringCellValue().trim())
                                .equalsIgnoreCase("DIVISION")) {
                            entity.setCalcType1(UnitCalculationType.DIVISION);
                        }
                    }

                    if (firstSheet.getRow(i).getCell(6) != null) {
                        firstSheet.getRow(i).getCell(6).setCellType(Cell.CELL_TYPE_STRING);
                        if (firstSheet.getRow(i).getCell(6).getStringCellValue() != null) {
                            firstSheet.getRow(i).getCell(6).setCellType(Cell.CELL_TYPE_STRING);
                            if (firstSheet.getRow(i).getCell(6).getStringCellValue().trim().length() != 0) {
                                entity.setCalcValue1(new Long(firstSheet.getRow(i).getCell(6).getStringCellValue().trim()));
                            }
                        }
                    }

                    entityList.add(entity);
                    if (i % 50 == 0) {
                        unitMasterRepository.flush();
                    }

                }
                if (entityList.size() == 50) {
                    unitMasterRepository.save(entityList);
                    unitMasterRepository.flush();
                    entityList = new ArrayList<>();
                }

            }

            if (entityList.size() != 0) {
                unitMasterRepository.save(entityList);
            }
            workbook.close();


        } catch (RestException exception) {

            log.error("->Exception in Create method : ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (ObjectOptimisticLockingFailureException e) {

            log.error("Exception in  -> saveOrUpdate method ", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_LOCKED_RECORD);

        } catch (JpaObjectRetrievalFailureException e) {

            log.error("Exception in  -> saveOrUpdate method ", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_DELETED_RECORD);

        } catch (Exception exception) {

            log.error("Exception in Create method : ", exception);

            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Exception Cause 1 ::: " + exceptionCause1);

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            if (exceptionCause1.contains(UniqueKey.UK_UNIT_UNITCODE)) {
                baseDto.setResponseCode(ErrorCode.UNIT_CODE_ALREADY_EXIST);
            }

            if (exceptionCause1.contains(UniqueKey.UK_UNIT_UNITNAME)) {
                baseDto.setResponseCode(ErrorCode.UNIT_NAME_ALREADY_EXIST);
            }

        }
    }

    public void loadSequence(BaseDto baseDto, CompanyMaster companyMaster) {


        try (XSSFWorkbook workbook = new XSSFWorkbook(
                new FileInputStream(new File(appResourcePath + "/efs-master-data/sequence.xlsx")))) {

            XSSFSheet firstSheet = workbook.getSheetAt(0);

            List<SequenceGenerator> entityList = new ArrayList<>();

            for (int i = 1; i <= firstSheet.getLastRowNum(); i++) {

                SequenceGenerator tmpObj = sequenceGeneratorRepo.findBySequenceType(
                        getSequenceType(firstSheet.getRow(i).getCell(0).getStringCellValue().trim()));

                if (tmpObj == null) {
                    SequenceGenerator entity = new SequenceGenerator();
                    entity.setCompanyMaster(companyMaster);
                    entity.setCompanyCode(companyMaster.getCompanyCode());
                    entity.setSequenceType(
                            getSequenceType(firstSheet.getRow(i).getCell(0).getStringCellValue().trim().toUpperCase()));
                    entity.setSequenceName(firstSheet.getRow(i).getCell(1).getStringCellValue().trim());
                    entity.setPrefix(firstSheet.getRow(i).getCell(2).getStringCellValue().trim().toUpperCase());
                    entity.setFormat(getSequenceFormat(
                            firstSheet.getRow(i).getCell(3).getStringCellValue().trim().toUpperCase()));

                    firstSheet.getRow(i).getCell(4).setCellType(Cell.CELL_TYPE_STRING);
                    entity.setCurrentSequenceValue(
                            Long.parseLong(firstSheet.getRow(i).getCell(4).getStringCellValue()));

                    if (firstSheet.getRow(i).getCell(5).getStringCellValue().trim().toUpperCase().equals("YES")) {
                        entity.setIsYear(YesNo.Yes);
                    } else {
                        entity.setIsYear(YesNo.No);
                    }

                    if (firstSheet.getRow(i).getCell(6).getStringCellValue().trim().toUpperCase().equals("YES")) {
                        entity.setIsMonth(YesNo.Yes);
                    } else {
                        entity.setIsMonth(YesNo.No);
                    }

                    entity.setSuffix(firstSheet.getRow(i).getCell(7).getStringCellValue());

                    entity.setSeparator(firstSheet.getRow(i).getCell(8).getStringCellValue());

                    entityList.add(entity);
                }

                if (entityList.size() == 50) {
                    sequenceGeneratorRepo.save(entityList);
                    sequenceGeneratorRepo.flush();
                    entityList = new ArrayList<>();
                }

            }
            if (entityList.size() != 0) {
                sequenceGeneratorRepo.save(entityList);
            }
            workbook.close();


        } catch (RestException exception) {

            log.error("->Exception in Create method : ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (ObjectOptimisticLockingFailureException e) {

            log.error("Exception in  -> saveOrUpdate method ", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_LOCKED_RECORD);

        } catch (JpaObjectRetrievalFailureException e) {

            log.error("Exception in  -> saveOrUpdate method ", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_DELETED_RECORD);

        } catch (Exception exception) {
            log.error("Failed to save ..", exception);

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Exception Cause 1 ::: " + exceptionCause1);

            // General error condition
            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            if (exceptionCause1.contains(UniqueKey.UK_SEQUENCE_TYPE)) {
                baseDto.setResponseCode(ErrorCode.SEQUENCE_TYPE_ALREADY_EXIST);
            }

            if (exceptionCause1.contains(UniqueKey.UK_SEQUENCE_NAME)) {
                baseDto.setResponseCode(ErrorCode.SEQUENCE_NAME_ALREADY_EXIST);
            }

            if (exceptionCause1.contains(UniqueKey.UK_SEQUENCE_PREFIX)) {
                baseDto.setResponseCode(ErrorCode.SEQUENCE_PREFIX_ALREADY_EXIST);
            }

        }

    }

    private SequenceType getSequenceType(String sequence) {

        if (SequenceType.PARTY.toString().equals(sequence)) {
            return SequenceType.PARTY;
        } else if (SequenceType.AES_BATCH.toString().equals(sequence)) {
            return SequenceType.AES_BATCH;
        } else if (SequenceType.AIRLINE_PREBOOKING.toString().equals(sequence)) {
            return SequenceType.AIRLINE_PREBOOKING;
        } else if (SequenceType.CAN.toString().equals(sequence)) {
            return SequenceType.CAN;
        } else if (SequenceType.CFS.toString().equals(sequence)) {
            return SequenceType.CFS;
        } else if (SequenceType.CONSOL.toString().equals(sequence)) {
            return SequenceType.CONSOL;
        } else if (SequenceType.DEPARTMENT.toString().equals(sequence)) {
            return SequenceType.DEPARTMENT;
        } else if (SequenceType.DESIGNATION.toString().equals(sequence)) {
            return SequenceType.DESIGNATION;
        } else if (SequenceType.DO.toString().equals(sequence)) {
            return SequenceType.DO;
        } else if (SequenceType.DOCUMENT.toString().equals(sequence)) {
            return SequenceType.DOCUMENT;
        } else if (SequenceType.EMPLOYEE.toString().equals(sequence)) {
            return SequenceType.EMPLOYEE;
        } else if (SequenceType.ENQUERY.toString().equals(sequence)) {
            return SequenceType.ENQUERY;
        } else if (SequenceType.FLIGHTPLAN.toString().equals(sequence)) {
            return SequenceType.FLIGHTPLAN;
        } else if (SequenceType.PURCHASE_ORDER.toString().equals(sequence)) {
            return SequenceType.PURCHASE_ORDER;
        } else if (SequenceType.QUOTATION.toString().equals(sequence)) {
            return SequenceType.QUOTATION;
        } else if (SequenceType.REFERENCE_NO.toString().equals(sequence)) {
            return SequenceType.REFERENCE_NO;
        } else if (SequenceType.SERVICE.toString().equals(sequence)) {
            return SequenceType.SERVICE;
        } else if (SequenceType.SHIPMENT.toString().equals(sequence)) {
            return SequenceType.SHIPMENT;
        }

        return null;
    }

    private SequenceFormat getSequenceFormat(String format) {

        if (SequenceFormat.ONE.toString().equals(format)) {
            return SequenceFormat.ONE;
        } else if (SequenceFormat.TWO.toString().equals(format)) {
            return SequenceFormat.TWO;
        } else if (SequenceFormat.THREE.toString().equals(format)) {
            return SequenceFormat.THREE;
        } else if (SequenceFormat.FOUR.toString().equals(format)) {
            return SequenceFormat.FOUR;
        } else if (SequenceFormat.FIVE.toString().equals(format)) {
            return SequenceFormat.FIVE;
        } else if (SequenceFormat.SIX.toString().equals(format)) {
            return SequenceFormat.SIX;
        } else if (SequenceFormat.SEVEN.toString().equals(format)) {
            return SequenceFormat.SEVEN;
        } else if (SequenceFormat.EIGHT.toString().equals(format)) {
            return SequenceFormat.EIGHT;
        } else if (SequenceFormat.NINE.toString().equals(format)) {
            return SequenceFormat.NINE;
        } else if (SequenceFormat.TEN.toString().equals(format)) {
            return SequenceFormat.TEN;
        }

        return null;
    }

    private DocumentType getDocumentType(String format) {

        if (DocumentType.INV.toString().equals(format)) {
            return DocumentType.INV;
        } else if (DocumentType.CRN.toString().equals(format)) {
            return DocumentType.CRN;
        } else if (DocumentType.PMT.toString().equals(format)) {
            return DocumentType.PMT;
        } else if (DocumentType.RPT.toString().equals(format)) {
            return DocumentType.RPT;
        }

        return null;
    }

    public void loadDocumentType(BaseDto baseDto) {


        try (XSSFWorkbook workbook = new XSSFWorkbook(
                new FileInputStream(new File(appResourcePath + "/efs-master-data/document-type-master.xlsx")))) {
            XSSFSheet firstSheet = workbook.getSheetAt(0);

            List<DocumentTypeMaster> entityList = new ArrayList<>();

            for (int i = 1; i <= firstSheet.getLastRowNum(); i++) {

                DocumentType code = getDocumentType(
                        firstSheet.getRow(i).getCell(0).getStringCellValue().trim().toUpperCase());

                DocumentTypeMaster tmpObj = documentTypeRepo.findByDocumentTypeCode(code);

                if (tmpObj == null) {

                    DocumentTypeMaster entity = new DocumentTypeMaster();
                    entity.setStatus(LovStatus.Active);
                    entity.setDocumentTypeCode(code);
                    entity.setDocumentTypeName(firstSheet.getRow(i).getCell(1).getStringCellValue().trim());
                    entityList.add(entity);

                }

                if (entityList.size() == 50) {
                    documentTypeRepo.save(entityList);
                    documentTypeRepo.flush();
                    entityList = new ArrayList<>();
                }

            }

            if (entityList.size() != 0) {
                documentTypeRepo.save(entityList);
            }

            workbook.close();


        } catch (RestException exception) {

            log.error("->Exception in Create method : ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (ObjectOptimisticLockingFailureException e) {

            log.error("Exception in  -> saveOrUpdate method ", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_LOCKED_RECORD);

        } catch (JpaObjectRetrievalFailureException e) {

            log.error("Exception in  -> saveOrUpdate method ", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_DELETED_RECORD);

        } catch (Exception exception) {
            log.error("Failed to save ..", exception);

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Exception Cause 1 ::: " + exceptionCause1);

            // General error condition
            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            if (exceptionCause1.contains("FK_")) {
                baseDto.setResponseCode(ErrorCode.SAVE_UPDATE_FK_RECORD);
            }

            if (exceptionCause1.contains(UniqueKey.UK_DOCUMENT_DOCUMENTTYPECODE)) {
                baseDto.setResponseCode(ErrorCode.DOCUMENT_TYPE_CODE_ALREADY_EXIST);

            }

            if (exceptionCause1.contains(UniqueKey.UK_DOCUMENT_DOCUMENTTYPENAME)) {
                baseDto.setResponseCode(ErrorCode.DOCUMENT_TYPE_NAME_ALREADY_EXIST);
            }

        }

    }


    public void customerOrAgentBulkUpload(BaseDto baseDto, byte[] file, LocationMaster locationMaster, EmployeeMaster employeeMaster, String uploadType, String fileName) {

        try {

            List<PartyMaster> list = metaConfigurationService.getPartyList(baseDto, file, locationMaster, employeeMaster, uploadType, fileName);

            for (PartyMaster partyMaster : list) {
                partyMaster.setPartyCode(sequenceGeneratorService.getSequence(SequenceType.PARTY, locationMaster));
            }

            partyMasterRepo.save(list);
            baseDto.setResponseCode(ErrorCode.SUCCESS);
            baseDto.setResponseObject(list);

        } catch (RestException exception) {

            log.error("Rest Exception occured with error code ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (Exception exception) {

            log.error("Exception occured ", exception);

            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Exception Cause 1 : " + exceptionCause1);

            // General error condition
            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            // Duplicate Party Code
            if (exceptionCause1.contains(UniqueKey.UK_PARTY_PARTYCODE)) {
                baseDto.setResponseCode(ErrorCode.PARTY_CODE_ALREADY_EXIST);
            }

            // Duplicate Party Name
            if (exceptionCause1.contains(UniqueKey.UK_PARTY_PARTYNAME)) {
                baseDto.setResponseCode(ErrorCode.PARTY_NAME_ALREADY_EXIST);
            }

            // Duplicate Vat Number
            if (exceptionCause1.contains(UniqueKey.UK_PARTYDETAIL_VALUEADDEDTAX)) {
                baseDto.setResponseCode(ErrorCode.PARTY_VAT_NUMBER_ALREADY_EXIST);
            }

            // Duplicate PAN Number
            if (exceptionCause1.contains(UniqueKey.UK_PARTYDETAIL_PAN)) {
                baseDto.setResponseCode(ErrorCode.PARTY_PAN_NUMBER_ALREADY_EXIST);
            }

            // Duplicate CST Number
            if (exceptionCause1.contains(UniqueKey.UK_PARTYDETAIL_CSTNO)) {
                baseDto.setResponseCode(ErrorCode.PARTY_CST_NUMBER_ALREADY_EXIST);
            }

            // Duplicate RAC Number
            if (exceptionCause1.contains(UniqueKey.UK_PARTYDETAIL_RACNO)) {
                baseDto.setResponseCode(ErrorCode.PARTY_RAC_NUMBER_ALREADY_EXIST);
            }

            // Duplicate SVT Number
            if (exceptionCause1.contains(UniqueKey.UK_PARTYDETAIL_SVTNO)) {
                baseDto.setResponseCode(ErrorCode.PARTY_SVT_NUMBER_ALREADY_EXIST);
            }

            // Duplicate SVAT Number
            if (exceptionCause1.contains(UniqueKey.UK_PARTYDETAIL_SVATNO)) {
                baseDto.setResponseCode(ErrorCode.PARTY_SVAT_NUMBER_ALREADY_EXIST);
            }

            // Duplicate IATA CODE
            if (exceptionCause1.contains(UniqueKey.UK_PARTYDETAIL_IATA_CODE)) {
                baseDto.setResponseCode(ErrorCode.PARTY_IATA_CODE_ALREADY_EXIST);
            }

            // Duplicate TIN number
            if (exceptionCause1.contains(UniqueKey.UK_PARTYDETAIL_TINNO)) {
                baseDto.setResponseCode(ErrorCode.PARTY_TIN_NUMBER_ALREADY_EXIST);
            }

            // Duplicate Party Service
            if (exceptionCause1.contains(UniqueKey.UK_PARTYSERVIE_PARTYSERVICE)) {
                log.info("Error Code : " + ErrorCode.PARTY_PARTY_SERVICE_ALREADY_EXIST);
                baseDto.setResponseCode(ErrorCode.PARTY_PARTY_SERVICE_ALREADY_EXIST);
            }

            // Duplicate Party Company
            if (exceptionCause1.contains(UniqueKey.UK_PARTYCOMPDIV_PARTYCOMPDIV)) {
                baseDto.setResponseCode(ErrorCode.PARTY_PARTY_COMPANY_ALREADY_EXIST);
            }

            // Duplicate Credit mapping
            if (exceptionCause1.contains(UniqueKey.UK_PARTY_CREDIT)) {
                baseDto.setResponseCode(ErrorCode.PARTY_CREDIT_ALREADY_EXIST);
            }
            // Duplicate Account
            if (exceptionCause1.contains(UniqueKey.UK_PARTY_ACCOUNT)) {
                baseDto.setResponseCode(ErrorCode.PARTY_ACCOUNT_ALREADY_EXIST);
            }

            // Duplicate Email
            if (exceptionCause1.contains(UniqueKey.UK_PARTYEMAIL_PARTY)) {
                baseDto.setResponseCode(ErrorCode.PARTY_EMAIL_ALREADY_EXIST);
            }
        }
        appUtil.setDesc(baseDto);
    }

    public void loadReportConfig(BaseDto baseDto, LocationMaster location) {


        try (XSSFWorkbook workbook = new XSSFWorkbook(
                new FileInputStream(new File(appResourcePath + "/efs-master-data/report-config.xlsx")))) {

            XSSFSheet firstSheet = workbook.getSheetAt(0);

            List<ReportConfigurationMaster> entityList = new ArrayList<>();

            for (int i = 1; i < firstSheet.getLastRowNum(); i++) {

                ReportConfigurationMaster tmpObj = reportConfigRepo.getByKeyAndLocation(
                        ReportConfig.valueOf(firstSheet.getRow(i).getCell(0).getStringCellValue().trim()),
                        location.getId());

                if (tmpObj == null) {
                    ReportConfigurationMaster entity = new ReportConfigurationMaster();

                    entity.setLocation(location);
                    entity.setLocationCode(location.getLocationCode());
                    entity.setReportKey(
                            ReportConfig.valueOf(firstSheet.getRow(i).getCell(0).getStringCellValue().trim()));

                    if (firstSheet.getRow(i).getCell(1) == null
                            || firstSheet.getRow(i).getCell(1).getStringCellValue() == null) {
                        entity.setReportValue(null);
                    } else {
                        entity.setReportValue(firstSheet.getRow(i).getCell(1).getStringCellValue().trim());
                    }

                    entityList.add(entity);
                }

                if (entityList.size() == 50) {
                    reportConfigRepo.save(entityList);
                    reportConfigRepo.flush();
                    entityList = new ArrayList<>();
                }

            }

            if (entityList.size() != 0) {
                reportConfigRepo.save(entityList);
            }

            workbook.close();


        } catch (RestException exception) {

            log.error("->Exception in Create method : ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (ObjectOptimisticLockingFailureException e) {

            log.error("Exception in  -> saveOrUpdate method ", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_LOCKED_RECORD);

        } catch (JpaObjectRetrievalFailureException e) {

            log.error("Exception in  -> saveOrUpdate method ", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_DELETED_RECORD);

        } catch (Exception exception) {
            log.error("Failed to save ..", exception);

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Exception Cause 1 ::: " + exceptionCause1);

            // General error condition
            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            if (exceptionCause1.contains("FK_")) {
                baseDto.setResponseCode(ErrorCode.SAVE_UPDATE_FK_RECORD);
            }

        }

    }

    public void loadDepartment(BaseDto baseDto, LocationMaster locationMaster) {


        try (XSSFWorkbook workbook = new XSSFWorkbook(
                new FileInputStream(new File(appResourcePath + "/efs-master-data/department_master.xlsx")))) {

            XSSFSheet firstSheet = workbook.getSheetAt(0);

            List<DepartmentMaster> entityList = new ArrayList<>();

            for (int i = 1; i < firstSheet.getLastRowNum(); i++) {

                DepartmentMaster tmpObj = departmentMasterRepository.findByName(firstSheet.getRow(i).getCell(0).getStringCellValue().trim().toUpperCase());

                if (tmpObj == null) {
                    DepartmentMaster entity = new DepartmentMaster();
                    entity.setDepartmentName(firstSheet.getRow(i).getCell(0).getStringCellValue().trim());
                    entity.setStatus(LovStatus.Active);
                    entity.setDepartmentCode(sequenceGeneratorService.getSequence(SequenceType.DEPARTMENT, locationMaster));
                    entityList.add(entity);
                }

                if (entityList.size() == 50) {
                    departmentMasterRepository.save(entityList);
                    departmentMasterRepository.flush();
                    entityList = new ArrayList<>();
                }

            }

            if (entityList.size() != 0) {
                departmentMasterRepository.save(entityList);
            }

            workbook.close();


        } catch (RestException exception) {

            log.error("->Exception in Create method : ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (ObjectOptimisticLockingFailureException e) {

            log.error("Exception in  -> saveOrUpdate method ", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_LOCKED_RECORD);

        } catch (JpaObjectRetrievalFailureException e) {

            log.error("Exception in  -> saveOrUpdate method ", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_DELETED_RECORD);

        } catch (Exception exception) {
            log.error("Failed to save ..", exception);

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Exception Cause 1 ::: " + exceptionCause1);

            // General error condition
            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            if (exceptionCause1.contains("FK_")) {
                baseDto.setResponseCode(ErrorCode.SAVE_UPDATE_FK_RECORD);
            }

            if (exceptionCause1.contains(UniqueKey.UK_DEPARTMENT_DEPTCODE)) {
                baseDto.setResponseCode(ErrorCode.DEPARTMENT_CODE_DUPLICATED);
            }

            if (exceptionCause1.contains(UniqueKey.UK_DEPARTMENT_DEPTNAME)) {
                baseDto.setResponseCode(ErrorCode.DEPARTMENT_NAME_DUPLICATED);
            }

        }

    }

    public void loadDesignation(BaseDto baseDto, LocationMaster locationMaster) {


        try (XSSFWorkbook workbook = new XSSFWorkbook(
                new FileInputStream(new File(appResourcePath + "/efs-master-data/designation_master.xlsx")))) {

            XSSFSheet firstSheet = workbook.getSheetAt(0);

            List<DesignationMaster> entityList = new ArrayList<>();

            for (int i = 1; i < firstSheet.getLastRowNum(); i++) {

                DesignationMaster tmpObj = designationMasterRepository.findByDesignationName(firstSheet.getRow(i).getCell(0).getStringCellValue().trim().toUpperCase());

                if (tmpObj == null) {
                    DesignationMaster entity = new DesignationMaster();
                    entity.setDesignationName(firstSheet.getRow(i).getCell(0).getStringCellValue().trim());
                    entity.setStatus(LovStatus.Active);
                    entity.setDesignationCode(sequenceGeneratorService.getSequence(SequenceType.DESIGNATION, locationMaster));

                    if (firstSheet.getRow(i).getCell(1).getStringCellValue().trim().equals("Y")) {
                        entity.setManager(YesNo.Yes);
                    } else {
                        entity.setManager(YesNo.No);
                    }

                    entityList.add(entity);
                }

                if (entityList.size() == 50) {
                    designationMasterRepository.save(entityList);
                    designationMasterRepository.flush();
                    entityList = new ArrayList<>();
                }

            }

            if (entityList.size() != 0) {
                designationMasterRepository.save(entityList);
            }

            workbook.close();


        } catch (RestException exception) {

            log.error("->Exception in Create method : ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (ObjectOptimisticLockingFailureException e) {

            log.error("Exception in  -> saveOrUpdate method ", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_LOCKED_RECORD);

        } catch (JpaObjectRetrievalFailureException e) {

            log.error("Exception in  -> saveOrUpdate method ", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_DELETED_RECORD);

        } catch (Exception exception) {
            log.error("Failed to save ..", exception);

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Exception Cause 1 ::: " + exceptionCause1);

            // General error condition
            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            if (exceptionCause1.contains("FK_")) {
                baseDto.setResponseCode(ErrorCode.SAVE_UPDATE_FK_RECORD);
            }

            if (exceptionCause1.contains(UniqueKey.UK_DESIGNATION_DESIGNATION_CODE)) {
                baseDto.setResponseCode(ErrorCode.DESIGNATION_CODE_DUPLICATED);
            }

            if (exceptionCause1.contains(UniqueKey.UK_DESIGNATION_DESIGNATION_NAME)) {
                baseDto.setResponseCode(ErrorCode.DESIGNATION_NAME_DUPLICATED);
            }

        }

    }


    public void loadCommodityMaster(BaseDto baseDto) {


        try (XSSFWorkbook workbook = new XSSFWorkbook(
                new FileInputStream(new File(appResourcePath + "/efs-master-data/commodity_master.xlsx")))) {
            XSSFSheet firstSheet = workbook.getSheetAt(0);

            List<CommodityMaster> entityList = new ArrayList<>();

            for (int i = 1; i <= firstSheet.getLastRowNum(); i++) {

                String hsCode = firstSheet.getRow(i).getCell(0).getStringCellValue().trim().toUpperCase();
                CommodityMaster tmpObj = commodityMasterRepository.findByhsCode(hsCode);

                if (tmpObj == null) {

                    CommodityMaster entity = new CommodityMaster();

                    entity.setStatus(LovStatus.Active);
                    entity.setHsCode(hsCode);
                    entity.setHsName(firstSheet.getRow(i).getCell(1).getStringCellValue().trim());

                    entityList.add(entity);
                }
                if (entityList.size() == 50) {
                    commodityMasterRepository.save(entityList);
                    commodityMasterRepository.flush();
                    entityList = new ArrayList<>();
                }
            }
            if (entityList.size() != 0) {
                commodityMasterRepository.save(entityList);
            }
            workbook.close();


        } catch (RestException exception) {

            log.error("->Exception in Create method : ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (ObjectOptimisticLockingFailureException e) {

            log.error("Exception in  -> saveOrUpdate method ", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_LOCKED_RECORD);

        } catch (JpaObjectRetrievalFailureException e) {

            log.error("Exception in  -> saveOrUpdate method ", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_DELETED_RECORD);

        } catch (Exception exception) {
            log.error("Failed to save ..", exception);

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Exception Cause 1 ::: " + exceptionCause1);

            // General error condition
            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            if (exceptionCause1.contains("FK_")) {
                baseDto.setResponseCode(ErrorCode.SAVE_UPDATE_FK_RECORD);
            }

            if (exceptionCause1.contains(UniqueKey.UK_COMMODITY_HSCODE)) {
                baseDto.setResponseCode(ErrorCode.COMMODITY_CODE_ALREADY_EXIST);
            }

            if (exceptionCause1.contains(UniqueKey.UK_COMMODITY_HSNAME)) {
                baseDto.setResponseCode(ErrorCode.COMMODITY_NAME_ALREADY_EXIST);
            }

        }
    }

    public void loadCarrierMaster(BaseDto baseDto) {


        try (XSSFWorkbook workbook = new XSSFWorkbook(
                new FileInputStream(new File(appResourcePath + "/efs-master-data/carrier_master.xlsx")))) {
            XSSFSheet firstSheet = workbook.getSheetAt(0);

            List<CarrierMaster> entityList = new ArrayList<>();

            for (int i = 1; i <= firstSheet.getLastRowNum(); i++) {

                String carrierCode = firstSheet.getRow(i).getCell(1).getStringCellValue().trim().toUpperCase();
                CarrierMaster tmpObj = carrierMasterRepo.findByCarrierCode(carrierCode);

                if (tmpObj == null) {

                    CarrierMaster entity = new CarrierMaster();

                    entity.setStatus(LovStatus.Active);
                    entity.setCarrierCode(carrierCode);
                    entity.setCarrierName(firstSheet.getRow(i).getCell(2).getStringCellValue().trim());

                    String trasportMode = firstSheet.getRow(i).getCell(0).getStringCellValue().trim().toUpperCase();
                    if (trasportMode.equals("AIR")) {
                        entity.setTransportMode(TransportMode.Air);

                        String icaoCode = firstSheet.getRow(i).getCell(4).getStringCellValue().trim();
                        if (icaoCode != null && icaoCode.trim().length() != 0) {
                            entity.setIataCode(icaoCode);
                        }

                    } else if (trasportMode.equals("OCEAN")) {
                        entity.setTransportMode(TransportMode.Ocean);

                        String scacCode = firstSheet.getRow(i).getCell(5).getStringCellValue().trim();
                        if (scacCode != null && scacCode.trim().length() != 0) {
                            entity.setScacCode(scacCode);
                        }

                    } else if (trasportMode.equals("RAIL")) {
                        entity.setTransportMode(TransportMode.Rail);
                    } else if (trasportMode.equals("ROAD")) {
                        entity.setTransportMode(TransportMode.Road);
                    } else {
                        continue;
                    }

                    if (firstSheet.getRow(i).getCell(3) != null) {
                        firstSheet.getRow(i).getCell(3).setCellType(Cell.CELL_TYPE_STRING);
                        String carrierNo = firstSheet.getRow(i).getCell(3).getStringCellValue().trim();
                        if (carrierNo != null && carrierNo.trim().length() != 0) {
                            entity.setCarrierNo(carrierNo);
                        }
                    }

                    entityList.add(entity);
                }
                if (entityList.size() == 50) {
                    carrierMasterRepo.save(entityList);
                    carrierMasterRepo.flush();
                    entityList = new ArrayList<>();
                }
            }
            if (entityList.size() != 0) {
                carrierMasterRepo.save(entityList);
            }
            workbook.close();


        } catch (RestException exception) {

            log.error("->Exception in Create method : ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (ObjectOptimisticLockingFailureException e) {

            log.error("Exception in  -> saveOrUpdate method ", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_LOCKED_RECORD);

        } catch (JpaObjectRetrievalFailureException e) {

            log.error("Exception in  -> saveOrUpdate method ", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_DELETED_RECORD);

        } catch (Exception exception) {
            log.error("Failed to save ..", exception);

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Exception Cause 1 ::: " + exceptionCause1);

            // General error condition
            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            if (exceptionCause1.contains("FK_")) {
                baseDto.setResponseCode(ErrorCode.SAVE_UPDATE_FK_RECORD);
            }

            if (exceptionCause1.contains(UniqueKey.UK_CARRIER_CARRIERCODE)) {
                baseDto.setResponseCode(ErrorCode.CARRIER_CODE_ALREADY_EXIST);

            }

            if (exceptionCause1.contains(UniqueKey.UK_CARRIER_CARRIERNAME)) {
                baseDto.setResponseCode(ErrorCode.CARRIER_NAME_ALREADY_EXIST);
            }

            if (exceptionCause1.contains(UniqueKey.UK_CARRIER_CARRIERNO)) {
                baseDto.setResponseCode(ErrorCode.CARRIER_NUM_ALREADY_EXIST);
            }

            if (exceptionCause1.contains(UniqueKey.UK_CARRIER_IATACODE)) {
                baseDto.setResponseCode(ErrorCode.CARRIER_IATA_CODE_ALREADY_EXIST);
            }

            if (exceptionCause1.contains(UniqueKey.UK_CARRIER_SCACCODE)) {
                baseDto.setResponseCode(ErrorCode.CARRIER_SCAC_CODE_ALREADY_EXIST);
            }

            if (exceptionCause1.contains(UniqueKey.UK_CARRIEREDI_EDITYPE)) {
                baseDto.setResponseCode(ErrorCode.EDI_TYPE_ALREDY_EXIST);
            }

        }
    }

    public void loadEventMaster(BaseDto baseDto) {


        try (XSSFWorkbook workbook = new XSSFWorkbook(
                new FileInputStream(new File(appResourcePath + "/efs-master-data/event_master.xlsx")))) {

            XSSFSheet firstSheet = workbook.getSheetAt(0);

            List<EventMaster> entityList = new ArrayList<>();

            for (int i = 1; i <= firstSheet.getLastRowNum(); i++) {

                String eventCode = firstSheet.getRow(i).getCell(0).getStringCellValue().trim().toUpperCase();
                EventMaster tmpObj = eventMasterRepository.findByEventCode(eventCode);

                if (tmpObj == null) {

                    EventMaster entity = new EventMaster();

                    entity.setStatus(LovStatus.Active);
                    entity.setEventCode(eventCode);
                    entity.setEventName(firstSheet.getRow(i).getCell(1).getStringCellValue().trim());
                    entity.setEventMasterType(EventMasterType.valueOf(firstSheet.getRow(i).getCell(2).getStringCellValue().trim().toUpperCase()));

                    entityList.add(entity);
                }
                if (entityList.size() == 50) {
                    eventMasterRepository.save(entityList);
                    eventMasterRepository.flush();
                    entityList = new ArrayList<>();
                }
            }
            if (entityList.size() != 0) {
                eventMasterRepository.save(entityList);
            }
            workbook.close();


        } catch (RestException exception) {

            log.error("->Exception in Create method : ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (ObjectOptimisticLockingFailureException e) {

            log.error("Exception in  -> saveOrUpdate method ", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_LOCKED_RECORD);

        } catch (JpaObjectRetrievalFailureException e) {

            log.error("Exception in  -> saveOrUpdate method ", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_DELETED_RECORD);

        } catch (Exception exception) {
            log.error("Failed to save ..", exception);

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Exception Cause 1 ::: " + exceptionCause1);

            // General error condition
            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            if (exceptionCause1.contains("FK_")) {
                baseDto.setResponseCode(ErrorCode.SAVE_UPDATE_FK_RECORD);
            }

            if (exceptionCause1.contains(UniqueKey.UK_EVENT_EVENTCODE)) {
                baseDto.setResponseCode(ErrorCode.MASTER_EVENT_ALREADY_CODE);
            }

            if (exceptionCause1.contains(UniqueKey.UK_EVENT_EVENTNAME)) {
                baseDto.setResponseCode(ErrorCode.MASTER_EVENT_ALREADY_NAME);
            }
        }
    }

    public void loadReferenceTypeMaster(BaseDto baseDto) {


        try (XSSFWorkbook workbook = new XSSFWorkbook(
                new FileInputStream(new File(appResourcePath + "/efs-master-data/reference_type_master.xlsx")))) {

            XSSFSheet firstSheet = workbook.getSheetAt(0);

            List<ReferenceTypeMaster> entityList = new ArrayList<>();

            for (int i = 1; i <= firstSheet.getLastRowNum(); i++) {

                String referenceTypeCode = firstSheet.getRow(i).getCell(0).getStringCellValue().trim().toUpperCase();
                ReferenceTypeMaster tmpObj = referenceTypeMasterRepository.findByReferenceTypeCode(referenceTypeCode);

                if (tmpObj == null) {

                    ReferenceTypeMaster entity = new ReferenceTypeMaster();

                    entity.setStatus(LovStatus.Active);
                    entity.setReferenceTypeCode(referenceTypeCode);
                    entity.setReferenceTypeName(firstSheet.getRow(i).getCell(1).getStringCellValue().trim());
                    entity.setReferenceType(ReferenceType.valueOf(firstSheet.getRow(i).getCell(2).getStringCellValue().trim()));

                    entityList.add(entity);
                }
                if (entityList.size() == 50) {
                    referenceTypeMasterRepository.save(entityList);
                    referenceTypeMasterRepository.flush();
                    entityList = new ArrayList<>();
                }
            }
            if (entityList.size() != 0) {
                referenceTypeMasterRepository.save(entityList);
            }
            workbook.close();


        } catch (RestException exception) {

            log.error("->Exception in Create method : ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (ObjectOptimisticLockingFailureException e) {

            log.error("Exception in  -> saveOrUpdate method ", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_LOCKED_RECORD);

        } catch (JpaObjectRetrievalFailureException e) {

            log.error("Exception in  -> saveOrUpdate method ", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_DELETED_RECORD);

        } catch (Exception exception) {
            log.error("Failed to save ..", exception);

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Exception Cause 1 ::: " + exceptionCause1);

            // General error condition
            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            if (exceptionCause1.contains("FK_")) {
                baseDto.setResponseCode(ErrorCode.SAVE_UPDATE_FK_RECORD);
            }

            if (exceptionCause1.contains(UniqueKey.UK_REFERENCE_REFERENCECODE)) {
                baseDto.setResponseCode(ErrorCode.REFERENCE_TYPE_CODE_ALREADY_EXIST);
            }

            if (exceptionCause1.contains(UniqueKey.UK_REFERENCE_REFERENCENAME)) {
                baseDto.setResponseCode(ErrorCode.REFERENCE_TYPE_NAME_ALREADY_EXIST);
            }
        }
    }

    public void loadPackMaster(BaseDto baseDto) {


        try (XSSFWorkbook workbook = new XSSFWorkbook(new FileInputStream(new File(appResourcePath + "/efs-master-data/pack-master.xlsx")))) {
            XSSFSheet firstSheet = workbook.getSheetAt(0);

            List<PackMaster> entityList = new ArrayList<>();

            for (int i = 1; i <= firstSheet.getLastRowNum(); i++) {

                String packCode = firstSheet.getRow(i).getCell(0).getStringCellValue().trim().toUpperCase();
                PackMaster tmpObj = packRepo.findByPackCode(packCode);

                if (tmpObj == null) {

                    PackMaster entity = new PackMaster();

                    entity.setStatus(LovStatus.Active);
                    entity.setPackCode(packCode);
                    entity.setPackName(firstSheet.getRow(i).getCell(1).getStringCellValue().trim().toUpperCase());


                    entityList.add(entity);
                }
                if (entityList.size() == 50) {
                    packRepo.save(entityList);
                    packRepo.flush();
                    entityList = new ArrayList<>();
                }

            }
            if (entityList.size() != 0) {
                packRepo.save(entityList);
            }
            workbook.close();


        } catch (RestException exception) {

            log.error("->Exception in Create method : ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (ObjectOptimisticLockingFailureException e) {

            log.error("Exception in  -> saveOrUpdate method ", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_LOCKED_RECORD);

        } catch (JpaObjectRetrievalFailureException e) {

            log.error("Exception in  -> saveOrUpdate method ", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_DELETED_RECORD);

        } catch (Exception exception) {
            log.error("Failed to save ..", exception);

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Exception Cause 1 ::: " + exceptionCause1);

            // General error condition
            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            if (exceptionCause1.contains("FK_")) {
                baseDto.setResponseCode(ErrorCode.SAVE_UPDATE_FK_RECORD);
            }

            if (exceptionCause1.contains(UniqueKey.UK_PACK_PACKCODE)) {
                baseDto.setResponseCode(ErrorCode.PACK_CODE_ALREADY_EXIST);
            }
            if (exceptionCause1.contains(UniqueKey.UK_PACK_PACKNAME)) {
                baseDto.setResponseCode(ErrorCode.PACK_NAME_ALREADY_EXIST);
            }


        }


    }

    public void loadChargeMaster(BaseDto baseDto) {


        try (XSSFWorkbook workbook = new XSSFWorkbook(
                new FileInputStream(new File(appResourcePath + "/efs-master-data/charge_master.xlsx")))) {
            XSSFSheet firstSheet = workbook.getSheetAt(0);

            List<ChargeMaster> entityList = new ArrayList<>();

            for (int i = 1; i <= firstSheet.getLastRowNum(); i++) {

                String code = firstSheet.getRow(i).getCell(0).getStringCellValue().trim().toUpperCase();
                ChargeMaster tmpObj = chargeRepo.findByChargeCode(code);

                if (tmpObj == null) {

                    ChargeMaster entity = new ChargeMaster();

                    entity.setStatus(LovStatus.Active);
                    entity.setChargeCode(code);
                    entity.setChargeName(firstSheet.getRow(i).getCell(1).getStringCellValue().trim());

                    String chargeType = firstSheet.getRow(i).getCell(2).getStringCellValue().trim().toUpperCase();
                    if (chargeType.equals("ORIGIN")) {
                        entity.setChargeType(ChargeType.Origin);
                    } else if (chargeType.equals("DESTINATION")) {
                        entity.setChargeType(ChargeType.Destination);
                    } else if (chargeType.equals("FREIGHT")) {
                        entity.setChargeType(ChargeType.Freight);
                    } else if (chargeType.equals("OTHER")) {
                        entity.setChargeType(ChargeType.Other);
                    } else {
                        continue;
                    }

                    String calcType = firstSheet.getRow(i).getCell(3).getStringCellValue().trim().toUpperCase();
                    if (calcType.equals("SHIPMENT")) {
                        entity.setCalculationType(ChargeCalculationType.Shipment);
                    } else if (calcType.equals("UNIT")) {
                        entity.setCalculationType(ChargeCalculationType.Unit);
                    } else if (calcType.equals("DOCUMENT")) {
                        entity.setCalculationType(ChargeCalculationType.Document);
                    } else if (calcType.equals("PERCENTAGE")) {
                        entity.setCalculationType(ChargeCalculationType.Percentage);
                    } else {
                        continue;
                    }

                    entityList.add(entity);
                }
                if (entityList.size() == 50) {
                    chargeRepo.save(entityList);
                    chargeRepo.flush();
                    entityList = new ArrayList<>();
                }
            }
            if (entityList.size() != 0) {
                chargeRepo.save(entityList);
            }
            workbook.close();


        } catch (RestException exception) {

            log.error("->Exception in Create method : ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (ObjectOptimisticLockingFailureException e) {

            log.error("Exception in  -> saveOrUpdate method ", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_LOCKED_RECORD);

        } catch (JpaObjectRetrievalFailureException e) {

            log.error("Exception in  -> saveOrUpdate method ", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_DELETED_RECORD);

        } catch (Exception exception) {
            log.error("Failed to save ..", exception);

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Exception Cause 1 ::: " + exceptionCause1);

            // General error condition
            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            if (exceptionCause1.contains("FK_")) {
                baseDto.setResponseCode(ErrorCode.SAVE_UPDATE_FK_RECORD);
            }

            if (exceptionCause1.contains(UniqueKey.UK_CHARGE_CHARGECODE)) {
                baseDto.setResponseCode(ErrorCode.CHARGE_CODE_ALREADY_EXIST);
            }

            if (exceptionCause1.contains(UniqueKey.UK_CHARGE_CHARGENAME)) {
                baseDto.setResponseCode(ErrorCode.CHARGE_NAME_ALREADY_EXIST);
            }

            if (exceptionCause1.contains(UniqueKey.UK_EDICHARGE_EDITYPE)) {
                baseDto.setResponseCode(ErrorCode.EDI_TYPE_ALREDY_EXIST);
            }

        }
    }

    private GlHead getHeadAccount(String headName) {

        if (GlHead.Assets.toString().toUpperCase().equals(headName.toUpperCase())) {
            return GlHead.Assets;
        } else if (GlHead.Liabilities.toString().toUpperCase().equals(headName.toUpperCase())) {
            return GlHead.Liabilities;
        } else if (GlHead.Incomes.toString().toUpperCase().equals(headName.toUpperCase())) {
            return GlHead.Incomes;
        } else if (GlHead.Expenditures.toString().toUpperCase().equals(headName.toUpperCase())) {
            return GlHead.Expenditures;
        } else if (GlHead.Equity.toString().toUpperCase().equals(headName.toUpperCase())) {
            return GlHead.Equity;
        }

        return null;
    }

    private YesNo getYesNo(String yesNo) {

        if (YesNo.Yes.toString().toUpperCase().equals(yesNo.toUpperCase())) {
            return YesNo.Yes;
        }

        return YesNo.No;
    }

    private void gLGroup(XSSFSheet firstSheet, CompanyMaster companyMaster) {

        List<GeneralLedgerGroup> entityList = new ArrayList<>();

        for (int i = 1; i <= firstSheet.getLastRowNum(); i++) {

            firstSheet.getRow(i).getCell(1).setCellType(Cell.CELL_TYPE_STRING);
            String code = firstSheet.getRow(i).getCell(1).getStringCellValue().trim().toUpperCase();
            GeneralLedgerGroup tmpObj = glGroupRepo.findByGlGroupCode(code);

            if (tmpObj == null) {

                GeneralLedgerGroup entity = new GeneralLedgerGroup();
                entity.setGlHead(getHeadAccount(firstSheet.getRow(i).getCell(0).getStringCellValue().trim()));
                entity.setCompanyMaster(companyMaster);
                entity.setCompanyCode(companyMaster.getCompanyCode());
                entity.setGlGroupCode(code);
                entity.setGlGroupName(firstSheet.getRow(i).getCell(2).getStringCellValue().trim());

                entityList.add(entity);
            }
            if (entityList.size() == 50) {
                glGroupRepo.save(entityList);
                glGroupRepo.flush();
                entityList = new ArrayList<>();
            }
        }
        if (entityList.size() != 0) {
            glGroupRepo.save(entityList);
        }
    }

    private void gLSubGroup(XSSFSheet firstSheet, CompanyMaster companyMaster) {

        Map<String, GeneralLedgerGroup> groupMap = new HashMap<>();

        List<GeneralLedgerSubGroup> entityList = new ArrayList<>();

        for (int i = 1; i <= firstSheet.getLastRowNum(); i++) {

            firstSheet.getRow(i).getCell(1).setCellType(Cell.CELL_TYPE_STRING);
            String gCode = firstSheet.getRow(i).getCell(1).getStringCellValue().trim().toUpperCase();
            GeneralLedgerGroup group = glGroupRepo.findByGlGroupCode(gCode);
            if (group != null && groupMap.get(group.getGlGroupCode()) == null) {
                groupMap.put(group.getGlGroupCode(), group);
            }

            firstSheet.getRow(i).getCell(2).setCellType(Cell.CELL_TYPE_STRING);
            String code = firstSheet.getRow(i).getCell(2).getStringCellValue().trim().toUpperCase();
            GeneralLedgerSubGroup tmpObj = glSubGroupRepo.findByGlSubGroupCode(code);

            if (tmpObj == null) {

                GeneralLedgerSubGroup entity = new GeneralLedgerSubGroup();
                entity.setGlHead(getHeadAccount(firstSheet.getRow(i).getCell(0).getStringCellValue().trim()));
                entity.setCompanyMaster(companyMaster);
                entity.setCompanyCode(companyMaster.getCompanyCode());
                if (groupMap.get(gCode) != null) {
                    entity.setGlGroup(groupMap.get(gCode));
                    entity.setGlGroupCode(groupMap.get(gCode).getGlGroupCode());
                }
                entity.setGlSubGroupCode(code);
                entity.setGlSubGroupName(firstSheet.getRow(i).getCell(3).getStringCellValue().trim());

                entityList.add(entity);
            }
            if (entityList.size() == 50) {
                glSubGroupRepo.save(entityList);
                glSubGroupRepo.flush();
                entityList = new ArrayList<>();
            }
        }
        if (entityList.size() != 0) {
            glSubGroupRepo.save(entityList);
        }
    }

    private void gLMaster(XSSFSheet firstSheet, CompanyMaster companyMaster) {

        List<GeneralLedgerMaster> entityList = new ArrayList<>();

        for (int i = 1; i <= firstSheet.getLastRowNum(); i++) {

            firstSheet.getRow(i).getCell(0).setCellType(Cell.CELL_TYPE_STRING);
            String code = firstSheet.getRow(i).getCell(0).getStringCellValue().trim().toUpperCase();
            GeneralLedgerMaster tmpObj = glMasterRepo.findByGlCode(code);

            if (tmpObj == null) {

                GeneralLedgerMaster entity = new GeneralLedgerMaster();
                entity.setCompanyMaster(companyMaster);
                entity.setCompanyCode(companyMaster.getCompanyCode());
                entity.setGlCode(code);
                entity.setGlName(firstSheet.getRow(i).getCell(1).getStringCellValue().trim());

                entityList.add(entity);
            }
            if (entityList.size() == 50) {
                glMasterRepo.save(entityList);
                glMasterRepo.flush();
                entityList = new ArrayList<>();
            }
        }
        if (entityList.size() != 0) {
            glMasterRepo.save(entityList);
        }
    }


    private void gLAccount(XSSFSheet firstSheet, CompanyMaster companyMaster) {

        Map<String, GeneralLedgerGroup> group = new HashMap<>();
        Map<String, GeneralLedgerSubGroup> sub = new HashMap<>();
        Map<String, GeneralLedgerMaster> master = new HashMap<>();

        for (int i = 1; i <= firstSheet.getLastRowNum(); i++) {

            GlHead glHead = getHeadAccount(firstSheet.getRow(i).getCell(0).getStringCellValue().trim());

            firstSheet.getRow(i).getCell(1).setCellType(Cell.CELL_TYPE_STRING);
            String gCode = firstSheet.getRow(i).getCell(1).getStringCellValue().trim().toUpperCase();
            GeneralLedgerGroup gObj = glGroupRepo.findByGlGroupCode(gCode);
            if (gObj != null && group.get(gObj.getGlGroupCode()) == null) {
                group.put(gObj.getGlGroupCode(), gObj);
            }

            String sCode = firstSheet.getRow(i).getCell(2).getStringCellValue().trim().toUpperCase();
            GeneralLedgerSubGroup sObj = glSubGroupRepo.findByGlSubGroupCode(sCode);
            if (sObj != null && sub.get(sObj.getGlSubGroupCode()) == null) {
                sub.put(sObj.getGlSubGroupCode(), sObj);
            }

            firstSheet.getRow(i).getCell(3).setCellType(Cell.CELL_TYPE_STRING);
            String mCode = firstSheet.getRow(i).getCell(3).getStringCellValue().trim().toUpperCase();
            GeneralLedgerMaster mObj = glMasterRepo.findByGlCode(mCode);
            if (mObj != null && master.get(mObj.getGlCode()) == null) {
                master.put(mObj.getGlCode(), mObj);
            }

            firstSheet.getRow(i).getCell(4).setCellType(Cell.CELL_TYPE_STRING);
            String accCode = firstSheet.getRow(i).getCell(4).getStringCellValue().trim().toUpperCase();
            GeneralLedgerAccount gAccount = glAccountRepo.findByGlAccountCode(accCode);

            if (gAccount != null) {
                continue;
            }

            if (glHead != null && group.get(gCode) != null
                    && sub.get(sCode) != null && master.get(mCode) != null) {

                GeneralLedger entity = glAccountRepo.getGeneralLedger(companyMaster.getId(), glHead, group.get(gCode).getId(), sub.get(sCode).getId());

                if (entity == null) {

                    entity = new GeneralLedger();
                    entity.setCompanyMaster(companyMaster);
                    entity.setCompanyCode(companyMaster.getCompanyCode());
                    entity.setGlHead(glHead);
                    if (null != gObj) {
                        entity.setGlGroup(group.get(gObj.getGlGroupCode()));
                        entity.setGlGroupCode(group.get(gObj.getGlGroupCode()).getGlGroupCode());
                    }
                    if (null != sObj) {
                        entity.setGlSubGroup(sub.get(sObj.getGlSubGroupCode()));
                        entity.setGlSubGroupCode(sub.get(sObj.getGlSubGroupCode()).getGlSubGroupCode());
                    }
                }

                gAccount = new GeneralLedgerAccount();
                gAccount.setStatus(LovStatus.Active);
                gAccount.setCompanyMaster(companyMaster);
                gAccount.setCompanyCode(companyMaster.getCompanyCode());
                gAccount.setGlMaster(master.get(mCode));
                gAccount.setGlMasterCode(master.get(mCode).getGlCode());
                gAccount.setGlAccountCode(accCode);
                gAccount.setGlAccountName(firstSheet.getRow(i).getCell(5).getStringCellValue().trim());
                gAccount.setIsJobAccount(YesNo.No);
                gAccount.setIsBankAccount(getYesNo(firstSheet.getRow(i).getCell(6).getStringCellValue().trim()));
                gAccount.setIsSubLedger(getYesNo(firstSheet.getRow(i).getCell(7).getStringCellValue().trim()));

                if (gAccount.getIsSubLedger() == YesNo.No) {
                    gAccount.setGlSubLedgerCode("0000");
                }


                entity.getGeneralLedgerAccountList().add(gAccount);
                for (GeneralLedgerAccount gt : entity.getGeneralLedgerAccountList()) {
                    gt.setGeneralLedger(entity);
                }


                glAccountRepo.save(entity);
            }

        }
    }

    public void loadGLData(BaseDto baseDto, CompanyMaster companyMaster) {


        try (XSSFWorkbook workbook = new XSSFWorkbook(
                new FileInputStream(new File(appResourcePath + "/efs-master-data/gl-master.xlsx")))) {


            gLGroup(workbook.getSheetAt(0), companyMaster);

            gLSubGroup(workbook.getSheetAt(1), companyMaster);

            gLMaster(workbook.getSheetAt(2), companyMaster);

            gLAccount(workbook.getSheetAt(3), companyMaster);

            workbook.close();


        } catch (RestException exception) {

            log.error("->Exception in Create method : ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (ObjectOptimisticLockingFailureException e) {

            log.error("Exception in  -> saveOrUpdate method ", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_LOCKED_RECORD);

        } catch (JpaObjectRetrievalFailureException e) {

            log.error("Exception in  -> saveOrUpdate method ", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_DELETED_RECORD);

        } catch (Exception exception) {
            log.error("Failed to save ..", exception);

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Exception Cause 1 ::: " + exceptionCause1);

            // General error condition
            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            if (exceptionCause1.contains("FK_")) {
                baseDto.setResponseCode(ErrorCode.SAVE_UPDATE_FK_RECORD);
            }

        }
    }

    public void mapGlCharge(BaseDto baseDto, LocationMaster locationMaster) {
        try {
            List<ServiceMaster> serviceList = serviceRepo.findAll();

            List<ChargeMaster> chargeList = chargeRepo.findAll();

            List<GeneralLedgerChargeMappingMaster> entityList = new ArrayList<>();

            if (serviceList != null && !serviceList.isEmpty() && chargeList != null && !chargeList.isEmpty()) {

                for (ServiceMaster serviceMaster : serviceList) {

                    for (ChargeMaster chargeMaster : chargeList) {
                        GeneralLedgerChargeMappingMaster glmap = new GeneralLedgerChargeMappingMaster();
                        glmap.setServiceMaster(serviceMaster);
                        glmap.setServiceCode(serviceMaster.getServiceCode());
                        glmap.setChargeMaster(chargeMaster);
                        glmap.setChargeCode(chargeMaster.getChargeCode());
                        glmap.setCurrencyMaster(locationMaster.getCurrencyMaster());
                        glmap.setStatus(LovStatus.Active);

                        entityList.add(glmap);

                        if (entityList.size() == 50) {
                            glChargeRepo.save(entityList);
                            glChargeRepo.flush();
                            entityList = new ArrayList<>();
                        }

                    }

                }
            }

            if (entityList.size() > 0) {
                glChargeRepo.save(entityList);
            }
        } catch (RestException exception) {

            log.error("->Exception in Create method : ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (Exception exception) {
            log.error("Failed to save ..", exception);

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Exception Cause 1 ::: " + exceptionCause1);

            // General error condition
            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            if (exceptionCause1.contains("FK_")) {
                baseDto.setResponseCode(ErrorCode.SAVE_UPDATE_FK_RECORD);
            }

        }

    }


    public void loadDynamicData(BaseDto baseDto) {


        try (XSSFWorkbook workbook = new XSSFWorkbook(
                new FileInputStream(new File(appResourcePath + "/efs-master-data/country-config.xlsx")))) {

            XSSFSheet firstSheet = workbook.getSheetAt(0);

            Map<String, CountryMaster> mapCountry = new HashMap<>();
            List<DynamicFields> entityList = new ArrayList<>();

            for (int i = 1; i <= firstSheet.getLastRowNum(); i++) {

                String Code = firstSheet.getRow(i).getCell(0).getStringCellValue().trim();
                DynamicFields tmpObj = dynamicFieldRepository.findByFieldId(Code);

                if (tmpObj == null) {

                    DynamicFields entity = new DynamicFields();

                    entity.setFieldId(Code);
                    entity.setFieldName(firstSheet.getRow(i).getCell(1).getStringCellValue().trim());
                    entity.setScreen(firstSheet.getRow(i).getCell(2).getStringCellValue().trim());
                    entity.setCountryMasterList(new ArrayList<>());

                    if (firstSheet.getRow(i).getCell(3).getStringCellValue() != null
                            && firstSheet.getRow(i).getCell(3).getStringCellValue().trim().length() != 0) {

                        String countrys[] = firstSheet.getRow(i).getCell(3).getStringCellValue().trim().split(",");

                        for (String countryCode : countrys) {
                            if (mapCountry.get(countryCode) == null) {
                                CountryMaster countryMaster = countryRepo.findByCountryCode(countryCode);
                                if (countryMaster != null) {
                                    mapCountry.put(countryCode, countryMaster);
                                }
                            }

                            if (mapCountry.get(countryCode) != null) {
                                entity.getCountryMasterList().add(mapCountry.get(countryCode));
                            }

                        }
                    }


                    entityList.add(entity);
                }
                if (entityList.size() == 50) {
                    dynamicFieldRepository.save(entityList);
                    dynamicFieldRepository.flush();
                    entityList = new ArrayList<>();
                }
            }
            if (entityList.size() != 0) {
                dynamicFieldRepository.save(entityList);
            }
            workbook.close();


        } catch (RestException exception) {

            log.error("->Exception in Create method : ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (Exception exception) {
            log.error("Failed to save ..", exception);

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Exception Cause 1 ::: " + exceptionCause1);

            // General error condition
            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            if (exceptionCause1.contains("FK_")) {
                baseDto.setResponseCode(ErrorCode.SAVE_UPDATE_FK_RECORD);
            }

            if (exceptionCause1.contains("UK_DYNAMIC_FIELD_NAME")) {
                baseDto.setResponseCode(ErrorCode.UK_DYNAMIC_FIELD_NAME);
            } else if (exceptionCause1.contains("UK_DYNAMIC_FIELD_ID")) {
                baseDto.setResponseCode(ErrorCode.UK_DYNAMIC_FIELD_ID);
            } else if (exceptionCause1.contains("UK_DFC_FIELD_COUNTRY")) {
                baseDto.setResponseCode(ErrorCode.COUNTRY_UK_DYNAMIC_FIELD);
            }
        }
    }
}
