package com.efreightsuite.service;

import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.DefaultMasterSearchDto;
import com.efreightsuite.dto.SearchRequest;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.ExceptionHelper;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.exception.UniqueKey;
import com.efreightsuite.model.DefaultMasterData;
import com.efreightsuite.repository.DefaultMasterDataRepository;
import com.efreightsuite.search.DefaultMasterDataSearchImpl;
import com.efreightsuite.util.AppUtil;
import com.efreightsuite.validation.DefaultMasterDataValidator;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.exception.ExceptionUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.orm.ObjectOptimisticLockingFailureException;
import org.springframework.orm.jpa.JpaObjectRetrievalFailureException;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class DefaultMasterDataService {

    @Autowired
    private
    AppUtil appUtil;

    @Autowired
    private
    DefaultMasterDataRepository defaultMasterDataRepository;

    @Autowired
    private
    DefaultMasterDataValidator defaultMasterDataValidator;

    @Autowired
    private
    DefaultMasterDataSearchImpl defaultMasterDataSearchImpl;

    public BaseDto getById(Long id) {

        BaseDto baseDto = new BaseDto();

        try {

            DefaultMasterData defaultMasterData = defaultMasterDataRepository.findById(id);

            AppUtil.setPartyToNull(defaultMasterData.getLocation().getPartyMaster());

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(defaultMasterData);

        } catch (RestException exception) {

            log.error("Exception in getById method : ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (Exception exception) {

            log.error("Exception in getById method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);
    }

    public BaseDto search(SearchRequest searchRequest) {

        BaseDto baseDto = new BaseDto();

        try {

            baseDto.setResponseObject(defaultMasterDataSearchImpl.search(searchRequest));

            baseDto.setResponseCode(ErrorCode.SUCCESS);

        } catch (Exception exception) {

            log.error("Exception in Search method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);

    }

    public BaseDto search(DefaultMasterSearchDto searchDto) {

        BaseDto baseDto = new BaseDto();

        try {

            baseDto.setResponseObject(defaultMasterDataSearchImpl.search(searchDto));
            baseDto.setResponseCode(ErrorCode.SUCCESS);
        } catch (Exception exception) {

            log.error("Exception in Search method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);

    }

    public BaseDto saveOrUpdate(DefaultMasterData defaultMasterData) {

        BaseDto baseDto = new BaseDto();

        try {

            log.info("saveOrUpdate method is Invoked....[ " + defaultMasterData.getId() + "]");

            defaultMasterDataValidator.validate(defaultMasterData);

            if (defaultMasterData.getId() != null) {
                defaultMasterDataRepository.getOne(defaultMasterData.getId());
            }

            defaultMasterData = defaultMasterDataRepository.save(defaultMasterData);

            log.info("Successfully Updated...[" + defaultMasterData.getId() + "]");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            AppUtil.setPartyToNull(defaultMasterData.getLocation().getPartyMaster());

            baseDto.setResponseObject(defaultMasterData);

        } catch (RestException exception) {

            log.error("Exception occured with error code ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (ObjectOptimisticLockingFailureException e) {

            log.error("Error in editing", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_LOCKED_RECORD);

        } catch (JpaObjectRetrievalFailureException e) {

            log.error("Error in editing", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_DELETED_RECORD);

        } catch (Exception exception) {

            log.error("Exception occured : ", exception);

            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Exception Cause 1 ::: ", exceptionCause1);

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            if (exceptionCause1.contains(UniqueKey.UK_DEFAULT_MASTER_DATA_CODE)) {
                baseDto.setResponseCode(ErrorCode.DEFAULTMASTER_CODE_ALREADY_EXIST);
            }

        }

        return appUtil.setDesc(baseDto);
    }

    public BaseDto delete(Long id) {

        BaseDto baseDto = new BaseDto();

        try {

            defaultMasterDataRepository.delete(id);

            baseDto.setResponseCode(ErrorCode.SUCCESS);

        } catch (DataIntegrityViolationException exception) {

            log.error("Exception occured : ", exception);

            ExceptionHelper helper = new ExceptionHelper(exception);

            log.error("Foreign key reference : " + helper.getFkReference());

            baseDto.setResponseCode(ErrorCode.ERROR_FK_CONSTRAINT);

        } catch (Exception exception) {

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            log.error("Exception in Delete method : ", exception);

            String rootCause = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Root Cause of Exception : " + rootCause);

            if (rootCause.contains("FK_")) {
                baseDto.setResponseCode(ErrorCode.CANNOT_DELETE_FK_RECORD);
            }
        }

        return appUtil.setDesc(baseDto);
    }

}
