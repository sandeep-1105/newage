package com.efreightsuite.service;

import com.efreightsuite.common.component.CacheRepository;
import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.CommentSearchDto;
import com.efreightsuite.dto.SearchRequest;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.ExceptionHelper;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.exception.UniqueKey;
import com.efreightsuite.model.CommentMaster;
import com.efreightsuite.repository.CommentMasterRepository;
import com.efreightsuite.search.CommentSearchImpl;
import com.efreightsuite.util.AppUtil;
import com.efreightsuite.util.SaaSUtil;
import com.efreightsuite.util.ValidateUtil;
import com.efreightsuite.validation.CommentMasterValidation;
import com.efreightsuite.validation.SearchDtoValidator;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.exception.ExceptionUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.orm.ObjectOptimisticLockingFailureException;
import org.springframework.orm.jpa.JpaObjectRetrievalFailureException;
import org.springframework.stereotype.Service;


@Service
@Log4j2
public class CommentMasterService {

    @Autowired
    private
    AppUtil appUtil;

    @Autowired
    private
    CommentSearchImpl commentSearchImpl;

    @Autowired
    private
    CommentMasterRepository commentMasterRepository;

    @Autowired
    SearchDtoValidator searchDtoValidator;

    @Autowired
    private
    CacheRepository cacheRepository;

    @Autowired
    private
    CommentMasterValidation commentMasterValidation;

    public BaseDto search(SearchRequest searchRequest) {

        log.info("Search method is called....");

        BaseDto baseDto = new BaseDto();

        try {

            baseDto.setResponseObject(commentSearchImpl.search(searchRequest));

            baseDto.setResponseCode(ErrorCode.SUCCESS);

        } catch (Exception exception) {

            log.error("Exception in Search method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);
    }

    /**
     * Method - getByCommentMasterId Returns CommentMaster based on given Comment id
     *
     * @return BaseDto
     */

    public BaseDto getByCommentId(Long id) {

        log.info("getByCommentId method is called...[" + id + "]");

        final BaseDto baseDto = new BaseDto();

        try {

            CommentMaster commentMaster = cacheRepository.get(SaaSUtil.getSaaSId(), CommentMaster.class.getName(), id,
                    commentMasterRepository);

            ValidateUtil.notNull(commentMaster, ErrorCode.COMMENT_ID_NOT_FOUND);

            log.info("getByCommentId is successfully executed....[" + commentMaster.getId() + "]");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(commentMaster);

        } catch (final RestException exception) {

            log.error("Exception in getByCommentId method : ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (final Exception exception) {

            log.error("Exception in getByCommentId method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);
    }

    /**
     * Method - search Returns list of CommentMaster based on the search keyword
     *
     * @return BaseDto
     */

    public BaseDto search(CommentSearchDto searchDto) {

        log.info("Search method is called...." + searchDto);

        BaseDto baseDto = new BaseDto();

        try {

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(commentSearchImpl.search(searchDto));

            log.info("Successfully Searching Comment...");

        } catch (Exception exception) {

            log.error("Exception in Search method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);

    }

    /**
     * Method - create Creates and returns a new Comment master
     *
     * @returns CommentMaster
     */

    public BaseDto create(CommentMaster commentMaster) {

        log.info("Create method is called - commentMasterId ..");

        final BaseDto baseDto = new BaseDto();

        try {
            commentMasterValidation.validate(commentMaster);

            cacheRepository.save(SaaSUtil.getSaaSId(), CommentMaster.class.getName(), commentMaster, commentMasterRepository);

            log.info("Comment Saved successfully.....");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(commentMaster);

        } catch (RestException exception) {

            log.error("Exception Cause 1 ::: ", exception);
            baseDto.setResponseCode(exception.getMessage());

        } catch (Exception exception) {

            log.error("Exception occured : ", exception);

            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Exception Cause 1 : " + exceptionCause1);

            // General error condition
            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            // Duplicate Comment Code

            if (exceptionCause1.contains(UniqueKey.UK_COMMENT_COMMENTCODE)) {
                baseDto.setResponseCode(ErrorCode.COMMENT_CODE_ALREADY_EXIST);
            }
            if (exceptionCause1.contains(UniqueKey.UK_COMMENT_COMMENTNAME)) {
                baseDto.setResponseCode(ErrorCode.COMMENT_NAME_ALREADY_EXIST);
            }
        }

        return appUtil.setDesc(baseDto);
    }

    /**
     * Method - update Updates and returns a updated comment master
     *
     * @param existingCommentMaster
     * @returns CommentMaster
     */

    public BaseDto update(CommentMaster existingCommentMaster) {

        log.info("Update method is called - existingCommentMaster : [" + existingCommentMaster.getId() + "]");

        BaseDto baseDto = new BaseDto();

        try {

            ValidateUtil.notNull(existingCommentMaster.getId(), ErrorCode.COMMENT_ID_NOT_NULL);

            commentMasterValidation.validate(existingCommentMaster);

            commentMasterRepository.getOne(existingCommentMaster.getId());

            existingCommentMaster = cacheRepository.save(SaaSUtil.getSaaSId(), CommentMaster.class.getName(), existingCommentMaster,
                    commentMasterRepository);

            log.info("CommentMaster updated successfully....[" + existingCommentMaster.getId() + "]");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(existingCommentMaster);

        } catch (RestException exception) {

            log.error("Exception occured with error code ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (ObjectOptimisticLockingFailureException e) {

            log.error("Error in editing", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_LOCKED_RECORD);

        } catch (JpaObjectRetrievalFailureException e) {

            log.error("Error in editing", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_DELETED_RECORD);

        } catch (Exception exception) {

            log.error("Exception occured : ", exception);

            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Exception Cause 1 ::: " + exceptionCause1);

            // General error condition
            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            // Duplicate comment Code

            if (exceptionCause1.contains(UniqueKey.UK_COMMENT_COMMENTCODE)) {
                baseDto.setResponseCode(ErrorCode.COMMENT_CODE_ALREADY_EXIST);
            }
            if (exceptionCause1.contains(UniqueKey.UK_COMMENT_COMMENTNAME)) {
                baseDto.setResponseCode(ErrorCode.COMMENT_NAME_ALREADY_EXIST);
            }
        }
        return appUtil.setDesc(baseDto);
    }

    /**
     * Deletes the Existing CommentMaster Based on the Given Id
     *
     * @Returns BaseDto
     */
    public BaseDto delete(Long id) {

        log.info("Delete method is Invoked...[" + id + "]");

        final BaseDto baseDto = new BaseDto();

        try {

            cacheRepository.delete(SaaSUtil.getSaaSId(), CommentMaster.class.getName(), id, commentMasterRepository);

            log.info("CommentMaster Deleted Successfully....[" + id + "]");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

        } catch (RestException exception) {

            log.error("Exception in Delete method : ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (DataIntegrityViolationException exception) {

            log.error("Exception occured : ", exception);

            ExceptionHelper helper = new ExceptionHelper(exception);

            log.error("Foreign key reference : " + helper.getFkReference());

            baseDto.setResponseCode(ErrorCode.ERROR_FK_CONSTRAINT);

        } catch (Exception exception) {

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            log.error("Exception in Delete method : ", exception);

            String rootCause = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Root Cause of Exception : " + rootCause);

            if (rootCause.contains("FK_")) {
                baseDto.setResponseCode(ErrorCode.CANNOT_DELETE_FK_RECORD);
            }
        }

        return appUtil.setDesc(baseDto);
    }

}


