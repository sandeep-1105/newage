package com.efreightsuite.service;

import java.util.List;

import com.efreightsuite.common.component.CacheRepository;
import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.GroupSubGroupDto;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.ExceptionHelper;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.exception.UniqueKey;
import com.efreightsuite.model.GeneralLedgerSubGroup;
import com.efreightsuite.repository.SubGroupMasterRepository;
import com.efreightsuite.search.GroupAndSubGroupSeacrhImpl;
import com.efreightsuite.util.AppUtil;
import com.efreightsuite.util.SaaSUtil;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.exception.ExceptionUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.orm.ObjectOptimisticLockingFailureException;
import org.springframework.orm.jpa.JpaObjectRetrievalFailureException;
import org.springframework.stereotype.Service;


@Service
@Log4j2
public class SubGroupMasterService {


    @Autowired
    private
    AppUtil appUtil;

    @Autowired
    private
    SubGroupMasterRepository subGroupMasterRepository;

    @Autowired
    private
    CacheRepository cacheRepository;

    @Autowired
    private
    GroupAndSubGroupSeacrhImpl groupAndSubGroupSeacrhImpl;

    public BaseDto search(GroupSubGroupDto searchDto) {

        log.info("Search method is called...." + searchDto);

        BaseDto baseDto = new BaseDto();

        try {

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(groupAndSubGroupSeacrhImpl.searchSubGroupLov(searchDto));

            log.info("Successfully Searching Subgroup...");

        } catch (Exception exception) {

            log.error("Exception in Search method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);
    }

    public BaseDto get(Long id) {
        log.info("get method is called.....[" + id + "]");

        BaseDto baseDto = new BaseDto();

        try {

            GeneralLedgerSubGroup subGroupMaster = cacheRepository.get(SaaSUtil.getSaaSId(), GeneralLedgerSubGroup.class.getName(), id, subGroupMasterRepository);

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(subGroupMaster);

        } catch (RestException ex) {

            log.error("Exception in get method ", ex);

            baseDto.setResponseCode(ex.getMessage());

        } catch (Exception e) {

            log.error("Exception in get method ", e);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);
    }

    public BaseDto create(List<GeneralLedgerSubGroup> subGroupMaster) {
        log.info("Create method is called.....");

        BaseDto baseDto = new BaseDto();

        try {

            subGroupMasterRepository.save(subGroupMaster);

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(subGroupMaster.size());

        } catch (RestException exception) {

            log.error("BadRequestException occured with error code ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (Exception exception) {

            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Exception Cause 1 ::: " + exceptionCause1);

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            if (exceptionCause1.contains(UniqueKey.UK_SGRP_CODE)) {
                baseDto.setResponseCode(ErrorCode.SUBGROUP_CODE_ALREADY_EXIST);
            }

            if (exceptionCause1.contains(UniqueKey.UK_SGRP_NAME)) {
                baseDto.setResponseCode(ErrorCode.SUBGROUP_NAME_ALREADY_EXIST);
            }
        }
        return appUtil.setDesc(baseDto);
    }

    public BaseDto update(List<GeneralLedgerSubGroup> existingSubGroupMaster) {

        BaseDto baseDto = new BaseDto();

        try {

            subGroupMasterRepository.save(existingSubGroupMaster);

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(existingSubGroupMaster);

        } catch (RestException exception) {

            log.error("Exception occured in update method ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (ObjectOptimisticLockingFailureException e) {

            log.error("Optimistic Lock Exception occured ", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_LOCKED_RECORD);

        } catch (JpaObjectRetrievalFailureException e) {

            log.error("Object Already Deleted................", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_DELETED_RECORD);

        } catch (Exception exception) {

            log.error("Exception occurred " + exception);

            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Exception Cause 1 ::: " + exceptionCause1);

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            if (exceptionCause1.contains(UniqueKey.UK_SGRP_CODE)) {
                baseDto.setResponseCode(ErrorCode.SUBGROUP_CODE_ALREADY_EXIST);
            }

            if (exceptionCause1.contains(UniqueKey.UK_SGRP_NAME)) {
                baseDto.setResponseCode(ErrorCode.SUBGROUP_NAME_ALREADY_EXIST);
            }
        }

        return appUtil.setDesc(baseDto);
    }

    public BaseDto delete(Long id) {
        log.info("Delete method is Invoked...[" + id + "]");

        BaseDto baseDto = new BaseDto();

        try {

            cacheRepository.delete(SaaSUtil.getSaaSId(), GeneralLedgerSubGroup.class.getName(), id, subGroupMasterRepository);

            log.info("SubGroupMaster Deleted Successfully....[" + id + "]");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

        } catch (DataIntegrityViolationException exception) {

            log.error("Exception occured : ", exception);

            ExceptionHelper helper = new ExceptionHelper(exception);

            log.error("Foreign key reference : " + helper.getFkReference());

            baseDto.setResponseCode(ErrorCode.ERROR_FK_CONSTRAINT);

        } catch (Exception exception) {

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            log.error("Exception in Delete method : ", exception);

            String rootCause = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Root Cause of Exception : " + rootCause);

            if (rootCause.contains("FK_")) {
                baseDto.setResponseCode(ErrorCode.CANNOT_DELETE_FK_RECORD);
            }
        }

        return appUtil.setDesc(baseDto);
    }


}
