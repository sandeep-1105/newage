package com.efreightsuite.service;

import java.util.List;

import com.efreightsuite.common.component.CacheRepository;
import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.ExceptionHelper;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.model.PartyEmailMapping;
import com.efreightsuite.repository.PartyEmailMappingRepository;
import com.efreightsuite.repository.PartyEmailMessageMappingRepository;
import com.efreightsuite.util.AppUtil;
import com.efreightsuite.util.SaaSUtil;
import com.efreightsuite.util.ValidateUtil;
import com.efreightsuite.validation.PartyEmailMappingValidator;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.exception.ExceptionUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.orm.ObjectOptimisticLockingFailureException;
import org.springframework.orm.jpa.JpaObjectRetrievalFailureException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author Devendrachary M
 */
@Service
@Log4j2
public class PartyEmailMappingService {

    @Autowired
    private
    PartyEmailMappingRepository partyEmailMappingRepository;

    @Autowired
    private
    AppUtil appUtil;

    @Autowired
    private
    CacheRepository cacheRepository;

    @Autowired
    private
    PartyEmailMappingValidator partyEmailMappingValidator;

    @Autowired
    SequenceGeneratorService sequenceGeneratorService;

    @Autowired
    PartyEmailMessageMappingRepository partyEmailMessageMappingRepository;

    /**
     * Method - get
     *
     * @param id
     * @return baseDto
     */

    public BaseDto get(Long id) {
        log.info("PartyEmailMappingService.get method started.");

        BaseDto baseDto = new BaseDto();
        try {

            ValidateUtil.notNull(id, ErrorCode.AUTOMAIL_ID_NOT_NULL);

            PartyEmailMapping partyEmailMapping = partyEmailMappingRepository.findById(id);

            log.info("Successfully getting PartyEmailMapping by id...");

            baseDto.setResponseCode(ErrorCode.SUCCESS);
            baseDto.setResponseObject(partyEmailMapping);

        } catch (RestException re) {
            log.error(
                    "RestException in PartyEmailMappingService.get method while getting the PartyEmailMapping : " + re);
            baseDto.setResponseCode(ErrorCode.AUTOMAIL_ID_INVALID);
        } catch (Exception e) {
            log.error("Exception in PartyEmailMappingService.get method while getting the PartyEmailMapping : ", e);
            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        log.info("PartyEmailMappingService.get method end.");
        return appUtil.setDesc(baseDto);
    }

    /*
     * create - This method creates a new PartyEmailMapping
     *
     * @Param PartyEmailMapping master entity
     *
     * @Return BaseDto
     */
    @Transactional
    public BaseDto create(PartyEmailMapping partyEmailMapping) {

        log.info("PartyEmailMappingService.create method is started.");
        BaseDto baseDto = new BaseDto();
        try {

            ValidateUtil.notNull(partyEmailMapping, ErrorCode.AUTOMAIL_NOT_NULL);

            partyEmailMappingValidator.validate(partyEmailMapping);

            PartyEmailMapping partyEmailMappingRes = cacheRepository.save(SaaSUtil.getSaaSId(),
                    PartyEmailMapping.class.getName(), partyEmailMapping, partyEmailMappingRepository);

            log.info("PartyEmailMapping Saved successfully.....");
            baseDto.setResponseCode(ErrorCode.SUCCESS);
            baseDto.setResponseObject(partyEmailMappingRes);

        } catch (RestException re) {

            log.error("Exception in PartyEmailMappingService.create ", re);

            baseDto.setResponseCode(re.getMessage());
        } catch (Exception exception) {

            log.error("Failed to save the Party Email Mapping..", exception);

            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Exception Cause 1 ::: " + exceptionCause1);

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

        }

        log.info("PartyEmailMappingService.create method is ended.");
        return appUtil.setDesc(baseDto);
    }

    public BaseDto update(PartyEmailMapping existingPartyEmailMapping) {
        log.info("PartyEmailMappingService.update method is started.");
        BaseDto baseDto = new BaseDto();

        try {

            ValidateUtil.notNull(existingPartyEmailMapping, ErrorCode.AUTOMAIL_NOT_NULL);

            partyEmailMappingValidator.validate(existingPartyEmailMapping);

            partyEmailMappingRepository.getOne(existingPartyEmailMapping.getId());

            PartyEmailMapping partyEmailMappingUpdateResponse = cacheRepository.save(SaaSUtil.getSaaSId(),
                    PartyEmailMapping.class.getName(), existingPartyEmailMapping, partyEmailMappingRepository);

            log.info("Service  updated successfully...[" + partyEmailMappingUpdateResponse.getId() + "]");
            log.info("PartyEmailMappingService updated successfully...");

            baseDto.setResponseCode(ErrorCode.SUCCESS);
            baseDto.setResponseObject(partyEmailMappingUpdateResponse);

        } catch (RestException exception) {

            log.error("Exception in update method : ", exception);

            baseDto.setResponseCode(exception.getMessage());
        } catch (ObjectOptimisticLockingFailureException e) {
            log.error("Error in editing Service", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_LOCKED_RECORD);
        } catch (JpaObjectRetrievalFailureException e) {
            log.error("Error in editing Service", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_DELETED_RECORD);
        } catch (Exception exception) {

            log.error("Failed to update the Party Email Mapping..", exception);

            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Exception Cause 1 ::: " + exceptionCause1);

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

        }

        log.info("PartyEmailMappingService.update method is ended.");
        return appUtil.setDesc(baseDto);

    }

    /*
     * Deletes the Existing PartyEmailMapping Based on the Given Id
     *
     * @param Id
     *
     * @Returns BaseDto
     */
    public BaseDto delete(Long id) {
        log.info("PartyEmailMappingService.delete method is started.[" + id + "]");

        BaseDto baseDto = new BaseDto();

        try {

            ValidateUtil.notNull(id, ErrorCode.AUTOMAIL_ID_NOT_NULL);

            cacheRepository.delete(SaaSUtil.getSaaSId(), PartyEmailMapping.class.getName(), id,
                    partyEmailMappingRepository);

            log.info("PartyEmailMapping Deleted Successfully....[" + id + "]");

            baseDto.setResponseCode(ErrorCode.SUCCESS);
            return appUtil.setDesc(baseDto);

        } catch (RestException exception) {

            log.error("RestException in Delete method : ", exception);

            baseDto.setResponseCode(exception.getMessage());
        } catch (DataIntegrityViolationException exception) {

            log.error("Exception occured : ", exception);

            ExceptionHelper helper = new ExceptionHelper(exception);

            log.error("Foreign key reference : " + helper.getFkReference());

            baseDto.setResponseCode(ErrorCode.ERROR_FK_CONSTRAINT);
        } catch (Exception exception) {

            log.error("Exception in Delete method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }
        log.info("PartyEmailMappingService.delete method is ended");
        return appUtil.setDesc(baseDto);

    }

    /*
     * Gives the all PartyEmailMapping
     * @Returns List<PartyEmailMessageMapping>
     */
    public BaseDto getAll() {

        log.info("PartyEmailMappingService.getAll method is started");

        BaseDto baseDto = new BaseDto();

        try {

            List<PartyEmailMapping> partyEmailMappingList = partyEmailMappingRepository.findAll();
            log.info("Successfully getting list of partyEmailMapping...");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(partyEmailMappingList);

        } catch (RestException exception) {

            log.error("Exception in getAll method ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (Exception exception) {

            log.error("Exception in getAll method ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }
        log.info("PartyEmailMappingService.getAll method is ended");
        return appUtil.setDesc(baseDto);
    }

}
