package com.efreightsuite.service;

import com.efreightsuite.common.component.CacheRepository;
import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.CategorySearchDto;
import com.efreightsuite.dto.SearchRequest;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.exception.UniqueKey;
import com.efreightsuite.model.CategoryMaster;
import com.efreightsuite.repository.CategoryMasterRepository;
import com.efreightsuite.search.CategoryMasterSearchImpl;
import com.efreightsuite.util.AppUtil;
import com.efreightsuite.util.SaaSUtil;
import com.efreightsuite.util.ValidateUtil;
import com.efreightsuite.validation.CategoryMasterValidator;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.exception.ExceptionUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.ObjectOptimisticLockingFailureException;
import org.springframework.orm.jpa.JpaObjectRetrievalFailureException;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class CategoryMasterService {

    @Autowired
    private
    AppUtil appUtil;

    @Autowired
    private
    CategoryMasterRepository categoryMasterRepository;

    @Autowired
    private
    CategoryMasterValidator categoryMasterValidator;

    @Autowired
    private
    CacheRepository cacheRepository;

    @Autowired
    private
    CategoryMasterSearchImpl categoryMasterSearchImpl;

    public BaseDto getByCategoryId(Long id) {

        log.info("getByCategoryId method is called [" + id + "]");

        BaseDto baseDto = new BaseDto();

        try {

            ValidateUtil.notNull(id, ErrorCode.CATEGORY_ID_NOT_NULL);

            CategoryMaster categoryMaster = cacheRepository.get(SaaSUtil.getSaaSId(), CategoryMaster.class.getName(),
                    id, categoryMasterRepository);

            ValidateUtil.notNull(categoryMaster, ErrorCode.CATEGORY_ID_NOT_FOUND);

            log.info("Successfully getting Category By Given Id.....[" + categoryMaster.getId() + "]");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(categoryMaster);

        } catch (RestException exception) {

            log.error("Exception in getByCategoryId method...", exception);

            baseDto.setResponseCode(exception.getMessage());
        } catch (Exception exception) {

            log.error("Exception in getByCategoryId method", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);

    }


    public BaseDto create(CategoryMaster categoryMaster) {

        log.info("Create method is called....");

        BaseDto baseDto = new BaseDto();

        try {

            categoryMasterValidator.validate(categoryMaster);

            categoryMaster = cacheRepository.save(SaaSUtil.getSaaSId(), CategoryMaster.class.getName(), categoryMaster,
                    categoryMasterRepository);

            log.info("CategoryMaster Saved successfully.....[" + categoryMaster.getId() + "]");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(categoryMaster);

        } catch (RestException exception) {

            log.error("RestException occured with error code ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (Exception e) {

            log.error("Exception in Update method : ", e);

            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(e);

            log.error("Exception Cause 1 ::: " + exceptionCause1);

            // General error condition
            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            if (exceptionCause1.contains(UniqueKey.UK_CATEGORY_CATEGORYCODE)) {
                baseDto.setResponseCode(ErrorCode.CATEGORY_CODE_ALREADY_EXIST);
            }

            if (exceptionCause1.contains(UniqueKey.UK_CATEGORY_CATEGORYNAME)) {
                baseDto.setResponseCode(ErrorCode.CATEGORY_NAME_ALREADY_EXIST);
            }

        }

        return appUtil.setDesc(baseDto);
    }

    /**
     * update - This method updates the Existing Category
     *
     * @Param ExistingCategoryMaster entity
     * @Return BaseDto
     */

    public BaseDto update(CategoryMaster existingCategoryMaster) {

        log.info("Update method is called....[" + existingCategoryMaster.getId() + "]");

        BaseDto baseDto = new BaseDto();

        try {

            categoryMasterValidator.validate(existingCategoryMaster);

            categoryMasterRepository.getOne(existingCategoryMaster.getId());

            existingCategoryMaster = cacheRepository.save(SaaSUtil.getSaaSId(), CategoryMaster.class.getName(),
                    existingCategoryMaster, categoryMasterRepository);

            log.info("Category Updated successfully.....[" + existingCategoryMaster.getId() + "]");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(existingCategoryMaster);

        } catch (RestException exception) {

            log.error("RestException occured with error code ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (ObjectOptimisticLockingFailureException e) {

            log.error("Error in editing Service", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_LOCKED_RECORD);

        } catch (JpaObjectRetrievalFailureException e) {

            log.error("Error in editing Service", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_DELETED_RECORD);

        } catch (Exception e) {

            log.error("Exception in Update method : ", e);

            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(e);

            log.error("Exception Cause 1 ::: " + exceptionCause1);

            // General error condition

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            if (exceptionCause1.contains(UniqueKey.UK_CATEGORY_CATEGORYCODE)) {
                baseDto.setResponseCode(ErrorCode.CATEGORY_CODE_ALREADY_EXIST);
            }

            if (exceptionCause1.contains(UniqueKey.UK_CATEGORY_CATEGORYNAME)) {
                baseDto.setResponseCode(ErrorCode.CATEGORY_NAME_ALREADY_EXIST);
            }

        }

        return appUtil.setDesc(baseDto);
    }

    /**
     * Deletes the Existing Category Based on the Given Id
     *
     * @Returns BaseDto
     */
    public BaseDto delete(Long id) {

        log.info("Delete method is Invoked...[" + id + "]");

        BaseDto baseDto = new BaseDto();

        try {

            ValidateUtil.notNull(id, ErrorCode.CATEGORY_ID_NOT_NULL);

            cacheRepository.delete(SaaSUtil.getSaaSId(), CategoryMaster.class.getName(), id, categoryMasterRepository);

            log.info("CategoryMaster Deleted Successfully....[" + id + "]");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

        } catch (RestException exception) {

            log.error("Exception in Delete method : ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (Exception e) {

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            log.error("Exception in Delete method : ", e);

            String rootCause = ExceptionUtils.getRootCauseMessage(e);

            log.error("Root Cause of Exception : " + rootCause);

            if (rootCause.contains("FK_")) {
                baseDto.setResponseCode(ErrorCode.CANNOT_DELETE_FK_RECORD);
            }

        }

        return appUtil.setDesc(baseDto);
    }

    public BaseDto search(CategorySearchDto searchDto) {

        log.info("Search method is called...." + searchDto);

        BaseDto baseDto = new BaseDto();

        try {

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(categoryMasterSearchImpl.search(searchDto));

            log.info("Successfully Searching .");

        } catch (Exception exception) {

            log.error("Exception in Search method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);

    }

    public BaseDto search(SearchRequest searchRequest) {

        log.info("Search method is called...." + searchRequest);

        BaseDto baseDto = new BaseDto();

        try {

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(categoryMasterSearchImpl.search(searchRequest));

            log.info("Successfully Searching .");

        } catch (Exception exception) {

            log.error("Exception in Search method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);

    }

}
