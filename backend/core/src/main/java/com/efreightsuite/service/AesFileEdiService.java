package com.efreightsuite.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import com.efreightsuite.communication.RabbitMQMessageSender;
import com.efreightsuite.dto.AesFileSubmissionResponseDto;
import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.FieldErrorDto;
import com.efreightsuite.dto.MessagingDto;
import com.efreightsuite.enumeration.EdiMessagingFor;
import com.efreightsuite.enumeration.IdType;
import com.efreightsuite.enumeration.OriginOfGoods;
import com.efreightsuite.enumeration.YesNo;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.model.AesCommodity;
import com.efreightsuite.model.AesFile;
import com.efreightsuite.model.AesFilerMaster;
import com.efreightsuite.model.AesUsPortMaster;
import com.efreightsuite.model.AesUsZipCodeMaster;
import com.efreightsuite.model.DtdcVehicle;
import com.efreightsuite.model.UserProfile;
import com.efreightsuite.repository.AesFileRepository;
import com.efreightsuite.repository.AesFilerMasterRepository;
import com.efreightsuite.repository.AesUsPortMasterRepository;
import com.efreightsuite.repository.AesUsZipCodeRepository;
import com.efreightsuite.util.AesUtil;
import com.efreightsuite.util.SaaSUtil;
import com.efreightsuite.util.TimeUtil;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.exception.ExceptionUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class AesFileEdiService {

    @Autowired
    private
    AesFileRepository aesFileRepository;

    @Autowired
    private
    RabbitMQMessageSender rabbitMQMessageSender;

    @Autowired
    private
    AesFilerMasterRepository aesFilerMasterRepository;

    @Autowired
    private
    AesUsZipCodeRepository aesUsZipCodeRepository;


    @Autowired
    private
    AesUsPortMasterRepository aesUsPortMasterRepository;

    public BaseDto save(Long aesFileId) {

        log.info("AesFileEdiService -> save is called for the AES ID : [" + aesFileId + "]");

        BaseDto baseDto = new BaseDto();

        try {


            AesFile aesFile = aesFileRepository.findById(aesFileId);
            UserProfile loginUser = AuthService.getCurrentUser();
            if (aesFile != null && aesFile.getId() != null && loginUser != null && loginUser.getId() != null) {
                AesFileSubmissionResponseDto aesMsgResponse = generateMessage(aesFile);
                if (aesMsgResponse.isError()) {
                    baseDto.setResponseCode(ErrorCode.AES_GENERATION_FAILED);
                    baseDto.setResponseObject(aesMsgResponse);
                } else {
                    aesFile.setAesQueueMessage(aesMsgResponse.getAesMsg());
                    aesFile.setAesStatus("Message Generated");
                    aesFile = aesFileRepository.save(aesFile);
                    MessagingDto data = new MessagingDto();
                    data.setEdiId(aesFileId);
                    data.setMessagingFor(EdiMessagingFor.AES);
                    data.setSelectedCompanyId(loginUser.getSelectedUserLocation().getCompanyMaster().getId());
                    data.setSelectedLocationId(loginUser.getSelectedUserLocation().getCompanyMaster().getId());
                    data.setSelectedCountryId(loginUser.getSelectedUserLocation().getCountryMaster().getId());
                    rabbitMQMessageSender.sendtoMQ(data, SaaSUtil.getSaaSId());

                    //data.setUserProfile(AuthService.getCurrentUser());
                    //data.setObject(aesFileEdi);
                    //rabbitMqSendMessage.sendtoMQ(false, data, SaaSUtil.getSaaSId());

                    baseDto.setResponseCode(ErrorCode.SUCCESS);
                }
            } else {
                baseDto.setResponseCode(ErrorCode.UNAUTHORIZED_USER);
            }


        } catch (Exception exception) {

            log.error("Exception in saveEdi method ", exception);

            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Exception Cause 1 ::: " + exceptionCause1);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return baseDto;

    }


    private AesFileSubmissionResponseDto generateMessage(AesFile aesFile) {
        AesFileSubmissionResponseDto aesFileSubmissionResponse = new AesFileSubmissionResponseDto();

        List<FieldErrorDto> fieldErrorList = new ArrayList<>();
        aesFileSubmissionResponse.setAesId(aesFile.getId());
        aesFileSubmissionResponse.setError(false);

        UserProfile loginUser = AuthService.getCurrentUser();
        StringBuilder lv_mq = new StringBuilder();
        String lv_msg = "";
        String lv_qty1 = null;
        String lv_qty2 = null;

        String lv_transmitter_date = AesUtil.getCurrentDate(TimeUtil.getCurrentLocationTime());
        // substr(:aes_job_detail.batch_no,1,6);
        String lv_batch_no = AesUtil.substr(aesFile.getBatchNo(), 1, 6);
        // substr(:aes_job_detail.transport_mode,1,2);
        String lv_mot = AesUtil.modAesTransportMode(aesFile.getAesTransportMode());
        // TODO Need to add this column
        // modAesFillingType(aesFile.getAesFilingType(),
        // "2");//substr(nvl(:aes_job_detail.filing_option,'2'),1,1);
        String lv_filing_option = "1";
        String lv_usppi_id = null;
        // substr(:aes_job_detail.destination_country_code,1,2);
        String lv_destination_country = AesUtil.modAesCountryCode(aesFile.getDestinationCountry());
        // substr(:aes_job_detail.origin_state,1,2);
        String lv_origin_state = AesUtil.modAesOriginState(aesFile.getOriginState());
        // substr(:aes_job_detail.scac_code,1,4);
        String lv_carrier_id = "";
        // nvl(:aes_job_detail.house_no,:aes_job_detail.booking_no);
        String lv_shipment_ref_no = AesUtil.nvlReplace(aesFile.getHawbNo(), aesFile.getMawbNo());
        // nvl(:aes_job_detail.shipment_filing_type,'A'); ---
        // shipment_filing_action column should be added
        String lv_shipment_filing = AesUtil.modAesFillingType(aesFile.getAesFilingType(), "A");
        // REMOVE_ASCII(substr(:aes_job_detail.carrier_name,1,23)); -- carrier
        // name should be taken
        String lv_carrier_name = AesUtil.modAesCarrierName(aesFile.getCarrier());
        // substr(:aes_job_detail.port_of_unlading,1,5); -- should be added
        String lv_unlading_port = AesUtil.modAesPort(aesFile.getPod(), 5);
        // substr(:aes_job_detail.port_of_exportation,1,4); -- should be added
        String lv_exportation_port = AesUtil.modAesPort(aesFile.getPol(), 4);
        // to_char(:aes_job_detail.etd,'YYYYMMDD');
        String lv_export_date = AesUtil.getCurrentDate(aesFile.getEtd());
        // nvl(:aes_job_detail.is_hazardous,'N');
        String lv_hazmat = AesUtil.modAesYesNo(aesFile.getHazardousCargo());
        // nvl(:aes_job_detail.inbond_type,70);
        String lv_inbound = AesUtil.modAesInBondType(aesFile.getInbondType());
        // substr(:aes_job_detail.import_entry_number,1,15);
        String lv_entry_no = AesUtil.customSubStringNullReturnEmptyString(aesFile.getImportEntryNo(), 0, 15, true);
        // substr(:aes_job_detail.foreign_trade_zone,1,7);
        String lv_ftz = AesUtil.customSubStringNullReturnEmptyString(aesFile.getForeignTradeZone(), 0, 7, true);
        // nvl(:aes_job_detail.is_routed_transaction,'N');
        String lv_routed = AesUtil.modAesYesNo(aesFile.getRoutedTransaction());
        // nvl(substr(REMOVE_ASCII(:aes_equipment_details.equipment_no),1,14),rpad('
        // ',14,' '));
        String lv_equiment_no = "";
        // nvl(substr(REMOVE_ASCII(:aes_equipment_details.seal_no),1,15),rpad('
        // ',15,' '));
        String lv_seal_no = "";
        // nvl(substr(:aes_job_detail.master_no,1,30),rpad(' ',30,' '));
        String lv_transportation_no = aesFile.getMasterUid();
        // TODO
        // substr(:aes_job_detail.shipper_id_number,1,11); -- To be enquire
        String lv_party_id = AesUtil.customSubStringNullReturnNull(aesFile.getAesUsppi().getIdNo(), 0, 11);
        // nvl(:aes_job_detail.shipper_id_type,'E');
        String lv_id_type = aesFile.getAesUsppi().getIdType() == null ? "E" : aesFile.getAesUsppi().getIdType().toString();
        // REMOVE_ASCII(substr(:aes_job_detail.shipper_name,1,30));
        String lv_usppi_name = AesUtil.nullOrAppendEmptyInRightPadd(AesUtil.removeAscii(aesFile.getAesUsppi().getShipper().getPartyName()), 30, " ");
        // nvl(substr(REMOVE_ASCII(replace(remove_symbol(:aes_job_detail.shipper_first_name),'
        // ','')),1,13),rpad(' ',13,' '));
        String lv_usppi_first_name = AesUtil.nullOrAppendEmptyInRightPadd(AesUtil.removeAscii(aesFile.getAesUsppi().getFirstName()), 13, " ");
        // nvl(substr(replace(REMOVE_ASCII(remove_symbol(:aes_job_detail.shipper_last_name)),'
        // ',''),1,20),rpad(' ',20,' '));
        String lv_usppi_last_name = AesUtil.nullOrAppendEmptyInRightPadd(AesUtil.removeAscii(aesFile.getAesUsppi().getLastName()), 20, " ");
        // REMOVE_ASCII(replace(:aes_job_detail.shipper_address,'-',' '));
        String lv_usppi_address = AesUtil.removeAscii(aesFile.getAesUsppi().getCargoOriginAddress().getAddressLine1()).replace("-", " ");
        // aesFile.getAesUsppi().getCargoOriginAddress().getAddressLine1();
        // trim(substr(replace(lv_usppi_address,chr(10),' '),1,32));
        String lv_usppi_address1 = AesUtil.nullOrAppendEmptyInRightPadd(aesFile.getAesUsppi().getCargoOriginAddress().getAddressLine2(), 32, " ");
        // nvl(trim(substr(replace(lv_usppi_address,chr(10),' '),33,32)),rpad('
        // ',32,' ')); -- shipper details
        String lv_usppi_address2 = AesUtil.nullOrAppendEmptyInRightPadd(aesFile.getAesUsppi().getCargoOriginAddress().getAddressLine3(), 32, " ");
        // nvl(substr(:aes_job_detail.shipper_phone,1,11),rpad(' ',13,' ')); --
        // shipper details
        String lv_usppi_phone = AesUtil.nullOrAppendEmptyInRightPadd(aesFile.getAesUsppi().getCargoOriginAddress().getPhone(), 13, " ");
        // nvl(substr(:aes_job_detail.shipper_city,1,25),rpad(' ',25,' ')); --
        // shipper details
        //log.info("City :: "+aesFile.getAesUsppi().getCityMaster());
        String lv_usppi_city = AesUtil.nullOrAppendEmptyInRightPadd(aesFile.getAesUsppi().getCityMaster().getCityName(), 25, " ");
        // nvl(substr(:aes_job_detail.shipper_state,1,2),rpad(' ',2,' ')); --AES_FILE_EDI_STATUS
        // shipper details
        String lv_usppi_state = AesUtil.nullOrAppendEmptyInRightPadd(aesFile.getAesUsppi().getStateMaster().getStateName(), 2, " ");
        String lv_usppi_country = "US";
        // nvl(substr(:aes_job_detail.shipper_zip_code,1,9),rpad(' ',9,' '));
        String lv_usppi_postal = AesUtil.nullOrAppendEmptyInRightPadd(aesFile.getAesUsppi().getZipCode(), 9, " ");
        // substr(:aes_job_detail.shipper_ein_no,1,9);
        String lv_shipper_ein = AesUtil.customSubStringNullReturnNull(aesFile.getAesUsppi().getEinNo(), 0, 9);
        // REMOVE_ASCII(substr(:aes_job_detail.consignee_name,1,30));
        String lv_consignee_name = AesUtil.customSubStringNullReturnEmptyString(AesUtil.removeAscii(aesFile.getAesUltimateConsignee().getUltimateConsignee().getPartyName()), 0, 30,
                true);
        // substr(:aes_job_detail.consignee_type,1,1);
        String lv_consignee_type = aesFile.getAesUltimateConsignee().getConsigneeType() == null ? " " : aesFile.getAesUltimateConsignee().getConsigneeType().toString().substring(0, 1);
        // :aes_job_detail.consignee_id_type;
        String lv_consignee_id_type = aesFile.getAesUltimateConsignee().getIdType() == null ? null : aesFile.getAesUltimateConsignee().getIdType().toString();
        // substr(:aes_job_detail.consignee_id,1,11);
        String lv_consignee_id_no = AesUtil.customSubStringNullReturnEmptyString(aesFile.getAesUltimateConsignee().getIdNo(), 0, 11, true);
        // nvl(substr(replace(REMOVE_ASCII(remove_symbol(:aes_job_detail.consignee_first_name)),'
        // ',''),1,13),rpad(' ',13,' '));
        String lv_consignee_first_name = AesUtil.nullOrAppendEmptyInRightPadd(AesUtil.removeAscii(aesFile.getAesUltimateConsignee().getFirstName()).replace("  ", " "), 13, " ");
        // nvl(substr(replace(REMOVE_ASCII(remove_symbol(:aes_job_detail.consignee_last_name)),'
        // ',''),1,20),rpad(' ',20,' '));
        String lv_consignee_last_name = AesUtil.nullOrAppendEmptyInRightPadd(AesUtil.removeAscii(aesFile.getAesUltimateConsignee().getLastName()).replace("  ", " "), 13, " ");
        // REMOVE_ASCII(replace(:aes_job_detail.consignee_address,'-',' '));
        String lv_consignee_address = AesUtil.removeAscii(aesFile.getAesUltimateConsignee().getCompanyAddress().getAddressLine1()).replace("-", " ");
        // trim(substr(replace(lv_consignee_address,chr(10),' '),1,32));
        String lv_consignee_address1 = AesUtil.nullOrAppendEmptyInRightPadd(aesFile.getAesUltimateConsignee().getCompanyAddress().getAddressLine2(), 32, " ");
        // nvl(trim(substr(replace(lv_consignee_address,chr(10),'
        // '),33,32)),rpad(' ',32,' '));
        String lv_consignee_address2 = AesUtil.nullOrAppendEmptyInRightPadd(aesFile.getAesUltimateConsignee().getCompanyAddress().getAddressLine3(), 32, " ");
        // nvl(substr(:aes_job_detail.consignee_phone,1,11),rpad(' ',13,' '));
        String lv_consignee_phone = AesUtil.nullOrAppendEmptyInRightPadd(aesFile.getAesUltimateConsignee().getContactNo(), 13, " ");
        // nvl(substr(:aes_job_detail.consignee_city,1,25),rpad(' ',25,' '));
        String lv_consignee_city = AesUtil.nullOrAppendEmptyInRightPadd(aesFile.getAesUltimateConsignee().getCityMaster().getCityName(), 25, " ");
        // nvl(substr(:aes_job_detail.consignee_state,1,2),rpad(' ',2,' '));
        String lv_consignee_state = aesFile.getAesUltimateConsignee().getStateMaster() == null ? "  " : (AesUtil.nullOrAppendEmptyInRightPadd(aesFile.getAesUltimateConsignee().getStateMaster().getStateName(), 2, " "));
        // nvl(substr(:aes_job_detail.consignee_country,1,2),rpad(' ',2,' '));
        String lv_consignee_country = AesUtil.nullOrAppendEmptyInRightPadd(aesFile.getAesUltimateConsignee().getCountryMaster().getCountryName(), 2, " ");
        // nvl(substr(:aes_job_detail.consignee_zip_code,1,9),rpad(' ',9,' '));
        String lv_consignee_postal = AesUtil.nullOrAppendEmptyInRightPadd(aesFile.getAesUltimateConsignee().getZipCode(), 9, " ");
        // :aes_job_detail.notify_name;

        String lv_notify = "";
        String lv_notify_name = "";
        String lv_notify_address = "";
        String lv_notify_address1 = "";
        String lv_notify_address2 = "";
        String lv_notify_first_name = "";
        String lv_notify_last_name = "";
        String lv_notify_phone = "";
        String lv_notify_city = "";
        String lv_notify_state = "";
        String lv_notify_country = "";
        String lv_notify_postal = "";
        if (aesFile.getAesIntermediateConsignee() != null) {
            if (aesFile.getAesIntermediateConsignee().getIntermediateConsignee() != null) {
                lv_notify = aesFile.getAesIntermediateConsignee().getIntermediateConsignee().getPartyName();
                // nvl(substr(REMOVE_ASCII(:aes_job_detail.notify_name),1,30),rpad('
                // ',30,' '));
                lv_notify_name = aesFile.getAesIntermediateConsignee().getIntermediateConsignee().getPartyName();
            }
            // rpad(' ',13,' ');
            // --nvl(substr(:aes_job_detail.notify_contact_name,1,13),rpad(' ',13,'
            // '));
            lv_notify_first_name = aesFile.getAesIntermediateConsignee().getFirstName();
            // nvl(substr(replace(REMOVE_ASCII(remove_symbol(:aes_job_detail.notify_contact_name)),'
            // ',''),1,20),rpad(' ',20,' '));
            lv_notify_last_name = aesFile.getAesIntermediateConsignee().getLastName();
            // REMOVE_ASCII(replace(:aes_job_detail.notify_address,'-',' '));
            if (aesFile.getAesIntermediateConsignee().getCompanyAddress() != null) {
                lv_notify_address = aesFile.getAesIntermediateConsignee().getCompanyAddress().getAddressLine1();
                // nvl(trim(substr(replace(lv_notify_address,chr(10),' '),1,32)),rpad('
                // ',32,' '));
                lv_notify_address1 = AesUtil.nullOrAppendEmptyInRightPadd(
                        aesFile.getAesIntermediateConsignee().getCompanyAddress().getAddressLine2(), 32, " ");
                // nvl(trim(substr(replace(lv_notify_address,chr(10),' '),33,32)),rpad('
                // ',32,' '));
                lv_notify_address2 = AesUtil.nullOrAppendEmptyInRightPadd(
                        aesFile.getAesIntermediateConsignee().getCompanyAddress().getAddressLine3(), 32, " ");
            }
            // nvl(substr(:aes_job_detail.notify_phone,1,11),rpad(' ',11,' '));
            lv_notify_phone = AesUtil.nullOrAppendEmptyInRightPadd(aesFile.getAesIntermediateConsignee().getContactNo(), 11, " ");
            // nvl(substr(:aes_job_detail.notify_city,1,25),rpad(' ',25,' '));
            if (aesFile.getAesIntermediateConsignee().getCityMaster() != null)
                lv_notify_city = AesUtil.nullOrAppendEmptyInRightPadd(aesFile.getAesIntermediateConsignee().getCityMaster().getCityName(), 25, " ");
            // nvl(substr(:aes_job_detail.notify_state,1,2),rpad(' ',2,' '));
            if (aesFile.getAesIntermediateConsignee().getStateMaster() != null)
                lv_notify_state = AesUtil.nullOrAppendEmptyInRightPadd(aesFile.getAesIntermediateConsignee().getStateMaster().getStateName(), 2, " ");
            // nvl(substr(:aes_job_detail.notify_country_code,1,2),rpad(' ',2,' '));
            if (aesFile.getAesIntermediateConsignee().getCountryMaster() != null)
                lv_notify_country = AesUtil.nullOrAppendEmptyInRightPadd(aesFile.getAesIntermediateConsignee().getCountryMaster().getCountryName(), 2, " ");
            // nvl(substr(:aes_job_detail.notify_zip_code,1,9),rpad(' ',9,' '));
            lv_notify_postal = AesUtil.nullOrAppendEmptyInRightPadd(aesFile.getAesIntermediateConsignee().getZipCode(), 9, " ");
            // nvl(REMOVE_ASCII(substr(:aes_job_detail.forwarder_name,1,30)),rpad('
            // ',30,' '));
        }

        String lv_forwarder_name = "";
        String lv_forwarder_first_name = "";
        String lv_forwarder_last_name = "";
        String lv_forwarder_address = "";
        String lv_forwarder_address1 = "";
        String lv_forwarder_address2 = "";
        String lv_forwarder_phone = "";
        String lv_forwarder_city = "";
        String lv_forwarder_state = "";
        String lv_forwarder_country = "";
        String lv_forwarder_postal = "";

        if (aesFile.getAesFreightForwarder().getForwarder() != null) {
            if (aesFile.getAesFreightForwarder().getForwarder().getPartyName() != null) {
                lv_forwarder_name = AesUtil.nullOrAppendEmptyInRightPadd(AesUtil.removeAscii(aesFile.getAesFreightForwarder().getForwarder().getPartyName()), 30, " ");
            }
            // nvl(substr(replace(REMOVE_ASCII(remove_symbol(:aes_job_detail.forwarder_contact_name)),'
            // ',''),1,13),rpad(' ',13,' '));
            lv_forwarder_first_name = AesUtil.nullOrAppendEmptyInRightPadd(AesUtil.removeAscii(aesFile.getAesFreightForwarder().getFirstName()), 13, " ");
            // nvl(substr(replace(REMOVE_ASCII(remove_symbol(:aes_job_detail.forwarder_last_name)),'
            // ',''),1,20),rpad(' ',20,' '));
            lv_forwarder_last_name = AesUtil.nullOrAppendEmptyInRightPadd(AesUtil.removeAscii(aesFile.getAesFreightForwarder().getLastName()), 20, " ");
            // REMOVE_ASCII(replace(:aes_job_detail.forwarder_address,'-',' '));

            if (aesFile.getAesFreightForwarder().getCompanyAddress() != null) {
                if (aesFile.getAesFreightForwarder().getCompanyAddress().getAddressLine1() != null) {
                    lv_forwarder_address = AesUtil.removeAscii(aesFile.getAesFreightForwarder().getCompanyAddress().getAddressLine1()).replace("-", " ");
                }
                // nvl(trim(substr(replace(lv_forwarder_address,chr(10),'
                // '),1,32)),rpad(' ',32,' '));
                lv_forwarder_address1 = AesUtil.nullOrAppendEmptyInRightPadd(aesFile.getAesFreightForwarder().getCompanyAddress().getAddressLine2(), 32, " ");
                // nvl(trim(substr(replace(lv_forwarder_address,chr(10),'
                // '),33,32)),rpad(' ',32,' '));
                lv_forwarder_address2 = AesUtil.nullOrAppendEmptyInRightPadd(aesFile.getAesFreightForwarder().getCompanyAddress().getAddressLine3(), 32, " ");
            }
            // nvl(substr(:aes_job_detail.forwarder_phone,1,11),rpad(' ',11,' '));
            lv_forwarder_phone = AesUtil.nullOrAppendEmptyInRightPadd(aesFile.getAesFreightForwarder().getContactNo(), 11, " ");
            // nvl(substr(:aes_job_detail.forwarder_city,1,25),rpad(' ',25,' '));
            if (aesFile.getAesFreightForwarder().getCityMaster() != null) {
                lv_forwarder_city = AesUtil.nullOrAppendEmptyInRightPadd(aesFile.getAesFreightForwarder().getCityMaster().getCityName(), 25, " ");
            }

            // nvl(substr(:aes_job_detail.forwarder_state,1,2),rpad(' ',2,' '));
            if (aesFile.getAesFreightForwarder().getStateMaster() != null) {
                lv_forwarder_state = AesUtil.nullOrAppendEmptyInRightPadd(aesFile.getAesFreightForwarder().getStateMaster().getStateCode(), 2, " ");
            }
            // nvl(substr(:aes_job_detail.forwarder_country_code,1,2),rpad(' ',2,'
            // '));

            // nvl(substr(:aes_job_detail.forwarder_city,1,25),rpad(' ',25,' '));
            if (aesFile.getAesFreightForwarder().getCountryMaster() != null) {
                lv_forwarder_country = AesUtil.nullOrAppendEmptyInRightPadd(aesFile.getAesFreightForwarder().getCountryMaster().getCountryCode(), 2, " ");
            }
            // nvl(substr(:aes_job_detail.forwarder_zip_code,1,9),rpad(' ',9,' '));
            lv_forwarder_postal = AesUtil.nullOrAppendEmptyInRightPadd(aesFile.getAesFreightForwarder().getZipCode(), 9, " ");
        }
        String lv_party_type = "";
        if (lv_id_type.equals("D")) {
            lv_usppi_id = AesUtil.nvlReplace(
                    AesUtil.customSubStringNullReturnNull(aesFile.getAesUsppi().getIdNo(), 0, 9),
                    AesUtil.nullOrAppendEmptyInRightPadd("", 11, " "));
            // := nvl(substr(:aes_job_detail.shipper_id_number,1,9),rpad('
            // ',11,' '));
        } else {
            lv_usppi_id = AesUtil.nvlReplace(
                    AesUtil.customSubStringNullReturnNull(aesFile.getAesUsppi().getIdNo(), 0, 11),
                    AesUtil.nullOrAppendEmptyInRightPadd("", 11, " "));
        }
        String lv_forwarder_id = AesUtil.nullOrAppendEmptyInRightPadd(aesFile.getAesFreightForwarder().getIdNo(), 11,
                " ");
        log.info("Location : " + loginUser.getSelectedUserLocation().getId() + " ; Country : " + loginUser.getSelectedUserLocation().getCountryMaster().getId() + " ; Company : " + loginUser.getSelectedUserLocation().getCompanyMaster().getId());
        AesFilerMaster aesFilerMaster = aesFilerMasterRepository.findByLocationAndCountryAndCompany(loginUser.getSelectedUserLocation().getId(), loginUser.getSelectedUserLocation().getCountryMaster().getId(), loginUser.getSelectedUserLocation().getCompanyMaster().getId());

        if (aesFilerMaster == null || aesFilerMaster.getAesFilerCode() == null || aesFilerMaster.getTransmitterCode() == null) {
            log.error("Please enter AES filer details in AES_FILER_MASTER");

//			fieldErrorList.add(new FieldErrorDto(null, "", -1));
            fieldErrorList.add(new FieldErrorDto(null, "Please enter AES filer details in AES_FILER_MASTER", -1));

            aesFileSubmissionResponse.setError(true);
            aesFileSubmissionResponse.setFieldErrorList(fieldErrorList);


            return aesFileSubmissionResponse;
        }
        String lv_filer_id = aesFilerMaster.getAesFilerCode();

        String lv_transmitter_id = aesFilerMaster.getTransmitterCode();

        if (lv_filer_id != null) {
            if (lv_filer_id.length() > 9) {
                log.error(ErrorCode.AESFILE_ID_INVALID);
                // save error; & return response
                FieldErrorDto fieldError = new FieldErrorDto();
                fieldError.setErrorMsg("AES Filer ID should contains 9 numeric characters");
                fieldErrorList.add(fieldError);
                aesFileSubmissionResponse.setError(true);
                aesFileSubmissionResponse.setFieldErrorList(fieldErrorList);
                return aesFileSubmissionResponse;
            }
            lv_filer_id = AesUtil.customSubStringNullReturnEmptyString(lv_filer_id, 0, 9, false);
            if (!AesUtil.checkinteger(lv_filer_id)) {
                FieldErrorDto fieldError = new FieldErrorDto();
                fieldError.setErrorMsg("AES Filer ID should contains 9 numeric characters");
                fieldErrorList.add(fieldError);
                aesFileSubmissionResponse.setError(true);
                aesFileSubmissionResponse.setFieldErrorList(fieldErrorList);
                return aesFileSubmissionResponse;
            }

            if (lv_transmitter_id == null) {
                log.error("Please enter AES transmitter ID in AES_FILER_MASTER");
                FieldErrorDto fieldError = new FieldErrorDto();
                fieldError.setErrorMsg("Please enter AES transmitter ID in AES_FILER_MASTER");
                fieldErrorList.add(fieldError);
                aesFileSubmissionResponse.setError(true);
                aesFileSubmissionResponse.setFieldErrorList(fieldErrorList);
                return aesFileSubmissionResponse;
            } else if (lv_transmitter_id.length() > 9) {
                log.error("AES transmitter ID should contains 9 numeric characters in configuration master code AES_TRANSMITTER_ID");
                FieldErrorDto fieldError = new FieldErrorDto();
                fieldError.setErrorMsg("AES transmitter ID should contains 9 numeric characters in configuration master code AES_TRANSMITTER_ID");
                fieldErrorList.add(fieldError);
                aesFileSubmissionResponse.setError(true);
                aesFileSubmissionResponse.setFieldErrorList(fieldErrorList);
                return aesFileSubmissionResponse;
            }

            lv_transmitter_id = AesUtil.customSubStringNullReturnEmptyString(lv_transmitter_id, 0, 9, false);
            if (!AesUtil.checkinteger(lv_transmitter_id)) {
                log.error("Transmitter ID should contains 9 numeric characters");
                FieldErrorDto fieldError = new FieldErrorDto();
                fieldError.setErrorMsg("Transmitter ID should contains 9 numeric characters");
                fieldErrorList.add(fieldError);
                aesFileSubmissionResponse.setError(true);
                aesFileSubmissionResponse.setFieldErrorList(fieldErrorList);
                return aesFileSubmissionResponse;
            }

        } else {
            log.error("Please enter AES filer details in AES_FILER_MASTER");
            FieldErrorDto fieldError = new FieldErrorDto();
            fieldError.setErrorMsg("Please enter AES filer details in AES_FILER_MASTER");
            fieldErrorList.add(fieldError);
            aesFileSubmissionResponse.setError(true);
            aesFileSubmissionResponse.setFieldErrorList(fieldErrorList);
            return aesFileSubmissionResponse;
        }
//		String lv_message_text = "";
        int lv_sl_no = 1;
        // TODO itn No have to add in aes form
        if (lv_shipment_filing.equals("A") && aesFile.getAesItnNo() != null) {
//			lv_message_text = lv_message_text + lv_sl_no + ". You cannot add a shipment if it is already accepted" + AesUtil.asciiTOString(10);
            //KET ERROR MSG
            fieldErrorList.add(new FieldErrorDto(null, "You cannot add a shipment if it is already accepted", lv_sl_no));
            aesFileSubmissionResponse.setError(true);
            lv_sl_no++;
        }
        if (!lv_origin_state.equals("PR") && lv_destination_country.equals("US")) {
//			lv_message_text = lv_message_text + lv_sl_no + ". Country of destination cannot be US for orgin state other than PUERTO RICO (PR)" + AesUtil.asciiTOString(10);
            //KET ERROR MSG
            fieldErrorList.add(new FieldErrorDto(null, "Country of destination cannot be US for orgin state other than PUERTO RICO (PR)", lv_sl_no));
            aesFileSubmissionResponse.setError(true);
            lv_sl_no++;
        }

        if (lv_origin_state.equals("PR") && lv_destination_country.equals("US") && lv_unlading_port.length() == 5) {
//			lv_message_text = lv_message_text + lv_sl_no
//					+ ". Port of unlading should be US port code, If country of destination is US"
//					+ AesUtil.asciiTOString(10);
//			lv_sl_no++;
            //KET ERROR MSG
            fieldErrorList.add(new FieldErrorDto(null, "Port of unlading should be US port code, If country of destination is US", lv_sl_no));
            aesFileSubmissionResponse.setError(true);
            lv_sl_no++;
        }

        if (lv_id_type.equals("E") && (lv_party_id == null || lv_party_id.length() != 11)) {
//			lv_message_text = lv_message_text + lv_sl_no
//					+ ". Shipper ID number should contain 11 characters, ADD 00 at the end if you dont have suffix"
//					+ AesUtil.asciiTOString(10);
//			lv_sl_no++;

            //KET ERROR MSG
            fieldErrorList.add(new FieldErrorDto(null, "Shipper ID number should contain 11 characters, ADD 00 at the end if you dont have suffix", lv_sl_no));
            aesFileSubmissionResponse.setError(true);
            lv_sl_no++;
        }

        if (lv_party_id != null && (lv_party_id.length() < 9 || lv_party_id.length() == 10 || lv_party_id.length() > 11)) {
//			lv_message_text = lv_message_text + lv_sl_no + ". Shipper ID number should contain 9 or 11 characters"
//					+ AesUtil.asciiTOString(10);
//			lv_sl_no++;

            //KET ERROR MSG
            fieldErrorList.add(new FieldErrorDto(null, "Shipper ID number should contain 9 or 11 characters", lv_sl_no));
            aesFileSubmissionResponse.setError(true);
            lv_sl_no++;
        }
        if (lv_usppi_last_name.length() < 2) {// && aesFile.getItNo() != null)
//			lv_message_text = lv_message_text + lv_sl_no + ". Shipper last name should contain atleast two characters"
//					+ AesUtil.asciiTOString(10);
//			lv_sl_no++;

            //KET ERROR MSG
            fieldErrorList.add(new FieldErrorDto(null, "Shipper last name should contain atleast two characters", lv_sl_no));
            aesFileSubmissionResponse.setError(true);
            lv_sl_no++;
        }
        if (lv_usppi_phone.length() < 8 || lv_usppi_phone.length() > 13 && aesFile.getAesItnNo() != null) {
//			lv_message_text = lv_message_text + lv_sl_no + ". Shipper phone number should contain 8 to 13 numbers"
//					+ AesUtil.asciiTOString(10);
//			lv_sl_no++;
            //KET ERROR MSG
            fieldErrorList.add(new FieldErrorDto(null, "Shipper phone number should contain 8 to 13 numbers", lv_sl_no));
            aesFileSubmissionResponse.setError(true);
            lv_sl_no++;
        }
        if (lv_consignee_id_no.length() < 9 || lv_consignee_id_no.length() == 10 || lv_consignee_id_no.length() > 11) {// &&
            // aesFile.getItNo()
            // !=
            // null)
//			lv_message_text = lv_message_text + lv_sl_no + ". Consignee ID number should contain 9 or 11 characters"
//					+ AesUtil.asciiTOString(10);
//			lv_sl_no++;

            //KET ERROR MSG
            fieldErrorList.add(new FieldErrorDto(null, "Consignee ID number should contain 9 or 11 characters", lv_sl_no));
            aesFileSubmissionResponse.setError(true);
            lv_sl_no++;
        }

        if (lv_consignee_phone.length() < 8 || lv_consignee_id_no.length() > 13) {// &&
            // aesFile.getItNo()
            // !=
            // null)
//			lv_message_text = lv_message_text + lv_sl_no + ". Consignee phone number should contain 8 to 13 numbers"
//					+ AesUtil.asciiTOString(10);
//			lv_sl_no++;

            //KET ERROR MSG
            fieldErrorList.add(new FieldErrorDto(null, "Consignee phone number should contain 8 to 13 numbers", lv_sl_no));
            aesFileSubmissionResponse.setError(true);
            lv_sl_no++;
        }

        if (lv_notify_phone.length() < 8 || lv_notify_phone.length() > 13) {// &&
            // aesFile.getItNo()
            // !=
            // null)
//			lv_message_text = lv_message_text + lv_sl_no + ". Notify phone number should contain 8 to 13 numbers"
//					+ AesUtil.asciiTOString(10);
//			lv_sl_no++;

            //KET ERROR MSG
            fieldErrorList.add(new FieldErrorDto(null, "Notify phone number should contain 8 to 13 numbers", lv_sl_no));
            aesFileSubmissionResponse.setError(true);
            lv_sl_no++;
        }

        if (aesFile.getAesFreightForwarder().getIdType() != null
                && Objects.equals(aesFile.getAesFreightForwarder().getIdType(), IdType.EIN)
                && lv_forwarder_id.length() != 11) {
//			lv_message_text = lv_message_text + lv_sl_no
//					+ ". Forwarder ID number should contain 11 characters, ADD 00 at the end if you dont have suffix"
//					+ AesUtil.asciiTOString(10);
//			lv_sl_no++;

            //KET ERROR MSG
            fieldErrorList.add(new FieldErrorDto(null, "Forwarder ID number should contain 11 characters, ADD 00 at the end if you dont have suffix", lv_sl_no));
            aesFileSubmissionResponse.setError(true);
            lv_sl_no++;
        }

        if (lv_forwarder_id.length() < 9 || lv_forwarder_id.length() == 10 || lv_forwarder_id.length() > 11) {
//			lv_message_text = lv_message_text + lv_sl_no + ". Forwarder ID number should contain 9 or 11 characters"
//					+ AesUtil.asciiTOString(10);
//			lv_sl_no++;

            //KET ERROR MSG
            fieldErrorList.add(new FieldErrorDto(null, "Forwarder ID number should contain 9 or 11 characters", lv_sl_no));
            aesFileSubmissionResponse.setError(true);
            lv_sl_no++;
        }

        if (lv_forwarder_phone.length() < 8 || lv_forwarder_phone.length() > 13) {
//			lv_message_text = lv_message_text + lv_sl_no + ". Forwarder phone number should contain 8 to 13 numbers"
//					+ AesUtil.asciiTOString(10);
//			lv_sl_no++;

            //KET ERROR MSG
            fieldErrorList.add(new FieldErrorDto(null, "Forwarder phone number should contain 8 to 13 numbers", lv_sl_no));
            aesFileSubmissionResponse.setError(true);
            lv_sl_no++;
        }

//		log.error("Error Message :: \n"+lv_message_text);
        // TODO have to start after this condition
        /*
         * if length(:aes_job_detail.forwarder_phone) < 8 or
		 * length(:aes_job_detail.forwarder_phone) > 13 then lv_message_text :=
		 * lv_message_text || lv_sl_no ||'. Forwarder phone number should
		 * contain 8 to 13 numbers' || chr(10); lv_sl_no := lv_sl_no + 1;
		 * --user_message('Phone number should contain 8 to 13 numbers');
		 * --go_item('aes_job_detail.forwarder_phone'); --raise
		 * form_trigger_failure; end if;
		 */
        if (aesFile.getAesFreightForwarder().getContactNo() == null
                || aesFile.getAesFreightForwarder().getContactNo().length() < 8
                || aesFile.getAesFreightForwarder().getContactNo().length() > 13) {
            // lv_message_text := lv_message_text || lv_sl_no ||'. Forwarder phone number should contain 8 to 13 numbers' || chr(10);
            // lv_sl_no := lv_sl_no + 1;

            // KET ERROR MSG
            fieldErrorList.add(new FieldErrorDto(null, "Forwarder phone number should contain 8 to 13 numbers", lv_sl_no));
            aesFileSubmissionResponse.setError(true);
            lv_sl_no++;
        }
        if (aesFile.getAesFreightForwarder().getLastName() == null
                || aesFile.getAesFreightForwarder().getLastName().length() < 2) {
//			lv_message_text :=  lv_message_text || lv_sl_no ||'. Forwarder last name should contain atleast two characters' || chr(10);
//		lv_sl_no := lv_sl_no + 1;

            // KET ERROR MSG
            fieldErrorList.add(new FieldErrorDto(null, "Forwarder last name should contain atleast two characters", lv_sl_no));
            aesFileSubmissionResponse.setError(true);
            lv_sl_no++;
        } else if (AesUtil.containsInteger(aesFile.getAesFreightForwarder().getLastName())) {

            // KET ERROR MSG
            fieldErrorList.add(new FieldErrorDto(null, "Forwarder last name should not contain numeric values", lv_sl_no));
            aesFileSubmissionResponse.setError(true);
            lv_sl_no++;
        }

        for (AesCommodity commodity : aesFile.getAesCommodityList()) {
            if (commodity.getFirstUnit() != null && commodity.getFirstUnit().getUnitCode() != null
                    && !commodity.getFirstUnit().getUnitCode().equals("X") && commodity.getFirstNoOfPieces() == null) {

//				lv_message_text :=  lv_message_text || lv_sl_no ||'. Please enter qty1 value in commodity detail' || chr(10);

                // KET ERROR MSG
                fieldErrorList.add(new FieldErrorDto(null, "Please enter qty1 value in commodity detail", lv_sl_no));
                aesFileSubmissionResponse.setError(true);
                lv_sl_no++;
            }
            if (commodity.getSecondUnit() != null && commodity.getSecondUnit().getUnitCode() != null
                    && !commodity.getSecondUnit().getUnitCode().equals("X") && commodity.getSecondNoOfPieces() == null) {
//				lv_message_text :=  lv_message_text || lv_sl_no ||'. Please enter qty2 value in commodity detail' || chr(10);
                // KET ERROR MSG
                fieldErrorList.add(new FieldErrorDto(null, "Please enter qty2 value in commodity detail", lv_sl_no));
                aesFileSubmissionResponse.setError(true);
                lv_sl_no++;
            }
        }
        if (aesFile.getAesUsppi().getStateMaster() != null && aesFile.getAesUsppi().getStateMaster().getStateCode() != null
                && aesFile.getAesUsppi().getZipCode() != null) {
            AesUsZipCodeMaster aesUsZipCodeMaster = aesUsZipCodeRepository.findByCode(aesFile.getAesUsppi().getZipCode());
            if (aesUsZipCodeMaster == null || !aesUsZipCodeMaster.getStateCode().equals(aesFile.getAesUsppi().getStateMaster().getStateCode())) {
//				 lv_message_text :=  lv_message_text || lv_sl_no ||'. This shipper zip code is  not valid for the shipper state code' || chr(10);
                // KET ERROR MSG
                fieldErrorList.add(new FieldErrorDto(null, "This shipper zip code is  not valid for the shipper state code", lv_sl_no));
                aesFileSubmissionResponse.setError(true);
                lv_sl_no++;
            }
            if (aesUsZipCodeMaster == null || !aesUsZipCodeMaster.getCode().equals(aesFile.getAesUsppi().getZipCode())) {
//				lv_message_text :=  lv_message_text || lv_sl_no ||'. This shipper zip code is not valid' || chr(10);
                fieldErrorList.add(new FieldErrorDto(null, "This shipper zip code is not valid", lv_sl_no));
                aesFileSubmissionResponse.setError(true);
                lv_sl_no++;
            }

        }

        if (aesFile.getAesFreightForwarder().getStateMaster() != null && aesFile.getAesFreightForwarder().getStateMaster().getStateCode() != null
                && aesFile.getAesFreightForwarder().getZipCode() != null) {
            AesUsZipCodeMaster aesUsZipCodeMaster = aesUsZipCodeRepository.findByCode(aesFile.getAesFreightForwarder().getZipCode());
            if (aesUsZipCodeMaster == null || !aesUsZipCodeMaster.getStateCode().equals(aesFile.getAesFreightForwarder().getStateMaster().getStateCode())) {
//				 lv_message_text :=  lv_message_text || lv_sl_no ||'. This forwarder zip code is not valid for the forwarder state code' || chr(10);
                // KET ERROR MSG
                fieldErrorList.add(new FieldErrorDto(null, "This forwarder zip code is not valid for the forwarder state code", lv_sl_no));
                aesFileSubmissionResponse.setError(true);
                lv_sl_no++;
            }
            if (aesUsZipCodeMaster == null || !aesUsZipCodeMaster.getCode().equals(aesFile.getAesFreightForwarder().getZipCode())) {
//				lv_message_text :=  lv_message_text || lv_sl_no ||'. This forwarder zip code is not valid' || chr(10);
                fieldErrorList.add(new FieldErrorDto(null, "This forwarder zip code is not valid", lv_sl_no));
                aesFileSubmissionResponse.setError(true);
                lv_sl_no++;
            }

        }

        if ((lv_party_id == null || lv_party_id.trim().length() < 9 || AesUtil.checkinteger(lv_party_id.substring(0, 9))) && lv_id_type.equals("E")) {
//			lv_message_text :=  lv_message_text || lv_sl_no ||'. Shipper ID number should contain first 9 numeric characters for EIN' || chr(10);
            fieldErrorList.add(new FieldErrorDto(null, "Shipper ID number should contain first 9 numeric characters for EIN", lv_sl_no));
            aesFileSubmissionResponse.setError(true);
            lv_sl_no++;

        }

        if ((lv_party_id == null || lv_party_id.trim().length() < 9 || AesUtil.checkinteger(lv_party_id.substring(0, 9))) && lv_id_type.equals("D")) {
//			lv_message_text :=  lv_message_text || lv_sl_no ||'. Shipper ID number should contain first 9 numeric characters for DUNS' || chr(10);
            fieldErrorList.add(new FieldErrorDto(null, "Shipper ID number should contain first 9 numeric characters for DUNS", lv_sl_no));
            aesFileSubmissionResponse.setError(true);
            lv_sl_no++;

        }

        if (!lv_inbound.equals("70") && lv_entry_no == null && aesFile.getInbondType() != null) {
//			lv_message_text :=  lv_message_text || lv_sl_no ||'. Import entry number must be entered for inbound type other than 70' || chr(10);
            fieldErrorList.add(new FieldErrorDto(null, "Import entry number must be entered for inbound type other than 70", lv_sl_no));
            aesFileSubmissionResponse.setError(true);
            lv_sl_no++;
        }
        if (AesUtil.chkListContains(Arrays.asList("67", "68"), lv_inbound) && lv_ftz == null) {
//			lv_message_text :=  lv_message_text || lv_sl_no ||'. Foreign trade zone must be entered for inbound type in 67 or 68' || chr(10);
            fieldErrorList.add(new FieldErrorDto(null, "Foreign trade zone must be entered for inbound type in 67 or 68", lv_sl_no));
            aesFileSubmissionResponse.setError(true);
            lv_sl_no++;
        }

        if (AesUtil.chkListContains(Arrays.asList("10", "11", "12"), lv_mot) && lv_unlading_port == null) {
//			lv_message_text :=  lv_message_text || lv_sl_no ||'. Port of unlading must be entered for sea shipment' || chr(10);
            fieldErrorList.add(new FieldErrorDto(null, "Port of unlading must be entered for sea shipment", lv_sl_no));
            aesFileSubmissionResponse.setError(true);
            lv_sl_no++;
        }
        if (fieldErrorList != null && !fieldErrorList.isEmpty() && aesFileSubmissionResponse.isError()) {
            aesFileSubmissionResponse.setFieldErrorList(fieldErrorList);
            return aesFileSubmissionResponse;
        }
        if (aesFile.getAesUsppi().getIdNo().length() < 9) {
//			log.error("USPPI ID must have 9 characters");
            fieldErrorList.add(new FieldErrorDto(null, "USPPI ID must have 9 characters", lv_sl_no));
            aesFileSubmissionResponse.setError(true);
            aesFileSubmissionResponse.setFieldErrorList(fieldErrorList);
            return aesFileSubmissionResponse;
        }
        List<String> _10_11_12 = new ArrayList<>();
        _10_11_12.add("10");
        _10_11_12.add("11");
        _10_11_12.add("12");
        List<String> _dest_code = new ArrayList<>();
        _dest_code.add("PR");
        _dest_code.add("US");
        if (!_10_11_12.contains(lv_mot) && !_dest_code.contains(lv_destination_country)) {
            lv_unlading_port = AesUtil.nullOrAppendEmptyInRightPadd(null, 5, " ");
        } else if (!_10_11_12.contains(lv_mot)) {
            lv_carrier_name = AesUtil.modAesCarrierName(aesFile.getCarrier());
        } else {
            lv_carrier_name = AesUtil.modAesCarrierName(aesFile.getCarrier());
            // TODO Have to ad Vessel Name
            // lv_carrier_name :=
            // substr(nvl(:aes_job_detail.vessel_name,:aes_job_detail.carrier_name),1,23);
        }

        List<String> _36_37 = new ArrayList<>();
        _36_37.add("36");
        _36_37.add("37");
        if (_36_37.contains(lv_inbound) || lv_ftz == null) {
            lv_ftz = AesUtil.nullOrAppendEmptyInRightPadd("", 7, " ");
        } else if (!_36_37.contains(lv_inbound) || lv_ftz != null) {
            lv_ftz = AesUtil.nullOrAppendEmptyInRightPadd(lv_ftz, 7, " ");
        }
        if (lv_forwarder_last_name == null) {
            lv_forwarder_last_name = lv_forwarder_first_name;
        }
        if (lv_notify_last_name == null) {
            lv_notify_last_name = lv_notify_first_name;
        }

        List<String> _40_41 = new ArrayList<>();
        _40_41.add("40");
        _40_41.add("41");
        if (_40_41.contains(lv_mot)) {
            lv_transportation_no = lv_transportation_no.substring(0, 3) + "-" + lv_transportation_no.substring(3);
        }
        if (_10_11_12.contains(lv_mot)) {
            lv_transportation_no = AesUtil.customSubStringNullReturnEmptyString(
                    AesUtil.nvlReplace(aesFile.getHawbNo(), aesFile.getMawbNo()), 0, 30, true);
        } else {
            lv_transportation_no = AesUtil.nullOrAppendEmptyInRightPadd(null, 30, " ");
        }

//		port_of_exportation
        String lv_port_export = null;
        String lv_port_unlading = null;
        if (lv_origin_state.equals("PR")) {
            AesUsPortMaster aesUsPortMaster = aesUsPortMasterRepository.findByPortCode(aesFile.getPol().getPortCode());
            if (aesUsPortMaster != null) {
                lv_port_export = aesUsPortMaster.getPortCode();
            }
//TODO
            aesUsPortMaster = aesUsPortMasterRepository.findByPortCode(aesFile.getPod().getPortCode());
            if (aesUsPortMaster != null) {
                lv_port_unlading = aesUsPortMaster.getPortCode();
            }
            if (lv_port_export != null && lv_port_unlading != null) {
//				 user_message('Port of export and port of unlading should not be same PR');
                log.error("Port of export and port of unlading should not be same PR");

                fieldErrorList.add(new FieldErrorDto(null, "Port of export and port of unlading should not be same PR", lv_sl_no));
                aesFileSubmissionResponse.setError(true);
                aesFileSubmissionResponse.setFieldErrorList(fieldErrorList);
                return aesFileSubmissionResponse;
            }
        }

        String fileName = "AES_" + aesFile.getHawbNo() + ".txt";

        String lv_line = "";

        lv_line = "A    " + lv_filer_id + AesUtil.nullOrAppendEmptyInRightPadd(null, 6, " ") + lv_id_type + "XP"
                + lv_transmitter_date
                + AesUtil.nullOrAppendEmptyInRightPadd(AesUtil.nvlReplace(lv_batch_no, ""), 6, " ") + "Y"
                + lv_transmitter_id + AesUtil.nullOrAppendEmptyInRightPadd(null, 33, " ") + "\n";

        if (lv_line != null) {
            lv_mq.append(lv_line);
        }
        lv_line = null;

        lv_line = "B  " + AesUtil.nullOrAppendEmptyInRightPadd(AesUtil.nvlReplace(lv_usppi_id, ""), 11, "0")
                + lv_id_type + AesUtil.nullOrAppendEmptyInRightPadd(null, 10, " ") + lv_usppi_name
                + AesUtil.nullOrAppendEmptyInRightPadd(null, 25, " ") + "\n";

        if (lv_line != null) {
            lv_mq.append(lv_line);
        }
        lv_line = null;

        if (lv_shipment_filing.equals("X")) {
            lv_line = "SC1" + AesUtil.nullOrAppendEmptyInRightPadd(null, 11, " ") + AesUtil.nullOrAppendEmptyInRightPadd(AesUtil.nvlReplace(lv_shipment_ref_no, ""), 17, " ")
                    + lv_shipment_filing + AesUtil.nullOrAppendEmptyInRightPadd(null, 48, " ") + "\n";
        } else {
            lv_line = "SC1N" + lv_mot + lv_destination_country + lv_origin_state + AesUtil.nullOrAppendEmptyInRightPadd(lv_carrier_id, 4, " ")
                    + AesUtil.rpad_nvl(lv_shipment_ref_no, 17) + lv_shipment_filing + AesUtil.rpad_nvl(lv_carrier_name, 23) + lv_filing_option + " "
                    + AesUtil.rpad_nvl(lv_unlading_port, 4) + AesUtil.rpad_nvl(lv_export_date, 8) + " " + lv_hazmat + AesUtil.nullOrAppendEmptyInRightPadd(null, 4, " ") + "\n";
        }

        if (lv_line != null) {
            lv_mq.append(lv_line);
        }
        lv_line = null;


        log.info("\n" + lv_msg);

        if (!lv_shipment_filing.equals("X")) {

            if (!lv_shipment_filing.equals("D")) {
                lv_line = "SC2" + AesUtil.nullOrAppendEmptyInRightPadd(lv_inbound == null ? "70" : lv_inbound, 2, " ")
                        + AesUtil.nullOrAppendEmptyInRightPadd(lv_entry_no, 15, " ") + lv_ftz
                        + AesUtil.nullOrAppendEmptyInRightPadd(null, 13, " ") + lv_routed
                        + AesUtil.nullOrAppendEmptyInRightPadd(null, 39, " ") + "\n";
                lv_mq.append(lv_line);
                lv_line = null;
            }

            if (!lv_shipment_filing.equals("C") && lv_equiment_no != null && lv_equiment_no.trim().length() != 0
                    && lv_transportation_no != null && lv_transportation_no.trim().length() != 0) {
                lv_line = "SC3" + AesUtil.nullOrAppendEmptyInRightPadd(lv_equiment_no, 14, " ")
                        + AesUtil.nullOrAppendEmptyInRightPadd(lv_seal_no, 15, " ")
                        + AesUtil.nullOrAppendEmptyInRightPadd(lv_transportation_no, 30, " ")
                        + AesUtil.nullOrAppendEmptyInRightPadd(null, 18, " ") + "\n";
                lv_mq.append(lv_line);
                lv_line = null;
            }

            lv_line = "N01" + AesUtil.nullOrAppendEmptyInRightPadd(lv_usppi_id, 11, "0") + lv_id_type + "E"
                    + AesUtil.nullOrAppendEmptyInRightPadd(lv_usppi_name, 30, " ")
                    + AesUtil.nullOrAppendEmptyInRightPadd(lv_usppi_first_name, 13, " ")
                    + AesUtil.nullOrAppendEmptyInRightPadd(lv_usppi_last_name, 20, " ") + " " + "\n";
            lv_mq.append(lv_line);
            lv_line = null;

            lv_line = "N02" + AesUtil.nullOrAppendEmptyInRightPadd(lv_usppi_address1, 32, " ")
                    + AesUtil.nullOrAppendEmptyInRightPadd(lv_usppi_address2, 32, " ")
                    + AesUtil.nullOrAppendEmptyInRightPadd(lv_usppi_phone, 13, " ") + "\n";
            lv_mq.append(lv_line);
            lv_line = null;

            if (lv_id_type.equals("E")) {
                lv_shipper_ein = AesUtil.nullOrAppendEmptyInRightPadd(null, 9, " ");
                lv_party_type = " ";
            } else {
                lv_party_type = "E";
                if (lv_shipper_ein == null || lv_shipper_ein.trim().length() == 0) {
//					log.error("Shipper EIN number should be entered,If Shipper ID Type is not E");
                    fieldErrorList.add(new FieldErrorDto(null, "Shipper EIN number should be entered,If Shipper ID Type is not E", lv_sl_no));
                    aesFileSubmissionResponse.setError(true);
                    aesFileSubmissionResponse.setFieldErrorList(fieldErrorList);
                    return aesFileSubmissionResponse;
                }
                lv_shipper_ein = AesUtil.nullOrAppendEmptyInRightPadd(lv_shipper_ein, 9, " ");
            }

            lv_line = "N03" + AesUtil.nullOrAppendEmptyInRightPadd(lv_usppi_city, 25, " ") + lv_usppi_state
                    + lv_usppi_country + AesUtil.nullOrAppendEmptyInRightPadd(lv_usppi_postal, 9, " ")
                    + AesUtil.nullOrAppendEmptyInRightPadd(lv_shipper_ein, 9, " ") + lv_party_type
                    + AesUtil.nullOrAppendEmptyInRightPadd(null, 29, " ") + "\n";
            lv_mq.append(lv_line);
            lv_line = null;

            if (lv_consignee_id_type != null && lv_consignee_id_no != null) {
                lv_consignee_id_no = AesUtil.nullOrAppendEmptyInRightPadd(lv_consignee_id_no, 11, "0");
            } else {
                lv_consignee_id_no = AesUtil.nullOrAppendEmptyInRightPadd(null, 11, " ");
                lv_consignee_id_type = " ";
            }
            if (aesFile.getAesUltimateConsignee().getSoldEnRoute() == null
                    && aesFile.getAesUltimateConsignee().getSoldEnRoute().equals(YesNo.No)) {

                lv_line = "N01" + lv_consignee_id_no + lv_consignee_id_type + "C"
                        + AesUtil.nullOrAppendEmptyInRightPadd(lv_consignee_name, 30, " ")
                        + AesUtil.nullOrAppendEmptyInRightPadd(lv_consignee_first_name, 13, " ")
                        + AesUtil.nullOrAppendEmptyInRightPadd(lv_consignee_last_name, 20, " ")
                        + AesUtil.modAesYesNo(aesFile.getAesUltimateConsignee().getSoldEnRoute()) + "\n";
                lv_mq.append(lv_line);
                lv_line = null;

                lv_line = "N02" + AesUtil.nullOrAppendEmptyInRightPadd(lv_consignee_address1, 32, " ")
                        + AesUtil.nullOrAppendEmptyInRightPadd(lv_consignee_address2, 32, " ")
                        + AesUtil.nullOrAppendEmptyInRightPadd(lv_usppi_phone, 13, " ") + "\n";
                lv_mq.append(lv_line);
                lv_line = null;

                lv_line = "N03" + AesUtil.nullOrAppendEmptyInRightPadd(lv_consignee_city, 25, " ") + lv_consignee_state
                        + lv_consignee_country + AesUtil.nullOrAppendEmptyInRightPadd(lv_consignee_postal, 9, " ")
                        + AesUtil.nullOrAppendEmptyInRightPadd(null, 9, " ") + " "
                        + AesUtil.nullOrAppendEmptyInRightPadd(lv_consignee_type, 1, " ")
                        + AesUtil.nullOrAppendEmptyInRightPadd(null, 28, " ") + "\n";
                lv_mq.append(lv_line);
                lv_line = null;
            }

            if (lv_notify != null) {
                lv_line = "N01" + AesUtil.nullOrAppendEmptyInRightPadd(null, 11, " ") + " I"
                        + AesUtil.nullOrAppendEmptyInRightPadd(lv_notify_name, 30, " ")
                        + AesUtil.nullOrAppendEmptyInRightPadd(lv_notify_first_name, 13, " ")
                        + AesUtil.nullOrAppendEmptyInRightPadd(lv_notify_last_name, 20, " ") + " " + "\n";
                lv_mq.append(lv_line);
                lv_line = null;


                lv_line = "N02" + AesUtil.nullOrAppendEmptyInRightPadd(lv_notify_address1, 32, " ")
                        + AesUtil.nullOrAppendEmptyInRightPadd(lv_notify_address2, 32, " ")
                        + AesUtil.nullOrAppendEmptyInRightPadd(lv_notify_phone, 13, " ") + "\n";
                lv_mq.append(lv_line);
                lv_line = null;


                lv_line = "N03" + AesUtil.nullOrAppendEmptyInRightPadd(lv_notify_city, 25, " ") + lv_notify_state
                        + lv_notify_country + AesUtil.nullOrAppendEmptyInRightPadd(lv_notify_postal, 9, " ")
                        + AesUtil.nullOrAppendEmptyInRightPadd(null, 9, " ") + " "
                        + AesUtil.nullOrAppendEmptyInRightPadd(null, 29, " ") + "\n";
                lv_mq.append(lv_line);
                lv_line = null;
            }

            lv_line = "N01" + AesUtil.nullOrAppendEmptyInRightPadd(lv_forwarder_id, 11, " ") + "EF"
                    + AesUtil.nullOrAppendEmptyInRightPadd(lv_forwarder_name, 30, " ")
                    + AesUtil.nullOrAppendEmptyInRightPadd(lv_forwarder_first_name, 13, " ")
                    + AesUtil.nullOrAppendEmptyInRightPadd(lv_forwarder_last_name, 20, " ") + " " + "\n";
            lv_mq.append(lv_line);
            lv_line = null;

            lv_line = "N02" + AesUtil.nullOrAppendEmptyInRightPadd(lv_forwarder_address1, 32, " ")
                    + AesUtil.nullOrAppendEmptyInRightPadd(lv_forwarder_address2, 32, " ")
                    + AesUtil.nullOrAppendEmptyInRightPadd(lv_forwarder_phone, 13, " ") + "\n";
            lv_mq.append(lv_line);
            lv_line = null;


            lv_line = "N03" + AesUtil.nullOrAppendEmptyInRightPadd(lv_forwarder_city, 25, " ") + lv_forwarder_state
                    + lv_forwarder_country + AesUtil.nullOrAppendEmptyInRightPadd(lv_forwarder_postal, 9, " ")
                    + AesUtil.nullOrAppendEmptyInRightPadd(null, 9, " ") + " "
                    + AesUtil.nullOrAppendEmptyInRightPadd(null, 29, " ") + "\n";
            lv_mq.append(lv_line);

            lv_line = null;


            if (aesFile.getAesCommodityList() != null && !aesFile.getAesCommodityList().isEmpty()
                    && aesFile.getAesCommodityList().size() > 1000) {

                log.error("Commodity Line should not exceed 1000 records");
            }

            int lv_commod_no = 0;
            String lv_domestic = "";
            for (AesCommodity rec_detail : aesFile.getAesCommodityList()) {

                lv_commod_no = lv_commod_no + 1;
                if (rec_detail.getAesExport().getExportCode().equals("HH")) {
                    lv_domestic = " ";
                } else {
                    if (rec_detail.getOriginOfGoods() == null || rec_detail.getOriginOfGoods().equals(OriginOfGoods.Foreign)) {
                        lv_domestic = "F";
                    } else {
                        lv_domestic = "D";
                    }
                }

                lv_line = "CL1" + rec_detail.getAesExport().getExportCode() + " "
                        + AesUtil.nullOrAppendEmptyInLeftPadd(lv_commod_no + "", 4, "0")
                        + AesUtil.nullOrAppendEmptyInRightPadd((rec_detail.getCommodityMaster() != null && rec_detail.getCommodityMaster().getDescription() != null) ? rec_detail.getCommodityMaster().getDescription().toUpperCase() : "", 45, " ")
                        + AesUtil.nullOrAppendEmptyInLeftPadd(AesUtil.nvlReplace(rec_detail.getLicenseValue(), "0"), 10, "0")
                        + AesUtil.nullOrAppendEmptyInRightPadd(null, 1, " ")
                        + AesUtil.modAesAction(rec_detail.getAesAction())
                        + AesUtil.nullOrAppendEmptyInRightPadd(rec_detail.getAesLicense().getLicenseCode(), 3, " ")
                        + AesUtil.nullOrAppendEmptyInRightPadd(null, 9, " ") + "\n";

                lv_mq.append(lv_line);
                lv_line = null;

                if (rec_detail.getFirstNoOfPieces() == null) {
                    lv_qty1 = AesUtil.nullOrAppendEmptyInLeftPadd(null, 10, "0");
                } else {
                    lv_qty1 = AesUtil.nullOrAppendEmptyInLeftPadd((rec_detail.getFirstNoOfPieces() == null ? null
                            : rec_detail.getFirstNoOfPieces().toString()), 10, "0");
                }

                if (rec_detail.getSecondNoOfPieces() == null) {
                    lv_qty2 = AesUtil.nullOrAppendEmptyInLeftPadd(null, 11, "0");
                } else {
                    lv_qty2 = AesUtil.nullOrAppendEmptyInLeftPadd((rec_detail.getSecondNoOfPieces() == null ? null
                            : rec_detail.getSecondNoOfPieces().toString()), 10, "0");
                }

                if (!AesUtil.modAesAction(rec_detail.getAesAction()).equals("D")) {
                    log.info("         || " + AesUtil.rpad_nvl(rec_detail.getAesSchedule().getScheduleCode(), 10));
                    log.info("         || " + AesUtil.rpad_nvl(rec_detail.getFirstUnit().getUnitCode(), 3));
                    log.info("         || " + lv_qty1);
                    log.info("         || " + AesUtil.lpad_nvl((rec_detail.getValueInUsd() == null ? null : rec_detail.getValueInUsd().toString()), 3));
                    log.info("         || " + AesUtil.rpad_nvl(rec_detail.getSecondUnit().getUnitCode(), 3));
                    log.info("         || " + lv_qty2);


                    log.info("         || " + AesUtil.nullOrAppendEmptyInLeftPadd((rec_detail.getGrossWeightInKg() == null ? null : rec_detail.getGrossWeightInKg().toString()), 10, "0"));
                    log.info("         || " + AesUtil.nullOrAppendEmptyInRightPadd(rec_detail.getEccn(), 5, " "));
                    log.info("         || " + AesUtil.nullOrAppendEmptyInRightPadd(rec_detail.getExportLicenseNo(), 12, " "));
                    log.info("         || " + AesUtil.nullOrAppendEmptyInRightPadd(null, 4, " "));


                    lv_line = "CL2" + AesUtil.rpad_nvl(rec_detail.getAesSchedule().getScheduleCode(), 10)
                            + AesUtil.rpad_nvl(rec_detail.getFirstUnit().getUnitCode(), 3) + lv_qty1
                            + AesUtil.lpad_nvl((rec_detail.getValueInUsd() == null ? null
                            : rec_detail.getValueInUsd().toString()), 3)
                            + AesUtil.rpad_nvl(rec_detail.getSecondUnit().getUnitCode(), 3) + lv_qty2
                            + AesUtil.nullOrAppendEmptyInLeftPadd((rec_detail.getGrossWeightInKg() == null ? null
                            : rec_detail.getGrossWeightInKg().toString()), 10, "0")
                            + AesUtil.nullOrAppendEmptyInRightPadd(rec_detail.getEccn(), 5, " ")
                            + AesUtil.nullOrAppendEmptyInRightPadd(rec_detail.getExportLicenseNo(), 12, " ")
                            + AesUtil.nullOrAppendEmptyInRightPadd(null, 4, " ") + "\n";

                    lv_mq.append(lv_line);
                    lv_line = null;
                    if (rec_detail.getUsml() != null && rec_detail.getUsml().getCategoryCode() != null && aesFile.getFilingOption() != null && aesFile.getFilingOption().equals("2")) {
                        lv_line = "ODT"
                                + AesUtil.nullOrAppendEmptyInRightPadd(rec_detail.getItarExcemption().getExcemptionNo(),
                                12, " ")
                                + AesUtil.nullOrAppendEmptyInRightPadd(rec_detail.getDtdcNo(), 6, " ")
                                + AesUtil.modAesYesNo(rec_detail.getSmeIndicator())
                                + AesUtil.nullOrAppendEmptyInRightPadd(rec_detail.getDtdcEligibilityIndicator(), 1, " ")
                                + AesUtil.nullOrAppendEmptyInRightPadd(rec_detail.getUsml().getCategoryCode(), 2, " ")
                                + AesUtil.nullOrAppendEmptyInRightPadd(null, 8, " ")
                                + AesUtil.nullOrAppendEmptyInRightPadd(rec_detail.getDtdc().getUnitCode(), 3, " ")
                                + AesUtil.nullOrAppendEmptyInLeftPadd(rec_detail.getDtdcQuantity().toString(), 7, "0")
                                + AesUtil.nullOrAppendEmptyInRightPadd(null, 37, " ") + "\n";
                        lv_mq.append(lv_line);
                        lv_line = null;

                    }
                    if (AesUtil.modAesYesNo(rec_detail.getIsUsedVehicle()).equals("Y")
                            && rec_detail.getVehicleProductId() != null) {
                        lv_line = "EV1"
                                + AesUtil.nullOrAppendEmptyInRightPadd(rec_detail.getVehicleProductId(), 25, " ")
                                + AesUtil.nullOrAppendEmptyInRightPadd(rec_detail.getVehicleIdType(), 1, " ")
                                + AesUtil.nullOrAppendEmptyInRightPadd(rec_detail.getVehicleTitle(), 15, " ")
                                + AesUtil.nullOrAppendEmptyInRightPadd(rec_detail.getVehicleTitleState(), 2, " ")
                                + AesUtil.nullOrAppendEmptyInRightPadd(null, 34, " ") + "\n";
                        lv_mq.append(lv_line);
                        lv_line = null;
                    }
                    for (DtdcVehicle rec_vehicle_detail : rec_detail.getDtdcVehicleList()) {
                        lv_line = "EV1"
                                + AesUtil.nullOrAppendEmptyInRightPadd(rec_vehicle_detail.getVinProductId(), 25, " ")
                                + AesUtil.nullOrAppendEmptyInRightPadd(rec_vehicle_detail.getVinIdType(), 1, " ")
                                + AesUtil.nullOrAppendEmptyInRightPadd(rec_vehicle_detail.getVehicleTitleNo(), 15, " ")
                                + AesUtil.nullOrAppendEmptyInRightPadd(rec_vehicle_detail.getVehicleTitleState().getStateCode(), 2, " ")
                                + AesUtil.nullOrAppendEmptyInRightPadd(null, 34, " ") + "\n";
                        lv_mq.append(lv_line);
                        lv_line = null;

                    }
                }
            }


            lv_line = "Y  "
                    + AesUtil.nullOrAppendEmptyInRightPadd(lv_usppi_id, 11, "0") + lv_id_type + AesUtil.nullOrAppendEmptyInRightPadd(null, 10, " ")
                    + AesUtil.nullOrAppendEmptyInRightPadd(lv_usppi_name, 30, " ") + AesUtil.nullOrAppendEmptyInRightPadd(null, 25, " ") + "\n";

            lv_mq.append(lv_line);
            lv_line = null;


            lv_line = "Z    " + lv_filer_id + AesUtil.nullOrAppendEmptyInRightPadd(null, 6, " ") + "EXP" + lv_transmitter_date + AesUtil.nullOrAppendEmptyInRightPadd(lv_batch_no, 6, " ")
                    + lv_transmitter_id + AesUtil.nullOrAppendEmptyInRightPadd(null, 30, " ");
            lv_mq.append(lv_line);
            lv_line = null;


            log.info("Generate EDI MEssage\n" + lv_mq);


        }
        aesFileSubmissionResponse.setAesMsg(lv_mq.toString());
        log.info("\n" + lv_msg);
        return aesFileSubmissionResponse;

    }
}
