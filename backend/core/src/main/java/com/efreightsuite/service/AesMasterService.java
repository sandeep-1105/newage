package com.efreightsuite.service;

import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.repository.AesUsForeignPortMasterRepository;
import com.efreightsuite.repository.AesUsPortMasterRepository;
import com.efreightsuite.repository.AesUsStateCodeRepository;
import com.efreightsuite.repository.AesUsZipCodeRepository;
import com.efreightsuite.util.AppUtil;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

/**
 * @author achyutananda Date - 07.03.2016 Updated - 11.03.2016
 */

@Service
@Log4j2
public class AesMasterService {


    @Autowired
    private
    AppUtil appUtil;


    @Autowired
    private
    AesUsPortMasterRepository aesUsPortMasterRepository;

    @Autowired
    private
    AesUsStateCodeRepository aesUsStateCodeRepository;

    @Autowired
    private
    AesUsZipCodeRepository aesUsZipCodeRepository;

    @Autowired
    private
    AesUsForeignPortMasterRepository aesUsForeignPortMasterRepository;


    public BaseDto getPortMaster(String keyword) {

        BaseDto baseDto = new BaseDto();
        try {
            Pageable pageable = new PageRequest(0, 10, Direction.ASC, "portCode");
            baseDto.setResponseObject(aesUsPortMasterRepository.findByKeyword(keyword + "%", pageable));

            baseDto.setResponseCode(ErrorCode.SUCCESS);
            log.info("AesMasterService -> Search is executed successfully..");
        } catch (Exception exception) {
            log.error("Exception in AesMasterService -> Search  method ", exception);
            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);

    }

    public BaseDto getUsZipCodeMaster(String keyword) {

        BaseDto baseDto = new BaseDto();
        try {
            Pageable pageable = new PageRequest(0, 10, Direction.ASC, "stateCode");
            baseDto.setResponseObject(aesUsZipCodeRepository.findByKeyword(keyword + "%", pageable));

            baseDto.setResponseCode(ErrorCode.SUCCESS);
            log.info("AesMasterService -> Search is executed successfully..");
        } catch (Exception exception) {
            log.error("Exception in AesMasterService -> Search  method ", exception);
            baseDto.setResponseCode(ErrorCode.FAILED);
        }
        return appUtil.setDesc(baseDto);
    }

    public BaseDto getUsStateCodeMaster(String keyword) {

        BaseDto baseDto = new BaseDto();
        try {
            Pageable pageable = new PageRequest(0, 10, Direction.ASC, "name");
            baseDto.setResponseObject(aesUsStateCodeRepository.findByKeyword(keyword + "%", pageable));

            baseDto.setResponseCode(ErrorCode.SUCCESS);
            log.info("AesMasterService -> Search is executed successfully..");
        } catch (Exception exception) {
            log.error("Exception in AesMasterService -> Search  method ", exception);
            baseDto.setResponseCode(ErrorCode.FAILED);
        }
        return appUtil.setDesc(baseDto);
    }
    /*
	public void savePortMaster(List<AesUsStateCodeMaster> entityList) {
		for(AesUsStateCodeMaster entity : entityList) {
			if(entity != null && entity.getName()!= null && entity.getStateCode() != null) {
				
				save(entity);
			}
		}
	}
	
	public void save(AesUsStateCodeMaster entity) {
		try{
			aesUsStateCodeRepository.save(entity);
		} catch(Exception ex){
			
		}
	}
	
	public void saveZip(List<AesUsZipCodeMaster> entityList) {
		for(AesUsZipCodeMaster entity : entityList) {
			if(entity != null && entity.getStateCode()!= null && entity.getCode() != null) {
				
				save(entity);
			}
		}
	}
	
	public void save(AesUsZipCodeMaster entity) {
		try{
			aesUsZipCodeRepository.save(entity);
		} catch(Exception ex){
			
		}
	}*/

    public BaseDto getForeignPortMaster(String keyword) {

        BaseDto baseDto = new BaseDto();
        try {
            Pageable pageable = new PageRequest(0, 10, Direction.ASC, "portCode");
            baseDto.setResponseObject(aesUsForeignPortMasterRepository.findByKeyword(keyword + "%", pageable));

            baseDto.setResponseCode(ErrorCode.SUCCESS);
            log.info("AesMasterService -> getForeignPortMaster is executed successfully..");
        } catch (Exception exception) {
            log.error("Exception in AesMasterService -> getForeignPortMaster  method ", exception);
            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);

    }

}
