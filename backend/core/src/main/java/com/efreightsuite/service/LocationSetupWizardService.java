package com.efreightsuite.service;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.xml.bind.DatatypeConverter;

import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.FileUploadDto;
import com.efreightsuite.dto.LocationSetupDTO;
import com.efreightsuite.enumeration.BooleanType;
import com.efreightsuite.enumeration.SequenceFormat;
import com.efreightsuite.enumeration.SequenceType;
import com.efreightsuite.enumeration.YesNo;
import com.efreightsuite.enumeration.common.LocationSetupSection;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.exception.UniqueKey;
import com.efreightsuite.model.SequenceGenerator;
import com.efreightsuite.model.SystemTrackManual;
import com.efreightsuite.model.common.CommonConfigurationMaster;
import com.efreightsuite.model.common.CommonRegularExpression;
import com.efreightsuite.model.common.LocationSetup;
import com.efreightsuite.model.common.LocationSetupService;
import com.efreightsuite.repository.LocationMasterRepository;
import com.efreightsuite.repository.common.CommonConfigurationMasterRepository;
import com.efreightsuite.repository.common.CommonRegularExpressionRepository;
import com.efreightsuite.repository.common.LocationSetupRepository;
import com.efreightsuite.service.common.CommonMetaConfigurationService;
import com.efreightsuite.util.AppUtil;
import com.efreightsuite.util.PasswordUtils;
import com.efreightsuite.util.ValidateUtil;
import com.efreightsuite.validation.LocationSetupValidator;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber.PhoneNumber;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class LocationSetupWizardService {


    @Value("${efs.resource.path}")
    private
    String appResourcePath;

    @Autowired
    private
    AppUtil appUtil;

    @Autowired
    private
    LocationSetupRepository locationSetupRepository;

    @Autowired
    private
    CommonMetaConfigurationService commonMetaConfigurationService;

    @Autowired
    SequenceGeneratorService sequenceGeneratorService;

    @Autowired
    LocationMasterRepository locationMasterRepository;

    @Autowired
    private
    SaasService saasService;

    @Autowired
    private
    LocationSetupValidator locationSetupValidator;

    @Autowired
    private
    CommonConfigurationMasterRepository commonConfigurationMasterRepository;

    @Autowired
    private
    CommonRegularExpressionRepository commonRegularExpressionRepository;

    @Autowired
    private SaasIdGeneratorService saasIdGeneratorService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    private final PhoneNumberUtil phoneNumberUtil = PhoneNumberUtil.getInstance();

    public BaseDto get(Long id) {
        log.info("Location Setup Wizard Get By ID : [" + id + "]");

        BaseDto baseDto = new BaseDto();

        try {

            LocationSetup locationSetup = locationSetupRepository.getOne(id);

            if (locationSetup != null) {
                if (locationSetup.getLogo() != null) {
                    locationSetup.setEncodedLogo(DatatypeConverter.printBase64Binary(locationSetup.getLogo()));
                }
                locationSetup.setLogo(null);
                locationSetup.setPassword(null);
                baseDto.setResponseCode(ErrorCode.SUCCESS);
                baseDto.setResponseObject(locationSetup);
                log.info("Successfully get Location Setup Wizard Get By ID : [" + id + "]");
            } else {
                baseDto.setResponseCode(ErrorCode.FAILED);
            }

        } catch (Exception exception) {

            baseDto.setResponseCode(ErrorCode.FAILED);

            log.error("Exception in get ", exception);

        }

        return baseDto;
    }

    public BaseDto createOrUpdate(LocationSetup locationSetup) {

        log.info("LocationSetupWizardService.createOrUpdate Method is called.");

        BaseDto baseDto = new BaseDto();
        try {

            ValidateUtil.notNull(locationSetup, ErrorCode.UR_FAILURE);
            if (locationSetup.getId() != null) {
                LocationSetup saved = locationSetupRepository.findOne(locationSetup.getId());
                if (StringUtils.isBlank(locationSetup.getPassword())) {
                    locationSetup.setPassword(saved.getPassword());
                }
            }

            if (locationSetup.isTab(LocationSetupSection.Trail)) {
                return handleTrail(locationSetup, baseDto);
            } else if (locationSetup.isTab(LocationSetupSection.Company)) {
                validateAndSetForCompanyTab(locationSetup);
                if (StringUtils.isBlank(locationSetup.getSaasId())) {
                    locationSetup.setSaasId(saasIdGeneratorService.generateSaasId(locationSetup));
                }
            } else if (locationSetup.isTab(LocationSetupSection.Login)) {
                validateAndSetForLoginTab(locationSetup);
            } else if (locationSetup.isTab(LocationSetupSection.Charge)) {
                locationSetupValidator.validateChargeTab(locationSetup);
            }

            if (locationSetup.getSystemTrackManual() == null) {
                locationSetup.setSystemTrackManual(new SystemTrackManual());
                locationSetup.getSystemTrackManual().setCreateDate(new Date());
                locationSetup.getSystemTrackManual().setCreateUser("Administrator");
            }

            locationSetup.getSystemTrackManual().setLastUpdatedDate(new Date());
            locationSetup.getSystemTrackManual().setLastUpdatedUser("Administrator");

            LocationSetup savedLocation = saasService.saveAndSendMail(locationSetup);

            baseDto.setResponseCode(ErrorCode.SUCCESS);
            baseDto.setResponseObject(new LocationSetup(savedLocation.getId(), savedLocation.getSaasId()));

        } catch (RestException exception) {

            log.error("Exception in create method : ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (Exception exception) {

            log.error("Exception in create method : ", exception);

            String exceptionCause = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Exception Cause  ::: ", exceptionCause);

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            if (exceptionCause.contains(UniqueKey.UK_COM_CHARGE_CHARGECODE)) {
                baseDto.setResponseCode(ErrorCode.CHARGE_CODE_ALREADY_EXIST);
            }

            if (exceptionCause.contains(UniqueKey.UK_COM_CHARGE_CHARGENAME)) {
                baseDto.setResponseCode(ErrorCode.CHARGE_NAME_ALREADY_EXIST);
            }


            if (exceptionCause.contains(UniqueKey.UK_COM_SEQUENCE_TYPE)) {
                baseDto.setResponseCode(ErrorCode.SEQUENCE_TYPE_ALREADY_EXIST);
            }

		/*	if (exceptionCause.contains(UniqueKey.UK_SEQUENCE_NAME)) {
                baseDto.setResponseCode(ErrorCode.SEQUENCE_NAME_ALREADY_EXIST);
			}*/

            if (exceptionCause.contains(UniqueKey.UK_COM_SEQUENCE_PREFIX)) {
                baseDto.setResponseCode(ErrorCode.SEQUENCE_PREFIX_ALREADY_EXIST);
            }


            if (exceptionCause.contains(UniqueKey.UK_LOC_DAYBOOK_DAYBOOKCODE)) {
                baseDto.setResponseCode(ErrorCode.DAYBOOK_CODE_ALREADY_EXIST);
            }

            if (exceptionCause.contains(UniqueKey.UK_LOC_DAYBOOK_DAYBOOKNAME)) {
                baseDto.setResponseCode(ErrorCode.DAYBOOK_NAME_ALREADY_EXIST);
            }


        }

        return appUtil.setDesCommon(baseDto);

    }

    private BaseDto handleTrail(LocationSetup locationSetup, BaseDto baseDto) {
        if (locationSetupValidator.validateTrail(locationSetup)) {
            locationSetup.setSystemTrackManual(new SystemTrackManual());
            locationSetup.getSystemTrackManual().setCreateDate(new Date());
            locationSetup.getSystemTrackManual().setCreateUser("Administrator");
            locationSetup.getSystemTrackManual().setLastUpdatedDate(new Date());
            locationSetup.getSystemTrackManual().setLastUpdatedUser("Administrator");
            locationSetup = saasService.sendTrailLink(locationSetup);
            baseDto.setResponseCode(ErrorCode.SUCCESS);
            baseDto.setResponseObject(locationSetup);
        } else {
            log.info("Exception in locationwizard trail method while creation new user");
        }
        return baseDto;
    }

    private void validateAndSetForLoginTab(LocationSetup locationSetup) {
        locationSetupValidator.validateLoginTab(locationSetup);
        if (locationSetup.getServiceList() != null && !locationSetup.getServiceList().isEmpty()) {
            for (LocationSetupService service : locationSetup.getServiceList()) {
                service.setLocationSetup(locationSetup);
            }
        }

        if (!PasswordUtils.isBcryptPassword(locationSetup.getPassword())) {
            locationSetup.setPassword(passwordEncoder.encode(locationSetup.getPassword()));
        }
    }

    private void validateAndSetForCompanyTab(LocationSetup locationSetup) {
        locationSetupValidator.validateCompanyTab(locationSetup);
        if (locationSetup.getCountryMaster() != null && locationSetup.getCountryMaster().getId() != null) {
            locationSetup.setCountryCode(locationSetup.getCountryMaster().getCountryCode());
        }

        if (locationSetup.getStateMaster() != null && locationSetup.getStateMaster().getId() != null) {
            locationSetup.setStateCode(locationSetup.getStateMaster().getStateCode());
        }

        if (locationSetup.getCityMaster() != null && locationSetup.getCityMaster().getId() != null) {
            locationSetup.setCityCode(locationSetup.getCityMaster().getCityCode());
        }

        if (locationSetup.getRegionMaster() != null && locationSetup.getRegionMaster().getId() != null) {
            locationSetup.setRegionCode(locationSetup.getRegionMaster().getRegionCode());
        }

        if (locationSetup.getTimeZoneMaster() != null && locationSetup.getTimeZoneMaster().getId() != null) {
            locationSetup.setTimeZone(locationSetup.getTimeZoneMaster().getActualTimeZone());
        }
    }


    public BaseDto activate(String key, Long id) {

        BaseDto baseDto = new BaseDto();

        if (key != null && id != null) {

            LocationSetup locationSetup = locationSetupRepository.getOne(id);

            if (locationSetup != null && locationSetup.getActivationKey().equals(key)) {

                locationSetup.setIsUserActivated(BooleanType.TRUE);
                saasService.registerNewCompany(new LocationSetupDTO(locationSetup), locationSetup);
                baseDto.setResponseCode(ErrorCode.SUCCESS);
            } else {
                log.error("Unknown locationSetup object");
                baseDto.setResponseCode(ErrorCode.FAILED);
            }
        } else {
            log.error("ID or KEY is missing");
            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return baseDto;
    }

    public BaseDto isUserActivated(Long id) {

        BaseDto baseDto = new BaseDto();

        if (id != null) {
            LocationSetup locationSetup = locationSetupRepository.getOne(id);
            if (locationSetup == null) {
                log.error("Unknown locationSetup object");
                baseDto.setResponseCode(ErrorCode.FAILED);
            }

            if (locationSetup != null
                    && locationSetup.getIsUserActivated() != null
                    && locationSetup.getIsUserActivated().equals(BooleanType.TRUE)) {
                log.info("[SECOND TIME REQUEST] This user has been already activated......");
                baseDto.setResponseCode(ErrorCode.SUCCESS);
            } else {
                log.info("[FIRST TIME REQUEST] This user has been already activated......");
                baseDto.setResponseCode(ErrorCode.FAILED);
            }
        } else {
            log.error("ID or KEY is missing");
            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return baseDto;
    }

    public ResponseEntity<BaseDto> isCompanyUnique(String companyName) {
        Optional<LocationSetup> locationSetup = locationSetupRepository.findByCompanyRegisteredName(companyName.toUpperCase());

        if (locationSetup.isPresent() && locationSetup.get().getIsUserActivated().equals(BooleanType.TRUE)) {
            return ResponseEntity.status(HttpStatus.CONFLICT).body(new BaseDto(ErrorCode.UR_COMPANY_NAME_DUPLICATED));
        }
        return ResponseEntity.ok(new BaseDto(ErrorCode.UR_COMPANY_NAME_UNIQUE));
    }

    public ResponseEntity<BaseDto> isPhoneNumberValid(String countryCode, String myPhoneNumber) {
        try {
            PhoneNumber phoneNumber = phoneNumberUtil.parseAndKeepRawInput(myPhoneNumber, countryCode);
            if (phoneNumberUtil.isValidNumber(phoneNumber)) {
                return ResponseEntity.ok(new BaseDto(ErrorCode.UR_PHONE_NUMBER_VALID));
            }
            return ResponseEntity.badRequest().body(new BaseDto(ErrorCode.UR_PHONE_NUMBER_INVALID));
        } catch (Exception exception) {
            return ResponseEntity
                    .status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(new BaseDto(ErrorCode.UR_PHONE_NUMBER_INVALID));
        }
    }

    public ResponseEntity<BaseDto> getCallingCode(String countryCode) {
        int phoneNumberCode = phoneNumberUtil.getCountryCodeForRegion(countryCode);
        return ResponseEntity.ok(new BaseDto(ErrorCode.SUCCESS, phoneNumberCode));
    }

    public BaseDto getAllCommonRegularExpression() {
        BaseDto baseDto = new BaseDto();
        try {
            Map<String, String> mapObj = new HashMap<>();
            for (CommonRegularExpression regExp : commonRegularExpressionRepository.findAll()) {
                mapObj.put(regExp.getRegExpName(), regExp.getJsRegExp());
            }
            log.info("Number of Regular Expressions " + mapObj.size());
            baseDto.setResponseObject(mapObj);
            baseDto.setResponseCode(ErrorCode.SUCCESS);
        } catch (Exception ex) {
            log.error("Exception on Getting : ", ex);
            baseDto.setResponseCode(ErrorCode.FAILED);
        }
        return baseDto;
    }

    public BaseDto getAllCommonConfigurations() {
        BaseDto baseDto = new BaseDto();
        try {
            Map<String, String> mapObj = new HashMap<>();
            for (CommonConfigurationMaster configuration : commonConfigurationMasterRepository.findAll()) {
                mapObj.put(configuration.getCode(), configuration.getValue());
            }
            log.info("Number of Common Configurations " + mapObj.size());
            baseDto.setResponseObject(mapObj);
            baseDto.setResponseCode(ErrorCode.SUCCESS);
        } catch (Exception ex) {
            log.error("Exception on Getting : ", ex);
            baseDto.setResponseCode(ErrorCode.FAILED);
        }
        return baseDto;
    }


    public BaseDto validateFiles(FileUploadDto fileUploadDto) {

        BaseDto baseDto = new BaseDto();

        try {

            commonMetaConfigurationService.validateCustomerOrAgentBulkUpload(fileUploadDto);

            commonMetaConfigurationService.validatePartyList(baseDto, fileUploadDto);

            baseDto.setResponseCode(ErrorCode.SUCCESS);

        } catch (RestException exception) {

            log.error("Exception in validateFiles method : ", exception);
            baseDto.setResponseCode(exception.getMessage());

        } catch (Exception exception) {
            log.error("Error ", exception);
            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return baseDto;
    }

    public BaseDto loadSequenceFromExcel() {

        log.info("loadSequenceFromExcel method started");

        BaseDto baseDto = new BaseDto();


        try (XSSFWorkbook workbook = new XSSFWorkbook(
                new FileInputStream(new File(appResourcePath + "/efs-master-data/sequence.xlsx")))) {

            XSSFSheet firstSheet = workbook.getSheetAt(0);

            List<SequenceGenerator> entityList = new ArrayList<>();

            for (int i = 1; i <= firstSheet.getLastRowNum(); i++) {

                SequenceGenerator entity = new SequenceGenerator();
                entity.setSequenceType(
                        getSequenceType(firstSheet.getRow(i).getCell(0).getStringCellValue().trim().toUpperCase()));
                entity.setSequenceName(firstSheet.getRow(i).getCell(1).getStringCellValue().trim());
                entity.setPrefix(firstSheet.getRow(i).getCell(2).getStringCellValue().trim().toUpperCase());
                entity.setFormat(getSequenceFormat(
                        firstSheet.getRow(i).getCell(3).getStringCellValue().trim().toUpperCase()));

                firstSheet.getRow(i).getCell(4).setCellType(Cell.CELL_TYPE_STRING);
                entity.setCurrentSequenceValue(
                        Long.parseLong(firstSheet.getRow(i).getCell(4).getStringCellValue()));

                if (firstSheet.getRow(i).getCell(5).getStringCellValue().trim().toUpperCase().equals("YES")) {
                    entity.setIsYear(YesNo.Yes);
                } else {
                    entity.setIsYear(YesNo.No);
                }

                if (firstSheet.getRow(i).getCell(6).getStringCellValue().trim().toUpperCase().equals("YES")) {
                    entity.setIsMonth(YesNo.Yes);
                } else {
                    entity.setIsMonth(YesNo.No);
                }

                entity.setSuffix(firstSheet.getRow(i).getCell(7).getStringCellValue());

                entity.setSeparator(firstSheet.getRow(i).getCell(8).getStringCellValue());

                entityList.add(entity);

            }

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(sequenceTypeBasedOnType(entityList));

            log.info("loadSequenceFromExcel method ends here and the size of sequence list : " + entityList.size());

            workbook.close();


        } catch (RestException exception) {

            log.error("Exception in loadSequenceFromExcel method...", exception);

            baseDto.setResponseCode(exception.getMessage());
        } catch (Exception exception) {

            log.error("Exception in loadSequenceFromExcel method", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }
        return appUtil.setDesc(baseDto);

    }

    private List<SequenceGenerator> sequenceTypeBasedOnType(List<SequenceGenerator> entityList) {

        //Fetch bellow sequence type data only :
        //PARTY, EMPLOYEE, ENQUERY, SHIPMENT, CONSOL, SERVICE, QUOTATION

        List<SequenceGenerator> newList = new ArrayList<>();
        for (SequenceGenerator sequence : entityList) {
            if (sequence.getSequenceType().equals(SequenceType.PARTY)
                    || sequence.getSequenceType().equals(SequenceType.EMPLOYEE)
                    || sequence.getSequenceType().equals(SequenceType.ENQUERY)
                    || sequence.getSequenceType().equals(SequenceType.SHIPMENT)
                    || sequence.getSequenceType().equals(SequenceType.CONSOL)
                    || sequence.getSequenceType().equals(SequenceType.SERVICE)
                    || sequence.getSequenceType().equals(SequenceType.QUOTATION)) {
                newList.add(sequence);
            }

        }
        return newList;
    }

    private SequenceType getSequenceType(String sequence) {

        if (SequenceType.PARTY.toString().equals(sequence)) {
            return SequenceType.PARTY;
        } else if (SequenceType.AES_BATCH.toString().equals(sequence)) {
            return SequenceType.AES_BATCH;
        } else if (SequenceType.AIRLINE_PREBOOKING.toString().equals(sequence)) {
            return SequenceType.AIRLINE_PREBOOKING;
        } else if (SequenceType.CAN.toString().equals(sequence)) {
            return SequenceType.CAN;
        } else if (SequenceType.CFS.toString().equals(sequence)) {
            return SequenceType.CFS;
        } else if (SequenceType.CONSOL.toString().equals(sequence)) {
            return SequenceType.CONSOL;
        } else if (SequenceType.DEPARTMENT.toString().equals(sequence)) {
            return SequenceType.DEPARTMENT;
        } else if (SequenceType.DESIGNATION.toString().equals(sequence)) {
            return SequenceType.DESIGNATION;
        } else if (SequenceType.DO.toString().equals(sequence)) {
            return SequenceType.DO;
        } else if (SequenceType.DOCUMENT.toString().equals(sequence)) {
            return SequenceType.DOCUMENT;
        } else if (SequenceType.EMPLOYEE.toString().equals(sequence)) {
            return SequenceType.EMPLOYEE;
        } else if (SequenceType.ENQUERY.toString().equals(sequence)) {
            return SequenceType.ENQUERY;
        } else if (SequenceType.FLIGHTPLAN.toString().equals(sequence)) {
            return SequenceType.FLIGHTPLAN;
        } else if (SequenceType.PURCHASE_ORDER.toString().equals(sequence)) {
            return SequenceType.PURCHASE_ORDER;
        } else if (SequenceType.QUOTATION.toString().equals(sequence)) {
            return SequenceType.QUOTATION;
        } else if (SequenceType.REFERENCE_NO.toString().equals(sequence)) {
            return SequenceType.REFERENCE_NO;
        } else if (SequenceType.SERVICE.toString().equals(sequence)) {
            return SequenceType.SERVICE;
        } else if (SequenceType.SHIPMENT.toString().equals(sequence)) {
            return SequenceType.SHIPMENT;
        }

        return null;
    }

    private SequenceFormat getSequenceFormat(String format) {

        if (SequenceFormat.ONE.toString().equals(format)) {
            return SequenceFormat.ONE;
        } else if (SequenceFormat.TWO.toString().equals(format)) {
            return SequenceFormat.TWO;
        } else if (SequenceFormat.THREE.toString().equals(format)) {
            return SequenceFormat.THREE;
        } else if (SequenceFormat.FOUR.toString().equals(format)) {
            return SequenceFormat.FOUR;
        } else if (SequenceFormat.FIVE.toString().equals(format)) {
            return SequenceFormat.FIVE;
        } else if (SequenceFormat.SIX.toString().equals(format)) {
            return SequenceFormat.SIX;
        } else if (SequenceFormat.SEVEN.toString().equals(format)) {
            return SequenceFormat.SEVEN;
        } else if (SequenceFormat.EIGHT.toString().equals(format)) {
            return SequenceFormat.EIGHT;
        } else if (SequenceFormat.NINE.toString().equals(format)) {
            return SequenceFormat.NINE;
        } else if (SequenceFormat.TEN.toString().equals(format)) {
            return SequenceFormat.TEN;
        }

        return null;
    }

}
