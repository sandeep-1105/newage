package com.efreightsuite.service;


import com.efreightsuite.common.component.CacheRepository;
import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.CitySearchDto;
import com.efreightsuite.dto.SearchRequest;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.exception.UniqueKey;
import com.efreightsuite.model.CityMaster;
import com.efreightsuite.repository.CityMasterRepository;
import com.efreightsuite.search.CitySearchService;
import com.efreightsuite.util.AppUtil;
import com.efreightsuite.util.SaaSUtil;
import com.efreightsuite.util.ValidateUtil;
import com.efreightsuite.validation.CityMasterValidator;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.exception.ExceptionUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.ObjectOptimisticLockingFailureException;
import org.springframework.orm.jpa.JpaObjectRetrievalFailureException;
import org.springframework.stereotype.Service;


@Service
@Log4j2
public class CityMasterService {


    @Autowired
    private
    AppUtil appUtil;


    @Autowired
    private
    CityMasterRepository cityMasterRepository;


    @Autowired
    private
    CacheRepository cacheRepository;


    @Autowired
    private
    CityMasterValidator cityMasterValidator;


    @Autowired
    private
    CitySearchService citySearchService;


    public BaseDto search(SearchRequest searchRequest, Long countryId) {

        log.info("CityMasterService -> Search method is called..");

        BaseDto baseDto = new BaseDto();

        try {


            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(citySearchService.search(searchRequest, countryId));

            log.info("CityMasterService -> Search is executed successfully..");

        } catch (Exception exception) {

            log.error("Exception in CityMasterService -> Search  method ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);

    }


    public BaseDto search(CitySearchDto citySearchDto) {

        log.info("CityMasterService -> Search method is called..");

        BaseDto baseDto = new BaseDto();

        try {

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(citySearchService.search(citySearchDto));

            log.info("CityMasterService -> Search is executed successfully..");

        } catch (Exception exception) {

            log.error("Exception in CityMasterService -> Search  method ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);

    }


    public BaseDto get(Long id) {

        log.info("get method is called [" + id + "]");

        BaseDto baseDto = new BaseDto();

        try {

            CityMaster cityMaster = cacheRepository.get(SaaSUtil.getSaaSId(), CityMaster.class.getName(), id, cityMasterRepository);

            ValidateUtil.notNull(cityMaster, ErrorCode.CITY_ID_NOT_FOUND);

            log.info("Successfully getting City by id.....[" + id + "]");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(cityMaster);


        } catch (RestException exception) {

            log.error("Exception in get method ", exception);

            baseDto.setResponseCode(exception.getMessage());
        } catch (Exception e) {

            log.error("Exception in get method", e);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);
    }


    public BaseDto saveOrUpdate(CityMaster cityMaster) {

        log.info("CityMasterService -> saveOrUpdate method is called.....");

        BaseDto baseDto = new BaseDto();

        try {

            cityMasterValidator.validate(cityMaster);


            cityMaster.setStateCode(cityMaster.getStateMaster().getStateCode());

            cityMaster.setCountryCode(cityMaster.getCountryMaster().getCountryCode());


            if (cityMaster.getId() != null) {
                cityMasterRepository.getOne(cityMaster.getId());
            }

            cityMasterRepository.save(cityMaster);

            cacheRepository.remove(SaaSUtil.getSaaSId(), CityMaster.class.getName(), cityMaster.getId());

            log.info("City Master Saved successfully  :  [" + cityMaster.getId() + "]");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(ErrorCode.SUCCESS);

        } catch (RestException exception) {

            log.error("Exception in CityMasterService -> saveOrUpdate method ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (ObjectOptimisticLockingFailureException e) {

            log.error("Exception in CityMasterService -> saveOrUpdate method ", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_LOCKED_RECORD);

        } catch (JpaObjectRetrievalFailureException e) {

            log.error("Exception in CityMasterService -> saveOrUpdate method ", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_DELETED_RECORD);

        } catch (Exception exception) {

            log.error("Exception in CityMasterService -> saveOrUpdate method ", exception);

            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Exception Cause 1 ::: " + exceptionCause1);

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            if (exceptionCause1.contains("FK_")) {
                baseDto.setResponseCode(ErrorCode.SAVE_UPDATE_FK_RECORD);
            }

            if (exceptionCause1.contains(UniqueKey.UK_CITY_CITYCODE)) {
                baseDto.setResponseCode(ErrorCode.CITY_CODE_ALREADY_EXIST);
            }

            if (exceptionCause1.contains(UniqueKey.UK_CITY_CITYNAME)) {
                baseDto.setResponseCode(ErrorCode.CITY_NAME_ALREADY_EXIST);
            }

        }
        return appUtil.setDesc(baseDto);
    }

    public BaseDto delete(Long id) {

        log.info("Delete method is Invoked...[" + id + "]");

        BaseDto baseDto = new BaseDto();

        try {

            cacheRepository.delete(SaaSUtil.getSaaSId(), CityMaster.class.getName(), id, cityMasterRepository);

            log.info("CityMaster Deleted Successfully....[" + id + "]");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

        } catch (Exception exception) {

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            log.error("Exception in CityMasterService -> delete method ", exception);

            String rootCause = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Root Cause of Exception : " + rootCause);

            if (rootCause.contains("FK_")) {
                baseDto.setResponseCode(ErrorCode.CANNOT_DELETE_FK_RECORD);
            }
        }

        return appUtil.setDesc(baseDto);
    }
}
