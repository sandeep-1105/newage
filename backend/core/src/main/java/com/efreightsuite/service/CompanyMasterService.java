package com.efreightsuite.service;

import java.util.List;

import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.CompanySearchReqDto;
import com.efreightsuite.dto.SearchRequest;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.ExceptionHelper;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.exception.UniqueKey;
import com.efreightsuite.model.CompanyMaster;
import com.efreightsuite.repository.CompanyMasterRepository;
import com.efreightsuite.search.CompanySearchService;
import com.efreightsuite.util.AppUtil;
import com.efreightsuite.util.ValidateUtil;
import com.efreightsuite.validation.CompanyMasterValidator;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.exception.ExceptionUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.orm.ObjectOptimisticLockingFailureException;
import org.springframework.orm.jpa.JpaObjectRetrievalFailureException;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class CompanyMasterService {

    @Autowired
    private
    CompanyMasterRepository companyMasterRepository;

    @Autowired
    private
    CompanySearchService companyImpl;

    @Autowired
    private
    CompanyMasterValidator companyMasterValidator;

    @Autowired
    private
    AppUtil appUtil;

    public BaseDto search(CompanySearchReqDto searchDto) {

        log.info("Search method is called....");

        BaseDto baseDto = new BaseDto();

        try {

            baseDto.setResponseObject(companyImpl.search(searchDto));
            baseDto.setResponseCode(ErrorCode.SUCCESS);

            log.info("Successfully getting list of company...");

        } catch (RestException exception) {

            log.error("Exception in getAll method ", exception);

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            baseDto.setResponseCode(exception.getMessage());

        } catch (Exception e) {

            log.error("Exception in getAll method ", e);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);

    }

    public BaseDto search(SearchRequest searchRequest) {

        log.info("search method is called....");

        BaseDto baseDto = new BaseDto();

        try {

            baseDto.setResponseObject(companyImpl.search(searchRequest));
            baseDto.setResponseCode(ErrorCode.SUCCESS);

            log.info("Successfully getting list of company...");

        } catch (RestException exception) {

            log.error("Exception in getAll method ", exception);

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            baseDto.setResponseCode(exception.getMessage());

        } catch (Exception e) {

            log.error("Exception in getAll method ", e);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);

    }

    public BaseDto get(Long id) {

        log.info("get method is called....");

        BaseDto baseDto = new BaseDto();

        try {

            CompanyMaster companyMaster = companyMasterRepository.findById(id);

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(companyMaster);

            log.info("Successfully getting company by id...[" + companyMaster.getId() + "]");

        } catch (Exception e) {

            log.error("Exception in get method", e);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);
    }

    public BaseDto delete(Long id) {

        log.info("Delete method is called....");

        BaseDto baseDto = new BaseDto();

        try {

            companyMasterRepository.delete(id);

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            log.info("Successfully deleting company by id...");

        } catch (RestException re) {

            log.error("Exception in delete method ", re);

            baseDto.setResponseCode(ErrorCode.COMPANY_ID_INVALID);

        } catch (DataIntegrityViolationException exception) {

            log.error("Exception occured : ", exception);

            ExceptionHelper helper = new ExceptionHelper(exception);

            log.error("Foreign key reference : " + helper.getFkReference());

            baseDto.setResponseCode(ErrorCode.ERROR_FK_CONSTRAINT);

        } catch (Exception e) {

            log.error("Exception in delete method", e);

            baseDto.setResponseCode(ErrorCode.FAILED);
            String rootCause = ExceptionUtils.getRootCauseMessage(e);

            log.error("Root Cause of Exception : " + rootCause);

            if (rootCause.contains("FK_")) {
                baseDto.setResponseCode(ErrorCode.CANNOT_DELETE_FK_RECORD);
            }

        }

        return appUtil.setDesc(baseDto);
    }

    public BaseDto getByCompanyCode(String companyCode) {

        log.info("get method is called....");

        BaseDto baseDto = new BaseDto();

        try {

            CompanyMaster companyMaster = companyMasterRepository.findByCompanyCode(companyCode);

            log.info("Successfully getting company by id...");

            baseDto.setResponseCode(ErrorCode.SUCCESS);
            baseDto.setResponseObject(companyMaster);

        } catch (RestException re) {
            log.error("Exception in get method ", re);
            baseDto.setResponseCode(ErrorCode.COMPANY_CODE_INVALID);

        } catch (Exception e) {

            log.error("Exception in get method", e);

            baseDto.setResponseCode(ErrorCode.FAILED);

        }

        return appUtil.setDesc(baseDto);

    }

    public BaseDto getAll() {

        log.info("getAll method is called....");

        BaseDto baseDto = new BaseDto();

        try {

            List<CompanyMaster> companyMasterList = companyMasterRepository.findAll();

            ValidateUtil.notEmpty(companyMasterList, ErrorCode.COMPANY_LIST_EMPTY);

            log.info("Successfully getting list of tos...");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(companyMasterList);

        } catch (RestException re) {

            log.error("Exception in getAll method ", re);

            baseDto.setResponseCode(re.getMessage());

        } catch (Exception e) {

            log.error("Exception in getAll method ", e);

            baseDto.setResponseCode(ErrorCode.FAILED);

        }
        return appUtil.setDesc(baseDto);
    }

    public BaseDto create(CompanyMaster companyMaster) {

        log.info("save method is called.....");

        BaseDto baseDto = new BaseDto();
        try {

            companyMasterValidator.validate(companyMaster);

            companyMaster = companyMasterRepository.save(companyMaster);

            log.info("Company Master saved successfully [" + companyMaster.getId() + "]");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(companyMaster);

        } catch (RestException exception) {

            log.error("Exception Cause 1 ::: ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (Exception exception) {

            log.error("Exception occured : ", exception);

            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Exception Cause 1 : " + exceptionCause1);

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            if (exceptionCause1.contains(UniqueKey.UK_COMPANY_COMPANYCODE)) {
                baseDto.setResponseCode(ErrorCode.COMPANY_CODE_ALREADY_EXIST);

            }

            if (exceptionCause1.contains(UniqueKey.UK_COMPANY_COMPANYNAME)) {
                baseDto.setResponseCode(ErrorCode.COMPANY_NAME_ALREADY_EXIST);
            }

        }

        return appUtil.setDesc(baseDto);

    }

    public BaseDto update(CompanyMaster companyMaster) {

        log.info("Update method is called - companyMaster : [" + companyMaster.getId() + "]");

        BaseDto baseDto = new BaseDto();

        try {

            companyMasterValidator.validate(companyMaster);

            companyMasterRepository.getOne(companyMaster.getId());

            companyMaster = companyMasterRepository.save(companyMaster);

            log.info("CompanyMaster updated successfully....[" + companyMaster.getId() + "]");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(companyMaster);

        } catch (RestException exception) {

            log.error("Exception occured with error code ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (ObjectOptimisticLockingFailureException e) {

            log.error("Error in editing", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_LOCKED_RECORD);

        } catch (JpaObjectRetrievalFailureException e) {

            log.error("Error in editing", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_DELETED_RECORD);

        } catch (Exception exception) {

            log.error("Exception occured : ", exception);

            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Exception Cause 1 ::: " + exceptionCause1);

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            if (exceptionCause1.contains(UniqueKey.UK_COMPANY_COMPANYCODE)) {
                baseDto.setResponseCode(ErrorCode.COMPANY_CODE_ALREADY_EXIST);

            }

            if (exceptionCause1.contains(UniqueKey.UK_COMPANY_COMPANYNAME)) {
                baseDto.setResponseCode(ErrorCode.COMPANY_NAME_ALREADY_EXIST);
            }
        }
        return appUtil.setDesc(baseDto);
    }

}
