package com.efreightsuite.service;

import java.util.List;

import com.efreightsuite.common.component.CacheRepository;
import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.EmailTemplateSearchDto;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.exception.UniqueKey;
import com.efreightsuite.model.EmailTemplate;
import com.efreightsuite.repository.EmailTemplateRepository;
import com.efreightsuite.search.EmailTemplateSearchImpl;
import com.efreightsuite.util.AppUtil;
import com.efreightsuite.util.SaaSUtil;
import com.efreightsuite.util.ValidateUtil;
import com.efreightsuite.validation.EmailTemplateValidator;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.exception.ExceptionUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.ObjectOptimisticLockingFailureException;
import org.springframework.orm.jpa.JpaObjectRetrievalFailureException;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class EmailTemplateService {

    @Autowired
    private
    AppUtil appUtil;

    @Autowired
    private
    EmailTemplateRepository emailTemplateRepository;

    @Autowired
    private
    EmailTemplateValidator emailTemplateValidator;

    @Autowired
    private
    CacheRepository cacheRepository;

    @Autowired
    private
    EmailTemplateSearchImpl emailTemplateSearchImpl;

    public BaseDto search(EmailTemplateSearchDto dto) {
        BaseDto baseDto = new BaseDto();
        try {
            baseDto.setResponseObject(emailTemplateSearchImpl.search(dto));
            baseDto.setResponseCode(ErrorCode.SUCCESS);
        } catch (RestException exception) {
            log.error("Exception in search Email Template : ", exception);
            baseDto.setResponseCode(exception.getMessage());
        } catch (Exception exception) {
            log.error("Exception in search Email Template : ", exception);
            baseDto.setResponseCode(ErrorCode.FAILED);
        }
        return appUtil.setDesc(baseDto);
    }


    /**
     * Method - getByEmailTemplateId Returns EmailTemplate based on given E-mail Template id
     *
     * @return BaseDto
     */

    public BaseDto getByEmailTemplateId(Long id) {
        log.info("getByEmail TemplateId method is called...[" + id + "]");
        final BaseDto baseDto = new BaseDto();
        try {
            EmailTemplate emailTemplate = cacheRepository.get(SaaSUtil.getSaaSId(), EmailTemplate.class.getName(), id, emailTemplateRepository);
            ValidateUtil.notNull(emailTemplate, ErrorCode.PACK_ID_NOT_FOUND);
            log.info("getByEmailTemplateId is successfully executed....[" + emailTemplate.getId() + "]");
            baseDto.setResponseCode(ErrorCode.SUCCESS);
            baseDto.setResponseObject(emailTemplate);
        } catch (final RestException exception) {
            log.error("Exception in getByEmail TemplateId method : ", exception);
            baseDto.setResponseCode(exception.getMessage());
        } catch (final Exception exception) {
            log.error("Exception in getByEmail TemplateId method : ", exception);
            baseDto.setResponseCode(ErrorCode.FAILED);
        }
        return appUtil.setDesc(baseDto);
    }

    /**
     * Method - getAll Returns All EmailTemplates
     *
     * @return BaseDto
     */

    public BaseDto getAll() {
        log.info("getAll Template method is called...");
        final BaseDto baseDto = new BaseDto();
        try {
            List<EmailTemplate> emailTemplateList = emailTemplateRepository.findAll();
            ValidateUtil.notEmpty(emailTemplateList, ErrorCode.PACK_ID_NOT_FOUND);
            baseDto.setResponseCode(ErrorCode.SUCCESS);
            baseDto.setResponseObject(emailTemplateList);
        } catch (final RestException exception) {
            log.error("Exception in getByEmail TemplateId method : ", exception);
            baseDto.setResponseCode(exception.getMessage());
        } catch (final Exception exception) {
            log.error("Exception in getByEmail TemplateId method : ", exception);
            baseDto.setResponseCode(ErrorCode.FAILED);
        }
        return appUtil.setDesc(baseDto);
    }

    /**
     * Method - create Creates and returns a new EmailTemplate E-mail Template
     *
     * @param emailTemplate
     * @returns EmailTemplate
     */

    public BaseDto create(EmailTemplate emailTemplate) {
        log.info("E-mail Template Create method is called  ..");
        final BaseDto baseDto = new BaseDto();
        try {
            emailTemplateValidator.validate(emailTemplate);

            cacheRepository.save(SaaSUtil.getSaaSId(), EmailTemplate.class.getName(), emailTemplate, emailTemplateRepository);
            log.info("Email Template Saved successfully.....");

            baseDto.setResponseCode(ErrorCode.SUCCESS);
            baseDto.setResponseObject(emailTemplate);
        } catch (RestException exception) {
            log.error("Exception Cause 1 ::: ", exception);
            baseDto.setResponseCode(exception.getMessage());
        } catch (Exception exception) {
            log.error("Exception occured : ", exception);
            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(exception);
            log.error("Exception Cause 1 : " + exceptionCause1);
            // General error condition
            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);
            // Duplicate pack Code
            if (exceptionCause1.contains(UniqueKey.UK_PACK_PACKCODE)) {
                baseDto.setResponseCode(ErrorCode.PACK_CODE_ALREADY_EXIST);
            }
        }
        return appUtil.setDesc(baseDto);
    }

    /**
     * Method - update Updates and returns a updated E-mail Template
     *
     * @param emailTemplate
     * @returns EmailTemplate
     */

    public BaseDto update(EmailTemplate emailTemplate) {

        log.info("Update method is called - existingEmail TemplateMaster : [" + emailTemplate.getId() + "]");

        BaseDto baseDto = new BaseDto();

        try {

            ValidateUtil.notNull(emailTemplate.getId(), ErrorCode.PACK_ID_NOT_NULL);
            emailTemplateValidator.validate(emailTemplate);

            cacheRepository.remove(SaaSUtil.getSaaSId(), EmailTemplate.class.getName(), emailTemplate.getId());

            cacheRepository.save(SaaSUtil.getSaaSId(), EmailTemplate.class.getName(), emailTemplate, emailTemplateRepository);


            log.info("EmailTemplate updated successfully....[" + emailTemplate.getId() + "]");
            baseDto.setResponseCode(ErrorCode.SUCCESS);
            baseDto.setResponseObject(emailTemplate);

        } catch (RestException exception) {
            log.error("Exception occured with error code ", exception);
            baseDto.setResponseCode(exception.getMessage());
        } catch (ObjectOptimisticLockingFailureException e) {
            log.error("Error in editing", e);
            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_LOCKED_RECORD);
        } catch (JpaObjectRetrievalFailureException e) {
            log.error("Error in editing", e);
            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_DELETED_RECORD);
        } catch (Exception exception) {
            log.error("Exception occured : ", exception);
            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(exception);
            log.error("Exception Cause 1 : " + exceptionCause1);
            // General error condition
            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);
            // Duplicate pack Code
            if (exceptionCause1.contains(UniqueKey.UK_PACK_PACKCODE)) {
                baseDto.setResponseCode(ErrorCode.PACK_CODE_ALREADY_EXIST);
            }
        }

        return appUtil.setDesc(baseDto);
    }


}
