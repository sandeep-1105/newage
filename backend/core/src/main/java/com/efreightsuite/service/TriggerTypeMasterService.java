package com.efreightsuite.service;

import java.util.List;

import com.efreightsuite.common.component.CacheRepository;
import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.SearchRequest;
import com.efreightsuite.dto.TriggerTypeDto;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.ExceptionHelper;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.exception.UniqueKey;
import com.efreightsuite.model.TriggerTypeMaster;
import com.efreightsuite.repository.TriggerTypeMasterRepository;
import com.efreightsuite.search.TriggerTypeImpl;
import com.efreightsuite.util.AppUtil;
import com.efreightsuite.util.SaaSUtil;
import com.efreightsuite.util.ValidateUtil;
import com.efreightsuite.validation.TriggerTypeMasterValidator;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.exception.ExceptionUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class TriggerTypeMasterService {

    @Autowired
    private
    AppUtil appUtil;

    @Autowired
    private
    TriggerTypeMasterRepository triggerTypeMasterRepository;

    @Autowired
    private
    TriggerTypeMasterValidator triggerTypeMasterValidator;

    @Autowired
    private
    CacheRepository cacheRepository;

    @Autowired
    private
    TriggerTypeImpl triggerTypeImp1;

    public BaseDto search(TriggerTypeDto searchDto) {

        log.info("Search method is called...." + searchDto);

        BaseDto baseDto = new BaseDto();

        try {
            baseDto.setResponseObject(triggerTypeImp1.search(searchDto));

            baseDto.setResponseCode(ErrorCode.SUCCESS);
            log.info("Successfully Searching Trigger Type...");

        } catch (Exception exception) {

            log.info("Exception in Search method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);
    }

    public BaseDto search(SearchRequest searchRequest) {

        log.info("Search method is called...." + searchRequest);

        BaseDto baseDto = new BaseDto();

        try {

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(triggerTypeImp1.search(searchRequest));

            log.info("Successfully Searching TriggerType...");

        } catch (Exception exception) {

            log.error("Exception in Search method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);

    }


    /*
     * Returns all the Trigger Type object as a list
     *
     * @Returns BaseDto
     */
    public BaseDto getAll() {

        log.info("getAll method is called....");

        BaseDto baseDto = new BaseDto();

        try {

            List<TriggerTypeMaster> triggerTypeMasterList = triggerTypeMasterRepository.findAll();

            baseDto.setResponseCode(ErrorCode.SUCCESS);
            baseDto.setResponseObject(triggerTypeMasterList);

            log.info("Successfully getting list of Trigger Type...");

        } catch (RestException re) {
            log.error("Exception in getAll method ", re);
            baseDto.setResponseCode(re.getMessage());

        } catch (Exception e) {
            log.error("Exception in getAll method ", e);
            baseDto.setResponseCode(ErrorCode.FAILED);

        }
        return appUtil.setDesc(baseDto);
    }

    /**
     * Method - get
     *
     * @return baseDto
     */

    public BaseDto get(Long id) {

        log.info("get method is called [" + id + "]");

        BaseDto baseDto = new BaseDto();

        try {

            ValidateUtil.notNull(id, ErrorCode.TRIGGER_TYPE_ID_NOT_NULL);

            TriggerTypeMaster triggerTypeMaster = cacheRepository.get(SaaSUtil.getSaaSId(), TriggerTypeMaster.class.getName(), id, triggerTypeMasterRepository);

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(triggerTypeMaster);

            log.info("Successfully getting Trigger Type Master By Given Id.....[" + triggerTypeMaster.getId() + "]");

        } catch (RestException exception) {

            log.error("Exception in get method...", exception);
            baseDto.setResponseCode(exception.getMessage());
        } catch (Exception exception) {

            log.error("Exception in get method", exception);
            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);

    }

	/*
     * create - This method creates a new Trigger Type Master
	 *
	 * @Param TriggerTypeMaster entity
	 *
	 * @Return BaseDto
	 */

    public BaseDto create(TriggerTypeMaster triggerTypeMaster) {

        log.info("Create method is called....");

        BaseDto baseDto = new BaseDto();

        try {

            triggerTypeMasterValidator.validate(triggerTypeMaster);

            cacheRepository.save(SaaSUtil.getSaaSId(), TriggerTypeMaster.class.getName(), triggerTypeMaster, triggerTypeMasterRepository);

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(ErrorCode.SUCCESS);

        } catch (RestException exception) {

            log.error("Rest Exception occured with error code ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (Exception exception) {

            log.error("Exception occured ", exception);

            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Exception Cause 1 : " + exceptionCause1);

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            // Duplicate Trigger Type Code
            if (exceptionCause1.contains(UniqueKey.UK_TRIGGERTYPE_TRIGGERTYPECODE)) {
                baseDto.setResponseCode(ErrorCode.TRIGGER_TYPE_CODE_ALREADY_EXIST);
            }

            // Duplicate Trigger Type Name
            if (exceptionCause1.contains(UniqueKey.UK_TRIGGERTYPE_TRIGGERTYPENAME)) {
                baseDto.setResponseCode(ErrorCode.TRIGGER_TYPE_NAME_ALREADY_EXIST);
            }
        }

        return appUtil.setDesc(baseDto);
    }
  
	/*
	 * update - This method updates the Existing Trigger Type Master
	 *
	 * @Param ExistingTriggerTypeMaster entity
	 *
	 * @Return BaseDto
	 */

    public BaseDto update(TriggerTypeMaster triggerTypeMaster) {

        log.info("update method is called....[" + triggerTypeMaster.getId() + "]");

        BaseDto baseDto = new BaseDto();

        try {

            if (triggerTypeMaster.getId() != null) {
                triggerTypeMasterRepository.getOne(triggerTypeMaster.getId());
            }

            triggerTypeMasterValidator.validate(triggerTypeMaster);

            triggerTypeMasterRepository.save(triggerTypeMaster);

            cacheRepository.remove(SaaSUtil.getSaaSId(), TriggerTypeMaster.class.getName(), triggerTypeMaster.getId());

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(ErrorCode.SUCCESS);

        } catch (RestException exception) {

            log.error("Rest Exception occured with error code ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (Exception exception) {

            log.error("Exception occured ", exception);

            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Exception Cause 1 : " + exceptionCause1);

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            // Duplicate Trigger Type Code
            if (exceptionCause1.contains(UniqueKey.UK_TRIGGERTYPE_TRIGGERTYPECODE)) {
                baseDto.setResponseCode(ErrorCode.TRIGGER_TYPE_CODE_ALREADY_EXIST);
            }

            // Duplicate Trigger Type Name
            if (exceptionCause1.contains(UniqueKey.UK_TRIGGERTYPE_TRIGGERTYPENAME)) {
                baseDto.setResponseCode(ErrorCode.TRIGGER_TYPE_NAME_ALREADY_EXIST);
            }
        }

        return appUtil.setDesc(baseDto);
    }


    /*
     * Deletes the Existing Trigger Type Based on the Given Id
     *
     * @param Id
     *
     * @Returns BaseDto
     */
    public BaseDto delete(Long id) {

        log.info("Delete method is Invoked...[" + id + "]");

        BaseDto baseDto = new BaseDto();

        try {

            ValidateUtil.notNull(id, ErrorCode.COUNTRY_ID_NOT_NULL);

            cacheRepository.delete(SaaSUtil.getSaaSId(), TriggerTypeMaster.class.getName(), id, triggerTypeMasterRepository);

            log.info("TriggerTypeMaster Deleted Successfully....[" + id + "]");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

        } catch (RestException exception) {

            log.error("Exception in Delete method : ", exception);


            baseDto.setResponseCode(exception.getMessage());

        } catch (DataIntegrityViolationException exception) {

            log.error("Exception occured : ", exception);

            ExceptionHelper helper = new ExceptionHelper(exception);

            log.error("Foreign key reference : " + helper.getFkReference());

            baseDto.setResponseCode(ErrorCode.ERROR_FK_CONSTRAINT);

        } catch (Exception exception) {

            log.error("Exception in Delete method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);
    }

}
