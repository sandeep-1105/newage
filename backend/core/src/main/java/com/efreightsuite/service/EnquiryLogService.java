package com.efreightsuite.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.efreightsuite.common.component.CacheRepository;
import com.efreightsuite.controller.representations.EnquiryLogRequest;
import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.EnquiryDto;
import com.efreightsuite.dto.EnquiryLogSearchDto;
import com.efreightsuite.dto.RecentRecordsRequest;
import com.efreightsuite.dto.SearchRespDto;
import com.efreightsuite.enumeration.AddressType;
import com.efreightsuite.enumeration.LovStatus;
import com.efreightsuite.enumeration.YesNo;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.ExceptionHelper;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.exception.UniqueKey;
import com.efreightsuite.model.*;
import com.efreightsuite.repository.*;
import com.efreightsuite.search.EnquiryLogSearchService;
import com.efreightsuite.service.mailer.EnquiryMailer;
import com.efreightsuite.util.AppUtil;
import com.efreightsuite.util.SaaSUtil;
import com.efreightsuite.util.TimeUtil;
import com.efreightsuite.util.ValidateUtil;
import com.efreightsuite.validators.EnquiryLogValidator;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.exception.ExceptionUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.orm.ObjectOptimisticLockingFailureException;
import org.springframework.orm.jpa.JpaObjectRetrievalFailureException;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class EnquiryLogService {

    @Autowired
    private
    EnquiryLogRepository enquiryLogRepository;

    @Autowired
    private
    AppUtil appUtil;

    @Autowired
    private
    EnquiryMailer enquiryMailer;

    @Autowired
    private
    CacheRepository cacheRepository;

    @Autowired
    private
    EnquiryLogValidator enquiryLogValidator;

    @Autowired
    private
    EnquiryLogSearchService enquiryLogSearchService;

    @Autowired
    private
    EnquiryAttachmentRepository enquiryAttachmentRepository;

    @Autowired
    private
    EnquiryDetailRepository enquiryDetailRepository;

    @Autowired
    private
    PartyTypeMasterRepository partyTypeMasterRepository;

    @Autowired
    private
    RateRequestChargeRepository rateRequestChargeRepository;

    @Autowired
    private PartyMasterRepository partyMasterRepository;

    public BaseDto createCustomer(Long id) {
        log.info("createCustomer method start.");
        BaseDto baseDto = new BaseDto();


        EnquiryLog enquiryLog = enquiryLogRepository.findById(id);
        try {

            if (enquiryLog.getEnquiryCustomer() != null) {
                log.info("Enquiry Customer is there...");

                PartyMaster partyMaster = new PartyMaster();

                partyMaster.setPartyName(enquiryLog.getEnquiryCustomer().getName());
                partyMaster.setFirstName(enquiryLog.getEnquiryCustomer().getFirstName());
                partyMaster.setLastName(enquiryLog.getEnquiryCustomer().getLastName());
                partyMaster.setCountryMaster(enquiryLog.getEnquiryCustomer().getCountryMaster());
                if (partyMaster.getCountryMaster() != null) {
                    partyMaster.setCountryCode(partyMaster.getCountryMaster().getCountryCode());
                }
                partyMaster.setStatus(LovStatus.Active);

                PartyAddressMaster partyAddressMaster = new PartyAddressMaster();

                partyAddressMaster.setPartyMaster(partyMaster);
                partyAddressMaster.setAddressType(AddressType.Primary);
                partyAddressMaster.setAddressLine1(enquiryLog.getEnquiryCustomer().getAddressLine1());
                partyAddressMaster.setAddressLine2(enquiryLog.getEnquiryCustomer().getAddressLine2());
                partyAddressMaster.setAddressLine3(enquiryLog.getEnquiryCustomer().getAddressLine3());
                partyAddressMaster.setStateMaster(enquiryLog.getEnquiryCustomer().getStateMaster());
                partyAddressMaster.setCityMaster(enquiryLog.getEnquiryCustomer().getCityMaster());
                partyAddressMaster.setZipCode(enquiryLog.getEnquiryCustomer().getZipCode());
                partyAddressMaster.setContactPerson(enquiryLog.getEnquiryCustomer().getContactPerson());
                partyAddressMaster.setPhone(enquiryLog.getEnquiryCustomer().getPhone());
                partyAddressMaster.setFax(enquiryLog.getEnquiryCustomer().getFax());
                partyAddressMaster.setMobileNo(enquiryLog.getEnquiryCustomer().getMobileNo());
                partyAddressMaster.setEmail(enquiryLog.getEnquiryCustomer().getEmail());
                partyAddressMaster.setCorporate(YesNo.No);
                partyMaster.setPartyAddressList(new ArrayList<>());
                partyMaster.getPartyAddressList().add(partyAddressMaster);


                PartyAssociateToPartyTypeMaster partyAssociateToPartyTypeMaster = new PartyAssociateToPartyTypeMaster();
                partyAssociateToPartyTypeMaster.setPartyMaster(partyMaster);
                partyMaster.setPartyTypeList(new ArrayList<>());
                partyAssociateToPartyTypeMaster.setPartyTypeMaster(partyTypeMasterRepository.findByPartyTypeName("CUSTOMER"));
                partyMaster.getPartyTypeList().add(partyAssociateToPartyTypeMaster);

                enquiryLog = enquiryLogSearchService.savePartyAndEnquiry(partyMaster, enquiryLog, enquiryLog.getEnquiryCustomer());

                baseDto.setResponseObject(get(enquiryLog.getId()).getResponseObject());
                baseDto.setResponseCode(ErrorCode.SUCCESS);
            } else {
                baseDto.setResponseCode(ErrorCode.PARTY_CUSTOMER_NOT_PROVIDED);
            }
        } catch (Exception exception) {
            log.error("Exception occured ", exception);

            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Exception Cause 1 : " + exceptionCause1);

            // General error condition
            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            boolean alreadyExist = false;
            // Duplicate Party Code
            if (exceptionCause1.contains(UniqueKey.UK_PARTY_PARTYCODE)) {
                alreadyExist = true;
                baseDto.setResponseCode(ErrorCode.PARTY_CODE_ALREADY_EXIST);
            }

            // Duplicate Party Name
            if (exceptionCause1.contains(UniqueKey.UK_PARTY_PARTYNAME)) {
                alreadyExist = true;
                baseDto.setResponseCode(ErrorCode.PARTY_NAME_ALREADY_EXIST);
            }

        }


        log.info("createCustomer method finished.");
        return appUtil.setDesc(baseDto);
    }

    public BaseDto get(Long id) {
        log.info("EnquiryLogService.get method start.");

        BaseDto baseDto = new BaseDto();
        try {

            EnquiryLog enquiryLog = enquiryLogRepository.findById(id);
            AppUtil.setPartyToNull(enquiryLog.getPartyMaster());

            EnquiryLog enquiryLogNew = new EnquiryLog();
            enquiryLogNew.setId(enquiryLog.getId());
            for (EnquiryDetail enquiryDetail : enquiryLog.getEnquiryDetailList()) {
                enquiryDetail.setEnquiryLog(null);
                enquiryDetail.setEnquiryLog(enquiryLogNew);
                /*enquiryDetail.getFromPortMaster().setPortGroupMaster(null);*/
                enquiryDetail.getFromPortMaster().setEdiList(null);
                /*enquiryDetail.getToPortMaster().setPortGroupMaster(null);*/
                enquiryDetail.getToPortMaster().setEdiList(null);
                EnquiryDetail enquiryDetailNew = new EnquiryDetail();
                enquiryDetailNew.setId(enquiryDetail.getId());
                for (EnquiryDimension enquiryDimension : enquiryDetail.getEnquiryDimensionList()) {
                    enquiryDimension.setEnquiryDetail(null);
                    enquiryDimension.setEnquiryDetail(enquiryDetailNew);
                }
                for (EnquiryContainer enquiryContainer : enquiryDetail.getEnquiryContainerList()) {
                    enquiryContainer.setEnquiryDetail(null);
                    enquiryContainer.setEnquiryDetail(enquiryDetailNew);
                }

                for (EnquiryAttachment enquiryAttachment : enquiryDetail.getEnquiryAttachmentList()) {
                    enquiryAttachment.setFile(null);
                    enquiryAttachment.setEnquiryDetail(null);
                    enquiryAttachment.setEnquiryDetail(enquiryDetailNew);
                }

                for (EnquiryValueAddedService enquiryValueAddedService : enquiryDetail.getEnquiryValueAddedServiceList()) {
                    enquiryValueAddedService.setEnquiryDetail(null);
                    enquiryValueAddedService.setEnquiryDetail(enquiryDetailNew);
                }

                for (RateRequest rateRequest : enquiryDetail.getRateRequestList()) {
                    rateRequest.setEnquiryDetail(null);
                    rateRequest.setEnquiryDetail(enquiryDetailNew);
                }
            }

            ValidateUtil.notNull(enquiryLog, ErrorCode.ENQUIRYLOG_ID_INVALID);

            log.info("Successfully getting EnquiryLog by id...");

            baseDto.setResponseCode(ErrorCode.SUCCESS);
            baseDto.setResponseObject(enquiryLog);

        } catch (RestException re) {
            log.error("RestException in EnquiryLogService.get method while getting the EnquiryLog : " + re);
            baseDto.setResponseCode(ErrorCode.ENQUIRYLOG_ID_INVALID);
        } catch (Exception e) {
            log.error("Exception in EnquiryLogService.get method while getting the EnquiryLog : ", e);
            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        log.info("EnquiryLogService.get method end.");
        return appUtil.setDesc(baseDto);
    }

    public BaseDto getByEnquiryNo(String enquiryNo) {
        log.info("EnquiryLogService.get method start.");

        BaseDto baseDto = new BaseDto();
        try {

            EnquiryLog enquiryLog = enquiryLogRepository.findByEnquiryNo(enquiryNo);

            EnquiryLog enquiryLogNew = new EnquiryLog();
            enquiryLogNew.setId(enquiryLog.getId());
            for (EnquiryDetail enquiryDetail : enquiryLog.getEnquiryDetailList()) {
                enquiryDetail.setEnquiryLog(enquiryLogNew);
                enquiryDetail.getFromPortMaster().setPortGroupMaster(null);
                enquiryDetail.getFromPortMaster().setEdiList(null);
                enquiryDetail.getToPortMaster().setPortGroupMaster(null);
                enquiryDetail.getToPortMaster().setEdiList(null);
                EnquiryDetail enquiryDetailnew = new EnquiryDetail();
                enquiryDetailnew.setId(enquiryDetail.getId());
                for (EnquiryDimension enquiryDimension : enquiryDetail.getEnquiryDimensionList()) {
                    enquiryDimension.setEnquiryDetail(enquiryDetailnew);
                }
                for (EnquiryContainer enquiryContainer : enquiryDetail.getEnquiryContainerList()) {
                    enquiryContainer.setEnquiryDetail(enquiryDetailnew);
                }

                for (EnquiryAttachment enquiryAttachment : enquiryDetail.getEnquiryAttachmentList()) {
                    enquiryAttachment.setFile(null);
                    enquiryAttachment.setEnquiryDetail(enquiryDetailnew);
                }
            }

            ValidateUtil.notNull(enquiryLog, ErrorCode.ENQUIRYLOG_ID_INVALID);

            log.info("Successfully getting EnquiryLog by id...");

            baseDto.setResponseCode(ErrorCode.SUCCESS);
            baseDto.setResponseObject(enquiryLog);

        } catch (RestException re) {
            log.error("RestException in EnquiryLogService.get method while getting the EnquiryLog : " + re);
            baseDto.setResponseCode(ErrorCode.ENQUIRYLOG_ID_INVALID);
        } catch (Exception e) {
            log.error("Exception in EnquiryLogService.get method while getting the EnquiryLog : ", e);
            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        log.info("EnquiryLogService.get method end.");
        return appUtil.setDesc(baseDto);
    }

    public BaseDto create(EnquiryLogRequest request) {
        BaseDto baseDto = new BaseDto();
        try {
            EnquiryLog enquiryLog = enquiryLogValidator.validate(request);

            enquiryLog.setPartyMaster(partyMasterRepository.findById(enquiryLog.getPartyMaster().getId()));

            for (EnquiryDetail enquiryDetail : enquiryLog.getEnquiryDetailList()) {
                enquiryDetail.setEnquiryLog(enquiryLog);

                for (EnquiryDimension enquiryDimension : enquiryDetail.getEnquiryDimensionList()) {
                    enquiryDimension.setEnquiryDetail(enquiryDetail);
                }

                for (EnquiryContainer enquiryContainer : enquiryDetail.getEnquiryContainerList()) {
                    enquiryContainer.setEnquiryDetail(enquiryDetail);
                }


                if (enquiryDetail.getEnquiryValueAddedServiceList() != null && !enquiryDetail.getEnquiryValueAddedServiceList().isEmpty()) {
                    for (EnquiryValueAddedService ev : enquiryDetail.getEnquiryValueAddedServiceList()) {
                        ev.setEnquiryDetail(enquiryDetail);
                    }
                } else {
                    enquiryDetail.setEnquiryValueAddedServiceList(new ArrayList<>());
                }

                if (enquiryDetail.getRateRequestList() != null && enquiryDetail.getRateRequestList().size() > 0) {

                    for (int i = 0; i < enquiryDetail.getRateRequestList().size(); i++) {
                        enquiryLogValidator.validateRateRequest(enquiryDetail.getRateRequestList().get(i));
                        enquiryDetail.getRateRequestList().get(i).setEnquiryDetail(enquiryDetail);
                        if (enquiryDetail.getRateRequestList().get(i).getChargeList() != null && enquiryDetail.getRateRequestList().get(i).getChargeList().size() > 0) {

                            for (int j = 0; j < enquiryDetail.getRateRequestList().get(i).getChargeList().size(); j++) {
                                enquiryLogValidator.validateRateRequestCharge(enquiryDetail.getRateRequestList().get(i).getChargeList().get(j));
                                SystemTrackManual stm = new SystemTrackManual();
                                Date currentLocationTime = TimeUtil.getCurrentLocationTime();
                                stm.setCreateDate(currentLocationTime);
                                stm.setCreateUser(AuthService.getCurrentUser().getUserName());
                                stm.setLastUpdatedDate(currentLocationTime);
                                stm.setLastUpdatedUser(AuthService.getCurrentUser().getUserName());
                                enquiryDetail.getRateRequestList().get(i).getChargeList().get(j).setSystemTrackManual(stm);
                                enquiryDetail.getRateRequestList().get(i).getChargeList().get(j).setRateRequest(enquiryDetail.getRateRequestList().get(i));
                            }
                        } else {
                            enquiryDetail.getRateRequestList().get(i).setChargeList(null);
                        }
                    }
                } else {
                    enquiryDetail.setRateRequestList(new ArrayList<>());
                }

                if (enquiryDetail.getEnquiryAttachmentList() != null && enquiryDetail.getEnquiryAttachmentList().size() != 0) {
                    for (EnquiryAttachment enquiryAttachment : enquiryDetail.getEnquiryAttachmentList()) {
                        enquiryAttachment.setEnquiryDetail(enquiryDetail);
                    }
                } else {
                    enquiryDetail.setEnquiryAttachmentList(new ArrayList<>());
                }
            }

            enquiryLog = enquiryLogSearchService.saveEnquiryTransactional(enquiryLog);

            log.info("EnquiryLog Saved successfully.....");
            baseDto.setResponseCode(ErrorCode.SUCCESS);
            baseDto.setResponseObject(new EnquiryLog(enquiryLog.getId(), enquiryLog.getEnquiryNo()));

            sendEmail(baseDto, enquiryLog);

        } catch (RestException re) {
            log.error("Exception in EnquiryLogService.create ", re);
            baseDto.setResponseCode(re.getMessage());
        } catch (Exception e) {
            log.error("Exception EnquiryLogService.create method while saving the EnquiryLog : ", e);

            String exceptionCause = ExceptionUtils.getRootCauseMessage(e);

            log.error("Exception Cause  ::: " + exceptionCause);

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            if (exceptionCause.contains(UniqueKey.UK_ENQDET_SERVICE)) {
                baseDto.setResponseCode(ErrorCode.UK_ENQDET_SERVICE);
            }

            if (exceptionCause.contains(UniqueKey.UK_ENQUIRYATT_REF)) {
                baseDto.setResponseCode(ErrorCode.ENQUIRY_REFERENCE_ALREADY_EXIST);
            }

            if (exceptionCause.contains(UniqueKey.UK_RATECHARGE)) {
                baseDto.setResponseCode(ErrorCode.ENQUIRY_RATEREQUEST_CHARGE_UNIQUE);
            }
        }

        log.info("EnquiryLogService.create method is end.");

        return appUtil.setDesc(baseDto);
    }

    public BaseDto update(Long enquiryId, EnquiryLogRequest updateRequest) {
        log.info("EnquiryLogService.update method is start.");
        BaseDto baseDto = new BaseDto();
        List<Long> olEnquiryDetailIds = new ArrayList<>();

        try {

            ValidateUtil.notNull(enquiryId, ErrorCode.ENQUIRYLOG_ID_NOT_NULL);

            EnquiryLog existingEnquiryLog = enquiryLogRepository.getOne(enquiryId);

            EnquiryLog enquiryLog = enquiryLogValidator.validate(updateRequest, existingEnquiryLog);

            for (EnquiryDetail enquiryDetail : enquiryLog.getEnquiryDetailList()) {
                enquiryDetail.setEnquiryLog(enquiryLog);

                if (enquiryDetail.getEnquiryNo() == null) {
                    enquiryDetail.setEnquiryNo(enquiryLog.getEnquiryNo());
                }


                if (enquiryDetail.getId() != null) {
                    olEnquiryDetailIds.add(enquiryDetail.getId());
                }

                if (enquiryDetail.getEnquiryDimensionList() != null && !enquiryDetail.getEnquiryDimensionList().isEmpty()) {
                    for (EnquiryDimension enquiryDimension : enquiryDetail.getEnquiryDimensionList()) {
                        enquiryDimension.setEnquiryDetail(enquiryDetail);
                    }
                }

                if (enquiryDetail.getEnquiryContainerList() != null && !enquiryDetail.getEnquiryContainerList().isEmpty()) {
                    for (EnquiryContainer enquiryContainer : enquiryDetail.getEnquiryContainerList()) {
                        enquiryContainer.setEnquiryDetail(enquiryDetail);
                    }
                }
                if (enquiryDetail.getEnquiryValueAddedServiceList() != null && !enquiryDetail.getEnquiryValueAddedServiceList().isEmpty()) {
                    for (EnquiryValueAddedService ev : enquiryDetail.getEnquiryValueAddedServiceList()) {
                        ev.setEnquiryDetail(enquiryDetail);
                    }
                } else {
                    enquiryDetail.setEnquiryValueAddedServiceList(new ArrayList<>());
                }


                if (enquiryDetail.getRateRequestList() != null && enquiryDetail.getRateRequestList().size() > 0) {

                    for (int i = 0; i < enquiryDetail.getRateRequestList().size(); i++) {
                        enquiryLogValidator.validateRateRequest(enquiryDetail.getRateRequestList().get(i));
                        enquiryDetail.getRateRequestList().get(i).setEnquiryDetail(enquiryDetail);
                        if (enquiryDetail.getRateRequestList().get(i).getChargeList() != null && enquiryDetail.getRateRequestList().get(i).getChargeList().size() > 0) {

                            for (int j = 0; j < enquiryDetail.getRateRequestList().get(i).getChargeList().size(); j++) {
                                enquiryLogValidator.validateRateRequestCharge(enquiryDetail.getRateRequestList().get(i).getChargeList().get(j));
                                Date currentLocationTime = TimeUtil.getCurrentLocationTime();
                                if (enquiryDetail.getRateRequestList().get(i).getChargeList().get(j).getId() == null) {
                                    SystemTrackManual stm = new SystemTrackManual();
                                    stm.setCreateDate(currentLocationTime);
                                    stm.setCreateUser(AuthService.getCurrentUser().getUserName());
                                    stm.setLastUpdatedDate(currentLocationTime);
                                    stm.setLastUpdatedUser(AuthService.getCurrentUser().getUserName());
                                    enquiryDetail.getRateRequestList().get(i).getChargeList().get(j).setSystemTrackManual(stm);
                                } else {
                                    SystemTrackManual stm = enquiryDetail.getRateRequestList().get(i).getChargeList().get(j).getSystemTrackManual();
                                    stm.setLastUpdatedDate(currentLocationTime);
                                    stm.setLastUpdatedUser(AuthService.getCurrentUser().getUserName());
                                    enquiryDetail.getRateRequestList().get(i).getChargeList().get(j).setSystemTrackManual(stm);
                                }
                                if (enquiryDetail.getRateRequestList().get(i).getRateAccepted() != null && enquiryDetail.getRateRequestList().get(i).getRateAccepted() == YesNo.No) {
                                    enquiryDetail.getRateRequestList().get(i).setRateAcceptedDate(null);
                                    enquiryDetail.getRateRequestList().get(i).setRateAcceptedEmployee(null);
                                } else {
                                    if (enquiryDetail.getRateRequestList().get(i).getRateAcceptedDate() == null) {
                                        enquiryDetail.getRateRequestList().get(i).setRateAcceptedDate(TimeUtil.getCurrentLocationTime());
                                        enquiryDetail.getRateRequestList().get(i).setRateAcceptedEmployee(AuthService.getCurrentUser().getEmployee());
                                    }
                                }
                                enquiryDetail.getRateRequestList().get(i).getChargeList().get(j).setRateRequest(enquiryDetail.getRateRequestList().get(i));
                            }
                        } else {
                            enquiryDetail.getRateRequestList().get(i).setChargeList(new ArrayList<>());
                        }
                    }
                } else {
                    enquiryDetail.setRateRequestList(new ArrayList<>());
                }


                if (enquiryDetail.getEnquiryAttachmentList() != null && enquiryDetail.getEnquiryAttachmentList().size() != 0) {

                    for (EnquiryAttachment enquiryAttachment : enquiryDetail.getEnquiryAttachmentList()) {
                        enquiryAttachment.setEnquiryDetail(enquiryDetail);
                        if (enquiryAttachment.getId() != null && enquiryAttachment.getFile() == null) {
                            EnquiryAttachment attachment = enquiryAttachmentRepository.getOne(enquiryAttachment.getId());
                            enquiryAttachment.setFile(attachment.getFile());

                        }
                    }

                } else {
                    enquiryDetail.setEnquiryAttachmentList(new ArrayList<>());
                }
            }


            EnquiryLog enquiryLogRes = enquiryLogRepository.save(enquiryLog);

            log.info("Enquiry Log updated successfully...");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(new EnquiryLog(enquiryLogRes.getId(), enquiryLogRes.getEnquiryNo()));

            sendEmail(baseDto, enquiryLogRes);

        } catch (RestException exception) {

            log.error("Exception in update method : ", exception);

            baseDto.setResponseCode(exception.getMessage());
        } catch (ObjectOptimisticLockingFailureException e) {
            log.error("Error in editing Service", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_LOCKED_RECORD);
        } catch (JpaObjectRetrievalFailureException e) {
            log.error("Error in editing Service", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_DELETED_RECORD);
        } catch (Exception e) {
            log.error("Failed to update the EnquiryLogService ..", e);

            String exceptionCause = ExceptionUtils.getRootCauseMessage(e);

            log.error("Exception Cause  ::: " + exceptionCause);

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            if (exceptionCause.contains(UniqueKey.UK_ENQDET_SERVICE)) {
                baseDto.setResponseCode(ErrorCode.UK_ENQDET_SERVICE);
            }

            if (exceptionCause.contains(UniqueKey.UK_ENQUIRYATT_REF)) {
                baseDto.setResponseCode(ErrorCode.ENQUIRY_REFERENCE_ALREADY_EXIST);
            }

            if (exceptionCause.contains(UniqueKey.UK_RATECHARGE)) {
                baseDto.setResponseCode(ErrorCode.ENQUIRY_RATEREQUEST_CHARGE_UNIQUE);
            }
        }

        log.info("EnquiryLogService.update method is end.");

        return appUtil.setDesc(baseDto);

    }

    private void sendEmail(BaseDto baseDto, EnquiryLog enquiryLogRes) {
        if (Objects.equals(baseDto.getResponseCode(), ErrorCode.SUCCESS) || baseDto.getResponseCode().equals(ErrorCode.SUCCESS)) {
            enquiryMailer.sendEnquiryMailToSalesDepo(enquiryLogRes, AuthService.getCurrentUser());
            enquiryMailer.sendEnquiryMailToRateRequest(enquiryLogRes, AuthService.getCurrentUser(), SaaSUtil.getSaaSId().toUpperCase());
        }
    }

    public BaseDto search(EnquiryLogSearchDto searchDto, String searchFlag) {

        log.info("Search method is called.");

        BaseDto baseDto = new BaseDto();

        try {

            baseDto.setResponseObject(enquiryLogSearchService.search(searchDto, searchFlag));

            log.info("Successfully Searching EnquiryLog...");

        } catch (Exception exception) {

            log.info("Exception in Search method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        log.info("Search method is Finished Successfully.");

        return appUtil.setDesc(baseDto);
    }

    public SearchRespDto searchRecentRecords(RecentRecordsRequest request) {
        return enquiryLogSearchService.searchRecentRecords(request);
    }

    public BaseDto delete(Long id) {
        log.info("EnquiryLogService.delete method is start.[" + id + "]");

        BaseDto baseDto = new BaseDto();

        try {

            ValidateUtil.notNull(id, ErrorCode.ENQUIRYLOG_ID_NOT_NULL);

            cacheRepository.delete(SaaSUtil.getSaaSId(), EnquiryLog.class.getName(), id, enquiryLogRepository);

            log.info("EnquiryLog Deleted Successfully....[" + id + "]");

            baseDto.setResponseCode(ErrorCode.SUCCESS);
            return appUtil.setDesc(baseDto);

        } catch (RestException exception) {

            log.error("Exception in Delete method : ", exception);

            baseDto.setResponseCode(exception.getMessage());
        } catch (DataIntegrityViolationException exception) {

            log.error("Exception occured : ", exception);

            ExceptionHelper helper = new ExceptionHelper(exception);

            log.error("Foreign key reference : " + helper.getFkReference());

            baseDto.setResponseCode(ErrorCode.ERROR_FK_CONSTRAINT);
        } catch (Exception exception) {

            log.error("Exception in Delete method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }
        log.info("EnquiryLogService.delete method is end");
        return appUtil.setDesc(baseDto);

    }

    public void downloadAttachment(Long attachmentId, HttpServletResponse response) {

        EnquiryAttachment attachment = enquiryAttachmentRepository.getOne(attachmentId);

        if (attachment != null && attachment.getFile() != null && attachment.getFileName() != null) {

            try {

                response.setContentType(attachment.getFileContentType());

                response.setHeader("Content-Disposition", "attachment; filename=" + attachment.getFileName());

                response.setContentLength(attachment.getFile().length);

                response.getOutputStream().write(attachment.getFile());

                response.getOutputStream().flush();

                response.getOutputStream().close();

            } catch (Exception e) {
                log.error("Error occur during download attachment file : " + attachmentId + ", ", e);
            }


        } else {
            log.info("Attachment document is not available : " + attachmentId);
        }

    }

    public BaseDto getAttachmentList(List<Long> list) {

        log.info("getAttachmentList is called");

        BaseDto baseDto = new BaseDto();

        try {

            List<EnquiryAttachment> attachmentList = enquiryAttachmentRepository.findAll(list);

            log.info("Successfully getting list of attachment....");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(attachmentList);

        } catch (Exception exception) {

            log.error("Exception while getting list of attachment...", exception);

            baseDto.setResponseCode(ErrorCode.SUCCESS);
        }

        return appUtil.setDesc(baseDto);
    }

    //For cancel the enquiry
    public BaseDto statusChange(EnquiryDto dto) {

        log.info("enquiry service -> status change method is called....");
        BaseDto baseDto = new BaseDto();
        try {
            if (dto != null && dto.getEnqId() != null) {
                enquiryLogSearchService.cancelEnquiry(baseDto, dto);
            } else {
                throw new RestException(ErrorCode.ENQUIRYLOG_ID_NOT_NULL);
            }
        } catch (RestException re) {
            log.error("Exception in enquiry service -> cancel method ", re);
            baseDto.setResponseCode(re.getMessage());

        } catch (Exception exception) {
            log.error("Exception in enquiry service -> cancel method ", exception);

            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(exception);
            log.error("Exception Cause 1 ::: " + exceptionCause1);
            baseDto.setResponseCode(ErrorCode.FAILED);
        }
        return appUtil.setDesc(baseDto);
    }

    //which is used for enquiry rate request charges save from public api..
    public BaseDto saveEnquiryDetail(RateRequest rq, HttpServletRequest httpServletRequest) {
        log.info("Get Enquiry Detail method is called.");
        BaseDto baseDto = new BaseDto();
        try {

            if (rq != null && rq.getPin() != null) {
                ValidateUtil.isStringEquals(rq.getPin(), rq.getVendor().getPartyDetail().getPin(), ErrorCode.PIN_IS_INVALID);
            } else {
                throw new RestException(ErrorCode.PIN_IS_MANDATORY);
            }

            EnquiryDetail ed = enquiryDetailRepository.findById(rq.getEnquiryDetail().getId());
            LocationMaster lm = ed.getEnquiryLog().getLocationMaster();
            List<RateRequestCharge> chargeList = new ArrayList<>();
            if (rq != null && ed != null) {
                if (rq.getChargeList() != null && !rq.getChargeList().isEmpty()) {
                    for (int j = 0; j < rq.getChargeList().size(); j++) {
                        if (rq.getChargeList().get(j).getId() == null) {
                            SystemTrackManual stm = new SystemTrackManual();
                            stm.setCreateDate(TimeUtil.getCurrentLocationTime(lm));
                            stm.setCreateUser(rq.getVendor().getPartyName());
                            stm.setLastUpdatedDate((TimeUtil.getCurrentLocationTime(lm)));
                            stm.setLastUpdatedUser(rq.getVendor().getPartyName());
                            rq.getChargeList().get(j).setSystemTrackManual(stm);
                        } else {
                            SystemTrackManual stm = rq.getChargeList().get(j).getSystemTrackManual();
                            stm.setLastUpdatedDate(TimeUtil.getCurrentLocationTime(lm));
                            stm.setLastUpdatedUser(rq.getVendor().getPartyName());
                            rq.getChargeList().get(j).setSystemTrackManual(stm);
                        }
                        rq.setCustomerIp(httpServletRequest.getRemoteAddr());
                        rq.getChargeList().get(j).setRateRequest(rq);
                        chargeList.add(rq.getChargeList().get(j));

                    }
                } else {
                    throw new RestException(ErrorCode.CHARGE_LIST_IS_MANDATORY);
                }
                rateRequestChargeRepository.save(chargeList);

                rateRequestChargeRepository.updateRateRequestReceivedDate(TimeUtil.getCurrentLocationTime(lm), rq.getId());
                baseDto.setResponseCode(ErrorCode.SUCCESS);

                if (ErrorCode.SUCCESS.equalsIgnoreCase(baseDto.getResponseCode())) {
                    enquiryMailer.sendEnquiryUpdateMailToSales(ed.getEnquiryNo());
                }
            } else {
                baseDto.setResponseCode(ErrorCode.FAILED);
            }
        } catch (RestException re) {
            baseDto.setResponseCode(re.getMessage());
        } catch (Exception e) {
            log.info("exxception in saving enquiry detail", e);
            baseDto.setResponseCode(ErrorCode.FAILED);
            String exceptionCause = ExceptionUtils.getRootCauseMessage(e);

            if (exceptionCause.contains(UniqueKey.UK_RATECHARGE)) {
                baseDto.setResponseCode(ErrorCode.ENQUIRY_RATEREQUEST_CHARGE_UNIQUE);
            }
        }
        return appUtil.setDesc(baseDto);
    }

    public BaseDto pushSave(EnquiryLog entity) {


        BaseDto baseDto = new BaseDto();

        try {
            enquiryLogRepository.save(entity);
            baseDto.setResponseCode(ErrorCode.SUCCESS);
        } catch (Exception e) {
            // TODO: handle exception
            baseDto.setResponseCode(ErrorCode.FAILED);
            log.info("exception in push api save", e);

        }
        return appUtil.setDesc(baseDto);
    }

}
