package com.efreightsuite.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import com.efreightsuite.dto.ShipmentMilestoneDto;
import com.efreightsuite.enumeration.EventMasterType;
import com.efreightsuite.enumeration.ImportExport;
import com.efreightsuite.enumeration.TransportMode;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.model.CfsReceiveEntry;
import com.efreightsuite.model.ServiceConnection;
import com.efreightsuite.model.Shipment;
import com.efreightsuite.model.ShipmentServiceDetail;
import com.efreightsuite.model.ShipmentServiceEvent;
import com.efreightsuite.repository.CfsReceiveEntryRepository;
import com.efreightsuite.repository.ShipmentRepository;
import com.efreightsuite.repository.ShipmentServiceDetailRepository;
import com.efreightsuite.util.TimeUtil;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.SerializationUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class ShipmentMilestone {

    @Autowired
    private
    ShipmentRepository shipmentRepository;

    @Autowired
    private
    CfsReceiveEntryRepository cfsReceiveEntryRepository;

    @Autowired
    ShipmentServiceDetailRepository shipmentServiceRepository;

    private int currentStatus = 1;

    public List<ShipmentMilestoneDto> milestone(Long shipmentId) {
        List<ShipmentMilestoneDto> mileStoneList = new ArrayList<>();

        Shipment shipment = shipmentRepository.findById(shipmentId);

        ShipmentServiceDetail exportService = null;
        ShipmentServiceDetail importService = null;

        for (ShipmentServiceDetail sd : shipment.getShipmentServiceList()) {
            if (exportService == null && sd.getServiceMaster().getTransportMode() == TransportMode.Air
                    && sd.getServiceMaster().getImportExport() == ImportExport.Export
                    && sd.getServiceMaster().getServiceType() == null) {
                exportService = sd;
            }

            if (importService == null && sd.getServiceMaster().getTransportMode() == TransportMode.Air
                    && sd.getServiceMaster().getImportExport() == ImportExport.Import
                    && sd.getServiceMaster().getServiceType() == null
                    && sd.getImportRef() == null) {
                importService = sd;
            }

        }

        //Getting Export service

        if (exportService == null) {
            throw new RestException(ErrorCode.TRACK_SHIPMENT_EXPORT_SERVICE_EMPTY);
        }


        // Sorting connection list based on ascending order
        if (exportService.getConnectionList() != null) {
            exportService.getConnectionList().sort(ServiceConnection.IdComparator);
        }


        // Step-1 : Booking
        int slNo = 1;
        mileStoneList.add(bookingDate(slNo, shipment, exportService));

        // Step-2 : Pickup
        slNo = 2;
        mileStoneList.add(pickUpDate(slNo, shipment, exportService));

        // Step-3 : Cargo Received
        slNo = 3;
        mileStoneList.add(cargoReceivedDate(slNo, shipment, exportService));

        // Step-4 : Connection etd and eta
        slNo = 4;
        if (exportService.getConnectionList() != null
                && exportService.getConnectionList().size() != 0) {
            for (ServiceConnection serviceConnection : exportService.getConnectionList()) {
                mileStoneList.add(connectionEtd(slNo, serviceConnection, shipment));
                slNo++;

                mileStoneList.add(connectionEta(slNo, serviceConnection, shipment));
                slNo++;
            }
        } else {
            mileStoneList.add(routingEtd(slNo, shipment, exportService));
            slNo++;
            mileStoneList.add(routingEta(slNo, shipment, exportService));
            slNo++;

        }

        if (importService != null) {
            // Step-5 : CAN
            mileStoneList.add(canDate(slNo, shipment, importService));
            slNo++;
            // Step-6 : Delivery
            mileStoneList.add(deliveryDate(slNo, shipment, importService));
        }

        for (ShipmentMilestoneDto dto : mileStoneList) {
            if (dto.getSerialNo() <= currentStatus) {
                dto.setFlightStatus(true);
                dto.setCurrentStatus(currentStatus);
            }
        }

        return mileStoneList;
    }

    private ShipmentMilestoneDto bookingDate(int slNo, Shipment shipment, ShipmentServiceDetail exportService) {
        ShipmentMilestoneDto dto = new ShipmentMilestoneDto();

        Date bookingDate = shipment.getShipmentReqDate();

        dto.setSerialNo(slNo);
        dto.setDate(bookingDate);
        dto.setLabelName("Booking Date");
        dto.setCustomerName(exportService.getParty().getPartyName());
        dto.setServiceName(exportService.getServiceMaster().getServiceName());
        if (bookingDate != null && isBefore(bookingDate, shipment)) {
            currentStatus = 1;
        }
        return dto;
    }

    private ShipmentMilestoneDto pickUpDate(int slNo, Shipment shipment, ShipmentServiceDetail exportService) {
        ShipmentMilestoneDto dto = new ShipmentMilestoneDto();

        Date pickUpDate = null;
        String pickUpPoint = null;

        if (exportService.getPickUpDeliveryPoint() != null) {
            pickUpDate = exportService.getPickUpDeliveryPoint().getPickUpPlanned();
            if (exportService.getPickUpDeliveryPoint().getPickupPoint() != null) {
                pickUpPoint = exportService.getPickUpDeliveryPoint().getPickupPoint()
                        .getPartyName();
            }
        }

        if (pickUpDate != null && isBefore(pickUpDate, shipment)) {
            currentStatus = slNo;
        } else {
            if (pickUpDate == null) {
                Date cargoReceivedDate = null;
                for (ShipmentServiceEvent event : exportService.getEventList()) {
                    if (event.getEventMaster().getEventMasterType() != null && event.getEventMaster().getEventMasterType().equals(EventMasterType.CARGO_RECEIVED)) {
                        cargoReceivedDate = event.getEventDate();
                        break;
                    }
                }
                pickUpDate = cargoReceivedDate;
            }
        }

        dto.setSerialNo(slNo);
        dto.setDate(pickUpDate);
        dto.setLabelName("Pickup Date");
        dto.setPickUpPoint(pickUpPoint);

        return dto;

    }

    private ShipmentMilestoneDto cargoReceivedDate(int slNo, Shipment shipment, ShipmentServiceDetail exportService) {
        ShipmentMilestoneDto dto = new ShipmentMilestoneDto();

        Date cargoReceivedDate = null;
        String recievedPoint = null;
        for (ShipmentServiceEvent event : exportService.getEventList()) {
            if (event.getEventMaster().getEventMasterType() != null && event.getEventMaster().getEventMasterType().equals(EventMasterType.CARGO_RECEIVED)) {
                cargoReceivedDate = event.getEventDate();
                break;
            }
        }

        if (cargoReceivedDate != null && isBefore(cargoReceivedDate, shipment)) {
            currentStatus = slNo;
        }

        List<CfsReceiveEntry> cfsReceiveEntryList = cfsReceiveEntryRepository.findByShipmentServiceUID(exportService.getServiceUid());
        if (cfsReceiveEntryList != null && cfsReceiveEntryList.size() > 0 && cfsReceiveEntryList.get(0).getCfsMaster() != null) {
            recievedPoint = cfsReceiveEntryList.get(0).getCfsMaster().getCfsName();
        }

        dto.setSerialNo(slNo);
        dto.setDate(cargoReceivedDate);
        dto.setLabelName("Cargo Received Date");
        dto.setRecievedPoint(recievedPoint);

        return dto;

    }

    private ShipmentMilestoneDto routingEtd(int slNo, Shipment shipment, ShipmentServiceDetail exportService) {
        ShipmentMilestoneDto dto = new ShipmentMilestoneDto();

        Date etd = exportService.getEtd();
        String polCode = exportService.getPol().getPortCode();
        String polName = exportService.getPol().getPortName();
        String carrierNo = exportService.getRouteNo();
        String carrierName = null;
        if (exportService.getCarrier() != null) {
            carrierName = exportService.getCarrier().getCarrierName();
        }

        ShipmentServiceDetail tempService = SerializationUtils.clone(exportService);

        Date tempEtd = tempService.getEtd();

        if (tempEtd != null && isBefore(tempEtd, shipment)) {
            currentStatus = slNo;
        }

        dto.setSerialNo(slNo);
        dto.setDate(etd);
        dto.setLabelName("ETD");
        dto.setPortCode(polCode);
        dto.setPortName(polName);
        dto.setCarrierNo(carrierNo);
        dto.setCarrierName(carrierName);

        return dto;

    }

    private ShipmentMilestoneDto routingEta(int slNo, Shipment shipment, ShipmentServiceDetail exportService) {
        ShipmentMilestoneDto dto = new ShipmentMilestoneDto();

        Date eta = exportService.getEta();
        String podCode = exportService.getPod().getPortCode();
        String podName = exportService.getPod().getPortName();
        String carrierNo = exportService.getRouteNo();
        String carrierName = null;
        if (exportService.getCarrier() != null) {
            carrierName = exportService.getCarrier().getCarrierName();
        }


        ShipmentServiceDetail tempService = SerializationUtils.clone(exportService);

        Date tempEta = tempService.getEta();

        if (tempEta != null && isBefore(tempEta, shipment)) {
            currentStatus = slNo;
        }

        dto.setSerialNo(slNo);
        dto.setDate(eta);
        dto.setLabelName("ETA");
        dto.setPortCode(podCode);
        dto.setPortName(podName);
        dto.setCarrierNo(carrierNo);
        dto.setCarrierName(carrierName);

        return dto;

    }


    private ShipmentMilestoneDto connectionEtd(int slNo, ServiceConnection serviceConnection, Shipment shipment) {
        ShipmentMilestoneDto dto = new ShipmentMilestoneDto();

        Date etd = serviceConnection.getEtd();
        String polCode = serviceConnection.getPol().getPortCode();
        String polName = serviceConnection.getPol().getPortName();
        String carrierNo = serviceConnection.getFlightVoyageNo();
        String carrierName = null;
        if (serviceConnection.getCarrierMaster() != null) {
            carrierName = serviceConnection.getCarrierMaster().getCarrierName();
        }

        ServiceConnection tempConnection = SerializationUtils.clone(serviceConnection);

        Date tempEtd = tempConnection.getEtd();

        if (tempEtd != null && isBefore(tempEtd, shipment)) {
            currentStatus = slNo;
        }

        dto.setSerialNo(slNo);
        dto.setDate(etd);
        dto.setLabelName("ETD");
        dto.setPortCode(polCode);
        dto.setPortName(polName);
        dto.setCarrierNo(carrierNo);
        dto.setCarrierName(carrierName);

        return dto;
    }

    private ShipmentMilestoneDto connectionEta(int slNo, ServiceConnection serviceConnection, Shipment shipment) {
        ShipmentMilestoneDto dto = new ShipmentMilestoneDto();

        Date eta = serviceConnection.getEta();
        String podCode = serviceConnection.getPod().getPortCode();
        String podName = serviceConnection.getPod().getPortName();
        String carrierNo = serviceConnection.getFlightVoyageNo();
        String carrierName = null;
        if (serviceConnection.getCarrierMaster() != null) {
            carrierName = serviceConnection.getCarrierMaster().getCarrierName();
        }

        ServiceConnection tempConnection = SerializationUtils.clone(serviceConnection);

        Date tempEta = tempConnection.getEta();

        if (tempEta != null && isBefore(tempEta, shipment)) {
            currentStatus = slNo;
        }

        dto.setSerialNo(slNo);
        dto.setDate(eta);
        dto.setLabelName("ETA");
        dto.setPortCode(podCode);
        dto.setPortName(podName);
        dto.setCarrierNo(carrierNo);
        dto.setCarrierName(carrierName);

        return dto;
    }

    private ShipmentMilestoneDto canDate(int slNo, Shipment shipment, ShipmentServiceDetail importService) {
        ShipmentMilestoneDto dto = new ShipmentMilestoneDto();

        Date canDate = null;
        String noticeSentTo = null;

        if (importService != null && importService.getDocumentList() != null
                && importService.getDocumentList().size() > 0
                && importService.getDocumentList().get(0) != null) {
            canDate = importService.getDocumentList().get(0).getCanIssuedDate();
            if (importService.getDocumentList().get(0).getConsignee() != null) {
                noticeSentTo = importService.getDocumentList().get(0).getConsignee()
                        .getPartyName();
            }

        }
        if (canDate != null && isBefore(canDate, shipment)) {
            currentStatus = slNo;
        }

        dto.setSerialNo(slNo);
        dto.setDate(canDate);
        dto.setLabelName("CAN Date");
        dto.setNoticeSentTo(noticeSentTo);

        return dto;

    }

    private ShipmentMilestoneDto deliveryDate(int slNo, Shipment shipment, ShipmentServiceDetail importService) {
        ShipmentMilestoneDto dto = new ShipmentMilestoneDto();

        Date deliveryDate = null;
        String deliveryPoint = null;

        if (importService != null && importService.getDocumentList() != null
                && importService.getDocumentList().size() > 0
                && importService.getDocumentList().get(0) != null) {
            deliveryDate = importService.getDocumentList().get(0).getDoIssuedDate();
            if (importService.getDocumentList().get(0).getConsignee() != null) {
                deliveryPoint = importService.getDocumentList().get(0).getConsignee()
                        .getPartyName();
            }

        }


        if (deliveryDate != null && isBefore(deliveryDate, shipment)) {
            currentStatus = slNo;
        }
        dto.setSerialNo(slNo);
        dto.setDate(deliveryDate);
        dto.setLabelName("Delivery Date");
        dto.setDeliveryPoint(deliveryPoint);

        return dto;

    }

    private boolean isBefore(Date dateToCompare, Shipment shipment) {
        Date currentLocationTime = TimeUtil.getCurrentLocationTime(shipment.getLocation());
        // Make time as 00:00:00
        currentLocationTime.setHours(0);
        currentLocationTime.setMinutes(0);
        currentLocationTime.setSeconds(0);

        // Make time as 00:00:00
        dateToCompare.setHours(0);
        dateToCompare.setMinutes(0);
        dateToCompare.setSeconds(0);
        boolean flag = false;
        if (dateToCompare != null && currentLocationTime != null) {
            flag = dateToCompare.before(currentLocationTime);
        }
        return flag;
    }

}
