package com.efreightsuite.service;

import java.util.ArrayList;
import java.util.Objects;

import com.efreightsuite.common.component.CacheRepository;
import com.efreightsuite.dto.AutoMailMasterSearchDto;
import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.SearchRequest;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.ExceptionHelper;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.exception.UniqueKey;
import com.efreightsuite.model.AutoMailGroupMaster;
import com.efreightsuite.model.AutoMailMaster;
import com.efreightsuite.repository.AutoMailGroupMasterRepository;
import com.efreightsuite.repository.AutoMailMasterRepository;
import com.efreightsuite.search.AutoMailMasterSearchImpl;
import com.efreightsuite.util.AppUtil;
import com.efreightsuite.util.SaaSUtil;
import com.efreightsuite.util.ValidateUtil;
import com.efreightsuite.validation.AutoMailMasterValidator;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.orm.ObjectOptimisticLockingFailureException;
import org.springframework.orm.jpa.JpaObjectRetrievalFailureException;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class AutoMailMasterService {

    @Autowired
    private
    AutoMailMasterRepository autoMailMasterRepository;

    @Autowired
    private
    AppUtil appUtil;

    @Autowired
    private
    CacheRepository cacheRepository;

    @Autowired
    private
    AutoMailMasterValidator autoMailMasterValidator;

    @Autowired
    private
    AutoMailMasterSearchImpl autoMailMasterSearchImpl;

    @Autowired
    SequenceGeneratorService sequenceGeneratorService;

    @Autowired
    private
    AutoMailGroupMasterRepository autoMailGroupMasterRepository;

    @Autowired
    private
    AutoMailGroupMasterService autoMailGroupMasterService;

    public BaseDto get(Long id) {
        log.info("AutoMailMasterService.get method started.");

        BaseDto baseDto = new BaseDto();
        try {

            ValidateUtil.notNull(id, ErrorCode.AUTOMAIL_ID_NOT_NULL);

            AutoMailMaster autoMailMaster = autoMailMasterRepository.findById(id);

            autoMailMaster.getAutoMailGroupMaster().setAutoMailMasterList(null);
            autoMailMaster.setTransAutoMailGroupMaster(autoMailMaster.getAutoMailGroupMaster());
            log.info("Successfully getting AutoMailMaster by id...");

            baseDto.setResponseCode(ErrorCode.SUCCESS);
            baseDto.setResponseObject(autoMailMaster);

        } catch (RestException re) {
            log.error("RestException in AutoMailMasterService.get method while getting the AutoMailMaster : "
                    + re);
            baseDto.setResponseCode(ErrorCode.AUTOMAIL_ID_INVALID);
        } catch (Exception e) {
            log.error(
                    "Exception in AutoMailMasterService.get method while getting the AutoMailMaster : ",
                    e);
            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        log.info("AutoMailMasterService.get method end.");
        return appUtil.setDesc(baseDto);
    }

    public BaseDto create(AutoMailMaster autoMailMaster) {

        log.info("AutoMailMasterService.create method is started.");
        BaseDto baseDto = new BaseDto();
        try {
            ValidateUtil.notNull(autoMailMaster, ErrorCode.AUTOMAIL_NOT_NULL);
            autoMailMasterValidator.validateToCreate(autoMailMaster);
            AutoMailGroupMaster autoMailGroupMaster = cacheRepository.get(SaaSUtil.getSaaSId(), AutoMailGroupMaster.class.getName(),
                    autoMailMaster.getAutoMailGroupMaster().getId(), autoMailGroupMasterRepository);

            autoMailMaster.setAutoMailGroupMaster(autoMailGroupMaster);

            if (CollectionUtils.isEmpty(autoMailGroupMaster.getAutoMailMasterList())) {
                autoMailGroupMaster.setAutoMailMasterList(new ArrayList<>());
            } else {
                for (AutoMailMaster am : autoMailGroupMaster.getAutoMailMasterList()) {
                    am.setAutoMailGroupMaster(autoMailGroupMaster);
                }
            }
            autoMailGroupMaster = autoMailGroupMasterRepository.save(autoMailGroupMaster);
            cacheRepository.remove(SaaSUtil.getSaaSId(), AutoMailGroupMaster.class.getName(), autoMailGroupMaster.getId());
            log.info("AutoMailMaster Saved successfully.....");
            baseDto.setResponseCode(ErrorCode.SUCCESS);
            baseDto.setResponseObject(ErrorCode.SUCCESS);
        } catch (RestException re) {

            log.error("Exception in AutoMailMasterService.create ", re);

            baseDto.setResponseCode(re.getMessage());
        } catch (Exception exception) {

            log.error("Failed to save the Auto Mail Master..", exception);

            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Exception Cause 1 ::: " + exceptionCause1);

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);


            // Duplicate Auto Mail code
            if (exceptionCause1.contains(UniqueKey.UK_AUTOMAIL_MESSAGECODE)) {
                baseDto.setResponseCode(ErrorCode.AUTOMAIL_MESSAGECODE_ALREADY_EXIST);
            }

            if (exceptionCause1.contains(UniqueKey.UK_AUTOMAIL_MESSAGENAME)) {
                baseDto.setResponseCode(ErrorCode.AUTOMAIL_MESSAGENAME_ALREADY_EXIST);
            }


        }


        log.info("AutoMailMasterService.create method is ended.");
        return appUtil.setDesc(baseDto);
    }

    /*
     * search - This method Retrieves the Auto Mail Master
     *
     * @Param AutoMailMasterSearchDto
     *
     * @Return BaseDto
     */
    public BaseDto search(AutoMailMasterSearchDto searchDto) {
        log.info("AutoMailMasterService.search method is started.[" + searchDto + "]");

        BaseDto baseDto = new BaseDto();

        try {

            baseDto.setResponseObject(autoMailMasterSearchImpl.search(searchDto));

            log.info("Successfully Searching AutoMailMaster...");

        } catch (Exception exception) {

            log.info("Exception in Search method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }
        log.info("AutoMailMasterService.search method is ended.");
        return appUtil.setDesc(baseDto);
    }


    public BaseDto searchByExternal(SearchRequest searchRequest) {
        log.info("AutoMailMasterService.search method is started.[" + searchRequest + "]");

        BaseDto baseDto = new BaseDto();

        try {

            //autoMailMasterSearchImpl.search(searchRequest);

            baseDto.setResponseObject(autoMailMasterSearchImpl.searchByExternal(searchRequest));
            baseDto.setResponseCode(ErrorCode.SUCCESS);

            log.info("Successfully Searching AutoMailMaster...");

        } catch (Exception exception) {

            log.info("Exception in Search method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }
        log.info("AutoMailMasterService.delete method is ended.");
        return appUtil.setDesc(baseDto);
    }


    public BaseDto search(SearchRequest searchRequest) {
        log.info("AutoMailMasterService.search method is started.[" + searchRequest + "]");

        BaseDto baseDto = new BaseDto();

        try {

            //autoMailMasterSearchImpl.search(searchRequest);

            baseDto.setResponseObject(autoMailMasterSearchImpl.search(searchRequest));
            baseDto.setResponseCode(ErrorCode.SUCCESS);

            log.info("Successfully Searching AutoMailMaster...");

        } catch (Exception exception) {

            log.info("Exception in Search method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }
        log.info("AutoMailMasterService.delete method is ended.");
        return appUtil.setDesc(baseDto);
    }

    public BaseDto update(AutoMailMaster existingAutoMailMaster) {
        log.info("AutoMailMasterService.update method is started.");
        BaseDto baseDto = new BaseDto();

        try {

            ValidateUtil.notNull(existingAutoMailMaster, ErrorCode.AUTOMAIL_NOT_NULL);

            existingAutoMailMaster.getAutoMailGroupMaster().getId();

            AutoMailGroupMaster autoMailGroupMasterFromDb = cacheRepository.get(SaaSUtil.getSaaSId(), AutoMailGroupMaster.class.getName(),
                    existingAutoMailMaster.getAutoMailGroupMaster().getId(), autoMailGroupMasterRepository);

            AutoMailGroupMaster tmp = autoMailMasterRepository.getAutoGroupId(existingAutoMailMaster.getId());

            existingAutoMailMaster.setAutoMailGroupMaster(autoMailGroupMasterFromDb);

            autoMailMasterValidator.validateToUpdate(existingAutoMailMaster);

            autoMailMasterRepository.getOne(existingAutoMailMaster.getId());


            autoMailMasterRepository.save(existingAutoMailMaster);
            autoMailGroupMasterService.putCache(null, autoMailGroupMasterFromDb.getId());

            if (!Objects.equals(tmp.getId(), autoMailGroupMasterFromDb.getId()) || !tmp.getId().equals(autoMailGroupMasterFromDb.getId())) {
                autoMailGroupMasterService.putCache(null, tmp.getId());
            }

            baseDto.setResponseCode(ErrorCode.SUCCESS);
            baseDto.setResponseObject(ErrorCode.SUCCESS);

        } catch (RestException exception) {

            log.error("Exception in Delete method : ", exception);

            baseDto.setResponseCode(exception.getMessage());
        } catch (ObjectOptimisticLockingFailureException e) {
            log.error("Error in editing Service", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_LOCKED_RECORD);
        } catch (JpaObjectRetrievalFailureException e) {
            log.error("Error in editing Service", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_DELETED_RECORD);
        } catch (Exception exception) {

            log.error("Failed to update the Auto Mail Master..", exception);

            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Exception Cause 1 ::: " + exceptionCause1);

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);


            // Duplicate Auto Mail code
            if (exceptionCause1.contains(UniqueKey.UK_AUTOMAIL_MESSAGECODE)) {
                baseDto.setResponseCode(ErrorCode.AUTOMAIL_MESSAGECODE_ALREADY_EXIST);
            }

            if (exceptionCause1.contains(UniqueKey.UK_AUTOMAIL_MESSAGENAME)) {
                baseDto.setResponseCode(ErrorCode.AUTOMAIL_MESSAGENAME_ALREADY_EXIST);
            }


        }

        log.info("AutoMailMasterService.update method is ended.");
        return appUtil.setDesc(baseDto);

    }

    /*
     * Deletes the Existing AutoMailMaster Based on the Given Id
     *
     * @param Id
     *
     * @Returns BaseDto
     */
    public BaseDto delete(Long id) {
        log.info("AutoMailMasterService.delete method is started.[" + id + "]");

        BaseDto baseDto = new BaseDto();

        try {

            ValidateUtil.notNull(id, ErrorCode.AUTOMAIL_ID_NOT_NULL);

            AutoMailGroupMaster tmp = autoMailMasterRepository.getAutoGroupId(id);

            autoMailMasterRepository.delete(id);
            autoMailGroupMasterService.putCache(null, tmp.getId());

            baseDto.setResponseCode(ErrorCode.SUCCESS);
            return appUtil.setDesc(baseDto);

        } catch (RestException exception) {

            log.error("Exception in Delete method : ", exception);

            baseDto.setResponseCode(exception.getMessage());
        } catch (DataIntegrityViolationException exception) {

            log.error("Exception occured : ", exception);

            ExceptionHelper helper = new ExceptionHelper(exception);

            log.error("Foreign key reference : " + helper.getFkReference());

            baseDto.setResponseCode(ErrorCode.ERROR_FK_CONSTRAINT);
        } catch (Exception exception) {

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            log.error("Exception in Delete method : ", exception);

            String rootCause = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Root Cause of Exception : " + rootCause);

            if (rootCause.contains("FK_")) {
                baseDto.setResponseCode(ErrorCode.CANNOT_DELETE_FK_RECORD);
            }
        }
        log.info("AutoMailMasterService.delete method is ended");
        return appUtil.setDesc(baseDto);

    }


}
