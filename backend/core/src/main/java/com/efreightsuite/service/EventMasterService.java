package com.efreightsuite.service;

import java.util.List;

import com.efreightsuite.common.component.CacheRepository;
import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.EventSearchDto;
import com.efreightsuite.dto.SearchRequest;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.ExceptionHelper;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.exception.UniqueKey;
import com.efreightsuite.model.EventMaster;
import com.efreightsuite.repository.EventMasterRepository;
import com.efreightsuite.search.EventSearchImpl;
import com.efreightsuite.util.AppUtil;
import com.efreightsuite.util.SaaSUtil;
import com.efreightsuite.util.ValidateUtil;
import com.efreightsuite.validation.EventMasterValidator;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.exception.ExceptionUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.orm.ObjectOptimisticLockingFailureException;
import org.springframework.orm.jpa.JpaObjectRetrievalFailureException;
import org.springframework.stereotype.Service;

/**
 * @author tanveer
 */

@Log4j2
@Service
public class EventMasterService {

    @Autowired
    private
    AppUtil appUtil;

    @Autowired
    private
    EventMasterRepository eventMasterRepository;

    @Autowired
    private
    CacheRepository cacheRepository;

    @Autowired
    private
    EventMasterValidator eventMasterValidator;

    @Autowired
    private
    EventSearchImpl eventSearchImpl;


    public BaseDto get(Long id) {

        log.info("get method is called.....[" + id + "]");

        BaseDto baseDto = new BaseDto();

        try {

            EventMaster eventMaster = cacheRepository.get(SaaSUtil.getSaaSId(), EventMaster.class.getName(), id, eventMasterRepository);

            ValidateUtil.notNull(eventMaster, ErrorCode.MASTER_EVENT_NOT_FOUND);

            log.info("Successfully getting TOS by id.....[" + eventMaster.getId() + "]");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(eventMaster);

        } catch (RestException ex) {

            log.error("Exception in get method ", ex);

            baseDto.setResponseCode(ex.getMessage());

        } catch (Exception e) {

            log.error("Exception in get method ", e);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);
    }


    public BaseDto create(EventMaster eventMaster) {

        log.info("Create method is called.....");

        BaseDto baseDto = new BaseDto();

        try {

            eventMasterValidator.validate(eventMaster);

            // Saving in the cache...
            cacheRepository.save(SaaSUtil.getSaaSId(), EventMaster.class.getName(), eventMaster, eventMasterRepository);

            log.info("TosMaster Saved successfully.....");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(ErrorCode.SUCCESS);

        } catch (RestException exception) {

            log.error("BadRequestException occured with error code ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (Exception exception) {

            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Exception Cause 1 ::: " + exceptionCause1);

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            if (exceptionCause1.contains(UniqueKey.UK_EVENT_EVENTCODE)) {
                baseDto.setResponseCode(ErrorCode.MASTER_EVENT_ALREADY_CODE);
            }

            if (exceptionCause1.contains(UniqueKey.UK_EVENT_EVENTNAME)) {
                baseDto.setResponseCode(ErrorCode.MASTER_EVENT_ALREADY_NAME);
            }
        }
        return appUtil.setDesc(baseDto);
    }


    public BaseDto update(EventMaster eventMaster) {


        BaseDto baseDto = new BaseDto();

        try {

            eventMasterValidator.validate(eventMaster);

            cacheRepository.save(SaaSUtil.getSaaSId(), EventMaster.class.getName(), eventMaster, eventMasterRepository);

            log.info("Evnet Updated successfully.");

            cacheRepository.remove(SaaSUtil.getSaaSId(), EventMaster.class.getName(), eventMaster.getId());

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(ErrorCode.SUCCESS);

        } catch (RestException exception) {

            log.error("Exception occured in update method ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (ObjectOptimisticLockingFailureException e) {

            log.error("Optimistic Lock Exception occured ", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_LOCKED_RECORD);

        } catch (JpaObjectRetrievalFailureException e) {

            log.error("Object Already Deleted................", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_DELETED_RECORD);

        } catch (Exception exception) {

            log.error("Exception occurred " + exception);

            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Exception Cause 1 ::: " + exceptionCause1);

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            if (exceptionCause1.contains(UniqueKey.UK_EVENT_EVENTCODE)) {
                baseDto.setResponseCode(ErrorCode.MASTER_EVENT_ALREADY_CODE);
            }

            if (exceptionCause1.contains(UniqueKey.UK_EVENT_EVENTNAME)) {
                baseDto.setResponseCode(ErrorCode.MASTER_EVENT_ALREADY_NAME);
            }
        }

        return appUtil.setDesc(baseDto);
    }


    public BaseDto delete(Long id) {

        log.info("Delete method is Invoked...[" + id + "]");

        BaseDto baseDto = new BaseDto();

        try {

            cacheRepository.delete(SaaSUtil.getSaaSId(), EventMaster.class.getName(), id, eventMasterRepository);

            log.info("TosMaster Deleted Successfully....[" + id + "]");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

        } catch (DataIntegrityViolationException exception) {

            log.error("Exception occured : ", exception);

            ExceptionHelper helper = new ExceptionHelper(exception);

            log.error("Foreign key reference : " + helper.getFkReference());

            baseDto.setResponseCode(ErrorCode.ERROR_FK_CONSTRAINT);

        } catch (Exception exception) {

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            log.error("Exception in Delete method : ", exception);

            String rootCause = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Root Cause of Exception : " + rootCause);

            if (rootCause.contains("FK_")) {
                baseDto.setResponseCode(ErrorCode.CANNOT_DELETE_FK_RECORD);
            }
        }

        return appUtil.setDesc(baseDto);
    }

    public BaseDto search(EventSearchDto searchDto) {

        log.info("Search method is called...." + searchDto);

        BaseDto baseDto = new BaseDto();

        try {

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(eventSearchImpl.search(searchDto));

            log.info("Successfully Searching Tos...");

        } catch (Exception exception) {

            log.error("Exception in Search method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);

    }

    public BaseDto search(SearchRequest searchRequest) {

        log.info("Search method is called...." + searchRequest);

        BaseDto baseDto = new BaseDto();

        try {

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(eventSearchImpl.search(searchRequest));

            log.info("Successfully Searching Event...");

        } catch (Exception exception) {

            log.error("Exception in Search method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);

    }

    public BaseDto searchEventNotInList(SearchRequest searchRequest) {


        BaseDto baseDto = new BaseDto();

        try {

            baseDto.setResponseObject(eventSearchImpl.searchEventNotInList(searchRequest));

            baseDto.setResponseCode(ErrorCode.SUCCESS);

        } catch (Exception exception) {

            log.error("Exception in searchEventNotInList method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);
    }

    public BaseDto getAll() {

        log.info("getAll method is called....");

        BaseDto baseDto = new BaseDto();

        try {

            List<EventMaster> list = eventMasterRepository.findAll();

            log.info("Successfully getting list of tos...");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(list);

        } catch (RestException re) {

            log.error("Exception in getAll method ", re);

            baseDto.setResponseCode(re.getMessage());

        } catch (Exception e) {

            log.error("Exception in getAll method ", e);

            baseDto.setResponseCode(ErrorCode.FAILED);

        }
        return appUtil.setDesc(baseDto);
    }

}
