package com.efreightsuite.service;

import com.efreightsuite.common.component.CacheRepository;
import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.CostCenterSearchDto;
import com.efreightsuite.dto.SearchRequest;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.ExceptionHelper;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.exception.UniqueKey;
import com.efreightsuite.model.CostCenterMaster;
import com.efreightsuite.repository.CostCenterMasterRepository;
import com.efreightsuite.search.CostCenterSearchImpl;
import com.efreightsuite.util.AppUtil;
import com.efreightsuite.util.SaaSUtil;
import com.efreightsuite.util.ValidateUtil;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.exception.ExceptionUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.orm.ObjectOptimisticLockingFailureException;
import org.springframework.orm.jpa.JpaObjectRetrievalFailureException;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class CostCenterMasterService {

    @Autowired
    private
    CostCenterMasterRepository costCenterMasterRepository;

    @Autowired
    private
    CostCenterSearchImpl costCenterSearchImpl;

    @Autowired
    private
    AppUtil appUtil;


    @Autowired
    private
    CacheRepository cacheRepository;


    /*
     * Returns all the CostCenter object as a list based on search keyword
     *
     * @Returns BaseDto
     */
    public BaseDto search(SearchRequest searchRequest) {

        log.info("getAll method is called....");

        BaseDto baseDto = new BaseDto();

        try {
            baseDto.setResponseCode(ErrorCode.SUCCESS);
            baseDto.setResponseObject(costCenterSearchImpl.search(searchRequest));
            return appUtil.setDesc(baseDto);

        } catch (RestException exception) {

            log.error("Exception in getAll method " + exception);

            baseDto.setResponseCode(exception.getMessage());

            return appUtil.setDesc(baseDto);

        } catch (Exception e) {

            log.error("Exception in getAll method ", e);

            baseDto.setResponseCode(ErrorCode.FAILED);
            return appUtil.setDesc(baseDto);
        }

    }

    public BaseDto search(CostCenterSearchDto costSearchDto) {

        log.info("Search method is called....");

        BaseDto baseDto = new BaseDto();

        try {

            baseDto.setResponseObject(costCenterSearchImpl.search(costSearchDto));

            baseDto.setResponseCode(ErrorCode.SUCCESS);

        } catch (Exception exception) {

            log.error("Exception in Search method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);
    }


    /**
     * Method - get
     *
     * @param id Costceneter id
     * @return baseDto
     */

    public BaseDto get(Long id) {

        BaseDto baseDto = new BaseDto();

        try {

            CostCenterMaster costCenterMaster = costCenterMasterRepository.findById(id);

            ValidateUtil.notNull(costCenterMaster, ErrorCode.COSTCENTER_ID_INVALID);

            log.info("Successfully getting costCenter by id...");

            baseDto.setResponseCode(ErrorCode.SUCCESS);
            baseDto.setResponseObject(costCenterMaster);

            return appUtil.setDesc(baseDto);

        } catch (RestException re) {
            log.error("RestException in get method " + re);
            baseDto.setResponseCode(ErrorCode.COSTCENTER_ID_INVALID);
            return appUtil.setDesc(baseDto);
        } catch (Exception e) {

            log.error("get method Exception", e);

            baseDto.setResponseCode(ErrorCode.FAILED);

            return appUtil.setDesc(baseDto);
        }

    }

    /**
     * Method - get
     *
     * @return baseDto
     */
    public BaseDto get(String costCenterCode) {

        log.info("get method is called....");

        BaseDto baseDto = new BaseDto();

        try {

            CostCenterMaster costCenterMaster = costCenterMasterRepository.findByCostCenterCode(costCenterCode);

            ValidateUtil.notNull(costCenterMaster, ErrorCode.COSTCENTER_CODE_NOTFOUND);

            log.info("Successfully getting Costcenter by id...");

            baseDto.setResponseCode(ErrorCode.SUCCESS);
            baseDto.setResponseObject(costCenterMaster);

            return appUtil.setDesc(baseDto);

        } catch (RestException re) {
            log.error("Exception in get method " + re);
            baseDto.setResponseCode(ErrorCode.COSTCENTER_CODE_NOTFOUND);
            return appUtil.setDesc(baseDto);
        } catch (Exception e) {

            log.error("Exception in get method", e);

            baseDto.setResponseCode(ErrorCode.FAILED);

            return appUtil.setDesc(baseDto);
        }

    }

    /*
     * create - This method creates a new CostCenter
	 *
	 * @Param CostCenter master entity
	 *
	 * @Return BaseDto
	 */

    public BaseDto create(CostCenterMaster costCenterMaster) {

        log.info("save method is called.....");
        BaseDto baseDto = new BaseDto();
        try {

            //ValidateUtil.notNull(costCenterMaster.getCompanyMaster(), ErrorCode.COSTCENTER_COMPANY__NOT_NULL);

            // Validating CostCenter code pattern
            ValidateUtil.checkPattern(costCenterMaster.getCostCenterCode(), "costCenterCode", true);
            // Validating CosTCenter code for not null
            ValidateUtil.notNull(costCenterMaster.getCostCenterCode(), ErrorCode.COSTCENTER_CODE_NOT_NULL);
            log.info("CostCenterCode Validated...");
            // Validating Costcenter name pattern
            ValidateUtil.checkPattern(costCenterMaster.getCostCenterName(), "costCenterName", true);
            // Validating Costcenter name for not null
            ValidateUtil.notNull(costCenterMaster.getCostCenterName(), ErrorCode.COSTCENTER_NAME_NOT_NULL);

            costCenterMaster.setCostCenterCode(costCenterMaster.getCostCenterCode().trim());

            costCenterMaster.setCostCenterName(costCenterMaster.getCostCenterName().trim());


            CostCenterMaster costCenterMasterCreated = cacheRepository.save(SaaSUtil.getSaaSId(), CostCenterMaster.class.getName(), costCenterMaster, costCenterMasterRepository);
            log.info("Costcenter Saved successfully.....");
            baseDto.setResponseCode(ErrorCode.SUCCESS);
            baseDto.setResponseObject(costCenterMasterCreated);

        } catch (RestException exception) {

            log.error("Exception occured in update method ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (Exception exception) {

            log.error("Exception occurred " + exception);

            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Exception Cause 1 ::: " + exceptionCause1);

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);
            if (exceptionCause1.contains(UniqueKey.UK_COSTCENTER_COSTCENTERCODE)) {
                baseDto.setResponseCode(ErrorCode.COSTCENTER_CODE_ALREADY_EXIST);
            }

            if (exceptionCause1.contains(UniqueKey.UK_COSTCENTER_COSTCENTERNAME)) {
                baseDto.setResponseCode(ErrorCode.COSTCENTER_NAME_ALREADY_EXIST);
            }
        }
        return appUtil.setDesc(baseDto);
    }

    public BaseDto update(CostCenterMaster existingCostCenter) {

        log.info("update method is called....");
        BaseDto baseDto = new BaseDto();

        try {

            costCenterMasterRepository.getOne(existingCostCenter.getId());

            // Validating Costcenter code pattern
            ValidateUtil.checkPattern(existingCostCenter.getCostCenterCode(), "costcentercode", true);
            // Validating COstcenter code for not null
            ValidateUtil.notNull(existingCostCenter.getCostCenterCode(), ErrorCode.COSTCENTER_CODE_NOT_NULL);
            log.info("CostCenter Code Validated...");

            // Validating Costcenter name pattern
            ValidateUtil.checkPattern(existingCostCenter.getCostCenterName(), "costCenterName", true);
            // Validating Costcenter name for not null
            ValidateUtil.notNull(existingCostCenter.getCostCenterName(), ErrorCode.COSTCENTER_NAME_NOT_NULL);

            existingCostCenter.setCostCenterCode(existingCostCenter.getCostCenterCode().trim());

            existingCostCenter.setCostCenterName(existingCostCenter.getCostCenterName().trim());

            existingCostCenter.setStatus(existingCostCenter.getStatus());


            existingCostCenter = cacheRepository.save(SaaSUtil.getSaaSId(), CostCenterMaster.class.getName(), existingCostCenter, costCenterMasterRepository);

            log.info("Costcenter updated successfully...");

            baseDto.setResponseCode(ErrorCode.SUCCESS);
            baseDto.setResponseObject(existingCostCenter);

        } catch (RestException exception) {

            log.error("Exception occured in update method ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (ObjectOptimisticLockingFailureException e) {

            log.error("Optimistic Lock Exception occured ", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_LOCKED_RECORD);

        } catch (JpaObjectRetrievalFailureException e) {

            log.error("Object Already Deleted................", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_DELETED_RECORD);

        } catch (Exception exception) {

            log.error("Exception occurred " + exception);

            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Exception Cause 1 ::: " + exceptionCause1);

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            if (exceptionCause1.contains(UniqueKey.UK_COSTCENTER_COSTCENTERCODE)) {
                baseDto.setResponseCode(ErrorCode.COSTCENTER_CODE_ALREADY_EXIST);
            }

            if (exceptionCause1.contains(UniqueKey.UK_COSTCENTER_COSTCENTERNAME)) {
                baseDto.setResponseCode(ErrorCode.COSTCENTER_NAME_ALREADY_EXIST);
            }
        }
        return appUtil.setDesc(baseDto);
    }


    public BaseDto delete(Long id) {

        log.info("Delete method is Invoked...[" + id + "]");

        BaseDto baseDto = new BaseDto();

        try {

            cacheRepository.delete(SaaSUtil.getSaaSId(), CostCenterMaster.class.getName(), id, costCenterMasterRepository);

            log.info("CostCenterMaster Deleted Successfully....[" + id + "]");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

        } catch (DataIntegrityViolationException exception) {

            log.error("Exception occured : ", exception);

            ExceptionHelper helper = new ExceptionHelper(exception);

            log.error("Foreign key reference : " + helper.getFkReference());

            baseDto.setResponseCode(ErrorCode.ERROR_FK_CONSTRAINT);

        } catch (Exception exception) {
            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            log.error("Exception in Delete method : ", exception);

            String rootCause = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Root Cause of Exception : " + rootCause);

            if (rootCause.contains("FK_")) {
                baseDto.setResponseCode(ErrorCode.CANNOT_DELETE_FK_RECORD);
            }
        }

        return appUtil.setDesc(baseDto);
    }


}
