package com.efreightsuite.service;

import java.util.List;

import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.SearchRequest;
import com.efreightsuite.dto.TriggerMasterSearchDto;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.ExceptionHelper;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.exception.UniqueKey;
import com.efreightsuite.model.TriggerMaster;
import com.efreightsuite.repository.TriggerMasterRepository;
import com.efreightsuite.search.TriggerMasterImpl;
import com.efreightsuite.util.AppUtil;
import com.efreightsuite.util.ValidateUtil;
import com.efreightsuite.validation.TriggerMasterValidator;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.exception.ExceptionUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class TriggerMasterService {

    @Autowired
    private
    AppUtil appUtil;

    @Autowired
    private
    TriggerMasterRepository triggerMasterRepository;

    @Autowired
    private
    TriggerMasterValidator triggerMasterValidator;

    @Autowired
    private
    TriggerMasterImpl triggerMasterImp1;

    public BaseDto search(TriggerMasterSearchDto searchDto) {

        log.info("Search method is called...." + searchDto);

        BaseDto baseDto = new BaseDto();

        try {
            baseDto.setResponseObject(triggerMasterImp1.search(searchDto));

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            log.info("Successfully Searching Carrier...");

        } catch (Exception exception) {

            log.info("Exception in Search method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);
    }

    public BaseDto getAll() {

        log.info("getAll method is called....");

        BaseDto baseDto = new BaseDto();

        try {

            List<TriggerMaster> triggerMasterList = triggerMasterRepository.findAll();

            log.info("Successfully getting list of Trigger Master...");
            baseDto.setResponseCode(ErrorCode.SUCCESS);
            baseDto.setResponseObject(triggerMasterList);

        } catch (RestException re) {
            log.error("Exception in getAll method ", re);
            baseDto.setResponseCode(re.getMessage());

        } catch (Exception e) {
            log.error("Exception in getAll method ", e);
            baseDto.setResponseCode(ErrorCode.FAILED);

        }
        return appUtil.setDesc(baseDto);
    }

    public BaseDto get(Long id) {

        log.info("get method is called [" + id + "]");

        BaseDto baseDto = new BaseDto();

        try {

            TriggerMaster triggerMaster = triggerMasterRepository.getOne(id);

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(triggerMaster);

        } catch (Exception exception) {

            log.error("Exception in get method", exception);
            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);

    }

    public BaseDto create(TriggerMaster triggerMaster) {

        log.info("Create method is called....");

        BaseDto baseDto = new BaseDto();

        try {

            triggerMasterValidator.validate(triggerMaster);

            triggerMaster = triggerMasterRepository.save(triggerMaster);

            log.info("TriggerMaster Saved successfully.....");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(ErrorCode.SUCCESS);

        } catch (RestException exception) {

            log.error("Exception in Delete method : ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (DataIntegrityViolationException exception) {

            log.error("DataIntegrityViolationException occured", exception);

            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Exception Cause 1 : " + exceptionCause1);

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            if (exceptionCause1.contains(UniqueKey.UK_TRIGGER_TRIGGERCODE)) {
                baseDto.setResponseCode(ErrorCode.TRIGGER_CODE_ALREADY_EXIST);
            }

            if (exceptionCause1.contains(UniqueKey.UK_TRIGGER_TRIGGERNAME)) {
                baseDto.setResponseCode(ErrorCode.TRIGGER_NAME_ALREADY_EXIST);
            }
        }

        return appUtil.setDesc(baseDto);
    }

    public BaseDto update(TriggerMaster existingTriggerTypeMaster) {

        log.info("update method is called....[" + existingTriggerTypeMaster.getId() + "]");

        BaseDto baseDto = new BaseDto();

        try {

            if (existingTriggerTypeMaster.getId() != null) {
                triggerMasterRepository.getOne(existingTriggerTypeMaster.getId());
            }

            triggerMasterValidator.validate(existingTriggerTypeMaster);

            triggerMasterRepository.save(existingTriggerTypeMaster);

            log.info("Trigger Master Updated successfully.....");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(ErrorCode.SUCCESS);

        } catch (RestException exception) {

            log.error("TriggerMaster Exception in update method : ", exception);
            baseDto.setResponseCode(exception.getMessage());

        } catch (DataIntegrityViolationException exception) {

            log.error("DataIntegrityViolationException occured", exception);

            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Exception Cause 1 : " + exceptionCause1);

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            if (exceptionCause1.contains(UniqueKey.UK_TRIGGER_TRIGGERCODE)) {
                baseDto.setResponseCode(ErrorCode.TRIGGER_CODE_ALREADY_EXIST);
            }

            if (exceptionCause1.contains(UniqueKey.UK_TRIGGER_TRIGGERNAME)) {
                baseDto.setResponseCode(ErrorCode.TRIGGER_NAME_ALREADY_EXIST);
            }
        }

        return appUtil.setDesc(baseDto);
    }

    public BaseDto search(SearchRequest searchRequest, String triggerType) {

        log.info("Search method is called...." + searchRequest);

        BaseDto baseDto = new BaseDto();

        try {

            baseDto.setResponseObject(triggerMasterImp1.search(searchRequest, triggerType));

            baseDto.setResponseCode(ErrorCode.SUCCESS);
            log.info("Successfully Searching Tos...");

        } catch (Exception exception) {

            log.error("Exception in Search method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);

    }

    public BaseDto delete(Long id) {

        log.info("Delete method is Invoked...[" + id + "]");

        BaseDto baseDto = new BaseDto();

        try {

            ValidateUtil.notNull(id, ErrorCode.COUNTRY_ID_NOT_NULL);

            triggerMasterRepository.delete(id);

            log.info("TriggerMaster Deleted Successfully....[" + id + "]");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

        } catch (RestException exception) {

            log.error("Exception in Delete method : ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (DataIntegrityViolationException exception) {

            log.error("Exception occured : ", exception);

            ExceptionHelper helper = new ExceptionHelper(exception);

            log.error("Foreign key reference : " + helper.getFkReference());

            baseDto.setResponseCode(ErrorCode.ERROR_FK_CONSTRAINT);

        } catch (Exception exception) {

            log.error("Exception in Delete method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);
    }

}
