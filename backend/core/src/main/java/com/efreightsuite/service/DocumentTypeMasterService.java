package com.efreightsuite.service;

import com.efreightsuite.common.component.CacheRepository;
import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.DocumentTypeSearchDto;
import com.efreightsuite.dto.SearchRequest;
import com.efreightsuite.enumeration.DocumentType;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.ExceptionHelper;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.exception.UniqueKey;
import com.efreightsuite.model.DocumentTypeMaster;
import com.efreightsuite.repository.DocumentTypeMasterRepository;
import com.efreightsuite.search.DocumentTypeSearchImpl;
import com.efreightsuite.util.AppUtil;
import com.efreightsuite.util.SaaSUtil;
import com.efreightsuite.util.ValidateUtil;
import com.efreightsuite.validation.DocumentTypeMasterValidator;
import com.efreightsuite.validation.SearchDtoValidator;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.exception.ExceptionUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.orm.ObjectOptimisticLockingFailureException;
import org.springframework.orm.jpa.JpaObjectRetrievalFailureException;
import org.springframework.stereotype.Service;

/**
 * @author tanveer Date - 07.03.2016 Updated - 11.03.2016
 */

@Service
@Log4j2
public class DocumentTypeMasterService {

    @Autowired
    private
    AppUtil appUtil;

    /**
     * DocumentTypeMasterRepository Object
     */

    @Autowired
    private
    DocumentTypeMasterRepository documentTypeMasterRepository;

    @Autowired
    SearchDtoValidator searchDtoValidator;

    @Autowired
    private
    DocumentTypeMasterValidator documentTypeMasterValidator;

    @Autowired
    private
    CacheRepository cacheRepository;

    @Autowired
    private
    DocumentTypeSearchImpl documentTypeSearchImpl;

    /**
     * Method - getByDocumentTypeId Returns DocumentTypeMaster based on given document type id
     *
     * @return BaseDto
     */

    public BaseDto getByDocumentTypeId(Long id) {

        log.info("getByDocumentTypeId method is called...[" + id + "]");

        final BaseDto baseDto = new BaseDto();

        try {

            DocumentTypeMaster documentTypeMaster = cacheRepository.get(SaaSUtil.getSaaSId(), DocumentTypeMaster.class.getName(), id,
                    documentTypeMasterRepository);

            ValidateUtil.notNull(documentTypeMaster, ErrorCode.DOCUMENT_TYPE_ID_NOT_FOUND);

            log.info("documentTypeMaster" + documentTypeMaster.getVersionLock());

            log.info("getByDocumentTypeId is successfully executed....[" + documentTypeMaster.getId() + "]");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(documentTypeMaster);

        } catch (final RestException exception) {

            log.error("Exception in getByDocumentTypeId method : ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (final Exception exception) {

            log.error("Exception in getByDocumentTypeId method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);
    }

    /**
     * Method - getByDocumentTypeCode Returns DocumentTypeMaster based on given document type code
     *
     * @return BaseDto
     */

    public BaseDto getByDocumentTypeCode(DocumentType documentTypeCode) {

        log.info("getByDocumentTypeCode method is called..." + documentTypeCode);

        final BaseDto baseDto = new BaseDto();

        try {

            final DocumentTypeMaster documentTypeMaster = documentTypeMasterRepository.findByDocumentTypeCode(documentTypeCode);

            log.info("documentTypeMaster" + documentTypeMaster.getVersionLock());
            ValidateUtil.notNull(documentTypeMaster, ErrorCode.DOCUMENT_TYPE_CODE_NOT_FOUND);

            log.info("DocumentType Found Based on the given code : [" + documentTypeMaster.getDocumentTypeCode() + "]");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(documentTypeMaster);

        } catch (RestException exception) {

            log.error("Exception in getByDocumentTypeCode method : ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (Exception exception) {

            log.error("Exception in getByDocumentTypeCode method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);
    }

    /**
     * Method - search Returns list of DocumentTypeMaster based on the search keyword
     *
     * @return BaseDto
     */

    public BaseDto search(DocumentTypeSearchDto searchDto) {

        log.info("Search method is called...." + searchDto);

        BaseDto baseDto = new BaseDto();

        try {

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(documentTypeSearchImpl.search(searchDto));

            log.info("Successfully Searching DocumentType...");

        } catch (Exception exception) {

            log.error("Exception in Search method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);

    }

    public BaseDto search(SearchRequest searchRequest) {

        log.info("Search method is called...." + searchRequest);

        BaseDto baseDto = new BaseDto();

        try {

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(documentTypeSearchImpl.search(searchRequest));

            log.info("Successfully Searching DocumentType...");

        } catch (Exception exception) {

            log.error("Exception in Search method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);

    }

    /**
     * Method - create Creates and returns a new document type master
     *
     * @returns DocumentTypeMaster
     */

    public BaseDto create(DocumentTypeMaster documentTypeMaster) {

        log.info("Create method is called - documentTypeMasterId ..");

        final BaseDto baseDto = new BaseDto();

        try {

            documentTypeMasterValidator.validate(documentTypeMaster);


            cacheRepository.save(SaaSUtil.getSaaSId(), DocumentTypeMaster.class.getName(), documentTypeMaster, documentTypeMasterRepository);

            log.info("DocumentType Saved successfully.....");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(documentTypeMaster);

        } catch (RestException exception) {

            log.error("Exception Cause 1 ::: ", exception);
            baseDto.setResponseCode(exception.getMessage());


        } catch (Exception exception) {

            log.error("Exception occured : ", exception);


            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Exception Cause 1 : " + exceptionCause1);

            // General error condition
            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            // Duplicate document type Code

            if (exceptionCause1.contains(UniqueKey.UK_DOCUMENT_DOCUMENTTYPECODE)) {
                baseDto.setResponseCode(ErrorCode.DOCUMENT_TYPE_CODE_ALREADY_EXIST);

            }

            if (exceptionCause1.contains(UniqueKey.UK_DOCUMENT_DOCUMENTTYPENAME)) {
                baseDto.setResponseCode(ErrorCode.DOCUMENT_TYPE_NAME_ALREADY_EXIST);
            }


        }

        return appUtil.setDesc(baseDto);
    }

    /**
     * Method - update Updates and returns a updated document type master
     *
     * @param existingDocumentTypeMaster
     * @returns DocumentTypeMaster
     */

    public BaseDto update(DocumentTypeMaster existingDocumentTypeMaster) {

        log.info("Update method is called - existingDocumentTypeMaster : [" + existingDocumentTypeMaster.getId() + "]");

        BaseDto baseDto = new BaseDto();

        try {

            ValidateUtil.notNull(existingDocumentTypeMaster.getId(), ErrorCode.DOCUMENT_TYPE_ID_NOT_NULL);

            documentTypeMasterValidator.validate(existingDocumentTypeMaster);

            documentTypeMasterRepository.getOne(existingDocumentTypeMaster.getId());

            cacheRepository.remove(SaaSUtil.getSaaSId(), DocumentTypeMaster.class.getName(), existingDocumentTypeMaster.getId());

            existingDocumentTypeMaster = cacheRepository.save(SaaSUtil.getSaaSId(), DocumentTypeMaster.class.getName(), existingDocumentTypeMaster,
                    documentTypeMasterRepository);


            log.info("documentTypeMaster" + existingDocumentTypeMaster.getVersionLock());


            log.info("DocumentTypeMaster updated successfully....[" + existingDocumentTypeMaster.getId() + "]");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(existingDocumentTypeMaster);

        } catch (RestException exception) {

            log.error("Exception occured with error code ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (ObjectOptimisticLockingFailureException e) {

            log.error("Error in editing", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_LOCKED_RECORD);

        } catch (JpaObjectRetrievalFailureException e) {

            log.error("Error in editing", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_DELETED_RECORD);

        } catch (Exception exception) {

            log.error("Exception occured : ", exception);

            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Exception Cause 1 ::: " + exceptionCause1);


            // General error condition
            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            // Duplicate document type Code

            if (exceptionCause1.contains(UniqueKey.UK_DOCUMENT_DOCUMENTTYPECODE)) {
                baseDto.setResponseCode(ErrorCode.DOCUMENT_TYPE_CODE_ALREADY_EXIST);

            }

            if (exceptionCause1.contains(UniqueKey.UK_DOCUMENT_DOCUMENTTYPENAME)) {
                baseDto.setResponseCode(ErrorCode.DOCUMENT_TYPE_NAME_ALREADY_EXIST);
            }


        }

        return appUtil.setDesc(baseDto);
    }

    /**
     * Deletes the Existing DocumentTypeMaster Based on the Given Id
     *
     * @Returns BaseDto
     */
    public BaseDto delete(Long id) {

        log.info("Delete method is Invoked...[" + id + "]");

        final BaseDto baseDto = new BaseDto();

        try {

            cacheRepository.delete(SaaSUtil.getSaaSId(), DocumentTypeMaster.class.getName(), id, documentTypeMasterRepository);

            log.info("DocumentTypeMaster Deleted Successfully....[" + id + "]");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

        } catch (RestException exception) {

            log.error("Exception in Delete method : ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (DataIntegrityViolationException exception) {

            log.error("Exception occured : ", exception);

            ExceptionHelper helper = new ExceptionHelper(exception);

            log.error("Foreign key reference : " + helper.getFkReference());

            baseDto.setResponseCode(ErrorCode.ERROR_FK_CONSTRAINT);

        } catch (Exception exception) {

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            log.error("Exception in Delete method : ", exception);

            String rootCause = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Root Cause of Exception : " + rootCause);

            if (rootCause.contains("FK_")) {
                baseDto.setResponseCode(ErrorCode.CANNOT_DELETE_FK_RECORD);
            }
        }

        return appUtil.setDesc(baseDto);
    }

}
