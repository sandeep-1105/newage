package com.efreightsuite.service;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.PreBookingMawbSearchDto;
import com.efreightsuite.enumeration.StockStatus;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.model.StockGeneration;
import com.efreightsuite.repository.StockRepository;
import com.efreightsuite.util.AppUtil;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;


@Service
@Log4j2
public class StockGenerationService {


    @Autowired
    private
    StockRepository stockRepository;


    @Autowired
    private
    AppUtil appUtil;


    @Transactional
    public List<StockGeneration> generateStock(StockGeneration stockGeneration) {
        log.info("Stock Generation -> method is called....");
        List<StockGeneration> stockList = new ArrayList<>();

        for (long i = stockGeneration.getStartingNo(); i <= stockGeneration.getEndingNo(); i++) {
            StockGeneration sg = new StockGeneration();
            sg.setConsol(null);
            sg.setServiceUid(null);
            sg.setShipmentUid(null);
            sg.setCarrier(stockGeneration.getCarrier());
            sg.setCarrierCode(stockGeneration.getCarrier().getCarrierCode());
            sg.setReceivedOnCheck(stockGeneration.getReceivedOnCheck());
            sg.setPor(stockGeneration.getPor());
            sg.setPortCode(stockGeneration.getPortCode());
            sg.setReceivedOn(stockGeneration.getReceivedOn());
            sg.setCarrierRefNo(stockGeneration.getCarrierRefNo());
            sg.setStartingNo(i);
            sg.setEndingNo(stockGeneration.getEndingNo());
            if (stockGeneration.getRemindNo() != null && i == stockGeneration.getRemindNo()) {
                sg.setRemindNo(stockGeneration.getRemindNo());
            } else {
                sg.setRemindNo(null);
            }
            sg.setAwbNo(i);
            sg.setCheckDigit(i % 7);

            sg.setStockStatus(StockStatus.Available);
            sg.setMawbNo(sg.getCarrier().getCarrierNo() + String.format("%07d", sg.getAwbNo()) + sg.getCheckDigit());
            stockList.add(sg);
            log.info("Stock Generation added -> " + sg);
        }
            /*if(stockGeneration.getRemindNo()!=null){
				 StockGeneration sg = stockRepository.updateRemindNo(stockGeneration.getCarrier().getId(),stockGeneration.getPor().getId());
				 if(sg!=null){
					 StockGeneration tmpObj = new StockGeneration();
					 tmpObj.setId(sg.getId());
					 tmpObj.setRemindNo(sg.getRemindNo());
					 sg.setRemindNo(null);
					 stockRepository.save(sg);
				 }
				 
			 }*/
        stockList = stockRepository.save(stockList);
        log.info("Stock Generation saved");

        return stockList;

    }


    public BaseDto getMawbBasedOnCarrierAndPorAndCount(PreBookingMawbSearchDto data) {
        BaseDto baseDto = new BaseDto();
        baseDto.setResponseCode(ErrorCode.SUCCESS);
        Page<String> mawb = null;
        if (data.getMawbList() != null && data.getMawbList().length > 0) {
            log.info("data.getMawbList() " + data.getMawbList());
            mawb = stockRepository.findMawbsByCarrierAndPorAndCount(data.getCarrierId(), data.getPorId(), data.getMawbList(), new PageRequest(0, data.getCount()));
        } else {
            log.info("data.getMawbList() " + data.getCarrierId() + "  ---   " + data.getPorId());
            mawb = stockRepository.findMawbsByCarrierAndPorAndCount(data.getCarrierId(), data.getPorId(), new PageRequest(0, data.getCount()));

        }
        baseDto.setResponseObject(mawb.getContent());

        return appUtil.setDesc(baseDto);
    }


}
