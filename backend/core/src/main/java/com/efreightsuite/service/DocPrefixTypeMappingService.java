package com.efreightsuite.service;

import com.efreightsuite.common.component.CacheRepository;
import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.ExceptionHelper;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.model.DocPrefixTypeMapping;
import com.efreightsuite.model.DocumentPrefixMaster;
import com.efreightsuite.repository.DocPrefixTypeMappingRepository;
import com.efreightsuite.repository.DocumentPrefixMasterRepository;
import com.efreightsuite.util.AppUtil;
import com.efreightsuite.util.SaaSUtil;
import com.efreightsuite.util.ValidateUtil;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class DocPrefixTypeMappingService {

    @Autowired
    private
    AppUtil appUtil;

    @Autowired
    private
    CacheRepository cacheRepository;

    @Autowired
    private
    DocPrefixTypeMappingRepository docPrefixTypeMappingRepository;


    @Autowired
    private
    DocumentPrefixMasterRepository documentPrefixMasterRepository;


    public BaseDto delete(Long id) {

        log.info("delete  method is called....[ " + id + " ] ");

        BaseDto baseDto = new BaseDto();

        try {

            DocPrefixTypeMapping dm = cacheRepository.get(SaaSUtil.getSaaSId(), DocPrefixTypeMapping.class.getName(), id, docPrefixTypeMappingRepository);

            DocumentPrefixMaster dpm = cacheRepository.get(SaaSUtil.getSaaSId(), DocumentPrefixMaster.class.getName(), dm.getDocumentPrefixMaster().getId(), documentPrefixMasterRepository);

            for (DocPrefixTypeMapping docPrefixTypeMapping : dpm.getDocPrefixTypeMappingList()) {
                if (docPrefixTypeMapping.getId().toString().equals(dm.getId().toString())) {
                    dpm.getDocPrefixTypeMappingList().remove(docPrefixTypeMapping);
                    break;
                }
            }

            cacheRepository.delete(SaaSUtil.getSaaSId(), DocPrefixTypeMapping.class.getName(), id, docPrefixTypeMappingRepository);

            cacheRepository.save(SaaSUtil.getSaaSId(), DocumentPrefixMaster.class.getName(), dpm, documentPrefixMasterRepository);

            log.info("Successfully deleted port by id...[ " + id + " ] ");

            baseDto.setResponseCode(ErrorCode.SUCCESS);


        } catch (DataIntegrityViolationException exception) {

            log.error("Exception occured : ", exception);

            ExceptionHelper helper = new ExceptionHelper(exception);

            log.error("Foreign key reference : " + helper.getFkReference());

            baseDto.setResponseCode(ErrorCode.ERROR_FK_CONSTRAINT);

        } catch (Exception exception) {

            log.error("Exception in Create method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }
        return appUtil.setDesc(baseDto);

    }

    public BaseDto getById(Long id) {

        log.info("getById method is called....[" + id + "]");

        BaseDto baseDto = new BaseDto();

        try {

            DocPrefixTypeMapping docPrefixTypeMapping = cacheRepository.get(SaaSUtil.getSaaSId(), DocPrefixTypeMapping.class.getName(), id, docPrefixTypeMappingRepository);

            ValidateUtil.notNull(docPrefixTypeMapping, ErrorCode.DOCUMENT_PREFIX_MAPPING__ID_NOT_FOUND);

            log.info("Successfully getting port by given id...[" + id + "]");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            for (DocPrefixTypeMapping dpm : docPrefixTypeMapping.getDocumentPrefixMaster().getDocPrefixTypeMappingList()) {
                dpm.setDocumentPrefixMaster(null);
            }

            baseDto.setResponseObject(docPrefixTypeMapping);

        } catch (DataIntegrityViolationException exception) {

            log.error("Exception occured : ", exception);

            ExceptionHelper helper = new ExceptionHelper(exception);

            log.error("Foreign key reference : " + helper.getFkReference());

            baseDto.setResponseCode(ErrorCode.ERROR_FK_CONSTRAINT);

        } catch (RestException exception) {

            log.error("Exception in getById method ", exception);

            baseDto.setResponseCode(ErrorCode.DOCUMENT_PREFIX_MAPPING__ID_NOT_FOUND);
        } catch (Exception exception) {

            log.error("Exception in getById method", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);
    }


}
