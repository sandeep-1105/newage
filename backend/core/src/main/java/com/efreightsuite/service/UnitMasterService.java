package com.efreightsuite.service;

import com.efreightsuite.common.component.CacheRepository;
import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.SearchRequest;
import com.efreightsuite.dto.UnitSearchDto;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.ExceptionHelper;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.exception.UniqueKey;
import com.efreightsuite.model.UnitMaster;
import com.efreightsuite.repository.UnitMasterRepository;
import com.efreightsuite.search.UnitSearchImpl;
import com.efreightsuite.util.AppUtil;
import com.efreightsuite.util.SaaSUtil;
import com.efreightsuite.util.ValidateUtil;
import com.efreightsuite.validation.UnitMasterValidator;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.exception.ExceptionUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.orm.ObjectOptimisticLockingFailureException;
import org.springframework.orm.jpa.JpaObjectRetrievalFailureException;
import org.springframework.stereotype.Service;

/**
 * @author tanveer
 */

@Service
@Log4j2
public class UnitMasterService {

    @Autowired
    private
    AppUtil appUtil;

    @Autowired
    private
    UnitMasterRepository unitMasterRepository;

    @Autowired
    private
    UnitMasterValidator unitMasterValidator;

    @Autowired
    private
    CacheRepository cacheRepository;

    @Autowired
    private
    UnitSearchImpl unitSearchImpl;


    public BaseDto get(Long id) {

        log.info("get method is called [" + id + "]");

        BaseDto baseDto = new BaseDto();

        try {

            ValidateUtil.notNull(id, ErrorCode.UNIT_ID_NOT_NULL);

            UnitMaster unitMaster = cacheRepository.get(SaaSUtil.getSaaSId(), UnitMaster.class.getName(), id, unitMasterRepository);

            ValidateUtil.notNull(unitMaster, ErrorCode.UNIT_ID_NOT_FOUND);

            log.info("Successfully getting Unit by id.....[" + unitMaster.getId() + "]");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(unitMaster);

        } catch (RestException exception) {

            log.error("Exception in get method ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (Exception e) {

            log.error("Exception in get method ", e);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);
    }


    public BaseDto getByUnitCode(String unitCode) {


        BaseDto baseDto = new BaseDto();

        try {

            ValidateUtil.notNull(unitCode, ErrorCode.UNIT_ID_NOT_NULL);

            UnitMaster unitMaster = unitMasterRepository.getByUnitCode(unitCode);

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(unitMaster);

        } catch (RestException exception) {

            log.error("Exception in get method ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (Exception e) {

            log.error("Exception in get method ", e);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);
    }


    public BaseDto create(UnitMaster unitMaster) {

        log.info("Create method is called.....");

        BaseDto baseDto = new BaseDto();

        try {

            unitMasterValidator.validate(unitMaster);

            // Saving in the cache...
            UnitMaster unitMasterUpdated = cacheRepository.save(SaaSUtil.getSaaSId(), UnitMaster.class.getName(), unitMaster, unitMasterRepository);

            log.info("UnitMaster Saved successfully.....[" + unitMasterUpdated.getId() + "]");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(unitMasterUpdated);

        } catch (RestException exception) {

            log.error("Occured with error code. ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (Exception exception) {

            log.error("Exception in Create method : ", exception);

            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Exception Cause 1 ::: " + exceptionCause1);

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            if (exceptionCause1.contains(UniqueKey.UK_UNIT_UNITCODE)) {
                baseDto.setResponseCode(ErrorCode.UNIT_CODE_ALREADY_EXIST);
            }

            if (exceptionCause1.contains(UniqueKey.UK_UNIT_UNITNAME)) {
                baseDto.setResponseCode(ErrorCode.UNIT_NAME_ALREADY_EXIST);
            }
        }

        return appUtil.setDesc(baseDto);
    }

    public BaseDto update(UnitMaster existingUnitMaster) {

        log.info("update method is called....ID [ " + existingUnitMaster.getId() + "]");

        BaseDto baseDto = new BaseDto();

        try {

            unitMasterValidator.validate(existingUnitMaster);
            ValidateUtil.notNull(existingUnitMaster.getId(), ErrorCode.UNIT_ID_NOT_NULL);

            log.info("existingTosMaster : " + existingUnitMaster);

            unitMasterRepository.getOne(existingUnitMaster.getId());

            existingUnitMaster = cacheRepository.save(SaaSUtil.getSaaSId(), UnitMaster.class.getName(), existingUnitMaster,
                    unitMasterRepository);

            log.info("UnitMaster Updated successfully.....[" + existingUnitMaster.getId() + "]");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(existingUnitMaster);


        } catch (RestException exception) {

            log.error("Exception occured with error code ", exception);

            baseDto.setResponseCode(exception.getMessage());
        } catch (ObjectOptimisticLockingFailureException e) {

            log.error("Error in editing Unit", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_LOCKED_RECORD);

        } catch (JpaObjectRetrievalFailureException e) {

            log.error("Error in editing Unit", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_DELETED_RECORD);

        } catch (Exception exception) {
            log.error("Exception in Create method : ", exception);

            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Exception Cause 1 ::: " + exceptionCause1);

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            if (exceptionCause1.contains(UniqueKey.UK_UNIT_UNITCODE)) {
                baseDto.setResponseCode(ErrorCode.UNIT_CODE_ALREADY_EXIST);
            }

            if (exceptionCause1.contains(UniqueKey.UK_UNIT_UNITNAME)) {
                baseDto.setResponseCode(ErrorCode.UNIT_NAME_ALREADY_EXIST);
            }
        }
        return appUtil.setDesc(baseDto);
    }


    public BaseDto delete(Long id) {

        log.info("Delete method is Invoked...[" + id + "]");

        BaseDto baseDto = new BaseDto();

        try {

            ValidateUtil.notNull(id, ErrorCode.UNIT_ID_NOT_NULL);

            cacheRepository.delete(SaaSUtil.getSaaSId(), UnitMaster.class.getName(), id, unitMasterRepository);

            log.info("UnitMaster Deleted Successfully....[" + id + "]");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

        } catch (RestException exception) {

            log.error("Exception in Delete method : ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (DataIntegrityViolationException exception) {

            log.error("Exception occured : ", exception);

            ExceptionHelper helper = new ExceptionHelper(exception);

            log.error("Foreign key reference : " + helper.getFkReference());

            baseDto.setResponseCode(ErrorCode.ERROR_FK_CONSTRAINT);

        } catch (Exception exception) {
            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            log.error("Exception in Delete method : ", exception);

            String rootCause = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Root Cause of Exception : " + rootCause);

            if (rootCause.contains("FK_")) {
                baseDto.setResponseCode(ErrorCode.CANNOT_DELETE_FK_RECORD);
            }
        }

        return appUtil.setDesc(baseDto);
    }


    public BaseDto search(UnitSearchDto searchDto) {

        log.info("Search method is called...." + searchDto);

        BaseDto baseDto = new BaseDto();

        try {
            baseDto.setResponseObject(unitSearchImpl.search(searchDto));
            baseDto.setResponseCode(ErrorCode.SUCCESS);
            log.info("Successfully Searching Unit...");

        } catch (Exception exception) {

            log.info("Exception in Search method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);
    }

    public BaseDto search(SearchRequest searchRequest) {

        log.info("Search method is called...." + searchRequest);

        BaseDto baseDto = new BaseDto();

        try {
            baseDto.setResponseObject(unitSearchImpl.search(searchRequest));
            baseDto.setResponseCode(ErrorCode.SUCCESS);
            log.info("Successfully Searching Unit...");

        } catch (Exception exception) {

            log.info("Exception in Search method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);
    }


}
