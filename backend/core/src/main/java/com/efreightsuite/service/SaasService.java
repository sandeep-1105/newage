package com.efreightsuite.service;

import java.util.Date;
import java.util.Optional;
import java.util.UUID;

import javax.transaction.Transactional;

import com.efreightsuite.client.RestAPIClient;
import com.efreightsuite.common.component.EmailRequestService;
import com.efreightsuite.configuration.JDBCUtil;
import com.efreightsuite.configuration.TenantContext;
import com.efreightsuite.configuration.TenantService;
import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.LocationSetupDTO;
import com.efreightsuite.dto.SaaSDto;
import com.efreightsuite.enumeration.common.LocationSetupSection;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.model.common.LocationSetup;
import com.efreightsuite.repository.common.LocationSetupRepository;
import com.efreightsuite.search.SaasServiceImpl;
import com.efreightsuite.util.AppUtil;
import com.efreightsuite.util.OtpGenerator;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import static com.efreightsuite.constants.Constants.*;

@Service
@Log4j2
public class SaasService {

    private static final String INIT_LOGGER_PREFIX = "\"Basic data loaded for ";
    private static final String INIT_LOGGER_PREFIX_ACT_TABLE = "Activity Table Creation - File";

    @Value("${efs.resource.path}")
    private
    String efsResourcePath;

    @Value("${efs.edi.application.internal.url}")
    private
    String ediInternalUrl;

    @Value("${efs.report.application.internal.url}")
    private
    String reportingInternalUrl;

    @Value("${spring.profiles.active}")
    private
    String efsActiveProfile;

    @Value("${newage.saas.jdbc.dbtype}")
    private
    String dbType;

    @Value("${newage.saas.jdbc.driver}")
    private
    String jdbcDriver;

    @Value("${newage.saas.jdbc.url}")
    private
    String dbUrl;

    @Value("${newage.saas.jdbc.username}")
    private
    String userName;

    @Value("${newage.saas.jdbc.password}")
    private
    String password;

    @Value("${newage.saas.jdbc.maxtotal}")
    private
    int maxtotal;

    @Value("${newage.saas.jdbc.maxidle}")
    private
    int maxidle;

    @Value("${newage.saas.jdbc.minidle}")
    private
    int minidle;

    @Value("${newage.saas.jdbc.validationquery}")
    private
    String validationquery;

    @Value("${newage.saas.jdbc.validationquerytimeout}")
    private
    int validationquerytimeout;

    @Autowired
    private
    SaasServiceImpl saasServiceImpl;

    @Autowired
    AppUtil appUtil;

    @Autowired
    private
    TenantService tenantService;

    @Autowired
    private
    EmailRequestService emailRequestService;

    @Autowired
    private LocationSetupRepository locationSetupRepository;

    @Autowired
    private
    InitSetupService initSetupService;

    @Autowired
    private Environment environment;

    @Autowired
    private RestAPIClient restApiClient;

    @Autowired
    private SaasIdGeneratorService saasIdGeneratorService;

    @Transactional
    public LocationSetup sendTrailLink(LocationSetup locationSetup) {

        locationSetup.setSaasId(saasIdGeneratorService.generateSaasId(locationSetup));
        locationSetup = locationSetupRepository.save(locationSetup);

        log.info("Location Setup Saved (" + locationSetup.getId() + ")");

        if (locationSetup.isTab(LocationSetupSection.Trail)) {
            emailRequestService.sendTrailMail(locationSetup.getId(), locationSetup.getActivationKey(), locationSetup.getEmail(), null, locationSetup.getCurrentSection(), locationSetup.getSaasId());
        }
        return locationSetup;
    }

    @Transactional
    public LocationSetup saveAndSendMail(LocationSetup locationSetup) {
        setAndUpdateLocationSetupId(locationSetup);
        locationSetup = locationSetupRepository.save(locationSetup);
        log.info("Location Setup Saved (" + locationSetup.getId() + ")");
        if (locationSetup.isTab(LocationSetupSection.Completed)) {
            locationSetup.setActivationKey(OtpGenerator.generateAlphanumeric(8));
            emailRequestService.sendAccountActivationMail(locationSetup.getId(), locationSetup.getActivationKey(), locationSetup.getEmail(), null);
        }
        return locationSetup;
    }

    private void setAndUpdateLocationSetupId(LocationSetup locationSetup) {
        Optional<LocationSetup> locationSetupOptional = locationSetupRepository.findByCompanyRegisteredName(locationSetup.getCompanyRegisteredName());
        locationSetupOptional.ifPresent(locationSetup1 -> locationSetup.setId(locationSetup1.getId()));
    }

    @Async
    /*TODO populate the data for the new tenant in a transactional context 
     */
    public void registerNewCompany(LocationSetupDTO locationSetupDTO, LocationSetup locationSetup) {
        try {
            createSaasProcess(locationSetupDTO);
            loadInitData(locationSetupDTO.getSaasId());
            createCompanyAndLocationProcess(locationSetupDTO);
            //after all the data has been loaded finally updating user status as activated.
            locationSetupRepository.save(locationSetup);
        } catch (Exception e) {
            log.error("Company Setup failed ", e);

        }
    }

    private void createSaasProcess(LocationSetupDTO locationSetup) {


        String saasPassword = "efs" + UUID.randomUUID().toString().replace("-", "").substring(0, 5);

        String saasUsername = locationSetup.getSaasId().toLowerCase();
        String saasId = locationSetup.getSaasId().toUpperCase();


        log.info("SaaS Creation Request Received " + new Date());
        log.info("Company Name : " + locationSetup);
        log.info("SaaS Id : " + saasId);
        log.info("Database Type : " + dbType);
        log.info("Database URL : " + dbUrl);
        log.info("UserName : " + saasUsername);
        log.info("Password : " + saasPassword);

        log.info("Schema Creation Started : " + new Date());
        boolean dbCreated = JDBCUtil.createSchema(dbType, dbUrl, userName, password, saasUsername, saasPassword);

        if (!dbCreated) {
            log.error("Schema Creation Failed : " + new Date());
            log.error("Schema Creation Failed SaaS Id : " + saasId);
            throw new RestException(ErrorCode.UR_SAAS_DB_CREATION_FAILED);
        } else {
            log.info("Schema Creation finished : " + new Date());
        }


        log.info("Writing datasource  configuration started : " + new Date());

        String fileName = efsResourcePath + "/efs-data-source/application-" + efsActiveProfile + "-datasource.yml";

        log.info("Writing datasource File Name : " + fileName);

        if (dbType.equalsIgnoreCase("mysql")) {
            //TODO : to externalise this mysql format url
            dbUrl = dbUrl.substring(0, dbUrl.lastIndexOf('/') + 1) + saasUsername + "?useSSL=false";
        } else if (dbType.equalsIgnoreCase("h2")) {
            dbUrl = "jdbc:h2:mem:efsdevtest;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=FALSE";
        }
        boolean fileWritten = JDBCUtil.addTenantInYml(fileName, dbUrl, saasUsername, saasPassword, jdbcDriver, saasId,
                locationSetup.getCompanyRegisteredName(), maxtotal, maxidle, minidle, validationquery, validationquerytimeout);

        if (!fileWritten) {
            log.error("Writing datasource  configuration failed : " + new Date());
            log.error("Writing datasource  configuration failed SaaS Id : " + saasId);
            throw new RestException(ErrorCode.UR_SAAS_DB_DATASOURCE_WRITE_FILE_FAILED);
        } else {
            log.info("Writing datasource configuration completed : " + new Date());
        }

        SaaSDto saaSDto = new SaaSDto(saasId, locationSetup.getCompanyRegisteredName(), jdbcDriver, dbUrl, saasUsername, saasPassword);
        restApiClient.postAsync(reportingInternalUrl + "/api/v1/report/saas/added", saaSDto, "COM");
        SaaSDto saaSDto2 = new SaaSDto(saasId, locationSetup.getCompanyRegisteredName(), jdbcDriver, dbUrl, saasUsername, saasPassword);
        restApiClient.postAsync(ediInternalUrl + "/api/v1/edi/saas/added", saaSDto2, "COM");


        log.info("Table Creation Started : " + new Date());
        log.info("Table Creation for SaaS : " + saasId);

        boolean tableCreated = JDBCUtil.tableCreation(
                environment,
                dbType,
                dbUrl,
                saasUsername,
                saasUsername,
                saasPassword);

        if (!tableCreated) {
            log.error("Table Creation failed : " + new Date());
            log.error("Table Creation failed SaaS Id : " + saasId);
            throw new RestException(ErrorCode.UR_SAAS_DB_TABLE_CREATION_FAILED);
        } else {
            log.info("Table Creation completed : " + new Date());
        }

        log.info("Activity Table Creation Started : " + new Date());
        log.info("Activity Table Creation for SaaS : " + saasId);

        String activityScriptFile1 = efsResourcePath + ACTIVIT_ORACLE_SCRIPT_PATH_SUFFIX + "activiti.oracle.create.engine.sql";
        if (dbType.equalsIgnoreCase("mysql")) {
            activityScriptFile1 = efsResourcePath + ACTIVIT_SQL_SCRIPT_PATH_SUFFIX + "activiti.mysql.create.engine.sql";
        } else if (dbType.equalsIgnoreCase("h2")) {
            activityScriptFile1 = efsResourcePath + ACTIVIT_H2_SCRIPT_PATH_SUFFIX + "activiti.h2.create.engine.sql";
        }
        log.info(INIT_LOGGER_PREFIX_ACT_TABLE + " 1 : " + activityScriptFile1);

        boolean activityScript1 = JDBCUtil.scriptExecute(environment, dbType, dbUrl, saasUsername, saasPassword, activityScriptFile1);

        if (!activityScript1) {
            log.error(INIT_LOGGER_PREFIX_ACT_TABLE + " 1 failed : " + new Date());
            throw new RestException(ErrorCode.UR_SAAS_DB_ACTIVITY_1_TABLE_CREATION_FAILED);
        } else {
            log.info(INIT_LOGGER_PREFIX_ACT_TABLE + " 1 completed : " + new Date());
        }


        String activityScriptFile2 = efsResourcePath + ACTIVIT_ORACLE_SCRIPT_PATH_SUFFIX + "activiti.oracle.create.history.sql";
        if (dbType.equalsIgnoreCase("mysql")) {
            activityScriptFile2 = efsResourcePath + ACTIVIT_SQL_SCRIPT_PATH_SUFFIX + "activiti.mysql.create.history.sql";
        } else if (dbType.equalsIgnoreCase("h2")) {
            activityScriptFile2 = efsResourcePath + ACTIVIT_H2_SCRIPT_PATH_SUFFIX + "activiti.h2.create.history.sql";
        }
        log.info(INIT_LOGGER_PREFIX_ACT_TABLE + " 2 : " + activityScriptFile2);

        boolean activityScript2 = JDBCUtil.scriptExecute(environment, dbType, dbUrl, saasUsername, saasPassword, activityScriptFile2);

        if (!activityScript2) {
            log.error(INIT_LOGGER_PREFIX_ACT_TABLE + " 2 failed : " + new Date());
            throw new RestException(ErrorCode.UR_SAAS_DB_ACTIVITY_2_TABLE_CREATION_FAILED);
        } else {
            log.info(INIT_LOGGER_PREFIX_ACT_TABLE + " 2 completed : " + new Date());
        }


        String activityScriptFile3 = efsResourcePath + ACTIVIT_ORACLE_SCRIPT_PATH_SUFFIX + "activiti.oracle.create.identity.sql";
        if (dbType.equalsIgnoreCase("mysql")) {
            activityScriptFile3 = efsResourcePath + ACTIVIT_SQL_SCRIPT_PATH_SUFFIX + "activiti.mysql.create.identity.sql";
        } else if (dbType.equalsIgnoreCase("h2")) {
            activityScriptFile3 = efsResourcePath + ACTIVIT_H2_SCRIPT_PATH_SUFFIX + "activiti.h2.create.identity.sql";
        }
        log.info(INIT_LOGGER_PREFIX_ACT_TABLE + " 3 : " + activityScriptFile3);

        boolean activityScript3 = JDBCUtil.scriptExecute(environment, dbType, dbUrl, saasUsername, saasPassword, activityScriptFile3);

        if (!activityScript3) {
            log.error(INIT_LOGGER_PREFIX_ACT_TABLE + " 3 failed : " + new Date());
            throw new RestException(ErrorCode.UR_SAAS_DB_ACTIVITY_3_TABLE_CREATION_FAILED);
        } else {
            log.info(INIT_LOGGER_PREFIX_ACT_TABLE + " 3 completed : " + new Date());
        }


        log.info("Datasource loaded to SessionDataRouting created  (" + saasId + ")");

        if (dbType.equalsIgnoreCase("mysql")) {
            dbUrl = environment.acceptsProfiles("docker")
                    ? "jdbc:mysql://db/" + saasUsername + "?useSSL=false"
                    : "jdbc:mysql://localhost/" + saasUsername + "?useSSL=false";

        } else if (dbType.equalsIgnoreCase("h2")) {
            dbUrl = "jdbc:h2:mem:efsdevtest;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=FALSE";
        }
        tenantService.loadNewDataSource(saasId, locationSetup.getCompanyRegisteredName(),
                jdbcDriver, dbUrl, saasUsername, saasPassword);

        log.info("Datasource loaded to SessionDataRouting successfully  (" + saasId + ")");
    }

    private void loadInitData(String saasId) {

        log.info("Basic data loaded to SaaS : " + saasId);
        TenantContext.setCurrentTenant(saasId);

        BaseDto baseDto = new BaseDto();

        log.info(INIT_LOGGER_PREFIX + "Error Message : " + new Date());
        initSetupService.loadErrorMessage(baseDto);

        log.info(INIT_LOGGER_PREFIX + "Regular Expression : " + new Date());
        initSetupService.loadRegularExpression(baseDto);

        log.info(INIT_LOGGER_PREFIX + "Feature : " + new Date());
        initSetupService.loadFeature(baseDto);

        log.info(INIT_LOGGER_PREFIX + "Country : " + new Date());
        initSetupService.loadCountry(baseDto);

        log.info(INIT_LOGGER_PREFIX + "State : " + new Date());
        initSetupService.loadState(baseDto);

        log.info(INIT_LOGGER_PREFIX + "City : " + new Date());
        initSetupService.loadCityMaster(baseDto);

        log.info(INIT_LOGGER_PREFIX + "Currency : " + new Date());
        initSetupService.loadCurrency(baseDto);

        log.info(INIT_LOGGER_PREFIX + "Period : " + new Date());
        initSetupService.loadPeriod(baseDto);

        log.info(INIT_LOGGER_PREFIX + "Party Type : " + new Date());
        initSetupService.loadPartyType(baseDto);

        log.info(INIT_LOGGER_PREFIX + "Trigger Type : " + new Date());
        initSetupService.loadTriggerType(baseDto);

        log.info(INIT_LOGGER_PREFIX + "Trgger : " + new Date());
        initSetupService.loadTrigger(baseDto);

        log.info(INIT_LOGGER_PREFIX + "Port group: " + new Date());
        initSetupService.loadPortGroup(baseDto);

        log.info(INIT_LOGGER_PREFIX + "Port Data : " + new Date());
        initSetupService.loadPortMaster(baseDto);

        log.info(INIT_LOGGER_PREFIX + "Service Data : " + new Date());
        initSetupService.loadService(baseDto);

        log.info(INIT_LOGGER_PREFIX + "Document type Data : " + new Date());
        initSetupService.loadDocumentType(baseDto);

        log.info(INIT_LOGGER_PREFIX + "Email Template Data : " + new Date());
        initSetupService.loadEmailTemplate(baseDto);

        log.info(INIT_LOGGER_PREFIX + "Tos Master Data : " + new Date());
        initSetupService.loadTos(baseDto);

        log.info(INIT_LOGGER_PREFIX + "Unit Master Data : " + new Date());
        initSetupService.loadUnit(baseDto);

        log.info(INIT_LOGGER_PREFIX + "Commodity Master Data : " + new Date());
        initSetupService.loadCommodityMaster(baseDto);

        log.info(INIT_LOGGER_PREFIX + "Carrier Master Data : " + new Date());
        initSetupService.loadCarrierMaster(baseDto);

        log.info(INIT_LOGGER_PREFIX + "Event Master Data : " + new Date());
        initSetupService.loadEventMaster(baseDto);

        log.info(INIT_LOGGER_PREFIX + "Reference Type Master Data : " + new Date());
        initSetupService.loadReferenceTypeMaster(baseDto);

        log.info(INIT_LOGGER_PREFIX + "Charge Master Data : " + new Date());
        initSetupService.loadChargeMaster(baseDto);

        log.info(INIT_LOGGER_PREFIX + "Pack Master Data : " + new Date());
        initSetupService.loadPackMaster(baseDto);

        log.info(INIT_LOGGER_PREFIX + "dynamic country Data : " + new Date());
        initSetupService.loadDynamicData(baseDto);
    }

    private void createCompanyAndLocationProcess(LocationSetupDTO locationSetup) {

        TenantContext.setCurrentTenant(locationSetup.getSaasId());

        saasServiceImpl.createCompanyAndLocationProcess(locationSetup);

        TenantContext.setCurrentTenant("COM");

        emailRequestService.sendSaasIdProcessCompletedMail(locationSetup);

    }

}
