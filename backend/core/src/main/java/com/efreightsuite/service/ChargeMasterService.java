package com.efreightsuite.service;

import java.util.ArrayList;
import java.util.List;

import com.efreightsuite.common.component.CacheRepository;
import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.ChargeSearchDto;
import com.efreightsuite.dto.FileUploadDto;
import com.efreightsuite.dto.SearchRequest;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.ExceptionHelper;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.exception.UniqueKey;
import com.efreightsuite.model.ChargeEdiMapping;
import com.efreightsuite.model.ChargeMaster;
import com.efreightsuite.repository.ChargeMasterRepository;
import com.efreightsuite.search.ChargeSearchImpl;
import com.efreightsuite.search.CommonSearchImpl;
import com.efreightsuite.util.AppUtil;
import com.efreightsuite.util.SaaSUtil;
import com.efreightsuite.util.ValidateUtil;
import com.efreightsuite.validation.ChargeMasterValidator;
import com.efreightsuite.validation.UnitMasterValidator;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.exception.ExceptionUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.orm.ObjectOptimisticLockingFailureException;
import org.springframework.orm.jpa.JpaObjectRetrievalFailureException;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class ChargeMasterService {

    @Autowired
    private
    AppUtil appUtil;

    @Autowired
    private
    ChargeMasterRepository chargeMasterRepository;

    @Autowired
    private
    ChargeSearchImpl chargeSearchImpl;

    @Autowired
    UnitMasterValidator unitMasterValidator;

    @Autowired
    private
    ChargeMasterValidator chargeMasterValidator;

    @Autowired
    private
    CacheRepository cacheRepository;

    @Autowired
    private
    CommonSearchImpl commonSearchImpl;


    @Autowired
    private
    MetaConfigurationService metaConfigurationService;

    public BaseDto search(ChargeSearchDto searchDto) {

        log.info("Search method is called...." + searchDto);

        BaseDto baseDto = new BaseDto();

        try {

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(chargeSearchImpl.search(searchDto));

            log.info("Successfully Searching ..");

        } catch (Exception exception) {

            log.error("Exception in Search method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);

    }

    public BaseDto search(SearchRequest searchRequest) {

        log.info("Search method is called...." + searchRequest);

        BaseDto baseDto = new BaseDto();

        try {

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(chargeSearchImpl.search(searchRequest));

            log.info("Successfully Searching ..");

        } catch (Exception exception) {

            log.error("Exception in Search method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);

    }


    public BaseDto get(Long id) {

        log.info("get method is called....ID : [" + id + "]");

        BaseDto baseDto = new BaseDto();

        try {

            ChargeMaster chargeMaster = chargeMasterRepository.findById(id);

            ValidateUtil.notNull(chargeMaster, ErrorCode.CHARGE_ID_NOT_FOUND);


            log.info("Successfully getting Charge by id...[" + chargeMaster.getId() + "]");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(chargeMaster);

        } catch (RestException exception) {

            log.error("Exception in get method ", exception);

            baseDto.setResponseCode(exception.getMessage());
        } catch (Exception e) {

            log.error("Exception in get method", e);

            baseDto.setResponseCode(ErrorCode.FAILED);

        }

        return appUtil.setDesc(baseDto);
    }


    public BaseDto getByChargeCode(String chargeCode) {

        log.info("getByCode method is called....ChargeCode : [" + chargeCode + "]");

        BaseDto baseDto = new BaseDto();

        try {
            ChargeMaster chargeMaster = chargeMasterRepository.findByChargeCode(chargeCode);

            ValidateUtil.notNull(chargeMaster, ErrorCode.CHARGE_CODE_NOTFOUND);

            if (chargeMaster.getEdiList() != null && chargeMaster.getEdiList().size() != 0) {
                for (ChargeEdiMapping cem : chargeMaster.getEdiList()) {
                    cem.setChargeMaster(null);
                }
            }

            log.info("Successfully getting Charge by code...");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(chargeMaster);

        } catch (RestException re) {

            log.error("Exception in get method ", re);

            baseDto.setResponseCode(ErrorCode.CHARGE_CODE_NOTFOUND);

        } catch (Exception e) {

            log.error("Exception in get method", e);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);
    }


    public BaseDto create(ChargeMaster chargeMaster) {

        log.info("create method is called.....");

        BaseDto baseDto = new BaseDto();

        try {

            chargeMasterValidator.validate(chargeMaster);


            if (chargeMaster.getEdiList() != null && chargeMaster.getEdiList().size() != 0) {
                for (ChargeEdiMapping cem : chargeMaster.getEdiList()) {
                    cem.setChargeMaster(chargeMaster);
                    cem.setChargeCode(chargeMaster.getChargeCode());
                }
            } else {
                chargeMaster.setEdiList(null);
            }

            cacheRepository.save(SaaSUtil.getSaaSId(), ChargeMaster.class.getName(), chargeMaster, chargeMasterRepository);

            log.info("ChargeMaster Saved successfully  [" + chargeMaster.getId() + "]");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(ErrorCode.SUCCESS);

        } catch (RestException exception) {

            log.error("Exception occured with error code ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (Exception e) {

            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(e);

            log.error("Exception Cause 1 ::: " + exceptionCause1);

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            if (exceptionCause1.contains(UniqueKey.UK_CHARGE_CHARGECODE)) {
                baseDto.setResponseCode(ErrorCode.CHARGE_CODE_ALREADY_EXIST);
            }

            if (exceptionCause1.contains(UniqueKey.UK_CHARGE_CHARGENAME)) {
                baseDto.setResponseCode(ErrorCode.CHARGE_NAME_ALREADY_EXIST);
            }

            if (exceptionCause1.contains(UniqueKey.UK_EDICHARGE_EDITYPE)) {
                baseDto.setResponseCode(ErrorCode.EDI_TYPE_ALREDY_EXIST);
            }

        }

        return appUtil.setDesc(baseDto);
    }


    public BaseDto update(ChargeMaster existingChargeMaster) {

        log.info("update method is called with ID : [" + existingChargeMaster.getId() + "]");

        BaseDto baseDto = new BaseDto();

        try {

            chargeMasterValidator.validate(existingChargeMaster);


            if (existingChargeMaster.getEdiList() != null && existingChargeMaster.getEdiList().size() != 0) {
                for (ChargeEdiMapping cem : existingChargeMaster.getEdiList()) {
                    cem.setChargeMaster(existingChargeMaster);
                    cem.setChargeCode(existingChargeMaster.getChargeCode());
                }
            } else {
                existingChargeMaster.setEdiList(new ArrayList<>());
                existingChargeMaster.getEdiList().clear();
            }

            existingChargeMaster = chargeMasterRepository.save(existingChargeMaster);

            log.info("ChargeMaster Updated successfully  :  [" + existingChargeMaster.getId() + "]");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(ErrorCode.SUCCESS);

        } catch (RestException exception) {

            log.error("Exception occured with error code ", exception);
            baseDto.setResponseCode(exception.getMessage());

        } catch (ObjectOptimisticLockingFailureException e) {

            log.error("Error in editing charge", e);
            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_LOCKED_RECORD);

        } catch (JpaObjectRetrievalFailureException e) {

            log.error("Error in editing charge", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_DELETED_RECORD);

        } catch (Exception e) {
            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(e);

            log.error("Exception Cause 1 ::: ", e);

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            if (exceptionCause1.contains(UniqueKey.UK_CHARGE_CHARGECODE)) {
                baseDto.setResponseCode(ErrorCode.CHARGE_CODE_ALREADY_EXIST);
            }

            if (exceptionCause1.contains(UniqueKey.UK_CHARGE_CHARGENAME)) {
                baseDto.setResponseCode(ErrorCode.CHARGE_NAME_ALREADY_EXIST);
            }

            if (exceptionCause1.contains(UniqueKey.UK_EDICHARGE_EDITYPE)) {
                baseDto.setResponseCode(ErrorCode.EDI_TYPE_ALREDY_EXIST);
            }

        }

        return appUtil.setDesc(baseDto);
    }


    public BaseDto delete(Long id) {

        log.info("Delete method is Invoked...[" + id + "]");

        BaseDto baseDto = new BaseDto();

        try {

            chargeMasterRepository.delete(id);

            log.info("ChargeMaster Deleted Successfully....[" + id + "]");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

        } catch (DataIntegrityViolationException exception) {

            log.error("Exception occured : ", exception);

            ExceptionHelper helper = new ExceptionHelper(exception);

            log.error("Foreign key reference : " + helper.getFkReference());

            baseDto.setResponseCode(ErrorCode.ERROR_FK_CONSTRAINT);

        } catch (Exception exception) {

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            log.error("Exception in Delete method : ", exception);

            String rootCause = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Root Cause of Exception : " + rootCause);

            if (rootCause.contains("FK_")) {
                baseDto.setResponseCode(ErrorCode.CANNOT_DELETE_FK_RECORD);
            }
        }

        return appUtil.setDesc(baseDto);
    }


    public BaseDto searchChargeNotInList(SearchRequest searchRequest) {

        log.info("searchChargeNotInList method is called....");

        BaseDto baseDto = new BaseDto();

        try {

            baseDto.setResponseObject(commonSearchImpl.searchChargeNotInList(searchRequest));

            baseDto.setResponseCode(ErrorCode.SUCCESS);

        } catch (Exception exception) {

            log.error("Exception in searchChargeNotInList method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);
    }

    public BaseDto chargeBulkUpload(FileUploadDto fileUploadDto) {

        BaseDto baseDto = new BaseDto();

        try {
            //metaConfigurationService.validateChargeBulkUpload(fileUploadDto);
            List<ChargeMaster> list = metaConfigurationService.getChargeList(baseDto, fileUploadDto);


            for (int i = 0; i < list.size(); i++) {

                for (int j = 0; j < list.size(); j++) {

                    if (i != j) {
                        if (list.get(i).getChargeCode().equalsIgnoreCase(list.get(j).getChargeCode())) {
                            throw new RestException(ErrorCode.CHARGE_CODE_ALREADY_EXIST);
                        }

                        if (list.get(i).getChargeName().equalsIgnoreCase(list.get(j).getChargeName())) {
                            throw new RestException(ErrorCode.CHARGE_NAME_ALREADY_EXIST);
                        }
                    }

                }

                //chargeMasterValidator.validate(list.get(i));

            }
            baseDto.setResponseCode(ErrorCode.SUCCESS);
            baseDto.setResponseObject(list);

        } catch (RestException exception) {

            log.error("Rest Exception occured with error code ", exception);
            baseDto.setResponseCode(ErrorCode.CSV_FILE_INVALID);

        } catch (Exception exception) {

            log.error("Exception occured ", exception);

            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Exception Cause 1 : " + exceptionCause1);

            // General error condition
            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);
        }
        return appUtil.setDesCommon(baseDto);

    }
}
