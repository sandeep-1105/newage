package com.efreightsuite.service.common;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.enumeration.DocumentType;
import com.efreightsuite.enumeration.FullGroupage;
import com.efreightsuite.enumeration.ImportExport;
import com.efreightsuite.enumeration.LovStatus;
import com.efreightsuite.enumeration.TransportMode;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.model.common.CommonCityMaster;
import com.efreightsuite.model.common.CommonCountryMaster;
import com.efreightsuite.model.common.CommonCurrencyMaster;
import com.efreightsuite.model.common.CommonDocumentTypeMaster;
import com.efreightsuite.model.common.CommonErrorMessage;
import com.efreightsuite.model.common.CommonRegionMaster;
import com.efreightsuite.model.common.CommonServiceMaster;
import com.efreightsuite.model.common.CommonStateMaster;
import com.efreightsuite.model.common.CommonTimeZoneMaster;
import com.efreightsuite.repository.common.CommonCityMasterRepository;
import com.efreightsuite.repository.common.CommonCountryMasterRepository;
import com.efreightsuite.repository.common.CommonCurrencyMasterRepository;
import com.efreightsuite.repository.common.CommonDocumentTypeMasterRepository;
import com.efreightsuite.repository.common.CommonErrorMessageRepository;
import com.efreightsuite.repository.common.CommonRegionMasterRepository;
import com.efreightsuite.repository.common.CommonServiceMasterRepository;
import com.efreightsuite.repository.common.CommonStateMasterRepository;
import com.efreightsuite.repository.common.CommonTimeZoneMasterRepository;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.orm.ObjectOptimisticLockingFailureException;
import org.springframework.orm.jpa.JpaObjectRetrievalFailureException;
import org.springframework.stereotype.Service;

@Log4j2
@Service
public class CommonDbFileUploaderService {

    @Value("${efs.resource.path}")
    private
    String appResourcePath;

    @Autowired
    private
    CommonErrorMessageRepository commonErrorMessageRepository;

    @Autowired
    private
    CommonTimeZoneMasterRepository commonTimeZoneMasterRepository;

    @Autowired
    private
    CommonServiceMasterRepository commonServiceMasterRepository;

    @Autowired
    private
    CommonCountryMasterRepository commonCountryMasterRepository;

    @Autowired
    private
    CommonCurrencyMasterRepository commonCurrencyMasterRepository;

    @Autowired
    private
    CommonStateMasterRepository commonStateMasterRepository;

    @Autowired
    private
    CommonCityMasterRepository commonCityMasterRepository;

    @Autowired
    private
    CommonRegionMasterRepository commonRegionMasterRepository;

    @Autowired
    private
    CommonDocumentTypeMasterRepository commonDocumentTypeMasterRepository;

    public void loadCommonData() {
        BaseDto baseDto = new BaseDto();

        loadCommonErrorMessages(baseDto);
        loadCommonTimeZones(baseDto);
        loadCommonServiceMaster(baseDto);
        loadCountry(baseDto);
        loadCommonCurrency(baseDto);
        loadState(baseDto);
        loadCityMaster(baseDto);
        loadDocumentType(baseDto);
        baseDto.setResponseCode(ErrorCode.SUCCESS);
        baseDto.setResponseObject(ErrorCode.SUCCESS);

    }

    private void loadCommonErrorMessages(BaseDto baseDto) {

        log.info("Upload Common Error Messages Data.");

        try (XSSFWorkbook workbook = new XSSFWorkbook(
                new FileInputStream(new File(appResourcePath + "/efs-master-data/common_error_message.xlsx")))) {
            XSSFSheet firstSheet = workbook.getSheetAt(0);

            List<CommonErrorMessage> entityList = new ArrayList<>();

            for (int i = 1; i <= firstSheet.getLastRowNum(); i++) {

                String code = firstSheet.getRow(i).getCell(0).getStringCellValue().trim().toUpperCase();
                String desc = firstSheet.getRow(i).getCell(1).getStringCellValue().trim();

                CommonErrorMessage entity = new CommonErrorMessage();
                entity.setErrorCode(code);
                entity.setErrorDesc(desc);
                entityList.add(entity);
                log.info(entity);

            }
            if (entityList.size() == 50) {
                commonErrorMessageRepository.save(entityList);
                commonErrorMessageRepository.flush();
                entityList = new ArrayList<>();
            }

            if (entityList.size() != 0) {
                commonErrorMessageRepository.save(entityList);
            }
            workbook.close();


        } catch (Exception exception) {
            log.error("Failed to save ..", exception);

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Exception Cause 1 ::: " + exceptionCause1);

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            if (exceptionCause1.contains("UK_ERRORMESSAGE_CODE")) {
                baseDto.setResponseCode(ErrorCode.LANGUAGE_CODE_EXIT);
            }
        }

    }

    private void loadCommonTimeZones(BaseDto baseDto) {

        log.info("Upload loadCommonTimeZones Data.");


        try (XSSFWorkbook workbook = new XSSFWorkbook(
                new FileInputStream(new File(appResourcePath + "/efs-master-data/common_time_zone_master.xlsx")))) {
            XSSFSheet firstSheet = workbook.getSheetAt(0);

            List<CommonTimeZoneMaster> entityList = new ArrayList<>();

            for (int i = 1; i <= firstSheet.getLastRowNum(); i++) {

                String actualTimeZone = firstSheet.getRow(i).getCell(0).getStringCellValue().trim();
                String displayTimeZone = firstSheet.getRow(i).getCell(1).getStringCellValue().trim();

                CommonTimeZoneMaster entity = new CommonTimeZoneMaster();
                entity.setActualTimeZone(actualTimeZone);
                entity.setDisplayTimeZone(displayTimeZone);
                entity.setStatus(LovStatus.Active);
                entityList.add(entity);
                log.info(entity);
            }
            if (entityList.size() == 50) {
                commonTimeZoneMasterRepository.save(entityList);
                commonTimeZoneMasterRepository.flush();
                entityList = new ArrayList<>();
            }

            if (entityList.size() != 0) {
                commonTimeZoneMasterRepository.save(entityList);
            }
            workbook.close();


        } catch (Exception exception) {
            log.error("Failed to save ..", exception);

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Exception Cause 1 ::: " + exceptionCause1);

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            if (exceptionCause1.contains("UK_COM_TIMEZONE_DISPLAY")) {
                baseDto.setResponseCode(ErrorCode.UR_COMMON_TIME_ZONE_DISPLAY_DUPLICATED);
            }

            if (exceptionCause1.contains("UK_COM_TIMEZONE_ACTUAL")) {
                baseDto.setResponseCode(ErrorCode.UR_COMMON_TIME_ZONE_ACTUAL_DUPLICATED);
            }
        }

    }

    private void loadCommonServiceMaster(BaseDto baseDto) {

        log.info("UploadCommonServiceMaster Data.");


        try (XSSFWorkbook workbook = new XSSFWorkbook(
                new FileInputStream(new File(appResourcePath + "/efs-master-data/service-master.xlsx")))) {
            XSSFSheet firstSheet = workbook.getSheetAt(1);

            List<CommonServiceMaster> entityList = new ArrayList<>();

            for (int i = 1; i <= firstSheet.getLastRowNum(); i++) {

                String code = firstSheet.getRow(i).getCell(0).getStringCellValue().trim().toUpperCase();
                String name = firstSheet.getRow(i).getCell(1).getStringCellValue().trim();
                String transportMode = firstSheet.getRow(i).getCell(2).getStringCellValue().trim();
                String fullGroupage = firstSheet.getRow(i).getCell(3).getStringCellValue().trim();
                String importExport = firstSheet.getRow(i).getCell(4).getStringCellValue().trim();

                CommonServiceMaster entity = new CommonServiceMaster();

                entity.setServiceCode(code);
                entity.setServiceName(name);
                entity.setStatus(LovStatus.Active);
                entity.setTransportMode(TransportMode.valueOf(transportMode));
                entity.setFullGroupage(FullGroupage.valueOf(fullGroupage));
                entity.setImportExport(ImportExport.valueOf(importExport));

                entityList.add(entity);

                log.info(entity);

            }
            if (entityList.size() == 50) {
                commonServiceMasterRepository.save(entityList);
                commonServiceMasterRepository.flush();
                entityList = new ArrayList<>();
            }

            if (entityList.size() != 0) {
                commonServiceMasterRepository.save(entityList);
            }
            workbook.close();


        } catch (Exception exception) {
            log.error("Failed to save ..", exception);

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Exception Cause 1 ::: " + exceptionCause1);

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            if (exceptionCause1.contains("UK_CM_SERVICE_SERVICECODE")) {
                baseDto.setResponseCode(ErrorCode.SERVICE_CODE_ALREADY_EXIST);
            }

            if (exceptionCause1.contains("UK_CM_SERVICE_SERVICENAME")) {
                baseDto.setResponseCode(ErrorCode.SERVICE_NAME_ALREADY_EXIST);
            }
        }

    }

    private void regionData(XSSFSheet firstSheet) {

        List<CommonRegionMaster> entityList = new ArrayList<>();

        for (int i = 1; i <= firstSheet.getLastRowNum(); i++) {

            firstSheet.getRow(i).getCell(0).setCellType(Cell.CELL_TYPE_STRING);
            String code = firstSheet.getRow(i).getCell(0).getStringCellValue().trim().toUpperCase();
            CommonRegionMaster tmpObj = commonRegionMasterRepository.findByRegionCode(code);

            if (tmpObj == null) {

                CommonRegionMaster entity = new CommonRegionMaster();
                entity.setStatus(LovStatus.Active);
                entity.setRegionCode(code);
                entity.setRegionName(firstSheet.getRow(i).getCell(1).getStringCellValue().trim());
                entityList.add(entity);

            }
            if (entityList.size() == 50) {
                commonRegionMasterRepository.save(entityList);
                commonRegionMasterRepository.flush();
                entityList = new ArrayList<>();
            }
        }

        if (entityList.size() != 0) {
            commonRegionMasterRepository.save(entityList);
        }

    }

    private void countryData(XSSFSheet firstSheet) {

        Map<String, CommonRegionMaster> regionMap = new HashMap<>();
        List<CommonCountryMaster> entityList = new ArrayList<>();

        for (int i = 1; i <= firstSheet.getLastRowNum(); i++) {

            firstSheet.getRow(i).getCell(3).setCellType(Cell.CELL_TYPE_STRING);
            String regionCode = firstSheet.getRow(i).getCell(3).getStringCellValue();

            if (regionCode != null && regionCode.trim().length() != 0
                    && regionMap.get(regionCode.trim().toUpperCase()) == null) {
                CommonRegionMaster reg = commonRegionMasterRepository.findByRegionCode(regionCode.trim().toUpperCase());
                if (reg != null) {
                    regionMap.put(regionCode.trim().toUpperCase(), reg);
                }
            }

            String countryCode = firstSheet.getRow(i).getCell(0).getStringCellValue().trim().toUpperCase();
            CommonCountryMaster tmpObj = commonCountryMasterRepository.findByCountryCode(countryCode);

            if (tmpObj == null) {

                CommonCountryMaster entity = new CommonCountryMaster();
                if (regionCode != null && regionCode.trim().length() != 0
                        && regionMap.get(regionCode.trim().toUpperCase()) != null) {
                    entity.setRegionMaster(regionMap.get(regionCode.trim().toUpperCase()));
                    entity.setRegionCode(entity.getRegionMaster().getRegionCode());
                }
                entity.setStatus(LovStatus.Active);
                entity.setCountryCode(countryCode);
                entity.setCountryName(firstSheet.getRow(i).getCell(1).getStringCellValue().trim());
                entity.setLocale(firstSheet.getRow(i).getCell(2).getStringCellValue().trim());

                File logoFile = new File(appResourcePath + "/efs-master-data/country-logo/"
                        + entity.getCountryCode().trim().toLowerCase() + ".png");
                try (FileInputStream logo = new FileInputStream(logoFile)) {

                    if (logo != null) {
                        byte[] bytesArray = new byte[(int) logoFile.length()];
                        int read = logo.read(bytesArray); // read file into bytes[]
                        logo.close();
                        entity.setImage(bytesArray);
                    }

                } catch (Exception e) {
                    log.error("Logo fetch " + e);
                }

                entityList.add(entity);

            }
            if (entityList.size() == 50) {
                commonCountryMasterRepository.save(entityList);
                commonCountryMasterRepository.flush();
                entityList = new ArrayList<>();
            }
        }

        if (entityList.size() != 0) {
            commonCountryMasterRepository.save(entityList);
        }
    }

    private void loadCountry(BaseDto baseDto) {


        try (XSSFWorkbook workbook = new XSSFWorkbook(
                new FileInputStream(new File(appResourcePath + "/efs-master-data/country-master.xlsx")))) {

            regionData(workbook.getSheetAt(0));

            countryData(workbook.getSheetAt(1));

            workbook.close();


        } catch (RestException exception) {

            log.error("->Exception in Create method : ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (ObjectOptimisticLockingFailureException e) {

            log.error("Exception in  -> saveOrUpdate method ", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_LOCKED_RECORD);

        } catch (JpaObjectRetrievalFailureException e) {

            log.error("Exception in  -> saveOrUpdate method ", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_DELETED_RECORD);

        } catch (Exception exception) {
            log.error("Failed to save ..", exception);

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Exception Cause 1 ::: " + exceptionCause1);

            // General error condition
            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            if (exceptionCause1.contains("UK_CMCOUNTRY_COUNTRYCODE")) {
                log.error("Duplicate Country Code");
                baseDto.setResponseCode(ErrorCode.COUNTRY_CODE_ALREADY_EXIST);
            }
            if (exceptionCause1.contains("UK_CMCOUNTRY_COUNTRYNAME")) {
                log.error("Duplicate Country name");
                baseDto.setResponseCode(ErrorCode.COUNTRY_NAME_ALREADY_EXIST);
            }

        }

    }

    private void loadCommonCurrency(BaseDto baseDto) {


        try (XSSFWorkbook workbook = new XSSFWorkbook(
                new FileInputStream(new File(appResourcePath + "/efs-master-data/currency-master.xlsx")))) {
            XSSFSheet firstSheet = workbook.getSheetAt(0);

            Map<String, CommonCountryMaster> map = new HashMap<>();

            for (int i = 1; i <= firstSheet.getLastRowNum(); i++) {

                String code = firstSheet.getRow(i).getCell(2).getStringCellValue().trim().toUpperCase();
                if (map.get(code) == null) {
                    CommonCountryMaster tmpObj = commonCountryMasterRepository.findByCountryCode(code);
                    map.put(code, tmpObj);
                }

                String currencyCode = firstSheet.getRow(i).getCell(0).getStringCellValue().trim().toUpperCase();
                CommonCurrencyMaster tmpObj = commonCurrencyMasterRepository.findByCurrencyCode(currencyCode);

                if (tmpObj == null) {

                    CommonCurrencyMaster entity = new CommonCurrencyMaster();
                    entity.setStatus(LovStatus.Active);
                    entity.setCurrencyCode(currencyCode);
                    entity.setCurrencyName(firstSheet.getRow(i).getCell(1).getStringCellValue().trim());
                    entity.setCountryMaster(map.get(code));
                    entity.setCountryCode(map.get(code).getCountryCode());

                    firstSheet.getRow(i).getCell(3).setCellType(Cell.CELL_TYPE_STRING);
                    entity.setDecimalPoint(firstSheet.getRow(i).getCell(3).getStringCellValue());

                    String prefix = firstSheet.getRow(i).getCell(5).getStringCellValue();
                    if (prefix != null && prefix.trim().length() != 0) {
                        entity.setPrefix(prefix.trim());
                    }

                    String sufix = firstSheet.getRow(i).getCell(6).getStringCellValue();
                    if (sufix != null && sufix.trim().length() != 0) {
                        entity.setSuffix(sufix.trim());
                    }

                    entity = commonCurrencyMasterRepository.save(entity);

					/*CommonCountryMaster tmpCountryObj = commonCountryMasterRepository.findByCountryCode(entity.getCountryMaster().getCountryCode());
                    tmpCountryObj.setCurrencyMaster(entity);
					tmpCountryObj.setCurrencyCode(entity.getCurrencyCode());
					countryMasterRepository.save(tmpCountryObj);*/


                    if (i % 50 == 0) {
                        commonCurrencyMasterRepository.flush();
                    }

                }

            }

            workbook.close();

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(ErrorCode.SUCCESS);

        } catch (RestException exception) {

            log.error("->Exception in Create method : ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (ObjectOptimisticLockingFailureException e) {

            log.error("Exception in  -> saveOrUpdate method ", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_LOCKED_RECORD);

        } catch (JpaObjectRetrievalFailureException e) {

            log.error("Exception in  -> saveOrUpdate method ", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_DELETED_RECORD);

        } catch (Exception exception) {
            log.error("Failed to save ..", exception);

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Exception Cause 1 ::: " + exceptionCause1);

            // General error condition
            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            if (exceptionCause1.contains("FK_")) {
                baseDto.setResponseCode(ErrorCode.SAVE_UPDATE_FK_RECORD);
            }

            if (exceptionCause1.contains("UK_CMCURRENCY_CURRENCYCODE")) {
                baseDto.setResponseCode(ErrorCode.CURRENCY_CODE_ALREADY_EXIST);
            }

            if (exceptionCause1.contains("UK_CMCURRENCY_CURRENCYNAME")) {
                baseDto.setResponseCode(ErrorCode.CURRENCY_NAME_ALREADY_EXIST);
            }

        }

    }

    private void loadState(BaseDto baseDto) {


        try (XSSFWorkbook workbook = new XSSFWorkbook(
                new FileInputStream(new File(appResourcePath + "/efs-master-data/state-master.xlsx")))) {
            XSSFSheet firstSheet = workbook.getSheetAt(0);

            Map<String, CommonCountryMaster> map = new HashMap<>();

            List<CommonStateMaster> entityList = new ArrayList<>();

            for (int i = 1; i <= firstSheet.getLastRowNum(); i++) {

                String countryCode = firstSheet.getRow(i).getCell(2).getStringCellValue().trim().toUpperCase();

                if (map.get(countryCode) == null) {
                    CommonCountryMaster tmpObj = commonCountryMasterRepository.findByCountryCode(countryCode);
                    map.put(countryCode, tmpObj);
                }

                firstSheet.getRow(i).getCell(0).setCellType(Cell.CELL_TYPE_STRING);
                String stateCode = firstSheet.getRow(i).getCell(0).getStringCellValue().trim().toUpperCase();
                CommonStateMaster stateMaster = commonStateMasterRepository.getState(stateCode, map.get(countryCode).getId());

                if (stateMaster == null) {

                    CommonStateMaster entity = new CommonStateMaster();
                    entity.setStatus(LovStatus.Active);
                    entity.setCountryCode(map.get(countryCode).getCountryCode());
                    entity.setCountryMaster(map.get(countryCode));
                    entity.setStateCode(stateCode);
                    entity.setStateName(firstSheet.getRow(i).getCell(1).getStringCellValue().trim());

                    entityList.add(entity);

                }

                if (entityList.size() == 50) {
                    commonStateMasterRepository.save(entityList);
                    commonStateMasterRepository.flush();
                    entityList = new ArrayList<>();
                }

            }

            if (entityList.size() != 0) {
                commonStateMasterRepository.save(entityList);
            }

            workbook.close();


        } catch (RestException exception) {

            log.error("->Exception in Create method : ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (ObjectOptimisticLockingFailureException e) {

            log.error("Exception in  -> saveOrUpdate method ", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_LOCKED_RECORD);

        } catch (JpaObjectRetrievalFailureException e) {

            log.error("Exception in  -> saveOrUpdate method ", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_DELETED_RECORD);

        } catch (Exception exception) {
            log.error("Failed to save ..", exception);

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Exception Cause 1 ::: " + exceptionCause1);

            // General error condition
            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            if (exceptionCause1.contains("FK_")) {
                baseDto.setResponseCode(ErrorCode.SAVE_UPDATE_FK_RECORD);
            }

            if (exceptionCause1.contains("UK_CMSTATEPROVINCE_STATECODE")) {
                baseDto.setResponseCode(ErrorCode.STATE_CODE_ALREADY_EXIST);
            }

            if (exceptionCause1.contains("UK_CMSTATEPROVINCE_STATENAME")) {
                baseDto.setResponseCode(ErrorCode.STATE_NAME_ALREADY_EXIST);
            }

        }

    }

    private void loadCityMaster(BaseDto baseDto) {


        try (XSSFWorkbook workbook = new XSSFWorkbook(
                new FileInputStream(new File(appResourcePath + "/efs-master-data/city-master.xlsx")))) {
            XSSFSheet firstSheet = workbook.getSheetAt(0);

            Map<String, CommonStateMaster> map = new HashMap<>();

            Map<String, CommonCountryMaster> mapCountry = new HashMap<>();
            List<CommonCityMaster> entityList = new ArrayList<>();

            for (int i = 1; i <= firstSheet.getLastRowNum(); i++) {

                String countryCode = firstSheet.getRow(i).getCell(3).getStringCellValue().trim().toUpperCase();
                String stateCode = firstSheet.getRow(i).getCell(2).getStringCellValue().trim().toUpperCase();
                if (stateCode == null || stateCode.length() == 0) {
                    stateCode = null;
                }
                String cityCode = firstSheet.getRow(i).getCell(0).getStringCellValue().trim().toUpperCase();

                if (map.get((stateCode + countryCode)) == null) {
                    CommonStateMaster tmpObj = commonStateMasterRepository.getState(stateCode, countryCode);
                    map.put((stateCode + countryCode), tmpObj);
                }

                if (mapCountry.get(countryCode) == null) {
                    CommonCountryMaster tmpObj = commonCountryMasterRepository.findByCountryCode(countryCode);
                    mapCountry.put(countryCode, tmpObj);
                }

                CommonCityMaster cityMaster = commonCityMasterRepository.findByCityCodeAndCountryCodeAndStateCode(cityCode, countryCode,
                        stateCode);

                if (cityMaster == null) {

                    CommonCityMaster entity = new CommonCityMaster();

                    if (map.get((stateCode + countryCode)) != null) {
                        CommonStateMaster st = map.get((stateCode + countryCode));
                        entity.setStateCode(st.getStateCode());
                        entity.setStateMaster(st);
                    }

                    if (mapCountry.get(countryCode) != null) {
                        CommonCountryMaster st = mapCountry.get(countryCode);
                        entity.setCountryCode(st.getCountryCode());
                        entity.setCountryMaster(st);
                    }

                    entity.setCityCode(cityCode);
                    entity.setCityName(firstSheet.getRow(i).getCell(1).getStringCellValue().trim());

                    entity.setStatus(LovStatus.Active);

                    entityList.add(entity);
                }

                if (entityList.size() == 50) {
                    commonCityMasterRepository.save(entityList);
                    commonCityMasterRepository.flush();
                    entityList = new ArrayList<>();
                }

            }

            if (entityList.size() != 0) {
                commonCityMasterRepository.save(entityList);
            }

            workbook.close();


        } catch (RestException exception) {

            log.error("->Exception in Create method : ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (ObjectOptimisticLockingFailureException e) {

            log.error("Exception in  -> saveOrUpdate method ", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_LOCKED_RECORD);

        } catch (JpaObjectRetrievalFailureException e) {

            log.error("Exception in  -> saveOrUpdate method ", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_DELETED_RECORD);

        } catch (Exception exception) {
            log.error("Failed to save ..", exception);

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Exception Cause 1 ::: " + exceptionCause1);

            // General error condition
            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            if (exceptionCause1.contains("FK_")) {
                baseDto.setResponseCode(ErrorCode.SAVE_UPDATE_FK_RECORD);
            }

            if (exceptionCause1.contains("UK_CMCITY_CITYCODE")) {
                baseDto.setResponseCode(ErrorCode.CITY_CODE_ALREADY_EXIST);
            }

            if (exceptionCause1.contains("UK_CMCITY_CITYNAME")) {
                baseDto.setResponseCode(ErrorCode.CITY_NAME_ALREADY_EXIST);
            }

        }

    }

    private void loadDocumentType(BaseDto baseDto) {


        try (XSSFWorkbook workbook = new XSSFWorkbook(new FileInputStream(new File(appResourcePath + "/efs-master-data/document-type-master.xlsx")))) {
            XSSFSheet firstSheet = workbook.getSheetAt(0);

            List<CommonDocumentTypeMaster> entityList = new ArrayList<>();

            for (int i = 1; i <= firstSheet.getLastRowNum(); i++) {

                DocumentType code = DocumentType.valueOf(firstSheet.getRow(i).getCell(0).getStringCellValue().trim().toUpperCase());

                CommonDocumentTypeMaster tmpObj = commonDocumentTypeMasterRepository.findByDocumentTypeCode(code);

                if (tmpObj == null) {

                    CommonDocumentTypeMaster entity = new CommonDocumentTypeMaster();
                    entity.setStatus(LovStatus.Active);
                    entity.setDocumentTypeCode(code);
                    entity.setDocumentTypeName(firstSheet.getRow(i).getCell(1).getStringCellValue().trim());
                    entityList.add(entity);

                }

                if (entityList.size() == 50) {
                    commonDocumentTypeMasterRepository.save(entityList);
                    commonDocumentTypeMasterRepository.flush();
                    entityList = new ArrayList<>();
                }

            }

            if (entityList.size() != 0) {
                commonDocumentTypeMasterRepository.save(entityList);
            }

            workbook.close();


        } catch (RestException exception) {

            log.error("->Exception in Create method : ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (ObjectOptimisticLockingFailureException e) {

            log.error("Exception in  -> saveOrUpdate method ", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_LOCKED_RECORD);

        } catch (JpaObjectRetrievalFailureException e) {

            log.error("Exception in  -> saveOrUpdate method ", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_DELETED_RECORD);

        } catch (Exception exception) {
            log.error("Failed to save ..", exception);

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Exception Cause 1 ::: " + exceptionCause1);

            // General error condition
            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            if (exceptionCause1.contains("FK_")) {
                baseDto.setResponseCode(ErrorCode.SAVE_UPDATE_FK_RECORD);
            }

            if (exceptionCause1.contains("UK_COM_DOC_DOCTYPECODE")) {
                baseDto.setResponseCode(ErrorCode.DOCUMENT_TYPE_CODE_ALREADY_EXIST);

            }

            if (exceptionCause1.contains("UK_COM_DOC_DOCTYPENAME")) {
                baseDto.setResponseCode(ErrorCode.DOCUMENT_TYPE_NAME_ALREADY_EXIST);
            }

        }

    }

}
