package com.efreightsuite.service;

import java.util.List;

import com.efreightsuite.common.component.CacheRepository;
import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.SearchRequest;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.exception.UniqueKey;
import com.efreightsuite.model.ProjectMaster;
import com.efreightsuite.repository.ProjectMasterRepository;
import com.efreightsuite.search.ProjectSearchImpl;
import com.efreightsuite.util.AppUtil;
import com.efreightsuite.util.SaaSUtil;
import com.efreightsuite.util.ValidateUtil;
import com.efreightsuite.validation.ProjectMasterValidator;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.exception.ExceptionUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.ObjectOptimisticLockingFailureException;
import org.springframework.orm.jpa.JpaObjectRetrievalFailureException;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class ProjectMasterService {

    @Autowired
    private
    AppUtil appUtil;

    @Autowired
    private
    ProjectMasterRepository projectMasterRepository;

    @Autowired
    private
    ProjectMasterValidator projectMasterValidator;


    @Autowired
    private
    ProjectSearchImpl projectSearchImpl;


    @Autowired
    private
    CacheRepository cacheRepository;

    public BaseDto create(ProjectMaster projectMaster) {

        log.info("Create method is invoked.");

        BaseDto baseDto = new BaseDto();

        try {

            projectMasterValidator.validate(projectMaster);

            projectMaster = cacheRepository.save(SaaSUtil.getSaaSId(), ProjectMaster.class.getName(), projectMaster, projectMasterRepository);

            log.info("Project Master Saved Successfully.....[" + projectMaster.getId() + "]");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(projectMaster);

        } catch (RestException exception) {

            log.error("Exception occured ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (Exception exception) {

            log.error("Exception occured : ", exception);

            String exceptionCause = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Exception Cause  : " + exceptionCause);

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            if (exceptionCause.contains(UniqueKey.UK_PROJECT_PROJECT_CODE)) {
                baseDto.setResponseCode(ErrorCode.PROJECT_CODE_ALREADY_EXIST);
            }

            if (exceptionCause.contains(UniqueKey.UK_PROJECT_PROJECT_NAME)) {
                baseDto.setResponseCode(ErrorCode.PROJECT_NAME_ALREADY_EXIST);
            }

        }

        return appUtil.setDesc(baseDto);
    }


    public BaseDto update(ProjectMaster existingProjectMaster) {

        log.info("Update method is invoked... [" + existingProjectMaster.getId() + "]");

        BaseDto baseDto = new BaseDto();

        try {

            projectMasterValidator.validate(existingProjectMaster);

            projectMasterRepository.getOne(existingProjectMaster.getId());

            existingProjectMaster = cacheRepository.save(SaaSUtil.getSaaSId(), ProjectMaster.class.getName(), existingProjectMaster, projectMasterRepository);

            log.info("ProjectMaster updated successfully....[" + existingProjectMaster.getId() + "]");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(existingProjectMaster);

        } catch (RestException exception) {

            log.error("Exception occured with error code ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (ObjectOptimisticLockingFailureException e) {

            log.error("Error in editing", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_LOCKED_RECORD);

        } catch (JpaObjectRetrievalFailureException e) {

            log.error("Error in editing", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_DELETED_RECORD);

        } catch (Exception exception) {

            log.error("Exception occured : ", exception);

            String exceptionCause = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Exception Cause : " + exceptionCause);

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            if (exceptionCause.contains(UniqueKey.UK_PROJECT_PROJECT_CODE)) {
                baseDto.setResponseCode(ErrorCode.PROJECT_CODE_ALREADY_EXIST);
            }

            if (exceptionCause.contains(UniqueKey.UK_PROJECT_PROJECT_NAME)) {
                baseDto.setResponseCode(ErrorCode.PROJECT_NAME_ALREADY_EXIST);
            }
        }

        return appUtil.setDesc(baseDto);
    }


    public BaseDto delete(Long id) {

        log.info("Delete method is Invoked...[" + id + "]");

        BaseDto baseDto = new BaseDto();

        try {

            cacheRepository.delete(SaaSUtil.getSaaSId(), ProjectMaster.class.getName(), id, projectMasterRepository);

            log.info("ProjectMaster Deleted Successfully....[" + id + "]");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

        } catch (Exception exception) {

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            log.error("Exception in Delete method : ", exception);

            String rootCause = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Root Cause of Exception : " + rootCause);

            if (rootCause.contains("FK_")) {
                baseDto.setResponseCode(ErrorCode.CANNOT_DELETE_FK_RECORD);
            }
        }

        return appUtil.setDesc(baseDto);
    }


    public BaseDto getAll() {

        log.info("getAll method is Invoked....");

        BaseDto baseDto = new BaseDto();

        try {

            List<ProjectMaster> projectMasterList = projectMasterRepository.findAll();

            ValidateUtil.notEmpty(projectMasterList, ErrorCode.PROJECT_LIST_EMPTY);

            log.info("Successfully getting list of project...");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(projectMasterList);

        } catch (RestException re) {

            log.error("Exception in getAll method ", re);

            baseDto.setResponseCode(re.getMessage());

        } catch (Exception e) {

            log.error("Exception in getAll method ", e);

            baseDto.setResponseCode(ErrorCode.FAILED);

        }
        return appUtil.setDesc(baseDto);
    }


    public BaseDto search(SearchRequest searchRequest) {

        log.info("Search method is called....");

        BaseDto baseDto = new BaseDto();

        try {

            baseDto.setResponseObject(projectSearchImpl.search(searchRequest));

            baseDto.setResponseCode(ErrorCode.SUCCESS);

        } catch (Exception exception) {

            log.error("Exception in Search method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);
    }


}
