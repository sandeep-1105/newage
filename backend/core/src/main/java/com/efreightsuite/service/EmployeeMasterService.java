package com.efreightsuite.service;

import java.util.ArrayList;

import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.DatatypeConverter;

import com.efreightsuite.common.component.CacheRepository;
import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.EmployeeSearchRequestDto;
import com.efreightsuite.dto.SearchRequest;
import com.efreightsuite.enumeration.SequenceType;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.ExceptionHelper;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.exception.UniqueKey;
import com.efreightsuite.model.EmployeeAttachment;
import com.efreightsuite.model.EmployeeMaster;
import com.efreightsuite.repository.EmployeeAttachmentRepository;
import com.efreightsuite.repository.EmployeeMasterRepository;
import com.efreightsuite.search.EmployeeSearchImpl;
import com.efreightsuite.util.AppUtil;
import com.efreightsuite.validation.EmployeeMasterValidator;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.exception.ExceptionUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class EmployeeMasterService {

    @Autowired
    private
    AppUtil appUtil;

    @Autowired
    private
    EmployeeMasterRepository employeeMasterRepository;

    @Autowired
    CacheRepository cacheRepository;

    @Autowired
    private
    EmployeeMasterValidator employeeMasterValidator;

    @Autowired
    private
    EmployeeSearchImpl employeeSearchImpl;

    @Autowired
    private
    EmployeeAttachmentRepository employeeAttachmentRepository;

    @Autowired
    private
    SequenceGeneratorService sequenceGeneratorService;


    public BaseDto search(EmployeeSearchRequestDto dto) {

        log.info("List Page Search method is called....");

        BaseDto baseDto = new BaseDto();

        try {

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(employeeSearchImpl.search(dto));

            log.info("Search Method Completed Successfully.................");

            return appUtil.setDesc(baseDto);

        } catch (Exception e) {

            log.error("Exception in Search method ", e);

            baseDto.setResponseCode(ErrorCode.FAILED);

            return appUtil.setDesc(baseDto);
        }

    }

    public BaseDto search(SearchRequest searchRequest, boolean isSalesman) {
        log.info("Search method is called....");

        BaseDto baseDto = new BaseDto();

        try {

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(employeeSearchImpl.search(searchRequest, isSalesman));

            log.info("Search Method Completed Successfully.................");

            return appUtil.setDesc(baseDto);

        } catch (Exception e) {

            log.error("Exception in Search method ", e);

            baseDto.setResponseCode(ErrorCode.FAILED);

            return appUtil.setDesc(baseDto);
        }

    }

    public BaseDto get(Long id) {

        log.info("get method is called [" + id + "]");

        BaseDto baseDto = new BaseDto();

        try {

            EmployeeMaster employeeMaster = employeeMasterRepository.getOne(id);

            if (employeeMaster.getImage() != null) {
                employeeMaster.setEncodedImage(DatatypeConverter.printBase64Binary(employeeMaster.getImage()));
            }

            employeeMaster.setImage(null);

            if (employeeMaster.getAttachmentList() != null && !employeeMaster.getAttachmentList().isEmpty()) {
                for (EmployeeAttachment employeeAttachment : employeeMaster.getAttachmentList()) {
                    employeeAttachment.setFile(null);
                }
            }


            log.info("Successfully getting Employee by id.....[" + id + "]");

            baseDto.setResponseObject(employeeMaster);

            baseDto.setResponseCode(ErrorCode.SUCCESS);

        } catch (Exception exception) {

            log.error("Exception in getting employee by id [" + id + "]", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);
    }

    public BaseDto create(EmployeeMaster employeeMaster) {

        log.info("Create method is Invoked.....");

        BaseDto baseDto = new BaseDto();

        try {

            employeeMasterValidator.validate(employeeMaster);

            employeeMaster.setEmployeeName(employeeMaster.getAliasName());

            if (employeeMaster.getId() == null) {
                if (employeeMaster.getAttachmentList() != null && !employeeMaster.getAttachmentList().isEmpty()) {
                    for (EmployeeAttachment employeeAttachment : employeeMaster.getAttachmentList()) {
                        employeeAttachment.setEmployeeMaster(employeeMaster);
                    }
                }
            } else {

                if (employeeMaster.getAttachmentList() != null && !employeeMaster.getAttachmentList().isEmpty()) {
                    for (EmployeeAttachment employeeAttachment : employeeMaster.getAttachmentList()) {
                        employeeAttachment.setEmployeeMaster(employeeMaster);
                        if (employeeAttachment.getId() != null) {
                            if (employeeAttachment.getFile() == null) {
                                EmployeeAttachment attachment = employeeAttachmentRepository.getOne(employeeAttachment.getId());
                                employeeAttachment.setFile(attachment.getFile());
                            }
                        }
                    }
                } else {
                    employeeMaster.setAttachmentList(new ArrayList<>());
                }
            }
            if (employeeMaster.getEmployeeCode() == null) {
                employeeMaster.setEmployeeCode(sequenceGeneratorService.getSequence(SequenceType.EMPLOYEE));
            }

            employeeMaster = employeeMasterRepository.save(employeeMaster);

            log.info("Employee Successfully Saved....[" + employeeMaster.getId() + "]");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(employeeMaster);

        } catch (RestException exception) {

            log.error("Rest Exception occured with error code ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (Exception exception) {

            log.error("Exception occured ", exception);

            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Exception Cause 1 : " + exceptionCause1);

            // General error condition
            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            // Duplicate State Code
            if (exceptionCause1.contains(UniqueKey.UK_EMPLOYEE_EMPLOYEECODE)) {
                baseDto.setResponseCode(ErrorCode.EMPLOYEE_CODE_ALREADY_EXIST);
            }
        }

        return appUtil.setDesc(baseDto);
    }

    public BaseDto searchByCompany(SearchRequest searchRequest, Long companyId) {

        log.info("searchByCompany method is called....");

        BaseDto baseDto = new BaseDto();

        try {

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(employeeSearchImpl.searchByCompany(searchRequest, companyId));

        } catch (RestException exception) {

            log.error("Exception in getAll method ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (Exception exception) {

            log.error("Exception in getAll method ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);

        }

        return appUtil.setDesc(baseDto);
    }

    // method to get emplyoees based on the user login country
    public BaseDto search(SearchRequest searchRequest, Long countryId, boolean isSalesman) {

        log.info("Search method is called..");

        BaseDto baseDto = new BaseDto();

        try {

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(employeeSearchImpl.search(searchRequest, countryId, isSalesman));

            log.info("Search method is executed successfully..");

        } catch (Exception exception) {

            log.error("Exception in Search method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);

    }

    public BaseDto delete(Long id) {

        log.info("Delete method is Called...[" + id + "]");
        BaseDto baseDto = new BaseDto();

        try {

            employeeMasterRepository.delete(id);

            log.info("Employee Master Deleted Successfully....[" + id + "]");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

        } catch (DataIntegrityViolationException exception) {

            log.error("Exception occured : ", exception);

            ExceptionHelper helper = new ExceptionHelper(exception);

            log.error("Foreign key reference : " + helper.getFkReference());

            baseDto.setResponseCode(ErrorCode.ERROR_FK_CONSTRAINT);

        } catch (Exception exception) {

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            log.error("Exception in Delete method : ", exception);

            String rootCause = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Root Cause of Exception : " + rootCause);

            if (rootCause.contains("FK_")) {
                baseDto.setResponseCode(ErrorCode.CANNOT_DELETE_FK_RECORD);
            }
        }

        return appUtil.setDesc(baseDto);
    }


    public void downloadAttachment(Long attachmentId, HttpServletResponse response) {

        EmployeeAttachment attachment = employeeAttachmentRepository.getOne(attachmentId);


        if (attachment != null && attachment.getFile() != null && attachment.getFileName() != null) {

            try {

                response.setContentType(attachment.getFileContentType());

                response.setHeader("Content-Disposition", "attachment; filename=" + attachment.getFileName());

                response.setContentLength(attachment.getFile().length);

                response.getOutputStream().write(attachment.getFile());

                response.getOutputStream().flush();

                response.getOutputStream().close();

            } catch (Exception e) {
                log.error("Error occur during download attachment file : " + attachmentId + ", ", e);
            }


        } else {
            log.info("Attachment document is not available : " + attachmentId);
        }

    }

}
