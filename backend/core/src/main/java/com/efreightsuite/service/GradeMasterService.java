package com.efreightsuite.service;


import com.efreightsuite.common.component.CacheRepository;
import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.GradeMasterSearchDto;
import com.efreightsuite.dto.SearchRequest;
import com.efreightsuite.enumeration.Priority;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.ExceptionHelper;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.exception.UniqueKey;
import com.efreightsuite.model.DefaultMasterData;
import com.efreightsuite.model.GradeMaster;
import com.efreightsuite.repository.GradeMasterRepository;
import com.efreightsuite.search.GradeSearchImpl;
import com.efreightsuite.util.AppUtil;
import com.efreightsuite.util.SaaSUtil;
import com.efreightsuite.validation.GradeMasterValidator;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.exception.ExceptionUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.orm.ObjectOptimisticLockingFailureException;
import org.springframework.orm.jpa.JpaObjectRetrievalFailureException;
import org.springframework.stereotype.Service;


@Service
@Log4j2
public class GradeMasterService {

    @Autowired
    private
    AppUtil appUtil;

    @Autowired
    private
    GradeMasterRepository gradeMasterRepository;

    @Autowired
    private
    CacheRepository cacheRepository;

    @Autowired
    private
    GradeMasterValidator gradeMasterValidator;

    @Autowired
    private
    GradeSearchImpl gradeSearchImpl;

    public BaseDto search(SearchRequest searchRequest) {

        log.info("getAll method is called....");

        BaseDto baseDto = new BaseDto();

        try {

            baseDto.setResponseCode(ErrorCode.SUCCESS);
            baseDto.setResponseObject(gradeSearchImpl.search(searchRequest));

        } catch (RestException re) {
            log.error("Exception in getAll method ", re);
            baseDto.setResponseCode(re.getMessage());

        } catch (Exception e) {
            log.error("Exception in getAll method ", e);
            baseDto.setResponseCode(ErrorCode.FAILED);

        }
        return appUtil.setDesc(baseDto);
    }


    public BaseDto search(GradeMasterSearchDto searchDto) {

        log.info("getAll method is called....");

        BaseDto baseDto = new BaseDto();

        try {

            log.info("Successfully getting list of grade...");
            baseDto.setResponseCode(ErrorCode.SUCCESS);
            baseDto.setResponseObject(gradeSearchImpl.search(searchDto));

        } catch (RestException re) {
            log.error("Exception in getAll method ", re);
            baseDto.setResponseCode(re.getMessage());

        } catch (Exception e) {
            log.error("Exception in getAll method ", e);
            baseDto.setResponseCode(ErrorCode.FAILED);

        }
        return appUtil.setDesc(baseDto);
    }


    public BaseDto get(Long id) {

        BaseDto baseDto = new BaseDto();

        try {


            GradeMaster gradeMaster = cacheRepository.get(SaaSUtil.getSaaSId(), GradeMaster.class.getName(), id, gradeMasterRepository);


            if (gradeMaster.getPriorityValue() == 1) {
                gradeMaster.setPriority(Priority.ONE);
            } else if (gradeMaster.getPriorityValue() == 2) {
                gradeMaster.setPriority(Priority.TWO);
            } else if (gradeMaster.getPriorityValue() == 3) {
                gradeMaster.setPriority(Priority.THREE);
            } else if (gradeMaster.getPriorityValue() == 4) {
                gradeMaster.setPriority(Priority.FOUR);
            } else if (gradeMaster.getPriorityValue() == 5) {
                gradeMaster.setPriority(Priority.FIVE);
            }


            baseDto.setResponseCode(ErrorCode.SUCCESS);
            baseDto.setResponseObject(gradeMaster);

        } catch (RestException exception) {

            log.error("Exception in getById method : ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (Exception exception) {

            log.error("Exception in getById method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);
    }


    public BaseDto saveOrUpdate(GradeMaster gradeMaster) {

        BaseDto baseDto = new BaseDto();

        try {

            log.info("saveOrUpdate method is Invoked....[ " + gradeMaster.getId() + "]");

            gradeMasterValidator.validate(gradeMaster);


            if (gradeMaster.getPriority().equals(Priority.ONE)) {
                gradeMaster.setPriorityValue(1);
            } else if (gradeMaster.getPriority().equals(Priority.TWO)) {
                gradeMaster.setPriorityValue(2);
            } else if (gradeMaster.getPriority().equals(Priority.THREE)) {
                gradeMaster.setPriorityValue(3);
            } else if (gradeMaster.getPriority().equals(Priority.FOUR)) {
                gradeMaster.setPriorityValue(4);
            } else {
                gradeMaster.setPriorityValue(5);
            }


            gradeMasterRepository.save(gradeMaster);

            if (gradeMaster.getId() != null) {

                cacheRepository.remove(SaaSUtil.getSaaSId(), DefaultMasterData.class.getName(), gradeMaster.getId());
            }

            log.info("ObjectGroupMaster Successfully Updated...[" + gradeMaster.getId() + "]");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(gradeMaster);

        } catch (RestException exception) {

            log.error("Exception occured with error code ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (ObjectOptimisticLockingFailureException e) {

            log.error("Error in editing", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_LOCKED_RECORD);

        } catch (JpaObjectRetrievalFailureException e) {

            log.error("Error in editing", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_DELETED_RECORD);

        } catch (Exception exception) {

            log.error("Exception occured : ", exception);

            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Exception Cause 1 ::: ", exceptionCause1);

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            if (exceptionCause1.contains(UniqueKey.UK_GRADE_GRADENAME)) {
                baseDto.setResponseCode(ErrorCode.GRADE_ALREADY_EXIST);
            }
            if (exceptionCause1.contains(UniqueKey.UK_GRADE_GRADEPRIORITY)) {
                baseDto.setResponseCode(ErrorCode.GRADE_PRIORITY_ALREADY_EXIST);
            }

        }

        return appUtil.setDesc(baseDto);
    }


    public BaseDto delete(Long id) {

        log.info("Delete method is Invoked...[" + id + "]");

        BaseDto baseDto = new BaseDto();

        try {

            cacheRepository.delete(SaaSUtil.getSaaSId(), GradeMaster.class.getName(), id, gradeMasterRepository);

            log.info("GradeMaster Deleted Successfully....[" + id + "]");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

        } catch (DataIntegrityViolationException exception) {

            log.error("Exception occured : ", exception);

            ExceptionHelper helper = new ExceptionHelper(exception);

            log.error("Foreign key reference : " + helper.getFkReference());

            baseDto.setResponseCode(ErrorCode.ERROR_FK_CONSTRAINT);

        } catch (Exception exception) {

            log.error("Exception in Delete method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);
    }
}
