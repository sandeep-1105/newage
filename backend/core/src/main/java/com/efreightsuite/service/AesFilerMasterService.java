package com.efreightsuite.service;

import com.efreightsuite.common.component.CacheRepository;
import com.efreightsuite.dto.AesFilerSearchDto;
import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.ExceptionHelper;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.exception.UniqueKey;
import com.efreightsuite.model.AesFilerMaster;
import com.efreightsuite.repository.AesFilerMasterRepository;
import com.efreightsuite.search.AesFilerSearchService;
import com.efreightsuite.util.AppUtil;
import com.efreightsuite.util.ValidateUtil;
import com.efreightsuite.validation.SearchDtoValidator;
import com.efreightsuite.validators.AesFilerMasterValidator;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.exception.ExceptionUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.orm.ObjectOptimisticLockingFailureException;
import org.springframework.orm.jpa.JpaObjectRetrievalFailureException;
import org.springframework.stereotype.Service;

/**
 * @author achyutananda Date - 07.03.2016 Updated - 11.03.2016
 */

@Service
@Log4j2
public class AesFilerMasterService {

    @Autowired
    private
    AppUtil appUtil;

    /**
     * AesFilerMasterRepository Object
     */

    @Autowired
    private
    AesFilerMasterRepository aesFilerMasterRepository;

    @Autowired
    SearchDtoValidator searchDtoValidator;

    @Autowired
    private
    AesFilerMasterValidator aesFilerMasterValidator;

    @Autowired
    CacheRepository cacheRepository;

    @Autowired
    private
    AesFilerSearchService aesFilerSearchService;

    /**
     * Method - getByAesFilerId Returns AesFilerMaster based on given document
     * type id
     *
     * @return BaseDto
     */

    public BaseDto getByAesFilerId(Long id) {

        log.info("getByAesFilerId method is called...[" + id + "]");

        final BaseDto baseDto = new BaseDto();

        try {

            //AesFilerMaster aesFilerMaster = cacheRepository.get(SaaSUtil.getSaaSId(), AesFilerMaster.class.getName(),	id, aesFilerMasterRepository);

            AesFilerMaster aesFilerMaster = aesFilerMasterRepository.getOne(id);

            ValidateUtil.notNull(aesFilerMaster, ErrorCode.AESFILER_NOT_FOUND);

            log.info("getByAesFilerId is successfully executed....[" + aesFilerMaster.getId() + "]");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(aesFilerMaster);

        } catch (final RestException exception) {

            log.error("Exception in getByAesFilerId method : ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (final Exception exception) {

            log.error("Exception in getByAesFilerId method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);
    }

    /**
     * Method - search Returns list of AesFilerMaster based on the search
     * keyword
     *
     * @return BaseDto
     */

    public BaseDto search(AesFilerSearchDto searchDto) {

        log.info("Search method is called...." + searchDto);

        BaseDto baseDto = new BaseDto();

        try {

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(aesFilerSearchService.search(searchDto));

            log.info("Successfully Searching AesFiler...");

        } catch (Exception exception) {

            log.error("Exception in Search method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);

    }

    /**
     * Method - create Creates and returns a new document type master
     *
     * @returns AesFilerMaster
     */

    public BaseDto create(AesFilerMaster aesFilerMaster) {

        log.info("Create method is called - aesFilerMasterId ..");

        final BaseDto baseDto = new BaseDto();

        try {

            aesFilerMasterValidator.validate(aesFilerMaster);

            //cacheRepository.save(SaaSUtil.getSaaSId(), AesFilerMaster.class.getName(), aesFilerMaster, aesFilerMasterRepository);

            aesFilerMasterRepository.save(aesFilerMaster);

            log.info("AesFiler Saved successfully.....");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(aesFilerMaster);

        } catch (RestException exception) {

            log.error("Exception Cause 1 ::: ", exception);
            baseDto.setResponseCode(exception.getMessage());

        } catch (Exception exception) {

            log.error("Exception occured : ", exception);

            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Exception Cause 1 : " + exceptionCause1);

            // General error condition
            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            // Duplicate document type Code

            if (exceptionCause1.contains(UniqueKey.UK_DOCUMENT_DOCUMENTTYPECODE)) {
                baseDto.setResponseCode(ErrorCode.AESFILER_CODE_ALREADY_EXIST);

            }
        }

        return appUtil.setDesc(baseDto);
    }

    /**
     * Method - update Updates and returns a updated document type master
     *
     * @param existingAesFilerMaster
     * @returns AesFilerMaster
     */

    public BaseDto update(AesFilerMaster existingAesFilerMaster) {

        log.info("Update method is called - existingAesFilerMaster : [" + existingAesFilerMaster.getId() + "]");

        BaseDto baseDto = new BaseDto();

        try {

            ValidateUtil.notNull(existingAesFilerMaster.getId(), ErrorCode.AESFILER_NOT_FOUND);

            aesFilerMasterValidator.validate(existingAesFilerMaster);

            aesFilerMasterRepository.getOne(existingAesFilerMaster.getId());

            //existingAesFilerMaster = cacheRepository.save(SaaSUtil.getSaaSId(), AesFilerMaster.class.getName(),existingAesFilerMaster, aesFilerMasterRepository);

            existingAesFilerMaster = aesFilerMasterRepository.save(existingAesFilerMaster);

            log.info("AesFilerMaster updated successfully....[" + existingAesFilerMaster.getId() + "]");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(existingAesFilerMaster);

        } catch (RestException exception) {

            log.error("Exception occured with error code ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (ObjectOptimisticLockingFailureException e) {

            log.error("Error in editing", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_LOCKED_RECORD);

        } catch (JpaObjectRetrievalFailureException e) {

            log.error("Error in editing", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_DELETED_RECORD);

        } catch (Exception exception) {

            log.error("Exception occured : ", exception);

            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Exception Cause 1 ::: " + exceptionCause1);

            // General error condition
            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            // Duplicate document type Code

            if (exceptionCause1.contains(UniqueKey.UK_DOCUMENT_DOCUMENTTYPECODE)) {
                baseDto.setResponseCode(ErrorCode.AESFILER_CODE_ALREADY_EXIST);

            }
        }

        return appUtil.setDesc(baseDto);
    }

    /**
     * Deletes the Existing AesFilerMaster Based on the Given Id
     *
     * @Returns BaseDto
     */
    public BaseDto delete(Long id) {

        log.info("Delete method is Invoked...[" + id + "]");

        final BaseDto baseDto = new BaseDto();

        try {

            //cacheRepository.delete(SaaSUtil.getSaaSId(), AesFilerMaster.class.getName(), id, aesFilerMasterRepository);

            aesFilerMasterRepository.delete(id);

            log.info("AesFilerMaster Deleted Successfully....[" + id + "]");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

        } catch (RestException exception) {

            log.error("Exception in Delete method : ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (DataIntegrityViolationException exception) {

            log.error("Exception occured : ", exception);

            ExceptionHelper helper = new ExceptionHelper(exception);

            log.error("Foreign key reference : " + helper.getFkReference());

            baseDto.setResponseCode(ErrorCode.ERROR_FK_CONSTRAINT);

        } catch (Exception exception) {

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            log.error("Exception in Delete method : ", exception);

            String rootCause = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Root Cause of Exception : " + rootCause);

            if (rootCause.contains("FK_")) {
                baseDto.setResponseCode(ErrorCode.CANNOT_DELETE_FK_RECORD);
            }
        }

        return appUtil.setDesc(baseDto);
    }

}
