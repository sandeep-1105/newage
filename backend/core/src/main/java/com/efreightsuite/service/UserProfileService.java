package com.efreightsuite.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.SearchRequest;
import com.efreightsuite.dto.UserProfileDto;
import com.efreightsuite.dto.UserProfileSearchDto;
import com.efreightsuite.enumeration.UserStatus;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.ExceptionHelper;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.exception.UniqueKey;
import com.efreightsuite.model.DefaultMasterData;
import com.efreightsuite.model.LocationMaster;
import com.efreightsuite.model.Role;
import com.efreightsuite.model.UserCompany;
import com.efreightsuite.model.UserCompanyLocation;
import com.efreightsuite.model.UserProfile;
import com.efreightsuite.repository.DefaultMasterDataRepository;
import com.efreightsuite.repository.LocationMasterRepository;
import com.efreightsuite.repository.UserProfileRepository;
import com.efreightsuite.repository.WorkFlowGroupRepository;
import com.efreightsuite.search.CommonSearchImpl;
import com.efreightsuite.search.UserProfileSearchImpl;
import com.efreightsuite.util.AppUtil;
import com.efreightsuite.util.PasswordUtils;
import com.efreightsuite.util.SaaSUtil;
import com.efreightsuite.util.ValidateUtil;
import com.efreightsuite.validation.UserProfileValidator;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.orm.ObjectOptimisticLockingFailureException;
import org.springframework.orm.jpa.JpaObjectRetrievalFailureException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class UserProfileService {

    @Autowired
    private
    UserProfileRepository userProfileRepo;

    @Autowired
    private
    EntityManager em;

    @Autowired
    private
    LocationMasterRepository locationMasterRepository;

    @Autowired
    UserProfileValidator userProfileValidator;

    @Autowired
    private
    DefaultMasterDataRepository defaultMasterDataRepository;

    @Autowired
    private
    AppUtil appUtil;

    @Autowired
    private
    CommonSearchImpl commonSearchImpl;

    @Autowired
    private
    UserProfileSearchImpl userProfileSearchImpl;

    @Autowired
    private
    WorkFlowGroupRepository workFlowGroupRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    public BaseDto saveUser(UserProfileDto userProfileDto) {
        UserProfile userProfile = new UserProfile(userProfileDto);
        BaseDto respDto = new BaseDto();
        try {
            userProfileValidator.validate(userProfile);
            userProfile.setWorkFlowList(workFlowGroupRepository.findAll());
            if (CollectionUtils.isNotEmpty(userProfile.getUserCompanyList())) {
                for (UserCompany userCompany : userProfile.getUserCompanyList()) {
                    userCompany.setUserProfile(userProfile);
                    if (CollectionUtils.isNotEmpty(userCompany.getUserCompanyLocationList())) {
                        for (UserCompanyLocation userCompanyLocation : userCompany.getUserCompanyLocationList()) {
                            userCompanyLocation.setUserCompany(userCompany);
                        }
                    } else {
                        userCompany.setUserCompanyLocationList(null);
                    }
                }
            } else {
                userProfile.setUserCompanyList(null);
            }
            userProfile.setPassword(passwordEncoder.encode(userProfile.getPassword()));
            UserProfile saved = userProfileRepo.save(userProfile);

            log.info("User Profile Saved successfully.....  " + userProfile.getId() + "]");
            respDto.setResponseObject(new UserProfile(saved.getId(), saved.getUserName()));
            respDto.setResponseCode(ErrorCode.SUCCESS);
        } catch (RestException exception) {

            log.error("RestException in Save User method : ", exception);

            respDto.setResponseCode(exception.getMessage());

        } catch (Exception exception) {

            log.error("Exception in Save User method : ", exception);

            String exceptionCause1 = ExceptionUtils
                    .getRootCauseMessage(exception);

            log.error("Exception Cause 1 ::: " + exceptionCause1);

            respDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            if (exceptionCause1.contains(UniqueKey.UK_USERPROFILE_USERNAME)) {
                respDto.setResponseCode(ErrorCode.USER_NAME_ALREADY_EXIST);
            }
            if (exceptionCause1.contains(UniqueKey.UK_USERCOM_USERCOMID)) {
                respDto.setResponseCode(ErrorCode.USER_COMPANY_ALREADY_EXIST);
            }

            if (exceptionCause1.contains(UniqueKey.UK_USERCOMLOC_USERCOMLOCID)) {
                respDto.setResponseCode(ErrorCode.USER_COMPANY_LOCATION_ALREADY_EXIST);
            }

            if (exceptionCause1.contains(UniqueKey.UK_USER_ROLE)) {
                respDto.setResponseCode(ErrorCode.USER_ROLE_ALREADY_EXIST);
            }
        }

        return appUtil.setDesc(respDto);
    }

    public BaseDto updateUser(UserProfileDto userProfileDto) {

        UserProfile updateUserProfile = new UserProfile(userProfileDto);

        return update(updateUserProfile);
    }

    private BaseDto update(UserProfile updateUserProfile) {
        log.info("updateUser method is called.... : ");
        BaseDto respDto = new BaseDto();

        try {
            userProfileValidator.validate(updateUserProfile);

            ValidateUtil.notNull(updateUserProfile.getId(), ErrorCode.USER_ID_NOT_NULL);

            userProfileRepo.getOne(updateUserProfile.getId());


            if (updateUserProfile.getStatus() != null && updateUserProfile.getStatus().equals(UserStatus.Active)) {

                if (updateUserProfile.getUserCompanyList() != null && updateUserProfile.getUserCompanyList().size() == 0) {

                    throw new RestException(ErrorCode.USER_COMPANY_AND_ROLE_MANDATORY);
                }

                if (updateUserProfile.getRoleList() != null && updateUserProfile.getRoleList().size() == 0) {
                    throw new RestException(ErrorCode.USER_COMPANY_AND_ROLE_MANDATORY);
                }

            }


            if (updateUserProfile.getUserCompanyList() != null && updateUserProfile.getUserCompanyList().size() > 0) {
                for (UserCompany userCompany : updateUserProfile.getUserCompanyList()) {
                    userCompany.setUserProfile(updateUserProfile);


                    if (userCompany.getUserCompanyLocationList() != null && userCompany.getUserCompanyLocationList().size() > 0) {
                        for (UserCompanyLocation userCompanyLocation : userCompany.getUserCompanyLocationList()) {
                            userCompanyLocation.setUserCompany(userCompany);
                        }
                    } else {
                        userCompany.setUserCompanyLocationList(new ArrayList<>());
                    }

                }
            } else {
                updateUserProfile.setUserCompanyList(new ArrayList<>());
            }

            if (!PasswordUtils.isBcryptPassword(updateUserProfile.getPassword())) {
                updateUserProfile.setPassword(passwordEncoder.encode(updateUserProfile.getPassword()));
            }
            UserProfile updated = userProfileRepo.save(updateUserProfile);

            log.info("UserProfile updated successfully...[" + updateUserProfile.getId() + "]");

            respDto.setResponseCode(ErrorCode.SUCCESS);

            respDto.setResponseObject(new UserProfile(updated.getId(), updated.getUserName()));
        } catch (RestException exception) {

            log.error("Exception in updateUser method : ", exception);

            respDto.setResponseCode(exception.getMessage());
        } catch (ObjectOptimisticLockingFailureException e) {

            log.error("Error in editing User Profile", e);

            respDto.setResponseCode(ErrorCode.CANNOT_UPDATE_LOCKED_RECORD);

        } catch (JpaObjectRetrievalFailureException e) {

            log.error("Error in editing User Profile", e);

            respDto.setResponseCode(ErrorCode.CANNOT_UPDATE_DELETED_RECORD);

        } catch (Exception exception) {

            log.error("Exception in updateUser method : ", exception);

            String exceptionCause1 = ExceptionUtils
                    .getRootCauseMessage(exception);

            log.error("Exception Cause 1 ::: " + exceptionCause1);

            respDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            if (exceptionCause1.contains(UniqueKey.UK_USERPROFILE_USERNAME)) {
                respDto.setResponseCode(ErrorCode.USER_NAME_ALREADY_EXIST);
            }

            if (exceptionCause1.contains(UniqueKey.UK_USERCOM_USERCOMID)) {
                respDto.setResponseCode(ErrorCode.USER_COMPANY_ALREADY_EXIST);
            }

            if (exceptionCause1.contains(UniqueKey.UK_USERCOMLOC_USERCOMLOCID)) {
                respDto.setResponseCode(ErrorCode.USER_COMPANY_LOCATION_ALREADY_EXIST);
            }

            if (exceptionCause1.contains(UniqueKey.UK_USER_ROLE)) {
                respDto.setResponseCode(ErrorCode.USER_ROLE_ALREADY_EXIST);
            }

        }
        return appUtil.setDesc(respDto);
    }

    public BaseDto delete(Long id) {

        log.info("delete  method is called....[" + id + "]");

        BaseDto baseDto = new BaseDto();

        try {

            userProfileRepo.delete(id);

            log.info("Successfully deleted User by id...");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

        } catch (DataIntegrityViolationException exception) {

            log.error("Exception occured : ", exception);

            ExceptionHelper helper = new ExceptionHelper(exception);

            log.error("Foreign key reference : " + helper.getFkReference());

            baseDto.setResponseCode(ErrorCode.ERROR_FK_CONSTRAINT);

        } catch (Exception exception) {

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            log.error("Exception in Delete method : ", exception);

            String rootCause = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Root Cause of Exception : " + rootCause);

            if (rootCause.contains("FK_")) {
                baseDto.setResponseCode(ErrorCode.CANNOT_DELETE_FK_RECORD);
            }
        }

        return appUtil.setDesc(baseDto);

    }

    public BaseDto search(UserProfileSearchDto userProfileSearchDto) {

        log.info("Search method is called....");

        BaseDto baseDto = new BaseDto();

        try {

            baseDto.setResponseObject(userProfileSearchImpl.search(userProfileSearchDto));


            baseDto.setResponseCode(ErrorCode.SUCCESS);

        } catch (Exception exception) {

            log.error("Exception in Search method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);
    }

    public BaseDto search(SearchRequest searchRequest) {

        log.info("Search method is called....");

        BaseDto baseDto = new BaseDto();

        try {

            baseDto.setResponseObject(userProfileSearchImpl.search(searchRequest));

            baseDto.setResponseCode(ErrorCode.SUCCESS);

        } catch (Exception exception) {

            log.error("Exception in Search method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);
    }

    public BaseDto searchUserNotInList(SearchRequest searchRequest) {

        log.info("searchUserNotInList method is called....");

        BaseDto baseDto = new BaseDto();

        try {

            baseDto.setResponseObject(commonSearchImpl.searchUserNotInList(searchRequest));

            baseDto.setResponseCode(ErrorCode.SUCCESS);

        } catch (Exception exception) {

            log.error("Exception in searchUserNotInList method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);
    }

    public BaseDto searchUserList(SearchRequest searchRequest) {

        log.info("Search method is called....");

        BaseDto baseDto = new BaseDto();

        try {

            baseDto.setResponseObject(userProfileSearchImpl.searchUserList(searchRequest));

            baseDto.setResponseCode(ErrorCode.SUCCESS);

        } catch (Exception exception) {

            log.error("Exception in Search method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);
    }

    public BaseDto get(Long id) {

        log.info("Get method is called....[" + id + "]");

        BaseDto baseDto = new BaseDto();

        try {

            UserProfile userProfile = userProfileRepo.getOne(id);

            ValidateUtil.notNull(userProfile, ErrorCode.USER_ID_NOT_FOUND);
            log.info("Successfully getting user profile by id....[" + id + "]");

            if (userProfile.getRoleList() != null && userProfile.getRoleList().size() != 0) {
                for (Role role : userProfile.getRoleList()) {
                    //role.setFeature(null);
//					Have to Change
//					role.setUserList(null);
                }
            }

            UserProfileDto userProfileDto = new UserProfileDto(userProfile);


            baseDto.setResponseCode(ErrorCode.SUCCESS);
            baseDto.setResponseObject(userProfileDto);

        } catch (RestException exception) {

            log.error("RestException in get method : ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (Exception exception) {

            log.error("Exception in get method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);

        }

        return appUtil.setDesc(baseDto);
    }

    public BaseDto getAll() {

        log.info("getAll method is called....");

        BaseDto baseDto = new BaseDto();

        try {

            List<UserProfile> userProfileList = userProfileRepo.findAll();

            // Validating user profile list is empty or not
            ValidateUtil.notEmpty(userProfileList, ErrorCode.USERPROFILE_LIST_EMPTY);
            log.info("Successfully getting list of user profile...");
            baseDto.setResponseCode(ErrorCode.SUCCESS);
            baseDto.setResponseObject(userProfileList);

        } catch (RestException re) {
            log.error("Exception in getAll method ", re);
            baseDto.setResponseCode(re.getMessage());

        } catch (Exception e) {
            log.error("Exception in getAll method ", e);
            baseDto.setResponseCode(ErrorCode.FAILED);

        }
        return appUtil.setDesc(baseDto);
    }

    public UserProfile getCurrentUser() {
        log.info("UserProfile Service getCurrentUser");
        Object obj = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (obj != null && !obj.toString().equals("anonymousUser")) {
            return (UserProfile) obj;
        }
        return null;
    }

    public BaseDto changeprofile(Long locationId) {

        BaseDto baseDto = new BaseDto();

        LocationMaster location = locationMasterRepository.findById(locationId);
        AppUtil.setPartyToNull(location.getPartyMaster());
        location.setTmpPartyMaster(location.getPartyMaster());


        UserProfile userProfile = (UserProfile) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        userProfile.setSelectedCompany(location.getCompanyMaster());
        userProfile.setSelectedUserLocation(location);

        Map<String, Object> map = new HashMap<>();

        for (DefaultMasterData data : defaultMasterDataRepository.getByLocation(location.getId())) {
            map.put(data.getCode(), data.getValue());
        }

        userProfile.setAppMasterData(map);

        List<GrantedAuthority> authorities = new ArrayList<>();

        for (String key : userProfile.getFeatureMap().keySet()) {
            authorities.add(new SimpleGrantedAuthority("ROLE_" + key));
        }

        Authentication authentication = new UsernamePasswordAuthenticationToken(userProfile,
                SaaSUtil.getSaaSId(), authorities);

        SecurityContextHolder.getContext().setAuthentication(authentication);

        baseDto.setResponseCode(ErrorCode.SUCCESS);

        UserProfile tmpProfile = new UserProfile();
        tmpProfile.setAppMasterData(map);
        tmpProfile.setSelectedUserLocation(location);


        baseDto.setResponseObject(tmpProfile);

        return appUtil.setDesc(baseDto);

    }

    public BaseDto getCompanyAdress(Long companyid) {

        BaseDto baseDto = new BaseDto();
        try {

            if (companyid != null) {
                String hql = "FROM LocationMaster lm WHERE lm.companyMaster like '" + companyid + "%' ";

                Query query = em.createQuery(hql);

                List<LocationMaster> list = query.getResultList();

                if (list.size() > 0) {
                    LocationMaster location = list.get(0);
                    baseDto.setResponseCode(ErrorCode.SUCCESS);
                    baseDto.setResponseObject(location);
                }
            }
        } catch (Exception e) {
            log.error("Exception in getAll method ", e);
            baseDto.setResponseCode(ErrorCode.FAILED);
        }
        return appUtil.setDesc(baseDto);
    }

    public BaseDto activate(Long userId) {
        UserProfile savedUser = userProfileRepo.findOne(userId);
        savedUser.setActivatedDate(new Date());
        savedUser.setStatus(UserStatus.Active);
        return update(savedUser);
    }

    public BaseDto deactivate(Long userId) {
        UserProfile savedUser = userProfileRepo.findOne(userId);
        savedUser.setStatus(UserStatus.Deactivate);
        savedUser.setDeactivatedDate(new Date());
        return update(savedUser);
    }
}
