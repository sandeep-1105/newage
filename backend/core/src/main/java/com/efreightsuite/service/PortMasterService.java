package com.efreightsuite.service;

import java.util.ArrayList;

import com.efreightsuite.common.component.CacheRepository;
import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.PortSearchDto;
import com.efreightsuite.dto.SearchRequest;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.exception.UniqueKey;
import com.efreightsuite.model.PortEdiMapping;
import com.efreightsuite.model.PortMaster;
import com.efreightsuite.repository.PortMasterRepository;
import com.efreightsuite.search.PortSearchImpl;
import com.efreightsuite.util.AppUtil;
import com.efreightsuite.util.SaaSUtil;
import com.efreightsuite.util.ValidateUtil;
import com.efreightsuite.validation.PortMasterValidator;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.exception.ExceptionUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.ObjectOptimisticLockingFailureException;
import org.springframework.orm.jpa.JpaObjectRetrievalFailureException;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class PortMasterService {

    @Autowired
    private
    AppUtil appUtil;

    @Autowired
    private
    PortMasterRepository portMasterRepository;

    @Autowired
    private
    CacheRepository cacheRepository;

    @Autowired
    private
    PortMasterValidator portMasterValidator;

    @Autowired
    private
    PortSearchImpl portSearchImpl;

    public BaseDto search(PortSearchDto searchDto) {

        log.info("Search method is called...." + searchDto);

        BaseDto baseDto = new BaseDto();

        try {

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(portSearchImpl.search(searchDto));

            log.info("Successfully Searching ...");

        } catch (Exception exception) {

            log.error("Exception in Search method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);

    }

    public BaseDto search(SearchRequest searchRequest) {

        log.info("Search method is called...." + searchRequest);

        BaseDto baseDto = new BaseDto();

        try {

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(portSearchImpl.search(searchRequest));

            log.info("Successfully Searching ...");

        } catch (Exception exception) {

            log.error("Exception in Search method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);

    }

    public BaseDto search(SearchRequest searchRequest, String transportMode) {

        log.info("Search method is called....Transport Mode " + transportMode);

        BaseDto baseDto = new BaseDto();

        try {

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(portSearchImpl.search(searchRequest, transportMode));

            log.info("Successfully Searching ...");

        } catch (Exception exception) {

            log.error("Exception in Search method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);

    }

    public BaseDto searchByGroup(SearchRequest searchRequest, Long groupId, String transportMode) {

        log.info("searchByGroup method is called....Transport Mode " + groupId);

        BaseDto baseDto = new BaseDto();

        try {

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(portSearchImpl.searchByGroup(searchRequest, groupId, transportMode));

            log.info("Successfully Searching searchByGroup...");

        } catch (Exception exception) {

            log.error("Exception in searchByGroup method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);

    }

    public BaseDto saveOrUpdate(PortMaster portMaster) {

        log.info("PortMasterService -> saveOrUpdate method is called.....");

        BaseDto baseDto = new BaseDto();

        try {

            portMasterValidator.validate(portMaster);


            portMaster.setPortGroupCode(portMaster.getPortGroupMaster().getPortGroupCode());
            portMaster.setCountryCode(portMaster.getPortGroupMaster().getCountryMaster().getCountryCode());

            if (portMaster.getId() != null) {
                portMasterRepository.getOne(portMaster.getId());
            }

            if (portMaster.getEdiList() != null && portMaster.getEdiList().size() != 0) {

                for (PortEdiMapping portEdiMapping : portMaster.getEdiList()) {
                    portEdiMapping.setPortMaster(portMaster);
                    portEdiMapping.setPortCode(portMaster.getPortCode());
                }

            } else {
                portMaster.setEdiList(new ArrayList<>());
                portMaster.getEdiList().clear();
            }


            portMasterRepository.save(portMaster);

            cacheRepository.remove(SaaSUtil.getSaaSId(), PortMaster.class.getName(), portMaster.getId());

            log.info("Port Master Saved successfully  :  [" + portMaster.getId() + "]");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(ErrorCode.SUCCESS);

        } catch (RestException exception) {

            log.error("Exception in PortMasterService -> saveOrUpdate method ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (ObjectOptimisticLockingFailureException e) {

            log.error("Exception in PortMasterService -> saveOrUpdate method ", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_LOCKED_RECORD);

        } catch (JpaObjectRetrievalFailureException e) {

            log.error("Exception in PortMasterService -> saveOrUpdate method ", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_DELETED_RECORD);

        } catch (Exception exception) {

            log.error("Exception in PortMasterService -> saveOrUpdate method ", exception);

            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Exception Cause 1 ::: " + exceptionCause1);

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            if (exceptionCause1.contains("FK_")) {
                baseDto.setResponseCode(ErrorCode.SAVE_UPDATE_FK_RECORD);
            }

            if (exceptionCause1.contains(UniqueKey.UK_PORT_PORTCODE)) {
                baseDto.setResponseCode(ErrorCode.PORT_CODE_ALREADY_EXIST);
            }

            if (exceptionCause1.contains(UniqueKey.UK_PORT_PORTNAME)) {
                baseDto.setResponseCode(ErrorCode.PORT_NAME_ALREADY_EXIST);
            }

            if (exceptionCause1.contains(UniqueKey.UK_EDIPORT_EDITYPE)) {
                baseDto.setResponseCode(ErrorCode.EDI_TYPE_ALREDY_EXIST);
            }

        }
        return appUtil.setDesc(baseDto);
    }

    public BaseDto delete(Long id) {

        log.info("PortMasterService -> delete  method called....[ " + id + " ] ");

        BaseDto baseDto = new BaseDto();

        try {

            cacheRepository.delete(SaaSUtil.getSaaSId(), PortMaster.class.getName(), id, portMasterRepository);

            log.info("Port Master Deleted Successfully.... [ +" + id + "]");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

        } catch (Exception exception) {

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            log.error("Exception in PortGroupMasterService -> delete method ", exception);

            String rootCause = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Root Cause of Exception : " + rootCause);

            if (rootCause.contains("FK_")) {
                baseDto.setResponseCode(ErrorCode.CANNOT_DELETE_FK_RECORD);
            }
        }
        return appUtil.setDesc(baseDto);

    }

    public BaseDto get(Long id) {

        log.info("PortMasterService -> get method is called....[" + id + "]");

        BaseDto baseDto = new BaseDto();

        try {

            PortMaster portMaster = cacheRepository.get(SaaSUtil.getSaaSId(), PortMaster.class.getName(), id, portMasterRepository);

            ValidateUtil.notNull(portMaster, ErrorCode.PORT_ID_NOT_FOUND);

            log.info("Successfully getting port by given id...[" + id + "]" + portMaster.getEdiList().size());

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(portMaster);

        } catch (RestException exception) {

            log.error("Exception in PortMasterService -> get method ", exception);

            baseDto.setResponseCode(exception.getMessage());
        } catch (Exception exception) {

            log.error("Exception in PortMasterService -> get method", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);
    }

    public BaseDto searchCountry(SearchRequest searchRequest, String transportMode, String countryCode) {
        log.info("Search method is called....Transport Mode  " + transportMode + " and country code:::" + countryCode);

        BaseDto baseDto = new BaseDto();

        try {

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(portSearchImpl.searchByCountry(searchRequest, transportMode, countryCode));

            log.info("Successfully Searching ...");

        } catch (Exception exception) {

            log.error("Exception in Search method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);
    }

}
