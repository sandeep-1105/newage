package com.efreightsuite.service;

import com.efreightsuite.common.component.CacheRepository;
import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.DepartmentSearchDto;
import com.efreightsuite.dto.SearchRequest;
import com.efreightsuite.enumeration.SequenceType;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.exception.UniqueKey;
import com.efreightsuite.model.DepartmentMaster;
import com.efreightsuite.repository.DepartmentMasterRepository;
import com.efreightsuite.search.DepartmentSearchImpl;
import com.efreightsuite.util.AppUtil;
import com.efreightsuite.util.SaaSUtil;
import com.efreightsuite.validation.DepartmentMasterValidator;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.exception.ExceptionUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.ObjectOptimisticLockingFailureException;
import org.springframework.orm.jpa.JpaObjectRetrievalFailureException;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class DepartmentMasterService {

    @Autowired
    private
    AppUtil appUtil;

    @Autowired
    private
    DepartmentMasterRepository departmentMasterRepository;

    @Autowired
    private
    CacheRepository cacheRepository;

    @Autowired
    private
    DepartmentSearchImpl departmentSearchImpl;

    @Autowired
    private
    DepartmentMasterValidator departmentMasterValidator;

    @Autowired
    private
    SequenceGeneratorService sequenceGeneratorService;

    public BaseDto search(DepartmentSearchDto searchDto) {

        log.info("Search method is called...." + searchDto);

        BaseDto baseDto = new BaseDto();

        try {

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(departmentSearchImpl.search(searchDto));

            log.info("Successfully Searching ...");

        } catch (Exception exception) {

            log.error("Exception in Search method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);

    }

    public BaseDto search(SearchRequest searchRequest) {

        log.info("Search method is called...." + searchRequest);

        BaseDto baseDto = new BaseDto();

        try {

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(departmentSearchImpl.search(searchRequest));

            log.info("Successfully Searching ...");

        } catch (Exception exception) {

            log.error("Exception in Search method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);

    }

    public BaseDto saveOrUpdate(DepartmentMaster departmentMaster) {

        log.info("DepartmentMasterService -> saveOrUpdate method is called.....");

        BaseDto baseDto = new BaseDto();

        try {

            departmentMasterValidator.validate(departmentMaster);

            if (departmentMaster.getId() != null) {
                departmentMasterRepository.getOne(departmentMaster.getId());
            } else {
                departmentMaster.setDepartmentCode(sequenceGeneratorService.getSequence(SequenceType.DEPARTMENT));
            }

            departmentMasterRepository.save(departmentMaster);

            cacheRepository.remove(SaaSUtil.getSaaSId(), DepartmentMaster.class.getName(), departmentMaster.getId());

            log.info("Department Master Saved successfully  :  [" + departmentMaster.getId() + "]");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(ErrorCode.SUCCESS);

        } catch (RestException exception) {

            log.error("Exception in DepartmentMasterService -> saveOrUpdate method ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (ObjectOptimisticLockingFailureException e) {

            log.error("Exception in DepartmentMasterService -> saveOrUpdate method ", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_LOCKED_RECORD);

        } catch (JpaObjectRetrievalFailureException e) {

            log.error("Exception in DepartmentMasterService -> saveOrUpdate method ", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_DELETED_RECORD);

        } catch (Exception exception) {

            log.error("Exception in DepartmentMasterService -> saveOrUpdate method ", exception);

            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Exception Cause 1 ::: " + exceptionCause1);

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            if (exceptionCause1.contains("FK_")) {
                baseDto.setResponseCode(ErrorCode.SAVE_UPDATE_FK_RECORD);
            }

            if (exceptionCause1.contains(UniqueKey.UK_DEPARTMENT_DEPTCODE)) {
                baseDto.setResponseCode(ErrorCode.DEPARTMENT_CODE_DUPLICATED);
            }

            if (exceptionCause1.contains(UniqueKey.UK_DEPARTMENT_DEPTNAME)) {
                baseDto.setResponseCode(ErrorCode.DEPARTMENT_NAME_DUPLICATED);
            }
        }
        return appUtil.setDesc(baseDto);
    }

    public BaseDto delete(Long id) {

        log.info("DepartmentMasterService -> delete  method called....[ " + id + " ] ");

        BaseDto baseDto = new BaseDto();

        try {

            cacheRepository.delete(SaaSUtil.getSaaSId(), DepartmentMaster.class.getName(), id,
                    departmentMasterRepository);

            log.info("Department Master Deleted Successfully.... [ +" + id + "]");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

        } catch (Exception exception) {

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            log.error("Exception in DepartmentMasterService -> delete method ", exception);

            String rootCause = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Root Cause of Exception : " + rootCause);

            if (rootCause.contains("FK_")) {
                baseDto.setResponseCode(ErrorCode.CANNOT_DELETE_FK_RECORD);
            }
        }
        return appUtil.setDesc(baseDto);

    }

    public BaseDto get(Long id) {

        log.info("DepartmentMasterService -> get method is called....[" + id + "]");

        BaseDto baseDto = new BaseDto();

        try {

            DepartmentMaster departmentMaster = cacheRepository.get(SaaSUtil.getSaaSId(),
                    DepartmentMaster.class.getName(), id, departmentMasterRepository);

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(departmentMaster);

        } catch (Exception exception) {

            log.error("Exception in DepartmentMasterService -> get method", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);
    }

}
