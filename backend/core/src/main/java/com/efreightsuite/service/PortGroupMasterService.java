package com.efreightsuite.service;

import com.efreightsuite.common.component.CacheRepository;
import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.PortGroupSearchDto;
import com.efreightsuite.dto.SearchRequest;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.exception.UniqueKey;
import com.efreightsuite.model.PortGroupMaster;
import com.efreightsuite.repository.PortGroupMasterRepository;
import com.efreightsuite.search.PortGroupSearchImpl;
import com.efreightsuite.util.AppUtil;
import com.efreightsuite.util.SaaSUtil;
import com.efreightsuite.util.ValidateUtil;
import com.efreightsuite.validation.PortGroupMasterValidator;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.exception.ExceptionUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.ObjectOptimisticLockingFailureException;
import org.springframework.orm.jpa.JpaObjectRetrievalFailureException;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class PortGroupMasterService {

    @Autowired
    private
    AppUtil appUtil;

    @Autowired
    private
    CacheRepository cacheRepository;

    @Autowired
    private
    PortGroupMasterRepository portGroupMasterRepository;

    @Autowired
    private
    PortGroupMasterValidator portGroupMasterValidator;

    @Autowired
    private
    PortGroupSearchImpl portGroupSearchImpl;

    public BaseDto get(Long id) {


        log.info("PortGroupView -> get method is called....[" + id + "]");

        BaseDto baseDto = new BaseDto();

        try {

            PortGroupMaster portGroupMaster = cacheRepository.get(SaaSUtil.getSaaSId(), PortGroupMaster.class.getName(), id, portGroupMasterRepository);

            ValidateUtil.notNull(portGroupMaster, ErrorCode.PORT_GROUP_ID_NOT_FOUND);

            log.info("Successfully getting Port Group Master by id...[" + portGroupMaster.getId() + "]");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(portGroupMaster);

        } catch (RestException exception) {

            log.error("Exception in PortGroupMasterService -> get method ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (Exception exception) {

            log.error("Exception in PortGroupMasterService -> get method", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);
    }

    public BaseDto saveOrUpdate(PortGroupMaster portGroupMaster) {

        log.info("PortGroupMasterService -> update method is called.... [" + portGroupMaster.getId() + "]");

        BaseDto baseDto = new BaseDto();

        try {

            portGroupMasterValidator.validate(portGroupMaster);

            if (portGroupMaster.getId() != null) {
                portGroupMasterRepository.getOne(portGroupMaster.getId());
            }

            portGroupMasterRepository.save(portGroupMaster);

            cacheRepository.remove(SaaSUtil.getSaaSId(), PortGroupMaster.class.getName(), portGroupMaster.getId());

            log.info("Port Group Master updated successfully...[" + portGroupMaster.getId() + "]");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(ErrorCode.SUCCESS);

        } catch (RestException exception) {

            log.error("Exception in PortGroupMasterService -> update method ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (ObjectOptimisticLockingFailureException e) {

            log.error("Exception in PortGroupMasterService -> update method  ", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_LOCKED_RECORD);

        } catch (JpaObjectRetrievalFailureException e) {

            log.error("Exception in PortGroupMasterService -> update method ", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_DELETED_RECORD);

        } catch (Exception exception) {

            log.error("Exception in PortGroupMasterService -> update method ", exception);

            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Exception Cause 1 ::: " + exceptionCause1);

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            // Duplicate Port Group code
            if (exceptionCause1.contains(UniqueKey.UK_PORTGROUP_PORTGROUPCODE)) {
                baseDto.setResponseCode(ErrorCode.PORT_GROUP_CODE_ALREADY_EXIST);
            }

            // Duplicate Port Group name
            if (exceptionCause1.contains(UniqueKey.UK_PORTGROUP_PORTGROUPNAME)) {
                baseDto.setResponseCode(ErrorCode.PORT_GROUP_NAME_ALREADY_EXIST);
            }
        }

        return appUtil.setDesc(baseDto);
    }

    public BaseDto delete(Long id) {

        log.debug("PortGroupMasterService -> delete method is called: [ +" + id + "]");

        BaseDto baseDto = new BaseDto();

        try {
            cacheRepository.delete(SaaSUtil.getSaaSId(), PortGroupMaster.class.getName(), id,
                    portGroupMasterRepository);

            log.info("PortGroupMaster Deleted Successfully.... [ +" + id + "]");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

        } catch (Exception exception) {

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            log.error("Exception in PortGroupMasterService -> delete method ", exception);

            String rootCause = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Root Cause of Exception : " + rootCause);

            if (rootCause.contains("FK_")) {
                baseDto.setResponseCode(ErrorCode.CANNOT_DELETE_FK_RECORD);
            }
        }

        return appUtil.setDesc(baseDto);
    }

    public BaseDto search(PortGroupSearchDto searchDto) {

        log.info("Search method is called...." + searchDto);

        BaseDto baseDto = new BaseDto();

        try {

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(portGroupSearchImpl.search(searchDto));

            log.info("Successfully Searching ...");

        } catch (Exception exception) {

            log.error("Exception in Search method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);

    }

    public BaseDto search(SearchRequest searchRequest) {

        log.info("Search method is called...." + searchRequest);

        BaseDto baseDto = new BaseDto();

        try {

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(portGroupSearchImpl.searchByGroup(searchRequest));

            log.info("Successfully Searching ...");

        } catch (Exception exception) {

            log.error("Exception in Search method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);

    }
}
