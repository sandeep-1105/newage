package com.efreightsuite.service;

import com.efreightsuite.common.component.CacheRepository;
import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.SearchRequest;
import com.efreightsuite.dto.ServiceTaxCategoryMasterSearchDto;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.ExceptionHelper;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.exception.UniqueKey;
import com.efreightsuite.model.ServiceTaxCategoryMaster;
import com.efreightsuite.model.ServiceTaxPercentage;
import com.efreightsuite.repository.ServiceTaxCategoryMasterRepository;
import com.efreightsuite.search.ServiceTaxCategorySearchImpl;
import com.efreightsuite.util.AppUtil;
import com.efreightsuite.util.SaaSUtil;
import com.efreightsuite.util.ValidateUtil;
import com.efreightsuite.validation.SearchDtoValidator;
import com.efreightsuite.validation.ServiceTaxCategoryMasterValidator;
import com.efreightsuite.validation.ServiceTaxPercentageValidator;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.exception.ExceptionUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.orm.ObjectOptimisticLockingFailureException;
import org.springframework.orm.jpa.JpaObjectRetrievalFailureException;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class ServiceTaxCategroyMasterService {


    @Autowired
    private
    AppUtil appUtil;

    /**
     * ServiceTaxCategoryMasterRepository Object
     */

    @Autowired
    private
    ServiceTaxCategoryMasterRepository serviceTaxCategoryMasterRepository;

    @Autowired
    SearchDtoValidator searchDtoValidator;

    @Autowired
    private
    ServiceTaxCategoryMasterValidator serviceTaxCategoryMasterValidator;

    @Autowired
    private
    CacheRepository cacheRepository;

    @Autowired
    private
    ServiceTaxCategorySearchImpl serviceTaxCategorySearchImpl;

    @Autowired
    private
    ServiceTaxPercentageValidator serviceTaxPercentageValidator;


    /**
     * Method - search Returns list of ServiceTaxCategoryMaster based on the search
     *
     * @return BaseDto
     */

    public BaseDto search(ServiceTaxCategoryMasterSearchDto searchDto) {

        log.info("Search method is called...." + searchDto);

        BaseDto baseDto = new BaseDto();

        try {

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(serviceTaxCategorySearchImpl.search(searchDto));

            log.info("Successfully Searching ServiceTaxCategory...");

        } catch (Exception exception) {

            log.error("Exception in Search method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);

    }


    public BaseDto search(SearchRequest searchRequest) {

        log.info("Search method is called...." + searchRequest);

        BaseDto baseDto = new BaseDto();

        try {

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            //baseDto.setResponseObject(serviceTaxCategorySearchImpl.search(searchRequest));

            log.info("Successfully Searching serviceTaxCategory master...");

        } catch (Exception exception) {

            log.error("Exception in Search method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);

    }


    /**
     * Method - create Creates and returns a new serviceTaxCategoryMaster
     *
     * @param serviceTaxCategoryMaster
     * @returns BaseDto
     */

    public BaseDto create(ServiceTaxCategoryMaster serviceTaxCategoryMaster) {

        log.info("Create method is called - serviceTaxCategoryMasterId ..");

        setCountryCompanyStatus(serviceTaxCategoryMaster);

        final BaseDto baseDto = new BaseDto();

        try {

            serviceTaxCategoryMasterValidator.validate(serviceTaxCategoryMaster);

            cacheRepository.save(SaaSUtil.getSaaSId(), ServiceTaxCategoryMaster.class.getName(), serviceTaxCategoryMaster, serviceTaxCategoryMasterRepository);

            log.info("serviceTaxCategory Saved successfully.....");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(serviceTaxCategoryMaster);

        } catch (RestException exception) {

            log.error("Exception Cause 1 ::: ", exception);
            baseDto.setResponseCode(exception.getMessage());


        } catch (Exception exception) {

            log.error("Exception occured : ", exception);


            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Exception Cause 1 : " + exceptionCause1);

            // General error condition
            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            // Duplicate ServiceTaxCategoryMaster Code

            if (exceptionCause1.contains(UniqueKey.UK_SERVTAXCATE_CODE)) {
                baseDto.setResponseCode(ErrorCode.SERVICETAXCATEGORY_CODE_ALREADY_EXIST);

            }

            if (exceptionCause1.contains(UniqueKey.UK_SERVTAXCATE_NAME)) {
                baseDto.setResponseCode(ErrorCode.SERVICETAXCATEGORY_NAME_ALREADY_EXIST);
            }
        }

        return appUtil.setDesc(baseDto);
    }


    /**
     * Method - update Updates and returns a updated ServiceTaxCategoryMaster
     *
     * @returns ServiceTaxCategoryMaster
     */

    public BaseDto update(ServiceTaxCategoryMaster exsistingServiceTaxCategoryMaster) {

        log.info("Update method is called - exsistingServiceTaxCategoryMaster : [" + exsistingServiceTaxCategoryMaster.getId() + "]");

        BaseDto baseDto = new BaseDto();

        setCountryCompanyStatus(exsistingServiceTaxCategoryMaster);

        try {

            ValidateUtil.notNull(exsistingServiceTaxCategoryMaster.getId(), ErrorCode.SERVICETAXCATEGORY_ID_NOT_NULL);

            serviceTaxCategoryMasterValidator.validate(exsistingServiceTaxCategoryMaster);

            serviceTaxCategoryMasterRepository.getOne(exsistingServiceTaxCategoryMaster.getId());

            exsistingServiceTaxCategoryMaster = cacheRepository.save(SaaSUtil.getSaaSId(), ServiceTaxCategoryMaster.class.getName(), exsistingServiceTaxCategoryMaster,
                    serviceTaxCategoryMasterRepository);

            log.info("ServiceTaxCategoryMaster updated successfully....[" + exsistingServiceTaxCategoryMaster.getId() + "]");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(exsistingServiceTaxCategoryMaster);

        } catch (RestException exception) {

            log.error("Exception occured with error code ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (ObjectOptimisticLockingFailureException e) {

            log.error("Error in editing", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_LOCKED_RECORD);

        } catch (JpaObjectRetrievalFailureException e) {

            log.error("Error in editing", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_DELETED_RECORD);

        } catch (Exception exception) {

            log.error("Exception occured : ", exception);

            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Exception Cause 1 ::: " + exceptionCause1);


            // General error condition
            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            // Duplicate ServiceTaxCategoryMaster Code

            if (exceptionCause1.contains(UniqueKey.UK_SERVTAXCATE_CODE)) {
                baseDto.setResponseCode(ErrorCode.SERVICETAXCATEGORY_CODE_ALREADY_EXIST);

            }

            if (exceptionCause1.contains(UniqueKey.UK_SERVTAXCATE_NAME)) {
                baseDto.setResponseCode(ErrorCode.SERVICETAXCATEGORY_NAME_ALREADY_EXIST);
            }

        }

        return appUtil.setDesc(baseDto);
    }

    /**
     * Deletes the Existing ServiceTaxCategoryMaster Based on the Given Id
     *
     * @Returns BaseDto
     */
    public BaseDto delete(Long id) {

        log.info("Delete method is Invoked...[" + id + "]");

        final BaseDto baseDto = new BaseDto();

        try {

            cacheRepository.delete(SaaSUtil.getSaaSId(), ServiceTaxCategoryMaster.class.getName(), id, serviceTaxCategoryMasterRepository);

            log.info("ServiceTaxCategoryMaster Deleted Successfully....[" + id + "]");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

        } catch (RestException exception) {

            log.error("Exception in Delete method : ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (DataIntegrityViolationException exception) {

            log.error("Exception occured : ", exception);

            ExceptionHelper helper = new ExceptionHelper(exception);

            log.error("Foreign key reference : " + helper.getFkReference());

            baseDto.setResponseCode(ErrorCode.ERROR_FK_CONSTRAINT);

        } catch (Exception exception) {

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            log.error("Exception in Delete method : ", exception);

            String rootCause = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Root Cause of Exception : " + rootCause);

            if (rootCause.contains("FK_")) {
                baseDto.setResponseCode(ErrorCode.CANNOT_DELETE_FK_RECORD);
            }
        }

        return appUtil.setDesc(baseDto);
    }


    /**
     * Method - getById Returns ServiceTaxCategoryMaster based on given id
     *
     * @return BaseDto
     */

    public BaseDto getById(Long id) {

        log.info("getById method is called...[" + id + "]");

        final BaseDto baseDto = new BaseDto();

        try {

            ServiceTaxCategoryMaster serviceTaxCategoryMaster = cacheRepository.get(SaaSUtil.getSaaSId(), ServiceTaxCategoryMaster.class.getName(), id,
                    serviceTaxCategoryMasterRepository);

            //	ServiceTaxCategoryMaster serviceTaxCategoryMaster = serviceTaxCategoryMasterRepository.getOne(id);


            ValidateUtil.notNull(serviceTaxCategoryMaster, ErrorCode.SERVICETAXCATEGORY_ID_NOT_FOUND);

            log.info("getById is successfully executed....[" + serviceTaxCategoryMaster.getId() + "]");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(serviceTaxCategoryMaster);

        } catch (final RestException exception) {

            log.error("Exception in getById method : ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (final Exception exception) {

            log.error("Exception in getId method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);
    }

    private void setCountryCompanyStatus(ServiceTaxCategoryMaster serviceTaxCategoryMaster) {

        for (ServiceTaxPercentage serviceTaxPercentage : serviceTaxCategoryMaster.getTaxPercentageList()) {
            serviceTaxPercentage.setCompanyMaster(AuthService.getCurrentUser().getSelectedUserLocation().getCompanyMaster());
            serviceTaxPercentage.setCountryMaster(AuthService.getCurrentUser().getSelectedUserLocation().getCountryMaster());
            serviceTaxPercentage.setCompanyCode(AuthService.getCurrentUser().getSelectedUserLocation().getCompanyMaster().getCompanyCode());
            serviceTaxPercentage.setCountryCode(AuthService.getCurrentUser().getSelectedUserLocation().getCountryMaster().getCountryCode());
            serviceTaxPercentageValidator.validate(serviceTaxPercentage);
        }

    }


}
