package com.efreightsuite.service;

import com.efreightsuite.common.component.CacheRepository;
import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.SearchRequest;
import com.efreightsuite.dto.StateSearchDto;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.exception.UniqueKey;
import com.efreightsuite.model.StateMaster;
import com.efreightsuite.repository.StateMasterRepository;
import com.efreightsuite.search.StateSearchImpl;
import com.efreightsuite.util.AppUtil;
import com.efreightsuite.util.SaaSUtil;
import com.efreightsuite.util.ValidateUtil;
import com.efreightsuite.validation.StateMasterValidator;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.exception.ExceptionUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.ObjectOptimisticLockingFailureException;
import org.springframework.orm.jpa.JpaObjectRetrievalFailureException;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class StateMasterService {


    @Autowired
    private
    AppUtil appUtil;

    @Autowired
    private
    StateMasterRepository stateMasterRepository;

    @Autowired
    private
    CacheRepository cacheRepository;

    @Autowired
    private
    StateMasterValidator stateMasterValidator;

    @Autowired
    private
    StateSearchImpl stateSearchImpl;


    public BaseDto search(SearchRequest searchRequest, Long countryId) {

        log.info("Search method is called..");

        BaseDto baseDto = new BaseDto();

        try {


            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(stateSearchImpl.search(searchRequest, countryId));

            log.info("Search method is executed successfully..");

        } catch (Exception exception) {

            log.error("Exception in Search method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);

    }

    public BaseDto search(SearchRequest searchRequest, String countryCode) {

        log.info("Search method is called..");

        BaseDto baseDto = new BaseDto();

        try {


            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(stateSearchImpl.search(searchRequest, countryCode));

            log.info("Search method is executed successfully..");

        } catch (Exception exception) {

            log.error("Exception in Search method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);

    }

    public BaseDto search(StateSearchDto searchDto) {

        log.info("Search method is called..");

        BaseDto baseDto = new BaseDto();

        try {


            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(stateSearchImpl.search(searchDto));

            log.info("Search method is executed successfully..");

        } catch (Exception exception) {

            log.error("Exception in Search method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);

    }


    public BaseDto get(Long id) {

        log.info("get method is called [" + id + "]");

        BaseDto baseDto = new BaseDto();

        try {

            StateMaster stateMaster = cacheRepository.get(SaaSUtil.getSaaSId(), StateMaster.class.getName(), id, stateMasterRepository);

            ValidateUtil.notNull(stateMaster, ErrorCode.STATE_ID_NOT_FOUND);

            log.info("Successfully getting State by id.....[" + id + "]");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(stateMaster);


        } catch (RestException exception) {

            log.error("Exception in get method ", exception);

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            baseDto.setResponseCode(exception.getMessage());
        } catch (Exception e) {

            log.error("Exception in get method", e);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);
    }


    public BaseDto saveOrUpdate(StateMaster stateMaster) {


        log.info("StateMasterService -> saveOrUpdate method is called.....");

        BaseDto baseDto = new BaseDto();

        try {

            stateMasterValidator.validate(stateMaster);


            stateMaster.setCountryCode(stateMaster.getCountryMaster().getCountryCode());


            if (stateMaster.getId() != null) {
                stateMasterRepository.getOne(stateMaster.getId());
            }

            stateMasterRepository.save(stateMaster);

            cacheRepository.remove(SaaSUtil.getSaaSId(), StateMaster.class.getName(), stateMaster.getId());

            log.info("State Master Saved successfully  :  [" + stateMaster.getId() + "]");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(ErrorCode.SUCCESS);

        } catch (RestException exception) {

            log.error("Exception in StateMasterService -> saveOrUpdate method ", exception);

            baseDto.setResponseCode(exception.getMessage());

        } catch (ObjectOptimisticLockingFailureException e) {

            log.error("Exception in StateMasterService -> saveOrUpdate method ", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_LOCKED_RECORD);

        } catch (JpaObjectRetrievalFailureException e) {

            log.error("Exception in StateMasterService -> saveOrUpdate method ", e);

            baseDto.setResponseCode(ErrorCode.CANNOT_UPDATE_DELETED_RECORD);

        } catch (Exception exception) {

            log.error("Exception in StateMasterService -> saveOrUpdate method ", exception);

            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Exception Cause 1 ::: " + exceptionCause1);

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            if (exceptionCause1.contains("FK_")) {
                baseDto.setResponseCode(ErrorCode.SAVE_UPDATE_FK_RECORD);
            }

            if (exceptionCause1.contains(UniqueKey.UK_STATE_STATECODE)) {
                baseDto.setResponseCode(ErrorCode.STATE_CODE_ALREADY_EXIST);
            }

            if (exceptionCause1.contains(UniqueKey.UK_STATE_STATENAME)) {
                baseDto.setResponseCode(ErrorCode.STATE_NAME_ALREADY_EXIST);
            }

        }
        return appUtil.setDesc(baseDto);

    }


    public BaseDto delete(Long id) {

        log.info("Delete method is Invoked...[" + id + "]");

        BaseDto baseDto = new BaseDto();

        try {

            cacheRepository.delete(SaaSUtil.getSaaSId(), StateMaster.class.getName(), id, stateMasterRepository);

            log.info("StateMaster Deleted Successfully....[" + id + "]");

            baseDto.setResponseCode(ErrorCode.SUCCESS);

        } catch (Exception exception) {

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

            log.error("Exception in CityMasterService -> delete method ", exception);

            String rootCause = ExceptionUtils.getRootCauseMessage(exception);

            log.error("Root Cause of Exception : " + rootCause);

            if (rootCause.contains("FK_")) {
                baseDto.setResponseCode(ErrorCode.CANNOT_DELETE_FK_RECORD);
            }
        }

        return appUtil.setDesc(baseDto);
    }
}
