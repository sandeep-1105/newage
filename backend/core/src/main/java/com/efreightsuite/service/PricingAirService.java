package com.efreightsuite.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.FetchQuotationChargeSearchDto;
import com.efreightsuite.dto.FileUploadDto;
import com.efreightsuite.dto.FromToCurrency;
import com.efreightsuite.dto.PricingAirSearchDto;
import com.efreightsuite.dto.PricingMasterListDto;
import com.efreightsuite.dto.SearchRequest;
import com.efreightsuite.enumeration.ChargeType;
import com.efreightsuite.enumeration.PPCC;
import com.efreightsuite.enumeration.SurchargeType;
import com.efreightsuite.enumeration.YesNo;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.exception.UniqueKey;
import com.efreightsuite.model.EnquiryLog;
import com.efreightsuite.model.PricingAttachment;
import com.efreightsuite.model.PricingCarrier;
import com.efreightsuite.model.PricingMaster;
import com.efreightsuite.model.PricingPortPair;
import com.efreightsuite.model.QuotationCharge;
import com.efreightsuite.model.Shipment;
import com.efreightsuite.model.ShipmentCharge;
import com.efreightsuite.model.ShipmentServiceDetail;
import com.efreightsuite.model.SurchargeMapping;
import com.efreightsuite.repository.CarrierMasterRepository;
import com.efreightsuite.repository.ChargeMasterRepository;
import com.efreightsuite.repository.CurrencyMasterRepository;
import com.efreightsuite.repository.DefaultMasterDataRepository;
import com.efreightsuite.repository.EnquiryLogRepository;
import com.efreightsuite.repository.PortMasterRepository;
import com.efreightsuite.repository.PricingAirAttachmentRepository;
import com.efreightsuite.repository.PricingMasterRepository;
import com.efreightsuite.repository.PricingPortPairRepository;
import com.efreightsuite.repository.SurchargeMappingRepository;
import com.efreightsuite.repository.UnitMasterRepository;
import com.efreightsuite.search.CurrencyRateSearchImpl;
import com.efreightsuite.search.PricingAirSearchImpl;
import com.efreightsuite.util.AppUtil;
import com.efreightsuite.util.TimeUtil;
import com.efreightsuite.validation.PricingAirValidator;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.exception.ExceptionUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Log4j2
@Service
public class PricingAirService {

    @Autowired
    private
    AppUtil appUtil;

    @Autowired
    private
    PricingMasterRepository pricingMasterRepository;

    @Autowired
    private
    PricingPortPairRepository pricingPortPairRepository;


    @Autowired
    private
    PricingAirSearchImpl pricingAirSearchImpl;

    @Autowired
    private
    PricingAirValidator pricingAirValidator;

    @Autowired
    private
    PricingAirAttachmentRepository pricingAirAttachmentRepository;

    @Autowired
    private
    EnquiryLogRepository enquiryLogRepository;

    @Autowired
    PortMasterRepository portMasterRepository;

    @Autowired
    ChargeMasterRepository chargeMasterRepository;

    @Autowired
    CarrierMasterRepository carrierMasterRepository;

    @Autowired
    UnitMasterRepository unitMasterRepository;

    @Autowired
    CurrencyMasterRepository currencyMasterRepository;

    @Autowired
    private
    FileUploadService fileUploadService;

    @Autowired
    private
    CurrencyRateSearchImpl currencyRateSearchImpl;

    @Autowired
    DefaultMasterDataRepository defaultMasterDataRepository;

    @Autowired
    private
    SurchargeMappingRepository surchargeMappingRepository;


    public BaseDto search(PricingAirSearchDto searchDto) {

        log.info("Search method is callqd...." + searchDto);

        BaseDto baseDto = new BaseDto();

        try {

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(pricingAirSearchImpl.search(searchDto));

            log.info("Successfully Searching PricingAir...");

        } catch (Exception exception) {

            log.error("Exception in Search method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);

    }

//	which is used for default charge search

    public BaseDto defaultChargeSearch(PricingAirSearchDto searchDto) {
        log.info("Default charge Search method is callqd...pricing air" + searchDto);

        BaseDto baseDto = new BaseDto();

        try {

            baseDto.setResponseCode(ErrorCode.SUCCESS);

            baseDto.setResponseObject(pricingAirSearchImpl.defaultChargeSearch(searchDto));

            log.info("Successfully Searching PricingAir...");

        } catch (Exception exception) {

            log.error("Exception in Search method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);
    }

    private void afterSet(PricingMaster PricingMaster) {
        AppUtil.setPartyToNull(PricingMaster.getLocationMaster().getPartyMaster());
    }

    public BaseDto create(PricingMaster pricingMaster) {
        log.info("Create method is start.");
        BaseDto baseDto = new BaseDto();

        try {
            Long id = pricingAirSearchImpl.processCreate(pricingMaster);
            baseDto.setResponseCode(ErrorCode.SUCCESS);
            baseDto.setResponseObject(getById(id));
        } catch (RestException re) {
            log.error("Exception in create ", re);
            baseDto.setResponseCode(re.getMessage());

        } catch (Exception e) {
            String exceptionCause = ExceptionUtils.getRootCauseMessage(e);
            log.error("Exception ", e);
            log.info("Exception Cause ::: " + exceptionCause);
            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);

        }

        log.info("Create method is end.");

        return appUtil.setDesc(baseDto);
    }

    public BaseDto update(PricingMaster pricingMaster) {

        log.info("Update method is start.");
        BaseDto baseDto = new BaseDto();
        try {
            Long id = pricingAirSearchImpl.processUpdate(pricingMaster);
            log.info("PricngAir update successfully.....");

            baseDto.setResponseCode(ErrorCode.SUCCESS);
            baseDto.setResponseObject(getById(id));

        } catch (RestException re) {

            log.error("Exception in update ", re);

            baseDto.setResponseCode(re.getMessage());

        } catch (Exception e) {

            String exceptionCause = ExceptionUtils.getRootCauseMessage(e);

            log.error("Exception ", e);

            log.info("Exception Cause ::: " + exceptionCause);

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);


        }

        log.info("update method is end.");

        return appUtil.setDesc(baseDto);
    }

    public BaseDto getById(Long id) {

        log.info("PricingAirService -> getById method is called....");

        BaseDto baseDto = new BaseDto();
        try {

            PricingMaster pricingMaster = pricingMasterRepository.findById(id);
            afterSet(pricingMaster);
            if (pricingMaster != null && pricingMaster.getAttachmentList() != null
                    && !pricingMaster.getAttachmentList().isEmpty()) {
                for (PricingAttachment attachment : pricingMaster.getAttachmentList()) {
                    attachment.setFile(null);
                }
            }

            baseDto.setResponseCode(ErrorCode.SUCCESS);
            baseDto.setResponseObject(pricingMaster);

        } catch (RestException re) {
            log.error("Exception in PricingAirService -> getById method ", re);
            baseDto.setResponseCode(re.getMessage());

        } catch (Exception e) {
            log.error("Exception in PricingAirService -> getById method ", e);
            baseDto.setResponseCode(ErrorCode.FAILED);

        }
        return appUtil.setDesc(baseDto);
    }

    //	Pricing port pair view for using default charges
    public BaseDto getPricingPort(Long id) {
        log.info("PricingAirService -> getByPricing port method is called....");

        BaseDto baseDto = new BaseDto();
        try {

            PricingPortPair pricingPortPair = pricingPortPairRepository.findOne(id);

            baseDto.setResponseCode(ErrorCode.SUCCESS);
            baseDto.setResponseObject(pricingPortPair);

        } catch (RestException re) {
            log.error("Exception in PricingAirService -> getByPricingPOrtId method ", re);
            baseDto.setResponseCode(re.getMessage());

        } catch (Exception e) {
            log.error("Exception in PricingAirService -> getByPricingPOrtId method ", e);
            baseDto.setResponseCode(ErrorCode.FAILED);

        }
        return appUtil.setDesc(baseDto);
    }

    public BaseDto upload(FileUploadDto fileUploadDto) {

        BaseDto baseDto = new BaseDto();

        try {
            pricingAirValidator.validatePricingAirUpload(fileUploadDto);
            List<PricingMaster> list = fileUploadService.getAirPricing(baseDto, fileUploadDto.getFile(),
                    fileUploadDto.getPricingMasterId());
            for (PricingMaster pp : list) {
                pricingAirSearchImpl.processUpdate(pp);
            }
            pricingMasterRepository.save(list);
            baseDto.setResponseCode(ErrorCode.SUCCESS);
            baseDto.setResponseObject(list);

        } catch (RestException exception) {
            log.error("Exception occured in upload method ", exception);
            baseDto.setResponseCode(exception.getMessage());
        } catch (Exception e) {

            String exceptionCause = ExceptionUtils.getRootCauseMessage(e);

            log.error("Exception ", e);

            log.info("Exception Cause ::: " + exceptionCause);

            baseDto.setResponseCode(ErrorCode.ERROR_GENERIC);


        }
        return appUtil.setDesc(baseDto);
    }

    public void download(Long id, HttpServletResponse response) {
        fileUploadService.download(id, response);
    }

    public void downloadAttachment(Long attachmentId, HttpServletResponse response) {

        log.info("downloadAttachment is called : " + attachmentId);

        PricingAttachment attachment = pricingAirAttachmentRepository.getOne(attachmentId);

        if (attachment != null && attachment.getFile() != null && attachment.getFileName() != null) {

            try {

                response.setContentType(attachment.getFileContentType());

                response.setHeader("Content-Disposition", "attachment; filename= " + attachment.getFileName());

                response.setContentLength(attachment.getFile().length);

                ServletOutputStream servletOutputStream = response.getOutputStream();

                servletOutputStream.write(attachment.getFile());

                servletOutputStream.flush();

                servletOutputStream.close();

            } catch (Exception exception) {
                log.error("Error occured during download attachment file : " + attachmentId + ", ", exception);
            }

        } else {
            log.info("Attachment is not present with the id : " + attachmentId);
        }

    }

    public BaseDto findByOriginAndDestination(Long originId, Long destinationId, Long enquiryId) {

        BaseDto baseDto = new BaseDto();

        try {
            List<QuotationCharge> quotationChargeList = new ArrayList<>();

            EnquiryLog enquiryLog = enquiryLogRepository.findById(enquiryId);

            PricingMaster pricingMaster = pricingMasterRepository.findAllByOriginIdAndDestinationId(originId,
                    destinationId);

            if (pricingMaster != null) {

                if (enquiryLog != null && enquiryLog.getEnquiryDetailList() != null
                        && !enquiryLog.getEnquiryDetailList().isEmpty()
                        && enquiryLog.getEnquiryDetailList().get(0) != null
                        && enquiryLog.getEnquiryDetailList().get(0).getId() != null) {
                    List<PricingPortPair> hazardousOriginPriceList = null;
                    List<PricingPortPair> hazardousDestinationPriceList = null;
                    List<PricingCarrier> hazardousFreightPriceList = null;

                    if (enquiryLog.getEnquiryDetailList().get(0).getHazardous() != null
                            && enquiryLog.getEnquiryDetailList().get(0).getHazardous().equals(YesNo.Yes)) {
                        hazardousOriginPriceList = pricingMaster.getHazardousOriginPriceList();
                        hazardousDestinationPriceList = pricingMaster.getHazardousDestinationPriceList();
                        hazardousFreightPriceList = pricingMaster.getHazardousFreightPriceList();
                    } else {
                        hazardousOriginPriceList = pricingMaster.getGeneralOriginPriceList();
                        hazardousDestinationPriceList = pricingMaster.getGeneralDestinationPriceList();
                        hazardousFreightPriceList = pricingMaster.getGeneralFreightPriceList();
                    }
                    QuotationCharge quotationCharge = null;
                    for (PricingPortPair pair : hazardousOriginPriceList) {
                        quotationCharge = new QuotationCharge();
                        quotationCharge.setChargeMaster(pair.getChargeMaster());
                        quotationCharge.setChargeCode(pair.getChargeCode());
                        if (pair.getChargeMaster() != null) {
                            quotationCharge.setChargeName(pair.getChargeMaster().getChargeName());
                        }
                        quotationCharge.setCurrencyMaster(pair.getCurrencyMaster());
                        quotationCharge.setCurrencyCode(pair.getCurrencyCode());
                        quotationCharge.setUnitMaster(pair.getUnitMaster());
                        quotationCharge.setUnitCode(pair.getUnitCode());
                        quotationCharge.setSellRateMinAmount(pair.getStdSelInMinimum());
                        quotationCharge.setSellRateAmountPerUnit(pair.getStdSelInAmount());
                        quotationCharge.setBuyRateCostPerUnit(pair.getCostInAmount());
                        quotationCharge.setBuyRateMinCost(pair.getCostInMinimum());
                        quotationChargeList.add(quotationCharge);
                    }

                    for (PricingPortPair pair : hazardousDestinationPriceList) {
                        quotationCharge = new QuotationCharge();
                        quotationChargeList.add(quotationCharge);
                    }

                    for (PricingCarrier pair : hazardousFreightPriceList) {
                        quotationCharge = new QuotationCharge();
                        quotationCharge.setChargeMaster(pair.getChargeMaster());
                        quotationCharge.setChargeCode(pair.getChargeCode());
                        if (pair.getChargeMaster() != null) {
                            quotationCharge.setChargeName(pair.getChargeMaster().getChargeName());
                        }
                        quotationCharge.setCurrencyMaster(pair.getCurrencyMaster());
                        quotationCharge.setCurrencyCode(pair.getCurrencyCode());
                        quotationCharge.setUnitMaster(pair.getUnitMaster());
                        quotationCharge.setUnitCode(pair.getUnitCode());
                        /*
						 * quotationCharge.setSellRateMinAmount(pair.
						 * getStdSelInMinimum());
						 * quotationCharge.setSellRateAmountPerUnit(pair.
						 * getStdSelInAmount());
						 * 
						 * quotationCharge.setBuyRateCostPerUnit(pair.
						 * getCostInAmount());
						 * quotationCharge.setBuyRateMinCost(pair.
						 * getCostInMinimum());
						 */

                        quotationChargeList.add(quotationCharge);
                    }

                }

            } else {
                log.info("NO Pricing master Data found for the Combination of Orgin id :" + originId
                        + " ; Destination id :" + destinationId);
            }
            baseDto.setResponseObject(quotationChargeList);

        } catch (Exception exception) {

            log.error("Exception on findByOriginAndDestination", exception);

        }

        return baseDto;
    }

    public BaseDto fetchQuotationCharges(FetchQuotationChargeSearchDto data) {

        log.info("PricingAirService -> getById method is called....");

        BaseDto baseDto = new BaseDto();
        try {
            PricingMaster pricingMaster = pricingMasterRepository.getByBasedIdOriginAndDestAndRoutedAndTrasitIsNullAndTransportMode(
                    data.getOriginId(), data.getDestinationId(), data.getWhoRouted(), data.getTransportMode());

            List<QuotationCharge> quotationChargeList = new ArrayList<>();
            if (pricingMaster != null && pricingMaster.getId() != null) {
                if (pricingMaster.getGeneralOriginPriceList() != null
                        && !pricingMaster.getGeneralOriginPriceList().isEmpty()) {

                    List<Long> ids = new ArrayList<>();

                    for (PricingPortPair pricingData : pricingMaster.getGeneralOriginPriceList()) {

                        if (ids.contains(pricingData.getChargeMaster().getId())) {
                            continue;
                        }

                        if (TimeUtil.validateFromAndToDateWithNow(pricingData.getValidFromDate(),
                                pricingData.getValidToDate())) {
                            ids.add(pricingData.getChargeMaster().getId());
                            quotationChargeList.add(copyCharge(pricingData));
                        }
                    }
                }

                if (pricingMaster.getGeneralDestinationPriceList() != null
                        && !pricingMaster.getGeneralDestinationPriceList().isEmpty()) {

                    List<Long> ids = new ArrayList<>();

                    for (PricingPortPair pricingData : pricingMaster.getGeneralDestinationPriceList()) {

                        if (ids.contains(pricingData.getChargeMaster().getId())) {
                            continue;
                        }

                        if (TimeUtil.validateFromAndToDateWithNow(pricingData.getValidFromDate(),
                                pricingData.getValidToDate())) {
                            ids.add(pricingData.getChargeMaster().getId());
                            quotationChargeList.add(copyCharge(pricingData));
                        }
                    }
                }

                if (data.getCarrierId() != null) {

                    boolean carrier = false;

                    List<PricingCarrier> carrierPricingList = pricingMasterRepository
                            .findAllByOriginIdAndDestinationIdAndCarrierId(data.getOriginId(),
                                    data.getDestinationId(), data.getCarrierId());
                    if (carrierPricingList != null && !carrierPricingList.isEmpty()) {
                        for (PricingCarrier pc : carrierPricingList) {

                            if (pc.getCarrierMaster() != null && pc.getCarrierMaster().getId() != null
                                    && pc.getCarrierMaster().getId().equals(data.getCarrierId())) {

                                if (!carrier && TimeUtil.validateFromAndToDateWithNow(pc.getValidFrom(),
                                        pc.getValidTo())) {

                                    quotationChargeList.add(copyCarrierCharge(pc, data.getChargable()));
                                    carrier = true;


                                    if (pc.getFuelSurcharge() != null && pc.getFuelValidFrom() != null && pc.getFuelValidTo() != null) {

                                        if (TimeUtil.validateFromAndToDateWithNow(pc.getFuelValidFrom(),
                                                pc.getFuelValidTo())) {
                                            QuotationCharge tmpCharge = getFuelSurcharge(pc, data.getChargable());
                                            if (tmpCharge != null) {
                                                quotationChargeList.add(tmpCharge);
                                            }
                                        }

                                    } else if (pc.getFuelSurcharge() != null && pc.getFuelValidFrom() == null && pc.getFuelValidTo() == null) {

                                        QuotationCharge tmpCharge = getFuelSurcharge(pc, data.getChargable());
                                        if (tmpCharge != null) {
                                            quotationChargeList.add(tmpCharge);
                                        }
                                    }

                                    if (pc.getSecuritySurcharge() != null && pc.getSecurityChargeValidFrom() != null && pc.getSecurityChargeValidTo() != null) {

                                        if (TimeUtil.validateFromAndToDateWithNow(pc.getSecurityChargeValidFrom(),
                                                pc.getSecurityChargeValidTo())) {
                                            QuotationCharge tmpCharge = getSecuritySurcharge(pc, data.getChargable());
                                            if (tmpCharge != null) {
                                                quotationChargeList.add(tmpCharge);
                                            }
                                        }
                                    } else if (pc.getSecuritySurcharge() != null && pc.getSecurityChargeValidFrom() == null && pc.getSecurityChargeValidTo() == null) {
                                        QuotationCharge tmpCharge = getSecuritySurcharge(pc, data.getChargable());
                                        if (tmpCharge != null) {
                                            quotationChargeList.add(tmpCharge);
                                        }
                                    }

                                }

                            }


                            if (carrier) {
                                break;
                            }

                        }
                    }
                }


                baseDto.setResponseObject(quotationChargeList);
                baseDto.setResponseCode(ErrorCode.SUCCESS);
            } else {
                baseDto.setResponseCode(ErrorCode.SUCCESS);
            }

        } catch (RestException re) {
            log.error("Exception in PricingAirService -> getById method ", re);
            baseDto.setResponseCode(re.getMessage());

        } catch (Exception e) {
            log.error("Exception in PricingAirService -> getById method ", e);
            baseDto.setResponseCode(ErrorCode.FAILED);

        }
        return appUtil.setDesc(baseDto);
    }

    private QuotationCharge copyCharge(PricingPortPair pricingData) {

        Map<Long, FromToCurrency> map = (Map<Long, FromToCurrency>) currencyRateSearchImpl.search(
                AuthService.getCurrentUser().getSelectedUserLocation().getCountryMaster().getCurrencyMaster().getId())
                .getSearchResult();

        QuotationCharge quotationCharge = new QuotationCharge();
        if (pricingData.getChargeMaster() != null && pricingData.getChargeMaster().getId() != null) {
            quotationCharge.setChargeMaster(pricingData.getChargeMaster());
            quotationCharge.setChargeCode(pricingData.getChargeMaster().getChargeCode());
            quotationCharge.setChargeName(pricingData.getChargeMaster().getChargeName());
        }
        if (pricingData.getCurrencyMaster() != null && pricingData.getCurrencyMaster().getId() != null) {
            quotationCharge.setCurrencyMaster(pricingData.getCurrencyMaster());
            quotationCharge.setCurrencyCode(pricingData.getCurrencyMaster().getCurrencyCode());
        }
        if (pricingData.getUnitMaster() != null && pricingData.getUnitMaster().getId() != null) {
            quotationCharge.setUnitMaster(pricingData.getUnitMaster());
            quotationCharge.setUnitCode(pricingData.getUnitMaster().getUnitCode());
        }

        if (map.get(quotationCharge.getCurrencyMaster().getId()) != null) {
            quotationCharge.setRoe(map.get(quotationCharge.getCurrencyMaster().getId()).getCSell());
        }

        quotationCharge.setSellRateMinAmount(pricingData.getStdSelInMinimum());
        quotationCharge.setSellRateAmountPerUnit(pricingData.getStdSelInAmount());
        quotationCharge.setBuyRateCostPerUnit(pricingData.getCostInAmount());
        quotationCharge.setBuyRateMinCost(pricingData.getCostInMinimum());
        quotationCharge.setPricingPortPairId(pricingData.getId());

        return quotationCharge;
    }

    private QuotationCharge copyCarrierCharge(PricingCarrier pricingData, double chargable) {
        Map<Long, FromToCurrency> map = (Map<Long, FromToCurrency>) currencyRateSearchImpl.search(
                AuthService.getCurrentUser().getSelectedUserLocation().getCountryMaster().getCurrencyMaster().getId())
                .getSearchResult();

        QuotationCharge quotationCharge = new QuotationCharge();

        if (pricingData.getChargeMaster() != null && pricingData.getChargeMaster().getId() != null) {
            quotationCharge.setChargeMaster(pricingData.getChargeMaster());
            quotationCharge.setChargeCode(pricingData.getChargeMaster().getChargeCode());
            quotationCharge.setChargeName(pricingData.getChargeMaster().getChargeName());
        }
        if (pricingData.getCurrencyMaster() != null && pricingData.getCurrencyMaster().getId() != null) {
            quotationCharge.setCurrencyMaster(pricingData.getCurrencyMaster());
            quotationCharge.setCurrencyCode(pricingData.getCurrencyMaster().getCurrencyCode());
        }
        if (pricingData.getUnitMaster() != null && pricingData.getUnitMaster().getId() != null) {
            quotationCharge.setUnitMaster(pricingData.getUnitMaster());
            quotationCharge.setUnitCode(pricingData.getUnitMaster().getUnitCode());
        }
        if (map.get(quotationCharge.getCurrencyMaster().getId()) != null) {
            quotationCharge.setRoe(map.get(quotationCharge.getCurrencyMaster().getId()).getCSell());
        }

        if (pricingData.getCarrierMaster().getIsPlus300() != null
                && pricingData.getCarrierMaster().getIsPlus300() == YesNo.Yes) {

            if (chargable > 1000) {

                if (pricingData.getStandardSellPrice() != null) {
                    quotationCharge.setSellRateMinAmount(pricingData.getStandardSellPrice().getMinimum());
                    quotationCharge.setSellRateAmountPerUnit(pricingData.getStandardSellPrice().getPlus1000());
                    quotationCharge.setNumberOfUnit("+1000");
                }

                if (pricingData.getCost() != null) {
                    quotationCharge.setBuyRateCostPerUnit(pricingData.getCost().getPlus1000());
                    quotationCharge.setBuyRateMinCost(pricingData.getCost().getMinimum());
                }

            } else if (chargable > 500 && chargable <= 1000) {
                if (pricingData.getStandardSellPrice() != null) {
                    quotationCharge.setSellRateMinAmount(pricingData.getStandardSellPrice().getMinimum());
                    quotationCharge.setSellRateAmountPerUnit(pricingData.getStandardSellPrice().getPlus500());
                    quotationCharge.setNumberOfUnit("+500");
                }

                if (pricingData.getCost() != null) {
                    quotationCharge.setBuyRateCostPerUnit(pricingData.getCost().getPlus500());
                    quotationCharge.setBuyRateMinCost(pricingData.getCost().getMinimum());
                }
            } else if (chargable > 250 && chargable <= 500) {
                if (pricingData.getStandardSellPrice() != null) {
                    quotationCharge.setSellRateMinAmount(pricingData.getStandardSellPrice().getMinimum());
                    quotationCharge.setSellRateAmountPerUnit(pricingData.getStandardSellPrice().getPlus250());
                    quotationCharge.setNumberOfUnit("+250");
                }

                if (pricingData.getCost() != null) {
                    quotationCharge.setBuyRateCostPerUnit(pricingData.getCost().getPlus250());
                    quotationCharge.setBuyRateMinCost(pricingData.getCost().getMinimum());
                }
            } else if (chargable > 100 && chargable <= 250) {
                if (pricingData.getStandardSellPrice() != null) {
                    quotationCharge.setSellRateMinAmount(pricingData.getStandardSellPrice().getMinimum());
                    quotationCharge.setSellRateAmountPerUnit(pricingData.getStandardSellPrice().getPlus100());
                    quotationCharge.setNumberOfUnit("+100");
                }

                if (pricingData.getCost() != null) {
                    quotationCharge.setBuyRateCostPerUnit(pricingData.getCost().getPlus100());
                    quotationCharge.setBuyRateMinCost(pricingData.getCost().getMinimum());
                }
            } else if (chargable > 45 && chargable <= 100) {
                if (pricingData.getStandardSellPrice() != null) {
                    quotationCharge.setSellRateMinAmount(pricingData.getStandardSellPrice().getMinimum());
                    quotationCharge.setSellRateAmountPerUnit(pricingData.getStandardSellPrice().getPlus45());
                    quotationCharge.setNumberOfUnit("+45");
                }

                if (pricingData.getCost() != null) {
                    quotationCharge.setBuyRateCostPerUnit(pricingData.getCost().getPlus45());
                    quotationCharge.setBuyRateMinCost(pricingData.getCost().getMinimum());
                }
            } else {
                if (pricingData.getStandardSellPrice() != null) {
                    quotationCharge.setSellRateMinAmount(pricingData.getStandardSellPrice().getMinimum());
                    quotationCharge.setSellRateAmountPerUnit(pricingData.getStandardSellPrice().getMinus45());
                    quotationCharge.setNumberOfUnit("-45");
                }

                if (pricingData.getCost() != null) {
                    quotationCharge.setBuyRateCostPerUnit(pricingData.getCost().getMinus45());
                    quotationCharge.setBuyRateMinCost(pricingData.getCost().getMinimum());
                }
            }

        } else {


            if (chargable > 1000) {

                if (pricingData.getStandardSellPrice() != null) {
                    quotationCharge.setSellRateMinAmount(pricingData.getStandardSellPrice().getMinimum());
                    quotationCharge.setSellRateAmountPerUnit(pricingData.getStandardSellPrice().getPlus1000());
                    quotationCharge.setNumberOfUnit("+1000");
                }

                if (pricingData.getCost() != null) {
                    quotationCharge.setBuyRateCostPerUnit(pricingData.getCost().getPlus1000());
                    quotationCharge.setBuyRateMinCost(pricingData.getCost().getMinimum());
                }

            } else if (chargable > 500 && chargable <= 1000) {
                if (pricingData.getStandardSellPrice() != null) {
                    quotationCharge.setSellRateMinAmount(pricingData.getStandardSellPrice().getMinimum());
                    quotationCharge.setSellRateAmountPerUnit(pricingData.getStandardSellPrice().getPlus500());
                    quotationCharge.setNumberOfUnit("+500");
                }

                if (pricingData.getCost() != null) {
                    quotationCharge.setBuyRateCostPerUnit(pricingData.getCost().getPlus500());
                    quotationCharge.setBuyRateMinCost(pricingData.getCost().getMinimum());
                }
            } else if (chargable > 300 && chargable <= 500) {
                if (pricingData.getStandardSellPrice() != null) {
                    quotationCharge.setSellRateMinAmount(pricingData.getStandardSellPrice().getMinimum());
                    quotationCharge.setSellRateAmountPerUnit(pricingData.getStandardSellPrice().getPlus300());
                    quotationCharge.setNumberOfUnit("+300");
                }

                if (pricingData.getCost() != null) {
                    quotationCharge.setBuyRateCostPerUnit(pricingData.getCost().getPlus300());
                    quotationCharge.setBuyRateMinCost(pricingData.getCost().getMinimum());
                }
            } else if (chargable > 100 && chargable <= 300) {
                if (pricingData.getStandardSellPrice() != null) {
                    quotationCharge.setSellRateMinAmount(pricingData.getStandardSellPrice().getMinimum());
                    quotationCharge.setSellRateAmountPerUnit(pricingData.getStandardSellPrice().getPlus100());
                    quotationCharge.setNumberOfUnit("+100");
                }

                if (pricingData.getCost() != null) {
                    quotationCharge.setBuyRateCostPerUnit(pricingData.getCost().getPlus100());
                    quotationCharge.setBuyRateMinCost(pricingData.getCost().getMinimum());
                }
            } else if (chargable > 45 && chargable <= 100) {
                if (pricingData.getStandardSellPrice() != null) {
                    quotationCharge.setSellRateMinAmount(pricingData.getStandardSellPrice().getMinimum());
                    quotationCharge.setSellRateAmountPerUnit(pricingData.getStandardSellPrice().getPlus45());
                    quotationCharge.setNumberOfUnit("+45");
                }

                if (pricingData.getCost() != null) {
                    quotationCharge.setBuyRateCostPerUnit(pricingData.getCost().getPlus45());
                    quotationCharge.setBuyRateMinCost(pricingData.getCost().getMinimum());
                }
            } else {
                if (pricingData.getStandardSellPrice() != null) {
                    quotationCharge.setSellRateMinAmount(pricingData.getStandardSellPrice().getMinimum());
                    quotationCharge.setSellRateAmountPerUnit(pricingData.getStandardSellPrice().getMinus45());
                    quotationCharge.setNumberOfUnit("-45");
                }

                if (pricingData.getCost() != null) {
                    quotationCharge.setBuyRateCostPerUnit(pricingData.getCost().getMinus45());
                    quotationCharge.setBuyRateMinCost(pricingData.getCost().getMinimum());
                }
            }


        }

        quotationCharge.setPricingCarrierId(pricingData.getId());
        return quotationCharge;
    }

    private QuotationCharge getSecuritySurcharge(PricingCarrier pricingData, double chargable) {


        Map<Long, FromToCurrency> map = (Map<Long, FromToCurrency>) currencyRateSearchImpl.search(
                AuthService.getCurrentUser().getSelectedUserLocation().getCountryMaster().getCurrencyMaster().getId())
                .getSearchResult();

        SurchargeMapping surcharge = surchargeMappingRepository.getBySurchargeType(SurchargeType.Security);

        if (surcharge == null) {
            return null;
        }

        QuotationCharge quotationCharge = new QuotationCharge();

        if (pricingData.getChargeMaster() != null && pricingData.getChargeMaster().getId() != null) {
            quotationCharge.setChargeMaster(surcharge.getChargeMaster());
            quotationCharge.setChargeCode(surcharge.getChargeMaster().getChargeCode());
            quotationCharge.setChargeName(surcharge.getChargeMaster().getChargeName());
        }
        if (pricingData.getCurrencyMaster() != null && pricingData.getCurrencyMaster().getId() != null) {
            quotationCharge.setCurrencyMaster(pricingData.getCurrencyMaster());
            quotationCharge.setCurrencyCode(pricingData.getCurrencyMaster().getCurrencyCode());
        }
        if (pricingData.getUnitMaster() != null && pricingData.getUnitMaster().getId() != null) {
            quotationCharge.setUnitMaster(pricingData.getUnitMaster());
            quotationCharge.setUnitCode(pricingData.getUnitMaster().getUnitCode());
        }
        if (map.get(quotationCharge.getCurrencyMaster().getId()) != null) {
            quotationCharge.setRoe(map.get(quotationCharge.getCurrencyMaster().getId()).getCSell());
        }

        quotationCharge.setSellRateAmountPerUnit(pricingData.getSecuritySurcharge());
        quotationCharge.setPricingCarrierId(pricingData.getId());
        quotationCharge.setSurchargeType(surcharge.getSurchargeType());

        return quotationCharge;
    }

    private QuotationCharge getFuelSurcharge(PricingCarrier pricingData, double chargable) {


        Map<Long, FromToCurrency> map = (Map<Long, FromToCurrency>) currencyRateSearchImpl.search(
                AuthService.getCurrentUser().getSelectedUserLocation().getCountryMaster().getCurrencyMaster().getId())
                .getSearchResult();

        SurchargeMapping surcharge = surchargeMappingRepository.getBySurchargeType(SurchargeType.Fuel);

        if (surcharge == null) {
            return null;
        }

        QuotationCharge quotationCharge = new QuotationCharge();

        if (pricingData.getChargeMaster() != null && pricingData.getChargeMaster().getId() != null) {
            quotationCharge.setChargeMaster(surcharge.getChargeMaster());
            quotationCharge.setChargeCode(surcharge.getChargeMaster().getChargeCode());
            quotationCharge.setChargeName(surcharge.getChargeMaster().getChargeName());
        }
        if (pricingData.getCurrencyMaster() != null && pricingData.getCurrencyMaster().getId() != null) {
            quotationCharge.setCurrencyMaster(pricingData.getCurrencyMaster());
            quotationCharge.setCurrencyCode(pricingData.getCurrencyMaster().getCurrencyCode());
        }
        if (pricingData.getUnitMaster() != null && pricingData.getUnitMaster().getId() != null) {
            quotationCharge.setUnitMaster(pricingData.getUnitMaster());
            quotationCharge.setUnitCode(pricingData.getUnitMaster().getUnitCode());
        }
        if (map.get(quotationCharge.getCurrencyMaster().getId()) != null) {
            quotationCharge.setRoe(map.get(quotationCharge.getCurrencyMaster().getId()).getCSell());
        }

        quotationCharge.setSellRateAmountPerUnit(pricingData.getFuelSurcharge());
        quotationCharge.setPricingCarrierId(pricingData.getId());
        quotationCharge.setSurchargeType(surcharge.getSurchargeType());

        return quotationCharge;
    }

    private ShipmentCharge getFreightCharges(PricingCarrier pricingCarrier, String key, String chargeNumberOfUnit) {
        ShipmentCharge shipmentCharge = new ShipmentCharge();
        shipmentCharge.setChargeMaster(pricingCarrier.getChargeMaster());
        shipmentCharge.setChargeCode(pricingCarrier.getChargeCode());
        shipmentCharge.setChargeName(pricingCarrier.getChargeMaster().getChargeName());
        shipmentCharge.setUnitMaster(pricingCarrier.getUnitMaster());
        shipmentCharge.setUnitCode(pricingCarrier.getUnitCode());
        shipmentCharge.setNumberOfUnit(chargeNumberOfUnit);

        if (pricingCarrier.getChargeMaster().getChargeType().equals(ChargeType.Origin)) {
            shipmentCharge.setPpcc(PPCC.Prepaid);
        } else if (pricingCarrier.getChargeMaster().getChargeType().equals(ChargeType.Destination) || pricingCarrier.getChargeMaster().getChargeType().equals(ChargeType.Freight)) {
            shipmentCharge.setPpcc(PPCC.Collect);
        }

        if (key.equals("TRUE") || key == "TRUE") {
            shipmentCharge.setRateCurrency(pricingCarrier.getCurrencyMaster());
            shipmentCharge.setRateMinimum(pricingCarrier.getStandardSellPrice().getMinimum());
        } else {
            shipmentCharge.setGrossSaleCurrency(pricingCarrier.getCurrencyMaster());
            shipmentCharge.setGrossSaleMinimum(pricingCarrier.getStandardSellPrice().getMinimum());
        }

        return shipmentCharge;

    }

    private List<ShipmentCharge> getOriginAndDestinationCharges(PricingPortPair pricingPortPair, String key, List<ShipmentCharge> charges) {
        ShipmentCharge shipmentCharge = new ShipmentCharge();
        shipmentCharge.setChargeMaster(pricingPortPair.getChargeMaster());
        shipmentCharge.setChargeCode(pricingPortPair.getChargeCode());
        shipmentCharge.setChargeName(pricingPortPair.getChargeMaster().getChargeName());
        shipmentCharge.setUnitMaster(pricingPortPair.getUnitMaster());
        shipmentCharge.setUnitCode(pricingPortPair.getUnitCode());
        if (pricingPortPair.getChargeMaster().getChargeType().equals(ChargeType.Origin)) {
            shipmentCharge.setPpcc(PPCC.Prepaid);
        } else if (pricingPortPair.getChargeMaster().getChargeType().equals(ChargeType.Destination) || pricingPortPair.getChargeMaster().getChargeType().equals(ChargeType.Freight)) {
            shipmentCharge.setPpcc(PPCC.Collect);
        }
        if (key.equals("TRUE") || key == "TRUE") {
            shipmentCharge.setRateCurrency(pricingPortPair.getCurrencyMaster());
            shipmentCharge.setRateMinimum(pricingPortPair.getStdSelInMinimum());
            shipmentCharge.setRateAmount(pricingPortPair.getStdSelInAmount());
        } else {
            shipmentCharge.setGrossSaleCurrency(pricingPortPair.getCurrencyMaster());
            shipmentCharge.setGrossSaleMinimum(pricingPortPair.getStdSelInMinimum());
            shipmentCharge.setGrossSaleAmount(pricingPortPair.getStdSelInAmount());
        }
        shipmentCharge.setCostCurrency(pricingPortPair.getCurrencyMaster());
        shipmentCharge.setCostMinimum(pricingPortPair.getCostInMinimum());
        shipmentCharge.setCostAmount(pricingPortPair.getCostInAmount());
        charges.add(shipmentCharge);
        return charges;
    }


    public BaseDto prepareShipment(SearchRequest searchRequest) {
        BaseDto baseDto = new BaseDto();
        try {
            Long carrierId = Long.parseLong(searchRequest.getParam1());
            Long pricingId = Long.parseLong(searchRequest.getParam2());
            PricingMaster pricingMaster = new PricingMaster();

            if (pricingId != null) {
                pricingMaster = pricingMasterRepository.getOne(pricingId);
            }

            Date today = TimeUtil.getCurrentLocationTime(AuthService.getCurrentUser().getSelectedUserLocation());
            String key = appUtil.getLocationConfig("booking.rates.merged", pricingMaster.getLocationMaster(), true);

            Shipment shipment = new Shipment();
//		shipment.setWhoRouted(pricingMaster.getWhoRouted());
            ShipmentServiceDetail shipmentServiceDetail = new ShipmentServiceDetail();
            shipmentServiceDetail.setWhoRouted(pricingMaster.getWhoRouted());
            shipmentServiceDetail.setOrigin(pricingMaster.getOrigin());
            shipmentServiceDetail.setPol(pricingMaster.getOrigin());
            shipmentServiceDetail.setDestination(pricingMaster.getDestination());
            shipmentServiceDetail.setPod(pricingMaster.getDestination());
            List<ShipmentServiceDetail> shipmentServiceDetails = new ArrayList<>();
            shipmentServiceDetails.add(shipmentServiceDetail);
            shipment.setShipmentServiceList(shipmentServiceDetails);
            List<ShipmentCharge> charges = new ArrayList<>();

            // Getting the charges from pricingMaster GeneralFreightPriceList
            for (PricingCarrier pricingCarrier : pricingMaster.getGeneralFreightPriceList()) {
                if (pricingCarrier.getCarrierMaster().getId().equals(carrierId) && today.after(pricingCarrier.getValidFrom()) && today.before(pricingCarrier.getValidTo())) {

                    if (key.equals("TRUE") || key == "TRUE") {

                        if (pricingCarrier.getStandardSellPrice().getMinus45() != null) {
                            ShipmentCharge shipmentCharge = getFreightCharges(pricingCarrier, key, "-45");
                            shipmentCharge.setRateAmount(pricingCarrier.getStandardSellPrice().getMinus45());
                            charges.add(shipmentCharge);
                        }

                        if (pricingCarrier.getStandardSellPrice().getPlus45() != null) {
                            ShipmentCharge shipmentCharge = getFreightCharges(pricingCarrier, key, "+45");
                            shipmentCharge.setRateAmount(pricingCarrier.getStandardSellPrice().getPlus45());
                            charges.add(shipmentCharge);
                        }
                        if (pricingCarrier.getStandardSellPrice().getPlus100() != null) {
                            ShipmentCharge shipmentCharge = getFreightCharges(pricingCarrier, key, "+100");
                            shipmentCharge.setRateAmount(pricingCarrier.getStandardSellPrice().getPlus100());
                            charges.add(shipmentCharge);
                        }
                        if (pricingCarrier.getStandardSellPrice().getPlus250() != null) {
                            ShipmentCharge shipmentCharge = getFreightCharges(pricingCarrier, key, "+250");
                            shipmentCharge.setRateAmount(pricingCarrier.getStandardSellPrice().getPlus250());
                            charges.add(shipmentCharge);
                        }
                        if (pricingCarrier.getStandardSellPrice().getPlus300() != null) {
                            ShipmentCharge shipmentCharge = getFreightCharges(pricingCarrier, key, "+300");
                            shipmentCharge.setRateAmount(pricingCarrier.getStandardSellPrice().getPlus300());
                            charges.add(shipmentCharge);
                        }
                        if (pricingCarrier.getStandardSellPrice().getPlus500() != null) {
                            ShipmentCharge shipmentCharge = getFreightCharges(pricingCarrier, key, "+500");
                            shipmentCharge.setRateAmount(pricingCarrier.getStandardSellPrice().getPlus500());
                            charges.add(shipmentCharge);
                        }
                        if (pricingCarrier.getStandardSellPrice().getPlus1000() != null) {
                            ShipmentCharge shipmentCharge = getFreightCharges(pricingCarrier, key, "+1000");
                            shipmentCharge.setRateAmount(pricingCarrier.getStandardSellPrice().getPlus1000());
                            charges.add(shipmentCharge);
                        }

                    } else {

                        if (pricingCarrier.getStandardSellPrice().getMinus45() != null) {
                            ShipmentCharge shipmentCharge = getFreightCharges(pricingCarrier, key, "-45");
                            shipmentCharge.setGrossSaleAmount(pricingCarrier.getStandardSellPrice().getMinus45());
                            charges.add(shipmentCharge);
                        }

                        if (pricingCarrier.getStandardSellPrice().getPlus45() != null) {
                            ShipmentCharge shipmentCharge = getFreightCharges(pricingCarrier, key, "+45");
                            shipmentCharge.setGrossSaleAmount(pricingCarrier.getStandardSellPrice().getPlus45());
                            charges.add(shipmentCharge);
                        }
                        if (pricingCarrier.getStandardSellPrice().getPlus100() != null) {
                            ShipmentCharge shipmentCharge = getFreightCharges(pricingCarrier, key, "+100");
                            shipmentCharge.setGrossSaleAmount(pricingCarrier.getStandardSellPrice().getPlus100());
                            charges.add(shipmentCharge);
                        }
                        if (pricingCarrier.getStandardSellPrice().getPlus250() != null) {
                            ShipmentCharge shipmentCharge = getFreightCharges(pricingCarrier, key, "+250");
                            shipmentCharge.setGrossSaleAmount(pricingCarrier.getStandardSellPrice().getPlus250());
                            charges.add(shipmentCharge);
                        }
                        if (pricingCarrier.getStandardSellPrice().getPlus300() != null) {
                            ShipmentCharge shipmentCharge = getFreightCharges(pricingCarrier, key, "+300");
                            shipmentCharge.setGrossSaleAmount(pricingCarrier.getStandardSellPrice().getPlus300());
                            charges.add(shipmentCharge);
                        }
                        if (pricingCarrier.getStandardSellPrice().getPlus500() != null) {
                            ShipmentCharge shipmentCharge = getFreightCharges(pricingCarrier, key, "+500");
                            shipmentCharge.setGrossSaleAmount(pricingCarrier.getStandardSellPrice().getPlus500());
                            charges.add(shipmentCharge);
                        }
                        if (pricingCarrier.getStandardSellPrice().getPlus1000() != null) {
                            ShipmentCharge shipmentCharge = getFreightCharges(pricingCarrier, key, "+1000");
                            shipmentCharge.setGrossSaleAmount(pricingCarrier.getStandardSellPrice().getPlus1000());
                            charges.add(shipmentCharge);
                        }

                    }

                }
            }
            // Getting the charges from pricingMaster GeneralOriginPriceList
            for (PricingPortPair pricingPortPair : pricingMaster.getGeneralOriginPriceList()) {
                if (today.after(pricingPortPair.getValidFromDate()) && today.before(pricingPortPair.getValidToDate())) {
                    charges = getOriginAndDestinationCharges(pricingPortPair, key, charges);
                }
            }

            // Getting the charges from pricingMaster GeneralDestinationPriceList
            for (PricingPortPair pricingPortPair : pricingMaster.getGeneralDestinationPriceList()) {
                if (today.after(pricingPortPair.getValidFromDate()) && today.before(pricingPortPair.getValidToDate())) {
                    charges = getOriginAndDestinationCharges(pricingPortPair, key, charges);
                }
            }
            shipment.getShipmentServiceList().get(0).setShipmentChargeList(charges);
            baseDto.setResponseObject(shipment);
            baseDto.setResponseCode(ErrorCode.SUCCESS);
        } catch (RestException re) {
            log.error("Exception in PricingAirService -> prepareShipment method ", re);
            baseDto.setResponseCode(re.getMessage());

        } catch (Exception e) {
            log.error("Exception in PricingAirService -> prepareShipment method ", e);
            baseDto.setResponseCode(ErrorCode.FAILED);

        }
        return appUtil.setDesc(baseDto);
    }


    //Updating pricing porir which is used for default charges updating
    public BaseDto updatePricingPortPair(PricingPortPair pricingPortPair) {
        BaseDto bd = new BaseDto();

        try {
            PricingMaster pm = new PricingMaster();
            PricingMaster tpm = null;
            pm = pricingMasterRepository.findById(pricingPortPair.getPricingMaster().getId());
            if (pm != null) {
                pm.setOrigin(pricingPortPair.getPricingMaster().getOrigin());
                pm.setDestination(pricingPortPair.getPricingMaster().getDestination());
                pm.setTransit(pricingPortPair.getPricingMaster().getTransit());
                pm.setWhoRouted(pricingPortPair.getPricingMaster().getWhoRouted());
                pm.setId(pricingPortPair.getPricingMaster().getId());
                pricingPortPair.setPricingMaster(pm);
                tpm = pricingAirSearchImpl.getExistingChargeUpdate(pm, pricingPortPair);
                if ((tpm == null) || (tpm != null && Objects.equals(pm.getId(), tpm.getId()))) {
                    pricingPortPairRepository.save(pricingPortPair);
                } else {
                    throw new RestException(ErrorCode.UK_PRICING_ALREADY_EXIST);
                }
                bd.setResponseCode(ErrorCode.SUCCESS);
            } else {
                bd.setResponseCode(ErrorCode.FAILED);
            }
        } catch (RestException ex) {

            log.error("Exception in get method ", ex);

            bd.setResponseCode(ex.getMessage());

        } catch (Exception e) {

            log.error("Exception in get method ", e);

            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(e);

            bd.setResponseCode(ErrorCode.FAILED);

            if (exceptionCause1.contains(UniqueKey.UK_PRICE_CHARGE)) {
                bd.setResponseCode(ErrorCode.DEFAUT_CHARGE_DUPLICATE);
            }

        }

        return bd;
    }


    public BaseDto createList(PricingMasterListDto pricingMasterListDto) {

        BaseDto bd = new BaseDto();

        try {

            if (pricingMasterListDto != null) {
                pricingAirSearchImpl.createPricingObject(pricingMasterListDto);
                bd.setResponseCode(ErrorCode.SUCCESS);
            } else {
                throw new RestException(ErrorCode.INVALID_PARAMETER);
            }
            return bd;
        } catch (RestException exception) {
            log.error("BadRequestException occured with error code ", exception);
            bd.setResponseCode(exception.getMessage());
        } catch (Exception e) {

            String exceptionCause1 = ExceptionUtils.getRootCauseMessage(e);

            bd.setResponseCode(ErrorCode.FAILED);

            if (exceptionCause1.contains(UniqueKey.UK_PRICE_CHARGE)) {
                bd.setResponseCode(ErrorCode.DEFAUT_CHARGE_DUPLICATE);
            }

        }
        return bd;
    }


}
