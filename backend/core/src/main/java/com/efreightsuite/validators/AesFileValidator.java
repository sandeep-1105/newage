package com.efreightsuite.validators;

import java.util.Objects;

import com.efreightsuite.enumeration.TransportMode;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.model.AesCommodity;
import com.efreightsuite.model.AesFile;
import com.efreightsuite.model.AesUltimateConsignee;
import com.efreightsuite.model.DtdcVehicle;
import com.efreightsuite.model.StateMaster;
import com.efreightsuite.repository.DefaultMasterDataRepository;
import com.efreightsuite.service.RegularExpressionService;
import com.efreightsuite.util.RegExpName;
import com.efreightsuite.util.ValidateUtil;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class AesFileValidator {

    @Autowired
    DefaultMasterDataRepository defaultMasterDataRepository;

    @Autowired
    private
    RegularExpressionService regExpService;

    public void validateInfo(AesFile aesFile) {

        log.info("aes validation begins");

        ValidateUtil.notNullOrEmpty(aesFile.getServiceUid(), ErrorCode.SERVICE_UID_CANNOT_NULL);
        ValidateUtil.notNullOrEmpty(aesFile.getShipmentUid(), ErrorCode.SHIPMENT_UID_CANNOT_NULL);
        //ValidateUtil.notNullOrEmpty(aesFile.getMasterUid(), ErrorCode.MASTER_UID_CANNOT_NULL);
        ValidateUtil.notNullOrEmpty(aesFile.getHawbNo(), ErrorCode.HAWB_NO_MANDATORY);
        //ValidateUtil.notNullOrEmpty(aesFile.getHawbNo(), ErrorCode.MAWB_NO_MANDATORY);
        ValidateUtil.notNull(aesFile.getAesFilingType(), ErrorCode.AES_FILLING_TYPE_MANDATORY);

        ValidateUtil.notNull(aesFile.getAesTransportMode(), ErrorCode.AES_TRANSPORT_CANNOT_NULL);
        aesFile.setAesTransportModeName(aesFile.getAesTransportMode().getAesTransportName());

        ValidateUtil.isStatusBlocked(aesFile.getAesTransportMode().getStatus(), ErrorCode.AES_TRANSPORT_BLOCK);
        ValidateUtil.isStatusHidden(aesFile.getAesTransportMode().getStatus(), ErrorCode.AES_TRANSPORT_HIDE);
        ValidateUtil.notNull(aesFile.getAesAction(), ErrorCode.AES_ACTION_MANDATORY);

        verifyOrigin(aesFile);
        verifyPol(aesFile);
        verifyDestination(aesFile);
        verifyPod(aesFile);

        ValidateUtil.notNull(aesFile.getEtd(), ErrorCode.AES_ETD_MANDATORY);
        verifyCarrier(aesFile);
        if (aesFile.getCarrier() != null
                && aesFile.getCarrier().getId() != null
                && (Objects.equals(aesFile.getCarrier().getTransportMode(), TransportMode.Air)
                || aesFile.getCarrier().getTransportMode().equals(TransportMode.Air))) {
            ValidateUtil.notNull(aesFile.getIataOrScacCode(), ErrorCode.AES_IATA_CODE_MANDATORY);
            aesFile.setIataCode(aesFile.getIataOrScacCode());
        }
        ValidateUtil.notNull(aesFile.getRoutedTransaction(), ErrorCode.AES_ROUTED_CANNOT_NULL);

        ValidateUtil.notNull(aesFile.getUsspiUltimateConsignee(), ErrorCode.AES_ULTIMATE_CONSIGNEE_CANNOT_NULL);
        ValidateUtil.notNull(aesFile.getHazardousCargo(), ErrorCode.AES_HAZARDOUS_CARGO_MANDATORY);

        ValidateUtil.notNull(aesFile.getInbondType(), ErrorCode.AES_IN_BOND_MANDATORY);
        aesFile.setInbondTypeCode(aesFile.getInbondType().getInbondType());

        ValidateUtil.isStatusBlocked(aesFile.getInbondType().getStatus(), ErrorCode.AES_IN_BOND_BLOCK);
        ValidateUtil.isStatusHidden(aesFile.getInbondType().getStatus(), ErrorCode.AES_IN_BOND_HIDE);

        if (aesFile.getImportEntryNo() != null && aesFile.getImportEntryNo().trim().length() != 0) {
            regExpService.validate(aesFile.getImportEntryNo(), RegExpName.Reg_Exp_AES_ENTRY_NO, ErrorCode.AES_IMPORT_ENTRY_NO_INVALID);
            //ValidateUtil.checkPattern(aesFile.getImportEntryNo(), "importentryno", true);
            ValidateUtil.validateLength(aesFile.getImportEntryNo(), 1, 15, ErrorCode.AES_IMPORT_ENTRY_NO_INVALID);
        }


        if (aesFile.getCountry() != null) {
            aesFile.setCountryCode(aesFile.getCountry().getCountryCode());
        }

        if (aesFile.getCompanyMaster() != null) {
            aesFile.setCompanyCode(aesFile.getCompanyMaster().getCompanyCode());
        }

        if (aesFile.getLocation() != null) {
            aesFile.setLocationCode(aesFile.getLocation().getLocationCode());
        }

        if (aesFile.getScac() != null) {
            aesFile.setScacCode(aesFile.getScac().getScacCode());
        }
        log.info("aes validation ends");
    }

    public void verifyUppsi(AesFile aesFile) {

        ValidateUtil.notNull(aesFile.getAesUsppi(), ErrorCode.AES_USPPI_MANDATORY);
        ValidateUtil.notNull(aesFile.getAesUsppi().getShipper(), ErrorCode.AES_USPPI_SHIPPER_MANDATORY);
        ValidateUtil.notNullOrEmpty(aesFile.getAesUsppi().getShipper().getPartyCode(), ErrorCode.AES_USPPI_SHIPPER_MANDATORY);
        ValidateUtil.isStatusBlocked(aesFile.getAesUsppi().getShipper().getStatus(), ErrorCode.AES_USPPI_SHIPPER_BLOCK);
        ValidateUtil.isStatusHidden(aesFile.getAesUsppi().getShipper().getStatus(), ErrorCode.AES_USPPI_SHIPPER_HIDE);
        ValidateUtil.isDefaulter(aesFile.getAesUsppi().getShipper(), ErrorCode.AES_USPPI_SHIPPER_DEFAULTER);
        aesFile.getAesUsppi().setShipperCode(aesFile.getAesUsppi().getShipper().getPartyCode());

        //ValidateUtil.notNullOrEmpty(aesFile.getAesUsppi().getCargoOrigin().getPartyCode(), ErrorCode.AES_USPPI_CARGO_MANDATORY);
        //ValidateUtil.isStatusBlocked(aesFile.getAesUsppi().getCargoOrigin().getStatus(), ErrorCode.AES_USPPI_CARGO_BLOCK);
        //ValidateUtil.isStatusHidden(aesFile.getAesUsppi().getCargoOrigin().getStatus(), ErrorCode.AES_USPPI_CARGO_HIDE);

        if (aesFile.getAesUsppi().getCargoOrigin() != null) {
            aesFile.getAesUsppi().setCargoOriginCode(aesFile.getAesUsppi().getCargoOrigin().getPartyCode());
        }

        ValidateUtil.notNull(aesFile.getAesUsppi().getCargoOriginAddress(), ErrorCode.AES_USPPI_SHIPPER_ADDRESS_MANDATORY);
        ValidateUtil.notNull(aesFile.getAesUsppi().getContactNo(), ErrorCode.AES_USPPI_SHIPPER_CONTACT_NO_MANDATORY);

        ValidateUtil.notNull(aesFile.getAesUsppi().getCityMaster(), ErrorCode.AES_USPPI_SHIPPER_CITY_MANDATORY);
        aesFile.getAesUsppi().setCityCode(aesFile.getAesUsppi().getCityCode());

        ValidateUtil.notNull(aesFile.getAesUsppi().getStateMaster(), ErrorCode.AES_USPPI_SHIPPER_STATE_MANDATORY);
        aesFile.getAesUsppi().setStateCode(aesFile.getAesUsppi().getStateMaster().getStateCode());

        ValidateUtil.notNull(aesFile.getAesUsppi().getZipCode(), ErrorCode.AES_USPPI_SHIPPER_ZIP_MANDATORY);
        ValidateUtil.notNull(aesFile.getAesUsppi().getIdType(), ErrorCode.AES_USPPI_SHIPPER_ID_TYPE_MANDATORY);

        ValidateUtil.notNull(aesFile.getAesUsppi().getIdNo(), ErrorCode.AES_USPPI_SHIPPER_ID_NO_MANDATORY);


        //ValidateUtil.notNull(aesFile.getAesUsppi().getCountryMaster(), ErrorCode.AES_USPPI_COUNTRY);

        if (aesFile.getAesUsppi().getCountryMaster() != null && aesFile.getAesUsppi().getCountryMaster().getId() != null) {
            ValidateUtil.isStatusBlocked(aesFile.getAesUsppi().getCountryMaster().getStatus(), ErrorCode.AES_USPPI_COUNTRY_BLOCK);
            ValidateUtil.isStatusHidden(aesFile.getAesUsppi().getCountryMaster().getStatus(), ErrorCode.AES_USPPI_COUNTRY_HIDE);
            aesFile.getAesUsppi().setCountryCode(aesFile.getAesUsppi().getCountryMaster().getCountryCode());
        }
        if (aesFile.getAesUsppi().getIdNo() != null && aesFile.getAesUsppi().getIdNo().trim().length() != 0) {
            regExpService.validate(aesFile.getAesUsppi().getIdNo(), RegExpName.Reg_Exp_AES_ID_NO, ErrorCode.AES_CONSIGNEE_ID_NO_INVALID);
            //ValidateUtil.checkPattern(aesFile.getAesUsppi().getIdNo(), "ultimateShipperIdNo", true);
        }
        if (aesFile.getAesUsppi().getFirstName() != null && aesFile.getAesUsppi().getFirstName().trim().length() != 0) {
            regExpService.validate(aesFile.getAesUsppi().getFirstName(), RegExpName.Reg_Exp_AES_NAME, ErrorCode.AES_CONSIGNEE_FIRST_NAME_INVALID);
            //ValidateUtil.checkPattern(aesFile.getAesUsppi().getFirstName(), "ultimateShipperFrstName", true);
        }

        ValidateUtil.notNull(aesFile.getAesUsppi().getLastName(), ErrorCode.AES_USPPI_LAST_NAME_MANDATORY);
        if (aesFile.getAesUsppi().getLastName() != null && aesFile.getAesUsppi().getLastName().trim().length() != 0) {
            regExpService.validate(aesFile.getAesUsppi().getLastName(), RegExpName.Reg_Exp_AES_NAME, ErrorCode.AES_CONSIGNEE_SECOND_NAME_INVALID);
            //ValidateUtil.checkPattern(aesFile.getAesUsppi().getLastName(), "ultimateShipperSecondName", true);
        }
    }


    public void verifyCommodity(AesCommodity aesCommodity) {

        ValidateUtil.notNull(aesCommodity.getAesAction(), ErrorCode.AES_COMMODITY_ACTION_MANDATORY);
        ValidateUtil.notNull(aesCommodity.getOriginOfGoods(), ErrorCode.AES_COMMODITY_GOODS_MANDATORY);

        ValidateUtil.notNull(aesCommodity.getAesLicense(), ErrorCode.AES_COMMODITY_LICENSE_MANDATORY);
        ValidateUtil.isStatusBlocked(aesCommodity.getAesLicense().getStatus(), ErrorCode.AES_COMMODITY_LICENSE_BLOCK);
        ValidateUtil.isStatusHidden(aesCommodity.getAesLicense().getStatus(), ErrorCode.AES_COMMODITY_LICENSE_HIDE);
        aesCommodity.setAesLicenseCode(aesCommodity.getAesLicense().getLicenseCode());

        ValidateUtil.notNull(aesCommodity.getAesSchedule(), ErrorCode.AES_COMMODITY_SCHEDULAR_MANDATORY);
        ValidateUtil.notNull(aesCommodity.getAesSchedule().getId(), ErrorCode.AES_COMMODITY_SCHEDULAR_MANDATORY);
        ValidateUtil.isStatusBlocked(aesCommodity.getAesSchedule().getStatus(), ErrorCode.AES_SCHEDULAR_BLOCK);
        ValidateUtil.isStatusHidden(aesCommodity.getAesSchedule().getStatus(), ErrorCode.AES_SCHEDULAR_HIDE);
        aesCommodity.setScheduleCode(aesCommodity.getAesSchedule().getScheduleCode());

        ValidateUtil.notNull(aesCommodity.getCommodityMaster(), ErrorCode.AES_COMMODITY_DESC_MANDATORY);
        ValidateUtil.notNull(aesCommodity.getCommodityMaster().getHsName(), ErrorCode.AES_COMMODITY_DESC_MANDATORY);
        ValidateUtil.isStatusBlocked(aesCommodity.getCommodityMaster().getStatus(), ErrorCode.AES_COMMODITY_DESC_BLOCK);
        ValidateUtil.isStatusHidden(aesCommodity.getCommodityMaster().getStatus(), ErrorCode.AES_COMMODITY_DESC_HIDE);
        aesCommodity.setCommodityCode(aesCommodity.getCommodityMaster().getHsCode());

        ValidateUtil.notNull(aesCommodity.getFirstNoOfPieces(), ErrorCode.AES_COMMODITY_FRSTPIECES_MANDATORY);
        ValidateUtil.notNull(aesCommodity.getSecondNoOfPieces(), ErrorCode.AES_COMMODITY_SECONDPIECES_MANDATORY);

        regExpService.validate(aesCommodity.getFirstNoOfPieces(), RegExpName.Reg_Exp_AES_PIECES, ErrorCode.AES_PIECES_NAME_INVALID);
        regExpService.validate(aesCommodity.getSecondNoOfPieces(), RegExpName.Reg_Exp_AES_PIECES, ErrorCode.AES_PIECES_NAME_INVALID);

        //ValidateUtil.checkPattern(aesCommodity.getFirstNoOfPieces(), "aesfrstpieces", true);
        //ValidateUtil.checkPattern(aesCommodity.getSecondNoOfPieces(),"aesscndpieces", true);
        ValidateUtil.notNull(aesCommodity.getGrossWeightInKg(), ErrorCode.AES_GROSS_WEIGHT_MANDATORY);


        if (aesCommodity.getItarExcemption() != null && aesCommodity.getItarExcemption().getExcemptionNo() != null) {
            ValidateUtil.isStatusBlocked(aesCommodity.getItarExcemption().getStatus(), ErrorCode.AES_COMMODITY_ITAR_BLOCK);
            ValidateUtil.isStatusHidden(aesCommodity.getItarExcemption().getStatus(), ErrorCode.AES_COMMODITY_ITAR_HIDE);
            aesCommodity.setItarExcemptionCode(aesCommodity.getItarExcemption().getExcemptionCode());

        }
        if (aesCommodity.getItarExcemption() != null && aesCommodity.getItarExcemption().getExcemptionNo() != null) {
            ValidateUtil.isStatusBlocked(aesCommodity.getItarExcemption().getStatus(), ErrorCode.AES_COMMODITY_ITAR_BLOCK);
            ValidateUtil.isStatusHidden(aesCommodity.getItarExcemption().getStatus(), ErrorCode.AES_COMMODITY_ITAR_HIDE);
        }

        if (aesCommodity.getDtdcNo() != null && aesCommodity.getDtdcNo().trim().length() != 0) {
            ValidateUtil.validateLength(aesCommodity.getDtdcNo(), 1, 6, ErrorCode.AES_DTDC_LENGTH_INVALID);
        }
        ValidateUtil.notNull(aesCommodity.getSmeIndicator(), ErrorCode.AES_SME_INDICATOR_MANDATORY);
        ValidateUtil.notNull(aesCommodity.getUsml(), ErrorCode.AES_CATEGORY_MANDATORY);
        ValidateUtil.notNull(aesCommodity.getUsml().getCategoryName(), ErrorCode.AES_CATEGORY_MANDATORY);
        ValidateUtil.isStatusBlocked(aesCommodity.getUsml().getStatus(), ErrorCode.AES_CATEGORY_BLOCK);
        ValidateUtil.isStatusHidden(aesCommodity.getUsml().getStatus(), ErrorCode.AES_CATEGORY_HIDE);
        aesCommodity.setUsmlCode(aesCommodity.getUsml().getCategoryCode());

        ValidateUtil.notNull(aesCommodity.getDtdc(), ErrorCode.AES_UNIT_CODE_MANDATORY);
        ValidateUtil.notNull(aesCommodity.getDtdc().getUnitName(), ErrorCode.AES_UNIT_CODE_MANDATORY);
        ValidateUtil.isStatusBlocked(aesCommodity.getDtdc().getStatus(), ErrorCode.AES_UNIT_CODE_BLOCK);
        ValidateUtil.isStatusHidden(aesCommodity.getDtdc().getStatus(), ErrorCode.AES_UNIT_CODE_HIDE);
        aesCommodity.setDtdcCode(aesCommodity.getDtdc().getUnitCode());

        ValidateUtil.notNull(aesCommodity.getDtdcQuantity(), ErrorCode.AES_QUANTITY_MANDATORY);
        ValidateUtil.validateLength(aesCommodity.getDtdcQuantity(), 1, 7, ErrorCode.AES_QUANTITY_INVALID);


        ValidateUtil.notNull(aesCommodity.getFirstUnit(), ErrorCode.AES_UNIT_CODE_MANDATORY);
        ValidateUtil.notNull(aesCommodity.getFirstUnit().getUnitName(), ErrorCode.AES_UNIT_CODE_MANDATORY);
        ValidateUtil.isStatusBlocked(aesCommodity.getFirstUnit().getStatus(), ErrorCode.AES_UNIT_CODE_BLOCK);
        ValidateUtil.isStatusHidden(aesCommodity.getFirstUnit().getStatus(), ErrorCode.AES_UNIT_CODE_HIDE);
        aesCommodity.setFirstUnitCode(aesCommodity.getFirstUnit().getUnitCode());

        ValidateUtil.notNull(aesCommodity.getSecondUnit(), ErrorCode.AES_UNIT_CODE_MANDATORY);
        ValidateUtil.notNull(aesCommodity.getSecondUnit().getUnitName(), ErrorCode.AES_UNIT_CODE_MANDATORY);
        ValidateUtil.isStatusBlocked(aesCommodity.getSecondUnit().getStatus(), ErrorCode.AES_UNIT_CODE_BLOCK);
        ValidateUtil.isStatusHidden(aesCommodity.getSecondUnit().getStatus(), ErrorCode.AES_UNIT_CODE_HIDE);
        aesCommodity.setSecondUnitCode(aesCommodity.getSecondUnit().getUnitCode());


        ValidateUtil.notNull(aesCommodity.getAesExport(), ErrorCode.AES_EXPORT_CODE_MANDATORY);
        ValidateUtil.notNull(aesCommodity.getAesExport().getExportCode(), ErrorCode.AES_EXPORT_CODE_MANDATORY);
        ValidateUtil.isStatusBlocked(aesCommodity.getAesExport().getStatus(), ErrorCode.AES_EXPORT_CODE_BLOCK);
        ValidateUtil.isStatusHidden(aesCommodity.getAesExport().getStatus(), ErrorCode.AES_EXPORT_CODE_HIDE);
        aesCommodity.setAesExportCode(aesCommodity.getAesExport().getExportCode());

        if (aesCommodity.getEccn() != null && aesCommodity.getEccn().trim().length() != 0) {

            if ((aesCommodity.getEccn().length()) > 5) {

                throw new RestException(ErrorCode.AES_ECCN_LENGTH_INVALID);
            }
        }


        if (aesCommodity.getExportLicenseNo() != null && aesCommodity.getExportLicenseNo().trim().length() != 0) {

            if ((aesCommodity.getExportLicenseNo().length()) > 12) {

                throw new RestException(ErrorCode.AES_EXPORT_LICENSE_LENGTH_INVALID);
            }
        }


        ValidateUtil.notNull(aesCommodity.getLicenseValue(), ErrorCode.AES_LICENSE_VALUE_MANDATORY);
        if (aesCommodity.getLicenseValue().length() > 10) {

            throw new RestException(ErrorCode.AES_LICENSE_VALUE_LENGTH_INVALID);
        }

        ValidateUtil.notNull(aesCommodity.getIsUsedVehicle(), ErrorCode.AES_USED_VEHICLE_VALUE_MANDATORY);
    }


    public void verifyFreightForwarder(AesFile aesFile) {

        ValidateUtil.notNull(aesFile.getAesFreightForwarder(), ErrorCode.AES_FREIGHT_FORWARDER_MANDATORY);
        ValidateUtil.notNull(aesFile.getAesFreightForwarder().getForwarder(), ErrorCode.AES_FREIGHT_FORWARDER_MANDATORY);
        ValidateUtil.notNull(aesFile.getAesFreightForwarder().getForwarder().getId(), ErrorCode.AES_FREIGHT_FORWARDER_MANDATORY);
        ValidateUtil.isStatusBlocked(aesFile.getAesFreightForwarder().getForwarder().getStatus(), ErrorCode.AES_FREIGHT_FORWARDER_BLOCK);
        ValidateUtil.isStatusHidden(aesFile.getAesFreightForwarder().getForwarder().getStatus(), ErrorCode.AES_FREIGHT_FORWARDER_HIDE);
        ValidateUtil.isDefaulter(aesFile.getAesFreightForwarder().getForwarder(), ErrorCode.AES_FREIGHT_FORWARDER_DEFAULTER);
        aesFile.getAesFreightForwarder().setForwarderCode(aesFile.getAesFreightForwarder().getForwarder().getPartyCode());

        //ValidateUtil.notNull(aesFile.getAesFreightForwarder().getCompany(), ErrorCode.AES_FREIGHT_COMPANY_MANDATORY);
        //ValidateUtil.notNull(aesFile.getAesFreightForwarder().getCompany().getId(), ErrorCode.AES_FREIGHT_COMPANY_MANDATORY);
        //ValidateUtil.isStatusBlocked(aesFile.getAesFreightForwarder().getCompany().getStatus(), ErrorCode.AES_FREIGHT_COMPANY_BLOCK);
        //ValidateUtil.isStatusHidden(aesFile.getAesFreightForwarder().getCompany().getStatus(), ErrorCode.AES_FREIGHT_COMPANY_HIDE);
        //ValidateUtil.isDefaulter(aesFile.getAesFreightForwarder().getCompany(), ErrorCode.AES_FREIGHT_COMPANY_DEFAULTER);

        if (aesFile.getAesFreightForwarder().getCompany() != null) {
            aesFile.getAesFreightForwarder().setCompanyCode(aesFile.getAesFreightForwarder().getCompany().getCountryCode());
        }

        ValidateUtil.notNull(aesFile.getAesFreightForwarder().getCompanyAddress(), ErrorCode.AES_FREIGHT_FORWARDER_ADDRESS);

        ValidateUtil.notNull(aesFile.getAesFreightForwarder().getStateMaster(), ErrorCode.AES_FREIGHT_FORWARDER_STATE_MANDATORY);
        ValidateUtil.isStatusBlocked(aesFile.getAesFreightForwarder().getStateMaster().getStatus(), ErrorCode.AES_FREIGHT_FORWARDER_STATE_BLOCK);
        ValidateUtil.isStatusHidden(aesFile.getAesFreightForwarder().getStateMaster().getStatus(), ErrorCode.AES_FREIGHT_FORWARDER_STATE_HIDE);
        aesFile.getAesFreightForwarder().setStateCode(aesFile.getAesFreightForwarder().getStateMaster().getStateCode());

        ValidateUtil.notNull(aesFile.getAesFreightForwarder().getCityMaster(), ErrorCode.AES_FREIGHT_FORWARDER_CITY);
        ValidateUtil.isStatusBlocked(aesFile.getAesFreightForwarder().getCityMaster().getStatus(), ErrorCode.AES_FREIGHT_FORWARDER_CITY_BLOCK);
        ValidateUtil.isStatusHidden(aesFile.getAesFreightForwarder().getCityMaster().getStatus(), ErrorCode.AES_FREIGHT_FORWARDER_CITY_HIDE);
        aesFile.getAesFreightForwarder().setCityCode(aesFile.getAesFreightForwarder().getCityCode());

        ValidateUtil.notNull(aesFile.getAesFreightForwarder().getCountryMaster(), ErrorCode.AES_FREIGHT_FORWARDER_COUNTRY);
        ValidateUtil.isStatusBlocked(aesFile.getAesFreightForwarder().getCountryMaster().getStatus(), ErrorCode.AES_FREIGHT_FORWARDER_COUNTRY_BLOCK);
        ValidateUtil.isStatusHidden(aesFile.getAesFreightForwarder().getCountryMaster().getStatus(), ErrorCode.AES_FREIGHT_FORWARDER_COUNTRY_HIDE);
        aesFile.getAesFreightForwarder().setCountryCode(aesFile.getAesFreightForwarder().getCountryCode());


        ValidateUtil.notNull(aesFile.getAesFreightForwarder().getFirstName(), ErrorCode.AES_FORWARDER_FRST_NAME_MANDATORY);
        regExpService.validate(aesFile.getAesFreightForwarder().getFirstName(), RegExpName.Reg_Exp_AES_NAME, ErrorCode.AES_FORWARDER_FIRST_NAME_INVALID);
        //ValidateUtil.checkPattern(aesFile.getAesFreightForwarder().getFirstName(), "forwarderFrstName", true);
        ValidateUtil.notNull(aesFile.getAesFreightForwarder().getLastName(), ErrorCode.AES_FORWARDER_SECND_NAME_MANDATORY);
        regExpService.validate(aesFile.getAesFreightForwarder().getLastName(), RegExpName.Reg_Exp_AES_NAME, ErrorCode.AES_FORWARDER_SECND_NAME_INVALID);

        ValidateUtil.notNull(aesFile.getAesFreightForwarder().getIdType(), ErrorCode.AES_FORWARDER_ID_TYPE_REQUIRED);
        ValidateUtil.notNull(aesFile.getAesFreightForwarder().getIdNo(), ErrorCode.AES_FORWARDER_ID_NO_REQUIRED);
        //ValidateUtil.checkPattern(aesFile.getAesFreightForwarder().getLastName(), "forwarderSecondName", true);
    }


    public void verifyintermediateConsignee(
            AesFile aesFile) {

        if (aesFile.getAesIntermediateConsignee() != null && aesFile.getAesIntermediateConsignee().getId() != null && aesFile.getAesIntermediateConsignee().getIntermediateConsignee() != null) {
            ValidateUtil.isStatusBlocked(aesFile.getAesIntermediateConsignee().getIntermediateConsignee().getStatus(), ErrorCode.AES_INTERMEDIATE_CONSIGNEE_BLOCK);
            ValidateUtil.isStatusHidden(aesFile.getAesIntermediateConsignee().getIntermediateConsignee().getStatus(), ErrorCode.AES_INTERMEDIATE_CONSIGNEE_HIDE);
            ValidateUtil.isDefaulter(aesFile.getAesIntermediateConsignee().getIntermediateConsignee(), ErrorCode.AES_INTERMEDIATE_CONSIGNEE_DEFAULTER);
            aesFile.getAesIntermediateConsignee().setIntermediateConsigneeCode(aesFile.getAesIntermediateConsignee().getIntermediateConsignee().getPartyCode());
        }

		/*if(aesFile.getAesIntermediateConsignee()!=null && aesFile.getAesIntermediateConsignee().getCompany()!=null&&aesFile.getAesIntermediateConsignee().getCompany().getId()!=null){

			ValidateUtil.isStatusBlocked(aesFile.getAesIntermediateConsignee().getCompany().getStatus(), ErrorCode.AES_INTERMEDIATE_CONSIGNEE_COMPANY_BLOCK);
			ValidateUtil.isStatusHidden(aesFile.getAesIntermediateConsignee().getCompany().getStatus(), ErrorCode.AES_INTERMEDIATE_CONSIGNEE_COMPANY_HIDE);
			ValidateUtil.isDefaulter(aesFile.getAesIntermediateConsignee().getCompany(), ErrorCode.AES_INTERMEDIATE_CONSIGNEE_COMPANY_DEFAULTER);
			
		}*/

        if (aesFile.getAesIntermediateConsignee().getCompany() != null) {
            aesFile.getAesIntermediateConsignee().setCompanyCode(aesFile.getAesIntermediateConsignee().getCompany().getPartyCode());
        }

        if ((aesFile.getAesIntermediateConsignee() != null && aesFile.getAesIntermediateConsignee().getIntermediateConsignee() != null && aesFile.getAesIntermediateConsignee().getIntermediateConsignee().getId() != null)
                && (aesFile.getAesIntermediateConsignee().getCompanyAddress() != null)) {
            ValidateUtil.validateLength(aesFile.getAesIntermediateConsignee().getCompanyAddress().getAddressLine1(), 1, 100, ErrorCode.AES_ULTIMATE_CONSIGNEE_COMPANY_ADDRESS_1_LENGTH);
        }
        if (aesFile.getAesIntermediateConsignee() != null && aesFile.getAesIntermediateConsignee().getCityMaster() != null && aesFile.getAesIntermediateConsignee().getCityMaster().getId() != null) {

            ValidateUtil.isStatusBlocked(aesFile.getAesIntermediateConsignee().getCityMaster().getStatus(), ErrorCode.AES_INTERCONS_CITY_BLOCK);
            ValidateUtil.isStatusHidden(aesFile.getAesIntermediateConsignee().getCityMaster().getStatus(), ErrorCode.AES_INTERCONS_CITY_HIDE);
            aesFile.getAesIntermediateConsignee().setCityCode(aesFile.getAesIntermediateConsignee().getCityCode());
        }
        if (aesFile.getAesIntermediateConsignee() != null && aesFile.getAesIntermediateConsignee().getCountryMaster() != null && aesFile.getAesIntermediateConsignee().getCountryMaster().getId() != null) {

            ValidateUtil.isStatusBlocked(aesFile.getAesIntermediateConsignee().getCountryMaster().getStatus(), ErrorCode.AES_INTERCONS_COUNTRY_BLOCK);
            ValidateUtil.isStatusHidden(aesFile.getAesIntermediateConsignee().getCountryMaster().getStatus(), ErrorCode.AES_INTERCONS_COUNTRY_HIDE);
            aesFile.getAesIntermediateConsignee().setCountryCode(aesFile.getAesIntermediateConsignee().getCountryMaster().getCountryCode());
        }

        if (aesFile.getAesIntermediateConsignee() != null && aesFile.getAesIntermediateConsignee().getStateMaster() != null && aesFile.getAesIntermediateConsignee().getStateMaster().getId() != null) {

            ValidateUtil.isStatusBlocked(aesFile.getAesIntermediateConsignee().getStateMaster().getStatus(), ErrorCode.AES_INTERCONS_STATE_BLOCK);
            ValidateUtil.isStatusHidden(aesFile.getAesIntermediateConsignee().getStateMaster().getStatus(), ErrorCode.AES_INTERCONS_STATE_HIDE);
            aesFile.getAesIntermediateConsignee().setStateCode(aesFile.getAesIntermediateConsignee().getStateMaster().getStateCode());
        }

        if (aesFile.getAesIntermediateConsignee() != null && aesFile.getAesIntermediateConsignee().getFirstName() != null && aesFile.getAesIntermediateConsignee().getFirstName().trim().length() != 0) {
            regExpService.validate(aesFile.getAesIntermediateConsignee().getFirstName(), RegExpName.Reg_Exp_AES_NAME, ErrorCode.AES_INT_CONSIGNEE_FIRST_NAME_INVALID);
            //ValidateUtil.checkPattern(aesFile.getAesIntermediateConsignee().getFirstName(), "intermediateconsFrstName", true);
        }
        if (aesFile.getAesIntermediateConsignee() != null && aesFile.getAesIntermediateConsignee().getLastName() != null && aesFile.getAesIntermediateConsignee().getLastName().trim().length() != 0) {
            regExpService.validate(aesFile.getAesIntermediateConsignee().getLastName(), RegExpName.Reg_Exp_AES_NAME, ErrorCode.AES_INT_CONSIGNEE_SECOND_NAME_INVALID);
            //ValidateUtil.checkPattern(aesFile.getAesIntermediateConsignee().getLastName(), "intermediateconsSecondName", true);
        }

    }


    public void verifyultimateConsignee(
            AesUltimateConsignee aesUltimateConsignee) {
        ValidateUtil.notNull(aesUltimateConsignee, ErrorCode.AES_ULTIMATE_CONSIGNEE_MANDATORY);
        ValidateUtil.notNull(aesUltimateConsignee.getUltimateConsignee(), ErrorCode.AES_ULTIMATE_CONSIGNEE_MANDATORY);
        ValidateUtil.notNullOrEmpty(aesUltimateConsignee.getUltimateConsignee().getPartyCode(), ErrorCode.AES_ULTIMATE_CONSIGNEE_MANDATORY);
        ValidateUtil.isStatusBlocked(aesUltimateConsignee.getUltimateConsignee().getStatus(), ErrorCode.AES_ULTIMATE_CONSIGNEE_BLOCK);
        ValidateUtil.isStatusHidden(aesUltimateConsignee.getUltimateConsignee().getStatus(), ErrorCode.AES_ULTIMATE_CONSIGNEE_HIDE);
        ValidateUtil.isDefaulter(aesUltimateConsignee.getUltimateConsignee(), ErrorCode.AES_ULTIMATE_CONSIGNEE_DEFAULTER);
        aesUltimateConsignee.setUltimateConsigneeCode(aesUltimateConsignee.getUltimateConsignee().getPartyCode());

        //ValidateUtil.notNull(aesUltimateConsignee.getCompany(), ErrorCode.AES_ULTIMATE_CONSIGNEE_COMPANY_MANDATORY);
        //ValidateUtil.notNullOrEmpty(aesUltimateConsignee.getCompany().getPartyCode(), ErrorCode.AES_ULTIMATE_CONSIGNEE_COMPANY_MANDATORY);
        //ValidateUtil.isStatusBlocked(aesUltimateConsignee.getCompany().getStatus(), ErrorCode.AES_ULTIMATE_CONSIGNEE_COMPANY_BLOCK);
        //ValidateUtil.isStatusHidden(aesUltimateConsignee.getCompany().getStatus(), ErrorCode.AES_ULTIMATE_CONSIGNEE_COMPANY_HIDE);
        //ValidateUtil.isDefaulter(aesUltimateConsignee.getCompany(), ErrorCode.AES_ULTIMATE_CONSIGNEE_COMPANY_DEFAULTER);

        if (aesUltimateConsignee.getCompany() != null) {
            aesUltimateConsignee.setCompanyCode(aesUltimateConsignee.getCompany().getPartyCode());
        }

        ValidateUtil.notNull(aesUltimateConsignee.getCompanyAddress(), ErrorCode.AES_ULTIMATE_CONSIGNEE_ADDRESS);
        ValidateUtil.notNullOrEmpty(aesUltimateConsignee.getCompanyAddress().getAddressLine1(), ErrorCode.AES_ULTIMATE_CONSIGNEE_COMPANY_ADDRESS_1_MANDATORY);
        ValidateUtil.validateLength(aesUltimateConsignee.getCompanyAddress().getAddressLine1(), 1, 100, ErrorCode.AES_ULTIMATE_CONSIGNEE_COMPANY_ADDRESS_1_LENGTH);

        //ValidateUtil.notNullOrEmpty(aesUltimateConsignee.getCompanyAddress().getAddressLine2(), ErrorCode.AES_ULTIMATE_CONSIGNEE_COMPANY_ADDRESS_2_MANDATORY);
        //ValidateUtil.validateLength(aesUltimateConsignee.getCompanyAddress().getAddressLine2(), 1, 100, ErrorCode.AES_ULTIMATE_CONSIGNEE_COMPANY_ADDRESS_2_LENGTH);

        if (aesUltimateConsignee.getCompanyAddress().getAddressLine3() != null && aesUltimateConsignee.getCompanyAddress().getAddressLine3().trim().length() != 0) {
            ValidateUtil.validateLength(aesUltimateConsignee.getCompanyAddress().getAddressLine3(), 1, 100, ErrorCode.AES_ULTIMATE_CONSIGNEE_COMPANY_ADDRESS_3_LENGTH);
        }

        //ValidateUtil.notNullOrEmpty(aesUltimateConsignee.getCompanyAddress().getAddressLine4(), ErrorCode.AES_ULTIMATE_CONSIGNEE_COMPANY_ADDRESS_4_MANDATORY);
        //ValidateUtil.validateLength(aesUltimateConsignee.getCompanyAddress().getAddressLine4(), 1, 100, ErrorCode.AES_ULTIMATE_CONSIGNEE_COMPANY_ADDRESS_4_LENGTH);


        ValidateUtil.notNull(aesUltimateConsignee.getConsigneeType(), ErrorCode.AES_ULTIMATE_CONSIGNEE_TYPE_MANDATORY);

        ValidateUtil.notNull(aesUltimateConsignee.getCityMaster(), ErrorCode.AES_ULTIMATE_CONSIGNEE_CITY);
        ValidateUtil.isStatusBlocked(aesUltimateConsignee.getCityMaster().getStatus(), ErrorCode.AES_ULCONS_CITY_BLOCK);
        ValidateUtil.isStatusHidden(aesUltimateConsignee.getCityMaster().getStatus(), ErrorCode.AES_ULCONS_CITY_HIDE);
        aesUltimateConsignee.setCityCode(aesUltimateConsignee.getCityMaster().getCityCode());


        ValidateUtil.notNull(aesUltimateConsignee.getCountryMaster(), ErrorCode.AES_ULTIMATE_CONSIGNEE_COUNTRY);
        ValidateUtil.isStatusBlocked(aesUltimateConsignee.getCountryMaster().getStatus(), ErrorCode.AES_ULCONS_COUNTRY_BLOCK);
        ValidateUtil.isStatusHidden(aesUltimateConsignee.getCountryMaster().getStatus(), ErrorCode.AES_ULCONS_COUNTRY_HIDE);
        aesUltimateConsignee.setCountryCode(aesUltimateConsignee.getCountryCode());

        if (aesUltimateConsignee.getStateMaster() != null && aesUltimateConsignee.getStateMaster().getId() != null) {
            ValidateUtil.isStatusBlocked(aesUltimateConsignee.getStateMaster().getStatus(), ErrorCode.AES_ULCONS_STATE_BLOCK);
            ValidateUtil.isStatusHidden(aesUltimateConsignee.getStateMaster().getStatus(), ErrorCode.AES_ULCONS_STATE_HIDE);
            aesUltimateConsignee.setStateCode(aesUltimateConsignee.getStateMaster().getStateCode());
        }

        if (aesUltimateConsignee.getIdNo() != null && aesUltimateConsignee.getIdNo().trim().length() != 0) {
            regExpService.validate(aesUltimateConsignee.getIdNo(), RegExpName.Reg_Exp_AES_ID_NO, ErrorCode.AES_CONSIGNEE_ID_NO_INVALID);
            //ValidateUtil.checkPattern(aesUltimateConsignee.getIdNo(), "ultimateConsigneeIdNo", true);
        }
        if (aesUltimateConsignee.getFirstName() != null && aesUltimateConsignee.getFirstName().trim().length() != 0) {
            regExpService.validate(aesUltimateConsignee.getFirstName(), RegExpName.Reg_Exp_AES_NAME, ErrorCode.AES_CONSIGNEE_FIRST_NAME_INVALID);
            //ValidateUtil.checkPattern(aesUltimateConsignee.getFirstName(), "ultimateConsigneeFrstName", true);
        }
        if (aesUltimateConsignee.getLastName() != null && aesUltimateConsignee.getLastName().trim().length() != 0) {
            regExpService.validate(aesUltimateConsignee.getLastName(), RegExpName.Reg_Exp_AES_NAME, ErrorCode.AES_CONSIGNEE_SECOND_NAME_INVALID);
            //ValidateUtil.checkPattern(aesUltimateConsignee.getLastName(), "ultimateConsigneeSecondName", true);
        }

    }


    private void verifyCarrier(AesFile aesFile) {

        ValidateUtil.notNull(aesFile.getCarrier(), ErrorCode.AES_CARRIER_CANNOT_NULL);
        ValidateUtil.notNull(aesFile.getCarrier().getId(), ErrorCode.AES_CARRIER_CANNOT_NULL);
        ValidateUtil.notNull(aesFile.getCarrier().getCarrierName(), ErrorCode.AES_CARRIER_CANNOT_NULL);
        ValidateUtil.isStatusBlocked(aesFile.getCarrier().getStatus(), ErrorCode.AES_CARRIER_STATUS_BLOCK);
        ValidateUtil.isStatusHidden(aesFile.getCarrier().getStatus(), ErrorCode.AES_CARRIER_STATUS_HIDE);

        aesFile.setCarrierCode(aesFile.getCarrier().getCarrierCode());
    }


    private void verifyPod(AesFile aesFile) {
        ValidateUtil.notNull(aesFile.getPod(), ErrorCode.AES_POD_CANNOT_NULL);
        ValidateUtil.notNull(aesFile.getPod().getId(), ErrorCode.AES_POD_CANNOT_NULL);
        ValidateUtil.notNull(aesFile.getPod().getPortName(), ErrorCode.AES_POD_CANNOT_NULL);
        ValidateUtil.isStatusBlocked(aesFile.getPod().getStatus(), ErrorCode.AES_POD_STATUS_BLOCK);
        ValidateUtil.isStatusHidden(aesFile.getPod().getStatus(), ErrorCode.AES_POD_STATUS_HIDE);
        aesFile.setPodCode(aesFile.getPod().getPortCode());
    }

    private void verifyPol(AesFile aesFile) {
        ValidateUtil.notNull(aesFile.getPol(), ErrorCode.AES_POL_CANNOT_NULL);
        ValidateUtil.notNull(aesFile.getPol().getId(), ErrorCode.AES_POL_CANNOT_NULL);
        ValidateUtil.notNull(aesFile.getPol().getPortName(), ErrorCode.AES_POL_CANNOT_NULL);
        ValidateUtil.isStatusBlocked(aesFile.getPol().getStatus(), ErrorCode.AES_POL_STATUS_BLOCK);
        ValidateUtil.isStatusHidden(aesFile.getPol().getStatus(), ErrorCode.AES_POL_STATUS_HIDE);
        aesFile.setPolCode(aesFile.getPol().getPortCode());
    }

    private void verifyDestination(AesFile aesFile) {

        ValidateUtil.notNull(aesFile.getDestinationCountry(), ErrorCode.AES_DESTINATION_COUNTRY_CANNOT_NULL);
        ValidateUtil.notNull(aesFile.getDestinationCountry().getId(), ErrorCode.AES_DESTINATION_COUNTRY_CANNOT_NULL);
        ValidateUtil.notNull(aesFile.getDestinationCountry().getCountryName(), ErrorCode.AES_DESTINATION_COUNTRY_CANNOT_NULL);
        ValidateUtil.isStatusBlocked(aesFile.getDestinationCountry().getStatus(), ErrorCode.AES_DESTINATION_COUNTRY_STATUS_BLOCK);
        ValidateUtil.isStatusHidden(aesFile.getDestinationCountry().getStatus(), ErrorCode.AES_DESTINATION_COUNTRY_STATUS_HIDE);
        aesFile.setDestinationCountryCode(aesFile.getDestinationCountry().getCountryCode());
    }


    private void verifyOrigin(AesFile aesFile) {

        ValidateUtil.notNull(aesFile.getOriginState(), ErrorCode.AES_ORIGIN_STATE_CANNOT_NULL);
        ValidateUtil.notNull(aesFile.getOriginState().getId(), ErrorCode.AES_ORIGIN_STATE_CANNOT_NULL);
        ValidateUtil.notNull(aesFile.getOriginState().getName(), ErrorCode.AES_ORIGIN_STATE_CANNOT_NULL);
        ValidateUtil.isStatusBlocked(aesFile.getOriginState().getStatus(), ErrorCode.AES_ORIGIN_STATE_STATUS_BLOCK);
        ValidateUtil.isStatusHidden(aesFile.getOriginState().getStatus(), ErrorCode.AES_ORIGIN_STATE_STATUS_HIDE);
        aesFile.setOriginStateCode(aesFile.getOriginState().getStateCode());

    }

    public void verifyDtdcVehicle(DtdcVehicle dtdcVehicle) {


        if (dtdcVehicle.getVinProductId() != null && dtdcVehicle.getVinProductId().trim().length() != 0) {

            if ((dtdcVehicle.getVinProductId().length()) > 25) {

                throw new RestException(ErrorCode.AES_VIN_PRODUCT_LENGTH_INVALID);
            }
        }

        if (dtdcVehicle.getVehicleTitleNo() != null && dtdcVehicle.getVehicleTitleNo().trim().length() != 0) {

            if ((dtdcVehicle.getVehicleTitleNo().length()) > 15) {

                throw new RestException(ErrorCode.AES_VEHICLE_TITLE_LENGTH_INVALID);
            }
        }

        if (dtdcVehicle.getVehicleTitleState() != null && dtdcVehicle.getVehicleTitleState().getId() != null) {

            ValidateUtil.isStatusBlocked(dtdcVehicle.getVehicleTitleState().getStatus(), ErrorCode.AES_VEHICLE_TITLE_BLOCK);
            ValidateUtil.isStatusHidden(dtdcVehicle.getVehicleTitleState().getStatus(), ErrorCode.AES_VEHICLE_TITLE_HIDE);
            dtdcVehicle.setVehicleTitleStateCode(dtdcVehicle.getVehicleTitleState().getStateCode());
        }


    }


}

