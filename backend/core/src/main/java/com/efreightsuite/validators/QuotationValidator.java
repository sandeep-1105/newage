package com.efreightsuite.validators;

import com.efreightsuite.common.component.CacheRepository;
import com.efreightsuite.enumeration.Approved;
import com.efreightsuite.enumeration.EmploymentStatus;
import com.efreightsuite.enumeration.LovStatus;
import com.efreightsuite.enumeration.YesNo;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.model.*;
import com.efreightsuite.repository.*;
import com.efreightsuite.service.RegularExpressionService;
import com.efreightsuite.util.RegExpName;
import com.efreightsuite.util.SaaSUtil;
import com.efreightsuite.util.ValidateUtil;
import com.efreightsuite.validation.QuotationAttachmentValidator;
import com.efreightsuite.validation.QuotationDetailValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class QuotationValidator {

    private final Logger log = LoggerFactory.getLogger(QuotationValidator.class);

    @Autowired
    private
    RegularExpressionService regExpService;

    @Autowired
    private
    QuotationDetailValidator quotationDetailValidator;

    @Autowired
    private
    QuotationAttachmentValidator quotationAttachmentValidator;

    @Autowired
    private
    CacheRepository cacheRepository;

    @Autowired
    private
    PartyMasterRepository partyMasterRepository;

    @Autowired
    private
    CompanyMasterRepository companyMasterRepository;

    @Autowired
    private
    CountryMasterRepository countryMasterRepository;

    @Autowired
    private
    LocationMasterRepository locationMasterRepository;

    @Autowired
    private
    QuotationRepository quotationRepository;

    @Autowired
    private
    EmployeeMasterRepository employeeMasterRepository;

    @Autowired
    private
    EmployeeMasterRepository salesManRepository;

    @Autowired
    private
    CurrencyMasterRepository currencyMasterRepository;


    public void validate(Quotation quotation) {
        log.info("Validating quotation for enquiry {}", quotation.getEnquiryNo());
        if (quotation.isNew()) {
            checkQuotationNotAssociatedWithEnquiry(quotation.getEnquiryNo());
        }

        verifyBaseData(quotation);

        PartyMaster customer = verifyPartyMaser(
                quotation,
                ErrorCode.QUOTATION_CUSTOMER_NOT_NULL,
                ErrorCode.QUOTATION_CUSTOMER_BLOCKED,
                ErrorCode.QUOTATION_CUSTOMER_HIDE,
                ErrorCode.QUOTATION_CUSTOMER_DEFAULTER);

        verifyAddress(quotation,
                new PartyAddressErrorCodeHolder(
                        ErrorCode.QUOTATION_CUSTOMER_ADDRESS_LINE1_REQUIRED,
                        ErrorCode.QUOTATION_CUSTOMER_ADDRESS_LINE_1_IS_INVALID,
                        ErrorCode.QUOTATION_CUSTOMER_ADDRESS_LINE_2_IS_INVALID,
                        ErrorCode.QUOTATION_CUSTOMER_ADDRESS_LINE_3_IS_INVALID,
                        ErrorCode.QUOTATION_CUSTOMER_CITY_STATE_REQUIRED));

        quotation.setCustomerCode(customer.getPartyCode());

        if (quotation.getShipper() != null && quotation.getShipper().getId() != null) {
            PartyMaster shipper = verifyPartyMaser(
                    quotation,
                    ErrorCode.QUOTATION_SHIPPER_NOT_NULL,
                    ErrorCode.QUOTATION_SHIPPER_BLOCKED,
                    ErrorCode.QUOTATION_SHIPPER_HIDE,
                    ErrorCode.QUOTATION_SHIPPER_DEFAULTER);

            verifyAddress(quotation,
                    new PartyAddressErrorCodeHolder(
                            ErrorCode.QUOTATION_SHIPPER_ADDRESS_LINE1_REQUIRED,
                            ErrorCode.QUOTATION_SHIPPER_ADDRESS_LINE1_REQUIRED,
                            ErrorCode.QUOTATION_SHIPPER_ADDRESS_LINE_2_IS_INVALID,
                            ErrorCode.QUOTATION_SHIPPER_ADDRESS_LINE_3_IS_INVALID,
                            ErrorCode.QUOTATION_SHIPPER_CITY_STATE_REQUIRED));

            quotation.setShipperCode(shipper.getPartyCode());
        }

        verifyValidFromDate(quotation);
        verifyValidToDate(quotation);
        verifyLoggedOnDate(quotation);

        verifyLoggedBy(quotation);

        verifySalesCoordinator(quotation);

        verifySalesman(quotation);

        verifyAttention(quotation);
        verifyApprover(quotation);
        verifyHeaderNote(quotation);
        verifyFooterNote(quotation);

        if (quotation.getQuotationDetailList() != null) {
            for (QuotationDetail quotationDetail : quotation.getQuotationDetailList()) {
                quotationDetailValidator.validate(quotationDetail);
            }
        }


        if (quotation.getQuotationAttachementList() != null) {
            for (QuotationAttachment quotationAttachment : quotation.getQuotationAttachementList()) {
                quotationAttachmentValidator.validate(quotationAttachment);
            }
        }

        quotation.setCompanyCode(companyMasterRepository.findById(quotation.getCompanyMaster().getId()).getCompanyCode());
        quotation.setLocationCode(locationMasterRepository.findById(quotation.getLocationMaster().getId()).getLocationCode());
        quotation.setCountryCode(countryMasterRepository.findById(quotation.getCountryMaster().getId()).getCountryCode());

        if (quotation.getAgent() != null) {
            quotation.setAgentCode(partyMasterRepository.getOne(quotation.getAgent().getId()).getPartyCode());
        }
        if (quotation.getLocalCurrency() != null) {
            quotation.setLocalCurrencyCode(currencyMasterRepository.getOne(quotation.getLocalCurrency().getId()).getCurrencyCode());
        }
    }

    private void verifyAddress(Quotation quotation, PartyAddressErrorCodeHolder partyAddressErrorCodeHolder) {
        verifyAddressLine1(quotation, partyAddressErrorCodeHolder.getErrorCodeAddressLine1Required(), partyAddressErrorCodeHolder.getErrorCodeAddressLine1Invalid());
        verifyAddressLine2(quotation, partyAddressErrorCodeHolder.getErrorCodeAddressLine2Invalid(), partyAddressErrorCodeHolder.getErrorCodeAddressLine3Invalid());
        verifyAddressCityState(quotation.getCustomerAddress(), partyAddressErrorCodeHolder.getErrorCodeCityStateRequired());
    }


    public void checkQuotationNotAssociatedWithEnquiry(String enquiryNo) {
        Quotation quot = quotationRepository.findByEnquiryNo(enquiryNo);
        if (quot != null) {
            log.error("Quotation is already associated with enquiry {}", enquiryNo);
            throw new RestException(ErrorCode.QUOTATION_ALREADY_CREATED_FROM_ENQUIRY);
        }
    }


    private void verifyBaseData(Quotation quotation) {



        ValidateUtil.notNull(quotation.getCompanyMaster(), ErrorCode.QUOTATION_COMPANY);
        ValidateUtil.notNull(quotation.getCompanyMaster().getId(), ErrorCode.QUOTATION_COMPANY);
        CompanyMaster company = companyMasterRepository.findById(quotation.getCompanyMaster().getId());
        ValidateUtil.notNullOrEmpty(company.getCompanyCode(), ErrorCode.QUOTATION_COMPANY);



        ValidateUtil.notNull(quotation.getLocationMaster(), ErrorCode.QUOTATION_LOCATION);
        ValidateUtil.notNull(quotation.getLocationMaster().getId(), ErrorCode.QUOTATION_LOCATION);
        LocationMaster location = locationMasterRepository.findById(quotation.getLocationMaster().getId());
        ValidateUtil.notNullOrEmpty(location.getLocationCode(), ErrorCode.QUOTATION_LOCATION);


        ValidateUtil.notNull(quotation.getCountryMaster(), ErrorCode.QUOTATION_COUNTRY);
        ValidateUtil.notNull(quotation.getCountryMaster().getId(), ErrorCode.QUOTATION_COUNTRY);
        CountryMaster country = countryMasterRepository.findById(quotation.getCountryMaster().getId());
        ValidateUtil.notNullOrEmpty(country.getCountryCode(), ErrorCode.QUOTATION_COUNTRY);
    }


    private PartyMaster verifyPartyMaser(Quotation quotation, String errorCodeCustomerNull, String errorCodeCustomerBlocked, String errorCodeCustomerHidden, String errorCodeCustomerDefaulter) {

        PartyMaster persisted = cacheRepository.get(SaaSUtil.getSaaSId(), PartyMaster.class.getName(), quotation.getCustomer().getId(), partyMasterRepository);

        ValidateUtil.notNull(persisted, errorCodeCustomerNull);
        ValidateUtil.notNull(persisted.getId(), errorCodeCustomerNull);

        if (LovStatus.Block.equals(persisted.getStatus())) {
            throw new RestException(errorCodeCustomerBlocked);

        }
        if (LovStatus.Hide.equals(persisted.getStatus())) {
            throw new RestException(errorCodeCustomerHidden);
        }

        ValidateUtil.isDefaulter(persisted, errorCodeCustomerDefaulter);
        return persisted;

    }

    private void verifyValidFromDate(Quotation quotation) {
        ValidateUtil.notNull(quotation.getValidFrom(), ErrorCode.QUOTATION_VALID_FROM_NOT_NULL);
    }

    private void verifyValidToDate(Quotation quotation) {
        ValidateUtil.notNull(quotation.getValidTo(), ErrorCode.QUOTATION_VALID_TO_NOT_NULL);
    }

    private void verifyLoggedOnDate(Quotation quotation) {
        ValidateUtil.notNull(quotation.getLoggedOn(), ErrorCode.QUOTATION_LOGGED_BY_DATE_REQUIRED);
    }

    private void verifyLoggedBy(Quotation quotation) {

        ValidateUtil.notNull(quotation.getLoggedBy(), ErrorCode.QUOTATION_LOGGED_BY_USER_REQUIRED);
        ValidateUtil.notNull(quotation.getLoggedBy().getId(), ErrorCode.QUOTATION_LOGGED_BY_USER_REQUIRED);

        EmployeeMaster loggedBy =  employeeMasterRepository.findById(quotation.getLoggedBy().getId());


        if (loggedBy.getEmployementStatus().equals(EmploymentStatus.RESIGNED)) {
            throw new RestException(ErrorCode.QUOTATION_LOGGED_BY_USER_RESIGNED);
        }

        if (loggedBy.getEmployementStatus().equals(EmploymentStatus.TERMINATED)) {
            throw new RestException(ErrorCode.QUOTATION_LOGGED_BY_USER_TERMINATED);
        }

        quotation.setLoggedByCode(loggedBy.getEmployeeCode());

    }


    private void verifySalesman(Quotation quotation) {

        if (quotation.getSalesman() != null && quotation.getSalesman().getId() != null) {
            EmployeeMaster salesman = salesManRepository.findById(quotation.getSalesman().getId());
            if (salesman.getIsSalesman().equals(YesNo.No)) {
                throw new RestException(ErrorCode.QUOTATION_SALESMAN_INVALID);
            }

            if (salesman.getEmployementStatus().equals(EmploymentStatus.RESIGNED)) {
                throw new RestException(ErrorCode.QUOTATION_SALESMAN_RESIGNED);
            }

            if (salesman.getEmployementStatus().equals(EmploymentStatus.TERMINATED)) {
                throw new RestException(ErrorCode.QUOTATION_SALESMAN_TERMINATED);
            }

            quotation.setSalesmanCode(salesman.getEmployeeCode());
        } else {
            quotation.setSalesman(null);
            quotation.setSalesmanCode(null);
        }

    }

    private void verifySalesCoordinator(Quotation quotation) {

        ValidateUtil.notNull(quotation.getSalesCoordinator(), ErrorCode.QUOTATION_SALES_COORDINATOR_REQUIRED);
        EmployeeMaster salesCoordinator = employeeMasterRepository.findById(quotation.getSalesCoordinator().getId());

        if (salesCoordinator.getEmployementStatus().equals(EmploymentStatus.RESIGNED)) {
            throw new RestException(ErrorCode.QUOTATION_SALES_COORDINATOR_RESIGNED);
        }

        if (salesCoordinator.getEmployementStatus().equals(EmploymentStatus.TERMINATED)) {
            throw new RestException(ErrorCode.QUOTATION_SALES_COORDINATOR_TERMINATED);
        }

        quotation.setSalesCoordinatorCode(salesCoordinator.getEmployeeCode());
    }

    private void verifyApprover(Quotation quotation) {
        if (quotation.getApproved() != null && !quotation.getApproved().equals(Approved.Pending)) {
            ValidateUtil.notNull(quotation.getApprovedBy(), ErrorCode.QUOTATION_APPROVED_BY_NOT_NULL);
            ValidateUtil.notNull(quotation.getApprovedDate(), ErrorCode.QUOTATION_APPROVED_DATE_NOT_NULL);
            quotation.setApprovedByCode(quotation.getApprovedBy().getEmployeeCode());
        } else {
            quotation.setApprovedBy(null);
            quotation.setApprovedDate(null);
            quotation.setApprovedByCode(null);
        }
    }

    private void verifyHeaderNote(Quotation quotation) {
        if (quotation.getHeaderNote() != null) {
            ValidateUtil.validateLength(quotation.getHeaderNote(), 0, 4000, ErrorCode.QUOTATION_HEADER_NOTE_INVALID);
        }
    }

    private void verifyFooterNote(Quotation quotation) {
        if (quotation.getFooterNote() != null) {
            ValidateUtil.validateLength(quotation.getFooterNote(), 0, 4000, ErrorCode.QUOTATION_FOOTER_NOTE_INVALID);
        }
    }


    private void verifyAddressLine1(Quotation quotation, String errorCodeAddressLine1Required, String errorCodeAddressLine1Invalid) {

        ValidateUtil.notNull(quotation.getCustomerAddress(), errorCodeAddressLine1Required);

        ValidateUtil.notNull(quotation.getCustomerAddress().getAddressLine1(), errorCodeAddressLine1Required);

        ValidateUtil.validateLength(quotation.getCustomerAddress().getAddressLine1(), 0, 100, errorCodeAddressLine1Invalid);
    }

    private void verifyAddressLine2(Quotation quotation, String errorCodeAddressLine2Invalid, String errorCodeAddressLine3Invalid) {

        if (quotation.getCustomerAddress().getAddressLine3() != null) {
            ValidateUtil.validateLength(quotation.getCustomerAddress().getAddressLine2(), 0, 100, errorCodeAddressLine2Invalid);
        }

        if (quotation.getCustomerAddress().getAddressLine3() != null) {

            ValidateUtil.validateLength(quotation.getCustomerAddress().getAddressLine3(), 0, 100, errorCodeAddressLine3Invalid);

        }
    }

    private void verifyAddressCityState(AddressMaster address, String errorCodeCityStateRequired) {
        ValidateUtil.notNull(address.getCity(), errorCodeCityStateRequired);
        ValidateUtil.notNull(address.getState(), errorCodeCityStateRequired);
    }


    private void verifyAttention(Quotation quotation) {
        if (quotation.getAttention() != null) {
            regExpService.validate(quotation.getAttention(), RegExpName.Reg_Exp_Master_PARTY_CONTACT_PERSON, ErrorCode.QUOTATION_ATTENTION_INVALID);
        }

    }


    private static class PartyAddressErrorCodeHolder {
        private final String errorCodeAddressLine1Required;
        private final String errorCodeAddressLine1Invalid;
        private final String errorCodeAddressLine2Invalid;
        private final String errorCodeAddressLine3Invalid;
        private final String errorCodeCityStateRequired;

        private PartyAddressErrorCodeHolder(String errorCodeAddressLine1Required, String errorCodeAddressLine1Invalid, String errorCodeAddressLine2Invalid, String errorCodeAddressLine3Invalid, String errorCodeCityStateRequired) {
            this.errorCodeAddressLine1Required = errorCodeAddressLine1Required;
            this.errorCodeAddressLine1Invalid = errorCodeAddressLine1Invalid;
            this.errorCodeAddressLine2Invalid = errorCodeAddressLine2Invalid;
            this.errorCodeAddressLine3Invalid = errorCodeAddressLine3Invalid;
            this.errorCodeCityStateRequired = errorCodeCityStateRequired;
        }

        public String getErrorCodeAddressLine1Required() {
            return this.errorCodeAddressLine1Required;
        }

        public String getErrorCodeAddressLine1Invalid() {
            return this.errorCodeAddressLine1Invalid;
        }

        public String getErrorCodeAddressLine2Invalid() {
            return this.errorCodeAddressLine2Invalid;
        }

        public String getErrorCodeAddressLine3Invalid() {
            return this.errorCodeAddressLine3Invalid;
        }

        public String getErrorCodeCityStateRequired() {
            return this.errorCodeCityStateRequired;
        }
    }
}

