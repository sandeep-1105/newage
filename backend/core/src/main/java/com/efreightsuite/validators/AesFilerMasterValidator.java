package com.efreightsuite.validators;

import com.efreightsuite.enumeration.LovStatus;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.model.AesFilerMaster;
import com.efreightsuite.service.RegularExpressionService;
import com.efreightsuite.util.RegExpName;
import com.efreightsuite.util.ValidateUtil;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Log4j2
@Service
public class AesFilerMasterValidator {

    @Autowired
    private
    RegularExpressionService regExpService;

    public void validate(AesFilerMaster aesFilerMaster) {
        ValidateUtil.notNull(aesFilerMaster.getAesFilerCode(), ErrorCode.AESFILER_CODE_NOT_NULL);
        regExpService.validate(aesFilerMaster.getAesFilerCode(), RegExpName.REG_EXP_MASTER_AES_FILER_CODE, ErrorCode.AESFILER_CODE_INVALID);

        ValidateUtil.notNull(aesFilerMaster.getTransmitterCode(), ErrorCode.AESFILER_TRANSMITTER_CODE_NOT_NULL);
        regExpService.validate(aesFilerMaster.getTransmitterCode(), RegExpName.REG_EXP_MASTER_AES_FILER_TRANSMITTER_CODE, ErrorCode.AESFILER_TRANSMITTER_CODE_INVALID);

        ValidateUtil.notNull(aesFilerMaster.getStatus(), ErrorCode.AESFILER_LOV_STATUS_NOT_NULL);
        ValidateUtil.notNull(aesFilerMaster.getCompanyMaster(), ErrorCode.AESFILER_COMPANY_NOT_NULL);
        ValidateUtil.notNull(aesFilerMaster.getLocationMaster(), ErrorCode.AESFILER_LOCATION_NOT_NULL);
        ValidateUtil.validateEnum(LovStatus.class, aesFilerMaster.getStatus(),
                ErrorCode.AESFILER_LOV_STATUS_INVALID);

    }
}
