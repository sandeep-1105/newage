package com.efreightsuite.validators;

import com.efreightsuite.controller.representations.EnquiryLogRequest;
import com.efreightsuite.enumeration.LovStatus;
import com.efreightsuite.enumeration.YesNo;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.model.*;
import com.efreightsuite.repository.*;
import com.efreightsuite.service.RegularExpressionService;
import com.efreightsuite.util.RegExpName;
import com.efreightsuite.util.ValidateUtil;
import com.efreightsuite.validation.EnquiryAttachmentValidator;
import com.efreightsuite.validation.EnquiryDimensionValidator;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.LocalDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Log4j2
public class EnquiryLogValidator {

    @Autowired
    private
    EnquiryAttachmentValidator enquiryAttachmentValidator;

    @Autowired
    private
    RegularExpressionService regExpService;

    @Autowired
    private
    EnquiryDimensionValidator enquiryDimensionValidator;

    @Autowired
    private
    EnquiryServiceMappingRepository enquiryServiceMappingRepository;

    @Autowired
    private
    CompanyMasterRepository companyMasterRepository;

    @Autowired
    private
    LocationMasterRepository locationMasterRepository;

    @Autowired
    private
    CountryMasterRepository countryMasterRepository;

    @Autowired
    private
    TosMasterRepository tosMasterRepository;

    @Autowired
    private
    PortMasterRepository portMasterRepository;

    @Autowired
    private
    CommodityMasterRepository commodityMasterRepository;

    @Autowired
    private
    PartyMasterRepository partyMasterRepository;

    @Autowired
    private
    ServiceMasterRepository serviceMasterRepository;

    public EnquiryLog validate(EnquiryLogRequest request, EnquiryLog existing) {
        EnquiryLogRequest validatedRequest = validateInternal(request);
        return validatedRequest.toEnquiryLog(existing);
    }

    public EnquiryLog validate(EnquiryLogRequest request) {
        EnquiryLogRequest validatedRequest = validateInternal(request);
        return validatedRequest.toEnquiryLog();
    }

    private EnquiryLogRequest validateInternal(EnquiryLogRequest request) {
        if (isPartyMasterNotPresent(request)) {
            throw new RestException(ErrorCode.ENQUIRYLOG_PARTY_NOT_NULL);
        }

        validateQuoteByAndReceivedOnDate(request);

        if (request.getStatus() == null) {
            throw new RestException(ErrorCode.ENQUIRYLOG_STATUS_NOT_NULL);
        }

        if (isSalesCoordinatorNotPresent(request)) {
            throw new RestException(ErrorCode.ENQUIRYLOG_SALES_CO_ORDINATOR_NOT_NULL);
        }

        if (CollectionUtils.isEmpty(request.getEnquiryDetailList())) {
            throw new RestException(ErrorCode.ENQUIRYLOG_DETAIL_LIST_EMPTY);
        }

        if (request.getLoggedBy() == null) {
            throw new RestException(ErrorCode.ENQUIRYLOG_LOGGEDBY_NOT_EMPTY);
        }

        validateCompanyMaster(request);
        validateLocationMaster(request);
        validateCountryMaster(request);
        setServiceMasterToEnquiryDetail(request);
        validateEnquiryDetails(request);
        return request;
    }

    void validateQuoteByAndReceivedOnDate(EnquiryLogRequest enquiryLog) {
        if (enquiryLog.getQuoteBy() == null) {
            throw new RestException(ErrorCode.ENQUIRYLOG_QUOTEBY_NOT_NULL);
        }
        if (enquiryLog.getReceivedOn() == null) {
            throw new RestException(ErrorCode.ENQUIRYLOG_RECEIVEDDATE_NOT_NULL);
        }
        if (LocalDate.fromDateFields(enquiryLog.getQuoteBy())
                .isBefore(LocalDate.fromDateFields(enquiryLog.getReceivedOn()))) {
            throw new RestException(ErrorCode.ENQUIRYLOG_QUOTEBY_BEFORE_RECEIVED_ON);
        }
    }

    private void setServiceMasterToEnquiryDetail(EnquiryLogRequest enquiryLog) {
        for (EnquiryDetail enquiryDetail : enquiryLog.getEnquiryDetailList()) {
            EnquiryServiceMapping enquiryServiceMapping = enquiryServiceMappingRepository.findByServiceAndTransportMode(enquiryDetail.getService(), enquiryDetail.getImportExport());
            if (enquiryServiceMapping != null) {
                enquiryDetail.setServiceMaster(enquiryServiceMapping.getServiceMaster());
            }
        }
    }

    private void validateEnquiryDetails(EnquiryLogRequest enquiryLog) {
        List<EnquiryDetail> enquiryDetailList = enquiryLog.getEnquiryDetailList();
        for (EnquiryDetail enquiryDetail : enquiryDetailList) {
            validateEnquiryDetail(enquiryLog, enquiryDetail);
        }
    }

    private void validateEnquiryDetail(EnquiryLogRequest enquiryLog, EnquiryDetail enquiryDetail) {
        enquiryDetail.setOverDimension(YesNo.No);
        validateAttachmentIfPresent(enquiryLog, enquiryDetail);

        if (CollectionUtils.isNotEmpty(enquiryDetail.getEnquiryDimensionList())) {
            enquiryDetail.setOverDimension(YesNo.No);
            for (EnquiryDimension enquiryDimension : enquiryDetail.getEnquiryDimensionList()) {
                enquiryDimensionValidator.validate(enquiryDimension, enquiryDetail, enquiryLog.getLocationMaster());
            }
        }

        regExpService.validate(enquiryDetail.getGrossWeight(), RegExpName.REG_EXP_GROSS_WEIGHT, ErrorCode.ENQUIRYLOG_GROSSWEIGHT_INVALID);
        regExpService.validate(enquiryDetail.getGrossWeight(), RegExpName.REG_EXP_VOLUME_WEIGHT, ErrorCode.ENQUIRYLOG_VOLUMEWEIGHT_INVALID);


        ValidateUtil.notNull(enquiryDetail.getServiceMaster().getId(), ErrorCode.ENQUIRYLOG_SERVICE_NOT_NULL);
        ServiceMaster serviceMaster = serviceMasterRepository.getOne(enquiryDetail.getServiceMaster().getId());
        if (serviceMaster.getStatus().equals(LovStatus.Block)) {
            throw new RestException(ErrorCode.SELECTED_SERVICE_BLOCKED);
        }

        validateTosIfPresent(enquiryDetail);

        ValidateUtil.notNull(enquiryDetail.getFromPortMaster().getId(), ErrorCode.ENQUIRYLOG_FROMPORT_NOT_NULL);
        PortMaster fromPortMaster = portMasterRepository.findById(enquiryDetail.getFromPortMaster().getId());
        if (fromPortMaster.getStatus().equals(LovStatus.Block)) {
            throw new RestException(ErrorCode.ENQUIRYLOG_ORIGIN_PORT_IS_BLOCKED);
        }

        ValidateUtil.notNull(enquiryDetail.getToPortMaster().getId(), ErrorCode.ENQUIRYLOG_TOPORT_NOT_NULL);
        PortMaster toPortMaster = portMasterRepository.findById(enquiryDetail.getToPortMaster().getId());
        if (toPortMaster.getStatus().equals(LovStatus.Block)) {
            throw new RestException(ErrorCode.ENQUIRYLOG_DESTINATION_PORT_IS_BLOCKED);
        }
        CommodityMaster commodityMaster=null;
        if (enquiryDetail.getCommodityMaster() == null || enquiryDetail.getCommodityMaster().getId() == null) {
            enquiryDetail.setCommodityMaster(null);
        } else {
            commodityMaster = commodityMasterRepository.findById(enquiryDetail.getCommodityMaster().getId());
            if (commodityMaster.getStatus().equals(LovStatus.Block)) {
                throw new RestException(ErrorCode.ENQUIRYLOG_COMMODITY_IS_BLOCKED);
            }
        }

        for (EnquiryContainer enquiryContainer : enquiryDetail.getEnquiryContainerList()) {
            validateEnquiryContainer(enquiryContainer);
        }

        if (CollectionUtils.isNotEmpty(enquiryDetail.getEnquiryValueAddedServiceList())) {
            for (EnquiryValueAddedService va : enquiryDetail.getEnquiryValueAddedServiceList()) {
                if (va.getValueAddedServices() != null) {
                    va.setValueAddedServicesCode(va.getValueAddedServices().getValueAddedSevicesCode());
                }
            }
        }

        setRateRequest(enquiryDetail);
        enquiryDetail.setServiceCode(serviceMaster.getServiceCode());
        enquiryDetail.setFromPortMasterCode(fromPortMaster.getPortCode());
        enquiryDetail.setToPortMasterCode(toPortMaster.getPortCode());



        if (enquiryDetail.getTosMaster() != null) {

            TosMaster tosMaster = tosMasterRepository.getOne(enquiryDetail.getTosMaster().getId());
            enquiryDetail.setTosCode(tosMaster.getTosCode());
        }

        if (commodityMaster != null) {
            enquiryDetail.setCommodityMasterCode(enquiryDetail.getCommodityMasterCode());
        }

        setAddressCodes(enquiryDetail.getPickupAddress());
        setAddressCodes(enquiryDetail.getDeliveryAddress());
    }

    private void setRateRequest(EnquiryDetail enquiryDetail) {
        if (CollectionUtils.isNotEmpty(enquiryDetail.getRateRequestList())) {
            for (RateRequest va : enquiryDetail.getRateRequestList()) {
                if (va.getVendor() != null) {
                    PartyMaster vendor = partyMasterRepository.findById(va.getVendor().getId());
                    va.setVendorCode(vendor.getPartyCode());
                }

                if (va.getRateAcceptedEmployee() != null) {
                    va.setRateAcceptedEmployeeCode(va.getRateAcceptedEmployee().getEmployeeCode());
                }

                if (CollectionUtils.isNotEmpty(va.getChargeList())) {
                    for (RateRequestCharge ra : va.getChargeList()) {
                        if (ra.getChargeMaster() != null) {
                            ra.setChargeCode(ra.getChargeMaster().getChargeCode());
                        }
                        if (ra.getUnitMaster() != null) {
                            ra.setUnitCode(ra.getUnitMaster().getUnitCode());
                        }
                        if (ra.getCurrencyMaster() != null) {
                            ra.setCurrencyCode(ra.getCurrencyMaster().getCurrencyCode());
                        }
                    }
                }
            }
        }
    }

    private void setAddressCodes(AddressMaster addressMaster) {
        if (addressMaster != null) {
            if (addressMaster.getState() != null) {
                addressMaster.setStateCode(addressMaster.getState().getStateCode());
            }

            if (addressMaster.getCity() != null) {
                addressMaster.setCityCode(addressMaster.getCity().getCityCode());
            }

            if (addressMaster.getCountry() != null) {
                addressMaster.setCountryCode(addressMaster.getCountry().getCountryCode());
            }
        }
    }

    private void validateAttachmentIfPresent(EnquiryLogRequest enquiryLog, EnquiryDetail enquiryDetail) {
        if (CollectionUtils.isNotEmpty(enquiryDetail.getEnquiryAttachmentList())) {
            for (EnquiryAttachment enquiryAttachment : enquiryDetail.getEnquiryAttachmentList()) {
                enquiryAttachmentValidator.validate(enquiryAttachment, enquiryLog.getLocationMaster());
            }
        }
    }

    private void validateTosIfPresent(EnquiryDetail enquiryDetail) {
        if (enquiryDetail.getTosMaster() != null && enquiryDetail.getTosMaster().getId() != null) {
            TosMaster tosMaster= tosMasterRepository.findOne(enquiryDetail.getTosMaster().getId());
            if (tosMaster.getStatus().equals(LovStatus.Block)) {
                throw new RestException(ErrorCode.ENQUIRYLOG_TOS_IS_BLOCKED);
            }

            if (tosMaster.getStatus().equals(LovStatus.Hide)) {
                throw new RestException(ErrorCode.ENQUIRYLOG_TOS_HIDDEN);
            }
        }
    }


    private boolean isSalesCoordinatorNotPresent(EnquiryLogRequest enquiryLog) {
        return enquiryLog.getSalesCoOrdinator() == null || enquiryLog.getSalesCoOrdinator().getId() == null;
    }

    private boolean isPartyMasterNotPresent(EnquiryLogRequest enquiryLog) {
        return enquiryLog.getPartyMaster() == null || enquiryLog.getPartyMaster().getId() == null;
    }

    private void validateCountryMaster(EnquiryLogRequest enquiryLog) {
        if (enquiryLog.getCountryMaster() == null || enquiryLog.getCountryMaster().getId() == null || StringUtils.isBlank(countryMasterRepository.findById(enquiryLog.getCountryMaster().getId()).getCountryCode())) {
            throw new RestException(ErrorCode.ENQUIRYLOG_COUNTRY);
        }

    }

    private void validateLocationMaster(EnquiryLogRequest enquiryLog) {
        if (enquiryLog.getLocationMaster() == null || enquiryLog.getLocationMaster().getId() == null || StringUtils.isBlank(locationMasterRepository.findById(enquiryLog.getLocationMaster().getId()).getLocationCode())) {
            throw new RestException(ErrorCode.ENQUIRYLOG_LOCATION);
        }

    }

    private void validateCompanyMaster(EnquiryLogRequest enquiryLog) {

        if (enquiryLog.getCompanyMaster() == null
                || enquiryLog.getCompanyMaster().getId() == null) {
            throw new RestException(ErrorCode.ENQUIRYLOG_COMPANY);
        }
        else {
            CompanyMaster companyMaster = companyMasterRepository.findById(enquiryLog.getCompanyMaster().getId());
            if(StringUtils.isBlank(companyMaster.getCompanyCode())
                    || StringUtils.isBlank(companyMaster.getCompanyName())){
                throw new RestException(ErrorCode.ENQUIRYLOG_COMPANY);
            }
        }



        }

    private void validateEnquiryContainer(EnquiryContainer enquiryContainer) {
        ValidateUtil.notNull(enquiryContainer, ErrorCode.ENQUIRYLOG_CONTAINER_NOT_NULL);
        ValidateUtil.notNullOrEmpty(enquiryContainer.getContainerCode(), ErrorCode.ENQUIRYLOG_CONTAINER_CODE_NOT_EMPTY);

        ValidateUtil.notNullOrEmpty(enquiryContainer.getContainerName(), ErrorCode.ENQUIRYLOG_CONTAINER_NAME_NOT_EMPTY);

        ValidateUtil.notNull(enquiryContainer.getNoOfContainer(),
                ErrorCode.ENQUIRYLOG_CONTAINER_NOOFCONTAINER_NOT_NULL);

    }

    //Validate Rate Request
    public void validateRateRequest(RateRequest rateRequest) {

        ValidateUtil.notNull(rateRequest, ErrorCode.ENQUIRYLOG_VENDOR_REQUEST_RATE_INVALID);
        //Vendor VALIDATION
        ValidateUtil.notNull(rateRequest.getVendor(), ErrorCode.ENQUIRYLOG_VENDOR_PARTY_MANDATORY);
        PartyMaster vendor = partyMasterRepository.findById(rateRequest.getVendor().getId());

        ValidateUtil.isStatusBlocked(vendor.getStatus(), ErrorCode.ENQUIRYLOG_VENDOR_PARTY_BLOCK);
        ValidateUtil.isStatusHidden(vendor.getStatus(), ErrorCode.ENQUIRYLOG_VENDOR_PARTY_HIDDEN);

        ValidateUtil.notNull(rateRequest.getEmailId(), ErrorCode.VENDOR_EMAIL_MANDATORY);
        ValidateUtil.isValidEmail(rateRequest.getEmailId(), ErrorCode.ENQUIRYLOG_VENDOR_EMAIL_INVALID);

    }

    //Rate charge
    public void validateRateRequestCharge(RateRequestCharge rateRequestCharge) {

        ValidateUtil.notNull(rateRequestCharge, ErrorCode.ENQUIRYLOG_VENDOR_RATE_CHARGE_INVALID);

        ValidateUtil.notNull(rateRequestCharge.getChargeMaster(), ErrorCode.ENQUIRYLOG_VENDOR_CHARGE_MANDATORY);
        ValidateUtil.isStatusBlocked(rateRequestCharge.getChargeMaster().getStatus(), ErrorCode.ENQUIRYLOG_VENDOR_CHARGE_BLOCK);
        ValidateUtil.isStatusHidden(rateRequestCharge.getChargeMaster().getStatus(), ErrorCode.ENQUIRYLOG_VENDOR_CHARGE_HIDDEN);

        ValidateUtil.notNull(rateRequestCharge.getUnitMaster(), ErrorCode.ENQUIRYLOG_VENDOR_UNIT_MANDATORY);
        ValidateUtil.isStatusBlocked(rateRequestCharge.getUnitMaster().getStatus(), ErrorCode.ENQUIRYLOG_VENDOR_UNIT_BLOCK);
        ValidateUtil.isStatusHidden(rateRequestCharge.getUnitMaster().getStatus(), ErrorCode.ENQUIRYLOG_VENDOR_UNIT_HIDDEN);

        ValidateUtil.notNull(rateRequestCharge.getCurrencyMaster(), ErrorCode.ENQUIRYLOG_VENDOR_CURRENCY_MANDATORY);
        ValidateUtil.isStatusBlocked(rateRequestCharge.getCurrencyMaster().getStatus(), ErrorCode.ENQUIRYLOG_VENDOR_CURRENCY_BLOCK);
        ValidateUtil.isStatusHidden(rateRequestCharge.getCurrencyMaster().getStatus(), ErrorCode.ENQUIRYLOG_VENDOR_CURRENCY_HIDDEN);


        if (rateRequestCharge.getBuyRate() != null) {

            if (rateRequestCharge.getBuyRate() == 0) {
                throw new RestException(ErrorCode.ENQUIRYLOG_VENDOR_BUYRATE_INVALID);
            }
            if (rateRequestCharge.getBuyRate() < 0 || rateRequestCharge.getBuyRate() > 99999999999.99) {
                throw new RestException(ErrorCode.ENQUIRYLOG_VENDOR_BUYRATE_INVALID);
            }
        }


    }

}
