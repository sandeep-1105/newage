package com.efreightsuite.configuration;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Reader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.efreightsuite.model.ServiceMaster;
import lombok.extern.log4j.Log4j2;
import org.apache.ibatis.jdbc.ScriptRunner;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.boot.spi.MetadataImplementor;
import org.hibernate.envers.boot.internal.EnversIntegrator;
import org.hibernate.envers.boot.internal.EnversService;
import org.hibernate.tool.hbm2ddl.SchemaExport;
import org.reflections.Reflections;

import org.springframework.core.env.Environment;

@Log4j2
public class JDBCUtil {

    public static boolean addTenantInYml(String dataSourceFile, String dbUrl, String username, String password, String driverName,
                                         String tenant, String companyName, int maxTotal, int maxIdel, int minIdel, String validationQuery,
                                         int validationTimeout) {
        BufferedWriter bw;
        PrintWriter out;
        try (FileWriter fw = new FileWriter(dataSourceFile, true)) {
            bw = new BufferedWriter(fw);

            out = new PrintWriter(bw);

            out.println("");
            out.println("     - ");
            out.println("       name: " + tenant);
            out.println("       default: false");
            out.println("       url: " + dbUrl);
            out.println("       username: " + username);
            out.println("       password: " + password);
            out.println("       driver-class-name: " + driverName);
            out.println("       company-name: " + companyName);
            out.println("       max-total: " + maxTotal);
            out.println("       max-idle: " + maxIdel);
            out.println("       min-idle: " + minIdel);
            out.println("       validation-query: " + validationQuery);
            out.println("       validation-query-timeout: " + validationTimeout);

            out.close();
            return true;
        } catch (IOException e) {
            log.error("Database writing failure for SaaS " + tenant + ", ", e);
        }
        return false;
    }

    private static boolean createTenantSchema(String driverClassName, String connectionURL, String userName, String password, List<String> sqlsToExecute) {
        Connection conn = null;
        Statement stmt = null;
        try {
            Class.forName(driverClassName);
            conn = DriverManager.getConnection(connectionURL, userName, password);
            stmt = conn.createStatement();
            for (String sql : sqlsToExecute) {
                stmt.executeUpdate(sql);
            }
            return true;
        } catch (Exception e) {
            log.error("Failed to DB Creation (SQLException) ", e);
            return false;
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    log.warn("Unable to close connection", e);
                }
            }
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException e) {
                    log.warn("Unable to close statement", e);
                }
            }
        }
    }

    public static boolean createSchema(String dbType, String connectionURL, String userName, String password, String dbOrUser, String dbOrUserPassword) {
        log.info("JDBC Connection Creation");
        if (dbType.toUpperCase().equals("MYSQL")) {
            List<String> sqlsToExecute = Arrays.asList(
                    "CREATE DATABASE " + dbOrUser,
                    "CREATE USER '" + dbOrUser + "'@'%' IDENTIFIED BY '" + dbOrUserPassword + "'",
                    "GRANT ALL PRIVILEGES ON " + dbOrUser + ".* TO '" + dbOrUser + "'@'%' WITH GRANT OPTION");

            return createTenantSchema("com.mysql.jdbc.Driver", connectionURL, userName, password, sqlsToExecute);
        } else if (dbType.toUpperCase().equals("ORACLE")) {
            List<String> sqlsToExecute = Arrays.asList(
                    "CREATE USER " + dbOrUser + " IDENTIFIED BY " + dbOrUserPassword,
                    "grant connect, dba to " + dbOrUser

            );
            return createTenantSchema("oracle.jdbc.driver.OracleDriver", connectionURL, userName, password, sqlsToExecute);

        } else if (dbType.toUpperCase().equals("H2")) {
            List<String> sqlsToExecute = Arrays.asList(
                    "CREATE DATABASE " + dbOrUser,
                    "CREATE USER '" + dbOrUser + "'@'%' IDENTIFIED BY '" + dbOrUserPassword + "'",
                    "GRANT ALL PRIVILEGES ON " + dbOrUser + ".* TO '" + dbOrUser + "'@'%' WITH GRANT OPTION"
            );

            return createTenantSchema("org.h2.Driver", connectionURL, userName, password, sqlsToExecute);

        } else {
            throw new IllegalArgumentException(String.format("%s not supported", dbType));
        }

    }

    public static boolean scriptExecute(Environment environment, String dbType, String connectionURL, String userName, String password, String scriptFile) {

        Connection conn = null;
        try {

            if (dbType.toUpperCase().equals("MYSQL")) {
                Class.forName("com.mysql.jdbc.Driver");
                String mySqlConnectionUrl = getMySqlConnectionUrl(environment, userName);
                conn = DriverManager.getConnection(mySqlConnectionUrl, userName, password);

                ScriptRunner sr = new ScriptRunner(conn);

                // Give the input file to Reader
                Reader reader = new BufferedReader(
                        new FileReader(scriptFile));

                // Exctute script
                sr.runScript(reader);

                sr.closeConnection();
            } else if (dbType.toUpperCase().equals("ORACLE")) {
                Class.forName("oracle.jdbc.driver.OracleDriver");
                conn = DriverManager.getConnection(connectionURL, userName, password);

                ScriptRunner sr = new ScriptRunner(conn);

                // Give the input file to Reader
                Reader reader = new BufferedReader(
                        new FileReader(scriptFile));

                // Exctute script
                sr.runScript(reader);
                sr.closeConnection();
            } else if (dbType.toUpperCase().equals("H2")) {
                Class.forName("org.h2.Driver");
                String mySqlConnectionUrl = "jdbc:h2:mem:efsdevtest;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=FALSE";
                conn = DriverManager.getConnection(mySqlConnectionUrl, userName, password);
                ScriptRunner sr = new ScriptRunner(conn);
                Reader reader = new BufferedReader(
                        new FileReader(scriptFile));
                // Exctute script
                sr.runScript(reader);
                sr.closeConnection();
            }

            return true;
        } catch (SQLException se) {
            // Handle errors for JDBC
            log.error("Activity Script failed (SQLException) ", se);
        } catch (Exception e) {
            // Handle errors for Class.forName
            log.error("Activity Script failed (Exception) ", e);
        } finally {
            // finally block used to close resources
            try {
                if (conn != null)
                    conn.close();
            } catch (SQLException se) {
                log.error("Activity Script JDBC Connection Closed failed (SQLException) ", se);
            } // end finally try
        } // end try

        return false;
    }

    private static String getMySqlConnectionUrl(Environment environment, String userName) {
        if (environment.acceptsProfiles("docker")) {
            return "jdbc:mysql://db/" + userName + "?useSSL=false";
        }
        return "jdbc:mysql://localhost/" + userName + "?useSSL=false";
    }

    public static boolean tableCreation(Environment environment, String dbType, String connectionURL, String dbOrUser, String userName, String password) {
        try {

            // Using Oracle Database.
            Map<String, String> settings = new HashMap<>();

            if (dbType.toUpperCase().equals("MYSQL")) {
                settings.put("connection.driver_class", "com.mysql.jdbc.Driver");
                settings.put("dialect", "org.hibernate.dialect.MySQL57InnoDBDialect");
                String mySqlConnectionUrl = getMySqlConnectionUrl(environment, userName);
                settings.put("hibernate.connection.url", mySqlConnectionUrl);
                settings.put("hibernate.globally_quoted_identifiers", "true");
                settings.put("hibernate.dbcp.maxActive", "10");
                settings.put("hibernate.dbcp.whenExhaustedAction", "1");
                settings.put("hibernate.dbcp.maxWait", "20000");
                settings.put("hibernate.dbcp.maxIdle", "10");
                settings.put("hibernate.dbcp.validationQuery", "select 1");
                settings.put("hibernate.dbcp.testOnBorrow", "true");
                settings.put("hibernate.dbcp.testOnReturn", "true");
            } else if (dbType.toUpperCase().equals("ORACLE")) {
                settings.put("connection.driver_class", "oracle.jdbc.driver.OracleDriver");
                settings.put("dialect", "org.hibernate.dialect.Oracle10gDialect");
                settings.put("hibernate.connection.url", connectionURL);
            } else if (dbType.toUpperCase().equals("H2")) {
                settings.put("connection.driver_class", "org.h2.Driver");
                settings.put("dialect", "org.hibernate.dialect.H2Dialect");
                String mySqlConnectionUrl = "jdbc:h2:mem:efsdevtest;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=FALSE";
                settings.put("hibernate.connection.url", mySqlConnectionUrl);
                settings.put("hibernate.globally_quoted_identifiers", "true");
                settings.put("hibernate.dbcp.maxActive", "10");
                settings.put("hibernate.dbcp.whenExhaustedAction", "1");
                settings.put("hibernate.dbcp.maxWait", "20000");
                settings.put("hibernate.dbcp.maxIdle", "10");
                settings.put("hibernate.dbcp.validationQuery", "select 1");
                settings.put("hibernate.dbcp.testOnBorrow", "true");
                settings.put("hibernate.dbcp.testOnReturn", "true");
            }

            settings.put("hibernate.connection.username", userName);
            settings.put("hibernate.connection.password", password);
            settings.put("hibernate.hbm2ddl.auto", "create");
            settings.put("show_sql", "true");
            settings.put("hibernate.connection.pool_size", "10");
            settings.put("hibernate.physical_naming_strategy", "org.springframework.boot.orm.jpa.hibernate.SpringPhysicalNamingStrategy");

            MetadataSources metadata = new MetadataSources(
                    new StandardServiceRegistryBuilder()
                            .applySettings(settings)
                            .applySetting(EnversService.INTEGRATION_ENABLED, true)
                            .applySetting(EnversIntegrator.AUTO_REGISTER, true)
                            .build());

            Reflections reflections = new Reflections(ServiceMaster.class.getPackage().getName());
            Set<Class<?>> classes = reflections.getTypesAnnotatedWith(javax.persistence.Entity.class);
            for (Class<?> clazz : classes) {
                metadata.addAnnotatedClass(clazz);
            }

            SchemaExport schemaExport = new SchemaExport(
                    (MetadataImplementor) metadata.buildMetadata()
            );
            schemaExport.setHaltOnError(true);
            schemaExport.setFormat(true);
            if (dbType.toLowerCase().equals("mysql")) {
                schemaExport.setDelimiter(";");
            }
            schemaExport.execute(true, true, false, true);
            return true;
        } catch (UnsupportedOperationException ex) {
            log.error(" EnversService#initialize should be called only once ", ex);
            return true;
        } catch (Exception e) {
            log.error(" Table Creation Exception ", e);
        }


        return false;
    }
}
