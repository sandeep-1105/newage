package com.efreightsuite.configuration;

import java.io.FileInputStream;
import java.util.HashMap;
import java.util.Map;

import com.efreightsuite.configuration.MultitenancyConfigurationProperties.Tenant;
import lombok.extern.log4j.Log4j2;
import org.activiti.engine.ProcessEngine;
import org.activiti.engine.ProcessEngineConfiguration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class ActivityBPMNDeployment {

    @Value("${efs.resource.path}")
    String appResourcePath;
    @Autowired
    private MultitenancyConfigurationProperties multitenancyProperties;

    public void createDeployment() {

        for (Tenant tenant : multitenancyProperties.getTenants()) {

            if (tenant.isDefault()) {
                continue;
            }
            deployment(tenant.getDriverClassName(), tenant.getUrl(), tenant.getUsername(), tenant.getPassword());

        }

    }

    public void deployment(String driverClass, String url, String userName, String password) {

        log.info("ActivityBPMNDeployment Process Engine Instance Created!!");

        Map<String, String> bpmnMap = new HashMap<>();

        bpmnMap.put("air-export-process.bpmn", appResourcePath + "/efs-actitivi-process/air-export-process.bpmn");
        bpmnMap.put("air-import-process.bpmn", appResourcePath + "/efs-actitivi-process/air-import-process.bpmn");

        ProcessEngine processEngineInstance = ProcessEngineConfiguration
                .createStandaloneProcessEngineConfiguration()
                .setJdbcDriver(driverClass)
                .setJdbcUrl(url)
                .setJdbcUsername(userName)
                .setJdbcPassword(password)
                .setJdbcMaxActiveConnections(100)
                .setJdbcMaxIdleConnections(20)
                .setAsyncExecutorActivate(true)
                // TODO: Shekhar: Setting only for testing
                .setDatabaseSchemaUpdate("true")
                .buildProcessEngine();

        for (String key : bpmnMap.keySet()) {
            try {
                log.info("Deployed file Name : " + bpmnMap.get(key));
                processEngineInstance
                        .getRepositoryService()
                        .createDeployment().name("EfsManualDeployment").category(key)
                        .addInputStream(key, new FileInputStream(bpmnMap.get(key))).deploy();
            } catch (Exception e) {
                log.error("Deployed failed file Name : " + bpmnMap.get(key));
                log.error(e);
            }
        }

        processEngineInstance.close();
    }

}
