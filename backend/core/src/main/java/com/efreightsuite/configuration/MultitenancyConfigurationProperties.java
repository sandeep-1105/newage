package com.efreightsuite.configuration;

import java.util.ArrayList;
import java.util.List;

import com.efreightsuite.support.ApplicationProperties;

import org.springframework.context.annotation.DependsOn;

@ApplicationProperties(locations = "file:${efs.resource.path}/efs-data-source/application-${spring.profiles.active}-datasource.yml", prefix = "multitenancy")
@DependsOn("applicationPropertiesBindingPostProcessor")
public class MultitenancyConfigurationProperties {

    private List<Tenant> tenants = new ArrayList<>();

    public List<Tenant> getTenants() {
        return tenants;
    }

    public static class Tenant {

        private String name;

        private boolean isDefault;

        private String driverClassName;

        private String url;

        private String username;

        private String password;

        private String companyName;

        private int maxTotal;

        private int maxIdle;

        private int minIdle;

        private String validationQuery;

        private int validationQueryTimeout;

        public int getMaxTotal() {
            return maxTotal;
        }

        public void setMaxTotal(int maxTotal) {
            this.maxTotal = maxTotal;
        }

        public int getMaxIdle() {
            return maxIdle;
        }

        public void setMaxIdle(int maxIdle) {
            this.maxIdle = maxIdle;
        }

        public int getMinIdle() {
            return minIdle;
        }

        public void setMinIdle(int minIdle) {
            this.minIdle = minIdle;
        }

        public String getValidationQuery() {
            return validationQuery;
        }

        public void setValidationQuery(String validationQuery) {
            this.validationQuery = validationQuery;
        }

        public int getValidationQueryTimeout() {
            return validationQueryTimeout;
        }

        public void setValidationQueryTimeout(int validationQueryTimeout) {
            this.validationQueryTimeout = validationQueryTimeout;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getDriverClassName() {
            return driverClassName;
        }

        public void setDriverClassName(String driverClassName) {
            this.driverClassName = driverClassName;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        public boolean isDefault() {
            return isDefault;
        }

        public void setDefault(boolean isDefault) {
            this.isDefault = isDefault;
        }

        public String getCompanyName() {
            return companyName;
        }

        public void setCompanyName(String companyName) {
            this.companyName = companyName;
        }

    }

}
