package com.efreightsuite.report.service;

import java.util.ArrayList;

import com.efreightsuite.dto.AutoAttachmentDto;
import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.enumeration.ReportName;
import com.efreightsuite.enumeration.SequenceType;
import com.efreightsuite.enumeration.YesNo;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.model.Consol;
import com.efreightsuite.model.ConsolAttachment;
import com.efreightsuite.model.ConsolDocument;
import com.efreightsuite.model.DocumentDetail;
import com.efreightsuite.model.DocumentMaster;
import com.efreightsuite.model.EnquiryAttachment;
import com.efreightsuite.model.EnquiryDetail;
import com.efreightsuite.model.Quotation;
import com.efreightsuite.model.QuotationAttachment;
import com.efreightsuite.model.QuotationDetail;
import com.efreightsuite.model.ReportMaster;
import com.efreightsuite.model.ShipmentAttachment;
import com.efreightsuite.model.ShipmentServiceDetail;
import com.efreightsuite.repository.ConsolDocumentRepository;
import com.efreightsuite.repository.ConsolRepository;
import com.efreightsuite.repository.DocumentDetailRepository;
import com.efreightsuite.repository.DocumentMasterRepository;
import com.efreightsuite.repository.EnquiryDetailRepository;
import com.efreightsuite.repository.QuotationDetailRepository;
import com.efreightsuite.repository.QuotationRepository;
import com.efreightsuite.repository.ReportMasterRepository;
import com.efreightsuite.repository.ShipmentServiceDetailRepository;
import com.efreightsuite.service.SequenceGeneratorService;
import com.efreightsuite.util.AppUtil;
import com.efreightsuite.util.CryptoException;
import com.efreightsuite.util.CryptoUtils;
import groovy.util.logging.Log4j;
import org.jfree.util.Log;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Log4j
public class AutoAttachService {

    @Autowired
    private
    AppUtil appUtil;

    @Autowired
    private
    DocumentMasterRepository documentMasterRepository;

    @Autowired
    private
    SequenceGeneratorService sequenceGeneratorService;

    @Autowired
    private
    EnquiryDetailRepository enquiryDetailRepository;

    @Autowired
    private
    QuotationRepository quotationRepository;

    @Autowired
    private
    QuotationDetailRepository quotationDetailRepository;

    @Autowired
    private
    ShipmentServiceDetailRepository shipmentServiceDetailRepository;

    @Autowired
    private
    DocumentDetailRepository documentDetailRepository;

    @Autowired
    private
    ConsolDocumentRepository consolDocumentRepository;

    @Autowired
    private
    ReportMasterRepository reportMasterRepository;

    @Autowired
    private
    ConsolRepository consolRepository;

    public BaseDto attach(AutoAttachmentDto autoAttachmentDto) throws CryptoException {

        String referenceNo = sequenceGeneratorService.getSequenceValueByType(SequenceType.REFERENCE_NO);
        ReportMaster rm = reportMasterRepository.findByReportKey(ReportName.valueOf(autoAttachmentDto.getDocumentName()));
        DocumentMaster dm = null;
        if (rm != null && rm.getDocumentMaster() != null) {
            dm = documentMasterRepository.findById(rm.getDocumentMaster().getId());
        }
        BaseDto bd = new BaseDto();

        if (autoAttachmentDto != null && autoAttachmentDto.getTransactionFor() != null) {

            switch (autoAttachmentDto.getTransactionFor()) {
                case "Enquiry":
                    bd = setAttachmentEnquiry(autoAttachmentDto, bd, referenceNo, dm);
                    break;
                case "Quotation":
                    bd = setAttachmentQuotation(autoAttachmentDto, bd, referenceNo, dm);
                    break;
                case "Shipment":
                    bd = setAttachmentShipmentService(autoAttachmentDto, bd, referenceNo, dm);
                    break;
                case "MasterShipment":
                    bd = setAttachmentConsol(autoAttachmentDto, bd, referenceNo, dm);
                    break;
                case "Invoice":
                    //setAttachmentInvoice(autoAttachmentDto,KEY,bd,referenceNo,dm);
                    break;
                default:
            }
        }
        return bd;
    }


    private BaseDto setAttachmentConsol(AutoAttachmentDto autoAttachmentDto, BaseDto bd, String referenceNo, DocumentMaster dm) throws CryptoException {


        ShipmentServiceDetail sd = null;
        ConsolDocument cd = null;
        Consol c = null;
        if (autoAttachmentDto.getParentId() != null) {
            c = consolRepository.findById(autoAttachmentDto.getParentId());
            cd = consolDocumentRepository.findById(c.getConsolDocument().getId());
        } else if (autoAttachmentDto.getChildId() != null) {
            sd = shipmentServiceDetailRepository.findById(autoAttachmentDto.getChildId());
            c = consolRepository.findByConsolUid(sd.getConsolUid() != null ? sd.getConsolUid() : null);
        } else if (autoAttachmentDto.getDocumentId() != null) {
            cd = consolDocumentRepository.findById(autoAttachmentDto.getDocumentId());
            c = consolRepository.findByConsolUid(cd.getConsolUid() != null ? cd.getConsolUid() : null);
        } else {
            Log.info("nothing selected");
        }
        if (dm != null && c != null) {
            String KEY = appUtil.getLocationConfig("shipment.attach.file.encrypt.key", c.getLocation(), false);
            ConsolAttachment ca = new ConsolAttachment();
            ca.setDocumentMaster(dm);
            ca.setIsCustoms(YesNo.No);
            ca.setIsProtected(dm.getIsProtected() != null ? dm.getIsProtected() : null);
            ca.setConsol(c);
            ca.setRefNo(referenceNo);
            if (dm.getIsProtected() != null && dm.getIsProtected().equals(YesNo.Yes)) {
                ca.setFile(CryptoUtils.encrypt(KEY, autoAttachmentDto.getDocumentObj()));
            } else {
                ca.setFile(autoAttachmentDto.getDocumentObj());
            }
            ca.setFileContentType(autoAttachmentDto.getFormat());
            ca.setFileName(autoAttachmentDto.getDocumentName());
            if (c.getConsolAttachmentList() != null) {
                c.getConsolAttachmentList().add(ca);
            } else {
                c.setConsolAttachmentList(new ArrayList<>());
                c.getConsolAttachmentList().add(ca);
            }
            consolRepository.save(c);
            bd.setResponseCode(ErrorCode.SUCCESS);
        } else {
            bd.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(bd);

    }


    //* For AUto attachment for enquiry*//
    private BaseDto setAttachmentEnquiry(AutoAttachmentDto autoAttachmentDto, BaseDto bd, String referenceNo, DocumentMaster dm) {

        EnquiryDetail ed = enquiryDetailRepository.findById(autoAttachmentDto.getChildId());
        if (ed != null) {
            EnquiryAttachment eq = new EnquiryAttachment();
            eq.setEnquiryDetail(ed);
            eq.setRefNo(referenceNo);
            eq.setFile(autoAttachmentDto.getDocumentObj());
            eq.setFileContentType(autoAttachmentDto.getFormat());
            eq.setFileName(autoAttachmentDto.getDocumentName());
            if (ed.getEnquiryAttachmentList() != null) {
                ed.getEnquiryAttachmentList().add(eq);
            } else {
                ed.setEnquiryAttachmentList(new ArrayList<>());
                ed.getEnquiryAttachmentList().add(eq);
            }
            enquiryDetailRepository.save(ed);
        }
        return bd;


    }


    private BaseDto setAttachmentShipmentService(AutoAttachmentDto autoAttachmentDto, BaseDto bd, String referenceNo, DocumentMaster dm) throws CryptoException {

        ShipmentServiceDetail sd = null;
        DocumentDetail dd = null;
        if (autoAttachmentDto.getChildId() != null) {
            sd = shipmentServiceDetailRepository.findById(autoAttachmentDto.getChildId());
        } else if (autoAttachmentDto.getDocumentId() != null) {
            dd = documentDetailRepository.findById(autoAttachmentDto.getDocumentId());
            sd = shipmentServiceDetailRepository.findByServiceUid(dd != null ? dd.getServiceUid() : null);
        } else {
            Log.info("nothing selected");
        }


        if (sd != null && dm != null) {
            String key = appUtil.getLocationConfig("shipment.attach.file.encrypt.key", sd.getLocation(), false);
            ShipmentAttachment sa = new ShipmentAttachment();
            sa.setDocumentMaster(dm);
            sa.setIsCustoms(YesNo.No);
            sa.setIsProtected(dm.getIsProtected());
            sa.setShipmentServiceDetail(sd);
            sa.setRefNo(referenceNo);
            if (dm.getIsProtected() != null && dm.getIsProtected().equals(YesNo.Yes)) {
                if (autoAttachmentDto.getDocumentObj() != null) {
                    sa.setFile(CryptoUtils.encrypt(key, autoAttachmentDto.getDocumentObj()));
                }
            } else {
                sa.setFile(autoAttachmentDto.getDocumentObj());
            }
            sa.setFileContentType(autoAttachmentDto.getFormat());
            sa.setFileName(autoAttachmentDto.getDocumentName());
            if (sd.getShipmentAttachmentList() != null) {
                sd.getShipmentAttachmentList().add(sa);
            } else {
                sd.setShipmentAttachmentList(new ArrayList<>());
                sd.getShipmentAttachmentList().add(sa);
            }
            shipmentServiceDetailRepository.save(sd);
            bd.setResponseCode(ErrorCode.SUCCESS);
        } else {
            bd.setResponseCode(ErrorCode.FAILED);
        }
        return appUtil.setDesc(bd);
    }


    private BaseDto setAttachmentQuotation(AutoAttachmentDto autoAttachmentDto, BaseDto bd, String referenceNo, DocumentMaster dm) {

        Quotation q = null;
        QuotationDetail qd = null;
        if (autoAttachmentDto.getParentId() != null) {
            q = quotationRepository.findById(autoAttachmentDto.getParentId());
        } else if (autoAttachmentDto.getChildId() != null) {
            qd = quotationDetailRepository.findById(autoAttachmentDto.getChildId());
            q = quotationRepository.findById(qd.getQuotation().getId());
        } else {
            Log.info("nothing selected");
        }
        if (q != null) {
            QuotationAttachment qa = new QuotationAttachment();
            qa.setQuotation(q);
            qa.setRefNo(referenceNo);
            qa.setFile(autoAttachmentDto.getDocumentObj());
            qa.setFileContentType(autoAttachmentDto.getFormat());
            qa.setFileName(autoAttachmentDto.getDocumentName());
            q.getQuotationAttachementList().add(qa);
            quotationRepository.save(q);
        }
        return bd;
    }


}

