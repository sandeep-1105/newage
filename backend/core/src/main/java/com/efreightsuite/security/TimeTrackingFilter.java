package com.efreightsuite.security;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import lombok.extern.log4j.Log4j2;
import org.apache.log4j.MDC;

@Log4j2
public class TimeTrackingFilter implements Filter {

    @Override
    public void init(FilterConfig arg0) throws ServletException {
        log.debug("TimeTrackingFilter Initialized.....");
    }

    @Override
    public void destroy() {
        log.debug("TimeTrackingFilter Destroyed......");
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {

        long start = System.currentTimeMillis();

        chain.doFilter(request, response);

        long end = System.currentTimeMillis();

        log.info(MDC.get("TrackId") + " [Time Taken : " + (end - start) + "] ms");

    }

}
