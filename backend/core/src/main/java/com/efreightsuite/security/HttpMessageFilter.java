package com.efreightsuite.security;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.Date;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ReadListener;
import javax.servlet.ServletException;
import javax.servlet.ServletInputStream;
import javax.servlet.ServletOutputStream;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.WriteListener;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;

import com.efreightsuite.common.component.HttpMessageComponent;
import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.model.HttpMessage;
import com.efreightsuite.util.JsonToObjectConverter;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.io.IOUtils;

@Log4j2
public class HttpMessageFilter implements Filter {

    @Getter
    @Setter
    boolean isFilterExecute;

    @Getter
    @Setter
    private HttpMessageComponent httpMessageComponent;

    @Override
    public void destroy() {
        // Nothing to do
        log.info("Destroyed");
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)

            throws IOException, ServletException {


        log.debug("HttpRequestFilter called");
        ResettableStreamHttpServletRequest wrappedRequest = new ResettableStreamHttpServletRequest(
                (HttpServletRequest) request);

        log.debug("Request URI" + wrappedRequest.getRequestURI());
        String uri = wrappedRequest.getRequestURI();


        if (isFilterExecute && uri.contains("/api/v1/")) {

            Long httpMessageId = null;
            String body = IOUtils.toString(wrappedRequest.getReader());
            log.debug("Request Body" + body);
            wrappedRequest.resetInputStream();

            HttpServletRequest httpServletRequest = (HttpServletRequest) request;


            HttpMessage httpMessage = new HttpMessage();

            httpMessage.setIpAddress(httpServletRequest.getRemoteAddr());
            httpMessage.setRequestMethod(httpServletRequest.getMethod());
            httpMessage.setPathInfo(httpServletRequest.getPathInfo());
            httpMessage.setRequestURL(httpServletRequest.getRequestURL().toString());
            httpMessage.setServletPath(httpServletRequest.getServletPath());
            httpMessage.setContextPath(httpServletRequest.getContextPath());

            httpMessage.setRequestContent(body.getBytes());
            httpMessage.setCreateDate(new Date());

            httpMessageId = httpMessageComponent.create(httpMessage).getId();

            if (response.getCharacterEncoding() == null) {
                response.setCharacterEncoding("UTF-8");
            }

            ResettableStreamHttpServletResponse responseCopier = new ResettableStreamHttpServletResponse(
                    (HttpServletResponse) response);

            chain.doFilter(wrappedRequest, responseCopier);

            if (response != null) {

                responseCopier.flushBuffer();
                byte[] copy = responseCopier.getCopy();

                String responseBody = new String(copy, response.getCharacterEncoding());
                BaseDto baseDto = (BaseDto) JsonToObjectConverter.converterJsonToObject(responseBody, BaseDto.class);
                if (baseDto != null && httpMessageId != null) {

                    HttpMessage httpMessageToUpdate = httpMessageComponent.findById(httpMessageId);


                    log.debug("Before if : httpMessageToUpdate : " + httpMessageToUpdate);

                    if (httpMessageToUpdate != null) {

                        httpMessageToUpdate.setTrackId(baseDto.getTrackId());
                        httpMessageToUpdate.setResponseContent(responseBody.getBytes());
                        httpMessageToUpdate.setResponseCode(responseCopier.getStatus());
                        httpMessageToUpdate.setLastUpdatedDate(new Date());

                        log.debug("httpMessageToUpdate : " + httpMessageToUpdate);

                        httpMessageComponent.update(httpMessageToUpdate);

                        httpMessageId = null;
                    }

                }

                log.debug("Response Body : " + responseBody);


            }

        } else {
            chain.doFilter(request, response);
        }


    }

    @Override
    public void init(FilterConfig arg0) throws ServletException {
        // Nothing to do
        log.debug("Init");
    }

    private static class ResettableStreamHttpServletRequest extends HttpServletRequestWrapper {

        private byte[] rawData;
        private final HttpServletRequest request;
        private final ResettableServletInputStream servletStream;

        public ResettableStreamHttpServletRequest(HttpServletRequest request) {
            super(request);
            this.request = request;
            this.servletStream = new ResettableServletInputStream();
        }

        public void resetInputStream() {
            servletStream.stream = new ByteArrayInputStream(rawData);
        }

        @Override
        public ServletInputStream getInputStream() throws IOException {

            if (rawData == null) {
                rawData = IOUtils.toByteArray(this.request.getReader());
                servletStream.stream = new ByteArrayInputStream(rawData);
            }
            return servletStream;
        }

        @Override
        public BufferedReader getReader() throws IOException {
            log.info("This object reference" + this);
            if (rawData == null) {
                rawData = IOUtils.toByteArray(this.request.getReader());
                servletStream.stream = new ByteArrayInputStream(rawData);
            }
            return new BufferedReader(new InputStreamReader(servletStream));
        }

        private class ResettableServletInputStream extends ServletInputStream {

            private InputStream stream;

            @Override
            public int read() throws IOException {
                return stream.read();
            }

            @Override
            public boolean isFinished() {
                // Nothing To do
                return false;
            }

            @Override
            public boolean isReady() {
                // Nothing To do
                return false;
            }

            @Override
            public void setReadListener(ReadListener listener) {
                // Nothing To do
            }
        }

    }

    private static class ResettableStreamHttpServletResponse extends HttpServletResponseWrapper {

        private ServletOutputStream outputStream;
        private PrintWriter writer;
        private ResettableServletOutputStream stream;

        public ResettableStreamHttpServletResponse(HttpServletResponse response) throws IOException {
            super(response);
        }

        @Override
        public ServletOutputStream getOutputStream() throws IOException {
            if (writer != null) {
                throw new IllegalStateException("getWriter() has already been called on this response.");
            }

            if (outputStream == null) {
                outputStream = getResponse().getOutputStream();
                stream = new ResettableServletOutputStream(outputStream);
            }

            return stream;
        }

        @Override
        public PrintWriter getWriter() throws IOException {
            if (outputStream != null) {
                throw new IllegalStateException("getOutputStream() has already been called on this response.");
            }

            if (writer == null) {
                stream = new ResettableServletOutputStream(getResponse().getOutputStream());
                writer = new PrintWriter(new OutputStreamWriter(stream, getResponse().getCharacterEncoding()), true);
            }

            return writer;
        }

        @Override
        public void flushBuffer() throws IOException {
            if (writer != null) {
                writer.flush();
            } else if (outputStream != null) {
                stream.flush();
            }
        }

        public byte[] getCopy() {
            if (stream != null) {
                return stream.getCopy();
            } else {
                return new byte[0];
            }
        }

        private class ResettableServletOutputStream extends ServletOutputStream {

            private final OutputStream outputStream;
            private final ByteArrayOutputStream byteArrayOutputStream;

            public ResettableServletOutputStream(OutputStream outputStream) {
                this.outputStream = outputStream;
                this.byteArrayOutputStream = new ByteArrayOutputStream(1024);
            }

            @Override
            public void write(int b) throws IOException {
                outputStream.write(b);
                byteArrayOutputStream.write(b);
            }

            public byte[] getCopy() {
                return byteArrayOutputStream.toByteArray();
            }

            @Override
            public boolean isReady() {
                return false;
            }

            @Override
            public void setWriteListener(WriteListener listener) {

            }

        }

    }

}
