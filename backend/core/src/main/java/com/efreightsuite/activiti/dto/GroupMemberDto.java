package com.efreightsuite.activiti.dto;

import java.util.List;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;


@ToString
public class GroupMemberDto {
    @Getter
    @Setter
    String groupId;

    @Getter
    @Setter
    List<Long> userId;
}
