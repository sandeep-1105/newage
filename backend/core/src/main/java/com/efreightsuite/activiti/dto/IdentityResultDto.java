package com.efreightsuite.activiti.dto;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;


@JsonIgnoreProperties(ignoreUnknown = true)
public class IdentityResultDto implements Serializable {

    private static final long serialVersionUID = 1L;

    @Getter
    @Setter
    List<GroupDto> grouplist;

    @Getter
    @Setter
    GroupDto group;

    @Getter
    @Setter
    List<UserDetailDto> userIdList;
}
