package com.efreightsuite.activiti;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.efreightsuite.enumeration.ActivitiGroup;
import com.efreightsuite.enumeration.DocumentType;
import com.efreightsuite.enumeration.ImportExport;
import com.efreightsuite.enumeration.YesNo;
import com.efreightsuite.model.Consol;
import com.efreightsuite.model.DocumentDetail;
import com.efreightsuite.model.EnquiryDetail;
import com.efreightsuite.model.EnquiryLog;
import com.efreightsuite.model.InvoiceCreditNote;
import com.efreightsuite.model.Quotation;
import com.efreightsuite.model.QuotationDetail;
import com.efreightsuite.model.Shipment;
import com.efreightsuite.model.ShipmentServiceDetail;
import com.efreightsuite.repository.ConsolRepository;
import com.efreightsuite.repository.DefaultMasterDataRepository;
import com.efreightsuite.repository.EnquiryLogRepository;
import com.efreightsuite.repository.InvoiceCreditNoteRepository;
import com.efreightsuite.repository.QuotationRepository;
import com.efreightsuite.repository.ShipmentRepository;
import com.efreightsuite.repository.ShipmentServiceDetailRepository;
import lombok.extern.log4j.Log4j2;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;
import org.activiti.engine.task.TaskQuery;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class ActivitiAirImportService {

    private final boolean workFlowEnabled = true;

    @Autowired
    DefaultMasterDataRepository defaultMasterDataRepository;
    @Autowired
    private
    EnquiryLogRepository enquiryLogRepository;
    @Autowired
    private
    QuotationRepository quotationRepository;
    @Autowired
    private
    ShipmentRepository shipmentRepository;
    @Autowired
    ShipmentServiceDetailRepository shipmentServiceDetailRepository;
    @Autowired
    private
    ConsolRepository consolRepo;
    @Autowired
    private
    InvoiceCreditNoteRepository invoiceRepo;
    @Autowired
    private
    ActivitiUtilService activitiUtilService;
    @Autowired
    private TaskService taskService;
    @Autowired
    private RuntimeService runtimeService;

    private void activityTaskCreated() {
        activitiUtilService.checkAndCreateGroupName(ActivitiGroup.Enquiry);
        activitiUtilService.checkAndCreateGroupName(ActivitiGroup.Quotation);
        activitiUtilService.checkAndCreateGroupName(ActivitiGroup.Shipment);
        activitiUtilService.checkAndCreateGroupName(ActivitiGroup.Pickup);
        activitiUtilService.checkAndCreateGroupName(ActivitiGroup.DeliveryToCfs);
        activitiUtilService.checkAndCreateGroupName(ActivitiGroup.LinkToMaster);
        activitiUtilService.checkAndCreateGroupName(ActivitiGroup.AesOrCustoms);
        activitiUtilService.checkAndCreateGroupName(ActivitiGroup.SignOff);
        activitiUtilService.checkAndCreateGroupName(ActivitiGroup.AirLineSubmission);
        activitiUtilService.checkAndCreateGroupName(ActivitiGroup.JobCompletion);
        activitiUtilService.checkAndCreateGroupName(ActivitiGroup.Invoice);
        activitiUtilService.checkAndCreateGroupName(ActivitiGroup.AtdConfirmation);
        activitiUtilService.checkAndCreateGroupName(ActivitiGroup.AtaConfirmation);
        activitiUtilService.checkAndCreateGroupName(ActivitiGroup.DeliveryOrder);
        activitiUtilService.checkAndCreateGroupName(ActivitiGroup.Clearance);
        activitiUtilService.checkAndCreateGroupName(ActivitiGroup.Pod);
        activitiUtilService.checkAndCreateGroupName(ActivitiGroup.CanAndInvoice);
        activitiUtilService.checkAndCreateGroupName(ActivitiGroup.Customs);
    }

    public void createAirImportEnquiry(EnquiryLog enquiryLog, EnquiryDetail enquiryDetail) {

        if (!workFlowEnabled) {
            return;
        }

        activityTaskCreated();

        //Export Task Created
        Map<String, Object> vars = new HashMap<>();
        vars.put("firstState", "1");
        vars.put("groupName", ActivitiGroup.Enquiry.toString());
        vars.put("EnquiryId", enquiryLog.getId());
        vars.put("EnquiryAirImportId", enquiryDetail.getId());
        ProcessInstance processInstance = activitiUtilService.startAirImportBPMNProcess(vars);
        enquiryDetail.setProcessInstanceId(processInstance.getId());

        TaskQuery activitiQuery = taskService.createTaskQuery();
        activitiQuery.processInstanceId(processInstance.getId());
        activitiQuery.orderByTaskCreateTime().desc();
        List<Task> taskList = activitiQuery.list();
        for (Task task : taskList) {
            enquiryDetail.setTaskId(task.getId());
            taskService.setVariableLocal(task.getId(), "EnquiryId", enquiryLog.getId());
            taskService.setVariableLocal(task.getId(), "EnquiryAirImportId", enquiryDetail.getId());
            taskService.setVariableLocal(task.getId(), "Trade", ImportExport.Import.toString());
            taskService.setVariableLocal(task.getId(), "Location", enquiryLog.getLocationMaster().getId());
            taskService.setVariableLocal(task.getId(), "Company", enquiryLog.getCompanyMaster().getId());
            taskService.setVariableLocal(task.getId(), "EnquiryNo", enquiryLog.getEnquiryNo());
            taskService.setVariableLocal(task.getId(), "groupName", ActivitiGroup.Enquiry.toString());

            if (enquiryLog.getPartyMaster() != null) {
                taskService.setVariableLocal(task.getId(), "customerName", enquiryLog.getPartyMaster().getPartyName());
            } else {
                taskService.setVariableLocal(task.getId(), "customerName", enquiryLog.getEnquiryCustomer().getName());
            }

            if (enquiryDetail.getServiceMaster() != null) {
                taskService.setVariableLocal(task.getId(), "serviceName", enquiryDetail.getServiceMaster().getServiceName());
            }

            if (enquiryDetail.getFromPortMaster() != null) {
                taskService.setVariableLocal(task.getId(), "originName", enquiryDetail.getFromPortMaster().getPortName());
            }

            if (enquiryDetail.getToPortMaster() != null) {
                taskService.setVariableLocal(task.getId(), "destinationName", enquiryDetail.getToPortMaster().getPortName());
            }

            if (enquiryLog.getReceivedOn() != null) {
                log.info("Saving Received On Date" + enquiryLog.getReceivedOn());
                taskService.setVariableLocal(task.getId(), "receivedOnDate", enquiryLog.getReceivedOn());
            }

            if (enquiryLog.getQuoteBy() != null) {
                log.info("Saving QuoteBy Date" + enquiryLog.getQuoteBy());
                taskService.setVariableLocal(task.getId(), "quoteByDate", enquiryLog.getQuoteBy());
            }
        }


    }


    public void cancelAirImportEnquiry(EnquiryLog enquiryLog, EnquiryDetail enquiryDetail) {

        if (!workFlowEnabled) {
            return;
        }

        Map<String, Object> variables = new HashMap<>();

        variables.put("enquiryCancelled", true);
        taskService.complete(enquiryDetail.getTaskId(), variables);
        enquiryDetail.setWorkflowCompleted(YesNo.Yes);
    }

    public void createAirImportQuotation(Quotation quotation, QuotationDetail quotationDetail) {

        if (!workFlowEnabled) {
            return;
        }

        activityTaskCreated();

        if (quotationDetail.getProcessInstanceId() != null) {

            log.debug("From Enquiry To Quotation");

            Map<String, Object> variables = new HashMap<>();

            variables.put("enquiryCancelled", false);
            taskService.complete(quotationDetail.getTaskId(), variables);

            if (quotation.getEnquiryNo() != null) {
                EnquiryLog tmpObj = enquiryLogRepository.findByEnquiryNo(quotation.getEnquiryNo());
                for (EnquiryDetail ed : tmpObj.getEnquiryDetailList()) {

                    if (ed.getProcessInstanceId() != null) {
                        if (quotationDetail.getProcessInstanceId().equals(ed.getProcessInstanceId())) {
                            ed.setWorkflowCompleted(YesNo.Yes);
                        }
                    }
                }
            }

        }

        if (quotationDetail.getProcessInstanceId() == null) {
            log.debug("Direct or Copy Quote Quotation");

            Map<String, Object> vars = new HashMap<>();
            vars.put("firstState", "2");
            vars.put("groupName", ActivitiGroup.Quotation.toString());
            vars.put("QuotationId", quotation.getId());
            vars.put("QuotationAirImportId", quotationDetail.getId());
            ProcessInstance processInstance = activitiUtilService.startAirImportBPMNProcess(vars);
            quotationDetail.setProcessInstanceId(processInstance.getId());
        }

        TaskQuery activitiQuery = taskService.createTaskQuery();
        activitiQuery.processInstanceId(quotationDetail.getProcessInstanceId());
        activitiQuery.orderByTaskCreateTime().desc();
        List<Task> taskList = activitiQuery.list();
        for (Task task : taskList) {
            quotationDetail.setTaskId(task.getId());
            taskService.setVariableLocal(task.getId(), "QuotationId", quotation.getId());
            taskService.setVariableLocal(task.getId(), "QuotationNo", quotation.getQuotationNo());
            taskService.setVariableLocal(task.getId(), "QuotationAirImportId", quotationDetail.getId());
            taskService.setVariableLocal(task.getId(), "Trade", ImportExport.Import.toString());
            taskService.setVariableLocal(task.getId(), "Location", quotation.getLocationMaster().getId());
            taskService.setVariableLocal(task.getId(), "Company", quotation.getCompanyMaster().getId());
            taskService.setVariableLocal(task.getId(), "groupName", ActivitiGroup.Quotation.toString());

            if (quotation.getCustomer() != null) {
                taskService.setVariableLocal(task.getId(), "customerName", quotation.getCustomer().getPartyName());
            }
            if (quotation.getValidTo() != null) {
                taskService.setVariableLocal(task.getId(), "expiresOn", quotation.getValidTo());
            }
            if (quotation.getSalesman() != null) {
                taskService.setVariableLocal(task.getId(), "salesman", quotation.getSalesman().getEmployeeName());
            }
            if (quotationDetail.getServiceMaster() != null) {
                taskService.setVariableLocal(task.getId(), "serviceName", quotationDetail.getServiceMaster().getServiceName());
            }
            if (quotationDetail.getOrigin() != null) {
                taskService.setVariableLocal(task.getId(), "originName", quotationDetail.getOrigin().getPortName());
            }
            if (quotationDetail.getDestination() != null) {
                taskService.setVariableLocal(task.getId(), "destinationName", quotationDetail.getDestination().getPortName());
            }
            if (quotation.getApproved() != null) {
                taskService.setVariableLocal(task.getId(), "quoteStatus", quotation.getApproved());
            }
            if (quotation.getQuoteType() != null) {
                taskService.setVariableLocal(task.getId(), "quoteType", quotation.getQuoteType());
            }
        }


    }

    public void cancelAirImportQuotation(Quotation quotation, QuotationDetail quotationDetail) {

        if (!workFlowEnabled) {
            return;
        }


        log.info("Cancell Quotation Log");

        Map<String, Object> variables = new HashMap<>();

        variables.put("quotationCancelled", true);
        taskService.complete(quotationDetail.getTaskId(), variables);
        quotationDetail.setWorkflowCompleted(YesNo.Yes);

    }


    public void createAirImportShipment(Long shipmentId, ShipmentServiceDetail shipmentServiceDetail) {

        activityTaskCreated();

        if (shipmentServiceDetail.getIsFreehandShipment() != null
                && shipmentServiceDetail.getIsFreehandShipment() == YesNo.Yes) {
            freeHandShipment(shipmentId, shipmentServiceDetail);
        } else {
            createShipment(shipmentId, shipmentServiceDetail);
        }

		/*if(shipmentServiceDetail.getExportRef()!=null){
    		ShipmentServiceDetail exportService = shipmentRepository.getByService(shipmentServiceDetail.getExportRef());
    		
    		pickUp(shipmentId, shipmentServiceDetail, exportService);
        	
        	cfs(shipmentId,shipmentServiceDetail,exportService);
        	
        	//cargoRecived(shipmentId,shipmentServiceDetail);
    	}*/


    }

    public void cancelAirImportShipment(ShipmentServiceDetail shipmentServiceDetail) {

        Map<String, Object> variables = new HashMap<>();
        variables.put("shipmentTaskTo", "3");
        if (shipmentServiceDetail.getTaskId() != null) {
            taskService.complete(shipmentServiceDetail.getTaskId(), variables);
            shipmentServiceDetail.setWorkflowCompleted(YesNo.Yes);
        }
    }

    private void freeHandShipment(Long shipmentId, ShipmentServiceDetail shipmentServiceDetail) {

        log.info("Hey I am Direct Shipment");

        Map<String, Object> vars = new HashMap<>();

        vars.put("firstState", "3");
        vars.put("groupName", ActivitiGroup.Shipment.toString());
        vars.put("ShipmentId", shipmentId);
        vars.put("ShipmentServiceId", shipmentServiceDetail.getId());
        ProcessInstance processInstance = activitiUtilService.startAirImportBPMNProcess(vars);
        shipmentServiceDetail.setProcessInstanceId(processInstance.getId());

        vars.put("customerName", shipmentServiceDetail.getParty() == null ? null : shipmentServiceDetail.getParty().getPartyName());
        vars.put("shipmentUid", shipmentServiceDetail.getShipmentUid());
        vars.put("shipmentDate	", shipmentServiceDetail.getShipment() == null ? null : shipmentServiceDetail.getShipment().getShipmentReqDate());
        vars.put("customerService", shipmentServiceDetail.getCustomerService() == null ? null : shipmentServiceDetail.getCustomerService().getEmployeeName());
        vars.put("origin", shipmentServiceDetail.getOrigin() == null ? null : shipmentServiceDetail.getOrigin().getPortName());
        vars.put("destination", shipmentServiceDetail.getDestination() == null ? null : shipmentServiceDetail.getDestination().getPortName());

        TaskQuery activitiQuery = taskService.createTaskQuery();
        activitiQuery.processInstanceId(shipmentServiceDetail.getProcessInstanceId());
        activitiQuery.orderByTaskCreateTime().desc();
        List<Task> childTaskList = activitiQuery.list();
        for (Task childTask : childTaskList) {
            shipmentServiceDetail.setTaskId(childTask.getId());
            shipmentServiceDetail.setProcessInstanceId(shipmentServiceDetail.getProcessInstanceId());
            taskService.setVariableLocal(childTask.getId(), "ShipmentId", shipmentId);
            taskService.setVariableLocal(childTask.getId(), "ShipmentServiceId", shipmentServiceDetail.getId());
            taskService.setVariableLocal(childTask.getId(), "Trade", ImportExport.Import.toString());
            taskService.setVariableLocal(childTask.getId(), "Location", shipmentServiceDetail.getLocation().getId());
            taskService.setVariableLocal(childTask.getId(), "Company", shipmentServiceDetail.getCompany().getId());
            taskService.setVariableLocal(childTask.getId(), "groupName", ActivitiGroup.Shipment.toString());
            taskService.setVariableLocal(childTask.getId(), "customerName", shipmentServiceDetail.getParty() == null ? null : shipmentServiceDetail.getParty().getPartyName());
            taskService.setVariableLocal(childTask.getId(), "shipmentUid", shipmentServiceDetail.getShipmentUid());
            taskService.setVariableLocal(childTask.getId(), "shipmentDate	", shipmentServiceDetail.getShipment() == null ? null : shipmentServiceDetail.getShipment().getShipmentReqDate());
            taskService.setVariableLocal(childTask.getId(), "customerService", shipmentServiceDetail.getCustomerService() == null ? null : shipmentServiceDetail.getCustomerService().getEmployeeName());
            taskService.setVariableLocal(childTask.getId(), "origin", shipmentServiceDetail.getOrigin() == null ? null : shipmentServiceDetail.getOrigin().getPortName());
            taskService.setVariableLocal(childTask.getId(), "destination", shipmentServiceDetail.getDestination() == null ? null : shipmentServiceDetail.getDestination().getPortName());
        }


    }

    private void createShipment(Long shipmentId, ShipmentServiceDetail shipmentServiceDetail) {
        Task task = null;
        boolean taskCreated = false;
        if (shipmentServiceDetail.getProcessInstanceId() != null) {
            task = taskService.createTaskQuery().taskId(shipmentServiceDetail.getTaskId()).includeTaskLocalVariables().singleResult();
        }

        if (shipmentServiceDetail.getProcessInstanceId() != null && task != null
                && task.getTaskLocalVariables() != null && task.getTaskLocalVariables().get("groupName") != null
                && task.getTaskLocalVariables().get("groupName").equals(ActivitiGroup.Quotation.toString())) {

            if (shipmentServiceDetail.getQuotationUid() != null) {
                Quotation tmpObj = quotationRepository.findByQuotationNo(shipmentServiceDetail.getQuotationUid());
                for (QuotationDetail ed : tmpObj.getQuotationDetailList()) {

                    if (ed.getProcessInstanceId() != null) {
                        if (shipmentServiceDetail.getProcessInstanceId().equals(ed.getProcessInstanceId())) {
                            ed.setWorkflowCompleted(YesNo.Yes);
                        }
                    }
                }
            }

            Map<String, Object> variables = new HashMap<>();
            variables.put("groupName", ActivitiGroup.Shipment.toString());
            variables.put("quotationCancelled", false);
            variables.put("ShipmentServiceId", shipmentServiceDetail.getId());
            variables.put("customerName", shipmentServiceDetail.getParty() == null ? null : shipmentServiceDetail.getParty().getPartyName());
            variables.put("shipmentUid", shipmentServiceDetail.getShipmentUid());
            variables.put("shipmentDate	", shipmentServiceDetail.getShipment() == null ? null : shipmentServiceDetail.getShipment().getShipmentReqDate());
            variables.put("customerService", shipmentServiceDetail.getCustomerService() == null ? null : shipmentServiceDetail.getCustomerService().getEmployeeName());
            variables.put("origin", shipmentServiceDetail.getOrigin() == null ? null : shipmentServiceDetail.getOrigin().getPortName());
            variables.put("destination", shipmentServiceDetail.getDestination() == null ? null : shipmentServiceDetail.getDestination().getPortName());

            taskService.complete(shipmentServiceDetail.getTaskId(), variables);

            taskCreated = true;

        }

        if (shipmentServiceDetail.getProcessInstanceId() == null) {
            log.info("Hey I am Direct Shipment");

            Map<String, Object> vars = new HashMap<>();

            vars.put("firstState", "3");
            vars.put("groupName", ActivitiGroup.Shipment.toString());
            vars.put("ShipmentId", shipmentId);
            vars.put("ShipmentServiceId", shipmentServiceDetail.getId());
            ProcessInstance processInstance = activitiUtilService.startAirImportBPMNProcess(vars);
            shipmentServiceDetail.setProcessInstanceId(processInstance.getId());

            vars.put("customerName", shipmentServiceDetail.getParty() == null ? null : shipmentServiceDetail.getParty().getPartyName());
            vars.put("shipmentUid", shipmentServiceDetail.getShipmentUid());
            vars.put("shipmentDate	", shipmentServiceDetail.getShipment() == null ? null : shipmentServiceDetail.getShipment().getShipmentReqDate());
            vars.put("customerService", shipmentServiceDetail.getCustomerService() == null ? null : shipmentServiceDetail.getCustomerService().getEmployeeName());
            vars.put("origin", shipmentServiceDetail.getOrigin() == null ? null : shipmentServiceDetail.getOrigin().getPortName());
            vars.put("destination", shipmentServiceDetail.getDestination() == null ? null : shipmentServiceDetail.getDestination().getPortName());


            taskCreated = true;


        }

        if (taskCreated) {
            TaskQuery activitiQuery = taskService.createTaskQuery();
            activitiQuery.processInstanceId(shipmentServiceDetail.getProcessInstanceId());
            activitiQuery.orderByTaskCreateTime().desc();
            List<Task> childTaskList = activitiQuery.list();
            for (Task childTask : childTaskList) {
                shipmentServiceDetail.setTaskId(childTask.getId());
                shipmentServiceDetail.setProcessInstanceId(shipmentServiceDetail.getProcessInstanceId());
                taskService.setVariableLocal(childTask.getId(), "ShipmentId", shipmentId);
                taskService.setVariableLocal(childTask.getId(), "ShipmentServiceId", shipmentServiceDetail.getId());
                taskService.setVariableLocal(childTask.getId(), "Trade", ImportExport.Import.toString());
                taskService.setVariableLocal(childTask.getId(), "Location", shipmentServiceDetail.getLocation().getId());
                taskService.setVariableLocal(childTask.getId(), "Company", shipmentServiceDetail.getCompany().getId());
                taskService.setVariableLocal(childTask.getId(), "groupName", ActivitiGroup.Shipment.toString());
                taskService.setVariableLocal(childTask.getId(), "customerName", shipmentServiceDetail.getParty() == null ? null : shipmentServiceDetail.getParty().getPartyName());
                taskService.setVariableLocal(childTask.getId(), "shipmentUid", shipmentServiceDetail.getShipmentUid());
                taskService.setVariableLocal(childTask.getId(), "shipmentDate	", shipmentServiceDetail.getShipment() == null ? null : shipmentServiceDetail.getShipment().getShipmentReqDate());
                taskService.setVariableLocal(childTask.getId(), "customerService", shipmentServiceDetail.getCustomerService() == null ? null : shipmentServiceDetail.getCustomerService().getEmployeeName());
                taskService.setVariableLocal(childTask.getId(), "origin", shipmentServiceDetail.getOrigin() == null ? null : shipmentServiceDetail.getOrigin().getPortName());
                taskService.setVariableLocal(childTask.getId(), "destination", shipmentServiceDetail.getDestination() == null ? null : shipmentServiceDetail.getDestination().getPortName());
            }
        }
    }


    public void pickUp(Long shipmentId, ShipmentServiceDetail shipmentServiceDetail, ShipmentServiceDetail exportService) {
        boolean isOurPickUp = false;
        if (exportService.getPickUpDeliveryPoint() != null && exportService.getPickUpDeliveryPoint().getId() != null
                && exportService.getPickUpDeliveryPoint().getIsOurPickUp() != null
                && exportService.getPickUpDeliveryPoint().getIsOurPickUp().equals(YesNo.Yes)) {
            isOurPickUp = true;
        }

        pickupTaskComplete(shipmentId, isOurPickUp, shipmentServiceDetail);

    }

    private void pickupTaskComplete(Long shipmentId, boolean isOurPickUp, ShipmentServiceDetail shipmentServiceDetail) {
        Task task = taskService.createTaskQuery().taskId(shipmentServiceDetail.getTaskId()).includeTaskLocalVariables().singleResult();

        if (isOurPickUp && task != null && task.getTaskLocalVariables().get("groupName") != null
                && task.getTaskLocalVariables().get("groupName").equals(ActivitiGroup.Shipment.toString())) {


            Map<String, Object> variables = new HashMap<>();
            variables.put("shipmentTaskTo", "1");
            variables.put("groupName", ActivitiGroup.Pickup.toString());


            variables.put("customerName", shipmentServiceDetail.getParty() == null ? null : shipmentServiceDetail.getParty().getPartyName());
            variables.put("shipmentUid", shipmentServiceDetail.getShipmentUid());
            variables.put("shipmentDate	", shipmentServiceDetail.getServiceReqDate());
            variables.put("customerService", shipmentServiceDetail.getCustomerService() == null ? null : shipmentServiceDetail.getCustomerService().getEmployeeName());
            variables.put("origin", shipmentServiceDetail.getOrigin() == null ? null : shipmentServiceDetail.getOrigin().getPortName());
            variables.put("destination", shipmentServiceDetail.getDestination() == null ? null : shipmentServiceDetail.getDestination().getPortName());


            variables.put("ourpickup", shipmentServiceDetail.getPickUpDeliveryPoint() == null ? null : shipmentServiceDetail.getPickUpDeliveryPoint().getIsOurPickUp());
            variables.put("pickupdate", shipmentServiceDetail.getPickUpDeliveryPoint() == null ? null : shipmentServiceDetail.getPickUpDeliveryPoint().getPickUpFollowUp());


            taskService.complete(shipmentServiceDetail.getTaskId(), variables);


            TaskQuery activitiQuery = taskService.createTaskQuery();
            activitiQuery.processInstanceId(shipmentServiceDetail.getProcessInstanceId());
            activitiQuery.orderByTaskCreateTime().desc();
            List<Task> childTaskList = activitiQuery.list();
            for (Task childTask : childTaskList) {
                shipmentServiceDetail.setTaskId(childTask.getId());
                shipmentServiceDetail.setProcessInstanceId(shipmentServiceDetail.getProcessInstanceId());
                taskService.setVariableLocal(childTask.getId(), "ShipmentId", shipmentId);
                taskService.setVariableLocal(childTask.getId(), "ShipmentServiceId", shipmentServiceDetail.getId());
                taskService.setVariableLocal(childTask.getId(), "Trade", ImportExport.Import.toString());
                taskService.setVariableLocal(childTask.getId(), "Location", shipmentServiceDetail.getLocation().getId());
                taskService.setVariableLocal(childTask.getId(), "Company", shipmentServiceDetail.getCompany().getId());
                taskService.setVariableLocal(childTask.getId(), "groupName", ActivitiGroup.Pickup.toString());

                taskService.setVariableLocal(childTask.getId(), "customerName", shipmentServiceDetail.getParty() == null ? null : shipmentServiceDetail.getParty().getPartyName());
                taskService.setVariableLocal(childTask.getId(), "shipmentUid", shipmentServiceDetail.getShipmentUid());
                taskService.setVariableLocal(childTask.getId(), "shipmentDate	", shipmentServiceDetail.getServiceReqDate());
                taskService.setVariableLocal(childTask.getId(), "customerService", shipmentServiceDetail.getCustomerService() == null ? null : shipmentServiceDetail.getCustomerService().getEmployeeName());
                taskService.setVariableLocal(childTask.getId(), "origin", shipmentServiceDetail.getOrigin() == null ? null : shipmentServiceDetail.getOrigin().getPortName());
                taskService.setVariableLocal(childTask.getId(), "destination", shipmentServiceDetail.getDestination() == null ? null : shipmentServiceDetail.getDestination().getPortName());


                taskService.setVariableLocal(childTask.getId(), "ourpickup", shipmentServiceDetail.getPickUpDeliveryPoint() == null ? null : shipmentServiceDetail.getPickUpDeliveryPoint().getIsOurPickUp());
                taskService.setVariableLocal(childTask.getId(), "pickupdate", shipmentServiceDetail.getPickUpDeliveryPoint() == null ? null : shipmentServiceDetail.getPickUpDeliveryPoint().getPickUpFollowUp());
            }

        }
    }

    public void cfs(Long shipmentId, ShipmentServiceDetail shipmentServiceDetail, ShipmentServiceDetail exportService) {


        boolean isCfs = false;

        if (exportService.getPickUpDeliveryPoint() == null) {
            return;
        }

        if (exportService.getPickUpDeliveryPoint().getPickUpActual() != null) {
            isCfs = true;
        }
        cfsTaskCompleted(shipmentId, isCfs, shipmentServiceDetail);

    }

    private void cfsTaskCompleted(Long shipmentId, boolean isCfs, ShipmentServiceDetail shipmentServiceDetail) {
        Task task = taskService.createTaskQuery().taskId(shipmentServiceDetail.getTaskId()).includeTaskLocalVariables().singleResult();

        if (isCfs && task != null && task.getTaskLocalVariables().get("groupName") != null
                && task.getTaskLocalVariables().get("groupName").equals(ActivitiGroup.Pickup.toString())) {


            Map<String, Object> variables = new HashMap<>();
            variables.put("groupName", ActivitiGroup.DeliveryToCfs.toString());

            variables.put("customerName", shipmentServiceDetail.getParty() == null ? null : shipmentServiceDetail.getParty().getPartyName());
            variables.put("shipmentUid", shipmentServiceDetail.getShipmentUid());
            variables.put("shipmentDate	", shipmentServiceDetail.getServiceReqDate());
            variables.put("customerService", shipmentServiceDetail.getCustomerService() == null ? null : shipmentServiceDetail.getCustomerService().getEmployeeName());
            variables.put("origin", shipmentServiceDetail.getOrigin() == null ? null : shipmentServiceDetail.getOrigin().getPortName());
            variables.put("destination", shipmentServiceDetail.getDestination() == null ? null : shipmentServiceDetail.getDestination().getPortName());

            variables.put("etd", shipmentServiceDetail.getEtd());

            taskService.complete(shipmentServiceDetail.getTaskId(), variables);


            TaskQuery activitiQuery = taskService.createTaskQuery();
            activitiQuery.processInstanceId(shipmentServiceDetail.getProcessInstanceId());
            activitiQuery.orderByTaskCreateTime().desc();
            List<Task> childTaskList = activitiQuery.list();
            for (Task childTask : childTaskList) {
                shipmentServiceDetail.setTaskId(childTask.getId());
                shipmentServiceDetail.setProcessInstanceId(shipmentServiceDetail.getProcessInstanceId());
                taskService.setVariableLocal(childTask.getId(), "ShipmentId", shipmentId);
                taskService.setVariableLocal(childTask.getId(), "ShipmentServiceId", shipmentServiceDetail.getId());
                taskService.setVariableLocal(childTask.getId(), "Trade", ImportExport.Import.toString());
                taskService.setVariableLocal(childTask.getId(), "Location", shipmentServiceDetail.getLocation().getId());
                taskService.setVariableLocal(childTask.getId(), "Company", shipmentServiceDetail.getCompany().getId());
                taskService.setVariableLocal(childTask.getId(), "groupName", ActivitiGroup.DeliveryToCfs.toString());

                taskService.setVariableLocal(childTask.getId(), "customerName", shipmentServiceDetail.getParty() == null ? null : shipmentServiceDetail.getParty().getPartyName());
                taskService.setVariableLocal(childTask.getId(), "shipmentUid", shipmentServiceDetail.getShipmentUid());
                taskService.setVariableLocal(childTask.getId(), "shipmentDate", shipmentServiceDetail.getServiceReqDate());
                taskService.setVariableLocal(childTask.getId(), "customerService", shipmentServiceDetail.getCustomerService() == null ? null : shipmentServiceDetail.getCustomerService().getEmployeeName());
                taskService.setVariableLocal(childTask.getId(), "origin", shipmentServiceDetail.getOrigin() == null ? null : shipmentServiceDetail.getOrigin().getPortName());
                taskService.setVariableLocal(childTask.getId(), "destination", shipmentServiceDetail.getDestination() == null ? null : shipmentServiceDetail.getDestination().getPortName());

                taskService.setVariableLocal(childTask.getId(), "etd", shipmentServiceDetail.getEtd());
            }

        }
    }

    private void cargoReceived(ShipmentServiceDetail shipmentServiceDetail) {


        Map<String, Object> variables = new HashMap<>();

        variables.put("groupName", ActivitiGroup.LinkToMaster.toString());
        variables.put("importAtdGroup", ActivitiGroup.AtdConfirmation.toString());

        variables.put("parentTask", shipmentServiceDetail.getTaskId());


        variables.put("customerName", shipmentServiceDetail.getParty() == null ? null : shipmentServiceDetail.getParty().getPartyName());
        variables.put("shipmentUid", shipmentServiceDetail.getShipmentUid());
        variables.put("shipmentDate	", shipmentServiceDetail.getServiceReqDate());
        variables.put("customerService", shipmentServiceDetail.getCustomerService() == null ? null : shipmentServiceDetail.getCustomerService().getEmployeeName());
        variables.put("origin", shipmentServiceDetail.getOrigin() == null ? null : shipmentServiceDetail.getOrigin().getPortName());
        variables.put("destination", shipmentServiceDetail.getDestination() == null ? null : shipmentServiceDetail.getDestination().getPortName());

        variables.put("carrier", shipmentServiceDetail.getCarrier() == null ? null : shipmentServiceDetail.getCarrier().getCarrierName());
        variables.put("flightNo", shipmentServiceDetail.getRouteNo());
        variables.put("mawb", shipmentServiceDetail.getMawbNo());
        variables.put("etd", shipmentServiceDetail.getEtd());
        variables.put("eta", shipmentServiceDetail.getEta());


        taskService.complete(shipmentServiceDetail.getTaskId(), variables);

    }

    public void atdConfirmation(Long shipmentId, ShipmentServiceDetail shipmentServiceDetail, ShipmentServiceDetail exportService) {

        Task task = taskService.createTaskQuery().taskId(shipmentServiceDetail.getTaskId()).includeTaskLocalVariables().singleResult();

        if (task != null && task.getTaskLocalVariables() != null && task.getTaskLocalVariables().get("groupName") != null
                && task.getTaskLocalVariables().get("groupName").equals(ActivitiGroup.DeliveryToCfs.toString())) {
            cargoReceived(shipmentServiceDetail);
        } else if (task != null && task.getTaskLocalVariables() != null && task.getTaskLocalVariables().get("groupName") != null
                && task.getTaskLocalVariables().get("groupName").equals(ActivitiGroup.Shipment.toString())) {

            pickupTaskComplete(shipmentId, true, shipmentServiceDetail);
            cfsTaskCompleted(shipmentId, true, shipmentServiceDetail);
            cargoReceived(shipmentServiceDetail);
        }


        TaskQuery subActivitiQuery = taskService.createTaskQuery();
        subActivitiQuery.processInstanceId(shipmentServiceDetail.getProcessInstanceId());
        subActivitiQuery.orderByTaskCreateTime().desc();
        List<Task> childTaskList = subActivitiQuery.list();
        for (Task childTask : childTaskList) {
            shipmentServiceDetail.setTaskId(childTask.getId());
            shipmentServiceDetail.setProcessInstanceId(childTask.getProcessInstanceId());
            taskService.setVariableLocal(childTask.getId(), "ShipmentId", shipmentId);
            taskService.setVariableLocal(childTask.getId(), "ShipmentServiceId", shipmentServiceDetail.getId());
            taskService.setVariableLocal(childTask.getId(), "Trade", ImportExport.Import.toString());
            taskService.setVariableLocal(childTask.getId(), "Location", shipmentServiceDetail.getLocation().getId());
            taskService.setVariableLocal(childTask.getId(), "Company", shipmentServiceDetail.getCompany().getId());

            taskService.setVariableLocal(childTask.getId(), "customerName", shipmentServiceDetail.getParty() == null ? null : shipmentServiceDetail.getParty().getPartyName());
            taskService.setVariableLocal(childTask.getId(), "shipmentUid", shipmentServiceDetail.getShipmentUid());
            taskService.setVariableLocal(childTask.getId(), "shipmentDate	", shipmentServiceDetail.getServiceReqDate());
            taskService.setVariableLocal(childTask.getId(), "customerService", shipmentServiceDetail.getCustomerService() == null ? null : shipmentServiceDetail.getCustomerService().getEmployeeName());
            taskService.setVariableLocal(childTask.getId(), "origin", shipmentServiceDetail.getOrigin() == null ? null : shipmentServiceDetail.getOrigin().getPortName());
            taskService.setVariableLocal(childTask.getId(), "destination", shipmentServiceDetail.getDestination() == null ? null : shipmentServiceDetail.getDestination().getPortName());

            taskService.setVariableLocal(childTask.getId(), "carrier", shipmentServiceDetail.getCarrier() == null ? null : shipmentServiceDetail.getCarrier().getCarrierName());
            taskService.setVariableLocal(childTask.getId(), "flightNo", shipmentServiceDetail.getRouteNo());
            taskService.setVariableLocal(childTask.getId(), "mawb", shipmentServiceDetail.getMawbNo());
            taskService.setVariableLocal(childTask.getId(), "etd", shipmentServiceDetail.getEtd());
            taskService.setVariableLocal(childTask.getId(), "eta", shipmentServiceDetail.getEta());


            if (childTask.getName().equals(ActivitiGroup.LinkToMaster.toString())) {
                taskService.setVariableLocal(childTask.getId(), "groupName", ActivitiGroup.LinkToMaster.toString());
            } else {
                taskService.setVariableLocal(childTask.getId(), "importAtdGroup", ActivitiGroup.AtdConfirmation.toString());
            }

        }

    }

    public void ataConfirmation(Long shipmentId, ShipmentServiceDetail shipmentServiceDetail) {

        if (!workFlowEnabled) {
            return;
        }

        Task task = taskService.createTaskQuery().taskId(shipmentServiceDetail.getTaskId()).includeTaskLocalVariables().singleResult();

        if (task != null && task.getTaskLocalVariables().get("groupName") != null
                && task.getTaskLocalVariables().get("groupName").equals(ActivitiGroup.Shipment.toString())) {


            Map<String, Object> variables = new HashMap<>();
            variables.put("groupName", ActivitiGroup.AtaConfirmation.toString());
            variables.put("shipmentTaskTo", 2);
            variables.put("customerName", shipmentServiceDetail.getParty() == null ? null : shipmentServiceDetail.getParty().getPartyName());
            variables.put("shipmentUid", shipmentServiceDetail.getShipmentUid());
            variables.put("shipmentDate	", shipmentServiceDetail.getServiceReqDate());
            variables.put("customerService", shipmentServiceDetail.getCustomerService() == null ? null : shipmentServiceDetail.getCustomerService().getEmployeeName());
            variables.put("origin", shipmentServiceDetail.getOrigin() == null ? null : shipmentServiceDetail.getOrigin().getPortName());
            variables.put("destination", shipmentServiceDetail.getDestination() == null ? null : shipmentServiceDetail.getDestination().getPortName());

            variables.put("etd", shipmentServiceDetail.getEtd());

            taskService.complete(shipmentServiceDetail.getTaskId(), variables);


            TaskQuery activitiQuery = taskService.createTaskQuery();
            activitiQuery.processInstanceId(shipmentServiceDetail.getProcessInstanceId());
            activitiQuery.orderByTaskCreateTime().desc();
            List<Task> childTaskList = activitiQuery.list();
            for (Task childTask : childTaskList) {
                shipmentServiceDetail.setTaskId(childTask.getId());
                shipmentServiceDetail.setProcessInstanceId(shipmentServiceDetail.getProcessInstanceId());
                taskService.setVariableLocal(childTask.getId(), "ShipmentId", shipmentId);
                taskService.setVariableLocal(childTask.getId(), "ShipmentServiceId", shipmentServiceDetail.getId());
                taskService.setVariableLocal(childTask.getId(), "Trade", ImportExport.Import.toString());
                taskService.setVariableLocal(childTask.getId(), "Location", shipmentServiceDetail.getLocation().getId());
                taskService.setVariableLocal(childTask.getId(), "Company", shipmentServiceDetail.getCompany().getId());
                taskService.setVariableLocal(childTask.getId(), "groupName", ActivitiGroup.AtaConfirmation.toString());

                taskService.setVariableLocal(childTask.getId(), "customerName", shipmentServiceDetail.getParty() == null ? null : shipmentServiceDetail.getParty().getPartyName());
                taskService.setVariableLocal(childTask.getId(), "shipmentUid", shipmentServiceDetail.getShipmentUid());
                taskService.setVariableLocal(childTask.getId(), "shipmentDate", shipmentServiceDetail.getServiceReqDate());
                taskService.setVariableLocal(childTask.getId(), "customerService", shipmentServiceDetail.getCustomerService() == null ? null : shipmentServiceDetail.getCustomerService().getEmployeeName());
                taskService.setVariableLocal(childTask.getId(), "origin", shipmentServiceDetail.getOrigin() == null ? null : shipmentServiceDetail.getOrigin().getPortName());
                taskService.setVariableLocal(childTask.getId(), "destination", shipmentServiceDetail.getDestination() == null ? null : shipmentServiceDetail.getDestination().getPortName());

            }
        }

        if (shipmentServiceDetail.getConsolUid() == null) {
            return;
        }

        Consol consol = consolRepo.findByConsolUid(shipmentServiceDetail.getConsolUid());

        if (consol.getAta() != null) {
            signOff(shipmentId, shipmentServiceDetail);
        }
    }

    public void signOff(Long shipmentId, ShipmentServiceDetail shipmentServiceDetail) {

        if (!workFlowEnabled) {
            return;
        }

        if (shipmentServiceDetail.getConsolUid() == null) {
            return;
        }

        Consol consol = consolRepo.findByConsolUid(shipmentServiceDetail.getConsolUid());

        if (consol.getAta() != null) {

            TaskQuery activitiQuery = taskService.createTaskQuery();
            activitiQuery.processInstanceId(shipmentServiceDetail.getProcessInstanceId());
            activitiQuery.orderByTaskCreateTime().desc();
            List<Task> childTaskList = activitiQuery.list();
            for (Task childTask : childTaskList) {

                if (childTask.getName().equals(ActivitiGroup.AtaConfirmation.toString())) {
                    Map<String, Object> variables = new HashMap<>();

                    variables.put("groupName", ActivitiGroup.SignOff.toString());
                    variables.put("ShipmentId", shipmentId);
                    variables.put("ShipmentServiceId", shipmentServiceDetail.getId());
                    variables.put("Trade", ImportExport.Import.toString());
                    variables.put("Location", shipmentServiceDetail.getLocation().getId());
                    variables.put("Company", shipmentServiceDetail.getCompany().getId());

                    taskService.complete(childTask.getId(), variables);

                    TaskQuery subActivitiQuery = taskService.createTaskQuery();
                    subActivitiQuery.processInstanceId(shipmentServiceDetail.getProcessInstanceId());
                    subActivitiQuery.orderByTaskCreateTime().desc();
                    List<Task> tmpChild = subActivitiQuery.list();
                    for (Task tmpChildTask : tmpChild) {

                        if (tmpChildTask.getName().equals(ActivitiGroup.SignOff.toString())) {
                            shipmentServiceDetail.setTaskId(tmpChildTask.getId());
                            shipmentServiceDetail.setProcessInstanceId(tmpChildTask.getProcessInstanceId());
                            taskService.setVariableLocal(tmpChildTask.getId(), "ShipmentId", shipmentId);
                            taskService.setVariableLocal(tmpChildTask.getId(), "ShipmentServiceId", shipmentServiceDetail.getId());
                            taskService.setVariableLocal(tmpChildTask.getId(), "Trade", ImportExport.Import.toString());
                            taskService.setVariableLocal(tmpChildTask.getId(), "Location", shipmentServiceDetail.getLocation().getId());
                            taskService.setVariableLocal(tmpChildTask.getId(), "Company", shipmentServiceDetail.getCompany().getId());
                            taskService.setVariableLocal(tmpChildTask.getId(), "groupName", ActivitiGroup.SignOff.toString());
                        }
                    }

                }

            }

        }


        if (shipmentServiceDetail.getShipmentServiceSignOff() != null
                && shipmentServiceDetail.getShipmentServiceSignOff().getIsSignOff() != null) {


            TaskQuery activitiQuery = taskService.createTaskQuery();
            activitiQuery.processInstanceId(shipmentServiceDetail.getProcessInstanceId());
            activitiQuery.orderByTaskCreateTime().desc();
            List<Task> childTaskList = activitiQuery.list();
            for (Task childTask : childTaskList) {

                if (childTask.getName().equals(ActivitiGroup.SignOff.toString())) {
                    Map<String, Object> variables = new HashMap<>();

                    variables.put("groupName", ActivitiGroup.CanAndInvoice.toString());
                    variables.put("ShipmentId", shipmentId);
                    variables.put("ShipmentServiceId", shipmentServiceDetail.getId());
                    variables.put("Trade", ImportExport.Import.toString());
                    variables.put("Location", shipmentServiceDetail.getLocation().getId());
                    variables.put("Company", shipmentServiceDetail.getCompany().getId());

                    taskService.complete(childTask.getId(), variables);

                    TaskQuery subActivitiQuery = taskService.createTaskQuery();
                    subActivitiQuery.processInstanceId(shipmentServiceDetail.getProcessInstanceId());
                    subActivitiQuery.orderByTaskCreateTime().desc();
                    List<Task> tmpChild = subActivitiQuery.list();
                    for (Task tmpChildTask : tmpChild) {

                        if (tmpChildTask.getName().equals(ActivitiGroup.CanAndInvoice.toString())) {
                            shipmentServiceDetail.setTaskId(tmpChildTask.getId());
                            shipmentServiceDetail.setProcessInstanceId(tmpChildTask.getProcessInstanceId());
                            taskService.setVariableLocal(tmpChildTask.getId(), "ShipmentId", shipmentId);
                            taskService.setVariableLocal(tmpChildTask.getId(), "ShipmentServiceId", shipmentServiceDetail.getId());
                            taskService.setVariableLocal(tmpChildTask.getId(), "Trade", ImportExport.Import.toString());
                            taskService.setVariableLocal(tmpChildTask.getId(), "Location", shipmentServiceDetail.getLocation().getId());
                            taskService.setVariableLocal(tmpChildTask.getId(), "Company", shipmentServiceDetail.getCompany().getId());
                            taskService.setVariableLocal(tmpChildTask.getId(), "groupName", ActivitiGroup.CanAndInvoice.toString());
                        }
                    }

                }

            }
        }

    }


    public void canAndInvoice(Long shipmentId, ShipmentServiceDetail shipmentServiceDetail) {


        if (!workFlowEnabled) {
            return;
        }

        List<InvoiceCreditNote> tmpList = invoiceRepo.findByShipmentServiceDetail(shipmentServiceDetail.getId(), DocumentType.INV);
        boolean invoice = false;
        if (tmpList == null || tmpList.size() == 0) {
            return;
        } else {
            invoice = true;
        }

        boolean can = false;
        for (DocumentDetail dd : shipmentServiceDetail.getDocumentList()) {
            if (dd.getCanNumber() != null) {
                can = true;
                break;
            }
        }

        if (!can) {
            return;
        }


        TaskQuery activitiQuery = taskService.createTaskQuery();
        activitiQuery.processInstanceId(shipmentServiceDetail.getProcessInstanceId());
        activitiQuery.orderByTaskCreateTime().desc();
        List<Task> childTaskList = activitiQuery.list();
        for (Task childTask : childTaskList) {

            if (childTask.getName().equals(ActivitiGroup.CanAndInvoice.toString())) {
                Map<String, Object> variables = new HashMap<>();

                variables.put("groupName", ActivitiGroup.Customs.toString());
                variables.put("ShipmentId", shipmentId);
                variables.put("ShipmentServiceId", shipmentServiceDetail.getId());
                variables.put("Trade", ImportExport.Import.toString());
                variables.put("Location", shipmentServiceDetail.getLocation().getId());
                variables.put("Company", shipmentServiceDetail.getCompany().getId());

                taskService.complete(childTask.getId(), variables);

                TaskQuery subActivitiQuery = taskService.createTaskQuery();
                subActivitiQuery.processInstanceId(shipmentServiceDetail.getProcessInstanceId());
                subActivitiQuery.orderByTaskCreateTime().desc();
                List<Task> tmpChild = subActivitiQuery.list();
                for (Task tmpChildTask : tmpChild) {

                    if (tmpChildTask.getName().equals(ActivitiGroup.Customs.toString())) {
                        shipmentServiceDetail.setTaskId(tmpChildTask.getId());
                        shipmentServiceDetail.setProcessInstanceId(tmpChildTask.getProcessInstanceId());
                        taskService.setVariableLocal(tmpChildTask.getId(), "ShipmentId", shipmentId);
                        taskService.setVariableLocal(tmpChildTask.getId(), "ShipmentServiceId", shipmentServiceDetail.getId());
                        taskService.setVariableLocal(tmpChildTask.getId(), "Trade", ImportExport.Import.toString());
                        taskService.setVariableLocal(tmpChildTask.getId(), "Location", shipmentServiceDetail.getLocation().getId());
                        taskService.setVariableLocal(tmpChildTask.getId(), "Company", shipmentServiceDetail.getCompany().getId());
                        taskService.setVariableLocal(tmpChildTask.getId(), "groupName", ActivitiGroup.Customs.toString());
                    }
                }

            }

        }

        if (can && invoice) {
            customs(shipmentId, shipmentServiceDetail);
        }

    }

    private void customs(Long shipmentId, ShipmentServiceDetail shipmentServiceDetail) {
        TaskQuery activitiQuery = taskService.createTaskQuery();
        activitiQuery.processInstanceId(shipmentServiceDetail.getProcessInstanceId());
        activitiQuery.orderByTaskCreateTime().desc();
        List<Task> childTaskList = activitiQuery.list();
        for (Task childTask : childTaskList) {

            if (childTask.getName().equals(ActivitiGroup.Customs.toString())) {
                Map<String, Object> variables = new HashMap<>();

                variables.put("groupName", ActivitiGroup.DeliveryOrder.toString());
                variables.put("ShipmentId", shipmentId);
                variables.put("ShipmentServiceId", shipmentServiceDetail.getId());
                variables.put("Trade", ImportExport.Import.toString());
                variables.put("Location", shipmentServiceDetail.getLocation().getId());
                variables.put("Company", shipmentServiceDetail.getCompany().getId());

                taskService.complete(childTask.getId(), variables);

                TaskQuery subActivitiQuery = taskService.createTaskQuery();
                subActivitiQuery.processInstanceId(shipmentServiceDetail.getProcessInstanceId());
                subActivitiQuery.orderByTaskCreateTime().desc();
                List<Task> tmpChild = subActivitiQuery.list();
                for (Task tmpChildTask : tmpChild) {

                    if (tmpChildTask.getName().equals(ActivitiGroup.DeliveryOrder.toString())) {
                        shipmentServiceDetail.setTaskId(tmpChildTask.getId());
                        shipmentServiceDetail.setProcessInstanceId(tmpChildTask.getProcessInstanceId());
                        taskService.setVariableLocal(tmpChildTask.getId(), "ShipmentId", shipmentId);
                        taskService.setVariableLocal(tmpChildTask.getId(), "ShipmentServiceId", shipmentServiceDetail.getId());
                        taskService.setVariableLocal(tmpChildTask.getId(), "Trade", ImportExport.Import.toString());
                        taskService.setVariableLocal(tmpChildTask.getId(), "Location", shipmentServiceDetail.getLocation().getId());
                        taskService.setVariableLocal(tmpChildTask.getId(), "Company", shipmentServiceDetail.getCompany().getId());
                        taskService.setVariableLocal(tmpChildTask.getId(), "groupName", ActivitiGroup.DeliveryOrder.toString());
                    }
                }

            }

        }


        boolean doTask = false;
        for (DocumentDetail dd : shipmentServiceDetail.getDocumentList()) {
            if (dd.getDoNumber() != null) {
                doTask = true;
                break;
            }
        }
        if (doTask) {
            deliveryOrder(shipmentId, shipmentServiceDetail);
        }

    }

    public void deliveryOrder(Long shipmentId, ShipmentServiceDetail shipmentServiceDetail) {

        boolean doTask = false;
        for (DocumentDetail dd : shipmentServiceDetail.getDocumentList()) {
            if (dd.getDoNumber() != null) {
                doTask = true;
                break;
            }
        }

        TaskQuery activitiQuery = taskService.createTaskQuery();
        activitiQuery.processInstanceId(shipmentServiceDetail.getProcessInstanceId());
        activitiQuery.orderByTaskCreateTime().desc();
        List<Task> childTaskList = activitiQuery.list();
        for (Task childTask : childTaskList) {

            if (childTask.getName().equals(ActivitiGroup.DeliveryOrder.toString())) {
                Map<String, Object> variables = new HashMap<>();

                variables.put("groupName", ActivitiGroup.Clearance.toString());
                variables.put("ShipmentId", shipmentId);
                variables.put("ShipmentServiceId", shipmentServiceDetail.getId());
                variables.put("Trade", ImportExport.Import.toString());
                variables.put("Location", shipmentServiceDetail.getLocation().getId());
                variables.put("Company", shipmentServiceDetail.getCompany().getId());

                taskService.complete(childTask.getId(), variables);

                TaskQuery subActivitiQuery = taskService.createTaskQuery();
                subActivitiQuery.processInstanceId(shipmentServiceDetail.getProcessInstanceId());
                subActivitiQuery.orderByTaskCreateTime().desc();
                List<Task> tmpChild = subActivitiQuery.list();
                for (Task tmpChildTask : tmpChild) {

                    if (tmpChildTask.getName().equals(ActivitiGroup.Clearance.toString())) {
                        shipmentServiceDetail.setTaskId(tmpChildTask.getId());
                        shipmentServiceDetail.setProcessInstanceId(tmpChildTask.getProcessInstanceId());
                        taskService.setVariableLocal(tmpChildTask.getId(), "ShipmentId", shipmentId);
                        taskService.setVariableLocal(tmpChildTask.getId(), "ShipmentServiceId", shipmentServiceDetail.getId());
                        taskService.setVariableLocal(tmpChildTask.getId(), "Trade", ImportExport.Import.toString());
                        taskService.setVariableLocal(tmpChildTask.getId(), "Location", shipmentServiceDetail.getLocation().getId());
                        taskService.setVariableLocal(tmpChildTask.getId(), "Company", shipmentServiceDetail.getCompany().getId());
                        taskService.setVariableLocal(tmpChildTask.getId(), "groupName", ActivitiGroup.Clearance.toString());
                    }
                }

            }

        }

        if (doTask) {
            if (shipmentServiceDetail.getIsClearance() != null && shipmentServiceDetail.getIsClearance() == YesNo.Yes) {
                Shipment shipment = shipmentRepository.findById(shipmentId);
                for (ShipmentServiceDetail sd : shipment.getShipmentServiceList()) {
                    if (activitiUtilService.isAirImportClearance(sd.getServiceMaster())) {
                        proofOfDelivery(shipmentId, shipmentServiceDetail);
                        break;
                    }
                }

            } else {
                proofOfDelivery(shipmentId, shipmentServiceDetail);

            }
        }


    }

    private void proofOfDelivery(Long shipmentId, ShipmentServiceDetail shipmentServiceDetail) {

        if (!workFlowEnabled) {
            return;
        }

        boolean clearanceTaskCompleted = false;


        if (shipmentServiceDetail.getIsClearance() != null && shipmentServiceDetail.getIsClearance() == YesNo.Yes) {
            Shipment shipment = shipmentRepository.findById(shipmentId);
            for (ShipmentServiceDetail sd : shipment.getShipmentServiceList()) {
                if (activitiUtilService.isAirImportClearance(sd.getServiceMaster())) {
                    clearanceTaskCompleted = true;
                    break;
                }
            }

        } else {
            clearanceTaskCompleted = true;

        }


        if (clearanceTaskCompleted) {
            TaskQuery activitiQuery = taskService.createTaskQuery();
            activitiQuery.processInstanceId(shipmentServiceDetail.getProcessInstanceId());
            activitiQuery.orderByTaskCreateTime().desc();
            List<Task> childTaskList = activitiQuery.list();
            for (Task childTask : childTaskList) {

                if (childTask.getName().equals(ActivitiGroup.Clearance.toString())) {
                    Map<String, Object> variables = new HashMap<>();

                    variables.put("groupName", ActivitiGroup.Pod.toString());
                    variables.put("ShipmentId", shipmentId);
                    variables.put("ShipmentServiceId", shipmentServiceDetail.getId());
                    variables.put("Trade", ImportExport.Import.toString());
                    variables.put("Location", shipmentServiceDetail.getLocation().getId());
                    variables.put("Company", shipmentServiceDetail.getCompany().getId());

                    taskService.complete(childTask.getId(), variables);

                    TaskQuery subActivitiQuery = taskService.createTaskQuery();
                    subActivitiQuery.processInstanceId(shipmentServiceDetail.getProcessInstanceId());
                    subActivitiQuery.orderByTaskCreateTime().desc();
                    List<Task> tmpChild = subActivitiQuery.list();
                    for (Task tmpChildTask : tmpChild) {

                        if (tmpChildTask.getName().equals(ActivitiGroup.Pod.toString())) {
                            shipmentServiceDetail.setTaskId(tmpChildTask.getId());
                            shipmentServiceDetail.setProcessInstanceId(tmpChildTask.getProcessInstanceId());
                            taskService.setVariableLocal(tmpChildTask.getId(), "ShipmentId", shipmentId);
                            taskService.setVariableLocal(tmpChildTask.getId(), "ShipmentServiceId", shipmentServiceDetail.getId());
                            taskService.setVariableLocal(tmpChildTask.getId(), "Trade", ImportExport.Import.toString());
                            taskService.setVariableLocal(tmpChildTask.getId(), "Location", shipmentServiceDetail.getLocation().getId());
                            taskService.setVariableLocal(tmpChildTask.getId(), "Company", shipmentServiceDetail.getCompany().getId());
                            taskService.setVariableLocal(tmpChildTask.getId(), "groupName", ActivitiGroup.Pod.toString());
                        }
                    }

                }

            }
        }

    }

    public void nominationLinkToMaster(Long shipmentId, ShipmentServiceDetail shipmentServiceDetail, ShipmentServiceDetail exportService) {

        if (!workFlowEnabled) {
            return;
        }

        TaskQuery activitiQuery = taskService.createTaskQuery();
        activitiQuery.processInstanceId(shipmentServiceDetail.getProcessInstanceId());
        activitiQuery.orderByTaskCreateTime().desc();
        List<Task> childTaskList = activitiQuery.list();
        for (Task childTask : childTaskList) {

            if (childTask.getName().equals(ActivitiGroup.LinkToMaster.toString())) {
                Map<String, Object> variables = new HashMap<>();

                variables.put("groupName", ActivitiGroup.AtaConfirmation.toString());
                variables.put("ShipmentId", shipmentId);
                variables.put("ShipmentServiceId", shipmentServiceDetail.getId());
                variables.put("Trade", ImportExport.Import.toString());
                variables.put("Location", shipmentServiceDetail.getLocation().getId());
                variables.put("Company", shipmentServiceDetail.getCompany().getId());

                taskService.complete(childTask.getId(), variables);


                TaskQuery subActivitiQuery = taskService.createTaskQuery();
                subActivitiQuery.processInstanceId(shipmentServiceDetail.getProcessInstanceId());
                subActivitiQuery.orderByTaskCreateTime().desc();
                List<Task> tmpChild = subActivitiQuery.list();
                for (Task tmpChildTask : tmpChild) {

                    if (tmpChildTask.getName().equals(ActivitiGroup.AtaConfirmation.toString())) {
                        shipmentServiceDetail.setTaskId(tmpChildTask.getId());
                        shipmentServiceDetail.setProcessInstanceId(tmpChildTask.getProcessInstanceId());
                        taskService.setVariableLocal(tmpChildTask.getId(), "ShipmentId", shipmentId);
                        taskService.setVariableLocal(tmpChildTask.getId(), "ShipmentServiceId", shipmentServiceDetail.getId());
                        taskService.setVariableLocal(tmpChildTask.getId(), "Trade", ImportExport.Import.toString());
                        taskService.setVariableLocal(tmpChildTask.getId(), "Location", shipmentServiceDetail.getLocation().getId());
                        taskService.setVariableLocal(tmpChildTask.getId(), "Company", shipmentServiceDetail.getCompany().getId());
                        taskService.setVariableLocal(tmpChildTask.getId(), "groupName", ActivitiGroup.AtaConfirmation.toString());
                    }
                }

            }

        }

    }

    public void exportAtdConfirmation(Long shipmentId, ShipmentServiceDetail shipmentServiceDetail, ShipmentServiceDetail exportService) {

        if (!workFlowEnabled) {
            return;
        }

        TaskQuery activitiQuery = taskService.createTaskQuery();
        activitiQuery.processInstanceId(shipmentServiceDetail.getProcessInstanceId());
        activitiQuery.orderByTaskCreateTime().desc();
        List<Task> childTaskList = activitiQuery.list();
        for (Task childTask : childTaskList) {

            if (childTask.getName().equals(ActivitiGroup.AtdConfirmation.toString())) {
                Map<String, Object> variables = new HashMap<>();

                variables.put("groupName", ActivitiGroup.AtaConfirmation.toString());
                variables.put("ShipmentId", shipmentId);
                variables.put("ShipmentServiceId", shipmentServiceDetail.getId());
                variables.put("Trade", ImportExport.Import.toString());
                variables.put("Location", shipmentServiceDetail.getLocation().getId());
                variables.put("Company", shipmentServiceDetail.getCompany().getId());

                taskService.complete(childTask.getId(), variables);


                TaskQuery subActivitiQuery = taskService.createTaskQuery();
                subActivitiQuery.processInstanceId(shipmentServiceDetail.getProcessInstanceId());
                subActivitiQuery.orderByTaskCreateTime().desc();
                List<Task> tmpChild = subActivitiQuery.list();
                for (Task tmpChildTask : tmpChild) {

                    if (tmpChildTask.getName().equals(ActivitiGroup.AtaConfirmation.toString())) {
                        shipmentServiceDetail.setTaskId(tmpChildTask.getId());
                        shipmentServiceDetail.setProcessInstanceId(tmpChildTask.getProcessInstanceId());
                        taskService.setVariableLocal(tmpChildTask.getId(), "ShipmentId", shipmentId);
                        taskService.setVariableLocal(tmpChildTask.getId(), "ShipmentServiceId", shipmentServiceDetail.getId());
                        taskService.setVariableLocal(tmpChildTask.getId(), "Trade", ImportExport.Import.toString());
                        taskService.setVariableLocal(tmpChildTask.getId(), "Location", shipmentServiceDetail.getLocation().getId());
                        taskService.setVariableLocal(tmpChildTask.getId(), "Company", shipmentServiceDetail.getCompany().getId());
                        taskService.setVariableLocal(tmpChildTask.getId(), "groupName", ActivitiGroup.AtaConfirmation.toString());
                    }
                }

            }

        }

    }

}
