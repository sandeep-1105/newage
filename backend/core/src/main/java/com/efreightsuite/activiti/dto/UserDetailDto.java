package com.efreightsuite.activiti.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * This class is used to transfer UserDetails
 *
 * @author user1
 */
@ToString
@JsonIgnoreProperties(ignoreUnknown = true)
public class UserDetailDto implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    /**
     * Unique id to identify the user
     */
    @Getter
    @Setter
    Long id;

    /**
     * User Id to identify the user uniquely
     */
    @Getter
    @Setter
    String userId;

    /**
     * Users name
     */
    @Getter
    @Setter
    String username;

    /**
     * active to check whether the user is active or not
     */
    @Setter
    @Getter
    Boolean active = true;

    /**
     * Users password is hashed and stored.
     */
    @Getter
    @Setter
    String password;

    /**
     * Group List of the user
     */
    @Getter
    @Setter
    List<GroupDto> groupList = new ArrayList<>();

    @Getter
    @Setter
    String firstName;

    @Getter
    @Setter
    String lastName;

}
