package com.efreightsuite.activiti;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.efreightsuite.activiti.dto.ActivitiSearchDto;
import com.efreightsuite.activiti.dto.AirExportTaskCountDto;
import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.EnquiryLogResponseDto;
import com.efreightsuite.dto.QuotationSearchResponseDto;
import com.efreightsuite.dto.SearchRespDto;
import com.efreightsuite.dto.ShipmentSearchResponseDto;
import com.efreightsuite.enumeration.ActivitiGroup;
import com.efreightsuite.enumeration.ImportExport;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.model.EnquiryDetail;
import com.efreightsuite.model.QuotationDetail;
import com.efreightsuite.model.ShipmentServiceDetail;
import com.efreightsuite.model.UserProfile;
import com.efreightsuite.model.WorkFlowGroup;
import com.efreightsuite.repository.EnquiryDetailRepository;
import com.efreightsuite.repository.QuotationDetailRepository;
import com.efreightsuite.repository.ShipmentServiceDetailRepository;
import com.efreightsuite.repository.UserProfileRepository;
import com.efreightsuite.service.AuthService;
import com.efreightsuite.util.AppUtil;
import lombok.extern.log4j.Log4j2;
import org.activiti.engine.TaskService;
import org.activiti.engine.task.Task;
import org.activiti.engine.task.TaskQuery;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import static com.efreightsuite.constants.Constants.*;

@Service
@Log4j2
public class ActivitiAirExportSearchService {

    @Autowired
    private
    AuthService authService;
    @Autowired
    private
    UserProfileRepository userProfileRepository;
    @Autowired
    private
    EnquiryDetailRepository enquiryDetailRepository;
    @Autowired
    private
    QuotationDetailRepository quotationDetailRepository;
    @Autowired
    private
    ShipmentServiceDetailRepository shipmentServiceDetailRepository;
    @Autowired
    private
    AppUtil appUtil;
    @Autowired
    private TaskService taskService;
    @Autowired
    private ActiviSearchFilter activiSearchFilter;

    public AirExportTaskCountDto getAirExportCount() {
        AirExportTaskCountDto countReposnse = new AirExportTaskCountDto();
        UserProfile userProfile = AuthService.getCurrentUser();
        log.info("UserProfile :: " + userProfile.getId());
        if (userProfile != null && userProfile.getId() != null && userProfile.getSelectedUserLocation() != null
                && userProfile.getSelectedUserLocation().getId() != null && userProfile.getSelectedCompany() != null
                && userProfile.getSelectedCompany().getId() != null) {

            List<WorkFlowGroup> workFlowGroupList = userProfileRepository
                    .findWorkFlowGroupListByUser(userProfile.getId());
            int totalTask = 0;
            if (workFlowGroupList != null && !workFlowGroupList.isEmpty()) {
                for (WorkFlowGroup wfg : workFlowGroupList) {
                    TaskQuery taskQuery = taskService.createTaskQuery();
                    taskQuery.taskVariableValueEquals(TRADE, ImportExport.Export.toString());
                    taskQuery.taskVariableValueEquals(LOCATION, userProfile.getSelectedUserLocation().getId());
                    taskQuery.taskVariableValueEquals(COMPANY, userProfile.getSelectedCompany().getId());
                    // taskQuery.taskName(wfg.getWorkFlowName().toString());

                    if (ActivitiGroup.AesOrCustoms.toString().equals(wfg.getWorkFlowName())) {
                        taskQuery.taskVariableValueEquals(AES_GROUP, wfg.getWorkFlowName());
                    } else if (ActivitiGroup.AirLineSubmission.toString().equals(wfg.getWorkFlowName())) {
                        taskQuery.taskVariableValueEquals(DIFF_GROUP, wfg.getWorkFlowName());
                    } else {
                        taskQuery.taskVariableValueEquals(GROUP_NAME, wfg.getWorkFlowName());
                    }

                    log.info("Location - " + userProfile.getSelectedUserLocation().getId().toString() + " ;; Company - "
                            + userProfile.getSelectedCompany().getId().toString() + " ; groupName - "
                            + wfg.getWorkFlowName());

                    int count = (int) taskQuery.count();
                    log.info("GroupName :: " + wfg.getWorkFlowName() + " ;; count -" + count);

                    totalTask = totalTask + count;

                    switch (wfg.getWorkFlowName().toLowerCase()) {
                        case ENQUIRY_LOWER_CASE:
                            countReposnse.setEnquiryCount(count);
                            break;
                        case QUOTATION_LOWER_CASE:
                            countReposnse.setQuotationCount(count);
                            break;
                        case SHIPMENT_LOWER_CASE:
                            countReposnse.setShipmentCount(count);
                            break;
                        case PICKUP_LOWER_CASE:
                            countReposnse.setPickUpCount(count);
                            break;
                        case DELIVERY_TO_CFS_LOWER_CASE:
                            countReposnse.setDeliverToCfsOrDepotCount(count);
                            break;
                        case AES_OR_CUSTOMS_LOWER_CASE:
                            countReposnse.setAesOrCustomsCount(count);
                            break;
                        case LINK_TO_MASTER_LOWER_CASE:
                            countReposnse.setLinkToMasterCount(count);
                            break;
                        case SIGN_OFF_LOWER_CASE:
                            countReposnse.setSignOffCount(count);
                            break;
                        case AIRLINE_SUB_LOWER_CASE:
                            countReposnse.setAirlIneSubmissionCount(count);
                            break;
                        case JOB_COMPLETION_LOWER_CASE:
                            countReposnse.setJobCompletionCount(count);
                            break;
                        case INVOICE_LOWER_CASE:
                            countReposnse.setInvoiceCount(count);
                            break;
                        case ATD_CONF_LOWER_CASE:
                            countReposnse.setAtdConfirmationCount(count);
                            break;
                    }

                }
            }
            countReposnse.setTotalTask(totalTask);

        }
        return countReposnse;
    }

    public BaseDto getAirExportTaskListByGroup(ActivitiSearchDto searchDto, String groupName) {

        log.info("getAirExportTaskListByGroup() method - [" + searchDto + "] groupName : " + groupName);

        BaseDto baseDto = new BaseDto();

        try {

            UserProfile userProfile = AuthService.getCurrentUser();
            if (userProfile.getId() == null) {
                userProfile = this.userProfileRepository.findByUserName(userProfile.getUserName());
            }

            if (userProfile.getId() != null
                    && userProfile.getSelectedUserLocation() != null
                    && userProfile.getSelectedUserLocation().getId() != null
                    && userProfile.getSelectedCompany() != null
                    && userProfile.getSelectedCompany().getId() != null) {

                List<WorkFlowGroup> workFlowGroupList = userProfileRepository.findWorkFlowGroupListByUser(userProfile.getId());


                if (workFlowGroupList != null && !workFlowGroupList.isEmpty()) {
                    log.info("List of WorkFlowGroup " + workFlowGroupList.size());

                    boolean canAccessGroup = false;

                    for (WorkFlowGroup wfg : workFlowGroupList) {
                        log.info("REQ Group - " + groupName + " ;; User GName - " + wfg.getWorkFlowName());
                        if (groupName.equals(wfg.getWorkFlowName())) {
                            canAccessGroup = true;
                            break;
                        }
                    }
                    if (canAccessGroup) {
                        TaskQuery taskQuery = taskService.createTaskQuery();
                        taskQuery.taskVariableValueEquals(TRADE, ImportExport.Export.toString());
                        taskQuery.taskVariableValueEquals(LOCATION, userProfile.getSelectedUserLocation().getId());
                        taskQuery.taskVariableValueEquals(COMPANY, userProfile.getSelectedCompany().getId());

                        if (ActivitiGroup.AesOrCustoms.toString().equals(groupName)) {
                            taskQuery.taskVariableValueEquals(AES_GROUP, groupName);
                        } else if (ActivitiGroup.AirLineSubmission.toString().equals(groupName)) {
                            taskQuery.taskVariableValueEquals(DIFF_GROUP, groupName);
                        } else {
                            taskQuery.taskVariableValueEquals(GROUP_NAME, groupName);
                        }


                        if (searchDto != null && searchDto.getKeyword() != null) {
                            switch (searchDto.getKeyword().toLowerCase()) {
                                case ENQUIRY_LOWER_CASE:
                                    taskQuery = activiSearchFilter.enquiryFilter(searchDto, taskQuery);
                                    break;
                                case QUOTATION_LOWER_CASE:
                                    taskQuery = activiSearchFilter.quotationFilter(searchDto, taskQuery);
                                    break;
                                case SHIPMENT_LOWER_CASE:
                                    taskQuery = activiSearchFilter.shipmentFilter(searchDto, taskQuery);
                                    break;
                                case PICKUP_LOWER_CASE:
                                    taskQuery = activiSearchFilter.pickupFilter(searchDto, taskQuery);
                                    break;
                                case DELIVERY_TO_CFS_LOWER_CASE:
                                    taskQuery = activiSearchFilter.cfsFilter(searchDto, taskQuery);
                                    break;
                                case LINK_TO_MASTER_LOWER_CASE:
                                    taskQuery = activiSearchFilter.linkingToMaster(searchDto, taskQuery);
                                    break;
                                case AES_OR_CUSTOMS_LOWER_CASE:
                                    taskQuery = activiSearchFilter.aesOrCustoms(searchDto, taskQuery);
                                    break;
                                case SIGN_OFF_LOWER_CASE:
                                    taskQuery = activiSearchFilter.signOff(searchDto, taskQuery);
                                    break;
                                case AIRLINE_SUB_LOWER_CASE:
                                    taskQuery = activiSearchFilter.airLineSubmission(searchDto, taskQuery);
                                    break;
                                case JOB_COMPLETION_LOWER_CASE:
                                    taskQuery = activiSearchFilter.jobCompletion(searchDto, taskQuery);
                                    break;
                                case INVOICE_LOWER_CASE:
                                    taskQuery = activiSearchFilter.invoice(searchDto, taskQuery);
                                    break;
                                case ATD_CONF_LOWER_CASE:
                                    taskQuery = activiSearchFilter.atdConfirmation(searchDto, taskQuery);
                                    break;
                            }
                        }
                        log.info("Task Query " + taskQuery.toString());
                        List<Task> taskList;
                        if (searchDto != null) {
                            taskList = taskQuery.listPage(searchDto.getSelectedPageNumber() * searchDto.getRecordPerPage(), searchDto.getRecordPerPage());
                        } else {
                            taskList = new ArrayList<>();
                        }
                        Long count = taskQuery.count();
                        log.info("Count " + count);

                        switch (groupName.toLowerCase()) {
                            case ENQUIRY_LOWER_CASE:
                                baseDto.setResponseObject(new SearchRespDto(count, getEnquiryLog(taskList)));
                                break;
                            case QUOTATION_LOWER_CASE:
                                baseDto.setResponseObject(new SearchRespDto(count, getQuotation(taskList)));
                                break;
                            case SHIPMENT_LOWER_CASE:
                                baseDto.setResponseObject(new SearchRespDto(count, getShipment(taskList)));
                                break;
                            case PICKUP_LOWER_CASE:
                                baseDto.setResponseObject(new SearchRespDto(count, getShipment(taskList)));
                                break;
                            case DELIVERY_TO_CFS_LOWER_CASE:
                                baseDto.setResponseObject(new SearchRespDto(count, getShipment(taskList)));
                                break;
                            case AES_OR_CUSTOMS_LOWER_CASE:
                                baseDto.setResponseObject(new SearchRespDto(count, getShipment(taskList)));
                                break;
                            case LINK_TO_MASTER_LOWER_CASE:
                                baseDto.setResponseObject(new SearchRespDto(count, getShipment(taskList)));
                                break;
                            case SIGN_OFF_LOWER_CASE:
                                baseDto.setResponseObject(new SearchRespDto(count, getShipment(taskList)));
                                break;
                            case AIRLINE_SUB_LOWER_CASE:
                                baseDto.setResponseObject(new SearchRespDto(count, getShipment(taskList)));
                                break;
                            case JOB_COMPLETION_LOWER_CASE:
                                baseDto.setResponseObject(new SearchRespDto(count, getShipment(taskList)));
                                break;
                            case INVOICE_LOWER_CASE:
                                baseDto.setResponseObject(new SearchRespDto(count, getShipment(taskList)));
                                break;
                            case ATD_CONF_LOWER_CASE:
                                baseDto.setResponseObject(new SearchRespDto(count, getShipment(taskList)));
                                break;
                        }

                    } else {
                        baseDto.setResponseObject(null);
                    }
                }
            }
            // baseDto.setResponseObject(carrierSearchImpl.search(searchDto));
            baseDto.setResponseCode(ErrorCode.SUCCESS);
        } catch (Exception exception) {

            log.info("Exception in Search method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);
    }

    private List<EnquiryLogResponseDto> getEnquiryLog(List<Task> taskList) {

        List<EnquiryLogResponseDto> listOfEnquiryDetail = new ArrayList<>();

        List<Long> idList = new ArrayList<>();
        for (Task task : taskList) {
            Map<String, Object> tmpObj = taskService.getVariables(task.getId());
            for (String key : tmpObj.keySet()) {
                if ("EnquiryAirExportId".equals(key)) {
                    idList.add((Long) tmpObj.get(key));
                }
            }
        }
        if (!idList.isEmpty()) {
            List<EnquiryDetail> tmpList = enquiryDetailRepository.findAllByIds(idList);

            for (EnquiryDetail ed : tmpList) {
                EnquiryLogResponseDto dto = new EnquiryLogResponseDto();
                if (ed.getEnquiryLog() != null) {
                    dto.setId(ed.getEnquiryLog().getId());

                    if (ed.getEnquiryLog().getPartyMaster() != null) {
                        dto.setCustomerName(ed.getEnquiryLog().getPartyMaster().getPartyName());
                        if (ed.getEnquiryLog().getPartyMaster().getGradeMaster() != null) {
                            dto.setCustomerColorCode(
                                    ed.getEnquiryLog().getPartyMaster().getGradeMaster().getColorCode());
                        }
                    } else {
                        if (ed.getEnquiryLog().getEnquiryCustomer() != null) {
                            dto.setCustomerName(ed.getEnquiryLog().getEnquiryCustomer().getName());
                        }
                    }

                    dto.setEnquiryNo(ed.getEnquiryLog().getEnquiryNo());

                    if (ed.getServiceMaster() != null) {
                        dto.setServiceName(ed.getServiceMaster().getServiceName());
                    }

                    if (ed.getFromPortMaster() != null) {
                        dto.setOriginName(ed.getFromPortMaster().getPortName());
                    }

                    if (ed.getToPortMaster() != null) {
                        dto.setDestinationName(ed.getToPortMaster().getPortName());
                    }

                    dto.setReceivedOn(ed.getEnquiryLog().getReceivedOn());
                    dto.setQuoteBy(ed.getEnquiryLog().getQuoteBy());

                    listOfEnquiryDetail.add(dto);
                }
            }
        }
        return listOfEnquiryDetail;
    }

    private List<QuotationSearchResponseDto> getQuotation(List<Task> taskList) {

        List<QuotationSearchResponseDto> listOfQuotationDetail = new ArrayList<>();

        log.info("QuotationId - ");
        List<Long> idList = new ArrayList<>();
        for (Task task : taskList) {
            Map<String, Object> tmpObj = taskService.getVariables(task.getId());
            for (String key : tmpObj.keySet()) {
                if ("QuotationAirExportId".equals(key)) {
                    idList.add((Long) tmpObj.get(key));
                }
            }
        }
        if (!idList.isEmpty()) {
            List<QuotationDetail> resultlist = quotationDetailRepository.findAllByIds(idList);
            for (QuotationDetail qd : resultlist) {
                /*
                 * qd.getQuotation().setQuotationDetailList(null);
				 * qd.getQuotation().setGeneralNoteList(null);
				 * qd.getQuotation().setQuotationAttachementList(null);
				 * qd.setQuotationDimensionList(null);
				 * qd.setQuotationChargeList(null);
				 * qd.setQuotationContainerList(null);
				 * qd.setServiceNoteList(null); qd.getQuotation().getCustomer();
				 * 
				 * 
				 * if(qd.getQuotation().getShipper()!=null){
				 * qd.getQuotation().getShipper(); }
				 * 
				 * 
				 * qd.getOrigin().setPortGroupMaster(null);
				 * qd.getOrigin().setEdiList(null);
				 * qd.getDestination().setPortGroupMaster(null);
				 * qd.getDestination().setEdiList(null);
				 * 
				 */

                QuotationSearchResponseDto dto = new QuotationSearchResponseDto();

                if (qd.getQuotation() != null) {
                    dto.setId(qd.getQuotation().getId());

                    if (qd.getQuotation().getCustomer() != null) {
                        dto.setCustomerName(qd.getQuotation().getCustomer().getPartyName());
                        if (qd.getQuotation().getCustomer().getGradeMaster() != null) {
                            dto.setCustomerColorCode(qd.getQuotation().getCustomer().getGradeMaster().getColorCode());
                        }
                    }

                    dto.setQuotationNo(qd.getQuotation().getQuotationNo());
                    dto.setExpiresOn(qd.getQuotation().getValidTo());
                    dto.setValidFrom(qd.getQuotation().getValidFrom());
                    if (qd.getQuotation().getSalesman() != null) {
                        dto.setSalesman(qd.getQuotation().getSalesman().getEmployeeName());
                    }
                    if (qd.getServiceMaster() != null) {
                        dto.setServiceName(qd.getServiceMaster().getServiceName());
                    }

                    if (qd.getOrigin() != null) {
                        dto.setOriginName(qd.getOrigin().getPortName());
                    }

                    if (qd.getDestination() != null) {
                        dto.setDestinationName(qd.getDestination().getPortName());
                    }

                    dto.setApproved(qd.getQuotation().getApproved());
                    dto.setQuoteType(qd.getQuotation().getQuoteType());

                }

                listOfQuotationDetail.add(dto);

            }
        }

        return listOfQuotationDetail;
    }

    private List<ShipmentSearchResponseDto> getShipment(List<Task> taskList) {
        log.info("getShipment - ");

        List<ShipmentSearchResponseDto> list = new ArrayList<>();
        List<Long> idList = new ArrayList<>();
        for (Task task : taskList) {

            Map<String, Object> tmpObj = taskService.getVariables(task.getId());
            for (String key : tmpObj.keySet()) {
                if ("ShipmentServiceId".equals(key)) {
                    idList.add((Long) tmpObj.get(key));

                }
            }

        }


        log.info("ID LIST " + idList);
        if (!idList.isEmpty()) {
            List<ShipmentServiceDetail> serviceList = shipmentServiceDetailRepository.findAllByIds(idList);

            for (ShipmentServiceDetail ssd : serviceList) {

                ShipmentSearchResponseDto dto = new ShipmentSearchResponseDto();

                dto.setId(ssd.getShipment().getId());
                if (ssd.getParty() != null) {
                    dto.setShipperName(ssd.getParty().getPartyName());
                    if (ssd.getShipper().getGradeMaster() != null) {
                        dto.setShipperColorCode(ssd.getShipper().getGradeMaster().getColorCode());
                    }
                }

                dto.setShipmentUid(ssd.getShipmentUid());
                dto.setShipmentReqDate(ssd.getShipment().getShipmentReqDate());
                if (ssd.getOrigin() != null) {
                    dto.setOriginName(ssd.getOrigin().getPortName());
                }

                if (ssd.getDestination() != null) {
                    dto.setDestinationName(ssd.getDestination().getPortName());
                }

                if (ssd.getCustomerService() != null) {
                    dto.setCustomerService(ssd.getCustomerService().getEmployeeName());
                }

                if (ssd.getPickUpDeliveryPoint() != null) {
                    dto.setIsOurPickUp(ssd.getPickUpDeliveryPoint().getIsOurPickUp());
                    dto.setPickUpFollowUp(ssd.getPickUpDeliveryPoint().getPickUpFollowUp());
                    dto.setPickUpActualUp(ssd.getPickUpDeliveryPoint().getPickUpActual());
                }

                dto.setEtd(ssd.getEtd());
                dto.setEta(ssd.getEta());
                if (ssd.getCarrier() != null) {
                    dto.setCarrierName(ssd.getCarrier().getCarrierName());
                }

                dto.setFlightNumber(ssd.getRouteNo());
                dto.setMawb(ssd.getMawbNo());
                dto.setConsolUid(ssd.getConsolUid());

                list.add(dto);
            }
        }

        return list;
    }

}
