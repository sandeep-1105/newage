package com.efreightsuite.activiti;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.efreightsuite.activiti.dto.ActivitiSearchDto;
import com.efreightsuite.activiti.dto.AirImportTaskCountDto;
import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.EnquiryLogResponseDto;
import com.efreightsuite.dto.QuotationSearchResponseDto;
import com.efreightsuite.dto.SearchRespDto;
import com.efreightsuite.dto.ShipmentSearchResponseDto;
import com.efreightsuite.enumeration.ActivitiGroup;
import com.efreightsuite.enumeration.ImportExport;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.model.EnquiryDetail;
import com.efreightsuite.model.QuotationDetail;
import com.efreightsuite.model.ShipmentServiceDetail;
import com.efreightsuite.model.UserProfile;
import com.efreightsuite.model.WorkFlowGroup;
import com.efreightsuite.repository.EnquiryDetailRepository;
import com.efreightsuite.repository.QuotationDetailRepository;
import com.efreightsuite.repository.ShipmentServiceDetailRepository;
import com.efreightsuite.repository.UserProfileRepository;
import com.efreightsuite.service.AuthService;
import com.efreightsuite.util.AppUtil;
import lombok.extern.log4j.Log4j2;
import org.activiti.engine.HistoryService;
import org.activiti.engine.IdentityService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.task.Task;
import org.activiti.engine.task.TaskQuery;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

/**
 * @author Achyutananda
 */
@Service
@Log4j2
public class ActivitiAirImportSearchService {

    @Autowired
    JdbcTemplate jdbcTemplate;
    @Autowired
    private
    AuthService authService;
    @Autowired
    private
    UserProfileRepository userProfileRepository;
    @Autowired
    private
    EnquiryDetailRepository enquiryDetailRepository;
    @Autowired
    private
    QuotationDetailRepository quotationDetailRepository;
    @Autowired
    private
    ShipmentServiceDetailRepository shipmentServiceDetailRepository;
    @Autowired
    private
    AppUtil appUtil;
    @Autowired
    private RuntimeService runtimeService;
    @Autowired
    private TaskService taskService;
    @Autowired
    private IdentityService identityService;
    @Autowired
    private HistoryService historyService;
    @Autowired
    private ActiviSearchFilter activiSearchFilter;

    public AirImportTaskCountDto getAirImportCount() {
        AirImportTaskCountDto countReposnse = new AirImportTaskCountDto();
        UserProfile userProfile = AuthService.getCurrentUser();
        log.info("UserProfile :: " + userProfile.getId());
        if (userProfile != null && userProfile.getId() != null && userProfile.getSelectedUserLocation() != null
                && userProfile.getSelectedUserLocation().getId() != null && userProfile.getSelectedCompany() != null
                && userProfile.getSelectedCompany().getId() != null) {

            List<WorkFlowGroup> workFlowGroupList = userProfileRepository.findWorkFlowGroupListByUser(userProfile.getId());
            int totalTask = 0;
            if (workFlowGroupList != null && !workFlowGroupList.isEmpty()) {
                for (WorkFlowGroup wfg : workFlowGroupList) {
                    TaskQuery taskQuery = taskService.createTaskQuery();
                    taskQuery.taskVariableValueEquals("Trade", ImportExport.Import.toString());
                    taskQuery.taskVariableValueEquals("Location", userProfile.getSelectedUserLocation().getId());
                    taskQuery.taskVariableValueEquals("Company", userProfile.getSelectedCompany().getId());

                    if (wfg.getWorkFlowName().equals(ActivitiGroup.AtdConfirmation.toString())) {
                        taskQuery.taskVariableValueEquals("importAtdGroup", wfg.getWorkFlowName());
                    } else {
                        taskQuery.taskVariableValueEquals("groupName", wfg.getWorkFlowName());
                    }

                    log.info("Location - " + userProfile.getSelectedUserLocation().getId().toString() + " ;; Company - " + userProfile.getSelectedCompany().getId().toString() + " ; groupName - " + wfg.getWorkFlowName());


                    int count = (int) taskQuery.count();
                    log.info("GroupName :: " + wfg.getWorkFlowName() + " ;; count -" + count);

                    totalTask = totalTask + count;

                    if (wfg.getWorkFlowName().equals(ActivitiGroup.Enquiry.toString())) {
                        countReposnse.setEnquiryCount(count);
                    } else if (wfg.getWorkFlowName().equals(ActivitiGroup.Quotation.toString())) {
                        countReposnse.setQuotationCount(count);
                    } else if (wfg.getWorkFlowName().equals(ActivitiGroup.Shipment.toString())) {
                        countReposnse.setShipmentCount(count);
                    } else if (wfg.getWorkFlowName().equals(ActivitiGroup.Pickup.toString())) {
                        countReposnse.setPickUpCount(count);
                    } else if (wfg.getWorkFlowName().equals(ActivitiGroup.DeliveryToCfs.toString())) {
                        countReposnse.setDeliverToCfsOrDepotCount(count);
                    } else if (wfg.getWorkFlowName().equals(ActivitiGroup.AtdConfirmation.toString())) {
                        countReposnse.setAtdConfirmationCount(count);
                    } else if (wfg.getWorkFlowName().equals(ActivitiGroup.LinkToMaster.toString())) {
                        countReposnse.setLinkToMasterCount(count);
                    } else if (wfg.getWorkFlowName().equals(ActivitiGroup.AtaConfirmation.toString())) {
                        countReposnse.setAtaConfirmationCount(count);
                    } else if (wfg.getWorkFlowName().equals(ActivitiGroup.SignOff.toString())) {
                        countReposnse.setSignOffCount(count);
                    } else if (wfg.getWorkFlowName().equals(ActivitiGroup.CanAndInvoice.toString())) {
                        countReposnse.setCanAndInvoiceCount(count);
                    } else if (wfg.getWorkFlowName().equals(ActivitiGroup.Customs.toString())) {
                        countReposnse.setCustomsCount(count);
                    } else if (wfg.getWorkFlowName().equals(ActivitiGroup.DeliveryOrder.toString())) {
                        countReposnse.setDeliveryOrderCount(count);
                    } else if (wfg.getWorkFlowName().equals(ActivitiGroup.Clearance.toString())) {
                        countReposnse.setClearanceCount(count);
                    } else if (wfg.getWorkFlowName().equals(ActivitiGroup.Pod.toString())) {
                        countReposnse.setPodCount(count);
                    }


                }
            }
            countReposnse.setTotalTask(totalTask);

        }
        return countReposnse;
    }


    public BaseDto getAirImportTaskListByGroup(ActivitiSearchDto searchDto, String groupName) {

        BaseDto baseDto = new BaseDto();

        try {
            UserProfile userProfile = AuthService.getCurrentUser();
            log.info("UserProfile :: " + userProfile.getId() + "userProfile.getSelectedUserLocation().getId() - " + userProfile.getSelectedUserLocation().getId() + " ; userProfile.getSelectedCompany().getId() - " + userProfile.getSelectedCompany().getId());
            if (userProfile != null && userProfile.getId() != null && userProfile.getSelectedUserLocation() != null
                    && userProfile.getSelectedUserLocation().getId() != null && userProfile.getSelectedCompany() != null
                    && userProfile.getSelectedCompany().getId() != null) {
                List<WorkFlowGroup> workFlowGroupList = userProfileRepository.findWorkFlowGroupListByUser(userProfile.getId());
                if (workFlowGroupList != null && !workFlowGroupList.isEmpty()) {
                    boolean canAccessGroup = false;
                    for (WorkFlowGroup wfg : workFlowGroupList) {
                        log.info("REQ Group - " + groupName + " ;; User GName - " + wfg.getWorkFlowName());
                        if (groupName.equals(wfg.getWorkFlowName())) {
                            canAccessGroup = true;
                            break;
                        }
                    }
                    if (canAccessGroup) {

                        TaskQuery taskQuery = taskService.createTaskQuery();
                        taskQuery.taskVariableValueEquals("Trade", ImportExport.Import.toString());
                        taskQuery.taskVariableValueEquals("Location", userProfile.getSelectedUserLocation().getId());
                        taskQuery.taskVariableValueEquals("Company", userProfile.getSelectedCompany().getId());

                        if (groupName.equals(ActivitiGroup.AtdConfirmation.toString())) {
                            taskQuery.taskVariableValueEquals("importAtdGroup", groupName);
                        } else {
                            taskQuery.taskVariableValueEquals("groupName", groupName);
                        }


                        if (searchDto != null && searchDto.getKeyword() != null) {

                            if (searchDto.getKeyword().equalsIgnoreCase("Enquiry")) {
                                taskQuery = activiSearchFilter.enquiryFilter(searchDto, taskQuery);
                            } else if (searchDto.getKeyword().equalsIgnoreCase("Quotation")) {
                                taskQuery = activiSearchFilter.quotationFilter(searchDto, taskQuery);
                            } else if (searchDto.getKeyword().equalsIgnoreCase("Shipment")) {
                                taskQuery = activiSearchFilter.shipmentFilter(searchDto, taskQuery);
                            } else if (searchDto.getKeyword().equalsIgnoreCase("pickup")) {
                                taskQuery = activiSearchFilter.pickupFilter(searchDto, taskQuery);
                            } else if (searchDto.getKeyword().equalsIgnoreCase("DeliveryToCfs")) {
                                taskQuery = activiSearchFilter.cfsFilter(searchDto, taskQuery);
                            } else if (searchDto.getKeyword().equalsIgnoreCase("AtdConfirmation")) {
                                taskQuery = activiSearchFilter.shipmentFilter(searchDto, taskQuery);
                            } else if (searchDto.getKeyword().equalsIgnoreCase("LinkToMaster")) {
                                taskQuery = activiSearchFilter.shipmentFilter(searchDto, taskQuery);
                            } else if (searchDto.getKeyword().equalsIgnoreCase("AtaConfirmation")) {
                                taskQuery = activiSearchFilter.importConsol(searchDto, taskQuery);
                            } else if (searchDto.getKeyword().equalsIgnoreCase("SignOff")) {
                                taskQuery = activiSearchFilter.shipmentFilter(searchDto, taskQuery);
                            } else if (searchDto.getKeyword().equalsIgnoreCase("CanAndInvoice")) {
                                taskQuery = activiSearchFilter.shipmentFilter(searchDto, taskQuery);
                            } else if (searchDto.getKeyword().equalsIgnoreCase("Customs")) {
                                taskQuery = activiSearchFilter.shipmentFilter(searchDto, taskQuery);
                            } else if (searchDto.getKeyword().equalsIgnoreCase("DeliveryOrder")) {
                                taskQuery = activiSearchFilter.importConsol(searchDto, taskQuery);
                            } else if (searchDto.getKeyword().equalsIgnoreCase("Clearance")) {
                                taskQuery = activiSearchFilter.shipmentFilter(searchDto, taskQuery);
                            } else if (searchDto.getKeyword().equalsIgnoreCase("Pod")) {
                                taskQuery = activiSearchFilter.shipmentFilter(searchDto, taskQuery);
                            }

                        }

                        log.info("Location - " + userProfile.getSelectedUserLocation().getId().toString() + " ;; Company - " + userProfile.getSelectedCompany().getId().toString() + " ; groupName - " + groupName);

                        List<Task> taskList = taskQuery.list();
                        Long count = taskQuery.count();

                        if (groupName.equals(ActivitiGroup.Enquiry.toString())) {
                            baseDto.setResponseObject(new SearchRespDto(count, getEnquiryLog(taskList)));
                        } else if (groupName.equals(ActivitiGroup.Quotation.toString())) {
                            baseDto.setResponseObject(new SearchRespDto(count, getQuotation(taskList)));
                        } else if (groupName.equals(ActivitiGroup.Shipment.toString())) {
                            baseDto.setResponseObject(new SearchRespDto(count, getShipment(taskList)));
                        } else if (groupName.equals(ActivitiGroup.Pickup.toString())) {
                            baseDto.setResponseObject(new SearchRespDto(count, getShipment(taskList)));
                        } else if (groupName.equals(ActivitiGroup.DeliveryToCfs.toString())) {
                            baseDto.setResponseObject(new SearchRespDto(count, getShipment(taskList)));
                        } else if (groupName.equals(ActivitiGroup.AtdConfirmation.toString())) {
                            baseDto.setResponseObject(new SearchRespDto(count, getShipment(taskList)));
                        } else if (groupName.equals(ActivitiGroup.LinkToMaster.toString())) {
                            baseDto.setResponseObject(new SearchRespDto(count, getShipment(taskList)));
                        } else if (groupName.equals(ActivitiGroup.AtaConfirmation.toString())) {
                            baseDto.setResponseObject(new SearchRespDto(count, getShipment(taskList)));
                        } else if (groupName.equals(ActivitiGroup.SignOff.toString())) {
                            baseDto.setResponseObject(new SearchRespDto(count, getShipment(taskList)));
                        } else if (groupName.equals(ActivitiGroup.CanAndInvoice.toString())) {
                            baseDto.setResponseObject(new SearchRespDto(count, getShipment(taskList)));
                        } else if (groupName.equals(ActivitiGroup.Customs.toString())) {
                            baseDto.setResponseObject(new SearchRespDto(count, getShipment(taskList)));
                        } else if (groupName.equals(ActivitiGroup.DeliveryOrder.toString())) {
                            baseDto.setResponseObject(new SearchRespDto(count, getShipment(taskList)));
                        } else if (groupName.equals(ActivitiGroup.Clearance.toString())) {
                            baseDto.setResponseObject(new SearchRespDto(count, getShipment(taskList)));
                        } else if (groupName.equals(ActivitiGroup.Pod.toString())) {
                            baseDto.setResponseObject(new SearchRespDto(count, getShipment(taskList)));
                        }

                    } else {
                        baseDto.setResponseObject(null);
                    }
                }
            }
//			baseDto.setResponseObject(carrierSearchImpl.search(searchDto));
            baseDto.setResponseCode(ErrorCode.SUCCESS);
            log.info("Successfully Searching Carrier...");

        } catch (Exception exception) {

            log.info("Exception in Search method : ", exception);

            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(baseDto);
    }


    private List<EnquiryLogResponseDto> getEnquiryLog(List<Task> taskList) {

        List<EnquiryLogResponseDto> listOfEnquiryDetail = new ArrayList<>();

        List<Long> idList = new ArrayList<>();
        for (Task task : taskList) {
            Map<String, Object> tmpObj = taskService.getVariables(task.getId());
            for (String key : tmpObj.keySet()) {
                if (key.equals("EnquiryAirImportId")) {
                    idList.add((Long) tmpObj.get(key));
                }
            }
        }
        if (!idList.isEmpty()) {
            if (!idList.isEmpty()) {
                List<EnquiryDetail> tmpList = enquiryDetailRepository.findAllByIds(idList);

                for (EnquiryDetail ed : tmpList) {
                    EnquiryLogResponseDto dto = new EnquiryLogResponseDto();
                    if (ed.getEnquiryLog() != null) {
                        dto.setId(ed.getEnquiryLog().getId());

                        if (ed.getEnquiryLog().getPartyMaster() != null) {
                            dto.setCustomerName(ed.getEnquiryLog().getPartyMaster().getPartyName());
                            if (ed.getEnquiryLog().getPartyMaster().getGradeMaster() != null) {
                                dto.setCustomerColorCode(
                                        ed.getEnquiryLog().getPartyMaster().getGradeMaster().getColorCode());
                            }
                        } else {
                            if (ed.getEnquiryLog().getEnquiryCustomer() != null) {
                                dto.setCustomerName(ed.getEnquiryLog().getEnquiryCustomer().getName());
                            }
                        }

                        dto.setEnquiryNo(ed.getEnquiryLog().getEnquiryNo());

                        if (ed.getServiceMaster() != null) {
                            dto.setServiceName(ed.getServiceMaster().getServiceName());
                        }

                        if (ed.getFromPortMaster() != null) {
                            dto.setOriginName(ed.getFromPortMaster().getPortName());
                        }

                        if (ed.getToPortMaster() != null) {
                            dto.setDestinationName(ed.getToPortMaster().getPortName());
                        }

                        dto.setReceivedOn(ed.getEnquiryLog().getReceivedOn());
                        dto.setQuoteBy(ed.getEnquiryLog().getQuoteBy());

                        listOfEnquiryDetail.add(dto);
                    }
                }
            }
        }
        return listOfEnquiryDetail;
    }


    private List<QuotationSearchResponseDto> getQuotation(List<Task> taskList) {
        List<QuotationSearchResponseDto> listOfQuotationDetail = new ArrayList<>();
        List<Long> idList = new ArrayList<>();
        for (Task task : taskList) {
            Map<String, Object> tmpObj = taskService.getVariables(task.getId());
            for (String key : tmpObj.keySet()) {
                if (key.equals("QuotationAirImportId")) {
                    idList.add((Long) tmpObj.get(key));
                }
            }
        }
        if (!idList.isEmpty()) {
            if (!idList.isEmpty()) {
                List<QuotationDetail> resultlist = quotationDetailRepository.findAllByIds(idList);
                for (QuotationDetail qd : resultlist) {
                    /*
    				 * qd.getQuotation().setQuotationDetailList(null);
    				 * qd.getQuotation().setGeneralNoteList(null);
    				 * qd.getQuotation().setQuotationAttachementList(null);
    				 * qd.setQuotationDimensionList(null);
    				 * qd.setQuotationChargeList(null);
    				 * qd.setQuotationContainerList(null);
    				 * qd.setServiceNoteList(null); qd.getQuotation().getCustomer();
    				 * 
    				 * 
    				 * if(qd.getQuotation().getShipper()!=null){
    				 * qd.getQuotation().getShipper(); }
    				 * 
    				 * 
    				 * qd.getOrigin().setPortGroupMaster(null);
    				 * qd.getOrigin().setEdiList(null);
    				 * qd.getDestination().setPortGroupMaster(null);
    				 * qd.getDestination().setEdiList(null);
    				 * 
    				 */

                    QuotationSearchResponseDto dto = new QuotationSearchResponseDto();

                    if (qd.getQuotation() != null) {
                        dto.setId(qd.getQuotation().getId());

                        if (qd.getQuotation().getCustomer() != null) {
                            dto.setCustomerName(qd.getQuotation().getCustomer().getPartyName());
                            if (qd.getQuotation().getCustomer().getGradeMaster() != null) {
                                dto.setCustomerColorCode(qd.getQuotation().getCustomer().getGradeMaster().getColorCode());
                            }
                        }

                        dto.setQuotationNo(qd.getQuotation().getQuotationNo());
                        dto.setExpiresOn(qd.getQuotation().getValidTo());
                        dto.setValidFrom(qd.getQuotation().getValidFrom());
                        if (qd.getQuotation().getSalesman() != null) {
                            dto.setSalesman(qd.getQuotation().getSalesman().getEmployeeName());
                        }
                        if (qd.getServiceMaster() != null) {
                            dto.setServiceName(qd.getServiceMaster().getServiceName());
                        }

                        if (qd.getOrigin() != null) {
                            dto.setOriginName(qd.getOrigin().getPortName());
                        }

                        if (qd.getDestination() != null) {
                            dto.setDestinationName(qd.getDestination().getPortName());
                        }

                        dto.setApproved(qd.getQuotation().getApproved());
                        dto.setQuoteType(qd.getQuotation().getQuoteType());

                    }

                    listOfQuotationDetail.add(dto);

                }
            }
        }

        return listOfQuotationDetail;

    }

    private List<ShipmentSearchResponseDto> getShipment(List<Task> taskList) {
        List<ShipmentSearchResponseDto> list = new ArrayList<>();
        List<Long> idList = new ArrayList<>();
        for (Task task : taskList) {
            Map<String, Object> tmpObj = taskService.getVariables(task.getId());
            for (String key : tmpObj.keySet()) {
                if (key.equals("ShipmentServiceId")) {
                    idList.add((Long) tmpObj.get(key));
                }
            }
        }
        if (!idList.isEmpty()) {
            List<ShipmentServiceDetail> serviceList = shipmentServiceDetailRepository.findAllByIds(idList);

            for (ShipmentServiceDetail ssd : serviceList) {
                ShipmentSearchResponseDto dto = new ShipmentSearchResponseDto();

                dto.setId(ssd.getShipment().getId());
                if (ssd.getParty() != null) {
                    dto.setShipperName(ssd.getParty().getPartyName());
                    if (ssd.getShipper().getGradeMaster() != null) {
                        dto.setShipperColorCode(ssd.getShipper().getGradeMaster().getColorCode());
                    }
                }

                dto.setShipmentUid(ssd.getShipmentUid());
                dto.setShipmentReqDate(ssd.getShipment().getShipmentReqDate());
                if (ssd.getOrigin() != null) {
                    dto.setOriginName(ssd.getOrigin().getPortName());
                }

                if (ssd.getDestination() != null) {
                    dto.setDestinationName(ssd.getDestination().getPortName());
                }

                if (ssd.getCustomerService() != null) {
                    dto.setCustomerService(ssd.getCustomerService().getEmployeeName());
                }

                if (ssd.getPickUpDeliveryPoint() != null) {
                    dto.setIsOurPickUp(ssd.getPickUpDeliveryPoint().getIsOurPickUp());
                    dto.setPickUpFollowUp(ssd.getPickUpDeliveryPoint().getPickUpFollowUp());
                    dto.setPickUpActualUp(ssd.getPickUpDeliveryPoint().getPickUpActual());
                }

                dto.setEtd(ssd.getEtd());
                dto.setEta(ssd.getEta());
                if (ssd.getCarrier() != null) {
                    dto.setCarrierName(ssd.getCarrier().getCarrierName());
                }

                dto.setFlightNumber(ssd.getRouteNo());
                dto.setMawb(ssd.getMawbNo());
                dto.setConsolUid(ssd.getConsolUid());

                list.add(dto);
            }
        }

        return list;
    }


}
