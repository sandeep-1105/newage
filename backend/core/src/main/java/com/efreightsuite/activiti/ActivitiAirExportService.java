package com.efreightsuite.activiti;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.efreightsuite.enumeration.ActivitiGroup;
import com.efreightsuite.enumeration.EventMasterType;
import com.efreightsuite.enumeration.ImportExport;
import com.efreightsuite.enumeration.YesNo;
import com.efreightsuite.model.*;
import com.efreightsuite.repository.*;
import com.efreightsuite.util.AppUtil;
import lombok.extern.log4j.Log4j2;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;
import org.activiti.engine.task.TaskQuery;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import static com.efreightsuite.constants.Constants.*;

@Service
@Log4j2
public class ActivitiAirExportService {

    private final boolean workFlowEnabled = true;

    private final String aesCountryCode = "US";

    @Autowired
    private
    AppUtil appUtil;
    @Autowired
    private
    EnquiryLogRepository enquiryLogRepository;
    @Autowired
    private
    QuotationRepository quotationRepository;
    @Autowired
    private
    ShipmentRepository shipmentRepository;
    @Autowired
    ShipmentServiceDetailRepository shipmentServiceDetailRepository;
    @Autowired
    private
    ActivitiUtilService activitiUtilService;
    @Autowired
    private
    ActivitiAirImportService activitiAirImportService;
    @Autowired
    private TaskService taskService;
    @Autowired
    private RuntimeService runtimeService;
    @Autowired
    private PartyMasterRepository partyMasterRepository;
    @Autowired
    private EmployeeMasterRepository employeeMasterRepository;

    @Autowired
    private ServiceMasterRepository serviceMasterRepository;

    @Autowired
    private PortMasterRepository portMasterRepository;



    private void activityTaskCreated() {
        activitiUtilService.checkAndCreateGroupName(ActivitiGroup.Enquiry);
        activitiUtilService.checkAndCreateGroupName(ActivitiGroup.Quotation);
        activitiUtilService.checkAndCreateGroupName(ActivitiGroup.Shipment);
        activitiUtilService.checkAndCreateGroupName(ActivitiGroup.Pickup);
        activitiUtilService.checkAndCreateGroupName(ActivitiGroup.DeliveryToCfs);
        activitiUtilService.checkAndCreateGroupName(ActivitiGroup.LinkToMaster);
        activitiUtilService.checkAndCreateGroupName(ActivitiGroup.AesOrCustoms);
        activitiUtilService.checkAndCreateGroupName(ActivitiGroup.SignOff);
        activitiUtilService.checkAndCreateGroupName(ActivitiGroup.AirLineSubmission);
        activitiUtilService.checkAndCreateGroupName(ActivitiGroup.JobCompletion);
        activitiUtilService.checkAndCreateGroupName(ActivitiGroup.Invoice);
        activitiUtilService.checkAndCreateGroupName(ActivitiGroup.AtdConfirmation);
        activitiUtilService.checkAndCreateGroupName(ActivitiGroup.AtaConfirmation);
        activitiUtilService.checkAndCreateGroupName(ActivitiGroup.DeliveryOrder);
        activitiUtilService.checkAndCreateGroupName(ActivitiGroup.Clearance);
        activitiUtilService.checkAndCreateGroupName(ActivitiGroup.Pod);
        activitiUtilService.checkAndCreateGroupName(ActivitiGroup.CanAndInvoice);
        activitiUtilService.checkAndCreateGroupName(ActivitiGroup.Customs);
    }

    public void createAirExportEnquiry(EnquiryLog enquiryLog, EnquiryDetail enquiryDetail) {

        if (!workFlowEnabled) {
            return;
        }

        activityTaskCreated();

        //Export Task Created
        Map<String, Object> vars = new HashMap<>();
        vars.put(FIRST_STATE, "1");
        vars.put(GROUP_NAME, ActivitiGroup.Enquiry.toString());
        vars.put(ENQUIRY_ID, enquiryLog.getId());
        vars.put(ENQUIRY_AIR_EXPORT_ID, enquiryDetail.getId());

        ProcessInstance processInstance = activitiUtilService.startAirExportBPMNProcess(vars);
        enquiryDetail.setProcessInstanceId(processInstance.getId());

        TaskQuery activitiQuery = taskService.createTaskQuery();
        activitiQuery.processInstanceId(processInstance.getId());
        activitiQuery.orderByTaskCreateTime().desc();
        List<Task> taskList = activitiQuery.list();
        for (Task task : taskList) {
            enquiryDetail.setTaskId(task.getId());
            taskService.setVariableLocal(task.getId(), ENQUIRY_ID, enquiryLog.getId());
            taskService.setVariableLocal(task.getId(), ENQUIRY_AIR_EXPORT_ID, enquiryDetail.getId());
            taskService.setVariableLocal(task.getId(), TRADE, ImportExport.Export.toString());
            taskService.setVariableLocal(task.getId(), LOCATION, enquiryLog.getLocationMaster().getId());
            taskService.setVariableLocal(task.getId(), COMPANY, enquiryLog.getCompanyMaster().getId());
            taskService.setVariableLocal(task.getId(), ENQUIRY_NO, enquiryLog.getEnquiryNo());
            taskService.setVariableLocal(task.getId(), GROUP_NAME, ActivitiGroup.Enquiry.toString());

            if (enquiryLog.getPartyMaster() != null) {
                taskService.setVariableLocal(task.getId(), CUSTOMER_NAME, enquiryLog.getPartyMaster().getPartyName());
            } else {
                taskService.setVariableLocal(task.getId(), CUSTOMER_NAME, enquiryLog.getEnquiryCustomer().getName());
            }

            if (enquiryDetail.getServiceMaster() != null) {
                taskService.setVariableLocal(task.getId(), SERVICE_NAME, enquiryDetail.getServiceMaster().getServiceName());
            }

            if (enquiryDetail.getFromPortMaster() != null) {
                taskService.setVariableLocal(task.getId(), ORIGIN_NAME, enquiryDetail.getFromPortMaster().getPortName());
            }

            if (enquiryDetail.getToPortMaster() != null) {
                taskService.setVariableLocal(task.getId(), DESTINATION_NAME, enquiryDetail.getToPortMaster().getPortName());
            }

            if (enquiryLog.getReceivedOn() != null) {
                log.info("Saving Received On Date" + enquiryLog.getReceivedOn());
                taskService.setVariableLocal(task.getId(), RECEIVED_ON_DATE, enquiryLog.getReceivedOn());
            }

            if (enquiryLog.getQuoteBy() != null) {
                log.info("Saving QuoteBy Date" + enquiryLog.getQuoteBy());
                taskService.setVariableLocal(task.getId(), QUOTE_BY_DATE, enquiryLog.getQuoteBy());
            }
        }
    }

    public void cancelAirExportEnquiry(EnquiryLog enquiryLog, EnquiryDetail enquiryDetail) {

        if (!workFlowEnabled) {
            return;
        }

        Map<String, Object> variables = new HashMap<>();

        variables.put(ENQUIRY_CANCELLED, true);
        taskService.complete(enquiryDetail.getTaskId(), variables);
        enquiryDetail.setWorkflowCompleted(YesNo.Yes);
    }

    public void createAirExportQuotation(Quotation quotation, QuotationDetail quotationDetail) {

        if (!workFlowEnabled) {
            return;
        }

        activityTaskCreated();

        if (quotationDetail.getProcessInstanceId() != null) {

            log.debug("From Enquiry To Quotation");

            Map<String, Object> variables = new HashMap<>();

            variables.put(ENQUIRY_CANCELLED, false);
            taskService.complete(quotationDetail.getTaskId(), variables);

            if (quotation.getEnquiryNo() != null) {
                EnquiryLog tmpObj = enquiryLogRepository.findByEnquiryNo(quotation.getEnquiryNo());
                for (EnquiryDetail ed : tmpObj.getEnquiryDetailList()) {

                    if (ed.getProcessInstanceId() != null
                            && ed.getProcessInstanceId().equals(quotationDetail.getProcessInstanceId()))

                        ed.setWorkflowCompleted(YesNo.Yes);

                }
            }

        } else {
            log.debug("Direct or Copy Quote Quotation");

            Map<String, Object> vars = new HashMap<>();
            vars.put(FIRST_STATE, "2");
            vars.put(GROUP_NAME, ActivitiGroup.Quotation.toString());
            vars.put(QUATATION_ID, quotation.getId());
            vars.put(QUATATION_AIR_EXPORT_ID, quotationDetail.getId());
            ProcessInstance processInstance = activitiUtilService.startAirExportBPMNProcess(vars);
            if (processInstance != null)
                quotationDetail.setProcessInstanceId(processInstance.getId());
        }

        TaskQuery activitiQuery = taskService.createTaskQuery();
        activitiQuery.processInstanceId(quotationDetail.getProcessInstanceId());
        activitiQuery.orderByTaskCreateTime().desc();
        for (Task task : activitiQuery.list()) {
            String taskId = task.getId();
            quotationDetail.setTaskId(task.getId());
            taskService.setVariableLocal(taskId, QUATATION_ID, quotation.getId());
            taskService.setVariableLocal(taskId, QUATATION_NO, quotation.getQuotationNo());
            taskService.setVariableLocal(taskId, QUATATION_AIR_EXPORT_ID, quotationDetail.getId());
            taskService.setVariableLocal(taskId, TRADE, ImportExport.Export.toString());
            taskService.setVariableLocal(taskId, LOCATION, (quotation.getLocationMaster() != null) ? quotation.getLocationMaster().getId() : null);
            taskService.setVariableLocal(taskId, COMPANY, (quotation.getCompanyMaster() != null) ? quotation.getCompanyMaster().getId() : null);
            taskService.setVariableLocal(taskId, GROUP_NAME, ActivitiGroup.Quotation.toString());

            if(quotation.getCustomer() != null ){
                PartyMaster partyMaster = partyMasterRepository.getOne(quotation.getCustomer().getId());
                taskService.setVariableLocal(task.getId(), CUSTOMER_NAME, partyMaster.getPartyName());
            }
            if (quotation.getValidTo() != null) {
                taskService.setVariableLocal(task.getId(), VALID_TO, quotation.getValidTo());
            }


            if (quotation.getSalesman() != null) {
                EmployeeMaster salesMan = employeeMasterRepository.findById(quotation.getSalesman().getId());
                taskService.setVariableLocal(task.getId(), SALES_MAN, salesMan.getEmployeeName());
            }


            if(quotationDetail.getServiceMaster() != null){
                ServiceMaster sm = serviceMasterRepository.getOne(quotationDetail.getServiceMaster().getId());
                taskService.setVariableLocal(task.getId(), SERVICE_NAME, sm.getServiceName());

            }
            if(quotationDetail.getOrigin() != null){
                PortMaster origin = portMasterRepository.findById(quotationDetail.getOrigin().getId());
                taskService.setVariableLocal(task.getId(), ORIGIN_NAME, origin.getPortName());
            }

            if(quotationDetail.getDestination() != null){
                PortMaster destination = portMasterRepository.findById(quotationDetail.getDestination().getId());
                taskService.setVariableLocal(task.getId(), DESTINATION_NAME, destination.getPortName());
            }


            if (quotation.getApproved() != null) {
                taskService.setVariableLocal(task.getId(), QUOTE_STATUS, quotation.getApproved());
            }
            if (quotation.getQuoteType() != null) {
                taskService.setVariableLocal(task.getId(), QUOTE_TYPE, quotation.getQuoteType());
            }
        }

    }

    public void cancelAirExportQuotation(Quotation quotation, QuotationDetail quotationDetail) {

        if (!workFlowEnabled) {
            return;
        }


        log.info("Cancell Quotation Log");

        Map<String, Object> variables = new HashMap<>();
        variables.put(QUOTATION_CANCELLED, true);
        taskService.complete(quotationDetail.getTaskId(), variables);
        quotationDetail.setWorkflowCompleted(YesNo.Yes);

    }

    public void createAirExportShipment(Long shipmentId, ShipmentServiceDetail shipmentServiceDetail) {

        activityTaskCreated();

        createShipment(shipmentId, shipmentServiceDetail);

        pickUp(shipmentId, shipmentServiceDetail);

        cfs(shipmentId, shipmentServiceDetail);

        cargoRecived(shipmentId, shipmentServiceDetail);

    }

    public void cancelAirExportShipment(ShipmentServiceDetail shipmentServiceDetail) {

        Map<String, Object> variables = new HashMap<>();
        variables.put(SHIPMENT_TASK_TO, "3");
        if (shipmentServiceDetail.getTaskId() != null) {
            taskService.complete(shipmentServiceDetail.getTaskId(), variables);
            shipmentServiceDetail.setWorkflowCompleted(YesNo.Yes);
        }
    }

    private void createShipment(Long shipmentId, ShipmentServiceDetail shipmentServiceDetail) {
        Task task = null;
        boolean taskCreated = false;
        if (shipmentServiceDetail.getProcessInstanceId() != null) {
            task = taskService.createTaskQuery().taskId(shipmentServiceDetail.getTaskId()).includeTaskLocalVariables().singleResult();

            if (task != null
                    && task.getTaskLocalVariables() != null
                    && task.getTaskLocalVariables().get(GROUP_NAME) != null
                    && task.getTaskLocalVariables().get(GROUP_NAME).equals(ActivitiGroup.Quotation.toString())) {


                if (shipmentServiceDetail.getQuotationUid() != null) {
                    Quotation tmpObj = quotationRepository.findByQuotationNo(shipmentServiceDetail.getQuotationUid());
                    for (QuotationDetail ed : tmpObj.getQuotationDetailList()) {

                        if (ed.getProcessInstanceId() != null
                                && ed.getProcessInstanceId().equals(shipmentServiceDetail.getProcessInstanceId())) {

                            ed.setWorkflowCompleted(YesNo.Yes);
                        }
                    }
                }

                Map<String, Object> variables = new HashMap<>();
                variables.put(GROUP_NAME, ActivitiGroup.Shipment.toString());
                variables.put(QUOTATION_CANCELLED, false);
                variables.put(CUSTOMER_NAME, shipmentServiceDetail.getParty() == null ? null : shipmentServiceDetail.getParty().getPartyName());
                variables.put(SHIPMENT_UID, shipmentServiceDetail.getShipmentUid());
                variables.put(SHIPMENT_DATE, shipmentServiceDetail.getShipment() == null ? null : shipmentServiceDetail.getShipment().getShipmentReqDate());
                variables.put(CUSTOMER_SERVICE, shipmentServiceDetail.getCustomerService() == null ? null : shipmentServiceDetail.getCustomerService().getEmployeeName());
                variables.put(ORIGIN, shipmentServiceDetail.getOrigin() == null ? null : shipmentServiceDetail.getOrigin().getPortName());
                variables.put(DESTINATION, shipmentServiceDetail.getDestination() == null ? null : shipmentServiceDetail.getDestination().getPortName());
                variables.put(SHIPMENT_SERVICE_ID, shipmentServiceDetail.getId());
                taskService.complete(shipmentServiceDetail.getTaskId(), variables);

                taskCreated = true;

            }
        } else {
            log.info("Hey I am Direct Shipment");


            Map<String, Object> vars = new HashMap<>();
            vars.put(FIRST_STATE, "3");
            vars.put(GROUP_NAME, ActivitiGroup.Shipment.toString());
            vars.put(SHIPMENT_ID, shipmentId);
            vars.put(SHIPMENT_SERVICE_ID, shipmentServiceDetail.getId());
            ProcessInstance processInstance = activitiUtilService.startAirExportBPMNProcess(vars);
            shipmentServiceDetail.setProcessInstanceId(processInstance.getId());

            vars.put(CUSTOMER_NAME, shipmentServiceDetail.getParty() == null ? null : shipmentServiceDetail.getParty().getPartyName());
            vars.put(SHIPMENT_UID, shipmentServiceDetail.getShipmentUid());
            vars.put(SHIPMENT_DATE, shipmentServiceDetail.getShipment() == null ? null : shipmentServiceDetail.getShipment().getShipmentReqDate());
            vars.put(CUSTOMER_SERVICE, shipmentServiceDetail.getCustomerService() == null ? null : shipmentServiceDetail.getCustomerService().getEmployeeName());
            vars.put(ORIGIN, shipmentServiceDetail.getOrigin() == null ? null : shipmentServiceDetail.getOrigin().getPortName());
            vars.put(DESTINATION, shipmentServiceDetail.getDestination() == null ? null : shipmentServiceDetail.getDestination().getPortName());


            taskCreated = true;


        }

        if (taskCreated) {
            TaskQuery activitiQuery = taskService.createTaskQuery();
            activitiQuery.processInstanceId(shipmentServiceDetail.getProcessInstanceId());
            activitiQuery.orderByTaskCreateTime().desc();
            for (Task childTask : activitiQuery.list()) {
                String childTaskId = childTask.getId();
                shipmentServiceDetail.setTaskId(childTaskId);
                shipmentServiceDetail.setProcessInstanceId(shipmentServiceDetail.getProcessInstanceId());
                taskService.setVariableLocal(childTaskId, SHIPMENT_ID, shipmentId);
                taskService.setVariableLocal(childTaskId, SHIPMENT_SERVICE_ID, shipmentServiceDetail.getId());
                taskService.setVariableLocal(childTaskId, TRADE, ImportExport.Export.toString());
                taskService.setVariableLocal(childTaskId, LOCATION, shipmentServiceDetail.getLocation().getId());
                taskService.setVariableLocal(childTaskId, COMPANY, shipmentServiceDetail.getCompany().getId());
                taskService.setVariableLocal(childTaskId, GROUP_NAME, ActivitiGroup.Shipment.toString());
                taskService.setVariableLocal(childTaskId, CUSTOMER_NAME, shipmentServiceDetail.getParty() == null ? null : shipmentServiceDetail.getParty().getPartyName());
                taskService.setVariableLocal(childTaskId, SHIPMENT_UID, shipmentServiceDetail.getShipmentUid());
                taskService.setVariableLocal(childTaskId, SHIPMENT_DATE, shipmentServiceDetail.getShipment() == null ? null : shipmentServiceDetail.getShipment().getShipmentReqDate());
                taskService.setVariableLocal(childTaskId, CUSTOMER_SERVICE, shipmentServiceDetail.getCustomerService() == null ? null : shipmentServiceDetail.getCustomerService().getEmployeeName());
                taskService.setVariableLocal(childTaskId, ORIGIN, shipmentServiceDetail.getOrigin() == null ? null : shipmentServiceDetail.getOrigin().getPortName());
                taskService.setVariableLocal(childTaskId, DESTINATION, shipmentServiceDetail.getDestination() == null ? null : shipmentServiceDetail.getDestination().getPortName());
            }
        }
    }


    private void pickUp(Long shipmentId, ShipmentServiceDetail shipmentServiceDetail) {
        boolean isOurPickUp = false;
        if (shipmentServiceDetail.getPickUpDeliveryPoint() != null && shipmentServiceDetail.getPickUpDeliveryPoint().getId() != null
                && shipmentServiceDetail.getPickUpDeliveryPoint().getIsOurPickUp() != null
                && shipmentServiceDetail.getPickUpDeliveryPoint().getIsOurPickUp().equals(YesNo.Yes)) {
            isOurPickUp = true;
        }

        Task task = taskService.createTaskQuery().taskId(shipmentServiceDetail.getTaskId()).includeTaskLocalVariables().singleResult();

        if (isOurPickUp && task != null && task.getTaskLocalVariables().get(GROUP_NAME) != null
                && task.getTaskLocalVariables().get(GROUP_NAME).equals(ActivitiGroup.Shipment.toString())) {


            Map<String, Object> variables = new HashMap<>();
            variables.put(SHIPMENT_TASK_TO, "1");
            variables.put(GROUP_NAME, ActivitiGroup.Pickup.toString());


            variables.put(CUSTOMER_NAME, shipmentServiceDetail.getParty() == null ? null : shipmentServiceDetail.getParty().getPartyName());
            variables.put(SHIPMENT_UID, shipmentServiceDetail.getShipmentUid());
            variables.put(SHIPMENT_DATE, shipmentServiceDetail.getServiceReqDate());
            variables.put(CUSTOMER_SERVICE, shipmentServiceDetail.getCustomerService() == null ? null : shipmentServiceDetail.getCustomerService().getEmployeeName());
            variables.put(ORIGIN, shipmentServiceDetail.getOrigin() == null ? null : shipmentServiceDetail.getOrigin().getPortName());
            variables.put(DESTINATION, shipmentServiceDetail.getDestination() == null ? null : shipmentServiceDetail.getDestination().getPortName());


            variables.put(OUR_PICK_UP, shipmentServiceDetail.getPickUpDeliveryPoint() == null ? null : shipmentServiceDetail.getPickUpDeliveryPoint().getIsOurPickUp());
            variables.put(PICK_UPDATE, shipmentServiceDetail.getPickUpDeliveryPoint() == null ? null : shipmentServiceDetail.getPickUpDeliveryPoint().getPickUpFollowUp());


            taskService.complete(shipmentServiceDetail.getTaskId(), variables);


            TaskQuery activitiQuery = taskService.createTaskQuery();
            activitiQuery.processInstanceId(shipmentServiceDetail.getProcessInstanceId());
            activitiQuery.orderByTaskCreateTime().desc();
            List<Task> childTaskList = activitiQuery.list();
            for (Task childTask : childTaskList) {
                String childTaskId = childTask.getId();
                shipmentServiceDetail.setTaskId(childTaskId);
                shipmentServiceDetail.setProcessInstanceId(shipmentServiceDetail.getProcessInstanceId());
                taskService.setVariableLocal(childTaskId, SHIPMENT_ID, shipmentId);
                taskService.setVariableLocal(childTaskId, SHIPMENT_SERVICE_ID, shipmentServiceDetail.getId());
                taskService.setVariableLocal(childTaskId, TRADE, ImportExport.Export.toString());
                taskService.setVariableLocal(childTaskId, LOCATION, shipmentServiceDetail.getLocation().getId());
                taskService.setVariableLocal(childTaskId, COMPANY, shipmentServiceDetail.getCompany().getId());
                taskService.setVariableLocal(childTaskId, GROUP_NAME, ActivitiGroup.Pickup.toString());

                taskService.setVariableLocal(childTaskId, CUSTOMER_NAME, shipmentServiceDetail.getParty() == null ? null : shipmentServiceDetail.getParty().getPartyName());
                taskService.setVariableLocal(childTaskId, SHIPMENT_UID, shipmentServiceDetail.getShipmentUid());
                taskService.setVariableLocal(childTaskId, SHIPMENT_DATE, shipmentServiceDetail.getServiceReqDate());
                taskService.setVariableLocal(childTaskId, CUSTOMER_SERVICE, shipmentServiceDetail.getCustomerService() == null ? null : shipmentServiceDetail.getCustomerService().getEmployeeName());
                taskService.setVariableLocal(childTaskId, ORIGIN, shipmentServiceDetail.getOrigin() == null ? null : shipmentServiceDetail.getOrigin().getPortName());
                taskService.setVariableLocal(childTaskId, DESTINATION, shipmentServiceDetail.getDestination() == null ? null : shipmentServiceDetail.getDestination().getPortName());


                taskService.setVariableLocal(childTaskId, OUR_PICK_UP, shipmentServiceDetail.getPickUpDeliveryPoint() == null ? null : shipmentServiceDetail.getPickUpDeliveryPoint().getIsOurPickUp());
                taskService.setVariableLocal(childTaskId, PICK_UPDATE, shipmentServiceDetail.getPickUpDeliveryPoint() == null ? null : shipmentServiceDetail.getPickUpDeliveryPoint().getPickUpFollowUp());
            }


            if (shipmentServiceDetail.getImportRef() != null) {
                ShipmentServiceDetail importService = shipmentRepository.getByService(shipmentServiceDetail.getImportRef());
                activitiAirImportService.pickUp(shipmentId, importService, shipmentServiceDetail);
            }

        }


    }


    private void cfs(Long shipmentId, ShipmentServiceDetail shipmentServiceDetail) {


        boolean isCfs = false;

        if (shipmentServiceDetail.getPickUpDeliveryPoint() == null) {
            return;
        }

        if (shipmentServiceDetail.getPickUpDeliveryPoint().getPickUpActual() != null) {
            isCfs = true;
        }

        Task task = taskService.createTaskQuery().taskId(shipmentServiceDetail.getTaskId()).includeTaskLocalVariables().singleResult();

        if (isCfs && task != null && task.getTaskLocalVariables().get(GROUP_NAME) != null
                && task.getTaskLocalVariables().get(GROUP_NAME).equals(ActivitiGroup.Pickup.toString())) {


            Map<String, Object> variables = new HashMap<>();
            variables.put(GROUP_NAME, ActivitiGroup.DeliveryToCfs.toString());
            variables.put(CUSTOMER_NAME, shipmentServiceDetail.getParty() == null ? null : shipmentServiceDetail.getParty().getPartyName());
            variables.put(SHIPMENT_UID, shipmentServiceDetail.getShipmentUid());
            variables.put(SHIPMENT_DATE, shipmentServiceDetail.getServiceReqDate());
            variables.put(CUSTOMER_SERVICE, shipmentServiceDetail.getCustomerService() == null ? null : shipmentServiceDetail.getCustomerService().getEmployeeName());
            variables.put(ORIGIN, shipmentServiceDetail.getOrigin() == null ? null : shipmentServiceDetail.getOrigin().getPortName());
            variables.put(DESTINATION, shipmentServiceDetail.getDestination() == null ? null : shipmentServiceDetail.getDestination().getPortName());

            variables.put(PICKUP_ACTUAL_DATE, shipmentServiceDetail.getPickUpDeliveryPoint() == null ? null : shipmentServiceDetail.getPickUpDeliveryPoint().getPickUpActual());

            taskService.complete(shipmentServiceDetail.getTaskId(), variables);


            TaskQuery activitiQuery = taskService.createTaskQuery();
            activitiQuery.processInstanceId(shipmentServiceDetail.getProcessInstanceId());
            activitiQuery.orderByTaskCreateTime().desc();
            List<Task> childTaskList = activitiQuery.list();
            for (Task childTask : childTaskList) {
                String childTaskId = childTask.getId();
                shipmentServiceDetail.setTaskId(childTaskId);
                shipmentServiceDetail.setProcessInstanceId(shipmentServiceDetail.getProcessInstanceId());
                taskService.setVariableLocal(childTaskId, SHIPMENT_ID, shipmentId);
                taskService.setVariableLocal(childTaskId, SHIPMENT_SERVICE_ID, shipmentServiceDetail.getId());
                taskService.setVariableLocal(childTaskId, TRADE, ImportExport.Export.toString());
                taskService.setVariableLocal(childTaskId, LOCATION, shipmentServiceDetail.getLocation().getId());
                taskService.setVariableLocal(childTaskId, COMPANY, shipmentServiceDetail.getCompany().getId());
                taskService.setVariableLocal(childTaskId, GROUP_NAME, ActivitiGroup.DeliveryToCfs.toString());

                taskService.setVariableLocal(childTaskId, CUSTOMER_NAME, shipmentServiceDetail.getParty() == null ? null : shipmentServiceDetail.getParty().getPartyName());
                taskService.setVariableLocal(childTaskId, SHIPMENT_UID, shipmentServiceDetail.getShipmentUid());
                taskService.setVariableLocal(childTaskId, SHIPMENT_DATE, shipmentServiceDetail.getServiceReqDate());
                taskService.setVariableLocal(childTaskId, CUSTOMER_SERVICE, shipmentServiceDetail.getCustomerService() == null ? null : shipmentServiceDetail.getCustomerService().getEmployeeName());
                taskService.setVariableLocal(childTaskId, ORIGIN, shipmentServiceDetail.getOrigin() == null ? null : shipmentServiceDetail.getOrigin().getPortName());
                taskService.setVariableLocal(childTaskId, DESTINATION, shipmentServiceDetail.getDestination() == null ? null : shipmentServiceDetail.getDestination().getPortName());

                taskService.setVariableLocal(childTaskId, PICKUP_ACTUAL_DATE, shipmentServiceDetail.getPickUpDeliveryPoint() == null ? null : shipmentServiceDetail.getPickUpDeliveryPoint().getPickUpActual());
            }

            if (shipmentServiceDetail.getImportRef() != null) {
                ShipmentServiceDetail importService = shipmentRepository.getByService(shipmentServiceDetail.getImportRef());
                activitiAirImportService.cfs(shipmentId, importService, shipmentServiceDetail);
            }

        }
    }


    private void cargoRecived(Long shipmentId, ShipmentServiceDetail shipmentServiceDetail) {
        Task task = taskService.createTaskQuery().taskId(shipmentServiceDetail.getTaskId()).includeTaskLocalVariables().singleResult();

        boolean isCargoReceived = false;
        boolean exportTaskCompleted = false;
        if (shipmentServiceDetail.getEventList() != null && !shipmentServiceDetail.getEventList().isEmpty()) {
            for (ShipmentServiceEvent se : shipmentServiceDetail.getEventList()) {
                if (se.getEventMaster() != null && se.getEventMaster().getEventMasterType() != null &&
                        se.getEventMaster().getEventMasterType().equals(EventMasterType.CARGO_RECEIVED)) {
                    isCargoReceived = true;
                    break;
                }
            }
        }


        if (task != null && isCargoReceived && task.getTaskLocalVariables().get(GROUP_NAME) != null
                && task.getTaskLocalVariables().get(GROUP_NAME).equals(ActivitiGroup.Shipment.toString())) {
            exportTaskCompleted = true;
            Map<String, Object> variables = new HashMap<>();

            variables.put(SHIPMENT_TASK_TO, "2");
            variables.put(GROUP_NAME, ActivitiGroup.LinkToMaster.toString());
            variables.put(AES_GROUP, ActivitiGroup.AesOrCustoms.toString());

            variables.put(PARENT_TASK, shipmentServiceDetail.getTaskId());

            variables.put(CUSTOMER_NAME, shipmentServiceDetail.getParty() == null ? null : shipmentServiceDetail.getParty().getPartyName());
            variables.put(SHIPMENT_UID, shipmentServiceDetail.getShipmentUid());
            variables.put(SHIPMENT_DATE, shipmentServiceDetail.getServiceReqDate());
            variables.put(CUSTOMER_SERVICE, shipmentServiceDetail.getCustomerService() == null ? null : shipmentServiceDetail.getCustomerService().getEmployeeName());
            variables.put(ORIGIN, shipmentServiceDetail.getOrigin() == null ? null : shipmentServiceDetail.getOrigin().getPortName());
            variables.put(DESTINATION, shipmentServiceDetail.getDestination() == null ? null : shipmentServiceDetail.getDestination().getPortName());

            variables.put(CARRIER, shipmentServiceDetail.getCarrier() == null ? null : shipmentServiceDetail.getCarrier().getCarrierName());
            variables.put(FLIGHT_NO, shipmentServiceDetail.getRouteNo());
            variables.put(MAWB, shipmentServiceDetail.getMawbNo());
            variables.put(ETD, shipmentServiceDetail.getEtd());
            variables.put(ETA, shipmentServiceDetail.getEta());

            taskService.complete(shipmentServiceDetail.getTaskId(), variables);
            TaskQuery subActivitiQuery = taskService.createTaskQuery();
            subActivitiQuery.processInstanceId(shipmentServiceDetail.getProcessInstanceId());

            subActivitiQuery.orderByTaskCreateTime().desc();
            List<Task> childTaskList = subActivitiQuery.list();
            for (Task childTask : childTaskList) {

                log.info("Before <<>> Task :: " + childTask.getName());
                String childTaskId = childTask.getId();
                shipmentServiceDetail.setTaskId(childTaskId);
                shipmentServiceDetail.setProcessInstanceId(childTask.getProcessInstanceId());
                taskService.setVariableLocal(childTaskId, SHIPMENT_ID, shipmentId);
                taskService.setVariableLocal(childTaskId, SHIPMENT_SERVICE_ID, shipmentServiceDetail.getId());
                taskService.setVariableLocal(childTaskId, TRADE, ImportExport.Export.toString());
                taskService.setVariableLocal(childTaskId, LOCATION, shipmentServiceDetail.getLocation().getId());
                taskService.setVariableLocal(childTaskId, COMPANY, shipmentServiceDetail.getCompany().getId());


                taskService.setVariableLocal(childTaskId, CUSTOMER_NAME, shipmentServiceDetail.getParty() == null ? null : shipmentServiceDetail.getParty().getPartyName());
                taskService.setVariableLocal(childTaskId, SHIPMENT_UID, shipmentServiceDetail.getShipmentUid());
                taskService.setVariableLocal(childTaskId, SHIPMENT_DATE, shipmentServiceDetail.getServiceReqDate());
                taskService.setVariableLocal(childTaskId, CUSTOMER_SERVICE, shipmentServiceDetail.getCustomerService() == null ? null : shipmentServiceDetail.getCustomerService().getEmployeeName());
                taskService.setVariableLocal(childTaskId, ORIGIN, shipmentServiceDetail.getOrigin() == null ? null : shipmentServiceDetail.getOrigin().getPortName());
                taskService.setVariableLocal(childTaskId, DESTINATION, shipmentServiceDetail.getDestination() == null ? null : shipmentServiceDetail.getDestination().getPortName());

                taskService.setVariableLocal(childTaskId, CARRIER, shipmentServiceDetail.getCarrier() == null ? null : shipmentServiceDetail.getCarrier().getCarrierName());
                taskService.setVariableLocal(childTaskId, FLIGHT_NO, shipmentServiceDetail.getRouteNo());
                taskService.setVariableLocal(childTaskId, MAWB, shipmentServiceDetail.getMawbNo());
                taskService.setVariableLocal(childTaskId, ETD, shipmentServiceDetail.getEtd());
                taskService.setVariableLocal(childTaskId, ETA, shipmentServiceDetail.getEta());


                if (childTask.getName().equals(ActivitiGroup.LinkToMaster.toString())) {
                    taskService.setVariableLocal(childTaskId, GROUP_NAME, ActivitiGroup.LinkToMaster.toString());
                } else {
                    taskService.setVariableLocal(childTaskId, AES_GROUP, ActivitiGroup.AesOrCustoms.toString());
                }

                if (childTask.getName().equals(ActivitiGroup.AesOrCustoms.toString())
                        && !shipmentServiceDetail.getOrigin().getPortGroupMaster().getCountryMaster().getCountryCode().toUpperCase().equals(aesCountryCode)) {
                    Map<String, Object> tmpVal = new HashMap<>();

                    tmpVal.put(SHIPMENT_ID, shipmentId);
                    tmpVal.put(SHIPMENT_SERVICE_ID, shipmentServiceDetail.getId());
                    tmpVal.put(TRADE, ImportExport.Export.toString());
                    tmpVal.put(LOCATION, shipmentServiceDetail.getLocation().getId());
                    tmpVal.put(COMPANY, shipmentServiceDetail.getCompany().getId());

                    tmpVal.put(CUSTOMER_NAME, shipmentServiceDetail.getParty() == null ? null : shipmentServiceDetail.getParty().getPartyName());
                    tmpVal.put(SHIPMENT_UID, shipmentServiceDetail.getShipmentUid());
                    tmpVal.put(SHIPMENT_DATE, shipmentServiceDetail.getServiceReqDate());
                    tmpVal.put(CUSTOMER_SERVICE, shipmentServiceDetail.getCustomerService() == null ? null : shipmentServiceDetail.getCustomerService().getEmployeeName());
                    tmpVal.put(ORIGIN, shipmentServiceDetail.getOrigin() == null ? null : shipmentServiceDetail.getOrigin().getPortName());
                    tmpVal.put(DESTINATION, shipmentServiceDetail.getDestination() == null ? null : shipmentServiceDetail.getDestination().getPortName());

                    tmpVal.put(CARRIER, shipmentServiceDetail.getCarrier() == null ? null : shipmentServiceDetail.getCarrier().getCarrierName());
                    tmpVal.put(FLIGHT_NO, shipmentServiceDetail.getRouteNo());
                    tmpVal.put(MAWB, shipmentServiceDetail.getMawbNo());
                    tmpVal.put(ETD, shipmentServiceDetail.getEtd());
                    tmpVal.put(ETA, shipmentServiceDetail.getEta());


                    taskService.complete(childTaskId, tmpVal);
                }
            }

        }


        if (task != null && isCargoReceived && task.getTaskLocalVariables() != null && task.getTaskLocalVariables().get(GROUP_NAME) != null
                && task.getTaskLocalVariables().get(GROUP_NAME).equals(ActivitiGroup.DeliveryToCfs.toString())) {
            exportTaskCompleted = true;
            Map<String, Object> variables = new HashMap<>();

            variables.put(SHIPMENT_TASK_TO, "2");
            variables.put(GROUP_NAME, ActivitiGroup.LinkToMaster.toString());
            variables.put(AES_GROUP, ActivitiGroup.AesOrCustoms.toString());

            variables.put(PARENT_TASK, shipmentServiceDetail.getTaskId());


            variables.put(CUSTOMER_NAME, shipmentServiceDetail.getParty() == null ? null : shipmentServiceDetail.getParty().getPartyName());
            variables.put(SHIPMENT_UID, shipmentServiceDetail.getShipmentUid());
            variables.put(SHIPMENT_DATE, shipmentServiceDetail.getServiceReqDate());
            variables.put(CUSTOMER_SERVICE, shipmentServiceDetail.getCustomerService() == null ? null : shipmentServiceDetail.getCustomerService().getEmployeeName());
            variables.put(ORIGIN, shipmentServiceDetail.getOrigin() == null ? null : shipmentServiceDetail.getOrigin().getPortName());
            variables.put(DESTINATION, shipmentServiceDetail.getDestination() == null ? null : shipmentServiceDetail.getDestination().getPortName());

            variables.put(CARRIER, shipmentServiceDetail.getCarrier() == null ? null : shipmentServiceDetail.getCarrier().getCarrierName());
            variables.put(FLIGHT_NO, shipmentServiceDetail.getRouteNo());
            variables.put(MAWB, shipmentServiceDetail.getMawbNo());
            variables.put(ETD, shipmentServiceDetail.getEtd());
            variables.put(ETA, shipmentServiceDetail.getEta());


            taskService.complete(shipmentServiceDetail.getTaskId(), variables);

            TaskQuery subActivitiQuery = taskService.createTaskQuery();
            subActivitiQuery.processInstanceId(shipmentServiceDetail.getProcessInstanceId());
            subActivitiQuery.orderByTaskCreateTime().desc();
            List<Task> childTaskList = subActivitiQuery.list();
            for (Task childTask : childTaskList) {
                String childTaskId = childTask.getId();
                shipmentServiceDetail.setTaskId(childTaskId);
                shipmentServiceDetail.setProcessInstanceId(childTask.getProcessInstanceId());
                taskService.setVariableLocal(childTaskId, SHIPMENT_ID, shipmentId);
                taskService.setVariableLocal(childTaskId, SHIPMENT_SERVICE_ID, shipmentServiceDetail.getId());
                taskService.setVariableLocal(childTaskId, TRADE, ImportExport.Export.toString());
                taskService.setVariableLocal(childTaskId, LOCATION, shipmentServiceDetail.getLocation().getId());
                taskService.setVariableLocal(childTaskId, COMPANY, shipmentServiceDetail.getCompany().getId());

                taskService.setVariableLocal(childTaskId, CUSTOMER_NAME, shipmentServiceDetail.getParty() == null ? null : shipmentServiceDetail.getParty().getPartyName());
                taskService.setVariableLocal(childTaskId, SHIPMENT_UID, shipmentServiceDetail.getShipmentUid());
                taskService.setVariableLocal(childTaskId, SHIPMENT_DATE, shipmentServiceDetail.getServiceReqDate());
                taskService.setVariableLocal(childTaskId, CUSTOMER_SERVICE, shipmentServiceDetail.getCustomerService() == null ? null : shipmentServiceDetail.getCustomerService().getEmployeeName());
                taskService.setVariableLocal(childTaskId, ORIGIN, shipmentServiceDetail.getOrigin() == null ? null : shipmentServiceDetail.getOrigin().getPortName());
                taskService.setVariableLocal(childTaskId, DESTINATION, shipmentServiceDetail.getDestination() == null ? null : shipmentServiceDetail.getDestination().getPortName());

                taskService.setVariableLocal(childTaskId, CARRIER, shipmentServiceDetail.getCarrier() == null ? null : shipmentServiceDetail.getCarrier().getCarrierName());
                taskService.setVariableLocal(childTaskId, FLIGHT_NO, shipmentServiceDetail.getRouteNo());
                taskService.setVariableLocal(childTaskId, MAWB, shipmentServiceDetail.getMawbNo());
                taskService.setVariableLocal(childTaskId, ETD, shipmentServiceDetail.getEtd());
                taskService.setVariableLocal(childTaskId, ETA, shipmentServiceDetail.getEta());


                if (childTask.getName().equals(ActivitiGroup.LinkToMaster.toString())) {
                    taskService.setVariableLocal(childTaskId, GROUP_NAME, ActivitiGroup.LinkToMaster.toString());
                } else {
                    taskService.setVariableLocal(childTaskId, AES_GROUP, ActivitiGroup.AesOrCustoms.toString());
                }


                if (childTask.getName().equals(ActivitiGroup.AesOrCustoms.toString())
                        && !shipmentServiceDetail.getOrigin().getPortGroupMaster().getCountryMaster().getCountryCode().toUpperCase().equals(aesCountryCode)) {
                    Map<String, Object> tmpVal = new HashMap<>();

                    tmpVal.put(SHIPMENT_ID, shipmentId);
                    tmpVal.put(SHIPMENT_SERVICE_ID, shipmentServiceDetail.getId());
                    tmpVal.put(TRADE, ImportExport.Export.toString());
                    tmpVal.put(LOCATION, shipmentServiceDetail.getLocation().getId());
                    tmpVal.put(COMPANY, shipmentServiceDetail.getCompany().getId());

                    tmpVal.put(CUSTOMER_NAME, shipmentServiceDetail.getParty() == null ? null : shipmentServiceDetail.getParty().getPartyName());
                    tmpVal.put(SHIPMENT_UID, shipmentServiceDetail.getShipmentUid());
                    tmpVal.put(SHIPMENT_DATE, shipmentServiceDetail.getServiceReqDate());
                    tmpVal.put(CUSTOMER_SERVICE, shipmentServiceDetail.getCustomerService() == null ? null : shipmentServiceDetail.getCustomerService().getEmployeeName());
                    tmpVal.put(ORIGIN, shipmentServiceDetail.getOrigin() == null ? null : shipmentServiceDetail.getOrigin().getPortName());
                    tmpVal.put(DESTINATION, shipmentServiceDetail.getDestination() == null ? null : shipmentServiceDetail.getDestination().getPortName());

                    tmpVal.put(CARRIER, shipmentServiceDetail.getCarrier() == null ? null : shipmentServiceDetail.getCarrier().getCarrierName());
                    tmpVal.put(FLIGHT_NO, shipmentServiceDetail.getRouteNo());
                    tmpVal.put(MAWB, shipmentServiceDetail.getMawbNo());
                    tmpVal.put(ETD, shipmentServiceDetail.getEtd());
                    tmpVal.put(ETA, shipmentServiceDetail.getEta());


                    taskService.complete(childTaskId, tmpVal);
                }

            }

        }

        if (isCargoReceived && exportTaskCompleted) {
            if (shipmentServiceDetail.getImportRef() != null) {
                ShipmentServiceDetail importService = shipmentRepository.getByService(shipmentServiceDetail.getImportRef());
                activitiAirImportService.atdConfirmation(shipmentId, importService, shipmentServiceDetail);
            }
        }
    }


    public void linkToMaster(Long shipmentId, Long consolId, ShipmentServiceDetail shipmentServiceDetail) {
        TaskQuery activitiQuery = taskService.createTaskQuery();
        activitiQuery.processInstanceId(shipmentServiceDetail.getProcessInstanceId());
        activitiQuery.orderByTaskCreateTime().desc();
        List<Task> childTaskList = activitiQuery.list();
        for (Task childTask : childTaskList) {

            if (childTask.getName().equals(ActivitiGroup.LinkToMaster.toString())) {
                Map<String, Object> variables = new HashMap<>();

                variables.put(GROUP_NAME, ActivitiGroup.SignOff.toString());
                variables.put(DIFF_GROUP, ActivitiGroup.AirLineSubmission.toString());
                variables.put(SHIPMENT_ID, shipmentId);
                variables.put(CONSOLE_ID, consolId);
                variables.put(SHIPMENT_SERVICE_ID, shipmentServiceDetail.getId());
                variables.put(TRADE, ImportExport.Export.toString());
                variables.put(LOCATION, shipmentServiceDetail.getLocation().getId());
                variables.put(COMPANY, shipmentServiceDetail.getCompany().getId());
                variables.put(CUSTOMER_NAME, shipmentServiceDetail.getParty() == null ? null : shipmentServiceDetail.getParty().getPartyName());
                taskService.complete(childTask.getId(), variables);

            }

        }

        linkedToMasterOrAesComplete(shipmentId, shipmentServiceDetail);


        if (shipmentServiceDetail.getImportRef() != null) {
            ShipmentServiceDetail importService = shipmentRepository.getByService(shipmentServiceDetail.getImportRef());
            activitiAirImportService.nominationLinkToMaster(shipmentId, importService, shipmentServiceDetail);
        }

    }


    public void createAirExportAesOrCustoms(Long shipmentId, ShipmentServiceDetail shipmentServiceDetail) {

        if (!workFlowEnabled) {
            return;
        }

        TaskQuery activitiQuery = taskService.createTaskQuery();
        activitiQuery.processInstanceId(shipmentServiceDetail.getProcessInstanceId());
        activitiQuery.orderByTaskCreateTime().desc();
        for (Task childTask : activitiQuery.list()) {

            if (ActivitiGroup.AesOrCustoms.toString().equals(childTask.getName())) {
                Map<String, Object> variables = new HashMap<>();

                variables.put(GROUP_NAME, ActivitiGroup.SignOff.toString());
                variables.put(DIFF_GROUP, ActivitiGroup.AirLineSubmission.toString());
                variables.put(SHIPMENT_ID, shipmentId);
                variables.put(SHIPMENT_SERVICE_ID, shipmentServiceDetail.getId());
                variables.put(TRADE, ImportExport.Export.toString());
                variables.put(CUSTOMER_NAME, shipmentServiceDetail.getParty() == null ? null : shipmentServiceDetail.getParty().getPartyName());
                variables.put(LOCATION, shipmentServiceDetail.getLocation().getId());
                variables.put(COMPANY, shipmentServiceDetail.getCompany().getId());
                taskService.complete(childTask.getId(), variables);
            }

        }

        linkedToMasterOrAesComplete(shipmentId, shipmentServiceDetail);

    }

    private void linkedToMasterOrAesComplete(Long shipmentId, ShipmentServiceDetail shipmentServiceDetail) {
        TaskQuery subActivitiQuery = taskService.createTaskQuery();
        subActivitiQuery.processInstanceId(shipmentServiceDetail.getProcessInstanceId());
        subActivitiQuery.orderByTaskCreateTime().desc();
        for (Task childTask : subActivitiQuery.list()) {

            if (ActivitiGroup.SignOff.toString().equals(childTask.getName())
                    || ActivitiGroup.AirLineSubmission.toString().equals(childTask.getName())) {

                String childTaskId = childTask.getId();
                shipmentServiceDetail.setTaskId(childTaskId);
                shipmentServiceDetail.setProcessInstanceId(childTask.getProcessInstanceId());
                taskService.setVariableLocal(childTaskId, SHIPMENT_ID, shipmentId);
                taskService.setVariableLocal(childTaskId, SHIPMENT_SERVICE_ID, shipmentServiceDetail.getId());
                taskService.setVariableLocal(childTaskId, TRADE, ImportExport.Export.toString());
                taskService.setVariableLocal(childTaskId, LOCATION, shipmentServiceDetail.getLocation().getId());
                taskService.setVariableLocal(childTaskId, COMPANY, shipmentServiceDetail.getCompany().getId());
                taskService.setVariableLocal(childTaskId, CUSTOMER_NAME, shipmentServiceDetail.getParty() == null ? null : shipmentServiceDetail.getParty().getPartyName());
                if (ActivitiGroup.SignOff.toString().equals(childTask.getName())) {
                    taskService.setVariableLocal(childTaskId, GROUP_NAME, ActivitiGroup.SignOff.toString());
                } else {
                    taskService.setVariableLocal(childTaskId, DIFF_GROUP, ActivitiGroup.AirLineSubmission.toString());
                }

                if (ActivitiGroup.SignOff.toString().equals(childTask.getName())
                        && appUtil.getLocationConfig("signoff.enabled", shipmentServiceDetail.getLocation(), true).equals("FALSE")) {

                    Map<String, Object> variables = new HashMap<>();

                    variables.put(GROUP_NAME, ActivitiGroup.JobCompletion.toString());
                    variables.put(SHIPMENT_ID, shipmentId);
                    variables.put(SHIPMENT_SERVICE_ID, shipmentServiceDetail.getId());
                    variables.put(TRADE, ImportExport.Export.toString());
                    variables.put(LOCATION, shipmentServiceDetail.getLocation().getId());
                    variables.put(COMPANY, shipmentServiceDetail.getCompany().getId());
                    variables.put(CUSTOMER_NAME, shipmentServiceDetail.getParty() == null ? null : shipmentServiceDetail.getParty().getPartyName());
                    taskService.complete(childTask.getId(), variables);


                }

            }
        }
    }


    public void createAirExportSignOff(Long shipmentId, ShipmentServiceDetail shipmentServiceDetail) {

        if (!workFlowEnabled) {
            return;
        }

        TaskQuery activitiQuery = taskService.createTaskQuery();
        activitiQuery.processInstanceId(shipmentServiceDetail.getProcessInstanceId());
        activitiQuery.orderByTaskCreateTime().desc();
        for (Task childTask : activitiQuery.list()) {

            if (childTask.getName().equals(ActivitiGroup.SignOff.toString())) {
                Map<String, Object> variables = new HashMap<>();

                variables.put(GROUP_NAME, ActivitiGroup.JobCompletion.toString());
                variables.put(SHIPMENT_ID, shipmentId);
                variables.put(SHIPMENT_SERVICE_ID, shipmentServiceDetail.getId());
                variables.put(CUSTOMER_NAME, shipmentServiceDetail.getParty() == null ? null : shipmentServiceDetail.getParty().getPartyName());
                variables.put(TRADE, ImportExport.Export.toString());
                variables.put(LOCATION, shipmentServiceDetail.getLocation().getId());
                variables.put(COMPANY, shipmentServiceDetail.getCompany().getId());

                taskService.complete(childTask.getId(), variables);

            }

        }

        sigOffOrAirlineSubmissionComplete(shipmentId, shipmentServiceDetail);

    }


    public void airlineSubmission(Long shipmentId, ShipmentServiceDetail shipmentServiceDetail) {

        if (!workFlowEnabled) {
            return;
        }

        TaskQuery activitiQuery = taskService.createTaskQuery();
        activitiQuery.processInstanceId(shipmentServiceDetail.getProcessInstanceId());
        activitiQuery.orderByTaskCreateTime().desc();
        for (Task childTask : activitiQuery.list()) {

            if (ActivitiGroup.AirLineSubmission.toString().equals(childTask.getName())) {
                Map<String, Object> variables = new HashMap<>();

                variables.put(GROUP_NAME, ActivitiGroup.JobCompletion.toString());
                variables.put(SHIPMENT_ID, shipmentId);
                variables.put(SHIPMENT_SERVICE_ID, shipmentServiceDetail.getId());
                variables.put(TRADE, ImportExport.Export.toString());
                variables.put(LOCATION, shipmentServiceDetail.getLocation().getId());
                variables.put(CUSTOMER_NAME, shipmentServiceDetail.getParty() == null ? null : shipmentServiceDetail.getParty().getPartyName());
                variables.put(COMPANY, shipmentServiceDetail.getCompany().getId());
                variables.put(CONSOLE_ID, shipmentServiceDetail.getConsolUid());
                taskService.complete(childTask.getId(), variables);

            }

        }

        sigOffOrAirlineSubmissionComplete(shipmentId, shipmentServiceDetail);
    }


    private void sigOffOrAirlineSubmissionComplete(Long shipmentId, ShipmentServiceDetail shipmentServiceDetail) {
        TaskQuery subActivitiQuery = taskService.createTaskQuery();
        subActivitiQuery.processInstanceId(shipmentServiceDetail.getProcessInstanceId());
        subActivitiQuery.orderByTaskCreateTime().desc();
        for (Task childTask : subActivitiQuery.list()) {
            if (ActivitiGroup.JobCompletion.toString().equals(childTask.getName())) {
                String childTaskId = childTask.getId();
                shipmentServiceDetail.setTaskId(childTaskId);
                shipmentServiceDetail.setProcessInstanceId(childTask.getProcessInstanceId());
                taskService.setVariableLocal(childTaskId, SHIPMENT_ID, shipmentId);
                taskService.setVariableLocal(childTaskId, SHIPMENT_SERVICE_ID, shipmentServiceDetail.getId());
                taskService.setVariableLocal(childTaskId, CUSTOMER_NAME, shipmentServiceDetail.getParty() == null ? null : shipmentServiceDetail.getParty().getPartyName());
                taskService.setVariableLocal(childTaskId, TRADE, ImportExport.Export.toString());
                taskService.setVariableLocal(childTaskId, LOCATION, shipmentServiceDetail.getLocation().getId());
                taskService.setVariableLocal(childTaskId, COMPANY, shipmentServiceDetail.getCompany().getId());
                taskService.setVariableLocal(childTaskId, GROUP_NAME, ActivitiGroup.JobCompletion.toString());
                taskService.setVariableLocal(childTaskId, CONSOLE_ID, shipmentServiceDetail.getConsolUid());
            }
        }
    }


    public void jobCompletion(Long shipmentId, ShipmentServiceDetail shipmentServiceDetail) {

        if (!workFlowEnabled) {
            return;
        }

        TaskQuery activitiQuery = taskService.createTaskQuery();
        activitiQuery.processInstanceId(shipmentServiceDetail.getProcessInstanceId());
        activitiQuery.orderByTaskCreateTime().desc();
        for (Task childTask : activitiQuery.list()) {

            if (ActivitiGroup.JobCompletion.toString().equals(childTask.getName())) {
                Map<String, Object> variables = new HashMap<>();

                variables.put(GROUP_NAME, ActivitiGroup.Invoice.toString());
                variables.put(SHIPMENT_ID, shipmentId);
                variables.put(SHIPMENT_SERVICE_ID, shipmentServiceDetail.getId());
                variables.put(CUSTOMER_NAME, shipmentServiceDetail.getParty() == null ? null : shipmentServiceDetail.getParty().getPartyName());
                variables.put(TRADE, ImportExport.Export.toString());
                variables.put(LOCATION, shipmentServiceDetail.getLocation().getId());
                variables.put(COMPANY, shipmentServiceDetail.getCompany().getId());

                taskService.complete(childTask.getId(), variables);

                TaskQuery subActivitiQuery = taskService.createTaskQuery();
                subActivitiQuery.processInstanceId(shipmentServiceDetail.getProcessInstanceId());
                subActivitiQuery.orderByTaskCreateTime().desc();
                for (Task tmpChildTask : subActivitiQuery.list()) {

                    if (ActivitiGroup.Invoice.toString().equals(tmpChildTask.getName())) {
                        String tmpChildTaskId = tmpChildTask.getId();
                        shipmentServiceDetail.setTaskId(tmpChildTaskId);
                        shipmentServiceDetail.setProcessInstanceId(tmpChildTask.getProcessInstanceId());
                        taskService.setVariableLocal(tmpChildTaskId, SHIPMENT_ID, shipmentId);
                        taskService.setVariableLocal(tmpChildTaskId, SHIPMENT_SERVICE_ID, shipmentServiceDetail.getId());
                        taskService.setVariableLocal(tmpChildTaskId, CUSTOMER_NAME, shipmentServiceDetail.getParty() == null ? null : shipmentServiceDetail.getParty().getPartyName());
                        taskService.setVariableLocal(tmpChildTaskId, TRADE, ImportExport.Export.toString());
                        taskService.setVariableLocal(tmpChildTaskId, LOCATION, shipmentServiceDetail.getLocation().getId());
                        taskService.setVariableLocal(tmpChildTaskId, COMPANY, shipmentServiceDetail.getCompany().getId());
                        taskService.setVariableLocal(tmpChildTaskId, GROUP_NAME, ActivitiGroup.Invoice.toString());
                    }
                }

            }

        }
    }

    public void createAirExportInvoice(Long shipmentId, ShipmentServiceDetail shipmentServiceDetail) {

        if (!workFlowEnabled) {
            return;
        }

        TaskQuery activitiQuery = taskService.createTaskQuery();
        activitiQuery.processInstanceId(shipmentServiceDetail.getProcessInstanceId());
        activitiQuery.orderByTaskCreateTime().desc();
        for (Task childTask : activitiQuery.list()) {

            if (ActivitiGroup.Invoice.toString().equals(childTask.getName())) {
                Map<String, Object> variables = new HashMap<>();

                variables.put(GROUP_NAME, ActivitiGroup.AtdConfirmation.toString());
                variables.put(SHIPMENT_ID, shipmentId);
                variables.put(SHIPMENT_SERVICE_ID, shipmentServiceDetail.getId());
                variables.put(TRADE, ImportExport.Export.toString());
                variables.put(LOCATION, shipmentServiceDetail.getLocation().getId());
                variables.put(COMPANY, shipmentServiceDetail.getCompany().getId());
                variables.put(CONSOLE_ID, shipmentServiceDetail.getConsolUid());


                taskService.complete(childTask.getId(), variables);

                TaskQuery subActivitiQuery = taskService.createTaskQuery();
                subActivitiQuery.processInstanceId(shipmentServiceDetail.getProcessInstanceId());
                subActivitiQuery.orderByTaskCreateTime().desc();
                for (Task tmpChildTask : subActivitiQuery.list()) {

                    if (ActivitiGroup.AtdConfirmation.toString().equals(tmpChildTask.getName())) {
                        String tmpChildTaskId = tmpChildTask.getId();
                        shipmentServiceDetail.setTaskId(tmpChildTaskId);
                        shipmentServiceDetail.setProcessInstanceId(tmpChildTask.getProcessInstanceId());
                        taskService.setVariableLocal(tmpChildTaskId, SHIPMENT_ID, shipmentId);
                        taskService.setVariableLocal(tmpChildTaskId, SHIPMENT_SERVICE_ID, shipmentServiceDetail.getId());
                        taskService.setVariableLocal(tmpChildTaskId, TRADE, ImportExport.Export.toString());
                        taskService.setVariableLocal(tmpChildTaskId, LOCATION, shipmentServiceDetail.getLocation().getId());
                        taskService.setVariableLocal(tmpChildTaskId, COMPANY, shipmentServiceDetail.getCompany().getId());
                        taskService.setVariableLocal(tmpChildTaskId, GROUP_NAME, ActivitiGroup.AtdConfirmation.toString());
                        taskService.setVariableLocal(tmpChildTaskId, CONSOLE_ID, shipmentServiceDetail.getConsolUid());
                    }
                }

            }

        }
    }


    public void atdConfirmation(Long shipmentId, ShipmentServiceDetail shipmentServiceDetail) {

        if (!workFlowEnabled) {
            return;
        }

        TaskQuery activitiQuery = taskService.createTaskQuery();
        activitiQuery.processInstanceId(shipmentServiceDetail.getProcessInstanceId());
        activitiQuery.orderByTaskCreateTime().desc();
        for (Task childTask : activitiQuery.list()) {
            if (ActivitiGroup.AtdConfirmation.toString().equals(childTask.getName())) {
                taskService.complete(childTask.getId(), new HashMap<>());
                shipmentServiceDetail.setWorkflowCompleted(YesNo.Yes);
            }

        }

        if (shipmentServiceDetail.getImportRef() != null) {
            ShipmentServiceDetail importService = shipmentRepository.getByService(shipmentServiceDetail.getImportRef());
            activitiAirImportService.exportAtdConfirmation(shipmentId, importService, shipmentServiceDetail);
        }


    }


}
