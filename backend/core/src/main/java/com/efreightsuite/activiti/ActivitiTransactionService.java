package com.efreightsuite.activiti;

import java.util.List;

import com.efreightsuite.enumeration.YesNo;
import com.efreightsuite.model.EnquiryDetail;
import com.efreightsuite.model.EnquiryLog;
import com.efreightsuite.model.Quotation;
import com.efreightsuite.model.QuotationDetail;
import com.efreightsuite.model.ServiceMaster;
import com.efreightsuite.model.Shipment;
import com.efreightsuite.model.ShipmentServiceDetail;
import com.efreightsuite.repository.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ActivitiTransactionService {

    private final boolean workFlowEnabled = true;

    @Autowired
    private
    EnquiryLogRepository enquiryLogRepository;

    @Autowired
    private
    QuotationRepository quotationRepository;

    @Autowired
    private
    ActivitiAirExportService activitiAirExportService;

    @Autowired
    private
    ActivitiAirImportService activitiAirImportService;

    @Autowired
    private
    ShipmentServiceDetailRepository shipmentServiceDetailRepository;

    @Autowired
    private
    ActivitiUtilService activitiUtilService;

    @Autowired
    private
    ServiceMasterRepository serviceMasterRepository;

    @Autowired
    private
    QuotationDetailRepository quotationDetailRepository;


    public EnquiryLog createEnquiryTask(EnquiryLog enquiryLog) {

        if (!workFlowEnabled) {
            return enquiryLog;
        }

        boolean needToSave = false;

        for (EnquiryDetail enquiryDetail : enquiryLog.getEnquiryDetailList()) {

            if (enquiryDetail.getProcessInstanceId() == null
                    && enquiryDetail.getTaskId() == null
                    && enquiryDetail.getServiceMaster() != null) {

                if (activitiUtilService.isAirExport(enquiryDetail.getServiceMaster(), enquiryDetail.getWorkflowCompleted())) {

                    needToSave = true;

                    activitiAirExportService.createAirExportEnquiry(enquiryLog, enquiryDetail);

                }

                if (activitiUtilService.isAirImport(enquiryDetail.getServiceMaster(), enquiryDetail.getWorkflowCompleted())) {

                    needToSave = true;

                    activitiAirImportService.createAirImportEnquiry(enquiryLog, enquiryDetail);
                }
            }

        }

        if (needToSave) {
            enquiryLog = enquiryLogRepository.save(enquiryLog);
        }

        return enquiryLog;

    }

    public void cancelEnquiry(EnquiryLog enquiryLog) {

        if (!workFlowEnabled) {
            return;
        }


        for (EnquiryDetail enquiryDetail : enquiryLog.getEnquiryDetailList()) {

            if (enquiryDetail.getProcessInstanceId() != null
                    && enquiryDetail.getTaskId() != null
                    && enquiryDetail.getServiceMaster() != null) {

                if (activitiUtilService.isAirExport(enquiryDetail.getServiceMaster(), enquiryDetail.getWorkflowCompleted())) {

                    activitiAirExportService.cancelAirExportEnquiry(enquiryLog, enquiryDetail);

                }

                if (activitiUtilService.isAirImport(enquiryDetail.getServiceMaster(), enquiryDetail.getWorkflowCompleted())) {

                    activitiAirImportService.cancelAirImportEnquiry(enquiryLog, enquiryDetail);
                }
            }
        }
    }

    public Quotation createQuotationTask(Quotation quotation) {

        if (!workFlowEnabled) {
            return quotation;
        }

        boolean needToSave = false;

        for (QuotationDetail quotatioonDetail : quotation.getQuotationDetailList()) {
            if (quotatioonDetail.getServiceMaster() != null) {
                ServiceMaster serviceMaster = serviceMasterRepository.getOne(quotatioonDetail.getServiceMaster().getId());
                if (activitiUtilService.isAirExport(serviceMaster, quotatioonDetail.getWorkflowCompleted())) {
                    needToSave = true;
                    activitiAirExportService.createAirExportQuotation(quotation, quotatioonDetail);
                }

                if (activitiUtilService.isAirImport(serviceMaster, quotatioonDetail.getWorkflowCompleted())) {
                    needToSave = true;
                    activitiAirImportService.createAirImportQuotation(quotation, quotatioonDetail);
                }
            }
        }


        if (needToSave) {
            quotation = quotationRepository.save(quotation);
        }

        return quotation;

    }

    public void cancelQuotation(Quotation quotation) {

        if (!workFlowEnabled) {
            return;
        }

        for (QuotationDetail quotationDetail : quotation.getQuotationDetailList()) {

            if (quotationDetail.getProcessInstanceId() != null
                    && quotationDetail.getTaskId() != null
                    && quotationDetail.getServiceMaster() != null) {

                if (activitiUtilService.isAirExport(quotationDetail.getServiceMaster(), quotationDetail.getWorkflowCompleted())) {
                    activitiAirExportService.cancelAirExportQuotation(quotation, quotationDetail);
                }

                if (activitiUtilService.isAirImport(quotationDetail.getServiceMaster(), quotationDetail.getWorkflowCompleted())) {
                    activitiAirImportService.cancelAirImportQuotation(quotation, quotationDetail);
                }
            }
        }

    }

    public Shipment createShipmentTask(Shipment shipment) {

        if (!workFlowEnabled) {
            return shipment;
        }

        for (ShipmentServiceDetail shipmentDetailService : shipment.getShipmentServiceList()) {
            Long shipmentId = shipment.getId();
            if(shipmentDetailService.getServiceMaster()!=null){
            ServiceMaster serviceMaster = serviceMasterRepository.getOne(shipmentDetailService.getServiceMaster().getId());
            if (activitiUtilService.isAirExport(serviceMaster, shipmentDetailService.getWorkflowCompleted())) {
                activitiAirExportService.createAirExportShipment(shipmentId, shipmentDetailService);
            }
            if (activitiUtilService.isAirImport(serviceMaster, shipmentDetailService.getWorkflowCompleted())) {
                activitiAirImportService.createAirImportShipment(shipmentId, shipmentDetailService);
            }
            }
        }

        return shipment;

    }

    public void cancelShipmentTask(ShipmentServiceDetail service) {

        if (!workFlowEnabled) {
            return;
        }

        if (activitiUtilService.isAirExport(service.getServiceMaster(), service.getWorkflowCompleted())) {
            activitiAirExportService.cancelAirExportShipment(service);
        }

        if (activitiUtilService.isAirImport(service.getServiceMaster(), service.getWorkflowCompleted())) {
            activitiAirImportService.cancelAirImportShipment(service);
        }

    }

    public void consolCreated(ServiceMaster serviceMaster, Long consolId, List<Long> serviceIds) {

        if (!workFlowEnabled) {
            return;
        }

        for (Long id : serviceIds) {

            ShipmentServiceDetail sd = shipmentServiceDetailRepository.findById(id);

            if (activitiUtilService.isAirImport(serviceMaster, sd.getWorkflowCompleted())) {

                if (activitiUtilService.isAirImport(serviceMaster, sd.getWorkflowCompleted())
                        && sd.getIsFreehandShipment() != null && sd.getIsFreehandShipment() == YesNo.Yes) {
                    activitiAirImportService.ataConfirmation(sd.getShipment().getId(), sd);
                } else if (activitiUtilService.isAirImport(serviceMaster, sd.getWorkflowCompleted())
                        && sd.getAutoImport() != null && sd.getAutoImport() == YesNo.Yes) {
                    if (sd.getTaskId() != null) {
                        activitiAirImportService.ataConfirmation(sd.getShipment().getId(), sd);
                    }
                } else if (activitiUtilService.isAirImport(serviceMaster, sd.getWorkflowCompleted())
                        && sd.getExportRef() != null) {
                    if (sd.getTaskId() != null) {
                        activitiAirImportService.ataConfirmation(sd.getShipment().getId(), sd);
                    }
                }


            } else if (activitiUtilService.isAirExport(serviceMaster, sd.getWorkflowCompleted())) {
                activitiAirExportService.linkToMaster(sd.getShipment().getId(), consolId, sd);
            }


        }


    }

    public void aesOrCustoms(Long shipmentId, ShipmentServiceDetail service) {

        if (!workFlowEnabled) {
            return;
        }

        if (activitiUtilService.isAirExport(service.getServiceMaster(), service.getWorkflowCompleted())) {
            activitiAirExportService.createAirExportAesOrCustoms(shipmentId, service);
        }

        if (activitiUtilService.isAirImport(service.getServiceMaster(), service.getWorkflowCompleted())) {

        }
    }

    public void signOff(Long shipmentId, ShipmentServiceDetail service) {

        if (!workFlowEnabled) {
            return;
        }

        if (activitiUtilService.isAirExport(service.getServiceMaster(), service.getWorkflowCompleted())) {
            activitiAirExportService.createAirExportSignOff(shipmentId, service);
        }

        if (activitiUtilService.isAirImport(service.getServiceMaster(), service.getWorkflowCompleted())) {
            activitiAirImportService.signOff(shipmentId, service);
        }

    }

    public void consolAirline(ServiceMaster serviceMaster, List<Long> serviceIds) {

        if (!workFlowEnabled) {
            return;
        }

        for (Long id : serviceIds) {
            ShipmentServiceDetail sd = shipmentServiceDetailRepository.findById(id);
            if (activitiUtilService.isAirExport(serviceMaster, sd.getWorkflowCompleted())) {
                activitiAirExportService.airlineSubmission(sd.getShipment().getId(), sd);
            }
        }

    }

    public void shipmentJobCompletion(ServiceMaster serviceMaster, List<Long> serviceIds) {

        if (!workFlowEnabled) {
            return;
        }

        for (Long id : serviceIds) {
            ShipmentServiceDetail sd = shipmentServiceDetailRepository.findById(id);
            if (activitiUtilService.isAirExport(serviceMaster, sd.getWorkflowCompleted())) {
                activitiAirExportService.jobCompletion(sd.getShipment().getId(), sd);
            }

        }

    }

    public void invoice(Long shipmentId, ShipmentServiceDetail service) {

        if (!workFlowEnabled) {
            return;
        }

        if (activitiUtilService.isAirExport(service.getServiceMaster(), service.getWorkflowCompleted())) {
            activitiAirExportService.createAirExportInvoice(shipmentId, service);
        }

        if (activitiUtilService.isAirImport(service.getServiceMaster(), service.getWorkflowCompleted())) {
            activitiAirImportService.canAndInvoice(shipmentId, service);
        }

    }

    public void shipmentAtd(ServiceMaster serviceMaster, List<Long> serviceIds) {

        if (!workFlowEnabled) {
            return;
        }

        for (Long id : serviceIds) {
            ShipmentServiceDetail sd = shipmentServiceDetailRepository.findById(id);
            if (activitiUtilService.isAirExport(serviceMaster, sd.getWorkflowCompleted())) {
                activitiAirExportService.atdConfirmation(sd.getShipment().getId(), sd);
            }
        }
    }

    public void cargoArrival(Long shipmentId, ShipmentServiceDetail service) {

        if (!workFlowEnabled) {
            return;
        }

        if (activitiUtilService.isAirImport(service.getServiceMaster(), service.getWorkflowCompleted())) {
            activitiAirImportService.canAndInvoice(shipmentId, service);
        }

    }


    public void doNumber(Long shipmentId, ShipmentServiceDetail service) {

        if (!workFlowEnabled) {
            return;
        }

        if (activitiUtilService.isAirImport(service.getServiceMaster(), service.getWorkflowCompleted())) {
            activitiAirImportService.deliveryOrder(shipmentId, service);
        }

    }
}
