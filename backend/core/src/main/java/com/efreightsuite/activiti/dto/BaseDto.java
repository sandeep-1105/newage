package com.efreightsuite.activiti.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * This dto provides errorCode that is sent to android pos
 *
 * @author user1
 */
@ToString
@JsonIgnoreProperties(ignoreUnknown = true)
public class BaseDto {

    /**
     * error code. 0 if success else unique error code value
     */
    @Getter
    @Setter
    int statusCode = 1000;
    @Getter
    @Setter
    String trackId;
    @Getter
    @Setter
    String errorDescription;

    public BaseDto() {

    }

    public BaseDto(int code) {
        this.statusCode = code;
    }

}

