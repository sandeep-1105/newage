package com.efreightsuite.activiti.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.ToString;

import java.io.Serializable;

@JsonIgnoreProperties(ignoreUnknown = true)
@ToString
public class AirExportImportTaskCountDto implements Serializable {

    private static final long serialVersionUID = 1L;

    AirImportTaskCountDto airImportTaskCountDto;

    AirExportTaskCountDto airExportTaskCountDto;

    public AirImportTaskCountDto getAirImportTaskCountDto() {
        return airImportTaskCountDto;
    }

    public AirExportTaskCountDto getAirExportTaskCountDto() {
        return airExportTaskCountDto;
    }

    public void setAirImportTaskCountDto(AirImportTaskCountDto airImportTaskCountDto) {
        this.airImportTaskCountDto = airImportTaskCountDto;
    }

    public void setAirExportTaskCountDto(AirExportTaskCountDto airExportTaskCountDto) {
        this.airExportTaskCountDto = airExportTaskCountDto;
    }
}
