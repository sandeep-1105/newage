package com.efreightsuite.activiti.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@JsonIgnoreProperties(ignoreUnknown = true)
@ToString
public class AirExportTaskCountDto implements Serializable {

    private static final long serialVersionUID = 1L;

    @Getter
    @Setter
    private int totalTask = 0;


    @Getter
    @Setter
    private int enquiryCount = 0;

    @Getter
    @Setter
    private int quotationCount = 0;

    @Getter
    @Setter
    private int shipmentCount = 0;

    @Getter
    @Setter
    private int shipmentYetToCargoReceiveCount = 0;

    @Getter
    @Setter
    private int pickUpCount = 0;

    @Getter
    @Setter
    private int deliverToCfsOrDepotCount = 0;

    @Getter
    @Setter
    private int aesOrCustomsCount = 0;

    @Getter
    @Setter
    private int linkToMasterCount = 0;

    @Getter
    @Setter
    private int signOffCount = 0;

    @Getter
    @Setter
    private int airlIneSubmissionCount = 0;

    @Getter
    @Setter
    private int jobCompletionCount = 0;

    @Getter
    @Setter
    private int invoiceCount = 0;

    @Getter
    @Setter
    private int atdConfirmationCount = 0;
}
