package com.efreightsuite.activiti;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.efreightsuite.activiti.dto.ActivitiSearchDto;
import lombok.extern.log4j.Log4j2;
import org.activiti.engine.task.TaskQuery;

import org.springframework.stereotype.Service;


@Service
@Log4j2
public class ActiviSearchFilter {

    private final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:SS");

    public TaskQuery enquiryFilter(ActivitiSearchDto searchDto, TaskQuery taskQuery) {
        if (searchDto.getSearchCustomerName() != null && searchDto.getSearchCustomerName().trim().length() > 0) {
            taskQuery = taskQuery.taskVariableValueLikeIgnoreCase("customerName", searchDto.getSearchCustomerName() + "%");
        }

        if (searchDto.getSearchEnquiryNo() != null && searchDto.getSearchEnquiryNo().trim().length() > 0) {
            taskQuery = taskQuery.or();
            taskQuery = taskQuery.taskVariableValueLikeIgnoreCase("EnquiryNo", searchDto.getSearchEnquiryNo() + "%");
            taskQuery = taskQuery.endOr();
        }

        if (searchDto.getSearchServiceName() != null && searchDto.getSearchServiceName().trim().length() > 0) {
            taskQuery = taskQuery.or();
            taskQuery = taskQuery.taskVariableValueLikeIgnoreCase("serviceName", searchDto.getSearchServiceName() + "%");
            taskQuery = taskQuery.endOr();
        }


        if (searchDto.getSearchOriginName() != null && searchDto.getSearchOriginName().trim().length() > 0) {
            taskQuery = taskQuery.or();
            taskQuery = taskQuery.taskVariableValueLikeIgnoreCase("originName", searchDto.getSearchOriginName() + "%");
            taskQuery = taskQuery.endOr();
        }

        if (searchDto.getSearchDestinationName() != null && searchDto.getSearchDestinationName().trim().length() > 0) {
            taskQuery = taskQuery.or();
            taskQuery = taskQuery.taskVariableValueLikeIgnoreCase("destinationName", searchDto.getSearchDestinationName() + "%");
            taskQuery = taskQuery.endOr();
        }


        if (searchDto.getSearchReceivedOn() != null && searchDto.getSearchReceivedOn().getStartDate() != null && searchDto.getSearchReceivedOn().getEndDate() != null) {

            try {
                Date dt1 = sdf.parse(searchDto.getSearchReceivedOn().getStartDate());
                Date dt2 = sdf.parse(searchDto.getSearchReceivedOn().getEndDate());

                taskQuery = taskQuery.or();
                taskQuery = taskQuery.taskVariableValueGreaterThanOrEqual("receivedOnDate", dt1);
                taskQuery = taskQuery.taskVariableValueLessThanOrEqual("receivedOnDate", dt2);
                taskQuery = taskQuery.endOr();

            } catch (ParseException e) {
                log.error("Date Conversion error in Enquiry Search ReceivedOnDate  ", e);
            }
        }

        if (searchDto.getSearchQuoteBy() != null && searchDto.getSearchQuoteBy().getStartDate() != null && searchDto.getSearchQuoteBy().getEndDate() != null) {

            try {
                Date dt1 = sdf.parse(searchDto.getSearchQuoteBy().getStartDate());
                Date dt2 = sdf.parse(searchDto.getSearchQuoteBy().getEndDate());

                taskQuery = taskQuery.or();
                taskQuery = taskQuery.taskVariableValueGreaterThanOrEqual("quoteByDate", dt1);
                taskQuery = taskQuery.taskVariableValueLessThanOrEqual("quoteByDate", dt2);
                taskQuery = taskQuery.endOr();

            } catch (ParseException e) {
                log.error("Date Conversion error in Enquiry Search QuoteBy  ", e);
            }
        }
        return taskQuery;
    }


    public TaskQuery quotationFilter(ActivitiSearchDto searchDto, TaskQuery taskQuery) {

        if (searchDto.getSearchCustomerName() != null && searchDto.getSearchCustomerName().trim().length() > 0) {
            taskQuery = taskQuery.taskVariableValueLikeIgnoreCase("customerName", searchDto.getSearchCustomerName() + "%");
        }

        if (searchDto.getSearchQuotationNo() != null && searchDto.getSearchQuotationNo().trim().length() > 0) {
            taskQuery = taskQuery.or();
            taskQuery = taskQuery.taskVariableValueLikeIgnoreCase("QuotationNo", searchDto.getSearchQuotationNo() + "%");
            taskQuery = taskQuery.endOr();
        }


        if (searchDto.getSearchValidTo() != null && searchDto.getSearchValidTo().getStartDate() != null && searchDto.getSearchValidTo().getEndDate() != null) {

            try {
                Date dt1 = sdf.parse(searchDto.getSearchValidTo().getStartDate());
                Date dt2 = sdf.parse(searchDto.getSearchValidTo().getEndDate());

                taskQuery = taskQuery.or();
                taskQuery = taskQuery.taskVariableValueGreaterThanOrEqual("validTo", dt1);
                taskQuery = taskQuery.taskVariableValueLessThanOrEqual("validTo", dt2);
                taskQuery = taskQuery.endOr();

            } catch (ParseException e) {
                log.error("Date Conversion error in validTo  ", e);
            }
        }


        if (searchDto.getSearchSalesman() != null && searchDto.getSearchSalesman().trim().length() > 0) {
            taskQuery = taskQuery.or();
            taskQuery = taskQuery.taskVariableValueLikeIgnoreCase("salesman", searchDto.getSearchSalesman() + "%");
            taskQuery = taskQuery.endOr();
        }


        if (searchDto.getSearchServiceName() != null && searchDto.getSearchServiceName().trim().length() > 0) {
            taskQuery = taskQuery.or();
            taskQuery = taskQuery.taskVariableValueLikeIgnoreCase("serviceName", searchDto.getSearchServiceName() + "%");
            taskQuery = taskQuery.endOr();
        }


        if (searchDto.getSearchOriginName() != null && searchDto.getSearchOriginName().trim().length() > 0) {
            taskQuery = taskQuery.or();
            taskQuery = taskQuery.taskVariableValueLikeIgnoreCase("originName", searchDto.getSearchOriginName() + "%");
            taskQuery = taskQuery.endOr();
        }

        if (searchDto.getSearchDestinationName() != null && searchDto.getSearchDestinationName().trim().length() > 0) {
            taskQuery = taskQuery.or();
            taskQuery = taskQuery.taskVariableValueLikeIgnoreCase("destinationName", searchDto.getSearchDestinationName() + "%");
            taskQuery = taskQuery.endOr();
        }

        if (searchDto.getSearchQuoteStatus() != null && searchDto.getSearchQuoteStatus().trim().length() > 0) {
            taskQuery = taskQuery.or();
            taskQuery = taskQuery.taskVariableValueLikeIgnoreCase("quoteStatus", searchDto.getSearchQuoteStatus() + "%");
            taskQuery = taskQuery.endOr();
        }

        if (searchDto.getSearchQuoteType() != null && searchDto.getSearchQuoteType().trim().length() > 0) {
            taskQuery = taskQuery.or();
            taskQuery = taskQuery.taskVariableValueLikeIgnoreCase("quoteType", searchDto.getSearchQuoteType() + "%");
            taskQuery = taskQuery.endOr();
        }

        return taskQuery;

    }


    public TaskQuery shipmentFilter(ActivitiSearchDto searchDto, TaskQuery taskQuery) {

        if (searchDto.getSearchCustomerName() != null && searchDto.getSearchCustomerName().trim().length() > 0) {
            taskQuery = taskQuery.taskVariableValueLikeIgnoreCase("customerName", searchDto.getSearchCustomerName() + "%");
        }

        if (searchDto.getSearchShipmentUid() != null && searchDto.getSearchShipmentUid().trim().length() > 0) {
            taskQuery = taskQuery.or();
            taskQuery = taskQuery.taskVariableValueLikeIgnoreCase("shipmentUid", searchDto.getSearchShipmentUid() + "%");
            taskQuery = taskQuery.endOr();
        }

        if (searchDto.getSearchShipmentDate() != null && searchDto.getSearchShipmentDate().getStartDate() != null && searchDto.getSearchShipmentDate().getEndDate() != null) {

            try {
                Date dt1 = sdf.parse(searchDto.getSearchShipmentDate().getStartDate());
                Date dt2 = sdf.parse(searchDto.getSearchShipmentDate().getEndDate());

                taskQuery = taskQuery.or();
                taskQuery = taskQuery.taskVariableValueGreaterThanOrEqual("shipmentDate", dt1);
                taskQuery = taskQuery.taskVariableValueLessThanOrEqual("shipmentDate", dt2);
                taskQuery = taskQuery.endOr();

            } catch (ParseException e) {
                log.error("Date Conversion error in validTo  ", e);
            }
        }


        if (searchDto.getSearchCustomerService() != null && searchDto.getSearchCustomerService().trim().length() > 0) {
            taskQuery = taskQuery.or();
            taskQuery = taskQuery.taskVariableValueLikeIgnoreCase("customerService", searchDto.getSearchCustomerService() + "%");
            taskQuery = taskQuery.endOr();
        }


        if (searchDto.getSearchOriginName() != null && searchDto.getSearchOriginName().trim().length() > 0) {
            taskQuery = taskQuery.or();
            taskQuery = taskQuery.taskVariableValueLikeIgnoreCase("origin", searchDto.getSearchOriginName() + "%");
            taskQuery = taskQuery.endOr();
        }

        if (searchDto.getSearchDestinationName() != null && searchDto.getSearchDestinationName().trim().length() > 0) {
            taskQuery = taskQuery.or();
            taskQuery = taskQuery.taskVariableValueLikeIgnoreCase("destination", searchDto.getSearchDestinationName() + "%");
            taskQuery = taskQuery.endOr();
        }

        return taskQuery;

    }


    public TaskQuery pickupFilter(ActivitiSearchDto searchDto, TaskQuery taskQuery) {

        if (searchDto.getSearchCustomerName() != null && searchDto.getSearchCustomerName().trim().length() > 0) {
            taskQuery = taskQuery.taskVariableValueLikeIgnoreCase("customerName", searchDto.getSearchCustomerName() + "%");
        }

        if (searchDto.getSearchShipmentUid() != null && searchDto.getSearchShipmentUid().trim().length() > 0) {
            taskQuery = taskQuery.or();
            taskQuery = taskQuery.taskVariableValueLikeIgnoreCase("shipmentUid", searchDto.getSearchShipmentUid() + "%");
            taskQuery = taskQuery.endOr();
        }


        if (searchDto.getSearchOriginName() != null && searchDto.getSearchOriginName().trim().length() > 0) {
            taskQuery = taskQuery.or();
            taskQuery = taskQuery.taskVariableValueLikeIgnoreCase("origin", searchDto.getSearchOriginName() + "%");
            taskQuery = taskQuery.endOr();
        }

        if (searchDto.getSearchDestinationName() != null && searchDto.getSearchDestinationName().trim().length() > 0) {
            taskQuery = taskQuery.or();
            taskQuery = taskQuery.taskVariableValueLikeIgnoreCase("destination", searchDto.getSearchDestinationName() + "%");
            taskQuery = taskQuery.endOr();
        }

        if (searchDto.getSearchIsOurPickUp() != null && searchDto.getSearchIsOurPickUp().trim().length() > 0) {
            taskQuery = taskQuery.or();
            taskQuery = taskQuery.taskVariableValueLikeIgnoreCase("ourpickup", searchDto.getSearchIsOurPickUp() + "%");
            taskQuery = taskQuery.endOr();
        }

        if (searchDto.getSearchPickUpFollowUp() != null && searchDto.getSearchPickUpFollowUp().getStartDate() != null && searchDto.getSearchPickUpFollowUp().getEndDate() != null) {

            try {
                Date dt1 = sdf.parse(searchDto.getSearchPickUpFollowUp().getStartDate());
                Date dt2 = sdf.parse(searchDto.getSearchPickUpFollowUp().getEndDate());

                taskQuery = taskQuery.or();
                taskQuery = taskQuery.taskVariableValueGreaterThanOrEqual("pickupdate", dt1);
                taskQuery = taskQuery.taskVariableValueLessThanOrEqual("pickupdate", dt2);
                taskQuery = taskQuery.endOr();

            } catch (ParseException e) {
                log.error("Date Conversion error in validTo  ", e);
            }
        }


        return taskQuery;

    }

    public TaskQuery cfsFilter(ActivitiSearchDto searchDto, TaskQuery taskQuery) {

        if (searchDto.getSearchCustomerName() != null && searchDto.getSearchCustomerName().trim().length() > 0) {
            taskQuery = taskQuery.taskVariableValueLikeIgnoreCase("customerName", searchDto.getSearchCustomerName() + "%");
        }

        if (searchDto.getSearchShipmentUid() != null && searchDto.getSearchShipmentUid().trim().length() > 0) {
            taskQuery = taskQuery.or();
            taskQuery = taskQuery.taskVariableValueLikeIgnoreCase("shipmentUid", searchDto.getSearchShipmentUid() + "%");
            taskQuery = taskQuery.endOr();
        }


        if (searchDto.getSearchOriginName() != null && searchDto.getSearchOriginName().trim().length() > 0) {
            taskQuery = taskQuery.or();
            taskQuery = taskQuery.taskVariableValueLikeIgnoreCase("origin", searchDto.getSearchOriginName() + "%");
            taskQuery = taskQuery.endOr();
        }

        if (searchDto.getSearchDestinationName() != null && searchDto.getSearchDestinationName().trim().length() > 0) {
            taskQuery = taskQuery.or();
            taskQuery = taskQuery.taskVariableValueLikeIgnoreCase("destination", searchDto.getSearchDestinationName() + "%");
            taskQuery = taskQuery.endOr();
        }

        if (searchDto.getSearchETD() != null && searchDto.getSearchETD().getStartDate() != null && searchDto.getSearchETD().getEndDate() != null) {

            try {
                Date dt1 = sdf.parse(searchDto.getSearchETD().getStartDate());
                Date dt2 = sdf.parse(searchDto.getSearchETD().getEndDate());

                taskQuery = taskQuery.or();
                taskQuery = taskQuery.taskVariableValueGreaterThanOrEqual("etd", dt1);
                taskQuery = taskQuery.taskVariableValueLessThanOrEqual("etd", dt2);
                taskQuery = taskQuery.endOr();

            } catch (ParseException e) {
                log.error("Date Conversion error in validTo  ", e);
            }
        }


        return taskQuery;

    }

    public TaskQuery importConsol(ActivitiSearchDto searchDto, TaskQuery taskQuery) {

        if (searchDto.getSearchCustomerName() != null && searchDto.getSearchCustomerName().trim().length() > 0) {
            taskQuery = taskQuery.taskVariableValueLikeIgnoreCase("customerName", searchDto.getSearchCustomerName() + "%");
        }

        if (searchDto.getSearchConsolUid() != null && searchDto.getSearchConsolUid().trim().length() > 0) {
            taskQuery = taskQuery.or();
            taskQuery = taskQuery.taskVariableValueLikeIgnoreCase("ConsolId", searchDto.getSearchConsolUid() + "%");
            taskQuery = taskQuery.endOr();
        }

        if (searchDto.getSearchShipmentUid() != null && searchDto.getSearchShipmentUid().trim().length() > 0) {
            taskQuery = taskQuery.or();
            taskQuery = taskQuery.taskVariableValueLikeIgnoreCase("shipmentUid", searchDto.getSearchShipmentUid() + "%");
            taskQuery = taskQuery.endOr();
        }


        if (searchDto.getSearchOriginName() != null && searchDto.getSearchOriginName().trim().length() > 0) {
            taskQuery = taskQuery.or();
            taskQuery = taskQuery.taskVariableValueLikeIgnoreCase("origin", searchDto.getSearchOriginName() + "%");
            taskQuery = taskQuery.endOr();
        }

        if (searchDto.getSearchDestinationName() != null && searchDto.getSearchDestinationName().trim().length() > 0) {
            taskQuery = taskQuery.or();
            taskQuery = taskQuery.taskVariableValueLikeIgnoreCase("destination", searchDto.getSearchDestinationName() + "%");
            taskQuery = taskQuery.endOr();
        }

        if (searchDto.getSearchETD() != null && searchDto.getSearchETD().getStartDate() != null && searchDto.getSearchETD().getEndDate() != null) {

            try {
                Date dt1 = sdf.parse(searchDto.getSearchETD().getStartDate());
                Date dt2 = sdf.parse(searchDto.getSearchETD().getEndDate());

                taskQuery = taskQuery.or();
                taskQuery = taskQuery.taskVariableValueGreaterThanOrEqual("etd", dt1);
                taskQuery = taskQuery.taskVariableValueLessThanOrEqual("etd", dt2);
                taskQuery = taskQuery.endOr();

            } catch (ParseException e) {
                log.error("Date Conversion error in validTo  ", e);
            }
        }

        if (searchDto.getSearchETA() != null && searchDto.getSearchETA().getStartDate() != null && searchDto.getSearchETA().getEndDate() != null) {

            try {
                Date dt1 = sdf.parse(searchDto.getSearchETA().getStartDate());
                Date dt2 = sdf.parse(searchDto.getSearchETA().getEndDate());

                taskQuery = taskQuery.or();
                taskQuery = taskQuery.taskVariableValueGreaterThanOrEqual("eta", dt1);
                taskQuery = taskQuery.taskVariableValueLessThanOrEqual("eta", dt2);
                taskQuery = taskQuery.endOr();

            } catch (ParseException e) {
                log.error("Date Conversion error in validTo  ", e);
            }
        }


        if (searchDto.getSearchCarrierName() != null && searchDto.getSearchCarrierName().trim().length() > 0) {
            taskQuery = taskQuery.or();
            taskQuery = taskQuery.taskVariableValueLikeIgnoreCase("carrier", searchDto.getSearchCarrierName() + "%");
            taskQuery = taskQuery.endOr();
        }

        if (searchDto.getSearchFlightNumber() != null && searchDto.getSearchFlightNumber().trim().length() > 0) {
            taskQuery = taskQuery.or();
            taskQuery = taskQuery.taskVariableValueLikeIgnoreCase("flightNo", searchDto.getSearchFlightNumber() + "%");
            taskQuery = taskQuery.endOr();
        }

        if (searchDto.getSearchMawb() != null && searchDto.getSearchMawb().trim().length() > 0) {
            taskQuery = taskQuery.or();
            taskQuery = taskQuery.taskVariableValueLikeIgnoreCase("mawb", searchDto.getSearchMawb() + "%");
            taskQuery = taskQuery.endOr();
        }

        return taskQuery;

    }

    public TaskQuery linkingToMaster(ActivitiSearchDto searchDto, TaskQuery taskQuery) {

        if (searchDto.getSearchCustomerName() != null && searchDto.getSearchCustomerName().trim().length() > 0) {
            taskQuery = taskQuery.taskVariableValueLikeIgnoreCase("customerName", searchDto.getSearchCustomerName() + "%");
        }

        if (searchDto.getSearchShipmentUid() != null && searchDto.getSearchShipmentUid().trim().length() > 0) {
            taskQuery = taskQuery.or();
            taskQuery = taskQuery.taskVariableValueLikeIgnoreCase("shipmentUid", searchDto.getSearchShipmentUid() + "%");
            taskQuery = taskQuery.endOr();
        }


        if (searchDto.getSearchOriginName() != null && searchDto.getSearchOriginName().trim().length() > 0) {
            taskQuery = taskQuery.or();
            taskQuery = taskQuery.taskVariableValueLikeIgnoreCase("origin", searchDto.getSearchOriginName() + "%");
            taskQuery = taskQuery.endOr();
        }

        if (searchDto.getSearchDestinationName() != null && searchDto.getSearchDestinationName().trim().length() > 0) {
            taskQuery = taskQuery.or();
            taskQuery = taskQuery.taskVariableValueLikeIgnoreCase("destination", searchDto.getSearchDestinationName() + "%");
            taskQuery = taskQuery.endOr();
        }

        if (searchDto.getSearchETD() != null && searchDto.getSearchETD().getStartDate() != null && searchDto.getSearchETD().getEndDate() != null) {

            try {
                Date dt1 = sdf.parse(searchDto.getSearchETD().getStartDate());
                Date dt2 = sdf.parse(searchDto.getSearchETD().getEndDate());

                taskQuery = taskQuery.or();
                taskQuery = taskQuery.taskVariableValueGreaterThanOrEqual("etd", dt1);
                taskQuery = taskQuery.taskVariableValueLessThanOrEqual("etd", dt2);
                taskQuery = taskQuery.endOr();

            } catch (ParseException e) {
                log.error("Date Conversion error in validTo  ", e);
            }
        }

        if (searchDto.getSearchETA() != null && searchDto.getSearchETA().getStartDate() != null && searchDto.getSearchETA().getEndDate() != null) {

            try {
                Date dt1 = sdf.parse(searchDto.getSearchETA().getStartDate());
                Date dt2 = sdf.parse(searchDto.getSearchETA().getEndDate());

                taskQuery = taskQuery.or();
                taskQuery = taskQuery.taskVariableValueGreaterThanOrEqual("eta", dt1);
                taskQuery = taskQuery.taskVariableValueLessThanOrEqual("eta", dt2);
                taskQuery = taskQuery.endOr();

            } catch (ParseException e) {
                log.error("Date Conversion error in validTo  ", e);
            }
        }


        if (searchDto.getSearchCarrierName() != null && searchDto.getSearchCarrierName().trim().length() > 0) {
            taskQuery = taskQuery.or();
            taskQuery = taskQuery.taskVariableValueLikeIgnoreCase("carrier", searchDto.getSearchCarrierName() + "%");
            taskQuery = taskQuery.endOr();
        }

        if (searchDto.getSearchFlightNumber() != null && searchDto.getSearchFlightNumber().trim().length() > 0) {
            taskQuery = taskQuery.or();
            taskQuery = taskQuery.taskVariableValueLikeIgnoreCase("flightNo", searchDto.getSearchFlightNumber() + "%");
            taskQuery = taskQuery.endOr();
        }

        if (searchDto.getSearchMawb() != null && searchDto.getSearchMawb().trim().length() > 0) {
            taskQuery = taskQuery.or();
            taskQuery = taskQuery.taskVariableValueLikeIgnoreCase("mawb", searchDto.getSearchMawb() + "%");
            taskQuery = taskQuery.endOr();
        }

        return taskQuery;

    }


    public TaskQuery aesOrCustoms(ActivitiSearchDto searchDto, TaskQuery taskQuery) {

        if (searchDto.getSearchCustomerName() != null && searchDto.getSearchCustomerName().trim().length() > 0) {
            taskQuery = taskQuery.taskVariableValueLikeIgnoreCase("customerName", searchDto.getSearchCustomerName() + "%");
        }

        if (searchDto.getSearchShipmentUid() != null && searchDto.getSearchShipmentUid().trim().length() > 0) {
            taskQuery = taskQuery.or();
            taskQuery = taskQuery.taskVariableValueLikeIgnoreCase("shipmentUid", searchDto.getSearchShipmentUid() + "%");
            taskQuery = taskQuery.endOr();
        }


        if (searchDto.getSearchOriginName() != null && searchDto.getSearchOriginName().trim().length() > 0) {
            taskQuery = taskQuery.or();
            taskQuery = taskQuery.taskVariableValueLikeIgnoreCase("origin", searchDto.getSearchOriginName() + "%");
            taskQuery = taskQuery.endOr();
        }

        if (searchDto.getSearchDestinationName() != null && searchDto.getSearchDestinationName().trim().length() > 0) {
            taskQuery = taskQuery.or();
            taskQuery = taskQuery.taskVariableValueLikeIgnoreCase("destination", searchDto.getSearchDestinationName() + "%");
            taskQuery = taskQuery.endOr();
        }

        if (searchDto.getSearchETD() != null && searchDto.getSearchETD().getStartDate() != null && searchDto.getSearchETD().getEndDate() != null) {

            try {
                Date dt1 = sdf.parse(searchDto.getSearchETD().getStartDate());
                Date dt2 = sdf.parse(searchDto.getSearchETD().getEndDate());

                taskQuery = taskQuery.or();
                taskQuery = taskQuery.taskVariableValueGreaterThanOrEqual("etd", dt1);
                taskQuery = taskQuery.taskVariableValueLessThanOrEqual("etd", dt2);
                taskQuery = taskQuery.endOr();

            } catch (ParseException e) {
                log.error("Date Conversion error in validTo  ", e);
            }
        }

        if (searchDto.getSearchETA() != null && searchDto.getSearchETA().getStartDate() != null && searchDto.getSearchETA().getEndDate() != null) {

            try {
                Date dt1 = sdf.parse(searchDto.getSearchETA().getStartDate());
                Date dt2 = sdf.parse(searchDto.getSearchETA().getEndDate());

                taskQuery = taskQuery.or();
                taskQuery = taskQuery.taskVariableValueGreaterThanOrEqual("eta", dt1);
                taskQuery = taskQuery.taskVariableValueLessThanOrEqual("eta", dt2);
                taskQuery = taskQuery.endOr();

            } catch (ParseException e) {
                log.error("Date Conversion error in validTo  ", e);
            }
        }


        if (searchDto.getSearchCarrierName() != null && searchDto.getSearchCarrierName().trim().length() > 0) {
            taskQuery = taskQuery.or();
            taskQuery = taskQuery.taskVariableValueLikeIgnoreCase("carrier", searchDto.getSearchCarrierName() + "%");
            taskQuery = taskQuery.endOr();
        }

        if (searchDto.getSearchFlightNumber() != null && searchDto.getSearchFlightNumber().trim().length() > 0) {
            taskQuery = taskQuery.or();
            taskQuery = taskQuery.taskVariableValueLikeIgnoreCase("flightNo", searchDto.getSearchFlightNumber() + "%");
            taskQuery = taskQuery.endOr();
        }

        if (searchDto.getSearchMawb() != null && searchDto.getSearchMawb().trim().length() > 0) {
            taskQuery = taskQuery.or();
            taskQuery = taskQuery.taskVariableValueLikeIgnoreCase("mawb", searchDto.getSearchMawb() + "%");
            taskQuery = taskQuery.endOr();
        }

        return taskQuery;

    }

    public TaskQuery signOff(ActivitiSearchDto searchDto, TaskQuery taskQuery) {

        if (searchDto.getSearchCustomerName() != null && searchDto.getSearchCustomerName().trim().length() > 0) {
            taskQuery = taskQuery.taskVariableValueLikeIgnoreCase("customerName", searchDto.getSearchCustomerName() + "%");
        }

        if (searchDto.getSearchShipmentUid() != null && searchDto.getSearchShipmentUid().trim().length() > 0) {
            taskQuery = taskQuery.or();
            taskQuery = taskQuery.taskVariableValueLikeIgnoreCase("shipmentUid", searchDto.getSearchShipmentUid() + "%");
            taskQuery = taskQuery.endOr();
        }


        if (searchDto.getSearchOriginName() != null && searchDto.getSearchOriginName().trim().length() > 0) {
            taskQuery = taskQuery.or();
            taskQuery = taskQuery.taskVariableValueLikeIgnoreCase("origin", searchDto.getSearchOriginName() + "%");
            taskQuery = taskQuery.endOr();
        }

        if (searchDto.getSearchDestinationName() != null && searchDto.getSearchDestinationName().trim().length() > 0) {
            taskQuery = taskQuery.or();
            taskQuery = taskQuery.taskVariableValueLikeIgnoreCase("destination", searchDto.getSearchDestinationName() + "%");
            taskQuery = taskQuery.endOr();
        }

        if (searchDto.getSearchETD() != null && searchDto.getSearchETD().getStartDate() != null && searchDto.getSearchETD().getEndDate() != null) {

            try {
                Date dt1 = sdf.parse(searchDto.getSearchETD().getStartDate());
                Date dt2 = sdf.parse(searchDto.getSearchETD().getEndDate());

                taskQuery = taskQuery.or();
                taskQuery = taskQuery.taskVariableValueGreaterThanOrEqual("etd", dt1);
                taskQuery = taskQuery.taskVariableValueLessThanOrEqual("etd", dt2);
                taskQuery = taskQuery.endOr();

            } catch (ParseException e) {
                log.error("Date Conversion error in validTo  ", e);
            }
        }

        if (searchDto.getSearchETA() != null && searchDto.getSearchETA().getStartDate() != null && searchDto.getSearchETA().getEndDate() != null) {

            try {
                Date dt1 = sdf.parse(searchDto.getSearchETA().getStartDate());
                Date dt2 = sdf.parse(searchDto.getSearchETA().getEndDate());

                taskQuery = taskQuery.or();
                taskQuery = taskQuery.taskVariableValueGreaterThanOrEqual("eta", dt1);
                taskQuery = taskQuery.taskVariableValueLessThanOrEqual("eta", dt2);
                taskQuery = taskQuery.endOr();

            } catch (ParseException e) {
                log.error("Date Conversion error in validTo  ", e);
            }
        }


        if (searchDto.getSearchCarrierName() != null && searchDto.getSearchCarrierName().trim().length() > 0) {
            taskQuery = taskQuery.or();
            taskQuery = taskQuery.taskVariableValueLikeIgnoreCase("carrier", searchDto.getSearchCarrierName() + "%");
            taskQuery = taskQuery.endOr();
        }

        if (searchDto.getSearchFlightNumber() != null && searchDto.getSearchFlightNumber().trim().length() > 0) {
            taskQuery = taskQuery.or();
            taskQuery = taskQuery.taskVariableValueLikeIgnoreCase("flightNo", searchDto.getSearchFlightNumber() + "%");
            taskQuery = taskQuery.endOr();
        }

        if (searchDto.getSearchMawb() != null && searchDto.getSearchMawb().trim().length() > 0) {
            taskQuery = taskQuery.or();
            taskQuery = taskQuery.taskVariableValueLikeIgnoreCase("mawb", searchDto.getSearchMawb() + "%");
            taskQuery = taskQuery.endOr();
        }

        return taskQuery;

    }

    public TaskQuery airLineSubmission(ActivitiSearchDto searchDto, TaskQuery taskQuery) {

        if (searchDto.getSearchCustomerName() != null && searchDto.getSearchCustomerName().trim().length() > 0) {
            taskQuery = taskQuery.taskVariableValueLikeIgnoreCase("customerName", searchDto.getSearchCustomerName() + "%");
        }

        if (searchDto.getSearchConsolUid() != null && searchDto.getSearchConsolUid().trim().length() > 0) {
            taskQuery = taskQuery.or();
            taskQuery = taskQuery.taskVariableValueLikeIgnoreCase("ConsolId", searchDto.getSearchConsolUid() + "%");
            taskQuery = taskQuery.endOr();
        }

        if (searchDto.getSearchShipmentUid() != null && searchDto.getSearchShipmentUid().trim().length() > 0) {
            taskQuery = taskQuery.or();
            taskQuery = taskQuery.taskVariableValueLikeIgnoreCase("shipmentUid", searchDto.getSearchShipmentUid() + "%");
            taskQuery = taskQuery.endOr();
        }


        if (searchDto.getSearchOriginName() != null && searchDto.getSearchOriginName().trim().length() > 0) {
            taskQuery = taskQuery.or();
            taskQuery = taskQuery.taskVariableValueLikeIgnoreCase("orgin", searchDto.getSearchOriginName() + "%");
            taskQuery = taskQuery.endOr();
        }

        if (searchDto.getSearchDestinationName() != null && searchDto.getSearchDestinationName().trim().length() > 0) {
            taskQuery = taskQuery.or();
            taskQuery = taskQuery.taskVariableValueLikeIgnoreCase("destination", searchDto.getSearchDestinationName() + "%");
            taskQuery = taskQuery.endOr();
        }

        if (searchDto.getSearchETD() != null && searchDto.getSearchETD().getStartDate() != null && searchDto.getSearchETD().getEndDate() != null) {

            try {
                Date dt1 = sdf.parse(searchDto.getSearchETD().getStartDate());
                Date dt2 = sdf.parse(searchDto.getSearchETD().getEndDate());

                taskQuery = taskQuery.or();
                taskQuery = taskQuery.taskVariableValueGreaterThanOrEqual("etd", dt1);
                taskQuery = taskQuery.taskVariableValueLessThanOrEqual("etd", dt2);
                taskQuery = taskQuery.endOr();

            } catch (ParseException e) {
                log.error("Date Conversion error in validTo  ", e);
            }
        }

        if (searchDto.getSearchETA() != null && searchDto.getSearchETA().getStartDate() != null && searchDto.getSearchETA().getEndDate() != null) {

            try {
                Date dt1 = sdf.parse(searchDto.getSearchETA().getStartDate());
                Date dt2 = sdf.parse(searchDto.getSearchETA().getEndDate());

                taskQuery = taskQuery.or();
                taskQuery = taskQuery.taskVariableValueGreaterThanOrEqual("eta", dt1);
                taskQuery = taskQuery.taskVariableValueLessThanOrEqual("eta", dt2);
                taskQuery = taskQuery.endOr();

            } catch (ParseException e) {
                log.error("Date Conversion error in validTo  ", e);
            }
        }


        if (searchDto.getSearchCarrierName() != null && searchDto.getSearchCarrierName().trim().length() > 0) {
            taskQuery = taskQuery.or();
            taskQuery = taskQuery.taskVariableValueLikeIgnoreCase("carrier", searchDto.getSearchCarrierName() + "%");
            taskQuery = taskQuery.endOr();
        }

        if (searchDto.getSearchFlightNumber() != null && searchDto.getSearchFlightNumber().trim().length() > 0) {
            taskQuery = taskQuery.or();
            taskQuery = taskQuery.taskVariableValueLikeIgnoreCase("flightNo", searchDto.getSearchFlightNumber() + "%");
            taskQuery = taskQuery.endOr();
        }

        if (searchDto.getSearchMawb() != null && searchDto.getSearchMawb().trim().length() > 0) {
            taskQuery = taskQuery.or();
            taskQuery = taskQuery.taskVariableValueLikeIgnoreCase("mawb", searchDto.getSearchMawb() + "%");
            taskQuery = taskQuery.endOr();
        }

        return taskQuery;

    }

    public TaskQuery jobCompletion(ActivitiSearchDto searchDto, TaskQuery taskQuery) {

        if (searchDto.getSearchCustomerName() != null && searchDto.getSearchCustomerName().trim().length() > 0) {
            taskQuery = taskQuery.taskVariableValueLikeIgnoreCase("customerName", searchDto.getSearchCustomerName() + "%");
        }

        if (searchDto.getSearchConsolUid() != null && searchDto.getSearchConsolUid().trim().length() > 0) {
            taskQuery = taskQuery.or();
            taskQuery = taskQuery.taskVariableValueLikeIgnoreCase("ConsolId", searchDto.getSearchConsolUid() + "%");
            taskQuery = taskQuery.endOr();
        }

        if (searchDto.getSearchShipmentUid() != null && searchDto.getSearchShipmentUid().trim().length() > 0) {
            taskQuery = taskQuery.or();
            taskQuery = taskQuery.taskVariableValueLikeIgnoreCase("shipmentUid", searchDto.getSearchShipmentUid() + "%");
            taskQuery = taskQuery.endOr();
        }


        if (searchDto.getSearchOriginName() != null && searchDto.getSearchOriginName().trim().length() > 0) {
            taskQuery = taskQuery.or();
            taskQuery = taskQuery.taskVariableValueLikeIgnoreCase("orgin", searchDto.getSearchOriginName() + "%");
            taskQuery = taskQuery.endOr();
        }

        if (searchDto.getSearchDestinationName() != null && searchDto.getSearchDestinationName().trim().length() > 0) {
            taskQuery = taskQuery.or();
            taskQuery = taskQuery.taskVariableValueLikeIgnoreCase("destination", searchDto.getSearchDestinationName() + "%");
            taskQuery = taskQuery.endOr();
        }

        if (searchDto.getSearchETD() != null && searchDto.getSearchETD().getStartDate() != null && searchDto.getSearchETD().getEndDate() != null) {

            try {
                Date dt1 = sdf.parse(searchDto.getSearchETD().getStartDate());
                Date dt2 = sdf.parse(searchDto.getSearchETD().getEndDate());

                taskQuery = taskQuery.or();
                taskQuery = taskQuery.taskVariableValueGreaterThanOrEqual("etd", dt1);
                taskQuery = taskQuery.taskVariableValueLessThanOrEqual("etd", dt2);
                taskQuery = taskQuery.endOr();

            } catch (ParseException e) {
                log.error("Date Conversion error in validTo  ", e);
            }
        }

        if (searchDto.getSearchETA() != null && searchDto.getSearchETA().getStartDate() != null && searchDto.getSearchETA().getEndDate() != null) {

            try {
                Date dt1 = sdf.parse(searchDto.getSearchETA().getStartDate());
                Date dt2 = sdf.parse(searchDto.getSearchETA().getEndDate());

                taskQuery = taskQuery.or();
                taskQuery = taskQuery.taskVariableValueGreaterThanOrEqual("eta", dt1);
                taskQuery = taskQuery.taskVariableValueLessThanOrEqual("eta", dt2);
                taskQuery = taskQuery.endOr();

            } catch (ParseException e) {
                log.error("Date Conversion error in validTo  ", e);
            }
        }


        if (searchDto.getSearchCarrierName() != null && searchDto.getSearchCarrierName().trim().length() > 0) {
            taskQuery = taskQuery.or();
            taskQuery = taskQuery.taskVariableValueLikeIgnoreCase("carrier", searchDto.getSearchCarrierName() + "%");
            taskQuery = taskQuery.endOr();
        }

        if (searchDto.getSearchFlightNumber() != null && searchDto.getSearchFlightNumber().trim().length() > 0) {
            taskQuery = taskQuery.or();
            taskQuery = taskQuery.taskVariableValueLikeIgnoreCase("flightNo", searchDto.getSearchFlightNumber() + "%");
            taskQuery = taskQuery.endOr();
        }

        if (searchDto.getSearchMawb() != null && searchDto.getSearchMawb().trim().length() > 0) {
            taskQuery = taskQuery.or();
            taskQuery = taskQuery.taskVariableValueLikeIgnoreCase("mawb", searchDto.getSearchMawb() + "%");
            taskQuery = taskQuery.endOr();
        }

        return taskQuery;

    }

    public TaskQuery invoice(ActivitiSearchDto searchDto, TaskQuery taskQuery) {

        if (searchDto.getSearchCustomerName() != null && searchDto.getSearchCustomerName().trim().length() > 0) {
            taskQuery = taskQuery.taskVariableValueLikeIgnoreCase("customerName", searchDto.getSearchCustomerName() + "%");
        }

        if (searchDto.getSearchShipmentUid() != null && searchDto.getSearchShipmentUid().trim().length() > 0) {
            taskQuery = taskQuery.or();
            taskQuery = taskQuery.taskVariableValueLikeIgnoreCase("shipmentUid", searchDto.getSearchShipmentUid() + "%");
            taskQuery = taskQuery.endOr();
        }


        if (searchDto.getSearchOriginName() != null && searchDto.getSearchOriginName().trim().length() > 0) {
            taskQuery = taskQuery.or();
            taskQuery = taskQuery.taskVariableValueLikeIgnoreCase("orgin", searchDto.getSearchOriginName() + "%");
            taskQuery = taskQuery.endOr();
        }

        if (searchDto.getSearchDestinationName() != null && searchDto.getSearchDestinationName().trim().length() > 0) {
            taskQuery = taskQuery.or();
            taskQuery = taskQuery.taskVariableValueLikeIgnoreCase("destination", searchDto.getSearchDestinationName() + "%");
            taskQuery = taskQuery.endOr();
        }

        if (searchDto.getSearchETD() != null && searchDto.getSearchETD().getStartDate() != null && searchDto.getSearchETD().getEndDate() != null) {

            try {
                Date dt1 = sdf.parse(searchDto.getSearchETD().getStartDate());
                Date dt2 = sdf.parse(searchDto.getSearchETD().getEndDate());

                taskQuery = taskQuery.or();
                taskQuery = taskQuery.taskVariableValueGreaterThanOrEqual("etd", dt1);
                taskQuery = taskQuery.taskVariableValueLessThanOrEqual("etd", dt2);
                taskQuery = taskQuery.endOr();

            } catch (ParseException e) {
                log.error("Date Conversion error in validTo  ", e);
            }
        }

        if (searchDto.getSearchETA() != null && searchDto.getSearchETA().getStartDate() != null && searchDto.getSearchETA().getEndDate() != null) {

            try {
                Date dt1 = sdf.parse(searchDto.getSearchETA().getStartDate());
                Date dt2 = sdf.parse(searchDto.getSearchETA().getEndDate());

                taskQuery = taskQuery.or();
                taskQuery = taskQuery.taskVariableValueGreaterThanOrEqual("eta", dt1);
                taskQuery = taskQuery.taskVariableValueLessThanOrEqual("eta", dt2);
                taskQuery = taskQuery.endOr();

            } catch (ParseException e) {
                log.error("Date Conversion error in validTo  ", e);
            }
        }


        if (searchDto.getSearchCarrierName() != null && searchDto.getSearchCarrierName().trim().length() > 0) {
            taskQuery = taskQuery.or();
            taskQuery = taskQuery.taskVariableValueLikeIgnoreCase("carrier", searchDto.getSearchCarrierName() + "%");
            taskQuery = taskQuery.endOr();
        }

        if (searchDto.getSearchFlightNumber() != null && searchDto.getSearchFlightNumber().trim().length() > 0) {
            taskQuery = taskQuery.or();
            taskQuery = taskQuery.taskVariableValueLikeIgnoreCase("flightNo", searchDto.getSearchFlightNumber() + "%");
            taskQuery = taskQuery.endOr();
        }

        if (searchDto.getSearchMawb() != null && searchDto.getSearchMawb().trim().length() > 0) {
            taskQuery = taskQuery.or();
            taskQuery = taskQuery.taskVariableValueLikeIgnoreCase("mawb", searchDto.getSearchMawb() + "%");
            taskQuery = taskQuery.endOr();
        }

        return taskQuery;

    }

    public TaskQuery atdConfirmation(ActivitiSearchDto searchDto, TaskQuery taskQuery) {

        if (searchDto.getSearchCustomerName() != null && searchDto.getSearchCustomerName().trim().length() > 0) {
            taskQuery = taskQuery.taskVariableValueLikeIgnoreCase("customerName", searchDto.getSearchCustomerName() + "%");
        }

        if (searchDto.getSearchShipmentUid() != null && searchDto.getSearchShipmentUid().trim().length() > 0) {
            taskQuery = taskQuery.or();
            taskQuery = taskQuery.taskVariableValueLikeIgnoreCase("shipmentUid", searchDto.getSearchShipmentUid() + "%");
            taskQuery = taskQuery.endOr();
        }


        if (searchDto.getSearchOriginName() != null && searchDto.getSearchOriginName().trim().length() > 0) {
            taskQuery = taskQuery.or();
            taskQuery = taskQuery.taskVariableValueLikeIgnoreCase("orgin", searchDto.getSearchOriginName() + "%");
            taskQuery = taskQuery.endOr();
        }

        if (searchDto.getSearchDestinationName() != null && searchDto.getSearchDestinationName().trim().length() > 0) {
            taskQuery = taskQuery.or();
            taskQuery = taskQuery.taskVariableValueLikeIgnoreCase("destination", searchDto.getSearchDestinationName() + "%");
            taskQuery = taskQuery.endOr();
        }

        if (searchDto.getSearchETD() != null && searchDto.getSearchETD().getStartDate() != null && searchDto.getSearchETD().getEndDate() != null) {

            try {
                Date dt1 = sdf.parse(searchDto.getSearchETD().getStartDate());
                Date dt2 = sdf.parse(searchDto.getSearchETD().getEndDate());

                taskQuery = taskQuery.or();
                taskQuery = taskQuery.taskVariableValueGreaterThanOrEqual("etd", dt1);
                taskQuery = taskQuery.taskVariableValueLessThanOrEqual("etd", dt2);
                taskQuery = taskQuery.endOr();

            } catch (ParseException e) {
                log.error("Date Conversion error in validTo  ", e);
            }
        }

        if (searchDto.getSearchETA() != null && searchDto.getSearchETA().getStartDate() != null && searchDto.getSearchETA().getEndDate() != null) {

            try {
                Date dt1 = sdf.parse(searchDto.getSearchETA().getStartDate());
                Date dt2 = sdf.parse(searchDto.getSearchETA().getEndDate());

                taskQuery = taskQuery.or();
                taskQuery = taskQuery.taskVariableValueGreaterThanOrEqual("eta", dt1);
                taskQuery = taskQuery.taskVariableValueLessThanOrEqual("eta", dt2);
                taskQuery = taskQuery.endOr();

            } catch (ParseException e) {
                log.error("Date Conversion error in validTo  ", e);
            }
        }


        if (searchDto.getSearchCarrierName() != null && searchDto.getSearchCarrierName().trim().length() > 0) {
            taskQuery = taskQuery.or();
            taskQuery = taskQuery.taskVariableValueLikeIgnoreCase("carrier", searchDto.getSearchCarrierName() + "%");
            taskQuery = taskQuery.endOr();
        }

        if (searchDto.getSearchFlightNumber() != null && searchDto.getSearchFlightNumber().trim().length() > 0) {
            taskQuery = taskQuery.or();
            taskQuery = taskQuery.taskVariableValueLikeIgnoreCase("flightNo", searchDto.getSearchFlightNumber() + "%");
            taskQuery = taskQuery.endOr();
        }

        if (searchDto.getSearchMawb() != null && searchDto.getSearchMawb().trim().length() > 0) {
            taskQuery = taskQuery.or();
            taskQuery = taskQuery.taskVariableValueLikeIgnoreCase("mawb", searchDto.getSearchMawb() + "%");
            taskQuery = taskQuery.endOr();
        }

        return taskQuery;

    }


}
