package com.efreightsuite.activiti.dto;

import java.io.Serializable;

import com.efreightsuite.dto.DateRange;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@JsonIgnoreProperties(ignoreUnknown = true)
@ToString
public class ActivitiSearchDto implements Serializable {

    private static final long serialVersionUID = 1L;

    @Getter
    @Setter
    Integer selectedPageNumber = 0;

    @Getter
    @Setter
    Integer recordPerPage = 10;

    @Getter
    @Setter
    String keyword;

    @Getter
    @Setter
    String searchCustomerName;

    @Getter
    @Setter
    String searchEnquiryNo;

    @Getter
    @Setter
    DateRange searchReceivedOn;

    @Getter
    @Setter
    DateRange searchQuoteBy;

    @Getter
    @Setter
    String searchServiceName;

    @Getter
    @Setter
    String searchOriginName;

    @Getter
    @Setter
    String searchDestinationName;


    @Getter
    @Setter
    String searchQuotationNo;

    @Getter
    @Setter
    DateRange searchValidTo;

    @Getter
    @Setter
    String searchSalesman;


    @Getter
    @Setter
    String searchQuoteStatus;


    @Getter
    @Setter
    String searchQuoteType;

    @Getter
    @Setter
    DateRange searchShipmentReqDate;

    @Getter
    @Setter
    String searchCustomerService;

    @Getter
    @Setter
    String searchIsOurPickUp;

    @Getter
    @Setter
    DateRange searchPickUpFollowUp;

    @Getter
    @Setter
    DateRange searchPickUpActualUp;


    @Getter
    @Setter
    DateRange searchETD;

    @Getter
    @Setter
    DateRange searchETA;

    @Getter
    @Setter
    String searchCarrierName;

    @Getter
    @Setter
    String searchFlightNumber;

    @Getter
    @Setter
    String searchMawb;

    @Getter
    @Setter
    String searchConsolUid;


    @Getter
    @Setter
    String searchShipmentUid;

    @Getter
    @Setter
    DateRange searchShipmentDate;

}
