package com.efreightsuite.activiti;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.efreightsuite.activiti.dto.BaseDto;
import com.efreightsuite.activiti.dto.TaskDto;
import com.efreightsuite.activiti.dto.TaskResponseDto;
import com.efreightsuite.activiti.dto.WorkFlowSearchDto;
import com.efreightsuite.enumeration.ActivitiGroup;
import com.efreightsuite.enumeration.ImportExport;
import com.efreightsuite.enumeration.ServiceType;
import com.efreightsuite.enumeration.TransportMode;
import com.efreightsuite.enumeration.YesNo;
import com.efreightsuite.model.ServiceMaster;
import com.efreightsuite.model.UserProfile;
import com.efreightsuite.model.WorkFlowGroup;
import com.efreightsuite.repository.UserProfileRepository;
import com.efreightsuite.service.AuthService;
import com.efreightsuite.util.AppUtil;
import lombok.extern.log4j.Log4j2;
import org.activiti.engine.HistoryService;
import org.activiti.engine.IdentityService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.identity.Group;
import org.activiti.engine.identity.User;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class ActivitiUtilService {

    private static final String AIR_EXPORT_PROCESS_KEY = "airExportProcess";
    private static final String AIR_IMPORT_PROCESS_KEY = "airImportProcess";
    @Autowired
    private
    JdbcTemplate jdbcTemplate;
    @Autowired
    private
    AuthService authService;
    @Autowired
    private
    UserProfileRepository userProfileRepository;
    @Autowired
    AppUtil appUtil;
    @Autowired
    private RuntimeService runtimeService;
    @Autowired
    private TaskService taskService;
    @Autowired
    private IdentityService identityService;
    @Autowired
    private HistoryService historyService;

    public ProcessInstance startAirExportBPMNProcess(Map<String, Object> vars) {
        try {
            ProcessInstance processInstance = runtimeService.startProcessInstanceByKey(AIR_EXPORT_PROCESS_KEY, vars);
            log.info("ProcessInstanceId -- " + processInstance.getProcessInstanceId());
            return processInstance;
        } catch (Exception e) {
            log.error("Exception while starting activity", e);
            return null;
        }
    }

    public ProcessInstance startAirImportBPMNProcess(Map<String, Object> vars) {
        try {
            ProcessInstance processInstance = runtimeService.startProcessInstanceByKey(AIR_IMPORT_PROCESS_KEY, vars);
            log.info("ProcessInstanceId -- " + processInstance.getProcessInstanceId());
            return processInstance;
        } catch (Exception e) {
            log.error("Exception while starting activity", e);
            return null;
        }
    }

    public void checkAndCreateGroupName(ActivitiGroup groupName) {
        try {
            Group group = identityService.createGroupQuery().groupId(groupName.toString()).singleResult();
            if (group == null || group.getId() == null) {
                group = identityService.newGroup(groupName.toString());
                group.setName(groupName.toString());
                group.setType("Active");
                identityService.saveGroup(group);
            }
            UserProfile userProfile = AuthService.getCurrentUser();
            if (userProfile != null && userProfile.getId() != null) {
                boolean mapGroup = false;
                List<WorkFlowGroup> workFlowGroupList = userProfileRepository.findWorkFlowGroupListByUser(userProfile.getId());
                if (workFlowGroupList != null && !workFlowGroupList.isEmpty()) {
                    for (WorkFlowGroup wfg : workFlowGroupList) {
                        if (wfg.getWorkFlowName().equals(groupName.toString())) {
                            mapGroup = true;
                        }
                    }
                }
                if (mapGroup) {
                    User user = identityService.createUserQuery().userId(userProfile.getId().toString()).singleResult();
                    if (user == null || user.getId() == null) {
                        user = identityService.newUser(userProfile.getId().toString());
                        user.setFirstName(userProfile.getEmployee().getFirstName());
                        user.setLastName(userProfile.getEmployee().getLastName());
                        user.setEmail(userProfile.getEmployee().getEmail());
                        user.setPassword(userProfile.getPassword());
                        identityService.saveUser(user);
                    }
                    List<Group> groupList = identityService.createGroupQuery().groupMember(userProfile.getId().toString()).list();
                    boolean alreadyMappedInGroup = false;
                    for (Group localGroup : groupList) {
                        if (localGroup.getName().equals(groupName.toString())) {
                            alreadyMappedInGroup = true;
                        }
                    }
                    if (!alreadyMappedInGroup) {
                        identityService.createMembership(userProfile.getId().toString(), groupName.toString());
                    }
                }
            }
        } catch (Exception e) {
            log.error("Error in group name checking", e);
        }
    }


    public boolean isAirExport(ServiceMaster service, YesNo workflowCompleted) {

        if (workflowCompleted != null && workflowCompleted == YesNo.Yes) {
            //This task already completed
            return false;
        }

        if (service.getTransportMode() == TransportMode.Air
                && service.getImportExport() == ImportExport.Export
                && service.getServiceType() == null) {
            return true;
        }

        return false;

    }

    public boolean isAirImport(ServiceMaster service, YesNo workflowCompleted) {

        if (workflowCompleted != null && workflowCompleted == YesNo.Yes) {
            //This task already completed
            return false;
        }

        if (service.getTransportMode() == TransportMode.Air
                && service.getImportExport() == ImportExport.Import
                && service.getServiceType() == null) {
            return true;
        }

        return false;

    }

    public boolean isAirImportClearance(ServiceMaster service) {


        if (service.getTransportMode() == TransportMode.Air
                && service.getImportExport() == ImportExport.Import
                && service.getServiceType() != null
                && service.getServiceType().getServiceType() == ServiceType.Clearance) {
            return true;
        }

        return false;

    }

}
