package com.efreightsuite.search;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.efreightsuite.dto.ProvisionalSearchDto;
import com.efreightsuite.dto.SearchRespDto;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class ProvisionalSearchImpl {

    @Autowired
    private
    EntityManager em;

    public SearchRespDto search(ProvisionalSearchDto dto) {

        Map<String, Object> params = new HashMap<>();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

        String hql = "";


        String searchQuery = "select new com.efreightsuite.dto.provisionalCostResponseDto ( "
                + " pr.id ,"
                + " bc.decimalPoint, "
                + " con.locale, "
                + " pr.masterUid,"
                + " pr.shipmentUid, "
                + " pr.serviceName, "
                + " pr.totalCost, "
                + " pr.totalRevenue, "
                + " pr.netAmount, "
                + " pr.systemTrack.lastUpdatedDate)"
                + " from Provisional pr "
                + " left join pr.localCurrency bc "
                + " left join bc.countryMaster con ";

        String countQuery = "select count(pr.id)"
                + " from Provisional pr "
                + " left join pr.localCurrency bc "
                + " left join bc.countryMaster con ";

        if (dto.getMasterUid() != null && dto.getMasterUid().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }
            hql = hql + " Upper(pr.masterUid) like '" + dto.getMasterUid().toUpperCase() + "%' ";
        }

        if (dto.getShipmentUid() != null && dto.getShipmentUid().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }
            hql = hql + " Upper(pr.shipmentUid) like '" + dto.getShipmentUid().trim().toUpperCase() + "%' ";
        }

        if (dto.getServiceName() != null && dto.getServiceName().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }
            hql = hql + " Upper(pr.serviceName) like '" + dto.getServiceName().trim().toUpperCase() + "%' ";
        }

        if (dto.getTotalCost() != null && dto.getTotalCost().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }
            hql = hql + " Upper(pr.totalCost) like '" + dto.getTotalCost() + "%' ";
        }

        if (dto.getTotalRevenue() != null && dto.getTotalRevenue().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }
            hql = hql + " Upper(pr.totalRevenue) like '" + dto.getTotalRevenue() + "%' ";
        }

        if (dto.getNetAmount() != null && dto.getNetAmount().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }
            hql = hql + " Upper(pr.netAmount) like '" + dto.getNetAmount() + "%' ";
        }

        if (dto.getLastUpdatedOn() != null && dto.getLastUpdatedOn().getStartDate() != null
                && dto.getLastUpdatedOn().getEndDate() != null) {

            try {
                Date dt1 = sdf.parse(dto.getLastUpdatedOn().getStartDate());
                Date dt2 = sdf.parse(dto.getLastUpdatedOn().getEndDate());

                if (hql.trim().length() != 0) {
                    hql = hql + " AND ";
                }

                params.put("lastUpdatedStartDate", dt1);
                params.put("lastUpdatedEndDate", dt2);

                log.info("dt1 " + dt1 + " dt2 " + dt2);
                hql = hql
                        + " ( pr.systemTrack.lastUpdatedDate >= :lastUpdatedStartDate AND pr.systemTrack.lastUpdatedDate  <= :lastUpdatedEndDate ) ";

            } catch (ParseException e1) {
                log.error("Date Conversion error : ", e1);
            }

        }

        if (hql.trim().length() != 0) {
            hql = " where " + hql;
        }

        String orderByQuery = "";

        if (dto.getSortByColumn() != null && dto.getSortByColumn().length() != 0) {

            if (dto.getSortByColumn().equalsIgnoreCase("masterUid")) {
                orderByQuery = orderByQuery + "pr.masterUid";
            } else if (dto.getSortByColumn().equalsIgnoreCase("shipmentUid")) {
                orderByQuery = orderByQuery + "pr.shipmentUid";
            } else if (dto.getSortByColumn().equalsIgnoreCase("serviceName")) {
                orderByQuery = orderByQuery + "pr.serviceName";
            } else if (dto.getSortByColumn().equalsIgnoreCase("totalCost")) {
                orderByQuery = orderByQuery + "pr.totalCost";
            } else if (dto.getSortByColumn().equalsIgnoreCase("partyName")) {
                orderByQuery = orderByQuery + "pr.totalRevenue";
            } else if (dto.getSortByColumn().equalsIgnoreCase("accountCode")) {
                orderByQuery = orderByQuery + "pr.netAmount";
            } else if (dto.getSortByColumn().equalsIgnoreCase("lastUpdatedDate")) {
                orderByQuery = orderByQuery + "pr.systemTrack.lastUpdatedDate";
            }
        }

        if (orderByQuery.length() != 0) {
            orderByQuery = " ORDER BY " + orderByQuery + " " + dto.getOrderByType();
        } else {
            orderByQuery = " order by pr.id DESC ";
        }

        countQuery = countQuery + hql;
        Query countQ = em.createQuery(countQuery);

        log.info("HQL Count Query For Search : " + countQuery);
        log.info("HQL params For Search : " + params);
        for (Entry<String, Object> e : params.entrySet()) {

            countQ.setParameter(e.getKey(), e.getValue());
        }
        Long countTotal = (Long) countQ.getSingleResult();
        log.info("Total Number of Records Found : " + countTotal);

        searchQuery = searchQuery + hql + orderByQuery;
        log.info("HQL Query For Search : " + searchQuery);
        Query query = em.createQuery(searchQuery);

        query.setFirstResult(dto.getSelectedPageNumber() * dto.getRecordPerPage());
        query.setMaxResults(dto.getRecordPerPage());

        for (Entry<String, Object> e : params.entrySet()) {
            query.setParameter(e.getKey(), e.getValue());
        }
        return new SearchRespDto(countTotal, query.getResultList());

    }

}
