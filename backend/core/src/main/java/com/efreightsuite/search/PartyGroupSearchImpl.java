package com.efreightsuite.search;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import com.efreightsuite.dto.PartyGroupSearchDto;
import com.efreightsuite.dto.SearchRequest;
import com.efreightsuite.dto.SearchRespDto;
import com.efreightsuite.model.PartyGroupMaster;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class PartyGroupSearchImpl {

    private static final String AND = " AND ";
    @Autowired
    private
    EntityManager em;

    public SearchRespDto search(SearchRequest dto) {

        String hql = " from PartyGroupMaster where status!='Hide' ";

        if (dto.getKeyword() != null && dto.getKeyword().trim().length() != 0) {


            hql = hql + " AND (";

            hql = hql + " Upper(partyGroupCode) like '" + dto.getKeyword().trim().toUpperCase() + "%' ";

            hql = hql + " OR ";

            hql = hql + " Upper(partyGroupName) like '" + dto.getKeyword().trim().toUpperCase() + "%' ";

            hql = hql + " ) ";

        }


        hql = hql + " order by partyGroupName ASC";

        log.info("PartyGroupMaster Search hql:[" + hql + "]");

        return search(dto.getSelectedPageNumber(), dto.getRecordPerPage(), hql);

    }


    public SearchRespDto search(PartyGroupSearchDto partyGroupSearchDto) {


        String hql = "";


        if (StringUtils.isNotEmpty(partyGroupSearchDto.getSearchPartyGroupName())) {
            hql = hql + " Upper(partyGroupName) like '" + partyGroupSearchDto.getSearchPartyGroupName().trim().toUpperCase() + "%' ";
        }

        if (StringUtils.isNotEmpty(partyGroupSearchDto.getSearchPartyGroupCode())) {
            if (hql.trim().length() != 0) {
                hql = hql + AND;
            }

            hql = hql + " Upper(partyGroupCode) like '" + partyGroupSearchDto.getSearchPartyGroupCode().trim().toUpperCase() + "%' ";
        }

        if (StringUtils.isNotEmpty(partyGroupSearchDto.getSearchStatus())) {

            if (hql.trim().length() != 0) {
                hql = hql + AND;
            }

            hql = hql + " Upper(status) like '" + partyGroupSearchDto.getSearchStatus().trim().toUpperCase() + "%' ";
        }


        if (StringUtils.isNotEmpty(partyGroupSearchDto.getSearchSalesLeadLocation())) {

            if (hql.trim().length() != 0) {
                hql = hql + AND;
            }

            hql = hql + " salesLeadLocation.id IN (select loc.id from LocationMaster loc where Upper(loc.locationName) like '" + partyGroupSearchDto.getSearchSalesLeadLocation().trim().toUpperCase() + "%') ";
        }

        if (hql.trim().length() != 0) {
            hql = " where " + hql;
        }

        hql = " from PartyGroupMaster " + hql;


        if (StringUtils.isNotEmpty(partyGroupSearchDto.getSortByColumn()) && StringUtils.isNotEmpty(partyGroupSearchDto.getOrderByType())) {
            hql = hql + " order by Upper(" + partyGroupSearchDto.getSortByColumn().trim() + ") " + partyGroupSearchDto.getOrderByType().trim();
        } else {
            hql = hql + " order by Upper(partyGroupName) ASC";
        }


        log.info("PartyGroupMaster Search hql:[" + hql + "]");


        return search(partyGroupSearchDto.getSelectedPageNumber(), partyGroupSearchDto.getRecordPerPage(), hql);
    }


    private SearchRespDto search(int selectedPageNumber, int recordPerPage, String hql) {

        String countQ = "Select count (id) " + hql;

        Query countQuery = em.createQuery(countQ);
        Long countTotal = (Long) countQuery.getSingleResult();

        TypedQuery<PartyGroupMaster> query = em.createQuery(hql, PartyGroupMaster.class);

        query.setFirstResult(selectedPageNumber * recordPerPage);

        query.setMaxResults(recordPerPage);

        List<PartyGroupMaster> resultlist = query.getResultList();

        SearchRespDto searchRespDto = new SearchRespDto(countTotal, resultlist);

        log.info(resultlist.size() + " number of rows are selected..");

        return searchRespDto;
    }


}
