package com.efreightsuite.search;

import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.efreightsuite.dto.SearchRespDto;
import com.efreightsuite.dto.WinConfigurationRequestDto;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
@Log4j2
public class WinConfigurationImpl {

    @Autowired
    private
    EntityManager em;

    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:SS");


    public SearchRespDto search(WinConfigurationRequestDto searchDto) {


        Map<String, Object> params = new HashMap<>();


        String searchQuery = "select new com.efreightsuite.dto.WinConfigurationResponseDto"
                + " (wr.id, wr.errorCode, wr.errorMessage,ws.awbID,ws.mawbId,ws.awbNumber,ws.winStatus,ws.consolUid) "
                + " from WinResponseRemarks wr "
                + " left join wr.winWebResponse ws ";

        String countQuery = "select count(wr.id) from WinResponseRemarks wr "
                + " left join wr.winWebResponse ws ";


        String filterQuery = "";

        if (searchDto.getErrorCode() != null
                && searchDto.getErrorCode().length() != 0) {

            if (filterQuery.trim().length() != 0) {
                filterQuery = filterQuery + " AND ";
            }

            filterQuery = filterQuery + " Upper(wr.errorCode) like '"
                    + searchDto.getErrorCode().trim().toUpperCase() + "%' ";
        }

        if (searchDto.getErrorMessage() != null
                && searchDto.getErrorMessage().length() != 0) {

            if (filterQuery.trim().length() != 0) {
                filterQuery = filterQuery + " AND ";
            }

            filterQuery = filterQuery + " Upper(wr.errorMessage) like '"
                    + searchDto.getErrorMessage().trim().toUpperCase() + "%' ";
        }

        if (String.valueOf(searchDto.getAwbID()) != null
                ) {
            if (filterQuery.trim().length() != 0) {
                filterQuery = filterQuery + " AND ";
            }
            filterQuery = filterQuery + " Upper(ws.awbID) like '"
                    + searchDto.getAwbID() + "%' ";
        }

        if (String.valueOf(searchDto.getMawbId()) != null
                ) {
            if (filterQuery.trim().length() != 0) {
                filterQuery = filterQuery + " AND ";
            }
            filterQuery = filterQuery + " Upper(ws.mawbId) like '"
                    + searchDto.getMawbId() + "%' ";
        }

        if (searchDto.getWinStatus() != null
                && searchDto.getWinStatus().length() != 0) {

            if (filterQuery.trim().length() != 0) {
                filterQuery = filterQuery + " AND ";
            }
            filterQuery = filterQuery
                    + " Upper(ws.winStatus) like '"
                    + searchDto.getWinStatus().trim().toUpperCase() + "%' ";
        }

        if (searchDto.getConsolUid() != null) {

            if (filterQuery.trim().length() != 0) {
                filterQuery = filterQuery + " AND ";
            }

            filterQuery = filterQuery
                    + " Upper(ws.consolUid) like '"
                    + searchDto.getConsolUid() + "%' ";
        }

/*		if (searchDto.getEtd() != null && searchDto.getEtd().getStartDate() != null
                && searchDto.getEtd().getEndDate() != null) {

			try {

				Date date1 = sdf.parse(searchDto.getEtd().getStartDate());
				Date date2 = sdf.parse(searchDto.getEtd().getEndDate());

				if (filterQuery.trim().length() != 0) {
					filterQuery = filterQuery + " AND ";
				}

				params.put("startDate", date1);
				params.put("endDate", date2);

				filterQuery = filterQuery
						+ "( aes.etd  >= :startDate AND aes.etd  <= :endDate )";

			} catch (ParseException exception) {
				log.error("Date Conversion Exception in etd....", exception);
			}

		}
*/

        if (filterQuery.trim().length() != 0) {
            filterQuery = " WHERE " + filterQuery;
        }

        // Building Order By Query

        String orderByQuery = "";

        if (searchDto.getSortByColumn() != null
                && searchDto.getSortByColumn().length() != 0) {

            if (searchDto.getSortByColumn().equalsIgnoreCase("errorCode")) {
                orderByQuery = orderByQuery + "wr.errorCode";
            } else if (searchDto.getSortByColumn().equalsIgnoreCase("errorMessage")) {
                orderByQuery = orderByQuery + "wr.errorMessage";
            } else if (searchDto.getSortByColumn().equalsIgnoreCase("awbID")) {
                orderByQuery = orderByQuery + "wr.awbID";
            } else if (searchDto.getSortByColumn().equalsIgnoreCase("mawbId")) {
                orderByQuery = orderByQuery
                        + "ws.mawbId";
            } else if (searchDto.getSortByColumn().equalsIgnoreCase("winStatus")) {
                orderByQuery = orderByQuery
                        + "ws.winStatus";
            } else if (searchDto.getSortByColumn().equalsIgnoreCase(
                    "consolUid")) {
                orderByQuery = orderByQuery + "ws.consolUid";
            }

        }

        if (orderByQuery.length() != 0) {
            orderByQuery = " ORDER BY Upper(" + orderByQuery + ") " + searchDto.getOrderByType();
        } else {
            orderByQuery = " ORDER BY Upper(ws.awbID) DESC ";
        }


        countQuery = countQuery + filterQuery;
        Query countQ = em.createQuery(countQuery);
        for (Entry<String, Object> e : params.entrySet()) {
            countQ.setParameter(e.getKey(), e.getValue());
        }
        Long countTotal = (Long) countQ.getSingleResult();
        log.info("HQL Count Query For Search : " + countQuery);
        log.info("Total Number of Records Found : " + countTotal);

        searchQuery = searchQuery + filterQuery + orderByQuery;
        log.info("HQL Query For Search : " + searchQuery);
        Query query = em.createQuery(searchQuery);

        query.setFirstResult(searchDto.getSelectedPageNumber()
                * searchDto.getRecordPerPage());
        query.setMaxResults(searchDto.getRecordPerPage());

        for (Entry<String, Object> e : params.entrySet()) {
            query.setParameter(e.getKey(), e.getValue());
        }

        return new SearchRespDto(countTotal,
                query.getResultList());

    }

}
