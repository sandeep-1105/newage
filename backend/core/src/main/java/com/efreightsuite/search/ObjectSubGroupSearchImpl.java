package com.efreightsuite.search;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import com.efreightsuite.dto.ObjectSubGroupMasterSearchDto;
import com.efreightsuite.dto.SearchRequest;
import com.efreightsuite.dto.SearchRespDto;
import com.efreightsuite.model.ObjectSubGroupMaster;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class ObjectSubGroupSearchImpl {

    @Autowired
    private
    EntityManager em;


    public SearchRespDto search(SearchRequest dto, Long groupId) {

        String hql = " from ObjectSubGroupMaster where status!='Hide' ";

        if (dto.getKeyword() != null && dto.getKeyword().trim().length() != 0) {

            hql = hql + " AND ( ";

            hql = hql + " Upper(objectSubGroupCode) like '" + dto.getKeyword().trim().toUpperCase() + "%' ";

            hql = hql + " OR ";

            hql = hql + " Upper(objectSubGroupName) like '" + dto.getKeyword().trim().toUpperCase() + "%' ";

            hql = hql + " ) ";

        }

        if (groupId != null) {
            hql = hql + " AND objectGroupMaster.id = " + groupId + " ";
        }

        hql = hql + " order by objectSubGroupName ASC";

        log.info("ObjectSubGroup Master Search hql:[" + hql + "]");

        return search(dto.getSelectedPageNumber(), dto.getRecordPerPage(), hql);
    }


    public SearchRespDto search(ObjectSubGroupMasterSearchDto dto) {

        String hql = "";

        if (dto.getSearchObjectGroup() != null && dto.getSearchObjectGroup().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }

            hql = hql + " objectGroupMaster.id IN (select cm.id from ObjectGroupMaster cm where Upper(cm.objectGroupName) like '" + dto.getSearchObjectGroup().trim().toUpperCase() + "%') ";

        }

        if (dto.getSearchObjectSubGroupCode() != null && dto.getSearchObjectSubGroupCode().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }

            hql = hql + " Upper(objectSubGroupCode) like '" + dto.getSearchObjectSubGroupCode().trim().toUpperCase() + "%' ";
        }

        if (dto.getSearchObjectSubGroupName() != null && dto.getSearchObjectSubGroupName().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }

            hql = hql + " Upper(objectSubGroupName) like '" + dto.getSearchObjectSubGroupName().trim().toUpperCase() + "%' ";
        }


        if (dto.getSearchStatus() != null && dto.getSearchStatus().toString().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }

            hql = hql + " Upper(status) like '" + dto.getSearchStatus().toString().toUpperCase() + "%' ";
        }


        if (hql.trim().length() != 0) {
            hql = " where " + hql;
        }

        hql = " from ObjectSubGroupMaster " + hql;


        if (dto.getSortByColumn() != null && dto.getOrderByType() != null && dto.getSortByColumn().trim().length() != 0
                && dto.getOrderByType().trim().length() != 0) {
            hql = hql + " order by Upper(" + dto.getSortByColumn().trim() + ") " + dto.getOrderByType().trim();
        } else {
            hql = hql + " order by Upper(objectSubGroupName) ASC";
        }

        log.info("ObjectSubGroup Master Search hql:[" + hql + "]");

        return search(dto.getSelectedPageNumber(), dto.getRecordPerPage(), hql);

    }


    private SearchRespDto search(int selectedPageNumber, int recordPerPage, String hql) {
        String countQ = "Select count (id) " + hql;

        Query countQuery = em.createQuery(countQ);

        Long countTotal = (Long) countQuery.getSingleResult();

        TypedQuery<ObjectSubGroupMaster> query = em.createQuery(hql, ObjectSubGroupMaster.class);

        query.setFirstResult(selectedPageNumber * recordPerPage);

        query.setMaxResults(recordPerPage);

        List<ObjectSubGroupMaster> resultlist = query.getResultList();

        SearchRespDto searchRespDto = new SearchRespDto(countTotal, resultlist);

        log.info("Number of rows selected : " + resultlist.size());

        return searchRespDto;

    }
}
