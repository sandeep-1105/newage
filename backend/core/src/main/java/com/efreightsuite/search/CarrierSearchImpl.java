package com.efreightsuite.search;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import com.efreightsuite.dto.CarrierSearchDto;
import com.efreightsuite.dto.SearchRequest;
import com.efreightsuite.dto.SearchRespDto;
import com.efreightsuite.model.CarrierAddressMapping;
import com.efreightsuite.model.CarrierEdiMapping;
import com.efreightsuite.model.CarrierMaster;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class CarrierSearchImpl {

    @Autowired
    private
    EntityManager em;

    public SearchRespDto search(CarrierSearchDto dto) {

        String hql = "";

        if (dto.getSearchCarrierCode() != null && dto.getSearchCarrierCode().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }

            hql = hql + " Upper(carrierCode) like '" + dto.getSearchCarrierCode().trim().toUpperCase() + "%' ";
        }

        if (dto.getSearchCarrierName() != null && dto.getSearchCarrierName().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }

            hql = hql + " Upper(carrierName) like '" + dto.getSearchCarrierName().trim().toUpperCase() + "%' ";
        }

        if (dto.getSearchTransportMode() != null && dto.getSearchTransportMode().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }

            hql = hql + " Upper(transportMode) like '" + dto.getSearchTransportMode().trim().toUpperCase() + "%' ";
        }

        if (dto.getSearchCarrierNo() != null && dto.getSearchCarrierNo().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }

            hql = hql + " Upper(carrierNo) like '" + dto.getSearchCarrierNo().trim().toUpperCase() + "%' ";
        }

        if (dto.getSearchScacCode() != null && dto.getSearchScacCode().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }

            hql = hql + " Upper(scacCode) like '" + dto.getSearchScacCode().trim().toUpperCase() + "%' ";
        }

        if (dto.getSearchIataCode() != null && dto.getSearchIataCode().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }

            hql = hql + " Upper(iataCode) like '" + dto.getSearchIataCode().trim().toUpperCase() + "%' ";
        }


        if (dto.getSearchStatus() != null && dto.getSearchStatus().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }

            hql = hql + " Upper(status) like '" + dto.getSearchStatus().trim().toUpperCase() + "%' ";
        }


        if (hql.trim().length() != 0) {
            hql = " where " + hql;
        }

        hql = " from CarrierMaster " + hql;


        if (dto.getSortByColumn() != null && dto.getOrderByType() != null && dto.getSortByColumn().trim().length() != 0 && dto.getOrderByType().trim().length() != 0) {
            hql = hql + " order by Upper(" + dto.getSortByColumn().trim() + ") " + dto.getOrderByType().trim();
        } else {
            hql = hql + " order by Upper(carrierName) ASC";
        }


        log.info("Carrier Master Search hql:[" + hql + "]");

        return search(dto.getSelectedPageNumber(), dto.getRecordPerPage(), hql);

    }

    public SearchRespDto search(SearchRequest dto) {

        String hql = " from CarrierMaster  where status!='Hide' ";

        if (dto.getKeyword() != null && dto.getKeyword().trim().length() != 0) {

            hql = hql + " AND ( ";

            hql = hql + " UPPER(carrierName) like '" + dto.getKeyword().trim().toUpperCase() + "%' ";

            hql = hql + " OR ";

            hql = hql + " UPPER(carrierCode) like '" + dto.getKeyword().trim().toUpperCase() + "%' ";

            hql = hql + " ) ";
        }


        hql = hql + " order by carrierName ASC";


        log.info("Carrier Master Search hql:[" + hql + "]");

        return search(dto.getSelectedPageNumber(), dto.getRecordPerPage(), hql);

    }

    public SearchRespDto search(SearchRequest dto, String transportMode) {

        String hql = " from CarrierMaster where status!='Hide' ";

        if (dto.getKeyword() != null && dto.getKeyword().trim().length() != 0) {

            hql = hql + " AND ( ";

            hql = hql + " UPPER(carrierName) like '" + dto.getKeyword().trim().toUpperCase() + "%' ";

            hql = hql + " OR ";

            hql = hql + " UPPER(carrierCode) like '" + dto.getKeyword().trim().toUpperCase() + "%' ";

            hql = hql + " ) ";
        }


        if (transportMode != null) {
            hql = hql + " AND transportMode = '" + transportMode + "' ";
        }

        hql = hql + " order by carrierName ASC";

        log.info("Carrier Master Search hql:[" + hql + "]");

        return search(dto.getSelectedPageNumber(), dto.getRecordPerPage(), hql);
    }


    private SearchRespDto search(int selectedPageNumber, int recordPerPage, String hql) {

        String countQ = "Select count (id) " + hql;
        Query countQuery = em.createQuery(countQ);

        Long countTotal = (Long) countQuery.getSingleResult();

        TypedQuery<CarrierMaster> query = em.createQuery(hql, CarrierMaster.class);

        query.setFirstResult(selectedPageNumber * recordPerPage);

        query.setMaxResults(recordPerPage);

        List<CarrierMaster> resultlist = query.getResultList();

        log.info("Number of rows Selected : " + resultlist.size());

        for (CarrierMaster obj : resultlist) {
            if (obj.getEdiList() != null && obj.getEdiList().size() != 0) {
                for (CarrierEdiMapping cem : obj.getEdiList()) {
                    cem.setCarrierMaster(null);
                }
            } else {
                obj.setEdiList(null);
            }

            if (obj.getAddressList() != null && obj.getAddressList().size() != 0) {
                for (CarrierAddressMapping cam : obj.getAddressList()) {
                    cam.setCarrierMaster(null);
                    cam.getLocationMaster().setPartyMaster(null);
                }
            } else {
                obj.setAddressList(null);
            }
        }

        return new SearchRespDto(countTotal, resultlist);
    }
}
