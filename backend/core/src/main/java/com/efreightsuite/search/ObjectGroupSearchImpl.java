package com.efreightsuite.search;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import com.efreightsuite.dto.ObjectGroupMasterSearchDto;
import com.efreightsuite.dto.SearchRequest;
import com.efreightsuite.dto.SearchRespDto;
import com.efreightsuite.model.ObjectGroupMaster;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class ObjectGroupSearchImpl {

    @Autowired
    private
    EntityManager em;


    public SearchRespDto search(SearchRequest dto) {

        String hql = " from ObjectGroupMaster where status!='Hide' ";

        if (dto.getKeyword() != null && dto.getKeyword().trim().length() != 0) {

            hql = hql + " AND ( ";

            hql = hql + " Upper(objectGroupCode) like '" + dto.getKeyword().trim().toUpperCase() + "%' ";

            hql = hql + " OR ";

            hql = hql + " Upper(objectGroupName) like '" + dto.getKeyword().trim().toUpperCase() + "%' ";

            hql = hql + " ) ";

        }

        hql = hql + " order by objectGroupName ASC";

        log.info("ObjectGroup Master Search hql:[" + hql + "]");

        return search(dto.getSelectedPageNumber(), dto.getRecordPerPage(), hql);
    }


    public SearchRespDto search(ObjectGroupMasterSearchDto dto) {

        String hql = "";


        if (dto.getSearchObjectGroupCode() != null && dto.getSearchObjectGroupCode().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }

            hql = hql + " Upper(objectGroupCode) like '" + dto.getSearchObjectGroupCode().trim().toUpperCase() + "%' ";
        }

        if (dto.getSearchObjectGroupName() != null && dto.getSearchObjectGroupName().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }

            hql = hql + " Upper(objectGroupName) like '" + dto.getSearchObjectGroupName().trim().toUpperCase() + "%' ";
        }


        if (dto.getSearchStatus() != null && dto.getSearchStatus().toString().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }

            hql = hql + " Upper(status) like '" + dto.getSearchStatus().toString().toUpperCase() + "%' ";
        }


        if (hql.trim().length() != 0) {
            hql = " where " + hql;
        }

        hql = " from ObjectGroupMaster " + hql;


        if (dto.getSortByColumn() != null && dto.getOrderByType() != null && dto.getSortByColumn().trim().length() != 0
                && dto.getOrderByType().trim().length() != 0) {
            hql = hql + " order by Upper(" + dto.getSortByColumn().trim() + ") " + dto.getOrderByType().trim();
        } else {
            hql = hql + " order by Upper(objectGroupName) ASC";
        }

        log.info("ObjectGroup Master Search hql:[" + hql + "]");

        return search(dto.getSelectedPageNumber(), dto.getRecordPerPage(), hql);

    }


    private SearchRespDto search(int selectedPageNumber, int recordPerPage, String hql) {
        String countQ = "Select count (id) " + hql;

        Query countQuery = em.createQuery(countQ);

        Long countTotal = (Long) countQuery.getSingleResult();

        TypedQuery<ObjectGroupMaster> query = em.createQuery(hql, ObjectGroupMaster.class);

        query.setFirstResult(selectedPageNumber * recordPerPage);

        query.setMaxResults(recordPerPage);

        List<ObjectGroupMaster> resultlist = query.getResultList();

        SearchRespDto searchRespDto = new SearchRespDto(countTotal, resultlist);

        log.info("Number of rows selected : " + resultlist.size());

        return searchRespDto;

    }
}
