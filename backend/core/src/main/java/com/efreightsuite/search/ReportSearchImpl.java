package com.efreightsuite.search;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import com.efreightsuite.dto.ReportSearchDto;
import com.efreightsuite.dto.SearchRespDto;
import com.efreightsuite.model.Reports;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class ReportSearchImpl {

    @Autowired
    private
    EntityManager em;


    public SearchRespDto reportsEnquiry(ReportSearchDto searchDto) {
        log.info("ReportsSearchImpl -> ReportsSearchImpl  method is called....");

        String hql = "";

        if (searchDto.getReportName() != null && searchDto.getReportName().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }

            hql = hql + " Upper(reportName) like '" + searchDto.getReportName().trim().toUpperCase() + "%' ";
        }

        if (searchDto.getReportShortName() != null && searchDto.getReportShortName().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }

            hql = hql + " Upper(reportShortName) like '" + searchDto.getReportShortName().trim().toUpperCase() + "%' ";
        }

        if (hql.trim().length() != 0) {
            hql = " where " + hql;
        }

        if (searchDto.getReportKey() != null && searchDto.getReportKey().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }

            hql = hql + " Upper(reportKey) like '" + searchDto.getReportKey().trim().toUpperCase() + "%' ";
        }

        if (hql.trim().length() != 0) {
            hql = " where " + hql;
        }


        hql = " from Reports " + hql;

        if (searchDto.getSortByColumn() != null && searchDto.getOrderByType() != null && searchDto.getSortByColumn().trim().length() != 0 && searchDto.getOrderByType().trim().length() != 0) {
            hql = hql + " order by " + searchDto.getSortByColumn().trim() + " " + searchDto.getOrderByType().trim();
        } else {
            hql = hql + " order by reportName ASC";
        }


        //hql = " from Reports " + hql + " order by reportName";

        log.info("Reports Search hql:[" + hql + "] ");

        log.info("ReportsSearch Impl Method is completed ");

        return search(searchDto.getSelectedPageNumber(), searchDto.getRecordPerPage(), hql);

    }

    public SearchRespDto searchKey(ReportSearchDto searchDto) {
        log.info("ReportsSearchImpl -> Report Search key  method is called....");

        String hql = "";

        if (searchDto.getReportName() != null && searchDto.getReportName().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }

            hql = hql + " Upper(reportName) like '" + searchDto.getReportName().trim().toUpperCase() + "%' ";
        }
        if (searchDto.getReportKey() != null && searchDto.getReportKey().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }

            hql = hql + " Upper(reportKey) like '" + searchDto.getReportKey().trim().toUpperCase() + "%' ";
        }


        if (searchDto.getReportShortName() != null && searchDto.getReportShortName().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }

            hql = hql + " Upper(reportShortName) like '" + searchDto.getReportShortName().trim().toUpperCase() + "%' ";
        }

        if (hql.trim().length() != 0) {
            hql = " where " + hql;
        }


        hql = " from Reports " + hql;

        log.info("Report Search key hql:[" + hql + "]");

        return search(searchDto.getSelectedPageNumber(), searchDto.getRecordPerPage(), hql);
    }

    private SearchRespDto search(int selectedPageNumber, int recordPerPage, String hql) {
        log.info("ReportsSearch count method is called ");
        TypedQuery<Reports> query = em.createQuery(hql, Reports.class);

        long countTotal = query.getResultList().size();

        query.setFirstResult(selectedPageNumber * recordPerPage);

        query.setMaxResults(recordPerPage);

        List<Reports> resultlist = query.getResultList();

        SearchRespDto searchRespDto = new SearchRespDto(countTotal, resultlist);

        log.info(resultlist.size() + " number of rows selected...");

        return searchRespDto;
    }

}
