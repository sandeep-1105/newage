package com.efreightsuite.search;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import com.efreightsuite.dto.EmployeeSearchRequestDto;
import com.efreightsuite.dto.SearchRequest;
import com.efreightsuite.dto.SearchRespDto;
import com.efreightsuite.model.EmployeeMaster;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class EmployeeSearchImpl {

    @Autowired
    private
    EntityManager em;

    public SearchRespDto search(SearchRequest dto, boolean isSalesman) {

        String hql = " from EmployeeMaster WHERE employementStatus != 'RESIGNED' AND employementStatus != 'TERMINATED'";

        if (dto.getKeyword() != null && dto.getKeyword().trim().length() != 0) {

            hql = hql + " AND ( ";

            hql = hql + " upper(employeeName) like '" + dto.getKeyword().trim().toUpperCase() + "%' ";

            hql = hql + " OR ";

            hql = hql + " upper(employeeCode) like '" + dto.getKeyword().trim().toUpperCase() + "%' ";

            hql = hql + " ) ";
        }

        if (isSalesman) {
            hql = hql + " AND isSalesman = 'Yes' ";
        }

        hql = hql + " order by employeeName ASC";

        log.info("EmployeeMaster Search hql:[" + hql + "]");

        return search(dto.getSelectedPageNumber(), dto.getRecordPerPage(), hql);


    }

    private SearchRespDto search(int selectedPageNumber, int recordPerPage, String hql) {
        String countQ = "Select count (id) " + hql;

        Query countQuery = em.createQuery(countQ);

        Long countTotal = (Long) countQuery.getSingleResult();

        TypedQuery<EmployeeMaster> query = em.createQuery(hql, EmployeeMaster.class);

        query.setFirstResult(selectedPageNumber * recordPerPage);

        query.setMaxResults(recordPerPage);

        List<EmployeeMaster> resultlist = query.getResultList();

        return new SearchRespDto(countTotal, resultlist);
    }

    public SearchRespDto searchByCompany(SearchRequest dto, Long onlyCompany) {

        String hql = " from EmployeeMaster WHERE employementStatus != 'RESIGNED' AND employementStatus != 'TERMINATED'";

        if (dto.getKeyword() != null && dto.getKeyword().trim().length() != 0) {

            hql = hql + " AND ( ";

            hql = hql + " UPPER(employeeName) like '" + dto.getKeyword().trim().toUpperCase() + "%' ";

            hql = hql + " OR ";

            hql = hql + " UPPER(employeeCode) like '" + dto.getKeyword().trim().toUpperCase() + "%' ";

            hql = hql + " ) ";
        }

        if (onlyCompany != null) {
            hql = hql + " AND companyMaster.id = " + onlyCompany + " ";
        }

        hql = hql + " order by employeeName ASC";


        log.info("EmployeeMaster Master Search hql:[" + hql + "]");

        return search(dto.getSelectedPageNumber(), dto.getRecordPerPage(), hql);

    }


    public SearchRespDto search(SearchRequest dto, Long countryId, boolean isSalesMan) {

        String hql = " from EmployeeMaster WHERE employementStatus != 'RESIGNED' AND employementStatus != 'TERMINATED'";

        if (dto.getKeyword() != null && dto.getKeyword().trim().length() != 0) {

            hql = hql + " AND ( ";

            hql = hql + " UPPER(employeeName) like '" + dto.getKeyword().trim().toUpperCase() + "%' ";

            hql = hql + " OR ";

            hql = hql + " UPPER(employeeCode) like '" + dto.getKeyword().trim().toUpperCase() + "%' ";

            hql = hql + " ) ";
        }
        if (isSalesMan) {
            hql = hql + " AND isSalesman = 'Yes' ";
        }
        if (countryId != null) {
            hql = hql + " AND locationMaster.countryMaster.id = " + countryId + " ";

        }
        hql = hql + " order by employeeName ASC";


        log.info("Emplyoee Master Search hql:[" + hql + "]");

        return search(dto.getSelectedPageNumber(), dto.getRecordPerPage(), hql);

    }


    public SearchRespDto search(EmployeeSearchRequestDto dto) {

        String searchQuery = "select new com.efreightsuite.dto.EmployeeSearchResponseDto"
                + " (e.id, e.employeeCode, e.employeeName, e.aliasName, e.employementStatus, "
                + " e.isSalesman, e.email, e.employeePhoneNo) " + " from EmployeeMaster e";

        String countQuery = "select count(e.id) from EmployeeMaster e ";

        String filterQuery = "";

        if (dto.getSearchEmployeeCode() != null && dto.getSearchEmployeeCode().trim().length() != 0) {
            if (filterQuery.trim().length() != 0) {
                filterQuery = filterQuery + " AND ";
            }

            filterQuery = filterQuery + " Upper(e.employeeCode) like '"
                    + dto.getSearchEmployeeCode().trim().toUpperCase() + "%' ";
        }

        if (dto.getSearchEmployeeName() != null && dto.getSearchEmployeeName().trim().length() != 0) {
            if (filterQuery.trim().length() != 0) {
                filterQuery = filterQuery + " AND ";
            }

            filterQuery = filterQuery + " Upper(e.employeeName) like '"
                    + dto.getSearchEmployeeName().trim().toUpperCase() + "%' ";
        }

        if (dto.getSearchAliasName() != null && dto.getSearchAliasName().trim().length() != 0) {
            if (filterQuery.trim().length() != 0) {
                filterQuery = filterQuery + " AND ";
            }

            filterQuery = filterQuery + " Upper(e.aliasName) like '" + dto.getSearchAliasName().trim().toUpperCase()
                    + "%' ";
        }

        if (dto.getSearchEmploymentStatus() != null && dto.getSearchEmploymentStatus().trim().length() != 0) {
            if (filterQuery.trim().length() != 0) {
                filterQuery = filterQuery + " AND ";
            }

            filterQuery = filterQuery + " Upper(e.employementStatus) like '" + dto.getSearchEmploymentStatus().trim().toUpperCase() + "%' ";
        }

        if (dto.getSearchIsSalesman() != null && dto.getSearchIsSalesman().trim().length() != 0) {
            if (filterQuery.trim().length() != 0) {
                filterQuery = filterQuery + " AND ";
            }

            filterQuery = filterQuery + " Upper(e.isSalesman) like '" + dto.getSearchIsSalesman().trim().toUpperCase() + "%' ";
        }

        if (dto.getSearchEmail() != null && dto.getSearchEmail().trim().length() != 0) {
            if (filterQuery.trim().length() != 0) {
                filterQuery = filterQuery + " AND ";
            }

            filterQuery = filterQuery + " Upper(e.email) like '" + dto.getSearchEmail().trim().toUpperCase() + "%' ";
        }

        if (dto.getSearchPhoneNumber() != null && dto.getSearchPhoneNumber().trim().length() != 0) {
            if (filterQuery.trim().length() != 0) {
                filterQuery = filterQuery + " AND ";
            }

            filterQuery = filterQuery + " Upper(e.employeePhoneNo) like '" + dto.getSearchPhoneNumber().trim().toUpperCase() + "%' ";
        }

        if (filterQuery.trim().length() != 0) {
            filterQuery = " WHERE " + filterQuery;
        }

        String orderByQuery = "";

        if (dto.getSortByColumn() != null && dto.getSortByColumn().length() != 0) {

            if (dto.getSortByColumn().equalsIgnoreCase("employeeCode")) {
                orderByQuery = orderByQuery + "e.employeeCode";
            } else if (dto.getSortByColumn().equalsIgnoreCase("employeeName")) {
                orderByQuery = orderByQuery + "e.employeeName";
            } else if (dto.getSortByColumn().equalsIgnoreCase("aliasName")) {
                orderByQuery = orderByQuery + "e.aliasName";
            } else if (dto.getSortByColumn().equalsIgnoreCase("employementStatus")) {
                orderByQuery = orderByQuery + "e.employementStatus";
            } else if (dto.getSortByColumn().equalsIgnoreCase("isSalesman")) {
                orderByQuery = orderByQuery + "e.isSalesman";
            } else if (dto.getSortByColumn().equalsIgnoreCase("email")) {
                orderByQuery = orderByQuery + "e.email";
            } else if (dto.getSortByColumn().equalsIgnoreCase("employeePhoneNo")) {
                orderByQuery = orderByQuery + "e.employeePhoneNo";
            } else {
                orderByQuery = orderByQuery + "e.employeeCode";
            }
        }

        if (orderByQuery.length() != 0) {
            orderByQuery = " ORDER BY Upper(" + orderByQuery + ") " + dto.getOrderByType();
        } else {
            orderByQuery = " ORDER BY Upper(e.employeeCode) ASC";
        }

        countQuery = countQuery + filterQuery;

        Query countQ = em.createQuery(countQuery);

        Long countTotal = (Long) countQ.getSingleResult();

        searchQuery = searchQuery + filterQuery + orderByQuery;

        Query query = em.createQuery(searchQuery);

        query.setFirstResult(dto.getSelectedPageNumber() * dto.getRecordPerPage());

        query.setMaxResults(dto.getRecordPerPage());

        return new SearchRespDto(countTotal, query.getResultList());
    }

}
