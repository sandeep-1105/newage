package com.efreightsuite.search;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import com.efreightsuite.dto.SearchRequest;
import com.efreightsuite.dto.SearchRespDto;
import com.efreightsuite.dto.TriggerMasterSearchDto;
import com.efreightsuite.model.TriggerMaster;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class TriggerMasterImpl {

    @Autowired
    private
    EntityManager em;

    public SearchRespDto search(TriggerMasterSearchDto dto) {

        String hql = "";

        if (dto.getSearchTriggerCode() != null && dto.getSearchTriggerCode().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }

            hql = hql + " UPPER(triggerCode) like '" + dto.getSearchTriggerCode().trim().toUpperCase() + "%' ";
        }

        if (dto.getSearchTriggerName() != null && dto.getSearchTriggerName().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }

            hql = hql + " UPPER(triggerName) like '" + dto.getSearchTriggerName().trim().toUpperCase() + "%' ";
        }

        if (dto.getSearchMailSubject() != null && dto.getSearchMailSubject().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }

            hql = hql + " UPPER(mailSubject) like '" + dto.getSearchMailSubject().trim().toUpperCase() + "%' ";
        }

        if (dto.getSearchTriggerType() != null && dto.getSearchTriggerType().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }

            hql = hql + " UPPER(triggerTypeMaster.triggerTypeCode) like '" + dto.getSearchTriggerType().trim().toUpperCase() + "%' ";
        }


        if (dto.getSearchUseForFinance() != null && dto.getSearchUseForFinance().toString().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }

            hql = hql + " UPPER(useForFinance) like '" + dto.getSearchUseForFinance().toString().trim().toUpperCase() + "%' ";
        }

        if (dto.getSearchStatus() != null && dto.getSearchStatus().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }

            hql = hql + " UPPER(status) like '" + dto.getSearchStatus().trim().toUpperCase() + "%' ";
        }

        if (hql.trim().length() != 0) {
            hql = " where " + hql;
        }

        hql = " from TriggerMaster " + hql;


        if (dto.getSortByColumn() != null && dto.getOrderByType() != null && dto.getSortByColumn().trim().length() != 0 && dto.getOrderByType().trim().length() != 0) {
            hql = hql + " order by Upper(" + dto.getSortByColumn().trim() + ") " + dto.getOrderByType().trim();
        } else {
            hql = hql + " order by Upper(triggerName) ASC";
        }


        log.info("Trigger Type Master Search hql:[" + hql + "]");


        return search(dto.getSelectedPageNumber(), dto.getRecordPerPage(), hql);

    }


    public SearchRespDto search(SearchRequest dto, String triggerType) {

        String hql = " from TriggerMaster where status!='HIDE' ";

        if (dto.getKeyword() != null && dto.getKeyword().trim().length() != 0) {

            hql = hql + " AND ( ";

            hql = hql + " UPPER(triggerName) like '" + dto.getKeyword().trim().toUpperCase() + "%' ";

            hql = hql + " OR ";

            hql = hql + " UPPER(triggerCode) like '" + dto.getKeyword().trim().toUpperCase() + "%' ";

            hql = hql + " ) ";
        }

        if (triggerType != null && triggerType.trim().length() != 0) {

            hql = hql + " AND UPPER(triggerTypeMaster.triggerTypeName) like '" + triggerType.trim().toUpperCase() + "%' ";
        }

        hql = hql + " order by triggerName ASC";

        log.info("Trigger Master Search hql:[" + hql + "]");

        return search(dto.getSelectedPageNumber(), dto.getRecordPerPage(), hql);
    }


    private SearchRespDto search(int selectedPageNumber, int recordPerPage, String hql) {

        String countQ = "Select count (id) " + hql;

        Query countQuery = em.createQuery(countQ);

        Long countTotal = (Long) countQuery.getSingleResult();

        TypedQuery<TriggerMaster> query = em.createQuery(hql, TriggerMaster.class);

        query.setFirstResult(selectedPageNumber * recordPerPage);

        query.setMaxResults(recordPerPage);

        List<TriggerMaster> resultlist = query.getResultList();

        SearchRespDto searchRespDto = new SearchRespDto(countTotal, resultlist);

        log.info("Number of rows selected : " + resultlist.size());

        return searchRespDto;
    }

}
