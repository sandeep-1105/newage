package com.efreightsuite.search;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import com.efreightsuite.dto.SearchRespDto;
import com.efreightsuite.dto.ServiceTaxChargeGroupDto;
import com.efreightsuite.model.ServiceTaxChargeGroupMaster;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class ServiceTaxChargeGroupSearchImpl {

    @Autowired
    private
    EntityManager em;

    public SearchRespDto search(ServiceTaxChargeGroupDto dto) {

        String hql = "";

        if (dto.getSearchServiceName() != null && dto.getSearchServiceName().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }

            hql = hql + " serviceMaster.id IN (select cm.id from ServiceMaster cm where Upper(cm.serviceName) like '" + dto.getSearchServiceName().trim().toUpperCase() + "%') ";

        }

        if (dto.getSearchChargeName() != null && dto.getSearchChargeName().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }

            hql = hql + " chargeMaster.id IN (select cm.id from ChargeMaster cm where Upper(cm.chargeName) like '" + dto.getSearchChargeName().trim().toUpperCase() + "%') ";

        }

        if (dto.getSearchCategoryName() != null && dto.getSearchCategoryName().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }

            hql = hql + " categoryMaster.id IN (select cm.id from CategoryMaster cm where Upper(cm.categoryName) like '" + dto.getSearchCategoryName().trim().toUpperCase() + "%') ";

        }

        if (hql.trim().length() != 0) {
            hql = " where " + hql;
        }


        hql = " from ServiceTaxChargeGroupMaster " + hql;

        if (dto.getSortByColumn() != null && dto.getOrderByType() != null && dto.getSortByColumn().trim().length() != 0 && dto.getOrderByType().trim().length() != 0) {
            hql = hql + " order by Upper(" + dto.getSortByColumn().trim() + ") " + dto.getOrderByType().trim();
        } else {

            hql = hql + " order by Upper(serviceMaster.serviceName) ASC";
        }

        log.info("Service Tax Charge Group Master Search hql:[" + hql + "]");

        return search(dto.getSelectedPageNumber(), dto.getRecordPerPage(), hql);
    }


    private SearchRespDto search(int selectedPageNumber, int recordPerPage, String hql) {

        String countQ = "Select count (id) " + hql;

        Query countQuery = em.createQuery(countQ);

        Long countTotal = (Long) countQuery.getSingleResult();

        TypedQuery<ServiceTaxChargeGroupMaster> query = em.createQuery(hql, ServiceTaxChargeGroupMaster.class);

        query.setFirstResult(selectedPageNumber * recordPerPage);

        query.setMaxResults(recordPerPage);

        List<ServiceTaxChargeGroupMaster> resultlist = query.getResultList();

        SearchRespDto searchRespDto = new SearchRespDto(countTotal, resultlist);

        log.info(resultlist.size() + " number of rows selected...");

        return searchRespDto;
    }
}
