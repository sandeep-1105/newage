package com.efreightsuite.search;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import com.efreightsuite.dto.DaybookSearchDto;
import com.efreightsuite.dto.SearchRequest;
import com.efreightsuite.dto.SearchRespDto;
import com.efreightsuite.model.DaybookMaster;
import com.efreightsuite.util.AppUtil;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class DaybookSearchImpl {

    @Autowired
    private
    EntityManager em;

    private final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:SS");

    public SearchRespDto search(DaybookSearchDto dto) {

        Map<String, Object> params = new HashMap<>();

        String hql = "";

        if (dto.getSearchDaybookCode() != null && dto.getSearchDaybookCode().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }

            hql = hql + " Upper(daybookCode) like '" + dto.getSearchDaybookCode().trim().toUpperCase() + "%' ";
        }

        if (dto.getSearchDaybookName() != null && dto.getSearchDaybookName().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }

            hql = hql + " Upper(daybookName) like '" + dto.getSearchDaybookName().trim().toUpperCase() + "%' ";
        }

        if (dto.getSearchLocationName() != null && dto.getSearchLocationName().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }

            hql = hql + " Upper(locationMaster.locationName) like '" + dto.getSearchLocationName().trim().toUpperCase() + "%' ";
        }

        if (dto.getSearcDocumentTypeName() != null && dto.getSearcDocumentTypeName().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }

            hql = hql + " Upper(documentTypeMaster.documentTypeName) like '" + dto.getSearcDocumentTypeName().trim().toUpperCase() + "%' ";
        }

        if (dto.getSearchCashAccountCode() != null && dto.getSearchCashAccountCode().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }

            hql = hql + " Upper(cashAccount.accountCode) like '" + dto.getSearchCashAccountCode().trim().toUpperCase() + "%' ";
        }

        if (dto.getSearchCashAccountName() != null && dto.getSearchCashAccountName().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }

            hql = hql + " Upper(cashAccount.accountName) like '" + dto.getSearchCashAccountName().trim().toUpperCase() + "%' ";
        }

        if (dto.getSearchBankAccountCode() != null && dto.getSearchBankAccountCode().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }

            hql = hql + " Upper(bankAccount.accountCode) like '" + dto.getSearchBankAccountCode().trim().toUpperCase() + "%' ";
        }

        if (dto.getSearchBankAccountName() != null && dto.getSearchBankAccountName().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }

            hql = hql + " Upper(bankAccount.accountName) like '" + dto.getSearchBankAccountName().trim().toUpperCase() + "%' ";
        }

        if (dto.getSearchBlockDate() != null && dto.getSearchBlockDate().getStartDate() != null && dto.getSearchBlockDate().getEndDate() != null) {

            try {
                Date dt1 = sdf.parse(dto.getSearchBlockDate().getStartDate());
                Date dt2 = sdf.parse(dto.getSearchBlockDate().getEndDate());

                if (hql.trim().length() != 0) {
                    hql = hql + " AND ";
                }

                params.put("fromStartDate", dt1);
                params.put("fromEndDate", dt2);
                hql = hql + " ( blockDate >= :fromStartDate AND blockDate <= :fromEndDate ) ";

            } catch (ParseException e1) {
                log.error("Date Conversion error in BlockDate: ", e1);
            }


        }

        if (dto.getSearchStatus() != null && dto.getSearchStatus().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }

            hql = hql + " Upper(status) like '" + dto.getSearchStatus().trim().toUpperCase() + "%' ";
        }

        if (hql.trim().length() != 0) {
            hql = " where " + hql;
        }


        hql = " from DaybookMaster d " + hql;


        if (dto.getSortByColumn() != null && dto.getOrderByType() != null && dto.getSortByColumn().trim().length() != 0 && dto.getOrderByType().trim().length() != 0) {
            hql = hql + " order by Upper(" + dto.getSortByColumn().trim() + ") " + dto.getOrderByType().trim();
        } else {
            hql = hql + " order by Upper(daybookName) ASC";
        }


        //hql = " from DaybookMaster " + hql + " order by daybookName";

        log.info("Daybook Master Search hql:[" + hql + "]");

        return search(dto.getSelectedPageNumber(), dto.getRecordPerPage(), hql, params);

    }

    public SearchRespDto searchByDocumentType(SearchRequest dto, Long documentTypeId) {

        String hql = " from DaybookMaster d where status!='Hide' and documentTypeMaster.id=" + documentTypeId;

        if (dto.getKeyword() != null && dto.getKeyword().trim().length() != 0) {

            hql = hql + " AND ( ";

            hql = hql + " Upper(daybookCode) like '" + dto.getKeyword().trim().toUpperCase() + "%' ";

            hql = hql + " OR ";

            hql = hql + " Upper(daybookName) like '" + dto.getKeyword().trim().toUpperCase() + "%' ";

            hql = hql + " ) ";

        }


        hql = hql + " order by daybookName ASC";


        log.info("Daybook Master Search hql:[" + hql + "]");


        return search(dto.getSelectedPageNumber(), dto.getRecordPerPage(), hql, null);
    }

    public SearchRespDto search(SearchRequest dto) {

        String hql = " from DaybookMaster d where status!='Hide' ";

        if (dto.getKeyword() != null && dto.getKeyword().trim().length() != 0) {

            hql = hql + " AND ( ";

            hql = hql + " Upper(daybookCode) like '" + dto.getKeyword().trim().toUpperCase() + "%' ";

            hql = hql + " OR ";

            hql = hql + " Upper(daybookName) like '" + dto.getKeyword().trim().toUpperCase() + "%' ";

            hql = hql + " ) ";

        }


        hql = hql + " order by daybookName ASC";


        log.info("Daybook Master Search hql:[" + hql + "]");


        return search(dto.getSelectedPageNumber(), dto.getRecordPerPage(), hql, null);
    }

    private SearchRespDto search(int selectedPageNumber, int recordPerPage, String hql, Map<String, Object> params) {

        String countQ = "Select count (id) " + hql;
        Query countQuery = em.createQuery(countQ);
        if (params != null) {
            for (Entry<String, Object> e : params.entrySet()) {
                countQuery.setParameter(e.getKey(), e.getValue());
            }
        }
        Long countTotal = (Long) countQuery.getSingleResult();

        String querySql = "select d " + hql;
        TypedQuery<DaybookMaster> query = em.createQuery(querySql, DaybookMaster.class);
        if (params != null) {
            for (Entry<String, Object> e : params.entrySet()) {
                query.setParameter(e.getKey(), e.getValue());
            }
        }
        query.setFirstResult(selectedPageNumber * recordPerPage);

        query.setMaxResults(recordPerPage);

        List<DaybookMaster> resultlist = query.getResultList();


        for (DaybookMaster dayBook : resultlist) {
            if (dayBook.getLocationMaster() != null)
                AppUtil.setPartyToNull(dayBook.getLocationMaster().getPartyMaster());
        }

        SearchRespDto searchRespDto = new SearchRespDto(countTotal, resultlist);

        log.info(resultlist.size() + " number of rows selected...");

        return searchRespDto;
    }

}
