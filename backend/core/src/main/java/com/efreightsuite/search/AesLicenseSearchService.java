package com.efreightsuite.search;

import java.util.List;

import com.efreightsuite.dto.SearchRequest;
import com.efreightsuite.dto.SearchRespDto;
import com.efreightsuite.enumeration.LovStatus;
import com.efreightsuite.model.AesLicense;
import com.efreightsuite.repository.AesLicenseRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import static java.util.stream.Collectors.toList;

@Service
public class AesLicenseSearchService {

    @Autowired
    private AesLicenseRepository repository;

    public SearchRespDto search(SearchRequest dto) {
        AesLicense probe = new AesLicense();
        probe.setLicenseCode(dto.getKeyword());
        probe.setLicenseType(dto.getKeyword());

        ExampleMatcher matcher = ExampleMatcher.matchingAny()
                .withIgnoreCase()
                .withIgnoreNullValues()
                .withIgnorePaths("versionLock")
                .withMatcher("licenseCode", ExampleMatcher.GenericPropertyMatcher::startsWith)
                .withMatcher("licenseType", ExampleMatcher.GenericPropertyMatcher::startsWith);

        Example<AesLicense> example = Example.of(probe, matcher);

        Sort sortCriteria = new Sort(Sort.Direction.ASC, "licenseType");

        Page<AesLicense> pageResult = repository.findAll(
                example,
                new PageRequest(dto.getSelectedPageNumber(), dto.getRecordPerPage(),
                        sortCriteria)
        );
        List<AesLicense> result = pageResult.getContent()
                .stream()
                .filter(bank -> bank.getStatus() != LovStatus.Hide)
                .collect(toList());
        return new SearchRespDto(pageResult.getTotalElements(), result);
    }

}
