package com.efreightsuite.search;

import java.util.List;

import com.efreightsuite.dto.SearchRequest;
import com.efreightsuite.dto.SearchRespDto;
import com.efreightsuite.enumeration.LovStatus;
import com.efreightsuite.model.AesTransportMode;
import com.efreightsuite.repository.AesTransportModeRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import static java.util.stream.Collectors.toList;

@Service
public class AesTransportModeSearchService {

    @Autowired
    private AesTransportModeRepository repository;

    public SearchRespDto search(SearchRequest dto) {
        AesTransportMode probe = new AesTransportMode();
        probe.setAesTransportName(dto.getKeyword());

        ExampleMatcher matcher = ExampleMatcher.matchingAny()
                .withIgnoreCase()
                .withIgnoreNullValues()
                .withIgnorePaths("versionLock")
                .withMatcher("aesTransportName", ExampleMatcher.GenericPropertyMatcher::startsWith);

        Example<AesTransportMode> example = Example.of(probe, matcher);

        Sort sortCriteria = new Sort(Sort.Direction.ASC, "aesTransportName");

        Page<AesTransportMode> pageResult = repository.findAll(
                example,
                new PageRequest(dto.getSelectedPageNumber(), dto.getRecordPerPage(),
                        sortCriteria)
        );
        List<AesTransportMode> result = pageResult.getContent()
                .stream()
                .filter(bank -> bank.getStatus() != LovStatus.Hide)
                .collect(toList());
        return new SearchRespDto(pageResult.getTotalElements(), result);
    }

}
