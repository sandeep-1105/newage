package com.efreightsuite.search;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import com.efreightsuite.dto.SearchRequest;
import com.efreightsuite.dto.SearchRespDto;
import com.efreightsuite.model.ProjectMaster;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
@Log4j2
public class ProjectSearchImpl {


    @Autowired
    private
    EntityManager em;

    public SearchRespDto search(SearchRequest dto) {

        String hql = " from ProjectMaster where status!='Hide' ";

        if (dto.getKeyword() != null && dto.getKeyword().trim().length() != 0) {

            hql = hql + " AND ( ";

            hql = hql + " UPPER(projectName) like '" + dto.getKeyword().trim().toUpperCase() + "%' ";

            hql = hql + " OR ";

            hql = hql + " UPPER(projectCode) like '" + dto.getKeyword().trim().toUpperCase() + "%' ";

            hql = hql + " ) ";
        }

        hql = hql + " order by projectName ASC";

        log.info("ProjectMaster Search hql:[" + hql + "]");

        return search(dto.getSelectedPageNumber(), dto.getRecordPerPage(), hql);
    }


    private SearchRespDto search(int selectedPageNumber, int recordPerPage, String hql) {

        String countQ = "Select count (id) " + hql;

        Query countQuery = em.createQuery(countQ);

        Long countTotal = (Long) countQuery.getSingleResult();

        TypedQuery<ProjectMaster> query = em.createQuery(hql, ProjectMaster.class);

        query.setFirstResult(selectedPageNumber * recordPerPage);

        query.setMaxResults(recordPerPage);

        List<ProjectMaster> resultlist = query.getResultList();

        log.info(resultlist.size() + " number of rows selected...");

        return new SearchRespDto(countTotal, resultlist);

    }

}
