package com.efreightsuite.search;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import com.efreightsuite.dto.EssentialChargeSearchDto;
import com.efreightsuite.dto.SearchRespDto;
import com.efreightsuite.model.EssentialChargeMaster;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class EssentialChargeSearchImpl {

    @Autowired
    private
    EntityManager em;

    public SearchRespDto search(EssentialChargeSearchDto dto) {

        String hql = "";

        if (dto.getServiceName() != null && dto.getServiceName().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }

            hql = hql + " UPPER(serviceMaster.serviceName) like '"
                    + dto.getServiceName().trim().toUpperCase() + "%' ";
        }

        if (dto.getChargeName() != null && dto.getChargeName().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }

            hql = hql + " UPPER(chargeMaster.chargeName) like '"
                    + dto.getChargeName().trim().toUpperCase() + "%') ";
        }

        if (dto.getOriginName() != null && dto.getOriginName().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }

            hql = hql + " UPPER(origin.portName) like '"
                    + dto.getOriginName().trim().toUpperCase() + "%') ";
        }

        if (dto.getDestinationName() != null && dto.getDestinationName().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }

            hql = hql + " UPPER(destination.portName) like '"
                    + dto.getDestinationName().trim().toUpperCase() + "%') ";
        }

        if (dto.getTosName() != null && dto.getTosName().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }

            hql = hql + " UPPER(tosMaster.tosName) like '"
                    + dto.getTosName().trim().toUpperCase() + "%') ";
        }


        if (dto.getCostOrRevenue() != null && dto.getCostOrRevenue().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }

            hql = hql + " crn = '" + dto.getCostOrRevenue().trim() + "' ";
        }

        if (dto.getPpcc() != null && dto.getPpcc().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }

            hql = hql + " ppcc = '" + dto.getPpcc().trim() + "' ";
        }

        if (dto.getRouted() != null && dto.getRouted().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }

            hql = hql + " routed = '" + dto.getRouted().trim() + "' ";
        }

        if (dto.getHazardous() != null && dto.getHazardous().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }

            hql = hql + " hazardous = '" + dto.getHazardous().trim() + "' ";
        }

        if (hql.trim().length() != 0) {
            hql = " where " + hql;
        }


        hql = " from EssentialChargeMaster " + hql;


        if (dto.getSortByColumn() != null && dto.getOrderByType() != null && dto.getSortByColumn().trim().length() != 0 && dto.getOrderByType().trim().length() != 0) {
            hql = hql + " order by Upper(" + dto.getSortByColumn().trim() + ") " + dto.getOrderByType().trim();
        } else {
            hql = hql + " order by Upper(serviceMaster.serviceName) ASC";
        }

        log.info("EssentialCharge Master Search hql:[" + hql + "]");

        return search(dto.getSelectedPageNumber(), dto.getRecordPerPage(), hql, true);

    }

    public SearchRespDto essentialcharges(EssentialChargeSearchDto dto) {

        String hql = " FROM EssentialChargeMaster WHERE serviceMaster.id = " + dto.getServiceId() + " AND origin.id = " + dto.getOriginId();


        if (dto.getDestinationId() != null) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }

            hql = hql + " destination.id = " + dto.getDestinationId();
        }

        if (dto.getTosId() != null) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }
            hql = hql + " tosMaster.id = " + dto.getTosId();
        }


        if (dto.getPpcc() != null && dto.getPpcc().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }

            hql = hql + " ppcc = '" + dto.getPpcc().trim() + "' ";
        }

        if (dto.getRouted() != null && dto.getRouted().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }

            hql = hql + " routed = '" + dto.getRouted().trim() + "' ";
        }

        if (dto.getHazardous() != null && dto.getHazardous().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }

            hql = hql + " hazardous = '" + dto.getHazardous().trim() + "' ";
        }


        hql = hql + " order by chargeMaster.chargeName ";


        log.info("EssentialCharge Master Search hql:[" + hql + "]");

        return search(dto.getSelectedPageNumber(), dto.getRecordPerPage(), hql, false);

    }

    private SearchRespDto search(int selectedPageNumber, int recordPerPage, String hql, boolean isLazy) {

        if (isLazy) {

            String countQ = "Select count (id) " + hql;

            Query countQuery = em.createQuery(countQ);

            Long countTotal = (Long) countQuery.getSingleResult();

            TypedQuery<EssentialChargeMaster> query = em.createQuery(hql, EssentialChargeMaster.class);

            query.setFirstResult(selectedPageNumber * recordPerPage);

            query.setMaxResults(recordPerPage);

            List<EssentialChargeMaster> resultlist = query.getResultList();

            SearchRespDto searchRespDto = new SearchRespDto(countTotal, resultlist);

            log.info(resultlist.size() + " number of rows selected...");

            return searchRespDto;

        } else {
            TypedQuery<EssentialChargeMaster> query = em.createQuery(hql, EssentialChargeMaster.class);
            List<EssentialChargeMaster> resultlist = query.getResultList();
            return new SearchRespDto(resultlist);
        }


    }


}
