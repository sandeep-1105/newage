package com.efreightsuite.search;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.efreightsuite.dto.JobLedgerReqDto;
import com.efreightsuite.dto.JobLedgerResChargeDto;
import com.efreightsuite.dto.JobLedgerResDocumentDto;
import com.efreightsuite.dto.JobLedgerResDto;
import com.efreightsuite.enumeration.CRN;
import com.efreightsuite.enumeration.DocumentType;
import com.efreightsuite.enumeration.YesNo;
import com.efreightsuite.model.Consol;
import com.efreightsuite.model.DocumentDetail;
import com.efreightsuite.model.InvoiceCreditNote;
import com.efreightsuite.model.InvoiceCreditNoteDetail;
import com.efreightsuite.model.ProvisionItem;
import com.efreightsuite.model.Provisional;
import com.efreightsuite.model.ShipmentCharge;
import com.efreightsuite.model.ShipmentLink;
import com.efreightsuite.model.ShipmentServiceDetail;
import com.efreightsuite.repository.ConsolRepository;
import com.efreightsuite.repository.DocumentDetailRepository;
import com.efreightsuite.repository.InvoiceCreditNoteRepository;
import com.efreightsuite.repository.ProvisionalRepository;
import com.efreightsuite.repository.ShipmentRepository;
import com.efreightsuite.repository.ShipmentServiceDetailRepository;
import com.efreightsuite.util.AppUtil;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class FinanceImpl {

    @Autowired
    private
    AppUtil appUtil;

    @Autowired
    private
    InvoiceCreditNoteRepository invoiceRepo;

    @Autowired
    private
    ConsolRepository consolRepo;

    @Autowired
    ShipmentRepository shipmentRepo;

    @Autowired
    private
    ShipmentServiceDetailRepository serviceRepo;

    @Autowired
    private
    DocumentDetailRepository documentRepo;

    @Autowired
    private
    ProvisionalRepository provisionalRepo;


    public JobLedgerResDto getJobLedger(JobLedgerReqDto requestDto) {

        JobLedgerResDto resp = new JobLedgerResDto();

        List<InvoiceCreditNote> documents = new ArrayList<>();
        Map<String, JobLedgerResChargeDto> mapCharge = new HashMap<>();
        Consol consol = null;
        String consolUid = null;
        List<String> serviceIds = new ArrayList<>();
        boolean isWhere = false;

        if (requestDto.getConsolUid() != null && requestDto.getConsolUid().trim().length() != 0) {

            consol = consolRepo.findByConsolUid(requestDto.getConsolUid());

            if (consol != null) {
                consolUid = consol.getConsolUid();
                getInvoiceCreditNote(documents, consol.getConsolUid(), null);

                if ((requestDto.getShipmentUid() == null || requestDto.getShipmentUid().trim().length() == 0)
                        && (requestDto.getDocumentUid() == null || requestDto.getDocumentUid().trim().length() == 0)) {

                    for (ShipmentLink sl : consol.getShipmentLinkList()) {
                        getInvoiceCreditNote(documents, consol.getConsolUid(), sl.getService().getServiceUid());
                        serviceIds.add(sl.getService().getServiceUid());
                    }

                } else if ((requestDto.getShipmentUid() != null && requestDto.getShipmentUid().trim().length() != 0)
                        || (requestDto.getDocumentUid() != null && requestDto.getDocumentUid().trim().length() != 0)) {
                    for (ShipmentLink sl : consol.getShipmentLinkList()) {
                        if (requestDto.getShipmentUid() != null && requestDto.getShipmentUid().trim().length() != 0) {
                            if (sl.getService().getShipmentUid().toUpperCase().equals(requestDto.getShipmentUid().toUpperCase())) {
                                getInvoiceCreditNote(documents, consol.getConsolUid(), sl.getService().getServiceUid());
                                serviceIds.add(sl.getService().getServiceUid());
                                break;
                            }
                        } else if (requestDto.getDocumentUid() != null && requestDto.getDocumentUid().trim().length() != 0) {
                            for (DocumentDetail dd : sl.getService().getDocumentList()) {
                                if (dd.getDocumentNo().toUpperCase().equals(requestDto.getDocumentUid().toUpperCase())) {
                                    getInvoiceCreditNote(documents, consol.getConsolUid(), sl.getService().getServiceUid());
                                    serviceIds.add(sl.getService().getServiceUid());
                                    break;
                                }
                            }
                        }
                    }
                }
            }

        } else if (requestDto.getDocumentUid() != null && requestDto.getDocumentUid().trim().length() != 0) {
            isWhere = true;
            DocumentDetail dd = documentRepo.findByDocumentNo(requestDto.getDocumentUid());
            if (dd != null) {
                getInvoiceCreditNote(documents, null, dd.getServiceUid());
                serviceIds.add(dd.getShipmentServiceDetail().getServiceUid());
            }
        } else if (requestDto.getServiceUid() != null && requestDto.getServiceUid().trim().length() != 0) {
            isWhere = true;
            getInvoiceCreditNote(documents, null, requestDto.getServiceUid());
            serviceIds.add(requestDto.getServiceUid());
        }


        assignToDocumentDto(resp, consol, documents);
        assignToMapChargeInInvoice(mapCharge, resp, consol, documents);
        if (serviceIds != null && serviceIds.size() != 0) {
            for (String serviceUid : serviceIds) {
                assignToMapChargeInProvision(mapCharge, resp, consolUid, serviceUid, isWhere);
            }
        }
        assignToChargetDto(resp, mapCharge);
        return resp;
    }

    private void assignToChargetDto(JobLedgerResDto resp, Map<String, JobLedgerResChargeDto> mapCharge) {


        double tDebit = 0.0, tCredit = 0.0, tPRevenue = 0.0, tPCost = 0.0, tPNeutral = 0.0,
                tARevenue = 0.0, tACost = 0.0, tANeutral = 0.0;

        for (String key : mapCharge.keySet()) {
            JobLedgerResChargeDto dto = mapCharge.get(key);

            tDebit = tDebit + dto.getAmountDebit();
            tCredit = tCredit + dto.getAmountCredit();

            tPRevenue = tPRevenue + dto.getProvisionalRevenue();
            tPCost = tPCost + dto.getProvisionalCost();
            tPNeutral = tPNeutral + dto.getProvisionalNeutral();

            tARevenue = tARevenue + dto.getActualRevenue();
            tACost = tACost + dto.getActualCost();
            tANeutral = tANeutral + dto.getActualNeutral();

            resp.getChargeWiseList().add(dto);
        }

        resp.setChargeTotalAmountDebit(tDebit);
        resp.setChargeTotalAmountCredit(tCredit);

        resp.setChargeTotalProvisionalRevenue(tPRevenue);
        resp.setChargeTotalProvisionalCost(tPCost);
        resp.setChargeTotalProvisionalNeutral(tPNeutral);

        resp.setChargeTotalActualRevenue(tARevenue);
        resp.setChargeTotalActualCost(tACost);
        resp.setChargeTotalActualNeutral(tANeutral);

        resp.setChargeProfitAmount(tCredit - tDebit);
        resp.setChargeProfitProvisional(tPRevenue - tPCost);
        resp.setChargeProfitActual(tARevenue - tACost);

        resp.setChargeGrossProfit((resp.getChargeProfitActual() * 100) / resp.getChargeTotalActualRevenue());

    }

    private void assignToMapChargeInProvision(Map<String, JobLedgerResChargeDto> mapCharge, JobLedgerResDto resp, String consolUid, String serviceUid, boolean isWhere) {

        List<ProvisionItem> items = new ArrayList<>();

        if (consolUid != null && !isWhere) {
            Provisional pro = provisionalRepo.findByMasterUid(consolUid);
            if (pro != null) {
                for (ProvisionItem tmp : pro.getProvisionalItemList()) {
                    if (tmp.getServiceUid() == null || isServiceIdEqual(serviceUid, tmp)) {
                        items.add(tmp);
                    }
                }
            }
        }

        if (serviceUid != null) {
            Provisional pro = provisionalRepo.findByServiceUid(serviceUid);
            if (pro != null) {
                items.addAll(pro.getProvisionalItemList());
            }
        }

        for (ProvisionItem pi : items) {

            JobLedgerResChargeDto dto = null;

            if (mapCharge.get(pi.getChargeMaster().getChargeCode()) == null) {
                dto = new JobLedgerResChargeDto();
                dto.setChargeCode(pi.getChargeMaster().getChargeCode());
                dto.setChargeName(pi.getChargeMaster().getChargeName());

                mapCharge.put(pi.getChargeMaster().getChargeCode(), dto);
            }

            dto = mapCharge.get(pi.getChargeMaster().getChargeCode());

            if (pi.getIsNeutral() != null && pi.getIsNeutral() == YesNo.Yes && pi.getSellLocalAmount() != null) {
                dto.setProvisionalNeutral(dto.getProvisionalNeutral() + (pi.getSellLocalAmount() * -1));
                dto.setAmountDebit(dto.getAmountDebit() + pi.getSellLocalAmount());
            } else if ((pi.getIsNeutral() == null || pi.getIsNeutral() == YesNo.No)
                    && pi.getSellLocalAmount() != null) {
                dto.setProvisionalCost(dto.getProvisionalCost() + pi.getSellLocalAmount());
                dto.setAmountDebit(dto.getAmountDebit() + pi.getSellLocalAmount());
            }
        }


        if (serviceUid != null) {
            ShipmentServiceDetail service = serviceRepo.findByServiceUid(serviceUid);
            if (service.getShipmentChargeList() != null && service.getShipmentChargeList().size() != 0) {

                if (resp.getChargeCurrencyCode() == null) {
                    resp.setChargeCurrencyCode(service.getLocalCurrency().getCurrencyCode());
                    resp.setChargeLocale(service.getLocalCurrency().getCountryMaster().getLocale());
                    resp.setChargeDecimalPoint(service.getLocalCurrency().getDecimalPoint());
                }

                for (ShipmentCharge rate : service.getShipmentChargeList()) {
                    JobLedgerResChargeDto dto = null;

                    if (mapCharge.get(rate.getChargeMaster().getChargeCode()) == null) {
                        dto = new JobLedgerResChargeDto();
                        dto.setChargeCode(rate.getChargeMaster().getChargeCode());
                        dto.setChargeName(rate.getChargeName());

                        mapCharge.put(rate.getChargeMaster().getChargeCode(), dto);
                    }

                    dto = mapCharge.get(rate.getChargeMaster().getChargeCode());

                    String rateMerged = appUtil.getLocationConfig("booking.rates.merged", service.getLocation(), true);

                    if (rateMerged != null && rateMerged.trim().length() != 0 && rateMerged.equals("TRUE")) {
                        dto.setProvisionalRevenue(dto.getProvisionalRevenue() + rate.getLocalTotalRateAmount());
                        dto.setAmountCredit(dto.getAmountCredit() + rate.getLocalTotalRateAmount());
                    } else {
                        dto.setProvisionalRevenue(dto.getProvisionalRevenue() + rate.getLocalTotalNetSale());
                        dto.setAmountCredit(dto.getAmountCredit() + rate.getLocalTotalNetSale());
                    }

                    if (rate.getLocalTotalCostAmount() > 0) {
                        dto.setProvisionalCost(dto.getProvisionalCost() + rate.getLocalTotalCostAmount());
                        dto.setAmountDebit(dto.getAmountDebit() + rate.getLocalTotalCostAmount());
                    }
                }
            }
        }
    }

    private boolean isServiceIdEqual(String serviceUid, ProvisionItem tmp) {
        return serviceUid != null && serviceUid.trim().length() != 0
                && tmp.getServiceUid() != null
                && tmp.getServiceUid().trim().equals(serviceUid.trim());
    }

    private void assignToMapChargeInInvoice(Map<String, JobLedgerResChargeDto> mapCharge, JobLedgerResDto resp, Consol consol, List<InvoiceCreditNote> documents) {

        if (documents != null && documents.size() != 0) {

            for (InvoiceCreditNote inv : documents) {

                if (resp.getChargeCurrencyCode() == null) {
                    resp.setChargeCurrencyCode(inv.getLocalCurrency().getCurrencyCode());
                    resp.setChargeLocale(inv.getLocalCurrency().getCountryMaster().getLocale());
                    resp.setChargeDecimalPoint(inv.getLocalCurrency().getDecimalPoint());
                }

                for (InvoiceCreditNoteDetail dd : inv.getInvoiceCreditNoteDetailList()) {

                    JobLedgerResChargeDto dto = null;

                    if (mapCharge.get(dd.getChargeMaster().getChargeCode()) == null) {
                        dto = new JobLedgerResChargeDto();
                        dto.setChargeCode(dd.getChargeMaster().getChargeCode());
                        dto.setChargeName(dd.getChargeName());
                        dto.setChargeType(dd.getChargeMaster().getChargeType().name());


                        mapCharge.put(dd.getChargeMaster().getChargeCode(), dto);
                    }

                    dto = mapCharge.get(dd.getChargeMaster().getChargeCode());

                    if (dd.getCrn() == CRN.Revenue) {
                        if (inv.getDocumentTypeMaster().getDocumentTypeCode() == DocumentType.CRN
                                && inv.getAdjustmentInvoiceNo() != null) {
                            dto.setAmountDebit(dto.getAmountDebit() + dd.getLocalAmount());
                            dto.setActualRevenue(dto.getActualRevenue() + (dd.getLocalAmount() * -1));
                        } else {
                            dto.setAmountCredit(dto.getAmountCredit() + dd.getLocalAmount());
                            dto.setActualRevenue(dto.getActualRevenue() + dd.getLocalAmount());
                        }

                    } else if (dd.getCrn() == CRN.Cost) {
                        dto.setAmountDebit(dto.getAmountDebit() + dd.getLocalAmount());
                        dto.setActualCost(dto.getActualCost() + dd.getLocalAmount());
                    } else if (dd.getCrn() == CRN.Neutral) {
                        dto.setActualNeutral(dto.getActualNeutral() + (dd.getLocalAmount() * -1));
                        dto.setAmountDebit(dto.getAmountDebit() + dd.getLocalAmount());
                    }

                }

            }

        }

    }

    private void assignToDocumentDto(JobLedgerResDto resp, Consol consol, List<InvoiceCreditNote> documents) {

        if (documents != null && documents.size() != 0) {

            double tDebit = 0.0, tCredit = 0.0, tRevenue = 0.0, tCost = 0.0, tNeutral = 0.0;

            for (InvoiceCreditNote inv : documents) {

                if (resp.getDocCurrencyCode() == null) {
                    resp.setDocCurrencyCode(inv.getLocalCurrency().getCurrencyCode());
                    resp.setDocLocale(inv.getLocalCurrency().getCountryMaster().getLocale());
                    resp.setDocDecimalPoint(inv.getLocalCurrency().getDecimalPoint());
                }

                JobLedgerResDocumentDto dto = new JobLedgerResDocumentDto();
                dto.setInvoiceCreditNoteId(inv.getId());
                dto.setMasterUid(inv.getMasterUid());
                dto.setShipmentUid(inv.getShipmentUid());
                dto.setAdjustmentInvoiceNo(inv.getAdjustmentInvoiceNo());
                dto.setDocumentTypeCode(inv.getDocumentTypeCode());
                dto.setServiceUid(inv.getShipmentServiceDetail() != null ? inv.getShipmentServiceDetail().getServiceUid() : null);

                if (inv.getShipmentServiceDetail() != null) {
                    dto.setServiceCode(inv.getShipmentServiceDetail().getServiceMaster().getServiceCode());
                    dto.setShipmentUid(inv.getShipmentServiceDetail().getShipmentUid());
                } else if (consol != null) {
                    dto.setServiceCode(consol.getServiceMaster().getServiceCode());
                }


                dto.setDayBookCode(inv.getDaybookMaster().getDaybookCode());
                dto.setDayBookName(inv.getDaybookMaster().getDaybookName());
                dto.setDocumentDate(inv.getInvoiceCreditNoteDate());
                dto.setDocumentNumber(inv.getInvoiceCreditNoteNo());
                dto.setDocumentType(inv.getDocumentTypeMaster().getDocumentTypeName());

                double debitAmount = 0.0, creditAmount = 0.0, actualRevenue = 0.0, actualCost = 0.0, actualNeutral = 0.0;

                for (InvoiceCreditNoteDetail dd : inv.getInvoiceCreditNoteDetailList()) {

                    if (dd.getCrn() == CRN.Revenue) {
                        if (inv.getDocumentTypeMaster().getDocumentTypeCode() == DocumentType.CRN
                                && inv.getAdjustmentInvoiceNo() != null) {
                            debitAmount = debitAmount + dd.getLocalAmount();
                            actualRevenue = actualRevenue + (dd.getLocalAmount() * -1);
                        } else {
                            creditAmount = creditAmount + dd.getLocalAmount();
                            actualRevenue = actualRevenue + dd.getLocalAmount();
                        }

                    } else if (dd.getCrn() == CRN.Cost) {
                        debitAmount = debitAmount + dd.getLocalAmount();
                        actualCost = actualCost + dd.getLocalAmount();
                    } else if (dd.getCrn() == CRN.Neutral) {
                        actualNeutral = actualNeutral + (dd.getLocalAmount() * -1);
                        debitAmount = debitAmount + dd.getLocalAmount();
                    }

                }

                dto.setAmountDebit(debitAmount);
                dto.setAmountCredit(creditAmount);
                dto.setActualRevenue(actualRevenue);
                dto.setActualCost(actualCost);
                dto.setActualNeutral(actualNeutral);


                tDebit = tDebit + dto.getAmountDebit();
                tCredit = tCredit + dto.getAmountCredit();
                tRevenue = tRevenue + dto.getActualRevenue();
                tCost = tCost + dto.getActualCost();
                tNeutral = tNeutral + dto.getActualNeutral();

                resp.getDocumentWiseList().add(dto);
            }

            resp.setDocTotalAmountDebit(tDebit);
            resp.setDocTotalAmountCredit(tCredit);
            resp.setDocTotalAcutalRevenue(tRevenue);
            resp.setDocTotalAcutalCost(tCost);
            resp.setDocTotalAcutalNeutral(tNeutral);

            resp.setDocNetProfitAmount(tCredit - tDebit);
            resp.setDocNetProfitActual(tRevenue - tCost);


        }

    }

    private void getInvoiceCreditNote(List<InvoiceCreditNote> documents, String consolUid, String serviceUid) {

        List<InvoiceCreditNote> tmpList = null;

        if (consolUid != null && serviceUid == null) {
            tmpList = invoiceRepo.findByConsolUid(consolUid);
        } else if (serviceUid != null) {
            tmpList = invoiceRepo.findByServiceUid(serviceUid);
        }

        if (tmpList != null && tmpList.size() != 0) {
            documents.addAll(tmpList);
        }

    }

}
