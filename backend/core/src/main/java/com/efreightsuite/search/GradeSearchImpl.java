package com.efreightsuite.search;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import com.efreightsuite.dto.GradeMasterSearchDto;
import com.efreightsuite.dto.SearchRequest;
import com.efreightsuite.dto.SearchRespDto;
import com.efreightsuite.enumeration.Priority;
import com.efreightsuite.model.GradeMaster;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class GradeSearchImpl {

    @Autowired
    private
    EntityManager em;

    /**
     * lov search
     */
    public SearchRespDto search(SearchRequest dto) {

        String hql = " from GradeMaster  ";

        if (dto.getKeyword() != null && dto.getKeyword().trim().length() != 0) {

            hql = hql + " where Upper(gradeName) like '" + dto.getKeyword().trim().toUpperCase() + "%' ";

        }

        hql = hql + " order by gradeName ASC";

        log.info("Grade Master Search hql:[" + hql + "]");

        return search(dto.getSelectedPageNumber(), dto.getRecordPerPage(), hql);
    }

    /**
     * list view page search
     */
    public SearchRespDto search(GradeMasterSearchDto dto) {

        String hql = "";


        if (dto.getSearchGradeName() != null && dto.getSearchGradeName().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }

            hql = hql + " Upper(gradeName) like '" + dto.getSearchGradeName().trim().toUpperCase() + "%' ";
        }


        if (dto.getSearchPriority() != null && dto.getSearchPriority().toString().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }

            String priorityValue = "0";
            if (dto.getSearchPriority().equals(Priority.ONE)) {
                priorityValue = "1";
            } else if (dto.getSearchPriority().equals(Priority.TWO)) {
                priorityValue = "2";
            } else if (dto.getSearchPriority().equals(Priority.THREE)) {
                priorityValue = "3";
            } else if (dto.getSearchPriority().equals(Priority.FOUR)) {
                priorityValue = "4";
            } else if (dto.getSearchPriority().equals(Priority.FIVE)) {
                priorityValue = "5";
            }

            hql = hql + " Upper(priority) like '" + priorityValue.toUpperCase() + "%' ";
        }

        if (dto.getSearchColorCode() != null && dto.getSearchColorCode().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }

            hql = hql + " Upper(colorCode) like '" + dto.getSearchColorCode().toUpperCase() + "%' ";
        }


        if (hql.trim().length() != 0) {
            hql = " where " + hql;
        }

        hql = " from GradeMaster " + hql;


        if (dto.getSortByColumn() != null && dto.getOrderByType() != null && dto.getSortByColumn().trim().length() != 0
                && dto.getOrderByType().trim().length() != 0) {
            hql = hql + " order by Upper(" + dto.getSortByColumn().trim() + ") " + dto.getOrderByType().trim();
        } else {
            hql = hql + " order by Upper(gradeName) ASC";
        }

        log.info("Grade Master Search hql:[" + hql + "]");

        return search(dto.getSelectedPageNumber(), dto.getRecordPerPage(), hql);

    }


    private SearchRespDto search(int selectedPageNumber, int recordPerPage, String hql) {
        String countQ = "Select count (id) " + hql;

        Query countQuery = em.createQuery(countQ);

        Long countTotal = (Long) countQuery.getSingleResult();

        TypedQuery<GradeMaster> query = em.createQuery(hql, GradeMaster.class);

        query.setFirstResult(selectedPageNumber * recordPerPage);

        query.setMaxResults(recordPerPage);

        List<GradeMaster> resultlist = query.getResultList();


        if (resultlist != null && !resultlist.isEmpty()) {

            for (GradeMaster gm : resultlist) {

                if (gm.getPriorityValue() == 1) {
                    gm.setPriority(Priority.ONE);
                } else if (gm.getPriorityValue() == 2) {
                    gm.setPriority(Priority.TWO);
                } else if (gm.getPriorityValue() == 3) {
                    gm.setPriority(Priority.THREE);
                } else if (gm.getPriorityValue() == 4) {
                    gm.setPriority(Priority.FOUR);
                } else if (gm.getPriorityValue() == 5) {
                    gm.setPriority(Priority.FIVE);
                }

            }
            log.info("Number of rows selected : " + resultlist.size());
        }

        return new SearchRespDto(countTotal, resultlist);

    }
}
