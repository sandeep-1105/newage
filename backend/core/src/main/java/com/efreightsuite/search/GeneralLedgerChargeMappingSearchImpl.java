package com.efreightsuite.search;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import com.efreightsuite.dto.SearchRequest;
import com.efreightsuite.dto.SearchRespDto;
import com.efreightsuite.model.GeneralLedgerChargeMappingMaster;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class GeneralLedgerChargeMappingSearchImpl {

    @Autowired
    private
    EntityManager em;

    public SearchRespDto search(SearchRequest dto, Long serviceId) {

        String hql = " from GeneralLedgerChargeMappingMaster where status!='Hide' ";

        if (serviceId == 0) {
            hql = hql + " AND serviceMaster.id IS NULL";
        } else {
            hql = hql + " AND serviceMaster.id=" + serviceId;
        }

        if (dto.getKeyword() != null && dto.getKeyword().trim().length() != 0) {

            hql = hql + " AND UPPER(chargeCode) like '" + dto.getKeyword().trim().toUpperCase() + "%' ";

        }

        hql = hql + " order by chargeCode ASC";

        log.info("GeneralLedgerChargeMapping Master Search hql:[" + hql + "]");

        return search(dto.getSelectedPageNumber(), dto.getRecordPerPage(), hql);

    }

    private SearchRespDto search(int selectedPageNumber, int recordPerPage, String hql) {

        String countQ = "Select count (id) " + hql;

        Query countQuery = em.createQuery(countQ);

        Long countTotal = (Long) countQuery.getSingleResult();

        TypedQuery<GeneralLedgerChargeMappingMaster> query = em.createQuery(hql, GeneralLedgerChargeMappingMaster.class);

        query.setFirstResult(selectedPageNumber * recordPerPage);

        query.setMaxResults(recordPerPage);

        List<GeneralLedgerChargeMappingMaster> resultlist = query.getResultList();

        return new SearchRespDto(countTotal, resultlist);

    }
}
