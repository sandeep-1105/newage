package com.efreightsuite.search;

import java.util.List;
import java.util.stream.Collectors;

import com.efreightsuite.dto.AutoMailGroupMasterSearchDto;
import com.efreightsuite.dto.SearchRequest;
import com.efreightsuite.dto.SearchRespDto;
import com.efreightsuite.model.AutoMailGroupMaster;
import com.efreightsuite.repository.AutoMailGroupMasterRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class AutoMailGroupMasterSearchService {

    @Autowired
    private AutoMailGroupMasterRepository repository;

    public SearchRespDto search(AutoMailGroupMasterSearchDto dto) {
        AutoMailGroupMaster probe = new AutoMailGroupMaster();
        probe.setMessageGroupCode(dto.getSearchMessageGroupCode());
        probe.setMessageGroupName(dto.getSearchMessageGroupName());

        ExampleMatcher matcher = ExampleMatcher.matching()
                .withIgnoreCase()
                .withIgnorePaths("versionLock")
                .withIgnoreNullValues()
                .withMatcher("messageGroupCode", ExampleMatcher.GenericPropertyMatcher::startsWith)
                .withMatcher("messageGroupName", ExampleMatcher.GenericPropertyMatcher::startsWith);

        Example<AutoMailGroupMaster> example = Example.of(probe, matcher);

        Sort sortCriteria = dto.getSortByColumn() != null && dto.getOrderByType() != null
                ? new Sort(Sort.Direction.valueOf(dto.getOrderByType()), dto.getSortByColumn())
                : new Sort(Sort.Direction.ASC, "messageGroupCode");

        Page<AutoMailGroupMaster> pageResult = repository.findAll(
                example,
                new PageRequest(dto.getSelectedPageNumber(), dto.getRecordPerPage(),
                        sortCriteria)
        );
        List<AutoMailGroupMaster> result = pageResult.getContent()
                .stream()
                .map(autoMailGroupMaster -> {
                    autoMailGroupMaster.getAutoMailMasterList().forEach(agm -> agm.setAutoMailGroupMaster(null));
                    return autoMailGroupMaster;
                }).collect(Collectors.toList());
        return new SearchRespDto(pageResult.getTotalElements(), result);
    }

    public SearchRespDto search(SearchRequest dto) {
        AutoMailGroupMaster probe = new AutoMailGroupMaster();
        probe.setMessageGroupCode(dto.getKeywordNullable());
        probe.setMessageGroupName(dto.getKeywordNullable());

        ExampleMatcher matcher = ExampleMatcher.matchingAny()
                .withIgnoreCase()
                .withIgnoreNullValues()
                .withIgnorePaths("versionLock")
                .withMatcher("messageGroupCode", ExampleMatcher.GenericPropertyMatcher::startsWith)
                .withMatcher("messageGroupName", ExampleMatcher.GenericPropertyMatcher::startsWith);

        Example<AutoMailGroupMaster> example = Example.of(probe, matcher);

        Sort sortCriteria = new Sort(Sort.Direction.ASC, "messageGroupCode");

        Page<AutoMailGroupMaster> pageResult = repository.findAll(
                example,
                new PageRequest(dto.getSelectedPageNumber(), dto.getRecordPerPage(),
                        sortCriteria)
        );
        List<AutoMailGroupMaster> result = pageResult.getContent()
                .stream()
                .map(autoMailGroupMaster -> {
                    autoMailGroupMaster.getAutoMailMasterList().forEach(agm -> agm.setAutoMailGroupMaster(null));
                    return autoMailGroupMaster;
                }).collect(Collectors.toList());

        return new SearchRespDto(pageResult.getTotalElements(), result);
    }

}
