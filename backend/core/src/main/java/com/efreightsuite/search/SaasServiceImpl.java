package com.efreightsuite.search;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import com.efreightsuite.common.component.CacheRepository;
import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.LocationSetupChargeDTO;
import com.efreightsuite.dto.LocationSetupDTO;
import com.efreightsuite.dto.LocationSetupDaybookDTO;
import com.efreightsuite.dto.LocationSetupSequenceDTO;
import com.efreightsuite.dto.LocationSetupServiceDTO;
import com.efreightsuite.enumeration.BooleanType;
import com.efreightsuite.enumeration.EmploymentStatus;
import com.efreightsuite.enumeration.ImportExport;
import com.efreightsuite.enumeration.LovStatus;
import com.efreightsuite.enumeration.SequenceType;
import com.efreightsuite.enumeration.UserStatus;
import com.efreightsuite.enumeration.WhichTransactionDate;
import com.efreightsuite.enumeration.YesNo;
import com.efreightsuite.model.*;
import com.efreightsuite.repository.*;
import com.efreightsuite.service.InitSetupService;
import com.efreightsuite.service.SequenceGeneratorService;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
@Log4j2
public class SaasServiceImpl {

    @Autowired
    private
    SequenceGeneratorService sequenceGeneratorService;

    @Autowired
    private
    CompanyMasterRepository companyMasterRepository;

    @Autowired
    private
    CountryMasterRepository countryMasterRepository;

    @Autowired
    private
    StateMasterRepository stateMasterRepository;

    @Autowired
    private
    CityMasterRepository cityMasterRepository;

    @Autowired
    private
    LocationMasterRepository locationMasterRepository;

    @Autowired
    private
    LogoMasterRepository logoMasterRepository;

    @Autowired
    private
    RegionMasterRepository regionMasterRepository;

    @Autowired
    private
    EmployeeMasterRepository employeeMasterRepository;

    @Autowired
    private
    RolesRepository rolesRepository;

    @Autowired
    private
    FeatureRepository featureRepository;

    @Autowired
    private
    InitSetupService initSetupService;

    @Autowired
    private
    DesignationMasterRepository designationRepo;

    @Autowired
    private
    CacheRepository cacheRepository;

    @Autowired
    private
    LanguageRepository languageRepo;

    @Autowired
    private
    LanguageDetailRepository languageDetailRepository;

    @Autowired
    private
    UserProfileRepository userProfileRepo;

    @Autowired
    private
    WorkFlowGroupRepository workFlowGroupRepo;

    @Autowired
    private
    CurrencyMasterRepository currencyMasterRepository;

    @Autowired
    private
    DefaultMasterDataRepository defaultMasterDataRepository;


    @Autowired
    private
    DateConfigurationRepository dateConfigurationRepository;

    @Autowired
    private
    DaybookMasterRepository daybookMasterRepository;

    @Autowired
    private
    DocumentTypeMasterRepository documentTypeMasterRepository;

    @Autowired
    private
    SequenceGeneratorRepository sequenceGeneratorRepository;

    @Autowired
    private
    ChargeMasterRepository chargeMasterRepository;

    @Autowired
    private
    ServiceMasterRepository serviceMasterRepository;

    @Transactional
    public void createCompanyAndLocationProcess(LocationSetupDTO locationSetup) {

        log.info("Company & Location Created with basic data which input from registration page");

        CompanyMaster companyMaster = companyCreation(locationSetup);

        log.info("Basic data loaded for GL Data : " + new Date());
        initSetupService.loadGLData(new BaseDto(), companyMaster);

        log.info("Basic data loaded for Sequence Data : " + new Date());
        initSetupService.loadSequence(new BaseDto(), companyMaster);

        LocationMaster locationMaster = locationCreation(companyMaster, locationSetup);

        log.info("Basic data loaded for Default data : " + new Date());
        initSetupService.loadDefaultMaster(new BaseDto(), locationMaster);

        log.info("Basic data loaded for Report Configuration Data : " + new Date());
        initSetupService.loadReportConfig(new BaseDto(), locationMaster);

        log.info("Basic data loaded for Edi Configuration : " + new Date());
        initSetupService.loadEdiConfiguration(new BaseDto(), locationMaster);

        log.info("Basic data loaded for Department Data : " + new Date());
        initSetupService.loadDepartment(new BaseDto(), locationMaster);

        log.info("Basic data loaded for Designation : " + new Date());
        initSetupService.loadDesignation(new BaseDto(), locationMaster);

        log.info("Mapping GLChargeMappingMaster " + new Date());
        initSetupService.mapGlCharge(new BaseDto(), locationMaster);

        logoCreation(locationMaster, locationSetup.getLogo());

        EmployeeMaster employee = employeeCreation(locationSetup, locationMaster);

        Role role = roleCreation();

        List<WorkFlowGroup> workFlowGroupList = workGroupFlowCreation();

        userProfileCreation(locationSetup, locationMaster, role, employee, workFlowGroupList);

        loadCache(locationSetup.getSaasId());

        if (locationSetup.getCustomerUploadedFile() != null) {
            log.info("Upload Customer ");
            initSetupService.customerOrAgentBulkUpload(new BaseDto(), locationSetup.getCustomerUploadedFile(), locationMaster, employee, "customer", locationSetup.getCustomerFileName());
        }

        if (locationSetup.getAgentUploadedFile() != null) {
            log.info("Upload Agent ");
            initSetupService.customerOrAgentBulkUpload(new BaseDto(), locationSetup.getAgentUploadedFile(), locationMaster, employee, "agent", locationSetup.getAgentFileName());
        }

		/*if(locationSetup.getAgentPortUploadedFile()!=null){
			log.info("Upload Agent Port Master ");
			initSetupService.agentPortMasterBulkUpload(new BaseDto(), locationSetup.getAgentPortUploadedFile());
		}*/

        log.info("Location Setup Finance Setup is started " + new Date());
        financeSetup(locationSetup, locationMaster); // get code and location, Only update required.

        log.info("Location Setup Charge Setup is started " + new Date());
        chargeSetup(locationSetup); // Find charge code and update/insert

        log.info("Location Setup Job Date Setup is started " + new Date());
        jobDateSetup(locationSetup); // Find and update/insert

        jobNumberSetup(locationSetup); // Find and update/insert

        log.info("Saving list of Day Book Master is started " + new Date());
        accountingDocument(locationSetup, locationMaster); // Find code and update/insert

        log.info("Saving list of Service is started " + new Date());
        serviceMasterActivation(locationSetup);


    }


    private void serviceMasterActivation(LocationSetupDTO locationSetup) {

        if (locationSetup.getServiceList() != null && locationSetup.getServiceList().size() != 0) {
            for (LocationSetupServiceDTO dt : locationSetup.getServiceList()) {
                ServiceMaster tmpObj = serviceMasterRepository.findByServiceCode(dt.getServiceCode());
                if (tmpObj != null && tmpObj.getId() != null) {
                    tmpObj.setStatus(LovStatus.Active);
                    serviceMasterRepository.save(tmpObj);
                }
            }
        }
    }

    private CompanyMaster companyCreation(LocationSetupDTO locationSetup) {

        log.info("Company Creation Started (" + locationSetup.getCompanyRegisteredName() + ")");

        CompanyMaster companyMaster = new CompanyMaster();
        companyMaster.setCompanyCode(locationSetup.getCompanyRegisteredName().substring(0, 3).toUpperCase());
        companyMaster.setCompanyName(locationSetup.getCompanyRegisteredName());
        companyMaster.setStatus(LovStatus.Active);

        companyMaster = companyMasterRepository.save(companyMaster);

        log.info("Company Creation Finished (" + locationSetup.getCompanyRegisteredName() + ")");

        return companyMaster;
    }

    private LocationMaster locationCreation(CompanyMaster companyMaster, LocationSetupDTO locationSetup) {
        log.info("Location Creation started");
        LocationMaster locationMaster = new LocationMaster();

        locationMaster.setCompanyMaster(companyMaster);

        locationMaster.setStatus(LovStatus.Active);
        locationMaster.setIsApproved(BooleanType.TRUE);
        locationMaster.setLocationCode(locationSetup.getCityMaster().getCityCode().toUpperCase());
        locationMaster.setLocationName(locationSetup.getCityMaster().getCityName());
        locationMaster.setIsHeadOffice(locationSetup.getIsHeadOffice());
        locationMaster.setSalutation(locationSetup.getSalutation());
        locationMaster.setFirstName(locationSetup.getFirstName());
        locationMaster.setLastName(locationSetup.getLastName());
        locationMaster.setEmail(locationSetup.getEmail());
        locationMaster.setPhone(locationSetup.getPhone());
        locationMaster.setZipCode(locationSetup.getZipCode());
        locationMaster.setAddressLine1(locationSetup.getAddressLine1());
        locationMaster.setAddressLine2(locationSetup.getAddressLine2());
        locationMaster.setAddressLine3(locationSetup.getAddressLine3());


        if (locationSetup.getCountryMaster() != null) {
            CountryMaster countryMaster = countryMasterRepository.findByCountryCode(locationSetup.getCountryMaster().getCountryCode());
            locationMaster.setCountryMaster(countryMaster);
        }


        if (locationSetup.getStateMaster() != null) {
            StateMaster stateMaster = stateMasterRepository.getState(locationSetup.getStateMaster().getStateCode(), locationSetup.getCountryMaster().getCountryCode());
            locationMaster.setStateMaster(stateMaster);
        }


        if (locationSetup.getCityMaster() != null) {
            CityMaster cityMaster = cityMasterRepository.findByCityCode(locationSetup.getCityMaster().getCityCode());
            locationMaster.setCityMaster(cityMaster);
        }

        if (locationSetup.getRegionMaster() != null) {
            RegionMaster regionMaster = regionMasterRepository.findByRegionCode(locationSetup.getRegionMaster().getRegionCode());
            locationMaster.setRegionMaster(regionMaster);
        }


        locationMaster.setTimeZone(locationSetup.getTimeZone());
        locationMaster.setDateFormat(locationSetup.getDateFormat());
        locationMaster.setDateTimeFormat(locationSetup.getDateFormat() + " HH:mm");


        if (locationSetup.getCurrencyMaster() != null) {
            CurrencyMaster currencyMaster = currencyMasterRepository.findByCurrencyCode(locationSetup.getCurrencyMaster().getCurrencyCode());

            if (currencyMaster != null) {
                locationMaster.setCurrencyMaster(currencyMaster);
            }

        }

        locationMaster.setMeasurement(locationSetup.getMeasurement());

        locationMaster = locationMasterRepository.save(locationMaster);
        log.info("Location Master is created [" + locationMaster.getId() + "]");

        return locationMaster;
    }

    private void logoCreation(LocationMaster locationMaster, byte[] logo) {

        if (logo == null) {
            return;
        }

        LogoMaster logoMaster = new LogoMaster();
        logoMaster.setStatus(LovStatus.Active);
        logoMaster.setLogo(logo);

        logoMaster.setLocationMaster(locationMaster);

        logoMaster.setCountryMaster(locationMaster.getCountryMaster());
        logoMaster.setCountryCode(locationMaster.getCountryMaster().getCountryCode());

        logoMaster.setCompanyMaster(locationMaster.getCompanyMaster());
        logoMaster.setCompanyCode(locationMaster.getCompanyMaster().getCompanyCode());

        logoMaster = logoMasterRepository.save(logoMaster);

        log.info("Logo Master is created [" + logoMaster.getId() + "]");

    }

    private EmployeeMaster employeeCreation(LocationSetupDTO locationSetup, LocationMaster locationMaster) {

        log.info("Employee Creation");

        EmployeeMaster emp = new EmployeeMaster();

        emp.setLocationMaster(locationMaster);
        emp.setCompanyMaster(locationMaster.getCompanyMaster());
        emp.setCountryMaster(locationMaster.getCountryMaster());
        emp.setEmployeeCode(sequenceGeneratorService.getSequence(SequenceType.EMPLOYEE, locationMaster));
        emp.setFirstName(locationSetup.getFirstName());
        emp.setLastName(locationSetup.getLastName());
        emp.setEmployeeName(locationSetup.getFirstName(), locationSetup.getLastName());
        emp.setEmployementStatus(EmploymentStatus.EMPLOYED);
        emp.setIsSalesman(YesNo.Yes);
        emp.setEmployeePhoneNo(locationSetup.getPhone());
        emp.setEmail(locationSetup.getEmail());

        DesignationMaster designationMaster = designationRepo.findByDesignationName(locationSetup.getPosition().toUpperCase());

        if (designationMaster == null) {
            designationMaster = new DesignationMaster();
            designationMaster.setDesignationCode(sequenceGeneratorService.getSequence(SequenceType.DESIGNATION, locationMaster));
            designationMaster.setDesignationName(locationSetup.getPosition());
            designationMaster.setStatus(LovStatus.Active);
            designationMaster = designationRepo.save(designationMaster);
        }

        EmployeeProfessionalDetail empProf = new EmployeeProfessionalDetail();
        empProf.setDesignationMaster(designationMaster);

        emp.setEmployeeProfessionalDetail(empProf);

        emp = employeeMasterRepository.save(emp);

        return emp;
    }

    private Role roleCreation() {

        Role role = rolesRepository.findByRoleName("Administrator");

        if (role != null) {
            return role;
        }

        List<Feature> featureList = featureRepository.findAll();

        role = new Role();
        role.setRoleName("Administrator");
        role.setStatus(LovStatus.Active);

        for (Feature feature : featureList) {

            RoleHasFeature rhf = new RoleHasFeature();
            rhf.setFeature(feature);
            rhf.setRole(role);

            if (feature.getCreate() == YesNo.Yes) {
                rhf.setValCreate(feature.getFeatureValue() + ".CREATE");
            }

            if (feature.getModify() == YesNo.Yes) {
                rhf.setValModify(feature.getFeatureValue() + ".MODIFY");
            }

            if (feature.getView() == YesNo.Yes) {
                rhf.setValView(feature.getFeatureValue() + ".VIEW");
            }

            if (feature.getDelete() == YesNo.Yes) {
                rhf.setValDelete(feature.getFeatureValue() + ".DELETE");
            }

            if (feature.getList() == YesNo.Yes) {
                rhf.setValList(feature.getFeatureValue() + ".LIST");
            }

            if (feature.getDownload() == YesNo.Yes) {
                rhf.setValDownload(feature.getFeatureValue() + ".DOWNLOAD");
            }

            role.getFeatureList().add(rhf);
        }

        role = rolesRepository.save(role);

        return role;
    }

    // For the time being , we are hardcoding the tasks but in future Will do all these configurable.
    private List<WorkFlowGroup> workGroupFlowCreation() {
        List<WorkFlowGroup> wfgList = new ArrayList<>();
        wfgList.add(new WorkFlowGroup("Enquiry"));
        wfgList.add(new WorkFlowGroup("Quotation"));
        wfgList.add(new WorkFlowGroup("Shipment"));
        wfgList.add(new WorkFlowGroup("Pickup"));
        wfgList.add(new WorkFlowGroup("DeliveryToCfs"));
        wfgList.add(new WorkFlowGroup("LinkToMaster"));
        wfgList.add(new WorkFlowGroup("AesOrCustoms"));
        wfgList.add(new WorkFlowGroup("SignOff"));
        wfgList.add(new WorkFlowGroup("AirLineSubmission"));
        wfgList.add(new WorkFlowGroup("JobCompletion"));
        wfgList.add(new WorkFlowGroup("Invoice"));
        wfgList.add(new WorkFlowGroup("AtdConfirmation"));
        wfgList.add(new WorkFlowGroup("CanAndInvoice"));
        wfgList.add(new WorkFlowGroup("DeliveryOrder"));
        wfgList.add(new WorkFlowGroup("Clearance"));
        wfgList.add(new WorkFlowGroup("pod"));
        wfgList.add(new WorkFlowGroup("AtaConfirmation"));
        wfgList.add(new WorkFlowGroup("Customs"));


        wfgList = workFlowGroupRepo.save(wfgList);

        return wfgList;
    }

    private void userProfileCreation(LocationSetupDTO locationSetup, LocationMaster locationMaster, Role role, EmployeeMaster employee, List<WorkFlowGroup> workFlowGroupList) {

        UserProfile userProfile = new UserProfile();

        userProfile.setUserName(locationSetup.getUsername());
        userProfile.setPassword(locationSetup.getPassword());
        userProfile.setStatus(UserStatus.Active);

        List<Role> roleList = new ArrayList<>();
        roleList.add(role);

        userProfile.setRoleList(roleList);
        userProfile.setWorkFlowList(workFlowGroupList);

        List<UserCompany> userCompanyList = new ArrayList<>();

        UserCompany uc = new UserCompany();
        uc.setCompanyMaster(locationMaster.getCompanyMaster());
        uc.setEmployee(employee);
        uc.setUserProfile(userProfile);
        uc.setYesNo(YesNo.Yes);

        List<UserCompanyLocation> uclList = new ArrayList<>();

        UserCompanyLocation ucl = new UserCompanyLocation();
        ucl.setUserCompany(uc);
        ucl.setLocationMaster(locationMaster);
        ucl.setYesNo(YesNo.Yes);

        uclList.add(ucl);

        uc.setUserCompanyLocationList(uclList);

        userCompanyList.add(uc);

        userProfile.setUserCompanyList(userCompanyList);

        userProfile = userProfileRepo.save(userProfile);

    }

    private void loadCache(String saasId) {
        for (Language language : languageRepo.findAll()) {
            cacheRepository.removeNLSCache(saasId, language);
            cacheRepository.pushNLS(saasId, language, languageDetailRepository.findAllByLanguage(language));
        }
    }

    private void saveOrUpdateDefaultMasterData(String code, String value, String name, LocationMaster locationMaster) {
        DefaultMasterData defaultMasterData = defaultMasterDataRepository.getCodeAndLocation(code, locationMaster.getId());

        if (defaultMasterData != null) {
            log.info("Default Master with  the default code " + code + " is found");
            defaultMasterData.setValue(value);
            defaultMasterDataRepository.save(defaultMasterData);
        } else {
            log.info("Default Master with  the default code " + code + " is not found");
            DefaultMasterData defaultMasterDataNew = new DefaultMasterData();
            defaultMasterDataNew.setCode(code);
            defaultMasterDataNew.setDefaultName(name);
            defaultMasterDataNew.setValue(value);
            defaultMasterDataNew.setLocation(locationMaster);
            defaultMasterDataNew.setLocationCode(locationMaster.getLocationCode());
            defaultMasterDataRepository.save(defaultMasterDataNew);
        }

        log.info("Default Master with  the default code " + code + " is saved");
    }

    private void financeSetup(LocationSetupDTO locationSetup, LocationMaster locationMaster) {

        if (locationSetup.getQuoteNeedApproval() != null) {
            if (locationSetup.getQuoteNeedApproval().equals(YesNo.Yes)) {
                saveOrUpdateDefaultMasterData("quotation.auto.approval.required", "true", "Manager auto approval required for quotation", locationMaster);
            } else {
                saveOrUpdateDefaultMasterData("quotation.auto.approval.required", "false", "Manager auto approval required for quotation", locationMaster);
            }
        }

        if (locationSetup.getInvoiceHeaderNote() != null && locationSetup.getInvoiceHeaderNote() != "") {
            saveOrUpdateDefaultMasterData("invoice.header.note", locationSetup.getInvoiceHeaderNote(), "Invoice header note", locationMaster);
        }

        if (locationSetup.getInvoiceFooterNote() != null && locationSetup.getInvoiceFooterNote() != "") {
            saveOrUpdateDefaultMasterData("invoice.footer.one", locationSetup.getInvoiceFooterNote(), "Invoice footer note", locationMaster);
        }
    }

    private void chargeSetup(LocationSetupDTO locationSetup) {

        if (locationSetup.getChargeList() != null && !locationSetup.getChargeList().isEmpty()) {

            List<ChargeMaster> chargeMasterList = new ArrayList<>();

            for (LocationSetupChargeDTO locationSetupCharge : locationSetup.getChargeList()) {
                ChargeMaster chargeMaster = chargeMasterRepository.findByChargeCode(locationSetupCharge.getChargeCode());
                if (chargeMaster == null) {
                    chargeMaster = new ChargeMaster();
                }
                chargeMaster.setCalculationType(locationSetupCharge.getCalculationType());
                chargeMaster.setChargeCode(locationSetupCharge.getChargeCode());
                chargeMaster.setChargeName(locationSetupCharge.getChargeName());
                chargeMaster.setChargeType(locationSetupCharge.getChargeType());
                chargeMaster.setStatus(LovStatus.Active);
                chargeMasterList.add(chargeMaster);
            }

            log.info("Saving " + chargeMasterList.size() + " number of charges.......");
            chargeMasterRepository.save(chargeMasterList);
        }
    }

    private void jobNumberSetup(LocationSetupDTO locationSetup) {

        if (locationSetup.getSequenceList() != null && !locationSetup.getSequenceList().isEmpty()) {
            List<SequenceGenerator> sequenceGeneratorList = new ArrayList<>();
            for (LocationSetupSequenceDTO locationSetupSequenceDTO : locationSetup.getSequenceList()) {
                SequenceGenerator sequenceGenerator = sequenceGeneratorRepository.findBySequenceType(locationSetupSequenceDTO.getSequenceType());
                if (sequenceGenerator != null) {
                    sequenceGenerator.setSequenceName(sequenceGenerator.getSequenceName());
                    sequenceGenerator.setSequenceType(sequenceGenerator.getSequenceType());
                    sequenceGenerator.setCurrentSequenceValue(locationSetupSequenceDTO.getCurrentSequenceValue());
                    sequenceGenerator.setPrefix(locationSetupSequenceDTO.getPrefix());
                    sequenceGenerator.setFormat(locationSetupSequenceDTO.getFormat());
                    sequenceGenerator.setIsYear(locationSetupSequenceDTO.getIsYear());
                    sequenceGenerator.setIsMonth(locationSetupSequenceDTO.getIsMonth());
                    sequenceGenerator.setSuffix(locationSetupSequenceDTO.getSuffix());
                    sequenceGenerator.setSeparator(locationSetupSequenceDTO.getSeparator());
                    sequenceGeneratorList.add(sequenceGenerator);
                }
            }
            sequenceGeneratorRepository.save(sequenceGeneratorList);
            log.info(sequenceGeneratorList.size() + " sequences are modified....");
        }
    }


    private void jobDateSetup(LocationSetupDTO locationSetup) {

        List<DateConfiguration> dateConfigurationList = new ArrayList<>();

        for (ServiceMaster serviceMaster : serviceMasterRepository.findAll()) {
            DateConfiguration dateConfiguration = new DateConfiguration();

            dateConfiguration.setServiceMaster(serviceMaster);
            dateConfiguration.setServiceCode(serviceMaster.getServiceCode());
            dateConfiguration.setWhichTransactionDate(WhichTransactionDate.Master);

            if (serviceMaster.getImportExport().equals(ImportExport.Import)) {
                dateConfiguration.setDateLogic(locationSetup.getImportJobDate());
            } else if (serviceMaster.getImportExport().equals(ImportExport.Export)) {
                dateConfiguration.setDateLogic(locationSetup.getExportJobDate());
            }

            dateConfigurationList.add(dateConfiguration);
        }

        if (!dateConfigurationList.isEmpty()) {
            log.info("Saving List of date configurations " + dateConfigurationList.size());
            dateConfigurationRepository.save(dateConfigurationList);
        }

    }

    private void accountingDocument(LocationSetupDTO locationSetup, LocationMaster locationMaster) {
        // Saving List of DayBook Master
        List<DaybookMaster> daybookMasterList = new ArrayList<>();

        if (locationSetup.getDaybookMasterList() != null && !locationSetup.getDaybookMasterList().isEmpty()) {
            for (LocationSetupDaybookDTO locationSetupDaybookDTO : locationSetup.getDaybookMasterList()) {
                DaybookMaster daybookMaster = new DaybookMaster();
                if (locationSetupDaybookDTO.getDocumentTypeMaster() != null) {
                    DocumentTypeMaster documentTypeMaster = documentTypeMasterRepository.findByDocumentTypeCode(locationSetupDaybookDTO.getDocumentTypeMaster().getDocumentTypeCode());
                    daybookMaster.setDocumentTypeMaster(documentTypeMaster);
                }
                daybookMaster.setDaybookCode(locationSetupDaybookDTO.getDaybookCode());
                daybookMaster.setDaybookName(locationSetupDaybookDTO.getDaybookName());
                daybookMaster.setCurrentSequenceValue(locationSetupDaybookDTO.getCurrentSequenceValue());
                daybookMaster.setPrefix(locationSetupDaybookDTO.getPrefix());
                daybookMaster.setSuffix(locationSetupDaybookDTO.getSuffix());
                daybookMaster.setIsYear(locationSetupDaybookDTO.getIsYear());
                daybookMaster.setIsMonth(locationSetupDaybookDTO.getIsMonth());
                daybookMaster.setFormat(locationSetupDaybookDTO.getFormat());
                daybookMaster.setSeparator(locationSetupDaybookDTO.getSeparator());
                daybookMaster.setLocationMaster(locationMaster);
                daybookMaster.setStatus(LovStatus.Active);
                daybookMasterList.add(daybookMaster);
            }

            log.info(daybookMasterList.size() + " Daybook Master saved");
            daybookMasterRepository.save(daybookMasterList);

        }
    }


}
