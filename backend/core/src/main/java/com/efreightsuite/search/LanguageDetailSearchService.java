package com.efreightsuite.search;

import com.efreightsuite.dto.LanguageDetailSearchReqDto;
import com.efreightsuite.dto.SearchRespDto;
import com.efreightsuite.enumeration.YesNo;
import com.efreightsuite.model.LanguageDetail;
import com.efreightsuite.repository.LanguageDetailRepository;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import static com.efreightsuite.constants.MatcherConstants.*;
import static java.util.stream.Collectors.toList;

@Service
@Log4j2
public class LanguageDetailSearchService {

    @Autowired
    private LanguageDetailRepository languageDetailRepository;

    public SearchRespDto search(LanguageDetailSearchReqDto dto) {

        LanguageDetail probe = new LanguageDetail();
        probe.setLanguageCode((StringUtils.isNotBlank(dto.getSearchLanguageCode()) ? dto.getSearchLanguageCode() : null));
        probe.setLanguageDesc((StringUtils.isNotBlank(dto.getSearchLanguageDesc()) ? dto.getSearchLanguageDesc() : null));
        probe.setFlagged(StringUtils.isNotBlank(dto.getSearchQcVerify()) ? YesNo.valueOf(dto.getSearchQcVerify()) : null);
        probe.setGroupName((StringUtils.isNotBlank(dto.getSearchGroupName()) ? dto.getSearchGroupName() : null));

        ExampleMatcher matcher = ExampleMatcher.matching()
                .withIgnoreCase()
                .withIgnorePaths(VERSION_LOCK)
                .withIgnoreNullValues()
                .withMatcher(LANGUAGE_CODE, ExampleMatcher.GenericPropertyMatcher::startsWith)
                .withMatcher(LANGUAGE_DESC, ExampleMatcher.GenericPropertyMatcher::startsWith)
                .withMatcher(FLAGGED, ExampleMatcher.GenericPropertyMatcher::startsWith)
                .withMatcher(GROUP_NAME, ExampleMatcher.GenericPropertyMatcher::startsWith);

        Example<LanguageDetail> example = Example.of(probe, matcher);

        Sort sortCriteria = dto.getSortCriteria("id");

        Page<LanguageDetail> pageResult = languageDetailRepository.findAll(
                example,
                new PageRequest(dto.getSelectedPageNumber(), dto.getRecordPerPage(),
                        sortCriteria)
        );
        List<LanguageDetail> result = getLangDetailByLanguage(pageResult, dto);
        List<LanguageDetail> finalResult = getLanguageDetailFilterByDateRange(result, dto);
        return new SearchRespDto(pageResult.getTotalElements(), finalResult);
    }

    private List<LanguageDetail> getLangDetailByLanguage(Page<LanguageDetail> pageResult, LanguageDetailSearchReqDto dto) {
        List<LanguageDetail> result = null;
        if (StringUtils.isNotBlank(dto.getSearchLanguageName())) {
            result = pageResult.getContent()
                    .stream()
                    .filter(languageDetail -> languageDetail.getLanguage().getLanguageName().toUpperCase().startsWith(dto.getSearchLanguageName().toUpperCase()))
                    .collect(toList());
        } else {
            result = pageResult.getContent();
        }
        return result;
    }

    private List<LanguageDetail> getLanguageDetailFilterByDateRange(List<LanguageDetail> result, LanguageDetailSearchReqDto dto) {
        List<LanguageDetail> languageDetails = result;
        if (null != dto.getLastUpdatedOn() && StringUtils.isNotBlank(dto.getLastUpdatedOn().getEndDate())) {
            Date searchDate = getFormattedDate(dto);
            Date searchDateWithOutTime = DateUtils.truncate(searchDate, DateUtils.RANGE_MONTH_SUNDAY);
            languageDetails = result.stream()
                    .filter(languageDetail -> {
                        Date date = languageDetail.getSystemTrack().getLastUpdatedDate();
                        Date dateWithOutTime = DateUtils.truncate(date, DateUtils.RANGE_MONTH_SUNDAY);
                        return searchDateWithOutTime.compareTo(dateWithOutTime) == 0;
                    })
                    .collect(toList());
        }
        return languageDetails;
    }

    private Date getFormattedDate(LanguageDetailSearchReqDto dto) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        String dateInString = dto.getLastUpdatedOn().getEndDate();
        Date date = null;
        try {
            date = formatter.parse(dateInString);
        } catch (ParseException e) {
            log.error(e);
        }
        return date;
    }
}
