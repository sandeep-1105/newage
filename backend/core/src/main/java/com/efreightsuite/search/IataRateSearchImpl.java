package com.efreightsuite.search;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import com.efreightsuite.dto.IataRateMasterSearchDto;
import com.efreightsuite.dto.SearchRespDto;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.model.IataRateMaster;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class IataRateSearchImpl {

    @Autowired
    private
    EntityManager em;

    private final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:SS");

    public SearchRespDto search(IataRateMasterSearchDto dto) {
        Map<String, Object> params = new HashMap<>();
        String hql = "";

        if (dto.getSearchCarrier() != null && dto.getSearchCarrier().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }

            hql = hql + " Upper(carrierMaster.carrierName) like '" + dto.getSearchCarrier().trim().toUpperCase()
                    + "%' ";
        }
        if (dto.getSearchPol() != null && dto.getSearchPol().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }

            hql = hql + " Upper(pol.portName) like '" + dto.getSearchPol().trim().toUpperCase() + "%' ";
        }

        if (dto.getSearchValidFromDate() != null && dto.getSearchValidFromDate().getStartDate() != null && dto.getSearchValidFromDate().getEndDate() != null) {

            try {
                Date dt1 = sdf.parse(dto.getSearchValidFromDate().getStartDate());
                Date dt2 = sdf.parse(dto.getSearchValidFromDate().getEndDate());

                if (hql.trim().length() != 0) {
                    hql = hql + " AND ";
                }

                params.put("validFromStartDate", dt1);
                params.put("validFromEndDate", dt2);
                hql = hql + " ( validFromDate >= :validFromStartDate AND validFromDate <= :validFromEndDate ) ";

                log.info("dates are " + dt1 + dt2);

            } catch (ParseException e1) {
                log.error("Date Conversion error in validFromDate: ", e1);
            }
        }


        if (dto.getSearchValidToDate() != null && dto.getSearchValidToDate().getStartDate() != null && dto.getSearchValidToDate().getEndDate() != null) {

            try {
                Date dt3 = sdf.parse(dto.getSearchValidToDate().getStartDate());
                Date dt4 = sdf.parse(dto.getSearchValidToDate().getEndDate());

                if (hql.trim().length() != 0) {
                    hql = hql + " AND ";
                }

                params.put("validToStartDate", dt3);
                params.put("validToEndDate", dt4);

                log.info("dates are " + dt3 + dt4);

                hql = hql + " ( validToDate >= :validToStartDate AND validToDate <= :validToEndDate ) ";

            } catch (ParseException e1) {
                log.error("Date Conversion error in validFromDate: ", e1);
            }
        }


        if (hql.trim().length() != 0) {
            hql = " where " + hql;
        }

        hql = " from IataRateMaster " + hql;

        if (dto.getSortByColumn() != null && dto.getOrderByType() != null && dto.getSortByColumn().trim().length() != 0
                && dto.getOrderByType().trim().length() != 0) {
            hql = hql + " order by " + dto.getSortByColumn().trim() + " " + dto.getOrderByType().trim();
        } else {
            hql = hql + " order by carrierMaster.carrierName ASC";
        }


        log.info("IataRate Master Search hql:[" + hql + "]");

        String countQ = "Select count (id) " + hql;

        Query countQuery = em.createQuery(countQ);
        TypedQuery<IataRateMaster> query = em.createQuery(hql, IataRateMaster.class);

        if (params != null) {

            for (Entry<String, Object> e : params.entrySet()) {
                countQuery.setParameter(e.getKey(), e.getValue());
            }

            for (Entry<String, Object> e : params.entrySet()) {
                query.setParameter(e.getKey(), e.getValue());
            }
        }

        Long countTotal = (Long) countQuery.getSingleResult();

        query.setFirstResult(dto.getSelectedPageNumber() * dto.getRecordPerPage());

        query.setMaxResults(dto.getRecordPerPage());

        List<IataRateMaster> resultlist = query.getResultList();

        SearchRespDto searchRespDto = new SearchRespDto(countTotal, resultlist);

        log.info(resultlist.size() + " number of rows selected...");

        return searchRespDto;


    }


    public void checkIataRateUnique(IataRateMaster ir) {
        log.info("check IataRate Unique method called");

        String hql = " from IataRateMaster where ";

        hql = hql + " carrierMaster.id = :carrier AND ";

        hql = hql + " pol.id = :pol";

        hql = hql + " AND validFromDate = :validFrom AND validToDate = :validTo ";

        hql = hql + " AND ( validFromDate BETWEEN :validFrom AND :validTo ";

        hql = hql + " OR validToDate BETWEEN :validFrom AND :validTo )";


        Query query = em.createQuery(hql);

        query.setParameter("carrier", ir.getCarrierMaster().getId());
        query.setParameter("pol", ir.getPol().getId());
        query.setParameter("validFrom", ir.getValidFromDate());
        query.setParameter("validTo", ir.getValidToDate());
        List<?> resultSet = query.getResultList();

        long duplicateCount = query.getResultList().size();

        if (duplicateCount > 0) {
            throw new RestException(ErrorCode.IATA_RATES_ALREADY_EXISTS);
        }


    }


}
