package com.efreightsuite.search;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import com.efreightsuite.dto.CarrierRateSearchDto;
import com.efreightsuite.dto.SearchRespDto;
import com.efreightsuite.model.CarrierRateMaster;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class CarrierRateSearchImpl {

    @Autowired
    private
    EntityManager em;

    public SearchRespDto search(CarrierRateSearchDto dto) {

        String hql = "";

        Map<String, Object> params = new HashMap<>();

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

        if (dto.getSearchCarrierName() != null && dto.getSearchCarrierName().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }
            hql = hql + " UPPER(carrierMaster.carrierName) like '" + dto.getSearchCarrierName().trim().toUpperCase() + "%' ";

        }

        if (dto.getSearchChargeName() != null && dto.getSearchChargeName().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }
            hql = hql + " UPPER(chargeMaster.chargeName) like '" + dto.getSearchChargeName().trim().toUpperCase() + "%' ";

        }

        if (dto.getSearchUnitName() != null && dto.getSearchUnitName().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }
            hql = hql + " UPPER(unitMaster.unitName) like '" + dto.getSearchUnitName().trim().toUpperCase() + "%' ";

        }

        if (dto.getSearchDestination() != null && dto.getSearchDestination().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }
            hql = hql + " UPPER(destination.portName) like '" + dto.getSearchDestination().trim().toUpperCase() + "%' ";

        }

        if (dto.getSearchorigin() != null && dto.getSearchorigin().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }
            hql = hql + " UPPER(origin.portName) like '" + dto.getSearchorigin().trim().toUpperCase() + "%' ";

        }

        if (dto.getSearchFirstAmount() != null && dto.getSearchFirstAmount().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }
            hql = hql + " UPPER(firstAmount) like '" + dto.getSearchFirstAmount().trim().toUpperCase() + "%' ";

        }

        if (dto.getSearchFirstMinAmount() != null && dto.getSearchFirstMinAmount().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }
            hql = hql + " UPPER(firstMinAmount) like '" + dto.getSearchFirstMinAmount().trim().toUpperCase() + "%' ";

        }

        if (dto.getSearchSecondAmount() != null && dto.getSearchSecondAmount().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }
            hql = hql + " UPPER(secondAmount) like '" + dto.getSearchSecondAmount().trim().toUpperCase() + "%' ";

        }

        if (dto.getSearchSecondMinAmount() != null && dto.getSearchSecondMinAmount().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }
            hql = hql + " UPPER(secondMinAmount) like '" + dto.getSearchSecondMinAmount().trim().toUpperCase() + "%' ";

        }

        if (dto.getSearchActualChargeble() != null && dto.getSearchActualChargeble().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }
            hql = hql + " UPPER(actualChargeable) like '" + dto.getSearchActualChargeble().trim().toUpperCase() + "%' ";

        }

        if (dto.getSearchFirstFromDate() != null && dto.getSearchFirstFromDate().getStartDate() != null
                && dto.getSearchFirstFromDate().getEndDate() != null) {
            try {
                Date dt1 = sdf.parse(dto.getSearchFirstFromDate().getStartDate());
                Date dt2 = sdf.parse(dto.getSearchFirstFromDate().getEndDate());

                if (hql.trim().length() != 0) {
                    hql = hql + " AND ";
                }

                params.put("fromStartDate", dt1);
                params.put("fromEndDate", dt2);
                hql = hql + " ( firstFromDate >= :fromStartDate AND firstFromDate <= :fromEndDate ) ";

            } catch (ParseException e) {
                log.error("Date Conversion error in carrier rate: +", e);
            }

        }

        if (dto.getSearchSecondFromDate() != null && dto.getSearchSecondFromDate().getStartDate() != null
                && dto.getSearchSecondFromDate().getEndDate() != null) {
            try {
                Date dt1 = sdf.parse(dto.getSearchSecondFromDate().getStartDate());
                Date dt2 = sdf.parse(dto.getSearchSecondFromDate().getEndDate());

                if (hql.trim().length() != 0) {
                    hql = hql + " AND ";
                }

                params.put("fromStartDate", dt1);
                params.put("fromEndDate", dt2);
                hql = hql + " ( secondFromDate >= :fromStartDate AND secondFromDate <= :fromEndDate ) ";

            } catch (ParseException e) {
                log.error("Date Conversion error in carrier rate: ", e);
            }

        }

        if (hql.trim().length() != 0) {
            hql = " where " + hql;
        }

        hql = " from CarrierRateMaster " + hql;


        if (dto.getSortByColumn() != null && dto.getOrderByType() != null && dto.getSortByColumn().trim().length() != 0
                && dto.getOrderByType().trim().length() != 0) {
            hql = hql + " order by Upper(" + dto.getSortByColumn().trim() + ") " + dto.getOrderByType().trim();
        } else {
            hql = hql + " order by Upper(carrierMaster.carrierName) ASC";
        }

        log.info("HQL : " + hql);

        String countQ = "Select count (id) " + hql;

        Query countQuery = em.createQuery(countQ);
        for (Entry<String, Object> e : params.entrySet()) {
            countQuery.setParameter(e.getKey(), e.getValue());
        }
        Long countTotal = (Long) countQuery.getSingleResult();


        TypedQuery<CarrierRateMaster> query = em.createQuery(hql, CarrierRateMaster.class);

        for (Entry<String, Object> e : params.entrySet()) {
            query.setParameter(e.getKey(), e.getValue());
        }

        query.setFirstResult(dto.getSelectedPageNumber() * dto.getRecordPerPage());

        query.setMaxResults(dto.getRecordPerPage());

        List<CarrierRateMaster> resultlist = query.getResultList();

        log.info("Number of rows selected : " + resultlist.size());

        return new SearchRespDto(countTotal, resultlist);

    }

}
