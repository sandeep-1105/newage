package com.efreightsuite.search;

import com.efreightsuite.dto.LocationSearchDto;
import com.efreightsuite.dto.SearchRequest;
import com.efreightsuite.dto.SearchRespDto;
import com.efreightsuite.enumeration.LovStatus;
import com.efreightsuite.model.LocationMaster;
import com.efreightsuite.repository.LocationMasterRepository;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.efreightsuite.constants.MatcherConstants.*;
import static java.util.stream.Collectors.toList;

@Service
@Log4j2
public class LocationSearchService {

    @Autowired
    private LocationMasterRepository locationMasterRepository;

    public SearchRespDto search(LocationSearchDto dto) {

        LocationMaster probe = new LocationMaster();
        probe.setLocationCode((StringUtils.isNotBlank(dto.getSearchCode())) ? dto.getSearchCode() : null);
        probe.setLocationName((StringUtils.isNotBlank(dto.getSearchName())) ? dto.getSearchName() : null);
        probe.setBranchName((StringUtils.isNotBlank(dto.getSearchBranchName())) ? dto.getSearchBranchName() : null);
        probe.setStatus((null != dto.getSearchStatus()) ? LovStatus.valueOf(dto.getSearchStatus()) : null);

        ExampleMatcher matcher = ExampleMatcher.matching()
                .withIgnoreCase()
                .withIgnorePaths(VERSION_LOCK)
                .withIgnoreNullValues()
                .withMatcher(LOCATION_CODE, ExampleMatcher.GenericPropertyMatcher::startsWith)
                .withMatcher(LOCATION_NAME, ExampleMatcher.GenericPropertyMatcher::startsWith)
                .withMatcher(BRANCH_NAME, ExampleMatcher.GenericPropertyMatcher::startsWith)
                .withMatcher(STATUS, ExampleMatcher.GenericPropertyMatcher::startsWith);

        Example<LocationMaster> example = Example.of(probe, matcher);

        Sort sortCriteria = dto.getSortCriteria(LOCATION_NAME);

        Page<LocationMaster> pageResult = locationMasterRepository.findAll(
                example,
                new PageRequest(dto.getSelectedPageNumber(), dto.getRecordPerPage(),
                        sortCriteria)
        );

        List<LocationMaster> result = pageResult.getContent();
        result = filterByCompanyName(result, dto.getSearchCompanyName());
        result = filterByCountryName(result, dto.getSearchCountryName());
        return new SearchRespDto(pageResult.getTotalElements(), result);

    }

    public SearchRespDto search(SearchRequest dto, Long countryId) {

        Page<LocationMaster> pageResult = search(dto);
        List<LocationMaster> result = pageResult.getContent();
        result = filterByHideStatus(result);
        result = filterByCountryId(result, countryId);
        return new SearchRespDto(pageResult.getTotalElements(), result);
    }


    public SearchRespDto searchByCompany(SearchRequest dto, Long companyId) {

        Page<LocationMaster> pageResult = search(dto);
        List<LocationMaster> result = pageResult.getContent();
        result = filterByHideStatus(result);
        result = filterByCompanyId(result, companyId);
        return new SearchRespDto(pageResult.getTotalElements(), result);

    }

    private Page<LocationMaster> search(SearchRequest dto) {
        LocationMaster probe = new LocationMaster();
        probe.setLocationName(dto.getKeywordNullable());
        probe.setLocationCode(dto.getKeywordNullable());

        ExampleMatcher matcher = ExampleMatcher.matchingAny()
                .withIgnoreCase()
                .withIgnorePaths(VERSION_LOCK)
                .withIgnoreNullValues()
                .withMatcher(LOCATION_NAME, ExampleMatcher.GenericPropertyMatcher::startsWith)
                .withMatcher(LOCATION_CODE, ExampleMatcher.GenericPropertyMatcher::startsWith);

        Example<LocationMaster> example = Example.of(probe, matcher);

        Sort sortCriteria = new Sort(Sort.Direction.ASC, LOCATION_NAME);
        Page<LocationMaster> pageResult = locationMasterRepository.findAll(
                example,
                new PageRequest(dto.getSelectedPageNumber(), dto.getRecordPerPage(),
                        sortCriteria)
        );
        return pageResult;
    }

    private List<LocationMaster> filterByHideStatus(List<LocationMaster> result) {
        if (null != result && result.size() > 0) {
            result = result
                    .stream()
                    .filter(locationMaster -> locationMaster.getStatus() != LovStatus.Hide)
                    .collect(toList());
        }
        return result;
    }

    private List<LocationMaster> filterByCountryId(List<LocationMaster> result, Long countryId) {
        if (null != result && result.size() > 0 && countryId != null && countryId != -1) {
            result = result
                    .stream()
                    .filter(locationMaster -> locationMaster.getCountryMaster().getId() == countryId)
                    .collect(toList());
        }
        return result;
    }

    private List<LocationMaster> filterByCountryName(List<LocationMaster> result, String countryName) {
        if (null != result && result.size() > 0 && StringUtils.isNotBlank(countryName)) {
            result = result
                    .stream()
                    .filter(locationMaster -> locationMaster.getCountryMaster().getCountryName().startsWith(countryName))
                    .collect(toList());
        }
        return result;
    }

    private List<LocationMaster> filterByCompanyId(List<LocationMaster> result, Long companyId) {
        if (null != result && result.size() > 0 && companyId != null && companyId != -1) {
            result = result
                    .stream()
                    .filter(locationMaster -> locationMaster.getCountryMaster().getId() == companyId)
                    .collect(toList());
        }
        return result;
    }

    private List<LocationMaster> filterByCompanyName(List<LocationMaster> result, String companyName) {
        if (null != result && result.size() > 0 && StringUtils.isNotBlank(companyName)) {
            result = result
                    .stream()
                    .filter(locationMaster -> locationMaster.getCompanyMaster().getCompanyName().startsWith(companyName))
                    .collect(toList());
        }
        return result;
    }

}
