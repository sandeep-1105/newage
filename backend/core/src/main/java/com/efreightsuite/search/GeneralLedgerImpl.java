package com.efreightsuite.search;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import com.efreightsuite.dto.SearchRequest;
import com.efreightsuite.dto.SearchRespDto;
import com.efreightsuite.enumeration.YesNo;
import com.efreightsuite.model.GeneralLedgerAccount;
import com.efreightsuite.service.AuthService;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class GeneralLedgerImpl {

    @Autowired
    private
    EntityManager em;

    public SearchRespDto getGLAccounts(SearchRequest dto, YesNo isSubLedger, YesNo isBank) {

        String hql = " from GeneralLedgerAccount where status!='Hide' AND companyMaster.id=" + AuthService.getCurrentUser().getSelectedCompany().getId() + " ";

        if (dto.getKeyword() != null && dto.getKeyword().trim().length() != 0) {

            hql = hql + " AND ( ";

            hql = hql + " UPPER(glAccountCode) like '" + dto.getKeyword().trim().toUpperCase() + "%' ";

            hql = hql + " OR ";

            hql = hql + " UPPER(glAccountName) like '" + dto.getKeyword().trim().toUpperCase() + "%' ";

            hql = hql + " ) ";
        }

        if (isSubLedger != null && isSubLedger == YesNo.Yes) {
            hql = hql + " AND isSubLedger = 'Yes' ";
        } else if (isSubLedger != null && isSubLedger == YesNo.No) {
            hql = hql + " AND isSubLedger = 'No' ";
        }

        if (isBank != null && isBank == YesNo.Yes) {
            hql = hql + " AND isBankAccount = 'Yes' ";
        } else if (isBank != null && isBank == YesNo.No) {
            hql = hql + " AND isBankAccount = 'No' ";
        }

        hql = hql + " order by glAccountName ASC";

        return search(dto.getSelectedPageNumber(), dto.getRecordPerPage(), hql);

    }

    private SearchRespDto search(int selectedPageNumber, int recordPerPage, String hql) {

        String countQ = "Select count (id) " + hql;

        Query countQuery = em.createQuery(countQ);

        Long countTotal = (Long) countQuery.getSingleResult();

        TypedQuery<GeneralLedgerAccount> query = em.createQuery(hql, GeneralLedgerAccount.class);

        query.setFirstResult(selectedPageNumber * recordPerPage);

        query.setMaxResults(recordPerPage);

        List<GeneralLedgerAccount> resultlist = query.getResultList();

        return new SearchRespDto(countTotal, resultlist);

    }
}
