package com.efreightsuite.search;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import com.efreightsuite.dto.LogoMasterSearchDto;
import com.efreightsuite.dto.SearchRespDto;
import com.efreightsuite.model.LogoMaster;
import com.efreightsuite.service.LogoMasterService;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class LogoMasterSearchImpl {

    @Autowired
    private
    EntityManager em;

    @Autowired
    LogoMasterService logoMasterService;

    public SearchRespDto search(LogoMasterSearchDto dto) {
        log.info("LogoMasterSearchImpl.search method started");

        String hql = "";

        if (dto.getSearchStatus() != null && dto.getSearchStatus().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }

            hql = hql + " Upper(status) like '" + dto.getSearchStatus().trim().toUpperCase() + "%' ";
        }

        if (dto.getSearchLocationName() != null && dto.getSearchLocationName().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }

            hql = hql
                    + " UPPER(locationMaster.locationName) like '" + dto.getSearchLocationName().trim().toUpperCase() + "%') ";
        }

        if (hql.trim().length() != 0) {
            hql = " where " + hql;
        }

        hql = " from LogoMaster " + hql;

        if (dto.getSortByColumn() != null && dto.getOrderByType() != null && dto.getSortByColumn().trim().length() != 0
                && dto.getOrderByType().trim().length() != 0) {
            hql = hql + " order by " + dto.getSortByColumn().trim() + " " + dto.getOrderByType().trim();
        } else {
            hql = hql + " order by locationMaster.locationName ASC";
        }

        log.info("LogoMaster Search hql:[" + hql + "]");

        String countQ = "Select count (id) " + hql;

        Query countQuery = em.createQuery(countQ);

        Long countTotal = (Long) countQuery.getSingleResult();

        TypedQuery<LogoMaster> query = em.createQuery(hql, LogoMaster.class);

        query.setFirstResult(dto.getSelectedPageNumber() * dto.getRecordPerPage());

        query.setMaxResults(dto.getRecordPerPage());

        List<LogoMaster> resultlistRes = query.getResultList();

        if (resultlistRes != null) {
            for (LogoMaster logoMaster : resultlistRes) {

                logoMaster.getLocationMaster().setCompanyMaster(null);
                logoMaster.getLocationMaster().setCountryMaster(null);
                logoMaster.getLocationMaster().setZoneMaster(null);
                logoMaster.getLocationMaster().setCityMaster(null);
                logoMaster.getLocationMaster().setCostCenterMaster(null);
                logoMaster.getLocationMaster().setPartyMaster(null);
                /*if(logoMaster.getLogo()!=null)
					logoMaster.setEncodedLogo(DatatypeConverter.printBase64Binary(logoMaster.getLogo()));*/
                logoMaster.setLogo(null);
            }

        }

        SearchRespDto searchRespDto = new SearchRespDto(countTotal, resultlistRes);

        log.info("LogoMasterSearchImpl.search method ended");
        return searchRespDto;
    }


    public LogoMaster getLogo(Long locationid) {

        String hql = "FROM LogoMaster lm WHERE lm.locationMaster like '" + locationid + "%' ";

        Query query = em.createQuery(hql);

        List<LogoMaster> list = query.getResultList();


        if (list.size() > 0) {
            return list.get(0);
        } else {
            return null;
        }
    }
}
