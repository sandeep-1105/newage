package com.efreightsuite.search;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.efreightsuite.dto.DefaultChargeRequestDto;
import com.efreightsuite.dto.SearchRespDto;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class DefaultChargeSearchImpl {

    @Autowired
    private
    EntityManager em;

    private final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:SS");

    public SearchRespDto search(DefaultChargeRequestDto dto) {

        log.info("DefaultChargeSearchImpl ->> search [" + dto + "]");

        Map<String, Object> params = new HashMap<>();


        String searchQuery = "SELECT dcm "
                + " FROM DefaultChargeMaster dcm"
                + " LEFT JOIN dcm.serviceMaster s"
                + " LEFT JOIN dcm.chargeMaster c"
                + " LEFT JOIN dcm.currencyMaster cu"
                + " LEFT JOIN dcm.unitMaster u"
                + " LEFT JOIN dcm.origin o"
                + " LEFT JOIN dcm.transitPort t"
                + " LEFT JOIN dcm.destination d"
                + " LEFT JOIN dcm.tosMaster tos";


        String countQuery = "SELECT COUNT(dcm.id) "
                + " FROM DefaultChargeMaster dcm"
                + " LEFT JOIN dcm.serviceMaster s"
                + " LEFT JOIN dcm.chargeMaster c"
                + " LEFT JOIN dcm.currencyMaster cu"
                + " LEFT JOIN dcm.unitMaster u"
                + " LEFT JOIN dcm.origin o"
                + " LEFT JOIN dcm.transitPort t"
                + " LEFT JOIN dcm.destination d"
                + " LEFT JOIN dcm.tosMaster tos";


        String filterQuery = "";

        if (dto.getSearchServiceCode() != null && dto.getSearchServiceCode().trim().length() != 0) {

            if (filterQuery.trim().length() != 0) {
                filterQuery = filterQuery + " AND ";
            }

            filterQuery = filterQuery + " Upper(dcm.serviceMaster.serviceCode) like '" + dto.getSearchServiceCode().trim().toUpperCase() + "%' ";
        }

        if (dto.getSearchChargeCode() != null && dto.getSearchChargeCode().trim().length() != 0) {

            if (filterQuery.trim().length() != 0) {
                filterQuery = filterQuery + " AND ";
            }

            filterQuery = filterQuery + " Upper(dcm.chargeMaster.chargeCode) like '" + dto.getSearchChargeCode().trim().toUpperCase() + "%' ";
        }

        if (dto.getSearchCurrencyCode() != null && dto.getSearchCurrencyCode().trim().length() != 0) {

            if (filterQuery.trim().length() != 0) {
                filterQuery = filterQuery + " AND ";
            }

            filterQuery = filterQuery + " Upper(dcm.currencyMaster.currencyCode) like '" + dto.getSearchCurrencyCode().trim().toUpperCase() + "%' ";
        }

        if (dto.getSearchUnitCode() != null && dto.getSearchUnitCode().trim().length() != 0) {

            if (filterQuery.trim().length() != 0) {
                filterQuery = filterQuery + " AND ";
            }

            filterQuery = filterQuery + " Upper(dcm.unitMaster.unitCode) like '" + dto.getSearchUnitCode().trim().toUpperCase() + "%' ";
        }


        if (dto.getSearchAmountPerUnit() != null && dto.getSearchAmountPerUnit().trim().length() != 0) {

            if (filterQuery.trim().length() != 0) {
                filterQuery = filterQuery + " AND ";
            }

            filterQuery = filterQuery + " Upper(dcm.amountPerUnit) like '" + dto.getSearchAmountPerUnit().trim().toUpperCase() + "%' ";
        }

        if (dto.getSearchMinAmount() != null && dto.getSearchMinAmount().trim().length() != 0) {

            if (filterQuery.trim().length() != 0) {
                filterQuery = filterQuery + " AND ";
            }

            filterQuery = filterQuery + " Upper(dcm.minAmount) like '" + dto.getSearchMinAmount().trim().toUpperCase() + "%' ";
        }

        if (dto.getSearchOriginCode() != null && dto.getSearchOriginCode().trim().length() != 0) {

            if (filterQuery.trim().length() != 0) {
                filterQuery = filterQuery + " AND ";
            }

            filterQuery = filterQuery + " Upper(dcm.origin.portCode) like '" + dto.getSearchOriginCode().trim().toUpperCase() + "%' ";
        }


        if (dto.getSearchTransitPortCode() != null && dto.getSearchTransitPortCode().trim().length() != 0) {

            if (filterQuery.trim().length() != 0) {
                filterQuery = filterQuery + " AND ";
            }

            filterQuery = filterQuery + " Upper(dcm.transitPort.portCode) like '" + dto.getSearchTransitPortCode().trim().toUpperCase() + "%' ";
        }

        if (dto.getSearchDestinationCode() != null && dto.getSearchDestinationCode().trim().length() != 0) {

            if (filterQuery.trim().length() != 0) {
                filterQuery = filterQuery + " AND ";
            }

            filterQuery = filterQuery + " Upper(dcm.destination.portCode) like '" + dto.getSearchDestinationCode().trim().toUpperCase() + "%' ";
        }

        if (dto.getSearchTosCode() != null && dto.getSearchTosCode().trim().length() != 0) {

            if (filterQuery.trim().length() != 0) {
                filterQuery = filterQuery + " AND ";
            }

            filterQuery = filterQuery + " Upper(dcm.tosMaster.tosCode) like '" + dto.getSearchTosCode().trim().toUpperCase() + "%' ";
        }

        if (dto.getSearchPpcc() != null && dto.getSearchPpcc().trim().length() != 0) {

            if (filterQuery.trim().length() != 0) {
                filterQuery = filterQuery + " AND ";
            }

            filterQuery = filterQuery + " Upper(dcm.ppcc) like '" + dto.getSearchPpcc().trim().toUpperCase() + "%' ";
        }

        if (dto.getSearchWhoRouted() != null && dto.getSearchWhoRouted().trim().length() != 0) {

            if (filterQuery.trim().length() != 0) {
                filterQuery = filterQuery + " AND ";
            }
            filterQuery = filterQuery + " Upper(dcm.whoRouted) like '" + dto.getSearchWhoRouted().trim().toUpperCase() + "%' ";
        }


        if (dto.getSearchValidFrom() != null && dto.getSearchValidFrom().getStartDate() != null && dto.getSearchValidFrom().getEndDate() != null) {

            try {
                Date dt1 = sdf.parse(dto.getSearchValidFrom().getStartDate());
                Date dt2 = sdf.parse(dto.getSearchValidFrom().getEndDate());

                if (filterQuery.trim().length() != 0) {
                    filterQuery = filterQuery + " AND ";
                }

                params.put("startDate", dt1);
                params.put("endDate", dt2);

                filterQuery = filterQuery + " ( dcm.validFrom >= :startDate AND dcm.validFrom <= :endDate ) ";

            } catch (ParseException e1) {
                log.error("Date Conversion error in Default Charge Search validFrom Date : ", e1);
            }

        }

        if (dto.getSearchValidTo() != null && dto.getSearchValidTo().getStartDate() != null && dto.getSearchValidTo().getEndDate() != null) {

            try {
                Date dt3 = sdf.parse(dto.getSearchValidTo().getStartDate());
                Date dt4 = sdf.parse(dto.getSearchValidTo().getEndDate());

                if (filterQuery.trim().length() != 0) {
                    filterQuery = filterQuery + " AND ";
                }

                params.put("startDate", dt3);
                params.put("endDate", dt4);

                filterQuery = filterQuery + " ( dcm.validTo >= :startDate AND dcm.validTo <= :endDate ) ";

            } catch (ParseException e1) {
                log.error("Date Conversion error in Default Charge Search validTo Date : ", e1);
            }

        }


        if (filterQuery.trim().length() != 0) {
            filterQuery = " WHERE " + filterQuery;
        }


        String orderByQuery = "";

        if (dto.getSortByColumn() != null && dto.getSortByColumn().trim().length() != 0) {

            if (dto.getSortByColumn().trim().equalsIgnoreCase("serviceCode")) {
                orderByQuery = "dcm.serviceMaster.serviceName";
            } else if (dto.getSortByColumn().trim().equalsIgnoreCase("chargeCode")) {
                orderByQuery = "dcm.chargeMaster.chargeName";
            } else if (dto.getSortByColumn().trim().equalsIgnoreCase("currencyCode")) {
                orderByQuery = "dcm.currencyMaster.currencyName";
            } else if (dto.getSortByColumn().trim().equalsIgnoreCase("unitCode")) {
                orderByQuery = "dcm.unitMaster.unitName";
            } else if (dto.getSortByColumn().trim().equalsIgnoreCase("amountPerUnit")) {
                orderByQuery = "dcm.amountPerUnit";
            } else if (dto.getSortByColumn().trim().equalsIgnoreCase("minAmount")) {
                orderByQuery = "dcm.minAmount";
            } else if (dto.getSortByColumn().trim().equalsIgnoreCase("originCode")) {
                orderByQuery = "dcm.origin.portName";
            } else if (dto.getSortByColumn().trim().equalsIgnoreCase("transitPortCode")) {
                orderByQuery = "dcm.transitPort.portName";
            } else if (dto.getSortByColumn().trim().equalsIgnoreCase("destinationCode")) {
                orderByQuery = "dcm.destination.portName";
            } else if (dto.getSortByColumn().trim().equalsIgnoreCase("tosCode")) {
                orderByQuery = "dcm.tosMaster.tosName";
            } else if (dto.getSortByColumn().trim().equalsIgnoreCase("ppcc")) {
                orderByQuery = "dcm.ppcc";
            } else if (dto.getSortByColumn().trim().equalsIgnoreCase("validFrom")) {
                orderByQuery = "dcm.whoRouted";
            } else if (dto.getSortByColumn().trim().equalsIgnoreCase("validTo")) {
                orderByQuery = "dcm.validFrom";
            } else {
                orderByQuery = "dcm.validTo";
            }

        }


        if (orderByQuery.trim().length() != 0) {
            orderByQuery = " ORDER BY Upper(" + orderByQuery + ") " + dto.getOrderByType();
        } else {
            orderByQuery = " ORDER BY Upper(dcm.serviceMaster.serviceName) ASC";
        }


        countQuery = countQuery + filterQuery;
        log.info("HQL Count Query : " + countQuery);
        Query countQ = em.createQuery(countQuery);
        for (Entry<String, Object> e : params.entrySet()) {
            countQ.setParameter(e.getKey(), e.getValue());
        }
        Long countTotal = (Long) countQ.getSingleResult();
        log.info("Total Number of Records Found : " + countTotal);


        searchQuery = searchQuery + filterQuery + orderByQuery;
        log.info("HQL Search Query  : " + searchQuery);
        Query sQuery = em.createQuery(searchQuery);
        sQuery.setFirstResult(dto.getSelectedPageNumber() * dto.getRecordPerPage());
        sQuery.setMaxResults(dto.getRecordPerPage());
        for (Entry<String, Object> e : params.entrySet()) {
            sQuery.setParameter(e.getKey(), e.getValue());
        }


        return new SearchRespDto(countTotal, sQuery.getResultList());

    }

}
