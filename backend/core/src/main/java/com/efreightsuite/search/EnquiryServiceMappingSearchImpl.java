package com.efreightsuite.search;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import com.efreightsuite.dto.EnquiryServiceMappingSearchDto;
import com.efreightsuite.dto.SearchRespDto;
import com.efreightsuite.model.EnquiryServiceMapping;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class EnquiryServiceMappingSearchImpl {

    private static final String AND = " AND ";
    @Autowired
    private
    EntityManager em;

    public SearchRespDto search(EnquiryServiceMappingSearchDto dto) {

        String hql = "";


        if (StringUtils.isNotEmpty(dto.getSearchService().toString())) {
            if (hql != null && hql.trim().length() != 0) {
                hql = hql + " AND ";
            }
            hql = hql + " service like '" + dto.getSearchService() + "' ";
        }
        if (StringUtils.isNotEmpty(dto.getSearchImportExport().toString())) {
            if (hql != null && hql.trim().length() != 0) {
                hql = hql + " AND ";
            }
            hql = hql + " importExport like '" + dto.getSearchImportExport() + "' ";
        }


        if (StringUtils.isNotEmpty(dto.getSearchTransport())) {
            if (hql != null && hql.trim().length() != 0) {
                hql = hql + " AND ";
            }
            hql = hql + " UPPER(serviceMaster.serviceName) like '" + dto.getSearchTransport().toUpperCase() + "%' ";
        }
        if (hql.trim().length() != 0) {
            hql = " where " + hql;
        }

        hql = " from EnquiryServiceMapping " + hql;

        if (dto.getSortByColumn() != null && dto.getOrderByType() != null && dto.getSortByColumn().trim().length() != 0
                && dto.getOrderByType().trim().length() != 0) {
            hql = hql + " order by Upper(" + dto.getSortByColumn().trim() + ") " + dto.getOrderByType().trim();
        } else {
            hql = hql + " order by Upper(service) ASC";
        }

        log.info("EnquiryServiceMapping Search hql:[" + hql + "]");

        return search(dto.getSelectedPageNumber(), dto.getRecordPerPage(), hql);

    }

    private SearchRespDto search(int selectedPageNumber, int recordPerPage, String hql) {

        StringBuilder countQ = new StringBuilder("Select count (id) ");
        Query countQuery = em.createQuery(countQ.append(hql).toString());

        Long countTotal = (Long) countQuery.getSingleResult();

        TypedQuery<EnquiryServiceMapping> query = em.createQuery(hql, EnquiryServiceMapping.class);

        query.setFirstResult(selectedPageNumber * recordPerPage);

        query.setMaxResults(recordPerPage);

        List<EnquiryServiceMapping> resultlist = query.getResultList();

        SearchRespDto searchRespDto = new SearchRespDto(countTotal, resultlist);

        log.info("Number of rows selected : " + resultlist.size());

        return searchRespDto;

    }

}
