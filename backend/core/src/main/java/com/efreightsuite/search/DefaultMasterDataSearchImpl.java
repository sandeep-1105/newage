package com.efreightsuite.search;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import com.efreightsuite.dto.DefaultMasterSearchDto;
import com.efreightsuite.dto.SearchRequest;
import com.efreightsuite.dto.SearchRespDto;
import com.efreightsuite.model.DefaultMasterData;
import com.efreightsuite.service.AuthService;
import com.efreightsuite.util.AppUtil;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class DefaultMasterDataSearchImpl {

    @Autowired
    private
    EntityManager em;


    public SearchRespDto search(SearchRequest dto) {

        String hql = " from DefaultMasterData where status!='Hide' AND location.id=" + AuthService.getCurrentUser().getSelectedUserLocation().getId();

        if (dto.getKeyword() != null && dto.getKeyword().trim().length() != 0) {

            hql = hql + " AND ( ";

            hql = hql + " Upper(code) like '" + dto.getKeyword().trim().toUpperCase() + "%' ";

            hql = hql + " OR ";

            hql = hql + " Upper(defaultName) like '" + dto.getKeyword().trim().toUpperCase() + "%' ";

            hql = hql + " ) ";

        }

        hql = hql + " order by defaultName ASC";

        log.info("Default Master Search hql:[" + hql + "]");

        return search(dto.getSelectedPageNumber(), dto.getRecordPerPage(), hql);
    }


    public SearchRespDto search(DefaultMasterSearchDto dto) {

        String hql = " from DefaultMasterData where location.id=" + AuthService.getCurrentUser().getSelectedUserLocation().getId();

        if (dto.getSearchSoftware() != null && dto.getSearchSoftware().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }

            hql = hql + " softwareMaster.id IN (select cm.id from SoftwareMaster cm where Upper(cm.softwareName) like '" + dto.getSearchSoftware().trim().toUpperCase() + "%') ";

        }

        if (dto.getSearchGroup() != null && dto.getSearchGroup().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }

            hql = hql + " objectGroupMaster.id IN (select cm.id from ObjectGroupMaster cm where Upper(cm.objectGroupName) like '" + dto.getSearchGroup().trim().toUpperCase() + "%') ";

        }

        if (dto.getSearchSubGroup() != null && dto.getSearchSubGroup().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }

            hql = hql + " objectSubGroupMaster.id IN (select cm.id from ObjectSubGroupMaster cm where Upper(cm.objectSubGroupName) like '" + dto.getSearchSubGroup().trim().toUpperCase() + "%') ";

        }


        if (dto.getSearchDefaultCode() != null && dto.getSearchDefaultCode().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }

            hql = hql + " Upper(code) like '" + dto.getSearchDefaultCode().trim().toUpperCase() + "%' ";
        }

        if (dto.getSearchDefaultName() != null && dto.getSearchDefaultName().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }

            hql = hql + " Upper(defaultName) like '" + dto.getSearchDefaultName().trim().toUpperCase() + "%' ";
        }


        if (dto.getSearchValue() != null && dto.getSearchValue().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }

            hql = hql + " Upper(value) like '" + dto.getSearchValue().toUpperCase() + "%' ";
        }

        if (dto.getSearchScopeFlag() != null && dto.getSearchScopeFlag().toString().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }

            hql = hql + " Upper(scopeFlag) like '" + dto.getSearchScopeFlag().toString().toUpperCase() + "%' ";
        }


        if (dto.getSortByColumn() != null && dto.getOrderByType() != null && dto.getSortByColumn().trim().length() != 0
                && dto.getOrderByType().trim().length() != 0) {
            hql = hql + " order by  Upper(" + dto.getSortByColumn().trim() + ") " + dto.getOrderByType().trim();
        } else {
            hql = hql + " order by Upper(defaultName) ASC";
        }

        log.info("Default Master Search hql:[" + hql + "]");

        return search(dto.getSelectedPageNumber(), dto.getRecordPerPage(), hql);

    }


    private SearchRespDto search(int selectedPageNumber, int recordPerPage, String hql) {
        String countQ = "Select count (id) " + hql;

        Query countQuery = em.createQuery(countQ);

        Long countTotal = (Long) countQuery.getSingleResult();

        TypedQuery<DefaultMasterData> query = em.createQuery(hql, DefaultMasterData.class);

        query.setFirstResult(selectedPageNumber * recordPerPage);

        query.setMaxResults(recordPerPage);

        List<DefaultMasterData> resultlist = query.getResultList();

        for (DefaultMasterData dcm : resultlist) {
            AppUtil.setPartyToNull(dcm.getLocation().getPartyMaster());
        }


        SearchRespDto searchRespDto = new SearchRespDto(countTotal, resultlist);

        log.info("Number of rows selected : " + resultlist.size());

        return searchRespDto;

    }
}
