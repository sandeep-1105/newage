package com.efreightsuite.search;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.transaction.Transactional;

import com.efreightsuite.dto.AirlinePrebookingSearchRequestDto;
import com.efreightsuite.dto.SearchRespDto;
import com.efreightsuite.enumeration.SequenceType;
import com.efreightsuite.enumeration.StockStatus;
import com.efreightsuite.model.AirlinePrebooking;
import com.efreightsuite.model.AirlinePrebookingSchedule;
import com.efreightsuite.model.StockGeneration;
import com.efreightsuite.repository.AirlinePrebookingRepository;
import com.efreightsuite.repository.StockRepository;
import com.efreightsuite.service.SequenceGeneratorService;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class AirlinePrebookingSearchImpl {

    @Autowired
    private
    EntityManager em;

    @Autowired
    private
    StockRepository stockRepository;

    @Autowired
    private
    SequenceGeneratorService sequenceGeneratorService;

    @Autowired
    private
    AirlinePrebookingRepository airlinePrebookingRepository;

    private final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:SS");

    public SearchRespDto search(AirlinePrebookingSearchRequestDto requestDto) {

        Map<String, Object> params = new HashMap<>();

        String searchQuery = "Select new com.efreightsuite.dto.AirlinePrebookingSearchResponseDto("
                + "apb.id, apb.prebookingNumber, apb.prebookingDate, apb.carrierMaster.carrierName,"
                + "apb.serviceMaster.serviceName, apb.por.portName,"
                + "apb.partyMaster.partyName, apb.noOfPieces, "
                + "apb.volume, apb.grossWeight, apb.volumeWeight, apb.noOfMawb, co.locale)"
                + "from AirlinePrebooking apb "
                + "left join apb.carrierMaster c "
                + "left join apb.serviceMaster s "
                + "left join apb.countryMaster co "
                + "left join apb.por p "
                + "left join apb.partyMaster pm ";

        String countQuery = "Select count(apb.id) from AirlinePrebooking apb "
                + "left join apb.carrierMaster c "
                + "left join apb.serviceMaster s "
                + "left join apb.por p "
                + "left join apb.countryMaster co "
                + "left join apb.partyMaster pm ";

        String filterQuery = "";

        if (requestDto.getPrebookingNumber() != null && requestDto.getPrebookingNumber().length() != 0) {

            if (filterQuery.trim().length() != 0) {
                filterQuery = filterQuery + " AND ";
            }

            filterQuery = filterQuery + " Upper(apb.prebookingNumber) like '"
                    + requestDto.getPrebookingNumber().trim().toUpperCase() + "%' ";
        }


        if (requestDto.getCarrierName() != null && requestDto.getCarrierName().length() != 0) {

            if (filterQuery.trim().length() != 0) {
                filterQuery = filterQuery + " AND ";
            }

            filterQuery = filterQuery + " Upper(apb.carrierMaster.carrierName) like '"
                    + requestDto.getCarrierName().trim().toUpperCase() + "%' ";
        }

        if (requestDto.getServiceName() != null
                && requestDto.getServiceName().length() != 0) {

            if (filterQuery.trim().length() != 0) {
                filterQuery = filterQuery + " AND ";
            }

            filterQuery = filterQuery + " Upper(apb.serviceMaster.serviceName) like '"
                    + requestDto.getServiceName().trim().toUpperCase() + "%' ";
        }

        if (requestDto.getPorName() != null
                && requestDto.getPorName().length() != 0) {

            if (filterQuery.trim().length() != 0) {
                filterQuery = filterQuery + " AND ";
            }

            filterQuery = filterQuery + " Upper(apb.por.portName) like '"
                    + requestDto.getPorName().trim().toUpperCase() + "%' ";
        }

        if (requestDto.getPartyName() != null
                && requestDto.getPartyName().length() != 0) {

            if (filterQuery.trim().length() != 0) {
                filterQuery = filterQuery + " AND ";
            }

            filterQuery = filterQuery
                    + " Upper(apb.partyMaster.partyName) like '"
                    + requestDto.getPartyName().trim().toUpperCase() + "%' ";
        }

        if (requestDto.getNoOfPieces() != null) {

            if (filterQuery.trim().length() != 0) {
                filterQuery = filterQuery + " AND ";
            }

            filterQuery = filterQuery + " apb.noOfPieces = " + requestDto.getNoOfPieces();
        }

        if (requestDto.getVolume() != null) {

            if (filterQuery.trim().length() != 0) {
                filterQuery = filterQuery + " AND ";
            }

            filterQuery = filterQuery + " apb.volume = " + requestDto.getVolume();
        }

        if (requestDto.getGrossWeight() != null) {

            if (filterQuery.trim().length() != 0) {
                filterQuery = filterQuery + " AND ";
            }

            filterQuery = filterQuery + " apb.grossWeight = " + requestDto.getGrossWeight();
        }

        if (requestDto.getVolumeWeight() != null) {

            if (filterQuery.trim().length() != 0) {
                filterQuery = filterQuery + " AND ";
            }

            filterQuery = filterQuery + " apb.volumeWeight = " + requestDto.getVolumeWeight();
        }

        if (requestDto.getNoOfMawb() != null) {

            if (filterQuery.trim().length() != 0) {
                filterQuery = filterQuery + " AND ";
            }

            filterQuery = filterQuery + " apb.noOfMawb = " + requestDto.getNoOfMawb();
        }


        if (requestDto.getPrebookingDate() != null && requestDto.getPrebookingDate().getStartDate() != null
                && requestDto.getPrebookingDate().getEndDate() != null) {

            try {

                Date date1 = sdf.parse(requestDto.getPrebookingDate().getStartDate());
                Date date2 = sdf.parse(requestDto.getPrebookingDate().getEndDate());

                if (filterQuery.trim().length() != 0) {
                    filterQuery = filterQuery + " AND ";
                }

                params.put("startDate", date1);
                params.put("endDate", date2);

                filterQuery = filterQuery
                        + "( apb.prebookingDate >= :startDate AND apb.prebookingDate <= :endDate )";

            } catch (ParseException exception) {
                log.error("Date Conversion Exception in prebookingDate....", exception);
            }

        }


        if (filterQuery.trim().length() != 0) {
            filterQuery = " WHERE " + filterQuery;
        }

        // Building Order By Query

        String orderByQuery = "";

        if (requestDto.getSortByColumn() != null
                && requestDto.getSortByColumn().length() != 0) {

            if (requestDto.getSortByColumn().equalsIgnoreCase("prebookingNumber")) {
                orderByQuery = orderByQuery + "apb.prebookingNumber";
            }

            if (requestDto.getSortByColumn().equalsIgnoreCase("carrierName")) {
                orderByQuery = orderByQuery + "apb.carrierMaster.carrierName";
            } else if (requestDto.getSortByColumn().equalsIgnoreCase("serviceName")) {
                orderByQuery = orderByQuery + "apb.ServiceMaster.serviceName";
            } else if (requestDto.getSortByColumn().equalsIgnoreCase("porName")) {
                orderByQuery = orderByQuery + "apb.por.portName";
            } else if (requestDto.getSortByColumn().equalsIgnoreCase("partyName")) {
                orderByQuery = orderByQuery + "apb.partyMaster.partyName";
            } else if (requestDto.getSortByColumn().equalsIgnoreCase("noOfPieces")) {
                orderByQuery = orderByQuery + "apb.noOfPieces";
            } else if (requestDto.getSortByColumn().equalsIgnoreCase("volume")) {
                orderByQuery = orderByQuery + "apb.volume";
            } else if (requestDto.getSortByColumn().equalsIgnoreCase("grossWeight")) {
                orderByQuery = orderByQuery + "apb.grossWeight";
            } else if (requestDto.getSortByColumn().equalsIgnoreCase("volumeWeight")) {
                orderByQuery = orderByQuery + "apb.volumeWeight";
            }


        }

        if (orderByQuery.length() != 0) {
            orderByQuery = " ORDER BY " + orderByQuery + " " + requestDto.getOrderByType();
        } else {
            orderByQuery = " ORDER BY apb.id DESC ";
        }


        countQuery = countQuery + filterQuery;
        Query countQ = em.createQuery(countQuery);
        for (Entry<String, Object> e : params.entrySet()) {
            countQ.setParameter(e.getKey(), e.getValue());
        }
        Long countTotal = (Long) countQ.getSingleResult();
        log.info("HQL Count Query For Search : " + countQuery);
        log.info("Total Number of Records Found : " + countTotal);

        searchQuery = searchQuery + filterQuery + orderByQuery;
        log.info("HQL Query For Search : " + searchQuery);
        Query query = em.createQuery(searchQuery);

        query.setFirstResult(requestDto.getSelectedPageNumber() * requestDto.getRecordPerPage());
        query.setMaxResults(requestDto.getRecordPerPage());

        for (Entry<String, Object> e : params.entrySet()) {
            query.setParameter(e.getKey(), e.getValue());
        }

        return new SearchRespDto(countTotal, query.getResultList());

    }

    @Transactional
    public AirlinePrebooking saveTransactional(AirlinePrebooking airlinePrebooking) {
        if (airlinePrebooking.getId() == null) {
            airlinePrebooking.setPrebookingNumber(sequenceGeneratorService.getSequenceValueByType(SequenceType.AIRLINE_PREBOOKING));
        }

        for (AirlinePrebookingSchedule airlinePrebookingSchedule : airlinePrebooking.getScheduleList()) {
            if (airlinePrebookingSchedule.getMawbNo() != null) {
                StockGeneration stockGeneration = stockRepository.findByMawb(airlinePrebookingSchedule.getMawbNo());
                stockGeneration.setStockStatus(StockStatus.Reserved);
                stockRepository.save(stockGeneration);
            }
        }
        airlinePrebooking = airlinePrebookingRepository.save(airlinePrebooking);
        return airlinePrebooking;
    }

}
