package com.efreightsuite.search;

import com.efreightsuite.dto.CountryMasterSearchDto;
import com.efreightsuite.dto.SearchRequest;
import com.efreightsuite.dto.SearchRespDto;
import com.efreightsuite.enumeration.LovStatus;
import com.efreightsuite.model.CountryMaster;
import com.efreightsuite.repository.CountryMasterRepository;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;
import org.springframework.stereotype.Service;

import java.util.List;

import static java.util.stream.Collectors.toList;

@Service
@Log4j2
public class CountrySearchService {

    @Autowired
    private CountryMasterRepository countryMasterRepository;

    public SearchRespDto search(SearchRequest dto) {

        CountryMaster probe = new CountryMaster();
        probe.setCountryCode(dto.getKeywordNullable());
        probe.setCountryName(dto.getKeywordNullable());

        ExampleMatcher matcher = ExampleMatcher.matchingAny()
                .withIgnoreCase()
                .withIgnorePaths("versionLock")
                .withIgnoreNullValues()
                .withMatcher("countryName", ExampleMatcher.GenericPropertyMatcher::startsWith)
                .withMatcher("countryCode", ExampleMatcher.GenericPropertyMatcher::startsWith);

        Example<CountryMaster> example = Example.of(probe, matcher);

        Sort sortCriteria = new Sort(Sort.Direction.ASC, "countryName");
        Page<CountryMaster> pageResult = countryMasterRepository.findAll(
                example,
                new PageRequest(dto.getSelectedPageNumber(), dto.getRecordPerPage(),
                        sortCriteria)
        );
        List<CountryMaster> result = pageResult.getContent()
                .stream()
                .filter(country -> country.getStatus() != LovStatus.Hide)
                .collect(toList());
        return new SearchRespDto(pageResult.getTotalElements(), result);

    }

    public SearchRespDto search(CountryMasterSearchDto dto) {

        CountryMaster probe = new CountryMaster();
        probe.setCountryCode((StringUtils.isNotBlank(dto.getSearchCountryCode()) ? dto.getSearchCountryCode() : null));
        probe.setCountryName((StringUtils.isNotBlank(dto.getSearchCountryName()) ? dto.getSearchCountryName() : null));
        probe.setCurrencyCode((StringUtils.isNotBlank(dto.getSearchCurrencyCode()) ? dto.getSearchCurrencyCode() : null));
        probe.setNationality((StringUtils.isNotBlank(dto.getSearchNationality()) ? dto.getSearchNationality() : null));
        probe.setStatus(dto.getSearchStatus());
        probe.setWeightOrCbm((null != dto.getSearchWeightOrCbm()) ? Double.valueOf(dto.getSearchWeightOrCbm()) : null);

        ExampleMatcher matcher = ExampleMatcher.matching()
                .withIgnoreCase()
                .withIgnorePaths("versionLock")
                .withIgnoreNullValues()
                .withMatcher("countryCode", ExampleMatcher.GenericPropertyMatcher::startsWith)
                .withMatcher("countryName", ExampleMatcher.GenericPropertyMatcher::startsWith)
                .withMatcher("currencyCode", ExampleMatcher.GenericPropertyMatcher::startsWith)
                .withMatcher("nationality", ExampleMatcher.GenericPropertyMatcher::startsWith)
                .withMatcher("status", ExampleMatcher.GenericPropertyMatcher::startsWith)
                .withMatcher("weightOrCbm", ExampleMatcher.GenericPropertyMatcher::startsWith);

        Example<CountryMaster> example = Example.of(probe, matcher);

        Sort sortCriteria = dto.getSortCriteria("countryName");

        Page<CountryMaster> pageResult = countryMasterRepository.findAll(
                example,
                new PageRequest(dto.getSelectedPageNumber(), dto.getRecordPerPage(),
                        sortCriteria)
        );
        List<CountryMaster> result = pageResult.getContent();
        return new SearchRespDto(pageResult.getTotalElements(), result);
    }

}
