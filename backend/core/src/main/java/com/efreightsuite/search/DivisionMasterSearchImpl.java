package com.efreightsuite.search;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import com.efreightsuite.dto.DivisionMasterSearchDto;
import com.efreightsuite.dto.SearchRequest;
import com.efreightsuite.dto.SearchRespDto;
import com.efreightsuite.model.DivisionMaster;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class DivisionMasterSearchImpl {

    @Autowired
    private
    EntityManager em;

    public SearchRespDto search(DivisionMasterSearchDto dto) {

        String hql = "";

        if (dto.getSearchDivisionName() != null && dto.getSearchDivisionName().trim().length() != 0) {
            if (hql != null && hql.trim().length() != 0) {
                hql = hql + " AND ";
            }
            hql = hql + " UPPER(divisionName) like '" + dto.getSearchDivisionName().toUpperCase() + "%' ";
        }

        if (dto.getSearchDivisionCode() != null && dto.getSearchDivisionCode().trim().length() != 0) {
            if (hql != null && hql.trim().length() != 0) {
                hql = hql + " AND ";
            }
            hql = hql + " UPPER(divisionCode) like '" + dto.getSearchDivisionCode().toUpperCase() + "%' ";
        }

        if (dto.getSearchCompanyName() != null && dto.getSearchCompanyName().trim().length() != 0) {
            if (hql != null && hql.trim().length() != 0) {
                hql = hql + " AND ";
            }
            hql = hql + " UPPER(companyMaster.companyName) like '" + dto.getSearchCompanyName().toUpperCase() + "%' ";
        }
        if (dto.getSearchStatus() != null && dto.getSearchStatus().toString().trim().length() != 0) {
            if (hql != null && hql.trim().length() != 0) {
                hql = hql + " AND ";
            }
            hql = hql + " UPPER(status) like '" + dto.getSearchStatus().toString().toUpperCase() + "%' ";
        }

        if (hql.trim().length() != 0) {
            hql = " where " + hql;
        }

        hql = " from DivisionMaster " + hql;

        if (dto.getSortByColumn() != null && dto.getOrderByType() != null && dto.getSortByColumn().trim().length() != 0
                && dto.getOrderByType().trim().length() != 0) {
            hql = hql + " order by Upper(" + dto.getSortByColumn().trim() + ") " + dto.getOrderByType().trim();
        } else {
            hql = hql + " order by Upper(divisionName) ASC";
        }

        log.info("DivisionMaster Search hql:[" + hql + "]");

        return search(dto.getSelectedPageNumber(), dto.getRecordPerPage(), hql);

    }

    public SearchRespDto search(SearchRequest dto, Long companyId) {

        String hql = " from DivisionMaster where status!='Hide' ";

        if (dto.getKeyword() != null && dto.getKeyword().trim().length() != 0) {

            hql = hql + " AND ( ";

            hql = hql + " UPPER(divisionName) like '" + dto.getKeyword().trim().toUpperCase() + "%' ";

            hql = hql + " OR ";

            hql = hql + " UPPER(divisionCode) like '" + dto.getKeyword().trim().toUpperCase() + "%' ";

            hql = hql + " ) ";
        }

        if (companyId != null) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }

            hql = hql + " companyMaster.id =" + companyId + " ";
        }


        hql = hql + " order by divisionName";

        log.info("Company Master Search hql:[" + hql + "]");

        return search(dto.getSelectedPageNumber(), dto.getRecordPerPage(), hql);

    }

    private SearchRespDto search(int selectedPageNumber, int recordPerPage, String hql) {

        String countQ = "Select count (id) " + hql;

        Query countQuery = em.createQuery(countQ);

        Long countTotal = (Long) countQuery.getSingleResult();

        TypedQuery<DivisionMaster> query = em.createQuery(hql, DivisionMaster.class);

        query.setFirstResult(selectedPageNumber * recordPerPage);
        query.setMaxResults(recordPerPage);
        List<DivisionMaster> resultlist = query.getResultList();

        return new SearchRespDto(countTotal, resultlist);
    }

}
