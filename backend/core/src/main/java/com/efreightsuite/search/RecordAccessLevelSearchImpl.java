package com.efreightsuite.search;


import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.efreightsuite.dto.RecordAccessLevelSearchDto;
import com.efreightsuite.dto.SearchRespDto;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class RecordAccessLevelSearchImpl {

    @Autowired
    private
    EntityManager em;


    public SearchRespDto search(RecordAccessLevelSearchDto dto) {

        log.info("RecordAccessLevelSearchImpl.search() method is called...");

        String searchQuery = "select new com.efreightsuite.dto.RecordAccessLevelSearchDto  "
                + " ( ral.id,   ral.recordAccessLevelName,  ral.description,  ral.status ) "
                + " from RecordAccessLevel ral ";

        String countQuery = "select count(ral.id) from RecordAccessLevel ral ";


        // Starting Filter

        String filterQuery = "";


        if (dto.getName() != null && dto.getName().length() != 0) {

            if (filterQuery.trim().length() != 0) {
                filterQuery = filterQuery + " AND ";
            }

            filterQuery += " Upper(ral.recordAccessLevelName) like '" + dto.getName().trim().toUpperCase() + "%' ";
        }

        if (dto.getDescription() != null && dto.getDescription().length() != 0) {

            if (filterQuery.trim().length() != 0) {
                filterQuery = filterQuery + " AND ";
            }

            filterQuery += " Upper(ral.description) like '" + dto.getDescription().trim().toUpperCase() + "%'";
        }

        if (dto.getStatus() != null && dto.getStatus().length() != 0) {

            if (filterQuery.trim().length() != 0) {
                filterQuery = filterQuery + " AND ";
            }

            filterQuery += " Upper(ral.status) like '" + dto.getStatus().trim().toUpperCase() + "%'";
        }


        if (filterQuery.trim().length() != 0) {
            filterQuery = " WHERE " + filterQuery;
        }

        countQuery = countQuery + filterQuery;
        Query countQ = em.createQuery(countQuery);

        log.info("HQL Count Query For Search : " + countQuery);

        Long countTotal = (Long) countQ.getSingleResult();
        log.info("Total Number of Records Found : " + countTotal);


        // Building Order By Query

        String orderByQuery = "";

        if (dto.getSortByColumn() != null && dto.getSortByColumn().length() != 0) {

            if (dto.getSortByColumn().equalsIgnoreCase("name")) {
                orderByQuery = orderByQuery + "ral.recordAccessLevelName";
            } else if (dto.getSortByColumn().equalsIgnoreCase("description")) {
                orderByQuery = orderByQuery + "ral.description";
            } else if (dto.getSortByColumn().equalsIgnoreCase("lovStatus")) {
                orderByQuery = orderByQuery + "ral.status";
            } else {
                orderByQuery = orderByQuery + "ral.recordAccessLevelName";
            }
        }

        if (orderByQuery.length() != 0) {
            orderByQuery = " ORDER BY Upper(" + orderByQuery + ") " + dto.getOrderByType();
        } else {
            orderByQuery = " ORDER BY Upper(ral.recordAccessLevelName) ASC ";
        }


        searchQuery = searchQuery + filterQuery + orderByQuery;
        log.info("HQL Query For Search : " + searchQuery);
        Query query = em.createQuery(searchQuery);

        query.setFirstResult(dto.getSelectedPageNumber() * dto.getRecordPerPage());
        query.setMaxResults(dto.getRecordPerPage());

        return new SearchRespDto(countTotal, query.getResultList());
    }
}
