package com.efreightsuite.search;

import com.efreightsuite.activiti.ActivitiTransactionService;
import com.efreightsuite.dto.*;
import com.efreightsuite.enumeration.*;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.model.*;
import com.efreightsuite.model.ServiceStatus;
import com.efreightsuite.repository.*;
import com.efreightsuite.service.AuthService;
import com.efreightsuite.service.SequenceGeneratorService;
import com.efreightsuite.util.*;
import com.efreightsuite.validation.ConsolValidator;
import com.efreightsuite.validation.InvoiceCreditNoteValidator;
import com.efreightsuite.validation.ProvisionalValidator;
import com.efreightsuite.validation.ShipmentServiceDetailValidator;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.transaction.Transactional;
import javax.transaction.Transactional.TxType;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Map.Entry;

@Service
@Log4j2
public class ConsolSearchImplNew {

    @Autowired
    private
    EntityManager em;

    private final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:SS");

    @Autowired
    private
    DocumentDetailRepository documentDetailRepository;

    @Autowired
    private
    ShipmentServiceDetailValidator shipmentServiceDetailValidator;

    @Autowired
    private
    CommentMasterRepository commentMasterRepository;

    @Autowired
    private
    LogoMasterRepository logoMasterRepository;

    @Autowired
    private
    LocationMasterRepository locationMasterRepository;

    @Autowired
    private
    ConsolSignOffRepository consolSignOffRepository;

    @Autowired
    private
    AppUtil appUtil;

    @Autowired
    private
    ConsolAttachmentRepository consolAttachmentRepository;

    @Autowired
    private
    InvoiceCreditNoteRepository invoiceCreditNoteRepository;

    @Autowired
    private
    DefaultMasterDataRepository defaultMasterDataRepository;

    @Autowired
    private
    ConsolValidator consolValidator;

    @Autowired
    private
    SequenceGeneratorService sequenceGeneratorService;

    @Autowired
    private
    ShipmentRepository shipmentRepository;

    @Autowired
    EventMasterRepository eventMasterRepository;

    @Autowired
    private
    ActivitiTransactionService activitiTransactionService;

    @Autowired
    ConsolEventRepository consolEventRepository;

    @Autowired
    private
    ShipmentServiceDetailRepository shipmentServiceDetailRepository;

    @Autowired
    private
    ConsolRepository consolRepository;

    @Autowired
    private
    StockRepository stockRepository;

    @Autowired
    private
    ProvisionalRepository provisionalRepository;


    @Autowired
    private
    SubJobSequenceRepository subJobSequenceRepository;

    @Autowired
    private
    ProvisionalValidator provisionalValidator;

    @Autowired
    private
    InvoiceCreditNoteValidator invoiceCreditNoteValidator;

    @Autowired
    private
    ShipmentSearchImpl shipmentSearchImpl;

    @Autowired
    private
    CompanyMasterRepository companyMasterRepository;

    @Autowired
    private
    CountryMasterRepository countryMasterRepository;

    @Autowired
    private
    CurrencyMasterRepository currencyMasterRepository;

    @Autowired
    private
    ServiceMasterRepository serviceMasterRepository;

    @Autowired
    private
    ChargeMasterRepository chargeMasterRepository;


    @Transactional
    public void processStatusChange(ConsolDto consolDto) {

        if (consolDto.getConsolId() != null && consolDto.getConsolStatus().equals("Cancelled")) {
            Consol consol = consolRepository.findById(consolDto.getConsolId());

            if (!invoiceCreditNoteValidator.accountingTransactionStatus(consol.getConsolUid(), null)) {
                throw new RestException(ErrorCode.INVOICE_MASTER_ACCOUNTING_NOT_DONE);
            }

            if (consol.getServiceMaster().getImportExport() == ImportExport.Export && consol.getServiceMaster().getTransportMode() == TransportMode.Air) {
                StockGeneration sg = stockRepository.findByMawb(consol.getConsolDocument().getMawbNo());
                if (sg != null) {
                    sg.setShipment(null);
                    sg.setShipmentUid(null);
                    sg.setServiceCode(null);
                    sg.setStockStatus(StockStatus.Available);
                    stockRepository.save(sg);
                }
            }

            for (ShipmentLink sl : consol.getShipmentLinkList()) {
                ShipmentDto dto = new ShipmentDto();
                dto.setServiceId(sl.getService().getId());
                dto.setShipmentId(sl.getService().getShipment().getId());
                shipmentSearchImpl.cancelProcess(dto);
            }

            consol = consolRepository.findById(consolDto.getConsolId());
            consol.getConsolDocument().setMawbIssueDate(null);
            consol.getConsolDocument().setMawbNo(null);
            consol.setJobStatus(JobStatus.Cancelled);
            consolRepository.save(consol);


        }
    }


    @Transactional(value = TxType.REQUIRES_NEW)
    private Integer getSubJobSequence(String colsolUid) {

        SubJobSequence obj = subJobSequenceRepository.getSubJobSequence(colsolUid);

        if (obj == null) {
            obj = new SubJobSequence();
            obj.setConsolUid(colsolUid);
            obj.setSequence(0);
            subJobSequenceRepository.save(obj);
        }

        Integer seq = obj.getSequence() + 1;
        obj.setSequence(seq);
        subJobSequenceRepository.save(obj);

        return seq;
    }

    private void setShipmentProvision(String consolUid, ShipmentLink shipmentLink) {


        Provisional provisional = provisionalRepository.findByServiceUid(shipmentLink.getService().getServiceUid());
        ShipmentServiceDetail service = shipmentServiceDetailRepository.findById(shipmentLink.getService().getId());

        Map<String, String> tmpMap = new HashMap<>();

        if (provisional == null) {

            provisional = new Provisional();

            provisional.setMasterUid(consolUid);
            provisional.setLocalCurrency(service.getLocalCurrency());
            provisional.setLocalCurrencyCode(service.getLocalCurrency().getCurrencyCode());
            provisional.setServiceName(service.getServiceMaster().getServiceName());
            provisional.setShipmentUid(service.getShipmentUid());
            provisional.setServiceUid(service.getServiceUid());

        } else {
            for (ProvisionItem pi : provisional.getProvisionalItemList()) {
                tmpMap.put(pi.getChargeMaster().getChargeCode(), pi.getChargeMaster().getChargeCode());
            }
        }


        if (service.getShipmentChargeList() != null && service.getShipmentChargeList().size() > 0) {
            for (ShipmentCharge charge : service.getShipmentChargeList()) {

                if (charge == null || charge.getId() == null) {
                    continue;
                }

                if (charge.getChargeMaster() != null && tmpMap.get(charge.getChargeMaster().getChargeCode()) != null) {
                    continue;
                }

                ProvisionItem pi = new ProvisionItem();
                pi.setChargeMaster(charge.getChargeMaster());
                pi.setChargeCode(charge.getChargeCode());

                if (charge != null && charge.getUnitMaster().getUnitType() == UnitType.Unit) {
                    if (charge.getActualChargeable() == ActualChargeable.ACTUAL) {
                        pi.setNoOfUnits(service.getBookedGrossWeightUnitKg());
                    } else {
                        pi.setNoOfUnits(service.getBookedChargeableUnit());
                    }
                } else {
                    pi.setNoOfUnits(1.0);
                }

                if (defaultMasterDataRepository.getCodeAndLocation("booking.rates.merged", service.getLocation().getId()).getValue().toUpperCase().equals("TRUE")) {
                    if (charge.getRateCurrency() != null) {
                        pi.setSellCurrency(charge.getRateCurrency());
                        pi.setSellCurrencyCode(charge.getRateCurrency().getCurrencyCode());
                        pi.setSellRoe(charge.getLocalRateAmountRoe());
                        pi.setSellAmountPerUnit(charge.getRateAmount());
                        pi.setSellAmount(charge.getTotalRateAmount());
                        pi.setSellLocalAmount(charge.getLocalTotalRateAmount());
                    }

                } else {

                    if (charge.getGrossSaleCurrency() != null) {
                        pi.setSellCurrency(charge.getGrossSaleCurrency());
                        pi.setSellCurrencyCode(charge.getRateCurrency().getCurrencyCode());
                        pi.setSellRoe(charge.getLocalGrossSaleRoe());
                        pi.setSellAmountPerUnit(charge.getGrossSaleAmount());
                        pi.setSellAmount(charge.getTotalGrossSale());
                        pi.setSellLocalAmount(charge.getLocalTotalGrossSale());
                    }

                }

                if (charge.getCostCurrency() != null) {
                    pi.setBuyCurrency(charge.getCostCurrency());
                    pi.setBuyCurrencyCode(charge.getCostCurrency().getCurrencyCode());
                    pi.setBuyRoe(charge.getLocalCostAmountRoe());
                    pi.setBuyAmountPerUnit(charge.getCostAmount());
                    pi.setBuyAmount(charge.getTotalCostAmount());
                    pi.setBuyLocalAmount(charge.getLocalTotalCostAmount());
                }


                pi.setProvisional(provisional);

                if (provisional.getProvisionalItemList() == null || provisional.getProvisionalItemList().isEmpty()) {
                    provisional.setProvisionalItemList(new ArrayList<>());
                }
                provisional.getProvisionalItemList().add(pi);
            }
        }

        double rAmt = 0.0, cAmt = 0.0;
        for (ProvisionItem pi : provisional.getProvisionalItemList()) {
            if (pi.getBuyLocalAmount() != null && pi.getBuyLocalAmount() > 0) {
                cAmt = cAmt + pi.getBuyLocalAmount();
            }

            if (pi.getSellLocalAmount() != null && pi.getSellLocalAmount() > 0) {
                rAmt = rAmt + pi.getSellLocalAmount();
            }
        }
        provisional.setTotalRevenue(rAmt);
        provisional.setTotalCost(cAmt);
        provisional.setNetAmount(rAmt - cAmt);

        provisionalValidator.validate(provisional);
        provisionalRepository.save(provisional);

    }


    @Transactional
    public void processCreate(BaseDto baseDto, Consol consol) throws CryptoException {

        List<Long> linkSericeIds = new ArrayList<>();

        String KEY = appUtil.getLocationConfig("shipment.attach.file.encrypt.key", consol.getLocation(), false);

        consolValidator.validate(consol);

        consol.setConsolUid(sequenceGeneratorService.getSequenceValueByType(SequenceType.CONSOL));

		/* Shipment Link start here */

        if (consol.getShipmentLinkList() != null && consol.getShipmentLinkList().size() != 0) {

            for (ShipmentLink cd : consol.getShipmentLinkList()) {


                if (cd.getId() == null &&
                        cd.getService().getShipmentChargeList() != null) {
                    setShipmentProvision(consol.getConsolUid(), cd);
                }


                cd.setSubJobSequence(getSubJobSequence(consol.getConsolUid()));

                ShipmentServiceDetail service = shipmentServiceDetailRepository.findById(cd.getService().getId());

                cd.setShipmentUid(service.getShipmentUid());
                cd.setServiceUid(service.getServiceUid());

                cd.setConsol(consol);
                cd.setConsolUid(consol.getConsolUid());

                cd.setCompany(consol.getCompany());

                if(cd.getCompany()!=null){
                    cd.setCompanyCode(companyMasterRepository.findById(cd.getCompany().getId()).getCompanyCode());
                }

                cd.setLocation(consol.getLocation());
                if(cd.getLocation()!=null) {
                    cd.setLocationCode(locationMasterRepository.findById(cd.getLocation().getId()).getLocationCode());
                }
                cd.setCountry(consol.getCountry());

                if(cd.getCountry()!=null) {
                    cd.setCountryCode(countryMasterRepository.findById(cd.getCountry().getId()).getCountryCode());
                }

                ShipmentServiceDetail ssd = shipmentRepository.getByService(cd.getService().getId());
                ssd.setConsolUid(consol.getConsolUid());

                ServiceStatus serviceStatus = new ServiceStatus();
                serviceStatus.setShipmentServiceDetail(ssd);
                serviceStatus.setServiceStatus(com.efreightsuite.enumeration.ServiceStatus.Generated);
                ssd.getServiceStatusList().add(serviceStatus);

                ssd.setLastUpdatedStatus(serviceStatus);

                ssd.setMawbNo(consol.getConsolDocument().getMawbNo());
                ssd.setMawbDate(consol.getConsolDocument().getMawbGeneratedDate());

                if (ssd.getIsTranshipment() != null && ssd.getIsTranshipment() == YesNo.Yes) {
                    ssd.setTranshipIsDoneLocation(YesNo.Yes);
                }

                shipmentServiceDetailRepository.save(ssd);

                linkSericeIds.add(ssd.getId());
                // shipmentRepository.updateServiceInConsol(consol.getConsolUid(),
                // cd.getService().getId());

            }
        } else {
            consol.setShipmentLinkList(null);
        }

		/* Shipment Link end here */

		/* Consol connections start here */

        if (consol.getConnectionList() != null && consol.getConnectionList().size() != 0) {
            for (ConsolConnection cc : consol.getConnectionList()) {
                cc.setConsol(consol);

            }
        } else {
            consol.setConnectionList(null);
        }
        /* Consol connections end here */

		/* Consol Charge Start here */

        if (consol.getChargeList() != null && consol.getChargeList().size() != 0) {
            for (ConsolCharge cc : consol.getChargeList()) {
                cc.setConsol(consol);
                cc.setConsolUid(consol.getConsolUid());

            }
        } else {
            consol.setChargeList(null);
        }
		/* Consol Charge End here */

		/* Document code start here */

        consol.getConsolDocument().setConsolUid(consol.getConsolUid());
        consol.getConsolDocument()
                .setDocumentNo(sequenceGeneratorService.getSequenceValueByType(SequenceType.DOCUMENT));

		/*if (consol.getServiceMaster().getImportExport().equals(ImportExport.Import))
			consol.getConsolDocument()
					.setDoNumber(sequenceGeneratorService.getSequenceValueByType(SequenceType.DONUMBER));*/
        if (consol.getConsolDocument().getDimensionList() != null
                && consol.getConsolDocument().getDimensionList().size() != 0) {
            for (ConsolDocumentDimension tmp : consol.getConsolDocument().getDimensionList()) {
                tmp.setDocumentDetail(consol.getConsolDocument());
            }
        } else {
            consol.getConsolDocument().setDimensionList(null);
        }

		/* Document code start here */

		/* Shipment Attachment Code */
        log.info("==================================================================================");
        log.info("Attachment Processing Begins....");

        if (consol.getConsolAttachmentList() != null && consol.getConsolAttachmentList().size() != 0) {
            log.info("There are " + consol.getConsolAttachmentList().size() + " number of attachments....");

            for (ConsolAttachment attachment : consol.getConsolAttachmentList()) {

                if (attachment.getIsProtected() != null && attachment.getIsProtected().equals(YesNo.Yes)) {
                    if (attachment.getFile() != null) {
                        attachment.setFile(CryptoUtils.encrypt(KEY, attachment.getFile()));
                    }
                }

                attachment.setConsol(consol);
            }
        } else {
            log.info("There are no attachment for this consol " + consol.getConsolUid());
            consol.setConsolAttachmentList(null);
        }

        log.info("Attachment Processing Finished...");
        log.info("==================================================================================");

		/* Consol Event Code Start here */

        if (consol.getEventList() != null && consol.getEventList().size() != 0) {
            for (ConsolEvent event : consol.getEventList()) {
                event.setConsol(consol);
            }
        } else {
            consol.setEventList(null);
        }

		/* Consol Event Code End here */

		/* Consol Reference Code Start here */

        if (consol.getReferenceList() != null && consol.getReferenceList().size() != 0) {
            for (ConsolReference reference : consol.getReferenceList()) {
                reference.setConsol(consol);
            }
        } else {
            consol.setReferenceList(null);
        }

		/* Consol Reference Code End here */

        // Consol pICK UP

        if (consol.getPickUpDeliveryPoint() != null) {

        }
        provisionalCost(consol);
        Consol savedCOnsol = consolRepository.save(consol);
        ServiceMaster serviceMaster = serviceMasterRepository.getOne(savedCOnsol.getServiceMaster().getId());

        if (savedCOnsol != null && serviceMaster.getImportExport().equals(ImportExport.Export)
                && savedCOnsol.getShipmentLinkList() != null && savedCOnsol.getShipmentLinkList().size() > 0) {
            if (savedCOnsol.getConsolDocument().getMawbNo() != null) {
                StockGeneration sgg = stockRepository.findByMawb(savedCOnsol.getConsolDocument().getMawbNo());
                if (sgg != null) {
                    if (sgg.getStockStatus().equals(StockStatus.Reserved) || sgg.getStockStatus().equals(StockStatus.Available)) {
                        sgg.setStockStatus(StockStatus.Generated);
                        if (sgg.getServiceCode() == null) {
                            sgg.setServiceCode(serviceMaster.getServiceCode());
                        }
                        sgg.setConsol(savedCOnsol);
                        sgg.setConsolUid(savedCOnsol.getConsolUid());
                        sgg.setLastUsedMawbDate(TimeUtil.getCurrentLocationTime());
                        stockRepository.save(sgg);
                    }
                }
            }
        }

        if (savedCOnsol != null) {
            if (savedCOnsol.getShipmentLinkList() != null && savedCOnsol.getShipmentLinkList().size() > 0) {
                for (ShipmentLink link : savedCOnsol.getShipmentLinkList()) {
                    if (link.getServiceUid() != null) {
                        Provisional pc = provisionalRepository.findByServiceUid(link.getServiceUid());
                        if (pc != null) {
                            pc.setMasterUid(savedCOnsol.getConsolUid());
                            provisionalRepository.save(pc);
                        }
                    }
                }
            }
        }


        activitiTransactionService.consolCreated(serviceMaster, savedCOnsol.getId(), linkSericeIds);
        if (savedCOnsol.getAtd() != null) {
            activitiTransactionService.shipmentAtd(serviceMaster, linkSericeIds);
        }

        baseDto.setResponseCode(ErrorCode.SUCCESS);
        baseDto.setResponseObject(new Consol(savedCOnsol));


    }


    @Transactional
    public void processUpdate(BaseDto baseDto, Consol consol) throws CryptoException {

        List<Long> linkSericeIds = new ArrayList<>();

        String KEY = appUtil.getLocationConfig("shipment.attach.file.encrypt.key", consol.getLocation(), false);

        consolValidator.validate(consol);


		/* Shipment Link start here */

        List<ShipmentServiceDetail> tmpServiceList = shipmentServiceDetailRepository.getByServiceByConsol(consol.getConsolUid());
        if (tmpServiceList != null && tmpServiceList.size() != 0) {
            for (ShipmentServiceDetail sd : tmpServiceList) {
                sd.setConsolUid(null);
            }
            shipmentServiceDetailRepository.save(tmpServiceList);
        }


        if (consol.getShipmentLinkList() != null && consol.getShipmentLinkList().size() != 0) {
            for (ShipmentLink cd : consol.getShipmentLinkList()) {
                ShipmentServiceDetail service = shipmentServiceDetailRepository.findById(cd.getService().getId());

                cd.setShipmentUid(service.getShipmentUid());
                cd.setServiceUid(service.getServiceUid());
                cd.getService().setShipment(null);

                if (cd.getSubJobSequence() == null) {
                    cd.setSubJobSequence(getSubJobSequence(consol.getConsolUid()));
                }


                cd.setConsol(consol);
                cd.setConsolUid(consol.getConsolUid());

                cd.setCompany(consol.getCompany());

                if(cd.getCompany()!=null){
                    cd.setCompanyCode(companyMasterRepository.findById(cd.getCompany().getId()).getCompanyCode());
                }

                cd.setLocation(consol.getLocation());
                if(cd.getLocation()!=null) {
                    cd.setLocationCode(locationMasterRepository.findById(cd.getLocation().getId()).getLocationCode());
                }

                cd.setCountry(consol.getCountry());
                if(cd.getCountry()!=null){
                    cd.setCountryCode(countryMasterRepository.findById(cd.getCountry().getId()).getCountryCode());
                }

                ShipmentServiceDetail ssd = shipmentRepository.getByService(cd.getService().getId());
                ssd.setConsolUid(consol.getConsolUid());

                ServiceStatus serviceStatus = new ServiceStatus();
                serviceStatus.setShipmentServiceDetail(ssd);
                serviceStatus.setServiceStatus(com.efreightsuite.enumeration.ServiceStatus.Generated);
                ssd.getServiceStatusList().add(serviceStatus);

                ssd.setLastUpdatedStatus(serviceStatus);


                if (ssd.getIsTranshipment() != null && ssd.getIsTranshipment() == YesNo.Yes) {
                    ssd.setTranshipIsDoneLocation(YesNo.Yes);
                }

                shipmentServiceDetailRepository.save(ssd);

                linkSericeIds.add(ssd.getId());

                List<InvoiceCreditNote> invoiceList = invoiceCreditNoteRepository.findByShipmentServiceDetail(ssd.getId());
                if (invoiceList != null && invoiceList.size() != 0) {
                    for (InvoiceCreditNote crn : invoiceList) {
                        crn.setMasterUid(consol.getConsolUid());
                    }

                    invoiceCreditNoteRepository.save(invoiceList);
                }

                // shipmentRepository.updateServiceInConsol(consol.getConsolUid(),
                // cd.getService().getId());
            }
        } else {
            consol.setShipmentLinkList(new ArrayList<>());
            consol.getShipmentLinkList().clear();
        }

		/* Shipment Link end here */

        // Consol Pick up delivery


		/* Consol Connections begins here... */
        if (consol.getConnectionList() != null && consol.getConnectionList().size() != 0) {
            for (ConsolConnection cc : consol.getConnectionList()) {
                cc.setConsol(consol);
            }
        } else {
            consol.setConnectionList(new ArrayList<>());
        }
		/* Consol Connections Code end here... */

		/* Consol Charge begins here... */
        if (consol.getChargeList() != null && consol.getChargeList().size() != 0) {
            for (ConsolCharge cc : consol.getChargeList()) {
                cc.setConsol(consol);
                cc.setConsolUid(consol.getConsolUid());
            }
        } else {
            consol.setChargeList(new ArrayList<>());
        }
		/* Consol Charge Code end here... */

		/* Document Code Start here */

        if (consol.getConsolDocument().getId() != null) {


        } else {
            consol.getConsolDocument().setConsolUid(consol.getConsolUid());
            consol.getConsolDocument()
                    .setDocumentNo(sequenceGeneratorService.getSequenceValueByType(SequenceType.DOCUMENT));

        }
        if (consol.getConsolDocument().getDimensionList() != null
                && consol.getConsolDocument().getDimensionList().size() != 0) {
            for (ConsolDocumentDimension tmp : consol.getConsolDocument().getDimensionList()) {
                tmp.setDocumentDetail(consol.getConsolDocument());
            }
        } else {

            consol.getConsolDocument().setDimensionList(new ArrayList<>());
        }

		/* Document Code End here */

        log.info("==================================================================================");

        if (consol.getConsolAttachmentList() != null && consol.getConsolAttachmentList().size() != 0) {

            for (ConsolAttachment attachment : consol.getConsolAttachmentList()) {

                attachment.setConsol(consol);

                if (attachment.getId() != null) {

                    ConsolAttachment dbAttachment = consolAttachmentRepository.getOne(attachment.getId());

                    if (attachment.getFile() != null) {

                        if (attachment.getIsProtected() != null && attachment.getIsProtected().equals(YesNo.Yes)) {
                            attachment.setFile(CryptoUtils.encrypt(KEY, attachment.getFile()));
                        }

                    } else {
                        if (attachment.getIsProtected() != null && attachment.getIsProtected().equals(YesNo.Yes)) {

                            if (dbAttachment.getIsProtected() != null && dbAttachment.getIsProtected().equals(YesNo.Yes)) {
                                attachment.setFile(dbAttachment.getFile());
                            } else {
                                attachment.setIsProtected(YesNo.No);
                                attachment.setFile(CryptoUtils.encrypt(KEY, dbAttachment.getFile()));
                            }

                        } else {

                            if (dbAttachment.getIsProtected() != null && dbAttachment.getIsProtected().equals(YesNo.Yes)) {
                                attachment.setFile(CryptoUtils.decrypt(KEY, dbAttachment.getFile()));
                            } else {
                                attachment.setIsProtected(YesNo.No);
                                attachment.setFile(dbAttachment.getFile());
                            }
                        }
                    }


                } else {
                    if (attachment.getIsProtected() != null && attachment.getIsProtected().equals(YesNo.Yes)) {
                        if (attachment.getFile() != null) {
                            attachment.setFile(CryptoUtils.encrypt(KEY, attachment.getFile()));
                        }
                    }
                }

            }
        } else {
            log.info("There are no attachments.......");
            consol.setConsolAttachmentList(new ArrayList<>());
        }

        log.info("==================================================================================");
		/* Attachment code ends here... */

		/* Consol Service Event begins here... */
        if (consol.getEventList() != null && consol.getEventList().size() != 0) {
            for (ConsolEvent event : consol.getEventList()) {
                event.setConsol(consol);
            }
        } else {
            consol.setEventList(new ArrayList<>());
        }
		
		
		/* Consol Service Event Code end here... */

		/* Consol Reference Code Start here */

        if (consol.getReferenceList() != null && consol.getReferenceList().size() != 0) {
            for (ConsolReference reference : consol.getReferenceList()) {
                reference.setConsol(consol);
            }
        } else {

            consol.setReferenceList(new ArrayList<>());
        }

		/* Consol Reference Code End here */
        provisionalCost(consol);
        Consol savedCOnsol = consolRepository.save(consol);
        ServiceMaster serviceMaster = serviceMasterRepository.getOne(savedCOnsol.getServiceMaster().getId());

        if (savedCOnsol.getIsJobCompleted() == YesNo.No
                || savedCOnsol.getIsJobCompleted().toString().equals(YesNo.No.toString())) {
            activitiTransactionService.consolCreated(serviceMaster, savedCOnsol.getId(), linkSericeIds);
        }

        if (savedCOnsol.getAtd() != null) {
            activitiTransactionService.shipmentAtd(serviceMaster, linkSericeIds);
        }

        baseDto.setResponseCode(ErrorCode.SUCCESS);
        baseDto.setResponseObject(new Consol(savedCOnsol));

    }

    @Transactional
    public BaseDto saveDocument(Consol cd, ShipmentServiceDetail sd, DocumentDetail dc, BaseDto baseDto) {
        if (cd != null && cd.getConsolUid() != null) {
            if (dc != null) {
                dc.setShipmentServiceDetail(sd);
                documentDetailRepository.save(dc);
                //shipmentServiceDetailRepository.save(sd);
                if (cd != null && cd.getShipmentLinkList() != null && cd.getShipmentLinkList().size() > 0) {
                    Double grossWeight = 0.0;
                    Long noOfPieces = 0L;
                    for (int i = 0; i < cd.getShipmentLinkList().size(); i++) {
                        if (cd.getShipmentLinkList().get(i).getService().getDocumentList() != null) {
                            for (int j = 0; j < cd.getShipmentLinkList().get(i).getService().getDocumentList().size(); j++) {
                                grossWeight = grossWeight + (cd.getShipmentLinkList().get(i).getService().getDocumentList().get(j).getGrossWeight());
                                noOfPieces = noOfPieces + (cd.getShipmentLinkList().get(i).getService().getDocumentList().get(j).getNoOfPieces());
                            }
                        }
                    }
                    cd.getConsolDocument().setGrossWeight(grossWeight);
                    cd.getConsolDocument().setNoOfPieces(noOfPieces);
                    Consol newConsol = consolRepository.save(cd);
                    baseDto.setResponseObject(newConsol.getId());
                } else {
                    throw new RestException("Consol is Mandatory");
                }
            } else {
                throw new RestException("Service is Mandatory");
            }
        }
        baseDto.setResponseCode(ErrorCode.SUCCESS);
        baseDto.setResponseDescription(ErrorCode.SUCCESS);
        return baseDto;
    }

    @Transactional
    public ConsolSignOff signOff(ConsolSignOff consolSignOff) {

        Consol consol = consolRepository.findByConsolUid(consolSignOff.getConsolUid());

        if (AuthService.getCurrentUser().getFeatureMap().get("AIR.MASTER_SHIPMENT.SIGNOFF.CREATE") != null
                && AuthService.getCurrentUser().getFeatureMap().get("AIR.MASTER_SHIPMENT.SIGNOFF.CREATE")) {

            String key = appUtil.getLocationConfig("master.shipment.signoff.all.or.agent.intra", consol.getLocation(), true);

            boolean isKey = key.equals("TRUE");

            if (consol.getConsolSignOff() != null && consol.getConsolSignOff().getId() != null && consol.getConsolSignOff().getIsSignOff() == YesNo.Yes) {
                throw new RestException(ErrorCode.SHIPMENT_SIGN_OFF_ALREADY);
            }

            List<String> unSuccessfull = new ArrayList<>();

            for (ShipmentLink sl : consol.getShipmentLinkList()) {

                if (appUtil.getLocationConfig("signoff.enabled", sl.getService().getLocation(), true).equals("FALSE")) {
                    throw new RestException(ErrorCode.SHIPMENT_SIGN_OFF_NOT_ENABLED);
                }

                if (!Objects.equals(consol.getDestination().getId(), sl.getService().getDestination().getId())) {
                    //Transhipment exclude for signoff
                    continue;
                }

                if (isKey) {
                    if (sl.getService().getWhoRouted() == WhoRouted.Self
                            && Objects.equals(sl.getService().getSalesman().getLocationMaster().getId(), sl.getService().getLocation().getId())) {
                        continue;
                    }
                }

                ShipmentServiceSignOff sg = null;

                try {

                    if (sl.getService().getShipmentServiceSignOff() != null && sl.getService().getShipmentServiceSignOff().isSignOff == YesNo.Yes) {
                        sg = sl.getService().getShipmentServiceSignOff();
                        continue;
                    }

                    sg = new ShipmentServiceSignOff();

                    if (sl.getService().getShipmentServiceSignOff() == null) {


                        sg.setSignOffBy(consolSignOff.getSignOffBy());
                        sg.setSignOffByCode(consolSignOff.getSignOffBy().getEmployeeCode());
                        sg.setSignOffDate(consolSignOff.getSignOffDate());
                        sg.setShipmentUid(sl.getService().getShipmentUid());
                        sg.setServiceUid(sl.getService().getServiceUid());
                        sg.setIsSignOff(YesNo.Yes);
                        sg.setConsolUid(consol.getConsolUid());

                        sl.getService().setShipmentServiceSignOff(sg);
                    }

                    shipmentServiceDetailValidator.signOff(sl.getService().getShipmentServiceSignOff(), sl.getService());
                } catch (Exception exception) {
                    log.error("Master Shipment Sign off failed :" + sl.getService().getServiceUid() + ": ", exception);
                    unSuccessfull.add(sl.getShipmentUid());
                    sl.getService().setShipmentServiceSignOff(sg);
                }

            }


            if (consolSignOff.getCommentMaster() == null || consolSignOff.getCommentMaster().getId() == null) {
                String code = appUtil.getLocationConfig("signoff.default.comment", consol.getLocation(), true);

                if (code != null && code.trim().length() != 0) {
                    CommentMaster commentMaster = commentMasterRepository.findByCommentCode(code);
                    if (commentMaster != null) {
                        consolSignOff.setCommentMaster(commentMaster);
                        consolSignOff.setCommentMasterCode(commentMaster.getCommentCode());
                        if (consolSignOff.getDescription() == null) {
                            consolSignOff.setDescription(commentMaster.getDescription());
                        }
                    }
                }


            }

            consolSignOff.setIsSignOff(YesNo.Yes);
            consolSignOff.setSignOffByCode(consolSignOff.getSignOffBy().getEmployeeCode());
            consolSignOff = consolSignOffRepository.save(consolSignOff);

            consol.setConsolSignOff(consolSignOff);

            consol = consolRepository.save(consol);

            if (unSuccessfull != null && unSuccessfull.size() != 0) {
                consol.getConsolSignOff().setUnSuccessfull(StringUtils.join(unSuccessfull, ','));
            }

            return consol.getConsolSignOff();


        } else {
            throw new RestException(ErrorCode.SHIPMENT_UNSIGN_OFF_INVALID_USER);
        }

    }

    @Transactional
    public BaseDto updateDoValues(List<DeliveryOrderDto> serviceDetailIdList, BaseDto bd) {
        Long totalPieces = 0L;
        Double totalGrossWt = 0.0;
        String doNumber = null;
        StringBuilder referenceNo = new StringBuilder();
        StringBuilder hawbNo = new StringBuilder();
        StringBuilder descriptionOfGoods = new StringBuilder();
        StringBuilder remark = new StringBuilder();
        DoResponseDto responseDto = new DoResponseDto();
        boolean isDo = true;
        for (DeliveryOrderDto dto : serviceDetailIdList) {
            DocumentDetail d = documentDetailRepository.findByHawbNo(dto.getHawbNo());
            ShipmentServiceDetail sd = shipmentServiceDetailRepository.findByServiceUid(d.getServiceUid());
            if (sd.getDocumentList().get(0).getReferenceNo() == null) {
                throw new RestException(ErrorCode.REFERNCE_NO_MANDATORY);
            }
            Consol c = consolRepository.findByConsolUid(sd.getConsolUid());
            if (c.getConsolDocument().getCarrierDoNo() == null) {
                throw new RestException(ErrorCode.CARRIER_DO_MANDATORY);
            }
            if (c.getConsolDocument().getIgmNo() == null) {
                throw new RestException(ErrorCode.IGM_NO_MANDATORY);
            }
            if (d != null && sd != null && isDo && d.getDoNumber() == null) {
                doNumber = sequenceGeneratorService.getSequenceValueByType(SequenceType.DO);
                d.setDoNumber(doNumber);
                isDo = false;
            } else {
                if (!isDo) {
                    d.setDoNumber(doNumber);
                }
            }
            d.setDoIssuedBy(AuthService.getCurrentUser().getEmployee());
            d.setDoIssuedDate(TimeUtil.getCurrentLocationTime());
            documentDetailRepository.save(d);
            if (responseDto.getIgmNo() == null) {
                responseDto.setIgmNo(c.getConsolDocument().getIgmNo());
            }
            if (responseDto.getMawbNo() == null) {
                responseDto.setMawbNo(c.getConsolDocument().getMawbNo());
            }
            if (responseDto.getDoNumber() == null) {
                responseDto.setDoNumber(d.getDocumentNo());
            }
            if (responseDto.getEta() == null) {
                responseDto.setEta(sd.getEta());
            }
            if (responseDto.getFlightNo() == null) {
                responseDto.setFlightNo(sd.getRouteNo());
            }
            if (responseDto.getDoDate() == null) {
                responseDto.setDoDate(d.getDoIssuedDate());
            }
            if (responseDto.getCarrierDo() == null) {
                responseDto.setCarrierDo(c.getConsolDocument().getCarrierDoNo() != null ? c.getConsolDocument().getCarrierDoNo() : "");
            }
            if (responseDto.getCompanyName() == null) {
                responseDto.setCompanyName(c.getCompany().getCompanyName());
            }
            if (responseDto.getLogo() == null) {
                LogoMaster lm = logoMasterRepository.findByLocationMaster(c.getLocation());
                if (lm != null && lm.getLogo() != null) {
                    responseDto.setLogo(lm.getLogo());
                } else {
                    responseDto.setLogo(null);
                }
            }
            if (responseDto.getAddress() == null) {
                LocationMaster lm = locationMasterRepository.findByLocationCode(c.getLocation().getLocationCode());
                String address = "";
                if (lm != null) {
                    StringJoiner joiner = new StringJoiner(",");
                    joiner.add((lm.getAddressLine1() != null ? lm.getAddressLine1() : "")).add((lm.getAddressLine2() != null ? lm.getAddressLine2() : "")).add((lm.getAddressLine3() != null ? lm.getAddressLine3() : "")).add((lm.getAddressLine4() != null ? lm.getAddressLine4() : ""));
                    address = joiner.toString();
                }
                if (address != null && address.trim().length() > 0) {
                    responseDto.setAddress((address).substring(0, address.length() - 1));
                }
            }
            totalPieces = totalPieces + (sd.getBookedPieces() != null ? sd.getBookedPieces() : 0L);
            totalGrossWt = totalGrossWt + (sd.getBookedGrossWeightUnitKg() != null ? sd.getBookedGrossWeightUnitKg() : 0.0);
            hawbNo.append(d.getHawbNo()).append(",");
            referenceNo.append(d.getReferenceNo()).append(",");
            descriptionOfGoods.append((d.getCommodityDescription() != null ? d.getCommodityDescription() : " ").trim()).append(",");
            remark.append((d.getMarksAndNo() != null ? d.getMarksAndNo() : " ").trim()).append(",");
        }
        if (hawbNo != null && hawbNo.toString() != "" && hawbNo.toString().trim().length() > 0) {
            responseDto.setHawbNo((hawbNo).substring(0, hawbNo.length() - 1));
        }
        if (referenceNo != null && referenceNo.toString() != "" && referenceNo.toString().trim().length() > 0) {
            responseDto.setReferenceNo((referenceNo).substring(0, referenceNo.length() - 1));
        }
        if (descriptionOfGoods != null && descriptionOfGoods.toString() != "" && descriptionOfGoods.toString().trim().length() > 0) {
            responseDto.setDescriptionOfGoods((descriptionOfGoods).substring(0, descriptionOfGoods.length() - 1));
        } else {
            responseDto.setDescriptionOfGoods(null);
        }
        if (remark != null && remark.toString() != "" && remark.toString().trim().length() > 0) {
            responseDto.setRemark((remark).substring(0, remark.length() - 1));
        } else {
            responseDto.setRemark(null);
        }
        responseDto.setTotalPieces(totalPieces);
        responseDto.setTotalGrossWt(totalGrossWt);
        bd.setResponseObject(responseDto);
        bd.setResponseCode(ErrorCode.SUCCESS);
        log.info("base dto object " + responseDto);
        return bd;
    }


    private void provisionalCost(Consol consol) {

        if (consol.getChargeList() == null || consol.getChargeList().size() == 0) {
            return;
        }


        if (consol.getShipmentLinkList() != null && consol.getShipmentLinkList().size() != 0) {
            //Back to Back and Direct Shipment. we are consider for service level

            for (ShipmentLink link : consol.getShipmentLinkList()) {
                Provisional provisional = provisionalRepository.findByServiceUid(link.getServiceUid());
                ShipmentServiceDetail service = shipmentServiceDetailRepository.findById(link.getService().getId());
                setProvisionalBuyRate(provisional, consol, service);
            }

        } else {
            Provisional provisional = provisionalRepository.findByMasterUid(consol.getConsolUid());
            setProvisionalBuyRate(provisional, consol, null);
        }

    }

    private void setProvisionalBuyRate(Provisional provisional, Consol consol, ShipmentServiceDetail service) {

        CurrencyMaster currency = currencyMasterRepository.getOne(consol.getCountry().getCurrencyMaster().getId());

        ServiceMaster serviceMaster = serviceMasterRepository.getOne(consol.getServiceMaster().getId());



        if (provisional == null) {
            provisional = new Provisional();
            if (service == null) {
                provisional.setMasterUid(consol.getConsolUid());

                provisional.setLocalCurrency(consol.getCountry().getCurrencyMaster());
                if (consol.getCountry().getCurrencyMaster() != null) {
                    provisional.setLocalCurrencyCode(currency.getCurrencyCode());
                } else {
                    throw new RestException(ErrorCode.CURRENCY_NOT_MAPPED_FOR_COUNTRY);
                }
                provisional.setServiceName(serviceMaster.getServiceName());
                provisional.setShipmentUid(null);
                provisional.setServiceUid(null);
            } else {
                provisional.setMasterUid(consol.getConsolUid());
                provisional.setLocalCurrency(service.getCountry().getCurrencyMaster());
                if (consol.getCountry().getCurrencyMaster() != null) {
                    provisional.setLocalCurrencyCode(currency.getCurrencyCode());
                } else {
                    throw new RestException(ErrorCode.CURRENCY_NOT_MAPPED_FOR_COUNTRY);
                }
                provisional.setServiceName(serviceMaster.getServiceName());
                provisional.setShipmentUid(service.getShipmentUid());
                provisional.setServiceUid(service.getServiceUid());
            }

        }


        for (ConsolCharge cc : consol.getChargeList()) {
            if (cc.getId() != null) {
                continue;
            }

            ChargeMaster chargeMaster = chargeMasterRepository.findById(cc.getChargeMaster().getId());
            CurrencyMaster currencyMaster = currencyMasterRepository.getOne(cc.getCurrency().getId());
            ProvisionItem pi = new ProvisionItem();
            pi.setChargeMaster(cc.getChargeMaster());
            pi.setChargeCode(chargeMaster.getChargeCode());
            pi.setBuyCurrency(cc.getCurrency());
            pi.setBuyCurrencyCode(currencyMaster.getCurrencyCode());
            pi.setBuyRoe(cc.getCurrencyRoe());
            pi.setBuyAmountPerUnit(cc.getAmountPerUnit());
            pi.setBuyAmount(cc.getAmount());
            pi.setNoOfUnits(cc.getUnit());
            pi.setBuyLocalAmount(cc.getLocalAmount());

            pi.setProvisional(provisional);

            if (provisional.getProvisionalItemList() == null || provisional.getProvisionalItemList().isEmpty()) {
                provisional.setProvisionalItemList(new ArrayList<>());
            }
            provisional.getProvisionalItemList().add(pi);
        }


        double rAmt = 0.0, cAmt = 0.0;
        for (ProvisionItem pi : provisional.getProvisionalItemList()) {
            if (pi.getBuyLocalAmount() != null && pi.getBuyLocalAmount() > 0) {
                cAmt = cAmt + pi.getBuyLocalAmount();
            }
        }
        provisional.setTotalRevenue(rAmt);
        provisional.setTotalCost(cAmt);
        provisional.setNetAmount(rAmt - cAmt);

        provisionalValidator.validate(provisional);
        provisional = provisionalRepository.save(provisional);


    }


    public SearchRespDto search(ConsolSearchDto dto) {
		Map<String, Object> params = new HashMap<>();
		
		log.info("ConsolSearchImplNew.search() method is called..."); 
		
		String searchQuery = "select new com.efreightsuite.dto.ConsolSearchResponseDto ( "
				+ " c.id, "
				+ " service.serviceName,"
				+ " c.consolUid, "
				+ " agent.partyName, "
				+ " cg.colorCode, "
				+ " cd.mawbNo, "
				+ " cd.mawbGeneratedDate, "
				+ " c.masterDate, "
				+ " origin.portName, "
				+ " destination.portName, "
				+ " c.ppcc, "
				+ " carrier.carrierName, "
				+ " c.etd, "
				+ " c.eta, "
				+ " created.employeeName, "
				+ " c.jobStatus)" 
				+ " from Consol c "
				+ " left join c.serviceMaster service "
				+ " left join c.agent.gradeMaster cg "
				+ " left join c.agent agent "
				+ " left join c.consolDocument cd "
				+ " left join c.origin origin "
				+ " left join c.destination destination "
				+ " left join c.carrier carrier "
				+ " left join c.createdBy created "
				+ " left join c.location loc "
				+ " left join c.country country "
				+ " left join c.routedAgent cust "
				+ " left join c.routedSalesman salesman "
				+ " left join loc.divisionMaster division ";
		
		String countQuery = "select count(c.id) " 
				+ " from Consol c "
				+ " left join c.serviceMaster service "
				+ " left join c.agent.gradeMaster cg "
				+ " left join c.agent agent "
				+ " left join c.consolDocument cd "
				+ " left join c.origin origin "
				+ " left join c.destination destination "
				+ " left join c.carrier carrier "
				+ " left join c.createdBy created "
				+ " left join c.location loc "
				+ " left join c.country country "
				+ " left join c.routedAgent cust "
				+ " left join c.routedSalesman salesman "
				+ " left join loc.divisionMaster division ";
		
		
		
		
		// Starting Filter
		
		String filterQuery = "";
		
		
		
		UserProfile userProfile = AuthService.getCurrentUser();

		Set<Long> locationList = new HashSet<>();
		
		if (userProfile.getSelectedUserLocation()!=null) {
			locationList.add(userProfile.getSelectedUserLocation().getId());
		}
		if (userProfile.getRecordAccessLevelProcessDTO() !=null) {
			
			//Location List
			if (userProfile.getRecordAccessLevelProcessDTO().getLocationList() != null
					&& !userProfile.getRecordAccessLevelProcessDTO().getLocationList().isEmpty()) {
				locationList.addAll(userProfile.getRecordAccessLevelProcessDTO().getLocationList());
			}
			
			//Country List
			if (userProfile.getRecordAccessLevelProcessDTO().getCountryList() != null
					&& !userProfile.getRecordAccessLevelProcessDTO().getCountryList().isEmpty()) {

				
				String ids = CommonUtils.commaSeparatedIds(userProfile.getRecordAccessLevelProcessDTO().getCountryList());
				if(ids.trim().length()>0) {
					if (filterQuery.trim().length() != 0) {
						filterQuery = filterQuery + " AND ";
					}
					filterQuery = filterQuery + " country.id   IN (" +ids+ ")";
				}
			}
			
			
			// Party List
			if (userProfile.getRecordAccessLevelProcessDTO().getCustomerList() != null
					&& !userProfile.getRecordAccessLevelProcessDTO().getCustomerList().isEmpty()) {
				String ids = CommonUtils.commaSeparatedIds(userProfile.getRecordAccessLevelProcessDTO().getCustomerList());
				if(ids.trim().length()>0) {
					if (filterQuery.trim().length() != 0) {
						filterQuery = filterQuery + " AND ";
					}
					filterQuery = filterQuery + " cust.id   IN (" +ids+ ")";
				}
			}
			// Salesman List
			if (userProfile.getRecordAccessLevelProcessDTO().getSalesmanList() != null
					&& !userProfile.getRecordAccessLevelProcessDTO().getSalesmanList().isEmpty()) {
				String ids = CommonUtils.commaSeparatedIds(userProfile.getRecordAccessLevelProcessDTO().getSalesmanList());
				if(ids.trim().length()>0) {
					if (filterQuery.trim().length() != 0) {
						filterQuery = filterQuery + " AND ";
					}
					filterQuery = filterQuery + " salesman.id   IN (" +ids+ ")";
				}
			}
			
			// Service List
			if (userProfile.getRecordAccessLevelProcessDTO().getServiceList() != null
					&& !userProfile.getRecordAccessLevelProcessDTO().getServiceList().isEmpty()) {
				String ids = CommonUtils.commaSeparatedIds(userProfile.getRecordAccessLevelProcessDTO().getServiceList());
				if(ids.trim().length()>0) {
					if (filterQuery.trim().length() != 0) {
						filterQuery = filterQuery + " AND ";
					}
					filterQuery = filterQuery + " service.id   IN (" +ids+ ")";
				}
			}
			// Division List
			if (userProfile.getRecordAccessLevelProcessDTO().getDivisionList() != null
					&& !userProfile.getRecordAccessLevelProcessDTO().getDivisionList().isEmpty()) {
				String ids = CommonUtils.commaSeparatedIds(userProfile.getRecordAccessLevelProcessDTO().getDivisionList());
				if(ids.trim().length()>0) {
					if (filterQuery.trim().length() != 0) {
						filterQuery = filterQuery + " AND ";
					}
					filterQuery = filterQuery + " division.id   IN (" +ids+ ")";
				}
			}
		}
		
		
		if(locationList != null && !locationList.isEmpty()) {
			String ids = CommonUtils.commaSeparatedIds(locationList);
			if(ids.trim().length()>0) {
				if (filterQuery.trim().length() != 0) {
					filterQuery = filterQuery + " AND ";
				}
				filterQuery = filterQuery + " loc.id   IN (" +ids+ ")";
			}
		}

		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		if(dto.getConsolUid() != null && dto.getConsolUid().length() != 0) {
			
			if (filterQuery.trim().length() != 0) {
				filterQuery = filterQuery + " AND ";
			}
			
			filterQuery += " Upper(c.consolUid) like '" + dto.getConsolUid().trim().toUpperCase() + "%'";
		}
		
		if(dto.getMasterNo() != null && dto.getMasterNo().length() != 0) {
			
			if (filterQuery.trim().length() != 0) {
				filterQuery = filterQuery + " AND ";
			}
			
			filterQuery += " Upper(cd.mawbNo) like '" + dto.getMasterNo().trim().toUpperCase() + "%'";
		}
		
		
		if(dto.getMawbDate() != null && dto.getMawbDate().getStartDate() != null && dto.getMawbDate().getEndDate() != null) {
			
			try {
				
				Date date1 = sdf.parse(dto.getMawbDate().getStartDate());
				Date date2 = sdf.parse(dto.getMawbDate().getEndDate());
				
				if(filterQuery.trim().length() != 0) {
					filterQuery = filterQuery + " AND ";
				}
				
				params.put("startDate1", date1);
				params.put("endDate1", date2);
				
				filterQuery = filterQuery + "( cd.mawbGeneratedDate  >= :startDate1 AND cd.mawbGeneratedDate  <= :endDate1 )";
				
			} catch(ParseException exception) {
				log.error("Date Conversion Exception in mawbGeneratedDate....", exception);
			}
			
		}
		
		
		if(dto.getShipmentDate() != null && dto.getShipmentDate().getStartDate() != null && dto.getShipmentDate().getEndDate() != null) {
			
			try {
				Date date1 = sdf.parse(dto.getShipmentDate().getStartDate());
				Date date2 = sdf.parse(dto.getShipmentDate().getEndDate());
				
				if(filterQuery.trim().length() != 0) {
					filterQuery = filterQuery + " AND ";
				}
				
				params.put("startDate2", date1);
				params.put("endDate2", date2);
				
				filterQuery = filterQuery + "( c.masterDate  >= :startDate2 AND c.masterDate  <= :endDate2 )";
				
			} catch(ParseException exception) {
				log.error("Date Conversion Exception in masterDate....", exception);
			}
			
		}
		
		
		
		if(dto.getOrigin() != null && dto.getOrigin().length() != 0) {
			
			if (filterQuery.trim().length() != 0) {
				filterQuery = filterQuery + " AND ";
			}
			
			filterQuery += " Upper(origin.portName) like '" + dto.getOrigin().trim().toUpperCase() + "%'";
		}

		if(dto.getDestination() != null && dto.getDestination().length() != 0) {
			
			if (filterQuery.trim().length() != 0) {
				filterQuery = filterQuery + " AND ";
			}
			
			filterQuery += " Upper(destination.portName) like '" + dto.getDestination().trim().toUpperCase() + "%'";
		}
		
		
		
		if(dto.getEtd() != null && dto.getEtd().getStartDate() != null && dto.getEtd().getEndDate() != null) {
			
			try {
				Date date1 = sdf.parse(dto.getEtd().getStartDate());
				Date date2 = sdf.parse(dto.getEtd().getEndDate());
				
				if(filterQuery.trim().length() != 0) {
					filterQuery = filterQuery + " AND ";
				}
				
				params.put("startDate3", date1);
				params.put("endDate3", date2);
				
				filterQuery = filterQuery + "( c.etd  >= :startDate3 AND c.etd  <= :endDate3 )";
				
			} catch(ParseException exception) {
				log.error("Date Conversion Exception in etd....", exception);
			}
			
		}


		if(dto.getEta() != null && dto.getEta().getStartDate() != null && dto.getEta().getEndDate() != null) {
	
			try {
				Date date1 = sdf.parse(dto.getEta().getStartDate());
				Date date2 = sdf.parse(dto.getEta().getEndDate());
				
				if(filterQuery.trim().length() != 0) {
					filterQuery = filterQuery + " AND ";
				}
				
				params.put("startDate4", date1);
				params.put("endDate4", date2);
				
				filterQuery = filterQuery + "( c.eta  >= :startDate4 AND c.eta  <= :endDate4 )";
				
			} catch(ParseException exception) {
				log.error("Date Conversion Exception in eta....", exception);
			}
			
		}
		
		if(dto.getStatus() != null && dto.getStatus().length() != 0) {
			
			if (filterQuery.trim().length() != 0) {
				filterQuery = filterQuery + " AND ";
			}
			
			filterQuery += " Upper(c.jobStatus) like '" + dto.getStatus().trim().toUpperCase() + "%'";
		}
		
		
		
		if (dto.getImportExport() != null && dto.getImportExport().trim().length() != 0 && (dto.getImportExport().trim().equals("Import") || dto.getImportExport().trim().equals("Export") )) {
		
			if (filterQuery.trim().length() != 0) {
				filterQuery = filterQuery + " AND ";
			}
		
			filterQuery += " Upper(service.importExport) like '" + dto.getImportExport().trim().toUpperCase() + "%'";
		}

		
		if(dto.getDirectShipment()!=null && (dto.getDirectShipment().equals("No")  || dto.getDirectShipment().equals("Yes"))){

			if (filterQuery.trim().length() != 0) {
				filterQuery = filterQuery + " AND ";
			}
			
			filterQuery += " c.directShipment like '" + dto.getDirectShipment() + "%'" ;
			
		}
		
		String advancedFilterQuery = "";
		
		if(dto.getSearchValue() != null && dto.getSearchValue().trim().length() != 0) {
			
			
			if(dto.getSearchName() != null && dto.getSearchName().trim().equals("Service")) {
				
				advancedFilterQuery = " Upper(service.serviceName)  like '" + dto.getSearchValue().trim().toUpperCase() + "%' ";
				
				if(dto.getServiceName() != null && dto.getServiceName().length() != 0) {
					advancedFilterQuery += " AND Upper(service.serviceName)  like '" + dto.getServiceName().trim().toUpperCase() + "%' ";
				}
				
			} else if(dto.getSearchName() != null && dto.getSearchName().trim().equals("Agent")) {
				
				advancedFilterQuery = " Upper(agent.partyName) like '" + dto.getSearchValue().trim().toUpperCase() + "%' ";
				
				if(dto.getPartyName() != null && dto.getPartyName().length() != 0) {
					advancedFilterQuery += " AND Upper(agent.partyName) like '" + dto.getPartyName().trim().toUpperCase() + "%' ";
				}
				
			} else if(dto.getSearchName() != null && dto.getSearchName().trim().equals("Freight")) {
				
				advancedFilterQuery = " Upper(c.ppcc) like '" + dto.getSearchValue().trim().toUpperCase() + "%' ";
				
				if(dto.getFreightTerm() != null && dto.getFreightTerm().length() != 0) {
					advancedFilterQuery += " AND Upper(c.ppcc) like '" + dto.getFreightTerm().trim().toUpperCase() + "%' ";
				}
				
			} else if(dto.getSearchName() != null && dto.getSearchName().trim().equals("Carrier")) {
				
				advancedFilterQuery = "  Upper(carrier.carrierName) like '" + dto.getSearchValue().trim().toUpperCase() + "%' ";
				
				if(dto.getCarrier() != null && dto.getCarrier().length() != 0) {
					advancedFilterQuery += " AND  Upper(carrier.carrierName) like '" + dto.getCarrier().trim().toUpperCase() + "%' ";
				}
				
			} else {
				advancedFilterQuery = "  Upper(created.employeeName) like '" + dto.getSearchValue().trim().toUpperCase() + "%' ";
				
				if(dto.getCreatedBy() != null && dto.getCreatedBy().length() != 0) {
					advancedFilterQuery += " AND  Upper(created.employeeName) like '" + dto.getCreatedBy().trim().toUpperCase() + "%' ";
				}
			}
			
			
			if (filterQuery.trim().length() != 0) {
				advancedFilterQuery =  advancedFilterQuery + " AND " + filterQuery;
			}
			
			
			if(advancedFilterQuery.trim().length() != 0) {
				advancedFilterQuery = " WHERE " + advancedFilterQuery;
			} 
			
		} else {
			
			if(filterQuery.trim().length() != 0) {
				filterQuery = " WHERE " + filterQuery;
			} 
		}
		
		
		
		if(advancedFilterQuery.trim().length() != 0) {
			filterQuery = advancedFilterQuery;
		}

		countQuery = countQuery  + filterQuery;
		Query countQ = em.createQuery(countQuery);

		log.info("HQL Count Query For Search : " + countQuery);
		log.info("HQL params For Search : " + params);
		for (Entry<String, Object> e : params.entrySet()) {
			
			countQ.setParameter(e.getKey(), e.getValue());
		}
		Long countTotal = (Long) countQ.getSingleResult();
		log.info("Total Number of Records Found : " + countTotal);
		
		
		
		
		
		// Building Order By Query

		String orderByQuery = "";

		if (dto.getSortByColumn() != null
				&& dto.getSortByColumn().length() != 0) {

			if (dto.getSortByColumn().equalsIgnoreCase("serviceName")) {
				orderByQuery = orderByQuery + "service.serviceName";
			}

			else if (dto.getSortByColumn().equalsIgnoreCase("masterId")) {
				orderByQuery = orderByQuery + "c.consolUid";
			}

			else if (dto.getSortByColumn().equalsIgnoreCase("agentName")) {
				orderByQuery = orderByQuery + "agent.partyName";
			}

			else if (dto.getSortByColumn().equalsIgnoreCase("mawbNumber")) {
				orderByQuery = orderByQuery + "cd.mawbNo";
			}

			else if (dto.getSortByColumn().equalsIgnoreCase("mawbDate")) {
				orderByQuery = orderByQuery + "cd.mawbGeneratedDate";
			}

			else if (dto.getSortByColumn().equalsIgnoreCase("masterDate")) {
				orderByQuery = orderByQuery + "c.masterDate";
			}

			else if (dto.getSortByColumn().equalsIgnoreCase("originName")) {
				orderByQuery = orderByQuery + "origin.portName";
			}

			else if (dto.getSortByColumn().equalsIgnoreCase("destinationName")) {
				orderByQuery = orderByQuery + "destination.portName";
			}

			else if (dto.getSortByColumn().equalsIgnoreCase("frieght")) {
				orderByQuery = orderByQuery + "c.ppcc";
			}

			else if (dto.getSortByColumn().equalsIgnoreCase("carrierName")) {
				orderByQuery = orderByQuery + "carrier.carrierName";
			}

			else if (dto.getSortByColumn().equalsIgnoreCase("etd")) {
				orderByQuery = orderByQuery + "c.etd";
			}

			else if (dto.getSortByColumn().equalsIgnoreCase("eta")) {
				orderByQuery = orderByQuery + "c.eta";
			}
			
			else if (dto.getSortByColumn().equalsIgnoreCase("createdBy")) {
				orderByQuery = orderByQuery + "created.employeeName";
			}
			
			else if (dto.getSortByColumn().equalsIgnoreCase("jobStatus")) {
				orderByQuery = orderByQuery + "c.jobStatus";
			}
			
		}

		if (orderByQuery.length() != 0) {
			orderByQuery = " ORDER BY " +orderByQuery+ " " + dto.getOrderByType();
		} else {
			orderByQuery = " ORDER BY c.id DESC ";
		}

		
		searchQuery = searchQuery  + filterQuery + orderByQuery;
		log.info("HQL Query For Search : " + searchQuery);
		Query query = em.createQuery(searchQuery);

        if(dto.getSelectedPageNumber()>=0 && dto.getRecordPerPage()>=0 ) {
            query.setFirstResult(dto.getSelectedPageNumber() * dto.getRecordPerPage());
            query.setMaxResults(dto.getRecordPerPage());
        }
		
		for (Entry<String, Object> e : params.entrySet()) {
			query.setParameter(e.getKey(), e.getValue());
		}

        return new SearchRespDto(countTotal, query.getResultList());
    }

    public SearchRespDto hawbSearchInDocument(AirDocumentSearchDto dto) {


        Map<String, Object> params = new HashMap<>();

        String standardConditionDd = " where dd.shipmentServiceDetail.id = ssd.id ";

        String searchQuery = "select new com.efreightsuite.dto.AirDocumentSearchResponseDto"
                + " (dd.id, dd.hawbNo, ssd.shipmentUid, shp.partyName, cons.partyName, org.portName, dst.portName, dd.chargebleWeight)"
                + " from DocumentDetail dd , ShipmentServiceDetail ssd"
                + " left join ssd.serviceMaster sm"
                + " left join dd.shipper shp"
                + " left join dd.consignee cons"
                + " left join ssd.origin org"
                + " left join ssd.destination dst "
                + standardConditionDd;


        String filterQuery = " AND ssd.serviceMaster.transportMode ='Air' "
                + " AND ssd.serviceMaster.importExport ='Export' "
                + " AND ssd.serviceMaster.serviceType is null "
				/*+ " AND dd.hawbNo is not null "*/
                + " AND ssd.location.id=" + AuthService.getCurrentUser().getSelectedUserLocation().getId();

        if (dto.getHawb() != null && dto.getHawb().trim().length() != 0) {

            if (filterQuery.trim().length() != 0) {
                filterQuery = filterQuery + " AND ";
            }

            filterQuery = filterQuery + " Upper(dd.hawbNo) like '" + dto.getHawb().trim().toUpperCase() + "%' ";

        }

        if (dto.getShipmentUid() != null && dto.getShipmentUid().trim().length() != 0) {

            if (filterQuery.trim().length() != 0) {
                filterQuery = filterQuery + " AND ";
            }
            filterQuery = filterQuery + " Upper(ssd.shipmentUid) like '" + dto.getShipmentUid().trim().toUpperCase() + "%' ";
        }

        if (dto.getShipperName() != null && dto.getShipperName().trim().length() != 0) {

            if (filterQuery.trim().length() != 0) {
                filterQuery = filterQuery + " AND ";
            }
            filterQuery = filterQuery + " Upper(dd.shipper.partyName) like '" + dto.getShipperName().trim().toUpperCase() + "%' ";
        }

        if (dto.getConsigneeName() != null && dto.getConsigneeName().trim().length() != 0) {

            if (filterQuery.trim().length() != 0) {
                filterQuery = filterQuery + " AND ";
            }
            filterQuery = filterQuery + " Upper(dd.consignee.partyName) like '" + dto.getConsigneeName().trim().toUpperCase() + "%' ";
        }

        if (dto.getOriginName() != null && dto.getOriginName().trim().length() != 0) {

            if (filterQuery.trim().length() != 0) {
                filterQuery = filterQuery + " AND ";
            }
            filterQuery = filterQuery + " Upper(ssd.origin.portName) like '" + dto.getOriginName().trim().toUpperCase() + "%' ";
        }

        if (dto.getDestinationName() != null && dto.getDestinationName().trim().length() != 0) {

            if (filterQuery.trim().length() != 0) {
                filterQuery = filterQuery + " AND ";
            }

            filterQuery = filterQuery + " Upper(ssd.destination.portName) like '" + dto.getDestinationName().trim().toUpperCase() + "%' ";
        }


        if (dto.getChargeableWeight() != null) {

            if (filterQuery.trim().length() != 0) {
                filterQuery = filterQuery + " AND ";
            }

            filterQuery = filterQuery + " dd.chargebleWeight = " + dto.getChargeableWeight();
        }

        String orderByQuery = "";

        if (dto.getSortByColumn() != null && dto.getSortByColumn().length() != 0) {
            if (dto.getSortByColumn().equalsIgnoreCase("shipmentUid")) {
                orderByQuery = orderByQuery + "ssd.shipmentUid";
            } else if (dto.getSortByColumn().equalsIgnoreCase("shipperName")) {
                orderByQuery = orderByQuery + "dd.shipper.partyName";
            } else if (dto.getSortByColumn().equalsIgnoreCase("consigneeName")) {
                orderByQuery = orderByQuery + "dd.consignee.partyName";
            } else if (dto.getSortByColumn().equalsIgnoreCase("originName")) {
                orderByQuery = orderByQuery + "ssd.origin.portName";
            } else if (dto.getSortByColumn().equalsIgnoreCase("destinationName")) {
                orderByQuery = orderByQuery + "ssd.destination.portName";
            } else if (dto.getSortByColumn().equalsIgnoreCase("hawb")) {
                orderByQuery = orderByQuery + "dd.hawb";
            } else if (dto.getSortByColumn().equalsIgnoreCase("chargeableWeight")) {
                orderByQuery = orderByQuery + "dd.chargebleWeight";
            }
        }

        if (orderByQuery.length() != 0) {
            orderByQuery = " ORDER BY " + orderByQuery + " " + dto.getOrderByType();
        } else {
            orderByQuery = " ORDER BY ssd.id DESC ";
        }

        searchQuery = searchQuery + filterQuery + orderByQuery;

        log.info("Search Query : " + searchQuery);


        String countQ = "Select count (dd.id) from DocumentDetail dd, ShipmentServiceDetail ssd " + standardConditionDd + " " + filterQuery;

        Query countQuery = em.createQuery(countQ);

        for (Entry<String, Object> e : params.entrySet()) {
            countQuery.setParameter(e.getKey(), e.getValue());
        }

        Long countTotal = (Long) countQuery.getSingleResult();


        Query query = em.createQuery(searchQuery);

        for (Entry<String, Object> e : params.entrySet()) {
            query.setParameter(e.getKey(), e.getValue());
        }

        query.setFirstResult(dto.getSelectedPageNumber() * dto.getRecordPerPage());
        query.setMaxResults(dto.getRecordPerPage());


        return new SearchRespDto(countTotal, query.getResultList());

    }


    public SearchRespDto mawbSearchInDocument(AirDocumentSearchDto dto) {


        Map<String, Object> params = new HashMap<>();

        String standardConditionDd = "where c.consolDocument.id = cd.id ";

        String searchQuery = "select new com.efreightsuite.dto.AirDocumentSearchResponseDto"
                + " (c.consolUid, cd.mawbNo, cd.id, shp.partyName, cons.partyName, org.portName, dst.portName, cd.chargebleWeight)"
                + " from ConsolDocument cd , Consol c "
                + " left join c.serviceMaster sm"
                + " left join cd.shipper shp"
                + " left join cd.firstNotify fn"
                + " left join cd.consignee cons"
                + " left join c.origin org"
                + " left join c.destination dst "
                + standardConditionDd;


        String filterQuery = " AND c.serviceMaster.transportMode ='Air' "
                + " AND c.serviceMaster.importExport ='Export' "
                + " AND c.serviceMaster.serviceType is null "
				/*+ " AND cd.mawbNo is not null "*/
                + " AND c.location.id=" + AuthService.getCurrentUser().getSelectedUserLocation().getId();


        if (dto.getMawb() != null && dto.getMawb().trim().length() != 0) {

            if (filterQuery.trim().length() != 0) {
                filterQuery = filterQuery + " AND ";
            }

            filterQuery = filterQuery + " Upper(cd.mawbNo) like '" + dto.getMawb().trim().toUpperCase() + "%' ";

        }

        if (dto.getMasterUid() != null && dto.getMasterUid().trim().length() != 0) {
            if (filterQuery.trim().length() != 0) {
                filterQuery = filterQuery + " AND ";
            }
            filterQuery = filterQuery + " Upper(c.consolUid) like '" + dto.getMasterUid().trim().toUpperCase() + "%' ";
        }

        if (dto.getShipperName() != null && dto.getShipperName().trim().length() != 0) {

            if (filterQuery.trim().length() != 0) {
                filterQuery = filterQuery + " AND ";
            }
            filterQuery = filterQuery + " Upper(cd.shipper.partyName) like '" + dto.getShipperName().trim().toUpperCase() + "%' ";
        }

        if (dto.getConsigneeName() != null && dto.getConsigneeName().trim().length() != 0) {

            if (filterQuery.trim().length() != 0) {
                filterQuery = filterQuery + " AND ";
            }
            filterQuery = filterQuery + " Upper(cd.consignee.partyName) like '" + dto.getConsigneeName().trim().toUpperCase() + "%' ";
        }


        if (dto.getOriginName() != null && dto.getOriginName().trim().length() != 0) {

            if (filterQuery.trim().length() != 0) {
                filterQuery = filterQuery + " AND ";
            }
            filterQuery = filterQuery + " Upper(c.origin.portName) like '" + dto.getOriginName().trim().toUpperCase() + "%' ";
        }

        if (dto.getDestinationName() != null && dto.getDestinationName().trim().length() != 0) {

            if (filterQuery.trim().length() != 0) {
                filterQuery = filterQuery + " AND ";
            }

            filterQuery = filterQuery + " Upper(c.destination.portName) like '" + dto.getDestinationName().trim().toUpperCase() + "%' ";
        }


        if (dto.getChargeableWeight() != null) {

            if (filterQuery.trim().length() != 0) {
                filterQuery = filterQuery + " AND ";
            }

            filterQuery = filterQuery + " cd.chargebleWeight = " + dto.getChargeableWeight();
        }


        String orderByQuery = "";

        if (dto.getSortByColumn() != null && dto.getSortByColumn().length() != 0) {
            if (dto.getSortByColumn().equalsIgnoreCase("masterUid")) {
                orderByQuery = orderByQuery + "c.consolUid";
            } else if (dto.getSortByColumn().equalsIgnoreCase("shipperName")) {
                orderByQuery = orderByQuery + "cd.shipper.partyName";
            } else if (dto.getSortByColumn().equalsIgnoreCase("consigneeName")) {
                orderByQuery = orderByQuery + "cd.consignee.partyName";
            } else if (dto.getSortByColumn().equalsIgnoreCase("originName")) {
                orderByQuery = orderByQuery + "c.origin.portName";
            } else if (dto.getSortByColumn().equalsIgnoreCase("destinationName")) {
                orderByQuery = orderByQuery + "c.destination.portName";
            } else if (dto.getSortByColumn().equalsIgnoreCase("mawb")) {
                orderByQuery = orderByQuery + "cd.mawbNo";
            } else if (dto.getSortByColumn().equalsIgnoreCase("chargeableWeight")) {
                orderByQuery = orderByQuery + "cd.chargebleWeight";
            }

        }

        if (orderByQuery.length() != 0) {
            orderByQuery = " ORDER BY " + orderByQuery + " " + dto.getOrderByType();
        } else {
            orderByQuery = " ORDER BY c.id DESC ";
        }

        searchQuery = searchQuery + filterQuery + orderByQuery;

        log.info("Search Query : " + searchQuery);

        String countQ = "Select count (cd.id) from ConsolDocument cd, Consol c " + standardConditionDd + " " + filterQuery;

        Query countQuery = em.createQuery(countQ);

        for (Entry<String, Object> e : params.entrySet()) {
            countQuery.setParameter(e.getKey(), e.getValue());
        }

        Long countTotal = (Long) countQuery.getSingleResult();

        Query query = em.createQuery(searchQuery);

        for (Entry<String, Object> e : params.entrySet()) {
            query.setParameter(e.getKey(), e.getValue());
        }

        query.setFirstResult(dto.getSelectedPageNumber() * dto.getRecordPerPage());
        query.setMaxResults(dto.getRecordPerPage());


        return new SearchRespDto(countTotal, query.getResultList());

    }

    public SearchRespDto doSearchInDocument(AirDocumentSearchDto dto) {


        Map<String, Object> params = new HashMap<>();

        String standardConditionDd = " where dd.shipmentServiceDetail.id = ssd.id ";

        String searchQuery = "select new com.efreightsuite.dto.AirDocumentSearchResponseDto"
                + " (dd.doNumber, dd.id, ssd.shipmentUid, shp.partyName, cons.partyName, org.portName, dst.portName, dd.chargebleWeight)"
                + " from DocumentDetail dd , ShipmentServiceDetail ssd"
                + " left join ssd.serviceMaster sm"
                + " left join dd.shipper shp"
                + " left join dd.consignee cons"
                + " left join ssd.origin org"
                + " left join ssd.destination dst "
                + standardConditionDd;


        String filterQuery = " AND ssd.serviceMaster.transportMode ='Air' "
                + " AND ssd.serviceMaster.importExport ='Import' "
                + " AND ssd.serviceMaster.serviceType is null "
			/*+ " AND dd.doNumber is not null "*/
                + " AND ssd.location.id=" + AuthService.getCurrentUser().getSelectedUserLocation().getId();


        if (dto.getDoNumber() != null && dto.getDoNumber().trim().length() != 0) {

            if (filterQuery.trim().length() != 0) {
                filterQuery = filterQuery + " AND ";
            }

            filterQuery = filterQuery + " Upper(dd.doNumber) like '" + dto.getDoNumber().trim().toUpperCase() + "%' ";
        }


        if (dto.getShipmentUid() != null && dto.getShipmentUid().trim().length() != 0) {

            if (filterQuery.trim().length() != 0) {
                filterQuery = filterQuery + " AND ";
            }
            filterQuery = filterQuery + " Upper(ssd.shipmentUid) like '" + dto.getShipmentUid().trim().toUpperCase() + "%' ";
        }


        if (dto.getShipperName() != null && dto.getShipperName().trim().length() != 0) {

            if (filterQuery.trim().length() != 0) {
                filterQuery = filterQuery + " AND ";
            }
            filterQuery = filterQuery + " Upper(dd.shipper.partyName) like '" + dto.getShipperName().trim().toUpperCase() + "%' ";
        }

        if (dto.getConsigneeName() != null && dto.getConsigneeName().trim().length() != 0) {

            if (filterQuery.trim().length() != 0) {
                filterQuery = filterQuery + " AND ";
            }
            filterQuery = filterQuery + " Upper(dd.consignee.partyName) like '" + dto.getConsigneeName().trim().toUpperCase() + "%' ";
        }


        if (dto.getOriginName() != null && dto.getOriginName().trim().length() != 0) {

            if (filterQuery.trim().length() != 0) {
                filterQuery = filterQuery + " AND ";
            }
            filterQuery = filterQuery + " Upper(ssd.origin.portName) like '" + dto.getOriginName().trim().toUpperCase() + "%' ";
        }

        if (dto.getDestinationName() != null && dto.getDestinationName().trim().length() != 0) {

            if (filterQuery.trim().length() != 0) {
                filterQuery = filterQuery + " AND ";
            }

            filterQuery = filterQuery + " Upper(ssd.destination.portName) like '" + dto.getDestinationName().trim().toUpperCase() + "%' ";
        }


        if (dto.getChargeableWeight() != null) {

            if (filterQuery.trim().length() != 0) {
                filterQuery = filterQuery + " AND ";
            }

            filterQuery = filterQuery + " dd.chargebleWeight = " + dto.getChargeableWeight();
        }


        String orderByQuery = "";

        if (dto.getSortByColumn() != null && dto.getSortByColumn().length() != 0) {
            if (dto.getSortByColumn().equalsIgnoreCase("shipmentUid")) {
                orderByQuery = orderByQuery + "ssd.shipmentUid";
            } else if (dto.getSortByColumn().equalsIgnoreCase("shipperName")) {
                orderByQuery = orderByQuery + "dd.shipper.partyName";
            } else if (dto.getSortByColumn().equalsIgnoreCase("consigneeName")) {
                orderByQuery = orderByQuery + "dd.consignee.partyName";
            } else if (dto.getSortByColumn().equalsIgnoreCase("originName")) {
                orderByQuery = orderByQuery + "ssd.origin.portName";
            } else if (dto.getSortByColumn().equalsIgnoreCase("destinationName")) {
                orderByQuery = orderByQuery + "ssd.destination.portName";
            } else if (dto.getSortByColumn().equalsIgnoreCase("doNumber")) {
                orderByQuery = orderByQuery + "dd.doNumber";
            } else if (dto.getSortByColumn().equalsIgnoreCase("chargeableWeight")) {
                orderByQuery = orderByQuery + "dd.chargebleWeight";
            }

        }

        if (orderByQuery.length() != 0) {
            orderByQuery = " ORDER BY " + orderByQuery + " " + dto.getOrderByType();
        } else {
            orderByQuery = " ORDER BY ssd.id DESC ";
        }

        searchQuery = searchQuery + filterQuery + orderByQuery;

        log.info("Search Query : " + searchQuery);

        String countQ = "Select count (dd.id) from DocumentDetail dd, ShipmentServiceDetail ssd " + standardConditionDd + " " + filterQuery;

        Query countQuery = em.createQuery(countQ);

        for (Entry<String, Object> e : params.entrySet()) {
            countQuery.setParameter(e.getKey(), e.getValue());
        }

        Long countTotal = (Long) countQuery.getSingleResult();

        Query query = em.createQuery(searchQuery);

        for (Entry<String, Object> e : params.entrySet()) {
            query.setParameter(e.getKey(), e.getValue());
        }

        query.setFirstResult(dto.getSelectedPageNumber() * dto.getRecordPerPage());
        query.setMaxResults(dto.getRecordPerPage());


        return new SearchRespDto(countTotal, query.getResultList());

    }

    @Transactional
    public void generateNumbers(GenerateNumDto generateNumDto, Consol c) {
        // TODO Auto-generated method stub
        String canNumber = null;
        String doNumber = null;

        if (generateNumDto != null) {
            if (c != null) {
                if (c.getShipmentLinkList() != null && !c.getShipmentLinkList().isEmpty()) {
                    for (ShipmentLink shipmentLink : c.getShipmentLinkList()) {
                        ShipmentServiceDetail serviceDetail = shipmentLink.getService();
                        if (serviceDetail != null) {
                            if (serviceDetail.getDocumentList() != null) {
                                Boolean isnum = false;
                                for (DocumentDetail documentDetail : serviceDetail.getDocumentList()) {
                                    if (documentDetail != null) {
                                        if (generateNumDto.getReportName().equals(ReportName.CARGO_ARRIVAL_NOTICE_CONSOL)) {
                                            if (documentDetail.getCanNumber() == null) {
                                                canNumber = sequenceGeneratorService.getSequenceValueByType(SequenceType.CAN);
                                            } else {
                                                canNumber = documentDetail.getCanNumber();
                                            }
                                            documentDetail.setCanNumber(canNumber);
                                            documentDetail.setCanIssuedBy(AuthService.getCurrentUser().getEmployee());
                                            documentDetail.setCanIssuedDate(TimeUtil.getCurrentLocationTime());
                                        } else if (generateNumDto.getReportName().equals(ReportName.MASTER_DO)) {
                                            if (!isnum && documentDetail.getDoNumber() == null) {
                                                isnum = true;
                                                doNumber = sequenceGeneratorService.getSequenceValueByType(SequenceType.DO);
                                            } else if (!isnum) {
                                                doNumber = documentDetail.getDoNumber();
                                                isnum = true;
                                            }
                                            documentDetail.setDoNumber(doNumber);
                                            documentDetail.setDoIssuedBy(AuthService.getCurrentUser().getEmployee());
                                            documentDetail.setDoIssuedDate(TimeUtil.getCurrentLocationTime());
                                        }
                                        documentDetailRepository.save(documentDetail);
                                    }
                                }
                            }
                        }

                    }
                }
            }

        }
    }
}
