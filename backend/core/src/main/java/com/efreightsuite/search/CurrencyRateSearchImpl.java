package com.efreightsuite.search;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import com.efreightsuite.dto.CurrencyRateSearchDto;
import com.efreightsuite.dto.FromToCurrency;
import com.efreightsuite.dto.SearchRespDto;
import com.efreightsuite.model.CurrencyRateMaster;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class CurrencyRateSearchImpl {

    @Autowired
    private
    EntityManager em;

    public SearchRespDto search(CurrencyRateSearchDto dto) {

        Map<String, Object> params = new HashMap<>();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

        String hql = "";

        if (dto.getSearchFromCurrencyCode() != null && dto.getSearchFromCurrencyCode().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }

            hql = hql + " Upper(fromCurrencyCode) like '" + dto.getSearchFromCurrencyCode().trim().toUpperCase() + "%' ";
        }

        if (dto.getSearchToCurrencyCode() != null && dto.getSearchToCurrencyCode().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }

            hql = hql + " Upper(toCurrencyCode) like '" + dto.getSearchToCurrencyCode().trim().toUpperCase() + "%' ";
        }

		/*if (dto.getSearchCurrencyDate() != null) {

			if (hql.trim().length() != 0) {
				hql = hql + " AND ";
			}

			hql = hql + " currencyDate = '" + new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(dto.getSearchCurrencyDate()) + "' ";
		}*/

        if (dto.getSearchSellRate() != null && dto.getSearchSellRate().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }
            hql = hql + " Upper(sellRate) like '" + dto.getSearchSellRate() + "%' ";
        }

        if (dto.getSearchBuyRate() != null && dto.getSearchBuyRate().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }
            hql = hql + " Upper(buyRate) like '" + dto.getSearchBuyRate() + "%' ";
        }

        if (dto.getSearchCurrencyDate() != null && dto.getSearchCurrencyDate().getStartDate() != null
                && dto.getSearchCurrencyDate().getEndDate() != null) {

            try {
                Date dt1 = sdf.parse(dto.getSearchCurrencyDate().getStartDate());
                Date dt2 = sdf.parse(dto.getSearchCurrencyDate().getEndDate());

                if (hql.trim().length() != 0) {
                    hql = hql + " AND ";
                }

                params.put("currencyDateStartDate", dt1);
                params.put("currencyDateEndDate", dt2);

                log.info("dt1 " + dt1 + " dt2 " + dt2);
                hql = hql
                        + " (currencyDate >= :currencyDateStartDate AND currencyDate  <= :currencyDateEndDate ) ";

            } catch (ParseException e1) {
                log.error("Date Conversion error : ", e1);
            }

        }


        if (hql.trim().length() != 0) {
            hql = " where " + hql;
        }
        hql = " from CurrencyRateMaster " + hql;


        if (dto.getSortByColumn() != null && dto.getOrderByType() != null && dto.getSortByColumn().trim().length() != 0 && dto.getOrderByType().trim().length() != 0) {
            hql = hql + " order by Upper(" + dto.getSortByColumn().trim() + ") " + dto.getOrderByType().trim();
        } else {
            hql = hql + " order by id DESC";
        }

        //hql = " from CurrencyRateMaster " + hql + " order by toCurrencyCode";

        log.info("Currecy Rate Master Search hql:[" + hql + "]");

        String countQ = "Select count (id) " + hql;

        Query countQuery = em.createQuery(countQ);

        for (Entry<String, Object> e : params.entrySet()) {
            countQuery.setParameter(e.getKey(), e.getValue());
        }

        Long countTotal = (Long) countQuery.getSingleResult();


        TypedQuery<CurrencyRateMaster> query = em.createQuery(hql, CurrencyRateMaster.class);

        for (Entry<String, Object> e : params.entrySet()) {
            query.setParameter(e.getKey(), e.getValue());
        }

        query.setFirstResult(dto.getSelectedPageNumber() * dto.getRecordPerPage());

        query.setMaxResults(dto.getRecordPerPage());

        List<CurrencyRateMaster> resultlist = query.getResultList();

        return new SearchRespDto(countTotal, resultlist);

    }

    public SearchRespDto search(Long fromCurrencyId) {

        String sql = " SELECT c.from_currency_id cfrom, c.to_currency_id cto, " +
/*						" (SELECT MAX(f.currency_date) FROM currency_rate_master f WHERE f.from_currency_id=c.from_currency_id AND f.to_currency_id=c.to_currency_id ) cdate, "+ 
*/                        " (SELECT f.buy_rate FROM efs_currency_rate_master f WHERE f.from_currency_id=c.from_currency_id AND f.to_currency_id=c.to_currency_id AND f.currency_date= (SELECT MAX(f.currency_date) FROM efs_currency_rate_master f WHERE f.from_currency_id=c.from_currency_id AND f.to_currency_id =c.to_currency_id) ) cbuy, " +
                " (SELECT f.sell_rate FROM efs_currency_rate_master f WHERE f.from_currency_id=c.from_currency_id AND f.to_currency_id=c.to_currency_id AND f.currency_date = (SELECT MAX(f.currency_date) FROM efs_currency_rate_master f WHERE f.from_currency_id=c.from_currency_id AND f.to_currency_id=c.to_currency_id) ) csell " +
                " FROM efs_currency_rate_master c WHERE c.from_currency_id=%id GROUP BY c.from_currency_id, c.to_currency_id";

        sql = sql.replaceAll("%id", fromCurrencyId + "");

        log.info("Currecy Rate Master Search sql:[" + sql + "]");

        javax.persistence.Query query = em.createNativeQuery(sql);

        List<Object[]> list = query.getResultList();

        Map<Long, FromToCurrency> map = new HashMap<>();
        if (list == null) {
            list = new ArrayList<>();
        } else {
            for (Object[] row : list) {
                FromToCurrency fc = new FromToCurrency();
                fc.setCFrom(Long.parseLong(row[0] + ""));
                fc.setCTo(Long.parseLong(row[1] + ""));
                fc.setCBuy(Double.parseDouble(row[2] + ""));
                fc.setCSell(Double.parseDouble(row[3] + ""));

                map.put(fc.getCTo(), fc);
            }
        }

        long countTotal = list.size();

        return new SearchRespDto(countTotal, map);

    }


}
