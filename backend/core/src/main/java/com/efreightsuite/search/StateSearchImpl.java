package com.efreightsuite.search;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import com.efreightsuite.dto.SearchRequest;
import com.efreightsuite.dto.SearchRespDto;
import com.efreightsuite.dto.StateSearchDto;
import com.efreightsuite.model.StateMaster;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class StateSearchImpl {

    @Autowired
    private
    EntityManager em;


    public SearchRespDto search(StateSearchDto dto) {

        String hql = "";

        if (dto.getSearchStateCode() != null && dto.getSearchStateCode().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }

            hql = hql + " Upper(stateCode) like '" + dto.getSearchStateCode().trim().toUpperCase() + "%' ";
        }

        if (dto.getSearchStateName() != null && dto.getSearchStateName().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }

            hql = hql + " Upper(stateName) like '" + dto.getSearchStateName().trim().toUpperCase() + "%' ";
        }

        if (dto.getSearchCountryName() != null && dto.getSearchCountryName().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }

            hql = hql + " countryMaster.id IN (select cm.id from CountryMaster cm where Upper(cm.countryName) like '" + dto.getSearchCountryName().trim().toUpperCase() + "%') ";
        }

        if (dto.getSearchStatus() != null && dto.getSearchStatus().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }

            hql = hql + " Upper(status) like '" + dto.getSearchStatus().trim().toUpperCase() + "%' ";
        }


        if (hql.trim().length() != 0) {
            hql = " where " + hql;
        }

        hql = " from StateMaster " + hql;


        if (dto.getSortByColumn() != null && dto.getOrderByType() != null && dto.getSortByColumn().trim().length() != 0 && dto.getOrderByType().trim().length() != 0) {
            hql = hql + " order by Upper(" + dto.getSortByColumn().trim() + ") " + dto.getOrderByType().trim();
        } else {
            hql = hql + " order by Upper(stateName) ASC";
        }

        log.info("State Master Search hql:[" + hql + "]");

        return search(dto.getSelectedPageNumber(), dto.getRecordPerPage(), hql);


    }

    public SearchRespDto search(SearchRequest dto, String countryCode) {

        String hql = " from StateMaster where status!='Hide' ";

        if (dto.getKeyword() != null && dto.getKeyword().trim().length() != 0) {

            hql = hql + " AND ( ";

            hql = hql + " UPPER(stateName) like '" + dto.getKeyword().trim().toUpperCase() + "%' ";

            hql = hql + " OR ";

            hql = hql + " UPPER(stateCode) like '" + dto.getKeyword().trim().toUpperCase() + "%' ";

            hql = hql + " ) ";
        }

        if (countryCode != null) {
            hql = hql + " AND countryMaster.countryCode = '" + countryCode + "' ";
        }

        hql = hql + " order by stateName ASC";


        log.info("State Master Search hql:[" + hql + "]");

        return search(dto.getSelectedPageNumber(), dto.getRecordPerPage(), hql);

    }

    public SearchRespDto search(SearchRequest dto, Long countryId) {

        String hql = " from StateMaster where status!='Hide' ";

        if (dto.getKeyword() != null && dto.getKeyword().trim().length() != 0) {

            hql = hql + " AND ( ";

            hql = hql + " UPPER(stateName) like '" + dto.getKeyword().trim().toUpperCase() + "%' ";

            hql = hql + " OR ";

            hql = hql + " UPPER(stateCode) like '" + dto.getKeyword().trim().toUpperCase() + "%' ";

            hql = hql + " ) ";
        }

        if (countryId != null && countryId != -1) {
            hql = hql + " AND countryMaster.id = " + countryId + " ";
        }

        hql = hql + " order by stateName ASC";


        log.info("State Master Search hql:[" + hql + "]");

        return search(dto.getSelectedPageNumber(), dto.getRecordPerPage(), hql);

    }

    private SearchRespDto search(int selectedPageNumber, int recordPerPage, String hql) {

        String countQ = "Select count (id) " + hql;

        Query countQuery = em.createQuery(countQ);
        Long countTotal = (Long) countQuery.getSingleResult();

        TypedQuery<StateMaster> query = em.createQuery(hql, StateMaster.class);

        query.setFirstResult(selectedPageNumber * recordPerPage);

        query.setMaxResults(recordPerPage);

        List<StateMaster> resultList = query.getResultList();

        log.info("Number of rows Selected : " + resultList.size());

        return new SearchRespDto(countTotal, resultList);

    }

}
