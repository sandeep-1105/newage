package com.efreightsuite.search;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.efreightsuite.activiti.ActivitiTransactionService;
import com.efreightsuite.dto.QuotationApproveDto;
import com.efreightsuite.dto.QuotationSearchDto;
import com.efreightsuite.dto.SearchRespDto;
import com.efreightsuite.enumeration.Approved;
import com.efreightsuite.enumeration.EnquiryStatus;
import com.efreightsuite.enumeration.SequenceType;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.model.DefaultMasterData;
import com.efreightsuite.model.EmployeeMaster;
import com.efreightsuite.model.EnquiryLog;
import com.efreightsuite.model.Quotation;
import com.efreightsuite.model.QuotationAttachment;
import com.efreightsuite.model.QuotationCarrier;
import com.efreightsuite.model.QuotationCharge;
import com.efreightsuite.model.QuotationContainer;
import com.efreightsuite.model.QuotationDetail;
import com.efreightsuite.model.QuotationDetailGeneralNote;
import com.efreightsuite.model.QuotationDimension;
import com.efreightsuite.model.QuotationGeneralNote;
import com.efreightsuite.model.QuotationValueAddedService;
import com.efreightsuite.model.UserProfile;
import com.efreightsuite.repository.DefaultMasterDataRepository;
import com.efreightsuite.repository.EmployeeMasterRepository;
import com.efreightsuite.repository.EnquiryLogRepository;
import com.efreightsuite.repository.QuotationAttachmentRepository;
import com.efreightsuite.repository.QuotationDetailRepository;
import com.efreightsuite.repository.QuotationRepository;
import com.efreightsuite.service.AuthService;
import com.efreightsuite.service.PartyMasterService;
import com.efreightsuite.service.SequenceGeneratorService;
import com.efreightsuite.util.AppUtil;
import com.efreightsuite.util.CommonUtils;
import com.efreightsuite.util.TimeUtil;
import com.efreightsuite.validators.QuotationValidator;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import static com.efreightsuite.constants.Constants.*;


@Service
@Log4j2
public class QuotationSearchImpl {

    @Autowired
    private
    AppUtil appUtil;

    @Autowired
    private
    EmployeeMasterRepository employeeMasterRepository;

    @Autowired
    private
    QuotationRepository quotationRepository;

    @Autowired
    private
    SequenceGeneratorService sequenceGeneratorService;

    @Autowired
    PartyMasterService partyMasterService;

    @Autowired
    private
    EnquiryLogRepository enquiryLogRepository;

    @Autowired
    private
    QuotationAttachmentRepository quotationAttachmentRepository;

    @Autowired
    private
    DefaultMasterDataRepository defaultMasterDataRepository;

    @Autowired
    private
    ActivitiTransactionService activitiTransactionService;

    @Autowired
    QuotationDetailRepository quotationDetailRepository;

    @Autowired
    private
    QuotationValidator quotationValidator;

    @Autowired
    private
    EntityManager em;


    @Transactional
    public Quotation updateQuotation(Quotation quotation, boolean isRejected, boolean isGroup) {


        quotationValidator.validate(quotation);

        checkQuotationUnique(quotation);


        if (quotation.getQuotationDetailList() != null) {

            for (QuotationDetail detail : quotation
                    .getQuotationDetailList()) {

                detail.setQuotation(quotation);

                if (detail.getQuotationValueAddedServiceList() != null) {
                    for (QuotationValueAddedService qv : detail.getQuotationValueAddedServiceList()) {
                        qv.setQuotationDetail(detail);
                    }
                } else {
                    detail.setQuotationValueAddedServiceList(new ArrayList<>());
                }

                if (detail.getQuotationCarrierList() != null) {
                    for (QuotationCarrier carrier : detail.getQuotationCarrierList()) {
                        carrier.setQuotationDetail(detail);

                        if (carrier.getQuotationChargeList() != null) {
                            for (QuotationCharge charge : carrier.getQuotationChargeList()) {
                                charge.setQuotationCarrier(carrier);
                            }
                        }

                    }
                }

                if (detail.getQuotationContainerList() != null) {
                    for (QuotationContainer container : detail
                            .getQuotationContainerList()) {
                        container.setQuotationDetail(detail);

                    }
                }

                if (detail.getQuotationDimensionList() != null) {
                    for (QuotationDimension dimension : detail
                            .getQuotationDimensionList()) {
                        dimension.setQuotationDetail(detail);

                    }
                }

                if (detail.getServiceNoteList() != null) {
                    for (QuotationDetailGeneralNote note : detail
                            .getServiceNoteList()) {
                        note.setQuotationDetail(detail);

                    }
                }
            }

        }

        if (quotation.getGeneralNoteList() != null) {
            for (QuotationGeneralNote generalNote : quotation
                    .getGeneralNoteList()) {
                generalNote.setQuotation(quotation);

            }
        }


        if (quotation.getQuotationAttachementList() != null) {
            for (QuotationAttachment quotationAttachment : quotation.getQuotationAttachementList()) {
                quotationAttachment.setQuotation(quotation);
                if (quotationAttachment.getId() != null) {

                    if (quotationAttachment.getFile() == null) {
                        QuotationAttachment attachment = quotationAttachmentRepository.getOne(quotationAttachment.getId());
                        quotationAttachment.setFile(attachment.getFile());
                    }

                }
            }
        } else {
            quotation.setQuotationAttachementList(new ArrayList<>());
        }

        Quotation savedQuotation = quotationRepository.saveAndFlush(quotation);

        if (isRejected && !isGroup) {
            lostQuotationApprove(quotation.getId());
        }
        log.info("existingquotation updated successfully...");
        return savedQuotation;

    }


    private void lostQuotationApprove(Long id) {

        QuotationApproveDto dto = new QuotationApproveDto();
        dto.setQuotationId(id);
        dto.setApproved(Approved.Approved);
        dto.setEmployeeId(AuthService.getCurrentUser().getEmployee().getId());
        dto.setApprovedDate(TimeUtil.getCurrentLocationTime());

        updateQuotationStatus(dto);
    }


    private void updateQuotationStatus(QuotationApproveDto dto) {


        Quotation quotation = quotationRepository.findById(dto.getQuotationId());
        EmployeeMaster employee = employeeMasterRepository.findById(dto.getEmployeeId());

        quotation.setApproved(dto.getApproved());
        quotation.setApprovedBy(employee);
        quotation.setApprovedDate(dto.getApprovedDate());

        String key = appUtil.getLocationConfig("quotation.client.approval.required", quotation.getLocationMaster(), true);

        if (quotation.getApproved() == Approved.Approved
                && _FALSE.equals(key)) {
            quotation.setApproved(Approved.Gained);
            quotation.setCustomerApprovedDate(TimeUtil.getCurrentLocationTime(quotation.getLocationMaster()));
        }

        quotation = quotationRepository.save(quotation);

        if (quotation.getApproved() == Approved.Rejected) {
            activitiTransactionService.cancelQuotation(quotation);
        }


    }


    @Transactional
    public Quotation createQuotation(Quotation quotation) {


        quotation.setQuotationNo(sequenceGeneratorService.getSequenceValueByType(SequenceType.QUOTATION));

        //Auto Approval for Manager - Start
        String manager = appUtil.getLocationConfig("quotation.auto.approval.required", quotation.getLocationMaster(), true);
        if (_TRUE.equals(manager)) {
            quotation.setApproved(Approved.Approved);
            quotation.setApprovedDate(TimeUtil.getCurrentLocationTime());
            quotation.setApprovedBy(AuthService.getCurrentUser().getEmployee());

            String client = appUtil.getLocationConfig("quotation.client.approval.required", quotation.getLocationMaster(), true);
            if (_FALSE.equals(client)) {
                quotation.setApproved(Approved.Approved);
            }
        }
        //Auto Approval for Manager - End

        Quotation quot = quotationRepository.save(quotation);

        if (quot.getEnquiryNo() != null) {
            EnquiryLog enquiryLog = enquiryLogRepository.findByEnquiryNo(quot.getEnquiryNo());
            enquiryLog.setQuotationNo(quot.getQuotationNo());
            enquiryLog.setStatus(EnquiryStatus.Gained);
            enquiryLogRepository.save(enquiryLog);
        }

        return activitiTransactionService.createQuotationTask(quot);

    }

    public SearchRespDto search(QuotationSearchDto dto) {

        Map<String, Object> params = new HashMap<>();

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:SS");

        String searchQuery = "SELECT NEW com.efreightsuite.dto.QuotationSearchResponseDto ("
                + "quotation.id,"
                + "service.serviceCode,"
                + "service.serviceName, "
                + "cust.partyName, "
                + "cg.colorCode, "
                + "quotation.quotationNo, "
                + "ori.portCode, "
                + "ori.portName, "
                + "dest.portCode, "
                + "dest.portName, "
                + "quotation.validFrom, "
                + "quotation.validTo, "
                + "qd.refNo, "
                + "quotation.bookingNo, "
                + "shipper.partyName, "
                + "sg.colorCode, "
                + "coor.employeeName,  "
                + "salesman.employeeName, "
                + "quotation.whoRouted, "
                + "agent.partyName, "
                + "quotation.loggedOn,"
                + "loggedBy.employeeName, "
                + "quotation.approved )"
                + " FROM QuotationDetail qd";

        String countQuery = "select count(qd.quotation.id) FROM QuotationDetail qd";

        String joinQuery = " LEFT JOIN qd.quotation quotation" + " LEFT JOIN qd.serviceMaster service"
                + " LEFT JOIN quotation.customer cust" + " LEFT JOIN cust.gradeMaster cg" + " LEFT JOIN qd.origin ori"
                + " LEFT JOIN qd.destination dest" + " LEFT JOIN quotation.shipper shipper"
                + " LEFT JOIN shipper.gradeMaster sg" + " LEFT JOIN quotation.salesCoordinator coor"
                + " LEFT JOIN quotation.salesman salesman" + " LEFT JOIN quotation.agent agent"
                + " LEFT JOIN quotation.loggedBy loggedBy"
                + " LEFT JOIN quotation.locationMaster loc"
                + " LEFT JOIN quotation.countryMaster country"
                + " LEFT JOIN loc.divisionMaster division";

        searchQuery = searchQuery + joinQuery;
        countQuery = countQuery + joinQuery;

        String filterQuery = StringUtils.EMPTY;

        UserProfile userProfile = AuthService.getCurrentUser();

        Set<Long> locationList = new HashSet<>();

        if (userProfile.getSelectedUserLocation() != null) {
            locationList.add(userProfile.getSelectedUserLocation().getId());
        }
        if (userProfile.getRecordAccessLevelProcessDTO() != null) {

            //Location List
            if (userProfile.getRecordAccessLevelProcessDTO().getLocationList() != null
                    && !userProfile.getRecordAccessLevelProcessDTO().getLocationList().isEmpty()) {
                locationList.addAll(userProfile.getRecordAccessLevelProcessDTO().getLocationList());
            }

            //Country List
            if (userProfile.getRecordAccessLevelProcessDTO().getCountryList() != null
                    && !userProfile.getRecordAccessLevelProcessDTO().getCountryList().isEmpty()) {


                String ids = CommonUtils.commaSeparatedIds(userProfile.getRecordAccessLevelProcessDTO().getCountryList());
                if (ids.trim().length() > 0) {
                    if (filterQuery.trim().length() != 0) {
                        filterQuery = filterQuery + AND;
                    }
                    filterQuery = filterQuery + " country.id   IN (" + ids + ")";
                }
            }


            // Party List
            if (userProfile.getRecordAccessLevelProcessDTO().getCustomerList() != null
                    && !userProfile.getRecordAccessLevelProcessDTO().getCustomerList().isEmpty()) {
                String ids = CommonUtils.commaSeparatedIds(userProfile.getRecordAccessLevelProcessDTO().getCustomerList());
                if (ids.trim().length() > 0) {
                    if (filterQuery.trim().length() != 0) {
                        filterQuery = filterQuery + AND;
                    }
                    filterQuery = filterQuery + " cust.id   IN (" + ids + ")";
                }
            }
            // Salesman List
            if (userProfile.getRecordAccessLevelProcessDTO().getSalesmanList() != null
                    && !userProfile.getRecordAccessLevelProcessDTO().getSalesmanList().isEmpty()) {
                String ids = CommonUtils.commaSeparatedIds(userProfile.getRecordAccessLevelProcessDTO().getSalesmanList());
                if (ids.trim().length() > 0) {
                    if (filterQuery.trim().length() != 0) {
                        filterQuery = filterQuery + AND;
                    }
                    filterQuery = filterQuery + " salesman.id   IN (" + ids + ")";
                }
            }

            // Service List
            if (userProfile.getRecordAccessLevelProcessDTO().getServiceList() != null
                    && !userProfile.getRecordAccessLevelProcessDTO().getServiceList().isEmpty()) {
                String ids = CommonUtils.commaSeparatedIds(userProfile.getRecordAccessLevelProcessDTO().getServiceList());
                if (ids.trim().length() > 0) {
                    if (filterQuery.trim().length() != 0) {
                        filterQuery = filterQuery + AND;
                    }
                    filterQuery = filterQuery + " service.id   IN (" + ids + ")";
                }
            }
            // Division List
            if (userProfile.getRecordAccessLevelProcessDTO().getDivisionList() != null
                    && !userProfile.getRecordAccessLevelProcessDTO().getDivisionList().isEmpty()) {
                String ids = CommonUtils.commaSeparatedIds(userProfile.getRecordAccessLevelProcessDTO().getDivisionList());
                if (ids.trim().length() > 0) {
                    if (filterQuery.trim().length() != 0) {
                        filterQuery = filterQuery + AND;
                    }
                    filterQuery = filterQuery + " division.id   IN (" + ids + ")";
                }
            }
        }


        if (locationList != null && !locationList.isEmpty()) {
            String ids = CommonUtils.commaSeparatedIds(locationList);
            if (ids.trim().length() > 0) {
                if (filterQuery.trim().length() != 0) {
                    filterQuery = filterQuery + AND;
                }
                filterQuery = filterQuery + " loc.id   IN (" + ids + ")";
            }
        }


        if (dto.getSearchServiceName() != null && dto.getSearchServiceName().length() != 0) {

            if (filterQuery.trim().length() != 0) {
                filterQuery = filterQuery + AND;
            }

            filterQuery += " Upper(service.serviceName) like '" + dto.getSearchServiceName().trim().toUpperCase() + "%' ";
        }

        if (dto.getSearchCustomerName() != null && dto.getSearchCustomerName().length() != 0) {

            if (filterQuery.trim().length() != 0) {
                filterQuery = filterQuery + AND;
            }

            filterQuery += " Upper(cust.partyName) like '" + dto.getSearchCustomerName().trim().toUpperCase() + "%' ";
        }

        if (dto.getSearchCustomerName() != null && dto.getSearchCustomerName().length() != 0) {

            if (filterQuery.trim().length() != 0) {
                filterQuery = filterQuery + AND;
            }

            filterQuery += " Upper(cust.partyName) like '" + dto.getSearchCustomerName().trim().toUpperCase() + "%' ";
        }

        if (dto.getSearchQuotationNo() != null && dto.getSearchQuotationNo().length() != 0) {

            if (filterQuery.trim().length() != 0) {
                filterQuery = filterQuery + AND;
            }

            filterQuery += " Upper(quotation.quotationNo) like '" + dto.getSearchQuotationNo().trim().toUpperCase()
                    + "%' ";
        }

        if (dto.getSearchPortOrigin() != null && dto.getSearchPortOrigin().length() != 0) {

            if (filterQuery.trim().length() != 0) {
                filterQuery = filterQuery + AND;
            }

            filterQuery += " Upper(ori.portName) like '" + dto.getSearchPortOrigin().trim().toUpperCase() + "%' ";
        }

        if (dto.getSearchPortDestination() != null && dto.getSearchPortDestination().length() != 0) {

            if (filterQuery.trim().length() != 0) {
                filterQuery = filterQuery + AND;
            }

            filterQuery += " Upper(dest.portName) like '" + dto.getSearchPortDestination().trim().toUpperCase() + "%' ";
        }


        if (dto.getSearchValidFrom() != null && dto.getSearchValidFrom().getStartDate() != null
                && dto.getSearchValidFrom().getEndDate() != null) {

            try {

                Date date1 = sdf.parse(dto.getSearchValidFrom().getStartDate());
                Date date2 = sdf.parse(dto.getSearchValidFrom().getEndDate());

                if (filterQuery.trim().length() != 0) {
                    filterQuery = filterQuery + AND;
                }

                params.put("startDate", date1);
                params.put("endDate", date2);

                filterQuery = filterQuery
                        + "( quotation.validFrom  >= :startDate AND quotation.validFrom  <= :endDate )";

            } catch (ParseException exception) {
                log.error("Date Conversion Exception in  validFrom....", exception);
            }

        }

        if (dto.getSearchValidTo() != null && dto.getSearchValidTo().getStartDate() != null
                && dto.getSearchValidTo().getEndDate() != null) {

            try {

                Date date1 = sdf.parse(dto.getSearchValidTo().getStartDate());
                Date date2 = sdf.parse(dto.getSearchValidTo().getEndDate());

                if (filterQuery.trim().length() != 0) {
                    filterQuery = filterQuery + AND;
                }

                params.put("startDate1", date1);
                params.put("endDate1", date2);

                filterQuery = filterQuery + "( quotation.validTo  >= :startDate1 AND quotation.validTo  <= :endDate1 )";

            } catch (ParseException exception) {
                log.error("Date Conversion Exception in validFrom....", exception);
            }
        }

        String advancedFilterQuery = "";

        if (dto.getSearchValue() != null && dto.getSearchValue().trim().length() != 0) {

            if (dto.getSearchName() != null && dto.getSearchName().trim().equals("Reference No")) {
                advancedFilterQuery = AND + "Upper(qd.refNo) like '" + dto.getSearchValue().trim().toUpperCase() + "%' ";

                if (dto.getSearchRefNo() != null && dto.getSearchRefNo().length() != 0) {

                    if (filterQuery.trim().length() != 0) {
                        filterQuery = filterQuery + AND;
                    }

                    filterQuery += " Upper(qd.refNo) like '" + dto.getSearchRefNo().trim().toUpperCase() + "%' ";
                }
            } else if (dto.getSearchName() != null && dto.getSearchName().trim().equals("Shipment ID")) {
                advancedFilterQuery = AND + "Upper(quotation.bookingNo) like '" + dto.getSearchValue().trim().toUpperCase() + "%' ";

                if (dto.getShipmentId() != null && dto.getShipmentId().length() != 0) {

                    if (filterQuery.trim().length() != 0) {
                        filterQuery = filterQuery + AND;
                    }

                    filterQuery += " Upper(quotation.bookingNo) like '" + dto.getShipmentId().trim().toUpperCase() + "%' ";
                }
            } else if (dto.getSearchName() != null && dto.getSearchName().trim().equals("Sales Co-ordinator")) {
                advancedFilterQuery = AND + "Upper(coor.employeeName) like '" + dto.getSearchValue().trim().toUpperCase() + "%' ";

                if (dto.getSearchSalesCordinator() != null && dto.getSearchSalesCordinator().length() != 0) {

                    if (filterQuery.trim().length() != 0) {
                        filterQuery = filterQuery + AND;
                    }

                    filterQuery += " Upper(coor.employeeName) like '" + dto.getSearchSalesCordinator().trim().toUpperCase() + "%' ";
                }
            } else if (dto.getSearchName() != null && dto.getSearchName().trim().equals("Salesman")) {
                advancedFilterQuery = AND + "Upper(salesman.employeeName) like '" + dto.getSearchValue().trim().toUpperCase() + "%' ";

                if (dto.getSearchSalesman() != null && dto.getSearchSalesman().length() != 0) {

                    if (filterQuery.trim().length() != 0) {
                        filterQuery = filterQuery + AND;
                    }

                    filterQuery += " Upper(salesman.employeeName) like '" + dto.getSearchSalesman().trim().toUpperCase() + "%' ";
                }
            } else {
                advancedFilterQuery = AND + "Upper(loggedBy.employeeName) like '" + dto.getSearchValue().trim().toUpperCase() + "%' ";

                if (dto.getSearchLoggedBy() != null && dto.getSearchLoggedBy().length() != 0) {

                    if (filterQuery.trim().length() != 0) {
                        filterQuery = filterQuery + AND;
                    }

                    filterQuery += " Upper(loggedBy.employeeName) like '" + dto.getSearchLoggedBy().trim().toUpperCase() + "%' ";
                }

            }


            if (advancedFilterQuery.trim().length() != 0) {
                filterQuery = filterQuery + advancedFilterQuery;
            }

        }


        if (dto.getSearchQuotationNo() != null && dto.getSearchQuotationNo().length() != 0) {

            if (filterQuery.trim().length() != 0) {
                filterQuery = filterQuery + AND;
            }

            filterQuery += " Upper(quotation.quotationNo) like '" + dto.getSearchQuotationNo().trim().toUpperCase() + "%' ";
        }


        if (dto.getSearchShipper() != null && dto.getSearchShipper().length() != 0) {

            if (filterQuery.trim().length() != 0) {
                filterQuery = filterQuery + AND;
            }

            filterQuery += " Upper(shipper.partyName) like '" + dto.getSearchShipper().trim().toUpperCase() + "%' ";
        }

        if (dto.getSearchLoggedOn() != null && dto.getSearchLoggedOn().getStartDate() != null
                && dto.getSearchLoggedOn().getEndDate() != null) {

            try {

                Date date1 = sdf.parse(dto.getSearchLoggedOn().getStartDate());
                Date date2 = sdf.parse(dto.getSearchLoggedOn().getEndDate());

                if (filterQuery.trim().length() != 0) {
                    filterQuery = filterQuery + AND;
                }

                params.put("startDate2", date1);
                params.put("endDate2", date2);

                filterQuery = filterQuery
                        + "( quotation.loggedOn  >= :startDate2 AND quotation.loggedOn  <= :endDate2 )";

            } catch (ParseException exception) {
                log.error("Date Conversion Exception in loggedOn....", exception);
            }
        }


        if (dto.getStatus() != null && dto.getStatus().trim().length() != 0 && !dto.getStatus().trim().equals("All")) {


            DefaultMasterData clientApproved = defaultMasterDataRepository.findByCodeAndLocationId("quotation.client.approval.required", AuthService.getCurrentUser().getSelectedUserLocation().getId());
            //DefaultMasterData autoApproved = defaultMasterDataRepository.findByCodeAndLocationId("quotation.auto.approval.required", AuthService.getCurrentUser().getSelectedUserLocation().getId());

            String statusQuery = "";

            if (dto.getStatus().equals(Approved.Pending.toString())) {
                statusQuery = QUOTATION_APPROVED_IN_WITH_OPEN_BRACET + Approved.Pending.toString() + CLOSING_BRACKET_WITH_INVERTED_COMMA;
            } else if (dto.getStatus().equals(Approved.Rejected.toString())) {
                statusQuery = QUOTATION_APPROVED_IN_WITH_OPEN_BRACET + Approved.Rejected.toString() + CLOSING_BRACKET_WITH_INVERTED_COMMA;
            } else if (_TRUE.equals(clientApproved.getValue().toUpperCase()) && Approved.Approved.toString().equals(dto.getStatus())) {
                statusQuery = QUOTATION_APPROVED_IN_WITH_OPEN_BRACET + Approved.Approved.toString() + "')";
            } else if (_FALSE.equals(clientApproved.getValue().toUpperCase()) && Approved.Approved.toString().equals(dto.getStatus())) {
                return new SearchRespDto(0L, null);
            } else if (Approved.ClientApproved.toString().equals(dto.getStatus())) {

                if (_TRUE.equals(clientApproved.getValue().toUpperCase())) {
                    statusQuery = QUOTATION_APPROVED_IN_WITH_OPEN_BRACET + Approved.ClientApproved.toString() + CLOSING_BRACKET_WITH_INVERTED_COMMA;
                } else {
                    statusQuery = QUOTATION_APPROVED_IN_WITH_OPEN_BRACET + Approved.Approved.toString() + CLOSING_BRACKET_WITH_INVERTED_COMMA;
                }
            }

            if (statusQuery.trim().length() != 0) {
                if (filterQuery.trim().length() != 0) {
                    filterQuery = filterQuery + AND + statusQuery;
                }
            }

        }

        if (filterQuery.trim().length() != 0) {
            filterQuery = " WHERE " + filterQuery;
        }

        countQuery = countQuery + filterQuery;
        Query countQ = em.createQuery(countQuery);

        log.info("HQL Count Query For Search : " + countQuery);
        log.info("HQL params For Search : " + params);
        for (Entry<String, Object> e : params.entrySet()) {
            countQ.setParameter(e.getKey(), e.getValue());
        }
        Long countTotal = (Long) countQ.getSingleResult();
        log.info("Total Number of Records Found : " + countTotal);

        String orderByQuery = StringUtils.EMPTY;

        if (dto.getSortByColumn() != null && dto.getSortByColumn().length() != 0) {

            if (SERVICE_NAME.equalsIgnoreCase(dto.getSortByColumn())) {
                orderByQuery = orderByQuery + "service.serviceName";
            } else if (CUSTOMER_NAME.equalsIgnoreCase(dto.getSortByColumn())) {
                orderByQuery = orderByQuery + "cust.partyName";
            } else if (QUATATION_NO.equalsIgnoreCase(dto.getSortByColumn())) {
                orderByQuery = orderByQuery + "quotation.quotationNo";
            } else if (ORIGIN_NAME.equalsIgnoreCase(dto.getSortByColumn())) {
                orderByQuery = orderByQuery + "ori.portName";
            } else if (DESTINATION_NAME.equalsIgnoreCase(dto.getSortByColumn())) {
                orderByQuery = orderByQuery + "dest.portName";
            } else if (VALID_FROM.equalsIgnoreCase(dto.getSortByColumn())) {
                orderByQuery = orderByQuery + "quotation.validFrom";
            } else if (EXPIRES_ON.equalsIgnoreCase(dto.getSortByColumn())) {
                orderByQuery = orderByQuery + "quotation.validTo";
            } else if (REFERENCE_NUMBER.equalsIgnoreCase(dto.getSortByColumn())) {
                orderByQuery = orderByQuery + "qd.refNo";
            } else if (SHIPMENT_UID.equalsIgnoreCase(dto.getSortByColumn())) {
                orderByQuery = orderByQuery + "quotation.bookingNo";
            } else if (SHIPPER_NAME.equalsIgnoreCase(dto.getSortByColumn())) {
                orderByQuery = orderByQuery + "shipper.partyName";
            } else if (SALES_COORDINATOR.equalsIgnoreCase(dto.getSortByColumn())) {
                orderByQuery = orderByQuery + "coor.employeeName";
            } else if (SALES_MAN.equalsIgnoreCase(dto.getSortByColumn())) {
                orderByQuery = orderByQuery + "salesman.employeeName";
            } else if (CREATED_ON.equalsIgnoreCase(dto.getSortByColumn())) {
                orderByQuery = orderByQuery + "quotation.loggedOn";
            } else if (CREATED_BY.equalsIgnoreCase(dto.getSortByColumn())) {
                orderByQuery = orderByQuery + "loggedBy.employeeName";
            }


        }

        if (orderByQuery.trim().length() != 0) {
            orderByQuery = ORDER_BY + orderByQuery + StringUtils.SPACE + dto.getOrderByType();
        } else {
            orderByQuery = ORDER_BY + "quotation.id DESC ";
        }

        searchQuery = searchQuery + filterQuery + orderByQuery;
        log.info("HQL Query For Search : " + searchQuery);
        Query query = em.createQuery(searchQuery);

        if(dto.getSelectedPageNumber()>=0 && dto.getRecordPerPage()>=0 ) {
            query.setFirstResult(dto.getSelectedPageNumber() * dto.getRecordPerPage());
            query.setMaxResults(dto.getRecordPerPage());
        }

        for (Entry<String, Object> e : params.entrySet()) {
            query.setParameter(e.getKey(), e.getValue());
        }

        return new SearchRespDto(countTotal, query.getResultList());

    }

    public void checkQuotationUnique(Quotation q) {

        log.info("unique method called");


        String hql = " from QuotationDetail where ";

        hql = hql + " quotation.customer.id = :customer ";

        hql = hql + AND + "quotation.validFrom = :validFrom" + AND + "quotation.validTo = :validTo  ";

        if (q.getId() != null) {

            hql = hql + AND + "quotation.id!=" + q.getId();

        }


        hql = hql + AND + "origin.id = :origin";

        hql = hql + AND + "destination.id = :destination";

        hql = hql + AND + "quotation.approved !=:approved ";


        Query query = em.createQuery(hql);

        for (QuotationDetail quotationDetail : q.getQuotationDetailList()) {

            if (null != q.getCustomer())
                query.setParameter(CUSTOMER, q.getCustomer().getId());
            query.setParameter(VALID_FROM, q.getValidFrom());
            query.setParameter(VALID_TO, q.getValidTo());
            if (null != quotationDetail.getOrigin())
                query.setParameter(ORIGIN, quotationDetail.getOrigin().getId());
            if (null != quotationDetail.getDestination())
                query.setParameter(DESTINATION, quotationDetail.getDestination().getId());
            query.setParameter(APPROVED, Approved.Rejected);
            List<?> resultSet = query.getResultList();


            long duplicateCount = resultSet.size();

            if (duplicateCount > 0) {
                throw new RestException(ErrorCode.QUOTATION_ALREADY_EXISTS);
            }
        }

    }
}
