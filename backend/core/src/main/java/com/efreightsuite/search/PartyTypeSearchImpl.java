package com.efreightsuite.search;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import com.efreightsuite.dto.PartyTypeSearchDto;
import com.efreightsuite.dto.SearchRequest;
import com.efreightsuite.dto.SearchRespDto;
import com.efreightsuite.model.PartyTypeMaster;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class PartyTypeSearchImpl {

    private static final String AND = " AND ";
    @Autowired
    private
    EntityManager em;

    public SearchRespDto search(SearchRequest dto) {

        String hql = " from PartyTypeMaster where status!='Hide' ";

        if (dto.getKeyword() != null && dto.getKeyword().trim().length() != 0) {


            hql = hql + " AND (";

            hql = hql + " Upper(partyTypeCode) like '" + dto.getKeyword().trim().toUpperCase() + "%' ";

            hql = hql + " OR ";

            hql = hql + " Upper(partyTypeName) like '" + dto.getKeyword().trim().toUpperCase() + "%' ";

            hql = hql + " ) ";

        }


        hql = hql + " order by partyTypeName ASC";

        log.info("partyTypeName Search hql:[" + hql + "]");

        return search(dto.getSelectedPageNumber(), dto.getRecordPerPage(), hql);

    }


    public SearchRespDto search(PartyTypeSearchDto dto) {


        String hql = "";


        if (StringUtils.isNotEmpty(dto.getSearchPartyTypeName())) {

            if (hql.trim().length() != 0) {
                hql = hql + AND;
            }

            hql = hql + " Upper(partyTypeName) like '" + dto.getSearchPartyTypeName().trim().toUpperCase() + "%' ";
        }

        if (StringUtils.isNotEmpty(dto.getSearchPartyTypeCode())) {

            if (hql.trim().length() != 0) {
                hql = hql + AND;
            }

            hql = hql + " Upper(partyTypeCode) like '" + dto.getSearchPartyTypeCode().trim().toUpperCase() + "%' ";
        }

        if (StringUtils.isNotEmpty(dto.getSearchStatus())) {

            if (hql.trim().length() != 0) {
                hql = hql + AND;
            }

            hql = hql + " Upper(status) like '" + dto.getSearchStatus().trim().toUpperCase() + "%' ";
        }

        if (hql.trim().length() != 0) {
            hql = " where " + hql;
        }

        hql = " from PartyTypeMaster " + hql;


        if (StringUtils.isNotEmpty(dto.getSortByColumn()) && StringUtils.isNotEmpty(dto.getOrderByType())) {
            hql = hql + " order by Upper(" + dto.getSortByColumn().trim() + ") " + dto.getOrderByType().trim();
        } else {
            hql = hql + " order by Upper(partyTypeName) ASC";
        }


        log.info("PartyTypeMaster  Search hql:[" + hql + "]");


        return search(dto.getSelectedPageNumber(), dto.getRecordPerPage(), hql);
    }


    private SearchRespDto search(int selectedPageNumber, int recordPerPage, String hql) {

        String countQ = "Select count (id) " + hql;

        Query countQuery = em.createQuery(countQ);

        Long countTotal = (Long) countQuery.getSingleResult();


        TypedQuery<PartyTypeMaster> query = em.createQuery(hql, PartyTypeMaster.class);

        query.setFirstResult(selectedPageNumber * recordPerPage);

        query.setMaxResults(recordPerPage);

        List<PartyTypeMaster> resultlist = query.getResultList();

        return new SearchRespDto(countTotal, resultlist);
    }


}
