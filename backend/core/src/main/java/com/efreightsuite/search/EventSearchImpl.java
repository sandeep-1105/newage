package com.efreightsuite.search;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import com.efreightsuite.dto.EventSearchDto;
import com.efreightsuite.dto.SearchRequest;
import com.efreightsuite.dto.SearchRespDto;
import com.efreightsuite.model.EventMaster;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class EventSearchImpl {

    @Autowired
    private
    EntityManager em;

    private static String commaSeparatedIds(String ids[]) {
        return StringUtils.join(ids, ",");
    }

    public SearchRespDto search(EventSearchDto dto) {

        String hql = "";

        if (dto.getSearchCode() != null && dto.getSearchCode().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }

            hql = hql + " Upper(eventCode) like '" + dto.getSearchCode().trim().toUpperCase() + "%' ";
        }

        if (dto.getSearchName() != null && dto.getSearchName().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }

            hql = hql + " Upper(eventName) like '" + dto.getSearchName().trim().toUpperCase() + "%' ";
        }

        if (dto.getSearchType() != null && dto.getSearchType().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }
            hql = hql + " Upper(eventMasterType) like '" + dto.getSearchType().trim().toUpperCase() + "%' ";
        }

        if (dto.getSearchCountryName() != null && dto.getSearchCountryName().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }
            hql = hql + " countryMaster.id IN (select cm.id from CountryMaster cm where Upper(cm.countryName) like '" + dto.getSearchCountryName().trim().toUpperCase() + "%') ";
        }


        if (dto.getSearchStatus() != null && dto.getSearchStatus().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }

            hql = hql + " Upper(status) like '" + dto.getSearchStatus().trim().toUpperCase() + "%' ";
        }

        if (hql.trim().length() != 0) {
            hql = " where " + hql;
        }

        hql = " from EventMaster " + hql;

        if (dto.getSortByColumn() != null && dto.getOrderByType() != null && dto.getSortByColumn().trim().length() != 0 && dto.getOrderByType().trim().length() != 0) {
            hql = hql + " order by " + dto.getSortByColumn().trim() + " " + dto.getOrderByType().trim();
        } else {
            hql = hql + " order by eventName ASC";
        }


        //hql = " from TosMaster " + hql + " order by tosName";

        log.info("Event Master Search hql:[" + hql + "]");

        return search(dto.getSelectedPageNumber(), dto.getRecordPerPage(), hql);

    }

    public SearchRespDto searchEventNotInList(SearchRequest dto) {

        String searchQuery = "SELECT NEW com.efreightsuite.dto.EventMasterSearchResponseDto (" +
                "  event.id,  event.eventCode,  event.eventName, event.eventMasterType , event.status)  FROM EventMaster event ";

        String countQuery = "SELECT COUNT(event.id) FROM EventMaster event ";

        String filterQuery = "";

        if (dto.getIds() != null && dto.getIds().length > 0) {
            filterQuery = " event.id NOT IN (" + commaSeparatedIds(dto.getIds()) + ")  ";
        }

        if (dto.getKeyword() != null && dto.getKeyword().trim().length() != 0) {

            if (filterQuery.trim().length() != 0) {
                filterQuery += " AND ";
            }

            filterQuery = filterQuery + " (UPPER(event.eventCode) LIKE '" + dto.getKeyword().trim().toUpperCase() + "%' OR  UPPER(event.eventName) LIKE '" +
                    dto.getKeyword().trim().toUpperCase() + "%' ) AND event.status != 'Hide' ";

        }

        if (filterQuery.trim().length() != 0) {
            filterQuery = " WHERE " + filterQuery;
        }

        searchQuery = searchQuery + filterQuery + " ORDER BY event.eventName ASC";

        countQuery = countQuery + filterQuery;

        log.info("Count Query : " + countQuery);

        Query countQ = em.createQuery(countQuery);

        Long countTotal = (Long) countQ.getSingleResult();

        log.info("Count Query Result : " + countTotal);

        Query query = em.createQuery(searchQuery);

        log.info("Search Query : " + query);

        query.setFirstResult(dto.getSelectedPageNumber() * dto.getRecordPerPage());

        query.setMaxResults(dto.getRecordPerPage());

        return new SearchRespDto(countTotal, query.getResultList());
    }


    public SearchRespDto search(SearchRequest dto) {

        String hql = " from EventMaster where status!='Hide' ";

        if (dto.getKeyword() != null && dto.getKeyword().trim().length() != 0) {

            hql = hql + " AND ( ";

            hql = hql + " UPPER(eventCode) like '" + dto.getKeyword().trim().toUpperCase() + "%' ";

            hql = hql + " OR ";

            hql = hql + " UPPER(eventName) like '" + dto.getKeyword().trim().toUpperCase() + "%' ";

            hql = hql + " ) ";
        }

        hql = hql + " order by eventName ASC";

        log.info("Event Master Search hql:[" + hql + "]");

        return search(dto.getSelectedPageNumber(), dto.getRecordPerPage(), hql);
    }

    private SearchRespDto search(int selectedPageNumber, int recordPerPage, String hql) {

        String countQ = "Select count (id) " + hql;

        Query countQuery = em.createQuery(countQ);

        Long countTotal = (Long) countQuery.getSingleResult();

        TypedQuery<EventMaster> query = em.createQuery(hql, EventMaster.class);

        query.setFirstResult(selectedPageNumber * recordPerPage);

        query.setMaxResults(recordPerPage);

        List<EventMaster> resultlist = query.getResultList();

        return new SearchRespDto(countTotal, resultlist);

    }
}
