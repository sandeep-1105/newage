package com.efreightsuite.search;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import com.efreightsuite.dto.EdiConfigrationSearchDto;
import com.efreightsuite.dto.SearchRespDto;
import com.efreightsuite.model.EdiConfigurationMaster;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class EdiConfigurationSearchImpl {

    @Autowired
    private
    EntityManager em;

    public SearchRespDto search(EdiConfigrationSearchDto dto, Long locationId) {

        String hql = "";

        if (dto.getSearchEdiKey() != null && dto.getSearchEdiKey().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }

            hql = hql + " Upper(ediConfigurationKey) like '" + dto.getSearchEdiKey().trim().toUpperCase() + "%' ";
        }

        if (dto.getSearchEdiValue() != null && dto.getSearchEdiValue().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }

            hql = hql + " Upper(ediConfigurationValue) like '" + dto.getSearchEdiValue().trim().toUpperCase() + "%' ";
        }

        if (dto.getSearchDescription() != null && dto.getSearchDescription().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }

            hql = hql + " Upper(description) like '" + dto.getSearchDescription().trim().toUpperCase() + "%' ";
        }

		/*if((dto.getSearchEdiKey()!=null &&  dto.getSearchEdiKey().trim().length()!=0)|| (dto.getSearchEdiValue()!=null &&  dto.getSearchEdiValue().trim().length()!=0)|| (dto.getSearchDescription()!=null && dto.getSearchDescription().trim().length()!=0)){
			hql = " where locationMaster.id="+locationId+" AND"+hql;
		}else{
			hql = " where locationMaster.id="+locationId;
		}*/

        hql = " from EdiConfigurationMaster " + hql;

        if (dto.getSortByColumn() != null && dto.getOrderByType() != null && dto.getSortByColumn().trim().length() != 0 && dto.getOrderByType().trim().length() != 0) {
            hql = hql + " order by Upper(" + dto.getSortByColumn().trim() + ") " + dto.getOrderByType().trim();
        } else {
            hql = hql + " order by Upper(ediConfigurationKey) ASC";
        }

        log.info("EDI Search hql:[" + hql + "]");

        return search(dto.getSelectedPageNumber(), dto.getRecordPerPage(), hql);

    }

    private SearchRespDto search(int selectedPageNumber, int recordPerPage, String hql) {

        String countQ = "Select count (id) " + hql;

        Query countQuery = em.createQuery(countQ);

        Long countTotal = (Long) countQuery.getSingleResult();

        TypedQuery<EdiConfigurationMaster> query = em.createQuery(hql, EdiConfigurationMaster.class);

        query.setFirstResult(selectedPageNumber * recordPerPage);

        query.setMaxResults(recordPerPage);

        List<EdiConfigurationMaster> resultlist = query.getResultList();

        SearchRespDto searchRespDto = new SearchRespDto(countTotal, resultlist);

        log.info(resultlist.size() + " number of rows selected...");

        return searchRespDto;
    }

}
