package com.efreightsuite.search;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import com.efreightsuite.dto.SearchRequest;
import com.efreightsuite.dto.SearchRespDto;
import com.efreightsuite.dto.ServiceSearchDto;
import com.efreightsuite.model.ServiceMaster;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class ServiceSearchImpl {

    @Autowired
    private
    EntityManager em;

    public SearchRespDto search(ServiceSearchDto dto) {

        String hql = "";

        if (dto.getSearchServiceCode() != null && dto.getSearchServiceCode().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }

            hql = hql + " Upper(serviceCode) like '" + dto.getSearchServiceCode().trim().toUpperCase() + "%' ";
        }

        if (dto.getSearchServiceName() != null && dto.getSearchServiceName().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }

            hql = hql + " Upper(serviceName) like '" + dto.getSearchServiceName().trim().toUpperCase() + "%' ";
        }

        if (dto.getSearchTransportMode() != null && dto.getSearchTransportMode().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }

            hql = hql + " Upper(transportMode) like '" + dto.getSearchTransportMode().trim().toUpperCase() + "%' ";
        }

        if (dto.getSearchImportExport() != null && dto.getSearchImportExport().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }

            hql = hql + " Upper(importExport) like '" + dto.getSearchImportExport().trim().toUpperCase() + "%' ";
        }

        if (dto.getSearchCostCenter() != null && dto.getSearchCostCenter().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }

            hql = hql + " Upper(costCenter.costCenterCode) like '" + dto.getSearchCostCenter().trim().toUpperCase() + "%' ";
        }

        if (dto.getSearchFullGroupage() != null && dto.getSearchFullGroupage().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }

            hql = hql + " Upper(fullGroupage) like '" + dto.getSearchFullGroupage().trim().toUpperCase() + "%' ";
        }

        if (dto.getSearchStatus() != null && dto.getSearchStatus().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }

            hql = hql + " Upper(status) like '" + dto.getSearchStatus().trim().toUpperCase() + "%' ";
        }

        if (hql.trim().length() != 0) {
            hql = " where " + hql;
        }


        hql = " from ServiceMaster " + hql;


        if (dto.getSortByColumn() != null && dto.getOrderByType() != null && dto.getSortByColumn().trim().length() != 0 && dto.getOrderByType().trim().length() != 0) {
            hql = hql + " order by Upper(" + dto.getSortByColumn().trim() + ") " + dto.getOrderByType().trim();
        } else {
            hql = hql + " order by Upper(serviceName) ASC";
        }

        log.info("Service Master Search hql:[" + hql + "]");


        return search(dto.getSelectedPageNumber(), dto.getRecordPerPage(), hql);

    }


    public SearchRespDto search(SearchRequest dto) {

        String hql = " from ServiceMaster where status!='Hide' ";

        if (dto.getKeyword() != null && dto.getKeyword().trim().length() != 0) {

            hql = hql + " AND ( ";

            hql = hql + " UPPER(serviceName) like '" + dto.getKeyword().trim().toUpperCase() + "%' ";

            hql = hql + " OR ";

            hql = hql + " UPPER(serviceCode) like '" + dto.getKeyword().trim().toUpperCase() + "%' ";

            hql = hql + " ) ";
        }

        hql = hql + " order by serviceName ASC";

        log.info("Service Master Search hql:[" + hql + "]");

        return search(dto.getSelectedPageNumber(), dto.getRecordPerPage(), hql);
    }


    public SearchRespDto search(SearchRequest dto, String transportMode) {

        String hql = " from ServiceMaster where status!='Hide' ";

        if (dto.getKeyword() != null && dto.getKeyword().trim().length() != 0) {

            hql = hql + " AND ( ";

            hql = hql + " UPPER(serviceName) like '" + dto.getKeyword().trim().toUpperCase() + "%' ";

            hql = hql + " OR ";

            hql = hql + " UPPER(serviceCode) like '" + dto.getKeyword().trim().toUpperCase() + "%' ";

            hql = hql + " ) ";
        }


        if (transportMode != null) {
            hql = hql + " AND transportMode = '" + transportMode + "' ";
        }

        hql = hql + " order by serviceName ASC";

        log.info("Service Master Search hql:[" + hql + "]");

        return search(dto.getSelectedPageNumber(), dto.getRecordPerPage(), hql);
    }

    private SearchRespDto search(int selectedPageNumber, int recordPerPage, String hql) {

        String countQ = "Select count (id) " + hql;
        Query countQuery = em.createQuery(countQ);

        Long countTotal = (Long) countQuery.getSingleResult();

        TypedQuery<ServiceMaster> query = em.createQuery(hql, ServiceMaster.class);

        query.setFirstResult(selectedPageNumber * recordPerPage);

        query.setMaxResults(recordPerPage);

        List<ServiceMaster> resultlist = query.getResultList();

        SearchRespDto searchRespDto = new SearchRespDto(countTotal, resultlist);

        log.info("Number of rows selected : " + resultlist.size());

        return searchRespDto;
    }

}
