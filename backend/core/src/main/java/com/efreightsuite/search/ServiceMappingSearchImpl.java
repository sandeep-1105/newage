package com.efreightsuite.search;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import com.efreightsuite.dto.SearchRespDto;
import com.efreightsuite.dto.ServiceMappingSearchDto;
import com.efreightsuite.model.ServiceMapping;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class ServiceMappingSearchImpl {

    @Autowired
    private
    EntityManager em;

    public SearchRespDto search(ServiceMappingSearchDto dto) {

        String hql = "";


        if (dto.getSearchImport() != null && dto.getSearchImport().trim().length() != 0) {
            if (hql != null && hql.trim().length() != 0) {
                hql = hql + " AND ";
            }
            hql = hql + " UPPER(importService.serviceName) like '" + dto.getSearchImport().toUpperCase() + "%' ";
        }
        if (dto.getSearchExport() != null && dto.getSearchExport().trim().length() != 0) {
            if (hql != null && hql.trim().length() != 0) {
                hql = hql + " AND ";
            }
            hql = hql + " UPPER(exportService.serviceName) like '" + dto.getSearchExport().toUpperCase() + "%' ";
        }

        if (hql.trim().length() != 0) {
            hql = " where " + hql;
        }

        hql = " from ServiceMapping " + hql;

        if (dto.getSortByColumn() != null && dto.getOrderByType() != null && dto.getSortByColumn().trim().length() != 0
                && dto.getOrderByType().trim().length() != 0) {
            hql = hql + " order by Upper(" + dto.getSortByColumn().trim() + ") " + dto.getOrderByType().trim();
        } else {
            hql = hql + " order by Upper(importService.serviceName) ASC";
        }

        log.info("ServiceMapping Search hql:[" + hql + "]");

        return search(dto.getSelectedPageNumber(), dto.getRecordPerPage(), hql);

    }


    private SearchRespDto search(int selectedPageNumber, int recordPerPage, String hql) {
        String countQ = "Select count (id) " + hql;

        Query countQuery = em.createQuery(countQ);

        Long countTotal = (Long) countQuery.getSingleResult();

        TypedQuery<ServiceMapping> query = em.createQuery(hql, ServiceMapping.class);

        query.setFirstResult(selectedPageNumber * recordPerPage);

        query.setMaxResults(recordPerPage);

        List<ServiceMapping> resultlist = query.getResultList();

        SearchRespDto searchRespDto = new SearchRespDto(countTotal, resultlist);

        log.info("Number of rows selected : " + resultlist.size());

        return searchRespDto;

    }

}
