package com.efreightsuite.search;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import com.efreightsuite.dto.DateConfigurationDto;
import com.efreightsuite.dto.SearchRespDto;
import com.efreightsuite.model.DateConfiguration;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
@Log4j2
public class DateConfigurationSearchImpl {

    @Autowired
    private
    EntityManager em;

    public SearchRespDto search(DateConfigurationDto dto) {


        String hql = "";

        if (dto.getServiceName() != null && dto.getServiceName().trim().length() != 0) {
            if (hql != null && hql.trim().length() != 0) {
                hql = hql + " AND ";
            }
            hql = hql + " UPPER(serviceMaster.serviceName) like '" + dto.getServiceName().toUpperCase() + "%' ";
        }

        if (dto.getWhichTransaction() != null && dto.getWhichTransaction().trim().length() != 0) {
            if (hql != null && hql.trim().length() != 0) {
                hql = hql + " AND ";
            }
            hql = hql + " whichTransactionDate like '" + dto.getWhichTransaction() + "' ";
        }

        if (dto.getDateLogic() != null && dto.getDateLogic().trim().length() != 0) {
            if (hql != null && hql.trim().length() != 0) {
                hql = hql + " AND ";
            }
            hql = hql + " dateLogic like '" + dto.getDateLogic() + "' ";
        }

        if (hql == null || hql.trim().length() == 0) {
            hql = " from DateConfiguration ";
        } else {
            hql = " from DateConfiguration Where " + hql;
        }

        if (dto.getSortByColumn() != null && dto.getOrderByType() != null && dto.getSortByColumn().trim().length() != 0
                && dto.getOrderByType().trim().length() != 0) {
            hql = hql + " order by Upper(" + dto.getSortByColumn().trim() + ") " + dto.getOrderByType().trim();
        } else {
            hql = hql + " order by Upper(serviceMaster.serviceName) ASC";
        }

        log.info("DateConfiguration Search hql:[" + hql + "]");


        return search(dto.getSelectedPageNumber(), dto.getRecordPerPage(), hql);

    }

    private SearchRespDto search(int selectedPageNumber, int recordPerPage, String hql) {

        String countQ = "Select count (id) " + hql;

        Query countQuery = em.createQuery(countQ);

        Long countTotal = (Long) countQuery.getSingleResult();

        TypedQuery<DateConfiguration> query = em.createQuery(hql, DateConfiguration.class);

        query.setFirstResult(selectedPageNumber * recordPerPage);

        query.setMaxResults(recordPerPage);

        List<DateConfiguration> resultlist = query.getResultList();

        SearchRespDto searchRespDto = new SearchRespDto(countTotal, resultlist);

        log.info("Number of rows selected : " + resultlist.size());

        return searchRespDto;

    }
}
