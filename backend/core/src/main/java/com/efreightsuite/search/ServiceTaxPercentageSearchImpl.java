package com.efreightsuite.search;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.efreightsuite.dto.SearchRespDto;
import com.efreightsuite.dto.ServiceTaxPercentageSearchDto;
import com.efreightsuite.dto.ServiceTaxPercentageSearchResponseDto;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class ServiceTaxPercentageSearchImpl {

    @Autowired
    private
    EntityManager em;

    public SearchRespDto search(ServiceTaxPercentageSearchDto dto) {


        String searchQuery = "SELECT new com.efreightsuite.dto.ServiceTaxPercentageSearchResponseDto(pm.id, pm.taxCode, pm.taxName, pm.status)"
                + " FROM ServiceTaxPercentage pm ";


        String countQuery = "SELECT count(pm.id) "
                + " FROM ServiceTaxPercentage pm";


        String filterQuery = "";

        if (dto.getSearchTaxCode() != null && dto.getSearchTaxCode().trim().length() != 0) {

            if (filterQuery.trim().length() != 0) {
                filterQuery += " AND ";
            }

            filterQuery = filterQuery + " Upper(pm.taxCode) like '" + dto.getSearchTaxCode().trim().toUpperCase() + "%' ";
        }

        if (dto.getSearchTaxName() != null && dto.getSearchTaxName().trim().length() != 0) {

            if (filterQuery.trim().length() != 0) {
                filterQuery += " AND ";
            }

            filterQuery = filterQuery + " Upper(pm.taxName) like '" + dto.getSearchTaxName().trim().toUpperCase() + "%' ";
        }

        if (dto.getSearchStatus() != null && dto.getSearchStatus().trim().length() != 0) {

            if (filterQuery.trim().length() != 0) {
                filterQuery += " AND ";
            }

            filterQuery = filterQuery + " Upper(pm.status) like '" + dto.getSearchStatus().trim().toUpperCase() + "%' ";
        }


        if (filterQuery.trim().length() != 0) {
            filterQuery = " WHERE " + filterQuery;
        }


        String orderByQuery = "";

        if (dto.getSortByColumn() != null && dto.getSortByColumn().trim().length() != 0) {

            if (dto.getSortByColumn().trim().equalsIgnoreCase("taxCode")) {
                orderByQuery = "pm.taxCode";
            } else if (dto.getSortByColumn().trim().equalsIgnoreCase("taxName")) {
                orderByQuery = "pm.taxCodeName";
            } else {
                orderByQuery = "pm.status";
            }

        }


        if (orderByQuery.trim().length() != 0) {
            orderByQuery = " ORDER BY Upper(" + orderByQuery + ") " + dto.getOrderByType();
        } else {
            orderByQuery = " ORDER BY Upper(pm.taxName) ASC";
        }


        countQuery = countQuery + filterQuery;

        log.info("Final Count Query : " + countQuery);

        Query countQ = em.createQuery(countQuery);

        Long countTotal = (Long) countQ.getSingleResult();

        searchQuery = searchQuery + filterQuery + orderByQuery;

        log.info("Final Search Query : " + searchQuery);

        Query searchQ = em.createQuery(searchQuery);

        searchQ.setFirstResult(dto.getSelectedPageNumber() * dto.getRecordPerPage());

        searchQ.setMaxResults(dto.getRecordPerPage());

        @SuppressWarnings("unchecked")
        List<ServiceTaxPercentageSearchResponseDto> resultList = searchQ.getResultList();

        SearchRespDto searchRespDto = new SearchRespDto(countTotal, resultList);

        log.info(resultList.size() + " number of rows selected...");

        return searchRespDto;


    }


}
