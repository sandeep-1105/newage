package com.efreightsuite.search;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import com.efreightsuite.dto.PortSearchDto;
import com.efreightsuite.dto.SearchRequest;
import com.efreightsuite.dto.SearchRespDto;
import com.efreightsuite.model.PortGroupMaster;
import com.efreightsuite.model.PortMaster;
import com.efreightsuite.repository.PortMasterRepository;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Log4j2
@Service
public class PortSearchImpl {


    @Autowired
    private
    EntityManager em;

    @Autowired
    PortMasterRepository portMasterRepository;

    public SearchRespDto search(PortSearchDto dto) {


        String hql = "";

        if (dto.getSearchPortName() != null && dto.getSearchPortName().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }

            hql = hql + " Upper(portName) like '" + dto.getSearchPortName().trim().toUpperCase() + "%' ";
        }

        if (dto.getSearchPortCode() != null && dto.getSearchPortCode().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }


            hql = hql + " Upper(portCode) like '" + dto.getSearchPortCode().trim().toUpperCase() + "%' ";

        }


        if (dto.getSearchTransportMode() != null && dto.getSearchTransportMode().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }

            hql = hql + " Upper(transportMode) like '" + dto.getSearchTransportMode().trim().toUpperCase() + "%' ";
        }

        if (dto.getSearchGroupName() != null && dto.getSearchGroupName().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }

            hql = hql + " portGroupMaster.id IN (select pg.id from PortGroupMaster pg where Upper(pg.portGroupName) like '" + dto.getSearchGroupName().trim().toUpperCase() + "%') ";
        }

        if (dto.getSearchGroupCode() != null && dto.getSearchGroupCode().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }

            hql = hql + " portGroupMaster.id IN (select pg.id from PortGroupMaster pg where Upper(pg.portGroupCode) like '" + dto.getSearchGroupCode().trim().toUpperCase() + "%') ";
        }

        if (dto.getSearchCountryName() != null && dto.getSearchCountryName().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }

            hql = hql + " portGroupMaster.countryMaster.id IN (select cm.id from CountryMaster cm where Upper(cm.countryName) like '" + dto.getSearchCountryName().trim().toUpperCase() + "%') ";
        }


        if (dto.getSearchStatus() != null && dto.getSearchStatus().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }

            hql = hql + " Upper(status) like '" + dto.getSearchStatus().trim().toUpperCase() + "%' ";
        }

        if (hql.trim().length() != 0) {
            hql = " where " + hql;
        }

        hql = " from PortMaster " + hql;


        if (dto.getSortByColumn() != null && dto.getOrderByType() != null && dto.getSortByColumn().trim().length() != 0 && dto.getOrderByType().trim().length() != 0) {
            hql = hql + " order by Upper(" + dto.getSortByColumn().trim() + ") " + dto.getOrderByType().trim();
        } else {
            hql = hql + " order by Upper(portName) ASC";
        }
        log.info("Port  Master Search hql:[" + hql + "]");

        return search(dto.getSelectedPageNumber(), dto.getRecordPerPage(), hql);

    }

    public SearchRespDto search(SearchRequest dto) {


        String hql = " from PortMaster where status!='Hide' ";

        if (dto.getKeyword() != null && dto.getKeyword().trim().length() != 0) {

            hql = hql + " AND ( ";

            hql = hql + " UPPER(portName) like '" + dto.getKeyword().trim().toUpperCase() + "%' ";

            hql = hql + " OR ";

            hql = hql + " UPPER(portCode) like '" + dto.getKeyword().trim().toUpperCase() + "%' ";

            hql = hql + " ) ";
        }


        hql = hql + " order by portName ASC";


        log.info("Port  Master Search hql:[" + hql + "]");


        return search(dto.getSelectedPageNumber(), dto.getRecordPerPage(), hql);
    }


    public SearchRespDto search(SearchRequest dto, String transportMode) {


        String hql = " from PortMaster where status!='Hide' ";

        if (dto.getKeyword() != null && dto.getKeyword().trim().length() != 0) {

            hql = hql + " AND ( ";

            hql = hql + " UPPER(portName) like '" + dto.getKeyword().trim().toUpperCase() + "%' ";

            hql = hql + " OR ";

            hql = hql + " UPPER(portCode) like '" + dto.getKeyword().trim().toUpperCase() + "%' ";

            hql = hql + " ) ";
        }

        if (transportMode != null) {
            hql = hql + " AND transportMode = '" + transportMode + "' ";
        }

        hql = hql + " order by portName ASC";


        log.info("Port  Master Search hql:[" + hql + "]");


        return search(dto.getSelectedPageNumber(), dto.getRecordPerPage(), hql);
    }

    public SearchRespDto searchByGroup(SearchRequest dto, Long groupId, String transportMode) {


        String hql = " from PortMaster where status!='Hide' ";

        if (dto.getKeyword() != null && dto.getKeyword().trim().length() != 0) {

            hql = hql + " AND ( ";

            hql = hql + " UPPER(portName) like '" + dto.getKeyword().trim().toUpperCase() + "%' ";

            hql = hql + " OR ";

            hql = hql + " UPPER(portCode) like '" + dto.getKeyword().trim().toUpperCase() + "%' ";

            hql = hql + " ) ";
        }

        if (groupId != null) {
            hql = hql + " AND portGroupMaster.id = '" + groupId + "' ";

        }

        if (transportMode != null) {
            hql = hql + " AND transportMode = '" + transportMode + "' ";
        }

        hql = hql + " order by portName ASC";


        log.info("Port  Master Search hql:[" + hql + "]");


        return search(dto.getSelectedPageNumber(), dto.getRecordPerPage(), hql);
    }

    private SearchRespDto search(int selectedPageNumber, int recordPerPage, String hql) {

        String countQ = "Select count (id) " + hql;

        Query countQuery = em.createQuery(countQ);

        Long countTotal = (Long) countQuery.getSingleResult();

        TypedQuery<PortMaster> query = em.createQuery(hql, PortMaster.class);

        query.setFirstResult(selectedPageNumber * recordPerPage);

        query.setMaxResults(recordPerPage);

        List<PortMaster> resultlist = query.getResultList();

        log.info(resultlist.size() + " number of rows selected...");

        return new SearchRespDto(countTotal, resultlist);
    }

    private SearchRespDto searchByGroup(int selectedPageNumber, int recordPerPage, String hql) {

        String countQ = "Select count (id) " + hql;

        Query countQuery = em.createQuery(countQ);

        Long countTotal = (Long) countQuery.getSingleResult();

        TypedQuery<PortGroupMaster> query = em.createQuery(hql, PortGroupMaster.class);

        query.setFirstResult(selectedPageNumber * recordPerPage);

        query.setMaxResults(recordPerPage);

        List<PortGroupMaster> resultlist = query.getResultList();

	/*for(PortGroupMaster pg : resultlist){
		pg.setPortMasterList(portMasterRepository.findAllByPortGroup(pg.getId()));
	}*/

        return new SearchRespDto(countTotal, resultlist);
    }

    public Object searchByCountry(SearchRequest searchRequest, String transportMode, String countryCode) {
        // TODO Auto-generated method stub
        String hql = " from PortMaster where status!='Hide' ";

        if (searchRequest.getKeyword() != null && searchRequest.getKeyword().trim().length() != 0) {

            hql = hql + " AND ( ";

            hql = hql + " UPPER(portName) like '" + searchRequest.getKeyword().trim().toUpperCase() + "%' ";

            hql = hql + " OR ";

            hql = hql + " UPPER(portCode) like '" + searchRequest.getKeyword().trim().toUpperCase() + "%' ";

            hql = hql + " ) ";
        }

        if (transportMode != null) {
            hql = hql + " AND transportMode = '" + transportMode + "' ";
        }
        if (countryCode != null) {
            hql = hql + " AND countryCode = '" + countryCode + "' ";
        }

        hql = hql + " order by portName ASC";

        log.info("Port  Master Search hql:[" + hql + "]");

        return search(searchRequest.getSelectedPageNumber(), searchRequest.getRecordPerPage(), hql);

    }
}
