package com.efreightsuite.search;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import com.efreightsuite.dto.DepartmentSearchDto;
import com.efreightsuite.dto.SearchRequest;
import com.efreightsuite.dto.SearchRespDto;
import com.efreightsuite.model.DepartmentMaster;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Log4j2
@Service
public class DepartmentSearchImpl {

    @Autowired
    private
    EntityManager em;

    public SearchRespDto search(DepartmentSearchDto dto) {

        String hql = "";

        if (dto.getSearchDepartmentName() != null && dto.getSearchDepartmentName().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }

            hql = hql + " Upper(departmentName) like '" + dto.getSearchDepartmentName().trim().toUpperCase() + "%' ";
        }

        if (dto.getSearchDepartmentCode() != null && dto.getSearchDepartmentCode().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }

            hql = hql + " Upper(departmentCode) like '" + dto.getSearchDepartmentCode().trim().toUpperCase() + "%' ";

        }

        if (dto.getSearchDepartmentHead() != null && dto.getSearchDepartmentHead().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }

            hql = hql + " Upper(departmentHead.employeeName) like '" + dto.getSearchDepartmentHead().trim().toUpperCase() + "%' ";

        }

        if (dto.getSearchStatus() != null && dto.getSearchStatus().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }

            hql = hql + " Upper(status) like '" + dto.getSearchStatus().trim().toUpperCase() + "%' ";
        }

        if (hql.trim().length() != 0) {
            hql = " where " + hql;
        }

        hql = " from DepartmentMaster " + hql;

        if (dto.getSortByColumn() != null && dto.getOrderByType() != null && dto.getSortByColumn().trim().length() != 0
                && dto.getOrderByType().trim().length() != 0) {
            hql = hql + " order by Upper(" + dto.getSortByColumn().trim() + ") " + dto.getOrderByType().trim();
        } else {
            hql = hql + " order by Upper(departmentName) ASC";
        }
        log.info("Department Master Search hql:[" + hql + "]");

        return search(dto.getSelectedPageNumber(), dto.getRecordPerPage(), hql);

    }

    public SearchRespDto search(SearchRequest dto) {

        String hql = " from DepartmentMaster where status!='Hide' ";

        if (dto.getKeyword() != null && dto.getKeyword().trim().length() != 0) {

            hql = hql + " AND ( ";

            hql = hql + " UPPER(departmentName) like '" + dto.getKeyword().trim().toUpperCase() + "%' ";

            hql = hql + " OR ";

            hql = hql + " UPPER(departmentCode) like '" + dto.getKeyword().trim().toUpperCase() + "%' ";

            hql = hql + " ) ";
        }

        hql = hql + " order by departmentName ASC";

        log.info("Department Master Search hql:[" + hql + "]");

        return search(dto.getSelectedPageNumber(), dto.getRecordPerPage(), hql);
    }

    private SearchRespDto search(int selectedPageNumber, int recordPerPage, String hql) {

        String countQ = "Select count (id) " + hql;

        Query countQuery = em.createQuery(countQ);

        Long countTotal = (Long) countQuery.getSingleResult();

        TypedQuery<DepartmentMaster> query = em.createQuery(hql, DepartmentMaster.class);

        query.setFirstResult(selectedPageNumber * recordPerPage);

        query.setMaxResults(recordPerPage);

        List<DepartmentMaster> resultlist = query.getResultList();

        log.info(resultlist.size() + " number of rows selected...");

        return new SearchRespDto(countTotal, resultlist);
    }
}
