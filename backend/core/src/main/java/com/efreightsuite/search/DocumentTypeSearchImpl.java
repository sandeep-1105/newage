package com.efreightsuite.search;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import com.efreightsuite.dto.DocumentTypeSearchDto;
import com.efreightsuite.dto.SearchRequest;
import com.efreightsuite.dto.SearchRespDto;
import com.efreightsuite.model.DocumentTypeMaster;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class DocumentTypeSearchImpl {

    @Autowired
    private
    EntityManager em;

    public SearchRespDto search(DocumentTypeSearchDto dto) {

        String hql = "";

        if (dto.getSearchDocumentTypeCode() != null && dto.getSearchDocumentTypeCode().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }

            hql = hql + " Upper(documentTypeCode) like '" + dto.getSearchDocumentTypeCode().trim().toUpperCase() + "%' ";
        }

        if (dto.getSearchDocumentTypeName() != null && dto.getSearchDocumentTypeName().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }

            hql = hql + " Upper(documentTypeName) like '" + dto.getSearchDocumentTypeName().trim().toUpperCase() + "%' ";
        }
        if (dto.getSearchStatus() != null && dto.getSearchStatus().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }

            hql = hql + " Upper(status) like '" + dto.getSearchStatus().trim().toUpperCase() + "%' ";
        }

        if (hql.trim().length() != 0) {
            hql = " where " + hql;
        }


        hql = " from DocumentTypeMaster " + hql;


        if (dto.getSortByColumn() != null && dto.getOrderByType() != null && dto.getSortByColumn().trim().length() != 0 && dto.getOrderByType().trim().length() != 0) {
            hql = hql + " order by Upper(" + dto.getSortByColumn().trim() + ") " + dto.getOrderByType().trim();
        } else {
            hql = hql + " order by Upper(documentTypeName) ASC";
        }


        log.info("Document Type Master Search hql:[" + hql + "]");

        return search(dto.getSelectedPageNumber(), dto.getRecordPerPage(), hql);

    }

    public SearchRespDto search(SearchRequest dto) {

        String hql = " from DocumentTypeMaster where status!='Hide' ";

        if (dto.getKeyword() != null && dto.getKeyword().trim().length() != 0) {

            hql = hql + " AND ( ";

            hql = hql + " Upper(documentTypeCode) like '" + dto.getKeyword().trim().toUpperCase() + "%' ";

            hql = hql + " OR ";

            hql = hql + " Upper(documentTypeName) like '" + dto.getKeyword().trim().toUpperCase() + "%' ";

            hql = hql + " ) ";

        }


        hql = hql + " order by documentTypeName ASC";


        log.info("Document Type Master Search hql:[" + hql + "]");


        return search(dto.getSelectedPageNumber(), dto.getRecordPerPage(), hql);
    }

    private SearchRespDto search(int selectedPageNumber, int recordPerPage, String hql) {

        String countQ = "Select count (id) " + hql;

        Query countQuery = em.createQuery(countQ);

        Long countTotal = (Long) countQuery.getSingleResult();

        TypedQuery<DocumentTypeMaster> query = em.createQuery(hql, DocumentTypeMaster.class);

        query.setFirstResult(selectedPageNumber * recordPerPage);

        query.setMaxResults(recordPerPage);

        List<DocumentTypeMaster> resultlist = query.getResultList();

        SearchRespDto searchRespDto = new SearchRespDto(countTotal, resultlist);

        log.info(resultlist.size() + " number of rows selected...");

        return searchRespDto;
    }

}
