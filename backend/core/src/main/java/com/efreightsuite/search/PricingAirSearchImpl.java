package com.efreightsuite.search;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;

import com.efreightsuite.dto.DefaultChargeSearchResponseDTo;
import com.efreightsuite.dto.PricingAirSearchDto;
import com.efreightsuite.dto.PricingAirSearchResponseDto;
import com.efreightsuite.dto.PricingMasterDto;
import com.efreightsuite.dto.PricingMasterListDto;
import com.efreightsuite.dto.SearchRespDto;
import com.efreightsuite.enumeration.ChargeType;
import com.efreightsuite.enumeration.TransportMode;
import com.efreightsuite.enumeration.WhoRouted;
import com.efreightsuite.enumeration.YesNo;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.model.LocationMaster;
import com.efreightsuite.model.PortMaster;
import com.efreightsuite.model.PricingAttachment;
import com.efreightsuite.model.PricingCarrier;
import com.efreightsuite.model.PricingMaster;
import com.efreightsuite.model.PricingPortPair;
import com.efreightsuite.repository.PricingAirAttachmentRepository;
import com.efreightsuite.repository.PricingMasterRepository;
import com.efreightsuite.repository.PricingPortPairRepository;
import com.efreightsuite.service.AuthService;
import com.efreightsuite.validation.PricingAirValidator;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class PricingAirSearchImpl {
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:SS");

    @Autowired
    private
    EntityManager em;


    @Autowired
    private
    PricingMasterRepository pricingMasterRepository;

    @Autowired
    PricingPortPairRepository pricingPortPairRepository;

    @Autowired
    private
    PricingAirValidator pricingAirValidator;

    @Autowired
    PricingAirAttachmentRepository pricingAirAttachmentRepository;

    private SearchRespDto search(int selectedPageNumber, int recordPerPage, String hql) {

        String countQ = "Select count (id) " + hql;

        Query countQuery = em.createQuery(countQ);

        Long countTotal = (Long) countQuery.getSingleResult();

        TypedQuery<PricingMaster> query = em.createQuery(hql, PricingMaster.class);

        query.setFirstResult(selectedPageNumber * recordPerPage);

        query.setMaxResults(recordPerPage);

        List<PricingMaster> resultlist = query.getResultList();

        SearchRespDto searchRespDto = new SearchRespDto(countTotal, resultlist);

        log.info(resultlist.size() + " number of rows selected...");

        return searchRespDto;
    }


    public SearchRespDto search(PricingAirSearchDto dto) {


        String searchQuery = "SELECT new com.efreightsuite.dto.PricingAirSearchResponseDto(pm.id, pm.origin.portName, pm.destination.portName, pm.transit.portName, pm.whoRouted)"
                + " FROM PricingMaster pm "
                + " LEFT JOIN pm.origin ori"
                + " LEFT JOIN pm.destination dest"
                + " LEFT JOIN pm.transit";


        String countQuery = "SELECT count(pm.id) "
                + " FROM PricingMaster pm"
                + " LEFT JOIN pm.origin ori"
                + " LEFT JOIN pm.destination dest"
                + " LEFT JOIN pm.transit";


        String filterQuery = "";

        if (dto.getSearchPortOrigin() != null && dto.getSearchPortOrigin().trim().length() != 0) {

            if (filterQuery.trim().length() != 0) {
                filterQuery += " AND ";
            }

            filterQuery = filterQuery + " Upper(pm.origin.portName) like '" + dto.getSearchPortOrigin().trim().toUpperCase() + "%' ";
        }

        if (dto.getSearchWhoRouted() != null && dto.getSearchWhoRouted().trim().length() != 0) {

            if (filterQuery.trim().length() != 0) {
                filterQuery += " AND ";
            }

            filterQuery = filterQuery + " pm.whoRouted like '" + dto.getSearchWhoRouted().trim() + "' ";
        }

        if (dto.getSearchPortDestination() != null && dto.getSearchPortDestination().trim().length() != 0) {

            if (filterQuery.trim().length() != 0) {
                filterQuery += " AND ";
            }

            filterQuery = filterQuery + " Upper(pm.destination.portName) like '" + dto.getSearchPortDestination().trim().toUpperCase() + "%' ";
        }

        if (dto.getSearchTransitPort() != null && dto.getSearchTransitPort().trim().length() != 0) {

            if (filterQuery.trim().length() != 0) {
                filterQuery += " AND ";
            }

            filterQuery = filterQuery + " Upper(pm.transit.portName) like '" + dto.getSearchTransitPort().trim().toUpperCase() + "%' ";
        }

        if (filterQuery.trim().length() != 0) {
            filterQuery = " WHERE " + filterQuery;
        }


        String orderByQuery = "";

        if (dto.getSortByColumn() != null && dto.getSortByColumn().trim().length() != 0) {

            if (dto.getSortByColumn().trim().equalsIgnoreCase("originName")) {
                orderByQuery = "pm.origin.portName";
            } else if (dto.getSortByColumn().trim().equalsIgnoreCase("destinationName")) {
                orderByQuery = "pm.destination.portName";
            } else {
                orderByQuery = "pm.transit.portName";
            }

        }


        if (orderByQuery.trim().length() != 0) {
            orderByQuery = " ORDER BY " + orderByQuery + " " + dto.getOrderByType();
        } else {
            orderByQuery = " ORDER BY pm.origin.portName ASC";
        }


        countQuery = countQuery + filterQuery;

        log.info("Final Count Query : " + countQuery);

        Query countQ = em.createQuery(countQuery);

        Long countTotal = (Long) countQ.getSingleResult();

        searchQuery = searchQuery + filterQuery + orderByQuery;

        log.info("Final Search Query : " + searchQuery);

        Query searchQ = em.createQuery(searchQuery);

        searchQ.setFirstResult(dto.getSelectedPageNumber() * dto.getRecordPerPage());

        searchQ.setMaxResults(dto.getRecordPerPage());

        @SuppressWarnings("unchecked")
        List<PricingAirSearchResponseDto> resultList = searchQ.getResultList();

        SearchRespDto searchRespDto = new SearchRespDto(countTotal, resultList);

        log.info(resultList.size() + " number of rows selected...");

        return searchRespDto;
    }


//serach which is used for default charges

    public SearchRespDto defaultChargeSearch(PricingAirSearchDto dto) {

        Map<String, Object> params = new HashMap<>();

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:SS");

        String searchQuery = "SELECT new  com.efreightsuite.dto.DefaultChargeSearchResponseDTo( ppp.id , sm.serviceName , cm.chargeName , c.currencyName ,um.unitName , ppp.stdSelInMinimum , ppp.stdSelInAmount , ppp.costInMinimum , ppp.costInAmount , ppp.validFromDate , ppp.validToDate ,"
                + " pm.id , ori.portName, dn.portName, tn.portName )"
                + " FROM PricingPortPair ppp "
                + " left join ppp.serviceMaster sm"
                + " left join ppp.chargeMaster cm "
                + " left join ppp.currencyMaster c "
                + " left join ppp.unitMaster um "
                + " left join ppp.pricingMaster pm "
                + " left join  pm.origin ori "
                + " left join  pm.destination dn "
                + " left join  pm.transit tn ";


        String countQuery = "SELECT count (ppp.id) "
                + " FROM PricingPortPair ppp "
                + " left join ppp.serviceMaster sm "
                + " left join ppp.chargeMaster cm "
                + " left join ppp.currencyMaster c"
                + " left join ppp.unitMaster um "
                + " left join ppp.pricingMaster pm "
                + " left join  pm.origin ori "
                + " left join  pm.destination dn "
                + " left join  pm.transit tn ";


        String filterQuery = "";


        if (dto.getSearchServiceName() != null && dto.getSearchServiceName().trim().length() != 0) {

            if (filterQuery.trim().length() != 0) {
                filterQuery += " AND ";
            }

            filterQuery = filterQuery + " UPPER(ppp.serviceMaster.serviceName) like '" + dto.getSearchServiceName().trim().toUpperCase() + "%' ";
        }


        if (dto.getSearchCharge() != null && dto.getSearchCharge().trim().length() != 0) {

            if (filterQuery.trim().length() != 0) {
                filterQuery += " AND ";
            }

            filterQuery = filterQuery + " UPPER(ppp.chargeMaster.chargeName) like '" + dto.getSearchCharge().trim().toUpperCase() + "%' ";
        }


        if (dto.getSearchCurrency() != null && dto.getSearchCurrency().trim().length() != 0) {

            if (filterQuery.trim().length() != 0) {
                filterQuery += " AND ";
            }

            filterQuery = filterQuery + " UPPER(ppp.currencyMaster.currencyName) like '" + dto.getSearchCurrency().trim().toUpperCase() + "%' ";
        }

        if (dto.getSearchUnit() != null && dto.getSearchUnit().trim().length() != 0) {

            if (filterQuery.trim().length() != 0) {
                filterQuery += " AND ";
            }

            filterQuery = filterQuery + " UPPER(ppp.unitMaster.unitName) like '" + dto.getSearchUnit().trim().toUpperCase() + "%' ";
        }


        if (dto.getSearchAmountPerUnit() != null && dto.getSearchAmountPerUnit().trim().length() != 0) {

            if (filterQuery.trim().length() != 0) {
                filterQuery += " AND ";
            }

            filterQuery = filterQuery + " UPPER(ppp.stdSelInAmount) like '" + dto.getSearchAmountPerUnit() + "%' ";
        }


        if (dto.getSearchMinAmount() != null && dto.getSearchMinAmount().trim().length() != 0) {

            if (filterQuery.trim().length() != 0) {
                filterQuery += " AND ";
            }

            filterQuery = filterQuery + " UPPER(ppp.stdSelInMinimum) like '" + dto.getSearchMinAmount() + "%' ";
        }


        if (dto.getSearchCostInMinimum() != null && dto.getSearchCostInMinimum().trim().length() != 0) {

            if (filterQuery.trim().length() != 0) {
                filterQuery += " AND ";
            }

            filterQuery = filterQuery + " UPPER(ppp.costInMinimum) like '" + dto.getSearchCostInMinimum() + "%' ";
        }


        if (dto.getSearchCostInAmount() != null && dto.getSearchCostInAmount().trim().length() != 0) {

            if (filterQuery.trim().length() != 0) {
                filterQuery += " AND ";
            }

            filterQuery = filterQuery + " UPPER(ppp.costInAmount) like '" + dto.getSearchCostInAmount() + "%' ";
        }

        if (dto.getSearchValidFrom() != null && dto.getSearchValidFrom().getStartDate() != null
                && dto.getSearchValidFrom().getEndDate() != null) {
            try {
                Date dt1 = sdf.parse(dto.getSearchValidFrom().getStartDate());
                Date dt2 = sdf.parse(dto.getSearchValidFrom().getEndDate());

                if (filterQuery.trim().length() != 0) {
                    filterQuery = filterQuery + " AND ";
                }

                params.put("fromStartDate", dt1);
                params.put("fromEndDate", dt2);

                filterQuery = filterQuery + " ( ppp.validFromDate >=:fromStartDate AND ppp.validFromDate <=:fromEndDate ) ";


            } catch (ParseException e) {
                log.error("Date Conversion error in Default charge: +", e);
            }

        }

        if (dto.getSearchValidTo() != null && dto.getSearchValidTo().getStartDate() != null && dto.getSearchValidTo().getEndDate() != null) {

            try {
                Date dt1 = sdf.parse(dto.getSearchValidTo().getStartDate());
                Date dt2 = sdf.parse(dto.getSearchValidTo().getEndDate());

                if (filterQuery.trim().length() != 0) {
                    filterQuery = filterQuery + " AND ";
                }

                params.put("startDate", dt1);
                params.put("endDate", dt2);

                filterQuery = filterQuery + " ( ppp.validToDate >=:startDate AND ppp.validToDate <=:endDate ) ";

            } catch (ParseException e1) {
                log.error("Date Conversion error in Default charge: ", e1);
            }

        }

        if (dto.getSearchPortOrigin() != null && dto.getSearchPortOrigin().trim().length() != 0) {

            if (filterQuery.trim().length() != 0) {
                filterQuery += " AND ";
            }

            filterQuery = filterQuery + " UPPER(pm.origin.portName) like '" + dto.getSearchPortOrigin().trim().toUpperCase() + "%' ";
        }

        if (dto.getSearchPortDestination() != null && dto.getSearchPortDestination().trim().length() != 0) {

            if (filterQuery.trim().length() != 0) {
                filterQuery += " AND ";
            }

            filterQuery = filterQuery + " UPPER(pm.destination.portName) like '" + dto.getSearchPortDestination().trim().toUpperCase() + "%' ";
        }

        if (dto.getSearchTransitPort() != null && dto.getSearchTransitPort().trim().length() != 0) {

            if (filterQuery.trim().length() != 0) {
                filterQuery += " AND ";
            }

            filterQuery = filterQuery + " UPPER(pm.transit.portName) like '" + dto.getSearchTransitPort().trim().toUpperCase() + "%' ";
        }

		
       /*if(dto.getSearchChargeType() != null && dto.getSearchChargeType().trim().length() != 0) {
			
			if(filterQuery.trim().length() != 0) {
				filterQuery += " AND ";
			}
			
			filterQuery = filterQuery + " (ppp.chargeType) like '" + dto.getSearchChargeType()+"%' ";
		}*/


        if (dto.getSearchImportExport() != null && dto.getSearchImportExport().trim().length() != 0) {


            if (dto.getSearchImportExport().equals("Both")) {


            } else {

                if (filterQuery.trim().length() != 0) {
                    filterQuery += " AND ";
                }

                filterQuery = filterQuery + " (ppp.serviceMaster.importExport) like '" + dto.getSearchImportExport() + "%' ";
            }

        }


        if (filterQuery.trim().length() != 0) {
            filterQuery = " WHERE" + filterQuery + " AND ppp.chargeType ='Origin' OR  ppp.chargeType = 'Destination'";
        } else {
            filterQuery = " WHERE ppp.chargeType ='Origin' OR  ppp.chargeType = 'Destination'";
        }

        String orderByQuery = "";

        if (dto.getSortByColumn() != null && dto.getSortByColumn().trim().length() != 0) {

            if (dto.getSortByColumn().trim().equalsIgnoreCase("serviceName")) {
                orderByQuery = "ppp.serviceMaster.serviceName";
            } else if (dto.getSortByColumn().trim().equalsIgnoreCase("chargeName")) {
                orderByQuery = "ppp.chargeMaster.chargeName";
            } else if (dto.getSortByColumn().trim().equalsIgnoreCase("currencyName")) {
                orderByQuery = "ppp.currencyMaster.currencyName";
            } else if (dto.getSortByColumn().trim().equalsIgnoreCase("unitName")) {
                orderByQuery = "ppp.unitMaster.unitName";
            } else if (dto.getSortByColumn().trim().equalsIgnoreCase("stdSelInMinimum")) {
                orderByQuery = "ppp.stdSelInMinimum";
            } else if (dto.getSortByColumn().trim().equalsIgnoreCase("stdSelInAmount")) {
                orderByQuery = "ppp.stdSelInAmount";
            } else if (dto.getSortByColumn().trim().equalsIgnoreCase("originName")) {
                orderByQuery = "pm.origin.portName";
            } else if (dto.getSortByColumn().trim().equalsIgnoreCase("transitName")) {
                orderByQuery = "pm.transit.portName";
            } else if (dto.getSortByColumn().trim().equalsIgnoreCase("destinationName")) {
                orderByQuery = "pm.destination.portName";
            } else if (dto.getSortByColumn().trim().equalsIgnoreCase("costInAmount")) {
                orderByQuery = "ppp.costInAmount";
            } else if (dto.getSortByColumn().trim().equalsIgnoreCase("costInMinimum")) {
                orderByQuery = "ppp.costInMinimum";
            } else if (dto.getSortByColumn().trim().equalsIgnoreCase("validFrom")) {
                orderByQuery = "ppp.validFromDate";
            } else if (dto.getSortByColumn().trim().equalsIgnoreCase("validTo")) {
                orderByQuery = "ppp.validToDate";
            }

        }


        if (orderByQuery.trim().length() != 0) {
            orderByQuery = " ORDER BY UPPER(" + orderByQuery + ") " + dto.getOrderByType();
        } else {
            orderByQuery = " ORDER BY UPPER(ppp.serviceMaster.serviceName) ASC";
        }

        countQuery = countQuery + filterQuery;

        log.info("Final Count Query : " + countQuery);

        Query countQ = em.createQuery(countQuery);

        for (Entry<String, Object> e : params.entrySet()) {

            countQ.setParameter(e.getKey(), e.getValue());
        }

        Long countTotal = (Long) countQ.getSingleResult();

        searchQuery = searchQuery + filterQuery + orderByQuery;

        log.info("Final Search Query : " + searchQuery);

        Query searchQ = em.createQuery(searchQuery);

        for (Entry<String, Object> e : params.entrySet()) {
            searchQ.setParameter(e.getKey(), e.getValue());
        }

        searchQ.setFirstResult(dto.getSelectedPageNumber() * dto.getRecordPerPage());

        searchQ.setMaxResults(dto.getRecordPerPage());

        @SuppressWarnings("unchecked")
        List<DefaultChargeSearchResponseDTo> resultList = searchQ.getResultList();

        SearchRespDto searchRespDto = new SearchRespDto(countTotal, resultList);

        log.info(resultList.size() + " number of rows selected...");

        return searchRespDto;
    }

    public PricingMaster getExistingChargeUpdate(PricingMaster pricingMasterValue, PricingPortPair updatePricingPortPair) {


        PricingMaster tmpPricing = null;

        TransportMode transportMode = null;


        if (pricingMasterValue != null && pricingMasterValue.getOrigin() != null && pricingMasterValue.getOrigin().getId() != null && pricingMasterValue.getGeneralOriginPriceList() != null && pricingMasterValue.getGeneralOriginPriceList().size() > 0) {

            if (pricingMasterValue.getGeneralOriginPriceList().get(0).getServiceMaster() != null) {
                transportMode = pricingMasterValue.getGeneralOriginPriceList().get(0).getServiceMaster().getTransportMode();
            } else {
                transportMode = updatePricingPortPair.getServiceMaster().getTransportMode();
            }
        } else if (pricingMasterValue != null && pricingMasterValue.getDestination() != null && pricingMasterValue.getDestination().getId() != null && pricingMasterValue.getGeneralDestinationPriceList() != null && pricingMasterValue.getGeneralDestinationPriceList().size() > 0) {

            if (pricingMasterValue.getGeneralDestinationPriceList().get(0).getServiceMaster() != null) {

                transportMode = pricingMasterValue.getGeneralDestinationPriceList().get(0).getServiceMaster().getTransportMode();
            } else {
                transportMode = updatePricingPortPair.getServiceMaster().getTransportMode();
            }
        }
        if (transportMode == null) {
            transportMode = TransportMode.Air;
        }
        WhoRouted whoRouted = null;
        PortMaster origin = null;
        PortMaster destination = null;
        PortMaster transit = null;
        if (null != pricingMasterValue) {
            whoRouted = pricingMasterValue.getWhoRouted();
            origin = pricingMasterValue.getOrigin();
            destination = pricingMasterValue.getDestination();
            transit = pricingMasterValue.getTransit();
        }
        LocationMaster location = AuthService.getCurrentUser().getSelectedUserLocation();

        if (origin != null && destination != null) {
            if (transit == null) {
                tmpPricing = pricingMasterRepository.getByBasedIdOriginAndDestAndRoutedAndTrasitIsNullAndTransportMode(origin.getId(), destination.getId(), whoRouted, transportMode);
            } else {
                tmpPricing = pricingMasterRepository.getByBasedIdOriginAndDestAndTrasitAndRoutedAndTransportMode(origin.getId(), destination.getId(), transit.getId(), whoRouted, transportMode);
            }
        } else if (origin == null && destination == null) {
            tmpPricing = pricingMasterRepository.getByLocationCharge(location.getId(), whoRouted, transportMode);
        } else if (origin != null && destination == null) {
            tmpPricing = pricingMasterRepository.getByLocationChargeWithOrigin(location.getId(), origin.getId(), whoRouted, transportMode);
        } else if (origin == null && destination != null) {
            tmpPricing = pricingMasterRepository.getByLocationChargeWithDestination(location.getId(), destination.getId(), whoRouted, transportMode);
        }

        return tmpPricing;

    }


    private void uniquePricing(PricingMaster pricingMaster) {

        try {

            PricingMaster tmpPricing = null;

            Long id = pricingMaster.getId();
            TransportMode transportMode = pricingMaster.getTransportMode();
            WhoRouted whoRouted = pricingMaster.getWhoRouted();
            PortMaster origin = pricingMaster.getOrigin();
            PortMaster destination = pricingMaster.getDestination();
            PortMaster transit = pricingMaster.getTransit();

            if (origin != null && destination != null) {
                if (transit == null) {
                    tmpPricing = pricingMasterRepository.getByBasedIdOriginAndDestAndRoutedAndTrasitIsNullAndTransportMode(origin.getId(), destination.getId(), whoRouted, transportMode);
                } else {
                    tmpPricing = pricingMasterRepository.getByBasedIdOriginAndDestAndTrasitAndRoutedAndTransportMode(origin.getId(), destination.getId(), transit.getId(), whoRouted, transportMode);
                }
            } else if (origin == null && destination == null) {
                tmpPricing = pricingMasterRepository.getByLocationCharge(AuthService.getCurrentUser().getSelectedUserLocation().getId(), whoRouted, transportMode);
            } else if (origin != null && destination == null) {
                tmpPricing = pricingMasterRepository.getByLocationChargeWithOrigin(AuthService.getCurrentUser().getSelectedUserLocation().getId(), origin.getId(), whoRouted, transportMode);
            } else if (origin == null && destination != null) {
                tmpPricing = pricingMasterRepository.getByLocationChargeWithDestination(AuthService.getCurrentUser().getSelectedUserLocation().getId(), destination.getId(), whoRouted, transportMode);
            }

            if (id != null) {

                if (tmpPricing == null || (tmpPricing != null && Objects.equals(tmpPricing.getId(), id))) {
                    return;
                }

            } else {
                if (tmpPricing == null) {
                    return;
                }
            }

        } catch (Exception exception) {
            log.error("Pricing Air Unique Validation is not matching : ", exception);
        }

        throw new RestException(ErrorCode.UK_PRICING_ALREADY_EXIST);

    }

    public Long processUpdate(PricingMaster pricingMaster) {

        pricingAirValidator.validate(pricingMaster);

        if (pricingMaster.getOrigin() != null && pricingMaster.getDestination() != null) {
            uniquePricing(pricingMaster);
        }

        if (pricingMaster.getGeneralOriginPriceList() != null && pricingMaster.getGeneralOriginPriceList().size() > 0) {
            for (PricingPortPair pricingPortPair : pricingMaster.getGeneralOriginPriceList()) {
                pricingAirValidator.pricingPortValidate(pricingPortPair);
                pricingPortPair.setHazardous(YesNo.No);
                pricingPortPair.setChargeType(ChargeType.Origin);
                pricingPortPair.setPricingMaster(pricingMaster);
            }

        } else {
            pricingMaster.getGeneralOriginPriceList().clear();
            // pricingMaster.setGeneralOriginPriceList(new ArrayList<>());
        }

        if (pricingMaster.getGeneralFreightPriceList() != null
                && pricingMaster.getGeneralFreightPriceList().size() > 0) {

            for (PricingCarrier pricingCarrier : pricingMaster.getGeneralFreightPriceList()) {
                if (pricingCarrier.getMinimumSellPrice() != null) {
                }
                if (pricingCarrier.getCost() != null) {
                }
                pricingCarrier.setHazardous(YesNo.No);
                pricingCarrier.setChargeType(ChargeType.Freight);
                pricingAirValidator.pricingCarrierValidate(pricingCarrier);
                pricingCarrier.setPricingMaster(pricingMaster);

            }
        } else {
            pricingMaster.getGeneralFreightPriceList().clear();
            // pricingMaster.setGeneralFreightPriceList(new ArrayList<>());
        }

        if (pricingMaster.getGeneralDestinationPriceList() != null
                && pricingMaster.getGeneralDestinationPriceList().size() > 0) {

            for (PricingPortPair pricingPortPair : pricingMaster.getGeneralDestinationPriceList()) {
                pricingPortPair.setHazardous(YesNo.No);
                pricingPortPair.setChargeType(ChargeType.Destination);
                pricingAirValidator.pricingPortValidate(pricingPortPair);
                pricingPortPair.setPricingMaster(pricingMaster);
            }

        } else {
            pricingMaster.getGeneralDestinationPriceList().clear();
            // pricingMaster.setGeneralDestinationPriceList(new ArrayList<>());
        }

        if (pricingMaster.getHazardousOriginPriceList() != null
                && pricingMaster.getHazardousOriginPriceList().size() > 0) {
            for (PricingPortPair pricingPortPair : pricingMaster.getHazardousOriginPriceList()) {
                pricingPortPair.setHazardous(YesNo.Yes);
                pricingPortPair.setChargeType(ChargeType.Origin);
                pricingAirValidator.pricingPortValidate(pricingPortPair);
                pricingPortPair.setPricingMaster(pricingMaster);
            }
        } else {
            pricingMaster.getHazardousOriginPriceList().clear();
            // pricingMaster.setHazardousOriginPriceList(new ArrayList<>());
        }

        if (pricingMaster.getHazardousFreightPriceList() != null
                && pricingMaster.getHazardousFreightPriceList().size() > 0) {

            for (PricingCarrier pricingCarrier : pricingMaster.getHazardousFreightPriceList()) {
                if (pricingCarrier.getMinimumSellPrice() != null) {
                }
                if (pricingCarrier.getCost() != null) {
                }
                pricingCarrier.setHazardous(YesNo.Yes);
                pricingCarrier.setChargeType(ChargeType.Freight);
                pricingAirValidator.pricingCarrierValidate(pricingCarrier);
                pricingCarrier.setPricingMaster(pricingMaster);
            }
        } else {
            pricingMaster.getHazardousFreightPriceList().clear();
            // pricingMaster.setHazardousFreightPriceList(new ArrayList<>());
        }

        if (pricingMaster.getHazardousDestinationPriceList() != null
                && pricingMaster.getHazardousDestinationPriceList().size() > 0) {

            for (PricingPortPair pricingPortPair : pricingMaster.getHazardousDestinationPriceList()) {
                pricingPortPair.setHazardous(YesNo.Yes);
                pricingPortPair.setChargeType(ChargeType.Destination);
                pricingAirValidator.pricingPortValidate(pricingPortPair);
                pricingPortPair.setPricingMaster(pricingMaster);
            }

        } else {
            pricingMaster.getHazardousDestinationPriceList().clear();
            // pricingMaster.setHazardousDestinationPriceList(new
            // ArrayList<>());
        }

        if (pricingMaster.getAttachmentList() != null && pricingMaster.getAttachmentList().size() > 0) {
            for (PricingAttachment pricingAttachment : pricingMaster.getAttachmentList()) {
                pricingAirValidator.pricingAttachmentValidate(pricingAttachment);
                pricingAttachment.setPricingMaster(pricingMaster);
            }
        } else {
            pricingMaster.getAttachmentList().clear();
            // pricingMaster.setAttachmentList(new ArrayList<>());
        }

        pricingMaster = pricingMasterRepository.save(pricingMaster);

        return pricingMaster.getId();
    }

    public Long processCreate(PricingMaster pricingMaster) {

        pricingAirValidator.validate(pricingMaster);

        uniquePricing(pricingMaster);

        if (pricingMaster.getGeneralOriginPriceList() != null && pricingMaster.getGeneralOriginPriceList().size() > 0) {

            for (PricingPortPair pricingPortPair : pricingMaster.getGeneralOriginPriceList()) {
                pricingAirValidator.pricingPortValidate(pricingPortPair);
                pricingPortPair.setHazardous(YesNo.No);
                pricingPortPair.setChargeType(ChargeType.Origin);
                pricingPortPair.setPricingMaster(pricingMaster);
            }

        } else {
            pricingMaster.setGeneralOriginPriceList(null);
        }

        if (pricingMaster.getGeneralFreightPriceList() != null
                && pricingMaster.getGeneralFreightPriceList().size() > 0) {

            for (PricingCarrier pricingCarrier : pricingMaster.getGeneralFreightPriceList()) {
                if (pricingCarrier.getMinimumSellPrice() != null) {
                }
                if (pricingCarrier.getCost() != null) {
                }
                pricingCarrier.setHazardous(YesNo.No);
                pricingCarrier.setChargeType(ChargeType.Freight);
                pricingAirValidator.pricingCarrierValidate(pricingCarrier);
                pricingCarrier.setPricingMaster(pricingMaster);

            }
        } else {
            pricingMaster.setGeneralFreightPriceList(null);
        }

        if (pricingMaster.getGeneralDestinationPriceList() != null
                && pricingMaster.getGeneralDestinationPriceList().size() > 0) {

            for (PricingPortPair pricingPortPair : pricingMaster.getGeneralDestinationPriceList()) {
                pricingPortPair.setHazardous(YesNo.No);
                pricingPortPair.setChargeType(ChargeType.Destination);
                pricingAirValidator.pricingPortValidate(pricingPortPair);
                pricingPortPair.setPricingMaster(pricingMaster);
            }

        } else {
            pricingMaster.setGeneralDestinationPriceList(null);
        }

        if (pricingMaster.getHazardousOriginPriceList() != null
                && pricingMaster.getHazardousOriginPriceList().size() > 0) {
            for (PricingPortPair pricingPortPair : pricingMaster.getHazardousOriginPriceList()) {
                pricingPortPair.setHazardous(YesNo.Yes);
                pricingPortPair.setChargeType(ChargeType.Origin);
                pricingAirValidator.pricingPortValidate(pricingPortPair);
                pricingPortPair.setPricingMaster(pricingMaster);
            }
        } else {
            pricingMaster.setHazardousOriginPriceList(null);
        }

        if (pricingMaster.getHazardousFreightPriceList() != null
                && pricingMaster.getHazardousFreightPriceList().size() > 0) {

            for (PricingCarrier pricingCarrier : pricingMaster.getHazardousFreightPriceList()) {
                if (pricingCarrier.getMinimumSellPrice() != null) {
                }
                if (pricingCarrier.getCost() != null) {
                }
                pricingCarrier.setHazardous(YesNo.Yes);
                pricingCarrier.setChargeType(ChargeType.Freight);
                pricingAirValidator.pricingCarrierValidate(pricingCarrier);
                pricingCarrier.setPricingMaster(pricingMaster);
            }
        } else {
            pricingMaster.setHazardousOriginPriceList(null);
        }

        if (pricingMaster.getHazardousDestinationPriceList() != null
                && pricingMaster.getHazardousDestinationPriceList().size() > 0) {

            for (PricingPortPair pricingPortPair : pricingMaster.getHazardousDestinationPriceList()) {
                pricingPortPair.setHazardous(YesNo.Yes);
                pricingPortPair.setChargeType(ChargeType.Destination);
                pricingAirValidator.pricingPortValidate(pricingPortPair);
                pricingPortPair.setPricingMaster(pricingMaster);
            }

        } else {
            pricingMaster.setHazardousDestinationPriceList(null);
        }

        if (pricingMaster.getAttachmentList() != null && pricingMaster.getAttachmentList().size() > 0) {
            for (PricingAttachment pricingAttachment : pricingMaster.getAttachmentList()) {
                pricingAirValidator.pricingAttachmentValidate(pricingAttachment);
                pricingAttachment.setPricingMaster(pricingMaster);
            }
        } else {
            pricingMaster.setAttachmentList(null);
        }

        pricingMaster = pricingMasterRepository.save(pricingMaster);

        return pricingMaster.getId();
    }


    //used for default charges add
    @Transactional
    public void createPricingObject(PricingMasterListDto pricingMasterListDto) {
        // TODO Auto-generated method stub

        if (pricingMasterListDto != null && pricingMasterListDto.getPricingMasterList() != null && pricingMasterListDto.getPricingMasterList().size() > 0) {

            for (PricingMasterDto pricingMasterValue : pricingMasterListDto.getPricingMasterList()) {

                PricingMaster pricingNew = getExistingCharge(pricingMasterValue);

                if (pricingNew != null && pricingNew.getId() != null) {

                    for (PricingPortPair pp : pricingMasterValue.getPricingPortPairList()) {
                        if (pricingNew.getOrigin() != null && pricingNew.getOrigin().getId() != null) {
                            PricingPortPair ppNew = new PricingPortPair();
                            if (pricingNew.getGeneralOriginPriceList() != null && pricingNew.getGeneralOriginPriceList().size() > 0) {


                            } else {
                                pricingNew.setGeneralOriginPriceList(new ArrayList<>());
                                ppNew.setChargeType(ChargeType.Origin);
                            }
                            setAllValuesOfPortPair(ppNew, pp, pricingNew);
                            pricingNew.getGeneralOriginPriceList().add(ppNew);
                        } else {
                            PricingPortPair ppNew = new PricingPortPair();
                            if (pricingNew.getGeneralDestinationPriceList() != null && pricingNew.getGeneralDestinationPriceList().size() > 0) {


                            } else {
                                pricingNew.setGeneralDestinationPriceList(new ArrayList<>());
                                ppNew.setChargeType(ChargeType.Destination);
                            }
                            setAllValuesOfPortPair(ppNew, pp, pricingNew);
                            //pricingNew.getGeneralDestinationPriceList().clear();
                            pricingNew.getGeneralDestinationPriceList().add(ppNew);
                        }
                    }

                    pricingMasterRepository.save(pricingNew);

                } else {
                    if (pricingMasterValue.getPricingPortPairList() != null && pricingMasterValue.getPricingPortPairList().size() > 0) {
                        pricingNew = new PricingMaster();
                        pricingNew.setOrigin(pricingMasterValue.getOrigin());
                        pricingNew.setOriginPortCode(pricingMasterValue.getOrigin() != null ? pricingMasterValue.getOrigin().getPortCode() : null);
                        pricingNew.setDestination(pricingMasterValue.getDestination());
                        pricingNew.setDestinationPortCode(pricingMasterValue.getDestination() != null ? pricingMasterValue.getDestination().getPortCode() : null);
                        pricingNew.setTransit(pricingMasterValue.getTransit());
                        pricingNew.setTransitPortCode(pricingMasterValue.getTransit() != null ? pricingMasterValue.getTransit().getPortCode() : null);
                        pricingNew.setWhoRouted(pricingMasterValue.getWhoRouted());
                        pricingNew.setLocationMaster(AuthService.getCurrentUser().getSelectedUserLocation());
                        pricingNew.setLocationCode(AuthService.getCurrentUser().getSelectedUserLocation().getLocationCode());
                        pricingNew.setCompanyMaster(AuthService.getCurrentUser().getSelectedCompany());
                        pricingNew.setCompanyCode(AuthService.getCurrentUser().getSelectedCompany().getCompanyCode());
                        pricingNew.setCountryMaster(AuthService.getCurrentUser().getSelectedUserLocation().getCountryMaster());
                        pricingNew.setCountryCode(AuthService.getCurrentUser().getSelectedUserLocation().getCountryMaster().getCountryCode());

                        for (int i = 0; i < pricingMasterValue.getPricingPortPairList().size(); i++) {
                            if (pricingNew.getOrigin() != null && pricingNew.getOrigin().getId() != null) {
                                setOriginTypeObject(pricingNew, pricingMasterValue.getPricingPortPairList().get(i));
                            } else if (pricingNew.getDestination() != null && pricingNew.getDestination().getId() != null) {
                                setDestinationTypeObject(pricingNew, pricingMasterValue.getPricingPortPairList().get(i));

                            }
                        }
                    }
                }

            }
        }
    }

    private PricingMaster getExistingCharge(PricingMasterDto pricingMasterValue) {


        PricingMaster tmpPricing = null;


        TransportMode transportMode = pricingMasterValue.getPricingPortPairList().get(0).getServiceMaster().getTransportMode();
        WhoRouted whoRouted = pricingMasterValue.getWhoRouted();
        PortMaster origin = pricingMasterValue.getOrigin();
        PortMaster destination = pricingMasterValue.getDestination();
        PortMaster transit = pricingMasterValue.getTransit();
        LocationMaster location = AuthService.getCurrentUser().getSelectedUserLocation();

        if (origin != null && destination != null) {
            if (transit == null) {
                tmpPricing = pricingMasterRepository.getByBasedIdOriginAndDestAndRoutedAndTrasitIsNullAndTransportMode(origin.getId(), destination.getId(), whoRouted, transportMode);
            } else {
                tmpPricing = pricingMasterRepository.getByBasedIdOriginAndDestAndTrasitAndRoutedAndTransportMode(origin.getId(), destination.getId(), transit.getId(), whoRouted, transportMode);
            }
        } else if (origin == null && destination == null) {
            tmpPricing = pricingMasterRepository.getByLocationCharge(location.getId(), whoRouted, transportMode);
        } else if (origin != null && destination == null) {
            tmpPricing = pricingMasterRepository.getByLocationChargeWithOrigin(location.getId(), origin.getId(), whoRouted, transportMode);
        } else if (origin == null && destination != null) {
            tmpPricing = pricingMasterRepository.getByLocationChargeWithDestination(location.getId(), destination.getId(), whoRouted, transportMode);
        }

        return tmpPricing;

    }


    private void setOriginTypeObject(PricingMaster pricingNew,
                                     PricingPortPair pricingPortPair) {
        if (pricingPortPair.getHazardous() != null && pricingPortPair.getHazardous().equals(YesNo.No)) {
            pricingNew.setGeneralOriginPriceList(new ArrayList<>());
            pricingNew.setTransportMode(pricingPortPair.getServiceMaster().getTransportMode());
            PricingPortPair portPairNew = new PricingPortPair();
            portPairNew.setChargeType(ChargeType.Origin);
            setAllValuesOfPortPair(portPairNew, pricingPortPair, pricingNew);
            pricingNew.getGeneralOriginPriceList().add(portPairNew);
            processCreate(pricingNew);
        } else {
            pricingNew.setHazardousOriginPriceList(new ArrayList<>());
            PricingPortPair portPairNew = new PricingPortPair();
            portPairNew.setChargeType(ChargeType.Origin);
            pricingNew.setTransportMode(pricingPortPair.getServiceMaster().getTransportMode());
            setAllValuesOfPortPair(portPairNew, pricingPortPair, pricingNew);
            pricingNew.getHazardousOriginPriceList().add(portPairNew);
            processCreate(pricingNew);
        }
    }


    private void setAllValuesOfPortPair(PricingPortPair portPairNew, PricingPortPair pricingPortPair, PricingMaster pricingNew) {

        portPairNew.setId(pricingPortPair.getId());
        portPairNew.setVersionLock(pricingPortPair.getVersionLock());
        portPairNew.setPricingMaster(pricingNew);
        portPairNew.setServiceMaster(pricingPortPair.getServiceMaster());
        portPairNew.setChargeMaster(pricingPortPair.getChargeMaster());
        portPairNew.setChargeCode(pricingPortPair.getChargeMaster() != null ? pricingPortPair.getChargeMaster().getChargeCode() : null);
        if (portPairNew.getChargeType() == null) {
            portPairNew.setChargeType(pricingPortPair.getChargeMaster() != null ? pricingPortPair.getChargeMaster().getChargeType() : null);
        }
        portPairNew.setCurrencyMaster(pricingPortPair.getCurrencyMaster());
        portPairNew.setCurrencyCode(pricingPortPair.getCurrencyMaster() != null ? pricingPortPair.getCurrencyMaster().getCurrencyCode() : null);
        portPairNew.setUnitMaster(pricingPortPair.getUnitMaster());
        portPairNew.setUnitCode(pricingPortPair.getUnitMaster() != null ? pricingPortPair.getUnitMaster().getUnitCode() : null);
        portPairNew.setStdSelInAmount(pricingPortPair.getStdSelInAmount());
        portPairNew.setStdSelInMinimum(pricingPortPair.getStdSelInMinimum());
        portPairNew.setCostInAmount(pricingPortPair.getCostInAmount());
        portPairNew.setCostInMinimum(pricingPortPair.getCostInMinimum());
        portPairNew.setTosMaster(pricingPortPair.getTosMaster());
        portPairNew.setPpcc(pricingPortPair.getPpcc());
        portPairNew.setWhoRouted(pricingPortPair.getWhoRouted());
        portPairNew.setValidToDate(pricingPortPair.getValidToDate());
        portPairNew.setValidFromDate(pricingPortPair.getValidFromDate());
        portPairNew.setDivisionMaster(pricingPortPair.getDivisionMaster());
        portPairNew.setHazardous(pricingPortPair.getHazardous());
        portPairNew.setIsClearance(pricingPortPair.getIsClearance());
        portPairNew.setIsForwarderOrDirect(pricingPortPair.getIsForwarderOrDirect());
        portPairNew.setAgent(pricingPortPair.getAgent());
        portPairNew.setCurrencyMaster(pricingPortPair.getCurrencyMaster());
        portPairNew.setCurrencyCode(pricingPortPair.getCurrencyMaster() != null ? pricingPortPair.getCurrencyMaster().getCurrencyCode() : null);
        portPairNew.setIsPersonalEffect(pricingPortPair.getIsPersonalEffect());
        portPairNew.setIsCoload(pricingPortPair.getIsCoload());
        portPairNew.setIsTransit(pricingPortPair.getIsTransit());
        portPairNew.setIsOurTransport(pricingPortPair.getIsOurTransport());
        portPairNew.setCommodityMaster(pricingPortPair.getCommodityMaster());
        portPairNew.setCarrierMaster(pricingPortPair.getCarrierMaster());
        portPairNew.setShipper(pricingPortPair.getShipper());
        portPairNew.setConsignee(pricingPortPair.getConsignee());
        portPairNew.setLocationMaster(AuthService.getCurrentUser().getSelectedUserLocation());
        portPairNew.setLocationCode(AuthService.getCurrentUser().getSelectedUserLocation().getLocationCode());
        portPairNew.setCompanyMaster(AuthService.getCurrentUser().getSelectedCompany());
        portPairNew.setCompanyCode(AuthService.getCurrentUser().getSelectedCompany().getCompanyCode());
        portPairNew.setCountryMaster(AuthService.getCurrentUser().getSelectedUserLocation().getCountryMaster());
        portPairNew.setCountryCode(AuthService.getCurrentUser().getSelectedUserLocation().getCountryMaster().getCountryCode());

    }


    private void setDestinationTypeObject(PricingMaster pricingNew,
                                          PricingPortPair pricingPortPair) {
        if (pricingPortPair.getHazardous() != null && pricingPortPair.getHazardous().equals(YesNo.No)) {
            pricingNew.setGeneralDestinationPriceList(new ArrayList<>());
            PricingPortPair portPairNew = new PricingPortPair();
            portPairNew.setChargeType(ChargeType.Destination);
            pricingNew.setTransportMode(pricingPortPair.getServiceMaster().getTransportMode());
            setAllValuesOfPortPair(portPairNew, pricingPortPair, pricingNew);
            pricingNew.getGeneralDestinationPriceList().add(portPairNew);
            processCreate(pricingNew);
        } else {
            pricingNew.setHazardousDestinationPriceList(new ArrayList<>());
            PricingPortPair portPairNew = new PricingPortPair();
            portPairNew.setChargeType(ChargeType.Destination);
            pricingNew.setTransportMode(pricingPortPair.getServiceMaster().getTransportMode());
            setAllValuesOfPortPair(portPairNew, pricingPortPair, pricingNew);
            pricingNew.getHazardousDestinationPriceList().add(portPairNew);
            processCreate(pricingNew);
        }

    }


}
