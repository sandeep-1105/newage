package com.efreightsuite.search;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import com.efreightsuite.common.component.CacheRepository;
import com.efreightsuite.dto.SearchRespDto;
import com.efreightsuite.dto.StockEnquiryDto;
import com.efreightsuite.dto.StockGenerationSearchDto;
import com.efreightsuite.model.CarrierMaster;
import com.efreightsuite.model.PortMaster;
import com.efreightsuite.model.StockGeneration;
import com.efreightsuite.repository.CarrierMasterRepository;
import com.efreightsuite.repository.PortMasterRepository;
import com.efreightsuite.util.SaaSUtil;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class StockSearchImpl {

    @Autowired
    private
    EntityManager em;

    private Date dt1;
    private Date dt2;
    private Boolean flag = true;

    @Autowired
    private
    CacheRepository cacheRepository;

    @Autowired
    private
    PortMasterRepository portMasterRepository;

    @Autowired
    private
    CarrierMasterRepository carrierMasterRepository;

    public SearchRespDto search(StockGenerationSearchDto dto) throws ParseException {

        String hql = "";

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

        Map<String, Object> params = new HashMap<>();


        if (dto.getCarrierName() != null && dto.getCarrierName().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }
            hql = hql + " Upper(carrier.carrierName) like '" + dto.getCarrierName().toUpperCase() + "%' ";
        }

        if (dto.getPortCode() != null && dto.getPortCode().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }
            hql = hql + " Upper(por.portCode) like '" + dto.getPortCode().trim().toUpperCase() + "%' ";
        }


        if (dto.getCarrierNo() != null && dto.getCarrierNo().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }
            hql = hql + " Upper(carrier.carrierNo) like '" + dto.getCarrierNo().trim().toUpperCase() + "%' ";
        }

        if (dto.getAwbNo() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }
            hql = hql + " Upper(awbNo) like '" + dto.getAwbNo() + "%' ";
        }

        if (dto.getCheckDigit() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }
            hql = hql + " Upper(checkDigit) like '" + dto.getCheckDigit() + "%' ";
        }

        if (dto.getMawbNo() != null && dto.getMawbNo().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }
            hql = hql + " Upper(mawbNo) like '" + dto.getMawbNo() + "%' ";
        }

        if (dto.getStockStatus() != null && dto.getStockStatus().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }
            hql = hql + " stockStatus like '" + dto.getStockStatus() + "%' ";
        }

        if (dto.getReceivedOn() != null && dto.getPriorDate() != null) {

            try {

                log.info("before date values" + dto.getReceivedOn() + "-----" + dto.getPriorDate());
                dt1 = sdf.parse(dto.getReceivedOn());
                dt2 = sdf.parse(dto.getPriorDate());

                log.info("after date values" + dt1 + "-----" + dt2);
                if (hql.trim().length() != 0) {
                    hql = hql + " AND ";
                }
                params.put("fromStartDate", dt2);
                params.put("fromEndDate", dt1);

                hql = hql + " ( receivedOnCheck >= :fromStartDate AND receivedOnCheck <= :fromEndDate ) ";

                flag = false;
            } catch (ParseException e1) {
                log.error("Date Conversion error in shipmentReqDate: ", e1);
            }


        }
        if (dto.getReceivedOn() != null && flag) {
            dt1 = sdf.parse(dto.getReceivedOn());
            params.put("fromStartDate", dt1);
            hql = hql + " ( receivedOnCheck == :fromStartDate) ";
        }


        if (dto.getConsolUid() != null && dto.getConsolUid().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }
            hql = hql + " consol.id IN (select c.id from Consol c where Upper(c.consolUid) like '" + dto.getConsolUid().trim().toUpperCase() + "%') ";
        }

        if (dto.getServiceCode() != null && dto.getServiceCode().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }
            hql = hql + " Upper(serviceCode) like '" + dto.getServiceCode() + "%' ";
        }

        if (dto.getEndingNo() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }
            hql = hql + " Upper(endingNo) like '" + dto.getEndingNo() + "%' ";
        }


        if (hql.trim().length() != 0) {
            hql = " where " + hql;
        }

        hql = " from StockGeneration " + hql;
        log.info("hql generation" + hql);
        if (dto.getSortByColumn() != null && dto.getOrderByType() != null && dto.getSortByColumn().trim().length() != 0 && dto.getOrderByType().trim().length() != 0) {
            hql = hql + " order by " + dto.getSortByColumn().trim() + " " + dto.getOrderByType().trim() + " nulls last";

        } else {

            hql = hql + " order by id ASC";
        }

        log.info("Stock Gneration Search hql:[" + hql + "]");


        return search(dto.getSelectedPageNumber(), dto.getRecordPerPage(), hql, params);

    }

    private SearchRespDto search(int selectedPageNumber, int recordPerPage, String hql, Map<String, Object> params) throws ParseException {
        String countQ = "Select count (id) " + hql;

        Query countQuery = em.createQuery(countQ);
        for (Entry<String, Object> e : params.entrySet()) {
            countQuery.setParameter(e.getKey(), e.getValue());
        }
        Long countTotal = (Long) countQuery.getSingleResult();


        TypedQuery<StockGeneration> query = em.createQuery(hql, StockGeneration.class);

        for (Entry<String, Object> e : params.entrySet()) {
            query.setParameter(e.getKey(), e.getValue());
        }

        query.setFirstResult(selectedPageNumber * recordPerPage);

        query.setMaxResults(recordPerPage);

        List<StockGeneration> resultlist = query.getResultList();

        return new SearchRespDto(countTotal, resultlist);
    }

    public SearchRespDto stockEnquiry(StockEnquiryDto searchDto) {


        String sql = " select s.carrier.id AS cId, s.por.id AS pId, COUNT(s) As StockInCount from StockGeneration s where UPPER(s.stockStatus) like 'AVAILABLE' ";

        if (searchDto.getCarrierNo() != null && searchDto.getCarrierNo().trim().length() != 0) {

            if (sql.trim().length() != 0) {
                sql = sql + " AND ";
            }
            sql = sql + " Upper(s.carrier.carrierNo) like '" + searchDto.getCarrierNo() + "%' ";
        }


        if (searchDto.getCarrierName() != null && searchDto.getCarrierName().trim().length() != 0) {

            if (sql.trim().length() != 0) {
                sql = sql + " AND ";
            }
            sql = sql + " Upper(s.carrier.carrierName) like '" + searchDto.getCarrierName() + "%' ";
        }

        if (searchDto.getPortName() != null && searchDto.getPortName().trim().length() != 0) {

            if (sql.trim().length() != 0) {
                sql = sql + " AND ";
            }
            sql = sql + " Upper(s.por.portName) like '" + searchDto.getPortName() + "%' ";
        }

        if (searchDto.getShipment() != null && searchDto.getShipment().trim().length() != 0) {

            if (sql.trim().length() != 0) {
                sql = sql + " AND ";
            }

            sql = sql + " Upper(s.consol.consolUid) like '" + searchDto.getShipment() + "%' ";
        }
        if (searchDto.getWarning() != null && searchDto.getWarning().trim().length() != 0) {

            if (sql.trim().length() != 0) {
                sql = sql + " AND ";
            }
            sql = sql + " Upper(s.remaindNo) like '" + searchDto.getWarning() + "%' ";
        }

        sql = sql + "  GROUP BY s.carrier.id , s.por.id  ";


        if (searchDto.getSortByColumn() != null && searchDto.getOrderByType() != null && searchDto.getSortByColumn().trim().length() != 0 && searchDto.getOrderByType().trim().length() != 0) {
            sql = sql + " order by " + searchDto.getSortByColumn().trim() + " " + searchDto.getOrderByType().trim();
        }


        javax.persistence.Query query = em.createQuery(sql);

        long countTotal = query.getResultList().size();

        query.setFirstResult(searchDto.getSelectedPageNumber() * searchDto.getRecordPerPage());

        query.setMaxResults(searchDto.getRecordPerPage());

        List<Object[]> results = query.getResultList();

        List<StockEnquiryDto> list = new ArrayList<>();

        for (Object[] result : results) {
            StockEnquiryDto dto = new StockEnquiryDto();

            Long cId = ((Number) result[0]).longValue();
            Long pId = ((Number) result[1]).longValue();

            dto.setStockInHand(((Number) result[2]).longValue());

            PortMaster po = cacheRepository.get(SaaSUtil.getSaaSId(), PortMaster.class.getName(), pId, portMasterRepository);
            dto.setPortName(po.getPortName());

            CarrierMaster co = cacheRepository.get(SaaSUtil.getSaaSId(), CarrierMaster.class.getName(), cId, carrierMasterRepository);
            dto.setCarrierNo(co.getCarrierNo());
            dto.setCarrierName(co.getCarrierName());


            String hql = "select s from StockGeneration s where s.carrier.id=" + cId + " and s.por.id=" + pId + " and s.stockStatus='Generated' order by s.id desc";

            List<StockGeneration> sgList = em.createQuery(hql, StockGeneration.class).setMaxResults(1).getResultList();

            if (sgList != null && sgList.size() != 0) {
                dto.setLastUsedMawb(sgList.get(0).getMawbNo());
                dto.setLastUsedMawbDate(sgList.get(0).getLastUsedMawbDate());
                if (sgList.get(0).getConsol() != null)
                    dto.setShipment(sgList.get(0).getConsol().getConsolUid());
                else
                    dto.setShipment(sgList.get(0).getShipmentUid());
            }


            String hql2 = "select s from StockGeneration s where s.carrier.id=" + cId + " and s.por.id=" + pId + " and s.stockStatus='Available' order by s.id asc";
            List<StockGeneration> sgList2 = em.createQuery(hql2, StockGeneration.class).setMaxResults(1).getResultList();

            if (sgList2 != null && sgList2.size() != 0) {
                dto.setNextMawb(sgList2.get(0).getMawbNo());
            }


            String hql3 = "select s from StockGeneration s where s.carrier.id=" + cId + " and s.por.id=" + pId + " and s.remindNo IS NOT NULL order by s.id desc";
            List<StockGeneration> sgList3 = em.createQuery(hql3, StockGeneration.class).setMaxResults(1).getResultList();

            if (sgList3 != null && sgList3.size() != 0) {
                dto.setWarning(sgList3.get(0).getMawbNo());
            }

            list.add(dto);
        }

        return new SearchRespDto(countTotal, list);

    }


    public StockGeneration getMawb(Long carrierId, Long porid) {

        Query query = em.createQuery("select s from StockGeneration s where s.carrier.id=" + carrierId
                + " and s.por.id=" + porid + " and Upper(s.stockStatus) like 'AVAILABLE' order by s.id asc");

        List<StockGeneration> results = query.getResultList();

        log.info(results.size() + " rows have been selected....");

        if (!results.isEmpty()) {
            return results.get(0);
        }
        return null;

    }

}
