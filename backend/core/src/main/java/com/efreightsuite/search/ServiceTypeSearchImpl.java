package com.efreightsuite.search;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import com.efreightsuite.dto.SearchRequest;
import com.efreightsuite.dto.SearchRespDto;
import com.efreightsuite.dto.ServiceTypeSearchDto;
import com.efreightsuite.model.ServiceTypeMaster;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class ServiceTypeSearchImpl {

    @Autowired
    private
    EntityManager em;

    public SearchRespDto search(SearchRequest dto, String transPortMode) {

        String hql = " from ServiceTypeMaster where status!='Hide' ";

        if (dto.getKeyword() != null && dto.getKeyword().trim().length() != 0) {

            hql = hql + " AND (";

            hql = hql + " UPPER(serviceTypeName) like '" + dto.getKeyword().trim().toUpperCase() + "%' ";

            hql = hql + " OR ";

            hql = hql + " UPPER(serviceTypeCode) like '" + dto.getKeyword().trim().toUpperCase() + "%' ";

            hql = hql + " ) ";
        }

        if (transPortMode != null && transPortMode.trim().length() > 0) {
            hql = hql + " AND transportMode = '" + transPortMode + "' " + "OR transportMode is NULL";
        } else {
            hql = hql + " AND transportMode is NULL";
        }

        hql = hql + " order by serviceTypeName ASC";


        log.info("serviceTypeName Search hql:[" + hql + "]");

        return search(dto.getSelectedPageNumber(), dto.getRecordPerPage(), hql);
    }


    public SearchRespDto search(ServiceTypeSearchDto dto) {


        String hql = "";


        if (dto.getSearchServiceTypeName() != null && dto.getSearchServiceTypeName().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }

            hql = hql + " Upper(serviceTypeName) like '" + dto.getSearchServiceTypeName().trim().toUpperCase() + "%' ";
        }

        if (dto.getSearchServiceTypeCode() != null && dto.getSearchServiceTypeCode().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }

            hql = hql + " Upper(serviceTypeCode) like '" + dto.getSearchServiceTypeCode().trim().toUpperCase() + "%' ";
        }

        if (dto.getSearchStatus() != null && dto.getSearchStatus().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }

            hql = hql + " Upper(status) like '" + dto.getSearchStatus().trim().toUpperCase() + "%' ";
        }

        if (dto.getSearchServiceTransportMode() != null && dto.getSearchServiceTransportMode().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }

            hql = hql + " Upper(transportMode) like '" + dto.getSearchServiceTransportMode().trim().toUpperCase() + "%' ";
        }

        if (dto.getSearchServiceType() != null && dto.getSearchServiceType().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }

            hql = hql + " Upper(serviceType) like '" + dto.getSearchServiceType().trim().toUpperCase() + "%' ";
        }

        if (hql.trim().length() != 0) {
            hql = " where " + hql;
        }

        hql = " from ServiceTypeMaster " + hql;


        if (dto.getSortByColumn() != null && dto.getOrderByType() != null && dto.getSortByColumn().trim().length() != 0 && dto.getOrderByType().trim().length() != 0) {
            hql = hql + " order by Upper(" + dto.getSortByColumn().trim() + ") " + dto.getOrderByType().trim();
        } else {
            hql = hql + " order by Upper(serviceTypeName) ASC";
        }


        log.info("ServiceTypeMaster  Search hql:[" + hql + "]");


        return search(dto.getSelectedPageNumber(), dto.getRecordPerPage(), hql);
    }


    private SearchRespDto search(int selectedPageNumber, int recordPerPage, String hql) {

        String countQ = "Select count (id) " + hql;

        Query countQuery = em.createQuery(countQ);

        Long countTotal = (Long) countQuery.getSingleResult();

        TypedQuery<ServiceTypeMaster> query = em.createQuery(hql, ServiceTypeMaster.class);

        query.setFirstResult(selectedPageNumber * recordPerPage);

        query.setMaxResults(recordPerPage);

        List<ServiceTypeMaster> resultlist = query.getResultList();

        return new SearchRespDto(countTotal, resultlist);
    }


}
