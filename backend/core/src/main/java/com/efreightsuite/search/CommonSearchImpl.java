package com.efreightsuite.search;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.efreightsuite.dto.SearchRequest;
import com.efreightsuite.dto.SearchRespDto;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Log4j2
@Service
public class CommonSearchImpl {
    @Autowired
    private
    EntityManager em;

    private static String commaSeparatedIds(String ids[]) {
        return StringUtils.join(ids, ",");
    }

    private SearchRespDto search(String countQuery, String searchQuery, SearchRequest dto) {
        log.info("Count Query : " + countQuery);

        Query countQ = em.createQuery(countQuery);

        Long countTotal = (Long) countQ.getSingleResult();

        log.info("Count Query Result : " + countTotal);

        Query query = em.createQuery(searchQuery);

        log.info("Search Query : " + query);

        query.setFirstResult(dto.getSelectedPageNumber() * dto.getRecordPerPage());

        query.setMaxResults(dto.getRecordPerPage());

        return new SearchRespDto(countTotal, query.getResultList());
    }

    public SearchRespDto searchServiceNotInList(SearchRequest dto) {

        String searchQuery = "SELECT NEW com.efreightsuite.dto.ServiceMasterSearchResponseDto (" +
                "  s.id,  s.serviceCode,  s.serviceName,  s.status)  FROM ServiceMaster s ";

        String countQuery = "SELECT COUNT(s.id) FROM ServiceMaster s ";

        String filterQuery = "";

        if (dto.getIds() != null && dto.getIds().length > 0) {
            filterQuery = " s.id NOT IN (" + commaSeparatedIds(dto.getIds()) + ")  ";
        }

        if (dto.getKeyword() != null && dto.getKeyword().trim().length() != 0) {

            if (filterQuery.trim().length() != 0) {
                filterQuery += " AND ";
            }

            filterQuery = filterQuery + " (UPPER(s.serviceCode) LIKE '" + dto.getKeyword().trim().toUpperCase() + "%' OR  UPPER(s.serviceName) LIKE '" +
                    dto.getKeyword().trim().toUpperCase() + "%' ) AND s.status != 'Hide' ";

        }

        if (filterQuery.trim().length() != 0) {
            filterQuery = " WHERE " + filterQuery;
        }

        searchQuery = searchQuery + filterQuery + " ORDER BY s.serviceName ASC";

        countQuery = countQuery + filterQuery;

        return search(countQuery, searchQuery, dto);
    }


    public SearchRespDto searchDivisionNotInList(SearchRequest dto) {

        String searchQuery = "SELECT NEW com.efreightsuite.dto.DivisionMasterSearchResponseDto (" + "  d.id, "
                + "  d.divisionCode, d.divisionName,  d.status) " + " FROM DivisionMaster d ";

        String countQuery = "SELECT COUNT(d.id) FROM DivisionMaster d ";

        String filterQuery = "";

        if (dto.getIds() != null && dto.getIds().length > 0) {
            filterQuery = " d.id NOT IN (" + commaSeparatedIds(dto.getIds()) + ")  ";
        }

        if (dto.getKeyword() != null && dto.getKeyword().trim().length() != 0) {

            if (filterQuery.trim().length() != 0) {
                filterQuery += " AND ";
            }

            filterQuery = filterQuery + " (UPPER(d.divisionCode) LIKE '" + dto.getKeyword().trim().toUpperCase() + "%' OR  UPPER(d.divisionName) LIKE '" +
                    dto.getKeyword().trim().toUpperCase() + "%' ) AND d.status != 'Hide' ";

        }

        if (filterQuery.trim().length() != 0) {
            filterQuery = " WHERE " + filterQuery;
        }

        searchQuery = searchQuery + filterQuery + " ORDER BY d.divisionName ASC";

        countQuery = countQuery + filterQuery;

        return search(countQuery, searchQuery, dto);
    }


    public SearchRespDto searchCountryNotInList(SearchRequest dto) {

        String searchQuery = "SELECT NEW com.efreightsuite.dto.CountryMasterSearchResponseDto (" +
                " c.id, c.countryCode,  c.countryName, c.image,  c.status) FROM CountryMaster c ";

        String countQuery = "SELECT COUNT(c.id) FROM CountryMaster c ";

        String filterQuery = "";

        if (dto.getIds() != null && dto.getIds().length > 0) {
            filterQuery = " c.id NOT IN ( " + commaSeparatedIds(dto.getIds()) + ")  ";
        }

        if (dto.getKeyword() != null && dto.getKeyword().trim().length() != 0) {

            if (filterQuery.trim().length() != 0) {
                filterQuery += " AND ";
            }

            filterQuery = filterQuery + " (UPPER(c.countryCode) LIKE '" + dto.getKeyword().trim().toUpperCase() + "%' OR  UPPER(c.countryName) LIKE '" +
                    dto.getKeyword().trim().toUpperCase() + "%' ) AND c.status != 'Hide' ";

        }

        if (filterQuery.trim().length() != 0) {
            filterQuery = " WHERE " + filterQuery;
        }

        searchQuery = searchQuery + filterQuery + " ORDER BY c.countryName ASC";

        countQuery = countQuery + filterQuery;

        return search(countQuery, searchQuery, dto);
    }


    public SearchRespDto searchLocationNotInList(SearchRequest dto) {

        String searchQuery = "SELECT NEW com.efreightsuite.dto.LocationMasterSearchResponseDto (" +
                " l.id, l.locationCode,  l.locationName,  l.status) FROM LocationMaster l ";

        String countQuery = "SELECT COUNT(l.id) FROM LocationMaster l ";

        String filterQuery = "";

        if (dto.getIds() != null && dto.getIds().length > 0) {
            filterQuery = " l.id NOT IN ( " + commaSeparatedIds(dto.getIds()) + ")  ";
        }

        if (dto.getKeyword() != null && dto.getKeyword().trim().length() != 0) {

            if (filterQuery.trim().length() != 0) {
                filterQuery += " AND ";
            }

            filterQuery = filterQuery + " (UPPER(l.locationCode) LIKE '" + dto.getKeyword().trim().toUpperCase() + "%' OR  UPPER(l.locationName) LIKE '" +
                    dto.getKeyword().trim().toUpperCase() + "%' ) AND l.status != 'Hide' ";

        }

        if (filterQuery.trim().length() != 0) {
            filterQuery = " WHERE " + filterQuery;
        }

        searchQuery = searchQuery + filterQuery + " ORDER BY l.locationName ASC";

        countQuery = countQuery + filterQuery;

        return search(countQuery, searchQuery, dto);
    }


    public SearchRespDto searchPartyNotInList(SearchRequest dto) {

        String searchQuery = "SELECT NEW com.efreightsuite.dto.PartySearchResponseDto (" +
                " p.id, p.partyCode,  p.partyName,  p.status) FROM PartyMaster p ";

        String countQuery = "SELECT COUNT(p.id) FROM PartyMaster p ";

        String filterQuery = "";

        if (dto.getIds() != null && dto.getIds().length > 0) {
            filterQuery = " p.id NOT IN ( " + commaSeparatedIds(dto.getIds()) + ")  ";
        }

        if (dto.getKeyword() != null && dto.getKeyword().trim().length() != 0) {

            if (filterQuery.trim().length() != 0) {
                filterQuery += " AND ";
            }

            filterQuery = filterQuery + " (UPPER(p.partyCode) LIKE '" + dto.getKeyword().trim().toUpperCase() + "%' OR  UPPER(p.partyName) LIKE '" +
                    dto.getKeyword().trim().toUpperCase() + "%' ) AND p.status != 'Hide' ";

        }

        if (filterQuery.trim().length() != 0) {
            filterQuery = " WHERE " + filterQuery;
        }

        searchQuery = searchQuery + filterQuery + " ORDER BY p.partyName ASC";

        countQuery = countQuery + filterQuery;

        return search(countQuery, searchQuery, dto);
    }

    public SearchRespDto searchUserNotInList(SearchRequest dto) {

        String searchQuery = "SELECT NEW com.efreightsuite.dto.UserDto (" +
                " u.id,  u.userName,  u.status) FROM UserProfile u ";

        String countQuery = "SELECT COUNT(u.id) FROM UserProfile u ";

        String filterQuery = "";

        if (dto.getIds() != null && dto.getIds().length > 0) {
            filterQuery = " u.id NOT IN ( " + commaSeparatedIds(dto.getIds()) + ")  ";
        }

        if (dto.getKeyword() != null && dto.getKeyword().trim().length() != 0) {

            if (filterQuery.trim().length() != 0) {
                filterQuery += " AND ";
            }

            filterQuery = filterQuery + " UPPER(u.userName) LIKE '" + dto.getKeyword().trim().toUpperCase() + "%' AND u.status = 'Active' ";

        }

        if (filterQuery.trim().length() != 0) {
            filterQuery = " WHERE " + filterQuery;
        }

        searchQuery = searchQuery + filterQuery + " ORDER BY u.userName ASC";

        countQuery = countQuery + filterQuery;

        return search(countQuery, searchQuery, dto);
    }


    public SearchRespDto searchChargeNotInList(SearchRequest dto) {

        String searchQuery = "SELECT NEW com.efreightsuite.dto.ChargeMasterSearchResponseDto (" +
                "  cm.id,  cm.chargeCode,  cm.chargeName,  cm.status)  FROM ChargeMaster cm ";

        String countQuery = "SELECT COUNT(cm.id) FROM ChargeMaster cm ";

        String filterQuery = "";

        if (dto.getIds() != null && dto.getIds().length > 0) {
            filterQuery = " cm.id NOT IN (" + commaSeparatedIds(dto.getIds()) + ")  ";
        }

        if (dto.getKeyword() != null && dto.getKeyword().trim().length() != 0) {

            if (filterQuery.trim().length() != 0) {
                filterQuery += " AND ";
            }

            filterQuery = filterQuery + " (UPPER(cm.chargeCode) LIKE '" + dto.getKeyword().trim().toUpperCase() + "%' OR  UPPER(cm.chargeName) LIKE '" +
                    dto.getKeyword().trim().toUpperCase() + "%' ) AND cm.status != 'Hide' ";

        }

        if (filterQuery.trim().length() != 0) {
            filterQuery = " WHERE " + filterQuery;
        }

        searchQuery = searchQuery + filterQuery + " ORDER BY cm.chargeName ASC";

        countQuery = countQuery + filterQuery;

        return search(countQuery, searchQuery, dto);
    }


}
