package com.efreightsuite.search;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import com.efreightsuite.dto.GroupSubGroupDto;
import com.efreightsuite.dto.SearchRespDto;
import com.efreightsuite.model.GeneralLedgerGroup;
import com.efreightsuite.model.GeneralLedgerSubGroup;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class GroupAndSubGroupSeacrhImpl {

    @Autowired
    private
    EntityManager em;

    //serach for group
    public SearchRespDto searchGroupLov(GroupSubGroupDto dto) {

        log.info("Group Search called.");

        String hql = " from GroupMaster where status!='Hide' ";

        if (dto.getKeyword() != null && dto.getKeyword().trim().length() != 0) {

            hql = hql + " AND ( ";

            hql = hql + " UPPER(groupCode) like '" + dto.getKeyword().trim().toUpperCase() + "%' ";

            hql = hql + " OR ";

            hql = hql + " UPPER(groupName) like '" + dto.getKeyword().trim().toUpperCase() + "%' ";

            hql = hql + " ) ";

        }
        if (dto.getGlHead() != null && dto.getGlHead().trim().length() != 0) {

            hql = hql + " AND glHead  like '" + dto.getGlHead() + "%'  ";
        }

        hql = hql + " order by groupName ASC";

        return search(dto.getSelectedPageNumber(), dto.getRecordPerPage(), hql, "group");

    }


    public SearchRespDto searchSubGroupLov(GroupSubGroupDto dto) {

        log.info("Sub Group Search called.");

        String hql = " from SubGroupMaster where status!='Hide' ";

        if (dto.getKeyword() != null && dto.getKeyword().trim().length() != 0) {

            hql = hql + " AND ( ";

            hql = hql + " UPPER(subGroupCode) like '" + dto.getKeyword().trim().toUpperCase() + "%' ";

            hql = hql + " OR ";

            hql = hql + " UPPER(subGroupName) like '" + dto.getKeyword().trim().toUpperCase() + "%' ";

            hql = hql + " ) ";

        }
        if (dto.getGlHead() != null && dto.getGlHead().trim().length() != 0) {

            hql = hql + " AND glHead  like '" + dto.getGlHead() + "%'  ";
        }

        hql = hql + " order by subGroupName ASC";

        return search(dto.getSelectedPageNumber(), dto.getRecordPerPage(), hql, "subgroup");
    }


    private SearchRespDto search(int selectedPageNumber, int recordPerPage, String hql, String which) {

        String countQ = "Select count (id) " + hql;

        Query countQuery = em.createQuery(countQ);

        Long countTotal = (Long) countQuery.getSingleResult();
        SearchRespDto searchRespDto;

        if (which == "group") {
            TypedQuery<GeneralLedgerGroup> query = em.createQuery(hql, GeneralLedgerGroup.class);
            query.setFirstResult(selectedPageNumber * recordPerPage);
            query.setMaxResults(recordPerPage);
            List<GeneralLedgerGroup> resultlist = query.getResultList();
            searchRespDto = new SearchRespDto(countTotal, resultlist);
        } else {
            TypedQuery<GeneralLedgerSubGroup> query = em.createQuery(hql, GeneralLedgerSubGroup.class);
            query.setFirstResult(selectedPageNumber * recordPerPage);
            query.setMaxResults(recordPerPage);
            List<GeneralLedgerSubGroup> resultlist = query.getResultList();
            searchRespDto = new SearchRespDto(countTotal, resultlist);
        }


        return searchRespDto;

    }


}
