package com.efreightsuite.search;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import com.efreightsuite.dto.FeatureDTO;
import com.efreightsuite.dto.RoleDTO;
import com.efreightsuite.dto.RoleSearchDto;
import com.efreightsuite.dto.SearchRequest;
import com.efreightsuite.dto.SearchRespDto;
import com.efreightsuite.enumeration.YesNo;
import com.efreightsuite.model.Feature;
import com.efreightsuite.model.Role;
import com.efreightsuite.model.RoleHasFeature;
import com.efreightsuite.repository.RolesRepository;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Log4j2
public class RoleSearchImpl {

    @Autowired
    private
    EntityManager em;

    @Autowired
    private
    RolesRepository roleRepository;

    @Transactional
    public Role saveUpdate(RoleDTO request) {

        Role existingRoleEntity;
        existingRoleEntity = roleRepository.findById(request.getId());

        if(existingRoleEntity != null) {
            return updateRole(existingRoleEntity, request);
        }
        else {
            return saveRole(request);
        }
    }

    private Role updateRole(Role roleEntity, RoleDTO data) {

        roleEntity.setStatus(data.getStatus());
        roleEntity.setRoleName(data.getRoleName());
        roleEntity.setDescription(data.getDescription());

        for (FeatureDTO featureDto : data.getFeaturesList()) {
            boolean featureExists = false;
            for (RoleHasFeature roleHasFeature : roleEntity.getFeatureList()) {
                if (roleHasFeature.getFeature() != null
                        && (roleHasFeature.getFeature().getId().longValue() == featureDto.getId().longValue())) {
                    featureExists = true;
                    boolean isSelectedFeature = updateFeature(featureDto, roleHasFeature);
                    if (isSelectedFeature) {
                        roleHasFeature.setRole(roleEntity);
                        roleHasFeature.setFeature(new Feature(featureDto.getId()));
                    }
                }
            }
            if(!featureExists){
                RoleHasFeature rhf = new RoleHasFeature();

                boolean isSelectedFeature = updateFeature(featureDto, rhf);

                if (isSelectedFeature) {
                    rhf.setRole(roleEntity);
                    rhf.setFeature(new Feature(featureDto.getId()));
                }
                roleEntity.getFeatureList().add(rhf);
            }

        }
        return roleRepository.save(roleEntity);
    }

    private Role saveRole(RoleDTO data) {

        Role roleEntity = new Role();
        roleEntity.setStatus(data.getStatus());
        roleEntity.setRoleName(data.getRoleName());
        roleEntity.setDescription(data.getDescription());

        List<RoleHasFeature> tmpSelecteds = new ArrayList<>();

        for (FeatureDTO featureDto : data.getFeaturesList()) {
            RoleHasFeature rhf = new RoleHasFeature();

            boolean isSelectedFeature = updateFeature(featureDto, rhf);

            if (isSelectedFeature) {
                rhf.setRole(roleEntity);
                rhf.setFeature(new Feature(featureDto.getId()));
                tmpSelecteds.add(rhf);
            }
        }
        roleEntity.getFeatureList().addAll(tmpSelecteds);

        return roleRepository.save(roleEntity);
    }

    private boolean updateFeature(FeatureDTO featureDto, RoleHasFeature roleHasFeature) {
        boolean isSelectedFeature = false;
        if (featureDto.getList() == YesNo.Yes && featureDto.getValList() != null && featureDto.getValList().toUpperCase().equals("YES")) {
            roleHasFeature.setValList(featureDto.getFeatureValue() + ".LIST");
            isSelectedFeature = true;
        } else {
            roleHasFeature.setValList(null);
        }
        if (featureDto.getCreate() == YesNo.Yes && featureDto.getValCreate() != null && featureDto.getValCreate().toUpperCase().equals("YES")) {
            roleHasFeature.setValCreate(featureDto.getFeatureValue() + ".CREATE");
            isSelectedFeature = true;
        } else {
            roleHasFeature.setValCreate(null);
        }
        if (featureDto.getModify() == YesNo.Yes && featureDto.getValModify() != null && featureDto.getValModify().toUpperCase().equals("YES")) {
            roleHasFeature.setValModify(featureDto.getFeatureValue() + ".MODIFY");
            isSelectedFeature = true;
        } else {
            roleHasFeature.setValModify(null);
        }
        if (featureDto.getView() == YesNo.Yes && featureDto.getValView() != null && featureDto.getValView().toUpperCase().equals("YES")) {
            roleHasFeature.setValView(featureDto.getFeatureValue() + ".VIEW");
            isSelectedFeature = true;
        } else {
            roleHasFeature.setValView(null);
        }
        if (featureDto.getDelete() == YesNo.Yes && featureDto.getValDelete() != null && featureDto.getValDelete().toUpperCase().equals("YES")) {
            roleHasFeature.setValDelete(featureDto.getFeatureValue() + ".DELETE");
            isSelectedFeature = true;
        } else {
            roleHasFeature.setValDelete(null);
        }
        if (featureDto.getDownload() == YesNo.Yes && featureDto.getValDownload() != null && featureDto.getValDownload().toUpperCase().equals("YES")) {
            roleHasFeature.setValDownload(featureDto.getFeatureValue() + ".DOWNLOAD");
            isSelectedFeature = true;
        } else {
            roleHasFeature.setValDownload(null);
        }
        return isSelectedFeature;
    }

    public SearchRespDto search(RoleSearchDto dto) {

        String hql = "";

        if (dto.getSearchRoleName() != null && dto.getSearchRoleName().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }

            hql = hql + " Upper(roleName) like '" + dto.getSearchRoleName().trim().toUpperCase() + "%' ";
        }

        if (dto.getSearchDescription() != null && dto.getSearchDescription().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }

            hql = hql + " Upper(description) like '" + dto.getSearchDescription().trim().toUpperCase() + "%' ";
        }


        if (dto.getSearchStatus() != null && dto.getSearchStatus().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }

            hql = hql + " Upper(status) like '" + dto.getSearchStatus().trim().toUpperCase() + "%' ";
        }

        if (hql.trim().length() != 0) {
            hql = " where " + hql;
        }

        hql = " from Role " + hql;

        if (dto.getSortByColumn() != null && dto.getOrderByType() != null && dto.getSortByColumn().trim().length() != 0 && dto.getOrderByType().trim().length() != 0) {
            hql = hql + " order by Upper(" + dto.getSortByColumn().trim() + ") " + dto.getOrderByType().trim();
        } else {
            hql = hql + " order by Upper(roleName) ASC";
        }

        log.info("Role Search hql:[" + hql + "]");

        return search(dto.getSelectedPageNumber(), dto.getRecordPerPage(), hql);

    }

    public SearchRespDto search(SearchRequest dto) {

        String hql = " from Role where status!='Hide' ";

        if (dto.getKeyword() != null && dto.getKeyword().trim().length() != 0) {

            hql = hql + " AND ( ";

            hql = hql + " UPPER(roleName) like '" + dto.getKeyword().trim().toUpperCase() + "%' ";

            hql = hql + " ) ";
        }

        hql = hql + " order by UPPER(roleName) ASC";

        log.info("Role Search hql:[" + hql + "]");

        return search(dto.getSelectedPageNumber(), dto.getRecordPerPage(), hql);
    }

    private SearchRespDto search(int selectedPageNumber, int recordPerPage, String hql) {

        String countQ = "Select count (id) " + hql;

        Query countQuery = em.createQuery(countQ);

        Long countTotal = (Long) countQuery.getSingleResult();

        TypedQuery<Role> query = em.createQuery(hql, Role.class);

        query.setFirstResult(selectedPageNumber * recordPerPage);

        query.setMaxResults(recordPerPage);

        List<Role> resultlist = query.getResultList();

        log.info("Number of Records Selected : " + resultlist.size());

        return new SearchRespDto(countTotal, RoleDTO.toDTOList(resultlist));
    }


}
