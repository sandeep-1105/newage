package com.efreightsuite.search;

import com.efreightsuite.dto.SearchRequest;
import com.efreightsuite.dto.SearchRespDto;
import com.efreightsuite.model.Language;
import com.efreightsuite.repository.LanguageRepository;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.efreightsuite.constants.MatcherConstants.LANGUAGE_NAME;
import static com.efreightsuite.constants.MatcherConstants.VERSION_LOCK;

@Service
@Log4j2
public class LanguageSearchService {

    @Autowired
    private LanguageRepository languageRepository;

    public SearchRespDto search(SearchRequest dto) {
        Language probe = new Language();
        probe.setLanguageName(dto.getKeywordNullable());
        ExampleMatcher matcher = ExampleMatcher.matchingAny()
                .withIgnoreCase()
                .withIgnorePaths(VERSION_LOCK)
                .withIgnoreNullValues()
                .withMatcher(LANGUAGE_NAME, ExampleMatcher.GenericPropertyMatcher::startsWith);

        Example<Language> example = Example.of(probe, matcher);

        Sort sortCriteria = new Sort(Sort.Direction.ASC, LANGUAGE_NAME);
        Page<Language> pageResult = languageRepository.findAll(
                example,
                new PageRequest(dto.getSelectedPageNumber(), dto.getRecordPerPage(),
                        sortCriteria)
        );
        List<Language> result = pageResult.getContent();
        return new SearchRespDto(pageResult.getTotalElements(), result);
    }

}
