package com.efreightsuite.search;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import com.efreightsuite.dto.CountryReportConfigSearchDto;
import com.efreightsuite.dto.SearchRespDto;
import com.efreightsuite.model.CountryReportConfigMaster;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class CountryReportConfigSearchImpl {


    @Autowired
    private
    EntityManager em;

    public SearchRespDto search(CountryReportConfigSearchDto dto) {

        String hql = "";

        if (dto.getSearchReportName() != null && dto.getSearchReportName().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }

            hql = hql + " Upper(reportName) like '" + dto.getSearchReportName().trim().toUpperCase() + "%') ";

        }

        if (dto.getSearchCountryName() != null && dto.getSearchCountryName().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }

            hql = hql + " countryMaster.id IN (select cm.id from CountryMaster cm where Upper(cm.countryName) like '" + dto.getSearchCountryName().trim().toUpperCase() + "%') ";

        }

        if (dto.getSearchCopies() != null && dto.getSearchCopies().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }

            hql = hql + " Upper(copies) like '" + dto.getSearchCopies().trim().toUpperCase() + "%') ";

        }

        if (hql.trim().length() != 0) {
            hql = " where " + hql;
        }


        hql = " from CountryReportConfigMaster " + hql;

        if (dto.getSortByColumn() != null && dto.getOrderByType() != null && dto.getSortByColumn().trim().length() != 0 && dto.getOrderByType().trim().length() != 0) {
            hql = hql + " order by " + dto.getSortByColumn().trim() + " " + dto.getOrderByType().trim();
        } else {

            hql = hql + " order by reportName ASC";
        }

        log.info("PageCountry Master Search hql:[" + hql + "]");

        return search(dto.getSelectedPageNumber(), dto.getRecordPerPage(), hql);
    }


    private SearchRespDto search(int selectedPageNumber, int recordPerPage, String hql) {

        String countQ = "Select count (id) " + hql;

        Query countQuery = em.createQuery(countQ);

        Long countTotal = (Long) countQuery.getSingleResult();

        TypedQuery<CountryReportConfigMaster> query = em.createQuery(hql, CountryReportConfigMaster.class);

        query.setFirstResult(selectedPageNumber * recordPerPage);

        query.setMaxResults(recordPerPage);

        List<CountryReportConfigMaster> resultlist = query.getResultList();

        SearchRespDto searchRespDto = new SearchRespDto(countTotal, resultlist);

        log.info(resultlist.size() + " number of rows selected...");

        return searchRespDto;
    }

}
