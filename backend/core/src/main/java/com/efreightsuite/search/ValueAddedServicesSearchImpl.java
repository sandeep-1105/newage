package com.efreightsuite.search;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import com.efreightsuite.dto.SearchRequest;
import com.efreightsuite.dto.SearchRespDto;
import com.efreightsuite.dto.ValueAddedServicesSearchDto;
import com.efreightsuite.model.ValueAddedServices;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class ValueAddedServicesSearchImpl {

    @Autowired
    private
    EntityManager em;


    public SearchRespDto search(SearchRequest dto) {

        String hql = " from ValueAddedServices where ";

        if (dto.getKeyword() != null && dto.getKeyword().trim().length() != 0) {

            hql = hql + " AND ( ";

            hql = hql + " Upper(valueAddeSevicesName) like '" + dto.getKeyword().trim().toUpperCase() + "%' ";

            hql = hql + " OR ";

            hql = hql + " Upper(valueAddeSevicesCode) like '" + dto.getKeyword().trim().toUpperCase() + "%' ";

            hql = hql + " ) ";

        }

        hql = hql + " order by valueAddeSevicesName ASC";

        log.info("ValueAddeSevices Master Search hql:[" + hql + "]");

        return search(dto.getSelectedPageNumber(), dto.getRecordPerPage(), hql);
    }


    public SearchRespDto search(ValueAddedServicesSearchDto dto) {

        String hql = "";

        if (dto.getSearchChargeName() != null && dto.getSearchChargeName().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }

            hql = hql + " chargeMaster.id IN (select cm.id from ChargeMaster cm where Upper(cm.chargeName) like '" + dto.getSearchChargeName().trim().toUpperCase() + "%') ";

        }

        if (dto.getSearchValueAddedSevicesCode() != null && dto.getSearchValueAddedSevicesCode().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }

            hql = hql + " Upper(valueAddedSevicesCode) like '" + dto.getSearchValueAddedSevicesCode().trim().toUpperCase() + "%' ";
        }

        if (dto.getSearchValueAddedSevicesName() != null && dto.getSearchValueAddedSevicesName().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }

            hql = hql + " Upper(valueAddedSevicesName) like '" + dto.getSearchValueAddedSevicesName().trim().toUpperCase() + "%' ";
        }


        if (hql.trim().length() != 0) {
            hql = " where " + hql;
        }

        hql = " from ValueAddedServices " + hql;


        if (dto.getSortByColumn() != null && dto.getOrderByType() != null && dto.getSortByColumn().trim().length() != 0
                && dto.getOrderByType().trim().length() != 0) {
            hql = hql + " order by  Upper(" + dto.getSortByColumn().trim() + ") " + dto.getOrderByType().trim();
        } else {
            hql = hql + " order by Upper(valueAddeSevicesName) ASC";
        }

        log.info("ValueAddeSevices Master Search hql:[" + hql + "]");

        return search(dto.getSelectedPageNumber(), dto.getRecordPerPage(), hql);

    }


    private SearchRespDto search(int selectedPageNumber, int recordPerPage, String hql) {
        String countQ = "Select count (id) " + hql;

        Query countQuery = em.createQuery(countQ);

        Long countTotal = (Long) countQuery.getSingleResult();

        TypedQuery<ValueAddedServices> query = em.createQuery(hql, ValueAddedServices.class);

        query.setFirstResult(selectedPageNumber * recordPerPage);

        query.setMaxResults(recordPerPage);

        List<ValueAddedServices> resultlist = query.getResultList();

        SearchRespDto searchRespDto = new SearchRespDto(countTotal, resultlist);

        log.info("Number of rows selected : " + resultlist.size());

        return searchRespDto;

    }
}
