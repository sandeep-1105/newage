package com.efreightsuite.search;

import java.util.Comparator;
import java.util.List;
import java.util.function.BiPredicate;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import com.efreightsuite.dto.AgentPortMasterSearchDto;
import com.efreightsuite.dto.SearchRespDto;
import com.efreightsuite.model.AgentPortMaster;
import com.efreightsuite.repository.AgentPortMasterRepository;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.stereotype.Service;

import static java.util.stream.Collectors.toList;

@Service
@Log4j2
public class AgentPortMasterSearchImpl {

    @Autowired
    private
    EntityManager em;

    @Autowired
    private AgentPortMasterRepository repository;

    public SearchRespDto search1(AgentPortMasterSearchDto dto) {
        AgentPortMaster probe = new AgentPortMaster();

        ExampleMatcher matcher = ExampleMatcher.matching()
                .withIgnoreCase()
                .withIgnorePaths("versionLock")
                .withIgnoreNullValues();

        Example<AgentPortMaster> example = Example.of(probe, matcher);

        List<AgentPortMaster> allAgentPortMasters = repository.findAll(example);
        BiPredicate<String, String> predicate = (target, toSearch) -> {
            if (toSearch != null
                    && !StringUtils.isBlank(toSearch.trim())) {
                return StringUtils.startsWithIgnoreCase(target, toSearch.trim());
            }
            return true;
        };

        List<AgentPortMaster> allResults = allAgentPortMasters
                .stream()
                .filter(agentPortMaster -> predicate
                        .test(agentPortMaster.getServiceMaster().getServiceName(), dto.getSearchService()))
                .filter(agentPortMaster -> predicate
                        .test(agentPortMaster.getAgent().getPartyName(), dto.getSearchAgent()))
                .filter(agentPortMaster -> predicate
                        .test(agentPortMaster.getPort().getPortName(), dto.getSearchPort()))
                .collect(toList());

        List<AgentPortMaster> pagedResults = allResults.stream()
                .sorted(Comparator.comparing(a -> a.getServiceMaster().getServiceName()))
                .limit(dto.getRecordPerPage())
                .skip((long) dto.getRecordPerPage() * dto.getRecordPerPage())
                .collect(toList());
        return new SearchRespDto((long) allAgentPortMasters.size(), pagedResults);
    }

    public SearchRespDto search(AgentPortMasterSearchDto dto) {

        String hql = "";

        if (dto.getSearchService() != null && dto.getSearchService().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }

            hql = hql + " serviceMaster.id IN (select cm.id from ServiceMaster cm where Upper(cm.serviceName) like '" + dto.getSearchService().trim().toUpperCase() + "%') ";

        }

        if (dto.getSearchAgent() != null && dto.getSearchAgent().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }

            hql = hql + " agent.id IN (select cm.id from PartyMaster cm where Upper(cm.partyName) like '" + dto.getSearchAgent().trim().toUpperCase() + "%') ";

        }

        if (dto.getSearchPort() != null && dto.getSearchPort().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }

            hql = hql + " port.id IN (select cm.id from PortMaster cm where Upper(cm.portName) like '" + dto.getSearchPort().trim().toUpperCase() + "%') ";

        }


        if (hql.trim().length() != 0) {
            hql = " where " + hql;
        }

        hql = " from AgentPortMaster " + hql;

        if (dto.getSortByColumn() != null && dto.getOrderByType() != null && dto.getSortByColumn().trim().length() != 0
                && dto.getOrderByType().trim().length() != 0) {
            hql = hql + " order by Upper(" + dto.getSortByColumn().trim() + ") " + dto.getOrderByType().trim();
        } else {
            hql = hql + " order by Upper(serviceMaster.serviceName) ASC";
        }

        log.info("AgentPort Master Search hql:[" + hql + "]");

        return search(dto.getSelectedPageNumber(), dto.getRecordPerPage(), hql);

    }


    private SearchRespDto search(int selectedPageNumber, int recordPerPage, String hql) {
        String countQ = "Select count (id) " + hql;

        Query countQuery = em.createQuery(countQ);

        Long countTotal = (Long) countQuery.getSingleResult();

        TypedQuery<AgentPortMaster> query = em.createQuery(hql, AgentPortMaster.class);

        query.setFirstResult(selectedPageNumber * recordPerPage);

        query.setMaxResults(recordPerPage);

        List<AgentPortMaster> resultlist = query.getResultList();

        SearchRespDto searchRespDto = new SearchRespDto(countTotal, resultlist);

        log.info("Number of rows selected : " + resultlist.size());

        return searchRespDto;

    }

}
