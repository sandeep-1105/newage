package com.efreightsuite.search;

import java.util.List;

import com.efreightsuite.dto.BankSearchDto;
import com.efreightsuite.dto.SearchRequest;
import com.efreightsuite.dto.SearchRespDto;
import com.efreightsuite.enumeration.LovStatus;
import com.efreightsuite.model.BankMaster;
import com.efreightsuite.repository.BankMasterRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.domain.ExampleMatcher.GenericPropertyMatcher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import static java.util.stream.Collectors.toList;

@Service
public class BankSearchService {

    @Autowired
    private BankMasterRepository repository;

    public SearchRespDto search(BankSearchDto dto) {
        BankMaster probe = new BankMaster();
        probe.setBankCode(dto.getSearchBankCodeNullable());
        probe.setSmartBusinessBankCode(dto.getSearchSmartBusinessBankCodeNullable());
        probe.setBankName(dto.getSearchBankNameNullable());
        probe.setStatus(dto.getSearchStatusNullable());

        ExampleMatcher matcher = ExampleMatcher.matching()
                .withIgnoreCase()
                .withIgnorePaths("versionLock")
                .withIgnoreNullValues()
                .withMatcher("bankCode", GenericPropertyMatcher::startsWith)
                .withMatcher("smartBusinessBankCode", GenericPropertyMatcher::startsWith)
                .withMatcher("bankName", GenericPropertyMatcher::startsWith)
                .withMatcher("status", GenericPropertyMatcher::startsWith);

        Example<BankMaster> example = Example.of(probe, matcher);

        Sort sortCriteria = dto.getSortByColumn() != null && dto.getOrderByType() != null
                ? new Sort(Sort.Direction.valueOf(dto.getOrderByType()), dto.getSortByColumn())
                : new Sort(Sort.Direction.ASC, "bankName");

        Page<BankMaster> pageResult = repository.findAll(
                example,
                new PageRequest(dto.getSelectedPageNumber(), dto.getRecordPerPage(),
                        sortCriteria)
        );
        List<BankMaster> result = pageResult.getContent();
        return new SearchRespDto(pageResult.getTotalElements(), result);
    }

    public SearchRespDto search(SearchRequest dto) {
        BankMaster probe = new BankMaster();
        probe.setBankCode(dto.getKeyword());
        probe.setBankName(dto.getKeyword());

        ExampleMatcher matcher = ExampleMatcher.matchingAny()
                .withIgnoreCase()
                .withIgnoreNullValues()
                .withIgnorePaths("versionLock")
                .withMatcher("bankCode", GenericPropertyMatcher::startsWith)
                .withMatcher("bankName", GenericPropertyMatcher::startsWith);

        Example<BankMaster> example = Example.of(probe, matcher);

        Sort sortCriteria = new Sort(Sort.Direction.ASC, "bankName");

        Page<BankMaster> pageResult = repository.findAll(
                example,
                new PageRequest(dto.getSelectedPageNumber(), dto.getRecordPerPage(),
                        sortCriteria)
        );
        List<BankMaster> result = pageResult.getContent()
                .stream()
                .filter(bank -> bank.getStatus() != LovStatus.Hide)
                .collect(toList());
        return new SearchRespDto(pageResult.getTotalElements(), result);
    }

}
