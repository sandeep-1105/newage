package com.efreightsuite.search;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import com.efreightsuite.dto.SearchRequest;
import com.efreightsuite.dto.SearchRespDto;
import com.efreightsuite.model.common.CommonCurrencyMaster;
import com.efreightsuite.model.common.CommonDocumentTypeMaster;
import com.efreightsuite.model.common.CommonLanguageMaster;
import com.efreightsuite.model.common.CommonTimeZoneMaster;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class PublicSearchImpl {

    @Autowired
    private
    EntityManager em;

    private static String commaSeparatedIds(String ids[]) {
        return StringUtils.join(ids, ",");
    }

    public SearchRespDto searchCommonServiceNotInList(SearchRequest dto) {

        String searchQuery = "SELECT NEW com.efreightsuite.dto.ServiceMasterSearchResponseDto (" +
                "  s.id,  s.serviceCode,  s.serviceName,  s.status)  FROM CommonServiceMaster s ";

        String countQuery = "SELECT COUNT(s.id) FROM CommonServiceMaster s ";

        String filterQuery = "";

        if (dto.getIds() != null && dto.getIds().length > 0) {
            filterQuery = " s.id NOT IN (" + commaSeparatedIds(dto.getIds()) + ")  ";
        }

        if (dto.getKeyword() != null && dto.getKeyword().trim().length() != 0) {

            if (filterQuery.trim().length() != 0) {
                filterQuery += " AND ";
            }

            filterQuery = filterQuery + " (UPPER(s.serviceCode) LIKE '" + dto.getKeyword().trim().toUpperCase() + "%' OR  UPPER(s.serviceName) LIKE '" +
                    dto.getKeyword().trim().toUpperCase() + "%' ) AND s.status != 'Hide' ";

        }

        if (filterQuery.trim().length() != 0) {
            filterQuery = " WHERE " + filterQuery;
        }

        searchQuery = searchQuery + filterQuery + " ORDER BY s.serviceName ASC";

        countQuery = countQuery + filterQuery;

        log.info("Count Query : " + countQuery);

        Query countQ = em.createQuery(countQuery);

        Long countTotal = (Long) countQ.getSingleResult();

        log.info("Count Query Result : " + countTotal);

        Query query = em.createQuery(searchQuery);

        log.info("Search Query : " + query);

        query.setFirstResult(dto.getSelectedPageNumber() * dto.getRecordPerPage());

        query.setMaxResults(dto.getRecordPerPage());

        return new SearchRespDto(countTotal, query.getResultList());
    }

    public SearchRespDto searchCommonDocumentType(SearchRequest dto) {

        String hql = " from CommonDocumentTypeMaster where status!='Hide' ";

        if (dto.getKeyword() != null && dto.getKeyword().trim().length() != 0) {

            hql = hql + " AND ( ";

            hql = hql + " Upper(documentTypeCode) like '" + dto.getKeyword().trim().toUpperCase() + "%' ";

            hql = hql + " OR ";

            hql = hql + " Upper(documentTypeName) like '" + dto.getKeyword().trim().toUpperCase() + "%' ";

            hql = hql + " ) ";

        }

        hql = hql + " order by documentTypeName ASC";

        log.info("searchCommonDocumentType Search hql:[" + hql + "]");

        String countQ = "Select count (id) " + hql;

        Query countQuery = em.createQuery(countQ);

        Long countTotal = (Long) countQuery.getSingleResult();

        TypedQuery<CommonDocumentTypeMaster> query = em.createQuery(hql, CommonDocumentTypeMaster.class);

        query.setFirstResult(dto.getSelectedPageNumber() * dto.getRecordPerPage());

        query.setMaxResults(dto.getRecordPerPage());

        List<CommonDocumentTypeMaster> resultlist = query.getResultList();

        SearchRespDto searchRespDto = new SearchRespDto(countTotal, resultlist);

        log.info("Number of rows selected : " + resultlist.size());

        return searchRespDto;

    }

    public SearchRespDto searchCommonTimeZone(SearchRequest dto) {

        String hql = " from CommonTimeZoneMaster where status!='Hide' ";

        if (dto.getKeyword() != null && dto.getKeyword().trim().length() != 0) {

            hql = hql + " AND ( ";

            hql = hql + " Upper(displayTimeZone) like '" + dto.getKeyword().trim().toUpperCase() + "%' ";

            hql = hql + " ) ";

        }

        hql = hql + " order by displayTimeZone ASC";

        log.info("CommonTimeZone Search hql:[" + hql + "]");

        String countQ = "Select count (id) " + hql;

        Query countQuery = em.createQuery(countQ);

        Long countTotal = (Long) countQuery.getSingleResult();

        TypedQuery<CommonTimeZoneMaster> query = em.createQuery(hql, CommonTimeZoneMaster.class);

        query.setFirstResult(dto.getSelectedPageNumber() * dto.getRecordPerPage());

        query.setMaxResults(dto.getRecordPerPage());

        List<CommonTimeZoneMaster> resultlist = query.getResultList();

        SearchRespDto searchRespDto = new SearchRespDto(countTotal, resultlist);

        log.info("Number of rows selected : " + resultlist.size());

        return searchRespDto;

    }

    public SearchRespDto searchCommonCurrency(SearchRequest dto) {

        String hql = " from CommonCurrencyMaster where status!='Hide' ";

        if (dto.getKeyword() != null && dto.getKeyword().trim().length() != 0) {

            hql = hql + " AND ( ";

            hql = hql + " UPPER(currencyName) like '" + dto.getKeyword().trim().toUpperCase() + "%' ";

            hql = hql + " OR ";

            hql = hql + " UPPER(currencyCode) like '" + dto.getKeyword().trim().toUpperCase() + "%' ";

            hql = hql + " ) ";
        }

        hql = hql + " order by currencyName ASC";

        log.info("Common Currecny Master Search hql:[" + hql + "]");

        String countQ = "Select count (id) " + hql;

        Query countQuery = em.createQuery(countQ);

        Long countTotal = (Long) countQuery.getSingleResult();

        TypedQuery<CommonCurrencyMaster> query = em.createQuery(hql, CommonCurrencyMaster.class);

        query.setFirstResult(dto.getSelectedPageNumber() * dto.getRecordPerPage());

        query.setMaxResults(dto.getRecordPerPage());

        List<CommonCurrencyMaster> resultlist = query.getResultList();

        SearchRespDto searchRespDto = new SearchRespDto(countTotal, resultlist);

        log.info("Number of rows selected : " + resultlist.size());

        return searchRespDto;
    }

    public SearchRespDto searchCommonLanguage(SearchRequest dto) {

        String hql = " from CommonLanguageMaster where status!='Hide' ";

        if (dto.getKeyword() != null && dto.getKeyword().trim().length() != 0) {

            hql = hql + " AND ( ";

            hql = hql + " UPPER(languageName) like '" + dto.getKeyword().trim().toUpperCase() + "%' ";

            hql = hql + " OR ";

            hql = hql + " UPPER(languageCode) like '" + dto.getKeyword().trim().toUpperCase() + "%' ";

            hql = hql + " ) ";
        }

        hql = hql + " order by languageName ASC";

        log.info("Common Language Master Search hql:[" + hql + "]");

        String countQ = "Select count (id) " + hql;

        Query countQuery = em.createQuery(countQ);

        Long countTotal = (Long) countQuery.getSingleResult();

        TypedQuery<CommonLanguageMaster> query = em.createQuery(hql, CommonLanguageMaster.class);

        query.setFirstResult(dto.getSelectedPageNumber() * dto.getRecordPerPage());

        query.setMaxResults(dto.getRecordPerPage());

        List<CommonLanguageMaster> resultlist = query.getResultList();

        SearchRespDto searchRespDto = new SearchRespDto(countTotal, resultlist);

        log.info("Number of rows selected : " + resultlist.size());

        return searchRespDto;
    }


}
