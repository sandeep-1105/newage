package com.efreightsuite.search;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import com.efreightsuite.dto.SearchRequest;
import com.efreightsuite.dto.SearchRespDto;
import com.efreightsuite.model.InBondTypeMaster;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class InBondTypeMasterSearchImpl {


    @Autowired
    private
    EntityManager em;

    public SearchRespDto search(SearchRequest dto) {

        String hql = " from InBondTypeMaster where status!='Hide' ";

        if (dto.getKeyword() != null && dto.getKeyword().trim().length() != 0) {

            hql = hql + " AND ( ";

            hql = hql + " UPPER(inbondType) like '" + dto.getKeyword().trim().toUpperCase() + "%' ";

            hql = hql + " ) ";
        }

        hql = hql + " order by inbondType ASC";

        return search(dto.getSelectedPageNumber(), dto.getRecordPerPage(), hql);

    }

    private SearchRespDto search(int selectedPageNumber, int recordPerPage, String hql) {

        String countQ = "Select count (id) " + hql;

        Query countQuery = em.createQuery(countQ);

        Long countTotal = (Long) countQuery.getSingleResult();


        TypedQuery<InBondTypeMaster> query = em.createQuery(hql, InBondTypeMaster.class);

        query.setFirstResult(selectedPageNumber * recordPerPage);

        query.setMaxResults(recordPerPage);

        List<InBondTypeMaster> resultlist = query.getResultList();

        SearchRespDto searchRespDto = new SearchRespDto(countTotal, resultlist);

        log.info(resultlist.size() + " number of rows selected...");

        return searchRespDto;

    }
}
