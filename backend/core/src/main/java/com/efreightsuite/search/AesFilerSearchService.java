package com.efreightsuite.search;

import java.util.List;

import com.efreightsuite.dto.AesFilerSearchDto;
import com.efreightsuite.dto.SearchRespDto;
import com.efreightsuite.model.AesFilerMaster;
import com.efreightsuite.repository.AesFilerMasterRepository;
import org.apache.commons.lang3.StringUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.domain.ExampleMatcher.GenericPropertyMatcher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import static java.util.stream.Collectors.toList;

@Service
public class AesFilerSearchService {

    @Autowired
    private AesFilerMasterRepository repository;

    public SearchRespDto search(AesFilerSearchDto dto) {
        AesFilerMaster probe = new AesFilerMaster();
        probe.setAesFilerCode(dto.getSearchAesFilerCodeNullable());
        probe.setTransmitterCode(dto.getSearchTrasmitterCodeNullable());
        probe.setSchemaName(dto.getSearchSchemaNameNullable());
        probe.setStatus(dto.getSearchStatusNullable());

        ExampleMatcher matcher = ExampleMatcher.matching()
                .withIgnoreCase()
                .withIgnoreNullValues()
                .withMatcher("aesFilerCode", GenericPropertyMatcher::startsWith)
                .withMatcher("transmitterCode", GenericPropertyMatcher::startsWith)
                .withMatcher("schemaName", GenericPropertyMatcher::startsWith)
                .withMatcher("status", GenericPropertyMatcher::startsWith);

        Example<AesFilerMaster> example = Example.of(probe, matcher);

        Sort sortCriteria = dto.getSortByColumn() != null && dto.getOrderByType() != null
                ? new Sort(Sort.Direction.valueOf(dto.getOrderByType()), dto.getSortByColumn())
                : new Sort(Sort.Direction.ASC, "schemaName");

        Page<AesFilerMaster> pageResult = repository.findAll(
                example,
                new PageRequest(dto.getSelectedPageNumber(), dto.getRecordPerPage(),
                        sortCriteria)
        );
        List<AesFilerMaster> result = pageResult.getContent()
                .stream()
                .filter(aes -> StringUtils.isBlank(dto.getSearchCountryCode())
                        || StringUtils.startsWithIgnoreCase(aes.getCountryMaster().getCountryCode(), dto.getSearchCountryCode()))
                .filter(aes -> StringUtils.isBlank(dto.getSearchLocationName())
                        || StringUtils.startsWithIgnoreCase(aes.getLocationMaster().getLocationName(), dto.getSearchLocationName()))
                .collect(toList());
        return new SearchRespDto(pageResult.getTotalElements(), result);
    }

}
