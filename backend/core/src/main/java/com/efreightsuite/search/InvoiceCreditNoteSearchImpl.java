package com.efreightsuite.search;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;

import com.efreightsuite.activiti.ActivitiTransactionService;
import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.InvoiceSearchDto;
import com.efreightsuite.dto.SearchRequest;
import com.efreightsuite.dto.SearchRespDto;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.model.InvoiceCreditNote;
import com.efreightsuite.model.InvoiceCreditNoteAttachment;
import com.efreightsuite.model.InvoiceCreditNoteDetail;
import com.efreightsuite.model.InvoiceCreditNoteReceipt;
import com.efreightsuite.model.InvoiceCreditNoteTax;
import com.efreightsuite.model.ShipmentServiceDetail;
import com.efreightsuite.repository.InvoiceCreditNoteRepository;
import com.efreightsuite.repository.ShipmentRepository;
import com.efreightsuite.service.DaybookMasterService;
import com.efreightsuite.validation.InvoiceCreditNoteValidator;
import com.efreightsuite.validation.InvoiceDetailValidator;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class InvoiceCreditNoteSearchImpl {

    @Autowired
    private
    EntityManager em;

    @Autowired
    private
    DaybookMasterService daybookMasterService;

    @Autowired
    private
    InvoiceCreditNoteValidator invoiceCreditNoteValidator;

    @Autowired
    private
    ShipmentRepository shipmentRepository;

    @Autowired
    private
    ActivitiTransactionService activitiTransactionService;

    @Autowired
    private
    InvoiceCreditNoteRepository invoiceCreditNoteRepository;
    @Autowired
    private
    InvoiceDetailValidator invoiceDetailValidator;

    public SearchRespDto search(InvoiceSearchDto dto) {
        Map<String, Object> params = new HashMap<>();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

        String hql = "";

        String searchQuery = "select new com.efreightsuite.dto.InvoiceCreditNoteResponseDto ( "
                + " inv.id ,"
                + " da.daybookCode ,"
                + " da.daybookName ,"
                + " ada.daybookCode, "
                + " inv.invoiceCreditNoteNo,"
                + " inv.adjustmentInvoiceNo,"
                + " inv.invoiceCreditNoteDate, "
                + " inv.dueDate, "
                + " inv.masterUid,"
                + " inv.shipmentUid, "
                + " se.serviceUid, "
                + " inv.customerAgent, "
                + " pa.partyName, "
                + " pac.accountCode, "
                + " inv.subledger, "
                + " bc.currencyCode, "
                + " bc.decimalPoint, "
                + " con.locale, "
                + " inv.currencyAmount,"
                + " inv.outstandingAmount,"
                + " inv.localAmount,"
                + " locCur.currencyCode,"
                + " locCur.decimalPoint,"
                + " inv.roe,"
                + " ada.daybookName, "
                + " inv.documentTypeCode, "
                + " locCon.locale, "
                + " inv.invoiceCreditNoteStatus )"
                + " from InvoiceCreditNote inv "
                + " left join inv.daybookMaster da "
                + " left join inv.adjustMentDaybookMaster ada "
                + " left join inv.shipmentServiceDetail se "
                + " left join inv.party pa "
                + " left join inv.partyAccount pac"
                + " left join inv.billingCurrency bc "
                + " left join bc.countryMaster con "
                + " left join inv.localCurrency locCur "
                + " left join locCur.countryMaster locCon ";

        String countQuery = "select count(inv.id)"
                + " from InvoiceCreditNote inv "
                + " left join inv.daybookMaster da "
                + " left join inv.adjustMentDaybookMaster ada "
                + " left join inv.shipmentServiceDetail se "
                + " left join inv.party pa "
                + " left join inv.partyAccount pac"
                + " left join inv.billingCurrency bc "
                + " left join bc.countryMaster con "
                + " left join inv.localCurrency locCur "
                + " left join locCur.countryMaster locCon ";


        if (dto.getDaybookName() != null && dto.getDaybookName().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }
            hql = hql + " UPPER(da.daybookName) like '" + dto.getDaybookName().trim().toUpperCase() + "%' ";

        }

        if (dto.getDaybookCode() != null && dto.getDaybookCode().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }
            hql = hql + " UPPER(da.daybookCode) like '" + dto.getDaybookCode().trim().toUpperCase() + "%' ";

        }
        if (dto.getAdjDaybookCode() != null && dto.getAdjDaybookCode().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }
            hql = hql + " UPPER(ada.daybookCode) like '" + dto.getAdjDaybookCode().trim().toUpperCase() + "%' ";

        }

        if (dto.getInvoiceCreditNoteNo() != null && dto.getInvoiceCreditNoteNo().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }
            hql = hql + " inv.invoiceCreditNoteNo like '" + dto.getInvoiceCreditNoteNo().trim().toUpperCase() + "%' ";

        }

        if (dto.getAdjustmentInvoiceNo() != null && dto.getAdjustmentInvoiceNo().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }
            hql = hql + " inv.adjustmentInvoiceNo like '" + dto.getAdjustmentInvoiceNo().trim().toUpperCase() + "%' ";

        }

        if (dto.getInvoiceDate() != null && dto.getInvoiceDate().getStartDate() != null && dto.getInvoiceDate().getEndDate() != null) {

            try {
                Date dt1 = sdf.parse(dto.getInvoiceDate().getStartDate());
                Date dt2 = sdf.parse(dto.getInvoiceDate().getEndDate());

                if (hql.trim().length() != 0) {
                    hql = hql + " AND ";
                }

                params.put("fromStartDate", dt1);
                params.put("fromEndDate", dt2);
                hql = hql + " ( inv.invoiceCreditNoteDate >= :fromStartDate AND inv.invoiceCreditNoteDate <= :fromEndDate ) ";

            } catch (ParseException e1) {
                log.error("Date Conversion error in invoiceCreditNoteDate: ", e1);
            }


        }

        if (dto.getDueDate() != null && dto.getDueDate().getStartDate() != null && dto.getDueDate().getEndDate() != null) {

            try {
                Date dt1 = sdf.parse(dto.getDueDate().getStartDate());
                Date dt2 = sdf.parse(dto.getDueDate().getEndDate());

                if (hql.trim().length() != 0) {
                    hql = hql + " AND ";
                }

                params.put("fromStartDate", dt1);
                params.put("fromEndDate", dt2);
                hql = hql + " ( inv.dueDate >= :fromStartDate AND inv.dueDate <= :fromEndDate ) ";

            } catch (ParseException e1) {
                log.error("Date Conversion error in dueDate: ", e1);
            }


        }

        if (dto.getMasterUid() != null && dto.getMasterUid().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }
            hql = hql + " UPPER(inv.masterUid) like '" + dto.getMasterUid().trim().toUpperCase() + "%' ";

        }

        if (dto.getShipmentUid() != null && dto.getShipmentUid().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }
            hql = hql + " UPPER(inv.shipmentUid) like '" + dto.getShipmentUid().trim().toUpperCase() + "%' ";

        }


        if (dto.getServiceId() != null && dto.getServiceId().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }
            hql = hql + " UPPER(se.serviceUid) like '" + dto.getServiceId().trim().toUpperCase() + "%' ";

        }


        if (dto.getCustomerAgent() != null && dto.getCustomerAgent().trim().length() != 0) {
            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }

            hql = hql + " UPPER(inv.customerAgent) like '" + dto.getCustomerAgent().trim().toUpperCase() + "%' ";

        }

        //ShipmentServiceId will come null for consol level invoice list.so no need to check null
        if (dto.getShipmentServiceId() != null) {
            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }
            hql = hql + " se.id = " + dto.getShipmentServiceId();
        }

        if (dto.getIsServiceNull() != null && dto.getIsServiceNull()) {
            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }
            hql = hql + " se.id is null";
        }


        if (dto.getPartyName() != null && dto.getPartyName().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }
            hql = hql + " UPPER(pa.partyName) like '" + dto.getPartyName().trim().toUpperCase() + "%' ";

        }

        if (dto.getPartyAccountCode() != null && dto.getPartyAccountCode().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }
            hql = hql + " UPPER(pac.accountCode) like '" + dto.getPartyAccountCode().trim().toUpperCase() + "%' ";

        }

        if (dto.getSubledger() != null && dto.getSubledger().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }
            hql = hql + " UPPER(inv.subledger) like '" + dto.getSubledger().trim().toUpperCase() + "%' ";

        }

        if (dto.getCurrencyCode() != null && dto.getCurrencyCode().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }
            hql = hql + " UPPER(bc.currencyCode) like '" + dto.getCurrencyCode().trim().toUpperCase() + "%' ";

        }

        if (dto.getAmount() != null && dto.getAmount().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }
            hql = hql + " UPPER(inv.currencyAmount) like '" + dto.getAmount().trim().toUpperCase() + "%' ";

        }


        if (dto.getOutstandingAmount() != null && dto.getOutstandingAmount().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }
            hql = hql + " UPPER(inv.outstandingAmount) like '" + dto.getOutstandingAmount().trim().toUpperCase() + "%' ";

        }

        if (dto.getLocalAmount() != null && dto.getLocalAmount().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }
            hql = hql + " UPPER(inv.localAmount) like '" + dto.getLocalAmount().trim().toUpperCase() + "%' ";

        }


        if (dto.getStatus() != null && dto.getStatus().trim().length() != 0) {
            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }

            hql = hql + " UPPER(inv.invoiceCreditNoteStatus) like '" + dto.getStatus().trim().toUpperCase() + "%' ";

        }


        if (dto.getDocumentTypeCode() != null && dto.getDocumentTypeCode().trim().length() != 0) {
            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }

            hql = hql + " UPPER(inv.documentTypeCode) like '" + dto.getDocumentTypeCode().trim().toUpperCase() + "%' ";


        }


        if (dto.getCreditNoteType() != null && dto.getCreditNoteType().trim().length() != 0) {
            if (dto.getCreditNoteType().equals("CRN-REVENUE")) {
                if (hql.trim().length() != 0) {
                    hql = hql + " AND ";
                }

                hql = hql + " inv.adjustmentInvoiceNo is not null ";
            }
            if (dto.getCreditNoteType().equals("CRN-COST")) {
                {
                    if (hql.trim().length() != 0) {
                        hql = hql + " AND ";
                    }

                    hql = hql + " inv.adjustmentInvoiceNo is null ";
                }
            }
        }

        if (hql.trim().length() != 0) {
            hql = " where " + hql;
        }

        String orderByQuery = "";

        if (dto.getSortByColumn() != null && dto.getSortByColumn().length() != 0) {

            if (dto.getSortByColumn().equalsIgnoreCase("daybookCode")) {
                orderByQuery = orderByQuery + "da.daybookCode";
            } else if (dto.getSortByColumn().equalsIgnoreCase("daybookName")) {
                orderByQuery = orderByQuery + "da.daybookName";
            } else if (dto.getSortByColumn().equalsIgnoreCase("adjDaybookCode")) {
                orderByQuery = orderByQuery + "ada.daybookCode";
            } else if (dto.getSortByColumn().equalsIgnoreCase("invoiceCreditNoteNo")) {
                orderByQuery = orderByQuery + "inv.invoiceCreditNoteNo";
            } else if (dto.getSortByColumn().equalsIgnoreCase("adjustmentInvoiceNo")) {
                orderByQuery = orderByQuery + "inv.adjustmentInvoiceNo";
            } else if (dto.getSortByColumn().equalsIgnoreCase("invoiceCreditNoteDate")) {
                orderByQuery = orderByQuery + "inv.invoiceCreditNoteDate";
            } else if (dto.getSortByColumn().equalsIgnoreCase("dueDate")) {
                orderByQuery = orderByQuery + "inv.dueDate";
            } else if (dto.getSortByColumn().equalsIgnoreCase("masterUid")) {
                orderByQuery = orderByQuery + "inv.masterUid";
            } else if (dto.getSortByColumn().equalsIgnoreCase("shipmentUid")) {
                orderByQuery = orderByQuery + "inv.shipmentUid";
            } else if (dto.getSortByColumn().equalsIgnoreCase("serviceUid")) {
                orderByQuery = orderByQuery + "se.serviceUid";
            } else if (dto.getSortByColumn().equalsIgnoreCase("customerAgent")) {
                orderByQuery = orderByQuery + "inv.customerAgent";
            } else if (dto.getSortByColumn().equalsIgnoreCase("partyName")) {
                orderByQuery = orderByQuery + "pa.partyName";
            } else if (dto.getSortByColumn().equalsIgnoreCase("accountCode")) {
                orderByQuery = orderByQuery + "pac.accountCode";
            } else if (dto.getSortByColumn().equalsIgnoreCase("subledger")) {
                orderByQuery = orderByQuery + "inv.subledger";
            } else if (dto.getSortByColumn().equalsIgnoreCase("currencyCode")) {
                orderByQuery = orderByQuery + "bc.currencyCode";
            } else if (dto.getSortByColumn().equalsIgnoreCase("currencyAmount")) {
                orderByQuery = orderByQuery + "inv.currencyAmount";
            } else if (dto.getSortByColumn().equalsIgnoreCase("outstandingAmount")) {
                orderByQuery = orderByQuery + "inv.outstandingAmount";
            } else if (dto.getSortByColumn().equalsIgnoreCase("localAmount")) {
                orderByQuery = orderByQuery + "inv.localAmount";
            } else if (dto.getSortByColumn().equalsIgnoreCase("invoiceCreditNoteStatus")) {
                orderByQuery = orderByQuery + "inv.invoiceCreditNoteStatus";
            }
        }

        if (orderByQuery.length() != 0) {
            orderByQuery = " ORDER BY " + orderByQuery + " " + dto.getOrderByType();
        } else {
            orderByQuery = " order by inv.id DESC ";
        }

        countQuery = countQuery + hql;
        Query countQ = em.createQuery(countQuery);

        log.info("HQL Count Query For Search : " + countQuery);
        log.info("HQL params For Search : " + params);
        for (Entry<String, Object> e : params.entrySet()) {

            countQ.setParameter(e.getKey(), e.getValue());
        }
        Long countTotal = (Long) countQ.getSingleResult();
        log.info("Total Number of Records Found : " + countTotal);

        searchQuery = searchQuery + hql + orderByQuery;
        log.info("HQL Query For Search : " + searchQuery);
        Query query = em.createQuery(searchQuery);

        query.setFirstResult(dto.getSelectedPageNumber() * dto.getRecordPerPage());
        query.setMaxResults(dto.getRecordPerPage());

        for (Entry<String, Object> e : params.entrySet()) {
            query.setParameter(e.getKey(), e.getValue());
        }
        return new SearchRespDto(countTotal, query.getResultList());
    }

    public SearchRespDto searchByDaybook(SearchRequest dto, Long daybookId, String shipmentUid, String consolUid, String shipmentServiceId) {

        String hql = " from InvoiceCreditNote where (adjustmentAmount IS NULL or adjustmentAmount<>netLocalAmount) and documentTypeCode = 'INV' "
                + " and daybookMaster.id=" + daybookId;
        if (shipmentUid != null)
            hql = hql + " and shipmentUid='" + shipmentUid + "'";
        if (consolUid != null)
            hql = hql + " and masterUid='" + consolUid + "'";
        if (shipmentServiceId != null)
            hql = hql + " and shipmentServiceDetail.id='" + shipmentServiceId + "'";
        if (shipmentUid == null && shipmentServiceId == null)
            hql = hql + " and shipmentServiceDetail.id is null ";

        if (dto.getKeyword() != null && dto.getKeyword().trim().length() != 0) {


            hql = hql + " and Upper(invoiceCreditNoteNo) like '" + dto.getKeyword().trim().toUpperCase() + "%' ";


        }


        hql = hql + " order by invoiceCreditNoteNo ASC";


        log.info("Invoice Search hql:[" + hql + "]");


        return search(dto.getSelectedPageNumber(), dto.getRecordPerPage(), hql);


    }

    private SearchRespDto search(int selectedPageNumber, int recordPerPage, String hql) {

        String countQ = "Select count (id) " + hql;

        Query countQuery = em.createQuery(countQ);
        Long countTotal = (Long) countQuery.getSingleResult();


        TypedQuery<InvoiceCreditNote> query = em.createQuery(hql, InvoiceCreditNote.class);

        query.setFirstResult(selectedPageNumber * recordPerPage);

        query.setMaxResults(recordPerPage);

        List<InvoiceCreditNote> resultlist = query.getResultList();


        for (InvoiceCreditNote invoiceCreditNote : resultlist) {
            invoiceCreditNote.setShipmentServiceDetail(null);
            invoiceCreditNote.setInvoiceCreditNoteAttachmentList(null);
            invoiceCreditNote.setInvoiceCreditNoteDetailList(null);
            invoiceCreditNote.setInvoiceCreditNoteReceiptList(null);


        }

        return new SearchRespDto(countTotal, resultlist);
    }

    public List<InvoiceSearchDto> findByShipmentServiceAndConsol(Long shipmentServiceId, String consolUid) {
        InvoiceSearchDto invoiceSearchDto = new InvoiceSearchDto();
        invoiceSearchDto.setMasterUid(consolUid);
        invoiceSearchDto.setShipmentServiceId(shipmentServiceId);
        if (shipmentServiceId == null) {
            invoiceSearchDto.setIsServiceNull(true);
        }
        return (List<InvoiceSearchDto>) search(invoiceSearchDto).getSearchResult();
    }

    public List<InvoiceSearchDto> findByShipmentServiceDetail(Long shipmentServiceId) {
        InvoiceSearchDto invoiceSearchDto = new InvoiceSearchDto();
        invoiceSearchDto.setShipmentServiceId(shipmentServiceId);
        return (List<InvoiceSearchDto>) search(invoiceSearchDto).getSearchResult();
    }

    @Transactional
    public void create(BaseDto baseDto, InvoiceCreditNote invoiceCreditNote) {


        invoiceCreditNoteValidator.validate(invoiceCreditNote);

        invoiceCreditNote.setInvoiceCreditNoteNo(daybookMasterService
                .generateDocumentNo(invoiceCreditNote.getDaybookMaster().getId()));

		/* Invoice Attachment Code Start here */

        if (invoiceCreditNote.getInvoiceCreditNoteAttachmentList() != null
                && invoiceCreditNote.getInvoiceCreditNoteAttachmentList().size() != 0) {
            for (InvoiceCreditNoteAttachment attachment : invoiceCreditNote.getInvoiceCreditNoteAttachmentList()) {

                if (attachment.getFile() == null && attachment.getId() != null) {
                    byte[] arr = invoiceCreditNoteRepository.getByteArray(attachment.getId());
                    if (arr != null) {
                        attachment.setFile(arr);
                    }
                }
                attachment.setId(null);
                attachment.setInvoiceCreditNote(invoiceCreditNote);
            }
        }

		/* Invoice Attachment Code Ends here */

        if (invoiceCreditNote.getInvoiceCreditNoteReceiptList() != null
                && invoiceCreditNote.getInvoiceCreditNoteReceiptList().size() != 0) {
            for (InvoiceCreditNoteReceipt invoiceReceipt : invoiceCreditNote.getInvoiceCreditNoteReceiptList()) {
                invoiceReceipt.setInvoiceCreditNote(invoiceCreditNote);
                invoiceReceipt.setId(null);

            }
        }

        if (invoiceCreditNote.getInvoiceCreditNoteDetailList() != null
                && invoiceCreditNote.getInvoiceCreditNoteDetailList().size() != 0) {
            for (InvoiceCreditNoteDetail invoiceDetail : invoiceCreditNote.getInvoiceCreditNoteDetailList()) {
                invoiceDetailValidator.verifyChargeisAssosiateWithGl(invoiceDetail, baseDto);
                invoiceDetail.setInvoiceCreditNote(invoiceCreditNote);
                ShipmentServiceDetail shipmentServiceDetail = new ShipmentServiceDetail();
                if (invoiceDetail.getShipmentServiceDetail() != null
                        && invoiceDetail.getShipmentServiceDetail().getId() != null) {
                    shipmentServiceDetail.setId(invoiceDetail.getShipmentServiceDetail().getId());
                } else {
                    shipmentServiceDetail = null;
                }
                invoiceDetail.setShipmentServiceDetail(shipmentServiceDetail);

                if (invoiceDetail.getInvoiceCreditNoteTaxList() != null
                        && invoiceDetail.getInvoiceCreditNoteTaxList().size() != 0) {
                    for (InvoiceCreditNoteTax invoiceTax : invoiceDetail.getInvoiceCreditNoteTaxList()) {

                        invoiceTax.setInvoiceCreditNoteDetail(invoiceDetail);
                        invoiceTax.setTaxCode(invoiceTax.getServiceTaxPercentage().getTaxCode());
                    }
                } else {
                    invoiceDetail.setInvoiceCreditNoteTaxList(null);
                }
            }
        }

        // credit note -revenue

        InvoiceCreditNote invoiceRes = new InvoiceCreditNote();
        if (invoiceCreditNote.getAdjustmentInvoiceNo() != null) {
            invoiceCreditNote.getPartyAddress().setId(null);
            for (InvoiceCreditNoteAttachment invoiceCreditNoteAttachment : invoiceCreditNote
                    .getInvoiceCreditNoteAttachmentList()) {
                invoiceCreditNoteAttachment.setId(null);
            }

            for (InvoiceCreditNoteDetail invoiceCreditNoteDetail : invoiceCreditNote
                    .getInvoiceCreditNoteDetailList()) {
                invoiceCreditNoteDetail.setId(null);
                if (invoiceCreditNoteDetail.getInvoiceCreditNoteTaxList() != null
                        && invoiceCreditNoteDetail.getInvoiceCreditNoteTaxList().size() != 0) {
                    for (InvoiceCreditNoteTax invoiceTax : invoiceCreditNoteDetail.getInvoiceCreditNoteTaxList()) {
                        invoiceTax.setInvoiceCreditNoteDetail(invoiceCreditNoteDetail);
                    }

                    if (invoiceCreditNoteDetail.getInvoiceCreditNoteTaxList() != null
                            && invoiceCreditNoteDetail.getInvoiceCreditNoteTaxList().size() != 0) {
                        for (InvoiceCreditNoteTax invoiceTax : invoiceCreditNoteDetail
                                .getInvoiceCreditNoteTaxList()) {
                            invoiceTax.setId(null);
                            invoiceTax.setInvoiceCreditNoteDetail(invoiceCreditNoteDetail);
                            invoiceTax.setTaxCode(invoiceTax.getServiceTaxPercentage().getTaxCode());
                        }
                    } else {
                        invoiceCreditNoteDetail.setInvoiceCreditNoteTaxList(null);
                    }

                }
            }

            for (InvoiceCreditNoteReceipt invoiceCreditNoteReceipt : invoiceCreditNote
                    .getInvoiceCreditNoteReceiptList()) {
                invoiceCreditNoteReceipt.setId(null);
            }

            invoiceCreditNote.setId(null);
            invoiceCreditNote.setOutstandingAmount(0.00);
            invoiceRes = invoiceCreditNoteRepository.save(invoiceCreditNote);

            InvoiceCreditNote mainInvoice = invoiceCreditNoteRepository
                    .findByInvoiceCreditNoteNo(invoiceCreditNote.getAdjustmentInvoiceNo());
            if (mainInvoice.getAdjustmentAmount() == null) {
                mainInvoice.setAdjustmentAmount(0.00);
            }
            if (mainInvoice.getAdjustmentCurrencyAmount() == null) {
                mainInvoice.setAdjustmentCurrencyAmount(0.00);
            }
            mainInvoice.setAdjustmentAmount(mainInvoice.getAdjustmentAmount() + invoiceRes.getNetLocalAmount());
            mainInvoice.setAdjustmentCurrencyAmount(mainInvoice.getAdjustmentCurrencyAmount() + invoiceRes.getNetCurrencyAmount());
            mainInvoice.setOutstandingAmount(mainInvoice.getCurrencyAmount() - (mainInvoice.getAdjustmentCurrencyAmount() + mainInvoice.getAmountReceived()));
            // updating main invoice
            invoiceCreditNoteRepository.save(mainInvoice);

        } else {
            invoiceRes = invoiceCreditNoteRepository.save(invoiceCreditNote);

            if (invoiceCreditNote.getShipmentServiceDetail() != null) {
                ShipmentServiceDetail sd = shipmentRepository.getByService(invoiceCreditNote.getShipmentServiceDetail().getId());

                activitiTransactionService.invoice(sd.getShipment().getId(), sd);
            }

        }

        baseDto.setResponseCode(ErrorCode.SUCCESS);

        InvoiceCreditNote tmp = new InvoiceCreditNote();
        tmp.setId(invoiceRes.getId());
        tmp.setInvoiceCreditNoteNo(invoiceRes.getInvoiceCreditNoteNo());

        baseDto.setResponseObject(tmp);

    }
}
