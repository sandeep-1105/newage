package com.efreightsuite.search;

import com.efreightsuite.dto.CitySearchDto;
import com.efreightsuite.dto.SearchRequest;
import com.efreightsuite.dto.SearchRespDto;
import com.efreightsuite.enumeration.LovStatus;
import com.efreightsuite.model.CityMaster;
import com.efreightsuite.repository.CityMasterRepository;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.efreightsuite.constants.MatcherConstants.*;
import static java.util.stream.Collectors.toList;

@Service
@Log4j2
public class CitySearchService {

    @Autowired
    CityMasterRepository cityMasterRepository;

    public SearchRespDto search(CitySearchDto dto) {

        CityMaster probe = CityMaster.createProbe();
        probe.setCityCode((StringUtils.isNotBlank(dto.getSearchCityCode())) ? dto.getSearchCityCode() : null);
        probe.setCityName((StringUtils.isNotBlank(dto.getSearchCityName())) ? dto.getSearchCityName() : null);
        probe.setStatus((null != dto.getSearchStatus()) ? LovStatus.valueOf(dto.getSearchStatus()) : null);

        ExampleMatcher matcher = ExampleMatcher.matching()
                .withIgnoreCase()
                .withIgnorePaths(VERSION_LOCK)
                .withIgnoreNullValues()
                .withMatcher(CITY_CODE, ExampleMatcher.GenericPropertyMatcher::startsWith)
                .withMatcher(CITY_NAME, ExampleMatcher.GenericPropertyMatcher::startsWith)
                .withMatcher(STATUS, ExampleMatcher.GenericPropertyMatcher::startsWith);

        Example<CityMaster> example = Example.of(probe, matcher);

        Sort sortCriteria = dto.getSortCriteria(CITY_NAME);

        Page<CityMaster> pageResult = cityMasterRepository.findAll(
                example,
                new PageRequest(dto.getSelectedPageNumber(), dto.getRecordPerPage(),
                        sortCriteria)
        );

        List<CityMaster> result = getCitiesFilterByCountryAndStateName(pageResult, dto);

        return new SearchRespDto(pageResult.getTotalElements(), result);
    }

    private List<CityMaster> getCitiesFilterByCountryAndStateName(Page<CityMaster> pageResult, CitySearchDto dto) {
        List<CityMaster> result = null;
        if (StringUtils.isNotBlank(dto.getSearchCountryName()) && StringUtils.isNotBlank(dto.getSearchStateName())) {
            result = pageResult.getContent()
                    .stream()
                    .filter(cityMaster ->
                            cityMaster.getCountryMaster().getCountryName().toUpperCase().startsWith(dto.getSearchCountryName().toUpperCase())
                                    && cityMaster.getStateMaster().getStateName().toUpperCase().startsWith(dto.getSearchStateName().toUpperCase()))
                    .collect(toList());
        } else {
            if (StringUtils.isNotBlank(dto.getSearchCountryName())) {
                result = pageResult.getContent()
                        .stream()
                        .filter(cityMaster -> cityMaster.getCountryMaster().getCountryName().toUpperCase().startsWith(dto.getSearchCountryName().toUpperCase()))
                        .collect(toList());
            } else if (StringUtils.isNotBlank(dto.getSearchStateName())) {
                result = pageResult.getContent()
                        .stream()
                        .filter(cityMaster -> cityMaster.getStateMaster().getStateName().toUpperCase().startsWith(dto.getSearchStateName().toUpperCase()))
                        .collect(toList());
            } else {
                result = pageResult.getContent();
            }
        }
        return result;
    }

    public SearchRespDto search(SearchRequest dto, Long countryId) {

        CityMaster probe = CityMaster.createProbe();
        probe.setCityName(dto.getKeywordNullable());
        probe.setCityCode(dto.getKeywordNullable());

        ExampleMatcher matcher = ExampleMatcher.matchingAny()
                .withIgnoreCase()
                .withIgnorePaths(VERSION_LOCK)
                .withIgnoreNullValues()
                .withMatcher(CITY_NAME, ExampleMatcher.GenericPropertyMatcher::startsWith)
                .withMatcher(CITY_CODE, ExampleMatcher.GenericPropertyMatcher::startsWith);

        Example<CityMaster> example = Example.of(probe, matcher);

        Sort sortCriteria = new Sort(Sort.Direction.ASC, CITY_NAME);
        Page<CityMaster> pageResult = cityMasterRepository.findAll(
                example,
                new PageRequest(dto.getSelectedPageNumber(), dto.getRecordPerPage(),
                        sortCriteria)
        );
        List<CityMaster> result = pageResult.getContent()
                .stream()
                .filter(cityMaster -> cityMaster.getStatus() != LovStatus.Hide)
                .collect(toList());
        if (countryId != null && countryId != -1) {
            result = result
                    .stream()
                    .filter(cityMaster -> cityMaster.getCountryMaster().getId() == countryId)
                    .collect(toList());
        }
        return new SearchRespDto(pageResult.getTotalElements(), result);
    }
}
