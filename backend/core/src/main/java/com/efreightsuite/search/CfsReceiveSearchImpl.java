package com.efreightsuite.search;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.efreightsuite.dto.CfsReceiveSearchRequestDto;
import com.efreightsuite.dto.SearchRespDto;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class CfsReceiveSearchImpl {

    @Autowired
    private
    EntityManager em;

    private final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:SS");


    public SearchRespDto search(CfsReceiveSearchRequestDto dto) {
        Map<String, Object> params = new HashMap<>();

        log.info("CfsReceiveSearchImpl.search() method is called...");

        String searchQuery = "select new com.efreightsuite.dto.CfsReceiveSearchResponseDto"
                + " (cfs.id, cfs.cfsReceiptNumber, cfs.cfsReceiptDate, cfs.cfsMaster.cfsName, "
                + "  ship.shipmentUid, cfs.proNumber, cfs.proDate, cfs.shipper.partyName, "
                + "  cfs.consignee.partyName, cfs.shipperRefNumber, cfs.onHand, cfs.transporter.partyName) "
                + " from CfsReceiveEntry cfs "
                + " left join cfs.cfsMaster c"
                + " left join cfs.shipper ship "
                + " left join cfs.consignee cons "
                + " left join cfs.transporter trans"
                + " left join cfs.shipmentServiceDetail ship";

        String countQuery = "select count(cfs.id) from CfsReceiveEntry cfs "
                + " left join cfs.cfsMaster c"
                + " left join cfs.shipper ship "
                + " left join cfs.consignee cons "
                + " left join cfs.transporter trans"
                + " left join cfs.shipmentServiceDetail ship";


        String filterQuery = "";

        if (dto.getSearchReceiptNumber() != null && dto.getSearchReceiptNumber().length() != 0) {

            if (filterQuery.trim().length() != 0) {
                filterQuery = filterQuery + " AND ";
            }

            filterQuery = filterQuery + " Upper(cfs.cfsReceiptNumber) like '" + dto.getSearchReceiptNumber().trim().toUpperCase() + "%' ";
        }

        if (dto.getSearchReceiptDate() != null && dto.getSearchReceiptDate().getStartDate() != null && dto.getSearchReceiptDate().getEndDate() != null) {

            try {

                Date date1 = sdf.parse(dto.getSearchReceiptDate().getStartDate());
                Date date2 = sdf.parse(dto.getSearchReceiptDate().getEndDate());

                if (filterQuery.trim().length() != 0) {
                    filterQuery = filterQuery + " AND ";
                }

                params.put("startDate", date1);
                params.put("endDate", date2);

                filterQuery = filterQuery + "( cfs.cfsReceiptDate  >= :startDate AND cfs.cfsReceiptDate  <= :endDate )";

            } catch (ParseException exception) {
                log.error("Date Conversion Exception in CFSReceiptDate....", exception);
            }

        }

        if (dto.getSearchCSF() != null && dto.getSearchCSF().length() != 0) {

            if (filterQuery.trim().length() != 0) {
                filterQuery = filterQuery + " AND ";
            }

            filterQuery = filterQuery + " Upper(cfs.cfsMaster.cfsName) like '" + dto.getSearchCSF().trim().toUpperCase() + "%' ";
        }

        if (dto.getSearchShipmentUid() != null && dto.getSearchShipmentUid().length() != 0) {

            if (filterQuery.trim().length() != 0) {
                filterQuery = filterQuery + " AND ";
            }

            filterQuery = filterQuery + " Upper(ship.shipmentUid) like '" + dto.getSearchShipmentUid().trim().toUpperCase() + "%' ";
        }

        if (dto.getSearchProNumber() != null && dto.getSearchProNumber().length() != 0) {

            if (filterQuery.trim().length() != 0) {
                filterQuery = filterQuery + " AND ";
            }

            filterQuery = filterQuery + " Upper(cfs.proNumber) like '" + dto.getSearchProNumber().trim().toUpperCase() + "%' ";
        }

        if (dto.getSearchProDate() != null && dto.getSearchProDate().getStartDate() != null && dto.getSearchProDate().getEndDate() != null) {

            try {

                Date date1 = sdf.parse(dto.getSearchProDate().getStartDate());
                Date date2 = sdf.parse(dto.getSearchProDate().getEndDate());

                if (filterQuery.trim().length() != 0) {
                    filterQuery = filterQuery + " AND ";
                }

                params.put("start", date1);
                params.put("end", date2);

                filterQuery = filterQuery + "( cfs.proDate  >= :start AND cfs.proDate  <= :end )";

            } catch (ParseException exception) {
                log.error("Date Conversion Exception in proDate....", exception);
            }

        }

        if (dto.getSearchShipper() != null && dto.getSearchShipper().length() != 0) {

            if (filterQuery.trim().length() != 0) {
                filterQuery = filterQuery + " AND ";
            }

            filterQuery = filterQuery + " Upper(cfs.shipper.partyName) like '" + dto.getSearchShipper().trim().toUpperCase() + "%' ";
        }

        if (dto.getSearchConsignee() != null && dto.getSearchConsignee().length() != 0) {

            if (filterQuery.trim().length() != 0) {
                filterQuery = filterQuery + " AND ";
            }

            filterQuery = filterQuery + " Upper(cfs.consignee.partyName) like '" + dto.getSearchConsignee().trim().toUpperCase() + "%' ";
        }

        if (dto.getSearchShipperRefNumber() != null && dto.getSearchShipperRefNumber().length() != 0) {

            if (filterQuery.trim().length() != 0) {
                filterQuery = filterQuery + " AND ";
            }

            filterQuery = filterQuery + " Upper(cfs.shipperRefNumber) like '" + dto.getSearchShipperRefNumber().trim().toUpperCase() + "%' ";
        }

        if (dto.getSearchOnHand() != null && dto.getSearchOnHand().length() != 0) {

            if (filterQuery.trim().length() != 0) {
                filterQuery = filterQuery + " AND ";
            }

            filterQuery = filterQuery + " Upper(cfs.onHand) like '" + dto.getSearchOnHand().trim().toUpperCase() + "%' ";
        }


        if (dto.getSearchTrucker() != null && dto.getSearchTrucker().length() != 0) {

            if (filterQuery.trim().length() != 0) {
                filterQuery = filterQuery + " AND ";
            }

            filterQuery = filterQuery + " Upper(cfs.transporter.partyName) like '" + dto.getSearchTrucker().trim().toUpperCase() + "%' ";
        }

        String advancedFilterQuery = "";

        if (dto.getSearchValue() != null && dto.getSearchValue().length() != 0) {

            if (dto.getSearchName() != null && dto.getSearchName().trim().equals("Shipper Ref No")) {

                advancedFilterQuery = " WHERE Upper(cfs.shipperRefNumber) like '" + dto.getSearchValue().trim().toUpperCase() + "%' ";

                if (dto.getSearchShipperRefNumber() != null && dto.getSearchShipperRefNumber().trim().length() != 0) {

                    advancedFilterQuery = advancedFilterQuery + " AND Upper(cfs.shipperRefNumber) like '" + dto.getSearchShipperRefNumber().trim().toUpperCase() + "%'";

                }
            } else if (dto.getSearchName() != null && dto.getSearchName().trim().equals("On Hand")) {

                advancedFilterQuery = " WHERE Upper(cfs.onHand) like '" + dto.getSearchValue().trim().toUpperCase() + "%' ";

                if (dto.getSearchOnHand() != null && dto.getSearchOnHand().trim().length() != 0) {

                    advancedFilterQuery = advancedFilterQuery + " AND Upper(cfs.onHand) like '" + dto.getSearchOnHand().trim().toUpperCase() + "%'";

                }
            } else if (dto.getSearchName() != null && dto.getSearchName().trim().equals("Trucker")) {

                advancedFilterQuery = " WHERE Upper(cfs.transporter.partyName) like '" + dto.getSearchValue().trim().toUpperCase() + "%' ";

                if (dto.getSearchTrucker() != null && dto.getSearchTrucker().trim().length() != 0) {

                    advancedFilterQuery = advancedFilterQuery + " AND Upper(cfs.transporter.partyName) like '" + dto.getSearchTrucker().trim().toUpperCase() + "%'";

                }
            }


            if (filterQuery.trim().length() != 0) {
                advancedFilterQuery = advancedFilterQuery + " AND " + filterQuery;
            }


        } else {
            if (filterQuery.trim().length() != 0) {
                filterQuery = " WHERE " + filterQuery;
            }
        }

        if (advancedFilterQuery.trim().length() != 0) {
            filterQuery = advancedFilterQuery;
        }


        // Building Order By Query

        String orderByQuery = "";

        if (dto.getSortByColumn() != null && dto.getSortByColumn().length() != 0) {

            if (dto.getSortByColumn().equalsIgnoreCase("receiptNumber")) {
                orderByQuery = orderByQuery + "cfs.cfsReceiptNumber";
            } else if (dto.getSortByColumn().equalsIgnoreCase("receiptDate")) {
                orderByQuery = orderByQuery + "cfs.cfsReceiptDate";
            } else if (dto.getSortByColumn().equalsIgnoreCase("cfsMasterName")) {
                orderByQuery = orderByQuery + "cfs.cfsMaster.cfsName";
            } else if (dto.getSortByColumn().equalsIgnoreCase("shipmentUid")) {
                orderByQuery = orderByQuery + "ship.shipmentUid";
            } else if (dto.getSortByColumn().equalsIgnoreCase("proNumber")) {
                orderByQuery = orderByQuery + "cfs.proNumber";
            } else if (dto.getSortByColumn().equalsIgnoreCase("proDate")) {
                orderByQuery = orderByQuery + "cfs.proDate";
            } else if (dto.getSortByColumn().equalsIgnoreCase("proDate")) {
                orderByQuery = orderByQuery + "cfs.proDate";
            } else if (dto.getSortByColumn().equalsIgnoreCase("shipperName")) {
                orderByQuery = orderByQuery + "cfs.shipper.partyName";
            } else if (dto.getSortByColumn().equalsIgnoreCase("consigneeName")) {
                orderByQuery = orderByQuery + "cfs.consignee.partyName";
            } else if (dto.getSortByColumn().equalsIgnoreCase("shipperRefNumber")) {
                orderByQuery = orderByQuery + "cfs.shipperRefNumber";
            } else if (dto.getSortByColumn().equalsIgnoreCase("onHand")) {
                orderByQuery = orderByQuery + "cfs.onHand";
            } else if (dto.getSortByColumn().equalsIgnoreCase("truckerName")) {
                orderByQuery = orderByQuery + "cfs.transporter.partyName";
            } else {
                orderByQuery = orderByQuery + "cfs.cfsReceiptNumber";
            }
        }

        if (orderByQuery.length() != 0) {
            orderByQuery = " ORDER BY Upper(" + orderByQuery + ") " + dto.getOrderByType();
        } else {
            orderByQuery = " ORDER BY Upper(cfs.cfsReceiptNumber) DESC ";
        }


        countQuery = countQuery + filterQuery;
        Query countQ = em.createQuery(countQuery);

        log.info("HQL Count Query For Search : " + countQuery);
        log.info("HQL params For Search : " + params);
        for (Entry<String, Object> e : params.entrySet()) {

            countQ.setParameter(e.getKey(), e.getValue());
        }
        Long countTotal = (Long) countQ.getSingleResult();
        log.info("Total Number of Records Found : " + countTotal);


        searchQuery = searchQuery + filterQuery + orderByQuery;
        log.info("HQL Query For Search : " + searchQuery);
        Query query = em.createQuery(searchQuery);

        query.setFirstResult(dto.getSelectedPageNumber() * dto.getRecordPerPage());
        query.setMaxResults(dto.getRecordPerPage());

        for (Entry<String, Object> e : params.entrySet()) {
            query.setParameter(e.getKey(), e.getValue());
        }


        return new SearchRespDto(countTotal, query.getResultList());
    }

}
