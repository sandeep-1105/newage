package com.efreightsuite.search;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import com.efreightsuite.dto.GeneralNoteMasterSearchDto;
import com.efreightsuite.dto.SearchRespDto;
import com.efreightsuite.model.GeneralNoteMaster;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class GeneralNoteSearchImpl {

    @Autowired
    private
    EntityManager em;

    public SearchRespDto search(GeneralNoteMasterSearchDto dto) {

        String hql = "";

        if (dto.getSearchServiceName() != null && dto.getSearchServiceName().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }

            hql = hql + " serviceMaster.id IN (select cm.id from ServiceMaster cm where Upper(cm.serviceName) like '" + dto.getSearchServiceName().trim().toUpperCase() + "%') ";

        }

        if (dto.getSearchLocationName() != null && dto.getSearchLocationName().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }

            hql = hql + " locationMaster.id IN (select cm.id from LocationMaster cm where Upper(cm.locationName) like '" + dto.getSearchLocationName().trim().toUpperCase() + "%') ";

        }

        if (dto.getSearchCategory() != null && dto.getSearchCategory().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }

            hql = hql + " UPPER(category) like '" + dto.getSearchCategory().trim().toUpperCase() + "%' ";
        }
        if (dto.getSearchSubCategory() != null && dto.getSearchSubCategory().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }

            hql = hql + " UPPER(subCategory) like '" + dto.getSearchSubCategory().trim().toUpperCase() + "%' ";
        }

        if (hql.trim().length() != 0) {
            hql = " where " + hql;
        }


        hql = " from GeneralNoteMaster " + hql;

        if (dto.getSortByColumn() != null && dto.getOrderByType() != null && dto.getSortByColumn().trim().length() != 0 && dto.getOrderByType().trim().length() != 0) {
            hql = hql + " order by Upper(" + dto.getSortByColumn().trim() + ") " + dto.getOrderByType().trim();
        } else {

            hql = hql + " order by Upper(category) ASC";
        }

        log.info("GeneralNote Master Search hql:[" + hql + "]");

        return search(dto.getSelectedPageNumber(), dto.getRecordPerPage(), hql);
    }

    private SearchRespDto search(int selectedPageNumber, int recordPerPage, String hql) {

        String countQ = "Select count (id) " + hql;

        Query countQuery = em.createQuery(countQ);

        Long countTotal = (Long) countQuery.getSingleResult();

        TypedQuery<GeneralNoteMaster> query = em.createQuery(hql, GeneralNoteMaster.class);

        query.setFirstResult(selectedPageNumber * recordPerPage);

        query.setMaxResults(recordPerPage);

        List<GeneralNoteMaster> resultlist = query.getResultList();

        SearchRespDto searchRespDto = new SearchRespDto(countTotal, resultlist);

        log.info(resultlist.size() + " number of rows selected...");

        return searchRespDto;
    }

}
