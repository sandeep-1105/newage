package com.efreightsuite.search;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import com.efreightsuite.dto.AutoMailMasterSearchDto;
import com.efreightsuite.dto.SearchRequest;
import com.efreightsuite.dto.SearchRespDto;
import com.efreightsuite.model.AutoMailMaster;
import com.efreightsuite.repository.AutoMailMasterRepository;
import com.efreightsuite.service.AutoMailMasterService;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class AutoMailMasterSearchImpl {

    @Autowired
    private
    EntityManager em;

    @Autowired
    AutoMailMasterService autoMailMasterService;

    @Autowired
    private
    AutoMailMasterRepository autoMailMasterRepository;

    public SearchRespDto search(AutoMailMasterSearchDto dto) {
        log.info("AutoMailMasterSearchImpl.search method started");

        String hql = "";

        if (dto.getSearchMessageName() != null && dto.getSearchMessageName().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }

            hql = hql + " Upper(messageName) like '" + dto.getSearchMessageName().trim().toUpperCase() + "%' ";
        }

        if (dto.getSearchMessageCode() != null && dto.getSearchMessageCode().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }

            hql = hql + " Upper(messageCode) like '" + dto.getSearchMessageCode().trim().toUpperCase() + "%' ";
        }

        if (dto.getSearchMessageSendType() != null && dto.getSearchMessageSendType().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }

            hql = hql + " Upper(messageSendType) like '" + dto.getSearchMessageSendType().trim().toUpperCase() + "%' ";
        }

        if (dto.getSearchStatus() != null && dto.getSearchStatus().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }

            hql = hql + " Upper(status) like '" + dto.getSearchStatus().trim().toUpperCase() + "%' ";
        }


        if (dto.getSearchMessageGroupName() != null && dto.getSearchMessageGroupName().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }

            hql = hql
                    + " autoMailGroupMaster.id IN (select am.id from AutoMailGroupMaster am where Upper(am.messageGroupName) like '"
                    + dto.getSearchMessageGroupName().trim().toUpperCase() + "%') ";
        }

        if (hql.trim().length() != 0) {
            hql = " where " + hql;
        }

        hql = " from AutoMailMaster " + hql;

        if (dto.getSortByColumn() != null && dto.getOrderByType() != null && dto.getSortByColumn().trim().length() != 0
                && dto.getOrderByType().trim().length() != 0) {

            if (dto.getSortByColumn().toUpperCase().startsWith("TRANS")) {
                hql = hql + " order by Upper(autoMailGroupMaster.messageGroupName) " + dto.getOrderByType().trim();
            } else {
                hql = hql + " order by Upper(" + dto.getSortByColumn().trim() + ") " + dto.getOrderByType().trim();
            }


        } else {
            hql = hql + " order by Upper(messageName) ASC";
        }

        log.info("AutoMailMaster Search hql:[" + hql + "]");

        return search(dto.getSelectedPageNumber(), dto.getRecordPerPage(), hql);
    }

    public SearchRespDto search(SearchRequest dto) {
        log.info("AutoMailMasterSearchImpl.search method started");


        String hql = " from AutoMailMaster ";

        if (dto.getKeyword() != null && dto.getKeyword().trim().length() != 0) {

            hql = hql + " where ";

            hql = hql + " Upper(messageCode) like '" + dto.getKeyword().trim().toUpperCase()
                    + "%' ";

            hql = hql + " OR ";

            hql = hql + " Upper(messageName) like '" + dto.getKeyword().trim().toUpperCase()
                    + "%' ";

            hql = hql + " ) ";

        }

        hql = hql + " order by messageCode ASC";

        log.info("AutoMailMaster Search hql:[" + hql + "]");

        return search(dto.getSelectedPageNumber(), dto.getRecordPerPage(), hql);

    }

    public SearchRespDto searchByExternal(SearchRequest dto) {
        log.info("AutoMailMasterSearchImpl.search method started");


        String hql = " from AutoMailMaster where messageSendType!='Internal' ";

        if (dto.getKeyword() != null && dto.getKeyword().trim().length() != 0) {

            hql = hql + " and ";

            hql = hql + " Upper(messageCode) like '" + dto.getKeyword().trim().toUpperCase()
                    + "%' ";

            hql = hql + " OR ";

            hql = hql + " Upper(messageName) like '" + dto.getKeyword().trim().toUpperCase()
                    + "%' ";

            hql = hql + " ) ";

        }

        hql = hql + " order by messageCode ASC";

        log.info("AutoMailMaster Search hql:[" + hql + "]");

        return search(dto.getSelectedPageNumber(), dto.getRecordPerPage(), hql);

    }

    private SearchRespDto search(int selectedPageNumber, int recordPerPage, String hql) {
        String countQ = "Select count (id) " + hql;
        Query countQuery = em.createQuery(countQ);

        Long countTotal = (Long) countQuery.getSingleResult();

        TypedQuery<AutoMailMaster> query = em.createQuery(hql, AutoMailMaster.class);

        query.setFirstResult(selectedPageNumber * recordPerPage);

        query.setMaxResults(recordPerPage);

        List<AutoMailMaster> resultlistRes = query.getResultList();


        if (resultlistRes != null) {
            for (AutoMailMaster autoMail : resultlistRes) {

                autoMail.setTransAutoMailGroupMaster(autoMailMasterRepository.getAutoGroupId(autoMail.getId()));

                log.info("autoMailautoMail: " + autoMail.getAutoMailGroupMaster());

                if (autoMail.getTransAutoMailGroupMaster() != null) {

                    autoMail.getTransAutoMailGroupMaster().setAutoMailMasterList(null);
                }

                log.info("autoMail :" + autoMail.getId());
            }

        }


        SearchRespDto searchRespDto = new SearchRespDto(countTotal, resultlistRes);

        log.info("AutoMailMasterSearchImpl.search method ended");
        return searchRespDto;
    }

}
