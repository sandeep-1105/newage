package com.efreightsuite.search;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import com.efreightsuite.dto.SearchRequest;
import com.efreightsuite.dto.SearchRespDto;
import com.efreightsuite.model.PartyAccountMaster;
import com.efreightsuite.model.PartyMaster;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class PartyAccountSearchImpl {

    @Autowired
    private
    EntityManager em;

    public SearchRespDto search(SearchRequest dto, String partyId) {

        String hql = " from PartyAccountMaster pa where pa.status!='Hide' AND pa.partyMaster.id='" + partyId + "' ";

        if (dto.getKeyword() != null && dto.getKeyword().trim().length() != 0) {

            hql = hql + " AND ( ";

            hql = hql + " upper(pa.accountCode) like '" + dto.getKeyword().trim().toUpperCase() + "%' ";

            hql = hql + " ) ";
        }

        hql = hql + " order by pa.accountCode ASC";

        log.info("Party Account Master Search hql:[" + hql + "]");

        return search(dto.getSelectedPageNumber(), dto.getRecordPerPage(), hql);

    }

    public SearchRespDto searchPartyAccountNotInParty(SearchRequest dto, Long partyId) {

        String hql = " from PartyAccountMaster pa where pa.status!='Hide' ";

        if (partyId != null && partyId > 0) {
            hql = hql + " AND pa.partyMaster.id !='" + partyId + "' ";
        }

        if (dto.getKeyword() != null && dto.getKeyword().trim().length() != 0) {

            hql = hql + " AND ( ";

            hql = hql + " upper(pa.partyMaster.partyName) like '" + dto.getKeyword().trim().toUpperCase() + "%' ";

            hql = hql + " ) ";
        }

        hql = hql + " order by pa.partyMaster.partyName ASC";

        log.info("Party Account Master Search hql:[" + hql + "]");

        return search(dto.getSelectedPageNumber(), dto.getRecordPerPage(), hql);

    }


    private SearchRespDto search(int selectedPageNumber, int recordPerPage, String hql) {

        Query countQuery = em.createQuery("Select count (id) " + hql);

        Long countTotal = (Long) countQuery.getSingleResult();

        TypedQuery<PartyAccountMaster> query = em.createQuery(hql, PartyAccountMaster.class);

        query.setFirstResult(selectedPageNumber * recordPerPage);

        query.setMaxResults(recordPerPage);

        List<PartyAccountMaster> resultList = query.getResultList();

        for (PartyAccountMaster pa : resultList) {
            pa.setTmpPartyMaster(new PartyMaster());
            pa.getTmpPartyMaster().setId(pa.getPartyMaster().getId());
            pa.getTmpPartyMaster().setPartyCode(pa.getPartyMaster().getPartyCode());
            pa.getTmpPartyMaster().setPartyName(pa.getPartyMaster().getPartyName());
            pa.getTmpPartyMaster().setStatus(pa.getPartyMaster().getStatus());
        }

        log.info("Number of rows selected " + resultList.size());

        return new SearchRespDto(countTotal, resultList);
    }

}
