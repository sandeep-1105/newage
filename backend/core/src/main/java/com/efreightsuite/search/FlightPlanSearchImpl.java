package com.efreightsuite.search;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import com.efreightsuite.dto.FlightPlanSearchDto;
import com.efreightsuite.dto.SearchRespDto;
import com.efreightsuite.model.FlightPlan;
import com.efreightsuite.util.TimeUtil;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class FlightPlanSearchImpl {

    @Autowired
    private
    EntityManager em;

    private final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:SS");

    private static String commaSeparatedIds(String ids[]) {
        return StringUtils.join(ids, ",");
    }

    public SearchRespDto search(FlightPlanSearchDto dto) {

        Map<String, Object> params = new HashMap<>();

        String hql = "";

        if (dto.getScheduleId() != null && dto.getScheduleId().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }
            hql = hql + " UPPER(scheduleId) like '" + dto.getScheduleId().trim().toUpperCase() + "%' ";

        }

        if (dto.getPod() != null && dto.getPod().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }
            hql = hql + " Upper(pod.portName) like '" + dto.getPod().trim().toUpperCase() + "%' ";
        }

        if (dto.getPol() != null && dto.getPol().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }
            hql = hql + " Upper(pol.portName) like '" + dto.getPol().trim().toUpperCase() + "%' ";
        }

        if (dto.getCarrier() != null && dto.getCarrier().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }
            hql = hql + " Upper(carrierMaster.carrierName) like '" + dto.getCarrier().trim().toUpperCase() + "%' ";
        }

        if (dto.getEtd() != null && dto.getEtd().getStartDate() != null && dto.getEtd().getEndDate() != null) {

            try {
                Date dt1 = sdf.parse(dto.getEtd().getStartDate());
                Date dt2 = sdf.parse(dto.getEtd().getEndDate());

                if (hql.trim().length() != 0) {
                    hql = hql + " AND ";
                }

                params.put("etdStartDate", dt1);
                params.put("etdEndDate", dt2);

                hql = hql + " ( etd >= :etdStartDate AND etd <= :etdEndDate )  ";

            } catch (ParseException e1) {
                log.error("Date Conversion error in etd: ", e1);
            }

        }

        if (dto.getEta() != null && dto.getEta().getStartDate() != null && dto.getEta().getEndDate() != null) {

            try {
                Date dt1 = sdf.parse(dto.getEta().getStartDate());
                Date dt2 = sdf.parse(dto.getEta().getEndDate());

                if (hql.trim().length() != 0) {
                    hql = hql + " AND ";
                }

                params.put("etaStartDate", dt1);
                params.put("etaEndDate", dt2);

                hql = hql + " ( eta >= :etaStartDate AND eta <= :etaEndDate )  ";

            } catch (ParseException e1) {
                log.error("Date Conversion error in eta: ", e1);
            }

        }

        if (dto.getCapacityReserved() != null && dto.getCapacityReserved().trim().length() != 0) {
            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }

            hql = hql + " UPPER(capacityReserved) like '" + dto.getCapacityReserved().trim().toUpperCase() + "%' ";

        }

        if (dto.getStatus() != null && dto.getStatus().trim().length() != 0) {
            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }

            hql = hql + " UPPER(flightPlanStatus) like '" + dto.getStatus().trim().toUpperCase() + "%' ";

        }

        if (dto.getFlightNo() != null && dto.getFlightNo().trim().length() != 0) {
            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }
            hql = hql + " Upper(flightNo) like '" + dto.getFlightNo().trim().toUpperCase() + "%' ";

        }

        if (dto.getCarrierId() != null) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }
            hql = hql + " carrierMaster.id = '" + dto.getCarrierId() + "' ";

        }

        if (dto.getPorId() != null) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }
            hql = hql + " pol.id = '" + dto.getPorId() + "' ";

        }

        if (dto.getIds() != null && dto.getIds().length > 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }

            hql = hql + " id NOT IN (" + commaSeparatedIds(dto.getIds()) + ")  ";

        }


        if (dto.getDate() != null) {
            Date dt1 = dto.getDate();

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }
            params.put("etd", dt1);
            hql = hql + " etd >= :etd ";
        }

        if (dto.getDirectStopOver() != null && dto.getDirectStopOver().trim().length() != 0) {
            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }
            hql = hql + " Upper(directStopOver) like '" + dto.getDirectStopOver().trim().toUpperCase() + "%' ";

        }

        if (hql.trim().length() != 0) {
            hql = " where " + hql;
        }

        hql = " from FlightPlan " + hql;

        if (dto.getSortByColumn() != null && dto.getOrderByType() != null && dto.getSortByColumn().trim().length() != 0
                && dto.getOrderByType().trim().length() != 0) {

            hql = hql + " order by " + dto.getSortByColumn().trim() + " " + dto.getOrderByType().trim();

        } else {
            hql = hql + " order by id DESC";
        }

        log.info("FlightPlan Search hql:[" + hql + "]");
        String countQ = "Select count (id) " + hql;

        Query countQuery = em.createQuery(countQ);
        for (Entry<String, Object> e : params.entrySet()) {
            countQuery.setParameter(e.getKey(), e.getValue());
        }
        Long countTotal = (Long) countQuery.getSingleResult();

        TypedQuery<FlightPlan> query = em.createQuery(hql, FlightPlan.class);

        for (Entry<String, Object> e : params.entrySet()) {
            query.setParameter(e.getKey(), e.getValue());
        }

        query.setFirstResult(dto.getSelectedPageNumber() * dto.getRecordPerPage());

        query.setMaxResults(dto.getRecordPerPage());

        List<FlightPlan> resultlist = query.getResultList();

        log.info("Number of rows selected : " + resultlist.size());

        return new SearchRespDto(countTotal, resultlist);

    }

    public SearchRespDto flightPlanSearch(FlightPlanSearchDto dto) {

        Map<String, Object> params = new HashMap<>();

        String hql = "";

        if (dto.getScheduleId() != null && dto.getScheduleId().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }
            hql = hql + " UPPER(scheduleId) like '" + dto.getScheduleId().trim().toUpperCase() + "%' ";

        }

        if (dto.getPod() != null && dto.getPod().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }
            hql = hql + " Upper(pod.portName) like '" + dto.getPod().trim().toUpperCase() + "%' ";
        }

        if (dto.getPol() != null && dto.getPol().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }
            hql = hql + " Upper(pol.portName) like '" + dto.getPol().trim().toUpperCase() + "%' ";
        }

        if (dto.getCarrier() != null && dto.getCarrier().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }
            hql = hql + " Upper(carrierMaster.carrierName) like '" + dto.getCarrier().trim().toUpperCase() + "%' ";
        }

        if (dto.getEtd() != null && dto.getEtd().getStartDate() != null && dto.getEtd().getEndDate() != null) {

            try {
                Date dt1 = sdf.parse(dto.getEtd().getStartDate());
                Date dt2 = sdf.parse(dto.getEtd().getEndDate());

                if (hql.trim().length() != 0) {
                    hql = hql + " AND ";
                }

                params.put("etdStartDate", dt1);
                params.put("etdEndDate", dt2);

                hql = hql + " ( etd >= :etdStartDate AND etd <= :etdEndDate )  ";

            } catch (ParseException e1) {
                log.error("Date Conversion error in etd: ", e1);
            }

        } else {
            Date dt = TimeUtil.getCurrentLocationTime();
            dt.setHours(0);
            dt.setMinutes(0);
            dt.setSeconds(0);
            params.put("etdDate", dt);
            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }
            hql = hql + " etd >= :etdDate";
        }

        if (dto.getEta() != null && dto.getEta().getStartDate() != null && dto.getEta().getEndDate() != null) {

            try {
                log.info("etaStartDate :-------", dto.getEta().getStartDate());
                log.info("etaEndDate :-------", dto.getEta().getEndDate());

                Date dt1 = sdf.parse(dto.getEta().getStartDate());
                Date dt2 = sdf.parse(dto.getEta().getEndDate());

                if (hql.trim().length() != 0) {
                    hql = hql + " AND ";
                }

                params.put("etaStartDate", dt1);
                params.put("etaEndDate", dt2);

                log.info("etaStartDate :-------", dt1);
                log.info("etaEndDate :-------", dt2);

                hql = hql + " ( eta >= :etaStartDate AND eta <= :etaEndDate )  ";

            } catch (ParseException e1) {
                log.error("Date Conversion error in eta: ", e1);
            }

        }

        if (dto.getCapacityReserved() != null && dto.getCapacityReserved().trim().length() != 0) {
            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }

            hql = hql + " UPPER(capacityReserved) like '" + dto.getCapacityReserved().trim().toUpperCase() + "%' ";

        }

        if (dto.getStatus() != null && dto.getStatus().trim().length() != 0) {
            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }

            hql = hql + " UPPER(flightPlanStatus) like '" + dto.getStatus().trim().toUpperCase() + "%' ";

        }

        if (dto.getFlightNo() != null && dto.getFlightNo().trim().length() != 0) {
            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }
            hql = hql + " Upper(flightNo) like '" + dto.getFlightNo().trim().toUpperCase() + "%' ";

        }
        if (dto.getDirectStopOver() != null && dto.getDirectStopOver().trim().length() != 0) {
            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }
            hql = hql + " Upper(directStopOver) like '" + dto.getDirectStopOver().trim().toUpperCase() + "%' ";

        }


        if (hql.trim().length() != 0) {
            hql = " where " + hql;
        }

        hql = " from FlightPlan " + hql;

        if (dto.getSortByColumn() != null && dto.getOrderByType() != null && dto.getSortByColumn().trim().length() != 0
                && dto.getOrderByType().trim().length() != 0) {

            hql = hql + " order by " + dto.getSortByColumn().trim() + " " + dto.getOrderByType().trim();

        } else {
            hql = hql + " order by etd ASC";
        }

        log.info("FlightPlan Search hql:[" + hql + "]");
        String countQ = "Select count (id) " + hql;

        Query countQuery = em.createQuery(countQ);
        for (Entry<String, Object> e : params.entrySet()) {
            countQuery.setParameter(e.getKey(), e.getValue());
        }
        Long countTotal = (Long) countQuery.getSingleResult();

        TypedQuery<FlightPlan> query = em.createQuery(hql, FlightPlan.class);

        for (Entry<String, Object> e : params.entrySet()) {
            query.setParameter(e.getKey(), e.getValue());
        }

        query.setFirstResult(dto.getSelectedPageNumber() * dto.getRecordPerPage());

        query.setMaxResults(dto.getRecordPerPage());

        List<FlightPlan> resultlist = query.getResultList();

        log.info("Number of rows selected : " + resultlist.size());

        return new SearchRespDto(countTotal, resultlist);

    }

}
