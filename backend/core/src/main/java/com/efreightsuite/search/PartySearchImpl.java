package com.efreightsuite.search;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;

import com.efreightsuite.common.component.CacheRepository;
import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.PartySearchDto;
import com.efreightsuite.dto.SearchRequest;
import com.efreightsuite.dto.SearchRespDto;
import com.efreightsuite.enumeration.LovStatus;
import com.efreightsuite.enumeration.SequenceType;
import com.efreightsuite.enumeration.YesNo;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.model.GeneralLedgerAccount;
import com.efreightsuite.model.GeneralSubLedger;
import com.efreightsuite.model.PartyAccountMaster;
import com.efreightsuite.model.PartyAddressMaster;
import com.efreightsuite.model.PartyAssociateToPartyTypeMaster;
import com.efreightsuite.model.PartyBusinessDetail;
import com.efreightsuite.model.PartyCompanyAssociate;
import com.efreightsuite.model.PartyContact;
import com.efreightsuite.model.PartyContactRelation;
import com.efreightsuite.model.PartyCreditLimit;
import com.efreightsuite.model.PartyEmailMapping;
import com.efreightsuite.model.PartyEmailMessageMapping;
import com.efreightsuite.model.PartyMaster;
import com.efreightsuite.model.PartyServiceMaster;
import com.efreightsuite.repository.GeneralLedgerRepository;
import com.efreightsuite.repository.GeneralSubLedgerRepository;
import com.efreightsuite.repository.PartyMasterRepository;
import com.efreightsuite.service.AuthService;
import com.efreightsuite.service.SequenceGeneratorService;
import com.efreightsuite.util.AppUtil;
import com.efreightsuite.util.SaaSUtil;
import com.efreightsuite.validation.PartyMasterValidator;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class PartySearchImpl {

    private static final String AND = " AND ";
    private static final String AND_B = " AND ( ";
    @Autowired
    private
    EntityManager em;
    @Autowired
    private
    PartyMasterRepository partyMasterRepository;
    @Autowired
    private
    CacheRepository cacheRepository;
    @Autowired
    private
    PartyMasterValidator partyMasterValidator;
    @Autowired
    private
    SequenceGeneratorService sequenceGeneratorService;
    @Autowired
    private
    GeneralLedgerRepository generalLedgerRepository;
    @Autowired
    private
    GeneralSubLedgerRepository generalSubLedgerRepository;

    public SearchRespDto search(PartySearchDto dto) {

        String hql = "";

        if (StringUtils.isNotEmpty(dto.getSearchPartyCode())) {
            hql = hql + " Upper(partyCode) like '" + dto.getSearchPartyCode().trim().toUpperCase() + "%' ";
        }

        if (StringUtils.isNotEmpty(dto.getSearchPartyName())) {

            if (hql.trim().length() != 0) {
                hql = hql + AND;
            }

            hql = hql + " Upper(partyName) like '" + dto.getSearchPartyName().trim().toUpperCase() + "%' ";
        }

        if (StringUtils.isNotEmpty(dto.getSearchCountryName())) {

            if (hql.trim().length() != 0) {
                hql = hql + AND;
            }

            hql = hql + " countryMaster.id IN (select cm.id from CountryMaster cm where Upper(cm.countryName) like '"
                    + dto.getSearchCountryName().trim().toUpperCase() + "%') ";
        }

        if (StringUtils.isNotEmpty(dto.getSearchStatus())) {

            if (hql.trim().length() != 0) {
                hql = hql + AND;
            }

            hql = hql + " Upper(status) like '" + dto.getSearchStatus().trim().toUpperCase() + "%' ";
        }

        if (hql.trim().length() != 0) {
            hql = " where " + hql;
        }

        hql = " from PartyMaster " + hql;

        if (StringUtils.isNotEmpty(dto.getSortByColumn()) && StringUtils.isNotEmpty(dto.getOrderByType())) {
            hql = hql + " order by Upper(" + dto.getSortByColumn().trim() + ") " + dto.getOrderByType().trim();
        } else {
            hql = hql + " order by Upper(partyName) ASC";
        }

        log.info("Party Master Search1 hql:[" + hql + "]");


        return search(dto.getSelectedPageNumber(), dto.getRecordPerPage(), hql, null);

    }

    private SearchRespDto search(int selectedPageNumber, int recordPerPage, String resultHql, String countHql) {
        String countQ = countHql == null ? ("Select count (id) " + resultHql) : countHql;
        Query countQuery = em.createQuery(countQ);
        Long countTotal = (Long) countQuery.getSingleResult();

        TypedQuery<PartyMaster> query = em.createQuery(resultHql, PartyMaster.class);
        query.setFirstResult(selectedPageNumber * recordPerPage);
        query.setMaxResults(recordPerPage);

        List<PartyMaster> resultList = query.getResultList();

        AppUtil.setPartyListToNull(resultList);
        return new SearchRespDto(countTotal, resultList);
    }

    public SearchRespDto search(SearchRequest dto) {

        String hql = " from PartyMaster where status!='Hide' ";

        if (dto.getId() != null && dto.getId().trim().length() != 0) {

            hql = hql + AND_B;

            hql = hql + " id != '" + dto.getId() + "' ";
            hql = hql + " ) ";
        }
        if (dto.getKeyword() != null && dto.getKeyword().trim().length() != 0) {

            hql = hql + AND_B;

            hql = hql + " upper(partyName) like '" + dto.getKeyword().trim().toUpperCase() + "%' ";


            hql = hql + " ) ";
        }

        hql = hql + " order by partyName ASC";

        log.info("Party Master Search2 hql:[" + hql + "]");

        return search(dto.getSelectedPageNumber(), dto.getRecordPerPage(), hql, null);

    }


    public SearchRespDto searchByAgent(SearchRequest dto) {

        String hql = " from PartyMaster p left join p.partyTypeList pt left join pt.partyTypeMaster ptm  where UPPER(ptm.partyTypeCode) = 'AG'  AND p.status!='Hide' ";

        if (dto.getKeyword() != null && dto.getKeyword().trim().length() != 0) {

            hql = hql + AND_B;

            hql = hql + " upper(p.partyName) like '" + dto.getKeyword().trim().toUpperCase() + "%' ";

            hql = hql + " ) ";
        }


        hql = hql + " order by p.partyName ASC";

        log.info("Party Master Search3 hql:[" + hql + "]");

        Query query = em.createQuery(hql);

        return new SearchRespDto(10L, query.getResultList());

    }

    public SearchRespDto search(SearchRequest dto, String partyType) {

        String hql = " from PartyMaster p, PartyAssociateToPartyTypeMaster py where p=py.partyMaster AND p.status!='Hide' AND py.partyTypeMaster.partyTypeName='" + partyType + "' ";

        if (dto.getKeyword() != null && dto.getKeyword().trim().length() != 0) {

            hql = hql + AND_B;

            hql = hql + " upper(p.partyName) like '" + dto.getKeyword().trim().toUpperCase() + "%' ";


            hql = hql + " ) ";
        }

        hql = hql + " order by p.partyName ASC";


        String countHql = " select count(p) " + hql;

        String resultHql = " select p " + hql;

        return search(dto.getSelectedPageNumber(), dto.getRecordPerPage(), resultHql, countHql);

    }

    @Transactional
    public void create(BaseDto baseDto, PartyMaster partyMaster) {


        partyMasterValidator.validate(partyMaster);

        if (CollectionUtils.isNotEmpty((partyMaster.getPartyTypeList()))) {
            for (PartyAssociateToPartyTypeMaster partyAssociateToPartyTypeMaster : partyMaster.getPartyTypeList()) {
                partyAssociateToPartyTypeMaster.setPartyMaster(partyMaster);
            }
        }


        if (CollectionUtils.isNotEmpty(partyMaster.getPartyAddressList())) {
            for (PartyAddressMaster partyAddressMaster : partyMaster.getPartyAddressList()) {
                partyAddressMaster.setPartyMaster(partyMaster);
            }
        }

        if (CollectionUtils.isNotEmpty(partyMaster.getPartyServiceList())) {
            for (PartyServiceMaster service : partyMaster.getPartyServiceList()) {
                service.setPartyMaster(partyMaster);
            }
        }


        if (CollectionUtils.isNotEmpty(partyMaster.getPartyCompanyAssociateList())) {
            for (PartyCompanyAssociate tmp : partyMaster.getPartyCompanyAssociateList()) {
                tmp.setPartyMaster(partyMaster);
                tmp.setCompanyCode(tmp.getCompanyMaster().getCompanyCode());
                if (tmp.getDivisionMaster() != null) {
                    tmp.setDivisionCode(tmp.getDivisionMaster().getDivisionCode());
                }
            }
        }

        if (CollectionUtils.isNotEmpty(partyMaster.getPartyAccountList())) {
            for (PartyAccountMaster account : partyMaster.getPartyAccountList()) {
                account.setPartyMaster(partyMaster);
            }
        }

        if (CollectionUtils.isNotEmpty(partyMaster.getPartyCreditLimitList())) {
            for (PartyCreditLimit partyCreditLimit : partyMaster.getPartyCreditLimitList()) {
                partyCreditLimit.setPartyMaster(partyMaster);
            }
        }

        if (CollectionUtils.isNotEmpty(partyMaster.getPartyEmailMappingList())) {
            for (PartyEmailMapping partyEmailMapping : partyMaster.getPartyEmailMappingList()) {
                partyEmailMapping.setPartyMaster(partyMaster);

                if (partyEmailMapping.getPartyEmailMessageMappingList() != null) {
                    for (PartyEmailMessageMapping pemm : partyEmailMapping.getPartyEmailMessageMappingList()) {
                        pemm.setPartyEmailMapping(partyEmailMapping);
                    }
                }
            }
        }

        if (CollectionUtils.isNotEmpty(partyMaster.getPartyBusinessDetailList())) {
            for (PartyBusinessDetail bussinessDetail : partyMaster.getPartyBusinessDetailList()) {
                bussinessDetail.setPartyMaster(partyMaster);
                bussinessDetail.setCreatedLocation(AuthService.getCurrentUser().getSelectedUserLocation());

            }
        }
        if (partyMaster.getCountryMaster() != null) {
            partyMaster.setCountryCode(partyMaster.getCountryMaster().getCountryCode());
        }


        if (CollectionUtils.isNotEmpty(partyMaster.getContactList())) {
            for (PartyContact contact : partyMaster.getContactList()) {
                contact.setPartyMaster(partyMaster);
                contact.setStatus(LovStatus.Active);
                if (CollectionUtils.isNotEmpty(contact.getRelationList())) {
                    for (PartyContactRelation relation : contact.getRelationList()) {
                        relation.setPartyContact(contact);
                    }
                }
            }
        }

        partyMaster.setPartyCode(sequenceGeneratorService.getSequence(SequenceType.PARTY));
        partyMaster = partyMasterRepository.save(partyMaster);

        if (CollectionUtils.isNotEmpty(partyMaster.getPartyAccountList())) {
            for (PartyAccountMaster pa : partyMaster.getPartyAccountList()) {
                createSubLedgerAccount(partyMaster, pa);
            }
        }

        cacheRepository.remove(SaaSUtil.getSaaSId(), PartyMaster.class.getName(), partyMaster.getId());


        log.info("Party Successfully Saved....[" + partyMaster.getId() + "]");

        baseDto.setResponseCode(ErrorCode.SUCCESS);

        PartyMaster tmpParty = new PartyMaster();
        tmpParty.setId(partyMaster.getId());
        tmpParty.setPartyCode(partyMaster.getPartyCode());

        baseDto.setResponseObject(tmpParty);


    }

    @Transactional
    public void update(BaseDto baseDto, PartyMaster partyMaster) {

        partyMasterRepository.getOne(partyMaster.getId());

        partyMasterValidator.validate(partyMaster);


        if (CollectionUtils.isNotEmpty(partyMaster.getPartyTypeList())) {
            for (PartyAssociateToPartyTypeMaster partyAssociateToPartyTypeMaster : partyMaster.getPartyTypeList()) {
                partyAssociateToPartyTypeMaster.setPartyMaster(partyMaster);
            }
        }


        if (CollectionUtils.isNotEmpty(partyMaster.getPartyAddressList())) {
            for (PartyAddressMaster partyAddressMaster : partyMaster.getPartyAddressList()) {
                partyAddressMaster.setPartyMaster(partyMaster);
            }
        } else {
            partyMaster.setPartyAddressList(new ArrayList<>());
            partyMaster.getPartyAddressList().clear();
        }

        if (CollectionUtils.isNotEmpty(partyMaster.getPartyServiceList())) {
            for (PartyServiceMaster service : partyMaster.getPartyServiceList()) {
                service.setPartyMaster(partyMaster);
            }
        } else {
            partyMaster.setPartyServiceList(new ArrayList<>());
            partyMaster.getPartyServiceList().clear();

        }


        if (CollectionUtils.isNotEmpty(partyMaster.getPartyCompanyAssociateList())) {
            for (PartyCompanyAssociate tmp : partyMaster.getPartyCompanyAssociateList()) {
                tmp.setPartyMaster(partyMaster);
                tmp.setCompanyCode(tmp.getCompanyMaster().getCompanyCode());
                if (tmp.getDivisionMaster() != null) {
                    tmp.setDivisionCode(tmp.getDivisionMaster().getDivisionCode());
                }
            }
        } else {
            partyMaster.setPartyCompanyAssociateList(new ArrayList<>());
            partyMaster.getPartyCompanyAssociateList().clear();
        }

        if (CollectionUtils.isNotEmpty(partyMaster.getPartyAccountList())) {
            for (PartyAccountMaster account : partyMaster.getPartyAccountList()) {
                account.setPartyMaster(partyMaster);

                if (account.getId() == null) {
                    createSubLedgerAccount(partyMaster, account);
                }

            }
        } else {
            partyMaster.setPartyAccountList(new ArrayList<>());
            partyMaster.getPartyAccountList().clear();
        }


        if (CollectionUtils.isNotEmpty(partyMaster.getPartyCreditLimitList())) {
            for (PartyCreditLimit partyCreditLimit : partyMaster.getPartyCreditLimitList()) {
                partyCreditLimit.setPartyMaster(partyMaster);
            }
        } else {
            partyMaster.setPartyCreditLimitList(new ArrayList<>());
            partyMaster.getPartyCreditLimitList().clear();
        }


        if (CollectionUtils.isNotEmpty(partyMaster.getPartyEmailMappingList())) {
            for (PartyEmailMapping partyEmailMapping : partyMaster.getPartyEmailMappingList()) {
                partyEmailMapping.setPartyMaster(partyMaster);

                if (partyEmailMapping.getPartyEmailMessageMappingList() != null) {
                    for (PartyEmailMessageMapping pemm : partyEmailMapping.getPartyEmailMessageMappingList()) {
                        pemm.setPartyEmailMapping(partyEmailMapping);
                    }
                } else {
                    partyEmailMapping.setPartyEmailMessageMappingList(new ArrayList<>());
                    partyEmailMapping.getPartyEmailMessageMappingList().clear();
                }
            }
        } else {
            partyMaster.setPartyEmailMappingList(new ArrayList<>());
            partyMaster.getPartyEmailMappingList().clear();
        }


        if (CollectionUtils.isNotEmpty(partyMaster.getPartyBusinessDetailList())) {
            for (PartyBusinessDetail bussinessDetail : partyMaster.getPartyBusinessDetailList()) {
                bussinessDetail.setPartyMaster(partyMaster);
                if (bussinessDetail.getId() == null) {
                    bussinessDetail.setCreatedLocation(AuthService.getCurrentUser().getSelectedUserLocation());
                }
            }

        } else {
            partyMaster.setPartyBusinessDetailList(new ArrayList<>());
            partyMaster.getPartyBusinessDetailList().clear();
        }


        if (partyMaster.getCountryMaster() != null) {
            partyMaster.setCountryCode(partyMaster.getCountryMaster().getCountryCode());
        }


        if (CollectionUtils.isNotEmpty(partyMaster.getContactList())) {
            for (PartyContact contact : partyMaster.getContactList()) {

                contact.setPartyMaster(partyMaster);

                if (contact.getId() == null) {
                    contact.setStatus(LovStatus.Active);
                }


                if (CollectionUtils.isNotEmpty(contact.getRelationList())) {
                    for (PartyContactRelation relation : contact.getRelationList()) {
                        relation.setPartyContact(contact);
                    }
                } else {
                    contact.setRelationList(new ArrayList<>());
                    contact.getRelationList().clear();
                }

            }
        } else {
            partyMaster.setContactList(new ArrayList<>());
            partyMaster.getContactList().clear();
        }

        partyMaster = partyMasterRepository.save(partyMaster);

        cacheRepository.remove(SaaSUtil.getSaaSId(), PartyMaster.class.getName(), partyMaster.getId());

        log.info("Party Successfully Saved....[" + partyMaster.getId() + "]");

        baseDto.setResponseCode(ErrorCode.SUCCESS);

        PartyMaster tmpParty = new PartyMaster();
        tmpParty.setId(partyMaster.getId());
        tmpParty.setPartyCode(partyMaster.getPartyCode());

        baseDto.setResponseObject(tmpParty);


    }

    private void createSubLedgerAccount(PartyMaster partyMaster, PartyAccountMaster partyAccount) {

        GeneralLedgerAccount gl = generalLedgerRepository.findByGlAccountId(partyAccount.getAccountMaster().getId());

        if (gl.getIsSubLedger() != null && gl.getIsSubLedger() == YesNo.Yes) {
            GeneralSubLedger subLedger = new GeneralSubLedger();

            subLedger.setPartyMaster(partyMaster.getId());
            subLedger.setGlSubLedgerCode(partyMaster.getPartyCode());
            subLedger.setGlSubLedgerName(partyMaster.getPartyName());
            subLedger.setGeneralLedgerAccount(gl);

            subLedger.setCompanyMaster(partyAccount.getLocationMaster().getCompanyMaster());
            subLedger.setCompanyCode(partyAccount.getLocationMaster().getCompanyMaster().getCompanyCode());

            subLedger.setLocationMaster(partyAccount.getLocationMaster());
            subLedger.setLocationCode(partyAccount.getLocationMaster().getLocationCode());

            subLedger.setStatus(LovStatus.Active);

            generalSubLedgerRepository.save(subLedger);
        } else {
            throw new RestException(ErrorCode.PARTY_ACCOUNT_GL_SUB_LEDGER_NOT_ENABLED);
        }

    }
}
