package com.efreightsuite.search;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import com.efreightsuite.dto.PortGroupSearchDto;
import com.efreightsuite.dto.SearchRequest;
import com.efreightsuite.dto.SearchRespDto;
import com.efreightsuite.model.PortGroupMaster;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Log4j2
@Service
public class PortGroupSearchImpl {


    @Autowired
    private
    EntityManager em;

    public SearchRespDto search(PortGroupSearchDto dto) {

        String hql = "";

        if (dto.getSearchPortGroupName() != null && dto.getSearchPortGroupName().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }

            hql = hql + " Upper(portGroupName) like '" + dto.getSearchPortGroupName().trim().toUpperCase() + "%' ";
        }

        if (dto.getSearchPortGroupCode() != null && dto.getSearchPortGroupCode().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }


            hql = hql + " Upper(portGroupCode) like '" + dto.getSearchPortGroupCode().trim().toUpperCase() + "%' ";

        }

        if (dto.getSearchCountryName() != null && dto.getSearchCountryName().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }

            hql = hql + " portGroupMaster.countryMaster.id IN (select cm.id from CountryMaster cm where Upper(cm.countryName) like '" + dto.getSearchCountryName().trim().toUpperCase() + "%') ";
        }


        if (dto.getSearchStatus() != null && dto.getSearchStatus().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }

            hql = hql + " Upper(status) like '" + dto.getSearchStatus().trim().toUpperCase() + "%' ";
        }

        if (hql.trim().length() != 0) {
            hql = " where " + hql;
        }

        hql = " from PortGroupMaster " + hql;


        if (dto.getSortByColumn() != null && dto.getOrderByType() != null && dto.getSortByColumn().trim().length() != 0 && dto.getOrderByType().trim().length() != 0) {
            hql = hql + " order by Upper(" + dto.getSortByColumn().trim() + ") " + dto.getOrderByType().trim();
        } else {
            hql = hql + " order by Upper(portGroupName) ASC ";
        }

        log.info("Port Group Master Search hql:[" + hql + "]");

        return search(dto.getSelectedPageNumber(), dto.getRecordPerPage(), hql);

    }


    private SearchRespDto search(int selectedPageNumber, int recordPerPage, String hql) {

        String countQ = "Select count (id) " + hql;

        log.info(" Count Query Issued : " + countQ);

        Query countQuery = em.createQuery(countQ);

        Long countTotal = (Long) countQuery.getSingleResult();

        log.info("Total Number of rows : " + countTotal);

        TypedQuery<PortGroupMaster> query = em.createQuery(hql, PortGroupMaster.class);

        query.setFirstResult(selectedPageNumber * recordPerPage);

        query.setMaxResults(recordPerPage);

        List<PortGroupMaster> resultList = query.getResultList();

        log.info(resultList.size() + " number of rows selected...");

        return new SearchRespDto(countTotal, resultList);
    }


    public SearchRespDto searchByGroup(SearchRequest dto) {


        String hql = " from PortGroupMaster ";

        if (dto.getKeyword() != null && dto.getKeyword().trim().length() != 0) {

            hql = hql + " where  ";

            hql = hql + " UPPER(portGroupName) like '" + dto.getKeyword().trim().toUpperCase() + "%' ";

            hql = hql + " OR ";

            hql = hql + " UPPER(portGroupCode) like '" + dto.getKeyword().trim().toUpperCase() + "%' ";

        }


        hql = hql + " order by portGroupName ASC";

        log.info("Port  Group Search hql:[" + hql + "]");


        return search(dto.getSelectedPageNumber(), dto.getRecordPerPage(), hql);
    }

/*

public Object searchByCountry(SearchRequest searchDto, String transportMode, String countryCode) {
	// TODO Auto-generated method stub
	String hql = " from PortMaster where status!='Hide' ";

	if (searchDto.getKeyword() != null && searchDto.getKeyword().trim().length() != 0) {
		
		hql = hql + " AND ( ";

		hql = hql + " UPPER(portName) like '" + searchDto.getKeyword().trim().toUpperCase() + "%' ";
		
		hql = hql + " OR ";
		
		hql = hql + " UPPER(portCode) like '" + searchDto.getKeyword().trim().toUpperCase() + "%' ";
		
		hql = hql + " ) ";
	}
	
	if(transportMode != null) {
    	hql = hql + " AND transportMode = '" + transportMode + "' ";
    }
	if(countryCode != null) {
    	hql = hql + " AND countryCode = '" + countryCode + "' ";
    }
	
    hql = hql + " order by portName ASC";
    
	log.info("Port  Master Search hql:[" + hql + "]");
	
	return search(searchDto.getSelectedPageNumber(), searchDto.getRecordPerPage(),hql);
	
}*/
}
