package com.efreightsuite.search;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.efreightsuite.dto.AesFileSearchDto;
import com.efreightsuite.dto.SearchRespDto;
import com.efreightsuite.dto.ShipmentSearchDto;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class AesFIleSearchImpl {

    @Autowired
    private
    EntityManager em;

    private final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:SS");

    public SearchRespDto search(AesFileSearchDto aesDto) {

        Map<String, Object> params = new HashMap<>();

        String searchQuery = "Select new com.efreightsuite.dto.AesFileSearchResponseDto("
                + "aes.id, aes.serviceUid, aes.shipmentUid, aes.masterUid,"
                + "aes.aesUsppi.shipper.partyName,"
                + "aes.aesUltimateConsignee.ultimateConsignee.partyName, "
                + "aes.originState.name, "
                + "aes.destinationCountry.countryName, "
                + "aes.etd, aes.status, aes.aesAction, aes.aesItnNo, aes.aesStatus) "
                + "from AesFile aes "
                + "left join aes.aesUsppi.shipper ship "
                + "left join aes.aesUltimateConsignee.ultimateConsignee ulti "
                + "left join aes.aesIntermediateConsignee.intermediateConsignee inter "
                + "left join aes.originState ori "
                + "left join aes.destinationCountry dest ";

        String countQuery = "Select count(aes.id) from AesFile aes "
                + "left join aes.aesUsppi.shipper ship "
                + "left join aes.aesUltimateConsignee.ultimateConsignee ulti "
                + "left join aes.originState ori "
                + "left join aes.destinationCountry dest ";

        String filterQuery = "";

        if (aesDto.getServiceUid() != null
                && aesDto.getServiceUid().length() != 0) {

            if (filterQuery.trim().length() != 0) {
                filterQuery = filterQuery + " AND ";
            }

            filterQuery = filterQuery + " Upper(aes.serviceUid) like '"
                    + aesDto.getServiceUid().trim().toUpperCase() + "%' ";
        }

        if (aesDto.getShipmentUid() != null
                && aesDto.getShipmentUid().length() != 0) {

            if (filterQuery.trim().length() != 0) {
                filterQuery = filterQuery + " AND ";
            }

            filterQuery = filterQuery + " Upper(aes.shipmentUid) like '"
                    + aesDto.getShipmentUid().trim().toUpperCase() + "%' ";
        }

        if (aesDto.getMasterUid() != null
                && aesDto.getMasterUid().length() != 0) {

            if (filterQuery.trim().length() != 0) {
                filterQuery = filterQuery + " AND ";
            }

            filterQuery = filterQuery + " Upper(aes.masterUid) like '"
                    + aesDto.getMasterUid().trim().toUpperCase() + "%' ";
        }

        if (aesDto.getShipperName() != null
                && aesDto.getShipperName().length() != 0) {

            if (filterQuery.trim().length() != 0) {
                filterQuery = filterQuery + " AND ";
            }

            filterQuery = filterQuery
                    + " Upper(aes.aesUsppi.shipper.partyName) like '"
                    + aesDto.getShipperName().trim().toUpperCase() + "%' ";
        }

        if (aesDto.getConsigneeName() != null
                && aesDto.getConsigneeName().length() != 0) {

            if (filterQuery.trim().length() != 0) {
                filterQuery = filterQuery + " AND ";
            }

            filterQuery = filterQuery
                    + " Upper(aes.aesUltimateConsignee.ultimateConsignee.partyName) like '"
                    + aesDto.getConsigneeName().trim().toUpperCase() + "%' ";
        }

        if (aesDto.getOriginStateName() != null
                && aesDto.getOriginStateName().length() != 0) {

            if (filterQuery.trim().length() != 0) {
                filterQuery = filterQuery + " AND ";
            }

            filterQuery = filterQuery
                    + " Upper(aes.originState.name) like '"
                    + aesDto.getOriginStateName().trim().toUpperCase() + "%' ";
        }

        if (aesDto.getDestinationCountryName() != null
                && aesDto.getDestinationCountryName().length() != 0) {

            if (filterQuery.trim().length() != 0) {
                filterQuery = filterQuery + " AND ";
            }

            filterQuery = filterQuery
                    + " Upper(aes.destinationCountry.countryName) like '"
                    + aesDto.getDestinationCountryName().trim().toUpperCase() + "%' ";
        }

        if (aesDto.getDestinationCountryName() != null
                && aesDto.getDestinationCountryName().length() != 0) {

            if (filterQuery.trim().length() != 0) {
                filterQuery = filterQuery + " AND ";
            }

            filterQuery = filterQuery
                    + " Upper(aes.destinationCountry.countryName) like '"
                    + aesDto.getDestinationCountryName().trim().toUpperCase() + "%' ";
        }

        if (aesDto.getEtd() != null && aesDto.getEtd().getStartDate() != null
                && aesDto.getEtd().getEndDate() != null) {

            try {

                Date date1 = sdf.parse(aesDto.getEtd().getStartDate());
                Date date2 = sdf.parse(aesDto.getEtd().getEndDate());

                if (filterQuery.trim().length() != 0) {
                    filterQuery = filterQuery + " AND ";
                }

                params.put("startDate", date1);
                params.put("endDate", date2);

                filterQuery = filterQuery
                        + "( aes.etd  >= :startDate AND aes.etd  <= :endDate )";

            } catch (ParseException exception) {
                log.error("Date Conversion Exception in etd....", exception);
            }

        }

        if (aesDto.getStatus() != null && aesDto.getStatus().length() != 0) {

            if (filterQuery.trim().length() != 0) {
                filterQuery = filterQuery + " AND ";
            }

            filterQuery = filterQuery + " Upper(aes.status) like '"
                    + aesDto.getStatus().trim().toUpperCase() + "%' ";
        }

        if (aesDto.getAesAction() != null
                && aesDto.getAesAction().length() != 0) {

            if (filterQuery.trim().length() != 0) {
                filterQuery = filterQuery + " AND ";
            }

            filterQuery = filterQuery + " Upper(aes.aesAction) like '"
                    + aesDto.getAesAction().trim().toUpperCase() + "%' ";
        }

        if (aesDto.getAesItnNo() != null && aesDto.getAesItnNo().length() != 0) {

            if (filterQuery.trim().length() != 0) {
                filterQuery = filterQuery + " AND ";
            }

            filterQuery = filterQuery + " Upper(aes.aesItnNo) like '"
                    + aesDto.getAesItnNo().trim().toUpperCase() + "%' ";
        }

        if (aesDto.getAesStatus() != null
                && aesDto.getAesStatus().length() != 0) {

            if (filterQuery.trim().length() != 0) {
                filterQuery = filterQuery + " AND ";
            }

            filterQuery = filterQuery + " Upper(aes.aesStatus) like '"
                    + aesDto.getAesStatus().trim().toUpperCase() + "%' ";
        }

        if (filterQuery.trim().length() != 0) {
            filterQuery = " WHERE " + filterQuery;
        }

        // Building Order By Query

        String orderByQuery = "";

        if (aesDto.getSortByColumn() != null
                && aesDto.getSortByColumn().length() != 0) {

            if (aesDto.getSortByColumn().equalsIgnoreCase("serviceUid")) {
                orderByQuery = orderByQuery + "aes.serviceUid";
            } else if (aesDto.getSortByColumn().equalsIgnoreCase("shipmentUid")) {
                orderByQuery = orderByQuery + "aes.shipmentUid";
            } else if (aesDto.getSortByColumn().equalsIgnoreCase("masterUid")) {
                orderByQuery = orderByQuery + "aes.masterUid";
            } else if (aesDto.getSortByColumn().equalsIgnoreCase("shipperName")) {
                orderByQuery = orderByQuery
                        + "aes.aesUsppi.shipper.partyName";
            } else if (aesDto.getSortByColumn().equalsIgnoreCase("consigneeName")) {
                orderByQuery = orderByQuery
                        + "aes.aesUltimateConsignee.ultimateConsignee.partyName";
            } else if (aesDto.getSortByColumn().equalsIgnoreCase(
                    "originStateName")) {
                orderByQuery = orderByQuery + "aes.originState.name";
            } else if (aesDto.getSortByColumn().equalsIgnoreCase(
                    "destinationCountryName")) {
                orderByQuery = orderByQuery
                        + "aes.destinationCountry.countryName";
            } else if (aesDto.getSortByColumn().equalsIgnoreCase("etd")) {
                orderByQuery = orderByQuery + "aes.etd";
            } else if (aesDto.getSortByColumn().equalsIgnoreCase("status")) {
                orderByQuery = orderByQuery + "aes.status";
            } else if (aesDto.getSortByColumn().equalsIgnoreCase("aesAction")) {
                orderByQuery = orderByQuery + "aes.aesAction";
            } else if (aesDto.getSortByColumn().equalsIgnoreCase("aesItnNo")) {
                orderByQuery = orderByQuery + "aes.aesItnNo";
            } else if (aesDto.getSortByColumn().equalsIgnoreCase("aesStatus")) {
                orderByQuery = orderByQuery + "aes.aesStatus";
            } else {
                orderByQuery = orderByQuery + "aes.serviceUid";
            }
        }

        if (orderByQuery.length() != 0) {
            orderByQuery = " ORDER BY Upper(" + orderByQuery + ") " + aesDto.getOrderByType();
        } else {
            orderByQuery = " ORDER BY Upper(aes.serviceUid) DESC ";
        }


        countQuery = countQuery + filterQuery;
        Query countQ = em.createQuery(countQuery);
        for (Entry<String, Object> e : params.entrySet()) {
            countQ.setParameter(e.getKey(), e.getValue());
        }
        Long countTotal = (Long) countQ.getSingleResult();
        log.info("HQL Count Query For Search : " + countQuery);
        log.info("Total Number of Records Found : " + countTotal);

        searchQuery = searchQuery + filterQuery + orderByQuery;
        log.info("HQL Query For Search : " + searchQuery);
        Query query = em.createQuery(searchQuery);

        query.setFirstResult(aesDto.getSelectedPageNumber()
                * aesDto.getRecordPerPage());
        query.setMaxResults(aesDto.getRecordPerPage());

        for (Entry<String, Object> e : params.entrySet()) {
            query.setParameter(e.getKey(), e.getValue());
        }

        return new SearchRespDto(countTotal,
                query.getResultList());

    }


    //for pending aes -which gives the list of shipment services
    public SearchRespDto searchPending(ShipmentSearchDto serviceDto) {

        Map<String, Object> params = new HashMap<>();

        String searchQuery = "SELECT new com.efreightsuite.dto.ShipmentServiceResponseDto("
                + "service.id, service.serviceUid, service.shipmentUid, service.consolUid, service.mawbNo,"
                + "service.origin.portName, "
                + "service.destination.portName, "
                + "service.etd,"
                + "dl.hawbNo)"
                + "from ShipmentServiceDetail service "
                + "left join service.origin ori "
                + "left join service.destination dest"
                + "left join service.documentList dl ";


        String countQuery = "Select count(service.id) from ShipmentServiceDetail service "
                + " left join service.origin ori "
                + " left join service.destination dest "
                + "left join service.documentList dl ";

        String filterQuery = "";

        if (serviceDto.getServiceUid() != null
                && serviceDto.getServiceUid().length() != 0) {

            if (filterQuery.trim().length() != 0) {
                filterQuery = filterQuery + " AND ";
            }

            filterQuery = filterQuery + " Upper(service.serviceUid) like '"
                    + serviceDto.getServiceUid().trim().toUpperCase() + "%' ";
        }

        if (serviceDto.getShipmentUid() != null
                && serviceDto.getShipmentUid().length() != 0) {

            if (filterQuery.trim().length() != 0) {
                filterQuery = filterQuery + " AND ";
            }

            filterQuery = filterQuery + " Upper(service.shipmentUid) like '"
                    + serviceDto.getShipmentUid().trim().toUpperCase() + "%' ";
        }

        if (serviceDto.getConsolUid() != null
                && serviceDto.getConsolUid().length() != 0) {

            if (filterQuery.trim().length() != 0) {
                filterQuery = filterQuery + " AND ";
            }

            filterQuery = filterQuery + " Upper(service.consolUid) like '"
                    + serviceDto.getConsolUid().trim().toUpperCase() + "%' ";
        }

        if (serviceDto.getMawbNo() != null
                && serviceDto.getMawbNo().length() != 0) {

            if (filterQuery.trim().length() != 0) {
                filterQuery = filterQuery + " AND ";
            }

            filterQuery = filterQuery + " Upper(service.mawbNo) like '"
                    + serviceDto.getMawbNo().trim().toUpperCase() + "%' ";
        }


        if (serviceDto.getOrigin() != null
                && serviceDto.getOrigin().length() != 0) {

            if (filterQuery.trim().length() != 0) {
                filterQuery = filterQuery + " AND ";
            }

            filterQuery = filterQuery
                    + " Upper(service.origin.portName) like '"
                    + serviceDto.getOrigin().trim().toUpperCase() + "%' ";
        }

        if (serviceDto.getDestination() != null
                && serviceDto.getDestination().length() != 0) {

            if (filterQuery.trim().length() != 0) {
                filterQuery = filterQuery + " AND ";
            }

            filterQuery = filterQuery
                    + " Upper(service.destination.portName) like '"
                    + serviceDto.getDestination().trim().toUpperCase() + "%' ";
        }


        if (serviceDto.getEtd() != null && serviceDto.getEtd().getStartDate() != null
                && serviceDto.getEtd().getEndDate() != null) {

            try {

                Date date1 = sdf.parse(serviceDto.getEtd().getStartDate());
                Date date2 = sdf.parse(serviceDto.getEtd().getEndDate());

                if (filterQuery.trim().length() != 0) {
                    filterQuery = filterQuery + " AND ";
                }

                params.put("startDate", date1);
                params.put("endDate", date2);

                filterQuery = filterQuery
                        + "( service.etd  >= :startDate AND service.etd  <= :endDate )";

            } catch (ParseException exception) {
                log.error("Date Conversion Exception in etd....", exception);
            }

        }

        if (filterQuery.trim().length() != 0) {
            filterQuery = " WHERE service.origin.portGroupMaster.countryMaster.countryCode like 'US' "
                    + "AND service.serviceMaster.serviceCode like 'AE' AND service.aesFile.id IS NULL AND" + filterQuery;
        } else {
            filterQuery = " WHERE service.origin.portGroupMaster.countryMaster.countryCode like 'US' "
                    + "AND service.serviceMaster.serviceCode like 'AE' AND service.aesFile.id IS NULL";
        }

        // Building Order By Query

        String orderByQuery = "";

        if (serviceDto.getSortByColumn() != null
                && serviceDto.getSortByColumn().length() != 0) {

            if (serviceDto.getSortByColumn().equalsIgnoreCase("serviceUid")) {
                orderByQuery = orderByQuery + "service.serviceUid";
            } else if (serviceDto.getSortByColumn().equalsIgnoreCase("shipmentUid")) {
                orderByQuery = orderByQuery + "service.shipmentUid";
            } else if (serviceDto.getSortByColumn().equalsIgnoreCase("consolUid")) {
                orderByQuery = orderByQuery + "service.consolUid";
            } else if (serviceDto.getSortByColumn().equalsIgnoreCase("mawbNo")) {
                orderByQuery = orderByQuery + "service.mawbNo";
            } else if (serviceDto.getSortByColumn().equalsIgnoreCase(
                    "origin")) {
                orderByQuery = orderByQuery + "service.origin.portName";
            } else if (serviceDto.getSortByColumn().equalsIgnoreCase(
                    "destination")) {
                orderByQuery = orderByQuery
                        + "service.destination.portName";
            } else {
                orderByQuery = orderByQuery + "service.serviceUid";
            }
        }

        if (orderByQuery.length() != 0) {
            orderByQuery = " ORDER BY Upper(" + orderByQuery + ") " + serviceDto.getOrderByType();
        } else {
            orderByQuery = " ORDER BY Upper(service.serviceUid) DESC ";
        }


        countQuery = countQuery + filterQuery;
        Query countQ = em.createQuery(countQuery);
        for (Entry<String, Object> e : params.entrySet()) {
            countQ.setParameter(e.getKey(), e.getValue());
        }
        Long countTotal = (Long) countQ.getSingleResult();
        log.info("HQL Count Query For Search : " + countQuery);
        log.info("Total Number of Records Found : " + countTotal);

        searchQuery = searchQuery + filterQuery + orderByQuery;
        log.info("HQL Query For Search : " + searchQuery);
        Query query = em.createQuery(searchQuery);


        query.setFirstResult(serviceDto.getSelectedPageNumber()
                * serviceDto.getRecordPerPage());
        query.setMaxResults(serviceDto.getRecordPerPage());

        for (Entry<String, Object> e : params.entrySet()) {
            query.setParameter(e.getKey(), e.getValue());
        }

        return new SearchRespDto(countTotal,
                query.getResultList());

    }

}
