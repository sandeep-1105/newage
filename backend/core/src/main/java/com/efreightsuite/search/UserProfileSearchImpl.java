package com.efreightsuite.search;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import com.efreightsuite.dto.SearchRequest;
import com.efreightsuite.dto.SearchRespDto;
import com.efreightsuite.dto.UserProfileSearchDto;
import com.efreightsuite.model.Role;
import com.efreightsuite.model.UserProfile;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class UserProfileSearchImpl {

    @Autowired
    private
    EntityManager em;

    public SearchRespDto search(UserProfileSearchDto dto) {

        String hql = "";


        if (dto.getSearchUserName() != null && dto.getSearchUserName().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }

            hql = hql + " Upper(userName) like '" + dto.getSearchUserName().trim().toUpperCase() + "%' ";
        }


        if (dto.getSearchStatus() != null && dto.getSearchStatus().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }

            hql = hql + " Upper(status) like '" + dto.getSearchStatus().trim().toUpperCase() + "%' ";
        }

        if (hql.trim().length() != 0) {
            hql = " where " + hql;
        }


        hql = " from UserProfile " + hql;


        if (dto.getSortByColumn() != null && dto.getOrderByType() != null && dto.getSortByColumn().trim().length() != 0 && dto.getOrderByType().trim().length() != 0) {
            hql = hql + " order by " + dto.getSortByColumn().trim() + " " + dto.getOrderByType().trim();
        } else {
            hql = hql + " order by id DESC";
        }

        log.info("User Profile Search hql:[" + hql + "]");


        return search(dto.getSelectedPageNumber(), dto.getRecordPerPage(), hql);

    }


    public SearchRespDto search(SearchRequest dto) {

        String hql = " from UserProfile where status!='To_be_activated' ";

        if (dto.getKeyword() != null && dto.getKeyword().trim().length() != 0) {

            hql = hql + " AND ( ";

            hql = hql + " UPPER(userName) like '" + dto.getKeyword().trim().toUpperCase() + "%' ";

            hql = hql + " ) ";
        }

        hql = hql + " order by id DESC";

        log.info("User Profile Search hql:[" + hql + "]");

        return search(dto.getSelectedPageNumber(), dto.getRecordPerPage(), hql);
    }


    public SearchRespDto searchUserList(SearchRequest dto) {

        String searchQuery = "select new com.efreightsuite.dto.UserDto (user.id, user.userName, user.status) FROM UserProfile user WHERE user.status = 'Active'";

        String countQ = "select count(user.id) FROM UserProfile user WHERE user.status = 'Active'";


        String filterQuery = "";

        if (dto.getKeyword() != null && dto.getKeyword().trim().length() != 0) {
            filterQuery = filterQuery + " user.userName like '" + dto.getKeyword().trim().toUpperCase() + "%' ";
        }

        if (filterQuery != "") {
            filterQuery = " AND " + filterQuery;
        }

        searchQuery = searchQuery + filterQuery + " order by user.userName ASC";

        countQ = countQ + filterQuery;

        Query countQuery = em.createQuery(countQ);

        Long countTotal = (Long) countQuery.getSingleResult();

        Query query = em.createQuery(searchQuery);

        query.setFirstResult(dto.getSelectedPageNumber() * dto.getRecordPerPage());

        query.setMaxResults(dto.getRecordPerPage());

        return new SearchRespDto(countTotal, query.getResultList());
    }


    private SearchRespDto search(int selectedPageNumber, int recordPerPage, String hql) {

        String countQ = "Select count (id) " + hql;

        Query countQuery = em.createQuery(countQ);

        Long countTotal = (Long) countQuery.getSingleResult();

        TypedQuery<UserProfile> query = em.createQuery(hql, UserProfile.class);

        query.setFirstResult(selectedPageNumber * recordPerPage);

        query.setMaxResults(recordPerPage);

        List<UserProfile> resultlist = query.getResultList();

        for (UserProfile userProfile : resultlist) {

            for (Role role : userProfile.getRoleList()) {
                //role.setFeature(null);
//				role.setUserList(null);
            }

        }

        SearchRespDto searchRespDto = new SearchRespDto(countTotal, resultlist);

        log.info("Number of rows selected : " + resultlist.size());

        return searchRespDto;
    }

}
