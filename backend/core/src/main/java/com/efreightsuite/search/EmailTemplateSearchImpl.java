package com.efreightsuite.search;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import com.efreightsuite.dto.EmailTemplateSearchDto;
import com.efreightsuite.dto.SearchRespDto;
import com.efreightsuite.model.EmailTemplate;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class EmailTemplateSearchImpl {

    @Autowired
    private
    EntityManager em;


    public SearchRespDto search(EmailTemplateSearchDto dto) {

        String hql = "";

        if (dto.getSearchFromEmailId() != null && dto.getSearchFromEmailId().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }

            hql = hql + " Upper(fromEmailId) like '" + dto.getSearchFromEmailId().trim().toUpperCase() + "%') ";

        }

        if (dto.getSearchTemplateType() != null && dto.getSearchTemplateType().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }

            hql = hql + " Upper(templateType) like '" + dto.getSearchTemplateType().trim().toUpperCase() + "%') ";

        }

        if (dto.getSearchSubject() != null && dto.getSearchSubject().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }

            hql = hql + " Upper(subject) like '" + dto.getSearchSubject().trim().toUpperCase() + "%') ";

        }

        if (hql.trim().length() != 0) {
            hql = " where " + hql;
        }

        hql = " from EmailTemplate " + hql;

        if (dto.getSortByColumn() != null && dto.getOrderByType() != null && dto.getSortByColumn().trim().length() != 0
                && dto.getOrderByType().trim().length() != 0) {
            hql = hql + " order by Upper(" + dto.getSortByColumn().trim() + ") " + dto.getOrderByType().trim();
        } else {
            hql = hql + " order by Upper(fromEmailId) ASC";
        }

        log.info("EmailTemplate Search hql:[" + hql + "]");

        return search(dto.getSelectedPageNumber(), dto.getRecordPerPage(), hql);

    }


    private SearchRespDto search(int selectedPageNumber, int recordPerPage, String hql) {
        String countQ = "Select count (id) " + hql;

        Query countQuery = em.createQuery(countQ);

        Long countTotal = (Long) countQuery.getSingleResult();

        TypedQuery<EmailTemplate> query = em.createQuery(hql, EmailTemplate.class);

        query.setFirstResult(selectedPageNumber * recordPerPage);

        query.setMaxResults(recordPerPage);

        List<EmailTemplate> resultlist = query.getResultList();

        SearchRespDto searchRespDto = new SearchRespDto(countTotal, resultlist);

        log.info("Number of rows selected : " + resultlist.size());

        return searchRespDto;

    }

}
