package com.efreightsuite.search;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import com.efreightsuite.dto.SearchRequest;
import com.efreightsuite.dto.SearchRespDto;
import com.efreightsuite.dto.ZoneMasterSearchDto;
import com.efreightsuite.model.ZoneMaster;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class ZoneSearchImpl {

    @Autowired
    private
    EntityManager em;

    /**
     * lov search
     */
    public SearchRespDto search(SearchRequest dto) {

        String hql = " from ZoneMaster where status!='Hide' ";

        if (dto.getKeyword() != null && dto.getKeyword().trim().length() != 0) {

            hql = hql + " AND ( ";

            hql = hql + " Upper(zoneCode) like '" + dto.getKeyword().trim().toUpperCase() + "%' ";

            hql = hql + " OR ";

            hql = hql + " Upper(zoneName) like '" + dto.getKeyword().trim().toUpperCase() + "%' ";

            hql = hql + " ) ";

        }

        hql = hql + " order by zoneName ASC";

        log.info("Zone Master Search hql:[" + hql + "]");

        return search(dto.getSelectedPageNumber(), dto.getRecordPerPage(), hql);
    }

    /**
     * list view page search
     */
    public SearchRespDto search(ZoneMasterSearchDto dto) {

        String hql = "";

        if (dto.getSearchCountryName() != null && dto.getSearchCountryName().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }

            hql = hql + " countryMaster.id IN (select cm.id from CountryMaster cm where Upper(cm.countryName) like '" + dto.getSearchCountryName().trim().toUpperCase() + "%') ";

        }

        if (dto.getSearchZoneCode() != null && dto.getSearchZoneCode().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }

            hql = hql + " Upper(zoneCode) like '" + dto.getSearchZoneCode().trim().toUpperCase() + "%' ";
        }

        if (dto.getSearchZoneName() != null && dto.getSearchZoneName().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }

            hql = hql + " Upper(zoneName) like '" + dto.getSearchZoneName().trim().toUpperCase() + "%' ";
        }


        if (dto.getSearchStatus() != null && dto.getSearchStatus().toString().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }

            hql = hql + " Upper(status) like '" + dto.getSearchStatus().toString().toUpperCase() + "%' ";
        }


        if (hql.trim().length() != 0) {
            hql = " where " + hql;
        }

        hql = " from ZoneMaster " + hql;


        if (dto.getSortByColumn() != null && dto.getOrderByType() != null && dto.getSortByColumn().trim().length() != 0
                && dto.getOrderByType().trim().length() != 0) {
            hql = hql + " order by Upper(" + dto.getSortByColumn().trim() + ") " + dto.getOrderByType().trim();
        } else {
            hql = hql + " order by Upper(zoneName) ASC";
        }

        log.info("Zone Master Search hql:[" + hql + "]");

        return search(dto.getSelectedPageNumber(), dto.getRecordPerPage(), hql);

    }


    private SearchRespDto search(int selectedPageNumber, int recordPerPage, String hql) {
        String countQ = "Select count (id) " + hql;

        Query countQuery = em.createQuery(countQ);

        Long countTotal = (Long) countQuery.getSingleResult();

        TypedQuery<ZoneMaster> query = em.createQuery(hql, ZoneMaster.class);

        query.setFirstResult(selectedPageNumber * recordPerPage);

        query.setMaxResults(recordPerPage);

        List<ZoneMaster> resultlist = query.getResultList();

        SearchRespDto searchRespDto = new SearchRespDto(countTotal, resultlist);

        log.info("Number of rows selected : " + resultlist.size());

        return searchRespDto;

    }
}
