package com.efreightsuite.search;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import com.efreightsuite.dto.DocumentPrefixSearchDto;
import com.efreightsuite.dto.SearchRespDto;
import com.efreightsuite.model.DocumentPrefixMaster;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
@Log4j2
public class DocumentPrefixSearchImpl {


    @Autowired
    private
    EntityManager em;

    public SearchRespDto search(DocumentPrefixSearchDto dto) {

        String hql = "";

        if (dto.getSearchDocumentName() != null && dto.getSearchDocumentName().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }

            hql = hql + " Upper(documentName) like '" + dto.getSearchDocumentName().trim().toUpperCase() + "%' ";
        }


        if (dto.getSearchDocumentCode() != null && dto.getSearchDocumentCode().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }

            hql = hql + " Upper(documentCode) like '" + dto.getSearchDocumentCode().trim().toUpperCase() + "%' ";
        }


        if (dto.getSearchDocumentPrefix() != null && dto.getSearchDocumentPrefix().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }

            hql = hql + " Upper(docPrefix) like '" + dto.getSearchDocumentPrefix().trim().toUpperCase() + "%' ";
        }
        if (dto.getSearchDocumentSuffix() != null && dto.getSearchDocumentSuffix().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }

            hql = hql + " Upper(docSuffix) like '" + dto.getSearchDocumentSuffix().trim().toUpperCase() + "%' ";
        }

        if (dto.getSearchStatus() != null && dto.getSearchStatus().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }

            hql = hql + " Upper(status) like '" + dto.getSearchStatus().trim().toUpperCase() + "%' ";
        }

        if (hql.trim().length() != 0) {
            hql = " where " + hql;
        }

        hql = " from  DocumentPrefixMaster " + hql;

        if (dto.getSortByColumn() != null && dto.getOrderByType() != null && dto.getSortByColumn().trim().length() != 0 && dto.getOrderByType().trim().length() != 0) {
            hql = hql + " order by Upper(" + dto.getSortByColumn().trim() + ") " + dto.getOrderByType().trim();
        } else {
            hql = hql + " order by Upper(serviceName) ASC";
        }

        log.info("Document Prefix Master Search hql:[" + hql + "]");

        String countQ = "Select count (id) " + hql;

        Query countQuery = em.createQuery(countQ);

        Long countTotal = (Long) countQuery.getSingleResult();


        TypedQuery<DocumentPrefixMaster> query = em.createQuery(hql, DocumentPrefixMaster.class);

        query.setFirstResult(dto.getSelectedPageNumber() * dto.getRecordPerPage());

        query.setMaxResults(dto.getRecordPerPage());

        List<DocumentPrefixMaster> resultlist = query.getResultList();

        for (DocumentPrefixMaster doc : resultlist) {
            doc.setDocPrefixTypeMappingList(null);
        }

        return new SearchRespDto(countTotal, resultlist);

    }

}
