package com.efreightsuite.search;

import com.efreightsuite.dto.CompanySearchReqDto;
import com.efreightsuite.dto.SearchRequest;
import com.efreightsuite.dto.SearchRespDto;
import com.efreightsuite.enumeration.LovStatus;
import com.efreightsuite.model.CompanyMaster;
import com.efreightsuite.repository.CompanyMasterRepository;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.persistence.EntityManager;
import java.util.List;

import static java.util.stream.Collectors.toList;

@Service
@Log4j2
public class CompanySearchService {

    @Autowired
    private
    EntityManager em;

    @Autowired
    private CompanyMasterRepository companyMasterRepository;

    public SearchRespDto search(CompanySearchReqDto dto) {

        CompanyMaster probe = CompanyMaster.createProbe();
        probe.setCompanyCode(dto.getSearchCompanyCode());
        probe.setCompanyName(dto.getSearchCompanyName());
        if (!StringUtils.isEmpty(dto.getSearchStatus())) {
                probe.setStatus(LovStatus.valueOf(StringUtils.capitalize(dto.getSearchStatus())));
        }

        ExampleMatcher matcher = ExampleMatcher.matching()
                .withIgnoreCase()
                .withIgnorePaths("versionLock")
                .withIgnoreNullValues()
                .withMatcher("companyCode", ExampleMatcher.GenericPropertyMatcher::startsWith)
                .withMatcher( "companyName", ExampleMatcher.GenericPropertyMatcher::startsWith)
                .withMatcher("status", ExampleMatcher.GenericPropertyMatcher::startsWith);

        Example<CompanyMaster> example = Example.of(probe, matcher);

        Sort sortCriteria = dto.getSortByColumn() != null && dto.getOrderByType() != null
                ? new Sort(Sort.Direction.valueOf(dto.getOrderByType()), dto.getSortByColumn())
                : new Sort(Sort.Direction.ASC,  "companyName");

        Page<CompanyMaster> pageResult = companyMasterRepository.findAll(
                example,
                new PageRequest(dto.getSelectedPageNumber(), dto.getRecordPerPage(),
                        sortCriteria)
        );

        List<CompanyMaster> result = pageResult.getContent();

        return new SearchRespDto(pageResult.getTotalElements(), result);

    }

    public SearchRespDto search(SearchRequest dto) {

        CompanyMaster probe = CompanyMaster.createProbe();
        probe.setCompanyCode(dto.getKeyword());
        probe.setCompanyName(dto.getKeyword());

        ExampleMatcher matcher = ExampleMatcher.matchingAny()
                .withIgnoreCase()
                .withIgnoreNullValues()
                .withIgnorePaths("versionLock")
                .withMatcher( "companyName", ExampleMatcher.GenericPropertyMatcher::startsWith)
                .withMatcher("companyCode", ExampleMatcher.GenericPropertyMatcher::startsWith);

        Example<CompanyMaster> example = Example.of(probe, matcher);

        Sort sortCriteria = new Sort(Sort.Direction.ASC,  "companyName");

        Page<CompanyMaster> pageResult = companyMasterRepository.findAll(
                example,
                new PageRequest(dto.getSelectedPageNumber(), dto.getRecordPerPage(),
                        sortCriteria)
        );
        List<CompanyMaster> result = pageResult.getContent()
                                               .stream()
                                               .filter(company -> company.getStatus() != LovStatus.Hide)
                                               .collect(toList());
        return new SearchRespDto(pageResult.getTotalElements(), result);
    }
}
