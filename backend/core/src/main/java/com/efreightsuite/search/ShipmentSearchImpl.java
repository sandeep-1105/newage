package com.efreightsuite.search;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;

import com.efreightsuite.activiti.ActivitiTransactionService;
import com.efreightsuite.common.component.CacheRepository;
import com.efreightsuite.constants.Constants;
import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.ConsolDto;
import com.efreightsuite.dto.CopyShipmentSearchDto;
import com.efreightsuite.dto.FromToCurrency;
import com.efreightsuite.dto.ImportExportSearchDto;
import com.efreightsuite.dto.ImportToExportDto;
import com.efreightsuite.dto.SearchRequest;
import com.efreightsuite.dto.SearchRespDto;
import com.efreightsuite.dto.ShipmentDto;
import com.efreightsuite.dto.ShipmentSearchRequestDto;
import com.efreightsuite.enumeration.AddressType;
import com.efreightsuite.enumeration.Approved;
import com.efreightsuite.enumeration.DimensionUnit;
import com.efreightsuite.enumeration.EventMasterType;
import com.efreightsuite.enumeration.ImportExport;
import com.efreightsuite.enumeration.LovStatus;
import com.efreightsuite.enumeration.SearchType;
import com.efreightsuite.enumeration.SequenceType;
import com.efreightsuite.enumeration.StatusOfShipment;
import com.efreightsuite.enumeration.StockStatus;
import com.efreightsuite.enumeration.TransportMode;
import com.efreightsuite.enumeration.TriggerType;
import com.efreightsuite.enumeration.WhoRouted;
import com.efreightsuite.enumeration.YesNo;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.model.*;
import com.efreightsuite.repository.AgentPortMasterRepository;
import com.efreightsuite.repository.DefaultMasterDataRepository;
import com.efreightsuite.repository.DocumentDetailRepository;
import com.efreightsuite.repository.DynamicFieldRepository;
import com.efreightsuite.repository.EventMasterRepository;
import com.efreightsuite.repository.LocationMasterRepository;
import com.efreightsuite.repository.QuotationRepository;
import com.efreightsuite.repository.ServiceMappingRepository;
import com.efreightsuite.repository.ServiceMasterRepository;
import com.efreightsuite.repository.ShipmentAttachmentRepository;
import com.efreightsuite.repository.ShipmentRepository;
import com.efreightsuite.repository.ShipmentServiceDetailRepository;
import com.efreightsuite.repository.ShipmentServiceEventRepository;
import com.efreightsuite.repository.ShipmentServiceTriggerRepository;
import com.efreightsuite.repository.StockRepository;
import com.efreightsuite.repository.TriggerMasterRepository;
import com.efreightsuite.service.AuthService;
import com.efreightsuite.service.SequenceGeneratorService;
import com.efreightsuite.util.AppUtil;
import com.efreightsuite.util.CommonUtils;
import com.efreightsuite.util.CryptoException;
import com.efreightsuite.util.CryptoUtils;
import com.efreightsuite.util.SaaSUtil;
import com.efreightsuite.util.TimeUtil;
import com.efreightsuite.validation.InvoiceCreditNoteValidator;
import com.efreightsuite.validation.ShipmentServiceDetailValidator;
import com.efreightsuite.validation.ShipmentValidator;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import static com.efreightsuite.constants.Constants.*;

@Service
@Log4j2
public class ShipmentSearchImpl {

    @Autowired
    private
    EntityManager em;

    @Autowired
    private
    AppUtil appUtil;


    @Autowired
    private
    CacheRepository cacheRepository;

    @Autowired
    private
    CurrencyRateSearchImpl currencyRateSearchImpl;

    @Autowired
    private
    DynamicFieldRepository dynamicFieldRepository;

    @Autowired
    private
    ShipmentServiceDetailValidator shipmentServiceDetailValidator;

    @Autowired
    private
    ShipmentServiceTriggerRepository shipmentServiceTriggerRepository;

    @Autowired
    private
    ServiceMappingRepository serviceMappingRepository;

    @Autowired
    private
    AgentPortMasterRepository agentPortMasterRepository;

    @Autowired
    private
    LocationMasterRepository locationMasterRepository;

    @Autowired
    private
    ServiceMasterRepository serviceMasterRepository;

    private final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:SS");

    @Autowired
    private
    ShipmentRepository shipmentRepository;

    @Autowired
    private
    StockRepository stockRepository;

    @Autowired
    private
    ShipmentValidator shipmentValidator;

    @Autowired
    private
    SequenceGeneratorService sequenceGeneratorService;

    @Autowired
    private
    ShipmentAttachmentRepository shipmentAttachmentRepository;

    @Autowired
    DefaultMasterDataRepository defaultMasterDataRepository;

    @Autowired
    private
    QuotationRepository quotationRepository;

    @Autowired
    private
    ActivitiTransactionService activitiTransactionService;

    @Autowired
    private
    EventMasterRepository eventMasterRepository;

    @Autowired
    private
    ShipmentServiceEventRepository shipmentServiceEventRepository;

    @Autowired
    private
    ShipmentServiceDetailRepository shipmentServiceDetailRepository;

    @Autowired
    private
    InvoiceCreditNoteValidator invoiceCreditNoteValidator;

    @Autowired
    private
    TriggerMasterRepository triggerMasterRepository;

    @Autowired
    private
    DocumentDetailRepository documentDetailRepository;


    private final StringBuilder searchQuery = new StringBuilder("select new com.efreightsuite.dto.ImportExportSearchResponseDto"
            + " (ssd.id, loc.locationName, ssd.shipmentUid, sm.serviceName,ssd.serviceUid,ssd.serviceReqDate,sh.partyName, sg.colorCode, co.partyName, cg.colorCode, prt.portName,dst.portName, ssd.eta, ssd.etd,dd.hawbNo, ssd.exportRef)"
            + " from DocumentDetail dd "
            + " left join dd.shipmentServiceDetail ssd"
            + " left join ssd.location loc"
            + " left join ssd.serviceMaster sm"
            + " left join dd.shipper sh"
            + " left join dd.shipper.gradeMaster sg"
            + " left join dd.consignee co "
            + " left join dd.consignee.gradeMaster cg"
            + " left join ssd.origin prt"
            + " left join ssd.destination dst");

    private final StringBuilder countQ = new StringBuilder("Select count (dd.id) from DocumentDetail dd  left join dd.shipmentServiceDetail ssd ");

    @Transactional
    public String toBeGenerateDONumber(Long doumentId) {
        DocumentDetail dd = documentDetailRepository.findById(doumentId);
        if (dd != null) {
            if (dd.getDoNumber() == null) {
                dd.setDoNumber(sequenceGeneratorService.getSequenceValueByType(SequenceType.DO));
            }
            dd.setDoIssuedBy(AuthService.getCurrentUser().getEmployee());
            dd.setDoIssuedDate(TimeUtil.getCurrentLocationTime());
            dd = documentDetailRepository.save(dd);
            activitiTransactionService.doNumber(dd.getShipmentServiceDetail().getShipment().getId(), dd.getShipmentServiceDetail());
        }
        return dd == null ? null : dd.getDoNumber();
    }


    @Transactional
    public String toBeGenerateCanNumber(Long doumentId) {

        String canNumber = null;
        DocumentDetail dd = documentDetailRepository.findById(doumentId);
        if (dd != null) {
            if (dd.getCanNumber() == null) {
                canNumber = sequenceGeneratorService.getSequenceValueByType(SequenceType.CAN);
                dd.setCanNumber(canNumber);
            }
            dd.setCanIssuedBy(AuthService.getCurrentUser().getEmployee());
            dd.setCanIssuedDate(TimeUtil.getCurrentLocationTime());
            documentDetailRepository.save(dd);
            ShipmentServiceDetail sd = shipmentServiceDetailRepository.findById(dd.getShipmentServiceDetail().getId());
            activitiTransactionService.cargoArrival(sd.getShipment().getId(), sd);
        }
        return canNumber;
    }


    public void cancelProcess(ShipmentDto shipmentDto) {

        if (shipmentDto.getShipmentId() != null && shipmentDto.getServiceId() == null) {
            // Shipment is cancelled

            Shipment shipment = shipmentRepository.findById(shipmentDto.getShipmentId());


            ShipmentStatus shipmentStatus = new ShipmentStatus();
            shipmentStatus.setShipment(shipment);
            shipmentStatus.setShipmentStatus(StatusOfShipment.Cancelled);

            shipment.getShipmentStatusList().add(shipmentStatus);

            shipment.setLastUpdatedStatus(shipmentStatus);

            for (ShipmentServiceDetail service : shipment.getShipmentServiceList()) {

                if (service.getLastUpdatedStatus()
                        .getServiceStatus() == com.efreightsuite.enumeration.ServiceStatus.Cancelled) {
                    continue;
                }

                if (!invoiceCreditNoteValidator.accountingTransactionStatus(null, service.getId())) {
                    throw new RestException(ErrorCode.INVOICE_SERVICE_ACCOUNTING_NOT_DONE);
                }

                ServiceStatus serviceStatus = new ServiceStatus();
                serviceStatus.setShipmentServiceDetail(service);
                serviceStatus.setServiceStatus(com.efreightsuite.enumeration.ServiceStatus.Cancelled);

                service.getServiceStatusList().add(serviceStatus);

                service.setLastUpdatedStatus(serviceStatus);

                service.setShipment(shipment);
                service.setShipmentUid(shipment.getShipmentUid());

            }

            shipmentRepository.save(shipment);
            for (ShipmentServiceDetail sd : shipment.getShipmentServiceList()) {
                activitiTransactionService.cancelShipmentTask(sd);
            }


        } else if (shipmentDto.getShipmentId() != null && shipmentDto.getServiceId() != null) {
            // Service is cancelled

            ShipmentServiceDetail service = shipmentRepository.getByService(shipmentDto.getServiceId());

            service.setMawbNo(null);
            service.setMawbDate(null);

            if (!invoiceCreditNoteValidator.accountingTransactionStatus(null, service.getId())) {
                throw new RestException(ErrorCode.INVOICE_SERVICE_ACCOUNTING_NOT_DONE);
            }

            ServiceStatus serviceStatus = new ServiceStatus();
            serviceStatus.setShipmentServiceDetail(service);
            serviceStatus.setServiceStatus(com.efreightsuite.enumeration.ServiceStatus.Cancelled);

            service.getServiceStatusList().add(serviceStatus);

            service.setLastUpdatedStatus(serviceStatus);


            shipmentServiceDetailRepository.save(service);
            activitiTransactionService.cancelShipmentTask(service);

        }

    }


    private void triggerUpdateProcess(ShipmentServiceDetail service, boolean isReceivedTrigger) {

        log.info("Trigger Update Process is started....................");
        TriggerType triggerType = null;
        if (isReceivedTrigger) {
            triggerType = TriggerType.RECEIVED;
        } else {
            if (service.getWhoRouted() != null && service.getWhoRouted().equals(WhoRouted.Agent)) {
                triggerType = TriggerType.AGENT;
            } else {
                triggerType = TriggerType.CUSTOMER;
            }
        }

        TriggerMaster triggerMaster = triggerMasterRepository.findByTriggerCode(triggerType.name().toUpperCase());

        if (triggerMaster != null && triggerMaster.getStatus().equals(LovStatus.Active)) {
            if (service.getShipmentServiceTriggerList() == null) {
                service.setShipmentServiceTriggerList(new ArrayList<>());
            }
            ShipmentServiceTrigger shipmentServiceTrigger = new ShipmentServiceTrigger();
            shipmentServiceTrigger.setTriggerMaster(triggerMaster);
            shipmentServiceTrigger.setTriggerTypeMaster(triggerMaster.getTriggerTypeMaster());
            shipmentServiceTrigger.setDate(TimeUtil.getCurrentLocationTime());
            shipmentServiceTrigger.setFollowUpDate(null);
            shipmentServiceTrigger.setFollowUpRequired(YesNo.No);
            shipmentServiceTrigger.setFollowUpDate(null);
            shipmentServiceTrigger.setCompleted(YesNo.No);
            shipmentServiceTrigger.setProtect(YesNo.No);
            shipmentServiceTrigger.setNote(triggerMaster.getNote());
            shipmentServiceTrigger.setServiceUid(service.getServiceUid());
            shipmentServiceTrigger.setShipmentUid(service.getShipment().getShipmentUid());
            shipmentServiceTrigger.setShipmentServiceDetail(service);
            service.getShipmentServiceTriggerList().add(shipmentServiceTrigger);
        }
        log.info("Trigger Update Process is finished....................");
    }

    @Transactional
    public Shipment createShipment(Shipment shipment, Map<String, String> newServiceMap) throws CryptoException {

        shipmentValidator.validate(shipment);
        if (CollectionUtils.isEmpty(shipment.getShipmentStatusList())) {
            shipment.setShipmentStatusList(new ArrayList<>());
        }
        ShipmentStatus shipmentStatus = new ShipmentStatus();
        shipmentStatus.setShipment(shipment);
        shipmentStatus.setShipmentStatus(StatusOfShipment.Created);
        shipment.getShipmentStatusList().add(shipmentStatus);
        shipment.setLastUpdatedStatus(shipmentStatus);
        if (shipment.getShipmentUid() == null) {
            shipment.setShipmentUid(sequenceGeneratorService.getSequence(SequenceType.SHIPMENT));
        }
        for (ShipmentServiceDetail service : shipment.getShipmentServiceList()) {

            if (service.getQuotationUid() != null) {
                updateQuotation(shipment, service);
            }
            service.setServiceUid(sequenceGeneratorService.getSequence(SequenceType.SERVICE));
            newServiceMap.put(service.getServiceUid(), service.getServiceUid());
            service.setShipment(shipment);
            service.setShipmentUid(shipment.getShipmentUid());
            if (CollectionUtils.isEmpty(service.getServiceStatusList())) {
                service.setServiceStatusList(new ArrayList<>());
            }
            ServiceStatus serviceStatus = new ServiceStatus();
            serviceStatus.setShipmentServiceDetail(service);
            serviceStatus.setServiceStatus(com.efreightsuite.enumeration.ServiceStatus.Booked);
            service.getServiceStatusList().add(serviceStatus);

            service.setLastUpdatedStatus(serviceStatus);
            /* Document Code Start here */

            if (CollectionUtils.isNotEmpty(service.getDocumentList())) {
                for (DocumentDetail dd : service.getDocumentList()) {
                    dd.setShipmentServiceDetail(service);
                    dd.setShipmentUid(shipment.getShipmentUid());
                    dd.setServiceUid(service.getServiceUid());
                    if (CollectionUtils.isNotEmpty(dd.getDimensionList())) {
                        for (ServiceDocumentDimension dim : dd.getDimensionList()) {
                            dim.setDocumentDetail(dd);
                        }
                    } else {
                        dd.setDimensionList(null);
                    }

                }
            } else {
                service.setDocumentList(null);
            }
            if (CollectionUtils.isNotEmpty(service.getShipmentChargeList())) {
                for (ShipmentCharge sc : service.getShipmentChargeList()) {

                    sc.setShipmentUid(shipment.getShipmentUid());

                    sc.setShipmentServiceDetail(service);
                    sc.setServiceUid(service.getServiceUid());

                }
            } else {
                service.setShipmentChargeList(null);
            }
            triggerUpdateProcess(service, false);
            if (CollectionUtils.isNotEmpty(service.getShipmentServiceTriggerList())) {
                for (ShipmentServiceTrigger sst : service.getShipmentServiceTriggerList()) {
                    sst.setServiceUid(service.getServiceUid());
                    sst.setShipmentUid(service.getShipment().getShipmentUid());
                    sst.setShipmentServiceDetail(service);
                }
            } else {
                service.setShipmentServiceTriggerList(null);
            }
            if (CollectionUtils.isNotEmpty(service.getConnectionList())) {
                for (ServiceConnection dd : service.getConnectionList()) {
                    dd.setShipmentServiceDetail(service);
                    dd.setShipmentUid(shipment.getShipmentUid());
                    dd.setServiceUid(service.getServiceUid());
                }
            } else {
                service.setConnectionList(null);
            }
            if (CollectionUtils.isNotEmpty(service.getShipmentAttachmentList())) {
                for (ShipmentAttachment attachment : service.getShipmentAttachmentList()) {

                    if (attachment.getIsProtected() != null && attachment.getIsProtected().equals(YesNo.Yes)) {

                        String key = appUtil.getLocationConfig("shipment.attach.file.encrypt.key", service.getLocation(), false);

                        if (attachment.getFile() != null) {
                            attachment.setFile(CryptoUtils.encrypt(key, attachment.getFile()));
                        }
                    }

                    attachment.setShipmentServiceDetail(service);
                }
            } else {
                service.setShipmentAttachmentList(null);
            }
            if (CollectionUtils.isNotEmpty(service.getEventList())) {
                for (ShipmentServiceEvent event : service.getEventList()) {
                    event.setShipmentServiceDetail(service);
                    ServiceMaster serviceMaster = serviceMasterRepository.getOne(service.getServiceMaster().getId());
                    if (serviceMaster.getTransportMode() == TransportMode.Air
                            && serviceMaster.getImportExport() == ImportExport.Export
                            && serviceMaster.getServiceType() == null) {

                        if (event.getEventMaster().getEventMasterType() != null && (event.getEventMaster().getEventMasterType() == EventMasterType.CARGO_RECEIVED
                                || event.getEventMaster().getEventMasterType().equals(EventMasterType.CARGO_RECEIVED))) {

                            if (service.getLastUpdatedStatus().getServiceStatus()
                                    .equals(com.efreightsuite.enumeration.ServiceStatus.Booked)
                                    || service.getLastUpdatedStatus()
                                    .getServiceStatus() == com.efreightsuite.enumeration.ServiceStatus.Booked) {
                                ServiceStatus shStatus = new ServiceStatus();
                                shStatus.setShipmentServiceDetail(service);
                                shStatus.setServiceStatus(com.efreightsuite.enumeration.ServiceStatus.Received);
                                service.getServiceStatusList().add(shStatus);

                                service.setLastUpdatedStatus(shStatus);

                                triggerUpdateProcess(service, true);
                            }

                        }

                    }

                }
            } else {
                service.setEventList(null);
            }
            if (CollectionUtils.isNotEmpty(service.getReferenceList())) {
                for (ShipmentServiceReference reference : service.getReferenceList()) {
                    reference.setShipmentServiceDetail(service);
                }
            } else {
                service.setReferenceList(null);
            }
            if (CollectionUtils.isNotEmpty(service.getAuthenticatedDocList())) {
                for (AuthenticatedDocs authenticatedDocs : service.getAuthenticatedDocList()) {
                    authenticatedDocs.setShipmentServiceDetail(service);
                }
            } else {
                service.setAuthenticatedDocList(null);
            }

            if (CollectionUtils.isNotEmpty(service.getReferenceList())) {
                for (ShipmentServiceReference reference : service.getReferenceList()) {
                    reference.setShipmentServiceDetail(service);
                }
            } else {
                service.setReferenceList(null);
            }

            if (CollectionUtils.isNotEmpty(service.getBillOfEntryList())) {
                for (BillOfEntry billOfEntry : service.getBillOfEntryList()) {
                    billOfEntry.setShipmentServiceDetail(service);
                }
            } else {
                service.setBillOfEntryList(null);
            }

            try {

                Double minimum = Double.parseDouble(
                        appUtil.getLocationConfig(SHIPMENT_MIN_CHRG_WEIGHT, service.getLocation(), false));
                if (service.getBookedChargeableUnit() < minimum) {
                    service.setIsMinimumShipment(YesNo.Yes);
                } else {
                    service.setIsMinimumShipment(YesNo.No);
                }
            } catch (Exception e) {
                throw new RestException(ErrorCode.SHIPMENT_MINIMUM_WEIGHT);
            }

        }

        Shipment persistedShipment = shipmentRepository.save(shipment);
        persistedShipment = activitiTransactionService.createShipmentTask(persistedShipment);
        persistedShipment = shipmentRepository.save(persistedShipment);

        if (persistedShipment != null && CollectionUtils.isNotEmpty(persistedShipment.getShipmentServiceList())) {
            for (ShipmentServiceDetail sd : persistedShipment.getShipmentServiceList()) {
                ServiceMaster serviceMaster = serviceMasterRepository.getOne(sd.getServiceMaster().getId());
                if (serviceMaster.getImportExport().equals(ImportExport.Export) && sd.getMawbNo() != null
                        && sd.getMawbNo().trim().length() != 0) {
                    String mawbNo = sd.getMawbNo();
                    StockGeneration sg = stockRepository.findByMawb(mawbNo);
                    if (sg != null && StockStatus.Available.equals(sg.getStockStatus())) {
                        sg.setShipment(persistedShipment);
                        sg.setShipmentUid(persistedShipment.getShipmentUid());
                        sg.setServiceCode(sd.getServiceCode());
                        sg.setStockStatus(StockStatus.Reserved);
                        stockRepository.save(sg);

                    }
                }
            }
        }

        return persistedShipment;

    }

    private void updateQuotation(Shipment shipment, ShipmentServiceDetail service) {
        Quotation quotation = quotationRepository.findByQuotationNo(service.getQuotationUid());
        quotation.setBookingNo(shipment.getShipmentUid());
        quotation.setApproved(Approved.Gained);
        for (QuotationDetail qd : quotation.getQuotationDetailList()) {
            for (QuotationCarrier qc : qd.getQuotationCarrierList()) {
                if (Objects.equals(service.getQuotationCarrierId(), qc.getId())) {
                    qd.setQuotationCarrierId(service.getQuotationCarrierId());
                    break;
                }
            }
        }
        quotationRepository.save(quotation);
    }

    @Transactional
    public Shipment updateShipment(Shipment shipment, Map<String, String> newServiceMap) throws CryptoException {

        log.info("<<<<Inside updateShipment() method:>>>>");

        shipmentValidator.validate(shipment);


        for (int i = 0; i < shipment.getShipmentServiceList().size(); i++) {

            log.info("Iterating over the shipment.getShipmentServiceList(): ");
            ShipmentServiceDetail service = shipment.getShipmentServiceList().get(i);

            if (service.getId() != null && service.getLastUpdatedStatus() != null && service.getLastUpdatedStatus().getServiceStatus() != null && service.getLastUpdatedStatus().getServiceStatus().equals(com.efreightsuite.enumeration.ServiceStatus.Cancelled)) {
                shipment.getShipmentServiceList().set(i, shipmentServiceDetailRepository.findById(service.getId()));
                continue;
            }


            service.setShipment(shipment);
            service.setShipmentUid(shipment.getShipmentUid());

            if (service.getId() == null) {

                //condition executed for directing update shipment with new service which fails in import to export nomination
                if (service.getServiceUid() == null) {
                    service.setServiceUid(sequenceGeneratorService.getSequenceValueByType(SequenceType.SERVICE));
                }
                newServiceMap.put(service.getServiceUid(), service.getServiceUid());

                if (CollectionUtils.isNotEmpty(service.getServiceStatusList())) {
                    service.setServiceStatusList(new ArrayList<>());
                }
                ServiceStatus serviceStatus = new ServiceStatus();
                serviceStatus.setShipmentServiceDetail(service);
                serviceStatus.setServiceStatus(com.efreightsuite.enumeration.ServiceStatus.Booked);
                service.getServiceStatusList().add(serviceStatus);

                service.setLastUpdatedStatus(serviceStatus);

                log.info("service.getQuotationUid() is :",service.getQuotationUid());
                if (service.getQuotationUid() != null) {
                    Quotation quotation = quotationRepository.findByQuotationNo(service.getQuotationUid());
                    quotation.setBookingNo(shipment.getShipmentUid());

                    for (QuotationDetail qd : quotation.getQuotationDetailList()) {
                        for (QuotationCarrier qc : qd.getQuotationCarrierList()) {
                            if (Objects.equals(service.getQuotationCarrierId(), qc.getId())) {
                                qd.setQuotationCarrierId(service.getQuotationCarrierId());
                                break;
                            }
                        }
                    }
                    log.info("Before saving quotation inside ShipmentSearchImpl ");
                    quotationRepository.save(quotation);
                    log.info("After saving quotation inside ShipmentSearchImpl ");

                }
            }

			/* Service Status Start here */
            for (ServiceStatus st : service.getServiceStatusList()) {
                st.setShipmentServiceDetail(service);
            }
			/* Service Status End here */

			/* Document Code Start here */
            if (CollectionUtils.isNotEmpty(service.getDocumentList())) {
                for (DocumentDetail dd : service.getDocumentList()) {
                    dd.setShipmentServiceDetail(service);

                    if (dd.getId() == null) {
                        dd.setShipmentUid(shipment.getShipmentUid());
                        dd.setServiceUid(service.getServiceUid());
                    }

                    if (CollectionUtils.isNotEmpty(dd.getDimensionList())) {
                        for (ServiceDocumentDimension dim : dd.getDimensionList()) {
                            dim.setDocumentDetail(dd);
                        }
                    } else {
                        dd.setDimensionList(new ArrayList<>());
                    }

                }
            } else {

                service.setDocumentList(new ArrayList<>());
            }

			/* Document Code End here */

			/* Shipment Charge Code begins here... */
            if (CollectionUtils.isNotEmpty(service.getShipmentChargeList())) {
                for (ShipmentCharge sc : service.getShipmentChargeList()) {
                    sc.setShipmentUid(shipment.getShipmentUid());
                    sc.setShipmentServiceDetail(service);
                    sc.setServiceUid(service.getServiceUid());
                }
            } else {
                service.setShipmentChargeList(new ArrayList<>());
            }
			/* Shipment Charge Code end here... */

			/* Shipment Connections begins here... */
            if (CollectionUtils.isNotEmpty(service.getConnectionList())) {
                for (ServiceConnection sc : service.getConnectionList()) {

                    sc.setShipmentServiceDetail(service);

                    if (sc.getId() == null) {
                        sc.setServiceUid(service.getServiceUid());
                        sc.setShipmentUid(shipment.getShipmentUid());
                    }
                }
            } else {
                service.setConnectionList(new ArrayList<>());
            }
			/* Shipment Connections Code end here... */

			/* Shipment ShipmentServiceTrigger begins here... */


            ArrayList<ShipmentServiceTrigger> newTriggerList = new ArrayList<>();

            if (CollectionUtils.isNotEmpty(service.getShipmentServiceTriggerList())) {
                for (ShipmentServiceTrigger sst : service.getShipmentServiceTriggerList()) {

                    if (sst.getServiceUid() != null && sst.getServiceUid().equals(service.getServiceUid())) {
                        log.info("Trigger from current service...");
                        newTriggerList.add(sst);
                    }
                    if (sst.getId() == null) {
                        log.info("new Trigger from current service...");
                        newTriggerList.add(sst);
                    }
                }
            }

            service.setShipmentServiceTriggerList(new ArrayList<>());
            service.setShipmentServiceTriggerList(newTriggerList);

            if (service.getId() == null) {
                triggerUpdateProcess(service, false);
            }


            for (ShipmentServiceTrigger sst : service.getShipmentServiceTriggerList()) {
                sst.setServiceUid(service.getServiceUid());
                sst.setShipmentUid(service.getShipment().getShipmentUid());
                sst.setShipmentServiceDetail(service);
            }


            log.info("Number of triggers updated " + service.getShipmentServiceTriggerList().size());
			/* Shipment ShipmentServiceTrigger Code end here... */


			/* Attachments code start here */


            if (CollectionUtils.isNotEmpty(service.getShipmentAttachmentList())) {

                String KEY = appUtil.getLocationConfig("shipment.attach.file.encrypt.key", service.getLocation(), false);

                for (ShipmentAttachment attachment : service.getShipmentAttachmentList()) {

                    attachment.setShipmentServiceDetail(service);


                    if (attachment.getId() != null) {
                        // Old Record
                        ShipmentAttachment dbAttachment = shipmentAttachmentRepository.getOne(attachment.getId());

                        if (attachment.getFile() != null) {
                            // File Given
                            if (attachment.getIsProtected() != null && attachment.getIsProtected().equals(YesNo.Yes)) {
                                // Encrypted
                                attachment.setFile(CryptoUtils.encrypt(KEY, attachment.getFile()));
                            } else {
                                // Not Encrypted
                            }

                        } else {
                            // File Not given So take the old file
                            if (attachment.getIsProtected() != null && attachment.getIsProtected().equals(YesNo.Yes)) {

                                if (dbAttachment.getIsProtected() != null && dbAttachment.getIsProtected().equals(YesNo.Yes)) {
                                    // Database File is already protected.
                                    attachment.setFile(dbAttachment.getFile());
                                } else {
                                    // Database File is not protected so ecrypt the Database FIle and Save.
                                    attachment.setFile(CryptoUtils.encrypt(KEY, dbAttachment.getFile()));
                                }

                            } else {

                                if (dbAttachment.getIsProtected() != null && dbAttachment.getIsProtected().equals(YesNo.Yes)) {
                                    // Database File is Already encrypted so decrypt it.
                                    attachment.setFile(CryptoUtils.decrypt(KEY, dbAttachment.getFile()));
                                } else {
                                    // Database FIle is already not encrypted
                                    attachment.setFile(dbAttachment.getFile());
                                }

                            }


                        }

                    } else {
                        // New File

                        if (attachment.getIsProtected() != null && attachment.getIsProtected().equals(YesNo.Yes)) {
                            if (attachment.getFile() != null) {
                                attachment.setFile(CryptoUtils.encrypt(KEY, attachment.getFile()));
                            }
                        }
                    }
                }

            } else {
                log.info("There are no attachments for this service.....");
                service.setShipmentAttachmentList(new ArrayList<>());
            }

			/* Attachment code ends here... */

			/* Shipment Service Event begins here... */
            if (CollectionUtils.isNotEmpty(service.getEventList())) {
                for (ShipmentServiceEvent event : service.getEventList()) {
                    event.setShipmentServiceDetail(service);
                    ServiceMaster serviceMaster = serviceMasterRepository.getOne(service.getServiceMaster().getId());
                    if (serviceMaster.getTransportMode() == TransportMode.Air
                            && serviceMaster.getImportExport() == ImportExport.Export
                            && serviceMaster.getServiceType() == null) {

                        if (event.getEventMaster().getEventMasterType() != null && (event.getEventMaster().getEventMasterType() == EventMasterType.CARGO_RECEIVED
                                || event.getEventMaster().getEventMasterType().equals(EventMasterType.CARGO_RECEIVED))) {

                            if (service.getLastUpdatedStatus().getServiceStatus()
                                    .equals(com.efreightsuite.enumeration.ServiceStatus.Booked)
                                    || service.getLastUpdatedStatus()
                                    .getServiceStatus() == com.efreightsuite.enumeration.ServiceStatus.Booked) {
                                ServiceStatus shStatus = new ServiceStatus();
                                shStatus.setShipmentServiceDetail(service);
                                shStatus.setServiceStatus(com.efreightsuite.enumeration.ServiceStatus.Received);
                                service.getServiceStatusList().add(shStatus);
                                service.setLastUpdatedStatus(shStatus);
                                triggerUpdateProcess(service, true);
                            }

                        }

                    }
                }

            } else {
                service.setEventList(new ArrayList<>());
            }


            if (CollectionUtils.isNotEmpty(service.getEventList()) && service.getIsDownloadPrintFor() != null) {

                EventMaster event = new EventMaster();
                ShipmentServiceEvent se = null;

                if (service.getIsDownloadPrintFor() == "HAWB" || service.getIsDownloadPrintFor().equals("HAWB")) {
                    event = eventMasterRepository.findByEventMasterType(EventMasterType.HAWB_RECEIVED);
                } else if (service.getIsDownloadPrintFor() == "DO" || service.getIsDownloadPrintFor().equals("DO")) {
                    event = eventMasterRepository.findByEventMasterType(EventMasterType.DO_RECEIVED);
                } else {
                    log.info("no documnets");
                }
                if (event != null) {
                    se = shipmentServiceEventRepository.findByEventId(event.getId(), service.getId());
                }
                if (se == null && event != null && event.getId() != null) {
                    se = new ShipmentServiceEvent();
                    se.setShipmentServiceDetail(service);
                    se.setEventMaster(event);
                    se.setEventDate(TimeUtil.getCurrentLocationTime(service.getLocation()));
                    se.setFollowUpRequired(YesNo.No);
                    se.setIsCompleted(YesNo.Yes);
                    service.getEventList().add(se);
                }
            }
			/* Shipment Service Event Code end here... */

			/* Shipment Reference Tab Code begins here... */
            if (CollectionUtils.isNotEmpty(service.getReferenceList())) {
                for (ShipmentServiceReference reference : service.getReferenceList()) {
                    reference.setShipmentServiceDetail(service);
                }
            } else {
                service.setReferenceList(new ArrayList<>());
            }
			/* Shipment Reference Tab Code end here... */
			
			/* Shipment authenticated docs Tab Code Start here */

            if (CollectionUtils.isNotEmpty(service.getAuthenticatedDocList())) {
                for (AuthenticatedDocs authenticatedDocs : service.getAuthenticatedDocList()) {
                    authenticatedDocs.setShipmentServiceDetail(service);
                }
            } else {
                service.setAuthenticatedDocList(new ArrayList<>());
            }

			/* Shipment authenticated docs Tab Code Ends here */
			
			/* Bill of Entry Tab Code Start here */

            if (CollectionUtils.isNotEmpty(service.getBillOfEntryList())) {
                for (BillOfEntry billOfEntry : service.getBillOfEntryList()) {
                    billOfEntry.setShipmentServiceDetail(service);
                }
            } else {
                service.setBillOfEntryList(new ArrayList<>());
            }

			/*Bill of Entry Tab Code Ends here */

            try {

                if (service.getBookedChargeableUnit() != null) {
                    Double minimum = Double.parseDouble(
                            appUtil.getLocationConfig(SHIPMENT_MIN_CHRG_WEIGHT, service.getLocation(), false));

                    if (service.getBookedChargeableUnit() < minimum) {
                        service.setIsMinimumShipment(YesNo.Yes);
                    } else {
                        service.setIsMinimumShipment(YesNo.No);
                    }
                }

            } catch (Exception e) {
                throw new RestException(ErrorCode.SHIPMENT_MINIMUM_WEIGHT);
            }

        }

        Shipment shipmentRes = shipmentRepository.save(shipment);
        shipmentRes = activitiTransactionService.createShipmentTask(shipmentRes);
       // shipmentRes = shipmentRepository.save(shipmentRes);

        if (shipmentRes != null && CollectionUtils.isNotEmpty(shipmentRes.getShipmentServiceList())) {
            for (ShipmentServiceDetail sd : shipmentRes.getShipmentServiceList()) {
                if (sd.getServiceMaster().getImportExport().equals(ImportExport.Export) && sd.getMawbNo() != null
                        && sd.getMawbNo().trim().length() != 0) {
                    String mawbNo = sd.getMawbNo();
                    StockGeneration sg = stockRepository.findByMawb(mawbNo);
                    if (sg != null && StockStatus.Available.equals(sg.getStockStatus())) {

                        sg.setShipment(shipmentRes);
                        sg.setShipmentUid(shipmentRes.getShipmentUid());
                        sg.setServiceCode(sd.getServiceCode());
                        sg.setStockStatus(StockStatus.Reserved);
                        stockRepository.save(sg);
                    }
                }
            }
        }


        return shipmentRes;

    }


    public SearchRespDto search(ShipmentSearchRequestDto dto) {

        Map<String, Object> params = new HashMap<>();

        log.info("ShipmentSearchImpl.search() method is called...");

        String searchQuery = "select new com.efreightsuite.dto.ShipmentSearchResponseDto ( "
                + " ssd.shipment.id, "
                + " sh.partyName,"
                + " sg.colorCode, "
                + " co.partyName, "
                + " cg.colorCode, "
                + " ship.shipmentUid, "
                + " ship.shipmentReqDate, "
                + " dd.origin.portName, "
                + " dd.destination.portName, "
                + " tos.tosName, "
                + " cre.employeeName, "
                + " lus.serviceStatus, "
                + " dd.hawbNo,"
                + " rl.referenceNumber,"
                + " cs.employeeName)"
                + " from DocumentDetail dd "
                + " left join dd.shipmentServiceDetail ssd "
                + " left join dd.shipper sh"
                + " left join dd.shipper.gradeMaster sg"
                + " left join dd.consignee co"
                + " left join dd.consignee.gradeMaster cg"
                + " left join dd.location loc "
                + " left join dd.country country "
                + " left join ssd.shipment ship "
                + " left join ssd.lastUpdatedStatus lus "
                + " left join ssd.tosMaster tos "
                + " left join ssd.party cust "
                + " left join ssd.salesman salesman "
                + " left join ssd.serviceMaster service "
                + " left join ssd.division division "
                + " left join ship.createdBy cre "
                + " left join ssd.customerService cs "
                + " left join ssd.referenceList rl ";

        String countQuery = "select count(dd.shipmentServiceDetail.shipment.id)"
                + " from DocumentDetail dd "
                + " left join dd.shipmentServiceDetail ssd "
                + " left join dd.shipper sh"
                + " left join dd.shipper.gradeMaster sg"
                + " left join dd.consignee co"
                + " left join dd.consignee.gradeMaster cg"
                + " left join dd.location loc "
                + " left join dd.country country"
                + " left join ssd.shipment ship "
                + " left join ssd.lastUpdatedStatus lus "
                + " left join ssd.tosMaster tos "
                + " left join ssd.party cust "
                + " left join ssd.salesman salesman "
                + " left join ssd.serviceMaster service "
                + " left join ssd.division division "
                + " left join ship.createdBy cre "
                + " left join ssd.customerService cs "
                + " left join ssd.referenceList rl ";

        String filterQuery = "";

        UserProfile userProfile = AuthService.getCurrentUser();

        Set<Long> locationList = new HashSet<>();

        if (userProfile.getSelectedUserLocation() != null) {
            locationList.add(userProfile.getSelectedUserLocation().getId());
        }
        if (userProfile.getRecordAccessLevelProcessDTO() != null) {

            //Location List
            if (userProfile.getRecordAccessLevelProcessDTO().getLocationList() != null
                    && !userProfile.getRecordAccessLevelProcessDTO().getLocationList().isEmpty()) {
                locationList.addAll(userProfile.getRecordAccessLevelProcessDTO().getLocationList());
            }

            //Country List
            if (userProfile.getRecordAccessLevelProcessDTO().getCountryList() != null
                    && !userProfile.getRecordAccessLevelProcessDTO().getCountryList().isEmpty()) {


                String ids = CommonUtils.commaSeparatedIds(userProfile.getRecordAccessLevelProcessDTO().getCountryList());
                if (ids.trim().length() > 0) {
                    if (filterQuery.trim().length() != 0) {
                        filterQuery = filterQuery + " AND ";
                    }
                    filterQuery = filterQuery + " country.id   IN (" + ids + ")";
                }
            }


            // Party List
            if (userProfile.getRecordAccessLevelProcessDTO().getCustomerList() != null
                    && !userProfile.getRecordAccessLevelProcessDTO().getCustomerList().isEmpty()) {
                String ids = CommonUtils.commaSeparatedIds(userProfile.getRecordAccessLevelProcessDTO().getCustomerList());
                if (ids.trim().length() > 0) {
                    if (filterQuery.trim().length() != 0) {
                        filterQuery = filterQuery + " AND ";
                    }
                    filterQuery = filterQuery + " cust.id   IN (" + ids + ")";
                }
            }
            // Salesman List
            if (userProfile.getRecordAccessLevelProcessDTO().getSalesmanList() != null
                    && !userProfile.getRecordAccessLevelProcessDTO().getSalesmanList().isEmpty()) {
                String ids = CommonUtils.commaSeparatedIds(userProfile.getRecordAccessLevelProcessDTO().getSalesmanList());
                if (ids.trim().length() > 0) {
                    if (filterQuery.trim().length() != 0) {
                        filterQuery = filterQuery + " AND ";
                    }
                    filterQuery = filterQuery + " salesman.id   IN (" + ids + ")";
                }
            }

            // Service List
            if (userProfile.getRecordAccessLevelProcessDTO().getServiceList() != null
                    && !userProfile.getRecordAccessLevelProcessDTO().getServiceList().isEmpty()) {
                String ids = CommonUtils.commaSeparatedIds(userProfile.getRecordAccessLevelProcessDTO().getServiceList());
                if (ids.trim().length() > 0) {
                    if (filterQuery.trim().length() != 0) {
                        filterQuery = filterQuery + " AND ";
                    }
                    filterQuery = filterQuery + " service.id   IN (" + ids + ")";
                }
            }
            // Division List
            if (userProfile.getRecordAccessLevelProcessDTO().getDivisionList() != null
                    && !userProfile.getRecordAccessLevelProcessDTO().getDivisionList().isEmpty()) {
                String ids = CommonUtils.commaSeparatedIds(userProfile.getRecordAccessLevelProcessDTO().getDivisionList());
                if (ids.trim().length() > 0) {
                    if (filterQuery.trim().length() != 0) {
                        filterQuery = filterQuery + " AND ";
                    }
                    filterQuery = filterQuery + " division.id   IN (" + ids + ")";
                }
            }
        }


        if (locationList != null && !locationList.isEmpty()) {
            String ids = CommonUtils.commaSeparatedIds(locationList);
            if (ids.trim().length() > 0) {
                if (filterQuery.trim().length() != 0) {
                    filterQuery = filterQuery + " AND ";
                }
                filterQuery = filterQuery + " loc.id   IN (" + ids + ")";
            }
        }

        if (dto.getShipperName() != null && dto.getShipperName().length() != 0) {

            if (filterQuery.trim().length() != 0) {
                filterQuery = filterQuery + " AND ";
            }

            filterQuery += " Upper(sh.partyName) like '" + dto.getShipperName().trim().toUpperCase() + "%' ";
        }

        if (dto.getConsigneeName() != null && dto.getConsigneeName().length() != 0) {

            if (filterQuery.trim().length() != 0) {
                filterQuery = filterQuery + " AND ";
            }

            filterQuery += " Upper(co.partyName) like '" + dto.getConsigneeName().trim().toUpperCase() + "%'";
        }


        if (dto.getShipmentUid() != null && dto.getShipmentUid().length() != 0) {

            if (filterQuery.trim().length() != 0) {
                filterQuery = filterQuery + " AND ";
            }

            filterQuery += " Upper(ship.shipmentUid) like '" + dto.getShipmentUid().trim().toUpperCase() + "%'";
        }


        if (dto.getShipmentDate() != null && dto.getShipmentDate().getStartDate() != null && dto.getShipmentDate().getEndDate() != null) {

            try {

                Date date1 = sdf.parse(dto.getShipmentDate().getStartDate());
                Date date2 = sdf.parse(dto.getShipmentDate().getEndDate());

                if (filterQuery.trim().length() != 0) {
                    filterQuery = filterQuery + " AND ";
                }

                params.put("startDate", date1);
                params.put("endDate", date2);

                filterQuery = filterQuery + "( ship.shipmentReqDate  >= :startDate AND ship.shipmentReqDate  <= :endDate )";

            } catch (ParseException exception) {
                log.error("Date Conversion Exception in shipmentReqDate....", exception);
            }

        }


        if (dto.getOriginName() != null && dto.getOriginName().length() != 0) {

            if (filterQuery.trim().length() != 0) {
                filterQuery = filterQuery + " AND ";
            }

            filterQuery += " Upper(dd.origin.portName) like '" + dto.getOriginName().trim().toUpperCase() + "%'";
        }

        if (dto.getDestinationName() != null && dto.getDestinationName().length() != 0) {

            if (filterQuery.trim().length() != 0) {
                filterQuery = filterQuery + " AND ";
            }

            filterQuery += " Upper(dd.destination.portName) like '" + dto.getDestinationName().trim().toUpperCase() + "%'";
        }

        if (dto.getTosName() != null && dto.getTosName().length() != 0) {

            if (filterQuery.trim().length() != 0) {
                filterQuery = filterQuery + " AND ";
            }

            filterQuery += " Upper(tos.tosName) like '" + dto.getTosName().trim().toUpperCase() + "%'";
        }

        if (dto.getServiceStatus() != null && dto.getServiceStatus().length() != 0) {

            if (filterQuery.trim().length() != 0) {
                filterQuery = filterQuery + " AND ";
            }

            filterQuery += " lus.serviceStatus like '" + dto.getServiceStatus().trim() + "%'";
        }

        if (dto.getImportExport() != null && dto.getImportExport().trim().length() != 0 && (dto.getImportExport().trim().equals("Import") || dto.getImportExport().trim().equals("Export"))) {

            if (filterQuery.trim().length() != 0) {
                filterQuery = filterQuery + " AND ";
            }

            filterQuery += " Upper(service.importExport) like '" + dto.getImportExport().trim().toUpperCase() + "%'";
        }

        String advancedFilterQuery = "";

        if (dto.getSearchValue() != null && dto.getSearchValue().trim().length() != 0) {


            if (dto.getSearchName() != null && dto.getSearchName().trim().equals("Hawb No")) {

                advancedFilterQuery = " WHERE Upper(dd.hawbNo) like '" + dto.getSearchValue().trim().toUpperCase() + "%' ";

                if (dto.getHawbNo() != null && dto.getHawbNo().trim().length() != 0) {

                    advancedFilterQuery = advancedFilterQuery + " AND Upper(dd.hawbNo) like '" + dto.getHawbNo().trim().toUpperCase() + "%'";

                }


            } else if (dto.getSearchName() != null && dto.getSearchName().trim().equals("Shipper Reference No")) {

                advancedFilterQuery = " WHERE  rl.referenceTypeMaster.referenceType = 'Shipper_Reference_No' AND UPPER(rl.referenceNumber) like '" + dto.getSearchValue().trim().toUpperCase() + "%'";

                if (dto.getShipperReferenceNumber() != null && dto.getShipperReferenceNumber().trim().length() != 0) {

                    advancedFilterQuery = advancedFilterQuery + " AND rl.referenceTypeMaster.referenceType = 'Shipper_Reference_No' AND UPPER(rl.referenceNumber) like '" + dto.getShipperReferenceNumber().trim().toUpperCase() + "%'";

                }


            } else if (dto.getSearchName() != null && dto.getSearchName().trim().equals("Customer Order No")) {

                advancedFilterQuery = "  WHERE rl.referenceTypeMaster.referenceType = 'Customer_Order_No' AND UPPER(rl.referenceNumber) like '" + dto.getSearchValue().trim().toUpperCase() + "%'";

                if (dto.getCustomerOrderNumber() != null && dto.getCustomerOrderNumber().trim().length() != 0) {

                    advancedFilterQuery = advancedFilterQuery + " AND rl.referenceTypeMaster.referenceType = 'Customer_Order_No' AND UPPER(rl.referenceNumber) like '" + dto.getCustomerOrderNumber().trim().toUpperCase() + "%'";

                }

            } else if (dto.getSearchName() != null && dto.getSearchName().trim().equals("Created By")) {

                advancedFilterQuery = " WHERE Upper(cre.employeeName) like '" + dto.getSearchValue().trim().toUpperCase() + "%' ";

                if (dto.getEmployeeName() != null && dto.getEmployeeName().length() != 0) {

                    advancedFilterQuery = advancedFilterQuery + " AND Upper(cre.employeeName) like '" + dto.getEmployeeName().trim().toUpperCase() + "%'";

                }


            } else {

                advancedFilterQuery = " WHERE Upper(cs.employeeName) like '" + dto.getSearchValue().trim().toUpperCase() + "%' ";

                if (dto.getCustomerService() != null && dto.getCustomerService().length() != 0) {

                    advancedFilterQuery = advancedFilterQuery + " AND Upper(cs.employeeName) like '" + dto.getCustomerService().trim().toUpperCase() + "%'";

                }

            }


            if (filterQuery.trim().length() != 0) {
                advancedFilterQuery = advancedFilterQuery + " AND " + filterQuery;
            }


        } else {

            if (filterQuery.trim().length() != 0) {
                filterQuery = " WHERE " + filterQuery;
            }
        }

        if (advancedFilterQuery.trim().length() != 0) {
            filterQuery = advancedFilterQuery;
        }


        String orderByQuery = "";

        if (dto.getSortByColumn() != null && dto.getSortByColumn().length() != 0) {

            if (dto.getSortByColumn().equalsIgnoreCase("shipperName")) {
                orderByQuery = orderByQuery + "sh.partyName";
            } else if (dto.getSortByColumn().equalsIgnoreCase("consigneeName")) {
                orderByQuery = orderByQuery + "co.partyName";
            } else if (dto.getSortByColumn().equalsIgnoreCase("shipmentUid")) {
                orderByQuery = orderByQuery + "ship.shipmentUid";
            } else if (dto.getSortByColumn().equalsIgnoreCase("shipmentReqDate")) {
                orderByQuery = orderByQuery + "ship.shipmentReqDate";
            } else if (dto.getSortByColumn().equalsIgnoreCase("originName")) {
                orderByQuery = orderByQuery + "dd.origin.portName";
            } else if (dto.getSortByColumn().equalsIgnoreCase("destinationName")) {
                orderByQuery = orderByQuery + "dd.destination.portName";
            } else if (dto.getSortByColumn().equalsIgnoreCase("tosName")) {
                orderByQuery = orderByQuery + "tos.tosName";
            }

        }

        if (orderByQuery.length() != 0) {
            orderByQuery = " ORDER BY " + orderByQuery + " " + dto.getOrderByType();
        } else {
            orderByQuery = " ORDER BY ssd.id DESC ";
        }


        countQuery = countQuery + filterQuery;
        Query countQ = em.createQuery(countQuery);

        log.info("HQL Count Query For Search : " + countQuery);
        log.info("HQL params For Search : " + params);
        for (Entry<String, Object> e : params.entrySet()) {

            countQ.setParameter(e.getKey(), e.getValue());
        }
        Long countTotal = (Long) countQ.getSingleResult();
        log.info("Total Number of Records Found : " + countTotal);


        searchQuery = searchQuery + filterQuery + orderByQuery;
        log.info("HQL Query For Search : " + searchQuery);
        Query query = em.createQuery(searchQuery);

        if(dto.getSelectedPageNumber()>=0 && dto.getRecordPerPage()>=0 ) {
            query.setFirstResult(dto.getSelectedPageNumber() * dto.getRecordPerPage());
            query.setMaxResults(dto.getRecordPerPage());
        }

        for (Entry<String, Object> e : params.entrySet()) {
            query.setParameter(e.getKey(), e.getValue());
        }


        return new SearchRespDto(countTotal, query.getResultList());
    }

    public List<ShipmentServiceDetail> shipmentLink(ConsolDto consolDto) {
        Map<String, Object> params = new HashMap<>();
        Map<String, Object> transParams = new HashMap<>();

        String hql = "";
        String transhipment = "";

        ServiceMaster serviceMaster = cacheRepository.get(SaaSUtil.getSaaSId(), ServiceMaster.class.getName(), consolDto.getServiceId(), serviceMasterRepository);

        if (serviceMaster.getImportExport() == ImportExport.Export) {
            if (consolDto.getConsolId() == null) {
                hql = " where consolUid is NULL and lastUpdatedStatus.serviceStatus IN ('Received') ";
            } else {
                hql = " where ( consolUid=:consolUid OR consolUid is NULL ) and lastUpdatedStatus.serviceStatus IN ('Received') ";
                params.put("consolUid", consolDto.getConsolId());
            }

            transhipment = " select con from ServiceConnection con where con.move='Air' AND con.shipmentServiceDetail.lastUpdatedStatus.serviceStatus IN ('Generated') AND con.shipmentServiceDetail.serviceMaster.transportMode='Air' AND con.shipmentServiceDetail.serviceMaster.importExport='Import' AND con.shipmentServiceDetail.serviceMaster.serviceType is NULL AND con.shipmentServiceDetail.isTranshipment='Yes' AND con.shipmentServiceDetail.transhipIsDoneLocation='No' ";

        } else {
            if (consolDto.getConsolId() == null) {
                hql = " where consolUid is NULL and lastUpdatedStatus.serviceStatus IN ('Booked') ";
            } else {
                hql = " where ( consolUid=:consolUid OR consolUid is NULL ) and lastUpdatedStatus.serviceStatus IN ('Booked') ";
                params.put("consolUid", consolDto.getConsolId());
            }

            if (consolDto.getMawbNo() != null) {
                if (hql.trim().length() != 0) {
                    hql = hql + " AND ";
                }
                hql = hql + " mawbNo=:mawbNo ";
                params.put("mawbNo", consolDto.getMawbNo());
            }

        }

        if (consolDto.getCarrierId() != null) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }
            hql = hql + " carrier.id=:carrierId ";
            params.put("carrierId", consolDto.getCarrierId());

            if (transhipment != null && transhipment.trim().length() != 0) {
                if (transhipment.trim().length() != 0) {
                    transhipment = transhipment + " AND ";
                }
                transhipment = transhipment + " con.carrierMaster.id=:carrierId ";
                transParams.put("carrierId", consolDto.getCarrierId());
            }

        }

        if (consolDto.getServiceId() != null && serviceMaster.getImportExport() == ImportExport.Export) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }
            hql = hql + " serviceMaster.id=:serviceMasterId ";
            params.put("serviceMasterId", consolDto.getServiceId());

        } else if (consolDto.getServiceId() != null && serviceMaster.getImportExport() == ImportExport.Import) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }
            hql = hql + " serviceMaster.transportMode=:transportMode AND serviceMaster.importExport=:importExport ";
            params.put("transportMode", TransportMode.Air);
            params.put("importExport", ImportExport.Import);

        }

        if (consolDto.getPolId() != null) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }
            hql = hql + " pol.id=:polId ";
            params.put("polId", consolDto.getPolId());


            if (transhipment != null && transhipment.trim().length() != 0) {
                if (transhipment.trim().length() != 0) {
                    transhipment = transhipment + " AND ";
                }
                transhipment = transhipment + " con.pol.id=:polId ";
                transParams.put("polId", consolDto.getPolId());
            }

        }

        if (consolDto.getPodId() != null) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }
            hql = hql + " pod.id=:podId ";
            params.put("podId", consolDto.getPodId());

            if (transhipment != null && transhipment.trim().length() != 0) {
                if (transhipment.trim().length() != 0) {
                    transhipment = transhipment + " AND ";
                }
                transhipment = transhipment + " con.pod.id=:podId ";
                transParams.put("podId", consolDto.getPodId());
            }
        }

        if (consolDto.getLocationId() != null) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }
            hql = hql + " location.id=:locationId ";
            params.put("locationId", consolDto.getLocationId());


            if (transhipment != null && transhipment.trim().length() != 0) {
                if (transhipment.trim().length() != 0) {
                    transhipment = transhipment + " AND ";
                }
                transhipment = transhipment + " con.shipmentServiceDetail.location.id=:locationId ";
                transParams.put("locationId", consolDto.getLocationId());
            }
        }

        if (consolDto.getEtd() != null) {

            Date dt1 = new Date(consolDto.getEtd().getTime());
            dt1.setHours(0);
            dt1.setMinutes(0);
            dt1.setSeconds(0);

            Date dt2 = new Date(consolDto.getEtd().getTime());
            dt2.setHours(23);
            dt2.setMinutes(59);
            dt2.setSeconds(59);

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }

            params.put(ETD_START_DATE, dt1);
            params.put(ETD_END_DATE, dt2);
            hql = hql + " ( etd >= :etdStartDate AND etd <= :etdEndDate ) ";


            if (transhipment != null && transhipment.trim().length() != 0) {
                if (transhipment.trim().length() != 0) {
                    transhipment = transhipment + " AND ";
                }
                transhipment = transhipment + " ( con.etd >= :etdStartDate AND con.etd <= :etdEndDate ) ";

                transParams.put(ETD_START_DATE, dt1);
                transParams.put(ETD_END_DATE, dt2);
            }

        }

        if (consolDto.getEta() != null) {

            Date dt1 = new Date(consolDto.getEta().getTime());
            dt1.setHours(0);
            dt1.setMinutes(0);
            dt1.setSeconds(0);
            Date dt2 = new Date(consolDto.getEta().getTime());
            dt2.setHours(23);
            dt2.setMinutes(59);
            dt2.setSeconds(59);

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }

            params.put(ETA_START_DATE, dt1);
            params.put(ETA_END_DATE, dt2);
            hql = hql + " ( eta >= :etaStartDate AND eta <= :etaEndDate ) ";

            if (transhipment != null && transhipment.trim().length() != 0) {
                if (transhipment.trim().length() != 0) {
                    transhipment = transhipment + " AND ";
                }
                transhipment = transhipment + " ( con.eta >= :etaStartDate AND con.eta <= :etaEndDate ) ";

                transParams.put(ETA_START_DATE, dt1);
                transParams.put(ETA_END_DATE, dt2);
            }

        }

        hql = " from ShipmentServiceDetail " + hql + " order by shipment.id asc";
		
		/*Without Transhipment - Start here*/
        TypedQuery<ShipmentServiceDetail> query = em.createQuery(hql, ShipmentServiceDetail.class);
        for (Entry<String, Object> e : params.entrySet()) {
            query.setParameter(e.getKey(), e.getValue());
        }


        List<ShipmentServiceDetail> tmpList = query.getResultList();
        if (tmpList != null) {
            for (ShipmentServiceDetail obj : tmpList) {
                obj.touch();
            }
        } else {
            tmpList = new ArrayList<>();
        }
		

		/*Without Transhipment - End here*/
		
		/*With Transhipment - Start here*/
        List<ServiceConnection> tmpConList = null;
        if (transhipment != null && transhipment.trim().length() > 0) {
            TypedQuery<ServiceConnection> conquery = em.createQuery(transhipment, ServiceConnection.class);
            for (Entry<String, Object> e : transParams.entrySet()) {
                conquery.setParameter(e.getKey(), e.getValue());
            }

            tmpConList = conquery.getResultList();
            if (tmpConList != null) {
                for (ServiceConnection obj : tmpConList) {
                    obj.getShipmentServiceDetail().touch();
                    tmpList.add(obj.getShipmentServiceDetail());
                }
            }


        }
		/*Without Transhipment - End here*/


        return tmpList;

    }


    public SearchRespDto importExportSearch(ImportExportSearchDto dto) {

        Map<String, Object> params = new HashMap<>();

        PartyMaster agent = AuthService.getCurrentUser().getSelectedUserLocation().getPartyMaster();

        Long agentId = agent != null ? agent.getId() : 0;


        String standardCondition = "";
        if (dto.getSearchType() != null && dto.getSearchType().equals(SearchType.All)) {
            standardCondition += " where dd.agent.id = " + agentId + "and dd.shipmentServiceDetail.serviceMaster.importExport IN('" + ImportExport.Import + "')";
        } else {
            standardCondition += " where dd.agent.id = " + agentId + "and dd.shipmentServiceDetail.serviceMaster.importExport = '" + ImportExport.Import + "' and dd.shipmentServiceDetail.lastUpdatedStatus.serviceStatus in ('" + com.efreightsuite.enumeration.ServiceStatus.Booked + "') and dd.shipmentServiceDetail.exportRef is null";

        }

        searchQuery.append(standardCondition);


        String filterQuery = " (ssd.isFreehandShipment is null OR ssd.isFreehandShipment = 'No') ";

        if (dto.getLocation() != null && dto.getLocation().trim().length() != 0) {
            if (filterQuery.trim().length() != 0) {
                filterQuery = filterQuery + " AND ";
            }
            filterQuery = filterQuery + " Upper(loc.locationName) like '" + dto.getLocation().trim().toUpperCase() + "%' ";
        }

        if (dto.getServiceUid() != null && dto.getServiceUid().trim().length() != 0) {
            if (filterQuery.trim().length() != 0) {
                filterQuery = filterQuery + " AND ";
            }
            filterQuery = filterQuery + " Upper(ssd.serviceUid) like '" + dto.getServiceUid().trim().toUpperCase() + "%' ";
        }

        if (dto.getNominationDate() != null && dto.getNominationDate().getStartDate() != null && dto.getNominationDate().getEndDate() != null) {

            try {
                Date dt1 = sdf.parse(dto.getNominationDate().getStartDate());
                Date dt2 = sdf.parse(dto.getNominationDate().getEndDate());

                if (filterQuery.trim().length() != 0) {
                    filterQuery = filterQuery + " AND ";
                }

                params.put("nominationStartDate", dt1);
                params.put("nominationEndDate", dt2);

                filterQuery = filterQuery + " ( ssd.serviceReqDate >= :nominationStartDate AND ssd.serviceReqDate <= :nominationEndDate ) ";

            } catch (ParseException e1) {
                log.error("Date Conversion error in ImportExport Search serviceReqDate: ", e1);
            }
        }


        if (dto.getShipperName() != null && dto.getShipperName().trim().length() != 0) {
            if (filterQuery.trim().length() != 0) {
                filterQuery = filterQuery + " AND ";
            }
            filterQuery = filterQuery + " Upper(sh.partyName) like '" + dto.getShipperName().trim().toUpperCase() + "%' ";
        }

        if (dto.getConsigneeName() != null && dto.getConsigneeName().trim().length() != 0) {
            if (filterQuery.trim().length() != 0) {
                filterQuery = filterQuery + " AND ";
            }
            filterQuery = filterQuery + " Upper(co.partyName) like '" + dto.getConsigneeName().trim().toUpperCase() + "%' ";
        }

        if (dto.getShipmentUid() != null && dto.getShipmentUid().trim().length() != 0) {

            if (filterQuery.trim().length() != 0) {
                filterQuery = filterQuery + " AND ";
            }
            filterQuery = filterQuery + " Upper(ssd.shipmentUid) like '" + dto.getShipmentUid().trim().toUpperCase() + "%' ";
        }

        if (dto.getService() != null && dto.getService().trim().length() != 0) {

            if (filterQuery.trim().length() != 0) {
                filterQuery = filterQuery + " AND ";
            }
            filterQuery = filterQuery + " Upper(ssd.serviceMaster.serviceName) like '" + dto.getService().trim().toUpperCase() + "%' ";
        }

        if (dto.getHawbNo() != null && dto.getHawbNo().trim().length() != 0) {

            if (filterQuery.trim().length() != 0) {
                filterQuery = filterQuery + " AND ";
            }
            filterQuery = filterQuery + " Upper(dd.hawbNo) like '" + dto.getHawbNo().trim().toUpperCase() + "%' ";
        }

        if (dto.getExportRef() != null && dto.getExportRef().trim().length() != 0) {

            if (filterQuery.trim().length() != 0) {
                filterQuery = filterQuery + " AND ";
            }
            filterQuery = filterQuery + " Upper(ssd.exportRef) like '" + dto.getExportRef().trim().toUpperCase() + "%' ";
        }


        if (dto.getOrigin() != null && dto.getOrigin().trim().length() != 0) {

            if (filterQuery.trim().length() != 0) {
                filterQuery = filterQuery + " AND ";
            }
            filterQuery = filterQuery + " Upper(ssd.origin.portName) like '" + dto.getOrigin().trim().toUpperCase() + "%' ";
        }

        if (dto.getDestination() != null && dto.getDestination().trim().length() != 0) {

            if (filterQuery.trim().length() != 0) {
                filterQuery = filterQuery + " AND ";
            }

            filterQuery = filterQuery + " Upper(ssd.destination.portName) like '" + dto.getDestination().trim().toUpperCase() + "%' ";
        }

        if (dto.getEtd() != null && dto.getEtd().getStartDate() != null && dto.getEtd().getEndDate() != null) {

            try {
                Date dt1 = sdf.parse(dto.getEtd().getStartDate());
                Date dt2 = sdf.parse(dto.getEtd().getEndDate());

                if (filterQuery.trim().length() != 0) {
                    filterQuery = filterQuery + " AND ";
                }

                params.put(ETD_START_DATE, dt1);
                params.put(ETD_END_DATE, dt2);

                filterQuery = filterQuery + " ( ssd.etd >= :etdStartDate AND ssd.etd <= :etdEndDate ) ";

            } catch (ParseException e1) {
                log.error("Date Conversion error in ImportExport Search etd on: ", e1);
            }
        }


        if (dto.getEta() != null && dto.getEta().getStartDate() != null && dto.getEta().getEndDate() != null) {

            try {
                Date dt1 = sdf.parse(dto.getEta().getStartDate());
                Date dt2 = sdf.parse(dto.getEta().getEndDate());

                if (filterQuery.trim().length() != 0) {
                    filterQuery = filterQuery + " AND ";
                }

                params.put(ETA_START_DATE, dt1);
                params.put(ETA_END_DATE, dt2);

                filterQuery = filterQuery + " ( ssd.eta >= :etaStartDate AND ssd.eta <= :etaEndDate ) ";

            } catch (ParseException e1) {
                log.error("Date Conversion error in ImportExport Search eta on: ", e1);
            }

        }

        if (filterQuery.trim().length() != 0) {
            filterQuery = " AND " + filterQuery;
        }


        String orderByQuery = "";

        if (dto.getSortByColumn() != null && dto.getSortByColumn().length() != 0) {
            if (dto.getSortByColumn().equalsIgnoreCase(LOCATION_NAME)) {
                orderByQuery = orderByQuery + "loc.locationName";
            } else if (dto.getSortByColumn().equalsIgnoreCase(SHIPMENT_UID)) {
                orderByQuery = orderByQuery + "ssd.shipmentUid";
            } else if (dto.getSortByColumn().equalsIgnoreCase(SERVICE_NAME)) {
                orderByQuery = orderByQuery + "sm.serviceName";
            } else if (dto.getSortByColumn().equalsIgnoreCase(SERVICE_UID)) {
                orderByQuery = orderByQuery + "ssd.serviceUid";
            } else if (dto.getSortByColumn().equalsIgnoreCase(SERVICE_REQ_DATE)) {
                orderByQuery = orderByQuery + "ssd.serviceReqDate";
            } else if (dto.getSortByColumn().equalsIgnoreCase(SHIPPER_NAME)) {
                orderByQuery = orderByQuery + "sh.partyName";
            } else if (dto.getSortByColumn().equalsIgnoreCase(CONSIGNEE_NAME)) {
                orderByQuery = orderByQuery + "co.partyName";
            } else if (dto.getSortByColumn().equalsIgnoreCase(ORIGIN_NAME)) {
                orderByQuery = orderByQuery + "prt.portName";
            } else if (dto.getSortByColumn().equalsIgnoreCase(DESTINATION_NAME)) {
                orderByQuery = orderByQuery + "dst.portName";
            } else if (dto.getSortByColumn().equalsIgnoreCase(ETA)) {
                orderByQuery = orderByQuery + "ssd.eta";
            } else if (dto.getSortByColumn().equalsIgnoreCase(ETD)) {
                orderByQuery = orderByQuery + "ssd.etd";
            } else if (dto.getSortByColumn().equalsIgnoreCase(HAWB_NO)) {
                orderByQuery = orderByQuery + "ssd.hawbNo";
            } else if (dto.getSortByColumn().equalsIgnoreCase(EXPORT_REF)) {
                orderByQuery = orderByQuery + "ssd.exportRef";
            } else {
                orderByQuery = orderByQuery + "ssd.serviceReqDate";
            }

        }

        if (orderByQuery.length() != 0) {
            orderByQuery = " ORDER BY " + orderByQuery + " " + dto.getOrderByType();
        } else {
            orderByQuery = " ORDER BY ssd.serviceReqDate DESC ";
        }

        searchQuery.append(filterQuery).append(orderByQuery);

        log.info("Search Query : " + searchQuery);

        Query query = em.createQuery(searchQuery.toString());

        for (Entry<String, Object> e : params.entrySet()) {
            query.setParameter(e.getKey(), e.getValue());
        }

        query.setFirstResult(dto.getSelectedPageNumber() * dto.getRecordPerPage());
        query.setMaxResults(dto.getRecordPerPage());

        countQ.append(standardCondition).append(filterQuery);

        Query countQuery = em.createQuery(countQ.toString());

        for (Entry<String, Object> e : params.entrySet()) {
            countQuery.setParameter(e.getKey(), e.getValue());
        }

        Long countTotal = (Long) countQuery.getSingleResult();

        return new SearchRespDto(countTotal, query.getResultList());


    }

    public SearchRespDto search(CopyShipmentSearchDto dto) {

        Map<String, Object> params = new HashMap<>();

        String searchQuery = "select new com.efreightsuite.dto.CopyShipmentSearchResponseDto"
                + " (dd.id, dd.origin.portName, dd.destination.portName, dd.shipper.partyName, dd.consignee.partyName, tos.tosName, sh.shipmentUid, sh.shipmentReqDate, ssd.id) "
                + " from DocumentDetail dd "
                + " left join dd.origin ori "
                + " left join dd.destination dest "
                + " left join dd.shipper ship "
                + " left join dd.consignee cons "
                + " left join dd.shipmentServiceDetail ssd"
                + " left join ssd.shipment sh"
                + " left join ssd.tosMaster tos"
                + " where dd.shipper.id = " + dto.getPartyId() + " and ssd.serviceMaster.id = " + dto.getServiceId() + " and ssd.location.id = " + AuthService.getCurrentUser().getSelectedUserLocation().getId();

        String countQ = "select count(dd.id) from DocumentDetail dd "
                + " left join dd.origin ori "
                + " left join dd.destination dest "
                + " left join dd.shipper ship "
                + " left join dd.consignee cons "
                + " left join dd.shipmentServiceDetail ssd"
                + " left join ssd.shipment sh"
                + " left join ssd.tosMaster tos"
                + " where dd.shipper.id = " + dto.getPartyId() + " and ssd.serviceMaster.id = " + dto.getServiceId() + " and ssd.location.id = " + AuthService.getCurrentUser().getSelectedUserLocation().getId();


        String filterQuery = "";

        if (dto.getOriginName() != null && dto.getOriginName().trim().length() != 0) {
            if (filterQuery.trim().length() != 0) {
                filterQuery = filterQuery + " AND ";
            }

            filterQuery = filterQuery + " Upper(dd.origin.portName) like '" + dto.getOriginName().trim().toUpperCase() + "%' ";
        }


        if (dto.getDestinationName() != null && dto.getDestinationName().trim().length() != 0) {
            if (filterQuery.trim().length() != 0) {
                filterQuery = filterQuery + " AND ";
            }

            filterQuery = filterQuery + " Upper(dd.destination.portName) like '" + dto.getDestinationName().trim().toUpperCase() + "%' ";
        }


        if (dto.getShipperName() != null && dto.getShipperName().trim().length() != 0) {
            if (filterQuery.trim().length() != 0) {
                filterQuery = filterQuery + " AND ";
            }

            filterQuery = filterQuery + " Upper(dd.shipper.partyName) like '" + dto.getShipperName().trim().toUpperCase() + "%' ";
        }


        if (dto.getConsigneeName() != null && dto.getConsigneeName().trim().length() != 0) {
            if (filterQuery.trim().length() != 0) {
                filterQuery = filterQuery + " AND ";
            }

            filterQuery = filterQuery + " Upper(dd.consignee.partyName) like '" + dto.getConsigneeName().trim().toUpperCase() + "%' ";
        }


        if (dto.getTosName() != null && dto.getTosName().trim().length() != 0) {
            if (filterQuery.trim().length() != 0) {
                filterQuery = filterQuery + " AND ";
            }

            filterQuery = filterQuery + " Upper(tos.tosName) like '" + dto.getTosName().trim().toUpperCase() + "%' ";
        }

        if (dto.getShipmentUid() != null && dto.getShipmentUid().trim().length() != 0) {
            if (filterQuery.trim().length() != 0) {
                filterQuery = filterQuery + " AND ";
            }

            filterQuery = filterQuery + " Upper(sh.shipmentUid) like '" + dto.getShipmentUid().trim().toUpperCase() + "%' ";
        }

        if (dto.getShipmentDate() != null && dto.getShipmentDate().getStartDate() != null && dto.getShipmentDate().getEndDate() != null) {

            try {
                Date dt1 = sdf.parse(dto.getShipmentDate().getStartDate());
                Date dt2 = sdf.parse(dto.getShipmentDate().getEndDate());

                if (filterQuery.trim().length() != 0) {
                    filterQuery = filterQuery + " AND ";
                }

                params.put("startDate", dt1);
                params.put("endDate", dt2);

                filterQuery = filterQuery + " ( sh.shipmentReqDate >= :startDate AND sh.shipmentReqDate <= :endDate ) ";

            } catch (ParseException e1) {
                log.error("Date Conversion error in Enquiry Search logged on: ", e1);
            }

        }


        if (filterQuery.trim().length() != 0) {
            filterQuery = " AND " + filterQuery;
        }


        countQ = countQ + filterQuery + " order by sh.shipmentUid DESC ";
        Query countQuery = em.createQuery(countQ);
        for (Entry<String, Object> e : params.entrySet()) {
            countQuery.setParameter(e.getKey(), e.getValue());
        }
        Long countTotal = (Long) countQuery.getSingleResult();


        searchQuery = searchQuery + filterQuery + " order by sh.shipmentUid DESC ";
        Query query = em.createQuery(searchQuery);
        for (Entry<String, Object> e : params.entrySet()) {
            query.setParameter(e.getKey(), e.getValue());
        }
        query.setFirstResult(dto.getSelectedPageNumber() * dto.getRecordPerPage());
        query.setMaxResults(dto.getRecordPerPage());


        return new SearchRespDto(countTotal, query.getResultList());


    }


    public SearchRespDto search(SearchRequest dto) {

        String searchQuery = "select new com.efreightsuite.dto.ShipmentLovResponseDto"
                + " (ship.id, ship.shipmentUid, ship.lastUpdatedStatus.shipmentStatus) "
                + " from Shipment ship "
                + " left join ship.lastUpdatedStatus status ";
        String countQ = "select count(ship.id)"
                + " from Shipment ship "
                + " left join ship.lastUpdatedStatus status ";


        String filterQuery = "";

        if (dto.getKeyword() != null && dto.getKeyword().trim().length() != 0) {
            filterQuery = filterQuery + " ship.shipmentUid like '" + dto.getKeyword().trim().toUpperCase() + "%' ";
        }

        if (filterQuery != "") {
            filterQuery = " WHERE " + filterQuery;
        }

        searchQuery = searchQuery + filterQuery + " order by ship.shipmentUid ASC";
        countQ = countQ + filterQuery;

        Query countQuery = em.createQuery(countQ);

        Long countTotal = (Long) countQuery.getSingleResult();

        Query query = em.createQuery(searchQuery);

        query.setFirstResult(dto.getSelectedPageNumber() * dto.getRecordPerPage());
        query.setMaxResults(dto.getRecordPerPage());

        return new SearchRespDto(countTotal, query.getResultList());

    }


    public SearchRespDto searchService(SearchRequest dto) {

        String searchQuery = "select new com.efreightsuite.dto.ShipmentServiceLovResponseDto"
                + " (s.id, s.shipment.shipmentUid, s.shipment.id, s.serviceUid, s.origin.portCode, s.destination.portCode, s.serviceMaster.serviceName) "
                + " from ShipmentServiceDetail s "
                + " left join s.shipment sh ";
        String countQ = "select count(s.id)"
                + " from ShipmentServiceDetail s "
                + " left join s.shipment sh";


        String filterQuery = " s.serviceMaster.transportMode = 'Air' AND s.serviceMaster.importExport='Export' AND s.serviceMaster.serviceType is NULL AND s.country.id = " + AuthService.getCurrentUser().getSelectedUserLocation().getCountryMaster().getId();

        if (dto.getKeyword() != null && dto.getKeyword().trim().length() != 0) {

            if (filterQuery.trim().length() != 0) {
                filterQuery = filterQuery + "AND ";
            }

            filterQuery = filterQuery + " s.shipment.shipmentUid like '" + dto.getKeyword().trim().toUpperCase() + "%' ";
        }

        if (filterQuery != "") {
            filterQuery = " WHERE " + filterQuery;
        }

        searchQuery = searchQuery + filterQuery + " order by s.shipment.shipmentUid DESC";

        countQ = countQ + filterQuery;

        log.info("Count Query : " + countQ);

        Query countQuery = em.createQuery(countQ);

        Long countTotal = (Long) countQuery.getSingleResult();

        log.info("Count Query Result : " + countTotal);

        Query query = em.createQuery(searchQuery);

        log.info("Search Query : " + query);

        query.setFirstResult(dto.getSelectedPageNumber() * dto.getRecordPerPage());

        query.setMaxResults(dto.getRecordPerPage());

        return new SearchRespDto(countTotal, query.getResultList());

    }

    private boolean dynamicField(String fieldId, Long countryId) {

        DynamicFields dy = dynamicFieldRepository.findByFieldId(fieldId);

        if (CollectionUtils.isNotEmpty(dy.getCountryMasterList())) {
            for (CountryMaster cm : dy.getCountryMasterList()) {
                if (Objects.equals(cm.getId(), countryId)) {
                    return true;
                }
            }
        }

        return false;
    }

    @Transactional
    public void importToExport(ImportToExportDto importToExportDto, BaseDto baseDto) {

        ShipmentServiceDetail importService = shipmentServiceDetailRepository.findByServiceUid(importToExportDto.getServiceUid());

        if (importService.getExportRef() != null) {
            throw new RestException(ErrorCode.IMPORT_TO_EXPORT_SERVICE_EXIST);
        }

        Shipment shipment = shipmentRepository.findByShipmentUid(importService.getShipmentUid());

        ShipmentServiceDetail exportService = getNewExportShipmentServiceDetail(shipment, importService, importToExportDto);

        shipment.getShipmentServiceList().add(exportService);

        exportService.setShipment(shipment);
        exportService.setImportRef(importService.getServiceUid());
        importService.setExportRef(exportService.getServiceUid());


        ShipmentServiceDetail sh = shipmentServiceDetailRepository.save(exportService);

        if (CollectionUtils.isNotEmpty(sh.getDocumentList())) {
            for (int i = 0; i < sh.getDocumentList().size(); i++) {
                importService.getDocumentList().get(i).setHawbNo(sh.getDocumentList().get(i).getHawbNo());
            }
        }

        shipmentServiceDetailRepository.save(importService);


        Shipment tmpShipment = shipmentRepository.findById(shipment.getId());

        baseDto.setResponseCode(ErrorCode.SUCCESS);
        baseDto.setResponseObject(tmpShipment);


    }

    private ShipmentServiceDetail getNewExportShipmentServiceDetail(Shipment shipment, ShipmentServiceDetail serviceImport, ImportToExportDto importToExportDto) {

        log.info("Start ShipmentService.getNewExportShipmentServiceDetail");


        log.info("Found the service to import : " + serviceImport.getId());

        ServiceMapping sm = serviceMappingRepository.findByExport(serviceImport.getServiceMaster().getId());

        if (sm == null) {
            throw new RestException(ErrorCode.SERVICE_MAPPING_NOT_DONE);
        }

        AgentPortMaster apm = agentPortMasterRepository.serachByServicePort(sm.getExportService().getId(), serviceImport.getDestination().getId());

        if (apm == null || apm.getAgent() == null) {
            throw new RestException(ErrorCode.NO_AGENT_FOUND);
        }

        UserProfile profile = AuthService.getCurrentUser();

        LocationMaster location = locationMasterRepository.findById(profile.getSelectedUserLocation().getId());

        ShipmentServiceDetail serviceExport = new ShipmentServiceDetail();


        //initial status - start
        serviceExport.setServiceStatusList(new ArrayList<>());

        ServiceStatus serviceStatus = new ServiceStatus();
        serviceStatus.setShipmentServiceDetail(serviceExport);
        serviceStatus.setServiceStatus(com.efreightsuite.enumeration.ServiceStatus.Booked);
        serviceExport.getServiceStatusList().add(serviceStatus);
        serviceExport.setLastUpdatedStatus(serviceStatus);
        //initial status - End


        serviceExport.setCompany(location.getCompanyMaster());
        serviceExport.setCompanyCode(location.getCompanyMaster().getCompanyCode());

        serviceExport.setLocation(location);
        serviceExport.setLocationCode(location.getLocationCode());

        serviceExport.setCountry(location.getCountryMaster());
        serviceExport.setCountryCode(location.getCountryMaster().getCountryCode());

        serviceExport.setLocalCurrency(location.getCurrencyMaster());
        serviceExport.setLocalCurrencyCode(location.getCurrencyMaster().getCurrencyCode());

        serviceExport.setTosMaster(serviceImport.getTosMaster());
        serviceExport.setTosCode(serviceImport.getTosMaster().getTosCode());
        if (serviceImport.getCommodity() != null) {
            serviceExport.setCommodity(serviceImport.getCommodity());
            serviceExport.setCommodityCode(serviceImport.getCommodity().getHsCode());
        }

        if (dynamicField("Shipment > Service > LBS", location.getCountryMaster().getId())) {
            serviceExport.setDimensionUnit(DimensionUnit.INCHORPOUNDS);
            String data = appUtil.getLocationConfig("inch.to.pounds", serviceExport.getLocation(), false);
            serviceExport.setDimensionUnitValue(Double.parseDouble(data));
        } else {
            serviceExport.setDimensionUnit(DimensionUnit.CENTIMETERORKILOS);
            String data = appUtil.getLocationConfig("centimeter.to.kilos", serviceExport.getLocation(), false);
            serviceExport.setDimensionUnitValue(Double.parseDouble(data));
        }

        String serviceUid = sequenceGeneratorService.getSequence(SequenceType.SERVICE);
        serviceExport.setServiceUid(serviceUid);
        serviceExport.setServiceMaster(sm.getExportService());
        serviceExport.setServiceCode(sm.getExportService().getServiceCode());
        // Getting and Setting the values for
        serviceExport.setShipmentUid(serviceImport.getShipment().getShipmentUid());
        serviceExport.setConsolUid(serviceImport.getConsolUid());
        serviceExport.setCustomerService(serviceImport.getCustomerService());
        serviceExport.setCustomerServiceCode(serviceImport.getCustomerServiceCode());
        serviceExport.setCustomerServicePersonEmail(serviceImport.getCustomerServicePersonEmail());
        serviceExport.setPpcc(serviceImport.getPpcc());
        serviceExport.setOrigin(serviceImport.getOrigin());
        serviceExport.setOriginCode(serviceImport.getOriginCode());
        serviceExport.setPol(serviceImport.getPol());
        serviceExport.setPolCode(serviceImport.getPolCode());


        serviceExport.setPod(serviceImport.getPod());
        serviceExport.setPodCode(serviceImport.getPodCode());

        serviceExport.setDestination(serviceImport.getDestination());
        serviceExport.setDestinationCode(serviceImport.getDestinationCode());

        serviceExport.setWhoRouted(serviceImport.getWhoRouted());
        serviceExport.setSalesman(serviceImport.getSalesman());
        serviceExport.setSalesmanCode(serviceImport.getSalesmanCode());


        serviceExport.setParty(serviceImport.getParty());
        serviceExport.setPartyCode(serviceImport.getPartyCode());
        serviceExport.setCoLoader(serviceImport.getCoLoader());
        serviceExport.setCoLoaderCode(serviceImport.getCoLoaderCode());
        serviceExport.setPackMaster(serviceImport.getPackMaster());
        serviceExport.setPackCode(serviceImport.getPackCode());
        serviceExport.setBookedPieces(serviceImport.getBookedPieces());
        serviceExport.setBookedGrossWeightUnitKg(serviceImport.getBookedGrossWeightUnitKg());
        serviceExport.setBookedVolumeWeightUnitKg(serviceImport.getBookedVolumeWeightUnitKg());
        serviceExport.setBookedChargeableUnit(serviceImport.getBookedChargeableUnit());
        serviceExport.setBookedGrossWeightUnitPound(serviceImport.getBookedGrossWeightUnitPound());
        serviceExport.setBookedVolumeWeightUnitPound(serviceImport.getBookedVolumeWeightUnitPound());
        serviceExport.setCarrier(serviceImport.getCarrier());
        serviceExport.setCarrierCode(serviceImport.getCarrierCode());
        serviceExport.setEtd(serviceImport.getEtd());
        serviceExport.setEta(serviceImport.getEta());
        serviceExport.setRouteNo(serviceImport.getRouteNo());
        serviceExport.setMawbNo(serviceImport.getMawbNo());

        serviceExport.setClientGrossRate(serviceImport.getClientGrossRate());
        serviceExport.setClientNetRate(serviceImport.getClientNetRate());
        serviceExport.setDeclaredCost(serviceImport.getDeclaredCost());
        serviceExport.setCost(serviceImport.getCost());
        serviceExport.setRate(serviceImport.getRate());
        serviceExport.setCarrierBookingNumber(serviceImport.getCarrierBookingNumber());
        serviceExport.setYard(serviceImport.getYard());
        serviceExport.setLoading(serviceImport.getLoading());
        serviceExport.setCfsRemark(serviceImport.getCfsRemark());
        serviceExport.setCfsReceiptNo(serviceImport.getCfsReceiptNo());
        serviceExport.setDnNo(serviceImport.getDnNo());
        serviceExport.setIsAgreed(serviceImport.getIsAgreed());
        serviceExport.setProject(serviceImport.getProject());

        serviceExport.setCustomerManifestName(serviceImport.getCustomerManifestName());
        serviceExport.setQuotationUid(serviceImport.getQuotationUid());
        serviceExport.setBookedVolumeUnit(serviceImport.getBookedVolumeUnit());
        serviceExport.setBookedVolumeUnitCode(serviceImport.getBookedVolumeUnitCode());
        serviceExport.setBookedVolumeUnitValue(serviceImport.getBookedVolumeUnitValue());
        serviceExport.setBookedVolumeUnitCbm(serviceImport.getBookedVolumeUnitCbm());
        serviceExport.setBookedGrossWeightUnit(serviceImport.getBookedGrossWeightUnit());
        serviceExport.setBookedGrossWeightUnitCode(serviceImport.getBookedGrossWeightUnitCode());
        serviceExport.setBookedGrossWeightUnitValue(serviceImport.getBookedGrossWeightUnitValue());
        serviceExport.setVesselId(serviceImport.getVesselId());
        serviceExport.setServiceDetail(serviceImport.getServiceDetail());
        serviceExport.setTradeCode(serviceImport.getTradeCode());
        serviceExport.setIsServiceJob(serviceImport.getIsServiceJob());
        serviceExport.setTransitMode(serviceImport.getTransitMode());
        serviceExport.setTransitPort(serviceImport.getTransitPort());
        serviceExport.setTransitPortCode(serviceImport.getTransitPortCode());

        serviceExport.setShipmentServiceTriggerList(null);
        serviceExport.setEventList(null);
        serviceExport.setPickUpDeliveryPoint(null);
        shipmentServiceDetailValidator.updateServiceDate(serviceExport);

        Map<Long, FromToCurrency> map = (Map<Long, FromToCurrency>) currencyRateSearchImpl.search(location.getCountryMaster().getCurrencyMaster().getId()).getSearchResult();

        List<ShipmentCharge> chargeList = new ArrayList<>();
        if (CollectionUtils.isNotEmpty(serviceImport.getShipmentChargeList())) {
            Double totalGross = 0.0;
            Double totalNet = 0.0;
            Double totalDeclareCost = 0.0;
            Double totalRate = 0.0;
            Double totalCost = 0.0;


            for (ShipmentCharge impSc : serviceImport.getShipmentChargeList()) {
                boolean costEntered = false;

                ShipmentCharge expSc = new ShipmentCharge();
                expSc.setShipmentUid(shipment.getShipmentUid());
                expSc.setShipmentServiceDetail(serviceExport);
                expSc.setServiceUid(serviceExport.getServiceUid());
                expSc.setCompany(serviceExport.getCompany());
                expSc.setCountry(serviceExport.getCountry());
                expSc.setCountryCode(serviceExport.getCountryCode());
                expSc.setLocation(serviceExport.getLocation());

                if (impSc.getChargeMaster() != null) {
                    expSc.setChargeMaster(impSc.getChargeMaster());
                    expSc.setChargeCode(impSc.getChargeMaster().getChargeCode());
                    expSc.setChargeName(impSc.getChargeName());
                }
                if (impSc.getPpcc() != null) {
                    expSc.setPpcc(impSc.getPpcc());
                }
                if (impSc.getUnitMaster() != null) {
                    expSc.setUnitMaster(impSc.getUnitMaster());
                    expSc.setUnitCode(impSc.getUnitMaster().getUnitCode());
                }

                if (impSc.getNumberOfUnit() != null) {
                    expSc.setNumberOfUnit(impSc.getNumberOfUnit());
                }

                if (impSc.getNumberOfUnit() != null) {
                    expSc.setNumberOfUnit(impSc.getNumberOfUnit());
                }

                if (impSc.getEstimatedUnit() != null) {
                    expSc.setEstimatedUnit(impSc.getEstimatedUnit());
                }

                //gross sale

                if (impSc.getGrossSaleCurrency() != null) {
                    expSc.setGrossSaleCurrency(impSc.getGrossSaleCurrency());
                    expSc.setGrossSaleCurrencyCode(impSc.getGrossSaleCurrency().getCurrencyCode());
                }

                if (impSc.getGrossSaleAmount() != null) {
                    expSc.setGrossSaleAmount(impSc.getGrossSaleAmount());
                }
                if (impSc.getGrossSaleMinimum() != null) {
                    expSc.setGrossSaleMinimum(impSc.getGrossSaleMinimum());
                }


                if (impSc.getLocalGrossSaleRoe() != null) {
                    if (map.get(impSc.getGrossSaleCurrency().getId()) != null) {
                        expSc.setLocalGrossSaleRoe(map.get(impSc.getGrossSaleCurrency().getId()).getCSell());
                    } else {
                        expSc.setLocalGrossSaleRoe(1.0);
                    }
                }

                if (impSc.getTotalGrossSale() != null) {
                    expSc.setTotalGrossSale(impSc.getTotalGrossSale());
                }

                if (impSc.getLocalTotalGrossSale() != null) {
                    expSc.setLocalTotalGrossSale(expSc.getTotalGrossSale() * (1 / expSc.getLocalGrossSaleRoe()));
                    totalGross = totalGross + expSc.getLocalTotalGrossSale();
                }


                //net sale

                if (impSc.getNetSaleCurrency() != null) {
                    expSc.setNetSaleCurrency(impSc.getNetSaleCurrency());
                    expSc.setNetSaleCurrencyCode(impSc.getNetSaleCurrency().getCurrencyCode());
                }

                if (impSc.getNetSaleAmount() != null) {
                    expSc.setNetSaleAmount(impSc.getNetSaleAmount());
                }

                if (impSc.getNetSaleMinimum() != null) {
                    expSc.setNetSaleMinimum(impSc.getNetSaleMinimum());
                }

                if (impSc.getLocalNetSaleRoe() != null) {
                    if (map.get(impSc.getNetSaleCurrency().getId()) != null) {
                        expSc.setLocalNetSaleRoe(map.get(impSc.getNetSaleCurrency().getId()).getCSell());
                    } else {
                        expSc.setLocalNetSaleRoe(1.0);
                    }
                }

                if (impSc.getTotalNetSale() != null) {
                    expSc.setTotalNetSale(impSc.getTotalNetSale());
                }

                if (impSc.getLocalTotalNetSale() != null) {
                    expSc.setLocalTotalNetSale(expSc.getTotalNetSale() * (1 / expSc.getLocalNetSaleRoe()));
                    totalNet = totalNet + expSc.getLocalTotalNetSale();
                }


                if (impSc.getBillToType() != null) {
                    expSc.setBillToType(impSc.getBillToType());
                }


                //Declared sale currency

                if (impSc.getDeclaredSaleCurrency() != null) {
                    expSc.setDeclaredSaleCurrency(impSc.getDeclaredSaleCurrency());
                    expSc.setDeclaredSaleCurrencyCode(impSc.getDeclaredSaleCurrency().getCurrencyCode());
                }

                if (impSc.getDeclaredSaleAmount() != null) {
                    expSc.setDeclaredSaleAmount(impSc.getDeclaredSaleAmount());
                }

                if (impSc.getDeclaredSaleMinimum() != null) {
                    expSc.setDeclaredSaleMinimum(impSc.getDeclaredSaleMinimum());
                }

                if (impSc.getLocalDeclaredSaleRoe() != null) {
                    if (map.get(impSc.getDeclaredSaleCurrency().getId()) != null) {
                        expSc.setLocalDeclaredSaleRoe(map.get(impSc.getDeclaredSaleCurrency().getId()).getCSell());
                    } else {
                        expSc.setLocalDeclaredSaleRoe(1.0);
                    }
                }

                if (impSc.getTotalDeclaredSale() != null) {
                    expSc.setTotalDeclaredSale(impSc.getTotalDeclaredSale());
                }

                if (impSc.getLocalTotalDeclaredSale() != null) {
                    expSc.setLocalTotalDeclaredSale(expSc.getTotalDeclaredSale() * (1 / expSc.getLocalDeclaredSaleRoe()));
                    totalDeclareCost = totalDeclareCost + expSc.getLocalTotalDeclaredSale();
                }


                //Rate Currency

                if (impSc.getRateCurrency() != null) {
                    expSc.setRateCurrency(impSc.getRateCurrency());
                    expSc.setRateCurrencyCode(impSc.getRateCurrency().getCurrencyCode());
                }

                if (impSc.getRateAmount() != null) {
                    expSc.setRateAmount(impSc.getRateAmount());
                }

                if (impSc.getRateMinimum() != null) {
                    expSc.setRateMinimum(impSc.getRateMinimum());
                }

                if (impSc.getLocalRateAmountRoe() != null) {
                    if (map.get(impSc.getRateCurrency().getId()) != null) {
                        expSc.setLocalRateAmountRoe(map.get(impSc.getRateCurrency().getId()).getCSell());
                    } else {
                        expSc.setLocalRateAmountRoe(1.0);
                    }
                }

                if (impSc.getTotalRateAmount() != null) {
                    expSc.setTotalRateAmount(impSc.getTotalRateAmount());
                }

                if (impSc.getLocalTotalRateAmount() != null) {
                    expSc.setLocalTotalRateAmount(expSc.getTotalRateAmount() * (1 / expSc.getLocalRateAmountRoe()));
                    totalRate = totalRate + expSc.getLocalTotalRateAmount();
                }


                //Cost

                if (impSc.getCostCurrency() != null) {
                    expSc.setCostCurrency(impSc.getCostCurrency());
                    expSc.setCostCurrencyCode(impSc.getCostCurrency().getCurrencyCode());
                }


                if (impSc.getCostAmount() != null) {
                    expSc.setCostAmount(impSc.getCostAmount());

                    if (impSc.getCostAmount() > 0) {
                        costEntered = true;
                    }
                }

                if (impSc.getCostMinimum() != null) {
                    expSc.setCostMinimum(impSc.getCostMinimum());

                    if (impSc.getCostMinimum() > 0) {
                        costEntered = true;
                    }

                }
                //As discussd with suresh and QC, If cost is not entered we not capture this charge
                if (!costEntered)
                    continue;

                if (impSc.getLocalCostAmountRoe() != null) {
                    if (map.get(impSc.getCostCurrency().getId()) != null) {
                        expSc.setLocalCostAmountRoe(map.get(impSc.getCostCurrency().getId()).getCSell());
                    } else {
                        expSc.setLocalCostAmountRoe(1.0);
                    }
                }

                if (impSc.getTotalCostAmount() != null) {
                    expSc.setTotalCostAmount(impSc.getTotalCostAmount());
                }

                if (impSc.getLocalTotalCostAmount() != null) {
                    expSc.setLocalTotalCostAmount(expSc.getTotalCostAmount() * (1 / expSc.getLocalCostAmountRoe()));
                    totalCost = totalCost + expSc.getLocalTotalCostAmount();
                }


                if (impSc.getVendor() != null) {
                    expSc.setVendor(impSc.getVendor());
                    expSc.setVendorCode(impSc.getVendor().getPartyCode());
                } else {
                    impSc.setVendor(null);
                }

                if (impSc.getActualChargeable() != null) {
                    expSc.setActualChargeable(impSc.getActualChargeable());
                }

                if (impSc.getDue() != null) {
                    expSc.setDue(impSc.getDue());
                }
                if (impSc.getNote() != null) {
                    expSc.setNote(impSc.getNote());
                }
                chargeList.add(expSc);
            }
            serviceExport.setClientGrossRate(totalGross);
            serviceExport.setClientNetRate(totalNet);
            serviceExport.setRate(totalRate);
            serviceExport.setShipmentChargeList(chargeList);
        } else {
            serviceExport.setShipmentChargeList(null);
        }

        List<DocumentDetail> documentList = new ArrayList<>();

        if (CollectionUtils.isNotEmpty(serviceImport.getDocumentList())) {

            for (DocumentDetail dd : serviceImport.getDocumentList()) {

                DocumentDetail ddNew = new DocumentDetail();

                ddNew.setShipmentServiceDetail(serviceExport);
                ddNew.setShipmentUid(shipment.getShipmentUid());
                ddNew.setServiceUid(serviceExport.getServiceUid());
                ddNew.setDocumentNo(sequenceGeneratorService.getSequence(SequenceType.DOCUMENT));
                ddNew.setDocumentReqDate(dd.getDocumentReqDate());

                if (dd.getShipper() != null) {
                    ddNew.setShipper(dd.getShipper());
                    ddNew.setShipperCode(dd.getShipperCode());
                    ddNew.setShipperAddress(new AddressMaster());
                    ddNew.getShipperAddress().setAddressLine1(dd.getShipperAddress().getAddressLine1());
                    ddNew.getShipperAddress().setAddressLine2(dd.getShipperAddress().getAddressLine2());
                    ddNew.getShipperAddress().setAddressLine3(dd.getShipperAddress().getAddressLine3());
                    ddNew.getShipperAddress().setAddressLine4(dd.getShipperAddress().getAddressLine4());
                } else {
                    ddNew.setShipper(null);
                    ddNew.setShipperCode(null);
                    ddNew.setShipperAddress(null);
                }

                if (dd.getForwarder() != null) {
                    ddNew.setForwarder(dd.getForwarder());
                    ddNew.setForwarderCode(dd.getForwarderCode());

                    ddNew.setForwarderAddress(new AddressMaster());
                    ddNew.getForwarderAddress().setAddressLine1(dd.getForwarderAddress().getAddressLine1());
                    ddNew.getForwarderAddress().setAddressLine2(dd.getForwarderAddress().getAddressLine2());
                    ddNew.getForwarderAddress().setAddressLine3(dd.getForwarderAddress().getAddressLine3());
                    ddNew.getForwarderAddress().setAddressLine4(dd.getForwarderAddress().getAddressLine4());

                } else {
                    ddNew.setForwarder(null);
                    ddNew.setForwarderCode(null);
                    ddNew.setForwarderAddress(null);
                }


                if (dd.getConsignee() != null) {
                    ddNew.setConsignee(dd.getConsignee());
                    ddNew.setConsigneeCode(dd.getConsigneeCode());

                    ddNew.setConsigneeAddress(new AddressMaster());
                    ddNew.getConsigneeAddress().setAddressLine1(dd.getConsigneeAddress().getAddressLine1());
                    ddNew.getConsigneeAddress().setAddressLine2(dd.getConsigneeAddress().getAddressLine2());
                    ddNew.getConsigneeAddress().setAddressLine3(dd.getConsigneeAddress().getAddressLine3());
                    ddNew.getConsigneeAddress().setAddressLine4(dd.getConsigneeAddress().getAddressLine4());

                } else {
                    ddNew.setConsignee(null);
                    ddNew.setConsigneeAddress(null);
                    ddNew.setConsigneeCode(null);
                }

                if (dd.getFirstNotify() != null) {
                    ddNew.setFirstNotify(dd.getFirstNotify());
                    ddNew.setFirstNotifyCode(dd.getFirstNotifyCode());

                    ddNew.setFirstNotifyAddress(new AddressMaster());
                    ddNew.getFirstNotifyAddress().setAddressLine1(dd.getFirstNotifyAddress().getAddressLine1());
                    ddNew.getFirstNotifyAddress().setAddressLine2(dd.getFirstNotifyAddress().getAddressLine2());
                    ddNew.getFirstNotifyAddress().setAddressLine3(dd.getFirstNotifyAddress().getAddressLine3());
                    ddNew.getFirstNotifyAddress().setAddressLine4(dd.getFirstNotifyAddress().getAddressLine4());

                } else {
                    ddNew.setFirstNotify(null);
                    ddNew.setFirstNotifyCode(null);
                    ddNew.setFirstNotifyAddress(null);
                }

                if (dd.getChaAgent() != null) {
                    ddNew.setChaAgent(dd.getChaAgent());
                    ddNew.setChaAgentCode(dd.getChaAgentCode());

                    ddNew.setChaAgentAddress(new AddressMaster());
                    ddNew.getChaAgentAddress().setAddressLine1(dd.getChaAgentAddress().getAddressLine1());
                    ddNew.getChaAgentAddress().setAddressLine2(dd.getChaAgentAddress().getAddressLine2());
                    ddNew.getChaAgentAddress().setAddressLine3(dd.getChaAgentAddress().getAddressLine3());
                    ddNew.getChaAgentAddress().setAddressLine4(dd.getChaAgentAddress().getAddressLine4());

                } else {
                    ddNew.setChaAgent(null);
                    ddNew.setChaAgentCode(null);
                    ddNew.setChaAgentAddress(null);
                }

                if (dd.getSecondNotify() != null) {
                    ddNew.setSecondNotify(dd.getSecondNotify());
                    ddNew.setSecondNotifyCode(dd.getSecondNotifyCode());

                    ddNew.setSecondNotifyAddress(new AddressMaster());
                    ddNew.getSecondNotifyAddress().setAddressLine1(dd.getSecondNotifyAddress().getAddressLine1());
                    ddNew.getSecondNotifyAddress().setAddressLine2(dd.getSecondNotifyAddress().getAddressLine2());
                    ddNew.getSecondNotifyAddress().setAddressLine3(dd.getSecondNotifyAddress().getAddressLine3());
                    ddNew.getSecondNotifyAddress().setAddressLine4(dd.getSecondNotifyAddress().getAddressLine4());
                } else {
                    ddNew.setSecondNotify(null);
                    ddNew.setSecondNotifyCode(null);
                    ddNew.setSecondNotifyAddress(null);
                }


                ddNew.setAgent(apm.getAgent());
                ddNew.setAgentCode(apm.getAgent().getPartyCode());


                for (PartyAddressMaster adress : apm.getAgent().getPartyAddressList()) {
                    if (adress.getAddressType().equals(AddressType.Primary)) {
                        AddressMaster am = new AddressMaster();
                        am.setAddressLine1(adress.getAddressLine1());
                        am.setAddressLine2(adress.getAddressLine2());
                        am.setAddressLine3(adress.getAddressLine3());

                        String address = getAddress(adress, apm.getAgent());


                        if (adress.getPoBox() != null) {
                            if (address.trim().length() == 0) {
                                address = adress.getPoBox();
                            } else {
                                address = address + ", " + adress.getPoBox();
                            }
                        }

                        am.setAddressLine4(address);

                        ddNew.setAgentAddress(am);
                    }
                }


                if (location != null && location.getPartyMaster() != null) {
                    ddNew.setIssuingAgent(location.getPartyMaster());
                    ddNew.setIssuingAgentCode(location.getPartyMaster().getPartyCode());
                    ddNew.setIssuingAgentAddress(new AddressMaster());

                    for (PartyAddressMaster pd : location.getPartyMaster().getPartyAddressList()) {
                        if (pd.getAddressType() == AddressType.Primary || pd.getAddressType().equals(AddressType.Primary)) {

                            ddNew.getIssuingAgentAddress().setAddressLine1(pd.getAddressLine1());
                            ddNew.getIssuingAgentAddress().setAddressLine2(pd.getAddressLine2());
                            if (location.getPartyMaster() != null && location.getPartyMaster().getCountryMaster() != null) {
                                ddNew.getIssuingAgentAddress().setAddressLine4(location.getPartyMaster().getCountryMaster().getCountryName());
                            } else {
                                ddNew.getIssuingAgentAddress().setAddressLine4(null);
                            }
                            ddNew.getIssuingAgentAddress().setAddressLine3(getAddress(pd, location.getPartyMaster()));
                            if (pd.getEmail() != null) {
                                ddNew.getIssuingAgentAddress().setEmail(pd.getEmail());
                            }

                            break;
                        }
                    }

                } else {
                    ddNew.setIssuingAgent(null);
                    ddNew.setIssuingAgentCode(null);
                    ddNew.setIssuingAgentAddress(null);
                }

                ddNew.setOrigin(dd.getOrigin());
                ddNew.setOriginCode(dd.getOriginCode());

                ddNew.setPol(dd.getPol());
                ddNew.setPolCode(dd.getPolCode());

                ddNew.setPod(dd.getPod());
                ddNew.setPodCode(dd.getPodCode());

                ddNew.setDestination(dd.getDestination());
                ddNew.setDestinationCode(dd.getDestinationCode());

                ddNew.setPackMaster(dd.getPackMaster());
                ddNew.setPackCode(dd.getPackCode());
                ddNew.setPackDescription(dd.getPackDescription());

                ddNew.setNoOfPieces(dd.getNoOfPieces());

                if (dynamicField("Shipment > Service > LBS", location.getCountryMaster().getId())) {
                    ddNew.setDimensionUnit(DimensionUnit.INCHORPOUNDS);
                    String data = appUtil.getLocationConfig("inch.to.pounds", serviceExport.getLocation(), false);
                    ddNew.setDimensionUnitValue(Double.parseDouble(data));
                } else {
                    ddNew.setDimensionUnit(DimensionUnit.CENTIMETERORKILOS);
                    String data = appUtil.getLocationConfig("centimeter.to.kilos", serviceExport.getLocation(), false);
                    ddNew.setDimensionUnitValue(Double.parseDouble(data));
                }

                ddNew.setChargebleWeight(dd.getChargebleWeight());
                ddNew.setGrossWeight(dd.getGrossWeight());
                ddNew.setVolumeWeight(dd.getVolumeWeight());

                ddNew.setVolumeWeightInPound(dd.getVolumeWeightInPound());
                ddNew.setGrossWeightInPound(dd.getGrossWeightInPound());

                ddNew.setBookedVolumeUnitCbm(dd.getBookedVolumeUnitCbm());

                ddNew.setDimensionGrossWeight(dd.getDimensionGrossWeight());
                ddNew.setDimensionGrossWeightInKg(dd.getDimensionGrossWeightInKg());
                ddNew.setDimensionVolumeWeight(dd.getDimensionVolumeWeight());

                ddNew.setRatePerCharge(dd.getRatePerCharge());

                ddNew.setHandlingInformation(dd.getHandlingInformation());
                ddNew.setAccoutingInformation(dd.getAccoutingInformation());

                ddNew.setCommodityDescription(dd.getCommodityDescription());

                ddNew.setMarksAndNo(dd.getMarksAndNo());

                ddNew.setRateClass(dd.getRateClass());

                ddNew.setBlReceivedPerson(dd.getBlReceivedPerson());
                ddNew.setBlReceivedPersonCode(dd.getBlReceivedPersonCode());
                ddNew.setBlReceivedDate(dd.getBlReceivedDate());

                ddNew.setCompany(serviceExport.getCompany());
                ddNew.setCountry(serviceExport.getCountry());
                ddNew.setCountryCode(serviceExport.getCountryCode());

                ddNew.setLocation(serviceExport.getLocation());

                ddNew.setDocPrefixMaster(dd.getDocPrefixMaster());
                ddNew.setDocPrefixCode(dd.getDocPrefixCode());

                ddNew.setPickUpDeliveryPoint(dd.getPickUpDeliveryPoint());

                ddNew.setServiceCode(dd.getServiceCode());


                ddNew.setVesselId(dd.getVesselId());
                ddNew.setVesselCode(dd.getVesselCode());
                ddNew.setRouteNo(dd.getRouteNo());
                ddNew.setTransitPort(dd.getTransitPort());
                ddNew.setTransitPortCode(dd.getTransitPortCode());
                ddNew.setTransitEta(dd.getTransitEta());
                ddNew.setTransitScheduleUid(dd.getTransitScheduleUid());
                ddNew.setMawbNo(dd.getMawbNo());
                ddNew.setCarrier(dd.getCarrier());
                ddNew.setEtd(dd.getEtd());
                ddNew.setEta(dd.getEta());
                ddNew.setCommodity(dd.getCommodity());
                ddNew.setCommodityCode(dd.getCommodityCode());
                ddNew.setDocumentSubDetail(dd.getDocumentSubDetail());

                List<ServiceDocumentDimension> documentDimensionList = new ArrayList<>();
                if (CollectionUtils.isNotEmpty(dd.getDimensionList())) {
                    for (ServiceDocumentDimension sdd : dd.getDimensionList()) {
                        // get and set the values
                        ServiceDocumentDimension sddNew = new ServiceDocumentDimension();

                        sddNew.setDocumentDetail(ddNew);
                        sddNew.setNoOfPiece(sdd.getNoOfPiece());
                        sddNew.setLength(sdd.getLength());
                        sddNew.setWidth(sdd.getWidth());
                        sddNew.setHeight(sdd.getHeight());
                        sddNew.setVolWeight(sdd.getVolWeight());
                        sddNew.setGrossWeight(sdd.getGrossWeight());

                        documentDimensionList.add(sddNew);
                    }
                    ddNew.setDimensionList(documentDimensionList);
                } else {
                    ddNew.setDimensionList(null);
                }
                documentList.add(ddNew);
            }
            serviceExport.setDocumentList(documentList);
        } else {
            serviceExport.setDocumentList(null);
        }

        try {
            Double minimum = Double.parseDouble(
                    appUtil.getLocationConfig(SHIPMENT_MIN_CHRG_WEIGHT, serviceExport.getLocation(), false));
            if (serviceExport.getBookedChargeableUnit() < minimum) {
                serviceExport.setIsMinimumShipment(YesNo.Yes);
            } else {
                serviceExport.setIsMinimumShipment(YesNo.No);
            }
        } catch (Exception e) {
            throw new RestException(ErrorCode.SHIPMENT_MINIMUM_WEIGHT);
        }

        log.info("ShipmentService.getNewExportShipmentServiceDetail end");
        return serviceExport;
    }

    private String getAddress(PartyAddressMaster pd, PartyMaster pm) {

        String partyAddress = StringUtils.EMPTY;
        if (pd != null) {
            partyAddress = (pd.getCityMaster() != null) ? partyAddress + pd.getCityMaster().getCityName() : partyAddress;
            partyAddress = (pd.getZipCode() != null) ? partyAddress + Constants.COMMA + pd.getZipCode() : partyAddress;
            partyAddress = (pd.getStateMaster() != null) ? partyAddress + Constants.COMMA + pd.getStateMaster().getStateName() : partyAddress;
            partyAddress = (pm != null && pm.getCountryMaster() != null) ? partyAddress + Constants.COMMA + pm.getCountryMaster().getCountryCode() : partyAddress;
        }
        return partyAddress;
    }


    public void afterGet(Shipment shipment) {
        if (shipment != null && shipment.getLocation() != null) {
            AppUtil.setPartyToNull(shipment.getLocation().getPartyMaster());

            if (CollectionUtils.isNotEmpty(shipment.getShipmentServiceList())) {
                for (ShipmentServiceDetail shipmentServiceDetail : shipment.getShipmentServiceList()) {

                    AppUtil.setService(shipmentServiceDetail);


                    if (shipmentServiceDetail.getConnectionList() != null) {
                        shipmentServiceDetail.getConnectionList().sort(ServiceConnection.IdComparator);
                    }


					/* Trigger is shown from other services */
                    List<ShipmentServiceTrigger> otherServiceTriggerList = shipmentServiceTriggerRepository.findByShipmentUidAndServiceUid(shipment.getShipmentUid(), shipmentServiceDetail.getServiceUid());

                    if (shipmentServiceDetail.getShipmentServiceTriggerList() != null) {
                        shipmentServiceDetail.getShipmentServiceTriggerList().addAll(otherServiceTriggerList);
                    } else {
                        shipmentServiceDetail.setShipmentServiceTriggerList(new ArrayList<>());
                        shipmentServiceDetail.setShipmentServiceTriggerList(otherServiceTriggerList);
                    }

                    if (shipmentServiceDetail.getShipmentServiceTriggerList() != null) {
                        shipmentServiceDetail.getShipmentServiceTriggerList().sort(ShipmentServiceTrigger.DateComparator);

                        for (ShipmentServiceTrigger st : shipmentServiceDetail.getShipmentServiceTriggerList()) {
                            ShipmentServiceDetail tmpService = shipmentServiceTriggerRepository.findOne(st.getId()).getShipmentServiceDetail();
                            if (st.getDate() != null && tmpService.getLocation() != null) {
                                st.setDateTimeZone(AppUtil.getDateTimeZone(st.getDate(), tmpService.getLocation().getTimeZone()));
                                st.setLocationName(tmpService.getLocation().getLocationName());
                            }
                        }
                    }


                    if (CollectionUtils.isNotEmpty(shipmentServiceDetail.getShipmentAttachmentList())) {
                        for (ShipmentAttachment attachment : shipmentServiceDetail.getShipmentAttachmentList()) {
                            attachment.setFile(null);
                        }
                    }
                }
            }
        }
    }

}
