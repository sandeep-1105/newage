package com.efreightsuite.search;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.transaction.Transactional;

import com.efreightsuite.activiti.ActivitiTransactionService;
import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.DateRange;
import com.efreightsuite.dto.EnquiryDto;
import com.efreightsuite.dto.EnquiryLogSearchDto;
import com.efreightsuite.dto.RecentRecordsRequest;
import com.efreightsuite.dto.RecentRecordsResponse;
import com.efreightsuite.dto.SearchRespDto;
import com.efreightsuite.enumeration.EnquiryCustomerStatus;
import com.efreightsuite.enumeration.EnquiryStatus;
import com.efreightsuite.enumeration.SequenceType;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.model.EnquiryCustomer;
import com.efreightsuite.model.EnquiryDetail;
import com.efreightsuite.model.EnquiryLog;
import com.efreightsuite.model.PartyMaster;
import com.efreightsuite.model.UserProfile;
import com.efreightsuite.repository.EnquiryCustomerRepository;
import com.efreightsuite.repository.EnquiryLogRepository;
import com.efreightsuite.repository.PartyMasterRepository;
import com.efreightsuite.repository.QuotationRepository;
import com.efreightsuite.service.AuthService;
import com.efreightsuite.service.SequenceGeneratorService;
import com.efreightsuite.util.CommonUtils;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import static com.efreightsuite.constants.EnquiryLogConstants.*;

@Service
@Log4j2
public class EnquiryLogSearchService {

    @Autowired
    private
    EntityManager em;

    @Autowired
    private
    QuotationRepository quotationRepository;

    @Autowired
    private
    SequenceGeneratorService sequenceGeneratorService;

    @Autowired
    private
    EnquiryLogRepository enquiryLogRepository;

    @Autowired
    private
    PartyMasterRepository partyMasterRepository;

    @Autowired
    private
    EnquiryCustomerRepository enquiryCustomerRepository;

    @Autowired
    private
    ActivitiTransactionService activitiTransactionService;

    @Transactional
    public void cancelEnquiry(BaseDto baseDto, EnquiryDto dto) {

        EnquiryLog cancelledEnquiry = enquiryLogRepository.findById(dto.getEnqId());
        cancelledEnquiry.setStatus(EnquiryStatus.Cancel);
        cancelledEnquiry = enquiryLogRepository.save(cancelledEnquiry);

        activitiTransactionService.cancelEnquiry(cancelledEnquiry);

        baseDto.setResponseCode(ErrorCode.SUCCESS);
        baseDto.setResponseObject(cancelledEnquiry);

    }

    @Transactional
    public EnquiryLog savePartyAndEnquiry(PartyMaster partyMaster, EnquiryLog enquiryLog, EnquiryCustomer enquiryCustomer) {
        partyMaster.setPartyCode(sequenceGeneratorService.getSequence(SequenceType.PARTY));
        partyMaster = partyMasterRepository.save(partyMaster);
        log.info("Party Master created with the id " + partyMaster.getId());
        enquiryLog.setPartyMaster(partyMaster);
        enquiryLog = enquiryLogRepository.save(enquiryLog);
        enquiryCustomer.setEnquiryCustomerStatus(EnquiryCustomerStatus.Approved);
        enquiryCustomerRepository.save(enquiryCustomer);
        return enquiryLog;
    }

    @Transactional
    public EnquiryLog saveEnquiryTransactional(EnquiryLog enquiryLog) {

        enquiryLog.setEnquiryNo(sequenceGeneratorService.getSequence(SequenceType.ENQUERY));
        if (enquiryLog.getEnquiryDetailList() != null && enquiryLog.getEnquiryDetailList().size() > 0) {
            for (EnquiryDetail ed : enquiryLog.getEnquiryDetailList()) {
                if (ed.getEnquiryNo() == null) {
                    ed.setEnquiryNo(enquiryLog.getEnquiryNo());
                }
            }
        }
        enquiryLog = enquiryLogRepository.save(enquiryLog);

        enquiryLog = activitiTransactionService.createEnquiryTask(enquiryLog);

        return enquiryLog;

    }

    public SearchRespDto search(EnquiryLogSearchDto dto, String searchFlag) {
        Map<String, Object> params = new HashMap<>();

        String hql = "";
        log.info("EnquiryLogSearchService.search() method is called...");

        String searchQuery = "select new com.efreightsuite.dto.EnquiryLogResponseDto ( "
                + " el.id ,"
                + " pm.partyName, "
                + " pg.colorCode,"
                + " ec.name, "
                + " el.enquiryNo,"
                + " el.quoteBy, "
                + " el.receivedOn, "
                + " el.quotationNo, "
                + " sc.employeeName, "
                + " sm.employeeName, "
                + " el.loggedOn, "
                + " lb.employeeName, "
                + " el.status )"
                + " from EnquiryDetail ed "
                + " left join ed.enquiryLog el "
                + " left join el.partyMaster pm"
                + " left join el.salesCoOrdinator sc"
                + " left join el.salesman sm"
                + " left join el.loggedBy lb"
                + " left join el.locationMaster loc"
                + " left join loc.divisionMaster division"
                + " left join el.countryMaster country "
                + " left join el.partyMaster.gradeMaster pg "
                + " left join ed.serviceMaster service"
                + " left join el.enquiryCustomer ec";

        String countQuery = "select count(el.id)"
                + " from EnquiryDetail ed "
                + " left join ed.enquiryLog el "
                + " left join el.partyMaster pm"
                + " left join el.salesCoOrdinator sc"
                + " left join el.salesman sm"
                + " left join el.loggedBy lb"
                + " left join el.locationMaster loc"
                + " left join loc.divisionMaster division"
                + " left join el.countryMaster country "
                + " left join el.partyMaster.gradeMaster pg "
                + " left join ed.serviceMaster service"
                + " left join el.enquiryCustomer ec";

        String partialHql = addLocationCriterias();

        if (StringUtils.isNotBlank(partialHql)) {
            hql += partialHql;
        }

        if (searchFlag.equalsIgnoreCase("quote")) {
            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }
            hql = hql + " el.quotationNo is not null ";
        } else if (searchFlag.equalsIgnoreCase("active")) {
            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }
            hql = hql + "el.quotationNo is null ";
        }

        hql += addCriteria(dto.getSearchEnquiryNo(), params, " Upper(el.enquiryNo) like :enquiryNo ", "enquiryNo");


        hql += addCriteria(dto.getSearchQuotationNo(), params, " Upper(el.quotationNo) like :quotationNo", "quotationNo");


        hql += addCriteria(dto.getSearchCustomerName(), params, " ((upper(pm.partyName) like :customerName) OR"
                + "(upper(ec.name) like :customerName AND ec.enquiryCustomerStatus like 'Pending')) ", "customerName");

        hql += addCriteria(dto.getSearchSalesCoOrdinatorName(), params, " Upper(sc.employeeName) like :coordinatorName", "coordinatorName");

        hql += addCriteria(dto.getSearchSalesmanName(), params, " Upper(sm.employeeName) like :salesman", "salesman");

        hql += addDateCriteria(dto.getSearchQuoteBy(), params, " ( el.quoteBy >= :quoteStartDate AND el.quoteBy <= :quoteEndDate ) ", "quoteStartDate", "quoteEndDate");

        hql += addDateCriteria(dto.getSearchReceivedOn(), params, " ( el.receivedOn >= :recStartDate AND el.receivedOn <= :recEndDate ) ", "recStartDate", "recEndDate");

        hql += addDateCriteria(dto.getSearchLoggedOnDate(), params, " ( el.loggedOn >= :logStartDate AND el.loggedOn <= :logEndDate ) ", "logStartDate", "logEndDate");

        hql += addCriteria(dto.getSearchLoggedByName(), params, " Upper(lb.employeeName)  like :loggedBy", "loggedBy");

        if (hql.trim().length() != 0) {
            if (searchFlag.equalsIgnoreCase("active")) {
                hql = hql + " AND ";
                hql = hql + "el.status like 'Active'";
            }
            hql = " where " + hql;
        }

        countQuery = countQuery + hql;
        log.info("HQL Count Query For Search : " + countQuery);
        Long countTotal = getNumberOfSearchResults(params, countQuery);
        log.info("Total Number of Records Found : " + countTotal);

        String orderByQuery = createOrderByQueryFragment(dto);
        searchQuery = searchQuery + hql + orderByQuery;
        log.info("HQL Query For Search : " + searchQuery);

        if (dto.getSelectedPageNumber()<0 && dto.getRecordPerPage()<0) {
            return createAndExecuteQueryForNoneMaxResult(dto, params, searchQuery, countTotal);
        }
        return createAndExecuteQuery(dto, params, searchQuery, countTotal);
    }

    private Long getNumberOfSearchResults(Map<String, Object> params, String countQuery) {
        Query countQ = em.createQuery(countQuery);
        log.info("HQL params For Search : " + params);
        for (Entry<String, Object> e : params.entrySet()) {
            countQ.setParameter(e.getKey(), e.getValue());
        }
        return (Long) countQ.getSingleResult();
    }

    private SearchRespDto createAndExecuteQuery(EnquiryLogSearchDto dto, Map<String, Object> params, String searchQuery, Long countTotal) {
        Query query = em.createQuery(searchQuery);

        query.setFirstResult(dto.getSelectedPageNumber() * dto.getRecordPerPage());
        query.setMaxResults(dto.getRecordPerPage());

        for (Entry<String, Object> e : params.entrySet()) {
            query.setParameter(e.getKey(), e.getValue());
        }
        return new SearchRespDto(countTotal, query.getResultList());
    }

    private SearchRespDto createAndExecuteQueryForNoneMaxResult(EnquiryLogSearchDto dto, Map<String, Object> params, String searchQuery, Long countTotal) {
        Query query = em.createQuery(searchQuery);

        for (Entry<String, Object> e : params.entrySet()) {
            query.setParameter(e.getKey(), e.getValue());
        }
        return new SearchRespDto(countTotal, query.getResultList());
    }

    private String createOrderByQueryFragment(EnquiryLogSearchDto dto) {
        String orderByQuery = "";

        if (StringUtils.isNotBlank(dto.getSortByColumn())) {
            switch (dto.getSortByColumn()) {
                case CUSTOMER_NAME: {
                    orderByQuery = orderByQuery + "pm.partyName";
                    break;
                }
                case ENQUIRY_NO: {
                    orderByQuery = orderByQuery + "el.enquiryNo";
                    break;
                }
                case QUOTE_BY: {
                    orderByQuery = orderByQuery + "el.quoteBy";
                    break;
                }
                case RECEIVED_ON: {
                    orderByQuery = orderByQuery + "el.receivedOn";
                    break;
                }
                case QUOTATION_NO: {
                    orderByQuery = orderByQuery + "el.quotationNo";
                    break;
                }
                case SALES_COORDINATE_NAME: {
                    orderByQuery = orderByQuery + "sc.employeeName";
                    break;
                }
                case SALES_MAN_NAME: {
                    orderByQuery = orderByQuery + "sm.employeeName";
                    break;
                }
                case LOGGED_ON_DATE: {
                    orderByQuery = orderByQuery + "el.loggedOn";
                    break;
                }
                case LOGGED_BY_NAME: {
                    orderByQuery = orderByQuery + "lb.employeeName";
                    break;
                }
                case STATUS: {
                    orderByQuery = orderByQuery + "el.status";
                    break;
                }
                default: {
                    log.info("No condition matched:");
                }
            }
        }

        if (orderByQuery.length() != 0) {
            orderByQuery = " ORDER BY " + orderByQuery + " " + dto.getOrderByType();
        } else {
            orderByQuery = " ORDER BY el.id DESC ";
        }
        return orderByQuery;
    }

    private String addDateCriteria(DateRange dateRange, Map<String, Object> params, String fragmentQl, String startDateParam, String endDateParam) {
        StringBuilder sb = new StringBuilder();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:SS");
        if (dateRange != null && dateRange.getStartDate() != null && dateRange.getEndDate() != null) {
            try {
                Date dt1 = sdf.parse(dateRange.getStartDate());
                Date dt2 = sdf.parse(dateRange.getEndDate());

                sb.append(fragmentQl);
                params.put(startDateParam, dt1);
                params.put(endDateParam, dt2);

            } catch (ParseException e) {
                throw new IllegalArgumentException(e);
            }

        }
        return sb.length() == 0 ? "" : " AND " + sb.toString();
    }

    private String addCriteria(String searchEnquiryNo, Map<String, Object> params, String fragmentQl, String param) {
        StringBuilder partial = new StringBuilder();
        if (StringUtils.isNotBlank(searchEnquiryNo)) {
            partial.append(fragmentQl);
            params.put(param, likeParam(searchEnquiryNo));
        }
        return partial.length() == 0 ? "" : " AND " + partial.toString();
    }

    private String likeParam(String str) {
        return trimAndUppercase(str) + "%";
    }

    private String trimAndUppercase(String str) {
        return StringUtils.upperCase(StringUtils.trim(str));
    }

    private String addLocationCriterias() {
        UserProfile userProfile = AuthService.getCurrentUser();
        String hql = "";
        Set<Long> locationList = new HashSet<>();

        if (userProfile.getSelectedUserLocation() != null) {
            locationList.add(userProfile.getSelectedUserLocation().getId());
        }
        if (userProfile.getRecordAccessLevelProcessDTO() != null) {

            //Location List
            if (userProfile.getRecordAccessLevelProcessDTO().getLocationList() != null
                    && !userProfile.getRecordAccessLevelProcessDTO().getLocationList().isEmpty()) {
                locationList.addAll(userProfile.getRecordAccessLevelProcessDTO().getLocationList());
            }

            //Country List
            if (userProfile.getRecordAccessLevelProcessDTO().getCountryList() != null
                    && !userProfile.getRecordAccessLevelProcessDTO().getCountryList().isEmpty()) {


                String ids = CommonUtils.commaSeparatedIds(userProfile.getRecordAccessLevelProcessDTO().getCountryList());
                if (ids.trim().length() > 0) {
                    hql = hql + " country.id   IN (" + ids + ")";
                }
            }


            // Party List
            if (userProfile.getRecordAccessLevelProcessDTO().getCustomerList() != null
                    && !userProfile.getRecordAccessLevelProcessDTO().getCustomerList().isEmpty()) {
                String ids = CommonUtils.commaSeparatedIds(userProfile.getRecordAccessLevelProcessDTO().getCustomerList());
                if (ids.trim().length() > 0) {
                    if (hql.trim().length() != 0) {
                        hql = hql + " AND ";
                    }
                    hql = hql + " pm.id   IN (" + ids + ")";
                }
            }
            // Salesman List
            if (userProfile.getRecordAccessLevelProcessDTO().getSalesmanList() != null
                    && !userProfile.getRecordAccessLevelProcessDTO().getSalesmanList().isEmpty()) {
                String ids = CommonUtils.commaSeparatedIds(userProfile.getRecordAccessLevelProcessDTO().getSalesmanList());
                if (ids.trim().length() > 0) {
                    if (hql.trim().length() != 0) {
                        hql = hql + " AND ";
                    }
                    hql = hql + " sm.id   IN (" + ids + ")";
                }
            }

            // Service List
            if (userProfile.getRecordAccessLevelProcessDTO().getServiceList() != null
                    && !userProfile.getRecordAccessLevelProcessDTO().getServiceList().isEmpty()) {
                String ids = CommonUtils.commaSeparatedIds(userProfile.getRecordAccessLevelProcessDTO().getServiceList());
                if (ids.trim().length() > 0) {
                    if (hql.trim().length() != 0) {
                        hql = hql + " AND ";
                    }
                    hql = hql + " service.id   IN (" + ids + ")";
                }
            }
            // Division List
            if (userProfile.getRecordAccessLevelProcessDTO().getDivisionList() != null
                    && !userProfile.getRecordAccessLevelProcessDTO().getDivisionList().isEmpty()) {
                String ids = CommonUtils.commaSeparatedIds(userProfile.getRecordAccessLevelProcessDTO().getDivisionList());
                if (ids.trim().length() > 0) {
                    if (hql.trim().length() != 0) {
                        hql = hql + " AND ";
                    }
                    hql = hql + " division.id   IN (" + ids + ")";
                }
            }
        }


        if (!locationList.isEmpty()) {
            String ids = CommonUtils.commaSeparatedIds(locationList);
            if (ids.trim().length() > 0) {
                if (hql.trim().length() != 0) {
                    hql = hql + " AND ";
                }
                hql = hql + " loc.id   IN (" + ids + ")";
            }
        }

        return hql;
    }

    public SearchRespDto searchRecentRecords(RecentRecordsRequest request) {

        log.info(request);

        String searchQuery = "select new com.efreightsuite.dto.RecentRecordsResponse ( "
                + " el.id, "
                + " el.locationMaster.locationName,"
                + " el.loggedOn, "
                + " el.loggedBy.employeeName, "
                + " el.partyMaster.partyName, "
                + " el.enquiryNo, "
                + " el.status, "
                + " el.quotationNo, "
                + " ed.serviceMaster.serviceName, "
                + " ed.serviceMaster.serviceCode, "
                + " ed.fromPortMaster.portName, "
                + " ed.fromPortMaster.portCode, "
                + " ed.toPortMaster.portName, "
                + " ed.toPortMaster.portCode)"
                + " from EnquiryDetail ed "
                + " left join ed.enquiryLog el ";

		/*String countQuery = "select count(el.id)"
                + " from EnquiryDetail ed "
				+ " left join ed.enquiryLog el ";*/

        String filterQuery = "";
        if (request.getPartyId() != null) {
            filterQuery = " WHERE el.partyMaster.id = " + request.getPartyId();
        }

		/*countQuery = countQuery  + filterQuery;

		Query countQ = em.createQuery(countQuery);
		log.info("HQL Count Query For Search : " + countQuery);
		Long countTotal = (Long) countQ.getSingleResult();

		log.info("Total Number of Records Found : " + countTotal);*/


        String orderByQuery = " ORDER BY el.id DESC ";

        searchQuery = searchQuery + filterQuery + orderByQuery;

        log.info("HQL Query For Search : " + searchQuery);

        Query query = em.createQuery(searchQuery);

        query.setFirstResult(request.getSelectedPageNumber() * request.getRecordPerPage());

        query.setMaxResults(request.getRecordPerPage());

        @SuppressWarnings("unchecked")
        List<RecentRecordsResponse> response = query.getResultList();

        if (response != null && response.size() > 0) {

            for (RecentRecordsResponse recentRecordsResponse : response) {

                if (recentRecordsResponse.getQuotationNo() != null) {

                    recentRecordsResponse.setQuoteStatus(quotationRepository.findStatus(recentRecordsResponse.getQuotationNo()).name());
                }
            }

        }

        return new SearchRespDto((long) query.getResultList().size(), response);

    }


}
