package com.efreightsuite.search;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import com.efreightsuite.dto.SearchRespDto;
import com.efreightsuite.dto.SequenceGeneratorSearchDto;
import com.efreightsuite.model.SequenceGenerator;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class SequenceGeneratorSearchImpl {

    @Autowired
    private
    EntityManager em;

    public SearchRespDto search(SequenceGeneratorSearchDto dto) {

		/*String hql = " from SequenceGenerator where companyMaster.id = " + AuthService.getCurrentUser().getSelectedCompany().getId()+" ";*/
        String hql = "";
        if (dto.getSearchSequenceName() != null && dto.getSearchSequenceName().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }

            hql = hql + " UPPER(sequenceName) like '" + dto.getSearchSequenceName().trim().toUpperCase() + "%' ";
        }

        if (dto.getSearchSequenceType() != null && dto.getSearchSequenceType().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }

            hql = hql + " UPPER(sequenceType) like '" + dto.getSearchSequenceType().trim().toUpperCase() + "%' ";
        }

        if (dto.getSearchCurrentSequenceValue() != null && dto.getSearchCurrentSequenceValue().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }

            hql = hql + " UPPER(currentSequenceValue) like '" + dto.getSearchCurrentSequenceValue().trim().toUpperCase() + "%' ";
        }
        if (dto.getSearchPrefix() != null && dto.getSearchPrefix().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }

            hql = hql + " UPPER(prefix) like '" + dto.getSearchPrefix().trim().toUpperCase() + "%' ";
        }

        if (dto.getSearchFormat() != null && dto.getSearchFormat().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }

            hql = hql + " UPPER(format) like '" + dto.getSearchFormat().trim().toUpperCase() + "%' ";
        }

        if (hql.trim().length() != 0) {
            hql = " from SequenceGenerator where " + hql;
        } else {
            hql = " from SequenceGenerator ";
        }


        if (dto.getSortByColumn() != null && dto.getOrderByType() != null && dto.getSortByColumn().trim().length() != 0 && dto.getOrderByType().trim().length() != 0) {
            hql = hql + " order by Upper(" + dto.getSortByColumn().trim() + ") " + dto.getOrderByType().trim();
        } else {

            hql = hql + " order by Upper(category) ASC";
        }

        log.info("SequenceGenerator  Search hql:[" + hql + "]");

        return search(dto.getSelectedPageNumber(), dto.getRecordPerPage(), hql);
    }

    private SearchRespDto search(int selectedPageNumber, int recordPerPage, String hql) {

        String countQ = "Select count (id) " + hql;

        Query countQuery = em.createQuery(countQ);

        Long countTotal = (Long) countQuery.getSingleResult();

        TypedQuery<SequenceGenerator> query = em.createQuery(hql, SequenceGenerator.class);

        query.setFirstResult(selectedPageNumber * recordPerPage);

        query.setMaxResults(recordPerPage);

        List<SequenceGenerator> resultlist = query.getResultList();

        SearchRespDto searchRespDto = new SearchRespDto(countTotal, resultlist);

        log.info(resultlist.size() + " number of rows selected...");

        return searchRespDto;
    }

}
