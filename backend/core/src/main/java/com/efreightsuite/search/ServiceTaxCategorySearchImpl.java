package com.efreightsuite.search;

import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import com.efreightsuite.dto.SearchRespDto;
import com.efreightsuite.dto.ServiceTaxCategoryMasterSearchDto;
import com.efreightsuite.model.ServiceTaxCategoryMaster;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class ServiceTaxCategorySearchImpl {

    @Autowired
    private
    EntityManager em;

    public SearchRespDto search(ServiceTaxCategoryMasterSearchDto dto) {
        Map<String, Object> params = new HashMap<>();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

        String hql = "";

        if (dto.getCode() != null && dto.getCode().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }
            hql = hql + " UPPER(code) like '" + dto.getCode().trim().toUpperCase() + "%' ";

        }

        if (dto.getName() != null && dto.getName().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }
            hql = hql + " UPPER(name) like '" + dto.getName().trim().toUpperCase() + "%' ";
        }

        if (dto.getStatus() != null && dto.getStatus().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }
            hql = hql + " Upper(status) like '" + dto.getStatus().trim().toUpperCase() + "%' ";
        }


        if (hql.trim().length() != 0) {
            hql = " where " + hql;
        }


        hql = " from ServiceTaxCategoryMaster " + hql;


        if (dto.getSortByColumn() != null && dto.getOrderByType() != null && dto.getSortByColumn().trim().length() != 0
                && dto.getOrderByType().trim().length() != 0) {


            hql = hql + " order by Upper(" + dto.getSortByColumn().trim() + ") " + dto.getOrderByType().trim();


        } else {
            hql = hql + " order by id DESC";
        }

        log.info("ServiceTaxCategoryMaster Search hql:[" + hql + "]");

        String countQ = "Select count (id) " + hql;

        Query countQuery = em.createQuery(countQ);
        for (Entry<String, Object> e : params.entrySet()) {
            countQuery.setParameter(e.getKey(), e.getValue());
        }
        Long countTotal = (Long) countQuery.getSingleResult();


        TypedQuery<ServiceTaxCategoryMaster> query = em.createQuery(hql, ServiceTaxCategoryMaster.class);

        for (Entry<String, Object> e : params.entrySet()) {
            query.setParameter(e.getKey(), e.getValue());
        }

        query.setFirstResult(dto.getSelectedPageNumber() * dto.getRecordPerPage());

        query.setMaxResults(dto.getRecordPerPage());


        List<ServiceTaxCategoryMaster> resultlistRes = query.getResultList();


        SearchRespDto searchRespDto = new SearchRespDto(countTotal, resultlistRes);

        log.info(resultlistRes.size() + " number of rows are selected....");
        return searchRespDto;


    }
}
