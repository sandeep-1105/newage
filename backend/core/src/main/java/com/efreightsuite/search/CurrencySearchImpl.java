package com.efreightsuite.search;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import com.efreightsuite.dto.CurrencySearchDto;
import com.efreightsuite.dto.SearchRequest;
import com.efreightsuite.dto.SearchRespDto;
import com.efreightsuite.model.CurrencyMaster;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class CurrencySearchImpl {

    @Autowired
    private
    EntityManager em;

    public SearchRespDto search(CurrencySearchDto dto) {

        String hql = "";

        if (dto.getSearchCurrencyCode() != null && dto.getSearchCurrencyCode().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }

            hql = hql + " Upper(currencyCode) like '" + dto.getSearchCurrencyCode().trim().toUpperCase() + "%' ";
        }

        if (dto.getSearchCurrencyName() != null && dto.getSearchCurrencyName().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }

            hql = hql + " Upper(currencyName) like '" + dto.getSearchCurrencyName().trim().toUpperCase() + "%' ";
        }

        if (dto.getSearchCountryName() != null && dto.getSearchCountryName().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }

            hql = hql + " countryMaster.id IN (select cm.id from CountryMaster cm where Upper(cm.countryName) like '" + dto.getSearchCountryName().trim().toUpperCase() + "%') ";
        }

        if (dto.getSearchPrefix() != null && dto.getSearchPrefix().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }

            hql = hql + " Upper(prefix) like '" + dto.getSearchPrefix().trim().toUpperCase() + "%' ";
        }

        if (dto.getSearchSuffix() != null && dto.getSearchSuffix().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }

            hql = hql + " Upper(suffix) like '" + dto.getSearchSuffix().trim().toUpperCase() + "%' ";
        }

        if (dto.getSearchStatus() != null && dto.getSearchStatus().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }

            hql = hql + " Upper(status) like '" + dto.getSearchStatus().trim().toUpperCase() + "%' ";
        }

        if (hql.trim().length() != 0) {
            hql = " where " + hql;
        }

        hql = " from CurrencyMaster " + hql;


        if (dto.getSortByColumn() != null && dto.getOrderByType() != null && dto.getSortByColumn().trim().length() != 0 && dto.getOrderByType().trim().length() != 0) {
            hql = hql + " order by Upper(" + dto.getSortByColumn().trim() + ") " + dto.getOrderByType().trim();
        } else {
            hql = hql + " order by Upper(currencyName) ASC";
        }
        //hql = " from CurrencyMaster " + hql + " order by currencyName";

        log.info("Currency Master Search hql:[" + hql + "]");

        return search(dto.getSelectedPageNumber(), dto.getRecordPerPage(), hql);


    }

    public SearchRespDto search(String excludeCurrencyCode, SearchRequest dto) {

        String hql = " from CurrencyMaster where status!='Hide' ";

        if (dto.getKeyword() != null && dto.getKeyword().trim().length() != 0) {

            hql = hql + " AND ( ";

            hql = hql + " UPPER(currencyName) like '" + dto.getKeyword().trim().toUpperCase() + "%' ";

            hql = hql + " OR ";

            hql = hql + " UPPER(currencyCode) like '" + dto.getKeyword().trim().toUpperCase() + "%' ";

            hql = hql + " ) ";
        }

        if (excludeCurrencyCode != null && excludeCurrencyCode.trim().length() != 0) {
            hql = hql + " AND UPPER(currencyCode) !='" + excludeCurrencyCode + "' ";
        }


        hql = hql + " order by currencyName ASC";

        log.info("Currency Master Search hql:[" + hql + "]");

        return search(dto.getSelectedPageNumber(), dto.getRecordPerPage(), hql);

    }

    private SearchRespDto search(int selectedPageNumber, int recordPerPage, String hql) {

        String countQ = "Select count (id) " + hql;

        Query countQuery = em.createQuery(countQ);

        Long countTotal = (Long) countQuery.getSingleResult();

        TypedQuery<CurrencyMaster> query = em.createQuery(hql, CurrencyMaster.class);

        query.setFirstResult(selectedPageNumber * recordPerPage);

        query.setMaxResults(recordPerPage);

        List<CurrencyMaster> resultlist = query.getResultList();

        return new SearchRespDto(countTotal, resultlist);

    }
}
