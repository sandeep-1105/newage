package com.efreightsuite.validation;

import com.efreightsuite.enumeration.LovStatus;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.model.ServiceTaxCategoryMaster;
import com.efreightsuite.service.RegularExpressionService;
import com.efreightsuite.util.RegExpName;
import com.efreightsuite.util.ValidateUtil;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Log4j2
@Service
public class ServiceTaxCategoryMasterValidator {

    @Autowired
    private
    RegularExpressionService regExpService;

    public void validate(ServiceTaxCategoryMaster serviceTaxCategoryMaster) {

        log.info("ServiceTaxCategoryMaster Started for ServiceTaxCategoryMaster : [" + serviceTaxCategoryMaster.getId() + "]");

        // ServiceTaxCategoryMaster Code Validation

        ValidateUtil.notNull(serviceTaxCategoryMaster.getCode(), ErrorCode.SERVICETAXCATEGORY_CODE_NOT_NULL);

        regExpService.validate(serviceTaxCategoryMaster.getCode(), RegExpName.Reg_Exp_Master_ServiceTaxCategroy_Code,
                ErrorCode.SERVICETAXCATEGORY_CODE_INVALID);

        log.info("ServiceTaxCategoryMaster Code validated...");

        // ServiceTaxCategoryMaster Name Validations

        ValidateUtil.notNull(serviceTaxCategoryMaster.getName(), ErrorCode.SERVICETAXCATEGORY_NAME_NOT_NULL);

        regExpService.validate(serviceTaxCategoryMaster.getCode(), RegExpName.Reg_Exp_Master_ServiceTaxCategroy_Name,
                ErrorCode.SERVICETAXCATEGORY_NAME_INVALID);

        log.info("ServiceTaxCategoryMaster Name validated...");

        if (serviceTaxCategoryMaster.getStatus() == null) {
            serviceTaxCategoryMaster.setStatus(LovStatus.Active);
        }

        ValidateUtil.notNull(serviceTaxCategoryMaster.getStatus(), ErrorCode.SERVICETAXCATEGORY_LOV_STATUS_NOT_NULL);

        log.info("ServiceTaxCategoryMaster Status Successfully Validated.....");

        log.info("ServiceTaxCategoryMasterValidator Finished for ServiceTaxCategoryMaster : [" + serviceTaxCategoryMaster.getId() + "]");
    }

}
