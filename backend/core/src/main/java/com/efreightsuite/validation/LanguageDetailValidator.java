package com.efreightsuite.validation;

import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.model.LanguageDetail;
import com.efreightsuite.util.ValidateUtil;

import org.springframework.stereotype.Service;


@Service
public class LanguageDetailValidator {

    public void validate(LanguageDetail languageDetail) {

        ValidateUtil.notNull(languageDetail.getLanguageCode(), ErrorCode.LANGUAGE_CODE_NOT_NULL);

        ValidateUtil.checkPattern(languageDetail.getLanguageCode(), "languageCode", true);

        ValidateUtil.notNull(languageDetail.getLanguageDesc(), ErrorCode.LANGUAGE_DESC_NOT_NULL);

        // ValidateUtil.checkPattern(languageDetail.getLanguageDesc(), "languageDesc", true);

        ValidateUtil.notNull(languageDetail.getGroupName(), ErrorCode.LANGUAGE_GROUP_NAME_NOT_NULL);

        //ValidateUtil.checkPattern(languageDetail.getGroupName(), "languageGroup", true);

        ValidateUtil.notNull(languageDetail.getLanguage(), ErrorCode.LANGUAGE_NOT_NULL);
    }
}
