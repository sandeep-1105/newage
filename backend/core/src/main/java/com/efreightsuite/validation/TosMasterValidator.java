package com.efreightsuite.validation;

import com.efreightsuite.enumeration.LovStatus;
import com.efreightsuite.enumeration.PPCC;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.model.TosMaster;
import com.efreightsuite.service.RegularExpressionService;
import com.efreightsuite.util.RegExpName;
import com.efreightsuite.util.ValidateUtil;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Log4j2
@Service
public class TosMasterValidator {

    @Autowired
    private
    RegularExpressionService regExpService;

    public void validate(TosMaster tosMaster) {

        ValidateUtil.notNull(tosMaster, ErrorCode.TOS_NOT_NULL);

        ValidateUtil.notNullOrEmpty(tosMaster.getTosCode(), ErrorCode.TOS_CODE_NOT_NULL);
        regExpService.validate(tosMaster.getTosCode(), RegExpName.Reg_Exp_Master_Tos_Code, ErrorCode.TOS_CODE_INVALID);

        ValidateUtil.notNullOrEmpty(tosMaster.getTosName(), ErrorCode.TOS_NAME_NOT_NULL);
        regExpService.validate(tosMaster.getTosName(), RegExpName.Reg_Exp_Master_Tos_Name, ErrorCode.TOS_NAME_INVALID);

        ValidateUtil.validateEnum(PPCC.class, tosMaster.getFreightPPCC(), ErrorCode.TOS_PP_CC_NOT_NULL);

        ValidateUtil.validateEnum(LovStatus.class, tosMaster.getStatus(), ErrorCode.TOS_STATUS_NOT_NULL);

        ValidateUtil.validateLength(tosMaster.getDescription(), 0, 4000, ErrorCode.TOS_DESCRIPTION_INVALID);
    }

}
