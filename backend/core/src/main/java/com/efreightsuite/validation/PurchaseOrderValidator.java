package com.efreightsuite.validation;

import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.model.PurchaseOrder;
import com.efreightsuite.repository.DefaultMasterDataRepository;
import com.efreightsuite.util.ValidateUtil;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author Sandhanapandian RR
 **/
@Service
public class PurchaseOrderValidator {


    @Autowired
    DefaultMasterDataRepository defaultMasterDataRepository;

    public void validate(PurchaseOrder purchaseOrder) {
        verifyBuyer(purchaseOrder);
        verifySupplier(purchaseOrder);
        verifyOrigin(purchaseOrder);
        verifyDestination(purchaseOrder);

        ValidateUtil.notNull(purchaseOrder.getPoDate(), ErrorCode.PO_DATE);
        if (purchaseOrder.getId() != null) {
            verifyPoNumber(purchaseOrder.getPoNo());
        }
        verifyOriginAgent(purchaseOrder);
        verifyDestinationAgent(purchaseOrder);
        ValidateUtil.notNull(purchaseOrder.getTransportMode(), ErrorCode.PO_TRANSPORT_MODE);
        verifyTos(purchaseOrder);

        if (purchaseOrder.getCompanyMaster() != null) {
            purchaseOrder.setCompanyCode(purchaseOrder.getCompanyMaster().getCompanyCode());
        }

        if (purchaseOrder.getLocationMaster() != null) {
            purchaseOrder.setLocationCode(purchaseOrder.getLocationMaster().getLocationCode());
        }
    }

    private void verifyPoNumber(String poNumber) {

        ValidateUtil.notNull(poNumber, ErrorCode.PO_NUMBER);
    }

    private void verifyTos(PurchaseOrder purchaseOrder) {
        if (purchaseOrder.getTosMaster() != null && purchaseOrder.getTosMaster().getId() != null) {
            ValidateUtil.notNullOrEmpty(purchaseOrder.getTosMaster().getTosCode(), ErrorCode.PO_TOS);
            ValidateUtil.isStatusBlocked(purchaseOrder.getTosMaster().getStatus(), ErrorCode.PO_TOS_BLOCK);
            ValidateUtil.isStatusHidden(purchaseOrder.getTosMaster().getStatus(), ErrorCode.PO_TOS_HIDE);
            purchaseOrder.setTosCode(purchaseOrder.getTosMaster().getTosCode());
        }
    }

    private void verifyDestination(PurchaseOrder purchaseOrder) {
        ValidateUtil.notNull(purchaseOrder.getOrigin(), ErrorCode.PO_DESTINATION);
        ValidateUtil.notNull(purchaseOrder.getOrigin().getId(), ErrorCode.PO_DESTINATION);
        ValidateUtil.notNullOrEmpty(purchaseOrder.getDestination().getPortCode(), ErrorCode.PO_DESTINATION);
        ValidateUtil.isStatusBlocked(purchaseOrder.getDestination().getStatus(), ErrorCode.PO_DESTINATION_BLOCK);
        ValidateUtil.isStatusHidden(purchaseOrder.getDestination().getStatus(), ErrorCode.PO_DESTINATION_HIDE);
        purchaseOrder.setDestinationCode(purchaseOrder.getDestinationCode());
    }

    private void verifyOrigin(PurchaseOrder purchaseOrder) {
        ValidateUtil.notNull(purchaseOrder.getOrigin(), ErrorCode.PO_ORIGIN);
        ValidateUtil.notNull(purchaseOrder.getOrigin().getId(), ErrorCode.PO_ORIGIN);
        ValidateUtil.notNullOrEmpty(purchaseOrder.getOrigin().getPortCode(), ErrorCode.PO_ORIGIN);
        ValidateUtil.isStatusBlocked(purchaseOrder.getOrigin().getStatus(), ErrorCode.PO_ORIGIN_BLOCK);
        ValidateUtil.isStatusHidden(purchaseOrder.getOrigin().getStatus(), ErrorCode.PO_ORIGIN_HIDE);
        purchaseOrder.setOriginCode(purchaseOrder.getOriginCode());
    }

    private void verifyBuyer(PurchaseOrder purchaseOrder) {
        ValidateUtil.notNull(purchaseOrder.getBuyer(), ErrorCode.PO_BUYER);
        ValidateUtil.notNull(purchaseOrder.getBuyer().getId(), ErrorCode.PO_BUYER);
        ValidateUtil.notNullOrEmpty(purchaseOrder.getBuyer().getBcCode(), ErrorCode.PO_BUYER);
        ValidateUtil.notNullOrEmpty(purchaseOrder.getBuyer().getBcName(), ErrorCode.PO_BUYER);
        ValidateUtil.isStatusBlocked(purchaseOrder.getBuyer().getStatus(), ErrorCode.PO_BUYER_BLOCK);
        ValidateUtil.isStatusHidden(purchaseOrder.getBuyer().getStatus(), ErrorCode.PO_BUYER_HIDE);
        purchaseOrder.setBuyerCode(purchaseOrder.getBuyer().getBcCode());
    }

    private void verifySupplier(PurchaseOrder purchaseOrder) {
        ValidateUtil.notNull(purchaseOrder.getSupplier(), ErrorCode.PO_SUPPLIER);
        ValidateUtil.notNull(purchaseOrder.getSupplier().getId(), ErrorCode.PO_SUPPLIER);
        ValidateUtil.notNullOrEmpty(purchaseOrder.getSupplier().getPartyCode(), ErrorCode.PO_SUPPLIER);
        ValidateUtil.isStatusBlocked(purchaseOrder.getSupplier().getStatus(), ErrorCode.PO_SUPPLIER_BLOCK);
        ValidateUtil.isStatusHidden(purchaseOrder.getSupplier().getStatus(), ErrorCode.PO_SUPPLIER_HIDE);
        purchaseOrder.setSupplierCode(purchaseOrder.getSupplier().getPartyCode());
    }

    private void verifyOriginAgent(PurchaseOrder purchaseOrder) {
        ValidateUtil.notNull(purchaseOrder.getOriginAgent(), ErrorCode.PO_ORIGIN_AGENT);
        ValidateUtil.notNull(purchaseOrder.getOriginAgent().getId(), ErrorCode.PO_ORIGIN_AGENT);
        ValidateUtil.notNullOrEmpty(purchaseOrder.getOriginAgent().getPartyCode(), ErrorCode.PO_ORIGIN_AGENT);
        ValidateUtil.isStatusBlocked(purchaseOrder.getOriginAgent().getStatus(), ErrorCode.PO_ORIGIN_AGENT_BLOCK);
        ValidateUtil.isStatusHidden(purchaseOrder.getOriginAgent().getStatus(), ErrorCode.PO_ORIGIN_AGENT_HIDE);
        purchaseOrder.setOriginAgentCode(purchaseOrder.getOriginAgent().getPartyCode());
    }

    private void verifyDestinationAgent(PurchaseOrder purchaseOrder) {
        ValidateUtil.notNull(purchaseOrder.getDestinationAgent(), ErrorCode.PO_DESTINATION_AGENT);
        ValidateUtil.notNull(purchaseOrder.getDestinationAgent().getId(), ErrorCode.PO_DESTINATION_AGENT);
        ValidateUtil.notNullOrEmpty(purchaseOrder.getDestinationAgent().getPartyCode(), ErrorCode.PO_DESTINATION_AGENT);
        ValidateUtil.isStatusBlocked(purchaseOrder.getDestinationAgent().getStatus(), ErrorCode.PO_DESTINATION_AGENT_BLOCK);
        ValidateUtil.isStatusHidden(purchaseOrder.getDestinationAgent().getStatus(), ErrorCode.PO_DESTINATION_AGENT_HIDE);
        purchaseOrder.setDestinationAgentCode(purchaseOrder.getDestinationAgent().getPartyCode());
    }

}
