package com.efreightsuite.validation;

import com.efreightsuite.enumeration.DimensionUnit;
import com.efreightsuite.enumeration.YesNo;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.model.EnquiryDetail;
import com.efreightsuite.model.EnquiryDimension;
import com.efreightsuite.model.LocationMaster;
import com.efreightsuite.repository.DefaultMasterDataRepository;
import com.efreightsuite.util.AppUtil;
import com.efreightsuite.util.ValidateUtil;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class EnquiryDimensionValidator {

    @Autowired
    private
    AppUtil appUtil;

    @Autowired
    DefaultMasterDataRepository defaultMasterDataRepository;

    public void validate(EnquiryDimension enquiryDimension, EnquiryDetail ed, LocationMaster location) {

        log.info("EnquiryDimension Validation Started......");
        verifyPieces(enquiryDimension);
        verifyLength(enquiryDimension, ed, location);
        verifyWidth(enquiryDimension, ed, location);
        verifyHeight(enquiryDimension, ed, location);
        verifyGrossWeight(enquiryDimension);
        log.info("EnquiryDimension Validation Finished......");
    }

    private void verifyPieces(EnquiryDimension enquiryDimension) {

        ValidateUtil.notNull(enquiryDimension.getNoOfPiece(), ErrorCode.QUOTATION_DIMENSION_PIECES_REQUIRED);

        ValidateUtil.belowRange(enquiryDimension.getNoOfPiece(), 0d, ErrorCode.QUOTATION_DIMENSION_PIECES_NEGATIVE);

        ValidateUtil.aboveRange(enquiryDimension.getNoOfPiece(), 1000000000d, ErrorCode.QUOTATION_DIMENSION_PIECES_NEGATIVE);

        log.info("Dimension No. Of Pieces Validated...............[" + enquiryDimension.getNoOfPiece() + "]");
    }

    private void verifyLength(EnquiryDimension enquiryDimension, EnquiryDetail ed, LocationMaster location) {

        ValidateUtil.notNull(enquiryDimension.getLength(), ErrorCode.QUOTATION_DIMENSION_LENGTH_REQUIRED);

        ValidateUtil.belowRange(enquiryDimension.getLength(), 0d, ErrorCode.QUOTATION_DIMENSION_LENGTH_NEGATIVE);

        ValidateUtil.aboveRange(enquiryDimension.getLength(), 100000d, ErrorCode.QUOTATION_DIMENSION_LENGTH_NEGATIVE);

        if (ed != null) {

            if (ed.getDimensionUnit() == DimensionUnit.CENTIMETERORKILOS) {
                String key = appUtil.getLocationConfig("Centimeter_Length", location, false);
                Long val = Long.parseLong(key);

                if (enquiryDimension.getLength() > val) {
                    ed.setOverDimension(YesNo.Yes);
                }

            } else {
                String key = appUtil.getLocationConfig("Inch_Length", location, false);
                Long val = Long.parseLong(key);
                if (enquiryDimension.getLength() > val) {
                    ed.setOverDimension(YesNo.Yes);
                }
            }


        }

        log.info("Dimension Length Validated...............[" + enquiryDimension.getLength() + "]");
    }

    private void verifyWidth(EnquiryDimension enquiryDimension, EnquiryDetail ed, LocationMaster location) {

        if (ed != null) {

            if (ed.getDimensionUnit() == DimensionUnit.CENTIMETERORKILOS) {
                String key = appUtil.getLocationConfig("Centimeter_Width", location, false);
                Long val = Long.parseLong(key);

                if (enquiryDimension.getWidth() > val) {
                    ed.setOverDimension(YesNo.Yes);
                }

            } else {
                String key = appUtil.getLocationConfig("Inch_Width", location, false);
                Long val = Long.parseLong(key);
                if (enquiryDimension.getWidth() > val) {
                    ed.setOverDimension(YesNo.Yes);
                }
            }


        }


        ValidateUtil.notNull(enquiryDimension.getWidth(), ErrorCode.QUOTATION_DIMENSION_WIDTH_REQUIRED);

        ValidateUtil.belowRange(enquiryDimension.getWidth(), 0d, ErrorCode.QUOTATION_DIMENSION_WIDTH_NEGATIVE);

        ValidateUtil.aboveRange(enquiryDimension.getWidth(), 100000d, ErrorCode.QUOTATION_DIMENSION_WIDTH_NEGATIVE);

        log.info("Dimension Width Validated...............[" + enquiryDimension.getWidth() + "]");
    }

    private void verifyHeight(EnquiryDimension enquiryDimension, EnquiryDetail ed, LocationMaster location) {

        if (ed != null) {

            if (ed.getDimensionUnit() == DimensionUnit.CENTIMETERORKILOS) {
                String key = appUtil.getLocationConfig("Centimeter_height", location, false);
                Long val = Long.parseLong(key);

                if (enquiryDimension.getHeight() > val) {
                    ed.setOverDimension(YesNo.Yes);
                }
            } else {
                String key = appUtil.getLocationConfig("Inch_height", location, false);
                Long val = Long.parseLong(key);
                if (enquiryDimension.getHeight() > val) {
                    ed.setOverDimension(YesNo.Yes);
                }
            }


        }

        ValidateUtil.notNull(enquiryDimension.getHeight(), ErrorCode.QUOTATION_DIMENSION_HEIGHT_REQUIRED);

        ValidateUtil.belowRange(enquiryDimension.getHeight(), 0, ErrorCode.QUOTATION_DIMENSION_HEIGHT_NEGATIVE);

        ValidateUtil.aboveRange(enquiryDimension.getHeight(), 100000d, ErrorCode.QUOTATION_DIMENSION_HEIGHT_NEGATIVE);

        log.info("Dimension Height Validated...............[" + enquiryDimension.getHeight() + "]");
    }

    private void verifyGrossWeight(EnquiryDimension enquiryDimension) {

        ValidateUtil.notNull(enquiryDimension.getGrossWeight(), ErrorCode.QUOTATION_DIMENSION_GROSS_WEIGHT_REQUIRED);

        ValidateUtil.belowRange(enquiryDimension.getGrossWeight(), 0, ErrorCode.QUOTATION_DIMENSION_GROSS_WEIGHT_NEGATIVE);

        ValidateUtil.aboveRange(enquiryDimension.getGrossWeight(), 1000000000d, ErrorCode.QUOTATION_DIMENSION_GROSS_WEIGHT_NEGATIVE);

        log.info("Dimension GrossWeight Validated ...............[" + enquiryDimension.getGrossWeight() + "]");
    }


}
