package com.efreightsuite.validation;

import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.model.CategoryMaster;
import com.efreightsuite.model.ChargeMaster;
import com.efreightsuite.model.ServiceMaster;
import com.efreightsuite.model.ServiceTaxChargeGroupMaster;
import com.efreightsuite.service.RegularExpressionService;
import com.efreightsuite.util.RegExpName;
import com.efreightsuite.util.ValidateUtil;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class ServiceTaxChargeGroupMasterValidator {

    @Autowired
    private
    RegularExpressionService regExpService;

    public void validate(ServiceTaxChargeGroupMaster stcGroupMaster) {

        validateServiceMaster(stcGroupMaster.getServiceMaster());
        validateChargeMaster(stcGroupMaster.getChargeMaster());
        validateCategoryMaster(stcGroupMaster.getCategoryMaster());

    }

    private void validateServiceMaster(ServiceMaster serviceMaster) {

        ValidateUtil.notNull(serviceMaster.getServiceName(), ErrorCode.SERVICE_NAME_NOT_NULL);

        regExpService.validate(serviceMaster.getServiceCode(), RegExpName.Reg_Exp_Master_Service_Name, ErrorCode.SERVICE_NAME_INVALID);

        ValidateUtil.notNull(serviceMaster.getServiceCode(), ErrorCode.SERVICE_CODE_NOT_NULL);

        regExpService.validate(serviceMaster.getServiceCode(), RegExpName.Reg_Exp_Master_Service_Code, ErrorCode.SERVICE_CODE_INVALID);

    }

    private void validateChargeMaster(ChargeMaster chargeMaster) {

        ValidateUtil.notNull(chargeMaster.getChargeCode(), ErrorCode.CHARGE_CODE_NOT_NULL);
        regExpService.validate(chargeMaster.getChargeCode(), RegExpName.Reg_Exp_Master_Charge_Code, ErrorCode.CHARGE_CODE_INVALID);
        ValidateUtil.validateLength(chargeMaster.getChargeCode(), 1, 10, ErrorCode.CHARGE_CODE_INVALID);


        ValidateUtil.notNull(chargeMaster.getChargeName(), ErrorCode.CHARGE_NAME_NOT_NULL);
        regExpService.validate(chargeMaster.getChargeName(), RegExpName.Reg_Exp_Master_Charge_Name, ErrorCode.CHARGE_NAME_INVALID);
        ValidateUtil.validateLength(chargeMaster.getChargeName(), 1, 100, ErrorCode.CHARGE_NAME_INVALID);

    }

    private void validateCategoryMaster(CategoryMaster categoryMaster) {

        ValidateUtil.notNullOrEmpty(categoryMaster.getCategoryCode(), ErrorCode.CATEGORY_CODE_NOT_NULL);
        ValidateUtil.validateLength(categoryMaster.getCategoryCode(), 1, 10, ErrorCode.CATEGORY_CODE_LENGTH_EXCEED);
        regExpService.validate(categoryMaster.getCategoryCode(), RegExpName.Reg_Exp_Master_CATEGORY_CODE, ErrorCode.CATEGORY_CODE_INVALID);


        ValidateUtil.notNullOrEmpty(categoryMaster.getCategoryName(), ErrorCode.CATEGORY_NAME_NOT_NULL);
        ValidateUtil.validateLength(categoryMaster.getCategoryName(), 1, 100, ErrorCode.CATEGORY_NAME_LENGTH_EXCEED);
        regExpService.validate(categoryMaster.getCategoryName(), RegExpName.Reg_Exp_Master_CATEGORY_NAME, ErrorCode.CATEGORY_NAME_INVALID);


    }

}
