package com.efreightsuite.validation;

import com.efreightsuite.enumeration.LovStatus;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.model.ObjectSubGroupMaster;
import com.efreightsuite.util.ValidateUtil;
import lombok.extern.log4j.Log4j2;

import org.springframework.stereotype.Service;

@Service
@Log4j2
public class ObjectSubGroupMasterValidator {


    public void validate(ObjectSubGroupMaster objectSubGroupMaster) {
        log.info("ObjectSubGroup Master Validation Started....[" + (objectSubGroupMaster.getId() != null ? objectSubGroupMaster.getId() : "") + "]");

        ValidateUtil.notNull(objectSubGroupMaster.getObjectSubGroupName(), ErrorCode.OBJECTSUBGROUP_NAME_NOT_NULL);
        ValidateUtil.notNull(objectSubGroupMaster.getObjectSubGroupCode(), ErrorCode.OBJECTSUBGROUP_CODE_NOT_NULL);
        validateObjectGroup(objectSubGroupMaster);
        if (objectSubGroupMaster.getStatus() == null) {
            objectSubGroupMaster.setStatus(LovStatus.Active);
        }

        log.info("ObjectSubGroup Master Validation Finished....[" + (objectSubGroupMaster.getId() != null ? objectSubGroupMaster.getId() : "") + "]");
    }


    private void validateObjectGroup(ObjectSubGroupMaster objectSubGroupMaster) {
        ValidateUtil.notNull(objectSubGroupMaster.getObjectSubGroupName(), ErrorCode.OBJECTGROUP_NAME_NOT_NULL);
        ValidateUtil.isStatusBlocked(objectSubGroupMaster.getObjectGroupMaster().getStatus(), ErrorCode.OBJECTGROUP_STATUS_BLOCK);
        ValidateUtil.isStatusHidden(objectSubGroupMaster.getObjectGroupMaster().getStatus(), ErrorCode.OBJECTGROUP_STATUS_HIDE);


    }
}
