package com.efreightsuite.validation;


import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.model.ServiceMapping;
import com.efreightsuite.util.ValidateUtil;
import lombok.extern.log4j.Log4j2;

import org.springframework.stereotype.Service;

@Service
@Log4j2
public class ServiceMappingValidator {

    public void validate(ServiceMapping serviceMapping) {
        log.info("ServiceMapping  validation started");

        ValidateUtil.notNull(serviceMapping.getImportService().getServiceName(), ErrorCode.SERVICE_IMPORT_NOT_NULL);
        ValidateUtil.notNull(serviceMapping.getExportService().getServiceName(), ErrorCode.SERVICE_EXPORT_NOT_NULL);

        if (serviceMapping.getImportService() != null) {
            serviceMapping.setImportCode(serviceMapping.getImportService().getServiceCode());
            ValidateUtil.isStatusBlocked(serviceMapping.getImportService().getStatus(), ErrorCode.SERVICE_IMPORT_BLOCK);
            ValidateUtil.isStatusHidden(serviceMapping.getImportService().getStatus(), ErrorCode.SERVICE_IMPORT_HIDE);
        }
        if (serviceMapping.getExportService() != null) {
            serviceMapping.setExportCode(serviceMapping.getExportService().getServiceCode());
            ValidateUtil.isStatusBlocked(serviceMapping.getExportService().getStatus(), ErrorCode.SERVICE_EXPORT_BLOCK);
            ValidateUtil.isStatusHidden(serviceMapping.getExportService().getStatus(), ErrorCode.SERVICE_EXPORT_HIDE);
        }

        log.info("ServiceMapping  validation Finished..");


    }


}

