package com.efreightsuite.validation;

import com.efreightsuite.enumeration.LovStatus;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.model.CurrencyMaster;
import com.efreightsuite.service.RegularExpressionService;
import com.efreightsuite.util.RegExpName;
import com.efreightsuite.util.ValidateUtil;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Log4j2
@Service
public class CurrencyMasterValidator {


    @Autowired
    CountryMasterValidator countryMasterValidator;

    @Autowired
    private
    RegularExpressionService regExpService;

    public void validate(CurrencyMaster currencyMaster) {


        ValidateUtil.notNull(currencyMaster, ErrorCode.CURRENCY_NOT_NULL);

        log.info("Currency Master Validation Begins.........[" + ((currencyMaster.getId() != null) ? currencyMaster.getId() : "") + "]");

        // Validate Currency Code

        ValidateUtil.notNullOrEmpty(currencyMaster.getCurrencyCode(), ErrorCode.CURRENCY_CODE_NOT_NULL_OR_EMPTY);
        ValidateUtil.validateLength(currencyMaster.getCurrencyCode(), 1, 3, ErrorCode.CURRENCY_CODE_LENGTH_INVALID);
        //ValidateUtil.checkPattern(currencyMaster.getCurrencyCode(), "currencyCode", true);
        regExpService.validate(currencyMaster.getCurrencyCode(), RegExpName.Reg_Exp_Master_CURRENCY_CODE, ErrorCode.CURRENCY_CODE_INVALID);
        log.debug("Currency Code Validated.");

        // Validate Currency Name

        ValidateUtil.notNullOrEmpty(currencyMaster.getCurrencyName(), ErrorCode.CURRENCY_NAME_NOT_NULL_OR_EMPTY);
        ValidateUtil.validateLength(currencyMaster.getCurrencyName(), 1, 100, ErrorCode.CURRENCY_NAME_LENGTH_INVALID);
        //ValidateUtil.checkPattern(currencyMaster.getCurrencyName(), "currencyName", true);
        regExpService.validate(currencyMaster.getCurrencyName(), RegExpName.Reg_Exp_Master_CURRENCY_NAME, ErrorCode.CURRENCY_NAME_INVALID);
        log.debug("Currency Name Validated.");


        // Validate Currency Lov Status

        ValidateUtil.validateEnum(LovStatus.class, currencyMaster.getStatus(), ErrorCode.CURRENCY_STATUS_NOT_NULL);
        log.debug("Currency Lov Status Validated.");


        // Validate Currency Prefix
        ValidateUtil.notNullOrEmpty(currencyMaster.getPrefix(), ErrorCode.CURRENCY_PREFIX_NOT_NULL_OR_EMPTY);
        ValidateUtil.validateLength(currencyMaster.getPrefix(), 1, 30, ErrorCode.CURRENCY_PREFIX_LENGTH_INVALID);
        //ValidateUtil.checkPattern(currencyMaster.getPrefix(), "currencyPrefix", true);
        regExpService.validate(currencyMaster.getPrefix(), RegExpName.Reg_Exp_Master_CURRENCY_PREFIX, ErrorCode.CURRENCY_PREFIX_INVALID);
        log.debug("Currency Prefix Validated.");

        // Validate Currency Suffix
        ValidateUtil.notNullOrEmpty(currencyMaster.getSuffix(), ErrorCode.CURRENCY_SUFFIX_NOT_NULL_OR_EMPTY);
        ValidateUtil.validateLength(currencyMaster.getSuffix(), 1, 30, ErrorCode.CURRENCY_SUFFIX_LENGTH_INVALID);
        //ValidateUtil.checkPattern(currencyMaster.getSuffix(), "currencySuffix", true);
        regExpService.validate(currencyMaster.getSuffix(), RegExpName.Reg_Exp_Master_CURRENCY_SUFFFIX, ErrorCode.CURRENCY_SUFFIX_INVALID);
        log.debug("Currency Suffix Validated.");


        // Validating Country

		
		/*ValidateUtil.notNull(currencyMaster.getCountryMaster(), ErrorCode.CURRENCY_COUNTRY_NOT_NULL);
        ValidateUtil.notNull(currencyMaster.getCountryMaster().getId(), ErrorCode.CURRENCY_COUNTRY_NOT_NULL);
		ValidateUtil.isStatusBlocked(currencyMaster.getCountryMaster().getStatus(), ErrorCode.CURRENCY_COUNTRY_BLOCK);
		ValidateUtil.isStatusHidden(currencyMaster.getCountryMaster().getStatus(), ErrorCode.CURRENCY_COUNTRY_HIDE);*/
        log.debug("Currency Country Validated.");


        ValidateUtil.notNull(currencyMaster.getDecimalPoint(), ErrorCode.CURRENCY_DECIMAL_REQUIRED);


        log.info("Currency Master Validation Ends.........[" + ((currencyMaster.getId() != null) ? currencyMaster.getId() : "") + "]");

    }

}
