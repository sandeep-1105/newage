package com.efreightsuite.validation;

import com.efreightsuite.enumeration.LovStatus;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.model.PartyTypeMaster;
import com.efreightsuite.service.RegularExpressionService;
import com.efreightsuite.util.RegExpName;
import com.efreightsuite.util.ValidateUtil;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class PartyTypeMasterValidator {

    @Autowired
    private
    RegularExpressionService regExpService;

    public void validate(PartyTypeMaster partyTypeMaster) {

        log.info("Party Type Master Validator Started..... Party Type ID : [" + partyTypeMaster.getId() + "]");


        ValidateUtil.notNull(partyTypeMaster.getPartyTypeCode(), ErrorCode.PARTY_TYPE_CODE_NOT_NULL);

        ValidateUtil.validateLength(partyTypeMaster.getPartyTypeCode(), 1, 10, ErrorCode.PARTY_TYPE_NAME_INVALID);

        regExpService.validate(partyTypeMaster.getPartyTypeCode(), RegExpName.Reg_Exp_Master_PARTY_TYPE_CODE, ErrorCode.PARTY_TYPE_CODE_INVALID);


        ValidateUtil.notNull(partyTypeMaster.getPartyTypeName(), ErrorCode.PARTY_TYPE_NAME_NOT_NULL);

        ValidateUtil.validateLength(partyTypeMaster.getPartyTypeName(), 1, 30, ErrorCode.PARTY_TYPE_NAME_INVALID);

        regExpService.validate(partyTypeMaster.getPartyTypeName(), RegExpName.Reg_Exp_Master_PARTY_TYPE_NAME, ErrorCode.PARTY_TYPE_NAME_INVALID);


        ValidateUtil.validateEnum(LovStatus.class, partyTypeMaster.getStatus(), ErrorCode.PARTY_TYPE_LOV_STATUS_NOT_NULL);


        log.info("Party Type Master Validator Finished..... Party Type ID : [" + partyTypeMaster.getId() + "]");


    }
}
