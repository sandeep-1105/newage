package com.efreightsuite.validation;

import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.model.SequenceGenerator;
import com.efreightsuite.service.AuthService;
import com.efreightsuite.util.ValidateUtil;
import lombok.extern.log4j.Log4j2;

import org.springframework.stereotype.Service;

@Service
@Log4j2
public class SequenceGeneratorValidator {

    public void validator(SequenceGenerator sequenceGenerator) {
        log.info("SequenceGenerator  validation started");

        sequenceGenerator.setCompanyMaster(AuthService.getCurrentUser().getSelectedCompany());
        sequenceGenerator.setCompanyCode(AuthService.getCurrentUser().getSelectedCompany().getCompanyCode());

        ValidateUtil.notNull(sequenceGenerator.getSequenceType(), ErrorCode.SEQUENCE_TYPE_NOT_NULL);

        ValidateUtil.notNull(sequenceGenerator.getFormat(), ErrorCode.SEQUENCE_FORMAT_NOT_NULL);

        ValidateUtil.notNull(sequenceGenerator.getPrefix(), ErrorCode.SEQUENCE_PREFIX_NOT_NULL);

        log.info("SequenceGenerator validation ended");
    }

}
