package com.efreightsuite.validation;

import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.model.EmployeeMaster;
import com.efreightsuite.service.RegularExpressionService;
import com.efreightsuite.util.RegExpName;
import com.efreightsuite.util.ValidateUtil;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class EmployeeMasterValidator {


    @Autowired
    private
    RegularExpressionService regExpService;

    public void validate(EmployeeMaster employeeMaster) {
        log.info("Employee Master Validation Started....[" + (employeeMaster.getId() != null ? employeeMaster.getId() : "") + "]");

        if (employeeMaster.getLocationMaster() != null) {
            employeeMaster.setCompanyMaster(employeeMaster.getLocationMaster().getCompanyMaster());
        }

		/*ValidateUtil.notNullOrEmpty(employeeMaster.getEmployeeCode(), ErrorCode.EMPLOYEE_CODE_REQUIRED);
		regExpService.validate(employeeMaster.getEmployeeCode(), RegExpName.REG_EXP_EMPLOYEE_CODE, ErrorCode.EMPLOYEE_CODE_INVALID);
		*/

        //ValidateUtil.notNullOrEmpty(employeeMaster.getFirstName(), ErrorCode.EMPLOYEE_FIRST_NAME_REQUIRED);

        //ValidateUtil.notNullOrEmpty(employeeMaster.getMiddleName(), ErrorCode.EMPLOYEE_MIDDLE_NAME_REQUIRED);

        //ValidateUtil.notNullOrEmpty(employeeMaster.getLastName(), ErrorCode.EMPLOYEE_LAST_NAME_REQUIRED);

        //regExpService.validate(employeeMaster.getFirstName(), RegExpName.REG_EXP_EMPLOYEE_NAME, ErrorCode.EMPLOYEE_FIRST_NAME_INVALID);

        //regExpService.validate(employeeMaster.getMiddleName(), RegExpName.REG_EXP_EMPLOYEE_NAME, ErrorCode.EMPLOYEE_MIDDLE_NAME_INVALID);

        //regExpService.validate(employeeMaster.getLastName(), RegExpName.REG_EXP_EMPLOYEE_NAME, ErrorCode.EMPLOYEE_LAST_NAME_INVALID);

        ValidateUtil.notNullOrEmpty(employeeMaster.getAliasName(), ErrorCode.EMPLOYEE_ALIAS_NAME_REQUIRED);


        ValidateUtil.notNull(employeeMaster.getEmployementStatus(), ErrorCode.EMPLOYEE_EMPLOYMENT_STATUS_REQUIRED);


        ValidateUtil.notNullOrEmpty(employeeMaster.getEmail(), ErrorCode.EMPLOYEE_EMAIL_REQUIRED);

        ValidateUtil.validateLength(employeeMaster.getEmail(), 0, 500, ErrorCode.EMPLOYEE_EMAIL_ADDRESS_INVALID);

        if (employeeMaster.getCc_email() != null && employeeMaster.getCc_email().length() != 0) {
            ValidateUtil.validateLength(employeeMaster.getCc_email(), 0, 500, ErrorCode.EMPLOYEE_EMAIL_ADDRESS_INVALID);
        }

        if (employeeMaster.getEmployeePhoneNo() != null && employeeMaster.getEmployeePhoneNo().length() != 0) {
            regExpService.validate(employeeMaster.getEmployeePhoneNo(), RegExpName.REG_EXP_PHONE_NUMBER, ErrorCode.EMPLOYEE_PHONE_NUMBER_INVALID);
        }

        validateEmployeePersonalDetails(employeeMaster);

        validateEmployeeProfessionalDetails(employeeMaster);

        log.info("Employee Master Validation Finished....[" + (employeeMaster.getId() != null ? employeeMaster.getId() : "") + "]");
    }


    private void validateEmployeePersonalDetails(EmployeeMaster employeeMaster) {


        ValidateUtil.notNull(employeeMaster.getEmployeePersonalDetail().getDateOfBirth(), ErrorCode.EMPLOYEE_DOB_REQUIRED);

        //ValidateUtil.notNull(employeeMaster.getEmployeePersonalDetail().getGender(), ErrorCode.EMPLOYEE_GENDER_REQUIRED);

        ValidateUtil.notNull(employeeMaster.getCountryMaster(), ErrorCode.EMPLOYEE_NATIONALITY_REQUIRED);
        ValidateUtil.isStatusBlocked(employeeMaster.getCountryMaster().getStatus(), ErrorCode.EMPLOYEE_NATIONALITY_BLOCKED);
        ValidateUtil.isStatusHidden(employeeMaster.getCountryMaster().getStatus(), ErrorCode.EMPLOYEE_NATIONALITY_HIDDEN);


        if (employeeMaster.getEmployeePersonalDetail().getPhoneNumber() != null && employeeMaster.getEmployeePersonalDetail().getPhoneNumber().trim().length() != 0) {
            regExpService.validate(employeeMaster.getEmployeePersonalDetail().getPhoneNumber(), RegExpName.REG_EXP_PHONE_NUMBER, ErrorCode.EMPLOYEE_PHONE_NUMBER_INVALID);
        }


        if (employeeMaster.getEmployeePersonalDetail().getFax() != null && employeeMaster.getEmployeePersonalDetail().getFax().trim().length() != 0) {
            regExpService.validate(employeeMaster.getEmployeePersonalDetail().getFax(), RegExpName.REG_EXP_FAX_NUMBER, ErrorCode.EMPLOYEE_PHONE_NUMBER_INVALID);
        }


        if (employeeMaster.getEmployeePersonalDetail().getPersonalEmail() != null && employeeMaster.getEmployeePersonalDetail().getPersonalEmail().trim().length() != 0) {
            ValidateUtil.validateLength(employeeMaster.getEmployeePersonalDetail().getPersonalEmail(), 0, 500, ErrorCode.EMPLOYEE_EMAIL_ADDRESS_INVALID);
        }


        if (employeeMaster.getEmployeePersonalDetail().getMobileNumber() != null && employeeMaster.getEmployeePersonalDetail().getMobileNumber().trim().length() != 0) {
            regExpService.validate(employeeMaster.getEmployeePersonalDetail().getMobileNumber(), RegExpName.REG_EXP_MOBILE_NUMBER, ErrorCode.EMPLOYEE_MOBILE_NUMBER_INVALID);
        }

        if (employeeMaster.getEmployeePersonalDetail().getPermanentPhoneNumber() != null && employeeMaster.getEmployeePersonalDetail().getPermanentPhoneNumber().trim().length() != 0) {
            regExpService.validate(employeeMaster.getEmployeePersonalDetail().getPermanentPhoneNumber(), RegExpName.REG_EXP_PHONE_NUMBER, ErrorCode.EMPLOYEE_PHONE_NUMBER_INVALID);
        }


        if (employeeMaster.getEmployeePersonalDetail().getTemporaryPhoneNumber() != null && employeeMaster.getEmployeePersonalDetail().getTemporaryPhoneNumber().trim().length() != 0) {
            regExpService.validate(employeeMaster.getEmployeePersonalDetail().getTemporaryPhoneNumber(), RegExpName.REG_EXP_PHONE_NUMBER, ErrorCode.EMPLOYEE_PHONE_NUMBER_INVALID);
        }

        if (employeeMaster.getEmployeePersonalDetail().getEmergencyPhoneNumber() != null && employeeMaster.getEmployeePersonalDetail().getEmergencyPhoneNumber().trim().length() != 0) {
            regExpService.validate(employeeMaster.getEmployeePersonalDetail().getEmergencyPhoneNumber(), RegExpName.REG_EXP_PHONE_NUMBER, ErrorCode.EMPLOYEE_PHONE_NUMBER_INVALID);
        }


    }


    private void validateEmployeeProfessionalDetails(EmployeeMaster employeeMaster) {

        ValidateUtil.notNull(employeeMaster.getLocationMaster(), ErrorCode.EMPLOYEE_LOCATION_MANDATORY);

        if (employeeMaster.getLocationMaster() != null && employeeMaster.getLocationMaster().getId() != null) {

            ValidateUtil.isStatusBlocked(employeeMaster.getLocationMaster().getStatus(), ErrorCode.EMPLOYEE_LOCATION_BLOCKED);
            ValidateUtil.isStatusHidden(employeeMaster.getLocationMaster().getStatus(), ErrorCode.EMPLOYEE_LOCATION_HIDDEN);

        }
        ValidateUtil.notNull(employeeMaster.getEmployeeProfessionalDetail().getDepartmentMaster(), ErrorCode.EMPLOYEE_DEPARTMENT_MANDATORY);

        if (employeeMaster.getEmployeeProfessionalDetail().getDepartmentMaster() != null && employeeMaster.getEmployeeProfessionalDetail().getDepartmentMaster().getId() != null) {

            ValidateUtil.isStatusBlocked(employeeMaster.getEmployeeProfessionalDetail().getDepartmentMaster().getStatus(), ErrorCode.EMPLOYEE_DEPARTMENT_BLOCKED);
            ValidateUtil.isStatusHidden(employeeMaster.getEmployeeProfessionalDetail().getDepartmentMaster().getStatus(), ErrorCode.EMPLOYEE_DEPARTMENT_HIDDEN);

        }


        if (employeeMaster.getEmployeeProfessionalDetail().getDepartmentHead() != null && employeeMaster.getEmployeeProfessionalDetail().getDepartmentHead().getId() != null) {

            ValidateUtil.isEmployeeResigned(employeeMaster.getEmployeeProfessionalDetail().getDepartmentHead().getEmployementStatus(), ErrorCode.EMPLOYEE_DEPARTMENT_HEAD_RESIGNED);
            ValidateUtil.isEmployeeTerminated(employeeMaster.getEmployeeProfessionalDetail().getDepartmentHead().getEmployementStatus(), ErrorCode.EMPLOYEE_DEPARTMENT_HEAD_TERMINATED);

        }

        if (employeeMaster.getEmployeeProfessionalDetail().getReportingTo() != null && employeeMaster.getEmployeeProfessionalDetail().getReportingTo().getId() != null) {

            ValidateUtil.isEmployeeResigned(employeeMaster.getEmployeeProfessionalDetail().getReportingTo().getEmployementStatus(), ErrorCode.EMPLOYEE_REPORTING_TO_RESIGNED);
            ValidateUtil.isEmployeeTerminated(employeeMaster.getEmployeeProfessionalDetail().getReportingTo().getEmployementStatus(), ErrorCode.EMPLOYEE_REPORTING_TO_TERMINATED);

        }

        ValidateUtil.notNull(employeeMaster.getEmployeeProfessionalDetail().getDesignationMaster(), ErrorCode.EMPLOYEE_DESIGNATION_MANDATORY);

        if (employeeMaster.getEmployeeProfessionalDetail().getDesignationMaster() != null && employeeMaster.getEmployeeProfessionalDetail().getDesignationMaster().getId() != null) {

            ValidateUtil.isStatusBlocked(employeeMaster.getEmployeeProfessionalDetail().getDesignationMaster().getStatus(), ErrorCode.EMPLOYEE_DESIGNATION_BLOCKED);
            ValidateUtil.isStatusHidden(employeeMaster.getEmployeeProfessionalDetail().getDesignationMaster().getStatus(), ErrorCode.EMPLOYEE_DESIGNATION_BLOCKED);

        }


    }


}
