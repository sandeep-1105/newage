package com.efreightsuite.validation;

import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.model.InvoiceCreditNoteAttachment;
import com.efreightsuite.repository.DefaultMasterDataRepository;
import com.efreightsuite.service.AuthService;
import com.efreightsuite.util.AppUtil;
import com.efreightsuite.util.ValidateUtil;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author user
 */
@Service
@Log4j2
public class InvoiceAttachmentValidator {

    @Autowired
    private
    AppUtil appUtil;


    @Autowired
    DefaultMasterDataRepository defaultMasterDataRepository;

    public void validate(InvoiceCreditNoteAttachment invoiceAttachment) {

        log.info("Invoice Attachment Validation Started....[" + invoiceAttachment.getId() + "]");

        verifyReferenceNumber(invoiceAttachment);

        verifyFile(invoiceAttachment);

        log.info("Invoice Attachment Validated....[" + invoiceAttachment.getId() + "]");
    }

    private void verifyReferenceNumber(InvoiceCreditNoteAttachment invoiceAttachment) {

        ValidateUtil.notNullOrEmpty(invoiceAttachment.getRefNo(), ErrorCode.INVOICE_ATTACHMENT_REFERENCE_NUMBER_REQUIRED);

        ValidateUtil.checkPattern(invoiceAttachment.getRefNo(), "attachment.refno", true);
    }

    private void verifyFile(InvoiceCreditNoteAttachment invoiceAttachment) {

        if (invoiceAttachment.getId() == null) {

            if (invoiceAttachment.getFile() != null && invoiceAttachment.getFileName() != null) {
                String key = appUtil.getLocationConfig("upload.invoice.file.size.in.mb", AuthService.getCurrentUser().getSelectedUserLocation(), false);


                if (key != null && key.trim().length() != 0) {
                    long FileSize = invoiceAttachment.getFile().length;
                    long allowedFileSize = Long.parseLong(key) * 1024 * 1024;
                    if (FileSize == 0) {
                        throw new RestException(ErrorCode.INVOICE_ATTACHMENT_FILE_EMPTY);
                    }

                    if (FileSize > allowedFileSize) {
                        throw new RestException(ErrorCode.INVOICE_ATTACHMENT_FILE_SIZE_EXCEED);
                    }

                }
            }
        }
    }

}
