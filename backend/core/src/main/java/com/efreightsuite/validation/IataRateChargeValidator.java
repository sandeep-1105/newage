package com.efreightsuite.validation;

import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.model.IataRateCharge;
import com.efreightsuite.model.PortMaster;
import com.efreightsuite.service.RegularExpressionService;
import com.efreightsuite.util.RegExpName;
import com.efreightsuite.util.ValidateUtil;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class IataRateChargeValidator {

    @Autowired
    private
    RegularExpressionService regExpService;

    public void validate(IataRateCharge iataRateCharge) {

        validatePod(iataRateCharge.getPod());
        validateMinAmount(iataRateCharge.getMinAmount());
        validateNormal(iataRateCharge.getNormal());
        validatePlus45(iataRateCharge.getPlus45());
        validatePlus100(iataRateCharge.getPlus100());
        validatePlus300(iataRateCharge.getPlus300());
        validatePlus500(iataRateCharge.getPlus500());
        validatePlus1000(iataRateCharge.getPlus1000());
    }

    private void validatePod(PortMaster pod) {
        ValidateUtil.notNull(pod, ErrorCode.POD_IS_EMPTY);
        ValidateUtil.notNull(pod.getId(), ErrorCode.POD_IS_EMPTY);
        ValidateUtil.notNull(pod.getStatus(), ErrorCode.POD_STATUS_EMPTY);
        ValidateUtil.isStatusBlocked(pod.getStatus(), ErrorCode.POD_IS_BLOCKED);
        ValidateUtil.isStatusHidden(pod.getStatus(), ErrorCode.POD_IS_HIDDEN);

    }

    private void validateMinAmount(Double minAmount) {
        if (minAmount != null) {
            regExpService.validate(minAmount, RegExpName.Reg_Exp_AMOUNT_TEN_SIX, ErrorCode.MIN_AMOUNT_INVALID);
            ValidateUtil.nonZero(minAmount, ErrorCode.MINAMOUNT_NOT_ZERO);
        }

    }

    private void validateNormal(Double normal) {
        if (normal != null) {
            regExpService.validate(normal, RegExpName.Reg_Exp_AMOUNT_TEN_SIX, ErrorCode.NORMAL_INVALID);
            ValidateUtil.nonZero(normal, ErrorCode.NORMAL_NOT_ZERO);
        }
    }

    private void validatePlus45(Double plus45) {
        if (plus45 != null) {
            regExpService.validate(plus45, RegExpName.Reg_Exp_AMOUNT_TEN_SIX, ErrorCode.PLUS45_INVALID);
            ValidateUtil.nonZero(plus45, ErrorCode.PLUS45_NOT_ZERO);
        }
    }

    private void validatePlus100(Double plus100) {
        if (plus100 != null) {
            regExpService.validate(plus100, RegExpName.Reg_Exp_AMOUNT_TEN_SIX, ErrorCode.PLUS100_INVALID);
            ValidateUtil.nonZero(plus100, ErrorCode.PLUS100_NOT_ZERO);
        }
    }

    private void validatePlus300(Double plus300) {
        if (plus300 != null) {
            regExpService.validate(plus300, RegExpName.Reg_Exp_AMOUNT_TEN_SIX, ErrorCode.PLUS300_INVALID);
            ValidateUtil.nonZero(plus300, ErrorCode.PLUS300_NOT_ZERO);
        }
    }

    private void validatePlus500(Double plus500) {
        if (plus500 != null) {
            regExpService.validate(plus500, RegExpName.Reg_Exp_AMOUNT_TEN_SIX, ErrorCode.PLUS500_INVALID);
            ValidateUtil.nonZero(plus500, ErrorCode.PLUS500_NOT_ZERO);
        }
    }

    private void validatePlus1000(Double plus1000) {
        if (plus1000 != null) {
            regExpService.validate(plus1000, RegExpName.Reg_Exp_AMOUNT_TEN_SIX, ErrorCode.PLUS1000_INVALID);
            ValidateUtil.nonZero(plus1000, ErrorCode.PLUS1000_NOT_ZERO);
        }
    }

}
