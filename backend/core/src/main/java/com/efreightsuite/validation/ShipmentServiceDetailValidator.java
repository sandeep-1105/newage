package com.efreightsuite.validation;

import com.efreightsuite.activiti.ActivitiTransactionService;
import com.efreightsuite.common.component.CacheRepository;
import com.efreightsuite.enumeration.*;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.model.*;
import com.efreightsuite.repository.*;
import com.efreightsuite.service.AuthService;
import com.efreightsuite.service.RegularExpressionService;
import com.efreightsuite.util.*;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Objects;

/**
 * @author user
 */
@Service
@Log4j2
public class ShipmentServiceDetailValidator {

    @Autowired
    private
    ActivitiTransactionService activitiTransactionService;

    @Autowired
    private
    ShipmentServiceDetailRepository shipmentServiceDetailRepository;

    @Autowired
    private
    ShipmentServiceSignOffRepository shipmentServiceSignOffRepository;

    @Autowired
    private
    ProvisionalRepository provisionalRepository;

    @Autowired
    DefaultMasterDataRepository defaultMasterDataRepository;

    @Autowired
    private
    DateConfigurationRepository dateConfigurationRepository;

    @Autowired
    private
    CacheRepository cacheRepository;

    @Autowired
    private
    PartyMasterRepository partyMasterRepository;


    @Autowired
    private
    RegularExpressionService regExpService;

    @Autowired
    private
    CommentMasterRepository commentMasterRepository;

    @Autowired
    private
    ProjectMasterRepository projectMasterRepository;

    @Autowired
    private
    ServiceMasterRepository serviceMasterRepository;

    @Autowired
    private
    EmployeeMasterRepository employeeMasterRepository;

    @Autowired
    private
    PortMasterRepository portMasterRepository;

    @Autowired
    private
    TosMasterRepository tosMasterRepository;

    @Autowired
    private
    CompanyMasterRepository companyMasterRepository;

    @Autowired
    private
    LocationMasterRepository locationMasterRepository;

    @Autowired
    private
    DivisionMasterRepository divisionMasterRepository;

    @Autowired
    private
    PackMasterRepository packMasterRepository;

    @Autowired
    private
    CarrierMasterRepository carrierMasterRepository;

    @Autowired
    private
    CfsMasterRepository cfsMasterRepository;

    @Autowired
    private
    AppUtil appUtil;

    public void validate(Shipment shipment, ShipmentServiceDetail service) {
        if (service.getId() == null) {
            isShipmentCreatedFromQuotation(service.getQuotationUid());
        }

        verifyProject(service);
        verifyService(service);
        verifyCustomerService(service);
        verifyOrigin(service);
        verifyTos(service);
        verifyDestination(service);
        verifyPol(service);
        verifyPod(service);
        verifyWhoRouted(shipment, service);
        verifyParty(service);
        verifyColoaded(service);
        verifyCompany(service);
        verifyLocation(service);
        verifyDivision(service);
        verifyPack(service);
        //verifyNoOfPieces(service);
        //verifyWeight(service);
        verifyCarrier(service);
        verifyRouteNo(service);
        verifyMAWBNo(service);
        verifyEtd(service);
        verifyEta(service);
        verifyHold(service);
        verifyBrokerageParty(service, service.getBrokerageParty());
        verifyBrokeragePercentage(service.getBrokeragePercentage());
        if (service.getPickUpDeliveryPoint() != null) {
            verifyTransporter(service.getPickUpDeliveryPoint());
            verifyPickUp(service.getPickUpDeliveryPoint());
            verifyDelivery(service.getPickUpDeliveryPoint());
            verifyDoorDelivery(service.getPickUpDeliveryPoint());
        }


        verifyMawb(service);
        updateServiceDate(service);
    }

    public ShipmentServiceSignOff unSignOff(ShipmentServiceSignOff shipmentServiceSignOff, ShipmentServiceDetail service) {

        String key = appUtil.getLocationConfig("signoff.enabled", service.getLocation(), true);

        if (key.equals("FALSE")) {
            throw new RestException(ErrorCode.SHIPMENT_SIGN_OFF_NOT_ENABLED);
        }

        if (AuthService.getCurrentUser().getFeatureMap().get("CRM.SHIPMENT.SERVICE.MORE.UNSIGN_OFF.MODIFY") != null
                && AuthService.getCurrentUser().getFeatureMap().get("CRM.SHIPMENT.SERVICE.MORE.UNSIGN_OFF.MODIFY")) {

            ShipmentServiceSignOff signOff = shipmentServiceSignOffRepository.getOne(shipmentServiceSignOff.getId());

            if (signOff.getIsSignOff() == YesNo.No) {
                throw new RestException(ErrorCode.SHIPMENT_UISIGN_OFF_ALREADY);
            }


            if (shipmentServiceSignOff.getCommentMaster() == null || shipmentServiceSignOff.getCommentMaster().getId() == null) {
                String code = appUtil.getLocationConfig("unsignoff.default.comment", service.getLocation(), true);
                CommentMaster commentMaster = commentMasterRepository.findByCommentCode(code);
                if (commentMaster != null) {
                    shipmentServiceSignOff.setCommentMaster(commentMaster);

                    if (shipmentServiceSignOff.getDescription() == null) {
                        shipmentServiceSignOff.setDescription(commentMaster.getDescription());
                    }
                }
            }

            shipmentServiceSignOff.setIsSignOff(YesNo.No);
            shipmentServiceSignOff.setSignOffByCode(shipmentServiceSignOff.getSignOffBy().getEmployeeCode());
            shipmentServiceSignOff = shipmentServiceSignOffRepository.save(shipmentServiceSignOff);

            return shipmentServiceSignOff;
        } else {
            throw new RestException(ErrorCode.SHIPMENT_UNSIGN_OFF_INVALID_USER);
        }

    }

    public ShipmentServiceSignOff signOff(ShipmentServiceSignOff shipmentServiceSignOff, ShipmentServiceDetail service) {

        String key = appUtil.getLocationConfig("signoff.enabled", service.getLocation(), true);

        if (key.equals("FALSE")) {
            throw new RestException(ErrorCode.SHIPMENT_SIGN_OFF_NOT_ENABLED);
        }

        ShipmentServiceDetail sd = shipmentServiceDetailRepository.findByServiceUid(shipmentServiceSignOff.getServiceUid());
        UserProfile loginUser = AuthService.getCurrentUser();

        double netAmt = 0, revenueAmt = 0;

        if (sd.getConsolUid() == null) {

            if (sd.getShipmentChargeList() == null || sd.getShipmentChargeList().isEmpty()) {
                throw new RestException(ErrorCode.SHIPMENT_SIGN_OFF_CHARGE_NOT_FOUND);
            }

            String rateMerged = appUtil.getLocationConfig("booking.rates.merged", service.getLocation(), true);


            if (rateMerged.equals("TRUE")) {
                netAmt = sd.getRate() - sd.getCost();
                revenueAmt = sd.getRate();
            } else {
                netAmt = sd.getClientNetRate() - sd.getCost();
                revenueAmt = sd.getClientNetRate();
            }

        } else {

            Provisional tm = provisionalRepository.findByServiceUid(sd.getServiceUid());

            if (tm == null) {
                throw new RestException(ErrorCode.SHIPMENT_SIGN_OFF_PVOS_NOT_FOUND);
            }

            netAmt = tm.getNetAmount();
            revenueAmt = tm.getTotalRevenue();
        }


        boolean accessLevel = false;
        boolean superUser = false;
        boolean superUserFlag = false; //true - Profit, false - Loss

        if (sd.getAutoImport() != null
                && sd.getAutoImport() == YesNo.Yes
                && sd.getServiceMaster().getImportExport() == ImportExport.Import) {
            superUser = false;
            //Auto import job any one can sign off
        } else {
            if (netAmt == 0) {
                //booking person
            } else if (netAmt < 0) {
                superUser = true;
                superUserFlag = false;
            } else {
                double profit = ((netAmt * 100) / revenueAmt);
                String sigPrece = appUtil.getLocationConfig("shipment.signoff.profit.percentage", service.getLocation(), false);

                Double conFigProfilt = Double.parseDouble(sigPrece);
                if (profit > conFigProfilt) {
                    superUser = true;
                    superUserFlag = true;
                }
            }

        }

        if (superUser) {
            if (loginUser.getFeatureMap().get("CRM.SHIPMENT.SERVICE.MORE.SIGN_OFF.CREATE") != null
                    && loginUser.getFeatureMap().get("CRM.SHIPMENT.SERVICE.MORE.SIGN_OFF.CREATE")) {
                accessLevel = true;
            } else {
                if (superUserFlag) {
                    throw new RestException(ErrorCode.SHIPMENT_SIGN_OFF_INVALID_SUPER_USER_PROFIT);
                } else {
                    throw new RestException(ErrorCode.SHIPMENT_SIGN_OFF_INVALID_SUPER_USER_LOSS);
                }

            }
        }


        //Checking Booking Person and Nominate Person
        if (Objects.equals(sd.getCustomerService().getId(), loginUser.getEmployee().getId())
                || sd.getCustomerService().getId().equals(loginUser.getEmployee().getId())) {
            accessLevel = true;
        } else {
            if (loginUser.getEmployee().getNominateEmployee() != null
                    && (Objects.equals(loginUser.getEmployee().getId(), loginUser.getEmployee().getNominateEmployee().getId())
                    || loginUser.getEmployee().getId().equals(loginUser.getEmployee().getNominateEmployee().getId()))) {
                accessLevel = true;
            }
        }

        if (!accessLevel) {
            throw new RestException(ErrorCode.SHIPMENT_SIGN_OFF_INVALID_USER);
        }


        if (sd.getShipmentServiceSignOff() != null && sd.getShipmentServiceSignOff().getId() != null && sd.getShipmentServiceSignOff().getIsSignOff() == YesNo.Yes) {
            throw new RestException(ErrorCode.SHIPMENT_SIGN_OFF_ALREADY);

        }

        if (shipmentServiceSignOff.getCommentMaster() == null || shipmentServiceSignOff.getCommentMaster().getId() == null) {
            String code = appUtil.getLocationConfig("signoff.default.comment", service.getLocation(), true);
            CommentMaster commentMaster = commentMasterRepository.findByCommentCode(code);
            if (commentMaster != null) {
                shipmentServiceSignOff.setCommentMaster(commentMaster);

                if (shipmentServiceSignOff.getDescription() == null) {
                    shipmentServiceSignOff.setDescription(commentMaster.getDescription());
                }
            }
        }

        shipmentServiceSignOff.setIsSignOff(YesNo.Yes);
        shipmentServiceSignOff.setSignOffByCode(shipmentServiceSignOff.getSignOffBy().getEmployeeCode());
        shipmentServiceSignOff = shipmentServiceSignOffRepository.save(shipmentServiceSignOff);

        activitiTransactionService.signOff(sd.getShipment().getId(), sd);

        sd.setShipmentServiceSignOff(shipmentServiceSignOff);
        shipmentServiceDetailRepository.save(sd);


        return shipmentServiceSignOff;

    }

    public void updateServiceDate(ShipmentServiceDetail service) {

        if (service.getId() != null && service.getServiceReqDate() != null) {
            return;
        }

        DateConfiguration dConfig = dateConfigurationRepository.getByLogicDate(service.getServiceMaster().getId(), WhichTransactionDate.Service);

        if (dConfig != null) {
            switch (dConfig.getDateLogic()) {
                case ETD:
                    service.setServiceReqDate(service.getEtd() == null ? null : service.getEtd());
                    break;
                case ETA:
                    service.setServiceReqDate(service.getEta() == null ? null : service.getEta());
                    break;
                /*case Atd:
					service.setServiceReqDate(service.getAtd()==null?null:service.getAtd());
					break;
				case Ata:
					service.setServiceReqDate(service.getAta()==null?null:service.getAta());
					break;*/
                case Date:
                    service.setServiceReqDate(TimeUtil.getCurrentLocationTime());
                    break;
                default:
                    service.setServiceReqDate(TimeUtil.getCurrentLocationTime());
            }
        }

        if (service.getServiceReqDate() == null) {
            service.setServiceReqDate(TimeUtil.getCurrentLocationTime());
        }

    }

    private void verifyProject(ShipmentServiceDetail service) {
        if (service.getProject() != null && service.getProject().getId() != null) {
            ProjectMaster project = projectMasterRepository.getOne(service.getProject().getId());
            ValidateUtil.notNullOrEmpty(project.getProjectCode(), ErrorCode.SHIPMENT_PROJECT);
            ValidateUtil.isStatusBlocked(project.getStatus(), ErrorCode.SHIPMENT_PROJECT_BLOCK);
            ValidateUtil.isStatusHidden(project.getStatus(), ErrorCode.SHIPMENT_PROJECT_HIDE);

            service.setProjectCode(project.getProjectCode());
        } else {
            service.setProject(null);
            service.setProjectCode(null);
        }
    }

    private void verifyMawb(ShipmentServiceDetail service) {

        if (service.getMawbNo() != null && service.getMawbDate() == null) {
            service.setMawbDate(TimeUtil.getCurrentLocationTime());
        }

        if (service.getMawbNo() == null) {
            service.setMawbDate(null);
        }

    }

    private void verifyBrokeragePercentage(Double brokeragePercentage) {
        if (brokeragePercentage != null) {
            if (brokeragePercentage < 0 || brokeragePercentage > 99999999999.999) {
                throw new RestException(ErrorCode.SHIPMENT_BROKERAGE_INVALID);
            }
        }
    }

    private void verifyBrokerageParty(ShipmentServiceDetail service, PartyMaster brokerageParty) {
        if (brokerageParty != null && brokerageParty.getId() != null) {
            PartyMaster tmpParty = cacheRepository.get(SaaSUtil.getSaaSId(), PartyMaster.class.getName(), brokerageParty.getId(), partyMasterRepository);
            ValidateUtil.notNullOrEmpty(tmpParty.getPartyCode(), ErrorCode.SHIPMENT_BROKERAGE_PARTY_INVALID);
            ValidateUtil.isStatusBlocked(tmpParty.getStatus(), ErrorCode.SHIPMENT_BROKERAGE_PARTY_BLOCK);
            ValidateUtil.isStatusHidden(tmpParty.getStatus(), ErrorCode.SHIPMENT_BROKERAGE_PARTY_HIDE);

            ValidateUtil.isDefaulter(tmpParty, ErrorCode.SHIPMENT_BROKERAGE_PARTY_DEFAULTER);
            service.setBrokeragePartyCode(tmpParty.getPartyCode());
        } else {
            service.setBrokeragePartyCode(null);
        }
    }

    private void verifyHold(ShipmentServiceDetail service) {

        if (service.getHoldShipment() == YesNo.Yes) {
            ValidateUtil.notNull(service.getHoldNote(), ErrorCode.SHIPMENT_SERVICE_HOLD_NOTE);
        } else {

            if (service.getHoldNote() != null && service.getHoldNote().trim().length() != 0) {
                ValidateUtil.notNull(service.getHoldReleaseNote(), ErrorCode.SHIPMENT_SERVICE_HOLD_RELEASE_NOTE);
            }

        }

    }

    private void verifyEta(ShipmentServiceDetail service) {

        if (ValidateUtil.isCargoRecived(service)) {
            ValidateUtil.notNull(service.getEta(), ErrorCode.SHIPMENT_ETA_NOT_NULL);
        }

        if (service.getEta() != null) {
            ValidateUtil.notNull(service.getEta(), ErrorCode.SHIPMENT_ETA_NOT_NULL);
            if (service.getEtd() != null && service.getEtd().after(service.getEta())) {
                throw new RestException(ErrorCode.SHIPMENT_ETD_BEFORE_ETA);
            }
        }

    }

    private void verifyEtd(ShipmentServiceDetail service) {

        if (ValidateUtil.isCargoRecived(service)) {
            ValidateUtil.notNull(service.getEtd(), ErrorCode.SHIPMENT_ETD_NOT_NULL);
        }

        if (service.getEtd() != null) {
            ValidateUtil.notNull(service.getEtd(), ErrorCode.SHIPMENT_ETD_NOT_NULL);
        }

    }

    private void verifyMAWBNo(ShipmentServiceDetail service) {
        if (service.getMawbNo() != null && service.getMawbNo().trim().length() != 0) {
            ValidateUtil.validateLength(service.getRouteNo(), 1, 11, ErrorCode.SHIPMENT_MAWB_NO);
        }
    }

    private void verifyRouteNo(ShipmentServiceDetail service) {

        if (ValidateUtil.isCargoRecived(service)) {
            ValidateUtil.notNullOrEmpty(service.getRouteNo(), ErrorCode.SHIPMENT_ROUTE_NO);
        }
        if (service.getRouteNo() != null && service.getServiceMaster().getTransportMode() == TransportMode.Air) {
            regExpService.validate(service.getRouteNo(), RegExpName.REG_EXP_FLIGHT_NUMBER, ErrorCode.SHIPMENT_FLIGHT_NO);
            ValidateUtil.validateLength(service.getRouteNo(), 1, 10, ErrorCode.SHIPMENT_FLIGHT_NO);
        } else {
            if (service.getRouteNo() != null) {
                ValidateUtil.validateLength(service.getRouteNo(), 1, 10, ErrorCode.SHIPMENT_ROUTE_NO);
            }
        }

    }

    private void verifyCarrier(ShipmentServiceDetail service) {

        if (ValidateUtil.isCargoRecived(service)) {
            ValidateUtil.notNull(service.getCarrier(), ErrorCode.SHIPMENT_CARRIER);
            ValidateUtil.notNull(service.getCarrier().getId(), ErrorCode.SHIPMENT_CARRIER);
        }

        if (service.getCarrier() != null && service.getCarrier().getId() != null) {
            ValidateUtil.notNull(service.getCarrier().getId(), ErrorCode.SHIPMENT_CARRIER);
            CarrierMaster carrier = carrierMasterRepository.findById(service.getCarrier().getId());

            ValidateUtil.notNullOrEmpty(carrier.getCarrierCode(), ErrorCode.SHIPMENT_CARRIER);
            ValidateUtil.isStatusBlocked(carrier.getStatus(), ErrorCode.SHIPMENT_CARRIER_BLOCK);
            ValidateUtil.isStatusHidden(carrier.getStatus(), ErrorCode.SHIPMENT_CARRIER_HIDE);
            service.setCarrierCode(carrier.getCarrierCode());
        } else {
            service.setCarrier(null);
            service.setCarrierCode(null);
        }

    }

    private void verifyWeight(ShipmentServiceDetail service) {


        if (service.getBookedGrossWeightUnitKg() <= 0) {
            throw new RestException(ErrorCode.SHIPMENT_GROSS_LESS_ZERO);
        }


        if (service.getBookedVolumeWeightUnitKg() != null && service.getBookedVolumeWeightUnitKg() < 0) {
            throw new RestException(ErrorCode.SHIPMENT_VOLUME_LESS_ZERO);
        }

        ValidateUtil.notNull(service.getBookedChargeableUnit(), ErrorCode.SHIPMENT_CHARGEABLE_WEIGHT);
        if (service.getBookedChargeableUnit() <= 0) {
            throw new RestException(ErrorCode.SHIPMENT_CHARGE_LESS_ZERO);
        }


        if (service.getBookedGrossWeightUnitPound() != null) {
            ValidateUtil.notNull(service.getBookedGrossWeightUnitPound(), ErrorCode.SHIPMENT_GROSS_WEIGHT_POUND);
            if (service.getBookedGrossWeightUnitPound() != null && service.getBookedGrossWeightUnitPound() < 0) {
                throw new RestException(ErrorCode.SHIPMENT_GROSS_WEIGHT_POUND_LESS_ZERO);
            }
        } else {
            service.setBookedGrossWeightUnitPound(null);
        }

        if (service.getBookedVolumeWeightUnitPound() != null) {
            if (service.getBookedVolumeWeightUnitPound() != null && service.getBookedVolumeWeightUnitPound() < 0) {
                throw new RestException(ErrorCode.SHIPMENT_VOLUME_WEIGHT_POUND_LESS_ZERO);
            }
        } else {
            service.setBookedVolumeWeightUnitPound(null);
        }

    }

    private void verifyNoOfPieces(ShipmentServiceDetail service) {

        if (service.getBookedPieces() != null) {
            if (service.getBookedPieces() < 0) {
                throw new RestException(ErrorCode.SHIPMENT_PIECES_LESS_ZERO);
            }
        }
    }

    private void verifyPack(ShipmentServiceDetail service) {
        if (service.getPackMaster() != null && service.getPackMaster().getId() != null) {
            ValidateUtil.notNull(service.getPackMaster(), ErrorCode.SHIPMENT_PACK);
            ValidateUtil.notNull(service.getPackMaster().getId(), ErrorCode.SHIPMENT_PACK);
            PackMaster pack = packMasterRepository.findById(service.getPackMaster().getId());

            ValidateUtil.notNullOrEmpty(pack.getPackCode(), ErrorCode.SHIPMENT_PACK);
            ValidateUtil.isStatusBlocked(pack.getStatus(), ErrorCode.SHIPMENT_PACK_BLOCK);
            ValidateUtil.isStatusHidden(pack.getStatus(), ErrorCode.SHIPMENT_PACK_HIDE);

            service.setPackCode(pack.getPackCode());
        } else {
            service.setPackMaster(null);
            service.setPackCode(null);
        }

    }

    private void verifyDivision(ShipmentServiceDetail service) {

        if (service.getDivision() != null && service.getDivision().getId() != null) {
            ValidateUtil.notNull(service.getDivision(), ErrorCode.SHIPMENT_DIVISION);
            ValidateUtil.notNull(service.getDivision().getId(), ErrorCode.SHIPMENT_DIVISION);

            DivisionMaster division = divisionMasterRepository.getOne(service.getDivision().getId());

            ValidateUtil.notNullOrEmpty(division.getDivisionCode(), ErrorCode.SHIPMENT_DIVISION);
            ValidateUtil.isStatusBlocked(division.getStatus(), ErrorCode.SHIPMENT_DIVISION_BLOCK);
            ValidateUtil.isStatusHidden(division.getStatus(), ErrorCode.SHIPMENT_DIVISION_HIDE);

            service.setDivisionCode(division.getDivisionCode());
        } else {
            service.setDivision(null);
            service.setDivisionCode(null);
        }


    }

    private void verifyLocation(ShipmentServiceDetail service) {
        ValidateUtil.notNull(service.getLocation(), ErrorCode.SHIPMENT_LOCATION);
        ValidateUtil.notNull(service.getLocation().getId(), ErrorCode.SHIPMENT_LOCATION);
        LocationMaster location = locationMasterRepository.findById(service.getLocation().getId());

        ValidateUtil.notNullOrEmpty(location.getLocationCode(), ErrorCode.SHIPMENT_LOCATION);
        ValidateUtil.isStatusBlocked(location.getStatus(), ErrorCode.SHIPMENT_LOCATION_BLOCK);
        ValidateUtil.isStatusHidden(location.getStatus(), ErrorCode.SHIPMENT_LOCATION_HIDE);
        service.setLocationCode(location.getLocationCode());

        service.setCountry(location.getCountryMaster());
        service.setCountryCode(location.getCountryMaster().getCountryCode());
    }

    private void verifyCompany(ShipmentServiceDetail service) {
        ValidateUtil.notNull(service.getCompany(), ErrorCode.SHIPMENT_COMPANY);
        ValidateUtil.notNull(service.getCompany().getId(), ErrorCode.SHIPMENT_COMPANY);
        CompanyMaster company = companyMasterRepository.findById(service.getCompany().getId());

        ValidateUtil.notNullOrEmpty(company.getCompanyCode(), ErrorCode.SHIPMENT_COMPANY);
        ValidateUtil.isStatusBlocked(company.getStatus(), ErrorCode.SHIPMENT_COMPANY_BLOCK);
        ValidateUtil.isStatusHidden(company.getStatus(), ErrorCode.SHIPMENT_COMPANY_HIDE);

        service.setCompanyCode(company.getCompanyCode());
    }

    private void verifyColoaded(ShipmentServiceDetail service) {

        if (service.getCoLoader() != null && service.getCoLoader().getId() != null) {
            PartyMaster tmpParty = cacheRepository.get(SaaSUtil.getSaaSId(), PartyMaster.class.getName(), service.getCoLoader().getId(), partyMasterRepository);
            ValidateUtil.notNullOrEmpty(tmpParty.getPartyCode(), ErrorCode.SHIPMENT_COLOADER);

            ValidateUtil.isDefaulter(tmpParty, ErrorCode.SHIPMENT_CO_LOADER_DEFAULTER);

            ValidateUtil.isStatusBlocked(tmpParty.getStatus(), ErrorCode.SHIPMENT_COLOADER_BLOCK);
            ValidateUtil.isStatusHidden(tmpParty.getStatus(), ErrorCode.SHIPMENT_COLOADER_HIDE);

            service.setCoLoaderCode(tmpParty.getPartyCode());
        } else {
            service.setCoLoader(null);
            service.setCoLoaderCode(null);
        }


    }

    private void verifyParty(ShipmentServiceDetail service) {
        PartyMaster tmpParty = cacheRepository.get(SaaSUtil.getSaaSId(), PartyMaster.class.getName(), service.getParty().getId(), partyMasterRepository);

        ValidateUtil.notNull(tmpParty, ErrorCode.SHIPMENT_PARTY);
        ValidateUtil.notNull(tmpParty.getId(), ErrorCode.SHIPMENT_PARTY);

        ValidateUtil.notNullOrEmpty(tmpParty.getPartyCode(), ErrorCode.SHIPMENT_PARTY);
        ValidateUtil.isStatusBlocked(tmpParty.getStatus(), ErrorCode.SHIPMENT_PARTY_BLOCK);
        ValidateUtil.isStatusHidden(tmpParty.getStatus(), ErrorCode.SHIPMENT_PARTY_HIDE);

        ValidateUtil.isDefaulter(tmpParty, ErrorCode.SHIPMENT_PARTY_DEFAULTER);

        service.setPartyCode(tmpParty.getPartyCode());
    }

    private void verifyWhoRouted(Shipment shipment, ShipmentServiceDetail service) {
        if (service.getWhoRouted().equals(WhoRouted.Self)) {

            ValidateUtil.notNull(service.getSalesman(), ErrorCode.SHIPMENT_SALESMAN);
            ValidateUtil.notNull(service.getSalesman().getId(), ErrorCode.SHIPMENT_SALESMAN);
            EmployeeMaster salesman = employeeMasterRepository.getOne(service.getSalesman().getId());

            ValidateUtil.notNullOrEmpty(salesman.getEmployeeCode(), ErrorCode.SHIPMENT_SALESMAN);

            ValidateUtil.isEmployeeResigned(salesman.getEmployementStatus(), ErrorCode.SHIPMENT_SALESMAN_RESIGNED);
            ValidateUtil.isEmployeeTerminated(salesman.getEmployementStatus(), ErrorCode.SHIPMENT_SALESMAN_TERMINATED);

            ValidateUtil.isSalesman(salesman.getIsSalesman(), ErrorCode.SHIPMENT_SALESMAN_ISSALESMAN);

            service.setSalesmanCode(salesman.getEmployeeCode());

        } else {
	/*		//NEW SHIPMENT COMMENTS
	if(shipment.getDirectShipment()!=null && shipment.getDirectShipment().equals(YesNo.No)){		
		
		PartyMaster tmpParty = cacheRepository.get(SaaSUtil.getSaaSId(), PartyMaster.class.getName(), service.getAgent().getId(), partyMasterRepository);
		
			ValidateUtil.notNull(tmpParty, ErrorCode.SHIPMENT_AGENT);
			ValidateUtil.notNull(tmpParty.getId(), ErrorCode.SHIPMENT_AGENT);
			ValidateUtil.notNullOrEmpty(tmpParty.getPartyCode(), ErrorCode.SHIPMENT_AGENT);
			
			ValidateUtil.isDefaulter(tmpParty, ErrorCode.SHIPMENT_ROUT_AGENT_DEFAULTER);
			
			ValidateUtil.isStatusBlocked(tmpParty.getStatus(), ErrorCode.SHIPMENT_AGENT_BLOCK);
			ValidateUtil.isStatusHidden(tmpParty.getStatus(), ErrorCode.SHIPMENT_AGENT_HIDE);
			
			service.setAgentCode(tmpParty.getPartyCode());
		}else{
			
                if(service.getAgent()!=null){
                	PartyMaster tmpParty = cacheRepository.get(SaaSUtil.getSaaSId(), PartyMaster.class.getName(), service.getAgent().getId(), partyMasterRepository);
                	ValidateUtil.isDefaulter(tmpParty, ErrorCode.SHIPMENT_ROUT_AGENT_DEFAULTER);
                	ValidateUtil.isStatusBlocked(tmpParty.getStatus(), ErrorCode.SHIPMENT_AGENT_BLOCK);
        			ValidateUtil.isStatusHidden(tmpParty.getStatus(), ErrorCode.SHIPMENT_AGENT_HIDE);
        			service.setAgentCode(tmpParty.getPartyCode());
                }else{
                	service.setAgentCode(null);
                }
			
		}
		*/


        }
    }

    private void verifyPod(ShipmentServiceDetail service) {
        ValidateUtil.notNull(service.getPod(), ErrorCode.SHIPMENT_POD);
        ValidateUtil.notNull(service.getPod().getId(), ErrorCode.SHIPMENT_POD);

        PortMaster pol = portMasterRepository.getOne(service.getPol().getId());
        PortMaster pod = portMasterRepository.getOne(service.getPod().getId());
        ServiceMaster serviceMaster = serviceMasterRepository.getOne(service.getServiceMaster().getId());
        ValidateUtil.notNullOrEmpty(pod.getPortCode(), ErrorCode.SHIPMENT_POD);

        ValidateUtil.isStatusBlocked(pod.getStatus(), ErrorCode.SHIPMENT_POD_BLOCK);
        ValidateUtil.isStatusHidden(pod.getStatus(), ErrorCode.SHIPMENT_POD_HIDE);

        if (serviceMaster.getTransportMode() != pod.getTransportMode()) {
            throw new RestException(ErrorCode.SHIPMENT_SERVICE_POD_MODE);
        }

        if (Objects.equals(service.getPod().getId(), service.getPol().getId())) {
            throw new RestException(ErrorCode.SHIPMENT_ORIGIN_DESTINATION_EQUAL);
        }

        if (pod.getPortCode().equals(pol.getPortCode())) {
            throw new RestException(ErrorCode.SHIPMENT_POD_POL_EQUAL);
        }

        service.setPodCode(pod.getPortCode());
    }

    private void verifyPol(ShipmentServiceDetail service) {
        ValidateUtil.notNull(service.getPol(), ErrorCode.SHIPMENT_POL);
        ValidateUtil.notNull(service.getPol().getId(), ErrorCode.SHIPMENT_POL);
        PortMaster pol = portMasterRepository.getOne(service.getPol().getId());
        ServiceMaster serviceMaster = serviceMasterRepository.getOne(service.getServiceMaster().getId());

        ValidateUtil.notNullOrEmpty(pol.getPortCode(), ErrorCode.SHIPMENT_POL);

        ValidateUtil.isStatusBlocked(pol.getStatus(), ErrorCode.SHIPMENT_POL_BLOCK);
        ValidateUtil.isStatusHidden(pol.getStatus(), ErrorCode.SHIPMENT_POL_HIDE);

        if (serviceMaster.getTransportMode() != pol.getTransportMode()) {
            throw new RestException(ErrorCode.SHIPMENT_SERVICE_POL_MODE);
        }

        service.setPolCode(pol.getPortCode());
    }

    private void verifyDestination(ShipmentServiceDetail service) {
        ValidateUtil.notNull(service.getDestination(), ErrorCode.SHIPMENT_DESTINATION);
        ValidateUtil.notNull(service.getDestination().getId(), ErrorCode.SHIPMENT_DESTINATION);
        PortMaster destination = portMasterRepository.getOne(service.getDestination().getId());
        PortMaster origin = portMasterRepository.getOne(service.getOrigin().getId());
        ServiceMaster serviceMaster = serviceMasterRepository.getOne(service.getServiceMaster().getId());

        ValidateUtil.notNullOrEmpty(destination.getPortCode(), ErrorCode.SHIPMENT_DESTINATION);

        ValidateUtil.isStatusBlocked(destination.getStatus(), ErrorCode.SHIPMENT_DESTINATION_BLOCK);
        ValidateUtil.isStatusHidden(destination.getStatus(), ErrorCode.SHIPMENT_DESTINATION_HIDE);

        ValidateUtil.isAirTransportMode(destination.getTransportMode(), ErrorCode.SHIPMENT_DESTINATION_AIR_MODE);

        if (Objects.equals(service.getDestination().getId(), service.getOrigin().getId())) {
            throw new RestException(ErrorCode.SHIPMENT_ORIGIN_DESTINATION_EQUAL);
        }

        if (destination.getPortCode().equals(origin.getPortCode())) {
            throw new RestException(ErrorCode.SHIPMENT_ORIGIN_DESTINATION_EQUAL);
        }

        if (serviceMaster.getTransportMode() != destination.getTransportMode()) {
            throw new RestException(ErrorCode.SHIPMENT_SERVICE_DESTINATION_MODE);
        }

        service.setDestinationCode(destination.getPortCode());

    }

    private void verifyTos(ShipmentServiceDetail shipment) {//NEW SHIPMENT COMMENTS
        ValidateUtil.notNull(shipment.getTosMaster(), ErrorCode.SHIPMENT_TOS);
        ValidateUtil.notNull(shipment.getTosMaster().getId(), ErrorCode.SHIPMENT_TOS);

        TosMaster tos = tosMasterRepository.getOne(shipment.getTosMaster().getId());
        ValidateUtil.notNullOrEmpty(tos.getTosCode(), ErrorCode.SHIPMENT_TOS);

        ValidateUtil.isStatusBlocked(tos.getStatus(), ErrorCode.SHIPMENT_TOS_BLOCK);
        ValidateUtil.isStatusHidden(tos.getStatus(), ErrorCode.SHIPMENT_TOS_HIDE);

        shipment.setTosCode(tos.getTosCode());
    }

    private void verifyOrigin(ShipmentServiceDetail service) {
        ValidateUtil.notNull(service.getOrigin(), ErrorCode.SHIPMENT_ORIGIN);
        ValidateUtil.notNull(service.getOrigin().getId(), ErrorCode.SHIPMENT_ORIGIN);

        PortMaster port = portMasterRepository.getOne(service.getOrigin().getId());
        ValidateUtil.notNullOrEmpty(port.getPortCode(), ErrorCode.SHIPMENT_ORIGIN);

        ValidateUtil.isStatusBlocked(port.getStatus(), ErrorCode.SHIPMENT_ORIGIN_BLOCK);
        ValidateUtil.isStatusHidden(port.getStatus(), ErrorCode.SHIPMENT_ORIGIN_HIDE);

        ValidateUtil.isAirTransportMode(port.getTransportMode(), ErrorCode.SHIPMENT_ORIGIN_AIR_MODE);


        if (port.getTransportMode() != port.getTransportMode()) {
            throw new RestException(ErrorCode.SHIPMENT_SERVICE_ORIGIN_MODE);
        }

        service.setOriginCode(port.getPortCode());


    }

    private void verifyCustomerService(ShipmentServiceDetail service) {
        ValidateUtil.notNull(service.getCustomerService(), ErrorCode.SHIPMENT_CUSTOMER_SERVICE);
        ValidateUtil.notNull(service.getCustomerService().getId(), ErrorCode.SHIPMENT_CUSTOMER_SERVICE);

        EmployeeMaster employee = employeeMasterRepository.findById(service.getCustomerService().getId());
        ValidateUtil.notNullOrEmpty(employee.getEmployeeCode(), ErrorCode.SHIPMENT_CUSTOMER_SERVICE);
        ValidateUtil.isEmployeeResigned(employee.getEmployementStatus(), ErrorCode.SHIPMENT_CUSTOMER_SERVICE_RESIGNED);
        ValidateUtil.isEmployeeTerminated(employee.getEmployementStatus(), ErrorCode.SHIPMENT_CUSTOMER_SERVICE_TERMINATED);

        service.setCustomerServiceCode(employee.getEmployeeCode());
    }

    private void verifyService(ShipmentServiceDetail service) {
        ValidateUtil.notNull(service.getServiceMaster(), ErrorCode.SHIPMENT_SERVICE);
        ValidateUtil.notNull(service.getServiceMaster().getId(), ErrorCode.SHIPMENT_SERVICE);

        ServiceMaster serviceMS = serviceMasterRepository.getOne(service.getServiceMaster().getId());
        ValidateUtil.notNullOrEmpty(serviceMS.getServiceCode(), ErrorCode.SHIPMENT_SERVICE);
        ValidateUtil.isStatusBlocked(serviceMS.getStatus(), ErrorCode.SHIPMENT_SERVICE_BLOCK);
        ValidateUtil.isStatusHidden(serviceMS.getStatus(), ErrorCode.SHIPMENT_SERVICE_HIDE);

        service.setServiceCode(serviceMS.getServiceCode());
    }


    private void verifyTransporter(PickUpDeliveryPoint pickUpDelivery) {
        if (pickUpDelivery.getTransporter() != null && pickUpDelivery.getTransporter().getId() != null) {

            PartyMaster tmpParty = cacheRepository.get(SaaSUtil.getSaaSId(), PartyMaster.class.getName(), pickUpDelivery.getTransporter().getId(), partyMasterRepository);

            ValidateUtil.notNull(tmpParty, ErrorCode.SHIPMENT_TRANSPORTER);
            ValidateUtil.notNull(tmpParty.getId(), ErrorCode.SHIPMENT_TRANSPORTER);
            ValidateUtil.notNullOrEmpty(tmpParty.getPartyCode(), ErrorCode.SHIPMENT_TRANSPORTER);

            ValidateUtil.isDefaulter(tmpParty, ErrorCode.SHIPMENT_TRANS_TRANS_DEFAULTER);

            ValidateUtil.isStatusBlocked(tmpParty.getStatus(), ErrorCode.SHIPMENT_TRANSPORTER_BLOCK);
            ValidateUtil.isStatusHidden(tmpParty.getStatus(), ErrorCode.SHIPMENT_TRANSPORTER_HIDE);

            pickUpDelivery.setTransporterCode(tmpParty.getPartyCode());
        } else {
            pickUpDelivery.setTransporter(null);
            pickUpDelivery.setTransporterCode(null);
            pickUpDelivery.setTransportAddress(null);
        }

    }


    private void verifyDoorDelivery(PickUpDeliveryPoint pickUpDelivery) {

        if (pickUpDelivery.getDoorDeliveryPoint() != null && pickUpDelivery.getDoorDeliveryPoint().getId() != null) {
            ValidateUtil.notNull(pickUpDelivery.getDoorDeliveryPoint(), ErrorCode.SHIPMENT_SERVICE_DOOR_DELIVERY_POINT);
            ValidateUtil.notNull(pickUpDelivery.getDoorDeliveryPoint().getId(), ErrorCode.SHIPMENT_SERVICE_DOOR_DELIVERY_POINT);
            PortMaster doorDeliveryPoint = portMasterRepository.findById(pickUpDelivery.getDoorDeliveryPoint().getId());
            ValidateUtil.notNullOrEmpty(doorDeliveryPoint.getPortCode(), ErrorCode.SHIPMENT_SERVICE_DOOR_DELIVERY_POINT);

            ValidateUtil.isStatusBlocked(doorDeliveryPoint.getStatus(), ErrorCode.SHIPMENT_SERVICE_DOOR_DELIVERY_POINT_BLOCK);
            ValidateUtil.isStatusHidden(doorDeliveryPoint.getStatus(), ErrorCode.SHIPMENT_SERVICE_DOOR_DELIVERY_POINT_HIDE);

            pickUpDelivery.setDoorDeliveryPointCode(doorDeliveryPoint.getPortCode());
        } else {
            pickUpDelivery.setDoorDeliveryPoint(null);
            pickUpDelivery.setDoorDeliveryPointCode(null);
        }

        if (pickUpDelivery.getDoorDeliveryFrom() != null && pickUpDelivery.getDoorDeliveryFrom().getId() != null) {
            ValidateUtil.notNull(pickUpDelivery.getDoorDeliveryFrom(), ErrorCode.SHIPMENT_SERVICE_DOOR_DELIVERY_FROM);
            ValidateUtil.notNull(pickUpDelivery.getDoorDeliveryFrom().getId(), ErrorCode.SHIPMENT_SERVICE_DOOR_DELIVERY_FROM);
            PortMaster doorDeliveryFrom = portMasterRepository.findById(pickUpDelivery.getDoorDeliveryFrom().getId());

            ValidateUtil.notNullOrEmpty(doorDeliveryFrom.getPortCode(), ErrorCode.SHIPMENT_SERVICE_DOOR_DELIVERY_FROM);

            ValidateUtil.isStatusBlocked(doorDeliveryFrom.getStatus(), ErrorCode.SHIPMENT_SERVICE_DOOR_DELIVERY_FROM_BLOCK);
            ValidateUtil.isStatusHidden(doorDeliveryFrom.getStatus(), ErrorCode.SHIPMENT_SERVICE_DOOR_DELIVERY_FROM_HIDE);

            pickUpDelivery.setDoorDeliveryFromCode(doorDeliveryFrom.getPortCode());
        } else {
            pickUpDelivery.setDoorDeliveryFrom(null);
            pickUpDelivery.setDoorDeliveryFromCode(null);
        }

        if (pickUpDelivery.getDoorDeliveryPlace() != null) {
            ValidateUtil.notNull(pickUpDelivery.getDoorDeliveryPlace(), ErrorCode.SHIPMENT_SERVICE_DOOR_DELIVERY_PLACE);
            ValidateUtil.validateLength(pickUpDelivery.getDoorDeliveryPlace(), 1, 500, ErrorCode.SHIPMENT_SERVICE_DOOR_DELIVERY_PLACE);
        } else {
            pickUpDelivery.setDoorDeliveryPlace(null);
        }

        if (pickUpDelivery.getDoorDeliveryContactPerson() != null && pickUpDelivery.getDoorDeliveryContactPerson().trim().length() != 0) {
            ValidateUtil.notNullOrEmpty(pickUpDelivery.getDoorDeliveryContactPerson(), ErrorCode.SHIPMENT_SERVICE_DOOR_DELIVERY_CONTACT_PERSON);
            ValidateUtil.validateLength(pickUpDelivery.getDoorDeliveryContactPerson(), 1, 100, ErrorCode.SHIPMENT_SERVICE_DOOR_DELIVERY_CONTACT_PERSON);
        } else {
            pickUpDelivery.setDoorDeliveryContactPerson(null);
        }

        if (pickUpDelivery.getDoorDeliveryMobileNo() != null && pickUpDelivery.getDoorDeliveryMobileNo().trim().length() != 0) {
            ValidateUtil.notNullOrEmpty(pickUpDelivery.getDoorDeliveryMobileNo(), ErrorCode.SHIPMENT_SERVICE_DOOR_DELIVERY_MOBILE);
            ValidateUtil.validateLength(pickUpDelivery.getDoorDeliveryMobileNo(), 1, 100, ErrorCode.SHIPMENT_SERVICE_DOOR_DELIVERY_MOBILE);
        } else {
            pickUpDelivery.setDoorDeliveryMobileNo(null);
        }

        if (pickUpDelivery.getDoorDeliveryPhoneNo() != null && pickUpDelivery.getDoorDeliveryPhoneNo().trim().length() != 0) {
            ValidateUtil.notNullOrEmpty(pickUpDelivery.getDoorDeliveryPhoneNo(), ErrorCode.SHIPMENT_SERVICE_DOOR_DELIVERY_PHONE);
            ValidateUtil.validateLength(pickUpDelivery.getDoorDeliveryPhoneNo(), 1, 100, ErrorCode.SHIPMENT_SERVICE_DOOR_DELIVERY_PHONE);
        } else {
            pickUpDelivery.setDoorDeliveryPhoneNo(null);
        }

        if (pickUpDelivery.getDoorDeliveryEmail() != null && pickUpDelivery.getDoorDeliveryEmail().trim().length() != 0) {
            ValidateUtil.notNullOrEmpty(pickUpDelivery.getDoorDeliveryEmail(), ErrorCode.SHIPMENT_SERVICE_DOOR_DELIVERY_EMAIL_LENGTH);
            ValidateUtil.validateLength(pickUpDelivery.getDoorDeliveryEmail(), 1, 500, ErrorCode.SHIPMENT_SERVICE_DOOR_DELIVERY_EMAIL_LENGTH);
		 			/*for(String email : pickUpDelivery.getDoorDeliveryEmail().split(defaultMasterDataRepository.findByCode("single.text.split.by.multiple.email").getValue())){
		 				if(!Pattern.compile(ValidateUtil.regularExp).matcher(email).matches()){
		 					throw new RestException(ErrorCode.SHIPMENT_SERVICE_DOOR_DELIVERY_EMAIL);
		 				}
		 			}*/
        } else {
            pickUpDelivery.setDoorDeliveryEmail(null);
        }
    }

    private void verifyDelivery(PickUpDeliveryPoint pickUpDelivery) {

        if (pickUpDelivery.getDeliveryPoint() != null && pickUpDelivery.getDeliveryPoint().getId() != null) {
            ValidateUtil.notNull(pickUpDelivery.getDeliveryPoint(), ErrorCode.SHIPMENT_SERVICE_DELIVERY_POINT);
            ValidateUtil.notNull(pickUpDelivery.getDeliveryPoint().getId(), ErrorCode.SHIPMENT_SERVICE_DELIVERY_POINT);

            CFSMaster cfs = cfsMasterRepository.getOne(pickUpDelivery.getDeliveryPoint().getId());
            ValidateUtil.notNullOrEmpty(cfs.getCfsCode(), ErrorCode.SHIPMENT_SERVICE_DELIVERY_POINT);

            ValidateUtil.isStatusBlocked(cfs.getStatus(), ErrorCode.SHIPMENT_SERVICE_DELIVERY_POINT_BLOCK);
            ValidateUtil.isStatusHidden(cfs.getStatus(), ErrorCode.SHIPMENT_SERVICE_DELIVERY_POINT_HIDE);

            pickUpDelivery.setDeliveryPointCode(cfs.getCfsCode());
        } else {
            pickUpDelivery.setDeliveryPoint(null);
            pickUpDelivery.setDeliveryPointCode(null);
        }

        if (pickUpDelivery.getDeliveryFrom() != null && pickUpDelivery.getDeliveryFrom().getId() != null) {
            ValidateUtil.notNull(pickUpDelivery.getDeliveryFrom(), ErrorCode.SHIPMENT_SERVICE_DELIVERY_FROM);
            ValidateUtil.notNull(pickUpDelivery.getDeliveryFrom().getId(), ErrorCode.SHIPMENT_SERVICE_DELIVERY_FROM);
            PortMaster deliveryFrom = portMasterRepository.findById(pickUpDelivery.getDeliveryFrom().getId());

            ValidateUtil.notNullOrEmpty(deliveryFrom.getPortCode(), ErrorCode.SHIPMENT_SERVICE_DELIVERY_FROM);

            ValidateUtil.isStatusBlocked(deliveryFrom.getStatus(), ErrorCode.SHIPMENT_SERVICE_DELIVERY_FROM_BLOCK);
            ValidateUtil.isStatusHidden(deliveryFrom.getStatus(), ErrorCode.SHIPMENT_SERVICE_DELIVERY_FROM_HIDE);

            pickUpDelivery.setDeliveryFromCode(deliveryFrom.getPortCode());
        } else {
            pickUpDelivery.setDeliveryFrom(null);
            pickUpDelivery.setDeliveryFromCode(null);
        }

        if (pickUpDelivery.getDeliveryPlace() != null) {
            ValidateUtil.notNull(pickUpDelivery.getDeliveryPlace(), ErrorCode.SHIPMENT_SERVICE_DELIVERY_PLACE);
            ValidateUtil.validateLength(pickUpDelivery.getDeliveryPlace(), 1, 500, ErrorCode.SHIPMENT_SERVICE_DELIVERY_PLACE);
        } else {
            pickUpDelivery.setDeliveryPlace(null);
        }

        if (pickUpDelivery.getDeliveryContactPerson() != null && pickUpDelivery.getDeliveryContactPerson().trim().length() != 0) {
            ValidateUtil.notNullOrEmpty(pickUpDelivery.getDeliveryContactPerson(), ErrorCode.SHIPMENT_SERVICE_DELIVERY_CONTACT_PERSON);
            ValidateUtil.validateLength(pickUpDelivery.getDeliveryContactPerson(), 1, 100, ErrorCode.SHIPMENT_SERVICE_DELIVERY_CONTACT_PERSON);
        } else {
            pickUpDelivery.setDeliveryContactPerson(null);
        }

        if (pickUpDelivery.getDeliveryMobileNo() != null && pickUpDelivery.getDeliveryMobileNo().trim().length() != 0) {
            ValidateUtil.notNullOrEmpty(pickUpDelivery.getDeliveryMobileNo(), ErrorCode.SHIPMENT_SERVICE_DELIVERY_MOBILE);
            ValidateUtil.validateLength(pickUpDelivery.getDeliveryMobileNo(), 1, 100, ErrorCode.SHIPMENT_SERVICE_DELIVERY_MOBILE);
        } else {
            pickUpDelivery.setDeliveryMobileNo(null);
        }

        if (pickUpDelivery.getDeliveryPhoneNo() != null && pickUpDelivery.getDeliveryPhoneNo().trim().length() != 0) {
            ValidateUtil.notNullOrEmpty(pickUpDelivery.getDeliveryPhoneNo(), ErrorCode.SHIPMENT_SERVICE_DELIVERY_PHONE);
            ValidateUtil.validateLength(pickUpDelivery.getDeliveryPhoneNo(), 1, 100, ErrorCode.SHIPMENT_SERVICE_DELIVERY_PHONE);
        } else {
            pickUpDelivery.setDeliveryPhoneNo(null);
        }

        if (pickUpDelivery.getDeliveryEmail() != null && pickUpDelivery.getDeliveryEmail().trim().length() != 0) {
            ValidateUtil.notNullOrEmpty(pickUpDelivery.getDeliveryEmail(), ErrorCode.SHIPMENT_SERVICE_DELIVERY_EMAIL_LENGTH);
            ValidateUtil.validateLength(pickUpDelivery.getDeliveryEmail(), 1, 500, ErrorCode.SHIPMENT_SERVICE_DELIVERY_EMAIL_LENGTH);
		 			/*for(String email : pickUpDelivery.getDeliveryEmail().split(defaultMasterDataRepository.findByCode("single.text.split.by.multiple.email").getValue())){
		 				if(!Pattern.compile(ValidateUtil.regularExp).matcher(email).matches()){
		 					throw new RestException(ErrorCode.SHIPMENT_SERVICE_DELIVERY_EMAIL);
		 				}
		 			}
		 			regExpService.validate(pickUpDelivery.getDeliveryEmail(), RegExpName.REG_EXP_MULTIPLE_EMAIL, ErrorCode.SHIPMENT_SERVICE_DELIVERY_EMAIL);*/
        } else {
            pickUpDelivery.setDeliveryEmail(null);
        }
    }

    private void verifyPickUp(PickUpDeliveryPoint pickUpDelivery) {

        if (pickUpDelivery.getPickupPoint() != null && pickUpDelivery.getPickupPoint().getId() != null) {

            PartyMaster tmpParty = cacheRepository.get(SaaSUtil.getSaaSId(), PartyMaster.class.getName(), pickUpDelivery.getPickupPoint().getId(), partyMasterRepository);

            ValidateUtil.notNull(tmpParty, ErrorCode.SHIPMENT_SERVICE_PICKUP_POINT);
            ValidateUtil.notNull(tmpParty.getId(), ErrorCode.SHIPMENT_SERVICE_PICKUP_POINT);
            ValidateUtil.notNullOrEmpty(tmpParty.getPartyCode(), ErrorCode.SHIPMENT_SERVICE_PICKUP_POINT);

            ValidateUtil.isDefaulter(tmpParty, ErrorCode.SHIPMENT_TRANS_PICKUP_DEFAULTER);

            ValidateUtil.isStatusBlocked(tmpParty.getStatus(), ErrorCode.SHIPMENT_SERVICE_PICKUP_POINT_BLOCK);
            ValidateUtil.isStatusHidden(tmpParty.getStatus(), ErrorCode.SHIPMENT_SERVICE_PICKUP_POINT_HIDE);

            pickUpDelivery.setPickupPointCode(tmpParty.getPartyCode());
        } else {
            pickUpDelivery.setPickupPoint(null);
            pickUpDelivery.setPickupPointCode(null);
        }

        if (pickUpDelivery.getPickupFrom() != null && pickUpDelivery.getPickupFrom().getId() != null) {
            ValidateUtil.notNull(pickUpDelivery.getPickupFrom(), ErrorCode.SHIPMENT_SERVICE_PICKUP_FROM);
            ValidateUtil.notNull(pickUpDelivery.getPickupFrom().getId(), ErrorCode.SHIPMENT_SERVICE_PICKUP_FROM);

            PortMaster pickUpFrom = portMasterRepository.findById(pickUpDelivery.getPickupFrom().getId());

            ValidateUtil.notNullOrEmpty(pickUpFrom.getPortCode(), ErrorCode.SHIPMENT_SERVICE_PICKUP_FROM);

            ValidateUtil.isStatusBlocked(pickUpFrom.getStatus(), ErrorCode.SHIPMENT_SERVICE_PICKUP_FROM_BLOCK);
            ValidateUtil.isStatusHidden(pickUpFrom.getStatus(), ErrorCode.SHIPMENT_SERVICE_PICKUP_FROM_HIDE);

            pickUpDelivery.setPickupFromCode(pickUpFrom.getPortCode());
        } else {
            pickUpDelivery.setPickupFrom(null);
            pickUpDelivery.setPickupFromCode(null);
        }

        if (pickUpDelivery.getPickUpPlace() != null) {
            ValidateUtil.notNull(pickUpDelivery.getPickUpPlace(), ErrorCode.SHIPMENT_SERVICE_PICKUP_PLACE);
            ValidateUtil.validateLength(pickUpDelivery.getPickUpPlace(), 1, 500, ErrorCode.SHIPMENT_SERVICE_PICKUP_PLACE);
        } else {
            pickUpDelivery.setPickUpPlace(null);
        }

        if (pickUpDelivery.getPickUpContactPerson() != null && pickUpDelivery.getPickUpContactPerson().trim().length() != 0) {
            ValidateUtil.notNullOrEmpty(pickUpDelivery.getPickUpContactPerson(), ErrorCode.SHIPMENT_SERVICE_PICKUP_CONTACT_PERSON);
            ValidateUtil.validateLength(pickUpDelivery.getPickUpContactPerson(), 1, 100, ErrorCode.SHIPMENT_SERVICE_PICKUP_CONTACT_PERSON);
        } else {
            pickUpDelivery.setPickUpContactPerson(null);
        }

        if (pickUpDelivery.getPickUpMobileNo() != null && pickUpDelivery.getPickUpMobileNo().trim().length() != 0) {
            ValidateUtil.notNullOrEmpty(pickUpDelivery.getPickUpMobileNo(), ErrorCode.SHIPMENT_SERVICE_PICKUP_MOBILE);
            ValidateUtil.validateLength(pickUpDelivery.getPickUpMobileNo(), 1, 100, ErrorCode.SHIPMENT_SERVICE_PICKUP_MOBILE);
        } else {
            pickUpDelivery.setPickUpMobileNo(null);
        }

        if (pickUpDelivery.getPickUpPhoneNo() != null && pickUpDelivery.getPickUpPhoneNo().trim().length() != 0) {
            ValidateUtil.notNullOrEmpty(pickUpDelivery.getPickUpPhoneNo(), ErrorCode.SHIPMENT_SERVICE_PICKUP_PHONE);
            ValidateUtil.validateLength(pickUpDelivery.getPickUpPhoneNo(), 1, 100, ErrorCode.SHIPMENT_SERVICE_PICKUP_PHONE);
        } else {
            pickUpDelivery.setPickUpPhoneNo(null);
        }

        if (pickUpDelivery.getPickUpEmail() != null && pickUpDelivery.getPickUpEmail().trim().length() != 0) {
            ValidateUtil.notNullOrEmpty(pickUpDelivery.getPickUpEmail(), ErrorCode.SHIPMENT_SERVICE_PICKUP_EMAIL_LENGTH);
            ValidateUtil.validateLength(pickUpDelivery.getPickUpEmail(), 1, 500, ErrorCode.SHIPMENT_SERVICE_PICKUP_EMAIL_LENGTH);
		 			/*for(String email : pickUpDelivery.getPickUpEmail().split(defaultMasterDataRepository.findByCode("single.text.split.by.multiple.email").getValue())){
		 				if(!Pattern.compile(ValidateUtil.regularExp).matcher(email).matches()){
		 					throw new RestException(ErrorCode.SHIPMENT_SERVICE_PICKUP_EMAIL);
		 				}
		 			}*/
        } else {
            pickUpDelivery.setPickUpEmail(null);
        }
    }

    public void isShipmentCreatedFromQuotation(String quotationNo) {
        log.info("inside isShipmentCreatedFromQuotation");
        ShipmentServiceDetail service = shipmentServiceDetailRepository.findByQuotationNo(quotationNo);
        if (service != null) {
            log.error("Shipment is already created from this quotation");
            throw new RestException(ErrorCode.SHIPMENT_ALREADY_CREATED_FROM_QUOTATION);
        }

    }

}
