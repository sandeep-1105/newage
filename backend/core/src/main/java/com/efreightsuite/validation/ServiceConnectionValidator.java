package com.efreightsuite.validation;

import java.util.Objects;

import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.model.CarrierMaster;
import com.efreightsuite.model.PortMaster;
import com.efreightsuite.model.ServiceConnection;
import com.efreightsuite.repository.CarrierMasterRepository;
import com.efreightsuite.repository.PortMasterRepository;
import com.efreightsuite.util.ValidateUtil;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class ServiceConnectionValidator {



    @Autowired
    private PortMasterRepository portMasterRepository;

    @Autowired
    private CarrierMasterRepository carrierMasterRepository;

    public void validate(ServiceConnection sc) {

        verifyMove(sc);

        verifyStatus(sc);

        verifyPol(sc);

        verifyPod(sc);

        verifyEtd(sc);

        verifyEta(sc);

        verifyCarrierVessel(sc);

        verifyFlightOrVoyageNo(sc);
    }


    private void verifyMove(ServiceConnection sc) {

        ValidateUtil.notNull(sc.getMove(), ErrorCode.SHIPMENT_MOVE_NOTNULL);

    }

    private void verifyStatus(ServiceConnection sc) {

        ValidateUtil.notNull(sc.getConnectionStatus(), ErrorCode.SHIPMENT_CONNECTION_STATUS);

    }

    private void verifyPol(ServiceConnection sc) {
        ValidateUtil.notNull(sc.getPol(), ErrorCode.SHIPMENT_CONNECTION_POL_MANDATORY);
        ValidateUtil.notNull(sc.getPol().getId(), ErrorCode.SHIPMENT_CONNECTION_POL_MANDATORY);

        PortMaster pol = portMasterRepository.findById(sc.getPol().getId());

        ValidateUtil.notNullOrEmpty(pol.getPortCode(), ErrorCode.SHIPMENT_CONNECTION_POL_MANDATORY);

        ValidateUtil.isStatusBlocked(pol.getStatus(), ErrorCode.SHIPMENT_CONNECTION_POL_BLOCKED);
        ValidateUtil.isStatusHidden(pol.getStatus(), ErrorCode.SHIPMENT_CONNECTION_POL_HIDDEN);

        if (sc.getMove() != pol.getTransportMode()) {
            throw new RestException(ErrorCode.SHIPMENT_SERVICE_POL_MODE);
        }

        sc.setPolCode(pol.getPortCode());
    }

    private void verifyPod(ServiceConnection sc) {
        ValidateUtil.notNull(sc.getPod(), ErrorCode.SHIPMENT_CONNECTION_POD_MANDATORY);
        ValidateUtil.notNull(sc.getPod().getId(), ErrorCode.SHIPMENT_CONNECTION_POD_MANDATORY);

        PortMaster pod = portMasterRepository.findById(sc.getPod().getId());
        PortMaster pol = portMasterRepository.findById(sc.getPol().getId());


        ValidateUtil.notNullOrEmpty(pod.getPortCode(), ErrorCode.SHIPMENT_CONNECTION_POD_MANDATORY);

        ValidateUtil.isStatusBlocked(pod.getStatus(), ErrorCode.SHIPMENT_CONNECTION_POD_BLOCKED);
        ValidateUtil.isStatusHidden(pod.getStatus(), ErrorCode.SHIPMENT_CONNECTION_POD_HIDDEN);

        if (sc.getMove() != pod.getTransportMode()) {
            throw new RestException(ErrorCode.SHIPMENT_SERVICE_POD_MODE);
        }

        if (Objects.equals(sc.getPod().getId(), sc.getPol().getId())) {
            throw new RestException(ErrorCode.SHIPMENT_CONNECTION_ORIGIN_DESTINATION_EQUAL);
        }

        if (pod.getPortCode().equals(pol.getPortCode())) {
            throw new RestException(ErrorCode.SHIPMENT_CONNECTION_POD_POL_EQUAL);
        }

        sc.setPodCode(pod.getPortCode());

    }

    private void verifyEtd(ServiceConnection sc) {
        ValidateUtil.notNull(sc.getEtd(), ErrorCode.SHIPMENT_CONNECTION_ETD_MANDATORY);
    }

    private void verifyEta(ServiceConnection sc) {
        ValidateUtil.notNull(sc.getEta(), ErrorCode.SHIPMENT_CONNECTION_ETA_MANDATORY);

        if (sc.getEtd().after(sc.getEta())) {
            throw new RestException(ErrorCode.SHIPMENT_CONNECTION_ETD_ETA_GREATER);
        }

    }

    private void verifyCarrierVessel(ServiceConnection sc) {
        ValidateUtil.notNull(sc.getCarrierMaster(), ErrorCode.SHIPMENT_CONNECTION_CARRIER_MANDATORY);
        ValidateUtil.notNull(sc.getCarrierMaster().getId(), ErrorCode.SHIPMENT_CONNECTION_CARRIER_MANDATORY);

        CarrierMaster carrier = carrierMasterRepository.findById(sc.getCarrierMaster().getId());

        ValidateUtil.notNullOrEmpty(carrier.getCarrierCode(), ErrorCode.SHIPMENT_CONNECTION_CARRIER_MANDATORY);
        ValidateUtil.isStatusBlocked(carrier.getStatus(), ErrorCode.SHIPMENT_CONNECTION_CARRIER_BLOCKED);
        ValidateUtil.isStatusHidden(carrier.getStatus(), ErrorCode.SHIPMENT_CONNECTION_CARRIER_HIDDEN);
        sc.setCarrierCode(carrier.getCarrierCode());
    }

    private void verifyFlightOrVoyageNo(ServiceConnection sc) {

        ValidateUtil.notNull(sc.getFlightVoyageNo(), ErrorCode.SHIPMENT_FLIGHTVOYAGE_NOT_NULL);
        ValidateUtil.validateLength(sc.getFlightVoyageNo(), 1, 10, ErrorCode.SHIPMENT_FLIGHTVOYAGE_NO_INVALID);

    }


}
