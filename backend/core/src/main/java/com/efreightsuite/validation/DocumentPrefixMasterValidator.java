package com.efreightsuite.validation;

import com.efreightsuite.enumeration.BooleanType;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.model.DocumentPrefixMaster;
import com.efreightsuite.service.RegularExpressionService;
import com.efreightsuite.util.RegExpName;
import com.efreightsuite.util.ValidateUtil;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class DocumentPrefixMasterValidator {

    @Autowired
    private
    RegularExpressionService regExpService;

    public void validate(DocumentPrefixMaster documentPrefixMaster) {

        ValidateUtil.notNull(documentPrefixMaster, ErrorCode.DOCUMENT_PREFIXM_NOT_NULL);

        ValidateUtil.notNullOrEmpty(documentPrefixMaster.getDocumentCode(), ErrorCode.DOCUMENT_PREFIXM_CODE_NOT_NULL_OR_EMPTY);

        ValidateUtil.validateLength(documentPrefixMaster.getDocumentCode(), 1, 10, ErrorCode.DOCUMENT_PREFIXM_CODE_LENGTH_EXCEED);

        regExpService.validate(documentPrefixMaster.getDocumentCode(), RegExpName.Reg_Exp_Master_DOCUMENT_PREFIX_CODE, ErrorCode.DOCUMENT_PREFIXM_CODE_INVALID);


        ValidateUtil.notNullOrEmpty(documentPrefixMaster.getDocumentName(), ErrorCode.DOCUMENT_PREFIXM_NAME_NOT_NULL_OR_EMPTY);

        ValidateUtil.validateLength(documentPrefixMaster.getDocumentName(), 1, 100, ErrorCode.DOCUMENT_PREFIXM_NAME__LENGTH_EXCEED);

        regExpService.validate(documentPrefixMaster.getDocumentName(), RegExpName.Reg_Exp_Master_DOCUMENT_PREFIX_NAME, ErrorCode.DOCUMENT_PREFIXM_NAME_INVALID);


        if (documentPrefixMaster.getDocSuffix() == null
                && documentPrefixMaster.getAddFDC() == null
                && documentPrefixMaster.getAddPOR() == null) {
            throw new RestException(ErrorCode.DOCUMENT_PREFIX_NON_SELECT);
        }

        if (documentPrefixMaster.getDocSuffix() == null && documentPrefixMaster.getAddFDC() == null && documentPrefixMaster.getAddPOR() == null) {

            ValidateUtil.notNullOrEmpty(documentPrefixMaster.getDocPrefix(), ErrorCode.DOCUMENT_PREFIX_NOT_NULL);

            ValidateUtil.validateLength(documentPrefixMaster.getDocPrefix(), 1, 20, ErrorCode.DOCUMENT_PREFIX_NAME__LENGTH_EXCEED);

            //regExpService.validate(documentPrefixMaster.getDocPrefix(), RegExpName.Reg_Exp_Master_DOCUMENT_PREFIX_PREFIX_VALUE, ErrorCode.DOCUMENT_PREFIX__INVALID);
        }

        if (documentPrefixMaster.getDocPrefix() == null && documentPrefixMaster.getAddFDC() == null && documentPrefixMaster.getAddPOR() == null) {

            ValidateUtil.notNullOrEmpty(documentPrefixMaster.getDocSuffix(), ErrorCode.DOCUMENT_SUFFIX_NOT_NULL);

            ValidateUtil.validateLength(documentPrefixMaster.getDocSuffix(), 1, 20, ErrorCode.DOCUMENT_SUFIX_NAME__LENGTH_EXCEED);

            //regExpService.validate(documentPrefixMaster.getDocSuffix(), RegExpName.Reg_Exp_Master_DOCUMENT_PREFIX_SUFFIX_VALUE, ErrorCode.DOCUMENT_SUFFIX_INVALID);

        }

        if (documentPrefixMaster.getDocPrefix() == null && documentPrefixMaster.getDocSuffix() == null && documentPrefixMaster.getAddFDC() == null) {
            ValidateUtil.validateEnum(BooleanType.class, documentPrefixMaster.getAddPOR(), ErrorCode.DOCUMENT_PREFIXM_ADDPOR_NOT_NULL);
        }

        if (documentPrefixMaster.getDocPrefix() == null && documentPrefixMaster.getDocSuffix() == null && documentPrefixMaster.getAddPOR() == null) {
            ValidateUtil.validateEnum(BooleanType.class, documentPrefixMaster.getAddFDC(), ErrorCode.DOCUMENT_PREFIXM_ADDFDC_NOT_NULL);
        }

        if (documentPrefixMaster.getFormula() != null) {
            ValidateUtil.notNullOrEmpty(documentPrefixMaster.getFormula(), ErrorCode.DOCUMENT_FORMULAE_NOT_NULL);
        }

    }

}
