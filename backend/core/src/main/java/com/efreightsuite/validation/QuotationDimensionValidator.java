package com.efreightsuite.validation;

import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.model.QuotationDimension;
import com.efreightsuite.util.ValidateUtil;
import lombok.extern.log4j.Log4j2;

import org.springframework.stereotype.Service;

@Service
@Log4j2
public class QuotationDimensionValidator {
    public void validate(QuotationDimension quotationDimension) {

        log.info("QuotationDimension Validation Started......");
        verifyPieces(quotationDimension);
        verifyLength(quotationDimension);
        verifyWidth(quotationDimension);
        verifyHeight(quotationDimension);
        verifyGrossWeight(quotationDimension);
        log.info("QuotationDimension Validation Finished......");
    }

    private void verifyPieces(QuotationDimension quotationDimension) {

        ValidateUtil.notNull(quotationDimension.getNoOfPiece(), ErrorCode.QUOTATION_DIMENSION_PIECES_REQUIRED);

        ValidateUtil.belowRange(quotationDimension.getNoOfPiece(), 0d, ErrorCode.QUOTATION_DIMENSION_PIECES_NEGATIVE);

        ValidateUtil.aboveRange(quotationDimension.getNoOfPiece(), 1000000000d, ErrorCode.QUOTATION_DIMENSION_PIECES_NEGATIVE);

        log.info("Dimension No. Of Pieces Validated...............[" + quotationDimension.getNoOfPiece() + "]");
    }

    private void verifyLength(QuotationDimension quotationDimension) {

        ValidateUtil.notNull(quotationDimension.getLength(), ErrorCode.QUOTATION_DIMENSION_LENGTH_REQUIRED);

        ValidateUtil.belowRange(quotationDimension.getLength(), 0d, ErrorCode.QUOTATION_DIMENSION_LENGTH_NEGATIVE);

        ValidateUtil.aboveRange(quotationDimension.getLength(), 100000d, ErrorCode.QUOTATION_DIMENSION_LENGTH_NEGATIVE);

        log.info("Dimension Length Validated...............[" + quotationDimension.getLength() + "]");
    }

    private void verifyWidth(QuotationDimension quotationDimension) {

        ValidateUtil.notNull(quotationDimension.getWidth(), ErrorCode.QUOTATION_DIMENSION_WIDTH_REQUIRED);

        ValidateUtil.belowRange(quotationDimension.getWidth(), 0d, ErrorCode.QUOTATION_DIMENSION_WIDTH_NEGATIVE);

        ValidateUtil.aboveRange(quotationDimension.getWidth(), 100000d, ErrorCode.QUOTATION_DIMENSION_WIDTH_NEGATIVE);

        log.info("Dimension Width Validated...............[" + quotationDimension.getWidth() + "]");
    }

    private void verifyHeight(QuotationDimension quotationDimension) {

        ValidateUtil.notNull(quotationDimension.getHeight(), ErrorCode.QUOTATION_DIMENSION_HEIGHT_REQUIRED);

        ValidateUtil.belowRange(quotationDimension.getHeight(), 0, ErrorCode.QUOTATION_DIMENSION_HEIGHT_NEGATIVE);

        ValidateUtil.aboveRange(quotationDimension.getHeight(), 100000d, ErrorCode.QUOTATION_DIMENSION_HEIGHT_NEGATIVE);

        log.info("Dimension Height Validated...............[" + quotationDimension.getHeight() + "]");
    }

    private void verifyGrossWeight(QuotationDimension quotationDimension) {

        ValidateUtil.notNull(quotationDimension.getGrossWeight(), ErrorCode.QUOTATION_DIMENSION_GROSS_WEIGHT_REQUIRED);

        ValidateUtil.belowRange(quotationDimension.getGrossWeight(), 0, ErrorCode.QUOTATION_DIMENSION_GROSS_WEIGHT_NEGATIVE);

        ValidateUtil.aboveRange(quotationDimension.getGrossWeight(), 1000000000d, ErrorCode.QUOTATION_DIMENSION_GROSS_WEIGHT_NEGATIVE);

        log.info("Dimension GrossWeight Validated ...............[" + quotationDimension.getGrossWeight() + "]");
    }


}
