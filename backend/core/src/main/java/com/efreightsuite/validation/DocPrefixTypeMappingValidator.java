package com.efreightsuite.validation;

import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.model.DocPrefixTypeMapping;
import com.efreightsuite.util.ValidateUtil;
import lombok.extern.log4j.Log4j2;

import org.springframework.stereotype.Service;


@Service
@Log4j2
public class DocPrefixTypeMappingValidator {


    public void validate(DocPrefixTypeMapping docPrefixTypeMapping) {

        ValidateUtil.notNull(docPrefixTypeMapping, ErrorCode.DOCUMENT_PREFIX_MAPPING_NOT_NULL);

        log.info("Document Prefix Master Validation Begins.........[" + (docPrefixTypeMapping.getId() != null ? docPrefixTypeMapping.getId() : "") + "]");

// Validate logo

        ValidateUtil.notNull(docPrefixTypeMapping.getLogo(), ErrorCode.DOCUMENT_PREFIXM_LOGO_NOT_NULL_OR_EMPTY);


// Validate locationcode

        ValidateUtil.notNullOrEmpty(docPrefixTypeMapping.getLocationMaster().getLocationCode(), ErrorCode.LOCATION_CODE_NOT_NULL_OR_EMPTY);

        ValidateUtil.isStatusBlocked(docPrefixTypeMapping.getLocationMaster().getStatus(), ErrorCode.LOCATION_BLOCK);

        ValidateUtil.isStatusHidden(docPrefixTypeMapping.getLocationMaster().getStatus(), ErrorCode.LOCATION_HIDE);


// Validate doctype

        ValidateUtil.notNull(docPrefixTypeMapping.getDocType(), ErrorCode.DOCUMENT_PREFIXM_DOCTYPE_NOT_NULL_OR_EMPTY);

// Validate ReportName

        ValidateUtil.notNullOrEmpty(docPrefixTypeMapping.getReportName(), ErrorCode.DOCUMENT_PREFIXM_REPORT_NOT_NULL_OR_EMPTY);


        ValidateUtil.validateLength(docPrefixTypeMapping.getReportName(), 1, 100, ErrorCode.DOCUMENT_PREFIXM_REPORT_LENGTH_INVALID);

        ValidateUtil.checkPattern(docPrefixTypeMapping.getReportName(), "documentPrefixReportName", true);


    }
}
