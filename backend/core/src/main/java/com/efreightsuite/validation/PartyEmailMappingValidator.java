package com.efreightsuite.validation;

import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.model.PartyEmailMapping;
import com.efreightsuite.model.PartyEmailMessageMapping;
import com.efreightsuite.util.ValidateUtil;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.collections4.CollectionUtils;

import org.springframework.stereotype.Service;

@Service
@Log4j2
public class PartyEmailMappingValidator {

    public void validate(PartyEmailMapping partyEmailMapping) {

        ValidateUtil.notNull(partyEmailMapping, ErrorCode.PARTYEMAILMAPPING_NOT_NULL);

        validateEmail(partyEmailMapping.getEmail());
        validateLocationMaster(partyEmailMapping);
        validateoriginCountry(partyEmailMapping);
        validateDestinationCountry(partyEmailMapping);

        if (partyEmailMapping.getCompanyMaster() != null) {
            partyEmailMapping.setCompanyCode(partyEmailMapping.getCompanyMaster().getCompanyCode());
        } else {
            partyEmailMapping.setCompanyCode(null);
        }

        if (partyEmailMapping.getCountryMaster() != null) {
            partyEmailMapping.setCountryCode(partyEmailMapping.getCountryMaster().getCountryCode());
        } else {
            partyEmailMapping.setCountryCode(null);
        }

        if (CollectionUtils.isNotEmpty(partyEmailMapping.getPartyEmailMessageMappingList())) {

            for (PartyEmailMessageMapping tmp : partyEmailMapping.getPartyEmailMessageMappingList()) {
                if (tmp.getAutoMailMaster() != null) {
                    tmp.setAutoMailMasterCode(tmp.getAutoMailMaster().getMessageCode());
                } else {
                    tmp.setAutoMailMasterCode(null);
                }

                if (tmp.getAutoMailGroupMaster() != null) {
                    tmp.setAutoMailGroupMasterCode(tmp.getAutoMailGroupMaster().getMessageGroupCode());
                } else {
                    tmp.setAutoMailGroupMasterCode(null);
                }
            }

        }
    }

    private void validateEmail(String email) {
        ValidateUtil.notNullOrEmpty(email, ErrorCode.PARTYEMAILMAPPING_EMAIL_NOT_NULL);
    }


    private void validateDestinationCountry(PartyEmailMapping partyEmailMapping) {

        if (partyEmailMapping.getDestinationCountry() != null && partyEmailMapping.getDestinationCountry().getId() != null) {
            ValidateUtil.isStatusBlocked(partyEmailMapping.getDestinationCountry().getStatus(), ErrorCode.PARTY_EMAIL_DESTINATION_COUNTRY_BLOCKED);
            ValidateUtil.isStatusHidden(partyEmailMapping.getDestinationCountry().getStatus(), ErrorCode.PARTY_EMAIL_DESTINATION_COUNTRY_HIDDEN);
            partyEmailMapping.setDestinationCountryCode(partyEmailMapping.getDestinationCountry().getCountryCode());
        } else {
            partyEmailMapping.setDestinationCountry(null);
            partyEmailMapping.setDestinationCountryCode(null);
        }

    }

    private void validateoriginCountry(PartyEmailMapping partyEmailMapping) {

        if (partyEmailMapping.getOriginCountry() != null && partyEmailMapping.getOriginCountry().getId() != null) {
            ValidateUtil.isStatusBlocked(partyEmailMapping.getOriginCountry().getStatus(), ErrorCode.PARTY_EMAIL_ORIGIN_COUNTRY_BLOCKED);
            ValidateUtil.isStatusHidden(partyEmailMapping.getOriginCountry().getStatus(), ErrorCode.PARTY_EMAIL_ORIGIN_COUNTRY_HIDDEN);
            partyEmailMapping.setOriginCountryCode(partyEmailMapping.getOriginCountry().getCountryCode());
        } else {
            partyEmailMapping.setOriginCountry(null);
            partyEmailMapping.setOriginCountryCode(null);
        }

    }


    private void validateLocationMaster(PartyEmailMapping partyEmailMapping) {

        ValidateUtil.notNull(partyEmailMapping.getLocationMaster(), ErrorCode.LOCATION_ID_NOT_NULL);
        ValidateUtil.notNull(partyEmailMapping.getLocationMaster().getId(), ErrorCode.LOCATION_ID_NOT_NULL);
        ValidateUtil.isStatusBlocked(partyEmailMapping.getLocationMaster().getStatus(), ErrorCode.PARTY_EMAIL_LOCATION_BLOCKED);
        ValidateUtil.isStatusHidden(partyEmailMapping.getLocationMaster().getStatus(), ErrorCode.PARTY_EMAIL_LOCATION_HIDDEN);
        partyEmailMapping.setLocationCode(partyEmailMapping.getLocationMaster().getLocationCode());
    }


}
