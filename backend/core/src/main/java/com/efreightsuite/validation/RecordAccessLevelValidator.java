package com.efreightsuite.validation;

import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.model.RecordAccessLevel;
import com.efreightsuite.model.RecordAccessLevelCountry;
import com.efreightsuite.model.RecordAccessLevelCustomer;
import com.efreightsuite.model.RecordAccessLevelDivision;
import com.efreightsuite.model.RecordAccessLevelLocation;
import com.efreightsuite.model.RecordAccessLevelSalesman;
import com.efreightsuite.model.RecordAccessLevelService;
import com.efreightsuite.util.ValidateUtil;

import org.springframework.stereotype.Service;

@Service
public class RecordAccessLevelValidator {

    public void validate(RecordAccessLevel recordAccessLevel) {

        ValidateUtil.notNullOrEmpty(recordAccessLevel.getRecordAccessLevelName(), ErrorCode.RAL_NAME_EMPTY);

        validateCountryList(recordAccessLevel);
        validateLocationList(recordAccessLevel);
        validateDivisionList(recordAccessLevel);
        validateServiceList(recordAccessLevel);
        validateSalesmanList(recordAccessLevel);
        validateCustomerList(recordAccessLevel);

    }

    private void validateCountryList(RecordAccessLevel recordAccessLevel) {

        if (recordAccessLevel.getCountryList() != null && !recordAccessLevel.getCountryList().isEmpty()) {
            for (RecordAccessLevelCountry country : recordAccessLevel.getCountryList()) {
                if (country.getCountryMaster() != null && country.getCountryMaster().getId() != null) {
                    ValidateUtil.isStatusBlocked(country.getCountryMaster().getStatus(), ErrorCode.RAL_COUNTRY_BLOCKED);
                }
            }
        }
    }

    private void validateLocationList(RecordAccessLevel recordAccessLevel) {

        if (recordAccessLevel.getLocationList() != null && !recordAccessLevel.getLocationList().isEmpty()) {
            for (RecordAccessLevelLocation location : recordAccessLevel.getLocationList()) {
                if (location.getLocationMaster() != null && location.getLocationMaster().getId() != null) {
                    ValidateUtil.isStatusBlocked(location.getLocationMaster().getStatus(), ErrorCode.RAL_LOCATION_BLOCKED);
                }
            }
        }
    }

    private void validateDivisionList(RecordAccessLevel recordAccessLevel) {

        if (recordAccessLevel.getDivisionList() != null && !recordAccessLevel.getDivisionList().isEmpty()) {
            for (RecordAccessLevelDivision division : recordAccessLevel.getDivisionList()) {
                if (division.getDivisionMaster() != null && division.getDivisionMaster().getId() != null) {
                    ValidateUtil.isStatusBlocked(division.getDivisionMaster().getStatus(), ErrorCode.RAL_DIVISION_BLOCKED);
                }
            }
        }
    }

    private void validateServiceList(RecordAccessLevel recordAccessLevel) {

        if (recordAccessLevel.getServiceList() != null && !recordAccessLevel.getServiceList().isEmpty()) {
            for (RecordAccessLevelService service : recordAccessLevel.getServiceList()) {
                if (service.getServiceMaster() != null && service.getServiceMaster().getId() != null) {
                    ValidateUtil.isStatusBlocked(service.getServiceMaster().getStatus(), ErrorCode.RAL_SERVICE_BLOCKED);
                }
            }
        }
    }

    private void validateSalesmanList(RecordAccessLevel recordAccessLevel) {

        if (recordAccessLevel.getSalesmanList() != null && !recordAccessLevel.getSalesmanList().isEmpty()) {
            for (RecordAccessLevelSalesman salesman : recordAccessLevel.getSalesmanList()) {
                if (salesman.getEmployeeMaster() != null && salesman.getEmployeeMaster().getId() != null) {
                    ValidateUtil.isEmployeeTerminated(salesman.getEmployeeMaster().getEmployementStatus(), ErrorCode.RAL_SALESMAN_TERMINATED);
                    ValidateUtil.isEmployeeResigned(salesman.getEmployeeMaster().getEmployementStatus(), ErrorCode.RAL_SALESMAN_RESIGNED);
                }
            }
        }
    }

    private void validateCustomerList(RecordAccessLevel recordAccessLevel) {

        if (recordAccessLevel.getCustomerList() != null && !recordAccessLevel.getCustomerList().isEmpty()) {
            for (RecordAccessLevelCustomer customer : recordAccessLevel.getCustomerList()) {
                if (customer.getPartyMaster() != null && customer.getPartyMaster().getId() != null) {
                    ValidateUtil.isStatusBlocked(customer.getPartyMaster().getStatus(), ErrorCode.RAL_CUSTOMER_BLOCKED);
                }
            }
        }
    }

}
