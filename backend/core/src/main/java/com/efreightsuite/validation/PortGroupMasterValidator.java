package com.efreightsuite.validation;

import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.model.PortGroupMaster;
import com.efreightsuite.service.RegularExpressionService;
import com.efreightsuite.util.RegExpName;
import com.efreightsuite.util.ValidateUtil;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Log4j2
@Service
public class PortGroupMasterValidator {

    @Autowired
    PortMasterValidator portMasterValidator;

    @Autowired
    private
    RegularExpressionService regExpService;


    public void validate(PortGroupMaster portGroupMaster) {


        log.info("Port Group Master Validation Started.... [" + portGroupMaster.getId() + "]");

        // Port Group Should not be null

        ValidateUtil.notNull(portGroupMaster, ErrorCode.PORT_GROUP_NOT_NULL);

        // Validating Port Group code for not null
        ValidateUtil.notNull(portGroupMaster.getPortGroupCode(), ErrorCode.PORT_GROUP_CODE_NOT_NULL);


        ValidateUtil.validateLength(portGroupMaster.getPortGroupCode(), 1, 10, ErrorCode.PORT_GROUP_CODE_LENGTH_INVALID);

        // Validating Port Group code pattern
        //ValidateUtil.checkPattern(portGroupMaster.getPortGroupCode(), "portGroupCode", true);
        regExpService.validate(portGroupMaster.getPortGroupCode(), RegExpName.Reg_Exp_Master_PORT_GROUP_CODE, ErrorCode.PORT_GROUP_CODE_INVALID);


        log.info("Port Group Code Validated...");

        // Validating Port Group Name for not null
        ValidateUtil.notNull(portGroupMaster.getPortGroupName(), ErrorCode.PORT_GROUP_NAME_NOT_NULL);


        // Validating Port Group name pattern
        //ValidateUtil.checkPattern(portGroupMaster.getPortGroupName(), "portGroupName", true);
        regExpService.validate(portGroupMaster.getPortGroupName(), RegExpName.Reg_Exp_Master_PORT_GROUP_NAME, ErrorCode.PORT_GROUP_NAME_INVALID);

        log.info("Port Group Name Validated...");

        // Validating Country

        ValidateUtil.notNull(portGroupMaster.getCountryMaster(), ErrorCode.PORT_GROUP_COUNTRY_NOT_NULL);
        ValidateUtil.isStatusBlocked(portGroupMaster.getCountryMaster().getStatus(), ErrorCode.PORT_GROUP_COUNTRY_INVALID);
        ValidateUtil.isStatusHidden(portGroupMaster.getCountryMaster().getStatus(), ErrorCode.PORT_GROUP_COUNTRY_INVALID);


        portGroupMaster.setCountryCode(portGroupMaster.getCountryMaster().getCountryCode());
        log.info("Port Group Country is Validated....");


        log.info("Port Group Master Validation Finished.... [" + portGroupMaster.getId() + "]");


    }

}
