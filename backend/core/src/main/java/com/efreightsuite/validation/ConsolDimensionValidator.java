package com.efreightsuite.validation;

import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.model.ConsolDimension;
import com.efreightsuite.util.ValidateUtil;
import lombok.extern.log4j.Log4j2;

import org.springframework.stereotype.Service;

@Service
@Log4j2
public class ConsolDimensionValidator {
    public void validate(ConsolDimension ConsolDimension) {

        log.info("ConsolDimension Validation Started......");
        verifyPieces(ConsolDimension);
        verifyLength(ConsolDimension);
        verifyWidth(ConsolDimension);
        verifyHeight(ConsolDimension);
        verifyGrossWeight(ConsolDimension);
        log.info("ConsolDimension Validation Finished......");
    }

    private void verifyPieces(ConsolDimension ConsolDimension) {

        ValidateUtil.notNull(ConsolDimension.getNoOfPiece(), ErrorCode.CONSOL_DIMENSION_PIECES_REQUIRED);

        ValidateUtil.belowRange(ConsolDimension.getNoOfPiece(), 0d, ErrorCode.CONSOL_DIMENSION_PIECES_NEGATIVE);

        ValidateUtil.aboveRange(ConsolDimension.getNoOfPiece(), 1000000000d, ErrorCode.CONSOL_DIMENSION_PIECES_INVALID);

        log.info("Dimension No. Of Pieces Validated...............[" + ConsolDimension.getNoOfPiece() + "]");
    }

    private void verifyLength(ConsolDimension ConsolDimension) {

        ValidateUtil.notNull(ConsolDimension.getLength(), ErrorCode.CONSOL_DIMENSION_LENGTH_REQUIRED);

        ValidateUtil.belowRange(ConsolDimension.getLength(), 0d, ErrorCode.CONSOL_DIMENSION_LENGTH_NEGATIVE);

        ValidateUtil.aboveRange(ConsolDimension.getLength(), 100000d, ErrorCode.CONSOL_DIMENSION_LENGTH_INVALID);

        log.info("Dimension Length Validated...............[" + ConsolDimension.getLength() + "]");
    }

    private void verifyWidth(ConsolDimension ConsolDimension) {

        ValidateUtil.notNull(ConsolDimension.getWidth(), ErrorCode.CONSOL_DIMENSION_WIDTH_REQUIRED);

        ValidateUtil.belowRange(ConsolDimension.getWidth(), 0d, ErrorCode.CONSOL_DIMENSION_WIDTH_NEGATIVE);

        ValidateUtil.aboveRange(ConsolDimension.getWidth(), 100000d, ErrorCode.CONSOL_DIMENSION_WIDTH_INVALID);

        log.info("Dimension Width Validated...............[" + ConsolDimension.getWidth() + "]");
    }

    private void verifyHeight(ConsolDimension ConsolDimension) {

        ValidateUtil.notNull(ConsolDimension.getHeight(), ErrorCode.CONSOL_DIMENSION_HEIGHT_REQUIRED);

        ValidateUtil.belowRange(ConsolDimension.getHeight(), 0, ErrorCode.CONSOL_DIMENSION_HEIGHT_NEGATIVE);

        ValidateUtil.aboveRange(ConsolDimension.getHeight(), 100000d, ErrorCode.CONSOL_DIMENSION_HEIGHT_INVALID);

        log.info("Dimension Height Validated...............[" + ConsolDimension.getHeight() + "]");
    }

    private void verifyGrossWeight(ConsolDimension ConsolDimension) {

        ValidateUtil.notNull(ConsolDimension.getGrossWeight(), ErrorCode.CONSOL_DIMENSION_GROSS_WEIGHT_REQUIRED);

        ValidateUtil.belowRange(ConsolDimension.getGrossWeight(), 0, ErrorCode.CONSOL_DIMENSION_GROSS_WEIGHT_NEGATIVE);

        ValidateUtil.aboveRange(ConsolDimension.getGrossWeight(), 1000000000d, ErrorCode.CONSOL_DIMENSION_GROSS_WEIGHT_INVALID);

        log.info("Dimension GrossWeight Validated ...............[" + ConsolDimension.getGrossWeight() + "]");
    }


}
