package com.efreightsuite.validation;

import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.model.DefaultMasterData;
import com.efreightsuite.model.LocationMaster;
import com.efreightsuite.repository.LocationMasterRepository;
import com.efreightsuite.util.ValidateUtil;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Log4j2
@Service
public class DefaultMasterDataValidator {

    @Autowired
    private
    LocationMasterRepository locationRepo;

    public void validate(DefaultMasterData defaultMasterData) {
        log.info("DefaultMasterData Validation Started..");
        verifyLocation(defaultMasterData);
        ValidateUtil.notNull(defaultMasterData.getDefaultName(), ErrorCode.DEFAULTMASTER_NAME_NOT_NULL);
        ValidateUtil.notNull(defaultMasterData.getCode(), ErrorCode.DEFAULTMASTER_CODE_NOT_NULL);
        ValidateUtil.notNull(defaultMasterData.getValue(), ErrorCode.DEFAULTMASTER_VALUE_NOT_NULL);
        validateSoftware(defaultMasterData);
        validateObjectGroup(defaultMasterData);
        validateObjectSubGroup(defaultMasterData);

        log.info("DefaultMasterData Validation Finished..");

    }

    private void verifyLocation(DefaultMasterData dcm) {

        ValidateUtil.notNull(dcm.getLocation(), ErrorCode.DEFAULTMASTER_LOCATION_NOT_NULL);

        LocationMaster location = locationRepo.findById(dcm.getLocation().getId());

        ValidateUtil.notNull(location, ErrorCode.DEFAULTMASTER_LOCATION_NOT_NULL);

        ValidateUtil.isStatusBlocked(location.getStatus(), ErrorCode.DEFAULTMASTER_LOCATION_BLOCK);

        ValidateUtil.isStatusHidden(location.getStatus(), ErrorCode.DEFAULTMASTER_LOCATION_HIDE);

        dcm.setLocation(location);
        dcm.setLocationCode(location.getLocationCode());
    }

    private void validateSoftware(DefaultMasterData defaultMaster) {
        ValidateUtil.notNull(defaultMaster.getSoftwareMaster().getId(), ErrorCode.SOFTWARE_ID_NOT_FOUND);
        ValidateUtil.notNull(defaultMaster.getSoftwareMaster().getSoftwareName(), ErrorCode.SOFTWARE_NAME_NOT_NULL);
        ValidateUtil.isStatusBlocked(defaultMaster.getSoftwareMaster().getStatus(), ErrorCode.SOFTWARE_STATUS_BLOCKED);
        ValidateUtil.isStatusBlocked(defaultMaster.getSoftwareMaster().getStatus(), ErrorCode.SOFTWARE_STATUS_HIDDEN);

    }

    private void validateObjectGroup(DefaultMasterData defaultMaster) {
        ValidateUtil.notNull(defaultMaster.getObjectGroupMaster().getId(), ErrorCode.OBJECTGROUP_ID_NOT_FOUND);
        ValidateUtil.notNull(defaultMaster.getObjectGroupMaster().getObjectGroupName(),
                ErrorCode.OBJECTGROUP_NAME_NOT_NULL);
        ValidateUtil.isStatusBlocked(defaultMaster.getObjectGroupMaster().getStatus(),
                ErrorCode.OBJECTGROUP_STATUS_BLOCK);
        ValidateUtil.isStatusBlocked(defaultMaster.getObjectGroupMaster().getStatus(),
                ErrorCode.OBJECTGROUP_STATUS_HIDE);

    }

    private void validateObjectSubGroup(DefaultMasterData defaultMaster) {
        ValidateUtil.notNull(defaultMaster.getObjectSubGroupMaster().getId(), ErrorCode.OBJECTSUBGROUP_ID_NOT_FOUND);
        ValidateUtil.notNull(defaultMaster.getObjectSubGroupMaster().getObjectSubGroupName(),
                ErrorCode.OBJECTSUBGROUP_NAME_NOT_NULL);
        ValidateUtil.isStatusBlocked(defaultMaster.getObjectSubGroupMaster().getStatus(),
                ErrorCode.OBJECTSUBGROUP_STATUS_BLOCKED);
        ValidateUtil.isStatusBlocked(defaultMaster.getObjectSubGroupMaster().getStatus(),
                ErrorCode.OBJECTSUBGROUP_STATUS_BLOCKED);

    }
}
