package com.efreightsuite.validation;

import com.efreightsuite.enumeration.AddressType;
import com.efreightsuite.enumeration.LovStatus;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.model.PartyAccountMaster;
import com.efreightsuite.model.PartyAddressMaster;
import com.efreightsuite.model.PartyAssociateToPartyTypeMaster;
import com.efreightsuite.model.PartyBusinessDetail;
import com.efreightsuite.model.PartyCompanyAssociate;
import com.efreightsuite.model.PartyContact;
import com.efreightsuite.model.PartyCreditLimit;
import com.efreightsuite.model.PartyEmailMapping;
import com.efreightsuite.model.PartyMaster;
import com.efreightsuite.model.PartyServiceMaster;
import com.efreightsuite.repository.DefaultMasterDataRepository;
import com.efreightsuite.repository.PartyMasterRepository;
import com.efreightsuite.repository.UserProfileRepository;
import com.efreightsuite.service.AuthService;
import com.efreightsuite.service.RegularExpressionService;
import com.efreightsuite.util.AppUtil;
import com.efreightsuite.util.RegExpName;
import com.efreightsuite.util.ValidateUtil;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.collections4.CollectionUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class PartyMasterValidator {

    @Autowired
    private
    RegularExpressionService regExpService;

    @Autowired
    private
    AppUtil appUtil;

    @Autowired
    DefaultMasterDataRepository defaultMasterDataRepository;

    @Autowired
    CountryMasterValidator countryMasterValidator;

    @Autowired
    CategoryMasterValidator categoryMasterValidator;

    @Autowired
    PartyMasterRepository partyMasterRepository;

    @Autowired
    private
    PartyMasterDetailValidator partyMasterDetailValidator;

    @Autowired
    private
    PartyAddressMasterValidator partyAddressMasterValidator;

    @Autowired
    private
    PartyServiceMasterValidator partyServiceMasterValidator;

    @Autowired
    private
    PartyEmailMappingValidator partyEmailMappingValidator;

    @Autowired
    private
    PartyAccountMasterValidator partyAccountMasterValidator;

    @Autowired
    private
    PartyCreditLimitValidator partyCreditLimitValidator;

    @Autowired
    private
    PartyBusinessDetailValidator partyBusinessDetailValidator;

    @Autowired
    private
    PartyContactValidator partyContactValidator;

    @Autowired
    UserProfileRepository userProfileRepository;

    public void validate(PartyMaster partyMaster) {

        verifyPartyName(partyMaster);

        verifyPartyStatus(partyMaster);

        verifyCountry(partyMaster);

        verifyPartyType(partyMaster);

        if (partyMaster.getGradeMaster() != null) {
            partyMaster.setGradeName(partyMaster.getGradeMaster().getGradeName());
        } else {
            partyMaster.setGradeName(null);
        }

        if (isPartyIndividual(partyMaster)) {
            verifyFirstName(partyMaster);
            verifyLastName(partyMaster);
            verifySalutation(partyMaster);
        } else {
            partyMaster.setFirstName(null);
            partyMaster.setLastName(null);
            partyMaster.setSalutation(null);
        }

        verifyCategory(partyMaster);

        partyMasterDetailValidator.validate(partyMaster);

        if (CollectionUtils.isNotEmpty(partyMaster.getPartyTypeList())) {
            for (PartyAssociateToPartyTypeMaster tmp : partyMaster.getPartyTypeList()) {
                tmp.setPartyTypeCode(tmp.getPartyTypeMaster().getPartyTypeCode());
            }
        }

        if (CollectionUtils.isNotEmpty(partyMaster.getPartyCompanyAssociateList())) {
            for (PartyCompanyAssociate tmp : partyMaster.getPartyCompanyAssociateList()) {
                ValidateUtil.notNull(tmp, ErrorCode.PARTY_DIVISION_INVALID);
                ValidateUtil.notNull(tmp.getCompanyMaster(), ErrorCode.PARTY_COMPANY_REQUIRED);
                ValidateUtil.notNull(tmp.getCompanyMaster().getId(), ErrorCode.PARTY_COMPANY_REQUIRED);
                tmp.setCompanyCode(tmp.getCompanyMaster().getCompanyCode());

                if (tmp.getDivisionMaster() != null && tmp.getDivisionMaster().getId() != null) {
                    if (tmp.getDivisionMaster().getStatus().equals(LovStatus.Block)) {
                        throw new RestException(ErrorCode.DIVISION_STATUS_BLOCK);
                    } else if (tmp.getDivisionMaster().getStatus().equals(LovStatus.Hide)) {
                        throw new RestException(ErrorCode.DIVISION_STATUS_HIDE);
                    }
                    tmp.setDivisionCode(tmp.getDivisionMaster().getDivisionCode());
                } else {

                    tmp.setDivisionMaster(null);
                    tmp.setDivisionCode(null);
                }
            }
        }


        if (CollectionUtils.isNotEmpty(partyMaster.getPartyAddressList())) {
            for (PartyAddressMaster partyAddressMaster : partyMaster.getPartyAddressList()) {
                partyAddressMasterValidator.validate(partyAddressMaster);
            }

            for (int i = 0; i < partyMaster.getPartyAddressList().size(); i++) {
                for (int j = 0; j < partyMaster.getPartyAddressList().size(); j++) {
                    if (i != j
                            && partyMaster.getPartyAddressList().get(j).getAddressType() != AddressType.Other
                            && partyMaster.getPartyAddressList().get(i).getAddressType() == partyMaster.getPartyAddressList().get(j).getAddressType()) {
                        throw new RestException(ErrorCode.PARTY_ADDRESS_TYPE_ALREADY_EXIST);
                    }
                }
            }

        } else {

            throw new RestException(ErrorCode.PARTY_ADDRESS_REQUIRED);
        }


        if (CollectionUtils.isNotEmpty(partyMaster.getPartyServiceList())) {
            for (PartyServiceMaster partyServiceMaster : partyMaster.getPartyServiceList()) {
                partyServiceMasterValidator.validate(partyServiceMaster);
            }
        }

        if (CollectionUtils.isNotEmpty(partyMaster.getPartyAccountList())) {
            for (PartyAccountMaster partyAccountMaster : partyMaster.getPartyAccountList()) {
                partyAccountMasterValidator.validate(partyAccountMaster);
            }
        }

        if (CollectionUtils.isNotEmpty(partyMaster.getPartyCreditLimitList())) {
            for (PartyCreditLimit partyCreditLimit : partyMaster.getPartyCreditLimitList()) {
                partyCreditLimitValidator.validate(partyCreditLimit);
            }
        }

        if (CollectionUtils.isNotEmpty(partyMaster.getPartyEmailMappingList())) {
            for (PartyEmailMapping partyEmailMapping : partyMaster.getPartyEmailMappingList()) {
                partyEmailMappingValidator.validate(partyEmailMapping);
            }
        }

        if (CollectionUtils.isNotEmpty(partyMaster.getPartyBusinessDetailList())) {
            for (PartyBusinessDetail partyBusinessDetail : partyMaster.getPartyBusinessDetailList()) {
                partyBusinessDetailValidator.validate(partyBusinessDetail);
            }
        }


        if (CollectionUtils.isNotEmpty(partyMaster.getContactList())) {
            for (PartyContact partyContact : partyMaster.getContactList()) {
                partyContactValidator.validate(partyContact);
            }
        }

    }

    private void verifyPartyName(PartyMaster partyMaster) {
        ValidateUtil.notNullOrEmpty(partyMaster.getPartyName(), ErrorCode.PARTY_NAME_NOT_NULL_OR_EMPTY);
        String data = appUtil.getLocationConfig("party.name.special.char.allowed", AuthService.getCurrentUser().getSelectedUserLocation(), true);
        if (data.equals("FALSE")) {
            regExpService.validate(partyMaster.getPartyName(), RegExpName.Reg_Exp_Master_PARTY_NAME, ErrorCode.PARTY_NAME_INVALID);
        }
    }

    private void verifyPartyStatus(PartyMaster partyMaster) {
        ValidateUtil.validateEnum(LovStatus.class, partyMaster.getStatus(), ErrorCode.PARTY_STATUS_INVALID);
    }

    private boolean isPartyIndividual(PartyMaster partyMaster) {
        boolean isIndividual = false;
        if (CollectionUtils.isNotEmpty(partyMaster.getPartyTypeList())) {
            for (PartyAssociateToPartyTypeMaster partyAssociateToPartyTypeMaster : partyMaster.getPartyTypeList()) {
                if ("INDIVIDUAL".equalsIgnoreCase(partyAssociateToPartyTypeMaster.getPartyTypeMaster().getPartyTypeName())) {
                    isIndividual = true;
                    break;
                }
            }
        }
        return isIndividual;
    }


    private void verifyFirstName(PartyMaster partyMaster) {

        ValidateUtil.notNullOrEmpty(partyMaster.getFirstName(), ErrorCode.PARTY_FIRST_NAME_IS_REQUIRED);

        regExpService.validate(partyMaster.getFirstName(), RegExpName.Reg_Exp_Master_PARTY_FIRST_NAME, ErrorCode.PARTY_FIRST_NAME_INVALID);

    }

    private void verifyLastName(PartyMaster partyMaster) {

        ValidateUtil.notNullOrEmpty(partyMaster.getLastName(), ErrorCode.PARTY_LAST_NAME_IS_REQUIRED);

        regExpService.validate(partyMaster.getLastName(), RegExpName.Reg_Exp_Master_PARTY_LAST_NAME, ErrorCode.PARTY_LAST_NAME_INVALID);

    }

    private void verifySalutation(PartyMaster partyMaster) {

        ValidateUtil.notNull(partyMaster.getSalutation(), ErrorCode.PARTY_SALUTATION_REQUIRED);

        log.info("PartyMaster Saluation is Validated....[" + partyMaster.getSalutation() + "]");

    }

    // To Verify Country and Country Code
    private void verifyCountry(PartyMaster partyMaster) {

        ValidateUtil.notNull(partyMaster.getCountryMaster(), ErrorCode.PARTY_COUNTRY_REQUIRED);

        ValidateUtil.notNull(partyMaster.getCountryMaster().getId(), ErrorCode.PARTY_COUNTRY_REQUIRED);

        ValidateUtil.isStatusBlocked(partyMaster.getCountryMaster().getStatus(), ErrorCode.PARTY_COUNTRY_BLOCKED);

        ValidateUtil.isStatusHidden(partyMaster.getCountryMaster().getStatus(), ErrorCode.PARTY_COUNTRY_HIDDEN);

        partyMaster.setCountryCode(partyMaster.getCountryMaster().getCountryCode());

        log.info("PartyMaster Country Validated...... [" + partyMaster.getCountryCode() + "]");
    }

    private void verifyCategory(PartyMaster partyMaster) {

        if (partyMaster.getCategoryMaster() != null && partyMaster.getCategoryMaster().getId() != null) {

            ValidateUtil.notNull(partyMaster.getCategoryMaster(), ErrorCode.PARTY_CATEGORY_REQUIRED);

            ValidateUtil.notNull(partyMaster.getCategoryMaster().getId(), ErrorCode.PARTY_CATEGORY_REQUIRED);

            ValidateUtil.isStatusBlocked(partyMaster.getCategoryMaster().getStatus(), ErrorCode.PARTY_BLOCKED_CATEGORY_SELECTED);

            ValidateUtil.isStatusHidden(partyMaster.getCategoryMaster().getStatus(), ErrorCode.PARTY_HIDDEN_CATEGORY_SELECTED);
            partyMaster.setCategoryCode(partyMaster.getCategoryMaster().getCategoryCode());
        } else {
            partyMaster.setCategoryMaster(null);
            partyMaster.setCategoryCode(null);
        }


    }


    // To Verify  PartyType
    private void verifyPartyType(PartyMaster partyMaster) {

        if (CollectionUtils.isEmpty(partyMaster.getPartyTypeList())) {
            throw new RestException(ErrorCode.PARTY_PARTY_TYPE_NOT_NULL);
        }


        for (PartyAssociateToPartyTypeMaster pp : partyMaster.getPartyTypeList()) {
            if (pp.getPartyTypeMaster() == null) {
                throw new RestException(ErrorCode.PARTY_PARTY_TYPE_NOT_NULL);
            } else {
                if (pp.getPartyTypeMaster().getStatus().equals(LovStatus.Block)) {
                    throw new RestException(ErrorCode.PARTY_PARTY_TYPE_BLOCKED_SELECTED);
                }
                if (pp.getPartyTypeMaster().getStatus().equals(LovStatus.Hide)) {
                    throw new RestException(ErrorCode.PARTY_PARTY_TYPE_HIDDEN_SELECTED);
                }
            }
        }
    }


}
