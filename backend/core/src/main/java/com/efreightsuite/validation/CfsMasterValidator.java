package com.efreightsuite.validation;

import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.model.CFSMaster;
import com.efreightsuite.service.RegularExpressionService;
import com.efreightsuite.util.RegExpName;
import com.efreightsuite.util.ValidateUtil;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Log4j2
@Service
public class CfsMasterValidator {

    @Autowired
    private
    RegularExpressionService regExpService;

    public void validate(CFSMaster cfsMaster) {


        ValidateUtil.notNullOrEmpty(cfsMaster.getCfsCode(), ErrorCode.MASTER_CFS_CODE);
        regExpService.validate(cfsMaster.getCfsCode(), RegExpName.Reg_Exp_Cfs_Receive_Code, ErrorCode.MASTER_CFS_CODE);

        ValidateUtil.notNullOrEmpty(cfsMaster.getCfsName(), ErrorCode.MASTER_CFS_NAME);
        regExpService.validate(cfsMaster.getCfsName(), RegExpName.Reg_Exp_Cfs_Receive_Name, ErrorCode.MASTER_CFS_NAME);

        if (cfsMaster.getPortMaster() != null && cfsMaster.getPortMaster().getId() != null) {

            ValidateUtil.notNull(cfsMaster.getPortMaster(), ErrorCode.MASTER_CFS_PORT);
            ValidateUtil.notNull(cfsMaster.getPortMaster().getId(), ErrorCode.MASTER_CFS_PORT);
            ValidateUtil.notNullOrEmpty(cfsMaster.getPortMaster().getPortCode(), ErrorCode.MASTER_CFS_PORT);

            ValidateUtil.isStatusBlocked(cfsMaster.getPortMaster().getStatus(), ErrorCode.MASTER_CFS_PORT_BLOCK);
            ValidateUtil.isStatusHidden(cfsMaster.getPortMaster().getStatus(), ErrorCode.MASTER_CFS_PORT_HIDE);


            cfsMaster.setPortCode(cfsMaster.getPortMaster().getPortCode());
        } else {
            cfsMaster.setPortCode(null);
            cfsMaster.setPortMaster(null);
        }

    }

}
