package com.efreightsuite.validation;

import com.efreightsuite.model.SurchargeMapping;
import com.efreightsuite.service.RegularExpressionService;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Log4j2
@Service
public class SurchargeMappingValidator {

    @Autowired
    RegularExpressionService regExpService;


    public void validate(SurchargeMapping surchargeMapping) {


        if (surchargeMapping.getChargeMaster() != null) {
            surchargeMapping.setChargeCode(surchargeMapping.getChargeMaster().getChargeCode());
        }


    }
}
