package com.efreightsuite.validation;

import java.util.ArrayList;

import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.model.common.LocationSetup;
import com.efreightsuite.model.common.LocationSetupCharge;
import com.efreightsuite.util.ValidateUtil;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Log4j2
@Service
public class LocationSetupValidator {

    @Autowired
    ChargeMasterValidator chargeMasterValidator;

    public void validateCompanyTab(LocationSetup locationSetup) {
        log.info("Validating the company tab");

        ValidateUtil.notNull(locationSetup.getSalutation(), ErrorCode.UR_SALUTATION_REQUIRED);
        ValidateUtil.notNullOrEmpty(locationSetup.getFirstName(), ErrorCode.UR_FIRST_NAME_REQUIRED);
        ValidateUtil.notNullOrEmpty(locationSetup.getLastName(), ErrorCode.UR_LAST_NAME_REQUIRED);
        ValidateUtil.notNullOrEmpty(locationSetup.getEmail(), ErrorCode.UR_EMAIL_REQUIRED);
        ValidateUtil.notNull(locationSetup.getCountryMaster(), ErrorCode.UR_COUNTRY_REQUIRED);
        ValidateUtil.notNull(locationSetup.getCityMaster(), ErrorCode.UR_CITY_REQUIRED);
        ValidateUtil.notNullOrEmpty(locationSetup.getPhone(), ErrorCode.UR_PHONE_REQUIRED);
        ValidateUtil.notNullOrEmpty(locationSetup.getCompanyRegisteredName(), ErrorCode.UR_REG_NAME_REQUIRED);
        ValidateUtil.notNullOrEmpty(locationSetup.getAddressLine1(), ErrorCode.UR_ADDRESS_LINE1_REQUIRED);
        ValidateUtil.notNullOrEmpty(locationSetup.getArea(), ErrorCode.UR_AREA_REQUIRED);
        ValidateUtil.notNull(locationSetup.getTimeZoneMaster(), ErrorCode.UR_TIME_ZONE_REQUIRED);
    }

    public void validateLoginTab(LocationSetup locationSetup) {
        log.info("Validating the login tab");

        ValidateUtil.notNullOrEmpty(locationSetup.getPosition(), ErrorCode.UR_POSITION_REQUIRED);
        ValidateUtil.notNullOrEmpty(locationSetup.getUserName(), ErrorCode.UR_USERNAME_REQUIRED);
        ValidateUtil.notNullOrEmpty(locationSetup.getPassword(), ErrorCode.UR_PASSWORD_REQUIRED);

        ValidateUtil.notNull(locationSetup.getServiceList(), ErrorCode.UR_SERVICE_REQUIRED);
        if (locationSetup.getServiceList().isEmpty()) {
            ValidateUtil.notNull(locationSetup.getServiceList(), ErrorCode.UR_SERVICE_REQUIRED);
        }
    }

    public boolean validateTrail(LocationSetup locationSetup) {

        ValidateUtil.notNullOrEmpty(locationSetup.getEmail(), ErrorCode.UR_EMAIL_REQUIRED);
        ValidateUtil.notNullOrEmpty(locationSetup.getFirstName(), ErrorCode.UR_FIRSTNAME_REQUIRED);
        ValidateUtil.notNullOrEmpty(locationSetup.getLastName(), ErrorCode.UR_LASTNAME_REQUIRED);
        ValidateUtil.notNullOrEmpty(locationSetup.getPassword(), ErrorCode.UR_PASSWORD_REQUIRED);

        return true;
    }


    public void validateChargeTab(LocationSetup locationSetup) {

        if (locationSetup.getChargeList() != null && !locationSetup.getChargeList().isEmpty()) {
            for (LocationSetupCharge chargeMaster : locationSetup.getChargeList()) {
                //chargeMasterValidator.validate(new ChargeMaster(charge));
                ValidateUtil.notNull(chargeMaster.getChargeCode(), ErrorCode.CHARGE_CODE_NOT_NULL);
                ValidateUtil.validateLength(chargeMaster.getChargeCode(), 1, 10, ErrorCode.CHARGE_CODE_INVALID);

                ValidateUtil.notNull(chargeMaster.getChargeName(), ErrorCode.CHARGE_NAME_NOT_NULL);
                ValidateUtil.validateLength(chargeMaster.getChargeName(), 1, 100, ErrorCode.CHARGE_NAME_INVALID);
            }
        } else {
            locationSetup.setChargeList(new ArrayList<>());
            locationSetup.getChargeList().clear();
        }
    }

}
