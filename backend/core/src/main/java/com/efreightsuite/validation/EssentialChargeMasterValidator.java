package com.efreightsuite.validation;


import java.util.Objects;

import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.model.EssentialChargeMaster;
import com.efreightsuite.service.AuthService;
import com.efreightsuite.util.ValidateUtil;
import lombok.extern.log4j.Log4j2;

import org.springframework.stereotype.Service;

@Log4j2
@Service
public class EssentialChargeMasterValidator {


    public void validate(EssentialChargeMaster essentialChargeMaster) {
        log.info("EssentialChargeMasterValidator validate Start");


        verifyServiceMaster(essentialChargeMaster);
        verifyChargeMaster(essentialChargeMaster);
        verifyOrigin(essentialChargeMaster);
        verifyDestination(essentialChargeMaster);
        if (essentialChargeMaster.getOrigin() != null && essentialChargeMaster.getDestination() != null && Objects.equals(essentialChargeMaster.getOrigin().getId(), essentialChargeMaster.getDestination().getId())) {
            throw new RestException(ErrorCode.ESSENTIAL_ORGIN_DESTINATION_SAME);
        }
        verifyTosMaster(essentialChargeMaster);
        verifyCrn(essentialChargeMaster);

        if (essentialChargeMaster.getId() == null) {
            verifyCompany(essentialChargeMaster);
        }

        log.info("EssentialChargeMasterValidator validate End");
    }

    private void verifyCompany(EssentialChargeMaster essentialChargeMaster) {
        essentialChargeMaster.setCompanyMaster(AuthService.getCurrentUser().getSelectedUserLocation().getCompanyMaster());
        essentialChargeMaster.setLocationMaster(AuthService.getCurrentUser().getSelectedUserLocation());
        essentialChargeMaster.setCountryMaster(AuthService.getCurrentUser().getSelectedUserLocation().getCountryMaster());
        essentialChargeMaster.setCompanyCode(essentialChargeMaster.getCompanyMaster().getCompanyCode());
        essentialChargeMaster.setLocationCode(essentialChargeMaster.getLocationMaster().getLocationCode());
        essentialChargeMaster.setCountryCode(essentialChargeMaster.getCountryMaster().getCountryCode());
    }

    private void verifyServiceMaster(EssentialChargeMaster essentialChargeMaster) {
        ValidateUtil.notNull(essentialChargeMaster.getChargeMaster(), ErrorCode.ESSENTIAL_CHARGE);
        ValidateUtil.notNull(essentialChargeMaster.getChargeMaster().getId(), ErrorCode.ESSENTIAL_CHARGE);
        ValidateUtil.notNullOrEmpty(essentialChargeMaster.getChargeMaster().getChargeCode(), ErrorCode.ESSENTIAL_CHARGE);
        ValidateUtil.isStatusBlocked(essentialChargeMaster.getChargeMaster().getStatus(), ErrorCode.ESSENTIAL_CHARGE_BLOCK);
        ValidateUtil.isStatusHidden(essentialChargeMaster.getChargeMaster().getStatus(), ErrorCode.ESSENTIAL_CHARGE_HIDE);
    }

    private void verifyDestination(EssentialChargeMaster essentialChargeMaster) {

        if (essentialChargeMaster != null) {
            if (essentialChargeMaster.getDestination() != null) {
                ValidateUtil.isStatusBlocked(essentialChargeMaster.getDestination().getStatus(), ErrorCode.ESSENTIAL_DESTINATION_BLOCK);
                ValidateUtil.isStatusHidden(essentialChargeMaster.getDestination().getStatus(), ErrorCode.ESSENTIAL_DESTINATION_HIDE);
                essentialChargeMaster.setDestinationCode(essentialChargeMaster.getDestination().getPortCode());
            }
            essentialChargeMaster.setDestination(null);
        }
    }

    private void verifyOrigin(EssentialChargeMaster essentialChargeMaster) {

        if (essentialChargeMaster != null) {
            if (essentialChargeMaster.getOrigin() != null) {
                ValidateUtil.isStatusBlocked(essentialChargeMaster.getOrigin().getStatus(), ErrorCode.ESSENTIAL_ORIGIN_BLOCK);
                ValidateUtil.isStatusHidden(essentialChargeMaster.getOrigin().getStatus(), ErrorCode.ESSENTIAL_ORIGIN_HIDE);
                essentialChargeMaster.setOriginPortCode(essentialChargeMaster.getOrigin().getPortCode());
            }
            essentialChargeMaster.setOrigin(null);
        }
    }

    private void verifyTosMaster(EssentialChargeMaster essentialChargeMaster) {
        if (essentialChargeMaster != null) {
            if (essentialChargeMaster.getTosMaster() != null) {
                ValidateUtil.isStatusBlocked(essentialChargeMaster.getTosMaster().getStatus(), ErrorCode.ESSENTIAL_TOS_BLOCK);
                ValidateUtil.isStatusHidden(essentialChargeMaster.getTosMaster().getStatus(), ErrorCode.ESSENTIAL_TOS_HIDE);
                essentialChargeMaster.setTosCode(essentialChargeMaster.getTosMaster().getTosCode());
            }
            essentialChargeMaster.setTosMaster(null);
        }
    }

    private void verifyChargeMaster(EssentialChargeMaster essentialChargeMaster) {
        ValidateUtil.notNull(essentialChargeMaster.getServiceMaster(), ErrorCode.ESSENTIAL_SERVICE);
        ValidateUtil.notNull(essentialChargeMaster.getServiceMaster().getId(), ErrorCode.ESSENTIAL_SERVICE);
        ValidateUtil.notNullOrEmpty(essentialChargeMaster.getServiceMaster().getServiceCode(), ErrorCode.ESSENTIAL_SERVICE);
        ValidateUtil.isStatusBlocked(essentialChargeMaster.getServiceMaster().getStatus(), ErrorCode.ESSENTIAL_SERVICE_BLOCK);
        ValidateUtil.isStatusHidden(essentialChargeMaster.getServiceMaster().getStatus(), ErrorCode.ESSENTIAL_SERVICE_HIDE);

    }


    private void verifyCrn(EssentialChargeMaster essentialChargeMaster) {
        ValidateUtil.notNull(essentialChargeMaster.getCrn(), ErrorCode.ESSENTIAL_CRN_REQUIRED);

    }
}
