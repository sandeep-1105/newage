package com.efreightsuite.validation;

import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.model.ChargeMaster;
import com.efreightsuite.model.ConsolCharge;
import com.efreightsuite.model.CurrencyMaster;
import com.efreightsuite.repository.ChargeMasterRepository;
import com.efreightsuite.repository.CurrencyMasterRepository;
import com.efreightsuite.util.ValidateUtil;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class ConsolChargeValidator {

    @Autowired
    private CurrencyMasterRepository currencyMasterRepository;

    @Autowired
    private ChargeMasterRepository chargeMasterRepository;

    public void validate(ConsolCharge charge) {

        if (charge.getLocation() != null) {
            charge.setLocationCode(charge.getLocation().getLocationCode());
        }

        if (charge.getCompany() != null) {
            charge.setCompanyCode(charge.getCompany().getCompanyCode());
        }

        if (charge.getCountry() != null) {
            charge.setCountryCode(charge.getCountry().getCountryCode());
        }

        verifyChargeCode(charge);

        verifyChargeName(charge);

        verifyCurrency(charge);

        verifyUnit(charge);

        verifyAmount(charge);

    }

    private void verifyAmount(ConsolCharge charge) {

        if (charge.getAmountPerUnit() != null) {
            if (charge.getAmountPerUnit() <= 0) {
                throw new RestException(ErrorCode.CONSOL_CHARGE_AMOUNT_PER_UNIT);
            }
        } else {
            charge.setAmountPerUnit(null);
        }

        if (charge.getAmount() != null) {
            if (charge.getAmount() <= 0) {
                throw new RestException(ErrorCode.CONSOL_CHARGE_AMOUNT);
            }
        } else {
            charge.setAmount(null);
        }

        if (charge.getLocalAmount() != null) {
            if (charge.getLocalAmount() <= 0) {
                throw new RestException(ErrorCode.CONSOL_CHARGE_LOCAL_AMOUNT);
            }
        } else {
            charge.setLocalAmount(null);
        }

        if (charge.getAmountPerUnit() != null || charge.getAmount() != null || charge.getLocalAmount() != null) {

            ValidateUtil.notNull(charge.getAmountPerUnit(), ErrorCode.CONSOL_CHARGE_AMOUNT_PER_UNIT);
            ValidateUtil.notNull(charge.getAmount(), ErrorCode.CONSOL_CHARGE_AMOUNT);
            ValidateUtil.notNull(charge.getLocalAmount(), ErrorCode.CONSOL_CHARGE_LOCAL_AMOUNT);
        }
    }

    private void verifyUnit(ConsolCharge charge) {

        ValidateUtil.notNull(charge.getUnit(), ErrorCode.CONSOL_CHARGE_UNIT);

        if (charge.getUnit() <= 0) {
            throw new RestException(ErrorCode.CONSOL_CHARGE_UNIT);
        }

    }

    private void verifyCurrency(ConsolCharge charge) {

        ValidateUtil.notNull(charge.getCurrency(), ErrorCode.CONSOL_CHARGE_CURRENCY);

        ValidateUtil.notNull(charge.getCurrency().getId(), ErrorCode.CONSOL_CHARGE_CURRENCY);

        CurrencyMaster currency = currencyMasterRepository.getOne(charge.getCurrency().getId());

        ValidateUtil.isStatusBlocked(currency.getStatus(), ErrorCode.CONSOL_CHARGE_CURRENCY_BLOCKED);

        ValidateUtil.isStatusHidden(currency.getStatus(), ErrorCode.CONSOL_CHARGE_CURRENCY_HIDDEN);

        charge.setCurrencyCode(currency.getCurrencyCode());

        if (charge.getCurrencyRoe() != null) {
            if (charge.getCurrencyRoe() <= 0) {
                throw new RestException(ErrorCode.CONSOL_CHARGE_ROE);
            }
        } else {
            charge.setCurrencyRoe(null);
        }

    }

    private void verifyChargeCode(ConsolCharge charge) {

        ValidateUtil.notNull(charge.getChargeMaster(), ErrorCode.CONSOL_CHARGE_REQUIRED);

        ValidateUtil.notNull(charge.getChargeMaster().getId(), ErrorCode.CONSOL_CHARGE_REQUIRED);

        ChargeMaster chargeMaster = chargeMasterRepository.findById(charge.getChargeMaster().getId());

        ValidateUtil.notNullOrEmpty(chargeMaster.getChargeCode(), ErrorCode.CONSOL_CHARGE_REQUIRED);

        ValidateUtil.isStatusBlocked(chargeMaster.getStatus(), ErrorCode.CONSOL_CHARGE_BLOCKED);

        ValidateUtil.isStatusHidden(chargeMaster.getStatus(), ErrorCode.CONSOL_CHARGE_HIDDEN);

        charge.setChargeCode(chargeMaster.getChargeCode());

    }

    private void verifyChargeName(ConsolCharge charge) {
        ValidateUtil.notNull(charge.getChargeName(), ErrorCode.CONSOL_CHARGE_NAME_REQUIRED);
    }


}
