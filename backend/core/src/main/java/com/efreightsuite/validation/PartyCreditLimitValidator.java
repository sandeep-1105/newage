package com.efreightsuite.validation;

import com.efreightsuite.enumeration.LovStatus;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.model.PartyCreditLimit;
import com.efreightsuite.util.ValidateUtil;
import lombok.extern.log4j.Log4j2;

import org.springframework.stereotype.Service;

/**
 * Class Name Purpose CreatedBy/ModifiedBy Date Version
 * <p>
 * PartyCreditLimitValidator Validator for PartyCreditLimit Achyutananda P
 * 18-05-16 0.0
 */

@Service
@Log4j2
public class PartyCreditLimitValidator {

    public void validate(PartyCreditLimit partyCreditLimit) {
        ValidateUtil.notNull(partyCreditLimit, ErrorCode.PARTY_CREDITLIMIT_REQUIRED);

        validateLocationMaster(partyCreditLimit);
        validateServiceMaster(partyCreditLimit);
        validateDivisionMaster(partyCreditLimit);
        validateCreditDays(partyCreditLimit);
        validatePublishCreditDays(partyCreditLimit);
        validateCreditAmount(partyCreditLimit);

        if (partyCreditLimit.getCompanyMaster() != null) {
            partyCreditLimit.setCompanyCode(partyCreditLimit.getCompanyMaster().getCompanyCode());
        } else {
            partyCreditLimit.setCompanyCode(null);
        }

        if (partyCreditLimit.getCountryMaster() != null) {
            partyCreditLimit.setCountryCode(partyCreditLimit.getCountryMaster().getCountryCode());
        } else {
            partyCreditLimit.setCountryCode(null);
        }
    }

    // Location Master validation
    private void validateLocationMaster(PartyCreditLimit partyCreditLimit) {

        ValidateUtil.notNull(partyCreditLimit, ErrorCode.PARTY_CREDIT_INVALID);

        ValidateUtil.notNull(partyCreditLimit.getLocationMaster(), ErrorCode.PARTY_ACCOUNT_LOCATION_REQUIRED);
        ValidateUtil.notNull(partyCreditLimit.getLocationMaster().getId(), ErrorCode.PARTY_ACCOUNT_LOCATION_REQUIRED);

        if (partyCreditLimit.getLocationMaster().getStatus().equals(LovStatus.Block)) {
            throw new RestException(ErrorCode.LOCATION_STATUS_BLOCK);
        }

        partyCreditLimit.setLocationCode(partyCreditLimit.getLocationMaster().getLocationCode());
    }

    // Service Master Validation
    private void validateServiceMaster(PartyCreditLimit partyCreditLimit) {

        if (partyCreditLimit.getServiceMaster() != null && partyCreditLimit.getServiceMaster().getId() != null) {
            ValidateUtil.notNull(partyCreditLimit.getServiceMaster(), ErrorCode.PARTY_CREDIT_SERVICE_OR_DIVISION_REQUIRED);
            ValidateUtil.notNull(partyCreditLimit.getServiceMaster().getId(), ErrorCode.PARTY_CREDIT_SERVICE_OR_DIVISION_REQUIRED);
            if (partyCreditLimit.getServiceMaster().getStatus().equals(LovStatus.Block)) {
                throw new RestException(ErrorCode.SERVICE_STATUS_BLOCK);
            } else if (partyCreditLimit.getServiceMaster().getStatus().equals(LovStatus.Hide)) {
                throw new RestException(ErrorCode.SERVICE_STATUS_HIDE);
            }

            partyCreditLimit.setServiceCode(partyCreditLimit.getServiceMaster().getServiceCode());
        } else {
            partyCreditLimit.setServiceMaster(null);
            partyCreditLimit.setServiceCode(null);
        }
    }

    // Division Master validation
    private void validateDivisionMaster(PartyCreditLimit partyCreditLimit) {

        if (partyCreditLimit.getDivisionMaster() != null && partyCreditLimit.getDivisionMaster().getId() != null) {
            if (partyCreditLimit.getDivisionMaster().getStatus().equals(LovStatus.Block)) {
                throw new RestException(ErrorCode.DIVISION_STATUS_BLOCK);
            } else if (partyCreditLimit.getDivisionMaster().getStatus().equals(LovStatus.Hide)) {
                throw new RestException(ErrorCode.DIVISION_STATUS_HIDE);
            }

            partyCreditLimit.setDivisionCode(partyCreditLimit.getDivisionMaster().getDivisionCode());
        } else {
            partyCreditLimit.setDivisionMaster(null);
            partyCreditLimit.setDivisionCode(null);
        }

    }

    private void validateCreditDays(PartyCreditLimit partyCreditLimit) {

        if (partyCreditLimit.getCreditDays() != null) {
            if (partyCreditLimit.getCreditDays() == 0) {
                throw new RestException(ErrorCode.PARTY_CREDITLIMIT_CREDITDAYS_NOT_ZERO);
            } else {
                if (partyCreditLimit.getCreditDays() >= 1 && partyCreditLimit.getCreditDays() <= 999) {
                    log.info("Party Credit days is>1 && <=999");
                } else {
                    throw new RestException(ErrorCode.PARTY_CREDIT_CREDITDAYS_INVALID);
                }
            }
        } else {
            if (partyCreditLimit.getCreditAmount() == null) {
                throw new RestException(ErrorCode.PARTY_CREDITLIMIT_OR_CREDITDAYS_REQUIRED);
            }
        }

    }

    private void validatePublishCreditDays(PartyCreditLimit partyCreditLimit) {

        if (partyCreditLimit.getPublishCreditDays() != null) {

            if (partyCreditLimit.getPublishCreditDays() == 0) {
                throw new RestException(ErrorCode.PARTY_CREDITLIMIT_PUBLICCREDITDAYS_NOT_ZERO);

            } else {
                if (partyCreditLimit.getPublishCreditDays() >= 1 && partyCreditLimit.getPublishCreditDays() <= 999) {
                    log.info("Party publich Credit days is>1 && <=999");
                } else {
                    throw new RestException(ErrorCode.PARTY_CREDIT_PUBLICCREDITDAYS_INVALID);
                }
            }
        }

    }

    private void validateCreditAmount(PartyCreditLimit partyCreditLimit) {

        if (partyCreditLimit.getCreditAmount() != null) {

            if (partyCreditLimit.getCreditAmount() == 0) {
                throw new RestException(ErrorCode.PARTY_CREDITLIMIT_CREDITAMOUNT_NOT_ZERO);

            } else {
                if (partyCreditLimit.getCreditAmount() >= 1 && partyCreditLimit.getCreditAmount() <= 9999999999D) {
                    log.info("Party Credit amount is:  >= 1 && <= 9999999999D");
                } else {
                    throw new RestException(ErrorCode.PARTY_CREDIT_CREDITAMOUNT_INVALID);
                }
            }
        } else {
            if (partyCreditLimit.getCreditDays() == null) {
                throw new RestException(ErrorCode.PARTY_CREDITLIMIT_OR_CREDITDAYS_REQUIRED);
            }
        }

    }

}
