package com.efreightsuite.validation;


import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.model.PricingCarrier;
import com.efreightsuite.model.PricingCarrierSlap;
import com.efreightsuite.service.RegularExpressionService;
import com.efreightsuite.util.RegExpName;
import com.efreightsuite.util.ValidateUtil;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class PricingCarrierSlapValidator {

    @Autowired
    private
    RegularExpressionService regExpService;


    public void validate(PricingCarrier pricingCarrier, PricingCarrierSlap pricingCarrierSlap) {

        if (pricingCarrierSlap != null) {

            //ValidateUtil.notNull(pricingCarrierSlap.getMinus45(), ErrorCode.PRICING_MASTER_MINUS45_REQUIRED);
            if (pricingCarrierSlap.getMinus45() != null) {
                regExpService.validate(pricingCarrierSlap.getMinus45(), RegExpName.Reg_Exp_AMOUNT_TEN_SIX, ErrorCode.PRICING_MASTER_MINUS45_INVALID);
                ValidateUtil.nonZero(pricingCarrierSlap.getMinus45(), ErrorCode.PRICING_MASTER_MINUS45_NOT_ZERO);

            }

            //ValidateUtil.notNull(pricingCarrierSlap.getPlus45(), ErrorCode.PRICING_MASTER_PLUS45_REQUIRED);
            if (pricingCarrierSlap.getPlus45() != null) {
                regExpService.validate(pricingCarrierSlap.getPlus45(), RegExpName.Reg_Exp_AMOUNT_TEN_SIX, ErrorCode.PRICING_MASTER_PLUS45_INVALID);
                ValidateUtil.nonZero(pricingCarrierSlap.getPlus45(), ErrorCode.PRICING_MASTER_PLUS45_NOT_ZERO);

            }


            //ValidateUtil.notNull(pricingCarrierSlap.getPlus100(), ErrorCode.PRICING_MASTER_PLUS100_REQUIRED);
            if (pricingCarrierSlap.getPlus100() != null) {
                regExpService.validate(pricingCarrierSlap.getPlus100(), RegExpName.Reg_Exp_AMOUNT_TEN_SIX, ErrorCode.PRICING_MASTER_PLUS100_INVALID);
                ValidateUtil.nonZero(pricingCarrierSlap.getPlus100(), ErrorCode.PRICING_MASTER_PLUS100_NOT_ZERO);

            }

            //ValidateUtil.notNull(pricingCarrierSlap.getPlus250(), ErrorCode.PRICING_MASTER_PLUS250_REQUIRED);
            if (pricingCarrierSlap.getPlus250() != null) {
                regExpService.validate(pricingCarrierSlap.getPlus250(), RegExpName.Reg_Exp_AMOUNT_TEN_SIX, ErrorCode.PRICING_MASTER_PLUS250_INVALID);
                ValidateUtil.nonZero(pricingCarrierSlap.getPlus250(), ErrorCode.PRICING_MASTER_PLUS250_NOT_ZERO);

            }

            if (pricingCarrierSlap.getPlus300() != null) {
                regExpService.validate(pricingCarrierSlap.getPlus300(), RegExpName.Reg_Exp_AMOUNT_TEN_SIX, ErrorCode.PRICING_MASTER_PLUS300_INVALID);
                ValidateUtil.nonZero(pricingCarrierSlap.getPlus300(), ErrorCode.PRICING_MASTER_PLUS300_NOT_ZERO);

            }

            //ValidateUtil.notNull(pricingCarrierSlap.getPlus500(), ErrorCode.PRICING_MASTER_PLUS500_REQUIRED);
            if (pricingCarrierSlap.getPlus500() != null) {
                regExpService.validate(pricingCarrierSlap.getPlus500(), RegExpName.Reg_Exp_AMOUNT_TEN_SIX, ErrorCode.PRICING_MASTER_PLUS500_INVALID);
                ValidateUtil.nonZero(pricingCarrierSlap.getPlus500(), ErrorCode.PRICING_MASTER_PLUS500_NOT_ZERO);

            }

            //ValidateUtil.notNull(pricingCarrierSlap.getPlus1000(), ErrorCode.PRICING_MASTER_PLUS100_REQUIRED);
            if (pricingCarrierSlap.getPlus1000() != null) {
                regExpService.validate(pricingCarrierSlap.getPlus1000(), RegExpName.Reg_Exp_AMOUNT_TEN_SIX, ErrorCode.PRICING_MASTER_PLUS1000_INVALID);
                ValidateUtil.nonZero(pricingCarrierSlap.getPlus1000(), ErrorCode.PRICING_MASTER_PLUS1000_NOT_ZERO);

            }

        }
    }
}

