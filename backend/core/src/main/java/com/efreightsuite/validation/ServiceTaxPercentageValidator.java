package com.efreightsuite.validation;

import com.efreightsuite.enumeration.LovStatus;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.model.ServiceTaxPercentage;
import com.efreightsuite.service.RegularExpressionService;
import com.efreightsuite.util.ValidateUtil;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class ServiceTaxPercentageValidator {

    @Autowired
    RegularExpressionService regExpService;


    public void validate(ServiceTaxPercentage serviceTaxPercentage) {

        ValidateUtil.notNull(serviceTaxPercentage.getCurrencyMaster().getId(), ErrorCode.CURRENCYMASTER_CODE_NOT_NULL);
        ValidateUtil.notNull(serviceTaxPercentage.getSubLedgerMaster().getId(), ErrorCode.SUBLEGDER_CODE_NOT_NULL);
        ValidateUtil.notNull(serviceTaxPercentage.getGeneralLedgerMaster().getId(), ErrorCode.GENERALEDGER_CODE_NOT_NULL);


        ValidateUtil.notNull(serviceTaxPercentage.getTaxCode(), ErrorCode.SERVICETAXPERCENTAGE_CODE_NOT_NULL);
        ValidateUtil.notNull(serviceTaxPercentage.getTaxName(), ErrorCode.SERVICETAXPERCENTAGE_NAME_NOT_NULL);
        if (serviceTaxPercentage.getStatus() == null)
            serviceTaxPercentage.setStatus(LovStatus.Active);
        ValidateUtil.validateEnum(LovStatus.class, serviceTaxPercentage.getStatus(), ErrorCode.SERVICETAXPERCENTAGE_STATUS_NOT_NULL);
        ValidateUtil.notNull(serviceTaxPercentage.getValidFrom(), ErrorCode.SERVICETAXPERCENTAGE_VALIDFROM_NOT_NULL);
        ValidateUtil.notNull(serviceTaxPercentage.getPercentage(), ErrorCode.SERVICETAXPERCENTAGE_PERCENTAGE_NOT_NULL);
        ValidateUtil.notNull(serviceTaxPercentage.getCompanyCode(), ErrorCode.SERVICETAXPERCENTAGE_COMPANY_NOT_NULL);
        ValidateUtil.notNull(serviceTaxPercentage.getCountryCode(), ErrorCode.SERVICETAXPERCENTAGE_COUNTRY_NOT_NULL);

        //regExpService.validate(serviceTaxPercentage.getTaxCode(), RegExpName.Reg_Exp_Master_Charge_Code, ErrorCode.CHARGE_CODE_INVALID);
        //ValidateUtil.validateLength(serviceTaxPercentage.getTaxCode(), 1, 10,ErrorCode.CHARGE_CODE_INVALID);


    }


}
