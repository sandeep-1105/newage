package com.efreightsuite.validation;

import com.efreightsuite.enumeration.DocumentType;
import com.efreightsuite.enumeration.LovStatus;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.model.DaybookMaster;
import com.efreightsuite.service.RegularExpressionService;
import com.efreightsuite.util.RegExpName;
import com.efreightsuite.util.ValidateUtil;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Log4j2
@Service
public class DaybookMasterValidator {

    @Autowired
    private
    RegularExpressionService regExpService;

    public void validate(DaybookMaster daybookMaster) {

        log.info("DaybookMasterValidator Started for Daybook : [" + daybookMaster.getId() + "]");

        // Daybook Code Validation

        ValidateUtil.notNull(daybookMaster.getDaybookCode(), ErrorCode.DAYBOOK_CODE_NOT_NULL);
        regExpService.validate(daybookMaster.getDaybookCode(), RegExpName.Reg_Exp_Master_Daybook_Code, ErrorCode.DAYBOOK_CODE_INVALID);

        log.info("Daybook Code validated...");

        // Daybook Name Validations

        ValidateUtil.notNull(daybookMaster.getDaybookName(), ErrorCode.DAYBOOK_NAME_NOT_NULL);

        regExpService.validate(daybookMaster.getDaybookName(), RegExpName.Reg_Exp_Master_Daybook_Name, ErrorCode.DAYBOOK_NAME_INVALID);

        log.info("Daybook Name validated...");

        ValidateUtil.notNull(daybookMaster.getLocationMaster(), ErrorCode.DAYBOOK_LOCATION_NOT_NULL);
        ValidateUtil.isStatusBlocked(daybookMaster.getLocationMaster().getStatus(), ErrorCode.DAYBOOK_LOCATION_BLOCKED);
        ValidateUtil.isStatusHidden(daybookMaster.getLocationMaster().getStatus(), ErrorCode.DAYBOOK_LOCATION_HIDDEN);

        ValidateUtil.notNull(daybookMaster.getDocumentTypeMaster(), ErrorCode.DAYBOOK_DOCUMENTTYPE_NULL);
        ValidateUtil.isStatusBlocked(daybookMaster.getDocumentTypeMaster().getStatus(), ErrorCode.DAYBOOK_DOCUMENTTYPE_BLOCKED);
        ValidateUtil.isStatusHidden(daybookMaster.getDocumentTypeMaster().getStatus(), ErrorCode.DAYBOOK_DOCUMENTTYPE_HIDDEN);

        if (daybookMaster.getDocumentTypeMaster().getDocumentTypeCode().equals(DocumentType.PMT)
                || daybookMaster.getDocumentTypeMaster().getDocumentTypeCode().equals(DocumentType.RPT)) {
            if (daybookMaster.getCashAccount() == null && daybookMaster.getBankAccount() == null && daybookMaster.getPdcAccount() == null) {
                throw new RestException(ErrorCode.ONE_BANK_ACCOUNT_MANDATORY);
            } else {
                if (daybookMaster.getCashAccount() != null || daybookMaster.getBankAccount() != null || daybookMaster.getPdcAccount() != null) {
                    log.info("validation finished for account in Daybook ");
                }
            }
        } else {
            if (daybookMaster.getCashAccount() != null && daybookMaster.getCashAccount().getId() != null) {
                ValidateUtil.isStatusBlocked(daybookMaster.getCashAccount().getStatus(), ErrorCode.DAYBOOK_CASH_ACCOUNT_BLOCKED);
                ValidateUtil.isStatusHidden(daybookMaster.getCashAccount().getStatus(), ErrorCode.DAYBOOK_CASH_ACCOUNT_HIDDEN);
            }
            if (daybookMaster.getBankAccount() != null && daybookMaster.getBankAccount().getId() != null) {
                ValidateUtil.isStatusBlocked(daybookMaster.getBankAccount().getStatus(), ErrorCode.DAYBOOK_BANK_ACCOUNT_BLOCKED);
                ValidateUtil.isStatusHidden(daybookMaster.getBankAccount().getStatus(), ErrorCode.DAYBOOK_BANK_ACCOUNT_HIDDEN);
            }
            if (daybookMaster.getPdcAccount() != null && daybookMaster.getPdcAccount().getId() != null) {
                ValidateUtil.isStatusBlocked(daybookMaster.getPdcAccount().getStatus(), ErrorCode.DAYBOOK_PDC_ACCOUNT_BLOCKED);
                ValidateUtil.isStatusHidden(daybookMaster.getPdcAccount().getStatus(), ErrorCode.DAYBOOK_PDC_ACCOUNT_HIDDEN);
            }
        }

        if (daybookMaster.getExchangeGainAccount() != null && daybookMaster.getExchangeGainAccount().getId() != null) {

            ValidateUtil.isStatusBlocked(daybookMaster.getExchangeGainAccount().getStatus(), ErrorCode.DAYBOOK_EXCHANGE_GAIN_ACCOUNT_BLOCKED);
            ValidateUtil.isStatusHidden(daybookMaster.getExchangeGainAccount().getStatus(), ErrorCode.DAYBOOK_EXCHANGE_GAIN_PDC_ACCOUNT_HIDDEN);

        }

        if (daybookMaster.getExchangeLossAccount() != null && daybookMaster.getExchangeLossAccount().getId() != null) {

            ValidateUtil.isStatusBlocked(daybookMaster.getExchangeLossAccount().getStatus(), ErrorCode.DAYBOOK_EXCHANGE_LOSS_ACCOUNT_BLOCKED);
            ValidateUtil.isStatusHidden(daybookMaster.getExchangeLossAccount().getStatus(), ErrorCode.DAYBOOK_EXCHANGE_LOSS_ACCOUNT_HIDDEN);

        }
        if (daybookMaster.getChequeNoFrom() != null && !daybookMaster.getChequeNoFrom().isEmpty()) {
            regExpService.validate(daybookMaster.getChequeNoFrom(), RegExpName.Reg_Exp_Master_Daybook_Cheque_From, ErrorCode.DAYBOOK_CHEQUENOFROM_INVALID);
        }
        if (daybookMaster.getChequeNoTo() != null && !daybookMaster.getChequeNoTo().isEmpty()) {
            regExpService.validate(daybookMaster.getChequeNoTo(), RegExpName.Reg_Exp_Master_Daybook_Cheque_To, ErrorCode.DAYBOOK_CHEQUENOTO_INVALID);
        }
        if (daybookMaster.getChequeReportName() != null && !daybookMaster.getChequeReportName().isEmpty()) {
            regExpService.validate(daybookMaster.getChequeReportName(), RegExpName.Reg_Exp_Master_Daybook_Report_Name, ErrorCode.DAYBOOK_CHEQUEREPORTNAME_INVALID);
        }

		/*if (daybookMaster.getNumericDaybook() != null && daybookMaster.getNumericDaybook().getId() != null) {

			ValidateUtil.isStatusBlocked(daybookMaster.getNumericDaybook().getStatus(), ErrorCode.DAYBOOK_BLOCKED);
			ValidateUtil.isStatusHidden(daybookMaster.getNumericDaybook().getStatus(), ErrorCode.DAYBOOK_HIDDEN);

		}*/
        ValidateUtil.notNull(daybookMaster.getStatus(), ErrorCode.DAYBOOK_LOV_STATUS_NOT_NULL);

        ValidateUtil.validateEnum(LovStatus.class, daybookMaster.getStatus(), ErrorCode.DAYBOOK_LOV_STATUS_INVALID);

        log.info("Daybook Status Successfully Validated.....");

        log.info("DaybookMasterValidator Finished for Daybook : [" + daybookMaster.getId() + "]");
    }
}
