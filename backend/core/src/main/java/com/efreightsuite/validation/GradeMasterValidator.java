package com.efreightsuite.validation;

import com.efreightsuite.model.GradeMaster;
import lombok.extern.log4j.Log4j2;

import org.springframework.stereotype.Service;

@Service
@Log4j2
public class GradeMasterValidator {

    public void validate(GradeMaster gradeMaster) {
        log.info("validation start :", gradeMaster.getId());
        if (gradeMaster.getColorCode() == null) {
            gradeMaster.setColorCode("#000000");
        }


        log.info("validation Finished :", gradeMaster.getId());

    }


}
