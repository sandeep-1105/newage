package com.efreightsuite.validation;

import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.model.CountryReportConfigMaster;
import com.efreightsuite.model.PageMaster;
import com.efreightsuite.service.RegularExpressionService;
import com.efreightsuite.util.ValidateUtil;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class CountryReportConfigValidator {


    @Autowired
    RegularExpressionService regExpService;

    public void validate(CountryReportConfigMaster pageCountryMaster) {

        log.info("In pageCountryValidatory" + pageCountryMaster);

        ValidateUtil.notNull(pageCountryMaster.getCountryMaster(), ErrorCode.COUNTRYREPORT_COUNTRY_NOT_NULL);

        ValidateUtil.notNull(pageCountryMaster.getReportName(), ErrorCode.COUNTRYREPORT_NAME_NOT_NULL);

        ValidateUtil.notNull(pageCountryMaster.getCopies(), ErrorCode.COUNTRYREPORT_COPIES_NOT_NULL);

        for (PageMaster pageMaster : pageCountryMaster.getPageMasterList()) {
            ValidateUtil.notNull(pageMaster.getPageName(), ErrorCode.PAGE_NAME_NOT_NULL);
            ValidateUtil.notNull(pageMaster.getPageCode(), ErrorCode.PAGE_CODE_NOT_NULL);
        }

        //ValidateUtil.notNull(pageCountryMaster.getPageMasterList(), ErrorCode.SERVICE_NAME_NOT_NULL);

    }

}
