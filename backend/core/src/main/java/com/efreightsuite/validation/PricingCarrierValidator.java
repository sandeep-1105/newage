package com.efreightsuite.validation;

import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.model.PricingCarrier;
import com.efreightsuite.service.AuthService;
import com.efreightsuite.service.RegularExpressionService;
import com.efreightsuite.util.RegExpName;
import com.efreightsuite.util.ValidateUtil;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class PricingCarrierValidator {

    @Autowired
    private
    RegularExpressionService regExpService;


    public void validate(PricingCarrier pricingCarrier) {

        pricingCarrier.setCompanyMaster(AuthService.getCurrentUser().getSelectedUserLocation().getCompanyMaster());
        pricingCarrier.setCompanyCode(AuthService.getCurrentUser().getSelectedUserLocation().getCompanyMaster().getCompanyCode());
        pricingCarrier.setLocationMaster(AuthService.getCurrentUser().getSelectedUserLocation());
        pricingCarrier.setLocationCode(AuthService.getCurrentUser().getSelectedUserLocation().getLocationCode());
        pricingCarrier.setCountryMaster(AuthService.getCurrentUser().getSelectedUserLocation().getCountryMaster());
        pricingCarrier.setCountryCode(AuthService.getCurrentUser().getSelectedUserLocation().getCountryMaster().getCountryCode());


        ValidateUtil.notNull(pricingCarrier.getChargeMaster(), ErrorCode.PRICING_MASTER_CHARGE_REQUIRED);
        ValidateUtil.notNull(pricingCarrier.getChargeMaster().getId(), ErrorCode.PRICING_MASTER_CHARGE_REQUIRED);
        ValidateUtil.isStatusBlocked(pricingCarrier.getChargeMaster().getStatus(), ErrorCode.PRICING_MASTER_CHARGE_BLOCKED);
        ValidateUtil.isStatusHidden(pricingCarrier.getChargeMaster().getStatus(), ErrorCode.PRICING_MASTER_CHARGE_HIDDEN);
        pricingCarrier.setChargeCode(pricingCarrier.getChargeMaster().getChargeCode());


        ValidateUtil.notNull(pricingCarrier.getCurrencyMaster(), ErrorCode.PRICING_MASTER_CURRENCY_REQUIRED);
        ValidateUtil.notNull(pricingCarrier.getCurrencyMaster().getId(), ErrorCode.PRICING_MASTER_CURRENCY_REQUIRED);
        ValidateUtil.isStatusBlocked(pricingCarrier.getCurrencyMaster().getStatus(), ErrorCode.PRICING_MASTER_CURRENCY_BLOCKED);
        ValidateUtil.isStatusHidden(pricingCarrier.getCurrencyMaster().getStatus(), ErrorCode.PRICING_MASTER_CURRENCY_HIDDEN);
        pricingCarrier.setCurrencyCode(pricingCarrier.getCurrencyMaster().getCountryCode());


        ValidateUtil.notNull(pricingCarrier.getCarrierMaster(), ErrorCode.PRICING_MASTER_CARRIER_REQUIRED);
        ValidateUtil.notNull(pricingCarrier.getCarrierMaster().getId(), ErrorCode.PRICING_MASTER_CARRIER_REQUIRED);
        ValidateUtil.isStatusBlocked(pricingCarrier.getCarrierMaster().getStatus(), ErrorCode.PRICING_MASTER_CARRIER_BLOCKED);
        ValidateUtil.isStatusHidden(pricingCarrier.getCarrierMaster().getStatus(), ErrorCode.PRICING_MASTER_CARRIER_HIDDEN);
        pricingCarrier.setCarrierCode(pricingCarrier.getCarrierMaster().getCarrierCode());


        ValidateUtil.notNull(pricingCarrier.getValidFrom(), ErrorCode.PRICING_MASTER_FROMDATE_REQUIRED);
        ValidateUtil.notNull(pricingCarrier.getValidTo(), ErrorCode.PRICING_MASTER_TODATE_REQUIRED);


        if (pricingCarrier.getFuelSurcharge() != null) {
            regExpService.validate(pricingCarrier.getFuelSurcharge(), RegExpName.Reg_Exp_AMOUNT_TEN_SIX, ErrorCode.PRICING_MASTER_FUEL_SURCHARGE_INVALID);
            ValidateUtil.nonZero(pricingCarrier.getFuelSurcharge(), ErrorCode.PRICING_MASTER_FUEL_SURCHARGE_NOT_ZERO);
        }

        if (pricingCarrier.getSecuritySurcharge() != null) {
            regExpService.validate(pricingCarrier.getSecuritySurcharge(), RegExpName.Reg_Exp_AMOUNT_TEN_SIX, ErrorCode.PRICING_MASTER_SECURITY_SURCHARGE_INVALID);
            ValidateUtil.nonZero(pricingCarrier.getSecuritySurcharge(), ErrorCode.PRICING_MASTER_SECURITY_SURCHARGE_NOT_ZERO);
        }

        if (pricingCarrier.getStandardSellPrice() != null) {

            if (pricingCarrier.getStandardSellPrice().getMinimum() != null) {
                regExpService.validate(pricingCarrier.getStandardSellPrice().getMinimum(), RegExpName.Reg_Exp_AMOUNT_TEN_SIX, ErrorCode.PRICING_MASTER_STDSELIN_MINIMUM_INVALID);
                ValidateUtil.nonZero(pricingCarrier.getStandardSellPrice().getMinimum(), ErrorCode.PRICING_MASTER_STDSELIN_MINIMUM_NOT_ZERO);
            }

            if (pricingCarrier.getStandardSellPrice().getMinus45() != null) {
                regExpService.validate(pricingCarrier.getStandardSellPrice().getMinus45(), RegExpName.Reg_Exp_AMOUNT_TEN_SIX, ErrorCode.PRICING_MASTER_STDSEL_MINUS45_INVALID);
                ValidateUtil.nonZero(pricingCarrier.getStandardSellPrice().getMinus45(), ErrorCode.PRICING_MASTER_MINUS45_NOT_ZERO);
            }

            if (pricingCarrier.getStandardSellPrice().getPlus45() != null) {
                regExpService.validate(pricingCarrier.getStandardSellPrice().getPlus45(), RegExpName.Reg_Exp_AMOUNT_TEN_SIX, ErrorCode.PRICING_MASTER_STDSEL_PLUS45_INVALID);
                ValidateUtil.nonZero(pricingCarrier.getStandardSellPrice().getPlus45(), ErrorCode.PRICING_MASTER_MINUS45_NOT_ZERO);
            }

            if (pricingCarrier.getStandardSellPrice().getPlus100() != null) {
                regExpService.validate(pricingCarrier.getStandardSellPrice().getPlus100(), RegExpName.Reg_Exp_AMOUNT_TEN_SIX, ErrorCode.PRICING_MASTER_STDSEL_PLUS100_INVALID);
                ValidateUtil.nonZero(pricingCarrier.getStandardSellPrice().getPlus100(), ErrorCode.PRICING_MASTER_PLUS100_NOT_ZERO);
            }

            if (pricingCarrier.getStandardSellPrice().getPlus250() != null) {
                regExpService.validate(pricingCarrier.getStandardSellPrice().getPlus250(), RegExpName.Reg_Exp_AMOUNT_TEN_SIX, ErrorCode.PRICING_MASTER_STDSEL_PLUS250_INVALID);
                ValidateUtil.nonZero(pricingCarrier.getStandardSellPrice().getPlus250(), ErrorCode.PRICING_MASTER_PLUS250_NOT_ZERO);
            }

            if (pricingCarrier.getStandardSellPrice().getPlus300() != null) {
                regExpService.validate(pricingCarrier.getStandardSellPrice().getPlus300(), RegExpName.Reg_Exp_AMOUNT_TEN_SIX, ErrorCode.PRICING_MASTER_STDSEL_PLUS300_INVALID);
                ValidateUtil.nonZero(pricingCarrier.getStandardSellPrice().getPlus300(), ErrorCode.PRICING_MASTER_PLUS300_NOT_ZERO);
            }

            if (pricingCarrier.getStandardSellPrice().getPlus500() != null) {
                regExpService.validate(pricingCarrier.getStandardSellPrice().getPlus500(), RegExpName.Reg_Exp_AMOUNT_TEN_SIX, ErrorCode.PRICING_MASTER_STDSEL_PLUS500_INVALID);
                ValidateUtil.nonZero(pricingCarrier.getStandardSellPrice().getPlus500(), ErrorCode.PRICING_MASTER_PLUS500_NOT_ZERO);
            }

            if (pricingCarrier.getStandardSellPrice().getPlus1000() != null) {
                regExpService.validate(pricingCarrier.getStandardSellPrice().getPlus1000(), RegExpName.Reg_Exp_AMOUNT_TEN_SIX, ErrorCode.PRICING_MASTER_STDSEL_PLUS1000_INVALID);
                ValidateUtil.nonZero(pricingCarrier.getStandardSellPrice().getPlus1000(), ErrorCode.PRICING_MASTER_PLUS1000_NOT_ZERO);
            }
        }

        if (pricingCarrier.getMinimumSellPrice() != null) {

            if (pricingCarrier.getMinimumSellPrice().getMinimum() != null) {
                regExpService.validate(pricingCarrier.getMinimumSellPrice().getMinimum(), RegExpName.Reg_Exp_AMOUNT_TEN_SIX, ErrorCode.PRICING_MASTER_MINSELIN_MINIMUM_INVALID);
                ValidateUtil.nonZero(pricingCarrier.getMinimumSellPrice().getMinimum(), ErrorCode.PRICING_MASTER_MINSELIN_MINIMUM_NOT_ZERO);
            }

            if (pricingCarrier.getMinimumSellPrice().getMinus45() != null) {
                regExpService.validate(pricingCarrier.getMinimumSellPrice().getMinus45(), RegExpName.Reg_Exp_AMOUNT_TEN_SIX, ErrorCode.PRICING_MASTER_MINSEL_MINUS45_INVALID);
                ValidateUtil.nonZero(pricingCarrier.getMinimumSellPrice().getMinus45(), ErrorCode.PRICING_MASTER_MINUS45_NOT_ZERO);
            }

            if (pricingCarrier.getMinimumSellPrice().getPlus45() != null) {
                regExpService.validate(pricingCarrier.getMinimumSellPrice().getPlus45(), RegExpName.Reg_Exp_AMOUNT_TEN_SIX, ErrorCode.PRICING_MASTER_MINSEL_PLUS45_INVALID);
                ValidateUtil.nonZero(pricingCarrier.getMinimumSellPrice().getPlus45(), ErrorCode.PRICING_MASTER_MINUS45_NOT_ZERO);
            }

            if (pricingCarrier.getMinimumSellPrice().getPlus100() != null) {
                regExpService.validate(pricingCarrier.getMinimumSellPrice().getPlus100(), RegExpName.Reg_Exp_AMOUNT_TEN_SIX, ErrorCode.PRICING_MASTER_MINSEL_PLUS100_INVALID);
                ValidateUtil.nonZero(pricingCarrier.getMinimumSellPrice().getPlus100(), ErrorCode.PRICING_MASTER_PLUS100_NOT_ZERO);
            }

            if (pricingCarrier.getMinimumSellPrice().getPlus250() != null) {
                regExpService.validate(pricingCarrier.getMinimumSellPrice().getPlus250(), RegExpName.Reg_Exp_AMOUNT_TEN_SIX, ErrorCode.PRICING_MASTER_MINSEL_PLUS250_INVALID);
                ValidateUtil.nonZero(pricingCarrier.getMinimumSellPrice().getPlus250(), ErrorCode.PRICING_MASTER_PLUS250_NOT_ZERO);
            }

            if (pricingCarrier.getMinimumSellPrice().getPlus300() != null) {
                regExpService.validate(pricingCarrier.getMinimumSellPrice().getPlus300(), RegExpName.Reg_Exp_AMOUNT_TEN_SIX, ErrorCode.PRICING_MASTER_MINSEL_PLUS300_INVALID);
                ValidateUtil.nonZero(pricingCarrier.getMinimumSellPrice().getPlus300(), ErrorCode.PRICING_MASTER_PLUS300_NOT_ZERO);
            }

            if (pricingCarrier.getMinimumSellPrice().getPlus500() != null) {
                regExpService.validate(pricingCarrier.getMinimumSellPrice().getPlus500(), RegExpName.Reg_Exp_AMOUNT_TEN_SIX, ErrorCode.PRICING_MASTER_MINSEL_PLUS500_INVALID);
                ValidateUtil.nonZero(pricingCarrier.getMinimumSellPrice().getPlus500(), ErrorCode.PRICING_MASTER_PLUS500_NOT_ZERO);
            }

            if (pricingCarrier.getMinimumSellPrice().getPlus1000() != null) {
                regExpService.validate(pricingCarrier.getMinimumSellPrice().getPlus1000(), RegExpName.Reg_Exp_AMOUNT_TEN_SIX, ErrorCode.PRICING_MASTER_MINSEL_PLUS1000_INVALID);
                ValidateUtil.nonZero(pricingCarrier.getMinimumSellPrice().getPlus1000(), ErrorCode.PRICING_MASTER_PLUS1000_NOT_ZERO);
            }
        }

        if (pricingCarrier.getCost() != null) {

            if (pricingCarrier.getCost().getMinimum() != null) {
                regExpService.validate(pricingCarrier.getCost().getMinimum(), RegExpName.Reg_Exp_AMOUNT_TEN_SIX, ErrorCode.PRICING_MASTER_MINSELIN_MINIMUM_INVALID);
                ValidateUtil.nonZero(pricingCarrier.getCost().getMinimum(), ErrorCode.PRICING_MASTER_MINSELIN_MINIMUM_NOT_ZERO);
            }

            if (pricingCarrier.getCost().getMinus45() != null) {
                regExpService.validate(pricingCarrier.getCost().getMinus45(), RegExpName.Reg_Exp_AMOUNT_TEN_SIX, ErrorCode.PRICING_MASTER_COST_MINUS45_INVALID);
                ValidateUtil.nonZero(pricingCarrier.getCost().getMinus45(), ErrorCode.PRICING_MASTER_MINUS45_NOT_ZERO);
            }

            if (pricingCarrier.getCost().getPlus45() != null) {
                regExpService.validate(pricingCarrier.getCost().getPlus45(), RegExpName.Reg_Exp_AMOUNT_TEN_SIX, ErrorCode.PRICING_MASTER_COST_PLUS45_INVALID);
                ValidateUtil.nonZero(pricingCarrier.getCost().getPlus45(), ErrorCode.PRICING_MASTER_MINUS45_NOT_ZERO);
            }

            if (pricingCarrier.getCost().getPlus100() != null) {
                regExpService.validate(pricingCarrier.getCost().getPlus100(), RegExpName.Reg_Exp_AMOUNT_TEN_SIX, ErrorCode.PRICING_MASTER_COST_PLUS100_INVALID);
                ValidateUtil.nonZero(pricingCarrier.getCost().getPlus100(), ErrorCode.PRICING_MASTER_PLUS100_NOT_ZERO);
            }

            if (pricingCarrier.getCost().getPlus250() != null) {
                regExpService.validate(pricingCarrier.getCost().getPlus250(), RegExpName.Reg_Exp_AMOUNT_TEN_SIX, ErrorCode.PRICING_MASTER_COST_PLUS250_INVALID);
                ValidateUtil.nonZero(pricingCarrier.getCost().getPlus250(), ErrorCode.PRICING_MASTER_PLUS250_NOT_ZERO);
            }

            if (pricingCarrier.getCost().getPlus300() != null) {
                regExpService.validate(pricingCarrier.getCost().getPlus300(), RegExpName.Reg_Exp_AMOUNT_TEN_SIX, ErrorCode.PRICING_MASTER_COST_PLUS300_INVALID);
                ValidateUtil.nonZero(pricingCarrier.getCost().getPlus300(), ErrorCode.PRICING_MASTER_PLUS300_NOT_ZERO);
            }

            if (pricingCarrier.getCost().getPlus500() != null) {
                regExpService.validate(pricingCarrier.getCost().getPlus500(), RegExpName.Reg_Exp_AMOUNT_TEN_SIX, ErrorCode.PRICING_MASTER_COST_PLUS500_INVALID);
                ValidateUtil.nonZero(pricingCarrier.getCost().getPlus500(), ErrorCode.PRICING_MASTER_PLUS500_NOT_ZERO);
            }

            if (pricingCarrier.getCost().getPlus1000() != null) {
                regExpService.validate(pricingCarrier.getCost().getPlus1000(), RegExpName.Reg_Exp_AMOUNT_TEN_SIX, ErrorCode.PRICING_MASTER_COST_PLUS1000_INVALID);
                ValidateUtil.nonZero(pricingCarrier.getCost().getPlus1000(), ErrorCode.PRICING_MASTER_PLUS1000_NOT_ZERO);
            }
        }

       /*
        ValidateUtil.notNull(pricingCarrier.getMinus45(), ErrorCode.PRICING_MASTER_MINUS45_REQUIRED);
        ValidateUtil.notNull(pricingCarrier.getPlus45(), ErrorCode.PRICING_MASTER_PLUS45_REQUIRED);
        ValidateUtil.notNull(pricingCarrier.getPlus100(), ErrorCode.PRICING_MASTER_PLUS100_REQUIRED);
        ValidateUtil.notNull(pricingCarrier.getPlus250(), ErrorCode.PRICING_MASTER_PLUS250_REQUIRED);    
        ValidateUtil.notNull(pricingCarrier.getPlus500(), ErrorCode.PRICING_MASTER_PLUS500_REQUIRED);
        ValidateUtil.notNull(pricingCarrier.getFuelSurcharge(), ErrorCode.PRICING_MASTER_FUELSURCHARGE_REQUIRED);
		ValidateUtil.notNull(pricingCarrier.getSecuritySurcharge(), ErrorCode.PRICING_MASTER_SECURITY_SURCHARGE_REQUIRED);
        ValidateUtil.notNull(pricingCarrier.getPlus1000(), ErrorCode.PRICING_MASTER_PLUS100_REQUIRED);  
       ValidateUtil.notNull(pricingCarrier.getFuelValidFrom(),ErrorCode.PRICING_MASTER_FUEL_VALIDFROM_REQUIRED);
       ValidateUtil.notNull(pricingCarrier.getFuelValidTo(), ErrorCode.PRICING_MASTER_FUEL_VALIDTO_REQUIRED);
       ValidateUtil.notNull(pricingCarrier.getSecurityChargeValidFrom(), ErrorCode.PRICING_MASTER_SECURITY_VALIDFROM_REQUIRED);
       ValidateUtil.notNull(pricingCarrier.getSecurityChargeValidTo(), ErrorCode.PRICING_MASTER_SECURITY_VALIDTO_REQUIRED);*/

    }

}
