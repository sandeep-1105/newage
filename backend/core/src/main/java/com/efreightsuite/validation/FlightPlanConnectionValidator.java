package com.efreightsuite.validation;

import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.model.FlightPlanConnection;
import com.efreightsuite.util.ValidateUtil;
import lombok.extern.log4j.Log4j2;

import org.springframework.stereotype.Service;

@Service
@Log4j2
public class FlightPlanConnectionValidator {

    public void validate(FlightPlanConnection flightPlanConnection) {

        verifyCarrierMaster(flightPlanConnection);

        verifyFlightNo(flightPlanConnection);

        verifyPol(flightPlanConnection);

        verifyPod(flightPlanConnection);

        verifyEtd(flightPlanConnection);

        verifyEta(flightPlanConnection);

    }

    private void verifyCarrierMaster(FlightPlanConnection flightPlanConnection) {

        ValidateUtil.notNull(flightPlanConnection.getCarrierMaster(), ErrorCode.FLIGHTPLAN_CONNECTION_CARRIER_MANDATORY);

        ValidateUtil.notNull(flightPlanConnection.getCarrierMaster().getId(), ErrorCode.FLIGHTPLAN_CONNECTION_CARRIER_MANDATORY);

        ValidateUtil.isStatusBlocked(flightPlanConnection.getCarrierMaster().getStatus(), ErrorCode.FLIGHTPLAN_CONNECTION_CARRIER_BLOCKED);

        ValidateUtil.isStatusHidden(flightPlanConnection.getCarrierMaster().getStatus(), ErrorCode.FLIGHTPLAN_CONNECTION_CARRIER_HIDDEN);

        log.info(flightPlanConnection.getCarrierMaster().getCarrierName() + " is validated successfully.");
    }

    private void verifyFlightNo(FlightPlanConnection flightPlanConnection) {

        ValidateUtil.notNullOrEmpty(flightPlanConnection.getFlightNo(), ErrorCode.FLIGHTPLAN_CONNECTION_FLIGHT_NO_REQUIRED);

        ValidateUtil.checkPattern(flightPlanConnection.getFlightNo(), "flightPlan.flightNo", true);

        log.info(flightPlanConnection.getFlightNo() + " is validated successfully.");
    }

    private void verifyPol(FlightPlanConnection flightPlanConnection) {

        ValidateUtil.notNull(flightPlanConnection.getPol(), ErrorCode.FLIGHTPLAN_CONNECTION_POL_MANDATORY);

        ValidateUtil.notNull(flightPlanConnection.getPol().getId(), ErrorCode.FLIGHTPLAN_CONNECTION_POL_MANDATORY);

        ValidateUtil.isStatusBlocked(flightPlanConnection.getPol().getStatus(), ErrorCode.FLIGHTPLAN_CONNECTION_POL_BLOCKED);

        ValidateUtil.isStatusHidden(flightPlanConnection.getPol().getStatus(), ErrorCode.FLIGHTPLAN_CONNECTION_POL_HIDDEN);

        if (flightPlanConnection.getPod() != null && flightPlanConnection.getPod().getId() != null) {
            if (flightPlanConnection.getPol().getPortCode().equals(flightPlanConnection.getPod().getPortCode())) {
                throw new RestException(ErrorCode.FLIGHTPLAN_CONNECTION_POL_POD_SAME);
            }
        }

        log.info(flightPlanConnection.getPol().getPortName() + " is validated successfully.");
    }

    private void verifyPod(FlightPlanConnection flightPlanConnection) {

        ValidateUtil.notNull(flightPlanConnection.getPod(), ErrorCode.FLIGHTPLAN_CONNECTION_POD_MANDATORY);

        ValidateUtil.notNull(flightPlanConnection.getPod().getId(), ErrorCode.FLIGHTPLAN_CONNECTION_POD_MANDATORY);

        ValidateUtil.isStatusBlocked(flightPlanConnection.getPod().getStatus(), ErrorCode.FLIGHTPLAN_CONNECTION_POD_BLOCKED);

        ValidateUtil.isStatusHidden(flightPlanConnection.getPod().getStatus(), ErrorCode.FLIGHTPLAN_CONNECTION_POD_HIDDEN);

        if (flightPlanConnection.getPol() != null && flightPlanConnection.getPol().getId() != null) {
            if (flightPlanConnection.getPod().getPortCode().equals(flightPlanConnection.getPol().getPortCode())) {
                throw new RestException(ErrorCode.FLIGHTPLAN_CONNECTION_POL_POD_SAME);
            }
        }

        log.info(flightPlanConnection.getPod().getPortName() + " is validated successfully.");
    }

    private void verifyEtd(FlightPlanConnection flightPlanConnection) {
        ValidateUtil.notNull(flightPlanConnection.getEtd(), ErrorCode.FLIGHTPLAN_CONNECTION_ETD_MANDATORY);

        if (flightPlanConnection.getEta() != null) {
            if (flightPlanConnection.getEtd().after(flightPlanConnection.getEta())) {
                throw new RestException(ErrorCode.FLIGHTPLAN_CONNECTION_ETD_ETA_GREATER);
            }
        }
    }

    private void verifyEta(FlightPlanConnection flightPlanConnection) {
        ValidateUtil.notNull(flightPlanConnection.getEta(), ErrorCode.FLIGHTPLAN_CONNECTION_ETA_MANDATORY);

        if (flightPlanConnection.getEtd() != null) {
            if (flightPlanConnection.getEtd().after(flightPlanConnection.getEta())) {
                throw new RestException(ErrorCode.FLIGHTPLAN_CONNECTION_ETD_ETA_GREATER);
            }
        }
    }

}
