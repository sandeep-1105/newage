package com.efreightsuite.validation;

import com.efreightsuite.enumeration.LovStatus;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.model.PartyServiceMaster;
import com.efreightsuite.util.ValidateUtil;
import lombok.extern.log4j.Log4j2;

import org.springframework.stereotype.Service;

/**
 * InterfaceName                  Purpose                               CreatedBy/ModifiedBy      Date          Version
 * <p>
 * PartyServiceMasterValidator   Validator for PartyServiceMaster           Devendrachary M        25-02-15       0.0
 */
@Service
@Log4j2
public class PartyServiceMasterValidator {


    public void validate(PartyServiceMaster partyServiceMaster) {
        ValidateUtil.notNull(partyServiceMaster, ErrorCode.PARTY_SERVICE_REQUIRED);

        log.info("PartyServiceMaster Validation Started....["
                + (partyServiceMaster.getId() != null ? partyServiceMaster.getId() : "") + "]");


        validateServiceMaster(partyServiceMaster);
        validateTosMaster(partyServiceMaster);
        validateLocationMaster(partyServiceMaster);
        validateSalesman(partyServiceMaster);
        validateCustomerService(partyServiceMaster);
        log.info("PartyServiceMaster Validation Finished....["
                + (partyServiceMaster.getId() != null ? partyServiceMaster.getId() : "") + "]");
    }


    //Service Master Validation
    private void validateServiceMaster(PartyServiceMaster partyServiceMaster) {

        ValidateUtil.notNull(partyServiceMaster, ErrorCode.PARTY_SERVICE_INVALID);

        ValidateUtil.notNull(partyServiceMaster.getServiceMaster(), ErrorCode.PARTY_SERVICE_REQUIRED);

        ValidateUtil.notNull(partyServiceMaster.getServiceMaster().getId(), ErrorCode.PARTY_SERVICE_REQUIRED);
        if (partyServiceMaster.getServiceMaster().getStatus().equals(LovStatus.Block)) {
            throw new RestException(ErrorCode.SERVICE_STATUS_BLOCK);
        } else if (partyServiceMaster.getServiceMaster().getStatus().equals(LovStatus.Hide)) {
            throw new RestException(ErrorCode.SERVICE_STATUS_HIDE);
        }

        partyServiceMaster.setServiceCode(partyServiceMaster.getServiceMaster().getServiceCode());
    }

    //Tos Master validation
    private void validateTosMaster(PartyServiceMaster partyServiceMaster) {

        if (partyServiceMaster.getTosMaster() == null || (partyServiceMaster.getTosMaster() != null && partyServiceMaster.getTosMaster().getId() == null)) {
            partyServiceMaster.setTosMaster(null);
            partyServiceMaster.setTosCode(null);
        } else {
            if (partyServiceMaster.getTosMaster().getStatus().equals(LovStatus.Block)) {
                throw new RestException(ErrorCode.TOS_STATUS_BLOCK);
            } else if (partyServiceMaster.getTosMaster().getStatus().equals(LovStatus.Hide)) {
                throw new RestException(ErrorCode.TOS_STATUS_HIDE);
            }

            partyServiceMaster.setTosCode(partyServiceMaster.getTosMaster().getTosCode());
        }

    }

    //Location Master validation
    private void validateLocationMaster(PartyServiceMaster partyServiceMaster) {

        ValidateUtil.notNull(partyServiceMaster.getLocationMaster().getId(), ErrorCode.PARTY_SERVICE_LOCATION_REQUIRED);
        partyServiceMaster.setLocationCode(partyServiceMaster.getLocationMaster().getLocationCode());
    }

    //Salesman validation
    private void validateSalesman(PartyServiceMaster partyServiceMaster) {

        ValidateUtil.notNull(partyServiceMaster.getSalesman().getId(), ErrorCode.PARTY_SERVICE_SALESMAN_REQUIRED);
        partyServiceMaster.setSalesmanCode(partyServiceMaster.getSalesman().getEmployeeCode());
    }

    //Customer Service validation
    private void validateCustomerService(PartyServiceMaster partyServiceMaster) {

        if (partyServiceMaster.getCustomerService() == null || (partyServiceMaster.getCustomerService() != null && partyServiceMaster.getCustomerService().getId() == null)) {
            partyServiceMaster.setCustomerService(null);
            partyServiceMaster.setCustomerServiceCode(null);
        } else {
            partyServiceMaster.setCustomerServiceCode(partyServiceMaster.getCustomerService().getEmployeeCode());
        }
    }


}
