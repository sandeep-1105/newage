package com.efreightsuite.validation;

import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.model.CfsDimension;
import com.efreightsuite.util.ValidateUtil;
import lombok.extern.log4j.Log4j2;

import org.springframework.stereotype.Service;

/*
 * Note - Quotation Dimension Error Codes are used since the dimension Table is same...
 * */
@Service
@Log4j2
public class CfsDimensionValidator {
    public void validate(CfsDimension cfsDimension) {

        log.info("CFS Dimension Validation Started......");
        verifyPieces(cfsDimension);
        verifyLength(cfsDimension);
        verifyWidth(cfsDimension);
        verifyHeight(cfsDimension);
        verifyGrossWeight(cfsDimension);
        log.info("CFS Dimension Validation Finished......");
    }

    private void verifyPieces(CfsDimension cfsDimension) {

        ValidateUtil.notNull(cfsDimension.getNoOfPiece(), ErrorCode.QUOTATION_DIMENSION_PIECES_REQUIRED);

        ValidateUtil.belowRange(cfsDimension.getNoOfPiece(), 0d, ErrorCode.QUOTATION_DIMENSION_PIECES_NEGATIVE);

        ValidateUtil.aboveRange(cfsDimension.getNoOfPiece(), 1000000000d, ErrorCode.QUOTATION_DIMENSION_PIECES_NEGATIVE);

        log.info("Dimension No. Of Pieces Validated...............[" + cfsDimension.getNoOfPiece() + "]");
    }

    private void verifyLength(CfsDimension cfsDimension) {

        ValidateUtil.notNull(cfsDimension.getLength(), ErrorCode.QUOTATION_DIMENSION_LENGTH_REQUIRED);

        ValidateUtil.belowRange(cfsDimension.getLength(), 0d, ErrorCode.QUOTATION_DIMENSION_LENGTH_NEGATIVE);

        ValidateUtil.aboveRange(cfsDimension.getLength(), 100000d, ErrorCode.QUOTATION_DIMENSION_LENGTH_NEGATIVE);

        log.info("Dimension Length Validated...............[" + cfsDimension.getLength() + "]");
    }

    private void verifyWidth(CfsDimension cfsDimension) {

        ValidateUtil.notNull(cfsDimension.getWidth(), ErrorCode.QUOTATION_DIMENSION_WIDTH_REQUIRED);

        ValidateUtil.belowRange(cfsDimension.getWidth(), 0d, ErrorCode.QUOTATION_DIMENSION_WIDTH_NEGATIVE);

        ValidateUtil.aboveRange(cfsDimension.getWidth(), 100000d, ErrorCode.QUOTATION_DIMENSION_WIDTH_NEGATIVE);

        log.info("Dimension Width Validated...............[" + cfsDimension.getWidth() + "]");
    }

    private void verifyHeight(CfsDimension cfsDimension) {

        ValidateUtil.notNull(cfsDimension.getHeight(), ErrorCode.QUOTATION_DIMENSION_HEIGHT_REQUIRED);

        ValidateUtil.belowRange(cfsDimension.getHeight(), 0, ErrorCode.QUOTATION_DIMENSION_HEIGHT_NEGATIVE);

        ValidateUtil.aboveRange(cfsDimension.getHeight(), 100000d, ErrorCode.QUOTATION_DIMENSION_HEIGHT_NEGATIVE);

        log.info("Dimension Height Validated...............[" + cfsDimension.getHeight() + "]");
    }

    private void verifyGrossWeight(CfsDimension cfsDimension) {

        ValidateUtil.notNull(cfsDimension.getGrossWeight(), ErrorCode.QUOTATION_DIMENSION_GROSS_WEIGHT_REQUIRED);

        ValidateUtil.belowRange(cfsDimension.getGrossWeight(), 0, ErrorCode.QUOTATION_DIMENSION_GROSS_WEIGHT_NEGATIVE);

        ValidateUtil.aboveRange(cfsDimension.getGrossWeight(), 1000000000d, ErrorCode.QUOTATION_DIMENSION_GROSS_WEIGHT_NEGATIVE);

        log.info("Dimension GrossWeight Validated ...............[" + cfsDimension.getGrossWeight() + "]");
    }


}
