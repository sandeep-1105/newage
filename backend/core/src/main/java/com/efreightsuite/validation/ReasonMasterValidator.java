package com.efreightsuite.validation;

import com.efreightsuite.enumeration.LovStatus;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.model.ReasonMaster;
import com.efreightsuite.util.ValidateUtil;
import lombok.extern.log4j.Log4j2;

import org.springframework.stereotype.Service;

@Log4j2
@Service
public class ReasonMasterValidator {

    public void validate(ReasonMaster reasonMaster) {

        log.info("ReasonMasterValidator Started for Reason : [" + reasonMaster.getId() + "]");

        // Reason Code Validation

        ValidateUtil.notNull(reasonMaster.getReasonCode(), ErrorCode.REASON_CODE_NOT_NULL);

        log.info("Reason Code validated...");

        // Reason Name Validations

        ValidateUtil.notNull(reasonMaster.getReasonName(), ErrorCode.REASON_NAME_NOT_NULL);

        //ValidateUtil.checkPattern(reasonMaster.getReasonName(), "reasonName", true);

        log.info("Reason Name validated...");

        if (reasonMaster.getStatus() == null) {
            reasonMaster.setStatus(LovStatus.Active);
        }

        ValidateUtil.notNull(reasonMaster.getStatus(), ErrorCode.REASON_LOV_STATUS_NOT_NULL);

        ValidateUtil.validateEnum(LovStatus.class, reasonMaster.getStatus(),
                ErrorCode.REASON_LOV_STATUS_INVALID);

        log.info("Reason Status Successfully Validated.....");

        log.info("ReasonMasterValidator Finished for Reason : [" + reasonMaster.getId() + "]");
    }
}
