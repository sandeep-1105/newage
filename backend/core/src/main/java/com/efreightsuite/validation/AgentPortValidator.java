package com.efreightsuite.validation;


import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.model.AgentPortMaster;
import com.efreightsuite.repository.UserProfileRepository;
import com.efreightsuite.service.AuthService;
import com.efreightsuite.util.AppUtil;
import com.efreightsuite.util.ValidateUtil;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Log4j2
@Service
public class AgentPortValidator {

    @Autowired
    AppUtil appUtil;

    @Autowired
    UserProfileRepository userProfileRepository;

    public void validate(AgentPortMaster agentPortMaster) {

        log.info("AgentPortValidator Started : [" + agentPortMaster.getId() + "]");


        ValidateUtil.notNull(agentPortMaster.getServiceMaster().getId(), ErrorCode.SERVICE_ID_NOT_NULL);
        ValidateUtil.isStatusBlocked(agentPortMaster.getServiceMaster().getStatus(), ErrorCode.SERVICE_STATUS_BLOCK);
        ValidateUtil.isStatusHidden(agentPortMaster.getServiceMaster().getStatus(), ErrorCode.SERVICE_STATUS_HIDE);
        agentPortMaster.setServiceCode(agentPortMaster.getServiceMaster().getServiceCode());

        ValidateUtil.notNull(agentPortMaster.getAgent().getId(), ErrorCode.AGENT_ID_NOT_NULL);
        ValidateUtil.isStatusBlocked(agentPortMaster.getAgent().getStatus(), ErrorCode.AGENT_STATUS_BLOCK);
        ValidateUtil.isStatusHidden(agentPortMaster.getAgent().getStatus(), ErrorCode.AGENT_STATUS_HIDE);

        ValidateUtil.notNull(agentPortMaster.getPort().getId(), ErrorCode.PARTY_ID_NOT_NULL);
        ValidateUtil.isStatusBlocked(agentPortMaster.getPort().getStatus(), ErrorCode.PORT_STATUS_BLOCK);
        ValidateUtil.isStatusHidden(agentPortMaster.getPort().getStatus(), ErrorCode.PORT_STATUS_HIDE);

        agentPortMaster.setLocationMaster(AuthService.getCurrentUser().getSelectedUserLocation());
        agentPortMaster.setCountryMaster(agentPortMaster.getPort().getPortGroupMaster().getCountryMaster());

        log.info("AgentPortValidator Finished  : [" + agentPortMaster.getId() + "]");
    }


}
