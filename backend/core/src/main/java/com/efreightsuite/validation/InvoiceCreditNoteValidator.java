package com.efreightsuite.validation;

import java.util.List;

import com.efreightsuite.enumeration.DocumentType;
import com.efreightsuite.enumeration.YesNo;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.model.Consol;
import com.efreightsuite.model.InvoiceCreditNote;
import com.efreightsuite.model.InvoiceCreditNoteAttachment;
import com.efreightsuite.model.InvoiceCreditNoteDetail;
import com.efreightsuite.model.InvoiceCreditNoteReceipt;
import com.efreightsuite.model.ShipmentServiceDetail;
import com.efreightsuite.repository.ConsolRepository;
import com.efreightsuite.repository.InvoiceCreditNoteRepository;
import com.efreightsuite.repository.ShipmentRepository;
import com.efreightsuite.service.AuthService;
import com.efreightsuite.service.RegularExpressionService;
import com.efreightsuite.util.RegExpName;
import com.efreightsuite.util.ValidateUtil;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author user
 */
@Service
@Log4j2
public class InvoiceCreditNoteValidator {

    @Autowired
    private
    InvoiceDetailValidator invoiceDetailValidator;

    @Autowired
    private
    InvoiceAttachmentValidator invoiceAttachmentValidator;

    @Autowired
    private
    InvoiceReceiptValidator invoiceReceiptValidator;

    @Autowired
    private
    ShipmentRepository shipmentRepository;

    @Autowired
    private
    ConsolRepository consolRepository;

    @Autowired
    private
    InvoiceCreditNoteRepository invoiceCreditNoteRepository;

    @Autowired
    private
    RegularExpressionService regExpService;

    public void validate(InvoiceCreditNote invoiceCreditNote) {
        verifySignOff(invoiceCreditNote);
        verifyDaybook(invoiceCreditNote);
        verifyAgentCustomer(invoiceCreditNote);
        verifyParty(invoiceCreditNote);
        verifyPartyAddress(invoiceCreditNote);
        verifyPartyAccount(invoiceCreditNote);
        verifyInvoiceDate(invoiceCreditNote);
        // verifyDueDate(invoice);
        verifyStatus(invoiceCreditNote);
        verifyBillingCurrency(invoiceCreditNote);
        verifyRoe(invoiceCreditNote);

        if (invoiceCreditNote.getInvoiceCreditNoteDetailList() != null
                && invoiceCreditNote.getInvoiceCreditNoteDetailList().size() > 0) {
            for (InvoiceCreditNoteDetail invoiceCharge : invoiceCreditNote.getInvoiceCreditNoteDetailList())
                invoiceDetailValidator.validate(invoiceCharge);
        }
        if (invoiceCreditNote.getInvoiceCreditNoteReceiptList() != null
                && invoiceCreditNote.getInvoiceCreditNoteReceiptList().size() > 0) {
            for (InvoiceCreditNoteReceipt invoiceReceipt : invoiceCreditNote.getInvoiceCreditNoteReceiptList())
                invoiceReceiptValidator.validate(invoiceReceipt);
        }

        if (invoiceCreditNote.getInvoiceCreditNoteAttachmentList() != null
                && invoiceCreditNote.getInvoiceCreditNoteAttachmentList().size() > 0) {
            for (InvoiceCreditNoteAttachment invoiceAttachment : invoiceCreditNote.getInvoiceCreditNoteAttachmentList())
                invoiceAttachmentValidator.validate(invoiceAttachment);
        }

        invoiceCreditNote.setLocation(AuthService.getCurrentUser().getSelectedUserLocation());
        invoiceCreditNote.setLocationCode(AuthService.getCurrentUser().getSelectedUserLocation().getLocationCode());

        if (invoiceCreditNote.getLocalCurrency() != null) {
            invoiceCreditNote.setLocalCurrencyCode(invoiceCreditNote.getLocalCurrency().getCurrencyCode());
        }

        if (invoiceCreditNote.getDocumentTypeMaster() != null) {
            invoiceCreditNote.setDocumentTypeCode(invoiceCreditNote.getDocumentTypeMaster().getDocumentTypeCode().toString());
        }

        if (invoiceCreditNote.getAdjustMentDaybookMaster() != null) {
            invoiceCreditNote.setAdjustMentDaybookCode(invoiceCreditNote.getAdjustMentDaybookMaster().getDaybookCode());
        }

        if (invoiceCreditNote.getReasonMaster() != null) {
            invoiceCreditNote.setReasonCode(invoiceCreditNote.getReasonMaster().getReasonCode());
        }

    }

    private void verifySignOff(InvoiceCreditNote invoice) {

        if (invoice.getMasterUid() != null && invoice.getShipmentServiceDetail() == null) {
            //Master Level

            Consol consol = consolRepository.findByConsolUid(invoice.getMasterUid());

            if (consol.getConsolSignOff() == null
                    || (consol.getConsolSignOff() != null && consol.getConsolSignOff().isSignOff == YesNo.No)) {
                throw new RestException(ErrorCode.INVOICE_SERVICE_SIGNOFF_REQ);
            }

        } else if (invoice.getShipmentServiceDetail() != null) {
            //Service Level

            ShipmentServiceDetail service = shipmentRepository.getByService(invoice.getShipmentServiceDetail().getId());

            if (service.getShipmentServiceSignOff() == null
                    || (service.getShipmentServiceSignOff() != null && service.getShipmentServiceSignOff().isSignOff == YesNo.No)) {
                throw new RestException(ErrorCode.INVOICE_MASTER_SIGNOFF_REQ);
            }

        }


    }

    private void verifyDaybook(InvoiceCreditNote invoice) {
        ValidateUtil.notNull(invoice.getDaybookMaster(), ErrorCode.INVOICE_DAYBOOK);
        ValidateUtil.notNull(invoice.getDaybookMaster().getId(), ErrorCode.INVOICE_DAYBOOK);
        ValidateUtil.isStatusBlocked(invoice.getDaybookMaster().getStatus(), ErrorCode.INVOICE_DAYBOOK_BLOCK);
        ValidateUtil.isStatusHidden(invoice.getDaybookMaster().getStatus(), ErrorCode.INVOICE_DAYBOOK_HIDE);
        invoice.setDaybookCode(invoice.getDaybookMaster().getDaybookCode());

    }

    private void verifyAgentCustomer(InvoiceCreditNote invoice) {
        ValidateUtil.notNull(invoice.getCustomerAgent(), ErrorCode.INVOICE_AGENT_CUSTOMER);
    }

    private void verifyPartyAddress(InvoiceCreditNote invoice) {
        ValidateUtil.notNull(invoice.getPartyAddress(), ErrorCode.INVOICE_PARTY_ADDRESS);

        ValidateUtil.notNullOrEmpty(invoice.getPartyAddress().getAddressLine1(), ErrorCode.INVOICE_PARTY_ADDRESS_1);
        ValidateUtil.validateLength(invoice.getPartyAddress().getAddressLine1(), 1, 100,
                ErrorCode.INVOICE_PARTY_ADDRESS_1_LENGTH);

        if (invoice.getPartyAddress().getAddressLine2() != null
                && invoice.getPartyAddress().getAddressLine2().trim().length() != 0) {
            ValidateUtil.notNullOrEmpty(invoice.getPartyAddress().getAddressLine2(), ErrorCode.INVOICE_PARTY_ADDRESS_2);
            ValidateUtil.validateLength(invoice.getPartyAddress().getAddressLine2(), 1, 100,
                    ErrorCode.INVOICE_PARTY_ADDRESS_2_LENGTH);

        }

        if (invoice.getPartyAddress().getAddressLine3() != null
                && invoice.getPartyAddress().getAddressLine3().trim().length() != 0) {
            ValidateUtil.validateLength(invoice.getPartyAddress().getAddressLine3(), 1, 100,
                    ErrorCode.INVOICE_PARTY_ADDRESS_3_LENGTH);
        }
        if (invoice.getPartyAddress().getAddressLine4() != null
                && invoice.getPartyAddress().getAddressLine4().trim().length() != 0) {
            ValidateUtil.notNullOrEmpty(invoice.getPartyAddress().getAddressLine4(), ErrorCode.INVOICE_PARTY_ADDRESS_4);
            ValidateUtil.validateLength(invoice.getPartyAddress().getAddressLine4(), 1, 100,
                    ErrorCode.INVOICE_PARTY_ADDRESS_4_LENGTH);
        }

        if (invoice.getPartyAddress().getCity() != null) {
            invoice.getPartyAddress().setCityCode(invoice.getPartyAddress().getCity().getCityCode());
        }

        if (invoice.getPartyAddress().getState() != null) {
            invoice.getPartyAddress().setStateCode(invoice.getPartyAddress().getState().getStateCode());
        }

        if (invoice.getPartyAddress().getCountry() != null) {
            invoice.getPartyAddress().setCountryCode(invoice.getPartyAddress().getCountry().getCountryCode());
        }
    }

    private void verifyParty(InvoiceCreditNote invoice) {
        ValidateUtil.notNull(invoice.getParty(), ErrorCode.INVOICE_PARTY);
        ValidateUtil.notNull(invoice.getParty().getId(), ErrorCode.INVOICE_PARTY);
        ValidateUtil.notNullOrEmpty(invoice.getParty().getPartyCode(), ErrorCode.INVOICE_PARTY);
        ValidateUtil.isStatusBlocked(invoice.getParty().getStatus(), ErrorCode.INVOICE_PARTY_BLOCK);
        ValidateUtil.isStatusHidden(invoice.getParty().getStatus(), ErrorCode.INVOICE_PARTY_HIDE);
        invoice.setPartyCode(invoice.getParty().getPartyCode());

    }

    private void verifyInvoiceDate(InvoiceCreditNote invoice) {
        ValidateUtil.notNull(invoice.getInvoiceCreditNoteDate(), ErrorCode.INVOICE_DATE);
    }

    private void verifyDueDate(InvoiceCreditNote invoice) {
        ValidateUtil.notNull(invoice.getDueDate(), ErrorCode.INVOICE_DUE_DATE);
    }

    private void verifyStatus(InvoiceCreditNote invoice) {
        ValidateUtil.notNull(invoice.getInvoiceCreditNoteStatus(), ErrorCode.INVOICE_STATUS);
    }

    private void verifyBillingCurrency(InvoiceCreditNote invoice) {
        ValidateUtil.notNull(invoice.getBillingCurrency(), ErrorCode.INVOICE_BILLING_CURRENCY);
        ValidateUtil.notNull(invoice.getBillingCurrency().getId(), ErrorCode.INVOICE_BILLING_CURRENCY);
        ValidateUtil.notNullOrEmpty(invoice.getBillingCurrency().getCurrencyCode(), ErrorCode.INVOICE_BILLING_CURRENCY);
        ValidateUtil.isStatusBlocked(invoice.getBillingCurrency().getStatus(),
                ErrorCode.INVOICE_BILLING_CURRENCY_BLOCK);
        ValidateUtil.isStatusHidden(invoice.getBillingCurrency().getStatus(), ErrorCode.INVOICE_BILLING_CURRENCY_HIDE);
        invoice.setBillingCurrencyCode(invoice.getBillingCurrency().getCurrencyCode());

    }

    private void verifyRoe(InvoiceCreditNote invoice) {
        ValidateUtil.notNull(invoice.getRoe(), ErrorCode.INVOICE_ROE);
        if (invoice.getRoe() != null && invoice.getRoe() <= 0) {
            throw new RestException(ErrorCode.INVOICE_ROE_CANNOT_ZERO_OR_NEGATIVE);
        }
        regExpService.validate(invoice.getRoe(), RegExpName.Reg_Exp_FINANCE_ROE, ErrorCode.INVOICE_ROE_INVALID);

    }

    private void verifyPartyAccount(InvoiceCreditNote invoice) {
        ValidateUtil.notNull(invoice.getPartyAccount(), ErrorCode.INVOICE_PARTY_ACCOUNT);
        ValidateUtil.notNull(invoice.getPartyAccount().getId(), ErrorCode.INVOICE_PARTY_ACCOUNT);
        ValidateUtil.isStatusBlocked(invoice.getPartyAccount().getStatus(), ErrorCode.INVOICE_PARTY_ACCOUNT_BLOCK);
        ValidateUtil.isStatusHidden(invoice.getPartyAccount().getStatus(), ErrorCode.INVOICE_PARTY_ACCOUNT_HIDE);

    }

    public boolean accountingTransactionStatus(String consolUid, Long serviceId) {
        /* true - Accounting transaction closed
		 * false - Accounting transaction is not closed
		 */

        boolean flag = false;

        List<InvoiceCreditNote> invoiceList = null;

        if (consolUid != null && serviceId == null) {
            //Master Level

            invoiceList = invoiceCreditNoteRepository.findByConsolUid(consolUid);

        } else if (consolUid == null && serviceId != null) {
            //Service Level

            invoiceList = invoiceCreditNoteRepository.findByShipmentServiceDetail(serviceId);

        }

        if (invoiceList != null && invoiceList.size() != 0) {
            flag = invoiceIsCancelled(invoiceList);
        } else {
            flag = true;
        }

        return flag;
    }

    private boolean invoiceIsCancelled(List<InvoiceCreditNote> invoiceList) {
        double invoiceAmt = 0.0;
        double reversalAmt = 0.0;

        for (InvoiceCreditNote invoice : invoiceList) {
            if (invoice.getDaybookMaster().getDocumentTypeMaster().getDocumentTypeCode() == DocumentType.INV) {
                invoiceAmt = invoiceAmt + invoice.getNetLocalAmount();
                reversalAmt = reversalAmt + (invoice.getAdjustmentAmount() == null ? 0.0 : invoice.getAdjustmentAmount());
            }
        }

        return invoiceAmt == reversalAmt;

    }
}
