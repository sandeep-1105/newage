package com.efreightsuite.validation;

import com.efreightsuite.enumeration.LovStatus;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.model.RegionMaster;
import com.efreightsuite.service.RegularExpressionService;
import com.efreightsuite.util.RegExpName;
import com.efreightsuite.util.ValidateUtil;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class RegionMasterValidator {


    @Autowired
    private
    RegularExpressionService regExpRegion;


    public void validate(RegionMaster rm) {

        log.info("Region  validation started");
        validateRegionCode(rm);
        validateRegionName(rm);
        validateRegionStatus(rm);
        log.info("Region  validation ended");

    }

    private void validateRegionStatus(RegionMaster rm) {
        if (rm.getId() != null) {
            ValidateUtil.notNull(rm.getStatus(), ErrorCode.REGION_STATUS_NOT_NULL);
        } else {
            rm.setStatus(LovStatus.Active);
        }
    }

    private void validateRegionName(RegionMaster rm) {

        ValidateUtil.notNull(rm.getRegionName(), ErrorCode.REGION_NAME_NOT_NULL);

        regExpRegion.validate(rm.getRegionName(), RegExpName.Reg_Exp_Master_Region_Name, ErrorCode.REGION_NAME_INVALID);


    }

    private void validateRegionCode(RegionMaster rm) {

        ValidateUtil.notNull(rm.getRegionCode(), ErrorCode.REGION_CODE_NOT_NULL);

        regExpRegion.validate(rm.getRegionCode(), RegExpName.Reg_Exp_Master_Bank_Code, ErrorCode.BANK_CODE_INVALID);
        rm.setRegionCode(rm.getRegionCode().toUpperCase());

    }


}
