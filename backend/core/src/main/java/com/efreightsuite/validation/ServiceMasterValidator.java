package com.efreightsuite.validation;

import com.efreightsuite.enumeration.FullGroupage;
import com.efreightsuite.enumeration.ImportExport;
import com.efreightsuite.enumeration.LovStatus;
import com.efreightsuite.enumeration.TransportMode;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.model.ServiceMaster;
import com.efreightsuite.service.RegularExpressionService;
import com.efreightsuite.util.RegExpName;
import com.efreightsuite.util.ValidateUtil;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class ServiceMasterValidator {

    @Autowired
    private
    RegularExpressionService regExpService;

    public void validate(ServiceMaster serviceMaster) {

        log.info("service  validation started");
        validateServiceCode(serviceMaster);
        validateServiceName(serviceMaster);
        ValidateUtil.validateEnum(TransportMode.class, serviceMaster.getTransportMode(),
                ErrorCode.SERVICE_TRANSPORT_MODE_NOTNULL);

        if (serviceMaster.getTransportMode().equals(TransportMode.Ocean)) {
            ValidateUtil.validateEnum(FullGroupage.class, serviceMaster.getFullGroupage(),
                    ErrorCode.SERVICE_FULL_GROUPAGE_NOTNULL);
        }

        ValidateUtil.validateEnum(ImportExport.class, serviceMaster.getImportExport(),
                ErrorCode.SERVICE_IMPORT_EXPORT__MODE_NOTNULL);

        ValidateUtil.validateEnum(LovStatus.class, serviceMaster.getStatus(), ErrorCode.SERVICE_LOV_STATUS_NOTNULL);
        log.info("service  validation ended");

        validateCostCenter(serviceMaster);
        validateServiceType(serviceMaster);
    }

    private void validateCostCenter(ServiceMaster serviceMaster) {

        log.info("validate Cost Center started");

        if (serviceMaster.getCostCenter() != null && serviceMaster.getCostCenter().getId() != null) {
            ValidateUtil.isStatusBlocked(serviceMaster.getCostCenter().getStatus(), ErrorCode.SERVICE_COST_CENTER_BLOCK);
            ValidateUtil.isStatusHidden(serviceMaster.getCostCenter().getStatus(), ErrorCode.SERVICE_COST_CENTER_HIDE);

            serviceMaster.setCostCenterCode(serviceMaster.getCostCenter().getCostCenterCode());
        } else {
            serviceMaster.setCostCenter(null);
            serviceMaster.setCostCenterCode(null);
        }

        log.info("service code validate ended");

    }

    private void validateServiceType(ServiceMaster serviceMaster) {

        log.info("validate getServiceType started");

        if (serviceMaster.getServiceType() != null && serviceMaster.getServiceType().getId() != null) {
            ValidateUtil.isStatusBlocked(serviceMaster.getServiceType().getStatus(), ErrorCode.SERVICE_TYPE_BLOCK);
            ValidateUtil.isStatusHidden(serviceMaster.getServiceType().getStatus(), ErrorCode.SERVICE_TYPE_HIDE);
        } else {
            serviceMaster.setServiceType(null);
        }

        log.info("service code validate ended");

    }

    private void validateServiceCode(ServiceMaster serviceMaster) {

        ValidateUtil.notNull(serviceMaster.getServiceCode(), ErrorCode.SERVICE_CODE_NOT_NULL);

        regExpService.validate(serviceMaster.getServiceCode(), RegExpName.Reg_Exp_Master_Service_Code, ErrorCode.SERVICE_CODE_INVALID);

    }

    private void validateServiceName(ServiceMaster serviceMaster) {

        ValidateUtil.notNull(serviceMaster.getServiceName(), ErrorCode.SERVICE_NAME_NOT_NULL);

        regExpService.validate(serviceMaster.getServiceCode(), RegExpName.Reg_Exp_Master_Service_Name, ErrorCode.SERVICE_NAME_INVALID);

    }

}
