package com.efreightsuite.validation;

import com.efreightsuite.enumeration.YesNo;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.model.*;
import com.efreightsuite.repository.ChargeMasterRepository;
import com.efreightsuite.repository.CurrencyMasterRepository;
import com.efreightsuite.repository.ShipmentServiceDetailRepository;
import com.efreightsuite.repository.UnitMasterRepository;
import com.efreightsuite.util.ValidateUtil;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class ProvisionalValidator {

    @Autowired
    private
    ShipmentServiceDetailRepository shipmentServiceDetailRepository;

    @Autowired
    private
    ChargeMasterRepository chargeMasterRepository;

    @Autowired
    private
    UnitMasterRepository unitMasterRepository;

    @Autowired
    private
    CurrencyMasterRepository currencyMasterRepository;


    public void validate(Provisional provisional) {

        log.info("ProvisionalValidator.validate start");

        if (provisional.getLocalCurrency() != null) {
            provisional.setLocalCurrencyCode(provisional.getLocalCurrency().getCurrencyCode());
        }

        verifyMasterID(provisional);
        verifyShipmentID(provisional);
        verifyServiceName(provisional);
        verifySignOff(provisional);

        for (ProvisionItem provisionItem : provisional.getProvisionalItemList()) {

            verifyServiceId(provisionItem);
            verifyCharge(provisionItem);
            verifyUnit(provisionItem);
            verifyNoOfUnits(provisionItem);
            verifyBuyCurrency(provisionItem);
            verifyBuyRoe(provisionItem);
            verifyBuyAmountPerUnit(provisionItem);
            verifyBuyAmount(provisionItem);
            //verifyBuyLocalAmount(provisionItem);

            verifySellCurrency(provisionItem);
            verifySellRoe(provisionItem);
            verifySellAmountPerUnit(provisionItem);
            verifySellAmount(provisionItem);
            //verifySellLocalAmount(provisionItem);

            verifyIsTaxable(provisionItem);

            if (provisionItem.getBillToParty() != null) {
                provisionItem.setBillToPartyCode(provisionItem.getBillToParty().getPartyCode());
            }

            if (provisionItem.getVendorSubledger() != null) {
                provisionItem.setVendorSubledgerCode(provisionItem.getVendorSubledger().getPartyCode());
            }

            if (provisionItem.getReferenceType() != null) {
                provisionItem.setReferenceTypeCode(provisionItem.getReferenceType().getReferenceTypeCode());
            }

        }
        // need write validation here based on test casesS

        log.info("ProvisionalValidator.validate end");

    }

    private void verifySignOff(Provisional provisional) {

        if (provisional.getMasterUid() != null && provisional.getServiceUid() == null) {
            //Master Level Provisional Cost
        } else {
            //Service Level Provisional Cost
            ShipmentServiceDetail sd = shipmentServiceDetailRepository.findByServiceUid(provisional.getServiceUid());

            if (sd.getShipmentServiceSignOff() != null
                    && sd.getShipmentServiceSignOff().getIsSignOff() == YesNo.Yes) {
                throw new RestException(ErrorCode.SHIPMENT_SIGN_OFF_PVOS_NOT_MODIFY);
            }
        }


    }

    private void verifyTotalRevenue(Provisional provisional) {
        //ValidateUtil.notNull(provisional.getTotalRevenue(), ErrorCode.PROVISION_TOTAL_REVENUE);
        if (provisional.getTotalRevenue() != null && provisional.getTotalRevenue() <= 0) {
            throw new RestException(ErrorCode.PROVISION_TOTAL_REVENUE_CANNOT_ZERO);
        }

    }

    private void verifyTotalCost(Provisional provisional) {
        //ValidateUtil.notNull(provisional.getTotalCost(), ErrorCode.PROVISION_TOTAL_COST);
        if (provisional.getTotalCost() != null && provisional.getTotalCost() <= 0) {
            throw new RestException(ErrorCode.PROVISION_TOTAL_COST_CANNOT_ZERO);
        }

    }

    private void verifyNet(Provisional provisional) {

        //ValidateUtil.notNull(provisional.getNetAmount(), ErrorCode.PROVISION_NET);
        if (provisional.getNetAmount() != null && provisional.getNetAmount() <= 0) {
            throw new RestException(ErrorCode.PROVISION_NET_CANNOT_ZERO);
        }

    }

    private void verifyMasterID(Provisional provisional) {
        if (provisional.getShipmentUid() == null)
            ValidateUtil.notNull(provisional.getMasterUid(), ErrorCode.PROVISION_MASTER_ID);
    }


    private void verifyShipmentID(Provisional provisional) {
        if (provisional.getMasterUid() == null)
            ValidateUtil.notNull(provisional.getShipmentUid(), ErrorCode.PROVISION_SHIPMENT_ID);
    }

    private void verifyServiceName(Provisional provisional) {
        ValidateUtil.notNull(provisional.getServiceName(), ErrorCode.PROVISION_SERVICE_NAME);
    }

    private void verifyCharge(ProvisionItem provisionItem) {
        ValidateUtil.notNull(provisionItem.getChargeMaster(), ErrorCode.PROVISION_CHARGE);
        ValidateUtil.notNull(provisionItem.getChargeMaster().getId(), ErrorCode.PROVISION_CHARGE);
        if (provisionItem.getChargeMaster() != null && provisionItem.getChargeMaster().getId() != null) {

            ChargeMaster charge = chargeMasterRepository.findById(provisionItem.getChargeMaster().getId());
            ValidateUtil.notNullOrEmpty(charge.getChargeCode(), ErrorCode.PROVISION_CHARGE);
            ValidateUtil.isStatusBlocked(charge.getStatus(), ErrorCode.PROVISION_CHARGE_BLOCK);
            ValidateUtil.isStatusHidden(charge.getStatus(), ErrorCode.PROVISION_CHARGE_HIDE);
            provisionItem.setChargeCode(charge.getChargeCode());
        } else {
            provisionItem.setChargeMaster(null);
            provisionItem.setChargeCode(null);
        }
    }

    private void verifyUnit(ProvisionItem provisionItem) {
        if (provisionItem.getUnitMaster() != null && provisionItem.getUnitMaster().getId() != null) {
            UnitMaster unitMaster = unitMasterRepository.getOne(provisionItem.getUnitMaster().getId());

            ValidateUtil.notNullOrEmpty(unitMaster.getUnitCode(), ErrorCode.PROVISION_UNIT);
            ValidateUtil.isStatusBlocked(unitMaster.getStatus(), ErrorCode.PROVISION_UNIT_BLOCK);
            ValidateUtil.isStatusHidden(unitMaster.getStatus(), ErrorCode.PROVISION_UNIT_HIDE);
            provisionItem.setUnitCode(unitMaster.getUnitCode());
        } else {
            provisionItem.setUnitMaster(null);
            provisionItem.setUnitCode(null);
        }
    }

    private void verifyServiceId(ProvisionItem provisional) {
        //ValidateUtil.notNull(provisional.getShipmentService(), ErrorCode.PROVISION_SERVICE_ID);
    }

    private void verifyNoOfUnits(ProvisionItem provisionItem) {
        //ValidateUtil.notNull(provisionItem.getNoOfUnits(), ErrorCode.PROVISION_NO_OF_UNITS);
        if (provisionItem.getNoOfUnits() != null && provisionItem.getNoOfUnits() <= 0) {
            throw new RestException(ErrorCode.PROVISION_NO_OF_UNITS_CANNOT_ZERO);
        }

    }

    private void verifyBuyCurrency(ProvisionItem provisionItem) {
        if (provisionItem.getSellCurrency() == null || provisionItem.getSellCurrency().getId() == null) {
            ValidateUtil.notNull(provisionItem.getBuyCurrency(), ErrorCode.PROVISION_BUY_CURRENCY);
            ValidateUtil.notNull(provisionItem.getBuyCurrency().getId(), ErrorCode.PROVISION_BUY_CURRENCY);
        }
        if (provisionItem.getBuyCurrency() != null && provisionItem.getBuyCurrency().getId() != null) {

            CurrencyMaster currencyMaster = currencyMasterRepository.getOne(provisionItem.getBuyCurrency().getId());
            ValidateUtil.notNullOrEmpty(currencyMaster.getCurrencyCode(), ErrorCode.PROVISION_BUY_CURRENCY);
            ValidateUtil.isStatusBlocked(currencyMaster.getStatus(), ErrorCode.PROVISION_BUY_CURRENCY_BLOCK);
            ValidateUtil.isStatusHidden(currencyMaster.getStatus(), ErrorCode.PROVISION_BUY_CURRENCY_HIDE);
            provisionItem.setBuyCurrencyCode(currencyMaster.getCurrencyCode());
        } else {
            provisionItem.setBuyCurrency(null);
            provisionItem.setBuyCurrencyCode(null);
        }
    }

    private void verifyBuyRoe(ProvisionItem provisionItem) {
        if (provisionItem.getSellRoe() == null) {
            ValidateUtil.notNull(provisionItem.getBuyRoe(), ErrorCode.PROVISION_BUY_ROE);
        }
        if (provisionItem.getBuyRoe() != null && provisionItem.getBuyRoe() <= 0) {
            throw new RestException(ErrorCode.PROVISION_BUY_ROE_CANNOT_ZERO);
        }

    }

    private void verifyBuyAmountPerUnit(ProvisionItem provisionItem) {
        //ValidateUtil.notNull(provisionItem.getBuyAmountPerUnit(), ErrorCode.PROVISION_BUY_AMOUNT_PER_UNIT);
        if (provisionItem.getBuyAmountPerUnit() != null && provisionItem.getBuyAmountPerUnit() <= 0) {
            throw new RestException(ErrorCode.PROVISION_BUY_AMOUNT_PER_UNIT_CANNOT_ZERO);
        }

    }

    private void verifyBuyAmount(ProvisionItem provisionItem) {
        //ValidateUtil.notNull(provisionItem.getBuyAmount(), ErrorCode.PROVISION_BUY_AMOUNT);
        /*if(provisionItem.getBuyAmount()!=null )
		{
			throw new RestException(ErrorCode.PROVISION_BUY_AMOUNT_CANNOT_ZERO);
		}*/

    }

    private void verifyBuyLocalAmount(ProvisionItem provisionItem) {
        //ValidateUtil.notNull(provisionItem.getBuyLocalAmount(), ErrorCode.PROVISION_BUY_LOCAL_AMOUNT);
		/*if(provisionItem.getBuyLocalAmount()!=null )
		{
			throw new RestException(ErrorCode.PROVISION_BUY_LOCAL_AMOUNT_CANNOT_ZERO);
		}*/

    }

    private void verifySellCurrency(ProvisionItem provisionItem) {
        if (provisionItem.getBuyCurrency() == null || provisionItem.getBuyCurrency().getId() == null) {
            log.info("provisionItem.getSellCurrency() : " + provisionItem.getSellCurrency());
            log.info("provisionItem.getSellCurrency code : " + provisionItem.getSellCurrency().getCurrencyCode());
            ValidateUtil.notNull(provisionItem.getSellCurrency(), ErrorCode.PROVISION_SELL_CURRENCY);
            ValidateUtil.notNull(provisionItem.getSellCurrency().getId(), ErrorCode.PROVISION_SELL_CURRENCY);
        }
        if (provisionItem.getSellCurrency() != null && provisionItem.getSellCurrency().getId() != null) {
            CurrencyMaster currencyMaster = currencyMasterRepository.getOne(provisionItem.getSellCurrency().getId());

            ValidateUtil.notNullOrEmpty(currencyMaster.getCurrencyCode(), ErrorCode.PROVISION_SELL_CURRENCY);
            ValidateUtil.isStatusBlocked(currencyMaster.getStatus(), ErrorCode.PROVISION_SELL_CURRENCY_BLOCK);
            ValidateUtil.isStatusHidden(currencyMaster.getStatus(), ErrorCode.PROVISION_SELL_CURRENCY_HIDE);
            provisionItem.setSellCurrencyCode(currencyMaster.getCurrencyCode());
        } else {
            provisionItem.setSellCurrency(null);
            provisionItem.setSellCurrencyCode(null);
        }
    }


    private void verifySellRoe(ProvisionItem provisionItem) {
        if (provisionItem.getBuyRoe() == null) {
            ValidateUtil.notNull(provisionItem.getSellRoe(), ErrorCode.PROVISION_SELL_ROE);
        }
        if (provisionItem.getSellRoe() != null && provisionItem.getSellRoe() <= 0) {
            throw new RestException(ErrorCode.PROVISION_SELL_ROE_CANNOT_ZERO);
        }

    }

    private void verifySellAmountPerUnit(ProvisionItem provisionItem) {
        //ValidateUtil.notNull(provisionItem.getSellAmountPerUnit(), ErrorCode.PROVISION_SELL_AMOUNT_PER_UNIT);
        if (provisionItem.getSellAmountPerUnit() != null && provisionItem.getSellAmountPerUnit() <= 0) {
            throw new RestException(ErrorCode.PROVISION_SELL_AMOUNT_PER_UNIT_CANNOT_ZERO);
        }

    }

    private void verifySellAmount(ProvisionItem provisionItem) {
        //ValidateUtil.notNull(provisionItem.getSellAmount(), ErrorCode.PROVISION_SELL_AMOUNT);
        if (provisionItem.getSellAmount() != null && provisionItem.getSellAmount() <= 0) {
            throw new RestException(ErrorCode.PROVISION_SELL_AMOUNT_CANNOT_ZERO);
        }

    }

    private void verifySellLocalAmount(ProvisionItem provisionItem) {
        //ValidateUtil.notNull(provisionItem.getSellLocalAmount(), ErrorCode.PROVISION_BUY_LOCAL_AMOUNT);
        if (provisionItem.getSellLocalAmount() != null && provisionItem.getSellLocalAmount() <= 0) {
            throw new RestException(ErrorCode.PROVISION_SELL_LOCAL_AMOUNT_CANNOT_ZERO);
        }

    }

    private void verifyIsTaxable(ProvisionItem provisionItem) {
        if (provisionItem.getIsTaxable() == null || !provisionItem.getIsTaxable()) {
            provisionItem.setIsTaxable(false);
        } else {
            provisionItem.setIsTaxable(true);
        }

    }


}
