package com.efreightsuite.validation;

import com.efreightsuite.dto.FileUploadDto;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.model.DefaultChargeMaster;
import com.efreightsuite.repository.DefaultMasterDataRepository;
import com.efreightsuite.service.AuthService;
import com.efreightsuite.util.AppUtil;
import com.efreightsuite.util.ValidateUtil;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class DefaultChargeMasterValidator {

    @Autowired
    private
    AppUtil appUtil;


    @Autowired
    DefaultMasterDataRepository defaultMasterDataRepository;

    public void validate(DefaultChargeMaster dcm) {

        verifyServiceMaster(dcm);
        verifyChargeMaster(dcm);
        verifyCurrenyMaster(dcm);
        verifyUnitMaster(dcm);
        verifyAmountPerUnit(dcm);
        verifyOtherOptional(dcm);

    }


    private void verifyOtherOptional(DefaultChargeMaster dcm) {
        if (dcm.getMinAmount() != null) {
            if (dcm.getMinAmount() > 9999999999.999999 || dcm.getAmountPerUnit() <= 0) {
                throw new RestException(ErrorCode.DEFAULT_CHARGE_MINAMOUNT_INVALID);
            }
        }

        if (dcm.getTosMaster() != null && dcm.getTosMaster().getId() != null) {
            ValidateUtil.isStatusBlocked(dcm.getTosMaster().getStatus(), ErrorCode.DEFAULT_CHARGE_TOS_BLOCKED);
            ValidateUtil.isStatusHidden(dcm.getTosMaster().getStatus(), ErrorCode.DEFAULT_CHARGE_TOS_HIDDEN);
        }


        if (dcm.getDivisionMaster() != null && dcm.getDivisionMaster().getId() != null) {
            ValidateUtil.isStatusBlocked(dcm.getDivisionMaster().getStatus(), ErrorCode.DEFAULT_CHARGE_DIVISION_BLOCKED);
            ValidateUtil.isStatusHidden(dcm.getDivisionMaster().getStatus(), ErrorCode.DEFAULT_CHARGE_DIVISION_HIDDEN);
        }

        if (dcm.getOrigin() != null && dcm.getOrigin().getId() != null) {
            ValidateUtil.isStatusBlocked(dcm.getOrigin().getStatus(), ErrorCode.DEFAULT_CHARGE_ORIGIN_BLOCKED);
            ValidateUtil.isStatusHidden(dcm.getOrigin().getStatus(), ErrorCode.DEFAULT_CHARGE_ORIGIN_HIDDEN);
        }

        if (dcm.getDestination() != null && dcm.getDestination().getId() != null) {
            ValidateUtil.isStatusBlocked(dcm.getDestination().getStatus(), ErrorCode.DEFAULT_CHARGE_DESTINATION_BLOCKED);
            ValidateUtil.isStatusHidden(dcm.getDestination().getStatus(), ErrorCode.DEFAULT_CHARGE_DESTINATION_HIDDEN);
        }

        if (dcm.getTransitPort() != null && dcm.getTransitPort().getId() != null) {
            ValidateUtil.isStatusBlocked(dcm.getTransitPort().getStatus(), ErrorCode.DEFAULT_CHARGE_TRANSITPORT_BLOCKED);
            ValidateUtil.isStatusHidden(dcm.getTransitPort().getStatus(), ErrorCode.DEFAULT_CHARGE_TRANSITPORT_HIDDEN);
        }


        if (dcm.getShipper() != null && dcm.getShipper().getId() != null) {

            ValidateUtil.isDefaulter(dcm.getShipper(), ErrorCode.DEFAULT_CHARGE_SHIPPER_DEFAULTER);
            ValidateUtil.isStatusBlocked(dcm.getShipper().getStatus(), ErrorCode.DEFAULT_CHARGE_SHIPPER_BLOCKED);
            ValidateUtil.isStatusHidden(dcm.getShipper().getStatus(), ErrorCode.DEFAULT_CHARGE_SHIPPER_HIDDEN);
        }

        if (dcm.getConsignee() != null && dcm.getConsignee().getId() != null) {
            ValidateUtil.isDefaulter(dcm.getConsignee(), ErrorCode.DEFAULT_CHARGE_CONSIGNEE_DEFAULTER);
            ValidateUtil.isStatusBlocked(dcm.getConsignee().getStatus(), ErrorCode.DEFAULT_CHARGE_CONSIGNEE_BLOCKED);
            ValidateUtil.isStatusHidden(dcm.getConsignee().getStatus(), ErrorCode.DEFAULT_CHARGE_CONSIGNEE_HIDDEN);
        }

        if (dcm.getAgent() != null && dcm.getAgent().getId() != null) {

            ValidateUtil.isDefaulter(dcm.getAgent(), ErrorCode.DEFAULT_CHARGE_AGENT_DEFAULTER);
            ValidateUtil.isStatusBlocked(dcm.getAgent().getStatus(), ErrorCode.DEFAULT_CHARGE_AGENT_BLOCKED);
            ValidateUtil.isStatusHidden(dcm.getAgent().getStatus(), ErrorCode.DEFAULT_CHARGE_AGENT_HIDDEN);
        }

        if (dcm.getCommodityMaster() != null && dcm.getCommodityMaster().getId() != null) {
            ValidateUtil.isStatusBlocked(dcm.getCommodityMaster().getStatus(), ErrorCode.DEFAULT_CHARGE_COMMODITY_BLOCKED);
            ValidateUtil.isStatusHidden(dcm.getCommodityMaster().getStatus(), ErrorCode.DEFAULT_CHARGE_COMMODITY_HIDDEN);
        }

        if (dcm.getCarrierMaster() != null && dcm.getCarrierMaster().getId() != null) {
            ValidateUtil.isStatusBlocked(dcm.getCarrierMaster().getStatus(), ErrorCode.DEFAULT_CHARGE_CARRIER_BLOCKED);
            ValidateUtil.isStatusHidden(dcm.getCarrierMaster().getStatus(), ErrorCode.DEFAULT_CHARGE_CARRIER_HIDDEN);
        }


    }


    private void verifyAmountPerUnit(DefaultChargeMaster dcm) {
        ValidateUtil.notNull(dcm.getAmountPerUnit(), ErrorCode.DEFAULT_CHARGE_AMOUNTUNIT_MANDATORY);
        if (dcm.getAmountPerUnit() > 9999999999.999999 || dcm.getAmountPerUnit() <= 0) {
            throw new RestException(ErrorCode.DEFAULT_CHARGE_AMOUNTUNIT_INVALID);
        }

    }


    private void verifyUnitMaster(DefaultChargeMaster dcm) {

        ValidateUtil.notNull(dcm.getUnitMaster(), ErrorCode.DEFAULT_CHARGE_UNIT_MANDATORY);
        ValidateUtil.isStatusBlocked(dcm.getUnitMaster().getStatus(), ErrorCode.DEFAULT_CHARGE_UNIT_BLOCKED);
        ValidateUtil.isStatusHidden(dcm.getUnitMaster().getStatus(), ErrorCode.DEFAULT_CHARGE_UNIT_HIDDEN);
        dcm.setUnitCode(dcm.getUnitMaster().getUnitCode());

    }


    private void verifyCurrenyMaster(DefaultChargeMaster dcm) {

        ValidateUtil.notNull(dcm.getCurrencyMaster(), ErrorCode.DEFAULT_CHARGE_CURRENCY_MANDATORY);
        ValidateUtil.isStatusBlocked(dcm.getCurrencyMaster().getStatus(), ErrorCode.DEFAULT_CHARGE_CURRENCY_BLOCKED);
        ValidateUtil.isStatusHidden(dcm.getCurrencyMaster().getStatus(), ErrorCode.DEFAULT_CHARGE_CURRENCY_HIDDEN);
        dcm.setCurrencyCode(dcm.getCurrencyMaster().getCurrencyCode());

    }


    private void verifyChargeMaster(DefaultChargeMaster dcm) {

        ValidateUtil.notNull(dcm.getChargeMaster(), ErrorCode.DEFAULT_CHARGE_CHARGE_MANDATORY);
        ValidateUtil.isStatusBlocked(dcm.getChargeMaster().getStatus(), ErrorCode.DEFAULT_CHARGE_CHARGE_BLOCKED);
        ValidateUtil.isStatusHidden(dcm.getChargeMaster().getStatus(), ErrorCode.DEFAULT_CHARGE_CHARGE_HIDDEN);
        dcm.setChargeCode(dcm.getChargeMaster().getChargeCode());
        dcm.setChargeDescription(dcm.getChargeDescription());

    }


    private void verifyServiceMaster(DefaultChargeMaster dcm) {
        ValidateUtil.notNull(dcm.getServiceMaster(), ErrorCode.DEFAULT_CHARGE_SERVICE_MANDATORY);
        ValidateUtil.isStatusBlocked(dcm.getServiceMaster().getStatus(), ErrorCode.DEFAULT_CHARGE_SERVICE_BLOCKED);
        ValidateUtil.isStatusHidden(dcm.getServiceMaster().getStatus(), ErrorCode.DEFAULT_CHARGE_SERVICE_HIDDEN);
        dcm.setServiceCode(dcm.getServiceMaster().getServiceCode());

    }


    public void validatUpload(FileUploadDto fileUploadDto) {


        ValidateUtil.notNull(fileUploadDto.getFile(), ErrorCode.PRICING_MASTER_CSV_FILE_REQUIRED);

        ValidateUtil.nonZero(fileUploadDto.getFile().length, ErrorCode.PRICING_MASTER_CSV_FILE_EMPTY);

        String key = appUtil.getLocationConfig("file.size.pricing.air.bulk.upload", AuthService.getCurrentUser().getSelectedUserLocation(), false);

        if (key != null && key.trim().length() != 0) {

            long uploadedFileSize = fileUploadDto.getFile().length;

            long allowedSize = Long.parseLong(key) * 1024 * 1024;

            ValidateUtil.aboveRange(uploadedFileSize, allowedSize, ErrorCode.PRICING_MASTER_CSV_FILE_LENGTH_EXCEED);
        }

    }


}
