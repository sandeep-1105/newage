package com.efreightsuite.validation;

import com.efreightsuite.enumeration.LovStatus;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.model.EventMaster;
import com.efreightsuite.service.RegularExpressionService;
import com.efreightsuite.util.RegExpName;
import com.efreightsuite.util.ValidateUtil;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Log4j2
@Service
public class EventMasterValidator {

    @Autowired
    private
    RegularExpressionService regExpEvent;

    public void validate(EventMaster eventMaster) {

        ValidateUtil.notNullOrEmpty(eventMaster.getEventCode(), ErrorCode.MASTER_EVENT_CODE);
        regExpEvent.validate(eventMaster.getEventCode(), RegExpName.Reg_Exp_MASTER_EVENT_CODE, ErrorCode.MASTER_EVENT_CODE_INVALID);
        eventMaster.setEventCode(eventMaster.getEventCode().toUpperCase());

        ValidateUtil.notNullOrEmpty(eventMaster.getEventName(), ErrorCode.MASTER_EVENT_NAME);
        regExpEvent.validate(eventMaster.getEventName(), RegExpName.Reg_Exp_MASTER_EVENT_NAME, ErrorCode.MASTER_EVENT_NAME_INVALID);

        //ValidateUtil.notNull(eventMaster.getEventMasterType(), ErrorCode.MASTER_EVENT_TYPE);

        ValidateUtil.notNull(eventMaster.getCountryMaster(), ErrorCode.COUNTRY_NOTNULL);

        if (eventMaster.getId() != null) {
            ValidateUtil.notNull(eventMaster.getStatus(), ErrorCode.MASTER_EVENT_STATUS);
        } else {
            eventMaster.setStatus(LovStatus.Active);
        }
    }

}
