package com.efreightsuite.validation;


import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.model.ReferenceTypeMaster;
import com.efreightsuite.service.RegularExpressionService;
import com.efreightsuite.util.RegExpName;
import com.efreightsuite.util.ValidateUtil;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Log4j2
@Service
public class ReferenceTypeMasterValidator {

    @Autowired
    private
    RegularExpressionService regExpService;

    public void validate(ReferenceTypeMaster referenceTypeMaster) {

        log.info("ReferenceTypeMasterValidator Started for ReferenceType : [" + referenceTypeMaster.getId() + "]");

        ValidateUtil.notNull(referenceTypeMaster.getReferenceTypeCode(), ErrorCode.REFERENCE_TYPE_CODE_NOT_NULL);

        regExpService.validate(referenceTypeMaster.getReferenceTypeCode(), RegExpName.Reg_Exp_Master_REFERENCE_TYPE_CODE, ErrorCode.REFERENCE_TYPE_CODE_INVALID);

        ValidateUtil.notNull(referenceTypeMaster.getReferenceTypeName(), ErrorCode.REFERENCE_TYPE_NAME_NOT_NULL);

        regExpService.validate(referenceTypeMaster.getReferenceTypeName(), RegExpName.Reg_Exp_Master_REFERENCE_TYPE_NAME, ErrorCode.REFERENCE_TYPE_NAME_INVALID);

        //ValidateUtil.notNull(referenceTypeMaster.getReferenceType(), ErrorCode.REFERENCE_TYPE_REFERNE_TYPE_REQ);


        //ValidateUtil.validateLength(referenceTypeMaster.getNotes(), 0, 4000,  ErrorCode.REFERENCE_TYPE_NOTES_INVALID);

        log.info("ReferenceTypeMasterValidator Finished for ReferenceType : [" + referenceTypeMaster.getId() + "]");
    }
}
