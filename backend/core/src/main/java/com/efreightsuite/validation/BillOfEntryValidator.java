package com.efreightsuite.validation;

import java.util.Date;

import com.efreightsuite.enumeration.TransactionType;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.model.BillOfEntry;
import com.efreightsuite.model.ShipmentServiceDetail;
import com.efreightsuite.service.RegularExpressionService;
import com.efreightsuite.util.RegExpName;
import com.efreightsuite.util.ValidateUtil;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class BillOfEntryValidator {

    @Autowired
    private
    RegularExpressionService regExpService;

    public void validate(BillOfEntry billOfEntry, ShipmentServiceDetail service) {

        log.info("BillOfEntry Validation Started......[" + billOfEntry.getId() + "]");

        verifyBillOfEntry(billOfEntry.getBillOfEntry());

        verifyDeclarationNo(billOfEntry.getDecleartionNo());

        verifyBoeDate(billOfEntry.getBoeDate());

        verifyBoeValue(billOfEntry.getBoeValue());

        verifyBoeInvoiceNo(billOfEntry.getBoeInvoiceNo());

        verifyTransactionType(billOfEntry.getTransactionType());

        verifyAmount(billOfEntry.getAmount());

        verifyProcessDate(billOfEntry.getProcessDate());

        verifyAckNo(billOfEntry.getAckNo());

        verifyAckDate(billOfEntry.getAckDate());

        verifyNote(billOfEntry.getNote());

        log.info("BillOfEntry Validation Finished......[" + billOfEntry.getId() + "]");

    }

    private void verifyBillOfEntry(Integer billOfEntry) {

        ValidateUtil.notNull(billOfEntry, ErrorCode.BILL_OF_ENTRY_REQUIRED);
        //regExpService.validate(billOfEntry, RegExpName.Reg_Exp_Master_Bill_of_Entry, ErrorCode.BILL_OF_ENTRY_INVALID);

    }

    private void verifyDeclarationNo(Integer declarationNo) {

        if (declarationNo != null) {
            //regExpService.validate(declarationNo, RegExpName.Reg_Exp_Master_Bill_of_Entry, ErrorCode.BILL_OF_ENTRY_DECLARATION_NO_INVALID);
        }

    }

    private void verifyBoeDate(Date boeDate) {

        if (boeDate != null) {
            //ValidateUtil.notNull(boeDate, ErrorCode.BILL_OF_ENTRY_BOE_DATE_INVALID);
        }

    }


    private void verifyBoeValue(Double boeValue) {

        if (boeValue != null) {
            //ValidateUtil.notNull(boeValue, ErrorCode.BILL_OF_ENTRY_BOE_VALUE_INVLAID);
        }

    }

    private void verifyBoeInvoiceNo(String boeInvoiceNo) {

        if (boeInvoiceNo != null) {
            //ValidateUtil.notNull(boeInvoiceNo, ErrorCode.BILL_OF_ENTRY_BOE_INVOICENO_INVALID);
        }

    }

    private void verifyTransactionType(TransactionType transactionType) {

        //ValidateUtil.notNull(transactionType, ErrorCode.BILL_OF_ENTRY_TRANSACTION_TYPE_REQUIRED);

    }

    private void verifyAmount(Double amount) {

        if (amount != null) {
            regExpService.validate(amount, RegExpName.Reg_Exp_AMOUNT_TEN_SIX, ErrorCode.BILL_OF_ENTRY_AMOUNT_INVALID);
        }

    }

    private void verifyProcessDate(Date date) {

        if (date != null) {
            //ValidateUtil.notNull(date, ErrorCode.BILL_OF_ENTRY_PROCESS_DATE_INVALID);
        }

    }

    private void verifyAckNo(Integer ackNo) {

        if (ackNo != null) {
            //ValidateUtil.notNull(ackNo, ErrorCode.BILL_OF_ENTRY_ACKNO_INVALID);
        }

    }

    private void verifyAckDate(Date date) {

        if (date != null) {
            //ValidateUtil.notNull(date, ErrorCode.BILL_OF_ENTRY_ACK_DATE_INVALID);
        }

    }

    private void verifyNote(String note) {

        if (note != null) {
            //ValidateUtil.notNull(note, ErrorCode.BILL_OF_ENTRY_NOTE_INVALID);
        }
    }

}
