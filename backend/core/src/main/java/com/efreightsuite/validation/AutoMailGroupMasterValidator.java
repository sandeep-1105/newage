package com.efreightsuite.validation;

import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.model.AutoMailGroupMaster;
import com.efreightsuite.service.RegularExpressionService;
import com.efreightsuite.util.RegExpName;
import com.efreightsuite.util.ValidateUtil;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


/**
 * @author Devendrachary M
 */
@Service
public class AutoMailGroupMasterValidator {

    @Autowired
    private
    RegularExpressionService regExpService;

    public void validateToCreate(AutoMailGroupMaster autoMailGroupMaster) {

        validateMessageGroupCode(autoMailGroupMaster);

        validateMessageGroupName(autoMailGroupMaster);

    }

    public void validateToUpdate(AutoMailGroupMaster autoMailGroupMaster) {

        validateToCreate(autoMailGroupMaster);

    }

    private void validateMessageGroupCode(AutoMailGroupMaster autoMailGroupMaster) {

        ValidateUtil.notNullOrEmpty(autoMailGroupMaster.getMessageGroupCode(), ErrorCode.AUTOMAILGROUP_MESSAGEGROUPCODE_NOT_NULL);

        regExpService.validate(autoMailGroupMaster.getMessageGroupCode(), RegExpName.Reg_Exp_Master_AUTO_MAIL_GROUP_CODE, ErrorCode.AUTOMAILGROUP_MESSAGEGROUPCODE_INVALID);
    }

    private void validateMessageGroupName(AutoMailGroupMaster autoMailGroupMaster) {

        ValidateUtil.notNullOrEmpty(autoMailGroupMaster.getMessageGroupName(), ErrorCode.AUTOMAILGROUP_MESSAGEGROUPNAME_NOT_NULL);

        regExpService.validate(autoMailGroupMaster.getMessageGroupName(), RegExpName.Reg_Exp_Master_AUTO_MAIL_GROUP_NAME, ErrorCode.AUTOMAILGROUP_MESSAGEGROUPCODE_INVALID);
    }

}
