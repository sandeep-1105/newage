package com.efreightsuite.validation;

import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.model.DesignationMaster;
import com.efreightsuite.util.ValidateUtil;

import org.springframework.stereotype.Service;

@Service
public class DesignationMasterValidator {

    public void validate(DesignationMaster designationMaster) {
        //Designation code is auto generated. so that validation is not required - refer bug id- NES-1632
        //ValidateUtil.notNullOrEmpty(designationMaster.getDesignationCode(), ErrorCode.DESIGNATION_CODE_REQUIRED);
        //ValidateUtil.validateLength(designationMaster.getDesignationCode(), 0, 10, ErrorCode.DESIGNATION_CODE_INVALID);

        ValidateUtil.notNullOrEmpty(designationMaster.getDesignationName(), ErrorCode.DESIGNATION_NAME_REQUIRED);
        ValidateUtil.validateLength(designationMaster.getDesignationName(), 0, 100, ErrorCode.DESIGNATION_NAME_INVALID);
    }
}
