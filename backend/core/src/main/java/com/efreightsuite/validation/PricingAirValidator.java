package com.efreightsuite.validation;

import com.efreightsuite.dto.FileUploadDto;
import com.efreightsuite.enumeration.TransportMode;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.model.PricingAttachment;
import com.efreightsuite.model.PricingCarrier;
import com.efreightsuite.model.PricingMaster;
import com.efreightsuite.model.PricingPortPair;
import com.efreightsuite.repository.DefaultMasterDataRepository;
import com.efreightsuite.service.AuthService;
import com.efreightsuite.util.AppUtil;
import com.efreightsuite.util.ValidateUtil;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Log4j2
@Service
public class PricingAirValidator {

    @Autowired
    private
    AppUtil appUtil;

    @Autowired
    private
    PricingPortPairValidator pricingPortPairValidator;

    @Autowired
    private
    PricingCarrierValidator pricingCarrierValidator;

    @Autowired
    private
    PricingCarrierSlapValidator pricingCarrierSlapValidator;

    @Autowired
    DefaultMasterDataRepository defaultMasterDataRepository;


    public void validate(PricingMaster pricingMaster) {

        log.info("PricingAirValidator Started : [" + pricingMaster.getId() + "]");


        pricingMaster.setCompanyMaster(AuthService.getCurrentUser().getSelectedUserLocation().getCompanyMaster());
        pricingMaster.setCompanyCode(AuthService.getCurrentUser().getSelectedUserLocation().getCompanyMaster().getCompanyCode());
        pricingMaster.setLocationMaster(AuthService.getCurrentUser().getSelectedUserLocation());
        pricingMaster.setLocationCode(AuthService.getCurrentUser().getSelectedUserLocation().getLocationCode());
        pricingMaster.setCountryMaster(AuthService.getCurrentUser().getSelectedUserLocation().getCountryMaster());
        pricingMaster.setCountryCode(AuthService.getCurrentUser().getSelectedUserLocation().getCountryMaster().getCountryCode());

        pricingMaster.setTransportMode(TransportMode.Air);

        // Origin Validation
        validateOrigin(pricingMaster);
        // Destination Validation
        validateDestination(pricingMaster);
        // Transit Validation
        validateTransit(pricingMaster);

        if (pricingMaster.getOrigin() == null && pricingMaster.getDestination() == null) {
            throw new RestException(ErrorCode.ORIGIN_DESTINATION_REQUIRED);
        }

        // General Tab Validations

        for (PricingPortPair pricingPortPair : pricingMaster.getGeneralOriginPriceList()) {
            pricingPortPairValidator.validate(pricingPortPair);
        }

        for (PricingPortPair pricingPortPair : pricingMaster.getGeneralDestinationPriceList()) {
            pricingPortPairValidator.validate(pricingPortPair);
        }

        for (PricingCarrier pricingCarrier : pricingMaster.getGeneralFreightPriceList()) {

            pricingCarrierValidator.validate(pricingCarrier);
            pricingCarrierSlapValidator.validate(pricingCarrier, pricingCarrier.getStandardSellPrice());
            pricingCarrierSlapValidator.validate(pricingCarrier, pricingCarrier.getMinimumSellPrice());
            pricingCarrierSlapValidator.validate(pricingCarrier, pricingCarrier.getCost());

        }


        // Hazardous Tab Validations

        for (PricingPortPair pricingPortPair : pricingMaster.getHazardousOriginPriceList()) {
            pricingPortPairValidator.validate(pricingPortPair);
        }
        for (PricingPortPair pricingPortPair : pricingMaster.getHazardousDestinationPriceList()) {
            pricingPortPairValidator.validate(pricingPortPair);
        }

        for (PricingCarrier pricingCarrier : pricingMaster.getHazardousFreightPriceList()) {
            pricingCarrierValidator.validate(pricingCarrier);
            pricingCarrierSlapValidator.validate(pricingCarrier, pricingCarrier.getStandardSellPrice());
            pricingCarrierSlapValidator.validate(pricingCarrier, pricingCarrier.getMinimumSellPrice());
            pricingCarrierSlapValidator.validate(pricingCarrier, pricingCarrier.getCost());

        }


        log.info("PricingAirValidator Finished  : [" + pricingMaster.getId() + "]");
    }


    private void validateOrigin(PricingMaster pricingMaster) {

        if (pricingMaster.getOrigin() != null && pricingMaster.getOrigin().getId() != null) {
            ValidateUtil.notNull(pricingMaster.getOrigin(), ErrorCode.PRICING_MASTER_ORIGIN_REQUIRED);
            ValidateUtil.notNull(pricingMaster.getOrigin().getId(), ErrorCode.PRICING_MASTER_ORIGIN_REQUIRED);
            ValidateUtil.isStatusBlocked(pricingMaster.getOrigin().getStatus(), ErrorCode.PRICING_MASTER_ORIGIN_BLOCKED);
            ValidateUtil.isStatusHidden(pricingMaster.getOrigin().getStatus(), ErrorCode.PRICING_MASTER_ORIGIN_HIDDEN);

            pricingMaster.setOriginPortCode(pricingMaster.getOrigin().getPortCode());

            if (pricingMaster.getDestination() != null && pricingMaster.getDestination().getId() != null) {
                if (pricingMaster.getOrigin().getId().equals(pricingMaster.getDestination().getId())) {
                    throw new RestException(ErrorCode.PRICING_MASTER_ORIGIN_AND_DESTINATION_SAME);
                }
            }

            if (pricingMaster.getTransit() != null && pricingMaster.getTransit().getId() != null && pricingMaster.getOrigin().getId() != null
                    && pricingMaster.getOrigin() != null) {
                if (pricingMaster.getOrigin().getId().equals(pricingMaster.getTransit().getId())) {
                    throw new RestException(ErrorCode.PRICING_MASTER_ORIGIN_AND_TRANSIT_SAME);
                }
            }
        }
    }

    public void validatePricingAirUpload(FileUploadDto fileUploadDto) {
        ValidateUtil.notNull(fileUploadDto.getFile(), ErrorCode.PRICING_MASTER_CSV_FILE_REQUIRED);

        ValidateUtil.nonZero(fileUploadDto.getFile().length, ErrorCode.PRICING_MASTER_CSV_FILE_EMPTY);

        String defaultMasterData = appUtil.getLocationConfig("file.size.pricing.air.bulk.upload", AuthService.getCurrentUser().getSelectedUserLocation(), false);

        if (defaultMasterData != null && defaultMasterData.trim().length() != 0) {

            long uploadedFileSize = fileUploadDto.getFile().length;

            long allowedSize = Integer.parseInt(defaultMasterData) * 1024L * 1024;

            ValidateUtil.aboveRange(uploadedFileSize, allowedSize, ErrorCode.PRICING_MASTER_CSV_FILE_LENGTH_EXCEED);
        }
    }

    private void validateDestination(PricingMaster pricingMaster) {

        if (pricingMaster.getDestination() != null && pricingMaster.getDestination().getId() != null) {
            ValidateUtil.notNull(pricingMaster.getDestination(), ErrorCode.PRICING_MASTER_DESTINATION_REQUIRED);
            ValidateUtil.notNull(pricingMaster.getDestination().getId(), ErrorCode.PRICING_MASTER_DESTINATION_REQUIRED);
            ValidateUtil.isStatusBlocked(pricingMaster.getDestination().getStatus(), ErrorCode.PRICING_MASTER_DESTINATION_BLOCKED);
            ValidateUtil.isStatusHidden(pricingMaster.getDestination().getStatus(), ErrorCode.PRICING_MASTER_DESTINATION_HIDDEN);

            pricingMaster.setDestinationPortCode(pricingMaster.getDestination().getPortCode());

            if (pricingMaster.getOrigin() != null && pricingMaster.getOrigin().getId() != null) {
                if (pricingMaster.getDestination().getId().equals(pricingMaster.getOrigin().getId())) {
                    throw new RestException(ErrorCode.PRICING_MASTER_ORIGIN_AND_DESTINATION_SAME);
                }
            }

            if (pricingMaster.getTransit() != null && pricingMaster.getTransit().getId() != null && pricingMaster.getDestination().getId() != null
                    && pricingMaster.getDestination() != null) {
                if (pricingMaster.getDestination().getId().equals(pricingMaster.getTransit().getId())) {
                    throw new RestException(ErrorCode.PRICING_MASTER_DESTINATION_AND_TRANSIT_SAME);
                }
            }
        }

    }


    private void validateTransit(PricingMaster pricingMaster) {


        if (pricingMaster.getTransit() != null && pricingMaster.getTransit().getId() != null) {

            ValidateUtil.isStatusBlocked(pricingMaster.getTransit().getStatus(), ErrorCode.PRICING_MASTER_TRANSIT_BLOCKED);
            ValidateUtil.isStatusHidden(pricingMaster.getTransit().getStatus(), ErrorCode.PRICING_MASTER_TRANSIT_HIDDEN);

            pricingMaster.setTransitPortCode(pricingMaster.getTransit().getPortCode());

            if (pricingMaster.getOrigin() != null && pricingMaster.getOrigin().getId() != null && pricingMaster.getTransit().getId() != null
                    && pricingMaster.getTransit() != null) {
                if (pricingMaster.getTransit().getId().equals(pricingMaster.getOrigin().getId())) {
                    throw new RestException(ErrorCode.PRICING_MASTER_ORIGIN_AND_TRANSIT_SAME);
                }
            }

            if (pricingMaster.getDestination() != null && pricingMaster.getDestination().getId() != null && pricingMaster.getTransit().getId() != null
                    && pricingMaster.getTransit() != null) {
                if (pricingMaster.getTransit().getId().equals(pricingMaster.getDestination().getId())) {
                    throw new RestException(ErrorCode.PRICING_MASTER_DESTINATION_AND_TRANSIT_SAME);
                }
            }
        } else {
            pricingMaster.setTransit(null);
            pricingMaster.setTransitPortCode(null);
        }


    }


    public void pricingPortValidate(PricingPortPair pricingPortPair) {

        pricingPortPair.setCompanyMaster(AuthService.getCurrentUser().getSelectedUserLocation().getCompanyMaster());
        pricingPortPair.setCompanyCode(AuthService.getCurrentUser().getSelectedUserLocation().getCompanyMaster().getCompanyCode());
        pricingPortPair.setLocationMaster(AuthService.getCurrentUser().getSelectedUserLocation());
        pricingPortPair.setLocationCode(AuthService.getCurrentUser().getSelectedUserLocation().getLocationCode());
        pricingPortPair.setCountryMaster(AuthService.getCurrentUser().getSelectedUserLocation().getCountryMaster());
        pricingPortPair.setCountryCode(AuthService.getCurrentUser().getSelectedUserLocation().getCountryMaster().getCountryCode());


    }

    public void pricingCarrierValidate(PricingCarrier pricingCarrier) {

        pricingCarrier.setCompanyMaster(AuthService.getCurrentUser().getSelectedUserLocation().getCompanyMaster());
        pricingCarrier.setCompanyCode(AuthService.getCurrentUser().getSelectedUserLocation().getCompanyMaster().getCompanyCode());
        pricingCarrier.setLocationMaster(AuthService.getCurrentUser().getSelectedUserLocation());
        pricingCarrier.setLocationCode(AuthService.getCurrentUser().getSelectedUserLocation().getLocationCode());
        pricingCarrier.setCountryMaster(AuthService.getCurrentUser().getSelectedUserLocation().getCountryMaster());
        pricingCarrier.setCountryCode(AuthService.getCurrentUser().getSelectedUserLocation().getCountryMaster().getCountryCode());


    }

    public void pricingAttachmentValidate(PricingAttachment pricingAttachment) {

        pricingAttachment.setCompanyMaster(AuthService.getCurrentUser().getSelectedUserLocation().getCompanyMaster());
        pricingAttachment.setCompanyCode(AuthService.getCurrentUser().getSelectedUserLocation().getCompanyMaster().getCompanyCode());
        pricingAttachment.setLocationMaster(AuthService.getCurrentUser().getSelectedUserLocation());
        pricingAttachment.setLocationCode(AuthService.getCurrentUser().getSelectedUserLocation().getLocationCode());
        pricingAttachment.setCountryMaster(AuthService.getCurrentUser().getSelectedUserLocation().getCountryMaster());
        pricingAttachment.setCountryCode(AuthService.getCurrentUser().getSelectedUserLocation().getCountryMaster().getCountryCode());


    }

}

