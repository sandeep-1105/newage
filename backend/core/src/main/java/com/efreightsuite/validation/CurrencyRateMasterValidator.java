package com.efreightsuite.validation;

import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.model.CurrencyRateMaster;
import com.efreightsuite.service.RegularExpressionService;
import com.efreightsuite.util.RegExpName;
import com.efreightsuite.util.ValidateUtil;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Log4j2
@Service
public class CurrencyRateMasterValidator {

    @Autowired
    private
    RegularExpressionService regExpService;

    @Autowired
    CurrencyMasterValidator currencyMasterValidator;

    @Autowired
    CountryMasterValidator countryMasterValidator;

    public void validate(CurrencyRateMaster currencyRateMaster) {

        log.info("CurrencyRateMaster Validation Begins.........["
                + (currencyRateMaster.getId() != null ? currencyRateMaster.getId() : "") + "]");

        // Validating Country
        //countryMasterValidator.validate(currencyRateMaster.getCountryMaster());

        // Validating FromCurrency
        //currencyMasterValidator.validate(currencyRateMaster.getFromCurrency());
        //log.debug("Successfully Validated FromCurrency In CurrencyRateMaster");

        // Validating ToCurrency
        //currencyMasterValidator.validate(currencyRateMaster.getToCurrency());
        //log.debug("Successfully Validated ToCurrency In CurrencyRateMaster");

        validateCurrencyRateMaster(currencyRateMaster);

        log.info("CurrencyRateMaster Validation Ends.........["
                + (currencyRateMaster.getId() != null ? currencyRateMaster.getId() : "") + "]");
    }

    private void validateCurrencyRateMaster(CurrencyRateMaster currencyRateMaster) {

        // validate date

        ValidateUtil.dateNotNull(currencyRateMaster.getCurrencyDate(), ErrorCode.CURRENCY_RATE_DATE_NOT_NULL);
        ValidateUtil.dateEmpty(currencyRateMaster.getCurrencyDate(), ErrorCode.CURRENCY_RATE_DATE_NOT_EMPTY);

        if (currencyRateMaster.getId() == null) {
            ValidateUtil.checkDate(currencyRateMaster.getCurrencyDate(), ErrorCode.CURRENCY_RATE_TODAY_DATE);
        }
        currencyRateMaster.getCurrencyDate().setHours(0);
        currencyRateMaster.getCurrencyDate().setMinutes(0);
        currencyRateMaster.getCurrencyDate().setSeconds(0);

        // validate BuyRate

        ValidateUtil.notNull(currencyRateMaster.getBuyRate(), ErrorCode.CURRENCY_RATE_BUY_RATE_NOT_NULL);

        ValidateUtil.nonZero(currencyRateMaster.getBuyRate(), ErrorCode.CURRENCY_RATE_BUY_RATE_NON_ZERO);

        ValidateUtil.checkDecimalLength(currencyRateMaster.getBuyRate(),
                ErrorCode.CURRENCY_RATE_BUY_RATE_LENGTH_INVALID);

        regExpService.validate(currencyRateMaster.getBuyRate(), RegExpName.Reg_Exp_Master_CURRENCY_BUY_RATE,
                ErrorCode.CURRENCY_RATE_BUYRATE_INVALID);

        // validate sell rate

        ValidateUtil.notNull(currencyRateMaster.getSellRate(), ErrorCode.CURRENCY_RATE_SELL_RATE_NOT_NULL);

        ValidateUtil.checkDecimalLength(currencyRateMaster.getSellRate(),
                ErrorCode.CURRENCY_RATE_SELL_RATE_LENGTH_INVALID);

        regExpService.validate(currencyRateMaster.getBuyRate(), RegExpName.Reg_Exp_Master_CURRENCY_SELL_RATE,
                ErrorCode.CURRENCY_RATE_SELLRATE_INVALID);

        if (currencyRateMaster.getBuyRate() > currencyRateMaster.getSellRate()) {

            throw new RestException(ErrorCode.CURRENCY_RATE_SELLRATE_GREATER_BUYRATE);
        }

		/*
         * if(currencyRateMaster.getBuyRate().equals(currencyRateMaster.
		 * getSellRate())){
		 * 
		 * throw new
		 * RestException(ErrorCode.CURRENCY_RATE_SELLRATE_BUYRATE_NOT_EQUAL);
		 * 
		 * }
		 */

    }

}
