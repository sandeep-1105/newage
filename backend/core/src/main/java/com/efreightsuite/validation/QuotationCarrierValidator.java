package com.efreightsuite.validation;

import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.model.CarrierMaster;
import com.efreightsuite.model.QuotationCarrier;
import com.efreightsuite.model.QuotationCharge;
import com.efreightsuite.repository.CarrierMasterRepository;
import com.efreightsuite.util.ValidateUtil;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class QuotationCarrierValidator {

    @Autowired
    private
    QuotationChargeValidator quotationChargeValidator;

    @Autowired
    private CarrierMasterRepository carrierMasterRepository;

    public void validate(QuotationCarrier quotationCarrier) {

        verifyCarrier(quotationCarrier);

        verifyTransitDays(quotationCarrier);

        verifyFrequency(quotationCarrier);

        if (quotationCarrier.getQuotationChargeList() != null && quotationCarrier.getQuotationChargeList().size() > 0) {
            for (QuotationCharge quotationCharge : quotationCarrier.getQuotationChargeList()) {
                quotationChargeValidator.validate(quotationCharge);
            }
        } else {
            throw new RestException(ErrorCode.QUOTATION_ATLEAST_ONE_CHARGE_REQUIRED);
        }

    }


    private void verifyCarrier(QuotationCarrier quotationCarrier) {

        if (quotationCarrier.getCarrierMaster() != null && quotationCarrier.getCarrierMaster().getId() != null) {
            CarrierMaster carrier = carrierMasterRepository.findById(quotationCarrier.getCarrierMaster().getId());

            ValidateUtil.isStatusBlocked(carrier.getStatus(), ErrorCode.QUOTATION_CARRIER_BLOCKED);

            ValidateUtil.isStatusHidden(carrier.getStatus(), ErrorCode.QUOTATION_CARRIER_HIDDEN);

            quotationCarrier.setCarrierCode(carrier.getCarrierCode());

        } else {
            quotationCarrier.setCarrierMaster(null);
            quotationCarrier.setCarrierCode(null);
        }

    }


    private void verifyTransitDays(QuotationCarrier quotationCarrier) {

        if (quotationCarrier.getTransitday() != null) {

            ValidateUtil.aboveRange(quotationCarrier.getTransitday(), 1000, ErrorCode.QUOTATION_TRANSIT_DAYS_ABOVE_RANGE);

            ValidateUtil.belowRange(quotationCarrier.getTransitday(), 0, ErrorCode.QUOTATION_TRANSIT_DAYS_NEGATIVE);

        }

    }

    private void verifyFrequency(QuotationCarrier quotationCarrier) {

        if (quotationCarrier.getFrequency() != null && quotationCarrier.getFrequency().getId() != null) {

            ValidateUtil.isStatusBlocked(quotationCarrier.getFrequency().getStatus(), ErrorCode.QUOTATION_FREQUENCY_BLOCKED);

            ValidateUtil.isStatusHidden(quotationCarrier.getFrequency().getStatus(), ErrorCode.QUOTATION_FREQUENCY_HIDDEN);

            quotationCarrier.setFrequencyCode(quotationCarrier.getFrequency().getFrequencyCode());
        } else {
            quotationCarrier.setFrequency(null);
            quotationCarrier.setFrequencyCode(null);
        }

    }

}
