package com.efreightsuite.validation;

import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.model.PeriodMaster;
import com.efreightsuite.util.ValidateUtil;

import org.springframework.stereotype.Service;

@Service
public class PeriodMasterValidator {

    public void validate(PeriodMaster periodMaster) {
        ValidateUtil.notNullOrEmpty(periodMaster.getFrequencyCode(), ErrorCode.PERIOD_FREQUENCY_CODE_REQUIRED);
        ValidateUtil.validateLength(periodMaster.getFrequencyCode(), 0, 10, ErrorCode.PERIOD_FREQUENCY_CODE_INVALID);
        ValidateUtil.notNullOrEmpty(periodMaster.getFrequencyName(), ErrorCode.PERIOD_FREQUENCY_NAME_REQUIRED);
        ValidateUtil.validateLength(periodMaster.getFrequencyName(), 0, 100, ErrorCode.PERIOD_FREQUENCY_NAME_INVALID);
    }
}
