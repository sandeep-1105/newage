package com.efreightsuite.validation;


import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.model.AirlineEdi;
import com.efreightsuite.model.AirlineEdiConnection;
import com.efreightsuite.model.AirlineEdiDimension;
import com.efreightsuite.model.AirlineEdiHouse;
import com.efreightsuite.service.RegularExpressionService;
import com.efreightsuite.util.RegExpName;
import com.efreightsuite.util.ValidateUtil;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class AirlineEdiValidator {


    @Autowired
    private
    RegularExpressionService regExpService;


    public void validate(AirlineEdi airlineEdi) {

        log.info("AirlineEdiValidator -> Validate method started......");

        if (airlineEdi.getLocationMaster() != null) {
            airlineEdi.setLocationCode(airlineEdi.getLocationMaster().getLocationCode());
        }

        if (airlineEdi.getCompanyMaster() != null) {
            airlineEdi.setCompanyCode(airlineEdi.getCompanyMaster().getCompanyCode());
        }

        if (airlineEdi.getCountryMaster() != null) {
            airlineEdi.setCountryCode(airlineEdi.getCountryMaster().getCountryCode());
        }

        ValidateUtil.notNull(airlineEdi.getCarrierMaster().getMessagingType(), ErrorCode.AIRLINE_EDI_CARRIER_NOT_ALLOWED);

        ValidateUtil.notNullOrEmpty(airlineEdi.getMawbNo(), ErrorCode.AIRLINE_EDI_MAWB_NO_REQUIRED);
        ValidateUtil.notNull(airlineEdi.getAwbDate(), ErrorCode.AIRLINE_EDI_MAWB_DATE_REQUIRED);

        ValidateUtil.notNull(airlineEdi.getOrigin(), ErrorCode.AIRLINE_EDI_ORIGIN_REQUIRED);
        ValidateUtil.notNull(airlineEdi.getOrigin().getId(), ErrorCode.AIRLINE_EDI_ORIGIN_REQUIRED);
        airlineEdi.setOriginCode(airlineEdi.getOrigin().getPortCode());

        ValidateUtil.notNull(airlineEdi.getDestination(), ErrorCode.AIRLINE_EDI_DESTINATION_REQUIRED);
        ValidateUtil.notNull(airlineEdi.getDestination().getId(), ErrorCode.AIRLINE_EDI_DESTINATION_REQUIRED);
        airlineEdi.setDestCode(airlineEdi.getDestination().getPortCode());

        ValidateUtil.notNull(airlineEdi.getPol(), ErrorCode.AIRLINE_EDI_POL_REQUIRED);
        ValidateUtil.notNull(airlineEdi.getPol().getId(), ErrorCode.AIRLINE_EDI_POL_REQUIRED);
        airlineEdi.setPolCode(airlineEdi.getPolCode());

        ValidateUtil.notNull(airlineEdi.getPod(), ErrorCode.AIRLINE_EDI_POD_REQUIRED);
        ValidateUtil.notNull(airlineEdi.getPod().getId(), ErrorCode.AIRLINE_EDI_POD_REQUIRED);
        airlineEdi.setPodCode(airlineEdi.getPod().getPortCode());

        ValidateUtil.notNull(airlineEdi.getCarrierMaster(), ErrorCode.AIRLINE_EDI_CARRIER_REQUIRED);
        ValidateUtil.notNull(airlineEdi.getCarrierMaster().getId(), ErrorCode.AIRLINE_EDI_CARRIER_REQUIRED);
        airlineEdi.setCarrierCode(airlineEdi.getCarrierMaster().getCarrierCode());

        ValidateUtil.notNullOrEmpty(airlineEdi.getFlightNumber(), ErrorCode.AIRLINE_EDI_FLIGHT_NO_REQUIRED);
        // ValidateUtil.checkPattern(airlineEdi.getFlightNumber(), "airline.edi.flightNumber", true);
        regExpService.validate(airlineEdi.getFlightNumber(), RegExpName.REG_EXP_AIRLINE_FLIGHT_NUMBER, ErrorCode.AIRLINE_EDI_FLIGHT_NO_INVALID);

        ValidateUtil.notNull(airlineEdi.getNoOfPieces(), ErrorCode.AIRLINE_EDI_NO_OF_PIECES_REQUIRED);
        ValidateUtil.notNull(airlineEdi.getVolumeWeight(), ErrorCode.AIRLINE_EDI_VOLUME_WEIGHT_REQUIRED);
        ValidateUtil.notNull(airlineEdi.getGrossWeight(), ErrorCode.AIRLINE_EDI_GROSS_WEIGHT_REQUIRED);
        ValidateUtil.notNull(airlineEdi.getChargebleWeight(), ErrorCode.AIRLINE_EDI_CHARGEABLE_WEIGHT_REQUIRED);
        ValidateUtil.notNullOrEmpty(airlineEdi.getCommodityDescription(), ErrorCode.AIRLINE_EDI_COMMODITY_DESCRIPTION_REQUIRED);

        if (airlineEdi.getCommodityMaster() != null) {
            airlineEdi.setCompanyCode(airlineEdi.getCommodityMaster().getHsCode());
        }

        if (airlineEdi.getCurrencyMaster() != null) {
            airlineEdi.setCurrencyCode(airlineEdi.getCurrencyMaster().getCurrencyCode());
        }

        ValidateUtil.notNull(airlineEdi.getShipper(), ErrorCode.AIRLINE_EDI_SHIPPER_REQUIRED);
        ValidateUtil.notNull(airlineEdi.getShipper().getId(), ErrorCode.AIRLINE_EDI_SHIPPER_REQUIRED);
        ValidateUtil.notNull(airlineEdi.getShipperAddress().getCountry(), ErrorCode.AIRLINE_EDI_SHIPPER_ADDRESS_REQUIRED);
        ValidateUtil.notNull(airlineEdi.getShipperAddress(), ErrorCode.AIRLINE_EDI_SHIPPER_ADDRESS_REQUIRED);
        ValidateUtil.notNull(airlineEdi.getShipperAddress().getCity(), ErrorCode.AIRLINE_EDI_SHIPPER_CITY_REQUIRED);
        airlineEdi.setShipperCode(airlineEdi.getShipper().getPartyCode());
        if (airlineEdi.getShipperAddress().getCity() != null) {
            airlineEdi.getShipperAddress().setCityCode(airlineEdi.getShipperAddress().getCity().getCityCode());
        }

        if (airlineEdi.getShipperAddress().getState() != null) {
            airlineEdi.getShipperAddress().setStateCode(airlineEdi.getShipperAddress().getStateCode());
        }

        if (airlineEdi.getShipperAddress().getCountry() != null) {
            airlineEdi.getShipperAddress().setCountryCode(airlineEdi.getShipperAddress().getCountryCode());
        }

        ValidateUtil.notNull(airlineEdi.getConsignee(), ErrorCode.AIRLINE_EDI_CONSIGNEE_REQUIRED);
        ValidateUtil.notNull(airlineEdi.getConsignee().getId(), ErrorCode.AIRLINE_EDI_CONSIGNEE_REQUIRED);
        ValidateUtil.notNull(airlineEdi.getConsigneeAddress().getCountry(), ErrorCode.AIRLINE_EDI_CONSIGNEE_ADDRESS_REQUIRED);
        ValidateUtil.notNull(airlineEdi.getConsigneeAddress(), ErrorCode.AIRLINE_EDI_CONSIGNEE_ADDRESS_REQUIRED);
        ValidateUtil.notNull(airlineEdi.getConsigneeAddress().getCity(), ErrorCode.AIRLINE_EDI_CONSIGNEE_CITY_REQUIRED);
        airlineEdi.setConsigneeCode(airlineEdi.getConsignee().getPartyCode());
        if (airlineEdi.getConsigneeAddress().getCity() != null) {
            airlineEdi.getConsigneeAddress().setCityCode(airlineEdi.getConsigneeAddress().getCity().getCityCode());
        }

        if (airlineEdi.getConsigneeAddress().getState() != null) {
            airlineEdi.getConsigneeAddress().setStateCode(airlineEdi.getConsigneeAddress().getStateCode());
        }

        if (airlineEdi.getConsigneeAddress().getCountry() != null) {
            airlineEdi.getConsigneeAddress().setCountryCode(airlineEdi.getConsigneeAddress().getCountryCode());
        }

        ValidateUtil.notNull(airlineEdi.getAgent(), ErrorCode.AIRLINE_EDI_AGENT_REQUIRED);
        ValidateUtil.notNull(airlineEdi.getAgent().getId(), ErrorCode.AIRLINE_EDI_AGENT_REQUIRED);
        ValidateUtil.notNull(airlineEdi.getAgentAddress().getCountry(), ErrorCode.AIRLINE_EDI_AGENT_ADDRESS_REQUIRED);
        ValidateUtil.notNull(airlineEdi.getAgentAddress(), ErrorCode.AIRLINE_EDI_AGENT_ADDRESS_REQUIRED);
        ValidateUtil.notNull(airlineEdi.getAgentAddress().getCity(), ErrorCode.AIRLINE_EDI_AGENT_CITY_REQUIRED);
        ValidateUtil.notNull(airlineEdi.getAgent().getPartyDetail(), ErrorCode.AIRLINE_EDI_AGENT_REQUIRED);
        ValidateUtil.notNullOrEmpty(airlineEdi.getAgent().getPartyDetail().getIataCode(), ErrorCode.AIRLINE_EDI_AGENT_ACCOUNT_REQUIRED);
        airlineEdi.setAgentCode(airlineEdi.getAgent().getPartyCode());
        if (airlineEdi.getAgentAddress().getCity() != null) {
            airlineEdi.getAgentAddress().setCityCode(airlineEdi.getAgentAddress().getCity().getCityCode());
        }

        if (airlineEdi.getAgentAddress().getState() != null) {
            airlineEdi.getAgentAddress().setStateCode(airlineEdi.getAgentAddress().getStateCode());
        }

        if (airlineEdi.getAgentAddress().getCountry() != null) {
            airlineEdi.getAgentAddress().setCountryCode(airlineEdi.getAgentAddress().getCountryCode());
        }
        log.info("AirlineEdiValidator -> Validate method finished......");

        //House validations
        if (airlineEdi.getHouseList() != null && airlineEdi.getHouseList().size() > 0) {

            for (AirlineEdiHouse house : airlineEdi.getHouseList()) {

                ValidateUtil.notNull(house.getCommodityDescription(), ErrorCode.AIRLINE_EDI_HOUSE_COMMODITY_REQUIRED);


                ValidateUtil.notNull(house.getShipper(), ErrorCode.AIRLINE_EDI_HOUSE_SHIPPER_REQUIRED);
                ValidateUtil.notNull(house.getShipperAddress(), ErrorCode.AIRLINE_EDI_HOUSE_SHIPPER_ADDRESS_REQUIRED);
                ValidateUtil.notNull(house.getShipper().getCountryMaster(), ErrorCode.AIRLINE_EDI_HOUSE_SHIPPER_COUNTRY_REQUIRED);
                ValidateUtil.notNull(house.getShipperAddress().getCity(), ErrorCode.AIRLINE_EDI_HOUSE_SHIPPER_CITY_REQUIRED);
                house.setShipperCode(house.getShipper().getPartyCode());

                ValidateUtil.notNull(house.getConsignee(), ErrorCode.AIRLINE_EDI_CONSIGNEE_REQUIRED);
                ValidateUtil.notNull(house.getConsigneeAddress(), ErrorCode.AIRLINE_EDI_HOUSE_CONSIGNEE_ADDRESS_REQUIRED);
                ValidateUtil.notNull(house.getConsignee().getCountryMaster(), ErrorCode.AIRLINE_EDI_HOUSE_CONSIGNEE_COUNTRY_REQUIRED);
                ValidateUtil.notNull(house.getConsigneeAddress().getCity(), ErrorCode.AIRLINE_EDI_HOUSE_CONSIGNEE_CITY_REQUIRED);
                house.setConsigneeCode(house.getConsignee().getPartyCode());

                if (house.getCompanyMaster() != null) {
                    house.setCompanyCode(house.getCompanyMaster().getCompanyCode());
                }

                if (house.getLocationMaster() != null) {
                    house.setLocationCode(house.getLocationMaster().getLocationCode());
                }

                if (house.getCountryMaster() != null) {
                    house.setCountryCode(house.getCountryMaster().getCountryCode());
                }

                if (house.getOrigin() != null) {
                    house.setOriginCode(house.getOrigin().getPortCode());
                }

                if (house.getDestination() != null) {
                    house.setDestinationCode(house.getDestination().getPortCode());
                }

                if (house.getAgent() != null) {
                    house.setAgentCode(house.getAgent().getPartyCode());
                }

                if (house.getTosMaster() != null) {
                    house.setTosCode(house.getTosMaster().getTosCode());
                }

                if (house.getCommodityMaster() != null) {
                    house.setCommodityCode(house.getCommodityMaster().getHsCode());
                }

//				ValidateUtil.notNull(house.getAgentAddress(), ErrorCode.AIRLINE_EDI_HOUSE_AGENT_ADDRESS_REQUIRED);
//				ValidateUtil.notNull(house.getAgent().getCountryMaster(), ErrorCode.AIRLINE_EDI_HOUSE_AGENT_COUNTRY_REQUIRED);
//				ValidateUtil.notNull(house.getAgentAddress().getCity(), ErrorCode.AIRLINE_EDI_HOUSE_AGENT_CITY_REQUIRED);
            }

        }

        if (airlineEdi.getConnectionList() != null && airlineEdi.getConnectionList().size() != 0) {
            for (AirlineEdiConnection conn : airlineEdi.getConnectionList()) {
                if (conn.getCompanyMaster() != null) {
                    conn.setCompanyCode(conn.getCompanyMaster().getCompanyCode());
                }

                if (conn.getLocationMaster() != null) {
                    conn.setLocationCode(conn.getLocationMaster().getLocationCode());
                }

                if (conn.getCountryMaster() != null) {
                    conn.setCountryCode(conn.getCountryMaster().getCountryCode());
                }

                if (conn.getPod() != null) {
                    conn.setPodCode(conn.getPod().getPortCode());
                }

                if (conn.getPol() != null) {
                    conn.setPolCode(conn.getPol().getPortCode());
                }
                if (conn.getCarrierMaster() != null) {
                    conn.setCarrierCode(conn.getCarrierMaster().getCarrierCode());
                }
            }
        }

        if (airlineEdi.getDimensionList() != null && airlineEdi.getDimensionList().size() != 0) {
            for (AirlineEdiDimension conn : airlineEdi.getDimensionList()) {
                if (conn.getCompanyMaster() != null) {
                    conn.setCompanyCode(conn.getCompanyMaster().getCompanyCode());
                }

                if (conn.getLocationMaster() != null) {
                    conn.setLocationCode(conn.getLocationMaster().getLocationCode());
                }

                if (conn.getCountryMaster() != null) {
                    conn.setCountryCode(conn.getCountryMaster().getCountryCode());
                }


            }
        }
    }

}
