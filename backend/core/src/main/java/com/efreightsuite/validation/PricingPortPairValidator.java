package com.efreightsuite.validation;

import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.model.PricingPortPair;
import com.efreightsuite.service.AuthService;
import com.efreightsuite.service.RegularExpressionService;
import com.efreightsuite.util.RegExpName;
import com.efreightsuite.util.ValidateUtil;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PricingPortPairValidator {

    @Autowired
    private
    RegularExpressionService regExpService;


    public void validate(PricingPortPair pricingPortPair) {

        pricingPortPair.setCompanyMaster(AuthService.getCurrentUser().getSelectedUserLocation().getCompanyMaster());
        pricingPortPair.setCompanyCode(AuthService.getCurrentUser().getSelectedUserLocation().getCompanyMaster().getCompanyCode());
        pricingPortPair.setLocationMaster(AuthService.getCurrentUser().getSelectedUserLocation());
        pricingPortPair.setLocationCode(AuthService.getCurrentUser().getSelectedUserLocation().getLocationCode());
        pricingPortPair.setCountryMaster(AuthService.getCurrentUser().getSelectedUserLocation().getCountryMaster());
        pricingPortPair.setCountryCode(AuthService.getCurrentUser().getSelectedUserLocation().getCountryMaster().getCountryCode());


        ValidateUtil.notNull(pricingPortPair.getChargeMaster(), ErrorCode.PRICING_MASTER_CHARGE_REQUIRED);
        ValidateUtil.notNull(pricingPortPair.getChargeMaster().getId(), ErrorCode.PRICING_MASTER_CHARGE_REQUIRED);
        ValidateUtil.isStatusBlocked(pricingPortPair.getChargeMaster().getStatus(), ErrorCode.PRICING_MASTER_CHARGE_BLOCKED);
        ValidateUtil.isStatusHidden(pricingPortPair.getChargeMaster().getStatus(), ErrorCode.PRICING_MASTER_CHARGE_HIDDEN);
        pricingPortPair.setChargeCode(pricingPortPair.getChargeMaster().getChargeCode());

        ValidateUtil.notNull(pricingPortPair.getUnitMaster(), ErrorCode.PRICING_MASTER_UNIT_REQUIRED);
        ValidateUtil.notNull(pricingPortPair.getUnitMaster().getId(), ErrorCode.PRICING_MASTER_UNIT_REQUIRED);
        ValidateUtil.isStatusBlocked(pricingPortPair.getUnitMaster().getStatus(), ErrorCode.PRICING_MASTER_UNIT_BLOCKED);
        ValidateUtil.isStatusHidden(pricingPortPair.getUnitMaster().getStatus(), ErrorCode.PRICING_MASTER_UNIT_HIDDEN);
        pricingPortPair.setUnitCode(pricingPortPair.getUnitMaster().getUnitCode());

        ValidateUtil.notNull(pricingPortPair.getCurrencyMaster(), ErrorCode.PRICING_MASTER_CURRENCY_REQUIRED);
        ValidateUtil.notNull(pricingPortPair.getCurrencyMaster().getId(), ErrorCode.PRICING_MASTER_CURRENCY_REQUIRED);
        ValidateUtil.isStatusBlocked(pricingPortPair.getCurrencyMaster().getStatus(), ErrorCode.PRICING_MASTER_CURRENCY_BLOCKED);
        ValidateUtil.isStatusHidden(pricingPortPair.getCurrencyMaster().getStatus(), ErrorCode.PRICING_MASTER_CURRENCY_HIDDEN);
        pricingPortPair.setCurrencyCode(pricingPortPair.getCurrencyMaster().getCurrencyCode());


        ValidateUtil.notNull(pricingPortPair.getValidFromDate(), ErrorCode.PRICING_MASTER_FROMDATE_REQUIRED);
        ValidateUtil.notNull(pricingPortPair.getValidToDate(), ErrorCode.PRICING_MASTER_TODATE_REQUIRED);

        //ValidateUtil.notNull(pricingPortPair.getStdSelInMinimum(), ErrorCode.PRICING_MASTER_STDSEL_MINIMUM_REQUIRED);

        ValidateUtil.notNull(pricingPortPair.getStdSelInAmount(), ErrorCode.PRICING_MASTER_STDSELLAMOUNT_REQUIRED);


        regExpService.validate(pricingPortPair.getStdSelInAmount(), RegExpName.Reg_Exp_AMOUNT_TEN_SIX, ErrorCode.PRICING_MASTER_STDSEL_AMOUNT_INVALID);
        ValidateUtil.nonZero(pricingPortPair.getStdSelInAmount(), ErrorCode.PRICING_MASTER_STDSEL_AMOUNT_NOT_ZERO);

        if (pricingPortPair.getStdSelInMinimum() != null) {
            regExpService.validate(pricingPortPair.getStdSelInMinimum(), RegExpName.Reg_Exp_AMOUNT_TEN_SIX, ErrorCode.PRICING_MASTER_STDSELIN_MINIMUM_INVALID);
            //ValidateUtil.nonZero(pricingPortPair.getStdSelInMinimum(), ErrorCode.PRICING_MASTER_STDSEL_AMOUNT_NOT_ZERO);
        }

        if (pricingPortPair.getMinSelInMinimum() != null) {
            regExpService.validate(pricingPortPair.getMinSelInMinimum(), RegExpName.Reg_Exp_AMOUNT_TEN_SIX, ErrorCode.PRICING_MASTER_MINSELIN_MINIMUM_INVALID);
            ValidateUtil.nonZero(pricingPortPair.getMinSelInMinimum(), ErrorCode.PRICING_MASTER_MINSELIN_MINIMUM_NOT_ZERO);
        }

        if (pricingPortPair.getMinSelInAmount() != null) {
            regExpService.validate(pricingPortPair.getMinSelInAmount(), RegExpName.Reg_Exp_AMOUNT_TEN_SIX, ErrorCode.PRICING_MASTER_MINSELAMOUNT_INVALID);
            ValidateUtil.nonZero(pricingPortPair.getMinSelInAmount(), ErrorCode.PRICING_MASTER_MINSELAMOUNT_NOT_ZERO);
        }

        if (pricingPortPair.getCostInMinimum() != null) {
            regExpService.validate(pricingPortPair.getCostInMinimum(), RegExpName.Reg_Exp_AMOUNT_TEN_SIX, ErrorCode.PRICING_MASTER_COST_MINIMUM_INVALID);
            //ValidateUtil.nonZero(pricingPortPair.getCostInMinimum(), ErrorCode.PRICING_MASTER_COST_MINIMUM_NOT_ZERO);
        }

        if (pricingPortPair.getCostInAmount() != null) {
            regExpService.validate(pricingPortPair.getCostInAmount(), RegExpName.Reg_Exp_AMOUNT_TEN_SIX, ErrorCode.PRICING_MASTER_COST_AMOUNT_INVALID);
            ValidateUtil.nonZero(pricingPortPair.getCostInAmount(), ErrorCode.PRICING_MASTER_COST_AMOUNT_NOT_ZERO);

        }

		/*ValidateUtil.notNull(pricingPortPair.getMinSelInAmount(), ErrorCode.PRICING_MASTER_MINSELAMOUNT_REQUIRED);
		ValidateUtil.notNull(pricingPortPair.getCostInAmount(), ErrorCode.PRICING_MASTER_COSTAMOUNT_REQUIRED);*/

    }

}
