package com.efreightsuite.validation;

import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.model.LogoMaster;
import com.efreightsuite.service.LocationMasterService;
import com.efreightsuite.util.ValidateUtil;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class LogoMasterValidator {

    @Autowired
    LocationMasterService locationMasterService;

    public void validateToUpdate(LogoMaster logoMaster) {
        // TODO Auto-generated method stub

        ValidateUtil.notNull(logoMaster.getLocationMaster(), ErrorCode.LOGOMASTER_LOCATION);
        ValidateUtil.notNull(logoMaster.getLocationMaster().getId(), ErrorCode.LOGOMASTER_LOCATION);


        ValidateUtil.notNull(logoMaster.getLogo(), ErrorCode.LOGOMASTER_LOGO);

        logoMaster.setLocationCode(logoMaster.getLocationMaster().getLocationCode());

        logoMaster.setCountryMaster(logoMaster.getLocationMaster().getCountryMaster());
        logoMaster.setCountryCode(logoMaster.getLocationMaster().getCountryMaster().getCountryCode());

        logoMaster.setCompanyMaster(logoMaster.getLocationMaster().getCompanyMaster());
        logoMaster.setCompanyCode(logoMaster.getLocationMaster().getCompanyMaster().getCompanyCode());
    }

}
