package com.efreightsuite.validation;

import com.efreightsuite.enumeration.LovStatus;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.model.CategoryMaster;
import com.efreightsuite.service.RegularExpressionService;
import com.efreightsuite.util.RegExpName;
import com.efreightsuite.util.ValidateUtil;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class CategoryMasterValidator {

    @Autowired
    private
    RegularExpressionService regExpService;

    public void validate(CategoryMaster categoryMaster) {

        log.info("CategoryMaster Validation Started....["
                + (categoryMaster.getId() != null ? categoryMaster.getId() : "") + "]");

        ValidateUtil.notNull(categoryMaster, ErrorCode.CATEGORY_NOT_NULL);

        validateCategoryMasterCode(categoryMaster);

        validateCategoryMasterName(categoryMaster);

        ValidateUtil.validateEnum(LovStatus.class, categoryMaster.getStatus(), ErrorCode.CATEGORY_STATUS_NOT_NULL);

        log.info("CategoryMaster Validation Finished....["
                + (categoryMaster.getId() != null ? categoryMaster.getId() : "") + "]");
    }

    private void validateCategoryMasterCode(CategoryMaster categoryMaster) {

        ValidateUtil.notNullOrEmpty(categoryMaster.getCategoryCode(), ErrorCode.CATEGORY_CODE_NOT_NULL);

        ValidateUtil.validateLength(categoryMaster.getCategoryCode(), 1, 10, ErrorCode.CATEGORY_CODE_LENGTH_EXCEED);

        regExpService.validate(categoryMaster.getCategoryCode(), RegExpName.Reg_Exp_Master_CATEGORY_CODE, ErrorCode.CATEGORY_CODE_INVALID);

        log.debug("Category  Code Validated...");

    }

    private void validateCategoryMasterName(CategoryMaster categoryMaster) {

        ValidateUtil.notNullOrEmpty(categoryMaster.getCategoryName(), ErrorCode.CATEGORY_NAME_NOT_NULL);

        ValidateUtil.validateLength(categoryMaster.getCategoryName(), 1, 100, ErrorCode.CATEGORY_NAME_LENGTH_EXCEED);

        regExpService.validate(categoryMaster.getCategoryName(), RegExpName.Reg_Exp_Master_CATEGORY_NAME, ErrorCode.CATEGORY_NAME_INVALID);

        log.debug("Category Name Validated...");

    }

}
