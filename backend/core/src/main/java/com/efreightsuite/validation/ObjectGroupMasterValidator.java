package com.efreightsuite.validation;

import com.efreightsuite.enumeration.LovStatus;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.model.ObjectGroupMaster;
import com.efreightsuite.util.ValidateUtil;
import lombok.extern.log4j.Log4j2;

import org.springframework.stereotype.Service;

@Service
@Log4j2
public class ObjectGroupMasterValidator {


    public void validate(ObjectGroupMaster objectGroupMaster) {
        log.info("ObjectGroup Master Validation Started...");
        if (objectGroupMaster != null) {

            ValidateUtil.notNull(objectGroupMaster.getObjectGroupName(), ErrorCode.OBJECTGROUP_NAME_NOT_NULL);
            ValidateUtil.notNull(objectGroupMaster.getObjectGroupCode(), ErrorCode.OBJECTGROUP_CODE_NOT_NULL);

            if (objectGroupMaster.getStatus() == null) {
                objectGroupMaster.setStatus(LovStatus.Active);
            }

        } else {
            throw new RestException(ErrorCode.OBJECTGROUP_CODE_NOT_NULL);
        }


        log.info("ObjectGroup Master Validation Finished....[" + (objectGroupMaster.getId() != null ? objectGroupMaster.getId() : "") + "]");
    }


}
