package com.efreightsuite.validation;

import com.efreightsuite.enumeration.LovStatus;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.model.DocumentMaster;
import com.efreightsuite.util.ValidateUtil;
import lombok.extern.log4j.Log4j2;

import org.springframework.stereotype.Service;

@Log4j2
@Service
public class DocumentMasterValidator {

    public void validate(DocumentMaster documentMaster) {

        log.info("DocumentMasterValidator Started for Document : [" + documentMaster.getId() + "]");

        // Document Code Validation

        ValidateUtil.notNull(documentMaster.getDocumentCode(), ErrorCode.DOCUMENT_CODE_NOT_NULL);

        ValidateUtil.checkPattern(documentMaster.getDocumentCode(), "documentCode", true);

        log.info("Document Code validated...");

        // Document Name Validations

        ValidateUtil.notNull(documentMaster.getDocumentName(), ErrorCode.DOCUMENT_NAME_NOT_NULL);

        ValidateUtil.checkPattern(documentMaster.getDocumentName(), "documentName", true);

        log.info("Document Name validated...");

        if (documentMaster.getId() == null) {
            if (documentMaster.getStatus() == null) {
                documentMaster.setStatus(LovStatus.Active);
            }
        }

        ValidateUtil.notNull(documentMaster.getStatus(), ErrorCode.DOCUMENT_LOV_STATUS_NOT_NULL);

        ValidateUtil.validateEnum(LovStatus.class, documentMaster.getStatus(),
                ErrorCode.DOCUMENT_LOV_STATUS_INVALID);

        log.info("Document Status Successfully Validated.....");

        log.info("DocumentMasterValidator Finished for Document : [" + documentMaster.getId() + "]");
    }
}

