package com.efreightsuite.validation;

import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.model.ServiceTypeMaster;
import com.efreightsuite.service.RegularExpressionService;
import com.efreightsuite.util.RegExpName;
import com.efreightsuite.util.ValidateUtil;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class ServiceTypeMasterValidator {

    @Autowired
    private
    RegularExpressionService regExpService;

    public void validate(ServiceTypeMaster serviceTypeMaster) {

        if (serviceTypeMaster != null) {

            ValidateUtil.notNull(serviceTypeMaster.getServiceTypeName(), ErrorCode.SERVICETYPE_NAME_NOT_NULL);
            regExpService.validate(serviceTypeMaster.getServiceTypeName(), RegExpName.Reg_Exp_Master_SERVICE_TYPE_NAME, ErrorCode.SERVICETYPE_NAME_INVALID);
            ValidateUtil.notNull(serviceTypeMaster.getServiceTypeCode(), ErrorCode.SERVICETYPE_CODE_NOT_NULL);
            regExpService.validate(serviceTypeMaster.getServiceTypeCode(), RegExpName.Reg_Exp_Master_SERVICE_TYPE_CODE, ErrorCode.SERVICETYPE_CODE_INVALID);
            ValidateUtil.notNull(serviceTypeMaster.getServiceType(), ErrorCode.SERVICE_TYPE_MANDATORY);

            log.info("CostCenterMaster Validation Started....["
                    + (serviceTypeMaster.getId() != null ? serviceTypeMaster
                    .getId() : "") + "]");

            log.info("CostCenterMaster Validation Finished....["
                    + (serviceTypeMaster.getId() != null ? serviceTypeMaster
                    .getId() : "") + "]");
        }

    }
}
