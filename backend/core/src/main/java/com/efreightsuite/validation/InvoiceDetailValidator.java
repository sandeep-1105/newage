package com.efreightsuite.validation;

import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.model.GeneralLedgerChargeMappingMaster;
import com.efreightsuite.model.InvoiceCreditNoteDetail;
import com.efreightsuite.model.ServiceMaster;
import com.efreightsuite.model.ShipmentServiceDetail;
import com.efreightsuite.repository.GeneralLedgerChargeMappingRepository;
import com.efreightsuite.repository.ShipmentServiceDetailRepository;
import com.efreightsuite.service.RegularExpressionService;
import com.efreightsuite.util.RegExpName;
import com.efreightsuite.util.ValidateUtil;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author user
 */
@Service
@Log4j2
public class InvoiceDetailValidator {

    @Autowired
    private
    RegularExpressionService regExpService;

    @Autowired
    private
    GeneralLedgerChargeMappingRepository glChargeMappingRepository;

    @Autowired
    private
    ShipmentServiceDetailRepository shipmentServiceDetailRepository;

    public void validate(InvoiceCreditNoteDetail invoiceDetail) {
        verifyServiceMaster(invoiceDetail);
        verifyChargeMaster(invoiceDetail);
        verifyChargeName(invoiceDetail);
        // verifyCrn(invoiceCharge);
        verifyCurrencyMaster(invoiceDetail);
        verifyRoe(invoiceDetail);
        verifyUnit(invoiceDetail);
        // verifyAmountPerUnit(invoiceCharge);
        verifyAmount(invoiceDetail);
        verifyLocalAmount(invoiceDetail);
        verifyBillingAmount(invoiceDetail);
        verifyDebitCredit(invoiceDetail);
        verifyIsTaxable(invoiceDetail);

    }

    public void verifyChargeisAssosiateWithGl(InvoiceCreditNoteDetail invoiceDetail, BaseDto baseDto) {
        if (invoiceDetail.getChargeMaster() != null && invoiceDetail.getShipmentServiceDetail() != null) {
            ServiceMaster serviceMaster = invoiceDetail.getShipmentServiceDetail().getServiceMaster();
            if (invoiceDetail.getShipmentServiceDetail().getServiceMaster() == null) {
                ShipmentServiceDetail shipmentServiceDetail = shipmentServiceDetailRepository.getOne(invoiceDetail.getShipmentServiceDetail().getId());
                if (shipmentServiceDetail != null && shipmentServiceDetail.getServiceMaster() != null) {
                    serviceMaster = shipmentServiceDetail.getServiceMaster();
                }
            }
            GeneralLedgerChargeMappingMaster glChargeMapping = glChargeMappingRepository.findByChargeAndService(invoiceDetail.getChargeMaster().getId(), serviceMaster.getId());
            if (glChargeMapping == null || glChargeMapping.getId() == null) {
                baseDto.getParams().add(invoiceDetail.getChargeMaster().getChargeCode());
                throw new RestException(ErrorCode.INVOICE_CHARGE_NOT_ASSOSIATE_WITH_GL_CHARGE);
            } else {
                invoiceDetail.setGlChargeMappingId(glChargeMapping.getId());
            }
        }
    }

    private void verifyIsTaxable(InvoiceCreditNoteDetail invoiceCharge) {
        if (invoiceCharge.getIsTaxable() == null) {
            invoiceCharge.setIsTaxable(false);
        }
    }

    private void verifyDebitCredit(InvoiceCreditNoteDetail invoiceCharge) {
        ValidateUtil.notNull(invoiceCharge.getDebitCredit(), ErrorCode.INVOICE_CHARGE_DEBIT_CREDIT);

    }

    private void verifyBillingAmount(InvoiceCreditNoteDetail invoiceCharge) {
        ValidateUtil.notNull(invoiceCharge.getCurrencyAmount(), ErrorCode.INVOICE_CHARGE_BILLING_AMOUNT);

    }

    private void verifyLocalAmount(InvoiceCreditNoteDetail invoiceCharge) {
        ValidateUtil.notNull(invoiceCharge.getLocalAmount(), ErrorCode.INVOICE_CHARGE_LOCAL_AMOUNT);

    }

    private void verifyAmount(InvoiceCreditNoteDetail invoiceCharge) {
        ValidateUtil.notNull(invoiceCharge.getAmount(), ErrorCode.INVOICE_CHARGE_AMOUNT);

    }

    private void verifyAmountPerUnit(InvoiceCreditNoteDetail invoiceCharge) {
        ValidateUtil.notNull(invoiceCharge.getAmountPerUnit(), ErrorCode.INVOICE_CHARGE_AMOUNT_PER_UNIT);

    }

    private void verifyUnit(InvoiceCreditNoteDetail invoiceCharge) {
        // ValidateUtil.notNull(invoiceCharge.getUnit(),
        // ErrorCode.INVOICE_CHARGE_UNIT);
    }

    private void verifyRoe(InvoiceCreditNoteDetail invoiceCharge) {
        if (invoiceCharge.getRoe() != null && invoiceCharge.getRoe() <= 0) {
            throw new RestException(ErrorCode.INVOICE_ROE_CANNOT_ZERO_OR_NEGATIVE);
        }
    }

    private void verifyServiceMaster(InvoiceCreditNoteDetail invoiceCharge) {

        if (invoiceCharge.getShipmentServiceDetail() != null) {
            ShipmentServiceDetail tmpObj = shipmentServiceDetailRepository.findById(invoiceCharge.getShipmentServiceDetail().getId());
            invoiceCharge.setServiceUid(tmpObj.getServiceUid());
        }
    }

    private void verifyChargeMaster(InvoiceCreditNoteDetail invoiceCharge) {
        ValidateUtil.notNull(invoiceCharge.getChargeMaster(), ErrorCode.INVOICE_CHARGE_CHARGE);
        ValidateUtil.notNull(invoiceCharge.getChargeMaster().getId(), ErrorCode.INVOICE_CHARGE_CHARGE);
        ValidateUtil.notNullOrEmpty(invoiceCharge.getChargeMaster().getChargeCode(), ErrorCode.INVOICE_CHARGE_CHARGE);
        ValidateUtil.isStatusBlocked(invoiceCharge.getChargeMaster().getStatus(),
                ErrorCode.INVOICE_CHARGE_CHARGE_BLOCK);
        ValidateUtil.isStatusHidden(invoiceCharge.getChargeMaster().getStatus(), ErrorCode.INVOICE_CHARGE_CHARGE_HIDE);
    }

    private void verifyChargeName(InvoiceCreditNoteDetail invoiceCharge) {
        ValidateUtil.notNull(invoiceCharge.getChargeName(), ErrorCode.INVOICE_CHARGE_NAME_REQUIRED);
        regExpService.validate(invoiceCharge.getChargeName(), RegExpName.Reg_Exp_Master_Charge_Name,
                ErrorCode.INVOICE_CHARGE_NAME_INVALID);
    }

    private void verifyCrn(InvoiceCreditNoteDetail invoiceCharge) {
        ValidateUtil.notNull(invoiceCharge.getCrn(), ErrorCode.INVOICE_CHARGE_CRN);

    }

    private void verifyCurrencyMaster(InvoiceCreditNoteDetail invoiceCharge) {
        if (invoiceCharge.getCurrencyMaster() != null && invoiceCharge.getCurrencyMaster().getId() != null) {
            ValidateUtil.isStatusBlocked(invoiceCharge.getCurrencyMaster().getStatus(),
                    ErrorCode.INVOICE_CHARGE_CURRENCY_BLOCK);
            ValidateUtil.isStatusHidden(invoiceCharge.getCurrencyMaster().getStatus(),
                    ErrorCode.INVOICE_CHARGE_CURRENCY_HIDE);
            invoiceCharge.setCurrencyCode(invoiceCharge.getCurrencyMaster().getCurrencyCode());
        }

    }

}
