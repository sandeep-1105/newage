package com.efreightsuite.validation;

import com.efreightsuite.dto.FileUploadDto;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.model.CarrierMaster;
import com.efreightsuite.model.CarrierRateMaster;
import com.efreightsuite.model.ChargeMaster;
import com.efreightsuite.model.PortMaster;
import com.efreightsuite.model.UnitMaster;
import com.efreightsuite.repository.DefaultMasterDataRepository;
import com.efreightsuite.service.AuthService;
import com.efreightsuite.service.RegularExpressionService;
import com.efreightsuite.util.AppUtil;
import com.efreightsuite.util.RegExpName;
import com.efreightsuite.util.ValidateUtil;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
@Log4j2
public class CarrierRateMasterValidator {

    @Autowired
    private
    AppUtil appUtil;

    @Autowired
    DefaultMasterDataRepository defaultMasterDataRepository;

    @Autowired
    private
    RegularExpressionService regExpService;

    public void checkValidation(CarrierRateMaster carrierRateMaster) {

        ValidateUtil.notNull(carrierRateMaster, ErrorCode.CARRIER_RATE_REQUIRED);
        verifyCarrier(carrierRateMaster.getCarrierMaster());
        verifyCharge(carrierRateMaster.getChargeMaster());
        verifyUnit(carrierRateMaster.getUnitMaster());

        if (carrierRateMaster.getOrigin() != null) {

            verifyOrigin(carrierRateMaster.getOrigin());
            carrierRateMaster.setOriginCode(carrierRateMaster.getOrigin().getPortCode());
        }
        if (carrierRateMaster.getDestination() != null) {

            verifyDestination(carrierRateMaster.getDestination());
            carrierRateMaster.setDestinationCode(carrierRateMaster.getDestination().getPortCode());

        }

        verifyFrstAmount(carrierRateMaster.getFirstAmount());
        verifyFrstMinAmount(carrierRateMaster.getFirstMinAmount());
        verifySecondAmount(carrierRateMaster.getSecondAmount());
        verifySecondMinAmount(carrierRateMaster.getSecondMinAmount());

        carrierRateMaster.setCarrierCode(carrierRateMaster.getCarrierMaster().getCarrierCode());
        carrierRateMaster.setChargeCode(carrierRateMaster.getChargeMaster().getChargeCode());
        carrierRateMaster.setUnitCode(carrierRateMaster.getUnitMaster().getUnitCode());

    }


    private void verifyFrstAmount(Double firstAmount) {
        ValidateUtil.notNull(firstAmount, ErrorCode.FIRST_AMOUNT_NOT_NULL);
        ValidateUtil.nonZero(firstAmount, ErrorCode.FIRST_AMOUNT_NOT_ZERO);
        regExpService.validate(firstAmount, RegExpName.Reg_Exp_Master_Carrier_Rate_or_amount, ErrorCode.DOUBLE_VALUE_INVALID);

    }

    private void verifyFrstMinAmount(Double firstMinAmount) {
        if (firstMinAmount != null) {
            ValidateUtil.nonZero(firstMinAmount, ErrorCode.FIRST_MIN_AMOUNT_NOT_ZERO);
            regExpService.validate(firstMinAmount, RegExpName.Reg_Exp_Master_Carrier_Rate_or_amount, ErrorCode.DOUBLE_VALUE_INVALID);

        }

    }

    private void verifySecondAmount(Double secondAmount) {
        if (secondAmount != null) {
            ValidateUtil.nonZero(secondAmount, ErrorCode.SECOND_AMOUNT_NOT_ZERO);
            regExpService.validate(secondAmount, RegExpName.Reg_Exp_Master_Carrier_Rate_or_amount, ErrorCode.DOUBLE_VALUE_INVALID);
        }
    }

    private void verifySecondMinAmount(Double secondMinAmount) {
        if (secondMinAmount != null) {
            ValidateUtil.nonZero(secondMinAmount, ErrorCode.SECOND_MIN_AMOUNT_NOT_ZERO);
            regExpService.validate(secondMinAmount, RegExpName.Reg_Exp_Master_Carrier_Rate_or_amount, ErrorCode.DOUBLE_VALUE_INVALID);
        }
    }


    private void verifyDestination(PortMaster destination) {
        if (destination != null && destination.getId() != null) {
            ValidateUtil.isStatusBlocked(destination.getStatus(), ErrorCode.DESTINATION_STATUS_BLOCK);
            ValidateUtil.isStatusHidden(destination.getStatus(), ErrorCode.DESTINATION_STATUS_HIDDEN);
        }
    }

    private void verifyOrigin(PortMaster origin) {
        if (origin != null && origin.getId() != null) {
            ValidateUtil.isStatusBlocked(origin.getStatus(), ErrorCode.ORIGIN_STATUS_BLOCK);
            ValidateUtil.isStatusHidden(origin.getStatus(), ErrorCode.ORIGIN_STATUS_HIDDEN);
        }
    }

    private void verifyUnit(UnitMaster unitMaster) {
        ValidateUtil.notNull(unitMaster, ErrorCode.CARRIER_RATE_UNIT_REQUIRED);
        ValidateUtil.notNull(unitMaster.getId(), ErrorCode.UNIT_ID_NOT_NULL);
        ValidateUtil.isStatusBlocked(unitMaster.getStatus(), ErrorCode.CARRIER_RATE_UNIT_BLOCKED);
        ValidateUtil.isStatusHidden(unitMaster.getStatus(), ErrorCode.CARRIER_RATE_UNIT_HIDDEN);

    }

    private void verifyCharge(ChargeMaster chargeMaster) {
        ValidateUtil.notNull(chargeMaster, ErrorCode.CARRIER_RATE_CHARGE_REQUIRED);
        ValidateUtil.notNull(chargeMaster.getId(), ErrorCode.CARRIER_RATE_CHARGE_REQUIRED);
        ValidateUtil.isStatusBlocked(chargeMaster.getStatus(), ErrorCode.CARRIER_RATE_CHARGE_BLOCKED);
        ValidateUtil.isStatusHidden(chargeMaster.getStatus(), ErrorCode.CARRIER_RATE_CHARGE_HIDDEN);

    }

    private void verifyCarrier(CarrierMaster carrierMaster) {

        ValidateUtil.notNull(carrierMaster, ErrorCode.CARRIER_RATE_CARRIER_REQUIRED);
        ValidateUtil.notNull(carrierMaster.getId(), ErrorCode.CARRIER_RATE_CARRIER_REQUIRED);
        ValidateUtil.isStatusBlocked(carrierMaster.getStatus(), ErrorCode.CARRIER_RATE_CARRIER_BLOCKED);
        ValidateUtil.isStatusHidden(carrierMaster.getStatus(), ErrorCode.CARRIER_RATE_CARRIER_HIDDEN);
    }


    public void validateBulkUpload(FileUploadDto fileUploadDto) {
        ValidateUtil.notNull(fileUploadDto.getFile(), ErrorCode.CARRIER_RATE_CSV_FILE_REQUIRED);

        ValidateUtil.nonZero(fileUploadDto.getFile().length, ErrorCode.CARRIER_RATE_CSV_FILE_EMPTY);

        String key = appUtil.getLocationConfig("file.size.carrier.rate.bulk.upload", AuthService.getCurrentUser().getSelectedUserLocation(), false);

        if (key != null && key.trim().length() != 0) {

            long uploadedFileSize = fileUploadDto.getFile().length;

            log.info("Uploaded File Size : " + uploadedFileSize);

            long allowedSize = Long.parseLong(key) * 1024 * 1024;

            log.info("Allowed Size : " + allowedSize);


            ValidateUtil.aboveRange(uploadedFileSize, allowedSize, ErrorCode.CARRIER_RATE_CSV_FILE_LENGTH_EXCEED);
        }
    }
}
