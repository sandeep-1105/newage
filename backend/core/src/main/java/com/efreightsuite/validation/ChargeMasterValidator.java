package com.efreightsuite.validation;


import com.efreightsuite.enumeration.ChargeCalculationType;
import com.efreightsuite.enumeration.ChargeType;
import com.efreightsuite.enumeration.LovStatus;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.model.ChargeEdiMapping;
import com.efreightsuite.model.ChargeMaster;
import com.efreightsuite.service.RegularExpressionService;
import com.efreightsuite.util.RegExpName;
import com.efreightsuite.util.ValidateUtil;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
@Log4j2
public class ChargeMasterValidator {

    @Autowired
    private
    RegularExpressionService regExpService;

    public void validate(ChargeMaster chargeMaster) {

        log.info("Charge Master Validator Started..... Charge ID : [" + chargeMaster.getId() + "]");

        ValidateUtil.notNull(chargeMaster.getChargeCode(), ErrorCode.CHARGE_CODE_NOT_NULL);
        regExpService.validate(chargeMaster.getChargeCode(), RegExpName.Reg_Exp_Master_Charge_Code, ErrorCode.CHARGE_CODE_INVALID);
        ValidateUtil.validateLength(chargeMaster.getChargeCode(), 1, 10, ErrorCode.CHARGE_CODE_INVALID);


        ValidateUtil.notNull(chargeMaster.getChargeName(), ErrorCode.CHARGE_NAME_NOT_NULL);
        regExpService.validate(chargeMaster.getChargeName(), RegExpName.Reg_Exp_Master_Charge_Name, ErrorCode.CHARGE_NAME_INVALID);
        ValidateUtil.validateLength(chargeMaster.getChargeName(), 1, 100, ErrorCode.CHARGE_NAME_INVALID);

        ValidateUtil.validateEnum(ChargeCalculationType.class, chargeMaster.getCalculationType(), ErrorCode.CHARGE_CALUCULATION_TYPE_NOT_NULL);

        ValidateUtil.validateEnum(ChargeType.class, chargeMaster.getChargeType(), ErrorCode.CHARGE_CHARGE_TYPE_NOT_NULL);

        ValidateUtil.validateEnum(LovStatus.class, chargeMaster.getStatus(), ErrorCode.CHARGE_LOV_STATUS_NOT_NULL);

        log.info("Charge Master Validator Finished..... Charge ID : [" + chargeMaster.getId() + "]");

        ediValidate(chargeMaster);
    }


    private void ediValidate(ChargeMaster chargeMaster) {

        if (chargeMaster.getEdiList() != null && chargeMaster.getEdiList().size() != 0) {

            for (ChargeEdiMapping cem : chargeMaster.getEdiList()) {

                ValidateUtil.notNull(cem.getEdiType(), ErrorCode.EDI_TYPE);
                regExpService.validate(cem.getEdiType(), RegExpName.Reg_Exp_Master_Edi_Type, ErrorCode.EDI_TYPE);

                if (cem.getEdiSubtype() != null && cem.getEdiSubtype().trim().length() != 0) {
                    regExpService.validate(cem.getEdiSubtype(), RegExpName.Reg_Exp_Master_Edi_Subtype, ErrorCode.EDI_SUBTYPE);
                }

                ValidateUtil.notNull(cem.getEdiValue(), ErrorCode.EDI_VALUE);
                regExpService.validate(cem.getEdiValue(), RegExpName.Reg_Exp_Master_Edi_Value, ErrorCode.EDI_VALUE);
            }
        }

    }

}
