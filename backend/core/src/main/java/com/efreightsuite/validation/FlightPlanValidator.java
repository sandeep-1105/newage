package com.efreightsuite.validation;

import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.model.FlightPlan;
import com.efreightsuite.model.FlightPlanConnection;
import com.efreightsuite.util.ValidateUtil;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author user
 */

@Service
@Log4j2
public class FlightPlanValidator {

    @Autowired
    private
    FlightPlanConnectionValidator flightPlanConnectionValidator;

    public void validate(FlightPlan flightPlan) {


        validateFlightPlanConnectionList(flightPlan);

        //Capacity Reserved (KG's)
        validateCapacityReserved(flightPlan);

        //Flight Frequency (in Days)
        //validateFlightFrequency(flightPlan);

        //Flight status
        validateStatus(flightPlan);

        //Effective Upto
        //validateEffectiveUpto(flightPlan);


    }


    private void validateFlightPlanConnectionList(FlightPlan flightPlan) {

        if (flightPlan.getConnectionList() == null || flightPlan.getConnectionList().size() == 0) {

            throw new RestException(ErrorCode.FLIGHTPLAN_CONNECTION_LIST_EMPTY);

        } else {

            for (FlightPlanConnection flightPlanConnection : flightPlan.getConnectionList()) {
                flightPlanConnectionValidator.validate(flightPlanConnection);
            }
        }

    }

    private void validateCapacityReserved(FlightPlan flightPlan) {

        if (flightPlan.getCapacityReserved() != null) {

            if (flightPlan.getCapacityReserved() <= 0 || flightPlan.getCapacityReserved() > 99999.999999) {
                throw new RestException(ErrorCode.FLIGHTPLAN_CAPACITY_RESERVED_INVALID);
            }

        } else {
            flightPlan.setCapacityReserved(null);
        }

        log.info(flightPlan.getCapacityReserved() + " is validated successfully..");

    }

    private void validateStatus(FlightPlan flightPlan) {

        ValidateUtil.notNull(flightPlan.getFlightPlanStatus(), ErrorCode.FLIGHTPLAN_STATUS_NOT_NULL);

        log.info(flightPlan.getFlightPlanStatus() + " is validated successfully...");
    }

    private void validateFlightFrequency(FlightPlan flightPlan) {


        ValidateUtil.notNull(flightPlan.getFlightFrequency(), ErrorCode.FLIGHTPLAN_FLIGHT_FREQUENCY_NOT_NULL);

        if (flightPlan.getFlightFrequency() <= 0 || flightPlan.getFlightFrequency() > 999) {
            throw new RestException(ErrorCode.FLIGHTPLAN_FLIGHT_FREQUENCY_INVALID);
        }

        log.info(flightPlan.getFlightFrequency() + " is successfully validated...");
    }

    private void validateEffectiveUpto(FlightPlan flightPlan) {
        ValidateUtil.notNull(flightPlan.getEffectiveUpto(), ErrorCode.FLIGHTPLAN_EFFECTIVE_UPTO_NOT_NULL);
        log.info(flightPlan.getEffectiveUpto() + " is successfully validated...");
    }

}
