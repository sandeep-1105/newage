package com.efreightsuite.validation;

import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.model.InvoiceCreditNoteReceipt;
import com.efreightsuite.util.ValidateUtil;
import lombok.extern.log4j.Log4j2;

import org.springframework.stereotype.Service;

/**
 * @author user
 */
@Service
@Log4j2
public class InvoiceReceiptValidator {

    public void validate(InvoiceCreditNoteReceipt invoiceReceipt) {
        verifyDaybookMaster(invoiceReceipt);
        verifyDaybookCode(invoiceReceipt);
        verifyDocumentNumber(invoiceReceipt);
        verifyCurrencyMaster(invoiceReceipt);
        verifyAmount(invoiceReceipt);

    }

    private void verifyAmount(InvoiceCreditNoteReceipt invoiceReceipt) {
        ValidateUtil.notNull(invoiceReceipt.getAmount(), ErrorCode.INVOICE_RECEIPT_AMOUNT);

    }

    private void verifyCurrencyMaster(InvoiceCreditNoteReceipt invoiceReceipt) {
        ValidateUtil.notNull(invoiceReceipt.getCurrencyMaster(), ErrorCode.INVOICE_RECEIPT_CURRENCY);
        ValidateUtil.notNull(invoiceReceipt.getCurrencyMaster().getId(), ErrorCode.INVOICE_RECEIPT_CURRENCY);
        ValidateUtil.notNullOrEmpty(invoiceReceipt.getCurrencyMaster().getCurrencyCode(),
                ErrorCode.INVOICE_RECEIPT_CURRENCY);
        ValidateUtil.isStatusBlocked(invoiceReceipt.getCurrencyMaster().getStatus(),
                ErrorCode.INVOICE_RECEIPT_CURRENCY_BLOCK);
        ValidateUtil.isStatusHidden(invoiceReceipt.getCurrencyMaster().getStatus(),
                ErrorCode.INVOICE_RECEIPT_CURRENCY_HIDE);

        invoiceReceipt.setCurrencyCode(invoiceReceipt.getCurrencyMaster().getCurrencyCode());

    }

    private void verifyDocumentNumber(InvoiceCreditNoteReceipt invoiceReceipt) {
        ValidateUtil.notNull(invoiceReceipt.getDocumentNumber(), ErrorCode.INVOICE_RECEIPT_DOCUMNET);

    }

    private void verifyDaybookCode(InvoiceCreditNoteReceipt invoiceReceipt) {
        ValidateUtil.notNull(invoiceReceipt.getDaybookCode(), ErrorCode.INVOICE_RECEIPT_DAYBOOK_CODE);

    }

    private void verifyDaybookMaster(InvoiceCreditNoteReceipt invoiceReceipt) {
        ValidateUtil.notNull(invoiceReceipt.getDaybookMaster(), ErrorCode.INVOICE_RECEIPT_DAYBOOK);
        ValidateUtil.notNull(invoiceReceipt.getDaybookMaster().getId(), ErrorCode.INVOICE_RECEIPT_DAYBOOK);
        ValidateUtil.notNullOrEmpty(invoiceReceipt.getDaybookMaster().getDaybookCode(),
                ErrorCode.INVOICE_RECEIPT_DAYBOOK);
        ValidateUtil.isStatusBlocked(invoiceReceipt.getDaybookMaster().getStatus(),
                ErrorCode.INVOICE_RECEIPT_DAYBOOK_BLOCK);
        ValidateUtil.isStatusHidden(invoiceReceipt.getDaybookMaster().getStatus(),
                ErrorCode.INVOICE_RECEIPT_DAYBOOK_HIDE);

    }

}
