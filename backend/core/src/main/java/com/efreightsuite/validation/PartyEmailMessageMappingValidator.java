package com.efreightsuite.validation;

import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.model.AutoMailGroupMaster;
import com.efreightsuite.model.AutoMailMaster;
import com.efreightsuite.model.PartyEmailMapping;
import com.efreightsuite.model.PartyEmailMessageMapping;
import com.efreightsuite.util.ValidateUtil;
import lombok.extern.log4j.Log4j2;

import org.springframework.stereotype.Service;

@Service
@Log4j2
public class PartyEmailMessageMappingValidator {

    public void validate(PartyEmailMessageMapping partyEmailMessageMapping) {

        log.info("PartyEmailMessageMapping Validation Started....["
                + (partyEmailMessageMapping.getId() != null ? partyEmailMessageMapping.getId() : "") + "]");

        ValidateUtil.notNull(partyEmailMessageMapping, ErrorCode.PARTYEMAILMESSAGEMAPPING_NOT_NULL);

        validatePartyEmailMapping(partyEmailMessageMapping.getPartyEmailMapping());
        validateAutoMailMaster(partyEmailMessageMapping.getAutoMailMaster());
        validateAutoMailGroupMaster(partyEmailMessageMapping.getAutoMailGroupMaster());

        log.info("PartyEmailMessageMapping Validation Finished....["
                + (partyEmailMessageMapping.getId() != null ? partyEmailMessageMapping.getId() : "") + "]");
    }

    private void validateAutoMailGroupMaster(AutoMailGroupMaster autoMailGroupMaster) {

        ValidateUtil.notNull(autoMailGroupMaster, ErrorCode.AUTOMAILGROUP_NOT_NULL);
        ValidateUtil.notNull(autoMailGroupMaster, ErrorCode.AUTOMAILGROUP_ID_NOT_NULL);

    }

    private void validateAutoMailMaster(AutoMailMaster autoMailMaster) {

        ValidateUtil.notNull(autoMailMaster, ErrorCode.AUTOMAIL_NOT_NULL);
        ValidateUtil.notNull(autoMailMaster.getId(), ErrorCode.AUTOMAIL_ID_NOT_NULL);


    }

    private void validatePartyEmailMapping(PartyEmailMapping partyEmailMapping) {

        ValidateUtil.notNull(partyEmailMapping, ErrorCode.PARTYEMAILMAPPING_NOT_NULL);

        ValidateUtil.notNull(partyEmailMapping.getId(), ErrorCode.PARTYEMAILMAPPING_ID_NOT_NULL);

    }

}
