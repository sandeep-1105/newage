package com.efreightsuite.validation;

import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.model.Consol;
import com.efreightsuite.model.ConsolAttachment;
import com.efreightsuite.util.ValidateUtil;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class ConsolAttachmentValidator {

    @Autowired
    private
    SuperAttachmentValidator superAttachmentValidator;

    public void validate(ConsolAttachment consolAttachment, Consol consol) {

        verifyDocumentName(consolAttachment);

        superAttachmentValidator.validate(consolAttachment, consol.getLocation());

    }

    private void verifyDocumentName(ConsolAttachment consolAttachment) {

        if (consolAttachment.getDocumentMaster() != null && consolAttachment.getDocumentMaster().getId() != null) {

            ValidateUtil.isStatusBlocked(consolAttachment.getDocumentMaster().getStatus(), ErrorCode.ATTACHMENT_DOCUMENT_BLOCKED);

            ValidateUtil.isStatusHidden(consolAttachment.getDocumentMaster().getStatus(), ErrorCode.ATTACHMENT_DOCUMENT_HIDDEN);

        } else {
            consolAttachment.setDocumentMaster(null);
        }
    }

}
