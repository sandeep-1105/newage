package com.efreightsuite.validation;

import com.efreightsuite.enumeration.LovStatus;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.model.PartyCountryField;
import com.efreightsuite.service.RegularExpressionService;
import com.efreightsuite.util.RegExpName;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.Validate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Log4j2
@Service
public class PartyCountryFieldValidator {

    @Autowired
    private
    RegularExpressionService regExpService;

    public void validate(PartyCountryField partyCountryField) {
        verifyBranchSerialNumber(partyCountryField);
        verifyBankDlrCode(partyCountryField);
        verifyKnownShipperValidationNumber(partyCountryField);
        verifyIdNumber(partyCountryField);
        verifyWareHouseName(partyCountryField);
        verifyBondHolder(partyCountryField);
        verifyBondNumber(partyCountryField);
        verifyBondSuretyCode(partyCountryField);
        verifyCountryOfIssuance(partyCountryField);
        log.info("Party Country Field Validations Ends........................");
    }

    private void verifyBranchSerialNumber(PartyCountryField partyCountryField) {
        if (partyCountryField.getBranchSlNo() != null && partyCountryField.getBranchSlNo().trim().length() != 0) {
            regExpService.validate(partyCountryField.getBranchSlNo(), RegExpName.Reg_Exp_Master_PARTY_BRANCH_SLNO, ErrorCode.PARTY_BRANCH_SERIAL_NO_INVALID);
        }
    }

    private void verifyBankDlrCode(PartyCountryField partyCountryField) {
        if (partyCountryField.getBankDLRCode() != null && partyCountryField.getBankDLRCode().trim().length() != 0) {
            regExpService.validate(partyCountryField.getBankDLRCode(), RegExpName.Reg_Exp_Master_PARTY_BANK_DLR_CODE, ErrorCode.PARTY_BANK_DLR_CODE_INVALID);
        }
    }

    private void verifyKnownShipperValidationNumber(PartyCountryField partyCountryField) {
        if (partyCountryField.getKnownShipperValidationNo() != null && partyCountryField.getKnownShipperValidationNo().trim().length() != 0) {
            regExpService.validate(partyCountryField.getKnownShipperValidationNo(), RegExpName.Reg_Exp_Master_PARTY_SHIPPER_VALIDATION_NO, ErrorCode.PARTY_KNOWN_SHIPPER_NUMBER_INVALID);
        }
    }


    private void verifyIdNumber(PartyCountryField partyCountryField) {
        if (partyCountryField.getIdNumber() != null && partyCountryField.getIdNumber().trim().length() != 0) {
            regExpService.validate(partyCountryField.getIdNumber(), RegExpName.Reg_Exp_Master_PARTY_ID_NUMBER, ErrorCode.PARTY_ID_NUMBER_INVALID);
        }
    }


    private void verifyWareHouseName(PartyCountryField partyCountryField) {
        if (partyCountryField.getWarehouseProviderName() != null && partyCountryField.getWarehouseProviderName().trim().length() != 0) {
            regExpService.validate(partyCountryField.getIdNumber(), RegExpName.Reg_Exp_Master_PARTY_WAREHOUSE_PROVIDER_NAME, ErrorCode.PARTY_WAREHOUSE_PROVIDER_NAME_INVALID);
        }
    }


    private void verifyBondHolder(PartyCountryField partyCountryField) {
        if (partyCountryField.getBondHolder() != null && partyCountryField.getBondHolder().trim().length() != 0) {
            regExpService.validate(partyCountryField.getBondHolder(), RegExpName.Reg_Exp_Master_PARTY_BOND_HOLDER, ErrorCode.PARTY_BOND_HOLDER_INVALID);
        }
    }

    private void verifyBondNumber(PartyCountryField partyCountryField) {
        if (partyCountryField.getBondNo() != null && partyCountryField.getBondNo().trim().length() != 0) {
            regExpService.validate(partyCountryField.getBondNo(), RegExpName.Reg_Exp_Master_PARTY_BOND_NUMBER, ErrorCode.PARTY_BOND_NUMBER_INVALID);
        }
    }

    private void verifyBondSuretyCode(PartyCountryField partyCountryField) {
        if (partyCountryField.getBondSuretyCode() != null && partyCountryField.getBondSuretyCode().length() != 0) {
            regExpService.validate(partyCountryField.getBondSuretyCode(), RegExpName.Reg_Exp_Master_PARTY_BOND_SECURITY_CODE, ErrorCode.PARTY_BOND_SURETY_CODE_INVALID);
        }
    }

    private void verifyCountryOfIssuance(PartyCountryField partyCountryField) {
        if (partyCountryField.getIdType() != null) {
            Validate.notNull(partyCountryField.getCountryOfissuance(), ErrorCode.PARTY_COUNTRY_OF_ISSUANCE_REQUIRED);
            LovStatus status = partyCountryField.getCountryOfissuance().getStatus();
            if (status.equals(LovStatus.Block)) {
                throw new RestException(ErrorCode.PARTY_BLOCKED_COUNTRY_OF_ISSUANCE);
            }
            if (status.equals(LovStatus.Hide)) {
                throw new RestException(ErrorCode.PARTY_HIDDEN_COUNTRY_OF_ISSUANCE);
            }
        }

        if (partyCountryField.getCountryOfissuance() != null) {
            partyCountryField.setCountryOfissuanceCode(partyCountryField.getCountryOfissuance().getCountryCode());
        } else {
            partyCountryField.setCountryOfissuanceCode(null);
        }
    }


}
