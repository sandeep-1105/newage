package com.efreightsuite.validation;

import com.efreightsuite.enumeration.LovStatus;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.model.CompanyMaster;
import com.efreightsuite.service.RegularExpressionService;
import com.efreightsuite.util.RegExpName;
import com.efreightsuite.util.ValidateUtil;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class CompanyMasterValidator {

    @Autowired
    private
    RegularExpressionService regExpService;

    public void validate(CompanyMaster companyMaster) {

        // TODO
        log.info("companyMaster Validation Started....[" + (companyMaster.getId() != null ? companyMaster.getId() : "") + "]");

        ValidateUtil.notNull(companyMaster, ErrorCode.COMPANY_NOT_NULL);

        ValidateUtil.notNullOrEmpty(companyMaster.getCompanyCode(), ErrorCode.COMPANY_CODE_NOT_NULL);
        regExpService.validate(companyMaster.getCompanyCode(), RegExpName.MASTER_COMPANY_CODE, ErrorCode.COMPANY_CODE_INVALID);

        ValidateUtil.notNullOrEmpty(companyMaster.getCompanyName(), ErrorCode.COMPANY_NAME_NOT_NULL);
        regExpService.validate(companyMaster.getCompanyName(), RegExpName.MASTER_COMPANY_NAME, ErrorCode.COMPANY_NAME_INVALID);

        ValidateUtil.validateEnum(LovStatus.class, companyMaster.getStatus(), ErrorCode.COMPANY_STATUS_NOT_NULL);

        log.info("companyMaster Validation Finished....[" + (companyMaster.getId() != null ? companyMaster.getId() : "") + "]");
    }

}
