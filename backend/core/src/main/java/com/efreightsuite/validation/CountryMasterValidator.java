package com.efreightsuite.validation;

import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.model.CountryMaster;
import com.efreightsuite.util.ValidateUtil;

import org.springframework.stereotype.Service;

@Service
public class CountryMasterValidator {

    public void validate(CountryMaster countryMaster) {
        ValidateUtil.notNull(countryMaster, ErrorCode.COUNTRY_NOT_NULL);
        ValidateUtil.notNull(countryMaster.getCountryCode(), ErrorCode.COUNTRY_CODE_NOT_NULL);
        ValidateUtil.notNull(countryMaster.getCurrencyMaster(), ErrorCode.COUNTRY_CURRENCY_NOT_NULL);
        countryMaster.setCurrencyCode(countryMaster.getCurrencyMaster().getCurrencyCode());
        ValidateUtil.validateLength(countryMaster.getCountryCode(), 0, 2, ErrorCode.COUNTRY_CODE_INVALID);
        countryMaster.setCountryCode(countryMaster.getCountryCode().toUpperCase().trim());
        ValidateUtil.notNull(countryMaster.getCountryName(), ErrorCode.COUNTRY_NAME_NOT_NULL);
        ValidateUtil.validateLength(countryMaster.getCountryName(), 0, 100, ErrorCode.COUNTRY_NAME_INVALID);
        countryMaster.setCountryName(countryMaster.getCountryName().trim());
    }
}
