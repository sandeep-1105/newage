package com.efreightsuite.validation;

import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.model.*;
import com.efreightsuite.repository.CommodityMasterRepository;
import com.efreightsuite.repository.PortMasterRepository;
import com.efreightsuite.repository.ServiceMasterRepository;
import com.efreightsuite.repository.TosMasterRepository;
import com.efreightsuite.util.ValidateUtil;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class QuotationDetailValidator {

    @Autowired
    private
    QuotationDimensionValidator quotationDimensionValidator;

    @Autowired
    private
    QuotationCarrierValidator quotationCarrierValidator;

    @Autowired
    private
    ServiceMasterRepository serviceMasterRepository;

    @Autowired
    private
    PortMasterRepository portMasterRepository;

    @Autowired
    private
    TosMasterRepository tosMasterRepository;

    @Autowired
    private
    CommodityMasterRepository commodityMasterRepository;



    public void validate(QuotationDetail quotationDetail) {

        log.info("Quotation Detail Validation Started......");
        if (quotationDetail != null) {

            verifyService(quotationDetail);

			/* EFS_QT_AIR_062 */
            verifyOrigin(quotationDetail);

			/* EFS_QT_AIR_069 */
            verifyDestination(quotationDetail);

			/* EFS_QT_AIR_076 */
            verifyOriginDestination(quotationDetail);

			/* EFS_QT_AIR_134 */
            verifyTos(quotationDetail);

            verifyCommodity(quotationDetail);

			/* EFS_QT_AIR_112 */
            verifyChargeableWeight(quotationDetail);

			/* EFS_QT_AIR_120 */
            verifyGrossWeight(quotationDetail);

			/* EFS_QT_AIR_127 */
            verifyVolumeWeight(quotationDetail);

            verifyRefNo(quotationDetail);

            if (quotationDetail.getQuotationDimensionList() != null && quotationDetail.getQuotationDimensionList().size() != 0) {
                for (QuotationDimension quotationDimension : quotationDetail.getQuotationDimensionList()) {
                    quotationDimensionValidator.validate(quotationDimension);
                }
            }

            if (quotationDetail.getQuotationCarrierList() != null && quotationDetail.getQuotationCarrierList().size() != 0) {
                for (QuotationCarrier quotationCarrier : quotationDetail.getQuotationCarrierList()) {
                    quotationCarrierValidator.validate(quotationCarrier);
                }
            }

            if (quotationDetail.getQuotationValueAddedServiceList() != null && quotationDetail.getQuotationValueAddedServiceList().size() != 0) {
                for (QuotationValueAddedService service : quotationDetail.getQuotationValueAddedServiceList()) {
                    if (service.getValueAddedServices() != null) {
                        service.setValueAddedServicesCode(service.getValueAddedServices().getValueAddedSevicesCode());
                    } else {
                        service.setValueAddedServicesCode(null);
                    }

                }
            }
        }
        log.info("Quotation Detail Validation Finished......");

    }

    private void verifyRefNo(QuotationDetail quotationDetail) {

    }

    private void verifyService(QuotationDetail quotationDetail) {


        if (quotationDetail.getServiceMaster() != null && quotationDetail.getServiceMaster().getId() != null) {
            ServiceMaster service = serviceMasterRepository.getOne(quotationDetail.getServiceMaster().getId());

            ValidateUtil.isStatusBlocked(service.getStatus(), ErrorCode.QUOTATION_SERVICE_BLOCKED);

            ValidateUtil.isStatusHidden(service.getStatus(), ErrorCode.QUOTATION_SERVICE_HIDDEN);

            quotationDetail.setServiceCode(service.getServiceCode());

            log.info("Quotation Service is Validated..... [" + service.getServiceName() + "]");
        } else {
            quotationDetail.setServiceMaster(null);
            quotationDetail.setServiceCode(null);
        }

        log.info("Quotation Service is not selected...............");
    }

    private void verifyOrigin(QuotationDetail quotationDetail) {


        ValidateUtil.notNull(quotationDetail.getOrigin(), ErrorCode.QUOTATION_PORT_OF_ORIGIN_REQUIRED);

        ValidateUtil.notNull(quotationDetail.getOrigin().getId(), ErrorCode.QUOTATION_PORT_OF_ORIGIN_REQUIRED);

        PortMaster origin = portMasterRepository.findById(quotationDetail.getOrigin().getId());

        ValidateUtil.isStatusBlocked(origin.getStatus(), ErrorCode.QUOTATION_PORT_OF_ORIGIN_BLOCKED);

        ValidateUtil.isStatusHidden(origin.getStatus(), ErrorCode.QUOTATION_PORT_OF_ORIGIN_HIDDEN);

        quotationDetail.setOriginCode(origin.getPortCode());

        log.info("Port of Origin is Validated............[" + origin.getPortName() + "]");
    }

    private void verifyDestination(QuotationDetail quotationDetail) {

        ValidateUtil.notNull(quotationDetail.getDestination(), ErrorCode.QUOTATION_PORT_OF_DESTINATION_REQUIRED);

        ValidateUtil.notNull(quotationDetail.getDestination().getId(), ErrorCode.QUOTATION_PORT_OF_DESTINATION_REQUIRED);

        PortMaster destination = portMasterRepository.findById(quotationDetail.getDestination().getId());

        ValidateUtil.isStatusBlocked(destination.getStatus(), ErrorCode.QUOTATION_PORT_OF_DESTINATION_BLOCKED);

        ValidateUtil.isStatusHidden(destination.getStatus(), ErrorCode.QUOTATION_PORT_OF_DESTINATION_HIDDEN);

        quotationDetail.setDestinationCode(destination.getPortCode());

        log.info("Port of Destination is Validated............[" + destination.getPortName() + "]");
    }

    private void verifyOriginDestination(QuotationDetail quotationDetail) {

        if (quotationDetail.getOrigin() != null && quotationDetail.getOrigin().getId() != null &&
                quotationDetail.getDestination() != null && quotationDetail.getDestination().getId() != null) {

            if (quotationDetail.getOrigin().equals(quotationDetail.getDestination())) {
                log.info("Origin & Destination is Same - ErrorCode : " + ErrorCode.QUOTATION_ORIGIN_DESTINATION_SAME);
                throw new RestException(ErrorCode.QUOTATION_ORIGIN_DESTINATION_SAME);
            }

        }

        log.info("Origin Port & Destination Port is Validated Successfully........");
    }


    private void verifyTos(QuotationDetail quotationDetail) {

        ValidateUtil.notNull(quotationDetail.getTosMaster(), ErrorCode.QUOTATION_TOS_REQUIRED);

        ValidateUtil.notNull(quotationDetail.getTosMaster().getId(), ErrorCode.QUOTATION_TOS_REQUIRED);

        TosMaster tosMaster = tosMasterRepository.getOne(quotationDetail.getTosMaster().getId());

        ValidateUtil.isStatusBlocked(tosMaster.getStatus(), ErrorCode.QUOTATION_TOS_BLOCKED);

        ValidateUtil.isStatusHidden(tosMaster.getStatus(), ErrorCode.QUOTATION_TOS_HIDDEN);

        quotationDetail.setTosCode(tosMaster.getTosCode());

        log.info("TOS is Validated............[" + tosMaster.getTosCode() + "]");
    }


    private void verifyCommodity(QuotationDetail quotationDetail) {

        if (quotationDetail.getCommodityMaster() != null && quotationDetail.getCommodityMaster().getId() != null) {

            CommodityMaster commodity = commodityMasterRepository.findById(quotationDetail.getCommodityMaster().getId());

            ValidateUtil.isStatusBlocked(commodity.getStatus(), ErrorCode.QUOTATION_COMMODITY_BLOCKED);

            ValidateUtil.isStatusHidden(commodity.getStatus(), ErrorCode.QUOTATION_COMMODITY_HIDDEN);

            quotationDetail.setCommodityCode(commodity.getHsCode());

            log.info("Quotation Commodity is valiedated.....  [ " + commodity.getHsName() + "]..");
        } else {
            quotationDetail.setCommodityMaster(null);
            quotationDetail.setCommodityCode(null);
        }

        log.info("Quotation Commodity is not selected............");
    }

    private void verifyChargeableWeight(QuotationDetail quotationDetail) {
        log.info("Chargeable Weight : " + quotationDetail.getChargebleWeight());
    }

    private void verifyGrossWeight(QuotationDetail quotationDetail) {
        log.info("Gross Weight : " + quotationDetail.getGrossWeight());
    }

    private void verifyVolumeWeight(QuotationDetail quotationDetail) {
        log.info("Volume Wieght : " + quotationDetail.getVolumeWeight());
    }


}
