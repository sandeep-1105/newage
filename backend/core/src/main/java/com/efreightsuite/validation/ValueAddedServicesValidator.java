package com.efreightsuite.validation;


import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.model.ValueAddedCharge;
import com.efreightsuite.model.ValueAddedServices;
import com.efreightsuite.util.ValidateUtil;
import lombok.extern.log4j.Log4j2;

import org.springframework.stereotype.Service;

@Log4j2
@Service
public class ValueAddedServicesValidator {

    public void validate(ValueAddedServices valueAddedServices) {
        log.info("ValueAddedServicesValidator Validation Started..");
        ValidateUtil.notNull(valueAddedServices.getValueAddedSevicesName(), ErrorCode.VAS_NAME_NOT_NULL);
        ValidateUtil.notNull(valueAddedServices.getValueAddedSevicesCode(), ErrorCode.VAS_CODE_NOT_NULL);
        validateCharge(valueAddedServices);
        log.info("ValueAddedServicesValidator Validation Finished..");

    }

    private void validateCharge(ValueAddedServices valueAddedServices) {
        if (valueAddedServices.getValueAddedChargeList() != null && valueAddedServices.getValueAddedChargeList().size() > 0) {
            for (ValueAddedCharge valueAddedCharge : valueAddedServices.getValueAddedChargeList()) {
                ValidateUtil.notNull(valueAddedCharge, ErrorCode.CHARGE_NOT_NULL);
                ValidateUtil.notNull(valueAddedCharge.getChargeMaster().getChargeName(), ErrorCode.CHARGE_NAME_NOT_NULL);
                ValidateUtil.notNull(valueAddedCharge.getChargeMaster().getChargeCode(), ErrorCode.CHARGE_CODE_NOT_NULL);
                ValidateUtil.isStatusBlocked(valueAddedCharge.getChargeMaster().getStatus(), ErrorCode.CHARGE_STATUS_BLOCKED);
                ValidateUtil.isStatusBlocked(valueAddedCharge.getChargeMaster().getStatus(), ErrorCode.CHARGE_STATUS_HIDDEN);
                valueAddedCharge.setChargeCode(valueAddedCharge.getChargeMaster().getChargeCode());
            }
        } else {
            valueAddedServices.getValueAddedChargeList().clear();
        }
    }
}
