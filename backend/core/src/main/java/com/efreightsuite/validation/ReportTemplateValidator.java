package com.efreightsuite.validation;

import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.model.ReportTemplate;
import com.efreightsuite.model.ReportTemplateParameter;
import com.efreightsuite.repository.DefaultMasterDataRepository;
import com.efreightsuite.repository.ReportTemplateRepository;
import com.efreightsuite.repository.UserProfileRepository;
import com.efreightsuite.service.RegularExpressionService;
import com.efreightsuite.util.AppUtil;
import com.efreightsuite.util.ValidateUtil;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class ReportTemplateValidator {

    @Autowired
    RegularExpressionService regExpService;

    @Autowired
    AppUtil appUtil;

    @Autowired
    DefaultMasterDataRepository defaultMasterDataRepository;

    @Autowired
    ReportTemplateRepository reportTemplateRepository;

    @Autowired
    UserProfileRepository userProfileRepository;

    public void validate(ReportTemplate reportTemplate) {
        log.info("ReportTemplate Validation Starts Here");

        ValidateUtil.notNullOrEmpty(reportTemplate.getReportCategory(), ErrorCode.REPORT_TEMPLATE_CATEGORY_NOT_NULL_OR_EMPTY);
        ValidateUtil.notNullOrEmpty(reportTemplate.getReportName(), ErrorCode.REPORT_TEMPLATE_NAME_NOT_NULL_OR_EMPTY);

        if (reportTemplate.getTemplateParameterList() != null && !reportTemplate.getTemplateParameterList().isEmpty()) {
            for (ReportTemplateParameter templateParameter : reportTemplate.getTemplateParameterList()) {
                ValidateUtil.notNullOrEmpty(templateParameter.getParamKey(), ErrorCode.PARTY_NAME_NOT_NULL_OR_EMPTY);
                ValidateUtil.notNullOrEmpty(templateParameter.getParamValue(), ErrorCode.PARTY_NAME_NOT_NULL_OR_EMPTY);

                templateParameter.setReportTemplate(reportTemplate);
            }
        } else {
            ValidateUtil.assertTrue(false, ErrorCode.PARTY_NAME_NOT_NULL_OR_EMPTY);
        }

    }

}
