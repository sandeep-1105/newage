package com.efreightsuite.validation;

import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.model.CfsAttachment;
import com.efreightsuite.repository.DefaultMasterDataRepository;
import com.efreightsuite.service.AuthService;
import com.efreightsuite.util.AppUtil;
import com.efreightsuite.util.ValidateUtil;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


/**
 * Note - Quotation Attachment Error Codes used since attachment directive is same.
 */
@Service
@Log4j2
public class CfsAttachmentValidator {

    @Autowired
    private
    AppUtil appUtil;


    @Autowired
    DefaultMasterDataRepository defaultMasterDataRepository;

    public void validate(CfsAttachment cfsAttachment) {

        ValidateUtil.notNullOrEmpty(cfsAttachment.getRefNo(), ErrorCode.QUOTATION_REF_NO_NOT_NULL);

        ValidateUtil.notNullOrEmpty(cfsAttachment.getFileName(), ErrorCode.QUOTATION_FILE_NAME_NOT_NULL);

        if (cfsAttachment.getId() == null) {
            ValidateUtil.notNull(cfsAttachment.getFile(), ErrorCode.QUOTATION_FILE_NOT_NULL);

            String key = appUtil.getLocationConfig("upload.file.size.in.mb", AuthService.getCurrentUser().getSelectedUserLocation(), false);

            if (key != null && key.trim().length() != 0) {
                long uploadedFileSize = cfsAttachment.getFile().length;
                long allowedSize = Long.parseLong(key) * 1024 * 1024;

                if (uploadedFileSize == 0) {
                    throw new RestException(ErrorCode.QUOTATION_FILE_EMPTY);
                }

                if (uploadedFileSize > allowedSize) {
                    throw new RestException(ErrorCode.QUOTATION_FILE_SIZE_EXCEED);
                }

            }
        }


        log.info("CFS Attachment Validated....[" + cfsAttachment.getId() + "]");
    }


}
