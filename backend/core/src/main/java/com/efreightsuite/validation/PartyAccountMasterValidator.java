package com.efreightsuite.validation;

import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.model.PartyAccountMaster;
import com.efreightsuite.util.ValidateUtil;
import lombok.extern.log4j.Log4j2;

import org.springframework.stereotype.Service;

/**
 * InterfaceName                  Purpose                               CreatedBy/ModifiedBy      Date          Version
 * <p>
 * PartyAccountMasterValidator   Validator for PartyAccountMaster           Achyutananda P        18-05-16       0.0
 */
@Service
@Log4j2
public class PartyAccountMasterValidator {


    public void validate(PartyAccountMaster partyAccountMaster) {

        validateLocationMaster(partyAccountMaster);

        validateAccountMaster(partyAccountMaster);

        validateCurrencyMaster(partyAccountMaster);

        validateBillingSubLedgerCode(partyAccountMaster);

        validateBillingAccountName(partyAccountMaster);

        if (partyAccountMaster.getCompanyMaster() != null) {
            partyAccountMaster.setCompanyCode(partyAccountMaster.getCompanyMaster().getCompanyCode());
        } else {
            partyAccountMaster.setCompanyCode(null);
        }

        if (partyAccountMaster.getCountryMaster() != null) {
            partyAccountMaster.setCountryCode(partyAccountMaster.getCountryMaster().getCountryCode());
        } else {
            partyAccountMaster.setCountryCode(null);
        }
    }

    private void validateBillingSubLedgerCode(PartyAccountMaster partyAccountMaster) {


        if (partyAccountMaster.getBillingSubLedgerCode() != null && partyAccountMaster.getBillingSubLedgerCode().getId() != null) {

            ValidateUtil.isStatusBlocked(partyAccountMaster.getBillingSubLedgerCode().getStatus(), ErrorCode.PARTY_BILLING_SUBLEGER_BLOCKED);

            ValidateUtil.isStatusHidden(partyAccountMaster.getBillingSubLedgerCode().getStatus(), ErrorCode.PARTY_BILLING_SUBLEGER_HIDDEN);
            partyAccountMaster.setBillingSubLedger(partyAccountMaster.getBillingSubLedgerCode().getPartyCode());
        } else {
            partyAccountMaster.setBillingSubLedgerCode(null);
            partyAccountMaster.setBillingSubLedger(null);
        }

    }

    private void validateBillingAccountName(PartyAccountMaster partyAccountMaster) {


        if (partyAccountMaster.getBillingAccountName() != null && partyAccountMaster.getBillingAccountName().getId() != null) {

            ValidateUtil.isStatusBlocked(partyAccountMaster.getBillingAccountName().getStatus(), ErrorCode.PARTY_BILLING_SUBLEGER_BLOCKED);

            ValidateUtil.isStatusHidden(partyAccountMaster.getBillingAccountName().getStatus(), ErrorCode.PARTY_BILLING_SUBLEGER_HIDDEN);


        } else {

            partyAccountMaster.setBillingAccountName(null);

        }

    }


    private void validateLocationMaster(PartyAccountMaster partyAccountMaster) {


        ValidateUtil.notNull(partyAccountMaster, ErrorCode.PARTY_ACCOUNTS_INVALID);

        ValidateUtil.notNull(partyAccountMaster.getLocationMaster(), ErrorCode.PARTY_ACCOUNT_LOCATION_REQUIRED);

        ValidateUtil.notNull(partyAccountMaster.getLocationMaster().getId(), ErrorCode.PARTY_ACCOUNT_LOCATION_REQUIRED);

        ValidateUtil.isStatusBlocked(partyAccountMaster.getLocationMaster().getStatus(), ErrorCode.PARTY_ACCOUNT_LOCATION_BLOCKED);

        ValidateUtil.isStatusHidden(partyAccountMaster.getLocationMaster().getStatus(), ErrorCode.PARTY_ACCOUNT_LOCATION_HIDDEN);
        partyAccountMaster.setLocationCode(partyAccountMaster.getLocationMaster().getLocationCode());


    }

    //Account Master Validation
    private void validateAccountMaster(PartyAccountMaster partyAccountMaster) {

        ValidateUtil.notNull(partyAccountMaster.getAccountMaster(), ErrorCode.PARTY_ACCOUNT_REQUIRED);

        ValidateUtil.notNull(partyAccountMaster.getAccountMaster().getId(), ErrorCode.PARTY_ACCOUNT_REQUIRED);

        ValidateUtil.isStatusBlocked(partyAccountMaster.getAccountMaster().getStatus(), ErrorCode.PARTY_ACCOUNT_STATUS_BLOCKED);

        ValidateUtil.isStatusHidden(partyAccountMaster.getAccountMaster().getStatus(), ErrorCode.PARTY_ACCOUNT_STATUS_HIDDEN);

        partyAccountMaster.setAccountCode(partyAccountMaster.getAccountMaster().getGlAccountCode());

    }


    private void validateCurrencyMaster(PartyAccountMaster partyAccountMaster) {

        ValidateUtil.notNull(partyAccountMaster.getCurrencyMaster(), ErrorCode.PARTY_ACCOUNT_CURRENCY_REQUIRED);

        ValidateUtil.notNull(partyAccountMaster.getCurrencyMaster().getId(), ErrorCode.PARTY_ACCOUNT_CURRENCY_REQUIRED);

        ValidateUtil.isStatusBlocked(partyAccountMaster.getCurrencyMaster().getStatus(), ErrorCode.PARTY_CURRENCY_STATUS_BLOCK);

        ValidateUtil.isStatusHidden(partyAccountMaster.getCurrencyMaster().getStatus(), ErrorCode.PARTY_CURRENCY_STATUS_HIDE);

        partyAccountMaster.setCurrencyCode(partyAccountMaster.getCurrencyMaster().getCurrencyCode());

    }


}
