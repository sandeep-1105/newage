package com.efreightsuite.validation;

import com.efreightsuite.enumeration.LovStatus;
import com.efreightsuite.enumeration.TransportMode;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.model.PortEdiMapping;
import com.efreightsuite.model.PortMaster;
import com.efreightsuite.service.RegularExpressionService;
import com.efreightsuite.util.RegExpName;
import com.efreightsuite.util.ValidateUtil;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Log4j2
@Service
public class PortMasterValidator {

    @Autowired
    private
    RegularExpressionService regExpService;

    public void validate(PortMaster portMaster) {

        log.info("PortMaster Validation Started............[" + portMaster.getId() + "]");

        ValidateUtil.notNull(portMaster.getPortGroupMaster(), ErrorCode.PORT_GROUP_NAME_NOT_NULL);

        ValidateUtil.isStatusBlocked(portMaster.getPortGroupMaster().getStatus(), ErrorCode.PORT_GROUP_ID_INVALID);

        ValidateUtil.isStatusHidden(portMaster.getPortGroupMaster().getStatus(), ErrorCode.PORT_GROUP_ID_INVALID);


        ValidateUtil.notNull(portMaster.getPortCode(), ErrorCode.PORT_CODE_NOT_NULL);

        ValidateUtil.notNull(portMaster.getTransportMode(), ErrorCode.PORT_TRANSPORT_MODE_NOT_NULL);

        ValidateUtil.notNull(portMaster.getPortName(), ErrorCode.PORT_NAME_NOT_NULL);

        ValidateUtil.validateLength(portMaster.getPortName(), 1, 100, ErrorCode.PORT_NAME_LENGTH_INVALID);

        regExpService.validate(portMaster.getPortName(), RegExpName.Reg_Exp_Master_PORT_NAME, ErrorCode.PORT_NAME_INVALID);

        log.info("Port Name Validated Successfully...");

        if (portMaster.getTransportMode().equals(TransportMode.Ocean)) {
            String portCode = StringUtils.substring(portMaster.getPortCode(), 0, 2);
            log.info("portCode :" + portCode + ";" + portMaster.getPortGroupMaster().getCountryMaster().getCountryCode());
            String countryCode = StringUtils.substring(portMaster.getPortGroupMaster().getCountryMaster().getCountryCode(), 0, 2);
            if (!portCode.toUpperCase().equals(countryCode)) {
                throw new RestException(ErrorCode.PORT_CODE_FOR_SEA_COUNTRY_INVALID);
            }
        }

        ValidateUtil.validateEnum(LovStatus.class, portMaster.getStatus(), ErrorCode.PORT_STATUS_NOT_NULL);

        log.info("Port Status Validated Successfully...");


        ValidateUtil.validateEnum(TransportMode.class, portMaster.getTransportMode(), ErrorCode.PORT_TRANSPORT_MODE_NOT_NULL);

        log.info("Port Transport Mode Validated Successfully...");

        switch (portMaster.getTransportMode()) {

            case Air:
                airValidator(portMaster);
                break;

            case Ocean:
                seaValidator(portMaster);
                break;

            default:
                otherValidator(portMaster);
                break;
        }

        ediValidate(portMaster);

        log.info("PortMaster Validation Finished............[" + portMaster.getId() + "]");

    }


    private void ediValidate(PortMaster portMaster) {

        if (portMaster.getEdiList() != null && portMaster.getEdiList().size() != 0) {
            for (PortEdiMapping cem : portMaster.getEdiList()) {

                ValidateUtil.notNull(cem.getEdiType(), ErrorCode.EDI_TYPE);
                ValidateUtil.validateLength(cem.getEdiType(), 1, 30, ErrorCode.EDI_TYPE);

                if (cem.getEdiSubtype() != null && cem.getEdiSubtype().trim().length() != 0) {
                    ValidateUtil.validateLength(cem.getEdiSubtype(), 1, 30, ErrorCode.EDI_SUBTYPE);
                }

                ValidateUtil.notNull(cem.getEdiValue(), ErrorCode.EDI_VALUE);
                ValidateUtil.validateLength(cem.getEdiValue(), 1, 30, ErrorCode.EDI_VALUE);

            }
        }

    }


    private void otherValidator(PortMaster portMaster) {

        // Validating PortCode pattern for Sea
        ValidateUtil.validateLength(portMaster.getPortCode(), 1, 10, ErrorCode.PORT_CODE_FOR_OTHER_LENGTH_INVALID);
        //ValidateUtil.checkPattern(portMaster.getPortCode(),	"portCodeForOther", true);
        regExpService.validate(portMaster.getPortCode(), RegExpName.Reg_Exp_Master_PORT_CODE_OTHER, ErrorCode.PORT_CODE_FOR_OTHER_INVALID);

        log.info("Port Code For Other than SEA / AIR Validated...[" + portMaster.getPortCode() + "]");

    }

    private void seaValidator(PortMaster portMaster) {

        // Validating PortCode pattern for Sea

        ValidateUtil.validateLength(portMaster.getPortCode(), 5, 5, ErrorCode.PORT_CODE_FOR_SEA_LENGTH_INVALID);

        //ValidateUtil.checkPattern(portMaster.getPortCode(),	"portCodeForSea", true);
        regExpService.validate(portMaster.getPortCode(), RegExpName.Reg_Exp_Master_PORT_CODE_SEA, ErrorCode.PORT_CODE_FOR_SEA_INVALID);


        log.info("Port Code For Sea Validated...[" + portMaster.getPortCode() + "]");

    }

    private void airValidator(PortMaster portMaster) {

        // Validating PortCode pattern for Sea
        ValidateUtil.validateLength(portMaster.getPortCode(), 3, 3, ErrorCode.PORT_CODE_FOR_AIR_LENGTH_INVALID);
        //ValidateUtil.checkPattern(portMaster.getPortCode(),	"portCodeForAir", true);
        regExpService.validate(portMaster.getPortCode(), RegExpName.Reg_Exp_Master_PORT_CODE_AIR, ErrorCode.PORT_CODE_FOR_AIR_INVALID);

        log.info("Port Code For AIR Validated...[" + portMaster.getPortCode() + "]");

    }


}


