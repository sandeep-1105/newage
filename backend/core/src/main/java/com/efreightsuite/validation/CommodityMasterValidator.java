package com.efreightsuite.validation;

import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.model.CommodityMaster;
import com.efreightsuite.service.RegularExpressionService;
import com.efreightsuite.util.RegExpName;
import com.efreightsuite.util.ValidateUtil;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class CommodityMasterValidator {

    @Autowired
    private
    RegularExpressionService regExpService;

    public void commodityValidator(CommodityMaster commodityMaster) {
        log.info("Commodity  validation started");
        validateCommodityCode(commodityMaster);
        validateCommodityName(commodityMaster);
        validateCommodityLovStatus(commodityMaster);
        log.info("Commodity  validation ended");

    }

    private void validateCommodityCode(CommodityMaster commodityMaster) {
        ValidateUtil.notNull(commodityMaster.getHsCode(), ErrorCode.COMMODITY_CODE_NOT_NULL);

        ValidateUtil.validateLength(commodityMaster.getHsCode(), 1, 10, ErrorCode.COMMODITY_CODE_LENGTH_EXCEED);

        regExpService.validate(commodityMaster.getHsCode(), RegExpName.Reg_Exp_Master_HS_CODE, ErrorCode.COMMODITY_CODE_INVALID);
    }

    private void validateCommodityName(CommodityMaster commodityMaster) {

        ValidateUtil.notNull(commodityMaster.getHsName(), ErrorCode.COMMODITY_NAME_NOT_NULL);

        ValidateUtil.validateLength(commodityMaster.getHsName(), 1, 500, ErrorCode.COMMODITY_NAME_LENGTH_EXCEED);

        regExpService.validate(commodityMaster.getHsName(), RegExpName.Reg_Exp_Master_HS_NAME, ErrorCode.COMMODITY_NAME_INVALID);
    }

    private void validateCommodityLovStatus(CommodityMaster commodityMaster) {

        ValidateUtil.notNull(commodityMaster.getStatus(), ErrorCode.COMMODITY_LOV_STATUS_NOTNULL);

    }

}
