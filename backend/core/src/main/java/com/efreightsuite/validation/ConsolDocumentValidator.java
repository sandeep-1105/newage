package com.efreightsuite.validation;

import com.efreightsuite.common.component.CacheRepository;
import com.efreightsuite.enumeration.ImportExport;
import com.efreightsuite.enumeration.RateClass;
import com.efreightsuite.enumeration.YesNo;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.model.*;
import com.efreightsuite.repository.DefaultMasterDataRepository;
import com.efreightsuite.repository.EmployeeMasterRepository;
import com.efreightsuite.repository.PackMasterRepository;
import com.efreightsuite.repository.PartyMasterRepository;
import com.efreightsuite.util.AppUtil;
import com.efreightsuite.util.SaaSUtil;
import com.efreightsuite.util.ValidateUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.regex.Pattern;

/**
 * @author user
 */
@Service
public class ConsolDocumentValidator {

    @Autowired
    private
    AppUtil appUtil;

    @Autowired
    private
    CacheRepository cacheRepository;

    @Autowired
    private
    PartyMasterRepository partyMasterRepository;

    @Autowired
    DefaultMasterDataRepository defaultMasterDataRepository;


    @Autowired
    private EmployeeMasterRepository employeeMasterRepository;

    @Autowired
    private PackMasterRepository packMasterRepository;


    public void validate(ConsolDocument consolDocument, Consol consol) {

        String emailSplit = appUtil.getLocationConfig("single.text.split.by.multiple.email", consol.getLocation(), false);

        verifyBLReceived(consolDocument);

        verifyMarksAndNo(consolDocument);

        verifyCommodityDescription(consolDocument);

        verifyAccoutingInformation(consolDocument);

        verifyHandlingInformation(consolDocument);

        verifyWeight(consolDocument);

        verifyNoOfPieces(consolDocument);

        verifyPack(consolDocument);

        setAgreed(consolDocument, consol);

        verifyShiper(consolDocument, consol, emailSplit);

        verifyConsignee(consol, consolDocument, emailSplit);

        verifyForwarder(consolDocument, emailSplit);

        verifyFirstNotify(consolDocument, emailSplit);

        verifySecondNotify(consolDocument, emailSplit);

        verifyAgent(consolDocument, emailSplit);

        verifyIssuingAgent(consolDocument, emailSplit);

        verifyRateCLass(consolDocument, consol);

        changeDateAndTime(consolDocument, consol);

        if (consol.getServiceMaster().getImportExport() == ImportExport.Import) {
            consolDocument.setRateClass(null);
        }
        verifyDate(consolDocument);

        if (consolDocument.getCompany() != null) {
            consolDocument.setCompanyCode(consolDocument.getCompany().getCompanyCode());
        }

        if (consolDocument.getLocation() != null) {
            consolDocument.setLocationCode(consolDocument.getLocation().getLocationCode());
        }

        if (consolDocument.getCountry() != null) {
            consolDocument.setCountryCode(consolDocument.getCountry().getCountryCode());
        }

        if (consolDocument.getMawbGeneratedBy() != null) {
            consolDocument.setMawbGeneratedByCode(consolDocument.getMawbGeneratedBy().getEmployeeCode());
        }
    }

    private void changeDateAndTime(ConsolDocument cd,
                                   Consol c) {
        if (cd.getMawbIssueDate() != null) {
            SimpleDateFormat hawb = new SimpleDateFormat("yyyy-MM-dd");
            try {
                cd.setMawbIssueDate(hawb.parse(hawb.format(cd.getMawbIssueDate())));
            } catch (ParseException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }

    }


    private void setAgreed(ConsolDocument consolDocument, Consol consol) {

        if (consol.getIataRate() != null) {
            consolDocument.setIsAgreed(YesNo.No);
        } else {
            consolDocument.setIsAgreed(YesNo.Yes);
        }

    }


    private void verifyRateCLass(ConsolDocument consolDocument, Consol consol) {

        if (consolDocument.getRateClass() == null) {
            if (consol.getConsolDocument().getChargebleWeight() < 45) {
                consolDocument.setRateClass(RateClass.N);
            } else {
                consolDocument.setRateClass(RateClass.Q);
            }
        }
    }


    private void verifyBLReceived(ConsolDocument consolDocument) {

        if ((consolDocument.getBlReceivedPerson() != null && consolDocument.getBlReceivedPerson().getId() != null)
                || consolDocument.getBlReceivedDate() != null) {

            if (consolDocument.getBlReceivedPerson() == null || consolDocument.getBlReceivedPerson().getId() == null) {
                throw new RestException(ErrorCode.CONSOL_DOCUMENT_BL_EMP);
            }

            if (consolDocument.getBlReceivedDate() == null) {
                throw new RestException(ErrorCode.CONSOL_DOCUMENT_BL_RECEIVED_DATE);
            }
        }

        if (consolDocument.getBlReceivedPerson() != null && consolDocument.getBlReceivedPerson().getId() != null) {

            ValidateUtil.notNull(consolDocument.getBlReceivedPerson(), ErrorCode.CONSOL_DOCUMENT_BL_EMP);
            ValidateUtil.notNull(consolDocument.getBlReceivedPerson().getId(), ErrorCode.CONSOL_DOCUMENT_BL_EMP);
            EmployeeMaster blReceivedPerson = employeeMasterRepository.findById(consolDocument.getBlReceivedPerson().getId());

            ValidateUtil.notNullOrEmpty(blReceivedPerson.getEmployeeCode(), ErrorCode.CONSOL_DOCUMENT_BL_EMP);

            ValidateUtil.isEmployeeResigned(blReceivedPerson.getEmployementStatus(), ErrorCode.CONSOL_DOCUMENT_BL_EMP_RESIGNED);
            ValidateUtil.isEmployeeTerminated(blReceivedPerson.getEmployementStatus(), ErrorCode.CONSOL_DOCUMENT_BL_EMP_TERMINATED);

            consolDocument.setBlReceivedPersonCode(blReceivedPerson.getEmployeeCode());

        } else {
            consolDocument.setBlReceivedPerson(null);
            consolDocument.setBlReceivedPersonCode(null);
        }

        if (consolDocument.getBlReceivedDate() != null) {
            ValidateUtil.notNull(consolDocument.getBlReceivedDate(), ErrorCode.CONSOL_DOCUMENT_BL_RECEIVED_DATE);
        } else {
            consolDocument.setBlReceivedDate(null);
        }
    }

    private void verifyMarksAndNo(ConsolDocument consolDocument) {
        if (consolDocument.getMarksAndNo() != null && consolDocument.getMarksAndNo().trim().length() != 0) {
            ValidateUtil.notNullOrEmpty(consolDocument.getMarksAndNo(), ErrorCode.CONSOL_DOCUMENT_MARKS_AND_NO);
            ValidateUtil.validateLength(consolDocument.getMarksAndNo(), 1, 4000, ErrorCode.CONSOL_DOCUMENT_MARKS_AND_NO);
        } else {
            consolDocument.setMarksAndNo(null);
        }
    }

    private void verifyCommodityDescription(ConsolDocument consolDocument) {
        if (consolDocument.getCommodityDescription() != null && consolDocument.getCommodityDescription().trim().length() != 0) {
            ValidateUtil.notNullOrEmpty(consolDocument.getCommodityDescription(), ErrorCode.CONSOL_DOCUMENT_COMMODITY_DESCRIPTION);
            ValidateUtil.validateLength(consolDocument.getCommodityDescription(), 1, 4000, ErrorCode.CONSOL_DOCUMENT_COMMODITY_DESCRIPTION);
        } else {
            consolDocument.setCommodityDescription(null);
        }
    }

    private void verifyAccoutingInformation(ConsolDocument consolDocument) {
        if (consolDocument.getAccountingInformation() != null && consolDocument.getAccountingInformation().trim().length() != 0) {
            ValidateUtil.notNullOrEmpty(consolDocument.getAccountingInformation(), ErrorCode.CONSOL_DOCUMENT_ACCOUNTING_INFO);
            ValidateUtil.validateLength(consolDocument.getAccountingInformation(), 1, 4000, ErrorCode.CONSOL_DOCUMENT_ACCOUNTING_INFO);
        } else {
            consolDocument.setAccountingInformation(null);
        }
    }

    private void verifyHandlingInformation(ConsolDocument consolDocument) {
        if (consolDocument.getHandlingInformation() != null && consolDocument.getHandlingInformation().trim().length() != 0) {
            ValidateUtil.notNullOrEmpty(consolDocument.getHandlingInformation(), ErrorCode.CONSOL_DOCUMENT_HANDLING_INFO);
            ValidateUtil.validateLength(consolDocument.getHandlingInformation(), 1, 4000, ErrorCode.CONSOL_DOCUMENT_HANDLING_INFO);
        } else {
            consolDocument.setHandlingInformation(null);
        }
    }

    private void verifyWeight(ConsolDocument consolDocument) {

        if (consolDocument.getGrossWeight() != null) {
            ValidateUtil.notNull(consolDocument.getGrossWeight(), ErrorCode.CONSOL_GROSS_WEIGHT);
            if (consolDocument.getGrossWeight() <= 0) {
                throw new RestException(ErrorCode.CONSOL_GROSS_LESS_ZERO);
            }
            if (consolDocument.getGrossWeight() > 99999999999.999) {
                throw new RestException(ErrorCode.CONSOL_GROSS_INVALID);
            }
        } else {
            consolDocument.setGrossWeight(null);
        }

        if (consolDocument.getVolumeWeight() != null) {
            if (consolDocument.getVolumeWeight() != null && consolDocument.getVolumeWeight() < 0) {
                throw new RestException(ErrorCode.CONSOL_VOLUME_LESS_ZERO);
            }
            if (consolDocument.getVolumeWeight() > 99999999999.999) {
                throw new RestException(ErrorCode.CONSOL_DOCVOLUME_INVALID);
            }
        } else {
            consolDocument.setVolumeWeight(null);
        }
        if (consolDocument.getChargebleWeight() != null) {
            ValidateUtil.notNull(consolDocument.getChargebleWeight(), ErrorCode.CONSOL_CHARGEABLE_WEIGHT);
            if (consolDocument.getChargebleWeight() <= 0) {
                throw new RestException(ErrorCode.CONSOL_CHARGE_LESS_ZERO);
            }
        } else {
            consolDocument.setChargebleWeight(null);
        }


        if (consolDocument.getGrossWeightInPound() != null) {
            ValidateUtil.notNull(consolDocument.getGrossWeightInPound(), ErrorCode.CONSOL_GROSS_WEIGHT_POUND);
            if (consolDocument.getGrossWeightInPound() != null && consolDocument.getGrossWeightInPound() <= 0) {
                throw new RestException(ErrorCode.CONSOL_GROSS_WEIGHT_POUND_LESS_ZERO);
            }
            if (consolDocument.getGrossWeightInPound() != null && consolDocument.getGrossWeightInPound() > 99999999999.999) {
                throw new RestException(ErrorCode.CONSOL_DOCGROSS_WEIGHT_POUND_INVALID);
            }
        } else {
            consolDocument.setGrossWeightInPound(null);
        }

        if (consolDocument.getVolumeWeightInPound() != null) {
            ValidateUtil.notNull(consolDocument.getGrossWeightInPound(), ErrorCode.CONSOL_DOCVOLUME_WEIGHT_POUND);
            if (consolDocument.getVolumeWeightInPound() != null && consolDocument.getVolumeWeightInPound() <= 0) {
                throw new RestException(ErrorCode.CONSOL_VOLUME_WEIGHT_POUND_LESS_ZERO);
            }
            if (consolDocument.getVolumeWeightInPound() != null && consolDocument.getVolumeWeightInPound() > 99999999999.999) {
                throw new RestException(ErrorCode.CONSOL_DOCVOLUME_WEIGHT_POUND_INVALID);
            }
        } else {
            consolDocument.setVolumeWeightInPound(null);
        }
    }

    private void verifyNoOfPieces(ConsolDocument consolDocument) {
        ValidateUtil.notNull(consolDocument.getNoOfPieces(), ErrorCode.CONSOL_NO_OF_PIECES);
        if (consolDocument.getNoOfPieces() <= 0 && consolDocument.getNoOfPieces() > 999999999) {
            throw new RestException(ErrorCode.CONSOL_DOCPIECES_INVALID);
        }

    }

    private void verifyPack(ConsolDocument consolDocument) {
        if (consolDocument.getPackMaster() != null && consolDocument.getPackMaster().getId() != null) {
            ValidateUtil.notNull(consolDocument.getPackMaster(), ErrorCode.CONSOL_PACK);
            ValidateUtil.notNull(consolDocument.getPackMaster().getId(), ErrorCode.CONSOL_PACK);
            PackMaster packMaster = packMasterRepository.findById(consolDocument.getPackMaster().getId());

            ValidateUtil.notNullOrEmpty(packMaster.getPackCode(), ErrorCode.CONSOL_PACK);
            ValidateUtil.isStatusBlocked(packMaster.getStatus(), ErrorCode.CONSOL_PACK_BLOCK);
            ValidateUtil.isStatusHidden(packMaster.getStatus(), ErrorCode.CONSOL_PACK_HIDE);

            consolDocument.setPackCode(packMaster.getPackCode());
        } else {
            consolDocument.setPackMaster(null);
            consolDocument.setPackCode(null);
        }

    }


    private void verifyIssuingAgent(ConsolDocument consolDocument, String emailSplit) {

        if (consolDocument.getIssuingAgent() != null && consolDocument.getIssuingAgent().getId() != null) {

            PartyMaster tmpParty = cacheRepository.get(SaaSUtil.getSaaSId(), PartyMaster.class.getName(), consolDocument.getIssuingAgent().getId(), partyMasterRepository);

            ValidateUtil.notNull(tmpParty, ErrorCode.CONSOL_DOCUMENT_ISSUING_AGENT);
            ValidateUtil.notNull(tmpParty.getId(), ErrorCode.CONSOL_DOCUMENT_ISSUING_AGENT);
            ValidateUtil.notNullOrEmpty(tmpParty.getPartyCode(), ErrorCode.CONSOL_DOCUMENT_ISSUING_AGENT);
            ValidateUtil.isStatusBlocked(tmpParty.getStatus(), ErrorCode.CONSOL_DOCUMENT_ISSUING_AGENT_BLOCK);
            ValidateUtil.isStatusHidden(tmpParty.getStatus(), ErrorCode.CONSOL_DOCUMENT_ISSUING_AGENT_HIDE);

            ValidateUtil.isDefaulter(tmpParty, ErrorCode.CONSOL_ISSAGE_DEFAULTER);

            ValidateUtil.notNull(consolDocument.getIssuingAgentAddress(), ErrorCode.CONSOL_DOCUMENT_ISSUING_AGENT_ADDRESS);

            ValidateUtil.notNullOrEmpty(consolDocument.getIssuingAgentAddress().getAddressLine1(), ErrorCode.CONSOL_DOCUMENT_ISSUING_AGENT_ADDRESS_1);
            ValidateUtil.validateLength(consolDocument.getIssuingAgentAddress().getAddressLine1(), 1, 100, ErrorCode.CONSOL_DOCUMENT_ISSUING_AGENT_ADDRESS_1_LENGTH);

            if (consolDocument.getIssuingAgentAddress().getAddressLine2() != null && consolDocument.getIssuingAgentAddress().getAddressLine2().trim().length() != 0) {
                ValidateUtil.notNullOrEmpty(consolDocument.getIssuingAgentAddress().getAddressLine2(), ErrorCode.CONSOL_DOCUMENT_ISSUING_AGENT_ADDRESS_2);
                ValidateUtil.validateLength(consolDocument.getIssuingAgentAddress().getAddressLine2(), 1, 100, ErrorCode.CONSOL_DOCUMENT_ISSUING_AGENT_ADDRESS_2_LENGTH);
            }

            if (consolDocument.getIssuingAgentAddress().getAddressLine3() != null && consolDocument.getIssuingAgentAddress().getAddressLine3().trim().length() != 0) {
                ValidateUtil.validateLength(consolDocument.getIssuingAgentAddress().getAddressLine3(), 1, 100, ErrorCode.CONSOL_DOCUMENT_ISSUING_AGENT_ADDRESS_3_LENGTH);
            }

            if (consolDocument.getIssuingAgentAddress().getAddressLine3() == null || consolDocument.getIssuingAgentAddress().getAddressLine3() == "") {
                consolDocument.getIssuingAgentAddress().setAddressLine3(consolDocument.getIssuingAgentAddress().getAddressLine4());
                consolDocument.getIssuingAgentAddress().setAddressLine4(null);
            }


            if (consolDocument.getIssuingAgentAddress().getAddressLine4() != null && consolDocument.getIssuingAgentAddress().getAddressLine4().trim().length() != 0) {
                ValidateUtil.validateLength(consolDocument.getIssuingAgentAddress().getAddressLine4(), 1, 100, ErrorCode.CONSOL_DOCUMENT_ISSUING_AGENT_ADDRESS_4_LENGTH);
            }

            if (consolDocument.getIssuingAgentAddress().getEmail() != null) {
                ValidateUtil.validateLength(consolDocument.getIssuingAgentAddress().getEmail(), 1, 500, ErrorCode.CONSOL_DOCUMENT_ISSUING_AGENT_EMAIL_LENGTH);

                for (String email : consolDocument.getIssuingAgentAddress().getEmail().split(emailSplit)) {
                    if (!Pattern.compile(ValidateUtil.regularExp).matcher(email).matches()) {
                        throw new RestException(ErrorCode.CONSOL_DOCUMENT_ISSUING_AGENT_EMAIL_LENGTH);
                    }
                }
            }


            if (tmpParty.getPartyDetail() == null || tmpParty.getPartyDetail().getIataCode() == null) {
                throw new RestException(ErrorCode.SHIPMENT_ISSUING_AGENT_DONT_IATACODE);
            }
            consolDocument.setIssuingAgentCode(tmpParty.getPartyCode());

            if (consolDocument.getIssuingAgentAddress().getCity() != null) {
                consolDocument.getIssuingAgentAddress().setCityCode(consolDocument.getIssuingAgentAddress().getCity().getCityCode());
            }

            if (consolDocument.getIssuingAgentAddress().getState() != null) {
                consolDocument.getIssuingAgentAddress().setStateCode(consolDocument.getIssuingAgentAddress().getStateCode());
            }

            if (consolDocument.getIssuingAgentAddress().getCountry() != null) {
                consolDocument.getIssuingAgentAddress().setCountryCode(consolDocument.getIssuingAgentAddress().getCountryCode());
            }

        } else {
            consolDocument.setIssuingAgent(null);
            consolDocument.setIssuingAgentCode(null);
            consolDocument.setIssuingAgentAddress(null);
        }

    }

    private void verifyAgent(ConsolDocument consolDocument, String emailSplit) {

        if (consolDocument.getAgent() != null && consolDocument.getAgent().getId() != null) {
            PartyMaster tmpParty = cacheRepository.get(SaaSUtil.getSaaSId(), PartyMaster.class.getName(), consolDocument.getAgent().getId(), partyMasterRepository);


            ValidateUtil.notNull(tmpParty, ErrorCode.CONSOL_DOCUMENT_AGENT);
            ValidateUtil.notNull(tmpParty.getId(), ErrorCode.CONSOL_DOCUMENT_AGENT);
            ValidateUtil.notNullOrEmpty(tmpParty.getPartyCode(), ErrorCode.CONSOL_DOCUMENT_AGENT);
            ValidateUtil.isStatusBlocked(tmpParty.getStatus(), ErrorCode.CONSOL_DOCUMENT_AGENT_BLOCK);
            ValidateUtil.isStatusHidden(tmpParty.getStatus(), ErrorCode.CONSOL_DOCUMENT_AGENT_HIDE);

            ValidateUtil.isDefaulter(tmpParty, ErrorCode.CONSOL_DAGE_DEFAULTER);

            ValidateUtil.notNull(consolDocument.getAgentAddress(), ErrorCode.CONSOL_DOCUMENT_AGENT_ADDRESS);

            ValidateUtil.notNullOrEmpty(consolDocument.getAgentAddress().getAddressLine1(), ErrorCode.CONSOL_DOCUMENT_AGENT_ADDRESS_1);
            ValidateUtil.validateLength(consolDocument.getAgentAddress().getAddressLine1(), 1, 100, ErrorCode.CONSOL_DOCUMENT_AGENT_ADDRESS_1_LENGTH);

            if (consolDocument.getAgentAddress().getAddressLine2() != null && consolDocument.getAgentAddress().getAddressLine2().trim().length() != 0) {
                ValidateUtil.notNullOrEmpty(consolDocument.getAgentAddress().getAddressLine2(), ErrorCode.CONSOL_DOCUMENT_AGENT_ADDRESS_2);
                ValidateUtil.validateLength(consolDocument.getAgentAddress().getAddressLine2(), 1, 100, ErrorCode.CONSOL_DOCUMENT_AGENT_ADDRESS_2_LENGTH);
            }

            if (consolDocument.getAgentAddress().getAddressLine3() != null && consolDocument.getAgentAddress().getAddressLine3().trim().length() != 0) {
                ValidateUtil.validateLength(consolDocument.getAgentAddress().getAddressLine3(), 1, 100, ErrorCode.CONSOL_DOCUMENT_AGENT_ADDRESS_3_LENGTH);
            }


            if (consolDocument.getAgentAddress().getAddressLine3() == null || consolDocument.getAgentAddress().getAddressLine3() == "") {
                consolDocument.getAgentAddress().setAddressLine3(consolDocument.getAgentAddress().getAddressLine4());
                consolDocument.getAgentAddress().setAddressLine4(null);
            }


            if (consolDocument.getAgentAddress().getAddressLine4() != null && consolDocument.getAgentAddress().getAddressLine4().trim().length() != 0) {
                ValidateUtil.validateLength(consolDocument.getAgentAddress().getAddressLine4(), 1, 100, ErrorCode.CONSOL_DOCUMENT_AGENT_ADDRESS_4_LENGTH);
            }


            if (consolDocument.getAgentAddress().getEmail() != null) {
                ValidateUtil.validateLength(consolDocument.getAgentAddress().getEmail(), 1, 500, ErrorCode.CONSOL_DOCUMENT_AGENT_EMAIL_LENGTH);
            }

            if (consolDocument.getAgentAddress().getEmail() != null) {
                for (String email : consolDocument.getAgentAddress().getEmail().split(emailSplit)) {
                    if (!Pattern.compile(ValidateUtil.regularExp).matcher(email).matches()) {
                        throw new RestException(ErrorCode.CONSOL_DOCUMENT_AGENT_EMAIL_LENGTH);
                    }
                }
            }
            consolDocument.setAgentCode(tmpParty.getPartyCode());

            if (consolDocument.getAgentAddress().getCity() != null) {
                consolDocument.getAgentAddress().setCityCode(consolDocument.getAgentAddress().getCity().getCityCode());
            }

            if (consolDocument.getAgentAddress().getState() != null) {
                consolDocument.getAgentAddress().setStateCode(consolDocument.getAgentAddress().getStateCode());
            }

            if (consolDocument.getAgentAddress().getCountry() != null) {
                consolDocument.getAgentAddress().setCountryCode(consolDocument.getAgentAddress().getCountryCode());
            }

        } else {
            consolDocument.setAgent(null);
            consolDocument.setAgentCode(null);
            consolDocument.setAgentAddress(null);
        }

    }

    private void verifySecondNotify(ConsolDocument consolDocument, String emailSplit) {

        if (consolDocument.getSecondNotify() != null && consolDocument.getSecondNotify().getId() != null) {
            PartyMaster tmpParty = cacheRepository.get(SaaSUtil.getSaaSId(), PartyMaster.class.getName(), consolDocument.getSecondNotify().getId(), partyMasterRepository);

            ValidateUtil.notNull(tmpParty, ErrorCode.CONSOL_DOCUMENT_SECOND_NOTIFY);
            ValidateUtil.notNull(tmpParty.getId(), ErrorCode.CONSOL_DOCUMENT_SECOND_NOTIFY);
            ValidateUtil.notNullOrEmpty(tmpParty.getPartyCode(), ErrorCode.CONSOL_DOCUMENT_SECOND_NOTIFY);
            ValidateUtil.isStatusBlocked(tmpParty.getStatus(), ErrorCode.CONSOL_DOCUMENT_SECOND_NOTIFY_BLOCK);
            ValidateUtil.isStatusHidden(tmpParty.getStatus(), ErrorCode.CONSOL_DOCUMENT_SECOND_NOTIFY_HIDE);
            ValidateUtil.isDefaulter(tmpParty, ErrorCode.CONSOL_SNOTI_DEFAULTER);

            ValidateUtil.notNull(consolDocument.getSecondNotifyAddress(), ErrorCode.CONSOL_DOCUMENT_SECOND_NOTIFY_ADDRESS);

            ValidateUtil.notNullOrEmpty(consolDocument.getSecondNotifyAddress().getAddressLine1(), ErrorCode.CONSOL_DOCUMENT_SECOND_NOTIFY_ADDRESS_1);
            ValidateUtil.validateLength(consolDocument.getSecondNotifyAddress().getAddressLine1(), 1, 100, ErrorCode.CONSOL_DOCUMENT_SECOND_NOTIFY_ADDRESS_1_LENGTH);

            if (consolDocument.getSecondNotifyAddress().getAddressLine2() != null && consolDocument.getSecondNotifyAddress().getAddressLine2().trim().length() != 0) {
                ValidateUtil.notNullOrEmpty(consolDocument.getSecondNotifyAddress().getAddressLine2(), ErrorCode.CONSOL_DOCUMENT_SECOND_NOTIFY_ADDRESS_2);
                ValidateUtil.validateLength(consolDocument.getSecondNotifyAddress().getAddressLine2(), 1, 100, ErrorCode.CONSOL_DOCUMENT_SECOND_NOTIFY_ADDRESS_2_LENGTH);
            }

            if (consolDocument.getSecondNotifyAddress().getAddressLine3() != null && consolDocument.getSecondNotifyAddress().getAddressLine3().trim().length() != 0) {
                ValidateUtil.validateLength(consolDocument.getSecondNotifyAddress().getAddressLine3(), 1, 100, ErrorCode.CONSOL_DOCUMENT_SECOND_NOTIFY_ADDRESS_3_LENGTH);
            }


            if (consolDocument.getSecondNotifyAddress().getAddressLine3() == null || consolDocument.getSecondNotifyAddress().getAddressLine3() == "") {
                consolDocument.getSecondNotifyAddress().setAddressLine3(consolDocument.getSecondNotifyAddress().getAddressLine4());
                consolDocument.getSecondNotifyAddress().setAddressLine4(null);
            }

            if (consolDocument.getSecondNotifyAddress().getAddressLine4() != null && consolDocument.getSecondNotifyAddress().getAddressLine4().trim().length() != 0) {
                ValidateUtil.validateLength(consolDocument.getSecondNotifyAddress().getAddressLine4(), 1, 100, ErrorCode.CONSOL_DOCUMENT_SECOND_NOTIFY_ADDRESS_4_LENGTH);
            }


            if (consolDocument.getSecondNotifyAddress().getEmail() != null) {
                ValidateUtil.validateLength(consolDocument.getSecondNotifyAddress().getEmail(), 1, 500, ErrorCode.CONSOL_DOCUMENT_SECOND_NOTIFY_EMAIL_LENGTH);
                for (String email : consolDocument.getSecondNotifyAddress().getEmail().split(emailSplit)) {
                    if (!Pattern.compile(ValidateUtil.regularExp).matcher(email).matches()) {
                        throw new RestException(ErrorCode.CONSOL_DOCUMENT_SECOND_NOTIFY_EMAIL_LENGTH);
                    }
                }
            }
            consolDocument.setSecondNotifyCode(tmpParty.getPartyCode());
            if (consolDocument.getSecondNotifyAddress().getCity() != null) {
                consolDocument.getSecondNotifyAddress().setCityCode(consolDocument.getSecondNotifyAddress().getCity().getCityCode());
            }

            if (consolDocument.getSecondNotifyAddress().getState() != null) {
                consolDocument.getSecondNotifyAddress().setStateCode(consolDocument.getSecondNotifyAddress().getStateCode());
            }

            if (consolDocument.getSecondNotifyAddress().getCountry() != null) {
                consolDocument.getSecondNotifyAddress().setCountryCode(consolDocument.getSecondNotifyAddress().getCountryCode());
            }
        } else {
            consolDocument.setSecondNotify(null);
            consolDocument.setSecondNotifyCode(null);
            consolDocument.setSecondNotifyAddress(null);
        }

    }

    private void verifyFirstNotify(ConsolDocument consolDocument, String emailSplit) {

        if (consolDocument.getFirstNotify() != null && consolDocument.getFirstNotify().getId() != null) {
            PartyMaster tmpParty = cacheRepository.get(SaaSUtil.getSaaSId(), PartyMaster.class.getName(), consolDocument.getFirstNotify().getId(), partyMasterRepository);

            ValidateUtil.notNull(tmpParty, ErrorCode.CONSOL_DOCUMENT_FIRST_NOTIFY);
            ValidateUtil.notNull(tmpParty.getId(), ErrorCode.CONSOL_DOCUMENT_FIRST_NOTIFY);
            ValidateUtil.notNullOrEmpty(tmpParty.getPartyCode(), ErrorCode.CONSOL_DOCUMENT_FIRST_NOTIFY);
            ValidateUtil.isStatusBlocked(tmpParty.getStatus(), ErrorCode.CONSOL_DOCUMENT_FIRST_NOTIFY_BLOCK);
            ValidateUtil.isStatusHidden(tmpParty.getStatus(), ErrorCode.CONSOL_DOCUMENT_FIRST_NOTIFY_HIDE);
            ValidateUtil.isDefaulter(tmpParty, ErrorCode.CONSOL_FNOTI_DEFAULTER);

            ValidateUtil.notNull(consolDocument.getFirstNotifyAddress(), ErrorCode.CONSOL_DOCUMENT_FIRST_NOTIFY_ADDRESS);

            ValidateUtil.notNullOrEmpty(consolDocument.getFirstNotifyAddress().getAddressLine1(), ErrorCode.CONSOL_DOCUMENT_FIRST_NOTIFY_ADDRESS_1);
            ValidateUtil.validateLength(consolDocument.getFirstNotifyAddress().getAddressLine1(), 1, 100, ErrorCode.CONSOL_DOCUMENT_FIRST_NOTIFY_ADDRESS_1_LENGTH);

            if (consolDocument.getFirstNotifyAddress().getAddressLine2() != null && consolDocument.getFirstNotifyAddress().getAddressLine2().trim().length() != 0) {
                ValidateUtil.notNullOrEmpty(consolDocument.getFirstNotifyAddress().getAddressLine2(), ErrorCode.CONSOL_DOCUMENT_FIRST_NOTIFY_ADDRESS_2);
                ValidateUtil.validateLength(consolDocument.getFirstNotifyAddress().getAddressLine2(), 1, 100, ErrorCode.CONSOL_DOCUMENT_FIRST_NOTIFY_ADDRESS_2_LENGTH);
            }

            if (consolDocument.getFirstNotifyAddress().getAddressLine3() != null && consolDocument.getFirstNotifyAddress().getAddressLine3().trim().length() != 0) {
                ValidateUtil.validateLength(consolDocument.getFirstNotifyAddress().getAddressLine3(), 1, 100, ErrorCode.CONSOL_DOCUMENT_FIRST_NOTIFY_ADDRESS_3_LENGTH);
            }


            if (consolDocument.getFirstNotifyAddress().getAddressLine3() == null || consolDocument.getFirstNotifyAddress().getAddressLine3() == "") {
                consolDocument.getFirstNotifyAddress().setAddressLine3(consolDocument.getFirstNotifyAddress().getAddressLine4());
                consolDocument.getFirstNotifyAddress().setAddressLine4(null);
            }


            if (consolDocument.getFirstNotifyAddress().getAddressLine4() != null && consolDocument.getFirstNotifyAddress().getAddressLine4().trim().length() != 0) {
                ValidateUtil.validateLength(consolDocument.getFirstNotifyAddress().getAddressLine4(), 1, 100, ErrorCode.CONSOL_DOCUMENT_FIRST_NOTIFY_ADDRESS_4_LENGTH);
            }


            if (consolDocument.getFirstNotifyAddress().getEmail() != null) {
                ValidateUtil.validateLength(consolDocument.getFirstNotifyAddress().getEmail(), 1, 500, ErrorCode.CONSOL_DOCUMENT_FIRST_NOTIFY_EMAIL_LENGTH);

                for (String email : consolDocument.getFirstNotifyAddress().getEmail().split(emailSplit)) {
                    if (!Pattern.compile(ValidateUtil.regularExp).matcher(email).matches()) {
                        throw new RestException(ErrorCode.CONSOL_DOCUMENT_FIRST_NOTIFY_EMAIL_LENGTH);
                    }
                }
            }
            consolDocument.setFirstNotifyCode(tmpParty.getPartyCode());

            if (consolDocument.getFirstNotifyAddress().getCity() != null) {
                consolDocument.getFirstNotifyAddress().setCityCode(consolDocument.getFirstNotifyAddress().getCity().getCityCode());
            }

            if (consolDocument.getFirstNotifyAddress().getState() != null) {
                consolDocument.getFirstNotifyAddress().setStateCode(consolDocument.getFirstNotifyAddress().getStateCode());
            }

            if (consolDocument.getFirstNotifyAddress().getCountry() != null) {
                consolDocument.getFirstNotifyAddress().setCountryCode(consolDocument.getFirstNotifyAddress().getCountryCode());
            }

        } else {
            consolDocument.setFirstNotify(null);
            consolDocument.setFirstNotifyCode(null);
            consolDocument.setFirstNotifyAddress(null);
        }

    }


    private void verifyConsignee(Consol consol, ConsolDocument consolDocument, String emailSplit) {

        if (consol.getServiceMaster().getImportExport() == ImportExport.Import) {
            ValidateUtil.notNull(consolDocument.getConsignee(), ErrorCode.CONSOL_DOCUMENT_CONSIGNEE);
            ValidateUtil.notNull(consolDocument.getConsignee().getId(), ErrorCode.CONSOL_DOCUMENT_CONSIGNEE);
        }

        if (consolDocument.getConsignee() != null && consolDocument.getConsignee().getId() != null) {

            PartyMaster tmpParty = cacheRepository.get(SaaSUtil.getSaaSId(), PartyMaster.class.getName(), consolDocument.getConsignee().getId(), partyMasterRepository);

            ValidateUtil.notNull(tmpParty, ErrorCode.CONSOL_DOCUMENT_CONSIGNEE);
            ValidateUtil.notNull(tmpParty.getId(), ErrorCode.CONSOL_DOCUMENT_CONSIGNEE);
            ValidateUtil.notNullOrEmpty(tmpParty.getPartyCode(), ErrorCode.CONSOL_DOCUMENT_CONSIGNEE);

            ValidateUtil.isStatusBlocked(tmpParty.getStatus(), ErrorCode.CONSOL_DOCUMENT_CONSIGNEE_BLOCK);
            ValidateUtil.isStatusHidden(tmpParty.getStatus(), ErrorCode.CONSOL_DOCUMENT_CONSIGNEE_HIDE);

            ValidateUtil.isDefaulter(tmpParty, ErrorCode.CONSOL_CONSIG_DEFAULTER);

            ValidateUtil.notNull(consolDocument.getConsigneeAddress(), ErrorCode.CONSOL_DOCUMENT_CONSIGNEE_ADDRESS);

            ValidateUtil.notNullOrEmpty(consolDocument.getConsigneeAddress().getAddressLine1(), ErrorCode.CONSOL_DOCUMENT_CONSIGNEE_ADDRESS_1);
            ValidateUtil.validateLength(consolDocument.getConsigneeAddress().getAddressLine1(), 1, 100, ErrorCode.CONSOL_DOCUMENT_CONSIGNEE_ADDRESS_1_LENGTH);

            if (consolDocument.getConsigneeAddress().getAddressLine2() != null && consolDocument.getConsigneeAddress().getAddressLine2().trim().length() != 0) {
                ValidateUtil.notNullOrEmpty(consolDocument.getConsigneeAddress().getAddressLine2(), ErrorCode.CONSOL_DOCUMENT_CONSIGNEE_ADDRESS_2);
                ValidateUtil.validateLength(consolDocument.getConsigneeAddress().getAddressLine2(), 1, 100, ErrorCode.CONSOL_DOCUMENT_CONSIGNEE_ADDRESS_2_LENGTH);
            }


            if (consolDocument.getConsigneeAddress().getAddressLine3() != null && consolDocument.getConsigneeAddress().getAddressLine3().trim().length() != 0) {
                ValidateUtil.validateLength(consolDocument.getConsigneeAddress().getAddressLine3(), 1, 100, ErrorCode.CONSOL_DOCUMENT_CONSIGNEE_ADDRESS_3_LENGTH);
            }

            if (consolDocument.getConsigneeAddress().getAddressLine3() == null || consolDocument.getConsigneeAddress().getAddressLine3() == "") {
                consolDocument.getConsigneeAddress().setAddressLine3(consolDocument.getConsigneeAddress().getAddressLine4());
                consolDocument.getConsigneeAddress().setAddressLine4(null);
            }

            if (consolDocument.getConsigneeAddress().getAddressLine4() != null && consolDocument.getConsigneeAddress().getAddressLine4().trim().length() != 0) {
                ValidateUtil.validateLength(consolDocument.getConsigneeAddress().getAddressLine4(), 1, 100, ErrorCode.CONSOL_DOCUMENT_CONSIGNEE_ADDRESS_4_LENGTH);
            }


            if (consolDocument.getConsigneeAddress().getEmail() != null) {
                ValidateUtil.validateLength(consolDocument.getConsigneeAddress().getEmail(), 1, 500, ErrorCode.CONSOL_DOCUMENT_CONSIGNEE_EMAIL_LENGTH);

                for (String email : consolDocument.getConsigneeAddress().getEmail().split(emailSplit)) {
                    if (!Pattern.compile(ValidateUtil.regularExp).matcher(email).matches()) {
                        throw new RestException(ErrorCode.CONSOL_DOCUMENT_CONSIGNEE_EMAIL_LENGTH);
                    }
                }
            }
            consolDocument.setConsigneeCode(tmpParty.getPartyCode());

            if (consolDocument.getConsigneeAddress().getCity() != null) {
                consolDocument.getConsigneeAddress().setCityCode(consolDocument.getConsigneeAddress().getCity().getCityCode());
            }

            if (consolDocument.getConsigneeAddress().getState() != null) {
                consolDocument.getConsigneeAddress().setStateCode(consolDocument.getConsigneeAddress().getStateCode());
            }

            if (consolDocument.getConsigneeAddress().getCountry() != null) {
                consolDocument.getConsigneeAddress().setCountryCode(consolDocument.getConsigneeAddress().getCountryCode());
            }

        } else {
            consolDocument.setConsignee(null);
            consolDocument.setConsigneeCode(null);
            consolDocument.setConsigneeAddress(null);
        }

    }

    private void verifyForwarder(ConsolDocument consolDocument, String emailSplit) {


        if (consolDocument.getForwarder() != null && consolDocument.getForwarder().getId() != null) {

            PartyMaster tmpParty = cacheRepository.get(SaaSUtil.getSaaSId(), PartyMaster.class.getName(), consolDocument.getForwarder().getId(), partyMasterRepository);


            ValidateUtil.isDefaulter(tmpParty, ErrorCode.CONSOL_FOR_DEFAULTER);

            ValidateUtil.isStatusBlocked(tmpParty.getStatus(), ErrorCode.CONSOL_DOCUMENT_FORWARDER_BLOCK);

            ValidateUtil.isStatusHidden(tmpParty.getStatus(), ErrorCode.CONSOL_DOCUMENT_FORWARDER_HIDE);


            ValidateUtil.notNull(consolDocument.getForwarderAddress(), ErrorCode.CONSOL_DOCUMENT_FORWARDER_ADDRESS);

            ValidateUtil.notNullOrEmpty(consolDocument.getForwarderAddress().getAddressLine1(), ErrorCode.CONSOL_DOCUMENT_FORWARDER_ADDRESS_1);

            ValidateUtil.validateLength(consolDocument.getForwarderAddress().getAddressLine1(), 1, 100, ErrorCode.CONSOL_DOCUMENT_FORWARDER_ADDRESS_1_LENGTH);

            if (consolDocument.getForwarderAddress().getAddressLine2() != null && consolDocument.getForwarderAddress().getAddressLine2().trim().length() != 0) {
                ValidateUtil.notNullOrEmpty(consolDocument.getForwarderAddress().getAddressLine2(), ErrorCode.CONSOL_DOCUMENT_FORWARDER_ADDRESS_2);
                ValidateUtil.validateLength(consolDocument.getForwarderAddress().getAddressLine2(), 1, 100, ErrorCode.CONSOL_DOCUMENT_FORWARDER_ADDRESS_2_LENGTH);
            }


            if (consolDocument.getForwarderAddress().getAddressLine3() != null && consolDocument.getForwarderAddress().getAddressLine3().trim().length() != 0) {
                ValidateUtil.validateLength(consolDocument.getForwarderAddress().getAddressLine3(), 1, 100, ErrorCode.CONSOL_DOCUMENT_FORWARDER_ADDRESS_3_LENGTH);
            }

            if (consolDocument.getForwarderAddress().getAddressLine3() == null || consolDocument.getForwarderAddress().getAddressLine3() == "") {
                consolDocument.getForwarderAddress().setAddressLine3(consolDocument.getForwarderAddress().getAddressLine4());
                consolDocument.getForwarderAddress().setAddressLine4(null);
            }

            if (consolDocument.getForwarderAddress().getAddressLine4() != null && consolDocument.getForwarderAddress().getAddressLine4().trim().length() != 0) {
                ValidateUtil.validateLength(consolDocument.getForwarderAddress().getAddressLine4(), 1, 100, ErrorCode.CONSOL_DOCUMENT_FORWARDER_ADDRESS_4_LENGTH);
            }


            if (consolDocument.getForwarderAddress().getEmail() != null && consolDocument.getForwarderAddress().getEmail().trim().length() != 0) {
                ValidateUtil.validateLength(consolDocument.getForwarderAddress().getEmail(), 1, 500, ErrorCode.CONSOL_DOCUMENT_FORWARDER_EMAIL_LENGTH);
                for (String email : consolDocument.getForwarderAddress().getEmail().split(emailSplit)) {
                    if (!Pattern.compile(ValidateUtil.regularExp).matcher(email).matches()) {
                        throw new RestException(ErrorCode.CONSOL_DOCUMENT_FORWARDER_EMAIL_LENGTH);
                    }
                }
            }
            consolDocument.setForwarderCode(tmpParty.getPartyCode());
            if (consolDocument.getForwarderAddress().getCity() != null) {
                consolDocument.getForwarderAddress().setCityCode(consolDocument.getForwarderAddress().getCity().getCityCode());
            }

            if (consolDocument.getForwarderAddress().getState() != null) {
                consolDocument.getForwarderAddress().setStateCode(consolDocument.getForwarderAddress().getStateCode());
            }

            if (consolDocument.getForwarderAddress().getCountry() != null) {
                consolDocument.getForwarderAddress().setCountryCode(consolDocument.getForwarderAddress().getCountryCode());
            }
        } else {

            consolDocument.setForwarder(null);
            consolDocument.setForwarderAddress(null);
        }
    }


    private void verifyShiper(ConsolDocument consolDocument, Consol consol, String emailSplit) {

        if (consol.getServiceMaster().getImportExport() == ImportExport.Export) {
            ValidateUtil.notNull(consolDocument.getShipper(), ErrorCode.CONSOL_DOCUMENT_SHIPPER);
            ValidateUtil.notNull(consolDocument.getShipper().getId(), ErrorCode.CONSOL_DOCUMENT_SHIPPER);
        }

        if (consolDocument.getShipper() != null && consolDocument.getShipper().getId() != null) {

            ValidateUtil.notNull(consolDocument.getShipper(), ErrorCode.CONSOL_DOCUMENT_SHIPPER);
            ValidateUtil.notNull(consolDocument.getShipper().getId(), ErrorCode.CONSOL_DOCUMENT_SHIPPER);

            PartyMaster tmpParty = cacheRepository.get(SaaSUtil.getSaaSId(), PartyMaster.class.getName(), consolDocument.getShipper().getId(), partyMasterRepository);

            ValidateUtil.notNullOrEmpty(tmpParty.getPartyCode(), ErrorCode.CONSOL_DOCUMENT_SHIPPER);
            ValidateUtil.isStatusBlocked(tmpParty.getStatus(), ErrorCode.CONSOL_DOCUMENT_SHIPPER_BLOCK);
            ValidateUtil.isStatusHidden(tmpParty.getStatus(), ErrorCode.CONSOL_DOCUMENT_SHIPPER_HIDE);

            ValidateUtil.isDefaulter(tmpParty, ErrorCode.CONSOL_SHIP_DEFAULTER);

            ValidateUtil.notNull(consolDocument.getShipperAddress(), ErrorCode.CONSOL_DOCUMENT_SHIPPER_ADDRESS);

            ValidateUtil.notNullOrEmpty(consolDocument.getShipperAddress().getAddressLine1(), ErrorCode.CONSOL_DOCUMENT_SHIPPER_ADDRESS_1);
            ValidateUtil.validateLength(consolDocument.getShipperAddress().getAddressLine1(), 1, 100, ErrorCode.CONSOL_DOCUMENT_SHIPPER_ADDRESS_1_LENGTH);
            if (consolDocument.getShipperAddress().getAddressLine2() != null && consolDocument.getShipperAddress().getAddressLine2().trim().length() != 0) {
                ValidateUtil.notNullOrEmpty(consolDocument.getShipperAddress().getAddressLine2(), ErrorCode.CONSOL_DOCUMENT_SHIPPER_ADDRESS_2);
                ValidateUtil.validateLength(consolDocument.getShipperAddress().getAddressLine2(), 1, 100, ErrorCode.CONSOL_DOCUMENT_SHIPPER_ADDRESS_2_LENGTH);
            }

            if (consolDocument.getShipperAddress().getAddressLine3() != null && consolDocument.getShipperAddress().getAddressLine3().trim().length() != 0) {
                ValidateUtil.validateLength(consolDocument.getShipperAddress().getAddressLine3(), 1, 100, ErrorCode.CONSOL_DOCUMENT_SHIPPER_ADDRESS_3_LENGTH);
            }

            if (consolDocument.getShipperAddress().getAddressLine3() == null || consolDocument.getShipperAddress().getAddressLine3() == "") {
                consolDocument.getShipperAddress().setAddressLine3(consolDocument.getShipperAddress().getAddressLine4());
                consolDocument.getShipperAddress().setAddressLine4(null);
            }

            if (consolDocument.getShipperAddress().getAddressLine4() != null && consolDocument.getShipperAddress().getAddressLine4().trim().length() != 0) {
                ValidateUtil.validateLength(consolDocument.getShipperAddress().getAddressLine4(), 1, 100, ErrorCode.CONSOL_DOCUMENT_SHIPPER_ADDRESS_4_LENGTH);
            }


            if (consolDocument.getShipperAddress().getEmail() != null && consolDocument.getShipperAddress().getEmail().trim().length() != 0) {
                ValidateUtil.validateLength(consolDocument.getShipperAddress().getEmail(), 1, 500, ErrorCode.CONSOL_DOCUMENT_SHIPPER_EMAIL_LENGTH);

                for (String email : consolDocument.getShipperAddress().getEmail().split(emailSplit)) {
                    if (!Pattern.compile(ValidateUtil.regularExp).matcher(email).matches()) {
                        throw new RestException(ErrorCode.CONSOL_DOCUMENT_SHIPPER_EMAIL_LENGTH);
                    }
                }
            }
            consolDocument.setShipperCode(tmpParty.getPartyCode());

            if (consolDocument.getShipperAddress().getCity() != null) {
                consolDocument.getShipperAddress().setCityCode(consolDocument.getShipperAddress().getCity().getCityCode());
            }

            if (consolDocument.getShipperAddress().getState() != null) {
                consolDocument.getShipperAddress().setStateCode(consolDocument.getShipperAddress().getStateCode());
            }

            if (consolDocument.getShipperAddress().getCountry() != null) {
                consolDocument.getShipperAddress().setCountryCode(consolDocument.getShipperAddress().getCountryCode());
            }

        } else {
            consolDocument.setShipper(null);
            consolDocument.setShipperAddress(null);
        }
    }

    private void verifyDate(ConsolDocument consolDocument) {
        ValidateUtil.notNull(consolDocument.getDocumentReqDate(), ErrorCode.CONSOL_DOCUMENT_DATEREQ);

    }

}
