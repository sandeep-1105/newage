package com.efreightsuite.validation;

import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.model.AirlinePrebooking;
import com.efreightsuite.util.ValidateUtil;
import lombok.extern.log4j.Log4j2;

import org.springframework.stereotype.Service;

@Service
@Log4j2
public class AirlinePrebookingEntryValidator {

    public void validate(AirlinePrebooking airlinePrebooking) {

        log.info("AirlinePrebookingEntry Validation Started......");

        validateCarrier(airlinePrebooking);

        validateService(airlinePrebooking);

        validatePor(airlinePrebooking);

        validateCustomer(airlinePrebooking);

        validateCharter(airlinePrebooking);

        verifyNoOfMawbs(airlinePrebooking);

        verifyNoOfPieces(airlinePrebooking);

        verifyVolumeCbm(airlinePrebooking);

        verifyGrossWeight(airlinePrebooking);

        verifyVolumeWeight(airlinePrebooking);

        log.info("AirlinePrebookingEntry Validation Finished......");
    }

    private void validateCarrier(AirlinePrebooking airlinePrebooking) {

        log.info("AirlinePrebookingEntry validateCarrier started");

        ValidateUtil.notNull(airlinePrebooking.getCarrierMaster(), ErrorCode.AIRLINE_PREBOOKING_CARRIER_MANDATORY);

        ValidateUtil.isStatusBlocked(airlinePrebooking.getCarrierMaster().getStatus(), ErrorCode.AIRLINE_PREBOOKING_CARRIER_BLOCKED);

        ValidateUtil.isStatusHidden(airlinePrebooking.getCarrierMaster().getStatus(), ErrorCode.AIRLINE_PREBOOKING_CARRIER_HIDDDEN);

        log.info("AirlinePrebookingEntry validateCarrier ended");

    }

    private void validateService(AirlinePrebooking airlinePrebooking) {

        log.info("AirlinePrebookingEntry validateService started");

        ValidateUtil.notNull(airlinePrebooking.getServiceMaster(), ErrorCode.AIRLINE_PREBOOKING_SERVICE_MANDATORY);

        ValidateUtil.isStatusBlocked(airlinePrebooking.getServiceMaster().getStatus(), ErrorCode.AIRLINE_PREBOOKING_SERVICE_BLOCKED);

        ValidateUtil.isStatusHidden(airlinePrebooking.getServiceMaster().getStatus(), ErrorCode.AIRLINE_PREBOOKING_SERVICE_HIDDDEN);

        log.info("AirlinePrebookingEntry validateService ended");

    }

    private void validatePor(AirlinePrebooking airlinePrebooking) {

        log.info("AirlinePrebookingEntry validatePor started");

        ValidateUtil.notNull(airlinePrebooking.getPor(), ErrorCode.AIRLINE_PREBOOKING_POR_MANDATORY);

        ValidateUtil.isStatusBlocked(airlinePrebooking.getPor().getStatus(), ErrorCode.AIRLINE_PREBOOKING_POR_BLOCKED);

        ValidateUtil.isStatusHidden(airlinePrebooking.getPor().getStatus(), ErrorCode.AIRLINE_PREBOOKING_POR_HIDDDEN);

        log.info("AirlinePrebookingEntry validatePor ended");

    }

    private void validateCustomer(AirlinePrebooking airlinePrebooking) {

        log.info("AirlinePrebookingEntry validateCustomer started");

        ValidateUtil.notNull(airlinePrebooking.getPartyMaster(), ErrorCode.AIRLINE_PREBOOKING_CUSTOMER_MANDATORY);

        ValidateUtil.isStatusBlocked(airlinePrebooking.getPartyMaster().getStatus(), ErrorCode.AIRLINE_PREBOOKING_CUSTOMER_BLOCKED);

        ValidateUtil.isStatusHidden(airlinePrebooking.getPartyMaster().getStatus(), ErrorCode.AIRLINE_PREBOOKING_CUSTOMER_HIDDDEN);

        log.info("AirlinePrebookingEntry validateCustomer ended");

    }

    private void validateCharter(AirlinePrebooking airlinePrebooking) {

        log.info("AirlinePrebookingEntry validateCharter started");

        if (airlinePrebooking.getCharterMaster() != null && airlinePrebooking.getCharterMaster().getId() != null) {

            ValidateUtil.isStatusBlocked(airlinePrebooking.getCharterMaster().getStatus(), ErrorCode.AIRLINE_PREBOOKING_CHARTER_BLOCKED);

            ValidateUtil.isStatusHidden(airlinePrebooking.getCharterMaster().getStatus(), ErrorCode.AIRLINE_PREBOOKING_CHARTER_HIDDDEN);

            log.info("AirlinePrebookingEntry validateCharter ended");

        }

    }

    private void verifyNoOfMawbs(AirlinePrebooking airlinePrebooking) {

        if (airlinePrebooking.getNoOfMawb() != null) {

            ValidateUtil.belowRange(airlinePrebooking.getNoOfMawb(), 0d, ErrorCode.AIRLINE_PREBOOKING_NO_OF_MAWBS_NEGATIVE);

            ValidateUtil.aboveRange(airlinePrebooking.getNoOfMawb(), 1000000000d, ErrorCode.AIRLINE_PREBOOKING_NO_OF_MAWBS_INVALID);

            log.info("AirlinePrebookingEntry No. Of Mawbs Validated...............[" + airlinePrebooking.getNoOfMawb() + "]");

        }
    }

    private void verifyNoOfPieces(AirlinePrebooking airlinePrebooking) {

        if (airlinePrebooking.getNoOfPieces() != null) {

            ValidateUtil.belowRange(airlinePrebooking.getNoOfPieces(), 0d, ErrorCode.AIRLINE_PREBOOKING_NO_OF_PIECES_NEGATIVE);

            ValidateUtil.aboveRange(airlinePrebooking.getNoOfPieces(), 1000000000d, ErrorCode.AIRLINE_PREBOOKING_NO_OF_PIECES_INVALID);

            log.info("AirlinePrebookingEntry No. Of Pieces Validated...............[" + airlinePrebooking.getNoOfPieces() + "]");

        }
    }

    private void verifyVolumeCbm(AirlinePrebooking airlinePrebooking) {

        if (airlinePrebooking.getVolume() != null) {

            ValidateUtil.belowRange(airlinePrebooking.getVolume(), 0, ErrorCode.AIRLINE_PREBOOKING_VOLUME_CBM_NEGATIVE);

            ValidateUtil.aboveRange(airlinePrebooking.getVolume(), 1000000000d, ErrorCode.AIRLINE_PREBOOKING_VOLUME_CBM_INVALID);

            log.info("AirlinePrebookingEntry verifyVolumeCbm Validated ...............[" + airlinePrebooking.getVolume() + "]");

        }

    }

    private void verifyGrossWeight(AirlinePrebooking airlinePrebooking) {

        if (airlinePrebooking.getGrossWeight() != null) {

            ValidateUtil.belowRange(airlinePrebooking.getGrossWeight(), 0, ErrorCode.AIRLINE_PREBOOKING_GROSS_WEIGHT_NEGATIVE);

            ValidateUtil.aboveRange(airlinePrebooking.getGrossWeight(), 1000000000d, ErrorCode.AIRLINE_PREBOOKING_GROSS_WEIGHT_INVALID);

            log.info("AirlinePrebookingEntry GrossWeight Validated ...............[" + airlinePrebooking.getGrossWeight() + "]");
        }
    }

    private void verifyVolumeWeight(AirlinePrebooking airlinePrebooking) {

        if (airlinePrebooking.getVolumeWeight() != null) {

            ValidateUtil.belowRange(airlinePrebooking.getVolumeWeight(), 0, ErrorCode.AIRLINE_PREBOOKING_VOLUME_WEIGHT_NEGATIVE);

            ValidateUtil.aboveRange(airlinePrebooking.getVolumeWeight(), 1000000000d, ErrorCode.AIRLINE_PREBOOKING_VOLUME_WEIGHT_INVALID);

            log.info("AirlinePrebookingEntry getVolumeWeight Validated ...............[" + airlinePrebooking.getVolumeWeight() + "]");

        }
    }


}
