package com.efreightsuite.validation;

import com.efreightsuite.enumeration.LovStatus;
import com.efreightsuite.enumeration.TransportMode;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.model.CarrierAddressMapping;
import com.efreightsuite.model.CarrierEdiMapping;
import com.efreightsuite.model.CarrierMaster;
import com.efreightsuite.service.RegularExpressionService;
import com.efreightsuite.util.RegExpName;
import com.efreightsuite.util.ValidateUtil;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author user
 */
@Service
@Log4j2
public class CarrierMasterValidator {

    @Autowired
    private
    RegularExpressionService regExpService;

    public void checkValidation(CarrierMaster carrierMaster) {
        log.info("Carrier Master Validation Called");
        if (carrierMaster.getTransportMode() != null) {
            switch (carrierMaster.getTransportMode()) {
                case Air:
                    airValidate(carrierMaster);
                    break;
                case Ocean:
                    seaValidate(carrierMaster);
                    break;
                case Rail:
                case Road:
                    landValidate(carrierMaster);
                    break;
                default:
            }

        } else {

            ValidateUtil.validateEnum(TransportMode.class, carrierMaster.getTransportMode(),
                    ErrorCode.CARRIER_TRANSPORT_MODE_NOTNULL);
        }

        ediValidate(carrierMaster);
        addressValidate(carrierMaster);
    }

    private void airValidate(CarrierMaster carrierMaster) {

        ValidateUtil.notNull(carrierMaster.getCarrierCode(), ErrorCode.CARRIER_CODE_NOT_NULL);
        regExpService.validate(carrierMaster.getCarrierCode(), RegExpName.Reg_Exp_Master_Air_Carrier_Code, ErrorCode.CARRIER_AIR_CODE_INVALID);

        ValidateUtil.notNull(carrierMaster.getCarrierName(), ErrorCode.CARRIER_NAME_NOT_NULL);
        regExpService.validate(carrierMaster.getCarrierName(), RegExpName.Reg_Exp_Master_Carrier_Name, ErrorCode.CARRIER_NAME_INVALID);

        ValidateUtil.notNull(carrierMaster.getCarrierNo(), ErrorCode.CARRIER_NUM_NOT_NULL);
        regExpService.validate(carrierMaster.getCarrierNo(), RegExpName.Reg_Exp_Master_Air_Carrier_No, ErrorCode.CARRIER_NUM__AIR_MODE_INVALID);

        ValidateUtil.validateEnum(TransportMode.class, carrierMaster.getTransportMode(), ErrorCode.CARRIER_TRANSPORT_MODE_NOTNULL);
        carrierMaster.setScacCode(null);
        ValidateUtil.validateEnum(LovStatus.class, carrierMaster.getStatus(), ErrorCode.CARRIER_LOV_STATUS_NOTNULL);

        if (carrierMaster.getIataCode() != null && carrierMaster.getIataCode().trim().length() == 0) {
            carrierMaster.setIataCode(null);
        } else {
            regExpService.validate(carrierMaster.getIataCode(), RegExpName.Reg_Exp_Master_Air_Carrier_Iata_No, ErrorCode.CARRIER_ICAO_CODE_INVALID);
        }

    }

    private void seaValidate(CarrierMaster carrierMaster) {

        ValidateUtil.notNull(carrierMaster.getCarrierCode(), ErrorCode.CARRIER_CODE_NOT_NULL);
        regExpService.validate(carrierMaster.getCarrierCode(), RegExpName.Reg_Exp_Master_Carrier_Code, ErrorCode.CARRIER_CODE_INVALID);

        ValidateUtil.notNull(carrierMaster.getCarrierName(), ErrorCode.CARRIER_NAME_NOT_NULL);
        regExpService.validate(carrierMaster.getCarrierName(), RegExpName.Reg_Exp_Master_Carrier_Name, ErrorCode.CARRIER_NAME_INVALID);

        if (carrierMaster.getCarrierNo() != null) {
            regExpService.validate(carrierMaster.getCarrierNo(), RegExpName.Reg_Exp_Master_Carrier_No, ErrorCode.CARRIER_NUM_INVALID);
        }

        ValidateUtil.validateEnum(TransportMode.class, carrierMaster.getTransportMode(), ErrorCode.CARRIER_TRANSPORT_MODE_NOTNULL);

        if (carrierMaster.getScacCode() != null) {
            regExpService.validate(carrierMaster.getScacCode(), RegExpName.Reg_Exp_Master_Carrier_Scac_Code, ErrorCode.CARRIER_SCAC_CODE_INVALID);
        }

        ValidateUtil.validateEnum(LovStatus.class, carrierMaster.getStatus(), ErrorCode.CARRIER_LOV_STATUS_NOTNULL);

        carrierMaster.setIataCode(null);
    }

    private void landValidate(CarrierMaster carrierMaster) {

        ValidateUtil.notNull(carrierMaster.getCarrierCode(), ErrorCode.CARRIER_CODE_NOT_NULL);
        regExpService.validate(carrierMaster.getCarrierCode(), RegExpName.Reg_Exp_Master_Carrier_Code, ErrorCode.CARRIER_CODE_INVALID);

        ValidateUtil.notNull(carrierMaster.getCarrierName(), ErrorCode.CARRIER_NAME_NOT_NULL);
        regExpService.validate(carrierMaster.getCarrierName(), RegExpName.Reg_Exp_Master_Carrier_Name, ErrorCode.CARRIER_NAME_INVALID);

        if (carrierMaster.getCarrierNo() != null) {
            regExpService.validate(carrierMaster.getCarrierNo(), RegExpName.Reg_Exp_Master_Carrier_No, ErrorCode.CARRIER_NUM_INVALID);
        }

        ValidateUtil.validateEnum(TransportMode.class, carrierMaster.getTransportMode(), ErrorCode.CARRIER_TRANSPORT_MODE_NOTNULL);

        if (carrierMaster.getScacCode() != null) {
            regExpService.validate(carrierMaster.getScacCode(), RegExpName.Reg_Exp_Master_Carrier_Scac_Code, ErrorCode.CARRIER_SCAC_CODE_INVALID);
        }

        ValidateUtil.validateEnum(LovStatus.class, carrierMaster.getStatus(), ErrorCode.CARRIER_LOV_STATUS_NOTNULL);

        carrierMaster.setIataCode(null);
    }

    private void ediValidate(CarrierMaster carrierMaster) {

        if (carrierMaster.getEdiList() != null && carrierMaster.getEdiList().size() != 0) {

            for (CarrierEdiMapping cem : carrierMaster.getEdiList()) {

                ValidateUtil.notNull(cem.getEdiType(), ErrorCode.EDI_TYPE);
                regExpService.validate(cem.getEdiType(), RegExpName.Reg_Exp_Master_Edi_Type, ErrorCode.EDI_TYPE);

                if (cem.getEdiSubtype() != null && cem.getEdiSubtype().trim().length() != 0) {
                    regExpService.validate(cem.getEdiSubtype(), RegExpName.Reg_Exp_Master_Edi_Subtype, ErrorCode.EDI_SUBTYPE);
                }

                ValidateUtil.notNull(cem.getEdiValue(), ErrorCode.EDI_VALUE);
                regExpService.validate(cem.getEdiValue(), RegExpName.Reg_Exp_Master_Edi_Value, ErrorCode.EDI_VALUE);
            }
        }

    }

    private void addressValidate(CarrierMaster carrierMaster) {

        if (carrierMaster.getAddressList() != null && carrierMaster.getAddressList().size() != 0) {
            for (CarrierAddressMapping cam : carrierMaster.getAddressList()) {

                ValidateUtil.notNull(cam.getLocationMaster(), ErrorCode.CARRIER_ADDRESS_LOCATION);

                ValidateUtil.notNull(cam.getLocationMaster().getId(), ErrorCode.CARRIER_ADDRESS_LOCATION);

                ValidateUtil.notNull(cam.getPartyMaster(), ErrorCode.CARRIER_ADDRESS_PARTY);

                ValidateUtil.notNull(cam.getPartyMaster().getId(), ErrorCode.CARRIER_ADDRESS_PARTY);

                ValidateUtil.notNullOrEmpty(cam.getAccountNumber(), ErrorCode.CARRIER_ADDRESS_ACCOUNTNUMBER);
            }
        }

    }
}
