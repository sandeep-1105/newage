package com.efreightsuite.validation;

import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.model.PartyGroupMaster;
import com.efreightsuite.model.PartyMaster;
import com.efreightsuite.service.RegularExpressionService;
import com.efreightsuite.util.RegExpName;
import com.efreightsuite.util.ValidateUtil;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.collections4.CollectionUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class PartyGroupMasterValidator {

    @Autowired
    private
    RegularExpressionService regExpService;

    public void validate(PartyGroupMaster partyGroupMaster) {

        log.info("PartyGroupMaster Validator Started..... PartyGroupMaster ID : [" + partyGroupMaster.getId() + "]");

        ValidateUtil.notNull(partyGroupMaster.getPartyGroupCode(), ErrorCode.PARTY_GROUP_CODE_REQUIRED);

        regExpService.validate(partyGroupMaster.getPartyGroupCode(), RegExpName.Reg_Exp_Master_PARTY_GROUP_CODE, ErrorCode.PARTY_GROUP_CODE_INVALID);


        ValidateUtil.notNull(partyGroupMaster.getPartyGroupName(), ErrorCode.PARTY_GROUP_NAME_REQUIRED);

        regExpService.validate(partyGroupMaster.getPartyGroupName(), RegExpName.Reg_Exp_Master_PARTY_GROUP_NAME, ErrorCode.PARTY_GROUP_NAME_INVALID);


        if (partyGroupMaster.getSalesLeadLocation() != null && partyGroupMaster.getSalesLeadLocation().getId() != null) {
            ValidateUtil.isStatusBlocked(partyGroupMaster.getSalesLeadLocation().getStatus(), ErrorCode.PARTY_GROUP_LOCATION_BLOCK);
            ValidateUtil.isStatusHidden(partyGroupMaster.getSalesLeadLocation().getStatus(), ErrorCode.PARTY_GROUP_LOCATION_HIDDEN);
        }

        if (CollectionUtils.isNotEmpty(partyGroupMaster.getPartyMasterList())) {

            for (PartyMaster partyMaster : partyGroupMaster.getPartyMasterList()) {

                ValidateUtil.isStatusBlocked(partyMaster.getStatus(), ErrorCode.PARTY_GROUP_PARTY_BLOCK);

                ValidateUtil.isStatusHidden(partyMaster.getStatus(), ErrorCode.PARTY_GROUP_PARTY_HIDDEN);
            }
        }

        log.info("PartyGroupMaster Validator Finished..... PartyGroupMaster  ID : [" + partyGroupMaster.getId() + "]");
    }
}
