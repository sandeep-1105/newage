package com.efreightsuite.validation;

import com.efreightsuite.dto.ImportToExportDto;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.util.ValidateUtil;
import lombok.extern.log4j.Log4j2;

import org.springframework.stereotype.Service;

/**
 * @author user
 */
@Service
@Log4j2
public class ImportToExportValidator {

    public void validate(ImportToExportDto importToExportDto) {
        log.info("ImportToExportValidator.validate method start");

        validateAgent(importToExportDto);
        validateServiceMaster(importToExportDto);

        log.info("ImportToExportValidator.validate method end");
    }


    private void validateAgent(ImportToExportDto importToExportDto) {
        log.info("ImportToExportValidator.validateAgent method start");

        if (importToExportDto.getAgent() != null && importToExportDto.getAgent().getId() != null) {


            ValidateUtil.notNull(importToExportDto.getAgent(), ErrorCode.IMPORTTOEXPORT_AGENT_NOT_NULL);
            ValidateUtil.notNull(importToExportDto.getAgent().getId(), ErrorCode.IMPORTTOEXPORT_AGENT_NOT_NULL);
            ValidateUtil.notNullOrEmpty(importToExportDto.getAgent().getPartyCode(), ErrorCode.IMPORTTOEXPORT_AGENT_NOT_NULL);
            ValidateUtil.isStatusBlocked(importToExportDto.getAgent().getStatus(), ErrorCode.IMPORTTOEXPORT_AGENT_BLOCKED);
            ValidateUtil.isStatusHidden(importToExportDto.getAgent().getStatus(), ErrorCode.IMPORTTOEXPORT_AGENT_HIDE);

            ValidateUtil.isDefaulter(importToExportDto.getAgent(), ErrorCode.IMPORTTOEXPORT_AGENT_DEFAULTER);

            ValidateUtil.notNull(importToExportDto.getAgentAddress(), ErrorCode.IMPORTTOEXPORT_AGENT_ADDRESS_NOT_NULL);

            ValidateUtil.notNullOrEmpty(importToExportDto.getAgentAddress().getAddressLine1(), ErrorCode.IMPORTTOEXPORT_AGENT_ADDRESS_LINE1_REQUIRED);
            ValidateUtil.validateLength(importToExportDto.getAgentAddress().getAddressLine1(), 1, 100, ErrorCode.IMPORTTOEXPORT_AGENT_ADDRESS_LINE1_INVALID);

            ValidateUtil.notNullOrEmpty(importToExportDto.getAgentAddress().getAddressLine2(), ErrorCode.IMPORTTOEXPORT_AGENT_ADDRESS_LINE2_REQUIRED);
            ValidateUtil.validateLength(importToExportDto.getAgentAddress().getAddressLine2(), 1, 100, ErrorCode.IMPORTTOEXPORT_AGENT_ADDRESS_LINE2_INVALID);

            if (importToExportDto.getAgentAddress().getAddressLine3() != null && importToExportDto.getAgentAddress().getAddressLine3().trim().length() != 0)
                ValidateUtil.validateLength(importToExportDto.getAgentAddress().getAddressLine3(), 1, 100, ErrorCode.IMPORTTOEXPORT_AGENT_ADDRESS_LINE3_INVALID);

            if (importToExportDto.getAgentAddress().getAddressLine4() != null)
                ValidateUtil.validateLength(importToExportDto.getAgentAddress().getAddressLine4(), 1, 100, ErrorCode.IMPORTTOEXPORT_AGENT_ADDRESS_LINE4_INVALID);


            //if(importToExportDto.getAgentAddress().getEmail()!=null)
            //ValidateUtil.validateLength(importToExportDto.getAgentAddress().getEmail(), 1, 500, ErrorCode.IMPORTTOEXPORT_AGENT_EMAIL);

				/*if(importToExportDto.getAgentAddress().getEmail()!=null){
				for(String email : importToExportDto.getAgentAddress().getEmail().split(defaultMasterDataRepository.findByCode("single.text.split.by.multiple.email").getValue())){
					if(!Pattern.compile(ValidateUtil.regularExp).matcher(email).matches()){
						throw new RestException(ErrorCode.IMPORTTOEXPORT_AGENT_EMAIL_LENGTH);
					}
				}
				}*/

        }

        log.info("ImportToExportValidator.validateAgent method end");

    }

    private void validateServiceMaster(ImportToExportDto dto) {

        log.info("ImportToExportValidator.validateServiceMaster method start");

        ValidateUtil.notNull(dto.getExportService().getServiceMaster(), ErrorCode.IMPORTTOEXPORT_SERVICE);
        ValidateUtil.notNull(dto.getExportService().getServiceMaster().getId(), ErrorCode.IMPORTTOEXPORT_SERVICE);
        ValidateUtil.notNullOrEmpty(dto.getExportService().getServiceMaster().getServiceCode(), ErrorCode.IMPORTTOEXPORT_SERVICE);
        ValidateUtil.isStatusBlocked(dto.getExportService().getServiceMaster().getStatus(), ErrorCode.IMPORTTOEXPORT_SERVICE_BLOCK);
        ValidateUtil.isStatusHidden(dto.getExportService().getServiceMaster().getStatus(), ErrorCode.IMPORTTOEXPORT_SERVICE_HIDE);

        log.info("ImportToExportValidator.validateServiceMaster method end");

    }

}

