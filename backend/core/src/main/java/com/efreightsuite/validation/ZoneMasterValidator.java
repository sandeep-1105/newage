package com.efreightsuite.validation;

import com.efreightsuite.enumeration.LovStatus;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.model.ZoneMaster;
import com.efreightsuite.service.RegularExpressionService;
import com.efreightsuite.util.ValidateUtil;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class ZoneMasterValidator {

    @Autowired
    CountryMasterValidator countryMasterValidator;

    @Autowired
    RegularExpressionService regExpService;

    public void validate(ZoneMaster zoneMaster) {
        log.info("Zone Master Validation Started....[" + (zoneMaster.getId() != null ? zoneMaster.getId() : "") + "]");

        ValidateUtil.notNull(zoneMaster, ErrorCode.ZONE_NOT_NULL);
        ValidateUtil.notNull(zoneMaster.getZoneCode(), ErrorCode.ZONE_CODE_NOT_NULL);
        ValidateUtil.validateLength(zoneMaster.getZoneCode(), 0, 10, ErrorCode.ZONE_CODE_INVALID);
        zoneMaster.setZoneCode(zoneMaster.getZoneCode().toUpperCase().trim());
        ValidateUtil.notNull(zoneMaster.getZoneName(), ErrorCode.ZONE_NAME_NOT_NULL);
        zoneMaster.setZoneName(zoneMaster.getZoneName().trim());
        ValidateUtil.validateLength(zoneMaster.getZoneName(), 0, 100, ErrorCode.COUNTRY_NAME_INVALID);
        if (zoneMaster.getStatus() == null) {
            zoneMaster.setStatus(LovStatus.Active);
        }
        validateCountry(zoneMaster);

        log.info("Zone Master Validation Finished....[" + (zoneMaster.getId() != null ? zoneMaster.getId() : "") + "]");
    }


    private void validateCountry(ZoneMaster zoneMaster) {
        ValidateUtil.notNull(zoneMaster.getCountryMaster().getCountryName(), ErrorCode.COUNTRY_NAME_NOT_NULL);
        ValidateUtil.notNull(zoneMaster.getCountryMaster().getId(), ErrorCode.COUNTRY_ID_NOT_NULL);
        ValidateUtil.isStatusBlocked(zoneMaster.getCountryMaster().getStatus(), ErrorCode.COUNTRY_STATUS_BLOCK);
        ValidateUtil.isStatusHidden(zoneMaster.getCountryMaster().getStatus(), ErrorCode.COUNTRY_STATUS_HIDE);

        zoneMaster.setCountryCode(zoneMaster.getCountryMaster().getCountryCode());

    }

}
