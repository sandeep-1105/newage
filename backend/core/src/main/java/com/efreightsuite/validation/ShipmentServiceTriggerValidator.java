package com.efreightsuite.validation;

import com.efreightsuite.enumeration.YesNo;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.model.ShipmentServiceTrigger;
import com.efreightsuite.model.TriggerMaster;
import com.efreightsuite.model.TriggerTypeMaster;
import com.efreightsuite.repository.TriggerMasterRepository;
import com.efreightsuite.repository.TriggerTypeMasterRepository;
import com.efreightsuite.util.ValidateUtil;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Log4j2

public class ShipmentServiceTriggerValidator {

    @Autowired
    private TriggerTypeMasterRepository triggerTypeMasterRepository;

    @Autowired
    private TriggerMasterRepository triggerMasterRepository;




    public void validate(ShipmentServiceTrigger st) {

        ValidateUtil.notNull(st.getDate(), ErrorCode.SHIPMENT_STATUS_DATE_REQUIRED);

        verifyTriggerType(st);

        verifyTriggerName(st);

        verifyProtect(st);

        // verifyAssignedTo(st);

        //verifyFollowUpDate(st);

        verifyNote(st);

        if (st.getEmployeeMaster() != null) {
            st.setEmployeeMasterCode(st.getEmployeeMaster().getEmployeeCode());
        }
    }

    private void verifyTriggerType(ShipmentServiceTrigger st) {

        ValidateUtil.notNull(st.getTriggerTypeMaster(), ErrorCode.SHIPMENT_STATUS_TYPE_REQUIRED);
        ValidateUtil.notNull(st.getTriggerTypeMaster().getId(), ErrorCode.SHIPMENT_STATUS_TYPE_REQUIRED);
        TriggerTypeMaster triggerTypeMaster = triggerTypeMasterRepository.getOne(st.getTriggerTypeMaster().getId());

        ValidateUtil.notNullOrEmpty(triggerTypeMaster.getTriggerTypeName(), ErrorCode.SHIPMENT_STATUS_TYPE_REQUIRED);
        ValidateUtil.isStatusBlocked(triggerTypeMaster.getStatus(), ErrorCode.SHIPMENT_STATUS_TYPE_BLOCKED);
        ValidateUtil.isStatusHidden(triggerTypeMaster.getStatus(), ErrorCode.SHIPMENT_STATUS_TYPE_HIDDEN);

        st.setTriggerTypeMasterCode(triggerTypeMaster.getTriggerTypeCode());
    }

    private void verifyTriggerName(ShipmentServiceTrigger st) {

        ValidateUtil.notNull(st.getTriggerMaster(), ErrorCode.SHIPMENT_STATUS_NAME_REQUIRED);
        ValidateUtil.notNull(st.getTriggerMaster().getId(), ErrorCode.SHIPMENT_STATUS_NAME_REQUIRED);
        TriggerMaster triggerMaster = triggerMasterRepository.getOne(st.getTriggerMaster().getId());

        ValidateUtil.notNullOrEmpty(triggerMaster.getTriggerName(), ErrorCode.SHIPMENT_STATUS_NAME_REQUIRED);
        ValidateUtil.isStatusBlocked(triggerMaster.getStatus(), ErrorCode.SHIPMENT_STATUS_NAME_BLOCKED);
        ValidateUtil.isStatusHidden(triggerMaster.getStatus(), ErrorCode.SHIPMENT_STATUS_NAME_HIDDEN);

        st.setTriggerMasterCode(triggerMaster.getTriggerCode());
    }

    private void verifyProtect(ShipmentServiceTrigger st) {

        ValidateUtil.notNull(st.getProtect(), ErrorCode.SHIPMENT_STATUS_PROTECT_REQUIRED);

    }

    private void verifyAssignedTo(ShipmentServiceTrigger st) {

        if (st.getFollowUpRequired().equals(YesNo.Yes)) {

            ValidateUtil.notNull(st.getEmployeeMaster(), ErrorCode.SHIPMENT_STATUS_ASSIGNED_TO_REQUIRED);
            ValidateUtil.notNull(st.getEmployeeMaster().getId(), ErrorCode.SHIPMENT_STATUS_ASSIGNED_TO_REQUIRED);
            ValidateUtil.notNullOrEmpty(st.getEmployeeMaster().getEmployeeName(),
                    ErrorCode.SHIPMENT_STATUS_ASSIGNED_TO_REQUIRED);
            ValidateUtil.isEmployeeResigned(st.getEmployeeMaster().getEmployementStatus(),
                    ErrorCode.SHIPMENT_STATUS_ASSIGNED_TO_RESIGNED);
            ValidateUtil.isEmployeeTerminated(st.getEmployeeMaster().getEmployementStatus(),
                    ErrorCode.SHIPMENT_STATUS_ASSIGNED_TO_TERMINATED);
        } else {
            st.setEmployeeMaster(null);
        }

    }

    private void verifyFollowUpDate(ShipmentServiceTrigger st) {
/*
        if (st.getFollowUpRequired().equals(YesNo.Yes)) {

			ValidateUtil.notNull(st.getFollowUpDate(), ErrorCode.SHIPMENT_STATUS_FOLLOW_UP_DATE_REQUIRED);
		} else {
			st.setFollowUpDate(null);
		}
*/
    }

    private void verifyNote(ShipmentServiceTrigger st) {

        ValidateUtil.notNull(st.getNote(), ErrorCode.SHIPMENT_STATUS_NOTE_REQUIRED);
        ValidateUtil.notNullOrEmpty(st.getNote(), ErrorCode.SHIPMENT_STATUS_NOTE_REQUIRED);
		/*
		 * ValidateUtil.checkPattern(st.getNote(), "serviceStatusNote", true);
		 */

    }

}
