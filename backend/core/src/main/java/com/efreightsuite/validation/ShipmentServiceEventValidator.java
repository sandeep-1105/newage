package com.efreightsuite.validation;

import com.efreightsuite.enumeration.YesNo;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.model.EmployeeMaster;
import com.efreightsuite.model.EventMaster;
import com.efreightsuite.model.ShipmentServiceEvent;
import com.efreightsuite.repository.EmployeeMasterRepository;
import com.efreightsuite.repository.EventMasterRepository;
import com.efreightsuite.util.ValidateUtil;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class ShipmentServiceEventValidator {

    @Autowired
    private EventMasterRepository eventMasterRepository;

    @Autowired
    private EmployeeMasterRepository employeeMasterRepository;

    public void validate(ShipmentServiceEvent event) {

        verifyEvent(event);

        verifyDate(event);

        verifyAssignedTo(event);

    }


    private void verifyEvent(ShipmentServiceEvent event) {

        ValidateUtil.notNull(event.getEventMaster(), ErrorCode.SHIPMENT_EVENT);
        ValidateUtil.notNull(event.getEventMaster().getId(), ErrorCode.SHIPMENT_EVENT);
        EventMaster eventMaster = eventMasterRepository.getOne(event.getEventMaster().getId());

        ValidateUtil.notNullOrEmpty(eventMaster.getEventCode(), ErrorCode.SHIPMENT_EVENT);
        ValidateUtil.isStatusBlocked(eventMaster.getStatus(), ErrorCode.SHIPMENT_EVENT_BLOCK);
        ValidateUtil.isStatusHidden(eventMaster.getStatus(), ErrorCode.SHIPMENT_EVENT_HIDE);

        event.setEventCode(eventMaster.getEventCode());

    }

    private void verifyDate(ShipmentServiceEvent event) {

        ValidateUtil.notNull(event.getEventDate(), ErrorCode.SHIPMENT_EVENT_DATE);
    }

    private void verifyAssignedTo(ShipmentServiceEvent event) {

        if (event.getFollowUpRequired() != null && event.getFollowUpRequired().equals(YesNo.Yes)) {

            ValidateUtil.notNull(event.getAssignedTo(), ErrorCode.SHIPMENT_EVENT_ASSINGED);
            ValidateUtil.notNull(event.getAssignedTo().getId(), ErrorCode.SHIPMENT_EVENT_ASSINGED);
            EmployeeMaster assinedTo = employeeMasterRepository.findById(event.getAssignedTo().getId());

            ValidateUtil.notNullOrEmpty(assinedTo.getEmployeeCode(), ErrorCode.SHIPMENT_EVENT_ASSINGED);
            ValidateUtil.isEmployeeResigned(assinedTo.getEmployementStatus(), ErrorCode.SHIPMENT_EVENT_ASSINGED_BLOCK);
            ValidateUtil.isEmployeeTerminated(assinedTo.getEmployementStatus(), ErrorCode.SHIPMENT_EVENT_ASSINGED_HIDE);

            ValidateUtil.notNull(event.getFollowUpDate(), ErrorCode.SHIPMENT_EVENT_FOLLOW_DATE);

            event.setAssignedToCode(assinedTo.getEmployeeCode());

        } else {
            event.setAssignedTo(null);
            event.setAssignedToCode(null);
            event.setFollowUpDate(null);
        }

    }

}
