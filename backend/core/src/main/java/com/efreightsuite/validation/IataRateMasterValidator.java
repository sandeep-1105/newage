package com.efreightsuite.validation;

import java.util.Date;

import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.model.CarrierMaster;
import com.efreightsuite.model.IataRateAttachment;
import com.efreightsuite.model.IataRateCharge;
import com.efreightsuite.model.IataRateMaster;
import com.efreightsuite.model.PortMaster;
import com.efreightsuite.service.AuthService;
import com.efreightsuite.util.ValidateUtil;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class IataRateMasterValidator {

    @Autowired
    private
    IataRateAttachmentValidator iataRateAttachmentValidator;

    @Autowired
    private
    IataRateChargeValidator iataRateChargeValidator;

    public void validate(IataRateMaster iataRateMaster) {

        validateCarrierMaster(iataRateMaster.getCarrierMaster());
        validatePol(iataRateMaster.getPol());
        validateFromDate(iataRateMaster.getValidFromDate());
        validateToDate(iataRateMaster.getValidToDate());
        compareDate(iataRateMaster.getValidFromDate(), iataRateMaster.getValidToDate());

        iataRateMaster.setCompanyMaster(AuthService.getCurrentUser().getSelectedUserLocation().getCompanyMaster());
        iataRateMaster.setCompanyCode(
                AuthService.getCurrentUser().getSelectedUserLocation().getCompanyMaster().getCompanyCode());
        iataRateMaster.setLocationMaster(AuthService.getCurrentUser().getSelectedUserLocation());
        iataRateMaster.setLocationCode(AuthService.getCurrentUser().getSelectedUserLocation().getLocationCode());
        iataRateMaster.setCountryMaster(AuthService.getCurrentUser().getSelectedUserLocation().getCountryMaster());
        iataRateMaster.setCountryCode(
                AuthService.getCurrentUser().getSelectedUserLocation().getCountryMaster().getCountryCode());

    }

    private void compareDate(Date validFromDate, Date validToDate) {

        if (validToDate.before(validFromDate)) {
            throw new RestException(ErrorCode.VALID_FROM_LESSTHAN_VALID_TO);
        }

    }

    private void validateToDate(Date validTo) {

        ValidateUtil.notNull(validTo, ErrorCode.VALID_TO_EMPTY);

    }

    private void validateFromDate(Date validFrom) {
        ValidateUtil.notNull(validFrom, ErrorCode.VALID_FROM_EMPTY);

    }

    private void validatePol(PortMaster pol) {

        ValidateUtil.notNull(pol, ErrorCode.POL_IS_EMPTY);
        ValidateUtil.notNull(pol.getId(), ErrorCode.POL_IS_EMPTY);
        ValidateUtil.notNull(pol.getStatus(), ErrorCode.POL_SATUS_IS_EMPTY);
        ValidateUtil.isStatusBlocked(pol.getStatus(), ErrorCode.POL_IS_BLOCKED);
        ValidateUtil.isStatusHidden(pol.getStatus(), ErrorCode.POL_IS_HIDDEN);

    }

    private void validateCarrierMaster(CarrierMaster carrierMaster) {
        ValidateUtil.notNull(carrierMaster, ErrorCode.CARRIER_IS_EMPTY);
        ValidateUtil.notNull(carrierMaster.getId(), ErrorCode.CARRIER_IS_EMPTY);
        ValidateUtil.notNull(carrierMaster.getStatus(), ErrorCode.CARRIER_STATUS_IS_EMPTY);
        ValidateUtil.isStatusBlocked(carrierMaster.getStatus(), ErrorCode.CARRIER_IS_BLOCKED);
        ValidateUtil.isStatusHidden(carrierMaster.getStatus(), ErrorCode.CARRIER_IS_HIDDEN);


    }

    public void iataRateAttachmentValidate(IataRateAttachment iataRateAttachment) {
        ValidateUtil.notNull(iataRateAttachment, ErrorCode.ATTACHMENT_IS_EMPTY);
        iataRateAttachmentValidator.validate(iataRateAttachment);
        iataRateAttachment.setCompanyMaster(AuthService.getCurrentUser().getSelectedUserLocation().getCompanyMaster());
        iataRateAttachment.setCompanyCode(
                AuthService.getCurrentUser().getSelectedUserLocation().getCompanyMaster().getCompanyCode());
        iataRateAttachment.setLocationMaster(AuthService.getCurrentUser().getSelectedUserLocation());
        iataRateAttachment.setLocationCode(AuthService.getCurrentUser().getSelectedUserLocation().getLocationCode());
        iataRateAttachment.setCountryMaster(AuthService.getCurrentUser().getSelectedUserLocation().getCountryMaster());
        iataRateAttachment.setCountryCode(
                AuthService.getCurrentUser().getSelectedUserLocation().getCountryMaster().getCountryCode());

    }

    public void iataRateChargeValidate(IataRateCharge iataRateCharge) {
        ValidateUtil.notNull(iataRateCharge, ErrorCode.CHARGE_IS_EMPTY);
        iataRateChargeValidator.validate(iataRateCharge);
        iataRateCharge.setCompanyMaster(AuthService.getCurrentUser().getSelectedUserLocation().getCompanyMaster());
        iataRateCharge.setCompanyCode(
                AuthService.getCurrentUser().getSelectedUserLocation().getCompanyMaster().getCompanyCode());
        iataRateCharge.setLocationMaster(AuthService.getCurrentUser().getSelectedUserLocation());
        iataRateCharge.setLocationCode(AuthService.getCurrentUser().getSelectedUserLocation().getLocationCode());
        iataRateCharge.setCountryMaster(AuthService.getCurrentUser().getSelectedUserLocation().getCountryMaster());
        iataRateCharge.setCountryCode(
                AuthService.getCurrentUser().getSelectedUserLocation().getCountryMaster().getCountryCode());

    }

}
