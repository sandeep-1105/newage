package com.efreightsuite.validation;

import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.model.LocationMaster;
import com.efreightsuite.model.SuperAttachment;
import com.efreightsuite.repository.DefaultMasterDataRepository;
import com.efreightsuite.util.AppUtil;
import com.efreightsuite.util.ValidateUtil;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class SuperAttachmentValidator {

    @Autowired
    private
    AppUtil appUtil;

    @Autowired
    DefaultMasterDataRepository defaultMasterDataRepository;

    public void validate(SuperAttachment superAttachment, LocationMaster location) {

        verifyReferenceNumber(superAttachment);

        verifyProtectedFile(superAttachment, location);

    }

    private void verifyReferenceNumber(SuperAttachment superAttachment) {

        ValidateUtil.notNullOrEmpty(superAttachment.getRefNo(), ErrorCode.ATTACHMENT_REFERENCE_NUMBER_REQUIRED);

        ValidateUtil.validateLength(superAttachment.getRefNo(), 0, 100, ErrorCode.ATTACHMENT_REFERENCE_NUMBER_INVALID);

    }

    private void verifyProtectedFile(SuperAttachment superAttachment, LocationMaster location) {

        if (superAttachment.getId() == null) {

            if (superAttachment.getFile() != null && superAttachment.getFileName() != null) {

                String key = appUtil.getLocationConfig("upload.file.size.in.mb", location, false);

                if (key != null && key.trim().length() != 0) {

                    long fileSize = superAttachment.getFile().length;

                    long allowedProtectedFileSize = Long.parseLong(key) * 1024 * 1024;

                    if (fileSize == 0) {
                        throw new RestException(ErrorCode.ATTACHMENT_PROTECTED_FILE_EMPTY);
                    }

                    if (fileSize > allowedProtectedFileSize) {
                        throw new RestException(ErrorCode.ATTACHMENT_PROTECTED_FILE_SIZE_EXCEED);
                    }

                }
            }
        }
    }

}
