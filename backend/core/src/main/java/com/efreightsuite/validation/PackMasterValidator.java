package com.efreightsuite.validation;

import com.efreightsuite.enumeration.LovStatus;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.model.PackMaster;
import com.efreightsuite.service.RegularExpressionService;
import com.efreightsuite.util.RegExpName;
import com.efreightsuite.util.ValidateUtil;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Log4j2
@Service
public class PackMasterValidator {

    @Autowired
    private
    RegularExpressionService regExpService;


    public void validate(PackMaster packMaster) {


        ValidateUtil.notNull(packMaster.getPackCode(), ErrorCode.PACK_CODE_NOT_NULL);
        regExpService.validate(packMaster.getPackCode(), RegExpName.Reg_Exp_Master_Pack_Code, ErrorCode.PACK_CODE_INVALID);

        ValidateUtil.notNull(packMaster.getPackName(), ErrorCode.PACK_NAME_NOT_NULL);
        regExpService.validate(packMaster.getPackName(), RegExpName.Reg_Exp_Master_Pack_Name, ErrorCode.PACK_NAME_INVALID);


        ValidateUtil.notNull(packMaster.getStatus(), ErrorCode.PACK_LOV_STATUS_NOT_NULL);

        ValidateUtil.validateEnum(LovStatus.class, packMaster.getStatus(),
                ErrorCode.PACK_LOV_STATUS_INVALID);

    }
}
