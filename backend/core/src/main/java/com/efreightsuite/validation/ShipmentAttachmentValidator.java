package com.efreightsuite.validation;

import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.model.DocumentMaster;
import com.efreightsuite.model.ShipmentAttachment;
import com.efreightsuite.model.ShipmentServiceDetail;
import com.efreightsuite.repository.DefaultMasterDataRepository;
import com.efreightsuite.repository.DocumentMasterRepository;
import com.efreightsuite.util.ValidateUtil;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class ShipmentAttachmentValidator {

    @Autowired
    DefaultMasterDataRepository defaultMasterDataRepository;

    @Autowired
    private
    SuperAttachmentValidator superAttachmentValidator;

    @Autowired
    private
    DocumentMasterRepository documentMasterRepository;

    public void validate(ShipmentAttachment shipmentAttachment, ShipmentServiceDetail service) {


        verifyDocument(shipmentAttachment);

        superAttachmentValidator.validate(shipmentAttachment, service.getLocation());

    }

    private void verifyDocument(ShipmentAttachment shipmentAttachment) {

        if (shipmentAttachment.getDocumentMaster() != null && shipmentAttachment.getDocumentMaster().getId() != null) {


            DocumentMaster document = documentMasterRepository.findById(shipmentAttachment.getDocumentMaster().getId());
            ValidateUtil.isStatusBlocked(document.getStatus(), ErrorCode.ATTACHMENT_DOCUMENT_BLOCKED);

            ValidateUtil.isStatusBlocked(document.getStatus(), ErrorCode.ATTACHMENT_DOCUMENT_HIDDEN);

        } else {
            shipmentAttachment.setDocumentMaster(null);
        }
    }

}
