package com.efreightsuite.validation;

import com.efreightsuite.dto.SearchRequest;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.util.ValidateUtil;

import org.springframework.stereotype.Service;

@Service
public class SearchDtoValidator {

    public void validate(SearchRequest searchRequest, boolean isPaginatedSearch) {

        if (isPaginatedSearch) {

            ValidateUtil.notNull(searchRequest.getSelectedPageNumber(), ErrorCode.SEARCH_SELECTED_PAGE_NUMBER_MISSING);

            ValidateUtil.notNull(searchRequest.getRecordPerPage(), ErrorCode.SEARCH_RECORD_PER_PAGE_MISSING);

        } else {
            ValidateUtil.notNullOrEmpty(searchRequest.getKeyword(), ErrorCode.SEARCH_KEYWORD_MISSING);
        }

    }

}
