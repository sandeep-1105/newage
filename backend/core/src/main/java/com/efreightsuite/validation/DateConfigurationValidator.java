package com.efreightsuite.validation;

import com.efreightsuite.common.component.CacheRepository;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.model.DateConfiguration;
import com.efreightsuite.model.ServiceMaster;
import com.efreightsuite.repository.ServiceMasterRepository;
import com.efreightsuite.util.SaaSUtil;
import com.efreightsuite.util.ValidateUtil;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Log4j2
@Service
public class DateConfigurationValidator {

    @Autowired
    private
    CacheRepository cacheRepository;

    @Autowired
    private
    ServiceMasterRepository serviceMasterRepository;


    public void validate(DateConfiguration dateConfiguration) {
        log.info("DateConfiguration Validation Started....[" + (dateConfiguration.getId()) + "]");

        ValidateUtil.notNull(dateConfiguration.getServiceMaster(), ErrorCode.DATE_CONFIG_SERVICE);
        ValidateUtil.notNull(dateConfiguration.getServiceMaster().getId(), ErrorCode.DATE_CONFIG_SERVICE);
        ValidateUtil.notNull(dateConfiguration.getServiceMaster().getServiceCode(), ErrorCode.DATE_CONFIG_SERVICE);
        ServiceMaster serviceMaster = cacheRepository.get(SaaSUtil.getSaaSId(), ServiceMaster.class.getName(), dateConfiguration.getServiceMaster().getId(), serviceMasterRepository);
        ValidateUtil.notNull(serviceMaster, ErrorCode.DATE_CONFIG_SERVICE);
        ValidateUtil.notNull(serviceMaster.getId(), ErrorCode.DATE_CONFIG_SERVICE);
        ValidateUtil.notNull(serviceMaster.getServiceCode(), ErrorCode.DATE_CONFIG_SERVICE);
        ValidateUtil.isStatusBlocked(serviceMaster.getStatus(), ErrorCode.DATE_CONFIG_BLOCK);
        ValidateUtil.isStatusHidden(serviceMaster.getStatus(), ErrorCode.DATE_CONFIG_HIDE);
        dateConfiguration.setServiceCode(serviceMaster.getServiceCode());

        //Which Transaction Validation
        ValidateUtil.notNull(dateConfiguration.getWhichTransactionDate(), ErrorCode.DATE_CONFIG_WHICH_OPERATION);

        //Date logic Validation
        ValidateUtil.notNull(dateConfiguration.getDateLogic(), ErrorCode.DATE_CONFIG_LOGIC_DATE);

        log.info("DateConfiguration Validation Finished....[" + (dateConfiguration.getId()) + "]");
    }

}
