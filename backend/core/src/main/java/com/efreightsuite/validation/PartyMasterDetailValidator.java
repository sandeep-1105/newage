package com.efreightsuite.validation;

import java.util.Objects;

import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.model.CountryMaster;
import com.efreightsuite.model.DynamicFields;
import com.efreightsuite.model.PartyMaster;
import com.efreightsuite.model.PartyMasterDetail;
import com.efreightsuite.repository.DefaultMasterDataRepository;
import com.efreightsuite.repository.DynamicFieldRepository;
import com.efreightsuite.service.RegularExpressionService;
import com.efreightsuite.util.RegExpName;
import com.efreightsuite.util.ValidateUtil;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.collections4.CollectionUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class PartyMasterDetailValidator {

    @Autowired
    private
    DynamicFieldRepository dynamicFieldRepository;

    @Autowired
    DefaultMasterDataRepository defaultMasterDataRepository;

    @Autowired
    private
    RegularExpressionService regExpService;

    public void validate(PartyMaster partyMaster) {

        PartyMasterDetail partyMasterDetail = partyMaster.getPartyDetail();

        log.info("PartyMasterDetail Validations Started...");

        verifyVatNumber(partyMasterDetail);
        verifyPanNumber(partyMasterDetail);
        verifyCstNumber(partyMasterDetail);
        verifySvtNumber(partyMasterDetail);
        verifySvatNumber(partyMasterDetail);
        verifyRacNumber(partyMasterDetail);
        verifyTinNumber(partyMasterDetail);
        verifyIataCode(partyMasterDetail);
        verifyNlsNames(partyMaster);
        verifyAccNumber(partyMasterDetail);

        verifyPin(partyMaster.getPartyDetail().getPin());
        verifyTsaNo(partyMaster.getPartyDetail().getTsaNo());
        verifySpotNo(partyMaster.getPartyDetail().getSpotNo());
        verifyPaymentSchedule(partyMasterDetail);


        if (String.valueOf(partyMasterDetail.getCafPercentage()) != "null") {

            log.info("Double value" + String.valueOf(partyMasterDetail.getCafPercentage()));

            if (partyMasterDetail.getCafPercentage() != 0.0d) {
                partyMasterDetail.setCafPercentage(Math.round((partyMasterDetail.getCafPercentage()) * 100.0) / 100.0);
                verifyCafPercentage(partyMasterDetail);
            }
        }

        if (String.valueOf(partyMasterDetail.getCcPercentage()) != "null" && partyMasterDetail.getCcPercentage() != 0.0d) {
            partyMasterDetail.setCcPercentage(Math.round((partyMasterDetail.getCcPercentage()) * 100.0) / 100.0);
            verifyCCPercentage(partyMasterDetail);

        }

        if (String.valueOf(partyMasterDetail.getCcPercentage()) != "null" && partyMasterDetail.getCcPercentage() != 0.0d) {
            partyMasterDetail.setTdsPercentage(Math.round((partyMasterDetail.getTdsPercentage()) * 100.0) / 100.0);
            verifyTDSPercentage(partyMasterDetail);
        }

        log.info("PartyMasterDetail Validations Finished...");

    }

    private void verifyPin(String pin) {
        if (pin != null) {
            regExpService.validate(pin, RegExpName.Reg_Exp_Master_PARTY_PIN, ErrorCode.PARTY_PIN_INVALID);
        }
    }

    private void verifyTsaNo(String tsaNo) {
        if (tsaNo != null) {
            regExpService.validate(tsaNo, RegExpName.Reg_Exp_Master_PARTY_TSANO, ErrorCode.PARTY_TSANO_INVALID);
        }
    }

    private void verifySpotNo(String spotNo) {
        if (spotNo != null) {
            regExpService.validate(spotNo, RegExpName.Reg_Exp_Master_PARTY_SPOTNO, ErrorCode.PARTY_SPOTNO_INVALID);
        }
    }

    public void verifyTDSPercentage(PartyMasterDetail partyMasterDetail) {

        ValidateUtil.decimalLength(partyMasterDetail.getTdsPercentage(), 0, 100, ErrorCode.PARTY_TDSPERCENTAGE_INVALID);
    }

    public void verifyCCPercentage(PartyMasterDetail partyMasterDetail) {

        ValidateUtil.decimalLength(partyMasterDetail.getCcPercentage(), 0, 100, ErrorCode.PARTY_CCPERCENTAGE_INVALID);
    }

    public void verifyCafPercentage(PartyMasterDetail partyMasterDetail) {

        ValidateUtil.decimalLength(partyMasterDetail.getCafPercentage(), 0, 100, ErrorCode.PARTY_CAFPERCENTAGE_INVALID);

    }

    public void verifyVatNumber(PartyMasterDetail partyMasterDetail) {
        if (partyMasterDetail.getValueAddedTaxNo() != null && partyMasterDetail.getValueAddedTaxNo().length() != 0) {
            regExpService.validate(partyMasterDetail.getValueAddedTaxNo(), RegExpName.Reg_Exp_Master_PARTY_VAT_NO, ErrorCode.PARTY_VAT_NUMBER_INVALID);
        }
    }

    public void verifyPanNumber(PartyMasterDetail partyMasterDetail) {
        if (partyMasterDetail.getPanNo() != null && partyMasterDetail.getPanNo().length() != 0) {
            regExpService.validate(partyMasterDetail.getPanNo(), RegExpName.Reg_Exp_Master_PARTY_PAN_NO, ErrorCode.PARTY_PAN_NUMBER_INVALID);
        }
    }

    private void verifyCstNumber(PartyMasterDetail partyMasterDetail) {
        if (partyMasterDetail.getCstNo() != null && partyMasterDetail.getCstNo().length() != 0) {
            regExpService.validate(partyMasterDetail.getCstNo(), RegExpName.Reg_Exp_Master_PARTY_CST_NO, ErrorCode.PARTY_CST_NUMBER_INVALID);
        }
    }

    private void verifySvtNumber(PartyMasterDetail partyMasterDetail) {
        if (partyMasterDetail.getSvtNo() != null && partyMasterDetail.getSvtNo().length() != 0) {
            regExpService.validate(partyMasterDetail.getSvtNo(), RegExpName.Reg_Exp_Master_PARTY_SVT_NO, ErrorCode.PARTY_SVT_NUMBER_INVALID);
        }
    }

    public void verifyGstNumber(PartyMasterDetail partyMasterDetail) {
        if (partyMasterDetail.getGstNo() != null && partyMasterDetail.getGstNo().length() != 0) {
            regExpService.validate(partyMasterDetail.getGstNo(), RegExpName.Reg_Exp_Master_PARTY_GST_NO, ErrorCode.PARTY_GST_NUMBER_INVALID);
        }
    }

    private void verifySvatNumber(PartyMasterDetail partyMasterDetail) {
        if (partyMasterDetail.getSvatNo() != null && partyMasterDetail.getSvatNo().length() != 0) {
            regExpService.validate(partyMasterDetail.getSvatNo(), RegExpName.Reg_Exp_Master_PARTY_SVAT_NO, ErrorCode.PARTY_SVAT_NUMBER_INVALID);
        }
    }

    private void verifyRacNumber(PartyMasterDetail partyMasterDetail) {
        if (partyMasterDetail.getRacNo() != null && partyMasterDetail.getRacNo().length() != 0) {
            regExpService.validate(partyMasterDetail.getRacNo(), RegExpName.Reg_Exp_Master_PARTY_RAC_NO, ErrorCode.PARTY_RAC_NUMBER_INVALID);
        }
    }

    private void verifyTinNumber(PartyMasterDetail partyMasterDetail) {
        if (partyMasterDetail.getTinNo() != null && partyMasterDetail.getTinNo().length() != 0) {
            regExpService.validate(partyMasterDetail.getTinNo(), RegExpName.Reg_Exp_Master_PARTY_TIN_NO, ErrorCode.PARTY_TIN_NUMBER_INVALID);
        }
    }

    private void verifyIataCode(PartyMasterDetail partyMasterDetail) {
        if (partyMasterDetail.getIataCode() != null && partyMasterDetail.getIataCode().length() != 0) {
            regExpService.validate(partyMasterDetail.getIataCode(), RegExpName.Reg_Exp_Master_PARTY_IATA_CODE, ErrorCode.PARTY_IATA_CODE_INVALID);
        }
    }


    private void verifyAccNumber(PartyMasterDetail partyMasterDetail) {
        if (partyMasterDetail.getAccountNumber() != null && partyMasterDetail.getAccountNumber().length() != 0) {
            regExpService.validate(partyMasterDetail.getAccountNumber(), RegExpName.Reg_Exp_Master_PARTY_ACC_NO, ErrorCode.PARTY_ACC_NUM_INVALID);
        }
    }


    private void verifyNlsNames(PartyMaster partyMaster) {

        DynamicFields tmp = dynamicFieldRepository.findByFieldId("Party Master > Party Name (Regional)");

        if (tmp != null && CollectionUtils.isNotEmpty(tmp.getCountryMasterList())) {

            for (CountryMaster country : tmp.getCountryMasterList()) {
                if (partyMaster.getCountryMaster() != null && Objects.equals(partyMaster.getCountryMaster().getId(), country.getId())) {
                    ValidateUtil.notNull(partyMaster.getPartyDetail().getNlsCustomerName(), ErrorCode.PARTY_NLS_CUSTOMER_NAME_REQUIRED);
                    regExpService.validate(partyMaster.getPartyDetail().getNlsCustomerName(), RegExpName.Reg_Exp_Master_PARTY_REGIONAL_NAME, ErrorCode.PARTY_NAME_REGIONAL_INVALID);
                }
            }

        }


    }


    private void verifyPaymentSchedule(PartyMasterDetail partyMasterDetail) {

        if (partyMasterDetail.getPaymentSchedule() != null && partyMasterDetail.getPaymentSchedule().getId() != null) {


            ValidateUtil.isStatusBlocked(partyMasterDetail.getPaymentSchedule().getStatus(), ErrorCode.PARTY_PAYMENT_SCHEDULE_BLOCKED);

            ValidateUtil.isStatusHidden(partyMasterDetail.getPaymentSchedule().getStatus(), ErrorCode.PARTY_PAYMENT_SCHEDULE_HIDDEN);

            log.info("Party Payment Schedule is validated : [" + partyMasterDetail.getPaymentSchedule().getFrequencyName() + "]");
            partyMasterDetail.setPaymentScheduleCode(partyMasterDetail.getPaymentSchedule().getFrequencyCode());
        } else {
            partyMasterDetail.setPaymentSchedule(null);
            partyMasterDetail.setPaymentScheduleCode(null);
        }

    }

}
