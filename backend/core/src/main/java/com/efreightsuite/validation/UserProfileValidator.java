package com.efreightsuite.validation;

import java.util.Date;

import com.efreightsuite.enumeration.UserStatus;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.model.UserProfile;
import com.efreightsuite.util.AppUtil;
import com.efreightsuite.util.ValidateUtil;
import lombok.extern.log4j.Log4j2;

import org.springframework.stereotype.Service;

@Service
@Log4j2
public class UserProfileValidator {

    public void validate(UserProfile userProfile) {

        log.info(
                "UserProfile Validation Started....[" + (userProfile.getId() != null ? userProfile.getId() : "") + "]");

        ValidateUtil.notNull(userProfile, ErrorCode.USERPROFILE_NOT_NULL);


        validateUserName(userProfile);
        validatePassword(userProfile);
        validatePasswordExpiresOn(userProfile);

        // Validating LOV Status of Category Master

        ValidateUtil.validateEnum(UserStatus.class, userProfile.getStatus(), ErrorCode.USERPROFILE_STATUS_NOT_NULL);

        log.info("UserProfile Validation Finished....[" + (userProfile.getId() != null ? userProfile.getId() : "")
                + "]");
    }

    private void validatePassword(UserProfile userProfile) {
        ValidateUtil.notNullOrEmpty(userProfile.getPassword(), ErrorCode.USERPROFILE_PASSWORD_NOT_NULL);

        ValidateUtil.validateLength(userProfile.getPassword(), 1, 255, ErrorCode.USERPROFILE_PASSWORD_LENGTH_EXCEED);

        ValidateUtil.checkPattern(userProfile.getPassword(), "userprofile.password", true);

        log.debug("Password Validated...");

    }

    private void validateUserName(UserProfile userProfile) {

        ValidateUtil.notNullOrEmpty(userProfile.getUserName(), ErrorCode.USERPROFILE_NAME_NOT_NULL);

        ValidateUtil.validateLength(userProfile.getUserName(), 1, 100, ErrorCode.USERPROFILE_NAME_LENGTH_EXCEED);

        ValidateUtil.checkPattern(userProfile.getUserName(), "userprofile.userName", true);

        log.debug("Usere Name Validated...");

    }


    private void validatePasswordExpiresOn(UserProfile userProfile) {

        Date currentDate = AppUtil.removeTime(new Date());
        if (userProfile.getPasswordExpiresOn() != null) {
            Date expireDate = AppUtil.removeTime(userProfile.getPasswordExpiresOn());
            if (expireDate.getTime() >= currentDate.getTime()) {
                log.info("valid passwordExpireDate");
            } else {
                throw new RestException(ErrorCode.PASSWORD_EXPIRESON_DATE);
            }
        }

        log.debug("PasswordExpiresOn Validated...");

    }

	/*private void validatePasswordExpiresOn(UserProfile userProfile) {
		log.debug("PasswordExpiresOn start...");

		if(userProfile.getPasswordExpiresOn()!=null)
		{
			
			Date currentLocationTime = TimeUtil.getCurrentLocationTime();
			
		    DateFormat currentDateformatter = new SimpleDateFormat("dd/MM/yyyy");
	        String currentDate = currentDateformatter.format(currentLocationTime);
	        
	        DateFormat expiresOnDateformatter = new SimpleDateFormat("dd/MM/yyyy");
	        String passwordExpiresOnDate = expiresOnDateformatter.format(userProfile.getPasswordExpiresOn());
	        
	        int notValid  =  passwordExpiresOnDate.compareTo(currentDate);
			
			if(notValid==-1)
			{
				throw new RestException(ErrorCode.PASSWORD_EXPIRESON_DATE);
			}
		}

		log.debug("PasswordExpiresOn Validated...");

	}*/

}
