package com.efreightsuite.validation;


import com.efreightsuite.enumeration.LovStatus;
import com.efreightsuite.enumeration.MappingUnit;
import com.efreightsuite.enumeration.UnitCalculationType;
import com.efreightsuite.enumeration.UnitType;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.model.UnitMaster;
import com.efreightsuite.service.RegularExpressionService;
import com.efreightsuite.util.RegExpName;
import com.efreightsuite.util.ValidateUtil;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class UnitMasterValidator {

    @Autowired
    private
    RegularExpressionService regExpService;

    public void validate(UnitMaster unitMaster) {


        ValidateUtil.notNull(unitMaster.getUnitCode(), ErrorCode.UNIT_CODE_NOT_NULL);
        regExpService.validate(unitMaster.getUnitCode(), RegExpName.Reg_Exp_Master_Unit_Code, ErrorCode.UNIT_CODE_INVALID);

        ValidateUtil.notNull(unitMaster.getUnitName(), ErrorCode.UNIT_NAME_NOT_NULL);
        regExpService.validate(unitMaster.getUnitName(), RegExpName.Reg_Exp_Master_Unit_Name, ErrorCode.UNIT_NAME_INVALID);

        ValidateUtil.notNull(unitMaster.getStatus(), ErrorCode.UNIT_LOV_STATUS_NOT_NULL);
        ValidateUtil.validateEnum(LovStatus.class, unitMaster.getStatus(), ErrorCode.UNIT_LOV_STATUS_NOT_NULL);

        ValidateUtil.notNull(unitMaster.getUnitType(), ErrorCode.UNIT_TYPE_NOT_NULL);

        if (unitMaster.getUnitType().equals(UnitType.Unit)) {

            ValidateUtil.notNull(unitMaster.getMappingUnit1(), ErrorCode.UNIT_MAPPING_1_NOT_NULL);
            ValidateUtil.validateEnum(MappingUnit.class, unitMaster.getMappingUnit1(), ErrorCode.UNIT_MAPPING_1_INVALID);

            ValidateUtil.notNull(unitMaster.getCalcType1(), ErrorCode.UNIT_CALC_TYPE_1_NOT_NULL);
            ValidateUtil.validateEnum(UnitCalculationType.class, unitMaster.getCalcType1(), ErrorCode.UNIT_CALC_TYPE_1_INVALID);

            ValidateUtil.notNull(unitMaster.getCalcValue1(), ErrorCode.UNIT_CALC_VALUE_1_NOT_NULL);
            regExpService.validate(unitMaster.getCalcValue1(), RegExpName.Reg_Exp_Master_Unit_Calculation_1, ErrorCode.UNIT_CALC_VALUE_1_INVALID);


            if (unitMaster.getMappingUnit2() != null || unitMaster.getCalcType2() != null || unitMaster.getCalcValue2() != null) {

                ValidateUtil.notNull(unitMaster.getMappingUnit2(), ErrorCode.UNIT_MAPPING_2_NOT_NULL);
                ValidateUtil.notNull(unitMaster.getCalcType2(), ErrorCode.UNIT_CALC_TYPE_2_NOT_NULL);
                ValidateUtil.notNull(unitMaster.getCalcValue2(), ErrorCode.UNIT_CALC_VALUE_2_NOT_NULL);

                ValidateUtil.validateEnum(MappingUnit.class, unitMaster.getMappingUnit2(), ErrorCode.UNIT_MAPPING_2_INVALID);
                ValidateUtil.validateEnum(UnitCalculationType.class, unitMaster.getCalcType2(), ErrorCode.UNIT_CALC_TYPE_2_INVALID);
                regExpService.validate(unitMaster.getCalcValue2(), RegExpName.Reg_Exp_Master_Unit_Calculation_2, ErrorCode.UNIT_CALC_VALUE_2_INVALID);
            }

        } else {
            unitMaster.setMappingUnit1(null);
            unitMaster.setCalcType1(null);
            unitMaster.setCalcValue1(null);

            unitMaster.setMappingUnit2(null);
            unitMaster.setCalcType2(null);
            unitMaster.setCalcValue2(null);
        }

    }

}
