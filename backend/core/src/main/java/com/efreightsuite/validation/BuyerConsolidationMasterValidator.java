package com.efreightsuite.validation;

import com.efreightsuite.enumeration.LovStatus;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.model.BuyerConsolidationMaster;
import com.efreightsuite.service.RegularExpressionService;
import com.efreightsuite.util.RegExpName;
import com.efreightsuite.util.ValidateUtil;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Log4j2
@Service
public class BuyerConsolidationMasterValidator {

    @Autowired
    private
    RegularExpressionService regExpRegion;

    public void validate(BuyerConsolidationMaster buyerConsolidationMaster) {
        log.info("Bc Master Validation Begins.... [" + (buyerConsolidationMaster.getId()) != null ? buyerConsolidationMaster.getId() : "" + "]");

        ValidateUtil.notNull(buyerConsolidationMaster, ErrorCode.BUYER_CONSOLIDATION_NOT_NULL);

        // Validating BcCode (Mandatory)
        ValidateUtil.notNullOrEmpty(buyerConsolidationMaster.getBcCode(), ErrorCode.BUYER_CONSOLIDATION_CODE_NOT_NULL);
        ValidateUtil.checkPattern(buyerConsolidationMaster.getBcCode(), "bcCode", true);
        regExpRegion.validate(buyerConsolidationMaster.getBcCode(), RegExpName.Reg_Exp_Master_Buyer_Consolidation_Code, ErrorCode.BUYER_CONSOLIDATION_CODE_INVALID);
        log.debug("BUYER_CONSOLIDATION Code Valdiated..............." + buyerConsolidationMaster.getBcCode());

        // Validating BcName (Mandatory)
        ValidateUtil.notNullOrEmpty(buyerConsolidationMaster.getBcName(), ErrorCode.BUYER_CONSOLIDATION_NAME_NOT_NULL);
        ValidateUtil.checkPattern(buyerConsolidationMaster.getBcName(), "bcName", true);
        regExpRegion.validate(buyerConsolidationMaster.getBcName(), RegExpName.Reg_Exp_Master_Buyer_Consolidation_Name, ErrorCode.BUYER_CONSOLIDATION_NAME_INVALID);
        log.debug("BUYER_CONSOLIDATION Name Valdiated..............." + buyerConsolidationMaster.getBcName());

        if (buyerConsolidationMaster.getStatus() == null) {
            buyerConsolidationMaster.setStatus(LovStatus.Active);
        }

        log.info("Bc Master Validation Ends.... [" + buyerConsolidationMaster.getId() + "]");
    }

}
