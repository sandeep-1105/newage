package com.efreightsuite.validation;

import com.efreightsuite.enumeration.EmploymentStatus;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.model.DepartmentMaster;
import com.efreightsuite.util.ValidateUtil;

import org.springframework.stereotype.Service;

@Service
public class DepartmentMasterValidator {

    public void validate(DepartmentMaster departmentMaster) {
        //Department code is auto generated hence no validation requires
        /*ValidateUtil.notNullOrEmpty(departmentMaster.getDepartmentCode(), ErrorCode.DEPARTMENT_CODE_REQUIRED);
		ValidateUtil.validateLength(departmentMaster.getDepartmentCode(), 0, 10, ErrorCode.DEPARTMENT_CODE_INVALID);*/
        ValidateUtil.notNullOrEmpty(departmentMaster.getDepartmentName(), ErrorCode.DEPARTMENT_NAME_REQUIRED);
        ValidateUtil.validateLength(departmentMaster.getDepartmentName(), 0, 100, ErrorCode.DEPARTMENT_NAME_INVALID);


        ValidateUtil.notNull(departmentMaster.getDepartmentHead(), ErrorCode.DEPARTMENT_DEPARTMENTHEAD_MANDATORY);
        ValidateUtil.notNull(departmentMaster.getDepartmentHead().getId(), ErrorCode.DEPARTMENT_DEPARTMENTHEAD_MANDATORY);


        if (departmentMaster.getDepartmentHead().getEmployementStatus().equals(EmploymentStatus.RESIGNED)) {
            throw new RestException(ErrorCode.DEPARTMENT_DEPARTMENTHEAD_RESIGNED);
        }

        if (departmentMaster.getDepartmentHead().getEmployementStatus().equals(EmploymentStatus.TERMINATED)) {
            throw new RestException(ErrorCode.DEPARTMENT_DEPARTMENTHEAD_TERMINATED);
        }


    }

}
