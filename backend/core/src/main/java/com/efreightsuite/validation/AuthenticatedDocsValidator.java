package com.efreightsuite.validation;

import java.util.regex.Pattern;

import com.efreightsuite.common.component.CacheRepository;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.model.AuthenticatedDocs;
import com.efreightsuite.model.PartyMaster;
import com.efreightsuite.model.PortMaster;
import com.efreightsuite.model.ShipmentServiceDetail;
import com.efreightsuite.repository.DefaultMasterDataRepository;
import com.efreightsuite.repository.PartyMasterRepository;
import com.efreightsuite.repository.PortMasterRepository;
import com.efreightsuite.service.AuthService;
import com.efreightsuite.util.AppUtil;
import com.efreightsuite.util.SaaSUtil;
import com.efreightsuite.util.ValidateUtil;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class AuthenticatedDocsValidator {

    @Autowired
    private
    AppUtil appUtil;

    @Autowired
    private
    CacheRepository cacheRepository;

    @Autowired
    private
    PartyMasterRepository partyMasterRepository;

    @Autowired
    DefaultMasterDataRepository defaultMasterDataRepository;

    @Autowired
    PortMasterRepository portMasterRepository;

    public void validate(AuthenticatedDocs authenticatedDocs, ShipmentServiceDetail service) {


        String emailSplit = appUtil.getLocationConfig("single.text.split.by.multiple.email", service.getLocation(), false);


        ValidateUtil.notNull(authenticatedDocs.getDocumentNo(), ErrorCode.AUTH_DOCS_DOCUMNENT_NO_MANDATORY);
        ValidateUtil.notNull(authenticatedDocs.getDate(), ErrorCode.AUTH_DOCS_DATE_MANDATORY);
        verifyOrigin(authenticatedDocs, authenticatedDocs.getOrigin());
        verifyDestination(authenticatedDocs, authenticatedDocs.getDestination());
        verifyPieces(authenticatedDocs.getNoOfPiece());
        verifyVolumeWeight(authenticatedDocs.getVolWeight());
        verifyGrossWeight(authenticatedDocs.getGrossWeight());
        verifyShiper(authenticatedDocs, emailSplit);
        verifyConsignee(authenticatedDocs, emailSplit);
        verifyFirstNotify(authenticatedDocs, emailSplit);
        verifyForwarder(authenticatedDocs, emailSplit);

        verifyCommodity(authenticatedDocs.getCommodity());
        verifyMarksAndNo(authenticatedDocs.getMarksAndNo());

        authenticatedDocs.setCompanyMaster(AuthService.getCurrentUser().getSelectedUserLocation().getCompanyMaster());
        authenticatedDocs.setCompanyCode(
                AuthService.getCurrentUser().getSelectedUserLocation().getCompanyMaster().getCompanyCode());
        authenticatedDocs.setLocationMaster(AuthService.getCurrentUser().getSelectedUserLocation());
        authenticatedDocs.setLocationCode(AuthService.getCurrentUser().getSelectedUserLocation().getLocationCode());
        authenticatedDocs.setCountryMaster(AuthService.getCurrentUser().getSelectedUserLocation().getCountryMaster());
        authenticatedDocs.setCountryCode(
                AuthService.getCurrentUser().getSelectedUserLocation().getCountryMaster().getCountryCode());

    }

    private void verifyMarksAndNo(String marksAndNo) {

        if (marksAndNo != null) {
            if (marksAndNo.length() > 4000) {
                throw new RestException(ErrorCode.AUTH_DOCS_MARKS_AND_NO_LENGTH_INVALID);
            }
        }

    }

    private void verifyCommodity(String commodity) {
        if (commodity != null) {
            if (commodity.length() > 4000) {
                throw new RestException(ErrorCode.AUTH_DOCS_COMMODITY_INVALID_LENGTH_INVALID);
            }
        }

    }

    private void verifyPieces(int noOfPiece) {
        ValidateUtil.nonZero(noOfPiece, ErrorCode.AUTH_DOCS_NOOFPIECES_NOT_ZERO);
        if (noOfPiece <= 0 && noOfPiece > 999999999) {
            throw new RestException(ErrorCode.AUTH_DOCS_NOOFPIECES_INVALID);
        }
    }

    private void verifyOrigin(AuthenticatedDocs authenticatedDocs, PortMaster origin) {
        ValidateUtil.notNull(origin, ErrorCode.AUTH_DOCS_ORIGIN);
        ValidateUtil.notNull(origin.getId(), ErrorCode.AUTH_DOCS_ORIGIN);

        PortMaster originMaster = portMasterRepository.findById(origin.getId());

        ValidateUtil.notNullOrEmpty(originMaster.getPortCode(), ErrorCode.AUTH_DOCS_ORIGIN);
        ValidateUtil.isStatusBlocked(originMaster.getStatus(), ErrorCode.AUTH_DOCS_ORIGIN_BLOCK);
        ValidateUtil.isStatusHidden(originMaster.getStatus(), ErrorCode.AUTH_DOCS_ORIGIN_HIDE);
        authenticatedDocs.setOriginCode(originMaster.getPortCode());
    }

    private void verifyDestination(AuthenticatedDocs authenticatedDocs, PortMaster destination) {
        ValidateUtil.notNull(destination, ErrorCode.AUTH_DOCS_DESTINATION);
        ValidateUtil.notNull(destination.getId(), ErrorCode.AUTH_DOCS_DESTINATION);

        PortMaster destinationMaster = portMasterRepository.findById(destination.getId());
        ValidateUtil.notNullOrEmpty(destinationMaster.getPortCode(), ErrorCode.AUTH_DOCS_DESTINATION);
        ValidateUtil.isStatusBlocked(destinationMaster.getStatus(), ErrorCode.AUTH_DOCS_DESTINATION_BLOCK);
        ValidateUtil.isStatusHidden(destinationMaster.getStatus(), ErrorCode.AUTH_DOCS_DESTINATION_HIDE);
        authenticatedDocs.setDestinationCode(destinationMaster.getPortCode());
    }

    private void verifyVolumeWeight(Double volumeWeight) {
        ValidateUtil.nonZero(volumeWeight, ErrorCode.AUTH_DOCS_VOLUME_WEIGHT_NOT_ZERO);
        /*ValidateUtil.checkPattern(volumeWeight, "volume-weight", true);*/

    }

    private void verifyGrossWeight(Double grossWeight) {
        ValidateUtil.nonZero(grossWeight, ErrorCode.AUTH_DOCS_GROSS_WEIGHT_NOT_ZERO);
	/*	ValidateUtil.checkPattern(grossWeight, "gross-weight", true);*/

    }

    private void verifyFirstNotify(AuthenticatedDocs authenticatedDocs, String emailSplit) {

        if (authenticatedDocs.getFirstNotify() != null && authenticatedDocs.getFirstNotify().getId() != null) {

            PartyMaster tmpParty = cacheRepository.get(SaaSUtil.getSaaSId(), PartyMaster.class.getName(),
                    authenticatedDocs.getFirstNotify().getId(), partyMasterRepository);

            ValidateUtil.notNull(tmpParty, ErrorCode.AUTH_DOCS_FIRST_NOTIFY);
            ValidateUtil.notNull(tmpParty.getId(), ErrorCode.AUTH_DOCS_FIRST_NOTIFY);
            ValidateUtil.notNullOrEmpty(tmpParty.getPartyCode(), ErrorCode.AUTH_DOCS_FIRST_NOTIFY);
            ValidateUtil.isStatusBlocked(tmpParty.getStatus(), ErrorCode.AUTH_DOCS_FIRST_NOTIFY_BLOCK);
            ValidateUtil.isStatusHidden(tmpParty.getStatus(), ErrorCode.AUTH_DOCS_FIRST_NOTIFY_HIDE);
            ValidateUtil.isDefaulter(tmpParty, ErrorCode.SHIPMENT_DOC_FNOTI_DEFAULTER);

            ValidateUtil.notNull(authenticatedDocs.getFirstNotifyAddress(),
                    ErrorCode.AUTH_DOCS_FIRST_NOTIFY_ADDRESS);

            ValidateUtil.notNullOrEmpty(authenticatedDocs.getFirstNotifyAddress().getAddressLine1(),
                    ErrorCode.AUTH_DOCS_FIRST_NOTIFY_ADDRESS_1);
            ValidateUtil.validateLength(authenticatedDocs.getFirstNotifyAddress().getAddressLine1(), 1, 100,
                    ErrorCode.AUTH_DOCS_FIRST_NOTIFY_ADDRESS_1_LENGTH);

            if (authenticatedDocs.getFirstNotifyAddress().getAddressLine2() != null && authenticatedDocs.getFirstNotifyAddress().getAddressLine2().trim().length() != 0) {
                ValidateUtil.notNullOrEmpty(authenticatedDocs.getFirstNotifyAddress().getAddressLine2(),
                        ErrorCode.AUTH_DOCS_FIRST_NOTIFY_ADDRESS_2);
                ValidateUtil.validateLength(authenticatedDocs.getFirstNotifyAddress().getAddressLine2(), 1, 100,
                        ErrorCode.AUTH_DOCS_FIRST_NOTIFY_ADDRESS_2_LENGTH);
            }


            if (authenticatedDocs.getFirstNotifyAddress().getAddressLine3() != null
                    && authenticatedDocs.getFirstNotifyAddress().getAddressLine3().trim().length() != 0)
                ValidateUtil.validateLength(authenticatedDocs.getFirstNotifyAddress().getAddressLine3(), 1, 100,
                        ErrorCode.AUTH_DOCS_FIRST_NOTIFY_ADDRESS_3_LENGTH);

            if (authenticatedDocs.getFirstNotifyAddress().getAddressLine4() != null && authenticatedDocs.getFirstNotifyAddress().getAddressLine4().trim().length() != 0)
                ValidateUtil.validateLength(authenticatedDocs.getFirstNotifyAddress().getAddressLine4(), 1, 100,
                        ErrorCode.AUTH_DOCS_FIRST_NOTIFY_ADDRESS_4_LENGTH);

            if (authenticatedDocs.getFirstNotifyAddress().getEmail() != null) {
                ValidateUtil.validateLength(authenticatedDocs.getFirstNotifyAddress().getEmail(), 1, 500,
                        ErrorCode.AUTH_DOCS_FIRST_NOTIFY_EMAIL_LENGTH);

                for (String email : authenticatedDocs.getFirstNotifyAddress().getEmail().split(emailSplit)) {
                    if (!Pattern.compile(ValidateUtil.regularExp).matcher(email).matches()) {
                        throw new RestException(ErrorCode.AUTH_DOCS_FIRST_NOTIFY_EMAIL_LENGTH);
                    }
                }
            }
            authenticatedDocs.setFirstNotifyCode(tmpParty.getPartyCode());

        } else {
            authenticatedDocs.setFirstNotify(null);
            authenticatedDocs.setFirstNotifyCode(null);
            authenticatedDocs.setFirstNotifyAddress(null);
        }

    }

    private void verifyConsignee(AuthenticatedDocs authenticatedDocs, String emailSplit) {

        if (authenticatedDocs.getConsignee() != null && authenticatedDocs.getConsignee().getId() != null) {

            PartyMaster tmpParty = cacheRepository.get(SaaSUtil.getSaaSId(), PartyMaster.class.getName(),
                    authenticatedDocs.getConsignee().getId(), partyMasterRepository);

            ValidateUtil.notNull(tmpParty, ErrorCode.AUTH_DOCS_CONSIGNEE);
            ValidateUtil.notNull(tmpParty.getId(), ErrorCode.AUTH_DOCS_CONSIGNEE);
            ValidateUtil.notNullOrEmpty(tmpParty.getPartyCode(), ErrorCode.AUTH_DOCS_CONSIGNEE);
            ValidateUtil.isStatusBlocked(tmpParty.getStatus(), ErrorCode.AUTH_DOCS_CONSIGNEE_BLOCK);
            ValidateUtil.isStatusHidden(tmpParty.getStatus(), ErrorCode.AUTH_DOCS_CONSIGNEE_HIDE);
            ValidateUtil.isDefaulter(tmpParty, ErrorCode.SHIPMENT_DOC_CONSIGNEE_DEFAULTER);

            ValidateUtil.notNull(authenticatedDocs.getConsigneeAddress(),
                    ErrorCode.AUTH_DOCS_CONSIGNEE_ADDRESS);

            ValidateUtil.notNullOrEmpty(authenticatedDocs.getConsigneeAddress().getAddressLine1(),
                    ErrorCode.AUTH_DOCS_CONSIGNEE_ADDRESS_1);
            ValidateUtil.validateLength(authenticatedDocs.getConsigneeAddress().getAddressLine1(), 1, 100,
                    ErrorCode.AUTH_DOCS_CONSIGNEE_ADDRESS_1_LENGTH);

            if (authenticatedDocs.getConsigneeAddress().getAddressLine2() != null && authenticatedDocs.getConsigneeAddress().getAddressLine2().trim().length() != 0) {
                ValidateUtil.notNullOrEmpty(authenticatedDocs.getConsigneeAddress().getAddressLine2(),
                        ErrorCode.AUTH_DOCS_CONSIGNEE_ADDRESS_2);
                ValidateUtil.validateLength(authenticatedDocs.getConsigneeAddress().getAddressLine2(), 1, 100,
                        ErrorCode.AUTH_DOCS_CONSIGNEE_ADDRESS_2_LENGTH);
            }


            if (authenticatedDocs.getConsigneeAddress().getAddressLine3() != null && authenticatedDocs.getConsigneeAddress().getAddressLine3().trim().length() != 0)
                ValidateUtil.validateLength(authenticatedDocs.getConsigneeAddress().getAddressLine3(), 1, 100,
                        ErrorCode.AUTH_DOCS_CONSIGNEE_ADDRESS_3_LENGTH);

            if (authenticatedDocs.getConsigneeAddress().getAddressLine4() != null && authenticatedDocs.getConsigneeAddress().getAddressLine4().trim().length() != 0)
                ValidateUtil.validateLength(authenticatedDocs.getConsigneeAddress().getAddressLine4(), 1, 100,
                        ErrorCode.AUTH_DOCS_CONSIGNEE_ADDRESS_4_LENGTH);

            if (authenticatedDocs.getConsigneeAddress().getEmail() != null) {
                ValidateUtil.validateLength(authenticatedDocs.getConsigneeAddress().getEmail(), 1, 500,
                        ErrorCode.AUTH_DOCS_CONSIGNEE_EMAIL_LENGTH);

                for (String email : authenticatedDocs.getConsigneeAddress().getEmail().split(emailSplit)) {
                    if (!Pattern.compile(ValidateUtil.regularExp).matcher(email).matches()) {
                        throw new RestException(ErrorCode.AUTH_DOCS_CONSIGNEE_EMAIL_LENGTH);
                    }
                }
            }
            authenticatedDocs.setConsigneeCode(tmpParty.getPartyCode());

        } else {
            authenticatedDocs.setConsignee(null);
            authenticatedDocs.setConsigneeCode(null);
            authenticatedDocs.setConsigneeAddress(null);
        }

    }

    private void verifyForwarder(AuthenticatedDocs authenticatedDocs, String emailSplit) {

        if (authenticatedDocs.getForwarder() != null && authenticatedDocs.getForwarder().getId() != null) {

            PartyMaster tmpParty = cacheRepository.get(SaaSUtil.getSaaSId(), PartyMaster.class.getName(),
                    authenticatedDocs.getForwarder().getId(), partyMasterRepository);

            ValidateUtil.isDefaulter(tmpParty, ErrorCode.SHIPMENT_DOC_FORW_DEFAULTER);
            ValidateUtil.isStatusBlocked(tmpParty.getStatus(), ErrorCode.AUTH_DOCS_FORWARDER_BLOCK);
            ValidateUtil.isStatusHidden(tmpParty.getStatus(), ErrorCode.AUTH_DOCS_FORWARDER_HIDE);

            ValidateUtil.notNull(authenticatedDocs.getForwarderAddress(),
                    ErrorCode.AUTH_DOCS_FORWARDER_ADDRESS);
            ValidateUtil.notNullOrEmpty(authenticatedDocs.getForwarderAddress().getAddressLine1(),
                    ErrorCode.AUTH_DOCS_FORWARDER_ADDRESS_1);
            ValidateUtil.validateLength(authenticatedDocs.getForwarderAddress().getAddressLine1(), 1, 100,
                    ErrorCode.AUTH_DOCS_FORWARDER_ADDRESS_1_LENGTH);

            if (authenticatedDocs.getForwarderAddress().getAddressLine2() != null && authenticatedDocs.getForwarderAddress().getAddressLine2().trim().length() != 0) {
                ValidateUtil.notNullOrEmpty(authenticatedDocs.getForwarderAddress().getAddressLine2(),
                        ErrorCode.AUTH_DOCS_FORWARDER_ADDRESS_2);
                ValidateUtil.validateLength(authenticatedDocs.getForwarderAddress().getAddressLine2(), 1, 100,
                        ErrorCode.AUTH_DOCS_FORWARDER_ADDRESS_2_LENGTH);
            }


            if (authenticatedDocs.getForwarderAddress().getAddressLine3() != null
                    && authenticatedDocs.getForwarderAddress().getAddressLine3().trim().length() != 0)
                ValidateUtil.validateLength(authenticatedDocs.getForwarderAddress().getAddressLine3(), 1, 100,
                        ErrorCode.AUTH_DOCS_FORWARDER_ADDRESS_3_LENGTH);

            if (authenticatedDocs.getForwarderAddress().getAddressLine4() != null
                    && authenticatedDocs.getForwarderAddress().getAddressLine4().trim().length() != 0)

                ValidateUtil.validateLength(authenticatedDocs.getForwarderAddress().getAddressLine4(), 1, 100,
                        ErrorCode.AUTH_DOCS_FORWARDER_ADDRESS_4_LENGTH);

            if (authenticatedDocs.getForwarderAddress().getEmail() != null
                    && authenticatedDocs.getForwarderAddress().getEmail().trim().length() != 0) {
                ValidateUtil.validateLength(authenticatedDocs.getForwarderAddress().getEmail(), 1, 500,
                        ErrorCode.AUTH_DOCS_FORWARDER_EMAIL_LENGTH);
                for (String email : authenticatedDocs.getForwarderAddress().getEmail().split(emailSplit)) {
                    if (!Pattern.compile(ValidateUtil.regularExp).matcher(email).matches()) {
                        throw new RestException(ErrorCode.AUTH_DOCS_FORWARDER_EMAIL_LENGTH);
                    }
                }
            }
            authenticatedDocs.setForwarderCode(tmpParty.getPartyCode());
        } else {
            authenticatedDocs.setForwarderCode(null);
            authenticatedDocs.setForwarderAddress(null);
            authenticatedDocs.setForwarder(null);
        }
    }

    private void verifyShiper(AuthenticatedDocs authenticatedDocs, String emailSplit) {
        if (authenticatedDocs.getShipper() != null && authenticatedDocs.getShipper().getId() != null) {

            PartyMaster tmpParty = cacheRepository.get(SaaSUtil.getSaaSId(), PartyMaster.class.getName(),
                    authenticatedDocs.getShipper().getId(), partyMasterRepository);

            ValidateUtil.notNull(tmpParty, ErrorCode.AUTH_DOCS_SHIPPER);
            ValidateUtil.notNull(tmpParty.getId(), ErrorCode.AUTH_DOCS_SHIPPER);
            ValidateUtil.notNullOrEmpty(tmpParty.getPartyCode(), ErrorCode.AUTH_DOCS_SHIPPER);
            ValidateUtil.isStatusBlocked(tmpParty.getStatus(), ErrorCode.AUTH_DOCS_SHIPPER_BLOCK);
            ValidateUtil.isStatusHidden(tmpParty.getStatus(), ErrorCode.AUTH_DOCS_SHIPPER_HIDE);

            ValidateUtil.isDefaulter(tmpParty, ErrorCode.SHIPMENT_DOC_SHIPPER_DEFAULTER);

            ValidateUtil.notNull(authenticatedDocs.getShipperAddress(), ErrorCode.AUTH_DOCS_SHIPPER_ADDRESS);

            ValidateUtil.notNullOrEmpty(authenticatedDocs.getShipperAddress().getAddressLine1(),
                    ErrorCode.AUTH_DOCS_SHIPPER_ADDRESS_1);
            ValidateUtil.validateLength(authenticatedDocs.getShipperAddress().getAddressLine1(), 1, 100,
                    ErrorCode.AUTH_DOCS_SHIPPER_ADDRESS_1_LENGTH);

            if (authenticatedDocs.getShipperAddress().getAddressLine2() != null && authenticatedDocs.getShipperAddress().getAddressLine2().trim().length() != 0) {
                ValidateUtil.notNullOrEmpty(authenticatedDocs.getShipperAddress().getAddressLine2(),
                        ErrorCode.AUTH_DOCS_SHIPPER_ADDRESS_2);
                ValidateUtil.validateLength(authenticatedDocs.getShipperAddress().getAddressLine2(), 1, 100,
                        ErrorCode.AUTH_DOCS_SHIPPER_ADDRESS_2_LENGTH);
            }


            if (authenticatedDocs.getShipperAddress().getAddressLine3() != null
                    && authenticatedDocs.getShipperAddress().getAddressLine3().trim().length() != 0)
                ValidateUtil.validateLength(authenticatedDocs.getShipperAddress().getAddressLine3(), 1, 100,
                        ErrorCode.AUTH_DOCS_SHIPPER_ADDRESS_3_LENGTH);

            if (authenticatedDocs.getShipperAddress().getAddressLine4() != null
                    && authenticatedDocs.getShipperAddress().getAddressLine4().trim().length() != 0)
                ValidateUtil.validateLength(authenticatedDocs.getShipperAddress().getAddressLine4(), 1, 100,
                        ErrorCode.AUTH_DOCS_SHIPPER_ADDRESS_4_LENGTH);

            if (authenticatedDocs.getShipperAddress().getEmail() != null
                    && authenticatedDocs.getShipperAddress().getEmail().trim().length() != 0) {
                ValidateUtil.validateLength(authenticatedDocs.getShipperAddress().getEmail(), 1, 500,
                        ErrorCode.AUTH_DOCS_SHIPPER_EMAIL_LENGTH);

                for (String email : authenticatedDocs.getShipperAddress().getEmail().split(emailSplit)) {
                    if (!Pattern.compile(ValidateUtil.regularExp).matcher(email).matches()) {
                        throw new RestException(ErrorCode.AUTH_DOCS_SHIPPER_EMAIL_LENGTH);
                    }
                }
            }
            authenticatedDocs.setShipperCode(tmpParty.getPartyCode());
        } else {
            authenticatedDocs.setShipperCode(null);
            authenticatedDocs.setShipperAddress(null);
            authenticatedDocs.setShipper(null);
        }
    }

}
