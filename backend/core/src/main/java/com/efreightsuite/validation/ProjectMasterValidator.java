package com.efreightsuite.validation;

import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.model.ProjectMaster;
import com.efreightsuite.util.ValidateUtil;

import org.springframework.stereotype.Service;


@Service
public class ProjectMasterValidator {

    public void validate(ProjectMaster projectMaster) {
        ValidateUtil.notNullOrEmpty(projectMaster.getProjectCode(), ErrorCode.PROJECT_CODE_REQUIRED);
        ValidateUtil.validateLength(projectMaster.getProjectCode(), 0, 10, ErrorCode.PROJECT_CODE_INVALID);
        ValidateUtil.notNullOrEmpty(projectMaster.getProjectName(), ErrorCode.PROJECT_NAME_REQUIRED);
        ValidateUtil.validateLength(projectMaster.getProjectName(), 0, 100, ErrorCode.PROJECT_NAME_INVALID);
    }
}
