package com.efreightsuite.validation;

import com.efreightsuite.enumeration.LovStatus;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.model.TriggerMaster;
import com.efreightsuite.util.ValidateUtil;
import lombok.extern.log4j.Log4j2;

import org.springframework.stereotype.Service;

@Service
@Log4j2
public class TriggerMasterValidator {

    public void validate(TriggerMaster triggerMaster) {

        log.info("TriggerMasterValidator Validation Started....");

        ValidateUtil.notNullOrEmpty(triggerMaster.getTriggerName(), ErrorCode.TRIGGER_NAME_NOT_NULL);
        ValidateUtil.validateLength(triggerMaster.getTriggerName(), 1, 100, ErrorCode.TRIGGER_NAME_LENGTH_EXCEED);
        triggerMaster.setTriggerName(triggerMaster.getTriggerName());

        ValidateUtil.notNullOrEmpty(triggerMaster.getTriggerCode(), ErrorCode.TRIGGER_CODE_NOT_NULL);
        ValidateUtil.validateLength(triggerMaster.getTriggerCode(), 1, 10, ErrorCode.TRIGGER_CODE_LENGTH_EXCEED);
        triggerMaster.setTriggerCode(triggerMaster.getTriggerCode());

        if (triggerMaster.getStatus() == null) {
            triggerMaster.setStatus(LovStatus.Active);
        }

        ValidateTriggerType(triggerMaster);
    }

    private void ValidateTriggerType(TriggerMaster triggerMaster) {
        ValidateUtil.notNull(triggerMaster.getTriggerTypeMaster().getId(), ErrorCode.TRIGGER_ID_NOT_NULL);
        ValidateUtil.notNullOrEmpty(triggerMaster.getTriggerTypeMaster().getTriggerTypeName(),
                ErrorCode.TRIGGER_TYPE_NAME_NOT_NULL);
        ValidateUtil.isStatusBlocked(triggerMaster.getTriggerTypeMaster().getStatus(),
                ErrorCode.TRIGGER_TYPE_STATUS_BLOCK);
        ValidateUtil.isStatusBlocked(triggerMaster.getTriggerTypeMaster().getStatus(),
                ErrorCode.TRIGGER_TYPE_STATUS_HIDDEN);

    }
}
