package com.efreightsuite.validation;

import com.efreightsuite.enumeration.LovStatus;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.model.StateMaster;
import com.efreightsuite.util.ValidateUtil;
import lombok.extern.log4j.Log4j2;

import org.springframework.stereotype.Service;

@Service
@Log4j2
public class StateMasterValidator {

    public void validate(StateMaster stateMaster) {
        log.info("State Master Validation Started....[" + (stateMaster.getId() != null ? stateMaster.getId() : "") + "]");

        ValidateUtil.notNull(stateMaster, ErrorCode.STATE_NOT_NULL);

        ValidateUtil.notNull(stateMaster.getStateCode(), ErrorCode.STATE_CODE_NOT_NULL);
        ValidateUtil.validateLength(stateMaster.getStateCode(), 1, 10, ErrorCode.STATE_CODE_INVALID);
        stateMaster.setStateCode(stateMaster.getStateCode().toUpperCase().trim());


        ValidateUtil.notNull(stateMaster.getStateName(), ErrorCode.STATE_NAME_NOT_NULL);
        ValidateUtil.validateLength(stateMaster.getStateName(), 1, 100, ErrorCode.STATE_NAME_INVALID);


        ValidateUtil.notNull(stateMaster.getCountryMaster(), ErrorCode.STATE_COUNTRY_NOT_NULL);
        ValidateUtil.isStatusBlocked(stateMaster.getCountryMaster().getStatus(), ErrorCode.STATE_COUNTRY_INVALID);
        ValidateUtil.isStatusHidden(stateMaster.getCountryMaster().getStatus(), ErrorCode.STATE_COUNTRY_INVALID);


        stateMaster.setCountryCode(stateMaster.getCountryMaster().getCountryCode());

        ValidateUtil.notNull(stateMaster.getStatus(), ErrorCode.STATE_LOV_STATUS_NOT_NULL);

        ValidateUtil.validateEnum(LovStatus.class, stateMaster.getStatus(), ErrorCode.STATE_LOV_STATUS_INVALID);


        log.info("State Master Validation Finished....[" + (stateMaster.getId() != null ? stateMaster.getId() : "") + "]");
    }

}
