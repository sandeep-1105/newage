package com.efreightsuite.validation;

import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.model.CostCenterMaster;
import com.efreightsuite.service.RegularExpressionService;
import com.efreightsuite.util.RegExpName;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class CostCenterMasterValidator {

    @Autowired
    private
    RegularExpressionService regExpService;


    public void validate(CostCenterMaster costCenterMaster) {
        if (costCenterMaster != null) {

            regExpService.validate(costCenterMaster.getCostCenterName(), RegExpName.Reg_Exp_Master_COST_CENTER_NAME, ErrorCode.COST_CENTER_NAME_INVALID);
            regExpService.validate(costCenterMaster.getCostCenterCode(), RegExpName.Reg_Exp_Master_COST_CENTER_CODE, ErrorCode.COST_CENTER_CODE_INVALID);

            // TODO
            log.info("CostCenterMaster Validation Started....["
                    + (costCenterMaster.getId() != null ? costCenterMaster.getId() : "") + "]");


            log.info("CostCenterMaster Validation Finished....["
                    + (costCenterMaster.getId() != null ? costCenterMaster.getId() : "") + "]");


        }

    }

}
