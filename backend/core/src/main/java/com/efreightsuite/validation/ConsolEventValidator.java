package com.efreightsuite.validation;

import com.efreightsuite.enumeration.YesNo;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.model.ConsolEvent;
import com.efreightsuite.model.EmployeeMaster;
import com.efreightsuite.model.EventMaster;
import com.efreightsuite.repository.EmployeeMasterRepository;
import com.efreightsuite.repository.EventMasterRepository;
import com.efreightsuite.util.ValidateUtil;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class ConsolEventValidator {

    @Autowired
    private EventMasterRepository eventMasterRepository;

    @Autowired
    private EmployeeMasterRepository employeeMasterRepository;

    public void validate(ConsolEvent event) {

        verifyEvent(event);

        verifyDate(event);

        verifyAssignedTo(event);

    }


    private void verifyEvent(ConsolEvent event) {

        ValidateUtil.notNull(event.getEventMaster(), ErrorCode.CONSOL_EVENT);
        ValidateUtil.notNull(event.getEventMaster().getId(), ErrorCode.CONSOL_EVENT);
        EventMaster eventMaster = eventMasterRepository.getOne(event.getEventMaster().getId());

        ValidateUtil.notNullOrEmpty(eventMaster.getEventCode(), ErrorCode.CONSOL_EVENT);
        ValidateUtil.isStatusBlocked(eventMaster.getStatus(), ErrorCode.CONSOL_EVENT_BLOCK);
        ValidateUtil.isStatusHidden(eventMaster.getStatus(), ErrorCode.CONSOL_EVENT_HIDE);
        event.setEventCode(eventMaster.getEventCode());
    }

    private void verifyDate(ConsolEvent event) {

        ValidateUtil.notNull(event.getEventDate(), ErrorCode.CONSOL_EVENT_DATE);
    }

    private void verifyAssignedTo(ConsolEvent event) {

        if (event.getFollowUpRequired().equals(YesNo.Yes)) {

            ValidateUtil.notNull(event.getAssignedTo(), ErrorCode.CONSOL_EVENT_ASSINGED);
            ValidateUtil.notNull(event.getAssignedTo().getId(), ErrorCode.CONSOL_EVENT_ASSINGED);

            EmployeeMaster assinedTo = employeeMasterRepository.findById(event.getAssignedTo().getId());

            ValidateUtil.notNullOrEmpty(assinedTo.getEmployeeCode(), ErrorCode.CONSOL_EVENT_ASSINGED);
            ValidateUtil.isEmployeeResigned(assinedTo.getEmployementStatus(), ErrorCode.CONSOL_EVENT_ASSINGED_BLOCK);
            ValidateUtil.isEmployeeTerminated(assinedTo.getEmployementStatus(), ErrorCode.CONSOL_EVENT_ASSINGED_HIDE);

            ValidateUtil.notNull(event.getFollowUpDate(), ErrorCode.CONSOL_EVENT_FOLLOW_DATE);
            event.setAssignedCode(assinedTo.getEmployeeCode());
        } else {
            event.setAssignedTo(null);
            event.setFollowUpDate(null);
            event.setAssignedCode(null);
        }

    }

}
