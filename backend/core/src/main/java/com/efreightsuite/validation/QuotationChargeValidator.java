package com.efreightsuite.validation;

import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.model.ChargeMaster;
import com.efreightsuite.model.CurrencyMaster;
import com.efreightsuite.model.QuotationCharge;
import com.efreightsuite.model.UnitMaster;
import com.efreightsuite.repository.ChargeMasterRepository;
import com.efreightsuite.repository.CurrencyMasterRepository;
import com.efreightsuite.repository.UnitMasterRepository;
import com.efreightsuite.util.ValidateUtil;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class QuotationChargeValidator {

    @Autowired
    private ChargeMasterRepository chargeMasterRepository;

    @Autowired
    private CurrencyMasterRepository currencyMasterRepository;

    @Autowired
    private UnitMasterRepository unitMasterRepository;



    public void validate(QuotationCharge quotationCharge) {

        log.info("QuotationCharge Validation Started......");
        /* EFS_QT_AIR_249 */
        verifyChargeCode(quotationCharge);

		/* EFS_QT_AIR_256 */
        verifyChargeName(quotationCharge);

		/* EFS_QT_AIR_264 */
        verifyCurrencyCode(quotationCharge);

		/* EFS_QT_AIR_270 */
        verifyUnitCode(quotationCharge);

		/* EFS_QT_AIR_284 */
	/*	verifyMinMaxUnit(quotationCharge);*/

        verifyBuyRate(quotationCharge);

        verifyBuyRateMinCost(quotationCharge);

        verifySellRateMinCost(quotationCharge);

        verifySellRate(quotationCharge);

        log.info("QuotationCharge Validation Finished......");

    }

    private void verifyChargeCode(QuotationCharge quotationCharge) {

        ValidateUtil.notNull(quotationCharge.getChargeMaster(), ErrorCode.QUOTATION_CHARGE_REQUIRED);
        ValidateUtil.notNull(quotationCharge.getChargeMaster().getId(), ErrorCode.QUOTATION_CHARGE_REQUIRED);

        ChargeMaster charge = chargeMasterRepository.findById(quotationCharge.getChargeMaster().getId());

        ValidateUtil.isStatusBlocked(charge.getStatus(), ErrorCode.QUOTATION_CHARGE_BLOCKED);

        ValidateUtil.isStatusHidden(charge.getStatus(), ErrorCode.QUOTATION_CHARGE_HIDDEN);
        quotationCharge.setChargeCode(charge.getChargeCode());
        log.info("Quotation Charge is validated..................[" + charge.getChargeCode() + "]");

    }

    private void verifyChargeName(QuotationCharge quotationCharge) {

        ValidateUtil.notNull(quotationCharge.getChargeName(), ErrorCode.QUOTATION_CHARGE_NAME_REQUIRED);

		/*ValidateUtil.checkPattern(quotationCharge.getChargeName(), "quotation.chargeName", true);*/

        log.info("Quotation Charge Name is validated.....[" + quotationCharge.getChargeName() + "]");
    }

    private void verifyCurrencyCode(QuotationCharge quotationCharge) {

        ValidateUtil.notNull(quotationCharge.getCurrencyMaster(), ErrorCode.QUOTATION_CURRENCY_REQUIRED);

        ValidateUtil.notNull(quotationCharge.getCurrencyMaster().getId(), ErrorCode.QUOTATION_CURRENCY_REQUIRED);

        CurrencyMaster currency = currencyMasterRepository.getOne(quotationCharge.getCurrencyMaster().getId());

        ValidateUtil.isStatusBlocked(currency.getStatus(),
                ErrorCode.QUOTATION_CURRENCY_BLOCKED);

        ValidateUtil.isStatusHidden(currency.getStatus(),
                ErrorCode.QUOTATION_CURRENCY_HIDDEN);

        quotationCharge.setCurrencyCode(currency.getCurrencyCode());

        log.info("Quotation Currency is validated..................[" + currency.getCurrencyCode() + "]");

    }

    private void verifyUnitCode(QuotationCharge quotationCharge) {

        ValidateUtil.notNull(quotationCharge.getUnitMaster(), ErrorCode.QUOTATION_UNIT_REQUIRED);

        ValidateUtil.notNull(quotationCharge.getUnitMaster().getId(), ErrorCode.QUOTATION_UNIT_REQUIRED);

        UnitMaster unit = unitMasterRepository.getOne(quotationCharge.getUnitMaster().getId());

        ValidateUtil.isStatusBlocked(unit.getStatus(), ErrorCode.QUOTATION_UNIT_BLOCKED);

        ValidateUtil.isStatusHidden(unit.getStatus(), ErrorCode.QUOTATION_UNIT_HIDDEN);

        quotationCharge.setUnitCode(unit.getUnitCode());

        log.info("Quotation Unit is validated..................[" + unit.getUnitCode() + "]");

    }

	/*private void verifyMinMaxUnit(QuotationCharge quotationCharge) {

		if (quotationCharge.getChargeMaster() != null || quotationCharge.getChargeMaster().getId() !=null) {

			if (quotationCharge.getNoOfMinUnit() != null) {
				ValidateUtil.notNull(quotationCharge.getNoOfMaxUnit(), ErrorCode.QUOTATION_CHARGE_MAX_VALUE_REQUIRED);
			}

			if (quotationCharge.getNoOfMaxUnit() != null) {
				ValidateUtil.notNull(quotationCharge.getNoOfMinUnit(), ErrorCode.QUOTATION_CHARGE_MIN_VALUE_REQUIRED);
			}

			if (quotationCharge.getNoOfMinUnit() != null && quotationCharge.getNoOfMaxUnit() != null) {

				ValidateUtil.nonZero(quotationCharge.getNoOfMinUnit(), ErrorCode.QUOTATION_CHARGE_MIN_VALUE_ZERO);

				ValidateUtil.nonZero(quotationCharge.getNoOfMaxUnit(), ErrorCode.QUOTATION_CHARGE_MAX_VALUE_ZERO);

				ValidateUtil.belowRange(quotationCharge.getNoOfMinUnit(), 0,
						ErrorCode.QUOTATION_CHARGE_MIN_VALUE_NEGATIVE);

				ValidateUtil.belowRange(quotationCharge.getNoOfMaxUnit(), 0,
						ErrorCode.QUOTATION_CHARGE_MAX_VALUE_NEGATIVE);
				
				if (quotationCharge.getNoOfMaxUnit() < quotationCharge.getNoOfMinUnit()) {
					throw new RestException(ErrorCode.QUOTATION_CHARGE_MIN_MAX_MISMATCH);
				}


			}

			log.info("Min Max value validated.....................MIN ["+quotationCharge.getNoOfMinUnit()+"]. MAX [" + quotationCharge.getNoOfMaxUnit() +"]");
		}
		log.info("Min & Max Value not given....");
	}*/


    private void verifyBuyRate(QuotationCharge quotationCharge) {


        if (quotationCharge.getChargeMaster() != null || quotationCharge.getChargeMaster().getId() != null) {
            if (quotationCharge.getBuyRateCostPerUnit() != null) {
                if (quotationCharge.getBuyRateCostPerUnit() < 0 || quotationCharge.getBuyRateCostPerUnit() > 99999999999.999) {
                    throw new RestException(ErrorCode.QUOTATION_BUY_RATE_COST_PER_UNIT_INVALID);
                }
            }


        }

    }

    private void verifyBuyRateMinCost(QuotationCharge quotationCharge) {


        if (quotationCharge.getChargeMaster() != null || quotationCharge.getChargeMaster().getId() != null) {

            if (quotationCharge.getBuyRateMinCost() != null) {
                if (quotationCharge.getBuyRateMinCost() < 0 || quotationCharge.getBuyRateMinCost() >= 99999999999.999) {
                    throw new RestException(ErrorCode.QUOTATION_BUY_RATE_MINIMUM_COST_INVALID);
                }
            }


        }

    }


    private void verifySellRateMinCost(QuotationCharge quotationCharge) {


        if (quotationCharge.getChargeMaster() != null || quotationCharge.getChargeMaster().getId() != null) {
            if (quotationCharge.getSellRateMinAmount() != null) {
                if (quotationCharge.getSellRateMinAmount() < 0 || quotationCharge.getSellRateMinAmount() >= 99999999999.999) {
                    throw new RestException(ErrorCode.QUOTATION_SELL_RATE_MINIMUM_AMOUNT_INVALID);
                }
            }


        }

    }

    private void verifySellRate(QuotationCharge quotationCharge) {


        if (quotationCharge.getChargeMaster() != null || quotationCharge.getChargeMaster().getId() != null) {
            if (quotationCharge.getSellRateAmountPerUnit() != null) {
                if (quotationCharge.getSellRateAmountPerUnit() < 0 || quotationCharge.getSellRateAmountPerUnit() > 99999999999.999) {
                    throw new RestException(ErrorCode.QUOTATION_SELL_RATE_AMOUNT_PER_UNIT_INVALID);
                }
            }
        }

    }


}
