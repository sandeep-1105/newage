package com.efreightsuite.validation;

import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.model.EmailTemplate;
import com.efreightsuite.util.ValidateUtil;
import lombok.extern.log4j.Log4j2;

import org.springframework.stereotype.Service;

@Service
@Log4j2
public class EmailTemplateValidator {

    public void validate(EmailTemplate emailTemplate) {

        log.info("EmailTemplateValidator Started for E-mail Template : [" + emailTemplate.getId() + "]");


        ValidateUtil.notNull(emailTemplate.getTemplateType(), ErrorCode.EMAIL_TEMPALTE_EVENT_TYPE_REQUIRED);
        ValidateUtil.notNullOrEmpty(emailTemplate.getSubject(), ErrorCode.EMAIL_TEMPALTE_SUBJECT_REQUIRED);
        ValidateUtil.notNullOrEmpty(emailTemplate.getFromEmailId(), ErrorCode.EMAIL_TEMPALTE_SENDER_EMAIL_REQUIRED);
        ValidateUtil.notNullOrEmpty(emailTemplate.getFromEmailPassword(), ErrorCode.EMAIL_TEMPALTE_SENDER_PASSWORD_REQUIRED);
        ValidateUtil.notNullOrEmpty(emailTemplate.getEmailType(), ErrorCode.EMAIL_TEMPALTE_TYPE_REQUIRED);
        ValidateUtil.notNullOrEmpty(emailTemplate.getSmtpServerAddress(), ErrorCode.EMAIL_TEMPALTE_SMTP_SERVER_REQUIRED);
        ValidateUtil.notNull(emailTemplate.getPort(), ErrorCode.EMAIL_TEMPALTE_SMTP_PORT_REQUIRED);


        log.info("EmailTemplateValidator Finished for E-mail Template : [" + emailTemplate.getId() + "]");
    }

}
