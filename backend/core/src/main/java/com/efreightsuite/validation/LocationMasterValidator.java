package com.efreightsuite.validation;

import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.model.LocationMaster;
import com.efreightsuite.service.RegularExpressionService;
import com.efreightsuite.util.RegExpName;
import com.efreightsuite.util.ValidateUtil;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class LocationMasterValidator {

    @Autowired
    private
    RegularExpressionService regExpService;

    @Autowired
    CountryMasterValidator countryMasterValidator;

    @Autowired
    CityMasterValidator cityMasterValidator;

    @Autowired
    ZoneMasterValidator zoneMasterValidator;

    @Autowired
    CompanyMasterValidator companyMasterValidator;

    @Autowired
    PartyMasterValidator partyMasterValidator;

    @Autowired
    CurrencyMasterValidator currencyMasterValidator;

    @Autowired
    CostCenterMasterValidator costCenterMasterValidator;


    public void validate(LocationMaster locationMaster) {

        log.info("LocationMaster Validation Started....["
                + (locationMaster.getId() != null ? locationMaster.getId() : "") + "]");

        // Validating Location code
        ValidateUtil.notNull(locationMaster.getLocationCode(), ErrorCode.LOCATION_CODE_NOT_NULL);
        regExpService.validate(locationMaster.getLocationCode(), RegExpName.Reg_Exp_Master_Location_Code, ErrorCode.LOCATION_CODE_INVALID);
        locationMaster.setLocationCode(locationMaster.getLocationCode().trim().toUpperCase());
        log.debug("Location Code Validated...");


        // Validating Location Name
        ValidateUtil.notNull(locationMaster.getLocationName(), ErrorCode.LOCATION_NAME_NOT_NULL);
        regExpService.validate(locationMaster.getLocationName(), RegExpName.Reg_Exp_Master_Location_Name, ErrorCode.LOCATION_NAME_INVALID);
        locationMaster.setLocationName(locationMaster.getLocationName().trim());
        log.debug("Location Name Validated...");

        //Validating branch name
        ValidateUtil.notNull(locationMaster.getBranchName(), ErrorCode.LOCATION_BRANCH_NAME);

        ValidateUtil.notNull(locationMaster.getCountryMaster(), ErrorCode.LOCATION_COUNTRY__NOT_NULL);
        ValidateUtil.isStatusBlocked(locationMaster.getCountryMaster().getStatus(), ErrorCode.LOCATION_COUNTRY_BLOCKED);
        ValidateUtil.isStatusHidden(locationMaster.getCountryMaster().getStatus(), ErrorCode.LOCATION_COUNTRY_HIDDEN);

        ValidateUtil.notNull(locationMaster.getCityMaster(), ErrorCode.LOCATION_CITY__NOT_NULL);
        ValidateUtil.isStatusBlocked(locationMaster.getCityMaster().getStatus(), ErrorCode.LOCATION_CITY_BLOCKED);
        ValidateUtil.isStatusHidden(locationMaster.getCityMaster().getStatus(), ErrorCode.LOCATION_CITY_HIDDEN);

        //ValidateUtil.notNull(locationMaster.getZoneMaster(),ErrorCode.LOCATION_ZONE__NOT_NULL);
        if (locationMaster.getZoneMaster() != null) {
            ValidateUtil.isStatusBlocked(locationMaster.getZoneMaster().getStatus(), ErrorCode.LOCATION_ZONE_BLOCKED);
            ValidateUtil.isStatusHidden(locationMaster.getZoneMaster().getStatus(), ErrorCode.LOCATION_ZONE_HIDDEN);
        }
        ValidateUtil.notNull(locationMaster.getCompanyMaster(), ErrorCode.LOCATION_COMPANY__NOT_NULL);
        ValidateUtil.isStatusBlocked(locationMaster.getCompanyMaster().getStatus(), ErrorCode.LOCATION_COMPANY_BLOCKED);
        ValidateUtil.isStatusHidden(locationMaster.getCompanyMaster().getStatus(), ErrorCode.LOCATION_COMPANY_HIDDEN);

        if (locationMaster.getPartyMaster() != null) {
            ValidateUtil.isStatusBlocked(locationMaster.getPartyMaster().getStatus(), ErrorCode.LOCATION_PARTY_BLOCKED);
            ValidateUtil.isStatusHidden(locationMaster.getPartyMaster().getStatus(), ErrorCode.LOCATION_PARTY_HIDDEN);
        }
        if (locationMaster.getRegionMaster() != null) {
            ValidateUtil.isStatusBlocked(locationMaster.getRegionMaster().getStatus(), ErrorCode.LOCATION_REGION_BLOCKED);
            ValidateUtil.isStatusHidden(locationMaster.getRegionMaster().getStatus(), ErrorCode.LOCATION_REGION_HIDDEN);
        }
        if (locationMaster.getDivisionMaster() != null) {
            ValidateUtil.isStatusBlocked(locationMaster.getDivisionMaster().getStatus(), ErrorCode.LOCATION_DIVISION_BLOCKED);
            ValidateUtil.isStatusHidden(locationMaster.getDivisionMaster().getStatus(), ErrorCode.LOCATION_DIVISION_HIDDEN);
        }

        ValidateUtil.notNull(locationMaster.getDateFormat(), ErrorCode.LOCATION_DATE_FORMAT);
        //ValidateUtil.notNull(locationMaster.getDateTimeFormat(),ErrorCode.LOCATION_DATE_TIME_FORMAT);


        ValidateUtil.notNull(locationMaster.getCurrencyMaster(), ErrorCode.LOCATION_CURRENCY_NOT_NULL);
        ValidateUtil.isStatusBlocked(locationMaster.getCurrencyMaster().getStatus(), ErrorCode.LOCATION_CURRENCY_BLOCKED);
        ValidateUtil.isStatusHidden(locationMaster.getCurrencyMaster().getStatus(), ErrorCode.LOCATION_CURRENCY_HIDDEN);


        log.info("LocationMaster Validation Finished....["
                + (locationMaster.getId() != null ? locationMaster.getId() : "") + "]");
    }

}
