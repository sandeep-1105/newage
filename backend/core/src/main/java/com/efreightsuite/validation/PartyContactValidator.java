package com.efreightsuite.validation;

import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.model.PartyContact;
import com.efreightsuite.model.PartyContactRelation;
import com.efreightsuite.repository.DefaultMasterDataRepository;
import com.efreightsuite.service.RegularExpressionService;
import com.efreightsuite.util.RegExpName;
import com.efreightsuite.util.ValidateUtil;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PartyContactValidator {


    @Autowired
    DefaultMasterDataRepository defaultMasterDataRepository;

    @Autowired
    private
    RegularExpressionService regExpService;

    public void validate(PartyContact partyContact) {

        ValidateUtil.notNullOrEmpty(partyContact.getFirstName(), ErrorCode.PARTY_CONTACT_FIRST_NAME_REQUIRED);

        ValidateUtil.validateLength(partyContact.getFirstName(), 0, 100, ErrorCode.PARTY_CONTACT_FIRST_NAME_INVALID);

        ValidateUtil.notNullOrEmpty(partyContact.getLastName(), ErrorCode.PARTY_CONTACT_LAST_NAME_REQUIRED);

        ValidateUtil.validateLength(partyContact.getLastName(), 0, 100, ErrorCode.PARTY_CONTACT_LAST_NAME_INVALID);

        // Official Info

        ValidateUtil.validateLength(partyContact.getDesignation(), 0, 100, ErrorCode.PARTY_CONTACT_OFFICIAL_DESIGNATION_INVALID);

        ValidateUtil.validateLength(partyContact.getDepartment(), 0, 100, ErrorCode.PARTY_CONTACT_OFFICIAL_DEPARTMENT_INVALID);

        ValidateUtil.validateLength(partyContact.getOfficalAddress(), 0, 4000, ErrorCode.PARTY_CONTACT_OFFICIAL_ADDRESS_INVALID);

        if (StringUtils.isNotEmpty(partyContact.getOfficalPhone())) {
            regExpService.validate(partyContact.getOfficalPhone(), RegExpName.REG_EXP_PHONE_NUMBER, ErrorCode.PARTY_CONTACT_OFFICIAL_PHONE_NO_INVALID);
            ValidateUtil.validateLength(partyContact.getOfficalPhone(), 0, 100, ErrorCode.PARTY_CONTACT_OFFICIAL_PHONE_NO_INVALID);
        } else {
            partyContact.setOfficalPhone(null);
        }


        if (StringUtils.isNotEmpty(partyContact.getOfficalMobile())) {
            regExpService.validate(partyContact.getOfficalMobile(), RegExpName.REG_EXP_MOBILE_NUMBER, ErrorCode.PARTY_CONTACT_OFFICIAL_MOBILE_NO_INVALID);
            ValidateUtil.validateLength(partyContact.getOfficalMobile(), 0, 100, ErrorCode.PARTY_CONTACT_OFFICIAL_MOBILE_NO_INVALID);
        } else {
            partyContact.setOfficalMobile(null);
        }


        ValidateUtil.validateLength(partyContact.getOfficalFax(), 0, 100, ErrorCode.PARTY_CONTACT_OFFICIAL_FAX_INVALID);

        if (StringUtils.isNotEmpty(partyContact.getOfficalEmail())) {
            ValidateUtil.validateLength(partyContact.getOfficalEmail(), 0, 500, ErrorCode.PARTY_CONTACT_OFFICIAL_EMAIL_INVALID);
            verifyOfficalEmail(partyContact.getOfficalEmail());
        } else {
            partyContact.setOfficalEmail(null);
        }


        ValidateUtil.validateLength(partyContact.getAssistantName(), 0, 100, ErrorCode.PARTY_CONTACT_OFFICIAL_ASSISTENT_NAME_INVALID);

        if (StringUtils.isNotEmpty(partyContact.getAssistantPhone())) {
            ValidateUtil.validateLength(partyContact.getAssistantPhone(), 0, 100, ErrorCode.PARTY_CONTACT_OFFICIAL_ASSISTENT_PHONE_INVALID);
            regExpService.validate(partyContact.getAssistantPhone(), RegExpName.REG_EXP_PHONE_NUMBER, ErrorCode.PARTY_CONTACT_OFFICIAL_ASSISTENT_PHONE_INVALID);
        } else {
            partyContact.setAssistantPhone(null);
        }

        ValidateUtil.validateLength(partyContact.getPreferredCallTime(), 0, 100, ErrorCode.PARTY_CONTACT_OFFICIAL_PREFERRED_CALL_TIME_INVALID);

        ValidateUtil.validateLength(partyContact.getReportingTo(), 0, 100, ErrorCode.PARTY_CONTACT_OFFICIAL_REPORTING_TO_INVALID);


        // Personal Info

        ValidateUtil.validateLength(partyContact.getPersonalAddress(), 0, 4000, ErrorCode.PARTY_CONTACT_PERSONAL_ADDRESS_INVALID);

        if (StringUtils.isNotEmpty(partyContact.getPersonalPhone())) {
            ValidateUtil.validateLength(partyContact.getPersonalPhone(), 0, 100, ErrorCode.PARTY_CONTACT_PERSONAL_PHONE_NO_INVALID);
            regExpService.validate(partyContact.getPersonalPhone(), RegExpName.REG_EXP_PHONE_NUMBER, ErrorCode.PARTY_CONTACT_PERSONAL_PHONE_NO_INVALID);
        } else {
            partyContact.setPersonalPhone(null);
        }


        if (StringUtils.isNotEmpty(partyContact.getPersonalMobile())) {
            ValidateUtil.validateLength(partyContact.getPersonalMobile(), 0, 100, ErrorCode.PARTY_CONTACT_PERSONAL_MOBILE_NO_INVALID);
            regExpService.validate(partyContact.getPersonalMobile(), RegExpName.REG_EXP_MOBILE_NUMBER, ErrorCode.PARTY_CONTACT_PERSONAL_MOBILE_NO_INVALID);
        } else {
            partyContact.setPersonalMobile(null);
        }


        ValidateUtil.validateLength(partyContact.getPersonalFax(), 0, 100, ErrorCode.PARTY_CONTACT_PERSONAL_FAX_INVALID);


        if (StringUtils.isNotEmpty(partyContact.getPersonalEmail())) {
            ValidateUtil.validateLength(partyContact.getPersonalEmail(), 0, 500, ErrorCode.PARTY_CONTACT_PERSONAL_EMAIL_INVALID);
            verifyPersonalEmail(partyContact.getPersonalEmail());
        } else {
            partyContact.setPersonalEmail(null);
        }


        ValidateUtil.validateLength(partyContact.getFormerCompany(), 0, 100, ErrorCode.PARTY_CONTACT_FORMER_COMPANY_INVALID);

        ValidateUtil.validateLength(partyContact.getQualification(), 0, 100, ErrorCode.PARTY_CONTACT_PERSONAL_QUALIFICATION_INVALID);

        if (StringUtils.isNotEmpty(partyContact.getNoOfChildren())) {
            ValidateUtil.validateLength(partyContact.getNoOfChildren(), 0, 5, ErrorCode.PARTY_CONTACT_PERSONAL_CHILDREN_INVALID);
            regExpService.validate(partyContact.getNoOfChildren(), RegExpName.Reg_Exp_Master_PARTY_CONTACT_PERSONAl_CHILDREN, ErrorCode.PARTY_CONTACT_PERSONAL_CHILDREN_INVALID);
        } else {
            partyContact.setNoOfChildren(null);
        }


        // Remarks tab
        ValidateUtil.validateLength(partyContact.getNote(), 0, 4000, ErrorCode.PARTY_CONTACT_REMARKS_NOTES_INVALID);

        // Party relation table
        if (CollectionUtils.isNotEmpty(partyContact.getRelationList())) {


            for (PartyContactRelation relation : partyContact.getRelationList()) {

                ValidateUtil.validateLength(relation.getFirstName(), 0, 100, ErrorCode.PARTY_CONTACT_RELATION_FIRST_NAME_INVALID);

                ValidateUtil.validateLength(relation.getLastName(), 0, 100, ErrorCode.PARTY_CONTACT_RELATION_LAST_NAME_INVALID);

                ValidateUtil.validateLength(relation.getNote(), 0, 400, ErrorCode.PARTY_CONTACT_RELATION_NOTES_INVALID);

            }

        }


    }


    private void verifyPersonalEmail(String emailAddress) {
        /*for (String email: emailAddress.split(defaultMasterDataRepository.findByCode("single.text.split.by.multiple.email").getValue())){
			regExpService.validate(email, RegExpName.REG_EXP_MULTIPLE_EMAIL, ErrorCode.PARTY_CONTCT_PERSONAL_EMAIL);
			//ValidateUtil.checkPattern(email, "party.contact.personal.email", true);
	      }*/
    }

    private void verifyOfficalEmail(String emailAddress) {
		/*for (String email: emailAddress.split(defaultMasterDataRepository.findByCode("single.text.split.by.multiple.email").getValue())){
			regExpService.validate(email, RegExpName.REG_EXP_MULTIPLE_EMAIL, ErrorCode.PARTY_CONTCT_OFFICE_EMAIL);
			//ValidateUtil.checkPattern(email, "party.contact.offical.email", true);
	      }*/
    }

}
