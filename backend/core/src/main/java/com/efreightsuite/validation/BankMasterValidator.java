package com.efreightsuite.validation;

import com.efreightsuite.enumeration.LovStatus;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.model.BankMaster;
import com.efreightsuite.service.RegularExpressionService;
import com.efreightsuite.util.RegExpName;
import com.efreightsuite.util.ValidateUtil;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class BankMasterValidator {


    @Autowired
    private
    RegularExpressionService regExpBANK;


    public void validate(BankMaster bm) {

        log.info("Bank  validation started");
        validateBankCode(bm);
        validateBankName(bm);
        validateSmartBankCode(bm);
        validateBankStatus(bm);
        log.info("Bank  validation ended");

    }

    private void validateBankStatus(BankMaster bm) {
        if (bm.getId() != null) {
            ValidateUtil.notNull(bm.getStatus(), ErrorCode.BANK_STATUS_NOT_NULL);
        } else {
            bm.setStatus(LovStatus.Active);
        }
    }

    private void validateSmartBankCode(BankMaster bm) {
        if (bm.getSmartBusinessBankCode() != null) {
            regExpBANK.validate(bm.getSmartBusinessBankCode(), RegExpName.Reg_Exp_Master_Bank_Smart_Bank_Code, ErrorCode.BANK_SMART_BANK_CODE_INVALID);
        }
    }

    private void validateBankName(BankMaster bm) {

        ValidateUtil.notNull(bm.getBankName(), ErrorCode.BANK_NAME_NOT_NULL);

        regExpBANK.validate(bm.getBankName(), RegExpName.Reg_Exp_Master_Bank_Code, ErrorCode.BANK_NAME_INVALID);


    }

    private void validateBankCode(BankMaster bm) {

        ValidateUtil.notNull(bm.getBankCode(), ErrorCode.BANK_CODE_NOT_NULL);
        regExpBANK.validate(bm.getBankCode(), RegExpName.Reg_Exp_Master_Bank_Code, ErrorCode.BANK_CODE_INVALID);
        bm.setBankCode(bm.getBankCode().toUpperCase());

    }


}
