package com.efreightsuite.validation;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;
import java.util.regex.Pattern;

import com.efreightsuite.common.component.CacheRepository;
import com.efreightsuite.enumeration.BooleanType;
import com.efreightsuite.enumeration.DocPrefixDocumentType;
import com.efreightsuite.enumeration.ImportExport;
import com.efreightsuite.enumeration.RateClass;
import com.efreightsuite.enumeration.SequenceType;
import com.efreightsuite.enumeration.YesNo;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.model.*;
import com.efreightsuite.repository.*;
import com.efreightsuite.service.SequenceGeneratorService;
import com.efreightsuite.util.AppUtil;
import com.efreightsuite.util.SaaSUtil;
import com.efreightsuite.util.TimeUtil;
import com.efreightsuite.util.ValidateUtil;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author user
 */
@Service
public class DocumentValidator {

    @Autowired
    private
    AppUtil appUtil;

    @Autowired
    private
    CacheRepository cacheRepository;

    @Autowired
    private
    PartyMasterRepository partyMasterRepository;

    @Autowired
    DefaultMasterDataRepository defaultMasterDataRepository;

    @Autowired
    private
    SequenceGeneratorService sequenceGeneratorService;

    @Autowired
    private
    DocumentPrefixMasterRepository documentPrefixMasterRepository;

    @Autowired
    private
    SequenceGeneratorRepository sequenceGeneratorRepository;

    @Autowired
    private
    DynamicFieldRepository dynamicFieldRepository;

    @Autowired
    private
    PackMasterRepository packMasterRepository;

    @Autowired
    private
    PortMasterRepository portMasterRepository;

    @Autowired
    private
    ServiceMasterRepository serviceMasterRepository;

    @Autowired
    private
    LocationMasterRepository locationMasterRepository;

    @Autowired
    private
    CarrierMasterRepository carrierMasterRepository;

    @Autowired
    private
    CityMasterRepository cityMasterRepository;

    @Autowired
    private
    StateMasterRepository stateMasterRepository;

    @Autowired
    private
    CountryMasterRepository countryMasterRepository;

    public void validate(DocumentDetail document, ShipmentServiceDetail service, Shipment sp) {

        String emailSplit = appUtil.getLocationConfig("single.text.split.by.multiple.email", service.getLocation(), false);

        verifyBLReceived(document);


        verifyMarksAndNo(document);

        verifyCommodityDescription(document);

        verifyAccoutingInformation(document);

        verifyHandlingInformation(document);

        verifyWeight(document, service);

        verifyNoOfPieces(document, service);

        verifyPack(document);

        setAgreed(document, service);

        changeDateAndTime(document, service);

        if (sp != null && sp.getIsFromConsolForImport() != YesNo.Yes) {

            verifyPod(document, service);

            verifyPol(document, service);

            verifyDestination(document, service);

            verifyOrigin(document, service);

            verifyIssuingAgent(document, emailSplit);

            verifyCarrier(document);
        } else {

            document.setIssuingAgent(null);
            document.setIssuingAgentAddress(null);
            document.setIssuingAgentCode(null);
        }

        verifyShiper(document, emailSplit);

        verifyConsignee(document, emailSplit);

        verifyForwarder(document, emailSplit);

        verifyFirstNotify(document, emailSplit);

        verifySecondNotify(document, emailSplit);

        verifyAgent(document, emailSplit);

        verifyChaAgent(document, emailSplit);


        verifyDate(document);

        setSequenceNo(document);

        setHawbNo(document, service);

        if (service.getServiceMaster().getImportExport() != ImportExport.Import) {
            verifyRateClass(document, service);
        } else {
            document.setRateClass(null);
        }

        if (document.getLocation() != null) {
            document.setLocationCode(document.getLocation().getLocationCode());
        }

        if (document.getCompany() != null) {
            document.setCompanyCode(document.getCompany().getCompanyCode());
        }

        if (document.getCarrier() != null) {
            document.setCarrierCode(document.getCarrier().getCarrierCode());
        }

        if (document.getHawbReceivedBy() != null) {
            document.setHawbReceivedByCode(document.getHawbReceivedBy().getEmployeeCode());
        }

        if (document.getDoIssuedBy() != null) {
            document.setDoIssuedByCode(document.getDoIssuedBy().getEmployeeCode());
        }

        if (document.getCanIssuedBy() != null) {
            document.setCanIssuedByCode(document.getCanIssuedBy().getEmployeeCode());
        }
    }

    private void changeDateAndTime(DocumentDetail document,
                                   ShipmentServiceDetail service) {


        if (document.getHawbReceivedOn() != null) {
            SimpleDateFormat hawb = new SimpleDateFormat("yyyy-MM-dd");
            try {
                document.setHawbReceivedOn(hawb.parse(hawb.format(document.getHawbReceivedOn())));
            } catch (ParseException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }

    }

    private boolean dynamicField(String fieldId, Long countryId) {

        DynamicFields dy = dynamicFieldRepository.findByFieldId(fieldId);

        if (dy != null) {
            if (dy.getCountryMasterList() != null && dy.getCountryMasterList().size() != 0) {
                for (CountryMaster cm : dy.getCountryMasterList()) {
                    if (Objects.equals(cm.getId(), countryId)) {
                        return true;
                    }
                }
            }
        }


        return false;
    }

    private void setAgreed(DocumentDetail document,
                           ShipmentServiceDetail service) {

        if (document.getRatePerCharge() != null) {
            service.setIsAgreed(YesNo.Yes);
        } else {
            service.setIsAgreed(YesNo.No);
        }
    }

    private void setSequenceNo(DocumentDetail document) {
        if (document.getDocumentNo() == null || document.getId() == null || document.getDocumentNo().trim().length() == 0)
            document.setDocumentNo(sequenceGeneratorService.getSequenceValueByType(SequenceType.DOCUMENT));
    }

    private void setHawbNo(DocumentDetail document, ShipmentServiceDetail service) {

        if (document.getHawbNo() == null || document.getHawbNo().trim().length() == 0) {

            DocumentPrefixMaster docPrefix = documentPrefixMasterRepository.getByDocumentPrefixMaster(service.getLocation().getId(), DocPrefixDocumentType.HAWB);


            if (docPrefix != null) {

                String sequence = "";

                if (docPrefix.getDocPrefix() != null && docPrefix.getDocPrefix().trim().length() != 0) {

                    if (sequence != null && sequence.trim().length() != 0) {
                        sequence = sequence + "/";
                    }

                    sequence = sequence + docPrefix.getDocPrefix();

                    Date date = TimeUtil.getCurrentLocationTime(service.getLocation());

                    sequence = sequence.replaceAll("#DD#", new SimpleDateFormat("dd").format(date));
                    sequence = sequence.replaceAll("#YYYY#", new SimpleDateFormat("yyyy").format(date));
                    sequence = sequence.replaceAll("#YY#", new SimpleDateFormat("yy").format(date));
                    sequence = sequence.replaceAll("#MM#", new SimpleDateFormat("MM").format(date));
                    sequence = sequence.replaceAll("#MMM#", new SimpleDateFormat("MMM").format(date));

                }

                if (docPrefix.getAddPOR() != null && docPrefix.getAddPOR() == BooleanType.TRUE) {

                    if (sequence != null && sequence.trim().length() != 0) {
                        sequence = sequence + "/";
                    }

                    sequence = sequence + document.getPol().getPortCode();

                }


                if (docPrefix.getAddFDC() != null && docPrefix.getAddFDC() == BooleanType.TRUE) {

                    if (sequence != null && sequence.trim().length() != 0) {
                        sequence = sequence + "/";
                    }

                    sequence = sequence + document.getPod().getPortCode();

                }

                {
                    SequenceGenerator seqGen = sequenceGeneratorRepository.findBySequenceType(SequenceType.DOCUMENT);

                    if (sequence != null && sequence.trim().length() != 0) {
                        sequence = sequence + "/";
                    }

                    sequence = sequence + (document.getDocumentNo().replaceAll(seqGen.getPrefix(), ""));
                }

                if (docPrefix.getDocSuffix() != null && docPrefix.getDocSuffix().trim().length() != 0) {

                    if (sequence != null && sequence.trim().length() != 0) {
                        sequence = sequence + "/";
                    }

                    sequence = sequence + docPrefix.getDocSuffix();

                    Date date = TimeUtil.getCurrentLocationTime(service.getLocation());

                    sequence = sequence.replaceAll("#DD#", new SimpleDateFormat("dd").format(date));
                    sequence = sequence.replaceAll("#YYYY#", new SimpleDateFormat("yyyy").format(date));
                    sequence = sequence.replaceAll("#YY#", new SimpleDateFormat("yy").format(date));
                    sequence = sequence.replaceAll("#MM#", new SimpleDateFormat("MM").format(date));
                    sequence = sequence.replaceAll("#MMM#", new SimpleDateFormat("MMM").format(date));
                }
                document.setHawbNo(sequence);

            } else {
                document.setHawbNo(document.getDocumentNo());
            }
        }
    }

    private void verifyCarrier(DocumentDetail document) {
        if (document.getCarrier() != null && document.getCarrier().getId() != null) {
            ValidateUtil.notNull(document.getCarrier(), ErrorCode.SHIPMENT_CARRIER);
            ValidateUtil.notNull(document.getCarrier().getId(), ErrorCode.SHIPMENT_CARRIER);
            CarrierMaster carrier = carrierMasterRepository.findById(document.getCarrier().getId());
            ValidateUtil.notNullOrEmpty(carrier.getCarrierCode(), ErrorCode.SHIPMENT_CARRIER);
            ValidateUtil.isStatusBlocked(carrier.getStatus(), ErrorCode.SHIPMENT_CARRIER_BLOCK);
            ValidateUtil.isStatusHidden(carrier.getStatus(), ErrorCode.SHIPMENT_CARRIER_HIDE);
        } else {
            document.setCarrier(null);
        }
    }

    private void verifyBLReceived(DocumentDetail document) {

        if ((document.getBlReceivedPerson() != null && document.getBlReceivedPerson().getId() != null)
                || document.getBlReceivedDate() != null) {

            if (document.getBlReceivedPerson() == null || document.getBlReceivedPerson().getId() == null) {
                throw new RestException(ErrorCode.SHIPMENT_DOCUMENT_BL_EMP);
            }

            if (document.getBlReceivedDate() == null) {
                throw new RestException(ErrorCode.SHIPMENT_DOCUMENT_BL_RECEIVED_DATE);
            }
        }

        if (document.getBlReceivedPerson() != null && document.getBlReceivedPerson().getId() != null) {

            ValidateUtil.notNull(document.getBlReceivedPerson(), ErrorCode.SHIPMENT_DOCUMENT_BL_EMP);
            ValidateUtil.notNull(document.getBlReceivedPerson().getId(), ErrorCode.SHIPMENT_DOCUMENT_BL_EMP);
            ValidateUtil.notNullOrEmpty(document.getBlReceivedPerson().getEmployeeCode(), ErrorCode.SHIPMENT_DOCUMENT_BL_EMP);

            ValidateUtil.isEmployeeResigned(document.getBlReceivedPerson().getEmployementStatus(), ErrorCode.SHIPMENT_DOCUMENT_BL_EMP_RESIGNED);
            ValidateUtil.isEmployeeTerminated(document.getBlReceivedPerson().getEmployementStatus(), ErrorCode.SHIPMENT_DOCUMENT_BL_EMP_TERMINATED);

            document.setBlReceivedPersonCode(document.getBlReceivedPerson().getEmployeeCode());

        } else {
            document.setBlReceivedPerson(null);
            document.setBlReceivedPersonCode(null);
        }

        if (document.getBlReceivedDate() != null) {
            ValidateUtil.notNull(document.getBlReceivedDate(), ErrorCode.SHIPMENT_DOCUMENT_BL_RECEIVED_DATE);
        } else {
            document.setBlReceivedDate(null);
        }
    }

    private void verifyRateClass(DocumentDetail document, ShipmentServiceDetail service) {
        if (document.getRateClass() == null) {
            if (document.getChargebleWeight() < 45) {
                document.setRateClass(RateClass.N);
            } else {
                document.setRateClass(RateClass.Q);
            }
        }
    }

    private void verifyMarksAndNo(DocumentDetail document) {
        if (document.getMarksAndNo() != null && document.getMarksAndNo().trim().length() != 0) {
            ValidateUtil.notNullOrEmpty(document.getMarksAndNo(), ErrorCode.SHIPMENT_DOCUMENT_MARKS_AND_NO);
            ValidateUtil.validateLength(document.getMarksAndNo(), 1, 4000, ErrorCode.SHIPMENT_DOCUMENT_MARKS_AND_NO);
        } else {
            document.setMarksAndNo(null);
        }
    }

    private void verifyCommodityDescription(DocumentDetail document) {
        if (document.getCommodityDescription() != null && document.getCommodityDescription().trim().length() != 0) {
            ValidateUtil.notNullOrEmpty(document.getCommodityDescription(), ErrorCode.SHIPMENT_DOCUMENT_COMMODITY_DESCRIPTION);
            ValidateUtil.validateLength(document.getCommodityDescription(), 1, 4000, ErrorCode.SHIPMENT_DOCUMENT_COMMODITY_DESCRIPTION);
        } else {
            document.setCommodityDescription(null);
        }
    }

    private void verifyAccoutingInformation(DocumentDetail document) {
        if (document.getAccoutingInformation() != null && document.getAccoutingInformation().trim().length() != 0) {
            ValidateUtil.notNullOrEmpty(document.getAccoutingInformation(), ErrorCode.SHIPMENT_DOCUMENT_ACCOUNTING_INFO);
            ValidateUtil.validateLength(document.getAccoutingInformation(), 1, 4000, ErrorCode.SHIPMENT_DOCUMENT_ACCOUNTING_INFO);
        } else {
            document.setAccoutingInformation(null);
        }
    }

    private void verifyHandlingInformation(DocumentDetail document) {
        if (document.getHandlingInformation() != null && document.getHandlingInformation().trim().length() != 0) {
            ValidateUtil.notNullOrEmpty(document.getHandlingInformation(), ErrorCode.SHIPMENT_DOCUMENT_HANDLING_INFO);
            ValidateUtil.validateLength(document.getHandlingInformation(), 1, 4000, ErrorCode.SHIPMENT_DOCUMENT_HANDLING_INFO);
        } else {
            document.setHandlingInformation(null);
        }
    }


    private void verifyWeight(DocumentDetail document, ShipmentServiceDetail service) {

		/*ValidateUtil.notNull(document.getRatePerCharge(), ErrorCode.SHIPMENT_RATE_PER_CHARGE);*/

        if (document.getRatePerCharge() != null) {
            if (document.getRatePerCharge() < 0) {
                throw new RestException(ErrorCode.SHIPMENT_RATE_PER_CHARGE_INVALID);
            }
        }

        ServiceMaster serviceMaster = serviceMasterRepository.getOne(service.getServiceMaster().getId());

        if (serviceMaster.getImportExport() == ImportExport.Export) {
            ValidateUtil.notNull(document.getGrossWeight(), ErrorCode.SHIPMENT_DOC_GROSS_WEIGHT_MANDATORY);
        }

        if (document.getGrossWeight() != null) {
            if (document.getGrossWeight() <= 0) {
                throw new RestException(ErrorCode.SHIPMENT_DOC_GROSS_WEIGHT_INVALID);
            }
            if (document.getGrossWeight() > 99999999999.999) {
                throw new RestException(ErrorCode.SHIPMENT_DOC_GROSS_WEIGHT_INVALID);
            }
        }

        if (serviceMaster.getImportExport() == ImportExport.Export) {
            ValidateUtil.notNull(document.getGrossWeightInPound(), ErrorCode.SHIPMENT_DOC_GROSS_POUND_MANDATORY);
        }
			
			
			
			
			/*if(service.getServiceMaster().getImportExport()==ImportExport.Export){
				ValidateUtil.notNull(document.getVolumeWeight(), ErrorCode.SHIPMENT_DOC_VOLUME_WEIGHT_MANDATORY);
			}*/

        if (document.getVolumeWeight() != null) {
            if (document.getVolumeWeight() != null && document.getVolumeWeight() < 0) {
                throw new RestException(ErrorCode.SHIPMENT_DOC_VOLUME_WEIGHT_INVALID);
            }
            if (document.getVolumeWeight() > 99999999999.999) {
                throw new RestException(ErrorCode.SHIPMENT_DOC_VOLUME_WEIGHT_INVALID);
            }
        }
			
			/*if(service.getServiceMaster().getImportExport()==ImportExport.Export){
				ValidateUtil.notNull(document.getVolumeWeightInPound(), ErrorCode.SHIPMENT_DOC_VOLUME_POUND_MANDATORY);	
			}*/

        if (document.getLocation() != null && document.getLocation().getCountryMaster() != null && dynamicField("Shipment > Service > LBS", document.getLocation().getCountryMaster().getId())) {

            if (serviceMaster.getImportExport() == ImportExport.Export) {
                if (document.getVolumeWeightInPound() != null) {
                    if (document.getVolumeWeightInPound() != null && document.getVolumeWeightInPound() <= 0) {
                        throw new RestException(ErrorCode.SHIPMENT_DOC_VOLUME_POUND_INVALID);
                    }
                    if (document.getVolumeWeightInPound() != null && document.getVolumeWeightInPound() > 99999999999.999) {
                        throw new RestException(ErrorCode.SHIPMENT_DOC_VOLUME_POUND_INVALID);
                    }
                }

                if (document.getGrossWeightInPound() != null) {
                    if (document.getGrossWeightInPound() != null && document.getGrossWeightInPound() <= 0) {
                        throw new RestException(ErrorCode.SHIPMENT_DOC_GROSS_POUND_INVALID);
                    }
                    if (document.getGrossWeightInPound() != null && document.getGrossWeightInPound() > 99999999999.999) {
                        throw new RestException(ErrorCode.SHIPMENT_DOC_GROSS_POUND_INVALID);
                    }
                }
            }


        }

        if (serviceMaster.getImportExport() == ImportExport.Export) {
            ValidateUtil.notNull(document.getChargebleWeight(), ErrorCode.SHIPMENT_CHARGEABLE_WEIGHT);
        }
        if (document.getChargebleWeight() != null) {
            if (document.getChargebleWeight() <= 0) {
                throw new RestException(ErrorCode.SHIPMENT_CHARGE_LESS_ZERO);
            }
        }


    }//Weight validation endss


    private void verifyNoOfPieces(DocumentDetail document, ShipmentServiceDetail service) {

        ServiceMaster serviceMaster = serviceMasterRepository.getOne(service.getServiceMaster().getId());

        if (serviceMaster.getImportExport() == ImportExport.Export) {
            ValidateUtil.notNull(document.getNoOfPieces(), ErrorCode.SHIPMENT_DOC_PIECES_MANDATORY);
            if (document.getNoOfPieces() <= 0 && document.getNoOfPieces() > 999999999) {
                throw new RestException(ErrorCode.SHIPMENT_DOC_PIECES_INVALID);
            }
        }

    }

    private void verifyPack(DocumentDetail document) {
        if (document.getPackMaster() != null && document.getPackMaster().getId() != null) {
            ValidateUtil.notNull(document.getPackMaster(), ErrorCode.SHIPMENT_PACK);
            ValidateUtil.notNull(document.getPackMaster().getId(), ErrorCode.SHIPMENT_PACK);
            PackMaster pack = packMasterRepository.getOne(document.getPackMaster().getId());
            ValidateUtil.notNullOrEmpty(pack.getPackCode(), ErrorCode.SHIPMENT_PACK);
            ValidateUtil.isStatusBlocked(pack.getStatus(), ErrorCode.SHIPMENT_PACK_BLOCK);
            ValidateUtil.isStatusHidden(pack.getStatus(), ErrorCode.SHIPMENT_PACK_HIDE);

            document.setPackCode(pack.getPackCode());
        } else {
            document.setPackMaster(null);
            document.setPackCode(null);
        }

    }

    private void verifyPod(DocumentDetail document, ShipmentServiceDetail service) {

        if (document.getPod() != null) {
            ValidateUtil.notNull(document.getPod(), ErrorCode.SHIPMENT_POD);
            ValidateUtil.notNull(document.getPod().getId(), ErrorCode.SHIPMENT_POD);

            PortMaster pod = portMasterRepository.getOne(document.getPod().getId());
            PortMaster pol = portMasterRepository.getOne(document.getPol().getId());
            ServiceMaster serviceMaster = serviceMasterRepository.getOne(service.getServiceMaster().getId());
            ValidateUtil.notNullOrEmpty(pod.getPortCode(), ErrorCode.SHIPMENT_POD);

            ValidateUtil.isStatusBlocked(pod.getStatus(), ErrorCode.SHIPMENT_POD_BLOCK);
            ValidateUtil.isStatusHidden(pod.getStatus(), ErrorCode.SHIPMENT_POD_HIDE);

            if (serviceMaster.getTransportMode() != pod.getTransportMode()) {
                throw new RestException(ErrorCode.SHIPMENT_SERVICE_POD_MODE);
            }

            if (Objects.equals(pod.getId(), pol.getId())) {
                throw new RestException(ErrorCode.SHIPMENT_ORIGIN_DESTINATION_EQUAL);
            }

            if (pod.getPortCode().equals(pol.getPortCode())) {
                throw new RestException(ErrorCode.SHIPMENT_POD_POL_EQUAL);
            }

            document.setPodCode(pod.getPortCode());
        } else {
            document.setPod(null);
            document.setPodCode(null);
        }

    }

    private void verifyPol(DocumentDetail document, ShipmentServiceDetail service) {

        if ((document.getPol() != null && document.getPod() == null)
                || (document.getPol() == null && document.getPod() != null)) {

            if (document.getPol() == null) {
                throw new RestException(ErrorCode.SHIPMENT_POL);
            }

            if (document.getPod() == null) {
                throw new RestException(ErrorCode.SHIPMENT_POD);
            }
        }

        if (document.getPol() != null) {
            ValidateUtil.notNull(document.getPol(), ErrorCode.SHIPMENT_POL);
            ValidateUtil.notNull(document.getPol().getId(), ErrorCode.SHIPMENT_POL);

            PortMaster pol = portMasterRepository.getOne(document.getPol().getId());
            ServiceMaster serviceMaster = serviceMasterRepository.getOne(service.getServiceMaster().getId());

            ValidateUtil.notNullOrEmpty(pol.getPortCode(), ErrorCode.SHIPMENT_POL);

            ValidateUtil.isStatusBlocked(pol.getStatus(), ErrorCode.SHIPMENT_POL_BLOCK);
            ValidateUtil.isStatusHidden(pol.getStatus(), ErrorCode.SHIPMENT_POL_HIDE);

            if (serviceMaster.getTransportMode() != pol.getTransportMode()) {
                throw new RestException(ErrorCode.SHIPMENT_SERVICE_POL_MODE);
            }

            document.setPolCode(pol.getPortCode());

        } else {
            document.setPol(null);
            document.setPolCode(null);
        }

    }

    private void verifyDestination(DocumentDetail document, ShipmentServiceDetail service) {


        ValidateUtil.notNull(document.getDestination(), ErrorCode.SHIPMENT_DESTINATION);
        ValidateUtil.notNull(document.getDestination().getId(), ErrorCode.SHIPMENT_DESTINATION);

        PortMaster destination = portMasterRepository.getOne(document.getDestination().getId());
        ServiceMaster serviceMaster = serviceMasterRepository.getOne(service.getServiceMaster().getId());
        PortMaster origin = portMasterRepository.getOne(service.getOrigin().getId());

        ValidateUtil.notNullOrEmpty(destination.getPortCode(), ErrorCode.SHIPMENT_DESTINATION);

        ValidateUtil.isStatusBlocked(destination.getStatus(), ErrorCode.SHIPMENT_DESTINATION_BLOCK);
        ValidateUtil.isStatusHidden(destination.getStatus(), ErrorCode.SHIPMENT_DESTINATION_HIDE);

        ValidateUtil.isAirTransportMode(destination.getTransportMode(), ErrorCode.SHIPMENT_DESTINATION_AIR_MODE);

        if (Objects.equals(destination.getId(), origin.getId())) {
            throw new RestException(ErrorCode.SHIPMENT_ORIGIN_DESTINATION_EQUAL);
        }

        if (destination.getPortCode().equals(origin.getPortCode())) {
            throw new RestException(ErrorCode.SHIPMENT_ORIGIN_DESTINATION_EQUAL);
        }

        if (serviceMaster.getTransportMode() != destination.getTransportMode()) {
            throw new RestException(ErrorCode.SHIPMENT_SERVICE_DESTINATION_MODE);
        }

        document.setDestinationCode(destination.getPortCode());


    }

    private void verifyOrigin(DocumentDetail document, ShipmentServiceDetail service) {

        if ((document.getOrigin() != null && document.getDestination() == null)
                || (document.getOrigin() == null && document.getDestination() != null)) {

            if (document.getOrigin() == null) {
                throw new RestException(ErrorCode.SHIPMENT_ORIGIN);
            }

            if (document.getDestination() == null) {
                throw new RestException(ErrorCode.SHIPMENT_DESTINATION);
            }
        }


        if (document.getOrigin() != null) {

            ValidateUtil.notNull(document.getOrigin(), ErrorCode.SHIPMENT_ORIGIN);
            ValidateUtil.notNull(document.getOrigin().getId(), ErrorCode.SHIPMENT_ORIGIN);

            ServiceMaster serviceMaster = serviceMasterRepository.getOne(service.getServiceMaster().getId());
            PortMaster origin = portMasterRepository.getOne(service.getOrigin().getId());


            ValidateUtil.notNullOrEmpty(origin.getPortCode(), ErrorCode.SHIPMENT_ORIGIN);

            ValidateUtil.isStatusBlocked(origin.getStatus(), ErrorCode.SHIPMENT_ORIGIN_BLOCK);
            ValidateUtil.isStatusHidden(origin.getStatus(), ErrorCode.SHIPMENT_ORIGIN_HIDE);

            ValidateUtil.isAirTransportMode(origin.getTransportMode(), ErrorCode.SHIPMENT_ORIGIN_AIR_MODE);


            if (serviceMaster.getTransportMode() != origin.getTransportMode()) {
                throw new RestException(ErrorCode.SHIPMENT_SERVICE_ORIGIN_MODE);
            }

            document.setOriginCode(origin.getPortCode());
        } else {
            document.setOrigin(null);
            document.setOriginCode(null);
        }


    }

    private void verifyChaAgent(DocumentDetail document, String emailSplit) {

        if (document.getChaAgent() != null && document.getChaAgent().getId() != null) {
            PartyMaster tmpParty = cacheRepository.get(SaaSUtil.getSaaSId(), PartyMaster.class.getName(), document.getChaAgent().getId(), partyMasterRepository);

            ValidateUtil.notNull(tmpParty, ErrorCode.SHIPMENT_DOCUMENT_CHA_AGENT);
            ValidateUtil.notNull(tmpParty.getId(), ErrorCode.SHIPMENT_DOCUMENT_CHA_AGENT);
            ValidateUtil.notNullOrEmpty(tmpParty.getPartyCode(), ErrorCode.SHIPMENT_DOCUMENT_CHA_AGENT);
            ValidateUtil.isStatusBlocked(tmpParty.getStatus(), ErrorCode.SHIPMENT_DOCUMENT_CHA_AGENT_BLOCK);
            ValidateUtil.isStatusHidden(tmpParty.getStatus(), ErrorCode.SHIPMENT_DOCUMENT_CHA_AGENT_HIDE);

            ValidateUtil.isDefaulter(tmpParty, ErrorCode.SHIPMENT_DOC_CHAAGENT_DEFAULTER);

            ValidateUtil.notNull(document.getChaAgentAddress(), ErrorCode.SHIPMENT_DOCUMENT_CHA_AGENT_ADDRESS);


            ValidateUtil.notNullOrEmpty(document.getChaAgentAddress().getAddressLine1(), ErrorCode.SHIPMENT_DOCUMENT_CHA_AGENT_ADDRESS_1);
            ValidateUtil.validateLength(document.getChaAgentAddress().getAddressLine1(), 1, 100, ErrorCode.SHIPMENT_DOCUMENT_CHA_AGENT_ADDRESS_1_LENGTH);

            if (document.getChaAgentAddress().getAddressLine2() != null && document.getChaAgentAddress().getAddressLine2().trim().length() != 0) {
                ValidateUtil.notNullOrEmpty(document.getChaAgentAddress().getAddressLine2(), ErrorCode.SHIPMENT_DOCUMENT_CHA_AGENT_ADDRESS_2);
                ValidateUtil.validateLength(document.getChaAgentAddress().getAddressLine2(), 1, 100, ErrorCode.SHIPMENT_DOCUMENT_CHA_AGENT_ADDRESS_2_LENGTH);
            }


            if (document.getChaAgentAddress().getAddressLine3() != null && document.getChaAgentAddress().getAddressLine3().trim().length() != 0)
                ValidateUtil.validateLength(document.getChaAgentAddress().getAddressLine3(), 1, 100, ErrorCode.SHIPMENT_DOCUMENT_CHA_AGENT_ADDRESS_3_LENGTH);


            if (document.getChaAgentAddress().getAddressLine3() == null || document.getChaAgentAddress().getAddressLine3() == "") {
                document.getChaAgentAddress().setAddressLine3(document.getChaAgentAddress().getAddressLine4());
                document.getChaAgentAddress().setAddressLine4(null);
            }


            if (document.getChaAgentAddress().getAddressLine4() != null && document.getChaAgentAddress().getAddressLine4().trim().length() != 0) {
                ValidateUtil.validateLength(document.getChaAgentAddress().getAddressLine4(), 1, 100, ErrorCode.SHIPMENT_DOCUMENT_CHA_AGENT_ADDRESS_4_LENGTH);
            }

            if (document.getChaAgentAddress().getEmail() != null) {
                ValidateUtil.validateLength(document.getChaAgentAddress().getEmail(), 1, 500, ErrorCode.SHIPMENT_DOCUMENT_CHA_AGENT_EMAIL_LENGTH);

                for (String email : document.getChaAgentAddress().getEmail().split(emailSplit)) {
                    if (!Pattern.compile(ValidateUtil.regularExp).matcher(email).matches()) {
                        throw new RestException(ErrorCode.SHIPMENT_DOCUMENT_CHA_AGENT_EMAIL_LENGTH);
                    }
                }
            }
            document.setChaAgentCode(tmpParty.getPartyCode());

            if (document.getChaAgentAddress().getCity() != null) {
                CityMaster city = cityMasterRepository.getOne(document.getChaAgentAddress().getCity().getId());
                document.getChaAgentAddress().setCityCode(city.getCityCode());
            }

            if (document.getChaAgentAddress().getState() != null) {
                StateMaster state = stateMasterRepository.getOne(document.getChaAgentAddress().getState().getId());
                document.getChaAgentAddress().setStateCode(state.getStateCode());
            }

            if (document.getChaAgentAddress().getCountry() != null) {
                CountryMaster country = countryMasterRepository.getOne(document.getChaAgentAddress().getCountry().getId());
                document.getChaAgentAddress().setCountryCode(country.getCountryCode());
            }
        } else {
            document.setChaAgent(null);
            document.setChaAgentCode(null);
            document.setChaAgentAddress(null);
        }

    }

    private void verifyIssuingAgent(DocumentDetail document, String emailSplit) {

        if (document.getIssuingAgent() != null && document.getIssuingAgent().getId() != null) {
            PartyMaster tmpParty = cacheRepository.get(SaaSUtil.getSaaSId(), PartyMaster.class.getName(), document.getIssuingAgent().getId(), partyMasterRepository);

            ValidateUtil.notNull(tmpParty, ErrorCode.SHIPMENT_DOCUMENT_ISSUING_AGENT);
            ValidateUtil.notNull(tmpParty.getId(), ErrorCode.SHIPMENT_DOCUMENT_ISSUING_AGENT);
            ValidateUtil.notNullOrEmpty(tmpParty.getPartyCode(), ErrorCode.SHIPMENT_DOCUMENT_ISSUING_AGENT);
            ValidateUtil.isStatusBlocked(tmpParty.getStatus(), ErrorCode.SHIPMENT_DOCUMENT_ISSUING_AGENT_BLOCK);
            ValidateUtil.isStatusHidden(tmpParty.getStatus(), ErrorCode.SHIPMENT_DOCUMENT_ISSUING_AGENT_HIDE);

            ValidateUtil.isDefaulter(tmpParty, ErrorCode.SHIPMENT_DOC_ISSAGENT_DEFAULTER);

            ValidateUtil.notNull(document.getIssuingAgentAddress(), ErrorCode.SHIPMENT_DOCUMENT_ISSUING_AGENT_ADDRESS);

            ValidateUtil.notNullOrEmpty(document.getIssuingAgentAddress().getAddressLine1(), ErrorCode.SHIPMENT_DOCUMENT_ISSUING_AGENT_ADDRESS_1);
            ValidateUtil.validateLength(document.getIssuingAgentAddress().getAddressLine1(), 1, 100, ErrorCode.SHIPMENT_DOCUMENT_ISSUING_AGENT_ADDRESS_1_LENGTH);

            if (document.getIssuingAgentAddress().getAddressLine2() != null && document.getIssuingAgentAddress().getAddressLine2().trim().length() != 0) {
                ValidateUtil.notNullOrEmpty(document.getIssuingAgentAddress().getAddressLine2(), ErrorCode.SHIPMENT_DOCUMENT_ISSUING_AGENT_ADDRESS_2);
                ValidateUtil.validateLength(document.getIssuingAgentAddress().getAddressLine2(), 1, 100, ErrorCode.SHIPMENT_DOCUMENT_ISSUING_AGENT_ADDRESS_2_LENGTH);
            }


            if (document.getIssuingAgentAddress().getAddressLine3() != null && document.getIssuingAgentAddress().getAddressLine3().trim().length() != 0)
                ValidateUtil.validateLength(document.getIssuingAgentAddress().getAddressLine3(), 1, 100, ErrorCode.SHIPMENT_DOCUMENT_ISSUING_AGENT_ADDRESS_3_LENGTH);

            //ValidateUtil.notNullOrEmpty(document.getIssuingAgentAddress().getAddressLine4(), ErrorCode.SHIPMENT_DOCUMENT_ISSUING_AGENT_ADDRESS_4);

            if (document.getIssuingAgentAddress().getAddressLine3() == null || document.getIssuingAgentAddress().getAddressLine3() == "") {
                document.getIssuingAgentAddress().setAddressLine3(document.getIssuingAgentAddress().getAddressLine4());
                document.getIssuingAgentAddress().setAddressLine4(null);
            }


            if (document.getIssuingAgentAddress().getAddressLine4() != null && document.getIssuingAgentAddress().getAddressLine4().trim().length() != 0) {
                ValidateUtil.validateLength(document.getIssuingAgentAddress().getAddressLine4(), 1, 100, ErrorCode.SHIPMENT_DOCUMENT_ISSUING_AGENT_ADDRESS_4_LENGTH);
            }

            if (document.getIssuingAgentAddress().getEmail() != null) {
                ValidateUtil.validateLength(document.getIssuingAgentAddress().getEmail(), 1, 500, ErrorCode.SHIPMENT_DOCUMENT_ISSUING_AGENT_EMAIL_LENGTH);

                for (String email : document.getIssuingAgentAddress().getEmail().split(emailSplit)) {
                    if (!Pattern.compile(ValidateUtil.regularExp).matcher(email).matches()) {
                        throw new RestException(ErrorCode.SHIPMENT_DOCUMENT_ISSUING_AGENT_EMAIL_LENGTH);
                    }
                }
            }
            document.setIssuingAgentCode(tmpParty.getPartyCode());


            if (document.getIssuingAgentAddress().getCity() != null) {
                CityMaster city = cityMasterRepository.getOne(document.getIssuingAgentAddress().getCity().getId());
                document.getIssuingAgentAddress().setCityCode(city.getCityCode());
            }

            if (document.getIssuingAgentAddress().getState() != null) {
                StateMaster state = stateMasterRepository.getOne(document.getIssuingAgentAddress().getState().getId());
                document.getIssuingAgentAddress().setStateCode(state.getStateCode());
            }

            if (document.getIssuingAgentAddress().getCountry() != null) {
                CountryMaster country = countryMasterRepository.getOne(document.getIssuingAgentAddress().getCountry().getId());
                document.getIssuingAgentAddress().setCountryCode(country.getCountryCode());
            }
        } else {
            document.setIssuingAgent(null);
            document.setIssuingAgentCode(null);
            document.setIssuingAgentAddress(null);
        }

    }

    private void verifyAgent(DocumentDetail document, String emailSplit) {

        if (document.getAgent() != null && document.getAgent().getId() != null) {

            PartyMaster tmpParty = cacheRepository.get(SaaSUtil.getSaaSId(), PartyMaster.class.getName(), document.getAgent().getId(), partyMasterRepository);


            ValidateUtil.notNull(tmpParty, ErrorCode.SHIPMENT_DOCUMENT_AGENT);
            ValidateUtil.notNull(tmpParty.getId(), ErrorCode.SHIPMENT_DOCUMENT_AGENT);
            ValidateUtil.notNullOrEmpty(tmpParty.getPartyCode(), ErrorCode.SHIPMENT_DOCUMENT_AGENT);
            ValidateUtil.isStatusBlocked(tmpParty.getStatus(), ErrorCode.SHIPMENT_DOCUMENT_AGENT_BLOCK);
            ValidateUtil.isStatusHidden(tmpParty.getStatus(), ErrorCode.SHIPMENT_DOCUMENT_AGENT_HIDE);

            ValidateUtil.isDefaulter(tmpParty, ErrorCode.SHIPMENT_DOC_AGENT_DEFAULTER);

            ValidateUtil.notNull(document.getAgentAddress(), ErrorCode.SHIPMENT_DOCUMENT_AGENT_ADDRESS);

            ValidateUtil.notNullOrEmpty(document.getAgentAddress().getAddressLine1(), ErrorCode.SHIPMENT_DOCUMENT_AGENT_ADDRESS_1);
            ValidateUtil.validateLength(document.getAgentAddress().getAddressLine1(), 1, 100, ErrorCode.SHIPMENT_DOCUMENT_AGENT_ADDRESS_1_LENGTH);

            if (document.getAgentAddress().getAddressLine2() != null && document.getAgentAddress().getAddressLine2().trim().length() != 0) {
                ValidateUtil.notNullOrEmpty(document.getAgentAddress().getAddressLine2(), ErrorCode.SHIPMENT_DOCUMENT_AGENT_ADDRESS_2);
                ValidateUtil.validateLength(document.getAgentAddress().getAddressLine2(), 1, 100, ErrorCode.SHIPMENT_DOCUMENT_AGENT_ADDRESS_2_LENGTH);
            }


            if (document.getAgentAddress().getAddressLine3() != null && document.getAgentAddress().getAddressLine3().trim().length() != 0)
                ValidateUtil.validateLength(document.getAgentAddress().getAddressLine3(), 1, 100, ErrorCode.SHIPMENT_DOCUMENT_AGENT_ADDRESS_3_LENGTH);


            if (document.getAgentAddress().getAddressLine3() == null || document.getAgentAddress().getAddressLine3() == "") {
                document.getAgentAddress().setAddressLine3(document.getAgentAddress().getAddressLine4());
                document.getAgentAddress().setAddressLine4(null);
            }


            //ValidateUtil.notNullOrEmpty(document.getAgentAddress().getAddressLine4(), ErrorCode.SHIPMENT_DOCUMENT_AGENT_ADDRESS_4);
            if (document.getAgentAddress().getAddressLine4() != null && document.getAgentAddress().getAddressLine4().trim().length() != 0) {
                ValidateUtil.validateLength(document.getAgentAddress().getAddressLine4(), 1, 100, ErrorCode.SHIPMENT_DOCUMENT_AGENT_ADDRESS_4_LENGTH);
            }

            //ValidateUtil.notNullOrEmpty(document.getAgentAddress().getEmail(), ErrorCode.SHIPMENT_DOCUMENT_AGENT_EMAIL);

            if (document.getAgentAddress().getEmail() != null)
                ValidateUtil.validateLength(document.getAgentAddress().getEmail(), 1, 500, ErrorCode.SHIPMENT_DOCUMENT_AGENT_EMAIL_LENGTH);

            if (document.getAgentAddress().getEmail() != null) {
                for (String email : document.getAgentAddress().getEmail().split(emailSplit)) {
                    if (!Pattern.compile(ValidateUtil.regularExp).matcher(email).matches()) {
                        throw new RestException(ErrorCode.SHIPMENT_DOCUMENT_AGENT_EMAIL_LENGTH);
                    }
                }
            }
            document.setAgentCode(tmpParty.getPartyCode());


            if (document.getAgentAddress().getCity() != null) {
                CityMaster city = cityMasterRepository.getOne(document.getAgentAddress().getCity().getId());
                document.getAgentAddress().setCityCode(city.getCityCode());
            }

            if (document.getAgentAddress().getState() != null) {
                StateMaster state = stateMasterRepository.getOne(document.getAgentAddress().getState().getId());
                document.getAgentAddress().setStateCode(state.getStateCode());
            }

            if (document.getAgentAddress().getCountry() != null) {
                CountryMaster country = countryMasterRepository.getOne(document.getAgentAddress().getCountry().getId());
                document.getAgentAddress().setCountryCode(country.getCountryCode());
            }
        } else {
            document.setAgent(null);
            document.setAgentCode(null);
            document.setAgentAddress(null);
        }

    }

    private void verifySecondNotify(DocumentDetail document, String emailSplit) {

        if (document.getSecondNotify() != null && document.getSecondNotify().getId() != null) {

            PartyMaster tmpParty = cacheRepository.get(SaaSUtil.getSaaSId(), PartyMaster.class.getName(), document.getSecondNotify().getId(), partyMasterRepository);


            ValidateUtil.notNull(tmpParty, ErrorCode.SHIPMENT_DOCUMENT_SECOND_NOTIFY);
            ValidateUtil.notNull(tmpParty.getId(), ErrorCode.SHIPMENT_DOCUMENT_SECOND_NOTIFY);
            ValidateUtil.notNullOrEmpty(tmpParty.getPartyCode(), ErrorCode.SHIPMENT_DOCUMENT_SECOND_NOTIFY);
            ValidateUtil.isStatusBlocked(tmpParty.getStatus(), ErrorCode.SHIPMENT_DOCUMENT_SECOND_NOTIFY_BLOCK);
            ValidateUtil.isStatusHidden(tmpParty.getStatus(), ErrorCode.SHIPMENT_DOCUMENT_SECOND_NOTIFY_HIDE);

            ValidateUtil.isDefaulter(tmpParty, ErrorCode.SHIPMENT_DOC_SNOTI_DEFAULTER);

            ValidateUtil.notNull(document.getSecondNotifyAddress(), ErrorCode.SHIPMENT_DOCUMENT_SECOND_NOTIFY_ADDRESS);

            ValidateUtil.notNullOrEmpty(document.getSecondNotifyAddress().getAddressLine1(), ErrorCode.SHIPMENT_DOCUMENT_SECOND_NOTIFY_ADDRESS_1);
            ValidateUtil.validateLength(document.getSecondNotifyAddress().getAddressLine1(), 1, 100, ErrorCode.SHIPMENT_DOCUMENT_SECOND_NOTIFY_ADDRESS_1_LENGTH);

            if (document.getSecondNotifyAddress().getAddressLine2() != null && document.getSecondNotifyAddress().getAddressLine2().trim().length() != 0) {
                ValidateUtil.notNullOrEmpty(document.getSecondNotifyAddress().getAddressLine2(), ErrorCode.SHIPMENT_DOCUMENT_SECOND_NOTIFY_ADDRESS_2);
                ValidateUtil.validateLength(document.getSecondNotifyAddress().getAddressLine2(), 1, 100, ErrorCode.SHIPMENT_DOCUMENT_SECOND_NOTIFY_ADDRESS_2_LENGTH);
            }


            if (document.getSecondNotifyAddress().getAddressLine3() != null && document.getSecondNotifyAddress().getAddressLine3().trim().length() != 0) {
                ValidateUtil.validateLength(document.getSecondNotifyAddress().getAddressLine3(), 1, 100, ErrorCode.SHIPMENT_DOCUMENT_SECOND_NOTIFY_ADDRESS_3_LENGTH);
            }


            if (document.getSecondNotifyAddress().getAddressLine3() == null || document.getSecondNotifyAddress().getAddressLine3() == "") {
                document.getSecondNotifyAddress().setAddressLine3(document.getSecondNotifyAddress().getAddressLine4());
                document.getSecondNotifyAddress().setAddressLine4(null);
            }
            //ValidateUtil.notNullOrEmpty(document.getSecondNotifyAddress().getAddressLine4(), ErrorCode.SHIPMENT_DOCUMENT_SECOND_NOTIFY_ADDRESS_4);

            if (document.getSecondNotifyAddress().getAddressLine4() != null && document.getSecondNotifyAddress().getAddressLine4().trim().length() != 0) {
                ValidateUtil.validateLength(document.getSecondNotifyAddress().getAddressLine4(), 1, 100, ErrorCode.SHIPMENT_DOCUMENT_SECOND_NOTIFY_ADDRESS_4_LENGTH);
            }

            //ValidateUtil.notNullOrEmpty(document.getSecondNotifyAddress().getEmail(), ErrorCode.SHIPMENT_DOCUMENT_SECOND_NOTIFY_EMAIL);

            if (document.getSecondNotifyAddress().getEmail() != null) {
                ValidateUtil.validateLength(document.getSecondNotifyAddress().getEmail(), 1, 500, ErrorCode.SHIPMENT_DOCUMENT_SECOND_NOTIFY_EMAIL_LENGTH);
                for (String email : document.getSecondNotifyAddress().getEmail().split(emailSplit)) {
                    if (!Pattern.compile(ValidateUtil.regularExp).matcher(email).matches()) {
                        throw new RestException(ErrorCode.SHIPMENT_DOCUMENT_SECOND_NOTIFY_EMAIL_LENGTH);
                    }
                }
            }
            document.setSecondNotifyCode(tmpParty.getPartyCode());


            if (document.getSecondNotifyAddress().getCity() != null) {
                CityMaster city = cityMasterRepository.getOne(document.getSecondNotifyAddress().getCity().getId());
                document.getSecondNotifyAddress().setCityCode(city.getCityCode());
            }

            if (document.getSecondNotifyAddress().getState() != null) {
                StateMaster state = stateMasterRepository.getOne(document.getSecondNotifyAddress().getState().getId());
                document.getSecondNotifyAddress().setStateCode(state.getStateCode());
            }

            if (document.getSecondNotifyAddress().getCountry() != null) {
                CountryMaster country = countryMasterRepository.getOne(document.getSecondNotifyAddress().getCountry().getId());
                document.getSecondNotifyAddress().setCountryCode(country.getCountryCode());
            }
        } else {
            document.setSecondNotify(null);
            document.setSecondNotifyCode(null);
            document.setSecondNotifyAddress(null);
        }

    }

    private void verifyFirstNotify(DocumentDetail document, String emailSplit) {

        if (document.getFirstNotify() != null && document.getFirstNotify().getId() != null) {

            PartyMaster tmpParty = cacheRepository.get(SaaSUtil.getSaaSId(), PartyMaster.class.getName(), document.getFirstNotify().getId(), partyMasterRepository);

            ValidateUtil.notNull(tmpParty, ErrorCode.SHIPMENT_DOCUMENT_FIRST_NOTIFY);
            ValidateUtil.notNull(tmpParty.getId(), ErrorCode.SHIPMENT_DOCUMENT_FIRST_NOTIFY);
            ValidateUtil.notNullOrEmpty(tmpParty.getPartyCode(), ErrorCode.SHIPMENT_DOCUMENT_FIRST_NOTIFY);
            ValidateUtil.isStatusBlocked(tmpParty.getStatus(), ErrorCode.SHIPMENT_DOCUMENT_FIRST_NOTIFY_BLOCK);
            ValidateUtil.isStatusHidden(tmpParty.getStatus(), ErrorCode.SHIPMENT_DOCUMENT_FIRST_NOTIFY_HIDE);
            ValidateUtil.isDefaulter(tmpParty, ErrorCode.SHIPMENT_DOC_FNOTI_DEFAULTER);

            ValidateUtil.notNull(document.getFirstNotifyAddress(), ErrorCode.SHIPMENT_DOCUMENT_FIRST_NOTIFY_ADDRESS);

            ValidateUtil.notNullOrEmpty(document.getFirstNotifyAddress().getAddressLine1(), ErrorCode.SHIPMENT_DOCUMENT_FIRST_NOTIFY_ADDRESS_1);
            ValidateUtil.validateLength(document.getFirstNotifyAddress().getAddressLine1(), 1, 100, ErrorCode.SHIPMENT_DOCUMENT_FIRST_NOTIFY_ADDRESS_1_LENGTH);

            if (document.getFirstNotifyAddress().getAddressLine2() != null && document.getFirstNotifyAddress().getAddressLine2().trim().length() != 0) {
                ValidateUtil.notNullOrEmpty(document.getFirstNotifyAddress().getAddressLine2(), ErrorCode.SHIPMENT_DOCUMENT_FIRST_NOTIFY_ADDRESS_2);
                ValidateUtil.validateLength(document.getFirstNotifyAddress().getAddressLine2(), 1, 100, ErrorCode.SHIPMENT_DOCUMENT_FIRST_NOTIFY_ADDRESS_2_LENGTH);
            }

            ValidateUtil.notNullOrEmpty(document.getFirstNotifyAddress().getAddressLine2(), ErrorCode.SHIPMENT_DOCUMENT_FIRST_NOTIFY_ADDRESS_2);
            ValidateUtil.validateLength(document.getFirstNotifyAddress().getAddressLine2(), 1, 100, ErrorCode.SHIPMENT_DOCUMENT_FIRST_NOTIFY_ADDRESS_2_LENGTH);

            if (document.getFirstNotifyAddress().getAddressLine3() != null && document.getFirstNotifyAddress().getAddressLine3().trim().length() != 0) {
                ValidateUtil.validateLength(document.getFirstNotifyAddress().getAddressLine3(), 1, 100, ErrorCode.SHIPMENT_DOCUMENT_FIRST_NOTIFY_ADDRESS_3_LENGTH);
            }


            if (document.getFirstNotifyAddress().getAddressLine3() == null || document.getFirstNotifyAddress().getAddressLine3() == "") {
                document.getFirstNotifyAddress().setAddressLine3(document.getFirstNotifyAddress().getAddressLine4());
                document.getFirstNotifyAddress().setAddressLine4(null);
            }
            //ValidateUtil.notNullOrEmpty(document.getFirstNotifyAddress().getAddressLine4(), ErrorCode.SHIPMENT_DOCUMENT_FIRST_NOTIFY_ADDRESS_4);

            if (document.getFirstNotifyAddress().getAddressLine4() != null && document.getFirstNotifyAddress().getAddressLine4().trim().length() != 0) {
                ValidateUtil.validateLength(document.getFirstNotifyAddress().getAddressLine4(), 1, 100, ErrorCode.SHIPMENT_DOCUMENT_FIRST_NOTIFY_ADDRESS_4_LENGTH);
            }

            //ValidateUtil.notNullOrEmpty(document.getFirstNotifyAddress().getEmail(), ErrorCode.SHIPMENT_DOCUMENT_FIRST_NOTIFY_EMAIL);

            if (document.getFirstNotifyAddress().getEmail() != null) {
                ValidateUtil.validateLength(document.getFirstNotifyAddress().getEmail(), 1, 500, ErrorCode.SHIPMENT_DOCUMENT_FIRST_NOTIFY_EMAIL_LENGTH);

                for (String email : document.getFirstNotifyAddress().getEmail().split(emailSplit)) {
                    if (!Pattern.compile(ValidateUtil.regularExp).matcher(email).matches()) {
                        throw new RestException(ErrorCode.SHIPMENT_DOCUMENT_FIRST_NOTIFY_EMAIL_LENGTH);
                    }
                }
            }
            document.setFirstNotifyCode(tmpParty.getPartyCode());

            if (document.getFirstNotifyAddress().getCity() != null) {
                CityMaster city = cityMasterRepository.getOne(document.getFirstNotifyAddress().getCity().getId());
                document.getFirstNotifyAddress().setCityCode(city.getCityCode());
            }

            if (document.getFirstNotifyAddress().getState() != null) {
                StateMaster state = stateMasterRepository.getOne(document.getFirstNotifyAddress().getState().getId());
                document.getFirstNotifyAddress().setStateCode(state.getStateCode());
            }

            if (document.getFirstNotifyAddress().getCountry() != null) {
                CountryMaster country = countryMasterRepository.getOne(document.getFirstNotifyAddress().getCountry().getId());
                document.getFirstNotifyAddress().setCountryCode(country.getCountryCode());
            }
        } else {
            document.setFirstNotify(null);
            document.setFirstNotifyCode(null);
            document.setFirstNotifyAddress(null);
        }

    }


    private void verifyConsignee(DocumentDetail document, String emailSplit) {

        if (document.getConsignee() != null && document.getConsignee().getId() != null) {

            PartyMaster tmpParty = cacheRepository.get(SaaSUtil.getSaaSId(), PartyMaster.class.getName(), document.getConsignee().getId(), partyMasterRepository);


            ValidateUtil.notNull(tmpParty, ErrorCode.SHIPMENT_DOCUMENT_CONSIGNEE);
            ValidateUtil.notNull(tmpParty.getId(), ErrorCode.SHIPMENT_DOCUMENT_CONSIGNEE);
            ValidateUtil.notNullOrEmpty(tmpParty.getPartyCode(), ErrorCode.SHIPMENT_DOCUMENT_CONSIGNEE);
            ValidateUtil.isStatusBlocked(tmpParty.getStatus(), ErrorCode.SHIPMENT_DOCUMENT_CONSIGNEE_BLOCK);
            ValidateUtil.isStatusHidden(tmpParty.getStatus(), ErrorCode.SHIPMENT_DOCUMENT_CONSIGNEE_HIDE);
            ValidateUtil.isDefaulter(tmpParty, ErrorCode.SHIPMENT_DOC_CONSIGNEE_DEFAULTER);

            ValidateUtil.notNull(document.getConsigneeAddress(), ErrorCode.SHIPMENT_DOCUMENT_CONSIGNEE_ADDRESS);

            ValidateUtil.notNullOrEmpty(document.getConsigneeAddress().getAddressLine1(), ErrorCode.SHIPMENT_DOCUMENT_CONSIGNEE_ADDRESS_1);
            ValidateUtil.validateLength(document.getConsigneeAddress().getAddressLine1(), 1, 100, ErrorCode.SHIPMENT_DOCUMENT_CONSIGNEE_ADDRESS_1_LENGTH);

            if (document.getConsigneeAddress().getAddressLine2() != null && document.getConsigneeAddress().getAddressLine2().trim().length() != 0) {
                ValidateUtil.notNullOrEmpty(document.getConsigneeAddress().getAddressLine2(), ErrorCode.SHIPMENT_DOCUMENT_CONSIGNEE_ADDRESS_2);
                ValidateUtil.validateLength(document.getConsigneeAddress().getAddressLine2(), 1, 100, ErrorCode.SHIPMENT_DOCUMENT_CONSIGNEE_ADDRESS_2_LENGTH);
            }


            if (document.getConsigneeAddress().getAddressLine3() != null && document.getConsigneeAddress().getAddressLine3().trim().length() != 0) {
                ValidateUtil.validateLength(document.getConsigneeAddress().getAddressLine3(), 1, 100, ErrorCode.SHIPMENT_DOCUMENT_CONSIGNEE_ADDRESS_3_LENGTH);
            }


            if (document.getConsigneeAddress().getAddressLine3() == null || document.getConsigneeAddress().getAddressLine3() == "") {
                document.getConsigneeAddress().setAddressLine3(document.getConsigneeAddress().getAddressLine4());
                document.getConsigneeAddress().setAddressLine4(null);
            }
            //ValidateUtil.notNullOrEmpty(document.getConsigneeAddress().getAddressLine4(), ErrorCode.SHIPMENT_DOCUMENT_CONSIGNEE_ADDRESS_4);

            if (document.getConsigneeAddress().getAddressLine4() != null && document.getConsigneeAddress().getAddressLine4().trim().length() != 0) {
                ValidateUtil.validateLength(document.getConsigneeAddress().getAddressLine4(), 1, 100, ErrorCode.SHIPMENT_DOCUMENT_CONSIGNEE_ADDRESS_4_LENGTH);
            }

            //ValidateUtil.notNullOrEmpty(document.getConsigneeAddress().getEmail(), ErrorCode.SHIPMENT_DOCUMENT_CONSIGNEE_EMAIL);

            if (document.getConsigneeAddress().getEmail() != null) {
                ValidateUtil.validateLength(document.getConsigneeAddress().getEmail(), 1, 500, ErrorCode.SHIPMENT_DOCUMENT_CONSIGNEE_EMAIL_LENGTH);

                for (String email : document.getConsigneeAddress().getEmail().split(emailSplit)) {
                    if (!Pattern.compile(ValidateUtil.regularExp).matcher(email).matches()) {
                        throw new RestException(ErrorCode.SHIPMENT_DOCUMENT_CONSIGNEE_EMAIL_LENGTH);
                    }
                }
            }
            document.setConsigneeCode(tmpParty.getPartyCode());


            if (document.getConsigneeAddress().getCity() != null) {
                CityMaster city = cityMasterRepository.getOne(document.getConsigneeAddress().getCity().getId());
                document.getConsigneeAddress().setCityCode(city.getCityCode());
            }

            if (document.getConsigneeAddress().getState() != null) {
                StateMaster state = stateMasterRepository.getOne(document.getConsigneeAddress().getState().getId());
                document.getConsigneeAddress().setStateCode(state.getStateCode());
            }

            if (document.getConsigneeAddress().getCountry() != null) {
                CountryMaster country = countryMasterRepository.getOne(document.getConsigneeAddress().getCountry().getId());
                document.getConsigneeAddress().setCountryCode(country.getCountryCode());
            }
        } else {
            document.setConsignee(null);
            document.setConsigneeCode(null);
            document.setConsigneeAddress(null);
        }

    }

    private void verifyForwarder(DocumentDetail document, String emailSplit) {


        //ValidateUtil.notNull(document.getForwarder(), ErrorCode.SHIPMENT_DOCUMENT_FORWARDER);
        //ValidateUtil.notNull(document.getForwarder().getId(), ErrorCode.SHIPMENT_DOCUMENT_FORWARDER);
        //ValidateUtil.notNullOrEmpty(document.getForwarder().getPartyCode(), ErrorCode.SHIPMENT_DOCUMENT_FORWARDER);

        if (document.getForwarder() != null && document.getForwarder().getId() != null) {

            PartyMaster tmpParty = cacheRepository.get(SaaSUtil.getSaaSId(), PartyMaster.class.getName(), document.getForwarder().getId(), partyMasterRepository);


            ValidateUtil.isDefaulter(tmpParty, ErrorCode.SHIPMENT_DOC_FORW_DEFAULTER);
            ValidateUtil.isStatusBlocked(tmpParty.getStatus(), ErrorCode.SHIPMENT_DOCUMENT_FORWARDER_BLOCK);
            ValidateUtil.isStatusHidden(tmpParty.getStatus(), ErrorCode.SHIPMENT_DOCUMENT_FORWARDER_HIDE);


            ValidateUtil.notNull(document.getForwarderAddress(), ErrorCode.SHIPMENT_DOCUMENT_FORWARDER_ADDRESS);
            ValidateUtil.notNullOrEmpty(document.getForwarderAddress().getAddressLine1(), ErrorCode.SHIPMENT_DOCUMENT_FORWARDER_ADDRESS_1);
            ValidateUtil.validateLength(document.getForwarderAddress().getAddressLine1(), 1, 100, ErrorCode.SHIPMENT_DOCUMENT_FORWARDER_ADDRESS_1_LENGTH);

            if (document.getForwarderAddress().getAddressLine2() != null && document.getForwarderAddress().getAddressLine2().trim().length() != 0) {
                ValidateUtil.notNullOrEmpty(document.getForwarderAddress().getAddressLine2(), ErrorCode.SHIPMENT_DOCUMENT_FORWARDER_ADDRESS_2);
                ValidateUtil.validateLength(document.getForwarderAddress().getAddressLine2(), 1, 100, ErrorCode.SHIPMENT_DOCUMENT_FORWARDER_ADDRESS_2_LENGTH);
            }


            if (document.getForwarderAddress().getAddressLine3() != null && document.getForwarderAddress().getAddressLine3().trim().length() != 0) {
                ValidateUtil.validateLength(document.getForwarderAddress().getAddressLine3(), 1, 100, ErrorCode.SHIPMENT_DOCUMENT_FORWARDER_ADDRESS_3_LENGTH);
            }


            if (document.getForwarderAddress().getAddressLine3() == null || document.getForwarderAddress().getAddressLine3() == "") {
                document.getForwarderAddress().setAddressLine3(document.getForwarderAddress().getAddressLine4());
                document.getForwarderAddress().setAddressLine4(null);
            }

            if (document.getForwarderAddress().getAddressLine4() != null && document.getForwarderAddress().getAddressLine4().trim().length() != 0) {
                ValidateUtil.validateLength(document.getForwarderAddress().getAddressLine4(), 1, 100, ErrorCode.SHIPMENT_DOCUMENT_FORWARDER_ADDRESS_4_LENGTH);
            }


            if (document.getForwarderAddress().getEmail() != null && document.getForwarderAddress().getEmail().trim().length() != 0) {
                ValidateUtil.validateLength(document.getForwarderAddress().getEmail(), 1, 500, ErrorCode.SHIPMENT_DOCUMENT_FORWARDER_EMAIL_LENGTH);
                for (String email : document.getForwarderAddress().getEmail().split(emailSplit)) {
                    if (!Pattern.compile(ValidateUtil.regularExp).matcher(email).matches()) {
                        throw new RestException(ErrorCode.SHIPMENT_DOCUMENT_FORWARDER_EMAIL_LENGTH);
                    }
                }
            }
            document.setForwarderCode(tmpParty.getPartyCode());

            if (document.getForwarderAddress().getCity() != null) {
                CityMaster city = cityMasterRepository.getOne(document.getForwarderAddress().getCity().getId());
                document.getForwarderAddress().setCityCode(city.getCityCode());
            }

            if (document.getForwarderAddress().getState() != null) {
                StateMaster state = stateMasterRepository.getOne(document.getForwarderAddress().getState().getId());
                document.getForwarderAddress().setStateCode(state.getStateCode());
            }

            if (document.getForwarderAddress().getCountry() != null) {
                CountryMaster country = countryMasterRepository.getOne(document.getForwarderAddress().getCountry().getId());
                document.getForwarderAddress().setCountryCode(country.getCountryCode());
            }
        } else {
            document.setForwarderCode(null);
            document.setForwarderAddress(null);
            document.setForwarder(null);
        }
    }


    private void verifyShiper(DocumentDetail document, String emailSplit) {

        PartyMaster tmpParty = cacheRepository.get(SaaSUtil.getSaaSId(), PartyMaster.class.getName(), document.getShipper().getId(), partyMasterRepository);

        ValidateUtil.notNull(tmpParty, ErrorCode.SHIPMENT_DOCUMENT_SHIPPER);
        ValidateUtil.notNull(tmpParty.getId(), ErrorCode.SHIPMENT_DOCUMENT_SHIPPER);
        ValidateUtil.notNullOrEmpty(tmpParty.getPartyCode(), ErrorCode.SHIPMENT_DOCUMENT_SHIPPER);
        ValidateUtil.isStatusBlocked(tmpParty.getStatus(), ErrorCode.SHIPMENT_DOCUMENT_SHIPPER_BLOCK);
        ValidateUtil.isStatusHidden(tmpParty.getStatus(), ErrorCode.SHIPMENT_DOCUMENT_SHIPPER_HIDE);

        ValidateUtil.isDefaulter(tmpParty, ErrorCode.SHIPMENT_DOC_SHIPPER_DEFAULTER);

        ValidateUtil.notNull(document.getShipperAddress(), ErrorCode.SHIPMENT_DOCUMENT_SHIPPER_ADDRESS);

        ValidateUtil.notNullOrEmpty(document.getShipperAddress().getAddressLine1(), ErrorCode.SHIPMENT_DOCUMENT_SHIPPER_ADDRESS_1);
        ValidateUtil.validateLength(document.getShipperAddress().getAddressLine1(), 1, 100, ErrorCode.SHIPMENT_DOCUMENT_SHIPPER_ADDRESS_1_LENGTH);
        if (document.getShipperAddress().getAddressLine2() != null && document.getShipperAddress().getAddressLine2().trim().length() != 0) {
            ValidateUtil.notNullOrEmpty(document.getShipperAddress().getAddressLine2(), ErrorCode.SHIPMENT_DOCUMENT_SHIPPER_ADDRESS_2);
            ValidateUtil.validateLength(document.getShipperAddress().getAddressLine2(), 1, 100, ErrorCode.SHIPMENT_DOCUMENT_SHIPPER_ADDRESS_2_LENGTH);
        }


        if (document.getShipperAddress().getAddressLine3() != null && document.getShipperAddress().getAddressLine3().trim().length() != 0)
            ValidateUtil.validateLength(document.getShipperAddress().getAddressLine3(), 1, 100, ErrorCode.SHIPMENT_DOCUMENT_SHIPPER_ADDRESS_3_LENGTH);

        if (document.getShipperAddress().getAddressLine3() == null || document.getShipperAddress().getAddressLine3() == "") {
            document.getShipperAddress().setAddressLine3(document.getShipperAddress().getAddressLine4());
            document.getShipperAddress().setAddressLine4(null);
        }

        if (document.getShipperAddress().getAddressLine4() != null && document.getShipperAddress().getAddressLine4().trim().length() != 0)
            ValidateUtil.validateLength(document.getShipperAddress().getAddressLine4(), 1, 100, ErrorCode.SHIPMENT_DOCUMENT_SHIPPER_ADDRESS_4_LENGTH);


        if (document.getShipperAddress().getEmail() != null && document.getShipperAddress().getEmail().trim().length() != 0) {
            ValidateUtil.validateLength(document.getShipperAddress().getEmail(), 1, 500, ErrorCode.SHIPMENT_DOCUMENT_SHIPPER_EMAIL_LENGTH);

            for (String email : document.getShipperAddress().getEmail().split(emailSplit)) {
                if (!Pattern.compile(ValidateUtil.regularExp).matcher(email).matches()) {
                    throw new RestException(ErrorCode.SHIPMENT_DOCUMENT_SHIPPER_EMAIL_LENGTH);
                }
            }
        }
        document.setShipperCode(tmpParty.getPartyCode());

        if (document.getShipperAddress().getCity() != null) {
            CityMaster city = cityMasterRepository.getOne(document.getShipperAddress().getCity().getId());
            document.getShipperAddress().setCityCode(city.getCityCode());
        }

        if (document.getShipperAddress().getState() != null) {
            StateMaster state = stateMasterRepository.getOne(document.getShipperAddress().getState().getId());
            document.getShipperAddress().setStateCode(state.getStateCode());
        }

        if (document.getShipperAddress().getCountry() != null) {
            CountryMaster country = countryMasterRepository.getOne(document.getShipperAddress().getCountry().getId());
            document.getShipperAddress().setCountryCode(country.getCountryCode());
        }
    }

    private void verifyDate(DocumentDetail document) {
        ValidateUtil.notNull(document.getDocumentReqDate(), ErrorCode.SHIPMENT_DOCUMENT_DATEREQ);

    }

}
