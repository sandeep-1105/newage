package com.efreightsuite.validation;

import com.efreightsuite.enumeration.LovStatus;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.model.CharterMaster;
import com.efreightsuite.service.RegularExpressionService;
import com.efreightsuite.util.RegExpName;
import com.efreightsuite.util.ValidateUtil;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Log4j2
@Service
public class CharterMasterValidator {

    @Autowired
    private
    RegularExpressionService regExpService;


    public void validate(CharterMaster charterMaster) {


        ValidateUtil.notNull(charterMaster.getCharterCode(), ErrorCode.CHARTER_CODE_NOT_NULL);
        regExpService.validate(charterMaster.getCharterCode(), RegExpName.Reg_Exp_Master_Charter_Code, ErrorCode.CHARTER_CODE_INVALID);

        ValidateUtil.notNull(charterMaster.getCharterName(), ErrorCode.CHARTER_NAME_NOT_NULL);
        regExpService.validate(charterMaster.getCharterName(), RegExpName.Reg_Exp_Master_Charter_Name, ErrorCode.CHARTER_NAME_INVALID);


        ValidateUtil.notNull(charterMaster.getStatus(), ErrorCode.CHARTER_LOV_STATUS_NOT_NULL);

        ValidateUtil.validateEnum(LovStatus.class, charterMaster.getStatus(),
                ErrorCode.CHARTER_LOV_STATUS_INVALID);

    }
}
