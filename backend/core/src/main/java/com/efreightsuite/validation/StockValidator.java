package com.efreightsuite.validation;

import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.model.StockGeneration;
import com.efreightsuite.service.RegularExpressionService;
import com.efreightsuite.util.RegExpName;
import com.efreightsuite.util.ValidateUtil;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class StockValidator {

    @Autowired
    private
    RegularExpressionService regExpService;


    public void validateStock(StockGeneration stockGeneration) {

        log.info("stock validation starts");

        ValidateUtil.notNull(stockGeneration, ErrorCode.STOCK_GENERATION_MANDATORY);
        validateCarrier(stockGeneration);
        validateRecievedDDate(stockGeneration);
        validatePor(stockGeneration);
        validateStartingNo(stockGeneration);
        validateEndingNo(stockGeneration);
        vaildateRemaindNo(stockGeneration);
        validateCarrierRefNo(stockGeneration);

    }

    private void validateCarrierRefNo(StockGeneration stockGeneration) {
        if (stockGeneration.getCarrierRefNo() != null && stockGeneration.getCarrierRefNo() != "")
            //ValidateUtil.checkPattern(stockGeneration.getCarrierRefNo(),"carrierRefNo", true);
            regExpService.validate(stockGeneration.getCarrierRefNo(), RegExpName.Reg_Exp_STOCK_CARRIER_REF_NO, ErrorCode.STOCK_CARRIER_REFNO_INVALID);

    }

    private void vaildateRemaindNo(StockGeneration stockGeneration) {


        if (stockGeneration.getRemindNo() != null && stockGeneration.getRemindNo() != 0) {

            if (stockGeneration.getRemindNo() > 9999999) {

                throw new RestException(ErrorCode.STOCK_REMAIND_NO_RANGE_INVALID);
            }

            if (stockGeneration.getEndingNo() != 0 && stockGeneration.getStartingNo() != 0) {
                if (stockGeneration.getRemindNo() < stockGeneration.getStartingNo() || stockGeneration.getRemindNo() > stockGeneration.getEndingNo()) {
                    throw new RestException(ErrorCode.STOCK_REMAIND_NO_RANGE_INVALID);

                }
            }

        }

    }


    private void validateStartingNo(StockGeneration stockGeneration) {
        ValidateUtil.nonZero(stockGeneration.getStartingNo(), ErrorCode.STOCK_STARTING_NO_NONZERO);
        ValidateUtil.nonZero(stockGeneration.getEndingNo(), ErrorCode.STOCK_ENDING_NO_NONZERO);

        if (stockGeneration.getStartingNo() > 9999999) {
            throw new RestException(ErrorCode.STOCK_STARTING_NO_INVALID);
        }
        if (stockGeneration.getStartingNo() > stockGeneration.getEndingNo()) {

            throw new RestException(ErrorCode.STOCK_ENDING_NO_GREATER);
        }

    }

    private void validateEndingNo(StockGeneration stockGeneration) {
        ValidateUtil.nonZero(stockGeneration.getEndingNo(), ErrorCode.STOCK_ENDING_NO_NONZERO);
        ValidateUtil.nonZero(stockGeneration.getStartingNo(), ErrorCode.STOCK_STARTING_NO_NONZERO);
        if (stockGeneration.getEndingNo() > 9999999) {
            throw new RestException(ErrorCode.STOCK_ENDING_NO_INVALID);
        }
        if (stockGeneration.getStartingNo() > stockGeneration.getEndingNo()) {

            throw new RestException(ErrorCode.STOCK_ENDING_NO_GREATER);
        }
    }


    private void validatePor(StockGeneration stockGeneration) {

        ValidateUtil.notNull(stockGeneration.getPor(), ErrorCode.STOCK_POR_CODE_MANDATORY);
        ValidateUtil.notNull(stockGeneration.getPor().getId(), ErrorCode.STOCK_POR_CODE_MANDATORY);
        ValidateUtil.notNullOrEmpty(stockGeneration.getPor().getPortCode(), ErrorCode.STOCK_POR_CODE_MANDATORY);
        ValidateUtil.isStatusBlocked(stockGeneration.getPor().getStatus(), ErrorCode.STOCK_POR_CODE_BLOCK);
        ValidateUtil.isStatusHidden(stockGeneration.getPor().getStatus(), ErrorCode.STOCK_POR_CODE_HIDE);
        stockGeneration.setPortCode(stockGeneration.getPor().getPortCode());
    }

    private void validateRecievedDDate(StockGeneration stockGeneration) {
        ValidateUtil.notNull(stockGeneration.getReceivedOn(), ErrorCode.STOCK_REC_DATE_MANDATORY);
    }


    private void validateCarrier(StockGeneration stockGeneration) {

        ValidateUtil.notNull(stockGeneration.getCarrier(), ErrorCode.STOCK_CARRIER_NAME_MANDATORY);
        ValidateUtil.notNull(stockGeneration.getCarrier().getId(), ErrorCode.STOCK_CARRIER_NAME_MANDATORY);
        ValidateUtil.notNullOrEmpty(stockGeneration.getCarrier().getCarrierNo(), ErrorCode.STOCK_CARRIER_NAME_MANDATORY);
        ValidateUtil.isStatusBlocked(stockGeneration.getCarrier().getStatus(), ErrorCode.STOCK_CARRIER_NAME_BLOCK);
        ValidateUtil.isStatusHidden(stockGeneration.getCarrier().getStatus(), ErrorCode.STOCK_CARRIER_NAME_HIDE);

    }


}
