package com.efreightsuite.validation;

import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.model.ShipmentServiceReference;
import com.efreightsuite.service.RegularExpressionService;
import com.efreightsuite.util.ValidateUtil;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class ShipmentServiceReferenceValidator {

    @Autowired
    RegularExpressionService regExpService;

    public void validate(ShipmentServiceReference reference) {

        ValidateUtil.notNull(reference.getReferenceTypeMaster(), ErrorCode.SHIPMENT_REFERENCE_NOT_NULL);

        ValidateUtil.notNull(reference.getReferenceTypeMaster().getId(), ErrorCode.SHIPMENT_REFERENCE_NOT_NULL);

        ValidateUtil.isStatusBlocked(reference.getReferenceTypeMaster().getStatus(), ErrorCode.SHIPMENT_REFERENCE_BLOCKED);

        ValidateUtil.isStatusHidden(reference.getReferenceTypeMaster().getStatus(), ErrorCode.SHIPMENT_REFERENCE_HIDDEN);

        reference.setReferenceTypeCode(reference.getReferenceTypeMaster().getReferenceTypeCode());

        ValidateUtil.notNullOrEmpty(reference.getReferenceNumber(), ErrorCode.SHIPMENT_REFERENCE_NUMBER_EMPTY);

        ValidateUtil.validateLength(reference.getReferenceNumber(), 1, 30, ErrorCode.SHIPMENT_REFERENCE_NUMBER_INVALID);

        ValidateUtil.validateLength(reference.getReferenceNotes(), 0, 4000, ErrorCode.SHIPMENT_REFERENCE_NOTES_INVALID);

    }
}
