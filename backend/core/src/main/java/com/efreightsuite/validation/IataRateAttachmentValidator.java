package com.efreightsuite.validation;

import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.model.IataRateAttachment;
import com.efreightsuite.repository.DefaultMasterDataRepository;
import com.efreightsuite.service.AuthService;
import com.efreightsuite.util.AppUtil;
import com.efreightsuite.util.ValidateUtil;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class IataRateAttachmentValidator {

    @Autowired
    private
    AppUtil appUtil;

    @Autowired
    DefaultMasterDataRepository defaultMasterDataRepository;

    public void validate(IataRateAttachment iataRateAttachment) {

        log.info("Enquiry Attachment Validator begins......");

        ValidateUtil.notNullOrEmpty(iataRateAttachment.getRefNo(), ErrorCode.ENQUIRY_ATTACHMENT_REFNO_NOT_NULL);

        ValidateUtil.checkPattern(iataRateAttachment.getRefNo(), "enquiry.attachment.refno", true);

        ValidateUtil.notNullOrEmpty(iataRateAttachment.getFileName(), ErrorCode.ENQUIRY_ATTACHMENT_FILE_NAME_NOT_NULL);

        if (iataRateAttachment.getId() == null) {

            ValidateUtil.notNull(iataRateAttachment.getFile(), ErrorCode.ENQUIRY_ATTACHMENT_FILE_NOT_NULL);

            String key = appUtil.getLocationConfig("upload.file.size.in.mb", AuthService.getCurrentUser().getSelectedUserLocation(), false);


            if (key != null && key.trim().length() != 0) {

                long uploadedFileSize = iataRateAttachment.getFile().length;

                long allowedSize = Long.parseLong(key) * 1024 * 1024;

                if (uploadedFileSize == 0) {

                    throw new RestException(ErrorCode.ENQUIRY_ATTACHMENT_FILE_EMPTY);

                }

                if (uploadedFileSize > allowedSize) {

                    throw new RestException(ErrorCode.ENQUIRY_ATTACHMENT_FILE_SIZE_EXCEED);

                }

            }
        }

        log.info("Enquiry Attachment Validator begins......");
    }

}
