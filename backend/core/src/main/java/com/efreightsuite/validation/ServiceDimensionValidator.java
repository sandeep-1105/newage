package com.efreightsuite.validation;

import com.efreightsuite.enumeration.DimensionUnit;
import com.efreightsuite.enumeration.YesNo;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.model.DocumentDetail;
import com.efreightsuite.model.ServiceDocumentDimension;
import com.efreightsuite.model.Shipment;
import com.efreightsuite.model.ShipmentServiceDetail;
import com.efreightsuite.repository.DefaultMasterDataRepository;
import com.efreightsuite.util.AppUtil;
import com.efreightsuite.util.ValidateUtil;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class ServiceDimensionValidator {

    @Autowired
    private
    AppUtil appUtil;

    @Autowired
    DefaultMasterDataRepository defaultMasterDataRepository;

    public void validate(Shipment shipment, DocumentDetail document, ServiceDocumentDimension dimension, ShipmentServiceDetail sd) {

        log.info("ServiceDimensionValidator Validation Started......");

        verifyPieces(dimension);
        verifyLength(dimension, document, shipment, sd);
        verifyWidth(dimension, document, shipment, sd);
        verifyHeight(dimension, document, shipment, sd);
        verifyVolumeWeight(dimension);
        verifyGrossWeight(dimension);
        log.info("ServiceDimensionValidator Validation Finished......");
    }

    private void verifyPieces(ServiceDocumentDimension dimension) {

        ValidateUtil.notNull(dimension.getNoOfPiece(), ErrorCode.SHIPMENT_SERVICE_DIMENSION_PIECES_REQUIRED);

        if (dimension.getNoOfPiece() < 0 || dimension.getNoOfPiece() > 1000000000d) {
            throw new RestException(ErrorCode.SHIPMENT_SERVICE_DIMENSION_PIECES_REQUIRED);
        }

    }

    private void verifyLength(ServiceDocumentDimension dimension, DocumentDetail document, Shipment shipment, ShipmentServiceDetail service) {

        ValidateUtil.notNull(dimension.getLength(), ErrorCode.SHIPMENT_SERVICE_DIMENSION_LENGTH_REQUIRED);

        if (dimension.getLength() < 0 || dimension.getLength() > 100000d) {
            throw new RestException(ErrorCode.SHIPMENT_SERVICE_DIMENSION_LENGTH_REQUIRED);
        }

        if (document.getDimensionUnit() == DimensionUnit.CENTIMETERORKILOS) {
            String key = appUtil.getLocationConfig("Centimeter_Length", service.getLocation(), false);
            Long val = Long.parseLong(key);

            if (dimension.getLength() > val) {
                service.setOverDimension(YesNo.Yes);
            }

        } else {
            String key = appUtil.getLocationConfig("Inch_Length", service.getLocation(), false);
            Long val = Long.parseLong(key);
            if (dimension.getLength() > val) {
                service.setOverDimension(YesNo.Yes);
            }
        }

    }

    private void verifyWidth(ServiceDocumentDimension dimension, DocumentDetail document, Shipment shipment, ShipmentServiceDetail service) {

        ValidateUtil.notNull(dimension.getWidth(), ErrorCode.SHIPMENT_SERVICE_DIMENSION_WIDTH_REQUIRED);

        if (dimension.getWidth() < 0 || dimension.getWidth() > 100000d) {
            throw new RestException(ErrorCode.SHIPMENT_SERVICE_DIMENSION_WIDTH_REQUIRED);
        }

        if (document.getDimensionUnit() == DimensionUnit.CENTIMETERORKILOS) {
            String key = appUtil.getLocationConfig("Centimeter_Width", service.getLocation(), false);
            Long val = Long.parseLong(key);

            if (dimension.getWidth() > val) {
                service.setOverDimension(YesNo.Yes);
            }

        } else {
            String key = appUtil.getLocationConfig("Inch_Width", service.getLocation(), false);
            Long val = Long.parseLong(key);
            if (dimension.getWidth() > val) {
                service.setOverDimension(YesNo.Yes);
            }
        }

    }

    private void verifyHeight(ServiceDocumentDimension dimension, DocumentDetail document, Shipment shipment, ShipmentServiceDetail service) {

        ValidateUtil.notNull(dimension.getHeight(), ErrorCode.SHIPMENT_SERVICE_DIMENSION_HEIGHT_REQUIRED);

        if (dimension.getHeight() < 0 || dimension.getHeight() > 100000d) {
            throw new RestException(ErrorCode.SHIPMENT_SERVICE_DIMENSION_HEIGHT_REQUIRED);
        }

        if (document.getDimensionUnit() == DimensionUnit.CENTIMETERORKILOS) {
            String key = appUtil.getLocationConfig("Centimeter_height", service.getLocation(), false);
            Long val = Long.parseLong(key);

            if (dimension.getHeight() > val) {
                service.setOverDimension(YesNo.Yes);
            }

        } else {
            String key = appUtil.getLocationConfig("Inch_height", service.getLocation(), false);
            Long val = Long.parseLong(key);
            if (dimension.getHeight() > val) {
                service.setOverDimension(YesNo.Yes);
            }
        }

    }

    private void verifyVolumeWeight(ServiceDocumentDimension dimension) {

        ValidateUtil.notNull(dimension.getVolWeight(), ErrorCode.SHIPMENT_SERVICE_DIMENSION_VOLUME_WEIGHT_REQUIRED);

        if (dimension.getVolWeight() < 0 || dimension.getVolWeight() > 100000000000d) {
            throw new RestException(ErrorCode.SHIPMENT_SERVICE_DIMENSION_VOLUME_WEIGHT_REQUIRED);
        }

    }

    private void verifyGrossWeight(ServiceDocumentDimension dimension) {

        ValidateUtil.notNull(dimension.getGrossWeight(), ErrorCode.SHIPMENT_SERVICE_DIMENSION_GROSS_WEIGHT_REQUIRED);

        if (dimension.getGrossWeight() < 0 || dimension.getVolWeight() > 100000000000d) {
            throw new RestException(ErrorCode.SHIPMENT_SERVICE_DIMENSION_GROSS_WEIGHT_REQUIRED);
        }

    }

}
