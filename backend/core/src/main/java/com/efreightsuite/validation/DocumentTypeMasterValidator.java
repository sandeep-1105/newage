package com.efreightsuite.validation;

import com.efreightsuite.enumeration.LovStatus;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.model.DocumentTypeMaster;
import com.efreightsuite.util.ValidateUtil;

import org.springframework.stereotype.Service;


@Service
public class DocumentTypeMasterValidator {

    public void validate(DocumentTypeMaster documentTypeMaster) {

        ValidateUtil.notNull(documentTypeMaster.getDocumentTypeCode(), ErrorCode.DOCUMENT_TYPE_CODE_NOT_NULL);

        ValidateUtil.notNull(documentTypeMaster.getDocumentTypeName(), ErrorCode.DOCUMENT_TYPE_NAME_NOT_NULL);

        ValidateUtil.notNull(documentTypeMaster.getStatus(), ErrorCode.DOCUMENT_TYPE_LOV_STATUS_NOT_NULL);

        ValidateUtil.validateEnum(LovStatus.class, documentTypeMaster.getStatus(), ErrorCode.DOCUMENT_TYPE_LOV_STATUS_INVALID);

    }
}
