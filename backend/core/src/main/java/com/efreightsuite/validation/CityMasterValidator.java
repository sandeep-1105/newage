package com.efreightsuite.validation;

import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.model.CityMaster;
import com.efreightsuite.util.ValidateUtil;
import lombok.extern.log4j.Log4j2;

import org.springframework.stereotype.Service;

@Service
@Log4j2
public class CityMasterValidator {

    public void validate(CityMaster cityMaster) {
        log.info("City Master Validation Started....[" + (cityMaster.getId() != null ? cityMaster.getId() : "") + "]");

        ValidateUtil.notNull(cityMaster, ErrorCode.CITY_NOT_NULL);

        ValidateUtil.notNull(cityMaster.getCityCode(), ErrorCode.CITY_CODE_NOT_NULL);
        ValidateUtil.validateLength(cityMaster.getCityCode(), 0, 10, ErrorCode.CITY_CODE_INVALID);
        cityMaster.setCityCode(cityMaster.getCityCode().toUpperCase().trim());

        ValidateUtil.notNull(cityMaster.getCityName(), ErrorCode.CITY_NAME_NOT_NULL);
        ValidateUtil.validateLength(cityMaster.getCityName(), 0, 100, ErrorCode.CITY_NAME_INVALID);


        ValidateUtil.notNull(cityMaster.getCountryMaster(), ErrorCode.CITY_COUNTRY_NOT_NULL);
        ValidateUtil.notNull(cityMaster.getCountryMaster().getId(), ErrorCode.CITY_COUNTRY_CODE_NULL);
        ValidateUtil.isStatusBlocked(cityMaster.getCountryMaster().getStatus(), ErrorCode.CITY_COUNTRY_INVALID);
        ValidateUtil.isStatusHidden(cityMaster.getCountryMaster().getStatus(), ErrorCode.CITY_COUNTRY_INVALID);


        ValidateUtil.notNull(cityMaster.getStateMaster(), ErrorCode.CITY_STATE_NOT_NULL);
        ValidateUtil.notNull(cityMaster.getStateMaster().getId(), ErrorCode.CITY_STATE_NOT_NULL);
        ValidateUtil.isStatusBlocked(cityMaster.getStateMaster().getStatus(), ErrorCode.CITY_STATE_BLOCKED);
        ValidateUtil.isStatusHidden(cityMaster.getStateMaster().getStatus(), ErrorCode.CITY_STATE_HIDDEN);


        log.info("City Master Validation Finished....[" + (cityMaster.getId() != null ? cityMaster.getId() : "") + "]");
    }

}
