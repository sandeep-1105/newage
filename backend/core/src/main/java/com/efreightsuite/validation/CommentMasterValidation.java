package com.efreightsuite.validation;

import com.efreightsuite.enumeration.LovStatus;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.model.CommentMaster;
import com.efreightsuite.service.RegularExpressionService;
import com.efreightsuite.util.RegExpName;
import com.efreightsuite.util.ValidateUtil;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Log4j2
@Service
public class CommentMasterValidation {

    @Autowired
    private
    RegularExpressionService regularExpressionService;

    public void validate(CommentMaster commentMaster) {

        log.info("CommentMasterValidator Started for CommentMaster : [" + commentMaster.getId() + "]");

        // Reason Code Validation

        ValidateUtil.notNull(commentMaster.getCommentCode(), ErrorCode.REASON_CODE_NOT_NULL);

        regularExpressionService.validate(commentMaster.getCommentCode(), RegExpName.Reg_Exp_Master_Comment_Code, ErrorCode.COMMENT_CODE_INVALID);

        log.info("CommentMaster Code validated...");

        // Reason Name Validations

        ValidateUtil.notNull(commentMaster.getCommentName(), ErrorCode.REASON_NAME_NOT_NULL);

        regularExpressionService.validate(commentMaster.getCommentCode(), RegExpName.Reg_Exp_Master_Comment_Name, ErrorCode.COMMENT_NAME_INVALID);

        //ValidateUtil.checkPattern(reasonMaster.getReasonName(), "reasonName", true);

        log.info("CommentMaster Name validated...");

        if (commentMaster.getStatus() == null) {
            commentMaster.setStatus(LovStatus.Active);
        }

        ValidateUtil.notNull(commentMaster.getStatus(), ErrorCode.REASON_LOV_STATUS_NOT_NULL);

        ValidateUtil.validateEnum(LovStatus.class, commentMaster.getStatus(),
                ErrorCode.REASON_LOV_STATUS_INVALID);

        ValidateUtil.notNull(commentMaster.getDescription(), ErrorCode.REASON_LOV_STATUS_NOT_NULL);

        log.info("CommentMaster Status Successfully Validated.....");

        log.info("CommentMasterValidator Finished for CommentMaster : [" + commentMaster.getId() + "]");
    }

}
