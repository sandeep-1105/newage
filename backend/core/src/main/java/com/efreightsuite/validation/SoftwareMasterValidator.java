package com.efreightsuite.validation;

import com.efreightsuite.enumeration.LovStatus;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.model.SoftwareMaster;
import com.efreightsuite.util.ValidateUtil;
import lombok.extern.log4j.Log4j2;

import org.springframework.stereotype.Service;

@Service
@Log4j2
public class SoftwareMasterValidator {

    public void validate(SoftwareMaster softwareMaster) {
        log.info("Software Master Validation Started....[" + (softwareMaster.getId() != null ? softwareMaster.getId() : "") + "]");

        ValidateUtil.notNull(softwareMaster.getSoftwareName(), ErrorCode.SOFTWARE_NAME_NOT_NULL);
        ValidateUtil.notNull(softwareMaster.getSoftwareCode(), ErrorCode.SOFTWARE_CODE_NOT_NULL);

        if (softwareMaster.getStatus() == null) {
            softwareMaster.setStatus(LovStatus.Active);
        }

        log.info("Software Master Validation Finished....[" + (softwareMaster.getId() != null ? softwareMaster.getId() : "") + "]");
    }
}
