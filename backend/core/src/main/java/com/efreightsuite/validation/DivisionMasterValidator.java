package com.efreightsuite.validation;

import com.efreightsuite.enumeration.LovStatus;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.model.DivisionMaster;
import com.efreightsuite.util.ValidateUtil;
import lombok.extern.log4j.Log4j2;

import org.springframework.stereotype.Service;

@Service
@Log4j2
public class DivisionMasterValidator {

    public void validate(DivisionMaster divisionMaster) {

        log.info("Division Master Validator Started..... Division ID : [" + divisionMaster.getId() + "]");

        ValidateUtil.notNull(divisionMaster.getDivisionCode(), ErrorCode.DIVISION_CODE_NOT_NULL);
        divisionMaster.setDivisionCode(divisionMaster.getDivisionCode().trim().toUpperCase());

        ValidateUtil.notNull(divisionMaster.getDivisionName(), ErrorCode.DIVISION_NAME_NOT_NULL);
        divisionMaster.setDivisionName(divisionMaster.getDivisionName().trim());

        ValidateUtil.notNull(divisionMaster.getCompanyMaster(), ErrorCode.DIVISION_COMPANY__NOT_NULL);
        ValidateUtil.isStatusBlocked(divisionMaster.getCompanyMaster().getStatus(), ErrorCode.COMPANY_STATUS_BLOCKED);
        ValidateUtil.isStatusHidden(divisionMaster.getCompanyMaster().getStatus(), ErrorCode.COMPANY_STATUS_HIDDEN);
        if (divisionMaster.getStatus() == null) {
            divisionMaster.setStatus(LovStatus.Active);
        }

        log.info("Division Master Validator Finished..... Division  ID : [" + divisionMaster.getId() + "]");


    }
}
