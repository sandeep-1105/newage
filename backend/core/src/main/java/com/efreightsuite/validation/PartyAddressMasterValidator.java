package com.efreightsuite.validation;

import com.efreightsuite.enumeration.LovStatus;
import com.efreightsuite.enumeration.YesNo;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.model.PartyAddressMaster;
import com.efreightsuite.repository.DefaultMasterDataRepository;
import com.efreightsuite.service.RegularExpressionService;
import com.efreightsuite.util.RegExpName;
import com.efreightsuite.util.ValidateUtil;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class PartyAddressMasterValidator {

    @Autowired
    private
    RegularExpressionService regExpService;


    @Autowired
    StateMasterValidator stateMasterValidator;

    @Autowired
    CityMasterValidator cityMasterValidator;

    @Autowired
    private
    PartyCountryFieldValidator partyCountryFieldValidator;

    @Autowired
    DefaultMasterDataRepository defaultMasterDataRepository;


    public void validate(PartyAddressMaster partyAddressMaster) {

        log.info("Party Address Validation Started....");

        verifyPoBoxNumber(partyAddressMaster);
        verifyAddressLine1(partyAddressMaster);
        verifyAddressLine2(partyAddressMaster);
        verifyAddressLine3(partyAddressMaster);
        verifyZipCode(partyAddressMaster);
        verifyCity(partyAddressMaster);
        verifyState(partyAddressMaster);


        verifyPhoneNumber(partyAddressMaster);
        verifyMobileNumber(partyAddressMaster);
        verifyFax(partyAddressMaster);
        verifyEmail(partyAddressMaster);
        verifyContactPerson(partyAddressMaster);

        verifyPickUpHourText(partyAddressMaster);
        if (partyAddressMaster.getPartyCountryField() != null) {
            verifyIsCorporateOffice(partyAddressMaster);
        }


        if (partyAddressMaster.getPartyCountryField() != null) {
            partyCountryFieldValidator.validate(partyAddressMaster.getPartyCountryField());
        }

        log.info("Party Address Validation Finished....");
    }

    private void verifyPickUpHourText(PartyAddressMaster partyAddressMaster) {

        if (partyAddressMaster.getPickupHoursText() != null && partyAddressMaster.getPickupHoursText().trim().length() != 0) {
            regExpService.validate(partyAddressMaster.getPickupHoursText(), RegExpName.Reg_Exp_Master_PARTY_ADD_PICKUP_HOURS, ErrorCode.PARTY_PICK_HOUR_TEXT_INVALID);
        }

        log.info("Pick Up Hour Text is validated......................................" + partyAddressMaster.getPickupHoursText());

    }

    public void verifyPoBoxNumber(PartyAddressMaster partyAddressMaster) {
        log.info("Inside verifyPoBoxNumber", partyAddressMaster);
    }

    public void verifyAddressLine1(PartyAddressMaster partyAddressMaster) {
        ValidateUtil.notNull(partyAddressMaster.getAddressLine1(), ErrorCode.PARTY_ADDRESS_LINE1_REQUIRED);
        ValidateUtil.validateLength(partyAddressMaster.getAddressLine1(), 0, 100, ErrorCode.PARTY_ADDRESS_LINE1_INVALID);
    }

    public void verifyAddressLine2(PartyAddressMaster partyAddressMaster) {

        if (partyAddressMaster.getAddressLine2() != null && partyAddressMaster.getAddressLine2().trim().length() != 0) {
            ValidateUtil.validateLength(partyAddressMaster.getAddressLine2(), 0, 100, ErrorCode.PARTY_ADDRESS_LINE2_INVALID);
        }
    }

    public void verifyAddressLine3(PartyAddressMaster partyAddressMaster) {
        if (partyAddressMaster.getAddressLine3() != null && partyAddressMaster.getAddressLine3().trim().length() != 0) {
            ValidateUtil.validateLength(partyAddressMaster.getAddressLine3(), 0, 100, ErrorCode.PARTY_ADDRESS_LINE3_INVALID);
        }
    }

    private void verifyState(PartyAddressMaster partyAddressMaster) {
        if (partyAddressMaster.getStateMaster() != null && partyAddressMaster.getStateMaster().getId() != null) {

            ValidateUtil.notNull(partyAddressMaster.getStateMaster(), ErrorCode.PARTY_STATE_REQUIRED);

            if (partyAddressMaster.getStateMaster().getStatus() == LovStatus.Block) {
                throw new RestException(ErrorCode.PARTY_BLOCKED_STATE_SELECTED);
            }

            if (partyAddressMaster.getStateMaster().getStatus() == LovStatus.Hide) {
                throw new RestException(ErrorCode.PARTY_HIDDEN_STATE_SELECTED);
            }
            partyAddressMaster.setStateMasterCode(partyAddressMaster.getStateMaster().getStateCode());
        } else {
            partyAddressMaster.setStateMaster(null);
            partyAddressMaster.setStateMasterCode(null);
        }
    }

    private void verifyCity(PartyAddressMaster partyAddressMaster) {
        if (partyAddressMaster.getCityMaster() != null) {
            partyAddressMaster.setCityMasterCode(partyAddressMaster.getCityMaster().getCityCode());
        } else {
            partyAddressMaster.setCityMasterCode(null);
        }
    }

    public void verifyZipCode(PartyAddressMaster partyAddressMaster) {
        if (partyAddressMaster.getZipCode() != null && partyAddressMaster.getZipCode().trim().length() != 0) {
            regExpService.validate(partyAddressMaster.getZipCode(), RegExpName.Reg_Exp_Master_PARTY_ZIP_CODE, ErrorCode.PARTY_ZIP_CODE_INVALID);
        }
    }

    public void verifyPhoneNumber(PartyAddressMaster partyAddressMaster) {
        if (partyAddressMaster.getPhone() != null && partyAddressMaster.getPhone().trim().length() != 0) {
            regExpService.validate(partyAddressMaster.getPhone(), RegExpName.REG_EXP_PHONE_NUMBER, ErrorCode.PARTY_PHONE_NUMBER_INVALID);

        }
        log.info("PartyAddressMaster PhoneNumber Validated......................................");
    }

    public void verifyMobileNumber(PartyAddressMaster partyAddressMaster) {
        if (partyAddressMaster.getMobileNo() != null && partyAddressMaster.getMobileNo().trim().length() != 0) {
            regExpService.validate(partyAddressMaster.getMobileNo(), RegExpName.REG_EXP_MOBILE_NUMBER, ErrorCode.PARTY_MOBILE_NUMBER_INVALID);
        }
        log.info("PartyAddressMaster MobileNumber Validated......................................");
    }

    public void verifyFax(PartyAddressMaster partyAddressMaster) {
        if (partyAddressMaster.getFax() != null && partyAddressMaster.getFax().trim().length() != 0) {

            ValidateUtil.validateLength(partyAddressMaster.getFax(), 0, 100, ErrorCode.PARTY_FAX_INVALID);

        }
        log.info("PartyAddressMaster Fax Validated......................................");
    }

    private void verifyEmail(PartyAddressMaster partyAddressMaster) {/*
        if(partyAddressMaster.getEmail()!=null && partyAddressMaster.getEmail().trim().length()!=0) {

			for (String email: partyAddressMaster.getEmail().split(defaultMasterDataRepository.findByCode("single.text.split.by.multiple.email").getValue())){
				regExpService.validate(email, RegExpName.REG_EXP_MULTIPLE_EMAIL, ErrorCode.PARTY_EMAIL_INVALID);
				//ValidateUtil.checkPattern(email, "partyAddressMaster.email", true);
		      }
			
		}
		log.info("PartyAddressMaster Email Validated......................................");
	*/
    }

    public void verifyContactPerson(PartyAddressMaster partyAddressMaster) {

        if (partyAddressMaster.getContactPerson() != null && partyAddressMaster.getContactPerson().trim().length() != 0) {
            regExpService.validate(partyAddressMaster.getContactPerson(), RegExpName.Reg_Exp_Master_PARTY_CONTACT_PERSON, ErrorCode.PARTY_CONTACT_PERSON_INVALID);
        } else {
            partyAddressMaster.setContactPerson(null);
        }

    }

    private void verifyIsCorporateOffice(PartyAddressMaster partyAddressMaster) {
        ValidateUtil.validateEnum(YesNo.class, partyAddressMaster.getCorporate(), ErrorCode.PARTY_IS_CORPORATE_OFFICE);
        log.info("PartyAddressMaster Is Corporate or Not Validated......................................");
    }


}
