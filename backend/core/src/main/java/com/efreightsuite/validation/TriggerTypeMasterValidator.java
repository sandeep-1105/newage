package com.efreightsuite.validation;

import com.efreightsuite.enumeration.LovStatus;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.model.TriggerTypeMaster;
import com.efreightsuite.service.RegularExpressionService;
import com.efreightsuite.util.RegExpName;
import com.efreightsuite.util.ValidateUtil;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class TriggerTypeMasterValidator {

    @Autowired
    private
    RegularExpressionService regExpService;

    public void validate(TriggerTypeMaster triggerTypeMaster) {

        log.info("TriggerTypeMasterValidator Validation Started....");

        validateTriggerTypeMasterCode(triggerTypeMaster);

        validateTriggerTypeMasterName(triggerTypeMaster);

        if (triggerTypeMaster.getStatus() == null) {
            triggerTypeMaster.setStatus(LovStatus.Active);
        }

        ValidateUtil.validateEnum(LovStatus.class, triggerTypeMaster.getStatus(), ErrorCode.TRIGGER_TYPE_STATUS_NOT_NULL);
    }

    private void validateTriggerTypeMasterCode(TriggerTypeMaster triggerTypeMaster) {

        ValidateUtil.notNullOrEmpty(triggerTypeMaster.getTriggerTypeCode(), ErrorCode.TRIGGER_TYPE_CODE_NOT_NULL);

        ValidateUtil.validateLength(triggerTypeMaster.getTriggerTypeCode(), 1, 10, ErrorCode.TRIGGER_TYPE_CODE_LENGTH_EXCEED);

        regExpService.validate(triggerTypeMaster.getTriggerTypeCode(), RegExpName.Reg_Exp_TRIGGER_TYPE_CODE, ErrorCode.TRIGGER_TYPE_CODE_INVALID);

    }

    private void validateTriggerTypeMasterName(TriggerTypeMaster triggerTypeMaster) {

        ValidateUtil.notNullOrEmpty(triggerTypeMaster.getTriggerTypeName(), ErrorCode.TRIGGER_TYPE_NAME_NOT_NULL);

        ValidateUtil.validateLength(triggerTypeMaster.getTriggerTypeName(), 1, 100, ErrorCode.TRIGGER_TYPE_NAME_LENGTH_EXCEED);

        regExpService.validate(triggerTypeMaster.getTriggerTypeName(), RegExpName.Reg_Exp_TRIGGER_TYPE_NAME, ErrorCode.TRIGGER_TYPE_NAME_INVALID);

    }

}
