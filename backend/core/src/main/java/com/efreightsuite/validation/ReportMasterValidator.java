package com.efreightsuite.validation;

import com.efreightsuite.enumeration.YesNo;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.model.ReportMaster;
import com.efreightsuite.util.ValidateUtil;
import lombok.extern.log4j.Log4j2;

import org.springframework.stereotype.Service;

@Log4j2
@Service
public class ReportMasterValidator {

    public void validate(ReportMaster reportMaster) {

        log.info("ReportMasterValidator Started for Report : [" + reportMaster.getId() + "]");

        // Report Code Validation

        ValidateUtil.notNull(reportMaster.getReportMasterCode(), ErrorCode.REPORT_CODE_NOT_NULL);

        log.info("Report Code validated...");

        // Report Name Validations

        ValidateUtil.notNull(reportMaster.getReportMasterName(), ErrorCode.REPORT_NAME_NOT_NULL);

        //ValidateUtil.checkPattern(reportMaster.getReportName(), "reportName", true);

        log.info("Report Name validated...");

        if (reportMaster.getIsReportEnable() == null) {
            reportMaster.setIsReportEnable(YesNo.Yes);
        }

        ValidateUtil.notNull(reportMaster.getIsReportEnable(), ErrorCode.REPORT_ISREPORTENABLE_NOTNULL);

        ValidateUtil.validateEnum(YesNo.class, reportMaster.getIsReportEnable(),
                ErrorCode.REPORT_ISREPORTENABLE_INVALID);

        log.info("Report Status Successfully Validated.....");

        log.info("ReportMasterValidator Finished for Report : [" + reportMaster.getId() + "]");
    }

}
