package com.efreightsuite.validation;

import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.model.CfsAttachment;
import com.efreightsuite.model.CfsDimension;
import com.efreightsuite.model.CfsReceiveEntry;
import com.efreightsuite.service.RegularExpressionService;
import com.efreightsuite.util.RegExpName;
import com.efreightsuite.util.ValidateUtil;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class CfsReceiveEntryValidator {

    @Autowired
    private
    CfsDimensionValidator cfsDimensionValidator;

    @Autowired
    private
    CfsAttachmentValidator cfsAttachmentValidator;

    @Autowired
    private
    RegularExpressionService regExpService;

    public void validate(CfsReceiveEntry cfsReceiveEntry) {

        log.info("CFS Receieve Entry Validation Started......");

        verifyCFSMaster(cfsReceiveEntry);
        verifyCFSReceiptDate(cfsReceiveEntry);
        verifyProNumber(cfsReceiveEntry);
        verifyProDate(cfsReceiveEntry);
        verifyShipper(cfsReceiveEntry);
        verifyConsignee(cfsReceiveEntry);
        verifyTransporter(cfsReceiveEntry);
        verifyWarehouseLocation(cfsReceiveEntry);
        verifyShipperReferenceNumber(cfsReceiveEntry);
        verifyShipperOnHand(cfsReceiveEntry);
        verifyNotes(cfsReceiveEntry);

        if (cfsReceiveEntry.getCfsDimensionList() != null && cfsReceiveEntry.getCfsDimensionList().size() != 0) {
            for (CfsDimension dimension : cfsReceiveEntry.getCfsDimensionList()) {
                cfsDimensionValidator.validate(dimension);
            }
        }


        if (cfsReceiveEntry.getCfsAttachementList() != null && cfsReceiveEntry.getCfsAttachementList().size() != 0) {
            for (CfsAttachment attachment : cfsReceiveEntry.getCfsAttachementList()) {
                cfsAttachmentValidator.validate(attachment);
            }
        }


        log.info("CFS Receieve Entry Validation Finished......");

    }

    private void verifyCFSMaster(CfsReceiveEntry cfsReceiveEntry) {


        ValidateUtil.notNull(cfsReceiveEntry.getCfsMaster(), ErrorCode.AIR_CFS_RECEIVE_CFS_REQUIRED);
        ValidateUtil.notNull(cfsReceiveEntry.getCfsMaster().getId(), ErrorCode.AIR_CFS_RECEIVE_CFS_REQUIRED);
        ValidateUtil.isStatusBlocked(cfsReceiveEntry.getCfsMaster().getStatus(), ErrorCode.AIR_CFS_RECEIVE_CFS_BLOCKED);
        ValidateUtil.isStatusHidden(cfsReceiveEntry.getCfsMaster().getStatus(), ErrorCode.AIR_CFS_RECEIVE_CFS_HIDDEN);

        cfsReceiveEntry.setCfsMasterCode(cfsReceiveEntry.getCfsMaster().getCfsCode());

    }


    private void verifyCFSReceiptDate(CfsReceiveEntry cfsReceiveEntry) {
        ValidateUtil.notNull(cfsReceiveEntry.getCfsReceiptDate(), ErrorCode.AIR_CFS_RECEIVE_RECEIPT_DATE_REQUIRED);
    }


    private void verifyProNumber(CfsReceiveEntry cfsReceiveEntry) {
        //ValidateUtil.notNull(cfsReceiveEntry.getProNumber(), ErrorCode.AIR_CFS_RECEIVE_PRO_NUMBER_REQUIRED);
        regExpService.validate(cfsReceiveEntry.getProNumber(), RegExpName.Reg_Exp_Cfs_Receive_Pro_Number, ErrorCode.AIR_CFS_RECEIVE_PRO_NUMBER_INVALID);
    }

    private void verifyProDate(CfsReceiveEntry cfsReceiveEntry) {
        //ValidateUtil.notNull(cfsReceiveEntry.getProDate(), ErrorCode.AIR_CFS_RECEIVE_PRO_DATE_REQUIRED);
    }

    private void verifyShipper(CfsReceiveEntry cfsReceiveEntry) {

        ValidateUtil.notNull(cfsReceiveEntry.getShipper(), ErrorCode.AIR_CFS_RECEIVE_SHIPPER_REQUIRED);
        ValidateUtil.notNull(cfsReceiveEntry.getShipper().getId(), ErrorCode.AIR_CFS_RECEIVE_SHIPPER_REQUIRED);
        ValidateUtil.isStatusBlocked(cfsReceiveEntry.getShipper().getStatus(), ErrorCode.AIR_CFS_RECEIVE_SHIPPER_BLOCKED);
        ValidateUtil.isStatusHidden(cfsReceiveEntry.getShipper().getStatus(), ErrorCode.AIR_CFS_RECEIVE_SHIPPER_HIDDEN);

        cfsReceiveEntry.setShipperCode(cfsReceiveEntry.getShipper().getPartyCode());

    }


    private void verifyConsignee(CfsReceiveEntry cfsReceiveEntry) {

		/*ValidateUtil.notNull(cfsReceiveEntry.getConsignee(), ErrorCode.AIR_CFS_RECEIVE_CONSIGNEE_REQUIRED);
		ValidateUtil.notNull(cfsReceiveEntry.getConsignee().getId(), ErrorCode.AIR_CFS_RECEIVE_CONSIGNEE_REQUIRED);*/

        if (cfsReceiveEntry.getConsignee() != null && cfsReceiveEntry.getConsignee().getId() != null) {
            ValidateUtil.isStatusBlocked(cfsReceiveEntry.getConsignee().getStatus(), ErrorCode.AIR_CFS_RECEIVE_CONSIGNEE_BLOCKED);
            ValidateUtil.isStatusHidden(cfsReceiveEntry.getConsignee().getStatus(), ErrorCode.AIR_CFS_RECEIVE_CONSIGNEE_HIDDEN);

            cfsReceiveEntry.setConsigneeCode(cfsReceiveEntry.getConsignee().getPartyCode());
        }


    }

    private void verifyTransporter(CfsReceiveEntry cfsReceiveEntry) {
		
		/*ValidateUtil.notNull(cfsReceiveEntry.getTransporter(), ErrorCode.AIR_CFS_RECEIVE_TRANSPORTER_REQUIRED);
		ValidateUtil.notNull(cfsReceiveEntry.getTransporter().getId(), ErrorCode.AIR_CFS_RECEIVE_TRANSPORTER_REQUIRED);*/

        if (cfsReceiveEntry.getTransporter() != null && cfsReceiveEntry.getTransporter().getId() != null) {
            ValidateUtil.isStatusBlocked(cfsReceiveEntry.getTransporter().getStatus(), ErrorCode.AIR_CFS_RECEIVE_TRANSPORTER_BLOCKED);
            ValidateUtil.isStatusHidden(cfsReceiveEntry.getTransporter().getStatus(), ErrorCode.AIR_CFS_RECEIVE_TRANSPORTER_HIDDEN);

            cfsReceiveEntry.setTransporterCode(cfsReceiveEntry.getTransporter().getPartyCode());
        }
    }

    private void verifyWarehouseLocation(CfsReceiveEntry cfsReceiveEntry) {
        //ValidateUtil.notNull(cfsReceiveEntry.getWarehouseLocation(), ErrorCode.AIR_CFS_RECEIVE_WAREHOUSE_LOCATION_REQUIRED);
        if (cfsReceiveEntry.getWarehouseLocation() != null) {
            regExpService.validate(cfsReceiveEntry.getWarehouseLocation(), RegExpName.Reg_Exp_Cfs_Receive_Warehouse_location, ErrorCode.AIR_CFS_RECEIVE_WAREHOUSE_LOCATION_INVALID);
        }
    }

    private void verifyShipperReferenceNumber(CfsReceiveEntry cfsReceiveEntry) {
        //ValidateUtil.notNull(cfsReceiveEntry.getShipperRefNumber(), ErrorCode.AIR_CFS_RECEIVE_SHIPPER_REFERENCE_REQUIRED);
        if (cfsReceiveEntry.getShipperRefNumber() != null) {
            regExpService.validate(cfsReceiveEntry.getShipperRefNumber(), RegExpName.Reg_Exp_Cfs_Receive_Shipper_Ref_No, ErrorCode.AIR_CFS_RECEIVE_SHIPPER_REFERENCE_INVALID);
        }
    }

    private void verifyShipperOnHand(CfsReceiveEntry cfsReceiveEntry) {
        //ValidateUtil.notNull(cfsReceiveEntry.getOnHand(), ErrorCode.AIR_CFS_RECEIVE_ON_HAND_REQUIRED);
        if (cfsReceiveEntry.getOnHand() != null) {
            regExpService.validate(cfsReceiveEntry.getOnHand(), RegExpName.Reg_Exp_Cfs_Receive_On_Hand, ErrorCode.AIR_CFS_RECEIVE_ON_HAND_INVALID);

        }
    }

    private void verifyNotes(CfsReceiveEntry cfsReceiveEntry) {
        //ValidateUtil.notNull(cfsReceiveEntry.getNotes(), ErrorCode.AIR_CFS_RECEIVE_NOTES_INVALID);
        //regExpService.validate(cfsReceiveEntry.getNotes(), RegExpName.Reg_Exp_Cfs_Receive_Entry_Notes, ErrorCode.AIR_CFS_RECEIVE_NOTES_INVALID);
    }


}
