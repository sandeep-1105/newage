package com.efreightsuite.validation;

import com.efreightsuite.enumeration.LovStatus;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.model.CompanyMaster;
import com.efreightsuite.model.DocumentIssueRestriction;
import com.efreightsuite.model.LocationMaster;
import com.efreightsuite.model.ServiceMaster;
import com.efreightsuite.util.ValidateUtil;

import org.springframework.stereotype.Service;

/**
 * @author user
 */
@Service
public class DocumentIssueRestrictionValidator {


    public void validate(DocumentIssueRestriction documentIssueRestriction) {
        verifyCompany(documentIssueRestriction.getCompanyMaster());
        verifyLocation(documentIssueRestriction.getLocationMaster());
        verifyService(documentIssueRestriction.getServiceMaster());
    }

    private void verifyCompany(CompanyMaster companyMaster) {
        ValidateUtil.notNull(companyMaster, ErrorCode.DOCUMENT_ISSUE_RESTRICTION_COMPANY_REQUIRED);
        ValidateUtil.notNull(companyMaster.getId(), ErrorCode.DOCUMENT_ISSUE_RESTRICTION_COMPANY_REQUIRED);
        ValidateUtil.notNullOrEmpty(companyMaster.getCompanyCode(), ErrorCode.DOCUMENT_ISSUE_RESTRICTION_COMPANY_REQUIRED);

        ValidateUtil.notNull(companyMaster.getStatus(), ErrorCode.DOCUMENT_ISSUE_RESTRICTION_COMPANY_REQUIRED);

        if (companyMaster.getStatus().equals(LovStatus.Block)) {
            throw new RestException(ErrorCode.DOCUMENT_ISSUE_RESTRICTION_COMPANY_BLOCKED);

        }
        if (companyMaster.getStatus().equals(LovStatus.Hide)) {
            throw new RestException(ErrorCode.DOCUMENT_ISSUE_RESTRICTION_COMPANY_HIDDEN);
        }
    }

    private void verifyLocation(LocationMaster locationMaster) {
        ValidateUtil.notNull(locationMaster, ErrorCode.DOCUMENT_ISSUE_RESTRICTION_LOCATION_REQUIRED);
        ValidateUtil.notNull(locationMaster.getId(), ErrorCode.DOCUMENT_ISSUE_RESTRICTION_LOCATION_REQUIRED);
        ValidateUtil.notNullOrEmpty(locationMaster.getLocationCode(), ErrorCode.DOCUMENT_ISSUE_RESTRICTION_LOCATION_REQUIRED);
        ValidateUtil.notNull(locationMaster.getStatus(), ErrorCode.DOCUMENT_ISSUE_RESTRICTION_LOCATION_INVALID);

        if (locationMaster.getStatus().equals(LovStatus.Block)) {
            throw new RestException(ErrorCode.DOCUMENT_ISSUE_RESTRICTION_LOCATION_BLOCKED);

        }
        if (locationMaster.getStatus().equals(LovStatus.Hide)) {
            throw new RestException(ErrorCode.DOCUMENT_ISSUE_RESTRICTION_LOCATION_HIDDEN);
        }
    }

    private void verifyService(ServiceMaster serviceMaster) {
        ValidateUtil.notNull(serviceMaster, ErrorCode.DOCUMENT_ISSUE_RESTRICTION_SERVICE_REQUIRED);
        ValidateUtil.notNull(serviceMaster.getId(), ErrorCode.DOCUMENT_ISSUE_RESTRICTION_SERVICE_REQUIRED);
        ValidateUtil.notNullOrEmpty(serviceMaster.getServiceCode(), ErrorCode.DOCUMENT_ISSUE_RESTRICTION_SERVICE_REQUIRED);
        ValidateUtil.notNull(serviceMaster.getStatus(), ErrorCode.DOCUMENT_ISSUE_RESTRICTION_SERVICE_INVALID);

        if (serviceMaster.getStatus().equals(LovStatus.Block)) {
            throw new RestException(ErrorCode.DOCUMENT_ISSUE_RESTRICTION_SERVICE_BLOCKED);

        }
        if (serviceMaster.getStatus().equals(LovStatus.Hide)) {
            throw new RestException(ErrorCode.DOCUMENT_ISSUE_RESTRICTION_SERVICE_HIDDEN);
        }

    }
}
