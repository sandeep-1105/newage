package com.efreightsuite.validation;

import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.model.CarrierMaster;
import com.efreightsuite.model.ConsolConnection;
import com.efreightsuite.model.PortMaster;
import com.efreightsuite.repository.CarrierMasterRepository;
import com.efreightsuite.repository.PortMasterRepository;
import com.efreightsuite.util.ValidateUtil;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Objects;

@Service
@Log4j2
public class ConsolConnectionValidator {

    @Autowired
    private PortMasterRepository portMasterRepository;

    @Autowired
    private CarrierMasterRepository carrierMasterRepository;


    public void validate(ConsolConnection cc) {

        verifyMove(cc);

        verifyStatus(cc);

        verifyPol(cc);

        verifyPod(cc);

        verifyEtd(cc);

        verifyEta(cc);

        verifyCarrierVessel(cc);

        verifyFlightOrVoyageNo(cc);
    }


    private void verifyMove(ConsolConnection cc) {

        ValidateUtil.notNull(cc.getMove(), ErrorCode.CONSOL_MOVE_NOTNULL);

    }

    private void verifyStatus(ConsolConnection cc) {

        ValidateUtil.notNull(cc.getConnectionStatus(), ErrorCode.CONSOL_CONNECTION_STATUS);

    }

    private void verifyPol(ConsolConnection cc) {
        ValidateUtil.notNull(cc.getPol(), ErrorCode.CONSOL_CONNECTION_POL_MANDATORY);
        ValidateUtil.notNull(cc.getPol().getId(), ErrorCode.CONSOL_CONNECTION_POL_MANDATORY);

        PortMaster pol = portMasterRepository.findById(cc.getPol().getId());

        ValidateUtil.notNullOrEmpty(pol.getPortCode(), ErrorCode.CONSOL_CONNECTION_POL_MANDATORY);

        ValidateUtil.isStatusBlocked(pol.getStatus(), ErrorCode.CONSOL_CONNECTION_POL_BLOCKED);
        ValidateUtil.isStatusHidden(pol.getStatus(), ErrorCode.CONSOL_CONNECTION_POL_HIDDEN);

        if (cc.getMove() != pol.getTransportMode()) {
            throw new RestException(ErrorCode.CONSOL_POL_MODE);
        }

        cc.setPolCode(pol.getPortCode());
    }

    private void verifyPod(ConsolConnection cc) {
        ValidateUtil.notNull(cc.getPod(), ErrorCode.CONSOL_CONNECTION_POD_MANDATORY);
        ValidateUtil.notNull(cc.getPod().getId(), ErrorCode.CONSOL_CONNECTION_POD_MANDATORY);

        PortMaster pod = portMasterRepository.findById(cc.getPod().getId());
        PortMaster pol = portMasterRepository.findById(cc.getPol().getId());

        ValidateUtil.notNullOrEmpty(pod.getPortCode(), ErrorCode.CONSOL_CONNECTION_POD_MANDATORY);

        ValidateUtil.isStatusBlocked(pod.getStatus(), ErrorCode.CONSOL_CONNECTION_POD_BLOCKED);
        ValidateUtil.isStatusHidden(pod.getStatus(), ErrorCode.CONSOL_CONNECTION_POD_HIDDEN);

        if (cc.getMove() != pod.getTransportMode()) {
            throw new RestException(ErrorCode.CONSOL_POD_MODE);
        }

        if (Objects.equals(pod.getId(), pol.getId())) {
            throw new RestException(ErrorCode.CONSOL_ORIGIN_DESTINATION_EQUAL);
        }

        if (pod.getPortCode().equals(pol.getPortCode())) {
            throw new RestException(ErrorCode.CONSOL_POD_POL_EQUAL);
        }

        cc.setPodCode(pod.getPortCode());

    }

    private void verifyEtd(ConsolConnection cc) {
        ValidateUtil.notNull(cc.getEtd(), ErrorCode.CONSOL_CONNECTION_ETD_MANDATORY);
    }

    private void verifyEta(ConsolConnection cc) {
        ValidateUtil.notNull(cc.getEta(), ErrorCode.CONSOL_CONNECTION_ETA_MANDATORY);

        if (cc.getEtd().after(cc.getEta())) {
            throw new RestException(ErrorCode.CONSOL_CONNECTION_ETD_ETA_GREATER);
        }

    }

    private void verifyCarrierVessel(ConsolConnection cc) {
        ValidateUtil.notNull(cc.getCarrierMaster(), ErrorCode.CONSOL_CONNECTION_CARRIER_MANDATORY);
        ValidateUtil.notNull(cc.getCarrierMaster().getId(), ErrorCode.CONSOL_CONNECTION_CARRIER_MANDATORY);

        CarrierMaster carrierMaster = carrierMasterRepository.findById(cc.getCarrierMaster().getId());

        ValidateUtil.notNullOrEmpty(carrierMaster.getCarrierCode(), ErrorCode.CONSOL_CONNECTION_CARRIER_MANDATORY);
        ValidateUtil.isStatusBlocked(carrierMaster.getStatus(), ErrorCode.CONSOL_CONNECTION_CARRIER_BLOCKED);
        ValidateUtil.isStatusHidden(carrierMaster.getStatus(), ErrorCode.CONSOL_CONNECTION_CARRIER_HIDDEN);
        cc.setCarrierCode(carrierMaster.getCarrierCode());
    }

    private void verifyFlightOrVoyageNo(ConsolConnection cc) {

        ValidateUtil.notNull(cc.getFlightVoyageNo(), ErrorCode.CONSOL_FLIGHTVOYAGE_NOT_NULL);
        ValidateUtil.validateLength(cc.getFlightVoyageNo(), 1, 10, ErrorCode.CONSOL_FLIGHTVOYAGE_NO_INVALID);

    }


}
