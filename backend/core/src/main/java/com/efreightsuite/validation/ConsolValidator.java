package com.efreightsuite.validation;

import com.efreightsuite.common.component.CacheRepository;
import com.efreightsuite.enumeration.ReferenceTypeKey;
import com.efreightsuite.enumeration.WhichTransactionDate;
import com.efreightsuite.enumeration.YesNo;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.model.*;
import com.efreightsuite.repository.*;
import com.efreightsuite.util.AppUtil;
import com.efreightsuite.util.SaaSUtil;
import com.efreightsuite.util.TimeUtil;
import com.efreightsuite.util.ValidateUtil;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * @author user
 */

@Service
@Log4j2
public class ConsolValidator {

    @Autowired
    private
    AppUtil appUtil;

    @Autowired
    private
    CacheRepository cacheRepository;

    @Autowired
    private
    PartyMasterRepository partyMasterRepository;

    @Autowired
    private
    DateConfigurationRepository dateConfigurationRepository;

    @Autowired
    DefaultMasterDataRepository defaultMasterDataRepository;

    @Autowired
    ConsolDimensionValidator consolDimensionValidator;

    @Autowired
    private
    ConsolConnectionValidator consolConnectionValidator;

    @Autowired
    private
    ConsolChargeValidator consolChargeValidator;

    @Autowired
    private
    ConsolEventValidator consolEventValidator;

    @Autowired
    private
    ConsolDocumentValidator consolDocumentValidator;

    @Autowired
    private
    ConsolAttachmentValidator consolAttachmentValidator;

    @Autowired
    private
    ReferenceTypeMasterRepository referenceTypeMasterRepository;

    @Autowired
    private
    ShipmentRepository shipmentRepository;

    @Autowired
    private
    ShipmentLinkRepository shipmentLinkRepository;

    @Autowired
    private
    EmployeeMasterRepository employeeMasterRepository;

    @Autowired
    private
    CompanyMasterRepository companyMasterRepository;

    @Autowired
    private
    LocationMasterRepository locationMasterRepository;

    @Autowired
    private
    CountryMasterRepository countryMasterRepository;

    @Autowired
    private
    ServiceMasterRepository serviceMasterRepository;

    @Autowired
    private
    PortMasterRepository portMasterRepository;

    @Autowired
    private
    CurrencyMasterRepository currencyMasterRepository;

    @Autowired
    private
    CarrierMasterRepository carrierMasterRepository;

    @Autowired
    private
    CfsMasterRepository cfsMasterRepository;

    public void validate(Consol consol) {
        verifyCreatedBy(consol);
        verifyCompany(consol);
        verifyLocation(consol);
        verifyCountry(consol);
        verifyService(consol);
        verifyAgent(consol);
        verifyDestination(consol);
        verifyOrigin(consol);
        verifyPol(consol);
        verifyPod(consol);
        verifyCurrency(consol);
        verifyWeight(consol);
        verifyCarrier(consol);
        verifyEta(consol);
        verifyEtd(consol);
        verifyFlightNo(consol);
        verifyAtd(consol);

        updateJobDate(consol);
        consolDocumentValidator.validate(consol.getConsolDocument(), consol);

        if (consol.getShipmentLinkList() != null && consol.getShipmentLinkList().size() != 0) {
            for (ShipmentLink cd : consol.getShipmentLinkList()) {
                if (cd.getId() == null) {
                    isConsolCreatedFromShipment(cd.getServiceUid());
                }
                checkMandatoryReference(cd.getService());
            }
        } else {
            throw new RestException(ErrorCode.CONSOL_SERVICE_DOCUMENT_MANDATORY);
        }

        if (consol.getConnectionList() != null && consol.getConnectionList().size() != 0) {
            for (ConsolConnection cc : consol.getConnectionList()) {
                consolConnectionValidator.validate(cc);
            }
        } else {


            String key = appUtil.getLocationConfig("connection.required", consol.getLocation(), true);

            if (key != null && key.trim().length() != 0) {

                if (key.equals("TRUE")) {
                    if (consol.getOrigin() != null && consol.getOrigin().getId() != null && consol.getPol() != null && consol.getPol().getId() != null) {
                        if (!consol.getOrigin().getId().equals(consol.getPol().getId())) {
                            throw new RestException(ErrorCode.CONNECTION_LIST_REQUIRED);
                        }
                    }
                    if (consol.getDestination() != null && consol.getDestination().getId() != null && consol.getPod() != null && consol.getPod().getId() != null) {
                        if (!consol.getDestination().getId().equals(consol.getPod().getId())) {
                            throw new RestException(ErrorCode.CONNECTION_LIST_REQUIRED);
                        }
                    }
                }
            }
        }

        if (consol.getChargeList() != null && consol.getChargeList().size() != 0) {
            for (ConsolCharge cc : consol.getChargeList()) {
                consolChargeValidator.validate(cc);
            }
        }

        if (consol.getEventList() != null && consol.getEventList().size() != 0) {
            for (ConsolEvent cc : consol.getEventList()) {
                consolEventValidator.validate(cc);
            }
        }


        if (consol.getConsolAttachmentList() != null && consol.getConsolAttachmentList().size() != 0) {
            for (ConsolAttachment shipmentAttachment : consol.getConsolAttachmentList()) {
                consolAttachmentValidator.validate(shipmentAttachment, consol);
            }
        }

        if (consol.getReferenceList() != null && consol.getReferenceList().size() != 0) {
            for (ConsolReference ref : consol.getReferenceList()) {
                ref.setReferenceTypeCode(ref.getReferenceTypeMaster().getReferenceTypeCode());
            }
        }

        if (consol.getPickUpDeliveryPoint() != null) {
            verifyTransporter(consol.getPickUpDeliveryPoint());
            verifyPickUp(consol.getPickUpDeliveryPoint());
            verifyDelivery(consol.getPickUpDeliveryPoint());
            verifyDoorDelivery(consol.getPickUpDeliveryPoint());
        }

        if (consol.getFirstCarrier() != null) {
            consol.setFirstCarrierCode(consol.getFirstCarrier().getCarrierCode());
        }

        if (consol.getLocalCurrency() != null) {
            consol.setLocalCurrencyCode(consol.getLocalCurrency().getCurrencyCode());
        }

        if (consol.getWeightUnit() != null) {
            consol.setWeightUnitCode(consol.getWeightUnit().getUnitCode());
        }

        if (consol.getVolumeUnit() != null) {
            consol.setVolumeUnitCode(consol.getVolumeUnit().getUnitCode());
        }
    }


    private void checkMandatoryReference(ShipmentServiceDetail service) {

        List<ShipmentServiceReference> referenceList = shipmentRepository.getByService(service.getId()).getReferenceList();
        List<ReferenceTypeMaster> listOfMandatoryReferences = referenceTypeMasterRepository.findAllByReferenceTypeKey(ReferenceTypeKey.Shipment);
        if (listOfMandatoryReferences != null) {
            log.info("Number of Mandatory References " + listOfMandatoryReferences.size());
            for (ReferenceTypeMaster mandatoryReference : listOfMandatoryReferences) {
                boolean found = false;
                if (referenceList != null && !referenceList.isEmpty()) {
                    for (ShipmentServiceReference shipmentServiceReference : referenceList) {
                        if (shipmentServiceReference.getReferenceTypeMaster().getId().equals(mandatoryReference.getId())) {
                            found = true;
                            break;
                        }
                    }
                }
                if (!found) {
                    RestException exception = new RestException(ErrorCode.SHIPMENT_REFERENCE_MANDATORY_WITH_PARAM);
                    exception.setParams(new ArrayList<>());
                    exception.getParams().add(mandatoryReference.getReferenceTypeName());
                    exception.getParams().add(service.getServiceMaster().getServiceCode());
                    exception.getParams().add(service.getShipmentUid());
                    throw exception;
                }
            }
        }
    }

    public void updateJobDate(Consol consol) {

        if (consol.getId() != null && consol.getMasterDate() != null) {
            return;
        }

        DateConfiguration dConfig = dateConfigurationRepository.getByLogicDate(consol.getServiceMaster().getId(), WhichTransactionDate.Master);


        if (dConfig != null) {
            switch (dConfig.getDateLogic()) {
                case ETD:
                    consol.setMasterDate(consol.getEtd() == null ? null : consol.getEtd());
                    break;
                case ETA:
                    consol.setMasterDate(consol.getEta() == null ? null : consol.getEta());
                    break;
                case ATD:
                    consol.setMasterDate(consol.getAtd() == null ? null : consol.getAtd());
                    break;
                case ATA:
                    consol.setMasterDate(consol.getAta() == null ? null : consol.getAta());
                    break;
                case Date:
                    consol.setMasterDate(TimeUtil.getCurrentLocationTime());
                    break;
                default:
                    consol.setMasterDate(TimeUtil.getCurrentLocationTime());
            }
        }

        if (consol.getId() == null || consol.getMasterDate() == null) {
            consol.setMasterDate(TimeUtil.getCurrentLocationTime());
        }

    }

    private void verifyDoorDelivery(PickUpDeliveryPoint pickUpDelivery) {

        if (pickUpDelivery.getDoorDeliveryPoint() != null && pickUpDelivery.getDoorDeliveryPoint().getId() != null) {
            ValidateUtil.notNull(pickUpDelivery.getDoorDeliveryPoint(), ErrorCode.SHIPMENT_SERVICE_DOOR_DELIVERY_POINT);
            ValidateUtil.notNull(pickUpDelivery.getDoorDeliveryPoint().getId(), ErrorCode.SHIPMENT_SERVICE_DOOR_DELIVERY_POINT);

            PortMaster doorDeliveryPoint = portMasterRepository.findById(pickUpDelivery.getDoorDeliveryPoint().getId());


            ValidateUtil.notNullOrEmpty(doorDeliveryPoint.getPortCode(), ErrorCode.SHIPMENT_SERVICE_DOOR_DELIVERY_POINT);

            ValidateUtil.isStatusBlocked(doorDeliveryPoint.getStatus(), ErrorCode.SHIPMENT_SERVICE_DOOR_DELIVERY_POINT_BLOCK);
            ValidateUtil.isStatusHidden(doorDeliveryPoint.getStatus(), ErrorCode.SHIPMENT_SERVICE_DOOR_DELIVERY_POINT_HIDE);

            pickUpDelivery.setDoorDeliveryPointCode(doorDeliveryPoint.getPortCode());
        } else {
            pickUpDelivery.setDoorDeliveryPoint(null);
            pickUpDelivery.setDoorDeliveryPointCode(null);
        }

        if (pickUpDelivery.getDoorDeliveryFrom() != null && pickUpDelivery.getDoorDeliveryFrom().getId() != null) {
            ValidateUtil.notNull(pickUpDelivery.getDoorDeliveryFrom(), ErrorCode.SHIPMENT_SERVICE_DOOR_DELIVERY_FROM);
            ValidateUtil.notNull(pickUpDelivery.getDoorDeliveryFrom().getId(), ErrorCode.SHIPMENT_SERVICE_DOOR_DELIVERY_FROM);

            PortMaster doorDeliveryFrom = portMasterRepository.findById(pickUpDelivery.getDoorDeliveryFrom().getId());


            ValidateUtil.notNullOrEmpty(doorDeliveryFrom.getPortCode(), ErrorCode.SHIPMENT_SERVICE_DOOR_DELIVERY_FROM);

            ValidateUtil.isStatusBlocked(doorDeliveryFrom.getStatus(), ErrorCode.SHIPMENT_SERVICE_DOOR_DELIVERY_FROM_BLOCK);
            ValidateUtil.isStatusHidden(doorDeliveryFrom.getStatus(), ErrorCode.SHIPMENT_SERVICE_DOOR_DELIVERY_FROM_HIDE);

            pickUpDelivery.setDoorDeliveryFromCode(doorDeliveryFrom.getPortCode());
        } else {
            pickUpDelivery.setDoorDeliveryFrom(null);
            pickUpDelivery.setDoorDeliveryFromCode(null);
        }

        if (pickUpDelivery.getDoorDeliveryPlace() != null) {
            ValidateUtil.notNull(pickUpDelivery.getDoorDeliveryPlace(), ErrorCode.SHIPMENT_SERVICE_DOOR_DELIVERY_PLACE);
            ValidateUtil.validateLength(pickUpDelivery.getDoorDeliveryPlace(), 1, 500, ErrorCode.SHIPMENT_SERVICE_DOOR_DELIVERY_PLACE);
        } else {
            pickUpDelivery.setDoorDeliveryPlace(null);
        }

        if (pickUpDelivery.getDoorDeliveryContactPerson() != null && pickUpDelivery.getDoorDeliveryContactPerson().trim().length() != 0) {
            ValidateUtil.notNullOrEmpty(pickUpDelivery.getDoorDeliveryContactPerson(), ErrorCode.SHIPMENT_SERVICE_DOOR_DELIVERY_CONTACT_PERSON);
            ValidateUtil.validateLength(pickUpDelivery.getDoorDeliveryContactPerson(), 1, 100, ErrorCode.SHIPMENT_SERVICE_DOOR_DELIVERY_CONTACT_PERSON);
        } else {
            pickUpDelivery.setDoorDeliveryContactPerson(null);
        }

        if (pickUpDelivery.getDoorDeliveryMobileNo() != null && pickUpDelivery.getDoorDeliveryMobileNo().trim().length() != 0) {
            ValidateUtil.notNullOrEmpty(pickUpDelivery.getDoorDeliveryMobileNo(), ErrorCode.SHIPMENT_SERVICE_DOOR_DELIVERY_MOBILE);
            ValidateUtil.validateLength(pickUpDelivery.getDoorDeliveryMobileNo(), 1, 100, ErrorCode.SHIPMENT_SERVICE_DOOR_DELIVERY_MOBILE);
        } else {
            pickUpDelivery.setDoorDeliveryMobileNo(null);
        }

        if (pickUpDelivery.getDoorDeliveryPhoneNo() != null && pickUpDelivery.getDoorDeliveryPhoneNo().trim().length() != 0) {
            ValidateUtil.notNullOrEmpty(pickUpDelivery.getDoorDeliveryPhoneNo(), ErrorCode.SHIPMENT_SERVICE_DOOR_DELIVERY_PHONE);
            ValidateUtil.validateLength(pickUpDelivery.getDoorDeliveryPhoneNo(), 1, 100, ErrorCode.SHIPMENT_SERVICE_DOOR_DELIVERY_PHONE);
        } else {
            pickUpDelivery.setDoorDeliveryPhoneNo(null);
        }

        if (pickUpDelivery.getDoorDeliveryEmail() != null && pickUpDelivery.getDoorDeliveryEmail().trim().length() != 0) {
            ValidateUtil.notNullOrEmpty(pickUpDelivery.getDoorDeliveryEmail(), ErrorCode.SHIPMENT_SERVICE_DOOR_DELIVERY_EMAIL_LENGTH);
            ValidateUtil.validateLength(pickUpDelivery.getDoorDeliveryEmail(), 1, 500, ErrorCode.SHIPMENT_SERVICE_DOOR_DELIVERY_EMAIL_LENGTH);
            /*for(String email : pickUpDelivery.getDoorDeliveryEmail().split(defaultMasterDataRepository.findByCode("single.text.split.by.multiple.email").getValue())){
				if(!Pattern.compile(ValidateUtil.regularExp).matcher(email).matches()){
					throw new RestException(ErrorCode.SHIPMENT_SERVICE_DOOR_DELIVERY_EMAIL);
				}
			}*/
        } else {
            pickUpDelivery.setDoorDeliveryEmail(null);
        }
    }

    private void verifyDelivery(PickUpDeliveryPoint pickUpDelivery) {

        if (pickUpDelivery.getDeliveryPoint() != null && pickUpDelivery.getDeliveryPoint().getId() != null) {
            ValidateUtil.notNull(pickUpDelivery.getDeliveryPoint(), ErrorCode.SHIPMENT_SERVICE_DELIVERY_POINT);
            ValidateUtil.notNull(pickUpDelivery.getDeliveryPoint().getId(), ErrorCode.SHIPMENT_SERVICE_DELIVERY_POINT);

            CFSMaster deliveryPoint = cfsMasterRepository.getOne(pickUpDelivery.getDeliveryPoint().getId());

            ValidateUtil.notNullOrEmpty(deliveryPoint.getCfsCode(), ErrorCode.SHIPMENT_SERVICE_DELIVERY_POINT);

            ValidateUtil.isStatusBlocked(deliveryPoint.getStatus(), ErrorCode.SHIPMENT_SERVICE_DELIVERY_POINT_BLOCK);
            ValidateUtil.isStatusHidden(deliveryPoint.getStatus(), ErrorCode.SHIPMENT_SERVICE_DELIVERY_POINT_HIDE);

            pickUpDelivery.setDeliveryPointCode(deliveryPoint.getCfsCode());
        } else {
            pickUpDelivery.setDeliveryPoint(null);
            pickUpDelivery.setDeliveryPointCode(null);
        }

        if (pickUpDelivery.getDeliveryFrom() != null && pickUpDelivery.getDeliveryFrom().getId() != null) {
            ValidateUtil.notNull(pickUpDelivery.getDeliveryFrom(), ErrorCode.SHIPMENT_SERVICE_DELIVERY_FROM);
            ValidateUtil.notNull(pickUpDelivery.getDeliveryFrom().getId(), ErrorCode.SHIPMENT_SERVICE_DELIVERY_FROM);

            PortMaster deliveryFrom = portMasterRepository.getOne(pickUpDelivery.getDeliveryFrom().getId());

            ValidateUtil.notNullOrEmpty(deliveryFrom.getPortCode(), ErrorCode.SHIPMENT_SERVICE_DELIVERY_FROM);

            ValidateUtil.isStatusBlocked(deliveryFrom.getStatus(), ErrorCode.SHIPMENT_SERVICE_DELIVERY_FROM_BLOCK);
            ValidateUtil.isStatusHidden(deliveryFrom.getStatus(), ErrorCode.SHIPMENT_SERVICE_DELIVERY_FROM_HIDE);

            pickUpDelivery.setDeliveryFromCode(deliveryFrom.getPortCode());
        } else {
            pickUpDelivery.setDeliveryFrom(null);
            pickUpDelivery.setDeliveryFromCode(null);
        }

        if (pickUpDelivery.getDeliveryPlace() != null) {
            ValidateUtil.notNull(pickUpDelivery.getDeliveryPlace(), ErrorCode.SHIPMENT_SERVICE_DELIVERY_PLACE);
            ValidateUtil.validateLength(pickUpDelivery.getDeliveryPlace(), 1, 500, ErrorCode.SHIPMENT_SERVICE_DELIVERY_PLACE);
        } else {
            pickUpDelivery.setDeliveryPlace(null);
        }

        if (pickUpDelivery.getDeliveryContactPerson() != null && pickUpDelivery.getDeliveryContactPerson().trim().length() != 0) {
            ValidateUtil.notNullOrEmpty(pickUpDelivery.getDeliveryContactPerson(), ErrorCode.SHIPMENT_SERVICE_DELIVERY_CONTACT_PERSON);
            ValidateUtil.validateLength(pickUpDelivery.getDeliveryContactPerson(), 1, 100, ErrorCode.SHIPMENT_SERVICE_DELIVERY_CONTACT_PERSON);
        } else {
            pickUpDelivery.setDeliveryContactPerson(null);
        }

        if (pickUpDelivery.getDeliveryMobileNo() != null && pickUpDelivery.getDeliveryMobileNo().trim().length() != 0) {
            ValidateUtil.notNullOrEmpty(pickUpDelivery.getDeliveryMobileNo(), ErrorCode.SHIPMENT_SERVICE_DELIVERY_MOBILE);
            ValidateUtil.validateLength(pickUpDelivery.getDeliveryMobileNo(), 1, 100, ErrorCode.SHIPMENT_SERVICE_DELIVERY_MOBILE);
        } else {
            pickUpDelivery.setDeliveryMobileNo(null);
        }

        if (pickUpDelivery.getDeliveryPhoneNo() != null && pickUpDelivery.getDeliveryPhoneNo().trim().length() != 0) {
            ValidateUtil.notNullOrEmpty(pickUpDelivery.getDeliveryPhoneNo(), ErrorCode.SHIPMENT_SERVICE_DELIVERY_PHONE);
            ValidateUtil.validateLength(pickUpDelivery.getDeliveryPhoneNo(), 1, 100, ErrorCode.SHIPMENT_SERVICE_DELIVERY_PHONE);
        } else {
            pickUpDelivery.setDeliveryPhoneNo(null);
        }

        if (pickUpDelivery.getDeliveryEmail() != null && pickUpDelivery.getDeliveryEmail().trim().length() != 0) {
            ValidateUtil.notNullOrEmpty(pickUpDelivery.getDeliveryEmail(), ErrorCode.SHIPMENT_SERVICE_DELIVERY_EMAIL_LENGTH);
            ValidateUtil.validateLength(pickUpDelivery.getDeliveryEmail(), 1, 500, ErrorCode.SHIPMENT_SERVICE_DELIVERY_EMAIL_LENGTH);
		/*	for(String email : pickUpDelivery.getDeliveryEmail().split(defaultMasterDataRepository.findByCode("single.text.split.by.multiple.email").getValue())){
				if(!Pattern.compile(ValidateUtil.regularExp).matcher(email).matches()){
					throw new RestException(ErrorCode.SHIPMENT_SERVICE_DELIVERY_EMAIL);
				}
			}*/
        } else {
            pickUpDelivery.setDeliveryEmail(null);
        }
    }

    private void verifyPickUp(PickUpDeliveryPoint pickUpDelivery) {

        if (pickUpDelivery.getPickupPoint() != null && pickUpDelivery.getPickupPoint().getId() != null) {
            PartyMaster tmpParty = cacheRepository.get(SaaSUtil.getSaaSId(), PartyMaster.class.getName(), pickUpDelivery.getPickupPoint().getId(), partyMasterRepository);

            ValidateUtil.notNull(tmpParty, ErrorCode.SHIPMENT_SERVICE_PICKUP_POINT);
            ValidateUtil.notNull(tmpParty.getId(), ErrorCode.SHIPMENT_SERVICE_PICKUP_POINT);
            ValidateUtil.notNullOrEmpty(tmpParty.getPartyCode(), ErrorCode.SHIPMENT_SERVICE_PICKUP_POINT);

            ValidateUtil.isDefaulter(tmpParty, ErrorCode.CONSOL_PIC_DEFAULTER);

            ValidateUtil.isStatusBlocked(tmpParty.getStatus(), ErrorCode.SHIPMENT_SERVICE_PICKUP_POINT_BLOCK);
            ValidateUtil.isStatusHidden(tmpParty.getStatus(), ErrorCode.SHIPMENT_SERVICE_PICKUP_POINT_HIDE);

            pickUpDelivery.setPickupPointCode(tmpParty.getPartyCode());
        } else {
            pickUpDelivery.setPickupPoint(null);
            pickUpDelivery.setPickupPointCode(null);
        }

        if (pickUpDelivery.getPickupFrom() != null && pickUpDelivery.getPickupFrom().getId() != null) {
            ValidateUtil.notNull(pickUpDelivery.getPickupFrom(), ErrorCode.SHIPMENT_SERVICE_PICKUP_FROM);
            ValidateUtil.notNull(pickUpDelivery.getPickupFrom().getId(), ErrorCode.SHIPMENT_SERVICE_PICKUP_FROM);

            PortMaster pickUpFrom = portMasterRepository.findById(pickUpDelivery.getPickupFrom().getId());

            ValidateUtil.notNullOrEmpty(pickUpFrom.getPortCode(), ErrorCode.SHIPMENT_SERVICE_PICKUP_FROM);

            ValidateUtil.isStatusBlocked(pickUpFrom.getStatus(), ErrorCode.SHIPMENT_SERVICE_PICKUP_FROM_BLOCK);
            ValidateUtil.isStatusHidden(pickUpFrom.getStatus(), ErrorCode.SHIPMENT_SERVICE_PICKUP_FROM_HIDE);

            pickUpDelivery.setPickupFromCode(pickUpFrom.getPortCode());
        } else {
            pickUpDelivery.setPickupFrom(null);
            pickUpDelivery.setPickupFromCode(null);
        }

        if (pickUpDelivery.getPickUpPlace() != null) {
            ValidateUtil.notNull(pickUpDelivery.getPickUpPlace(), ErrorCode.SHIPMENT_SERVICE_PICKUP_PLACE);
            ValidateUtil.validateLength(pickUpDelivery.getPickUpPlace(), 1, 500, ErrorCode.SHIPMENT_SERVICE_PICKUP_PLACE);
        } else {
            pickUpDelivery.setPickUpPlace(null);
        }

        if (pickUpDelivery.getPickUpContactPerson() != null && pickUpDelivery.getPickUpContactPerson().trim().length() != 0) {
            ValidateUtil.notNullOrEmpty(pickUpDelivery.getPickUpContactPerson(), ErrorCode.SHIPMENT_SERVICE_PICKUP_CONTACT_PERSON);
            ValidateUtil.validateLength(pickUpDelivery.getPickUpContactPerson(), 1, 100, ErrorCode.SHIPMENT_SERVICE_PICKUP_CONTACT_PERSON);
        } else {
            pickUpDelivery.setPickUpContactPerson(null);
        }

        if (pickUpDelivery.getPickUpMobileNo() != null && pickUpDelivery.getPickUpMobileNo().trim().length() != 0) {
            ValidateUtil.notNullOrEmpty(pickUpDelivery.getPickUpMobileNo(), ErrorCode.SHIPMENT_SERVICE_PICKUP_MOBILE);
            ValidateUtil.validateLength(pickUpDelivery.getPickUpMobileNo(), 1, 100, ErrorCode.SHIPMENT_SERVICE_PICKUP_MOBILE);
        } else {
            pickUpDelivery.setPickUpMobileNo(null);
        }

        if (pickUpDelivery.getPickUpPhoneNo() != null && pickUpDelivery.getPickUpPhoneNo().trim().length() != 0) {
            ValidateUtil.notNullOrEmpty(pickUpDelivery.getPickUpPhoneNo(), ErrorCode.SHIPMENT_SERVICE_PICKUP_PHONE);
            ValidateUtil.validateLength(pickUpDelivery.getPickUpPhoneNo(), 1, 100, ErrorCode.SHIPMENT_SERVICE_PICKUP_PHONE);
        } else {
            pickUpDelivery.setPickUpPhoneNo(null);
        }

        if (pickUpDelivery.getPickUpEmail() != null && pickUpDelivery.getPickUpEmail().trim().length() != 0) {
            ValidateUtil.notNullOrEmpty(pickUpDelivery.getPickUpEmail(), ErrorCode.SHIPMENT_SERVICE_PICKUP_EMAIL_LENGTH);
            ValidateUtil.validateLength(pickUpDelivery.getPickUpEmail(), 1, 500, ErrorCode.SHIPMENT_SERVICE_PICKUP_EMAIL_LENGTH);
			/*for(String email : pickUpDelivery.getPickUpEmail().split(defaultMasterDataRepository.findByCode("single.text.split.by.multiple.email").getValue())){
				if(!Pattern.compile(ValidateUtil.regularExp).matcher(email).matches()){
					throw new RestException(ErrorCode.SHIPMENT_SERVICE_PICKUP_EMAIL);
				}
			}*/
        } else {
            pickUpDelivery.setPickUpEmail(null);
        }
    }


    private void verifyTransporter(PickUpDeliveryPoint pickUpDelivery) {
        if (pickUpDelivery.getTransporter() != null && pickUpDelivery.getTransporter().getId() != null) {

            PartyMaster tmpParty = cacheRepository.get(SaaSUtil.getSaaSId(), PartyMaster.class.getName(), pickUpDelivery.getTransporter().getId(), partyMasterRepository);


            ValidateUtil.notNull(tmpParty, ErrorCode.SHIPMENT_TRANSPORTER);
            ValidateUtil.notNull(tmpParty.getId(), ErrorCode.SHIPMENT_TRANSPORTER);
            ValidateUtil.notNullOrEmpty(tmpParty.getPartyCode(), ErrorCode.SHIPMENT_TRANSPORTER);

            ValidateUtil.isDefaulter(tmpParty, ErrorCode.CONSOL_TRAS_DEFAULTER);

            ValidateUtil.isStatusBlocked(tmpParty.getStatus(), ErrorCode.SHIPMENT_TRANSPORTER_BLOCK);
            ValidateUtil.isStatusHidden(tmpParty.getStatus(), ErrorCode.SHIPMENT_TRANSPORTER_HIDE);


            pickUpDelivery.setTransporterCode(tmpParty.getPartyCode());
        } else {
            pickUpDelivery.setTransporter(null);
            pickUpDelivery.setTransporterCode(null);
            pickUpDelivery.setTransportAddress(null);
        }

    }

    private void verifyWeight(Consol consol) {

        if (consol.getConsolDocument().getRatePerCharge() != null && consol.getConsolDocument().getRatePerCharge() < 0) {
            throw new RestException(ErrorCode.CONSOL_RATE_PER_CHARGE_INVAILD);
        }


        if (consol.getConsolDocument().getVolume() != null) {
            ValidateUtil.notNull(consol.getConsolDocument().getVolume(), ErrorCode.CONSOL_VOLUME_INVALID);
            if (consol.getConsolDocument().getVolume() < 0) {
                throw new RestException(ErrorCode.CONSOL_VOLUME_INVALID);
            }
        } else {
            consol.getConsolDocument().setVolume(null);
        }

    }

    private void verifyCurrency(Consol consol) {

        if (consol.getCurrency() != null && consol.getCurrency().getId() != null) {

            CurrencyMaster currency = currencyMasterRepository.getOne(consol.getCurrency().getId());
            ValidateUtil.notNullOrEmpty(currency.getCurrencyCode(), ErrorCode.CONSOL_CURRENCY);
            ValidateUtil.isStatusBlocked(currency.getStatus(), ErrorCode.CONSOL_CURRENCY_BLOCK);
            ValidateUtil.isStatusHidden(currency.getStatus(), ErrorCode.CONSOL_CURRENCY_HIDE);

            ValidateUtil.notNull(consol.getCurrencyRate(), ErrorCode.CONSOL_CURRENCY_CURRENT_RATE_INVALID);

            if (consol.getCurrencyRate() <= 0) {
                throw new RestException(ErrorCode.CONSOL_CURRENCY_CURRENT_RATE_INVALID);
            }

            consol.setCurrencyCode(currency.getCurrencyCode());
        } else {
            consol.setCurrency(null);
            consol.setCurrencyCode(null);
            consol.setCurrencyRate(null);
        }
    }

    private void verifyPod(Consol consol) {
        ValidateUtil.notNull(consol.getPod(), ErrorCode.CONSOL_POD);
        ValidateUtil.notNull(consol.getPod().getId(), ErrorCode.CONSOL_POD);
        PortMaster pod = portMasterRepository.findById(consol.getPod().getId());
        PortMaster pol = portMasterRepository.findById(consol.getPol().getId());
        ServiceMaster serviceMaster = serviceMasterRepository.getOne(consol.getServiceMaster().getId());

        ValidateUtil.notNullOrEmpty(pod.getPortCode(), ErrorCode.CONSOL_POD);

        ValidateUtil.isStatusBlocked(pod.getStatus(), ErrorCode.CONSOL_POD_BLOCK);
        ValidateUtil.isStatusHidden(pod.getStatus(), ErrorCode.CONSOL_POD_HIDE);

        ValidateUtil.isNotEqualTransportMode(pod.getTransportMode(), serviceMaster.getTransportMode(), ErrorCode.CONSOL_POD_SERVICE_TRANSPORT_INVALID);

        if (Objects.equals(pod.getId(), pol.getId())) {
            throw new RestException(ErrorCode.CONSOL_POL_POD_EQUAL);
        }

        if (pod.getPortCode().equals(pol.getPortCode())) {
            throw new RestException(ErrorCode.CONSOL_POL_POD_EQUAL);
        }

        consol.setPodCode(pod.getPortCode());
    }

    private void verifyPol(Consol consol) {
        ValidateUtil.notNull(consol.getPol(), ErrorCode.CONSOL_POL);
        ValidateUtil.notNull(consol.getPol().getId(), ErrorCode.CONSOL_POL);
        PortMaster pol = portMasterRepository.findById(consol.getPol().getId());
        ServiceMaster serviceMaster = serviceMasterRepository.getOne(consol.getServiceMaster().getId());

        ValidateUtil.notNullOrEmpty(pol.getPortCode(), ErrorCode.CONSOL_POL);

        ValidateUtil.isStatusBlocked(pol.getStatus(), ErrorCode.CONSOL_POL_BLOCK);
        ValidateUtil.isStatusHidden(pol.getStatus(), ErrorCode.CONSOL_POL_HIDE);

        ValidateUtil.isNotEqualTransportMode(pol.getTransportMode(), serviceMaster.getTransportMode(), ErrorCode.CONSOL_POL_SERVICE_TRANSPORT_INVALID);


        consol.setPolCode(pol.getPortCode());
    }

    private void verifyDestination(Consol consol) {
        ValidateUtil.notNull(consol.getDestination(), ErrorCode.CONSOL_DESTINATION);
        ValidateUtil.notNull(consol.getDestination().getId(), ErrorCode.CONSOL_DESTINATION);

        PortMaster destination = portMasterRepository.findById(consol.getDestination().getId());
        ServiceMaster serviceMaster = serviceMasterRepository.getOne(consol.getServiceMaster().getId());
        PortMaster origin = portMasterRepository.findById(consol.getOrigin().getId());

        ValidateUtil.notNullOrEmpty(destination.getPortCode(), ErrorCode.CONSOL_DESTINATION);

        ValidateUtil.isStatusBlocked(destination.getStatus(), ErrorCode.CONSOL_DESTINATION_BLOCK);
        ValidateUtil.isStatusHidden(destination.getStatus(), ErrorCode.CONSOL_DESTINATION_HIDE);

        ValidateUtil.isNotEqualTransportMode(destination.getTransportMode(), serviceMaster.getTransportMode(), ErrorCode.CONSOL_DESTINATION_SERVICE_TRANSPORT_INVALID);

        if (Objects.equals(consol.getDestination().getId(), consol.getOrigin().getId())) {
            throw new RestException(ErrorCode.CONSOL_ORIGIN_DESTINATION_EQUAL);
        }

        if (destination.getPortCode().equals(origin.getPortCode())) {
            throw new RestException(ErrorCode.CONSOL_ORIGIN_DESTINATION_EQUAL);
        }

        consol.setDestinationCode(destination.getPortCode());

    }

    private void verifyOrigin(Consol consol) {
        ValidateUtil.notNull(consol.getOrigin(), ErrorCode.CONSOL_ORIGIN);
        ValidateUtil.notNull(consol.getOrigin().getId(), ErrorCode.CONSOL_ORIGIN);
        PortMaster origin = portMasterRepository.findById(consol.getOrigin().getId());
        ServiceMaster serviceMaster = serviceMasterRepository.getOne(consol.getServiceMaster().getId());

        ValidateUtil.notNullOrEmpty(origin.getPortCode(), ErrorCode.CONSOL_ORIGIN);

        ValidateUtil.isStatusBlocked(origin.getStatus(), ErrorCode.CONSOL_ORIGIN_BLOCK);
        ValidateUtil.isStatusHidden(origin.getStatus(), ErrorCode.CONSOL_ORIGIN_HIDE);

        ValidateUtil.isNotEqualTransportMode(origin.getTransportMode(), serviceMaster.getTransportMode(), ErrorCode.CONSOL_ORIGIN_SERVICE_TRANSPORT_INVALID);


        consol.setOriginCode(origin.getPortCode());
    }

    private void verifyAgent(Consol consol) {
        if (consol.getDirectShipment() == YesNo.No) {

            ValidateUtil.notNull(consol.getAgent(), ErrorCode.CONSOL_AGENT);
            ValidateUtil.notNull(consol.getAgent().getId(), ErrorCode.CONSOL_AGENT);

            PartyMaster tmpParty = cacheRepository.get(SaaSUtil.getSaaSId(), PartyMaster.class.getName(), consol.getAgent().getId(), partyMasterRepository);

            ValidateUtil.notNullOrEmpty(tmpParty.getPartyCode(), ErrorCode.CONSOL_AGENT);

            ValidateUtil.isStatusBlocked(tmpParty.getStatus(), ErrorCode.CONSOL_AGENT_BLOCK);
            ValidateUtil.isStatusHidden(tmpParty.getStatus(), ErrorCode.CONSOL_AGENT_HIDE);

            ValidateUtil.isDefaulter(tmpParty, ErrorCode.CONSOL_PARTY_DEFAULTER);

            if (consol.getAgentAddress() != null) {

                if (consol.getAgentAddress().getAddressLine1() != null && consol.getAgentAddress().getAddressLine1().trim().length() != 0)
                    ValidateUtil.validateLength(consol.getAgentAddress().getAddressLine1(), 1, 100, ErrorCode.CONSOL_AGENT_ADDRESS_1_LENGTH);

                if (consol.getAgentAddress().getAddressLine2() != null && consol.getAgentAddress().getAddressLine2().trim().length() != 0)
                    ValidateUtil.validateLength(consol.getAgentAddress().getAddressLine2(), 1, 100, ErrorCode.CONSOL_AGENT_ADDRESS_2_LENGTH);

                if (consol.getAgentAddress().getAddressLine3() != null && consol.getAgentAddress().getAddressLine3().trim().length() != 0)
                    ValidateUtil.validateLength(consol.getAgentAddress().getAddressLine3(), 1, 100, ErrorCode.CONSOL_AGENT_ADDRESS_3_LENGTH);

                if (consol.getAgentAddress().getAddressLine4() != null && consol.getAgentAddress().getAddressLine4().trim().length() != 0)
                    ValidateUtil.validateLength(consol.getAgentAddress().getAddressLine4(), 1, 100, ErrorCode.CONSOL_AGENT_ADDRESS_4_LENGTH);

            }

            consol.setAgentCode(tmpParty.getPartyCode());
            if (consol.getAgentAddress().getCity() != null) {
                consol.getAgentAddress().setCityCode(consol.getAgentAddress().getCity().getCityCode());
            }

            if (consol.getAgentAddress().getState() != null) {
                consol.getAgentAddress().setStateCode(consol.getAgentAddress().getStateCode());
            }

            if (consol.getAgentAddress().getCountry() != null) {
                consol.getAgentAddress().setCountryCode(consol.getAgentAddress().getCountryCode());
            }
        } else {
            consol.setAgent(null);
            consol.setAgentCode(null);
            consol.setAgentAddress(null);
        }
    }

    private void verifyService(Consol consol) {
        ValidateUtil.notNull(consol.getServiceMaster(), ErrorCode.CONSOL_SERVICE);
        ValidateUtil.notNull(consol.getServiceMaster().getId(), ErrorCode.CONSOL_SERVICE);
        ServiceMaster serviceMaster = serviceMasterRepository.getOne(consol.getServiceMaster().getId());

        ValidateUtil.notNullOrEmpty(serviceMaster.getServiceCode(), ErrorCode.CONSOL_SERVICE);
        ValidateUtil.isStatusBlocked(serviceMaster.getStatus(), ErrorCode.CONSOL_SERVICE_BLOCK);
        ValidateUtil.isStatusHidden(serviceMaster.getStatus(), ErrorCode.CONSOL_SERVICE_HIDE);

        consol.setServiceCode(serviceMaster.getServiceCode());
    }

    private void verifyCountry(Consol consol) {
        ValidateUtil.notNull(consol.getCountry(), ErrorCode.CONSOL_COUNTRY);
        ValidateUtil.notNull(consol.getCountry().getId(), ErrorCode.CONSOL_COUNTRY);

        CountryMaster country = countryMasterRepository.findById(consol.getCountry().getId());

        ValidateUtil.notNullOrEmpty(country.getCountryCode(), ErrorCode.CONSOL_COUNTRY);
        ValidateUtil.isStatusBlocked(country.getStatus(), ErrorCode.CONSOL_COUNTRY_BLOCK);
        ValidateUtil.isStatusHidden(country.getStatus(), ErrorCode.CONSOL_COUNTRY_HIDE);

        consol.setCountryCode(country.getCountryCode());
    }

    private void verifyLocation(Consol consol) {
        ValidateUtil.notNull(consol.getLocation(), ErrorCode.CONSOL_LOCATION);
        ValidateUtil.notNull(consol.getLocation().getId(), ErrorCode.CONSOL_LOCATION);
        LocationMaster location = locationMasterRepository.findById(consol.getLocation().getId());

        ValidateUtil.notNullOrEmpty(location.getLocationCode(), ErrorCode.CONSOL_LOCATION);
        ValidateUtil.isStatusBlocked(location.getStatus(), ErrorCode.CONSOL_LOCATION_BLOCK);
        ValidateUtil.isStatusHidden(location.getStatus(), ErrorCode.CONSOL_LOCATION_HIDE);

        consol.setLocationCode(location.getLocationCode());
    }

    private void verifyCompany(Consol consol) {
        ValidateUtil.notNull(consol.getCompany(), ErrorCode.CONSOL_COMPANY);
        ValidateUtil.notNull(consol.getCompany().getId(), ErrorCode.CONSOL_COMPANY);
        CompanyMaster company = companyMasterRepository.findById(consol.getCompany().getId());

        ValidateUtil.notNullOrEmpty(company.getCompanyCode(), ErrorCode.CONSOL_COMPANY);
        ValidateUtil.isStatusBlocked(company.getStatus(), ErrorCode.CONSOL_COMPANY_BLOCK);
        ValidateUtil.isStatusHidden(company.getStatus(), ErrorCode.CONSOL_COMPANY_HIDE);

        consol.setCompanyCode(company.getCompanyCode());
    }

    private void verifyCreatedBy(Consol consol) {
        ValidateUtil.notNull(consol.getCreatedBy(), ErrorCode.CONSOL_CREATED_BY);
        ValidateUtil.notNull(consol.getCreatedBy().getId(), ErrorCode.CONSOL_CREATED_BY);

        EmployeeMaster createdBy = employeeMasterRepository.findById(consol.getCreatedBy().getId());

        ValidateUtil.notNullOrEmpty(createdBy.getEmployeeCode(), ErrorCode.CONSOL_CREATED_BY);

        ValidateUtil.isEmployeeResigned(createdBy.getEmployementStatus(), ErrorCode.CONSOL_CREATED_BY_RESIGED);
        ValidateUtil.isEmployeeTerminated(createdBy.getEmployementStatus(), ErrorCode.CONSOL_CREATED_BY_TERMINATED);

        consol.setCreatedByCode(createdBy.getEmployeeCode());
    }

    private void verifyCarrier(Consol consol) {

        ValidateUtil.notNull(consol.getCarrier(), ErrorCode.CONSOL_CARRIER_MANDATORY);
        ValidateUtil.notNull(consol.getCarrier().getId(), ErrorCode.CONSOL_CARRIER_MANDATORY);

        CarrierMaster carrier = carrierMasterRepository.findById(consol.getCarrier().getId());

        ValidateUtil.notNullOrEmpty(carrier.getCarrierCode(), ErrorCode.CONSOL_CARRIER_MANDATORY);

        ValidateUtil.isStatusBlocked(carrier.getStatus(), ErrorCode.CONSOL_CARRIER_BLOCK);
        ValidateUtil.isStatusHidden(carrier.getStatus(), ErrorCode.CONSOL_CARRIER_HIDE);

        consol.setCarrierCode(carrier.getCarrierCode());

        if (consol.getFirstCarrier() == null) {
            consol.setFirstCarrier(consol.getCarrier());
        }
        if (consol.getFirstcarrierNo() == null) {
            consol.setFirstcarrierNo(carrier.getCarrierNo());
        }
    }


    private void verifyFlightNo(Consol consol) {

        ValidateUtil.notNullOrEmpty(consol.getConsolDocument().getRouteNo(), ErrorCode.CONSOL_FLIGHT_NO_MANDATORY);

    }

    private void verifyEtd(Consol consol) {
        ValidateUtil.notNull(consol.getEtd(), ErrorCode.CONSOL_ETD_MANDATORY);
    }

    private void verifyEta(Consol consol) {
        ValidateUtil.notNull(consol.getEta(), ErrorCode.CONSOL_ETA_MANDATORY);
    }

    private void verifyAtd(Consol consol) {
        if (consol.getAtd() != null) {
            isCargoLoaded(consol);
        }
    }

    private void isCargoLoaded(Consol consol) {
        if (consol.getServiceMailTrigger() == null) {
            ServiceMailTrigger trigger = new ServiceMailTrigger();
            trigger.setIsCargoLoaded(YesNo.Yes);
            consol.setServiceMailTrigger(trigger);
        } else {
            consol.getServiceMailTrigger().setIsCargoLoaded(YesNo.Yes);
        }

    }


    public void isConsolCreatedFromShipment(String serviceUid) {
        ShipmentLink shipmentLink = shipmentLinkRepository.findByServiceUid(serviceUid);
        if (shipmentLink != null) {
            log.error("Master shipment is already created from shipment");
            throw new RestException(ErrorCode.CONSOL_ALREADY_CREATED_FROM_SHIPMENT);
        }

    }
}