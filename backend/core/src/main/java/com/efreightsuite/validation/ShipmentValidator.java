package com.efreightsuite.validation;

import java.util.regex.Pattern;

import com.efreightsuite.common.component.CacheRepository;
import com.efreightsuite.enumeration.ImportExport;
import com.efreightsuite.enumeration.ServiceStatus;
import com.efreightsuite.enumeration.YesNo;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.model.*;
import com.efreightsuite.repository.*;
import com.efreightsuite.service.RegularExpressionService;
import com.efreightsuite.util.AppUtil;
import com.efreightsuite.util.TimeUtil;
import com.efreightsuite.util.ValidateUtil;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author user
 */
@Service
@Log4j2
public class ShipmentValidator {

    @Autowired
    private
    AppUtil appUtil;

    @Autowired
    RegularExpressionService regExpService;

    @Autowired
    CacheRepository cacheRepository;

    @Autowired
    PartyMasterRepository partyMasterRepository;

    @Autowired
    private
    ShipmentServiceDetailValidator shipmentServiceDetailValidator;

    @Autowired
    private
    DocumentValidator documentValidator;

    @Autowired
    private
    ServiceDimensionValidator serviceDimensionValidator;

    @Autowired
    private
    ShipmentChargeValidator shipmentChargeValidator;

    @Autowired
    private
    ServiceConnectionValidator serviceConnectionValidator;

    @Autowired
    private
    ShipmentAttachmentValidator shipmentAttachmentValidator;

    @Autowired
    private
    ShipmentServiceTriggerValidator shipmentServiceTriggerValidator;

    @Autowired
    private
    ShipmentServiceEventValidator shipmentServiceEventValidator;

    @Autowired
    private
    ShipmentServiceReferenceValidator shipmentServiceReferenceValidator;

    @Autowired
    private
    AuthenticatedDocsValidator authenticatedDocsValidator;

    @Autowired
    DefaultMasterDataRepository defaultMasterDataRepository;

    @Autowired
    PortMasterRepository portMasterRepository;

    @Autowired
    private
    BillOfEntryValidator billOfEntryValidator;

    @Autowired
    private
    LocationMasterRepository locationMasterRepository;

    @Autowired
    private
    CompanyMasterRepository companyMasterRepository;

    @Autowired
    private
    EmployeeMasterRepository employeeMasterRepository;

    @Autowired
    private
    CfsMasterRepository cfsMasterRepository;


    @Autowired
    ShipmentServiceDetailRepository shipmentServiceDetailRepository;


    public void validate(Shipment shipment) {
        verifyLocation(shipment);
        verifyCompany(shipment);
        verifyShipmentReqDate(shipment);
        verifyCreatedBy(shipment);
        verifyParty(shipment);
        verifyPartyAddress(shipment);

        // This two methods are commented based on the requirement (3.1.2017)
        /*verifyOrigin(shipment);
		verifyDestination(shipment);*/

        verifyProject(shipment);
        verifyTos(shipment);
        verifyCommodity(shipment);
        verifyWhoRouted(shipment);
        verifyAesNo(shipment);

        if (shipment.getShipmentServiceList() == null || shipment.getShipmentServiceList().size() == 0) {
            throw new RestException(ErrorCode.SHIPMENT_SERVICE_REQ);
        }

        // To set the shipment level origin and destination port group of first service in the shipment.

        setShipmentOriginDestination(shipment);
        log.info("setShipmentOriginDestination called");
        for (int i = 0; i < shipment.getShipmentServiceList().size(); i++) {

            ShipmentServiceDetail service = shipment.getShipmentServiceList().get(i);

            if (service.getId() != null && service.getLastUpdatedStatus().getServiceStatus().equals(ServiceStatus.Cancelled)) {
                continue;
            }

            shipmentServiceDetailValidator.validate(shipment, service);
            service.setOverDimension(YesNo.No);
            if (service.getDocumentList() != null && service.getDocumentList().size() != 0) {
                for (DocumentDetail document : service.getDocumentList()) {
                    documentValidator.validate(document, service, shipment);
                    if (document.getDimensionList() != null && document.getDimensionList().size() != 0) {
                        for (ServiceDocumentDimension dimension : document.getDimensionList()) {
                            serviceDimensionValidator.validate(shipment, document, dimension, service);
                        }
                    }

                    if (document.getPickUpDeliveryPoint() != null) {
                        String emailSplit = appUtil.getLocationConfig("single.text.split.by.multiple.email", service.getLocation(), false);
                        verifyTransporter(document.getPickUpDeliveryPoint());
                        verifyPickUp(document.getPickUpDeliveryPoint(), emailSplit);
                        verifyDelivery(document.getPickUpDeliveryPoint(), emailSplit);
                        verifyDoorDelivery(document.getPickUpDeliveryPoint(), emailSplit);
                    }

                }
            }

            if (service.getShipmentChargeList() != null && service.getShipmentChargeList().size() != 0) {
                for (ShipmentCharge shipmentCharge : service.getShipmentChargeList()) {
                    shipmentChargeValidator.validate(shipmentCharge, service);
                }
            } else {
                service.setShipmentChargeList(null);
            }

            if (service.getConnectionList() != null && service.getConnectionList().size() != 0) {
                for (ServiceConnection sc : service.getConnectionList()) {
                    serviceConnectionValidator.validate(sc);
                }
            } else {

                String defaultMasterData = appUtil.getLocationConfig("connection.required", service.getLocation(), true);

                if (defaultMasterData != null) {

                    if (defaultMasterData.equals("TRUE")) {
                        if (service.getOrigin() != null && service.getOrigin().getId() != null && service.getPol() != null && service.getPol().getId() != null) {
                            if (!service.getOrigin().getId().equals(service.getPol().getId())) {
                                throw new RestException(ErrorCode.CONNECTION_LIST_REQUIRED);
                            }
                        }
                        if (service.getDestination() != null && service.getDestination().getId() != null && service.getPod() != null && service.getPod().getId() != null) {
                            if (!service.getDestination().getId().equals(service.getPod().getId())) {
                                throw new RestException(ErrorCode.CONNECTION_LIST_REQUIRED);
                            }
                        }
                    }
                }


            }

            if (service.getConnectionList() == null || service.getConnectionList().size() == 0) {
                service.setConnectionList(null);
            }
			
			
			/*Transhipment validate - start here */

            boolean transhipment = false;

            if (service.getOrigin() != null && service.getOrigin().getId() != null && service.getPol() != null && service.getPol().getId() != null) {
                if (!service.getOrigin().getId().equals(service.getPol().getId())) {
                    transhipment = true;
                }
            }
            if (service.getDestination() != null && service.getDestination().getId() != null && service.getPod() != null && service.getPod().getId() != null) {
                if (!service.getDestination().getId().equals(service.getPod().getId())) {
                    transhipment = true;
                }
            }

            if (transhipment) {
                service.setIsTranshipment(YesNo.Yes);
                if (service.getConnectionList() == null || service.getConnectionList().size() == 0) {
                    throw new RestException(ErrorCode.TRANSHIPMENT_CONNECTION_LIST_REQUIRED);
                }

            } else {
                service.setIsTranshipment(YesNo.No);
            }
			
			/* Transhipment validate - End Here */


            if (service.getShipmentAttachmentList() != null && service.getShipmentAttachmentList().size() != 0) {
                for (ShipmentAttachment shipmentAttachment : service.getShipmentAttachmentList()) {
                    shipmentAttachmentValidator.validate(shipmentAttachment, service);
                }
            } else {
                service.setShipmentAttachmentList(null);
            }

            if (service.getShipmentServiceTriggerList() != null && service.getShipmentServiceTriggerList().size() != 0) {
                for (ShipmentServiceTrigger shipmentServiceTrigger : service.getShipmentServiceTriggerList()) {
                    shipmentServiceTriggerValidator.validate(shipmentServiceTrigger);
                }
            } else {
                service.setShipmentServiceTriggerList(null);
            }

            if (service.getEventList() != null && service.getEventList().size() != 0) {
                for (ShipmentServiceEvent event : service.getEventList()) {
                    shipmentServiceEventValidator.validate(event);
                }
            } else {
                service.setEventList(null);
            }

            if (service.getReferenceList() != null && service.getReferenceList().size() != 0) {
                for (ShipmentServiceReference reference : service.getReferenceList()) {
                    shipmentServiceReferenceValidator.validate(reference);
                }
            } else {
                service.setReferenceList(null);
            }
            if (service.getAuthenticatedDocList() != null && service.getAuthenticatedDocList().size() != 0) {
                for (AuthenticatedDocs authenticatedDocs : service.getAuthenticatedDocList()) {
                    authenticatedDocsValidator.validate(authenticatedDocs, service);
                }
            } else {
                service.setAuthenticatedDocList(null);
            }

            if (service.getBillOfEntryList() != null && service.getBillOfEntryList().size() != 0) {
                for (BillOfEntry billOfEntry : service.getBillOfEntryList()) {
                    billOfEntryValidator.validate(billOfEntry, service);
                }
            } else {
                service.setBillOfEntryList(null);
            }

            if (service.getServiceMaster().getImportExport() == ImportExport.Export) {
                setWeight(service);
            }
        }
        log.info("End of Validate in ShipmentValidator class:");
    }

    private void verifyAesNo(Shipment shipment) {
		/*//NEW SHIPMENT COMMENTS
    if(shipment.getAesNo()!=null){
    	regExpService.validate(shipment.getAesNo(), RegExpName.Reg_Exp_AES_NO, ErrorCode.AES_NO_INVALID);
		ValidateUtil.validateLength(shipment.getAesNo(), 1, 30, ErrorCode.AES_NO_INVALID);
      }*/
    }


    private void setShipmentOriginDestination(Shipment shipment) {

        log.info("setShipmentOriginDestination is called");
        if (shipment.getShipmentServiceList() != null && !shipment.getShipmentServiceList().isEmpty()) {
            if (shipment.getShipmentServiceList().get(0) != null) {/*
				//NEW SHIPMENT COMMENTS
				if(shipment.getShipmentServiceList().get(0).getOrigin() != null) {
					PortGroupMaster portGroupMasterOrigin = portMasterRepository.getPortGroupByPortId(shipment.getShipmentServiceList().get(0).getOrigin().getId());
					if(portGroupMasterOrigin != null) {
						shipment.setOrigin(portGroupMasterOrigin);
						shipment.setOriginCode(portGroupMasterOrigin.getPortGroupCode());	
					}
				}
				
				if(shipment.getShipmentServiceList().get(0).getDestination() != null) {
					PortGroupMaster portGroupMasterDestination = portMasterRepository.getPortGroupByPortId(shipment.getShipmentServiceList().get(0).getDestination().getId());
					
					if(portGroupMasterDestination != null) {
						shipment.setDestination(portGroupMasterDestination);
						shipment.setDestinationCode(portGroupMasterDestination.getPortGroupCode());	
					}
				}
			*/
            }
        }
    }

    private void verifyLocation(Shipment service) {
        ValidateUtil.notNull(service.getLocation(), ErrorCode.SHIPMENT_LOCATION);
        ValidateUtil.notNull(service.getLocation().getId(), ErrorCode.SHIPMENT_LOCATION);

        LocationMaster location = locationMasterRepository.findById(service.getLocation().getId());

        ValidateUtil.notNullOrEmpty(location.getLocationCode(), ErrorCode.SHIPMENT_LOCATION);
        ValidateUtil.isStatusBlocked(location.getStatus(), ErrorCode.SHIPMENT_LOCATION_BLOCK);
        ValidateUtil.isStatusHidden(location.getStatus(), ErrorCode.SHIPMENT_LOCATION_HIDE);
        service.setLocationCode(location.getLocationCode());

        service.setCountry(location.getCountryMaster());
        service.setCountryCode(location.getCountryMaster().getCountryCode());
    }

    private void verifyCompany(Shipment service) {
        log.info("service--company" + service.getCompanyMaster());
        ValidateUtil.notNull(service.getCompanyMaster(), ErrorCode.SHIPMENT_COMPANY);
        ValidateUtil.notNull(service.getCompanyMaster().getId(), ErrorCode.SHIPMENT_COMPANY);
        CompanyMaster company = companyMasterRepository.findById(service.getCompanyMaster().getId());

        ValidateUtil.notNullOrEmpty(company.getCompanyCode(), ErrorCode.SHIPMENT_COMPANY);
        ValidateUtil.isStatusBlocked(company.getStatus(), ErrorCode.SHIPMENT_COMPANY_BLOCK);
        ValidateUtil.isStatusHidden(company.getStatus(), ErrorCode.SHIPMENT_COMPANY_HIDE);

        service.setCompanyCode(company.getCompanyCode());
    }

    private void verifyDoorDelivery(PickUpDeliveryPoint pickUpDelivery, String emailSplit) {

        if (pickUpDelivery.getDoorDeliveryPoint() != null && pickUpDelivery.getDoorDeliveryPoint().getId() != null) {
            ValidateUtil.notNull(pickUpDelivery.getDoorDeliveryPoint(), ErrorCode.SHIPMENT_SERVICE_DOOR_DELIVERY_POINT);
            ValidateUtil.notNull(pickUpDelivery.getDoorDeliveryPoint().getId(), ErrorCode.SHIPMENT_SERVICE_DOOR_DELIVERY_POINT);

            PortMaster doorDeliveryPoint = portMasterRepository.findById(pickUpDelivery.getDoorDeliveryPoint().getId());

            ValidateUtil.notNullOrEmpty(doorDeliveryPoint.getPortCode(), ErrorCode.SHIPMENT_SERVICE_DOOR_DELIVERY_POINT);

            ValidateUtil.isStatusBlocked(doorDeliveryPoint.getStatus(), ErrorCode.SHIPMENT_SERVICE_DOOR_DELIVERY_POINT_BLOCK);
            ValidateUtil.isStatusHidden(doorDeliveryPoint.getStatus(), ErrorCode.SHIPMENT_SERVICE_DOOR_DELIVERY_POINT_HIDE);

            pickUpDelivery.setDoorDeliveryPointCode(doorDeliveryPoint.getPortCode());
        } else {
            pickUpDelivery.setDoorDeliveryPoint(null);
            pickUpDelivery.setDoorDeliveryPointCode(null);
        }

        if (pickUpDelivery.getDoorDeliveryFrom() != null && pickUpDelivery.getDoorDeliveryFrom().getId() != null) {
            ValidateUtil.notNull(pickUpDelivery.getDoorDeliveryFrom(), ErrorCode.SHIPMENT_SERVICE_DOOR_DELIVERY_FROM);
            ValidateUtil.notNull(pickUpDelivery.getDoorDeliveryFrom().getId(), ErrorCode.SHIPMENT_SERVICE_DOOR_DELIVERY_FROM);

            PortMaster doorDeliveryFrom = portMasterRepository.findById(pickUpDelivery.getDoorDeliveryFrom().getId());

            ValidateUtil.notNullOrEmpty(doorDeliveryFrom.getPortCode(), ErrorCode.SHIPMENT_SERVICE_DOOR_DELIVERY_FROM);

            ValidateUtil.isStatusBlocked(doorDeliveryFrom.getStatus(), ErrorCode.SHIPMENT_SERVICE_DOOR_DELIVERY_FROM_BLOCK);
            ValidateUtil.isStatusHidden(doorDeliveryFrom.getStatus(), ErrorCode.SHIPMENT_SERVICE_DOOR_DELIVERY_FROM_HIDE);

            pickUpDelivery.setDoorDeliveryFromCode(doorDeliveryFrom.getPortCode());
        } else {
            pickUpDelivery.setDoorDeliveryFrom(null);
            pickUpDelivery.setDoorDeliveryFromCode(null);
        }

        if (pickUpDelivery.getDoorDeliveryPlace() != null) {
            ValidateUtil.notNull(pickUpDelivery.getDoorDeliveryPlace(), ErrorCode.SHIPMENT_SERVICE_DOOR_DELIVERY_PLACE);
            ValidateUtil.validateLength(pickUpDelivery.getDoorDeliveryPlace(), 1, 500, ErrorCode.SHIPMENT_SERVICE_DOOR_DELIVERY_PLACE);
        } else {
            pickUpDelivery.setDoorDeliveryPlace(null);
        }

        if (pickUpDelivery.getDoorDeliveryContactPerson() != null && pickUpDelivery.getDoorDeliveryContactPerson().trim().length() != 0) {
            ValidateUtil.notNullOrEmpty(pickUpDelivery.getDoorDeliveryContactPerson(), ErrorCode.SHIPMENT_SERVICE_DOOR_DELIVERY_CONTACT_PERSON);
            ValidateUtil.validateLength(pickUpDelivery.getDoorDeliveryContactPerson(), 1, 100, ErrorCode.SHIPMENT_SERVICE_DOOR_DELIVERY_CONTACT_PERSON);
        } else {
            pickUpDelivery.setDoorDeliveryContactPerson(null);
        }

        if (pickUpDelivery.getDoorDeliveryMobileNo() != null && pickUpDelivery.getDoorDeliveryMobileNo().trim().length() != 0) {
            ValidateUtil.notNullOrEmpty(pickUpDelivery.getDoorDeliveryMobileNo(), ErrorCode.SHIPMENT_SERVICE_DOOR_DELIVERY_MOBILE);
            ValidateUtil.validateLength(pickUpDelivery.getDoorDeliveryMobileNo(), 1, 100, ErrorCode.SHIPMENT_SERVICE_DOOR_DELIVERY_MOBILE);
        } else {
            pickUpDelivery.setDoorDeliveryMobileNo(null);
        }

        if (pickUpDelivery.getDoorDeliveryPhoneNo() != null && pickUpDelivery.getDoorDeliveryPhoneNo().trim().length() != 0) {
            ValidateUtil.notNullOrEmpty(pickUpDelivery.getDoorDeliveryPhoneNo(), ErrorCode.SHIPMENT_SERVICE_DOOR_DELIVERY_PHONE);
            ValidateUtil.validateLength(pickUpDelivery.getDoorDeliveryPhoneNo(), 1, 100, ErrorCode.SHIPMENT_SERVICE_DOOR_DELIVERY_PHONE);
        } else {
            pickUpDelivery.setDoorDeliveryPhoneNo(null);
        }

        if (pickUpDelivery.getDoorDeliveryEmail() != null && pickUpDelivery.getDoorDeliveryEmail().trim().length() != 0) {
            ValidateUtil.notNullOrEmpty(pickUpDelivery.getDoorDeliveryEmail(), ErrorCode.SHIPMENT_SERVICE_DOOR_DELIVERY_EMAIL_LENGTH);
            ValidateUtil.validateLength(pickUpDelivery.getDoorDeliveryEmail(), 1, 500, ErrorCode.SHIPMENT_SERVICE_DOOR_DELIVERY_EMAIL_LENGTH);
            for (String email : pickUpDelivery.getDoorDeliveryEmail().split(emailSplit)) {
                if (!Pattern.compile(ValidateUtil.regularExp).matcher(email).matches()) {
                    throw new RestException(ErrorCode.SHIPMENT_SERVICE_DOOR_DELIVERY_EMAIL);
                }
            }
        } else {
            pickUpDelivery.setDoorDeliveryEmail(null);
        }
    }

    private void verifyDelivery(PickUpDeliveryPoint pickUpDelivery, String emailSplit) {

        if (pickUpDelivery.getDeliveryPoint() != null && pickUpDelivery.getDeliveryPoint().getId() != null) {
            ValidateUtil.notNull(pickUpDelivery.getDeliveryPoint(), ErrorCode.SHIPMENT_SERVICE_DELIVERY_POINT);
            ValidateUtil.notNull(pickUpDelivery.getDeliveryPoint().getId(), ErrorCode.SHIPMENT_SERVICE_DELIVERY_POINT);

            CFSMaster deliveryPoint = cfsMasterRepository.getOne(pickUpDelivery.getDeliveryPoint().getId());

            ValidateUtil.notNullOrEmpty(deliveryPoint.getCfsCode(), ErrorCode.SHIPMENT_SERVICE_DELIVERY_POINT);

            ValidateUtil.isStatusBlocked(deliveryPoint.getStatus(), ErrorCode.SHIPMENT_SERVICE_DELIVERY_POINT_BLOCK);
            ValidateUtil.isStatusHidden(deliveryPoint.getStatus(), ErrorCode.SHIPMENT_SERVICE_DELIVERY_POINT_HIDE);

            pickUpDelivery.setDeliveryPointCode(deliveryPoint.getCfsCode());
        } else {
            pickUpDelivery.setDeliveryPoint(null);
            pickUpDelivery.setDeliveryPointCode(null);
        }

        if (pickUpDelivery.getDeliveryFrom() != null && pickUpDelivery.getDeliveryFrom().getId() != null) {
            ValidateUtil.notNull(pickUpDelivery.getDeliveryFrom(), ErrorCode.SHIPMENT_SERVICE_DELIVERY_FROM);
            ValidateUtil.notNull(pickUpDelivery.getDeliveryFrom().getId(), ErrorCode.SHIPMENT_SERVICE_DELIVERY_FROM);

            PortMaster deliveryFrom = portMasterRepository.findById(pickUpDelivery.getDeliveryFrom().getId());

            ValidateUtil.notNullOrEmpty(deliveryFrom.getPortCode(), ErrorCode.SHIPMENT_SERVICE_DELIVERY_FROM);

            ValidateUtil.isStatusBlocked(deliveryFrom.getStatus(), ErrorCode.SHIPMENT_SERVICE_DELIVERY_FROM_BLOCK);
            ValidateUtil.isStatusHidden(deliveryFrom.getStatus(), ErrorCode.SHIPMENT_SERVICE_DELIVERY_FROM_HIDE);

            pickUpDelivery.setDeliveryFromCode(deliveryFrom.getPortCode());
        } else {
            pickUpDelivery.setDeliveryFrom(null);
            pickUpDelivery.setDeliveryFromCode(null);
        }

        if (pickUpDelivery.getDeliveryPlace() != null) {
            ValidateUtil.notNull(pickUpDelivery.getDeliveryPlace(), ErrorCode.SHIPMENT_SERVICE_DELIVERY_PLACE);
            ValidateUtil.validateLength(pickUpDelivery.getDeliveryPlace(), 1, 500, ErrorCode.SHIPMENT_SERVICE_DELIVERY_PLACE);
        } else {
            pickUpDelivery.setDeliveryPlace(null);
        }

        if (pickUpDelivery.getDeliveryContactPerson() != null && pickUpDelivery.getDeliveryContactPerson().trim().length() != 0) {
            ValidateUtil.notNullOrEmpty(pickUpDelivery.getDeliveryContactPerson(), ErrorCode.SHIPMENT_SERVICE_DELIVERY_CONTACT_PERSON);
            ValidateUtil.validateLength(pickUpDelivery.getDeliveryContactPerson(), 1, 100, ErrorCode.SHIPMENT_SERVICE_DELIVERY_CONTACT_PERSON);
        } else {
            pickUpDelivery.setDeliveryContactPerson(null);
        }

        if (pickUpDelivery.getDeliveryMobileNo() != null && pickUpDelivery.getDeliveryMobileNo().trim().length() != 0) {
            ValidateUtil.notNullOrEmpty(pickUpDelivery.getDeliveryMobileNo(), ErrorCode.SHIPMENT_SERVICE_DELIVERY_MOBILE);
            ValidateUtil.validateLength(pickUpDelivery.getDeliveryMobileNo(), 1, 100, ErrorCode.SHIPMENT_SERVICE_DELIVERY_MOBILE);
        } else {
            pickUpDelivery.setDeliveryMobileNo(null);
        }

        if (pickUpDelivery.getDeliveryPhoneNo() != null && pickUpDelivery.getDeliveryPhoneNo().trim().length() != 0) {
            ValidateUtil.notNullOrEmpty(pickUpDelivery.getDeliveryPhoneNo(), ErrorCode.SHIPMENT_SERVICE_DELIVERY_PHONE);
            ValidateUtil.validateLength(pickUpDelivery.getDeliveryPhoneNo(), 1, 100, ErrorCode.SHIPMENT_SERVICE_DELIVERY_PHONE);
        } else {
            pickUpDelivery.setDeliveryPhoneNo(null);
        }

        if (pickUpDelivery.getDeliveryEmail() != null && pickUpDelivery.getDeliveryEmail().trim().length() != 0) {
            ValidateUtil.notNullOrEmpty(pickUpDelivery.getDeliveryEmail(), ErrorCode.SHIPMENT_SERVICE_DELIVERY_EMAIL_LENGTH);
            ValidateUtil.validateLength(pickUpDelivery.getDeliveryEmail(), 1, 500, ErrorCode.SHIPMENT_SERVICE_DELIVERY_EMAIL_LENGTH);
            for (String email : pickUpDelivery.getDeliveryEmail().split(emailSplit)) {
                if (!Pattern.compile(ValidateUtil.regularExp).matcher(email).matches()) {
                    throw new RestException(ErrorCode.SHIPMENT_SERVICE_DELIVERY_EMAIL);
                }
            }
        } else {
            pickUpDelivery.setDeliveryEmail(null);
        }
    }

    private void verifyPickUp(PickUpDeliveryPoint pickUpDelivery, String emailSplit) {

        if (pickUpDelivery.getPickupPoint() != null && pickUpDelivery.getPickupPoint().getId() != null) {
            ValidateUtil.notNull(pickUpDelivery.getPickupPoint(), ErrorCode.SHIPMENT_SERVICE_PICKUP_POINT);
            ValidateUtil.notNull(pickUpDelivery.getPickupPoint().getId(), ErrorCode.SHIPMENT_SERVICE_PICKUP_POINT);
            PartyMaster pickUpPoint = partyMasterRepository.findById(pickUpDelivery.getPickupPoint().getId());


            ValidateUtil.notNullOrEmpty(pickUpPoint.getPartyCode(), ErrorCode.SHIPMENT_SERVICE_PICKUP_POINT);

            ValidateUtil.isStatusBlocked(pickUpPoint.getStatus(), ErrorCode.SHIPMENT_SERVICE_PICKUP_POINT_BLOCK);
            ValidateUtil.isStatusHidden(pickUpPoint.getStatus(), ErrorCode.SHIPMENT_SERVICE_PICKUP_POINT_HIDE);

            pickUpDelivery.setPickupPointCode(pickUpPoint.getPartyCode());
        } else {
            pickUpDelivery.setPickupPoint(null);
            pickUpDelivery.setPickupPointCode(null);
        }

        if (pickUpDelivery.getPickupFrom() != null && pickUpDelivery.getPickupFrom().getId() != null) {
            ValidateUtil.notNull(pickUpDelivery.getPickupFrom(), ErrorCode.SHIPMENT_SERVICE_PICKUP_FROM);
            ValidateUtil.notNull(pickUpDelivery.getPickupFrom().getId(), ErrorCode.SHIPMENT_SERVICE_PICKUP_FROM);
            PortMaster pickupFrom = portMasterRepository.findById(pickUpDelivery.getPickupFrom().getId());


            ValidateUtil.notNullOrEmpty(pickupFrom.getPortCode(), ErrorCode.SHIPMENT_SERVICE_PICKUP_FROM);

            ValidateUtil.isStatusBlocked(pickupFrom.getStatus(), ErrorCode.SHIPMENT_SERVICE_PICKUP_FROM_BLOCK);
            ValidateUtil.isStatusHidden(pickupFrom.getStatus(), ErrorCode.SHIPMENT_SERVICE_PICKUP_FROM_HIDE);

            pickUpDelivery.setPickupFromCode(pickupFrom.getPortCode());
        } else {
            pickUpDelivery.setPickupFrom(null);
            pickUpDelivery.setPickupFromCode(null);
        }

        if (pickUpDelivery.getPickUpPlace() != null) {
            ValidateUtil.notNull(pickUpDelivery.getPickUpPlace(), ErrorCode.SHIPMENT_SERVICE_PICKUP_PLACE);
            ValidateUtil.validateLength(pickUpDelivery.getPickUpPlace(), 1, 500, ErrorCode.SHIPMENT_SERVICE_PICKUP_PLACE);
        } else {
            pickUpDelivery.setPickUpPlace(null);
        }

        if (pickUpDelivery.getPickUpContactPerson() != null && pickUpDelivery.getPickUpContactPerson().trim().length() != 0) {
            ValidateUtil.notNullOrEmpty(pickUpDelivery.getPickUpContactPerson(), ErrorCode.SHIPMENT_SERVICE_PICKUP_CONTACT_PERSON);
            ValidateUtil.validateLength(pickUpDelivery.getPickUpContactPerson(), 1, 100, ErrorCode.SHIPMENT_SERVICE_PICKUP_CONTACT_PERSON);
        } else {
            pickUpDelivery.setPickUpContactPerson(null);
        }

        if (pickUpDelivery.getPickUpMobileNo() != null && pickUpDelivery.getPickUpMobileNo().trim().length() != 0) {
            ValidateUtil.notNullOrEmpty(pickUpDelivery.getPickUpMobileNo(), ErrorCode.SHIPMENT_SERVICE_PICKUP_MOBILE);
            ValidateUtil.validateLength(pickUpDelivery.getPickUpMobileNo(), 1, 100, ErrorCode.SHIPMENT_SERVICE_PICKUP_MOBILE);
        } else {
            pickUpDelivery.setPickUpMobileNo(null);
        }

        if (pickUpDelivery.getPickUpPhoneNo() != null && pickUpDelivery.getPickUpPhoneNo().trim().length() != 0) {
            ValidateUtil.notNullOrEmpty(pickUpDelivery.getPickUpPhoneNo(), ErrorCode.SHIPMENT_SERVICE_PICKUP_PHONE);
            ValidateUtil.validateLength(pickUpDelivery.getPickUpPhoneNo(), 1, 100, ErrorCode.SHIPMENT_SERVICE_PICKUP_PHONE);
        } else {
            pickUpDelivery.setPickUpPhoneNo(null);
        }

        if (pickUpDelivery.getPickUpEmail() != null && pickUpDelivery.getPickUpEmail().trim().length() != 0) {
            ValidateUtil.notNullOrEmpty(pickUpDelivery.getPickUpEmail(), ErrorCode.SHIPMENT_SERVICE_PICKUP_EMAIL_LENGTH);
            ValidateUtil.validateLength(pickUpDelivery.getPickUpEmail(), 1, 500, ErrorCode.SHIPMENT_SERVICE_PICKUP_EMAIL_LENGTH);
            for (String email : pickUpDelivery.getPickUpEmail().split(emailSplit)) {
                if (!Pattern.compile(ValidateUtil.regularExp).matcher(email).matches()) {
                    throw new RestException(ErrorCode.SHIPMENT_SERVICE_PICKUP_EMAIL);
                }
            }
        } else {
            pickUpDelivery.setPickUpEmail(null);
        }
    }


    private void verifyTransporter(PickUpDeliveryPoint pickUpDelivery) {
        if (pickUpDelivery.getTransporter() != null && pickUpDelivery.getTransporter().getId() != null) {
            ValidateUtil.notNull(pickUpDelivery.getTransporter(), ErrorCode.SHIPMENT_TRANSPORTER);
            ValidateUtil.notNull(pickUpDelivery.getTransporter().getId(), ErrorCode.SHIPMENT_TRANSPORTER);
            PartyMaster transporter = partyMasterRepository.findById(pickUpDelivery.getTransporter().getId());

            ValidateUtil.notNullOrEmpty(transporter.getPartyCode(), ErrorCode.SHIPMENT_TRANSPORTER);

            ValidateUtil.isStatusBlocked(transporter.getStatus(), ErrorCode.SHIPMENT_TRANSPORTER_BLOCK);
            ValidateUtil.isStatusHidden(transporter.getStatus(), ErrorCode.SHIPMENT_TRANSPORTER_HIDE);

            //ValidateUtil.notNull(pickUpDelivery.getTransportAddress(), ErrorCode.SHIPMENT_TRANSPORTER_ADDRESS);
			/*ValidateUtil.notNullOrEmpty(pickUpDelivery.getTransportAddress().getAddressLine1(), ErrorCode.SHIPMENT_TRANSPORTER_ADDRESS_1);
			ValidateUtil.validateLength(pickUpDelivery.getTransportAddress().getAddressLine1(), 1, 100, ErrorCode.SHIPMENT_TRANSPORTER_ADDRESS_1_LENGTH);
			ValidateUtil.notNullOrEmpty(pickUpDelivery.getTransportAddress().getAddressLine2(), ErrorCode.SHIPMENT_TRANSPORTER_ADDRESS_2);
			ValidateUtil.validateLength(pickUpDelivery.getTransportAddress().getAddressLine2(), 1, 100, ErrorCode.SHIPMENT_TRANSPORTER_ADDRESS_2_LENGTH);

			if(pickUpDelivery.getTransportAddress().getAddressLine3()!=null && pickUpDelivery.getTransportAddress().getAddressLine3().trim().length()!=0)
				ValidateUtil.validateLength(pickUpDelivery.getTransportAddress().getAddressLine3(), 1, 100, ErrorCode.SHIPMENT_TRANSPORTER_ADDRESS_3_LENGTH);
			
			if(pickUpDelivery.getTransportAddress().getAddressLine4()!=null && pickUpDelivery.getTransportAddress().getAddressLine4().trim().length()!=0)
			ValidateUtil.validateLength(pickUpDelivery.getTransportAddress().getAddressLine4(), 1, 100, ErrorCode.SHIPMENT_TRANSPORTER_ADDRESS_4_LENGTH);
			*/
			/*if(pickUpDelivery.getTransportAddress().getEmail()!=null && pickUpDelivery.getTransportAddress().getEmail().trim().length()!=0){
				ValidateUtil.validateLength(pickUpDelivery.getTransportAddress().getEmail(), 1, 500, ErrorCode.SHIPMENT_TRANSPORTER_EMAIL_LENGTH);
				for(String email : pickUpDelivery.getTransportAddress().getEmail().split(defaultMasterDataRepository.findByCode("single.text.split.by.multiple.email").getValue())){
					if(!Pattern.compile(ValidateUtil.regularExp).matcher(email).matches()){
						throw new RestException(ErrorCode.SHIPMENT_TRANSPORTER_EMAIL_LENGTH);
					}
				}
			}*/


            pickUpDelivery.setTransporterCode(transporter.getPartyCode());
        } else {
            pickUpDelivery.setTransporter(null);
            pickUpDelivery.setTransporterCode(null);
            pickUpDelivery.setTransportAddress(null);
        }

    }

    private void setWeight(ShipmentServiceDetail service) {


        double volweight = 0.000;
        double grossWeight = 0.000;

        double volpound = 0.000;
        double grosspound = 0.000;

        double chargebleweight = 0.000;

        Double volumeInCbm = null;

        Long noofpieces = 0L;

        if (service.getDocumentList() != null && service.getDocumentList().size() != 0) {
            for (DocumentDetail dd : service.getDocumentList()) {

                if (dd.getVolumeWeight() != null) {
                    volweight += dd.getVolumeWeight();
                }
                grossWeight += dd.getGrossWeight();

                if (dd.getVolumeWeightInPound() != null) {
                    volpound += dd.getVolumeWeightInPound();
                }
                grosspound += dd.getGrossWeightInPound();

                chargebleweight += dd.getChargebleWeight();

                noofpieces += dd.getNoOfPieces();

                if (dd.getBookedVolumeUnitCbm() != null) {
                    if (volumeInCbm == null) {
                        volumeInCbm = 0.0D;
                    }
                    volumeInCbm += dd.getBookedVolumeUnitCbm();
                }
            }

            if (service.getBookedVolumeWeightUnitKg() != null && service.getBookedVolumeWeightUnitKg() != volweight) {
                throw new RestException(ErrorCode.SHIPMENT_VOLUME_KG_MISMATCH);
            }
            if (service.getBookedGrossWeightUnitKg() != grossWeight) {
                throw new RestException(ErrorCode.SHIPMENT_GROSS_KG_MISMATCH);
            }
            if (service.getBookedVolumeWeightUnitPound() != null && service.getBookedVolumeWeightUnitPound() != volpound) {
                throw new RestException(ErrorCode.SHIPMENT_VOLUME_POUND_MISMATCH);
            }
            if (service.getBookedGrossWeightUnitPound() != grosspound) {
                throw new RestException(ErrorCode.SHIPMENT_GROSS_POUND_MISMATCH);
            }
            if (service.getBookedChargeableUnit() != chargebleweight) {
                throw new RestException(ErrorCode.SHIPMENT_CHARGE_KG_MISMATCH);
            }
            if (!service.getBookedPieces().equals(noofpieces)) {
                throw new RestException(ErrorCode.SHIPMENT_PIECES_MISMATCH);
            }
            if (service.getBookedVolumeUnitCbm() != null && volumeInCbm != null) {
                if (service.getBookedVolumeUnitCbm().compareTo(volumeInCbm) != 0) {
                    throw new RestException(ErrorCode.SHIPMENT_VOLUME_CBM_MISMATCH);
                }
            }

        }

    }

    private void verifyWhoRouted(Shipment shipment) {/*
		if(shipment.getWhoRouted().equals(WhoRouted.Self)){
			
			ValidateUtil.notNull(shipment.getSalesman(), ErrorCode.SHIPMENT_SALESMAN);
			ValidateUtil.notNull(shipment.getSalesman().getId(), ErrorCode.SHIPMENT_SALESMAN);
			ValidateUtil.notNullOrEmpty(shipment.getSalesman().getEmployeeCode(), ErrorCode.SHIPMENT_SALESMAN);
			
			ValidateUtil.isEmployeeResigned(shipment.getSalesman().getEmployementStatus(), ErrorCode.SHIPMENT_SALESMAN_RESIGNED);
			ValidateUtil.isEmployeeTerminated(shipment.getSalesman().getEmployementStatus(), ErrorCode.SHIPMENT_SALESMAN_TERMINATED);
			
			ValidateUtil.isSalesman(shipment.getSalesman().getIsSalesman(), ErrorCode.SHIPMENT_SALESMAN_ISSALESMAN);
			
			shipment.setSalesmanCode(shipment.getSalesman().getEmployeeCode());
			
		}else{
            if(shipment.getDirectShipment()!=null && shipment.getDirectShipment().equals(YesNo.No)){			
			ValidateUtil.notNull(shipment.getAgent(), ErrorCode.SHIPMENT_AGENT);
			ValidateUtil.notNull(shipment.getAgent().getId(), ErrorCode.SHIPMENT_AGENT);
			ValidateUtil.notNullOrEmpty(shipment.getAgent().getPartyCode(), ErrorCode.SHIPMENT_AGENT);
			ValidateUtil.isStatusBlocked(shipment.getAgent().getStatus(), ErrorCode.SHIPMENT_AGENT_BLOCK);
			ValidateUtil.isStatusHidden(shipment.getAgent().getStatus(), ErrorCode.SHIPMENT_AGENT_HIDE);
			shipment.setAgentCode(shipment.getAgent().getPartyCode());
             }else{

            	 if(shipment.getAgent()!=null &&shipment.getDirectShipment().equals(YesNo.Yes)){
            		 ValidateUtil.isStatusBlocked(shipment.getAgent().getStatus(), ErrorCode.SHIPMENT_AGENT_BLOCK);
         			ValidateUtil.isStatusHidden(shipment.getAgent().getStatus(), ErrorCode.SHIPMENT_AGENT_HIDE);
         			shipment.setAgentCode(shipment.getAgent().getPartyCode());
            	 }else{
            		 
            		 shipment.setAgentCode(null);
            	 }
            	 
             }
			
		}
	*/
    }

    private void verifyCommodity(Shipment shipment) {/*
	//NEW SHIPMENT COMMENTS
		if(shipment.getCommodity()!=null && shipment.getCommodity().getId()!=null){
			ValidateUtil.notNull(shipment.getCommodity(), ErrorCode.SHIPMENT_COMMODITY);
			ValidateUtil.notNull(shipment.getCommodity().getId(), ErrorCode.SHIPMENT_COMMODITY);
			ValidateUtil.notNullOrEmpty(shipment.getCommodity().getHsCode(), ErrorCode.SHIPMENT_COMMODITY);
			
			ValidateUtil.isStatusBlocked(shipment.getCommodity().getStatus(), ErrorCode.SHIPMENT_COMMODITY_BLOCK);
			ValidateUtil.isStatusHidden(shipment.getCommodity().getStatus(), ErrorCode.SHIPMENT_COMMODITY_HIDE);
			
			shipment.setCommodityCode(shipment.getCommodity().getHsCode());
		}else{
			shipment.setCommodity(null);
			shipment.setCommodityCode(null);
		}
		
	*/
    }

    private void verifyTos(Shipment shipment) {/*//NEW SHIPMENT COMMENTS
		ValidateUtil.notNull(shipment.getTosMaster(), ErrorCode.SHIPMENT_TOS);
		ValidateUtil.notNull(shipment.getTosMaster().getId(), ErrorCode.SHIPMENT_TOS);
		ValidateUtil.notNullOrEmpty(shipment.getTosMaster().getTosCode(), ErrorCode.SHIPMENT_TOS);
		
		ValidateUtil.isStatusBlocked(shipment.getTosMaster().getStatus(), ErrorCode.SHIPMENT_TOS_BLOCK);
		ValidateUtil.isStatusHidden(shipment.getTosMaster().getStatus(), ErrorCode.SHIPMENT_TOS_HIDE);
		
		shipment.setTosCode(shipment.getTosMaster().getTosCode());
	*/
    }

    private void verifyProject(Shipment shipment) {/*
		//NEW SHIPMENT COMMENTS
		if(shipment.getProject()!=null && shipment.getProject().getId()!=null){
			ValidateUtil.notNullOrEmpty(shipment.getProject().getProjectCode(), ErrorCode.SHIPMENT_PROJECT);
			ValidateUtil.isStatusBlocked(shipment.getProject().getStatus(), ErrorCode.SHIPMENT_PROJECT_BLOCK);
			ValidateUtil.isStatusHidden(shipment.getProject().getStatus(), ErrorCode.SHIPMENT_PROJECT_HIDE);
			
			shipment.setProjectCode(shipment.getProject().getProjectCode());
		}else{
			shipment.setProject(null);
			shipment.setProjectCode(null);
		}
	*/
    }

    @SuppressWarnings("unused")
    private void verifyDestination(Shipment shipment) {/*
	//NEW SHIPMENT COMMENTS
		ValidateUtil.notNull(shipment.getDestination(), ErrorCode.SHIPMENT_DESTINATION);
		ValidateUtil.notNull(shipment.getDestination().getId(), ErrorCode.SHIPMENT_DESTINATION);
		ValidateUtil.notNullOrEmpty(shipment.getDestination().getPortGroupCode(), ErrorCode.SHIPMENT_DESTINATION);
		
		//ValidateUtil.isStatusBlocked(shipment.getDestination().getStatus(), ErrorCode.SHIPMENT_DESTINATION_BLOCK);
		//ValidateUtil.isStatusHidden(shipment.getDestination().getStatus(), ErrorCode.SHIPMENT_DESTINATION_HIDE);
		
		//ValidateUtil.isAirTransportMode(shipment.getDestination().getTransportMode(), ErrorCode.SHIPMENT_DESTINATION_AIR_MODE );
		
		if(shipment.getDestination().getId()==shipment.getOrigin().getId()){
			throw new RestException(ErrorCode.SHIPMENT_ORIGIN_DESTINATION_EQUAL);
		}
		
		if(shipment.getDestination().getPortGroupCode().equals(shipment.getOrigin().getPortGroupCode())){
			throw new RestException(ErrorCode.SHIPMENT_ORIGIN_DESTINATION_EQUAL);
		}
		
		shipment.setDestinationCode(shipment.getDestination().getPortGroupCode());
				
	*/
    }


    private void verifyOrigin(Shipment shipment) {/*//NEW SHIPMENT COMMENTS
		
		ValidateUtil.notNull(shipment.getOrigin(), ErrorCode.SHIPMENT_ORIGIN);
		ValidateUtil.notNull(shipment.getOrigin().getId(), ErrorCode.SHIPMENT_ORIGIN);
		ValidateUtil.notNullOrEmpty(shipment.getOrigin().getPortGroupCode(), ErrorCode.SHIPMENT_ORIGIN);
		
		//ValidateUtil.isStatusBlocked(shipment.getOrigin().getStatus(), ErrorCode.SHIPMENT_ORIGIN_BLOCK);
		//ValidateUtil.isStatusHidden(shipment.getOrigin().getStatus(), ErrorCode.SHIPMENT_ORIGIN_HIDE);
		
		//ValidateUtil.isAirTransportMode(shipment.getOrigin().getTransportMode(), ErrorCode.SHIPMENT_ORIGIN_AIR_MODE );
		
				
		shipment.setOriginCode(shipment.getOrigin().getPortGroupCode());*/
    }

    private void verifyPartyAddress(Shipment shipment) {/*//NEW SHIPMENT COMMENTS
		ValidateUtil.notNull(shipment.getPartyAddress(), ErrorCode.SHIPMENT_PARTY_ADDRESS);
		
		ValidateUtil.notNullOrEmpty(shipment.getPartyAddress().getAddressLine1(), ErrorCode.SHIPMENT_PARTY_ADDRESS_1);
		ValidateUtil.validateLength(shipment.getPartyAddress().getAddressLine1(), 1, 100, ErrorCode.SHIPMENT_PARTY_ADDRESS_1_LENGTH);
		if(shipment.getPartyAddress().getAddressLine2()!=null && shipment.getPartyAddress().getAddressLine2().trim().length()!=0) {
			ValidateUtil.notNullOrEmpty(shipment.getPartyAddress().getAddressLine2(), ErrorCode.SHIPMENT_PARTY_ADDRESS_2);
			ValidateUtil.validateLength(shipment.getPartyAddress().getAddressLine2(), 1, 100, ErrorCode.SHIPMENT_PARTY_ADDRESS_2_LENGTH);
		}
		
		
		if(shipment.getPartyAddress().getAddressLine3()!=null && shipment.getPartyAddress().getAddressLine3().trim().length()!=0) {
			ValidateUtil.validateLength(shipment.getPartyAddress().getAddressLine3(), 1, 100, ErrorCode.SHIPMENT_PARTY_ADDRESS_3_LENGTH);
		}
		if(shipment.getPartyAddress().getAddressLine4()!=null && shipment.getPartyAddress().getAddressLine4().trim().length()!=0) {
			ValidateUtil.notNullOrEmpty(shipment.getPartyAddress().getAddressLine4(), ErrorCode.SHIPMENT_PARTY_ADDRESS_4);
			ValidateUtil.validateLength(shipment.getPartyAddress().getAddressLine4(), 1, 100, ErrorCode.SHIPMENT_PARTY_ADDRESS_4_LENGTH);
			
		}
		
	*/
    }

    private void verifyParty(Shipment shipment) {/*//NEW SHIPMENT COMMENTS
		
		PartyMaster tmpParty = cacheRepository.get(SaaSUtil.getSaaSId(), PartyMaster.class.getName(), shipment.getParty().getId(), partyMasterRepository);
		
		ValidateUtil.notNull(tmpParty, ErrorCode.SHIPMENT_PARTY);
		ValidateUtil.notNull(tmpParty.getId(), ErrorCode.SHIPMENT_PARTY);
		ValidateUtil.notNullOrEmpty(tmpParty.getPartyCode(), ErrorCode.SHIPMENT_PARTY);
		ValidateUtil.isStatusBlocked(tmpParty.getStatus(), ErrorCode.SHIPMENT_PARTY_BLOCK);
		ValidateUtil.isStatusHidden(tmpParty.getStatus(), ErrorCode.SHIPMENT_PARTY_HIDE);
		
		ValidateUtil.isDefaulter(tmpParty, ErrorCode.SHIPMENT_PARTY_DEFAULTER);
		
		shipment.setPartyCode(tmpParty.getPartyCode());
	*/
    }

    private void verifyCreatedBy(Shipment shipment) {
        ValidateUtil.notNull(shipment.getCreatedBy(), ErrorCode.SHIPMENT_CREATED_BY);
        ValidateUtil.notNull(shipment.getCreatedBy().getId(), ErrorCode.SHIPMENT_CREATED_BY);
        EmployeeMaster employee = employeeMasterRepository.findById(shipment.getCreatedBy().getId());
        ValidateUtil.notNullOrEmpty(employee.getEmployeeCode(), ErrorCode.SHIPMENT_CREATED_BY);

        ValidateUtil.isEmployeeResigned(employee.getEmployementStatus(), ErrorCode.SHIPMENT_CREATED_BY_RESIGED);
        ValidateUtil.isEmployeeTerminated(employee.getEmployementStatus(), ErrorCode.SHIPMENT_CREATED_BY_TERMINATED);

        shipment.setCreatedByCode(employee.getEmployeeCode());
    }

    private void verifyShipmentReqDate(Shipment shipment) {
        ValidateUtil.notNull(shipment.getShipmentReqDate(), ErrorCode.SHIPMENT_SHIPMENT_REQ_DATE);
        if (shipment.getId() == null) {
            shipment.setShipmentReqDate(TimeUtil.getCurrentLocationTime());
        }
    }
}
