package com.efreightsuite.validation;

import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.model.QuotationAttachment;
import com.efreightsuite.repository.DefaultMasterDataRepository;
import com.efreightsuite.service.AuthService;
import com.efreightsuite.util.AppUtil;
import com.efreightsuite.util.ValidateUtil;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class QuotationAttachmentValidator {

    @Autowired
    private
    AppUtil appUtil;

    @Autowired
    DefaultMasterDataRepository defaultMasterDataRepository;

    public void validate(QuotationAttachment quotationAttachment) {

        ValidateUtil.notNullOrEmpty(quotationAttachment.getRefNo(), ErrorCode.QUOTATION_REF_NO_NOT_NULL);


        ValidateUtil.notNullOrEmpty(quotationAttachment.getFileName(), ErrorCode.QUOTATION_FILE_NAME_NOT_NULL);


        if (quotationAttachment.getId() == null) {
            ValidateUtil.notNull(quotationAttachment.getFile(), ErrorCode.QUOTATION_FILE_NOT_NULL);

            String defaultMasterData = appUtil.getLocationConfig("upload.file.size.in.mb", AuthService.getCurrentUser().getSelectedUserLocation(), false);


            if (defaultMasterData != null && defaultMasterData.trim().length() != 0) {
                long uploadedFileSize = quotationAttachment.getFile().length;
                log.info("Uploaded File Size : " + uploadedFileSize);
                long allowedSize = (long) Integer.parseInt(defaultMasterData) * 1024 * 1024;
                log.info("Allowed Size : " + allowedSize);

                if (uploadedFileSize == 0) {
                    throw new RestException(ErrorCode.QUOTATION_FILE_EMPTY);
                }

                if (uploadedFileSize > allowedSize) {
                    throw new RestException(ErrorCode.QUOTATION_FILE_SIZE_EXCEED);
                }

            }
        }


        log.info("Quotation Attachment Validated....[" + quotationAttachment.getId() + "]");
    }


}
