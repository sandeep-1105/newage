package com.efreightsuite.validation;

import com.efreightsuite.enumeration.GeneralNoteSubCategory;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.model.GeneralNoteMaster;
import com.efreightsuite.service.RegularExpressionService;
import com.efreightsuite.util.ValidateUtil;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Log4j2
@Service
public class GeneralNoteMasterValidator {

    @Autowired
    RegularExpressionService regExpService;

    public void validate(GeneralNoteMaster generalNoteMaster) {
        log.info("in General not validate method");

        ValidateUtil.notNull(generalNoteMaster.getCategory(), ErrorCode.GENERALNOTE_CATEGORY_NOT_NULL);
        ValidateUtil.notNull(generalNoteMaster.getCategory(), ErrorCode.GENERALNOTE_SUB_CATEGORY_NOT_NULL);

        if (generalNoteMaster.getSubCategory() != null
                && generalNoteMaster.getSubCategory() == GeneralNoteSubCategory.Service) {
            ValidateUtil.notNull(generalNoteMaster.getServiceMaster(), ErrorCode.GENERALNOTE_SERVICE_NOT_NULL);
        }
        ValidateUtil.notNull(generalNoteMaster.getCategory(), ErrorCode.GENERALNOTE_LOCATION_NOT_NULL);

        if (generalNoteMaster.getServiceMaster() != null) {
            generalNoteMaster.setServiceCode(generalNoteMaster.getServiceMaster().getServiceCode());
        }

        if (generalNoteMaster.getLocationMaster() != null) {
            generalNoteMaster.setLocationCode(generalNoteMaster.getLocationMaster().getLocationCode());
        }

        ValidateUtil.validateLength(generalNoteMaster.getNote(), 0, 1000, ErrorCode.GENERALNOTE_INVALID_NOTE_LENGTH);

    }

}
