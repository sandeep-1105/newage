package com.efreightsuite.validation;

import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.model.EnquiryAttachment;
import com.efreightsuite.model.LocationMaster;
import com.efreightsuite.repository.DefaultMasterDataRepository;
import com.efreightsuite.util.AppUtil;
import com.efreightsuite.util.ValidateUtil;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class EnquiryAttachmentValidator {

    @Autowired
    private
    AppUtil appUtil;

    @Autowired
    DefaultMasterDataRepository defaultMasterDataRepository;

    public void validate(EnquiryAttachment enquiryAttachment, LocationMaster location) {

        log.info("Enquiry Attachment Validator begins......");

        ValidateUtil.notNullOrEmpty(enquiryAttachment.getRefNo(), ErrorCode.ENQUIRY_ATTACHMENT_REFNO_NOT_NULL);
        ValidateUtil.validateLength(enquiryAttachment.getRefNo(), 0, 100, ErrorCode.ATTACHMENT_REFERENCE_NUMBER_INVALID);
        ValidateUtil.notNullOrEmpty(enquiryAttachment.getFileName(), ErrorCode.ENQUIRY_ATTACHMENT_FILE_NAME_NOT_NULL);


        if (enquiryAttachment.getId() == null) {

            ValidateUtil.notNull(enquiryAttachment.getFile(), ErrorCode.ENQUIRY_ATTACHMENT_FILE_NOT_NULL);

            String key = appUtil.getLocationConfig("upload.file.size.in.mb", location, false);

            if (key != null && key.trim().length() != 0) {

                long uploadedFileSize = enquiryAttachment.getFile().length;

                long allowedSize = Long.parseLong(key) * 1024 * 1024;

                if (uploadedFileSize == 0) {

                    throw new RestException(ErrorCode.ENQUIRY_ATTACHMENT_FILE_EMPTY);

                }

                if (uploadedFileSize > allowedSize) {

                    throw new RestException(ErrorCode.ENQUIRY_ATTACHMENT_FILE_SIZE_EXCEED);

                }

            }
        }

        log.info("Enquiry Attachment Validator begins......");
    }


}
