package com.efreightsuite.validation;

import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.model.*;
import com.efreightsuite.repository.*;
import com.efreightsuite.service.RegularExpressionService;
import com.efreightsuite.util.AppUtil;
import com.efreightsuite.util.RegExpName;
import com.efreightsuite.util.ValidateUtil;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class ShipmentChargeValidator {

    @Autowired
    private
    AppUtil appUtil;


    @Autowired
    private
    RegularExpressionService regExpService;

    @Autowired
    DefaultMasterDataRepository defaultMasterDataRepository;

    @Autowired
    ChargeMasterRepository chargeMasterRepository;

    @Autowired
    UnitMasterRepository unitMasterRepository;

    @Autowired
    CurrencyMasterRepository currencyMasterRepository;

    @Autowired
    PartyMasterRepository partyMasterRepository;


    public void validate(ShipmentCharge shipmentCharge, ShipmentServiceDetail service) {

        log.info("ShipmentCharge Validation Started......[" + shipmentCharge.getId() + "]");

        String key = appUtil.getLocationConfig("booking.rates.merged", service.getLocation(), true);

        shipmentCharge.setCompany(service.getCompany());
        shipmentCharge.setCompanyCode(service.getCompanyCode());

        shipmentCharge.setLocation(service.getLocation());
        shipmentCharge.setLocationCode(service.getLocationCode());

        shipmentCharge.setCountry(service.getCountry());
        shipmentCharge.setCountryCode(service.getCountryCode());


        verifyChargeCode(shipmentCharge);

        verifyChargeName(shipmentCharge);

        verifyUnitMaster(shipmentCharge);

		
		/* Verify Gross Sale */
        if (key.equals("FALSE") || key == "FALSE") {
            verifyGrossSaleCurrency(shipmentCharge);

            verifyGrossSaleAmount(shipmentCharge);

            verifyGrossSaleMinimum(shipmentCharge);
        }
		
		
		/* Verify Net Sale */
        if (key.equals("FALSE") || key == "FALSE") {
            verifyNetSaleCurrency(shipmentCharge);

            verifyNetSaleAmount(shipmentCharge);

            verifyNetSaleMinimum(shipmentCharge);
        }
		
		/* Verify Declared Sale */

        if (key.equals("FALSE") || key == "FALSE") {
            verifyDeclaredSaleCurrency(shipmentCharge);

            verifyDeclaredSaleAmount(shipmentCharge);

            verifyDeclaredSaleMinimum(shipmentCharge);
        }
		
		/* Verify Rate */

        if (key.equals("TRUE") || key == "TRUE") {
            verifyRateCurrency(shipmentCharge);

            verifyRateAmount(shipmentCharge);

            verifyRateMinimum(shipmentCharge);
        }


	/* Verify Cost */

        verifyCostCurrency(shipmentCharge);
        verifyCostAmount(shipmentCharge);
        verifyCostMinimum(shipmentCharge);
		
		
		
		
		/*Verify Vendor*/

        if (key.equals("TRUE") || key == "TRUE") {
            verifyVendor(shipmentCharge);
        }


        log.info("ShipmentCharge Validation Finished......[" + shipmentCharge.getId() + "]");

    }

    private void verifyChargeCode(ShipmentCharge shipmentCharge) {

        ValidateUtil.notNull(shipmentCharge.getChargeMaster(), ErrorCode.SHIPMENT_CHARGE_REQUIRED);

        ValidateUtil.notNull(shipmentCharge.getChargeMaster().getId(), ErrorCode.SHIPMENT_CHARGE_REQUIRED);

        ChargeMaster charge = chargeMasterRepository.findById(shipmentCharge.getChargeMaster().getId());

        ValidateUtil.isStatusBlocked(charge.getStatus(), ErrorCode.SHIPMENT_CHARGE_BLOCKED);

        ValidateUtil.isStatusHidden(charge.getStatus(), ErrorCode.SHIPMENT_CHARGE_HIDDEN);

        shipmentCharge.setChargeCode(charge.getChargeCode());

        log.info("SHIPMENT Charge is validated..................[" + charge.getChargeCode() + "]");

    }

    private void verifyChargeName(ShipmentCharge shipmentCharge) {

        ValidateUtil.notNull(shipmentCharge.getChargeName(), ErrorCode.SHIPMENT_CHARGE_NAME_REQUIRED);

        regExpService.validate(shipmentCharge.getChargeName(), RegExpName.Reg_Exp_Master_Charge_Name, ErrorCode.SHIPMENT_CHARGE_NAME_INVALID);

        log.info("SHIPMENT Charge Name is validated.....[" + shipmentCharge.getChargeName() + "]");
    }

    private void verifyUnitMaster(ShipmentCharge shipmentCharge) {

        ValidateUtil.notNull(shipmentCharge.getUnitMaster(), ErrorCode.SHIPMENT_CHARGE_UNIT_REQUIRED);

        ValidateUtil.notNull(shipmentCharge.getUnitMaster().getId(), ErrorCode.SHIPMENT_CHARGE_UNIT_REQUIRED);

        UnitMaster unit = unitMasterRepository.getOne(shipmentCharge.getUnitMaster().getId());

        ValidateUtil.isStatusBlocked(unit.getStatus(), ErrorCode.SHIPMENT_CHARGE_UNIT_BLOCKED);

        ValidateUtil.isStatusHidden(unit.getStatus(), ErrorCode.SHIPMENT_CHARGE_UNIT_HIDDEN);

        shipmentCharge.setUnitCode(unit.getUnitCode());

        log.info("SHIPMENT Unit is validated..................[" + unit.getUnitCode() + "]");

    }


    private void verifyGrossSaleCurrency(ShipmentCharge shipmentCharge) {
        if (shipmentCharge.getGrossSaleCurrency() != null && shipmentCharge.getGrossSaleCurrency().getId() != null) {

            CurrencyMaster grossSaleCurrency = currencyMasterRepository.getOne(shipmentCharge.getGrossSaleCurrency().getId());

            ValidateUtil.isStatusBlocked(grossSaleCurrency.getStatus(), ErrorCode.SHIPMENT_CHARGE_GROSS_CURRENCY_BLOCKED);

            ValidateUtil.isStatusHidden(grossSaleCurrency.getStatus(), ErrorCode.SHIPMENT_CHARGE_GROSS_CURRENCY_HIDDEN);

            shipmentCharge.setGrossSaleCurrencyCode(grossSaleCurrency.getCurrencyCode());
        } else {
            shipmentCharge.setGrossSaleCurrency(null);
            shipmentCharge.setGrossSaleCurrencyCode(null);
        }
    }

    private void verifyGrossSaleAmount(ShipmentCharge shipmentCharge) {
        if (shipmentCharge.getGrossSaleAmount() != null) {
			
			/*ValidateUtil.nonZero(shipmentCharge.getGrossSaleAmount(), ErrorCode.SHIPMENT_CHARGE_GROSS_AMOUNT_ZERO);
			*/

            if (shipmentCharge.getGrossSaleAmount() < 0 || shipmentCharge.getGrossSaleAmount() > 99999999999.999) {
                throw new RestException(ErrorCode.SHIPMENT_CHARGE_GROSS_AMOUNT_INVALID);
            }
        }
    }

    private void verifyGrossSaleMinimum(ShipmentCharge shipmentCharge) {
        if (shipmentCharge.getGrossSaleMinimum() != null) {
			
			/*ValidateUtil.nonZero(shipmentCharge.getGrossSaleMinimum(), ErrorCode.SHIPMENT_CHARGE_GROSS_MINIMUM_ZERO);
			*/

            if (shipmentCharge.getGrossSaleMinimum() < 0 || shipmentCharge.getGrossSaleMinimum() > 99999999999.999) {
                throw new RestException(ErrorCode.SHIPMENT_CHARGE_GROSS_MINIMUM_INVALID);
            }
        }
    }


    private void verifyNetSaleCurrency(ShipmentCharge shipmentCharge) {
        if (shipmentCharge.getNetSaleCurrency() != null && shipmentCharge.getNetSaleCurrency().getId() != null) {

            CurrencyMaster netSaleCurrency = currencyMasterRepository.getOne(shipmentCharge.getNetSaleCurrency().getId());

            ValidateUtil.isStatusBlocked(netSaleCurrency.getStatus(), ErrorCode.SHIPMENT_CHARGE_NET_CURRENCY_BLOCKED);

            ValidateUtil.isStatusHidden(netSaleCurrency.getStatus(), ErrorCode.SHIPMENT_CHARGE_NET_CURRENCY_HIDDEN);

            shipmentCharge.setNetSaleCurrencyCode(netSaleCurrency.getCurrencyCode());
        } else {
            shipmentCharge.setNetSaleCurrency(null);
            shipmentCharge.setNetSaleCurrencyCode(null);
        }
    }

    private void verifyNetSaleAmount(ShipmentCharge shipmentCharge) {
        if (shipmentCharge.getNetSaleAmount() != null) {
			
			/*ValidateUtil.nonZero(shipmentCharge.getNetSaleAmount(), ErrorCode.SHIPMENT_CHARGE_NET_AMOUNT_ZERO);
			*/

            if (shipmentCharge.getNetSaleAmount() < 0 || shipmentCharge.getNetSaleAmount() > 99999999999.999) {
                throw new RestException(ErrorCode.SHIPMENT_CHARGE_NET_AMOUNT_INVALID);
            }
        }
    }

    private void verifyNetSaleMinimum(ShipmentCharge shipmentCharge) {
        if (shipmentCharge.getNetSaleMinimum() != null) {
			
			/*ValidateUtil.nonZero(shipmentCharge.getNetSaleMinimum(), ErrorCode.SHIPMENT_CHARGE_NET_MINIMUM_ZERO);
			*/

            if (shipmentCharge.getNetSaleMinimum() < 0 || shipmentCharge.getNetSaleMinimum() > 99999999999.999) {
                throw new RestException(ErrorCode.SHIPMENT_CHARGE_NET_MINIMUM_INVALID);
            }
        }
    }


    private void verifyDeclaredSaleCurrency(ShipmentCharge shipmentCharge) {
        if (shipmentCharge.getDeclaredSaleCurrency() != null && shipmentCharge.getDeclaredSaleCurrency().getId() != null) {

            CurrencyMaster declaredSaleCurrency = currencyMasterRepository.getOne(shipmentCharge.getDeclaredSaleCurrency().getId());

            ValidateUtil.isStatusBlocked(declaredSaleCurrency.getStatus(), ErrorCode.SHIPMENT_CHARGE_DECLARED_CURRENCY_BLOCKED);

            ValidateUtil.isStatusHidden(declaredSaleCurrency.getStatus(), ErrorCode.SHIPMENT_CHARGE_DECLARED_CURRENCY_HIDDEN);

            shipmentCharge.setDeclaredSaleCurrencyCode(declaredSaleCurrency.getCurrencyCode());
        } else {
            shipmentCharge.setDeclaredSaleCurrency(null);
            shipmentCharge.setDeclaredSaleCurrencyCode(null);
        }
    }

    private void verifyDeclaredSaleAmount(ShipmentCharge shipmentCharge) {
        if (shipmentCharge.getDeclaredSaleAmount() != null) {
			
			/*ValidateUtil.nonZero(shipmentCharge.getDeclaredSaleAmount(), ErrorCode.SHIPMENT_CHARGE_DECLARED_AMOUNT_ZERO);
			*/

            if (shipmentCharge.getDeclaredSaleAmount() < 0 || shipmentCharge.getDeclaredSaleAmount() > 99999999999.999) {
                throw new RestException(ErrorCode.SHIPMENT_CHARGE_DECLARED_AMOUNT_INVALID);
            }
        }
    }

    private void verifyDeclaredSaleMinimum(ShipmentCharge shipmentCharge) {
        if (shipmentCharge.getDeclaredSaleMinimum() != null) {
			
			/*ValidateUtil.nonZero(shipmentCharge.getDeclaredSaleMinimum(), ErrorCode.SHIPMENT_CHARGE_DECLARED_MINIMUM_ZERO);
			*/

            if (shipmentCharge.getDeclaredSaleMinimum() < 0 || shipmentCharge.getDeclaredSaleMinimum() > 99999999999.999) {
                throw new RestException(ErrorCode.SHIPMENT_CHARGE_DECLARED_MINIMUM_INVALID);
            }
        }
    }


    private void verifyRateCurrency(ShipmentCharge shipmentCharge) {
        if (shipmentCharge.getRateCurrency() != null && shipmentCharge.getRateCurrency().getId() != null) {

            CurrencyMaster rateCurrency = currencyMasterRepository.getOne(shipmentCharge.getRateCurrency().getId());

            ValidateUtil.isStatusBlocked(rateCurrency.getStatus(), ErrorCode.SHIPMENT_CHARGE_RATE_CURRENCY_BLOCKED);

            ValidateUtil.isStatusHidden(rateCurrency.getStatus(), ErrorCode.SHIPMENT_CHARGE_RATE_CURRENCY_HIDDEN);

            shipmentCharge.setRateCurrencyCode(rateCurrency.getCurrencyCode());
        } else {
            shipmentCharge.setRateCurrency(null);
            shipmentCharge.setRateCurrencyCode(null);
        }
    }

    private void verifyRateAmount(ShipmentCharge shipmentCharge) {
        if (shipmentCharge.getRateAmount() != null) {
			
			/*ValidateUtil.nonZero(shipmentCharge.getRateAmount(), ErrorCode.SHIPMENT_CHARGE_RATE_AMOUNT_ZERO);
			*/

            if (shipmentCharge.getRateAmount() < 0 || shipmentCharge.getRateAmount() > 99999999999.999) {
                throw new RestException(ErrorCode.SHIPMENT_CHARGE_RATE_AMOUNT_INVALID);
            }
        }
    }

    private void verifyRateMinimum(ShipmentCharge shipmentCharge) {
        if (shipmentCharge.getRateMinimum() != null) {
			
			/*ValidateUtil.nonZero(shipmentCharge.getRateMinimum(), ErrorCode.SHIPMENT_CHARGE_RATE_MINIMUM_ZERO);
			*/

            if (shipmentCharge.getRateMinimum() < 0 || shipmentCharge.getRateMinimum() > 99999999999.999) {
                throw new RestException(ErrorCode.SHIPMENT_CHARGE_RATE_MINIMUM_INVALID);
            }
        }
    }


    private void verifyCostCurrency(ShipmentCharge shipmentCharge) {
        if (shipmentCharge.getCostCurrency() != null && shipmentCharge.getCostCurrency().getId() != null) {

            CurrencyMaster costCurrency = currencyMasterRepository.getOne(shipmentCharge.getCostCurrency().getId());

            ValidateUtil.isStatusBlocked(costCurrency.getStatus(), ErrorCode.SHIPMENT_CHARGE_COST_CURRENCY_BLOCKED);

            ValidateUtil.isStatusHidden(costCurrency.getStatus(), ErrorCode.SHIPMENT_CHARGE_COST_CURRENCY_HIDDEN);

            shipmentCharge.setCostCurrencyCode(costCurrency.getCurrencyCode());
        } else {
            shipmentCharge.setCostCurrency(null);
            shipmentCharge.setCostCurrencyCode(null);
        }
    }

    private void verifyCostAmount(ShipmentCharge shipmentCharge) {
        if (shipmentCharge.getCostAmount() != null) {
			
			/*ValidateUtil.nonZero(shipmentCharge.getCostAmount(), ErrorCode.SHIPMENT_CHARGE_COST_AMOUNT_ZERO);
			*/

            if (shipmentCharge.getCostAmount() < 0 || shipmentCharge.getCostAmount() > 99999999999.999) {
                throw new RestException(ErrorCode.SHIPMENT_CHARGE_COST_AMOUNT_INVALID);
            }
        }
    }

    private void verifyCostMinimum(ShipmentCharge shipmentCharge) {
        if (shipmentCharge.getCostMinimum() != null) {
			
			/*ValidateUtil.nonZero(shipmentCharge.getCostMinimum(), ErrorCode.SHIPMENT_CHARGE_COST_MINIMUM_ZERO);
			*/

            if (shipmentCharge.getCostMinimum() < 0 || shipmentCharge.getCostMinimum() > 99999999999.999) {
                throw new RestException(ErrorCode.SHIPMENT_CHARGE_COST_MINIMUM_INVALID);
            }
        }

    }


    private void verifyVendor(ShipmentCharge shipmentCharge) {


        if (shipmentCharge.getVendor() != null && shipmentCharge.getVendor().getId() != null) {

            PartyMaster vendor = partyMasterRepository.findById(shipmentCharge.getVendor().getId());

            ValidateUtil.isStatusBlocked(vendor.getStatus(), ErrorCode.SHIPMENT_CHARGE_VENDOR_BLOCKED);

            ValidateUtil.isStatusHidden(vendor.getStatus(), ErrorCode.SHIPMENT_CHARGE_VENDOR_HIDDEN);

            shipmentCharge.setVendorCode(vendor.getPartyCode());

        } else {
            shipmentCharge.setVendor(null);
            shipmentCharge.setVendorCode(null);
        }
    }
}
