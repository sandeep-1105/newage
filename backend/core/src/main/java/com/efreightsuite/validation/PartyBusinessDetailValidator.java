package com.efreightsuite.validation;

import com.efreightsuite.enumeration.LovStatus;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.model.PartyBusinessDetail;
import com.efreightsuite.service.RegularExpressionService;
import com.efreightsuite.util.RegExpName;
import com.efreightsuite.util.ValidateUtil;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class PartyBusinessDetailValidator {

    @Autowired
    private
    RegularExpressionService regExpService;

    public void validate(PartyBusinessDetail partyBusinessDetail) {

        ValidateUtil.notNull(partyBusinessDetail, ErrorCode.PARTY_BUSINESS_INVALID);

        ValidateUtil.notNull(partyBusinessDetail.getServiceMaster(), ErrorCode.PARTY_BUSINESS_SERVICE_REQUIRED);

        if (partyBusinessDetail.getServiceMaster() != null && partyBusinessDetail.getServiceMaster().getId() != null
                && LovStatus.Block.equals(partyBusinessDetail.getServiceMaster().getStatus())) {
            throw new RestException(ErrorCode.PARTY_BUSINESS_SERVICE_BLOCKED);
        }
        if (partyBusinessDetail.getServiceMaster() != null && partyBusinessDetail.getServiceMaster().getId() != null
                && LovStatus.Hide.equals(partyBusinessDetail.getServiceMaster().getStatus())) {
            throw new RestException(ErrorCode.PARTY_BUSINESS_SERVICE_HIDE);
        }

        partyBusinessDetail.setServiceCode(partyBusinessDetail.getServiceMaster().getServiceCode());


        if (partyBusinessDetail.getTosMaster() != null && partyBusinessDetail.getTosMaster().getId() != null) {

            if (partyBusinessDetail.getTosMaster().getStatus().equals(LovStatus.Block)) {
                throw new RestException(ErrorCode.PARTY_BUSINESS_TOS_BLOCKED);
            }

            if (partyBusinessDetail.getTosMaster().getStatus().equals(LovStatus.Hide)) {
                throw new RestException(ErrorCode.PARTY_BUSINESS_TOS_HIDE);
            }

            partyBusinessDetail.setTosCode(partyBusinessDetail.getTosMaster().getTosCode());

        } else {
            partyBusinessDetail.setTosMaster(null);
            partyBusinessDetail.setTosCode(null);
        }

        if (partyBusinessDetail.getOrigin() != null && partyBusinessDetail.getOrigin().getId() != null) {
            if (partyBusinessDetail.getOrigin().getStatus().equals(LovStatus.Block)) {
                throw new RestException(ErrorCode.PARTY_BUSINESS_ORIGIN_PORT_BLOCKED);
            }

            if (partyBusinessDetail.getOrigin().getStatus().equals(LovStatus.Hide)) {
                throw new RestException(ErrorCode.PARTY_BUSINESS_ORIGIN_PORT_HIDE);
            }
            partyBusinessDetail.setOriginCode(partyBusinessDetail.getOrigin().getPortCode());
        } else {
            partyBusinessDetail.setOrigin(null);
            partyBusinessDetail.setOriginCode(null);
        }
        if (partyBusinessDetail.getDestination() != null && partyBusinessDetail.getDestination().getId() != null) {
            if (partyBusinessDetail.getDestination().getStatus().equals(LovStatus.Block)) {
                throw new RestException(ErrorCode.PARTY_BUSINESS_DESTINATION_BLOCKED);
            }

            if (partyBusinessDetail.getDestination().getStatus().equals(LovStatus.Hide)) {
                throw new RestException(ErrorCode.PARTY_BUSINESS_DESTINATION_HIDE);
            }
            partyBusinessDetail.setDestinationCode(partyBusinessDetail.getDestination().getPortCode());
        } else {
            partyBusinessDetail.setDestination(null);
            partyBusinessDetail.setDestinationCode(null);
        }

        if (partyBusinessDetail.getCommodity() != null && partyBusinessDetail.getCommodity().getId() != null) {

            if (partyBusinessDetail.getCommodity().getStatus().equals(LovStatus.Block)) {
                throw new RestException(ErrorCode.PARTY_BUSINESS_COMMODITY_BLOCKED);
            }
            if (partyBusinessDetail.getCommodity().getStatus().equals(LovStatus.Hide)) {
                throw new RestException(ErrorCode.PARTY_BUSINESS_COMMODITY_HIDE);
            }
            partyBusinessDetail.setCommodityCode(partyBusinessDetail.getCommodity().getHsCode());
        } else {
            partyBusinessDetail.setCommodity(null);
            partyBusinessDetail.setCommodityCode(null);
        }

        if (partyBusinessDetail.getEstimatedRevenue() != null) {
            regExpService.validate(partyBusinessDetail.getEstimatedRevenue(), RegExpName.Reg_Exp_Master_PARTY_ESTIMATED_REVENUE, ErrorCode.ESTIMATED_REVENUE_INVALID);
        }

        if (partyBusinessDetail.getNoOfShipments() != null) {
            regExpService.validate(partyBusinessDetail.getNoOfShipments(), RegExpName.Reg_Exp_Master_PARTY_NO_OF_SHIPMENTS, ErrorCode.BUSINESS_NOOFSHIPMENTS_INVALID);
        }

        if (partyBusinessDetail.getNoOfUnits() != null) {

            regExpService.validate(partyBusinessDetail.getNoOfUnits(), RegExpName.Reg_Exp_Master_PARTY_NO_OF_UNITS, ErrorCode.BUSINESS_NOOFUNITS_INVALID);

            if (partyBusinessDetail.getNoOfUnits() > 9999999999d) {

                throw new RestException(ErrorCode.BUSINESS_NOOFUNITS_INVALID);
            }
        }

        if (partyBusinessDetail.getPeriod() != null && partyBusinessDetail.getPeriod().getId() != null) {

            if (partyBusinessDetail.getPeriod().getStatus().equals(LovStatus.Block)) {
                throw new RestException(ErrorCode.PARTY_BUSINESS_PERIOD_BLOCKED);
            }

            if (partyBusinessDetail.getPeriod().getStatus().equals(LovStatus.Hide)) {
                throw new RestException(ErrorCode.PARTY_BUSINESS_PERIOD_HIDE);
            }
            partyBusinessDetail.setPeriodCode(partyBusinessDetail.getPeriod().getFrequencyCode());

        } else {
            partyBusinessDetail.setPeriod(null);
            partyBusinessDetail.setPeriodCode(null);
        }


        if (partyBusinessDetail.getUnitMaster() != null && partyBusinessDetail.getUnitMaster().getId() != null) {

            if (partyBusinessDetail.getUnitMaster().getStatus().equals(LovStatus.Block)) {
                throw new RestException(ErrorCode.PARTY_BUSINESS_UNIT_BLOCKED);
            }

            if (partyBusinessDetail.getUnitMaster().getStatus().equals(LovStatus.Hide)) {
                throw new RestException(ErrorCode.PARTY_BUSINESS_UNIT_HIDDEN);
            }

            partyBusinessDetail.setUnitCode(partyBusinessDetail.getUnitMaster().getUnitCode());

        } else {
            partyBusinessDetail.setUnitMaster(null);
            partyBusinessDetail.setUnitCode(null);
        }


        if (partyBusinessDetail.getCreatedLocation() != null) {
            partyBusinessDetail.setCreatedLocationCode(partyBusinessDetail.getCreatedLocation().getLocationCode());
        } else {
            partyBusinessDetail.setCreatedLocationCode(null);
        }
    }

}
