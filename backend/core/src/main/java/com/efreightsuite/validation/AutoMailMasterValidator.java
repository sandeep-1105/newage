package com.efreightsuite.validation;

import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.model.AutoMailMaster;
import com.efreightsuite.service.RegularExpressionService;
import com.efreightsuite.util.RegExpName;
import com.efreightsuite.util.ValidateUtil;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author Devendrachary M
 */
@Service
@Log4j2
public class AutoMailMasterValidator {

    @Autowired
    private
    RegularExpressionService regExpService;

    public void validateToCreate(AutoMailMaster autoMailMaster) {

        log.info("AutoMailMasterValidator.validateToCreate method is started.");

        validateAutoMailGroup(autoMailMaster);

        validateAutoMailName(autoMailMaster);

        validateAutoMailCode(autoMailMaster);

        log.info("AutoMailMasterValidator.validateToCreate method is ended.");
    }

    public void validateToUpdate(AutoMailMaster autoMailMaster) {
        validateToCreate(autoMailMaster);
    }


    private void validateAutoMailGroup(AutoMailMaster autoMailMaster) {

        ValidateUtil.notNull(autoMailMaster.getAutoMailGroupMaster(), ErrorCode.AUTOMAILGROUP_NOT_NULL);

        ValidateUtil.notNull(autoMailMaster.getAutoMailGroupMaster().getId(), ErrorCode.AUTOMAILGROUP_NOT_NULL);

    }


    private void validateAutoMailName(AutoMailMaster autoMailMaster) {

        ValidateUtil.notNull(autoMailMaster.getMessageName(), ErrorCode.AUTOMAIL_MESSAGENAME_NOT_NULL);

        regExpService.validate(autoMailMaster.getMessageName(), RegExpName.Reg_Exp_Master_AUTO_MAIL_NAME, ErrorCode.AUTOMAIL_MESSAGENAME_INVALID);
    }


    private void validateAutoMailCode(AutoMailMaster autoMailMaster) {

        ValidateUtil.notNull(autoMailMaster.getMessageCode(), ErrorCode.AUTOMAIL_MESSAGECODE_NOT_NULL);

        regExpService.validate(autoMailMaster.getMessageCode(), RegExpName.Reg_Exp_Master_AUTO_MAIL_CODE, ErrorCode.AUTOMAIL_MESSAGECODE_INVALID);
    }

}
