package com.efreightsuite.communication;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

import lombok.extern.log4j.Log4j2;
import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;

import org.springframework.stereotype.Service;


@Service
@Log4j2
public class FtpTransfer {

    public boolean upload(FtpDto ftpDto) {

        FTPClient ftpClient = new FTPClient();

        try {
            ftpClient.connect(ftpDto.getServer(), ftpDto.getPort());
            ftpClient.login(ftpDto.getUserName(), ftpDto.getPassword());
            ftpClient.enterLocalPassiveMode();

            ftpClient.setFileType(FTP.BINARY_FILE_TYPE);

            File firstLocalFile = new File(ftpDto.getLocalFileLocation() + "/" + ftpDto.getFileName());
            boolean done;
            try (InputStream inputStream = new FileInputStream(firstLocalFile)) {

                log.debug("Start uploading file : " + ftpDto);
                done = ftpClient.storeFile(ftpDto.getRemoteFileLocation() + "/" + ftpDto.getFileName(),
                        inputStream);
                inputStream.close();
            }

            if (done) {
                log.debug("File is uploaded successfully.");
            }
            return done;
        } catch (Exception ex) {
            log.error("FTP file transfer upload error ", ex);
        } finally {
            try {
                if (ftpClient.isConnected()) {
                    ftpClient.logout();
                    ftpClient.disconnect();
                }
            } catch (Exception ex) {
                log.error("FTP file transfer connection closing error ", ex);
            }
        }
        return false;
    }

    public boolean download(FtpDto ftpDto) {

        FTPClient ftpClient = new FTPClient();
        try {

            ftpClient.connect(ftpDto.getServer(), ftpDto.getPort());
            ftpClient.login(ftpDto.getUserName(), ftpDto.getPassword());
            ftpClient.enterLocalPassiveMode();

            ftpClient.setFileType(FTP.BINARY_FILE_TYPE);

            File downloadFile1 = new File(ftpDto.getLocalFileLocation() + "/" + ftpDto.getFileName());
            boolean success;
            try (OutputStream outputStream = new BufferedOutputStream(new FileOutputStream(downloadFile1))) {
                success = ftpClient.retrieveFile(ftpDto.getRemoteFileLocation() + "/" + ftpDto.getFileName(),
                        outputStream);
                outputStream.close();
            }

            if (success) {
                log.debug("File has been downloaded successfully.");
            }

            return success;

        } catch (Exception ex) {
            log.error("FTP file transfer download error ", ex);
        } finally {
            try {
                if (ftpClient.isConnected()) {
                    ftpClient.logout();
                    ftpClient.disconnect();
                }
            } catch (Exception ex) {
                log.error("FTP file transfer connection closing error ", ex);
            }
        }
        return false;
    }
}
