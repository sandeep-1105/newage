package com.efreightsuite.communication;

import lombok.Data;

@Data
public class IbmMqDto {

    String hostName;
    int port;
    String queueManager;
    String channel;
    String queueName;
    String topic;
    String message;
    String response;

    boolean done;
}
