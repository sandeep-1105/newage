package com.efreightsuite.communication;

import java.util.Date;
import java.util.Objects;

import com.efreightsuite.dto.MessagingDto;
import com.efreightsuite.enumeration.EdiMessagingFor;
import com.efreightsuite.enumeration.EdiStatus;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.model.AesFile;
import com.efreightsuite.model.AesFileEdiStatus;
import com.efreightsuite.model.AirlineEdi;
import com.efreightsuite.model.AirlineEdiStatus;
import com.efreightsuite.repository.AesFileEdiStatusRepository;
import com.efreightsuite.repository.AesFileRepository;
import com.efreightsuite.repository.AirlineEdiRepository;
import com.efreightsuite.repository.AirlineEdiStatusRepository;
import com.efreightsuite.util.AppUtil;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.log4j.Log4j2;
import org.apache.log4j.MDC;

import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessagePostProcessor;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Log4j2
@Service
public class RabbitMQMessageSender {

    @Value("${queue.name}")
    private
    String queueName;

    @Autowired
    private
    RabbitTemplate rabbitTemplate;

    @Autowired
    private
    AppUtil appUtil;

    @Autowired
    private
    AirlineEdiRepository airlineEdiRepository;

    @Autowired
    private
    AirlineEdiStatusRepository airlineEdiStatusRepository;

    @Autowired
    private
    AesFileRepository aesFileRepository;

    @Autowired
    private
    AesFileEdiStatusRepository aesFileEdiStatusRepository;

    public void sendtoMQ(MessagingDto request, String saasId) {
        if (Objects.equals(request.getMessagingFor(),EdiMessagingFor.WIN)) {
            callWinRabbit(request, saasId);
            return;
        }
        AirlineEdiStatus airlineEdiStatus = new AirlineEdiStatus();
        AirlineEdi airlineEdi = null;
        if (request.isAirlineEDI()) {
            airlineEdi = new AirlineEdi(request.getEdiId());
            airlineEdiStatus.setCreateDate(new Date());
            airlineEdiStatus.setAirlineEdi(airlineEdi);
        }

        AesFile aesFile = null;
        AesFileEdiStatus aesFileEdiStatus = new AesFileEdiStatus();

        if (request.getMessagingFor().equals(EdiMessagingFor.AES)) {
            log.info("Hey I am here :: " + EdiMessagingFor.AES);
            aesFile = aesFileRepository.findById(request.getEdiId());
            aesFileEdiStatus = new AesFileEdiStatus();
            aesFileEdiStatus.setAesFile(aesFile);
            aesFileEdiStatus.setCreateDate(new Date());
        }

        try {
            log.info("About to send a message to queue [" + queueName + "]");
            log.info("Host [" + rabbitTemplate.getConnectionFactory().getHost() + "]");
            log.info("Port [" + rabbitTemplate.getConnectionFactory().getPort() + "]");
            log.info("Virtual Host[" + rabbitTemplate.getConnectionFactory().getVirtualHost() + "]");

            log.info("Data Sent to Queue : ");
            logObjectToJSON(request);

            rabbitTemplate.convertAndSend(queueName, request, message -> {

                message.getMessageProperties().setHeader("TrackId", MDC.get("TrackId"));

                message.getMessageProperties().setHeader("SaasId", saasId);

                message.getMessageProperties().setContentType("application/json");

                return message;
            });


            if (request.isAirlineEDI()) {
                airlineEdiStatus.setStatus(EdiStatus.QUEUEING_SUCCESS);
                airlineEdiStatus.setStatusMessage(appUtil.getDesc(ErrorCode.EDI_STATUS_SENT_TO_QUEUE));

                if (null != airlineEdi) {
                    airlineEdi.setStatus(EdiStatus.QUEUEING_SUCCESS);
                    airlineEdi.setStatusMessage(appUtil.getDesc(ErrorCode.EDI_STATUS_SENT_TO_QUEUE));
                }
            } else {
                if (null != aesFile)
                    aesFile.setAesStatus("Local MQ Initiated");
                aesFileEdiStatus.setMessagingStatus("Local MQ Initiated");
                aesFileEdiStatus.setMessagingError(null);
            }
        } catch (Exception exception) {
            log.error("Exception in SendRabbitMqMessage :", exception);

            if (request.isAirlineEDI()) {
                airlineEdiStatus.setStatus(EdiStatus.QUEUEING_FAILURE);
                airlineEdiStatus.setStatusMessage(appUtil.getDesc(ErrorCode.EDI_STATUS_SENT_TO_QUEUE));
                if (null != airlineEdi) {
                    airlineEdi.setStatus(EdiStatus.QUEUEING_FAILURE);
                    airlineEdi.setStatusMessage(appUtil.getDesc(ErrorCode.EDI_STATUS_SENT_TO_QUEUE));
                }
            } else {
                if (null != aesFile)
                    aesFile.setAesStatus("Local MQ Initiation Failed");
                aesFileEdiStatus.setMessagingStatus("Local MQ Initiation Failed");
                String errorMsg = exception.getMessage();
                if (errorMsg.length() >= 4000) {
                    errorMsg = errorMsg.substring(0, 4000);
                }
                aesFileEdiStatus.setMessagingError(errorMsg);
            }
        }

        if (request.isAirlineEDI()) {
            airlineEdiStatusRepository.save(airlineEdiStatus);
            airlineEdiRepository.updateEdiStatus(airlineEdi.getStatusMessage(), airlineEdi.getStatus(), airlineEdi.getId());
//			airlineEdiRepository.save(airlineEdi);
        } else {

            log.info("aesFile :: " + aesFileEdiStatus);
            aesFileEdiStatus.setAesFile(aesFile);
            aesFileEdiStatusRepository.save(aesFileEdiStatus);
            if (null != aesFile)
                aesFileRepository.updateEdiStatus(aesFile.getStatus(), aesFile.getId());
        }
    }


    private void logObjectToJSON(Object data) {

        ObjectMapper objectMapper = new ObjectMapper();
        try {
            String json = objectMapper.writeValueAsString(data);
            log.info("Json Request :: " + json);
        } catch (JsonProcessingException e) {
            log.error("Encountered exception", e);
        }
    }


    private void callWinRabbit(MessagingDto request, String saasId) {
        try {
            log.info("About to send a message to queue [" + queueName + "]");
            log.info("Host [" + rabbitTemplate.getConnectionFactory().getHost() + "] Port ["
                    + rabbitTemplate.getConnectionFactory().getPort() + "]" + "Virtual Host["
                    + rabbitTemplate.getConnectionFactory().getVirtualHost() + "]");

            rabbitTemplate.convertAndSend(queueName, request, message -> {

                message.getMessageProperties().setHeader("TrackId", MDC.get("TrackId"));

                message.getMessageProperties().setHeader("SaasId", saasId);

                return message;
            });

        } catch (Exception exception) {
            log.error("Exception in SendRabbitMqMessage :", exception);
        }

    }
}
