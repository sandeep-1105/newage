package com.efreightsuite.communication;

import lombok.Data;

@Data
public class FtpDto {

    String server;
    int port;
    String userName;
    String password;
    String localFileLocation;
    String remoteFileLocation;
    String fileName;

    byte[] localFile;
    byte[] remoteFile;

}
