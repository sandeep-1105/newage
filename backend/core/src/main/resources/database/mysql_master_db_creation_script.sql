-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema efsdev
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema efsdev
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `efsdev` DEFAULT CHARACTER SET utf8 ;
USE `efsdev` ;

-- -----------------------------------------------------
-- Table `efsdev`.`efs_currency_master`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_currency_master` (
  `id` BIGINT(20) NOT NULL,
  `country_code` VARCHAR(10) NULL DEFAULT NULL,
  `currency_code` VARCHAR(10) NOT NULL,
  `currency_name` VARCHAR(100) NOT NULL,
  `decimal_point` VARCHAR(10) NOT NULL,
  `prefix` VARCHAR(30) NULL DEFAULT NULL,
  `status` VARCHAR(10) NOT NULL,
  `suffix` VARCHAR(30) NULL DEFAULT NULL,
  `symbol` VARCHAR(20) NULL DEFAULT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  `country_id` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `UK_CURRENCY_CURRENCYCODE` (`currency_code` ASC),
  UNIQUE INDEX `UK_CURRENCY_CURRENCYNAME` (`currency_name` ASC),
  INDEX `FK_CURRENCY_COUNTRYID` (`country_id` ASC),
  CONSTRAINT `FK_CURRENCY_COUNTRYID`
    FOREIGN KEY (`country_id`)
    REFERENCES `efsdev`.`efs_country_master` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_region_master`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_region_master` (
  `id` BIGINT(20) NOT NULL,
  `region_code` VARCHAR(10) NOT NULL,
  `region_name` VARCHAR(100) NOT NULL,
  `status` VARCHAR(10) NOT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `UK_REGION_REGIONCODE` (`region_code` ASC),
  UNIQUE INDEX `UK_REGION_REGIONNAME` (`region_name` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_country_master`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_country_master` (
  `id` BIGINT(20) NOT NULL,
  `country_code` VARCHAR(10) NOT NULL,
  `country_name` VARCHAR(100) NOT NULL,
  `currency_code` VARCHAR(10) NULL DEFAULT NULL,
  `image` LONGBLOB NULL DEFAULT NULL,
  `locale` VARCHAR(10) NULL DEFAULT NULL,
  `nationality` VARCHAR(100) NULL DEFAULT NULL,
  `region_code` VARCHAR(100) NULL DEFAULT NULL,
  `status` VARCHAR(10) NOT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  `weight_cbm` DOUBLE NULL DEFAULT NULL,
  `currency_id` BIGINT(20) NULL DEFAULT NULL,
  `region_id` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `UK_COUNTRY_COUNTRYCODE` (`country_code` ASC),
  UNIQUE INDEX `UK_COUNTRY_COUNTRYNAME` (`country_name` ASC),
  INDEX `FK_COUNTRY_CURRENCYID` (`currency_id` ASC),
  INDEX `FK_COUNTRY_REGIONID` (`region_id` ASC),
  CONSTRAINT `FK_COUNTRY_CURRENCYID`
    FOREIGN KEY (`currency_id`)
    REFERENCES `efsdev`.`efs_currency_master` (`id`),
  CONSTRAINT `FK_COUNTRY_REGIONID`
    FOREIGN KEY (`region_id`)
    REFERENCES `efsdev`.`efs_region_master` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_state_province_master`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_state_province_master` (
  `id` BIGINT(20) NOT NULL,
  `country_code` VARCHAR(10) NOT NULL,
  `state_code` VARCHAR(10) NOT NULL,
  `state_name` VARCHAR(100) NOT NULL,
  `status` VARCHAR(10) NOT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  `country_id` BIGINT(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `UK_STATEPROVINCE_STATECODE` (`state_code` ASC, `country_id` ASC),
  UNIQUE INDEX `UK_STATEPROVINCE_STATENAME` (`state_name` ASC, `country_id` ASC),
  INDEX `FK_STATEPROVINCE_COUNTRYID` (`country_id` ASC),
  CONSTRAINT `FK_STATEPROVINCE_COUNTRYID`
    FOREIGN KEY (`country_id`)
    REFERENCES `efsdev`.`efs_country_master` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_city_master`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_city_master` (
  `id` BIGINT(20) NOT NULL,
  `city_code` VARCHAR(10) NOT NULL,
  `city_name` VARCHAR(100) NOT NULL,
  `country_code` VARCHAR(10) NULL DEFAULT NULL,
  `state_code` VARCHAR(10) NULL DEFAULT NULL,
  `status` VARCHAR(10) NOT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  `country_id` BIGINT(20) NULL DEFAULT NULL,
  `state_id` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `UK_CITY_CITYCODE` (`city_code` ASC),
  UNIQUE INDEX `UK_CITY_CITYNAME` (`city_name` ASC, `state_id` ASC, `country_id` ASC),
  INDEX `FK_CITY_COUNTRYID` (`country_id` ASC),
  INDEX `FK_CITY_STATEID` (`state_id` ASC),
  CONSTRAINT `FK_CITY_COUNTRYID`
    FOREIGN KEY (`country_id`)
    REFERENCES `efsdev`.`efs_country_master` (`id`),
  CONSTRAINT `FK_CITY_STATEID`
    FOREIGN KEY (`state_id`)
    REFERENCES `efsdev`.`efs_state_province_master` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_address_master`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_address_master` (
  `id` BIGINT(20) NOT NULL,
  `address_line_1` VARCHAR(100) NULL DEFAULT NULL,
  `address_line_2` VARCHAR(100) NULL DEFAULT NULL,
  `address_line_3` VARCHAR(100) NULL DEFAULT NULL,
  `address_line_4` VARCHAR(150) NULL DEFAULT NULL,
  `city_code` VARCHAR(100) NULL DEFAULT NULL,
  `contact` VARCHAR(100) NULL DEFAULT NULL,
  `country_code` VARCHAR(100) NULL DEFAULT NULL,
  `email` VARCHAR(500) NULL DEFAULT NULL,
  `fax` VARCHAR(500) NULL DEFAULT NULL,
  `mobile` VARCHAR(500) NULL DEFAULT NULL,
  `phone` VARCHAR(500) NULL DEFAULT NULL,
  `po_box` VARCHAR(20) NULL DEFAULT NULL,
  `state_code` VARCHAR(100) NULL DEFAULT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  `zip_code` VARCHAR(10) NULL DEFAULT NULL,
  `city` BIGINT(20) NULL DEFAULT NULL,
  `country` BIGINT(20) NULL DEFAULT NULL,
  `state` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `FK_ADDR_CTY` (`city` ASC),
  INDEX `FK_ADDR_CTRY` (`country` ASC),
  INDEX `FK_ADDR_STAT` (`state` ASC),
  CONSTRAINT `FK_ADDR_CTRY`
    FOREIGN KEY (`country`)
    REFERENCES `efsdev`.`efs_country_master` (`id`),
  CONSTRAINT `FK_ADDR_CTY`
    FOREIGN KEY (`city`)
    REFERENCES `efsdev`.`efs_city_master` (`id`),
  CONSTRAINT `FK_ADDR_STAT`
    FOREIGN KEY (`state`)
    REFERENCES `efsdev`.`efs_state_province_master` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_category_master`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_category_master` (
  `id` BIGINT(20) NOT NULL,
  `category_code` VARCHAR(10) NOT NULL,
  `category_name` VARCHAR(100) NOT NULL,
  `status` VARCHAR(10) NOT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `UK_CATEGORY_CATEGORYCODE` (`category_code` ASC),
  UNIQUE INDEX `UK_CATEGORY_CATEGORYNAME` (`category_name` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_hs_code_master`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_hs_code_master` (
  `id` BIGINT(20) NOT NULL,
  `description` VARCHAR(4000) NULL DEFAULT NULL,
  `hs_code` VARCHAR(10) NOT NULL,
  `hs_name` VARCHAR(500) NOT NULL,
  `status` VARCHAR(10) NOT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `UK_COMMODITY_HSCODE` (`hs_code` ASC),
  UNIQUE INDEX `UK_COMMODITY_HSNAME` (`hs_name` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_carrier_master`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_carrier_master` (
  `id` BIGINT(20) NOT NULL,
  `carrier_code` VARCHAR(10) NOT NULL,
  `carrier_name` VARCHAR(100) NOT NULL,
  `carrier_no` VARCHAR(10) NULL DEFAULT NULL,
  `iata_code` VARCHAR(10) NULL DEFAULT NULL,
  `image` LONGBLOB NULL DEFAULT NULL,
  `is_plus_300` VARCHAR(10) NULL DEFAULT NULL,
  `messaging_type` VARCHAR(10) NULL DEFAULT NULL,
  `scac_code` VARCHAR(10) NULL DEFAULT NULL,
  `status` VARCHAR(10) NOT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `transport_mode` VARCHAR(10) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `UK_CARRIER_CARRIERCODE` (`carrier_code` ASC),
  UNIQUE INDEX `UK_CARRIER_CARRIERNAME` (`carrier_name` ASC),
  UNIQUE INDEX `UK_CARRIER_CARRIERNO` (`carrier_no` ASC),
  UNIQUE INDEX `UK_CARRIER_SCACCODE` (`scac_code` ASC),
  UNIQUE INDEX `UK_CARRIER_IATACODE` (`iata_code` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_company_master`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_company_master` (
  `id` BIGINT(20) NOT NULL,
  `company_code` VARCHAR(10) NOT NULL,
  `company_name` VARCHAR(100) NOT NULL,
  `status` VARCHAR(10) NOT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `UK_COMPANY_COMPANYCODE` (`company_code` ASC),
  UNIQUE INDEX `UK_COMPANY_COMPANYNAME` (`company_name` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_grade_master`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_grade_master` (
  `id` BIGINT(20) NOT NULL,
  `color_code` VARCHAR(100) NULL DEFAULT NULL,
  `grade_name` VARCHAR(100) NOT NULL,
  `priority` INT(11) NULL DEFAULT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `UK_GRADE_GRADENAME` (`grade_name` ASC),
  UNIQUE INDEX `UK_GRADE_GRADEPRIORITY` (`priority` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_period_master`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_period_master` (
  `id` BIGINT(20) NOT NULL,
  `frequency_code` VARCHAR(10) NULL DEFAULT NULL,
  `frequency_name` VARCHAR(100) NULL DEFAULT NULL,
  `no_of_day` BIGINT(20) NULL DEFAULT NULL,
  `status` VARCHAR(10) NOT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `UK_PERIOD_FREQUENCYCODE` (`frequency_code` ASC),
  UNIQUE INDEX `UK_PERIOD_FREQUENCYNAME` (`frequency_name` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_party_master_detail`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_party_master_detail` (
  `id` BIGINT(20) NOT NULL,
  `agent_edi_email` VARCHAR(255) NULL DEFAULT NULL,
  `account_number` VARCHAR(255) NULL DEFAULT NULL,
  `auto_outstanding_email` VARCHAR(255) NULL DEFAULT NULL,
  `billing_fsc_sc_on_gross_wt` VARCHAR(255) NULL DEFAULT NULL,
  `caf_percentage` DOUBLE NULL DEFAULT NULL,
  `cc_percentage` DOUBLE NULL DEFAULT NULL,
  `corporate_non_corporate` VARCHAR(255) NOT NULL,
  `cst_no` VARCHAR(255) NULL DEFAULT NULL,
  `customer_ecc_number` VARCHAR(255) NULL DEFAULT NULL,
  `direct_status_update` VARCHAR(255) NULL DEFAULT NULL,
  `email_message_format` VARCHAR(255) NULL DEFAULT NULL,
  `gst_no` VARCHAR(100) NULL DEFAULT NULL,
  `iata_code` VARCHAR(255) NULL DEFAULT NULL,
  `import_sailing_schedule_email` VARCHAR(255) NULL DEFAULT NULL,
  `is_blanket_nomination` VARCHAR(5) NULL DEFAULT NULL,
  `iva_code` VARCHAR(255) NULL DEFAULT NULL,
  `match_invoice_quote_output` VARCHAR(255) NULL DEFAULT NULL,
  `nls_customer_name` VARCHAR(255) NULL DEFAULT NULL,
  `pan_no` VARCHAR(255) NULL DEFAULT NULL,
  `period_code` VARCHAR(100) NULL DEFAULT NULL,
  `personal_idenfication_no` VARCHAR(10) NULL DEFAULT NULL,
  `rac_no` VARCHAR(255) NULL DEFAULT NULL,
  `sailing_schedule_email` VARCHAR(255) NULL DEFAULT NULL,
  `sailing_schedule_fax` VARCHAR(255) NULL DEFAULT NULL,
  `spot_no` VARCHAR(100) NULL DEFAULT NULL,
  `status_update_email_export` VARCHAR(255) NULL DEFAULT NULL,
  `status_update_email_import` VARCHAR(255) NULL DEFAULT NULL,
  `svat_no` VARCHAR(255) NULL DEFAULT NULL,
  `svt_no` VARCHAR(255) NULL DEFAULT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `tds_percentage` DOUBLE NULL DEFAULT NULL,
  `tin_no` VARCHAR(255) NULL DEFAULT NULL,
  `transhipment_email` VARCHAR(255) NULL DEFAULT NULL,
  `tsa_no` VARCHAR(100) NULL DEFAULT NULL,
  `value_added_tax_no` VARCHAR(255) NULL DEFAULT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  `period_id` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `UK_PARTYDETAIL_VALUEADDEDTAX` (`value_added_tax_no` ASC),
  UNIQUE INDEX `UK_PARTYDETAIL_PAN` (`pan_no` ASC),
  UNIQUE INDEX `UK_PARTYDETAIL_RACNO` (`rac_no` ASC),
  UNIQUE INDEX `UK_PARTYDETAIL_CSTNO` (`cst_no` ASC),
  UNIQUE INDEX `UK_PARTYDETAIL_SVTNO` (`svt_no` ASC),
  UNIQUE INDEX `UK_PARTYDETAIL_SVATNO` (`svat_no` ASC),
  UNIQUE INDEX `UK_PARTYDETAIL_TINNO` (`tin_no` ASC),
  UNIQUE INDEX `UK_PARTYDETAIL_IATA_CODE` (`iata_code` ASC),
  UNIQUE INDEX `UK_PARTYDETAIL_GSTNO` (`gst_no` ASC),
  INDEX `FK_PARTY_PERIODID` (`period_id` ASC),
  CONSTRAINT `FK_PARTY_PERIODID`
    FOREIGN KEY (`period_id`)
    REFERENCES `efsdev`.`efs_period_master` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_party_master`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_party_master` (
  `id` BIGINT(20) NOT NULL,
  `category_code` VARCHAR(100) NULL DEFAULT NULL,
  `country_code` VARCHAR(10) NULL DEFAULT NULL,
  `first_name` VARCHAR(255) NULL DEFAULT NULL,
  `grade_name` VARCHAR(255) NULL DEFAULT NULL,
  `is_defaulter` VARCHAR(10) NULL DEFAULT NULL,
  `last_name` VARCHAR(255) NULL DEFAULT NULL,
  `open_date` DATETIME(6) NULL DEFAULT NULL,
  `party_code` VARCHAR(100) NOT NULL,
  `party_name` VARCHAR(100) NOT NULL,
  `salutation` VARCHAR(10) NULL DEFAULT NULL,
  `status` VARCHAR(10) NOT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  `category_id` BIGINT(20) NULL DEFAULT NULL,
  `country_id` BIGINT(20) NULL DEFAULT NULL,
  `grade_id` BIGINT(20) NULL DEFAULT NULL,
  `party_detail_id` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `UK_PARTY_PARTYCODE` (`party_code` ASC),
  UNIQUE INDEX `UK_PARTY_PARTYNAME` (`party_name` ASC),
  INDEX `IDX_PARTY_PARTYCOUNTRY` (`country_id` ASC),
  INDEX `IDX_PARTY_PARTYSTATUS` (`status` ASC),
  INDEX `FK_PARTY_CATEGORYID` (`category_id` ASC),
  INDEX `FK_PARTY_GRADEID` (`grade_id` ASC),
  INDEX `FK_PARTY_PARTYDETAILID` (`party_detail_id` ASC),
  CONSTRAINT `FK_PARTY_CATEGORYID`
    FOREIGN KEY (`category_id`)
    REFERENCES `efsdev`.`efs_category_master` (`id`),
  CONSTRAINT `FK_PARTY_COUNTRYID`
    FOREIGN KEY (`country_id`)
    REFERENCES `efsdev`.`efs_country_master` (`id`),
  CONSTRAINT `FK_PARTY_GRADEID`
    FOREIGN KEY (`grade_id`)
    REFERENCES `efsdev`.`efs_grade_master` (`id`),
  CONSTRAINT `FK_PARTY_PARTYDETAILID`
    FOREIGN KEY (`party_detail_id`)
    REFERENCES `efsdev`.`efs_party_master_detail` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_aes_freight_forwarder`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_aes_freight_forwarder` (
  `id` BIGINT(20) NOT NULL,
  `city_code` VARCHAR(100) NULL DEFAULT NULL,
  `company_code` VARCHAR(100) NULL DEFAULT NULL,
  `contact_no` VARCHAR(20) NULL DEFAULT NULL,
  `country_code` VARCHAR(100) NULL DEFAULT NULL,
  `first_name` VARCHAR(100) NULL DEFAULT NULL,
  `forwarder_code` VARCHAR(100) NULL DEFAULT NULL,
  `id_no` VARCHAR(30) NULL DEFAULT NULL,
  `id_type` VARCHAR(255) NULL DEFAULT NULL,
  `last_name` VARCHAR(100) NULL DEFAULT NULL,
  `state_code` VARCHAR(100) NULL DEFAULT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  `zip_code` VARCHAR(10) NOT NULL,
  `city_id` BIGINT(20) NOT NULL,
  `company_id` BIGINT(20) NULL DEFAULT NULL,
  `company_address_id` BIGINT(20) NOT NULL,
  `country_id` BIGINT(20) NOT NULL,
  `forwarder_id` BIGINT(20) NOT NULL,
  `state_province` BIGINT(20) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `FK_AES_FRE_FOR_CONS_CITY_ID` (`city_id` ASC),
  INDEX `FK_AES_FRE_FOR_CONS_COMP_ID` (`company_id` ASC),
  INDEX `FK_AESFREFOR_COS_COMP_ADD_ID` (`company_address_id` ASC),
  INDEX `FK_AES_FRE_FOR_CONS_COURY_ID` (`country_id` ASC),
  INDEX `FK_AES_FRE_FOR_CONS_ID` (`forwarder_id` ASC),
  INDEX `FK_AES_FRE_FOR_CONS_ST_ID` (`state_province` ASC),
  CONSTRAINT `FK_AESFREFOR_COS_COMP_ADD_ID`
    FOREIGN KEY (`company_address_id`)
    REFERENCES `efsdev`.`efs_address_master` (`id`),
  CONSTRAINT `FK_AES_FRE_FOR_CONS_CITY_ID`
    FOREIGN KEY (`city_id`)
    REFERENCES `efsdev`.`efs_city_master` (`id`),
  CONSTRAINT `FK_AES_FRE_FOR_CONS_COMP_ID`
    FOREIGN KEY (`company_id`)
    REFERENCES `efsdev`.`efs_party_master` (`id`),
  CONSTRAINT `FK_AES_FRE_FOR_CONS_COURY_ID`
    FOREIGN KEY (`country_id`)
    REFERENCES `efsdev`.`efs_country_master` (`id`),
  CONSTRAINT `FK_AES_FRE_FOR_CONS_ID`
    FOREIGN KEY (`forwarder_id`)
    REFERENCES `efsdev`.`efs_party_master` (`id`),
  CONSTRAINT `FK_AES_FRE_FOR_CONS_ST_ID`
    FOREIGN KEY (`state_province`)
    REFERENCES `efsdev`.`efs_state_province_master` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_aes_intmedi_consig`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_aes_intmedi_consig` (
  `id` BIGINT(20) NOT NULL,
  `city_code` VARCHAR(100) NULL DEFAULT NULL,
  `company_code` VARCHAR(100) NULL DEFAULT NULL,
  `contact_no` VARCHAR(20) NULL DEFAULT NULL,
  `country_code` VARCHAR(100) NULL DEFAULT NULL,
  `first_name` VARCHAR(100) NULL DEFAULT NULL,
  `intermediate_consignee_code` VARCHAR(100) NULL DEFAULT NULL,
  `last_name` VARCHAR(100) NULL DEFAULT NULL,
  `state_code` VARCHAR(100) NULL DEFAULT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  `zip_code` VARCHAR(10) NULL DEFAULT NULL,
  `city_id` BIGINT(20) NULL DEFAULT NULL,
  `company_id` BIGINT(20) NULL DEFAULT NULL,
  `company_address_id` BIGINT(20) NULL DEFAULT NULL,
  `country_id` BIGINT(20) NULL DEFAULT NULL,
  `intermediate_consignee_id` BIGINT(20) NULL DEFAULT NULL,
  `state_province` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `FK_AES_INTER_CONS_CITY_ID` (`city_id` ASC),
  INDEX `FK_AES_INTER_CONS_COMPANY_ID` (`company_id` ASC),
  INDEX `FK_AESINTER_CONSCOMPANYADDRID` (`company_address_id` ASC),
  INDEX `FK_AES_INTER_CONS_COUNTRY_ID` (`country_id` ASC),
  INDEX `FK_AES_INTER_CONS_ID` (`intermediate_consignee_id` ASC),
  INDEX `FK_AES_INTER_CONS_STATE_ID` (`state_province` ASC),
  CONSTRAINT `FK_AESINTER_CONSCOMPANYADDRID`
    FOREIGN KEY (`company_address_id`)
    REFERENCES `efsdev`.`efs_address_master` (`id`),
  CONSTRAINT `FK_AES_INTER_CONS_CITY_ID`
    FOREIGN KEY (`city_id`)
    REFERENCES `efsdev`.`efs_city_master` (`id`),
  CONSTRAINT `FK_AES_INTER_CONS_COMPANY_ID`
    FOREIGN KEY (`company_id`)
    REFERENCES `efsdev`.`efs_party_master` (`id`),
  CONSTRAINT `FK_AES_INTER_CONS_COUNTRY_ID`
    FOREIGN KEY (`country_id`)
    REFERENCES `efsdev`.`efs_country_master` (`id`),
  CONSTRAINT `FK_AES_INTER_CONS_ID`
    FOREIGN KEY (`intermediate_consignee_id`)
    REFERENCES `efsdev`.`efs_party_master` (`id`),
  CONSTRAINT `FK_AES_INTER_CONS_STATE_ID`
    FOREIGN KEY (`state_province`)
    REFERENCES `efsdev`.`efs_state_province_master` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_aes_ultimate_consignee`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_aes_ultimate_consignee` (
  `id` BIGINT(20) NOT NULL,
  `city_code` VARCHAR(100) NULL DEFAULT NULL,
  `company_code` VARCHAR(100) NULL DEFAULT NULL,
  `consignee_type` VARCHAR(255) NOT NULL,
  `contact_no` VARCHAR(20) NULL DEFAULT NULL,
  `country_code` VARCHAR(100) NULL DEFAULT NULL,
  `first_name` VARCHAR(100) NULL DEFAULT NULL,
  `id_no` VARCHAR(30) NULL DEFAULT NULL,
  `id_type` VARCHAR(255) NULL DEFAULT NULL,
  `last_name` VARCHAR(100) NULL DEFAULT NULL,
  `sold_en_route` VARCHAR(10) NULL DEFAULT NULL,
  `state_code` VARCHAR(100) NULL DEFAULT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `ultimate_consignee_code` VARCHAR(100) NULL DEFAULT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  `zip_code` VARCHAR(10) NULL DEFAULT NULL,
  `city_id` BIGINT(20) NULL DEFAULT NULL,
  `company_id` BIGINT(20) NULL DEFAULT NULL,
  `company_address_id` BIGINT(20) NOT NULL,
  `country_id` BIGINT(20) NULL DEFAULT NULL,
  `state_province` BIGINT(20) NULL DEFAULT NULL,
  `ultimate_consignee_id` BIGINT(20) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `FK_AES_ULTI_CONS_CITY_ID` (`city_id` ASC),
  INDEX `FK_AES_ULTI_CONS_COMPANY_ID` (`company_id` ASC),
  INDEX `FK_AES_ULTI_CONS_COM_ADD_ID` (`company_address_id` ASC),
  INDEX `FK_AES_ULTI_CONS_COUNTRY_ID` (`country_id` ASC),
  INDEX `FK_AES_ULTI_CONS_STATE_ID` (`state_province` ASC),
  INDEX `FK_AES_ULTI_CONS_ID` (`ultimate_consignee_id` ASC),
  CONSTRAINT `FK_AES_ULTI_CONS_CITY_ID`
    FOREIGN KEY (`city_id`)
    REFERENCES `efsdev`.`efs_city_master` (`id`),
  CONSTRAINT `FK_AES_ULTI_CONS_COMPANY_ID`
    FOREIGN KEY (`company_id`)
    REFERENCES `efsdev`.`efs_party_master` (`id`),
  CONSTRAINT `FK_AES_ULTI_CONS_COM_ADD_ID`
    FOREIGN KEY (`company_address_id`)
    REFERENCES `efsdev`.`efs_address_master` (`id`),
  CONSTRAINT `FK_AES_ULTI_CONS_COUNTRY_ID`
    FOREIGN KEY (`country_id`)
    REFERENCES `efsdev`.`efs_country_master` (`id`),
  CONSTRAINT `FK_AES_ULTI_CONS_ID`
    FOREIGN KEY (`ultimate_consignee_id`)
    REFERENCES `efsdev`.`efs_party_master` (`id`),
  CONSTRAINT `FK_AES_ULTI_CONS_STATE_ID`
    FOREIGN KEY (`state_province`)
    REFERENCES `efsdev`.`efs_state_province_master` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_aes_usppi`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_aes_usppi` (
  `id` BIGINT(20) NOT NULL,
  `cargo_origin_code` VARCHAR(100) NULL DEFAULT NULL,
  `city_code` VARCHAR(100) NULL DEFAULT NULL,
  `contact_no` VARCHAR(20) NULL DEFAULT NULL,
  `country_code` VARCHAR(100) NULL DEFAULT NULL,
  `ein_no` VARCHAR(30) NULL DEFAULT NULL,
  `first_name` VARCHAR(100) NULL DEFAULT NULL,
  `id_no` VARCHAR(11) NULL DEFAULT NULL,
  `id_type` VARCHAR(255) NULL DEFAULT NULL,
  `last_name` VARCHAR(100) NULL DEFAULT NULL,
  `shipper_code` VARCHAR(100) NULL DEFAULT NULL,
  `state_code` VARCHAR(100) NULL DEFAULT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  `zip_code` VARCHAR(10) NULL DEFAULT NULL,
  `cargo_origin_id` BIGINT(20) NULL DEFAULT NULL,
  `cargo_origin_address_id` BIGINT(20) NOT NULL,
  `city_id` BIGINT(20) NULL DEFAULT NULL,
  `country_id` BIGINT(20) NULL DEFAULT NULL,
  `shipper_id` BIGINT(20) NOT NULL,
  `state_province` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `FK_USPPI_CARGO_ORIGIN_ID` (`cargo_origin_id` ASC),
  INDEX `FK_USPPI_CARGO_ORIGIN_ADDR_ID` (`cargo_origin_address_id` ASC),
  INDEX `FK_USPPI_CITY_ID` (`city_id` ASC),
  INDEX `FK_USPPI_COUNTRY_ID` (`country_id` ASC),
  INDEX `FK_USPPI_SHIPPER_ID` (`shipper_id` ASC),
  INDEX `FK_USPPI_STATE_ID` (`state_province` ASC),
  CONSTRAINT `FK_USPPI_CARGO_ORIGIN_ADDR_ID`
    FOREIGN KEY (`cargo_origin_address_id`)
    REFERENCES `efsdev`.`efs_address_master` (`id`),
  CONSTRAINT `FK_USPPI_CARGO_ORIGIN_ID`
    FOREIGN KEY (`cargo_origin_id`)
    REFERENCES `efsdev`.`efs_party_master` (`id`),
  CONSTRAINT `FK_USPPI_CITY_ID`
    FOREIGN KEY (`city_id`)
    REFERENCES `efsdev`.`efs_city_master` (`id`),
  CONSTRAINT `FK_USPPI_COUNTRY_ID`
    FOREIGN KEY (`country_id`)
    REFERENCES `efsdev`.`efs_country_master` (`id`),
  CONSTRAINT `FK_USPPI_SHIPPER_ID`
    FOREIGN KEY (`shipper_id`)
    REFERENCES `efsdev`.`efs_party_master` (`id`),
  CONSTRAINT `FK_USPPI_STATE_ID`
    FOREIGN KEY (`state_province`)
    REFERENCES `efsdev`.`efs_state_province_master` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_iata_master`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_iata_master` (
  `id` BIGINT(20) NOT NULL,
  `iata_code` VARCHAR(10) NOT NULL,
  `iata_name` VARCHAR(100) NOT NULL,
  `status` VARCHAR(10) NOT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `UK_IATAMASTER_IATACODE` (`iata_code` ASC),
  UNIQUE INDEX `UK_IATAMASTER_IATANAME` (`iata_name` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_inbound_type_master`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_inbound_type_master` (
  `id` BIGINT(20) NOT NULL,
  `inbond_type` VARCHAR(100) NOT NULL,
  `status` VARCHAR(10) NOT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_cost_center_master`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_cost_center_master` (
  `id` BIGINT(20) NOT NULL,
  `cost_center_code` VARCHAR(10) NOT NULL,
  `cost_center_name` VARCHAR(100) NOT NULL,
  `status` VARCHAR(10) NOT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `UK_COSTCENTER_COSTCENTERCODE` (`cost_center_code` ASC),
  UNIQUE INDEX `UK_COSTCENTER_COSTCENTERNAME` (`cost_center_name` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_division_master`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_division_master` (
  `id` BIGINT(20) NOT NULL,
  `division_code` VARCHAR(10) NOT NULL,
  `division_name` VARCHAR(100) NOT NULL,
  `status` VARCHAR(10) NOT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  `company_id` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `UK_DIVISION_DIVISIONCODE` (`division_code` ASC, `company_id` ASC),
  UNIQUE INDEX `UK_DIVISION_DIVISIONNAME` (`division_name` ASC, `company_id` ASC),
  INDEX `FK_DIVISION_COMPANYID` (`company_id` ASC),
  CONSTRAINT `FK_DIVISION_COMPANYID`
    FOREIGN KEY (`company_id`)
    REFERENCES `efsdev`.`efs_company_master` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_zone_master`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_zone_master` (
  `id` BIGINT(20) NOT NULL,
  `country_code` VARCHAR(10) NULL DEFAULT NULL,
  `status` VARCHAR(10) NOT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  `zone_code` VARCHAR(10) NOT NULL,
  `zone_name` VARCHAR(100) NOT NULL,
  `country_id` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `UK_ZONE_ZONECODE` (`zone_code` ASC),
  UNIQUE INDEX `UK_ZONE_ZONENAME` (`zone_name` ASC),
  INDEX `FK_ZONE_COUNTRYID` (`country_id` ASC),
  CONSTRAINT `FK_ZONE_COUNTRYID`
    FOREIGN KEY (`country_id`)
    REFERENCES `efsdev`.`efs_country_master` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_location_master`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_location_master` (
  `id` BIGINT(20) NOT NULL,
  `address_line_1` VARCHAR(255) NULL DEFAULT NULL,
  `address_line_2` VARCHAR(255) NULL DEFAULT NULL,
  `address_line_3` VARCHAR(255) NULL DEFAULT NULL,
  `address_line_4` VARCHAR(255) NULL DEFAULT NULL,
  `bank_account_name` VARCHAR(50) NULL DEFAULT NULL,
  `bank_account_number` VARCHAR(25) NULL DEFAULT NULL,
  `bank_account_type` VARCHAR(20) NULL DEFAULT NULL,
  `bank_address` VARCHAR(255) NULL DEFAULT NULL,
  `bank_name` VARCHAR(50) NULL DEFAULT NULL,
  `branch_name` VARCHAR(100) NULL DEFAULT NULL,
  `city_no` VARCHAR(255) NULL DEFAULT NULL,
  `company_code` VARCHAR(10) NULL DEFAULT NULL,
  `cost_center_code` VARCHAR(10) NULL DEFAULT NULL,
  `country_code` VARCHAR(10) NULL DEFAULT NULL,
  `country_no` VARCHAR(255) NULL DEFAULT NULL,
  `date_format` VARCHAR(30) NOT NULL,
  `date_time_format` VARCHAR(30) NOT NULL,
  `email` VARCHAR(500) NULL DEFAULT NULL,
  `first_name` VARCHAR(255) NULL DEFAULT NULL,
  `is_approved` VARCHAR(5) NULL DEFAULT NULL,
  `is_head_office` VARCHAR(5) NULL DEFAULT NULL,
  `is_owr_own_office` VARCHAR(5) NULL DEFAULT NULL,
  `last_name` VARCHAR(255) NULL DEFAULT NULL,
  `location_code` VARCHAR(10) NOT NULL,
  `location_name` VARCHAR(100) NOT NULL,
  `is_measurement` VARCHAR(20) NULL DEFAULT NULL,
  `phone` VARCHAR(100) NULL DEFAULT NULL,
  `salutation` VARCHAR(10) NULL DEFAULT NULL,
  `status` VARCHAR(10) NOT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `time_zone` VARCHAR(255) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  `zip_code` VARCHAR(10) NULL DEFAULT NULL,
  `city_id` BIGINT(20) NULL DEFAULT NULL,
  `company_id` BIGINT(20) NULL DEFAULT NULL,
  `cost_center_id` BIGINT(20) NULL DEFAULT NULL,
  `country_id` BIGINT(20) NULL DEFAULT NULL,
  `currency_id` BIGINT(20) NULL DEFAULT NULL,
  `division_id` BIGINT(20) NULL DEFAULT NULL,
  `agent_id` BIGINT(20) NULL DEFAULT NULL,
  `region_id` BIGINT(20) NULL DEFAULT NULL,
  `state_id` BIGINT(20) NULL DEFAULT NULL,
  `zone_id` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `UK_LOCATION_LOCATIONCODE` (`location_code` ASC),
  UNIQUE INDEX `UK_LOCATION_LOCATIONNAME` (`location_name` ASC),
  INDEX `FK_LOCATION_CITYID` (`city_id` ASC),
  INDEX `FK_LOCATION_COMPANYID` (`company_id` ASC),
  INDEX `FK_LOCATION_COSTCENTERID` (`cost_center_id` ASC),
  INDEX `FK_LOCATION_COUNTRYID` (`country_id` ASC),
  INDEX `FK_LOC_CURRENCYID` (`currency_id` ASC),
  INDEX `FK_LOCATION_DIVISIONID` (`division_id` ASC),
  INDEX `FK_LOCATION_AGENTID` (`agent_id` ASC),
  INDEX `FK_LOCATION_REGIONID` (`region_id` ASC),
  INDEX `FK_LOCATION_STATE` (`state_id` ASC),
  INDEX `FK_LOCATION_ZONEID` (`zone_id` ASC),
  CONSTRAINT `FK_LOCATION_AGENTID`
    FOREIGN KEY (`agent_id`)
    REFERENCES `efsdev`.`efs_party_master` (`id`),
  CONSTRAINT `FK_LOCATION_CITYID`
    FOREIGN KEY (`city_id`)
    REFERENCES `efsdev`.`efs_city_master` (`id`),
  CONSTRAINT `FK_LOCATION_COMPANYID`
    FOREIGN KEY (`company_id`)
    REFERENCES `efsdev`.`efs_company_master` (`id`),
  CONSTRAINT `FK_LOCATION_COSTCENTERID`
    FOREIGN KEY (`cost_center_id`)
    REFERENCES `efsdev`.`efs_cost_center_master` (`id`),
  CONSTRAINT `FK_LOCATION_COUNTRYID`
    FOREIGN KEY (`country_id`)
    REFERENCES `efsdev`.`efs_country_master` (`id`),
  CONSTRAINT `FK_LOCATION_DIVISIONID`
    FOREIGN KEY (`division_id`)
    REFERENCES `efsdev`.`efs_division_master` (`id`),
  CONSTRAINT `FK_LOCATION_REGIONID`
    FOREIGN KEY (`region_id`)
    REFERENCES `efsdev`.`efs_region_master` (`id`),
  CONSTRAINT `FK_LOCATION_STATE`
    FOREIGN KEY (`state_id`)
    REFERENCES `efsdev`.`efs_state_province_master` (`id`),
  CONSTRAINT `FK_LOCATION_ZONEID`
    FOREIGN KEY (`zone_id`)
    REFERENCES `efsdev`.`efs_zone_master` (`id`),
  CONSTRAINT `FK_LOC_CURRENCYID`
    FOREIGN KEY (`currency_id`)
    REFERENCES `efsdev`.`efs_currency_master` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_aes_us_state_code_master`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_aes_us_state_code_master` (
  `id` BIGINT(20) NOT NULL,
  `name` VARCHAR(100) NOT NULL,
  `state_code` VARCHAR(100) NOT NULL,
  `status` VARCHAR(10) NOT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `UK_AES_USST_CODE_MASTER_CODE` (`state_code` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_aes_us_foreign_port_master`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_aes_us_foreign_port_master` (
  `id` BIGINT(20) NOT NULL,
  `port_code` VARCHAR(10) NOT NULL,
  `port_name` VARCHAR(100) NOT NULL,
  `transport_mode` VARCHAR(10) NULL DEFAULT NULL,
  `status` VARCHAR(10) NOT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `UK_AES_US_FOREIGN_PORT_CODE` (`port_code` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_aes_us_port_master`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_aes_us_port_master` (
  `id` BIGINT(20) NOT NULL,
  `port_code` VARCHAR(10) NOT NULL,
  `port_name` VARCHAR(100) NOT NULL,
  `transport_mode` VARCHAR(10) NULL DEFAULT NULL,
  `status` VARCHAR(10) NOT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `UK_AES_US_PORT_MASTER_CODE` (`port_code` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_scac_master`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_scac_master` (
  `id` BIGINT(20) NOT NULL,
  `scac_code` VARCHAR(10) NOT NULL,
  `scac_name` VARCHAR(100) NOT NULL,
  `status` VARCHAR(10) NOT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `UK_SCACMASTER_SCACCODE` (`scac_code` ASC),
  UNIQUE INDEX `UK_SCACMASTER_SCACNAME` (`scac_name` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_aes_transport_mode`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_aes_transport_mode` (
  `id` BIGINT(20) NOT NULL,
  `aes_transport_name` VARCHAR(100) NOT NULL,
  `status` VARCHAR(10) NOT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_aes_file`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_aes_file` (
  `id` BIGINT(20) NOT NULL,
  `inbond_type` VARCHAR(255) NULL DEFAULT NULL,
  `aes_action` VARCHAR(30) NULL DEFAULT NULL,
  `aes_filing_type` VARCHAR(255) NOT NULL,
  `aes_itn_no` VARCHAR(50) NULL DEFAULT NULL,
  `aes_queue_msg` LONGTEXT NULL DEFAULT NULL,
  `aes_status` VARCHAR(150) NULL DEFAULT NULL,
  `aes_transport_name` VARCHAR(255) NULL DEFAULT NULL,
  `batch_no` VARCHAR(255) NULL DEFAULT NULL,
  `carrier_code` VARCHAR(100) NULL DEFAULT NULL,
  `company_code` VARCHAR(100) NULL DEFAULT NULL,
  `country_code` VARCHAR(100) NULL DEFAULT NULL,
  `destination_country_code` VARCHAR(100) NULL DEFAULT NULL,
  `etd` DATETIME(6) NULL DEFAULT NULL,
  `filing_option` VARCHAR(255) NULL DEFAULT NULL,
  `foreign_trade_zone` VARCHAR(50) NULL DEFAULT NULL,
  `hawb_no` VARCHAR(100) NULL DEFAULT NULL,
  `hazardous_cargo` VARCHAR(255) NOT NULL,
  `iata_code` VARCHAR(30) NULL DEFAULT NULL,
  `iata_scac_code` VARCHAR(100) NULL DEFAULT NULL,
  `import_entry_no` VARCHAR(50) NULL DEFAULT NULL,
  `location_code` VARCHAR(100) NULL DEFAULT NULL,
  `master_uid` VARCHAR(255) NULL DEFAULT NULL,
  `mawb_no` VARCHAR(30) NULL DEFAULT NULL,
  `origin_state_code` VARCHAR(100) NULL DEFAULT NULL,
  `pod_code` VARCHAR(100) NULL DEFAULT NULL,
  `pol_code` VARCHAR(100) NULL DEFAULT NULL,
  `routed_transaction` VARCHAR(255) NOT NULL,
  `scac_code` VARCHAR(100) NULL DEFAULT NULL,
  `segment_code` VARCHAR(255) NULL DEFAULT NULL,
  `service_uid` VARCHAR(255) NOT NULL,
  `shipment_uid` VARCHAR(255) NOT NULL,
  `status` VARCHAR(50) NULL DEFAULT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `usspi_ultimate_consignee` VARCHAR(255) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  `inbond_id` BIGINT(20) NULL DEFAULT NULL,
  `aes_freight_forwarder_id` BIGINT(20) NULL DEFAULT NULL,
  `aes_intermediate_consignee_id` BIGINT(20) NULL DEFAULT NULL,
  `aes_transport` BIGINT(20) NOT NULL,
  `aes_ultimate_consignee_id` BIGINT(20) NULL DEFAULT NULL,
  `aes_usppi_id` BIGINT(20) NULL DEFAULT NULL,
  `carrier_id` BIGINT(20) NULL DEFAULT NULL,
  `company_id` BIGINT(20) NULL DEFAULT NULL,
  `country_id` BIGINT(20) NULL DEFAULT NULL,
  `destination_country_id` BIGINT(20) NOT NULL,
  `iata_id` BIGINT(20) NULL DEFAULT NULL,
  `location_id` BIGINT(20) NULL DEFAULT NULL,
  `origin_state_id` BIGINT(20) NOT NULL,
  `pod` BIGINT(20) NOT NULL,
  `pol` BIGINT(20) NOT NULL,
  `scac_id` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `FK_AES_INBOND_ID` (`inbond_id` ASC),
  INDEX `FK_AES_FILE_FRE_FOR_ID` (`aes_freight_forwarder_id` ASC),
  INDEX `FK_AES_FILE_INTER_CONSIG_ID` (`aes_intermediate_consignee_id` ASC),
  INDEX `FK_AES_TRANSPORT_ID` (`aes_transport` ASC),
  INDEX `FK_AES_FILE_ULTI_CONSIG_ID` (`aes_ultimate_consignee_id` ASC),
  INDEX `FK_AES_FILE_USPPI_ID` (`aes_usppi_id` ASC),
  INDEX `FK_AES_CARRIER_ID` (`carrier_id` ASC),
  INDEX `FK_AES_COMPANYID` (`company_id` ASC),
  INDEX `FK_AES_COUNTRYID` (`country_id` ASC),
  INDEX `FK_AES_DESTINATION_COUNTRY_ID` (`destination_country_id` ASC),
  INDEX `FK_AES_IATA_ID` (`iata_id` ASC),
  INDEX `FK_AES_LOCATIONID` (`location_id` ASC),
  INDEX `FK_AES_ORIGIN_STATE_ID` (`origin_state_id` ASC),
  INDEX `FK_AES_POD_ID` (`pod` ASC),
  INDEX `FK_AES_POL_ID` (`pol` ASC),
  INDEX `FK_AES_SCAC_ID` (`scac_id` ASC),
  CONSTRAINT `FK_AES_CARRIER_ID`
    FOREIGN KEY (`carrier_id`)
    REFERENCES `efsdev`.`efs_carrier_master` (`id`),
  CONSTRAINT `FK_AES_COMPANYID`
    FOREIGN KEY (`company_id`)
    REFERENCES `efsdev`.`efs_company_master` (`id`),
  CONSTRAINT `FK_AES_COUNTRYID`
    FOREIGN KEY (`country_id`)
    REFERENCES `efsdev`.`efs_country_master` (`id`),
  CONSTRAINT `FK_AES_DESTINATION_COUNTRY_ID`
    FOREIGN KEY (`destination_country_id`)
    REFERENCES `efsdev`.`efs_country_master` (`id`),
  CONSTRAINT `FK_AES_FILE_FRE_FOR_ID`
    FOREIGN KEY (`aes_freight_forwarder_id`)
    REFERENCES `efsdev`.`efs_aes_freight_forwarder` (`id`),
  CONSTRAINT `FK_AES_FILE_INTER_CONSIG_ID`
    FOREIGN KEY (`aes_intermediate_consignee_id`)
    REFERENCES `efsdev`.`efs_aes_intmedi_consig` (`id`),
  CONSTRAINT `FK_AES_FILE_ULTI_CONSIG_ID`
    FOREIGN KEY (`aes_ultimate_consignee_id`)
    REFERENCES `efsdev`.`efs_aes_ultimate_consignee` (`id`),
  CONSTRAINT `FK_AES_FILE_USPPI_ID`
    FOREIGN KEY (`aes_usppi_id`)
    REFERENCES `efsdev`.`efs_aes_usppi` (`id`),
  CONSTRAINT `FK_AES_IATA_ID`
    FOREIGN KEY (`iata_id`)
    REFERENCES `efsdev`.`efs_iata_master` (`id`),
  CONSTRAINT `FK_AES_INBOND_ID`
    FOREIGN KEY (`inbond_id`)
    REFERENCES `efsdev`.`efs_inbound_type_master` (`id`),
  CONSTRAINT `FK_AES_LOCATIONID`
    FOREIGN KEY (`location_id`)
    REFERENCES `efsdev`.`efs_location_master` (`id`),
  CONSTRAINT `FK_AES_ORIGIN_STATE_ID`
    FOREIGN KEY (`origin_state_id`)
    REFERENCES `efsdev`.`efs_aes_us_state_code_master` (`id`),
  CONSTRAINT `FK_AES_POD_ID`
    FOREIGN KEY (`pod`)
    REFERENCES `efsdev`.`efs_aes_us_foreign_port_master` (`id`),
  CONSTRAINT `FK_AES_POL_ID`
    FOREIGN KEY (`pol`)
    REFERENCES `efsdev`.`efs_aes_us_port_master` (`id`),
  CONSTRAINT `FK_AES_SCAC_ID`
    FOREIGN KEY (`scac_id`)
    REFERENCES `efsdev`.`efs_scac_master` (`id`),
  CONSTRAINT `FK_AES_TRANSPORT_ID`
    FOREIGN KEY (`aes_transport`)
    REFERENCES `efsdev`.`efs_aes_transport_mode` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_export_code_master`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_export_code_master` (
  `id` BIGINT(20) NOT NULL,
  `export_code` VARCHAR(50) NOT NULL,
  `export_name` VARCHAR(100) NOT NULL,
  `status` VARCHAR(10) NOT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_unit_master`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_unit_master` (
  `id` BIGINT(20) NOT NULL,
  `calc_type1` VARCHAR(20) NULL DEFAULT NULL,
  `calc_type2` VARCHAR(20) NULL DEFAULT NULL,
  `calc_value1` BIGINT(20) NULL DEFAULT NULL,
  `calc_value2` BIGINT(20) NULL DEFAULT NULL,
  `decimals` VARCHAR(10) NULL DEFAULT NULL,
  `mapping_unit1` VARCHAR(20) NULL DEFAULT NULL,
  `mapping_unit2` VARCHAR(20) NULL DEFAULT NULL,
  `status` VARCHAR(10) NOT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `unit_code` VARCHAR(10) NOT NULL,
  `unit_name` VARCHAR(100) NOT NULL,
  `unit_type` VARCHAR(20) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `UK_UNIT_UNITCODE` (`unit_code` ASC),
  UNIQUE INDEX `UK_UNIT_UNITNAME` (`unit_name` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_itar_excemption`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_itar_excemption` (
  `id` BIGINT(20) NOT NULL,
  `excemption_code` VARCHAR(50) NOT NULL,
  `excemption_name` VARCHAR(100) NOT NULL,
  `excemption_n0` VARCHAR(100) NOT NULL,
  `status` VARCHAR(10) NOT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_aes_license`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_aes_license` (
  `id` BIGINT(20) NOT NULL,
  `license_code` VARCHAR(50) NOT NULL,
  `license_name` VARCHAR(100) NOT NULL,
  `license_type` VARCHAR(100) NOT NULL,
  `status` VARCHAR(10) NOT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_aes_schedule_b`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_aes_schedule_b` (
  `id` BIGINT(20) NOT NULL,
  `schedule_code` VARCHAR(50) NOT NULL,
  `schedule_name` VARCHAR(100) NOT NULL,
  `status` VARCHAR(10) NOT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  `commodity_id` BIGINT(20) NOT NULL,
  `firs_unit_id` BIGINT(20) NOT NULL,
  `secon_unit_id` BIGINT(20) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `FK_AESSCHEDULE_COMMODITY_ID` (`commodity_id` ASC),
  INDEX `FK_AESSCHEDULE_FUNIT_ID` (`firs_unit_id` ASC),
  INDEX `FK_AESSCHEDULE_SUNIT_ID` (`secon_unit_id` ASC),
  CONSTRAINT `FK_AESSCHEDULE_COMMODITY_ID`
    FOREIGN KEY (`commodity_id`)
    REFERENCES `efsdev`.`efs_hs_code_master` (`id`),
  CONSTRAINT `FK_AESSCHEDULE_FUNIT_ID`
    FOREIGN KEY (`firs_unit_id`)
    REFERENCES `efsdev`.`efs_unit_master` (`id`),
  CONSTRAINT `FK_AESSCHEDULE_SUNIT_ID`
    FOREIGN KEY (`secon_unit_id`)
    REFERENCES `efsdev`.`efs_unit_master` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_aes_commodity`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_aes_commodity` (
  `id` BIGINT(20) NOT NULL,
  `action` VARCHAR(30) NULL DEFAULT NULL,
  `aes_export_code` VARCHAR(100) NULL DEFAULT NULL,
  `license_code` VARCHAR(255) NULL DEFAULT NULL,
  `commodity_code` VARCHAR(255) NULL DEFAULT NULL,
  `unit_code` VARCHAR(100) NULL DEFAULT NULL,
  `ddtc_eligibility_indicator` VARCHAR(255) NULL DEFAULT NULL,
  `dtdc_no` VARCHAR(10) NULL DEFAULT NULL,
  `dtdc_quantity` BIGINT(20) NULL DEFAULT NULL,
  `eccn` VARCHAR(50) NULL DEFAULT NULL,
  `eligible_party_certification` VARCHAR(255) NULL DEFAULT NULL,
  `export_license_no` VARCHAR(50) NULL DEFAULT NULL,
  `first_no_of_pieces` BIGINT(20) NULL DEFAULT NULL,
  `first_unit_code` VARCHAR(100) NULL DEFAULT NULL,
  `gross_weight_in_kg` DOUBLE NOT NULL,
  `is_used_vehicle` VARCHAR(255) NULL DEFAULT NULL,
  `aes_itar_excemption_code` VARCHAR(100) NULL DEFAULT NULL,
  `license_value` VARCHAR(50) NULL DEFAULT NULL,
  `origin_goods` VARCHAR(30) NULL DEFAULT NULL,
  `schedule_code` VARCHAR(255) NULL DEFAULT NULL,
  `second_no_of_pieces` BIGINT(20) NULL DEFAULT NULL,
  `second_unit_code` VARCHAR(100) NULL DEFAULT NULL,
  `sme_indicator` VARCHAR(255) NULL DEFAULT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `category_code` VARCHAR(100) NULL DEFAULT NULL,
  `value_in_usd` DOUBLE NOT NULL,
  `vehicle_id_type` VARCHAR(255) NULL DEFAULT NULL,
  `vehicle_product_id` VARCHAR(255) NULL DEFAULT NULL,
  `vehicle_title` VARCHAR(255) NULL DEFAULT NULL,
  `vehicle_title_state` VARCHAR(255) NULL DEFAULT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  `aes_export_id` BIGINT(20) NOT NULL,
  `license_id` BIGINT(20) NOT NULL,
  `schedule_id` BIGINT(20) NOT NULL,
  `aes_id` BIGINT(20) NOT NULL,
  `commodity_id` BIGINT(20) NOT NULL,
  `unit_code_id` BIGINT(20) NULL DEFAULT NULL,
  `first_unit_id` BIGINT(20) NOT NULL,
  `aes_itar_excemption_id` BIGINT(20) NULL DEFAULT NULL,
  `second_unit_id` BIGINT(20) NOT NULL,
  `category_id` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `FK_AES_EXPORT_ID` (`aes_export_id` ASC),
  INDEX `FK_AES_LICENSE_ID` (`license_id` ASC),
  INDEX `FK_AES_SCHEDULE_ID` (`schedule_id` ASC),
  INDEX `FK_AES_COMM_ID` (`aes_id` ASC),
  INDEX `FK_AES_COMMODITY_ID` (`commodity_id` ASC),
  INDEX `FK_AES_UNITCODEID` (`unit_code_id` ASC),
  INDEX `FK_AES_FUNIT_ID` (`first_unit_id` ASC),
  INDEX `FK_AES_ITAR_ID` (`aes_itar_excemption_id` ASC),
  INDEX `FK_AES_SUNIT_ID` (`second_unit_id` ASC),
  INDEX `FK_AES_CATEGORYID` (`category_id` ASC),
  CONSTRAINT `FK_AES_CATEGORYID`
    FOREIGN KEY (`category_id`)
    REFERENCES `efsdev`.`efs_category_master` (`id`),
  CONSTRAINT `FK_AES_COMMODITY_ID`
    FOREIGN KEY (`commodity_id`)
    REFERENCES `efsdev`.`efs_hs_code_master` (`id`),
  CONSTRAINT `FK_AES_COMM_ID`
    FOREIGN KEY (`aes_id`)
    REFERENCES `efsdev`.`efs_aes_file` (`id`),
  CONSTRAINT `FK_AES_EXPORT_ID`
    FOREIGN KEY (`aes_export_id`)
    REFERENCES `efsdev`.`efs_export_code_master` (`id`),
  CONSTRAINT `FK_AES_FUNIT_ID`
    FOREIGN KEY (`first_unit_id`)
    REFERENCES `efsdev`.`efs_unit_master` (`id`),
  CONSTRAINT `FK_AES_ITAR_ID`
    FOREIGN KEY (`aes_itar_excemption_id`)
    REFERENCES `efsdev`.`efs_itar_excemption` (`id`),
  CONSTRAINT `FK_AES_LICENSE_ID`
    FOREIGN KEY (`license_id`)
    REFERENCES `efsdev`.`efs_aes_license` (`id`),
  CONSTRAINT `FK_AES_SCHEDULE_ID`
    FOREIGN KEY (`schedule_id`)
    REFERENCES `efsdev`.`efs_aes_schedule_b` (`id`),
  CONSTRAINT `FK_AES_SUNIT_ID`
    FOREIGN KEY (`second_unit_id`)
    REFERENCES `efsdev`.`efs_unit_master` (`id`),
  CONSTRAINT `FK_AES_UNITCODEID`
    FOREIGN KEY (`unit_code_id`)
    REFERENCES `efsdev`.`efs_unit_master` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_aes_file_edi`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_aes_file_edi` (
  `id` BIGINT(20) NOT NULL,
  `company_code` VARCHAR(30) NULL DEFAULT NULL,
  `country_code` VARCHAR(30) NULL DEFAULT NULL,
  `location_code` VARCHAR(30) NULL DEFAULT NULL,
  `messaging_error` VARCHAR(4000) NULL DEFAULT NULL,
  `messaging_status` VARCHAR(15) NULL DEFAULT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  `aes_file_id` BIGINT(20) NULL DEFAULT NULL,
  `company_id` BIGINT(20) NULL DEFAULT NULL,
  `country_id` BIGINT(20) NULL DEFAULT NULL,
  `location_id` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `FK_AIR_AES_EDI_ID` (`aes_file_id` ASC),
  INDEX `FK_AIR_AES_EDI_COMPANYID` (`company_id` ASC),
  INDEX `FK_AIR_AES_EDI_COUNTRYID` (`country_id` ASC),
  INDEX `FK_AIR_AES_EDI_LOCATIONID` (`location_id` ASC),
  CONSTRAINT `FK_AIR_AES_EDI_COMPANYID`
    FOREIGN KEY (`company_id`)
    REFERENCES `efsdev`.`efs_company_master` (`id`),
  CONSTRAINT `FK_AIR_AES_EDI_COUNTRYID`
    FOREIGN KEY (`country_id`)
    REFERENCES `efsdev`.`efs_country_master` (`id`),
  CONSTRAINT `FK_AIR_AES_EDI_ID`
    FOREIGN KEY (`aes_file_id`)
    REFERENCES `efsdev`.`efs_aes_file` (`id`),
  CONSTRAINT `FK_AIR_AES_EDI_LOCATIONID`
    FOREIGN KEY (`location_id`)
    REFERENCES `efsdev`.`efs_location_master` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_aes_file_edi_status`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_aes_file_edi_status` (
  `id` BIGINT(20) NOT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `messaging_error` VARCHAR(4000) NULL DEFAULT NULL,
  `messaging_status` VARCHAR(250) NULL DEFAULT NULL,
  `mq_action_status` VARCHAR(255) NULL DEFAULT NULL,
  `mq_action_type` VARCHAR(255) NULL DEFAULT NULL,
  `mq_message` LONGTEXT NULL DEFAULT NULL,
  `aes_file_id` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `FK_AES_FILE_STATUS_EDI_ID` (`aes_file_id` ASC),
  CONSTRAINT `FK_AES_FILE_STATUS_EDI_ID`
    FOREIGN KEY (`aes_file_id`)
    REFERENCES `efsdev`.`efs_aes_file` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_aes_file_status_history`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_aes_file_status_history` (
  `id` BIGINT(20) NOT NULL,
  `acr_date` DATETIME(6) NULL DEFAULT NULL,
  `acr_time` VARCHAR(255) NULL DEFAULT NULL,
  `batch_no` VARCHAR(255) NULL DEFAULT NULL,
  `bound_type` VARCHAR(255) NULL DEFAULT NULL,
  `branch_code` VARCHAR(255) NULL DEFAULT NULL,
  `company_code` VARCHAR(255) NULL DEFAULT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `customs_response_date` VARCHAR(255) NULL DEFAULT NULL,
  `disposition_code` VARCHAR(255) NULL DEFAULT NULL,
  `house_no` VARCHAR(255) NULL DEFAULT NULL,
  `itn_no` VARCHAR(255) NULL DEFAULT NULL,
  `job_no` VARCHAR(255) NULL DEFAULT NULL,
  `job_uid` VARCHAR(255) NULL DEFAULT NULL,
  `location_acr_date` DATETIME(6) NULL DEFAULT NULL,
  `location_acr_time` VARCHAR(255) NULL DEFAULT NULL,
  `location_code` VARCHAR(255) NULL DEFAULT NULL,
  `message` LONGTEXT NULL DEFAULT NULL,
  `message_type` VARCHAR(255) NULL DEFAULT NULL,
  `remarks` VARCHAR(255) NULL DEFAULT NULL,
  `response_code` VARCHAR(255) NULL DEFAULT NULL,
  `response_description` VARCHAR(255) NULL DEFAULT NULL,
  `segment_code` VARCHAR(255) NULL DEFAULT NULL,
  `severity_level` VARCHAR(255) NULL DEFAULT NULL,
  `sl_no` VARCHAR(255) NULL DEFAULT NULL,
  `status` VARCHAR(255) NULL DEFAULT NULL,
  `subjob_no` VARCHAR(255) NULL DEFAULT NULL,
  `subjob_uid` VARCHAR(255) NULL DEFAULT NULL,
  `user_id` VARCHAR(255) NULL DEFAULT NULL,
  `aes_file_id` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `FK_AES_FILE_HIS_STATUS_EDI_ID` (`aes_file_id` ASC),
  CONSTRAINT `FK_AES_FILE_HIS_STATUS_EDI_ID`
    FOREIGN KEY (`aes_file_id`)
    REFERENCES `efsdev`.`efs_aes_file` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_aes_filer`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_aes_filer` (
  `id` BIGINT(20) NOT NULL,
  `aes_filer_code` VARCHAR(30) NOT NULL,
  `company_code` VARCHAR(30) NULL DEFAULT NULL,
  `country_code` VARCHAR(30) NULL DEFAULT NULL,
  `location_code` VARCHAR(30) NULL DEFAULT NULL,
  `schema_name` VARCHAR(100) NULL DEFAULT NULL,
  `status` VARCHAR(10) NOT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `transmitter_code` VARCHAR(30) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  `company_id` BIGINT(20) NULL DEFAULT NULL,
  `country_id` BIGINT(20) NULL DEFAULT NULL,
  `location_id` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `FK_AES_FILER_COMPANYID` (`company_id` ASC),
  INDEX `FK_AES_FILER_COUNTRYID` (`country_id` ASC),
  INDEX `FK_AES_FILER_LOCATIONID` (`location_id` ASC),
  CONSTRAINT `FK_AES_FILER_COMPANYID`
    FOREIGN KEY (`company_id`)
    REFERENCES `efsdev`.`efs_company_master` (`id`),
  CONSTRAINT `FK_AES_FILER_COUNTRYID`
    FOREIGN KEY (`country_id`)
    REFERENCES `efsdev`.`efs_country_master` (`id`),
  CONSTRAINT `FK_AES_FILER_LOCATIONID`
    FOREIGN KEY (`location_id`)
    REFERENCES `efsdev`.`efs_location_master` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_aes_uszip_code_master`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_aes_uszip_code_master` (
  `id` BIGINT(20) NOT NULL,
  `code` VARCHAR(100) NOT NULL,
  `state_code` VARCHAR(100) NOT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `UK_AES_USZP_CODE_MASTER_CODE` (`code` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_port_group_master`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_port_group_master` (
  `id` BIGINT(20) NOT NULL,
  `country_code` VARCHAR(10) NULL DEFAULT NULL,
  `port_group_code` VARCHAR(10) NOT NULL,
  `port_group_name` VARCHAR(100) NOT NULL,
  `status` VARCHAR(10) NOT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  `country_id` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `UK_PORTGROUP_PORTGROUPCODE` (`port_group_code` ASC),
  UNIQUE INDEX `UK_PORTGROUP_PORTGROUPNAME` (`port_group_name` ASC),
  INDEX `FK_PORTGROUP_COUNTRYID` (`country_id` ASC),
  CONSTRAINT `FK_PORTGROUP_COUNTRYID`
    FOREIGN KEY (`country_id`)
    REFERENCES `efsdev`.`efs_country_master` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_port_master`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_port_master` (
  `id` BIGINT(20) NOT NULL,
  `country_code` VARCHAR(10) NOT NULL,
  `port_code` VARCHAR(10) NOT NULL,
  `port_group_code` VARCHAR(10) NOT NULL,
  `port_name` VARCHAR(100) NOT NULL,
  `status` VARCHAR(10) NOT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `transport_mode` VARCHAR(10) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  `port_group_id` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `UK_PORT_PORTCODE` (`port_code` ASC),
  UNIQUE INDEX `UK_PORT_PORTNAME` (`port_name` ASC, `transport_mode` ASC),
  INDEX `FK_PORT_PORTGROUPID` (`port_group_id` ASC),
  CONSTRAINT `FK_PORT_PORTGROUPID`
    FOREIGN KEY (`port_group_id`)
    REFERENCES `efsdev`.`efs_port_group_master` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_service_type_master`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_service_type_master` (
  `id` BIGINT(20) NOT NULL,
  `service_type` VARCHAR(30) NULL DEFAULT NULL,
  `service_type_code` VARCHAR(20) NOT NULL,
  `service_type_name` VARCHAR(100) NOT NULL,
  `status` VARCHAR(10) NOT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `transport_mode` VARCHAR(10) NULL DEFAULT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `UK_SERVICETYPE_SERVICECODE` (`service_type_code` ASC),
  UNIQUE INDEX `UK_SERVICETYPE_SERVICENAME` (`service_type_name` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_service_master`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_service_master` (
  `id` BIGINT(20) NOT NULL,
  `cost_center_code` VARCHAR(30) NULL DEFAULT NULL,
  `full_groupage` VARCHAR(10) NULL DEFAULT NULL,
  `import_export` VARCHAR(10) NOT NULL,
  `service_code` VARCHAR(10) NOT NULL,
  `service_name` VARCHAR(100) NOT NULL,
  `status` VARCHAR(10) NOT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `transport_mode` VARCHAR(10) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  `cost_center_id` BIGINT(20) NULL DEFAULT NULL,
  `service_type_master` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `UK_SERVICE_SERVICECODE` (`service_code` ASC),
  UNIQUE INDEX `UK_SERVICE_SERVICENAME` (`service_name` ASC),
  INDEX `FK_SERVICE_COSTCENTERID` (`cost_center_id` ASC),
  INDEX `FK_SERVICE_SERVICETYPEID` (`service_type_master` ASC),
  CONSTRAINT `FK_SERVICE_COSTCENTERID`
    FOREIGN KEY (`cost_center_id`)
    REFERENCES `efsdev`.`efs_cost_center_master` (`id`),
  CONSTRAINT `FK_SERVICE_SERVICETYPEID`
    FOREIGN KEY (`service_type_master`)
    REFERENCES `efsdev`.`efs_service_type_master` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_agent_port_master`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_agent_port_master` (
  `id` BIGINT(20) NOT NULL,
  `service_code` VARCHAR(255) NOT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  `party_master_id` BIGINT(20) NOT NULL,
  `country_id` BIGINT(20) NULL DEFAULT NULL,
  `location_id` BIGINT(20) NULL DEFAULT NULL,
  `port_master_id` BIGINT(20) NOT NULL,
  `service_master_id` BIGINT(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `UK_SVC_PORT_AGENTID` (`service_master_id` ASC, `party_master_id` ASC, `port_master_id` ASC),
  INDEX `FK_AGETNPORT_AGENTID` (`party_master_id` ASC),
  INDEX `FK_AGETNPORT_COUNTRYID` (`country_id` ASC),
  INDEX `FK_AGETNPORT_LOCID` (`location_id` ASC),
  INDEX `FK_AGETNPORT_PORTID` (`port_master_id` ASC),
  CONSTRAINT `FK_AGETNPORT_AGENTID`
    FOREIGN KEY (`party_master_id`)
    REFERENCES `efsdev`.`efs_party_master` (`id`),
  CONSTRAINT `FK_AGETNPORT_COUNTRYID`
    FOREIGN KEY (`country_id`)
    REFERENCES `efsdev`.`efs_country_master` (`id`),
  CONSTRAINT `FK_AGETNPORT_LOCID`
    FOREIGN KEY (`location_id`)
    REFERENCES `efsdev`.`efs_location_master` (`id`),
  CONSTRAINT `FK_AGETNPORT_PORTID`
    FOREIGN KEY (`port_master_id`)
    REFERENCES `efsdev`.`efs_port_master` (`id`),
  CONSTRAINT `FK_AGETNPORT_SERVICEID`
    FOREIGN KEY (`service_master_id`)
    REFERENCES `efsdev`.`efs_service_master` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_charge_master`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_charge_master` (
  `id` BIGINT(20) NOT NULL,
  `calculation_type` VARCHAR(20) NOT NULL,
  `charge_code` VARCHAR(10) NOT NULL,
  `charge_name` VARCHAR(100) NOT NULL,
  `charge_type` VARCHAR(20) NOT NULL,
  `status` VARCHAR(10) NOT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `UK_CHARGE_CHARGECODE` (`charge_code` ASC),
  UNIQUE INDEX `UK_CHARGE_CHARGENAME` (`charge_name` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_employee_personal_detail`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_employee_personal_detail` (
  `id` BIGINT(20) NOT NULL,
  `airport_name` VARCHAR(100) NULL DEFAULT NULL,
  `date_of_birth` DATETIME(6) NULL DEFAULT NULL,
  `emergency_address` VARCHAR(4000) NULL DEFAULT NULL,
  `emergency_phone_number` VARCHAR(100) NULL DEFAULT NULL,
  `fax` VARCHAR(100) NULL DEFAULT NULL,
  `gender` VARCHAR(10) NULL DEFAULT NULL,
  `home_town` VARCHAR(100) NULL DEFAULT NULL,
  `marital_status` VARCHAR(10) NULL DEFAULT NULL,
  `mobile_number` VARCHAR(100) NULL DEFAULT NULL,
  `permanent_address` VARCHAR(4000) NULL DEFAULT NULL,
  `permanent_phone_number` VARCHAR(100) NULL DEFAULT NULL,
  `personal_email` VARCHAR(500) NULL DEFAULT NULL,
  `phone_number` VARCHAR(100) NULL DEFAULT NULL,
  `religion` VARCHAR(100) NULL DEFAULT NULL,
  `temporary_address` VARCHAR(4000) NULL DEFAULT NULL,
  `temporary_phone_number` VARCHAR(100) NULL DEFAULT NULL,
  `wedding_anniversary_date` DATETIME(6) NULL DEFAULT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_department_master`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_department_master` (
  `id` BIGINT(20) NOT NULL,
  `department_code` VARCHAR(100) NOT NULL,
  `department_name` VARCHAR(100) NOT NULL,
  `status` VARCHAR(10) NOT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  `department_head` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `UK_DEPARTMENT_DEPTCODE` (`department_code` ASC),
  UNIQUE INDEX `UK_DEPARTMENT_DEPTNAME` (`department_name` ASC),
  INDEX `FK_DEPARTMENT_HEAD` (`department_head` ASC),
  CONSTRAINT `FK_DEPARTMENT_HEAD`
    FOREIGN KEY (`department_head`)
    REFERENCES `efsdev`.`efs_employee_master` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_designation_master`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_designation_master` (
  `id` BIGINT(20) NOT NULL,
  `designation_code` VARCHAR(100) NOT NULL,
  `designation_name` VARCHAR(100) NOT NULL,
  `manager` VARCHAR(10) NULL DEFAULT NULL,
  `profile` LONGTEXT NULL DEFAULT NULL,
  `status` VARCHAR(10) NOT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `UK_DESIGNATION_DESIG_CODE` (`designation_code` ASC),
  UNIQUE INDEX `UK_DESIGNATION_DESIG_NAME` (`designation_name` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_employee_profess_detail`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_employee_profess_detail` (
  `id` BIGINT(20) NOT NULL,
  `date_of_joining` DATETIME(6) NULL DEFAULT NULL,
  `designation_qualifier` VARCHAR(100) NULL DEFAULT NULL,
  `designation_start_date` DATETIME(6) NULL DEFAULT NULL,
  `employment_end_date` DATETIME(6) NULL DEFAULT NULL,
  `job_description` VARCHAR(4000) NULL DEFAULT NULL,
  `national_id` VARCHAR(100) NULL DEFAULT NULL,
  `payroll_emp_id` VARCHAR(100) NULL DEFAULT NULL,
  `probation_period` BIGINT(20) NULL DEFAULT NULL,
  `joining_status` VARCHAR(15) NULL DEFAULT NULL,
  `transfer_date` DATETIME(6) NULL DEFAULT NULL,
  `department_head_id` BIGINT(20) NULL DEFAULT NULL,
  `department_id` BIGINT(20) NULL DEFAULT NULL,
  `designation_id` BIGINT(20) NULL DEFAULT NULL,
  `grade_id` BIGINT(20) NULL DEFAULT NULL,
  `reporting_to_id` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `FK_EMPLOYEE_DEPT_HEAD_ID` (`department_head_id` ASC),
  INDEX `FK_EMPLOYEE_DEPTID` (`department_id` ASC),
  INDEX `FK_EMPLOYEE_DESIGNATION_ID` (`designation_id` ASC),
  INDEX `FK_GRADE_ID` (`grade_id` ASC),
  INDEX `FK_EMPLOYEE_REPORTING_TO_ID` (`reporting_to_id` ASC),
  CONSTRAINT `FK_EMPLOYEE_DEPTID`
    FOREIGN KEY (`department_id`)
    REFERENCES `efsdev`.`efs_department_master` (`id`),
  CONSTRAINT `FK_EMPLOYEE_DEPT_HEAD_ID`
    FOREIGN KEY (`department_head_id`)
    REFERENCES `efsdev`.`efs_employee_master` (`id`),
  CONSTRAINT `FK_EMPLOYEE_DESIGNATION_ID`
    FOREIGN KEY (`designation_id`)
    REFERENCES `efsdev`.`efs_designation_master` (`id`),
  CONSTRAINT `FK_EMPLOYEE_REPORTING_TO_ID`
    FOREIGN KEY (`reporting_to_id`)
    REFERENCES `efsdev`.`efs_employee_master` (`id`),
  CONSTRAINT `FK_GRADE_ID`
    FOREIGN KEY (`grade_id`)
    REFERENCES `efsdev`.`efs_grade_master` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_employee_master`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_employee_master` (
  `id` BIGINT(20) NOT NULL,
  `employee_alias_name` VARCHAR(100) NULL DEFAULT NULL,
  `cc_email` VARCHAR(500) NULL DEFAULT NULL,
  `email` VARCHAR(500) NOT NULL,
  `employee_code` VARCHAR(255) NOT NULL,
  `employee_name` VARCHAR(100) NOT NULL,
  `employee_phone_no` VARCHAR(255) NULL DEFAULT NULL,
  `employment_status` VARCHAR(15) NOT NULL,
  `first_name` VARCHAR(100) NULL DEFAULT NULL,
  `image` LONGBLOB NULL DEFAULT NULL,
  `is_salesman` VARCHAR(10) NOT NULL,
  `last_name` VARCHAR(100) NULL DEFAULT NULL,
  `middle_name` VARCHAR(100) NULL DEFAULT NULL,
  `reason_for_resignation` VARCHAR(100) NULL DEFAULT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  `company_id` BIGINT(20) NULL DEFAULT NULL,
  `country_id` BIGINT(20) NULL DEFAULT NULL,
  `personal_detail_id` BIGINT(20) NULL DEFAULT NULL,
  `professional_detail_id` BIGINT(20) NULL DEFAULT NULL,
  `location_id` BIGINT(20) NULL DEFAULT NULL,
  `nominate_employee` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `UK_EMPLOYEE_EMPLOYEECODE` (`employee_code` ASC),
  INDEX `FK_EMPLOYEE_COMPANY_ID` (`company_id` ASC),
  INDEX `FK_EMPLOYEE_COUNTRYID` (`country_id` ASC),
  INDEX `FK_EMPLOYEE_PERSONAL_ID` (`personal_detail_id` ASC),
  INDEX `FK_EMPLOYEE_PROFESSIONAL_ID` (`professional_detail_id` ASC),
  INDEX `FK_EMPLOYEE_LOCATION_ID` (`location_id` ASC),
  INDEX `FK_EMPLOYEE_NOMINATE` (`nominate_employee` ASC),
  CONSTRAINT `FK_EMPLOYEE_COMPANY_ID`
    FOREIGN KEY (`company_id`)
    REFERENCES `efsdev`.`efs_company_master` (`id`),
  CONSTRAINT `FK_EMPLOYEE_COUNTRYID`
    FOREIGN KEY (`country_id`)
    REFERENCES `efsdev`.`efs_country_master` (`id`),
  CONSTRAINT `FK_EMPLOYEE_LOCATION_ID`
    FOREIGN KEY (`location_id`)
    REFERENCES `efsdev`.`efs_location_master` (`id`),
  CONSTRAINT `FK_EMPLOYEE_NOMINATE`
    FOREIGN KEY (`nominate_employee`)
    REFERENCES `efsdev`.`efs_employee_master` (`id`),
  CONSTRAINT `FK_EMPLOYEE_PERSONAL_ID`
    FOREIGN KEY (`personal_detail_id`)
    REFERENCES `efsdev`.`efs_employee_personal_detail` (`id`),
  CONSTRAINT `FK_EMPLOYEE_PROFESSIONAL_ID`
    FOREIGN KEY (`professional_detail_id`)
    REFERENCES `efsdev`.`efs_employee_profess_detail` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_enquiry_customer`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_enquiry_customer` (
  `id` BIGINT(20) NOT NULL,
  `address_line_1` VARCHAR(100) NULL DEFAULT NULL,
  `address_line_2` VARCHAR(100) NULL DEFAULT NULL,
  `address_line_3` VARCHAR(100) NULL DEFAULT NULL,
  `city_code` VARCHAR(100) NULL DEFAULT NULL,
  `contact_person` VARCHAR(255) NULL DEFAULT NULL,
  `country_code` VARCHAR(10) NULL DEFAULT NULL,
  `email` VARCHAR(500) NULL DEFAULT NULL,
  `status` VARCHAR(20) NOT NULL,
  `fax` VARCHAR(100) NULL DEFAULT NULL,
  `first_name` VARCHAR(255) NULL DEFAULT NULL,
  `last_name` VARCHAR(255) NULL DEFAULT NULL,
  `mobile_no` VARCHAR(100) NULL DEFAULT NULL,
  `name` VARCHAR(100) NULL DEFAULT NULL,
  `phone` VARCHAR(100) NULL DEFAULT NULL,
  `po_box` VARCHAR(20) NULL DEFAULT NULL,
  `state_province_code` VARCHAR(100) NULL DEFAULT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  `zip_code` VARCHAR(10) NULL DEFAULT NULL,
  `city_id` BIGINT(20) NULL DEFAULT NULL,
  `country_id` BIGINT(20) NULL DEFAULT NULL,
  `state_province` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `FK_ENQ_CUST_CITYID` (`city_id` ASC),
  INDEX `FK_ENQ_CUST_COUNTRYID` (`country_id` ASC),
  INDEX `FK_ENQ_CUST_STATEID` (`state_province` ASC),
  CONSTRAINT `FK_ENQ_CUST_CITYID`
    FOREIGN KEY (`city_id`)
    REFERENCES `efsdev`.`efs_city_master` (`id`),
  CONSTRAINT `FK_ENQ_CUST_COUNTRYID`
    FOREIGN KEY (`country_id`)
    REFERENCES `efsdev`.`efs_country_master` (`id`),
  CONSTRAINT `FK_ENQ_CUST_STATEID`
    FOREIGN KEY (`state_province`)
    REFERENCES `efsdev`.`efs_state_province_master` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_enquiry_log`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_enquiry_log` (
  `id` BIGINT(20) NOT NULL,
  `company_code` VARCHAR(30) NULL DEFAULT NULL,
  `country_code` VARCHAR(30) NULL DEFAULT NULL,
  `created_by` VARCHAR(100) NULL DEFAULT NULL,
  `created_on` DATETIME(6) NULL DEFAULT NULL,
  `enquiry_no` VARCHAR(100) NULL DEFAULT NULL,
  `location_code` VARCHAR(30) NULL DEFAULT NULL,
  `logged_by_code` VARCHAR(100) NULL DEFAULT NULL,
  `logged_on` DATETIME(6) NULL DEFAULT NULL,
  `party_code` VARCHAR(100) NULL DEFAULT NULL,
  `quotation_no` VARCHAR(100) NULL DEFAULT NULL,
  `quote_by` DATETIME(6) NULL DEFAULT NULL,
  `received_on` DATETIME(6) NULL DEFAULT NULL,
  `sales_co_ordinator_code` VARCHAR(100) NULL DEFAULT NULL,
  `salesman_code` VARCHAR(100) NULL DEFAULT NULL,
  `status` VARCHAR(10) NOT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  `company_id` BIGINT(20) NULL DEFAULT NULL,
  `country_id` BIGINT(20) NULL DEFAULT NULL,
  `enquiry_customer_id` BIGINT(20) NULL DEFAULT NULL,
  `location_id` BIGINT(20) NULL DEFAULT NULL,
  `logged_by` BIGINT(20) NULL DEFAULT NULL,
  `party_id` BIGINT(20) NULL DEFAULT NULL,
  `sales_co_ordinator_id` BIGINT(20) NULL DEFAULT NULL,
  `salesman_id` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `UK_ENQUIRY_NO` (`enquiry_no` ASC),
  INDEX `FK_ENQUIRYLOG_COMPANYID` (`company_id` ASC),
  INDEX `FK_ENQUIRYLOG_COUNTRYID` (`country_id` ASC),
  INDEX `FK_ENQUIRYLOG_CUSTID` (`enquiry_customer_id` ASC),
  INDEX `FK_ENQUIRYLOG_LOCATIONID` (`location_id` ASC),
  INDEX `FK_ENQUIRYLOG_LOGGEDBY` (`logged_by` ASC),
  INDEX `FK_ENQUIRYLOG_PARTYID` (`party_id` ASC),
  INDEX `FK_ENQUIRYLOG_SALESCOORDID` (`sales_co_ordinator_id` ASC),
  INDEX `FK_ENQUIRYLOG_SALESMANID` (`salesman_id` ASC),
  CONSTRAINT `FK_ENQUIRYLOG_COMPANYID`
    FOREIGN KEY (`company_id`)
    REFERENCES `efsdev`.`efs_company_master` (`id`),
  CONSTRAINT `FK_ENQUIRYLOG_COUNTRYID`
    FOREIGN KEY (`country_id`)
    REFERENCES `efsdev`.`efs_country_master` (`id`),
  CONSTRAINT `FK_ENQUIRYLOG_CUSTID`
    FOREIGN KEY (`enquiry_customer_id`)
    REFERENCES `efsdev`.`efs_enquiry_customer` (`id`),
  CONSTRAINT `FK_ENQUIRYLOG_LOCATIONID`
    FOREIGN KEY (`location_id`)
    REFERENCES `efsdev`.`efs_location_master` (`id`),
  CONSTRAINT `FK_ENQUIRYLOG_LOGGEDBY`
    FOREIGN KEY (`logged_by`)
    REFERENCES `efsdev`.`efs_employee_master` (`id`),
  CONSTRAINT `FK_ENQUIRYLOG_PARTYID`
    FOREIGN KEY (`party_id`)
    REFERENCES `efsdev`.`efs_party_master` (`id`),
  CONSTRAINT `FK_ENQUIRYLOG_SALESCOORDID`
    FOREIGN KEY (`sales_co_ordinator_id`)
    REFERENCES `efsdev`.`efs_employee_master` (`id`),
  CONSTRAINT `FK_ENQUIRYLOG_SALESMANID`
    FOREIGN KEY (`salesman_id`)
    REFERENCES `efsdev`.`efs_employee_master` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_tos_master`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_tos_master` (
  `id` BIGINT(20) NOT NULL,
  `description` VARCHAR(4000) NULL DEFAULT NULL,
  `freight_ppcc` VARCHAR(10) NULL DEFAULT NULL,
  `status` VARCHAR(10) NOT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `tos_code` VARCHAR(10) NOT NULL,
  `tos_name` VARCHAR(100) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `UK_TOSMASTER_TOSCODE` (`tos_code` ASC),
  UNIQUE INDEX `UK_TOSMASTER_TOSNAME` (`tos_name` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_enquiry_detail`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_enquiry_detail` (
  `id` BIGINT(20) NOT NULL,
  `clearance` VARCHAR(255) NULL DEFAULT NULL,
  `commodity_code` VARCHAR(100) NULL DEFAULT NULL,
  `dimension_gross_weight` DOUBLE NULL DEFAULT NULL,
  `dimension_gross_weight_in_kg` DOUBLE NULL DEFAULT NULL,
  `dimension_total_pieces` DOUBLE NULL DEFAULT NULL,
  `dimension_unit` VARCHAR(50) NOT NULL,
  `dimension_unit_value` DOUBLE NULL DEFAULT NULL,
  `dimension_vol_weight` DOUBLE NULL DEFAULT NULL,
  `enquiry_no` VARCHAR(100) NULL DEFAULT NULL,
  `from_port_code` VARCHAR(100) NULL DEFAULT NULL,
  `gross_weight` DOUBLE NULL DEFAULT NULL,
  `hazardous` VARCHAR(255) NULL DEFAULT NULL,
  `import_export` VARCHAR(255) NULL DEFAULT NULL,
  `notes` VARCHAR(500) NULL DEFAULT NULL,
  `over_dimension` VARCHAR(5) NULL DEFAULT NULL,
  `process_instance_id` VARCHAR(255) NULL DEFAULT NULL,
  `service` VARCHAR(255) NULL DEFAULT NULL,
  `service_code` VARCHAR(100) NULL DEFAULT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `task_id` VARCHAR(255) NULL DEFAULT NULL,
  `to_port_code` VARCHAR(100) NULL DEFAULT NULL,
  `tos_code` VARCHAR(100) NULL DEFAULT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  `vol_weight` DOUBLE NULL DEFAULT NULL,
  `workflow_completed` VARCHAR(5) NULL DEFAULT NULL,
  `tos_id` BIGINT(20) NULL DEFAULT NULL,
  `commodity_id` BIGINT(20) NULL DEFAULT NULL,
  `delivery_address_id` BIGINT(20) NULL DEFAULT NULL,
  `enquiry_log_id` BIGINT(20) NULL DEFAULT NULL,
  `from_port_id` BIGINT(20) NULL DEFAULT NULL,
  `pickup_address_id` BIGINT(20) NULL DEFAULT NULL,
  `service_id` BIGINT(20) NULL DEFAULT NULL,
  `to_port_id` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `UK_ENQDET_SERVICE` (`enquiry_log_id` ASC, `service_id` ASC, `from_port_id` ASC, `to_port_id` ASC),
  INDEX `FK_ENQUIRYDETAIL_TOSID` (`tos_id` ASC),
  INDEX `FK_ENQDET_COMMODITYID` (`commodity_id` ASC),
  INDEX `FK_ENQUIRYDETAIL_DELIVERY` (`delivery_address_id` ASC),
  INDEX `FK_ENQDET_FROMPORTID` (`from_port_id` ASC),
  INDEX `FK_ENQUIRYDETAIL_PICKUP` (`pickup_address_id` ASC),
  INDEX `FK_ENQDET_SERVICEID` (`service_id` ASC),
  INDEX `FK_ENQUIRYDETAIL_TOPORTID` (`to_port_id` ASC),
  CONSTRAINT `FK_ENQDET_COMMODITYID`
    FOREIGN KEY (`commodity_id`)
    REFERENCES `efsdev`.`efs_hs_code_master` (`id`),
  CONSTRAINT `FK_ENQDET_ENQUIRYLOGID`
    FOREIGN KEY (`enquiry_log_id`)
    REFERENCES `efsdev`.`efs_enquiry_log` (`id`),
  CONSTRAINT `FK_ENQDET_FROMPORTID`
    FOREIGN KEY (`from_port_id`)
    REFERENCES `efsdev`.`efs_port_master` (`id`),
  CONSTRAINT `FK_ENQDET_SERVICEID`
    FOREIGN KEY (`service_id`)
    REFERENCES `efsdev`.`efs_service_master` (`id`),
  CONSTRAINT `FK_ENQUIRYDETAIL_DELIVERY`
    FOREIGN KEY (`delivery_address_id`)
    REFERENCES `efsdev`.`efs_address_master` (`id`),
  CONSTRAINT `FK_ENQUIRYDETAIL_PICKUP`
    FOREIGN KEY (`pickup_address_id`)
    REFERENCES `efsdev`.`efs_address_master` (`id`),
  CONSTRAINT `FK_ENQUIRYDETAIL_TOPORTID`
    FOREIGN KEY (`to_port_id`)
    REFERENCES `efsdev`.`efs_port_master` (`id`),
  CONSTRAINT `FK_ENQUIRYDETAIL_TOSID`
    FOREIGN KEY (`tos_id`)
    REFERENCES `efsdev`.`efs_tos_master` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_agent_rate_request`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_agent_rate_request` (
  `id` BIGINT(20) NOT NULL,
  `contact_person` VARCHAR(100) NULL DEFAULT NULL,
  `customer_ip` VARCHAR(100) NULL DEFAULT NULL,
  `email_id` VARCHAR(500) NULL DEFAULT NULL,
  `is_mail_sent` VARCHAR(5) NULL DEFAULT NULL,
  `rate_accepted` VARCHAR(10) NOT NULL,
  `rate_accepted_date` DATETIME(6) NULL DEFAULT NULL,
  `rate_accepted_employee_code` VARCHAR(100) NULL DEFAULT NULL,
  `request_date` DATETIME(6) NULL DEFAULT NULL,
  `response_date` DATETIME(6) NULL DEFAULT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `agent_code` VARCHAR(100) NULL DEFAULT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  `enquiry_detail_id` BIGINT(20) NULL DEFAULT NULL,
  `rate_accepted_employee_id` BIGINT(20) NULL DEFAULT NULL,
  `agent_id` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `FK_RATEREQ_ENQDETID` (`enquiry_detail_id` ASC),
  INDEX `FK_RATEREQ_EMPID` (`rate_accepted_employee_id` ASC),
  INDEX `FK_RATEREQ_VENDOR` (`agent_id` ASC),
  CONSTRAINT `FK_RATEREQ_EMPID`
    FOREIGN KEY (`rate_accepted_employee_id`)
    REFERENCES `efsdev`.`efs_employee_master` (`id`),
  CONSTRAINT `FK_RATEREQ_ENQDETID`
    FOREIGN KEY (`enquiry_detail_id`)
    REFERENCES `efsdev`.`efs_enquiry_detail` (`id`),
  CONSTRAINT `FK_RATEREQ_VENDOR`
    FOREIGN KEY (`agent_id`)
    REFERENCES `efsdev`.`efs_party_master` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_agent_rate_req_chr`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_agent_rate_req_chr` (
  `id` BIGINT(20) NOT NULL,
  `buy_rate` DOUBLE NULL DEFAULT NULL,
  `buy_rate_min_cost` DOUBLE NULL DEFAULT NULL,
  `charge_code` VARCHAR(100) NULL DEFAULT NULL,
  `charge_name` VARCHAR(100) NULL DEFAULT NULL,
  `currency_code` VARCHAR(100) NULL DEFAULT NULL,
  `notes` VARCHAR(500) NULL DEFAULT NULL,
  `shipment_movement` VARCHAR(20) NULL DEFAULT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(255) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(255) NOT NULL,
  `unit_code` VARCHAR(100) NULL DEFAULT NULL,
  `unit_slap` VARCHAR(10) NULL DEFAULT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  `charge_id` BIGINT(20) NOT NULL,
  `currency_id` BIGINT(20) NOT NULL,
  `rate_request_id` BIGINT(20) NOT NULL,
  `unit_id` BIGINT(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `UK_RATECHARGE` (`rate_request_id` ASC, `charge_id` ASC),
  INDEX `FK_RATEREQCHA_CHAID` (`charge_id` ASC),
  INDEX `FK_RATEREQCHA_CURRID` (`currency_id` ASC),
  INDEX `FK_RATEREQCHA_UNITID` (`unit_id` ASC),
  CONSTRAINT `FK_RATEREQCHA_CHAID`
    FOREIGN KEY (`charge_id`)
    REFERENCES `efsdev`.`efs_charge_master` (`id`),
  CONSTRAINT `FK_RATEREQCHA_CURRID`
    FOREIGN KEY (`currency_id`)
    REFERENCES `efsdev`.`efs_currency_master` (`id`),
  CONSTRAINT `FK_RATEREQCHA_RATRQID`
    FOREIGN KEY (`rate_request_id`)
    REFERENCES `efsdev`.`efs_agent_rate_request` (`id`),
  CONSTRAINT `FK_RATEREQCHA_UNITID`
    FOREIGN KEY (`unit_id`)
    REFERENCES `efsdev`.`efs_unit_master` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_airline_edi`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_airline_edi` (
  `id` BIGINT(20) NOT NULL,
  `agent_code` VARCHAR(100) NULL DEFAULT NULL,
  `agent_iata_code` VARCHAR(30) NULL DEFAULT NULL,
  `awb_date` DATETIME(6) NULL DEFAULT NULL,
  `carrier_code` VARCHAR(100) NULL DEFAULT NULL,
  `chargeble_weight` DOUBLE NULL DEFAULT NULL,
  `commodity_code` VARCHAR(100) NULL DEFAULT NULL,
  `commodity_description` VARCHAR(4000) NULL DEFAULT NULL,
  `company_code` VARCHAR(30) NULL DEFAULT NULL,
  `consignee_code` VARCHAR(100) NULL DEFAULT NULL,
  `country_code` VARCHAR(30) NULL DEFAULT NULL,
  `currency_code` VARCHAR(100) NULL DEFAULT NULL,
  `dest_code` VARCHAR(100) NULL DEFAULT NULL,
  `dim_gross_weight` DOUBLE NULL DEFAULT NULL,
  `dim_gross_weight_in_pound` DOUBLE NULL DEFAULT NULL,
  `dim_no_of_pieces` BIGINT(20) NULL DEFAULT NULL,
  `dim_volume_weight` DOUBLE NULL DEFAULT NULL,
  `dim_volume_weight_in_pound` DOUBLE NULL DEFAULT NULL,
  `dimension_unit` VARCHAR(50) NOT NULL,
  `dimension_unit_value` DOUBLE NULL DEFAULT NULL,
  `direct_sipment` VARCHAR(10) NOT NULL,
  `eawb` VARCHAR(10) NOT NULL,
  `edi_generate_date` DATETIME(6) NULL DEFAULT NULL,
  `flight_no` VARCHAR(30) NULL DEFAULT NULL,
  `gross_weight` DOUBLE NULL DEFAULT NULL,
  `handling_information` VARCHAR(100) NULL DEFAULT NULL,
  `location_code` VARCHAR(30) NULL DEFAULT NULL,
  `master_date` DATETIME(6) NULL DEFAULT NULL,
  `master_shipment_uid` VARCHAR(30) NULL DEFAULT NULL,
  `mawb_no` VARCHAR(30) NULL DEFAULT NULL,
  `no_of_pieces` BIGINT(20) NULL DEFAULT NULL,
  `origin_code` VARCHAR(100) NULL DEFAULT NULL,
  `paper` VARCHAR(10) NOT NULL,
  `pod_code` VARCHAR(100) NULL DEFAULT NULL,
  `pol_code` VARCHAR(100) NULL DEFAULT NULL,
  `freight_term` VARCHAR(10) NOT NULL,
  `rate_class` VARCHAR(2) NULL DEFAULT NULL,
  `service_uid` VARCHAR(30) NULL DEFAULT NULL,
  `shipper_code` VARCHAR(100) NULL DEFAULT NULL,
  `status` VARCHAR(30) NULL DEFAULT NULL,
  `status_messsage` VARCHAR(4000) NULL DEFAULT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `value_for_carriage` VARCHAR(10) NULL DEFAULT NULL,
  `value_for_custom` VARCHAR(10) NULL DEFAULT NULL,
  `value_for_insurance` VARCHAR(10) NULL DEFAULT NULL,
  `volume_weight` DOUBLE NULL DEFAULT NULL,
  `agent_id` BIGINT(20) NULL DEFAULT NULL,
  `agent_address_id` BIGINT(20) NULL DEFAULT NULL,
  `carrier_id` BIGINT(20) NULL DEFAULT NULL,
  `commodity_id` BIGINT(20) NULL DEFAULT NULL,
  `company_id` BIGINT(20) NULL DEFAULT NULL,
  `consignee_id` BIGINT(20) NULL DEFAULT NULL,
  `consignee_address_id` BIGINT(20) NULL DEFAULT NULL,
  `country_id` BIGINT(20) NULL DEFAULT NULL,
  `currency_id` BIGINT(20) NULL DEFAULT NULL,
  `dest_id` BIGINT(20) NULL DEFAULT NULL,
  `location_id` BIGINT(20) NULL DEFAULT NULL,
  `origin_id` BIGINT(20) NULL DEFAULT NULL,
  `pod_id` BIGINT(20) NULL DEFAULT NULL,
  `pol_id` BIGINT(20) NULL DEFAULT NULL,
  `shipper_id` BIGINT(20) NULL DEFAULT NULL,
  `shipper_address_id` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `FK_AIR_EDI_AGENT_ID` (`agent_id` ASC),
  INDEX `FK_AIR_EDI_AGENTADDID` (`agent_address_id` ASC),
  INDEX `FK_AIR_EDI_CARRIER_ID` (`carrier_id` ASC),
  INDEX `FK_AIR_EDI_COMMODITY_ID` (`commodity_id` ASC),
  INDEX `FK_AIR_EDI_COMPANYID` (`company_id` ASC),
  INDEX `FK_AIR_EDI_CONSIGNEE_ID` (`consignee_id` ASC),
  INDEX `FK_AIR_EDI_AGENTDESTADDID` (`consignee_address_id` ASC),
  INDEX `FK_AIR_EDI_COUNTRYID` (`country_id` ASC),
  INDEX `FK_AIR_EDI_CURRENCY_ID` (`currency_id` ASC),
  INDEX `FK_AIR_EDI_DEST_ID` (`dest_id` ASC),
  INDEX `FK_AIR_EDI_LOCATIONID` (`location_id` ASC),
  INDEX `FK_AIR_EDI_ORIGIN_ID` (`origin_id` ASC),
  INDEX `FK_AIR_EDI_POD_ID` (`pod_id` ASC),
  INDEX `FK_AIR_EDI_POL_ID` (`pol_id` ASC),
  INDEX `FK_AIR_EDI_SHIPPER_ID` (`shipper_id` ASC),
  INDEX `FK_AIR_EDI_SHIPPERADDID` (`shipper_address_id` ASC),
  CONSTRAINT `FK_AIR_EDI_AGENTADDID`
    FOREIGN KEY (`agent_address_id`)
    REFERENCES `efsdev`.`efs_address_master` (`id`),
  CONSTRAINT `FK_AIR_EDI_AGENTDESTADDID`
    FOREIGN KEY (`consignee_address_id`)
    REFERENCES `efsdev`.`efs_address_master` (`id`),
  CONSTRAINT `FK_AIR_EDI_AGENT_ID`
    FOREIGN KEY (`agent_id`)
    REFERENCES `efsdev`.`efs_party_master` (`id`),
  CONSTRAINT `FK_AIR_EDI_CARRIER_ID`
    FOREIGN KEY (`carrier_id`)
    REFERENCES `efsdev`.`efs_carrier_master` (`id`),
  CONSTRAINT `FK_AIR_EDI_COMMODITY_ID`
    FOREIGN KEY (`commodity_id`)
    REFERENCES `efsdev`.`efs_hs_code_master` (`id`),
  CONSTRAINT `FK_AIR_EDI_COMPANYID`
    FOREIGN KEY (`company_id`)
    REFERENCES `efsdev`.`efs_company_master` (`id`),
  CONSTRAINT `FK_AIR_EDI_CONSIGNEE_ID`
    FOREIGN KEY (`consignee_id`)
    REFERENCES `efsdev`.`efs_party_master` (`id`),
  CONSTRAINT `FK_AIR_EDI_COUNTRYID`
    FOREIGN KEY (`country_id`)
    REFERENCES `efsdev`.`efs_country_master` (`id`),
  CONSTRAINT `FK_AIR_EDI_CURRENCY_ID`
    FOREIGN KEY (`currency_id`)
    REFERENCES `efsdev`.`efs_currency_master` (`id`),
  CONSTRAINT `FK_AIR_EDI_DEST_ID`
    FOREIGN KEY (`dest_id`)
    REFERENCES `efsdev`.`efs_port_master` (`id`),
  CONSTRAINT `FK_AIR_EDI_LOCATIONID`
    FOREIGN KEY (`location_id`)
    REFERENCES `efsdev`.`efs_location_master` (`id`),
  CONSTRAINT `FK_AIR_EDI_ORIGIN_ID`
    FOREIGN KEY (`origin_id`)
    REFERENCES `efsdev`.`efs_port_master` (`id`),
  CONSTRAINT `FK_AIR_EDI_POD_ID`
    FOREIGN KEY (`pod_id`)
    REFERENCES `efsdev`.`efs_port_master` (`id`),
  CONSTRAINT `FK_AIR_EDI_POL_ID`
    FOREIGN KEY (`pol_id`)
    REFERENCES `efsdev`.`efs_port_master` (`id`),
  CONSTRAINT `FK_AIR_EDI_SHIPPERADDID`
    FOREIGN KEY (`shipper_address_id`)
    REFERENCES `efsdev`.`efs_address_master` (`id`),
  CONSTRAINT `FK_AIR_EDI_SHIPPER_ID`
    FOREIGN KEY (`shipper_id`)
    REFERENCES `efsdev`.`efs_party_master` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_airline_edi_connection`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_airline_edi_connection` (
  `id` BIGINT(20) NOT NULL,
  `carrier_code` VARCHAR(100) NULL DEFAULT NULL,
  `company_code` VARCHAR(30) NULL DEFAULT NULL,
  `country_code` VARCHAR(30) NULL DEFAULT NULL,
  `eta` DATETIME(6) NULL DEFAULT NULL,
  `etd` DATETIME(6) NULL DEFAULT NULL,
  `flight_no` VARCHAR(30) NULL DEFAULT NULL,
  `location_code` VARCHAR(30) NULL DEFAULT NULL,
  `pod_code` VARCHAR(100) NULL DEFAULT NULL,
  `pol_code` VARCHAR(100) NULL DEFAULT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  `airline_edi_id` BIGINT(20) NULL DEFAULT NULL,
  `carrier_id` BIGINT(20) NULL DEFAULT NULL,
  `company_id` BIGINT(20) NULL DEFAULT NULL,
  `country_id` BIGINT(20) NULL DEFAULT NULL,
  `location_id` BIGINT(20) NULL DEFAULT NULL,
  `pod_id` BIGINT(20) NULL DEFAULT NULL,
  `pol_id` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `FK_EDI_CONNECT_EDI_ID` (`airline_edi_id` ASC),
  INDEX `FK_EDI_CONNECT_CARRIER_ID` (`carrier_id` ASC),
  INDEX `FK_AIR_EDI_CON_COMPANYID` (`company_id` ASC),
  INDEX `FK_AIR_EDI_CON_COUNTRYID` (`country_id` ASC),
  INDEX `FK_AIR_EDI_CON_LOCATIONID` (`location_id` ASC),
  INDEX `FK_EDI_CONNECT_POD_ID` (`pod_id` ASC),
  INDEX `FK_EDI_CONNECT_POL_ID` (`pol_id` ASC),
  CONSTRAINT `FK_AIR_EDI_CON_COMPANYID`
    FOREIGN KEY (`company_id`)
    REFERENCES `efsdev`.`efs_company_master` (`id`),
  CONSTRAINT `FK_AIR_EDI_CON_COUNTRYID`
    FOREIGN KEY (`country_id`)
    REFERENCES `efsdev`.`efs_country_master` (`id`),
  CONSTRAINT `FK_AIR_EDI_CON_LOCATIONID`
    FOREIGN KEY (`location_id`)
    REFERENCES `efsdev`.`efs_location_master` (`id`),
  CONSTRAINT `FK_EDI_CONNECT_CARRIER_ID`
    FOREIGN KEY (`carrier_id`)
    REFERENCES `efsdev`.`efs_carrier_master` (`id`),
  CONSTRAINT `FK_EDI_CONNECT_EDI_ID`
    FOREIGN KEY (`airline_edi_id`)
    REFERENCES `efsdev`.`efs_airline_edi` (`id`),
  CONSTRAINT `FK_EDI_CONNECT_POD_ID`
    FOREIGN KEY (`pod_id`)
    REFERENCES `efsdev`.`efs_port_master` (`id`),
  CONSTRAINT `FK_EDI_CONNECT_POL_ID`
    FOREIGN KEY (`pol_id`)
    REFERENCES `efsdev`.`efs_port_master` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_airline_edi_dimension`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_airline_edi_dimension` (
  `id` BIGINT(20) NOT NULL,
  `company_code` VARCHAR(30) NULL DEFAULT NULL,
  `country_code` VARCHAR(30) NULL DEFAULT NULL,
  `gross_weight` DOUBLE NULL DEFAULT NULL,
  `gross_weight_in_kg` DOUBLE NULL DEFAULT NULL,
  `height` INT(11) NULL DEFAULT NULL,
  `length` INT(11) NULL DEFAULT NULL,
  `location_code` VARCHAR(30) NULL DEFAULT NULL,
  `no_of_pieces` INT(11) NULL DEFAULT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  `volume_weight` DOUBLE NULL DEFAULT NULL,
  `width` INT(11) NULL DEFAULT NULL,
  `airline_edi_id` BIGINT(20) NULL DEFAULT NULL,
  `company_id` BIGINT(20) NULL DEFAULT NULL,
  `country_id` BIGINT(20) NULL DEFAULT NULL,
  `location_id` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `FK_EDI_DIMENSION_EDI_ID` (`airline_edi_id` ASC),
  INDEX `FK_AIR_EDI_DIM_COMPANYID` (`company_id` ASC),
  INDEX `FK_AIR_EDI_DIM_COUNTRYID` (`country_id` ASC),
  INDEX `FK_AIR_EDI_DIM_LOCATIONID` (`location_id` ASC),
  CONSTRAINT `FK_AIR_EDI_DIM_COMPANYID`
    FOREIGN KEY (`company_id`)
    REFERENCES `efsdev`.`efs_company_master` (`id`),
  CONSTRAINT `FK_AIR_EDI_DIM_COUNTRYID`
    FOREIGN KEY (`country_id`)
    REFERENCES `efsdev`.`efs_country_master` (`id`),
  CONSTRAINT `FK_AIR_EDI_DIM_LOCATIONID`
    FOREIGN KEY (`location_id`)
    REFERENCES `efsdev`.`efs_location_master` (`id`),
  CONSTRAINT `FK_EDI_DIMENSION_EDI_ID`
    FOREIGN KEY (`airline_edi_id`)
    REFERENCES `efsdev`.`efs_airline_edi` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_airline_edi_house`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_airline_edi_house` (
  `id` BIGINT(20) NOT NULL,
  `agent_code` VARCHAR(100) NULL DEFAULT NULL,
  `chargeble_weight` DOUBLE NULL DEFAULT NULL,
  `commodity_code` VARCHAR(100) NULL DEFAULT NULL,
  `commodity_desc` VARCHAR(4000) NULL DEFAULT NULL,
  `company_code` VARCHAR(30) NULL DEFAULT NULL,
  `consignee_code` VARCHAR(100) NULL DEFAULT NULL,
  `country_code` VARCHAR(30) NULL DEFAULT NULL,
  `destination_code` VARCHAR(100) NULL DEFAULT NULL,
  `document_no` VARCHAR(255) NULL DEFAULT NULL,
  `external_no_of_pieces` BIGINT(20) NULL DEFAULT NULL,
  `gross_weight` DOUBLE NULL DEFAULT NULL,
  `hawb_no` VARCHAR(255) NULL DEFAULT NULL,
  `location_code` VARCHAR(30) NULL DEFAULT NULL,
  `origin_code` VARCHAR(100) NULL DEFAULT NULL,
  `service_uid` VARCHAR(30) NULL DEFAULT NULL,
  `shipment_uid` VARCHAR(30) NULL DEFAULT NULL,
  `shipper_code` VARCHAR(100) NULL DEFAULT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `tos_code` VARCHAR(100) NULL DEFAULT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  `volume_weight` DOUBLE NULL DEFAULT NULL,
  `agent_id` BIGINT(20) NULL DEFAULT NULL,
  `agent_address_id` BIGINT(20) NULL DEFAULT NULL,
  `airline_edi_id` BIGINT(20) NULL DEFAULT NULL,
  `commodity_id` BIGINT(20) NULL DEFAULT NULL,
  `company_id` BIGINT(20) NULL DEFAULT NULL,
  `consignee_id` BIGINT(20) NULL DEFAULT NULL,
  `consignee_address_id` BIGINT(20) NULL DEFAULT NULL,
  `country_id` BIGINT(20) NULL DEFAULT NULL,
  `destination_id` BIGINT(20) NULL DEFAULT NULL,
  `location_id` BIGINT(20) NULL DEFAULT NULL,
  `origin_id` BIGINT(20) NULL DEFAULT NULL,
  `shipper_id` BIGINT(20) NULL DEFAULT NULL,
  `shipper_address_id` BIGINT(20) NULL DEFAULT NULL,
  `tos_id` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `FK_AIR_EDI_HAGENT_ID` (`agent_id` ASC),
  INDEX `FK_AIR_HEDI_AGENTADDID` (`agent_address_id` ASC),
  INDEX `FK_EDI_HOUSE_EDI_ID` (`airline_edi_id` ASC),
  INDEX `FK_EDI_HOUSE_COMMODITY_ID` (`commodity_id` ASC),
  INDEX `FK_AIR_EDI_HOUSE_COMPANYID` (`company_id` ASC),
  INDEX `FK_EDI_HOUSE_HCONSIGNEE_ID` (`consignee_id` ASC),
  INDEX `FK_AIR_HEDI_AGENTDESTADDID` (`consignee_address_id` ASC),
  INDEX `FK_AIR_EDI_HOUSE_COUNTRYID` (`country_id` ASC),
  INDEX `FK_EDI_HOUSE_DEST_ID` (`destination_id` ASC),
  INDEX `FK_AIR_EDI_HOUSE_LOCATIONID` (`location_id` ASC),
  INDEX `FK_EDI_HOUSE_ORIGIN_ID` (`origin_id` ASC),
  INDEX `FK_EDI_HOUSE_HSHIPPER_ID` (`shipper_id` ASC),
  INDEX `FK_AIR_HEDI_SHIPPERADDID` (`shipper_address_id` ASC),
  INDEX `FK_EDI_HOUSE_TOS_ID` (`tos_id` ASC),
  CONSTRAINT `FK_AIR_EDI_HAGENT_ID`
    FOREIGN KEY (`agent_id`)
    REFERENCES `efsdev`.`efs_party_master` (`id`),
  CONSTRAINT `FK_AIR_EDI_HOUSE_COMPANYID`
    FOREIGN KEY (`company_id`)
    REFERENCES `efsdev`.`efs_company_master` (`id`),
  CONSTRAINT `FK_AIR_EDI_HOUSE_COUNTRYID`
    FOREIGN KEY (`country_id`)
    REFERENCES `efsdev`.`efs_country_master` (`id`),
  CONSTRAINT `FK_AIR_EDI_HOUSE_LOCATIONID`
    FOREIGN KEY (`location_id`)
    REFERENCES `efsdev`.`efs_location_master` (`id`),
  CONSTRAINT `FK_AIR_HEDI_AGENTADDID`
    FOREIGN KEY (`agent_address_id`)
    REFERENCES `efsdev`.`efs_address_master` (`id`),
  CONSTRAINT `FK_AIR_HEDI_AGENTDESTADDID`
    FOREIGN KEY (`consignee_address_id`)
    REFERENCES `efsdev`.`efs_address_master` (`id`),
  CONSTRAINT `FK_AIR_HEDI_SHIPPERADDID`
    FOREIGN KEY (`shipper_address_id`)
    REFERENCES `efsdev`.`efs_address_master` (`id`),
  CONSTRAINT `FK_EDI_HOUSE_COMMODITY_ID`
    FOREIGN KEY (`commodity_id`)
    REFERENCES `efsdev`.`efs_hs_code_master` (`id`),
  CONSTRAINT `FK_EDI_HOUSE_DEST_ID`
    FOREIGN KEY (`destination_id`)
    REFERENCES `efsdev`.`efs_port_master` (`id`),
  CONSTRAINT `FK_EDI_HOUSE_EDI_ID`
    FOREIGN KEY (`airline_edi_id`)
    REFERENCES `efsdev`.`efs_airline_edi` (`id`),
  CONSTRAINT `FK_EDI_HOUSE_HCONSIGNEE_ID`
    FOREIGN KEY (`consignee_id`)
    REFERENCES `efsdev`.`efs_party_master` (`id`),
  CONSTRAINT `FK_EDI_HOUSE_HSHIPPER_ID`
    FOREIGN KEY (`shipper_id`)
    REFERENCES `efsdev`.`efs_party_master` (`id`),
  CONSTRAINT `FK_EDI_HOUSE_ORIGIN_ID`
    FOREIGN KEY (`origin_id`)
    REFERENCES `efsdev`.`efs_port_master` (`id`),
  CONSTRAINT `FK_EDI_HOUSE_TOS_ID`
    FOREIGN KEY (`tos_id`)
    REFERENCES `efsdev`.`efs_tos_master` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_airline_edi_status`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_airline_edi_status` (
  `id` BIGINT(20) NOT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `status` VARCHAR(30) NULL DEFAULT NULL,
  `status_messsage` VARCHAR(4000) NULL DEFAULT NULL,
  `airline_edi_id` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `FK_AIR_EDI_STATUS_EDI_ID` (`airline_edi_id` ASC),
  CONSTRAINT `FK_AIR_EDI_STATUS_EDI_ID`
    FOREIGN KEY (`airline_edi_id`)
    REFERENCES `efsdev`.`efs_airline_edi` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_charter_master`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_charter_master` (
  `id` BIGINT(20) NOT NULL,
  `charter_code` VARCHAR(10) NOT NULL,
  `charter_name` VARCHAR(100) NOT NULL,
  `status` VARCHAR(10) NOT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `UK_CHARTER_CHARTERCODE` (`charter_code` ASC),
  UNIQUE INDEX `UK_CHARTER_CHARTERNAME` (`charter_name` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_airline_prebooking`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_airline_prebooking` (
  `id` BIGINT(20) NOT NULL,
  `company_code` VARCHAR(30) NULL DEFAULT NULL,
  `country_code` VARCHAR(30) NULL DEFAULT NULL,
  `gross_weight` DOUBLE NULL DEFAULT NULL,
  `location_code` VARCHAR(30) NULL DEFAULT NULL,
  `no_of_mawb` INT(11) NULL DEFAULT NULL,
  `no_of_pieces` BIGINT(20) NULL DEFAULT NULL,
  `notes` VARCHAR(4000) NULL DEFAULT NULL,
  `prebooking_date` DATETIME(6) NULL DEFAULT NULL,
  `prebooking_no` VARCHAR(100) NOT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  `volume` DOUBLE NULL DEFAULT NULL,
  `volume_weight` DOUBLE NULL DEFAULT NULL,
  `carrier_id` BIGINT(20) NULL DEFAULT NULL,
  `charter_id` BIGINT(20) NULL DEFAULT NULL,
  `company_id` BIGINT(20) NULL DEFAULT NULL,
  `country_id` BIGINT(20) NULL DEFAULT NULL,
  `location_id` BIGINT(20) NULL DEFAULT NULL,
  `party_id` BIGINT(20) NULL DEFAULT NULL,
  `por_id` BIGINT(20) NULL DEFAULT NULL,
  `service_id` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `FK_AIR_PREBOOK_CARRIERID` (`carrier_id` ASC),
  INDEX `FK_AIR_PREBOOK_CHARTERID` (`charter_id` ASC),
  INDEX `FK_AIR_PREBOOK_COMPANYID` (`company_id` ASC),
  INDEX `FK_AIR_PREBOOK_COUNTRYID` (`country_id` ASC),
  INDEX `FK_AIR_PREBOOK_LOCATIONID` (`location_id` ASC),
  INDEX `FK_AIR_PREBOOK_PARTYID` (`party_id` ASC),
  INDEX `FK_AIR_PREBOOK_PORID` (`por_id` ASC),
  INDEX `FK_AIR_PREBOOK_SERVICEID` (`service_id` ASC),
  CONSTRAINT `FK_AIR_PREBOOK_CARRIERID`
    FOREIGN KEY (`carrier_id`)
    REFERENCES `efsdev`.`efs_carrier_master` (`id`),
  CONSTRAINT `FK_AIR_PREBOOK_CHARTERID`
    FOREIGN KEY (`charter_id`)
    REFERENCES `efsdev`.`efs_charter_master` (`id`),
  CONSTRAINT `FK_AIR_PREBOOK_COMPANYID`
    FOREIGN KEY (`company_id`)
    REFERENCES `efsdev`.`efs_company_master` (`id`),
  CONSTRAINT `FK_AIR_PREBOOK_COUNTRYID`
    FOREIGN KEY (`country_id`)
    REFERENCES `efsdev`.`efs_country_master` (`id`),
  CONSTRAINT `FK_AIR_PREBOOK_LOCATIONID`
    FOREIGN KEY (`location_id`)
    REFERENCES `efsdev`.`efs_location_master` (`id`),
  CONSTRAINT `FK_AIR_PREBOOK_PARTYID`
    FOREIGN KEY (`party_id`)
    REFERENCES `efsdev`.`efs_party_master` (`id`),
  CONSTRAINT `FK_AIR_PREBOOK_PORID`
    FOREIGN KEY (`por_id`)
    REFERENCES `efsdev`.`efs_port_master` (`id`),
  CONSTRAINT `FK_AIR_PREBOOK_SERVICEID`
    FOREIGN KEY (`service_id`)
    REFERENCES `efsdev`.`efs_service_master` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_flight_plan`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_flight_plan` (
  `id` BIGINT(20) NOT NULL,
  `balance` DOUBLE NULL DEFAULT NULL,
  `booked` DOUBLE NULL DEFAULT NULL,
  `capacity_reserved` DOUBLE NULL DEFAULT NULL,
  `direct_stopover` VARCHAR(30) NULL DEFAULT NULL,
  `effective_upto` DATETIME(6) NULL DEFAULT NULL,
  `eta` DATETIME(6) NULL DEFAULT NULL,
  `etd` DATETIME(6) NULL DEFAULT NULL,
  `flight_frequency` INT(11) NULL DEFAULT NULL,
  `flight_no` VARCHAR(255) NULL DEFAULT NULL,
  `flight_status` VARCHAR(30) NOT NULL,
  `generated` DOUBLE NULL DEFAULT NULL,
  `received` DOUBLE NULL DEFAULT NULL,
  `schedule_id` VARCHAR(50) NOT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `transit` DOUBLE NULL DEFAULT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  `carrier_id` BIGINT(20) NULL DEFAULT NULL,
  `pod_id` BIGINT(20) NULL DEFAULT NULL,
  `pol_id` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `UK_FLIGHTP_SCHEDULEID` (`schedule_id` ASC),
  INDEX `FK_FLIGHTP_CARRIERID` (`carrier_id` ASC),
  INDEX `FK_FLIGHTP_PODID` (`pod_id` ASC),
  INDEX `FK_FLIGHTP_POLID` (`pol_id` ASC),
  CONSTRAINT `FK_FLIGHTP_CARRIERID`
    FOREIGN KEY (`carrier_id`)
    REFERENCES `efsdev`.`efs_carrier_master` (`id`),
  CONSTRAINT `FK_FLIGHTP_PODID`
    FOREIGN KEY (`pod_id`)
    REFERENCES `efsdev`.`efs_port_master` (`id`),
  CONSTRAINT `FK_FLIGHTP_POLID`
    FOREIGN KEY (`pol_id`)
    REFERENCES `efsdev`.`efs_port_master` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_airline_prebook_sched`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_airline_prebook_sched` (
  `id` BIGINT(20) NOT NULL,
  `gross_weight` DOUBLE NULL DEFAULT NULL,
  `mawb_no` VARCHAR(255) NULL DEFAULT NULL,
  `no_of_piece` INT(11) NULL DEFAULT NULL,
  `status` VARCHAR(255) NULL DEFAULT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  `vol_weight` DOUBLE NULL DEFAULT NULL,
  `volume` DOUBLE NULL DEFAULT NULL,
  `airline_prebooking_id` BIGINT(20) NOT NULL,
  `flight_plan_id` BIGINT(20) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `FK_AIRPRESCHM_AIRPREID` (`airline_prebooking_id` ASC),
  INDEX `FK_AIRPRESCHM_FLIID` (`flight_plan_id` ASC),
  CONSTRAINT `FK_AIRPRESCHM_AIRPREID`
    FOREIGN KEY (`airline_prebooking_id`)
    REFERENCES `efsdev`.`efs_airline_prebooking` (`id`),
  CONSTRAINT `FK_AIRPRESCHM_FLIID`
    FOREIGN KEY (`flight_plan_id`)
    REFERENCES `efsdev`.`efs_flight_plan` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_ams_disposition_master`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_ams_disposition_master` (
  `id` BIGINT(20) NOT NULL,
  `code` VARCHAR(10) NOT NULL,
  `name` VARCHAR(100) NOT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_audit_log`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_audit_log` (
  `id` BIGINT(20) NOT NULL,
  `class_name` VARCHAR(70) NULL DEFAULT NULL,
  `create_date` DATETIME(6) NULL DEFAULT NULL,
  `http_response_code` INT(11) NULL DEFAULT NULL,
  `last_updated_date` DATETIME(6) NULL DEFAULT NULL,
  `last_updated_user_name` VARCHAR(30) NULL DEFAULT NULL,
  `method_name` VARCHAR(40) NULL DEFAULT NULL,
  `status_code` INT(11) NULL DEFAULT NULL,
  `user_name` VARCHAR(30) NULL DEFAULT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_service_old_entry`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_service_old_entry` (
  `id` BIGINT(20) NOT NULL,
  `booked_chargeable_unit` DOUBLE NULL DEFAULT NULL,
  `booked_gross_wt_unit_kg` DOUBLE NULL DEFAULT NULL,
  `booked_gross_wt_unit_pound` DOUBLE NULL DEFAULT NULL,
  `booked_pieces` BIGINT(20) NULL DEFAULT NULL,
  `booked_volume_wt_unit_kg` DOUBLE NULL DEFAULT NULL,
  `booked_volume_wt_unit_pound` DOUBLE NULL DEFAULT NULL,
  `cfs_receipt_no` VARCHAR(30) NOT NULL,
  `service_uid` VARCHAR(255) NOT NULL,
  `shipment_uid` VARCHAR(255) NOT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_service_status`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_service_status` (
  `id` BIGINT(20) NOT NULL,
  `service_status` VARCHAR(30) NOT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  `shipment_service_id` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `FK_SERVSTATUS_SHIPMENTSERVID` (`shipment_service_id` ASC),
  CONSTRAINT `FK_SERVSTATUS_SHIPMENTSERVID`
    FOREIGN KEY (`shipment_service_id`)
    REFERENCES `efsdev`.`efs_service` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_pack_master`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_pack_master` (
  `id` BIGINT(20) NOT NULL,
  `pack_code` VARCHAR(10) NOT NULL,
  `pack_name` VARCHAR(100) NOT NULL,
  `status` VARCHAR(10) NOT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `UK_PACK_PACKCODE` (`pack_code` ASC),
  UNIQUE INDEX `UK_PACK_PACKNAME` (`pack_name` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_cfs_master`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_cfs_master` (
  `id` BIGINT(20) NOT NULL,
  `cfs_code` VARCHAR(100) NOT NULL,
  `cfs_name` VARCHAR(100) NOT NULL,
  `port_code` VARCHAR(10) NULL DEFAULT NULL,
  `status` VARCHAR(10) NOT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  `address_id` BIGINT(20) NULL DEFAULT NULL,
  `port_master_id` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `UK_CFSMAS_CFSCODE` (`cfs_code` ASC),
  UNIQUE INDEX `UK_CFSMAS_CFSNAME` (`cfs_name` ASC),
  INDEX `FK_CFS_ADDRESS` (`address_id` ASC),
  INDEX `UK_CFSMAS_PORTID` (`port_master_id` ASC),
  CONSTRAINT `FK_CFS_ADDRESS`
    FOREIGN KEY (`address_id`)
    REFERENCES `efsdev`.`efs_address_master` (`id`),
  CONSTRAINT `UK_CFSMAS_PORTID`
    FOREIGN KEY (`port_master_id`)
    REFERENCES `efsdev`.`efs_port_master` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_pickup_delivery_point`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_pickup_delivery_point` (
  `id` BIGINT(20) NOT NULL,
  `delivery_actual` DATETIME(6) NULL DEFAULT NULL,
  `delivery_contact_person` VARCHAR(255) NULL DEFAULT NULL,
  `delivery_email` VARCHAR(500) NULL DEFAULT NULL,
  `delivery_expected` DATETIME(6) NULL DEFAULT NULL,
  `delivery_from_code` VARCHAR(255) NULL DEFAULT NULL,
  `delivery_mobile_no` VARCHAR(255) NULL DEFAULT NULL,
  `delivery_phone_no` VARCHAR(255) NULL DEFAULT NULL,
  `delivery_point_code` VARCHAR(255) NULL DEFAULT NULL,
  `door_delivery_actual` DATETIME(6) NULL DEFAULT NULL,
  `door_delivery_contact_person` VARCHAR(255) NULL DEFAULT NULL,
  `door_delivery_email` VARCHAR(500) NULL DEFAULT NULL,
  `door_delivery_expected` DATETIME(6) NULL DEFAULT NULL,
  `door_delivery_from_code` VARCHAR(255) NULL DEFAULT NULL,
  `door_delivery_mobile_no` VARCHAR(255) NULL DEFAULT NULL,
  `door_delivery_phone_no` VARCHAR(255) NULL DEFAULT NULL,
  `door_delivery_point_code` VARCHAR(255) NULL DEFAULT NULL,
  `is_our_pick_up` VARCHAR(255) NULL DEFAULT NULL,
  `pickup_actual` DATETIME(6) NULL DEFAULT NULL,
  `pickup_client_ref_no` VARCHAR(255) NULL DEFAULT NULL,
  `pickup_contact_person` VARCHAR(255) NULL DEFAULT NULL,
  `pickup_email` VARCHAR(500) NULL DEFAULT NULL,
  `pickup_fax` VARCHAR(255) NULL DEFAULT NULL,
  `pickup_follow_up` DATETIME(6) NULL DEFAULT NULL,
  `pickup_mobile_no` VARCHAR(255) NULL DEFAULT NULL,
  `pickup_phone_no` VARCHAR(255) NULL DEFAULT NULL,
  `pickup_planned` DATETIME(6) NULL DEFAULT NULL,
  `pickup_from_code` VARCHAR(255) NULL DEFAULT NULL,
  `pickup_point_code` VARCHAR(255) NULL DEFAULT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `transporter_address` VARCHAR(255) NULL DEFAULT NULL,
  `transporter_code` VARCHAR(255) NULL DEFAULT NULL,
  `transporter_manifest_name` VARCHAR(255) NULL DEFAULT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  `delivery_from_id` BIGINT(20) NULL DEFAULT NULL,
  `delivery_place` BIGINT(20) NULL DEFAULT NULL,
  `delivery_point_id` BIGINT(20) NULL DEFAULT NULL,
  `door_delivery_from_id` BIGINT(20) NULL DEFAULT NULL,
  `door_delivery_place` BIGINT(20) NULL DEFAULT NULL,
  `door_delivery_point_id` BIGINT(20) NULL DEFAULT NULL,
  `pickup_place` BIGINT(20) NULL DEFAULT NULL,
  `pickup_from_id` BIGINT(20) NULL DEFAULT NULL,
  `pickup_point_id` BIGINT(20) NULL DEFAULT NULL,
  `transporter_address_id` BIGINT(20) NULL DEFAULT NULL,
  `transporter_id` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `FK_PICKDLI_DELVFROM` (`delivery_from_id` ASC),
  INDEX `FK_PICKUP_DELIVERY_PLACE` (`delivery_place` ASC),
  INDEX `FK_PICKDLI_DELVPOINT` (`delivery_point_id` ASC),
  INDEX `FK_PICKDLI_DORDELVFROM` (`door_delivery_from_id` ASC),
  INDEX `FK_PICKUP_DOOR_DELIVERY_PLACE` (`door_delivery_place` ASC),
  INDEX `FK_PICKDLI_DORDELVPOINT` (`door_delivery_point_id` ASC),
  INDEX `FK_PICKUP_PLACE` (`pickup_place` ASC),
  INDEX `FK_PICKDLI_PICKFROM` (`pickup_from_id` ASC),
  INDEX `FK_PICKDLI_PICKPOINT` (`pickup_point_id` ASC),
  INDEX `FK_PICKDLI_TRANSPORTERADD` (`transporter_address_id` ASC),
  INDEX `FK_PICKDLI_TRANSPORTERID` (`transporter_id` ASC),
  CONSTRAINT `FK_PICKDLI_DELVFROM`
    FOREIGN KEY (`delivery_from_id`)
    REFERENCES `efsdev`.`efs_port_master` (`id`),
  CONSTRAINT `FK_PICKDLI_DELVPOINT`
    FOREIGN KEY (`delivery_point_id`)
    REFERENCES `efsdev`.`efs_cfs_master` (`id`),
  CONSTRAINT `FK_PICKDLI_DORDELVFROM`
    FOREIGN KEY (`door_delivery_from_id`)
    REFERENCES `efsdev`.`efs_port_master` (`id`),
  CONSTRAINT `FK_PICKDLI_DORDELVPOINT`
    FOREIGN KEY (`door_delivery_point_id`)
    REFERENCES `efsdev`.`efs_port_master` (`id`),
  CONSTRAINT `FK_PICKDLI_PICKFROM`
    FOREIGN KEY (`pickup_from_id`)
    REFERENCES `efsdev`.`efs_port_master` (`id`),
  CONSTRAINT `FK_PICKDLI_PICKPOINT`
    FOREIGN KEY (`pickup_point_id`)
    REFERENCES `efsdev`.`efs_party_master` (`id`),
  CONSTRAINT `FK_PICKDLI_TRANSPORTERADD`
    FOREIGN KEY (`transporter_address_id`)
    REFERENCES `efsdev`.`efs_address_master` (`id`),
  CONSTRAINT `FK_PICKDLI_TRANSPORTERID`
    FOREIGN KEY (`transporter_id`)
    REFERENCES `efsdev`.`efs_party_master` (`id`),
  CONSTRAINT `FK_PICKUP_DELIVERY_PLACE`
    FOREIGN KEY (`delivery_place`)
    REFERENCES `efsdev`.`efs_address_master` (`id`),
  CONSTRAINT `FK_PICKUP_DOOR_DELIVERY_PLACE`
    FOREIGN KEY (`door_delivery_place`)
    REFERENCES `efsdev`.`efs_address_master` (`id`),
  CONSTRAINT `FK_PICKUP_PLACE`
    FOREIGN KEY (`pickup_place`)
    REFERENCES `efsdev`.`efs_address_master` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_buyer_consol`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_buyer_consol` (
  `id` BIGINT(20) NOT NULL,
  `bc_code` VARCHAR(100) NOT NULL,
  `bc_name` VARCHAR(100) NOT NULL,
  `ref1` VARCHAR(200) NULL DEFAULT NULL,
  `ref2` VARCHAR(200) NULL DEFAULT NULL,
  `ref3` VARCHAR(200) NULL DEFAULT NULL,
  `ref4` VARCHAR(200) NULL DEFAULT NULL,
  `ref5` VARCHAR(200) NULL DEFAULT NULL,
  `status` VARCHAR(10) NOT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `UK_BUYERS_CONSOLD_BCCODE` (`bc_code` ASC),
  UNIQUE INDEX `UK_BUYERS_CONSOLD_BCNAME` (`bc_name` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_purchase_order`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_purchase_order` (
  `id` BIGINT(20) NOT NULL,
  `bcc_email` VARCHAR(500) NULL DEFAULT NULL,
  `buyer_code` VARCHAR(255) NOT NULL,
  `company_code` VARCHAR(100) NULL DEFAULT NULL,
  `consol_or_direct` VARCHAR(10) NOT NULL,
  `delivery_to` VARCHAR(255) NULL DEFAULT NULL,
  `dest_agent_code` VARCHAR(255) NULL DEFAULT NULL,
  `destination_agent_email` VARCHAR(500) NULL DEFAULT NULL,
  `destination` VARCHAR(255) NULL DEFAULT NULL,
  `doc_received_date` DATETIME(6) NULL DEFAULT NULL,
  `family_product_code` VARCHAR(255) NULL DEFAULT NULL,
  `location_code` VARCHAR(100) NULL DEFAULT NULL,
  `origin_agent_code` VARCHAR(255) NULL DEFAULT NULL,
  `origin_agent_email` VARCHAR(500) NULL DEFAULT NULL,
  `origin` VARCHAR(255) NULL DEFAULT NULL,
  `po_attachment_date_format` VARCHAR(40) NULL DEFAULT NULL,
  `po_attachment_filename` VARCHAR(255) NULL DEFAULT NULL,
  `po_date` DATETIME(6) NOT NULL,
  `po_no` VARCHAR(255) NOT NULL,
  `processing_person_email` VARCHAR(500) NULL DEFAULT NULL,
  `received_from` VARCHAR(255) NULL DEFAULT NULL,
  `special_instruction` VARCHAR(4000) NULL DEFAULT NULL,
  `status` VARCHAR(10) NOT NULL,
  `supplier_code` VARCHAR(255) NOT NULL,
  `supplier_email` VARCHAR(500) NULL DEFAULT NULL,
  `supplier_reference_no` VARCHAR(255) NULL DEFAULT NULL,
  `supplier_remark` VARCHAR(4000) NULL DEFAULT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `tos_code` VARCHAR(255) NULL DEFAULT NULL,
  `transport_mode` VARCHAR(10) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  `buyer_id` BIGINT(20) NOT NULL,
  `company_id` BIGINT(20) NOT NULL,
  `destination_id` BIGINT(20) NULL DEFAULT NULL,
  `dest_agent_id` BIGINT(20) NULL DEFAULT NULL,
  `location_id` BIGINT(20) NOT NULL,
  `origin_id` BIGINT(20) NULL DEFAULT NULL,
  `origin_agent_id` BIGINT(20) NULL DEFAULT NULL,
  `supplier_id` BIGINT(20) NOT NULL,
  `tos_id` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `UK_PURCHASE_ORDER_NO` (`po_no` ASC),
  INDEX `FK_PURCHASE_ORDER_BUYER` (`buyer_id` ASC),
  INDEX `FK_PURCHASE_ORDER_COMPANY` (`company_id` ASC),
  INDEX `FK_PURCHASE_ORDER_DESTINATION` (`destination_id` ASC),
  INDEX `FK_PURCHASE_ORDER_DEST_AGENT` (`dest_agent_id` ASC),
  INDEX `FK_PURCHASE_ORDER_LOCATION` (`location_id` ASC),
  INDEX `FK_PURCHASE_ORDER_ORIGIN` (`origin_id` ASC),
  INDEX `FK_PURCHASE_ORDER_ORIGIN_AGENT` (`origin_agent_id` ASC),
  INDEX `FK_PURCHASE_ORDER_SUPPLIER` (`supplier_id` ASC),
  INDEX `FK_PURCHASE_ORDER_TOS` (`tos_id` ASC),
  CONSTRAINT `FK_PURCHASE_ORDER_BUYER`
    FOREIGN KEY (`buyer_id`)
    REFERENCES `efsdev`.`efs_buyer_consol` (`id`),
  CONSTRAINT `FK_PURCHASE_ORDER_COMPANY`
    FOREIGN KEY (`company_id`)
    REFERENCES `efsdev`.`efs_company_master` (`id`),
  CONSTRAINT `FK_PURCHASE_ORDER_DESTINATION`
    FOREIGN KEY (`destination_id`)
    REFERENCES `efsdev`.`efs_port_master` (`id`),
  CONSTRAINT `FK_PURCHASE_ORDER_DEST_AGENT`
    FOREIGN KEY (`dest_agent_id`)
    REFERENCES `efsdev`.`efs_party_master` (`id`),
  CONSTRAINT `FK_PURCHASE_ORDER_LOCATION`
    FOREIGN KEY (`location_id`)
    REFERENCES `efsdev`.`efs_location_master` (`id`),
  CONSTRAINT `FK_PURCHASE_ORDER_ORIGIN`
    FOREIGN KEY (`origin_id`)
    REFERENCES `efsdev`.`efs_port_master` (`id`),
  CONSTRAINT `FK_PURCHASE_ORDER_ORIGIN_AGENT`
    FOREIGN KEY (`origin_agent_id`)
    REFERENCES `efsdev`.`efs_party_master` (`id`),
  CONSTRAINT `FK_PURCHASE_ORDER_SUPPLIER`
    FOREIGN KEY (`supplier_id`)
    REFERENCES `efsdev`.`efs_party_master` (`id`),
  CONSTRAINT `FK_PURCHASE_ORDER_TOS`
    FOREIGN KEY (`tos_id`)
    REFERENCES `efsdev`.`efs_tos_master` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_service_master_detail`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_service_master_detail` (
  `id` BIGINT(20) NOT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  `company_id` BIGINT(20) NULL DEFAULT NULL,
  `cost_center_id` BIGINT(20) NULL DEFAULT NULL,
  `service_master_id` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `FK_SEGMENTDETA_COMPANYID` (`company_id` ASC),
  INDEX `FK_SEGMENTDETA_COSTCENTERID` (`cost_center_id` ASC),
  INDEX `FK_SEGMENTDETA_SERVICEMASID` (`service_master_id` ASC),
  CONSTRAINT `FK_SEGMENTDETA_COMPANYID`
    FOREIGN KEY (`company_id`)
    REFERENCES `efsdev`.`efs_company_master` (`id`),
  CONSTRAINT `FK_SEGMENTDETA_COSTCENTERID`
    FOREIGN KEY (`cost_center_id`)
    REFERENCES `efsdev`.`efs_cost_center_master` (`id`),
  CONSTRAINT `FK_SEGMENTDETA_SERVICEMASID`
    FOREIGN KEY (`service_master_id`)
    REFERENCES `efsdev`.`efs_service_master` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_service_detail`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_service_detail` (
  `id` BIGINT(20) NOT NULL,
  `destination_clearance` VARCHAR(5) NULL DEFAULT NULL,
  `destination_trucking` VARCHAR(5) NULL DEFAULT NULL,
  `origin_clearance` VARCHAR(5) NULL DEFAULT NULL,
  `origin_trucking` VARCHAR(5) NULL DEFAULT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_shipment_status`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_shipment_status` (
  `id` BIGINT(20) NOT NULL,
  `shipment_status` VARCHAR(30) NOT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  `shipment_id` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `FK_SHIPSTATUS_SHIPMENTID` (`shipment_id` ASC),
  CONSTRAINT `FK_SHIPSTATUS_SHIPMENTID`
    FOREIGN KEY (`shipment_id`)
    REFERENCES `efsdev`.`efs_shipment` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_shipment_detail`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_shipment_detail` (
  `id` BIGINT(20) NOT NULL,
  `shipping_bill_date` DATETIME(6) NULL DEFAULT NULL,
  `shipping_bill_number` VARCHAR(255) NULL DEFAULT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `UK_SHIPMENTDETAIL_SHIPBILLNO` (`shipping_bill_number` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_shipment`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_shipment` (
  `id` BIGINT(20) NOT NULL,
  `company_code` VARCHAR(255) NULL DEFAULT NULL,
  `country_code` VARCHAR(255) NULL DEFAULT NULL,
  `created_by_code` VARCHAR(255) NOT NULL,
  `customer_manifest_name` VARCHAR(255) NULL DEFAULT NULL,
  `is_from_consol` VARCHAR(5) NULL DEFAULT NULL,
  `location_code` VARCHAR(255) NULL DEFAULT NULL,
  `shipment_req_date` DATETIME(6) NOT NULL,
  `shipment_uid` VARCHAR(255) NOT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  `company_id` BIGINT(20) NULL DEFAULT NULL,
  `country_id` BIGINT(20) NULL DEFAULT NULL,
  `created_by` BIGINT(20) NOT NULL,
  `last_updated_status` BIGINT(20) NULL DEFAULT NULL,
  `location_id` BIGINT(20) NULL DEFAULT NULL,
  `shipment_detail` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `UK_SHIPMENT_SHIPMENTUID` (`shipment_uid` ASC),
  INDEX `IDX_SHIPMENT_SHIPMENTDATE` (`shipment_req_date` ASC),
  INDEX `IDX_SHIPMENT_CRTEBYID` (`created_by` ASC),
  INDEX `FK_SHIPMENT_COMPANYID` (`company_id` ASC),
  INDEX `FK_SHIPMENT_COUNTRYID` (`country_id` ASC),
  INDEX `FK_SHIPMENT_LASTUPTSTATUS` (`last_updated_status` ASC),
  INDEX `FK_SHIPMENT_LOCATIONID` (`location_id` ASC),
  INDEX `FK_SHIPMENT_SHIPMENTDETAIL` (`shipment_detail` ASC),
  CONSTRAINT `FK_SHIPMENT_COMPANYID`
    FOREIGN KEY (`company_id`)
    REFERENCES `efsdev`.`efs_company_master` (`id`),
  CONSTRAINT `FK_SHIPMENT_COUNTRYID`
    FOREIGN KEY (`country_id`)
    REFERENCES `efsdev`.`efs_country_master` (`id`),
  CONSTRAINT `FK_SHIPMENT_CREATEDID`
    FOREIGN KEY (`created_by`)
    REFERENCES `efsdev`.`efs_employee_master` (`id`),
  CONSTRAINT `FK_SHIPMENT_LASTUPTSTATUS`
    FOREIGN KEY (`last_updated_status`)
    REFERENCES `efsdev`.`efs_shipment_status` (`id`),
  CONSTRAINT `FK_SHIPMENT_LOCATIONID`
    FOREIGN KEY (`location_id`)
    REFERENCES `efsdev`.`efs_location_master` (`id`),
  CONSTRAINT `FK_SHIPMENT_SHIPMENTDETAIL`
    FOREIGN KEY (`shipment_detail`)
    REFERENCES `efsdev`.`efs_shipment_detail` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_comment_master`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_comment_master` (
  `id` BIGINT(20) NOT NULL,
  `comment_code` VARCHAR(10) NOT NULL,
  `comment_name` VARCHAR(100) NOT NULL,
  `description` VARCHAR(2000) NOT NULL,
  `status` VARCHAR(10) NOT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `UK_COMMENT_COMMENTCODE` (`comment_code` ASC),
  UNIQUE INDEX `UK_COMMENT_COMMENTNAME` (`comment_name` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_service_signoff`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_service_signoff` (
  `id` BIGINT(20) NOT NULL,
  `consol_uid` VARCHAR(255) NULL DEFAULT NULL,
  `description` VARCHAR(4000) NULL DEFAULT NULL,
  `is_sign_off` VARCHAR(10) NULL DEFAULT NULL,
  `service_uid` VARCHAR(255) NULL DEFAULT NULL,
  `shipment_uid` VARCHAR(255) NULL DEFAULT NULL,
  `signoff_by_code` VARCHAR(100) NULL DEFAULT NULL,
  `signoff_date` DATETIME(6) NULL DEFAULT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  `comment_id` BIGINT(20) NULL DEFAULT NULL,
  `signoff_by` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `FK_SHIPSIGNOFF_COMMENT` (`comment_id` ASC),
  INDEX `FK_SHIPSIGNOFF_BY` (`signoff_by` ASC),
  CONSTRAINT `FK_SHIPSIGNOFF_BY`
    FOREIGN KEY (`signoff_by`)
    REFERENCES `efsdev`.`efs_employee_master` (`id`),
  CONSTRAINT `FK_SHIPSIGNOFF_COMMENT`
    FOREIGN KEY (`comment_id`)
    REFERENCES `efsdev`.`efs_comment_master` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_project_master`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_project_master` (
  `id` BIGINT(20) NOT NULL,
  `project_code` VARCHAR(10) NULL DEFAULT NULL,
  `project_name` VARCHAR(100) NULL DEFAULT NULL,
  `status` VARCHAR(10) NOT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `UK_PROJECT_PROJECTCODE` (`project_code` ASC),
  UNIQUE INDEX `UK_PROJECT_PROJECTNAME` (`project_name` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_service`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_service` (
  `id` BIGINT(20) NOT NULL,
  `aes_no` VARCHAR(255) NULL DEFAULT NULL,
  `agent_code` VARCHAR(255) NULL DEFAULT NULL,
  `auto_import` VARCHAR(255) NULL DEFAULT NULL,
  `booked_chargeable_unit` DOUBLE NULL DEFAULT NULL,
  `booked_gross_wt_unit_code` VARCHAR(255) NULL DEFAULT NULL,
  `booked_gross_wt_unit_kg` DOUBLE NULL DEFAULT NULL,
  `booked_gross_wt_unit_pound` DOUBLE NULL DEFAULT NULL,
  `booked_gross_wt_unit_value` DOUBLE NULL DEFAULT NULL,
  `booked_pieces` BIGINT(20) NULL DEFAULT NULL,
  `booked_volume_unit_cbm` DOUBLE NULL DEFAULT NULL,
  `booked_volume_unit_code` VARCHAR(255) NULL DEFAULT NULL,
  `booked_volume_unit_value` DOUBLE NULL DEFAULT NULL,
  `booked_volume_wt_unit_code` VARCHAR(255) NULL DEFAULT NULL,
  `booked_volume_wt_unit_kg` DOUBLE NULL DEFAULT NULL,
  `booked_volume_wt_unit_pound` DOUBLE NULL DEFAULT NULL,
  `booked_volume_wt_unit_value` DOUBLE NULL DEFAULT NULL,
  `brokerage_party_code` VARCHAR(100) NULL DEFAULT NULL,
  `brokerage_percentage` DOUBLE NULL DEFAULT NULL,
  `carrier_booking_number` VARCHAR(255) NULL DEFAULT NULL,
  `carrier_code` VARCHAR(255) NULL DEFAULT NULL,
  `cfs_receipt_no` VARCHAR(255) NULL DEFAULT NULL,
  `cfs_remark` VARCHAR(255) NULL DEFAULT NULL,
  `client_gross_rate` DOUBLE NULL DEFAULT NULL,
  `client_net_rate` DOUBLE NULL DEFAULT NULL,
  `co_loaded_code` VARCHAR(255) NULL DEFAULT NULL,
  `commodity_code` VARCHAR(255) NULL DEFAULT NULL,
  `company_code` VARCHAR(255) NOT NULL,
  `consol_uid` VARCHAR(255) NULL DEFAULT NULL,
  `cost` DOUBLE NULL DEFAULT NULL,
  `country_code` VARCHAR(10) NULL DEFAULT NULL,
  `customer_manifest_name` VARCHAR(255) NULL DEFAULT NULL,
  `customer_service_person_code` VARCHAR(255) NOT NULL,
  `customer_service_person_email` VARCHAR(255) NULL DEFAULT NULL,
  `declared_cost` DOUBLE NULL DEFAULT NULL,
  `service_destination` VARCHAR(255) NOT NULL,
  `dimension_unit` VARCHAR(50) NULL DEFAULT NULL,
  `dimension_unit_value` DOUBLE NULL DEFAULT NULL,
  `direct_shipment` VARCHAR(5) NULL DEFAULT NULL,
  `division_code` VARCHAR(255) NULL DEFAULT NULL,
  `dn_no` VARCHAR(255) NULL DEFAULT NULL,
  `eta` DATETIME(6) NULL DEFAULT NULL,
  `etd` DATETIME(6) NULL DEFAULT NULL,
  `export_ref` VARCHAR(255) NULL DEFAULT NULL,
  `hazardous` VARCHAR(5) NULL DEFAULT NULL,
  `hold_note` VARCHAR(4000) NULL DEFAULT NULL,
  `hold_release_note` VARCHAR(4000) NULL DEFAULT NULL,
  `hold_shipment` VARCHAR(5) NULL DEFAULT NULL,
  `import_ref` VARCHAR(255) NULL DEFAULT NULL,
  `internal_note` LONGTEXT NULL DEFAULT NULL,
  `is_agreed` VARCHAR(255) NOT NULL,
  `is_all_inclusive_charge` VARCHAR(10) NULL DEFAULT NULL,
  `is_clearance` VARCHAR(5) NULL DEFAULT NULL,
  `is_freehand` VARCHAR(5) NULL DEFAULT NULL,
  `is_marks_no` VARCHAR(255) NOT NULL,
  `is_minimum_shipment` VARCHAR(10) NULL DEFAULT NULL,
  `is_process_completed` VARCHAR(255) NULL DEFAULT NULL,
  `is_service_job` VARCHAR(5) NULL DEFAULT NULL,
  `is_transhipment` VARCHAR(5) NULL DEFAULT NULL,
  `item_no` VARCHAR(50) NULL DEFAULT NULL,
  `loading` VARCHAR(255) NULL DEFAULT NULL,
  `local_currency_code` VARCHAR(255) NULL DEFAULT NULL,
  `location_code` VARCHAR(255) NOT NULL,
  `mawb_date` DATETIME(6) NULL DEFAULT NULL,
  `mawb_no` VARCHAR(30) NULL DEFAULT NULL,
  `notes` LONGTEXT NULL DEFAULT NULL,
  `service_origin` VARCHAR(255) NOT NULL,
  `over_dimension` VARCHAR(5) NULL DEFAULT NULL,
  `pack_code` VARCHAR(255) NULL DEFAULT NULL,
  `party_code` VARCHAR(255) NOT NULL,
  `po_no` VARCHAR(255) NULL DEFAULT NULL,
  `pod` VARCHAR(255) NOT NULL,
  `pol` VARCHAR(255) NOT NULL,
  `freight_term` VARCHAR(10) NOT NULL,
  `pro_tracking_no` VARCHAR(255) NULL DEFAULT NULL,
  `process_instance_id` VARCHAR(255) NULL DEFAULT NULL,
  `project_code` VARCHAR(100) NULL DEFAULT NULL,
  `quotation_carrier_id` BIGINT(20) NULL DEFAULT NULL,
  `quotation_uid` VARCHAR(255) NULL DEFAULT NULL,
  `rate` DOUBLE NULL DEFAULT NULL,
  `route_no` VARCHAR(30) NULL DEFAULT NULL,
  `salesman_code` VARCHAR(255) NULL DEFAULT NULL,
  `schedule_uid` VARCHAR(255) NULL DEFAULT NULL,
  `service_code` VARCHAR(255) NOT NULL,
  `service_req_date` DATETIME(6) NULL DEFAULT NULL,
  `service_uid` VARCHAR(255) NOT NULL,
  `shipment_uid` VARCHAR(255) NOT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `task_id` VARCHAR(255) NULL DEFAULT NULL,
  `tos_code` VARCHAR(255) NULL DEFAULT NULL,
  `trade_code` VARCHAR(255) NULL DEFAULT NULL,
  `tranship_is_done_location` VARCHAR(5) NULL DEFAULT NULL,
  `transit_mode` VARCHAR(255) NULL DEFAULT NULL,
  `transit_port` VARCHAR(255) NULL DEFAULT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  `schedule_id` BIGINT(20) NULL DEFAULT NULL,
  `who_routed` VARCHAR(255) NULL DEFAULT NULL,
  `workflow_completed` VARCHAR(5) NULL DEFAULT NULL,
  `yard` VARCHAR(255) NULL DEFAULT NULL,
  `aes_id` BIGINT(20) NULL DEFAULT NULL,
  `agent_id` BIGINT(20) NULL DEFAULT NULL,
  `booked_gross_wt_unit_id` BIGINT(20) NULL DEFAULT NULL,
  `booked_volume_unit_id` BIGINT(20) NULL DEFAULT NULL,
  `booked_volume_wt_unit_id` BIGINT(20) NULL DEFAULT NULL,
  `brokerage_party_id` BIGINT(20) NULL DEFAULT NULL,
  `carrier_id` BIGINT(20) NULL DEFAULT NULL,
  `co_loaded_id` BIGINT(20) NULL DEFAULT NULL,
  `commodity_id` BIGINT(20) NULL DEFAULT NULL,
  `company_id` BIGINT(20) NOT NULL,
  `country_id` BIGINT(20) NULL DEFAULT NULL,
  `customer_service_person_id` BIGINT(20) NOT NULL,
  `service_destination_id` BIGINT(20) NOT NULL,
  `division_id` BIGINT(20) NULL DEFAULT NULL,
  `last_updated_status` BIGINT(20) NULL DEFAULT NULL,
  `local_currency_id` BIGINT(20) NULL DEFAULT NULL,
  `location_id` BIGINT(20) NOT NULL,
  `service_origin_id` BIGINT(20) NOT NULL,
  `pack_id` BIGINT(20) NULL DEFAULT NULL,
  `party_id` BIGINT(20) NOT NULL,
  `pickup_delivery_id` BIGINT(20) NULL DEFAULT NULL,
  `pod_id` BIGINT(20) NOT NULL,
  `pol_id` BIGINT(20) NOT NULL,
  `project_id` BIGINT(20) NULL DEFAULT NULL,
  `purchase_order_id` BIGINT(20) NULL DEFAULT NULL,
  `salesman_id` BIGINT(20) NULL DEFAULT NULL,
  `segment_master_detail` BIGINT(20) NULL DEFAULT NULL,
  `service_detail_id` BIGINT(20) NULL DEFAULT NULL,
  `service_master_id` BIGINT(20) NOT NULL,
  `service_receive_id` BIGINT(20) NULL DEFAULT NULL,
  `shipment_id` BIGINT(20) NOT NULL,
  `sign_off_id` BIGINT(20) NULL DEFAULT NULL,
  `tos_id` BIGINT(20) NULL DEFAULT NULL,
  `transit_port_id` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `UK_SERVICE_SERVICEUID` (`service_uid` ASC),
  UNIQUE INDEX `UK_SERVICE_DUPLICATE` (`shipment_id` ASC, `service_master_id` ASC, `location_id` ASC, `company_id` ASC, `country_id` ASC),
  INDEX `FK_AES_ID` (`aes_id` ASC),
  INDEX `FK_SERVICE_AGENTID` (`agent_id` ASC),
  INDEX `FK_SERVICE_BOOKGROSSWEIGHTUNIT` (`booked_gross_wt_unit_id` ASC),
  INDEX `FK_SERVICE_BOOKEDVOLUMEUNITID` (`booked_volume_unit_id` ASC),
  INDEX `FK_SERVICE_BOOKVOLUMEWEIGHTID` (`booked_volume_wt_unit_id` ASC),
  INDEX `FK_SERVICE_BROKERAGE_PARTY_ID` (`brokerage_party_id` ASC),
  INDEX `FK_SERVICE_CARRIERID` (`carrier_id` ASC),
  INDEX `FK_SERVICE_COLOADEDID` (`co_loaded_id` ASC),
  INDEX `FK_SHIPMENT_COMMODITYID` (`commodity_id` ASC),
  INDEX `FK_SERVICE_COMPANYID` (`company_id` ASC),
  INDEX `FK_SERVICE_COUNTRYID` (`country_id` ASC),
  INDEX `FK_SERVICE_CUSTSERVIEPERSONID` (`customer_service_person_id` ASC),
  INDEX `FK_SERVICE_DESTINATIONID` (`service_destination_id` ASC),
  INDEX `FK_SERVICE_DIVISONID` (`division_id` ASC),
  INDEX `FK_SERVICE_LASTUPTSTATUS` (`last_updated_status` ASC),
  INDEX `FK_SERVICE_LOCCURRID` (`local_currency_id` ASC),
  INDEX `FK_SERVICE_LOCATIONID` (`location_id` ASC),
  INDEX `FK_SERVICE_ORIGINID` (`service_origin_id` ASC),
  INDEX `FK_SERVICE_PACKMASTERID` (`pack_id` ASC),
  INDEX `FK_SERVICE_PARTYID` (`party_id` ASC),
  INDEX `FK_SERVICE_PICKDELID` (`pickup_delivery_id` ASC),
  INDEX `FK_SERVICE_PODID` (`pod_id` ASC),
  INDEX `FK_SERVICE_POLID` (`pol_id` ASC),
  INDEX `FK_SHIPMENT_PROJECTID` (`project_id` ASC),
  INDEX `FK_SERVICE_PURCORDID` (`purchase_order_id` ASC),
  INDEX `FK_SERVICE_SALESMANID` (`salesman_id` ASC),
  INDEX `FK_SERVICE_SEGMENTID` (`segment_master_detail` ASC),
  INDEX `FK_SERVICE_SERVICEDETAILID` (`service_detail_id` ASC),
  INDEX `FK_SERVICE_SERVICEMASID` (`service_master_id` ASC),
  INDEX `FK_SERVICE_ENTRY_ID` (`service_receive_id` ASC),
  INDEX `FK_SERVICE_SIGNOFFID` (`sign_off_id` ASC),
  INDEX `FK_SHIPMENT_TOSID` (`tos_id` ASC),
  INDEX `FK_SERVICE_TRANSITPORTID` (`transit_port_id` ASC),
  CONSTRAINT `FK_AES_ID`
    FOREIGN KEY (`aes_id`)
    REFERENCES `efsdev`.`efs_aes_file` (`id`),
  CONSTRAINT `FK_SERVICE_AGENTID`
    FOREIGN KEY (`agent_id`)
    REFERENCES `efsdev`.`efs_party_master` (`id`),
  CONSTRAINT `FK_SERVICE_BOOKEDVOLUMEUNITID`
    FOREIGN KEY (`booked_volume_unit_id`)
    REFERENCES `efsdev`.`efs_unit_master` (`id`),
  CONSTRAINT `FK_SERVICE_BOOKGROSSWEIGHTUNIT`
    FOREIGN KEY (`booked_gross_wt_unit_id`)
    REFERENCES `efsdev`.`efs_unit_master` (`id`),
  CONSTRAINT `FK_SERVICE_BOOKVOLUMEWEIGHTID`
    FOREIGN KEY (`booked_volume_wt_unit_id`)
    REFERENCES `efsdev`.`efs_unit_master` (`id`),
  CONSTRAINT `FK_SERVICE_BROKERAGE_PARTY_ID`
    FOREIGN KEY (`brokerage_party_id`)
    REFERENCES `efsdev`.`efs_party_master` (`id`),
  CONSTRAINT `FK_SERVICE_CARRIERID`
    FOREIGN KEY (`carrier_id`)
    REFERENCES `efsdev`.`efs_carrier_master` (`id`),
  CONSTRAINT `FK_SERVICE_COLOADEDID`
    FOREIGN KEY (`co_loaded_id`)
    REFERENCES `efsdev`.`efs_party_master` (`id`),
  CONSTRAINT `FK_SERVICE_COMPANYID`
    FOREIGN KEY (`company_id`)
    REFERENCES `efsdev`.`efs_company_master` (`id`),
  CONSTRAINT `FK_SERVICE_COUNTRYID`
    FOREIGN KEY (`country_id`)
    REFERENCES `efsdev`.`efs_country_master` (`id`),
  CONSTRAINT `FK_SERVICE_CUSTSERVIEPERSONID`
    FOREIGN KEY (`customer_service_person_id`)
    REFERENCES `efsdev`.`efs_employee_master` (`id`),
  CONSTRAINT `FK_SERVICE_DESTINATIONID`
    FOREIGN KEY (`service_destination_id`)
    REFERENCES `efsdev`.`efs_port_master` (`id`),
  CONSTRAINT `FK_SERVICE_DIVISONID`
    FOREIGN KEY (`division_id`)
    REFERENCES `efsdev`.`efs_division_master` (`id`),
  CONSTRAINT `FK_SERVICE_ENTRY_ID`
    FOREIGN KEY (`service_receive_id`)
    REFERENCES `efsdev`.`efs_service_old_entry` (`id`),
  CONSTRAINT `FK_SERVICE_LASTUPTSTATUS`
    FOREIGN KEY (`last_updated_status`)
    REFERENCES `efsdev`.`efs_service_status` (`id`),
  CONSTRAINT `FK_SERVICE_LOCATIONID`
    FOREIGN KEY (`location_id`)
    REFERENCES `efsdev`.`efs_location_master` (`id`),
  CONSTRAINT `FK_SERVICE_LOCCURRID`
    FOREIGN KEY (`local_currency_id`)
    REFERENCES `efsdev`.`efs_currency_master` (`id`),
  CONSTRAINT `FK_SERVICE_ORIGINID`
    FOREIGN KEY (`service_origin_id`)
    REFERENCES `efsdev`.`efs_port_master` (`id`),
  CONSTRAINT `FK_SERVICE_PACKMASTERID`
    FOREIGN KEY (`pack_id`)
    REFERENCES `efsdev`.`efs_pack_master` (`id`),
  CONSTRAINT `FK_SERVICE_PARTYID`
    FOREIGN KEY (`party_id`)
    REFERENCES `efsdev`.`efs_party_master` (`id`),
  CONSTRAINT `FK_SERVICE_PICKDELID`
    FOREIGN KEY (`pickup_delivery_id`)
    REFERENCES `efsdev`.`efs_pickup_delivery_point` (`id`),
  CONSTRAINT `FK_SERVICE_PODID`
    FOREIGN KEY (`pod_id`)
    REFERENCES `efsdev`.`efs_port_master` (`id`),
  CONSTRAINT `FK_SERVICE_POLID`
    FOREIGN KEY (`pol_id`)
    REFERENCES `efsdev`.`efs_port_master` (`id`),
  CONSTRAINT `FK_SERVICE_PURCORDID`
    FOREIGN KEY (`purchase_order_id`)
    REFERENCES `efsdev`.`efs_purchase_order` (`id`),
  CONSTRAINT `FK_SERVICE_SALESMANID`
    FOREIGN KEY (`salesman_id`)
    REFERENCES `efsdev`.`efs_employee_master` (`id`),
  CONSTRAINT `FK_SERVICE_SEGMENTID`
    FOREIGN KEY (`segment_master_detail`)
    REFERENCES `efsdev`.`efs_service_master_detail` (`id`),
  CONSTRAINT `FK_SERVICE_SERVICEDETAILID`
    FOREIGN KEY (`service_detail_id`)
    REFERENCES `efsdev`.`efs_service_detail` (`id`),
  CONSTRAINT `FK_SERVICE_SERVICEMASID`
    FOREIGN KEY (`service_master_id`)
    REFERENCES `efsdev`.`efs_service_master` (`id`),
  CONSTRAINT `FK_SERVICE_SHIPMENTID`
    FOREIGN KEY (`shipment_id`)
    REFERENCES `efsdev`.`efs_shipment` (`id`),
  CONSTRAINT `FK_SERVICE_SIGNOFFID`
    FOREIGN KEY (`sign_off_id`)
    REFERENCES `efsdev`.`efs_service_signoff` (`id`),
  CONSTRAINT `FK_SERVICE_TRANSITPORTID`
    FOREIGN KEY (`transit_port_id`)
    REFERENCES `efsdev`.`efs_port_master` (`id`),
  CONSTRAINT `FK_SHIPMENT_COMMODITYID`
    FOREIGN KEY (`commodity_id`)
    REFERENCES `efsdev`.`efs_hs_code_master` (`id`),
  CONSTRAINT `FK_SHIPMENT_PROJECTID`
    FOREIGN KEY (`project_id`)
    REFERENCES `efsdev`.`efs_project_master` (`id`),
  CONSTRAINT `FK_SHIPMENT_TOSID`
    FOREIGN KEY (`tos_id`)
    REFERENCES `efsdev`.`efs_tos_master` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_authenticated_docs`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_authenticated_docs` (
  `id` BIGINT(20) NOT NULL,
  `commodity` LONGTEXT NULL DEFAULT NULL,
  `company_code` VARCHAR(30) NOT NULL,
  `consignee_code` VARCHAR(255) NULL DEFAULT NULL,
  `country_code` VARCHAR(30) NOT NULL,
  `auth_docs_date` DATETIME(6) NOT NULL,
  `destination_code` VARCHAR(100) NULL DEFAULT NULL,
  `document_no` VARCHAR(255) NULL DEFAULT NULL,
  `first_notify_code` VARCHAR(255) NULL DEFAULT NULL,
  `forwarder_code` VARCHAR(255) NULL DEFAULT NULL,
  `gross_weight` DOUBLE NULL DEFAULT NULL,
  `location_code` VARCHAR(30) NOT NULL,
  `marks_and_no` LONGTEXT NULL DEFAULT NULL,
  `no_of_piece` INT(11) NULL DEFAULT NULL,
  `origin_code` VARCHAR(100) NULL DEFAULT NULL,
  `shipper_code` VARCHAR(255) NULL DEFAULT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  `vol_weight` DOUBLE NULL DEFAULT NULL,
  `company_id` BIGINT(20) NOT NULL,
  `consignee_id` BIGINT(20) NULL DEFAULT NULL,
  `consignee_address_id` BIGINT(20) NULL DEFAULT NULL,
  `country_id` BIGINT(20) NOT NULL,
  `destination_id` BIGINT(20) NOT NULL,
  `first_notify_id` BIGINT(20) NULL DEFAULT NULL,
  `first_notify_address_id` BIGINT(20) NULL DEFAULT NULL,
  `forwarder_id` BIGINT(20) NULL DEFAULT NULL,
  `forwarder_address_id` BIGINT(20) NULL DEFAULT NULL,
  `location_id` BIGINT(20) NOT NULL,
  `origin_id` BIGINT(20) NOT NULL,
  `shipment_service_id` BIGINT(20) NOT NULL,
  `shipper_id` BIGINT(20) NULL DEFAULT NULL,
  `shipper_address_id` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `FK_AUTH_DOCS_COMPID` (`company_id` ASC),
  INDEX `FK_AUTH_DOCS_CONSIGNEEID` (`consignee_id` ASC),
  INDEX `FK_AUTH_DOCS_CONSIGNADDID` (`consignee_address_id` ASC),
  INDEX `FK_AUTH_DOCS_COUNTID` (`country_id` ASC),
  INDEX `FK_AUTH_DOCS_DESTINATIONID` (`destination_id` ASC),
  INDEX `FK_AUTH_DOCS_FIRSTNOTIFYID` (`first_notify_id` ASC),
  INDEX `FK_AUTH_DOCS_FIRNOTADDID` (`first_notify_address_id` ASC),
  INDEX `FK_AUTH_DOCS_FORWARDID` (`forwarder_id` ASC),
  INDEX `FK_AUTH_DOCS_FORWARDADDID` (`forwarder_address_id` ASC),
  INDEX `FK_AUTH_DOCS_LOCTID` (`location_id` ASC),
  INDEX `FK_AUTH_DOCS_ORIGINID` (`origin_id` ASC),
  INDEX `FK_AUTH_DOCS_SERVIDETID` (`shipment_service_id` ASC),
  INDEX `FK_AUTH_DOCS_SHIPPERID` (`shipper_id` ASC),
  INDEX `FK_AUTH_DOCS_SHIPPERADDID` (`shipper_address_id` ASC),
  CONSTRAINT `FK_AUTH_DOCS_COMPID`
    FOREIGN KEY (`company_id`)
    REFERENCES `efsdev`.`efs_company_master` (`id`),
  CONSTRAINT `FK_AUTH_DOCS_CONSIGNADDID`
    FOREIGN KEY (`consignee_address_id`)
    REFERENCES `efsdev`.`efs_address_master` (`id`),
  CONSTRAINT `FK_AUTH_DOCS_CONSIGNEEID`
    FOREIGN KEY (`consignee_id`)
    REFERENCES `efsdev`.`efs_party_master` (`id`),
  CONSTRAINT `FK_AUTH_DOCS_COUNTID`
    FOREIGN KEY (`country_id`)
    REFERENCES `efsdev`.`efs_country_master` (`id`),
  CONSTRAINT `FK_AUTH_DOCS_DESTINATIONID`
    FOREIGN KEY (`destination_id`)
    REFERENCES `efsdev`.`efs_port_master` (`id`),
  CONSTRAINT `FK_AUTH_DOCS_FIRNOTADDID`
    FOREIGN KEY (`first_notify_address_id`)
    REFERENCES `efsdev`.`efs_address_master` (`id`),
  CONSTRAINT `FK_AUTH_DOCS_FIRSTNOTIFYID`
    FOREIGN KEY (`first_notify_id`)
    REFERENCES `efsdev`.`efs_party_master` (`id`),
  CONSTRAINT `FK_AUTH_DOCS_FORWARDADDID`
    FOREIGN KEY (`forwarder_address_id`)
    REFERENCES `efsdev`.`efs_address_master` (`id`),
  CONSTRAINT `FK_AUTH_DOCS_FORWARDID`
    FOREIGN KEY (`forwarder_id`)
    REFERENCES `efsdev`.`efs_party_master` (`id`),
  CONSTRAINT `FK_AUTH_DOCS_LOCTID`
    FOREIGN KEY (`location_id`)
    REFERENCES `efsdev`.`efs_location_master` (`id`),
  CONSTRAINT `FK_AUTH_DOCS_ORIGINID`
    FOREIGN KEY (`origin_id`)
    REFERENCES `efsdev`.`efs_port_master` (`id`),
  CONSTRAINT `FK_AUTH_DOCS_SERVIDETID`
    FOREIGN KEY (`shipment_service_id`)
    REFERENCES `efsdev`.`efs_service` (`id`),
  CONSTRAINT `FK_AUTH_DOCS_SHIPPERADDID`
    FOREIGN KEY (`shipper_address_id`)
    REFERENCES `efsdev`.`efs_address_master` (`id`),
  CONSTRAINT `FK_AUTH_DOCS_SHIPPERID`
    FOREIGN KEY (`shipper_id`)
    REFERENCES `efsdev`.`efs_party_master` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_auto_mail_group_master`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_auto_mail_group_master` (
  `id` BIGINT(20) NOT NULL,
  `message_group_code` VARCHAR(10) NULL DEFAULT NULL,
  `message_group_name` VARCHAR(100) NULL DEFAULT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `UK_AUTOMAIL_GROUP_MSGGRPCODE` (`message_group_code` ASC),
  UNIQUE INDEX `UK_AUTOMAIL_GROUP_MSGGRPNAME` (`message_group_name` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_auto_mail_master`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_auto_mail_master` (
  `id` BIGINT(20) NOT NULL,
  `status` VARCHAR(255) NULL DEFAULT NULL,
  `event_type` VARCHAR(255) NULL DEFAULT NULL,
  `message_code` VARCHAR(10) NOT NULL,
  `message_name` VARCHAR(100) NOT NULL,
  `message_send_type` VARCHAR(255) NULL DEFAULT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  `message_group_id` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `UK_AUTOMAIL_MESSAGECODE` (`message_code` ASC),
  UNIQUE INDEX `UK_AUTOMAIL_MESSAGENAME` (`message_name` ASC),
  INDEX `FK_AUTOMAIL_AUTOMAILGROUPID` (`message_group_id` ASC),
  CONSTRAINT `FK_AUTOMAIL_AUTOMAILGROUPID`
    FOREIGN KEY (`message_group_id`)
    REFERENCES `efsdev`.`efs_auto_mail_group_master` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_bank_master`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_bank_master` (
  `id` BIGINT(20) NOT NULL,
  `bank_code` VARCHAR(10) NOT NULL,
  `bank_name` VARCHAR(100) NOT NULL,
  `smart_business_bank_code` VARCHAR(30) NULL DEFAULT NULL,
  `status` VARCHAR(10) NOT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `UK_BANK_BANKCODE` (`bank_code` ASC),
  UNIQUE INDEX `UK_BANK_BANKNAME` (`bank_name` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_bank_account`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_bank_account` (
  `id` BIGINT(20) NOT NULL,
  `account_number` VARCHAR(30) NOT NULL,
  `branch_name` VARCHAR(30) NULL DEFAULT NULL,
  `company_code` VARCHAR(30) NOT NULL,
  `iban` VARCHAR(30) NULL DEFAULT NULL,
  `ifsc` VARCHAR(30) NULL DEFAULT NULL,
  `is_multiple_credit` VARCHAR(5) NOT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  `bank_master_id` BIGINT(20) NOT NULL,
  `bank_address_id` BIGINT(20) NULL DEFAULT NULL,
  `company_id` BIGINT(20) NOT NULL,
  `swift_address_id` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `FK_BANKACC_BANKID` (`bank_master_id` ASC),
  INDEX `FK_BANKACC_BANKADDRID` (`bank_address_id` ASC),
  INDEX `FK_BANKACC_COMPANY` (`company_id` ASC),
  INDEX `FK_BANKACC_BANKSWIFADDRID` (`swift_address_id` ASC),
  CONSTRAINT `FK_BANKACC_BANKADDRID`
    FOREIGN KEY (`bank_address_id`)
    REFERENCES `efsdev`.`efs_address_master` (`id`),
  CONSTRAINT `FK_BANKACC_BANKID`
    FOREIGN KEY (`bank_master_id`)
    REFERENCES `efsdev`.`efs_bank_master` (`id`),
  CONSTRAINT `FK_BANKACC_BANKSWIFADDRID`
    FOREIGN KEY (`swift_address_id`)
    REFERENCES `efsdev`.`efs_address_master` (`id`),
  CONSTRAINT `FK_BANKACC_COMPANY`
    FOREIGN KEY (`company_id`)
    REFERENCES `efsdev`.`efs_company_master` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_bill_of_entry`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_bill_of_entry` (
  `id` BIGINT(20) NOT NULL,
  `ack_date` DATETIME(6) NULL DEFAULT NULL,
  `ack_no` INT(11) NULL DEFAULT NULL,
  `amount` DOUBLE NULL DEFAULT NULL,
  `bill_of_entry` INT(11) NULL DEFAULT NULL,
  `boe_date` DATETIME(6) NULL DEFAULT NULL,
  `boe_invoice_no` VARCHAR(13) NULL DEFAULT NULL,
  `boe_value` DOUBLE NULL DEFAULT NULL,
  `decleartion_no` INT(11) NULL DEFAULT NULL,
  `note` LONGTEXT NULL DEFAULT NULL,
  `process_date` DATETIME(6) NULL DEFAULT NULL,
  `received_date` DATETIME(6) NULL DEFAULT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `transaction_type` VARCHAR(255) NULL DEFAULT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  `service_id` BIGINT(20) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `FK_BOE_SERVIDETID` (`service_id` ASC),
  CONSTRAINT `FK_BOE_SERVIDETID`
    FOREIGN KEY (`service_id`)
    REFERENCES `efsdev`.`efs_service` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_carrier_address_mapping`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_carrier_address_mapping` (
  `id` BIGINT(20) NOT NULL,
  `account_no` VARCHAR(100) NOT NULL,
  `carrier_code` VARCHAR(10) NOT NULL,
  `location_code` VARCHAR(10) NOT NULL,
  `party_code` VARCHAR(10) NOT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  `carrier_id` BIGINT(20) NULL DEFAULT NULL,
  `location_id` BIGINT(20) NULL DEFAULT NULL,
  `party_id` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `FK_CARRADDR_CARRIERID` (`carrier_id` ASC),
  INDEX `FK_CARRADDR_LOCATIONID` (`location_id` ASC),
  INDEX `FK_CARRADDR_PARTYID` (`party_id` ASC),
  CONSTRAINT `FK_CARRADDR_CARRIERID`
    FOREIGN KEY (`carrier_id`)
    REFERENCES `efsdev`.`efs_carrier_master` (`id`),
  CONSTRAINT `FK_CARRADDR_LOCATIONID`
    FOREIGN KEY (`location_id`)
    REFERENCES `efsdev`.`efs_location_master` (`id`),
  CONSTRAINT `FK_CARRADDR_PARTYID`
    FOREIGN KEY (`party_id`)
    REFERENCES `efsdev`.`efs_party_master` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_carrier_address_master`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_carrier_address_master` (
  `id` BIGINT(20) NOT NULL,
  `our_account_no` VARCHAR(40) NULL DEFAULT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  `carrier_id` BIGINT(20) NULL DEFAULT NULL,
  `location_id` BIGINT(20) NULL DEFAULT NULL,
  `carrier_agent_id` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `UK_CARRIERADD_CARRIER_LOCATION` (`carrier_id` ASC, `location_id` ASC),
  INDEX `FK_CARRIER_LOCATIONID` (`location_id` ASC),
  INDEX `FK_CARRIER_CARRIERAGENTID` (`carrier_agent_id` ASC),
  CONSTRAINT `FK_CARRIER_CARRIERAGENTID`
    FOREIGN KEY (`carrier_agent_id`)
    REFERENCES `efsdev`.`efs_party_master` (`id`),
  CONSTRAINT `FK_CARRIER_CARRIERID`
    FOREIGN KEY (`carrier_id`)
    REFERENCES `efsdev`.`efs_carrier_master` (`id`),
  CONSTRAINT `FK_CARRIER_LOCATIONID`
    FOREIGN KEY (`location_id`)
    REFERENCES `efsdev`.`efs_location_master` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_carrier_edi_mapping`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_carrier_edi_mapping` (
  `id` BIGINT(20) NOT NULL,
  `carrier_code` VARCHAR(10) NOT NULL,
  `edi_subtype` VARCHAR(100) NULL DEFAULT NULL,
  `edi_type` VARCHAR(100) NOT NULL,
  `edi_value` VARCHAR(100) NOT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  `carrier_id` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `UK_CARRIEREDI_EDITYPE` (`carrier_id` ASC, `edi_type` ASC),
  CONSTRAINT `FK_EDICARRIER_CARRIERID`
    FOREIGN KEY (`carrier_id`)
    REFERENCES `efsdev`.`efs_carrier_master` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_carrier_rate_master`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_carrier_rate_master` (
  `id` BIGINT(20) NOT NULL,
  `actual_chargeable` VARCHAR(255) NULL DEFAULT NULL,
  `carrier_code` VARCHAR(255) NOT NULL,
  `charge_code` VARCHAR(255) NOT NULL,
  `destination_code` VARCHAR(255) NULL DEFAULT NULL,
  `frsteff_amount` DOUBLE NULL DEFAULT NULL,
  `frsteff_frm_date` DATETIME(6) NULL DEFAULT NULL,
  `frsteff_min_amount` DOUBLE NULL DEFAULT NULL,
  `origin_code` VARCHAR(255) NULL DEFAULT NULL,
  `secondeff_amount` DOUBLE NULL DEFAULT NULL,
  `secondeff_frm_date` DATETIME(6) NULL DEFAULT NULL,
  `secondeff_min_amount` DOUBLE NULL DEFAULT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `unit_code` VARCHAR(255) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  `carrier_master_id` BIGINT(20) NOT NULL,
  `charge_master_id` BIGINT(20) NOT NULL,
  `destination_id` BIGINT(20) NULL DEFAULT NULL,
  `origin_id` BIGINT(20) NULL DEFAULT NULL,
  `unit_master_id` BIGINT(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `UK_CARRIER_RATE` (`carrier_master_id` ASC, `charge_master_id` ASC, `unit_master_id` ASC, `origin_id` ASC, `destination_id` ASC),
  INDEX `FK_CARRATE_CHARGEMASID` (`charge_master_id` ASC),
  INDEX `FK_CARRATE_DESTID` (`destination_id` ASC),
  INDEX `FK_CARRATE_ORIGINID` (`origin_id` ASC),
  INDEX `FK_CARRATE_UNITMASID` (`unit_master_id` ASC),
  CONSTRAINT `FK_CARRATE_CARRIERMASID`
    FOREIGN KEY (`carrier_master_id`)
    REFERENCES `efsdev`.`efs_carrier_master` (`id`),
  CONSTRAINT `FK_CARRATE_CHARGEMASID`
    FOREIGN KEY (`charge_master_id`)
    REFERENCES `efsdev`.`efs_charge_master` (`id`),
  CONSTRAINT `FK_CARRATE_DESTID`
    FOREIGN KEY (`destination_id`)
    REFERENCES `efsdev`.`efs_port_master` (`id`),
  CONSTRAINT `FK_CARRATE_ORIGINID`
    FOREIGN KEY (`origin_id`)
    REFERENCES `efsdev`.`efs_port_master` (`id`),
  CONSTRAINT `FK_CARRATE_UNITMASID`
    FOREIGN KEY (`unit_master_id`)
    REFERENCES `efsdev`.`efs_unit_master` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_cfs_receive_entry`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_cfs_receive_entry` (
  `id` BIGINT(20) NOT NULL,
  `booking_volume_in_cbm` DOUBLE NULL DEFAULT NULL,
  `cfs_code` VARCHAR(30) NULL DEFAULT NULL,
  `cfs_receipt_date` DATETIME(6) NULL DEFAULT NULL,
  `cfs_receipt_no` VARCHAR(30) NOT NULL,
  `chargeble_weight` DOUBLE NULL DEFAULT NULL,
  `company_code` VARCHAR(255) NULL DEFAULT NULL,
  `consignee_code` VARCHAR(30) NULL DEFAULT NULL,
  `country_code` VARCHAR(255) NULL DEFAULT NULL,
  `dimension_gross_weight` DOUBLE NULL DEFAULT NULL,
  `dimension_gross_weight_in_kg` DOUBLE NULL DEFAULT NULL,
  `dimension_unit` VARCHAR(50) NOT NULL,
  `dimension_unit_value` DOUBLE NULL DEFAULT NULL,
  `dimension_volume_weight` DOUBLE NULL DEFAULT NULL,
  `gross_weight` DOUBLE NULL DEFAULT NULL,
  `gross_weight_in_pound` DOUBLE NULL DEFAULT NULL,
  `location_code` VARCHAR(255) NULL DEFAULT NULL,
  `no_of_pieces` BIGINT(20) NULL DEFAULT NULL,
  `notes` VARCHAR(4000) NULL DEFAULT NULL,
  `on_hand` VARCHAR(30) NULL DEFAULT NULL,
  `pro_date` DATETIME(6) NULL DEFAULT NULL,
  `pro_number` VARCHAR(30) NULL DEFAULT NULL,
  `service_uid` VARCHAR(30) NULL DEFAULT NULL,
  `shipper_code` VARCHAR(30) NULL DEFAULT NULL,
  `shipper_ref_no` VARCHAR(30) NULL DEFAULT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `transporter_code` VARCHAR(30) NULL DEFAULT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  `volume_weight` DOUBLE NULL DEFAULT NULL,
  `volume_weight_in_pound` DOUBLE NULL DEFAULT NULL,
  `warehouse_loc` VARCHAR(100) NULL DEFAULT NULL,
  `cfs_id` BIGINT(20) NULL DEFAULT NULL,
  `company_id` BIGINT(20) NULL DEFAULT NULL,
  `consignee_id` BIGINT(20) NULL DEFAULT NULL,
  `country_id` BIGINT(20) NULL DEFAULT NULL,
  `location_id` BIGINT(20) NULL DEFAULT NULL,
  `service_id` BIGINT(20) NULL DEFAULT NULL,
  `shipper_id` BIGINT(20) NULL DEFAULT NULL,
  `transporter_id` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `FK_CFSRCV_CFSID` (`cfs_id` ASC),
  INDEX `FK_CFSRCV_COMPANYID` (`company_id` ASC),
  INDEX `FK_CFSRCV_CONSIGNID` (`consignee_id` ASC),
  INDEX `FK_CFSRCV_COUNTRYID` (`country_id` ASC),
  INDEX `FK_CFSRCV_LOCATIONID` (`location_id` ASC),
  INDEX `FK_CFSRCV_SERVICEID` (`service_id` ASC),
  INDEX `FK_CFSRCV_SHIPPERID` (`shipper_id` ASC),
  INDEX `FK_CFSRCV_TRANSPORTID` (`transporter_id` ASC),
  CONSTRAINT `FK_CFSRCV_CFSID`
    FOREIGN KEY (`cfs_id`)
    REFERENCES `efsdev`.`efs_cfs_master` (`id`),
  CONSTRAINT `FK_CFSRCV_COMPANYID`
    FOREIGN KEY (`company_id`)
    REFERENCES `efsdev`.`efs_company_master` (`id`),
  CONSTRAINT `FK_CFSRCV_CONSIGNID`
    FOREIGN KEY (`consignee_id`)
    REFERENCES `efsdev`.`efs_party_master` (`id`),
  CONSTRAINT `FK_CFSRCV_COUNTRYID`
    FOREIGN KEY (`country_id`)
    REFERENCES `efsdev`.`efs_country_master` (`id`),
  CONSTRAINT `FK_CFSRCV_LOCATIONID`
    FOREIGN KEY (`location_id`)
    REFERENCES `efsdev`.`efs_location_master` (`id`),
  CONSTRAINT `FK_CFSRCV_SERVICEID`
    FOREIGN KEY (`service_id`)
    REFERENCES `efsdev`.`efs_service` (`id`),
  CONSTRAINT `FK_CFSRCV_SHIPPERID`
    FOREIGN KEY (`shipper_id`)
    REFERENCES `efsdev`.`efs_party_master` (`id`),
  CONSTRAINT `FK_CFSRCV_TRANSPORTID`
    FOREIGN KEY (`transporter_id`)
    REFERENCES `efsdev`.`efs_party_master` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_cfs_attachment`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_cfs_attachment` (
  `id` BIGINT(20) NOT NULL,
  `file_attached` LONGBLOB NULL DEFAULT NULL,
  `file_content_type` VARCHAR(100) NULL DEFAULT NULL,
  `file_name` VARCHAR(100) NULL DEFAULT NULL,
  `ref_no` VARCHAR(100) NULL DEFAULT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  `cfs_receive_id` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `UK_CFSATTACH_REF` (`ref_no` ASC, `cfs_receive_id` ASC),
  INDEX `FK_CFSATTACH_CFSID` (`cfs_receive_id` ASC),
  CONSTRAINT `FK_CFSATTACH_CFSID`
    FOREIGN KEY (`cfs_receive_id`)
    REFERENCES `efsdev`.`efs_cfs_receive_entry` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_cfs_dimension`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_cfs_dimension` (
  `id` BIGINT(20) NOT NULL,
  `gross_weight` DOUBLE NULL DEFAULT NULL,
  `gross_weight_in_kg` DOUBLE NULL DEFAULT NULL,
  `height` INT(11) NULL DEFAULT NULL,
  `length` INT(11) NULL DEFAULT NULL,
  `no_of_pieces` INT(11) NULL DEFAULT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  `volume_weight` DOUBLE NULL DEFAULT NULL,
  `width` INT(11) NULL DEFAULT NULL,
  `cfs_receive_id` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `FK_CFSDIM_CFSID` (`cfs_receive_id` ASC),
  CONSTRAINT `FK_CFSDIM_CFSID`
    FOREIGN KEY (`cfs_receive_id`)
    REFERENCES `efsdev`.`efs_cfs_receive_entry` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_charge_edi_mapping`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_charge_edi_mapping` (
  `id` BIGINT(20) NOT NULL,
  `charge_code` VARCHAR(10) NOT NULL,
  `edi_subtype` VARCHAR(100) NULL DEFAULT NULL,
  `edi_type` VARCHAR(100) NOT NULL,
  `edi_value` VARCHAR(100) NOT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  `charge_id` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `UK_EDICHARGE_EDITYPE` (`charge_id` ASC, `edi_type` ASC),
  CONSTRAINT `FK_EDICHARGE_CHARGEID`
    FOREIGN KEY (`charge_id`)
    REFERENCES `efsdev`.`efs_charge_master` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_common_region_master`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_common_region_master` (
  `id` BIGINT(20) NOT NULL,
  `region_code` VARCHAR(10) NOT NULL,
  `region_name` VARCHAR(100) NOT NULL,
  `status` VARCHAR(10) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `UK_CMREGION_REGIONCODE` (`region_code` ASC),
  UNIQUE INDEX `UK_CMREGION_REGIONNAME` (`region_name` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_common_country_master`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_common_country_master` (
  `id` BIGINT(20) NOT NULL,
  `country_code` VARCHAR(10) NOT NULL,
  `country_name` VARCHAR(100) NOT NULL,
  `image` LONGBLOB NULL DEFAULT NULL,
  `locale` VARCHAR(10) NULL DEFAULT NULL,
  `region_code` VARCHAR(100) NULL DEFAULT NULL,
  `status` VARCHAR(10) NOT NULL,
  `region_id` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `UK_CMCOUNTRY_COUNTRYCODE` (`country_code` ASC),
  UNIQUE INDEX `UK_CMCOUNTRY_COUNTRYNAME` (`country_name` ASC),
  INDEX `FK_COMCOUNTRY_COMREGIONID` (`region_id` ASC),
  CONSTRAINT `FK_COMCOUNTRY_COMREGIONID`
    FOREIGN KEY (`region_id`)
    REFERENCES `efsdev`.`efs_common_region_master` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_common_state_province`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_common_state_province` (
  `id` BIGINT(20) NOT NULL,
  `country_code` VARCHAR(10) NOT NULL,
  `state_code` VARCHAR(10) NOT NULL,
  `state_name` VARCHAR(100) NOT NULL,
  `status` VARCHAR(10) NOT NULL,
  `country_id` BIGINT(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `UK_CMSTATEPROVINCE_STATECODE` (`state_code` ASC, `country_id` ASC),
  UNIQUE INDEX `UK_CMSTATEPROVINCE_STATENAME` (`state_name` ASC, `country_id` ASC),
  INDEX `FK_CMSTATEPROVINCE_COUNTRYID` (`country_id` ASC),
  CONSTRAINT `FK_CMSTATEPROVINCE_COUNTRYID`
    FOREIGN KEY (`country_id`)
    REFERENCES `efsdev`.`efs_common_country_master` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_common_city_master`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_common_city_master` (
  `id` BIGINT(20) NOT NULL,
  `city_code` VARCHAR(10) NOT NULL,
  `city_name` VARCHAR(100) NOT NULL,
  `country_code` VARCHAR(10) NULL DEFAULT NULL,
  `state_code` VARCHAR(10) NULL DEFAULT NULL,
  `status` VARCHAR(10) NOT NULL,
  `country_id` BIGINT(20) NULL DEFAULT NULL,
  `state_id` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `UK_CMCITY_CITYCODE` (`city_code` ASC, `state_id` ASC, `country_id` ASC),
  UNIQUE INDEX `UK_CMCITY_CITYNAME` (`city_name` ASC, `state_id` ASC, `country_id` ASC),
  INDEX `FK_CMCITY_COUNTRYID` (`country_id` ASC),
  INDEX `FK_CMCITY_STATEID` (`state_id` ASC),
  CONSTRAINT `FK_CMCITY_COUNTRYID`
    FOREIGN KEY (`country_id`)
    REFERENCES `efsdev`.`efs_common_country_master` (`id`),
  CONSTRAINT `FK_CMCITY_STATEID`
    FOREIGN KEY (`state_id`)
    REFERENCES `efsdev`.`efs_common_state_province` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_common_config_master`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_common_config_master` (
  `id` BIGINT(20) NOT NULL,
  `code` VARCHAR(255) NOT NULL,
  `description` VARCHAR(4000) NULL DEFAULT NULL,
  `value` VARCHAR(4000) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `UK_COM_CONFIGURATION_CODE` (`code` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_common_currency_master`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_common_currency_master` (
  `id` BIGINT(20) NOT NULL,
  `country_code` VARCHAR(10) NULL DEFAULT NULL,
  `currency_code` VARCHAR(10) NOT NULL,
  `currency_name` VARCHAR(100) NOT NULL,
  `decimal_point` VARCHAR(10) NOT NULL,
  `prefix` VARCHAR(30) NULL DEFAULT NULL,
  `status` VARCHAR(10) NOT NULL,
  `suffix` VARCHAR(30) NULL DEFAULT NULL,
  `country_id` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `UK_CMCURRENCY_CURRENCYCODE` (`currency_code` ASC),
  UNIQUE INDEX `UK_CMCURRENCY_CURRENCYNAME` (`currency_name` ASC),
  INDEX `FK_COM_CURRENCY_COUNTRYID` (`country_id` ASC),
  CONSTRAINT `FK_COM_CURRENCY_COUNTRYID`
    FOREIGN KEY (`country_id`)
    REFERENCES `efsdev`.`efs_common_country_master` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_common_doc_type_master`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_common_doc_type_master` (
  `id` BIGINT(20) NOT NULL,
  `document_type_code` VARCHAR(30) NOT NULL,
  `document_type_name` VARCHAR(100) NOT NULL,
  `status` VARCHAR(10) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `UK_COM_DOC_DOCTYPECODE` (`document_type_code` ASC),
  UNIQUE INDEX `UK_COM_DOC_DOCTYPENAME` (`document_type_name` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_common_email_request`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_common_email_request` (
  `id` BIGINT(20) NOT NULL,
  `email_bcc` VARCHAR(500) NULL DEFAULT NULL,
  `email_cc` VARCHAR(500) NULL DEFAULT NULL,
  `email_body` LONGTEXT NOT NULL,
  `email_type` VARCHAR(30) NOT NULL,
  `email_from` VARCHAR(100) NOT NULL,
  `email_from_password` VARCHAR(100) NOT NULL,
  `server_port` INT(11) NULL DEFAULT NULL,
  `smtp_server_address` VARCHAR(255) NULL DEFAULT NULL,
  `email_subject` VARCHAR(255) NOT NULL,
  `email_to` VARCHAR(500) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_common_email_template`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_common_email_template` (
  `id` BIGINT(20) NOT NULL,
  `bcc_email_address` VARCHAR(255) NULL DEFAULT NULL,
  `cc_email_address` VARCHAR(255) NULL DEFAULT NULL,
  `email_type` VARCHAR(255) NULL DEFAULT NULL,
  `from_email_id` VARCHAR(255) NULL DEFAULT NULL,
  `from_email_password` VARCHAR(255) NULL DEFAULT NULL,
  `port` INT(11) NULL DEFAULT NULL,
  `smtp_server_address` VARCHAR(255) NULL DEFAULT NULL,
  `subject` VARCHAR(255) NULL DEFAULT NULL,
  `template` LONGTEXT NULL DEFAULT NULL,
  `template_type` VARCHAR(255) NULL DEFAULT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_common_error_message`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_common_error_message` (
  `id` BIGINT(20) NOT NULL,
  `error_code` VARCHAR(20) NOT NULL,
  `error_desc` VARCHAR(1000) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `UK_ERRORMESSAGE_CODE` (`error_code` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_common_language_master`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_common_language_master` (
  `id` BIGINT(20) NOT NULL,
  `language_code` VARCHAR(20) NOT NULL,
  `language_name` VARCHAR(20) NOT NULL,
  `status` VARCHAR(10) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `UK_48uedsw4g51507djc2f8g884f` (`language_code` ASC),
  UNIQUE INDEX `UK_g1g79hcqfb0sauognyb3d04hu` (`language_name` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_common_reg_exp`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_common_reg_exp` (
  `id` BIGINT(20) NOT NULL,
  `java_reg_exp` VARCHAR(255) NOT NULL,
  `js_reg_exp` VARCHAR(255) NOT NULL,
  `reg_exp_name` VARCHAR(50) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `UK_COM_REGEXP_NAME` (`reg_exp_name` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_common_service_master`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_common_service_master` (
  `id` BIGINT(20) NOT NULL,
  `full_groupage` VARCHAR(10) NULL DEFAULT NULL,
  `import_export` VARCHAR(10) NOT NULL,
  `service_code` VARCHAR(10) NOT NULL,
  `service_name` VARCHAR(100) NOT NULL,
  `status` VARCHAR(10) NOT NULL,
  `transport_mode` VARCHAR(10) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `UK_CM_SERVICE_SERVICECODE` (`service_code` ASC),
  UNIQUE INDEX `UK_CM_SERVICE_SERVICENAME` (`service_name` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_common_time_zone_master`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_common_time_zone_master` (
  `id` BIGINT(20) NOT NULL,
  `actual_timezone` VARCHAR(100) NOT NULL,
  `display_timezone` VARCHAR(100) NOT NULL,
  `status` VARCHAR(10) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `UK_COM_TIMEZONE_DISPLAY` (`display_timezone` ASC),
  UNIQUE INDEX `UK_COM_TIMEZONE_ACTUAL` (`actual_timezone` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_consol_detail`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_consol_detail` (
  `id` BIGINT(20) NOT NULL,
  `document_received_date` DATETIME(6) NULL DEFAULT NULL,
  `local_serial` VARCHAR(255) NULL DEFAULT NULL,
  `arrival_code` VARCHAR(255) NULL DEFAULT NULL,
  `ata_date` DATETIME(6) NULL DEFAULT NULL,
  `ata_no` BIGINT(20) NULL DEFAULT NULL,
  `bl_released` VARCHAR(5) NULL DEFAULT NULL,
  `cargo_nature` VARCHAR(255) NULL DEFAULT NULL,
  `cargo_type_code` VARCHAR(255) NULL DEFAULT NULL,
  `carrier_brokerage_amount` DOUBLE NULL DEFAULT NULL,
  `carrier_brokerage_currency_id` TINYBLOB NULL DEFAULT NULL,
  `carrier_brokerage_currency` VARCHAR(10) NULL DEFAULT NULL,
  `carrier_brokerage_percentage` DOUBLE NULL DEFAULT NULL,
  `carrier_calculation_mode` VARCHAR(255) NULL DEFAULT NULL,
  `carrier_can_received_date` DATETIME(6) NULL DEFAULT NULL,
  `carrier_commission_percentage` DOUBLE NULL DEFAULT NULL,
  `carrier_do_date` DATETIME(6) NULL DEFAULT NULL,
  `carrier_do_issued_date` DATETIME(6) NULL DEFAULT NULL,
  `carrier_do_no` VARCHAR(255) NULL DEFAULT NULL,
  `carrier_incentive_percentage` DOUBLE NULL DEFAULT NULL,
  `charter_code` VARCHAR(255) NULL DEFAULT NULL,
  `cri_number` VARCHAR(255) NULL DEFAULT NULL,
  `customs_approval_no` VARCHAR(255) NULL DEFAULT NULL,
  `customs_job_date` DATETIME(6) NULL DEFAULT NULL,
  `customs_job_no` VARCHAR(255) NULL DEFAULT NULL,
  `export_declaration` VARCHAR(255) NULL DEFAULT NULL,
  `export_doc_send_date` DATETIME(6) NULL DEFAULT NULL,
  `igm_date` DATETIME(6) NULL DEFAULT NULL,
  `igm_no` VARCHAR(255) NULL DEFAULT NULL,
  `is_carrier_do_issued` VARCHAR(5) NULL DEFAULT NULL,
  `is_direct` VARCHAR(5) NULL DEFAULT NULL,
  `is_minimum` VARCHAR(5) NULL DEFAULT NULL,
  `job_status` VARCHAR(255) NULL DEFAULT NULL,
  `known_unknown_text` VARCHAR(255) NULL DEFAULT NULL,
  `lc_note` VARCHAR(255) NULL DEFAULT NULL,
  `line_do_handed_by` VARCHAR(255) NULL DEFAULT NULL,
  `line_do_handed_date_to_cfs` DATETIME(6) NULL DEFAULT NULL,
  `line_invoice_received` DATETIME(6) NULL DEFAULT NULL,
  `line_invoice_received_by` VARCHAR(255) NULL DEFAULT NULL,
  `manifest_sent_date_cfs` DATETIME(6) NULL DEFAULT NULL,
  `mark_no` VARCHAR(255) NULL DEFAULT NULL,
  `mbl_surrender_date` DATETIME(6) NULL DEFAULT NULL,
  `mrn` VARCHAR(255) NULL DEFAULT NULL,
  `mrn_date` DATETIME(6) NULL DEFAULT NULL,
  `mucr` VARCHAR(255) NULL DEFAULT NULL,
  `noc_date` DATETIME(6) NULL DEFAULT NULL,
  `noc_number` VARCHAR(255) NULL DEFAULT NULL,
  `note` VARCHAR(255) NULL DEFAULT NULL,
  `onward` VARCHAR(255) NULL DEFAULT NULL,
  `optional_shipping_info_1` VARCHAR(255) NULL DEFAULT NULL,
  `optional_shipping_info_2` VARCHAR(255) NULL DEFAULT NULL,
  `overseas_invoice_checked` VARCHAR(5) NULL DEFAULT NULL,
  `overseas_invoice_query_note` VARCHAR(255) NULL DEFAULT NULL,
  `previous_ccn_no` VARCHAR(255) NULL DEFAULT NULL,
  `rate_class` VARCHAR(1) NULL DEFAULT NULL,
  `recovery_amount` DOUBLE NULL DEFAULT NULL,
  `release_code` VARCHAR(255) NULL DEFAULT NULL,
  `rotation_date` DATETIME(6) NULL DEFAULT NULL,
  `rotation_number` VARCHAR(255) NULL DEFAULT NULL,
  `service_issue_note` VARCHAR(255) NULL DEFAULT NULL,
  `ship_calling_no` VARCHAR(255) NULL DEFAULT NULL,
  `shipping_bill_date` DATETIME(6) NULL DEFAULT NULL,
  `shipping_bill_no` VARCHAR(255) NULL DEFAULT NULL,
  `shipping_bill_type` VARCHAR(255) NULL DEFAULT NULL,
  `slpa_no` VARCHAR(255) NULL DEFAULT NULL,
  `spot_no` VARCHAR(255) NULL DEFAULT NULL,
  `subagent_calculation_mode` VARCHAR(255) NULL DEFAULT NULL,
  `subagent_commission_percentage` DOUBLE NULL DEFAULT NULL,
  `subagent_incentive_percentage` DOUBLE NULL DEFAULT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `transfer_of_ownership_date` DATETIME(6) NULL DEFAULT NULL,
  `transhipment_serial` VARCHAR(255) NULL DEFAULT NULL,
  `unstuff_place` VARCHAR(255) NULL DEFAULT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  `vessel_birth_terminal_code` VARCHAR(255) NULL DEFAULT NULL,
  `via_no` VARCHAR(255) NULL DEFAULT NULL,
  `charter_id` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `FK_CONSODETAILL_CHARTERID` (`charter_id` ASC),
  CONSTRAINT `FK_CONSODETAILL_CHARTERID`
    FOREIGN KEY (`charter_id`)
    REFERENCES `efsdev`.`efs_charter_master` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_doc_prefix_master`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_doc_prefix_master` (
  `id` BIGINT(20) NOT NULL,
  `add_fdc_to_doc` VARCHAR(5) NULL DEFAULT NULL,
  `add_por_to_doc` VARCHAR(5) NULL DEFAULT NULL,
  `doc_prefix` VARCHAR(20) NULL DEFAULT NULL,
  `doc_suffix` VARCHAR(20) NULL DEFAULT NULL,
  `doc_prefix_code` VARCHAR(10) NOT NULL,
  `doc_prefix_name` VARCHAR(100) NOT NULL,
  `formula` VARCHAR(255) NULL DEFAULT NULL,
  `status` VARCHAR(10) NOT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `UK_DOCPREFIIX_DOCPREFIXCODE` (`doc_prefix_code` ASC),
  UNIQUE INDEX `UK_DOCPREFIIX_DOCPREFIXNAME` (`doc_prefix_name` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_document_sub_detail`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_document_sub_detail` (
  `id` BIGINT(20) NOT NULL,
  `actual_sale_kg` DOUBLE NULL DEFAULT NULL,
  `ams_house_no` VARCHAR(255) NULL DEFAULT NULL,
  `badge_no` VARCHAR(255) NULL DEFAULT NULL,
  `booking_received_date` DATETIME(6) NULL DEFAULT NULL,
  `brokerage_hold_note` VARCHAR(255) NULL DEFAULT NULL,
  `caf_percentage` DOUBLE NULL DEFAULT NULL,
  `cargo_movement` VARCHAR(255) NULL DEFAULT NULL,
  `cc_percentage` DOUBLE NULL DEFAULT NULL,
  `ccn_date` DATETIME(6) NULL DEFAULT NULL,
  `ccn_no` VARCHAR(255) NULL DEFAULT NULL,
  `cd_no` VARCHAR(255) NULL DEFAULT NULL,
  `clearance_email` VARCHAR(255) NULL DEFAULT NULL,
  `co_loader_booking_no` VARCHAR(255) NULL DEFAULT NULL,
  `declared_sale_kg` DOUBLE NULL DEFAULT NULL,
  `demurrage_calc_date` DATETIME(6) NULL DEFAULT NULL,
  `demurrage_date` DATETIME(6) NULL DEFAULT NULL,
  `ducr_no` VARCHAR(255) NULL DEFAULT NULL,
  `ep_copy_dispatch_date` DATETIME(6) NULL DEFAULT NULL,
  `ep_copy_received_date` DATETIME(6) NULL DEFAULT NULL,
  `expected_cfs_arrival_code` VARCHAR(255) NULL DEFAULT NULL,
  `expected_do_date` DATETIME(6) NULL DEFAULT NULL,
  `expected_doc_received_date` DATETIME(6) NULL DEFAULT NULL,
  `expected_doc_send_date` DATETIME(6) NULL DEFAULT NULL,
  `fsc_hold_note` VARCHAR(255) NULL DEFAULT NULL,
  `hbl_type` VARCHAR(255) NULL DEFAULT NULL,
  `imo_temperature` VARCHAR(255) NULL DEFAULT NULL,
  `imported_code` VARCHAR(255) NULL DEFAULT NULL,
  `is_all_prepaid` VARCHAR(5) NULL DEFAULT NULL,
  `is_brokerage_hold` VARCHAR(5) NULL DEFAULT NULL,
  `is_customs_cleared_by_us` VARCHAR(5) NULL DEFAULT NULL,
  `is_fsc_hold` VARCHAR(5) NULL DEFAULT NULL,
  `it_approved_port` VARCHAR(255) NULL DEFAULT NULL,
  `it_no` VARCHAR(255) NULL DEFAULT NULL,
  `it_submitted_date` DATETIME(6) NULL DEFAULT NULL,
  `item_no` VARCHAR(255) NULL DEFAULT NULL,
  `known_unknown_text` VARCHAR(255) NULL DEFAULT NULL,
  `ligm_date` DATETIME(6) NULL DEFAULT NULL,
  `ligm_no` VARCHAR(255) NULL DEFAULT NULL,
  `optional_shipping_info_1` VARCHAR(255) NULL DEFAULT NULL,
  `optional_shipping_info_2` VARCHAR(255) NULL DEFAULT NULL,
  `release_code` VARCHAR(255) NULL DEFAULT NULL,
  `release_date` DATETIME(6) NULL DEFAULT NULL,
  `routed_by_salesman_email` VARCHAR(255) NULL DEFAULT NULL,
  `shiprite_asc_name` VARCHAR(255) NULL DEFAULT NULL,
  `shiprite_carrier_name` VARCHAR(255) NULL DEFAULT NULL,
  `shiprite_carrier_scac` VARCHAR(255) NULL DEFAULT NULL,
  `shiprite_extra_charge` DOUBLE NULL DEFAULT NULL,
  `shiprite_final_rate` DOUBLE NULL DEFAULT NULL,
  `shiprite_fsc` DOUBLE NULL DEFAULT NULL,
  `shiprite_pickup_address_line_1` VARCHAR(255) NULL DEFAULT NULL,
  `shiprite_pickup_address_line_2` VARCHAR(255) NULL DEFAULT NULL,
  `shiprite_pickup_address_line_3` VARCHAR(255) NULL DEFAULT NULL,
  `shiprite_special_note` VARCHAR(255) NULL DEFAULT NULL,
  `shiprite_total_charge` DOUBLE NULL DEFAULT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `tran_ccn_date` DATETIME(6) NULL DEFAULT NULL,
  `tran_ccn_no` VARCHAR(255) NULL DEFAULT NULL,
  `truck_handover_to_transporter` DATETIME(6) NULL DEFAULT NULL,
  `unique_consignment_no` VARCHAR(255) NULL DEFAULT NULL,
  `unstuff_code` VARCHAR(255) NULL DEFAULT NULL,
  `unstuff_date` DATETIME(6) NULL DEFAULT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  `wwa_booking_no` VARCHAR(255) NULL DEFAULT NULL,
  `wwa_customer_control_code` VARCHAR(255) NULL DEFAULT NULL,
  `it_approved_port_id` BIGINT(20) NULL DEFAULT NULL,
  `shiprite_carrier_id` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `FK_DOCSUBDETAIL_APPROVEPORTID` (`it_approved_port_id` ASC),
  INDEX `FK_DOCSUBDETAIL_SHIPCARRIEID` (`shiprite_carrier_id` ASC),
  CONSTRAINT `FK_DOCSUBDETAIL_APPROVEPORTID`
    FOREIGN KEY (`it_approved_port_id`)
    REFERENCES `efsdev`.`efs_port_master` (`id`),
  CONSTRAINT `FK_DOCSUBDETAIL_SHIPCARRIEID`
    FOREIGN KEY (`shiprite_carrier_id`)
    REFERENCES `efsdev`.`efs_carrier_master` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_consol_document`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_consol_document` (
  `id` BIGINT(20) NOT NULL,
  `accounting_information` VARCHAR(400) NULL DEFAULT NULL,
  `agent_code` VARCHAR(255) NULL DEFAULT NULL,
  `agent_manifest_name` VARCHAR(255) NULL DEFAULT NULL,
  `amount_of_insurance` VARCHAR(255) NULL DEFAULT NULL,
  `awb` VARCHAR(255) NULL DEFAULT NULL,
  `awbdistprintallow` VARCHAR(255) NULL DEFAULT NULL,
  `bl_received_date` DATETIME(6) NULL DEFAULT NULL,
  `bl_received_person_code` VARCHAR(30) NULL DEFAULT NULL,
  `booking_chargeable_unit` DOUBLE NULL DEFAULT NULL,
  `booking_gross_weight` DOUBLE NULL DEFAULT NULL,
  `booking_gross_wt_unit_code` VARCHAR(255) NULL DEFAULT NULL,
  `booking_gross_wt_unit_value` DOUBLE NULL DEFAULT NULL,
  `booking_volume` DOUBLE NULL DEFAULT NULL,
  `booking_volume_unit_code` VARCHAR(255) NULL DEFAULT NULL,
  `booking_volume_unit_value` DOUBLE NULL DEFAULT NULL,
  `booking_volume_weight` DOUBLE NULL DEFAULT NULL,
  `booking_volume_wt_unit_code` VARCHAR(255) NULL DEFAULT NULL,
  `booking_volume_wt_unit_value` DOUBLE NULL DEFAULT NULL,
  `carrier_do_no` VARCHAR(255) NULL DEFAULT NULL,
  `chargeble_weight` DOUBLE NULL DEFAULT NULL,
  `charges_at_destination` VARCHAR(255) NULL DEFAULT NULL,
  `charges_dest_currency` VARCHAR(255) NULL DEFAULT NULL,
  `chargescode` VARCHAR(255) NULL DEFAULT NULL,
  `coll_valuation_charge` VARCHAR(255) NULL DEFAULT NULL,
  `coll_weight_charge` VARCHAR(255) NULL DEFAULT NULL,
  `coll_tax` VARCHAR(255) NULL DEFAULT NULL,
  `commodity_code` VARCHAR(255) NULL DEFAULT NULL,
  `commodity_description` VARCHAR(400) NULL DEFAULT NULL,
  `commodity_item_no` VARCHAR(255) NULL DEFAULT NULL,
  `company_code` VARCHAR(100) NULL DEFAULT NULL,
  `consignee_code` VARCHAR(255) NULL DEFAULT NULL,
  `consignee_manifest_name` VARCHAR(255) NULL DEFAULT NULL,
  `consol_uid` VARCHAR(255) NOT NULL,
  `country_code` VARCHAR(10) NULL DEFAULT NULL,
  `currency_conversion_rates` VARCHAR(255) NULL DEFAULT NULL,
  `declared_value_carriage` VARCHAR(255) NULL DEFAULT NULL,
  `declared_value_customs` VARCHAR(255) NULL DEFAULT NULL,
  `dim_gross_weight` DOUBLE NULL DEFAULT NULL,
  `dim_gross_weight_in_pound` DOUBLE NULL DEFAULT NULL,
  `dim_no_of_pieces` BIGINT(20) NULL DEFAULT NULL,
  `dim_volume_weight` DOUBLE NULL DEFAULT NULL,
  `dim_volume_weight_in_pound` DOUBLE NULL DEFAULT NULL,
  `dimension_unit` VARCHAR(50) NOT NULL,
  `dimension_unit_value` DOUBLE NULL DEFAULT NULL,
  `diversion_contray` VARCHAR(255) NULL DEFAULT NULL,
  `doc_prefix_code` VARCHAR(255) NULL DEFAULT NULL,
  `document_no` VARCHAR(255) NOT NULL,
  `document_req_date` DATETIME(6) NOT NULL,
  `first_notify_code` VARCHAR(255) NULL DEFAULT NULL,
  `first_notify_manifest_name` VARCHAR(255) NULL DEFAULT NULL,
  `forwarder_code` VARCHAR(255) NULL DEFAULT NULL,
  `forwarder_manifest_name` VARCHAR(255) NULL DEFAULT NULL,
  `gross_weight` DOUBLE NULL DEFAULT NULL,
  `gross_weight_in_pound` DOUBLE NULL DEFAULT NULL,
  `handling_information` VARCHAR(400) NULL DEFAULT NULL,
  `hold_note` VARCHAR(400) NULL DEFAULT NULL,
  `hold_release_number` VARCHAR(30) NULL DEFAULT NULL,
  `hold_release_note` LONGTEXT NULL DEFAULT NULL,
  `igm_no` VARCHAR(30) NULL DEFAULT NULL,
  `is_agreed` VARCHAR(255) NOT NULL,
  `is_hold` VARCHAR(10) NULL DEFAULT NULL,
  `is_marks_no` VARCHAR(255) NOT NULL,
  `agent_sign` VARCHAR(255) NULL DEFAULT NULL,
  `doc_issuing_agent_code` VARCHAR(255) NULL DEFAULT NULL,
  `doc_issuing_agent_name` VARCHAR(255) NULL DEFAULT NULL,
  `item_no` VARCHAR(30) NULL DEFAULT NULL,
  `location_code` VARCHAR(100) NULL DEFAULT NULL,
  `marks_and_no` VARCHAR(400) NULL DEFAULT NULL,
  `master_do_no` VARCHAR(255) NULL DEFAULT NULL,
  `mawb_generated_by_code` VARCHAR(255) NULL DEFAULT NULL,
  `mawb_generated_date` DATETIME(6) NULL DEFAULT NULL,
  `mawb_issue_date` DATETIME(6) NULL DEFAULT NULL,
  `mawb_no` VARCHAR(30) NULL DEFAULT NULL,
  `no_of_pieces` BIGINT(20) NULL DEFAULT NULL,
  `note` VARCHAR(400) NULL DEFAULT NULL,
  `optional_ship_info` VARCHAR(255) NULL DEFAULT NULL,
  `optional_information` VARCHAR(255) NULL DEFAULT NULL,
  `othercharges_coll_agent` VARCHAR(255) NULL DEFAULT NULL,
  `othercharges_coll_carrier` VARCHAR(255) NULL DEFAULT NULL,
  `othercharges_pre_agent` VARCHAR(255) NULL DEFAULT NULL,
  `othercharges_pre_carrier` VARCHAR(255) NULL DEFAULT NULL,
  `others_collect` VARCHAR(255) NULL DEFAULT NULL,
  `others_prepaid` VARCHAR(255) NULL DEFAULT NULL,
  `pack_code` VARCHAR(255) NULL DEFAULT NULL,
  `pack_description` VARCHAR(400) NULL DEFAULT NULL,
  `pan_no` VARCHAR(255) NULL DEFAULT NULL,
  `pre_valuation_charge` VARCHAR(255) NULL DEFAULT NULL,
  `pre_weight_charge` VARCHAR(255) NULL DEFAULT NULL,
  `pre_tax` VARCHAR(255) NULL DEFAULT NULL,
  `rate_class` VARCHAR(2) NULL DEFAULT NULL,
  `rate_per_charge` DOUBLE NULL DEFAULT NULL,
  `route_no` VARCHAR(255) NULL DEFAULT NULL,
  `second_notify_code` VARCHAR(255) NULL DEFAULT NULL,
  `second_notify_manifest_name` VARCHAR(255) NULL DEFAULT NULL,
  `service_code` VARCHAR(255) NULL DEFAULT NULL,
  `shipper_code` VARCHAR(255) NULL DEFAULT NULL,
  `shipper_manifest_name` VARCHAR(255) NULL DEFAULT NULL,
  `shipper_sign` VARCHAR(255) NULL DEFAULT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `to_address` VARCHAR(255) NULL DEFAULT NULL,
  `total_collect` VARCHAR(255) NULL DEFAULT NULL,
  `total_charges` VARCHAR(255) NULL DEFAULT NULL,
  `total_prepaid` VARCHAR(255) NULL DEFAULT NULL,
  `trade` VARCHAR(255) NULL DEFAULT NULL,
  `transit_eta` DATETIME(6) NULL DEFAULT NULL,
  `transit_port` VARCHAR(255) NULL DEFAULT NULL,
  `transit_schedule_uid` VARCHAR(255) NULL DEFAULT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  `vessel_code` VARCHAR(255) NULL DEFAULT NULL,
  `vessel_id` BIGINT(20) NULL DEFAULT NULL,
  `volume` DOUBLE NULL DEFAULT NULL,
  `volume_weight` DOUBLE NULL DEFAULT NULL,
  `volume_weight_in_pound` DOUBLE NULL DEFAULT NULL,
  `wt_val_col` VARCHAR(255) NULL DEFAULT NULL,
  `wt_val_ppd` VARCHAR(255) NULL DEFAULT NULL,
  `agent_id` BIGINT(20) NULL DEFAULT NULL,
  `agent_address_id` BIGINT(20) NULL DEFAULT NULL,
  `bl_received_person_id` BIGINT(20) NULL DEFAULT NULL,
  `booking_gross_wt_unit_id` BIGINT(20) NULL DEFAULT NULL,
  `booking_volume_unit_id` BIGINT(20) NULL DEFAULT NULL,
  `booking_volume_wt_unit_id` BIGINT(20) NULL DEFAULT NULL,
  `commodity_id` BIGINT(20) NULL DEFAULT NULL,
  `company_id` BIGINT(20) NULL DEFAULT NULL,
  `consignee_id` BIGINT(20) NULL DEFAULT NULL,
  `consignee_address_id` BIGINT(20) NULL DEFAULT NULL,
  `country_id` BIGINT(20) NULL DEFAULT NULL,
  `doc_prefix_id` BIGINT(20) NULL DEFAULT NULL,
  `document_sub_detail` BIGINT(20) NULL DEFAULT NULL,
  `first_notify_id` BIGINT(20) NULL DEFAULT NULL,
  `first_notify_address_id` BIGINT(20) NULL DEFAULT NULL,
  `forwarder_id` BIGINT(20) NULL DEFAULT NULL,
  `forwarder_address_id` BIGINT(20) NULL DEFAULT NULL,
  `doc_issuing_agent_id` BIGINT(20) NULL DEFAULT NULL,
  `doc_issuing_agent_add_id` BIGINT(20) NULL DEFAULT NULL,
  `location_id` BIGINT(20) NULL DEFAULT NULL,
  `mawb_generated_by` BIGINT(20) NULL DEFAULT NULL,
  `pack_id` BIGINT(20) NULL DEFAULT NULL,
  `pickup_delivery_id` BIGINT(20) NULL DEFAULT NULL,
  `second_notify_id` BIGINT(20) NULL DEFAULT NULL,
  `second_notify_address_id` BIGINT(20) NULL DEFAULT NULL,
  `shipper_id` BIGINT(20) NULL DEFAULT NULL,
  `shipper_address_id` BIGINT(20) NULL DEFAULT NULL,
  `transit_port_id` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `UK_CONDOCUMENT_DOCUMENTNO` (`document_no` ASC),
  INDEX `IDX_CONSOLDOC_MAWB` (`mawb_no` ASC),
  INDEX `FK_CONSLDOC_AGENTID` (`agent_id` ASC),
  INDEX `FK_CONSLDOC_AGENTADDID` (`agent_address_id` ASC),
  INDEX `FK_CONSLDOC_BLRECEMPID` (`bl_received_person_id` ASC),
  INDEX `FK_CONSLDOC_BOOKGROSSWGTID` (`booking_gross_wt_unit_id` ASC),
  INDEX `FK_CONSLDOC_BOOKVOLUMEID` (`booking_volume_unit_id` ASC),
  INDEX `FK_CONSLDOC_BOOKVOLWGTID` (`booking_volume_wt_unit_id` ASC),
  INDEX `FK_CONSLDOC_COMMODITYID` (`commodity_id` ASC),
  INDEX `FK_CONSLDOC_COMPANYID` (`company_id` ASC),
  INDEX `FK_CONSLDOC_CONSIGNEEID` (`consignee_id` ASC),
  INDEX `FK_CONSLDOC_CONSIGNADDID` (`consignee_address_id` ASC),
  INDEX `FK_CONSLDOC_COUNTRYID` (`country_id` ASC),
  INDEX `FK_CONSLDOC_DOCPREFIXID` (`doc_prefix_id` ASC),
  INDEX `FK_CONSLDOC_DOCSUBDETAILID` (`document_sub_detail` ASC),
  INDEX `FK_CONSLDOC_FIRSTNOTIFYID` (`first_notify_id` ASC),
  INDEX `FK_CONSLDOC_FIRNOTADDID` (`first_notify_address_id` ASC),
  INDEX `FK_CONSLDOC_FORWARDID` (`forwarder_id` ASC),
  INDEX `FK_CONSLDOC_FORWARDADDID` (`forwarder_address_id` ASC),
  INDEX `FK_CONSLDOC_ISSAGENTID` (`doc_issuing_agent_id` ASC),
  INDEX `FK_CONSLDOC_ISSAGENTADDID` (`doc_issuing_agent_add_id` ASC),
  INDEX `FK_CONSLDOC_LOCATIONID` (`location_id` ASC),
  INDEX `FK_CONSLDOC_MAWBEMPID` (`mawb_generated_by` ASC),
  INDEX `FK_CONSLDOC_PACKID` (`pack_id` ASC),
  INDEX `FK_CONSLDOC_PICKDELID` (`pickup_delivery_id` ASC),
  INDEX `FK_CONSLDOC_SECONDNOTIFYID` (`second_notify_id` ASC),
  INDEX `FK_CONSLDOC_SECNOTADDID` (`second_notify_address_id` ASC),
  INDEX `FK_CONSLDOC_SHIPPERID` (`shipper_id` ASC),
  INDEX `FK_CONSLDOC_SHIPPERADDID` (`shipper_address_id` ASC),
  INDEX `FK_CONSLDOC_TRANSITPORTID` (`transit_port_id` ASC),
  CONSTRAINT `FK_CONSLDOC_AGENTADDID`
    FOREIGN KEY (`agent_address_id`)
    REFERENCES `efsdev`.`efs_address_master` (`id`),
  CONSTRAINT `FK_CONSLDOC_AGENTID`
    FOREIGN KEY (`agent_id`)
    REFERENCES `efsdev`.`efs_party_master` (`id`),
  CONSTRAINT `FK_CONSLDOC_BLRECEMPID`
    FOREIGN KEY (`bl_received_person_id`)
    REFERENCES `efsdev`.`efs_employee_master` (`id`),
  CONSTRAINT `FK_CONSLDOC_BOOKGROSSWGTID`
    FOREIGN KEY (`booking_gross_wt_unit_id`)
    REFERENCES `efsdev`.`efs_unit_master` (`id`),
  CONSTRAINT `FK_CONSLDOC_BOOKVOLUMEID`
    FOREIGN KEY (`booking_volume_unit_id`)
    REFERENCES `efsdev`.`efs_unit_master` (`id`),
  CONSTRAINT `FK_CONSLDOC_BOOKVOLWGTID`
    FOREIGN KEY (`booking_volume_wt_unit_id`)
    REFERENCES `efsdev`.`efs_unit_master` (`id`),
  CONSTRAINT `FK_CONSLDOC_COMMODITYID`
    FOREIGN KEY (`commodity_id`)
    REFERENCES `efsdev`.`efs_hs_code_master` (`id`),
  CONSTRAINT `FK_CONSLDOC_COMPANYID`
    FOREIGN KEY (`company_id`)
    REFERENCES `efsdev`.`efs_company_master` (`id`),
  CONSTRAINT `FK_CONSLDOC_CONSIGNADDID`
    FOREIGN KEY (`consignee_address_id`)
    REFERENCES `efsdev`.`efs_address_master` (`id`),
  CONSTRAINT `FK_CONSLDOC_CONSIGNEEID`
    FOREIGN KEY (`consignee_id`)
    REFERENCES `efsdev`.`efs_party_master` (`id`),
  CONSTRAINT `FK_CONSLDOC_COUNTRYID`
    FOREIGN KEY (`country_id`)
    REFERENCES `efsdev`.`efs_country_master` (`id`),
  CONSTRAINT `FK_CONSLDOC_DOCPREFIXID`
    FOREIGN KEY (`doc_prefix_id`)
    REFERENCES `efsdev`.`efs_doc_prefix_master` (`id`),
  CONSTRAINT `FK_CONSLDOC_DOCSUBDETAILID`
    FOREIGN KEY (`document_sub_detail`)
    REFERENCES `efsdev`.`efs_document_sub_detail` (`id`),
  CONSTRAINT `FK_CONSLDOC_FIRNOTADDID`
    FOREIGN KEY (`first_notify_address_id`)
    REFERENCES `efsdev`.`efs_address_master` (`id`),
  CONSTRAINT `FK_CONSLDOC_FIRSTNOTIFYID`
    FOREIGN KEY (`first_notify_id`)
    REFERENCES `efsdev`.`efs_party_master` (`id`),
  CONSTRAINT `FK_CONSLDOC_FORWARDADDID`
    FOREIGN KEY (`forwarder_address_id`)
    REFERENCES `efsdev`.`efs_address_master` (`id`),
  CONSTRAINT `FK_CONSLDOC_FORWARDID`
    FOREIGN KEY (`forwarder_id`)
    REFERENCES `efsdev`.`efs_party_master` (`id`),
  CONSTRAINT `FK_CONSLDOC_ISSAGENTADDID`
    FOREIGN KEY (`doc_issuing_agent_add_id`)
    REFERENCES `efsdev`.`efs_address_master` (`id`),
  CONSTRAINT `FK_CONSLDOC_ISSAGENTID`
    FOREIGN KEY (`doc_issuing_agent_id`)
    REFERENCES `efsdev`.`efs_party_master` (`id`),
  CONSTRAINT `FK_CONSLDOC_LOCATIONID`
    FOREIGN KEY (`location_id`)
    REFERENCES `efsdev`.`efs_location_master` (`id`),
  CONSTRAINT `FK_CONSLDOC_MAWBEMPID`
    FOREIGN KEY (`mawb_generated_by`)
    REFERENCES `efsdev`.`efs_employee_master` (`id`),
  CONSTRAINT `FK_CONSLDOC_PACKID`
    FOREIGN KEY (`pack_id`)
    REFERENCES `efsdev`.`efs_pack_master` (`id`),
  CONSTRAINT `FK_CONSLDOC_PICKDELID`
    FOREIGN KEY (`pickup_delivery_id`)
    REFERENCES `efsdev`.`efs_pickup_delivery_point` (`id`),
  CONSTRAINT `FK_CONSLDOC_SECNOTADDID`
    FOREIGN KEY (`second_notify_address_id`)
    REFERENCES `efsdev`.`efs_address_master` (`id`),
  CONSTRAINT `FK_CONSLDOC_SECONDNOTIFYID`
    FOREIGN KEY (`second_notify_id`)
    REFERENCES `efsdev`.`efs_party_master` (`id`),
  CONSTRAINT `FK_CONSLDOC_SHIPPERADDID`
    FOREIGN KEY (`shipper_address_id`)
    REFERENCES `efsdev`.`efs_address_master` (`id`),
  CONSTRAINT `FK_CONSLDOC_SHIPPERID`
    FOREIGN KEY (`shipper_id`)
    REFERENCES `efsdev`.`efs_party_master` (`id`),
  CONSTRAINT `FK_CONSLDOC_TRANSITPORTID`
    FOREIGN KEY (`transit_port_id`)
    REFERENCES `efsdev`.`efs_port_master` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_service_mail_trigger`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_service_mail_trigger` (
  `id` BIGINT(20) NOT NULL,
  `is_cargo_loaded` VARCHAR(10) NULL DEFAULT NULL,
  `is_cargo_loaded_mail_trigger` VARCHAR(10) NULL DEFAULT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_consol_signoff`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_consol_signoff` (
  `id` BIGINT(20) NOT NULL,
  `comment_code` VARCHAR(255) NULL DEFAULT NULL,
  `consol_uid` VARCHAR(255) NULL DEFAULT NULL,
  `description` VARCHAR(4000) NULL DEFAULT NULL,
  `is_sign_off` VARCHAR(10) NULL DEFAULT NULL,
  `service_uid` VARCHAR(255) NULL DEFAULT NULL,
  `shipment_uid` VARCHAR(255) NULL DEFAULT NULL,
  `signoff_by_code` VARCHAR(255) NULL DEFAULT NULL,
  `signoff_date` DATETIME(6) NULL DEFAULT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  `comment_id` BIGINT(20) NULL DEFAULT NULL,
  `signoff_by` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `FK_CONSOLSIGNOFF_COMMENT` (`comment_id` ASC),
  INDEX `FK_CONSOLSIGNOFF_BY` (`signoff_by` ASC),
  CONSTRAINT `FK_CONSOLSIGNOFF_BY`
    FOREIGN KEY (`signoff_by`)
    REFERENCES `efsdev`.`efs_employee_master` (`id`),
  CONSTRAINT `FK_CONSOLSIGNOFF_COMMENT`
    FOREIGN KEY (`comment_id`)
    REFERENCES `efsdev`.`efs_comment_master` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_consol`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_consol` (
  `id` BIGINT(20) NOT NULL,
  `accounting_information` VARCHAR(255) NULL DEFAULT NULL,
  `agent_code` VARCHAR(255) NULL DEFAULT NULL,
  `agent_manifest_name` VARCHAR(255) NULL DEFAULT NULL,
  `ata` DATETIME(6) NULL DEFAULT NULL,
  `atd` DATETIME(6) NULL DEFAULT NULL,
  `carrier_agent_address_line_1` VARCHAR(255) NULL DEFAULT NULL,
  `carrier_agent_address_line_2` VARCHAR(255) NULL DEFAULT NULL,
  `carrier_agent_address_line_3` VARCHAR(255) NULL DEFAULT NULL,
  `carrier_agent_code` VARCHAR(255) NULL DEFAULT NULL,
  `carrier_agent_manifest_name` VARCHAR(255) NULL DEFAULT NULL,
  `carrier_code` VARCHAR(255) NULL DEFAULT NULL,
  `co_loader_code` VARCHAR(255) NULL DEFAULT NULL,
  `commodity_description` VARCHAR(255) NULL DEFAULT NULL,
  `company_code` VARCHAR(10) NOT NULL,
  `consol_uid` VARCHAR(100) NOT NULL,
  `country_code` VARCHAR(10) NOT NULL,
  `created_by_code` VARCHAR(30) NOT NULL,
  `currency_code` VARCHAR(10) NULL DEFAULT NULL,
  `currency_rate` DOUBLE NULL DEFAULT NULL,
  `destination` VARCHAR(255) NOT NULL,
  `direct_shipment` VARCHAR(5) NULL DEFAULT NULL,
  `doc_issue_agent_addr_line_1` VARCHAR(255) NULL DEFAULT NULL,
  `doc_issue_agent_addr_line_2` VARCHAR(255) NULL DEFAULT NULL,
  `doc_issue_agent_addr_line_3` VARCHAR(255) NULL DEFAULT NULL,
  `document_issuing_agent_code` VARCHAR(255) NULL DEFAULT NULL,
  `document_issuing_agent_name` VARCHAR(255) NULL DEFAULT NULL,
  `eta` DATETIME(6) NULL DEFAULT NULL,
  `etd` DATETIME(6) NULL DEFAULT NULL,
  `first_carrier_code` VARCHAR(255) NULL DEFAULT NULL,
  `first_notify_address_line_1` VARCHAR(255) NULL DEFAULT NULL,
  `first_notify_address_line_2` VARCHAR(255) NULL DEFAULT NULL,
  `first_notify_address_line_3` VARCHAR(255) NULL DEFAULT NULL,
  `first_notify_code` VARCHAR(255) NULL DEFAULT NULL,
  `first_notify_manifest_name` VARCHAR(255) NULL DEFAULT NULL,
  `carrier_no` VARCHAR(255) NULL DEFAULT NULL,
  `handling_address_line_1` VARCHAR(255) NULL DEFAULT NULL,
  `handling_address_line_2` VARCHAR(255) NULL DEFAULT NULL,
  `handling_address_line_3` VARCHAR(255) NULL DEFAULT NULL,
  `handling_agent_code` VARCHAR(255) NULL DEFAULT NULL,
  `handling_agent_manifest_name` VARCHAR(255) NULL DEFAULT NULL,
  `handling_information` VARCHAR(255) NULL DEFAULT NULL,
  `iata_account_code` VARCHAR(255) NULL DEFAULT NULL,
  `iata_rate` DOUBLE NULL DEFAULT NULL,
  `internal_note` LONGTEXT NULL DEFAULT NULL,
  `is_co_loader` VARCHAR(5) NULL DEFAULT NULL,
  `is_job_completed` VARCHAR(5) NOT NULL,
  `is_line_do_issued` VARCHAR(5) NULL DEFAULT NULL,
  `is_x_ray` VARCHAR(5) NULL DEFAULT NULL,
  `job_status` VARCHAR(30) NOT NULL,
  `known_shipper` VARCHAR(255) NULL DEFAULT NULL,
  `local_currency_code` VARCHAR(10) NULL DEFAULT NULL,
  `location_code` VARCHAR(10) NOT NULL,
  `master_date` DATETIME(6) NOT NULL,
  `mis_job_close_date` DATETIME(6) NULL DEFAULT NULL,
  `mis_job_close_status` VARCHAR(255) NULL DEFAULT NULL,
  `mis_job_close_user` VARCHAR(255) NULL DEFAULT NULL,
  `notes` LONGTEXT NULL DEFAULT NULL,
  `origin` VARCHAR(255) NOT NULL,
  `pod` VARCHAR(255) NOT NULL,
  `pol` VARCHAR(255) NOT NULL,
  `freight_term` VARCHAR(10) NOT NULL,
  `routed_agent_code` VARCHAR(255) NULL DEFAULT NULL,
  `salesman_code` VARCHAR(255) NULL DEFAULT NULL,
  `schedule_uid` VARCHAR(255) NULL DEFAULT NULL,
  `service_code` VARCHAR(10) NOT NULL,
  `service_type_code` VARCHAR(255) NULL DEFAULT NULL,
  `shc` VARCHAR(255) NULL DEFAULT NULL,
  `shipped_on_board_date` DATETIME(6) NULL DEFAULT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `total_amount` DOUBLE NULL DEFAULT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  `vessel_code` VARCHAR(255) NULL DEFAULT NULL,
  `vessel_id` BIGINT(20) NULL DEFAULT NULL,
  `volume_unit_code` VARCHAR(255) NULL DEFAULT NULL,
  `weight_unit_code` VARCHAR(255) NULL DEFAULT NULL,
  `who_routed` VARCHAR(255) NULL DEFAULT NULL,
  `win_ref_id` BIGINT(20) NULL DEFAULT NULL,
  `win_eawb` VARCHAR(255) NULL DEFAULT NULL,
  `win_error_code` VARCHAR(255) NULL DEFAULT NULL,
  `win_error_message` VARCHAR(500) NULL DEFAULT NULL,
  `win_status` VARCHAR(255) NULL DEFAULT NULL,
  `win_with_paper` VARCHAR(255) NULL DEFAULT NULL,
  `agent_id` BIGINT(20) NULL DEFAULT NULL,
  `agent_address_id` BIGINT(20) NULL DEFAULT NULL,
  `airline_edi_id` BIGINT(20) NULL DEFAULT NULL,
  `carrier_id` BIGINT(20) NULL DEFAULT NULL,
  `carrier_agent_id` BIGINT(20) NULL DEFAULT NULL,
  `co_loader_id` BIGINT(20) NULL DEFAULT NULL,
  `company_id` BIGINT(20) NOT NULL,
  `consol_detail_id` BIGINT(20) NULL DEFAULT NULL,
  `document_id` BIGINT(20) NULL DEFAULT NULL,
  `sign_off_id` BIGINT(20) NULL DEFAULT NULL,
  `country_id` BIGINT(20) NOT NULL,
  `created_by` BIGINT(20) NOT NULL,
  `currency_id` BIGINT(20) NULL DEFAULT NULL,
  `destination_id` BIGINT(20) NOT NULL,
  `document_issuing_agent_id` BIGINT(20) NULL DEFAULT NULL,
  `first_carrier_id` BIGINT(20) NULL DEFAULT NULL,
  `first_notify_id` BIGINT(20) NULL DEFAULT NULL,
  `handling_agent_id` BIGINT(20) NULL DEFAULT NULL,
  `local_currency_id` BIGINT(20) NULL DEFAULT NULL,
  `location_id` BIGINT(20) NOT NULL,
  `origin_id` BIGINT(20) NOT NULL,
  `pickup_delivery_id` BIGINT(20) NULL DEFAULT NULL,
  `pod_id` BIGINT(20) NOT NULL,
  `pol_id` BIGINT(20) NOT NULL,
  `routed_agent_id` BIGINT(20) NULL DEFAULT NULL,
  `routed_salesman_id` BIGINT(20) NULL DEFAULT NULL,
  `master_mail_trigger` BIGINT(20) NULL DEFAULT NULL,
  `service_id` BIGINT(20) NOT NULL,
  `volume_unit` BIGINT(20) NULL DEFAULT NULL,
  `weight_unit` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `UK_CONSOL_CONSOLUID` (`consol_uid` ASC),
  INDEX `IDX_CONSOL_SERVICEID` (`service_id` ASC),
  INDEX `IDX_CONSOL_AGENTID` (`agent_id` ASC),
  INDEX `IDX_CONSOL_DOCID` (`document_id` ASC),
  INDEX `IDX_CONSOL_MASTDATE` (`master_date` ASC),
  INDEX `IDX_CONSOL_ORIGINID` (`origin_id` ASC),
  INDEX `IDX_CONSOL_DESTID` (`destination_id` ASC),
  INDEX `IDX_CONSOL_CARRIERID` (`carrier_id` ASC),
  INDEX `IDX_CONSOL_ETD` (`etd` ASC),
  INDEX `IDX_CONSOL_ETA` (`eta` ASC),
  INDEX `IDX_CONSOL_CREATID` (`created_by` ASC),
  INDEX `IDX_CONSOL_FRTTERM` (`freight_term` ASC),
  INDEX `IDX_CONSOL_JOBSTATUS` (`job_status` ASC),
  INDEX `FK_CONSOL_AGENTADDID` (`agent_address_id` ASC),
  INDEX `FK_CONSOL_EDI` (`airline_edi_id` ASC),
  INDEX `FK_CONSOL_CARRIERAGENTID` (`carrier_agent_id` ASC),
  INDEX `FK_CONSOL_COLOADERID` (`co_loader_id` ASC),
  INDEX `FK_CONSOL_COMPANYID` (`company_id` ASC),
  INDEX `FK_CONSOL_CONSOLDETAILID` (`consol_detail_id` ASC),
  INDEX `FK_CONSOL_SIGNOFFID` (`sign_off_id` ASC),
  INDEX `FK_CONSOL_COUNTRYID` (`country_id` ASC),
  INDEX `FK_CONSOL_CURRENCYCODE` (`currency_id` ASC),
  INDEX `FK_CONSOL_DOCISSUEAGENTID` (`document_issuing_agent_id` ASC),
  INDEX `FK_CONSOL_FIRSTCARRIERID` (`first_carrier_id` ASC),
  INDEX `FK_CONSOL_FIRSTNOTIFYID` (`first_notify_id` ASC),
  INDEX `FK_CONSOL_HANDLINGAGENTID` (`handling_agent_id` ASC),
  INDEX `FK_CONSOL_LOCCURR` (`local_currency_id` ASC),
  INDEX `FK_CONSOL_LOCATIONID` (`location_id` ASC),
  INDEX `FK_CONSOL_PICKDELID` (`pickup_delivery_id` ASC),
  INDEX `FK_CONSOL_POD` (`pod_id` ASC),
  INDEX `FK_CONSOL_POL` (`pol_id` ASC),
  INDEX `FK_CONSOL_ROUTED_AGENTID` (`routed_agent_id` ASC),
  INDEX `FK_CONSOL_SALEMANID` (`routed_salesman_id` ASC),
  INDEX `FK_CONSOL_SERMAILTRI` (`master_mail_trigger` ASC),
  INDEX `FK_CONSOL_VOLUMEUNIT` (`volume_unit` ASC),
  INDEX `FK_CONSOL_WEIGHTUNIT` (`weight_unit` ASC),
  CONSTRAINT `FK_CONSOL_AGENTADDID`
    FOREIGN KEY (`agent_address_id`)
    REFERENCES `efsdev`.`efs_address_master` (`id`),
  CONSTRAINT `FK_CONSOL_AGENTID`
    FOREIGN KEY (`agent_id`)
    REFERENCES `efsdev`.`efs_party_master` (`id`),
  CONSTRAINT `FK_CONSOL_CARRIERAGENTID`
    FOREIGN KEY (`carrier_agent_id`)
    REFERENCES `efsdev`.`efs_party_master` (`id`),
  CONSTRAINT `FK_CONSOL_CARRIERID`
    FOREIGN KEY (`carrier_id`)
    REFERENCES `efsdev`.`efs_carrier_master` (`id`),
  CONSTRAINT `FK_CONSOL_COLOADERID`
    FOREIGN KEY (`co_loader_id`)
    REFERENCES `efsdev`.`efs_party_master` (`id`),
  CONSTRAINT `FK_CONSOL_COMPANYID`
    FOREIGN KEY (`company_id`)
    REFERENCES `efsdev`.`efs_company_master` (`id`),
  CONSTRAINT `FK_CONSOL_CONSOLDETAILID`
    FOREIGN KEY (`consol_detail_id`)
    REFERENCES `efsdev`.`efs_consol_detail` (`id`),
  CONSTRAINT `FK_CONSOL_COUNTRYID`
    FOREIGN KEY (`country_id`)
    REFERENCES `efsdev`.`efs_country_master` (`id`),
  CONSTRAINT `FK_CONSOL_CREATEDBY`
    FOREIGN KEY (`created_by`)
    REFERENCES `efsdev`.`efs_employee_master` (`id`),
  CONSTRAINT `FK_CONSOL_CURRENCYCODE`
    FOREIGN KEY (`currency_id`)
    REFERENCES `efsdev`.`efs_currency_master` (`id`),
  CONSTRAINT `FK_CONSOL_DESTINATION`
    FOREIGN KEY (`destination_id`)
    REFERENCES `efsdev`.`efs_port_master` (`id`),
  CONSTRAINT `FK_CONSOL_DOCID`
    FOREIGN KEY (`document_id`)
    REFERENCES `efsdev`.`efs_consol_document` (`id`),
  CONSTRAINT `FK_CONSOL_DOCISSUEAGENTID`
    FOREIGN KEY (`document_issuing_agent_id`)
    REFERENCES `efsdev`.`efs_party_master` (`id`),
  CONSTRAINT `FK_CONSOL_EDI`
    FOREIGN KEY (`airline_edi_id`)
    REFERENCES `efsdev`.`efs_airline_edi` (`id`),
  CONSTRAINT `FK_CONSOL_FIRSTCARRIERID`
    FOREIGN KEY (`first_carrier_id`)
    REFERENCES `efsdev`.`efs_carrier_master` (`id`),
  CONSTRAINT `FK_CONSOL_FIRSTNOTIFYID`
    FOREIGN KEY (`first_notify_id`)
    REFERENCES `efsdev`.`efs_party_master` (`id`),
  CONSTRAINT `FK_CONSOL_HANDLINGAGENTID`
    FOREIGN KEY (`handling_agent_id`)
    REFERENCES `efsdev`.`efs_party_master` (`id`),
  CONSTRAINT `FK_CONSOL_LOCATIONID`
    FOREIGN KEY (`location_id`)
    REFERENCES `efsdev`.`efs_location_master` (`id`),
  CONSTRAINT `FK_CONSOL_LOCCURR`
    FOREIGN KEY (`local_currency_id`)
    REFERENCES `efsdev`.`efs_currency_master` (`id`),
  CONSTRAINT `FK_CONSOL_ORIGIN`
    FOREIGN KEY (`origin_id`)
    REFERENCES `efsdev`.`efs_port_master` (`id`),
  CONSTRAINT `FK_CONSOL_PICKDELID`
    FOREIGN KEY (`pickup_delivery_id`)
    REFERENCES `efsdev`.`efs_pickup_delivery_point` (`id`),
  CONSTRAINT `FK_CONSOL_POD`
    FOREIGN KEY (`pod_id`)
    REFERENCES `efsdev`.`efs_port_master` (`id`),
  CONSTRAINT `FK_CONSOL_POL`
    FOREIGN KEY (`pol_id`)
    REFERENCES `efsdev`.`efs_port_master` (`id`),
  CONSTRAINT `FK_CONSOL_ROUTED_AGENTID`
    FOREIGN KEY (`routed_agent_id`)
    REFERENCES `efsdev`.`efs_party_master` (`id`),
  CONSTRAINT `FK_CONSOL_SALEMANID`
    FOREIGN KEY (`routed_salesman_id`)
    REFERENCES `efsdev`.`efs_employee_master` (`id`),
  CONSTRAINT `FK_CONSOL_SERMAILTRI`
    FOREIGN KEY (`master_mail_trigger`)
    REFERENCES `efsdev`.`efs_service_mail_trigger` (`id`),
  CONSTRAINT `FK_CONSOL_SERVICEID`
    FOREIGN KEY (`service_id`)
    REFERENCES `efsdev`.`efs_service_master` (`id`),
  CONSTRAINT `FK_CONSOL_SIGNOFFID`
    FOREIGN KEY (`sign_off_id`)
    REFERENCES `efsdev`.`efs_consol_signoff` (`id`),
  CONSTRAINT `FK_CONSOL_VOLUMEUNIT`
    FOREIGN KEY (`volume_unit`)
    REFERENCES `efsdev`.`efs_unit_master` (`id`),
  CONSTRAINT `FK_CONSOL_WEIGHTUNIT`
    FOREIGN KEY (`weight_unit`)
    REFERENCES `efsdev`.`efs_unit_master` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_document_master`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_document_master` (
  `id` BIGINT(20) NOT NULL,
  `document_code` VARCHAR(10) NULL DEFAULT NULL,
  `document_name` VARCHAR(100) NULL DEFAULT NULL,
  `is_protected` VARCHAR(255) NULL DEFAULT NULL,
  `status` VARCHAR(10) NOT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `UK_DOCUMENT_DOCUMENTCODE` (`document_code` ASC),
  UNIQUE INDEX `UK_DOCUMENT_DOCUMENTNAME` (`document_name` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_consol_attachment`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_consol_attachment` (
  `id` BIGINT(20) NOT NULL,
  `file_content` LONGBLOB NULL DEFAULT NULL,
  `file_content_type` VARCHAR(100) NULL DEFAULT NULL,
  `file_name` VARCHAR(100) NULL DEFAULT NULL,
  `is_customs` VARCHAR(255) NULL DEFAULT NULL,
  `is_protected` VARCHAR(255) NULL DEFAULT NULL,
  `ref_no` VARCHAR(100) NULL DEFAULT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  `consol_id` BIGINT(20) NULL DEFAULT NULL,
  `document_id` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `UK_CONSLATTCH_REF` (`ref_no` ASC, `consol_id` ASC),
  INDEX `FK_CONSOL_ATT_CONSOLID` (`consol_id` ASC),
  INDEX `FK_CONSOL_ATT_DOCUMENTID` (`document_id` ASC),
  CONSTRAINT `FK_CONSOL_ATT_CONSOLID`
    FOREIGN KEY (`consol_id`)
    REFERENCES `efsdev`.`efs_consol` (`id`),
  CONSTRAINT `FK_CONSOL_ATT_DOCUMENTID`
    FOREIGN KEY (`document_id`)
    REFERENCES `efsdev`.`efs_document_master` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_consol_connection`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_consol_connection` (
  `id` BIGINT(20) NOT NULL,
  `carrier_code` VARCHAR(255) NULL DEFAULT NULL,
  `connection_status` VARCHAR(30) NOT NULL,
  `eta` DATETIME(6) NOT NULL,
  `etd` DATETIME(6) NOT NULL,
  `flight_voyage_no` VARCHAR(255) NOT NULL,
  `move` VARCHAR(10) NOT NULL,
  `pod` VARCHAR(255) NOT NULL,
  `pol` VARCHAR(255) NOT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  `carrier_id` BIGINT(20) NULL DEFAULT NULL,
  `consol_id` BIGINT(20) NULL DEFAULT NULL,
  `pod_id` BIGINT(20) NOT NULL,
  `pol_id` BIGINT(20) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `FK_CONSOLCONN_CARRIERID` (`carrier_id` ASC),
  INDEX `FK_CONSOLCONN_CONSOL` (`consol_id` ASC),
  INDEX `FK_CONSOLCONN_PODID` (`pod_id` ASC),
  INDEX `FK_CONSOLCONN_POLID` (`pol_id` ASC),
  CONSTRAINT `FK_CONSOLCONN_CARRIERID`
    FOREIGN KEY (`carrier_id`)
    REFERENCES `efsdev`.`efs_carrier_master` (`id`),
  CONSTRAINT `FK_CONSOLCONN_CONSOL`
    FOREIGN KEY (`consol_id`)
    REFERENCES `efsdev`.`efs_consol` (`id`),
  CONSTRAINT `FK_CONSOLCONN_PODID`
    FOREIGN KEY (`pod_id`)
    REFERENCES `efsdev`.`efs_port_master` (`id`),
  CONSTRAINT `FK_CONSOLCONN_POLID`
    FOREIGN KEY (`pol_id`)
    REFERENCES `efsdev`.`efs_port_master` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_consol_dimension`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_consol_dimension` (
  `id` BIGINT(20) NOT NULL,
  `gross_weight` DOUBLE NULL DEFAULT NULL,
  `height` INT(11) NULL DEFAULT NULL,
  `length` INT(11) NULL DEFAULT NULL,
  `no_of_piece` INT(11) NULL DEFAULT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  `vol_weight` DOUBLE NULL DEFAULT NULL,
  `width` INT(11) NULL DEFAULT NULL,
  `consol_id` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `FK_CONSOLDIM_CONSOL_ID` (`consol_id` ASC),
  CONSTRAINT `FK_CONSOLDIM_CONSOL_ID`
    FOREIGN KEY (`consol_id`)
    REFERENCES `efsdev`.`efs_consol` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_consol_doc_dimen`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_consol_doc_dimen` (
  `id` BIGINT(20) NOT NULL,
  `gross_weight` DOUBLE NULL DEFAULT NULL,
  `gross_weight_in_kg` DOUBLE NULL DEFAULT NULL,
  `height` INT(11) NULL DEFAULT NULL,
  `length` INT(11) NULL DEFAULT NULL,
  `no_of_pieces` INT(11) NULL DEFAULT NULL,
  `service_document_id` BIGINT(20) NULL DEFAULT NULL,
  `service_id` BIGINT(20) NULL DEFAULT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  `volume_weight` DOUBLE NULL DEFAULT NULL,
  `width` INT(11) NULL DEFAULT NULL,
  `consol_document_id` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `FK_DOCUD_SERVIDETID` (`consol_document_id` ASC),
  CONSTRAINT `FK_DOCUD_SERVIDETID`
    FOREIGN KEY (`consol_document_id`)
    REFERENCES `efsdev`.`efs_consol_document` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_event_master`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_event_master` (
  `id` BIGINT(20) NOT NULL,
  `event_code` VARCHAR(10) NOT NULL,
  `event_master_type` VARCHAR(30) NULL DEFAULT NULL,
  `event_name` VARCHAR(100) NOT NULL,
  `status` VARCHAR(10) NOT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  `country_id` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `UK_EVENT_EVENTCODE` (`event_code` ASC),
  UNIQUE INDEX `UK_EVENT_EVENTNAME` (`event_name` ASC),
  INDEX `FK_COUNTRY_ID` (`country_id` ASC),
  CONSTRAINT `FK_COUNTRY_ID`
    FOREIGN KEY (`country_id`)
    REFERENCES `efsdev`.`efs_country_master` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_consol_event`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_consol_event` (
  `id` BIGINT(20) NOT NULL,
  `assigned_code` VARCHAR(100) NULL DEFAULT NULL,
  `event_code` VARCHAR(100) NULL DEFAULT NULL,
  `event_date` DATETIME(6) NOT NULL,
  `follow_up_date` DATETIME(6) NULL DEFAULT NULL,
  `follow_up_required` VARCHAR(255) NOT NULL,
  `is_completed` VARCHAR(255) NOT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  `assigned_id` BIGINT(20) NULL DEFAULT NULL,
  `consol_id` BIGINT(20) NOT NULL,
  `event_master_id` BIGINT(20) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `FK_CONSOLEVE_ASSIGNEDID` (`assigned_id` ASC),
  INDEX `FK_CONSOLEVE_CONSOLIDETID` (`consol_id` ASC),
  INDEX `FK_CONSOLEVE_EVENTID` (`event_master_id` ASC),
  CONSTRAINT `FK_CONSOLEVE_ASSIGNEDID`
    FOREIGN KEY (`assigned_id`)
    REFERENCES `efsdev`.`efs_employee_master` (`id`),
  CONSTRAINT `FK_CONSOLEVE_CONSOLIDETID`
    FOREIGN KEY (`consol_id`)
    REFERENCES `efsdev`.`efs_consol` (`id`),
  CONSTRAINT `FK_CONSOLEVE_EVENTID`
    FOREIGN KEY (`event_master_id`)
    REFERENCES `efsdev`.`efs_event_master` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_consol_rate`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_consol_rate` (
  `id` BIGINT(20) NOT NULL,
  `amount` DOUBLE NULL DEFAULT NULL,
  `amount_per_unit` DOUBLE NULL DEFAULT NULL,
  `charge_code` VARCHAR(255) NULL DEFAULT NULL,
  `charge_name` VARCHAR(255) NULL DEFAULT NULL,
  `company_code` VARCHAR(100) NULL DEFAULT NULL,
  `consol_uid` VARCHAR(255) NULL DEFAULT NULL,
  `country_code` VARCHAR(10) NULL DEFAULT NULL,
  `crn` VARCHAR(255) NULL DEFAULT NULL,
  `currency_code` VARCHAR(255) NULL DEFAULT NULL,
  `currency_roe` DOUBLE NULL DEFAULT NULL,
  `due` VARCHAR(255) NULL DEFAULT NULL,
  `local_amount` DOUBLE NULL DEFAULT NULL,
  `location_code` VARCHAR(10) NULL DEFAULT NULL,
  `ppcc` VARCHAR(255) NULL DEFAULT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `unit` DOUBLE NULL DEFAULT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  `charge_id` BIGINT(20) NULL DEFAULT NULL,
  `company_id` BIGINT(20) NULL DEFAULT NULL,
  `consol_id` BIGINT(20) NULL DEFAULT NULL,
  `country_id` BIGINT(20) NULL DEFAULT NULL,
  `currency_id` BIGINT(20) NULL DEFAULT NULL,
  `location_id` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `FK_CONSRATE_CHARGEID` (`charge_id` ASC),
  INDEX `FK_CONSRATE_COMPANYID` (`company_id` ASC),
  INDEX `FK_CONSRATE_SERVICEID` (`consol_id` ASC),
  INDEX `FK_CONSRATE_COUNTRYID` (`country_id` ASC),
  INDEX `FK_CONSRATE_CURRENCYID` (`currency_id` ASC),
  INDEX `FK_CONSRATE_LOCATIONID` (`location_id` ASC),
  CONSTRAINT `FK_CONSRATE_CHARGEID`
    FOREIGN KEY (`charge_id`)
    REFERENCES `efsdev`.`efs_charge_master` (`id`),
  CONSTRAINT `FK_CONSRATE_COMPANYID`
    FOREIGN KEY (`company_id`)
    REFERENCES `efsdev`.`efs_company_master` (`id`),
  CONSTRAINT `FK_CONSRATE_COUNTRYID`
    FOREIGN KEY (`country_id`)
    REFERENCES `efsdev`.`efs_country_master` (`id`),
  CONSTRAINT `FK_CONSRATE_CURRENCYID`
    FOREIGN KEY (`currency_id`)
    REFERENCES `efsdev`.`efs_currency_master` (`id`),
  CONSTRAINT `FK_CONSRATE_LOCATIONID`
    FOREIGN KEY (`location_id`)
    REFERENCES `efsdev`.`efs_location_master` (`id`),
  CONSTRAINT `FK_CONSRATE_SERVICEID`
    FOREIGN KEY (`consol_id`)
    REFERENCES `efsdev`.`efs_consol` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_newage_audit_info`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_newage_audit_info` (
  `id` INT(11) NOT NULL,
  `timestamp` BIGINT(20) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_consol_rate_aud`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_consol_rate_aud` (
  `id` BIGINT(20) NOT NULL,
  `rev` INT(11) NOT NULL,
  `revtype` TINYINT(4) NULL DEFAULT NULL,
  `amount` DOUBLE NULL DEFAULT NULL,
  `amount_per_unit` DOUBLE NULL DEFAULT NULL,
  `charge_code` VARCHAR(255) NULL DEFAULT NULL,
  `charge_name` VARCHAR(255) NULL DEFAULT NULL,
  `company_code` VARCHAR(100) NULL DEFAULT NULL,
  `consol_uid` VARCHAR(255) NULL DEFAULT NULL,
  `country_code` VARCHAR(10) NULL DEFAULT NULL,
  `crn` VARCHAR(255) NULL DEFAULT NULL,
  `currency_code` VARCHAR(255) NULL DEFAULT NULL,
  `currency_roe` DOUBLE NULL DEFAULT NULL,
  `due` VARCHAR(255) NULL DEFAULT NULL,
  `local_amount` DOUBLE NULL DEFAULT NULL,
  `location_code` VARCHAR(10) NULL DEFAULT NULL,
  `ppcc` VARCHAR(255) NULL DEFAULT NULL,
  `create_date` DATETIME(6) NULL DEFAULT NULL,
  `create_user` VARCHAR(30) NULL DEFAULT NULL,
  `last_updated_date` DATETIME(6) NULL DEFAULT NULL,
  `last_updated_user` VARCHAR(30) NULL DEFAULT NULL,
  `unit` DOUBLE NULL DEFAULT NULL,
  `charge_id` BIGINT(20) NULL DEFAULT NULL,
  `company_id` BIGINT(20) NULL DEFAULT NULL,
  `consol_id` BIGINT(20) NULL DEFAULT NULL,
  `country_id` BIGINT(20) NULL DEFAULT NULL,
  `currency_id` BIGINT(20) NULL DEFAULT NULL,
  `location_id` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`, `rev`),
  INDEX `FK3voyojoksah8p9lnlppluifq7` (`rev` ASC),
  CONSTRAINT `FK3voyojoksah8p9lnlppluifq7`
    FOREIGN KEY (`rev`)
    REFERENCES `efsdev`.`efs_newage_audit_info` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_referencetype_master`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_referencetype_master` (
  `id` BIGINT(20) NOT NULL,
  `is_mandatory` VARCHAR(255) NULL DEFAULT NULL,
  `reference_type` VARCHAR(50) NULL DEFAULT NULL,
  `referencetype_code` VARCHAR(10) NOT NULL,
  `reference_type_key` VARCHAR(50) NULL DEFAULT NULL,
  `referencetype_name` VARCHAR(100) NOT NULL,
  `status` VARCHAR(10) NOT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `UK_REFERENCE_REFERENCECODE` (`referencetype_code` ASC),
  UNIQUE INDEX `UK_REFERENCE_REFERENCENAME` (`referencetype_name` ASC),
  UNIQUE INDEX `UK_REFERENCE_REFERENCETYPE` (`reference_type` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_consol_reference`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_consol_reference` (
  `id` BIGINT(20) NOT NULL,
  `reference_notes` VARCHAR(4000) NULL DEFAULT NULL,
  `reference_number` VARCHAR(30) NOT NULL,
  `reference_type_code` VARCHAR(100) NULL DEFAULT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  `consol_id` BIGINT(20) NOT NULL,
  `reference_type_id` BIGINT(20) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `FK_CONSOLREF_CONSOLID` (`consol_id` ASC),
  INDEX `FK_CONSOLREF_REFTYPE_ID` (`reference_type_id` ASC),
  CONSTRAINT `FK_CONSOLREF_CONSOLID`
    FOREIGN KEY (`consol_id`)
    REFERENCES `efsdev`.`efs_consol` (`id`),
  CONSTRAINT `FK_CONSOLREF_REFTYPE_ID`
    FOREIGN KEY (`reference_type_id`)
    REFERENCES `efsdev`.`efs_referencetype_master` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_consol_signoff_aud`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_consol_signoff_aud` (
  `id` BIGINT(20) NOT NULL,
  `rev` INT(11) NOT NULL,
  `revtype` TINYINT(4) NULL DEFAULT NULL,
  `comment_code` VARCHAR(255) NULL DEFAULT NULL,
  `consol_uid` VARCHAR(255) NULL DEFAULT NULL,
  `description` VARCHAR(4000) NULL DEFAULT NULL,
  `is_sign_off` VARCHAR(10) NULL DEFAULT NULL,
  `service_uid` VARCHAR(255) NULL DEFAULT NULL,
  `shipment_uid` VARCHAR(255) NULL DEFAULT NULL,
  `signoff_by_code` VARCHAR(255) NULL DEFAULT NULL,
  `signoff_date` DATETIME(6) NULL DEFAULT NULL,
  `create_date` DATETIME(6) NULL DEFAULT NULL,
  `create_user` VARCHAR(30) NULL DEFAULT NULL,
  `last_updated_date` DATETIME(6) NULL DEFAULT NULL,
  `last_updated_user` VARCHAR(30) NULL DEFAULT NULL,
  `comment_id` BIGINT(20) NULL DEFAULT NULL,
  `signoff_by` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`, `rev`),
  INDEX `FKabs7bulo6has4v9ug7exy97o3` (`rev` ASC),
  CONSTRAINT `FKabs7bulo6has4v9ug7exy97o3`
    FOREIGN KEY (`rev`)
    REFERENCES `efsdev`.`efs_newage_audit_info` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_dynamic_fields`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_dynamic_fields` (
  `id` BIGINT(20) NOT NULL,
  `field_id` VARCHAR(255) NOT NULL,
  `field_name` VARCHAR(255) NOT NULL,
  `remark` VARCHAR(255) NULL DEFAULT NULL,
  `screen` VARCHAR(255) NULL DEFAULT NULL,
  `screentab` VARCHAR(255) NULL DEFAULT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `UK_DYNAMIC_FIELD_NAME` (`field_name` ASC),
  UNIQUE INDEX `UK_DYNAMIC_FIELD_ID` (`field_id` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_country_dynamic_fields`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_country_dynamic_fields` (
  `id` BIGINT(20) NOT NULL,
  `show_field` VARCHAR(5) NOT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  `country_id` BIGINT(20) NOT NULL,
  `dynamic_field_id` BIGINT(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `UK_COMPDYNFIELDCOUNTRY_FIELD` (`country_id` ASC, `dynamic_field_id` ASC),
  INDEX `FK_COMPANY_DYNAMIC_FIELD_ID` (`dynamic_field_id` ASC),
  CONSTRAINT `FK_COMPANY_DYNAMIC_COUNTRY_ID`
    FOREIGN KEY (`country_id`)
    REFERENCES `efsdev`.`efs_country_master` (`id`),
  CONSTRAINT `FK_COMPANY_DYNAMIC_FIELD_ID`
    FOREIGN KEY (`dynamic_field_id`)
    REFERENCES `efsdev`.`efs_dynamic_fields` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_country_report_config`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_country_report_config` (
  `id` BIGINT(20) NOT NULL,
  `copies` BIGINT(20) NOT NULL,
  `report_name` VARCHAR(255) NULL DEFAULT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  `country_id` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `UK_PAGECOUNTRY_KEY` (`report_name` ASC, `country_id` ASC),
  INDEX `FK_COUNTRY` (`country_id` ASC),
  CONSTRAINT `FK_COUNTRY`
    FOREIGN KEY (`country_id`)
    REFERENCES `efsdev`.`efs_country_master` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_currency_rate_master`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_currency_rate_master` (
  `id` BIGINT(20) NOT NULL,
  `buy_rate` DOUBLE NOT NULL,
  `country_code` VARCHAR(10) NULL DEFAULT NULL,
  `currency_date` DATETIME(6) NULL DEFAULT NULL,
  `from_currency_code` VARCHAR(10) NULL DEFAULT NULL,
  `sell_rate` DOUBLE NOT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `to_currency_code` VARCHAR(10) NULL DEFAULT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  `country_id` BIGINT(20) NULL DEFAULT NULL,
  `from_currency_id` BIGINT(20) NULL DEFAULT NULL,
  `to_currency_id` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `UK_CURRENCY_FROMTODATE` (`from_currency_code` ASC, `to_currency_code` ASC, `currency_date` ASC),
  INDEX `FK_CURRENCYRATE_COUNTRYID` (`country_id` ASC),
  INDEX `FK_CURRENCYRATE_FROMCURRENCYID` (`from_currency_id` ASC),
  INDEX `FK_CURRENCYRATE_TOCURRENCYID` (`to_currency_id` ASC),
  CONSTRAINT `FK_CURRENCYRATE_COUNTRYID`
    FOREIGN KEY (`country_id`)
    REFERENCES `efsdev`.`efs_country_master` (`id`),
  CONSTRAINT `FK_CURRENCYRATE_FROMCURRENCYID`
    FOREIGN KEY (`from_currency_id`)
    REFERENCES `efsdev`.`efs_currency_master` (`id`),
  CONSTRAINT `FK_CURRENCYRATE_TOCURRENCYID`
    FOREIGN KEY (`to_currency_id`)
    REFERENCES `efsdev`.`efs_currency_master` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_custom_report_template`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_custom_report_template` (
  `id` BIGINT(20) NOT NULL,
  `is_default` VARCHAR(255) NULL DEFAULT NULL,
  `name` VARCHAR(100) NULL DEFAULT NULL,
  `report_name` VARCHAR(255) NULL DEFAULT NULL,
  `template` LONGTEXT NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_date_configuration`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_date_configuration` (
  `id` BIGINT(20) NOT NULL,
  `date_logic` VARCHAR(255) NOT NULL,
  `service_code` VARCHAR(255) NOT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  `which_transaction_date` VARCHAR(255) NOT NULL,
  `service_id` BIGINT(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `UK_DATECONFI_DATE` (`service_id` ASC, `which_transaction_date` ASC),
  CONSTRAINT `FK_DATECONFI_SERVICE`
    FOREIGN KEY (`service_id`)
    REFERENCES `efsdev`.`efs_service_master` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_general_ledger_group`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_general_ledger_group` (
  `id` BIGINT(20) NOT NULL,
  `company_code` VARCHAR(30) NOT NULL,
  `gl_group_code` VARCHAR(30) NOT NULL,
  `gl_group_name` VARCHAR(100) NOT NULL,
  `gl_head` VARCHAR(30) NOT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  `company_id` BIGINT(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `UK_GLGRP_CODE` (`gl_group_code` ASC),
  UNIQUE INDEX `UK_GLGRP_NAME` (`gl_group_name` ASC),
  INDEX `FK_GLGRP_COMPANY` (`company_id` ASC),
  CONSTRAINT `FK_GLGRP_COMPANY`
    FOREIGN KEY (`company_id`)
    REFERENCES `efsdev`.`efs_company_master` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_general_ledger_sub_group`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_general_ledger_sub_group` (
  `id` BIGINT(20) NOT NULL,
  `company_code` VARCHAR(30) NOT NULL,
  `gl_group_code` VARCHAR(30) NOT NULL,
  `gl_head` VARCHAR(30) NOT NULL,
  `gl_sub_group_code` VARCHAR(30) NOT NULL,
  `gl_sub_group_name` VARCHAR(100) NOT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  `company_id` BIGINT(20) NOT NULL,
  `gl_group_id` BIGINT(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `UK_GLSUBGRP_CODE` (`gl_sub_group_code` ASC),
  UNIQUE INDEX `UK_GLSUBGRP_NAME` (`gl_sub_group_name` ASC),
  INDEX `FK_GLSUBGRP_COMPANY` (`company_id` ASC),
  INDEX `FK_GLSUBGRP_GROUP` (`gl_group_id` ASC),
  CONSTRAINT `FK_GLSUBGRP_COMPANY`
    FOREIGN KEY (`company_id`)
    REFERENCES `efsdev`.`efs_company_master` (`id`),
  CONSTRAINT `FK_GLSUBGRP_GROUP`
    FOREIGN KEY (`gl_group_id`)
    REFERENCES `efsdev`.`efs_general_ledger_group` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_general_ledger`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_general_ledger` (
  `id` BIGINT(20) NOT NULL,
  `company_code` VARCHAR(30) NOT NULL,
  `gl_group_code` VARCHAR(30) NOT NULL,
  `gl_head` VARCHAR(30) NOT NULL,
  `gl_sub_group_code` VARCHAR(30) NOT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  `company_id` BIGINT(20) NOT NULL,
  `gl_group_id` BIGINT(20) NOT NULL,
  `gl_sub_group_id` BIGINT(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `UK_GL_GLUNIQUE` (`company_id` ASC, `gl_head` ASC, `gl_group_id` ASC, `gl_sub_group_id` ASC),
  INDEX `FK_GL_GROUP` (`gl_group_id` ASC),
  INDEX `FK_GL_SUBGROUP` (`gl_sub_group_id` ASC),
  CONSTRAINT `FK_GL_COMPANY`
    FOREIGN KEY (`company_id`)
    REFERENCES `efsdev`.`efs_company_master` (`id`),
  CONSTRAINT `FK_GL_GROUP`
    FOREIGN KEY (`gl_group_id`)
    REFERENCES `efsdev`.`efs_general_ledger_group` (`id`),
  CONSTRAINT `FK_GL_SUBGROUP`
    FOREIGN KEY (`gl_sub_group_id`)
    REFERENCES `efsdev`.`efs_general_ledger_sub_group` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_general_ledger_master`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_general_ledger_master` (
  `id` BIGINT(20) NOT NULL,
  `company_code` VARCHAR(30) NOT NULL,
  `gl_code` VARCHAR(30) NOT NULL,
  `gl_name` VARCHAR(100) NOT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  `company_id` BIGINT(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `UK_GLMASTER_NAME` (`gl_name` ASC),
  UNIQUE INDEX `UK_GLMASTER_CODE` (`gl_code` ASC),
  INDEX `FK_GLMASTER_COMPANY` (`company_id` ASC),
  CONSTRAINT `FK_GLMASTER_COMPANY`
    FOREIGN KEY (`company_id`)
    REFERENCES `efsdev`.`efs_company_master` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_general_ledger_account`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_general_ledger_account` (
  `id` BIGINT(20) NOT NULL,
  `company_code` VARCHAR(30) NOT NULL,
  `gl_account_code` VARCHAR(30) NOT NULL,
  `gl_account_name` VARCHAR(100) NOT NULL,
  `gl_master_code` VARCHAR(30) NOT NULL,
  `gl_sub_ledger_code` VARCHAR(30) NULL DEFAULT NULL,
  `is_bank_account` VARCHAR(5) NULL DEFAULT NULL,
  `is_job_account` VARCHAR(5) NULL DEFAULT NULL,
  `is_sub_ledger` VARCHAR(5) NULL DEFAULT NULL,
  `status` VARCHAR(10) NOT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  `bank_account_id` BIGINT(20) NULL DEFAULT NULL,
  `company_id` BIGINT(20) NOT NULL,
  `general_ledger_id` BIGINT(20) NULL DEFAULT NULL,
  `gl_master_id` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `UK_GLEDGERACC_ACCCODE` (`gl_account_code` ASC),
  UNIQUE INDEX `UK_GLEDGERACC_ACCNAME` (`gl_account_name` ASC),
  INDEX `FK_GLEDGERACC_BANKACCID` (`bank_account_id` ASC),
  INDEX `FK_GLEDGERACC_COMPANY` (`company_id` ASC),
  INDEX `FK_GLEDGERACC_GENDLDG` (`general_ledger_id` ASC),
  INDEX `FK_GLEDGERACC_GLMASTERID` (`gl_master_id` ASC),
  CONSTRAINT `FK_GLEDGERACC_BANKACCID`
    FOREIGN KEY (`bank_account_id`)
    REFERENCES `efsdev`.`efs_bank_account` (`id`),
  CONSTRAINT `FK_GLEDGERACC_COMPANY`
    FOREIGN KEY (`company_id`)
    REFERENCES `efsdev`.`efs_company_master` (`id`),
  CONSTRAINT `FK_GLEDGERACC_GENDLDG`
    FOREIGN KEY (`general_ledger_id`)
    REFERENCES `efsdev`.`efs_general_ledger` (`id`),
  CONSTRAINT `FK_GLEDGERACC_GLMASTERID`
    FOREIGN KEY (`gl_master_id`)
    REFERENCES `efsdev`.`efs_general_ledger_master` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_document_type_master`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_document_type_master` (
  `id` BIGINT(20) NOT NULL,
  `document_type_code` VARCHAR(30) NOT NULL,
  `document_type_name` VARCHAR(100) NOT NULL,
  `status` VARCHAR(10) NOT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `UK_DOCUMENT_DOCUMENTTYPECODE` (`document_type_code` ASC),
  UNIQUE INDEX `UK_DOCUMENT_DOCUMENTTYPENAME` (`document_type_name` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_daybook_master`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_daybook_master` (
  `id` BIGINT(20) NOT NULL,
  `block_date` DATETIME(6) NULL DEFAULT NULL,
  `cheque_no_from` VARCHAR(255) NULL DEFAULT NULL,
  `cheque_no_to` VARCHAR(255) NULL DEFAULT NULL,
  `cheque_report_name` VARCHAR(255) NULL DEFAULT NULL,
  `current_sequence_value` BIGINT(20) NULL DEFAULT NULL,
  `daybook_code` VARCHAR(10) NOT NULL,
  `daybook_name` VARCHAR(100) NOT NULL,
  `format` VARCHAR(255) NULL DEFAULT NULL,
  `is_month` VARCHAR(10) NULL DEFAULT NULL,
  `is_year` VARCHAR(10) NULL DEFAULT NULL,
  `numeric_daybook_code` VARCHAR(255) NULL DEFAULT NULL,
  `numeric_daybook_id` BIGINT(20) NULL DEFAULT NULL,
  `prefix` VARCHAR(5) NULL DEFAULT NULL,
  `separator` VARCHAR(1) NULL DEFAULT NULL,
  `status` VARCHAR(10) NOT NULL,
  `suffix` VARCHAR(5) NULL DEFAULT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  `bank_account` BIGINT(20) NULL DEFAULT NULL,
  `cash_account` BIGINT(20) NULL DEFAULT NULL,
  `document_type_id` BIGINT(20) NULL DEFAULT NULL,
  `exchange_gain_account` BIGINT(20) NULL DEFAULT NULL,
  `exchange_loss_account` BIGINT(20) NULL DEFAULT NULL,
  `location_id` BIGINT(20) NULL DEFAULT NULL,
  `pdc_account` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `UK_DAYBOOK_DAYBOOKCODE` (`daybook_code` ASC),
  UNIQUE INDEX `UK_DAYBOOK_DAYBOOKNAME` (`daybook_name` ASC),
  INDEX `FK_DAYBOOK_BANK_ACC` (`bank_account` ASC),
  INDEX `FK_DAYBOOK_CASH_ACC` (`cash_account` ASC),
  INDEX `FK_DAYBOOK_DOCUMENTTYPEID` (`document_type_id` ASC),
  INDEX `FK_DAYBOOK_EXC_GAIN_ACC` (`exchange_gain_account` ASC),
  INDEX `FK_DAYBOOK_EXC_LOSS_ACC` (`exchange_loss_account` ASC),
  INDEX `FK_DAYBOOK_LOCATIONID` (`location_id` ASC),
  INDEX `FK_DAYBOOK_PDS_ACC` (`pdc_account` ASC),
  CONSTRAINT `FK_DAYBOOK_BANK_ACC`
    FOREIGN KEY (`bank_account`)
    REFERENCES `efsdev`.`efs_general_ledger_account` (`id`),
  CONSTRAINT `FK_DAYBOOK_CASH_ACC`
    FOREIGN KEY (`cash_account`)
    REFERENCES `efsdev`.`efs_general_ledger_account` (`id`),
  CONSTRAINT `FK_DAYBOOK_DOCUMENTTYPEID`
    FOREIGN KEY (`document_type_id`)
    REFERENCES `efsdev`.`efs_document_type_master` (`id`),
  CONSTRAINT `FK_DAYBOOK_EXC_GAIN_ACC`
    FOREIGN KEY (`exchange_gain_account`)
    REFERENCES `efsdev`.`efs_general_ledger_account` (`id`),
  CONSTRAINT `FK_DAYBOOK_EXC_LOSS_ACC`
    FOREIGN KEY (`exchange_loss_account`)
    REFERENCES `efsdev`.`efs_general_ledger_account` (`id`),
  CONSTRAINT `FK_DAYBOOK_LOCATIONID`
    FOREIGN KEY (`location_id`)
    REFERENCES `efsdev`.`efs_location_master` (`id`),
  CONSTRAINT `FK_DAYBOOK_PDS_ACC`
    FOREIGN KEY (`pdc_account`)
    REFERENCES `efsdev`.`efs_general_ledger_account` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_default_charge`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_default_charge` (
  `id` BIGINT(20) NOT NULL,
  `amount_per_unit` DOUBLE NOT NULL,
  `charge_code` VARCHAR(255) NOT NULL,
  `charge_desc` VARCHAR(255) NULL DEFAULT NULL,
  `cost_in_amount` DOUBLE NULL DEFAULT NULL,
  `cost_in_minimum` DOUBLE NULL DEFAULT NULL,
  `currency_code` VARCHAR(255) NOT NULL,
  `is_clearance` VARCHAR(5) NULL DEFAULT NULL,
  `is_coload` VARCHAR(5) NULL DEFAULT NULL,
  `is_forwarder_direct` VARCHAR(15) NULL DEFAULT NULL,
  `is_imco` VARCHAR(255) NULL DEFAULT NULL,
  `is_our_transport` VARCHAR(5) NULL DEFAULT NULL,
  `is_personal_effect` VARCHAR(5) NULL DEFAULT NULL,
  `is_transit` VARCHAR(5) NULL DEFAULT NULL,
  `min_amount` DOUBLE NULL DEFAULT NULL,
  `ppcc` VARCHAR(255) NULL DEFAULT NULL,
  `service_code` VARCHAR(255) NULL DEFAULT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `unit_code` VARCHAR(255) NOT NULL,
  `valid_from` DATETIME(6) NULL DEFAULT NULL,
  `valid_to` DATETIME(6) NULL DEFAULT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  `who_routed` VARCHAR(255) NULL DEFAULT NULL,
  `agent_id` BIGINT(20) NULL DEFAULT NULL,
  `carrier_id` BIGINT(20) NULL DEFAULT NULL,
  `charge_master_id` BIGINT(20) NOT NULL,
  `commodity_id` BIGINT(20) NULL DEFAULT NULL,
  `consignee_id` BIGINT(20) NULL DEFAULT NULL,
  `currency_id` BIGINT(20) NOT NULL,
  `destination_id` BIGINT(20) NULL DEFAULT NULL,
  `division_id` BIGINT(20) NULL DEFAULT NULL,
  `origin_id` BIGINT(20) NULL DEFAULT NULL,
  `service_master_id` BIGINT(20) NOT NULL,
  `shipper_id` BIGINT(20) NULL DEFAULT NULL,
  `tos_id` BIGINT(20) NULL DEFAULT NULL,
  `transit_port_id` BIGINT(20) NULL DEFAULT NULL,
  `unit_id` BIGINT(20) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `FK_DEFCHG_AGTSID` (`agent_id` ASC),
  INDEX `FK_DEFCHG_CARID` (`carrier_id` ASC),
  INDEX `FK_DEFCHG_CHGMASID` (`charge_master_id` ASC),
  INDEX `FK_DEFCHG_CMDTID` (`commodity_id` ASC),
  INDEX `FK_DEFCHG_CONSID` (`consignee_id` ASC),
  INDEX `FK_DEFCHG_CURMASID` (`currency_id` ASC),
  INDEX `FK_DEFCHG_DSTMASID` (`destination_id` ASC),
  INDEX `FK_DEFCHG_DIVMASID` (`division_id` ASC),
  INDEX `FK_DEFCHG_ORGMASID` (`origin_id` ASC),
  INDEX `FK_DEFCHG_SVCMASID` (`service_master_id` ASC),
  INDEX `FK_DEFCHG_SHPID` (`shipper_id` ASC),
  INDEX `FK_DEFCHG_TOSID` (`tos_id` ASC),
  INDEX `FK_DEFCHG_TSTMASID` (`transit_port_id` ASC),
  INDEX `FK_DEFCHG_UNTMASID` (`unit_id` ASC),
  CONSTRAINT `FK_DEFCHG_AGTSID`
    FOREIGN KEY (`agent_id`)
    REFERENCES `efsdev`.`efs_party_master` (`id`),
  CONSTRAINT `FK_DEFCHG_CARID`
    FOREIGN KEY (`carrier_id`)
    REFERENCES `efsdev`.`efs_carrier_master` (`id`),
  CONSTRAINT `FK_DEFCHG_CHGMASID`
    FOREIGN KEY (`charge_master_id`)
    REFERENCES `efsdev`.`efs_charge_master` (`id`),
  CONSTRAINT `FK_DEFCHG_CMDTID`
    FOREIGN KEY (`commodity_id`)
    REFERENCES `efsdev`.`efs_hs_code_master` (`id`),
  CONSTRAINT `FK_DEFCHG_CONSID`
    FOREIGN KEY (`consignee_id`)
    REFERENCES `efsdev`.`efs_party_master` (`id`),
  CONSTRAINT `FK_DEFCHG_CURMASID`
    FOREIGN KEY (`currency_id`)
    REFERENCES `efsdev`.`efs_currency_master` (`id`),
  CONSTRAINT `FK_DEFCHG_DIVMASID`
    FOREIGN KEY (`division_id`)
    REFERENCES `efsdev`.`efs_division_master` (`id`),
  CONSTRAINT `FK_DEFCHG_DSTMASID`
    FOREIGN KEY (`destination_id`)
    REFERENCES `efsdev`.`efs_port_master` (`id`),
  CONSTRAINT `FK_DEFCHG_ORGMASID`
    FOREIGN KEY (`origin_id`)
    REFERENCES `efsdev`.`efs_port_master` (`id`),
  CONSTRAINT `FK_DEFCHG_SHPID`
    FOREIGN KEY (`shipper_id`)
    REFERENCES `efsdev`.`efs_party_master` (`id`),
  CONSTRAINT `FK_DEFCHG_SVCMASID`
    FOREIGN KEY (`service_master_id`)
    REFERENCES `efsdev`.`efs_service_master` (`id`),
  CONSTRAINT `FK_DEFCHG_TOSID`
    FOREIGN KEY (`tos_id`)
    REFERENCES `efsdev`.`efs_tos_master` (`id`),
  CONSTRAINT `FK_DEFCHG_TSTMASID`
    FOREIGN KEY (`transit_port_id`)
    REFERENCES `efsdev`.`efs_port_master` (`id`),
  CONSTRAINT `FK_DEFCHG_UNTMASID`
    FOREIGN KEY (`unit_id`)
    REFERENCES `efsdev`.`efs_unit_master` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_object_group_master`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_object_group_master` (
  `id` BIGINT(20) NOT NULL,
  `object_group_code` VARCHAR(10) NOT NULL,
  `object_group_name` VARCHAR(100) NOT NULL,
  `status` VARCHAR(10) NOT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `UK_OBJECTGROUP_OBJECTGROUPCODE` (`object_group_code` ASC),
  UNIQUE INDEX `UK_OBJECTGROUP_OBJECTGROUPNAME` (`object_group_name` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_object_sub_group_master`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_object_sub_group_master` (
  `id` BIGINT(20) NOT NULL,
  `object_sub_group_code` VARCHAR(10) NOT NULL,
  `object_sub_group_name` VARCHAR(100) NOT NULL,
  `status` VARCHAR(10) NOT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  `object_group_id` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `UK_OBJECTSUBGP_OBJECTSUBGPCODE` (`object_sub_group_code` ASC),
  UNIQUE INDEX `UK_OBJECTSUBGP_OBJECTSUBGPNAME` (`object_sub_group_name` ASC),
  INDEX `FK_OBJSUBGRP_OBJGRP` (`object_group_id` ASC),
  CONSTRAINT `FK_OBJSUBGRP_OBJGRP`
    FOREIGN KEY (`object_group_id`)
    REFERENCES `efsdev`.`efs_object_group_master` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_software_master`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_software_master` (
  `id` BIGINT(20) NOT NULL,
  `software_code` VARCHAR(10) NOT NULL,
  `software_name` VARCHAR(100) NOT NULL,
  `status` VARCHAR(10) NOT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `UK_SOFTWARE_SOFTWARECODE` (`software_code` ASC),
  UNIQUE INDEX `UK_SOFTWARE_SOFTWARENAME` (`software_name` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_default_master_data`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_default_master_data` (
  `id` BIGINT(20) NOT NULL,
  `code` VARCHAR(255) NOT NULL,
  `default_name` VARCHAR(255) NULL DEFAULT NULL,
  `description` VARCHAR(4000) NULL DEFAULT NULL,
  `location_code` VARCHAR(50) NULL DEFAULT NULL,
  `scope_flag` VARCHAR(255) NULL DEFAULT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `value` VARCHAR(4000) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  `location_id` BIGINT(20) NULL DEFAULT NULL,
  `object_group_id` BIGINT(20) NULL DEFAULT NULL,
  `object_sub_group_id` BIGINT(20) NULL DEFAULT NULL,
  `software_id` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `UK_DEFAULT_MASTER_DATA_CODE` (`code` ASC, `location_id` ASC),
  INDEX `FK_DEFAMASTERDATA_LOCID` (`location_id` ASC),
  INDEX `FK_OBJECTGROUP_OBJECTGROUPID` (`object_group_id` ASC),
  INDEX `FK_OBJSUBGRP_OBJSUBGRPID` (`object_sub_group_id` ASC),
  INDEX `FK_SOFTWARE_SOFTWAREID` (`software_id` ASC),
  CONSTRAINT `FK_DEFAMASTERDATA_LOCID`
    FOREIGN KEY (`location_id`)
    REFERENCES `efsdev`.`efs_location_master` (`id`),
  CONSTRAINT `FK_OBJECTGROUP_OBJECTGROUPID`
    FOREIGN KEY (`object_group_id`)
    REFERENCES `efsdev`.`efs_object_group_master` (`id`),
  CONSTRAINT `FK_OBJSUBGRP_OBJSUBGRPID`
    FOREIGN KEY (`object_sub_group_id`)
    REFERENCES `efsdev`.`efs_object_sub_group_master` (`id`),
  CONSTRAINT `FK_SOFTWARE_SOFTWAREID`
    FOREIGN KEY (`software_id`)
    REFERENCES `efsdev`.`efs_software_master` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_doc_prefix_type`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_doc_prefix_type` (
  `id` BIGINT(20) NOT NULL,
  `document_type` VARCHAR(50) NULL DEFAULT NULL,
  `location_code` VARCHAR(10) NOT NULL,
  `logo` LONGBLOB NULL DEFAULT NULL,
  `report_name` VARCHAR(100) NOT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  `doc_prefix_master_id` BIGINT(20) NULL DEFAULT NULL,
  `location_id` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `UK_DOCPRETYPE_LOCDOC` (`location_code` ASC, `document_type` ASC),
  UNIQUE INDEX `UK_DOCPRETYPE_LOCREPORT` (`location_code` ASC, `document_type` ASC, `report_name` ASC),
  INDEX `FK_DOCPREMAPP_DOCPREFIXID` (`doc_prefix_master_id` ASC),
  INDEX `FK_DOCPREMAPP_LOCATIONID` (`location_id` ASC),
  CONSTRAINT `FK_DOCPREMAPP_DOCPREFIXID`
    FOREIGN KEY (`doc_prefix_master_id`)
    REFERENCES `efsdev`.`efs_doc_prefix_master` (`id`),
  CONSTRAINT `FK_DOCPREMAPP_LOCATIONID`
    FOREIGN KEY (`location_id`)
    REFERENCES `efsdev`.`efs_location_master` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_document_detail`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_document_detail` (
  `id` BIGINT(20) NOT NULL,
  `accounting_information` VARCHAR(400) NULL DEFAULT NULL,
  `agent_code` VARCHAR(255) NULL DEFAULT NULL,
  `agent_manifest_name` VARCHAR(255) NULL DEFAULT NULL,
  `amount_of_insurance` VARCHAR(255) NULL DEFAULT NULL,
  `bl_received_date` DATETIME(6) NULL DEFAULT NULL,
  `bl_received_person_code` VARCHAR(30) NULL DEFAULT NULL,
  `booking_volume_in_cbm` DOUBLE NULL DEFAULT NULL,
  `can_issued_by_code` VARCHAR(100) NULL DEFAULT NULL,
  `can_issued_date` DATETIME(6) NULL DEFAULT NULL,
  `can_number` VARCHAR(100) NULL DEFAULT NULL,
  `carrier_code` VARCHAR(100) NULL DEFAULT NULL,
  `ccn_date` DATETIME(6) NULL DEFAULT NULL,
  `ccn_number` VARCHAR(30) NULL DEFAULT NULL,
  `doc_cha_agent_code` VARCHAR(255) NULL DEFAULT NULL,
  `chargeble_weight` DOUBLE NULL DEFAULT NULL,
  `charges_at_destination` VARCHAR(255) NULL DEFAULT NULL,
  `charges_dest_currency` VARCHAR(255) NULL DEFAULT NULL,
  `chargescode` CHAR(1) NULL DEFAULT NULL,
  `coll_valuation_charge` VARCHAR(255) NULL DEFAULT NULL,
  `coll_weight_charge` VARCHAR(255) NULL DEFAULT NULL,
  `coll_tax` VARCHAR(255) NULL DEFAULT NULL,
  `commodity_code` VARCHAR(255) NULL DEFAULT NULL,
  `commodity_description` VARCHAR(400) NULL DEFAULT NULL,
  `commodity_item_no` VARCHAR(255) NULL DEFAULT NULL,
  `company_code` VARCHAR(100) NULL DEFAULT NULL,
  `consignee_code` VARCHAR(255) NULL DEFAULT NULL,
  `consignee_manifest_name` VARCHAR(255) NULL DEFAULT NULL,
  `country_code` VARCHAR(10) NULL DEFAULT NULL,
  `currency_conversion_rates` VARCHAR(255) NULL DEFAULT NULL,
  `customer_ip` VARCHAR(100) NULL DEFAULT NULL,
  `declared_value_carriage` VARCHAR(255) NULL DEFAULT NULL,
  `declared_value_customs` VARCHAR(255) NULL DEFAULT NULL,
  `doc_destination` VARCHAR(255) NULL DEFAULT NULL,
  `dimension_gross_weight` DOUBLE NULL DEFAULT NULL,
  `dimension_gross_weight_in_kg` DOUBLE NULL DEFAULT NULL,
  `dimension_no_of_pieces` BIGINT(20) NULL DEFAULT NULL,
  `dimension_unit` VARCHAR(50) NOT NULL,
  `dimension_unit_value` DOUBLE NULL DEFAULT NULL,
  `dimension_volume_weight` DOUBLE NULL DEFAULT NULL,
  `diversion_contray` VARCHAR(255) NULL DEFAULT NULL,
  `do_issued_by_code` VARCHAR(100) NULL DEFAULT NULL,
  `do_issued_date` DATETIME(6) NULL DEFAULT NULL,
  `do_number` VARCHAR(100) NULL DEFAULT NULL,
  `do_signature` VARCHAR(255) NULL DEFAULT NULL,
  `doc_prefix_code` VARCHAR(255) NULL DEFAULT NULL,
  `document_no` VARCHAR(100) NOT NULL,
  `document_req_date` DATETIME(6) NOT NULL,
  `eta` DATETIME(6) NULL DEFAULT NULL,
  `etd` DATETIME(6) NULL DEFAULT NULL,
  `first_notify_code` VARCHAR(255) NULL DEFAULT NULL,
  `first_notify_manifest_name` VARCHAR(255) NULL DEFAULT NULL,
  `forwarder_code` VARCHAR(255) NULL DEFAULT NULL,
  `forwarder_manifest_name` VARCHAR(255) NULL DEFAULT NULL,
  `gross_weight` DOUBLE NULL DEFAULT NULL,
  `gross_weight_in_pound` DOUBLE NULL DEFAULT NULL,
  `handling_information` VARCHAR(400) NULL DEFAULT NULL,
  `hawb_no` VARCHAR(100) NULL DEFAULT NULL,
  `hawb_received_by_code` VARCHAR(100) NULL DEFAULT NULL,
  `hawb_received_on` DATETIME(6) NULL DEFAULT NULL,
  `hold_note` VARCHAR(400) NULL DEFAULT NULL,
  `is_customer_conform` VARCHAR(255) NULL DEFAULT NULL,
  `agent_sign` VARCHAR(255) NULL DEFAULT NULL,
  `doc_issuing_agent_code` VARCHAR(255) NULL DEFAULT NULL,
  `doc_issuing_agent_name` VARCHAR(255) NULL DEFAULT NULL,
  `location_code` VARCHAR(100) NULL DEFAULT NULL,
  `manifest_hawb_no` VARCHAR(100) NULL DEFAULT NULL,
  `marks_and_no` VARCHAR(400) NULL DEFAULT NULL,
  `mawb_no` VARCHAR(30) NULL DEFAULT NULL,
  `no_of_pieces` BIGINT(20) NULL DEFAULT NULL,
  `optional_information` VARCHAR(255) NULL DEFAULT NULL,
  `doc_origin` VARCHAR(255) NULL DEFAULT NULL,
  `othercharges_coll_agent` VARCHAR(255) NULL DEFAULT NULL,
  `othercharges_coll_carrier` VARCHAR(255) NULL DEFAULT NULL,
  `othercharges_pre_agent` VARCHAR(255) NULL DEFAULT NULL,
  `othercharges_pre_carrier` VARCHAR(255) NULL DEFAULT NULL,
  `others_collect` CHAR(1) NULL DEFAULT NULL,
  `others_prepaid` CHAR(1) NULL DEFAULT NULL,
  `pack_code` VARCHAR(255) NULL DEFAULT NULL,
  `pack_description` VARCHAR(400) NULL DEFAULT NULL,
  `pan_no` VARCHAR(255) NULL DEFAULT NULL,
  `pod` VARCHAR(255) NULL DEFAULT NULL,
  `pol` VARCHAR(255) NULL DEFAULT NULL,
  `pre_valuation_charge` VARCHAR(255) NULL DEFAULT NULL,
  `pre_weight_charge` VARCHAR(255) NULL DEFAULT NULL,
  `pre_tax` VARCHAR(255) NULL DEFAULT NULL,
  `rate_class` VARCHAR(2) NULL DEFAULT NULL,
  `rate_per_charge` DOUBLE NULL DEFAULT NULL,
  `ref_no` VARCHAR(30) NULL DEFAULT NULL,
  `route_no` VARCHAR(255) NULL DEFAULT NULL,
  `second_notify_code` VARCHAR(255) NULL DEFAULT NULL,
  `second_notify_manifest_name` VARCHAR(255) NULL DEFAULT NULL,
  `service_code` VARCHAR(255) NULL DEFAULT NULL,
  `service_uid` VARCHAR(255) NOT NULL,
  `shipment_uid` VARCHAR(255) NOT NULL,
  `shipper_code` VARCHAR(255) NULL DEFAULT NULL,
  `shipper_manifest_name` VARCHAR(255) NULL DEFAULT NULL,
  `shipper_sign` VARCHAR(255) NULL DEFAULT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `total_amount` VARCHAR(255) NULL DEFAULT NULL,
  `total_collect` VARCHAR(255) NULL DEFAULT NULL,
  `total_charges` VARCHAR(255) NULL DEFAULT NULL,
  `total_prepaid` VARCHAR(255) NULL DEFAULT NULL,
  `trans_ccn_date` DATETIME(6) NULL DEFAULT NULL,
  `trans_ccn_number` VARCHAR(30) NULL DEFAULT NULL,
  `transit_eta` DATETIME(6) NULL DEFAULT NULL,
  `transit_port` VARCHAR(255) NULL DEFAULT NULL,
  `transit_schedule_uid` VARCHAR(255) NULL DEFAULT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  `vessel_code` VARCHAR(255) NULL DEFAULT NULL,
  `vessel_id` BIGINT(20) NULL DEFAULT NULL,
  `volume_weight` DOUBLE NULL DEFAULT NULL,
  `volume_weight_in_pound` DOUBLE NULL DEFAULT NULL,
  `wt_val_col` CHAR(1) NULL DEFAULT NULL,
  `wt_val_ppd` CHAR(1) NULL DEFAULT NULL,
  `agent_id` BIGINT(20) NULL DEFAULT NULL,
  `agent_address_id` BIGINT(20) NULL DEFAULT NULL,
  `bl_received_person_id` BIGINT(20) NULL DEFAULT NULL,
  `can_issued_by` BIGINT(20) NULL DEFAULT NULL,
  `carrier_id` BIGINT(20) NULL DEFAULT NULL,
  `doc_cha_agent_id` BIGINT(20) NULL DEFAULT NULL,
  `doc_cha_agent_add_id` BIGINT(20) NULL DEFAULT NULL,
  `commodity_id` BIGINT(20) NULL DEFAULT NULL,
  `company_id` BIGINT(20) NULL DEFAULT NULL,
  `consignee_id` BIGINT(20) NULL DEFAULT NULL,
  `consignee_address_id` BIGINT(20) NULL DEFAULT NULL,
  `country_id` BIGINT(20) NULL DEFAULT NULL,
  `doc_destination_id` BIGINT(20) NULL DEFAULT NULL,
  `do_issued_by` BIGINT(20) NULL DEFAULT NULL,
  `doc_prefix_id` BIGINT(20) NULL DEFAULT NULL,
  `document_sub_detail` BIGINT(20) NULL DEFAULT NULL,
  `first_notify_id` BIGINT(20) NULL DEFAULT NULL,
  `first_notify_address_id` BIGINT(20) NULL DEFAULT NULL,
  `forwarder_id` BIGINT(20) NULL DEFAULT NULL,
  `forwarder_address_id` BIGINT(20) NULL DEFAULT NULL,
  `hawb_received_id` BIGINT(20) NULL DEFAULT NULL,
  `doc_issuing_agent_id` BIGINT(20) NULL DEFAULT NULL,
  `doc_issuing_agent_add_id` BIGINT(20) NULL DEFAULT NULL,
  `location_id` BIGINT(20) NULL DEFAULT NULL,
  `doc_origin_id` BIGINT(20) NULL DEFAULT NULL,
  `pack_id` BIGINT(20) NULL DEFAULT NULL,
  `pickup_delivery_id` BIGINT(20) NULL DEFAULT NULL,
  `pod_id` BIGINT(20) NULL DEFAULT NULL,
  `pol_id` BIGINT(20) NULL DEFAULT NULL,
  `second_notify_id` BIGINT(20) NULL DEFAULT NULL,
  `second_notify_address_id` BIGINT(20) NULL DEFAULT NULL,
  `service_id` BIGINT(20) NULL DEFAULT NULL,
  `shipper_id` BIGINT(20) NOT NULL,
  `shipper_address_id` BIGINT(20) NOT NULL,
  `transit_port_id` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `UK_DOCUMENTDETA_DOCUMENTNO` (`document_no` ASC),
  INDEX `FK_DOCDETAIL_AGENTID` (`agent_id` ASC),
  INDEX `FK_DOCDETAIL_AGENTADDID` (`agent_address_id` ASC),
  INDEX `FK_DOCDETAIL_BLRECEMPID` (`bl_received_person_id` ASC),
  INDEX `FK_DOCDETAIL_CANISS` (`can_issued_by` ASC),
  INDEX `FK_DOCDETAIL_CARRIERID` (`carrier_id` ASC),
  INDEX `FK_DOCDETAIL_CHAAGENTID` (`doc_cha_agent_id` ASC),
  INDEX `FK_DOCDETAIL_CHAAGENTADDID` (`doc_cha_agent_add_id` ASC),
  INDEX `FK_DOCDETAIL_COMMODITYID` (`commodity_id` ASC),
  INDEX `FK_DOCDETAIL_COMPANYID` (`company_id` ASC),
  INDEX `FK_DOCDETAIL_CONSIGNEEID` (`consignee_id` ASC),
  INDEX `FK_DOCDETAIL_CONSIGNADDID` (`consignee_address_id` ASC),
  INDEX `FK_DOCDETAIL_COUNTRYID` (`country_id` ASC),
  INDEX `FK_DOCDETAIL_DOCDESTIID` (`doc_destination_id` ASC),
  INDEX `FK_DOCDETAIL_DOISS` (`do_issued_by` ASC),
  INDEX `FK_DOCDETAIL_DOCPREFIXID` (`doc_prefix_id` ASC),
  INDEX `FK_DOCDETAIL_DOCSUBDETAILID` (`document_sub_detail` ASC),
  INDEX `FK_DOCDETAIL_FIRSTNOTIFYID` (`first_notify_id` ASC),
  INDEX `FK_DOCDETAIL_FIRNOTADDID` (`first_notify_address_id` ASC),
  INDEX `FK_DOCDETAIL_FORWARDID` (`forwarder_id` ASC),
  INDEX `FK_DOCDETAIL_FORWARDADDID` (`forwarder_address_id` ASC),
  INDEX `FK_DOCDETAIL_HAWBRECEIVED` (`hawb_received_id` ASC),
  INDEX `FK_DOCDETAIL_ISSAGENTID` (`doc_issuing_agent_id` ASC),
  INDEX `FK_DOCDETAIL_ISSAGENTADDID` (`doc_issuing_agent_add_id` ASC),
  INDEX `FK_DOCDETAIL_LOCATIONID` (`location_id` ASC),
  INDEX `FK_DOCDETAIL_DOCORIGINID` (`doc_origin_id` ASC),
  INDEX `FK_DOCDETAIL_PACKID` (`pack_id` ASC),
  INDEX `FK_DOCDETAIL_PICKDELID` (`pickup_delivery_id` ASC),
  INDEX `FK_DOCDETAIL_PODID` (`pod_id` ASC),
  INDEX `FK_DOCDETAIL_POLID` (`pol_id` ASC),
  INDEX `FK_DOCDETAIL_SECONDNOTIFYID` (`second_notify_id` ASC),
  INDEX `FK_DOCDETAIL_SECNOTADDID` (`second_notify_address_id` ASC),
  INDEX `FK_DOCDETAIL_SERVICEID` (`service_id` ASC),
  INDEX `FK_DOCDETAIL_SHIPPERID` (`shipper_id` ASC),
  INDEX `FK_DOCDETAIL_SHIPPERADDID` (`shipper_address_id` ASC),
  INDEX `FK_DOCDETAIL_TRANSITPORTID` (`transit_port_id` ASC),
  CONSTRAINT `FK_DOCDETAIL_AGENTADDID`
    FOREIGN KEY (`agent_address_id`)
    REFERENCES `efsdev`.`efs_address_master` (`id`),
  CONSTRAINT `FK_DOCDETAIL_AGENTID`
    FOREIGN KEY (`agent_id`)
    REFERENCES `efsdev`.`efs_party_master` (`id`),
  CONSTRAINT `FK_DOCDETAIL_BLRECEMPID`
    FOREIGN KEY (`bl_received_person_id`)
    REFERENCES `efsdev`.`efs_employee_master` (`id`),
  CONSTRAINT `FK_DOCDETAIL_CANISS`
    FOREIGN KEY (`can_issued_by`)
    REFERENCES `efsdev`.`efs_employee_master` (`id`),
  CONSTRAINT `FK_DOCDETAIL_CARRIERID`
    FOREIGN KEY (`carrier_id`)
    REFERENCES `efsdev`.`efs_carrier_master` (`id`),
  CONSTRAINT `FK_DOCDETAIL_CHAAGENTADDID`
    FOREIGN KEY (`doc_cha_agent_add_id`)
    REFERENCES `efsdev`.`efs_address_master` (`id`),
  CONSTRAINT `FK_DOCDETAIL_CHAAGENTID`
    FOREIGN KEY (`doc_cha_agent_id`)
    REFERENCES `efsdev`.`efs_party_master` (`id`),
  CONSTRAINT `FK_DOCDETAIL_COMMODITYID`
    FOREIGN KEY (`commodity_id`)
    REFERENCES `efsdev`.`efs_hs_code_master` (`id`),
  CONSTRAINT `FK_DOCDETAIL_COMPANYID`
    FOREIGN KEY (`company_id`)
    REFERENCES `efsdev`.`efs_company_master` (`id`),
  CONSTRAINT `FK_DOCDETAIL_CONSIGNADDID`
    FOREIGN KEY (`consignee_address_id`)
    REFERENCES `efsdev`.`efs_address_master` (`id`),
  CONSTRAINT `FK_DOCDETAIL_CONSIGNEEID`
    FOREIGN KEY (`consignee_id`)
    REFERENCES `efsdev`.`efs_party_master` (`id`),
  CONSTRAINT `FK_DOCDETAIL_COUNTRYID`
    FOREIGN KEY (`country_id`)
    REFERENCES `efsdev`.`efs_country_master` (`id`),
  CONSTRAINT `FK_DOCDETAIL_DOCDESTIID`
    FOREIGN KEY (`doc_destination_id`)
    REFERENCES `efsdev`.`efs_port_master` (`id`),
  CONSTRAINT `FK_DOCDETAIL_DOCORIGINID`
    FOREIGN KEY (`doc_origin_id`)
    REFERENCES `efsdev`.`efs_port_master` (`id`),
  CONSTRAINT `FK_DOCDETAIL_DOCPREFIXID`
    FOREIGN KEY (`doc_prefix_id`)
    REFERENCES `efsdev`.`efs_doc_prefix_master` (`id`),
  CONSTRAINT `FK_DOCDETAIL_DOCSUBDETAILID`
    FOREIGN KEY (`document_sub_detail`)
    REFERENCES `efsdev`.`efs_document_sub_detail` (`id`),
  CONSTRAINT `FK_DOCDETAIL_DOISS`
    FOREIGN KEY (`do_issued_by`)
    REFERENCES `efsdev`.`efs_employee_master` (`id`),
  CONSTRAINT `FK_DOCDETAIL_FIRNOTADDID`
    FOREIGN KEY (`first_notify_address_id`)
    REFERENCES `efsdev`.`efs_address_master` (`id`),
  CONSTRAINT `FK_DOCDETAIL_FIRSTNOTIFYID`
    FOREIGN KEY (`first_notify_id`)
    REFERENCES `efsdev`.`efs_party_master` (`id`),
  CONSTRAINT `FK_DOCDETAIL_FORWARDADDID`
    FOREIGN KEY (`forwarder_address_id`)
    REFERENCES `efsdev`.`efs_address_master` (`id`),
  CONSTRAINT `FK_DOCDETAIL_FORWARDID`
    FOREIGN KEY (`forwarder_id`)
    REFERENCES `efsdev`.`efs_party_master` (`id`),
  CONSTRAINT `FK_DOCDETAIL_HAWBRECEIVED`
    FOREIGN KEY (`hawb_received_id`)
    REFERENCES `efsdev`.`efs_employee_master` (`id`),
  CONSTRAINT `FK_DOCDETAIL_ISSAGENTADDID`
    FOREIGN KEY (`doc_issuing_agent_add_id`)
    REFERENCES `efsdev`.`efs_address_master` (`id`),
  CONSTRAINT `FK_DOCDETAIL_ISSAGENTID`
    FOREIGN KEY (`doc_issuing_agent_id`)
    REFERENCES `efsdev`.`efs_party_master` (`id`),
  CONSTRAINT `FK_DOCDETAIL_LOCATIONID`
    FOREIGN KEY (`location_id`)
    REFERENCES `efsdev`.`efs_location_master` (`id`),
  CONSTRAINT `FK_DOCDETAIL_PACKID`
    FOREIGN KEY (`pack_id`)
    REFERENCES `efsdev`.`efs_pack_master` (`id`),
  CONSTRAINT `FK_DOCDETAIL_PICKDELID`
    FOREIGN KEY (`pickup_delivery_id`)
    REFERENCES `efsdev`.`efs_pickup_delivery_point` (`id`),
  CONSTRAINT `FK_DOCDETAIL_PODID`
    FOREIGN KEY (`pod_id`)
    REFERENCES `efsdev`.`efs_port_master` (`id`),
  CONSTRAINT `FK_DOCDETAIL_POLID`
    FOREIGN KEY (`pol_id`)
    REFERENCES `efsdev`.`efs_port_master` (`id`),
  CONSTRAINT `FK_DOCDETAIL_SECNOTADDID`
    FOREIGN KEY (`second_notify_address_id`)
    REFERENCES `efsdev`.`efs_address_master` (`id`),
  CONSTRAINT `FK_DOCDETAIL_SECONDNOTIFYID`
    FOREIGN KEY (`second_notify_id`)
    REFERENCES `efsdev`.`efs_party_master` (`id`),
  CONSTRAINT `FK_DOCDETAIL_SERVICEID`
    FOREIGN KEY (`service_id`)
    REFERENCES `efsdev`.`efs_service` (`id`),
  CONSTRAINT `FK_DOCDETAIL_SHIPPERADDID`
    FOREIGN KEY (`shipper_address_id`)
    REFERENCES `efsdev`.`efs_address_master` (`id`),
  CONSTRAINT `FK_DOCDETAIL_SHIPPERID`
    FOREIGN KEY (`shipper_id`)
    REFERENCES `efsdev`.`efs_party_master` (`id`),
  CONSTRAINT `FK_DOCDETAIL_TRANSITPORTID`
    FOREIGN KEY (`transit_port_id`)
    REFERENCES `efsdev`.`efs_port_master` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_document_iss_restri`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_document_iss_restri` (
  `id` BIGINT(20) NOT NULL,
  `enable_credit_limit_checking` VARCHAR(5) NOT NULL,
  `enable_invoice_checking` VARCHAR(5) NOT NULL,
  `enable_issue_restriction` VARCHAR(5) NOT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  `company_id` BIGINT(20) NULL DEFAULT NULL,
  `location_id` BIGINT(20) NULL DEFAULT NULL,
  `service_id` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `UK_DOC_ISS_RES_COM_LOC_SER` (`company_id` ASC, `location_id` ASC, `service_id` ASC),
  INDEX `FK_DOC_ISSUE_RES_LOCATION_ID` (`location_id` ASC),
  INDEX `FK_DOC_ISSUE_RES_SERVICE_ID` (`service_id` ASC),
  CONSTRAINT `FK_DOC_ISSUE_RES_COMPANY_ID`
    FOREIGN KEY (`company_id`)
    REFERENCES `efsdev`.`efs_company_master` (`id`),
  CONSTRAINT `FK_DOC_ISSUE_RES_LOCATION_ID`
    FOREIGN KEY (`location_id`)
    REFERENCES `efsdev`.`efs_location_master` (`id`),
  CONSTRAINT `FK_DOC_ISSUE_RES_SERVICE_ID`
    FOREIGN KEY (`service_id`)
    REFERENCES `efsdev`.`efs_service_master` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_document_issue_res_tos`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_document_issue_res_tos` (
  `doc_issue_res_id` BIGINT(20) NOT NULL,
  `tos_id` BIGINT(20) NOT NULL,
  UNIQUE INDEX `UK_DOC_ISS_RES_DOC_TOS` (`doc_issue_res_id` ASC, `tos_id` ASC),
  INDEX `FK_DOC_ISS_RES_TOS_ID` (`tos_id` ASC),
  CONSTRAINT `FK_DOC_ISS_RES_DOC_ID`
    FOREIGN KEY (`doc_issue_res_id`)
    REFERENCES `efsdev`.`efs_document_iss_restri` (`id`),
  CONSTRAINT `FK_DOC_ISS_RES_TOS_ID`
    FOREIGN KEY (`tos_id`)
    REFERENCES `efsdev`.`efs_tos_master` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_document_old_entry`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_document_old_entry` (
  `id` BIGINT(20) NOT NULL,
  `chargeble_weight` DOUBLE NULL DEFAULT NULL,
  `dimension_gross_weight` DOUBLE NULL DEFAULT NULL,
  `dimension_gross_weight_in_kg` DOUBLE NULL DEFAULT NULL,
  `dimension_no_of_pieces` BIGINT(20) NULL DEFAULT NULL,
  `dimension_unit` VARCHAR(50) NOT NULL,
  `dimension_unit_value` DOUBLE NULL DEFAULT NULL,
  `dimension_volume_weight` DOUBLE NULL DEFAULT NULL,
  `document_no` VARCHAR(100) NOT NULL,
  `gross_weight` DOUBLE NULL DEFAULT NULL,
  `gross_weight_in_pound` DOUBLE NULL DEFAULT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  `volume_weight` DOUBLE NULL DEFAULT NULL,
  `volume_weight_in_pound` DOUBLE NULL DEFAULT NULL,
  `service_old_id` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `UK_DOCUMENOLD_DOCUMENTNO` (`document_no` ASC),
  INDEX `FK_DOCOLD_SID` (`service_old_id` ASC),
  CONSTRAINT `FK_DOCOLD_SID`
    FOREIGN KEY (`service_old_id`)
    REFERENCES `efsdev`.`efs_service_old_entry` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_document_old_dimension`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_document_old_dimension` (
  `id` BIGINT(20) NOT NULL,
  `document_no` VARCHAR(255) NOT NULL,
  `gross_weight` DOUBLE NULL DEFAULT NULL,
  `gross_weight_in_kg` DOUBLE NULL DEFAULT NULL,
  `height` INT(11) NULL DEFAULT NULL,
  `length` INT(11) NULL DEFAULT NULL,
  `no_of_pieces` INT(11) NULL DEFAULT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  `volume_weight` DOUBLE NULL DEFAULT NULL,
  `width` INT(11) NULL DEFAULT NULL,
  `doc_old_id` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `FK_DOLD_ID` (`doc_old_id` ASC),
  CONSTRAINT `FK_DOLD_ID`
    FOREIGN KEY (`doc_old_id`)
    REFERENCES `efsdev`.`efs_document_old_entry` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_dtdc_vehicle`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_dtdc_vehicle` (
  `id` BIGINT(20) NOT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `vehicle_title_no` VARCHAR(15) NULL DEFAULT NULL,
  `title_state_code` VARCHAR(100) NULL DEFAULT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  `vin_id_type` VARCHAR(25) NULL DEFAULT NULL,
  `vin_product_id` VARCHAR(25) NULL DEFAULT NULL,
  `aes_commodity_id` BIGINT(20) NULL DEFAULT NULL,
  `title_state_id` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `FK_VEHI_DTDC_ID` (`aes_commodity_id` ASC),
  INDEX `FK_VEHI_DTDC_TITLE_ID` (`title_state_id` ASC),
  CONSTRAINT `FK_VEHI_DTDC_ID`
    FOREIGN KEY (`aes_commodity_id`)
    REFERENCES `efsdev`.`efs_aes_commodity` (`id`),
  CONSTRAINT `FK_VEHI_DTDC_TITLE_ID`
    FOREIGN KEY (`title_state_id`)
    REFERENCES `efsdev`.`efs_state_province_master` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_dynamic_field_countries`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_dynamic_field_countries` (
  `field_id` BIGINT(20) NOT NULL,
  `country_id` BIGINT(20) NOT NULL,
  UNIQUE INDEX `UK_DFC_FIELD_COUNTRY` (`field_id` ASC, `country_id` ASC),
  INDEX `FK_DFC_COUNTRY_ID` (`country_id` ASC),
  CONSTRAINT `FK_DFC_COUNTRY_ID`
    FOREIGN KEY (`country_id`)
    REFERENCES `efsdev`.`efs_country_master` (`id`),
  CONSTRAINT `FK_DFC_FIELD_ID`
    FOREIGN KEY (`field_id`)
    REFERENCES `efsdev`.`efs_dynamic_fields` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_edi_configuration_master`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_edi_configuration_master` (
  `id` BIGINT(20) NOT NULL,
  `description` VARCHAR(4000) NULL DEFAULT NULL,
  `key` VARCHAR(255) NOT NULL,
  `value` VARCHAR(255) NOT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  `location_id` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `UK_EDI_CONFIGURATION_KEY` (`key` ASC, `location_id` ASC),
  INDEX `FK_EDICOMLOC_LOCATIONID` (`location_id` ASC),
  CONSTRAINT `FK_EDICOMLOC_LOCATIONID`
    FOREIGN KEY (`location_id`)
    REFERENCES `efsdev`.`efs_location_master` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_edi_response_status`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_edi_response_status` (
  `id` BIGINT(20) NOT NULL,
  `carrier_code` VARCHAR(100) NULL DEFAULT NULL,
  `edi_status_type` VARCHAR(10) NOT NULL,
  `edi_type` VARCHAR(10) NOT NULL,
  `flight_number` VARCHAR(100) NULL DEFAULT NULL,
  `master_number` VARCHAR(30) NOT NULL,
  `status_date` DATETIME(6) NOT NULL,
  `status_name` VARCHAR(100) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  `consol_id` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `FK_EDIRESP_CONSOLID` (`consol_id` ASC),
  CONSTRAINT `FK_EDIRESP_CONSOLID`
    FOREIGN KEY (`consol_id`)
    REFERENCES `efsdev`.`efs_consol` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_email`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_email` (
  `id` BIGINT(20) NOT NULL,
  `destination_country` VARCHAR(10) NULL DEFAULT NULL,
  `email` VARCHAR(255) NULL DEFAULT NULL,
  `imco` VARCHAR(255) NULL DEFAULT NULL,
  `origin_country` VARCHAR(10) NULL DEFAULT NULL,
  `receiver_code` VARCHAR(255) NULL DEFAULT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  `destination_country_id` BIGINT(20) NULL DEFAULT NULL,
  `employee_id` BIGINT(20) NULL DEFAULT NULL,
  `location_id` BIGINT(20) NULL DEFAULT NULL,
  `origin_country_id` BIGINT(20) NULL DEFAULT NULL,
  `party_id` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `FK_EMAIL_DESTICOUNTRYID` (`destination_country_id` ASC),
  INDEX `FK_EMAIL_EMPLOYEEID` (`employee_id` ASC),
  INDEX `FK_EMAIL_LOCATIONID` (`location_id` ASC),
  INDEX `FK_EMAIL_ORIGINCOUNTRYID` (`origin_country_id` ASC),
  INDEX `FK_EMAIL_PARTYID` (`party_id` ASC),
  CONSTRAINT `FK_EMAIL_DESTICOUNTRYID`
    FOREIGN KEY (`destination_country_id`)
    REFERENCES `efsdev`.`efs_country_master` (`id`),
  CONSTRAINT `FK_EMAIL_EMPLOYEEID`
    FOREIGN KEY (`employee_id`)
    REFERENCES `efsdev`.`efs_employee_master` (`id`),
  CONSTRAINT `FK_EMAIL_LOCATIONID`
    FOREIGN KEY (`location_id`)
    REFERENCES `efsdev`.`efs_location_master` (`id`),
  CONSTRAINT `FK_EMAIL_ORIGINCOUNTRYID`
    FOREIGN KEY (`origin_country_id`)
    REFERENCES `efsdev`.`efs_country_master` (`id`),
  CONSTRAINT `FK_EMAIL_PARTYID`
    FOREIGN KEY (`party_id`)
    REFERENCES `efsdev`.`efs_party_master` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_email_request`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_email_request` (
  `id` BIGINT(20) NOT NULL,
  `email_bcc` VARCHAR(500) NULL DEFAULT NULL,
  `email_cc` VARCHAR(500) NULL DEFAULT NULL,
  `email_body` LONGTEXT NOT NULL,
  `email_status` VARCHAR(20) NOT NULL,
  `email_type` VARCHAR(30) NOT NULL,
  `error_desc` VARCHAR(255) NULL DEFAULT NULL,
  `email_from` VARCHAR(100) NOT NULL,
  `email_from_password` VARCHAR(100) NOT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(255) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(255) NOT NULL,
  `server_port` INT(11) NULL DEFAULT NULL,
  `smtp_server_address` VARCHAR(255) NULL DEFAULT NULL,
  `email_subject` VARCHAR(255) NOT NULL,
  `email_to` VARCHAR(500) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_email_request_attachment`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_email_request_attachment` (
  `id` BIGINT(20) NOT NULL,
  `attachment` BLOB NULL DEFAULT NULL,
  `file_name` VARCHAR(100) NOT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(255) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(255) NOT NULL,
  `mime_type` VARCHAR(100) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  `email_request_id` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `FK_EMAILREQATTACH_EMAILREQID` (`email_request_id` ASC),
  CONSTRAINT `FK_EMAILREQATTACH_EMAILREQID`
    FOREIGN KEY (`email_request_id`)
    REFERENCES `efsdev`.`efs_email_request` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_email_template`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_email_template` (
  `id` BIGINT(20) NOT NULL,
  `bcc_email_address` VARCHAR(255) NULL DEFAULT NULL,
  `cc_email_address` VARCHAR(255) NULL DEFAULT NULL,
  `email_type` VARCHAR(255) NULL DEFAULT NULL,
  `from_email_id` VARCHAR(255) NULL DEFAULT NULL,
  `from_email_password` VARCHAR(255) NULL DEFAULT NULL,
  `port` INT(11) NULL DEFAULT NULL,
  `smtp_server_address` VARCHAR(255) NULL DEFAULT NULL,
  `subject` VARCHAR(255) NULL DEFAULT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `template` LONGTEXT NULL DEFAULT NULL,
  `template_type` VARCHAR(255) NULL DEFAULT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_employee_attachment`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_employee_attachment` (
  `id` BIGINT(20) NOT NULL,
  `file_attached` LONGBLOB NULL DEFAULT NULL,
  `file_content_type` VARCHAR(100) NULL DEFAULT NULL,
  `file_name` VARCHAR(100) NULL DEFAULT NULL,
  `ref_no` VARCHAR(100) NULL DEFAULT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  `employee_id` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `UK_EMPLOYEE_ATT_REF` (`ref_no` ASC, `employee_id` ASC),
  INDEX `FK_EMPATT_EMPID` (`employee_id` ASC),
  CONSTRAINT `FK_EMPATT_EMPID`
    FOREIGN KEY (`employee_id`)
    REFERENCES `efsdev`.`efs_employee_master` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_enquiry_attachment`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_enquiry_attachment` (
  `id` BIGINT(20) NOT NULL,
  `file_attached` LONGBLOB NULL DEFAULT NULL,
  `file_content_type` VARCHAR(100) NULL DEFAULT NULL,
  `file_name` VARCHAR(100) NULL DEFAULT NULL,
  `ref_no` VARCHAR(100) NULL DEFAULT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  `enquiry_detail_id` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `UK_ENQUIRYATT_REF` (`ref_no` ASC, `enquiry_detail_id` ASC),
  INDEX `FK_ENQUIRYATTACH_ENQDETID` (`enquiry_detail_id` ASC),
  CONSTRAINT `FK_ENQUIRYATTACH_ENQDETID`
    FOREIGN KEY (`enquiry_detail_id`)
    REFERENCES `efsdev`.`efs_enquiry_detail` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_enquiry_container`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_enquiry_container` (
  `id` BIGINT(20) NOT NULL,
  `container_code` VARCHAR(255) NULL DEFAULT NULL,
  `container_name` VARCHAR(255) NULL DEFAULT NULL,
  `no_of_container` INT(11) NULL DEFAULT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  `enquiry_detail_id` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `FK_ENQUIRYCONTAI_ENQDETID` (`enquiry_detail_id` ASC),
  CONSTRAINT `FK_ENQUIRYCONTAI_ENQDETID`
    FOREIGN KEY (`enquiry_detail_id`)
    REFERENCES `efsdev`.`efs_enquiry_detail` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_enquiry_dimensions`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_enquiry_dimensions` (
  `id` BIGINT(20) NOT NULL,
  `gross_weight` DOUBLE NULL DEFAULT NULL,
  `gross_weight_in_kg` DOUBLE NULL DEFAULT NULL,
  `hight` INT(11) NULL DEFAULT NULL,
  `legth` INT(11) NULL DEFAULT NULL,
  `no_of_pieces` INT(11) NULL DEFAULT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  `vol_weight` DOUBLE NULL DEFAULT NULL,
  `width` INT(11) NULL DEFAULT NULL,
  `enquiry_detail_id` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `FK_ENQUIRYDIMENSION_ENQDETID` (`enquiry_detail_id` ASC),
  CONSTRAINT `FK_ENQUIRYDIMENSION_ENQDETID`
    FOREIGN KEY (`enquiry_detail_id`)
    REFERENCES `efsdev`.`efs_enquiry_detail` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_enquiry_service_mapping`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_enquiry_service_mapping` (
  `id` BIGINT(20) NOT NULL,
  `import_export` VARCHAR(20) NOT NULL,
  `service` VARCHAR(20) NOT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  `service_master_id` BIGINT(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `UK_ENQUIRY_SERVICE_MAPPING` (`service` ASC, `import_export` ASC),
  UNIQUE INDEX `UK_ENQSERVICE_SERVICEID` (`service_master_id` ASC),
  CONSTRAINT `FK_ENQSERVICE_SERVICEID`
    FOREIGN KEY (`service_master_id`)
    REFERENCES `efsdev`.`efs_service_master` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_value_added_service`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_value_added_service` (
  `id` BIGINT(20) NOT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `value_added_code` VARCHAR(255) NOT NULL,
  `value_added_name` VARCHAR(255) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `UK_VAS_VASNAME` (`value_added_name` ASC),
  UNIQUE INDEX `UK_VAS_VASCODE` (`value_added_code` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_enquiry_vat_service`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_enquiry_vat_service` (
  `id` BIGINT(20) NOT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `value_added_code` VARCHAR(100) NULL DEFAULT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  `enquiry_detail_id` BIGINT(20) NULL DEFAULT NULL,
  `value_added_id` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `FK_ENQVAS_ENQID` (`enquiry_detail_id` ASC),
  INDEX `FK_ENQVAS_VALUEADDEDID` (`value_added_id` ASC),
  CONSTRAINT `FK_ENQVAS_ENQID`
    FOREIGN KEY (`enquiry_detail_id`)
    REFERENCES `efsdev`.`efs_enquiry_detail` (`id`),
  CONSTRAINT `FK_ENQVAS_VALUEADDEDID`
    FOREIGN KEY (`value_added_id`)
    REFERENCES `efsdev`.`efs_value_added_service` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_essential_charge_master`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_essential_charge_master` (
  `id` BIGINT(20) NOT NULL,
  `company_code` VARCHAR(30) NOT NULL,
  `country_code` VARCHAR(30) NOT NULL,
  `crn` VARCHAR(255) NULL DEFAULT NULL,
  `destination_code` VARCHAR(30) NULL DEFAULT NULL,
  `hazardous` VARCHAR(10) NULL DEFAULT NULL,
  `location_code` VARCHAR(30) NOT NULL,
  `origin_code` VARCHAR(30) NULL DEFAULT NULL,
  `ppcc` VARCHAR(10) NULL DEFAULT NULL,
  `routed` VARCHAR(10) NULL DEFAULT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `tos_code` VARCHAR(255) NULL DEFAULT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  `charge_id` BIGINT(20) NULL DEFAULT NULL,
  `company_id` BIGINT(20) NOT NULL,
  `country_id` BIGINT(20) NOT NULL,
  `destination__id` BIGINT(20) NULL DEFAULT NULL,
  `location_id` BIGINT(20) NOT NULL,
  `origin_id` BIGINT(20) NULL DEFAULT NULL,
  `service_id` BIGINT(20) NULL DEFAULT NULL,
  `tos_id` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `UK_ESSENTIAL_CHARGE_MASTER` (`service_id` ASC, `charge_id` ASC, `crn` ASC, `origin_id` ASC, `destination__id` ASC, `tos_id` ASC, `ppcc` ASC, `routed` ASC, `hazardous` ASC),
  INDEX `FK_ESSENTIALCM_CHARGEID` (`charge_id` ASC),
  INDEX `FK_ESSENTIALCM_COMPID` (`company_id` ASC),
  INDEX `FK_ESSENTIALCM_COUNTID` (`country_id` ASC),
  INDEX `FK_ESSENTIALCM_DESTID` (`destination__id` ASC),
  INDEX `FK_ESSENTIALCM_LOCID` (`location_id` ASC),
  INDEX `FK_ESSENTIALCM_ORIGINID` (`origin_id` ASC),
  INDEX `FK_ESSENTIALCM_TOSID` (`tos_id` ASC),
  CONSTRAINT `FK_ESSENTIALCM_CHARGEID`
    FOREIGN KEY (`charge_id`)
    REFERENCES `efsdev`.`efs_charge_master` (`id`),
  CONSTRAINT `FK_ESSENTIALCM_COMPID`
    FOREIGN KEY (`company_id`)
    REFERENCES `efsdev`.`efs_company_master` (`id`),
  CONSTRAINT `FK_ESSENTIALCM_COUNTID`
    FOREIGN KEY (`country_id`)
    REFERENCES `efsdev`.`efs_country_master` (`id`),
  CONSTRAINT `FK_ESSENTIALCM_DESTID`
    FOREIGN KEY (`destination__id`)
    REFERENCES `efsdev`.`efs_port_master` (`id`),
  CONSTRAINT `FK_ESSENTIALCM_LOCID`
    FOREIGN KEY (`location_id`)
    REFERENCES `efsdev`.`efs_location_master` (`id`),
  CONSTRAINT `FK_ESSENTIALCM_ORIGINID`
    FOREIGN KEY (`origin_id`)
    REFERENCES `efsdev`.`efs_port_master` (`id`),
  CONSTRAINT `FK_ESSENTIALCM_SERVICEID`
    FOREIGN KEY (`service_id`)
    REFERENCES `efsdev`.`efs_service_master` (`id`),
  CONSTRAINT `FK_ESSENTIALCM_TOSID`
    FOREIGN KEY (`tos_id`)
    REFERENCES `efsdev`.`efs_tos_master` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_feature`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_feature` (
  `id` BIGINT(20) NOT NULL,
  `is_create` VARCHAR(10) NOT NULL,
  `is_delete` VARCHAR(10) NOT NULL,
  `is_download` VARCHAR(10) NOT NULL,
  `feature_name` VARCHAR(100) NOT NULL,
  `feature_value` VARCHAR(100) NOT NULL,
  `is_applicable` VARCHAR(10) NOT NULL,
  `is_list` VARCHAR(10) NOT NULL,
  `is_modify` VARCHAR(10) NOT NULL,
  `is_view` VARCHAR(10) NOT NULL,
  `parent_id` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `UK_FEATURE_FEATUREVALUE` (`feature_value` ASC),
  INDEX `FKgpijnsre2ubrmvc5uicfj6yuh` (`parent_id` ASC),
  CONSTRAINT `FKgpijnsre2ubrmvc5uicfj6yuh`
    FOREIGN KEY (`parent_id`)
    REFERENCES `efsdev`.`efs_feature` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_flex_column_master`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_flex_column_master` (
  `id` BIGINT(20) NOT NULL,
  `default_value` VARCHAR(255) NULL DEFAULT NULL,
  `drop_down_list` VARCHAR(255) NULL DEFAULT NULL,
  `flex_data_type` VARCHAR(255) NULL DEFAULT NULL,
  `flex_key` VARCHAR(255) NULL DEFAULT NULL,
  `flex_label` VARCHAR(255) NULL DEFAULT NULL,
  `flex_table_name` VARCHAR(255) NULL DEFAULT NULL,
  `is_required` VARCHAR(255) NULL DEFAULT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `UK_FLXCOLMS_TBLKEY` (`flex_table_name` ASC, `flex_key` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_flex_column_value`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_flex_column_value` (
  `id` BIGINT(20) NOT NULL,
  `flex_value` VARCHAR(255) NULL DEFAULT NULL,
  `row_id` BIGINT(20) NULL DEFAULT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  `flex_column_master_id` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `UK_FLXCOLVAL_TBLVAL` (`flex_column_master_id` ASC, `row_id` ASC),
  CONSTRAINT `FK_FLXCOLVAL_FLXCOL`
    FOREIGN KEY (`flex_column_master_id`)
    REFERENCES `efsdev`.`efs_flex_column_master` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_flight_plan_connection`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_flight_plan_connection` (
  `id` BIGINT(20) NOT NULL,
  `eta` DATETIME(6) NULL DEFAULT NULL,
  `etd` DATETIME(6) NULL DEFAULT NULL,
  `flight_no` VARCHAR(255) NULL DEFAULT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  `carrier_id` BIGINT(20) NULL DEFAULT NULL,
  `flight_plan_id` BIGINT(20) NULL DEFAULT NULL,
  `pod_id` BIGINT(20) NULL DEFAULT NULL,
  `pol_id` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `UK_FLIGHTP_FLIGHTCON` (`carrier_id` ASC, `pol_id` ASC, `pod_id` ASC, `etd` ASC, `eta` ASC),
  INDEX `FK_FLIGHTPCONN_FLIGHTP` (`flight_plan_id` ASC),
  INDEX `FK_FLIGHTPCONN_PODID` (`pod_id` ASC),
  INDEX `FK_FLIGHTPCONN_POLID` (`pol_id` ASC),
  CONSTRAINT `FK_FLIGHTPCONN_CARRIERID`
    FOREIGN KEY (`carrier_id`)
    REFERENCES `efsdev`.`efs_carrier_master` (`id`),
  CONSTRAINT `FK_FLIGHTPCONN_FLIGHTP`
    FOREIGN KEY (`flight_plan_id`)
    REFERENCES `efsdev`.`efs_flight_plan` (`id`),
  CONSTRAINT `FK_FLIGHTPCONN_PODID`
    FOREIGN KEY (`pod_id`)
    REFERENCES `efsdev`.`efs_port_master` (`id`),
  CONSTRAINT `FK_FLIGHTPCONN_POLID`
    FOREIGN KEY (`pol_id`)
    REFERENCES `efsdev`.`efs_port_master` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_general_note_master`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_general_note_master` (
  `id` BIGINT(20) NOT NULL,
  `category` VARCHAR(50) NULL DEFAULT NULL,
  `location_code` VARCHAR(30) NULL DEFAULT NULL,
  `note` VARCHAR(4000) NULL DEFAULT NULL,
  `service_code` VARCHAR(30) NULL DEFAULT NULL,
  `sub_category` VARCHAR(50) NULL DEFAULT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  `location_id` BIGINT(20) NULL DEFAULT NULL,
  `service_master_id` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `FK_GENNOTEMAS_LOCATIONID` (`location_id` ASC),
  INDEX `FK_GENNOTEMAS_SERVICEID` (`service_master_id` ASC),
  CONSTRAINT `FK_GENNOTEMAS_LOCATIONID`
    FOREIGN KEY (`location_id`)
    REFERENCES `efsdev`.`efs_location_master` (`id`),
  CONSTRAINT `FK_GENNOTEMAS_SERVICEID`
    FOREIGN KEY (`service_master_id`)
    REFERENCES `efsdev`.`efs_service_master` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_general_sub_ledger`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_general_sub_ledger` (
  `id` BIGINT(20) NOT NULL,
  `company_code` VARCHAR(30) NOT NULL,
  `gl_sub_ledger_code` VARCHAR(30) NOT NULL,
  `gl_sub_ledger_name` VARCHAR(100) NOT NULL,
  `location_code` VARCHAR(30) NOT NULL,
  `gl_sub_ledger_party_id` BIGINT(20) NOT NULL,
  `status` VARCHAR(10) NOT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  `company_id` BIGINT(20) NOT NULL,
  `general_ledger_account_id` BIGINT(20) NULL DEFAULT NULL,
  `location_id` BIGINT(20) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `FK_GLSUBLED_COMPANY` (`company_id` ASC),
  INDEX `FK_GLSUBLED_GENLEDACC` (`general_ledger_account_id` ASC),
  INDEX `FK_GLSUBLED_LOCATION` (`location_id` ASC),
  CONSTRAINT `FK_GLSUBLED_COMPANY`
    FOREIGN KEY (`company_id`)
    REFERENCES `efsdev`.`efs_company_master` (`id`),
  CONSTRAINT `FK_GLSUBLED_GENLEDACC`
    FOREIGN KEY (`general_ledger_account_id`)
    REFERENCES `efsdev`.`efs_general_ledger_account` (`id`),
  CONSTRAINT `FK_GLSUBLED_LOCATION`
    FOREIGN KEY (`location_id`)
    REFERENCES `efsdev`.`efs_location_master` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_gl_charge_mapping_master`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_gl_charge_mapping_master` (
  `id` BIGINT(20) NOT NULL,
  `charge_code` VARCHAR(30) NOT NULL,
  `service_code` VARCHAR(30) NOT NULL,
  `status` VARCHAR(10) NOT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  `asset_ledger_id` BIGINT(20) NULL DEFAULT NULL,
  `asset_sub_ledger_id` BIGINT(20) NULL DEFAULT NULL,
  `charge_id` BIGINT(20) NOT NULL,
  `cost_ledger_id` BIGINT(20) NULL DEFAULT NULL,
  `cost_sub_ledger_id` BIGINT(20) NULL DEFAULT NULL,
  `currency_id` BIGINT(20) NOT NULL,
  `liabilities_ledger_id` BIGINT(20) NULL DEFAULT NULL,
  `liabilities_sub_ledger_id` BIGINT(20) NULL DEFAULT NULL,
  `neutral_ledger_id` BIGINT(20) NULL DEFAULT NULL,
  `neutral_sub_ledger_id` BIGINT(20) NULL DEFAULT NULL,
  `revenue_ledger_id` BIGINT(20) NULL DEFAULT NULL,
  `revenue_sub_ledger_id` BIGINT(20) NULL DEFAULT NULL,
  `service_id` BIGINT(20) NOT NULL,
  `transit_cost_ledger_id` BIGINT(20) NULL DEFAULT NULL,
  `transit_cost_sub_ledger_id` BIGINT(20) NULL DEFAULT NULL,
  `transit_ledger_id` BIGINT(20) NULL DEFAULT NULL,
  `transit_sub_ledger_id` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `FK_GLCHARMAP_ASSET` (`asset_ledger_id` ASC),
  INDEX `FK_GLCHARMAP_ASSETSUB` (`asset_sub_ledger_id` ASC),
  INDEX `FK_GLCHARMAP_CHARFID` (`charge_id` ASC),
  INDEX `FK_GLCHARMAP_COST` (`cost_ledger_id` ASC),
  INDEX `FK_GLCHARMAP_SUBCOST` (`cost_sub_ledger_id` ASC),
  INDEX `FK_GLCHARMAP_CURREN` (`currency_id` ASC),
  INDEX `FK_GLCHARMAP_LIABILITIES` (`liabilities_ledger_id` ASC),
  INDEX `FK_GLCHARMAP_LIABILITIESSUB` (`liabilities_sub_ledger_id` ASC),
  INDEX `FK_GLCHARMAP_NEUTRAL` (`neutral_ledger_id` ASC),
  INDEX `FK_GLCHARMAP_NEUTRALSUB` (`neutral_sub_ledger_id` ASC),
  INDEX `FK_GLCHARMAP_REVENUE` (`revenue_ledger_id` ASC),
  INDEX `FK_GLCHARMAP_SUBREVENUE` (`revenue_sub_ledger_id` ASC),
  INDEX `FK_GLCHARMAP_SERVIID` (`service_id` ASC),
  INDEX `FK_GLCHARMAP_TRANSITCOST` (`transit_cost_ledger_id` ASC),
  INDEX `FK_GLCHARMAP_TRANSITCOSTSUB` (`transit_cost_sub_ledger_id` ASC),
  INDEX `FK_GLCHARMAP_TRANSIT` (`transit_ledger_id` ASC),
  INDEX `FK_GLCHARMAP_TRANSITSUB` (`transit_sub_ledger_id` ASC),
  CONSTRAINT `FK_GLCHARMAP_ASSET`
    FOREIGN KEY (`asset_ledger_id`)
    REFERENCES `efsdev`.`efs_general_ledger_account` (`id`),
  CONSTRAINT `FK_GLCHARMAP_ASSETSUB`
    FOREIGN KEY (`asset_sub_ledger_id`)
    REFERENCES `efsdev`.`efs_general_sub_ledger` (`id`),
  CONSTRAINT `FK_GLCHARMAP_CHARFID`
    FOREIGN KEY (`charge_id`)
    REFERENCES `efsdev`.`efs_charge_master` (`id`),
  CONSTRAINT `FK_GLCHARMAP_COST`
    FOREIGN KEY (`cost_ledger_id`)
    REFERENCES `efsdev`.`efs_general_ledger_account` (`id`),
  CONSTRAINT `FK_GLCHARMAP_CURREN`
    FOREIGN KEY (`currency_id`)
    REFERENCES `efsdev`.`efs_currency_master` (`id`),
  CONSTRAINT `FK_GLCHARMAP_LIABILITIES`
    FOREIGN KEY (`liabilities_ledger_id`)
    REFERENCES `efsdev`.`efs_general_ledger_account` (`id`),
  CONSTRAINT `FK_GLCHARMAP_LIABILITIESSUB`
    FOREIGN KEY (`liabilities_sub_ledger_id`)
    REFERENCES `efsdev`.`efs_general_sub_ledger` (`id`),
  CONSTRAINT `FK_GLCHARMAP_NEUTRAL`
    FOREIGN KEY (`neutral_ledger_id`)
    REFERENCES `efsdev`.`efs_general_ledger_account` (`id`),
  CONSTRAINT `FK_GLCHARMAP_NEUTRALSUB`
    FOREIGN KEY (`neutral_sub_ledger_id`)
    REFERENCES `efsdev`.`efs_general_sub_ledger` (`id`),
  CONSTRAINT `FK_GLCHARMAP_REVENUE`
    FOREIGN KEY (`revenue_ledger_id`)
    REFERENCES `efsdev`.`efs_general_ledger_account` (`id`),
  CONSTRAINT `FK_GLCHARMAP_SERVIID`
    FOREIGN KEY (`service_id`)
    REFERENCES `efsdev`.`efs_service_master` (`id`),
  CONSTRAINT `FK_GLCHARMAP_SUBCOST`
    FOREIGN KEY (`cost_sub_ledger_id`)
    REFERENCES `efsdev`.`efs_general_sub_ledger` (`id`),
  CONSTRAINT `FK_GLCHARMAP_SUBREVENUE`
    FOREIGN KEY (`revenue_sub_ledger_id`)
    REFERENCES `efsdev`.`efs_general_sub_ledger` (`id`),
  CONSTRAINT `FK_GLCHARMAP_TRANSIT`
    FOREIGN KEY (`transit_ledger_id`)
    REFERENCES `efsdev`.`efs_general_ledger_account` (`id`),
  CONSTRAINT `FK_GLCHARMAP_TRANSITCOST`
    FOREIGN KEY (`transit_cost_ledger_id`)
    REFERENCES `efsdev`.`efs_general_ledger_account` (`id`),
  CONSTRAINT `FK_GLCHARMAP_TRANSITCOSTSUB`
    FOREIGN KEY (`transit_cost_sub_ledger_id`)
    REFERENCES `efsdev`.`efs_general_sub_ledger` (`id`),
  CONSTRAINT `FK_GLCHARMAP_TRANSITSUB`
    FOREIGN KEY (`transit_sub_ledger_id`)
    REFERENCES `efsdev`.`efs_general_sub_ledger` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_win_status_response`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_win_status_response` (
  `id` BIGINT(20) NOT NULL,
  `awb_id` BIGINT(20) NULL DEFAULT NULL,
  `awb_number` VARCHAR(255) NULL DEFAULT NULL,
  `consol_uid` VARCHAR(255) NULL DEFAULT NULL,
  `created_date` VARCHAR(255) NULL DEFAULT NULL,
  `destination_name` VARCHAR(255) NULL DEFAULT NULL,
  `origin_name` VARCHAR(255) NULL DEFAULT NULL,
  `status` VARCHAR(255) NULL DEFAULT NULL,
  `updated_date` VARCHAR(255) NULL DEFAULT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_hawb_win_response`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_hawb_win_response` (
  `id` BIGINT(20) NOT NULL,
  `hawb_id` INT(11) NULL DEFAULT NULL,
  `hawb_number` VARCHAR(255) NULL DEFAULT NULL,
  `status` VARCHAR(255) NULL DEFAULT NULL,
  `win_status_id` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `WIN_WEB_STATUS_ID` (`win_status_id` ASC),
  CONSTRAINT `WIN_WEB_STATUS_ID`
    FOREIGN KEY (`win_status_id`)
    REFERENCES `efsdev`.`efs_win_status_response` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_http_message`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_http_message` (
  `id` BIGINT(20) NOT NULL,
  `context_path` VARCHAR(300) NULL DEFAULT NULL,
  `create_date` DATETIME(6) NULL DEFAULT NULL,
  `ip_address` VARCHAR(50) NULL DEFAULT NULL,
  `last_updated_date` DATETIME(6) NULL DEFAULT NULL,
  `path_info` VARCHAR(500) NULL DEFAULT NULL,
  `request_content` LONGBLOB NULL DEFAULT NULL,
  `http_method` VARCHAR(30) NULL DEFAULT NULL,
  `request_url` VARCHAR(3000) NULL DEFAULT NULL,
  `response_code` INT(11) NULL DEFAULT NULL,
  `response_content` LONGBLOB NULL DEFAULT NULL,
  `servlet_path` VARCHAR(300) NULL DEFAULT NULL,
  `track_id` VARCHAR(100) NULL DEFAULT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_iata_rate_master`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_iata_rate_master` (
  `id` BIGINT(20) NOT NULL,
  `company_code` VARCHAR(30) NOT NULL,
  `country_code` VARCHAR(30) NOT NULL,
  `currency_code` VARCHAR(30) NULL DEFAULT NULL,
  `location_code` VARCHAR(30) NOT NULL,
  `online_offline` VARCHAR(30) NULL DEFAULT NULL,
  `remark` VARCHAR(4000) NULL DEFAULT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `valid_from_date` DATETIME(6) NOT NULL,
  `valid_to_date` DATETIME(6) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  `carrier_id` BIGINT(20) NOT NULL,
  `company_id` BIGINT(20) NOT NULL,
  `country_id` BIGINT(20) NOT NULL,
  `currency_id` BIGINT(20) NULL DEFAULT NULL,
  `location_id` BIGINT(20) NOT NULL,
  `pol` BIGINT(20) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `FK_IATARATE_CARRIERID` (`carrier_id` ASC),
  INDEX `FK_IATARATE_COMPID` (`company_id` ASC),
  INDEX `FK_IATARATE_COUNTID` (`country_id` ASC),
  INDEX `FK_IATARATE_CURREID` (`currency_id` ASC),
  INDEX `FK_IATARATE_LOCTID` (`location_id` ASC),
  INDEX `FK_IATARATE_POLID` (`pol` ASC),
  CONSTRAINT `FK_IATARATE_CARRIERID`
    FOREIGN KEY (`carrier_id`)
    REFERENCES `efsdev`.`efs_carrier_master` (`id`),
  CONSTRAINT `FK_IATARATE_COMPID`
    FOREIGN KEY (`company_id`)
    REFERENCES `efsdev`.`efs_company_master` (`id`),
  CONSTRAINT `FK_IATARATE_COUNTID`
    FOREIGN KEY (`country_id`)
    REFERENCES `efsdev`.`efs_country_master` (`id`),
  CONSTRAINT `FK_IATARATE_CURREID`
    FOREIGN KEY (`currency_id`)
    REFERENCES `efsdev`.`efs_currency_master` (`id`),
  CONSTRAINT `FK_IATARATE_LOCTID`
    FOREIGN KEY (`location_id`)
    REFERENCES `efsdev`.`efs_location_master` (`id`),
  CONSTRAINT `FK_IATARATE_POLID`
    FOREIGN KEY (`pol`)
    REFERENCES `efsdev`.`efs_port_master` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_iata_rate_attachment`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_iata_rate_attachment` (
  `id` BIGINT(20) NOT NULL,
  `company_code` VARCHAR(30) NOT NULL,
  `country_code` VARCHAR(30) NOT NULL,
  `file_attached` LONGBLOB NULL DEFAULT NULL,
  `file_content_type` VARCHAR(100) NULL DEFAULT NULL,
  `file_name` VARCHAR(100) NULL DEFAULT NULL,
  `location_code` VARCHAR(30) NOT NULL,
  `ref_no` VARCHAR(100) NULL DEFAULT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  `company_id` BIGINT(20) NOT NULL,
  `country_id` BIGINT(20) NOT NULL,
  `iata_rate_id` BIGINT(20) NULL DEFAULT NULL,
  `location_id` BIGINT(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `UK_IATARATEATT_REF` (`ref_no` ASC, `iata_rate_id` ASC),
  INDEX `FK_IATARATEATT_COMPID` (`company_id` ASC),
  INDEX `FFK_IATARATEATT_COUNTID` (`country_id` ASC),
  INDEX `FK_IATARATEATT_IATARATEID` (`iata_rate_id` ASC),
  INDEX `FK_IATARATEATT_LOCTID` (`location_id` ASC),
  CONSTRAINT `FFK_IATARATEATT_COUNTID`
    FOREIGN KEY (`country_id`)
    REFERENCES `efsdev`.`efs_country_master` (`id`),
  CONSTRAINT `FK_IATARATEATT_COMPID`
    FOREIGN KEY (`company_id`)
    REFERENCES `efsdev`.`efs_company_master` (`id`),
  CONSTRAINT `FK_IATARATEATT_IATARATEID`
    FOREIGN KEY (`iata_rate_id`)
    REFERENCES `efsdev`.`efs_iata_rate_master` (`id`),
  CONSTRAINT `FK_IATARATEATT_LOCTID`
    FOREIGN KEY (`location_id`)
    REFERENCES `efsdev`.`efs_location_master` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_iata_rate_charge`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_iata_rate_charge` (
  `id` BIGINT(20) NOT NULL,
  `amount` DOUBLE NULL DEFAULT NULL,
  `company_code` VARCHAR(30) NOT NULL,
  `country_code` VARCHAR(30) NOT NULL,
  `location_code` VARCHAR(30) NOT NULL,
  `min_amount` DOUBLE NULL DEFAULT NULL,
  `normal` DOUBLE NULL DEFAULT NULL,
  `plus_100` DOUBLE NULL DEFAULT NULL,
  `plus_1000` DOUBLE NULL DEFAULT NULL,
  `plus_300` DOUBLE NULL DEFAULT NULL,
  `plus_45` DOUBLE NULL DEFAULT NULL,
  `plus_500` DOUBLE NULL DEFAULT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  `company_id` BIGINT(20) NOT NULL,
  `country_id` BIGINT(20) NOT NULL,
  `iata_rate_id` BIGINT(20) NULL DEFAULT NULL,
  `location_id` BIGINT(20) NOT NULL,
  `pod` BIGINT(20) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `FK_IATARATECHARGE_COMPID` (`company_id` ASC),
  INDEX `FK_IATARATECHARGE_COUNTID` (`country_id` ASC),
  INDEX `FK_IATARATECHARGE_IATARATEID` (`iata_rate_id` ASC),
  INDEX `FK_IATARATECHARGE_LOCTID` (`location_id` ASC),
  INDEX `FK_IATARATECHARGE_PODID` (`pod` ASC),
  CONSTRAINT `FK_IATARATECHARGE_COMPID`
    FOREIGN KEY (`company_id`)
    REFERENCES `efsdev`.`efs_company_master` (`id`),
  CONSTRAINT `FK_IATARATECHARGE_COUNTID`
    FOREIGN KEY (`country_id`)
    REFERENCES `efsdev`.`efs_country_master` (`id`),
  CONSTRAINT `FK_IATARATECHARGE_IATARATEID`
    FOREIGN KEY (`iata_rate_id`)
    REFERENCES `efsdev`.`efs_iata_rate_master` (`id`),
  CONSTRAINT `FK_IATARATECHARGE_LOCTID`
    FOREIGN KEY (`location_id`)
    REFERENCES `efsdev`.`efs_location_master` (`id`),
  CONSTRAINT `FK_IATARATECHARGE_PODID`
    FOREIGN KEY (`pod`)
    REFERENCES `efsdev`.`efs_port_master` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_party_account_master`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_party_account_master` (
  `id` BIGINT(20) NOT NULL,
  `account_code` VARCHAR(30) NOT NULL,
  `bill_party_code` VARCHAR(100) NULL DEFAULT NULL,
  `company_code` VARCHAR(100) NULL DEFAULT NULL,
  `country_code` VARCHAR(10) NULL DEFAULT NULL,
  `currency_code` VARCHAR(10) NULL DEFAULT NULL,
  `is_tax_exempted` VARCHAR(10) NULL DEFAULT NULL,
  `location_code` VARCHAR(100) NULL DEFAULT NULL,
  `status` VARCHAR(10) NOT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `term_code` VARCHAR(255) NULL DEFAULT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  `account_master_id` BIGINT(20) NULL DEFAULT NULL,
  `bill_party_account_id` BIGINT(20) NULL DEFAULT NULL,
  `bill_party_id` BIGINT(20) NULL DEFAULT NULL,
  `company_id` BIGINT(20) NULL DEFAULT NULL,
  `country_id` BIGINT(20) NULL DEFAULT NULL,
  `currency_id` BIGINT(20) NULL DEFAULT NULL,
  `location_id` BIGINT(20) NULL DEFAULT NULL,
  `party_id` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `UK_PARTY_ACCOUNT` (`party_id` ASC, `location_id` ASC, `account_master_id` ASC, `currency_id` ASC),
  INDEX `FK_PARTYACCOUNT_ACCOUNTID` (`account_master_id` ASC),
  INDEX `FK_PARTYACC_BILLPARTYACC` (`bill_party_account_id` ASC),
  INDEX `FK_PARTYACC_BILLINGPARTY` (`bill_party_id` ASC),
  INDEX `FK_PARTYACCOUNT_COMPANYID` (`company_id` ASC),
  INDEX `FK_PARTYACCOUNT_COUNTRYID` (`country_id` ASC),
  INDEX `FK_PARTYACCOUNT_CURRENCYID` (`currency_id` ASC),
  INDEX `FK_PARTYACCOUNT_LOCATIONID` (`location_id` ASC),
  CONSTRAINT `FK_PARTYACCOUNT_ACCOUNTID`
    FOREIGN KEY (`account_master_id`)
    REFERENCES `efsdev`.`efs_general_ledger_account` (`id`),
  CONSTRAINT `FK_PARTYACCOUNT_COMPANYID`
    FOREIGN KEY (`company_id`)
    REFERENCES `efsdev`.`efs_company_master` (`id`),
  CONSTRAINT `FK_PARTYACCOUNT_COUNTRYID`
    FOREIGN KEY (`country_id`)
    REFERENCES `efsdev`.`efs_country_master` (`id`),
  CONSTRAINT `FK_PARTYACCOUNT_CURRENCYID`
    FOREIGN KEY (`currency_id`)
    REFERENCES `efsdev`.`efs_currency_master` (`id`),
  CONSTRAINT `FK_PARTYACCOUNT_LOCATIONID`
    FOREIGN KEY (`location_id`)
    REFERENCES `efsdev`.`efs_location_master` (`id`),
  CONSTRAINT `FK_PARTYACCOUNT_PARTYID`
    FOREIGN KEY (`party_id`)
    REFERENCES `efsdev`.`efs_party_master` (`id`),
  CONSTRAINT `FK_PARTYACC_BILLINGPARTY`
    FOREIGN KEY (`bill_party_id`)
    REFERENCES `efsdev`.`efs_party_master` (`id`),
  CONSTRAINT `FK_PARTYACC_BILLPARTYACC`
    FOREIGN KEY (`bill_party_account_id`)
    REFERENCES `efsdev`.`efs_party_account_master` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_reason_master`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_reason_master` (
  `id` BIGINT(20) NOT NULL,
  `description` VARCHAR(2000) NULL DEFAULT NULL,
  `reason_code` VARCHAR(30) NOT NULL,
  `reason_name` VARCHAR(100) NOT NULL,
  `status` VARCHAR(10) NOT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `UK_REASON_REASONCODE` (`reason_code` ASC),
  UNIQUE INDEX `UK_REASON_REASONNAME` (`reason_name` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_invoice_credit_note`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_invoice_credit_note` (
  `id` BIGINT(20) NOT NULL,
  `adjustment_daybook_code` VARCHAR(100) NULL DEFAULT NULL,
  `adjustment_amount` DOUBLE NULL DEFAULT NULL,
  `adjustment_currency_amount` DOUBLE NULL DEFAULT NULL,
  `adjustment_invoice_no` VARCHAR(255) NULL DEFAULT NULL,
  `amount_received` DOUBLE NULL DEFAULT NULL,
  `billing_currency_code` VARCHAR(100) NULL DEFAULT NULL,
  `currency_amount` DOUBLE NULL DEFAULT NULL,
  `customer_agent` VARCHAR(10) NOT NULL,
  `daybook_code` VARCHAR(100) NULL DEFAULT NULL,
  `document_type_code` VARCHAR(100) NULL DEFAULT NULL,
  `due_date` DATETIME(6) NULL DEFAULT NULL,
  `invoice_credit_note_date` DATETIME(6) NOT NULL,
  `invoice_credit_note_no` VARCHAR(255) NULL DEFAULT NULL,
  `invoice_credit_note_status` VARCHAR(30) NOT NULL,
  `local_amount` DOUBLE NULL DEFAULT NULL,
  `local_currency_code` VARCHAR(100) NULL DEFAULT NULL,
  `location_code` VARCHAR(100) NULL DEFAULT NULL,
  `master_uid` VARCHAR(255) NULL DEFAULT NULL,
  `net_currency_amount` DOUBLE NULL DEFAULT NULL,
  `net_local_amount` DOUBLE NULL DEFAULT NULL,
  `note` LONGTEXT NULL DEFAULT NULL,
  `outstanding_amount` DOUBLE NULL DEFAULT NULL,
  `party_code` VARCHAR(100) NULL DEFAULT NULL,
  `reason` VARCHAR(255) NULL DEFAULT NULL,
  `reason_code` VARCHAR(255) NULL DEFAULT NULL,
  `roe` DOUBLE NOT NULL,
  `shipment_uid` VARCHAR(255) NULL DEFAULT NULL,
  `subledger` VARCHAR(255) NULL DEFAULT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `total_credit_currency_amount` DOUBLE NULL DEFAULT NULL,
  `total_credit_local_amount` DOUBLE NULL DEFAULT NULL,
  `total_debit_currency_amount` DOUBLE NULL DEFAULT NULL,
  `total_debit_local_amount` DOUBLE NULL DEFAULT NULL,
  `total_tax_amount` DOUBLE NULL DEFAULT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  `location_id` BIGINT(20) NULL DEFAULT NULL,
  `adjustment_daybook_id` BIGINT(20) NULL DEFAULT NULL,
  `billing_currency_id` BIGINT(20) NOT NULL,
  `daybook_id` BIGINT(20) NOT NULL,
  `document_type_id` BIGINT(20) NULL DEFAULT NULL,
  `local_currency_id` BIGINT(20) NULL DEFAULT NULL,
  `party_id` BIGINT(20) NOT NULL,
  `party_account_id` BIGINT(20) NOT NULL,
  `party_address_id` BIGINT(20) NOT NULL,
  `reason_id` BIGINT(20) NULL DEFAULT NULL,
  `shipment_service_detail_id` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `IDX_INVOICE_CRDTNOTE_DATE` (`invoice_credit_note_date` ASC),
  INDEX `IDX_INVOICE_CRDTNOTE_PARTYID` (`party_id` ASC),
  INDEX `IDX_INVOICE_CRDTNOTE_BILLCURID` (`billing_currency_id` ASC),
  INDEX `FK_INV_CRDNTE_LOCID` (`location_id` ASC),
  INDEX `FK_INV_CRDNTE_ADJDAYBOOKID` (`adjustment_daybook_id` ASC),
  INDEX `FK_INV_CRDNTE_DAYBOOKID` (`daybook_id` ASC),
  INDEX `FK_INV_CRDNTE_DOCUMENTID` (`document_type_id` ASC),
  INDEX `FK_INV_CRDNTE_LOCCURID` (`local_currency_id` ASC),
  INDEX `FK_INV_CRDNTE_PARTYACCID` (`party_account_id` ASC),
  INDEX `FK_INV_CRDNTE_PARTYADDR` (`party_address_id` ASC),
  INDEX `FK_INV_CRN_REASONID` (`reason_id` ASC),
  INDEX `FK_INV_CRDNTE_SERVICEDETID` (`shipment_service_detail_id` ASC),
  CONSTRAINT `FK_INV_CRDNTE_ADJDAYBOOKID`
    FOREIGN KEY (`adjustment_daybook_id`)
    REFERENCES `efsdev`.`efs_daybook_master` (`id`),
  CONSTRAINT `FK_INV_CRDNTE_BILLCURID`
    FOREIGN KEY (`billing_currency_id`)
    REFERENCES `efsdev`.`efs_currency_master` (`id`),
  CONSTRAINT `FK_INV_CRDNTE_DAYBOOKID`
    FOREIGN KEY (`daybook_id`)
    REFERENCES `efsdev`.`efs_daybook_master` (`id`),
  CONSTRAINT `FK_INV_CRDNTE_DOCUMENTID`
    FOREIGN KEY (`document_type_id`)
    REFERENCES `efsdev`.`efs_document_type_master` (`id`),
  CONSTRAINT `FK_INV_CRDNTE_LOCCURID`
    FOREIGN KEY (`local_currency_id`)
    REFERENCES `efsdev`.`efs_currency_master` (`id`),
  CONSTRAINT `FK_INV_CRDNTE_LOCID`
    FOREIGN KEY (`location_id`)
    REFERENCES `efsdev`.`efs_location_master` (`id`),
  CONSTRAINT `FK_INV_CRDNTE_PARTYACCID`
    FOREIGN KEY (`party_account_id`)
    REFERENCES `efsdev`.`efs_party_account_master` (`id`),
  CONSTRAINT `FK_INV_CRDNTE_PARTYADDR`
    FOREIGN KEY (`party_address_id`)
    REFERENCES `efsdev`.`efs_address_master` (`id`),
  CONSTRAINT `FK_INV_CRDNTE_PARTYID`
    FOREIGN KEY (`party_id`)
    REFERENCES `efsdev`.`efs_party_master` (`id`),
  CONSTRAINT `FK_INV_CRDNTE_SERVICEDETID`
    FOREIGN KEY (`shipment_service_detail_id`)
    REFERENCES `efsdev`.`efs_service` (`id`),
  CONSTRAINT `FK_INV_CRN_REASONID`
    FOREIGN KEY (`reason_id`)
    REFERENCES `efsdev`.`efs_reason_master` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_invoice_cdt_note_attach`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_invoice_cdt_note_attach` (
  `id` BIGINT(20) NOT NULL,
  `file_attached` LONGBLOB NOT NULL,
  `file_content_type` VARCHAR(100) NOT NULL,
  `file_name` VARCHAR(100) NOT NULL,
  `ref_no` VARCHAR(100) NOT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  `invoice_credit_note_id` BIGINT(20) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `FK_INV_CRE_ATTC_INVOICEID` (`invoice_credit_note_id` ASC),
  CONSTRAINT `FK_INV_CRE_ATTC_INVOICEID`
    FOREIGN KEY (`invoice_credit_note_id`)
    REFERENCES `efsdev`.`efs_invoice_credit_note` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_invoice_crn_detail`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_invoice_crn_detail` (
  `id` BIGINT(20) NOT NULL,
  `account_code` VARCHAR(255) NULL DEFAULT NULL,
  `amount` DOUBLE NOT NULL,
  `amount_per_unit` DOUBLE NULL DEFAULT NULL,
  `charge_code` VARCHAR(255) NOT NULL,
  `charge_name` VARCHAR(255) NOT NULL,
  `crn` VARCHAR(10) NULL DEFAULT NULL,
  `currency_amount` DOUBLE NULL DEFAULT NULL,
  `currency_code` VARCHAR(255) NULL DEFAULT NULL,
  `debit_credit` VARCHAR(10) NOT NULL,
  `gl_charge_mapping_id` BIGINT(20) NULL DEFAULT NULL,
  `is_taxable` BIT(1) NULL DEFAULT NULL,
  `local_amount` DOUBLE NULL DEFAULT NULL,
  `provisional_item_id` BIGINT(20) NULL DEFAULT NULL,
  `roe` DOUBLE NULL DEFAULT NULL,
  `service_uid` VARCHAR(255) NULL DEFAULT NULL,
  `shipment_rate_id` BIGINT(20) NULL DEFAULT NULL,
  `sub_job_sequence` INT(11) NULL DEFAULT NULL,
  `subledger_code` VARCHAR(255) NULL DEFAULT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `unit` DOUBLE NULL DEFAULT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  `charge_id` BIGINT(20) NOT NULL,
  `currency_id` BIGINT(20) NULL DEFAULT NULL,
  `invoice_credit_note_id` BIGINT(20) NOT NULL,
  `shipment_service_detail_id` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `FK_INV_CREDET_CHARGEID` (`charge_id` ASC),
  INDEX `FK_INV_CREDET_CURRENCYID` (`currency_id` ASC),
  INDEX `FK_INV_CREDET_INVOICEID` (`invoice_credit_note_id` ASC),
  INDEX `FK_INV_CREDET_SERVICEDETAILID` (`shipment_service_detail_id` ASC),
  CONSTRAINT `FK_INV_CREDET_CHARGEID`
    FOREIGN KEY (`charge_id`)
    REFERENCES `efsdev`.`efs_charge_master` (`id`),
  CONSTRAINT `FK_INV_CREDET_CURRENCYID`
    FOREIGN KEY (`currency_id`)
    REFERENCES `efsdev`.`efs_currency_master` (`id`),
  CONSTRAINT `FK_INV_CREDET_INVOICEID`
    FOREIGN KEY (`invoice_credit_note_id`)
    REFERENCES `efsdev`.`efs_invoice_credit_note` (`id`),
  CONSTRAINT `FK_INV_CREDET_SERVICEDETAILID`
    FOREIGN KEY (`shipment_service_detail_id`)
    REFERENCES `efsdev`.`efs_service` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_service_tax_categ`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_service_tax_categ` (
  `id` BIGINT(20) NOT NULL,
  `code` VARCHAR(30) NOT NULL,
  `name` VARCHAR(100) NOT NULL,
  `status` VARCHAR(10) NOT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `UK_SERVTAXCATE_CODE` (`code` ASC),
  UNIQUE INDEX `UK_SERVTAXCATE_NAME` (`name` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_sub_ledger_master`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_sub_ledger_master` (
  `id` BIGINT(20) NOT NULL,
  `ledger_code` VARCHAR(20) NOT NULL,
  `ledger_name` VARCHAR(20) NOT NULL,
  `status` VARCHAR(10) NOT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `UK_LEDGER_ACCCODE` (`ledger_code` ASC),
  UNIQUE INDEX `UK_LEDGER_ACCNAME` (`ledger_name` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_service_tax_percentage`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_service_tax_percentage` (
  `id` BIGINT(20) NOT NULL,
  `company_code` VARCHAR(30) NOT NULL,
  `country_code` VARCHAR(30) NOT NULL,
  `note` VARCHAR(200) NULL DEFAULT NULL,
  `percentage` DOUBLE NOT NULL,
  `status` VARCHAR(10) NULL DEFAULT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `tax_code` VARCHAR(30) NOT NULL,
  `tax_name` VARCHAR(100) NOT NULL,
  `valid_from` DATETIME(6) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  `company_id` BIGINT(20) NOT NULL,
  `country_id` BIGINT(20) NOT NULL,
  `currency_id` BIGINT(20) NOT NULL,
  `gen_ldgr_id` BIGINT(20) NOT NULL,
  `service_tax_category_id` BIGINT(20) NOT NULL,
  `sub_ldgr_id` BIGINT(20) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `FK_SERVTAXPERC_COMPID` (`company_id` ASC),
  INDEX `FK_SERVTAXPERC_COUNTID` (`country_id` ASC),
  INDEX `FK_SERVTAXPERC_CURRENCYID` (`currency_id` ASC),
  INDEX `FK_SGENLEDGERID` (`gen_ldgr_id` ASC),
  INDEX `FK_SERVTAXPERC_STCID` (`service_tax_category_id` ASC),
  INDEX `FK_SLEDGERID` (`sub_ldgr_id` ASC),
  CONSTRAINT `FK_SERVTAXPERC_COMPID`
    FOREIGN KEY (`company_id`)
    REFERENCES `efsdev`.`efs_company_master` (`id`),
  CONSTRAINT `FK_SERVTAXPERC_COUNTID`
    FOREIGN KEY (`country_id`)
    REFERENCES `efsdev`.`efs_country_master` (`id`),
  CONSTRAINT `FK_SERVTAXPERC_CURRENCYID`
    FOREIGN KEY (`currency_id`)
    REFERENCES `efsdev`.`efs_currency_master` (`id`),
  CONSTRAINT `FK_SERVTAXPERC_STCID`
    FOREIGN KEY (`service_tax_category_id`)
    REFERENCES `efsdev`.`efs_service_tax_categ` (`id`),
  CONSTRAINT `FK_SGENLEDGERID`
    FOREIGN KEY (`gen_ldgr_id`)
    REFERENCES `efsdev`.`efs_general_ledger_account` (`id`),
  CONSTRAINT `FK_SLEDGERID`
    FOREIGN KEY (`sub_ldgr_id`)
    REFERENCES `efsdev`.`efs_sub_ledger_master` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_invoice_credit_note_tax`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_invoice_credit_note_tax` (
  `id` BIGINT(20) NOT NULL,
  `amount` DOUBLE NULL DEFAULT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `tax_code` VARCHAR(30) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  `inv_crn_detail_id` BIGINT(20) NOT NULL,
  `service_tax_percentage_id` BIGINT(20) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `FK_INV_CRED_DETAILS_TAX_INVID` (`inv_crn_detail_id` ASC),
  INDEX `FK_INV_SER_TAX_ID` (`service_tax_percentage_id` ASC),
  CONSTRAINT `FK_INV_CRED_DETAILS_TAX_INVID`
    FOREIGN KEY (`inv_crn_detail_id`)
    REFERENCES `efsdev`.`efs_invoice_crn_detail` (`id`),
  CONSTRAINT `FK_INV_SER_TAX_ID`
    FOREIGN KEY (`service_tax_percentage_id`)
    REFERENCES `efsdev`.`efs_service_tax_percentage` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_invoice_crn_receipt`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_invoice_crn_receipt` (
  `id` BIGINT(20) NOT NULL,
  `amount` DOUBLE NULL DEFAULT NULL,
  `currency_code` VARCHAR(100) NULL DEFAULT NULL,
  `daybook_code` VARCHAR(100) NOT NULL,
  `document_number` VARCHAR(255) NULL DEFAULT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  `daybook_id` BIGINT(20) NOT NULL,
  `currency_id` BIGINT(20) NULL DEFAULT NULL,
  `invoice_credit_note_id` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `FK_INV_REC_DAYBOOKID` (`daybook_id` ASC),
  INDEX `FK_INV_REC_CURRENCYID` (`currency_id` ASC),
  INDEX `FK_INV_REC_INVOICEID` (`invoice_credit_note_id` ASC),
  CONSTRAINT `FK_INV_REC_CURRENCYID`
    FOREIGN KEY (`currency_id`)
    REFERENCES `efsdev`.`efs_currency_master` (`id`),
  CONSTRAINT `FK_INV_REC_DAYBOOKID`
    FOREIGN KEY (`daybook_id`)
    REFERENCES `efsdev`.`efs_daybook_master` (`id`),
  CONSTRAINT `FK_INV_REC_INVOICEID`
    FOREIGN KEY (`invoice_credit_note_id`)
    REFERENCES `efsdev`.`efs_invoice_credit_note` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_language`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_language` (
  `id` BIGINT(20) NOT NULL,
  `language_name` VARCHAR(20) NOT NULL,
  `status` VARCHAR(10) NOT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `UK_h42hdmwuahu9y5jh1w74bskrl` (`language_name` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_language_detail`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_language_detail` (
  `id` BIGINT(20) NOT NULL,
  `flagged` VARCHAR(10) NULL DEFAULT NULL,
  `group_name` VARCHAR(30) NOT NULL,
  `language_code` VARCHAR(20) NOT NULL,
  `language_desc` VARCHAR(1000) NOT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  `language_id` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `UK_LANGDETAIL_CODE` (`language_id` ASC, `language_code` ASC),
  CONSTRAINT `FK_LANGUGE_LANGUAGEID`
    FOREIGN KEY (`language_id`)
    REFERENCES `efsdev`.`efs_language` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_location_setup`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_location_setup` (
  `id` BIGINT(20) NOT NULL,
  `activation_key` VARCHAR(10) NULL DEFAULT NULL,
  `address_line_1` VARCHAR(255) NULL DEFAULT NULL,
  `address_line_2` VARCHAR(255) NULL DEFAULT NULL,
  `address_line_3` VARCHAR(255) NULL DEFAULT NULL,
  `agent_file_name` VARCHAR(100) NULL DEFAULT NULL,
  `agent_port_file_name` VARCHAR(100) NULL DEFAULT NULL,
  `agent_port_uploaded_file` LONGBLOB NULL DEFAULT NULL,
  `agent_uploaded_file` LONGBLOB NULL DEFAULT NULL,
  `area` VARCHAR(255) NULL DEFAULT NULL,
  `city_code` VARCHAR(10) NULL DEFAULT NULL,
  `company_reg_name` VARCHAR(100) NULL DEFAULT NULL,
  `country_code` VARCHAR(10) NULL DEFAULT NULL,
  `current_section` VARCHAR(15) NULL DEFAULT NULL,
  `customer_file_name` VARCHAR(100) NULL DEFAULT NULL,
  `customer_uploaded_file` LONGBLOB NULL DEFAULT NULL,
  `date_format` VARCHAR(30) NULL DEFAULT NULL,
  `dial_code` VARCHAR(10) NULL DEFAULT NULL,
  `email` VARCHAR(500) NULL DEFAULT NULL,
  `export_job_date` VARCHAR(255) NULL DEFAULT NULL,
  `fin_end_date` DATETIME(6) NULL DEFAULT NULL,
  `fin_start_date` DATETIME(6) NULL DEFAULT NULL,
  `first_name` VARCHAR(255) NULL DEFAULT NULL,
  `import_job_date` VARCHAR(255) NULL DEFAULT NULL,
  `invoice_footer` VARCHAR(4000) NULL DEFAULT NULL,
  `invoice_header` VARCHAR(4000) NULL DEFAULT NULL,
  `is_head_office` VARCHAR(5) NULL DEFAULT NULL,
  `is_provisional_cost` VARCHAR(10) NULL DEFAULT NULL,
  `is_user_activated` VARCHAR(5) NULL DEFAULT NULL,
  `last_name` VARCHAR(255) NULL DEFAULT NULL,
  `logo` LONGBLOB NULL DEFAULT NULL,
  `measurement` VARCHAR(10) NULL DEFAULT NULL,
  `mobile` VARCHAR(100) NULL DEFAULT NULL,
  `password` VARCHAR(255) NULL DEFAULT NULL,
  `phone` VARCHAR(100) NULL DEFAULT NULL,
  `position` VARCHAR(255) NULL DEFAULT NULL,
  `quote_need_approval` VARCHAR(10) NULL DEFAULT NULL,
  `region_code` VARCHAR(10) NULL DEFAULT NULL,
  `saas_id` VARCHAR(10) NULL DEFAULT NULL,
  `salutation` VARCHAR(10) NULL DEFAULT NULL,
  `state_code` VARCHAR(10) NULL DEFAULT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(255) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(255) NOT NULL,
  `time_zone` VARCHAR(100) NULL DEFAULT NULL,
  `user_name` VARCHAR(255) NULL DEFAULT NULL,
  `zip_code` VARCHAR(10) NULL DEFAULT NULL,
  `city_id` BIGINT(20) NULL DEFAULT NULL,
  `country_id` BIGINT(20) NULL DEFAULT NULL,
  `currency_id` BIGINT(20) NULL DEFAULT NULL,
  `language_id` BIGINT(20) NULL DEFAULT NULL,
  `region_id` BIGINT(20) NULL DEFAULT NULL,
  `state_id` BIGINT(20) NULL DEFAULT NULL,
  `timezone_id` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `UK_LOCATION_SETUP_REG_NAME` (`company_reg_name` ASC),
  UNIQUE INDEX `UK_LOCATION_SETUP_SAAS_ID` (`saas_id` ASC),
  INDEX `FK_LOCATION_SETUP_CITY` (`city_id` ASC),
  INDEX `FK_LOCATION_SETUP_COUNTRY` (`country_id` ASC),
  INDEX `FK_LOCATION_SETUP_CURR_ID` (`currency_id` ASC),
  INDEX `FK_LOCATION_SETUP_LANGUAGE_ID` (`language_id` ASC),
  INDEX `FK_LOCATION_SETUP_REGION` (`region_id` ASC),
  INDEX `FK_LOCATION_SETUP_STATE` (`state_id` ASC),
  INDEX `FK_LOCATION_SETUP_TZ_ID` (`timezone_id` ASC),
  CONSTRAINT `FK_LOCATION_SETUP_CITY`
    FOREIGN KEY (`city_id`)
    REFERENCES `efsdev`.`efs_common_city_master` (`id`),
  CONSTRAINT `FK_LOCATION_SETUP_COUNTRY`
    FOREIGN KEY (`country_id`)
    REFERENCES `efsdev`.`efs_common_country_master` (`id`),
  CONSTRAINT `FK_LOCATION_SETUP_CURR_ID`
    FOREIGN KEY (`currency_id`)
    REFERENCES `efsdev`.`efs_common_currency_master` (`id`),
  CONSTRAINT `FK_LOCATION_SETUP_LANGUAGE_ID`
    FOREIGN KEY (`language_id`)
    REFERENCES `efsdev`.`efs_common_language_master` (`id`),
  CONSTRAINT `FK_LOCATION_SETUP_REGION`
    FOREIGN KEY (`region_id`)
    REFERENCES `efsdev`.`efs_common_region_master` (`id`),
  CONSTRAINT `FK_LOCATION_SETUP_STATE`
    FOREIGN KEY (`state_id`)
    REFERENCES `efsdev`.`efs_common_state_province` (`id`),
  CONSTRAINT `FK_LOCATION_SETUP_TZ_ID`
    FOREIGN KEY (`timezone_id`)
    REFERENCES `efsdev`.`efs_common_time_zone_master` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_location_setup_charge`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_location_setup_charge` (
  `id` BIGINT(20) NOT NULL,
  `calculation_type` VARCHAR(20) NOT NULL,
  `charge_code` VARCHAR(10) NOT NULL,
  `charge_name` VARCHAR(100) NOT NULL,
  `charge_type` VARCHAR(20) NOT NULL,
  `status` VARCHAR(10) NOT NULL,
  `location_setup_id` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `UK_COM_CHARGE_CHARGECODE` (`charge_code` ASC, `location_setup_id` ASC),
  UNIQUE INDEX `UK_COM_CHARGE_CHARGENAME` (`charge_name` ASC, `location_setup_id` ASC),
  INDEX `FK_LOCSETCHARGE_LOCSETUPID` (`location_setup_id` ASC),
  CONSTRAINT `FK_LOCSETCHARGE_LOCSETUPID`
    FOREIGN KEY (`location_setup_id`)
    REFERENCES `efsdev`.`efs_location_setup` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_location_setup_daybook`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_location_setup_daybook` (
  `id` BIGINT(20) NOT NULL,
  `current_sequence_value` BIGINT(20) NULL DEFAULT NULL,
  `daybook_code` VARCHAR(10) NOT NULL,
  `daybook_name` VARCHAR(100) NOT NULL,
  `format` VARCHAR(255) NULL DEFAULT NULL,
  `is_month` VARCHAR(10) NULL DEFAULT NULL,
  `is_year` VARCHAR(10) NULL DEFAULT NULL,
  `prefix` VARCHAR(5) NULL DEFAULT NULL,
  `separator` VARCHAR(1) NULL DEFAULT NULL,
  `suffix` VARCHAR(5) NULL DEFAULT NULL,
  `document_type_id` BIGINT(20) NULL DEFAULT NULL,
  `location_setup_id` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `UK_LOC_DAYBOOK_DAYBOOKCODE` (`daybook_code` ASC, `location_setup_id` ASC),
  UNIQUE INDEX `UK_LOC_DAYBOOK_DAYBOOKNAME` (`daybook_name` ASC, `location_setup_id` ASC),
  INDEX `FK_LOCSETDAYBOOK_DOCTYPEID` (`document_type_id` ASC),
  INDEX `FK_LOCSETDAYBOOK_LOCSETUPID` (`location_setup_id` ASC),
  CONSTRAINT `FK_LOCSETDAYBOOK_DOCTYPEID`
    FOREIGN KEY (`document_type_id`)
    REFERENCES `efsdev`.`efs_common_doc_type_master` (`id`),
  CONSTRAINT `FK_LOCSETDAYBOOK_LOCSETUPID`
    FOREIGN KEY (`location_setup_id`)
    REFERENCES `efsdev`.`efs_location_setup` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_location_setup_sequence`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_location_setup_sequence` (
  `id` BIGINT(20) NOT NULL,
  `current_sequence_value` BIGINT(20) NULL DEFAULT NULL,
  `format` VARCHAR(255) NULL DEFAULT NULL,
  `is_month` VARCHAR(10) NULL DEFAULT NULL,
  `is_year` VARCHAR(10) NULL DEFAULT NULL,
  `prefix` VARCHAR(5) NULL DEFAULT NULL,
  `separator` VARCHAR(1) NULL DEFAULT NULL,
  `sequence_name` VARCHAR(50) NULL DEFAULT NULL,
  `sequence_type` VARCHAR(30) NULL DEFAULT NULL,
  `suffix` VARCHAR(5) NULL DEFAULT NULL,
  `location_setup_id` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `UK_COM_SEQUENCE_TYPE` (`sequence_type` ASC, `location_setup_id` ASC),
  UNIQUE INDEX `UK_COM_SEQUENCE_PREFIX` (`prefix` ASC, `location_setup_id` ASC),
  INDEX `FK_LOCSETSEQ_LOCSETUPID` (`location_setup_id` ASC),
  CONSTRAINT `FK_LOCSETSEQ_LOCSETUPID`
    FOREIGN KEY (`location_setup_id`)
    REFERENCES `efsdev`.`efs_location_setup` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_location_setup_service`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_location_setup_service` (
  `id` BIGINT(20) NOT NULL,
  `location_setup_id` BIGINT(20) NULL DEFAULT NULL,
  `service_master_id` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `FK_LOCSETSERVICE_LOCSETUPID` (`location_setup_id` ASC),
  INDEX `FK_LOCSETSERVICE_SERVICEID` (`service_master_id` ASC),
  CONSTRAINT `FK_LOCSETSERVICE_LOCSETUPID`
    FOREIGN KEY (`location_setup_id`)
    REFERENCES `efsdev`.`efs_location_setup` (`id`),
  CONSTRAINT `FK_LOCSETSERVICE_SERVICEID`
    FOREIGN KEY (`service_master_id`)
    REFERENCES `efsdev`.`efs_common_service_master` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_user_profile`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_user_profile` (
  `id` BIGINT(20) NOT NULL,
  `activated_date` DATETIME(6) NULL DEFAULT NULL,
  `activated_user` VARCHAR(30) NULL DEFAULT NULL,
  `deactivated_date` DATETIME(6) NULL DEFAULT NULL,
  `deactivated_user` VARCHAR(30) NULL DEFAULT NULL,
  `note` LONGTEXT NULL DEFAULT NULL,
  `password` VARCHAR(30) NOT NULL,
  `password_expires_on` DATETIME(6) NULL DEFAULT NULL,
  `reset_token_date` DATETIME(6) NULL DEFAULT NULL,
  `reset_token_key` VARCHAR(30) NULL DEFAULT NULL,
  `status` VARCHAR(20) NOT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `user_name` VARCHAR(30) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `UK_USERPROFILE_USERNAME` (`user_name` ASC),
  UNIQUE INDEX `UK_USERPROFILE_RESETTOKEN` (`reset_token_key` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_login_history`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_login_history` (
  `id` BIGINT(20) NOT NULL,
  `browser` VARCHAR(50) NULL DEFAULT NULL,
  `browser_version` VARCHAR(50) NULL DEFAULT NULL,
  `application` VARCHAR(50) NULL DEFAULT NULL,
  `employee_code` VARCHAR(255) NOT NULL,
  `login_status` VARCHAR(10) NOT NULL,
  `login_time` DATETIME(6) NOT NULL,
  `logout_time` DATETIME(6) NULL DEFAULT NULL,
  `platform` VARCHAR(50) NULL DEFAULT NULL,
  `saas_id` VARCHAR(50) NULL DEFAULT NULL,
  `session_id` VARCHAR(50) NOT NULL,
  `session_status` VARCHAR(20) NULL DEFAULT NULL,
  `source_ip` VARCHAR(20) NOT NULL,
  `timeout_time` DATETIME(6) NULL DEFAULT NULL,
  `username` VARCHAR(30) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  `employee_id` BIGINT(20) NULL DEFAULT NULL,
  `user_profile_id` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `FK_LOGINHIS_EMPLOYEEID` (`employee_id` ASC),
  INDEX `FK_LOGINHIS_USERPROFILEID` (`user_profile_id` ASC),
  CONSTRAINT `FK_LOGINHIS_EMPLOYEEID`
    FOREIGN KEY (`employee_id`)
    REFERENCES `efsdev`.`efs_employee_master` (`id`),
  CONSTRAINT `FK_LOGINHIS_USERPROFILEID`
    FOREIGN KEY (`user_profile_id`)
    REFERENCES `efsdev`.`efs_user_profile` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_logo_master`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_logo_master` (
  `id` BIGINT(20) NOT NULL,
  `company_code` VARCHAR(30) NULL DEFAULT NULL,
  `country_code` VARCHAR(30) NULL DEFAULT NULL,
  `location_code` VARCHAR(30) NULL DEFAULT NULL,
  `logo` LONGBLOB NULL DEFAULT NULL,
  `status` VARCHAR(10) NOT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  `company_id` BIGINT(20) NULL DEFAULT NULL,
  `country_id` BIGINT(20) NULL DEFAULT NULL,
  `location_id` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `UK_LOGO_LOCATION` (`location_id` ASC),
  INDEX `FK_LOGO_COMPANYID` (`company_id` ASC),
  INDEX `FK_LOGO_COUNTRYID` (`country_id` ASC),
  CONSTRAINT `FK_LOGO_COMPANYID`
    FOREIGN KEY (`company_id`)
    REFERENCES `efsdev`.`efs_company_master` (`id`),
  CONSTRAINT `FK_LOGO_COUNTRYID`
    FOREIGN KEY (`country_id`)
    REFERENCES `efsdev`.`efs_country_master` (`id`),
  CONSTRAINT `FK_LOGO_LOCATIONID`
    FOREIGN KEY (`location_id`)
    REFERENCES `efsdev`.`efs_location_master` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_page_master`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_page_master` (
  `id` BIGINT(20) NOT NULL,
  `page_code` VARCHAR(255) NOT NULL,
  `page_name` VARCHAR(255) NOT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  `page_country_id` BIGINT(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `UK_PAGE_NAME` (`page_name` ASC, `page_country_id` ASC),
  UNIQUE INDEX `UK_PAGE_CODE` (`page_code` ASC, `page_country_id` ASC),
  INDEX `PAGE_COUNTRY_MASTER_FK` (`page_country_id` ASC),
  CONSTRAINT `PAGE_COUNTRY_MASTER_FK`
    FOREIGN KEY (`page_country_id`)
    REFERENCES `efsdev`.`efs_country_report_config` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_party_country_field`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_party_country_field` (
  `id` BIGINT(20) NOT NULL,
  `aes_consignee_type` VARCHAR(255) NULL DEFAULT NULL,
  `aes_filing_type` VARCHAR(255) NULL DEFAULT NULL,
  `aes_to_be_filed_by` VARCHAR(255) NULL DEFAULT NULL,
  `bank_dlr_code` VARCHAR(255) NULL DEFAULT NULL,
  `bond_activity_code` VARCHAR(255) NULL DEFAULT NULL,
  `bond_holder` VARCHAR(255) NULL DEFAULT NULL,
  `bond_no` VARCHAR(255) NULL DEFAULT NULL,
  `bond_surety_code` VARCHAR(255) NULL DEFAULT NULL,
  `bond_type` VARCHAR(255) NULL DEFAULT NULL,
  `branch_sl_no` VARCHAR(255) NULL DEFAULT NULL,
  `country_of_issuance_code` VARCHAR(100) NULL DEFAULT NULL,
  `date_of_birth` DATETIME(6) NULL DEFAULT NULL,
  `export_expiration_date` DATETIME(6) NULL DEFAULT NULL,
  `export_power_of_attorny` VARCHAR(255) NULL DEFAULT NULL,
  `id_number` VARCHAR(255) NULL DEFAULT NULL,
  `id_type` VARCHAR(255) NULL DEFAULT NULL,
  `isf_expiration_date` DATETIME(6) NULL DEFAULT NULL,
  `isf_power_of_attorny` VARCHAR(255) NULL DEFAULT NULL,
  `known_shipper_validation_date` DATETIME(6) NULL DEFAULT NULL,
  `known_shipper_validation_no` VARCHAR(255) NULL DEFAULT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  `warehouse_provider_name` VARCHAR(255) NULL DEFAULT NULL,
  `country_of_issuance` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `FK_PARTYADDRECOUNT_COUNTISS` (`country_of_issuance` ASC),
  CONSTRAINT `FK_PARTYADDRECOUNT_COUNTISS`
    FOREIGN KEY (`country_of_issuance`)
    REFERENCES `efsdev`.`efs_country_master` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_party_address_master`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_party_address_master` (
  `id` BIGINT(20) NOT NULL,
  `address_line_1` VARCHAR(100) NULL DEFAULT NULL,
  `address_line_2` VARCHAR(100) NULL DEFAULT NULL,
  `address_line_3` VARCHAR(100) NULL DEFAULT NULL,
  `address_regional_language` VARCHAR(2000) NULL DEFAULT NULL,
  `address_type` VARCHAR(10) NOT NULL,
  `city_code` VARCHAR(100) NULL DEFAULT NULL,
  `contact_person` VARCHAR(255) NULL DEFAULT NULL,
  `corporate` VARCHAR(10) NOT NULL,
  `email` VARCHAR(500) NULL DEFAULT NULL,
  `fax` VARCHAR(100) NULL DEFAULT NULL,
  `mobile_no` VARCHAR(100) NULL DEFAULT NULL,
  `phone` VARCHAR(100) NULL DEFAULT NULL,
  `pickup_hours_text` VARCHAR(255) NULL DEFAULT NULL,
  `po_box` VARCHAR(20) NULL DEFAULT NULL,
  `state_province_code` VARCHAR(100) NULL DEFAULT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  `zip_code` VARCHAR(10) NULL DEFAULT NULL,
  `city_id` BIGINT(20) NULL DEFAULT NULL,
  `party_country_field_id` BIGINT(20) NULL DEFAULT NULL,
  `party_id` BIGINT(20) NULL DEFAULT NULL,
  `state_province` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `FK_PARTYADDRE_CITYID` (`city_id` ASC),
  INDEX `FK6ye14d08tpycvvwt5paov7acv` (`party_country_field_id` ASC),
  INDEX `FK_PARTYADDRE_PARTYID` (`party_id` ASC),
  INDEX `FK_PARTYADDRE_STATEPROVI` (`state_province` ASC),
  CONSTRAINT `FK6ye14d08tpycvvwt5paov7acv`
    FOREIGN KEY (`party_country_field_id`)
    REFERENCES `efsdev`.`efs_party_country_field` (`id`),
  CONSTRAINT `FK_PARTYADDRE_CITYID`
    FOREIGN KEY (`city_id`)
    REFERENCES `efsdev`.`efs_city_master` (`id`),
  CONSTRAINT `FK_PARTYADDRE_PARTYID`
    FOREIGN KEY (`party_id`)
    REFERENCES `efsdev`.`efs_party_master` (`id`),
  CONSTRAINT `FK_PARTYADDRE_STATEPROVI`
    FOREIGN KEY (`state_province`)
    REFERENCES `efsdev`.`efs_state_province_master` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_party_type_master`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_party_type_master` (
  `id` BIGINT(20) NOT NULL,
  `party_type_code` VARCHAR(10) NOT NULL,
  `party_type_name` VARCHAR(100) NOT NULL,
  `status` VARCHAR(10) NOT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `UK_PARTYTYPE_PARTYTYPECODE` (`party_type_code` ASC),
  UNIQUE INDEX `UK_PARTYTYPE_PARTYTYPENAME` (`party_type_name` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_party_asso_type`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_party_asso_type` (
  `id` BIGINT(20) NOT NULL,
  `party_type_code` VARCHAR(100) NULL DEFAULT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  `party_id` BIGINT(20) NULL DEFAULT NULL,
  `party_type_id` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `FK_PARTYASSPARTYTYPE_PARTYID` (`party_id` ASC),
  INDEX `FK_PARTYASSPARTYTYPE_PARTYTYID` (`party_type_id` ASC),
  CONSTRAINT `FK_PARTYASSPARTYTYPE_PARTYID`
    FOREIGN KEY (`party_id`)
    REFERENCES `efsdev`.`efs_party_master` (`id`),
  CONSTRAINT `FK_PARTYASSPARTYTYPE_PARTYTYID`
    FOREIGN KEY (`party_type_id`)
    REFERENCES `efsdev`.`efs_party_type_master` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_party_bussiness_detail`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_party_bussiness_detail` (
  `id` BIGINT(20) NOT NULL,
  `business_notes` VARCHAR(5000) NULL DEFAULT NULL,
  `bussiness_status` VARCHAR(255) NULL DEFAULT NULL,
  `closuredate` DATETIME(6) NULL DEFAULT NULL,
  `commodity_code` VARCHAR(100) NULL DEFAULT NULL,
  `created_location_code` VARCHAR(100) NULL DEFAULT NULL,
  `current_potential` VARCHAR(255) NULL DEFAULT NULL,
  `destination_code` VARCHAR(100) NULL DEFAULT NULL,
  `estimated_revenue` BIGINT(20) NULL DEFAULT NULL,
  `no_of_shipments` DOUBLE NULL DEFAULT NULL,
  `no_of_units` DOUBLE NULL DEFAULT NULL,
  `note_check` VARCHAR(5) NULL DEFAULT NULL,
  `origin_code` VARCHAR(100) NULL DEFAULT NULL,
  `period_code` VARCHAR(100) NULL DEFAULT NULL,
  `reason` VARCHAR(255) NULL DEFAULT NULL,
  `service_code` VARCHAR(100) NULL DEFAULT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `tos_code` VARCHAR(100) NULL DEFAULT NULL,
  `unit_code` VARCHAR(255) NULL DEFAULT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  `commodity_id` BIGINT(20) NULL DEFAULT NULL,
  `created_location_id` BIGINT(20) NULL DEFAULT NULL,
  `destination_id` BIGINT(20) NULL DEFAULT NULL,
  `origin_id` BIGINT(20) NULL DEFAULT NULL,
  `party_id` BIGINT(20) NULL DEFAULT NULL,
  `period_id` BIGINT(20) NULL DEFAULT NULL,
  `service_id` BIGINT(20) NULL DEFAULT NULL,
  `tos_id` BIGINT(20) NULL DEFAULT NULL,
  `unit_id` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `FK_ARTYBUSSINESS_COMMODITYID` (`commodity_id` ASC),
  INDEX `FK_PARTYBUSS_CRELOCID` (`created_location_id` ASC),
  INDEX `FK_PARTYBUSSINESS_TOPORTID` (`destination_id` ASC),
  INDEX `FK_PARTYBUSSINESS_FROMPORTID` (`origin_id` ASC),
  INDEX `FK_PARTYBUSSINESS_PARTYID` (`party_id` ASC),
  INDEX `FK_PARTYBUSSINESS_PERIODID` (`period_id` ASC),
  INDEX `FK_PARTYBUSSINESS_SERVICEID` (`service_id` ASC),
  INDEX `FK_PARTYBUSSINESS_TOSID` (`tos_id` ASC),
  INDEX `FK_PARTYBUSSINESS_UNITID` (`unit_id` ASC),
  CONSTRAINT `FK_ARTYBUSSINESS_COMMODITYID`
    FOREIGN KEY (`commodity_id`)
    REFERENCES `efsdev`.`efs_hs_code_master` (`id`),
  CONSTRAINT `FK_PARTYBUSSINESS_FROMPORTID`
    FOREIGN KEY (`origin_id`)
    REFERENCES `efsdev`.`efs_port_master` (`id`),
  CONSTRAINT `FK_PARTYBUSSINESS_PARTYID`
    FOREIGN KEY (`party_id`)
    REFERENCES `efsdev`.`efs_party_master` (`id`),
  CONSTRAINT `FK_PARTYBUSSINESS_PERIODID`
    FOREIGN KEY (`period_id`)
    REFERENCES `efsdev`.`efs_period_master` (`id`),
  CONSTRAINT `FK_PARTYBUSSINESS_SERVICEID`
    FOREIGN KEY (`service_id`)
    REFERENCES `efsdev`.`efs_service_master` (`id`),
  CONSTRAINT `FK_PARTYBUSSINESS_TOPORTID`
    FOREIGN KEY (`destination_id`)
    REFERENCES `efsdev`.`efs_port_master` (`id`),
  CONSTRAINT `FK_PARTYBUSSINESS_TOSID`
    FOREIGN KEY (`tos_id`)
    REFERENCES `efsdev`.`efs_tos_master` (`id`),
  CONSTRAINT `FK_PARTYBUSSINESS_UNITID`
    FOREIGN KEY (`unit_id`)
    REFERENCES `efsdev`.`efs_unit_master` (`id`),
  CONSTRAINT `FK_PARTYBUSS_CRELOCID`
    FOREIGN KEY (`created_location_id`)
    REFERENCES `efsdev`.`efs_location_master` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_party_company_associate`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_party_company_associate` (
  `id` BIGINT(20) NOT NULL,
  `company_code` VARCHAR(20) NULL DEFAULT NULL,
  `division_code` VARCHAR(20) NULL DEFAULT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  `company_id` BIGINT(20) NULL DEFAULT NULL,
  `division_id` BIGINT(20) NULL DEFAULT NULL,
  `party_id` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `UK_PARTYCOMPDIV_PARTYCOMPDIV` (`party_id` ASC, `company_id` ASC, `division_id` ASC),
  INDEX `FK_PARTYASSCOMP_COMPID` (`company_id` ASC),
  INDEX `FK_PARTYASSCOMP_DIVISIONID` (`division_id` ASC),
  CONSTRAINT `FK_PARTYASSCOMP_COMPID`
    FOREIGN KEY (`company_id`)
    REFERENCES `efsdev`.`efs_company_master` (`id`),
  CONSTRAINT `FK_PARTYASSCOMP_DIVISIONID`
    FOREIGN KEY (`division_id`)
    REFERENCES `efsdev`.`efs_division_master` (`id`),
  CONSTRAINT `FK_PARTYASSCOMP_PARTYID`
    FOREIGN KEY (`party_id`)
    REFERENCES `efsdev`.`efs_party_master` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_party_contact`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_party_contact` (
  `id` BIGINT(20) NOT NULL,
  `assistant_name` VARCHAR(100) NULL DEFAULT NULL,
  `assistant_phone` VARCHAR(100) NULL DEFAULT NULL,
  `department` VARCHAR(100) NULL DEFAULT NULL,
  `designation` VARCHAR(100) NULL DEFAULT NULL,
  `engaged_us_previous` VARCHAR(10) NOT NULL,
  `first_name` VARCHAR(100) NULL DEFAULT NULL,
  `former_company` VARCHAR(100) NULL DEFAULT NULL,
  `is_call` VARCHAR(10) NOT NULL,
  `is_favourite` VARCHAR(10) NOT NULL,
  `is_married` VARCHAR(10) NOT NULL,
  `is_primary` VARCHAR(10) NOT NULL,
  `is_send_mail` VARCHAR(10) NOT NULL,
  `last_name` VARCHAR(100) NULL DEFAULT NULL,
  `lead_id` BIGINT(20) NULL DEFAULT NULL,
  `no_of_children` VARCHAR(5) NULL DEFAULT NULL,
  `note` VARCHAR(4000) NULL DEFAULT NULL,
  `offical_address` VARCHAR(255) NULL DEFAULT NULL,
  `offical_email` VARCHAR(500) NULL DEFAULT NULL,
  `offical_fax` VARCHAR(100) NULL DEFAULT NULL,
  `offical_mobile` VARCHAR(100) NULL DEFAULT NULL,
  `offical_phone` VARCHAR(100) NULL DEFAULT NULL,
  `personal_address` VARCHAR(255) NULL DEFAULT NULL,
  `personal_email` VARCHAR(500) NULL DEFAULT NULL,
  `personal_fax` VARCHAR(100) NULL DEFAULT NULL,
  `personal_mobile` VARCHAR(100) NULL DEFAULT NULL,
  `personal_phone` VARCHAR(100) NULL DEFAULT NULL,
  `preferred_call_time` VARCHAR(100) NULL DEFAULT NULL,
  `qualification` VARCHAR(50) NULL DEFAULT NULL,
  `reporting_to` VARCHAR(100) NULL DEFAULT NULL,
  `salutation` VARCHAR(10) NULL DEFAULT NULL,
  `status` VARCHAR(10) NOT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  `party_id` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `FK_PARTYCONT_PARTYID` (`party_id` ASC),
  CONSTRAINT `FK_PARTYCONT_PARTYID`
    FOREIGN KEY (`party_id`)
    REFERENCES `efsdev`.`efs_party_master` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_party_contact_relation`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_party_contact_relation` (
  `id` BIGINT(20) NOT NULL,
  `first_name` VARCHAR(100) NULL DEFAULT NULL,
  `last_name` VARCHAR(100) NULL DEFAULT NULL,
  `note` VARCHAR(400) NULL DEFAULT NULL,
  `personal_event` VARCHAR(20) NULL DEFAULT NULL,
  `personal_event_data` DATETIME(6) NULL DEFAULT NULL,
  `relation_ship` VARCHAR(20) NULL DEFAULT NULL,
  `salutation` VARCHAR(10) NULL DEFAULT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  `party_contact_id` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `FK_PARTYCONTRE_PARTYID` (`party_contact_id` ASC),
  CONSTRAINT `FK_PARTYCONTRE_PARTYID`
    FOREIGN KEY (`party_contact_id`)
    REFERENCES `efsdev`.`efs_party_contact` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_party_credit_limit`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_party_credit_limit` (
  `id` BIGINT(20) NOT NULL,
  `auto_trigger_email` VARCHAR(255) NULL DEFAULT NULL,
  `company_code` VARCHAR(10) NULL DEFAULT NULL,
  `country_code` VARCHAR(10) NULL DEFAULT NULL,
  `credit_amount` DOUBLE NULL DEFAULT NULL,
  `credit_days` INT(11) NULL DEFAULT NULL,
  `customer_email` VARCHAR(255) NULL DEFAULT NULL,
  `customer_operation_email` VARCHAR(255) NULL DEFAULT NULL,
  `division_code` VARCHAR(100) NULL DEFAULT NULL,
  `invoice_mail` VARCHAR(255) NULL DEFAULT NULL,
  `location_code` VARCHAR(100) NULL DEFAULT NULL,
  `publish_credit_days` INT(11) NULL DEFAULT NULL,
  `service_code` VARCHAR(100) NULL DEFAULT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  `company_id` BIGINT(20) NULL DEFAULT NULL,
  `country_id` BIGINT(20) NULL DEFAULT NULL,
  `division_id` BIGINT(20) NULL DEFAULT NULL,
  `location_id` BIGINT(20) NULL DEFAULT NULL,
  `party_id` BIGINT(20) NULL DEFAULT NULL,
  `service_id` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `FK_PARTYCREDLMT_COMPANYID` (`company_id` ASC),
  INDEX `FK_PARTYCREDLMT_COUNTRYID` (`country_id` ASC),
  INDEX `FK_PARTYCREDLMT_DIVISIONID` (`division_id` ASC),
  INDEX `FK_PARTYCREDLMT_LOCATIONID` (`location_id` ASC),
  INDEX `FK_PARTYCREDLMT_PARTYID` (`party_id` ASC),
  INDEX `FK_PARTYCREDLMT_SERVICEID` (`service_id` ASC),
  CONSTRAINT `FK_PARTYCREDLMT_COMPANYID`
    FOREIGN KEY (`company_id`)
    REFERENCES `efsdev`.`efs_company_master` (`id`),
  CONSTRAINT `FK_PARTYCREDLMT_COUNTRYID`
    FOREIGN KEY (`country_id`)
    REFERENCES `efsdev`.`efs_country_master` (`id`),
  CONSTRAINT `FK_PARTYCREDLMT_DIVISIONID`
    FOREIGN KEY (`division_id`)
    REFERENCES `efsdev`.`efs_division_master` (`id`),
  CONSTRAINT `FK_PARTYCREDLMT_LOCATIONID`
    FOREIGN KEY (`location_id`)
    REFERENCES `efsdev`.`efs_location_master` (`id`),
  CONSTRAINT `FK_PARTYCREDLMT_PARTYID`
    FOREIGN KEY (`party_id`)
    REFERENCES `efsdev`.`efs_party_master` (`id`),
  CONSTRAINT `FK_PARTYCREDLMT_SERVICEID`
    FOREIGN KEY (`service_id`)
    REFERENCES `efsdev`.`efs_service_master` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_party_email_mapping`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_party_email_mapping` (
  `id` BIGINT(20) NOT NULL,
  `company_code` VARCHAR(100) NULL DEFAULT NULL,
  `country_code` VARCHAR(100) NULL DEFAULT NULL,
  `destination_country_code` VARCHAR(100) NULL DEFAULT NULL,
  `email` VARCHAR(500) NOT NULL,
  `imco` VARCHAR(255) NULL DEFAULT NULL,
  `location_code` VARCHAR(100) NULL DEFAULT NULL,
  `origin_country_code` VARCHAR(100) NULL DEFAULT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  `company_id` BIGINT(20) NULL DEFAULT NULL,
  `country_id` BIGINT(20) NULL DEFAULT NULL,
  `destination_country_id` BIGINT(20) NULL DEFAULT NULL,
  `location_id` BIGINT(20) NULL DEFAULT NULL,
  `origin_country_id` BIGINT(20) NULL DEFAULT NULL,
  `party_id` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `UK_PARTYEMAIL_PARTY` (`party_id` ASC, `email` ASC, `location_id` ASC, `destination_country_id` ASC, `origin_country_id` ASC, `imco` ASC),
  INDEX `FK_PARTYEMAIL_COMPANY_ID` (`company_id` ASC),
  INDEX `FK_PARTYEMAIL_CNTID` (`country_id` ASC),
  INDEX `FK_PARTYEMAIL_DESTCNTID` (`destination_country_id` ASC),
  INDEX `FK_PARTYEMAIL_LOCATIONID` (`location_id` ASC),
  INDEX `FK_PARTYEMAIL_ORICNTID` (`origin_country_id` ASC),
  CONSTRAINT `FK_PARTYEMAIL_CNTID`
    FOREIGN KEY (`country_id`)
    REFERENCES `efsdev`.`efs_country_master` (`id`),
  CONSTRAINT `FK_PARTYEMAIL_COMPANY_ID`
    FOREIGN KEY (`company_id`)
    REFERENCES `efsdev`.`efs_company_master` (`id`),
  CONSTRAINT `FK_PARTYEMAIL_DESTCNTID`
    FOREIGN KEY (`destination_country_id`)
    REFERENCES `efsdev`.`efs_country_master` (`id`),
  CONSTRAINT `FK_PARTYEMAIL_LOCATIONID`
    FOREIGN KEY (`location_id`)
    REFERENCES `efsdev`.`efs_location_master` (`id`),
  CONSTRAINT `FK_PARTYEMAIL_ORICNTID`
    FOREIGN KEY (`origin_country_id`)
    REFERENCES `efsdev`.`efs_country_master` (`id`),
  CONSTRAINT `FK_PARTYEMAIL_PARTYID`
    FOREIGN KEY (`party_id`)
    REFERENCES `efsdev`.`efs_party_master` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_party_email_msg_map`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_party_email_msg_map` (
  `id` BIGINT(20) NOT NULL,
  `message_group_code` VARCHAR(100) NULL DEFAULT NULL,
  `message_code` VARCHAR(100) NULL DEFAULT NULL,
  `status` VARCHAR(255) NULL DEFAULT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  `message_group_id` BIGINT(20) NULL DEFAULT NULL,
  `message_id` BIGINT(20) NULL DEFAULT NULL,
  `email_mapping_id` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `FK_PARTYEMAILMSG_AUTMAGRPID` (`message_group_id` ASC),
  INDEX `FK_PARTYEMAILMSG_ORIGINID` (`message_id` ASC),
  INDEX `FK_PARTYEMAILMSG_PAREMAMAPID` (`email_mapping_id` ASC),
  CONSTRAINT `FK_PARTYEMAILMSG_AUTMAGRPID`
    FOREIGN KEY (`message_group_id`)
    REFERENCES `efsdev`.`efs_auto_mail_group_master` (`id`),
  CONSTRAINT `FK_PARTYEMAILMSG_ORIGINID`
    FOREIGN KEY (`message_id`)
    REFERENCES `efsdev`.`efs_auto_mail_master` (`id`),
  CONSTRAINT `FK_PARTYEMAILMSG_PAREMAMAPID`
    FOREIGN KEY (`email_mapping_id`)
    REFERENCES `efsdev`.`efs_party_email_mapping` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_party_group_master`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_party_group_master` (
  `id` BIGINT(20) NOT NULL,
  `party_group_code` VARCHAR(10) NOT NULL,
  `party_group_name` VARCHAR(100) NOT NULL,
  `status` VARCHAR(10) NOT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  `sales_lead_location_id` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `UK_PARTYGRP_PARTYGROUPCODE` (`party_group_code` ASC),
  UNIQUE INDEX `UK_PARTYGRP_PARTYGROUPNAME` (`party_group_name` ASC),
  INDEX `FK_PARTYGRP_SALESLOCID` (`sales_lead_location_id` ASC),
  CONSTRAINT `FK_PARTYGRP_SALESLOCID`
    FOREIGN KEY (`sales_lead_location_id`)
    REFERENCES `efsdev`.`efs_location_master` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_party_group_has_party`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_party_group_has_party` (
  `party_group_id` BIGINT(20) NOT NULL,
  `party_id` BIGINT(20) NOT NULL,
  UNIQUE INDEX `UK_PARTYGROUP_PARTY` (`party_group_id` ASC, `party_id` ASC),
  INDEX `FK_PARTYGRP_PARTY_2` (`party_id` ASC),
  CONSTRAINT `FK_PARTYGRP_PARTY_1`
    FOREIGN KEY (`party_group_id`)
    REFERENCES `efsdev`.`efs_party_group_master` (`id`),
  CONSTRAINT `FK_PARTYGRP_PARTY_2`
    FOREIGN KEY (`party_id`)
    REFERENCES `efsdev`.`efs_party_master` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_party_service_master`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_party_service_master` (
  `id` BIGINT(20) NOT NULL,
  `customer_service_code` VARCHAR(100) NULL DEFAULT NULL,
  `location_code` VARCHAR(100) NULL DEFAULT NULL,
  `salesman_code` VARCHAR(100) NULL DEFAULT NULL,
  `service_code` VARCHAR(100) NULL DEFAULT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `tos_code` VARCHAR(100) NULL DEFAULT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  `customer_service_id` BIGINT(20) NULL DEFAULT NULL,
  `location_id` BIGINT(20) NULL DEFAULT NULL,
  `party_id` BIGINT(20) NULL DEFAULT NULL,
  `salesman_id` BIGINT(20) NULL DEFAULT NULL,
  `service_id` BIGINT(20) NULL DEFAULT NULL,
  `tos_id` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `UK_PARTYSERVIE_PARTYSERVICE` (`party_id` ASC, `service_id` ASC, `location_id` ASC),
  INDEX `FK_PARTYSERVICE_CUSTSERVID` (`customer_service_id` ASC),
  INDEX `FK_PARTYSERVICE_LOCATIONID` (`location_id` ASC),
  INDEX `FK_PARTYSERVICE_SALEMANID` (`salesman_id` ASC),
  INDEX `FK_PARTYSERVICE_SERVICEID` (`service_id` ASC),
  INDEX `FK_PARTYSERVICE_TOSID` (`tos_id` ASC),
  CONSTRAINT `FK_PARTYSERVICE_CUSTSERVID`
    FOREIGN KEY (`customer_service_id`)
    REFERENCES `efsdev`.`efs_employee_master` (`id`),
  CONSTRAINT `FK_PARTYSERVICE_LOCATIONID`
    FOREIGN KEY (`location_id`)
    REFERENCES `efsdev`.`efs_location_master` (`id`),
  CONSTRAINT `FK_PARTYSERVICE_PARTYID`
    FOREIGN KEY (`party_id`)
    REFERENCES `efsdev`.`efs_party_master` (`id`),
  CONSTRAINT `FK_PARTYSERVICE_SALEMANID`
    FOREIGN KEY (`salesman_id`)
    REFERENCES `efsdev`.`efs_employee_master` (`id`),
  CONSTRAINT `FK_PARTYSERVICE_SERVICEID`
    FOREIGN KEY (`service_id`)
    REFERENCES `efsdev`.`efs_service_master` (`id`),
  CONSTRAINT `FK_PARTYSERVICE_TOSID`
    FOREIGN KEY (`tos_id`)
    REFERENCES `efsdev`.`efs_tos_master` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_po_attachment`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_po_attachment` (
  `id` BIGINT(20) NOT NULL,
  `customs` VARCHAR(255) NULL DEFAULT NULL,
  `protected_file_attached` LONGBLOB NULL DEFAULT NULL,
  `protected_file_content_type` VARCHAR(100) NULL DEFAULT NULL,
  `protected_file_name` VARCHAR(100) NULL DEFAULT NULL,
  `ref_no` VARCHAR(100) NULL DEFAULT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `unprotected_file_attached` LONGBLOB NULL DEFAULT NULL,
  `unprotected_file_content_type` VARCHAR(100) NULL DEFAULT NULL,
  `unprotected_file_name` VARCHAR(100) NULL DEFAULT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  `purchase_order_id` BIGINT(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `UK_PO_ATTACHMENT_REF` (`ref_no` ASC, `purchase_order_id` ASC),
  INDEX `FK_PO_ATTACHMENT_PO` (`purchase_order_id` ASC),
  CONSTRAINT `FK_PO_ATTACHMENT_PO`
    FOREIGN KEY (`purchase_order_id`)
    REFERENCES `efsdev`.`efs_purchase_order` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_po_vehicle_info`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_po_vehicle_info` (
  `id` BIGINT(20) NOT NULL,
  `delivery_date` DATETIME(6) NULL DEFAULT NULL,
  `driver_name` VARCHAR(100) NULL DEFAULT NULL,
  `net_weight_kg` DOUBLE NULL DEFAULT NULL,
  `no_of_piece` INT(11) NULL DEFAULT NULL,
  `pickup_date` DATETIME(6) NULL DEFAULT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `vehicle_no` VARCHAR(100) NULL DEFAULT NULL,
  `vehicle_type` VARCHAR(100) NULL DEFAULT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  `volume_cbm` DOUBLE NULL DEFAULT NULL,
  `purchase_order_id` BIGINT(20) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `FK_PO_VEHICLE_INFO_PO` (`purchase_order_id` ASC),
  CONSTRAINT `FK_PO_VEHICLE_INFO_PO`
    FOREIGN KEY (`purchase_order_id`)
    REFERENCES `efsdev`.`efs_purchase_order` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_port_edi_mapping`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_port_edi_mapping` (
  `id` BIGINT(20) NOT NULL,
  `edi_subtype` VARCHAR(100) NULL DEFAULT NULL,
  `edi_type` VARCHAR(100) NOT NULL,
  `edi_value` VARCHAR(100) NOT NULL,
  `port_code` VARCHAR(10) NOT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  `port_id` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `UK_EDIPORT_EDITYPE` (`port_id` ASC, `edi_type` ASC),
  CONSTRAINT `FK_EDIPORT_PORTID`
    FOREIGN KEY (`port_id`)
    REFERENCES `efsdev`.`efs_port_master` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_pricing_master`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_pricing_master` (
  `id` BIGINT(20) NOT NULL,
  `company_code` VARCHAR(30) NOT NULL,
  `country_code` VARCHAR(30) NOT NULL,
  `destination_port_code` VARCHAR(30) NULL DEFAULT NULL,
  `location_code` VARCHAR(30) NOT NULL,
  `origin_port_code` VARCHAR(30) NULL DEFAULT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `transit_port_code` VARCHAR(30) NULL DEFAULT NULL,
  `transport_mode` VARCHAR(10) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  `who_routed` VARCHAR(255) NULL DEFAULT NULL,
  `company_id` BIGINT(20) NOT NULL,
  `country_id` BIGINT(20) NOT NULL,
  `destination_port_id` BIGINT(20) NULL DEFAULT NULL,
  `location_id` BIGINT(20) NOT NULL,
  `origin_port_id` BIGINT(20) NULL DEFAULT NULL,
  `transit_port_id` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `FK_PRICMASTER_COMPID` (`company_id` ASC),
  INDEX `FK_PRICMASTER_COUNTID` (`country_id` ASC),
  INDEX `FK_PRICMASTER_DESTID` (`destination_port_id` ASC),
  INDEX `FK_PRICMASTER_LOCTID` (`location_id` ASC),
  INDEX `FK_PRICMASTER_ORIGINID` (`origin_port_id` ASC),
  INDEX `FK_PRICMASTER_TRANSITID` (`transit_port_id` ASC),
  CONSTRAINT `FK_PRICMASTER_COMPID`
    FOREIGN KEY (`company_id`)
    REFERENCES `efsdev`.`efs_company_master` (`id`),
  CONSTRAINT `FK_PRICMASTER_COUNTID`
    FOREIGN KEY (`country_id`)
    REFERENCES `efsdev`.`efs_country_master` (`id`),
  CONSTRAINT `FK_PRICMASTER_DESTID`
    FOREIGN KEY (`destination_port_id`)
    REFERENCES `efsdev`.`efs_port_master` (`id`),
  CONSTRAINT `FK_PRICMASTER_LOCTID`
    FOREIGN KEY (`location_id`)
    REFERENCES `efsdev`.`efs_location_master` (`id`),
  CONSTRAINT `FK_PRICMASTER_ORIGINID`
    FOREIGN KEY (`origin_port_id`)
    REFERENCES `efsdev`.`efs_port_master` (`id`),
  CONSTRAINT `FK_PRICMASTER_TRANSITID`
    FOREIGN KEY (`transit_port_id`)
    REFERENCES `efsdev`.`efs_port_master` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_pricing_attachment`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_pricing_attachment` (
  `id` BIGINT(20) NOT NULL,
  `company_code` VARCHAR(30) NOT NULL,
  `country_code` VARCHAR(30) NOT NULL,
  `file_attached` LONGBLOB NULL DEFAULT NULL,
  `file_content_type` VARCHAR(100) NULL DEFAULT NULL,
  `file_name` VARCHAR(100) NULL DEFAULT NULL,
  `location_code` VARCHAR(30) NOT NULL,
  `ref_no` VARCHAR(100) NULL DEFAULT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  `company_id` BIGINT(20) NOT NULL,
  `country_id` BIGINT(20) NOT NULL,
  `location_id` BIGINT(20) NOT NULL,
  `pricing_master_id` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `UK_PRICATTACH_REF` (`ref_no` ASC, `pricing_master_id` ASC),
  INDEX `FK_PRICATTACH_COMPID` (`company_id` ASC),
  INDEX `FK_PRICATTACH_COUNTID` (`country_id` ASC),
  INDEX `FK_PRICATTACH_LOCTID` (`location_id` ASC),
  INDEX `FK_PRICATTACH_PRICINGID` (`pricing_master_id` ASC),
  CONSTRAINT `FK_PRICATTACH_COMPID`
    FOREIGN KEY (`company_id`)
    REFERENCES `efsdev`.`efs_company_master` (`id`),
  CONSTRAINT `FK_PRICATTACH_COUNTID`
    FOREIGN KEY (`country_id`)
    REFERENCES `efsdev`.`efs_country_master` (`id`),
  CONSTRAINT `FK_PRICATTACH_LOCTID`
    FOREIGN KEY (`location_id`)
    REFERENCES `efsdev`.`efs_location_master` (`id`),
  CONSTRAINT `FK_PRICATTACH_PRICINGID`
    FOREIGN KEY (`pricing_master_id`)
    REFERENCES `efsdev`.`efs_pricing_master` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_pricing_carrier_slap`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_pricing_carrier_slap` (
  `id` BIGINT(20) NOT NULL,
  `minimum` DOUBLE NULL DEFAULT NULL,
  `minus_45` DOUBLE NULL DEFAULT NULL,
  `plus_100` DOUBLE NULL DEFAULT NULL,
  `plus_1000` DOUBLE NULL DEFAULT NULL,
  `plus_250` DOUBLE NULL DEFAULT NULL,
  `plus_300` DOUBLE NULL DEFAULT NULL,
  `plus_45` DOUBLE NULL DEFAULT NULL,
  `plus_500` DOUBLE NULL DEFAULT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_pricing_carrier`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_pricing_carrier` (
  `id` BIGINT(20) NOT NULL,
  `carrier_code` VARCHAR(30) NULL DEFAULT NULL,
  `charge_code` VARCHAR(30) NULL DEFAULT NULL,
  `charge_type` VARCHAR(255) NULL DEFAULT NULL,
  `company_code` VARCHAR(30) NOT NULL,
  `country_code` VARCHAR(30) NOT NULL,
  `currency_code` VARCHAR(30) NULL DEFAULT NULL,
  `fuel_surcharge` DOUBLE NULL DEFAULT NULL,
  `fuel_valid_from` DATETIME(6) NULL DEFAULT NULL,
  `fuel_valid_to` DATETIME(6) NULL DEFAULT NULL,
  `hazardous` VARCHAR(255) NULL DEFAULT NULL,
  `location_code` VARCHAR(30) NOT NULL,
  `security_charge_valid_from` DATETIME(6) NULL DEFAULT NULL,
  `security_charge_valid_to` DATETIME(6) NULL DEFAULT NULL,
  `security_surcharge` DOUBLE NULL DEFAULT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `unit_code` VARCHAR(30) NULL DEFAULT NULL,
  `valid_from` DATETIME(6) NULL DEFAULT NULL,
  `valid_to` DATETIME(6) NULL DEFAULT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  `carrier_id` BIGINT(20) NULL DEFAULT NULL,
  `charge_id` BIGINT(20) NULL DEFAULT NULL,
  `company_id` BIGINT(20) NOT NULL,
  `cost` BIGINT(20) NULL DEFAULT NULL,
  `country_id` BIGINT(20) NOT NULL,
  `currency_id` BIGINT(20) NULL DEFAULT NULL,
  `location_id` BIGINT(20) NOT NULL,
  `minimum_sell_price` BIGINT(20) NULL DEFAULT NULL,
  `pricing_master_id` BIGINT(20) NULL DEFAULT NULL,
  `service_id` BIGINT(20) NULL DEFAULT NULL,
  `standard_sell_price` BIGINT(20) NULL DEFAULT NULL,
  `unit_id` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `FK_PRIC_CARRIER_CARRID` (`carrier_id` ASC),
  INDEX `FK_PRIC_CARRIER_CHARGID` (`charge_id` ASC),
  INDEX `FK_PRIC_CARRIER_COMPID` (`company_id` ASC),
  INDEX `FK_PRIC_CARRIER_COSTID` (`cost` ASC),
  INDEX `FK_PRIC_CARRIER_COUNTID` (`country_id` ASC),
  INDEX `FK_PRIC_CARRIER_CURREID` (`currency_id` ASC),
  INDEX `FK_PRIC_CARRIER_LOCTID` (`location_id` ASC),
  INDEX `FK_PRIC_CARRIER_MINSELLID` (`minimum_sell_price` ASC),
  INDEX `FK_PRIC_CARRIER_ID` (`pricing_master_id` ASC),
  INDEX `FK_PC_SERVICE_ID` (`service_id` ASC),
  INDEX `FK_PRIC_CARRIER_STDSELLID` (`standard_sell_price` ASC),
  INDEX `FK_PRIC_CARRIER_UNITID` (`unit_id` ASC),
  CONSTRAINT `FK_PC_SERVICE_ID`
    FOREIGN KEY (`service_id`)
    REFERENCES `efsdev`.`efs_service_master` (`id`),
  CONSTRAINT `FK_PRIC_CARRIER_CARRID`
    FOREIGN KEY (`carrier_id`)
    REFERENCES `efsdev`.`efs_carrier_master` (`id`),
  CONSTRAINT `FK_PRIC_CARRIER_CHARGID`
    FOREIGN KEY (`charge_id`)
    REFERENCES `efsdev`.`efs_charge_master` (`id`),
  CONSTRAINT `FK_PRIC_CARRIER_COMPID`
    FOREIGN KEY (`company_id`)
    REFERENCES `efsdev`.`efs_company_master` (`id`),
  CONSTRAINT `FK_PRIC_CARRIER_COSTID`
    FOREIGN KEY (`cost`)
    REFERENCES `efsdev`.`efs_pricing_carrier_slap` (`id`),
  CONSTRAINT `FK_PRIC_CARRIER_COUNTID`
    FOREIGN KEY (`country_id`)
    REFERENCES `efsdev`.`efs_country_master` (`id`),
  CONSTRAINT `FK_PRIC_CARRIER_CURREID`
    FOREIGN KEY (`currency_id`)
    REFERENCES `efsdev`.`efs_currency_master` (`id`),
  CONSTRAINT `FK_PRIC_CARRIER_ID`
    FOREIGN KEY (`pricing_master_id`)
    REFERENCES `efsdev`.`efs_pricing_master` (`id`),
  CONSTRAINT `FK_PRIC_CARRIER_LOCTID`
    FOREIGN KEY (`location_id`)
    REFERENCES `efsdev`.`efs_location_master` (`id`),
  CONSTRAINT `FK_PRIC_CARRIER_MINSELLID`
    FOREIGN KEY (`minimum_sell_price`)
    REFERENCES `efsdev`.`efs_pricing_carrier_slap` (`id`),
  CONSTRAINT `FK_PRIC_CARRIER_STDSELLID`
    FOREIGN KEY (`standard_sell_price`)
    REFERENCES `efsdev`.`efs_pricing_carrier_slap` (`id`),
  CONSTRAINT `FK_PRIC_CARRIER_UNITID`
    FOREIGN KEY (`unit_id`)
    REFERENCES `efsdev`.`efs_unit_master` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_pricing_port_pair`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_pricing_port_pair` (
  `id` BIGINT(20) NOT NULL,
  `charge_code` VARCHAR(30) NULL DEFAULT NULL,
  `charge_type` VARCHAR(255) NULL DEFAULT NULL,
  `company_code` VARCHAR(30) NOT NULL,
  `cost_in_amount` DOUBLE NULL DEFAULT NULL,
  `cost_in_minimum` DOUBLE NULL DEFAULT NULL,
  `country_code` VARCHAR(30) NOT NULL,
  `currency_code` VARCHAR(30) NULL DEFAULT NULL,
  `hazardous` VARCHAR(255) NULL DEFAULT NULL,
  `is_clearance` VARCHAR(5) NULL DEFAULT NULL,
  `is_coload` VARCHAR(5) NULL DEFAULT NULL,
  `is_forwarder_direct` VARCHAR(15) NULL DEFAULT NULL,
  `is_our_transport` VARCHAR(5) NULL DEFAULT NULL,
  `is_personal_effect` VARCHAR(5) NULL DEFAULT NULL,
  `is_transit` VARCHAR(5) NULL DEFAULT NULL,
  `location_code` VARCHAR(30) NOT NULL,
  `min_sel_in_amount` DOUBLE NULL DEFAULT NULL,
  `min_sel_in_minimum` DOUBLE NULL DEFAULT NULL,
  `ppcc` VARCHAR(255) NULL DEFAULT NULL,
  `std_sel_in_amount` DOUBLE NULL DEFAULT NULL,
  `std_sel_in_minimum` DOUBLE NULL DEFAULT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `unit_code` VARCHAR(30) NULL DEFAULT NULL,
  `valid_from_date` DATETIME(6) NOT NULL,
  `valid_to_date` DATETIME(6) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  `who_routed` VARCHAR(255) NULL DEFAULT NULL,
  `agent_id` BIGINT(20) NULL DEFAULT NULL,
  `carrier_id` BIGINT(20) NULL DEFAULT NULL,
  `charge_id` BIGINT(20) NULL DEFAULT NULL,
  `commodity_id` BIGINT(20) NULL DEFAULT NULL,
  `company_id` BIGINT(20) NOT NULL,
  `consignee_id` BIGINT(20) NULL DEFAULT NULL,
  `country_id` BIGINT(20) NOT NULL,
  `currency_id` BIGINT(20) NULL DEFAULT NULL,
  `division_id` BIGINT(20) NULL DEFAULT NULL,
  `location_id` BIGINT(20) NOT NULL,
  `pricing_master_id` BIGINT(20) NULL DEFAULT NULL,
  `service_id` BIGINT(20) NULL DEFAULT NULL,
  `shipper_id` BIGINT(20) NULL DEFAULT NULL,
  `tos_id` BIGINT(20) NULL DEFAULT NULL,
  `unit_id` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `FK_PRICPORT_AGTSID` (`agent_id` ASC),
  INDEX `FK_PRICPORT_CARID` (`carrier_id` ASC),
  INDEX `FK_PRICPORT_CHARGID` (`charge_id` ASC),
  INDEX `FK_PRICPORT_CMDTID` (`commodity_id` ASC),
  INDEX `FK_PRICPORT_COMPID` (`company_id` ASC),
  INDEX `FK_PRICPORT_CONSID` (`consignee_id` ASC),
  INDEX `FK_PRICPORT_COUNTID` (`country_id` ASC),
  INDEX `FK_PRICPORT_CURREID` (`currency_id` ASC),
  INDEX `FK_PRICPORT_DIVMASID` (`division_id` ASC),
  INDEX `FK_PRICPORT_LOCTID` (`location_id` ASC),
  INDEX `FK_PRICPORT_PRICID` (`pricing_master_id` ASC),
  INDEX `FK_PPP_SERVICE_ID` (`service_id` ASC),
  INDEX `FK_PRICPORT_SHPID` (`shipper_id` ASC),
  INDEX `FK_PRICPORT_TOSID` (`tos_id` ASC),
  INDEX `FK_PRICPORT_UNITID` (`unit_id` ASC),
  CONSTRAINT `FK_PPP_SERVICE_ID`
    FOREIGN KEY (`service_id`)
    REFERENCES `efsdev`.`efs_service_master` (`id`),
  CONSTRAINT `FK_PRICPORT_AGTSID`
    FOREIGN KEY (`agent_id`)
    REFERENCES `efsdev`.`efs_party_master` (`id`),
  CONSTRAINT `FK_PRICPORT_CARID`
    FOREIGN KEY (`carrier_id`)
    REFERENCES `efsdev`.`efs_carrier_master` (`id`),
  CONSTRAINT `FK_PRICPORT_CHARGID`
    FOREIGN KEY (`charge_id`)
    REFERENCES `efsdev`.`efs_charge_master` (`id`),
  CONSTRAINT `FK_PRICPORT_CMDTID`
    FOREIGN KEY (`commodity_id`)
    REFERENCES `efsdev`.`efs_hs_code_master` (`id`),
  CONSTRAINT `FK_PRICPORT_COMPID`
    FOREIGN KEY (`company_id`)
    REFERENCES `efsdev`.`efs_company_master` (`id`),
  CONSTRAINT `FK_PRICPORT_CONSID`
    FOREIGN KEY (`consignee_id`)
    REFERENCES `efsdev`.`efs_party_master` (`id`),
  CONSTRAINT `FK_PRICPORT_COUNTID`
    FOREIGN KEY (`country_id`)
    REFERENCES `efsdev`.`efs_country_master` (`id`),
  CONSTRAINT `FK_PRICPORT_CURREID`
    FOREIGN KEY (`currency_id`)
    REFERENCES `efsdev`.`efs_currency_master` (`id`),
  CONSTRAINT `FK_PRICPORT_DIVMASID`
    FOREIGN KEY (`division_id`)
    REFERENCES `efsdev`.`efs_division_master` (`id`),
  CONSTRAINT `FK_PRICPORT_LOCTID`
    FOREIGN KEY (`location_id`)
    REFERENCES `efsdev`.`efs_location_master` (`id`),
  CONSTRAINT `FK_PRICPORT_PRICID`
    FOREIGN KEY (`pricing_master_id`)
    REFERENCES `efsdev`.`efs_pricing_master` (`id`),
  CONSTRAINT `FK_PRICPORT_SHPID`
    FOREIGN KEY (`shipper_id`)
    REFERENCES `efsdev`.`efs_party_master` (`id`),
  CONSTRAINT `FK_PRICPORT_TOSID`
    FOREIGN KEY (`tos_id`)
    REFERENCES `efsdev`.`efs_tos_master` (`id`),
  CONSTRAINT `FK_PRICPORT_UNITID`
    FOREIGN KEY (`unit_id`)
    REFERENCES `efsdev`.`efs_unit_master` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_provisional`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_provisional` (
  `id` BIGINT(20) NOT NULL,
  `local_currency_code` VARCHAR(100) NULL DEFAULT NULL,
  `master_uid` VARCHAR(255) NULL DEFAULT NULL,
  `net_amount` DOUBLE NULL DEFAULT NULL,
  `service_name` VARCHAR(255) NULL DEFAULT NULL,
  `service_uid` VARCHAR(255) NULL DEFAULT NULL,
  `shipment_uid` VARCHAR(255) NULL DEFAULT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `total_cost` DOUBLE NULL DEFAULT NULL,
  `total_revenue` DOUBLE NULL DEFAULT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  `local_currency_id` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `FK_PROVISIONAL_LOCCURID` (`local_currency_id` ASC),
  CONSTRAINT `FK_PROVISIONAL_LOCCURID`
    FOREIGN KEY (`local_currency_id`)
    REFERENCES `efsdev`.`efs_currency_master` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_provision_item`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_provision_item` (
  `id` BIGINT(20) NOT NULL,
  `bill_to_party_code` VARCHAR(100) NULL DEFAULT NULL,
  `buy_amount` DOUBLE NULL DEFAULT NULL,
  `buy_amnt_per_unit` DOUBLE NULL DEFAULT NULL,
  `buy_currency_code` VARCHAR(100) NULL DEFAULT NULL,
  `buy_description` VARCHAR(255) NULL DEFAULT NULL,
  `buy_local_amount` DOUBLE NULL DEFAULT NULL,
  `buy_roe` DOUBLE NULL DEFAULT NULL,
  `charge_code` VARCHAR(100) NULL DEFAULT NULL,
  `is_neutral` VARCHAR(20) NULL DEFAULT NULL,
  `is_taxable` BIT(1) NULL DEFAULT NULL,
  `no_of_units` DOUBLE NULL DEFAULT NULL,
  `plus_minus_symbol` VARCHAR(255) NULL DEFAULT NULL,
  `reference` VARCHAR(255) NULL DEFAULT NULL,
  `referencetype_code` VARCHAR(100) NULL DEFAULT NULL,
  `sell_amount` DOUBLE NULL DEFAULT NULL,
  `sell_amnt_per_unit` DOUBLE NULL DEFAULT NULL,
  `sell_currency_code` VARCHAR(100) NULL DEFAULT NULL,
  `sell_description` VARCHAR(255) NULL DEFAULT NULL,
  `sell_local_amount` DOUBLE NULL DEFAULT NULL,
  `sell_roe` DOUBLE NULL DEFAULT NULL,
  `service_uid` VARCHAR(255) NULL DEFAULT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `unit_code` VARCHAR(100) NULL DEFAULT NULL,
  `vendor_subledger_code` VARCHAR(100) NULL DEFAULT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  `bill_to_party_id` BIGINT(20) NULL DEFAULT NULL,
  `buy_currency_id` BIGINT(20) NULL DEFAULT NULL,
  `charge_id` BIGINT(20) NULL DEFAULT NULL,
  `provisional_id` BIGINT(20) NULL DEFAULT NULL,
  `referencetype_id` BIGINT(20) NULL DEFAULT NULL,
  `sell_currency_id` BIGINT(20) NULL DEFAULT NULL,
  `service_id` BIGINT(20) NULL DEFAULT NULL,
  `unit_id` BIGINT(20) NULL DEFAULT NULL,
  `vendor_subledger_id` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `FK_PRVSNLITEM_BILL_PARTYID` (`bill_to_party_id` ASC),
  INDEX `FK_PRVSNLITEM_CURRBUYID` (`buy_currency_id` ASC),
  INDEX `FK_PRVSNLITEM_CHARGEID` (`charge_id` ASC),
  INDEX `FK_PROV_ITEM_PROV_ID` (`provisional_id` ASC),
  INDEX `FK_PRVSNLITEM_RETYPID` (`referencetype_id` ASC),
  INDEX `FK_PRVSNLITEM_CURRSELLID` (`sell_currency_id` ASC),
  INDEX `FK_PRVSNLITEM_SHPMSERVDTLID` (`service_id` ASC),
  INDEX `FK_PRVSNLITEM_UNITID` (`unit_id` ASC),
  INDEX `FK_PRVSNLITEM_BILL_VENDORID` (`vendor_subledger_id` ASC),
  CONSTRAINT `FK_PROV_ITEM_PROV_ID`
    FOREIGN KEY (`provisional_id`)
    REFERENCES `efsdev`.`efs_provisional` (`id`),
  CONSTRAINT `FK_PRVSNLITEM_BILL_PARTYID`
    FOREIGN KEY (`bill_to_party_id`)
    REFERENCES `efsdev`.`efs_party_master` (`id`),
  CONSTRAINT `FK_PRVSNLITEM_BILL_VENDORID`
    FOREIGN KEY (`vendor_subledger_id`)
    REFERENCES `efsdev`.`efs_party_master` (`id`),
  CONSTRAINT `FK_PRVSNLITEM_CHARGEID`
    FOREIGN KEY (`charge_id`)
    REFERENCES `efsdev`.`efs_charge_master` (`id`),
  CONSTRAINT `FK_PRVSNLITEM_CURRBUYID`
    FOREIGN KEY (`buy_currency_id`)
    REFERENCES `efsdev`.`efs_currency_master` (`id`),
  CONSTRAINT `FK_PRVSNLITEM_CURRSELLID`
    FOREIGN KEY (`sell_currency_id`)
    REFERENCES `efsdev`.`efs_currency_master` (`id`),
  CONSTRAINT `FK_PRVSNLITEM_RETYPID`
    FOREIGN KEY (`referencetype_id`)
    REFERENCES `efsdev`.`efs_referencetype_master` (`id`),
  CONSTRAINT `FK_PRVSNLITEM_SHPMSERVDTLID`
    FOREIGN KEY (`service_id`)
    REFERENCES `efsdev`.`efs_service` (`id`),
  CONSTRAINT `FK_PRVSNLITEM_UNITID`
    FOREIGN KEY (`unit_id`)
    REFERENCES `efsdev`.`efs_unit_master` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_purchase_order_item`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_purchase_order_item` (
  `id` BIGINT(20) NOT NULL,
  `amount_per_unit` DOUBLE NULL DEFAULT NULL,
  `currency_code` DOUBLE NULL DEFAULT NULL,
  `delivery_from` DATETIME(6) NULL DEFAULT NULL,
  `delivery_within` DATETIME(6) NULL DEFAULT NULL,
  `description` VARCHAR(250) NULL DEFAULT NULL,
  `item_no` VARCHAR(100) NULL DEFAULT NULL,
  `net_weight` DOUBLE NULL DEFAULT NULL,
  `no_of_pieces` INT(11) NULL DEFAULT NULL,
  `order_piece` INT(11) NULL DEFAULT NULL,
  `order_volume` DOUBLE NULL DEFAULT NULL,
  `order_weight` DOUBLE NULL DEFAULT NULL,
  `pack_code` VARCHAR(255) NULL DEFAULT NULL,
  `pickup_from` DATETIME(6) NULL DEFAULT NULL,
  `pickup_within` DATETIME(6) NULL DEFAULT NULL,
  `piece_or_count` INT(11) NULL DEFAULT NULL,
  `qc_status` VARCHAR(255) NULL DEFAULT NULL,
  `ref1` VARCHAR(255) NULL DEFAULT NULL,
  `ref2` VARCHAR(255) NULL DEFAULT NULL,
  `ref3` VARCHAR(255) NULL DEFAULT NULL,
  `ref4` VARCHAR(255) NULL DEFAULT NULL,
  `ref5` VARCHAR(255) NULL DEFAULT NULL,
  `supplier_conf_date` DATETIME(6) NULL DEFAULT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `total_amount` DOUBLE NULL DEFAULT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  `currency_id` BIGINT(20) NULL DEFAULT NULL,
  `pack_id` BIGINT(20) NULL DEFAULT NULL,
  `purchase_order_id` BIGINT(20) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `FK_PO_ITEM_CURRECNY` (`currency_id` ASC),
  INDEX `FK_PO_ITEM_PACK` (`pack_id` ASC),
  INDEX `FK_PO_ITEM_PO` (`purchase_order_id` ASC),
  CONSTRAINT `FK_PO_ITEM_CURRECNY`
    FOREIGN KEY (`currency_id`)
    REFERENCES `efsdev`.`efs_currency_master` (`id`),
  CONSTRAINT `FK_PO_ITEM_PACK`
    FOREIGN KEY (`pack_id`)
    REFERENCES `efsdev`.`efs_pack_master` (`id`),
  CONSTRAINT `FK_PO_ITEM_PO`
    FOREIGN KEY (`purchase_order_id`)
    REFERENCES `efsdev`.`efs_purchase_order` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_purchase_order_remark`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_purchase_order_remark` (
  `id` BIGINT(20) NOT NULL,
  `pickup_date` DATETIME(6) NULL DEFAULT NULL,
  `delivery_date` DATETIME(6) NULL DEFAULT NULL,
  `remark` VARCHAR(4000) NULL DEFAULT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `type` VARCHAR(20) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  `purchase_order_id` BIGINT(20) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `FK_PO_REMARK_PO` (`purchase_order_id` ASC),
  CONSTRAINT `FK_PO_REMARK_PO`
    FOREIGN KEY (`purchase_order_id`)
    REFERENCES `efsdev`.`efs_purchase_order` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_quotation`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_quotation` (
  `id` BIGINT(20) NOT NULL,
  `agent_code` VARCHAR(100) NULL DEFAULT NULL,
  `approved` VARCHAR(255) NULL DEFAULT NULL,
  `approved_by_code` VARCHAR(100) NULL DEFAULT NULL,
  `approved_date` DATETIME(6) NULL DEFAULT NULL,
  `attention` VARCHAR(100) NULL DEFAULT NULL,
  `booking_no` VARCHAR(100) NULL DEFAULT NULL,
  `company_code` VARCHAR(30) NULL DEFAULT NULL,
  `copy_quotation_no` VARCHAR(100) NULL DEFAULT NULL,
  `country_code` VARCHAR(30) NULL DEFAULT NULL,
  `customer_approved_date` DATETIME(6) NULL DEFAULT NULL,
  `customer_approved_ip` VARCHAR(20) NULL DEFAULT NULL,
  `customer_code` VARCHAR(100) NULL DEFAULT NULL,
  `enquiry_no` VARCHAR(100) NULL DEFAULT NULL,
  `footer_note` LONGTEXT NOT NULL,
  `header_note` LONGTEXT NOT NULL,
  `is_print_carrier` VARCHAR(255) NULL DEFAULT NULL,
  `local_currency_code` VARCHAR(100) NULL DEFAULT NULL,
  `location_code` VARCHAR(30) NULL DEFAULT NULL,
  `logged_by_code` VARCHAR(100) NULL DEFAULT NULL,
  `logged_on` DATETIME(6) NULL DEFAULT NULL,
  `quotation_no` VARCHAR(100) NOT NULL,
  `quote_type` VARCHAR(255) NULL DEFAULT NULL,
  `reject_reason` LONGTEXT NULL DEFAULT NULL,
  `sales_co_ordinator_code` VARCHAR(100) NULL DEFAULT NULL,
  `salesman_code` VARCHAR(100) NULL DEFAULT NULL,
  `shipper_code` VARCHAR(100) NULL DEFAULT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `valid_from` DATETIME(6) NULL DEFAULT NULL,
  `valid_to` DATETIME(6) NULL DEFAULT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  `who_routed` VARCHAR(255) NOT NULL,
  `agent_id` BIGINT(20) NULL DEFAULT NULL,
  `approved_by` BIGINT(20) NULL DEFAULT NULL,
  `company_id` BIGINT(20) NULL DEFAULT NULL,
  `country_id` BIGINT(20) NULL DEFAULT NULL,
  `customer_id` BIGINT(20) NULL DEFAULT NULL,
  `customer_address_id` BIGINT(20) NULL DEFAULT NULL,
  `local_currency_id` BIGINT(20) NULL DEFAULT NULL,
  `location_id` BIGINT(20) NULL DEFAULT NULL,
  `logged_by` BIGINT(20) NULL DEFAULT NULL,
  `sales_co_ordinator_id` BIGINT(20) NULL DEFAULT NULL,
  `salesman_id` BIGINT(20) NULL DEFAULT NULL,
  `shipper_id` BIGINT(20) NULL DEFAULT NULL,
  `shipper_address_id` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `UK_QUOTATION_NO` (`quotation_no` ASC),
  INDEX `FK_QUOTATION_AGENTID` (`agent_id` ASC),
  INDEX `FK_QUOTATION_APPROVEDBY` (`approved_by` ASC),
  INDEX `FK_QUOTATION_COMPANYID` (`company_id` ASC),
  INDEX `FK_QUOTATION_COUNTRYID` (`country_id` ASC),
  INDEX `FK_QUOTATION_CUSTOMERID` (`customer_id` ASC),
  INDEX `FK_QUOTATION_CUSTOMERADD` (`customer_address_id` ASC),
  INDEX `FK_QUOTATION_LOCCURID` (`local_currency_id` ASC),
  INDEX `FK_QUOTATION_LOCATIONID` (`location_id` ASC),
  INDEX `FK_QUOTATION_LOGGEDBY` (`logged_by` ASC),
  INDEX `FK_QUOTATION_SALESCOORID` (`sales_co_ordinator_id` ASC),
  INDEX `FK_QUOTATION_SALESMANID` (`salesman_id` ASC),
  INDEX `FK_QUOTATION_SHIPPERID` (`shipper_id` ASC),
  INDEX `FK_QUOTATION_SHIPPERADD` (`shipper_address_id` ASC),
  CONSTRAINT `FK_QUOTATION_AGENTID`
    FOREIGN KEY (`agent_id`)
    REFERENCES `efsdev`.`efs_party_master` (`id`),
  CONSTRAINT `FK_QUOTATION_APPROVEDBY`
    FOREIGN KEY (`approved_by`)
    REFERENCES `efsdev`.`efs_employee_master` (`id`),
  CONSTRAINT `FK_QUOTATION_COMPANYID`
    FOREIGN KEY (`company_id`)
    REFERENCES `efsdev`.`efs_company_master` (`id`),
  CONSTRAINT `FK_QUOTATION_COUNTRYID`
    FOREIGN KEY (`country_id`)
    REFERENCES `efsdev`.`efs_country_master` (`id`),
  CONSTRAINT `FK_QUOTATION_CUSTOMERADD`
    FOREIGN KEY (`customer_address_id`)
    REFERENCES `efsdev`.`efs_address_master` (`id`),
  CONSTRAINT `FK_QUOTATION_CUSTOMERID`
    FOREIGN KEY (`customer_id`)
    REFERENCES `efsdev`.`efs_party_master` (`id`),
  CONSTRAINT `FK_QUOTATION_LOCATIONID`
    FOREIGN KEY (`location_id`)
    REFERENCES `efsdev`.`efs_location_master` (`id`),
  CONSTRAINT `FK_QUOTATION_LOCCURID`
    FOREIGN KEY (`local_currency_id`)
    REFERENCES `efsdev`.`efs_currency_master` (`id`),
  CONSTRAINT `FK_QUOTATION_LOGGEDBY`
    FOREIGN KEY (`logged_by`)
    REFERENCES `efsdev`.`efs_employee_master` (`id`),
  CONSTRAINT `FK_QUOTATION_SALESCOORID`
    FOREIGN KEY (`sales_co_ordinator_id`)
    REFERENCES `efsdev`.`efs_employee_master` (`id`),
  CONSTRAINT `FK_QUOTATION_SALESMANID`
    FOREIGN KEY (`salesman_id`)
    REFERENCES `efsdev`.`efs_employee_master` (`id`),
  CONSTRAINT `FK_QUOTATION_SHIPPERADD`
    FOREIGN KEY (`shipper_address_id`)
    REFERENCES `efsdev`.`efs_address_master` (`id`),
  CONSTRAINT `FK_QUOTATION_SHIPPERID`
    FOREIGN KEY (`shipper_id`)
    REFERENCES `efsdev`.`efs_party_master` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_quotation_attachment`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_quotation_attachment` (
  `id` BIGINT(20) NOT NULL,
  `file_attached` LONGBLOB NULL DEFAULT NULL,
  `file_content_type` VARCHAR(100) NULL DEFAULT NULL,
  `file_name` VARCHAR(100) NULL DEFAULT NULL,
  `ref_no` VARCHAR(100) NULL DEFAULT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  `quotation_id` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `UK_QUOTATIONATT_REF` (`ref_no` ASC, `quotation_id` ASC),
  INDEX `FK_QUOTEATT_QUTOTEID` (`quotation_id` ASC),
  CONSTRAINT `FK_QUOTEATT_QUTOTEID`
    FOREIGN KEY (`quotation_id`)
    REFERENCES `efsdev`.`efs_quotation` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_quotation_detail`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_quotation_detail` (
  `id` BIGINT(20) NOT NULL,
  `chargeble_weight` DOUBLE NULL DEFAULT NULL,
  `commodity_code` VARCHAR(100) NULL DEFAULT NULL,
  `to_port_code` VARCHAR(20) NOT NULL,
  `dimension_gross_weight` DOUBLE NULL DEFAULT NULL,
  `dimension_gross_weight_in_kg` DOUBLE NULL DEFAULT NULL,
  `dimension_unit` VARCHAR(50) NOT NULL,
  `dimension_unit_value` DOUBLE NULL DEFAULT NULL,
  `dimension_vol_weight` DOUBLE NULL DEFAULT NULL,
  `gross_weight` DOUBLE NULL DEFAULT NULL,
  `hazardous` VARCHAR(255) NULL DEFAULT NULL,
  `from_port_code` VARCHAR(20) NOT NULL,
  `process_instance_id` VARCHAR(255) NULL DEFAULT NULL,
  `quotation_carrier_id` BIGINT(20) NULL DEFAULT NULL,
  `ref_no` VARCHAR(100) NULL DEFAULT NULL,
  `service_code` VARCHAR(100) NULL DEFAULT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `task_id` VARCHAR(255) NULL DEFAULT NULL,
  `tos_code` VARCHAR(20) NULL DEFAULT NULL,
  `total_no_of_pieces` INT(11) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  `volume_weight` DOUBLE NULL DEFAULT NULL,
  `workflow_completed` VARCHAR(5) NULL DEFAULT NULL,
  `commodity_id` BIGINT(20) NULL DEFAULT NULL,
  `delivery_address_id` BIGINT(20) NULL DEFAULT NULL,
  `to_port_id` BIGINT(20) NULL DEFAULT NULL,
  `from_port_id` BIGINT(20) NULL DEFAULT NULL,
  `pickup_address_id` BIGINT(20) NULL DEFAULT NULL,
  `quotation_id` BIGINT(20) NULL DEFAULT NULL,
  `service_id` BIGINT(20) NULL DEFAULT NULL,
  `tos_id` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `FK_QUOTATION_COMMODITYID` (`commodity_id` ASC),
  INDEX `FK_QUOTEDETAIL_DELIVERY` (`delivery_address_id` ASC),
  INDEX `FK_QUOTATION_DESTINATIONID` (`to_port_id` ASC),
  INDEX `FK_QUOTATION_ORIGINID` (`from_port_id` ASC),
  INDEX `FK_QUOTEDETAIL_PICKUP` (`pickup_address_id` ASC),
  INDEX `FK_QUOTATION_DETAIL` (`quotation_id` ASC),
  INDEX `FK_QUOTATIONDETAIL_SERVICEID` (`service_id` ASC),
  INDEX `FK_QUOTATION_TOSID` (`tos_id` ASC),
  CONSTRAINT `FK_QUOTATIONDETAIL_SERVICEID`
    FOREIGN KEY (`service_id`)
    REFERENCES `efsdev`.`efs_service_master` (`id`),
  CONSTRAINT `FK_QUOTATION_COMMODITYID`
    FOREIGN KEY (`commodity_id`)
    REFERENCES `efsdev`.`efs_hs_code_master` (`id`),
  CONSTRAINT `FK_QUOTATION_DESTINATIONID`
    FOREIGN KEY (`to_port_id`)
    REFERENCES `efsdev`.`efs_port_master` (`id`),
  CONSTRAINT `FK_QUOTATION_DETAIL`
    FOREIGN KEY (`quotation_id`)
    REFERENCES `efsdev`.`efs_quotation` (`id`),
  CONSTRAINT `FK_QUOTATION_ORIGINID`
    FOREIGN KEY (`from_port_id`)
    REFERENCES `efsdev`.`efs_port_master` (`id`),
  CONSTRAINT `FK_QUOTATION_TOSID`
    FOREIGN KEY (`tos_id`)
    REFERENCES `efsdev`.`efs_tos_master` (`id`),
  CONSTRAINT `FK_QUOTEDETAIL_DELIVERY`
    FOREIGN KEY (`delivery_address_id`)
    REFERENCES `efsdev`.`efs_address_master` (`id`),
  CONSTRAINT `FK_QUOTEDETAIL_PICKUP`
    FOREIGN KEY (`pickup_address_id`)
    REFERENCES `efsdev`.`efs_address_master` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_quotation_carrier`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_quotation_carrier` (
  `id` BIGINT(20) NOT NULL,
  `carrier_code` VARCHAR(20) NULL DEFAULT NULL,
  `frequency_code` VARCHAR(100) NULL DEFAULT NULL,
  `is_all_inclusive_charge` VARCHAR(10) NULL DEFAULT NULL,
  `lump_sum` DOUBLE NULL DEFAULT NULL,
  `lump_sum_buy_amt` DOUBLE NULL DEFAULT NULL,
  `per_kg` DOUBLE NULL DEFAULT NULL,
  `per_kg_buy` DOUBLE NULL DEFAULT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `transit_day` INT(11) NULL DEFAULT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  `carrier_id` BIGINT(20) NULL DEFAULT NULL,
  `frequency_id` BIGINT(20) NULL DEFAULT NULL,
  `quotation_detail_id` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `FK_QUOTECARR_CARRID` (`carrier_id` ASC),
  INDEX `FK_QUOTECARR_FREQID` (`frequency_id` ASC),
  INDEX `FK_QUOTECARR_QUTOEID` (`quotation_detail_id` ASC),
  CONSTRAINT `FK_QUOTECARR_CARRID`
    FOREIGN KEY (`carrier_id`)
    REFERENCES `efsdev`.`efs_carrier_master` (`id`),
  CONSTRAINT `FK_QUOTECARR_FREQID`
    FOREIGN KEY (`frequency_id`)
    REFERENCES `efsdev`.`efs_period_master` (`id`),
  CONSTRAINT `FK_QUOTECARR_QUTOEID`
    FOREIGN KEY (`quotation_detail_id`)
    REFERENCES `efsdev`.`efs_quotation_detail` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_quotation_carrier_aud`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_quotation_carrier_aud` (
  `id` BIGINT(20) NOT NULL,
  `rev` INT(11) NOT NULL,
  `revtype` TINYINT(4) NULL DEFAULT NULL,
  `quotation_detail_id` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`, `rev`),
  INDEX `FKh2fx8jyu7ek33hk4s8xh8onao` (`rev` ASC),
  CONSTRAINT `FKh2fx8jyu7ek33hk4s8xh8onao`
    FOREIGN KEY (`rev`)
    REFERENCES `efsdev`.`efs_newage_audit_info` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_quotation_charge`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_quotation_charge` (
  `id` BIGINT(20) NOT NULL,
  `actual_chargeable` VARCHAR(255) NULL DEFAULT NULL,
  `agent_rate_request_charge_id` BIGINT(20) NULL DEFAULT NULL,
  `buy_rate_amount` DOUBLE NULL DEFAULT NULL,
  `buy_rate_local_amount` DOUBLE NULL DEFAULT NULL,
  `buy_rate_cost_per_unit` DOUBLE NULL DEFAULT NULL,
  `buy_rate_min_cost` DOUBLE NULL DEFAULT NULL,
  `charge_code` VARCHAR(20) NOT NULL,
  `charge_name` VARCHAR(100) NOT NULL,
  `currency_code` VARCHAR(20) NOT NULL,
  `estimated_unit` DOUBLE NULL DEFAULT NULL,
  `group_name` VARCHAR(255) NULL DEFAULT NULL,
  `is_group` VARCHAR(10) NULL DEFAULT NULL,
  `no_of_unit` VARCHAR(255) NULL DEFAULT NULL,
  `pricing_carrier_id` BIGINT(20) NULL DEFAULT NULL,
  `pricing_port_pair_id` BIGINT(20) NULL DEFAULT NULL,
  `roe` DOUBLE NULL DEFAULT NULL,
  `sell_rate_amount` DOUBLE NULL DEFAULT NULL,
  `sell_rate_amount_per_unit` DOUBLE NULL DEFAULT NULL,
  `sell_rate_local_amount` DOUBLE NULL DEFAULT NULL,
  `sell_rate_min_amount` DOUBLE NULL DEFAULT NULL,
  `surcharge_type` VARCHAR(50) NULL DEFAULT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `unit_code` VARCHAR(20) NOT NULL,
  `value_added_id` BIGINT(20) NULL DEFAULT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  `charge_id` BIGINT(20) NULL DEFAULT NULL,
  `currency_id` BIGINT(20) NULL DEFAULT NULL,
  `quotation_carrier_id` BIGINT(20) NULL DEFAULT NULL,
  `unit_id` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `FK_QUOTECHAR_CHARGEID` (`charge_id` ASC),
  INDEX `FK_QUOTECHAR_CURRENCYID` (`currency_id` ASC),
  INDEX `FK_QUOTECARRR_CARRID` (`quotation_carrier_id` ASC),
  INDEX `FK_QUOTECHAR_UNITID` (`unit_id` ASC),
  CONSTRAINT `FK_QUOTECARRR_CARRID`
    FOREIGN KEY (`quotation_carrier_id`)
    REFERENCES `efsdev`.`efs_quotation_carrier` (`id`),
  CONSTRAINT `FK_QUOTECHAR_CHARGEID`
    FOREIGN KEY (`charge_id`)
    REFERENCES `efsdev`.`efs_charge_master` (`id`),
  CONSTRAINT `FK_QUOTECHAR_CURRENCYID`
    FOREIGN KEY (`currency_id`)
    REFERENCES `efsdev`.`efs_currency_master` (`id`),
  CONSTRAINT `FK_QUOTECHAR_UNITID`
    FOREIGN KEY (`unit_id`)
    REFERENCES `efsdev`.`efs_unit_master` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_quotation_charge_aud`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_quotation_charge_aud` (
  `id` BIGINT(20) NOT NULL,
  `rev` INT(11) NOT NULL,
  `revtype` TINYINT(4) NULL DEFAULT NULL,
  `actual_chargeable` VARCHAR(255) NULL DEFAULT NULL,
  `agent_rate_request_charge_id` BIGINT(20) NULL DEFAULT NULL,
  `buy_rate_amount` DOUBLE NULL DEFAULT NULL,
  `buy_rate_local_amount` DOUBLE NULL DEFAULT NULL,
  `buy_rate_cost_per_unit` DOUBLE NULL DEFAULT NULL,
  `buy_rate_min_cost` DOUBLE NULL DEFAULT NULL,
  `charge_code` VARCHAR(20) NULL DEFAULT NULL,
  `charge_name` VARCHAR(100) NULL DEFAULT NULL,
  `currency_code` VARCHAR(20) NULL DEFAULT NULL,
  `estimated_unit` DOUBLE NULL DEFAULT NULL,
  `group_name` VARCHAR(255) NULL DEFAULT NULL,
  `is_group` VARCHAR(10) NULL DEFAULT NULL,
  `no_of_unit` VARCHAR(255) NULL DEFAULT NULL,
  `pricing_carrier_id` BIGINT(20) NULL DEFAULT NULL,
  `pricing_port_pair_id` BIGINT(20) NULL DEFAULT NULL,
  `roe` DOUBLE NULL DEFAULT NULL,
  `sell_rate_amount` DOUBLE NULL DEFAULT NULL,
  `sell_rate_amount_per_unit` DOUBLE NULL DEFAULT NULL,
  `sell_rate_local_amount` DOUBLE NULL DEFAULT NULL,
  `sell_rate_min_amount` DOUBLE NULL DEFAULT NULL,
  `surcharge_type` VARCHAR(50) NULL DEFAULT NULL,
  `create_date` DATETIME(6) NULL DEFAULT NULL,
  `create_user` VARCHAR(30) NULL DEFAULT NULL,
  `last_updated_date` DATETIME(6) NULL DEFAULT NULL,
  `last_updated_user` VARCHAR(30) NULL DEFAULT NULL,
  `unit_code` VARCHAR(20) NULL DEFAULT NULL,
  `value_added_id` BIGINT(20) NULL DEFAULT NULL,
  `charge_id` BIGINT(20) NULL DEFAULT NULL,
  `currency_id` BIGINT(20) NULL DEFAULT NULL,
  `quotation_carrier_id` BIGINT(20) NULL DEFAULT NULL,
  `unit_id` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`, `rev`),
  INDEX `FKrvtl3ikuyi42tqutmp2t4qbcx` (`rev` ASC),
  CONSTRAINT `FKrvtl3ikuyi42tqutmp2t4qbcx`
    FOREIGN KEY (`rev`)
    REFERENCES `efsdev`.`efs_newage_audit_info` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_quotation_container`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_quotation_container` (
  `id` BIGINT(20) NOT NULL,
  `container_code` VARCHAR(255) NULL DEFAULT NULL,
  `container_name` VARCHAR(255) NULL DEFAULT NULL,
  `no_ofcontainer` INT(11) NULL DEFAULT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  `quotation_detail_id` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `FK_QUOTATIONCONT_ID` (`quotation_detail_id` ASC),
  CONSTRAINT `FK_QUOTATIONCONT_ID`
    FOREIGN KEY (`quotation_detail_id`)
    REFERENCES `efsdev`.`efs_quotation_detail` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_quotation_det_gen_note`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_quotation_det_gen_note` (
  `id` BIGINT(20) NOT NULL,
  `note` LONGTEXT NULL DEFAULT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  `quotation_detail_id` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `FK_QUOTGENNOTDET_QUTOTEDETID` (`quotation_detail_id` ASC),
  CONSTRAINT `FK_QUOTGENNOTDET_QUTOTEDETID`
    FOREIGN KEY (`quotation_detail_id`)
    REFERENCES `efsdev`.`efs_quotation_detail` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_quotation_dimension`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_quotation_dimension` (
  `id` BIGINT(20) NOT NULL,
  `gross_weight` DOUBLE NULL DEFAULT NULL,
  `gross_weight_in_kg` DOUBLE NULL DEFAULT NULL,
  `height` INT(11) NULL DEFAULT NULL,
  `length` INT(11) NULL DEFAULT NULL,
  `no_of_piece` INT(11) NULL DEFAULT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  `vol_weight` DOUBLE NULL DEFAULT NULL,
  `width` INT(11) NULL DEFAULT NULL,
  `quotation_detail_id` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `FK_QUOTATIONDIMENSION_ID` (`quotation_detail_id` ASC),
  CONSTRAINT `FK_QUOTATIONDIMENSION_ID`
    FOREIGN KEY (`quotation_detail_id`)
    REFERENCES `efsdev`.`efs_quotation_detail` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_quotation_general_note`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_quotation_general_note` (
  `id` BIGINT(20) NOT NULL,
  `note` LONGTEXT NULL DEFAULT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  `quotation_id` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `FK_QUOTGENNOT_QUTOTEID` (`quotation_id` ASC),
  CONSTRAINT `FK_QUOTGENNOT_QUTOTEID`
    FOREIGN KEY (`quotation_id`)
    REFERENCES `efsdev`.`efs_quotation` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_quotation_vat_service`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_quotation_vat_service` (
  `id` BIGINT(20) NOT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `value_added_code` VARCHAR(100) NULL DEFAULT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  `quotation_detail_id` BIGINT(20) NULL DEFAULT NULL,
  `value_added_id` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `FK_QUOTATION_QUOTID` (`quotation_detail_id` ASC),
  INDEX `FK__QUOTATION_VALUEADDEDID` (`value_added_id` ASC),
  CONSTRAINT `FK_QUOTATION_QUOTID`
    FOREIGN KEY (`quotation_detail_id`)
    REFERENCES `efsdev`.`efs_quotation_detail` (`id`),
  CONSTRAINT `FK__QUOTATION_VALUEADDEDID`
    FOREIGN KEY (`value_added_id`)
    REFERENCES `efsdev`.`efs_value_added_service` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_recent_history`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_recent_history` (
  `id` BIGINT(20) NOT NULL,
  `created_date` DATETIME(6) NOT NULL,
  `location_id` BIGINT(20) NOT NULL,
  `orgin_and_destination` VARCHAR(255) NULL DEFAULT NULL,
  `service_code` VARCHAR(255) NULL DEFAULT NULL,
  `service_type` VARCHAR(255) NULL DEFAULT NULL,
  `show_service` VARCHAR(255) NULL DEFAULT NULL,
  `state_category` VARCHAR(255) NULL DEFAULT NULL,
  `state_name` VARCHAR(100) NULL DEFAULT NULL,
  `state_param` VARCHAR(4000) NULL DEFAULT NULL,
  `sub_title` VARCHAR(255) NULL DEFAULT NULL,
  `title` VARCHAR(255) NULL DEFAULT NULL,
  `user_id` BIGINT(20) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_record_access_level`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_record_access_level` (
  `id` BIGINT(20) NOT NULL,
  `description` VARCHAR(4000) NULL DEFAULT NULL,
  `ral_name` VARCHAR(100) NOT NULL,
  `status` VARCHAR(10) NOT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `UK_RAL_NAME` (`ral_name` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_record_acc_lv_count`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_record_acc_lv_count` (
  `id` BIGINT(20) NOT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  `country_id` BIGINT(20) NULL DEFAULT NULL,
  `record_access_level_id` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `FK_RALC_COUNTRY_ID` (`country_id` ASC),
  INDEX `FK_RALC_RAL_ID` (`record_access_level_id` ASC),
  CONSTRAINT `FK_RALC_COUNTRY_ID`
    FOREIGN KEY (`country_id`)
    REFERENCES `efsdev`.`efs_country_master` (`id`),
  CONSTRAINT `FK_RALC_RAL_ID`
    FOREIGN KEY (`record_access_level_id`)
    REFERENCES `efsdev`.`efs_record_access_level` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_record_acc_lvl_cust`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_record_acc_lvl_cust` (
  `id` BIGINT(20) NOT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  `party_id` BIGINT(20) NULL DEFAULT NULL,
  `record_access_level_id` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `FK_RALP_PARTY_ID` (`party_id` ASC),
  INDEX `FK_RALP_RAL_ID` (`record_access_level_id` ASC),
  CONSTRAINT `FK_RALP_PARTY_ID`
    FOREIGN KEY (`party_id`)
    REFERENCES `efsdev`.`efs_party_master` (`id`),
  CONSTRAINT `FK_RALP_RAL_ID`
    FOREIGN KEY (`record_access_level_id`)
    REFERENCES `efsdev`.`efs_record_access_level` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_record_acc_lvl_divis`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_record_acc_lvl_divis` (
  `id` BIGINT(20) NOT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  `division_id` BIGINT(20) NULL DEFAULT NULL,
  `record_access_level_id` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `FK_RALL_LOCATION_ID` (`division_id` ASC),
  INDEX `FK_RALL_RAL_ID` (`record_access_level_id` ASC),
  CONSTRAINT `FK_RALL_LOCATION_ID`
    FOREIGN KEY (`division_id`)
    REFERENCES `efsdev`.`efs_division_master` (`id`),
  CONSTRAINT `FK_RALL_RAL_ID`
    FOREIGN KEY (`record_access_level_id`)
    REFERENCES `efsdev`.`efs_record_access_level` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_record_acc_lvl_locate`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_record_acc_lvl_locate` (
  `id` BIGINT(20) NOT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  `location_id` BIGINT(20) NULL DEFAULT NULL,
  `record_access_level_id` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `FK_RALD_DIVISION_ID` (`location_id` ASC),
  INDEX `FK_RALD_RAL_ID` (`record_access_level_id` ASC),
  CONSTRAINT `FK_RALD_DIVISION_ID`
    FOREIGN KEY (`location_id`)
    REFERENCES `efsdev`.`efs_location_master` (`id`),
  CONSTRAINT `FK_RALD_RAL_ID`
    FOREIGN KEY (`record_access_level_id`)
    REFERENCES `efsdev`.`efs_record_access_level` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_record_acc_lvl_salman`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_record_acc_lvl_salman` (
  `id` BIGINT(20) NOT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  `employee_id` BIGINT(20) NULL DEFAULT NULL,
  `record_access_level_id` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `FK_RALS_EMPLOYEE_ID` (`employee_id` ASC),
  INDEX `FK_RALS_RAL_ID` (`record_access_level_id` ASC),
  CONSTRAINT `FK_RALS_EMPLOYEE_ID`
    FOREIGN KEY (`employee_id`)
    REFERENCES `efsdev`.`efs_employee_master` (`id`),
  CONSTRAINT `FK_RALS_RAL_ID`
    FOREIGN KEY (`record_access_level_id`)
    REFERENCES `efsdev`.`efs_record_access_level` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_record_acc_lvl_serv`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_record_acc_lvl_serv` (
  `id` BIGINT(20) NOT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  `record_access_level_id` BIGINT(20) NULL DEFAULT NULL,
  `service_id` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `FK_RALSE_RAL_ID` (`record_access_level_id` ASC),
  INDEX `FK_RALSE_SERVICE_ID` (`service_id` ASC),
  CONSTRAINT `FK_RALSE_RAL_ID`
    FOREIGN KEY (`record_access_level_id`)
    REFERENCES `efsdev`.`efs_record_access_level` (`id`),
  CONSTRAINT `FK_RALSE_SERVICE_ID`
    FOREIGN KEY (`service_id`)
    REFERENCES `efsdev`.`efs_service_master` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_record_access_level_user`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_record_access_level_user` (
  `id` BIGINT(20) NOT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  `record_access_level_id` BIGINT(20) NULL DEFAULT NULL,
  `user_id` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `FK_RALU_RAL_ID` (`record_access_level_id` ASC),
  INDEX `FK_RALU_USER_ID` (`user_id` ASC),
  CONSTRAINT `FK_RALU_RAL_ID`
    FOREIGN KEY (`record_access_level_id`)
    REFERENCES `efsdev`.`efs_record_access_level` (`id`),
  CONSTRAINT `FK_RALU_USER_ID`
    FOREIGN KEY (`user_id`)
    REFERENCES `efsdev`.`efs_user_profile` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_regular_expression`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_regular_expression` (
  `id` BIGINT(20) NOT NULL,
  `java_reg_exp` VARCHAR(255) NOT NULL,
  `js_reg_exp` VARCHAR(255) NOT NULL,
  `reg_exp_name` VARCHAR(50) NOT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `UK_REGEXP_NAME` (`reg_exp_name` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_report_config_master`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_report_config_master` (
  `id` BIGINT(20) NOT NULL,
  `location_code` VARCHAR(50) NULL DEFAULT NULL,
  `report_key` VARCHAR(250) NOT NULL,
  `report_value` VARCHAR(4000) NULL DEFAULT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  `location_id` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `UK_REPORT_CONF_KEY` (`report_key` ASC, `location_id` ASC),
  INDEX `FK_REPORT_CONF_LOCID` (`location_id` ASC),
  CONSTRAINT `FK_REPORT_CONF_LOCID`
    FOREIGN KEY (`location_id`)
    REFERENCES `efsdev`.`efs_location_master` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_report_master`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_report_master` (
  `id` BIGINT(20) NOT NULL,
  `is_report_enable` VARCHAR(5) NULL DEFAULT NULL,
  `report_key` VARCHAR(100) NOT NULL,
  `report_master_code` VARCHAR(30) NOT NULL,
  `report_master_name` VARCHAR(100) NOT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  `docmaster_id` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `UK_REPORT_REPORTCODE` (`report_master_name` ASC),
  UNIQUE INDEX `UK_REPORT_REPORTNAME` (`report_master_code` ASC),
  UNIQUE INDEX `UK_REPORT_REPORTKEY` (`report_key` ASC),
  INDEX `FK_REPORT_DOCUMENT_ID` (`docmaster_id` ASC),
  CONSTRAINT `FK_REPORT_DOCUMENT_ID`
    FOREIGN KEY (`docmaster_id`)
    REFERENCES `efsdev`.`efs_document_master` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_report_template`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_report_template` (
  `id` BIGINT(20) NOT NULL,
  `default_report` VARCHAR(5) NULL DEFAULT NULL,
  `report_category` VARCHAR(255) NULL DEFAULT NULL,
  `report_name` VARCHAR(255) NULL DEFAULT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `template` LONGTEXT NULL DEFAULT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `COMP_UNQ_R_TEMPLATE_CAT` (`report_category` ASC, `report_name` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_report_parameter`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_report_parameter` (
  `id` BIGINT(20) NOT NULL,
  `param_key` VARCHAR(255) NULL DEFAULT NULL,
  `param_value` VARCHAR(255) NULL DEFAULT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  `report_template_id` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `FK_R_PARAM_R_ID` (`report_template_id` ASC),
  CONSTRAINT `FK_R_PARAM_R_ID`
    FOREIGN KEY (`report_template_id`)
    REFERENCES `efsdev`.`efs_report_template` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_reports`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_reports` (
  `id` BIGINT(20) NOT NULL,
  `report_key` VARCHAR(255) NULL DEFAULT NULL,
  `report_name` VARCHAR(255) NULL DEFAULT NULL,
  `short_name` VARCHAR(255) NULL DEFAULT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_reports_form_data`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_reports_form_data` (
  `id` BIGINT(20) NOT NULL,
  `autocomplete_searchmtd` VARCHAR(255) NULL DEFAULT NULL,
  `col_width` VARCHAR(255) NULL DEFAULT NULL,
  `data_type` VARCHAR(255) NULL DEFAULT NULL,
  `date_picker_format` VARCHAR(255) NULL DEFAULT NULL,
  `datetime_picker_format` VARCHAR(255) NULL DEFAULT NULL,
  `default_boolean` BIT(1) NOT NULL,
  `default_date` VARCHAR(255) NULL DEFAULT NULL,
  `default_time` VARCHAR(255) NULL DEFAULT NULL,
  `dynamicmaster` VARCHAR(255) NULL DEFAULT NULL,
  `is_required` BIT(1) NOT NULL,
  `label_name` VARCHAR(255) NULL DEFAULT NULL,
  `list_style` VARCHAR(255) NULL DEFAULT NULL,
  `lov_general_render` VARCHAR(255) NULL DEFAULT NULL,
  `max_length` INT(11) NOT NULL,
  `min_length` INT(11) NOT NULL,
  `model_name` VARCHAR(255) NULL DEFAULT NULL,
  `render_item` VARCHAR(255) NULL DEFAULT NULL,
  `row_no` INT(11) NOT NULL,
  `selected_value` VARCHAR(255) NULL DEFAULT NULL,
  `time_picker_format` VARCHAR(255) NULL DEFAULT NULL,
  `userdefined_array` VARCHAR(255) NULL DEFAULT NULL,
  `report_id` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `FK_REPORTFORMDATA_PID` (`report_id` ASC),
  CONSTRAINT `FK_REPORTFORMDATA_PID`
    FOREIGN KEY (`report_id`)
    REFERENCES `efsdev`.`efs_reports` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_reset_password_history`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_reset_password_history` (
  `id` BIGINT(20) NOT NULL,
  `create_date` DATETIME(6) NULL DEFAULT NULL,
  `create_user` VARCHAR(30) NULL DEFAULT NULL,
  `email_id` VARCHAR(100) NOT NULL,
  `error_code` VARCHAR(30) NULL DEFAULT NULL,
  `error_description` VARCHAR(1000) NULL DEFAULT NULL,
  `expiry_date` DATETIME(6) NULL DEFAULT NULL,
  `last_updated_date` DATETIME(6) NULL DEFAULT NULL,
  `last_updated_user` VARCHAR(30) NULL DEFAULT NULL,
  `status` VARCHAR(10) NULL DEFAULT NULL,
  `url` VARCHAR(300) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_role`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_role` (
  `id` BIGINT(20) NOT NULL,
  `description` VARCHAR(4000) NULL DEFAULT NULL,
  `role_name` VARCHAR(100) NOT NULL,
  `status` VARCHAR(10) NOT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `UK_ROLE_ROLENAME` (`role_name` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_role_has_feature`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_role_has_feature` (
  `id` BIGINT(20) NOT NULL,
  `val_create` VARCHAR(255) NULL DEFAULT NULL,
  `val_delete` VARCHAR(255) NULL DEFAULT NULL,
  `val_download` VARCHAR(255) NULL DEFAULT NULL,
  `val_list` VARCHAR(255) NULL DEFAULT NULL,
  `val_modify` VARCHAR(255) NULL DEFAULT NULL,
  `val_view` VARCHAR(255) NULL DEFAULT NULL,
  `feature_id` BIGINT(20) NULL DEFAULT NULL,
  `role_id` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `UK_ROLEHASFEATURE_UNQ` (`role_id` ASC, `feature_id` ASC),
  INDEX `FK_ROLEHASFEATURE_FEATURE` (`feature_id` ASC),
  CONSTRAINT `FK_ROLEHASFEATURE_FEATURE`
    FOREIGN KEY (`feature_id`)
    REFERENCES `efsdev`.`efs_feature` (`id`),
  CONSTRAINT `FK_ROLEHASFEATURE_ROLE`
    FOREIGN KEY (`role_id`)
    REFERENCES `efsdev`.`efs_role` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_sequence_generator`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_sequence_generator` (
  `id` BIGINT(20) NOT NULL,
  `company_code` VARCHAR(50) NOT NULL,
  `current_sequence_value` BIGINT(20) NULL DEFAULT NULL,
  `format` VARCHAR(255) NULL DEFAULT NULL,
  `is_month` VARCHAR(10) NULL DEFAULT NULL,
  `is_year` VARCHAR(10) NULL DEFAULT NULL,
  `prefix` VARCHAR(5) NULL DEFAULT NULL,
  `separator` VARCHAR(1) NULL DEFAULT NULL,
  `sequence_name` VARCHAR(50) NULL DEFAULT NULL,
  `sequence_type` VARCHAR(30) NULL DEFAULT NULL,
  `suffix` VARCHAR(5) NULL DEFAULT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  `company_id` BIGINT(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `UK_SEQUENCE_TYPE` (`sequence_type` ASC, `company_id` ASC),
  UNIQUE INDEX `UK_SEQUENCE_NAME` (`sequence_name` ASC, `company_id` ASC),
  INDEX `FK_SEQUENCE_COMPANY` (`company_id` ASC),
  CONSTRAINT `FK_SEQUENCE_COMPANY`
    FOREIGN KEY (`company_id`)
    REFERENCES `efsdev`.`efs_company_master` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_service_account_mapping`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_service_account_mapping` (
  `id` BIGINT(20) NOT NULL,
  `note` LONGTEXT NOT NULL,
  `service_code` VARCHAR(30) NOT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  `account_id` BIGINT(20) NOT NULL,
  `service_id` BIGINT(20) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `FK_SERACCMAPP_ACCOUNT` (`account_id` ASC),
  INDEX `FK_SERACCMAPP_SERVIID` (`service_id` ASC),
  CONSTRAINT `FK_SERACCMAPP_ACCOUNT`
    FOREIGN KEY (`account_id`)
    REFERENCES `efsdev`.`efs_general_ledger_account` (`id`),
  CONSTRAINT `FK_SERACCMAPP_SERVIID`
    FOREIGN KEY (`service_id`)
    REFERENCES `efsdev`.`efs_service_master` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_service_connection`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_service_connection` (
  `id` BIGINT(20) NOT NULL,
  `carrier_code` VARCHAR(100) NULL DEFAULT NULL,
  `connection_status` VARCHAR(30) NOT NULL,
  `eta` DATETIME(6) NOT NULL,
  `etd` DATETIME(6) NOT NULL,
  `flight_voyage_no` VARCHAR(255) NOT NULL,
  `move` VARCHAR(10) NOT NULL,
  `pod` VARCHAR(255) NOT NULL,
  `pol` VARCHAR(255) NOT NULL,
  `service_uid` VARCHAR(255) NOT NULL,
  `shipment_uid` VARCHAR(255) NOT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  `carrier_id` BIGINT(20) NULL DEFAULT NULL,
  `pod_id` BIGINT(20) NOT NULL,
  `pol_id` BIGINT(20) NOT NULL,
  `service_id` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `FK_SERCONN_CARRIERID` (`carrier_id` ASC),
  INDEX `FK_SERCONN_PODID` (`pod_id` ASC),
  INDEX `FK_SERCONN_POLID` (`pol_id` ASC),
  INDEX `FK_SERCONN_SERVICEID` (`service_id` ASC),
  CONSTRAINT `FK_SERCONN_CARRIERID`
    FOREIGN KEY (`carrier_id`)
    REFERENCES `efsdev`.`efs_carrier_master` (`id`),
  CONSTRAINT `FK_SERCONN_PODID`
    FOREIGN KEY (`pod_id`)
    REFERENCES `efsdev`.`efs_port_master` (`id`),
  CONSTRAINT `FK_SERCONN_POLID`
    FOREIGN KEY (`pol_id`)
    REFERENCES `efsdev`.`efs_port_master` (`id`),
  CONSTRAINT `FK_SERCONN_SERVICEID`
    FOREIGN KEY (`service_id`)
    REFERENCES `efsdev`.`efs_service` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_service_doc_dimen`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_service_doc_dimen` (
  `id` BIGINT(20) NOT NULL,
  `gross_weight` DOUBLE NULL DEFAULT NULL,
  `gross_weight_in_kg` DOUBLE NULL DEFAULT NULL,
  `height` INT(11) NULL DEFAULT NULL,
  `length` INT(11) NULL DEFAULT NULL,
  `no_of_pieces` INT(11) NULL DEFAULT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  `volume_weight` DOUBLE NULL DEFAULT NULL,
  `width` INT(11) NULL DEFAULT NULL,
  `document_id` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `FK_SERDOCDIM_DOCID` (`document_id` ASC),
  CONSTRAINT `FK_SERDOCDIM_DOCID`
    FOREIGN KEY (`document_id`)
    REFERENCES `efsdev`.`efs_document_detail` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_service_mapping`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_service_mapping` (
  `id` BIGINT(20) NOT NULL,
  `export_code` VARCHAR(20) NOT NULL,
  `import_code` VARCHAR(20) NOT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  `export_service_id` BIGINT(20) NULL DEFAULT NULL,
  `import_service_id` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `UK_SERMAP_IMPID` (`import_service_id` ASC),
  UNIQUE INDEX `UK_SERMAP_EXPID` (`export_service_id` ASC),
  CONSTRAINT `FK_SERVICEMPG_EXPORTID`
    FOREIGN KEY (`export_service_id`)
    REFERENCES `efsdev`.`efs_service_master` (`id`),
  CONSTRAINT `FK_SERVICEMPG_IMPORTID`
    FOREIGN KEY (`import_service_id`)
    REFERENCES `efsdev`.`efs_service_master` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_service_signoff_aud`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_service_signoff_aud` (
  `id` BIGINT(20) NOT NULL,
  `rev` INT(11) NOT NULL,
  `revtype` TINYINT(4) NULL DEFAULT NULL,
  `consol_uid` VARCHAR(255) NULL DEFAULT NULL,
  `description` VARCHAR(4000) NULL DEFAULT NULL,
  `is_sign_off` VARCHAR(10) NULL DEFAULT NULL,
  `service_uid` VARCHAR(255) NULL DEFAULT NULL,
  `shipment_uid` VARCHAR(255) NULL DEFAULT NULL,
  `signoff_by_code` VARCHAR(100) NULL DEFAULT NULL,
  `signoff_date` DATETIME(6) NULL DEFAULT NULL,
  `create_date` DATETIME(6) NULL DEFAULT NULL,
  `create_user` VARCHAR(30) NULL DEFAULT NULL,
  `last_updated_date` DATETIME(6) NULL DEFAULT NULL,
  `last_updated_user` VARCHAR(30) NULL DEFAULT NULL,
  `comment_id` BIGINT(20) NULL DEFAULT NULL,
  `signoff_by` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`, `rev`),
  INDEX `FK338p0htm146xrqo18pld6wg4k` (`rev` ASC),
  CONSTRAINT `FK338p0htm146xrqo18pld6wg4k`
    FOREIGN KEY (`rev`)
    REFERENCES `efsdev`.`efs_newage_audit_info` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_service_tax_chr_map`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_service_tax_chr_map` (
  `id` BIGINT(20) NOT NULL,
  `charge_code` VARCHAR(30) NOT NULL,
  `service_code` VARCHAR(30) NOT NULL,
  `service_tax_category_code` VARCHAR(30) NOT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  `charge_id` BIGINT(20) NOT NULL,
  `service_id` BIGINT(20) NOT NULL,
  `service_tax_category_id` BIGINT(20) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `FK_SERVTAXCHAM_CHARFID` (`charge_id` ASC),
  INDEX `FK_SERVTAXCHAM_SERVIID` (`service_id` ASC),
  INDEX `FK_SERVTAXCHAM_TAXCATID` (`service_tax_category_id` ASC),
  CONSTRAINT `FK_SERVTAXCHAM_CHARFID`
    FOREIGN KEY (`charge_id`)
    REFERENCES `efsdev`.`efs_charge_master` (`id`),
  CONSTRAINT `FK_SERVTAXCHAM_SERVIID`
    FOREIGN KEY (`service_id`)
    REFERENCES `efsdev`.`efs_service_master` (`id`),
  CONSTRAINT `FK_SERVTAXCHAM_TAXCATID`
    FOREIGN KEY (`service_tax_category_id`)
    REFERENCES `efsdev`.`efs_service_tax_categ` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_shipment_attachment`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_shipment_attachment` (
  `id` BIGINT(20) NOT NULL,
  `file_content` LONGBLOB NULL DEFAULT NULL,
  `file_content_type` VARCHAR(100) NULL DEFAULT NULL,
  `file_name` VARCHAR(100) NULL DEFAULT NULL,
  `is_customs` VARCHAR(255) NULL DEFAULT NULL,
  `is_protected` VARCHAR(255) NULL DEFAULT NULL,
  `ref_no` VARCHAR(100) NULL DEFAULT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  `document_id` BIGINT(20) NULL DEFAULT NULL,
  `shipment_service_id` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `UK_SHIPMENTATT_REF` (`ref_no` ASC, `shipment_service_id` ASC),
  INDEX `FK_SHIPMENT_DOCUMENTID` (`document_id` ASC),
  INDEX `FK_SHIPATTCH_SERVICEID` (`shipment_service_id` ASC),
  CONSTRAINT `FK_SHIPATTCH_SERVICEID`
    FOREIGN KEY (`shipment_service_id`)
    REFERENCES `efsdev`.`efs_service` (`id`),
  CONSTRAINT `FK_SHIPMENT_DOCUMENTID`
    FOREIGN KEY (`document_id`)
    REFERENCES `efsdev`.`efs_document_master` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_shipment_link`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_shipment_link` (
  `id` BIGINT(20) NOT NULL,
  `company_code` VARCHAR(10) NULL DEFAULT NULL,
  `consol_uid` VARCHAR(255) NULL DEFAULT NULL,
  `country_code` VARCHAR(10) NULL DEFAULT NULL,
  `location_code` VARCHAR(10) NULL DEFAULT NULL,
  `service_uid` VARCHAR(255) NULL DEFAULT NULL,
  `shipment_uid` VARCHAR(255) NULL DEFAULT NULL,
  `sub_job_sequence` INT(11) NULL DEFAULT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  `company_id` BIGINT(20) NULL DEFAULT NULL,
  `consol_id` BIGINT(20) NULL DEFAULT NULL,
  `country_id` BIGINT(20) NULL DEFAULT NULL,
  `location_id` BIGINT(20) NULL DEFAULT NULL,
  `service_id` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `UK_CONSOLLINK_SUBJOBID` (`consol_id` ASC, `sub_job_sequence` ASC),
  INDEX `FK_SHIPLINK_COMPANYID` (`company_id` ASC),
  INDEX `FK_SHIPLINK_COUNTRYID` (`country_id` ASC),
  INDEX `FK_SHIPLINK_LOCATIONID` (`location_id` ASC),
  INDEX `FK_SHIPLINK_SERVICEID` (`service_id` ASC),
  CONSTRAINT `FK_SHIPLINK_COMPANYID`
    FOREIGN KEY (`company_id`)
    REFERENCES `efsdev`.`efs_company_master` (`id`),
  CONSTRAINT `FK_SHIPLINK_CONSOLID`
    FOREIGN KEY (`consol_id`)
    REFERENCES `efsdev`.`efs_consol` (`id`),
  CONSTRAINT `FK_SHIPLINK_COUNTRYID`
    FOREIGN KEY (`country_id`)
    REFERENCES `efsdev`.`efs_country_master` (`id`),
  CONSTRAINT `FK_SHIPLINK_LOCATIONID`
    FOREIGN KEY (`location_id`)
    REFERENCES `efsdev`.`efs_location_master` (`id`),
  CONSTRAINT `FK_SHIPLINK_SERVICEID`
    FOREIGN KEY (`service_id`)
    REFERENCES `efsdev`.`efs_service` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_shipment_rate`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_shipment_rate` (
  `id` BIGINT(20) NOT NULL,
  `actual_chargeable` VARCHAR(255) NULL DEFAULT NULL,
  `bill_to_type` VARCHAR(255) NULL DEFAULT NULL,
  `charge_code` VARCHAR(255) NULL DEFAULT NULL,
  `charge_name` VARCHAR(255) NULL DEFAULT NULL,
  `company_code` VARCHAR(100) NULL DEFAULT NULL,
  `cost_amount` DOUBLE NULL DEFAULT NULL,
  `cost_currency_code` VARCHAR(10) NULL DEFAULT NULL,
  `cost_minimum` DOUBLE NULL DEFAULT NULL,
  `country_code` VARCHAR(10) NULL DEFAULT NULL,
  `declared_sale_amount` DOUBLE NULL DEFAULT NULL,
  `declared_sale_currency_code` VARCHAR(10) NULL DEFAULT NULL,
  `declared_sale_minimum` DOUBLE NULL DEFAULT NULL,
  `due` VARCHAR(255) NULL DEFAULT NULL,
  `estimated_unit` DOUBLE NULL DEFAULT NULL,
  `gross_sale_amount` DOUBLE NULL DEFAULT NULL,
  `gross_sale_currency_code` VARCHAR(10) NULL DEFAULT NULL,
  `gross_sale_minimum` DOUBLE NULL DEFAULT NULL,
  `local_amount` DOUBLE NULL DEFAULT NULL,
  `local_cost_amount_roe` DOUBLE NULL DEFAULT NULL,
  `local_currency_code` VARCHAR(10) NULL DEFAULT NULL,
  `local_declared_sale_roe` DOUBLE NULL DEFAULT NULL,
  `local_gross_sale_roe` DOUBLE NULL DEFAULT NULL,
  `local_net_sale_roe` DOUBLE NULL DEFAULT NULL,
  `local_rate_amount_roe` DOUBLE NULL DEFAULT NULL,
  `local_total_cost_amount` DOUBLE NULL DEFAULT NULL,
  `local_total_declared_sale` DOUBLE NULL DEFAULT NULL,
  `local_total_gross_sale` DOUBLE NULL DEFAULT NULL,
  `local_total_net_sale` DOUBLE NULL DEFAULT NULL,
  `local_total_rate_amount` DOUBLE NULL DEFAULT NULL,
  `location_code` VARCHAR(100) NULL DEFAULT NULL,
  `net_sale_amount` DOUBLE NULL DEFAULT NULL,
  `net_sale_currency_code` VARCHAR(10) NULL DEFAULT NULL,
  `net_sale_minimum` DOUBLE NULL DEFAULT NULL,
  `note` LONGTEXT NULL DEFAULT NULL,
  `no_of_unit` VARCHAR(255) NULL DEFAULT NULL,
  `ppcc` VARCHAR(255) NULL DEFAULT NULL,
  `quotation_id` BIGINT(20) NULL DEFAULT NULL,
  `rate_amount` DOUBLE NULL DEFAULT NULL,
  `rate_currency_code` VARCHAR(10) NULL DEFAULT NULL,
  `rate_minimum` DOUBLE NULL DEFAULT NULL,
  `service_uid` VARCHAR(255) NULL DEFAULT NULL,
  `shipment_uid` VARCHAR(255) NULL DEFAULT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `total_cost_amount` DOUBLE NULL DEFAULT NULL,
  `total_declared_sale` DOUBLE NULL DEFAULT NULL,
  `total_gross_sale` DOUBLE NULL DEFAULT NULL,
  `total_net_sale` DOUBLE NULL DEFAULT NULL,
  `total_rate_amount` DOUBLE NULL DEFAULT NULL,
  `unit_code` VARCHAR(255) NULL DEFAULT NULL,
  `vendor_code` VARCHAR(255) NULL DEFAULT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  `charge_id` BIGINT(20) NULL DEFAULT NULL,
  `company_id` BIGINT(20) NULL DEFAULT NULL,
  `cost_currency_id` BIGINT(20) NULL DEFAULT NULL,
  `country_id` BIGINT(20) NULL DEFAULT NULL,
  `declared_sale_currency_id` BIGINT(20) NULL DEFAULT NULL,
  `gross_sale_currency_id` BIGINT(20) NULL DEFAULT NULL,
  `local_currency_id` BIGINT(20) NULL DEFAULT NULL,
  `location_id` BIGINT(20) NULL DEFAULT NULL,
  `net_sale_currency_id` BIGINT(20) NULL DEFAULT NULL,
  `rate_currency_id` BIGINT(20) NULL DEFAULT NULL,
  `service_id` BIGINT(20) NULL DEFAULT NULL,
  `unit_id` BIGINT(20) NULL DEFAULT NULL,
  `vendor_id` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `FK_SHIPRATE_CHARGEID` (`charge_id` ASC),
  INDEX `FK_SHIPRATE_COMPANYID` (`company_id` ASC),
  INDEX `FK_SHIPRATE_COSTCURRENCYID` (`cost_currency_id` ASC),
  INDEX `FK_SHIPRATE_COUNTRYID` (`country_id` ASC),
  INDEX `FK_SHIPRATE_DECSALECURRID` (`declared_sale_currency_id` ASC),
  INDEX `FK_SHIPRATE_GROSALECURRID` (`gross_sale_currency_id` ASC),
  INDEX `FK_SHIPRATE_LOCALCURR` (`local_currency_id` ASC),
  INDEX `FK_SHIPRATE_LOCATIONID` (`location_id` ASC),
  INDEX `FK_SHIPRATE_NETSALECURRID` (`net_sale_currency_id` ASC),
  INDEX `FK_SHIPRATE_RATECURRENCYID` (`rate_currency_id` ASC),
  INDEX `FK_SHIPRATE_SERVICEID` (`service_id` ASC),
  INDEX `FK_SHIPRATE_UNITID` (`unit_id` ASC),
  INDEX `FK_SHIPRATE_VENDORID` (`vendor_id` ASC),
  CONSTRAINT `FK_SHIPRATE_CHARGEID`
    FOREIGN KEY (`charge_id`)
    REFERENCES `efsdev`.`efs_charge_master` (`id`),
  CONSTRAINT `FK_SHIPRATE_COMPANYID`
    FOREIGN KEY (`company_id`)
    REFERENCES `efsdev`.`efs_company_master` (`id`),
  CONSTRAINT `FK_SHIPRATE_COSTCURRENCYID`
    FOREIGN KEY (`cost_currency_id`)
    REFERENCES `efsdev`.`efs_currency_master` (`id`),
  CONSTRAINT `FK_SHIPRATE_COUNTRYID`
    FOREIGN KEY (`country_id`)
    REFERENCES `efsdev`.`efs_country_master` (`id`),
  CONSTRAINT `FK_SHIPRATE_DECSALECURRID`
    FOREIGN KEY (`declared_sale_currency_id`)
    REFERENCES `efsdev`.`efs_currency_master` (`id`),
  CONSTRAINT `FK_SHIPRATE_GROSALECURRID`
    FOREIGN KEY (`gross_sale_currency_id`)
    REFERENCES `efsdev`.`efs_currency_master` (`id`),
  CONSTRAINT `FK_SHIPRATE_LOCALCURR`
    FOREIGN KEY (`local_currency_id`)
    REFERENCES `efsdev`.`efs_currency_master` (`id`),
  CONSTRAINT `FK_SHIPRATE_LOCATIONID`
    FOREIGN KEY (`location_id`)
    REFERENCES `efsdev`.`efs_location_master` (`id`),
  CONSTRAINT `FK_SHIPRATE_NETSALECURRID`
    FOREIGN KEY (`net_sale_currency_id`)
    REFERENCES `efsdev`.`efs_currency_master` (`id`),
  CONSTRAINT `FK_SHIPRATE_RATECURRENCYID`
    FOREIGN KEY (`rate_currency_id`)
    REFERENCES `efsdev`.`efs_currency_master` (`id`),
  CONSTRAINT `FK_SHIPRATE_SERVICEID`
    FOREIGN KEY (`service_id`)
    REFERENCES `efsdev`.`efs_service` (`id`),
  CONSTRAINT `FK_SHIPRATE_UNITID`
    FOREIGN KEY (`unit_id`)
    REFERENCES `efsdev`.`efs_unit_master` (`id`),
  CONSTRAINT `FK_SHIPRATE_VENDORID`
    FOREIGN KEY (`vendor_id`)
    REFERENCES `efsdev`.`efs_party_master` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_shipment_rate_aud`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_shipment_rate_aud` (
  `id` BIGINT(20) NOT NULL,
  `rev` INT(11) NOT NULL,
  `revtype` TINYINT(4) NULL DEFAULT NULL,
  `actual_chargeable` VARCHAR(255) NULL DEFAULT NULL,
  `bill_to_type` VARCHAR(255) NULL DEFAULT NULL,
  `charge_code` VARCHAR(255) NULL DEFAULT NULL,
  `charge_name` VARCHAR(255) NULL DEFAULT NULL,
  `company_code` VARCHAR(100) NULL DEFAULT NULL,
  `cost_amount` DOUBLE NULL DEFAULT NULL,
  `cost_currency_code` VARCHAR(10) NULL DEFAULT NULL,
  `cost_minimum` DOUBLE NULL DEFAULT NULL,
  `country_code` VARCHAR(10) NULL DEFAULT NULL,
  `declared_sale_amount` DOUBLE NULL DEFAULT NULL,
  `declared_sale_currency_code` VARCHAR(10) NULL DEFAULT NULL,
  `declared_sale_minimum` DOUBLE NULL DEFAULT NULL,
  `due` VARCHAR(255) NULL DEFAULT NULL,
  `estimated_unit` DOUBLE NULL DEFAULT NULL,
  `gross_sale_amount` DOUBLE NULL DEFAULT NULL,
  `gross_sale_currency_code` VARCHAR(10) NULL DEFAULT NULL,
  `gross_sale_minimum` DOUBLE NULL DEFAULT NULL,
  `local_amount` DOUBLE NULL DEFAULT NULL,
  `local_cost_amount_roe` DOUBLE NULL DEFAULT NULL,
  `local_currency_code` VARCHAR(10) NULL DEFAULT NULL,
  `local_declared_sale_roe` DOUBLE NULL DEFAULT NULL,
  `local_gross_sale_roe` DOUBLE NULL DEFAULT NULL,
  `local_net_sale_roe` DOUBLE NULL DEFAULT NULL,
  `local_rate_amount_roe` DOUBLE NULL DEFAULT NULL,
  `local_total_cost_amount` DOUBLE NULL DEFAULT NULL,
  `local_total_declared_sale` DOUBLE NULL DEFAULT NULL,
  `local_total_gross_sale` DOUBLE NULL DEFAULT NULL,
  `local_total_net_sale` DOUBLE NULL DEFAULT NULL,
  `local_total_rate_amount` DOUBLE NULL DEFAULT NULL,
  `location_code` VARCHAR(100) NULL DEFAULT NULL,
  `net_sale_amount` DOUBLE NULL DEFAULT NULL,
  `net_sale_currency_code` VARCHAR(10) NULL DEFAULT NULL,
  `net_sale_minimum` DOUBLE NULL DEFAULT NULL,
  `note` LONGTEXT NULL DEFAULT NULL,
  `no_of_unit` VARCHAR(255) NULL DEFAULT NULL,
  `ppcc` VARCHAR(255) NULL DEFAULT NULL,
  `quotation_id` BIGINT(20) NULL DEFAULT NULL,
  `rate_amount` DOUBLE NULL DEFAULT NULL,
  `rate_currency_code` VARCHAR(10) NULL DEFAULT NULL,
  `rate_minimum` DOUBLE NULL DEFAULT NULL,
  `service_uid` VARCHAR(255) NULL DEFAULT NULL,
  `shipment_uid` VARCHAR(255) NULL DEFAULT NULL,
  `create_date` DATETIME(6) NULL DEFAULT NULL,
  `create_user` VARCHAR(30) NULL DEFAULT NULL,
  `last_updated_date` DATETIME(6) NULL DEFAULT NULL,
  `last_updated_user` VARCHAR(30) NULL DEFAULT NULL,
  `total_cost_amount` DOUBLE NULL DEFAULT NULL,
  `total_declared_sale` DOUBLE NULL DEFAULT NULL,
  `total_gross_sale` DOUBLE NULL DEFAULT NULL,
  `total_net_sale` DOUBLE NULL DEFAULT NULL,
  `total_rate_amount` DOUBLE NULL DEFAULT NULL,
  `unit_code` VARCHAR(255) NULL DEFAULT NULL,
  `vendor_code` VARCHAR(255) NULL DEFAULT NULL,
  `charge_id` BIGINT(20) NULL DEFAULT NULL,
  `company_id` BIGINT(20) NULL DEFAULT NULL,
  `cost_currency_id` BIGINT(20) NULL DEFAULT NULL,
  `country_id` BIGINT(20) NULL DEFAULT NULL,
  `declared_sale_currency_id` BIGINT(20) NULL DEFAULT NULL,
  `gross_sale_currency_id` BIGINT(20) NULL DEFAULT NULL,
  `local_currency_id` BIGINT(20) NULL DEFAULT NULL,
  `location_id` BIGINT(20) NULL DEFAULT NULL,
  `net_sale_currency_id` BIGINT(20) NULL DEFAULT NULL,
  `rate_currency_id` BIGINT(20) NULL DEFAULT NULL,
  `service_id` BIGINT(20) NULL DEFAULT NULL,
  `unit_id` BIGINT(20) NULL DEFAULT NULL,
  `vendor_id` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`, `rev`),
  INDEX `FKprcoydlexbcb3nqy1wxhrw0oo` (`rev` ASC),
  CONSTRAINT `FKprcoydlexbcb3nqy1wxhrw0oo`
    FOREIGN KEY (`rev`)
    REFERENCES `efsdev`.`efs_newage_audit_info` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_shipment_ser_ref`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_shipment_ser_ref` (
  `id` BIGINT(20) NOT NULL,
  `reference_notes` VARCHAR(4000) NULL DEFAULT NULL,
  `reference_number` VARCHAR(30) NOT NULL,
  `reference_type_code` VARCHAR(100) NULL DEFAULT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  `reference_type_id` BIGINT(20) NOT NULL,
  `service_id` BIGINT(20) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `FK_SHIPREF_REFTYPE_ID` (`reference_type_id` ASC),
  INDEX `FK_REFERENCE_SERVIDETID` (`service_id` ASC),
  CONSTRAINT `FK_REFERENCE_SERVIDETID`
    FOREIGN KEY (`service_id`)
    REFERENCES `efsdev`.`efs_service` (`id`),
  CONSTRAINT `FK_SHIPREF_REFTYPE_ID`
    FOREIGN KEY (`reference_type_id`)
    REFERENCES `efsdev`.`efs_referencetype_master` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_shipment_service_event`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_shipment_service_event` (
  `id` BIGINT(20) NOT NULL,
  `assigned_code` VARCHAR(100) NULL DEFAULT NULL,
  `event_code` VARCHAR(100) NULL DEFAULT NULL,
  `event_date` DATETIME(6) NOT NULL,
  `follow_up_date` DATETIME(6) NULL DEFAULT NULL,
  `follow_up_required` VARCHAR(255) NOT NULL,
  `is_completed` VARCHAR(255) NOT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  `assigned_id` BIGINT(20) NULL DEFAULT NULL,
  `event_master_id` BIGINT(20) NOT NULL,
  `service_id` BIGINT(20) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `FK_EVENT_ASSIGNEDID` (`assigned_id` ASC),
  INDEX `FK_EVENT_EVENTID` (`event_master_id` ASC),
  INDEX `FK_EVENT_SERVIDETID` (`service_id` ASC),
  CONSTRAINT `FK_EVENT_ASSIGNEDID`
    FOREIGN KEY (`assigned_id`)
    REFERENCES `efsdev`.`efs_employee_master` (`id`),
  CONSTRAINT `FK_EVENT_EVENTID`
    FOREIGN KEY (`event_master_id`)
    REFERENCES `efsdev`.`efs_event_master` (`id`),
  CONSTRAINT `FK_EVENT_SERVIDETID`
    FOREIGN KEY (`service_id`)
    REFERENCES `efsdev`.`efs_service` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_trigger_type_master`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_trigger_type_master` (
  `id` BIGINT(20) NOT NULL,
  `status` VARCHAR(10) NOT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `trigger_type_code` VARCHAR(10) NOT NULL,
  `trigger_type_name` VARCHAR(100) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `UK_TRIGGERTYPE_TRIGTYPECODE` (`trigger_type_code` ASC),
  UNIQUE INDEX `UK_TRIGGERTYPE_TRIGTYPENAME` (`trigger_type_name` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_trigger_master`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_trigger_master` (
  `id` BIGINT(20) NOT NULL,
  `air_days` INT(11) NULL DEFAULT NULL,
  `land_days` INT(11) NULL DEFAULT NULL,
  `mail_subject` VARCHAR(100) NULL DEFAULT NULL,
  `note` VARCHAR(4000) NULL DEFAULT NULL,
  `sea_days` INT(11) NULL DEFAULT NULL,
  `status` VARCHAR(10) NOT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `trigger_code` VARCHAR(10) NOT NULL,
  `trigger_name` VARCHAR(100) NOT NULL,
  `use_for_finance` VARCHAR(10) NULL DEFAULT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  `trigger_type_id` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `UK_TRIGGER_TRIGGERCODE` (`trigger_code` ASC),
  UNIQUE INDEX `UK_TRIGGER_TRIGGERNAME` (`trigger_name` ASC),
  INDEX `FK_TRIGGER_TRIGGERTYPEID` (`trigger_type_id` ASC),
  CONSTRAINT `FK_TRIGGER_TRIGGERTYPEID`
    FOREIGN KEY (`trigger_type_id`)
    REFERENCES `efsdev`.`efs_trigger_type_master` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_shipment_service_trigger`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_shipment_service_trigger` (
  `id` BIGINT(20) NOT NULL,
  `completed` VARCHAR(255) NOT NULL,
  `trigger_date` DATETIME(6) NOT NULL,
  `employee_master_code` VARCHAR(100) NULL DEFAULT NULL,
  `follow_up_date` DATETIME(6) NULL DEFAULT NULL,
  `follow_up_required` VARCHAR(255) NOT NULL,
  `note` LONGTEXT NOT NULL,
  `protect` VARCHAR(255) NOT NULL,
  `service_uid` VARCHAR(255) NULL DEFAULT NULL,
  `shipment_uid` VARCHAR(255) NULL DEFAULT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `trigger_master_code` VARCHAR(100) NULL DEFAULT NULL,
  `trigger_type_master_code` VARCHAR(100) NULL DEFAULT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  `employee_master_id` BIGINT(20) NULL DEFAULT NULL,
  `service_id` BIGINT(20) NOT NULL,
  `trigger_master_id` BIGINT(20) NOT NULL,
  `trigger_type_master_id` BIGINT(20) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `FK_SST_EMP_MSTR_ID` (`employee_master_id` ASC),
  INDEX `FK_SST_SERVIDETID` (`service_id` ASC),
  INDEX `FK_SST_TRGR_ID` (`trigger_master_id` ASC),
  INDEX `FK_SST_TRGR_TYP_ID` (`trigger_type_master_id` ASC),
  CONSTRAINT `FK_SST_EMP_MSTR_ID`
    FOREIGN KEY (`employee_master_id`)
    REFERENCES `efsdev`.`efs_employee_master` (`id`),
  CONSTRAINT `FK_SST_SERVIDETID`
    FOREIGN KEY (`service_id`)
    REFERENCES `efsdev`.`efs_service` (`id`),
  CONSTRAINT `FK_SST_TRGR_ID`
    FOREIGN KEY (`trigger_master_id`)
    REFERENCES `efsdev`.`efs_trigger_master` (`id`),
  CONSTRAINT `FK_SST_TRGR_TYP_ID`
    FOREIGN KEY (`trigger_type_master_id`)
    REFERENCES `efsdev`.`efs_trigger_type_master` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_srvc_tax_chrg_grp_mstr`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_srvc_tax_chrg_grp_mstr` (
  `id` BIGINT(20) NOT NULL,
  `note` VARCHAR(2000) NULL DEFAULT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  `category_master_id` BIGINT(20) NULL DEFAULT NULL,
  `charge_master_id` BIGINT(20) NULL DEFAULT NULL,
  `service_master_id` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `UK_SVC_CHRG_CAT` (`service_master_id` ASC, `charge_master_id` ASC, `category_master_id` ASC),
  INDEX `FK_STCG_CATG` (`category_master_id` ASC),
  INDEX `FK_STCG_CHRG` (`charge_master_id` ASC),
  CONSTRAINT `FK_STCG_CATG`
    FOREIGN KEY (`category_master_id`)
    REFERENCES `efsdev`.`efs_category_master` (`id`),
  CONSTRAINT `FK_STCG_CHRG`
    FOREIGN KEY (`charge_master_id`)
    REFERENCES `efsdev`.`efs_charge_master` (`id`),
  CONSTRAINT `FK_STCG_SRVC`
    FOREIGN KEY (`service_master_id`)
    REFERENCES `efsdev`.`efs_service_master` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_stock_generation`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_stock_generation` (
  `id` BIGINT(20) NOT NULL,
  `awb_no` BIGINT(20) NOT NULL,
  `carrier_code` VARCHAR(100) NOT NULL,
  `carrier_ref_no` VARCHAR(255) NULL DEFAULT NULL,
  `check_digit` BIGINT(20) NOT NULL,
  `consol_uid` VARCHAR(255) NULL DEFAULT NULL,
  `ending_no` BIGINT(20) NOT NULL,
  `last_used_mawb_date` DATETIME(6) NULL DEFAULT NULL,
  `mawb_no` VARCHAR(20) NOT NULL,
  `port_code` VARCHAR(10) NOT NULL,
  `received_on` DATETIME(6) NOT NULL,
  `received_on_check` DATETIME(6) NULL DEFAULT NULL,
  `remind_no` BIGINT(20) NULL DEFAULT NULL,
  `service_code` VARCHAR(255) NULL DEFAULT NULL,
  `service_uid` VARCHAR(255) NULL DEFAULT NULL,
  `shipment_uid` VARCHAR(255) NULL DEFAULT NULL,
  `starting_no` BIGINT(20) NOT NULL,
  `stock_status` VARCHAR(20) NOT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  `carrier_id` BIGINT(20) NOT NULL,
  `consol_id` BIGINT(20) NULL DEFAULT NULL,
  `por_id` BIGINT(20) NOT NULL,
  `shipment_id` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `UK_STOCK_MAWB_UNIQUE` (`awb_no` ASC, `carrier_id` ASC, `check_digit` ASC),
  INDEX `FK_STOCK_CARRIERID` (`carrier_id` ASC),
  INDEX `FK_STOCK_CONSOLID` (`consol_id` ASC),
  INDEX `FK_STOCK_POR` (`por_id` ASC),
  INDEX `FK_STOCK_SHIPMENTID` (`shipment_id` ASC),
  CONSTRAINT `FK_STOCK_CARRIERID`
    FOREIGN KEY (`carrier_id`)
    REFERENCES `efsdev`.`efs_carrier_master` (`id`),
  CONSTRAINT `FK_STOCK_CONSOLID`
    FOREIGN KEY (`consol_id`)
    REFERENCES `efsdev`.`efs_consol` (`id`),
  CONSTRAINT `FK_STOCK_POR`
    FOREIGN KEY (`por_id`)
    REFERENCES `efsdev`.`efs_port_master` (`id`),
  CONSTRAINT `FK_STOCK_SHIPMENTID`
    FOREIGN KEY (`shipment_id`)
    REFERENCES `efsdev`.`efs_shipment` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_sub_job_sequence`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_sub_job_sequence` (
  `id` BIGINT(20) NOT NULL,
  `consol_uid` VARCHAR(255) NOT NULL,
  `sequence` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `UK_SUBJOBSEQ_CONSOLUID` (`consol_uid` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_surcharge_mapping`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_surcharge_mapping` (
  `id` BIGINT(20) NOT NULL,
  `charge_code` VARCHAR(255) NULL DEFAULT NULL,
  `surcharge_type` VARCHAR(60) NOT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  `charge_id` BIGINT(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `UK_SURCHARGE_CHARGEID` (`charge_id` ASC),
  UNIQUE INDEX `UK_SURCHARGE_SURCHARGE` (`surcharge_type` ASC),
  CONSTRAINT `FK_SURCHARGE_CHARGEID`
    FOREIGN KEY (`charge_id`)
    REFERENCES `efsdev`.`efs_charge_master` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_time_zone_master`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_time_zone_master` (
  `id` BIGINT(20) NOT NULL,
  `actual_timezone` VARCHAR(100) NOT NULL,
  `display_timezone` VARCHAR(100) NOT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `UK_TIMEZONE_DISPLAY` (`display_timezone` ASC),
  UNIQUE INDEX `UK_TIMEZONE_ACTUAL` (`actual_timezone` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_user_company`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_user_company` (
  `id` BIGINT(20) NOT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  `default_company` VARCHAR(20) NOT NULL,
  `company_id` BIGINT(20) NULL DEFAULT NULL,
  `employee_id` BIGINT(20) NULL DEFAULT NULL,
  `user_id` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `UK_USERCOM_USERCOMID` (`company_id` ASC, `user_id` ASC),
  INDEX `FK_USERCOM_EMPLOYEEID` (`employee_id` ASC),
  INDEX `FK_USERCOM_USERID` (`user_id` ASC),
  CONSTRAINT `FK_USERCOM_EMPLOYEEID`
    FOREIGN KEY (`employee_id`)
    REFERENCES `efsdev`.`efs_employee_master` (`id`),
  CONSTRAINT `FK_USERCOM_LOCATIONID`
    FOREIGN KEY (`company_id`)
    REFERENCES `efsdev`.`efs_company_master` (`id`),
  CONSTRAINT `FK_USERCOM_USERID`
    FOREIGN KEY (`user_id`)
    REFERENCES `efsdev`.`efs_user_profile` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_user_company_location`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_user_company_location` (
  `id` BIGINT(20) NOT NULL,
  `note` LONGTEXT NULL DEFAULT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  `default_location` VARCHAR(20) NOT NULL,
  `location_id` BIGINT(20) NULL DEFAULT NULL,
  `user_company_id` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `UK_USERCOMLOC_USERCOMLOCID` (`location_id` ASC, `user_company_id` ASC),
  INDEX `FK_USERCOMLOC_USERID` (`user_company_id` ASC),
  CONSTRAINT `FK_USERCOMLOC_LOCATIONID`
    FOREIGN KEY (`location_id`)
    REFERENCES `efsdev`.`efs_location_master` (`id`),
  CONSTRAINT `FK_USERCOMLOC_USERID`
    FOREIGN KEY (`user_company_id`)
    REFERENCES `efsdev`.`efs_user_company` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_user_has_role`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_user_has_role` (
  `user_id` BIGINT(20) NOT NULL,
  `role_id` BIGINT(20) NOT NULL,
  UNIQUE INDEX `UK_USER_ROLE` (`user_id` ASC, `role_id` ASC),
  INDEX `FKc6grvv4b17gds9sbtflrq7klc` (`role_id` ASC),
  CONSTRAINT `FK1k8gts71igsnwfrr3aylolxkj`
    FOREIGN KEY (`user_id`)
    REFERENCES `efsdev`.`efs_user_profile` (`id`),
  CONSTRAINT `FKc6grvv4b17gds9sbtflrq7klc`
    FOREIGN KEY (`role_id`)
    REFERENCES `efsdev`.`efs_role` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_work_flow_group`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_work_flow_group` (
  `id` BIGINT(20) NOT NULL,
  `name` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `UK_WRKFLOW_NAME` (`name` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_user_work_flow`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_user_work_flow` (
  `user_id` BIGINT(20) NOT NULL,
  `work_flow_id` BIGINT(20) NOT NULL,
  UNIQUE INDEX `UK_USER_WORK_FLOW` (`user_id` ASC, `work_flow_id` ASC),
  INDEX `FKb716lmnundxub6mw1n9c54hdy` (`work_flow_id` ASC),
  CONSTRAINT `FKb716lmnundxub6mw1n9c54hdy`
    FOREIGN KEY (`work_flow_id`)
    REFERENCES `efsdev`.`efs_work_flow_group` (`id`),
  CONSTRAINT `FKn4mdnye6n8fgsj19up0501aum`
    FOREIGN KEY (`user_id`)
    REFERENCES `efsdev`.`efs_user_profile` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_value_added_charge`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_value_added_charge` (
  `id` BIGINT(20) NOT NULL,
  `charge_code` VARCHAR(100) NULL DEFAULT NULL,
  `create_date` DATETIME(6) NOT NULL,
  `create_user` VARCHAR(30) NOT NULL,
  `last_updated_date` DATETIME(6) NOT NULL,
  `last_updated_user` VARCHAR(30) NOT NULL,
  `version_lock` BIGINT(20) NULL DEFAULT NULL,
  `charge_id` BIGINT(20) NULL DEFAULT NULL,
  `value_added_id` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `FK_VASCHAR_CHARGEID` (`charge_id` ASC),
  INDEX `FK_VASCHAR_VASID` (`value_added_id` ASC),
  CONSTRAINT `FK_VASCHAR_CHARGEID`
    FOREIGN KEY (`charge_id`)
    REFERENCES `efsdev`.`efs_charge_master` (`id`),
  CONSTRAINT `FK_VASCHAR_VASID`
    FOREIGN KEY (`value_added_id`)
    REFERENCES `efsdev`.`efs_value_added_service` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_win_web_response`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_win_web_response` (
  `id` BIGINT(20) NOT NULL,
  `agent_id` INT(11) NULL DEFAULT NULL,
  `agent_name` VARCHAR(255) NULL DEFAULT NULL,
  `awb_id` BIGINT(20) NULL DEFAULT NULL,
  `awb_number` VARCHAR(255) NULL DEFAULT NULL,
  `consol_id` VARCHAR(255) NULL DEFAULT NULL,
  `contact_id` INT(11) NULL DEFAULT NULL,
  `contact_name` VARCHAR(255) NULL DEFAULT NULL,
  `date_time` VARCHAR(255) NULL DEFAULT NULL,
  `location_id` BIGINT(20) NULL DEFAULT NULL,
  `mawb_id` BIGINT(20) NULL DEFAULT NULL,
  `transaction_id` INT(11) NULL DEFAULT NULL,
  `win_status` VARCHAR(255) NULL DEFAULT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`efs_win_response_remarks`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`efs_win_response_remarks` (
  `id` BIGINT(20) NOT NULL,
  `error_code` VARCHAR(255) NULL DEFAULT NULL,
  `error_message` VARCHAR(4000) NULL DEFAULT NULL,
  `identifier` VARCHAR(255) NULL DEFAULT NULL,
  `reference_number` VARCHAR(255) NULL DEFAULT NULL,
  `win_web_id` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `WIN_WEB_RESNE_ID` (`win_web_id` ASC),
  CONSTRAINT `WIN_WEB_RESNE_ID`
    FOREIGN KEY (`win_web_id`)
    REFERENCES `efsdev`.`efs_win_web_response` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`hibernate_sequence`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`hibernate_sequence` (
  `next_val` BIGINT(20) NULL DEFAULT NULL)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `efsdev`.`hibernate_sequences`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `efsdev`.`hibernate_sequences` (
  `sequence_name` VARCHAR(255) NOT NULL,
  `next_val` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`sequence_name`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

-- ------------------------------------------------------
-- Insert/s
-- ------------------------------------------------------
INSERT INTO efs_common_language_master
(id, language_code, language_name, status)
VALUES
('1', 'EN', 'English', 'Yes');
