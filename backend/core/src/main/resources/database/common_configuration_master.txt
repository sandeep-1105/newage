INSERT INTO EFS_COMMON_CONFIG_MASTER(ID, CODE, DESCRIPTION, "VALUE") VALUES(1, 'file.size.party.master.bulk.upload', 'Upload File Size from Location Setup', '5');

INSERT INTO EFS_COMMON_CONFIG_MASTER(ID, CODE, DESCRIPTION, "VALUE") VALUES(2, 'imperial.or.metric', 'Imperial will be choosen based on the mentioned country code', 'US, UK');