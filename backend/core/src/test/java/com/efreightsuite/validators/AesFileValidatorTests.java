package com.efreightsuite.validators;

import java.util.Date;

import com.efreightsuite.enumeration.AesAction;
import com.efreightsuite.enumeration.FillingType;
import com.efreightsuite.enumeration.LovStatus;
import com.efreightsuite.enumeration.TransportMode;
import com.efreightsuite.enumeration.YesNo;
import com.efreightsuite.model.AesFile;
import com.efreightsuite.model.AesTransportMode;
import com.efreightsuite.model.AesUsForeignPortMaster;
import com.efreightsuite.model.AesUsPortMaster;
import com.efreightsuite.model.AesUsStateCodeMaster;
import com.efreightsuite.model.CarrierMaster;
import com.efreightsuite.model.CountryMaster;
import com.efreightsuite.model.InBondTypeMaster;
import org.junit.Test;

public class AesFileValidatorTests {

    @Test
    public void should_validate_successfully_when_data_is_present() throws Exception {
        AesFileValidator aesFileValidator = new AesFileValidator();
        AesFile aesFile = new AesFile();
        aesFile.setServiceUid("suid");
        aesFile.setShipmentUid("shid");
        aesFile.setHawbNo("hawb");
        aesFile.setAesFilingType(FillingType.POST_SHIPMENT);
        AesTransportMode aesTransportMode = new AesTransportMode();
        aesTransportMode.setStatus(LovStatus.Active);
        aesFile.setAesTransportMode(aesTransportMode);
        aesFile.setAesAction(AesAction.Add);
        aesFile.setEtd(new Date());
        AesUsStateCodeMaster originState = new AesUsStateCodeMaster();
        originState.setId(1L);
        originState.setName("Origin");
        originState.setStatus(LovStatus.Active);
        originState.setStateCode("o1");
        aesFile.setOriginState(originState);
        AesUsPortMaster pol = new AesUsPortMaster();
        pol.setId(1L);
        pol.setPortName("p1");
        pol.setStatus(LovStatus.Active);
        aesFile.setPol(pol);

        CountryMaster c1 = new CountryMaster(1L, "c1", "country 1");
        c1.setStatus(LovStatus.Active);
        aesFile.setDestinationCountry(c1);

        AesUsForeignPortMaster pod = new AesUsForeignPortMaster();
        pod.setId(1L);
        pod.setPortCode("pc1");
        pod.setPortName("p1");
        pod.setPortType(TransportMode.Air);
        pod.setStatus(LovStatus.Active);
        aesFile.setPod(pod);

        CarrierMaster carrier = new CarrierMaster();
        carrier.setId(1L);
        carrier.setCarrierName("C1");
        carrier.setStatus(LovStatus.Active);
        carrier.setTransportMode(TransportMode.Air);
        carrier.setIataCode("1111");
        aesFile.setCarrier(carrier);

        aesFile.setIataOrScacCode("aaaaa");
        aesFile.setRoutedTransaction(YesNo.Yes);
        aesFile.setUsspiUltimateConsignee(YesNo.Yes);
        aesFile.setHazardousCargo(YesNo.Yes);
        InBondTypeMaster inbondType = new InBondTypeMaster();
        inbondType.setStatus(LovStatus.Active);
        aesFile.setInbondType(inbondType);

        aesFileValidator.validateInfo(aesFile);
    }
}
