package com.efreightsuite.validators;

import com.efreightsuite.common.component.CacheRepository;
import com.efreightsuite.enumeration.EmploymentStatus;
import com.efreightsuite.enumeration.LovStatus;
import com.efreightsuite.enumeration.YesNo;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.model.*;
import com.efreightsuite.repository.*;
import com.efreightsuite.validation.QuotationAttachmentValidator;
import com.efreightsuite.validation.QuotationDetailValidator;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Date;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.Matchers.*;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class QuotationValidatorTests {

    @Mock
    private QuotationRepository quotationRepository;

    @Mock
    private CacheRepository cacheRepository;

    @Mock
    private
    QuotationDetailValidator quotationDetailValidator;

    @Mock
    private
    QuotationAttachmentValidator quotationAttachmentValidator;

    @Mock
    private
    PartyMasterRepository partyMasterRepository;

    @Mock
    private
    CompanyMasterRepository companyMasterRepository;

    @Mock
    private
    CountryMasterRepository countryMasterRepository;

    @Mock
    private
    LocationMasterRepository locationMasterRepository;

    @Mock
    private
    EmployeeMasterRepository employeeMasterRepository;

    @Mock
    private
    CurrencyMasterRepository currencyMasterRepository;

    @Mock
    private
    EmployeeMasterRepository salesManRepository;

    @InjectMocks
    private QuotationValidator validator;

    @Before
    public void setUp(){
        //when(companyMasterRepository.findById(Mockito.anyLong())).thenReturn(new CompanyMaster(1L, "CC1", "Company 1"));
        //when(locationMasterRepository.findById(Mockito.anyLong())).thenReturn(new LocationMaster(1L));
        //when(countryMasterRepository.findById(Mockito.anyLong())).thenReturn(new CountryMaster(1L,"IN","India"));
    }

    @Test
    public void should_throw_exception_when_trying_to_recreate_quotation_from_enquiry() throws Exception {
        Quotation quotation = new Quotation();
        quotation.setEnquiryNo("ENQ001");
        when(quotationRepository.findByEnquiryNo("ENQ001")).thenReturn(new Quotation());

        assertThatThrownBy(() -> validator.validate(quotation))
                .isInstanceOf(RestException.class)
                .hasMessage(ErrorCode.QUOTATION_ALREADY_CREATED_FROM_ENQUIRY);
    }

    @Test
    public void should_throw_exception_when_company_master_is_not_present() throws Exception {
        Quotation quotation = quotationWithoutEnquiry();
        assertThatThrownBy(() -> validator.validate(quotation))
                .isInstanceOf(RestException.class)
                .hasMessage(ErrorCode.QUOTATION_COMPANY);

        CompanyMaster companyMaster = new CompanyMaster();
        quotation.setCompanyMaster(companyMaster);
        assertThatThrownBy(() -> validator.validate(quotation))
                .isInstanceOf(RestException.class)
                .hasMessage(ErrorCode.QUOTATION_COMPANY);

        companyMaster = new CompanyMaster(1L);
        quotation.setCompanyMaster(companyMaster);
        when(companyMasterRepository.findById(Mockito.anyLong())).thenReturn(companyMaster);

        assertThatThrownBy(() -> validator.validate(quotation))
                .isInstanceOf(RestException.class)
                .hasMessage(ErrorCode.QUOTATION_COMPANY);
    }

    @Test
    public void should_throw_exception_when_location_master_is_not_present() throws Exception {
        Quotation quotation = quotationWithCompany();
        when(companyMasterRepository.findById(Mockito.anyLong())).thenReturn(quotation.getCompanyMaster());
        when(quotationRepository.findByEnquiryNo("ENQ001")).thenReturn(null);

        assertThatThrownBy(() -> validator.validate(quotation))
                .isInstanceOf(RestException.class)
                .hasMessage(ErrorCode.QUOTATION_LOCATION);

        LocationMaster locationMaster = new LocationMaster(1L);
        quotation.setLocationMaster(locationMaster);
        when(locationMasterRepository.findById(Mockito.anyLong())).thenReturn(locationMaster);
        assertThatThrownBy(() -> validator.validate(quotation))
                .isInstanceOf(RestException.class)
                .hasMessage(ErrorCode.QUOTATION_LOCATION);

    }

    @Test
    public void should_throw_exception_when_country_master_is_not_present() throws Exception {
        Quotation quotation = quotationWithLocation();
        when(locationMasterRepository.findById(Mockito.anyLong())).thenReturn(quotation.getLocationMaster());
        when(companyMasterRepository.findById(Mockito.anyLong())).thenReturn(new CompanyMaster(1L, "CC1", "Company 1"));

        assertThatThrownBy(() -> validator.validate(quotation))
                .isInstanceOf(RestException.class)
                .hasMessage(ErrorCode.QUOTATION_COUNTRY);

        CountryMaster countryMaster = new CountryMaster(1L);
        quotation.setCountryMaster(countryMaster);
        when(countryMasterRepository.findById(Mockito.anyLong())).thenReturn(countryMaster);
        assertThatThrownBy(() -> validator.validate(quotation))
                .isInstanceOf(RestException.class)
                .hasMessage(ErrorCode.QUOTATION_COUNTRY);
    }

    @Test
    public void should_throw_exception_when_customer_not_in_db() throws Exception {
        Quotation quotation = quotationWithCountry();
        quotation.setCustomer(new PartyMaster(1L));
        when(companyMasterRepository.findById(Mockito.anyLong())).thenReturn(new CompanyMaster(1L, "CC1", "Company 1"));
        when(locationMasterRepository.findById(Mockito.anyLong())).thenReturn(new LocationMaster(1L,"IRN"));
        when(countryMasterRepository.findById(Mockito.anyLong())).thenReturn(new CountryMaster(1L,"IN","India"));

        when(cacheRepository.get(anyString(), anyString(), anyLong(), any())).thenReturn(null);

        assertThatThrownBy(() -> validator.validate(quotation))
                .isInstanceOf(RestException.class)
                .hasMessage(ErrorCode.QUOTATION_CUSTOMER_NOT_NULL);
    }

    @Test
    public void should_throw_exception_when_customer_status_is_blocked() throws Exception {
        Quotation quotation = quotationWithCountry();
        PartyMaster partyMaster = new PartyMaster(1L);
        partyMaster.setStatus(LovStatus.Block);
        quotation.setCustomer(partyMaster);

        when(companyMasterRepository.findById(Mockito.anyLong())).thenReturn(new CompanyMaster(1L, "CC1", "Company 1"));
        when(locationMasterRepository.findById(Mockito.anyLong())).thenReturn(new LocationMaster(1L,"IRN"));
        when(countryMasterRepository.findById(Mockito.anyLong())).thenReturn(new CountryMaster(1L,"IN","India"));
        when(partyMasterRepository.findById(Mockito.anyLong())).thenReturn(partyMaster);


        when(cacheRepository.get(anyString(), anyString(), anyLong(), any())).thenReturn(partyMaster);

        assertThatThrownBy(() -> validator.validate(quotation))
                .isInstanceOf(RestException.class)
                .hasMessage(ErrorCode.QUOTATION_CUSTOMER_BLOCKED);
    }

    @Test
    public void should_throw_exception_when_customer_status_is_hidden() throws Exception {
        Quotation quotation = quotationWithCountry();
        PartyMaster partyMaster = new PartyMaster(1L);
        partyMaster.setStatus(LovStatus.Hide);
        quotation.setCustomer(partyMaster);
        when(companyMasterRepository.findById(Mockito.anyLong())).thenReturn(new CompanyMaster(1L, "CC1", "Company 1"));
        when(locationMasterRepository.findById(Mockito.anyLong())).thenReturn(new LocationMaster(1L,"IRN"));
        when(countryMasterRepository.findById(Mockito.anyLong())).thenReturn(new CountryMaster(1L,"IN","India"));
        when(partyMasterRepository.findById(Mockito.anyLong())).thenReturn(partyMaster);

        when(cacheRepository.get(anyString(), anyString(), anyLong(), any())).thenReturn(partyMaster);

        assertThatThrownBy(() -> validator.validate(quotation))
                .isInstanceOf(RestException.class)
                .hasMessage(ErrorCode.QUOTATION_CUSTOMER_HIDE);
    }

    @Test
    public void should_throw_exception_when_customer_status_is_defaulter() throws Exception {
        Quotation quotation = quotationWithCountry();
        PartyMaster partyMaster = new PartyMaster(1L);
        partyMaster.setStatus(LovStatus.Active);
        partyMaster.setIsDefaulter(YesNo.Yes);
        quotation.setCustomer(partyMaster);
        when(companyMasterRepository.findById(Mockito.anyLong())).thenReturn(new CompanyMaster(1L, "CC1", "Company 1"));
        when(locationMasterRepository.findById(Mockito.anyLong())).thenReturn(new LocationMaster(1L,"IRN"));
        when(countryMasterRepository.findById(Mockito.anyLong())).thenReturn(new CountryMaster(1L,"IN","India"));
        when(partyMasterRepository.findById(Mockito.anyLong())).thenReturn(partyMaster);


        when(cacheRepository.get(anyString(), anyString(), anyLong(), any())).thenReturn(partyMaster);

        assertThatThrownBy(() -> validator.validate(quotation))
                .isInstanceOf(RestException.class)
                .hasMessage(ErrorCode.QUOTATION_CUSTOMER_DEFAULTER);
    }

    @Test
    public void should_throw_exception_when_customer_address1_is_not_present() throws Exception {
        Quotation quotation = quotationWithCustomer();
        when(companyMasterRepository.findById(Mockito.anyLong())).thenReturn(new CompanyMaster(1L, "CC1", "Company 1"));
        when(locationMasterRepository.findById(Mockito.anyLong())).thenReturn(new LocationMaster(1L,"IRN"));
        when(countryMasterRepository.findById(Mockito.anyLong())).thenReturn(new CountryMaster(1L,"IN","India"));
        when(partyMasterRepository.findById(Mockito.anyLong())).thenReturn(quotation.getCustomer());


        when(cacheRepository.get(anyString(), anyString(), anyLong(), any())).thenReturn(new PartyMaster(1L));
        assertThatThrownBy(() -> validator.validate(quotation))
                .isInstanceOf(RestException.class)
                .hasMessage(ErrorCode.QUOTATION_CUSTOMER_ADDRESS_LINE1_REQUIRED);

        quotation.setCustomerAddress(new AddressMaster());

        assertThatThrownBy(() -> validator.validate(quotation))
                .isInstanceOf(RestException.class)
                .hasMessage(ErrorCode.QUOTATION_CUSTOMER_ADDRESS_LINE1_REQUIRED);
    }

    @Test
    public void should_throw_exception_when_customer_city_is_not_present() throws Exception {
        Quotation quotation = quotationWithCustomer();
        when(companyMasterRepository.findById(Mockito.anyLong())).thenReturn(new CompanyMaster(1L, "CC1", "Company 1"));
        when(locationMasterRepository.findById(Mockito.anyLong())).thenReturn(new LocationMaster(1L,"IRN"));
        when(countryMasterRepository.findById(Mockito.anyLong())).thenReturn(new CountryMaster(1L,"IN","India"));
        when(partyMasterRepository.findById(Mockito.anyLong())).thenReturn(quotation.getCustomer());


        when(cacheRepository.get(anyString(), anyString(), anyLong(), any())).thenReturn(new PartyMaster(1L));
        AddressMaster customerAddress = new AddressMaster(1L, "a1", "a2", "a3");
        quotation.setCustomerAddress(customerAddress);

        assertThatThrownBy(() -> validator.validate(quotation))
                .isInstanceOf(RestException.class)
                .hasMessage(ErrorCode.QUOTATION_CUSTOMER_CITY_STATE_REQUIRED);

        customerAddress.setCity(new CityMaster(1L, "City1"));
        quotation.setCustomerAddress(customerAddress);

        assertThatThrownBy(() -> validator.validate(quotation))
                .isInstanceOf(RestException.class)
                .hasMessage(ErrorCode.QUOTATION_CUSTOMER_CITY_STATE_REQUIRED);
    }

    @Test
    public void should_throw_exception_when_shipper_is_not_present() throws Exception {
        Quotation quotation = quotationWithCustomerAndAddress();
        PartyMaster shipper = new PartyMaster(1L);
        quotation.setShipper(shipper);
        when(companyMasterRepository.findById(Mockito.anyLong())).thenReturn(new CompanyMaster(1L, "CC1", "Company 1"));
        when(locationMasterRepository.findById(Mockito.anyLong())).thenReturn(new LocationMaster(1L,"IRN"));
        when(countryMasterRepository.findById(Mockito.anyLong())).thenReturn(new CountryMaster(1L,"IN","India"));
        when(partyMasterRepository.findById(Mockito.anyLong())).thenReturn(shipper);


        when(cacheRepository.get(anyString(), anyString(), anyLong(), any())).thenReturn(quotation.getCustomer(), new PartyMaster());

        assertThatThrownBy(() -> validator.validate(quotation))
                .isInstanceOf(RestException.class)
                .hasMessage(ErrorCode.QUOTATION_SHIPPER_NOT_NULL);
    }

    @Test
    public void should_throw_exception_when_valid_from_date_is_not_present() throws Exception {
        Quotation quotation = quotationWthShipper();
        when(companyMasterRepository.findById(Mockito.anyLong())).thenReturn(new CompanyMaster(1L, "CC1", "Company 1"));
        when(locationMasterRepository.findById(Mockito.anyLong())).thenReturn(new LocationMaster(1L,"IRN"));
        when(countryMasterRepository.findById(Mockito.anyLong())).thenReturn(new CountryMaster(1L,"IN","India"));
        when(partyMasterRepository.findById(Mockito.anyLong())).thenReturn(quotation.getShipper());

        assertThatThrownBy(() -> validator.validate(quotation))
                .isInstanceOf(RestException.class)
                .hasMessage(ErrorCode.QUOTATION_VALID_FROM_NOT_NULL);
    }

    @Test
    public void should_throw_exception_when_valid_to_date_is_not_present() throws Exception {
        Quotation quotation = quotationWthShipper();
        quotation.setValidFrom(new Date());
        when(companyMasterRepository.findById(Mockito.anyLong())).thenReturn(new CompanyMaster(1L, "CC1", "Company 1"));
        when(locationMasterRepository.findById(Mockito.anyLong())).thenReturn(new LocationMaster(1L,"IRN"));
        when(countryMasterRepository.findById(Mockito.anyLong())).thenReturn(new CountryMaster(1L,"IN","India"));
        when(partyMasterRepository.findById(Mockito.anyLong())).thenReturn(quotation.getShipper());
        assertThatThrownBy(() -> validator.validate(quotation))
                .isInstanceOf(RestException.class)
                .hasMessage(ErrorCode.QUOTATION_VALID_TO_NOT_NULL);
    }

    @Test
    public void should_throw_exception_when_loggedon_date_is_not_present() throws Exception {
        Quotation quotation = quotationWthShipper();
        quotation.setValidFrom(new Date());
        quotation.setValidTo(new Date());
        when(companyMasterRepository.findById(Mockito.anyLong())).thenReturn(new CompanyMaster(1L, "CC1", "Company 1"));
        when(locationMasterRepository.findById(Mockito.anyLong())).thenReturn(new LocationMaster(1L,"IRN"));
        when(countryMasterRepository.findById(Mockito.anyLong())).thenReturn(new CountryMaster(1L,"IN","India"));
        when(partyMasterRepository.findById(Mockito.anyLong())).thenReturn(quotation.getShipper());
        assertThatThrownBy(() -> validator.validate(quotation))
                .isInstanceOf(RestException.class)
                .hasMessage(ErrorCode.QUOTATION_LOGGED_BY_DATE_REQUIRED);
    }

    @Test
    public void should_throw_exception_when_logged_by_is_not_present() throws Exception {
        Quotation quotation = quotationWithDates();
        when(companyMasterRepository.findById(Mockito.anyLong())).thenReturn(new CompanyMaster(1L, "CC1", "Company 1"));
        when(locationMasterRepository.findById(Mockito.anyLong())).thenReturn(new LocationMaster(1L,"IRN"));
        when(countryMasterRepository.findById(Mockito.anyLong())).thenReturn(new CountryMaster(1L,"IN","India"));
        when(partyMasterRepository.findById(Mockito.anyLong())).thenReturn(quotation.getShipper());

        assertThatThrownBy(() -> validator.validate(quotation))
                .isInstanceOf(RestException.class)
                .hasMessage(ErrorCode.QUOTATION_LOGGED_BY_USER_REQUIRED);

        EmployeeMaster loggedBy = new EmployeeMaster();
        quotation.setLoggedBy(loggedBy);
        assertThatThrownBy(() -> validator.validate(quotation))
                .isInstanceOf(RestException.class)
                .hasMessage(ErrorCode.QUOTATION_LOGGED_BY_USER_REQUIRED);

        loggedBy.setId(1L);
        loggedBy.setEmployementStatus(EmploymentStatus.TERMINATED);
        quotation.setLoggedBy(loggedBy);
        when(employeeMasterRepository.findById(Mockito.anyLong())).thenReturn(quotation.getLoggedBy());
        assertThatThrownBy(() -> validator.validate(quotation))
                .isInstanceOf(RestException.class)
                .hasMessage(ErrorCode.QUOTATION_LOGGED_BY_USER_TERMINATED);
    }

    @Test
    public void should_throw_exception_when_sales_coordinator_is_not_present() throws Exception {
        Quotation quotation = quotationWithLoggedBy();
        when(companyMasterRepository.findById(Mockito.anyLong())).thenReturn(new CompanyMaster(1L, "CC1", "Company 1"));
        when(locationMasterRepository.findById(Mockito.anyLong())).thenReturn(new LocationMaster(1L,"IRN"));
        when(countryMasterRepository.findById(Mockito.anyLong())).thenReturn(new CountryMaster(1L,"IN","India"));
        when(partyMasterRepository.findById(Mockito.anyLong())).thenReturn(quotation.getShipper());
        when(employeeMasterRepository.findById(Mockito.anyLong())).thenReturn(quotation.getLoggedBy());
        assertThatThrownBy(() -> validator.validate(quotation))
                .isInstanceOf(RestException.class)
                .hasMessage(ErrorCode.QUOTATION_SALES_COORDINATOR_REQUIRED);

    }

    @Test
    public void should_throw_exception_when_salesman_is_not_present() throws Exception {
        Quotation quotation = quotationWithLoggedBy();
        quotation.setSalesCoordinator(new EmployeeMaster(1L));
        EmployeeMaster salesMan = new EmployeeMaster(1L);
        salesMan.setIsSalesman(YesNo.No);
        quotation.setSalesman(salesMan);

        when(companyMasterRepository.findById(Mockito.anyLong())).thenReturn(new CompanyMaster(1L, "CC1", "Company 1"));
        when(locationMasterRepository.findById(Mockito.anyLong())).thenReturn(new LocationMaster(1L,"IRN"));
        when(countryMasterRepository.findById(Mockito.anyLong())).thenReturn(new CountryMaster(1L,"IN","India"));
        when(partyMasterRepository.findById(Mockito.anyLong())).thenReturn(quotation.getShipper());
        when(employeeMasterRepository.findById(Mockito.anyLong())).thenReturn(quotation.getLoggedBy());
        when(salesManRepository.findById(Mockito.anyLong())).thenReturn(salesMan);

        assertThatThrownBy(() -> validator.validate(quotation))
                .isInstanceOf(RestException.class)
                .hasMessage(ErrorCode.QUOTATION_SALESMAN_INVALID);
    }

    @Test
    public void should_validate_successful_when_all_data_is_present() throws Exception {
        Quotation quotation = quotationWithLoggedBy();
        quotation.setSalesCoordinator(new EmployeeMaster(1L));
        EmployeeMaster salesMan = new EmployeeMaster(1L);
        salesMan.setIsSalesman(YesNo.Yes);
        quotation.setSalesman(salesMan);

        when(companyMasterRepository.findById(Mockito.anyLong())).thenReturn(new CompanyMaster(1L, "C1", "Company 1"));
        when(locationMasterRepository.findById(Mockito.anyLong())).thenReturn(new LocationMaster(1L,"L1"));
        when(countryMasterRepository.findById(Mockito.anyLong())).thenReturn(new CountryMaster(1L,"CC1","India"));
        when(partyMasterRepository.findById(Mockito.anyLong())).thenReturn(quotation.getShipper());
        when(employeeMasterRepository.findById(Mockito.anyLong())).thenReturn(quotation.getLoggedBy());
        when(salesManRepository.findById(Mockito.anyLong())).thenReturn(quotation.getSalesman());

        validator.validate(quotation);
        assertThat(quotation.getCompanyCode()).isEqualTo("C1");
        assertThat(quotation.getLocationCode()).isEqualTo("L1");
        assertThat(quotation.getCountryCode()).isEqualTo("CC1");
        assertThat(quotation.getCustomerCode()).isEqualTo("CUS1");
        assertThat(quotation.getShipperCode()).isEqualTo("SP1");
    }

    private Quotation quotationWithLoggedBy() {
        Quotation quotation = quotationWithDates();
        EmployeeMaster loggedBy = new EmployeeMaster();
        loggedBy.setId(1L);
        quotation.setLoggedBy(loggedBy);
        return quotation;
    }

    private Quotation quotationWithDates() {
        Quotation quotation = quotationWthShipper();
        quotation.setValidFrom(new Date());
        quotation.setValidTo(new Date());
        quotation.setLoggedOn(new Date());
        return quotation;
    }

    private Quotation quotationWthShipper() {
        Quotation quotation = quotationWithCustomerAndAddress();
        PartyMaster shipper = new PartyMaster(1L);
        shipper.setPartyCode("SP1");
        AddressMaster shipperAddress = new AddressMaster(1L, "a1", "a2", "a3");
        quotation.setCustomerAddress(shipperAddress);
        shipperAddress.setCity(new CityMaster(1L, "City1"));
        shipperAddress.setState(new StateMaster(1L, "State 1"));
        quotation.setShipperAddress(shipperAddress);
        quotation.setShipper(shipper);

        when(cacheRepository.get(anyString(), anyString(), anyLong(), any())).thenReturn(quotation.getCustomer(), shipper);
        return quotation;
    }

    private Quotation quotationWithCustomerAndAddress() {
        Quotation quotation = quotationWithCustomer();
        AddressMaster customerAddress = new AddressMaster(1L, "a1", "a2", "a3");
        quotation.setCustomerAddress(customerAddress);
        customerAddress.setCity(new CityMaster(1L, "City1"));
        customerAddress.setState(new StateMaster(1L, "State 1"));
        quotation.setCustomerAddress(customerAddress);
        return quotation;
    }


    private Quotation quotationWithCustomer() {
        Quotation quotation = quotationWithCountry();
        PartyMaster partyMaster = new PartyMaster(1L);
        partyMaster.setStatus(LovStatus.Active);
        partyMaster.setPartyCode("CUS1");
        quotation.setCustomer(partyMaster);
        return quotation;
    }

    private Quotation quotationWithCountry() {
        Quotation quotation = quotationWithLocation();
        CountryMaster countryMaster = new CountryMaster(1L);
        countryMaster.setCountryCode("CC1");
        quotation.setCountryMaster(countryMaster);
        return quotation;
    }

    private Quotation quotationWithCompany() {
        Quotation quotation = quotationWithoutEnquiry();
        CompanyMaster companyMaster = new CompanyMaster(1L, "C1", "Company 1");
        quotation.setCompanyMaster(companyMaster);
        return quotation;
    }


    private Quotation quotationWithLocation() {
        Quotation quotation = quotationWithCompany();
        LocationMaster locationMaster = new LocationMaster(1L, "L1");
        quotation.setLocationMaster(locationMaster);
        return quotation;
    }

    private Quotation quotationWithoutEnquiry() {
        Quotation quotation = new Quotation();
        quotation.setEnquiryNo("ENQ001");
        when(quotationRepository.findByEnquiryNo("ENQ001")).thenReturn(null);
        return quotation;
    }
}
