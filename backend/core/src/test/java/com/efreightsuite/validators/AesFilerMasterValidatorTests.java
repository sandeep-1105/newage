package com.efreightsuite.validators;

import com.efreightsuite.enumeration.LovStatus;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.model.AesFilerMaster;
import com.efreightsuite.model.CompanyMaster;
import com.efreightsuite.model.LocationMaster;
import com.efreightsuite.service.RegularExpressionService;
import com.efreightsuite.util.RegExpName;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class AesFilerMasterValidatorTests {

    @Mock
    private RegularExpressionService regularExpressionService;

    @InjectMocks
    private AesFilerMasterValidator validator;

    @Test
    public void should_throw_exception_aes_code_is_not_present() throws Exception {
        Assertions.assertThatThrownBy(() -> validator.validate(new AesFilerMaster()))
                .isInstanceOf(RestException.class)
                .hasMessage(ErrorCode.AESFILER_CODE_NOT_NULL);
    }

    @Test
    public void should_validate_successfully_when_object_is_valid() throws Exception {
        AesFilerMaster aesFilerMaster = new AesFilerMaster();
        aesFilerMaster.setAesFilerCode("AES-1");
        aesFilerMaster.setLocationMaster(new LocationMaster());
        aesFilerMaster.setCompanyMaster(new CompanyMaster());
        aesFilerMaster.setStatus(LovStatus.Active);
        aesFilerMaster.setTransmitterCode("T1");

        validator.validate(aesFilerMaster);

        verify(regularExpressionService).validate("AES-1", RegExpName.REG_EXP_MASTER_AES_FILER_CODE, ErrorCode.AESFILER_CODE_INVALID);
        verify(regularExpressionService).validate("T1", RegExpName.REG_EXP_MASTER_AES_FILER_TRANSMITTER_CODE, ErrorCode.AESFILER_TRANSMITTER_CODE_INVALID);
    }
}
