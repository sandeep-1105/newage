package com.efreightsuite.validators;

import com.efreightsuite.controller.representations.EnquiryLogRequest;
import com.efreightsuite.enumeration.EnquiryStatus;
import com.efreightsuite.enumeration.ImportExport;
import com.efreightsuite.enumeration.LovStatus;
import com.efreightsuite.enumeration.Service;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.model.*;
import com.efreightsuite.repository.*;
import com.efreightsuite.service.RegularExpressionService;
import com.efreightsuite.validation.EnquiryAttachmentValidator;
import com.efreightsuite.validation.EnquiryDimensionValidator;
import org.joda.time.DateTime;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collections;
import java.util.Date;

import static com.efreightsuite.controller.representations.EnquiryLogRequest.Builder;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.assertj.core.api.Assertions.fail;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)

public class EnquiryLogValidatorTests {


    @Mock
    private EnquiryServiceMappingRepository enquiryServiceMappingRepository;

    @Spy
    private RegularExpressionService regularExpressionService;

    @Mock
    private
    EnquiryAttachmentValidator enquiryAttachmentValidator;

    @Mock
    private
    EnquiryDimensionValidator enquiryDimensionValidator;

    @Mock
    private
    CompanyMasterRepository companyMasterRepository;

    @Mock
    private
    LocationMasterRepository locationMasterRepository;

    @Mock
    private
    CountryMasterRepository countryMasterRepository;

    @Mock
    private
    TosMasterRepository tosMasterRepository;


    @Mock
    private
    PortMasterRepository portMasterRepository;

    @Autowired
    private
    CommodityMasterRepository commodityMasterRepository;

    @Mock
    private
    PartyMasterRepository partyMasterRepository;

    @Mock
    private
    ServiceMasterRepository serviceMasterRepository;



    @InjectMocks
    private final EnquiryLogValidator validator = new EnquiryLogValidator();

    @Test
    public void should_throw_exception_when_party_master_is_not_present() throws Exception {
        final EnquiryLogRequest enquiryLog = new Builder().build();
        assertThatThrownBy(() -> validator.validate(enquiryLog))
                .isInstanceOf(RestException.class)
                .hasMessage("ERR7003");

        EnquiryLogRequest anotherEnquiryLog = new Builder()
                .withPartyMaster(new PartyMaster())
                .build();
        assertThatThrownBy(() -> validator.validate(anotherEnquiryLog))
                .isInstanceOf(RestException.class)
                .hasMessage("ERR7003");
    }

    @Test
    public void should_throw_exception_when_quote_by_date_is_null() throws Exception {
        PartyMaster partyMaster = new PartyMaster(1L);
        EnquiryLogRequest enquiryLog = new Builder()
                .withPartyMaster(partyMaster)
                .build();
        assertThatThrownBy(() -> validator.validate(enquiryLog))
                .isInstanceOf(RestException.class)
                .hasMessage("ERR8781");
    }

    @Test
    public void should_throw_exception_when_received_by_date_is_null() throws Exception {
        PartyMaster partyMaster = new PartyMaster(1L);
        EnquiryLogRequest enquiryLog = new Builder()
                .withPartyMaster(partyMaster)
                .withQuoteBy(new Date())
                .build();
        assertThatThrownBy(() -> validator.validate(enquiryLog))
                .isInstanceOf(RestException.class)
                .hasMessage("ERR8121");
    }

    @Test
    public void should_throw_exception_when_quote_by_date_is_before_received_by_date() throws Exception {
        PartyMaster partyMaster = new PartyMaster(1L);
        EnquiryLogRequest enquiryLog = new Builder()
                .withPartyMaster(partyMaster)
                .withQuoteBy(new Date())
                .withReceivedOn(new DateTime().plusDays(1).toDate())
                .build();
        assertThatThrownBy(() -> validator.validate(enquiryLog))
                .isInstanceOf(RestException.class)
                .hasMessage("ERR8812");
    }

    @Test
    public void should_not_throw_exception_when_quote_by_date_is_same_day_as_received_date() throws Exception {
        PartyMaster partyMaster = new PartyMaster(1L);
        EnquiryLogRequest enquiryLog = new Builder()
                .withPartyMaster(partyMaster)
                .withQuoteBy(new DateTime().minusSeconds(1).toDate())
                .withReceivedOn(new Date())
                .build();
        try {
            validator.validateQuoteByAndReceivedOnDate(enquiryLog);
        } catch (Exception e) {
            fail("Should not throw exception");
        }

    }

    @Test
    public void should_throw_exception_when_status_is_not_present() throws Exception {
        PartyMaster partyMaster = new PartyMaster(1L);
        EnquiryLogRequest enquiryLog = new Builder()
                .withPartyMaster(partyMaster)
                .withQuoteBy(new Date())
                .withReceivedOn(new DateTime().minusDays(1).toDate())
                .build();
        assertThatThrownBy(() -> validator.validate(enquiryLog))
                .isInstanceOf(RestException.class)
                .hasMessage("ERR7007");
    }

    @Test
    public void should_throw_exception_when_sales_coordinator_is_not_present() throws Exception {
        PartyMaster partyMaster = new PartyMaster(1L);
        Builder builder = new Builder()
                .withPartyMaster(partyMaster)
                .withQuoteBy(new Date())
                .withReceivedOn(new DateTime().minusDays(1).toDate())
                .withStatus(EnquiryStatus.Active);
        final EnquiryLogRequest enquiryLog = builder.build();
        assertThatThrownBy(() -> validator.validate(enquiryLog))
                .isInstanceOf(RestException.class)
                .hasMessage("ERR7009");

        builder.withSalesCoOrdinator(new EmployeeMaster());
        EnquiryLogRequest enquiryLog2 = builder.build();
        assertThatThrownBy(() -> validator.validate(enquiryLog2))
                .isInstanceOf(RestException.class)
                .hasMessage("ERR7009");
    }

    @Test
    public void should_throw_exception_when_enquiry_details_is_not_present() throws Exception {
        PartyMaster partyMaster = new PartyMaster(1L);
        Builder builder = new Builder()
                .withPartyMaster(partyMaster)
                .withQuoteBy(new Date())
                .withReceivedOn(new DateTime().minusDays(1).toDate())
                .withStatus(EnquiryStatus.Active)
                .withSalesCoOrdinator(new EmployeeMaster(1L));

        assertThatThrownBy(() -> validator.validate(builder.build()))
                .isInstanceOf(RestException.class)
                .hasMessage("ERR7014");

        builder.withEnquiryDetailList(Collections.emptyList());
        assertThatThrownBy(() -> validator.validate(builder.build()))
                .isInstanceOf(RestException.class)
                .hasMessage("ERR7014");
    }

    @Test
    public void should_throw_exception_when_logged_by_is_not_present() throws Exception {
        PartyMaster partyMaster = new PartyMaster(1L);
        EnquiryDetail enquiryDetail = new EnquiryDetail();
        enquiryDetail.setService(Service.Air);
        enquiryDetail.setImportExport(ImportExport.Export);

        EnquiryLogRequest enquiryLog = new Builder()
                .withPartyMaster(partyMaster)
                .withQuoteBy(new Date())
                .withReceivedOn(new DateTime().minusDays(1).toDate())
                .withStatus(EnquiryStatus.Active)
                .withSalesCoOrdinator(new EmployeeMaster(1L))
                .withEnquiryDetailList(Collections.singletonList(enquiryDetail))
                .build();

        assertThatThrownBy(() -> validator.validate(enquiryLog))
                .isInstanceOf(RestException.class)
                .hasMessage("ERR8043");
    }

    @Test
    public void should_throw_exception_when_company_master_is_not_present() throws Exception {
        PartyMaster partyMaster = new PartyMaster(1L);
        EnquiryDetail enquiryDetail = new EnquiryDetail();
        enquiryDetail.setService(Service.Air);
        enquiryDetail.setImportExport(ImportExport.Export);

        CompanyMaster companyMaster1 = new CompanyMaster();
        companyMaster1.setCompanyName("Xebia");

        when(companyMasterRepository.findById(Mockito.anyLong())).thenReturn(companyMaster1);

        Builder builder = new Builder()
                .withPartyMaster(partyMaster)
                .withQuoteBy(new Date())
                .withReceivedOn(new DateTime().minusDays(1).toDate())
                .withStatus(EnquiryStatus.Active)
                .withSalesCoOrdinator(new EmployeeMaster(1L))
                .withEnquiryDetailList(Collections.singletonList(enquiryDetail))
                .withLoggedBy(new EmployeeMaster(1L))
                .withCompanyMaster(null);
        final EnquiryLogRequest enquiryLog = builder.build();

        assertThatThrownBy(() -> validator.validate(enquiryLog))
                .isInstanceOf(RestException.class)
                .hasMessage("ERR8814");

        CompanyMaster companyMaster = new CompanyMaster();
        EnquiryLogRequest enquiryLog2 = builder
                .withCompanyMaster(companyMaster)
                .build();

        assertThatThrownBy(() -> validator.validate(enquiryLog2))
                .isInstanceOf(RestException.class)
                .hasMessage("ERR8814");

        companyMaster = new CompanyMaster(1L);
        EnquiryLogRequest enquiryLog3 = builder
                .withCompanyMaster(companyMaster)
                .build();

        assertThatThrownBy(() -> validator.validate(enquiryLog3))
                .isInstanceOf(RestException.class)
                .hasMessage("ERR8814");

    }

    @Test
    public void should_throw_exception_when_location_master_is_not_present() throws Exception {
        PartyMaster partyMaster = new PartyMaster(1L);
        EnquiryDetail enquiryDetail = new EnquiryDetail();
        enquiryDetail.setService(Service.Air);
        enquiryDetail.setImportExport(ImportExport.Export);
        CompanyMaster companyMaster = new CompanyMaster(1L, "CC1", "Company 1");
        /*LocationMaster locationMaster = new LocationMaster();
        locationMaster.setLocationCode("XYZ");*/

        when(companyMasterRepository.findById(Mockito.anyLong())).thenReturn(companyMaster);


        Builder builder = new Builder()
                .withPartyMaster(partyMaster)
                .withQuoteBy(new Date())
                .withReceivedOn(new DateTime().minusDays(1).toDate())
                .withStatus(EnquiryStatus.Active)
                .withSalesCoOrdinator(new EmployeeMaster(1L))
                .withEnquiryDetailList(Collections.singletonList(enquiryDetail))
                .withLoggedBy(new EmployeeMaster(1L))
                .withCompanyMaster(companyMaster)
                .withLocationMaster(null);

        assertThatThrownBy(() -> validator.validate(builder.build()))
                .isInstanceOf(RestException.class)
                .hasMessage("ERR8813");


        assertThatThrownBy(() -> validator.validate(builder.withLocationMaster(new LocationMaster()).build()))
                .isInstanceOf(RestException.class)
                .hasMessage("ERR8813");

        when(locationMasterRepository.findById(Mockito.anyLong())).thenReturn(new LocationMaster(1L));

        assertThatThrownBy(() -> validator.validate(builder.withLocationMaster(new LocationMaster(1L)).build()))
                .isInstanceOf(RestException.class)
                .hasMessage("ERR8813");
    }

    @Test
    public void should_throw_exception_when_country_master_is_not_present() throws Exception {
        PartyMaster partyMaster = new PartyMaster(1L);
        EnquiryDetail enquiryDetail = new EnquiryDetail();
        enquiryDetail.setService(Service.Air);
        enquiryDetail.setImportExport(ImportExport.Export);
        CompanyMaster companyMaster = new CompanyMaster(1L, "CC1", "Company 1");

        when(companyMasterRepository.findById(Mockito.anyLong())).thenReturn(companyMaster);
        when(locationMasterRepository.findById(Mockito.anyLong())).thenReturn(new LocationMaster(1L, "LL1"));


        Builder builder = new Builder()
                .withPartyMaster(partyMaster)
                .withQuoteBy(new Date())
                .withReceivedOn(new DateTime().minusDays(1).toDate())
                .withStatus(EnquiryStatus.Active)
                .withSalesCoOrdinator(new EmployeeMaster(1L))
                .withEnquiryDetailList(Collections.singletonList(enquiryDetail))
                .withLoggedBy(new EmployeeMaster(1L))
                .withCompanyMaster(companyMaster)
                .withLocationMaster(new LocationMaster(1L, "LL1"))
                .withCountryMaster(null);

        assertThatThrownBy(() -> validator.validate(builder.build()))
                .isInstanceOf(RestException.class)
                .hasMessage("ERR8815");

        when(countryMasterRepository.findById(Mockito.anyLong())).thenReturn(new CountryMaster(1L));


        assertThatThrownBy(() -> validator.validate(builder.withCountryMaster(new CountryMaster(1L)).build()))
                .isInstanceOf(RestException.class)
                .hasMessage("ERR8815");
    }

    @Test
    public void should_set_service_master_to_enquiry_detail() throws Exception {
        PartyMaster partyMaster = new PartyMaster(1L);
        CountryMaster countryMaster = new CountryMaster(1L);
        countryMaster.setCountryCode("IN");

        EnquiryDetail enquiryDetail = new EnquiryDetail();
        enquiryDetail.setService(Service.Air);
        enquiryDetail.setImportExport(ImportExport.Export);
        enquiryDetail.setFromPortMaster(new PortMaster(1L, LovStatus.Active));
        enquiryDetail.setToPortMaster(new PortMaster(1L, LovStatus.Active));

        EnquiryServiceMapping serviceMapping = new EnquiryServiceMapping();
        serviceMapping.setServiceMaster(new ServiceMaster(1L, LovStatus.Active));

        when(enquiryServiceMappingRepository.findByServiceAndTransportMode(Service.Air, ImportExport.Export)).thenReturn(serviceMapping);
        doNothing().when(regularExpressionService).validate(any(), anyString(), anyString());

        when(companyMasterRepository.findById(Mockito.anyLong())).thenReturn(new CompanyMaster(1L, "CC1", "Company 1"));
        when(locationMasterRepository.findById(Mockito.anyLong())).thenReturn(new LocationMaster(1L, "LL1"));
        when(countryMasterRepository.findById(Mockito.anyLong())).thenReturn(countryMaster);
        when(serviceMasterRepository.getOne(Mockito.anyLong())).thenReturn(new ServiceMaster(1l,LovStatus.Active));
        when(portMasterRepository.findById(Mockito.anyLong())).thenReturn(new PortMaster(1L, LovStatus.Active));
        when(tosMasterRepository.getOne(Mockito.anyLong())).thenReturn(new TosMaster("tosCode","tosName",LovStatus.Active,null));

        EnquiryLogRequest enquiryLog = new Builder()
                .withPartyMaster(partyMaster)
                .withQuoteBy(new Date())
                .withReceivedOn(new DateTime().minusDays(1).toDate())
                .withStatus(EnquiryStatus.Active)
                .withSalesCoOrdinator(new EmployeeMaster(1L))
                .withEnquiryDetailList(Collections.singletonList(enquiryDetail))
                .withLoggedBy(new EmployeeMaster(1L))
                .withCompanyMaster(new CompanyMaster(1L, "CC1", "Company 1"))
                .withCountryMaster(countryMaster)
                .withLocationMaster(new LocationMaster(1L, "LL1"))
                .withCountryMaster(countryMaster)
                .build();

        validator.validate(enquiryLog);
        verify(enquiryServiceMappingRepository, times(1)).findByServiceAndTransportMode(Service.Air, ImportExport.Export);
    }
}
