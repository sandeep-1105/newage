package com.efreightsuite.controller;

import java.util.Arrays;
import java.util.Date;

import com.efreightsuite.dto.BankSearchDto;
import com.efreightsuite.dto.SearchRequest;
import com.efreightsuite.enumeration.LovStatus;
import com.efreightsuite.model.BankMaster;
import com.efreightsuite.model.SystemTrack;
import com.efreightsuite.repository.BankMasterRepository;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class BankMasterControllerTests extends BaseRestApiTest {

    private static final String API_URL = "/api/v1/bankmaster/get/search";

    @Autowired
    private BankMasterRepository bankMasterRepository;

    @Before
    public void setUp() throws Exception {
        SystemTrack systemTrack = new SystemTrack("test_user", new Date(), "test_user", new Date());
        this.bankMasterRepository.save(
                Arrays.asList(
                        new BankMaster("ICICI", "bank01", LovStatus.Active, systemTrack),
                        new BankMaster("HDFC", "bank02", LovStatus.Active, systemTrack),
                        new BankMaster("Punjab National Bank", "bank03", LovStatus.Active, systemTrack)
                )
        );
    }

    @After
    public void tearDown() throws Exception {
        this.bankMasterRepository.deleteAll();
    }

    @Test
    @WithMockUser(username = "test_user", roles = "MASTER.FINANCE.BANK.LIST")
    public void should_search_bank_by_name() throws Exception {
        String query = this.gson.toJson(new BankSearchDto("icic"));
        this.mockMvc.perform(
                post(API_URL)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(query))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.responseObject.searchResult").isArray())
                .andExpect(jsonPath("$.responseObject.searchResult.length()").value(1))
                .andExpect(jsonPath("$.responseObject.searchResult[*].bankName")
                        .value(toJsonArray("ICICI")));
    }

    @Test
    @WithMockUser(username = "test_user", roles = "MASTER.FINANCE.BANK.LIST")
    public void should_search_bank_by_name_code_and_status() throws Exception {
        String query = this.gson.toJson(new BankSearchDto("HDFC", "bank02", "active"));
        this.mockMvc.perform(
                post(API_URL)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(query))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.responseObject.searchResult").isArray())
                .andExpect(jsonPath("$.responseObject.searchResult.length()").value(1))
                .andExpect(jsonPath("$.responseObject.searchResult[*].bankName")
                        .value(toJsonArray("HDFC")));
    }

    @Test
    @WithMockUser(username = "test_user", roles = "MASTER.FINANCE.BANK.LIST")
    public void should_search_without_keyword() throws Exception {
        String query = this.gson.toJson(new BankSearchDto());
        this.mockMvc.perform(
                post(API_URL)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(query))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.responseObject.searchResult").isArray())
                .andExpect(jsonPath("$.responseObject.searchResult.length()").value(3))
                .andExpect(jsonPath("$.responseObject.searchResult[*].bankName")
                        .value(toJsonArray("HDFC", "ICICI", "Punjab National Bank")));
    }

    @Test
    @WithMockUser(username = "test_user", roles = "MASTER.FINANCE.BANK.LIST")
    public void should_search_bank_keyword() throws Exception {
        String query = this.gson.toJson(new SearchRequest("hdf"));
        this.mockMvc.perform(
                post(API_URL + "/keyword")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(query))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.responseObject.searchResult").isArray())
                .andExpect(jsonPath("$.responseObject.searchResult.length()").value(1))
                .andExpect(jsonPath("$.responseObject.searchResult[*].bankName")
                        .value(toJsonArray("HDFC")));
    }
}
