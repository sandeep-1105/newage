package com.efreightsuite.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.efreightsuite.dto.FeatureDTO;
import com.efreightsuite.dto.RoleDTO;
import com.efreightsuite.enumeration.LovStatus;
import com.efreightsuite.enumeration.YesNo;
import com.efreightsuite.util.AppUtil;
import org.junit.Test;
import org.mockito.Matchers;

import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MvcResult;

import static org.hamcrest.CoreMatchers.notNullValue;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class RoleControllerTests extends BaseRestApiTest {

    @MockBean
    protected AppUtil appUtil;

    @Test
    @WithMockUser(username = "test_user", roles = {"SETUP.ROLES_AND_PRIVILEGES.CREATE"})
    public void should_add_new_role_and_return_200_ok() throws Exception {

        RoleDTO roleDTO = createRoleDTO();
        String request = gson.toJson(roleDTO);
        when(appUtil.setDesc(Matchers.any())).then(invocation -> invocation.getArguments()[0]);
        this.mockMvc.perform(
                post("/api/v1/role/create")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(request))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.responseObject.id").value(notNullValue()));

    }


    @Test
    @WithMockUser(username = "test_user", roles = {"SETUP.ROLES_AND_PRIVILEGES.CREATE", "SETUP.ROLES_AND_PRIVILEGES.MODIFY"})
    public void should_update_role_by_adding_new_feature() throws Exception {

        RoleDTO roleDTO = createRoleDTO();
        String request = gson.toJson(roleDTO);
        when(appUtil.setDesc(Matchers.any())).then(invocation -> invocation.getArguments()[0]);
        MvcResult mvcResult = this.mockMvc.perform(
                post("/api/v1/role/create")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(request))
                .andDo(print())
                .andExpect(status().isOk()).andReturn();

        String json = mvcResult.getResponse().getContentAsString();
        Map<String, Object> map = fromJson(json);
        long id = getIdFromResponse(map);

        RoleDTO updatedRoleDTO = updateRoleDTONewFeature(roleDTO, id);

        this.mockMvc.perform(
                post("/api/v1/role/update")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(gson.toJson(updatedRoleDTO)))
                .andDo(print())
                .andExpect(status().isOk());
    }

    private RoleDTO updateRoleDTONewFeature(RoleDTO roleDTO, long id) {

        FeatureDTO featureDTO = new FeatureDTO();

        featureDTO.setFeatureName("Quotation");
        featureDTO.setFeatureValue("SALE.QUOTATION");
        featureDTO.setCreate(YesNo.Yes);
        featureDTO.setValCreate("Yes");
        featureDTO.setList(YesNo.Yes);
        featureDTO.setValList("Yes");
        featureDTO.setModify(YesNo.Yes);
        featureDTO.setValModify("Yes");
        featureDTO.setDelete(YesNo.No);
        featureDTO.setValDelete(null);
        featureDTO.setDownload(YesNo.No);
        featureDTO.setValDownload(null);
        featureDTO.setView(YesNo.Yes);
        featureDTO.setValView("Yes");

        roleDTO.getFeaturesList().add(featureDTO);
        roleDTO.setId(id);

        return roleDTO;
    }

    private RoleDTO createRoleDTO() {

        RoleDTO roleDTO = new RoleDTO();
        roleDTO.setRoleName("Test1");
        roleDTO.setDescription("Test user");

        FeatureDTO featureDTO1 = new FeatureDTO();
        List<FeatureDTO> featuresList = new ArrayList<>();

        featureDTO1.setFeatureName("Sales");
        featureDTO1.setFeatureValue("SALE");
        featureDTO1.setCreate(YesNo.No);
        featureDTO1.setValCreate(null);
        featureDTO1.setList(YesNo.No);
        featureDTO1.setValList(null);
        featureDTO1.setModify(YesNo.No);
        featureDTO1.setValModify(null);
        featureDTO1.setDelete(YesNo.No);
        featureDTO1.setValDelete(null);
        featureDTO1.setDownload(YesNo.No);
        featureDTO1.setValDownload(null);
        featureDTO1.setView(YesNo.No);
        featureDTO1.setValView(null);

        featuresList.add(featureDTO1);
        roleDTO.setFeaturesList(featuresList);
        roleDTO.setStatus(LovStatus.Active);
        roleDTO.setSystemTrack(null);
        roleDTO.setUserList(null);

        return roleDTO;
    }
}
