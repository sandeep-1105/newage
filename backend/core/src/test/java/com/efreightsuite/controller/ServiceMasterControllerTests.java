package com.efreightsuite.controller;

import com.efreightsuite.enumeration.ImportExport;
import com.efreightsuite.enumeration.LovStatus;
import com.efreightsuite.enumeration.TransportMode;
import com.efreightsuite.model.ServiceMaster;
import com.efreightsuite.model.SystemTrack;
import com.efreightsuite.repository.ServiceMasterRepository;
import com.efreightsuite.util.AppUtil;
import org.junit.Test;
import org.mockito.Matchers;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class ServiceMasterControllerTests extends BaseRestApiTest {

    @MockBean
    protected AppUtil appUtil;
    @MockBean
    ServiceMasterRepository serviceMasterRepository;

    @Test
    @WithMockUser(username = "test_user")
    public void should_return_air_export_service() throws Exception {

        when(appUtil.setDesc(Matchers.any())).then(invocation -> invocation.getArguments()[0]);
        given(serviceMasterRepository
                .getAirExportService())
                .willReturn(new ServiceMaster("Air Export",
                        "AE",
                        TransportMode.Air,
                        ImportExport.Export,
                        new SystemTrack(),
                        LovStatus.Active));
        this.mockMvc.perform(get("/api/v1/servicemaster/get/airexportservice"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.responseObject.serviceCode").value("AE"))
                .andExpect(jsonPath("$.responseObject.serviceName").value("Air Export"))
                .andExpect(jsonPath("$.responseObject.importExport").value("Export"))
                .andExpect(jsonPath("$.responseObject.transportMode").value("Air"));
    }
}

