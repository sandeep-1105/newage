package com.efreightsuite.controller;

import java.util.Arrays;
import java.util.Date;

import com.efreightsuite.dto.AutoMailGroupMasterSearchDto;
import com.efreightsuite.dto.SearchRequest;
import com.efreightsuite.model.AutoMailGroupMaster;
import com.efreightsuite.model.SystemTrack;
import com.efreightsuite.repository.AutoMailGroupMasterRepository;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class AutoMailGroupMasterControllerTests extends BaseRestApiTest {

    private static final String API_URL = "/api/v1/automailgroupmaster/get/search";

    @Autowired
    private AutoMailGroupMasterRepository autoMailGroupMasterRepository;

    @Before
    public void setUp() throws Exception {
        SystemTrack systemTrack = new SystemTrack("test_user", new Date(), "test_user", new Date());
        this.autoMailGroupMasterRepository.save(
                Arrays.asList(
                        new AutoMailGroupMaster("Group 1", "g1", systemTrack),
                        new AutoMailGroupMaster("Group 2", "g2", systemTrack),
                        new AutoMailGroupMaster("Another Group 3", "g3", systemTrack)
                )
        );
    }

    @After
    public void tearDown() throws Exception {
        this.autoMailGroupMasterRepository.deleteAll();
    }

    @Test
    @WithMockUser(username = "test_user", roles = "MASTER")
    public void should_search_by_name() throws Exception {
        String query = this.gson.toJson(new AutoMailGroupMasterSearchDto("group", null));
        this.mockMvc.perform(
                post(API_URL)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(query))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.responseObject.searchResult").isArray())
                .andExpect(jsonPath("$.responseObject.searchResult.length()").value(2))
                .andExpect(jsonPath("$.responseObject.searchResult[*].messageGroupName")
                        .value(toJsonArray("Group 1", "Group 2")));
    }

    @Test
    @WithMockUser(username = "test_user", roles = "MASTER")
    public void should_search_by_name_and_code() throws Exception {
        String query = this.gson.toJson(new AutoMailGroupMasterSearchDto("group", "g2"));
        this.mockMvc.perform(
                post(API_URL)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(query))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.responseObject.searchResult").isArray())
                .andExpect(jsonPath("$.responseObject.searchResult.length()").value(1))
                .andExpect(jsonPath("$.responseObject.searchResult[*].messageGroupName")
                        .value(toJsonArray("Group 2")));
    }

    @Test
    @WithMockUser(username = "test_user")
    public void should_search_without_keyword() throws Exception {
        String query = this.gson.toJson(new AutoMailGroupMasterSearchDto());
        this.mockMvc.perform(
                post(API_URL)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(query))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.responseObject.searchResult").isArray())
                .andExpect(jsonPath("$.responseObject.searchResult.length()").value(3))
                .andExpect(jsonPath("$.responseObject.searchResult[*].messageGroupName")
                        .value(toJsonArray("Group 1", "Group 2", "Another Group 3")));
    }

    @Test
    @WithMockUser(username = "test_user")
    public void should_search_with_keyword() throws Exception {
        String query = this.gson.toJson(new SearchRequest("group"));
        this.mockMvc.perform(
                post(API_URL + "/keyword")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(query))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.responseObject.searchResult").isArray())
                .andExpect(jsonPath("$.responseObject.searchResult.length()").value(2))
                .andExpect(jsonPath("$.responseObject.searchResult[*].messageGroupName")
                        .value(toJsonArray("Group 1", "Group 2")));
    }
}
