package com.efreightsuite.controller;

import com.efreightsuite.dto.CompanySearchReqDto;
import com.efreightsuite.dto.SearchRequest;
import com.efreightsuite.enumeration.LovStatus;
import com.efreightsuite.model.CompanyMaster;
import com.efreightsuite.model.SystemTrack;
import com.efreightsuite.repository.CompanyMasterRepository;
import com.efreightsuite.util.AppUtil;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Matchers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.ResultActions;

import java.util.Date;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class CompanyMasterControllerTests extends BaseRestApiTest {


    @MockBean
    private AppUtil appUtil;

    @Autowired
    private CompanyMasterRepository companyMasterRepository;

    @Before
    public void setUpTestData() {
        SystemTrack systemTrack = new SystemTrack("test_user", new Date(), "test_user", new Date());
        this.companyMasterRepository.save(new CompanyMaster("TYKO Corp", "TYK", LovStatus.Active, systemTrack));
        this.companyMasterRepository.save(new CompanyMaster("IBM Corp", "IBM", LovStatus.Block, systemTrack));
    }

    @Test
    @WithMockUser(username = "test_user")
    public void should_be_able_search_company_by_company_code() throws Exception {

        CompanySearchReqDto companySearchReq = new CompanySearchReqDto();
        companySearchReq.setSearchCompanyCode("TYK");
        when(appUtil.setDesc(Matchers.any())).then(invocation -> invocation.getArguments()[0]);
        this.mockMvc.perform(
                post("/api/v1/companymaster/get/search")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(toJson(companySearchReq)))
                .andDo(print())
                .andExpect(jsonPath("$.responseObject.searchResult").isArray())
                .andExpect(jsonPath("$.responseObject.searchResult.length()").value(1))
                .andExpect(jsonPath("$.responseObject.searchResult[*].companyCode").value(toJsonArray("TYK")));
    }

    @Test
    @WithMockUser(username = "test_user")
    public void should_be_able_search_company_by_company_name() throws Exception {

        CompanySearchReqDto companySearchReq = new CompanySearchReqDto();
        companySearchReq.setSearchCompanyName("TYKO Corp");
        when(appUtil.setDesc(Matchers.any())).then(invocation -> invocation.getArguments()[0]);
        this.mockMvc.perform(
                post("/api/v1/companymaster/get/search")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(toJson(companySearchReq)))
                .andDo(print())
                .andExpect(jsonPath("$.responseObject.searchResult").isArray())
                .andExpect(jsonPath("$.responseObject.searchResult.length()").value(1))
                .andExpect(jsonPath("$.responseObject.searchResult[*].companyCode").value(toJsonArray("TYK")));
    }

    @Test
    @WithMockUser(username = "test_user")
    public void should_be_able_search_company_by_status() throws Exception {

        CompanySearchReqDto companySearchReq = new CompanySearchReqDto();
        companySearchReq.setSearchStatus("Block");
        when(appUtil.setDesc(Matchers.any())).then(invocation -> invocation.getArguments()[0]);
        this.mockMvc.perform(
                post("/api/v1/companymaster/get/search")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(toJson(companySearchReq)))
                .andDo(print())
                .andExpect(jsonPath("$.responseObject.searchResult").isArray())
                .andExpect(jsonPath("$.responseObject.searchResult.length()").value(1));
    }

    @Test
    @WithMockUser(username = "test_user")
    public void should_be_able_search_company_by_keyword() throws Exception {

        SearchRequest searchRequest = new SearchRequest("TYK");
        when(appUtil.setDesc(Matchers.any())).then(invocation -> invocation.getArguments()[0]);
        this.mockMvc.perform(
                post("/api/v1/companymaster/get/search/keyword")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(toJson(searchRequest)))
                .andDo(print())
                .andExpect(jsonPath("$.responseObject.searchResult").isArray())
                .andExpect(jsonPath("$.responseObject.searchResult.length()").value(1))
                .andExpect(jsonPath("$.responseObject.searchResult[*].companyCode").value(toJsonArray("TYK")));
    }

    private ResultActions makeGetCompanyMasterByIdRequest(long companyMasterId, int version) throws Exception {
        return this.mockMvc.perform(get("/api/v1/companymaster/get/id/{id}", companyMasterId))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.responseObject.versionLock").value(version));
    }

    private String createCompanyRequestJson() {
        CompanyMaster companyMaster = createCompanyMaster("TYK", "TYKO Corp", LovStatus.Active, 0);
        return gson.toJson(companyMaster);
    }

    private CompanyMaster createCompanyMaster(String companyCode, String companyName, LovStatus status, int version) {

        CompanyMaster companyMaster = new CompanyMaster();
        companyMaster.setCompanyCode(companyCode);
        companyMaster.setCompanyName(companyName);
        companyMaster.setStatus(status);
        companyMaster.setSystemTrack(new SystemTrack());
        companyMaster.setVersionLock(version);

        return companyMaster;
    }
}