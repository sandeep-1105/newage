package com.efreightsuite.controller;

import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

import com.efreightsuite.controller.representations.EnquiryLogRequest;
import com.efreightsuite.dto.DateRange;
import com.efreightsuite.dto.EnquiryLogSearchDto;
import com.efreightsuite.enumeration.DimensionUnit;
import com.efreightsuite.enumeration.EnquiryStatus;
import com.efreightsuite.enumeration.ImportExport;
import com.efreightsuite.enumeration.SequenceType;
import com.efreightsuite.enumeration.YesNo;
import com.efreightsuite.model.EnquiryDetail;
import com.efreightsuite.model.PortMaster;
import com.efreightsuite.model.UserProfile;
import com.efreightsuite.model.WorkFlowGroup;
import com.efreightsuite.repository.UserProfileRepository;
import com.efreightsuite.util.AppUtil;
import org.joda.time.DateTime;
import org.junit.Test;
import org.mockito.Matchers;

import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;

import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.Matchers.contains;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class EnquiryLogControllerTests extends BaseRestApiTest {

    @MockBean
    private AppUtil appUtil;
    @MockBean
    private UserProfileRepository userProfileRepository;

    @Test
    @WithMockUser(username = "test_user", roles = {"SALE.ENQUIRY.CREATE", "SALE.ENQUIRY.VIEW", "SALE.ENQUIRY.MODIFY"})
    public void should_save_get_and_update_an_enquiry() throws Exception {
        String request = createEnquiryRequestJson();
        given(sequenceGeneratorService.getSequence(SequenceType.ENQUERY)).willReturn("ENQ001");
        when(appUtil.setDesc(Matchers.any())).then(invocation -> invocation.getArguments()[0]);
        MvcResult createEnquiryResult = makeCreateEnquiryRequest(request)
                .andDo(print())
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.responseObject.id").value(notNullValue()))
                .andExpect(jsonPath("$.responseObject.enquiryNo").value("ENQ001"))
                .andReturn();

        MockHttpServletResponse response = createEnquiryResult.getResponse();
        Map<String, Object> enquiry = fromJson(response.getContentAsString());
        long enquiryId = getIdFromResponse(enquiry);

        MvcResult getResult = makeGetEnquiryByIdRequest(enquiryId, 0)
                .andReturn();

        Map<String, Object> obj = fromJson(getResult.getResponse().getContentAsString());

        List<Object> enquiryDetails = (List<Object>) ((Map<String, Object>) obj.get("responseObject")).get("enquiryDetailList");

        long enquiryDetailId = ((Double) ((Map<String, Object>) enquiryDetails.get(0)).get("id")).longValue();
        int enquiryDetailVersionId = ((Double) ((Map<String, Object>) enquiryDetails.get(0)).get("versionLock")).intValue();
        EnquiryLogRequest updateEnquiry = createEnquiryRequest(
                enquiryId,
                new DateTime().plusDays(3).toDate(),
                enquiryDetailId,
                enquiryDetailVersionId);

        this.mockMvc.perform(
                post("/api/v1/enquirylog/{id}/update", enquiryId)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(gson.toJson(updateEnquiry)))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.responseObject.id").value(enquiryId))
                .andExpect(jsonPath("$.responseObject.enquiryNo").value("ENQ001"));

        makeGetEnquiryByIdRequest(enquiryId, 1)
                .andReturn();

        this.mockMvc.perform(get("/api/v1/enquirylog/get/enquiryNo/ENQ001"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.responseObject.id").value(notNullValue()))
                .andExpect(jsonPath("$.responseObject.versionLock").value(1))
                .andReturn();
    }

    @Test
    @WithMockUser(username = "test_user", roles = {"SALE.ENQUIRY.CREATE", "Administrator"})
    public void should_be_able_search_enquiry_by_keyword() throws Exception {
        String request = createEnquiryRequestJson();
        given(sequenceGeneratorService.getSequence(SequenceType.ENQUERY)).willReturn("ENQ001");
        when(appUtil.setDesc(Matchers.any())).then(invocation -> invocation.getArguments()[0]);
        makeCreateEnquiryRequest(request)
                .andDo(print())
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.responseObject.id").value(notNullValue()))
                .andExpect(jsonPath("$.responseObject.enquiryNo").value("ENQ001"));

        String searchQuery = "{\n" +
                "    \"selectedPageNumber\": 0,\n" +
                "    \"recordPerPage\": 10,\n" +
                "    \"keyword\": \"Enquiry\",\n" +
                "    \"searchCustomerName\": \"Party 1\"\n" +
                "}";

        UserProfile userProfile = new UserProfile(1L);
        userProfile.setSelectedUserLocation(location);
        userProfile.setSelectedCompany(company);
        given(userProfileRepository.findByUserName(anyString())).willReturn(userProfile);
        WorkFlowGroup workFlowGroup = new WorkFlowGroup();
        workFlowGroup.setWorkFlowName("Enquiry");
        given(userProfileRepository.findWorkFlowGroupListByUser(1L)).willReturn(Collections.singletonList(workFlowGroup));

        this.mockMvc.perform(
                post("/api/v1/activiti/airexport/group/Enquiry")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(searchQuery))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.responseObject.searchResult.length()").value(1));
    }

    @Test
    @WithMockUser(username = "test_user", roles = {"SALE.ENQUIRY.CREATE", "SALE.ENQUIRY.LIST"})
    public void should_search_enquiry_by_customer_name() throws Exception {
        createEnquiries(new DateTime(), "ENQ001", "ENQ002", "ENQ003");

        String searchRequest = toJson(
                new EnquiryLogSearchDto.Builder()
                        .withSearchCustomerName("party 1")
                        .build());
        this.mockMvc.perform(
                post("/api/v1/enquirylog/get/search/active")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(searchRequest))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.responseObject.searchResult.length()").value(2))
                .andExpect(jsonPath("$.responseObject.searchResult[*].enquiryNo", contains("ENQ002", "ENQ001")));
    }

    @Test
    @WithMockUser(username = "test_user", roles = {"SALE.ENQUIRY.CREATE", "SALE.ENQUIRY.LIST"})
    public void should_search_enquiry_by_enquiry_no() throws Exception {
        createEnquiries(new DateTime(), "ENQ001", "ENQ002", "ENQ003");

        String searchRequest = toJson(
                new EnquiryLogSearchDto.Builder()
                        .withSearchEnquiryNo("ENQ001")
                        .build());
        this.mockMvc.perform(
                post("/api/v1/enquirylog/get/search/active")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(searchRequest))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.responseObject.searchResult.length()").value(1))
                .andExpect(jsonPath("$.responseObject.searchResult[*].enquiryNo", contains("ENQ001")));
    }

    @Test
    @WithMockUser(username = "test_user", roles = {"SALE.ENQUIRY.CREATE", "SALE.ENQUIRY.LIST"})
    public void should_search_enquiry_by_quote_by_date() throws Exception {
        DateTime today = new DateTime();
        createEnquiries(today, "ENQ001", "ENQ002", "ENQ003");

        DateTime quoteByDate = today.plusDays(3);
        DateRange quoteByRange = toDateRange(quoteByDate, quoteByDate);

        String searchRequest = toJson(
                new EnquiryLogSearchDto.Builder()
                        .withSearchQuoteBy(quoteByRange)
                        .build());
        this.mockMvc.perform(
                post("/api/v1/enquirylog/get/search/active")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(searchRequest))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.responseObject.searchResult.length()").value(1))
                .andExpect(jsonPath("$.responseObject.searchResult[*].enquiryNo", contains("ENQ001")));
    }

    @Test
    @WithMockUser(username = "test_user", roles = {"SALE.ENQUIRY.CREATE", "SALE.ENQUIRY.LIST"})
    public void should_search_enquiry_by_received_on_date() throws Exception {
        DateTime receivedOn = new DateTime();
        createEnquiries(receivedOn, "ENQ001", "ENQ002", "ENQ003");

        DateRange receivedOnRange = toDateRange(receivedOn.minusDays(4), receivedOn);

        String searchRequest = toJson(
                new EnquiryLogSearchDto.Builder()
                        .withSearchReceivedOn(receivedOnRange)
                        .build());
        this.mockMvc.perform(
                post("/api/v1/enquirylog/get/search/active")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(searchRequest))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.responseObject.searchResult.length()").value(2))
                .andExpect(jsonPath("$.responseObject.searchResult[*].enquiryNo", contains("ENQ002", "ENQ001")));
    }

    @Test
    @WithMockUser(username = "test_user", roles = {"SALE.ENQUIRY.CREATE", "SALE.ENQUIRY.LIST"})
    public void should_search_enquiry_by_sales_coordinator() throws Exception {
        DateTime receivedOn = new DateTime();
        createEnquiries(receivedOn, "ENQ001", "ENQ002", "ENQ003");

        String searchRequest = toJson(
                new EnquiryLogSearchDto.Builder()
                        .withSearchSalesCoOrdinatorName("salesman")
                        .build());
        this.mockMvc.perform(
                post("/api/v1/enquirylog/get/search/active")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(searchRequest))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.responseObject.searchResult.length()").value(2))
                .andExpect(jsonPath("$.responseObject.searchResult[*].enquiryNo", contains("ENQ002", "ENQ001")));
    }

    @Test
    @WithMockUser(username = "test_user", roles = {"SALE.ENQUIRY.CREATE", "SALE.ENQUIRY.LIST"})
    public void should_search_enquiry_by_salesman() throws Exception {
        DateTime receivedOn = new DateTime();
        createEnquiries(receivedOn, "ENQ001", "ENQ002", "ENQ003");

        String searchRequest = toJson(
                new EnquiryLogSearchDto.Builder()
                        .withSearchSalesmanName("salesman")
                        .build());
        this.mockMvc.perform(
                post("/api/v1/enquirylog/get/search/active")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(searchRequest))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.responseObject.searchResult.length()").value(2))
                .andExpect(jsonPath("$.responseObject.searchResult[*].enquiryNo", contains("ENQ002", "ENQ001")));
    }

    @Test
    @WithMockUser(username = "test_user", roles = {"SALE.ENQUIRY.CREATE", "SALE.ENQUIRY.LIST"})
    public void should_search_enquiry_by_logged_by() throws Exception {
        DateTime receivedOn = new DateTime();
        createEnquiries(receivedOn, "ENQ001", "ENQ002", "ENQ003");

        String searchRequest = toJson(
                new EnquiryLogSearchDto.Builder()
                        .withSearchLoggedByName("employee")
                        .build());
        this.mockMvc.perform(
                post("/api/v1/enquirylog/get/search/active")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(searchRequest))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.responseObject.searchResult.length()").value(2))
                .andExpect(jsonPath("$.responseObject.searchResult[*].enquiryNo", contains("ENQ002", "ENQ001")));
    }

    @Test
    @WithMockUser(username = "test_user", roles = {"SALE.ENQUIRY.CREATE", "SALE.ENQUIRY.LIST"})
    public void should_search_enquiry_by_logged_on() throws Exception {
        DateTime today = new DateTime();
        createEnquiries(today, "ENQ001", "ENQ002", "ENQ003");
        DateRange loggedOnRange = toDateRange(today, today);
        String searchRequest = toJson(
                new EnquiryLogSearchDto.Builder()
                        .withSearchLoggedOnDate(loggedOnRange)
                        .build());
        this.mockMvc.perform(
                post("/api/v1/enquirylog/get/search/active")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(searchRequest))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.responseObject.searchResult.length()").value(2))
                .andExpect(jsonPath("$.responseObject.searchResult[*].enquiryNo", contains("ENQ002", "ENQ001")));
    }

    @Test
    @WithMockUser(username = "test_user", roles = {"SALE.ENQUIRY.CREATE", "SALE.ENQUIRY.LIST"})
    public void should_search_enquiry_by_multiple_criterias() throws Exception {
        DateTime today = new DateTime();
        createEnquiries(today, "ENQ001", "ENQ002", "ENQ003");
        DateTime quoteByDate = today.plusDays(3);
        DateRange quoteByRange = toDateRange(quoteByDate, quoteByDate);
        DateRange loggedOnRange = toDateRange(today, today);
        String searchRequest = toJson(
                new EnquiryLogSearchDto.Builder()
                        .withSearchCustomerName("party")
                        .withSearchLoggedOnDate(loggedOnRange)
                        .withSearchQuoteBy(quoteByRange)
                        .build());
        this.mockMvc.perform(
                post("/api/v1/enquirylog/get/search/active")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(searchRequest))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.responseObject.searchResult.length()").value(1))
                .andExpect(jsonPath("$.responseObject.searchResult[*].enquiryNo", contains("ENQ001")));
    }

    @Test
    @WithMockUser(username = "test_user", roles = {"SALE.ENQUIRY.CREATE"})
    public void should_save_an_enquiry_when_received_date_is_same_as_quote_by_date() throws Exception {

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        Date enquiryReceivedOn = simpleDateFormat.parse("2017-03-09 16:45:50");
        Date enquiryQuoteBy = simpleDateFormat.parse("2017-03-09 00:00:00");

        EnquiryLogRequest enquiryRequest = createEnquiryRequest(enquiryReceivedOn,
                                                                enquiryQuoteBy,
                                                                EnquiryStatus.Active,
                                                                toPortMaster,
                                                                destinationPortMaster);

        String request =  gson.toJson(enquiryRequest);
        given(sequenceGeneratorService.getSequence(SequenceType.ENQUERY)).willReturn("ENQ001");
        when(appUtil.setDesc(Matchers.any())).then(invocation -> invocation.getArguments()[0]);
        MvcResult createEnquiryResult = makeCreateEnquiryRequest(request)
                .andDo(print())
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.responseObject.id").value(notNullValue()))
                .andExpect(jsonPath("$.responseObject.enquiryNo").value("ENQ001"))
                .andReturn();

        MockHttpServletResponse response = createEnquiryResult.getResponse();
        Map<String, Object> enquiry = fromJson(response.getContentAsString());
        long enquiryId = getIdFromResponse(enquiry);
    }


    private DateRange toDateRange(DateTime start, DateTime end) {
        String startDateText = dateTimeFormatter.print(start.withHourOfDay(0).withMinuteOfHour(0).withSecondOfMinute(0));
        String endDateText = dateTimeFormatter.print(end.withHourOfDay(23).withMinuteOfHour(59).withSecondOfMinute(59));
        return new DateRange(startDateText, endDateText);
    }

    private void createEnquiries(DateTime receivedOn, String enquiryNo, String... enquiryNos) {
        String enquiry1 = toJson(createEnquiryRequest(
                receivedOn.toDate(),
                receivedOn.plusDays(3).toDate(),
                EnquiryStatus.Active,
                toPortMaster,
                destinationPortMaster
        ));
        String enquiry2 = toJson(createEnquiryRequest(
                receivedOn.minusDays(3).toDate(),
                receivedOn.plusDays(5).toDate(),
                EnquiryStatus.Active,
                destinationPortMaster,
                toPortMaster
        ));
        String enquiry3 = toJson(createEnquiryRequest(
                receivedOn.toDate(),
                receivedOn.plusDays(10).toDate(),
                EnquiryStatus.Cancel,
                toPortMaster,
                destination2PortMaster
        ));
        given(sequenceGeneratorService.getSequence(SequenceType.ENQUERY)).willReturn(enquiryNo, enquiryNos);
        when(appUtil.setDesc(Matchers.any())).then(invocation -> invocation.getArguments()[0]);
        Stream.of(enquiry1, enquiry2, enquiry3).forEach(enquiry -> {
            try {
                makeCreateEnquiryRequest(enquiry);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        });
    }

    private ResultActions makeCreateEnquiryRequest(String request) throws Exception {
        return this.mockMvc.perform(
                post("/api/v1/enquirylog/create")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(request));
    }

    private ResultActions makeGetEnquiryByIdRequest(long enquiryId, int version) throws Exception {
        return this.mockMvc.perform(get("/api/v1/enquirylog/get/id/{id}", enquiryId))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.responseObject.versionLock").value(version));
    }

    private String createEnquiryRequestJson() {
        EnquiryLogRequest enquiry = createEnquiryRequest(null, new Date(), null, 0);
        return gson.toJson(enquiry);
    }

    private EnquiryLogRequest createEnquiryRequest(Long id, Date quoteByDate, Long enquiryDetailId, int version) {
        EnquiryDetail enquiryDetail = new EnquiryDetail();
        enquiryDetail.setId(enquiryDetailId);
        enquiryDetail.setVersionLock(version);
        enquiryDetail.setClearance(YesNo.No);
        enquiryDetail.setHazardous(YesNo.No);
        enquiryDetail.setImportExport(ImportExport.Export);
        enquiryDetail.setToPortMaster(toPortMaster);
        enquiryDetail.setFromPortMaster(destinationPortMaster);
        enquiryDetail.setTosMaster(tosMaster);
        enquiryDetail.setServiceMaster(serviceMaster);
        enquiryDetail.setDimensionUnit(DimensionUnit.INCHORPOUNDS);

        return new EnquiryLogRequest.Builder()
                .withId(id)
                .withPartyMaster(partialPartyMaster())
                .withQuoteBy(quoteByDate)
                .withReceivedOn(new DateTime().minusDays(1).toDate())
                .withStatus(EnquiryStatus.Active)
                .withSalesCoOrdinator(partialSalesman())
                .withEnquiryDetailList(Collections.singletonList(enquiryDetail))
                .withLoggedBy(partialEmployeeMaster())
                .withCountryMaster(partialCountryMaster())
                .withCompanyMaster(partialCompanyMaster())
                .withSalesman(partialSalesman())
                .withLocationMaster(partialLocationMaster())
                .build();
    }

    private EnquiryLogRequest createEnquiryRequest(Date receivedOn, Date quoteBy, EnquiryStatus status, PortMaster toPortMaster, PortMaster destinationPortMaster) {
        EnquiryDetail enquiryDetail = new EnquiryDetail();
        enquiryDetail.setClearance(YesNo.No);
        enquiryDetail.setHazardous(YesNo.No);
        enquiryDetail.setImportExport(ImportExport.Export);
        enquiryDetail.setToPortMaster(toPortMaster);
        enquiryDetail.setFromPortMaster(destinationPortMaster);
        enquiryDetail.setTosMaster(tosMaster);
        enquiryDetail.setServiceMaster(serviceMaster);
        enquiryDetail.setDimensionUnit(DimensionUnit.INCHORPOUNDS);

        return new EnquiryLogRequest.Builder()
                .withPartyMaster(partialPartyMaster())
                .withQuoteBy(quoteBy)
                .withReceivedOn(receivedOn)
                .withStatus(status)
                .withSalesCoOrdinator(partialSalesman())
                .withEnquiryDetailList(Collections.singletonList(enquiryDetail))
                .withLoggedBy(partialEmployeeMaster())
                .withCountryMaster(partialCountryMaster())
                .withCompanyMaster(partialCompanyMaster())
                .withSalesman(partialSalesman())
                .withLocationMaster(partialLocationMaster())
                .build();
    }

}
