package com.efreightsuite.controller;

import java.util.Date;
import java.util.Map;

import com.efreightsuite.dto.QuotationApproveDto;
import com.efreightsuite.enumeration.Approved;
import com.efreightsuite.enumeration.EnquiryStatus;
import com.efreightsuite.enumeration.SequenceType;
import com.efreightsuite.enumeration.WhoRouted;
import com.efreightsuite.model.AddressMaster;
import com.efreightsuite.model.CompanyMaster;
import com.efreightsuite.model.CountryMaster;
import com.efreightsuite.model.EmployeeMaster;
import com.efreightsuite.model.EnquiryLog;
import com.efreightsuite.model.LocationMaster;
import com.efreightsuite.model.PartyMaster;
import com.efreightsuite.repository.EnquiryLogRepository;
import org.junit.Test;
import org.mockito.ArgumentCaptor;

import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MvcResult;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class QuotationControllerTests extends BaseRestApiTest {

    @MockBean
    private EnquiryLogRepository enquiryLogRepository;

    @Test
    @WithMockUser(username = "test_user", roles = {"SALE.QUOTATION.CREATE", "SALE.QUOTATION.VIEW", "SALE.QUOTATION.MODIFY", "SALE.QUOTATION.APPROVE.MODIFY", "SALE.QUOTATION.LOST.MODIFY"})
    public void should_save_get_and_update_a_direct_quotation() throws Exception {
        given(sequenceGeneratorService.getSequenceValueByType(SequenceType.QUOTATION)).willReturn("Q001");
        String createRequest = createDirectQuotationRequestJson();
        MvcResult createQuotationResult = this.mockMvc.perform(
                post("/api/v1/quotation/create")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(createRequest))
                .andDo(print())
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.responseObject.id").value(notNullValue()))
                .andExpect(jsonPath("$.responseObject.quotationNo").value("Q001"))
                .andReturn();

        MockHttpServletResponse response = createQuotationResult.getResponse();
        Map<String, Object> quotation = fromJson(response.getContentAsString());
        long quotationId = getIdFromResponse(quotation);

        this.mockMvc.perform(get("/api/v1/quotation/get/id/{id}", quotationId))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.responseObject.quotationNo").value("Q001"))
                .andExpect(jsonPath("$.responseObject.versionLock").value(0))
                .andReturn();

        this.mockMvc.perform(get("/api/v1/quotation/get/quotationNo/Q001"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.responseObject.id").value(notNullValue()))
                .andExpect(jsonPath("$.responseObject.versionLock").value(0))
                .andReturn();

        // Approve a quotation
        QuotationApproveDto quotationApproveDto = new QuotationApproveDto(quotationId, Approved.Approved, employee.getId());
        String approvalRequest = gson.toJson(quotationApproveDto);
        this.mockMvc.perform(
                post("/api/v1/quotation/statusupdate")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(approvalRequest))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.responseObject").value(quotationId));

        // Update quotation
        MvcResult getResult = this.mockMvc.perform(get("/api/v1/quotation/get/id/{id}", quotationId))
                .andReturn();
        quotation = fromJson(getResult.getResponse().getContentAsString());
        int versionLock = ((Double) ((Map<String, Object>) quotation.get("responseObject")).get("versionLock")).intValue();

        QuotationRequest updateRequest = createQuotationRequest();
        updateRequest.setId(quotationId);
        updateRequest.setVersionLock(versionLock);
        updateRequest.setQuotationNo("Q001");
        updateRequest.setFooterNote("Updated Footer Note");
        updateRequest.setHeaderNote("Updated Header Note");

        String updatedRequestJson = gson.toJson(updateRequest);

        this.mockMvc.perform(
                post("/api/v1/quotation/update")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(updatedRequestJson))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.responseObject.id").value(quotationId))
                .andExpect(jsonPath("$.responseObject.quotationNo").value("Q001"));

        this.mockMvc.perform(get("/api/v1/quotation/get/id/{id}", quotationId))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.responseObject.quotationNo").value("Q001"))
                .andExpect(jsonPath("$.responseObject.versionLock").value(2))
                .andExpect(jsonPath("$.responseObject.headerNote").value("Updated Header Note"))
                .andExpect(jsonPath("$.responseObject.footerNote").value("Updated Footer Note"));
    }

    @Test
    @WithMockUser(username = "test_user", roles = {"SALE.QUOTATION.CREATE"})
    public void should_create_a_quotation_with_enquiry() throws Exception {
        given(sequenceGeneratorService.getSequenceValueByType(SequenceType.QUOTATION)).willReturn("Q001");
        given(enquiryLogRepository.findByEnquiryNo("ENQ001")).willReturn(new EnquiryLog());
        String createRequest = createQuotationRequestWithEnquiryJson();
        this.mockMvc.perform(
                post("/api/v1/quotation/create")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(createRequest))
                .andDo(print())
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.responseObject.id").value(notNullValue()))
                .andExpect(jsonPath("$.responseObject.quotationNo").value("Q001"));
        ArgumentCaptor<EnquiryLog> argument = ArgumentCaptor.forClass(EnquiryLog.class);
        verify(enquiryLogRepository).save(argument.capture());
        EnquiryLog enquiryLog = argument.getValue();
        assertThat(enquiryLog.getStatus()).isEqualTo(EnquiryStatus.Gained);
        assertThat(enquiryLog.getQuotationNo()).isEqualTo("Q001");
    }

    private String createDirectQuotationRequestJson() {
        QuotationRequest request = createQuotationRequest();
        return gson.toJson(request);
    }

    private String createQuotationRequestWithEnquiryJson() {
        QuotationRequest request = createQuotationRequest();
        request.setEnquiryNo("ENQ001");
        return gson.toJson(request);
    }

    private QuotationRequest createQuotationRequest() {
        QuotationRequest request = new QuotationRequest();
        request.setCompanyMaster(partialCompanyMaster());
        request.setLocationMaster(partialLocationMaster());
        request.setCountryMaster(partialCountryMaster());
        request.setCustomer(partialPartyMaster());
        request.setCustomerAddress(addressMaster);
        request.setShipperAddress(addressMaster);
        request.setValidFrom(new Date());
        request.setValidTo(new Date());
        request.setLoggedOn(new Date());
        request.setLoggedBy(partialEmployeeMaster());
        request.setSalesCoordinator(partialEmployeeMaster());
        request.setHeaderNote("Header note");
        request.setFooterNote("Footer note");
        request.setWhoRouted(WhoRouted.Agent);
        return request;
    }

}


class QuotationRequest {

    private Long id;
    private int versionLock;
    private String quotationNo;
    private String enquiryNo;
    private PartyMaster customer;
    private CompanyMaster companyMaster;
    private LocationMaster locationMaster;
    private CountryMaster countryMaster;
    private AddressMaster customerAddress;
    private AddressMaster shipperAddress;
    private Date validFrom;
    private Date validTo;
    private Date loggedOn;
    private EmployeeMaster loggedBy;
    private EmployeeMaster salesCoordinator;
    private String headerNote;
    private String footerNote;
    private WhoRouted whoRouted;

    public void setId(Long id) {
        this.id = id;
    }

    public void setVersionLock(int versionLock) {
        this.versionLock = versionLock;
    }

    public void setQuotationNo(String quotationNo) {
        this.quotationNo = quotationNo;
    }

    public void setEnquiryNo(String enquiryNo) {
        this.enquiryNo = enquiryNo;
    }

    public void setCustomer(PartyMaster customer) {
        this.customer = customer;
    }

    public void setCompanyMaster(CompanyMaster companyMaster) {
        this.companyMaster = companyMaster;
    }

    public void setLocationMaster(LocationMaster locationMaster) {
        this.locationMaster = locationMaster;
    }

    public void setCountryMaster(CountryMaster countryMaster) {
        this.countryMaster = countryMaster;
    }

    public void setCustomerAddress(AddressMaster customerAddress) {
        this.customerAddress = customerAddress;
    }

    public void setShipperAddress(AddressMaster shipperAddress) {
        this.shipperAddress = shipperAddress;
    }

    public void setValidFrom(Date validFrom) {
        this.validFrom = validFrom;
    }

    public void setValidTo(Date validTo) {
        this.validTo = validTo;
    }

    public void setLoggedOn(Date loggedOn) {
        this.loggedOn = loggedOn;
    }

    public void setLoggedBy(EmployeeMaster loggedBy) {
        this.loggedBy = loggedBy;
    }

    public void setSalesCoordinator(EmployeeMaster salesCoordinator) {
        this.salesCoordinator = salesCoordinator;
    }

    public void setHeaderNote(String headerNote) {
        this.headerNote = headerNote;
    }

    public void setFooterNote(String footerNote) {
        this.footerNote = footerNote;
    }

    public void setWhoRouted(WhoRouted whoRouted) {
        this.whoRouted = whoRouted;
    }
}
