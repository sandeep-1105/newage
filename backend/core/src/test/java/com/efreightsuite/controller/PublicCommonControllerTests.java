package com.efreightsuite.controller;

import java.util.Arrays;

import com.efreightsuite.dto.SearchRequest;
import com.efreightsuite.enumeration.LovStatus;
import com.efreightsuite.model.common.CommonCityMaster;
import com.efreightsuite.model.common.CommonCountryMaster;
import com.efreightsuite.model.common.CommonRegionMaster;
import com.efreightsuite.model.common.CommonStateMaster;
import com.efreightsuite.repository.common.CommonCityMasterRepository;
import com.efreightsuite.repository.common.CommonCountryMasterRepository;
import com.efreightsuite.repository.common.CommonRegionMasterRepository;
import com.efreightsuite.repository.common.CommonStateMasterRepository;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class PublicCommonControllerTests extends BaseRestApiTest {

    @Autowired
    private CommonCountryMasterRepository countryRepository;

    @Autowired
    private CommonCityMasterRepository cityRepository;

    @Autowired
    private CommonStateMasterRepository stateRepository;

    @Autowired
    private CommonRegionMasterRepository regionRepository;

    @Before
    public void setupBaseTestData() throws Exception {
        CommonCountryMaster india = countryRepository.save(new CommonCountryMaster("India", "IN", LovStatus.Active));
        CommonCountryMaster uae = countryRepository.save(new CommonCountryMaster("United Arab Emirates", "UAE", LovStatus.Active));
        countryRepository.save(
                Arrays.asList(
                        new CommonCountryMaster("United States of America", "USA", LovStatus.Active),
                        new CommonCountryMaster("Pakistan", "PAK", LovStatus.Active)
                ));

        cityRepository.save(
                Arrays.asList(
                        new CommonCityMaster("GGN", "Gurgaon", LovStatus.Active),
                        new CommonCityMaster("DXB", "Dubai", LovStatus.Active),
                        new CommonCityMaster("MUM", "Mumbai", LovStatus.Active)
                ));

        stateRepository.save(
                Arrays.asList(
                        new CommonStateMaster("HAR", "Haryana", "IND", india, LovStatus.Active),
                        new CommonStateMaster("ADH", "Abu Dhabi", "UAE", uae, LovStatus.Active),
                        new CommonStateMaster("MHR", "Maharashtra", "IND", india, LovStatus.Active)
                )
        );

        regionRepository.save(
                Arrays.asList(
                        new CommonRegionMaster("53", "Australia and New Zealand", LovStatus.Active),
                        new CommonRegionMaster("7", "Eastern Europe", LovStatus.Active),
                        new CommonRegionMaster("29", "Caribbean", LovStatus.Active)
                ));
    }

    @After
    public void tearDown() throws Exception {
        stateRepository.deleteAll();
        cityRepository.deleteAll();
        countryRepository.deleteAll();
        regionRepository.deleteAll();
    }

    @Test
    public void should_return_result_when_common_country_matches_search_query() throws Exception {
        this.mockMvc.perform(
                post("/public/api/v1/common/countrymaster/get/search/keyword")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(gson.toJson(new SearchRequest("Ind"))))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.responseObject.totalRecord").value(1))
                .andExpect(jsonPath("$.responseObject.searchResult").isArray())
                .andExpect(jsonPath("$.responseObject.searchResult").isNotEmpty());
    }

    @Test
    public void should_return_empty_result_when_common_country_does_not_match_search_query() throws Exception {
        this.mockMvc.perform(
                post("/public/api/v1/common/countrymaster/get/search/keyword")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(gson.toJson(new SearchRequest("XYZ", 0, 10))))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.responseObject.totalRecord").value(0))
                .andExpect(jsonPath("$.responseObject.searchResult").isArray())
                .andExpect(jsonPath("$.responseObject.searchResult").isEmpty());
    }

    @Test
    public void should_return_common_city_for_valid_search_request() throws Exception {
        this.mockMvc.perform(
                post("/public/api/v1/common/citymaster/get/search/keyword/-1/-1")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(gson.toJson(new SearchRequest("Gur"))))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.responseObject.totalRecord").value(1))
                .andExpect(jsonPath("$.responseObject.searchResult").isArray())
                .andExpect(jsonPath("$.responseObject.searchResult").isNotEmpty());
    }

    @Test
    public void should_return_empty_result_when_city_does_not_match_search_query() throws Exception {
        this.mockMvc.perform(
                post("/public/api/v1/common/citymaster/get/search/keyword/-1/-1")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(gson.toJson(new SearchRequest("XYZ", 0, 10))))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.responseObject.totalRecord").value(0))
                .andExpect(jsonPath("$.responseObject.searchResult").isArray())
                .andExpect(jsonPath("$.responseObject.searchResult").isEmpty());
    }

    @Test
    public void should_return_common_state_for_valid_search_request() throws Exception {
        this.mockMvc.perform(
                post("/public/api/v1/common/statemaster/get/search/keyword/-1")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(gson.toJson(new SearchRequest("Har"))))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.responseObject.totalRecord").value(1))
                .andExpect(jsonPath("$.responseObject.searchResult").isArray())
                .andExpect(jsonPath("$.responseObject.searchResult").isNotEmpty());
    }

    @Test
    public void should_return_empty_result_when_common_state_does_not_match_search_query() throws Exception {
        this.mockMvc.perform(
                post("/public/api/v1/common/statemaster/get/search/keyword/-1")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(gson.toJson(new SearchRequest("XYZ", 0, 10))))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.responseObject.totalRecord").value(0))
                .andExpect(jsonPath("$.responseObject.searchResult").isArray())
                .andExpect(jsonPath("$.responseObject.searchResult").isEmpty());
    }

    @Test
    public void should_return_common_region_for_valid_search_request() throws Exception {
        this.mockMvc.perform(
                post("/public/api/v1/common/regionmaster/get/search/keyword")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(gson.toJson(new SearchRequest("Aus"))))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.responseObject.totalRecord").value(1))
                .andExpect(jsonPath("$.responseObject.searchResult").isArray())
                .andExpect(jsonPath("$.responseObject.searchResult").isNotEmpty());
    }

    @Test
    public void should_return_empty_result_when_common_region_does_not_match_search_query() throws Exception {
        this.mockMvc.perform(
                post("/public/api/v1/common/regionmaster/get/search/keyword")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(gson.toJson(new SearchRequest("XYZ", 0, 10))))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.responseObject.totalRecord").value(0))
                .andExpect(jsonPath("$.responseObject.searchResult").isArray())
                .andExpect(jsonPath("$.responseObject.searchResult").isEmpty());
    }


}
