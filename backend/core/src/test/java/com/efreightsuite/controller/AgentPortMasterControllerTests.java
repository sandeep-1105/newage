package com.efreightsuite.controller;

import org.junit.After;
import org.junit.Test;
import org.springframework.security.test.context.support.WithMockUser;

import static org.hamcrest.CoreMatchers.notNullValue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class AgentPortMasterControllerTests extends BaseRestApiTest {

    @After
    public void tearDown() throws Exception {
        agentPortMasterRepository.deleteAll();
    }

    @Test
    @WithMockUser(username = "test_user")
    public void search_agent_by_portCode() throws Exception {

        System.out.println("party master id:"+agentPortMaster.getAgent().getId());
        this.mockMvc.perform(get("/api/v1/agentportmaster/get/agent/pc1"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.responseObject.id").value(notNullValue()))
                .andExpect(jsonPath("$.responseObject.versionLock").value(0))
                .andExpect(jsonPath("$.responseObject.partyCode").value(notNullValue()))
                .andReturn();
    }
}