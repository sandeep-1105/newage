package com.efreightsuite.controller;

import java.util.Arrays;
import java.util.Date;

import com.efreightsuite.dto.AesFilerSearchDto;
import com.efreightsuite.enumeration.LovStatus;
import com.efreightsuite.model.AesFilerMaster;
import com.efreightsuite.model.CountryMaster;
import com.efreightsuite.model.LocationMaster;
import com.efreightsuite.model.SystemTrack;
import com.efreightsuite.repository.AesFilerMasterRepository;
import com.efreightsuite.repository.CountryMasterRepository;
import com.efreightsuite.repository.LocationMasterRepository;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class AesFilerMasterControllerTests extends BaseRestApiTest {

    private static final String API_URL = "/api/v1/aesfilermaster/get/search";
    @Autowired
    private AesFilerMasterRepository repository;
    @Autowired
    private CountryMasterRepository countryMasterRepository;
    @Autowired
    private LocationMasterRepository locationMasterRepository;

    @Before
    public void setupBaseTestData() throws Exception {
        SystemTrack systemTrack = new SystemTrack("test_user", new Date(), "test_user", new Date());
        CountryMaster india = new CountryMaster("IN", "India","INR", LovStatus.Active, systemTrack);
        india = countryMasterRepository.save(india);
        CountryMaster pakistan = new CountryMaster("PAK", "Pakistan","PKR", LovStatus.Active, systemTrack);
        pakistan = countryMasterRepository.save(pakistan);

        LocationMaster gurgaon = new LocationMaster(
                "GGN",
                "Gurgaon",
                "IST",
                LovStatus.Active,
                "ddMMyyy",
                "ddMMyyy hh:mm:ss"
        );
        gurgaon = locationMasterRepository.save(gurgaon);
        LocationMaster islamabad = new LocationMaster(
                "ISL",
                "Islamabad",
                "IST",
                LovStatus.Active,
                "ddMMyyy",
                "ddMMyyy hh:mm:ss"
        );
        islamabad = locationMasterRepository.save(islamabad);
        AesFilerMaster aesFilerMaster1 = new AesFilerMaster("Test schema 1", "aes-123", "t-123", LovStatus.Active, systemTrack, india, gurgaon);
        AesFilerMaster aesFilerMaster2 = new AesFilerMaster("Test schema 2", "aes-1231", "t-1231", LovStatus.Active, systemTrack, pakistan, gurgaon);
        AesFilerMaster aesFilerMaster3 = new AesFilerMaster("Another test schema", "aes-456", "t-456", LovStatus.Active, systemTrack, pakistan, islamabad);
        this.repository.save(Arrays.asList(aesFilerMaster1, aesFilerMaster2, aesFilerMaster3));
    }

    @After
    public void tearDown() throws Exception {
        this.repository.deleteAll();
        this.countryMasterRepository.deleteAll();
        this.locationMasterRepository.deleteAll();
    }

    @Test
    @WithMockUser(username = "test_user", roles = "MASTER.GENERAL.AES_FILER.LIST")
    public void should_search_aes_filer_by_schema_name() throws Exception {
        String query = this.gson.toJson(new AesFilerSearchDto("test"));
        this.mockMvc.perform(
                post(API_URL)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(query))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.responseObject.searchResult").isArray())
                .andExpect(jsonPath("$.responseObject.searchResult.length()").value(2))
                .andExpect(jsonPath("$.responseObject.searchResult[*].schemaName")
                        .value(toJsonArray("Test schema 1", "Test schema 2")));
    }

    @Test
    @WithMockUser(username = "test_user", roles = "MASTER.GENERAL.AES_FILER.LIST")
    public void should_search_aes_filer_by_empty_schema_name() throws Exception {
        String query = this.gson.toJson(new AesFilerSearchDto(""));
        this.mockMvc.perform(
                post(API_URL)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(query))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.responseObject.searchResult").isArray())
                .andExpect(jsonPath("$.responseObject.searchResult.length()").value(3))
                .andExpect(jsonPath("$.responseObject.searchResult[*].schemaName")
                        .value(toJsonArray("Another test schema", "Test schema 1", "Test schema 2")));
    }

    @Test
    @WithMockUser(username = "test_user", roles = "MASTER.GENERAL.AES_FILER.LIST")
    public void should_search_aes_filer_by_schema_name_and_country_code() throws Exception {
        String query = this.gson.toJson(new AesFilerSearchDto("Test", "IN"));
        this.mockMvc.perform(
                post(API_URL)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(query))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.responseObject.searchResult").isArray())
                .andExpect(jsonPath("$.responseObject.searchResult.length()").value(1))
                .andExpect(jsonPath("$.responseObject.searchResult[*].schemaName")
                        .value(toJsonArray("Test schema 1")));
    }

    @Test
    @WithMockUser(username = "test_user", roles = "MASTER.GENERAL.AES_FILER.LIST")
    public void should_search_aes_filer_by_schema_name_and_location() throws Exception {
        String query = this.gson.toJson(new AesFilerSearchDto(null, null, "gurg"));
        this.mockMvc.perform(
                post(API_URL)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(query))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.responseObject.searchResult").isArray())
                .andExpect(jsonPath("$.responseObject.searchResult.length()").value(2))
                .andExpect(jsonPath("$.responseObject.searchResult[*].schemaName")
                        .value(toJsonArray("Test schema 1", "Test schema 2")));
    }

    @Test
    @WithMockUser(username = "test_user", roles = "MASTER.GENERAL.AES_FILER.LIST")
    public void should_search_aes_filer_by_schema_name_order_by_country_code() throws Exception {
        AesFilerSearchDto aesFilerSearchDto = new AesFilerSearchDto("test");
        aesFilerSearchDto.setOrderByType("DESC");
        aesFilerSearchDto.setSortByColumn("countryCode");
        String query = this.gson.toJson(aesFilerSearchDto);
        this.mockMvc.perform(
                post(API_URL)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(query))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.responseObject.searchResult").isArray())
                .andExpect(jsonPath("$.responseObject.searchResult.length()").value(2))
                .andExpect(jsonPath("$.responseObject.searchResult[*].schemaName")
                        .value(toJsonArray("Test schema 2", "Test schema 1")));
    }
}
