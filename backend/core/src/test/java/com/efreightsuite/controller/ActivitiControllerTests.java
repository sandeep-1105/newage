package com.efreightsuite.controller;

import com.efreightsuite.activiti.ActivitiAirExportSearchService;
import com.efreightsuite.activiti.ActivitiAirImportSearchService;
import com.efreightsuite.activiti.dto.AirExportImportTaskCountDto;
import com.efreightsuite.activiti.dto.AirExportTaskCountDto;
import com.efreightsuite.activiti.dto.AirImportTaskCountDto;
import com.efreightsuite.util.AppUtil;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Matchers;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class ActivitiControllerTests extends BaseRestApiTest {

    @MockBean
    protected AppUtil appUtil;

    @MockBean
    private
    ActivitiAirExportSearchService airExportSearchService;

    @MockBean
    private
    ActivitiAirImportSearchService airImportSearchService;

    private AirExportTaskCountDto createExportTaskCounts() {
        AirExportTaskCountDto airExportTaskCountDto = new AirExportTaskCountDto();
        airExportTaskCountDto.setEnquiryCount(2);
        airExportTaskCountDto.setQuotationCount(2);
        airExportTaskCountDto.setShipmentCount(2);
        airExportTaskCountDto.setShipmentYetToCargoReceiveCount(2);
        airExportTaskCountDto.setPickUpCount(2);
        airExportTaskCountDto.setDeliverToCfsOrDepotCount(2);
        airExportTaskCountDto.setAesOrCustomsCount(2);
        airExportTaskCountDto.setLinkToMasterCount(2);
        airExportTaskCountDto.setSignOffCount(2);
        airExportTaskCountDto.setAirlIneSubmissionCount(2);
        airExportTaskCountDto.setJobCompletionCount(2);
        airExportTaskCountDto.setInvoiceCount(2);
        airExportTaskCountDto.setJobCompletionCount(2);
        airExportTaskCountDto.setTotalTask(26);

        return airExportTaskCountDto;
    }

    private AirImportTaskCountDto createImportTaskCounts() {
        AirImportTaskCountDto airImportTaskCountDto = new AirImportTaskCountDto();
        airImportTaskCountDto.setEnquiryCount(2);
        airImportTaskCountDto.setQuotationCount(2);
        airImportTaskCountDto.setShipmentCount(2);
        airImportTaskCountDto.setPickUpCount(2);
        airImportTaskCountDto.setDeliverToCfsOrDepotCount(2);
        airImportTaskCountDto.setLinkToMasterCount(2);
        airImportTaskCountDto.setSignOffCount(2);
        airImportTaskCountDto.setAirlIneSubmissionCount(2);
        airImportTaskCountDto.setJobCompletionCount(2);
        airImportTaskCountDto.setCanAndInvoiceCount(2);
        airImportTaskCountDto.setAtdConfirmationCount(2);
        airImportTaskCountDto.setAtaConfirmationCount(2);
        airImportTaskCountDto.setCustomsCount(2);
        airImportTaskCountDto.setDeliveryOrderCount(2);
        airImportTaskCountDto.setClearanceCount(2);
        airImportTaskCountDto.setPodCount(2);
        airImportTaskCountDto.setTotalTask(32);

        return airImportTaskCountDto;
    }

    @Test
    @WithMockUser(username = "test_user")
    public void should_return_export_import_count() throws Exception {

        given(airExportSearchService.getAirExportCount()).willReturn(createExportTaskCounts());
        given(airImportSearchService.getAirImportCount()).willReturn(createImportTaskCounts());
        when(appUtil.setDesc(Matchers.any())).then(invocation -> invocation.getArguments()[0]);
        this.mockMvc.perform(get("/api/v1/activiti/airexportimport/getcount"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.airImportTaskCountDto.totalTask").value(32))
                .andExpect(jsonPath("$.airExportTaskCountDto.totalTask").value(26));
    }
}