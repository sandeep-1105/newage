package com.efreightsuite.controller;

import com.efreightsuite.dto.CitySearchDto;
import com.efreightsuite.dto.CountryMasterSearchDto;
import com.efreightsuite.dto.SearchRequest;
import com.efreightsuite.enumeration.LovStatus;
import com.efreightsuite.model.CityMaster;
import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;

import static org.junit.Assert.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


public class CityMasterControllerTests extends BaseRestApiTest {

    private static final String API_URL = "/api/v1/citymaster/get/search/keyword";

    @Test
    @WithMockUser(username = "test_user", roles = {"MASTER.GENERAL.CITY.LIST"})
    public void should_search_city_by_city_code() throws Exception {
        CitySearchDto request = new CitySearchDto("City 1", null);
        this.mockMvc.perform(
                post(API_URL)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(toJson(request)))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.responseObject.searchResult").isArray())
                .andExpect(jsonPath("$.responseObject.searchResult.length()").value(1))
                .andExpect(jsonPath("$.responseObject.searchResult[*].cityName")
                        .value(toJsonArray("City 1")));

    }

    @Test
    @WithMockUser(username = "test_user", roles = {"MASTER.GENERAL.CITY.LIST"})
    public void should_search_city_by_city_name() throws Exception {

        CitySearchDto request = new CitySearchDto("City", null);
        this.mockMvc.perform(
                post(API_URL)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(toJson(request)))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.responseObject.searchResult").isArray())
                .andExpect(jsonPath("$.responseObject.searchResult.length()").value(1))
                .andExpect(jsonPath("$.responseObject.searchResult[*].cityName")
                        .value(toJsonArray("City 1")));

    }

    @Test
    @WithMockUser(username = "test_user", roles = {"MASTER.GENERAL.CITY.LIST"})
    public void should_search_city_by_city_name_and_code() throws Exception {

        CitySearchDto request = new CitySearchDto("City", "cy1");
        this.mockMvc.perform(
                post(API_URL)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(toJson(request)))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.responseObject.searchResult").isArray())
                .andExpect(jsonPath("$.responseObject.searchResult.length()").value(1))
                .andExpect(jsonPath("$.responseObject.searchResult[*].cityName")
                        .value(toJsonArray("City 1")));

    }

    @Test
    @WithMockUser(username = "test_user", roles = {"MASTER.GENERAL.CITY.LIST"})
    public void should_search_city_by_city_name_code_and_state_name() throws Exception {

        cityMaster.setStateMaster(stateMaster);
        cityMasterRepository.save(cityMaster);

        CitySearchDto request = new CitySearchDto("City 1", "cy1", "State 1");
        this.mockMvc.perform(
                post(API_URL)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(toJson(request)))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.responseObject.searchResult").isArray())
                .andExpect(jsonPath("$.responseObject.searchResult.length()").value(1))
                .andExpect(jsonPath("$.responseObject.searchResult[*].cityName")
                        .value(toJsonArray("City 1")));

    }

    @Test
    @WithMockUser(username = "test_user", roles = {"MASTER.GENERAL.CITY.LIST"})
    public void should_search_city_by_city_name_code_state_and_country_name() throws Exception {

        cityMaster.setStateMaster(stateMaster);
        cityMaster.setCountryMaster(country);
        cityMasterRepository.save(cityMaster);

        CitySearchDto request = new CitySearchDto("City 1", "cy1", "State 1", "India");
        this.mockMvc.perform(
                post(API_URL)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(toJson(request)))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.responseObject.searchResult").isArray())
                .andExpect(jsonPath("$.responseObject.searchResult.length()").value(1))
                .andExpect(jsonPath("$.responseObject.searchResult[*].cityName")
                        .value(toJsonArray("City 1")));

    }

    @Test
    @WithMockUser(username = "test_user", roles = {"MASTER.GENERAL.CITY.LIST"})
    public void should_search_city_by_city_name_code_state_country_name_and_status() throws Exception {

        cityMaster.setStateMaster(stateMaster);
        cityMaster.setCountryMaster(country);
        cityMasterRepository.save(cityMaster);

        CitySearchDto request = new CitySearchDto("City 1", "cy1", "State 1", "India", LovStatus.Active.name());
        this.mockMvc.perform(
                post(API_URL)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(toJson(request)))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.responseObject.searchResult").isArray())
                .andExpect(jsonPath("$.responseObject.searchResult.length()").value(1))
                .andExpect(jsonPath("$.responseObject.searchResult[*].cityName")
                        .value(toJsonArray("City 1")));

    }

    @Test
    @WithMockUser(username = "test_user")
    public void should_search_all_city_by_keyword_and_country_id() throws Exception {

        cityMaster.setStateMaster(stateMaster);
        cityMaster.setCountryMaster(country);
        CityMaster cm = cityMasterRepository.save(cityMaster);
        Long countryId = cm.getCountryMaster().getId();
        SearchRequest request = new SearchRequest("cy1");
        this.mockMvc.perform(
                post(API_URL + "/"+String.valueOf(countryId))
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(toJson(request)))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.responseObject.searchResult").isArray())
                .andExpect(jsonPath("$.responseObject.searchResult.length()").value(1))
                .andExpect(jsonPath("$.responseObject.searchResult[*].cityName")
                        .value(toJsonArray("City 1")));

    }
}