package com.efreightsuite.controller;

import com.efreightsuite.dto.DateRange;
import com.efreightsuite.dto.LanguageDetailSearchReqDto;
import com.efreightsuite.dto.SearchRequest;
import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;

import java.text.SimpleDateFormat;
import java.util.Date;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


public class LanguageDetailControllerTest extends BaseRestApiTest {

    private static final String API_URL = "/api/v1/language/get/search";

    @Test
    @WithMockUser(username = "test_user")
    public void search_language_by_camelCase_keyword() throws Exception {
        SearchRequest request = new SearchRequest("Eng");
        this.mockMvc.perform(
                post(API_URL + "/keyword")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(toJson(request)))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.responseObject.searchResult").isArray())
                .andExpect(jsonPath("$.responseObject.searchResult.length()").value(1))
                .andExpect(jsonPath("$.responseObject.searchResult[*].languageName")
                        .value(toJsonArray("English")));
    }

    @Test
    @WithMockUser(username = "test_user")
    public void search_language_by_lower_case_keyword() throws Exception {
        SearchRequest request = new SearchRequest("eng");
        this.mockMvc.perform(
                post(API_URL + "/keyword")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(toJson(request)))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.responseObject.searchResult").isArray())
                .andExpect(jsonPath("$.responseObject.searchResult.length()").value(1))
                .andExpect(jsonPath("$.responseObject.searchResult[*].languageName")
                        .value(toJsonArray("English")));
    }

    @Test
    @WithMockUser(username = "test_user", roles = {"SETUP.ERROR_MESSAGE.LIST"})
    public void search_language_by_language_name() throws Exception {
        LanguageDetailSearchReqDto request = new LanguageDetailSearchReqDto("English");
        this.mockMvc.perform(
                post(API_URL)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(toJson(request)))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.responseObject.searchResult").isArray())
                .andExpect(jsonPath("$.responseObject.searchResult.length()").value(1))
                .andExpect(jsonPath("$.responseObject.searchResult[*].language.languageName")
                        .value(toJsonArray("English")));
    }

    @Test
    @WithMockUser(username = "test_user", roles = {"SETUP.ERROR_MESSAGE.LIST"})
    public void search_language_by_language_code_name() throws Exception {
        LanguageDetailSearchReqDto request = new LanguageDetailSearchReqDto("ERR8845","English");
        this.mockMvc.perform(
                post(API_URL)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(toJson(request)))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.responseObject.searchResult").isArray())
                .andExpect(jsonPath("$.responseObject.searchResult.length()").value(1))
                .andExpect(jsonPath("$.responseObject.searchResult[*].language.languageName")
                        .value(toJsonArray("English")));
    }

    @Test
    @WithMockUser(username = "test_user", roles = {"SETUP.ERROR_MESSAGE.LIST"})
    public void search_language_by_language_code_desc_name_status_group_and_date() throws Exception {
        LanguageDetailSearchReqDto request = getPartialLanguageSearchDto();
        this.mockMvc.perform(
                post(API_URL)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(toJson(request)))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.responseObject.searchResult").isArray())
                .andExpect(jsonPath("$.responseObject.searchResult.length()").value(1))
                .andExpect(jsonPath("$.responseObject.searchResult[*].language.languageName")
                        .value(toJsonArray("English")));
    }

    private LanguageDetailSearchReqDto getPartialLanguageSearchDto() {
        DateRange dateRange = new DateRange(getFormattedDate(), getFormattedDate());
        return new LanguageDetailSearchReqDto.Builder("ERR8845", "city is blocked", "English")
                .withSearchQcVerify("No")
                .withSearchGroupName("Enquiry")
                .withLastUpdatedOn(dateRange)
                .build();
    }

    private String getFormattedDate() {
        Date date = new Date();
        SimpleDateFormat dmyFormat = new SimpleDateFormat("yyyy-MM-dd");
        String formattedDate = dmyFormat.format(date);
        return formattedDate;
    }
}