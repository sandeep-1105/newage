package com.efreightsuite.controller;

import com.efreightsuite.dto.LocationSearchDto;
import com.efreightsuite.dto.SearchRequest;
import com.efreightsuite.enumeration.LovStatus;
import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class LocationMasterControllerTests extends BaseRestApiTest {

    private static final String API_URL = "/api/v1/locationmaster/get/search";

    @Test
    @WithMockUser(username = "test_user")
    public void search_location_by_keyword() throws Exception {
        SearchRequest request = new SearchRequest("GGN");
        this.mockMvc.perform(
                post(API_URL + "/keyword")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(toJson(request)))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.responseObject.searchResult").isArray())
                .andExpect(jsonPath("$.responseObject.searchResult.length()").value(1))
                .andExpect(jsonPath("$.responseObject.searchResult[*].locationName")
                        .value(toJsonArray("Gurgaon")));
    }

    @Test
    @WithMockUser(username = "test_user")
    public void search_location_by_country_id() throws Exception {
        SearchRequest request = new SearchRequest("GGN");
        Long countryId = locationMasterRepository.findAll().get(0).getCountryMaster().getId();
        this.mockMvc.perform(
                post(API_URL + "/keyword/" + String.valueOf(countryId))
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(toJson(request)))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.responseObject.searchResult").isArray())
                .andExpect(jsonPath("$.responseObject.searchResult.length()").value(1))
                .andExpect(jsonPath("$.responseObject.searchResult[*].locationName")
                        .value(toJsonArray("Gurgaon")));
    }

    @Test
    @WithMockUser(username = "test_user")
    public void search_location_by_company_id() throws Exception {
        SearchRequest request = new SearchRequest("GGN");
        Long companyId = locationMasterRepository.findAll().get(0).getCompanyMaster().getId();
        this.mockMvc.perform(
                post(API_URL + "/keyword/" + String.valueOf(companyId))
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(toJson(request)))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.responseObject.searchResult").isArray())
                .andExpect(jsonPath("$.responseObject.searchResult.length()").value(1))
                .andExpect(jsonPath("$.responseObject.searchResult[*].locationName")
                        .value(toJsonArray("Gurgaon")));
    }

    @Test
    @WithMockUser(username = "test_user", roles = {"SETUP.LOCATION_MASTER.LIST"})
    public void search_location_by_location_code_name_branch_company_and_country() throws Exception {
        LocationSearchDto request = new LocationSearchDto("GGN", "Gurgaon", "test branch", "Xebia", "India", LovStatus.Active.name());
        this.mockMvc.perform(
                post(API_URL)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(toJson(request)))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.responseObject.searchResult").isArray())
                .andExpect(jsonPath("$.responseObject.searchResult.length()").value(1))
                .andExpect(jsonPath("$.responseObject.searchResult[*].locationName")
                        .value(toJsonArray("Gurgaon")));
    }
}