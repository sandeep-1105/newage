package com.efreightsuite.controller;

import com.efreightsuite.dto.CountryMasterSearchDto;
import com.efreightsuite.dto.SearchRequest;
import com.efreightsuite.enumeration.LovStatus;
import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


public class CountryMasterControllerTests extends BaseRestApiTest {

    private static final String API_URL = "/api/v1/countrymaster/get/search";

    @Test
    @WithMockUser(username = "test_user", roles = {"MASTER.GENERAL.COUNTRY.LIST"})
    public void should_search_country_by_countryCode() throws Exception {

        CountryMasterSearchDto request = new CountryMasterSearchDto("IN");
        this.mockMvc.perform(
                post(API_URL)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(toJson(request)))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.responseObject.searchResult").isArray())
                .andExpect(jsonPath("$.responseObject.searchResult.length()").value(1))
                .andExpect(jsonPath("$.responseObject.searchResult[*].countryName")
                        .value(toJsonArray("India")));
    }

    @Test
    @WithMockUser(username = "test_user", roles = {"MASTER.GENERAL.COUNTRY.LIST"})
    public void should_search_country_by_country_code_and_name() throws Exception {

        CountryMasterSearchDto request = new CountryMasterSearchDto("India", "IN");
        this.mockMvc.perform(
                post(API_URL)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(toJson(request)))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.responseObject.searchResult").isArray())
                .andExpect(jsonPath("$.responseObject.searchResult.length()").value(1))
                .andExpect(jsonPath("$.responseObject.searchResult[*].countryName")
                        .value(toJsonArray("India")));
    }

    @Test
    @WithMockUser(username = "test_user", roles = {"MASTER.GENERAL.COUNTRY.LIST"})
    public void should_search_country_by_country_code_name_and_currency_code() throws Exception {

        CountryMasterSearchDto request = new CountryMasterSearchDto("India", "IN", "INR");
        this.mockMvc.perform(
                post(API_URL)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(toJson(request)))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.responseObject.searchResult").isArray())
                .andExpect(jsonPath("$.responseObject.searchResult.length()").value(1))
                .andExpect(jsonPath("$.responseObject.searchResult[*].countryName")
                        .value(toJsonArray("India")));
    }

    @Test
    @WithMockUser(username = "test_user", roles = {"MASTER.GENERAL.COUNTRY.LIST"})
    public void should_give_no_result_when_search_country_by_invalid_code_valid_name_and_currency_code() throws Exception {

        CountryMasterSearchDto request = new CountryMasterSearchDto("India", "INNR", "INR");
        this.mockMvc.perform(
                post(API_URL)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(toJson(request)))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.responseObject.searchResult").isArray())
                .andExpect(jsonPath("$.responseObject.searchResult.length()").value(0));

    }

    @Test
    @WithMockUser(username = "test_user", roles = {"MASTER.GENERAL.COUNTRY.LIST"})
    public void should_search_country_by_country_code_name_currency_and_status() throws Exception {

        CountryMasterSearchDto request = new CountryMasterSearchDto.Builder("India", "IN", "INR").withSearchStatus(LovStatus.Active).build();
        this.mockMvc.perform(
                post(API_URL)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(toJson(request)))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.responseObject.searchResult").isArray())
                .andExpect(jsonPath("$.responseObject.searchResult.length()").value(1))
                .andExpect(jsonPath("$.responseObject.searchResult[*].countryName")
                        .value(toJsonArray("India")));
    }

    @Test
    @WithMockUser(username = "test_user")
    public void should_search_country_by_keyword() throws Exception {

        SearchRequest request = new SearchRequest("Ind");
        this.mockMvc.perform(
                post(API_URL + "/keyword")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(toJson(request)))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.responseObject.searchResult").isArray())
                .andExpect(jsonPath("$.responseObject.searchResult.length()").value(1))
                .andExpect(jsonPath("$.responseObject.searchResult[*].countryName")
                        .value(toJsonArray("India")));

    }

    @Test
    @WithMockUser(username = "test_user")
    public void should_search_country_by_keyword_not_in_list() throws Exception {

        SearchRequest request = new SearchRequest("IN");
        this.mockMvc.perform(
                post(API_URL + "/exclude")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(toJson(request)))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.responseObject.searchResult").isArray())
                .andExpect(jsonPath("$.responseObject.searchResult.length()").value(1))
                .andExpect(jsonPath("$.responseObject.searchResult[*].countryName")
                        .value(toJsonArray("India")));
    }
}