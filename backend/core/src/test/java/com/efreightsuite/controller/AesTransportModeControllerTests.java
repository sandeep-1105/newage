package com.efreightsuite.controller;

import java.util.Arrays;
import java.util.Date;

import com.efreightsuite.dto.SearchRequest;
import com.efreightsuite.enumeration.LovStatus;
import com.efreightsuite.model.AesTransportMode;
import com.efreightsuite.model.SystemTrack;
import com.efreightsuite.repository.AesTransportModeRepository;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class AesTransportModeControllerTests extends BaseRestApiTest {

    private static final String API_URL = "/api/v1/aestransport/get/search/keyword";

    @Autowired
    private AesTransportModeRepository repository;

    @Before
    public void setUp() throws Exception {
        SystemTrack systemTrack = new SystemTrack("test_user", new Date(), "test_user", new Date());
        this.repository.save(
                Arrays.asList(
                        new AesTransportMode("Transport 1", LovStatus.Active, systemTrack),
                        new AesTransportMode("Transport 2", LovStatus.Active, systemTrack),
                        new AesTransportMode("Another Transport 3", LovStatus.Hide, systemTrack)
                ));
    }

    @After
    public void tearDown() throws Exception {
        this.repository.deleteAll();
    }

    @Test
    @WithMockUser(username = "test_user")
    public void should_search_bank_keyword() throws Exception {
        String query = this.gson.toJson(new SearchRequest("trans"));
        this.mockMvc.perform(
                post(API_URL)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(query))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.responseObject.searchResult").isArray())
                .andExpect(jsonPath("$.responseObject.searchResult.length()").value(2))
                .andExpect(jsonPath("$.responseObject.searchResult[*].aesTransportName")
                        .value(toJsonArray("Transport 1", "Transport 2")));
    }

    @Test
    @WithMockUser(username = "test_user")
    public void should_search_bank_with_empty_keyword() throws Exception {
        String query = this.gson.toJson(new SearchRequest(""));
        this.mockMvc.perform(
                post(API_URL)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(query))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.responseObject.searchResult").isArray())
                .andExpect(jsonPath("$.responseObject.searchResult.length()").value(2))
                .andExpect(jsonPath("$.responseObject.searchResult[*].aesTransportName")
                        .value(toJsonArray("Transport 1", "Transport 2")));
    }
}
