package com.efreightsuite.controller;

import java.util.Collections;
import java.util.Date;
import java.util.Map;

import com.efreightsuite.dto.RoleDTO;
import com.efreightsuite.dto.UserCompanyDTO;
import com.efreightsuite.enumeration.UserStatus;
import com.efreightsuite.enumeration.YesNo;
import com.efreightsuite.model.UserProfile;
import com.efreightsuite.repository.UserProfileRepository;
import com.efreightsuite.util.PasswordUtils;
import org.assertj.core.api.Condition;
import org.junit.Test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MvcResult;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class UserProfileControllerTests extends BaseRestApiTest {

    @Autowired
    private UserProfileRepository userProfileRepository;

    @Test
    @WithMockUser(username = "test_user", roles = "SETUP.USER_CREATION.CREATE")
    public void should_create_user_profile_with_password_bcrypted() throws Exception {
        makeCreateUserProfileRequest(UserStatus.Active);

        UserProfile user = userProfileRepository.findByUserName("user");
        assertThat(user.getPassword()).is(new Condition<String>() {
            @Override
            public boolean matches(String password) {
                return PasswordUtils.isBcryptPassword(password);
            }
        });
    }

    @Test
    @WithMockUser(username = "test_user", roles = {"SETUP.USER_CREATION.CREATE", "SETUP.USER_CREATION.VIEW"})
    public void should_fetch_user_profile_by_id() throws Exception {
        MvcResult mvcResult = makeCreateUserProfileRequest(UserStatus.Active);
        Map<String, Object> response = fromJson(mvcResult.getResponse().getContentAsString());
        long userId = getIdFromResponse(response);

        this.mockMvc.perform(
                get("/api/v1/userprofile/get/id/{id}", userId))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.responseObject.userName").value("user"))
                .andExpect(jsonPath("$.responseObject.password", nullValue()));
    }

    @Test
    @WithMockUser(username = "test_user", roles = {"SETUP.USER_CREATION.CREATE", "SETUP.USER_CREATION.VIEW", "SETUP.USER_CREATION.MODIFY"})
    public void should_update_user_profile() throws Exception {
        MvcResult mvcResult = makeCreateUserProfileRequest(UserStatus.Active);
        Map<String, Object> response = fromJson(mvcResult.getResponse().getContentAsString());
        long userId = getIdFromResponse(response);

        MvcResult getUserProfileResult = this.mockMvc.perform(
                get("/api/v1/userprofile/get/id/{id}", userId))
                .andReturn();

        Map<String, Object> userProfile = getResponseObject(fromJson(getUserProfileResult.getResponse().getContentAsString()));
        userProfile.put("password", "password123");
        userProfile.put("userName", "user123");

        UserCompanyDTO userCompany = new UserCompanyDTO();
        userCompany.setEmployee(employee);
        userCompany.setYesNo(YesNo.Yes);

        userProfile.put("userCompanyList", Collections.singletonList(userCompany));
        userProfile.put("roleList", Collections.singletonList(new RoleDTO("tester")));

        this.mockMvc.perform(
                post("/api/v1/userprofile/update")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(toJson(userProfile)))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.responseObject.id").value(userId));

        this.mockMvc.perform(
                get("/api/v1/userprofile/get/id/{id}", userId))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.responseObject.userName").value("user123"))
                .andExpect(jsonPath("$.responseObject.password", nullValue()));

        UserProfile user = userProfileRepository.findOne(userId);
        assertThat(user.getPassword()).is(new Condition<String>() {
            @Override
            public boolean matches(String password) {
                return PasswordUtils.isBcryptPassword(password);
            }
        });
    }

    @Test
    @WithMockUser(username = "test_user", roles = {"SETUP.USER_CREATION.CREATE", "SETUP.USER_CREATION.VIEW", "SETUP.USER_CREATION.MODIFY"})
    public void should_activate_a_user() throws Exception {
        MvcResult mvcResult = makeCreateUserProfileRequest(UserStatus.To_be_activated);
        Map<String, Object> response = fromJson(mvcResult.getResponse().getContentAsString());
        long userId = getIdFromResponse(response);

        this.mockMvc.perform(
                post("/api/v1/userprofile/{id}/activate", userId)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());

        this.mockMvc.perform(
                get("/api/v1/userprofile/get/id/{id}", userId))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.responseObject.userName").value("user"))
                .andExpect(jsonPath("$.responseObject.status").value("Active"));

        UserProfile user = userProfileRepository.findOne(userId);
        assertThat(user.getPassword()).is(new Condition<String>() {
            @Override
            public boolean matches(String password) {
                return PasswordUtils.isBcryptPassword(password);
            }
        });
    }

    @Test
    @WithMockUser(username = "test_user", roles = {"SETUP.USER_CREATION.CREATE", "SETUP.USER_CREATION.VIEW", "SETUP.USER_CREATION.MODIFY"})
    public void should_deactivate_a_user() throws Exception {
        MvcResult mvcResult = makeCreateUserProfileRequest(UserStatus.Active);
        Map<String, Object> response = fromJson(mvcResult.getResponse().getContentAsString());
        long userId = getIdFromResponse(response);

        this.mockMvc.perform(
                post("/api/v1/userprofile/{id}/deactivate", userId)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());

        this.mockMvc.perform(
                get("/api/v1/userprofile/get/id/{id}", userId))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.responseObject.userName").value("user"))
                .andExpect(jsonPath("$.responseObject.status").value("Deactivate"));
    }

    private MvcResult makeCreateUserProfileRequest(UserStatus status) throws Exception {
        String createRequestJson = createRequest(status);
        return this.mockMvc.perform(
                post("/api/v1/userprofile/create")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(createRequestJson))
                .andDo(print())
                .andExpect(status().isCreated())
                .andReturn();
    }

    private String createRequest(UserStatus status) {
        UserProfile userProfile = new UserProfile();
        userProfile.setUserName("user");
        userProfile.setPassword("password");
        userProfile.setSelectedCompany(company);
        if (status == UserStatus.Active) {
            userProfile.setActivatedDate(new Date());
        }
        userProfile.setStatus(status);
        return gson.toJson(userProfile);
    }
}
