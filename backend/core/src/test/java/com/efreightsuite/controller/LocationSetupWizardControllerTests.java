package com.efreightsuite.controller;

import java.util.Collections;
import java.util.Date;
import java.util.Map;

import com.efreightsuite.enumeration.BooleanType;
import com.efreightsuite.enumeration.Measurement;
import com.efreightsuite.enumeration.SalutationType;
import com.efreightsuite.enumeration.common.LocationSetupSection;
import com.efreightsuite.model.common.LocationSetup;
import com.efreightsuite.model.common.LocationSetupService;
import com.efreightsuite.repository.common.LocationSetupRepository;
import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;

import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class LocationSetupWizardControllerTests extends BaseRestApiTest {

    @Autowired
    private LocationSetupRepository repository;

    @Before
    public void setUp() throws Exception {
        Date now = new Date();
        LocationSetup locationActivated = new LocationSetup("SAAS_ID_1", "Test_Company", "user", now);
        locationActivated.setIsUserActivated(BooleanType.TRUE);
        repository.save(locationActivated);

        LocationSetup locationNotActivated = new LocationSetup("SAAS_ID_2", "No_Activated_Test_Company", "user", now);
        locationNotActivated.setIsUserActivated(BooleanType.FALSE);
        repository.save(locationNotActivated);
    }

    @Test
    public void should_return_200_when_company_name_is_unique_and_user_is_not_activated() throws Exception {
        this.mockMvc.perform(get("/public/api/v1/locationsetup/is_company_unique/Another_Test_Company/-1"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.responseCode").value("ERRC0009"));
    }

    @Test
    public void should_return_200_when_company_name_exists_and_user_is_not_activated() throws Exception {
        this.mockMvc.perform(get("/public/api/v1/locationsetup/is_company_unique/No_Activated_Test_Company/-1"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.responseCode").value("ERRC0009"));
    }

    @Test
    public void should_return_409_when_company_name_exists_and_user_is_activated() throws Exception {
        this.mockMvc.perform(get("/public/api/v1/locationsetup/is_company_unique/Test_Company/-1"))
                .andDo(print())
                .andExpect(status().isConflict())
                .andExpect(jsonPath("$.responseCode").value("ERRC0010"));
    }

    @Test
    public void should_return_409_when_company_name_exists_in_different_case() throws Exception {
        this.mockMvc.perform(get("/public/api/v1/locationsetup/is_company_unique/test_company/-1"))
                .andDo(print())
                .andExpect(status().isConflict())
                .andExpect(jsonPath("$.responseCode").value("ERRC0010"));
    }

    @Test
    public void should_save_company_section_in_company_registration_wizard() throws Exception {
        String request = companySectionRequest();
        this.mockMvc.perform(
                post("/public/api/v1/locationsetup/create_company")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(request))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.responseObject.id", notNullValue()))
                .andExpect(jsonPath("$.responseObject.saasId", notNullValue()));
    }

    @Test
    public void should_save_company_and_login_section_in_company_registration_wizard() throws Exception {
        String companySectionRequest = companySectionRequest();
        MvcResult mvcResult = this.mockMvc.perform(
                post("/public/api/v1/locationsetup/create_company")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(companySectionRequest))
                .andReturn();
        long id = getIdFromResponse(mvcResult);

        MvcResult getResponse = this.mockMvc.perform(get("/public/api/v1/locationsetup/get/{id}", id))
                .andReturn();

        Map<String, Object> responseObject = getResponseObject(getResponseAsMap(getResponse));
        String loginSectionRequest = loginSectionRequest(responseObject);
        this.mockMvc.perform(
                post("/public/api/v1/locationsetup/create_company")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(loginSectionRequest))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.responseObject.id", notNullValue()))
                .andExpect(jsonPath("$.responseObject.saasId", notNullValue()));
    }

    @Test
    public void should_save_company_login_and_configuration_section_in_company_registration_wizard() throws Exception {
        String companySectionRequest = companySectionRequest();
        MvcResult mvcResult = this.mockMvc.perform(
                post("/public/api/v1/locationsetup/create_company")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(companySectionRequest))
                .andReturn();
        long id = getIdFromResponse(mvcResult);

        MvcResult getResponse = this.mockMvc.perform(get("/public/api/v1/locationsetup/get/{id}", id))
                .andReturn();

        Map<String, Object> responseObject = getResponseObject(getResponseAsMap(getResponse));
        String loginSectionRequest = loginSectionRequest(responseObject);
        this.mockMvc.perform(
                post("/public/api/v1/locationsetup/create_company")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(loginSectionRequest))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.responseObject.id", notNullValue()))
                .andExpect(jsonPath("$.responseObject.saasId", notNullValue()));

        getResponse = this.mockMvc.perform(get("/public/api/v1/locationsetup/get/{id}", id))
                .andReturn();

        responseObject = getResponseObject(getResponseAsMap(getResponse));

        String configurationSectionRequest = configurationSectionRequest(responseObject);

        this.mockMvc.perform(
                post("/public/api/v1/locationsetup/create_company")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(configurationSectionRequest))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.responseObject.id", notNullValue()))
                .andExpect(jsonPath("$.responseObject.saasId", notNullValue()));
    }

    @Test
    public void should_complete_company_registration() throws Exception {
        String companySectionRequest = companySectionRequest();
        MvcResult mvcResult = this.mockMvc.perform(
                post("/public/api/v1/locationsetup/create_company")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(companySectionRequest))
                .andReturn();
        long id = getIdFromResponse(mvcResult);
        MvcResult getResponse = this.mockMvc.perform(get("/public/api/v1/locationsetup/get/{id}", id))
                .andReturn();

        Map<String, Object> responseObject = getResponseObject(getResponseAsMap(getResponse));
        String loginSectionRequest = loginSectionRequest(responseObject);

        this.mockMvc.perform(
                post("/public/api/v1/locationsetup/create_company")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(loginSectionRequest))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.responseObject.id", notNullValue()))
                .andExpect(jsonPath("$.responseObject.saasId", notNullValue()));

        getResponse = this.mockMvc.perform(get("/public/api/v1/locationsetup/get/{id}", id))
                .andReturn();
        responseObject = getResponseObject(getResponseAsMap(getResponse));

        String configurationSectionRequest = configurationSectionRequest(responseObject);

        this.mockMvc.perform(
                post("/public/api/v1/locationsetup/create_company")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(configurationSectionRequest))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.responseObject.id", notNullValue()))
                .andExpect(jsonPath("$.responseObject.saasId", notNullValue()));

        getResponse = this.mockMvc.perform(get("/public/api/v1/locationsetup/get/{id}", id))
                .andReturn();
        responseObject = getResponseObject(getResponseAsMap(getResponse));
        responseObject.put("currentSection", LocationSetupSection.Completed);
        String completionRequest = toJson(responseObject);

        this.mockMvc.perform(
                post("/public/api/v1/locationsetup/create_company")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(completionRequest))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.responseObject.id", notNullValue()))
                .andExpect(jsonPath("$.responseObject.saasId", notNullValue()));

        this.mockMvc.perform(get("/public/api/v1/locationsetup/get/{id}", id))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.responseObject.password", nullValue()))
                .andExpect(jsonPath("$.responseObject.companyRegisteredName").value("Test Company"))
                .andExpect(jsonPath("$.responseObject.cityMaster", notNullValue()))
                .andExpect(jsonPath("$.responseObject.countryMaster", notNullValue()))
                .andExpect(jsonPath("$.responseObject.languageMaster", notNullValue()))
                .andExpect(jsonPath("$.responseObject.currentSection").value("Completed"))
                .andExpect(jsonPath("$.responseObject.saasId", notNullValue()));
    }


    private String configurationSectionRequest(Map<String, Object> responseObject) {
        DateTime start = new DateTime(2018, 3, 1, 0, 0);
        responseObject.put("password", "password");
        responseObject.put("measurement", Measurement.Imperial);
        responseObject.put("currentSection", LocationSetupSection.Configuration);
        responseObject.put("currencyMaster", commonCurrencyMaster);
        responseObject.put("languageMaster", commonLanguageMaster);
        responseObject.put("financialStartDate", start.toDate());
        responseObject.put("financialEndDate", start.plusYears(1).toDate());
        String configurationSectionRequest = toJson(responseObject);
        return configurationSectionRequest;
    }


    private String loginSectionRequest(Map<String, Object> responseObject) {
        responseObject.put("userName", "test_user");
        responseObject.put("password", "password");
        responseObject.put("position", "Tester");
        responseObject.put("currentSection", LocationSetupSection.Login);
        responseObject.put("serviceList", Collections.singletonList(new LocationSetupService(commonServiceMaster)));
        return toJson(responseObject);
    }

    private String companySectionRequest() {
        LocationSetup locationSetup = new LocationSetup();
        locationSetup.setCurrentSection(LocationSetupSection.Company);
        locationSetup.setSalutation(SalutationType.Mr);
        locationSetup.setFirstName("Test");
        locationSetup.setLastName("User");
        locationSetup.setEmail("test@test.com");
        locationSetup.setCountryMaster(commonCountry);
        locationSetup.setPhone("9898989898");
        locationSetup.setCompanyRegisteredName("Test Company");
        locationSetup.setAddressLine1("Address Line 1");
        locationSetup.setArea("Area 1");
        locationSetup.setCityMaster(commonCityMaster);
        locationSetup.setTimeZoneMaster(commonTimeZoneMaster);
        locationSetup.setDateFormat("DD-MM-YYYY");

        return toJson(locationSetup);
    }
}
