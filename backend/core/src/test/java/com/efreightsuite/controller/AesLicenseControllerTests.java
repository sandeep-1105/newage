package com.efreightsuite.controller;

import java.util.Arrays;
import java.util.Date;

import com.efreightsuite.dto.SearchRequest;
import com.efreightsuite.enumeration.LovStatus;
import com.efreightsuite.model.AesLicense;
import com.efreightsuite.model.SystemTrack;
import com.efreightsuite.repository.AesLicenseRepository;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class AesLicenseControllerTests extends BaseRestApiTest {

    private static final String API_URL = "/api/v1/aeslicense/get/search/keyword";

    @Autowired
    private AesLicenseRepository repository;

    @Before
    public void setUp() throws Exception {
        SystemTrack systemTrack = new SystemTrack("test_user", new Date(), "test_user", new Date());
        this.repository.save(
                Arrays.asList(
                        new AesLicense("l1", "License 1", "ltype1", LovStatus.Active, systemTrack),
                        new AesLicense("l2", "License 2", "ltype2", LovStatus.Active, systemTrack),
                        new AesLicense("l3", "Another License 3", "ltype3", LovStatus.Active, systemTrack)
                )
        );
    }

    @After
    public void tearDown() throws Exception {
        this.repository.deleteAll();
    }

    @Test
    @WithMockUser(username = "test_user")
    public void should_search_bank_keyword() throws Exception {
        String query = this.gson.toJson(new SearchRequest("lic"));
        this.mockMvc.perform(
                post(API_URL)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(query))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.responseObject.searchResult").isArray())
                .andExpect(jsonPath("$.responseObject.searchResult.length()").value(2))
                .andExpect(jsonPath("$.responseObject.searchResult[*].licenseCode")
                        .value(toJsonArray("License 1", "License 2")));
    }

    @Test
    @WithMockUser(username = "test_user")
    public void should_search_bank_with_empty_keyword() throws Exception {
        String query = this.gson.toJson(new SearchRequest(""));
        this.mockMvc.perform(
                post(API_URL)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(query))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.responseObject.searchResult").isArray())
                .andExpect(jsonPath("$.responseObject.searchResult.length()").value(3))
                .andExpect(jsonPath("$.responseObject.searchResult[*].licenseCode")
                        .value(toJsonArray("License 1","License 2", "Another License 3")));
    }
}
