package com.efreightsuite.controller;

import java.util.Collections;
import java.util.Date;
import java.util.Map;

import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.enumeration.SequenceType;
import com.efreightsuite.enumeration.WhoRouted;
import com.efreightsuite.enumeration.YesNo;
import com.efreightsuite.model.LocationMaster;
import com.efreightsuite.model.Shipment;
import com.efreightsuite.model.ShipmentServiceDetail;
import com.efreightsuite.model.UserProfile;
import com.efreightsuite.service.mailer.ShipmentMailer;
import com.efreightsuite.util.AppUtil;
import org.junit.Test;

import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MvcResult;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willDoNothing;
import static org.mockito.Matchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class ShipmentControllerTests extends BaseRestApiTest {

    @MockBean
    private ShipmentMailer shipmentMailer;
    @MockBean
    protected AppUtil appUtil;

    @Test
    @WithMockUser(username = "test_user", roles = {"CRM.SHIPMENT.CREATE", "CRM.SHIPMENT.VIEW", "CRM.SHIPMENT.MODIFY"})
    public void should_create_get_and_update_a_shipment() throws Exception {
        String request = createShipmentRequest();
        given(sequenceGeneratorService.getSequence(SequenceType.SHIPMENT)).willReturn("SHP001");
        given(sequenceGeneratorService.getSequence(SequenceType.SERVICE)).willReturn("SER001");
        given(appUtil.getLocationConfig(anyString(), any(LocationMaster.class), anyBoolean())).willReturn("1.0");
        willDoNothing().given(shipmentMailer).shipmentMailTrigger(any(Shipment.class), any(UserProfile.class), anyMapOf(String.class, String.class));
        given(appUtil.setDesc(any(BaseDto.class))).willCallRealMethod();

        MvcResult result = this.mockMvc.perform(
                post("/api/v1/shipment/create")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(request))
                .andDo(print())
                .andExpect(status().isCreated())
                .andExpect(header().string("Location", containsString("/api/v1/shipment/get/id")))
                .andExpect(jsonPath("$.responseObject.id").value(notNullValue()))
                .andExpect(jsonPath("$.responseObject.shipmentUid").value("SHP001"))
                .andReturn();

        MockHttpServletResponse response = result.getResponse();
        Map<String, Object> shipment = fromJson(response.getContentAsString());
        long shipmentId = getIdFromResponse(shipment);

        MvcResult getShipmentResult = this.mockMvc.perform(get("/api/v1/shipment/get/id/{id}", shipmentId))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.responseObject.versionLock").value(1))
                .andReturn();

        Map<String, Object> fullShipment = (Map<String, Object>) fromJson(getShipmentResult.getResponse().getContentAsString()).get("responseObject");
        fullShipment.put("companyCode", "CC2");
        this.mockMvc.perform(
                post("/api/v1/shipment/update")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(toJson(fullShipment)))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.responseObject.id").value(shipmentId))
                .andExpect(jsonPath("$.responseObject.shipmentUid").value("SHP001"));

        this.mockMvc.perform(get("/api/v1/shipment/get/id/{id}", shipmentId))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.responseObject.versionLock").value(2))
                .andReturn();
    }

    @Test
    @WithMockUser(username = "test_user", roles = {"CRM.SHIPMENT.CREATE", "CRM.SHIPMENT.VIEW", "CRM.SHIPMENT.MODIFY"})
    public void should_get_concurrent_modification_exception_when_user_tries_to_update_old_version() throws Exception {
        String request = createShipmentRequest();
        given(sequenceGeneratorService.getSequence(SequenceType.SHIPMENT)).willReturn("SHP001");
        given(sequenceGeneratorService.getSequence(SequenceType.SERVICE)).willReturn("SER001");
        given(appUtil.getLocationConfig(anyString(), any(LocationMaster.class), anyBoolean())).willReturn("1.0");
        willDoNothing().given(shipmentMailer).shipmentMailTrigger(any(Shipment.class), any(UserProfile.class), anyMapOf(String.class, String.class));
        given(appUtil.setDesc(any(BaseDto.class))).willCallRealMethod();

        MvcResult result = this.mockMvc.perform(
                post("/api/v1/shipment/create")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(request))
                .andDo(print())
                .andExpect(status().isCreated())
                .andExpect(header().string("Location", containsString("/api/v1/shipment/get/id")))
                .andExpect(jsonPath("$.responseObject.id").value(notNullValue()))
                .andExpect(jsonPath("$.responseObject.shipmentUid").value("SHP001"))
                .andReturn();

        MockHttpServletResponse response = result.getResponse();
        Map<String, Object> shipment = fromJson(response.getContentAsString());
        long shipmentId = getIdFromResponse(shipment);

        MvcResult getShipmentResult = this.mockMvc.perform(get("/api/v1/shipment/get/id/{id}", shipmentId))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.responseObject.versionLock").value(1))
                .andReturn();

        Map<String, Object> previousVersion = (Map<String, Object>) fromJson(getShipmentResult.getResponse().getContentAsString()).get("responseObject");
        previousVersion.put("companyCode", "CC2");
        this.mockMvc.perform(
                post("/api/v1/shipment/update")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(toJson(previousVersion)))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.responseObject.id").value(shipmentId))
                .andExpect(jsonPath("$.responseObject.shipmentUid").value("SHP001"));

        previousVersion.put("companyCode", "CC3");
        this.mockMvc.perform(
                post("/api/v1/shipment/update")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(toJson(previousVersion)))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.responseCode").value("ERR20"));
    }

    private String createShipmentRequest() {
        Shipment shipment = new Shipment();
        shipment.setCompanyCode("CC1");
        shipment.setCountryCode("IN");
        shipment.setCreatedByCode("CBY1");
        shipment.setIsFromConsolForImport(YesNo.No);
        shipment.setLocationCode("GGN");
        shipment.setCountry(country);
        shipment.setLocation(location);
        shipment.setCompanyMaster(company);
        shipment.setShipmentReqDate(new Date());
        shipment.setCreatedBy(employee);
        ShipmentServiceDetail shipmentServiceDetail = new ShipmentServiceDetail();
        shipmentServiceDetail.setHazardous(YesNo.No);
        shipmentServiceDetail.setAutoImport(YesNo.No);
        shipmentServiceDetail.setServiceReqDate(new Date());
        shipmentServiceDetail.setMawbDate(new Date());
        shipmentServiceDetail.setServiceMaster(serviceMaster);
        shipmentServiceDetail.setCustomerService(employee);
        shipmentServiceDetail.setOrigin(toPortMaster);
        shipmentServiceDetail.setTosMaster(tosMaster);
        shipmentServiceDetail.setDestination(destinationPortMaster);
        shipmentServiceDetail.setPol(toPortMaster);
        shipmentServiceDetail.setPod(destinationPortMaster);
        shipmentServiceDetail.setWhoRouted(WhoRouted.Self);
        shipmentServiceDetail.setSalesman(salesman);
        shipmentServiceDetail.setWorkflowCompleted(YesNo.No);
        shipmentServiceDetail.setParty(partyMaster);
        shipmentServiceDetail.setCompany(company);
        shipmentServiceDetail.setLocation(location);
        shipmentServiceDetail.setDocumentList(Collections.emptyList());
        shipment.setShipmentReqDate(new Date());
        shipment.setShipmentServiceList(Collections.singletonList(shipmentServiceDetail));
        return gson.toJson(shipment);
    }

}
