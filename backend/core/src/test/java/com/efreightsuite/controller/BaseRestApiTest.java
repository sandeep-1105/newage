package com.efreightsuite.controller;

import com.efreightsuite.enumeration.*;
import com.efreightsuite.model.*;
import com.efreightsuite.model.common.*;
import com.efreightsuite.repository.*;
import com.efreightsuite.repository.common.*;
import com.efreightsuite.service.SequenceGeneratorService;
import com.google.gson.*;
import com.google.gson.reflect.TypeToken;
import net.minidev.json.JSONArray;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.transaction.annotation.Transactional;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.Map;

@RunWith(SpringRunner.class)
@ActiveProfiles("test")
@SpringBootTest
@AutoConfigureMockMvc
@Transactional
abstract class BaseRestApiTest {

    DateTimeFormatter dateTimeFormatter = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:SS");

    @MockBean
    SequenceGeneratorService sequenceGeneratorService;

    @Autowired
    MockMvc mockMvc;

    @Autowired
    LocationMasterRepository locationMasterRepository;
    @Autowired
    CountryMasterRepository countryMasterRepository;
    @Autowired
    CompanyMasterRepository companyMasterRepository;
    @Autowired
    EmployeeMasterRepository employeeMasterRepository;
    @Autowired
    ServiceMasterRepository serviceMasterRepository;
    @Autowired
    PortMasterRepository portMasterRepository;
    @Autowired
    TosMasterRepository tosMasterRepository;
    @Autowired
    PartyMasterRepository partyMasterRepository;
    @Autowired
    StateMasterRepository stateMasterRepository;
    @Autowired
    CityMasterRepository cityMasterRepository;
    @Autowired
    CommonCountryMasterRepository commonCountryMasterRepository;
    @Autowired
    CommonCityMasterRepository commonCityMasterRepository;
    @Autowired
    CommonTimeZoneMasterRepository commonTimeZoneMasterRepository;
    @Autowired
    CommonServiceMasterRepository commonServiceMasterRepository;
    @Autowired
    CommonCurrencyMasterRepository commonCurrencyMasterRepository;
    @Autowired
    CommonLanguageMasterRepository commonLanguageMasterRepository;
    @Autowired
    LanguageRepository languageRepository;
    @Autowired
    LanguageDetailRepository languageDetailRepository;

    @Autowired
    AgentPortMasterRepository agentPortMasterRepository;

    final Gson gson;

    CountryMaster country;
    LocationMaster location;
    CompanyMaster company;
    EmployeeMaster employee;
    ServiceMaster serviceMaster;
    PortMaster toPortMaster;
    PortMaster destinationPortMaster;
    TosMaster tosMaster;
    EmployeeMaster salesman;
    PartyMaster partyMaster;
    CityMaster cityMaster;
    StateMaster stateMaster;
    AddressMaster addressMaster;
    PortMaster destination2PortMaster;
    CommonCountryMaster commonCountry;
    CommonCityMaster commonCityMaster;
    CommonTimeZoneMaster commonTimeZoneMaster;
    CommonServiceMaster commonServiceMaster;
    CommonCurrencyMaster commonCurrencyMaster;
    CommonLanguageMaster commonLanguageMaster;
    Language language;
    LanguageDetail languageDetail;
    AgentPortMaster agentPortMaster = new AgentPortMaster();

    public BaseRestApiTest() {
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(Date.class, new JsonSerializer<Date>() {
            DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

            @Override
            public JsonElement serialize(Date src, Type typeOfSrc, JsonSerializationContext context) {
                return src == null ? null : new JsonPrimitive(df.format(src));
            }
        });
        gsonBuilder.registerTypeAdapter(YesNo.class, new JsonSerializer<YesNo>() {
            @Override
            public JsonElement serialize(YesNo src, Type typeOfSrc, JsonSerializationContext context) {
                return src == YesNo.Yes ? new JsonPrimitive(true) : new JsonPrimitive(false);
            }
        });
        gsonBuilder.registerTypeAdapter(DimensionUnit.class, new JsonSerializer<DimensionUnit>() {
            @Override
            public JsonElement serialize(DimensionUnit src, Type typeOfSrc, JsonSerializationContext context) {
                return src == DimensionUnit.INCHORPOUNDS ? new JsonPrimitive(true) : new JsonPrimitive(false);
            }
        });
        this.gson = gsonBuilder.create();
    }

    @Before
    public void setupBaseTestData() throws Exception {
        SystemTrack systemTrack = new SystemTrack("test_user", new Date(), "test_user", new Date());

        country = countryMasterRepository.save(new CountryMaster("IN", "India", "INR",LovStatus.Active, systemTrack));
        commonCountry = commonCountryMasterRepository.save(new CommonCountryMaster("India", "IN", LovStatus.Active));
        company = this.companyMasterRepository.save(new CompanyMaster("Xebia", "XI", LovStatus.Active, systemTrack));
        location = locationMasterRepository.save(new LocationMaster.Builder("GGN","Gurgaon","IST",LovStatus.Active,"ddMMyyy","ddMMyyy hh:mm:ss")
                    .withBranchName("test branch")
                    .withCompanyMaster(company)
                    .withCountryMaster(country).build());
        location.setCountryMaster(country);
        location.setCompanyMaster(company);
        employee = this.employeeMasterRepository.save(new EmployeeMaster("Employee 1", "E01", "emp1@test.com", systemTrack));
        salesman = this.employeeMasterRepository.save(new EmployeeMaster("Salesman 1", "S01", "semp1@test.com", YesNo.Yes, systemTrack));
        serviceMaster = this.serviceMasterRepository.save(new ServiceMaster("Service 1", "s1", TransportMode.Air, ImportExport.Export, systemTrack, LovStatus.Active));
        commonServiceMaster = this.commonServiceMasterRepository.save(new CommonServiceMaster("s1", "Service 1", TransportMode.Air, ImportExport.Export, LovStatus.Active));
        toPortMaster = this.portMasterRepository.save(new PortMaster("pg1", "pc1", "Port 1", "IN", LovStatus.Active, systemTrack, TransportMode.Air));
        destinationPortMaster = this.portMasterRepository.save(new PortMaster("pg2", "pc2", "Port 2", "IN", LovStatus.Active, systemTrack, TransportMode.Air));
        destination2PortMaster = this.portMasterRepository.save(new PortMaster("pg3", "pc3", "Port 3", "IN", LovStatus.Active, systemTrack, TransportMode.Air));
        tosMaster = this.tosMasterRepository.save(new TosMaster("Tos1", "t1", LovStatus.Active, systemTrack));
        partyMaster = this.partyMasterRepository.save(new PartyMaster("Party 1", "p1", LovStatus.Active, systemTrack));
        cityMaster = this.cityMasterRepository.save(new CityMaster("cy1", "City 1", LovStatus.Active, systemTrack));
        commonCityMaster = this.commonCityMasterRepository.save(new CommonCityMaster("GGN", "Gurgaon", LovStatus.Active));
        stateMaster = this.stateMasterRepository.save(new StateMaster("s1", "State 1", LovStatus.Active, country, country.getCountryCode(), systemTrack));
        addressMaster = new AddressMaster("Address Line 1", stateMaster, cityMaster);
        commonTimeZoneMaster = this.commonTimeZoneMasterRepository.save(new CommonTimeZoneMaster("Indian Standard Time", "IST", LovStatus.Active));
        commonCurrencyMaster = this.commonCurrencyMasterRepository.save(new CommonCurrencyMaster("INR", "Indian Rupee", "2", LovStatus.Active));
        commonLanguageMaster = this.commonLanguageMasterRepository.save(new CommonLanguageMaster("EN", "English", YesNo.Yes));
        language = this.languageRepository.save(new Language("English",YesNo.Yes));
        languageDetail = this.languageDetailRepository.save(new LanguageDetail(language,"ERR8845","city is blocked","Enquiry",YesNo.No,systemTrack));
        agentPortMaster.setAgent(partyMaster);
        agentPortMaster.setCountryMaster(country);
        agentPortMaster.setLocationMaster(location);
        agentPortMaster.setPort(toPortMaster);
        agentPortMaster.setServiceMaster(serviceMaster);
        agentPortMaster.setServiceCode("S001");
        agentPortMaster = this.agentPortMasterRepository.save(agentPortMaster);

    }

    protected String toJson(Object object) {
        return gson.toJson(object);
    }

    Map<String, Object> fromJson(String json) {
        Type type = new TypeToken<Map<String, Object>>() {
        }.getType();
        return gson.fromJson(json, type);
    }

    @SafeVarargs
    static <T> JSONArray toJsonArray(T... els) {
        JSONArray idArray = new JSONArray();
        idArray.addAll(Arrays.asList(els));
        return idArray;
    }

    protected PartyMaster partialPartyMaster() {
        return new PartyMaster(this.partyMaster.getId());
    }

    public CompanyMaster partialCompanyMaster() {
        return new CompanyMaster(company.getId(), company.getCompanyCode(), company.getCompanyName());
    }

    public LocationMaster partialLocationMaster() {
        return new LocationMaster(location.getId(), location.getLocationCode());
    }

    public CountryMaster partialCountryMaster() {
        return new CountryMaster(country.getId(), country.getCountryCode(), country.getCountryName());
    }

    public EmployeeMaster partialEmployeeMaster() {
        return new EmployeeMaster(employee.getId(), employee.getEmployeeCode(), employee.getEmployementStatus());
    }

    public EmployeeMaster partialSalesman() {
        return new EmployeeMaster(salesman.getId(), salesman.getEmployeeCode(), salesman.getEmployementStatus());
    }

    protected long getIdFromResponse(Map<String, Object> enquiry) {
        return ((Double) getResponseObject(enquiry).get("id")).longValue();
    }

    protected Map<String, Object> getResponseObject(Map<String, Object> enquiry) {
        return (Map<String, Object>) enquiry.get("responseObject");
    }

    protected long getIdFromResponse(MvcResult mvcResult) {
        Map<String, Object> responseAsMap = getResponseAsMap(mvcResult);
        return ((Double) getResponseObject(responseAsMap).get("id")).longValue();
    }

    protected Map<String, Object> getResponseAsMap(MvcResult mvcResult) {
        try {
            return fromJson(mvcResult.getResponse().getContentAsString());
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }
}
