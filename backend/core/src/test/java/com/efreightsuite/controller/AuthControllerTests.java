package com.efreightsuite.controller;

import java.util.Arrays;
import java.util.Collections;

import javax.servlet.ServletContext;
import javax.servlet.http.Cookie;

import com.efreightsuite.dto.LoginRequest;
import com.efreightsuite.model.AesTransportMode;
import com.efreightsuite.model.LoginHistory;
import com.efreightsuite.model.UserCompany;
import com.efreightsuite.model.UserCompanyLocation;
import com.efreightsuite.model.UserProfile;
import com.efreightsuite.repository.AesTransportModeRepository;
import com.efreightsuite.repository.DefaultMasterDataRepository;
import com.efreightsuite.repository.LoginHistoryRepository;
import com.efreightsuite.repository.UserProfileRepository;
import org.junit.Test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.web.servlet.MvcResult;

import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class AuthControllerTests extends BaseRestApiTest {

    @MockBean
    private UserProfileRepository userProfileRepository;

    @MockBean
    private LoginHistoryRepository loginHistoryRepository;

    @MockBean
    private DefaultMasterDataRepository defaultMasterDataRepository;

    @MockBean
    private AesTransportModeRepository aesTransportModeRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private ServletContext servletContext;

    @Test
    public void should_return_401_when_login_with_invalid_username_and_password() throws Exception {
        final String request = toJson(new LoginRequest("test", "test"));
        this.mockMvc
                .perform(post("/api/v1/auth/login")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(request))
                .andDo(print())
                .andExpect(status().isUnauthorized())
                .andExpect(jsonPath("$.responseCode").value("ERR2"));
    }

    @Test
    public void should_return_400_when_credentials_not_present() throws Exception {
        final String request = toJson(new LoginRequest("", ""));
        this.mockMvc
                .perform(post("/api/v1/auth/login")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(request))
                .andDo(print())
                .andExpect(status().isBadRequest());
    }

    @Test
    public void should_return_401_when_login_exits_but_user_not_associated_with_company() throws Exception {
        given(this.userProfileRepository.findByUserName("test")).willReturn(new UserProfile());
        final String request = toJson(new LoginRequest("test", "test"));
        this.mockMvc
                .perform(post("/api/v1/auth/login")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(request))
                .andDo(print())
                .andExpect(status().isUnauthorized())
                .andExpect(jsonPath("$.responseCode").value("ERR46"));
    }

    @Test
    public void should_return_200_ok_when_credentials_are_valid() throws Exception {
        UserProfile userProfile = createUserProfile();
        given(this.defaultMasterDataRepository.getByLocation(anyLong())).willReturn(Collections.emptyList());
        given(this.aesTransportModeRepository.findByTransportName("40-Air,Non Containerized"))
                .willReturn(new AesTransportMode());
        given(this.userProfileRepository.findByUserName("test"))
                .willReturn(userProfile);
        final String request = toJson(new LoginRequest("test", "password"));

        this.mockMvc
                .perform(post("/api/v1/auth/login")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(request))
                .andDo(print())
                .andExpect(status().isOk());

        verify(loginHistoryRepository, times(1)).save(any(LoginHistory.class));
    }

    @Test
    public void should_return_401_when_user_company_is_not_associated_with_location() throws Exception {
        UserProfile userProfileWithoutUserCompanyLocation = createUserProfile(false);
        given(this.defaultMasterDataRepository.getByLocation(anyLong())).willReturn(Collections.emptyList());
        given(this.aesTransportModeRepository.findByTransportName("40-Air,Non Containerized"))
                .willReturn(new AesTransportMode());
        given(this.userProfileRepository.findByUserName("test"))
                .willReturn(userProfileWithoutUserCompanyLocation);

        final String request = toJson(new LoginRequest("test", "password"));

        this.mockMvc
                .perform(post("/api/v1/auth/login")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(request))
                .andDo(print())
                .andExpect(status().isUnauthorized())
                .andExpect(jsonPath("$.responseCode").value("ERR46"));

    }

    @Test
    public void should_return_401_when_username_is_valid_but_password_is_invalid() throws Exception {
        UserProfile userProfile = createUserProfile();
        given(this.defaultMasterDataRepository.getByLocation(anyLong())).willReturn(Collections.emptyList());
        given(this.aesTransportModeRepository.findByTransportName("40-Air,Non Containerized"))
                .willReturn(new AesTransportMode());
        given(this.userProfileRepository.findByUserName("test"))
                .willReturn(userProfile);
        final String request = toJson(new LoginRequest("test", "invalid_password"));

        this.mockMvc
                .perform(post("/api/v1/auth/login")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(request))
                .andDo(print())
                .andExpect(status().isUnauthorized())
                .andExpect(jsonPath("$.responseCode").value("ERR2"));

        verify(loginHistoryRepository, times(1)).save(any(LoginHistory.class));
    }

    @Test
    public void should_return_401_after_session_has_expired() throws Exception {
        UserProfile userProfile = createUserProfile();
        given(this.defaultMasterDataRepository.getByLocation(anyLong())).willReturn(Collections.emptyList());
        given(this.aesTransportModeRepository.findByTransportName("40-Air,Non Containerized"))
                .willReturn(new AesTransportMode());
        given(this.userProfileRepository.findByUserName("test"))
                .willReturn(userProfile);
        final String request = toJson(new LoginRequest("test", "password"));

        MockHttpSession mockHttpSession = new MockHttpSession(servletContext);
        mockHttpSession.setMaxInactiveInterval(30);
        MvcResult mvcResult = this.mockMvc
                .perform(post("/api/v1/auth/login")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(request)
                        .session(mockHttpSession))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn();
        Cookie[] cookies = mvcResult.getResponse().getCookies();

        verify(loginHistoryRepository, times(1)).save(any(LoginHistory.class));
        this.mockMvc.perform(get("/api/v1/application/getalltimezone").session(mockHttpSession).cookie(cookies))
                .andDo(print())
                .andExpect(status().isOk());

        mockHttpSession.invalidate();
        Arrays.stream(cookies).forEach(cookie -> {
            cookie.setMaxAge(0);
            cookie.setValue(null);
            cookie.setPath("");
        });

        this.mockMvc.perform(get("/api/v1/application/getalltimezone").session(mockHttpSession).cookie(cookies))
                .andDo(print())
                .andExpect(status().isUnauthorized())
                .andExpect(status().reason("You have been logged out because of inactivity"));
    }

    private UserProfile createUserProfile(boolean shouldSetUserCompanyList) {
        UserProfile userProfile = new UserProfile();
        UserCompany userCompany = new UserCompany();
        userCompany.setEmployee(employee);
        userCompany.setCompanyMaster(company);
        UserCompanyLocation userCompanyLocation = new UserCompanyLocation();
        userCompanyLocation.setLocationMaster(location);
        if (shouldSetUserCompanyList) {
            userCompany.setUserCompanyLocationList(Collections.singletonList(userCompanyLocation));
        }
        userProfile.setUserCompanyList(Collections.singletonList(userCompany));
        userProfile.setUserName("test");
        userProfile.setPassword(passwordEncoder.encode("password"));
        return userProfile;

    }

    private UserProfile createUserProfile() {
        return createUserProfile(true);
    }


}
