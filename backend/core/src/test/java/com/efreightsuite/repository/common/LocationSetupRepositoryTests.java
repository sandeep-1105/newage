package com.efreightsuite.repository.common;

import java.util.Arrays;
import java.util.Date;
import java.util.Set;

import com.efreightsuite.model.common.LocationSetup;
import org.junit.Test;

import org.springframework.beans.factory.annotation.Autowired;

import static org.assertj.core.api.Assertions.assertThat;

public class LocationSetupRepositoryTests extends BaseDataJpaTests {

    @Autowired
    private LocationSetupRepository repository;

    @Test
    public void repository_is_not_null() throws Exception {
        assertThat(repository).isNotNull();
    }

    @Test
    public void should_find_all_saas_ids() throws Exception {
        Date now = new Date();
        repository.save(
                Arrays.asList(
                        new LocationSetup("SAAS_ID_1", "user", now),
                        new LocationSetup("SAAS_ID_2", "user", now),
                        new LocationSetup("SAAS_ID_3", "user", now),
                        new LocationSetup("SAAS_ID_4", "user", now),
                        new LocationSetup("SAAS_ID_5", "user", now)
                )
        );
        Set<String> saasIds = repository.findAllSaasIds();
        assertThat(saasIds).hasSize(5);
    }
}
