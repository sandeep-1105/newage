package com.efreightsuite.repository.common;

import com.efreightsuite.util.AppUtil;
import org.junit.runner.RunWith;

import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@DataJpaTest
abstract class BaseDataJpaTests {

    @MockBean
    private AppUtil appUtil;
}
