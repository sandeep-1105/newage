package com.efreightsuite.repository.common;

import com.efreightsuite.enumeration.LovStatus;
import com.efreightsuite.model.common.CommonCountryMaster;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import static org.assertj.core.api.Assertions.assertThat;

public class CommonCountryMasterRepositoryTests extends BaseDataJpaTests {

    @Autowired
    private CommonCountryMasterRepository repository;

    @Before
    public void setUp() throws Exception {
        repository.save(new CommonCountryMaster("India", "IN", LovStatus.Active));
        repository.save(new CommonCountryMaster("Indonesia", "IND", LovStatus.Active));
        repository.save(new CommonCountryMaster("United States of America", "USA", LovStatus.Active));
        repository.save(new CommonCountryMaster("Pakistan", "PAK", LovStatus.Active));
        repository.save(new CommonCountryMaster("Bangladesh", "BNG", LovStatus.Hide));
    }

    @After
    public void tearDown() throws Exception {
        repository.deleteAll();
    }

    @Test
    public void repository_is_not_null() throws Exception {
        assertThat(repository).isNotNull();
    }

    @Test
    public void should_find_country_by_name() throws Exception {
        Page<CommonCountryMaster> page = repository.searchCountries("India", new PageRequest(0, 10));
        assertThat(page.getTotalElements()).isEqualTo(1L);
        assertThat(page.getContent()).hasSize(1);
    }

    @Test
    public void should_find_country_starting_with_ind() throws Exception {
        Page<CommonCountryMaster> page = repository.searchCountries("Ind", new PageRequest(0, 10));
        assertThat(page.getTotalElements()).isEqualTo(2L);
        assertThat(page.getContent()).hasSize(2);
        assertThat(page.getContent()).extracting("countryName").containsExactly("India", "Indonesia");
    }


    @Test
    public void should_find_country_case_insensitively() throws Exception {
        Page<CommonCountryMaster> page = repository.searchCountries("india", new PageRequest(0, 10));
        assertThat(page.getTotalElements()).isEqualTo(1L);
        assertThat(page.getContent()).hasSize(1);
    }

    @Test
    public void should_find_country_with_order_by() throws Exception {
        Page<CommonCountryMaster> page = repository.searchCountries("Ind", new PageRequest(0, 10));
        assertThat(page.getTotalElements()).isEqualTo(2L);
        assertThat(page.getContent()).hasSize(2);
        assertThat(page.getContent()).extracting("countryName").containsExactly("India", "Indonesia");
    }

    @Test
    public void should_not_include_country_with_hide_status() throws Exception {
        Page<CommonCountryMaster> page = repository.searchCountries("BAN", new PageRequest(0, 10));
        assertThat(page.getTotalElements()).isEqualTo(0L);
        assertThat(page.getContent()).isEmpty();
    }

    @Test
    public void should_find_country_by_code() throws Exception {
        Page<CommonCountryMaster> page = repository.searchCountries("USA", new PageRequest(0, 10));
        assertThat(page.getTotalElements()).isEqualTo(1L);
        assertThat(page.getContent()).hasSize(1);
    }
}
