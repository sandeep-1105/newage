package com.efreightsuite.util;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class OtpGeneratorTests {

    @Test
    public void should_generate_alphanumeric_otp() throws Exception {
        String otp = OtpGenerator.generateAlphanumeric(3);
        assertThat(otp).hasSize(3);
    }

    @Test
    public void should_generate_alphabetic_otp() throws Exception {
        String otp = OtpGenerator.generateAlphabetic(10);
        assertThat(otp).hasSize(10);
    }
}
