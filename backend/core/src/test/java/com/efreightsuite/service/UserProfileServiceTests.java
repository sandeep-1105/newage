package com.efreightsuite.service;

import java.util.Collections;

import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.UserProfileDto;
import com.efreightsuite.enumeration.UserStatus;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.model.UserProfile;
import com.efreightsuite.model.WorkFlowGroup;
import com.efreightsuite.repository.UserProfileRepository;
import com.efreightsuite.repository.WorkFlowGroupRepository;
import com.efreightsuite.util.AppUtil;
import com.efreightsuite.validation.UserProfileValidator;
import org.joda.time.DateTime;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import org.springframework.security.crypto.password.PasswordEncoder;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.AdditionalAnswers.returnsFirstArg;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class UserProfileServiceTests {

    @Mock
    private WorkFlowGroupRepository workFlowGroupRepository;
    @Mock
    private UserProfileRepository userProfileRepository;
    @Mock
    private AppUtil appUtil;
    @Mock
    private PasswordEncoder passwordEncoder;

    @InjectMocks
    private UserProfileService service;

    @Test
    public void should_save_user_profile_and_set_workflow_groups() throws Exception {
        service.userProfileValidator = new UserProfileValidator();
        UserProfileDto userProfileDto = new UserProfileDto();
        userProfileDto.setUserName("user1");
        userProfileDto.setPassword("password");
        userProfileDto.setPasswordExpiresOn(new DateTime().plusDays(3).toDate());
        userProfileDto.setStatus(UserStatus.Active);
        when(appUtil.setDesc(any(BaseDto.class))).thenAnswer(returnsFirstArg());
        when(workFlowGroupRepository.findAll()).thenReturn(Collections.singletonList(new WorkFlowGroup()));
        when(userProfileRepository.save(any(UserProfile.class))).thenReturn(new UserProfile(1L, "user1"));
        when(passwordEncoder.encode("password")).thenReturn("password");

        BaseDto baseDto = service.saveUser(userProfileDto);
        assertThat(baseDto.getResponseCode()).isEqualTo(ErrorCode.SUCCESS);
        verify(userProfileRepository, times(1)).save(any(UserProfile.class));
        verify(workFlowGroupRepository, times(1)).findAll();
    }
}
