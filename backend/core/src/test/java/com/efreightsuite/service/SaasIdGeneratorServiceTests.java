package com.efreightsuite.service;

import java.util.Collections;
import java.util.Set;
import java.util.stream.Stream;

import com.efreightsuite.model.common.LocationSetup;
import com.efreightsuite.repository.common.LocationSetupRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static java.util.stream.Collectors.toSet;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class SaasIdGeneratorServiceTests {

    @Mock
    private LocationSetupRepository locationSetupRepository;

    @InjectMocks
    private SaasIdGeneratorService saasIdGeneratorService = new SaasIdGeneratorService();

    @Test
    public void should_generate_unique_saas_id_when_db_is_empty() throws Exception {
        when(locationSetupRepository.findAllSaasIds()).thenReturn(Collections.emptySet());
        String saasId = saasIdGeneratorService.generateSaasId(new LocationSetup());
        assertThat(saasId).isNotNull();
    }

    @Test
    public void should_generate_unique_saas_id_when_data_in_db() throws Exception{
        Set<String> ids = Stream.of("EFSRAJ","EFSRAJ","EFSBBB","EFSCCC","EFSDDD","EFSEEE").collect(toSet());
        when(locationSetupRepository.findAllSaasIds()).thenReturn(ids);
        String saasId = saasIdGeneratorService.generateSaasId(new LocationSetup());
        assertThat(saasId).isNotNull();
        assertThat(saasId).isNotIn(ids);
    }

    @Test
    public void should_return_existing_saas_id_when_location_setup_has_saas_id() throws Exception{
        when(locationSetupRepository.findAllSaasIds()).thenReturn(Collections.emptySet());
        LocationSetup locationSetup = new LocationSetup();
        locationSetup.setSaasId("EFSABC");
        String saasId = saasIdGeneratorService.generateSaasId(locationSetup);
        assertThat(saasId).isEqualTo("EFSABC");
    }
}
