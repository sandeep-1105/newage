package com.efreightsuite.service;

import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.model.common.LocationSetup;
import com.efreightsuite.repository.common.LocationSetupRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class LocationSetupWizardServiceTests {


    @InjectMocks
    LocationSetupWizardService service = new LocationSetupWizardService();

    @Mock
    private LocationSetupRepository locationSetupRepository;

    @Test
    public void should_give_calling_code_for_country_code() throws Exception {
        ResponseEntity<BaseDto> response = service.getCallingCode("IN");
        assertThat(response.getBody().getResponseObject()).isEqualTo(91);
    }

    @Test
    public void should_return_response_code_200_when_phone_number_is_valid() throws Exception {
        ResponseEntity<BaseDto> response = service.isPhoneNumberValid("IN", "9898980012");
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    }

    @Test
    public void should_return_response_code_400_when_phone_number_is_invalid() throws Exception {
        ResponseEntity<BaseDto> response = service.isPhoneNumberValid("IN", "000000000000");
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
    }

    @Test
    public void should_return_error_resp_in_activation_when_user_has_invalid_id_or_activation_key() throws Exception{
        BaseDto respForNullIdAndKey =  service.activate(null,null);
        BaseDto respForNullId =  service.activate("RSF9N70FF",null);
        BaseDto respForNullKey =  service.activate(null,0l);
        assertEquals(respForNullIdAndKey.getResponseCode(), "ERR1");
        assertEquals(respForNullId.getResponseCode(), "ERR1");
        assertEquals(respForNullKey.getResponseCode(), "ERR1");
    }

    @Test
    public void should_return_error_resp_in_activation_when_user_does_not_exist() throws Exception{

        when(locationSetupRepository.getOne(anyLong())).thenReturn(null);
        BaseDto baseDto =  service.activate("RSF9N70FF",0l);
        assertEquals(baseDto.getResponseCode(), "ERR1");
    }

    @Test
    public void should_return_error_resp_in_activation_when_invalid_activation_key() throws Exception{

        LocationSetup locationSetup = new LocationSetup();
        locationSetup.setActivationKey("RSF9N70FF123");
        locationSetup.setSaasId("EFSAAA");
        locationSetup.setId(1l);
        when(locationSetupRepository.getOne(anyLong())).thenReturn(locationSetup);
        BaseDto baseDto =  service.activate("RSF9N70FF",0l);
        assertEquals(baseDto.getResponseCode(), "ERR1");
    }

}
