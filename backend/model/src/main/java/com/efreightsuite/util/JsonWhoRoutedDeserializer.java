package com.efreightsuite.util;

import java.io.IOException;

import com.efreightsuite.enumeration.WhoRouted;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

import org.springframework.stereotype.Component;

@Component
public class JsonWhoRoutedDeserializer extends JsonDeserializer<WhoRouted> {

    @Override
    public WhoRouted deserialize(JsonParser jsonparser, DeserializationContext ctxt) throws IOException {
        System.out.println("Who Routed :: " + jsonparser.getText());
        String string = jsonparser.getText();

        if (string != null && string.trim().length() != 0) {
            if (string.trim().toUpperCase().equals("TRUE")) {
                return WhoRouted.Agent;
            } else {
                return WhoRouted.Self;
            }
        }
        return WhoRouted.Self;

    }

}
