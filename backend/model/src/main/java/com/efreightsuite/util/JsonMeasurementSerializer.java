package com.efreightsuite.util;

import java.io.IOException;

import com.efreightsuite.enumeration.Measurement;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import org.springframework.stereotype.Component;

@Component
public class JsonMeasurementSerializer extends JsonSerializer<Measurement> {

    @Override
    public void serialize(Measurement value, JsonGenerator gen, SerializerProvider provider) throws IOException {
        if (value != null && value.equals(Measurement.Imperial)) {
            gen.writeObject(false);
        } else {
            gen.writeObject(true);
        }

    }
}
