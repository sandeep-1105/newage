package com.efreightsuite.util;

import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import com.efreightsuite.model.LocationMaster;
import com.efreightsuite.model.SystemTrackManual;
import com.efreightsuite.model.UserProfile;

public class TimeUtil {


    private TimeUtil() {

    }


    public static Date getCurrentLocationTime(LocationMaster locationMaster) {
        LocalDateTime ldt = LocalDateTime.now(ZoneId.of(locationMaster.getTimeZone()));
        Instant instant = ldt.atZone(ZoneId.systemDefault()).toInstant();
        return Date.from(instant);
    }

    public static Date getCurrentLocationTime(UserProfile userProfile) {


        String timeZone = userProfile.getSelectedUserLocation().getTimeZone();

        LocalDateTime ldt = LocalDateTime.now(ZoneId.of(timeZone));
        Instant instant = ldt.atZone(ZoneId.systemDefault()).toInstant();
        return Date.from(instant);

    }

/*	public static SystemTrack getCreateSystemTrack() {

		SystemTrack systemTrack = new SystemTrack();

		systemTrack.setCreateDate(getCurrentLocationTime());
		systemTrack.setCreateUser(AuthService.getCurrentUser().getUserName());

		systemTrack.setLastUpdatedDate(getCurrentLocationTime());
		systemTrack.setLastUpdatedUser(AuthService.getCurrentUser().getUserName());

		return systemTrack;
	}

	public static void setUpdateSystemTrack(SystemTrack systemTrack) {

		if (systemTrack == null) {
			systemTrack = new SystemTrack();
			systemTrack.setCreateDate(getCurrentLocationTime());
			systemTrack.setCreateUser(AuthService.getCurrentUser().getUserName());
		}
		systemTrack.setLastUpdatedDate(getCurrentLocationTime());
		systemTrack.setLastUpdatedUser(AuthService.getCurrentUser().getUserName());

	}*/

    public static SystemTrackManual getCreateSystemTrack(UserProfile userProfile) {

        SystemTrackManual systemTrack = new SystemTrackManual();

        systemTrack.setCreateDate(getCurrentLocationTime(userProfile));
        systemTrack.setCreateUser(userProfile.getUserName());

        systemTrack.setLastUpdatedDate(getCurrentLocationTime(userProfile));
        systemTrack.setLastUpdatedUser(userProfile.getUserName());

        return systemTrack;
    }

    public static void setUpdateSystemTrack(SystemTrackManual systemTrack, UserProfile userProfile) {

        if (systemTrack == null) {
            systemTrack = new SystemTrackManual();
            systemTrack.setCreateDate(getCurrentLocationTime(userProfile));
            systemTrack.setCreateUser(userProfile.getUserName());
        }
        systemTrack.setLastUpdatedDate(getCurrentLocationTime(userProfile));
        systemTrack.setLastUpdatedUser(userProfile.getUserName());

    }

    public static Date getServerDateTime() {
        return new Date();
    }

    public static String getServerDateTime(String pattern) {
        SimpleDateFormat sdf = new SimpleDateFormat(pattern);
        return sdf.format(new Date());
    }

    public static Date addMinutesToDate(int minutes, Date beforeTime) {
        final long ONE_MINUTE_IN_MILLIS = 60000;
        long curTimeInMs = beforeTime.getTime();
        Date afterAddingMins = new Date(curTimeInMs + (minutes * ONE_MINUTE_IN_MILLIS));
        return afterAddingMins;
    }


    public static String getLocationDate(LocationMaster locationMaster) {
        if (locationMaster != null && locationMaster.getJavaDateFormat() != null) {
            return new SimpleDateFormat(locationMaster.getJavaDateFormat()).format(getCurrentLocationTime(locationMaster));
        } else {
            return new SimpleDateFormat("dd-MMM-yyyy").format(getCurrentLocationTime(locationMaster));
        }
    }

    public static String getLocationDate(LocationMaster locationMaster, Date date) {
        if (locationMaster != null && locationMaster.getJavaDateFormat() != null) {
            return new SimpleDateFormat(locationMaster.getJavaDateFormat()).format(date);
        } else {
            return new SimpleDateFormat("dd-MMM-yyyy").format(date);
        }
    }

    public static Date addDate(Date date, Integer day) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.DAY_OF_YEAR, day);
        return calendar.getTime();
    }


    public static long differenceInDay(Date d1, Date d2) {
        long diff = d2.getTime() - d1.getTime();
        return TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
    }
}
