package com.efreightsuite.util;

import java.io.IOException;

import com.efreightsuite.enumeration.PPCC;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

import org.springframework.stereotype.Component;

@Component
public class JsonPPccDeserializer extends JsonDeserializer<PPCC> {

    @Override
    public PPCC deserialize(JsonParser jsonparser, DeserializationContext ctxt) throws IOException {

        String string = jsonparser.getText();

        if (string != null && string.trim().length() != 0) {
            if (string.trim().toUpperCase().equals("TRUE")) {
                return PPCC.Collect;
            } else {
                return PPCC.Prepaid;
            }
        }
        return PPCC.Prepaid;

    }

}
