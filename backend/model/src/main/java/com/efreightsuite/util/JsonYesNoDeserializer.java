package com.efreightsuite.util;

import java.io.IOException;

import com.efreightsuite.enumeration.YesNo;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

import org.springframework.stereotype.Component;

@Component
public class JsonYesNoDeserializer extends JsonDeserializer<YesNo> {

    @Override
    public YesNo deserialize(JsonParser jsonparser, DeserializationContext ctxt) throws IOException {

        String string = jsonparser.getText();

        if (string != null && string.trim().length() != 0) {
            if (string.trim().toUpperCase().equals("TRUE")) {
                return YesNo.Yes;
            } else {
                return YesNo.No;
            }
        }
        return YesNo.No;

    }

}
