package com.efreightsuite.util;

public class RegExpName {


    public static String REG_EXP_MOBILE_NUMBER = "mobile.number";
    public static String REG_EXP_PHONE_NUMBER = "phone.number";
    public static String REG_EXP_FAX_NUMBER = "fax.number";
    public static String REG_EXP_SINGLE_EMAIL = "single.email";
    public static String REG_EXP_MULTIPLE_EMAIL = "multiple.email";
    public static String Reg_Exp_AMOUNT_TEN_SIX = "amount.ten.six";

    public static String Reg_Exp_Master_Service_Code = "master.service.code";
    public static String Reg_Exp_Master_Service_Name = "master.service.name";

    public static String Reg_Exp_Master_Carrier_Code = "master.carrier.code";
    public static String Reg_Exp_Master_Carrier_Name = "master.carrier.name";

    public static String Reg_Exp_Master_Account_Code = "master.account.code";
    public static String Reg_Exp_Master_Account_Name = "master.account.name";

    public static String Reg_Exp_Master_Air_Carrier_Code = "master.air.carrier.code";
    public static String Reg_Exp_Master_Air_Carrier_No = "master.air.carrier.no";
    public static String Reg_Exp_Master_Air_Carrier_Iata_No = "master.air.carrier.iata.no";
    public static String Reg_Exp_Master_Carrier_No = "master.carrier.no";
    public static String Reg_Exp_Master_Carrier_Scac_Code = "master.carrier.scac.code";

    public static String Reg_Exp_Master_Edi_Type = "master.edi.type";
    public static String Reg_Exp_Master_Edi_Subtype = "master.edi.subtype";
    public static String Reg_Exp_Master_Edi_Value = "master.edi.value";

    public static String Reg_Exp_Master_Carrier_Rate_or_amount = "master.carrier.rate.or.amount";

    public static String Reg_Exp_Master_Unit_Code = "master.unit.code";
    public static String Reg_Exp_Master_Unit_Name = "master.unit.name";
    public static String Reg_Exp_Master_Unit_Calculation_1 = "master.master.unit.calculation.1";
    public static String Reg_Exp_Master_Unit_Calculation_2 = "master.master.unit.calculation.2";

    public static String Reg_Exp_Master_Tos_Code = "master.tos.code";
    public static String Reg_Exp_Master_Tos_Name = "master.tos.name";

    public static String Reg_Exp_Master_Company_Code = "master.company.code";
    public static String Reg_Exp_Master_Company_Name = "master.company.name";

    public static String Reg_Exp_Master_Pack_Code = "master.pack.code";
    public static String Reg_Exp_Master_Pack_Name = "master.pack.name";

    public static String Reg_Exp_Master_Charge_Code = "master.charge.code";
    public static String Reg_Exp_Master_Charge_Name = "master.charge.name";

    public static String Reg_Exp_Master_HS_CODE = "master.hs.code";
    public static String Reg_Exp_Master_HS_NAME = "master.hs.name";

    public static String Reg_Exp_Master_CATEGORY_CODE = "master.category.code";
    public static String Reg_Exp_Master_CATEGORY_NAME = "master.category.name";

    public static String Reg_Exp_Master_CURRENCY_BUY_RATE = "master.currency.buy.rate";
    public static String Reg_Exp_Master_CURRENCY_SELL_RATE = "master.currency.sell.rate";

    public static String Reg_Exp_Master_DOCUMENT_PREFIX_CODE = "master.document.prefix.code";
    public static String Reg_Exp_Master_DOCUMENT_PREFIX_NAME = "master.document.prefix.name";
    public static String Reg_Exp_Master_DOCUMENT_PREFIX_PREFIX_VALUE = "master.document.prefix.prefix.value";
    public static String Reg_Exp_Master_DOCUMENT_PREFIX_SUFFIX_VALUE = "master.document.prefix.suffix.value";

    public static String Reg_Exp_Master_CURRENCY_CODE = "master.currency.code";
    public static String Reg_Exp_Master_CURRENCY_NAME = "master.currency.name";
    public static String Reg_Exp_Master_CURRENCY_PREFIX = "master.currency.prefix";
    public static String Reg_Exp_Master_CURRENCY_SUFFFIX = "master.currency.sufffix";

    public static String Reg_Exp_Master_PARTY_GROUP_CODE = "master.party.group.code";
    public static String Reg_Exp_Master_PARTY_GROUP_NAME = "master.party.group.name";

    public static String Reg_Exp_Master_PARTY_TYPE_CODE = "master.party.type.code";
    public static String Reg_Exp_Master_PARTY_TYPE_NAME = "master.party.type.name";

    public static String Reg_Exp_Master_REFERENCE_TYPE_CODE = "master.reference.type.code";
    public static String Reg_Exp_Master_REFERENCE_TYPE_NAME = "master.reference.type.name";

    public static String Reg_Exp_Master_COST_CENTER_NAME = "master.cost.center.name";
    public static String Reg_Exp_Master_COST_CENTER_CODE = "master.cost.center.code";

    public static String Reg_Exp_Master_SERVICE_TYPE_NAME = "master.service.type.name";
    public static String Reg_Exp_Master_SERVICE_TYPE_CODE = "master.service.type.code";

    public static String Reg_Exp_Master_AUTO_MAIL_NAME = "master.auto.mail.name";
    public static String Reg_Exp_Master_AUTO_MAIL_CODE = "master.auto.mail.code";

    public static String Reg_Exp_Master_AUTO_MAIL_GROUP_NAME = "master.auto.mail.group.name";
    public static String Reg_Exp_Master_AUTO_MAIL_GROUP_CODE = "master.auto.mail.group.code";

    public static String Reg_Exp_Master_PORT_GROUP_CODE = "master.port.group.code";
    public static String Reg_Exp_Master_PORT_GROUP_NAME = "master.port.group.name";

    public static String Reg_Exp_Master_PORT_CODE = "master.port.code";
    public static String Reg_Exp_Master_PORT_NAME = "master.port.name";

    public static String Reg_Exp_Master_PORT_CODE_OTHER = "master.port.code.other";
    public static String Reg_Exp_Master_PORT_CODE_AIR = "master.port.code.air";
    public static String Reg_Exp_Master_PORT_CODE_SEA = "master.port.code.sea";

    public static String Reg_Exp_Master_PARTY_NAME = "master.party.name";
    public static String Reg_Exp_Master_PARTY_FIRST_NAME = "master.party.first.name";
    public static String Reg_Exp_Master_PARTY_LAST_NAME = "master.party.last.name";

    public static String Reg_Exp_Master_PARTY_VAT_NO = "master.party.vat.number";
    public static String Reg_Exp_Master_PARTY_PAN_NO = "master.party.pan.number";
    public static String Reg_Exp_Master_PARTY_CST_NO = "master.party.cst.number";
    public static String Reg_Exp_Master_PARTY_SVT_NO = "master.party.svt.number";
    public static String Reg_Exp_Master_PARTY_SVAT_NO = "master.party.svat.number";
    public static String Reg_Exp_Master_PARTY_RAC_NO = "master.party.rac.number";
    public static String Reg_Exp_Master_PARTY_TIN_NO = "master.party.tin.number";
    public static String Reg_Exp_Master_PARTY_IATA_CODE = "master.party.iata.code";
    public static String Reg_Exp_Master_PARTY_REGIONAL_NAME = "master.party.regional.name";
    public static String Reg_Exp_Master_PARTY_BRANCH_SLNO = "master.party.branch.slno";
    public static String Reg_Exp_Master_PARTY_BANK_DLR_CODE = "master.party.bank.drl.code";
    public static String Reg_Exp_Master_PARTY_SHIPPER_VALIDATION_NO = "master.party.shipper.validation.no";
    public static String Reg_Exp_Master_PARTY_ID_NUMBER = "master.party.id.number";
    public static String Reg_Exp_Master_PARTY_BOND_HOLDER = "master.party.bond.holder";
    public static String Reg_Exp_Master_PARTY_BOND_NUMBER = "master.party.bond.no";
    public static String Reg_Exp_Master_PARTY_BOND_SECURITY_CODE = "master.party.bond.security.code";
    public static String Reg_Exp_Master_PARTY_WAREHOUSE_PROVIDER_NAME = "master.party.warehouse.provider.name";
    public static String Reg_Exp_Master_PARTY_PO_BOX = "master.party.po.box";
    public static String Reg_Exp_Master_PARTY_ZIP_CODE = "master.party.zip.code";
    public static String Reg_Exp_Master_PARTY_CONTACT_PERSON = "master.party.contact.person";

    public static String Reg_Exp_Master_PARTY_ACC_NO = "master.party.acc.number";
    public static String Reg_Exp_Master_PARTY_PIN = "master.party.pin";
    public static String Reg_Exp_Master_PARTY_TSANO = "master.party.tsano";
    public static String Reg_Exp_Master_PARTY_SPOTNO = "master.party.spotno";
    public static String Reg_Exp_Master_PARTY_GST_NO = "master.party.gstno";

    public static String Reg_Exp_Master_PARTY_ESTIMATED_REVENUE = "master.party.estimated.revenue";
    public static String Reg_Exp_Master_PARTY_NO_OF_SHIPMENTS = "master.party.no.of.shipments";
    public static String Reg_Exp_Master_PARTY_NO_OF_UNITS = "master.party.no.of.units";

    public static String Reg_Exp_Master_PARTY_CREDIT_DAYS = "master.party.credit.days";
    public static String Reg_Exp_Master_PARTY_PUBLISHED_CREDIT_DAYS = "master.party.published.credit.days";
    public static String Reg_Exp_Master_PARTY_CREDIT_AMOUNT = "master.party.credit.amount";

    public static String REG_EXP_GROSS_WEIGHT = "gross.weight";
    public static String REG_EXP_VOLUME_WEIGHT = "volume.weight";
    public static String REG_EXP_NO_OF_PIECES = "no.of.pieces";
    public static String REG_EXP_GROSS_WEIGHT_POUND = "gross.weight.pound";
    public static String REG_EXP_VOLUME_WEIGHT_POUND = "volume.weight.pound";
    public static String REG_EXP_VOLUME_WEIGHT_CBM = "volume.weight.cbm";
    public static String REG_EXP_GROSS_WEIGHT_KG = "gross.weight.kg";
    public static String REG_EXP_VOLUME_WEIGHT_KG = "volume.weight.kg";
    public static String REG_EXP_RATE_CLASS = "rate.class";
    public static String REG_EXP_RATE_PER_CHARGE = "rate.per.charge";
    public static String REG_EXP_MAWB_NUMBER = "mawb.number";

    public static String REG_EXP_DIMENSION_NO_OF_PIECES = "dimension.no.of.pieces";
    public static String REG_EXP_DIMENSION_LENGTH = "dimension.length";
    public static String REG_EXP_DIMENSION_WIDTH = "dimension.width";
    public static String REG_EXP_DIMENSION_HEIGHT = "dimension.height";
    public static String REG_EXP_DIMENSION_GROSS_WEIGHT = "dimension.gross.weight";

    public static String REG_EXP_FLIGHT_NUMBER = "flight.number";
    public static String REG_EXP_AIRLINE_FLIGHT_NUMBER = "airline.edi.flight.number";

    public static String REG_EXP_SHIPMENT_BROKERAGE_PERCENTAGE = "shipment.brokerage.percentage";

    public static String Reg_Exp_Master_PARTY_CONTACT_PERSONAl_CHILDREN = "party.contact.personal.children";

    public static String Reg_Exp_Master_PARTY_ADD_PICKUP_HOURS = "party.address.pickup.hours";

    public static String Reg_Exp_Master_PARTY_DETAIL_CC_PERCENTAGE = "party.detail.cc.percentage";
    public static String Reg_Exp_Master_PARTY_DETAIL_TDS_PERCENTAGE = "party.detail.tds.percentage";

    public static String Reg_Exp_Master_CURRENCY_RATE = "currency.rate";

    public static String Reg_Exp_FINANCE_ROE = "finance.roe";
    public static String Reg_Exp_FINANCE_UNIT = "finance.unit";
    public static String Reg_Exp_FINANCE_REFERENCE_NO = "finance.reference";

    public static String Reg_Exp_Cfs_Receive_Code = "cfs.receive.code";
    public static String Reg_Exp_Cfs_Receive_Name = "cfs.receive.name";

    public static String Reg_Exp_Cfs_Receive_Pro_Number = "cfs.receive.pro.number";
    public static String Reg_Exp_Cfs_Receive_Warehouse_location = "cfs.receive.warehouse.location";
    public static String Reg_Exp_Cfs_Receive_Shipper_Ref_No = "cfs.receive.shipper.reference.number";
    public static String Reg_Exp_Cfs_Receive_On_Hand = "cfs.receive.on.hand";
    public static String Reg_Exp_Cfs_Receive_Entry_Notes = "cfs.receive.entry.notes";

    public static String Reg_Exp_TRIGGER_TYPE_CODE = "trigger_type.code";
    public static String Reg_Exp_TRIGGER_TYPE_NAME = "trigger_type.name";

    public static String Reg_Exp_AES_NO = "aes.no";
    public static String Reg_Exp_AES_ENTRY_NO = "aes.entry.no";
    public static String Reg_Exp_AES_ID_NO = "aes.id.no";
    public static String Reg_Exp_AES_NAME = "aes.name";
    public static String Reg_Exp_AES_PIECES = "aes.pieces";

    public static String Reg_Exp_AES_FOREIGN_TRADE_ZONE = "aes.foreign.trade.zone";
    public static String Reg_Exp_AES_ADDRESS = "aes.address";
    public static String Reg_Exp_AES_CONTACT_NO = "aes.contact.no";
    public static String Reg_Exp_AES_ZIP_CODE = "aes.zip.code";
    public static String Reg_Exp_AES_VALUE_USD = "aes.value.usd";
    public static String Reg_Exp_AES_GROSS_WEIGHT = "aes.gross.weight";

    public static String Reg_Exp_STOCK_CARRIER_REF_NO = "stock.carrierref.no";

    public static String Reg_Exp_FLIGHT_PLAN_CAPACITYRESERVED = "flightplan.capacityReserved";
    public static String Reg_Exp_FLIGHT_PLAN_FLIGHTFREQUENCY = "flightplan.flightFrequency";
    public static String Reg_Exp_FLIGHT_PLAN_FLIGHTNUMBER = "flightplan.flightNumber";

    public static String Reg_Exp_PO_ALPHA_TEN = "po.alphalengthten";
    public static String Reg_Exp_PO_ALPHA_THIRTY = "po.alphalengththirty";
    public static String Reg_Exp_PO_ALPHA_FIFTY = "po.alphalengthfifty";
    public static String Reg_Exp_PO_ALPHA_HUNDRED = "po.alphalengthhundred";
    public static String Reg_Exp_PO_ALPHA_TWOFIFTY = "po.alphalengthtwofifty";
    public static String Reg_Exp_PO_ALPHA_FVEHND = "po.alphalengthfivehnd";
    public static String Reg_Exp_PO_PERC = "po.percentage";

    public static String Reg_Exp_Master_DefaultMaster_Code = "master.defaultmaster.code";

    // bank master
    public static String Reg_Exp_Master_Bank_Name = "bank.name";
    public static String Reg_Exp_Master_Bank_Code = "bank.code";
    public static String Reg_Exp_Master_Bank_Smart_Bank_Code = "bank.smart.bank.code";

    // event master
    public static String Reg_Exp_MASTER_EVENT_NAME = "event.name";
    public static String Reg_Exp_MASTER_EVENT_CODE = "event.code";

    // region master
    public static String Reg_Exp_Master_Region_Name = "region.name";
    public static String Reg_Exp_Master_Region_Code = "region.code";

    // Aes Filer master
    public static String REG_EXP_MASTER_AES_FILER_CODE = "aesfiler.code";
    public static String REG_EXP_MASTER_AES_FILER_TRANSMITTER_CODE = "aesfiler.transmitterCode";


    public static String REG_EXP_EMPLOYEE_CODE = "employee.code";
    public static String REG_EXP_EMPLOYEE_NAME = "employee.name";

    //service tax category
    public static String Reg_Exp_Master_ServiceTaxCategroy_Name = "servicetaxcategory.name";
    public static String Reg_Exp_Master_ServiceTaxCategroy_Code = "servicetaxcategory.code";


    public static String Reg_Exp_Master_Charter_Code = "master.charter.code";
    public static String Reg_Exp_Master_Charter_Name = "master.charter.name";

    //comment master
    public static String Reg_Exp_Master_Comment_Code = "master.comment.code";
    public static String Reg_Exp_Master_Comment_Name = "master.comment.name";

    //daybook master
    public static String Reg_Exp_Master_Daybook_Code = "master.Daybook.code";
    public static String Reg_Exp_Master_Daybook_Name = "master.Daybook.name";

    public static String Reg_Exp_Master_Daybook_Cheque_From = "master.Daybook.cheque.from";
    public static String Reg_Exp_Master_Daybook_Cheque_To = "master.Daybook.cheque.to";
    public static String Reg_Exp_Master_Daybook_Report_Name = "master.Daybook.report.name";

    //location master
    public static String Reg_Exp_Master_Location_Code = "master.location.code";
    public static String Reg_Exp_Master_Location_Name = "master.location.name";

    public static String MASTER_COMPANY_CODE = "master.company.code";
    public static String MASTER_COMPANY_NAME = "master.company.name";

    // buyer consolidation master
    public static String Reg_Exp_Master_Buyer_Consolidation_Name = "buyer.bcname";
    public static String Reg_Exp_Master_Buyer_Consolidation_Code = "buyer.bccode";

    // bill of entry
    public static String Reg_Exp_Master_Bill_of_Entry = "billofentry.billofentry";


}
