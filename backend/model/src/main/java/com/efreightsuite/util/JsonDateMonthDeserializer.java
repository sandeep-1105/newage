package com.efreightsuite.util;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import lombok.extern.log4j.Log4j2;

import org.springframework.stereotype.Component;

@Log4j2
@Component
public class JsonDateMonthDeserializer extends JsonDeserializer<Date> {

    @Override
    public Date deserialize(JsonParser jsonparser, DeserializationContext ctxt) throws IOException {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM");
        Date date = null;
        try {
            date = dateFormat.parse(jsonparser.getText());
            log.info(jsonparser.getCurrentName());

            if (jsonparser.getCurrentName().equals("financialEndDate")) {

                Calendar calendar = Calendar.getInstance();
                calendar.setTime(date);
                calendar.add(Calendar.MONTH, 1);
                date = calendar.getTime();
                calendar.setTime(date);
                calendar.add(Calendar.DAY_OF_YEAR, -1);
                date = calendar.getTime();

                //date = TimeUtil.addDate(date, -1);
            }
        } catch (ParseException e) {
            log.error("Date paraser invaild [" + jsonparser.getText() + "], " + e.getMessage());
        }
        log.info("==================================");
        log.info("JSON " + jsonparser.getText() + " Date " + date);
        log.info("==================================");
        return date;

    }

}
