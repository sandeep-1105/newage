package com.efreightsuite.util;

import java.io.IOException;

import com.efreightsuite.enumeration.BooleanType;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import org.springframework.stereotype.Component;

@Component
public class JsonBooleanTypeSerializer extends JsonSerializer<BooleanType> {


    @Override
    public void serialize(BooleanType value, JsonGenerator gen, SerializerProvider provider) throws IOException {
        if (value != null && value == BooleanType.TRUE) {
            gen.writeObject(true);
        } else {
            gen.writeObject(false);
        }

    }
}
