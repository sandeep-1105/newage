package com.efreightsuite.util;

import java.io.IOException;

import com.efreightsuite.enumeration.WhoRouted;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import org.springframework.stereotype.Component;

@Component
public class JsonWhoRoutedSerializer extends JsonSerializer<WhoRouted> {

    @Override
    public void serialize(WhoRouted value, JsonGenerator gen, SerializerProvider provider) throws IOException {
        if (value != null && value == WhoRouted.Agent) {
            gen.writeObject(true);
        } else {
            gen.writeObject(false);
        }

    }
}
