package com.efreightsuite.util;

import java.io.IOException;

import com.efreightsuite.enumeration.BondType;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import org.springframework.stereotype.Component;

@Component
public class JsonBondTypeSerializer extends JsonSerializer<BondType> {

    @Override
    public void serialize(BondType value, JsonGenerator gen, SerializerProvider provider) throws IOException {
        if (value != null && value == BondType.SINGLE_TRANSACTION) {
            gen.writeObject(true);
        } else if (value != null && value == BondType.CONTINIUOUS) {
            gen.writeObject(false);
        } else {
            gen.writeObject(null);
        }


    }
}
