package com.efreightsuite.util;

import java.io.IOException;

import com.efreightsuite.enumeration.DimensionUnit;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import org.springframework.stereotype.Component;

@Component
public class JsonDimensionUnitSerializer extends JsonSerializer<DimensionUnit> {

    @Override
    public void serialize(DimensionUnit value, JsonGenerator gen, SerializerProvider provider) throws IOException {
        if (value != null && value == DimensionUnit.INCHORPOUNDS) {
            gen.writeObject(true);
        } else if (value != null && value == DimensionUnit.CENTIMETERORKILOS) {
            gen.writeObject(false);
        } else {
            gen.writeObject(null);
        }


    }
}
