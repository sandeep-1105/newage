package com.efreightsuite.util;

import java.io.IOException;

import com.efreightsuite.enumeration.FillingType;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import org.springframework.stereotype.Component;

@Component
public class JsonFillingTypeSerializer extends JsonSerializer<FillingType> {

    @Override
    public void serialize(FillingType value, JsonGenerator gen, SerializerProvider provider) throws IOException {
        if (value != null && value == FillingType.POST_SHIPMENT) {
            gen.writeObject(true);
        } else if (value != null && value == FillingType.PRE_SHIPMENT) {
            gen.writeObject(false);
        } else {
            gen.writeObject(null);
        }


    }
}
