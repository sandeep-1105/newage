package com.efreightsuite.util;

import java.io.IOException;

import com.efreightsuite.enumeration.FillingType;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

import org.springframework.stereotype.Component;

@Component
public class JsonFillingTypeDeserializer extends JsonDeserializer<FillingType> {

    @Override
    public FillingType deserialize(JsonParser jsonparser, DeserializationContext ctxt) throws IOException {

        String string = jsonparser.getText();

        if (string != null && string.trim().length() != 0) {
            if (string.trim().toUpperCase().equals("TRUE")) {
                return FillingType.POST_SHIPMENT;
            } else {
                return FillingType.PRE_SHIPMENT;
            }
        }
        return null;

    }

}
