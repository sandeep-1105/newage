package com.efreightsuite.util;

import com.efreightsuite.dto.EnquiryLogDto;
import com.efreightsuite.dto.PartyDto;
import com.efreightsuite.model.EnquiryLog;
import com.efreightsuite.model.PartyMaster;

public class ModelToDtoConversion {

    public static EnquiryLogDto enquiryModelToenquiryDto(EnquiryLog enquiry) {

        EnquiryLogDto dto = new EnquiryLogDto();

        if (enquiry == null)
            return null;


        dto.setEnquiryNo(enquiry.getEnquiryNo());
        dto.setId(enquiry.getId());
        dto.setReceivedOn(enquiry.getReceivedOn());
        dto.setQuoteBy(enquiry.getQuoteBy());

        dto.setPartyDto(partyModelToPartyDto(enquiry.getPartyMaster()));

        return dto;

    }

    public static PartyDto partyModelToPartyDto(PartyMaster party) {

        PartyDto dto = new PartyDto();

        if (party == null)
            return null;


        dto.setId(party.getId());
        dto.setPartyCode(party.getPartyCode());
        dto.setPartyName(party.getPartyName());
        dto.setStatus(party.getStatus());


        return dto;

    }
}
