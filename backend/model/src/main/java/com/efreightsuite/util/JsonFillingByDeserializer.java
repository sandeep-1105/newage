package com.efreightsuite.util;

import java.io.IOException;

import com.efreightsuite.enumeration.FillingBy;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

import org.springframework.stereotype.Component;

@Component
public class JsonFillingByDeserializer extends JsonDeserializer<FillingBy> {

    @Override
    public FillingBy deserialize(JsonParser jsonparser, DeserializationContext ctxt) throws IOException {

        String string = jsonparser.getText();

        if (string != null && string.trim().length() != 0) {
            if (string.trim().toUpperCase().equals("TRUE")) {
                return FillingBy.FORWARDER;
            } else {
                return FillingBy.SELF;
            }
        }
        return null;

    }

}
