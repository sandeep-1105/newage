package com.efreightsuite.util;

import java.io.IOException;

import com.efreightsuite.enumeration.FillingBy;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import org.springframework.stereotype.Component;

@Component
public class JsonFillingBySerializer extends JsonSerializer<FillingBy> {

    @Override
    public void serialize(FillingBy value, JsonGenerator gen, SerializerProvider provider) throws IOException {
        if (value != null && value == FillingBy.FORWARDER) {
            gen.writeObject(true);
        } else if (value != null && value == FillingBy.SELF) {
            gen.writeObject(false);
        } else {
            gen.writeObject(null);
        }


    }
}
