package com.efreightsuite.util.edi;

public enum EdiUpdate {
    INITIATE, PROGRESS, PENDING, COMPLETE
}
