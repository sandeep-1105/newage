package com.efreightsuite.util;

import java.io.IOException;

import com.efreightsuite.enumeration.YesNo;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import org.springframework.stereotype.Component;

@Component
public class JsonYesNoSerializer extends JsonSerializer<YesNo> {

    @Override
    public void serialize(YesNo value, JsonGenerator gen, SerializerProvider provider) throws IOException {
        if (value != null && value == YesNo.Yes) {
            gen.writeObject(true);
        } else {
            gen.writeObject(false);
        }

    }
}
