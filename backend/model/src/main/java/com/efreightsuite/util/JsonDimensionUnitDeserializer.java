package com.efreightsuite.util;

import java.io.IOException;

import com.efreightsuite.enumeration.DimensionUnit;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

import org.springframework.stereotype.Component;

@Component
public class JsonDimensionUnitDeserializer extends JsonDeserializer<DimensionUnit> {

    @Override
    public DimensionUnit deserialize(JsonParser jsonparser, DeserializationContext ctxt) throws IOException {

        String string = jsonparser.getText();

        if (string != null && string.trim().length() != 0) {
            if (string.trim().toUpperCase().equals("TRUE")) {
                return DimensionUnit.INCHORPOUNDS;
            } else {
                return DimensionUnit.CENTIMETERORKILOS;
            }
        }
        return null;

    }

}
