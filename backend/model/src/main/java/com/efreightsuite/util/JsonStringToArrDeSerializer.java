package com.efreightsuite.util;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;

import org.springframework.stereotype.Component;

@Component
@Log4j2
public class JsonStringToArrDeSerializer extends JsonDeserializer<String> {

    @Override
    public String deserialize(JsonParser jsonParser, DeserializationContext ctxt) throws IOException {

        String str = "";
        if (jsonParser.getCurrentToken() == JsonToken.START_ARRAY) {
            while (jsonParser.nextToken() != JsonToken.END_ARRAY) {
                str = str + jsonParser.getValueAsString() + ",";
                log.info("String " + jsonParser.getValueAsString());
            }
        }
        if (str.length() > 0) {
            return StringUtils.stripEnd(str, ",");
        } else {
            return null;
        }

    }

}
