package com.efreightsuite.util.edi;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class EdiStatusUpdater {

    private EdiStatus ediStatus;

    public EdiStatusUpdater() {

    }

    public EdiStatusUpdater(EdiStatus ediStatus) {
        this.ediStatus = ediStatus;
    }

    public void update(EdiUpdate ediUpdate) {
        this.ediStatus.update(ediUpdate);
    }
}
