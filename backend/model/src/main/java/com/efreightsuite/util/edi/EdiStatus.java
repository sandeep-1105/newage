package com.efreightsuite.util.edi;

public interface EdiStatus {
    public void update(EdiUpdate ediUpdate);
}
