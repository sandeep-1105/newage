package com.efreightsuite.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.log4j.Log4j2;

@Log4j2
public class JsonToObjectConverter {

    @SuppressWarnings("unchecked")
    public static Object converterJsonToObject(String jsonString, Class clas) {

        log.info("JsonToObjectConverter.converterJsonToObject start");
        ObjectMapper mapper = new ObjectMapper();

        Object object = null;
        try {
            // Convert JSON string to Object
            object = mapper.readValue(jsonString, clas);

        } catch (Exception e) {
            log.debug("Exception in JsonToObjectConverter.converterJsonToObject converting JSON to Object", e);
            return null;
        }

        log.info("JsonToObjectConverter.converterJsonToObject start");
        return object;

    }

}
