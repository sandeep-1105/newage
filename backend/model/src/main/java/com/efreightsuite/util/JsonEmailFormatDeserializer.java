package com.efreightsuite.util;

import java.io.IOException;

import com.efreightsuite.enumeration.EmailFormat;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

import org.springframework.stereotype.Component;

@Component
public class JsonEmailFormatDeserializer extends JsonDeserializer<EmailFormat> {

    @Override
    public EmailFormat deserialize(JsonParser jsonparser, DeserializationContext ctxt) throws IOException {

        String string = jsonparser.getText();

        if (string != null && string.trim().length() != 0) {
            if (string.trim().toUpperCase().equals("TRUE")) {
                return EmailFormat.HTML;
            } else {
                return EmailFormat.TEXT;
            }
        }
        return null;

    }

}
