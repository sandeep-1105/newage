package com.efreightsuite.util;

public class StaticSearchQuery {
    public static String DocumentIssueRestrictionSearch = "select di from DocumentIssueRestriction di left join di.serviceMaster sm left join di.locationMaster loc left join di.companyMaster com ";

    public static String PurchaseOrderSearch = "select new com.efreightsuite.dto.PurchaseOrderListResponseDto(po.id, po.buyer.bcName, po.supplier.partyName,po.poNo,"
            + " po.poDate, po.origin.portName ,po.destination.portName, po.transportMode, po.tosMaster.tosName, po.status) from PurchaseOrder po left join po.buyer buy left join po.supplier su left join po.origin ori "
            + "left join po.destination dest left join po.originAgent oriAg left join po.destinationAgent destAg left join po.tosMaster tos ";


    public static String BuyerConsolidationSearch = "select bcm from BuyerConsolidationMaster bcm ";

    public static String DynamicFieldSearch = "select df from DynamicFields df ";
}
