package com.efreightsuite.util;

import java.io.IOException;

import com.efreightsuite.enumeration.Gender;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

import org.springframework.stereotype.Component;

@Component
public class JsonGenderDeserializer extends JsonDeserializer<Gender> {

    @Override
    public Gender deserialize(JsonParser jsonparser, DeserializationContext ctxt) throws IOException {

        String string = jsonparser.getText();

        if (string != null && string.trim().length() != 0) {
            if (string.trim().toUpperCase().equals("TRUE")) {
                return Gender.Male;
            } else {
                return Gender.Female;
            }
        }
        return Gender.Male;

    }

}
