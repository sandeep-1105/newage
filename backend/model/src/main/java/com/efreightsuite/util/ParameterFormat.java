package com.efreightsuite.util;

import java.util.HashMap;
import java.util.Map;

import com.efreightsuite.exception.ErrorCode;

/**
 * The Class ParameterFormat.
 */
public class ParameterFormat {

    private static Map<String, String[]> mapExpression;

    static {

        mapExpression = new HashMap<>();

        mapExpression.put("eventCode", new String[]{"[A-Z]{1,3}", ErrorCode.MASTER_EVENT_CODE});
        mapExpression.put("eventName", new String[]{"[ 0-9a-zA-Z-'.,&@/:;?!()$#*_]{0,100}", ErrorCode.MASTER_EVENT_NAME});

	/*  mapExpression.put("cfsCode", new String[] { "[A-Z]{1,3}",ErrorCode.MASTER_CFS_CODE   });
        mapExpression.put("cfsName",new String[] { "[ 0-9a-zA-Z-'.,&@/:;?!()$#*_]{0,100}", ErrorCode.MASTER_CFS_NAME  });

		mapExpression.put("pro.number",new String[] { "[ 0-9a-zA-Z-'.,&@/:;?!()$#*_]{0,100}", ErrorCode.AIR_CFS_RECEIVE_PRO_NUMBER_INVALID});
		mapExpression.put("warehouse.location",new String[] { "[ 0-9a-zA-Z-'.,&@/:;?!()$#*_]{0,100}", ErrorCode.AIR_CFS_RECEIVE_WAREHOUSE_LOCATION_INVALID});
		mapExpression.put("shipper.reference.number",new String[] { "[ 0-9a-zA-Z-'.,&@/:;?!()$#*_]{0,100}", ErrorCode.AIR_CFS_RECEIVE_SHIPPER_REFERENCE_INVALID});
		mapExpression.put("on.hand",new String[] { "[ 0-9a-zA-Z-'.,&@/:;?!()$#*_]{0,100}", ErrorCode.AIR_CFS_RECEIVE_ON_HAND_INVALID});
		mapExpression.put("receive.entry.notes",new String[] { "[ 0-9a-zA-Z-'.,&@/:;?!()$#*_]{0,4000}", ErrorCode.AIR_CFS_RECEIVE_NOTES_INVALID});

	*/


        mapExpression.put("languageName", new String[]{"[a-zA-Z0-9]{0,19}", ErrorCode.LANGUAGE_NAME_INVALID});
        mapExpression.put("languageCode", new String[]{"[a-zA-Z0-9]{0,19}", ErrorCode.LANGUAGE_CODE_INVALID});
        mapExpression.put("languageDesc", new String[]{"[a-zA-Z0-9][a-zA-Z0-9 ]{2,999}", ErrorCode.LANGUAGE_DESC_INVALID});
        mapExpression.put("languageGroup", new String[]{"[a-zA-Z0-9]{0,19}", ErrorCode.LANGUAGE_GROUP_INVALID});


        mapExpression.put("companyCode", new String[]{"[a-zA-Z][a-zA-Z0-9]{2,9}", ErrorCode.COMPANY_CODE_INVALID});
        mapExpression.put("companyName", new String[]{"[a-zA-Z0-9][a-zA-Z0-9 ]{2,99}", ErrorCode.COMPANY_NAME_INVALID});

        mapExpression.put("stateCode", new String[]{"[a-zA-Z][a-zA-Z0-9]{1,10}", ErrorCode.STATE_CODE_INVALID});
        mapExpression.put("stateName", new String[]{"[a-zA-Z0-9][a-zA-Z0-9 ]{1,100}", ErrorCode.STATE_NAME_INVALID});

        mapExpression.put("zoneCode", new String[]{"[a-zA-Z][a-zA-Z0-9]{2,9}", ErrorCode.ZONE_CODE_INVALID});
        mapExpression.put("zoneName", new String[]{"[a-zA-Z0-9][a-zA-Z0-9 ]{2,99}", ErrorCode.ZONE_NAME_INVALID});

        mapExpression.put("documentPrefixDocType", new String[]{"[a-zA-Z0-9]{0,100}", ErrorCode.DOCUMENT_PREFIXM_DOCTYPE_LENGTH_INVALID});
        mapExpression.put("documentPrefixReportName", new String[]{"[a-zA-Z0-9]{0,100}", ErrorCode.DOCUMENT_PREFIXM_DOCTYPE_LENGTH_INVALID});

        mapExpression.put("divisionCode", new String[]{"[a-zA-Z0-9]{1,10}", ErrorCode.DIVISION_CODE_INVALID});
        mapExpression.put("divisionName", new String[]{"[a-zA-Z0-9 ]{1,100}", ErrorCode.DIVISION_NAME_INVALID});

        mapExpression.put("locationCode", new String[]{"[a-zA-Z0-9]{1,10}", ErrorCode.LOCATION_CODE_INVALID});
        mapExpression.put("locationName", new String[]{"[a-zA-Z0-9 ]{1,100}", ErrorCode.LOCATION_NAME_INVALID});

		/*mapExpression.put("triggerTypeCode",new String[] { "[a-zA-Z0-9]{0,10}", ErrorCode.TRIGGER_TYPE_CODE_INVALID  });
		mapExpression.put("triggerTypeName",new String[] { "[a-zA-Z0-9 ]{0,100}", ErrorCode.TRIGGER_TYPE_NAME_INVALID  });
*/



		/*mapExpression.put("enquiryDimension.noOfPieces", new String[] { "[0-9]{1,10}", ErrorCode.ENQUIRYLOG_PIECES_INVALID });
		mapExpression.put("enquiryDimension.length", new String[] { "[0-9]{1,10}", ErrorCode.ENQUIRYLOG_LENGTH_INVALID });
		mapExpression.put("enquiryDimension.grossWeight", new String[] { "[0-9]{1,10}+(.[0-9]{1,3})", ErrorCode.ENQUIRYLOG_GROSSWEIGHT_INVALID });
		mapExpression.put("enquiryDimension.height", new String[] { "[0-9]{1,10}", ErrorCode.ENQUIRYLOG_HEIGHT_INVALID });
		mapExpression.put("enquiryDimension.width", new String[] { "[0-9]{1,10}", ErrorCode.ENQUIRYLOG_WIDTH_INVALID });*/

        mapExpression.put("enquiry.attachment.refno", new String[]{"[a-zA-Z0-9 ]{0,100}", ErrorCode.ENQUIRY_ATTACHMENT_REF_NO_INVALID});

        mapExpression.put("enquiryLog.partyCode", new String[]{"[C][0-9]{0,6}", ErrorCode.ENQUIRYLOG_PARTYCODE_INVALID});
        mapExpression.put("enquiryDetail.serviceCode", new String[]{"[a-zA-Z0-9][a-zA-Z0-9]{0,10}", ErrorCode.ENQUIRYLOG_SERVICECODE_INVALID});
        mapExpression.put("enquiryDetail.tosCode", new String[]{"[a-zA-Z]{1,3}", ErrorCode.ENQUIRYLOG_TOSCODE_INVALID});
        mapExpression.put("enquiryDetail.hsCode", new String[]{"[a-zA-Z0-9]{1,10}", ErrorCode.ENQUIRYLOG_COMMODITYGROUPCODE_INVALID});
        mapExpression.put("enquiryDetail.remarks", new String[]{"[ A-Za-z0-9\n_@.#&+-]{0,4000}", ErrorCode.ENQUIRYLOG_REMARKS_INVALID});
        mapExpression.put("nquiryDetail.pickUpAddress", new String[]{"[ A-Za-z0-9\n_@.,*#&+-]{0,500}", ErrorCode.ENQUIRYLOG_PICKUP_ADDRESS_INVALID});
        mapExpression.put("nquiryDetail.deliveryAddress", new String[]{"[ A-Za-z0-9\n_@,*.#&+-]{0,500}", ErrorCode.ENQUIRYLOG_DELIVERY_ADDRESS_INVALID});

		/*mapExpression.put("partyMaster.partyCode", new String[] { "[C][0-9]{0,6}", ErrorCode.PARTY_CODE_INVALID });
		mapExpression.put("partyMaster.partyName", new String[] { "[a-zA-Z0-9 ]{0,100}", ErrorCode.PARTY_NAME_INVALID });
		mapExpression.put("partyMaster.partyFirstName", new String[] { "[a-zA-Z ]{0,100}", ErrorCode.PARTY_FIRST_NAME_INVALID });
		mapExpression.put("partyMaster.partyLastName", new String[] { "[a-zA-Z ]{0,100}", ErrorCode.PARTY_LAST_NAME_INVALID });*/

		/*mapExpression.put("partyMaster.partyCreditLimit.creditDays", new String[] { "[0-9]{0,3}", ErrorCode.PARTY_CREDIT_CREDITDAYS_INVALID });
		mapExpression.put("partyMaster.partyCreditLimit.publishCreditDays", new String[] { "[0-9]{0,3}", ErrorCode.PARTY_CREDIT_PUBLICCREDITDAYS_INVALID });
		mapExpression.put("partyMaster.partyCreditLimit.creditAmount", new String[] { "[0-9]{0,10}", ErrorCode.PARTY_CREDIT_CREDITAMOUNT_INVALID });
		*/
		/*mapExpression.put("partyAddressMaster.zipCode", new String[] { "[A-Z0-9a-z ]{0,6}$", ErrorCode.PARTY_ZIP_CODE_INVALID });*/
		/*mapExpression.put("partyAddressMaster.poBox", new String[] { "[A-Z0-9]{0,10}", ErrorCode.PARTY_PO_BOX_INVALID });*/


		/*mapExpression.put("party.contact.office.phone", new String[] {  "[0-9-'.,&@+/:;?! ()$#*_]{0,15}", ErrorCode.PARTY_CONTACT_OFFICIAL_PHONE_NO_INVALID });
		mapExpression.put("party.contact.office.mobile", new String[] {  "[0-9-'.,&@+/:;?! ()$#*_]{0,15}", ErrorCode.PARTY_CONTACT_OFFICIAL_MOBILE_NO_INVALID });
		mapExpression.put("party.contact.assitant.phone", new String[] {  "[0-9-'.,&@+/:;?! ()$#*_]{0,15}", ErrorCode.PARTY_CONTACT_OFFICIAL_ASSISTENT_PHONE_INVALID });
		mapExpression.put("party.contact.personal.phone", new String[] {  "[0-9-'.,&@+/:;?! ()$#*_]{0,15}", ErrorCode.PARTY_CONTACT_PERSONAL_PHONE_NO_INVALID });
		mapExpression.put("party.contact.personal.mobile", new String[] {  "[0-9-'.,&@+/:;?! ()$#*_]{0,15}", ErrorCode.PARTY_CONTACT_PERSONAL_MOBILE_NO_INVALID });
		mapExpression.put("party.contact.personal.children", new String[] {  "[0-9]{0,5}", ErrorCode.PARTY_CONTACT_PERSONAL_CHILDREN_INVALID });
		*/

		/*mapExpression.put("partyAddressMaster.phoneNumber", new String[] {  "[0-9-'.,&@+/:;?! ()$#*_]{0,100}", ErrorCode.PARTY_PHONE_NUMBER_INVALID });
		mapExpression.put("partyAddressMaster.mobileNumber", new String[] {  "[ 0-9A-Z-'.,&@+/:;?! ()$#*_]{0,100}", ErrorCode.PARTY_MOBILE_NUMBER_INVALID });
		mapExpression.put("partyAddressMaster.email", new String[] {"^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$", ErrorCode.PARTY_EMAIL_INVALID });
		*//*mapExpression.put("partyAddressMaster.contactPerson", new String[] { "[a-zA-Za-z. ]{0,100}", ErrorCode.PARTY_CONTACT_PERSON_INVALID });*/
		/*mapExpression.put("partyAddressMaster.pickUpHourText", new String[] { "[a-zA-Z0-9_@.(){}/#& +-:,!%&^']{0,255}", ErrorCode.PARTY_PICK_HOUR_TEXT_INVALID });

		mapExpression.put("party.contact.personal.email", new String[] {"^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$", ErrorCode.PARTY_CONTCT_PERSONAL_EMAIL });
		mapExpression.put("party.contact.offical.email", new String[] {"^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$", ErrorCode.PARTY_CONTCT_OFFICE_EMAIL });
		*/
		/*mapExpression.put("partyCountryField.bankDLRCode", new String[] { "[A-Z0-9]{0,25}", ErrorCode.PARTY_BANK_DLR_CODE_INVALID });*/
		/*mapExpression.put("partyCountryField.knownShipperNumber", new String[] { "[a-zA-Z0-9]{0,50}", ErrorCode.PARTY_KNOWN_SHIPPER_NUMBER_INVALID });*/
		/*mapExpression.put("partyCountryField.idNumber", new String[] { "[0-9]{0,9}", ErrorCode.PARTY_ID_NUMBER_INVALID });*/
		/*mapExpression.put("partyCountryField.branchSlNo", new String[] { "[0-9]{0,5}", ErrorCode.PARTY_BRANCH_SERIAL_NO_INVALID });*/
		/*mapExpression.put("partyCountryField.wareHouseProviderName", new String[] { "[a-zA-Z0-9 ]{0,100}", ErrorCode.PARTY_WAREHOUSE_PROVIDER_NAME_INVALID });*/

		/*mapExpression.put("partyCountryField.bondHolder", new String[] { "[a-zA-Z0-9 ]{0,100}", ErrorCode.PARTY_BOND_HOLDER_INVALID });*/
		/*mapExpression.put("partyCountryField.bondNo", new String[] { "[A-Z0-9]{0,20}", ErrorCode.PARTY_BOND_NUMBER_INVALID });*/
		/*mapExpression.put("partyCountryField.bondSuretyCode", new String[] { "[A-Z0-9a-z ]{0,20}", ErrorCode.PARTY_BOND_SURETY_CODE_INVALID });*/

/*		mapExpression.put("partyMasterDetail.vatNumber", new String[] { "[A-Z0-9a-z ]{0,30}", ErrorCode.PARTY_VAT_NUMBER_INVALID });
		mapExpression.put("partyMasterDetail.panNumber", new String[] { "[A-Z0-9a-z ]{0,10}", ErrorCode.PARTY_PAN_NUMBER_INVALID });
		mapExpression.put("partyMasterDetail.cstNumber", new String[] { "[0-9]{0,30}", ErrorCode.PARTY_CST_NUMBER_INVALID });
		mapExpression.put("partyMasterDetail.svtNumber", new String[] { "[A-Z0-9a-z ]{0,30}", ErrorCode.PARTY_SVT_NUMBER_INVALID });
		mapExpression.put("partyMasterDetail.svatNumber", new String[] { "[A-Z0-9a-z ]{0,30}", ErrorCode.PARTY_SVAT_NUMBER_INVALID });
		mapExpression.put("partyMasterDetail.racNumber", new String[] { "[A-Z0-9a-z ]{0,30}", ErrorCode.PARTY_RAC_NUMBER_INVALID });
		mapExpression.put("partyMasterDetail.tinNumber", new String[] { "[0-9]{0,30}", ErrorCode.PARTY_TIN_NUMBER_INVALID });
		mapExpression.put("partyMasterDetail.iataCode", new String[] { "[0-9]{0,30}", ErrorCode.PARTY_IATA_CODE_INVALID });*/




		/*mapExpression.put("serviceTypeCode",new String[] { "[a-zA-Z0-9 ]{1,20}", ErrorCode.SERVICETYPE_CODE_INVALID });
		mapExpression.put("serviceTypeName",new String[] { "[a-zA-Z0-9 ]{1,100}",ErrorCode.SERVICETYPE_NAME_INVALID });

		*/

		/*mapExpression.put("enquiry.attachment.refno", new String[] { "[a-zA-Z0-9 ]{0,100}", ErrorCode.ENQUIRY_ATTACHMENT_REF_NO_INVALID});*/
		/*mapExpression.put("quotation.attention", new String[] { "[a-zA-Z0-9 ]{0,100}", ErrorCode.QUOTATION_ATTENTION_INVALID});*/
        mapExpression.put("quotation.chargeName", new String[]{"([ A-Za-z0-9_@.(){}/#& $+-,!%&^'])*{1,100}", ErrorCode.QUOTATION_CHARGE_NAME_INVALID});
        mapExpression.put("quotation.attachment.refno", new String[]{"[a-zA-Z0-9 ]{0,100}", ErrorCode.QUOTATION_ATTACHMENT_REF_NO_INVALID});
		/*mapExpression.put("attachment.refno", new String[] { "[a-zA-Z0-9 ]{0,100}", ErrorCode.ATTACHMENT_REFERENCE_NUMBER_INVALID});*/

		/*mapExpression.put("estimatedRevenue",new String[] { "^[0-9]{1,5}+(.[0-9]{1,6})?$", ErrorCode.ESTIMATED_REVENUE_INVALID  });
		mapExpression.put("businessnoofunits",new String[] { "^[0-9]{0,30}", ErrorCode.BUSINESS_NOOFUNITS_INVALID  });
		*/
        mapExpression.put("email", new String[]{"^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$", ErrorCode.PARTY_EMAIL_INVALID});

		/*mapExpression.put("flightNumber",new String[] { "[a-zA-Z0-9]{0,10}", ErrorCode.SHIPMENT_FLIGHT_NO  });*/

		/*mapExpression.put("shipment.chargeName", new String[] { "([ a-zA-Za-z0-9_@.(){}/#& $+-,!%&^'])*{1,100}", ErrorCode.SHIPMENT_CHARGE_NAME_INVALID});*/
		/*mapExpression.put("serviceStatusNote", new String[] { "[  A-Za-z0-9\n_@.#&+-]{0,4000}", ErrorCode.SHIPMENT_STATUS_NOTE_INVALID});*/

		/*mapExpression.put("shipmentReferenceNumber", new String[] { "[  A-Za-z0-9\n_@.#&+-]{0,30}", ErrorCode.SHIPMENT_REFERENCE_NUMBER_INVALID});*/

        mapExpression.put("consol.chargeName", new String[]{"([ a-zA-Za-z0-9_@.(){}/#& $+-,!%&^'])*{1,100}", ErrorCode.CONSOL_CHARGE_NAME_INVALID});
        mapExpression.put("flightPlan.flightNo", new String[]{"[a-zA-Z0-9]{0,10}", ErrorCode.FLIGHTPLAN_CONNECTION_FLIGHT_NO_INVALID});

        mapExpression.put("carrierRefNo", new String[]{"[a-zA-Z0-9 ]{0,30}", ErrorCode.STOCK_CARRIER_REFNO_INVALID});


        mapExpression.put("flightPlan.flightNo", new String[]{"[a-zA-Z0-9_#+-]{0,10}", ErrorCode.FLIGHTPLAN_CONNECTION_FLIGHT_NO_INVALID});

		/*
		mapExpression.put("ultimateConsigneeIdNo",new String[] { "[a-zA-Z0-9 ]{1,11}", ErrorCode.AES_CONSIGNEE_ID_NO_INVALID });
		mapExpression.put("ultimateConsigneeFrstName",new String[] { "[a-zA-Z0-9 ]{1,100}", ErrorCode.AES_CONSIGNEE_FIRST_NAME_INVALID  });
		mapExpression.put("ultimateConsigneeSecondName",new String[] { "[a-zA-Z0-9 ]{1,100}", ErrorCode.AES_CONSIGNEE_SECOND_NAME_INVALID  });
		mapExpression.put("importentryno",new String[] { "[a-zA-Z0-9 ]{1,15}", ErrorCode.AES_IMPORT_ENTRY_NO_INVALID });

		mapExpression.put("ultimateShipperIdNo",new String[] { "[a-zA-Z0-9 ]{1,11}", ErrorCode.AES_CONSIGNEE_ID_NO_INVALID });
		mapExpression.put("ultimateShipperFrstName",new String[] { "[a-zA-Z0-9 ]{1,100}", ErrorCode.AES_CONSIGNEE_FIRST_NAME_INVALID  });
		mapExpression.put("ultimateShipperSecondName",new String[] { "[a-zA-Z0-9 ]{1,100}", ErrorCode.AES_CONSIGNEE_SECOND_NAME_INVALID  });

		mapExpression.put("forwarderFirstName",new String[] { "[a-zA-Z0-9 ]{1,100}", ErrorCode.AES_FORWARDER_FIRST_NAME_INVALID  });
		mapExpression.put("forwarderSecondName",new String[] { "[a-zA-Z0-9 ]{1,100}", ErrorCode.AES_FORWARDER_SECOND_NAME_INVALID  });

		mapExpression.put("intermediateconsFrstName",new String[] { "[a-zA-Z0-9 ]{1,100}", ErrorCode.AES_INT_CONSIGNEE_FIRST_NAME_INVALID  });
		mapExpression.put("intermediateconsSecondName",new String[] { "[a-zA-Z0-9 ]{1,100}", ErrorCode.AES_INT_CONSIGNEE_SECOND_NAME_INVALID  });


		mapExpression.put("forwarderFrstName",new String[] { "[a-zA-Z0-9 ]{1,100}", ErrorCode.AES_FORWARDER_FIRST_NAME_INVALID  });
		mapExpression.put("forwarderSecondName",new String[] { "[a-zA-Z0-9 ]{1,100}", ErrorCode.AES_FORWARDER_SECND_NAME_INVALID  });


		mapExpression.put("aesfrstpieces",new String[] { "[0-9 ]{1,10}", ErrorCode.AES_PIECES_NAME_INVALID  });
		mapExpression.put("aesscndpieces",new String[] { "[0-9 ]{1,10}", ErrorCode.AES_PIECES_NAME_INVALID  });

		*/

		/*mapExpression.put("minimum",new String[] {"^-?[0-9]{1,10}+(.[0-9]{1,6})?$", ErrorCode.PRICING_MASTER_MINIMUM_INVALID });
		mapExpression.put("minus45",new String[] {"^-?[0-9]{1,10}+(.[0-9]{1,6})?$", ErrorCode.PRICING_MASTER_MINUS45_INVALID });
		mapExpression.put("plus45",new String[] {"^-?[0-9]{1,10}+(.[0-9]{1,6})?$", ErrorCode.PRICING_MASTER_PLUS45_INVALID });
		mapExpression.put("plus100",new String[] {"^-?[0-9]{1,10}+(.[0-9]{1,6})?$", ErrorCode.PRICING_MASTER_PLUS100_INVALID });
		mapExpression.put("plus250",new String[] {"^-?[0-9]{1,10}+(.[0-9]{1,6})?$", ErrorCode.PRICING_MASTER_PLUS250_INVALID });
		mapExpression.put("plus500",new String[] {"^-?[0-9]{1,10}+(.[0-9]{1,6})?$", ErrorCode.PRICING_MASTER_PLUS500_INVALID });
		mapExpression.put("plus1000",new String[] {"^-?[0-9]{1,10}+(.[0-9]{1,6})?$", ErrorCode.PRICING_MASTER_PLUS1000_INVALID });


		mapExpression.put("minSelInMinimum",new String[] {"^-?[0-9]{1,10}+(.[0-9]{1,6})?$", ErrorCode.PRICING_MASTER_MINSELIN_MINIMUM_INVALID });
		mapExpression.put("stdSelInMinimum",new String[] {"^-?[0-9]{1,10}+(.[0-9]{1,6})?$", ErrorCode.PRICING_MASTER_STDSELIN_MINIMUM_INVALID });
		mapExpression.put("costInMinimum",new String[] {"^-?[0-9]{1,10}+(.[0-9]{1,6})?$", ErrorCode.PRICING_MASTER_COST_MINIMUM_INVALID });

		mapExpression.put("minSelInAmount",new String[] {"^-?[0-9]{1,10}+(.[0-9]{1,6})?$", ErrorCode.PRICING_MASTER_MINSELAMOUNT_INVALID });
		mapExpression.put("stdSelInAmount",new String[] {"^-?[0-9]{1,10}+(.[0-9]{1,6})?$", ErrorCode.PRICING_MASTER_STDSEL_AMOUNT_INVALID });
		mapExpression.put("costSelInAmount",new String[] {"^-?[0-9]{1,10}+(.[0-9]{1,6})?$", ErrorCode.PRICING_MASTER_COST_AMOUNT_INVALID });

		mapExpression.put("fuelSurcharge",new String[] {"^-?[0-9]{1,10}+(.[0-9]{1,6})?$", ErrorCode.PRICING_MASTER_FUEL_SURCHARGE_INVALID });
		mapExpression.put("securitySurcharge",new String[] {"^-?[0-9]{1,10}+(.[0-9]{1,6})?$", ErrorCode.PRICING_MASTER_SECURITY_SURCHARGE_INVALID });
		*/
		/*mapExpression.put("iatarate-minAmount",new String[] {"^-?[0-9]{1,10}+(.[0-9]{1,6})?$", ErrorCode.MIN_AMOUNT_INVALID });
		mapExpression.put("iatarate-normal",new String[] {"^-?[0-9]{1,10}+(.[0-9]{1,6})?$", ErrorCode.NORMAL_INVALID });
		mapExpression.put("iatarate-plus45",new String[] {"^-?[0-9]{1,10}+(.[0-9]{1,6})?$", ErrorCode.PLUS45_INVALID });
		mapExpression.put("iatarate-plus100",new String[] {"^-?[0-9]{1,10}+(.[0-9]{1,6})?$", ErrorCode.PLUS100_INVALID });
		mapExpression.put("iatarate-plus250",new String[] {"^-?[0-9]{1,10}+(.[0-9]{1,6})?$", ErrorCode.PLUS300_INVALID });
		mapExpression.put("iatarate-plus500",new String[] {"^-?[0-9]{1,10}+(.[0-9]{1,6})?$", ErrorCode.PLUS500_INVALID });
		mapExpression.put("iatarate-plus1000",new String[] {"^-?[0-9]{1,10}+(.[0-9]{1,6})?$", ErrorCode.PLUS1000_INVALID });*/
		/*mapExpression.put("gross-weight",new String[] {"^-?[0-9]{1,11}+(.[0-9]{1,2})?$", ErrorCode.AUTH_DOCS_GROSSWEIGHT_INVALID });
		mapExpression.put("volume-weight",new String[] {"^-?[0-9]{1,11}+(.[0-9]{1,2})?$", ErrorCode.AUTH_DOCS_VOLUMEWEIGHT_INVALID });*/

    }

    /**
     * The map.
     */
    private ParameterFormat() {
    }

    /**
     * Gets the pattern and error message.
     *
     * @param key the key
     * @return the pattern and error message
     */
    public static String[] getPatternAndErrorMessage(String key) {
        return mapExpression.get(key);
    }
}

