package com.efreightsuite.util.edi;

import java.util.Date;

import com.efreightsuite.model.AirlineEdi;
import com.efreightsuite.model.AirlineEdiStatus;
import com.efreightsuite.repository.AirlineEdiRepository;
import com.efreightsuite.repository.AirlineEdiStatusRepository;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Log4j2
@Service
public class AirlineEdiStatusUpdater implements EdiStatus {

    AirlineEdi airlineEdi;

    Long airlineEdiId;

    AirlineEdiStatus airlineEdiStatus;

    @Autowired
    AirlineEdiRepository airlineEdiRepository;

    @Autowired
    AirlineEdiStatusRepository airlineEdiStatusRepository;


    public AirlineEdiStatusUpdater() {
        this.airlineEdiStatus = new AirlineEdiStatus();
    }

    /*	public AirlineEdiStatusUpdator(Long airlineEdiId) {
            log.info("Airline EDI ID : " + airlineEdiId);
            this.airlineEdi = airlineEdiRepository.findOne(airlineEdiId);
        }
    */
    public void setAirlineEdi(Long airlineEdiId) {
        this.airlineEdiId = airlineEdiId;
    }

    @Override
    public void update(EdiUpdate ediUpdate) {

        airlineEdi = airlineEdiRepository.findOne(airlineEdiId);

        airlineEdiStatus.setCreateDate(new Date());
        airlineEdiStatus.setAirlineEdi(airlineEdi);

        switch (ediUpdate) {
            case INITIATE:
                break;
            case COMPLETE:
                break;
            case PENDING:
                break;
            case PROGRESS:
                break;
        }

        airlineEdiStatusRepository.save(airlineEdiStatus);
        airlineEdiRepository.save(airlineEdi);

    }

}
