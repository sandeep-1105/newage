package com.efreightsuite.util;

import java.io.IOException;

import com.efreightsuite.enumeration.Measurement;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

import org.springframework.stereotype.Component;

@Component
public class JsonMeasurementDeserializer extends JsonDeserializer<Measurement> {

    @Override
    public Measurement deserialize(JsonParser jsonparser, DeserializationContext ctxt) throws IOException {

        String string = jsonparser.getText();

        if (string != null && string.trim().length() != 0) {
            if (string.trim().toUpperCase().equals("FALSE")) {
                return Measurement.Imperial;
            } else {
                return Measurement.Metric;
            }
        }
        return Measurement.Imperial;

    }

}
