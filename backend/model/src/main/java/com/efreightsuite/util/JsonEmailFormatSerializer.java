package com.efreightsuite.util;

import java.io.IOException;

import com.efreightsuite.enumeration.EmailFormat;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import org.springframework.stereotype.Component;

@Component
public class JsonEmailFormatSerializer extends JsonSerializer<EmailFormat> {

    @Override
    public void serialize(EmailFormat value, JsonGenerator gen, SerializerProvider provider) throws IOException {
        if (value != null && value == EmailFormat.HTML) {
            gen.writeObject(true);
        } else if (value != null && value == EmailFormat.TEXT) {
            gen.writeObject(false);
        } else {
            gen.writeObject(null);
        }


    }
}
