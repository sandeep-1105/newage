package com.efreightsuite.util;

import java.io.IOException;

import com.efreightsuite.enumeration.PPCC;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import org.springframework.stereotype.Component;

@Component
public class JsonPPccSerializer extends JsonSerializer<PPCC> {

    @Override
    public void serialize(PPCC value, JsonGenerator gen, SerializerProvider provider) throws IOException {
        if (value != null && value == PPCC.Collect) {
            gen.writeObject(true);
        } else {
            gen.writeObject(false);
        }

    }
}
