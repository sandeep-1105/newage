package com.efreightsuite.util;

import java.io.IOException;

import com.efreightsuite.enumeration.Gender;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import org.springframework.stereotype.Component;

@Component
public class JsonGenderSerializer extends JsonSerializer<Gender> {

    @Override
    public void serialize(Gender value, JsonGenerator gen, SerializerProvider provider) throws IOException {
        if (value != null && value == Gender.Male) {
            gen.writeObject(true);
        } else {
            gen.writeObject(false);
        }

    }
}
