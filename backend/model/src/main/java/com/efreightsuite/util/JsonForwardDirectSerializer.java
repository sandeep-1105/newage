package com.efreightsuite.util;

import java.io.IOException;

import com.efreightsuite.enumeration.ForwarderDirect;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

public class JsonForwardDirectSerializer extends JsonSerializer<ForwarderDirect> {

    @Override
    public void serialize(ForwarderDirect value, JsonGenerator gen,
                          SerializerProvider serializers) throws IOException,
            JsonProcessingException {
        if (value != null && value == ForwarderDirect.Forwarder) {
            gen.writeObject(false);
        } else {
            gen.writeObject(true);
        }

    }

}
