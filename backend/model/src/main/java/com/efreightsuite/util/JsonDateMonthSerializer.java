package com.efreightsuite.util;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import lombok.extern.log4j.Log4j2;

import org.springframework.stereotype.Component;

@Component
@Log4j2
public class JsonDateMonthSerializer extends JsonSerializer<Date> {


    @Override
    public void serialize(Date date, JsonGenerator gen, SerializerProvider provider) throws IOException {
        //String[] months = {"01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12"};
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String returnString = dateFormat.format(date);
        //int firstHyphen = formattedDate.indexOf("-");
        //String returnString = formattedDate.substring(0, firstHyphen);
        //String month = formattedDate.substring(firstHyphen+1, formattedDate.indexOf("-", firstHyphen+1));
        //Integer monthNumber;
        /*if(month.startsWith("0")) {
			monthNumber = new Integer(month.substring(1));
		} else {
			monthNumber = new Integer(month);
		}
		returnString = months[monthNumber-1] + "-" + returnString;*/
        gen.writeString(returnString);
    }
}
