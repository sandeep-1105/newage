package com.efreightsuite.util;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import org.springframework.stereotype.Component;

@Component
public class JsonStringToArrSerializer extends JsonSerializer<String> {

    @Override
    public void serialize(String value, JsonGenerator gen, SerializerProvider provider) throws IOException {
        if (value != null && value.trim().length() > 0) {
            gen.writeObject(value.trim().split(","));
        } else {
            gen.writeObject(new String[]{});
        }


    }
}
