package com.efreightsuite.util;

import java.io.IOException;

import com.efreightsuite.enumeration.ForwarderDirect;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

public class JsonForwardDirectDeSerializer extends JsonDeserializer<ForwarderDirect> {

    @Override
    public ForwarderDirect deserialize(JsonParser jsonparser, DeserializationContext ctxt)
            throws IOException, JsonProcessingException {

        String string = jsonparser.getText();

        if (string != null && string.trim().length() != 0) {
            if (string.trim().toUpperCase().equals("TRUE")) {
                return ForwarderDirect.Direct;
            } else {
                return ForwarderDirect.Forwarder;
            }
        }
        return ForwarderDirect.Forwarder;

    }
}

	
