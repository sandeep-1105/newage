package com.efreightsuite.repository;

import java.util.List;

import com.efreightsuite.model.QuotationCarrier;
import com.efreightsuite.model.QuotationDetail;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface QuotationDetailRepository extends JpaRepository<QuotationDetail, Long> {

    QuotationDetail findById(Long id);

    @Query("select e from QuotationDetail e WHERE e.quotation.id in :idList order by e.id desc")
    List<QuotationDetail> findAllByIds(@Param("idList") List<Long> idList);

    @Query("select e from QuotationCarrier e WHERE e.id =?1")
    QuotationCarrier getQuotationCarrier(Long id);
}


