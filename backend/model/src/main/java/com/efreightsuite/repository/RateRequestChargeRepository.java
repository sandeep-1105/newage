package com.efreightsuite.repository;

import java.util.Date;

import javax.transaction.Transactional;

import com.efreightsuite.model.RateRequestCharge;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

public interface RateRequestChargeRepository extends JpaRepository<RateRequestCharge, Long> {

    @Modifying
    @Transactional
    @Query(value = "UPDATE RateRequest ssd SET ssd.responseDate=?1 WHERE ssd.id=?2")
    int updateRateRequestReceivedDate(Date responseDate, Long id);

}
