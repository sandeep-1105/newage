package com.efreightsuite.repository;

import java.util.List;

import com.efreightsuite.enumeration.SessionStatus;
import com.efreightsuite.model.LoginHistory;

import org.springframework.data.jpa.repository.JpaRepository;

public interface LoginHistoryRepository extends JpaRepository<LoginHistory, Long> {

    List<LoginHistory> findBySessionIdAndSessionStatus(String sessionId, SessionStatus sessionStatus);

}
