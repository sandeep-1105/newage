package com.efreightsuite.repository;

import com.efreightsuite.enumeration.TemplateType;
import com.efreightsuite.model.EmailTemplate;

import org.springframework.data.jpa.repository.JpaRepository;

public interface EmailTemplateRepository extends JpaRepository<EmailTemplate, Long> {

    EmailTemplate findById(Long id);

    EmailTemplate findByTemplateType(TemplateType templateType);


}
