package com.efreightsuite.repository;

import com.efreightsuite.model.CFSMaster;

import org.springframework.data.jpa.repository.JpaRepository;

public interface CfsMasterRepository extends JpaRepository<CFSMaster, Long> {
}
