package com.efreightsuite.repository;

import java.util.List;

import com.efreightsuite.model.PartyEmailMapping;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

/**
 * @author Devendrachary M
 */

public interface PartyEmailMappingRepository extends JpaRepository<PartyEmailMapping, Long> {

    PartyEmailMapping findById(Long id);

    @Override
    List<PartyEmailMapping> findAll();

    @Query(value = "select p from PartyEmailMapping p where p.partyMaster.id=?1")
    List<PartyEmailMapping> getAll(Long id);


}
