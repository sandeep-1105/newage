package com.efreightsuite.repository;

import com.efreightsuite.model.CustomReportTemplate;

import org.springframework.data.jpa.repository.JpaRepository;

public interface CustomReportTemplateRepository extends JpaRepository<CustomReportTemplate, Long> {
}
