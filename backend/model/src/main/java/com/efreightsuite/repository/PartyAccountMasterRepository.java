package com.efreightsuite.repository;

import java.util.List;

import com.efreightsuite.model.PartyAccountMaster;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface PartyAccountMasterRepository extends JpaRepository<PartyAccountMaster, Long> {
    PartyAccountMaster findById(Long id);

    @Override
    List<PartyAccountMaster> findAll();

    @Query(value = "select p from PartyAccountMaster p where p.partyMaster.id=?1 order by p.accountMaster.glAccountName asc")
    List<PartyAccountMaster> getAll(Long id);
}
