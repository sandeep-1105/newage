package com.efreightsuite.repository.common;

import com.efreightsuite.model.common.CommonRegularExpression;

import org.springframework.data.jpa.repository.JpaRepository;


public interface CommonRegularExpressionRepository extends JpaRepository<CommonRegularExpression, Long> {
}
