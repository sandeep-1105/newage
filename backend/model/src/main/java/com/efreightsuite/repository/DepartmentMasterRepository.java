package com.efreightsuite.repository;

import com.efreightsuite.model.DepartmentMaster;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface DepartmentMasterRepository extends JpaRepository<DepartmentMaster, Long> {

    @Query(value = "select e from DepartmentMaster e where UPPER(e.departmentName) like ?1")
    DepartmentMaster findByName(String name);

}
