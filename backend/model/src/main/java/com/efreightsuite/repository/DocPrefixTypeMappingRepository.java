package com.efreightsuite.repository;

import java.util.List;

import com.efreightsuite.enumeration.DocPrefixDocumentType;
import com.efreightsuite.model.DocPrefixTypeMapping;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface DocPrefixTypeMappingRepository extends JpaRepository<DocPrefixTypeMapping, String> {


    DocPrefixTypeMapping findById(Long id);

    @Query(value = "select c from DocPrefixTypeMapping c where c.documentPrefixMaster.id=?1")
    List<DocPrefixTypeMapping> getByDocument(Long documentPrefixMasterId);

    @Query(value = "select c from DocPrefixTypeMapping c where c.docType=?1 AND c.locationCode=?2")
    DocPrefixTypeMapping getFormula(DocPrefixDocumentType docType, String locationCode);


}
