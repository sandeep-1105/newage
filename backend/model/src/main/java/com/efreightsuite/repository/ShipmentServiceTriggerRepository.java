package com.efreightsuite.repository;

import java.util.List;

import com.efreightsuite.model.ShipmentServiceTrigger;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface ShipmentServiceTriggerRepository extends JpaRepository<ShipmentServiceTrigger, Long> {

    @Query(value = "select t from ShipmentServiceTrigger t where t.shipmentUid=?1 and t.serviceUid != ?2")
    List<ShipmentServiceTrigger> findByShipmentUidAndServiceUid(String shipmentUid, String serviceUid);

}
