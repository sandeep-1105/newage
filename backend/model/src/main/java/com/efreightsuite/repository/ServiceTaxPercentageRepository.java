package com.efreightsuite.repository;

import java.util.List;

import com.efreightsuite.model.ServiceTaxPercentage;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface ServiceTaxPercentageRepository extends JpaRepository<ServiceTaxPercentage, Long> {

    ServiceTaxPercentage findById(Long id);

    @Query(value = "select anatt from ServiceTaxPercentage anatt where anatt.serviceTaxCategory.id=?1")
    List<ServiceTaxPercentage> findByServiceTaxCategoryMaster(Long id);

    @Query(value = "select anatt from ServiceTaxPercentage anatt where anatt.serviceTaxCategory.id=?1 AND anatt.countryMaster.id=?2 AND anatt.companyMaster.id=?3")
    List<ServiceTaxPercentage> findServiceTaxPercentage(Long id, Long countryId, Long companyId);

}
