package com.efreightsuite.repository;

import java.util.List;

import com.efreightsuite.enumeration.SequenceType;
import com.efreightsuite.model.SequenceGenerator;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface SequenceGeneratorRepository extends JpaRepository<SequenceGenerator, Long> {

    SequenceGenerator findById(Long id);

    SequenceGenerator findBySequenceType(SequenceType sequenceType);

    @Query("Select a from SequenceGenerator a where a.sequenceType=?1 and a.companyMaster.id=?2")
    SequenceGenerator getByTypeAndCompany(SequenceType sequenceType, Long companyId);

    @Query("Select a from SequenceGenerator a where a.sequenceType=?1")
    SequenceGenerator findBySequenceType(String sequenceType);

    @Override
    @Query("Select a from SequenceGenerator a ORDER BY a.sequenceType ASC")
    List<SequenceGenerator> findAll();


}
