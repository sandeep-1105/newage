package com.efreightsuite.repository;

import java.util.List;

import com.efreightsuite.model.AesFileStatusHistory;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface AesFileStatusHistoryRepository extends JpaRepository<AesFileStatusHistory, Long> {

    public AesFileStatusHistory findById(Long Id);

    @Query("SELECT afsh FROM AesFileStatusHistory afsh WHERE afsh.aesFile.id=?1")
    public List<AesFileStatusHistory> findByAesFile(Long id);
}
