package com.efreightsuite.repository;

import com.efreightsuite.model.AgentPortMaster;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import java.util.List;
public interface AgentPortMasterRepository extends JpaRepository<AgentPortMaster, Long> {

    @Query("Select p from AgentPortMaster p where p.serviceMaster.id=:serviceId AND p.port.id=:portId ")
    AgentPortMaster serachByServicePort(@Param("serviceId") Long serviceId, @Param("portId") Long portId);

    @Query("Select p from AgentPortMaster p where p.port.portCode=:portCode")
    List<AgentPortMaster> searchByPortCode(@Param("portCode") String portCode);


}
