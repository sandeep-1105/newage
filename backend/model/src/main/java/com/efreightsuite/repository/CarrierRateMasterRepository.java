package com.efreightsuite.repository;

import com.efreightsuite.model.CarrierRateMaster;

import org.springframework.data.jpa.repository.JpaRepository;

public interface CarrierRateMasterRepository extends JpaRepository<CarrierRateMaster, Long> {

}
