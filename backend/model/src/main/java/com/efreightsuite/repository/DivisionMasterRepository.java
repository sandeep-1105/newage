package com.efreightsuite.repository;

import java.util.List;

import com.efreightsuite.model.DivisionMaster;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface DivisionMasterRepository extends JpaRepository<DivisionMaster, Long> {

    @Override
    @Query("Select c from DivisionMaster c where c.status!='Hide' ORDER BY c.divisionName ASC")
    List<DivisionMaster> findAll();

    @Query("Select c from DivisionMaster c WHERE c.companyMaster.id=?1 ORDER BY c.divisionName ASC")
    List<DivisionMaster> findByCompanyMaster(Long companyId);


    DivisionMaster findBydivisionCode(String string);
}
