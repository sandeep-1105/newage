package com.efreightsuite.repository;

import com.efreightsuite.model.EnquiryCustomer;

import org.springframework.data.jpa.repository.JpaRepository;

public interface EnquiryCustomerRepository extends JpaRepository<EnquiryCustomer, Long> {

}
