package com.efreightsuite.repository;

import java.util.List;

import com.efreightsuite.model.EnquiryValueAddedService;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface EnquiryValueAddedServiceRepository extends JpaRepository<EnquiryValueAddedService, Long> {

    @Query(value = "SELECT ea FROM EnquiryValueAddedService ea WHERE ea.enquiryDetail.id = ?1")
    List<EnquiryValueAddedService> getValueAddedlist(Long id);


}
