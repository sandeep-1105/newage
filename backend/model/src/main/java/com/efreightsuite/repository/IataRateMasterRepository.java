package com.efreightsuite.repository;

import com.efreightsuite.model.IataRateMaster;

import org.springframework.data.jpa.repository.JpaRepository;

public interface IataRateMasterRepository extends JpaRepository<IataRateMaster, Long> {

}
