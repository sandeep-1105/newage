package com.efreightsuite.repository;

import java.util.List;

import com.efreightsuite.model.EnquiryDetail;
import com.efreightsuite.model.EnquiryLog;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * @author Devendrachary M
 */

public interface EnquiryDetailRepository extends JpaRepository<EnquiryDetail, Long> {

    EnquiryDetail findById(Long id);

    @Override
    List<EnquiryDetail> findAll();

    @Query(value = "select e from EnquiryDetail e where e.enquiryLog.id=?1")
    List<EnquiryDetail> getAll(Long id);

    @Query(value = "select e from EnquiryLog e where e.enquiryNo=?1")
    EnquiryLog findByEnquiryNo(String enquiryNo);

    @Query("select e from EnquiryDetail e WHERE e.id in :idList order by e.id desc")
    List<EnquiryDetail> findAllByIds(@Param("idList") List<Long> idList);

}
