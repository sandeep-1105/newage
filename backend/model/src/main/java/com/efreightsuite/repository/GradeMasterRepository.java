package com.efreightsuite.repository;

import com.efreightsuite.model.GradeMaster;

import org.springframework.data.jpa.repository.JpaRepository;

public interface GradeMasterRepository extends JpaRepository<GradeMaster, Long> {

}
