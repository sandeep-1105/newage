package com.efreightsuite.repository;

import com.efreightsuite.model.AddressMaster;
import com.efreightsuite.model.ConsolDocument;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface ConsolDocumentRepository extends JpaRepository<ConsolDocument, Long> {

    ConsolDocument findById(Long id);

    @Query("select cd.issuingAgentAddress from ConsolDocument cd where cd.id = :documentID")
    AddressMaster findByIdForIssuingAgentAddress(@Param("documentID") Long documentID);

/*
	@Query(value="select new com.efreightsuite.model.ConsolDocument(dd.id, dd.documentNo)  from ConsolDocument dd where dd.consol.id=?1")
	List<ConsolDocument> findAllConsolById(Long consolId);

	@Query(value="select dd  from ConsolDocument dd where dd.id=:consolId")
	ConsolDocument findDocumentById(@Param("consolId") Long consolId);*/
}
