package com.efreightsuite.repository.common;

import com.efreightsuite.model.common.CommonCurrencyMaster;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface CommonCurrencyMasterRepository extends JpaRepository<CommonCurrencyMaster, Long> {

    @Query("Select c from CommonCurrencyMaster c WHERE UPPER(c.currencyCode)=?1")
    CommonCurrencyMaster findByCurrencyCode(String currencyCode);

}
