package com.efreightsuite.repository;

import java.util.List;

import com.efreightsuite.model.EdiResponseStatus;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface EdiResponseStatusRepository extends JpaRepository<EdiResponseStatus, Long> {

    @Query("SELECT new EdiResponseStatus(ers.id, ers.masterNumber, ers.ediType, ers.ediStatusType, ers.statusName, ers.statusDate, ers.carrierCode, ers.flightNumber)  FROM EdiResponseStatus ers where ers.consol.id = :consolId ")
    public List<EdiResponseStatus> findAllByConsolId(@Param("consolId") Long consolId);

}
