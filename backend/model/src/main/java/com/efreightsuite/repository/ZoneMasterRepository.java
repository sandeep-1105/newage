package com.efreightsuite.repository;

import com.efreightsuite.model.ZoneMaster;

import org.springframework.data.jpa.repository.JpaRepository;

public interface ZoneMasterRepository extends JpaRepository<ZoneMaster, Long> {

}
