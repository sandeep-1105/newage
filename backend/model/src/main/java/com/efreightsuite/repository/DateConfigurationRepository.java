package com.efreightsuite.repository;

import com.efreightsuite.enumeration.WhichTransactionDate;
import com.efreightsuite.model.DateConfiguration;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface DateConfigurationRepository extends JpaRepository<DateConfiguration, Long> {

    @Query(value = "select c from DateConfiguration c where c.serviceMaster.id=?1 and c.whichTransactionDate=?2")
    DateConfiguration getByLogicDate(Long serviceId, WhichTransactionDate whichTransactionDate);

}
