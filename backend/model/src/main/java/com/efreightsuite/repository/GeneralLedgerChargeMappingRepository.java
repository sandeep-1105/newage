package com.efreightsuite.repository;

import com.efreightsuite.model.GeneralLedgerChargeMappingMaster;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface GeneralLedgerChargeMappingRepository extends JpaRepository<GeneralLedgerChargeMappingMaster, Long> {

    @Query(value = "select g from GeneralLedgerChargeMappingMaster g where g.chargeMaster.id=?1 AND g.serviceMaster.id=?2")
    public GeneralLedgerChargeMappingMaster findByChargeAndService(Long chargeId, Long serviceMasterId);

}
