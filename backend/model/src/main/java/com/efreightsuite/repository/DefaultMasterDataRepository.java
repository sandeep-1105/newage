package com.efreightsuite.repository;

import java.util.List;

import com.efreightsuite.model.DefaultMasterData;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface DefaultMasterDataRepository extends JpaRepository<DefaultMasterData, Long> {


    @Query(value = "select c from DefaultMasterData c where c.code=?1 and c.location.id=?2")
    DefaultMasterData getCodeAndLocation(String code, Long locationId);


    DefaultMasterData findById(Long id);

    @Query(value = "select c from DefaultMasterData c where c.code LIKE :keyWord")
    List<DefaultMasterData> findAllByCodePrefix(@Param("keyWord") String keyWord);

    @Query(value = "select c from DefaultMasterData c where c.location.id=?1")
    List<DefaultMasterData> getByLocation(Long locationId);

    @Query(value = "select c from DefaultMasterData c where c.code=?1")
    DefaultMasterData findByCode(String code);

    @Query(value = "select c from DefaultMasterData c where c.code=?1 and c.location.id=?2")
    DefaultMasterData findByCodeAndLocationId(String string, Long locationId);

}
