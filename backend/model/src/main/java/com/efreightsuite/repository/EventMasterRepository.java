package com.efreightsuite.repository;

import com.efreightsuite.enumeration.EventMasterType;
import com.efreightsuite.model.EventMaster;

import org.springframework.data.jpa.repository.JpaRepository;

public interface EventMasterRepository extends JpaRepository<EventMaster, Long> {

    EventMaster findByEventMasterType(EventMasterType eventMasterType);

    EventMaster findByEventCode(String eventCode);
}
