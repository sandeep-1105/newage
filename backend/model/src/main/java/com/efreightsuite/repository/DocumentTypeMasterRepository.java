package com.efreightsuite.repository;

import java.util.List;

import com.efreightsuite.enumeration.DocumentType;
import com.efreightsuite.model.DocumentTypeMaster;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface DocumentTypeMasterRepository extends JpaRepository<DocumentTypeMaster, Long> {

    DocumentTypeMaster findById(Long id);

    DocumentTypeMaster findByDocumentTypeCode(DocumentType documentTypeCode);

    @Query("SELECT p FROM DocumentTypeMaster p ORDER BY p.documentTypeCode, p.documentTypeName ASC")
    @Override
    List<DocumentTypeMaster> findAll();

    @Query("Select p from DocumentTypeMaster p WHERE (UPPER(p.documentTypeCode) like :keyword% or UPPER(p.documentTypeName) like :keyword%) ORDER BY p.documentTypeCode, p.documentTypeName ASC")
    Page<DocumentTypeMaster> findByKeyword(@Param("keyword") String keyword, Pageable page);

}
