package com.efreightsuite.repository;

import java.util.List;

import com.efreightsuite.model.RecordAccessLevelUser;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface RecordAccessLevelUserRepository extends JpaRepository<RecordAccessLevelUser, Long> {

    @Query("select user from RecordAccessLevelUser user WHERE user.userProfile.id = :id")
    List<RecordAccessLevelUser> findAllByUserProfile(@Param("id") Long id);
}
