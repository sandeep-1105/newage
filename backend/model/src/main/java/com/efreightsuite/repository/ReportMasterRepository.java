package com.efreightsuite.repository;

import com.efreightsuite.enumeration.ReportName;
import com.efreightsuite.enumeration.YesNo;
import com.efreightsuite.model.ReportMaster;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface ReportMasterRepository extends JpaRepository<ReportMaster, Long> {

    @Query(value = "select rm from ReportMaster rm where rm.reportKey=?1 AND rm.isReportEnable=?2")
    ReportMaster findByReportKeyAndIsReportEnable(ReportName reportKey, YesNo isReportEnable);

    @Query(value = "select rm from ReportMaster rm where rm.reportKey=?1")
    ReportMaster findByReportKey(ReportName reportKey);


}
