package com.efreightsuite.repository;

import java.util.List;

import com.efreightsuite.model.ReasonMaster;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface ReasonMasterRepository extends JpaRepository<ReasonMaster, Long> {

    ReasonMaster findById(Long id);

    ReasonMaster findByReasonCode(String reasonCode);

    @Query("SELECT p FROM ReasonMaster p ORDER BY p.reasonCode, p.reasonName ASC")
    @Override
    List<ReasonMaster> findAll();

    @Query("Select p from ReasonMaster p WHERE (UPPER(p.reasonCode) like :keyword% or UPPER(p.reasonName) like :keyword%) ORDER BY p.reasonCode, p.reasonName ASC")
    Page<ReasonMaster> findByKeyword(@Param("keyword") String keyword, Pageable page);

}
