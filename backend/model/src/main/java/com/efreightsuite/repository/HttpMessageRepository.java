package com.efreightsuite.repository;

import java.util.List;

import com.efreightsuite.model.HttpMessage;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface HttpMessageRepository extends JpaRepository<HttpMessage, Long> {

    HttpMessage findById(Long id);

    @Override
    @Query("Select h from HttpMessage h ORDER BY h.ipAddress ASC")
    List<HttpMessage> findAll();

}
