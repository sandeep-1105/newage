package com.efreightsuite.repository;

import java.util.List;

import com.efreightsuite.model.PartyAddressMaster;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface PartyAddressMasterRepository extends JpaRepository<PartyAddressMaster, Long> {

    @Query(value = "select p from PartyAddressMaster p where p.partyMaster.id=?1")
    List<PartyAddressMaster> getAll(Long id);

}
