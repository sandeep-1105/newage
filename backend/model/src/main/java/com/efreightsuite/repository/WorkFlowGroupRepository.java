package com.efreightsuite.repository;

import com.efreightsuite.model.WorkFlowGroup;

import org.springframework.data.jpa.repository.JpaRepository;

public interface WorkFlowGroupRepository extends JpaRepository<WorkFlowGroup, Long> {

    public WorkFlowGroup findByWorkFlowName(String workFlowName);

    public WorkFlowGroup findById(Long workFlowGroupId);

}
