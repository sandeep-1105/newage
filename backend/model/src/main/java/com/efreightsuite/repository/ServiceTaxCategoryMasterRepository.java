package com.efreightsuite.repository;

import com.efreightsuite.model.ServiceTaxCategoryMaster;

import org.springframework.data.jpa.repository.JpaRepository;

public interface ServiceTaxCategoryMasterRepository extends JpaRepository<ServiceTaxCategoryMaster, Long> {

    ServiceTaxCategoryMaster findById(Long id);


}
