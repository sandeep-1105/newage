package com.efreightsuite.repository;

import java.util.List;

import com.efreightsuite.model.UserProfile;
import com.efreightsuite.model.WorkFlowGroup;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface UserProfileRepository extends JpaRepository<UserProfile, Long> {

    UserProfile findByUserName(String userName);

    @Query("select u.userProfile from UserCompany u where u.employee.email=?1")
    List<UserProfile> findByEmployee(String emailId);

    @Query("select u.workFlowList from UserProfile u where u.id=:userId")
    List<WorkFlowGroup> findWorkFlowGroupListByUser(@Param("userId") Long userId);
}
