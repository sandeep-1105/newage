package com.efreightsuite.repository;

import com.efreightsuite.model.ServiceChargeTaxCategoryMapping;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface ServiceChargeTaxCategoryMappingRepository extends JpaRepository<ServiceChargeTaxCategoryMapping, Long> {

    ServiceChargeTaxCategoryMapping findById(Long id);

    @Query(value = "select sctcm from ServiceChargeTaxCategoryMapping sctcm where sctcm.serviceMaster.id=?1 And sctcm.chargeMaster.id=?2")
    ServiceChargeTaxCategoryMapping findByServiceMasterAndChargeMaster(Long serviceId, Long chargeId);


}
