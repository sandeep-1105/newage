package com.efreightsuite.repository;

import java.util.List;

import com.efreightsuite.model.AutoMailGroupMaster;
import com.efreightsuite.model.AutoMailMaster;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

/**
 * @author Devendrachary M
 */

public interface AutoMailMasterRepository extends JpaRepository<AutoMailMaster, Long> {

    AutoMailMaster findById(Long id);

    @Override
    List<AutoMailMaster> findAll();

    @Query(value = "select a.autoMailGroupMaster from  AutoMailMaster a where a.id=?1")
    AutoMailGroupMaster getAutoGroupId(Long id);


}
