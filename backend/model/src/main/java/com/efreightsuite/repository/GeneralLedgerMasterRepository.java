package com.efreightsuite.repository;

import com.efreightsuite.model.GeneralLedgerMaster;

import org.springframework.data.jpa.repository.JpaRepository;

public interface GeneralLedgerMasterRepository extends JpaRepository<GeneralLedgerMaster, Long> {

    public GeneralLedgerMaster findById(Long id);

    public GeneralLedgerMaster findByGlCode(String glCode);

}
