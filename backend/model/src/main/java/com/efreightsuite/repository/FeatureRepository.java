package com.efreightsuite.repository;

import java.util.List;

import com.efreightsuite.enumeration.YesNo;
import com.efreightsuite.model.Feature;

import org.springframework.data.jpa.repository.JpaRepository;

public interface FeatureRepository extends JpaRepository<Feature, Long> {

    public List<Feature> findAllByParentAndIsApplicable(Feature parent, YesNo isApplicable);

    public List<Feature> findByIsApplicableOrderByIdAsc(YesNo isApplicable);

    public Feature findByFeatureValue(String featureValue);

    public Feature findById(Long id);
}
