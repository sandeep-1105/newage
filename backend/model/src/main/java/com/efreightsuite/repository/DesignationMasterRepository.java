package com.efreightsuite.repository;

import com.efreightsuite.model.DesignationMaster;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface DesignationMasterRepository extends JpaRepository<DesignationMaster, Long> {

    @Query("select d from DesignationMaster d where UPPER(d.designationName) like ?1")
    DesignationMaster findByDesignationName(String designationName);
}
