package com.efreightsuite.repository;

import com.efreightsuite.model.ConsolSignOff;

import org.springframework.data.jpa.repository.JpaRepository;

public interface ConsolSignOffRepository extends JpaRepository<ConsolSignOff, Long> {
}
