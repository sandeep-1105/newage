package com.efreightsuite.repository;

import com.efreightsuite.model.UserCompanyLocation;

import org.springframework.data.jpa.repository.JpaRepository;

public interface UserLocationMappingRepository extends JpaRepository<UserCompanyLocation, Long> {


}
