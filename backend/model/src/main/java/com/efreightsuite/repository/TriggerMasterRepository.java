package com.efreightsuite.repository;

import java.util.List;

import com.efreightsuite.model.TriggerMaster;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

;

public interface TriggerMasterRepository extends JpaRepository<TriggerMaster, Long> {

    @Query("SELECT t from TriggerMaster t ORDER BY t.triggerName ASC")
    List<TriggerMaster> findAll();

    TriggerMaster findByTriggerCode(String triggerCode);

}
