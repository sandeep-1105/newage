package com.efreightsuite.repository;

import java.util.List;

import com.efreightsuite.model.InvoiceCreditNoteAttachment;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface InvoiceAttachmentRepository extends JpaRepository<InvoiceCreditNoteAttachment, Long> {


    @Query(value = "select anatt from InvoiceCreditNoteAttachment anatt where anatt.invoiceCreditNote.id=?1")
    List<InvoiceCreditNoteAttachment> findByinvoiceCreditNote(Long id);


}


