package com.efreightsuite.repository;

import com.efreightsuite.model.AirlineEdiStatus;

import org.springframework.data.jpa.repository.JpaRepository;

public interface AirlineEdiStatusRepository extends JpaRepository<AirlineEdiStatus, Long> {

}
