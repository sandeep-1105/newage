package com.efreightsuite.repository;

import com.efreightsuite.model.AesUsStateCodeMaster;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface AesUsStateCodeRepository extends JpaRepository<AesUsStateCodeMaster, Long> {

    public AesUsStateCodeMaster findById(Long Id);

    public AesUsStateCodeMaster findByStateCode(String code);

    @Query("Select p from AesUsStateCodeMaster p WHERE (UPPER(p.stateCode) like UPPER(:keyword) or UPPER(p.name) like UPPER(:keyword))")
    Page<AesUsStateCodeMaster> findByKeyword(@Param("keyword") String keyword, Pageable page);

}
