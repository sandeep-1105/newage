package com.efreightsuite.repository;

import com.efreightsuite.model.EmployeeMaster;

import org.springframework.data.jpa.repository.JpaRepository;

public interface EmployeeMasterRepository extends JpaRepository<EmployeeMaster, Long> {

    EmployeeMaster findById(Long id);

    EmployeeMaster findByEmployeeCode(String employeeCode);

    EmployeeMaster findByEmail(String email);
}
