package com.efreightsuite.repository;

import java.util.List;

import com.efreightsuite.model.CountryMaster;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface CountryMasterRepository extends JpaRepository<CountryMaster, Long> {

    CountryMaster findById(Long id);

    CountryMaster findByCountryCode(String countryCode);

    @Query("SELECT c from CountryMaster c where c.status!='Hide' ORDER BY c.countryName ASC")
    List<CountryMaster> findAll();

    @Query("SELECT new com.efreightsuite.model.CountryMaster(c.id) from CountryMaster c")
    List<CountryMaster> findAllCountryObjectWithIdAlone();

    @Query("FROM CountryMaster cm WHERE UPPER(cm.countryName) = :countryName")
    CountryMaster findByCountryName(@Param("countryName") String countryName);
}
