package com.efreightsuite.repository;

import java.util.List;

import com.efreightsuite.model.PartyTypeMaster;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface PartyTypeMasterRepository extends JpaRepository<PartyTypeMaster, Long> {

    @Query(value = "select e from PartyTypeMaster e where e.status='Active' order by e.partyTypeName asc")
    List<PartyTypeMaster> getAll();


    @Query(value = "SELECT ptm FROM PartyTypeMaster ptm WHERE UPPER(ptm.partyTypeName) = ?1")
    PartyTypeMaster findByPartyTypeName(String partyTypeName);

    @Query(value = "SELECT ptm FROM PartyTypeMaster ptm WHERE UPPER(ptm.partyTypeCode) = ?1")
    PartyTypeMaster findByPartyTypeCode(String partyTypeCode);
}
