package com.efreightsuite.repository;

import java.util.List;

import com.efreightsuite.model.EdiConfigurationMaster;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface EdiConfigurationMasterRepository extends JpaRepository<EdiConfigurationMaster, Long> {

    @Query(value = "select ecm from EdiConfigurationMaster ecm where ecm.ediConfigurationKey LIKE :keyword")
    public List<EdiConfigurationMaster> findAllByKeyLike(@Param("keyword") String key);

    @Query(value = "select ecm from EdiConfigurationMaster ecm where ecm.ediConfigurationKey=?1")
    public List<EdiConfigurationMaster> findByKey(String key);


    @Query(value = "select ecm from EdiConfigurationMaster ecm where UPPER(ecm.ediConfigurationKey)=?1 AND ecm.locationMaster.id=?2")
    public EdiConfigurationMaster findByKey(String key, Long locationId);

    @Query(value = "select ecm from EdiConfigurationMaster ecm where ecm.locationMaster.id=?1")
    public EdiConfigurationMaster findByLocationId(Long id);


}
