package com.efreightsuite.repository;

import java.util.List;

import com.efreightsuite.model.ServiceTypeMaster;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface ServiceTypeMasterRepository extends JpaRepository<ServiceTypeMaster, Long> {

    @Query(value = "select e from ServiceTypeMaster e where e.status!='Hide' order by e.serviceTypeName asc")
    List<ServiceTypeMaster> getAll();

    ServiceTypeMaster findByServiceTypeCode(String serviceTypeCode);
}
