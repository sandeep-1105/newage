package com.efreightsuite.repository;

import com.efreightsuite.model.CommentMaster;

import org.springframework.data.jpa.repository.JpaRepository;


public interface CommentMasterRepository extends JpaRepository<CommentMaster, Long> {

    CommentMaster findByCommentCode(String commentCode);
}
