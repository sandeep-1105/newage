package com.efreightsuite.repository;

import com.efreightsuite.model.AesFileEdiStatus;

import org.springframework.data.jpa.repository.JpaRepository;

public interface AesFileEdiStatusRepository extends JpaRepository<AesFileEdiStatus, Long> {

}
