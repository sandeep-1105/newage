package com.efreightsuite.repository;

import java.util.Date;
import java.util.List;

import com.efreightsuite.model.FlightPlan;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface FlightPlanRepository extends JpaRepository<FlightPlan, Long> {

    FlightPlan findById(Long id);

    FlightPlan findByScheduleId(String scheduleId);


    //where s.stockStatus!='Available' AND s.carrier.id = ?1 AND s.por.id = ?2 and s.mawbNo not in (?3)  order by s.id desc")
    @Query(value = "select fp from FlightPlan fp where fp.carrierMaster.id = ?1 and fp.pol.id = ?2 and fp.etd >=?3 order by fp.id desc")
    public List<FlightPlan> findFlightPlanSchedule(Long carrierId, Long porId, Date now);


    @Query(value = "select fp from FlightPlan fp where fp.id not in (?4) and fp.carrierMaster.id = ?1 and fp.pol.id = ?2 and fp.etd >=?3 order by fp.id desc")
    public List<FlightPlan> findFlightPlanSchedule(Long carrierId, Long porId, Date now, Long[] ids);
}
