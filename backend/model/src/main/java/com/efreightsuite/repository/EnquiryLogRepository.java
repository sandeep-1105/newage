package com.efreightsuite.repository;

import java.util.List;

import com.efreightsuite.model.EnquiryLog;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author Devendrachary M
 */

public interface EnquiryLogRepository extends JpaRepository<EnquiryLog, Long> {

    EnquiryLog findById(Long id);

    @Override
    List<EnquiryLog> findAll();

    EnquiryLog findByEnquiryNo(String enquiryNo);

}
