package com.efreightsuite.repository;

import java.util.List;

import com.efreightsuite.model.RecentHistory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

public interface RecentHistoryRepository extends JpaRepository<RecentHistory, Long> {

    RecentHistory findById(Long id);

    Page<RecentHistory> findAllByLocationIdAndUserId(Long locationId, Long userId, Pageable page);

    Page<RecentHistory> findAllByLocationIdAndUserIdAndStateCategory(Long locationId, Long userId, String stateCategory, Pageable page);

    List<RecentHistory> findAllByLocationIdAndUserId(Long locationId, Long userId);

    @Modifying
    @Transactional
    @Query("DELETE from RecentHistory rh where rh.locationId = :locationId and rh.userId = :userId")
    int deleteAllByLocationIdAndUserId(@Param("locationId") Long locationId, @Param("userId") Long userId);

}
