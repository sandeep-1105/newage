package com.efreightsuite.repository;

import com.efreightsuite.model.PricingPortPair;

import org.springframework.data.jpa.repository.JpaRepository;

public interface PricingPortPairRepository extends JpaRepository<PricingPortPair, Long> {

}
