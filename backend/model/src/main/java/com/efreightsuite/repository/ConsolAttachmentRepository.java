package com.efreightsuite.repository;

import com.efreightsuite.model.ConsolAttachment;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface ConsolAttachmentRepository extends JpaRepository<ConsolAttachment, Long> {

    @Query(value = "SELECT ea.file FROM ConsolAttachment ea WHERE ea.id = ?1")
    byte[] getByteArray(Long id);

}


