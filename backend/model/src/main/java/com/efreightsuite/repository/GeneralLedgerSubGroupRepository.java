package com.efreightsuite.repository;

import com.efreightsuite.model.GeneralLedgerSubGroup;

import org.springframework.data.jpa.repository.JpaRepository;

public interface GeneralLedgerSubGroupRepository extends JpaRepository<GeneralLedgerSubGroup, Long> {

    public GeneralLedgerSubGroup findById(Long Id);

    public GeneralLedgerSubGroup findByGlSubGroupCode(String glSubGroupCode);

}
