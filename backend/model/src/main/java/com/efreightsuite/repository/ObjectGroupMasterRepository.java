package com.efreightsuite.repository;

import com.efreightsuite.model.ObjectGroupMaster;

import org.springframework.data.jpa.repository.JpaRepository;

public interface ObjectGroupMasterRepository extends JpaRepository<ObjectGroupMaster, Long> {

}
