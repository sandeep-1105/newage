package com.efreightsuite.repository;

import com.efreightsuite.model.IataMaster;

import org.springframework.data.jpa.repository.JpaRepository;

public interface IataMasterRepository extends JpaRepository<IataMaster, Long> {

}
