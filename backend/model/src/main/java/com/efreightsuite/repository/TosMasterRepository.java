package com.efreightsuite.repository;

import com.efreightsuite.model.TosMaster;

import org.springframework.data.jpa.repository.JpaRepository;

public interface TosMasterRepository extends JpaRepository<TosMaster, Long> {

    TosMaster findByTosCode(String string);
}
