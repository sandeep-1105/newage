package com.efreightsuite.repository;

import java.util.List;

import com.efreightsuite.model.CategoryMaster;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface CategoryMasterRepository extends JpaRepository<CategoryMaster, Long> {

    @Query(value = "select c from CategoryMaster c order by c.categoryName asc")
    List<CategoryMaster> getAll();
}
