package com.efreightsuite.repository.common;

import com.efreightsuite.enumeration.DocumentType;
import com.efreightsuite.model.common.CommonDocumentTypeMaster;

import org.springframework.data.jpa.repository.JpaRepository;

public interface CommonDocumentTypeMasterRepository extends JpaRepository<CommonDocumentTypeMaster, Long> {
    CommonDocumentTypeMaster findByDocumentTypeCode(DocumentType documentTypeCode);
}
