package com.efreightsuite.repository;

import java.util.List;

import com.efreightsuite.model.CompanyMaster;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface CompanyMasterRepository extends JpaRepository<CompanyMaster, Long> {

    CompanyMaster findById(Long id);

    CompanyMaster findByCompanyCode(String companyCode);

    @Override
    @Query("Select c from CompanyMaster c ORDER BY c.companyName ASC")
    List<CompanyMaster> findAll();

    @Query("Select c from CompanyMaster c where (c.companyName like :keyword% or c.companyCode like :keyword%) ORDER BY c.companyName ASC")
    Page<CompanyMaster> findByKeyword(@Param("keyword") String keyword, Pageable page);

}
