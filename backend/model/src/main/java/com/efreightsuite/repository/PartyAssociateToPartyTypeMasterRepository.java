package com.efreightsuite.repository;

import java.util.List;

import com.efreightsuite.model.PartyAssociateToPartyTypeMaster;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface PartyAssociateToPartyTypeMasterRepository extends JpaRepository<PartyAssociateToPartyTypeMaster, Long> {

    @Query(value = "select c from PartyAssociateToPartyTypeMaster c where c.partyMaster.id=?1 order by c.partyTypeMaster.partyTypeName asc")
    List<PartyAssociateToPartyTypeMaster> getAll(Long id);

}
