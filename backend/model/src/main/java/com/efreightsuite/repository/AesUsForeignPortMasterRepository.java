package com.efreightsuite.repository;

import com.efreightsuite.model.AesUsForeignPortMaster;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface AesUsForeignPortMasterRepository extends JpaRepository<AesUsForeignPortMaster, Long> {

    public AesUsForeignPortMaster findById(Long Id);

    public AesUsForeignPortMaster findByPortCode(String code);


    @Query("Select p from AesUsForeignPortMaster p WHERE (UPPER(p.portName) like UPPER(:keyword) or UPPER(p.portCode) like UPPER(:keyword))")
    Page<AesUsForeignPortMaster> findByKeyword(@Param("keyword") String keyword, Pageable page);

}
