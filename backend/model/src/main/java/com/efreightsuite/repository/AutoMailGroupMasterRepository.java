package com.efreightsuite.repository;

import java.util.List;

import com.efreightsuite.model.AutoMailGroupMaster;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author Devendrachary M
 */

public interface AutoMailGroupMasterRepository extends JpaRepository<AutoMailGroupMaster, Long> {

    AutoMailGroupMaster findById(Long id);

    @Override
    List<AutoMailGroupMaster> findAll();


}
