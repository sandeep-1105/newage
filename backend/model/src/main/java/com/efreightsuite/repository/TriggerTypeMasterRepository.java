package com.efreightsuite.repository;

import java.util.List;

import com.efreightsuite.model.TriggerTypeMaster;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

;

public interface TriggerTypeMasterRepository extends JpaRepository<TriggerTypeMaster, Long> {

    @Query("SELECT t from TriggerTypeMaster t ORDER BY t.triggerTypeName ASC")
    List<TriggerTypeMaster> findAll();

    TriggerTypeMaster findByTriggerTypeCode(String triggerTypeCode);
}
