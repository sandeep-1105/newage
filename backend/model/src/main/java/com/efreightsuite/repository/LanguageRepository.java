package com.efreightsuite.repository;

import java.util.List;

import com.efreightsuite.enumeration.YesNo;
import com.efreightsuite.model.Language;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface LanguageRepository extends JpaRepository<Language, Long> {

    Language findById(Long id);

    Language findByLanguageName(String languageName);

    Language findByLanguageNameAndStatus(String languageName, YesNo status);

    @Override
    @Query("Select e from Language e ORDER BY e.languageName ASC")
    List<Language> findAll();

}
