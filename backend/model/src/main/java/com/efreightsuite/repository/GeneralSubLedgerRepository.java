package com.efreightsuite.repository;

import com.efreightsuite.model.GeneralSubLedger;

import org.springframework.data.jpa.repository.JpaRepository;

public interface GeneralSubLedgerRepository extends JpaRepository<GeneralSubLedger, Long> {

}
