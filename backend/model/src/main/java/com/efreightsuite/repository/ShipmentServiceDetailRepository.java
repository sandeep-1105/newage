package com.efreightsuite.repository;

import java.util.Date;
import java.util.List;

import com.efreightsuite.model.PartyAddressMaster;
import com.efreightsuite.model.PurchaseOrder;
import com.efreightsuite.model.ShipmentServiceDetail;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface ShipmentServiceDetailRepository extends JpaRepository<ShipmentServiceDetail, Long> {

    @Query(value = "select s from ShipmentServiceDetail s where s.shipment.id=?1")
    List<ShipmentServiceDetail> findByShipment(Long shipmentId);

    @Query("select e from ShipmentServiceDetail e WHERE e.id in :idList order by e.id desc")
    List<ShipmentServiceDetail> findAllByIds(@Param("idList") List<Long> idList);

    ShipmentServiceDetail findById(Long id);

    @Query("select dd.shipmentServiceDetail from DocumentDetail dd where dd.id = :documentID")
    ShipmentServiceDetail findByIdForShipmentServiceDetail(@Param("documentID") Long documentID);

    @Query(value = "select p from PartyAddressMaster p where p.id=:documentID")
    PartyAddressMaster findByFaxNumber(@Param("documentID") Long documentID);

    ShipmentServiceDetail findByServiceUid(String serviceUid);


    @Query("select po from PurchaseOrder po where id = :poId")
    PurchaseOrder findByPOId(@Param("poId") Long poId);

    @Query("select po from PurchaseOrder po where poNo = :poNo")
    PurchaseOrder findByPONo(@Param("poNo") Long poNo);


    @Query(value = "select ssd from ShipmentServiceDetail ssd where ssd.consolUid=?1")
    List<ShipmentServiceDetail> getByServiceByConsol(String colsolUid);

    @Query(value = "select ssd from ShipmentServiceDetail ssd where ssd.mawbNo=?1")
    List<ShipmentServiceDetail> getByServiceByMawbNo(String mawbNo);

    @Query("select sd from ShipmentServiceDetail sd where sd.serviceUid =:serviceUid")
    ShipmentServiceDetail findService(@Param("serviceUid") String serviceUid);

    //Reports	
    @Query(value = "select ssd from ShipmentServiceDetail ssd where ssd.serviceCode=?1 and ssd.serviceReqDate >= ?2 AND ssd.serviceReqDate <= ?3")
    // for PS Report
    List<ShipmentServiceDetail> findByServiceDetail(String serviceCode, Date fromDate, Date toDate);

    @Query(value = "select ssd from ShipmentServiceDetail ssd where ssd.serviceMaster.serviceName=?1 and ssd.serviceReqDate >= ?2 AND ssd.serviceReqDate <= ?3")
        // for PS Report
    List<ShipmentServiceDetail> findByServiceByDetail(String serviceName, Date fromDate, Date toDate);

    @Query(value = "SELECT * FROM SERVICE ssd INNER JOIN PORT_MASTER pm on pm.id=ssd.POL_ID WHERE ssd.service_code=?1 "
            + " and ssd.COUNTRY_CODE=?2 and pm.PORT_CODE=?3 "
            + " AND TRUNC(ssd.SERVICE_REQ_DATE) >= TO_DATE(?4,'YYYY-MM-DD HH24:MI:SS')"
            + " AND TRUNC(ssd.SERVICE_REQ_DATE) <= TO_DATE(?5,'YYYY-MM-DD HH24:MI:SS')", nativeQuery = true)
        //@Query(value = "select ssd from ShipmentServiceDetail ssd where ssd.serviceMaster.serviceCode=?1 and ssd.country.countryCode=?2 and ssd.transitPort.portCode=?3 and ssd.serviceReqDate >= ?4 AND ssd.serviceReqDate <= ?5")   // For volume report
    List<ShipmentServiceDetail> findByAllShipment(String serviceCode, String countryCode, String portCode, String fromDate, String toDate);

    @Query(value = "select ssd from  ShipmentServiceDetail ssd where ssd.serviceCode=?1 and ssd.origin.portCode=?2  and ssd.destination.portCode=?3 and ssd.carrier.carrierName=?4 and   ssd.serviceReqDate >= ?5 AND  ssd.serviceReqDate <= ?6 ")
        // for CarrierVolume
    List<ShipmentServiceDetail> findByCarrier(String serviceCode, String origin, String destination, String carrier, Date fromDate, Date toDate);

    @Query(value = "SELECT * FROM SERVICE ssd INNER JOIN PORT_MASTER pm on pm.id=ssd.POL_ID WHERE ssd.service_code=?1 "
            + " and ssd.service_origin_id=?2 "
            + " and ssd.service_destination_id=?3"
            + " and ssd.carrier_id=?4"
            + " AND TRUNC(ssd.SERVICE_REQ_DATE) >= TO_DATE(?5,'YYYY-MM-DD HH24:MI:SS')"
            + " AND TRUNC(ssd.SERVICE_REQ_DATE) <= TO_DATE(?6,'YYYY-MM-DD HH24:MI:SS')", nativeQuery = true)
        //@Query(value = "select ssd from  ShipmentServiceDetail ssd where ssd.serviceCode=?1 and ssd.origin.portCode=?2  and ssd.destination.portCode=?3 and ssd.carrier.carrierName=?4 and   ssd.serviceReqDate >= ?5 AND  ssd.serviceReqDate <= ?6 ")  // for CarrierVolume
    List<ShipmentServiceDetail> findByCarrier(String serviceCode, Long origin, Long destination, Long carrier, String fromDate, String toDate);


    @Query(value = "select s from ShipmentServiceDetail s where s.quotationUid=?1")
    ShipmentServiceDetail findByQuotationNo(String quotationNo);


    @Query(value = "select s from ShipmentServiceDetail s where s.mawbNo=?1 and s.serviceMaster.id=?2 and s.consolUid is NULL ")
    List<ShipmentServiceDetail> findByMawbNo(String mawbNo, Long serviceId);
}
