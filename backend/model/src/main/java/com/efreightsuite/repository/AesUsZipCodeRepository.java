package com.efreightsuite.repository;

import com.efreightsuite.model.AesUsZipCodeMaster;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface AesUsZipCodeRepository extends JpaRepository<AesUsZipCodeMaster, Long> {

    public AesUsZipCodeMaster findById(Long Id);

    public AesUsZipCodeMaster findByCode(String code);

    @Query("Select p from AesUsZipCodeMaster p WHERE (UPPER(p.stateCode) like UPPER(:keyword) or UPPER(p.code) like UPPER(:keyword))")
    Page<AesUsZipCodeMaster> findByKeyword(@Param("keyword") String keyword, Pageable page);
}
