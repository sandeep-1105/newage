package com.efreightsuite.repository;

import com.efreightsuite.model.AesTransportMode;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface AesTransportModeRepository extends JpaRepository<AesTransportMode, Long> {

    @Query(value = "select t from AesTransportMode t where t.aesTransportName=?1")
    AesTransportMode findByTransportName(String string);

}
