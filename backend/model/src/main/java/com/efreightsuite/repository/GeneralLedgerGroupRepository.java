package com.efreightsuite.repository;

import com.efreightsuite.model.GeneralLedgerGroup;

import org.springframework.data.jpa.repository.JpaRepository;

public interface GeneralLedgerGroupRepository extends JpaRepository<GeneralLedgerGroup, Long> {

    public GeneralLedgerGroup findById(Long Id);

    public GeneralLedgerGroup findByGlGroupCode(String glGroupCode);

}
