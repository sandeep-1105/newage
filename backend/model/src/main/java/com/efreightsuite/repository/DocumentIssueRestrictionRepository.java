package com.efreightsuite.repository;

import com.efreightsuite.model.DocumentIssueRestriction;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;


public interface DocumentIssueRestrictionRepository extends JpaRepository<DocumentIssueRestriction, Long> {

    @Query("Select di from DocumentIssueRestriction di where di.companyMaster.id=:companyId and di.locationMaster.id=:locationId and di.serviceMaster.id=:serviceId")
    public DocumentIssueRestriction findExceptionalTermByCompanyMasterAndLocationMasterAndServiceMaster(@Param("companyId") Long companyId,
                                                                                                        @Param("locationId") Long locationId, @Param("serviceId") Long serviceId);

    public DocumentIssueRestriction findById(Long id);

}
