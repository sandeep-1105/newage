package com.efreightsuite.repository;

import com.efreightsuite.model.ServiceTaxChargeGroupMaster;

import org.springframework.data.jpa.repository.JpaRepository;


public interface ServiceTaxChargeGroupMasterRepository extends JpaRepository<ServiceTaxChargeGroupMaster, Long> {

}
