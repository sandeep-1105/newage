package com.efreightsuite.repository.common;

import com.efreightsuite.model.common.CommonErrorMessage;

import org.springframework.data.jpa.repository.JpaRepository;

public interface CommonErrorMessageRepository extends JpaRepository<CommonErrorMessage, Long> {

    CommonErrorMessage findByErrorCode(String errorCode);

}
