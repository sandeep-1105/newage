package com.efreightsuite.repository;

import java.util.List;

import com.efreightsuite.model.CharterMaster;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface CharterMasterRepository extends JpaRepository<CharterMaster, Long> {

    CharterMaster findById(Long id);

    CharterMaster findByCharterCode(String charterCode);

    @Query("SELECT p FROM CharterMaster p ORDER BY p.charterCode, p.charterName ASC")
    @Override
    List<CharterMaster> findAll();

    @Query("Select p from CharterMaster p WHERE (UPPER(p.charterCode) like :keyword% or UPPER(p.charterName) like :keyword%) ORDER BY p.charterCode, p.charterName ASC")
    Page<CharterMaster> findByKeyword(@Param("keyword") String keyword, Pageable page);

}
