package com.efreightsuite.repository;

import java.util.List;

import com.efreightsuite.model.LocationMaster;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface LocationMasterRepository extends JpaRepository<LocationMaster, Long> {

    @Query("SELECT l from LocationMaster l where l.status!='Hide' ORDER BY l.locationName ASC")
    List<LocationMaster> getAllRecord();

    @Query("SELECT l from LocationMaster l where l.status!='Hide' AND l.countryMaster.id=?1 ORDER BY l.locationName ASC")
    List<LocationMaster> getByCountryWise(Long countryId);

    LocationMaster findById(Long id);


    @Query("SELECT l from LocationMaster l where l.status!='Hide' AND l.countryMaster.id=?1 AND l.partyMaster.id=?2")
    LocationMaster getByCountryAndAgent(Long countryId, Long agentId);

    LocationMaster findByLocationCode(String locationCode);

    LocationMaster findByLocationName(String locationName);


    @Query("SELECT l from LocationMaster l where UPPER(l.locationName)=?1")
    LocationMaster findByLocationNameInUpperCase(String locationName);
}
