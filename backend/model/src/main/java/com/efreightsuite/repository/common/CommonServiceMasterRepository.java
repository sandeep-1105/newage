package com.efreightsuite.repository.common;

import com.efreightsuite.model.common.CommonServiceMaster;

import org.springframework.data.jpa.repository.JpaRepository;


public interface CommonServiceMasterRepository extends JpaRepository<CommonServiceMaster, Long> {
}
