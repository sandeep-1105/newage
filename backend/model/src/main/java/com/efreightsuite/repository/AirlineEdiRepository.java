package com.efreightsuite.repository;

import javax.transaction.Transactional;

import com.efreightsuite.enumeration.EdiStatus;
import com.efreightsuite.model.AirlineEdi;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface AirlineEdiRepository extends JpaRepository<AirlineEdi, Long> {

    @Modifying
    @Transactional
    @Query("update AirlineEdi ae set ae.statusMessage = :statusMessage, ae.status = :status where ae.id = :airlineEdiId")
    int updateEdiStatus(@Param("statusMessage") String statusMessage, @Param("status") EdiStatus status, @Param("airlineEdiId") Long airlineEdiId);

}
