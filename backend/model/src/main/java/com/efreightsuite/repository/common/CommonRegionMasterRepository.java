package com.efreightsuite.repository.common;

import com.efreightsuite.enumeration.LovStatus;
import com.efreightsuite.model.common.CommonRegionMaster;
import org.hibernate.validator.constraints.NotBlank;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CommonRegionMasterRepository extends JpaRepository<CommonRegionMaster, Long> {

    CommonRegionMaster findByRegionCode(String regionCode);

    Page<CommonRegionMaster> findAllByStatusNotAndRegionNameStartingWithOrRegionCodeStartingWithAllIgnoreCaseOrderByRegionName(LovStatus status, String region, String regionCode, Pageable pageable);

    default Page<CommonRegionMaster> searchRegions(@NotBlank String query, PageRequest request) {
        return findAllByStatusNotAndRegionNameStartingWithOrRegionCodeStartingWithAllIgnoreCaseOrderByRegionName(LovStatus.Hide, query, query, request);
    }
}
