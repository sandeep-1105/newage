package com.efreightsuite.repository;

import com.efreightsuite.model.ObjectSubGroupMaster;

import org.springframework.data.jpa.repository.JpaRepository;

public interface ObjectSubgroupMasterRepository extends JpaRepository<ObjectSubGroupMaster, Long> {

}
