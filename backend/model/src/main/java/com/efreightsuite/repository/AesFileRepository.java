package com.efreightsuite.repository;

import java.util.List;

import javax.transaction.Transactional;

import com.efreightsuite.model.AesFile;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface AesFileRepository extends JpaRepository<AesFile, Long> {

    AesFile findByShipmentUid(String shipmentUid);

    AesFile findByServiceUid(String serviceuid);

    AesFile findByMasterUid(String masteruid);

    @Query("select a from AesFile a where a.masterUid=?1")
    List<AesFile> findAllByMasterUid(String masteruid);

    AesFile findById(Long id);

    AesFile findByBatchNo(String batchNo);

    @Modifying
    @Transactional
    @Query("update AesFile ae set ae.aesStatus = :status where ae.id = :airlineEdiId")
    int updateEdiStatus(@Param("status") String status, @Param("airlineEdiId") Long airlineEdiId);


}
