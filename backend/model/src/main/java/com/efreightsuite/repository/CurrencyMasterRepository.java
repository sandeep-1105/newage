package com.efreightsuite.repository;

import java.util.List;

import com.efreightsuite.model.CurrencyMaster;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface CurrencyMasterRepository extends JpaRepository<CurrencyMaster, Long> {

    @Query("Select c from CurrencyMaster c ORDER BY c.currencyName, c.currencyCode ASC")
    @Override
    List<CurrencyMaster> findAll();


    CurrencyMaster findByCurrencyCode(String currencyCode);

    @Query("Select c from CurrencyMaster c WHERE c.currencyCode!=?1 and c.status != 'Hide'  ORDER BY c.currencyName")
    List<CurrencyMaster> findExcludeCurrencyCode(String exculdeCurrencyCode);

    @Query("Select new com.efreightsuite.model.CurrencyMaster(c.id, c.currencyCode) from CurrencyMaster c WHERE c.id in :idList")
    List<CurrencyMaster> findIdAndNameByIds(@Param("idList") List<Long> idList);

    @Query("Select c from CurrencyMaster c WHERE (UPPER(c.currencyCode) like :keyword% or UPPER(c.currencyName) like :keyword%) ORDER BY c.currencyCode, c.currencyName ASC")
    Page<CurrencyMaster> findByKeyword(@Param("keyword") String keyword, Pageable page);

}
