package com.efreightsuite.repository;

import com.efreightsuite.model.ShipmentAttachment;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface ShipmentAttachmentRepository extends JpaRepository<ShipmentAttachment, Long> {

    @Query(value = "SELECT ea.file FROM ShipmentAttachment ea WHERE ea.id = ?1")
    byte[] getByteArray(Long id);

}


