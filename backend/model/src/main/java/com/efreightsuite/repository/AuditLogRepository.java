package com.efreightsuite.repository;

import java.util.List;

import com.efreightsuite.model.AuditLog;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface AuditLogRepository extends JpaRepository<AuditLog, Long> {

    AuditLog findById(Long id);

    AuditLog findByStatusCode(int statusCode);

    AuditLog findByMethodName(String methodName);

    @Override
    @Query("Select a from AuditLog a ORDER BY a.methodName ASC")
    List<AuditLog> findAll();


}
