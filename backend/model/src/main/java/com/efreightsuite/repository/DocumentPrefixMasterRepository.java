package com.efreightsuite.repository;

import com.efreightsuite.enumeration.DocPrefixDocumentType;
import com.efreightsuite.model.DocumentPrefixMaster;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface DocumentPrefixMasterRepository extends JpaRepository<DocumentPrefixMaster, Long> {

    DocumentPrefixMaster findById(Long id);

    @Query(value = "select dc.documentPrefixMaster from DocPrefixTypeMapping dc where dc.locationMaster.id=?1 and dc.docType=?2")
    DocumentPrefixMaster getByDocumentPrefixMaster(Long locationId, DocPrefixDocumentType docPrefixDocumentType);


}
