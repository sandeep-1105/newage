package com.efreightsuite.repository;

import com.efreightsuite.model.QuotationAttachment;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface QuotationAttachmentRepository extends JpaRepository<QuotationAttachment, Long> {

    @Query(value = "SELECT ea.file FROM QuotationAttachment ea WHERE ea.id = ?1")
    byte[] getByteArray(Long id);

}


