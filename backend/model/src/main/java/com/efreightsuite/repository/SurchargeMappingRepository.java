package com.efreightsuite.repository;

import com.efreightsuite.enumeration.SurchargeType;
import com.efreightsuite.model.SurchargeMapping;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface SurchargeMappingRepository extends JpaRepository<SurchargeMapping, Long> {

    @Query(value = "select sm from SurchargeMapping sm where sm.chargeMaster.id=?1")
    SurchargeMapping getByChargeMaster(Long chargeId);

    @Query(value = "select sm from SurchargeMapping sm where sm.surchargeType=?1")
    SurchargeMapping getBySurchargeType(SurchargeType surchargeType);

}
