package com.efreightsuite.repository;

import java.util.List;

import com.efreightsuite.model.ConsolDimension;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface ConsolDocumentDimensionRepository extends JpaRepository<ConsolDimension, Long> {

    @Query(value = "select cd from ConsolDimension cd where cd.id = :consolId")
    List<ConsolDimension> findAllDimension(@Param("consolId") Long consolId);

	/*@Query("select dd.shipper from DocumentDetail dd where dd.id = :documentID")
	PartyMaster  findByIdForShipper(@Param("documentID") Long documentID);*/
}
