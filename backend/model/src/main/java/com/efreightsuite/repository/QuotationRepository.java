package com.efreightsuite.repository;

import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import com.efreightsuite.enumeration.Approved;
import com.efreightsuite.model.Quotation;
import com.efreightsuite.model.QuotationAttachment;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface QuotationRepository extends JpaRepository<Quotation, Long> {

    Quotation findById(Long id);

    Quotation findByQuotationNo(String quotationNo);

    @Query(value = "select q from Quotation q where q.enquiryNo=?1")
    Quotation findByEnquiryNo(String enquiryNo);

    @Override
    List<Quotation> findAll();

    @Query(value = "select q from QuotationAttachment q where q.quotation.id=?1")
    List<QuotationAttachment> getAllAttachment(Long quotationId);

    @Modifying
    @Transactional
    @Query(value = "UPDATE Quotation q SET q.approved=?1, q.approvedBy.id=?2, q.approvedDate=?3 WHERE q.id=?4")
    int customerApproval(Approved approved, Long employeeId, Date date, Long quotationId);

    @Query("select e from Quotation e WHERE e.id in :idList order by e.id desc")
    List<Quotation> findAllByIds(@Param("idList") List<Long> idList);


    @Query(value = "select q.approved from Quotation q where q.quotationNo=?1")
    Approved findStatus(String quotationNo);
}


