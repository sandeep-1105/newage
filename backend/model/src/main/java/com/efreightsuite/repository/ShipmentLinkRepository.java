package com.efreightsuite.repository;

import java.util.List;

import com.efreightsuite.model.ShipmentLink;
import com.efreightsuite.model.ShipmentServiceDetail;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface ShipmentLinkRepository extends JpaRepository<ShipmentLink, Long> {

    public ShipmentLink findByService(ShipmentServiceDetail shipmentServiceDetail);

    public ShipmentLink findByServiceUid(String serviceUid);

    @Query(value = "select s from ShipmentLink s where s.consolUid=?1")
    public List<ShipmentLink> findAllByConsolUid(String consolUid);


}
