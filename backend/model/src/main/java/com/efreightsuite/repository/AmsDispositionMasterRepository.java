package com.efreightsuite.repository;

import com.efreightsuite.model.AmsDispositionMaster;

import org.springframework.data.jpa.repository.JpaRepository;

public interface AmsDispositionMasterRepository extends JpaRepository<AmsDispositionMaster, Long> {
    AmsDispositionMaster findByCode(String code);
}
