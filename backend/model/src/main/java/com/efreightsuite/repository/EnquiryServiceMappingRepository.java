package com.efreightsuite.repository;


import com.efreightsuite.enumeration.ImportExport;
import com.efreightsuite.enumeration.Service;
import com.efreightsuite.model.EnquiryServiceMapping;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface EnquiryServiceMappingRepository extends JpaRepository<EnquiryServiceMapping, Long> {

    @Query("select esm from EnquiryServiceMapping esm WHERE esm.service=:service AND esm.importExport =:importExport")
    EnquiryServiceMapping findByServiceAndTransportMode(@Param("service") Service service, @Param("importExport") ImportExport importExport);

}
