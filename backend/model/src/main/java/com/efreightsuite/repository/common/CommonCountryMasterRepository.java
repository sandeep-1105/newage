package com.efreightsuite.repository.common;

import com.efreightsuite.enumeration.LovStatus;
import com.efreightsuite.model.common.CommonCountryMaster;
import org.hibernate.validator.constraints.NotBlank;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CommonCountryMasterRepository extends JpaRepository<CommonCountryMaster, Long> {
    CommonCountryMaster findByCountryCode(String countryCode);

    Page<CommonCountryMaster> findAllByStatusNotAndCountryNameStartingWithOrCountryCodeStartingWithAllIgnoreCaseOrderByCountryName(LovStatus status, String country, String countryCode, Pageable pageable);

    default Page<CommonCountryMaster> searchCountries(@NotBlank String query, PageRequest request) {
        return findAllByStatusNotAndCountryNameStartingWithOrCountryCodeStartingWithAllIgnoreCaseOrderByCountryName(LovStatus.Hide, query, query, request);
    }
}
