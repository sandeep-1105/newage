package com.efreightsuite.repository;

import java.util.List;

import com.efreightsuite.model.SubLedgerMaster;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface SubLedgerMasterRepository extends JpaRepository<SubLedgerMaster, Long> {

    @Query(value = "select c from SubLedgerMaster c where c.status!='Hide' order by c.ledgerName asc")
    List<SubLedgerMaster> findAll();

}
