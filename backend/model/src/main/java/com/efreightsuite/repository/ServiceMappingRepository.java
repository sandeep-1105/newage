package com.efreightsuite.repository;

import com.efreightsuite.model.ServiceMapping;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface ServiceMappingRepository extends JpaRepository<ServiceMapping, Long> {

    public ServiceMapping findByImportCode(String importCode);

    public ServiceMapping findByExportCode(String exportCode);

    @Query(value = "select s from ServiceMapping s where s.exportService.id=?1")
    public ServiceMapping findByImport(Long exportId);

    @Query(value = "select s from ServiceMapping s where s.importService.id=?1")
    public ServiceMapping findByExport(Long importId);

    @Query(value = "select s from ServiceMapping s where s.exportService.id=?1 and s.importService.id=?2")
    public ServiceMapping findByImportAndExport(Long exportId, Long importId);
}
