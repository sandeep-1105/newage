package com.efreightsuite.repository;

import java.util.List;

import com.efreightsuite.enumeration.DocumentType;
import com.efreightsuite.model.InvoiceCreditNote;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface InvoiceCreditNoteRepository extends JpaRepository<InvoiceCreditNote, Long> {

    InvoiceCreditNote findById(Long id);

    @Query("select icn from InvoiceCreditNote icn where icn.shipmentServiceDetail.id=?1")
    List<InvoiceCreditNote> findByShipmentServiceDetail(Long shipmentServiceId);

    @Query("select icn from InvoiceCreditNote icn where icn.shipmentServiceDetail.id=?1 and icn.documentTypeMaster.documentTypeCode=?2")
    List<InvoiceCreditNote> findByShipmentServiceDetail(Long shipmentServiceId, DocumentType documentType);

    @Query("select icn from InvoiceCreditNote icn where icn.masterUid=?1 and icn.shipmentServiceDetail.id is null")
    List<InvoiceCreditNote> findByConsolUid(String consolUid);

    @Query("select count(*)  from InvoiceCreditNote icn where icn.masterUid=:masterUid or icn.shipmentServiceDetail.id in (:serviceUids)")
    Long countByConsolUidAndServiceUid(@Param("masterUid") String masterUid, @Param("serviceUids") Long[] serviceUids);

    @Query("select icn from InvoiceCreditNote icn where icn.invoiceCreditNoteNo=?1 and icn.documentTypeMaster.documentTypeCode='INV' and icn.daybookMaster.id=?2")
    InvoiceCreditNote findByInvoiceCreditNoteNoAndDaybook(String invoiceCreditNoteNo, Long daybookId);

    @Query("select icn from InvoiceCreditNote icn where icn.invoiceCreditNoteNo=?1 and icn.documentTypeMaster.documentTypeCode='INV'")
    InvoiceCreditNote findByInvoiceCreditNoteNo(String invoiceCreditNoteNo);

    @Query("select icn from InvoiceCreditNote icn where icn.shipmentServiceDetail.serviceUid=?1")
    List<InvoiceCreditNote> findByServiceUid(String serviceUid);

    @Query(value = "SELECT ea.file FROM InvoiceCreditNoteAttachment ea WHERE ea.id = ?1")
    byte[] getByteArray(Long id);

}
