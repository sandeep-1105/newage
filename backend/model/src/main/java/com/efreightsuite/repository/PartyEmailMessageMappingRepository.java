package com.efreightsuite.repository;

import java.util.List;

import com.efreightsuite.model.PartyEmailMessageMapping;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author Devendrachary M
 */

public interface PartyEmailMessageMappingRepository extends JpaRepository<PartyEmailMessageMapping, Long> {

    PartyEmailMessageMapping findById(Long id);

    @Override
    List<PartyEmailMessageMapping> findAll();


}
