package com.efreightsuite.repository;

import java.util.List;

import com.efreightsuite.model.ChargeMaster;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface ChargeMasterRepository extends JpaRepository<ChargeMaster, Long> {

    ChargeMaster findById(Long id);

    ChargeMaster findByChargeCode(String chargeCode);

    ChargeMaster findByChargeName(String chargeName);

    @Query("Select c from ChargeMaster c ORDER BY c.chargeName, c.chargeCode ASC")
    @Override
    List<ChargeMaster> findAll();

    @Query("Select c from ChargeMaster c where ( UPPER(c.chargeCode) like :keyword% or UPPER(c.chargeName) like :keyword%) ORDER BY c.chargeCode, c.chargeName ASC")
    Page<ChargeMaster> findByKeyword(@Param("keyword") String keyword, Pageable page);

}
