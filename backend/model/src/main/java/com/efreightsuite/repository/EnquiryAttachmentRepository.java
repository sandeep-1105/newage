package com.efreightsuite.repository;

import com.efreightsuite.model.EnquiryAttachment;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface EnquiryAttachmentRepository extends JpaRepository<EnquiryAttachment, Long> {

    @Query(value = "SELECT ea.file FROM EnquiryAttachment ea WHERE ea.id = ?1")
    byte[] getByteArray(Long id);

}
