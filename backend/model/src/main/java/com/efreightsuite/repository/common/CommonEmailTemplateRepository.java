package com.efreightsuite.repository.common;

import com.efreightsuite.enumeration.common.TemplateType;
import com.efreightsuite.model.common.CommonEmailTemplate;

import org.springframework.data.jpa.repository.JpaRepository;

public interface CommonEmailTemplateRepository extends JpaRepository<CommonEmailTemplate, Long> {

    CommonEmailTemplate findById(Long id);

    CommonEmailTemplate findByTemplateType(TemplateType templateType);

}
