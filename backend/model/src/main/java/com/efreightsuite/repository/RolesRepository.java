package com.efreightsuite.repository;

import com.efreightsuite.model.Role;

import org.springframework.data.jpa.repository.JpaRepository;

public interface RolesRepository extends JpaRepository<Role, Long> {

    public Role findByRoleName(String roleName);

    public Role findById(Long roleId);

}
