package com.efreightsuite.repository;

import com.efreightsuite.model.Provisional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface ProvisionalRepository extends JpaRepository<Provisional, Long> {

    Provisional findById(Long id);

    @Query("select p from Provisional p where p.masterUid=?1 and p.serviceUid is null")
    Provisional findByMasterUid(String masterUid);

    Provisional findByServiceUid(String serviceUid);

}
