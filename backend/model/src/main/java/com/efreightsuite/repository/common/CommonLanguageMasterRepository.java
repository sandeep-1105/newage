package com.efreightsuite.repository.common;

import com.efreightsuite.model.common.CommonLanguageMaster;

import org.springframework.data.jpa.repository.JpaRepository;

public interface CommonLanguageMasterRepository extends JpaRepository<CommonLanguageMaster, Long> {
}
