package com.efreightsuite.repository;

import com.efreightsuite.model.AirlinePrebooking;

import org.springframework.data.jpa.repository.JpaRepository;

public interface AirlinePrebookingRepository extends JpaRepository<AirlinePrebooking, Long> {

    AirlinePrebooking findById(Long id);

}
