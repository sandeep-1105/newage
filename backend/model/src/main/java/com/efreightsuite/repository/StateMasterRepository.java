package com.efreightsuite.repository;

import java.util.List;

import com.efreightsuite.model.StateMaster;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface StateMasterRepository extends JpaRepository<StateMaster, Long> {

    @Query(value = "select s from StateMaster s where s.status!='Hide' order by s.stateName asc")
    List<StateMaster> findAll();

    @Query(value = "select s from StateMaster s where s.stateCode=?1 and s.countryMaster.id=?2")
    StateMaster getState(String stateCode, Long id);

    @Query(value = "select s from StateMaster s where s.stateCode=?1 and s.countryCode=?2")
    StateMaster getState(String stateCode, String countryCode);

    @Query("FROM StateMaster sm WHERE UPPER(sm.stateName) = :stateName")
    StateMaster findByStateName(@Param("stateName") String stateName);

    StateMaster findByStateCode(String stateCode);
}
