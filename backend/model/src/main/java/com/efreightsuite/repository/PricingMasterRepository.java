package com.efreightsuite.repository;

import java.util.List;

import com.efreightsuite.enumeration.TransportMode;
import com.efreightsuite.enumeration.WhoRouted;
import com.efreightsuite.model.PricingCarrier;
import com.efreightsuite.model.PricingMaster;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface PricingMasterRepository extends JpaRepository<PricingMaster, Long> {

    PricingMaster findById(Long id);

    @Query("Select pm from PricingMaster pm where pm.origin.id=:originId and pm.destination.id=:destinationId and pm.transit is NULL  ")
    PricingMaster findAllByOriginIdAndDestinationId(@Param("originId") Long originId, @Param("destinationId") Long destinationId);

    @Query("Select pm from PricingMaster pm where pm.origin.id=:originId and pm.destination.id=:destinationId and pm.whoRouted=:whoRouted and pm.transit is NULL  ")
    PricingMaster findAllByOriginIdAndDestinationIdAndRouted(@Param("originId") Long originId, @Param("destinationId") Long destinationId, @Param("whoRouted") WhoRouted whoRouted);


    @Query("Select pm.generalFreightPriceList from PricingMaster pm  join pm.generalFreightPriceList as gf  where gf.carrierMaster.id =:carrierId and pm.origin.id=:originId and pm.destination.id=:destinationId")
    List<PricingCarrier> findAllByOriginIdAndDestinationIdAndCarrierId(@Param("originId") Long originId, @Param("destinationId") Long destinationId, @Param("carrierId") Long carrierId);

    @Query("Select pm.hazardousFreightPriceList from PricingMaster pm  join pm.hazardousFreightPriceList as gf  where gf.carrierMaster.id =:carrierId and pm.origin.id=:originId and pm.destination.id=:destinationId")
    List<PricingCarrier> findAllByOriginIdAndDestinationIdAndCarrierIdForHazardous(@Param("originId") Long originId, @Param("destinationId") Long destinationId, @Param("carrierId") Long carrierId);


    @Query("Select pm from PricingMaster pm where pm.origin.portCode=?1 and pm.destination.portCode=?2 and pm.whoRouted=?3 and pm.transit IS NULL and pm.transportMode=?4")
    PricingMaster getByOriginAndDestAndRoutedAndTrasitIsNullAndTransportMode(String originId, String destinationId, WhoRouted whoRouted, TransportMode transportMode);

    @Query("Select pm from PricingMaster pm where pm.origin.portCode=?1 and pm.destination.portCode=?2 and pm.transit.portCode=?3 and pm.whoRouted=?4 and pm.transportMode=?5")
    PricingMaster getByOriginAndDestAndTrasitAndRoutedAndTransportMode(String originId, String destinationId, String transitId, WhoRouted whoRouted, TransportMode transportMode);

    @Query("Select pm from PricingMaster pm where pm.origin.id=?1 and pm.destination.id=?2 and pm.whoRouted=?3 and pm.transit IS NULL and pm.transportMode=?4")
    PricingMaster getByBasedIdOriginAndDestAndRoutedAndTrasitIsNullAndTransportMode(Long originId, Long destinationId, WhoRouted whoRouted, TransportMode transportMode);

    @Query("Select pm from PricingMaster pm where pm.origin.id=?1 and pm.destination.id=?2 and pm.transit.id=?3 and pm.whoRouted=?4 and pm.transportMode=?5")
    PricingMaster getByBasedIdOriginAndDestAndTrasitAndRoutedAndTransportMode(Long originId, Long destinationId, Long transitId, WhoRouted whoRouted, TransportMode transportMode);

    @Query("Select pm from PricingMaster pm where pm.locationMaster.id=?1 and pm.origin is null and pm.destination is null and pm.whoRouted=?2 and pm.transit IS NULL and pm.transportMode=?3")
    PricingMaster getByLocationCharge(Long locationId, WhoRouted whoRouted, TransportMode transportMode);

    @Query("Select pm from PricingMaster pm where pm.locationMaster.id=?1 and pm.origin.id=?2 and pm.destination is null and pm.whoRouted=?3 and pm.transit IS NULL and pm.transportMode=?4")
    PricingMaster getByLocationChargeWithOrigin(Long locationId, Long originId, WhoRouted whoRouted, TransportMode transportMode);

    @Query("Select pm from PricingMaster pm where pm.locationMaster.id=?1 and pm.origin is null and pm.destination.id=?2 and pm.whoRouted=?3 and pm.transit IS NULL and pm.transportMode=?4")
    PricingMaster getByLocationChargeWithDestination(Long locationId, Long destinationId, WhoRouted whoRouted, TransportMode transportMode);


}
