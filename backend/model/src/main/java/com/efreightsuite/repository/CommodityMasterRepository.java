package com.efreightsuite.repository;

import java.util.List;

import com.efreightsuite.model.CommodityMaster;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;


public interface CommodityMasterRepository extends JpaRepository<CommodityMaster, Long> {

    CommodityMaster findById(Long id);

/*
	CommodityMaster findByCommodityCode(String commodityCode);


	CommodityMaster findById(Long id);*/

    @Override
    @Query("Select c from CommodityMaster c ORDER BY c.hsName ASC")
    List<CommodityMaster> findAll();

    CommodityMaster findByhsCode(String string);

	/*@Query("Select c from CommodityMaster c where (UPPER(c.hsName) like :keyword% or UPPER(c.hsCode) like :keyword%) ORDER BY c.hsName ASC")
	Page<CommodityMaster> findByKeyword(@Param("keyword")String keyword,Pageable page);*/


}
