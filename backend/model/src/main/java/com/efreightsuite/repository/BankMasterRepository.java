package com.efreightsuite.repository;

import com.efreightsuite.model.BankMaster;

import org.springframework.data.jpa.repository.JpaRepository;

public interface BankMasterRepository extends JpaRepository<BankMaster, Long> {


}
