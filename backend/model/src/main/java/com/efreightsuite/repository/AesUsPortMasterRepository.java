package com.efreightsuite.repository;

import com.efreightsuite.model.AesUsPortMaster;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface AesUsPortMasterRepository extends JpaRepository<AesUsPortMaster, Long> {

    public AesUsPortMaster findById(Long Id);

    public AesUsPortMaster findByPortCode(String code);


    @Query("Select p from AesUsPortMaster p WHERE (UPPER(p.portName) like UPPER(:keyword) or UPPER(p.portCode) like UPPER(:keyword))")
    Page<AesUsPortMaster> findByKeyword(@Param("keyword") String keyword, Pageable page);

}
