package com.efreightsuite.repository.common;

import java.util.Optional;
import java.util.Set;

import com.efreightsuite.model.common.LocationSetup;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface LocationSetupRepository extends JpaRepository<LocationSetup, Long> {

    Optional<LocationSetup> findByCompanyRegisteredNameIgnoreCase(String companyRegisteredName);

    default Optional<LocationSetup> findByCompanyRegisteredName(String companyRegisteredName){
        return findByCompanyRegisteredNameIgnoreCase(companyRegisteredName);
    }

    @Query("select ls.saasId FROM LocationSetup ls")
    Set<String> findAllSaasIds();

}
