package com.efreightsuite.repository;

import java.util.List;

import com.efreightsuite.model.PortGroupMaster;
import com.efreightsuite.model.PortMaster;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface PortMasterRepository extends JpaRepository<PortMaster, Long> {

    PortMaster findById(Long id);

    PortMaster findByPortCode(String portCode);

    PortMaster findByPortName(String portName);

    @Override
    @Query("Select p from PortMaster p ORDER BY p.portName, p.portCode ASC")
    List<PortMaster> findAll();

    @Query("Select p from PortMaster p where ( UPPER(p.portCode) like :keyword% or UPPER(p.portName) like :keyword%) ORDER BY p.portCode, p.portName ASC")
    Page<PortMaster> findByKeyword(@Param("keyword") String keyword, Pageable page);

    @Query("Select p from PortMaster p where p.portGroupMaster.id=:portGroupId ORDER BY p.portName")
    List<PortMaster> findAllByPortGroup(@Param("portGroupId") Long portGroupId);

    @Query(value = "select a.portGroupMaster from  PortMaster a where a.portGroupMaster.id=?1")
    PortGroupMaster getPortGroup(Long portId);

    @Query(value = "select a.portGroupMaster from  PortMaster a where a.id=?1")
    PortGroupMaster getPortGroupByPortId(Long portId);


}
