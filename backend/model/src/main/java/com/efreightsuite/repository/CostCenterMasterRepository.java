package com.efreightsuite.repository;

import java.util.List;

import com.efreightsuite.model.CostCenterMaster;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface CostCenterMasterRepository extends JpaRepository<CostCenterMaster, Long> {

    CostCenterMaster findById(Long id);

    CostCenterMaster findByCostCenterCode(String costCenterCode);

    CostCenterMaster findByCostCenterName(String costCenterName);

    @Query("Select c from CostCenterMaster c ORDER BY c.costCenterName, c.costCenterCode ASC")
    @Override
    List<CostCenterMaster> findAll();

    @Query("Select c from CostCenterMaster c where (c.costCenterCode like %:keyword% or c.costCenterName like %:keyword%) ORDER BY c.costCenterName ASC")
    List<CostCenterMaster> findByKeyword(@Param("keyword") String keyword);

}
