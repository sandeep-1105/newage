package com.efreightsuite.repository;

import java.util.List;

import com.efreightsuite.model.ServiceDocumentDimension;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface ServiceDocumentDimensionRepository extends JpaRepository<ServiceDocumentDimension, Long> {

    @Query("select new com.efreightsuite.model.ServiceDocumentDimension(dd.noOfPiece, dd.length, dd.width, dd.height, dd.volWeight, dd.grossWeight, dd.grossWeightKg) from ServiceDocumentDimension dd where dd.documentDetail.id = :documentID")
    List<ServiceDocumentDimension> findAllDimensionsByDocument(@Param("documentID") Long documentID);
}
