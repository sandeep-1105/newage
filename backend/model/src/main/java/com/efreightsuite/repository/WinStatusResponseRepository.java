package com.efreightsuite.repository;

import com.efreightsuite.model.WinStatusResponse;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface WinStatusResponseRepository extends JpaRepository<WinStatusResponse, Long> {

    @Query("SELECT w FROM  WinStatusResponse w where w.awbId=?1")
    WinStatusResponse findByAwbId(Long awbId);


}
