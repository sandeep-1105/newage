package com.efreightsuite.repository;

import java.util.List;

import com.efreightsuite.model.Language;
import com.efreightsuite.model.LanguageDetail;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface LanguageDetailRepository extends JpaRepository<LanguageDetail, Long> {

    LanguageDetail findById(Long id);

    LanguageDetail findByLanguageCode(String languageCode);

    List<LanguageDetail> findByLanguage(Language language);

    @Query("SELECT DISTINCT c.groupName FROM LanguageDetail AS c order by c.groupName asc")
    List<String> fetchAllGroupName();

    @Override
    @Query("Select e from LanguageDetail e ORDER BY e.languageCode ASC")
    List<LanguageDetail> findAll();

    @Query("Select e from LanguageDetail e WHERE e.language = ?1 ORDER BY e.languageCode ASC")
    List<LanguageDetail> findAllByLanguage(Language language);

    @Query("Select e from LanguageDetail e where (e.languageCode like :keyword% or e.languageDesc like :keyword%) ORDER BY e.languageCode ASC")
    Page<LanguageDetail> findByKeyword(@Param("keyword") String keyword, Pageable page);

}
