package com.efreightsuite.repository;

import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import com.efreightsuite.enumeration.YesNo;
import com.efreightsuite.model.AddressMaster;
import com.efreightsuite.model.CarrierMaster;
import com.efreightsuite.model.DocumentDetail;
import com.efreightsuite.model.PartyMaster;
import com.efreightsuite.model.PortMaster;
import com.efreightsuite.model.ShipmentServiceDetail;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface DocumentDetailRepository extends JpaRepository<DocumentDetail, Long> {

    DocumentDetail findById(Long documentID);

    @Query("select dd.shipper from DocumentDetail dd where dd.id = :documentID")
    PartyMaster findByIdForShipper(@Param("documentID") Long documentID);

    @Query("select dd.shipperAddress from DocumentDetail dd where dd.id = :documentID")
    AddressMaster findByIdForShipperAddress(@Param("documentID") Long documentID);

    @Query("select dd.consignee from DocumentDetail dd where dd.id = :documentID")
    PartyMaster findByIdForConsignee(@Param("documentID") Long documentID);

    @Query("select dd.consigneeAddress from DocumentDetail dd where dd.id = :documentID")
    AddressMaster findByIdForConsigneeAddress(@Param("documentID") Long documentID);

    @Query("select dd.agent from DocumentDetail dd where dd.id = :documentID")
    PartyMaster findByIdForAgent(@Param("documentID") Long documentID);

    @Query("select dd.agentAddress from DocumentDetail dd where dd.id = :documentID")
    AddressMaster findByIdForAgentAddress(@Param("documentID") Long documentID);


    @Query("select dd.issuingAgentAddress from DocumentDetail dd where dd.id = :documentID")
    AddressMaster findByIdForIssuingAgentAddress(@Param("documentID") Long documentID);


    @Query("select dd.firstNotify from DocumentDetail dd where dd.id = :documentID")
    PartyMaster findByIdForFirstNotify(@Param("documentID") Long documentID);

    @Query("select dd.firstNotifyAddress from DocumentDetail dd where dd.id = :documentID")
    AddressMaster findByIdForFirstNotifyAddress(@Param("documentID") Long documentID);

    @Query("select dd.shipmentServiceDetail from DocumentDetail dd where dd.id = :documentID")
    ShipmentServiceDetail findByIdForShipmentServiceDetail(@Param("documentID") Long documentID);

    @Query("select dd.issuingAgent from DocumentDetail dd where dd.id = :documentID")
    PartyMaster findByIdForIssuingAgent(@Param("documentID") Long documentID);

    @Query("select dd.pol from DocumentDetail dd where dd.id = :documentID")
    PortMaster findByIdForPol(@Param("documentID") Long documentID);

    @Query("select dd.carrier from DocumentDetail dd where dd.id = :documentID")
    CarrierMaster findByIdForCarrier(@Param("documentID") Long documentID);


    @Query("select dd.destination from DocumentDetail dd where dd.id = :documentID")
    PortMaster findByIdForDestination(@Param("documentID") Long documentID);

    @Query("select dd from DocumentDetail dd where dd.id = :documentID")
    DocumentDetail findByIdForNonMappingObjects(@Param("documentID") Long documentID);


    @Query(value = "select dd from DocumentDetail dd where dd.shipmentServiceDetail.id=?1")
    List<DocumentDetail> findAllDocumentById(Long shipmentId);

    @Query("select dd from DocumentDetail dd where dd.documentNo = :documentNo")
    DocumentDetail findByDocumentNo(@Param("documentNo") String documentNo);

    @Query("select dd from DocumentDetail dd where dd.hawbNo = :hawbNo")
    DocumentDetail findByHawbNo(@Param("hawbNo") String hawbNo);

    @Modifying
    @Transactional
    @Query("update DocumentDetail dd set dd.customerIp=?1, dd.isCustomerConform=?2 where dd.id = ?3")
    int updateIsCustomerConform(String ip, YesNo isCustomerConform, Long documentID);


    @Query("select dd.shipmentServiceDetail from DocumentDetail dd where dd.id = :documentID")
    ShipmentServiceDetail findByService(@Param("documentID") Long documentID);

    @Modifying
    @Query(value = "UPDATE DocumentDetail dd SET dd.doNumber=?1, dd.doIssuedDate = ?2 where dd.id = ?3")
    int updateDoNumber(String doNumber, Date doIssuedDate, Long id);

}
