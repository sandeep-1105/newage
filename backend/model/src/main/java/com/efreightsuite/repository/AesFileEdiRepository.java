package com.efreightsuite.repository;

import javax.transaction.Transactional;

import com.efreightsuite.model.AesFileEdi;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface AesFileEdiRepository extends JpaRepository<AesFileEdi, Long> {

    public AesFileEdi findById(Long Id);


    @Modifying
    @Transactional
    @Query("update AesFileEdi ae set ae.messagingStatus = :statusMessage where ae.id = :aesEdiId")
    int updateEdiStatus(@Param("statusMessage") String statusMessage, @Param("aesEdiId") Long aesEdiId);
}
