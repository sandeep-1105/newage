package com.efreightsuite.repository;

import com.efreightsuite.model.ServiceOldEntry;

import org.springframework.data.jpa.repository.JpaRepository;


public interface ServiceOldEntryRepository extends JpaRepository<ServiceOldEntry, Long> {

}
