package com.efreightsuite.repository;

import java.util.List;

import com.efreightsuite.model.PartyAccountMaster;
import com.efreightsuite.model.PartyCreditLimit;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface PartyCreditLimitRepository extends JpaRepository<PartyCreditLimit, Long> {
    PartyAccountMaster findById(Long id);

    @Override
    List<PartyCreditLimit> findAll();

    @Query(value = "select p from PartyCreditLimit p where p.partyMaster.id=?1")
    List<PartyCreditLimit> getAll(Long id);
}
