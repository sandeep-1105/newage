package com.efreightsuite.repository;

import com.efreightsuite.enumeration.ReportConfig;
import com.efreightsuite.model.ReportConfigurationMaster;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface ReportConfigurationMasterRepository extends JpaRepository<ReportConfigurationMaster, Long> {

    @Query(value = "select rc from ReportConfigurationMaster rc where rc.reportKey = ?1 and rc.location.id=?2")
    ReportConfigurationMaster getByKeyAndLocation(ReportConfig reportKey, Long locationId);

}
