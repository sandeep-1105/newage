package com.efreightsuite.repository;

import java.util.List;

import com.efreightsuite.model.PortGroupMaster;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface PortGroupMasterRepository extends JpaRepository<PortGroupMaster, Long> {

    PortGroupMaster findById(Long id);

    PortGroupMaster findByPortGroupCode(String portGroupCode);

    PortGroupMaster findByPortGroupName(String portGroupName);

    @Query("Select p from PortGroupMaster p ORDER BY p.portGroupName, p.portGroupCode ASC")
    @Override
    List<PortGroupMaster> findAll();

    @Query("Select p from PortGroupMaster p WHERE (UPPER(p.portGroupCode) like :keyword% or UPPER(p.portGroupName) like :keyword%) ORDER BY p.portGroupCode, p.portGroupName ASC")
    Page<PortGroupMaster> findByKeyword(@Param("keyword") String keyword, Pageable page);

    @Query("Select p from PortGroupMaster p WHERE p.countryMaster.countryCode=?1")
    PortGroupMaster findByCountry(String countryCode);

}
