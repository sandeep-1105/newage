package com.efreightsuite.repository;

import com.efreightsuite.model.ScacMaster;

import org.springframework.data.jpa.repository.JpaRepository;

public interface ScacMasterRepository extends JpaRepository<ScacMaster, Long> {

}
