package com.efreightsuite.repository;

import java.util.List;

import com.efreightsuite.model.PackMaster;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface PackMasterRepository extends JpaRepository<PackMaster, Long> {

    PackMaster findById(Long id);

    PackMaster findByPackCode(String packCode);

    @Query("SELECT p FROM PackMaster p ORDER BY p.packCode, p.packName ASC")
    @Override
    List<PackMaster> findAll();

    @Query("Select p from PackMaster p WHERE (UPPER(p.packCode) like :keyword% or UPPER(p.packName) like :keyword%) ORDER BY p.packCode, p.packName ASC")
    Page<PackMaster> findByKeyword(@Param("keyword") String keyword, Pageable page);

}
