package com.efreightsuite.repository;

import java.util.List;

import com.efreightsuite.model.PartyServiceMaster;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

/**
 * InterfaceName                  Purpose                               CreatedBy/ModifiedBy      Date          Version
 * <p>
 * PartyGroupMasterRepository   Repository for PartyServiceMaster           Devendrachary M        25-02-15       0.0
 */
public interface PartyServiceMasterRepository extends JpaRepository<PartyServiceMaster, Long> {

    PartyServiceMaster findById(Long id);


    @Override
    List<PartyServiceMaster> findAll();

    @Query(value = "select p from PartyServiceMaster p where p.partyMaster.id=?1 order by p.serviceMaster.serviceName asc")
    List<PartyServiceMaster> getAll(Long id);

}
