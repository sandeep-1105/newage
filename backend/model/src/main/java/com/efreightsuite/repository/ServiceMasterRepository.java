package com.efreightsuite.repository;

import com.efreightsuite.model.ServiceMaster;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;


public interface ServiceMasterRepository extends JpaRepository<ServiceMaster, Long> {

    public ServiceMaster findByServiceCode(String serviceCode);

    @Query("select s from ServiceMaster s where s.transportMode='Air' And s.importExport='Export' And s.serviceType is null and s.serviceCode='AE'")
    public ServiceMaster getAirExportService();


}
