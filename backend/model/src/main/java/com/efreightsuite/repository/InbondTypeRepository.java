package com.efreightsuite.repository;

import com.efreightsuite.model.InBondTypeMaster;

import org.springframework.data.jpa.repository.JpaRepository;

public interface InbondTypeRepository extends JpaRepository<InBondTypeMaster, Long> {


}
