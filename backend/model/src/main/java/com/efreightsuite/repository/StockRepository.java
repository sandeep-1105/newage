package com.efreightsuite.repository;

import java.util.List;

import com.efreightsuite.model.StockGeneration;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;


public interface StockRepository extends JpaRepository<StockGeneration, Long> {


    @Query(value = "select s from StockGeneration s where s.stockStatus!='Cancelled' AND s.carrier.id = ?1 AND s.por.id = ?2  order by s.id desc")
    public List<StockGeneration> getStock(Long carrierId, Long porId);


    @Query(value = "select s from StockGeneration s where s.stockStatus!='Available' AND s.carrier.id = ?1 AND s.por.id = ?2  order by s.id desc")
    public List<StockGeneration> getStock(Long carrierId, Long porId, Pageable page);

    @Query(value = "select sg from StockGeneration sg where sg.remindNo IS NOT NULL AND sg.carrier.id=?1 and por.id=?2")
    StockGeneration updateRemindNo(Long carrierId, Long porId);


    @Query(value = "select sg from StockGeneration sg where sg.mawbNo=?1")
    public StockGeneration findByMawb(String mawbNo);

    @Query(value = "select s.mawbNo from StockGeneration s where s.stockStatus='Available' AND s.carrier.id = ?1 AND s.por.id = ?2  order by s.id desc")
    public Page<String> findMawbsByCarrierAndPorAndCount(Long carrierId, Long porId, Pageable page);

    @Query(value = "select s.mawbNo from StockGeneration s where s.stockStatus='Available' AND s.carrier.id = ?1 AND s.por.id = ?2 and s.mawbNo not in (?3)  order by s.id desc")
    public Page<String> findMawbsByCarrierAndPorAndCount(Long carrierId, Long porId, String[] mawbs, Pageable page);

}
