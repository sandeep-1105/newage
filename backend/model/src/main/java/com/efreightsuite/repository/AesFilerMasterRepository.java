package com.efreightsuite.repository;

import com.efreightsuite.model.AesFilerMaster;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface AesFilerMasterRepository extends JpaRepository<AesFilerMaster, Long> {

    @Query("Select af from AesFilerMaster af WHERE af.locationMaster.id=:location and af.companyMaster.id=:company and af.countryMaster.id=:country")
    AesFilerMaster findByLocationAndCountryAndCompany(@Param("location") Long location, @Param("country") Long country, @Param("company") Long company);
}
