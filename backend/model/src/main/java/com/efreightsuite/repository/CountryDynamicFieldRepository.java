package com.efreightsuite.repository;

import com.efreightsuite.model.CountryDynamicField;

import org.springframework.data.jpa.repository.JpaRepository;


public interface CountryDynamicFieldRepository extends JpaRepository<CountryDynamicField, Long> {

    public CountryDynamicField findById(Long id);
    /*
	@Query("select new com.efreightsuite.dto.CountryDynamicFieldDto(cd.dynamicField.fieldName, cd.showField) from CountryDynamicField cd where cd.countryMaster.id = :countryId")
	public List<CountryDynamicFieldDto> findAllByCountryId(@Param("countryId") Long countryId);*/

}
