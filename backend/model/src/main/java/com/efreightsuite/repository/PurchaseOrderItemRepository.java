package com.efreightsuite.repository;

import com.efreightsuite.model.PurchaseOrderItem;

import org.springframework.data.jpa.repository.JpaRepository;

public interface PurchaseOrderItemRepository extends JpaRepository<PurchaseOrderItem, Long> {

    PurchaseOrderItem findById(Long id);

}
