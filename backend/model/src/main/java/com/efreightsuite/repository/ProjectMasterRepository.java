package com.efreightsuite.repository;

import java.util.List;

import com.efreightsuite.model.ProjectMaster;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface ProjectMasterRepository extends JpaRepository<ProjectMaster, Long> {

    @Query("SELECT p FROM ProjectMaster p  where p.status!='Hide' ORDER BY p.projectCode, p.projectName ASC")
    @Override
    List<ProjectMaster> findAll();

}
