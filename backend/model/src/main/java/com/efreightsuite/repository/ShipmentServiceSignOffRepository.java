package com.efreightsuite.repository;

import com.efreightsuite.model.ShipmentServiceSignOff;

import org.springframework.data.jpa.repository.JpaRepository;

public interface ShipmentServiceSignOffRepository extends JpaRepository<ShipmentServiceSignOff, Long> {

}
