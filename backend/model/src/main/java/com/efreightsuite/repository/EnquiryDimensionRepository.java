package com.efreightsuite.repository;

import java.util.List;

import com.efreightsuite.model.EnquiryDimension;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

/**
 * @author Devendrachary M
 */
public interface EnquiryDimensionRepository extends JpaRepository<EnquiryDimension, Long> {

    EnquiryDimension findById(Long id);

    @Override
    List<EnquiryDimension> findAll();

    @Query(value = "select e from EnquiryDimension e where e.id=?1")
    List<EnquiryDimension> getAll(Long id);

}
