package com.efreightsuite.repository;

import java.util.List;

import com.efreightsuite.model.LocationMaster;
import com.efreightsuite.model.LogoMaster;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

/**
 * @author Devendrachary M
 */

public interface LogoMasterRepository extends JpaRepository<LogoMaster, Long> {

    LogoMaster findById(Long id);

    @Override
    List<LogoMaster> findAll();

    LogoMaster findByLocationMaster(LocationMaster locationMaster);

    @Query(value = "select lm from LogoMaster lm where lm.locationMaster.id=?1")
    LogoMaster findByLocationId(Long locationId);


}
