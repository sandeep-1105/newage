package com.efreightsuite.repository;

import com.efreightsuite.model.ConsolEvent;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface ConsolEventRepository extends JpaRepository<ConsolEvent, Long> {

    @Query(value = "select c from ConsolEvent c where c.eventMaster.id=?1 and c.consol.id=?2")
    ConsolEvent findByEventId(Long id, Long id2);


}
