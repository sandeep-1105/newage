package com.efreightsuite.repository;

import com.efreightsuite.model.GeneralLedgerGroup;

import org.springframework.data.jpa.repository.JpaRepository;

public interface GroupMasterRepository extends JpaRepository<GeneralLedgerGroup, Long> {


}
