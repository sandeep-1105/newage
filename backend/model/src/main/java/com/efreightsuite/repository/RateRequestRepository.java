package com.efreightsuite.repository;

import com.efreightsuite.model.EnquiryDetail;
import com.efreightsuite.model.RateRequest;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

public interface RateRequestRepository extends JpaRepository<RateRequest, Long> {


    @Query(value = "select r.enquiryDetail from RateRequest r where r.id=?1")
    EnquiryDetail findEnquiryDetailId(Long rateRequestId);

    @Modifying
    @Query(value = "update RateRequest rr set rr.isMailSent ='Yes'  where rr.id =?1")
    void UpdateMailFalg(Long id);

}
