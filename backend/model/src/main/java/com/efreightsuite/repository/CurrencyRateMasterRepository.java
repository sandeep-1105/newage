package com.efreightsuite.repository;

import com.efreightsuite.model.CurrencyRateMaster;

import org.springframework.data.jpa.repository.JpaRepository;

public interface CurrencyRateMasterRepository extends JpaRepository<CurrencyRateMaster, Long> {


}
