package com.efreightsuite.repository;

import com.efreightsuite.model.EmailRequestAttachment;

import org.springframework.data.jpa.repository.JpaRepository;

public interface EmailRequestAttachmentRepository extends JpaRepository<EmailRequestAttachment, Long> {

    EmailRequestAttachment findById(Long id);


}
