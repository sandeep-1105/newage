package com.efreightsuite.repository;

import java.util.List;

import com.efreightsuite.model.PartyContact;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface PartyContactRepository extends JpaRepository<PartyContact, Long> {


    @Query(value = "select p from PartyContact p where p.partyMaster.id=?1")
    List<PartyContact> getAll(Long id);

}
