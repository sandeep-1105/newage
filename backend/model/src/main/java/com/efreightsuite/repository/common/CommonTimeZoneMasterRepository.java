package com.efreightsuite.repository.common;

import com.efreightsuite.model.common.CommonTimeZoneMaster;

import org.springframework.data.jpa.repository.JpaRepository;

public interface CommonTimeZoneMasterRepository extends JpaRepository<CommonTimeZoneMaster, Long> {
}
