package com.efreightsuite.repository;

import java.util.List;

import com.efreightsuite.model.DocumentMaster;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;


public interface DocumentMasterRepository extends JpaRepository<DocumentMaster, Long> {

    DocumentMaster findById(Long id);


    @Query(value = "SELECT d FROM DocumentMaster d where d.documentName=?1")
    DocumentMaster findByDocumentName(String documentName);

    @Query("SELECT p FROM DocumentMaster p ORDER BY p.documentCode, p.documentName ASC")
    @Override
    List<DocumentMaster> findAll();

    @Query("Select p from DocumentMaster p WHERE (UPPER(p.documentCode) like :keyword% or UPPER(p.documentName) like :keyword%) ORDER BY p.documentCode, p.documentName ASC")
    Page<DocumentMaster> findByKeyword(@Param("keyword") String keyword, Pageable page);

}

