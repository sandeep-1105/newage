package com.efreightsuite.repository;

import com.efreightsuite.model.PricingAttachment;

import org.springframework.data.jpa.repository.JpaRepository;

public interface PricingAirAttachmentRepository extends JpaRepository<PricingAttachment, Long> {

}
