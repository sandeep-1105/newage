package com.efreightsuite.repository;

import com.efreightsuite.model.WinWebConnectResponse;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface WinWebConnectResponseRepository extends JpaRepository<WinWebConnectResponse, Long> {


    @Query("SELECT w FROM  WinWebConnectResponse w where w.consolUid=?1")
    WinWebConnectResponse findByConsolUid(String consolUid);


    @Query("SELECT w FROM  WinWebConnectResponse w where w.awbID=?1")
    WinWebConnectResponse findByAwbId(Long awbID);


}
