package com.efreightsuite.repository;

import com.efreightsuite.model.DefaultChargeMaster;

import org.springframework.data.jpa.repository.JpaRepository;

public interface DefaultChargeMasterRepository extends JpaRepository<DefaultChargeMaster, Long> {


}
