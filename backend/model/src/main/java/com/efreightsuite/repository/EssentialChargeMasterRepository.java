package com.efreightsuite.repository;

import java.util.List;

import com.efreightsuite.model.EssentialChargeMaster;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface EssentialChargeMasterRepository extends JpaRepository<EssentialChargeMaster, Long> {

    @Query(value = "Select em from EssentialChargeMaster em where em.serviceMaster.id=?1")
    List<EssentialChargeMaster> findByService(Long id);
}
