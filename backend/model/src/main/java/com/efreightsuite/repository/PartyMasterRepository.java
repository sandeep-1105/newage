package com.efreightsuite.repository;

import java.util.List;

import com.efreightsuite.model.PartyCompanyAssociate;
import com.efreightsuite.model.PartyMaster;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface PartyMasterRepository extends JpaRepository<PartyMaster, Long> {


    @Query(value = "select c from PartyCompanyAssociate c where c.partyMaster.id=?1 order by c.companyMaster.companyName asc")
    List<PartyCompanyAssociate> getPartyCompanyAssociate(Long partyId);

    PartyMaster getByPartyCode(String partyCode);

    PartyMaster findByPartyName(String string);

    PartyMaster findById(Long id);

}
