package com.efreightsuite.repository;

import com.efreightsuite.model.EmailRequest;

import org.springframework.data.jpa.repository.JpaRepository;

public interface EmailRequestRepository extends JpaRepository<EmailRequest, Long> {

    EmailRequest findById(Long id);

	/*@Query(value="select e.versionLock from EmailRequest e where e.id=:1")
	long findByIdCurrentVersion(Long id);*/


}
