package com.efreightsuite.repository;

import java.util.List;

import com.efreightsuite.model.ShipmentCharge;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface ShipmentChargeRepository extends JpaRepository<ShipmentCharge, String> {

    //ReportsFormData findById(Long id);
    @Query(value = "select sc from ShipmentCharge sc where sc.serviceUid=?1")
    List<ShipmentCharge> findByCharge(String id);

}
