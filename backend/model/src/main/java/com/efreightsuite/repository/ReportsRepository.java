package com.efreightsuite.repository;

import java.util.List;

import com.efreightsuite.model.ReportsFormData;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface ReportsRepository extends JpaRepository<ReportsFormData, Long> {

    //ReportsFormData findById(Long id);

    @Query(value = "select rf from ReportsFormData rf where rf.report.id=?1")
    List<ReportsFormData> findById(Long id);

}
