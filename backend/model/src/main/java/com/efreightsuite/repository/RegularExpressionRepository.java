package com.efreightsuite.repository;

import com.efreightsuite.model.RegularExpression;

import org.springframework.data.jpa.repository.JpaRepository;

public interface RegularExpressionRepository extends JpaRepository<RegularExpression, Long> {

    RegularExpression findByRegExpName(String regExpName);
}
