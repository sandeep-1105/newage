package com.efreightsuite.repository;


import javax.transaction.Transactional;

import com.efreightsuite.enumeration.ImportExport;
import com.efreightsuite.model.Consol;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface ConsolRepository extends JpaRepository<Consol, Long> {

    Consol findById(Long id);

    Consol findByConsolUid(String colsolUid);

    @Modifying
    @Transactional
    @Query(value = "UPDATE ServiceMailTrigger s SET s.isCargoLoadedMailTrigger='Yes' WHERE s.id=?1")
    int updateServiceMailTrigger(Long id);

    @Query(value = "select c.id from Consol c where c.consolUid=?1")
    Long getConsolIdBasedOnUid(String consolUid);

    @Query(value = "select c from Consol c where c.consolDocument.mawbNo = :mawb AND c.serviceMaster.importExport = :importExport")
    Consol getConsolByMawbAndService(@Param("mawb") String mawbNo, @Param("importExport") ImportExport importExport);

    @Query(value = "select c from Consol c where c.winAwbID = :winAwbID")
    Consol findByAwbId(@Param("winAwbID") Long winAwbID);


}
