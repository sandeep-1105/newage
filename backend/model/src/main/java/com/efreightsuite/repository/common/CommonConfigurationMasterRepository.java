package com.efreightsuite.repository.common;

import com.efreightsuite.model.common.CommonConfigurationMaster;

import org.springframework.data.jpa.repository.JpaRepository;

public interface CommonConfigurationMasterRepository extends JpaRepository<CommonConfigurationMaster, Long> {
    CommonConfigurationMaster findByCode(String code);
}
