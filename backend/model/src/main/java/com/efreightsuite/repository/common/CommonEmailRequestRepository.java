package com.efreightsuite.repository.common;

import com.efreightsuite.model.common.CommonEmailRequest;

import org.springframework.data.jpa.repository.JpaRepository;

public interface CommonEmailRequestRepository extends JpaRepository<CommonEmailRequest, Long> {

    CommonEmailRequest findById(Long id);
}
