package com.efreightsuite.repository;

import java.util.List;

import com.efreightsuite.model.ValueAddedServices;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface ValueAddedServicesRepository extends JpaRepository<ValueAddedServices, Long> {

    @Query(value = "select e from ValueAddedServices e order by e.valueAddedSevicesName asc")
    List<ValueAddedServices> getAll();

}
