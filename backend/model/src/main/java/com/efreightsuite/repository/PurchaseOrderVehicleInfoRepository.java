package com.efreightsuite.repository;

import com.efreightsuite.model.PurchaseOrderVehicleInfo;

import org.springframework.data.jpa.repository.JpaRepository;

public interface PurchaseOrderVehicleInfoRepository extends JpaRepository<PurchaseOrderVehicleInfo, Long> {

    PurchaseOrderVehicleInfo findById(Long id);

}
