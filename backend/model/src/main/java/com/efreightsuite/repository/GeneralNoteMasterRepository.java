package com.efreightsuite.repository;

import java.util.List;

import com.efreightsuite.enumeration.GeneralNoteCategory;
import com.efreightsuite.enumeration.GeneralNoteSubCategory;
import com.efreightsuite.model.GeneralNoteMaster;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface GeneralNoteMasterRepository extends JpaRepository<GeneralNoteMaster, Long> {


    @Query("Select a from GeneralNoteMaster a where a.category=?1 and a.subCategory=?2  and a.serviceMaster is NULL and a.locationMaster.id=?3 ")
    List<GeneralNoteMaster> getGeneralNote(GeneralNoteCategory category, GeneralNoteSubCategory subCategory, Long locationId);

    @Query("Select a from GeneralNoteMaster a where a.category=?1 and a.subCategory=?2 and a.serviceMaster.id=?3 and a.locationMaster.id=?4")
    List<GeneralNoteMaster> getGeneralNote(GeneralNoteCategory category, GeneralNoteSubCategory subCategory, Long serviceMasterId, Long locationId);


}
