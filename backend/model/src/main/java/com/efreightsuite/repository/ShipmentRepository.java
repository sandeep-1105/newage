package com.efreightsuite.repository;

import com.efreightsuite.model.DocumentDetail;
import com.efreightsuite.model.Shipment;
import com.efreightsuite.model.ShipmentServiceDetail;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface ShipmentRepository extends JpaRepository<Shipment, Long> {

    Shipment findByShipmentUid(String shipmentUid);

    Shipment findById(Long id);

    @Query(value = "select ssd from ShipmentServiceDetail ssd where ssd.id=?1")
    ShipmentServiceDetail getByService(Long serviceId);

    @Query(value = "select q from DocumentDetail q where q.id=?1")
    DocumentDetail getDocument(Long id);

    @Query(value = "select s.id from Shipment s where s.shipmentUid=?1")
    Long getShipmentIdBasedOnUid(String shipmentUid);

    @Query(value = "select ssd from ShipmentServiceDetail ssd where ssd.serviceUid=?1")
    ShipmentServiceDetail getByService(String serviceUid);

    @Query(value = "select ssd from ShipmentServiceDetail ssd where ssd.shipmentUid=?1")
    ShipmentServiceDetail getByShipmentUid(String shipmentUid);

	

	/*@Modifying
	@Query(value="UPDATE ShipmentServiceDetail ssd SET ssd.mawbNo=NULL WHERE ssd.mawbNo=?1")
	int updateStockInShipment(String mawbNo);

	@Modifying
	@Query(value="UPDATE DocumentDetail ssd SET ssd.mawbNo=NULL WHERE ssd.mawbNo=?1")
	int updateStockInDocument(String mawbNo);*/


}
