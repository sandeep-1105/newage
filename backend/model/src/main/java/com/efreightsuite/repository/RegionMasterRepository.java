package com.efreightsuite.repository;

import com.efreightsuite.model.RegionMaster;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface RegionMasterRepository extends JpaRepository<RegionMaster, Long> {

    @Query("FROM RegionMaster rm WHERE UPPER(rm.regionName) = :regionName")
    RegionMaster findByRegionName(@Param("regionName") String regionName);

    RegionMaster findByRegionCode(String regionCode);

}
