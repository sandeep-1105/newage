package com.efreightsuite.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface CfsAttachmentRepository extends JpaRepository<com.efreightsuite.model.CfsAttachment, Long> {

    @Query(value = "SELECT ea.file FROM CfsAttachment ea WHERE ea.id = ?1")
    byte[] getByteArray(Long id);

}
