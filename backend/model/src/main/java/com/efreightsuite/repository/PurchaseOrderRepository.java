package com.efreightsuite.repository;

import com.efreightsuite.model.PurchaseOrder;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface PurchaseOrderRepository extends JpaRepository<PurchaseOrder, Long> {

    PurchaseOrder findById(Long id);

    @Query("select po from PurchaseOrder po where id = :poId")
    PurchaseOrder findByPOId(@Param("poId") Long poId);

    @Query("select po from PurchaseOrder po where poNo = :poNo")
    PurchaseOrder findByPONo(@Param("poNo") String poNo);


}
