package com.efreightsuite.repository;

import java.util.List;

import com.efreightsuite.model.PeriodMaster;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface PeriodMasterRepository extends JpaRepository<PeriodMaster, Long> {

    @Query("SELECT p FROM PeriodMaster p  where p.status!='Hide' ORDER BY p.frequencyCode, p.frequencyName ASC")
    @Override
    List<PeriodMaster> findAll();

    PeriodMaster findByFrequencyCode(String frequencyCode);

}
