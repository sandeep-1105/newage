package com.efreightsuite.repository;

import com.efreightsuite.model.PurchaseOrderRemarkFollowUp;

import org.springframework.data.jpa.repository.JpaRepository;

public interface PurchaseOrderRemarkFollowUpRepository extends JpaRepository<PurchaseOrderRemarkFollowUp, Long> {

    PurchaseOrderRemarkFollowUp findById(Long id);

}
