package com.efreightsuite.repository;

import com.efreightsuite.model.SoftwareMaster;

import org.springframework.data.jpa.repository.JpaRepository;

public interface SoftwareMasterRepository extends JpaRepository<SoftwareMaster, Long> {

}
