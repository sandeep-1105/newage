package com.efreightsuite.repository;

import com.efreightsuite.model.PurchaseOrderAttachment;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface PurchaseOrderAttachmentRepository extends JpaRepository<PurchaseOrderAttachment, Long> {
    PurchaseOrderAttachment findById(Long id);

    @Query(value = "SELECT ea.protectedFile FROM PurchaseOrderAttachment ea WHERE ea.id = ?1")
    byte[] getByteArrayProtected(Long id);

    @Query(value = "SELECT ea.unprotectedFile FROM PurchaseOrderAttachment ea WHERE ea.id = ?1")
    byte[] getByteArrayUnProtected(Long id);

}
