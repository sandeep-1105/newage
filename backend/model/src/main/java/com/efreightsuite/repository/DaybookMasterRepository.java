package com.efreightsuite.repository;

import java.util.List;

import com.efreightsuite.model.DaybookMaster;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface DaybookMasterRepository extends JpaRepository<DaybookMaster, Long> {

    DaybookMaster findById(Long id);

    DaybookMaster findByDaybookCode(String daybookCode);

    @Query("SELECT p FROM DaybookMaster p ORDER BY p.daybookCode, p.daybookName ASC")
    @Override
    List<DaybookMaster> findAll();

    @Query("Select p from DaybookMaster p WHERE (UPPER(p.daybookCode) like :keyword% or UPPER(p.daybookName) like :keyword%) ORDER BY p.daybookCode, p.daybookName ASC")
    Page<DaybookMaster> findByKeyword(@Param("keyword") String keyword, Pageable page);


    DaybookMaster findByDaybookName(String daybookName);
}
