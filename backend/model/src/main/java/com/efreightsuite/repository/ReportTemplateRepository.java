package com.efreightsuite.repository;

import com.efreightsuite.model.ReportTemplate;

import org.springframework.data.jpa.repository.JpaRepository;

public interface ReportTemplateRepository extends JpaRepository<ReportTemplate, Long> {


    ReportTemplate findById(Long id);

}
