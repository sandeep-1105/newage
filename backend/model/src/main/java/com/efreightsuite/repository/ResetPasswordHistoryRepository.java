package com.efreightsuite.repository;

import java.util.List;

import com.efreightsuite.model.ResetPasswordHistory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface ResetPasswordHistoryRepository extends JpaRepository<ResetPasswordHistory, Long> {

    ResetPasswordHistory findById(Long id);

    @Override
    @Query("Select r from ResetPasswordHistory r ORDER BY r.id ASC")
    List<ResetPasswordHistory> findAll();

    @Query("Select r from ResetPasswordHistory r where (r.id like :keyword% or r.status like :keyword%) ORDER BY r.id ASC")
    Page<ResetPasswordHistory> findByKeyword(@Param("keyword") String keyword, Pageable page);


}
