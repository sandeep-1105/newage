package com.efreightsuite.repository;

import com.efreightsuite.model.CarrierMaster;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface CarrierMasterRepository extends JpaRepository<CarrierMaster, Long> {

    CarrierMaster findById(Long id);

    CarrierMaster findByCarrierName(String carrierName);

    CarrierMaster findByCarrierCode(String carrierCode);

    @Query(value = "select c from CarrierMaster c where c.carrierNo=?1 AND c.status!='Hide' AND c.status!='Block'")
    CarrierMaster getByCarrierNo(String carrierNo);

}
