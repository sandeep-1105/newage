package com.efreightsuite.repository.common;

import com.efreightsuite.enumeration.LovStatus;
import com.efreightsuite.model.common.CommonStateMaster;
import org.hibernate.validator.constraints.NotBlank;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface CommonStateMasterRepository extends JpaRepository<CommonStateMaster, Long> {
    @Query("FROM CommonStateMaster sm WHERE UPPER(sm.stateName) = :stateName")
    CommonStateMaster findByStateName(@Param("stateName") String stateName);

    @Query(value = "select s from CommonStateMaster s where s.stateCode=?1 and s.countryCode=?2")
    CommonStateMaster getState(String stateCode, String countryCode);

    @Query(value = "select s from CommonStateMaster s where s.stateCode=?1 and s.countryMaster.id=?2")
    CommonStateMaster getState(String stateCode, Long id);

    Page<CommonStateMaster> findAllByStatusNotAndStateNameStartingWithOrStateCodeStartingWithAllIgnoreCaseOrderByStateName(LovStatus status, String state, String stateCodeCode, Pageable pageable);

    default Page<CommonStateMaster> searchStates(@NotBlank String query, PageRequest request) {
        return findAllByStatusNotAndStateNameStartingWithOrStateCodeStartingWithAllIgnoreCaseOrderByStateName(LovStatus.Hide, query, query, request);
    }
}
