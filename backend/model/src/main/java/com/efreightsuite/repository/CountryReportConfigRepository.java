package com.efreightsuite.repository;


import com.efreightsuite.model.CountryReportConfigMaster;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface CountryReportConfigRepository extends JpaRepository<CountryReportConfigMaster, Long> {

    @Query("select crc from CountryReportConfigMaster crc where crc.reportName = :reportName AND  crc.countryMaster.id=:id")
    CountryReportConfigMaster findbyReportName(@Param("reportName") String reportName, @Param("id") Long id);

}
