package com.efreightsuite.repository;

import java.util.List;

import com.efreightsuite.model.PartyGroupMaster;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface PartyGroupMasterRepository extends JpaRepository<PartyGroupMaster, Long> {

    PartyGroupMaster findById(Long id);


    @Query("Select pgm from PartyGroupMaster pgm ORDER BY pgm.partyGroupCode, pgm.partyGroupName  ASC")
    @Override
    List<PartyGroupMaster> findAll();

}
