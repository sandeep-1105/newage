package com.efreightsuite.repository;

import com.efreightsuite.model.GeneralLedgerSubGroup;

import org.springframework.data.jpa.repository.JpaRepository;

public interface SubGroupMasterRepository extends JpaRepository<GeneralLedgerSubGroup, Long> {


}
