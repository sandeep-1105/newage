package com.efreightsuite.repository;

import java.util.List;

import com.efreightsuite.dto.CountryDynamicFieldDto;
import com.efreightsuite.model.DynamicFields;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;


public interface DynamicFieldRepository extends JpaRepository<DynamicFields, Long> {

    public DynamicFields findById(Long id);

    public DynamicFields findByFieldId(String filedId);

    @Query("select new com.efreightsuite.dto.CountryDynamicFieldDto(cd.fieldId, 'true') from DynamicFields cd join cd.countryMasterList cm  where cm.id in (:countryIds)")
    public List<CountryDynamicFieldDto> findAllByCountryId(@Param("countryIds") Long[] countryIds);

}
