package com.efreightsuite.repository;

import com.efreightsuite.enumeration.GlHead;
import com.efreightsuite.model.GeneralLedger;
import com.efreightsuite.model.GeneralLedgerAccount;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface GeneralLedgerRepository extends JpaRepository<GeneralLedger, Long> {

    @Query(value = "select g from GeneralLedgerAccount g where UPPER(g.glAccountName)=?1")
    public GeneralLedgerAccount findByAccountNameInUpperCase(String glAccountName);

    @Query(value = "select g from GeneralLedgerAccount g where g.glAccountCode=?1")
    public GeneralLedgerAccount findByGlAccountCode(String glAccountCode);

    @Query(value = "select g from GeneralLedgerAccount g where g.id=?1")
    public GeneralLedgerAccount findByGlAccountId(Long id);

    @Query(value = "select g from GeneralLedger g where g.companyMaster.id=?1 and g.glHead=?2 and g.glGroup.id=?3 and g.glSubGroup.id=?4")
    public GeneralLedger getGeneralLedger(Long companyId, GlHead glHead, Long glGroupId, Long glSubGroupId);
}
