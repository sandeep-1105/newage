package com.efreightsuite.repository;

import java.util.List;

import com.efreightsuite.model.CityMaster;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface CityMasterRepository extends JpaRepository<CityMaster, Long> {

    @Query(value = "select c from CityMaster c where c.status!='Hide' order by c.cityName asc")
    List<CityMaster> getAllByStatus();

    CityMaster findByCityCodeAndCountryCodeAndStateCode(String cityCode, String countryCode, String stateCode);

    CityMaster findByCityCode(String cityCode);

    @Query("FROM CityMaster cm WHERE UPPER(cm.cityName) = :cityName")
    CityMaster findByCityName(@Param("cityName") String cityName);
}
