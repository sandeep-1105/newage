package com.efreightsuite.repository;


import java.util.List;

import com.efreightsuite.enumeration.ReferenceTypeKey;
import com.efreightsuite.model.ReferenceTypeMaster;
import com.efreightsuite.model.ShipmentServiceReference;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface ReferenceTypeMasterRepository extends JpaRepository<ReferenceTypeMaster, Long> {

    @Query(value = "select ssr from ShipmentServiceReference ssr where ssr.shipmentServiceDetail.id=:id ")
    ShipmentServiceReference findByReferenceNumber(@Param("id") Long id);

    @Query(value = "SELECT rtm FROM ReferenceTypeMaster rtm WHERE rtm.referenceTypeKey=:key AND rtm.isMandatory = 'Yes'")
    List<ReferenceTypeMaster> findAllByReferenceTypeKey(@Param("key") ReferenceTypeKey key);

    ReferenceTypeMaster findByReferenceTypeCode(String referenceTypeCode);


}
