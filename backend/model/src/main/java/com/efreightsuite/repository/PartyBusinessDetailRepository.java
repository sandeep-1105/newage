package com.efreightsuite.repository;

import java.util.List;

import com.efreightsuite.model.PartyBusinessDetail;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface PartyBusinessDetailRepository extends JpaRepository<PartyBusinessDetail, Long> {


    @Override
    List<PartyBusinessDetail> findAll();


    @Query(value = "select p from PartyBusinessDetail p where p.partyMaster.id=?1")
    List<PartyBusinessDetail> getAll(Long id);

}
