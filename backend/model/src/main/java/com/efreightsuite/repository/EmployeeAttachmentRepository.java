package com.efreightsuite.repository;

import com.efreightsuite.model.EmployeeAttachment;

import org.springframework.data.jpa.repository.JpaRepository;

public interface EmployeeAttachmentRepository extends JpaRepository<EmployeeAttachment, Long> {

}
