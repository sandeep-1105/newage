package com.efreightsuite.repository;

import com.efreightsuite.model.AesLicense;

import org.springframework.data.jpa.repository.JpaRepository;

public interface AesLicenseRepository extends JpaRepository<AesLicense, Long> {

}
