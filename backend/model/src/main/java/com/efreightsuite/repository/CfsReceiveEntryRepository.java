package com.efreightsuite.repository;

import java.util.List;

import com.efreightsuite.model.CfsReceiveEntry;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface CfsReceiveEntryRepository extends JpaRepository<CfsReceiveEntry, Long> {

    CfsReceiveEntry findById(Long id);

    @Query(value = "select cf from CfsReceiveEntry cf where cf.shipmentServiceDetail.serviceUid=:serviceUid")
    List<CfsReceiveEntry> findByShipmentServiceUID(@Param("serviceUid") String shipmentUid);


    @Query(value = "select cf from CfsReceiveEntry cf where cf.shipmentServiceDetail.serviceUid=:serviceUid")
    List<CfsReceiveEntry> findByShipmentService(@Param("serviceUid") String shipmentUid);

}

