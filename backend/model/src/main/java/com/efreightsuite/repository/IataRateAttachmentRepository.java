package com.efreightsuite.repository;

import com.efreightsuite.model.IataRateAttachment;

import org.springframework.data.jpa.repository.JpaRepository;

public interface IataRateAttachmentRepository extends JpaRepository<IataRateAttachment, Long> {

}


