package com.efreightsuite.repository;

import java.util.List;

import com.efreightsuite.model.UnitMaster;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface UnitMasterRepository extends JpaRepository<UnitMaster, Long> {

    @Override
    @Query("SELECT u from UnitMaster u ORDER BY u.unitName ASC")
    List<UnitMaster> findAll();

    UnitMaster getByUnitCode(String unitCode);

    UnitMaster findByUnitName(String unitName);

    UnitMaster findByUnitCode(String unitCode);

}
