package com.efreightsuite.repository;

import com.efreightsuite.model.BuyerConsolidationMaster;

import org.springframework.data.jpa.repository.JpaRepository;

public interface BuyerConsolidationRepository extends JpaRepository<BuyerConsolidationMaster, Long> {
}
