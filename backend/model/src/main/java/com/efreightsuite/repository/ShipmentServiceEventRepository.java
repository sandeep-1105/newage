package com.efreightsuite.repository;

import com.efreightsuite.model.ShipmentServiceEvent;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface ShipmentServiceEventRepository extends JpaRepository<ShipmentServiceEvent, Long> {

    @Query(value = "select s from ShipmentServiceEvent s where s.eventMaster.id=?1 and s.shipmentServiceDetail.id=?2")
    ShipmentServiceEvent findByEventId(Long id, Long id2);

}
