package com.efreightsuite.repository;

import com.efreightsuite.model.SubJobSequence;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface SubJobSequenceRepository extends JpaRepository<SubJobSequence, Long> {

    @Query(value = "SELECT s FROM SubJobSequence s WHERE s.consolUid=?1")
    SubJobSequence getSubJobSequence(String colsolUid);

}
