package com.efreightsuite.repository;

import com.efreightsuite.model.RecordAccessLevel;

import org.springframework.data.jpa.repository.JpaRepository;

public interface RecordAccessLevelRepository extends JpaRepository<RecordAccessLevel, Long> {


    RecordAccessLevel findById(Long id);

}
