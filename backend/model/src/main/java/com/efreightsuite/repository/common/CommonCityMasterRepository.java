package com.efreightsuite.repository.common;

import com.efreightsuite.enumeration.LovStatus;
import com.efreightsuite.model.common.CommonCityMaster;
import org.hibernate.validator.constraints.NotBlank;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface CommonCityMasterRepository extends JpaRepository<CommonCityMaster, Long> {

    @Query("FROM CommonCityMaster cm WHERE UPPER(cm.cityName) = :cityName")
    CommonCityMaster findByCityName(@Param("cityName") String cityName);

    CommonCityMaster findByCityCodeAndCountryCodeAndStateCode(String cityCode, String countryCode, String stateCode);

    Page<CommonCityMaster> findAllByStatusNotAndCityNameStartingWithOrCityCodeStartingWithAllIgnoreCaseOrderByCityName(LovStatus status, String city, String cityCode, Pageable pageable);

    default Page<CommonCityMaster> searchCities(@NotBlank String query, PageRequest request) {
        return findAllByStatusNotAndCityNameStartingWithOrCityCodeStartingWithAllIgnoreCaseOrderByCityName(LovStatus.Hide, query, query, request);
    }
}
