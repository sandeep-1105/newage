package com.efreightsuite.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.UniqueConstraint;
import javax.persistence.Version;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;


@Entity
@Getter
@Setter
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Table(name = "efs_dynamic_fields",
        uniqueConstraints = {
                @UniqueConstraint(columnNames = {"field_name"}, name = "UK_DYNAMIC_FIELD_NAME"),
                @UniqueConstraint(columnNames = {"field_id"}, name = "UK_DYNAMIC_FIELD_ID")})
public class DynamicFields implements Serializable {
    private static final long serialVersionUID = 1L;

    /*Auto Generation Id*/
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    @Column(name = "field_name", nullable = false)
    String fieldName;

    @Column(name = "field_id", nullable = false)
    String fieldId;

    String screen;

    String screentab;

    String remark;

    @ManyToMany
    @JoinTable(name = "efs_dynamic_field_countries",
            joinColumns = {@JoinColumn(name = "field_id", foreignKey = @ForeignKey(name = "FK_DFC_FIELD_ID"))},
            inverseJoinColumns = {@JoinColumn(name = "country_id", foreignKey = @ForeignKey(name = "FK_DFC_COUNTRY_ID"))},
            uniqueConstraints = {@UniqueConstraint(columnNames = {"field_id", "country_id"}, name = "UK_DFC_FIELD_COUNTRY")})
    List<CountryMaster> countryMasterList;

    /*Create/Last User and Data Info*/
    @Embedded
    SystemTrack systemTrack;

    @Version
    @Column(name = "version_lock")
    long versionLock;

}
