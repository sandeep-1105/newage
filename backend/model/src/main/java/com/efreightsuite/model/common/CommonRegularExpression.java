package com.efreightsuite.model.common;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.UniqueConstraint;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;


@Entity
@Getter
@Setter
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Table(name = "efs_common_reg_exp", uniqueConstraints = {
        @UniqueConstraint(columnNames = {"reg_exp_name"}, name = "UK_COM_REGEXP_NAME")}

)
public class CommonRegularExpression implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    @Column(name = "reg_exp_name", length = 50, nullable = false)
    String regExpName;

    @Column(name = "java_reg_exp", length = 255, nullable = false)
    String javaRegExp;

    @Column(name = "js_reg_exp", length = 255, nullable = false)
    String jsRegExp;

}
