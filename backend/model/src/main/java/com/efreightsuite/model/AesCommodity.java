package com.efreightsuite.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Version;

import com.efreightsuite.enumeration.AesAction;
import com.efreightsuite.enumeration.OriginOfGoods;
import com.efreightsuite.enumeration.YesNo;
import com.efreightsuite.util.JsonYesNoDeserializer;
import com.efreightsuite.util.JsonYesNoSerializer;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Getter;
import lombok.Setter;


@Entity
@Getter
@Setter
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Table(name = "efs_aes_commodity")
public class AesCommodity implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    @Version
    @Column(name = "version_lock")
    long versionLock;

    /* Create/Last User and Data Info */
    @Embedded
    SystemTrack systemTrack;

    /* Origin State */
    @ManyToOne(fetch = FetchType.LAZY)
    @JsonBackReference
    @JoinColumn(name = "aes_id", foreignKey = @ForeignKey(name = "FK_AES_COMM_ID"), nullable = false)
    AesFile aesfile;


    /* Action */
    @Column(name = "action", length = 30)
    @Enumerated(EnumType.STRING)
    AesAction aesAction;

    /* Origin of Goods */
    @Column(name = "origin_goods", length = 30)
    @Enumerated(EnumType.STRING)
    OriginOfGoods originOfGoods;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "license_id", foreignKey = @ForeignKey(name = "FK_AES_LICENSE_ID"), nullable = false)
    AesLicense aesLicense;

    @Column(name = "license_code", length = 255)
    String aesLicenseCode;

    /* Commodity */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "commodity_id", foreignKey = @ForeignKey(name = "FK_AES_COMMODITY_ID"), nullable = false)
    CommodityMaster commodityMaster;

    @Column(name = "commodity_code", length = 255)
    String commodityCode;


    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "schedule_id", foreignKey = @ForeignKey(name = "FK_AES_SCHEDULE_ID"), nullable = false)
    AesScheduleB aesSchedule;

    @Column(name = "schedule_code", length = 255)
    String scheduleCode;


    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "first_unit_id", foreignKey = @ForeignKey(name = "FK_AES_FUNIT_ID"), nullable = false)
    UnitMaster firstUnit;

    @Column(name = "first_unit_code", length = 100)
    String firstUnitCode;

    @Column(name = "first_no_of_pieces")
    Long firstNoOfPieces;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "second_unit_id", foreignKey = @ForeignKey(name = "FK_AES_SUNIT_ID"), nullable = false)
    UnitMaster secondUnit;

    @Column(name = "second_unit_code", length = 100)
    String secondUnitCode;

    @Column(name = "second_no_of_pieces")
    Long secondNoOfPieces;

    @Column(name = "value_in_usd", nullable = false)
    Double valueInUsd;

    @Column(name = "gross_weight_in_kg", nullable = false)
    Double grossWeightInKg;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "aes_export_id", foreignKey = @ForeignKey(name = "FK_AES_EXPORT_ID"), nullable = false)
    ExportCodeMaster aesExport;

    @Column(name = "aes_export_code", length = 100)
    String aesExportCode;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "aes_itar_excemption_id", foreignKey = @ForeignKey(name = "FK_AES_ITAR_ID"))
    ItarExcemptionMaster itarExcemption;

    @Column(name = "aes_itar_excemption_code", length = 100)
    String itarExcemptionCode;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "category_id", foreignKey = @ForeignKey(name = "FK_AES_CATEGORYID"))
    CategoryMaster usml;

    @Column(name = "category_code", length = 100)
    String usmlCode;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "unit_code_id", foreignKey = @ForeignKey(name = "FK_AES_UNITCODEID"))
    UnitMaster dtdc;

    @Column(name = "unit_code", length = 100)
    String dtdcCode;

    /* dtdcNo */
    @Column(name = "dtdc_no", length = 10)
    String dtdcNo;

    @Column(name = "ddtc_eligibility_indicator")
    String dtdcEligibilityIndicator;

    /*sme_indicator*/
    @Column(name = "sme_indicator")
    @Enumerated(EnumType.STRING)
    @JsonSerialize(using = JsonYesNoSerializer.class)
    @JsonDeserialize(using = JsonYesNoDeserializer.class)
    YesNo smeIndicator;

    /*party_certification*/
    @Column(name = "eligible_party_certification")
    @Enumerated(EnumType.STRING)
    @JsonSerialize(using = JsonYesNoSerializer.class)
    @JsonDeserialize(using = JsonYesNoDeserializer.class)
    YesNo eligiblePartyCertification;

    /*isUsedVehicle*/
    @Column(name = "is_used_vehicle")
    @Enumerated(EnumType.STRING)
    @JsonSerialize(using = JsonYesNoSerializer.class)
    @JsonDeserialize(using = JsonYesNoDeserializer.class)
    YesNo isUsedVehicle;

    @Column(name = "dtdc_quantity")
    Long dtdcQuantity;


    @Column(name = "eccn", length = 50)
    String eccn;

    @Column(name = "export_license_no", length = 50)
    String exportLicenseNo;

    @Column(name = "license_value", length = 50)
    String licenseValue;

    @OneToMany(mappedBy = "aesCommodit", cascade = CascadeType.ALL, orphanRemoval = true)
    @JsonManagedReference
    List<DtdcVehicle> dtdcVehicleList = new ArrayList<>();


    @Column(name = "vehicle_product_id")
    String vehicleProductId;

    @Column(name = "vehicle_id_type")
    String vehicleIdType;


    @Column(name = "vehicle_title_state")
    String vehicleTitleState;

    @Column(name = "vehicle_title")
    String vehicleTitle;

}
