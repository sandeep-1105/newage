package com.efreightsuite.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.UniqueConstraint;
import javax.persistence.Version;

import com.efreightsuite.enumeration.LovStatus;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;


@Entity
@Getter
@Setter
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Table(name = "efs_region_master",
        uniqueConstraints = {@UniqueConstraint(columnNames = {"region_code"}, name = "UK_REGION_REGIONCODE"),
                @UniqueConstraint(columnNames = {"region_name"}, name = "UK_REGION_REGIONNAME")}

)
public class RegionMaster implements Serializable {


    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /*Auto Generation Id*/
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    @Version
    @Column(name = "version_lock")
    long versionLock;

    /*Create/Last User and Data Info*/
    @Embedded
    SystemTrack systemTrack;

    /*Region Code*/
    @Column(name = "region_code", nullable = false, length = 10)
    String regionCode;

    /*Region Name*/
    @Column(name = "region_name", nullable = false, length = 100)
    String regionName;

    /*Lov Status*/
    @Column(name = "status", nullable = false, length = 10)
    @Enumerated(EnumType.STRING)
    LovStatus status;

    public RegionMaster() {
        // TODO Auto-generated constructor stub
    }

    public RegionMaster(Long id) {
        this.id = id;
    }

    public RegionMaster(Long id, String regionName) {
        this.id = id;
        this.regionName = regionName;
    }
}
