package com.efreightsuite.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "efs_win_status_response")
public class WinStatusResponse implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /* Auto Generation Id */
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    @Column(name = "created_date")
    String createdDate;

    @Column(name = "updated_date")
    String updatedDate;

    @Column(name = "consol_uid")
    String consolUid;

    @Column(name = "awb_id")
    Long awbId;

    @Column(name = "awb_number")
    String awbNumber;

    @Column(name = "status")
    String status;

    @Column(name = "origin_name")
    String originName;

    @Column(name = "destination_name")
    String destination;

    @OneToMany(mappedBy = "winStatusResponse", cascade = CascadeType.ALL, orphanRemoval = true)
    @JsonManagedReference
    List<HawbWinResponse> hawbs = new ArrayList<>();
}
