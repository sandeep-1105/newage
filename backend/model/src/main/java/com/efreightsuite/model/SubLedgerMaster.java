package com.efreightsuite.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.UniqueConstraint;
import javax.persistence.Version;

import com.efreightsuite.enumeration.LovStatus;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;


@Entity
@Getter
@Setter
@Table(name = "efs_sub_ledger_master", uniqueConstraints = {
        @UniqueConstraint(columnNames = {"ledger_code"}, name = "UK_LEDGER_ACCCODE"),
        @UniqueConstraint(columnNames = {"ledger_name"}, name = "UK_LEDGER_ACCNAME")})
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class SubLedgerMaster implements Serializable {


    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /* Auto Generation Id */
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    @Version
    @Column(name = "version_lock")
    long versionLock;

    /* Create/Last User and Data Info */
    @Embedded
    SystemTrack systemTrack;

    /* Currency Code */
    @Column(name = "ledger_code", nullable = false, length = 20)
    String ledgerCode;

    /* Currency Name */
    @Column(name = "ledger_name", nullable = false, length = 20)
    String ledgerName;

    /* Lov Status */
    @Column(name = "status", nullable = false, length = 10)
    @Enumerated(EnumType.STRING)
    LovStatus status;


}
