package com.efreightsuite.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.UniqueConstraint;
import javax.persistence.Version;

import com.efreightsuite.enumeration.ImportExport;
import com.efreightsuite.enumeration.Service;
import lombok.Getter;
import lombok.Setter;


@Entity
@Getter
@Setter
@Table(name = "efs_enquiry_service_mapping", uniqueConstraints = {
        @UniqueConstraint(columnNames = {"service", "import_export"}, name = "UK_ENQUIRY_SERVICE_MAPPING"),
        @UniqueConstraint(columnNames = {"service_master_id"}, name = "UK_ENQSERVICE_SERVICEID")
})
public class EnquiryServiceMapping implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    @Version
    @Column(name = "version_lock")
    long versionLock;

    @Embedded
    SystemTrack systemTrack;

    @Column(name = "service", nullable = false, length = 20)
    @Enumerated(EnumType.STRING)
    Service service;

    @Column(name = "import_export", nullable = false, length = 20)
    @Enumerated(EnumType.STRING)
    ImportExport importExport;

    @ManyToOne
    @JoinColumn(name = "service_master_id", foreignKey = @ForeignKey(name = "FK_ENQSERVICE_SERVICEID"), nullable = false)
    ServiceMaster serviceMaster;


}


