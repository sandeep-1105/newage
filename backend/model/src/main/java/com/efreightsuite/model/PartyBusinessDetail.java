package com.efreightsuite.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Version;

import com.efreightsuite.enumeration.CurrentPotential;
import com.efreightsuite.enumeration.PartyBusinessStatus;
import com.efreightsuite.enumeration.Reason;
import com.efreightsuite.util.JsonDateDeserializer;
import com.efreightsuite.util.JsonDateSerializer;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Getter;
import lombok.Setter;


@Entity
@Table(name = "efs_party_bussiness_detail")
@Getter
@Setter
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class PartyBusinessDetail implements Serializable {


    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 1L;

    /*Auto Generation Id*/
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    @Version
    @Column(name = "version_lock")
    long versionLock;

    /*Create/Last User and Data Info*/
    @Embedded
    SystemTrack systemTrack;

    /*Party id*/
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "party_id", foreignKey = @ForeignKey(name = "FK_PARTYBUSSINESS_PARTYID"))
    @JsonBackReference
    PartyMaster partyMaster;

    /*Created Location Id*/
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "created_location_id", foreignKey = @ForeignKey(name = "FK_PARTYBUSS_CRELOCID"))
    LocationMaster createdLocation;

    @Column(name = "created_location_code", length = 100)
    String createdLocationCode;

    /*Service Id*/
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "service_id", foreignKey = @ForeignKey(name = "FK_PARTYBUSSINESS_SERVICEID"))
    ServiceMaster serviceMaster;

    @Column(name = "service_code", length = 100)
    String serviceCode;

    /*Period*/
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "period_id", foreignKey = @ForeignKey(name = "FK_PARTYBUSSINESS_PERIODID"))
    PeriodMaster period;

    @Column(name = "period_code", length = 100)
    String periodCode;

    /* Source  */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "origin_id", foreignKey = @ForeignKey(name = "FK_PARTYBUSSINESS_FROMPORTID"))
    PortMaster origin;

    @Column(name = "origin_code", length = 100)
    String originCode;

    /* Destination  */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "destination_id", foreignKey = @ForeignKey(name = "FK_PARTYBUSSINESS_TOPORTID"))
    PortMaster destination;

    @Column(name = "destination_code", length = 100)
    String destinationCode;

    /*Commodity id*/
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "commodity_id", foreignKey = @ForeignKey(name = "FK_ARTYBUSSINESS_COMMODITYID"))
    CommodityMaster commodity;

    @Column(name = "commodity_code", length = 100)
    String commodityCode;

    /* Tos */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "tos_id", foreignKey = @ForeignKey(name = "FK_PARTYBUSSINESS_TOSID"))
    TosMaster tosMaster;

    @Column(name = "tos_code", length = 100)
    String tosCode;

    /*Minimum No Of Unit*/
    @Column(name = "no_of_units")
    Double noOfUnits;


    /* Tos */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "unit_id", foreignKey = @ForeignKey(name = "FK_PARTYBUSSINESS_UNITID"))
    UnitMaster unitMaster;


    /*Unit Code*/
    @Column(name = "unit_code")
    String unitCode;

    /*Minimum No Of Shipments*/
    @Column(name = "no_of_shipments")
    Double noOfShipments;


    /* estimated_revenue */
    @Column(name = "estimated_revenue")
    Long estimatedRevenue;


    /* Service  */
    @Column(name = "current_potential")
    @Enumerated(EnumType.STRING)
    CurrentPotential currentPotential;


    /* closuredate */
    @Column(name = "closuredate")
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date closureDate;


    /* bussinessStatus  */
    @Column(name = "bussiness_status")
    @Enumerated(EnumType.STRING)
    PartyBusinessStatus bussinessStatus;

    /* reason  */
    @Column(name = "reason")
    @Enumerated(EnumType.STRING)
    Reason reason;

    /*notes */
    @Column(name = "note_check", length = 5)
    String noteChecker;

    /*notes */
    @Column(name = "business_notes", length = 5000)
    String businessNotes;

    public PartyMaster getPartyMaster() {

        if (this.partyMaster == null)
            return null;

        PartyMaster tm = new PartyMaster();
        tm.setId(partyMaster.getId());
        tm.setPartyCode(partyMaster.getPartyCode());
        tm.setPartyName(partyMaster.getPartyName());
        return tm;
    }
}
