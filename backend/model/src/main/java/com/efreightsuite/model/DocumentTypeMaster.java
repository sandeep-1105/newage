package com.efreightsuite.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.UniqueConstraint;
import javax.persistence.Version;

import com.efreightsuite.enumeration.DocumentType;
import com.efreightsuite.enumeration.LovStatus;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;


@Entity
@Getter
@Setter
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Table(name = "efs_document_type_master",
        uniqueConstraints = {@UniqueConstraint(columnNames = {"document_type_code"}, name = "UK_DOCUMENT_DOCUMENTTYPECODE"),
                @UniqueConstraint(columnNames = {"document_type_name"}, name = "UK_DOCUMENT_DOCUMENTTYPENAME")}

)
public class DocumentTypeMaster implements Serializable {


    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /*Auto Generation Id*/
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    @Version
    @Column(name = "version_lock")
    long versionLock;

    /*Create/Last User and Data Info*/
    @Embedded
    //@JsonIgnore
            SystemTrack systemTrack;

    /*DocumentType*/
    @Column(name = "document_type_code", nullable = false, length = 30)
    @Enumerated(EnumType.STRING)
    DocumentType documentTypeCode;

    /* Document Type Name */
    @Column(name = "document_type_name", nullable = false, length = 100)
    String documentTypeName;

    /*Lov Status*/
    @Column(name = "status", nullable = false, length = 10)
    @Enumerated(EnumType.STRING)
    LovStatus status;


}
