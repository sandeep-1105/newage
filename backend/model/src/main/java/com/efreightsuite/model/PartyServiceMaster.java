package com.efreightsuite.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.UniqueConstraint;
import javax.persistence.Version;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;


@Entity
@Table(name = "efs_party_service_master", uniqueConstraints = {
        @UniqueConstraint(columnNames = {"party_id", "service_id", "location_id"}, name = "UK_PARTYSERVIE_PARTYSERVICE")}
)
@Getter
@Setter
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class PartyServiceMaster implements Serializable {
    private static final long serialVersionUID = 1L;

    /*Auto Generation Id*/
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    @Version
    @Column(name = "version_lock")
    long versionLock;

    /*Create/Last User and Data Info*/
    @Embedded
    SystemTrack systemTrack;

    /*Party id*/
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "party_id", foreignKey = @ForeignKey(name = "FK_PARTYSERVICE_PARTYID"))
    @JsonBackReference
    PartyMaster partyMaster;

    /*Segment Id*/
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "service_id", foreignKey = @ForeignKey(name = "FK_PARTYSERVICE_SERVICEID"))
    ServiceMaster serviceMaster;

    @Column(name = "service_code", length = 100)
    String serviceCode;

    /*TOS id*/
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "tos_id", foreignKey = @ForeignKey(name = "FK_PARTYSERVICE_TOSID"))
    TosMaster tosMaster;

    @Column(name = "tos_code", length = 100)
    String tosCode;

    /*Location id*/
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "location_id", foreignKey = @ForeignKey(name = "FK_PARTYSERVICE_LOCATIONID"))
    LocationMaster locationMaster;

    @Column(name = "location_code", length = 100)
    String locationCode;

    /*Salesman Id*/
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "salesman_id", foreignKey = @ForeignKey(name = "FK_PARTYSERVICE_SALEMANID"))
    EmployeeMaster salesman;

    @Column(name = "salesman_code", length = 100)
    String salesmanCode;

    /*Customer Service id*/
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "customer_service_id", foreignKey = @ForeignKey(name = "FK_PARTYSERVICE_CUSTSERVID"))
    EmployeeMaster customerService;

    @Column(name = "customer_service_code", length = 100)
    String customerServiceCode;

    public PartyMaster getPartyMaster() {

        if (this.partyMaster == null)
            return null;

        PartyMaster tm = new PartyMaster();
        tm.setId(partyMaster.getId());
        tm.setPartyCode(partyMaster.getPartyCode());
        tm.setPartyName(partyMaster.getPartyName());
        return tm;
    }
}
