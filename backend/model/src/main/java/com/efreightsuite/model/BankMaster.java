package com.efreightsuite.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.UniqueConstraint;
import javax.persistence.Version;

import com.efreightsuite.enumeration.LovStatus;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;


@Entity
@Getter
@Setter
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Table(name = "efs_bank_master",
        uniqueConstraints = {@UniqueConstraint(columnNames = {"bank_code"}, name = "UK_BANK_BANKCODE"),
                @UniqueConstraint(columnNames = {"bank_name"}, name = "UK_BANK_BANKNAME")}

)
public class BankMaster implements Serializable {


    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /*Auto Generation Id*/
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    @Version
    @Column(name = "version_lock")
    long versionLock;

    /*Create/Last User and Data Info*/
    @Embedded
    SystemTrack systemTrack;

    /*Bank Code*/
    @Column(name = "bank_code", nullable = false, length = 10)
    String bankCode;

    /*Bank Name*/
    @Column(name = "bank_name", nullable = false, length = 100)
    String bankName;

    /*Smart Business Bank Code*/
    @Column(name = "smart_business_bank_code", length = 30)
    String smartBusinessBankCode;

    /*Lov Status*/
    @Column(name = "status", nullable = false, length = 10)
    @Enumerated(EnumType.STRING)
    LovStatus status;


    public BankMaster() {
    }

    public BankMaster(String bankName, String bankCode, LovStatus status, SystemTrack systemTrack) {
        this.bankName = bankName;
        this.bankCode = bankCode;
        this.status = status;
        this.systemTrack = systemTrack;
    }
}
