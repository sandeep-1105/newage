package com.efreightsuite.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Version;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;


@Entity
@Getter
@Setter
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Table(name = "efs_report_parameter")
public class ReportTemplateParameter implements Serializable {
    private static final long serialVersionUID = 1L;

    /*Auto Generation Id*/
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    // CC address
    String paramKey;

    // BCC address
    String paramValue;

    /* Party id */
    @ManyToOne
    @JoinColumn(name = "report_template_id", foreignKey = @ForeignKey(name = "FK_R_PARAM_R_ID"))
    @JsonBackReference
    ReportTemplate reportTemplate;


    /*Create/Last User and Data Info*/
    @Embedded
    //@JsonIgnore
            SystemTrack systemTrack;


    @Version
    @Column(name = "version_lock")
    long versionLock;

}
