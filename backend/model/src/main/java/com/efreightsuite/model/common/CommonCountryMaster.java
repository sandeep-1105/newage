package com.efreightsuite.model.common;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.UniqueConstraint;

import com.efreightsuite.enumeration.LovStatus;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;


@Entity
@Getter
@Setter
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Table(name = "efs_common_country_master",
        uniqueConstraints = {
                @UniqueConstraint(columnNames = {"country_code"}, name = "UK_CMCOUNTRY_COUNTRYCODE"),
                @UniqueConstraint(columnNames = {"country_name"}, name = "UK_CMCOUNTRY_COUNTRYNAME")
        },
        indexes = {
                @Index(name = "IDX_COUNTRY_NAME", columnList = "country_name"),
                @Index(name = "IDX_COUNTRY_CODE", columnList = "country_code")
        }
)
public class CommonCountryMaster implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    @Column(name = "country_code", nullable = false, length = 10)
    String countryCode;

    @Column(name = "country_name", nullable = false, length = 100)
    String countryName;

    @ManyToOne
    @JoinColumn(name = "region_id", foreignKey = @ForeignKey(name = "FK_COMCOUNTRY_COMREGIONID"))
    CommonRegionMaster regionMaster;

    @Column(name = "region_code", length = 100)
    String regionCode;

    @Column(name = "locale", length = 10)
    String locale;

    @Column(name = "status", nullable = false, length = 10)
    @Enumerated(EnumType.STRING)
    LovStatus status;

    @Column(name = "image")
    @Lob
    byte[] image;

    public CommonCountryMaster() {
    }

    public CommonCountryMaster(Long id) {
        this.id = id;
    }

    public CommonCountryMaster(Long id, String countryName) {
        this.id = id;
        this.countryName = countryName;
    }

    public CommonCountryMaster(String countryName, String countryCode, LovStatus status) {
        this.countryCode = countryCode;
        this.countryName = countryName;
        this.status = status;
    }

    public CommonCountryMaster(CommonCountryMaster obj) {
        this.id = obj.id;
        this.countryCode = obj.countryCode;
        this.countryName = obj.countryName;
        this.locale = obj.locale;
        this.status = obj.status;
    }
}
