package com.efreightsuite.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.UniqueConstraint;
import javax.persistence.Version;

import com.efreightsuite.enumeration.LovStatus;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;


@Entity
@Getter
@Setter
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Table(name = "efs_currency_master", uniqueConstraints = {
        @UniqueConstraint(columnNames = {"currency_code"}, name = "UK_CURRENCY_CURRENCYCODE"),
        @UniqueConstraint(columnNames = {"currency_name"}, name = "UK_CURRENCY_CURRENCYNAME")})
public class CurrencyMaster implements Serializable {
    private static final long serialVersionUID = 1L;

    /* Auto Generation Id */
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    @Version
    @Column(name = "version_lock")
    long versionLock;

    /* Create/Last User and Data Info */
    @Embedded
    SystemTrack systemTrack;

    /* Currency Code */
    @Column(name = "currency_code", nullable = false, length = 10)
    String currencyCode;

    /* Currency Name */
    @Column(name = "currency_name", nullable = false, length = 100)
    String currencyName;

    /* Country Code */
    @ManyToOne
    @JoinColumn(name = "country_id", foreignKey = @ForeignKey(name = "FK_CURRENCY_COUNTRYID"))
    CountryMaster countryMaster;
    /* Country Code */
    @Column(name = "country_code", length = 10)
    String countryCode;
    /* Currency Prefix */
    @Column(name = "prefix", length = 30)
    String prefix;
    /* Currency Suffix */
    @Column(name = "suffix", length = 30)
    String suffix;
    /* Decimal Point */
    @Column(name = "decimal_point", nullable = false, length = 10)
    String decimalPoint;
    /* Currency Symbol */
    @Column(name = "symbol", length = 20)
    String symbol;
    /* Lov Status */
    @Column(name = "status", nullable = false, length = 10)
    @Enumerated(EnumType.STRING)
    LovStatus status;

    public CurrencyMaster() {
    }

    public CurrencyMaster(Long id, String currencyCode) {
        this.id = id;
        this.currencyCode = currencyCode;
    }

    //This getter used lazy init exception
    public CountryMaster getCountryMaster() {
        if (countryMaster == null) {
            return null;
        }

        if (countryMaster.getCurrencyMaster() != null) {
            CurrencyMaster tmp = new CurrencyMaster();
            tmp.setId(countryMaster.getCurrencyMaster().getId());
            tmp.setCurrencyCode(countryMaster.getCurrencyMaster().getCurrencyCode());
            tmp.setCurrencyName(countryMaster.getCurrencyMaster().getCurrencyName());
            tmp.setStatus(countryMaster.getCurrencyMaster().getStatus());

            countryMaster.setCurrencyMaster(tmp);
        }


        return countryMaster;
    }

}
