package com.efreightsuite.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Version;

import com.efreightsuite.util.JsonDateDeserializer;
import com.efreightsuite.util.JsonDateSerializer;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Getter;
import lombok.Setter;


@Entity
@Getter
@Setter
@Table(name = "efs_purchase_order_item")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class PurchaseOrderItem implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    @Version
    @Column(name = "version_lock")
    long versionLock;

    @Embedded
    SystemTrack systemTrack;

    /*Item Number*/
    @Column(name = "item_no", length = 100)
    String itemNo;

    @Column(name = "description", length = 250)
    String description;

    @Column(name = "no_of_Pieces")
    Integer noOfPiece;

    /*TOS Id*/
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "pack_id", foreignKey = @ForeignKey(name = "FK_PO_ITEM_PACK"), nullable = true)
    PackMaster packMaster;

    /*TOS Code*/
    @Column(name = "pack_code", nullable = true)
    String packCode;


    @Column(name = "piece_or_count")
    Integer pieceRCount;

    @Column(name = "order_piece")
    Integer orderPiece;

    @Column(name = "order_volume")
    Double orderVolume;

    @Column(name = "order_weight")
    Double orderWeight;

    @Column(name = "net_weight")
    Double netWeight;

    @Column(name = "pickup_from", nullable = true)
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date pickUpFrom;

    @Column(name = "pickup_within", nullable = true)
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date pickUpWithin;

    @Column(name = "delivery_from", nullable = true)
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date deliveryFrom;

    @Column(name = "delivery_within", nullable = true)
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date deliveryWithin;

    @Column(name = "supplier_conf_date", nullable = true)
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date supplierConfDate;

    @Column(name = "qc_status")
    String qcStatus;
    @Column(name = "ref1")
    String ref1;
    @Column(name = "ref2")
    String ref2;
    @Column(name = "ref3")
    String ref3;
    @Column(name = "ref4")
    String ref4;
    @Column(name = "ref5")
    String ref5;

    @Column(name = "amount_per_unit")
    Double amountPerUnit;

    @Column(name = "total_amount")
    Double totalAmount;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "currency_id", nullable = true, foreignKey = @ForeignKey(name = "FK_PO_ITEM_CURRECNY"))
    CurrencyMaster currencyMaster;

    @Column(name = "currency_code", length = 100)
    Double currencyCode;

    /* Purchase Order id */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "purchase_order_id", nullable = false, foreignKey = @ForeignKey(name = "FK_PO_ITEM_PO"))
    @JsonBackReference
    PurchaseOrder purchaseOrder;
}
