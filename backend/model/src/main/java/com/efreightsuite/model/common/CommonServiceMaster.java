package com.efreightsuite.model.common;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.UniqueConstraint;

import com.efreightsuite.enumeration.FullGroupage;
import com.efreightsuite.enumeration.ImportExport;
import com.efreightsuite.enumeration.LovStatus;
import com.efreightsuite.enumeration.TransportMode;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;


@Entity
@Getter
@Setter
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Table(name = "efs_common_service_master", uniqueConstraints = {
        @UniqueConstraint(columnNames = {"service_code"}, name = "UK_CM_SERVICE_SERVICECODE"),
        @UniqueConstraint(columnNames = {"service_name"}, name = "UK_CM_SERVICE_SERVICENAME")}

)
public class CommonServiceMaster implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    @Column(name = "service_code", nullable = false, length = 10)
    String serviceCode;

    @Column(name = "service_name", nullable = false, length = 100)
    String serviceName;

    @Column(name = "transport_mode", nullable = false, length = 10)
    @Enumerated(EnumType.STRING)
    TransportMode transportMode;

    @Column(name = "import_export", nullable = false, length = 10)
    @Enumerated(EnumType.STRING)
    ImportExport importExport;

    @Column(name = "full_groupage", length = 10)
    @Enumerated(EnumType.STRING)
    FullGroupage fullGroupage;

    @Column(name = "status", nullable = false, length = 10)
    @Enumerated(EnumType.STRING)
    LovStatus status;

    public CommonServiceMaster() {

    }

    public CommonServiceMaster(Long id) {
        this.id = id;
    }

    public CommonServiceMaster(String serviceCode, String serviceName, TransportMode transportMode, ImportExport importExport, LovStatus status) {
        this.serviceCode = serviceCode;
        this.serviceName = serviceName;
        this.transportMode = transportMode;
        this.importExport = importExport;
        this.status = status;
    }
}
