package com.efreightsuite.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;
import javax.persistence.Version;

import com.efreightsuite.enumeration.YesNo;
import com.efreightsuite.util.JsonYesNoDeserializer;
import com.efreightsuite.util.JsonYesNoSerializer;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Getter;
import lombok.Setter;


@Entity
@Getter
@Setter
@Table(name = "efs_po_attachment", uniqueConstraints = {
        @UniqueConstraint(columnNames = {"ref_no", "purchase_order_id"}, name = "UK_PO_ATTACHMENT_REF")
})
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class PurchaseOrderAttachment implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    @Version
    @Column(name = "version_lock")
    long versionLock;

    @Embedded
    SystemTrack systemTrack;

    /*Reference Number*/
    @Column(name = "ref_no", length = 100)
    String refNo;

    /*Customs*/
    @Column(name = "customs")
    @Enumerated(EnumType.STRING)
    @JsonSerialize(using = JsonYesNoSerializer.class)
    @JsonDeserialize(using = JsonYesNoDeserializer.class)
    YesNo customs;

    /*Unprotected File Name*/
    @Column(name = "unprotected_file_name", length = 100)
    String unprotectedFileName;

    /*Unprotected File Content Type*/
    @Column(name = "unprotected_file_content_type", length = 100)
    String unprotectedFileContentType;

    /*Unprotected File*/
    @Column(name = "unprotected_file_attached")
    @Lob
    byte[] unprotectedFile;

    /*protected File Name*/
    @Column(name = "protected_file_name", length = 100)
    String protectedFileName;

    /*protected File Content Type*/
    @Column(name = "protected_file_content_type", length = 100)
    String protectedFileContentType;

    /*protected File*/
    @Column(name = "protected_file_attached")
    @Lob
    byte[] protectedFile;

    /* Purchase Order id */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "purchase_order_id", nullable = false, foreignKey = @ForeignKey(name = "FK_PO_ATTACHMENT_PO"))
    @JsonBackReference
    PurchaseOrder purchaseOrder;

    @Transient
    boolean swapped = false;
}
