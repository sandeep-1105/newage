package com.efreightsuite.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.UniqueConstraint;
import javax.persistence.Version;

import com.efreightsuite.enumeration.GlHead;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Getter;
import lombok.Setter;


@Entity
@Getter
@Setter
@Table(name = "efs_general_ledger", uniqueConstraints = {
        @UniqueConstraint(columnNames = {"company_id", "gl_head", "gl_group_id", "gl_sub_group_id"}, name = "UK_GL_GLUNIQUE")})
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class GeneralLedger implements Serializable {
    private static final long serialVersionUID = 1L;

    /* Auto Generation Id */
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    @Version
    @Column(name = "version_lock")
    long versionLock;

    /* Create/Last User and Data Info */
    @Embedded
    SystemTrack systemTrack;

    @ManyToOne
    @JoinColumn(name = "company_id", nullable = false, foreignKey = @ForeignKey(name = "FK_GL_COMPANY"))
    CompanyMaster companyMaster;

    @Column(name = "company_code", nullable = false, length = 30)
    String companyCode;

    @Column(name = "gl_head", nullable = false, length = 30)
    @Enumerated(EnumType.STRING)
    GlHead glHead;

    @ManyToOne
    @JoinColumn(name = "gl_group_id", nullable = false, foreignKey = @ForeignKey(name = "FK_GL_GROUP"))
    GeneralLedgerGroup glGroup;

    @Column(name = "gl_group_code", nullable = false, length = 30)
    String glGroupCode;

    @ManyToOne
    @JoinColumn(name = "gl_sub_group_id", nullable = false, foreignKey = @ForeignKey(name = "FK_GL_SUBGROUP"))
    GeneralLedgerSubGroup glSubGroup;

    @Column(name = "gl_sub_group_code", nullable = false, length = 30)
    String glSubGroupCode;


    @OneToMany(mappedBy = "generalLedger", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.EAGER)
    @JsonManagedReference
    List<GeneralLedgerAccount> generalLedgerAccountList = new ArrayList<>();


}
