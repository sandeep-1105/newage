package com.efreightsuite.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Version;

import com.efreightsuite.enumeration.ConnectionStatus;
import com.efreightsuite.enumeration.TransportMode;
import com.efreightsuite.util.JsonDateDeserializer;
import com.efreightsuite.util.JsonDateSerializer;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Getter;
import lombok.Setter;


@Entity
@Table(name = "efs_consol_connection")
@Getter
@Setter
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class ConsolConnection implements Serializable {
    private static final long serialVersionUID = 1L;

    /* Auto Generation Id */
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    @Version
    @Column(name = "version_lock")
    long versionLock;

    /* Create/Last User and Data Info */
    @Embedded
    SystemTrack systemTrack;

    /* Consol id */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "consol_id", foreignKey = @ForeignKey(name = "FK_CONSOLCONN_CONSOL"))
    @JsonBackReference
    Consol consol;

    /* Transport Mode */
    @Column(name = "move", nullable = false, length = 10)
    @Enumerated(EnumType.STRING)
    TransportMode move;

    /* ETD */
    @Column(name = "etd", nullable = false)
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date etd;

    /* ETA */
    @Column(name = "eta", nullable = false)
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date eta;

    /* POL */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "pol_id", foreignKey = @ForeignKey(name = "FK_CONSOLCONN_POLID"), nullable = false)
    PortMaster pol;

    @Column(name = "pol", nullable = false)
    String polCode;

    /* POD */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "pod_id", foreignKey = @ForeignKey(name = "FK_CONSOLCONN_PODID"), nullable = false)
    PortMaster pod;

    @Column(name = "pod", nullable = false)
    String podCode;

    /* Carrier Id */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "carrier_id", foreignKey = @ForeignKey(name = "FK_CONSOLCONN_CARRIERID"))
    CarrierMaster carrierMaster;

    @Column(name = "carrier_code")
    String carrierCode;

    @Column(name = "flight_voyage_no", nullable = false)
    String flightVoyageNo;

    /* Lov Status */
    @Column(name = "connection_status", nullable = false, length = 30)
    @Enumerated(EnumType.STRING)
    ConnectionStatus connectionStatus;


}

