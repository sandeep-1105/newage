package com.efreightsuite.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Version;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;


@Entity
@Table(name = "efs_pricing_carrier_slap")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Setter
@Getter

public class PricingCarrierSlap implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    @Version
    @Column(name = "version_lock")
    long versionLock;

    @Embedded
    SystemTrack systemTrack;

    @Column(name = "minimum")
    Double minimum;

    @Column(name = "minus_45")
    Double minus45;

    @Column(name = "plus_45")
    Double plus45;

    @Column(name = "plus_100")
    Double plus100;

    @Column(name = "plus_250")
    Double plus250;

    @Column(name = "plus_300")
    Double plus300;

    @Column(name = "plus_500")
    Double plus500;

    @Column(name = "plus_1000")
    Double plus1000;


}
