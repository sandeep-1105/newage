package com.efreightsuite.model;

import java.io.Serializable;
import java.util.Comparator;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Transient;
import javax.persistence.Version;

import com.efreightsuite.enumeration.YesNo;
import com.efreightsuite.util.JsonDateDeserializer;
import com.efreightsuite.util.JsonDateSerializer;
import com.efreightsuite.util.JsonYesNoDeserializer;
import com.efreightsuite.util.JsonYesNoSerializer;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Type;


@Entity
@Getter
@Setter
@Table(name = "efs_shipment_service_trigger")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class ShipmentServiceTrigger implements Serializable {

    private static final long serialVersionUID = 1L;
    public static Comparator<ShipmentServiceTrigger> DateComparator = new Comparator<ShipmentServiceTrigger>() {

        @Override
        public int compare(ShipmentServiceTrigger o1, ShipmentServiceTrigger o2) {
            if (o1.getSystemTrack().getCreateDate().before(o2.getSystemTrack().getCreateDate())) {
                return -1;
            } else if (o1.getSystemTrack().getCreateDate().after(o2.getSystemTrack().getCreateDate())) {
                return 1;
            } else {
                return 0;
            }
        }

    };
    /* Auto Generation Id */
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;
    @Version
    @Column(name = "version_lock")
    long versionLock;
    /* Create/Last User and Data Info */
    @Embedded
    SystemTrack systemTrack;
    /* Date */
    @Column(name = "trigger_date", nullable = false)
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date date;
    /* Follow Up Date */
    @Column(name = "follow_up_date")
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date followUpDate;
    /* Shipment Service Detail */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "service_id", nullable = false, foreignKey = @ForeignKey(name = "FK_SST_SERVIDETID"))
    @JsonBackReference
    ShipmentServiceDetail shipmentServiceDetail;
    /* Service UID */
    @Column(name = "service_uid")
    String serviceUid;
    /* Shipment UID */
    @Column(name = "shipment_uid")
    String shipmentUid;
    /* TriggerMaster ID */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "trigger_master_id", nullable = false, foreignKey = @ForeignKey(name = "FK_SST_TRGR_ID"))
    TriggerMaster triggerMaster;
    @Column(name = "trigger_master_code", length = 100)
    String triggerMasterCode;
    /* TriggerTypeMaster ID */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "trigger_type_master_id", nullable = false, foreignKey = @ForeignKey(name = "FK_SST_TRGR_TYP_ID"))
    TriggerTypeMaster triggerTypeMaster;
    @Column(name = "trigger_type_master_code", length = 100)
    String triggerTypeMasterCode;
    /* Assigned to */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "employee_master_id", foreignKey = @ForeignKey(name = "FK_SST_EMP_MSTR_ID"))
    EmployeeMaster employeeMaster;
    @Column(name = "employee_master_code", length = 100)
    String employeeMasterCode;
    /* Protect */
    @Column(name = "protect", nullable = false)
    @Enumerated(EnumType.STRING)
    @JsonSerialize(using = JsonYesNoSerializer.class)
    @JsonDeserialize(using = JsonYesNoDeserializer.class)
    YesNo protect;
    /* Follow Up Required */
    @Column(name = "follow_up_required", nullable = false)
    @Enumerated(EnumType.STRING)
    @JsonSerialize(using = JsonYesNoSerializer.class)
    @JsonDeserialize(using = JsonYesNoDeserializer.class)
    YesNo followUpRequired;
    /* Note */
    @Column(name = "note", nullable = false)
    @Lob
    @Type(type = "org.hibernate.type.MaterializedClobType")
    String note;
    /* completed */
    @Column(name = "completed", nullable = false)
    @Enumerated(EnumType.STRING)
    @JsonSerialize(using = JsonYesNoSerializer.class)
    @JsonDeserialize(using = JsonYesNoDeserializer.class)
    YesNo completed;
    @Transient
    String dateTimeZone;
    @Transient
    String locationName;


}
