package com.efreightsuite.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.UniqueConstraint;

import com.efreightsuite.dto.WorkFlowGroupDTO;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Table(name = "efs_work_flow_group", uniqueConstraints = {
        @UniqueConstraint(columnNames = {"name"}, name = "UK_WRKFLOW_NAME")})
public class WorkFlowGroup implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /* Auto Generation Id */
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    /* Service Name */
    @Column(name = "name", nullable = false, length = 100)
    String workFlowName;

    public WorkFlowGroup() {

    }

    public WorkFlowGroup(String workFlowName) {
        this.workFlowName = workFlowName;
    }

    public WorkFlowGroup(Long id) {
        this.id = id;
    }


    public WorkFlowGroup(WorkFlowGroupDTO dto) {
        this.id = dto.getId();
        this.workFlowName = dto.getWorkFlowName();
    }

}
