package com.efreightsuite.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Version;

import com.efreightsuite.enumeration.LovStatus;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "efs_aes_filer")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class AesFilerMaster implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    private Long id;

    @Version
    @Column(name = "version_lock")
    private long versionLock;

    /* Create/Last User and Data Info */
    @Embedded
    private SystemTrack systemTrack;

    /* Location */
    @ManyToOne
    @JoinColumn(name = "location_id", foreignKey = @ForeignKey(name = "FK_AES_FILER_LOCATIONID"))
    private LocationMaster locationMaster;

    @Column(name = "location_code", length = 30)
    private String locationCode;

    /* Company */
    @ManyToOne
    @JoinColumn(name = "company_id", foreignKey = @ForeignKey(name = "FK_AES_FILER_COMPANYID"))
    private CompanyMaster companyMaster;

    @Column(name = "company_code", length = 30)
    private String companyCode;

    /* Country */
    @ManyToOne
    @JoinColumn(name = "country_id", foreignKey = @ForeignKey(name = "FK_AES_FILER_COUNTRYID"))
    private CountryMaster countryMaster;

    @Column(name = "country_code", length = 30)
    private String countryCode;

    /* Aes filer code */
    @Column(name = "aes_filer_code", nullable = false, length = 30)
    private String aesFilerCode;

    /* Transmitter code */
    @Column(name = "transmitter_code", nullable = false, length = 30)
    private String transmitterCode;

    /* Schema name */
    @Column(name = "schema_name", nullable = false, length = 100)
    private String schemaName;

    /* Lov Status */
    @Column(name = "status", nullable = false, length = 10)
    @Enumerated(EnumType.STRING)
    private LovStatus status;

    public AesFilerMaster() {
    }

    public AesFilerMaster(String schemaName, String aesFilerCode, String transmitterCode, LovStatus status, SystemTrack systemTrack, CountryMaster countryMaster, LocationMaster locationMaster) {
        this.aesFilerCode = aesFilerCode;
        this.transmitterCode = transmitterCode;
        this.schemaName = schemaName;
        this.status = status;
        this.systemTrack = systemTrack;
        this.countryMaster = countryMaster;
        this.locationMaster = locationMaster;
    }

    public Long getId() {
        return this.id;
    }

    public long getVersionLock() {
        return this.versionLock;
    }

    public SystemTrack getSystemTrack() {
        return this.systemTrack;
    }

    public void setSystemTrack(SystemTrack systemTrack) {
        this.systemTrack = systemTrack;
    }

    public LocationMaster getLocationMaster() {
        return this.locationMaster;
    }

    public void setLocationMaster(LocationMaster locationMaster) {
        this.locationMaster = locationMaster;
    }

    public String getLocationCode() {
        return this.locationCode;
    }

    public void setLocationCode(String locationCode) {
        this.locationCode = locationCode;
    }

    public CompanyMaster getCompanyMaster() {
        return this.companyMaster;
    }

    public void setCompanyMaster(CompanyMaster companyMaster) {
        this.companyMaster = companyMaster;
    }

    public String getCompanyCode() {
        return this.companyCode;
    }

    public void setCompanyCode(String companyCode) {
        this.companyCode = companyCode;
    }

    public CountryMaster getCountryMaster() {
        return this.countryMaster;
    }

    public void setCountryMaster(CountryMaster countryMaster) {
        this.countryMaster = countryMaster;
    }

    public String getCountryCode() {
        return this.countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getAesFilerCode() {
        return this.aesFilerCode;
    }

    public void setAesFilerCode(String aesFilerCode) {
        this.aesFilerCode = aesFilerCode;
    }

    public String getTransmitterCode() {
        return this.transmitterCode;
    }

    public void setTransmitterCode(String transmitterCode) {
        this.transmitterCode = transmitterCode;
    }

    public String getSchemaName() {
        return this.schemaName;
    }

    public void setSchemaName(String schemaName) {
        this.schemaName = schemaName;
    }

    public LovStatus getStatus() {
        return this.status;
    }

    public void setStatus(LovStatus status) {
        this.status = status;
    }
}
