package com.efreightsuite.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Version;

import com.efreightsuite.util.JsonDateDeserializer;
import com.efreightsuite.util.JsonDateSerializer;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Type;


@Entity
@Getter
@Setter
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Table(name = "efs_authenticated_docs")
public class AuthenticatedDocs implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    @Version
    @Column(name = "version_lock")
    long versionLock;

    /* Create/Last User and Data Info */
    @Embedded
    SystemTrack systemTrack;

    /* Shipment Service Detail */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "shipment_service_id", nullable = false, foreignKey = @ForeignKey(name = "FK_AUTH_DOCS_SERVIDETID"))
    @JsonBackReference
    ShipmentServiceDetail shipmentServiceDetail;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "company_id", nullable = false, foreignKey = @ForeignKey(name = "FK_AUTH_DOCS_COMPID"))
    CompanyMaster companyMaster;

    @Column(name = "company_code", nullable = false, length = 30)
    String companyCode;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "location_id", nullable = false, foreignKey = @ForeignKey(name = "FK_AUTH_DOCS_LOCTID"))
    LocationMaster locationMaster;

    @Column(name = "location_code", nullable = false, length = 30)
    String locationCode;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "country_id", nullable = false, foreignKey = @ForeignKey(name = "FK_AUTH_DOCS_COUNTID"))
    CountryMaster countryMaster;

    @Column(name = "country_code", nullable = false, length = 30)
    String countryCode;

    @Column(name = "document_no")
    String documentNo;

    /* Date */
    @Column(name = "auth_docs_date", nullable = false)
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date date;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "origin_id", foreignKey = @ForeignKey(name = "FK_AUTH_DOCS_ORIGINID"), nullable = false)
    PortMaster origin;

    @Column(name = "origin_code", length = 100)
    String originCode;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "destination_id", foreignKey = @ForeignKey(name = "FK_AUTH_DOCS_DESTINATIONID"), nullable = false)
    PortMaster destination;

    @Column(name = "destination_code", length = 100)
    String destinationCode;

    /* No. of Pieces */
    @Column(name = "no_of_piece")
    int noOfPiece;

    /* Gross Weight (kg) */
    @Column(name = "gross_weight")
    double grossWeight;

    /* Vol Weight(kg) */
    @Column(name = "vol_weight")
    double volWeight;

    /* Shipper id */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "shipper_id", foreignKey = @ForeignKey(name = "FK_AUTH_DOCS_SHIPPERID"))
    PartyMaster shipper;

    /* Shipper Code */
    @Column(name = "shipper_code")
    String shipperCode;

    /* shipperAddress */
    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "shipper_address_id", foreignKey = @ForeignKey(name = "FK_AUTH_DOCS_SHIPPERADDID"))
    AddressMaster shipperAddress;

    /* Consignee id */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "consignee_id", foreignKey = @ForeignKey(name = "FK_AUTH_DOCS_CONSIGNEEID"))
    PartyMaster consignee;

    /* Consignee Code */
    @Column(name = "consignee_code")
    String consigneeCode;

    /* Forward Address */
    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "consignee_address_id", foreignKey = @ForeignKey(name = "FK_AUTH_DOCS_CONSIGNADDID"))
    AddressMaster consigneeAddress;

    /* First Notify id */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "first_notify_id", foreignKey = @ForeignKey(name = "FK_AUTH_DOCS_FIRSTNOTIFYID"))
    PartyMaster firstNotify;

    /* First Notify Code */
    @Column(name = "first_notify_code")
    String firstNotifyCode;

    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    /* First Notify Address */
    @JoinColumn(name = "first_notify_address_id", foreignKey = @ForeignKey(name = "FK_AUTH_DOCS_FIRNOTADDID"))
    AddressMaster firstNotifyAddress;

    /* Forwarder id */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "forwarder_id", foreignKey = @ForeignKey(name = "FK_AUTH_DOCS_FORWARDID"))
    PartyMaster forwarder;

    /* Forwarder Code */
    @Column(name = "forwarder_code")
    String forwarderCode;

    /* Forward Address */
    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "forwarder_address_id", foreignKey = @ForeignKey(name = "FK_AUTH_DOCS_FORWARDADDID"))
    AddressMaster forwarderAddress;

    /* commodity  */
    @Column(name = "commodity")
    @Lob
    @Type(type = "org.hibernate.type.MaterializedClobType")
    String commodity;

    /* marksAndNo  */
    @Column(name = "marks_and_no")
    @Lob
    @Type(type = "org.hibernate.type.MaterializedClobType")
    String marksAndNo;
}
