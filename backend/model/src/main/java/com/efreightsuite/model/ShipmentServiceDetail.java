package com.efreightsuite.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.*;

import com.efreightsuite.enumeration.BooleanType;
import com.efreightsuite.enumeration.DimensionUnit;
import com.efreightsuite.enumeration.PPCC;
import com.efreightsuite.enumeration.TradeCode;
import com.efreightsuite.enumeration.TransitMode;
import com.efreightsuite.enumeration.WhoRouted;
import com.efreightsuite.enumeration.YesNo;
import com.efreightsuite.util.JsonDateDeserializer;
import com.efreightsuite.util.JsonDateSerializer;
import com.efreightsuite.util.JsonDimensionUnitDeserializer;
import com.efreightsuite.util.JsonDimensionUnitSerializer;
import com.efreightsuite.util.JsonPPccDeserializer;
import com.efreightsuite.util.JsonPPccSerializer;
import com.efreightsuite.util.JsonWhoRoutedDeserializer;
import com.efreightsuite.util.JsonWhoRoutedSerializer;
import com.efreightsuite.util.JsonYesNoDeserializer;
import com.efreightsuite.util.JsonYesNoSerializer;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Type;


@Entity
@Table(name = "efs_service", uniqueConstraints = {
        @UniqueConstraint(columnNames = {"service_uid"}, name = "UK_SERVICE_SERVICEUID"),
        @UniqueConstraint(columnNames = {"shipment_id", "service_master_id", "location_id", "company_id", "country_id"}, name = "UK_SERVICE_DUPLICATE")}

)
@Getter
@Setter
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class ShipmentServiceDetail implements Serializable {
    private static final long serialVersionUID = 1L;

    /* Auto Generation Id */
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    @Version
    @Column(name = "version_lock")
    long versionLock;

    /* Create/Last User and Data Info */
    @Embedded
    SystemTrack systemTrack;


    /* Service UID */
    @Column(name = "service_uid", nullable = false)
    String serviceUid;

    /* Shipment id */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "shipment_id", foreignKey = @ForeignKey(name = "FK_SERVICE_SHIPMENTID"), nullable = false)
    @JsonBackReference
    Shipment shipment;

    /* Shipment UID */
    @Column(name = "shipment_uid", nullable = false)
    String shipmentUid;

    /* Consol UID */
    @Column(name = "consol_uid")
    String consolUid;

    /* Consol UID */
    @Column(name = "quotation_uid")
    String quotationUid;

    @Column(name = "quotation_carrier_id")
    Long quotationCarrierId;

    @Column(name = "pro_tracking_no")
    String proTrackingNo;


    /* Service */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "service_master_id", foreignKey = @ForeignKey(name = "FK_SERVICE_SERVICEMASID"), nullable = false)
    ServiceMaster serviceMaster;

    /* Service Code */
    @Column(name = "service_code", nullable = false)
    String serviceCode;

    /*TOS Id*/
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "tos_id", foreignKey = @ForeignKey(name = "FK_SHIPMENT_TOSID"))
    TosMaster tosMaster;

    /*TOS Code*/
    @Column(name = "tos_code")
    String tosCode;

    /*Commodity Id*/
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "commodity_id", foreignKey = @ForeignKey(name = "FK_SHIPMENT_COMMODITYID"))
    CommodityMaster commodity;

    /*Commodity Group Code*/
    @Column(name = "commodity_code")
    String commodityCode;

    /*Service Request Date*/
    @Column(name = "service_req_date")
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date serviceReqDate;

    /* Direct Shipment*/
    @Column(name = "direct_shipment", length = 5)
    @Enumerated(EnumType.STRING)
    @JsonSerialize(using = JsonYesNoSerializer.class)
    @JsonDeserialize(using = JsonYesNoDeserializer.class)
    YesNo directShipment = YesNo.No;

    @Column(name = "is_freehand", length = 5)
    @Enumerated(EnumType.STRING)
    @JsonSerialize(using = JsonYesNoSerializer.class)
    @JsonDeserialize(using = JsonYesNoDeserializer.class)
    YesNo isFreehandShipment = YesNo.No;

    @Column(name = "is_clearance", length = 5)
    @Enumerated(EnumType.STRING)
    @JsonSerialize(using = JsonYesNoSerializer.class)
    @JsonDeserialize(using = JsonYesNoDeserializer.class)
    YesNo isClearance = YesNo.No;

    @Column(name = "is_transhipment", length = 5)
    @Enumerated(EnumType.STRING)
    @JsonSerialize(using = JsonYesNoSerializer.class)
    @JsonDeserialize(using = JsonYesNoDeserializer.class)
    YesNo isTranshipment = YesNo.No;


    @Column(name = "tranship_is_done_location", length = 5)
    @Enumerated(EnumType.STRING)
    @JsonSerialize(using = JsonYesNoSerializer.class)
    @JsonDeserialize(using = JsonYesNoDeserializer.class)
    YesNo transhipIsDoneLocation = YesNo.No;

    /* Customer Service Person id */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "customer_service_person_id", foreignKey = @ForeignKey(name = "FK_SERVICE_CUSTSERVIEPERSONID"), nullable = false)
    EmployeeMaster customerService;

    /* Service Code */
    @Column(name = "customer_service_person_code", nullable = false)
    String customerServiceCode;

    /* Customer Service Person Email */
    @Column(name = "customer_service_person_email")
    String customerServicePersonEmail;

    /* Freight Term */
    @Column(name = "freight_term", length = 10, nullable = false)
    @Enumerated(EnumType.STRING)
    @JsonSerialize(using = JsonPPccSerializer.class)
    @JsonDeserialize(using = JsonPPccDeserializer.class)
    PPCC ppcc = PPCC.Prepaid;

    /* Service Origin */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "service_origin_id", foreignKey = @ForeignKey(name = "FK_SERVICE_ORIGINID"), nullable = false)
    PortMaster origin;

    @Column(name = "service_origin", nullable = false)
    String originCode;

    /* POL */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "pol_id", foreignKey = @ForeignKey(name = "FK_SERVICE_POLID"), nullable = false)
    PortMaster pol;

    @Column(name = "pol", nullable = false)
    String polCode;

    /* POD */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "pod_id", foreignKey = @ForeignKey(name = "FK_SERVICE_PODID"), nullable = false)
    PortMaster pod;

    @Column(name = "pod", nullable = false)
    String podCode;

    /* Service Destination */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "service_destination_id", foreignKey = @ForeignKey(name = "FK_SERVICE_DESTINATIONID"), nullable = false)
    PortMaster destination;

    @Column(name = "service_destination", nullable = false)
    String destinationCode;

    /* Who Routed */
    @Column(name = "who_routed")
    @Enumerated(EnumType.STRING)
    @JsonSerialize(using = JsonWhoRoutedSerializer.class)
    @JsonDeserialize(using = JsonWhoRoutedDeserializer.class)
    WhoRouted whoRouted = WhoRouted.Self;

    /* Salesman id */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "salesman_id", foreignKey = @ForeignKey(name = "FK_SERVICE_SALESMANID"))
    EmployeeMaster salesman;

    /*Salesman Code*/
    @Column(name = "salesman_code")
    String salesmanCode;

    /* Agent Id */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "agent_id", foreignKey = @ForeignKey(name = "FK_SERVICE_AGENTID"))
    PartyMaster agent;

    /*Customer Code*/
    @Column(name = "agent_code")
    String agentCode;

    /* Division Id */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "division_id", foreignKey = @ForeignKey(name = "FK_SERVICE_DIVISONID"))
    DivisionMaster division;

    /* Division Code */
    @Column(name = "division_code")
    String divisionCode;

    /* Company Id */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "company_id", foreignKey = @ForeignKey(name = "FK_SERVICE_COMPANYID"), nullable = false)
    CompanyMaster company;

    /* Company Code */
    @Column(name = "company_code", nullable = false)
    String companyCode;

    /* Location Id */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "location_id", foreignKey = @ForeignKey(name = "FK_SERVICE_LOCATIONID"), nullable = false)
    LocationMaster location;

    /* Company Code */
    @Column(name = "location_code", nullable = false)
    String locationCode;

    /* Customer id */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "party_id", foreignKey = @ForeignKey(name = "FK_SERVICE_PARTYID"), nullable = false)
    PartyMaster party;

    /* Customer Code */
    @Column(name = "party_code", nullable = false)
    String partyCode;

    /* Co-Loaded Id */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "co_loaded_id", foreignKey = @ForeignKey(name = "FK_SERVICE_COLOADEDID"))
    PartyMaster coLoader;


    /* Co-Loaded Code */
    @Column(name = "co_loaded_code")
    String coLoaderCode;

    /* Item No */
    @Column(name = "item_no", length = 50)
    String itemNo;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "project_id", foreignKey = @ForeignKey(name = "FK_SHIPMENT_PROJECTID"))
    ProjectMaster project;

    @Column(name = "project_code", length = 100)
    String projectCode;

    /* Pack Id */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "pack_id", foreignKey = @ForeignKey(name = "FK_SERVICE_PACKMASTERID"))
    PackMaster packMaster;

    /* Pack Code */
    @Column(name = "pack_code")
    String packCode;

    /* Dimension Unit */
    @Column(name = "dimension_unit", length = 50)
    @Enumerated(EnumType.STRING)
    @JsonSerialize(using = JsonDimensionUnitSerializer.class)
    @JsonDeserialize(using = JsonDimensionUnitDeserializer.class)
    DimensionUnit dimensionUnit;

    /* Dimension Unit Value */
    @Column(name = "dimension_unit_value")
    double dimensionUnitValue;

    /* Booked Pieces */
    @Column(name = "booked_pieces")
    Long bookedPieces = 0L;

    /* Booked Gross Weight Unit KG */
    @Column(name = "booked_gross_wt_unit_kg")
    Double bookedGrossWeightUnitKg = 0.0;

    /* Booked Volume Weight Unit KG */
    @Column(name = "booked_volume_wt_unit_kg")
    Double bookedVolumeWeightUnitKg = 0.0;

    /* Booked Chargeable Unit */
    @Column(name = "booked_chargeable_unit")
    Double bookedChargeableUnit = 0.0;

    /* Booked Gross Weight Unit KG */
    @Column(name = "booked_gross_wt_unit_pound")
    Double bookedGrossWeightUnitPound = 0.0;

    /* Booked Volume Weight Unit KG */
    @Column(name = "booked_volume_wt_unit_pound")
    Double bookedVolumeWeightUnitPound = 0.0;


    /* Carrier Id */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "carrier_id", foreignKey = @ForeignKey(name = "FK_SERVICE_CARRIERID"))
    CarrierMaster carrier;

    /* Carrier Code */
    @Column(name = "carrier_code")
    String carrierCode;

    /* ETD */
    @Column(name = "etd")
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date etd;

    /* ETA */
    @Column(name = "eta")
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date eta;

    /* flight No */
    @Column(name = "route_no", length = 30)
    String routeNo;

    /* Master No */
    @Column(name = "mawb_no", length = 30)
    String mawbNo;

    /* Master No date */
    @Column(name = "mawb_date")
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date mawbDate;

    /* Local Currency */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "local_currency_id", foreignKey = @ForeignKey(name = "FK_SERVICE_LOCCURRID"))
    CurrencyMaster localCurrency;

    /* Local Currency Code */
    @Column(name = "local_currency_code")
    String localCurrencyCode;


    /* Client Gross Rate */
    @Column(name = "client_gross_rate")
    Double clientGrossRate;

    /* Client Net Rate */
    @Column(name = "client_net_rate")
    Double clientNetRate;


    /* Declared Cost */
    @Column(name = "declared_cost")
    Double declaredCost;

    /* cost */
    @Column(name = "cost")
    Double cost;

    /* Rate */
    @Column(name = "rate")
    Double rate;

    @Column(name = "carrier_booking_number")
    String carrierBookingNumber;

    @Column(name = "yard")
    String yard;

    @Column(name = "loading")
    String loading;

    @Column(name = "cfs_remark")
    String cfsRemark;

    @Column(name = "cfs_receipt_no")
    String cfsReceiptNo;

    @Column(name = "dn_no")
    String dnNo;

    @Column(name = "is_agreed", nullable = false)
    @Enumerated(EnumType.STRING)
    @JsonSerialize(using = JsonYesNoSerializer.class)
    @JsonDeserialize(using = JsonYesNoDeserializer.class)
    YesNo isAgreed = YesNo.No;


    @Column(name = "is_marks_no", nullable = false)
    @Enumerated(EnumType.STRING)
    @JsonSerialize(using = JsonYesNoSerializer.class)
    @JsonDeserialize(using = JsonYesNoDeserializer.class)
    YesNo isMarksNo = YesNo.Yes;

    /*AES No*/
    @Column(name = "aes_no")
    String aesNo;

    @Column(name = "auto_import")
    @Enumerated(EnumType.STRING)
    @JsonSerialize(using = JsonYesNoSerializer.class)
    @JsonDeserialize(using = JsonYesNoDeserializer.class)
    YesNo autoImport;

    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "pickup_delivery_id", foreignKey = @ForeignKey(name = "FK_SERVICE_PICKDELID"))
    PickUpDeliveryPoint pickUpDeliveryPoint;


    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "purchase_order_id", foreignKey = @ForeignKey(name = "FK_SERVICE_PURCORDID"))
    PurchaseOrder purchaseOrder;


    /*Purchase Order Number*/
    @Column(name = "po_no")
    String poNo;

    @ManyToOne(fetch = FetchType.LAZY)
    //@Transient
    @JoinColumn(name = "last_updated_status", foreignKey = @ForeignKey(name = "FK_SERVICE_LASTUPTSTATUS"))
    ServiceStatus lastUpdatedStatus;

    // AES FILE
    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "aes_id", foreignKey = @ForeignKey(name = "FK_AES_ID"))
    AesFile aesFile;

    // Service receive entry
    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "service_receive_id", foreignKey = @ForeignKey(name = "FK_SERVICE_ENTRY_ID"))
    ServiceOldEntry serviceReceiveEntry;

    /* Service */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "segment_master_detail", foreignKey = @ForeignKey(name = "FK_SERVICE_SEGMENTID"))
    ServiceMasterDetail segment;

    /* Country Id */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "country_id", foreignKey = @ForeignKey(name = "FK_SERVICE_COUNTRYID"))
    CountryMaster country;

    /* Country Code */
    @Column(name = "country_code", length = 10)
    String countryCode;

    /* Customer Manifest Name */
    @Column(name = "customer_manifest_name")
    String customerManifestName;

    /* Booked Volume Unit id */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "booked_volume_unit_id", foreignKey = @ForeignKey(name = "FK_SERVICE_BOOKEDVOLUMEUNITID"))
    UnitMaster bookedVolumeUnit;

    /* Booked Volume Unit Code */
    @Column(name = "booked_volume_unit_code")
    String bookedVolumeUnitCode;

    /* Booked Volume Unit Value */
    @Column(name = "booked_volume_unit_value")
    Double bookedVolumeUnitValue;
    /* Booked Volume Unit CBM */
    @Column(name = "booked_volume_unit_cbm")
    Double bookedVolumeUnitCbm;

    /* Booked Gross Weight Unit id */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "booked_gross_wt_unit_id", foreignKey = @ForeignKey(name = "FK_SERVICE_BOOKGROSSWEIGHTUNIT"))
    UnitMaster bookedGrossWeightUnit;

    /* Booked Gross Weight Unit Code */
    @Column(name = "booked_gross_wt_unit_code")
    String bookedGrossWeightUnitCode;

    /* Booked Gross Weight Unit Value */
    @Column(name = "booked_gross_wt_unit_value")
    Double bookedGrossWeightUnitValue;

    /* Booked Volume Weight Unit id */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "booked_volume_wt_unit_id", foreignKey = @ForeignKey(name = "FK_SERVICE_BOOKVOLUMEWEIGHTID"))
    UnitMaster bookedVolumeWeightUnit;

    /* Booked Volume Weight Unit Code */
    @Column(name = "booked_volume_wt_unit_code")
    String bookedVolumeWeightUnitCode;

    /* Booked Volume Weight Unit Value */
    @Column(name = "booked_volume_wt_unit_value")
    Double bookedVolumeWeightUnitValue;

    @Column(name = "hold_shipment", length = 5)
    @Enumerated(EnumType.STRING)
    @JsonSerialize(using = JsonYesNoSerializer.class)
    @JsonDeserialize(using = JsonYesNoDeserializer.class)
    YesNo holdShipment = YesNo.No;

    @Column(name = "hazardous", length = 5)
    @Enumerated(EnumType.STRING)
    @JsonSerialize(using = JsonYesNoSerializer.class)
    @JsonDeserialize(using = JsonYesNoDeserializer.class)
    YesNo hazardous = YesNo.No;

    @Column(name = "over_dimension", length = 5)
    @Enumerated(EnumType.STRING)
    @JsonSerialize(using = JsonYesNoSerializer.class)
    @JsonDeserialize(using = JsonYesNoDeserializer.class)
    YesNo overDimension = YesNo.No;

    /* Is Minimum Shipment */
    @Column(name = "is_minimum_shipment", length = 10)
    @Enumerated(EnumType.STRING)
    @JsonSerialize(using = JsonYesNoSerializer.class)
    @JsonDeserialize(using = JsonYesNoDeserializer.class)
    YesNo isMinimumShipment = YesNo.No;

    @Column(name = "hold_note", length = 4000)
    String holdNote;

    @Column(name = "hold_release_note", length = 4000)
    String holdReleaseNote;

    /* Schedule UID */
    @Column(name = "schedule_uid")
    String scheduleUid;
    /* Schedule Id */
    @Column(name = "schedule_id")
    Long vesselId;

    /* Service Detail */
    @OneToOne
    @JoinColumn(name = "service_detail_id", foreignKey = @ForeignKey(name = "FK_SERVICE_SERVICEDETAILID"))
    ServiceDetail serviceDetail;

    /* Trade Code */
    @Column(name = "trade_code")
    @Enumerated(EnumType.STRING)
    TradeCode tradeCode;

    /* Is Service Job */
    @Column(name = "is_service_job", length = 5)
    @Enumerated(EnumType.STRING)
    BooleanType isServiceJob;

    /* Transit Mode */
    @Column(name = "transit_mode")
    @Enumerated(EnumType.STRING)
    TransitMode transitMode;

    /* Transit Port */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "transit_port_id", foreignKey = @ForeignKey(name = "FK_SERVICE_TRANSITPORTID"))
    PortMaster transitPort;

    @Column(name = "transit_port")
    String transitPortCode;

    /* Is process completed */
    @Column(name = "is_process_completed")
    @Enumerated(EnumType.STRING)
    @JsonSerialize(using = JsonYesNoSerializer.class)
    @JsonDeserialize(using = JsonYesNoDeserializer.class)
    YesNo isProcessCompleted = YesNo.No;

    /* Brokerage Party */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "brokerage_party_id", foreignKey = @ForeignKey(name = "FK_SERVICE_BROKERAGE_PARTY_ID"), nullable = true)
    PartyMaster brokerageParty;

    @Column(name = "brokerage_party_code", length = 100)
    String brokeragePartyCode;

    /* Brokerage Percentage */
    @Column(name = "brokerage_percentage")
    Double brokeragePercentage;

    /*Export Ref No*/
    @Column(name = "export_ref")
    String exportRef;

    @Column(name = "import_ref")
    String importRef;

    /* Notes */
    @Column(name = "notes")
    @Lob
    @Type(type = "org.hibernate.type.MaterializedClobType")
    String notes;

    /* internal_note  */
    @Column(name = "internal_note")
    @Lob
    @Type(type = "org.hibernate.type.MaterializedClobType")
    String internalNote;

    @Transient
    String reasonForHold;

    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "sign_off_id", foreignKey = @ForeignKey(name = "FK_SERVICE_SIGNOFFID"))
    ShipmentServiceSignOff shipmentServiceSignOff;

    /* Is All Inclusive */
    @Column(name = "is_all_inclusive_charge", length = 10)
    @Enumerated(EnumType.STRING)
    @JsonSerialize(using = JsonYesNoSerializer.class)
    @JsonDeserialize(using = JsonYesNoDeserializer.class)
    YesNo isAllInclusiveCharges = YesNo.No;

    @Column(name = "process_instance_id")
    String processInstanceId;

    @Column(name = "task_id")
    String taskId;

    @Column(name = "workflow_completed", length = 5)
    @Enumerated(EnumType.STRING)
    @JsonSerialize(using = JsonYesNoSerializer.class)
    @JsonDeserialize(using = JsonYesNoDeserializer.class)
    YesNo workflowCompleted = YesNo.No;

    @Transient
    String isDownloadPrintFor;

    @OneToMany(mappedBy = "shipmentServiceDetail", cascade = CascadeType.ALL, orphanRemoval = true)
    @JsonManagedReference
    private List<ServiceStatus> serviceStatusList = new ArrayList<>();

    @OneToMany(mappedBy = "shipmentServiceDetail", cascade = CascadeType.ALL, orphanRemoval = true)
    @JsonManagedReference
    private List<DocumentDetail> documentList = new ArrayList<>();

    /*Shipment charge or rate tab*/
    @OneToMany(mappedBy = "shipmentServiceDetail", cascade = CascadeType.ALL, orphanRemoval = true)
    @JsonManagedReference
    private List<ShipmentCharge> shipmentChargeList = new ArrayList<>();

    /*Shipment Status tab*/
    @OneToMany(mappedBy = "shipmentServiceDetail", cascade = CascadeType.ALL, orphanRemoval = true)
    @JsonManagedReference
    private List<ShipmentServiceTrigger> shipmentServiceTriggerList = new ArrayList<>();

    /*Shipment document attachment */
    @OneToMany(mappedBy = "shipmentServiceDetail", cascade = CascadeType.ALL, orphanRemoval = true)
    @JsonManagedReference
    private List<ShipmentAttachment> shipmentAttachmentList = new ArrayList<>();

    /*Shipment COnnection List */
    @OneToMany(mappedBy = "shipmentServiceDetail", cascade = CascadeType.ALL, orphanRemoval = true)
    @JsonManagedReference
    private List<ServiceConnection> connectionList = new ArrayList<>();

    /*Event tab*/
    @OneToMany(mappedBy = "shipmentServiceDetail", cascade = CascadeType.ALL, orphanRemoval = true)
    @JsonManagedReference
    private List<ShipmentServiceEvent> eventList = new ArrayList<>();

    /*Reference tab*/
    @OneToMany(mappedBy = "shipmentServiceDetail", cascade = CascadeType.ALL, orphanRemoval = true)
    @JsonManagedReference
    private List<ShipmentServiceReference> referenceList = new ArrayList<>();

    /*AuthenticatedDocs tab*/
    @OneToMany(mappedBy = "shipmentServiceDetail", cascade = CascadeType.ALL, orphanRemoval = true)
    @JsonManagedReference
    private List<AuthenticatedDocs> authenticatedDocList = new ArrayList<>();

    /*CFS Receive Entry List*/
    @OneToMany(mappedBy = "shipmentServiceDetail", cascade = CascadeType.ALL, orphanRemoval = true)
    @JsonManagedReference
    private List<CfsReceiveEntry> cfsReceiveEntryList = new ArrayList<>();

    /*Bill of Entry List*/
    @OneToMany(mappedBy = "shipmentServiceDetail", cascade = CascadeType.ALL, orphanRemoval = true)
    @JsonManagedReference
    private List<BillOfEntry> billOfEntryList = new ArrayList<>();

    public ShipmentServiceDetail() {

    }

    public ShipmentServiceDetail(Long id) {
        this.id = id;
    }

    public ShipmentServiceDetail(ShipmentServiceDetail service) {
        this.id = service.getId();
        this.serviceUid = service.getServiceUid();
        this.consolUid = service.getConsolUid();
    }

    public String getHawbNo() {

        if (documentList != null && documentList.size() != 0) {
            return documentList.get(0).getDocumentNo();
        }

        return null;

    }

    public PartyMaster getShipper() {

        if (documentList != null && documentList.size() != 0) {
            return documentList.get(0).getShipper();
        }

        return null;

    }

    public AddressMaster getShipperAddress() {

        if (documentList != null && documentList.size() != 0) {
            return documentList.get(0).getShipperAddress();
        }

        return null;

    }

    public PartyMaster getConsignee() {

        if (documentList != null && documentList.size() != 0) {
            return documentList.get(0).getConsignee();
        }

        return null;

    }

    public void touch() {
        if (shipment != null) {/*
            setHazardous(shipment.getHazardous());
			setOverDimension(shipment.getOverDimension());
			setHoldShipment(shipment.getHoldShipment());
			setReasonForHold(shipment.getReasonForHold());
		*/
        }
    }

    public AddressMaster getConsigneeAddress() {

        if (documentList != null && documentList.size() != 0) {
            return documentList.get(0).getConsigneeAddress();
        }

        return null;

    }

    public PartyMaster getNotifyParty() {

        if (documentList != null && documentList.size() != 0) {
            return documentList.get(0).getFirstNotify();
        }

        return null;

    }

    public AddressMaster getNotifyPartyAddress() {

        if (documentList != null && documentList.size() != 0) {
            return documentList.get(0).getFirstNotifyAddress();
        }

        return null;

    }


}
