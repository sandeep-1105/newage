package com.efreightsuite.model;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.UniqueConstraint;
import javax.persistence.Version;

import com.efreightsuite.enumeration.LovStatus;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;


@Entity
@Getter
@Setter
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Table(name = "efs_cfs_master", uniqueConstraints = {
        @UniqueConstraint(columnNames = {"cfs_code"}, name = "UK_CFSMAS_CFSCODE"),
        @UniqueConstraint(columnNames = {"cfs_name"}, name = "UK_CFSMAS_CFSNAME")}

)
public class CFSMaster implements Serializable {

    private static final long serialVersionUID = 1L;

    /* Auto Generation Id */
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    @Version
    @Column(name = "version_lock")
    long versionLock;

    /* Create/Last User and Date Info */
    @Embedded
    SystemTrack systemTrack;

    /* CFS Code */
    @Column(name = "cfs_code", nullable = false, length = 100)
    String cfsCode;

    /* CFS Name */
    @Column(name = "cfs_name", nullable = false, length = 100)
    String cfsName;

    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "address_id", foreignKey = @ForeignKey(name = "FK_CFS_ADDRESS"))
    AddressMaster addressMaster;

    /* Port Id */
    @ManyToOne
    @JoinColumn(name = "port_master_id", foreignKey = @ForeignKey(name = "UK_CFSMAS_PORTID"))
    PortMaster portMaster;

    /* Port Code */
    @Column(name = "port_code", length = 10)
    String portCode;

    /* Status */
    @Column(name = "status", nullable = false, length = 10)
    @Enumerated(EnumType.STRING)
    LovStatus status;

}
