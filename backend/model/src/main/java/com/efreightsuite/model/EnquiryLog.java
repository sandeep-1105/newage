package com.efreightsuite.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.UniqueConstraint;
import javax.persistence.Version;

import com.efreightsuite.enumeration.EnquiryStatus;
import com.efreightsuite.util.JsonDateDeserializer;
import com.efreightsuite.util.JsonDateSerializer;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Getter;
import lombok.Setter;


@Entity
@Getter
@Setter
@Table(name = "efs_enquiry_log", uniqueConstraints = {
        @UniqueConstraint(columnNames = {"enquiry_no"}, name = "UK_ENQUIRY_NO")
})
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class EnquiryLog implements Serializable {
    private static final long serialVersionUID = 1L;

    /* Auto Generation Id */
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    @Version
    @Column(name = "version_lock")
    long versionLock;

    /* Create/Last User and Data Info */
    @Embedded
    SystemTrack systemTrack;

    /* Location */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "location_id", foreignKey = @ForeignKey(name = "FK_ENQUIRYLOG_LOCATIONID"))
    LocationMaster locationMaster;

    @Column(name = "location_code", length = 30)
    String locationCode;

    /* Company */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "company_id", foreignKey = @ForeignKey(name = "FK_ENQUIRYLOG_COMPANYID"))
    CompanyMaster companyMaster;

    @Column(name = "company_code", length = 30)
    String companyCode;

    /* Country */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "country_id", foreignKey = @ForeignKey(name = "FK_ENQUIRYLOG_COUNTRYID"))
    CountryMaster countryMaster;

    @Column(name = "country_code", length = 30)
    String countryCode;

    /* Logged On */
    @Column(name = "logged_on", nullable = true)
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date loggedOn;


    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "logged_by", foreignKey = @ForeignKey(name = "FK_ENQUIRYLOG_LOGGEDBY"))
    EmployeeMaster loggedBy;

    @Column(name = "logged_by_code", length = 100)
    String loggedByCode;


    /* Party */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "party_id", foreignKey = @ForeignKey(name = "FK_ENQUIRYLOG_PARTYID"))
    PartyMaster partyMaster;

    @Column(name = "party_code", length = 100)
    String partyMasterCode;

	/* Enquiry Customer */

    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "enquiry_customer_id", foreignKey = @ForeignKey(name = "FK_ENQUIRYLOG_CUSTID"))
    EnquiryCustomer enquiryCustomer;

    /* Enquiry No */
    @Column(name = "enquiry_no", nullable = true, length = 100)
    String enquiryNo;

    /* Received On */
    @Column(name = "received_on", nullable = true)
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date receivedOn;

    /* Status */
    @Column(name = "status", nullable = false, length = 10)
    @Enumerated(EnumType.STRING)
    EnquiryStatus status;

    /* Quote By */
    @Column(name = "quote_by")
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date quoteBy;

    /* Quotation No */
    @Column(name = "quotation_no", length = 100)
    String quotationNo;

    /* Sales Co-ordinator Id */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "sales_co_ordinator_id", foreignKey = @ForeignKey(name = "FK_ENQUIRYLOG_SALESCOORDID"))
    EmployeeMaster salesCoOrdinator;

    @Column(name = "sales_co_ordinator_code", length = 100)
    String salesCoOrdinatorCode;

    /* Salesman */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "salesman_id", foreignKey = @ForeignKey(name = "FK_ENQUIRYLOG_SALESMANID"))
    EmployeeMaster salesman;

    @Column(name = "salesman_code", length = 100)
    String salesmanCode;

    /* Created On */
    @Column(name = "created_on")
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date createdOn;

    /* Created By */
    @Column(name = "created_by", length = 100)
    String createdBy;

    /*EnquiryDetails*/
    @OneToMany(mappedBy = "enquiryLog", cascade = CascadeType.ALL, orphanRemoval = true)
    @JsonManagedReference
    private List<EnquiryDetail> enquiryDetailList = new ArrayList<>();

    public EnquiryLog() {
    }

    public EnquiryLog(Long id, String enquiryNo) {
        this.id = id;
        this.enquiryNo = enquiryNo;
    }

    public void setEnquiryDetailList(List<EnquiryDetail> enquiryDetailList) {
        this.enquiryDetailList.clear();
        if (enquiryDetailList != null) {
            this.enquiryDetailList.addAll(enquiryDetailList);
        }
    }
}
