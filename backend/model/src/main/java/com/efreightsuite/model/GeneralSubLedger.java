package com.efreightsuite.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Version;

import com.efreightsuite.enumeration.LovStatus;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;


@Entity
@Getter
@Setter
@Table(name = "efs_general_sub_ledger")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class GeneralSubLedger implements Serializable {

    private static final long serialVersionUID = 1L;

    /* Auto Generation Id */
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    @Version
    @Column(name = "version_lock")
    long versionLock;

    /* Create/Last User and Data Info */
    @Embedded
    SystemTrack systemTrack;

    @ManyToOne
    @JoinColumn(name = "general_ledger_account_id", foreignKey = @ForeignKey(name = "FK_GLSUBLED_GENLEDACC"))
    @JsonBackReference
    GeneralLedgerAccount generalLedgerAccount;

    @ManyToOne
    @JoinColumn(name = "company_id", nullable = false, foreignKey = @ForeignKey(name = "FK_GLSUBLED_COMPANY"))
    CompanyMaster companyMaster;

    @Column(name = "company_code", nullable = false, length = 30)
    String companyCode;

    @ManyToOne
    @JoinColumn(name = "location_id", nullable = false, foreignKey = @ForeignKey(name = "FK_GLSUBLED_LOCATION"))
    LocationMaster locationMaster;

    @Column(name = "location_code", nullable = false, length = 30)
    String locationCode;

    @Column(name = "gl_sub_ledger_code", nullable = false, length = 30)
    String glSubLedgerCode;

    @Column(name = "gl_sub_ledger_name", nullable = false, length = 100)
    String glSubLedgerName;

    @Column(name = "gl_sub_ledger_party_id", nullable = false)
    Long partyMaster;

    /*Status*/
    @Column(name = "status", nullable = false, length = 10)
    @Enumerated(EnumType.STRING)
    LovStatus status;
}
