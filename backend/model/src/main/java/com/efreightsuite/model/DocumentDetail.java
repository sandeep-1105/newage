package com.efreightsuite.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.*;

import com.efreightsuite.enumeration.DimensionUnit;
import com.efreightsuite.enumeration.RateClass;
import com.efreightsuite.enumeration.ServiceCode;
import com.efreightsuite.enumeration.YesNo;
import com.efreightsuite.util.JsonDateDeserializer;
import com.efreightsuite.util.JsonDateSerializer;
import com.efreightsuite.util.JsonDimensionUnitDeserializer;
import com.efreightsuite.util.JsonDimensionUnitSerializer;
import com.efreightsuite.util.JsonYesNoDeserializer;
import com.efreightsuite.util.JsonYesNoSerializer;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Getter;
import lombok.Setter;


@Entity
@Table(name = "efs_document_detail", uniqueConstraints = {
        @UniqueConstraint(columnNames = {"document_no"}, name = "UK_DOCUMENTDETA_DOCUMENTNO")
})
@Getter
@Setter
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class DocumentDetail implements Serializable {
    private static final long serialVersionUID = 1L;
    /* isCustomerConform */
    @Column(name = "is_customer_conform")
    @Enumerated(EnumType.STRING)
    @JsonSerialize(using = JsonYesNoSerializer.class)
    @JsonDeserialize(using = JsonYesNoDeserializer.class)
    public YesNo isCustomerConform = YesNo.No;
    /* Auto Generation Id */
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;
    @Version
    @Column(name = "version_lock")
    long versionLock;
    /* Create/Last User and Data Info */
    @Embedded
    SystemTrack systemTrack;
    /* Service id */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "service_id", foreignKey = @ForeignKey(name = "FK_DOCDETAIL_SERVICEID"))
    @JsonBackReference
    ShipmentServiceDetail shipmentServiceDetail;
    /* Document Req Date */
    @Column(name = "document_req_date", nullable = false)
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date documentReqDate;
    @Column(name = "shipment_uid", nullable = false)
    String shipmentUid;
    @Column(name = "service_uid", nullable = false)
    String serviceUid;//new Gen
    @Column(name = "document_no", length = 100, nullable = false)
    String documentNo;
    /* Shipper id */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "shipper_id", foreignKey = @ForeignKey(name = "FK_DOCDETAIL_SHIPPERID"), nullable = false)
    PartyMaster shipper;
    /* Shipper Code */
    @Column(name = "shipper_code")
    String shipperCode;
    /* shipperAddress */
    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "shipper_address_id", foreignKey = @ForeignKey(name = "FK_DOCDETAIL_SHIPPERADDID"), nullable = false)
    AddressMaster shipperAddress;
    /* Shipper Manifest Name */
    @Column(name = "shipper_manifest_name")
    String shipperManifestName;
    /* Forwarder id */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "forwarder_id", foreignKey = @ForeignKey(name = "FK_DOCDETAIL_FORWARDID"))
    PartyMaster forwarder;
    /* Forwarder Code */
    @Column(name = "forwarder_code")
    String forwarderCode;
    /* Forwarder Manifest Name */
    @Column(name = "forwarder_manifest_name")
    String forwarderManifestName;
    /* Forward Address */
    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "forwarder_address_id", foreignKey = @ForeignKey(name = "FK_DOCDETAIL_FORWARDADDID"))
    AddressMaster forwarderAddress;
    /* Consignee id */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "consignee_id", foreignKey = @ForeignKey(name = "FK_DOCDETAIL_CONSIGNEEID"))
    PartyMaster consignee;
    /* Consignee Code */
    @Column(name = "consignee_code")
    String consigneeCode;
    /* Consignee Manifest Name */
    @Column(name = "consignee_manifest_name")
    String consigneeManifestName;
    /* Forward Address */
    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "consignee_address_id", foreignKey = @ForeignKey(name = "FK_DOCDETAIL_CONSIGNADDID"))
    AddressMaster consigneeAddress;
    /* First Notify id */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "first_notify_id", foreignKey = @ForeignKey(name = "FK_DOCDETAIL_FIRSTNOTIFYID"))
    PartyMaster firstNotify;
    /* First Notify Code */
    @Column(name = "first_notify_code")
    String firstNotifyCode;
    /* First Notify Manifest Name */
    @Column(name = "first_notify_manifest_name")
    String firstNotifyManifestName;
    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    /* First Notify Address */
    @JoinColumn(name = "first_notify_address_id", foreignKey = @ForeignKey(name = "FK_DOCDETAIL_FIRNOTADDID"))
    AddressMaster firstNotifyAddress;
    /* Second Notify id */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "second_notify_id", foreignKey = @ForeignKey(name = "FK_DOCDETAIL_SECONDNOTIFYID"))
    PartyMaster secondNotify;
    /* Second Notify Code */
    @Column(name = "second_notify_code")
    String secondNotifyCode;
    /* Second Notify Manifest Name */
    @Column(name = "second_notify_manifest_name")
    String secondNotifyManifestName;
    /* Second Notify Address */
    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "second_notify_address_id", foreignKey = @ForeignKey(name = "FK_DOCDETAIL_SECNOTADDID"))
    AddressMaster secondNotifyAddress;
    /* Agent id */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "agent_id", foreignKey = @ForeignKey(name = "FK_DOCDETAIL_AGENTID"))
    PartyMaster agent;
    /* Agent Code */
    @Column(name = "agent_code")
    String agentCode;
    /* Agent Manifest Name */
    @Column(name = "agent_manifest_name")
    String agentManifestName;
    /* Agent Address */
    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "agent_address_id", foreignKey = @ForeignKey(name = "FK_DOCDETAIL_AGENTADDID"))
    AddressMaster agentAddress;
    /* Issuing Agent id */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "doc_issuing_agent_id", foreignKey = @ForeignKey(name = "FK_DOCDETAIL_ISSAGENTID"))
    PartyMaster issuingAgent;
    /* Issuing Agent Code */
    @Column(name = "doc_issuing_agent_code")
    String issuingAgentCode;
    /* Document Issuing Agent Name */
    @Column(name = "doc_issuing_agent_name")
    String issuingAgentName;
    /* Issuing Address */
    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "doc_issuing_agent_add_id", foreignKey = @ForeignKey(name = "FK_DOCDETAIL_ISSAGENTADDID"))
    AddressMaster issuingAgentAddress;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "doc_cha_agent_id", foreignKey = @ForeignKey(name = "FK_DOCDETAIL_CHAAGENTID"))
    PartyMaster chaAgent;
    @Column(name = "doc_cha_agent_code")
    String chaAgentCode;
    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "doc_cha_agent_add_id", foreignKey = @ForeignKey(name = "FK_DOCDETAIL_CHAAGENTADDID"))
    AddressMaster chaAgentAddress;
    /* Origin */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "doc_origin_id", foreignKey = @ForeignKey(name = "FK_DOCDETAIL_DOCORIGINID"))
    PortMaster origin;
    @Column(name = "doc_origin")
    String originCode;
    /* POL */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "pol_id", foreignKey = @ForeignKey(name = "FK_DOCDETAIL_POLID"))
    PortMaster pol;
    @Column(name = "pol")
    String polCode;
    /* POD */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "pod_id", foreignKey = @ForeignKey(name = "FK_DOCDETAIL_PODID"))
    PortMaster pod;
    @Column(name = "pod")
    String podCode;
    /* Destination */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "doc_destination_id", foreignKey = @ForeignKey(name = "FK_DOCDETAIL_DOCDESTIID"))
    PortMaster destination;
    @Column(name = "doc_destination")
    String destinationCode;
    /* Pack Id */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "pack_id", foreignKey = @ForeignKey(name = "FK_DOCDETAIL_PACKID"))
    PackMaster packMaster;
    /* Pack Code */
    @Column(name = "pack_code")
    String packCode;
    /* Pack Description */
    @Column(name = "pack_description", length = 400)
    String packDescription;
    /* Booking No Of Pack */
    @Column(name = "no_of_pieces")
    Long noOfPieces;
    /* Dimension Unit */
    @Column(name = "dimension_unit", nullable = false, length = 50)
    @Enumerated(EnumType.STRING)
    @JsonSerialize(using = JsonDimensionUnitSerializer.class)
    @JsonDeserialize(using = JsonDimensionUnitDeserializer.class)
    DimensionUnit dimensionUnit;
    /* Dimension Unit Value */
    @Column(name = "dimension_unit_value")
    double dimensionUnitValue;
    /* chargeble weight */
    @Column(name = "chargeble_weight")
    Double chargebleWeight;
    /* gross weight */
    @Column(name = "gross_weight")
    Double grossWeight;
    /* volume weight */
    @Column(name = "volume_weight")
    Double volumeWeight;
    /*  Vol Weight in pound */
    @Column(name = "volume_weight_in_pound")
    Double volumeWeightInPound;
    /*  Gross Weight in pound*/
    @Column(name = "gross_weight_in_pound")
    Double grossWeightInPound;
    /* Booking Volume in CBM */
    @Column(name = "booking_volume_in_cbm")
    Double bookedVolumeUnitCbm;
    /* gross weight */
    @Column(name = "dimension_gross_weight")
    Double dimensionGrossWeight;
    /* volume weight */
    @Column(name = "dimension_volume_weight")
    Double dimensionVolumeWeight;
    @Column(name = "dimension_gross_weight_in_kg")
    Double dimensionGrossWeightInKg;
    @Column(name = "dimension_no_of_pieces")
    Long dimensionNoOfPieces;
    /* Rate / Charge */
    @Column(name = "rate_per_charge")
    Double ratePerCharge;
    /* Handling Information */
    @Column(name = "handling_information", length = 400)
    String handlingInformation;
    /* Accounting Information */
    @Column(name = "accounting_information", length = 400)
    String accoutingInformation;
    /* Commodity Description */
    @Column(name = "commodity_description", length = 400)
    String commodityDescription;
    /* Marks & No */
    @Column(name = "marks_and_no", length = 400)
    String marksAndNo;
    /* Rate Class */
    @Column(name = "rate_class", length = 2)
    @Enumerated(EnumType.STRING)
    RateClass rateClass;
    /* BL Received Person */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "bl_received_person_id", foreignKey = @ForeignKey(name = "FK_DOCDETAIL_BLRECEMPID"))
    EmployeeMaster blReceivedPerson;
    /* BL Received Person Code */
    @Column(name = "bl_received_person_code", length = 30)
    String blReceivedPersonCode;
    /* BL Received Date */
    @Column(name = "bl_received_date")
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date blReceivedDate;
    /* Company Id */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "company_id", foreignKey = @ForeignKey(name = "FK_DOCDETAIL_COMPANYID"))
    CompanyMaster company;
    @Column(name = "company_code", length = 100)
    String companyCode;
    /* Country id */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "country_id", foreignKey = @ForeignKey(name = "FK_DOCDETAIL_COUNTRYID"))
    CountryMaster country;
    /* Country Code */
    @Column(name = "country_code", length = 10)
    String countryCode;
    /* Location Id */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "location_id", foreignKey = @ForeignKey(name = "FK_DOCDETAIL_LOCATIONID"))
    LocationMaster location;
    @Column(name = "location_code", length = 100)
    String locationCode;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "doc_prefix_id", foreignKey = @ForeignKey(name = "FK_DOCDETAIL_DOCPREFIXID"))
    DocumentPrefixMaster docPrefixMaster;
    @Column(name = "doc_prefix_code")
    String docPrefixCode;
    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "pickup_delivery_id", foreignKey = @ForeignKey(name = "FK_DOCDETAIL_PICKDELID"))
    PickUpDeliveryPoint pickUpDeliveryPoint;
    /* Service Code */
    @Column(name = "service_code")
    @Enumerated(EnumType.STRING)
    ServiceCode serviceCode;
    /* Vessel Id */
    @Column(name = "vessel_id")
    Long vesselId;
    /* Vessel Code */
    @Column(name = "vessel_code")
    String vesselCode;
    /* Route No */
    @Column(name = "route_no")
    String routeNo;
    /* Transit Port */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "transit_port_id", foreignKey = @ForeignKey(name = "FK_DOCDETAIL_TRANSITPORTID"))
    PortMaster transitPort;
    @Column(name = "transit_port")
    String transitPortCode;
    /* Transit ETA */
    @Column(name = "transit_eta")
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date transitEta;
    /* Transit Schedule UID */
    @Column(name = "transit_schedule_uid")
    String transitScheduleUid;
    /* Master No */
    @Column(name = "mawb_no", length = 30)
    String mawbNo;
    /* Carrier Id */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "carrier_id", foreignKey = @ForeignKey(name = "FK_DOCDETAIL_CARRIERID"))
    CarrierMaster carrier;
    @Column(name = "carrier_code", length = 100)
    String carrierCode;
    /* ETD */
    @Column(name = "etd")
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date etd;
    /* ETA */
    @Column(name = "eta")
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date eta;
    /* Commodity Id */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "commodity_id", foreignKey = @ForeignKey(name = "FK_DOCDETAIL_COMMODITYID"))
    CommodityMaster commodity;
    /* Commodity Code */
    @Column(name = "commodity_code")
    String commodityCode;
    @OneToOne
    @JoinColumn(name = "document_sub_detail", foreignKey = @ForeignKey(name = "FK_DOCDETAIL_DOCSUBDETAILID"))
    DocumentSubDetail documentSubDetail;
    @OneToMany(mappedBy = "documentDetail", cascade = CascadeType.ALL, orphanRemoval = true)
    @JsonManagedReference
    List<ServiceDocumentDimension> dimensionList = new ArrayList<>();
    /* Master No */
    @Column(name = "hawb_no", length = 100)
    String hawbNo;
    @Column(name = "manifest_hawb_no", length = 100)
    String manifestHawbNo;
    /* hawb_received_by */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "hawb_received_id", foreignKey = @ForeignKey(name = "FK_DOCDETAIL_HAWBRECEIVED"))
    EmployeeMaster hawbReceivedBy;
    @Column(name = "hawb_received_by_code", length = 100)
    String hawbReceivedByCode;

    //For Report -Dynamic Purpose -HAWB
    /* hawb_received__on */
    @Column(name = "hawb_received_on")
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date hawbReceivedOn;
    @Column(name = "declared_value_carriage")
    String declaredValueForCarriage = "NVD";
    @Column(name = "diversion_contray")
    String diversionContray = "SCI";
    @Column(name = "declared_value_customs")
    String declaredValueForCustoms = "NCV";
    @Column(name = "optional_information")
    String optionalShippingInfo;
    @Column(name = "amount_of_insurance")
    String amountOfInsurance = "NIL";
    @Column(name = "chargescode")
    Character chgsCode;
    @Column(name = "wt_ValPpd")
    Character wtValPrepaid;
    @Column(name = "wt_ValCol")
    Character wtValCollect;
    @Column(name = "others_prepaid")
    Character othersPrepaid;
    @Column(name = "others_collect")
    Character othersCollect;
    @Column(name = "total_amount")
    String totalAmount;
    @Column(name = "pre_weight_charge")
    String preWeightCharge;
    @Column(name = "coll_weight_charge")
    String collWeightCharge;
    @Column(name = "pre_valuation_charge")
    String preValuationCharge;
    @Column(name = "coll_valuation_charge")
    String collValuationCharge;
    @Column(name = "pre_tax")
    String prepaidTax;
    @Column(name = "coll_tax")
    String collectTax;
    @Column(name = "othercharges_pre_agent")
    String otherChargesDuePreAgent;
    @Column(name = "othercharges_coll_agent")
    String otherChargesDueCollAgent;
    @Column(name = "othercharges_pre_carrier")
    String otherChargesDuePreCarrier;
    @Column(name = "othercharges_coll_carrier")
    String otherChargesDueCollCarrier;
    @Column(name = "total_prepaid")
    String totalPrepaid;
    @Column(name = "total_collect")
    String totalCollect;
    @Column(name = "total_charges")
    String totalCollectCharges;
    @Column(name = "currency_conversion_rates")
    String currencyConversionRates;
    @Column(name = "charges_dest_currency")
    String chargesInDestCurrency;
    @Column(name = "charges_at_destination")
    String chargesAtDestination;
    @Column(name = "pan_no")
    String panNo;
    @Column(name = "shipper_sign")
    String shipperSignature;
    @Column(name = "agent_sign")
    String issCarrierAgentSign;
    @Column(name = "commodity_item_no")
    String commodityNo;
    @Column(name = "do_signature")
    String doSignature;
    @Column(name = "do_number", length = 100)
    String doNumber;
    @Column(name = "do_issued_date")
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date doIssuedDate;
    /* do_received_by */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "do_issued_by", foreignKey = @ForeignKey(name = "FK_DOCDETAIL_DOISS"))
    EmployeeMaster doIssuedBy;
    @Column(name = "do_issued_by_code", length = 100)
    String doIssuedByCode;
    @Column(name = "can_number", length = 100)
    String canNumber;
    /* hawb_received_by */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "can_issued_by", foreignKey = @ForeignKey(name = "FK_DOCDETAIL_CANISS"))
    EmployeeMaster canIssuedBy;
    @Column(name = "can_issued_by_code", length = 100)
    String canIssuedByCode;
    @Column(name = "ccn_number", length = 30)
    String ccnNumber;
    @Column(name = "ccn_date")
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date ccnDate;
    @Column(name = "trans_ccn_number", length = 30)
    String transCcnNumber;
    @Column(name = "trans_ccn_date")
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date transCcnDate;
    @Column(name = "can_issued_date")
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date canIssuedDate;
    @Column(name = "hold_note", length = 400)
    String holdNote;
    /* referenceNo */
    @Column(name = "ref_no", length = 30)
    String referenceNo;
    /* Customer IP */
    @Column(name = "customer_ip", length = 100)
    String customerIp;
    //used for Documents report
    @Transient
    Long serviceId;

    @Transient
    Consol consol;
}
