package com.efreightsuite.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Version;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;


@Entity
@Getter
@Setter
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Table(name = "efs_email_request_attachment")
public class EmailRequestAttachment implements Serializable {
    private static final long serialVersionUID = 1L;

    /* Auto Generation Id */
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    @Version
    @Column(name = "version_lock")
    long versionLock;

    /* Create/Last User and Data Info */
    @Embedded
    SystemTrackManual manualSystemTrack;

    /* Port Group id */
    @ManyToOne
    @JoinColumn(name = "email_request_id", foreignKey = @ForeignKey(name = "FK_EMAILREQATTACH_EMAILREQID"))
    @JsonBackReference
    EmailRequest emailRequest;

    @Column(name = "attachment", length = 10000)
    byte[] attachement;

    @Column(name = "file_name", nullable = false, length = 100)
    String fileName;

    @Column(name = "mime_type", nullable = false, length = 100)
    String mimeType;

}
