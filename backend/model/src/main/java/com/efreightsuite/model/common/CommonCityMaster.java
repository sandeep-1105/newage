package com.efreightsuite.model.common;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.UniqueConstraint;

import com.efreightsuite.enumeration.LovStatus;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;


@Entity
@Getter
@Setter
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Table(name = "efs_common_city_master",
        uniqueConstraints = {@UniqueConstraint(columnNames = {"city_code", "state_id", "country_id"}, name = "UK_CMCITY_CITYCODE"),
                @UniqueConstraint(columnNames = {"city_name", "state_id", "country_id"}, name = "UK_CMCITY_CITYNAME")}
)
public class CommonCityMaster implements Serializable {
    private static final long serialVersionUID = 1L;

    /*Auto Generation Id*/
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    /*Country Id*/
    @ManyToOne
    @JoinColumn(name = "country_id", foreignKey = @ForeignKey(name = "FK_CMCITY_COUNTRYID"))
    CommonCountryMaster countryMaster;

    /*Country Code*/
    @Column(name = "country_code", length = 10)
    String countryCode;

    /*Country Id*/
    @ManyToOne
    @JoinColumn(name = "state_id", foreignKey = @ForeignKey(name = "FK_CMCITY_STATEID"))
    CommonStateMaster stateMaster;

    /*Country Code*/
    @Column(name = "state_code", length = 10)
    String stateCode;

    /*City Code*/
    @Column(name = "city_code", nullable = false, length = 10)
    String cityCode;

    /*City Name*/
    @Column(name = "city_name", nullable = false, length = 100)
    String cityName;

    /* Lov Status */
    @Column(name = "status", nullable = false, length = 10)
    @Enumerated(EnumType.STRING)
    LovStatus status = LovStatus.Active;

    public CommonCityMaster() {
    }

    public CommonCityMaster(CommonCityMaster commonCityMaster) {
        this.id = commonCityMaster.getId();
        this.cityName = commonCityMaster.getCityName();
        this.cityCode = commonCityMaster.getCityCode();
        this.status = commonCityMaster.getStatus();
    }

    public CommonCityMaster(String cityCode, String cityName, LovStatus status) {
        this.cityCode = cityCode;
        this.cityName = cityName;
        this.status = status;
    }
}
