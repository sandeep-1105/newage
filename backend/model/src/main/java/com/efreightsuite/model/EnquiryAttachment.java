package com.efreightsuite.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.UniqueConstraint;
import javax.persistence.Version;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;


@Entity
@Table(name = "efs_enquiry_attachment", uniqueConstraints = {
        @UniqueConstraint(columnNames = {"ref_no", "enquiry_detail_id"}, name = "UK_ENQUIRYATT_REF")
})
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Setter
@Getter

public class EnquiryAttachment implements Serializable {

    private static final long serialVersionUID = 1L;

    /* Id */
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    /* Version Lock */
    @Version
    @Column(name = "version_lock")
    long versionLock;

    /* Save or Update DateTime information */
    @Embedded
    SystemTrack systemTrack;

    /* EnquiryDetail */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "enquiry_detail_id", foreignKey = @ForeignKey(name = "FK_ENQUIRYATTACH_ENQDETID"))
    @JsonBackReference
    EnquiryDetail enquiryDetail;

    /* Reference Number */
    @Column(name = "ref_no", length = 100)
    String refNo;

    /* File Name */
    @Column(name = "file_name", length = 100)
    String fileName;

    /* File Content Type */
    @Column(name = "file_content_type", length = 100)
    String fileContentType;

    /* File */
    @Column(name = "file_attached")
    @Lob
    byte[] file;

}
