package com.efreightsuite.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Transient;
import javax.persistence.Version;

import com.efreightsuite.enumeration.YesNo;
import com.efreightsuite.util.JsonDateDeserializer;
import com.efreightsuite.util.JsonDateSerializer;
import com.efreightsuite.util.JsonYesNoDeserializer;
import com.efreightsuite.util.JsonYesNoSerializer;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.envers.Audited;
import org.hibernate.envers.RelationTargetAuditMode;


@Entity
@Table(name = "efs_consol_signoff")
@Getter
@Setter
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Audited
public class ConsolSignOff implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    public Long id;

    @Version
    @Column(name = "version_lock")
    public long versionLock;

    @Embedded
    public SystemTrack systemTrack;

    @Column(name = "consol_uid")
    public String consolUid;

    @Column(name = "shipment_uid")
    public String shipmentUid;

    @Column(name = "service_uid")
    public String serviceUid;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "signoff_by", foreignKey = @ForeignKey(name = "FK_CONSOLSIGNOFF_BY"))
    @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
    public EmployeeMaster signOffBy;

    @Column(name = "signoff_by_code")
    public String signOffByCode;

    @Column(name = "signoff_date")
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    public Date signOffDate;

    @Column(name = "is_sign_off", length = 10)
    @Enumerated(EnumType.STRING)
    @JsonSerialize(using = JsonYesNoSerializer.class)
    @JsonDeserialize(using = JsonYesNoDeserializer.class)
    public YesNo isSignOff;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "comment_id", foreignKey = @ForeignKey(name = "FK_CONSOLSIGNOFF_COMMENT"))
    @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
    public CommentMaster commentMaster;

    @Column(name = "comment_code")
    public String commentMasterCode;

    @Column(name = "description", length = 4000)
    public String description;

    @Transient
    String unSuccessfull;
}
