package com.efreightsuite.model.common;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.UniqueConstraint;

import com.efreightsuite.enumeration.SequenceFormat;
import com.efreightsuite.enumeration.SequenceType;
import com.efreightsuite.enumeration.YesNo;
import com.efreightsuite.util.JsonYesNoDeserializer;
import com.efreightsuite.util.JsonYesNoSerializer;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Table(name = "efs_location_setup_sequence", uniqueConstraints = {
        @UniqueConstraint(columnNames = {"sequence_type", "location_setup_id"}, name = "UK_COM_SEQUENCE_TYPE"),
        @UniqueConstraint(columnNames = {"prefix", "location_setup_id"}, name = "UK_COM_SEQUENCE_PREFIX")})
public class LocationSetupSequence implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    @ManyToOne
    @JoinColumn(name = "location_setup_id", foreignKey = @ForeignKey(name = "FK_LOCSETSEQ_LOCSETUPID"))
    @JsonBackReference
    LocationSetup locationSetup;

    @Column(name = "sequence_name", length = 50)
    String sequenceName;

    @Column(name = "sequence_type", length = 30)
    @Enumerated(EnumType.STRING)
    SequenceType sequenceType;

    @Column(name = "current_sequence_value")
    Long currentSequenceValue;

    @Column(name = "prefix", length = 5)
    String prefix;

    @Column(name = "format")
    @Enumerated(EnumType.STRING)
    SequenceFormat format;

    @Column(name = "is_year", length = 10)
    @Enumerated(EnumType.STRING)
    @JsonSerialize(using = JsonYesNoSerializer.class)
    @JsonDeserialize(using = JsonYesNoDeserializer.class)
    YesNo isYear = YesNo.No;

    @Column(name = "is_month", length = 10)
    @Enumerated(EnumType.STRING)
    @JsonSerialize(using = JsonYesNoSerializer.class)
    @JsonDeserialize(using = JsonYesNoDeserializer.class)
    YesNo isMonth = YesNo.No;

    @Column(name = "suffix", length = 5)
    String suffix;

    @Column(name = "separator", length = 1)
    String separator;
}
