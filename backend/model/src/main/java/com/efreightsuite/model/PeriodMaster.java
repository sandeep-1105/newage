package com.efreightsuite.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.UniqueConstraint;
import javax.persistence.Version;

import com.efreightsuite.enumeration.LovStatus;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;


@Entity
@Getter
@Setter
@Table(name = "efs_period_master",
        uniqueConstraints = {
                @UniqueConstraint(columnNames = {"frequency_code"}, name = "UK_PERIOD_FREQUENCYCODE"),
                @UniqueConstraint(columnNames = {"frequency_name"}, name = "UK_PERIOD_FREQUENCYNAME")}

)
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class PeriodMaster implements Serializable {

    private static final long serialVersionUID = 1L;

    /* Auto Generation Id */
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    @Version
    @Column(name = "version_lock")
    long versionLock;

    /*Create/LastUpdated User and Date Info*/
    @Embedded
    SystemTrack systemTrack;

    /* Period Name */
    @Column(name = "frequency_name", length = 100)
    String frequencyName;

    /* Quotation No */
    @Column(name = "frequency_code", length = 10)
    String frequencyCode;

    @Column(name = "no_of_day")
    Long noOfDay;

    /* Lov Status */
    @Column(name = "status", nullable = false, length = 10)
    @Enumerated(EnumType.STRING)
    LovStatus status;
}
