package com.efreightsuite.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.*;

import com.efreightsuite.enumeration.AWB;
import com.efreightsuite.enumeration.DimensionUnit;
import com.efreightsuite.enumeration.ImportExport;
import com.efreightsuite.enumeration.RateClass;
import com.efreightsuite.enumeration.ServiceCode;
import com.efreightsuite.enumeration.YesNo;
import com.efreightsuite.util.JsonDateDeserializer;
import com.efreightsuite.util.JsonDateSerializer;
import com.efreightsuite.util.JsonYesNoDeserializer;
import com.efreightsuite.util.JsonYesNoSerializer;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Type;


@Entity
@Table(name = "efs_consol_document", uniqueConstraints = {
        @UniqueConstraint(columnNames = {"document_no"}, name = "UK_CONDOCUMENT_DOCUMENTNO")},
        indexes = {@Index(columnList = "mawb_no", name = "IDX_CONSOLDOC_MAWB")})
@Getter
@Setter
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class ConsolDocument implements Serializable {
    private static final long serialVersionUID = 1L;

	/* Auto Generation Id */

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    @Version
    @Column(name = "version_lock")
    long versionLock;

    /* Create/Last User and Data Info */
    @Embedded
    SystemTrack systemTrack;

    @Column(name = "is_agreed", nullable = false)
    @Enumerated(EnumType.STRING)
    @JsonSerialize(using = JsonYesNoSerializer.class)
    @JsonDeserialize(using = JsonYesNoDeserializer.class)
    YesNo isAgreed = YesNo.No;


    @Column(name = "is_marks_no", nullable = false)
    @Enumerated(EnumType.STRING)
    @JsonSerialize(using = JsonYesNoSerializer.class)
    @JsonDeserialize(using = JsonYesNoDeserializer.class)
    YesNo isMarksNo = YesNo.Yes;


    /* Document Req Date */
    @Column(name = "document_req_date", nullable = false)
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date documentReqDate;


    @Column(name = "consol_uid", nullable = false)
    String consolUid;


    @Column(name = "document_no", nullable = false)
    String documentNo;


    /* Shipper id */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "shipper_id", foreignKey = @ForeignKey(name = "FK_CONSLDOC_SHIPPERID"))
    PartyMaster shipper;

    /* Shipper Code */
    @Column(name = "shipper_code")
    String shipperCode;

    /* shipperAddress */
    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "shipper_address_id", foreignKey = @ForeignKey(name = "FK_CONSLDOC_SHIPPERADDID"))
    AddressMaster shipperAddress;

    /* Shipper Manifest Name */
    @Column(name = "shipper_manifest_name")
    String shipperManifestName;


    /* Forwarder id */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "forwarder_id", foreignKey = @ForeignKey(name = "FK_CONSLDOC_FORWARDID"))
    PartyMaster forwarder;

    /* Forwarder Code */
    @Column(name = "forwarder_code")
    String forwarderCode;

    /* Forwarder Manifest Name */
    @Column(name = "forwarder_manifest_name")
    String forwarderManifestName;

    /* Forward Address */
    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "forwarder_address_id", foreignKey = @ForeignKey(name = "FK_CONSLDOC_FORWARDADDID"))
    AddressMaster forwarderAddress;


    /* Consignee id */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "consignee_id", foreignKey = @ForeignKey(name = "FK_CONSLDOC_CONSIGNEEID"))
    PartyMaster consignee;

    /* Consignee Code */
    @Column(name = "consignee_code")
    String consigneeCode;

    /* Consignee Manifest Name */
    @Column(name = "consignee_manifest_name")
    String consigneeManifestName;

    /* Forward Address */
    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "consignee_address_id", foreignKey = @ForeignKey(name = "FK_CONSLDOC_CONSIGNADDID"))
    AddressMaster consigneeAddress;

    /* First Notify id */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "first_notify_id", foreignKey = @ForeignKey(name = "FK_CONSLDOC_FIRSTNOTIFYID"))
    PartyMaster firstNotify;

    /* First Notify Code */
    @Column(name = "first_notify_code")
    String firstNotifyCode;

    /* First Notify Manifest Name */
    @Column(name = "first_notify_manifest_name")
    String firstNotifyManifestName;

    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    /* First Notify Address */
    @JoinColumn(name = "first_notify_address_id", foreignKey = @ForeignKey(name = "FK_CONSLDOC_FIRNOTADDID"))
    AddressMaster firstNotifyAddress;


    /* Second Notify id */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "second_notify_id", foreignKey = @ForeignKey(name = "FK_CONSLDOC_SECONDNOTIFYID"))
    PartyMaster secondNotify;

    /* Second Notify Code */
    @Column(name = "second_notify_code")
    String secondNotifyCode;

    /* Second Notify Manifest Name */
    @Column(name = "second_notify_manifest_name")
    String secondNotifyManifestName;

    /* Second Notify Address */
    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "second_notify_address_id", foreignKey = @ForeignKey(name = "FK_CONSLDOC_SECNOTADDID"))
    AddressMaster secondNotifyAddress;


    /* Agent id */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "agent_id", foreignKey = @ForeignKey(name = "FK_CONSLDOC_AGENTID"))
    PartyMaster agent;

    /* Agent Code */
    @Column(name = "agent_code")
    String agentCode;

    /* Agent Manifest Name */
    @Column(name = "agent_manifest_name")
    String agentManifestName;

    /* Agent Address */
    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "agent_address_id", foreignKey = @ForeignKey(name = "FK_CONSLDOC_AGENTADDID"))
    AddressMaster agentAddress;


    /* Issuing Agent id */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "doc_issuing_agent_id", foreignKey = @ForeignKey(name = "FK_CONSLDOC_ISSAGENTID"))
    PartyMaster issuingAgent;

    /* Issuing Agent Code */
    @Column(name = "doc_issuing_agent_code")
    String issuingAgentCode;

    /* Document Issuing Agent Name */
    @Column(name = "doc_issuing_agent_name")
    String issuingAgentName;

    /* Issuing Address */
    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "doc_issuing_agent_add_id", foreignKey = @ForeignKey(name = "FK_CONSLDOC_ISSAGENTADDID"))
    AddressMaster issuingAgentAddress;

    /* Volume */
    @Column(name = "volume")
    Double volume;

    /* Pack Id */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "pack_id", foreignKey = @ForeignKey(name = "FK_CONSLDOC_PACKID"))
    PackMaster packMaster;

    /* Pack Code */
    @Column(name = "pack_code")
    String packCode;

    /* Pack Description */
    @Column(name = "pack_description", length = 400)
    String packDescription;

    /* Booking No Of Pack */
    @Column(name = "no_of_pieces")
    Long noOfPieces;


    /* Rate / Charge */
    @Column(name = "rate_per_charge")
    Double ratePerCharge;


    /* Dimension Unit */
    @Column(name = "dimension_unit", nullable = false, length = 50)
    @Enumerated(EnumType.STRING)
    DimensionUnit dimensionUnit;

    /* Dimension Unit Value */
    @Column(name = "dimension_unit_value")
    double dimensionUnitValue;


    /* chargeble weight */
    @Column(name = "chargeble_weight")
    Double chargebleWeight;

    /* gross weight */
    @Column(name = "gross_weight")
    Double grossWeight;

    /* volume weight */
    @Column(name = "volume_weight")
    Double volumeWeight;

    /*  Vol Weight in pound */
    @Column(name = "volume_weight_in_pound")
    Double volumeWeightInPound;

    /*  Gross Weight in pound*/
    @Column(name = "gross_weight_in_pound")
    Double grossWeightInPound;


    @Column(name = "dim_no_of_pieces")
    Long dimNoOfPieces;

    @Column(name = "dim_gross_weight")
    Double dimGrossWeight;

    @Column(name = "dim_gross_weight_in_pound")
    Double dimGrossWeightInPound;

    @Column(name = "dim_volume_weight")
    Double dimVolumeWeight;

    @Column(name = "dim_volume_weight_in_pound")
    Double dimVolumeWeightInPound;


    /* Handling Information */
    @Column(name = "handling_information", length = 400)
    String handlingInformation;

    /* Accounting Information */
    @Column(name = "accounting_information", length = 400)
    String accountingInformation;

    /* Commodity Description */
    @Column(name = "commodity_description", length = 400)
    String commodityDescription;

    /* Marks & No */
    @Column(name = "marks_and_no", length = 400)
    String marksAndNo;

    /* Note */
    @Column(name = "note", length = 400)
    String note;

    /* Rate Class */
    @Column(name = "rate_class", length = 2)
    @Enumerated(EnumType.STRING)
    RateClass rateClass;

    /* BL Received Person */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "bl_received_person_id", foreignKey = @ForeignKey(name = "FK_CONSLDOC_BLRECEMPID"))
    EmployeeMaster blReceivedPerson;

    /* BL Received Person Code */
    @Column(name = "bl_received_person_code", length = 30)
    String blReceivedPersonCode;

    /* BL Received Date */
    @Column(name = "bl_received_date")
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date blReceivedDate;

    /* Company Id */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "company_id", foreignKey = @ForeignKey(name = "FK_CONSLDOC_COMPANYID"))
    CompanyMaster company;

    @Column(name = "company_code", length = 100)
    String companyCode;

    /* Country id */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "country_id", foreignKey = @ForeignKey(name = "FK_CONSLDOC_COUNTRYID"))
    CountryMaster country;

    /* Country Code */
    @Column(name = "country_code", length = 10)
    String countryCode;

    /* Location Id */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "location_id", foreignKey = @ForeignKey(name = "FK_CONSLDOC_LOCATIONID"))
    LocationMaster location;

    @Column(name = "location_code", length = 100)
    String locationCode;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "doc_prefix_id", foreignKey = @ForeignKey(name = "FK_CONSLDOC_DOCPREFIXID"))
    DocumentPrefixMaster docPrefixMaster;

    @Column(name = "doc_prefix_code")
    String docPrefixCode;


    /* Doc Pickup & Delivery point id */
    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "pickup_delivery_id", foreignKey = @ForeignKey(name = "FK_CONSLDOC_PICKDELID"))
    PickUpDeliveryPoint pickUpDeliveryPoint;


    /* Service Code */
    @Column(name = "service_code")
    @Enumerated(EnumType.STRING)
    ServiceCode serviceCode;

    /* Booking Volume Unit id */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "booking_volume_unit_id", foreignKey = @ForeignKey(name = "FK_CONSLDOC_BOOKVOLUMEID"))
    UnitMaster bookingVolumeUnit;

    /* Booking Volume Unit Code */
    @Column(name = "booking_volume_unit_code")
    String bookingVolumeUnitCode;

    /* Booking Volume Unit Value */
    @Column(name = "booking_volume_unit_value")
    Double bookingVolumeUnitValue;

    /* Booking Volume */
    @Column(name = "booking_volume")
    Double bookingVolume;

    /* Booking Gross Weight Unit id */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "booking_gross_wt_unit_id", foreignKey = @ForeignKey(name = "FK_CONSLDOC_BOOKGROSSWGTID"))
    UnitMaster bookingGrossWeightUnit;

    /* Booking Gross Weight Unit Code */
    @Column(name = "booking_gross_wt_unit_code")
    String bookingGrossWeightUnitCode;

    /* Booking Gross Weight Unit Value */
    @Column(name = "booking_gross_wt_unit_value")
    Double bookingGrossWeightUnitValue;

    /* Booking Gross Weight */
    @Column(name = "booking_gross_weight")
    Double bookingGrossWeight;

    /* Booking Volume Weight Unit id */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "booking_volume_wt_unit_id", foreignKey = @ForeignKey(name = "FK_CONSLDOC_BOOKVOLWGTID"))
    UnitMaster bookingVolumeWeightUnit;

    /* Booking Volume Weight Unit Code */
    @Column(name = "booking_volume_wt_unit_code")
    String bookingVolumeWeightUnitCode;

    /* Booking Volume Weight Unit Value */
    @Column(name = "booking_volume_wt_unit_value")
    Double bookingVolumeWeightUnitValue;

    /* Booking Volume Weight Unit KG */
    @Column(name = "booking_volume_weight")
    Double bookingVolumeWeight;

    /* Booking Chargeable Unit */
    @Column(name = "booking_chargeable_unit")
    Double bookingChargeableUnit;

    /* Vessel Id */
    @Column(name = "vessel_id")
    Long vesselId;

    /* Vessel Code */
    @Column(name = "vessel_code")
    String vesselCode;

    /* Route No */
    @Column(name = "route_no")
    String routeNo;

    /* Transit Port */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "transit_port_id", foreignKey = @ForeignKey(name = "FK_CONSLDOC_TRANSITPORTID"))
    PortMaster transitPort;

    @Column(name = "transit_port")
    String transitPortCode;

    /* Transit ETA */
    @Column(name = "transit_eta")
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date transitEta;

    /* Transit Schedule UID */
    @Column(name = "transit_schedule_uid")
    String transitScheduleUid;


    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "mawb_generated_by", foreignKey = @ForeignKey(name = "FK_CONSLDOC_MAWBEMPID"))
    EmployeeMaster mawbGeneratedBy;

    @Column(name = "mawb_generated_by_code")
    String mawbGeneratedByCode;

    /* Master No */
    @Column(name = "mawb_no", length = 30)
    String mawbNo;

    /* Master Issue Date */
    @Column(name = "mawb_issue_date")
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date mawbIssueDate;

    /* Master No */
    @Column(name = "mawb_generated_date")
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date mawbGeneratedDate;


    // Added for Issue Improvement NES-956 Start

	/* AWB */

    @Column(name = "awb")
    @Enumerated(EnumType.STRING)
    AWB awb;

    /* AWB Printing at Destination Allowed */
    @Column(name = "awbdistprintallow")
    @Enumerated(EnumType.STRING)
    YesNo awbDistPrintAllow;

    /*Trade */
    @Column(name = "trade")
    @Enumerated(EnumType.STRING)
    ImportExport trade;

    // Added for Issue Improvement NES-956 End


    /* Commodity Id */
    @ManyToOne
    @JoinColumn(name = "commodity_id", foreignKey = @ForeignKey(name = "FK_CONSLDOC_COMMODITYID"))
    CommodityMaster commodity;

    /* Commodity Code */
    @Column(name = "commodity_code")
    String commodityCode;


    @OneToMany(mappedBy = "documentDetail", cascade = CascadeType.ALL, orphanRemoval = true)
    @JsonManagedReference
    List<ConsolDocumentDimension> dimensionList = new ArrayList<>();


    @OneToOne
    @JoinColumn(name = "document_sub_detail", foreignKey = @ForeignKey(name = "FK_CONSLDOC_DOCSUBDETAILID"))
    DocumentSubDetail documentSubDetail;


    //For Report -Dynamic Purpose -HAWB

    @Column(name = "declared_value_carriage")
    String declaredValueForCarriage = "NVD";

    @Column(name = "diversion_contray")
    String diversionContray = "SCI";


    @Column(name = "declared_value_customs")
    String declaredValueForCustoms = "NCV";


    @Column(name = "optional_information")
    String optionalShippingInfo;

    @Column(name = "amount_of_insurance")
    String amountOfInsurance = "NIL";

    @Column(name = "chargescode")
    String chgsCode;

    @Column(name = "wt_ValPpd")
    String wtValPrepaid;

    @Column(name = "wt_ValCol")
    String wtValCollect;


    @Column(name = "others_prepaid")
    String othersPrepaid;

    @Column(name = "others_collect")
    String othersCollect;


    @Column(name = "pre_weight_charge")
    String preWeightCharge;

    @Column(name = "coll_weight_charge")
    String collWeightCharge;

    @Column(name = "pre_valuation_charge")
    String preValuationCharge;


    @Column(name = "coll_valuation_charge")
    String collValuationCharge;


    @Column(name = "pre_tax")
    String prepaidTax;

    @Column(name = "coll_tax")
    String collectTax;

    @Column(name = "othercharges_pre_agent")
    String otherChargesDuePreAgent;


    @Column(name = "othercharges_coll_agent")
    String otherChargesDueCollAgent;


    @Column(name = "othercharges_pre_carrier")
    String otherChargesDuePreCarrier;


    @Column(name = "othercharges_coll_carrier")
    String otherChargesDueCollCarrier;

    @Column(name = "total_prepaid")
    String totalPrepaid;

    @Column(name = "total_collect")
    String totalCollect;

    @Column(name = "total_charges")
    String totalCollectCharges;

    @Column(name = "currency_conversion_rates")
    String currencyConversionRates;

    @Column(name = "charges_dest_currency")
    String chargesInDestCurrency;

    @Column(name = "charges_at_destination")
    String chargesAtDestination;

    @Column(name = "pan_no")
    String panNo;

    @Column(name = "shipper_sign")
    String shipperSignature;

    @Column(name = "agent_sign")
    String issCarrierAgentSign;

    @Column(name = "optional_ship_info")
    String optShipInfo;

    @Column(name = "commodity_item_no")
    String commodityNo;

    @Column(name = "master_do_no")
    String masterDoNo;

    @Column(name = "carrier_do_no")
    String carrierDoNo;


    @Column(name = "to_address")
    String toAddress;

    /* igm_no */
    @Column(name = "igm_no", length = 30)
    String igmNo;


    /**
     * Consol Import Document
     * Other Info Section
     */

    @Column(name = "is_hold", length = 10)
    @Enumerated(EnumType.STRING)
    @JsonSerialize(using = JsonYesNoSerializer.class)
    @JsonDeserialize(using = JsonYesNoDeserializer.class)
    YesNo isHold = YesNo.No;

    @Column(name = "hold_release_number", length = 30)
    String holdReleaseNumber;

    @Column(name = "hold_note", length = 400)
    String holdNote;

    /*Item No*/
    @Column(name = "item_no", length = 30)
    String itemNo;

    /*holdReleasenote */
    @Column(name = "hold_release_note")
    @Lob
    @Type(type = "org.hibernate.type.MaterializedClobType")
    String holdReleasenote;


}
