package com.efreightsuite.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Version;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;


@Entity
@Getter
@Setter
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Table(name = "efs_email")
public class Email implements Serializable {
    private static final long serialVersionUID = 1L;

    /*Auto Generation Id*/
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    @Version
    @Column(name = "version_lock")
    long versionLock;

    /*Create/Last User and Data Info*/
    @Embedded
    @JsonIgnore
    SystemTrack systemTrack;

    /*Location Id*/
    @ManyToOne
    @JoinColumn(name = "location_id", foreignKey = @ForeignKey(name = "FK_EMAIL_LOCATIONID"))
    LocationMaster locationMaster;

    /*party Id*/
    @ManyToOne
    @JoinColumn(name = "party_id", foreignKey = @ForeignKey(name = "FK_EMAIL_PARTYID"))
    PartyMaster partyMaster;

    /*Employee Id*/
    @ManyToOne
    @JoinColumn(name = "employee_id", foreignKey = @ForeignKey(name = "FK_EMAIL_EMPLOYEEID"))
    EmployeeMaster employeeMaster;

    /*Receiver Code*/
    @Column(name = "receiver_code")
    String receiverCode;

    /*email*/
    @Column(name = "email")
    String email;

    /*Origin Country Id*/
    @ManyToOne
    @JoinColumn(name = "origin_country_id", foreignKey = @ForeignKey(name = "FK_EMAIL_ORIGINCOUNTRYID"))
    CountryMaster origin;

    /*Origin Country*/
    @Column(name = "origin_country", length = 10)
    String originName;

    /*Destination Country id*/
    @ManyToOne
    @JoinColumn(name = "destination_country_id", foreignKey = @ForeignKey(name = "FK_EMAIL_DESTICOUNTRYID"))
    CountryMaster destination;

    /*Destination Country*/
    @Column(name = "destination_country", length = 10)
    String destinationName;

    /*IMCO Code*/
    @Column(name = "imco")
    String imco;


}
