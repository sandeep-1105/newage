package com.efreightsuite.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Version;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;


@Entity
@Getter
@Setter
@Table(name = "efs_service_status")/*, uniqueConstraints = {
        @UniqueConstraint(columnNames = { "shipment_service_id","service_status" }, name = "UK_SERVICESTATUS_STATUS") }
)
 */
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class ServiceStatus implements Serializable {
    private static final long serialVersionUID = 1L;

    /*Auto Generation Id*/
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    @Version
    @Column(name = "version_lock")
    long versionLock;

    /*Create/Last User and Data Info*/
    @Embedded
    SystemTrack systemTrack;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "shipment_service_id", foreignKey = @ForeignKey(name = "FK_SERVSTATUS_SHIPMENTSERVID"))
    @JsonBackReference
    ShipmentServiceDetail shipmentServiceDetail;

    /*Shipment Status*/
    @Column(name = "service_status", nullable = false, length = 30)
    @Enumerated(EnumType.STRING)
    com.efreightsuite.enumeration.ServiceStatus serviceStatus;
}
