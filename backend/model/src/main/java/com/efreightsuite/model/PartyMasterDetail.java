package com.efreightsuite.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.UniqueConstraint;
import javax.persistence.Version;

import com.efreightsuite.enumeration.BooleanType;
import com.efreightsuite.enumeration.CorporateNonCorporate;
import com.efreightsuite.enumeration.EmailFormat;
import com.efreightsuite.util.JsonBooleanTypeDeSerializer;
import com.efreightsuite.util.JsonBooleanTypeSerializer;
import com.efreightsuite.util.JsonEmailFormatDeserializer;
import com.efreightsuite.util.JsonEmailFormatSerializer;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Getter;
import lombok.Setter;


@Entity
@Table(name = "efs_party_master_detail", uniqueConstraints = {
        @UniqueConstraint(columnNames = {"value_added_tax_no"}, name = "UK_PARTYDETAIL_VALUEADDEDTAX"),
        @UniqueConstraint(columnNames = {"pan_no"}, name = "UK_PARTYDETAIL_PAN"),
        @UniqueConstraint(columnNames = {"rac_no"}, name = "UK_PARTYDETAIL_RACNO"),
        @UniqueConstraint(columnNames = {"cst_no"}, name = "UK_PARTYDETAIL_CSTNO"),
        @UniqueConstraint(columnNames = {"svt_no"}, name = "UK_PARTYDETAIL_SVTNO"),
        @UniqueConstraint(columnNames = {"svat_no"}, name = "UK_PARTYDETAIL_SVATNO"),
        @UniqueConstraint(columnNames = {"tin_no"}, name = "UK_PARTYDETAIL_TINNO"),
        @UniqueConstraint(columnNames = {"iata_code"}, name = "UK_PARTYDETAIL_IATA_CODE"),
        @UniqueConstraint(columnNames = {"gst_no"}, name = "UK_PARTYDETAIL_GSTNO")

}

)
@Getter
@Setter
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class PartyMasterDetail implements Serializable {
    private static final long serialVersionUID = 1L;

    /* Auto Generation Id */
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    @Version
    @Column(name = "version_lock")
    long versionLock;

    /* Create/Last User and Data Info */
    @Embedded
    SystemTrack systemTrack;

    /* NLS Customer Name */
    @Column(name = "nls_customer_name")
    String nlsCustomerName;

    /* Corporate Non Corporate */
    @Column(name = "corporate_non_corporate", nullable = false)
    @Enumerated(EnumType.STRING)
    CorporateNonCorporate corporate;

    /* Value Added Tax No */
    @Column(name = "value_added_tax_no")
    String valueAddedTaxNo;

    /* PAN No */
    @Column(name = "pan_no")
    String panNo;

    /* GST No */
    @Column(name = "gst_no", length = 100)
    String gstNo;

    /* RAC No */
    @Column(name = "rac_no")
    String racNo;

    /* CST No */
    @Column(name = "cst_no")
    String cstNo;

    /* SVT No */
    @Column(name = "svt_no")
    String svtNo;

    /* SVAT No */
    @Column(name = "svat_no")
    String svatNo;

    /* TIN No */
    @Column(name = "tin_no")
    String tinNo;

    /* PIN NO */
    @Column(name = "personal_idenfication_no", length = 10)
    String pin;

    /* TSA NO */
    @Column(name = "tsa_no", length = 100)
    String tsaNo;

    /* Spot NO */
    @Column(name = "spot_no", length = 100)
    String spotNo;


    /* IVA Code */
    @Column(name = "iva_code")
    String ivaCode;

    /* IATA Code */
    @Column(name = "iata_code")
    String iataCode;

    // Quote Output
    @Column(name = "match_invoice_quote_output")
    @Enumerated(EnumType.STRING)
    @JsonSerialize(using = JsonBooleanTypeSerializer.class)
    @JsonDeserialize(using = JsonBooleanTypeDeSerializer.class)
    BooleanType matchInvoiceQuoteOutput;

    // Direct Status Update
    @Column(name = "direct_status_update")
    @Enumerated(EnumType.STRING)
    @JsonSerialize(using = JsonBooleanTypeSerializer.class)
    @JsonDeserialize(using = JsonBooleanTypeDeSerializer.class)
    BooleanType directStatusUpdate;

    /* Is blanket Nomination */
    @Column(name = "is_blanket_nomination", length = 5)
    @Enumerated(EnumType.STRING)
    @JsonSerialize(using = JsonBooleanTypeSerializer.class)
    @JsonDeserialize(using = JsonBooleanTypeDeSerializer.class)
    BooleanType isBlanketNomination;

    /*Frequency*/
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "period_id", foreignKey = @ForeignKey(name = "FK_PARTY_PERIODID"))
    PeriodMaster paymentSchedule;

    @Column(name = "period_code", length = 100)
    String paymentScheduleCode;

    /* CAF Percentage */
    @Column(name = "caf_percentage")
    Double cafPercentage;

    /* CC Percentage */
    @Column(name = "cc_percentage")
    Double ccPercentage;

    /* Billing FSC SC ON Gross Wight */
    @Column(name = "billing_fsc_sc_on_gross_wt")
    @Enumerated(EnumType.STRING)
    @JsonSerialize(using = JsonBooleanTypeSerializer.class)
    @JsonDeserialize(using = JsonBooleanTypeDeSerializer.class)
    BooleanType billingFscScOnGrossWeight;

    // TDS Percentage
    @Column(name = "tds_percentage")
    double tdsPercentage;

    /* Email Message Format */
    @Column(name = "email_message_format")
    @Enumerated(EnumType.STRING)
    @JsonSerialize(using = JsonEmailFormatSerializer.class)
    @JsonDeserialize(using = JsonEmailFormatDeserializer.class)
    EmailFormat emailMessageFormat;

    /* Status Update Email Export */
    @Column(name = "status_update_email_export")
    String statusUpdateEmailExport;

    /* Status Update Email Import */
    @Column(name = "status_update_email_import")
    String statusUpdateEmailImport;

    /* Export Sailing Schedule Email */
    @Column(name = "sailing_schedule_email")
    String sailingScheduleEmail;

    /* Export Sailing Schedule Fax */
    @Column(name = "sailing_schedule_fax")
    String sailingScheduleFax;

    /* Auto Outstanding Email */
    @Column(name = "auto_outstanding_email")
    String autoOutstandingEmail;

    /* Transhipment Email */
    @Column(name = "transhipment_email")
    String transhipmentEmail;

    /* Import Sailing Schedule Email */
    @Column(name = "import_sailing_schedule_email")
    String importSailingScheduleEmail;

    /* Agent EDI Email */
    @Column(name = "agent_edi_email")
    String AgentEdiEmail;

    /* Customer ECC Number */
    @Column(name = "customer_ecc_number")
    String customerEccNumber;

    /* RA Number */
    @Column(name = "account_number")
    String accountNumber;

}
