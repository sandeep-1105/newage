package com.efreightsuite.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Version;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;


@Entity
@Getter
@Setter
@Table(name = "efs_airline_edi_dimension")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class AirlineEdiDimension implements Serializable {

    private static final long serialVersionUID = 1L;

    /* Auto Generation Id */
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    @Version
    @Column(name = "version_lock")
    long versionLock;

    /* Create/Last User and Data Info */
    @Embedded
    SystemTrack systemTrack;

    /* Shipment Service Detail */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "airline_edi_id", foreignKey = @ForeignKey(name = "FK_EDI_DIMENSION_EDI_ID"))
    @JsonBackReference
    AirlineEdi airlineEdi;

	/* Login Location Information */

    /* Location */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "location_id", foreignKey = @ForeignKey(name = "FK_AIR_EDI_DIM_LOCATIONID"))
    LocationMaster locationMaster;

    @Column(name = "location_code", length = 30)
    String locationCode;

    /* Company */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "company_id", foreignKey = @ForeignKey(name = "FK_AIR_EDI_DIM_COMPANYID"))
    CompanyMaster companyMaster;

    @Column(name = "company_code", length = 30)
    String companyCode;

    /* Country */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "country_id", foreignKey = @ForeignKey(name = "FK_AIR_EDI_DIM_COUNTRYID"))
    CountryMaster countryMaster;

    @Column(name = "country_code", length = 30)
    String countryCode;

    /* No. of Pieces */
    @Column(name = "no_of_pieces")
    Integer noOfPiece;

    /*length*/
    @Column(name = "length")
    Integer length;

    /*width*/
    @Column(name = "width")
    Integer width;

    /*height*/
    @Column(name = "height")
    Integer height;

    /*Volume Weight(kg) */
    @Column(name = "volume_weight")
    Double volWeight;

    /* Gross Weight (kg)*/
    @Column(name = "gross_weight")
    Double grossWeight;

    @Column(name = "gross_weight_in_kg")
    Double grossWeightKg = 0.0;
}
