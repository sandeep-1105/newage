package com.efreightsuite.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Version;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;


@Entity
@Getter
@Setter
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Table(name = "efs_airline_prebook_sched")
public class AirlinePrebookingSchedule implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    @Version
    @Column(name = "version_lock")
    long versionLock;

    /* Create/Last User and Data Info */
    @Embedded
    SystemTrack systemTrack;


    /* Shipment Service Detail */
    @ManyToOne
    @JoinColumn(name = "airline_prebooking_id", nullable = false, foreignKey = @ForeignKey(name = "FK_AIRPRESCHM_AIRPREID"))
    @JsonBackReference
    AirlinePrebooking airlinePrebooking;

    @ManyToOne
    @JoinColumn(name = "flight_plan_id", nullable = false, foreignKey = @ForeignKey(name = "FK_AIRPRESCHM_FLIID"))
    FlightPlan flightPlan;

    @Column(name = "mawb_no")
    String mawbNo;

    /* No. of Pieces */
    @Column(name = "no_of_piece")
    int noOfPiece;

    /* Gross Weight (kg) */
    @Column(name = "gross_weight")
    double grossWeight;

    /* Vol Weight(kg) */
    @Column(name = "vol_weight")
    double volWeight;

    /* Vol Weight(kg) */
    @Column(name = "volume")
    double volume;

    @Column(name = "status")
    String status;
}
