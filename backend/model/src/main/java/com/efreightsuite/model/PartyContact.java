package com.efreightsuite.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Version;

import com.efreightsuite.enumeration.LovStatus;
import com.efreightsuite.enumeration.SalutationType;
import com.efreightsuite.enumeration.YesNo;
import com.efreightsuite.util.JsonYesNoDeserializer;
import com.efreightsuite.util.JsonYesNoSerializer;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Getter;
import lombok.Setter;


@Entity
@Getter
@Setter
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Table(name = "efs_party_contact")
public class PartyContact implements Serializable {
    private static final long serialVersionUID = 1L;

    /*Auto Generation Id*/
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    @Version
    @Column(name = "version_lock")
    long versionLock;

    @Embedded
    SystemTrack systemTrack;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "party_id", foreignKey = @ForeignKey(name = "FK_PARTYCONT_PARTYID"))
    @JsonBackReference
    PartyMaster partyMaster;


    @Column(name = "is_favourite", length = 10, nullable = false)
    @Enumerated(EnumType.STRING)
    @JsonSerialize(using = JsonYesNoSerializer.class)
    @JsonDeserialize(using = JsonYesNoDeserializer.class)
    YesNo isFavourite;

    @Column(name = "salutation", length = 10)
    @Enumerated(EnumType.STRING)
    SalutationType salutationType;

    @Column(name = "first_name", length = 100)
    String firstName;

    @Column(name = "last_name", length = 100)
    String lastName;

    @Column(name = "status", length = 10, nullable = false)
    @Enumerated(EnumType.STRING)
    LovStatus status;

    @Column(name = "is_call", length = 10, nullable = false)
    @Enumerated(EnumType.STRING)
    @JsonSerialize(using = JsonYesNoSerializer.class)
    @JsonDeserialize(using = JsonYesNoDeserializer.class)
    YesNo isCall;

    @Column(name = "is_send_mail", length = 10, nullable = false)
    @Enumerated(EnumType.STRING)
    @JsonSerialize(using = JsonYesNoSerializer.class)
    @JsonDeserialize(using = JsonYesNoDeserializer.class)
    YesNo isSendMail;

    @Column(name = "is_primary", length = 10, nullable = false)
    @Enumerated(EnumType.STRING)
    @JsonSerialize(using = JsonYesNoSerializer.class)
    @JsonDeserialize(using = JsonYesNoDeserializer.class)
    YesNo isPrimary;

    @Column(name = "designation", length = 100)
    String designation;

    @Column(name = "department", length = 100)
    String department;

    @Column(name = "offical_address", length = 255)
    String officalAddress;

    @Column(name = "offical_phone", length = 100)
    String officalPhone;

    @Column(name = "offical_mobile", length = 100)
    String officalMobile;

    @Column(name = "offical_fax", length = 100)
    String officalFax;

    @Column(name = "offical_email", length = 500)
    String officalEmail;

    @Column(name = "assistant_name", length = 100)
    String assistantName;

    @Column(name = "assistant_phone", length = 100)
    String assistantPhone;

    @Column(name = "preferred_call_time", length = 100)
    String preferredCallTime;

    @Column(name = "reporting_to", length = 100)
    String reportingTo;

    @Column(name = "lead_id")
    Long lead;

    @Column(name = "engaged_us_previous", length = 10, nullable = false)
    @Enumerated(EnumType.STRING)
    @JsonSerialize(using = JsonYesNoSerializer.class)
    @JsonDeserialize(using = JsonYesNoDeserializer.class)
    YesNo engagedUsPrevious;

    @Column(name = "personal_address", length = 255)
    String personalAddress;

    @Column(name = "personal_phone", length = 100)
    String personalPhone;

    @Column(name = "personal_mobile", length = 100)
    String personalMobile;

    @Column(name = "personal_fax", length = 100)
    String personalFax;

    @Column(name = "personal_email", length = 500)
    String personalEmail;

    @Column(name = "former_company", length = 100)
    String formerCompany;

    @Column(name = "qualification", length = 50)
    String qualification;

    @Column(name = "is_married", length = 10, nullable = false)
    @Enumerated(EnumType.STRING)
    @JsonSerialize(using = JsonYesNoSerializer.class)
    @JsonDeserialize(using = JsonYesNoDeserializer.class)
    YesNo isMarried;

    @Column(name = "no_of_children", length = 5)
    String noOfChildren;

    @Column(name = "note", length = 4000)
    String note;


    @OneToMany(mappedBy = "partyContact", cascade = CascadeType.ALL, orphanRemoval = true)
    @JsonManagedReference
    List<PartyContactRelation> relationList = new ArrayList<>();


    public PartyMaster getPartyMaster() {

        if (this.partyMaster == null)
            return null;

        PartyMaster tm = new PartyMaster();
        tm.setId(partyMaster.getId());
        tm.setPartyCode(partyMaster.getPartyCode());
        tm.setPartyName(partyMaster.getPartyName());
        return tm;
    }
}
