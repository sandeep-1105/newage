package com.efreightsuite.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.UniqueConstraint;
import javax.persistence.Version;

import com.efreightsuite.util.JsonDateDeserializer;
import com.efreightsuite.util.JsonDateSerializer;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Getter;
import lombok.Setter;


@Entity
@Table(name = "efs_flight_plan_connection", uniqueConstraints = {
        @UniqueConstraint(columnNames = {"carrier_id", "pol_id", "pod_id", "etd", "eta"}, name = "UK_FLIGHTP_FLIGHTCON")
})
@Getter
@Setter
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class FlightPlanConnection implements Serializable, Cloneable {
    private static final long serialVersionUID = 1L;

    /* Auto Generation Id */
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    @Version
    @Column(name = "version_lock")
    long versionLock;

    /* Create/Last User and Data Info */
    @Embedded
    SystemTrack systemTrack;

    /* FlightPlan id */
    @ManyToOne
    @JoinColumn(name = "flight_plan_id", foreignKey = @ForeignKey(name = "FK_FLIGHTPCONN_FLIGHTP"))
    @JsonBackReference
    FlightPlan flightPlan;

    /* Carrier Id */
    @ManyToOne
    @JoinColumn(name = "carrier_id", foreignKey = @ForeignKey(name = "FK_FLIGHTPCONN_CARRIERID"))
    CarrierMaster carrierMaster;

	/* Flight */

    @Column(name = "flight_no")
    String flightNo;

    /* POL */
    @ManyToOne
    @JoinColumn(name = "pol_id", foreignKey = @ForeignKey(name = "FK_FLIGHTPCONN_POLID"))
    PortMaster pol;

    /* POD */
    @ManyToOne
    @JoinColumn(name = "pod_id", foreignKey = @ForeignKey(name = "FK_FLIGHTPCONN_PODID"))
    PortMaster pod;

    /* ETD */
    @Column(name = "etd")
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date etd;

    /* ETA */
    @Column(name = "eta")
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date eta;

    public FlightPlanConnection(CarrierMaster carrierMaster, String flightNo, PortMaster pol, PortMaster pod, Date etd,
                                Date eta) {
        super();
        this.carrierMaster = carrierMaster;
        this.flightNo = flightNo;
        this.pol = pol;
        this.pod = pod;
        this.etd = etd;
        this.eta = eta;
    }

    public FlightPlanConnection() {

    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}

