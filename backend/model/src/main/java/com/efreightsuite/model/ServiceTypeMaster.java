package com.efreightsuite.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.UniqueConstraint;
import javax.persistence.Version;

import com.efreightsuite.enumeration.LovStatus;
import com.efreightsuite.enumeration.ServiceType;
import com.efreightsuite.enumeration.TransportMode;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;


@Entity
@Table(name = "efs_service_type_master", uniqueConstraints = {
        @UniqueConstraint(columnNames = {"service_type_code"}, name = "UK_SERVICETYPE_SERVICECODE"),
        @UniqueConstraint(columnNames = {"service_type_name"}, name = "UK_SERVICETYPE_SERVICENAME")}
)
@Getter
@Setter
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class ServiceTypeMaster implements Serializable {


    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    @Version
    @Column(name = "version_lock")
    long versionLock;

    @Embedded
    SystemTrack systemTrack;

    @Column(name = "service_type_code", nullable = false, length = 20)
    String serviceTypeCode;

    @Column(name = "service_type_name", nullable = false, length = 100)
    String serviceTypeName;

    @Column(name = "service_type", length = 30)
    @Enumerated(EnumType.STRING)
    ServiceType serviceType;

    @Column(name = "status", nullable = false, length = 10)
    @Enumerated(EnumType.STRING)
    LovStatus status;

    /* Transport Mode */
    @Column(name = "transport_mode", length = 10)
    @Enumerated(EnumType.STRING)
    TransportMode transportMode;

}
