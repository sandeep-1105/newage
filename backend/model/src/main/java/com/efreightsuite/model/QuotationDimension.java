package com.efreightsuite.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Version;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;


@Entity
@Getter
@Setter
@Table(name = "efs_quotation_dimension")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class QuotationDimension implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /* Auto Generation Id */
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    @Version
    @Column(name = "version_lock")
    long versionLock;

    @Embedded
    SystemTrack systemTrack;

    /* QuotationDetail */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "quotation_detail_id", foreignKey = @ForeignKey(name = "FK_QUOTATIONDIMENSION_ID"))
    @JsonBackReference
    QuotationDetail QuotationDetail;

    /* No. of Pieces */
    @Column(name = "no_of_piece")
    int noOfPiece;

    /*legth*/
    @Column(name = "length")
    int length;

    /*width*/
    @Column(name = "width")
    int width;

    /*hight*/
    @Column(name = "height")
    int height;

    /* Vol Weight(kg) */
    @Column(name = "vol_weight")
    double volWeight;

    /* Gross Weight (kg)*/
    @Column(name = "gross_weight")
    double grossWeight;

    /* Gross Weight in kg*/
    @Column(name = "gross_weight_in_kg")
    double grossWeightKg = 0.0;

}
