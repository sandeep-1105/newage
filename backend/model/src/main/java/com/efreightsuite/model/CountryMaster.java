package com.efreightsuite.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;
import javax.persistence.Version;

import com.efreightsuite.enumeration.LovStatus;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;


@Entity
@Getter
@Setter
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Table(name = "efs_country_master", uniqueConstraints = {
        @UniqueConstraint(columnNames = {"country_code"}, name = "UK_COUNTRY_COUNTRYCODE"),
        @UniqueConstraint(columnNames = {"country_name"}, name = "UK_COUNTRY_COUNTRYNAME")})
public class CountryMaster implements Serializable {

    private static final long serialVersionUID = 1L;

    /* Auto Generation Id */
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    @Version
    @Column(name = "version_lock")
    long versionLock;

    /* Create/Last User and Data Info */
    @Embedded
    SystemTrack systemTrack;


    /* Country Code */
    @Column(name = "country_code", nullable = false, length = 10)
    String countryCode;

    /* Country Name */
    @Column(name = "country_name", nullable = false, length = 100)
    String countryName;

    /*Locale code*/
    @Column(name = "locale", length = 10)
    String locale;


    /*Currency id*/
    @ManyToOne
    @JoinColumn(name = "currency_id", foreignKey = @ForeignKey(name = "FK_COUNTRY_CURRENCYID"))
    CurrencyMaster currencyMaster;

    /*Currency Code*/
    @Column(name = "currency_code", length = 10)
    String currencyCode;

    /*Region id*/
    @ManyToOne
    @JoinColumn(name = "region_id", foreignKey = @ForeignKey(name = "FK_COUNTRY_REGIONID"))
    RegionMaster regionMaster;

    /*Region Code*/
    @Column(name = "region_code", length = 100)
    String regionCode;

    /* Nationality*/
    @Column(name = "nationality", length = 100)
    String nationality;

    /* weight */
    @Column(name = "weight_cbm")
    Double weightOrCbm;

    /* Lov Status */
    @Column(name = "status", nullable = false, length = 10)
    @Enumerated(EnumType.STRING)
    LovStatus status;


    /* Country Flag */
    @Column(name = "image")
    @Lob
    byte[] image;

    /* Encoded Image for showing in ui */
    /* Note : this is not a column */
    @Getter
    @Setter
    @Transient
    String encodedImage;


    public CountryMaster() {

    }

    public CountryMaster(Long id) {
        this.id = id;
    }

    public CountryMaster(Long id, String countryName) {
        this.id = id;
        this.countryName = countryName;
    }

    public CountryMaster(Long id, String countryCode, String countryName) {
        this.id = id;
        this.countryCode = countryCode;
        this.countryName = countryName;
    }

    public CountryMaster(CountryMaster obj) {
        this.id = obj.id;
        this.versionLock = obj.versionLock;
        this.systemTrack = obj.systemTrack;
        this.countryCode = obj.countryCode;
        this.countryName = obj.countryName;
        this.locale = obj.locale;
        this.currencyCode = obj.currencyCode;
        this.nationality = obj.nationality;
        this.weightOrCbm = obj.weightOrCbm;
        this.status = obj.status;
    }

    public CountryMaster(String countryCode, String countryName,String currencyCode, LovStatus status, SystemTrack systemTrack) {
        this.countryCode = countryCode;
        this.countryName = countryName;
        this.currencyCode = currencyCode;
        this.status = status;
        this.systemTrack = systemTrack;
    }
}
