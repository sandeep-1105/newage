package com.efreightsuite.model.common;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.UniqueConstraint;

import com.efreightsuite.enumeration.DocumentType;
import com.efreightsuite.enumeration.LovStatus;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;


@Entity
@Getter
@Setter
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Table(name = "efs_common_doc_type_master",
        uniqueConstraints = {@UniqueConstraint(columnNames = {"document_type_code"}, name = "UK_COM_DOC_DOCTYPECODE"),
                @UniqueConstraint(columnNames = {"document_type_name"}, name = "UK_COM_DOC_DOCTYPENAME")}

)
public class CommonDocumentTypeMaster implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    @Column(name = "document_type_code", nullable = false, length = 30)
    @Enumerated(EnumType.STRING)
    DocumentType documentTypeCode;

    /* Document Type Name */
    @Column(name = "document_type_name", nullable = false, length = 100)
    String documentTypeName;

    /*Lov Status*/
    @Column(name = "status", nullable = false, length = 10)
    @Enumerated(EnumType.STRING)
    LovStatus status;
}
