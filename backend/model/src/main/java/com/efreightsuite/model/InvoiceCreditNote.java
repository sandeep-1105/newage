package com.efreightsuite.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.*;

import com.efreightsuite.enumeration.CustomerAgent;
import com.efreightsuite.enumeration.InvoiceCreditNoteStatus;
import com.efreightsuite.util.JsonDateDeserializer;
import com.efreightsuite.util.JsonDateSerializer;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Type;


@Entity
@Getter
@Setter
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Table(name = "efs_invoice_credit_note",
        indexes = {
                @Index(columnList = "invoice_credit_note_date", name = "IDX_INVOICE_CRDTNOTE_DATE"),
                @Index(columnList = "party_id", name = "IDX_INVOICE_CRDTNOTE_PARTYID"),
                @Index(columnList = "billing_currency_id", name = "IDX_INVOICE_CRDTNOTE_BILLCURID")}
)
public class InvoiceCreditNote implements Serializable {
    private static final long serialVersionUID = 1L;

    /* Auto Generation Id */
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    /* version lock */
    @Version
    @Column(name = "version_lock")
    long versionLock;

    /* Create/Last User and Date Info */
    @Embedded
    SystemTrack systemTrack;

    /* Daybook id */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "daybook_id", foreignKey = @ForeignKey(name = "FK_INV_CRDNTE_DAYBOOKID"), nullable = false)
    DaybookMaster daybookMaster;

    @Column(name = "daybook_code", length = 100)
    String daybookCode;

    /* Customer/Agent */
    @Column(name = "customer_agent", nullable = false, length = 10)
    @Enumerated(EnumType.STRING)
    CustomerAgent customerAgent;

    /* Party id */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "party_id", foreignKey = @ForeignKey(name = "FK_INV_CRDNTE_PARTYID"), nullable = false)
    PartyMaster party;

    @Column(name = "party_code", length = 100)
    String partyCode;

    /* Party address */
    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "party_address_id", foreignKey = @ForeignKey(name = "FK_INV_CRDNTE_PARTYADDR"), nullable = false)
    AddressMaster partyAddress;

    /* Invoice Date */
    @Column(name = "invoice_credit_note_date", nullable = false)
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date invoiceCreditNoteDate;

    /* Due Date */
    @Column(name = "due_date", nullable = true)
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date dueDate;

    /* PartyAccount id */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "party_account_id", foreignKey = @ForeignKey(name = "FK_INV_CRDNTE_PARTYACCID"), nullable = false)
    PartyAccountMaster partyAccount;

    /* Subledger */
    @Column(name = "subledger")
    String subledger;

    /* Shipment UID */
    @Column(name = "shipment_uid")
    String shipmentUid;

    /* Master UID */
    @Column(name = "master_uid")
    String masterUid;

    /* Billing currency */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "billing_currency_id", foreignKey = @ForeignKey(name = "FK_INV_CRDNTE_BILLCURID"), nullable = false)
    CurrencyMaster billingCurrency;

    @Column(name = "billing_currency_code", length = 100)
    String billingCurrencyCode;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "local_currency_id", foreignKey = @ForeignKey(name = "FK_INV_CRDNTE_LOCCURID"))
    CurrencyMaster localCurrency;

    @Column(name = "local_currency_code", length = 100)
    String localCurrencyCode;

    /* ROE (Rate Of Exchange) */
    @Column(name = "roe", nullable = false)
    Double roe;

    /* Invoice Local Amount */
    @Column(name = "local_amount")
    Double localAmount;

    /* Invoice Currency Amount */
    @Column(name = "currency_amount")
    Double currencyAmount;

    /* Amount Received */
    @Column(name = "amount_received")
    Double amountReceived;

    /* Outstanding */
    @Column(name = "outstanding_amount")
    Double outstandingAmount;

    /* Total Debit Currency Amount */
    @Column(name = "total_debit_currency_amount")
    Double totalDebitCurrencyAmount;

    /* Total Debit Local Amount*/
    @Column(name = "total_debit_local_amount")
    Double totalDebitLocalAmount;

    /* Total Credit Currency Amount*/
    @Column(name = "total_credit_currency_amount")
    Double totalCreditCurrencyAmount;

    /* Total Credit Local Amount*/
    @Column(name = "total_credit_local_amount")
    Double totalCreditLocalAmount;

    /* Net Currency Amount*/
    @Column(name = "net_currency_amount")
    Double netCurrencyAmount;

    /* Net Local Amount*/
    @Column(name = "net_local_amount")
    Double netLocalAmount;

    /* Adjustment Amount*/
    @Column(name = "adjustment_amount")
    Double adjustmentAmount;

    /* Adjustment currency Amount*/
    @Column(name = "adjustment_currency_amount")
    Double adjustmentCurrencyAmount;

    /* Note */
    @Column(name = "note")
    @Lob
    @Type(type = "org.hibernate.type.MaterializedClobType")
    String note;

    /* total tax Amount */
    @Column(name = "total_tax_amount")
    Double totalTaxAmount;

    /* Invoice Status */
    @Column(name = "invoice_credit_note_status", nullable = false, length = 30)
    @Enumerated(EnumType.STRING)
    InvoiceCreditNoteStatus invoiceCreditNoteStatus;

    @OneToMany(mappedBy = "invoiceCreditNote", cascade = CascadeType.ALL, orphanRemoval = true)
    @JsonManagedReference
    List<InvoiceCreditNoteReceipt> invoiceCreditNoteReceiptList = new ArrayList<>();

    @OneToMany(mappedBy = "invoiceCreditNote", cascade = CascadeType.ALL, orphanRemoval = true)
    @JsonManagedReference
    List<InvoiceCreditNoteAttachment> invoiceCreditNoteAttachmentList = new ArrayList<>();

    @OneToMany(mappedBy = "invoiceCreditNote", cascade = CascadeType.ALL, orphanRemoval = true)
    @JsonManagedReference
    List<InvoiceCreditNoteDetail> invoiceCreditNoteDetailList = new ArrayList<>();


    /* shipmentServiceDetail Id */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "shipment_service_detail_id", foreignKey = @ForeignKey(name = "FK_INV_CRDNTE_SERVICEDETID"))
    ShipmentServiceDetail shipmentServiceDetail;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "document_type_id", foreignKey = @ForeignKey(name = "FK_INV_CRDNTE_DOCUMENTID"))
    DocumentTypeMaster documentTypeMaster;

    /* documentTypeCode */
    @Column(name = "document_type_code", length = 100)
    String documentTypeCode;


    /* invoice CreditNote No */
    @Column(name = "invoice_credit_note_no")
    String invoiceCreditNoteNo;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "location_id", foreignKey = @ForeignKey(name = "FK_INV_CRDNTE_LOCID"))
    LocationMaster Location;

    @Column(name = "location_code", length = 100)
    String locationCode;

    /* adjustment invoice No */
    @Column(name = "adjustment_invoice_no")
    String adjustmentInvoiceNo;

    /*adjustment Daybook id */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "adjustment_daybook_id", foreignKey = @ForeignKey(name = "FK_INV_CRDNTE_ADJDAYBOOKID"))
    DaybookMaster adjustMentDaybookMaster;

    @Column(name = "adjustment_daybook_code", length = 100)
    String adjustMentDaybookCode;

    /* reasonMaster */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "reason_id", foreignKey = @ForeignKey(name = "FK_INV_CRN_REASONID"))
    ReasonMaster reasonMaster;

    /* reasonCode */
    @Column(name = "reason_code")
    String reasonCode;


    /* reason */
    @Column(name = "reason")
    String reason;

}
