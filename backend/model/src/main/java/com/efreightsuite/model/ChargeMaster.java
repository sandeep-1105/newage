package com.efreightsuite.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.UniqueConstraint;
import javax.persistence.Version;

import com.efreightsuite.enumeration.ChargeCalculationType;
import com.efreightsuite.enumeration.ChargeType;
import com.efreightsuite.enumeration.LovStatus;
import com.efreightsuite.model.common.LocationSetupCharge;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Getter;
import lombok.Setter;


@Entity
@Getter
@Setter
@Table(name = "efs_charge_master", uniqueConstraints = {
        @UniqueConstraint(columnNames = {"charge_code"}, name = "UK_CHARGE_CHARGECODE"),
        @UniqueConstraint(columnNames = {"charge_name"}, name = "UK_CHARGE_CHARGENAME")})
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class ChargeMaster implements Serializable {
    private static final long serialVersionUID = 1L;

    /* Auto Generation Id */
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    @Version
    @Column(name = "version_lock")
    long versionLock;

    /* Create/Last User and Date Info */
    @Embedded
    SystemTrack systemTrack;

    /* Charge Code */
    @Column(name = "charge_code", nullable = false, length = 10)
    String chargeCode;

    /* Charge Name */
    @Column(name = "charge_name", nullable = false, length = 100)
    String chargeName;

    /* Charge Type */
    @Column(name = "charge_type", nullable = false, length = 20)
    @Enumerated(EnumType.STRING)
    ChargeType chargeType;

    /* Calculation Type */
    @Column(name = "calculation_type", nullable = false, length = 20)
    @Enumerated(EnumType.STRING)
    ChargeCalculationType calculationType;

    /* Lov Status */
    @Column(name = "status", nullable = false, length = 10)
    @Enumerated(EnumType.STRING)
    LovStatus status;

    @JsonManagedReference
    @OneToMany(mappedBy = "chargeMaster", cascade = CascadeType.ALL, orphanRemoval = true)
    List<ChargeEdiMapping> ediList = new ArrayList<>();


    public ChargeMaster() {
        // TODO Auto-generated constructor stub
    }


    public ChargeMaster(LocationSetupCharge charge) {
        this.chargeCode = charge.getChargeCode();
        this.chargeName = charge.getChargeName();
        this.chargeType = charge.getChargeType();
        this.calculationType = charge.getCalculationType();
        this.status = charge.getStatus();
    }


}
