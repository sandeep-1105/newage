package com.efreightsuite.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Version;

import com.efreightsuite.enumeration.LovStatus;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;


@Entity
@Getter
@Setter
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Table(name = "efs_aes_license")
public class AesLicense implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    @Version
    @Column(name = "version_lock")
    long versionLock;

    /* Create/Last User and Data Info */
    @Embedded
    SystemTrack systemTrack;

    /* Action */
    @Column(name = "license_code", length = 50, nullable = false)
    String licenseCode;

    @Column(name = "license_name", length = 100, nullable = false)
    String licenseName;

    @Column(name = "license_type", length = 100, nullable = false)
    String licenseType;


    /* Lov Status */
    @Column(name = "status", nullable = false, length = 10)
    @Enumerated(EnumType.STRING)
    LovStatus status;

    public AesLicense() {
    }

    public AesLicense(String licenseName, String licenseCode, String licenseType, LovStatus status, SystemTrack systemTrack) {
        this.licenseName = licenseName;
        this.licenseCode = licenseCode;
        this.licenseType = licenseType;
        this.systemTrack = systemTrack;
        this.status = status;
    }
}
