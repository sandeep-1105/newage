package com.efreightsuite.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Transient;
import javax.persistence.Version;

import com.efreightsuite.enumeration.YesNo;
import com.efreightsuite.util.JsonDateDeserializer;
import com.efreightsuite.util.JsonDateSerializer;
import com.efreightsuite.util.JsonYesNoDeserializer;
import com.efreightsuite.util.JsonYesNoSerializer;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Getter;
import lombok.Setter;


@Entity
@Getter
@Setter
@Table(name = "efs_agent_rate_request")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class RateRequest implements Serializable {
    private static final long serialVersionUID = 1L;

    /* Auto Generation Id */
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    @Version
    @Column(name = "version_lock")
    long versionLock;

    /* Create/Last User and Data Info */
    @Embedded
    SystemTrack systemTrack;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "enquiry_detail_id", foreignKey = @ForeignKey(name = "FK_RATEREQ_ENQDETID"))
    @JsonBackReference
    EnquiryDetail enquiryDetail;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "agent_id", foreignKey = @ForeignKey(name = "FK_RATEREQ_VENDOR"))
    PartyMaster vendor;

    @Column(name = "agent_code", length = 100)
    String vendorCode;

    @Column(name = "contact_person", length = 100)
    String contactPerson;

    @Column(name = "email_id", length = 500)
    String emailId;

    @Column(name = "request_date", nullable = true)
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date requestDate;

    @Column(name = "response_date")
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date responseDate;

    @Column(name = "rate_accepted", length = 10, nullable = false)
    @Enumerated(EnumType.STRING)
    YesNo rateAccepted = YesNo.No;

    @Column(name = "rate_accepted_date")
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date rateAcceptedDate;

    /* Customer IP */
    @Column(name = "customer_ip", length = 100)
    String customerIp;

    /* Pin Id */
    @Transient
    String pin;

    @Column(name = "is_mail_sent", length = 5)
    @Enumerated(EnumType.STRING)
    @JsonSerialize(using = JsonYesNoSerializer.class)
    @JsonDeserialize(using = JsonYesNoDeserializer.class)
    YesNo isMailSent = YesNo.Yes.No;


    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "rate_accepted_employee_id", foreignKey = @ForeignKey(name = "FK_RATEREQ_EMPID"))
    EmployeeMaster rateAcceptedEmployee;

    @Column(name = "rate_accepted_employee_code", length = 100)
    String rateAcceptedEmployeeCode;

    @OneToMany(mappedBy = "rateRequest", cascade = CascadeType.ALL, orphanRemoval = true)
    @JsonManagedReference
    @OrderBy("id ASC")
    List<RateRequestCharge> chargeList = new ArrayList<>();


    @Transient
    String encodedLogo;

    @Transient
    String companyAddress;

}
