package com.efreightsuite.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.*;

import com.efreightsuite.enumeration.BooleanType;
import com.efreightsuite.enumeration.JobStatus;
import com.efreightsuite.enumeration.PPCC;
import com.efreightsuite.enumeration.ServiceCode;
import com.efreightsuite.enumeration.WhoRouted;
import com.efreightsuite.enumeration.YesNo;
import com.efreightsuite.util.JsonBooleanTypeDeSerializer;
import com.efreightsuite.util.JsonBooleanTypeSerializer;
import com.efreightsuite.util.JsonDateDeserializer;
import com.efreightsuite.util.JsonDateSerializer;
import com.efreightsuite.util.JsonYesNoDeserializer;
import com.efreightsuite.util.JsonYesNoSerializer;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Type;


@Entity
@Getter
@Setter
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Table(name = "efs_consol",
        uniqueConstraints = {@UniqueConstraint(columnNames = {"consol_uid"}, name = "UK_CONSOL_CONSOLUID")},
        indexes = {
                @Index(columnList = "service_id", name = "IDX_CONSOL_SERVICEID"),
                @Index(columnList = "agent_id", name = "IDX_CONSOL_AGENTID"),
                @Index(columnList = "document_id", name = "IDX_CONSOL_DOCID"),
                @Index(columnList = "master_date", name = "IDX_CONSOL_MASTDATE"),
                @Index(columnList = "origin_id", name = "IDX_CONSOL_ORIGINID"),
                @Index(columnList = "destination_id", name = "IDX_CONSOL_DESTID"),
                @Index(columnList = "carrier_id", name = "IDX_CONSOL_CARRIERID"),
                @Index(columnList = "etd", name = "IDX_CONSOL_ETD"),
                @Index(columnList = "eta", name = "IDX_CONSOL_ETA"),
                @Index(columnList = "created_by", name = "IDX_CONSOL_CREATID"),
                @Index(columnList = "freight_term", name = "IDX_CONSOL_FRTTERM"),
                @Index(columnList = "job_status", name = "IDX_CONSOL_JOBSTATUS")
        })
public class Consol implements Serializable {
    private static final long serialVersionUID = 1L;
    // Win EAWB
    @Column(name = "win_eawb")
    @Enumerated(EnumType.STRING)
    @JsonSerialize(using = JsonBooleanTypeSerializer.class)
    @JsonDeserialize(using = JsonBooleanTypeDeSerializer.class)
    public BooleanType winEawb = BooleanType.FALSE;
    // Win EPAPER
    @Column(name = "win_with_paper")
    @Enumerated(EnumType.STRING)
    @JsonSerialize(using = JsonBooleanTypeSerializer.class)
    @JsonDeserialize(using = JsonBooleanTypeDeSerializer.class)
    public BooleanType winWithPaper = BooleanType.FALSE;
    /* Auto Generation Id */
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;
    @Version
    @Column(name = "version_lock")
    long versionLock;
    /* Create/Last User and Data Info */
    @Embedded
    SystemTrack systemTrack;
    /* Created By */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "created_by", foreignKey = @ForeignKey(name = "FK_CONSOL_CREATEDBY"), nullable = false)
    EmployeeMaster createdBy;
    /* Company Code */
    @Column(name = "created_by_code", length = 30, nullable = false)
    String createdByCode;
    /* Company Id */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "company_id", foreignKey = @ForeignKey(name = "FK_CONSOL_COMPANYID"), nullable = false)
    CompanyMaster company;
    /* Company Code */
    @Column(name = "company_code", length = 10, nullable = false)
    String companyCode;
    /* Location Id */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "location_id", foreignKey = @ForeignKey(name = "FK_CONSOL_LOCATIONID"), nullable = false)
    LocationMaster location;
    /* Location Code */
    @Column(name = "location_code", length = 10, nullable = false)
    String locationCode;
    /* Country Id */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "country_id", foreignKey = @ForeignKey(name = "FK_CONSOL_COUNTRYID"), nullable = false)
    CountryMaster country;
    /* Country Code */
    @Column(name = "country_code", length = 10, nullable = false)
    String countryCode;
    /* Consol UID */
    @Column(name = "consol_uid", nullable = false, length = 100)
    String consolUid;
    /* Master Date */
    @Column(name = "master_date", nullable = false)
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date masterDate;
    /* Who Routed */
    @Column(name = "who_routed")
    @Enumerated(EnumType.STRING)
    WhoRouted whoRouted;
    /* Salesman id */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "routed_salesman_id", foreignKey = @ForeignKey(name = "FK_CONSOL_SALEMANID"))
    EmployeeMaster routedSalesman;
    /* Salesman Code */
    @Column(name = "salesman_code")
    String salesmanCode;
    /* Customer id */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "routed_agent_id", foreignKey = @ForeignKey(name = "FK_CONSOL_ROUTED_AGENTID"))
    PartyMaster routedAgent;
    /* Customer Code */
    @Column(name = "routed_agent_code")
    String routedAgentCode;
    /* Service Id */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "service_id", foreignKey = @ForeignKey(name = "FK_CONSOL_SERVICEID"), nullable = false)
    ServiceMaster serviceMaster;
    /* Service Code */
    @Column(name = "service_code", length = 10, nullable = false)
    String serviceCode;
    /* Agent id */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "agent_id", foreignKey = @ForeignKey(name = "FK_CONSOL_AGENTID"))
    PartyMaster agent;
    /* Agent Code */
    @Column(name = "agent_code")
    String agentCode;
    /* Agent Manifest Name */
    @Column(name = "agent_manifest_name")
    String agentManifestName;
    /* Agent Address */
    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "agent_address_id", foreignKey = @ForeignKey(name = "FK_CONSOL_AGENTADDID"))
    AddressMaster agentAddress;
    /* Freight Term */
    @Column(name = "freight_term", length = 10, nullable = false)
    @Enumerated(EnumType.STRING)
    PPCC ppcc;
    /* Job Status */
    @Column(name = "job_status", length = 30, nullable = false)
    @Enumerated(EnumType.STRING)
    JobStatus jobStatus;
    /* Is Job Completed */
    @Column(name = "is_job_completed", length = 5, nullable = false)
    @Enumerated(EnumType.STRING)
    @JsonSerialize(using = JsonYesNoSerializer.class)
    @JsonDeserialize(using = JsonYesNoDeserializer.class)
    YesNo isJobCompleted;
    /* Origin */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "origin_id", foreignKey = @ForeignKey(name = "FK_CONSOL_ORIGIN"), nullable = false)
    PortMaster origin;
    /* Origin Code */
    @Column(name = "origin", nullable = false)
    String originCode;
    /* POL */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "pol_id", foreignKey = @ForeignKey(name = "FK_CONSOL_POL"), nullable = false)
    PortMaster pol;
    /* POL */
    @Column(name = "pol", nullable = false)
    String polCode;
    /* POD */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "pod_id", foreignKey = @ForeignKey(name = "FK_CONSOL_POD"), nullable = false)
    PortMaster pod;
    /* POD */
    @Column(name = "pod", nullable = false)
    String podCode;
    /* Destination */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "destination_id", foreignKey = @ForeignKey(name = "FK_CONSOL_DESTINATION"), nullable = false)
    PortMaster destination;
    /* Destination */
    @Column(name = "destination", nullable = false)
    String destinationCode;
    /* currency id */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "currency_id", foreignKey = @ForeignKey(name = "FK_CONSOL_CURRENCYCODE"))
    CurrencyMaster currency;
    /* Currency Code */
    @Column(name = "currency_code", length = 10)
    String currencyCode;
    /* currency Rate */
    @Column(name = "currency_rate")
    Double currencyRate;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "local_currency_id", foreignKey = @ForeignKey(name = "FK_CONSOL_LOCCURR"))
    CurrencyMaster localCurrency;
    @Column(name = "local_currency_code", length = 10)
    String localCurrencyCode;
    /* Total Amount */
    @Column(name = "total_amount")
    Double totalAmount;
    /* Carrier Id */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "carrier_id", foreignKey = @ForeignKey(name = "FK_CONSOL_CARRIERID"))
    CarrierMaster carrier;
    /* Carrier Code */
    @Column(name = "carrier_code")
    String carrierCode;
    /* firstCarrier Id for Mawb */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "first_carrier_id", foreignKey = @ForeignKey(name = "FK_CONSOL_FIRSTCARRIERID"))
    CarrierMaster firstCarrier;
    /* Carrier Code */
    @Column(name = "first_carrier_code")
    String firstCarrierCode;
    /* carrierNo for Mawb */
    @Column(name = "carrier_no")
    String firstcarrierNo;
    /* ETD */
    @Column(name = "etd")
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date etd;
    /* ETA */
    @Column(name = "eta")
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date eta;
    /* ATD */
    @Column(name = "atd")
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date atd;
    /* ATA */
    @Column(name = "ata")
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date ata;
    /* Schedule UID */
    @Column(name = "schedule_uid")
    String scheduleUid;
    /* Vessel Id */
    @Column(name = "vessel_id")
    Long vesselId;
    /* Vessel Code */
    @Column(name = "vessel_code")
    String vesselCode;
    /* Handling Agent id */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "handling_agent_id", foreignKey = @ForeignKey(name = "FK_CONSOL_HANDLINGAGENTID"))
    PartyMaster handlingAgent;
    /* Handling Agent Code */
    @Column(name = "handling_agent_code")
    String handlingAgentCode;
    /* Handling Agent Manifest Name */
    @Column(name = "handling_agent_manifest_name")
    String handlingAgentManifestName;
    /* Handling Address Line 1 */
    @Column(name = "handling_address_line_1", length = 255)
    String handlingAddress1;
    /* Handling Address Line 2 */
    @Column(name = "handling_address_line_2", length = 255)
    String handlingAddress2;
    /* Handling Address Line 3 */
    @Column(name = "handling_address_line_3", length = 255)
    String handlingAddress3;
    /* Carrier Agent id */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "carrier_agent_id", foreignKey = @ForeignKey(name = "FK_CONSOL_CARRIERAGENTID"))
    PartyMaster carrierAgent;
    /* Carrier Agent Code */
    @Column(name = "carrier_agent_code")
    String carrierAgentCode;
    /* Carrier Agent Manifest Name */
    @Column(name = "carrier_agent_manifest_name")
    String carrierAgentManifestName;
    /* Carrier Agent Address Line 1 */
    @Column(name = "carrier_agent_address_line_1", length = 255)
    String carrierAgentAddress1;
    /* Carrier Agent Address Line 2 */
    @Column(name = "carrier_agent_address_line_2", length = 255)
    String carrierAgentAddress2;
    /* Carrier Agent Address Line 3 */
    @Column(name = "carrier_agent_address_line_3", length = 255)
    String carrierAgentAddress3;
    /* Weight unit */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "weight_unit", foreignKey = @ForeignKey(name = "FK_CONSOL_WEIGHTUNIT"))
    UnitMaster weightUnit;
    @Column(name = "weight_unit_code")
    String weightUnitCode;
    /* Volume unit */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "volume_unit", foreignKey = @ForeignKey(name = "FK_CONSOL_VOLUMEUNIT"))
    UnitMaster volumeUnit;
    @Column(name = "volume_unit_code")
    String volumeUnitCode;
    /* Is Co-Loader */
    @Column(name = "is_co_loader", length = 5)
    @Enumerated(EnumType.STRING)
    BooleanType isCoLoader;
    /* Co-Loader id */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "co_loader_id", foreignKey = @ForeignKey(name = "FK_CONSOL_COLOADERID"))
    PartyMaster coLoader;
    /* Co-load Code */
    @Column(name = "co_loader_code")
    String coLoaderCode;
    /* Service Code */
    @Column(name = "service_type_code")
    @Enumerated(EnumType.STRING)
    ServiceCode serviceTypeCode;
    /* is Line DO issued */
    @Column(name = "is_line_do_issued", length = 5)
    @Enumerated(EnumType.STRING)
    BooleanType isLineDoIssued;
    /* Known Shipper */
    @Column(name = "known_shipper")
    @Enumerated(EnumType.STRING)
    YesNo knownShipper = YesNo.Yes;
    /* Commodity description */
    @Column(name = "commodity_description")
    String commodityDescription;
    /* Handling Information */
    @Column(name = "handling_information")
    String handlingInformation;
    /* Accounting Information */
    @Column(name = "accounting_information")
    String accoutingInformation;
    /* IATA Account Code */
    @Column(name = "iata_account_code")
    String iataAccountCode;
    /* IATA Rate */
    @Column(name = "iata_rate")
    Double iataRate;
    /* Document Issuing Agent id */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "document_issuing_agent_id", foreignKey = @ForeignKey(name = "FK_CONSOL_DOCISSUEAGENTID"))
    PartyMaster documentIssuingAgent;
    /* Document Issuing Agent Code */
    @Column(name = "document_issuing_agent_code")
    String documentIssuingAgentCode;
    /* Document Issuing Agent Name */
    @Column(name = "document_issuing_agent_name")
    String documentIssuingAgentName;
    /* Document Issuing Agent Address Line 1 */
    @Column(name = "doc_issue_agent_addr_line_1", length = 255)
    String documentIssuingAgentAddress1;
    /* Document Issuing Agent Address Line 2 */
    @Column(name = "doc_issue_agent_addr_line_2", length = 255)
    String documentIssuingAgentAddress2;
    /* Document Issuing Agent Address Line 3 */
    @Column(name = "doc_issue_agent_addr_line_3", length = 255)
    String documentIssuingAgentAddress3;
    /* First Notify id */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "first_notify_id", foreignKey = @ForeignKey(name = "FK_CONSOL_FIRSTNOTIFYID"))
    PartyMaster firstNotify;
    /* First Notify Code */
    @Column(name = "first_notify_code")
    String firstNotifyCode;
    /* First Notify Manifest Name */
    @Column(name = "first_notify_manifest_name")
    String firstNotifyManifestName;
    /* First Notify Address Line 1 */
    @Column(name = "first_notify_address_line_1", length = 255)
    String firstNotifyAddress1;
    /* First Notify Address Line 2 */
    @Column(name = "first_notify_address_line_2", length = 255)
    String firstNotifyAddress2;
    /* First Notify Address Line 3 */
    @Column(name = "first_notify_address_line_3", length = 255)
    String firstNotifyAddress3;
    /* Shipped on board Date */
    @Column(name = "shipped_on_board_date")
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date shippedOnBoardDate;
    /* Is X-Ray */
    @Column(name = "is_x_ray", length = 5)
    @Enumerated(EnumType.STRING)
    BooleanType isXRay;
    /* SHC */
    @Column(name = "shc")
    String shc;
    /* MIS Job Close Status */
    @Column(name = "mis_job_close_status")
    String misJobCloseStatus;
    /* MIS Job Close User */
    @Column(name = "mis_job_close_user")
    String misJobCloseUser;
    /* MIS Job Close Date */
    @Column(name = "mis_job_close_date")
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date misJobCloseDate;
    /* Direct Shipment */
    @Column(name = "direct_shipment", length = 5)
    @Enumerated(EnumType.STRING)
    YesNo directShipment = YesNo.No;
    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "consol_detail_id", foreignKey = @ForeignKey(name = "FK_CONSOL_CONSOLDETAILID"))
    ConsolDetail consolDetail;
    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "document_id", foreignKey = @ForeignKey(name = "FK_CONSOL_DOCID"))
    ConsolDocument consolDocument;
    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "master_mail_trigger", foreignKey = @ForeignKey(name = "FK_CONSOL_SERMAILTRI"))
    ServiceMailTrigger serviceMailTrigger;
    @OneToMany(mappedBy = "consol", cascade = CascadeType.ALL, orphanRemoval = true)
    @JsonManagedReference
    @OrderBy("subJobSequence ASC")
    List<ShipmentLink> shipmentLinkList = new ArrayList<>();
    /* Consol Connection List */
    @OneToMany(mappedBy = "consol", cascade = CascadeType.ALL, orphanRemoval = true)
    @JsonManagedReference
    List<ConsolConnection> connectionList = new ArrayList<>();
    /* Consol Charge List */
    @OneToMany(mappedBy = "consol", cascade = CascadeType.ALL, orphanRemoval = true)
    @JsonManagedReference
    List<ConsolCharge> chargeList = new ArrayList<>();
    /* Consol document attachment */
    @OneToMany(mappedBy = "consol", cascade = CascadeType.ALL, orphanRemoval = true)
    @JsonManagedReference
    List<ConsolAttachment> consolAttachmentList = new ArrayList<>();
    /* Consol Event tab */
    @OneToMany(mappedBy = "consol", cascade = CascadeType.ALL, orphanRemoval = true)
    @JsonManagedReference
    List<ConsolEvent> eventList = new ArrayList<>();
    /* Consol Reference Tab */
    @OneToMany(mappedBy = "consol", cascade = CascadeType.ALL, orphanRemoval = true)
    @JsonManagedReference
    List<ConsolReference> referenceList = new ArrayList<>();
    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "pickup_delivery_id", foreignKey = @ForeignKey(name = "FK_CONSOL_PICKDELID"))
    PickUpDeliveryPoint pickUpDeliveryPoint;
    /* Generated EDI ID */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "airline_edi_id", foreignKey = @ForeignKey(name = "FK_CONSOL_EDI"))
    AirlineEdi airlineEdi;
    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "sign_off_id", foreignKey = @ForeignKey(name = "FK_CONSOL_SIGNOFFID"))
    ConsolSignOff consolSignOff;
    @Transient
    String isDownloadPrintFor;
    /* Notes */
    @Column(name = "notes")
    @Lob
    @Type(type = "org.hibernate.type.MaterializedClobType")
    String notes;
    /* internal_note */
    @Column(name = "internal_note")
    @Lob
    @Type(type = "org.hibernate.type.MaterializedClobType")
    String internalNote;
    /* MIS Job Close User */
    @Column(name = "win_ref_id")
    Long winAwbID;

    /* WIN Status */
    @Column(name = "win_status")
    String winStatus;

    /* MIS Job Close User */
    @Column(name = "win_error_code")
    String winErrorCode;

    /* WIN Status */
    @Column(name = "win_error_message", length = 500)
    String winErrorMessage;

    public Consol() {

    }

    public Consol(Consol consol) {
        this.id = consol.getId();
        this.consolUid = consol.getConsolUid();
    }
}
