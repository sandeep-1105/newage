package com.efreightsuite.model.common;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Type;


@Entity
@Getter
@Setter
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Table(name = "efs_common_email_request")
public class CommonEmailRequest implements Serializable {

    private static final long serialVersionUID = 1L;

    /* Auto Generation Id */
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    /* From Email Address */
    @Column(name = "email_from", nullable = false, length = 100)
    String fromEmailId;

    /* From Email Address */
    @Column(name = "email_from_password", nullable = false, length = 100)
    String fromEmailIdPassword;

    /* List of email ID of receivers */
    @Column(name = "email_to", nullable = false, length = 500)
    String toEmailIdList;

    /* List of email ID of cc */
    @Column(name = "email_cc", length = 500)
    String ccEmailIdList;

    /* List of email ID of BCC */
    @Column(name = "email_bcc", length = 500)
    String bccEmailIdList;

    /* subject of email */
    @Column(name = "email_subject", nullable = false, length = 255)
    String subject;

    /* email body content to send */
    @Column(name = "email_body", nullable = false)
    @Lob
    @Type(type = "org.hibernate.type.MaterializedClobType")
    String emailBody;

    /* email type to send */
    @Column(name = "email_type", nullable = false, length = 30)
    String emailType;

    @Column(name = "server_port")
    Integer port;

    //E-mail Settings
    @Column(name = "smtp_server_address", length = 255)
    String smtpServerAddress;

}
