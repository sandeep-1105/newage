package com.efreightsuite.model.common;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.UniqueConstraint;

import com.efreightsuite.enumeration.ChargeCalculationType;
import com.efreightsuite.enumeration.ChargeType;
import com.efreightsuite.enumeration.LovStatus;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;


@Entity
@Getter
@Setter
@Table(name = "efs_location_setup_charge", uniqueConstraints = {
        @UniqueConstraint(columnNames = {"charge_code", "location_setup_id"}, name = "UK_COM_CHARGE_CHARGECODE"),
        @UniqueConstraint(columnNames = {"charge_name", "location_setup_id"}, name = "UK_COM_CHARGE_CHARGENAME")})
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class LocationSetupCharge implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    @ManyToOne
    @JoinColumn(name = "location_setup_id", foreignKey = @ForeignKey(name = "FK_LOCSETCHARGE_LOCSETUPID"))
    @JsonBackReference
    LocationSetup locationSetup;

    /* Charge Code */
    @Column(name = "charge_code", nullable = false, length = 10)
    String chargeCode;

    @Column(name = "charge_name", nullable = false, length = 100)
    String chargeName;

    @Column(name = "charge_type", nullable = false, length = 20)
    @Enumerated(EnumType.STRING)
    ChargeType chargeType;

    @Column(name = "calculation_type", nullable = false, length = 20)
    @Enumerated(EnumType.STRING)
    ChargeCalculationType calculationType;

    @Column(name = "status", nullable = false, length = 10)
    @Enumerated(EnumType.STRING)
    LovStatus status;
}
