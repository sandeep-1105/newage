package com.efreightsuite.model.common;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;


@Entity
@Getter
@Setter
@Table(name = "efs_location_setup_service")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class LocationSetupService implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    @ManyToOne
    @JoinColumn(name = "location_setup_id", foreignKey = @ForeignKey(name = "FK_LOCSETSERVICE_LOCSETUPID"))
    @JsonBackReference
    LocationSetup locationSetup;

    @ManyToOne
    @JoinColumn(name = "service_master_id", foreignKey = @ForeignKey(name = "FK_LOCSETSERVICE_SERVICEID"))
    CommonServiceMaster serviceMaster;

    public LocationSetupService() {
    }

    public LocationSetupService(CommonServiceMaster serviceMaster) {
        this.serviceMaster = serviceMaster;
    }
}
