package com.efreightsuite.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.UniqueConstraint;
import javax.persistence.Version;

import com.efreightsuite.enumeration.LovStatus;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;


@Entity
@Getter
@Setter
@Table(name = "efs_project_master",
        uniqueConstraints = {
                @UniqueConstraint(columnNames = {"project_code"}, name = "UK_PROJECT_PROJECTCODE"),
                @UniqueConstraint(columnNames = {"project_name"}, name = "UK_PROJECT_PROJECTNAME")}
)
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class ProjectMaster implements Serializable {

    private static final long serialVersionUID = 1L;

    /* Auto Generation Id */
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    @Version
    @Column(name = "version_lock")
    long versionLock;

    /*Create/LastUpdated User and Date Info*/
    @Embedded
    SystemTrack systemTrack;

    /* Period Name */
    @Column(name = "project_name", length = 100)
    String projectName;

    /* Quotation No */
    @Column(name = "project_code", length = 10)
    String projectCode;

    /* Lov Status */
    @Column(name = "status", nullable = false, length = 10)
    @Enumerated(EnumType.STRING)
    LovStatus status;


}
