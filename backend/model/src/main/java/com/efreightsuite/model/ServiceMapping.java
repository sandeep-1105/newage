package com.efreightsuite.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.UniqueConstraint;
import javax.persistence.Version;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;


@Entity
@Table(name = "efs_service_mapping", uniqueConstraints = {
        @UniqueConstraint(columnNames = {"import_service_id"}, name = "UK_SERMAP_IMPID"),
        @UniqueConstraint(columnNames = {"export_service_id"}, name = "UK_SERMAP_EXPID")}
)
@Getter
@Setter
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class ServiceMapping implements Serializable {


    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    @Version
    @Column(name = "version_lock")
    long versionLock;

    @Embedded
    SystemTrack systemTrack;

    @ManyToOne
    @JoinColumn(name = "import_service_id", foreignKey = @ForeignKey(name = "FK_SERVICEMPG_IMPORTID"))
    ServiceMaster importService;

    @Column(name = "import_code", nullable = false, length = 20)
    String importCode;

    @ManyToOne
    @JoinColumn(name = "export_service_id", foreignKey = @ForeignKey(name = "FK_SERVICEMPG_EXPORTID"))
    ServiceMaster exportService;

    @Column(name = "export_code", nullable = false, length = 20)
    String exportCode;

}
