package com.efreightsuite.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Transient;
import javax.persistence.Version;

import com.efreightsuite.dto.ReportCharge;
import com.efreightsuite.dto.ReportCurrency;
import com.efreightsuite.enumeration.YesNo;
import com.efreightsuite.util.JsonYesNoDeserializer;
import com.efreightsuite.util.JsonYesNoSerializer;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.envers.Audited;
import org.hibernate.envers.RelationTargetAuditMode;


@Entity
@Getter
@Setter
@Table(name = "efs_quotation_carrier")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class QuotationCarrier implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /* Auto Generation Id */
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    @Version
    @Column(name = "version_lock")
    long versionLock;

    /*Tracking informations*/
    @Embedded
    SystemTrack systemTrack;

    /* EnquiryDetail */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "quotation_detail_id", foreignKey = @ForeignKey(name = "FK_QUOTECARR_QUTOEID"))
    @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
    @JsonBackReference
    QuotationDetail quotationDetail;

    /* Charge */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "carrier_id", foreignKey = @ForeignKey(name = "FK_QUOTECARR_CARRID"))
    CarrierMaster carrierMaster;

    /* Charge Name */
    @Column(name = "carrier_code", length = 20)
    String carrierCode;

    /* Transit Days */
    @Column(name = "transit_day")
    Integer transitday;

    /*Frequency*/
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "frequency_id", foreignKey = @ForeignKey(name = "FK_QUOTECARR_FREQID"))
    PeriodMaster frequency;

    @Column(name = "frequency_code", length = 100)
    String frequencyCode;

    @Column(name = "lump_sum")
    Double lumpSum;

    @Column(name = "per_kg")
    Double perKg;


    @Column(name = "lump_sum_buy_amt")
    Double lumpSumBuyAmt;

    @Column(name = "per_kg_buy")
    Double perKgBuy;


    /* Is All Inclusive */
    @Column(name = "is_all_inclusive_charge", length = 10)
    @Enumerated(EnumType.STRING)
    @JsonSerialize(using = JsonYesNoSerializer.class)
    @JsonDeserialize(using = JsonYesNoDeserializer.class)
    YesNo isAllInclusiveCharges = YesNo.No;
    @Transient
    List<ReportCharge> mapChargeList = new ArrayList<>();
    @Transient
    List<ReportCurrency> currencyList = new ArrayList<>();
    @Transient
    String chargeList;
    @OneToMany(mappedBy = "quotationCarrier", cascade = CascadeType.ALL, orphanRemoval = true)
    @JsonManagedReference
    private List<QuotationCharge> quotationChargeList = new ArrayList<>();

}
