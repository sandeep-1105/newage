package com.efreightsuite.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Version;

import com.efreightsuite.enumeration.BooleanType;
import com.efreightsuite.enumeration.JobStatus;
import com.efreightsuite.util.JsonDateDeserializer;
import com.efreightsuite.util.JsonDateSerializer;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Getter;
import lombok.Setter;


@Entity
@Getter
@Setter
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Table(name = "efs_consol_detail")
public class ConsolDetail implements Serializable {
    private static final long serialVersionUID = 1L;

    /*Auto Generation Id*/
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    @Version
    @Column(name = "version_lock")
    long versionLock;

    /*Create/Last User and Data Info*/
    @Embedded
    @JsonIgnore
    SystemTrack systemTrack;

    /*Is Carrier DO Issued*/
    @Column(name = "is_carrier_do_issued", length = 5)
    @Enumerated(EnumType.STRING)
    BooleanType isCarrierDoIssued;

    /*Carrier DO Issued Date*/
    @Column(name = "carrier_do_issued_date")
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date carrierDoIssuedDate;

    /*Carrier DO No*/
    @Column(name = "carrier_do_no")
    String carrierDoNo;

    /*Carrier DO Date*/
    @Column(name = "carrier_do_date")
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date carrierDoDate;

    /*ATA No*/
    @Column(name = "ata_no")
    Long ataNo;

    /*ATA Date*/
    @Column(name = "ata_date")
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date ataDate;

    /*Cargo Type Code*/
    @Column(name = "cargo_type_code")
    String cargoTypeCode;

    /*Document Received Date*/
    @Column(name = "document_received_date")
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date DocumentReceivedDate;

    /*BL Release*/
    @Column(name = "bl_released", length = 5)
    @Enumerated(EnumType.STRING)
    BooleanType blReleased;

    /*Is Direct*/
    @Column(name = "is_direct", length = 5)
    @Enumerated(EnumType.STRING)
    BooleanType isDirect;

    /*Note*/
    @Column(name = "note")
    String note;

    /*Is Minimum*/
    @Column(name = "is_minimum", length = 5)
    @Enumerated(EnumType.STRING)
    BooleanType isMinimum;


    /*LC Note*/
    @Column(name = "lc_note")
    String lcNote;

    /*Rotation Number*/
    @Column(name = "rotation_number")
    String rotationNumber;

    /*MRN*/
    @Column(name = "mrn")
    String mrn;

    /*MRN Date*/
    @Column(name = "mrn_date")
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date mrnDate;

    /*NOC Number*/
    @Column(name = "noc_number")
    String nocNumber;

    /*NOC Date*/
    @Column(name = "noc_date")
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date nocDate;

    /*Charter Id*/
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "charter_id", foreignKey = @ForeignKey(name = "FK_CONSODETAILL_CHARTERID"))
    CharterMaster charterMaster;

    /*Charter Code*/
    @Column(name = "charter_code")
    String charterCode;

    /*CRI Number*/
    @Column(name = "cri_number")
    String criNumber;

    /*Service Issue Note*/
    @Column(name = "service_issue_note")
    String serviceIssueNote;

    /*Arrival Code*/
    @Column(name = "arrival_code")
    String arrivalCode;

    /*Release Code*/
    @Column(name = "release_code")
    String releaseCode;

    /*IGM No*/
    @Column(name = "igm_no")
    String igmNo;

    /*IGM Date*/
    @Column(name = "igm_date")
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date igmDate;

    /*Local Serial*/
    @Column(name = "local_serial")
    String LocalSerial;

    /*Transhipment Serial*/
    @Column(name = "transhipment_serial")
    String transhipmentSerial;

    /*Ship Calling No*/
    @Column(name = "ship_calling_no")
    String shipCallingNo;

    /*Customs Approval No*/
    @Column(name = "customs_approval_no")
    String customsApprovalNo;

    /*SLPA No*/
    @Column(name = "slpa_no")
    String slpaNo;

    /*Previous CCN No*/
    @Column(name = "previous_ccn_no")
    String previousCcnNo;

    /*VIA No*/
    @Column(name = "via_no")
    String via_no;

    /*Cargo nature*/
    @Column(name = "cargo_nature")
    String cargoNature;

    /*Rotation Date*/
    @Column(name = "rotation_date")
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date rotationDate;

    /*Onward*/
    @Column(name = "onward")
    String onward;

    /*spot_no*/
    @Column(name = "spot_no")
    String spotNo;

    /*Unstuff Place*/
    @Column(name = "unstuff_place")
    String unstuffPlace;

    /*Job Status*/
    @Column(name = "job_status")
    @Enumerated(EnumType.STRING)
    JobStatus jobStatus;

    /*Transfer of Ownership Date*/
    @Column(name = "transfer_of_ownership_date")
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date transferOfOwnershipDate;

    /*Export Doc Send Date*/
    @Column(name = "export_doc_send_date")
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date exportDocSendDate;

    /*Export Declaration*/
    @Column(name = "export_declaration")
    String exportDeclaration;

    /*Known Unknown Text*/
    @Column(name = "known_unknown_text")
    String knownUnknownText;

    /*Mark No*/
    @Column(name = "mark_no")
    String markNo;

    /*Rate Class*/
    @Column(name = "rate_class", length = 1)
    String rateClass;

    /*Optional Shipping Info 1*/
    @Column(name = "optional_shipping_info_1")
    String optionalShippingInfo1;

    /*Optional Shipping Info 2*/
    @Column(name = "optional_shipping_info_2")
    String optionalShippingInfo2;

    /*MBL Surrender Date*/
    @Column(name = "mbl_surrender_date")
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date mblSurrenderDate;

    /*MUCR*/
    @Column(name = "mucr")
    String mucr;

    /*Carrier CAN Received Date*/
    @Column(name = "carrier_can_received_date")
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date carrierCanReceivedDate;

    /*Manifest Sent Date CFS*/
    @Column(name = "manifest_sent_date_cfs")
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date manifestSentDateCfs;

    /*Shipping Bill No*/
    @Column(name = "shipping_bill_no")
    String shippingBillNo;

    /*Shipping Bill Type*/
    @Column(name = "shipping_bill_type")
    String shippingBillType;

    /*Shipping Bill Date*/
    @Column(name = "shipping_bill_date")
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date shippingBillDate;

    /*Carrier Commission Percentage*/
    @Column(name = "carrier_commission_percentage")
    Double carrierCommissionPercentage;

    /*Carrier Incentive Percentage*/
    @Column(name = "carrier_incentive_percentage")
    Double carrierIncentivePercentage;

    /*Sub Agent Commission Percentage*/
    @Column(name = "subagent_commission_percentage")
    Double subAgentCommissionPercentage;

    /*Sub Agent Incentive Percentage*/
    @Column(name = "subagent_incentive_percentage")
    Double subAgentIncentivePercentage;

    /*Carrier Calculation Mode*/
    @Column(name = "carrier_calculation_mode")
    String carrierCalculationMode;

    /*Sub Agent Calculation Mode*/
    @Column(name = "subagent_calculation_mode")
    String subAgentCalculationMode;

    /*Carrier Brokerage Percentage*/
    @Column(name = "carrier_brokerage_percentage")
    Double carrierBrokeragePercentage;

    /*Line Invoice Received*/
    @Column(name = "line_invoice_received")
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date lineInvoiceReceived;

    /*Line Invoice Received By*/
    @Column(name = "line_invoice_received_by")
    String lineInvoiceReceivedBy;

    /*Line DO Handed Date to CFS*/
    @Column(name = "line_do_handed_date_to_cfs")
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date lineDoHandedDateToCfs;

    /*Line DO Handed_by */
    @Column(name = "line_do_handed_by")
    String lineDoHandedBy;

    /*Carrier Brokerage Currency*/
    @Column(name = "carrier_brokerage_currency_id")
    CurrencyMaster carrierBrokerageCurrecny;

    /*carrierBrokerageCurrecny Code*/
    @Column(name = "carrier_brokerage_currency", length = 10)
    String carrierBrokerageCurrecnyCode;

    /*Carrier Brokerage Amount*/
    @Column(name = "carrier_brokerage_amount")
    Double carrierBrokerageAmount;

    /*Overseas Invoice Checked*/
    @Column(name = "overseas_invoice_checked", length = 5)
    @Enumerated(EnumType.STRING)
    BooleanType overseasInvoiceChecked;

    /*Overseas Invoice Query Note*/
    @Column(name = "overseas_invoice_query_note")
    String overseasInvoiceQueryNote;

    /*Recovery Amount*/
    @Column(name = "recovery_amount")
    Double recoveryAmount;

    /*Vessel Birth Terminal Code*/
    @Column(name = "vessel_birth_terminal_code")
    String vesselBirthTerminalCode;

    /*Customs Job Date*/
    @Column(name = "customs_job_date")
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date customsJobDate;

    /*Customs Job No*/
    @Column(name = "customs_job_no")
    String customsJobNo;


}
