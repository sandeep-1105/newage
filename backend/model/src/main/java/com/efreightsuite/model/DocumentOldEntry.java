package com.efreightsuite.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.UniqueConstraint;
import javax.persistence.Version;

import com.efreightsuite.enumeration.DimensionUnit;
import com.efreightsuite.util.JsonDimensionUnitDeserializer;
import com.efreightsuite.util.JsonDimensionUnitSerializer;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Getter;
import lombok.Setter;


@Entity
@Table(name = "efs_document_old_entry", uniqueConstraints = {
        @UniqueConstraint(columnNames = {"document_no"}, name = "UK_DOCUMENOLD_DOCUMENTNO")
})
@Getter
@Setter
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class DocumentOldEntry implements Serializable {
    private static final long serialVersionUID = 1L;

    /* Auto Generation Id */
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    @Version
    @Column(name = "version_lock")
    long versionLock;

    /* Create/Last User and Data Info */
    @Embedded
    SystemTrack systemTrack;

    /* Service id */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "service_old_id", foreignKey = @ForeignKey(name = "FK_DOCOLD_SID"))
    @JsonBackReference
    ServiceOldEntry serviceEntry;

    @Column(name = "document_no", length = 100, nullable = false)
    String documentNo;

    /* Dimension Unit */
    @Column(name = "dimension_unit", nullable = false, length = 50)
    @Enumerated(EnumType.STRING)
    @JsonSerialize(using = JsonDimensionUnitSerializer.class)
    @JsonDeserialize(using = JsonDimensionUnitDeserializer.class)
    DimensionUnit dimensionUnit;

    /* Dimension Unit Value */
    @Column(name = "dimension_unit_value")
    double dimensionUnitValue;


    /* chargeble weight */
    @Column(name = "chargeble_weight")
    Double chargebleWeight;

    /* gross weight */
    @Column(name = "gross_weight")
    Double grossWeight;

    /* volume weight */
    @Column(name = "volume_weight")
    Double volumeWeight;

    /*  Vol Weight in pound */
    @Column(name = "volume_weight_in_pound")
    Double volumeWeightInPound;

    /*  Gross Weight in pound*/
    @Column(name = "gross_weight_in_pound")
    Double grossWeightInPound;

    /* gross weight */
    @Column(name = "dimension_gross_weight")
    Double dimensionGrossWeight;

    /* volume weight */
    @Column(name = "dimension_volume_weight")
    Double dimensionVolumeWeight;

    @Column(name = "dimension_gross_weight_in_kg")
    Double dimensionGrossWeightInKg;

    @Column(name = "dimension_no_of_pieces")
    Long dimensionNoOfPieces;

    @OneToMany(mappedBy = "documentOldEntry", cascade = CascadeType.ALL, orphanRemoval = true)
    @JsonManagedReference
    List<ServiceOldDimension> dimensionList = new ArrayList<>();

}
