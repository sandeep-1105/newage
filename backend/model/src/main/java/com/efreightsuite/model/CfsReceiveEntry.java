package com.efreightsuite.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Transient;
import javax.persistence.Version;

import com.efreightsuite.enumeration.DimensionUnit;
import com.efreightsuite.util.JsonDateDeserializer;
import com.efreightsuite.util.JsonDateSerializer;
import com.efreightsuite.util.JsonDimensionUnitDeserializer;
import com.efreightsuite.util.JsonDimensionUnitSerializer;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Getter;
import lombok.Setter;


@Entity
@Getter
@Setter
@Table(name = "efs_cfs_receive_entry")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class CfsReceiveEntry implements Serializable {

    private static final long serialVersionUID = 1L;

    /* Auto Generated Id */
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    /* Version Locking */
    @Version
    @Column(name = "version_lock")
    long versionLock;

    /* Created / Last Updated User and Date Info */
    @Embedded
    SystemTrack systemTrack;

    /* Auto Generated CFS Receipt Number */
    @Column(name = "cfs_receipt_no", length = 30, nullable = false)
    String cfsReceiptNumber;

    /* CFS Receipt Date */
    @Column(name = "cfs_receipt_date")
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date cfsReceiptDate;

    /* CFS */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "cfs_id", foreignKey = @ForeignKey(name = "FK_CFSRCV_CFSID"))
    CFSMaster cfsMaster;

    /* CFS Code */
    @Column(name = "cfs_code", length = 30)
    String cfsMasterCode;

    /*Shipment Service */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "service_id", foreignKey = @ForeignKey(name = "FK_CFSRCV_SERVICEID"))
    @JsonBackReference
    ShipmentServiceDetail shipmentServiceDetail;

    /* Service Uid */
    @Column(name = "service_uid", length = 30)
    String serviceUid;

    @Transient
    String shipmentUid;


    /* Pro Number */
    @Column(name = "pro_number", length = 30)
    String proNumber;

    /* Pro Date */
    @Column(name = "pro_date")
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date proDate;

    /* Shipper */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "shipper_id", foreignKey = @ForeignKey(name = "FK_CFSRCV_SHIPPERID"))
    PartyMaster shipper;

    /* Shipper Code */
    @Column(name = "shipper_code", length = 30)
    String shipperCode;

    /* Consignee */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "consignee_id", foreignKey = @ForeignKey(name = "FK_CFSRCV_CONSIGNID"))
    PartyMaster consignee;

    /* Consignee Code */
    @Column(name = "consignee_code", length = 30)
    String consigneeCode;

    /* Transporter */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "transporter_id", foreignKey = @ForeignKey(name = "FK_CFSRCV_TRANSPORTID"))
    PartyMaster transporter;

    /* Transporter Code */
    @Column(name = "transporter_code", length = 30)
    String transporterCode;

    /* Warehouse location */
    @Column(name = "warehouse_loc", length = 100)
    String warehouseLocation;

    /* Shipper Reference Number */
    @Column(name = "shipper_ref_no", length = 30)
    String shipperRefNumber;

    /* On-Hand */
    @Column(name = "on_hand", length = 30)
    String onHand;

	/* Dimension Tab */
    /* Dimension Unit */


    /* Booking No Of Pack */
    @Column(name = "no_of_pieces")
    Long noOfPieces;


    @Column(name = "dimension_unit", nullable = false, length = 50)
    @Enumerated(EnumType.STRING)
    @JsonSerialize(using = JsonDimensionUnitSerializer.class)
    @JsonDeserialize(using = JsonDimensionUnitDeserializer.class)
    DimensionUnit dimensionUnit;

    /* Dimension Unit Value */
    @Column(name = "dimension_unit_value")
    double dimensionUnitValue;

    /* chargeble weight */
    @Column(name = "chargeble_weight")
    Double chargebleWeight;

    /* gross weight */
    @Column(name = "gross_weight")
    Double grossWeight;

    /* volume weight */
    @Column(name = "volume_weight")
    Double volumeWeight;

    /*  Vol Weight in pound */
    @Column(name = "volume_weight_in_pound")
    Double volumeWeightInPound;

    /*  Gross Weight in pound*/
    @Column(name = "gross_weight_in_pound")
    Double grossWeightInPound;

    /* Booking Volume in CBM */
    @Column(name = "booking_volume_in_cbm")
    Double bookedVolumeUnitCbm;

    /* gross weight */
    @Column(name = "dimension_gross_weight")
    Double dimensionGrossWeight;

    /* volume weight */
    @Column(name = "dimension_volume_weight")
    Double dimensionVolumeWeight;

    @Column(name = "dimension_gross_weight_in_kg")
    Double dimensionGrossWeightInKg;


    /* CFS Dimension List */
    @OneToMany(mappedBy = "cfsReceiveEntry", cascade = CascadeType.ALL, orphanRemoval = true)
    @JsonManagedReference
    List<CfsDimension> cfsDimensionList = new ArrayList<>();

    /* Attachment Tab */
	/* CFS Attachment List */
    @OneToMany(mappedBy = "cfsReceiveEntry", cascade = CascadeType.ALL, orphanRemoval = true)
    @JsonManagedReference
    List<CfsAttachment> cfsAttachementList = new ArrayList<>();

    /* Notes Tab */
	/* Notes */
    @Column(name = "notes", length = 4000)
    String notes;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "company_id", foreignKey = @ForeignKey(name = "FK_CFSRCV_COMPANYID"))
    CompanyMaster companyMaster;

    @Column(name = "company_code")
    String companyCode;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "location_id", foreignKey = @ForeignKey(name = "FK_CFSRCV_LOCATIONID"))
    LocationMaster location;

    @Column(name = "location_code")
    String locationCode;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "country_id", foreignKey = @ForeignKey(name = "FK_CFSRCV_COUNTRYID"))
    CountryMaster country;

    @Column(name = "country_code")
    String countryCode;

}
