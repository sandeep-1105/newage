package com.efreightsuite.model;

import java.io.Serializable;
import java.util.Comparator;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

import com.efreightsuite.enumeration.EdiStatus;
import com.efreightsuite.util.JsonDateDeserializer;
import com.efreightsuite.util.JsonDateSerializer;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Getter;
import lombok.Setter;


@Entity
@Getter
@Setter
@Table(name = "efs_airline_edi_status")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class AirlineEdiStatus implements Serializable {


    /**
     *
     */
    private static final long serialVersionUID = 1L;
    public static Comparator<AirlineEdiStatus> IdComparator = new Comparator<AirlineEdiStatus>() {

        @Override
        public int compare(AirlineEdiStatus o1, AirlineEdiStatus o2) {
            if (o1.getId() > o2.getId()) {
                return -1;
            } else if (o1.getId() < o2.getId()) {
                return 1;
            } else {
                return 0;
            }
        }

    };
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;
    /* Created Date */
    @Column(name = "create_date", nullable = false)
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date createDate;
    /* AirlineEdi */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "airline_edi_id", foreignKey = @ForeignKey(name = "FK_AIR_EDI_STATUS_EDI_ID"))
    @JsonBackReference
    AirlineEdi airlineEdi;
    /* Status Message */
    @Column(name = "status_messsage", length = 4000, nullable = true)
    String statusMessage;
    @Column(name = "status", length = 30)
    @Enumerated(EnumType.STRING)
    EdiStatus status;

}
