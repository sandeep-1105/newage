package com.efreightsuite.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Version;

import com.efreightsuite.enumeration.LovStatus;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;


@Entity
@Getter
@Setter
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Table(name = "efs_gl_charge_mapping_master")
public class GeneralLedgerChargeMappingMaster implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    @Version
    @Column(name = "version_lock")
    long versionLock;

    /* Create/Last User and Data Info */
    @Embedded
    SystemTrack systemTrack;

    @ManyToOne
    @JoinColumn(name = "service_id", nullable = false, foreignKey = @ForeignKey(name = "FK_GLCHARMAP_SERVIID"))
    ServiceMaster serviceMaster;

    @Column(name = "service_code", nullable = false, length = 30)
    String serviceCode;

    @ManyToOne
    @JoinColumn(name = "charge_id", nullable = false, foreignKey = @ForeignKey(name = "FK_GLCHARMAP_CHARFID"))
    ChargeMaster chargeMaster;

    @Column(name = "charge_code", nullable = false, length = 30)
    String chargeCode;

    @ManyToOne
    @JoinColumn(name = "revenue_ledger_id", foreignKey = @ForeignKey(name = "FK_GLCHARMAP_REVENUE"))
    GeneralLedgerAccount revenueLedger;

    @ManyToOne
    @JoinColumn(name = "revenue_sub_ledger_id", foreignKey = @ForeignKey(name = "FK_GLCHARMAP_SUBREVENUE"))
    GeneralSubLedger revenueSubLedger;

    @ManyToOne
    @JoinColumn(name = "cost_ledger_id", foreignKey = @ForeignKey(name = "FK_GLCHARMAP_COST"))
    GeneralLedgerAccount costLedger;

    @ManyToOne
    @JoinColumn(name = "cost_sub_ledger_id", foreignKey = @ForeignKey(name = "FK_GLCHARMAP_SUBCOST"))
    GeneralSubLedger costSubLedger;

    @ManyToOne
    @JoinColumn(name = "neutral_ledger_id", foreignKey = @ForeignKey(name = "FK_GLCHARMAP_NEUTRAL"))
    GeneralLedgerAccount neutralLedger;

    @ManyToOne
    @JoinColumn(name = "neutral_sub_ledger_id", foreignKey = @ForeignKey(name = "FK_GLCHARMAP_NEUTRALSUB"))
    GeneralSubLedger neutralSubLedger;


    @ManyToOne
    @JoinColumn(name = "currency_id", nullable = false, foreignKey = @ForeignKey(name = "FK_GLCHARMAP_CURREN"))
    CurrencyMaster currencyMaster;

    @ManyToOne
    @JoinColumn(name = "transit_ledger_id", foreignKey = @ForeignKey(name = "FK_GLCHARMAP_TRANSIT"))
    GeneralLedgerAccount transitLedger;

    @ManyToOne
    @JoinColumn(name = "transit_sub_ledger_id", foreignKey = @ForeignKey(name = "FK_GLCHARMAP_TRANSITSUB"))
    GeneralSubLedger transitSubLedger;

    @ManyToOne
    @JoinColumn(name = "transit_cost_ledger_id", foreignKey = @ForeignKey(name = "FK_GLCHARMAP_TRANSITCOST"))
    GeneralLedgerAccount transitCostLedger;

    @ManyToOne
    @JoinColumn(name = "transit_cost_sub_ledger_id", foreignKey = @ForeignKey(name = "FK_GLCHARMAP_TRANSITCOSTSUB"))
    GeneralSubLedger transitCostSubLedger;

    @ManyToOne
    @JoinColumn(name = "asset_ledger_id", foreignKey = @ForeignKey(name = "FK_GLCHARMAP_ASSET"))
    GeneralLedgerAccount assetLedger;

    @ManyToOne
    @JoinColumn(name = "asset_sub_ledger_id", foreignKey = @ForeignKey(name = "FK_GLCHARMAP_ASSETSUB"))
    GeneralSubLedger assetSubLedger;

    @ManyToOne
    @JoinColumn(name = "liabilities_ledger_id", foreignKey = @ForeignKey(name = "FK_GLCHARMAP_LIABILITIES"))
    GeneralLedgerAccount liabilitiesLedger;

    @ManyToOne
    @JoinColumn(name = "liabilities_sub_ledger_id", foreignKey = @ForeignKey(name = "FK_GLCHARMAP_LIABILITIESSUB"))
    GeneralSubLedger liabilitiesSubLedger;

    @Column(name = "status", nullable = false, length = 10)
    @Enumerated(EnumType.STRING)
    LovStatus status;

}
