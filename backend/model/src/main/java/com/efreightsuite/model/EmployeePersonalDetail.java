package com.efreightsuite.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

import com.efreightsuite.enumeration.Gender;
import com.efreightsuite.enumeration.MaritalStatus;
import com.efreightsuite.util.JsonDateDeserializer;
import com.efreightsuite.util.JsonDateSerializer;
import com.efreightsuite.util.JsonGenderDeserializer;
import com.efreightsuite.util.JsonGenderSerializer;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Getter;
import lombok.Setter;


@Entity
@Getter
@Setter
@Table(name = "efs_employee_personal_detail")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class EmployeePersonalDetail implements Serializable {
    private static final long serialVersionUID = 1L;

    /* Auto Generation Id */
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    /* Date of Birth */
    @Column(name = "date_of_birth")
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date dateOfBirth;

    /* Gender */
    @Column(name = "gender", length = 10)
    @Enumerated(EnumType.STRING)
    @JsonSerialize(using = JsonGenderSerializer.class)
    @JsonDeserialize(using = JsonGenderDeserializer.class)
    Gender gender = Gender.Male;

    /* Religion */
    @Column(name = "religion", nullable = true, length = 100)
    String religion;

    /* Marital Status */
    @Column(name = "marital_status", length = 10)
    @Enumerated(EnumType.STRING)
    MaritalStatus maritalStatus;

    /* Wedding Anniversary Date */
    @Column(name = "wedding_anniversary_date")
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date weddingAnniversaryDate;

    /* Phone Number */
    @Column(name = "phone_number", nullable = true, length = 100)
    String phoneNumber;

    /* Fax */
    @Column(name = "fax", nullable = true, length = 100)
    String fax;

    /* Personal Email Id */
    @Column(name = "personal_email", nullable = true, length = 500)
    String personalEmail;

    /* Mobile Number */
    @Column(name = "mobile_number", nullable = true, length = 100)
    String mobileNumber;

    /* Home Town */
    @Column(name = "home_town", nullable = true, length = 100)
    String homeTown;

    /* Airport Name */
    @Column(name = "airport_name", nullable = true, length = 100)
    String airportName;

    /* Permanent Address */
    @Column(name = "permanent_address", nullable = true, length = 4000)
    String permanentAddress;

    /* Permanent Phone Number */
    @Column(name = "permanent_phone_number", nullable = true, length = 100)
    String permanentPhoneNumber;

    /* Temporary Address */
    @Column(name = "temporary_address", nullable = true, length = 4000)
    String temporaryAddress;

    /* Temporary Phone Number */
    @Column(name = "temporary_phone_number", nullable = true, length = 100)
    String temporaryPhoneNumber;

    /* Emergency Address */
    @Column(name = "emergency_address", nullable = true, length = 4000)
    String emergencyAddress;

    /* Emergency Phone Number */
    @Column(name = "emergency_phone_number", nullable = true, length = 100)
    String emergencyPhoneNumber;

}
