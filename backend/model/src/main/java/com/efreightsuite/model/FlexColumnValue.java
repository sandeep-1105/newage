package com.efreightsuite.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.UniqueConstraint;
import javax.persistence.Version;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;


@Entity
@Getter
@Setter
@Table(name = "efs_flex_column_value", uniqueConstraints = {
        @UniqueConstraint(columnNames = {"flex_column_master_id", "row_id"}, name = "UK_FLXCOLVAL_TBLVAL")})
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class FlexColumnValue implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    @Version
    @Column(name = "version_lock")
    long versionLock;

    @Embedded
    SystemTrack systemTrack;

    @ManyToOne
    @JoinColumn(name = "flex_column_master_id", foreignKey = @ForeignKey(name = "FK_FLXCOLVAL_FLXCOL"))
    FlexColumnMaster flexColumnMaster;

    @Column(name = "row_id")
    Long rowId;

    @Column(name = "flex_value")
    String flexValue;


}
