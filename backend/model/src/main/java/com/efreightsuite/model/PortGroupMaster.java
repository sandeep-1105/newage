package com.efreightsuite.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.UniqueConstraint;
import javax.persistence.Version;

import com.efreightsuite.enumeration.LovStatus;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;


@Entity
@Getter
@Setter
@Table(name = "efs_port_group_master", uniqueConstraints = {
        @UniqueConstraint(columnNames = {"port_group_code"}, name = "UK_PORTGROUP_PORTGROUPCODE"),
        @UniqueConstraint(columnNames = {"port_group_name"}, name = "UK_PORTGROUP_PORTGROUPNAME")}

)
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class PortGroupMaster implements Serializable {
    private static final long serialVersionUID = 1L;

    /* Auto Generation Id */
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    /* Version Lock */
    @Version
    @Column(name = "version_lock")
    long versionLock;

    /* Created / Last User and Data Info */
    @Embedded
    SystemTrack systemTrack;

    /* Country id */
    @ManyToOne
    @JoinColumn(name = "country_id", foreignKey = @ForeignKey(name = "FK_PORTGROUP_COUNTRYID"))
    CountryMaster countryMaster;

    /* Country Code */
    @Column(name = "country_code", length = 10)
    String countryCode;

    /* Port Group Code */
    @Column(name = "port_group_code", nullable = false, length = 10)
    String portGroupCode;

    /* Port Group Name */
    @Column(name = "port_group_name", nullable = false, length = 100)
    String portGroupName;

    /* Lov Status */
    @Column(name = "status", nullable = false, length = 10)
    @Enumerated(EnumType.STRING)
    LovStatus status;

}
