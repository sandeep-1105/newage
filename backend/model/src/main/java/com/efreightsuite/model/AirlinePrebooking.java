package com.efreightsuite.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Version;

import com.efreightsuite.util.JsonDateDeserializer;
import com.efreightsuite.util.JsonDateSerializer;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Getter;
import lombok.Setter;


@Entity
@Getter
@Setter
@Table(name = "efs_airline_prebooking")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class AirlinePrebooking implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    @Version
    @Column(name = "version_lock")
    long versionLock;

    /* Created / Last Updated User and Date Info */
    @Embedded
    SystemTrack systemTrack;


    /* Flight Plan UID */
    @Column(name = "prebooking_no", nullable = false, length = 100)
    String prebookingNumber;

	/* Login Location Information */

    /* Location */
    @ManyToOne
    @JoinColumn(name = "location_id", foreignKey = @ForeignKey(name = "FK_AIR_PREBOOK_LOCATIONID"))
    LocationMaster locationMaster;

    @Column(name = "location_code", length = 30)
    String locationCode;

    /* Company */
    @ManyToOne
    @JoinColumn(name = "company_id", foreignKey = @ForeignKey(name = "FK_AIR_PREBOOK_COMPANYID"))
    CompanyMaster companyMaster;

    @Column(name = "company_code", length = 30)
    String companyCode;

    /* Country */
    @ManyToOne
    @JoinColumn(name = "country_id", foreignKey = @ForeignKey(name = "FK_AIR_PREBOOK_COUNTRYID"))
    CountryMaster countryMaster;

    @Column(name = "country_code", length = 30)
    String countryCode;


    /* Carrier Id */
    @ManyToOne
    @JoinColumn(name = "carrier_id", foreignKey = @ForeignKey(name = "FK_AIR_PREBOOK_CARRIERID"))
    CarrierMaster carrierMaster;

    /* prebooking Date */
    @Column(name = "prebooking_date")
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date prebookingDate;

    /* Service Id */
    @ManyToOne
    @JoinColumn(name = "service_id", foreignKey = @ForeignKey(name = "FK_AIR_PREBOOK_SERVICEID"))
    ServiceMaster serviceMaster;

    /* POR */
    @ManyToOne
    @JoinColumn(name = "por_id", foreignKey = @ForeignKey(name = "FK_AIR_PREBOOK_PORID"))
    PortMaster por;

    /* Party id */
    @ManyToOne
    @JoinColumn(name = "party_id", foreignKey = @ForeignKey(name = "FK_AIR_PREBOOK_PARTYID"))
    PartyMaster partyMaster;

    /* Number of MAWB's */
    @Column(name = "no_of_mawb")
    Integer noOfMawb;

    /* Number Of Pieces*/
    @Column(name = "no_of_pieces")
    Long noOfPieces;

    /*Charter Id*/
    @ManyToOne
    @JoinColumn(name = "charter_id", foreignKey = @ForeignKey(name = "FK_AIR_PREBOOK_CHARTERID"))
    CharterMaster charterMaster;

    /* Volume */
    @Column(name = "volume")
    Double volume;

    /* Gross Weight */
    @Column(name = "gross_weight")
    Double grossWeight;

    /* Volume Weight */
    @Column(name = "volume_weight")
    Double volumeWeight;

    /* Notes */
    @Column(name = "notes", length = 4000)
    String notes;

    @OneToMany(mappedBy = "airlinePrebooking", cascade = CascadeType.ALL, orphanRemoval = true)
    @JsonManagedReference
    List<AirlinePrebookingSchedule> scheduleList = new ArrayList<>();

}
