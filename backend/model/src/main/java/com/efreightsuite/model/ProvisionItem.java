package com.efreightsuite.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Version;

import com.efreightsuite.enumeration.YesNo;
import com.efreightsuite.util.JsonYesNoDeserializer;
import com.efreightsuite.util.JsonYesNoSerializer;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Getter;
import lombok.Setter;


@Entity
@Getter
@Setter
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Table(name = "efs_provision_item")
public class ProvisionItem implements Serializable {
    private static final long serialVersionUID = 1L;

    /* Auto Generation Id */
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    @Version
    @Column(name = "version_lock")
    long versionLock;

    /* Create/Last User and Data Info */
    @Embedded
    SystemTrack systemTrack;


    /* Provisional id */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "provisional_id", foreignKey = @ForeignKey(name = "FK_PROV_ITEM_PROV_ID"))
    @JsonBackReference
    Provisional provisional;


    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "service_id", foreignKey = @ForeignKey(name = "FK_PRVSNLITEM_SHPMSERVDTLID"))
    ShipmentServiceDetail shipmentService;

    /* serviceUid */
    @Column(name = "service_uid")
    String serviceUid;

    /* Charge Id */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "charge_id", foreignKey = @ForeignKey(name = "FK_PRVSNLITEM_CHARGEID"))
    ChargeMaster chargeMaster;

    @Column(name = "charge_code", length = 100)
    String chargeCode;

    /* UnitMaster Id */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "unit_id", foreignKey = @ForeignKey(name = "FK_PRVSNLITEM_UNITID"))
    UnitMaster unitMaster;

    @Column(name = "unit_code", length = 100)
    String unitCode;

    /* No.of Units */
    @Column(name = "no_of_units")
    Double noOfUnits;

    /* Currency */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "sell_currency_id", foreignKey = @ForeignKey(name = "FK_PRVSNLITEM_CURRSELLID"))
    CurrencyMaster sellCurrency;

    @Column(name = "sell_currency_code", length = 100)
    String sellCurrencyCode;

    /* ROE */
    @Column(name = "sell_roe")
    Double sellRoe;

    /* Amount per unit*/
    @Column(name = "sell_amnt_per_unit")
    Double sellAmountPerUnit;

    /* Amount */
    @Column(name = "sell_amount")
    Double sellAmount;

    /* Local Amount */
    @Column(name = "sell_local_amount")
    Double sellLocalAmount;

    /* taxable */
    @Column(name = "is_taxable", length = 10)
    Boolean isTaxable;

    /* Currency */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "buy_currency_id", foreignKey = @ForeignKey(name = "FK_PRVSNLITEM_CURRBUYID"))
    CurrencyMaster buyCurrency;

    @Column(name = "buy_currency_code", length = 100)
    String buyCurrencyCode;

    /* ROE */
    @Column(name = "buy_roe")
    Double buyRoe;

    /* Amount per unit*/
    @Column(name = "buy_amnt_per_unit")
    Double buyAmountPerUnit;

    /* Amount */
    @Column(name = "buy_amount")
    Double buyAmount;

    /* Local Amount */
    @Column(name = "buy_local_amount")
    Double buyLocalAmount;

    /* Plus Minus Symbol*/
    @Column(name = "plus_minus_symbol")
    String plusMinus;

    /*BillToParty id*/
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "bill_to_party_id", foreignKey = @ForeignKey(name = "FK_PRVSNLITEM_BILL_PARTYID"))
    PartyMaster billToParty;

    @Column(name = "bill_to_party_code", length = 100)
    String billToPartyCode;

    /*vendorSubledger id*/
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "vendor_Subledger_id", foreignKey = @ForeignKey(name = "FK_PRVSNLITEM_BILL_VENDORID"))
    PartyMaster vendorSubledger;


    @Column(name = "vendor_Subledger_code", length = 100)
    String vendorSubledgerCode;

    /* sellDescription */
    @Column(name = "sell_description")
    String sellDescription;

    /* buyDescription */
    @Column(name = "buy_description")
    String buyDescription;

    /* referenceType */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "referencetype_id", foreignKey = @ForeignKey(name = "FK_PRVSNLITEM_RETYPID"))
    ReferenceTypeMaster referenceType;

    @Column(name = "referencetype_code", length = 100)
    String referenceTypeCode;

    /*Reference*/
    @Column(name = "reference")
    String reference;

    /* is neutral */
    @Column(name = "is_neutral", length = 20)
    @Enumerated(EnumType.STRING)
    @JsonSerialize(using = JsonYesNoSerializer.class)
    @JsonDeserialize(using = JsonYesNoDeserializer.class)
    YesNo isNeutral;


}
