package com.efreightsuite.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.UniqueConstraint;
import javax.persistence.Version;

import com.efreightsuite.dto.RecordAccessLevelCountryDTO;
import com.efreightsuite.dto.RecordAccessLevelCustomerDTO;
import com.efreightsuite.dto.RecordAccessLevelDTO;
import com.efreightsuite.dto.RecordAccessLevelDivisionDTO;
import com.efreightsuite.dto.RecordAccessLevelLocationDTO;
import com.efreightsuite.dto.RecordAccessLevelSalesmanDTO;
import com.efreightsuite.dto.RecordAccessLevelServiceDTO;
import com.efreightsuite.dto.RecordAccessLevelUserDTO;
import com.efreightsuite.enumeration.LovStatus;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Getter;
import lombok.Setter;


@Entity
@Getter
@Setter
@Table(name = "efs_record_access_level", uniqueConstraints = {
        @UniqueConstraint(columnNames = {"ral_name"}, name = "UK_RAL_NAME")})
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class RecordAccessLevel implements Serializable {
    private static final long serialVersionUID = 1L;

    /* Auto Generation Id */
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    @Version
    @Column(name = "version_lock")
    long versionLock;

    /* Create/Last User and Data Info */
    @Embedded
    SystemTrack systemTrack;

    /* Name */
    @Column(name = "ral_name", nullable = false, length = 100)
    String recordAccessLevelName;

    /* Lov Status */
    @Column(name = "status", nullable = false, length = 10)
    @Enumerated(EnumType.STRING)
    LovStatus status;

    /* Description */
    @Column(name = "description", length = 4000)
    String description;

    @OneToMany(mappedBy = "recordAccessLevel", cascade = CascadeType.ALL, orphanRemoval = true)
    @JsonManagedReference
    List<RecordAccessLevelUser> userList;

    @OneToMany(mappedBy = "recordAccessLevel", cascade = CascadeType.ALL, orphanRemoval = true)
    @JsonManagedReference
    List<RecordAccessLevelCountry> countryList;

    @OneToMany(mappedBy = "recordAccessLevel", cascade = CascadeType.ALL, orphanRemoval = true)
    @JsonManagedReference
    List<RecordAccessLevelLocation> locationList;

    @OneToMany(mappedBy = "recordAccessLevel", cascade = CascadeType.ALL, orphanRemoval = true)
    @JsonManagedReference
    List<RecordAccessLevelDivision> divisionList;

    @OneToMany(mappedBy = "recordAccessLevel", cascade = CascadeType.ALL, orphanRemoval = true)
    @JsonManagedReference
    List<RecordAccessLevelService> serviceList;

    @OneToMany(mappedBy = "recordAccessLevel", cascade = CascadeType.ALL, orphanRemoval = true)
    @JsonManagedReference
    List<RecordAccessLevelSalesman> salesmanList;

    @OneToMany(mappedBy = "recordAccessLevel", cascade = CascadeType.ALL, orphanRemoval = true)
    @JsonManagedReference
    List<RecordAccessLevelCustomer> customerList;


    public RecordAccessLevel() {
    }

    public RecordAccessLevel(RecordAccessLevelDTO dto) {

        this.id = dto.getId();
        this.versionLock = dto.getVersionLock();
        this.systemTrack = dto.getSystemTrack();
        this.recordAccessLevelName = dto.getRecordAccessLevelName();
        this.status = dto.getStatus();
        this.description = dto.getDescription();


        if (dto.getUserList() != null) {
            this.setUserList(new ArrayList<>());
            for (RecordAccessLevelUserDTO userDto : dto.getUserList()) {
                RecordAccessLevelUser userEntity = new RecordAccessLevelUser(userDto);
                userEntity.setRecordAccessLevel(this);
                this.getUserList().add(userEntity);
            }
        }


        if (dto.getCountryList() != null) {
            this.setCountryList(new ArrayList<>());
            for (RecordAccessLevelCountryDTO countryDto : dto.getCountryList()) {
                RecordAccessLevelCountry countryEntity = new RecordAccessLevelCountry(countryDto);
                countryEntity.setRecordAccessLevel(this);
                this.getCountryList().add(countryEntity);
            }
        }


        if (dto.getLocationList() != null) {
            this.setLocationList(new ArrayList<>());
            for (RecordAccessLevelLocationDTO locationDto : dto.getLocationList()) {
                RecordAccessLevelLocation locationEntity = new RecordAccessLevelLocation(locationDto);
                locationEntity.setRecordAccessLevel(this);
                this.getLocationList().add(locationEntity);
            }
        }


        if (dto.getDivisionList() != null) {
            this.setDivisionList(new ArrayList<>());
            for (RecordAccessLevelDivisionDTO divisionDto : dto.getDivisionList()) {
                RecordAccessLevelDivision divisionEntity = new RecordAccessLevelDivision(divisionDto);
                divisionEntity.setRecordAccessLevel(this);
                this.getDivisionList().add(divisionEntity);
            }
        }


        if (dto.getServiceList() != null) {
            this.setServiceList(new ArrayList<>());
            for (RecordAccessLevelServiceDTO serviceDto : dto.getServiceList()) {
                RecordAccessLevelService serviceEntity = new RecordAccessLevelService(serviceDto);
                serviceEntity.setRecordAccessLevel(this);
                this.getServiceList().add(serviceEntity);
            }
        }


        if (dto.getSalesmanList() != null) {
            this.setSalesmanList(new ArrayList<>());
            for (RecordAccessLevelSalesmanDTO salesmanDto : dto.getSalesmanList()) {
                RecordAccessLevelSalesman salesmanEntity = new RecordAccessLevelSalesman(salesmanDto);
                salesmanEntity.setRecordAccessLevel(this);
                this.getSalesmanList().add(salesmanEntity);
            }
        }


        if (dto.getCustomerList() != null) {
            this.setCustomerList(new ArrayList<>());
            for (RecordAccessLevelCustomerDTO customerDto : dto.getCustomerList()) {
                RecordAccessLevelCustomer customerEntity = new RecordAccessLevelCustomer(customerDto);
                customerEntity.setRecordAccessLevel(this);
                this.getCustomerList().add(customerEntity);
            }
        }
    }

}
