package com.efreightsuite.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Transient;
import javax.persistence.Version;

import com.efreightsuite.enumeration.ActualChargeable;
import com.efreightsuite.enumeration.BillToType;
import com.efreightsuite.enumeration.DUE;
import com.efreightsuite.enumeration.PPCC;
import com.efreightsuite.util.JsonDateDeserializer;
import com.efreightsuite.util.JsonDateSerializer;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Type;
import org.hibernate.envers.Audited;
import org.hibernate.envers.RelationTargetAuditMode;


@Entity
@Getter
@Setter
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Audited
@Table(name = "efs_shipment_rate")
public class ShipmentCharge implements Serializable {
    private static final long serialVersionUID = 1L;

    /* Auto Generation Id */
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    @Version
    @Column(name = "version_lock")
    long versionLock;

    /* Create/Last User and Data Info */
    @Embedded
    SystemTrack systemTrack;

    /* Location Id */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "location_id", foreignKey = @ForeignKey(name = "FK_SHIPRATE_LOCATIONID"))
    @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
    LocationMaster location;

    @Column(name = "location_code", length = 100)
    String locationCode;

    /* Company Id */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "company_id", foreignKey = @ForeignKey(name = "FK_SHIPRATE_COMPANYID"))
    @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
    CompanyMaster company;

    @Column(name = "company_code", length = 100)
    String companyCode;

    /* Country Id */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "country_id", foreignKey = @ForeignKey(name = "FK_SHIPRATE_COUNTRYID"))
    @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
    CountryMaster country;

    /* Country Code */
    @Column(name = "country_code", length = 10)
    String countryCode;


    /* Shipment UID */
    @Column(name = "shipment_uid")
    String shipmentUid;

    /* Service id */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "service_id", foreignKey = @ForeignKey(name = "FK_SHIPRATE_SERVICEID"))
    @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
    @JsonBackReference
    ShipmentServiceDetail shipmentServiceDetail;

    /* Service UID */
    @Column(name = "service_uid")
    String serviceUid;// new id same as shipment service Detail


    /* Charge Id */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "charge_id", foreignKey = @ForeignKey(name = "FK_SHIPRATE_CHARGEID"))
    @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
    ChargeMaster chargeMaster;

    /* Charge Code */
    @Column(name = "charge_code")
    String chargeCode;

    /* Charge Name */
    @Column(name = "charge_name")
    String chargeName;

    /* Bill To Type */
    @Column(name = "ppcc")
    @Enumerated(EnumType.STRING)
    PPCC ppcc;


    /* Sell Unit id */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "unit_id", foreignKey = @ForeignKey(name = "FK_SHIPRATE_UNITID"))
    @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
    UnitMaster unitMaster;

    /* Sell Unit Code */
    @Column(name = "unit_code")
    String unitCode;

    /* Estimated Unit */
    @Column(name = "estimated_unit")
    Double estimatedUnit;


    /* Number of Units */
    @Column(name = "no_of_unit")
    String numberOfUnit;

    /* Gross Sale Currency Id */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "gross_sale_currency_id", foreignKey = @ForeignKey(name = "FK_SHIPRATE_GROSALECURRID"))
    @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
    CurrencyMaster grossSaleCurrency;

    /* Gross Sale Currency Code */
    @Column(name = "gross_sale_currency_code", length = 10)
    String grossSaleCurrencyCode;

    /* Gross Sale Amount */
    @Column(name = "gross_sale_amount")
    Double grossSaleAmount;

    /* Gross Sale Minimum */
    @Column(name = "gross_sale_minimum")
    Double grossSaleMinimum;


    /* total Gross Sale */
    @Column(name = "total_gross_sale")
    Double totalGrossSale;

    /* Local Gross Sale ROE */
    @Column(name = "local_gross_sale_roe")
    Double localGrossSaleRoe;

    /* Local Amount for Gross Sale */
    @Column(name = "local_total_gross_sale")
    Double localTotalGrossSale;


    /* Net Sale Currency id */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "net_sale_currency_id", foreignKey = @ForeignKey(name = "FK_SHIPRATE_NETSALECURRID"))
    @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
    CurrencyMaster netSaleCurrency;

    /* Net Sale Currency Code */
    @Column(name = "net_sale_currency_code", length = 10)
    String netSaleCurrencyCode;

    /* Net Sale Amount */
    @Column(name = "net_sale_amount")
    Double netSaleAmount;

    /* Net Sale Minimum */
    @Column(name = "net_sale_minimum")
    Double netSaleMinimum;

    /* total Net Sale */
    @Column(name = "total_net_sale")
    Double totalNetSale;

    /* Local Net Sale ROE */
    @Column(name = "local_net_sale_roe")
    Double localNetSaleRoe;

    /* Local Amount for Net Sale */
    @Column(name = "local_total_net_sale")
    Double localTotalNetSale;


    /* Bill To Type */
    @Column(name = "bill_to_type")
    @Enumerated(EnumType.STRING)
    BillToType billToType;


    /* Declared Sale Currency id */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "declared_sale_currency_id", foreignKey = @ForeignKey(name = "FK_SHIPRATE_DECSALECURRID"))
    @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
    CurrencyMaster declaredSaleCurrency;

    /* Declared Sale Currency Code */
    @Column(name = "declared_sale_currency_code", length = 10)
    String declaredSaleCurrencyCode;

    /* Declared Sale Amount */
    @Column(name = "declared_sale_amount")
    Double declaredSaleAmount;

    /* Declared Sale Minimum */
    @Column(name = "declared_sale_minimum")
    Double declaredSaleMinimum;


    /* total Declared Sale */
    @Column(name = "total_declared_sale")
    Double totalDeclaredSale;

    /* Local Declared Sale ROE */
    @Column(name = "local_declared_sale_roe")
    Double localDeclaredSaleRoe;

    /* Local Amount for Declared Sale */
    @Column(name = "local_total_declared_sale")
    Double localTotalDeclaredSale;


    /* Rate Currency Id */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "rate_currency_id", foreignKey = @ForeignKey(name = "FK_SHIPRATE_RATECURRENCYID"))
    @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
    CurrencyMaster rateCurrency;

    /* Rate Currency Code */
    @Column(name = "rate_currency_code", length = 10)
    String rateCurrencyCode;

    /* Rate Amount */
    @Column(name = "rate_amount")
    Double rateAmount;

    /* Rate Minimum */
    @Column(name = "rate_minimum")
    Double rateMinimum;


    /* total Rate Amount */
    @Column(name = "total_rate_amount")
    Double totalRateAmount;

    /* Local Rate Amount ROE */
    @Column(name = "local_rate_amount_roe")
    Double localRateAmountRoe;

    /* Local Amount for Rate Amount */
    @Column(name = "local_total_rate_amount")
    Double localTotalRateAmount;


    /* Cost Currency Id */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "cost_currency_id", foreignKey = @ForeignKey(name = "FK_SHIPRATE_COSTCURRENCYID"))
    @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
    CurrencyMaster costCurrency;

    /* Cost Currency Code */
    @Column(name = "cost_currency_code", length = 10)
    String costCurrencyCode;

    /* Cost Amount */
    @Column(name = "cost_amount")
    Double costAmount;

    /* Cost Minimum */
    @Column(name = "cost_minimum")
    Double costMinimum;


    /* total Cost Amount */
    @Column(name = "total_cost_amount")
    Double totalCostAmount;

    /* Local Cost Amount ROE */
    @Column(name = "local_cost_amount_roe")
    Double localCostAmountRoe;

    /* Local Amount for Cost Amount */
    @Column(name = "local_total_cost_amount")
    Double localTotalCostAmount;

    /* Vendor id */
    @ManyToOne
    @JoinColumn(name = "vendor_id", foreignKey = @ForeignKey(name = "FK_SHIPRATE_VENDORID"))
    @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
    PartyMaster vendor;

    /* Vendor Code */
    @Column(name = "vendor_code")
    String vendorCode;

	
	/*//used for displaying charges in hawb bill based on yes/no
     Protect
	@Column(name = "is_house_charge", nullable = false)
	@Enumerated(EnumType.STRING)
	@JsonSerialize(using = JsonYesNoSerializer.class)
	@JsonDeserialize(using = JsonYesNoDeserializer.class)
	YesNo isHouseCharge;
	*/

    // actual_chargeable
    @Column(name = "actual_chargeable")
    @Enumerated(EnumType.STRING)
    ActualChargeable actualChargeable;

    /* Note */
    @Column(name = "note")
    @Lob
    @Type(type = "org.hibernate.type.MaterializedClobType")
    String note;


    /* Local Currency Id */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "local_currency_id", foreignKey = @ForeignKey(name = "FK_SHIPRATE_LOCALCURR"))
    @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
    CurrencyMaster localCurrency;

    /* Local Currency Code */
    @Column(name = "local_currency_code", length = 10)
    String localCurrencyCode;

    /* Local Amount */
    @Column(name = "local_amount")
    Double localAmount = 0.0;

    /* Due */
    @Column(name = "due")
    @Enumerated(EnumType.STRING)
    DUE due;

    /* quotation Id */
    @Column(name = "quotation_id")
    Long quotationId;

    @Transient
    String revisionType;

    @Transient
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date auditDate;

}
