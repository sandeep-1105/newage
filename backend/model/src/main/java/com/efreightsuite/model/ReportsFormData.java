package com.efreightsuite.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;


@Entity
@Getter
@Setter
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Table(name = "efs_reports_form_data")
public class ReportsFormData implements Serializable {
    private static final long serialVersionUID = 1L;

    /*Auto Generation Id*/
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    String labelName;

    String modelName;

    String dataType;

    int minLength;

    int maxLength;

    boolean isRequired;

    String datePickerFormat;

    String timePickerFormat;

    String datetimePickerFormat;

    String defaultDate;//Today

    String defaultTime;//now

    boolean defaultBoolean;

    String colWidth;

    String userdefinedArray;//Static Data for dropdown like ['ACTIVE', 'BLOCKED', 'DELETED']

    //LOV Options

    String listStyle;

    String autocompleteSearchMTD;

    String renderItem;

    String dynamicmaster;

    int rowNo;

    String selectedValue;
    String lovGeneralRender;

    @ManyToOne
    @JoinColumn(name = "report_id", foreignKey = @ForeignKey(name = "FK_REPORTFORMDATA_PID"))
    @JsonBackReference
    Reports report;


}  
