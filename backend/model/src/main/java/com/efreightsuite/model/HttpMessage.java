package com.efreightsuite.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Version;

import com.efreightsuite.util.JsonDateSerializer;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Getter;
import lombok.Setter;


@Entity
@Getter
@Setter
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Table(name = "efs_http_message")
public class HttpMessage implements Serializable {

    private static final long serialVersionUID = 1L;

    /* Auto Generation Id */
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    @Version
    @Column(name = "version_lock")
    long versionLock;

    /*Created Track Id */
    @Column(name = "track_id", length = 100)
    String trackId;

    /*Created Response Code */
    @Column(name = "response_code", length = 50)
    int responseCode;


    /*Created IP Address */
    @Column(name = "ip_address", length = 50)
    String ipAddress;

    /*Created Method */
    @Column(name = "http_method", length = 30)
    String requestMethod;

    /*Created PathInfo */
    @Column(name = "path_info", length = 500)
    String pathInfo;

    /*Created Request Url */
    @Column(name = "request_url", length = 3000)
    String requestURL;

    /*Created Header Names */
    @Column(name = "servlet_path", length = 300)
    String servletPath;

    /*Created Context Path */
    @Column(name = "context_path", length = 300)
    String contextPath;

    /*Created Date */
    @Column(name = "create_date")
    @JsonSerialize(using = JsonDateSerializer.class)
    Date createDate;

    /*Last Updated Date */
    @Column(name = "last_updated_date")
    @JsonSerialize(using = JsonDateSerializer.class)
    Date lastUpdatedDate;


    /*Request Content */
    @Column(name = "request_content")
    @Lob
    byte[] requestContent;

    /*Response Content */
    @Column(name = "response_content")
    @Lob
    byte[] responseContent;

}
