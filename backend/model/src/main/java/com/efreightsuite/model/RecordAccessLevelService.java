package com.efreightsuite.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Version;

import com.efreightsuite.dto.RecordAccessLevelServiceDTO;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;


@Entity
@Getter
@Setter
@Table(name = "efs_record_acc_lvl_serv")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class RecordAccessLevelService implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    @Version
    @Column(name = "version_lock")
    long versionLock;

    @Embedded
    SystemTrack systemTrack;

    @ManyToOne
    @JoinColumn(name = "record_access_level_id", foreignKey = @ForeignKey(name = "FK_RALSE_RAL_ID"))
    @JsonBackReference
    RecordAccessLevel recordAccessLevel;

    @ManyToOne
    @JoinColumn(name = "service_id", foreignKey = @ForeignKey(name = "FK_RALSE_SERVICE_ID"))
    ServiceMaster serviceMaster;


    public RecordAccessLevelService() {
        // TODO Auto-generated constructor stub
    }

    public RecordAccessLevelService(RecordAccessLevelServiceDTO dto) {
        this.id = dto.getId();
        this.versionLock = dto.getVersionLock();
        this.systemTrack = dto.getSystemTrack();
        this.serviceMaster = dto.getServiceMaster();
    }
}
