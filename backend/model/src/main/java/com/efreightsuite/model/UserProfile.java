package com.efreightsuite.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.persistence.*;

import com.efreightsuite.dto.RecordAccessLevelProcessDTO;
import com.efreightsuite.dto.RoleDTO;
import com.efreightsuite.dto.UserCompanyDTO;
import com.efreightsuite.dto.UserProfileDto;
import com.efreightsuite.dto.WorkFlowGroupDTO;
import com.efreightsuite.enumeration.UserStatus;
import com.efreightsuite.util.JsonDateDeserializer;
import com.efreightsuite.util.JsonDateSerializer;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Type;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;

@Entity
@Getter
@Setter
@Table(name = "efs_user_profile",
        uniqueConstraints = {
                @UniqueConstraint(columnNames = {"user_name"}, name = "UK_USERPROFILE_USERNAME"),
                @UniqueConstraint(columnNames = {"reset_token_key"}, name = "UK_USERPROFILE_RESETTOKEN")
        },
        indexes = {
                @Index(name = "IDX_USERNAME", columnList = "user_name")
        }

)
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class UserProfile implements UserDetails, Serializable {
    private static final long serialVersionUID = 1L;

    /* Auto Generation Id */
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    @Version
    @Column(name = "version_lock")
    long versionLock;

    /* Create/Last User and Data Info */
    @Embedded
    SystemTrack systemTrack;

    /* User Name */
    @Column(name = "user_name", nullable = false, length = 30)
    String userName;

    /* Password */
    @Column(name = "password", nullable = false)
    String password;

    /* Reset Token Key */
    @Column(name = "reset_token_key", length = 30)
    String resetTokenKey;

    /* Reset Token Date */
    @Column(name = "reset_token_date")
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date resetTokenDate;

    /* Employee Id */
    @Transient
    EmployeeMaster employee;


    @ManyToMany
    @JoinTable(name = "efs_user_has_role", joinColumns = {@JoinColumn(name = "user_id")}, inverseJoinColumns = {@JoinColumn(name = "role_id")},
            uniqueConstraints = {@UniqueConstraint(
                    columnNames = {"user_id", "role_id"}, name = "UK_USER_ROLE")})
    List<Role> roleList = new ArrayList<>();

    @Transient
    Map<String, Boolean> featureMap = new HashMap<>();


    @OneToMany(mappedBy = "userProfile", cascade = CascadeType.ALL, orphanRemoval = true)
    @JsonManagedReference
    List<UserCompany> userCompanyList = new ArrayList<>();

    @Transient
    CompanyMaster selectedCompany;

    @Transient
    LocationMaster selectedUserLocation;


    /* Password Expires On */
    @Column(name = "password_expires_on", nullable = true)
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date passwordExpiresOn;

    /* Note */
    @Column(name = "note")
    @Lob
    @Type(type = "org.hibernate.type.MaterializedClobType")
    String note;

    /* User Status */
    @Column(name = "status", nullable = false, length = 20)
    @Enumerated(EnumType.STRING)
    UserStatus status;

    /*Activated User Name */
    @Column(name = "activated_user", length = 30)
    String activatedUser;

    /*Activated Date */
    @Column(name = "activated_date")
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date activatedDate;

    /*Deactivated User Name */
    @Column(name = "deactivated_user", length = 30)
    String deactivatedUser;

    /*Deactivated Date */
    @Column(name = "deactivated_date")
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date deactivatedDate;

    @ManyToMany
    @JoinTable(name = "efs_user_work_flow", joinColumns = {@JoinColumn(name = "user_id")}, inverseJoinColumns = {@JoinColumn(name = "work_flow_id")},
            uniqueConstraints = {@UniqueConstraint(
                    columnNames = {"user_id", "work_flow_id"}, name = "UK_USER_WORK_FLOW")})
    List<WorkFlowGroup> workFlowList = new ArrayList<>();

    @Transient
    String sessionId;

    @Transient
    String reportServerBasePath;

    @Transient
    Map<String, Object> appMasterData = new HashMap<>();

    @Transient
    RecordAccessLevelProcessDTO recordAccessLevelProcessDTO = null;

    public UserProfile() {
    }

    public UserProfile(Long id, String userName) {
        this.id = id;
        this.userName = userName;
    }

    public UserProfile(Long id) {
        this.id = id;
    }

    public UserProfile(UserProfileDto dto) {
        this.id = dto.getId();
        this.versionLock = dto.getVersionLock();
        this.systemTrack = dto.getSystemTrack();
        this.userName = dto.getUserName();
        this.password = dto.getPassword();
        this.resetTokenKey = dto.getResetTokenKey();
        this.resetTokenDate = dto.getResetTokenDate();
        this.employee = dto.getEmployee();
        this.passwordExpiresOn = dto.getPasswordExpiresOn();
        this.activatedUser = dto.getActivatedUser();
        this.activatedDate = dto.getActivatedDate();
        this.deactivatedDate = dto.getDeactivatedDate();
        this.deactivatedUser = dto.getDeactivatedUser();
        this.note = dto.getNote();
        this.sessionId = dto.getSessionId();
        this.reportServerBasePath = dto.getReportServerBasePath();
        this.status = dto.getStatus();
        this.selectedCompany = dto.getSelectedCompany();
        this.selectedUserLocation = dto.getSelectedUserLocation();


        this.appMasterData = dto.getAppMasterData();
        this.featureMap = dto.getFeatureMap();

        if (dto.getRoleList() != null) {
            this.setRoleList(new ArrayList<>());
            for (RoleDTO role : dto.getRoleList()) {
                this.getRoleList().add(new Role(role));
            }
        }

        if (dto.getUserCompanyList() != null) {
            this.setUserCompanyList(new ArrayList<>());
            for (UserCompanyDTO us : dto.getUserCompanyList()) {
                UserCompany userCompnay = new UserCompany(us);
                userCompnay.setUserProfile(this);
                this.getUserCompanyList().add(userCompnay);
            }
        }

        if (dto.getWorkFlowList() != null) {
            for (WorkFlowGroupDTO wfg : dto.getWorkFlowList()) {
                this.workFlowList.add(new WorkFlowGroup(wfg));
            }
        }

    }

    public UserProfile(User user) {
        this.userName = user.getUsername();
        this.password = user.getPassword();
    }

    public String getEmployeeEmail() {
        return this.getEmployee() != null ? this.getEmployee().getEmail() : null;
    }

    public String getUserName() {
        return this.userName;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return this.roleList == null
                ? Collections.emptyList()
                : this.roleList.stream().map(role -> new SimpleGrantedAuthority(role.getRoleName())).collect(Collectors.toList());
    }

    @Override
    public String getUsername() {
        return this.userName;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }



}

