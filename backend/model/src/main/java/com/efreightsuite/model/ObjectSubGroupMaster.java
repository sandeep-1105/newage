package com.efreightsuite.model;


import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.UniqueConstraint;
import javax.persistence.Version;

import com.efreightsuite.enumeration.LovStatus;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;


@Entity
@Getter
@Setter
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Table(name = "efs_object_sub_group_master",
        uniqueConstraints = {
                @UniqueConstraint(columnNames = {"object_sub_group_code"}, name = "UK_OBJECTSUBGP_OBJECTSUBGPCODE"),
                @UniqueConstraint(columnNames = {"object_sub_group_name"}, name = "UK_OBJECTSUBGP_OBJECTSUBGPNAME")}

)
public class ObjectSubGroupMaster implements Serializable {


    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    @Version
    @Column(name = "version_lock")
    long versionLock;

    @Embedded
    SystemTrack systemTrack;

    @JoinColumn(name = "object_group_id", foreignKey = @ForeignKey(name = "FK_OBJSUBGRP_OBJGRP"))
    @ManyToOne
    ObjectGroupMaster objectGroupMaster;

    @Column(name = "object_sub_group_code", nullable = false, length = 10)
    String objectSubGroupCode;

    @Column(name = "object_sub_group_name", nullable = false, length = 100)
    String objectSubGroupName;

    @Column(name = "status", nullable = false, length = 10)
    @Enumerated(EnumType.STRING)
    LovStatus status;
}
