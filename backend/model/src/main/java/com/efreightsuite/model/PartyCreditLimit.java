package com.efreightsuite.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Version;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;


@Entity
@Getter
@Setter
@Table(name = "efs_party_credit_limit")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class PartyCreditLimit implements Serializable {
    private static final long serialVersionUID = 1L;

    /*Auto Generation Id*/
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    @Version
    @Column(name = "version_lock")
    long versionLock;

    /*Create/Last User and Data Info*/
    @Embedded
    SystemTrack systemTrack;

    /*Party id*/
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "party_id", foreignKey = @ForeignKey(name = "FK_PARTYCREDLMT_PARTYID"))
    @JsonBackReference
    PartyMaster partyMaster;

    /*Company Id*/
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "company_id", foreignKey = @ForeignKey(name = "FK_PARTYCREDLMT_COMPANYID"))
    CompanyMaster companyMaster;

    /*company Code*/
    @Column(name = "company_code", length = 10)
    String companyCode;

    /*Division id*/
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "division_id", foreignKey = @ForeignKey(name = "FK_PARTYCREDLMT_DIVISIONID"))
    DivisionMaster divisionMaster;

    @Column(name = "division_code", length = 100)
    String divisionCode;

    /*Country Id*/
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "country_id", foreignKey = @ForeignKey(name = "FK_PARTYCREDLMT_COUNTRYID"))
    CountryMaster countryMaster;

    /*Country Code*/
    @Column(name = "country_code", length = 10)
    String countryCode;

    /*Service Id*/
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "service_id", foreignKey = @ForeignKey(name = "FK_PARTYCREDLMT_SERVICEID"))
    ServiceMaster serviceMaster;

    @Column(name = "service_code", length = 100)
    String serviceCode;

    /*Location Id*/
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "location_id", foreignKey = @ForeignKey(name = "FK_PARTYCREDLMT_LOCATIONID"))
    LocationMaster locationMaster;

    @Column(name = "location_code", length = 100)
    String locationCode;

    /*Credit Day*/
    @Column(name = "credit_days")
    Integer creditDays;

    /*Credit Amount*/
    @Column(name = "credit_amount")
    Double creditAmount;

    /*Published credit day*/
    @Column(name = "publish_credit_days")
    Integer publishCreditDays;

    /*Customer email*/
    @Column(name = "customer_email")
    String customerEmail;

    /*Auto Trigger Email*/
    @Column(name = "auto_trigger_email")
    String autoTriggerEmail;

    /*Customer Operation Email*/
    @Column(name = "customer_operation_email")
    String customerOpeationEmail;

    /*Invoice Email*/
    @Column(name = "invoice_mail")
    String invoiceMail;

    public PartyMaster getPartyMaster() {

        if (this.partyMaster == null)
            return null;

        PartyMaster tm = new PartyMaster();
        tm.setId(partyMaster.getId());
        tm.setPartyCode(partyMaster.getPartyCode());
        tm.setPartyName(partyMaster.getPartyName());
        return tm;
    }
}
