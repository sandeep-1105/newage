package com.efreightsuite.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Transient;
import javax.persistence.Version;

import com.efreightsuite.enumeration.TransportMode;
import com.efreightsuite.enumeration.WhoRouted;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Where;


@Entity
@Getter
@Setter
@Table(name = "efs_pricing_master")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class PricingMaster implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    @Version
    @Column(name = "version_lock")
    long versionLock;

    @Embedded
    SystemTrack systemTrack;

    @ManyToOne
    @JoinColumn(name = "company_id", nullable = false, foreignKey = @ForeignKey(name = "FK_PRICMASTER_COMPID"))
    CompanyMaster companyMaster;

    @Column(name = "company_code", nullable = false, length = 30)
    String companyCode;

    @ManyToOne
    @JoinColumn(name = "location_id", nullable = false, foreignKey = @ForeignKey(name = "FK_PRICMASTER_LOCTID"))
    LocationMaster locationMaster;

    @Column(name = "location_code", nullable = false, length = 30)
    String locationCode;

    @ManyToOne
    @JoinColumn(name = "country_id", nullable = false, foreignKey = @ForeignKey(name = "FK_PRICMASTER_COUNTID"))
    CountryMaster countryMaster;

    @Column(name = "country_code", nullable = false, length = 30)
    String countryCode;

    @Column(name = "transport_mode", nullable = false, length = 10)
    @Enumerated(EnumType.STRING)
    TransportMode transportMode;

    @ManyToOne
    @JoinColumn(name = "origin_port_id", foreignKey = @ForeignKey(name = "FK_PRICMASTER_ORIGINID"))
    PortMaster origin;

    @Column(name = "origin_port_code", length = 30)
    String originPortCode;

    @ManyToOne
    @JoinColumn(name = "destination_port_id", foreignKey = @ForeignKey(name = "FK_PRICMASTER_DESTID"))
    PortMaster destination;

    @Column(name = "destination_port_code", length = 30)
    String destinationPortCode;

    @ManyToOne
    @JoinColumn(name = "transit_port_id", foreignKey = @ForeignKey(name = "FK_PRICMASTER_TRANSITID"))
    PortMaster transit;

    @Column(name = "transit_port_code", length = 30)
    String transitPortCode;

    /*Who Routed*/
    @Column(name = "who_routed")
    @Enumerated(EnumType.STRING)
    WhoRouted whoRouted;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "pricing_master_id", referencedColumnName = "id")
    @JsonManagedReference
    @Where(clause = " hazardous = 'No' AND charge_type = 'Origin'")
    List<PricingPortPair> generalOriginPriceList = new ArrayList<>();

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "pricing_master_id", referencedColumnName = "id")
    @JsonManagedReference
    @Where(clause = " hazardous = 'No' AND charge_type = 'Destination'")
    List<PricingPortPair> generalDestinationPriceList = new ArrayList<>();

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "pricing_master_id", referencedColumnName = "id")
    @JsonManagedReference
    @Where(clause = " hazardous = 'No' AND charge_type = 'Freight'")
    List<PricingCarrier> generalFreightPriceList = new ArrayList<>();

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "pricing_master_id", referencedColumnName = "id")
    @Where(clause = " hazardous = 'Yes' AND charge_type = 'Origin'")
    @JsonManagedReference
    List<PricingPortPair> hazardousOriginPriceList = new ArrayList<>();

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "pricing_master_id", referencedColumnName = "id")
    @Where(clause = " hazardous = 'Yes' AND charge_type = 'Destination'")
    @JsonManagedReference
    List<PricingPortPair> hazardousDestinationPriceList = new ArrayList<>();

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "pricing_master_id", referencedColumnName = "id")
    @Where(clause = " hazardous = 'Yes' AND charge_type = 'Freight'")
    @JsonManagedReference
    List<PricingCarrier> hazardousFreightPriceList = new ArrayList<>();


    @OneToMany(mappedBy = "pricingMaster", cascade = CascadeType.ALL, orphanRemoval = true)
    @JsonManagedReference
    List<PricingAttachment> attachmentList = new ArrayList<>();

    @Transient
    Map<String, List<PricingPortPair>> gOriginMap;

    @Transient
    Map<String, List<PricingPortPair>> gDestinationMap;

    @Transient
    Map<String, List<PricingCarrier>> gFreightMap;

    @Transient
    Map<String, List<PricingPortPair>> hOriginMap;

    @Transient
    Map<String, List<PricingPortPair>> hDestinationMap;

    @Transient
    Map<String, List<PricingCarrier>> hFreightMap;

    public void loadMap() {
        gOriginMap = new HashMap<>();
        if (generalOriginPriceList != null && generalOriginPriceList.size() != 0) {
            for (PricingPortPair pp : generalOriginPriceList) {
                if (gOriginMap.get(pp.getChargeMaster().getChargeCode()) == null) {
                    List<PricingPortPair> tmpList = new ArrayList<>();
                    tmpList.add(pp);
                    gOriginMap.put(pp.getChargeMaster().getChargeCode(), tmpList);
                } else {
                    gOriginMap.get(pp.getChargeMaster().getChargeCode()).add(pp);
                }
            }
        }

        gDestinationMap = new HashMap<>();
        if (generalDestinationPriceList != null && generalDestinationPriceList.size() != 0) {
            for (PricingPortPair pp : generalDestinationPriceList) {
                if (gDestinationMap.get(pp.getChargeMaster().getChargeCode()) == null) {
                    List<PricingPortPair> tmpList = new ArrayList<>();
                    tmpList.add(pp);
                    gDestinationMap.put(pp.getChargeMaster().getChargeCode(), tmpList);
                } else {
                    gDestinationMap.get(pp.getChargeMaster().getChargeCode()).add(pp);
                }
            }
        }

        gFreightMap = new HashMap<>();
        if (generalFreightPriceList != null && generalFreightPriceList.size() != 0) {
            for (PricingCarrier pp : generalFreightPriceList) {
                String key = pp.getCarrierMaster().getCarrierCode() + "#" + pp.getChargeMaster().getChargeCode();
                if (gFreightMap.get(key) == null) {
                    List<PricingCarrier> tmpList = new ArrayList<>();
                    tmpList.add(pp);
                    gFreightMap.put(key, tmpList);
                } else {
                    gFreightMap.get(key).add(pp);
                }
            }
        }

        hOriginMap = new HashMap<>();
        if (hazardousOriginPriceList != null && hazardousOriginPriceList.size() != 0) {
            for (PricingPortPair pp : hazardousOriginPriceList) {
                if (hOriginMap.get(pp.getChargeMaster().getChargeCode()) == null) {
                    List<PricingPortPair> tmpList = new ArrayList<>();
                    tmpList.add(pp);
                    hOriginMap.put(pp.getChargeMaster().getChargeCode(), tmpList);
                } else {
                    hOriginMap.get(pp.getChargeMaster().getChargeCode()).add(pp);
                }
            }
        }

        hDestinationMap = new HashMap<>();
        if (hazardousDestinationPriceList != null && hazardousDestinationPriceList.size() != 0) {
            for (PricingPortPair pp : hazardousDestinationPriceList) {
                if (hDestinationMap.get(pp.getChargeMaster().getChargeCode()) == null) {
                    List<PricingPortPair> tmpList = new ArrayList<>();
                    tmpList.add(pp);
                    hDestinationMap.put(pp.getChargeMaster().getChargeCode(), tmpList);
                } else {
                    hDestinationMap.get(pp.getChargeMaster().getChargeCode()).add(pp);
                }
            }
        }

        hFreightMap = new HashMap<>();
        if (hazardousFreightPriceList != null && hazardousFreightPriceList.size() != 0) {
            for (PricingCarrier pp : hazardousFreightPriceList) {
                String key = pp.getCarrierMaster().getCarrierCode() + "#" + pp.getChargeMaster().getChargeCode();
                if (hFreightMap.get(key) == null) {
                    List<PricingCarrier> tmpList = new ArrayList<>();
                    tmpList.add(pp);
                    hFreightMap.put(key, tmpList);
                } else {
                    hFreightMap.get(key).add(pp);
                }
            }
        }
    }

}
