package com.efreightsuite.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Version;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Type;

@Entity
@Getter
@Setter
@Table(name = "efs_quotation_det_gen_note")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class QuotationDetailGeneralNote implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    @Version
    @Column(name = "version_lock")
    long versionLock;

    @Embedded
    SystemTrack systemTrack;

    /* Service */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "quotation_detail_id", foreignKey = @ForeignKey(name = "FK_QUOTGENNOTDET_QUTOTEDETID"))
    @JsonBackReference
    QuotationDetail quotationDetail;

    /*notes */
    @Column(name = "note")
    @Lob
    @Type(type = "org.hibernate.type.MaterializedClobType")
    String note;

}
