package com.efreightsuite.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Version;

import com.efreightsuite.enumeration.YesNo;
import com.efreightsuite.util.JsonDateDeserializer;
import com.efreightsuite.util.JsonDateSerializer;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Getter;
import lombok.Setter;


@Entity
@Getter
@Setter
@Table(name = "efs_shipment_service_event")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class ShipmentServiceEvent implements Serializable {

    private static final long serialVersionUID = 1L;

    /* Auto Generation Id */
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    @Version
    @Column(name = "version_lock")
    long versionLock;

    /* Create/Last User and Data Info */
    @Embedded
    SystemTrack systemTrack;

    /* Shipment Service Detail */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "service_id", nullable = false, foreignKey = @ForeignKey(name = "FK_EVENT_SERVIDETID"))
    @JsonBackReference
    ShipmentServiceDetail shipmentServiceDetail;

    /* TriggerMaster ID*/
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "event_master_id", nullable = false, foreignKey = @ForeignKey(name = "FK_EVENT_EVENTID"))
    EventMaster eventMaster;

    @Column(name = "event_code", length = 100)
    String eventCode;

    /* Date */
    @Column(name = "event_date", nullable = false)
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date eventDate;

    /* Follow Up Required */
    @Column(name = "follow_up_required", nullable = false)
    @Enumerated(EnumType.STRING)
    YesNo followUpRequired = YesNo.No;

    /* Assigned to */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "assigned_id", foreignKey = @ForeignKey(name = "FK_EVENT_ASSIGNEDID"))
    EmployeeMaster assignedTo;

    @Column(name = "assigned_code", length = 100)
    String assignedToCode;

    /* Follow Up Date */
    @Column(name = "follow_up_date")
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date followUpDate;

    /* status */
    @Column(name = "is_completed", nullable = false)
    @Enumerated(EnumType.STRING)
    YesNo isCompleted = YesNo.No;


}
