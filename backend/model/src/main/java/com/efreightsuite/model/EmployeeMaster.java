package com.efreightsuite.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;

import com.efreightsuite.enumeration.EmploymentStatus;
import com.efreightsuite.enumeration.YesNo;
import com.efreightsuite.util.JsonYesNoDeserializer;
import com.efreightsuite.util.JsonYesNoSerializer;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Getter;
import lombok.Setter;


@Entity
@Getter
@Setter
@Table(name = "efs_employee_master", uniqueConstraints = {
        @UniqueConstraint(columnNames = {"employee_code"}, name = "UK_EMPLOYEE_EMPLOYEECODE")})
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class EmployeeMaster implements Serializable {
    private static final long serialVersionUID = 1L;

    /* Auto Generation Id */
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    @Version
    @Column(name = "version_lock")
    long versionLock;

    /* Create/Last User and Data Info */
    @Embedded
    SystemTrack systemTrack;

    /* Location id */
    @ManyToOne
    @JoinColumn(name = "location_id", foreignKey = @ForeignKey(name = "FK_EMPLOYEE_LOCATION_ID"))
    LocationMaster locationMaster;

    @ManyToOne
    @JoinColumn(name = "nominate_employee", foreignKey = @ForeignKey(name = "FK_EMPLOYEE_NOMINATE"))
    EmployeeMaster nominateEmployee;

    /* Company id */
    @ManyToOne
    @JoinColumn(name = "company_id", foreignKey = @ForeignKey(name = "FK_EMPLOYEE_COMPANY_ID"))
    CompanyMaster companyMaster;

    /* Country */
    @ManyToOne
    @JoinColumn(name = "country_id", foreignKey = @ForeignKey(name = "FK_EMPLOYEE_COUNTRYID"))
    CountryMaster countryMaster;

    /* Employee Code */
    @Column(name = "employee_code", nullable = false)
    String employeeCode;

    /* Employee First Name */
    @Column(name = "first_name", length = 100)
    String firstName;

    /* Employee Middle Name */
    @Column(name = "middle_name", length = 100)
    String middleName;

    /* Employee Last Name */
    @Column(name = "last_name", length = 100)
    String lastName;

    /* Employee Alias Name */
    @Column(name = "employee_alias_name", length = 100)
    String aliasName;

    /* Employee Name */
    @Column(name = "employee_name", nullable = false, length = 100)
    String employeeName;

    /* Employment Status */
    @Column(name = "employment_status", nullable = false, length = 15)
    @Enumerated(EnumType.STRING)
    EmploymentStatus employementStatus = EmploymentStatus.EMPLOYED;

    /* Employment Salesman */
    @Column(name = "is_salesman", nullable = false, length = 10)
    @Enumerated(EnumType.STRING)
    @JsonSerialize(using = JsonYesNoSerializer.class)
    @JsonDeserialize(using = JsonYesNoDeserializer.class)
    YesNo isSalesman = YesNo.No;

    /* Employee Phone Number */
    @Column(name = "employee_phone_no")
    String employeePhoneNo;

    /* Employee Email */
    @Column(name = "email", nullable = false, length = 500)
    String email;

    /* Reason For Registration */
    @Column(name = "reason_for_resignation", length = 100)
    String reasonForRegistration;

    /* Employee CC Email */
    @Column(name = "cc_email", length = 500)
    String cc_email;

    /* Employee Image */
    @Column(name = "image")
    @Lob
    byte[] image;

    /* Encoded Image for showing in ui */
    /* Note : this is not a column */
    @Getter
    @Setter
    @Transient
    String encodedImage;

    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "personal_detail_id", foreignKey = @ForeignKey(name = "FK_EMPLOYEE_PERSONAL_ID"))
    EmployeePersonalDetail employeePersonalDetail;

    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "professional_detail_id", foreignKey = @ForeignKey(name = "FK_EMPLOYEE_PROFESSIONAL_ID"))
    EmployeeProfessionalDetail employeeProfessionalDetail;

    @OneToMany(mappedBy = "employeeMaster", cascade = CascadeType.ALL, orphanRemoval = true)
    @JsonManagedReference
    List<EmployeeAttachment> attachmentList = new ArrayList<>();


    public EmployeeMaster() {
    }

    public EmployeeMaster(Long id) {
        this.id = id;
    }

    public EmployeeMaster(Long id, String employeeCode, EmploymentStatus employementStatus) {
        this.id = id;
        this.employeeCode = employeeCode;
        this.employementStatus = employementStatus;
    }

    public void setEmployeeName(String firstName, String lastName) {

        if (firstName == null) {
            firstName = "";
        }

        if (lastName == null) {
            lastName = "";
        }
        String tmp = firstName.trim() + " " + lastName.trim();
        this.setEmployeeName(tmp.trim());
    }

    public EmployeeMaster getNominateEmployee() {

        if (nominateEmployee == null) {
            return null;
        }

        EmployeeMaster emp = new EmployeeMaster();
        emp.setId(id);
        emp.setEmployementStatus(nominateEmployee.getEmployementStatus());
        emp.setFirstName(nominateEmployee.getFirstName());
        emp.setLastName(nominateEmployee.getLastName());
        emp.setMiddleName(nominateEmployee.getMiddleName());
        emp.setEmployeeName(nominateEmployee.getEmployeeName());
        emp.setEmployeeCode(nominateEmployee.getEmployeeCode());

        return emp;
    }

    public EmployeeMaster(String employeeName, String employeeCode, String email, SystemTrack systemTrack) {
        this.employeeName = employeeName;
        this.employeeCode = employeeCode;
        this.email = email;
        this.systemTrack = systemTrack;
    }

    public EmployeeMaster(String employeeName, String employeeCode, String email, YesNo isSalesman, SystemTrack systemTrack) {
        this.employeeName = employeeName;
        this.employeeCode = employeeCode;
        this.email = email;
        this.systemTrack = systemTrack;
        this.isSalesman = isSalesman;
    }

    public void setAttachmentList(List<EmployeeAttachment> attachmentList) {
        this.attachmentList.clear();
        if (attachmentList != null) {
            this.attachmentList.addAll(attachmentList);
        }
    }
}
