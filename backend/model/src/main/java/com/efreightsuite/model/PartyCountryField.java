package com.efreightsuite.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Version;

import com.efreightsuite.enumeration.BondActivityCode;
import com.efreightsuite.enumeration.BondType;
import com.efreightsuite.enumeration.ConsigneeType;
import com.efreightsuite.enumeration.FillingBy;
import com.efreightsuite.enumeration.FillingType;
import com.efreightsuite.enumeration.IdType;
import com.efreightsuite.enumeration.YesNo;
import com.efreightsuite.util.JsonBondTypeDeserializer;
import com.efreightsuite.util.JsonBondTypeSerializer;
import com.efreightsuite.util.JsonDateDeserializer;
import com.efreightsuite.util.JsonDateSerializer;
import com.efreightsuite.util.JsonFillingByDeserializer;
import com.efreightsuite.util.JsonFillingBySerializer;
import com.efreightsuite.util.JsonFillingTypeDeserializer;
import com.efreightsuite.util.JsonFillingTypeSerializer;
import com.efreightsuite.util.JsonYesNoDeserializer;
import com.efreightsuite.util.JsonYesNoSerializer;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Getter;
import lombok.Setter;


@Entity
@Getter
@Setter
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Table(name = "efs_party_country_field")
public class PartyCountryField implements Serializable {
    private static final long serialVersionUID = 1L;

    /*Auto Generation Id*/
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    @Version
    @Column(name = "version_lock")
    long versionLock;

    /*Create/Last User and Data Info*/
    @Embedded
    SystemTrack systemTrack;

    /*Branch SL No*/
    @Column(name = "branch_sl_no")
    String branchSlNo;

    /*Bank DLR code*/
    @Column(name = "bank_dlr_code")
    String bankDLRCode;

    /*Known Shipper Validation no*/
    @Column(name = "known_shipper_validation_no")
    String knownShipperValidationNo;

    /*Known Shipper Validation Date*/
    @Column(name = "known_shipper_validation_date")
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date knownShipperValidationDate;

    /*Id Number*/
    @Column(name = "id_number")
    String idNumber;

    /*Id Type*/
    @Column(name = "id_type")
    @Enumerated(EnumType.STRING)
    IdType idType;

    /*COUNTRY_OF_ISSUANCE*/
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "country_of_issuance", foreignKey = @ForeignKey(name = "FK_PARTYADDRECOUNT_COUNTISS"))
    CountryMaster countryOfissuance;

    @Column(name = "country_of_issuance_code", length = 100)
    String countryOfissuanceCode;

    /* Date of Birth*/
    @Column(name = "date_of_birth")
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date dateOfBirth;


    /*Warehouse Provider Name*/
    @Column(name = "warehouse_provider_name")
    String warehouseProviderName;


    /*Bond Holder*/
    @Column(name = "bond_holder")
    String bondHolder;

    /*Bond Activity Code*/
    @Column(name = "bond_activity_code")
    @Enumerated(EnumType.STRING)
    BondActivityCode bondActivityCode;

    /*Bond Type*/
    @Column(name = "bond_type")
    @Enumerated(EnumType.STRING)
    @JsonSerialize(using = JsonBondTypeSerializer.class)
    @JsonDeserialize(using = JsonBondTypeDeserializer.class)
    BondType bondType;

    /*Bond No*/
    @Column(name = "bond_no")
    String bondNo;

    /*Surety Code*/
    @Column(name = "bond_surety_code")
    String bondSuretyCode;


    /*Import Power of Attorny*/
    @Column(name = "isf_power_of_attorny")
    @Enumerated(EnumType.STRING)
    @JsonSerialize(using = JsonYesNoSerializer.class)
    @JsonDeserialize(using = JsonYesNoDeserializer.class)
    YesNo importPowerOfAttorny;

    /*Import Expiration Date*/
    @Column(name = "isf_expiration_date")
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date importExpirationDate;

    /*AES Filing Type*/
    @Column(name = "aes_filing_type")
    @Enumerated(EnumType.STRING)
    @JsonSerialize(using = JsonFillingTypeSerializer.class)
    @JsonDeserialize(using = JsonFillingTypeDeserializer.class)
    FillingType aesFilingType;

    /*AES consignee Type*/
    @Column(name = "aes_consignee_type")
    @Enumerated(EnumType.STRING)
    ConsigneeType aesConsigneeType;

    /*Export Power of Attorny*/
    @Column(name = "export_power_of_attorny")
    @Enumerated(EnumType.STRING)
    @JsonSerialize(using = JsonYesNoSerializer.class)
    @JsonDeserialize(using = JsonYesNoDeserializer.class)
    YesNo exportPowerOfAttorny;

    /*Export Expiration Date*/
    @Column(name = "export_expiration_date")
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date exportExpirationDate;

    /*AES_TO_BE_FILED_BY*/
    @Column(name = "aes_to_be_filed_by")
    @Enumerated(EnumType.STRING)
    @JsonSerialize(using = JsonFillingBySerializer.class)
    @JsonDeserialize(using = JsonFillingByDeserializer.class)
    FillingBy aesToBeFiledBy;


}
