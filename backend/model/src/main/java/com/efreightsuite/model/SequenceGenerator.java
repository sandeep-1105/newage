package com.efreightsuite.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.UniqueConstraint;
import javax.persistence.Version;

import com.efreightsuite.enumeration.SequenceFormat;
import com.efreightsuite.enumeration.SequenceType;
import com.efreightsuite.enumeration.YesNo;
import com.efreightsuite.util.JsonYesNoDeserializer;
import com.efreightsuite.util.JsonYesNoSerializer;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Table(name = "efs_sequence_generator", uniqueConstraints = {
        @UniqueConstraint(columnNames = {"sequence_type", "company_id"}, name = "UK_SEQUENCE_TYPE"),
        @UniqueConstraint(columnNames = {"sequence_name", "company_id"}, name = "UK_SEQUENCE_NAME")}

)
public class SequenceGenerator implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    @Version
    @Column(name = "version_lock")
    long versionLock;

    @Embedded
    @JsonIgnore
    SystemTrack systemTrack;

    @ManyToOne
    @JoinColumn(name = "company_id", nullable = false, foreignKey = @ForeignKey(name = "FK_SEQUENCE_COMPANY"))
    @JsonBackReference
    CompanyMaster companyMaster;

    @Column(name = "company_code", nullable = false, length = 50)
    String companyCode;


    @Column(name = "sequence_name", length = 50)
    String sequenceName;

    @Column(name = "sequence_type", length = 30)
    @Enumerated(EnumType.STRING)
    SequenceType sequenceType;

    @Column(name = "current_sequence_value")
    long currentSequenceValue;

    @Column(name = "prefix", length = 5)
    String prefix;

    @Column(name = "format")
    @Enumerated(EnumType.STRING)
    SequenceFormat format;

    @Column(name = "is_year", length = 10)
    @Enumerated(EnumType.STRING)
    @JsonSerialize(using = JsonYesNoSerializer.class)
    @JsonDeserialize(using = JsonYesNoDeserializer.class)
    YesNo isYear;

    @Column(name = "is_month", length = 10)
    @Enumerated(EnumType.STRING)
    @JsonSerialize(using = JsonYesNoSerializer.class)
    @JsonDeserialize(using = JsonYesNoDeserializer.class)
    YesNo isMonth;

    @Column(name = "suffix", length = 5)
    String suffix;

    @Column(name = "separator", length = 1)
    String separator;
}
