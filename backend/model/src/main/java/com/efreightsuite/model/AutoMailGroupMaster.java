package com.efreightsuite.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.UniqueConstraint;
import javax.persistence.Version;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Getter;
import lombok.Setter;

/**
 * @author Devendrachary M
 */

@Entity
@Getter
@Setter
@Table(name = "efs_auto_mail_group_master", uniqueConstraints = {
        @UniqueConstraint(columnNames = {"message_group_code"}, name = "UK_AUTOMAIL_GROUP_MSGGRPCODE"),
        @UniqueConstraint(columnNames = {"message_group_name"}, name = "UK_AUTOMAIL_GROUP_MSGGRPNAME")})
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class AutoMailGroupMaster implements Serializable {


    private static final long serialVersionUID = 1L;

    /* Auto Generation Id */
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    @Version
    @Column(name = "version_lock")
    long versionLock;

    /* Create/Last User and Data Info */
    @Embedded
    SystemTrack systemTrack;

    /* Message Group Code */
    @Column(name = "message_group_code", nullable = true, length = 10)
    String messageGroupCode;

    /* Message Group Name */
    @Column(name = "message_group_name", nullable = true, length = 100)
    String messageGroupName;


    /* AutoMailMaster ID */
    @OneToMany(mappedBy = "autoMailGroupMaster", cascade = CascadeType.ALL, orphanRemoval = true)
    @JsonManagedReference
    List<AutoMailMaster> autoMailMasterList = new ArrayList<>();

    public AutoMailGroupMaster() {
    }

    public AutoMailGroupMaster(String messageGroupName, String messageGroupCode, SystemTrack systemTrack) {
        this.messageGroupName = messageGroupName;
        this.messageGroupCode = messageGroupCode;
        this.systemTrack = systemTrack;
    }

    public AutoMailGroupMaster(Long id) {
        this.id = id;
    }


    public void addAutoMail(List<AutoMailMaster> dataList) {
        this.autoMailMasterList = new ArrayList<>();
        for (AutoMailMaster data : dataList) {
            if (data != null) {
                AutoMailMaster am = new AutoMailMaster();
                am.setId(data.getId());
                am.setAutoMailStatus(data.getAutoMailStatus());
                am.setEventType(data.getEventType());
                am.setMessageCode(data.getMessageCode());
                am.setMessageName(data.getMessageName());
                am.setAutoMailGroupMaster(this);
                this.autoMailMasterList.add(am);
            }
        }

    }


}
