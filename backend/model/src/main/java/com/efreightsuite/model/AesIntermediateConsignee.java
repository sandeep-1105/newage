package com.efreightsuite.model;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Version;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;


@Entity
@Getter
@Setter
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Table(name = "efs_aes_intmedi_consig")
public class AesIntermediateConsignee implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    @Version
    @Column(name = "version_lock")
    long versionLock;

    /* Create/Last User and Data Info */
    @Embedded
    SystemTrack systemTrack;

    /* Ultimate Consignee */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "intermediate_consignee_id", foreignKey = @ForeignKey(name = "FK_AES_INTER_CONS_ID"))
    PartyMaster intermediateConsignee;

    @Column(name = "intermediate_consignee_code", length = 100)
    String intermediateConsigneeCode;

    /* firstName */
    @Column(name = "first_name", length = 100)
    String firstName;

    /* lastName */
    @Column(name = "last_name", length = 100)
    String lastName;

    /* Contact No */
    @Column(name = "contact_no", length = 20)
    String contactNo;


    /* company */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "company_id", foreignKey = @ForeignKey(name = "FK_AES_INTER_CONS_COMPANY_ID"))
    PartyMaster company;

    @Column(name = "company_code", length = 100)
    String companyCode;

    /* company Address */
    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "company_address_id", foreignKey = @ForeignKey(name = "FK_AESINTER_CONSCOMPANYADDRID"))
    AddressMaster companyAddress;

    /* City Id */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "city_id", foreignKey = @ForeignKey(name = "FK_AES_INTER_CONS_CITY_ID"))
    CityMaster cityMaster;

    @Column(name = "city_code", length = 100)
    String cityCode;

    /* State or Province */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "state_province", foreignKey = @ForeignKey(name = "FK_AES_INTER_CONS_STATE_ID"))
    StateMaster stateMaster;

    @Column(name = "state_code", length = 100)
    String stateCode;

    /* Country Id */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "country_id", foreignKey = @ForeignKey(name = "FK_AES_INTER_CONS_COUNTRY_ID"))
    CountryMaster countryMaster;

    @Column(name = "country_code", length = 100)
    String countryCode;

    /* Zip Code */
    @Column(name = "zip_code", length = 10)
    String zipCode;
}
