package com.efreightsuite.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.UniqueConstraint;
import javax.persistence.Version;

import com.efreightsuite.enumeration.LovStatus;
import com.efreightsuite.enumeration.TransportMode;
import com.efreightsuite.enumeration.YesNo;
import com.efreightsuite.util.JsonDateDeserializer;
import com.efreightsuite.util.JsonDateSerializer;
import com.efreightsuite.util.JsonYesNoDeserializer;
import com.efreightsuite.util.JsonYesNoSerializer;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Getter;
import lombok.Setter;


@Entity
@Getter
@Setter
@Table(name = "efs_purchase_order",
        uniqueConstraints = {@UniqueConstraint(columnNames = {"po_no"}, name = "UK_PURCHASE_ORDER_NO")}
)
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class PurchaseOrder implements Serializable {
    private static final long serialVersionUID = 1L;

    /*Auto Generation Id*/
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    @Version
    @Column(name = "version_lock")
    long versionLock;

    /*Create/Last User and Data Info*/
    @Embedded
    SystemTrack systemTrack;


    /*Company Id*/
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "company_id", nullable = false, foreignKey = @ForeignKey(name = "FK_PURCHASE_ORDER_COMPANY"))
    CompanyMaster companyMaster;

    @Column(name = "company_code", length = 100)
    String companyCode;

    /* Location Id */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "location_id", nullable = false, foreignKey = @ForeignKey(name = "FK_PURCHASE_ORDER_LOCATION"))
    LocationMaster locationMaster;

    @Column(name = "location_code", length = 100)
    String locationCode;

    /*Buyer id*/
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "buyer_id", foreignKey = @ForeignKey(name = "FK_PURCHASE_ORDER_BUYER"), nullable = false)
    BuyerConsolidationMaster buyer;

    /*Buyer Code*/
    @Column(name = "buyer_code", nullable = false)
    String buyerCode;

    /*Supplier id*/
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "supplier_id", foreignKey = @ForeignKey(name = "FK_PURCHASE_ORDER_SUPPLIER"), nullable = false)
    PartyMaster supplier;

    /*Supplier Code*/
    @Column(name = "supplier_code", nullable = false)
    String supplierCode;

    /*Origin*/
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "origin_id", foreignKey = @ForeignKey(name = "FK_PURCHASE_ORDER_ORIGIN"), nullable = true)
    PortMaster origin;

    /*Origin Code*/
    @Column(name = "origin", nullable = true)
    String originCode;

    /*Destination*/
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "destination_id", foreignKey = @ForeignKey(name = "FK_PURCHASE_ORDER_DESTINATION"), nullable = true)
    PortMaster destination;

    /*Distination Code*/
    @Column(name = "destination", nullable = true)
    String destinationCode;

    /*Purchase Order Number*/
    @Column(name = "po_no", nullable = false)
    String poNo;

    /*Purchase Order Date*/
    @Column(name = "po_date", nullable = false)
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date poDate;

    /*Lov Status*/
    @Column(name = "status", nullable = false, length = 10)
    @Enumerated(EnumType.STRING)
    LovStatus status = LovStatus.Active;

    /*Origin Agent*/
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "origin_agent_id", foreignKey = @ForeignKey(name = "FK_PURCHASE_ORDER_ORIGIN_AGENT"))
    PartyMaster originAgent;

    /*Origin Agent Code*/
    @Column(name = "origin_agent_code")
    String originAgentCode;

    /*Origin Agent*/
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "dest_agent_id", foreignKey = @ForeignKey(name = "FK_PURCHASE_ORDER_DEST_AGENT"))
    PartyMaster destinationAgent;

    /*Origin Agent Code*/
    @Column(name = "dest_agent_code")
    String destinationAgentCode;


    /* Transport Mode */
    @Column(name = "transport_mode", nullable = false, length = 10)
    @Enumerated(EnumType.STRING)
    TransportMode transportMode;

    /*Consol or Direct*/
    @Column(name = "consol_or_direct", nullable = false, length = 10)
    @Enumerated(EnumType.STRING)
    @JsonSerialize(using = JsonYesNoSerializer.class)
    @JsonDeserialize(using = JsonYesNoDeserializer.class)
    YesNo consolRdirect;

    /*TOS Id*/
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "tos_id", foreignKey = @ForeignKey(name = "FK_PURCHASE_ORDER_TOS"), nullable = true)
    TosMaster tosMaster;

    /*TOS Code*/
    @Column(name = "tos_code", nullable = true)
    String tosCode;

    /**
     * More Info Starts Here
     */
    @Column(name = "supplier_reference_no")
    String supplierReferenceNo;

    @Column(name = "supplier_remark", length = 4000)
    String supplierRemark;

    @Column(name = "special_instruction", length = 4000)
    String specialInstruction;

    @Column(name = "received_from")
    String receivedFrom;

    @Column(name = "delivery_to")
    String deliveryTo;

    /*Purchase Order Date*/
    @Column(name = "doc_received_date", nullable = true)
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date docReceivedDate;

    @Column(name = "family_product_code")
    String familyProductCode;

    @Column(name = "bcc_email", length = 500)
    String bccEmail;

    @Column(name = "supplier_email", length = 500)
    String supplierEmail;

    @Column(name = "origin_agent_email", length = 500)
    String originAgentEmail;

    @Column(name = "destination_agent_email", length = 500)
    String destinationAgentEmail;

    @Column(name = "processing_person_email", length = 500)
    String processingPersonEmail;
    /** More Info Ends Here **/


    /**
     * Attachment Starts Here
     **/
    @Column(name = "po_attachment_filename")
    String poAttachmentFileName;

    @Column(name = "po_attachment_date_format", length = 40)
    String poAttachmentDateFormat;

    //Purchase Order Attachment
    @OneToMany(mappedBy = "purchaseOrder", cascade = CascadeType.ALL, orphanRemoval = true)
    @JsonManagedReference
    List<PurchaseOrderAttachment> purchaseOrderAttachmentList = new ArrayList<>();

    //Purchase Order Attachment
    @OneToMany(mappedBy = "purchaseOrder", cascade = CascadeType.ALL, orphanRemoval = true)
    @JsonManagedReference
    List<PurchaseOrderItem> purchaseOrderItemList = new ArrayList<>();

    //Purchase Order Attachment
    @OneToMany(mappedBy = "purchaseOrder", cascade = CascadeType.ALL, orphanRemoval = true)
    @JsonManagedReference
    List<PurchaseOrderRemarkFollowUp> purchaseOrderRemarkFollowUpList = new ArrayList<>();

    //Purchase Order Attachment
    @OneToMany(mappedBy = "purchaseOrder", cascade = CascadeType.ALL, orphanRemoval = true)
    @JsonManagedReference
    List<PurchaseOrderVehicleInfo> purchaseOrderVehicleInfoList = new ArrayList<>();
    /** Attachment Starts Here **/
}
