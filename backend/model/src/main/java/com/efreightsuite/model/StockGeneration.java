package com.efreightsuite.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;
import javax.persistence.Version;

import com.efreightsuite.enumeration.StockStatus;
import com.efreightsuite.util.JsonDateDeserializer;
import com.efreightsuite.util.JsonDateSerializer;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Getter;
import lombok.Setter;


@Entity
@Getter
@Setter
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Table(name = "efs_stock_generation", uniqueConstraints = {
        @UniqueConstraint(columnNames = {"awb_no", "carrier_id", "check_digit"}, name = "UK_STOCK_MAWB_UNIQUE")}

)
public class StockGeneration implements Serializable {


    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 1L;

    /* Auto Generation Id */
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    /* Create/Last User and Data Info */
    @Embedded
    SystemTrack systemTrack;


    /* Carrier Id */
    @ManyToOne
    @JoinColumn(name = "carrier_id", foreignKey = @ForeignKey(name = "FK_STOCK_CARRIERID"), nullable = false)
    CarrierMaster carrier;

    /*Carrier Code*/
    @Column(name = "carrier_code", length = 100, nullable = false)
    String carrierCode;

    /* Consol Id */
    @ManyToOne
    @JoinColumn(name = "consol_id", foreignKey = @ForeignKey(name = "FK_STOCK_CONSOLID"))
    Consol consol;

    /* Consol Id */
    @ManyToOne
    @JoinColumn(name = "shipment_id", foreignKey = @ForeignKey(name = "FK_STOCK_SHIPMENTID"))
    Shipment shipment;

    @Column(name = "service_code")
    String serviceCode;

    @Column(name = "shipment_uid")
    String shipmentUid;

    @Column(name = "service_uid")
    String serviceUid;


    @Column(name = "consol_uid")
    String consolUid;

    /*Carrier ref No*/
    @Column(name = "carrier_ref_no")
    String carrierRefNo;

    /* POD */
    @ManyToOne
    @JoinColumn(name = "por_id", foreignKey = @ForeignKey(name = "FK_STOCK_POR"), nullable = false)
    PortMaster por;

    /*Carrier DO No*/
    @Column(name = "port_code", length = 10, nullable = false)
    String portCode;

    /* Received On */
    @Column(name = "received_on", nullable = false)
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date receivedOn;

    @Column(name = "awb_no", nullable = false)
    Long awbNo;

    @Column(name = "check_digit", nullable = false)
    Long checkDigit;

    /* Lov Status */
    @Column(name = "stock_status", nullable = false, length = 20)
    @Enumerated(EnumType.STRING)
    StockStatus stockStatus;

    @Column(name = "starting_no", nullable = false)
    Long startingNo;

    @Column(name = "ending_no", nullable = false)
    Long endingNo;

    @Column(name = "remind_no")
    Long remindNo;

    @Column(name = "mawb_no", length = 20, nullable = false)
    String mawbNo;


    @Version
    @Column(name = "version_lock")
    long versionLock;

    @Getter
    @Setter
    @Transient
    String statusChange;

    /* Received On */
    @Column(name = "received_on_check")
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date receivedOnCheck;


    @Column(name = "last_used_mawb_date")
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date lastUsedMawbDate;


}
