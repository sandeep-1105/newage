package com.efreightsuite.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Version;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;


@Entity
@Getter
@Setter
@Table(name = "efs_quotation_container")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class QuotationContainer implements Serializable {


    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /* Auto Generation Id */
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    @Version
    @Column(name = "version_lock")
    long versionLock;

    @Embedded
    SystemTrack systemTrack;

    /* QuotationDetail */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "quotation_detail_id", foreignKey = @ForeignKey(name = "FK_QUOTATIONCONT_ID"))
    @JsonBackReference
    QuotationDetail quotationDetail;


    /* container */
    @Column(name = "container_code")
    String containerCode;


    /* No. of Pieces */
    @Column(name = "container_name")
    String containerName;


    /* No. of Pieces */
    @Column(name = "no_ofcontainer")
    int noOfContainer;


}
