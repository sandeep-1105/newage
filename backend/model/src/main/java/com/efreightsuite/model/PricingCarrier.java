package com.efreightsuite.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Version;

import com.efreightsuite.enumeration.ChargeType;
import com.efreightsuite.enumeration.YesNo;
import com.efreightsuite.util.JsonDateDeserializer;
import com.efreightsuite.util.JsonDateSerializer;
import com.efreightsuite.util.JsonYesNoDeserializer;
import com.efreightsuite.util.JsonYesNoSerializer;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Getter;
import lombok.Setter;


@Entity
@Getter
@Setter
@Table(name = "efs_pricing_carrier")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class PricingCarrier implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    @Version
    @Column(name = "version_lock")
    long versionLock;

    @Embedded
    SystemTrack systemTrack;

    @ManyToOne
    @JoinColumn(name = "pricing_master_id", foreignKey = @ForeignKey(name = "FK_PRIC_CARRIER_ID"))
    @JsonBackReference
    PricingMaster pricingMaster;

    @ManyToOne
    @JoinColumn(name = "service_id", foreignKey = @ForeignKey(name = "FK_PC_SERVICE_ID"))
    ServiceMaster serviceMaster;


    @Column(name = "hazardous")
    @Enumerated(EnumType.STRING)
    @JsonSerialize(using = JsonYesNoSerializer.class)
    @JsonDeserialize(using = JsonYesNoDeserializer.class)
    YesNo hazardous;

    @Column(name = "charge_type")
    @Enumerated(EnumType.STRING)
    ChargeType chargeType;

    @ManyToOne
    @JoinColumn(name = "company_id", nullable = false, foreignKey = @ForeignKey(name = "FK_PRIC_CARRIER_COMPID"))
    CompanyMaster companyMaster;

    @Column(name = "company_code", nullable = false, length = 30)
    String companyCode;

    @ManyToOne
    @JoinColumn(name = "location_id", nullable = false, foreignKey = @ForeignKey(name = "FK_PRIC_CARRIER_LOCTID"))
    LocationMaster locationMaster;

    @Column(name = "location_code", nullable = false, length = 30)
    String locationCode;

    @ManyToOne
    @JoinColumn(name = "country_id", nullable = false, foreignKey = @ForeignKey(name = "FK_PRIC_CARRIER_COUNTID"))
    CountryMaster countryMaster;

    @Column(name = "country_code", nullable = false, length = 30)
    String countryCode;

    @ManyToOne
    @JoinColumn(name = "charge_id", foreignKey = @ForeignKey(name = "FK_PRIC_CARRIER_CHARGID"))
    ChargeMaster chargeMaster;

    @Column(name = "charge_code", length = 30)
    String chargeCode;

    @ManyToOne
    @JoinColumn(name = "unit_id", foreignKey = @ForeignKey(name = "FK_PRIC_CARRIER_UNITID"))
    UnitMaster unitMaster;

    @Column(name = "unit_code", length = 30)
    String unitCode;

    @ManyToOne
    @JoinColumn(name = "currency_id", foreignKey = @ForeignKey(name = "FK_PRIC_CARRIER_CURREID"))
    CurrencyMaster currencyMaster;

    @Column(name = "currency_code", length = 30)
    String currencyCode;

    @ManyToOne
    @JoinColumn(name = "carrier_id", foreignKey = @ForeignKey(name = "FK_PRIC_CARRIER_CARRID"))
    CarrierMaster carrierMaster;

    @Column(name = "carrier_code", length = 30)
    String carrierCode;

    @Column(name = "valid_from")
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date validFrom;

    @Column(name = "valid_to")
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date validTo;

    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "minimum_sell_price", foreignKey = @ForeignKey(name = "FK_PRIC_CARRIER_MINSELLID"))
    PricingCarrierSlap minimumSellPrice;

    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "standard_sell_price", foreignKey = @ForeignKey(name = "FK_PRIC_CARRIER_STDSELLID"))
    PricingCarrierSlap standardSellPrice;

    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "cost", foreignKey = @ForeignKey(name = "FK_PRIC_CARRIER_COSTID"))
    PricingCarrierSlap cost;

    @Column(name = "fuel_valid_from")
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date fuelValidFrom;

    @Column(name = "fuel_valid_to")
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date fuelValidTo;

    @Column(name = "fuel_surcharge")
    Double fuelSurcharge;

    @Column(name = "security_charge_valid_from")
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date securityChargeValidFrom;

    @Column(name = "security_charge_valid_to")
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date securityChargeValidTo;

    @Column(name = "security_surcharge")
    Double securitySurcharge;


}
