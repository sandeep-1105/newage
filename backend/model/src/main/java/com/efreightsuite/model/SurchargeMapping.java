package com.efreightsuite.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.UniqueConstraint;
import javax.persistence.Version;

import com.efreightsuite.enumeration.SurchargeType;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;


@Entity
@Getter
@Setter
@Table(name = "efs_surcharge_mapping", uniqueConstraints = {
        @UniqueConstraint(columnNames = {"charge_id"}, name = "UK_SURCHARGE_CHARGEID"),
        @UniqueConstraint(columnNames = {"surcharge_type"}, name = "UK_SURCHARGE_SURCHARGE")
})
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class SurchargeMapping implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    @Version
    @Column(name = "version_lock")
    long versionLock;

    /* Create/Last User and Data Info */
    @Embedded
    SystemTrack systemTrack;

    /* Charge Id */
    @ManyToOne
    @JoinColumn(name = "charge_id", nullable = false, foreignKey = @ForeignKey(name = "FK_SURCHARGE_CHARGEID"))
    ChargeMaster chargeMaster;

    /* Charge Code */
    @Column(name = "charge_code")
    String chargeCode;

    @Column(name = "surcharge_type", nullable = false, length = 60)
    @Enumerated(EnumType.STRING)
    SurchargeType surchargeType;

}
