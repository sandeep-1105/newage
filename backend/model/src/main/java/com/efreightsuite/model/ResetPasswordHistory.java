package com.efreightsuite.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Version;

import com.efreightsuite.util.JsonDateDeserializer;
import com.efreightsuite.util.JsonDateSerializer;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Getter;
import lombok.Setter;


@Entity
@Getter
@Setter
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Table(name = "efs_reset_password_history")
public class ResetPasswordHistory implements Serializable {
    private static final long serialVersionUID = 1L;

    /* Auto Generation Id */
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    @Version
    @Column(name = "version_lock")
    long versionLock;

    /* Created User Name */
    @Column(name = "create_user", length = 30)
    String createUser;

    /* Created Date */
    @Column(name = "create_date")
    @JsonSerialize(using = JsonDateSerializer.class)
    Date createDate;

    /* Last Updated User Name */
    @Column(name = "last_updated_user", length = 30)
    String lastUpdatedUser;

    /* Last Updated Date */
    @Column(name = "last_updated_date")
    @JsonSerialize(using = JsonDateSerializer.class)
    Date lastUpdatedDate;

    /* String url */
    @Column(name = "url", nullable = false, length = 300)
    String url;

    /* Timeout Time */
    @Column(name = "expiry_date")
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date expiryDate;

    /* String status - PENDING,SUCESS */
    @Column(name = "status", length = 10)
    // @Enumerated(EnumType.STRING)
            String status;

    @Column(name = "email_id", nullable = false, length = 100)
    String emailId;

    @Column(name = "error_code", length = 30)
    String errorCode;

    @Column(name = "error_description", length = 1000)
    String errorDescription;

}
