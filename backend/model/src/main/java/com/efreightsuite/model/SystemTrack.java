package com.efreightsuite.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import com.efreightsuite.util.JsonDateDeserializer;
import com.efreightsuite.util.JsonDateSerializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@Embeddable
public class SystemTrack implements Serializable {

    /*Created User Name */
    @Column(name = "create_user", length = 30, nullable = false)
    private String createUser;

    /*Created Date */
    @Column(name = "create_date", nullable = false)
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    private Date createDate;

    /*Last Updated User Name */
    @Column(name = "last_updated_user", length = 30, nullable = false)
    private String lastUpdatedUser;

    /*Last Updated Date */
    @Column(name = "last_updated_date", nullable = false)
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    private Date lastUpdatedDate;

    public SystemTrack() {
    }

    public SystemTrack(String createUser, Date createDate, String lastUpdatedUser, Date lastUpdatedDate) {
        this.createUser = createUser;
        this.createDate = createDate;
        this.lastUpdatedUser = lastUpdatedUser;
        this.lastUpdatedDate = lastUpdatedDate;
    }

    public String getCreateUser() {
        return this.createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public Date getCreateDate() {
        return this.createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getLastUpdatedUser() {
        return this.lastUpdatedUser;
    }

    public void setLastUpdatedUser(String lastUpdatedUser) {
        this.lastUpdatedUser = lastUpdatedUser;
    }

    public Date getLastUpdatedDate() {
        return this.lastUpdatedDate;
    }

    public void setLastUpdatedDate(Date lastUpdatedDate) {
        this.lastUpdatedDate = lastUpdatedDate;
    }
}
