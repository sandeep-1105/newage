package com.efreightsuite.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.UniqueConstraint;
import javax.persistence.Version;

import com.efreightsuite.enumeration.DateLogic;
import com.efreightsuite.enumeration.WhichTransactionDate;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;


@Entity
@Getter
@Setter
@Table(name = "efs_date_configuration", uniqueConstraints = {
        @UniqueConstraint(columnNames = {"service_id", "which_transaction_date"}, name = "UK_DATECONFI_DATE")})
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class DateConfiguration implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    @Version
    @Column(name = "version_lock")
    long versionLock;

    @Embedded
    SystemTrack systemTrack;

    @ManyToOne
    @JoinColumn(name = "service_id", foreignKey = @ForeignKey(name = "FK_DATECONFI_SERVICE"), nullable = false)
    ServiceMaster serviceMaster;

    @Column(name = "service_code", nullable = false)
    String serviceCode;

    @Column(name = "date_logic", nullable = false)
    @Enumerated(EnumType.STRING)
    DateLogic dateLogic;

    @Column(name = "which_transaction_date", nullable = false)
    @Enumerated(EnumType.STRING)
    WhichTransactionDate whichTransactionDate;
}
