package com.efreightsuite.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.UniqueConstraint;
import javax.persistence.Version;

import com.efreightsuite.enumeration.ScopeFlag;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;


@Entity
@Getter
@Setter
@Table(name = "efs_default_master_data",
        uniqueConstraints = {
                @UniqueConstraint(columnNames = {"code", "location_id"}, name = "UK_DEFAULT_MASTER_DATA_CODE")
        })
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class DefaultMasterData implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    @Version
    @Column(name = "version_lock")
    long versionLock;

    @Embedded
    SystemTrack systemTrack;

    @ManyToOne
    @JoinColumn(name = "location_id", foreignKey = @ForeignKey(name = "FK_DEFAMASTERDATA_LOCID"))
    LocationMaster location;

    @Column(name = "location_code", length = 50)
    String locationCode;

    @Column(name = "default_name", length = 255)
    String defaultName;

    @Column(name = "code", nullable = false, length = 255)
    String code;


    @Column(name = "value", nullable = false, length = 4000)
    String value;


    @Column(name = "description", length = 4000)
    String description;

    @ManyToOne
    @JoinColumn(name = "software_id", foreignKey = @ForeignKey(name = "FK_SOFTWARE_SOFTWAREID"))
    SoftwareMaster softwareMaster;

    @ManyToOne
    @JoinColumn(name = "object_group_id", foreignKey = @ForeignKey(name = "FK_OBJECTGROUP_OBJECTGROUPID"))
    ObjectGroupMaster objectGroupMaster;

    @ManyToOne
    @JoinColumn(name = "object_sub_group_id", foreignKey = @ForeignKey(name = "FK_OBJSUBGRP_OBJSUBGRPID"))
    ObjectSubGroupMaster objectSubGroupMaster;

    @Column(name = "scope_flag")
    @Enumerated(EnumType.STRING)
    ScopeFlag scopeFlag;


}
