package com.efreightsuite.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Version;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;


@Entity
@Getter
@Setter
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Table(name = "efs_carrier_address_mapping")
public class CarrierAddressMapping implements Serializable {
    private static final long serialVersionUID = 1L;

    /* Auto Generation Id */
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    @Version
    @Column(name = "version_lock")
    long versionLock;

    /* Create/Last User and Data Info */
    @Embedded
    SystemTrack systemTrack;

    /* Carrier Id */
    @ManyToOne
    @JoinColumn(name = "carrier_id", foreignKey = @ForeignKey(name = "FK_CARRADDR_CARRIERID"))
    @JsonBackReference
    CarrierMaster carrierMaster;

    /* Carrier Code */
    @Column(name = "carrier_code", nullable = false, length = 10)
    String carrierCode;


    /* Location Id */
    @ManyToOne
    @JoinColumn(name = "location_id", foreignKey = @ForeignKey(name = "FK_CARRADDR_LOCATIONID"))
    LocationMaster locationMaster;

    /* Location Code */
    @Column(name = "location_code", nullable = false, length = 10)
    String locationCode;


    /* Party Id */
    @ManyToOne
    @JoinColumn(name = "party_id", foreignKey = @ForeignKey(name = "FK_CARRADDR_PARTYID"))
    PartyMaster partyMaster;

    /* Party Code */
    @Column(name = "party_code", nullable = false, length = 10)
    String partyCode;


    /* Account Number */
    @Column(name = "account_no", nullable = false, length = 100)
    String accountNumber;


}
