package com.efreightsuite.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.*;

import com.efreightsuite.enumeration.LovStatus;
import com.efreightsuite.enumeration.SalutationType;
import com.efreightsuite.enumeration.YesNo;
import com.efreightsuite.util.JsonDateDeserializer;
import com.efreightsuite.util.JsonDateSerializer;
import com.efreightsuite.util.JsonYesNoDeserializer;
import com.efreightsuite.util.JsonYesNoSerializer;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Getter;
import lombok.Setter;


@Entity
@Table(name = "efs_party_master", uniqueConstraints = {
        @UniqueConstraint(columnNames = {"party_code"}, name = "UK_PARTY_PARTYCODE"),
        @UniqueConstraint(columnNames = {"party_name"}, name = "UK_PARTY_PARTYNAME")},
        indexes = {
                @Index(columnList = "country_id", name = "IDX_PARTY_PARTYCOUNTRY"),
                @Index(columnList = "status", name = "IDX_PARTY_PARTYSTATUS")}
)
@Getter
@Setter
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class PartyMaster implements Serializable {
    private static final long serialVersionUID = 1L;

    /* Auto Generation Id */
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    @Version
    @Column(name = "version_lock")
    long versionLock;

    /* Create/Last User and Data Info */
    @Embedded
    SystemTrack systemTrack;

    @Column(name = "is_defaulter", length = 10)
    @Enumerated(EnumType.STRING)
    @JsonSerialize(using = JsonYesNoSerializer.class)
    @JsonDeserialize(using = JsonYesNoDeserializer.class)
    YesNo isDefaulter;

    /* Party Code */
    @Column(name = "party_code", nullable = false, length = 100)
    String partyCode;

    /* Party Name */
    @Column(name = "party_name", nullable = false, length = 100)
    String partyName;

    /*Lov Status*/
    @Column(name = "status", nullable = false, length = 10)
    @Enumerated(EnumType.STRING)
    LovStatus status;

    /* Salutation */
    @Column(name = "salutation", length = 10)
    @Enumerated(EnumType.STRING)
    SalutationType salutation;

    /*First Name */
    @Column(name = "first_name")
    String firstName;

    /*Last Name */
    @Column(name = "last_name")
    String lastName;


    /* Open Date*/
    @Column(name = "open_date")
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date openDate;


    /* Country Id */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "country_id", foreignKey = @ForeignKey(name = "FK_PARTY_COUNTRYID"))
    CountryMaster countryMaster;


    /* Country Code*/
    @Column(name = "country_code", length = 10)
    String countryCode;

    /*Category Id*/
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "category_id", foreignKey = @ForeignKey(name = "FK_PARTY_CATEGORYID"))
    CategoryMaster categoryMaster;

    @Column(name = "category_code", length = 100)
    String categoryCode;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "grade_id", foreignKey = @ForeignKey(name = "FK_PARTY_GRADEID"))
    GradeMaster gradeMaster;

    @Column(name = "grade_name", length = 255)
    String gradeName;

    // Party Detail
    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "party_detail_id", foreignKey = @ForeignKey(name = "FK_PARTY_PARTYDETAILID"))
    PartyMasterDetail partyDetail;

    // List of party type for this Party
    @OneToMany(mappedBy = "partyMaster", cascade = CascadeType.ALL, orphanRemoval = true)
    @JsonManagedReference
    List<PartyAssociateToPartyTypeMaster> partyTypeList = new ArrayList<>();

    // List of party Address for this Party
    @OneToMany(mappedBy = "partyMaster", cascade = CascadeType.ALL, orphanRemoval = true)
    @JsonManagedReference
    List<PartyAddressMaster> partyAddressList = new ArrayList<>();

    // List of party service for this Party
    @OneToMany(mappedBy = "partyMaster", cascade = CascadeType.ALL, orphanRemoval = true)
    @JsonManagedReference
    List<PartyServiceMaster> partyServiceList = new ArrayList<>();

    // List of party account for this Party
    @OneToMany(mappedBy = "partyMaster", cascade = CascadeType.ALL, orphanRemoval = true)
    @JsonManagedReference
    List<PartyAccountMaster> partyAccountList = new ArrayList<>();

    // List of party credit limit for this Party
    @OneToMany(mappedBy = "partyMaster", cascade = CascadeType.ALL, orphanRemoval = true)
    @JsonManagedReference
    List<PartyCreditLimit> partyCreditLimitList = new ArrayList<>();

    // List of party email mapping for this Party
    @OneToMany(mappedBy = "partyMaster", cascade = CascadeType.ALL, orphanRemoval = true)
    @JsonManagedReference
    List<PartyEmailMapping> partyEmailMappingList = new ArrayList<>();


    @OneToMany(mappedBy = "partyMaster", cascade = CascadeType.ALL, orphanRemoval = true)
    @JsonManagedReference
    List<PartyBusinessDetail> partyBusinessDetailList = new ArrayList<>();

    @OneToMany(mappedBy = "partyMaster", cascade = CascadeType.ALL, orphanRemoval = true)
    @JsonManagedReference
    List<PartyCompanyAssociate> partyCompanyAssociateList = new ArrayList<>();

    @OneToMany(mappedBy = "partyMaster", cascade = CascadeType.ALL, orphanRemoval = true)
    @JsonManagedReference
    @OrderBy("firstName ASC")
    List<PartyContact> contactList = new ArrayList<>();


    public PartyMaster() {
    }

    public PartyMaster(Long id) {
        this.id = id;
    }

    public PartyMaster(PartyMaster obj) {
        this.id = obj.getId();
        this.partyCode = obj.getPartyCode();
        this.partyName = obj.getPartyName();
        this.status = obj.getStatus();

    }

    public PartyMaster(String partyName, String partyCode, LovStatus status, SystemTrack systemTrack) {
        this.systemTrack = systemTrack;
        this.partyCode = partyCode;
        this.partyName = partyName;
        this.status = status;
    }

    public void setContactList(List<PartyContact> contactList) {
        this.contactList.clear();
        if (contactList != null) {
            this.contactList.addAll(contactList);
        }
    }

    public void setPartyAccountList(List<PartyAccountMaster> partyAccountList) {
        this.partyAccountList.clear();
        if (partyAccountList != null) {
            this.partyAccountList.addAll(partyAccountList);
        }
    }

    public void setPartyTypeList(List<PartyAssociateToPartyTypeMaster> partyTypeList) {
        this.partyTypeList.clear();
        if (partyTypeList != null) {
            this.partyTypeList.addAll(partyTypeList);
        }
    }

    public void setPartyAddressList(List<PartyAddressMaster> partyAddressList) {
        this.partyAddressList.clear();
        if (partyAddressList != null) {
            this.partyAddressList.addAll(partyAddressList);
        }
    }

    public void setPartyServiceList(List<PartyServiceMaster> partyServiceList) {
        this.partyServiceList.clear();
        if (partyServiceList != null) {
            this.partyServiceList.addAll(partyServiceList);
        }
    }

    public void setPartyCreditLimitList(List<PartyCreditLimit> partyCreditLimitList) {
        this.partyCreditLimitList.clear();
        if (partyCreditLimitList != null) {
            this.partyCreditLimitList.addAll(partyCreditLimitList);
        }
    }

    public void setPartyEmailMappingList(List<PartyEmailMapping> partyEmailMappingList) {
        this.partyEmailMappingList.clear();
        if (partyEmailMappingList != null) {
            this.partyEmailMappingList.addAll(partyEmailMappingList);

        }
    }

    public void setPartyBusinessDetailList(List<PartyBusinessDetail> partyBusinessDetailList) {
        this.partyBusinessDetailList.clear();
        if (partyBusinessDetailList != null) {
            this.partyBusinessDetailList.addAll(partyBusinessDetailList);
        }
    }

    public void setPartyCompanyAssociateList(List<PartyCompanyAssociate> partyCompanyAssociateList) {
        this.partyCompanyAssociateList.clear();
        if (partyCompanyAssociateList != null) {
            this.partyCompanyAssociateList.addAll(partyCompanyAssociateList);
        }
    }
}
