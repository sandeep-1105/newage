package com.efreightsuite.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.UniqueConstraint;
import javax.persistence.Version;

import com.efreightsuite.enumeration.DirectStopOver;
import com.efreightsuite.enumeration.FlightPlanStatus;
import com.efreightsuite.util.JsonDateDeserializer;
import com.efreightsuite.util.JsonDateSerializer;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Getter;
import lombok.Setter;


@Entity
@Getter
@Setter
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Table(name = "efs_flight_plan", uniqueConstraints = {
        @UniqueConstraint(columnNames = {"schedule_id"}, name = "UK_FLIGHTP_SCHEDULEID")})

public class FlightPlan implements Serializable, Cloneable {
    private static final long serialVersionUID = 1L;

    /* Auto Generation Id */
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    @Version
    @Column(name = "version_lock")
    long versionLock;

    /* Create/Last User and Data Info */
    @Embedded
    SystemTrack systemTrack;

    /* Flight Plan UID */
    @Column(name = "schedule_id", nullable = false, length = 50)
    String scheduleId;

    /* FlightPlan Connection List */
    @OneToMany(mappedBy = "flightPlan", cascade = CascadeType.ALL, orphanRemoval = true)
    @JsonManagedReference
    List<FlightPlanConnection> connectionList = new ArrayList<>();

    /* Capacity Reserved */
    @Column(name = "capacity_reserved")
    Double capacityReserved;

    /* Flight Plan Status */
    @Column(name = "flight_status", nullable = false, length = 30)
    @Enumerated(EnumType.STRING)
    FlightPlanStatus flightPlanStatus;


    /* DirectConnecting */
    @Column(name = "direct_stopover", length = 30)
    @Enumerated(EnumType.STRING)
    DirectStopOver directStopOver;

    /* Flight Frequency (in Days) */
    @Column(name = "flight_frequency ")
    Integer flightFrequency;

    /* Effective Upto */
    @Column(name = "effective_upto")
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date effectiveUpto;

    /* Booked */
    @Column(name = "booked")
    Double booked;

    /* Booked */
    @Column(name = "received")
    Double received;

    /* Generated */
    @Column(name = "generated")
    Double generated;

    /* Transit */
    @Column(name = "transit")
    Double transit;

    /* Balance */
    @Column(name = "balance")
    Double balance;

    /* Parent Column Start Here */
    /* Carrier Id */
    @ManyToOne
    @JoinColumn(name = "carrier_id", foreignKey = @ForeignKey(name = "FK_FLIGHTP_CARRIERID"))
    CarrierMaster carrierMaster;

    /* Flight Number */
    @Column(name = "flight_no")
    String flightNo;

    /* POL */
    @ManyToOne
    @JoinColumn(name = "pol_id", foreignKey = @ForeignKey(name = "FK_FLIGHTP_POLID"))
    PortMaster pol;

    /* POD */
    @ManyToOne
    @JoinColumn(name = "pod_id", foreignKey = @ForeignKey(name = "FK_FLIGHTP_PODID"))
    PortMaster pod;

    /* ETD */
    @Column(name = "etd")
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date etd;

    /* ETA */
    @Column(name = "eta")
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date eta;

	/* Parent Column Ends Here */

}
