package com.efreightsuite.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.UniqueConstraint;
import javax.persistence.Version;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;


@Entity
@Getter
@Setter
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Table(name = "efs_port_edi_mapping", uniqueConstraints = {
        @UniqueConstraint(columnNames = {"port_id", "edi_type"}, name = "UK_EDIPORT_EDITYPE")
})
public class PortEdiMapping implements Serializable {
    private static final long serialVersionUID = 1L;

    /* Auto Generation Id */
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    @Version
    @Column(name = "version_lock")
    long versionLock;

    /* Create/Last User and Data Info */
    @Embedded
    SystemTrack systemTrack;


    /* Port Id */
    @JsonBackReference
    @ManyToOne
    @JoinColumn(name = "port_id", foreignKey = @ForeignKey(name = "FK_EDIPORT_PORTID"))
    PortMaster portMaster;

    /* Port Code */
    @Column(name = "port_code", nullable = false, length = 10)
    String portCode;

    /* EDI Type */
    @Column(name = "edi_type", nullable = false, length = 100)
    String ediType;

    /* EDI Sub Type */
    @Column(name = "edi_subtype", length = 100)
    String ediSubtype;

    /* EDI Value */
    @Column(name = "edi_value", nullable = false, length = 100)
    String ediValue;

}
