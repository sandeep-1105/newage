package com.efreightsuite.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Version;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;


@Entity
@Getter
@Setter
@Table(name = "efs_cfs_dimension")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class CfsDimension implements Serializable {

    private static final long serialVersionUID = 1L;

    /* Auto Generation Id */
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    @Version
    @Column(name = "version_lock")
    long versionLock;

    /* Create/Last User and Data Info */
    @Embedded
    SystemTrack systemTrack;

    /* No. of Pieces */
    @Column(name = "no_of_pieces")
    Integer noOfPiece;

    /* length */
    @Column(name = "length")
    int length;

    /* width */
    @Column(name = "width")
    int width;

    /* height */
    @Column(name = "height")
    int height;

    /* Volume Weight(kg) */
    @Column(name = "volume_weight")
    Double volWeight;

    /* Gross Weight (kg) */
    @Column(name = "gross_weight")
    Double grossWeight;

    @Column(name = "gross_weight_in_kg")
    Double grossWeightKg = 0.0;

    /* CFS Receive Entry */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "cfs_receive_id", foreignKey = @ForeignKey(name = "FK_CFSDIM_CFSID"))
    @JsonBackReference
    CfsReceiveEntry cfsReceiveEntry;

    public CfsDimension() {
    }

    public CfsDimension(Integer noOfPiece, int length, int width, int height, Double volWeight, Double grossWeight,
                        Double grossWeightKg) {
        this.noOfPiece = noOfPiece;
        this.length = length;
        this.width = width;
        this.height = height;
        this.volWeight = volWeight;
        this.grossWeight = grossWeight;
        this.grossWeightKg = grossWeightKg;
    }
}
