package com.efreightsuite.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Transient;

import com.efreightsuite.util.JsonDateDeserializer;
import com.efreightsuite.util.JsonDateOnlyDeserializer;
import com.efreightsuite.util.JsonDateOnlySerializer;
import com.efreightsuite.util.JsonDateSerializer;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Getter;
import lombok.Setter;


@Entity
@Getter
@Setter
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Table(name = "efs_recent_history")
public class RecentHistory implements Serializable {
    private static final long serialVersionUID = 1L;

    /*Auto Generation Id*/
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    @Column(name = "title")
    String title;

    @Column(name = "sub_title")
    String subTitle;

    /*State Name*/
    @Column(name = "state_name", length = 100)
    String stateName;

    /*State Param*/
    @Column(name = "state_param", length = 4000)
    String stateParam;

    /*State Category*/
    @Column(name = "state_category")
    String stateCategory;

    /*Service Type*/
    @Column(name = "service_type")
    String serviceType;

    /*Service Code*/
    @Column(name = "service_code")
    String serviceCode;
    /*Service Type*/
    @Column(name = "orgin_and_destination")
    String orginAndDestination;

    /*Location Id*/
    @Column(name = "location_id", nullable = false)
    Long locationId;

    /*User Id*/
    @Column(name = "user_id", nullable = false)
    Long userId;

    @Column(name = "show_service")
    String showService;

    /*Created Date */
    @Column(name = "created_date", nullable = false)
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date createdDate;

    @Transient
    @JsonSerialize(using = JsonDateOnlySerializer.class)
    @JsonDeserialize(using = JsonDateOnlyDeserializer.class)
    Date groupByDate;

    public RecentHistory() {
    }

    public Date getGroupByDate() {    // (2)
        //System.out.println("createdDate ----- "+createdDate);
        return createdDate;
    }
}
