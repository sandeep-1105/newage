package com.efreightsuite.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "efs_win_web_response")
public class WinWebConnectResponse implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;


    /* Auto Generation Id */
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    //unique WIN
    @Column(name = "awb_id")
    Long awbID;

    @Column(name = "transaction_id")
    int transactionID;

    @Column(name = "mawb_id")
    Long mawbId;

    @Column(name = "agent_id")
    int agentId;

    @Column(name = "awb_number")
    String awbNumber;

    @Column(name = "win_status")
    String winStatus;

    @OneToMany(mappedBy = "winWebResponse", cascade = CascadeType.ALL, orphanRemoval = true)
    @JsonManagedReference
    List<WinResponseRemarks> remarks = new ArrayList<>();

    @Column(name = "consol_id")
    String consolUid;

    @Column(name = "agent_name")
    String agentName;

    @Column(name = "contact_id")
    int contactId;

    @Column(name = "contact_name")
    String contactName;

    @Column(name = "date_time")
    String datetime;

    @Column(name = "location_id")
    Long locationId;


}
