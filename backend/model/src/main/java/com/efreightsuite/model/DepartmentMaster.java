package com.efreightsuite.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.UniqueConstraint;
import javax.persistence.Version;

import com.efreightsuite.enumeration.LovStatus;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;


@Entity
@Getter
@Setter
@Table(name = "efs_department_master", uniqueConstraints = {
        @UniqueConstraint(columnNames = {"department_code"}, name = "UK_DEPARTMENT_DEPTCODE"),
        @UniqueConstraint(columnNames = {"department_name"}, name = "UK_DEPARTMENT_DEPTNAME")}

)
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class DepartmentMaster implements Serializable {
    private static final long serialVersionUID = 1L;

    /* Auto Generation Id */
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    /* Version Lock */
    @Version
    @Column(name = "version_lock")
    long versionLock;

    /* Created / Last Updated User and Date Info */
    @Embedded
    SystemTrack systemTrack;

    /* Department Code */
    @Column(name = "department_code", nullable = false, length = 100)
    String departmentCode;

    /* Department Name */
    @Column(name = "department_name", nullable = false, length = 100)
    String departmentName;

    /*Department Head*/
    @ManyToOne
    @JoinColumn(name = "department_head", foreignKey = @ForeignKey(name = "FK_DEPARTMENT_HEAD"))
    EmployeeMaster departmentHead;

    /* Lov Status */
    @Column(name = "status", nullable = false, length = 10)
    @Enumerated(EnumType.STRING)
    LovStatus status;
}
