package com.efreightsuite.model;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Version;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;


@Entity
@Table(name = "efs_airline_edi_house")
@Getter
@Setter
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class AirlineEdiHouse implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    @Version
    @Column(name = "version_lock")
    long versionLock;

    @Embedded
    SystemTrack systemTrack;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "airline_edi_id", foreignKey = @ForeignKey(name = "FK_EDI_HOUSE_EDI_ID"))
    @JsonBackReference
    AirlineEdi airlineEdi;

	/* Login and Location Information */

    /* Location */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "location_id", foreignKey = @ForeignKey(name = "FK_AIR_EDI_HOUSE_LOCATIONID"))
    LocationMaster locationMaster;

    @Column(name = "location_code", length = 30)
    String locationCode;

    /* Company */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "company_id", foreignKey = @ForeignKey(name = "FK_AIR_EDI_HOUSE_COMPANYID"))
    CompanyMaster companyMaster;

    @Column(name = "company_code", length = 30)
    String companyCode;

    /* Country */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "country_id", foreignKey = @ForeignKey(name = "FK_AIR_EDI_HOUSE_COUNTRYID"))
    CountryMaster countryMaster;

    @Column(name = "country_code", length = 30)
    String countryCode;

    @Column(name = "shipment_uid", length = 30)
    String shipmentUid;

    @Column(name = "service_uid", length = 30)
    String serviceUid;

    /*	@Column(name = "document_uid", length = 30)
        String documentUid;
        */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "origin_id", foreignKey = @ForeignKey(name = "FK_EDI_HOUSE_ORIGIN_ID"))
    PortMaster origin;

    @Column(name = "origin_code", length = 100)
    String originCode;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "destination_id", foreignKey = @ForeignKey(name = "FK_EDI_HOUSE_DEST_ID"))
    PortMaster destination;

    @Column(name = "destination_code", length = 100)
    String destinationCode;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "shipper_id", foreignKey = @ForeignKey(name = "FK_EDI_HOUSE_HSHIPPER_ID"))
    PartyMaster shipper;

    @Column(name = "shipper_code", length = 100)
    String shipperCode;

    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "shipper_address_id", foreignKey = @ForeignKey(name = "FK_AIR_HEDI_SHIPPERADDID"))
    AddressMaster shipperAddress;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "consignee_id", foreignKey = @ForeignKey(name = "FK_EDI_HOUSE_HCONSIGNEE_ID"))
    PartyMaster consignee;

    @Column(name = "consignee_code", length = 100)
    String consigneeCode;

    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "consignee_address_id", foreignKey = @ForeignKey(name = "FK_AIR_HEDI_AGENTDESTADDID"))
    AddressMaster consigneeAddress;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "agent_id", foreignKey = @ForeignKey(name = "FK_AIR_EDI_HAGENT_ID"))
    PartyMaster agent;

    @Column(name = "agent_code", length = 100)
    String agentCode;

    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "agent_address_id", foreignKey = @ForeignKey(name = "FK_AIR_HEDI_AGENTADDID"))
    AddressMaster agentAddress;


    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "tos_id", foreignKey = @ForeignKey(name = "FK_EDI_HOUSE_TOS_ID"))
    TosMaster tosMaster;

    @Column(name = "tos_code", length = 100)
    String tosCode;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "commodity_id", foreignKey = @ForeignKey(name = "FK_EDI_HOUSE_COMMODITY_ID"))
    CommodityMaster commodityMaster;

    @Column(name = "commodity_code", length = 100)
    String commodityCode;

    @Column(name = "external_no_of_pieces")
    Long externalNumberOfPieces;

    @Column(name = "gross_weight")
    Double grossWeight;

    @Column(name = "volume_weight")
    Double volumeWeight;

    @Column(name = "document_no")
    String documentNo;

    @Column(name = "hawb_no")
    String hawbNo;

    @Column(name = "chargeble_weight")
    Double chargebleWeight;

    @Column(name = "commodity_desc", length = 4000)
    String commodityDescription;
}
