package com.efreightsuite.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

import com.efreightsuite.enumeration.TemplateType;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Type;


@Entity
@Getter
@Setter
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Table(name = "efs_email_template")
public class EmailTemplate implements Serializable {
    private static final long serialVersionUID = 1L;

    /*Auto Generation Id*/
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;


    // CC address
    String ccEmailAddress;

    // BCC address
    String bccEmailAddress;

    //E-mail Settings
    String smtpServerAddress;

    //From Email Id
    @Column(name = "from_email_id")
    String fromEmailId;

    //From EmailPassword
    @Column(name = "from_email_password")
    String fromEmailPassword;

    @Column(name = "port")
    Integer port;

    /* Content Type */
    @Column(name = "template_type")
    @Enumerated(EnumType.STRING)
    TemplateType templateType;

    /* Subject */
    @Column(name = "subject")
    String subject;

    /*Email Type*/
    @Column(name = "email_type")
    String emailType;

    /*Template*/
    @Column(name = "template")
    @Lob
    @Type(type = "org.hibernate.type.MaterializedClobType")
    String template;

    /*Create/Last User and Data Info*/
    @Embedded
    //@JsonIgnore
            SystemTrack systemTrack;


}
