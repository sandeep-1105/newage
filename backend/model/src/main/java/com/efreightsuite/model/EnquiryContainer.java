package com.efreightsuite.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Version;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

/**
 * @author Devendrachary M
 */

@Entity
@Getter
@Setter
@Table(name = "efs_enquiry_container")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class EnquiryContainer implements Serializable {
    private static final long serialVersionUID = 1L;

    /* Auto Generation Id */
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    @Version
    @Column(name = "version_lock")
    long versionLock;

    // Create/Last User and Data Info
    @Embedded
    SystemTrack systemTrack;

    /* EnquiryDetail */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "enquiry_detail_id", foreignKey = @ForeignKey(name = "FK_ENQUIRYCONTAI_ENQDETID"))
    @JsonBackReference
    EnquiryDetail enquiryDetail;

    /* Container Code*/
    @Column(name = "container_code")
    String containerCode;

    /* Container Name*/
    @Column(name = "container_name")
    String containerName;

    /*No of Container*/
    @Column(name = "no_of_container")
    int noOfContainer;


}
