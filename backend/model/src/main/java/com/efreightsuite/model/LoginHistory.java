package com.efreightsuite.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Version;

import com.efreightsuite.enumeration.LoginStatus;
import com.efreightsuite.enumeration.SessionStatus;
import com.efreightsuite.util.JsonDateDeserializer;
import com.efreightsuite.util.JsonDateSerializer;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Getter;
import lombok.Setter;


@Entity
@Getter
@Setter
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Table(name = "efs_login_history")
public class LoginHistory implements Serializable {
    private static final long serialVersionUID = 1L;

    /* Auto Generation Id */
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    @Version
    @Column(name = "version_lock")
    long versionLock;

    /* User Profile Id */
    @ManyToOne
    @JoinColumn(name = "user_profile_id", foreignKey = @ForeignKey(name = "FK_LOGINHIS_USERPROFILEID"))
    UserProfile userProfile;

    /* User Profile */
    @Column(name = "username", nullable = false, length = 30)
    String userName;

    /* Employee Id */
    @ManyToOne
    @JoinColumn(name = "employee_id", foreignKey = @ForeignKey(name = "FK_LOGINHIS_EMPLOYEEID"))
    EmployeeMaster employee;

    /* Employee Code */
    @Column(name = "employee_code", nullable = false)
    String employeeCode;

    /* Login Time */
    @Column(name = "login_time", nullable = false)
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date loginTime;

    /* Logout Time */
    @Column(name = "logout_time")
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date logoutTime;

    /* Timeout Time */
    @Column(name = "timeout_time")
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date timeoutTime;

    /* SaaS id */
    @Column(name = "saas_id", length = 50)
    String saasId;

    /* Session Id */
    @Column(name = "session_id", nullable = false, length = 50)
    String sessionId;

    /* Source IP */
    @Column(name = "source_ip", nullable = false, length = 20)
    String sourceIp;

    /* Browser */
    @Column(name = "browser", length = 50)
    String browser;

    /* Browser Version */
    @Column(name = "browser_version", length = 50)
    String browserVersion;

    /* Platform */
    @Column(name = "platform", length = 50)
    String platform;

    /* Application */
    @Column(name = "application", length = 50)
    String deviceName;

    /* Login Status */
    @Column(name = "login_status", nullable = false, length = 10)
    @Enumerated(EnumType.STRING)
    LoginStatus loginStatus;

    /* Session Status */
    @Column(name = "session_status", length = 20)
    @Enumerated(EnumType.STRING)
    SessionStatus sessionStatus;
}
