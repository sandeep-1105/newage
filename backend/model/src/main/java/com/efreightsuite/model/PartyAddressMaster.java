package com.efreightsuite.model;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Version;

import com.efreightsuite.enumeration.AddressType;
import com.efreightsuite.enumeration.YesNo;
import com.efreightsuite.util.JsonYesNoDeserializer;
import com.efreightsuite.util.JsonYesNoSerializer;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Getter;
import lombok.Setter;


@Entity
@Table(name = "efs_party_address_master")
@Getter
@Setter
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class PartyAddressMaster implements Serializable {
    private static final long serialVersionUID = 1L;

    /*Auto Generation Id*/
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    @Version
    @Column(name = "version_lock")
    long versionLock;

    /*Create/Last User and Data Info*/
    @Embedded
    SystemTrack systemTrack;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "party_id", foreignKey = @ForeignKey(name = "FK_PARTYADDRE_PARTYID"))
    @JsonBackReference
    PartyMaster partyMaster;

    /*Address Type*/
    @Column(name = "address_type", nullable = false, length = 10)
    @Enumerated(EnumType.STRING)
    AddressType addressType;

    /*Corporate*/
    @Column(name = "corporate", nullable = false, length = 10)
    @Enumerated(EnumType.STRING)
    @JsonSerialize(using = JsonYesNoSerializer.class)
    @JsonDeserialize(using = JsonYesNoDeserializer.class)
    YesNo corporate;

    /*Address Line 1 */
    @Column(name = "address_line_1", length = 100)
    String addressLine1;

    /*Address Line 2 */
    @Column(name = "address_line_2", length = 100)
    String addressLine2;

    /*Address Line 3 */
    @Column(name = "address_line_3", length = 100)
    String addressLine3;

    /*PO Box */
    @Column(name = "po_box", length = 20)
    String poBox;

    /*State or Province*/
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "state_province", foreignKey = @ForeignKey(name = "FK_PARTYADDRE_STATEPROVI"))
    StateMaster stateMaster;

    @Column(name = "state_province_code", length = 100)
    String stateMasterCode;

    /*City Id*/
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "city_id", foreignKey = @ForeignKey(name = "FK_PARTYADDRE_CITYID"))
    CityMaster cityMaster;

    @Column(name = "city_code", length = 100)
    String cityMasterCode;

    /*Zip Code */
    @Column(name = "zip_code", length = 10)
    String zipCode;


    /*Contact Person */
    @Column(name = "contact_person")
    String contactPerson;

    /*Phone */
    @Column(name = "phone", length = 100)
    String phone;

    /*Fax */
    @Column(name = "fax", length = 100)
    String fax;

    /*Mobile No */
    @Column(name = "mobile_no", length = 100)
    String mobileNo;

    /*Email */
    @Column(name = "email", length = 500)
    String email;

    /*Pickup hours Text*/
    @Column(name = "pickup_hours_text", length = 255)
    String pickupHoursText;


    /*Address in Regional Language */
    @Column(name = "address_regional_language", length = 2000)
    String addressRegionalLanguage;

    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    PartyCountryField partyCountryField;

    public PartyMaster getPartyMaster() {

        if (this.partyMaster == null)
            return null;

        PartyMaster tm = new PartyMaster();
        tm.setId(partyMaster.getId());
        tm.setPartyCode(partyMaster.getPartyCode());
        tm.setPartyName(partyMaster.getPartyName());
        return tm;
    }

}
