package com.efreightsuite.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;
import javax.persistence.Version;

import com.efreightsuite.enumeration.DocPrefixDocumentType;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;


@Entity
@Getter
@Setter
@Table(name = "efs_doc_prefix_type",
        uniqueConstraints = {
                @UniqueConstraint(columnNames = {"location_code", "document_type"}, name = "UK_DOCPRETYPE_LOCDOC"),
                @UniqueConstraint(columnNames = {"location_code", "document_type", "report_name"}, name = "UK_DOCPRETYPE_LOCREPORT")
        }
)
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class DocPrefixTypeMapping implements Serializable {
    private static final long serialVersionUID = 1L;

    /*Auto Generation Id*/
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    @Version
    @Column(name = "version_lock")
    long versionLock;

    /*Create/Last User and Data Info*/
    @Embedded
    SystemTrack systemTrack;

    @ManyToOne
    @JoinColumn(name = "doc_prefix_master_id", foreignKey = @ForeignKey(name = "FK_DOCPREMAPP_DOCPREFIXID"))
    @JsonBackReference
    DocumentPrefixMaster documentPrefixMaster;

    @ManyToOne
    @JoinColumn(name = "location_id", foreignKey = @ForeignKey(name = "FK_DOCPREMAPP_LOCATIONID"))
    LocationMaster locationMaster;

    /*Location Code*/
    @Column(name = "location_code", nullable = false, length = 10)
    String locationCode;

    /* Lov Status */
    @Column(name = "document_type", length = 50)
    @Enumerated(EnumType.STRING)
    DocPrefixDocumentType docType;


    /*Report Name*/
    @Column(name = "report_name", nullable = false, length = 100)
    String reportName;

	/*Logo*/

    @Column(name = "logo")
    @Lob
    byte[] logo;

    /* encoded logo for showing in ui */
	/* Note : this is not a column*/
    @Transient
    String encodedLogo;


}
