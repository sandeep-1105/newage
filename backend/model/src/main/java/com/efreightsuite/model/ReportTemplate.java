package com.efreightsuite.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.UniqueConstraint;
import javax.persistence.Version;

import com.efreightsuite.enumeration.BooleanType;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Type;


@Entity
@Getter
@Setter
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Table(name = "efs_report_template", uniqueConstraints = @UniqueConstraint(name = "COMP_UNQ_R_TEMPLATE_CAT",
        columnNames = {"report_category", "report_name"}))
public class ReportTemplate implements Serializable {
    private static final long serialVersionUID = 1L;

    /* Auto Generation Id */
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    /* Subject */
    @Column(name = "report_category")
    String reportCategory;

    /* Email Type */
    @Column(name = "report_name")
    String reportName;

    /* Origin Clearance */
    @Column(name = "default_report", length = 5)
    @Enumerated(EnumType.STRING)
    BooleanType defaultReport;

    /* Template */
    @Column(name = "template")
    @Lob
    @Type(type = "org.hibernate.type.MaterializedClobType")
    String template;

    @OneToMany(mappedBy = "reportTemplate", cascade = CascadeType.ALL, orphanRemoval = true)
    @JsonManagedReference
    List<ReportTemplateParameter> templateParameterList = new ArrayList<>();

    /* Create/Last User and Data Info */
    @Embedded
    // @JsonIgnore
            SystemTrack systemTrack;

    @Version
    @Column(name = "version_lock")
    long versionLock;

}
