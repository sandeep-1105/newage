package com.efreightsuite.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Version;

import com.efreightsuite.enumeration.DimensionUnit;
import com.efreightsuite.enumeration.YesNo;
import com.efreightsuite.util.JsonDimensionUnitDeserializer;
import com.efreightsuite.util.JsonDimensionUnitSerializer;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Getter;
import lombok.Setter;


@Entity
@Getter
@Setter
@Table(name = "efs_quotation_detail")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class QuotationDetail implements Serializable {

    private static final long serialVersionUID = 1L;

    /* Auto Generation Id */
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    @Version
    @Column(name = "version_lock")
    long versionLock;

    @Embedded
    SystemTrack systemTrack;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "quotation_id", foreignKey = @ForeignKey(name = "FK_QUOTATION_DETAIL"))
    Quotation quotation;

    /* Service */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "service_id", foreignKey = @ForeignKey(name = "FK_QUOTATIONDETAIL_SERVICEID"))
    ServiceMaster serviceMaster;

    @Column(name = "service_code", length = 100)
    String serviceCode;


    /* Source  */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "from_port_id", foreignKey = @ForeignKey(name = "FK_QUOTATION_ORIGINID"))
    PortMaster origin;

    @Column(name = "from_port_code", nullable = false, length = 20)
    String originCode;

    /* Destination  */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "to_port_id", foreignKey = @ForeignKey(name = "FK_QUOTATION_DESTINATIONID"))
    PortMaster destination;

    @Column(name = "to_port_code", nullable = false, length = 20)
    String destinationCode;

    /*Tos*/
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "tos_id", foreignKey = @ForeignKey(name = "FK_QUOTATION_TOSID"))
    TosMaster tosMaster;

    @Column(name = "tos_code", length = 20)
    String tosCode;


    /*Commodity id*/
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "commodity_id", foreignKey = @ForeignKey(name = "FK_QUOTATION_COMMODITYID"))
    CommodityMaster commodityMaster;

    @Column(name = "commodity_code", length = 100)
    String commodityCode;


    /* Total No. of Pieces */
    @Column(name = "total_no_of_pieces", nullable = false)
    int totalNoOfPieces;

    /* Quotation No */
    @Column(name = "chargeble_weight")
    double chargebleWeight;

    /* Quotation No */
    @Column(name = "gross_weight")
    double grossWeight;

    /* Quotation No */
    @Column(name = "volume_weight")
    double volumeWeight;

    /*  Dimension Vol Weight(kg) */
    @Column(name = "dimension_vol_weight")
    double dimensionVolWeight;

    /*  Dimension Gross Weight (kg)*/
    @Column(name = "dimension_gross_weight")
    double dimensionGrossWeight;

    /* Dimension Unit */
    @Column(name = "dimension_unit", nullable = false, length = 50)
    @Enumerated(EnumType.STRING)
    @JsonSerialize(using = JsonDimensionUnitSerializer.class)
    @JsonDeserialize(using = JsonDimensionUnitDeserializer.class)
    DimensionUnit dimensionUnit;

    /* Pick Up Address */
    @Column(name = "dimension_unit_value")
    double dimensionUnitValue;

    @Column(name = "dimension_gross_weight_in_kg")
    double dimensionGrossWeightInKg = 0.0;


    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "pickup_address_id", foreignKey = @ForeignKey(name = "FK_QUOTEDETAIL_PICKUP"))
    AddressMaster pickupAddress;

    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "delivery_address_id", foreignKey = @ForeignKey(name = "FK_QUOTEDETAIL_DELIVERY"))
    AddressMaster deliveryAddress;

    /* Ref.No */
    @Column(name = "ref_no", length = 100)
    String refNo;


    /* hazardous */
    @Column(name = "hazardous")
    @Enumerated(EnumType.STRING)
    YesNo hazardous = YesNo.No;

    @Column(name = "process_instance_id")
    String processInstanceId;

    @Column(name = "task_id")
    String taskId;

    @Column(name = "workflow_completed", length = 5)
    @Enumerated(EnumType.STRING)
    YesNo workflowCompleted = YesNo.No;

    @Column(name = "quotation_carrier_id")
    Long quotationCarrierId;

    @OneToMany(mappedBy = "QuotationDetail", cascade = CascadeType.ALL, orphanRemoval = true)
    @JsonManagedReference
    private List<QuotationDimension> quotationDimensionList = new ArrayList<>();

    @OneToMany(mappedBy = "quotationDetail", cascade = CascadeType.ALL, orphanRemoval = true)
    @JsonManagedReference
    private List<QuotationCarrier> quotationCarrierList = new ArrayList<>();

    @OneToMany(mappedBy = "quotationDetail", cascade = CascadeType.ALL, orphanRemoval = true)
    @JsonManagedReference
    private List<QuotationValueAddedService> quotationValueAddedServiceList = new ArrayList<>();

    @OneToMany(mappedBy = "quotationDetail", cascade = CascadeType.ALL, orphanRemoval = true)
    @JsonManagedReference
    private List<QuotationContainer> quotationContainerList = new ArrayList<>();

    @OneToMany(mappedBy = "quotationDetail", cascade = CascadeType.ALL, orphanRemoval = true)
    @JsonManagedReference
    private List<QuotationDetailGeneralNote> serviceNoteList = new ArrayList<>();

}
