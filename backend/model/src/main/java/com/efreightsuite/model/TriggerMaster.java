package com.efreightsuite.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.UniqueConstraint;
import javax.persistence.Version;

import com.efreightsuite.enumeration.LovStatus;
import com.efreightsuite.enumeration.YesNo;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;


@Entity
@Getter
@Setter
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Table(name = "efs_trigger_master",
        uniqueConstraints = {@UniqueConstraint(columnNames = {"trigger_code"}, name = "UK_TRIGGER_TRIGGERCODE"),
                @UniqueConstraint(columnNames = {"trigger_name"}, name = "UK_TRIGGER_TRIGGERNAME")}

)
public class TriggerMaster implements Serializable {

    private static final long serialVersionUID = 1L;

    /*Auto Generation Id*/
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    @Version
    @Column(name = "version_lock")
    long versionLock;

    /*Create/Last User and Date Info*/
    @Embedded
    SystemTrack systemTrack;

    /*Trigger Code*/
    @Column(name = "trigger_code", nullable = false, length = 10)
    String triggerCode;

    /*Trigger Name*/
    @Column(name = "trigger_name", nullable = false, length = 100)
    String triggerName;


    /*Trigger Type ID*/
    @ManyToOne
    @JoinColumn(name = "trigger_type_id", foreignKey = @ForeignKey(name = "FK_TRIGGER_TRIGGERTYPEID"))
    TriggerTypeMaster triggerTypeMaster;


    /*Mail Subject*/
    @Column(name = "mail_subject", length = 100)
    String mailSubject;

    /*Status*/
    @Column(name = "status", nullable = false, length = 10)
    @Enumerated(EnumType.STRING)
    LovStatus status;

    /*Use for Finance*/
    @Column(name = "use_for_finance", length = 10)
    @Enumerated(EnumType.STRING)
    YesNo useForFinance = YesNo.No;

    @Column(name = "air_days")
    Integer airDays;

    @Column(name = "sea_days")
    Integer seaDays;

    @Column(name = "land_days")
    Integer landDays;


    /*notes */
    @Column(name = "note", length = 4000)
    String note;


}
