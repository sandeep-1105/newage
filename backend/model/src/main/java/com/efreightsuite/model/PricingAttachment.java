package com.efreightsuite.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.UniqueConstraint;
import javax.persistence.Version;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;


@Entity
@Table(name = "efs_pricing_attachment", uniqueConstraints = {
        @UniqueConstraint(columnNames = {"ref_no", "pricing_master_id"}, name = "UK_PRICATTACH_REF")
})
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Setter
@Getter

public class PricingAttachment implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    @Version
    @Column(name = "version_lock")
    long versionLock;

    @Embedded
    SystemTrack systemTrack;

    @ManyToOne
    @JoinColumn(name = "company_id", nullable = false, foreignKey = @ForeignKey(name = "FK_PRICATTACH_COMPID"))
    CompanyMaster companyMaster;

    @Column(name = "company_code", nullable = false, length = 30)
    String companyCode;

    @ManyToOne
    @JoinColumn(name = "location_id", nullable = false, foreignKey = @ForeignKey(name = "FK_PRICATTACH_LOCTID"))
    LocationMaster locationMaster;

    @Column(name = "location_code", nullable = false, length = 30)
    String locationCode;

    @ManyToOne
    @JoinColumn(name = "country_id", nullable = false, foreignKey = @ForeignKey(name = "FK_PRICATTACH_COUNTID"))
    CountryMaster countryMaster;

    @Column(name = "country_code", nullable = false, length = 30)
    String countryCode;

    @ManyToOne
    @JoinColumn(name = "pricing_master_id", foreignKey = @ForeignKey(name = "FK_PRICATTACH_PRICINGID"))
    @JsonBackReference
    PricingMaster pricingMaster;

    @Column(name = "ref_no", length = 100)
    String refNo;

    @Column(name = "file_name", length = 100)
    String fileName;

    @Column(name = "file_content_type", length = 100)
    String fileContentType;

    @Column(name = "file_attached")
    @Lob
    byte[] file;

}
