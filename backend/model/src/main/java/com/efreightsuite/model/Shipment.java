package com.efreightsuite.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.*;

import com.efreightsuite.enumeration.YesNo;
import com.efreightsuite.util.JsonDateDeserializer;
import com.efreightsuite.util.JsonDateSerializer;
import com.efreightsuite.util.JsonYesNoDeserializer;
import com.efreightsuite.util.JsonYesNoSerializer;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Getter;
import lombok.Setter;


@Entity
@Getter
@Setter
@Table(name = "efs_shipment",
        uniqueConstraints = {@UniqueConstraint(columnNames = {"shipment_uid"}, name = "UK_SHIPMENT_SHIPMENTUID")},
        indexes = {
                @Index(columnList = "shipment_req_date", name = "IDX_SHIPMENT_SHIPMENTDATE"),
                @Index(columnList = "created_by", name = "IDX_SHIPMENT_CRTEBYID")}
)
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Shipment implements Serializable {
    private static final long serialVersionUID = 1L;

    /*Auto Generation Id*/
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    @Version
    @Column(name = "version_lock")
    long versionLock;

    /*Create/Last User and Data Info*/
    @Embedded
    SystemTrack systemTrack;

    /*Shipment UID*/
    @Column(name = "shipment_uid", nullable = false)
    String shipmentUid;

    /*Shipment Request Date*/
    @Column(name = "shipment_req_date", nullable = false)
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date shipmentReqDate;

    /*Created By*/
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "created_by", foreignKey = @ForeignKey(name = "FK_SHIPMENT_CREATEDID"), nullable = false)
    EmployeeMaster createdBy;

    /*Salesman Code*/
    @Column(name = "created_by_code", nullable = false)
    String createdByCode;


    /*Company Id*/
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "company_id", foreignKey = @ForeignKey(name = "FK_SHIPMENT_COMPANYID"))
    CompanyMaster companyMaster;

    /* Company Code */
    @Column(name = "company_code")
    String companyCode;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "location_id", foreignKey = @ForeignKey(name = "FK_SHIPMENT_LOCATIONID"))
    LocationMaster location;

    @Column(name = "location_code")
    String locationCode;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "country_id", foreignKey = @ForeignKey(name = "FK_SHIPMENT_COUNTRYID"))
    CountryMaster country;

    /* Country Code */
    @Column(name = "country_code")
    String countryCode;

    /*Customer Manifest Name*/
    @Column(name = "customer_manifest_name")
    String customerManifestName;


    @OneToOne
    @JoinColumn(name = "shipment_detail", foreignKey = @ForeignKey(name = "FK_SHIPMENT_SHIPMENTDETAIL"))
    ShipmentDetail shipmentDetail;

    @ManyToOne(fetch = FetchType.LAZY)
    //@Transient
    @JoinColumn(name = "last_updated_status", foreignKey = @ForeignKey(name = "FK_SHIPMENT_LASTUPTSTATUS"))
    ShipmentStatus lastUpdatedStatus;

    @Column(name = "is_from_consol", length = 5)
    @Enumerated(EnumType.STRING)
    @JsonSerialize(using = JsonYesNoSerializer.class)
    @JsonDeserialize(using = JsonYesNoDeserializer.class)
    YesNo isFromConsolForImport = YesNo.No;

    @OneToMany(mappedBy = "shipment", cascade = CascadeType.ALL, orphanRemoval = true)
    @JsonManagedReference
    private List<ShipmentStatus> shipmentStatusList = new ArrayList<>();

    @OneToMany(mappedBy = "shipment", cascade = CascadeType.ALL, orphanRemoval = true)
    @JsonManagedReference
    private List<ShipmentServiceDetail> shipmentServiceList = new ArrayList<>();

    public Shipment() {
    }

    public Shipment(Long id, String shipmentUid) {
        this.id = id;
        this.shipmentUid = shipmentUid;
    }

}
