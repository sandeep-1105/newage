package com.efreightsuite.model;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Version;

import com.efreightsuite.enumeration.IdType;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;


@Entity
@Getter
@Setter
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Table(name = "efs_aes_usppi")
public class AesUsppi implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    @Version
    @Column(name = "version_lock")
    long versionLock;

    /* Create/Last User and Data Info */
    @Embedded
    SystemTrack systemTrack;

    /* Customer id */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "shipper_id", foreignKey = @ForeignKey(name = "FK_USPPI_SHIPPER_ID"), nullable = false)
    PartyMaster shipper;

    @Column(name = "shipper_code", length = 100)
    String shipperCode;

    /* Id Type */
    @Column(name = "id_type")
    @Enumerated(EnumType.STRING)
    IdType idType;

    /* idNo */
    @Column(name = "id_no", length = 11)
    String idNo;

    /* EIN No */
    @Column(name = "ein_no", length = 30)
    String einNo;

    /* firstName */
    @Column(name = "first_name", length = 100)
    String firstName;

    /* lastName */
    @Column(name = "last_name", length = 100)
    String lastName;

    /* Contact No */
    @Column(name = "contact_no", length = 20)
    String contactNo;

    /* Cargo Origin */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "cargo_origin_id", foreignKey = @ForeignKey(name = "FK_USPPI_CARGO_ORIGIN_ID"))
    PartyMaster cargoOrigin;

    @Column(name = "cargo_origin_code", length = 100)
    String cargoOriginCode;

    /* Cargo Origin Address */
    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "cargo_origin_address_id", foreignKey = @ForeignKey(name = "FK_USPPI_CARGO_ORIGIN_ADDR_ID"), nullable = false)
    AddressMaster cargoOriginAddress;

    /* City Id */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "city_id", foreignKey = @ForeignKey(name = "FK_USPPI_CITY_ID"))
    CityMaster cityMaster;

    @Column(name = "city_code", length = 100)
    String cityCode;

    /* State or Province */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "state_province", foreignKey = @ForeignKey(name = "FK_USPPI_STATE_ID"))
    StateMaster stateMaster;

    @Column(name = "state_code", length = 100)
    String stateCode;

    /* Country Id */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "country_id", foreignKey = @ForeignKey(name = "FK_USPPI_COUNTRY_ID"))
    CountryMaster countryMaster;

    @Column(name = "country_code", length = 100)
    String countryCode;

    /* Zip Code */
    @Column(name = "zip_code", length = 10)
    String zipCode;


}
