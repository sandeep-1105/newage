package com.efreightsuite.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.UniqueConstraint;
import javax.persistence.Version;

import com.efreightsuite.enumeration.LovStatus;
import com.efreightsuite.enumeration.YesNo;
import com.efreightsuite.util.JsonYesNoDeserializer;
import com.efreightsuite.util.JsonYesNoSerializer;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Getter;
import lombok.Setter;


@Entity
@Getter
@Setter
@Table(name = "efs_general_ledger_account", uniqueConstraints = {
        @UniqueConstraint(columnNames = {"gl_account_code"}, name = "UK_GLEDGERACC_ACCCODE"),
        @UniqueConstraint(columnNames = {"gl_account_name"}, name = "UK_GLEDGERACC_ACCNAME")})
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class GeneralLedgerAccount implements Serializable {
    private static final long serialVersionUID = 1L;

    /* Auto Generation Id */
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    @Version
    @Column(name = "version_lock")
    long versionLock;

    /* Create/Last User and Data Info */
    @Embedded
    SystemTrack systemTrack;

    @ManyToOne
    @JoinColumn(name = "general_ledger_id", foreignKey = @ForeignKey(name = "FK_GLEDGERACC_GENDLDG"))
    @JsonBackReference
    GeneralLedger generalLedger;

    @ManyToOne
    @JoinColumn(name = "gl_master_id", foreignKey = @ForeignKey(name = "FK_GLEDGERACC_GLMASTERID"))
    GeneralLedgerMaster glMaster;

    @Column(name = "gl_master_code", nullable = false, length = 30)
    String glMasterCode;

    @ManyToOne
    @JoinColumn(name = "company_id", nullable = false, foreignKey = @ForeignKey(name = "FK_GLEDGERACC_COMPANY"))
    CompanyMaster companyMaster;

    @Column(name = "company_code", nullable = false, length = 30)
    String companyCode;


    @Column(name = "gl_account_code", nullable = false, length = 30)
    String glAccountCode;

    @Column(name = "gl_account_name", nullable = false, length = 100)
    String glAccountName;

    @Column(name = "is_sub_ledger", length = 5)
    @Enumerated(EnumType.STRING)
    @JsonSerialize(using = JsonYesNoSerializer.class)
    @JsonDeserialize(using = JsonYesNoDeserializer.class)
    YesNo isSubLedger = YesNo.No;

    @Column(name = "gl_sub_ledger_code", length = 30)
    String glSubLedgerCode;

    @Column(name = "is_job_account", length = 5)
    @Enumerated(EnumType.STRING)
    @JsonSerialize(using = JsonYesNoSerializer.class)
    @JsonDeserialize(using = JsonYesNoDeserializer.class)
    YesNo isJobAccount = YesNo.No;

    @Column(name = "is_bank_account", length = 5)
    @Enumerated(EnumType.STRING)
    @JsonSerialize(using = JsonYesNoSerializer.class)
    @JsonDeserialize(using = JsonYesNoDeserializer.class)
    YesNo isBankAccount = YesNo.No;

    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "bank_account_id", foreignKey = @ForeignKey(name = "FK_GLEDGERACC_BANKACCID"))
    BankAccount bankAccount;

    /* Status */
    @Column(name = "status", nullable = false, length = 10)
    @Enumerated(EnumType.STRING)
    LovStatus status;

    @OneToMany(mappedBy = "generalLedgerAccount", cascade = CascadeType.ALL, orphanRemoval = true)
    @JsonManagedReference
    private List<GeneralSubLedger> subLedgerAccountList = new ArrayList<>();

}
