package com.efreightsuite.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;
import javax.persistence.Version;

import com.efreightsuite.enumeration.MailEventType;
import com.efreightsuite.enumeration.MessageSendType;
import com.efreightsuite.enumeration.Status;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

/**
 * @author Devendrachary M
 */

@Entity
@Getter
@Setter
@Table(name = "efs_auto_mail_master", uniqueConstraints = {
        @UniqueConstraint(columnNames = {"message_code"}, name = "UK_AUTOMAIL_MESSAGECODE"),
        @UniqueConstraint(columnNames = {"message_name"}, name = "UK_AUTOMAIL_MESSAGENAME")})
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class AutoMailMaster implements Serializable {
    private static final long serialVersionUID = 1L;

    /* Auto Generation Id */
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    @Version
    @Column(name = "version_lock")
    long versionLock;

    /* Create/Last User and Data Info */
    @Embedded
    SystemTrack systemTrack;

    /* Message Name */
    @Column(name = "message_name", nullable = false, length = 100)
    String messageName;

    /* Message Code */
    @Column(name = "message_code", nullable = false, length = 10)
    String messageCode;

    /*Message Send Type () */
    @Column(name = "message_send_type")
    @Enumerated(EnumType.STRING)
    MessageSendType messageSendType;

    /* Status (enable/disable)  */
    @Column(name = "status")
    @Enumerated(EnumType.STRING)
    Status autoMailStatus;

    /* Message Group Id */
    @ManyToOne
    @JoinColumn(name = "message_group_id", foreignKey = @ForeignKey(name = "FK_AUTOMAIL_AUTOMAILGROUPID"))
    @JsonBackReference
    AutoMailGroupMaster autoMailGroupMaster;

    @Transient
    AutoMailGroupMaster transAutoMailGroupMaster;

    /* Event  */
    @Column(name = "event_type")
    @Enumerated(EnumType.STRING)
    MailEventType eventType;


}
