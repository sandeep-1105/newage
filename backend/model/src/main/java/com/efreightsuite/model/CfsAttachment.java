package com.efreightsuite.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.UniqueConstraint;
import javax.persistence.Version;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;


@Entity
@Getter
@Setter
@Table(name = "efs_cfs_attachment", uniqueConstraints = {
        @UniqueConstraint(columnNames = {"ref_no", "cfs_receive_id"}, name = "UK_CFSATTACH_REF")})
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class CfsAttachment implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    @Version
    @Column(name = "version_lock")
    long versionLock;

    @Embedded
    SystemTrack systemTrack;

    @Column(name = "ref_no", length = 100)
    String refNo;

    @Column(name = "file_name", length = 100)
    String fileName;

    @Column(name = "file_content_type", length = 100)
    String fileContentType;

    @Column(name = "file_attached")
    @Lob
    byte[] file;

    /* CFS Receive Entry */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "cfs_receive_id", foreignKey = @ForeignKey(name = "FK_CFSATTACH_CFSID"))
    @JsonBackReference
    CfsReceiveEntry cfsReceiveEntry;

}
