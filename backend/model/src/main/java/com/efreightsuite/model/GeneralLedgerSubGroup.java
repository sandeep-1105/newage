package com.efreightsuite.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.UniqueConstraint;
import javax.persistence.Version;

import com.efreightsuite.enumeration.GlHead;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;


@Entity
@Getter
@Setter
@Table(name = "efs_general_ledger_sub_group", uniqueConstraints = {
        @UniqueConstraint(columnNames = {"gl_sub_group_code"}, name = "UK_GLSUBGRP_CODE"),
        @UniqueConstraint(columnNames = {"gl_sub_group_name"}, name = "UK_GLSUBGRP_NAME")})
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class GeneralLedgerSubGroup implements Serializable {

    private static final long serialVersionUID = 1L;

    /* Auto Generation Id */
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    @Version
    @Column(name = "version_lock")
    long versionLock;

    /* Create/Last User and Data Info */
    @Embedded
    SystemTrack systemTrack;

    /* gl_head */
    @Column(name = "gl_head", nullable = false, length = 30)
    @Enumerated(EnumType.STRING)
    GlHead glHead;

    @ManyToOne
    @JoinColumn(name = "company_id", nullable = false, foreignKey = @ForeignKey(name = "FK_GLSUBGRP_COMPANY"))
    CompanyMaster companyMaster;

    @Column(name = "company_code", nullable = false, length = 30)
    String companyCode;

    @ManyToOne
    @JoinColumn(name = "gl_group_id", nullable = false, foreignKey = @ForeignKey(name = "FK_GLSUBGRP_GROUP"))
    GeneralLedgerGroup glGroup;

    @Column(name = "gl_group_code", nullable = false, length = 30)
    String glGroupCode;

    /* Group Code */
    @Column(name = "gl_sub_group_code", nullable = false, length = 30)
    String glSubGroupCode;

    /* Group Name */
    @Column(name = "gl_sub_group_name", nullable = false, length = 100)
    String glSubGroupName;

}
