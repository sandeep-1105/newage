package com.efreightsuite.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.UniqueConstraint;
import javax.persistence.Version;

import com.efreightsuite.enumeration.LovStatus;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;


@Entity
@Getter
@Setter
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Table(name = "efs_zone_master",
        uniqueConstraints = {
                @UniqueConstraint(columnNames = {"zone_code"}, name = "UK_ZONE_ZONECODE"),
                @UniqueConstraint(columnNames = {"zone_name"}, name = "UK_ZONE_ZONENAME")
        }
)

public class ZoneMaster implements Serializable {
    private static final long serialVersionUID = 1L;

    /*Auto Generation Id*/
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    @Version
    @Column(name = "version_lock")
    long versionLock;

    /*Create/Last User and Data Info*/
    @Embedded
    @JsonIgnore
    SystemTrack systemTrack;

    /*Country id*/
    @ManyToOne
    @JoinColumn(name = "country_id", foreignKey = @ForeignKey(name = "FK_ZONE_COUNTRYID"))
    CountryMaster countryMaster;

    /*Country Code*/
    @Column(name = "country_code", length = 10)
    String countryCode;

    /*Zone Code*/
    @Column(name = "zone_code", nullable = false, length = 10)
    String zoneCode;

    /*Zone Name*/
    @Column(name = "zone_name", nullable = false, length = 100)
    String zoneName;

    /* Lov Status */
    @Column(name = "status", nullable = false, length = 10)
    @Enumerated(EnumType.STRING)
    LovStatus status;
}
