package com.efreightsuite.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.UniqueConstraint;
import javax.persistence.Version;

import com.efreightsuite.util.JsonDateDeserializer;
import com.efreightsuite.util.JsonDateSerializer;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Getter;
import lombok.Setter;


@Entity
@Getter
@Setter
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Table(name = "efs_currency_rate_master", uniqueConstraints = {
        @UniqueConstraint(columnNames = {"from_currency_code", "to_currency_code", "currency_date"}, name = "UK_CURRENCY_FROMTODATE")})
public class CurrencyRateMaster implements Serializable {
    private static final long serialVersionUID = 1L;

    /*Auto Generation Id*/
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    @Version
    @Column(name = "version_lock")
    long versionLock;

    /*Create/Last User and Data Info*/
    @Embedded
    SystemTrack systemTrack;

    /*Country id*/
    @ManyToOne
    @JoinColumn(name = "country_id", foreignKey = @ForeignKey(name = "FK_CURRENCYRATE_COUNTRYID"))
    CountryMaster countryMaster;

    /*From Currency id*/
    @ManyToOne
    @JoinColumn(name = "from_currency_id", foreignKey = @ForeignKey(name = "FK_CURRENCYRATE_FROMCURRENCYID"))
    CurrencyMaster fromCurrency;

    /*To Currency id*/
    @ManyToOne
    @JoinColumn(name = "to_currency_id", foreignKey = @ForeignKey(name = "FK_CURRENCYRATE_TOCURRENCYID"))
    CurrencyMaster toCurrency;


    /*Country Code*/
    @Column(name = "country_code", length = 10)
    String countryCode;


    /*fromCurrencyCode Code*/
    @Column(name = "from_currency_code", length = 10)
    String fromCurrencyCode;


    /*To Currency Code*/
    @Column(name = "to_currency_code", length = 10)
    String toCurrencyCode;


    /*Currency Date */
    @Column(name = "currency_date")
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date currencyDate;


    /*Buy Rate*/
    @Column(name = "buy_rate", nullable = false)
    Double buyRate;

    /*Sell Rate*/
    @Column(name = "sell_rate", nullable = false)
    Double sellRate;


}
