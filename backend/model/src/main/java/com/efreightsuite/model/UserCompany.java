package com.efreightsuite.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.UniqueConstraint;
import javax.persistence.Version;

import com.efreightsuite.dto.UserCompanyDTO;
import com.efreightsuite.dto.UserCompanyLocationDTO;
import com.efreightsuite.enumeration.YesNo;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Getter;
import lombok.Setter;


@Entity
@Getter
@Setter
@Table(name = "efs_user_company",
        uniqueConstraints = {@UniqueConstraint(columnNames = {"company_id", "user_id"}, name = "UK_USERCOM_USERCOMID")}

)
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class UserCompany implements Serializable {
    private static final long serialVersionUID = 1L;

    /*Auto Generation Id*/
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    @Version
    @Column(name = "version_lock")
    long versionLock;

    /*Create/Last User and Data Info*/
    @Embedded
    SystemTrack systemTrack;

    /*User id*/
    @ManyToOne
    @JoinColumn(name = "user_id", foreignKey = @ForeignKey(name = "FK_USERCOM_USERID"))
    @JsonBackReference
    UserProfile userProfile;

    /* Employee Id */
    @ManyToOne
    @JoinColumn(name = "employee_id", foreignKey = @ForeignKey(name = "FK_USERCOM_EMPLOYEEID"))
    EmployeeMaster employee;


    /*Company id*/
    @ManyToOne
    @JoinColumn(name = "company_id", foreignKey = @ForeignKey(name = "FK_USERCOM_LOCATIONID"))
    CompanyMaster companyMaster;

    /* Default Company */
    @Column(name = "default_company", nullable = false, length = 20)
    @Enumerated(EnumType.STRING)
    YesNo yesNo;

    @OneToMany(mappedBy = "userCompany", cascade = CascadeType.ALL, orphanRemoval = true)
    @JsonManagedReference
    List<UserCompanyLocation> userCompanyLocationList = new ArrayList<>();


    public UserCompany(UserCompanyDTO dto) {
        this.id = dto.getId();
        this.versionLock = dto.getVersionLock();
        this.systemTrack = dto.getSystemTrack();
        if (dto.getUserProfile() != null)
            this.userProfile = new UserProfile(dto.getUserProfile().getId());
        this.employee = dto.getEmployee();
        this.companyMaster = dto.getCompanyMaster();
        this.yesNo = dto.getYesNo();
        if (dto.getUserCompanyLocationList() != null) {
            this.setUserCompanyLocationList(new ArrayList());
            for (UserCompanyLocationDTO loc : dto.getUserCompanyLocationList()) {
                UserCompanyLocation userCompanyLocation = new UserCompanyLocation(loc);
                userCompanyLocation.setUserCompany(this);
                this.getUserCompanyLocationList().add(userCompanyLocation);
            }
        }


    }

    public UserCompany() {
    }


}
