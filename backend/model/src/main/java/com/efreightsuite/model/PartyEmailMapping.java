package com.efreightsuite.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.UniqueConstraint;
import javax.persistence.Version;

import com.efreightsuite.enumeration.YesNo;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Getter;
import lombok.Setter;

/**
 * @author Devendrachary M
 */

@Entity
@Getter
@Setter
@Table(name = "efs_party_email_mapping", uniqueConstraints = {
        @UniqueConstraint(columnNames = {"party_id", "email", "location_id", "destination_country_id", "origin_country_id", "imco"}, name = "UK_PARTYEMAIL_PARTY")
})
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class PartyEmailMapping implements Serializable {
    private static final long serialVersionUID = 1L;

    /* Auto Generation Id */
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    @Version
    @Column(name = "version_lock")
    long versionLock;

    /* Create/Last User and Data Info */
    @Embedded
    SystemTrack systemTrack;

    /* Party email */
    @Column(name = "email", nullable = false, length = 500)
    String email;

    /*Party id*/
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "party_id", foreignKey = @ForeignKey(name = "FK_PARTYEMAIL_PARTYID"))
    @JsonBackReference
    PartyMaster partyMaster;

    /* company id */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "company_id", foreignKey = @ForeignKey(name = "FK_PARTYEMAIL_COMPANY_ID"))
    CompanyMaster companyMaster;

    @Column(name = "company_code", length = 100)
    String companyCode;

    /* country code */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "country_id", foreignKey = @ForeignKey(name = "FK_PARTYEMAIL_CNTID"))
    CountryMaster countryMaster;

    @Column(name = "country_code", length = 100)
    String countryCode;

    /* location id */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "location_id", foreignKey = @ForeignKey(name = "FK_PARTYEMAIL_LOCATIONID"))
    LocationMaster locationMaster;

    @Column(name = "location_code", length = 100)
    String locationCode;

    /* origin country */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "destination_country_id", foreignKey = @ForeignKey(name = "FK_PARTYEMAIL_DESTCNTID"))
    CountryMaster destinationCountry;

    @Column(name = "destination_country_code", length = 100)
    String destinationCountryCode;

    /* destination_country */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "origin_country_id", foreignKey = @ForeignKey(name = "FK_PARTYEMAIL_ORICNTID"))
    CountryMaster originCountry;

    @Column(name = "origin_country_code", length = 100)
    String originCountryCode;

    // List of party email message mapping for this party email
    @OneToMany(mappedBy = "partyEmailMapping", cascade = CascadeType.ALL, orphanRemoval = true)
    @JsonManagedReference
    List<PartyEmailMessageMapping> partyEmailMessageMappingList = new ArrayList<>();

    /* imco */
    @Column(name = "imco")
    @Enumerated(EnumType.STRING)
    YesNo imco;


    public PartyMaster getPartyMaster() {

        if (this.partyMaster == null)
            return null;

        PartyMaster tm = new PartyMaster();
        tm.setId(partyMaster.getId());
        tm.setPartyCode(partyMaster.getPartyCode());
        tm.setPartyName(partyMaster.getPartyName());
        return tm;
    }
}
