package com.efreightsuite.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

import com.efreightsuite.util.JsonDateDeserializer;
import com.efreightsuite.util.JsonDateSerializer;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Type;


@Entity
@Getter
@Setter
@Table(name = "efs_aes_file_edi_status")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class AesFileEdiStatus implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /**
     *
     */

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    /* Created Date */
    @Column(name = "create_date", nullable = false)
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date createDate;

    /* AirlineEdi */
    @ManyToOne
    @JoinColumn(name = "aes_file_id", foreignKey = @ForeignKey(name = "FK_AES_FILE_STATUS_EDI_ID"))
    AesFile aesFile;

    @Column(name = "messaging_status", length = 250, nullable = true)
    // MQ Message Sent, Mq Connection Failure, In Progress, Ftp Connection
            // Failure, Win Connection Failure, Completed
            String messagingStatus;


    @Column(name = "mq_message")
    @Lob
    @Type(type = "org.hibernate.type.MaterializedClobType")
    String mqMessage;

    String mqActionType;

    String mqActionStatus;

    @Column(name = "messaging_error", length = 4000, nullable = true)
    String messagingError;
}
