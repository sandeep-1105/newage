package com.efreightsuite.model.common;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.UniqueConstraint;

import com.efreightsuite.enumeration.LovStatus;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;


@Entity
@Getter
@Setter
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Table(name = "efs_common_time_zone_master", uniqueConstraints = {
        @UniqueConstraint(columnNames = {"display_timezone"}, name = "UK_COM_TIMEZONE_DISPLAY"),
        @UniqueConstraint(columnNames = {"actual_timezone"}, name = "UK_COM_TIMEZONE_ACTUAL")}

)
public class CommonTimeZoneMaster implements Serializable {
    private static final long serialVersionUID = 1L;

    /* Auto Generation Id */
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    @Column(name = "display_timezone", nullable = false, length = 100)
    String displayTimeZone;

    @Column(name = "actual_timezone", nullable = false, length = 100)
    String actualTimeZone;

    /*Lov Status*/
    @Column(name = "status", nullable = false, length = 10)
    @Enumerated(EnumType.STRING)
    LovStatus status;

    public CommonTimeZoneMaster() {
    }

    public CommonTimeZoneMaster(String displayTimeZone, String actualTimeZone, LovStatus status) {
        this.displayTimeZone = displayTimeZone;
        this.actualTimeZone = actualTimeZone;
        this.status = status;
    }
}
