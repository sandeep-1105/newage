package com.efreightsuite.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.UniqueConstraint;
import javax.persistence.Version;

import com.efreightsuite.enumeration.LovStatus;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;


@Entity
@Getter
@Setter
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Table(name = "efs_state_province_master",
        uniqueConstraints = {
                @UniqueConstraint(columnNames = {"state_code", "country_id"}, name = "UK_STATEPROVINCE_STATECODE"),
                @UniqueConstraint(columnNames = {"state_name", "country_id"}, name = "UK_STATEPROVINCE_STATENAME")
        }
)
public class StateMaster implements Serializable {


    private static final long serialVersionUID = 1L;

    /*Auto Generation Id*/
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    @Version
    @Column(name = "version_lock")
    long versionLock;

    /*Create/Last User and Data Info*/
    @Embedded
    SystemTrack systemTrack;

    /*State Code*/
    @Column(name = "state_code", nullable = false, length = 10)
    String stateCode;

    /*State Name*/
    @Column(name = "state_name", nullable = false, length = 100)
    String stateName;

    /*Lov Status*/
    @Column(name = "status", nullable = false, length = 10)
    @Enumerated(EnumType.STRING)
    LovStatus status;

    /*Country Id*/
    @ManyToOne
    @JoinColumn(name = "country_id", foreignKey = @ForeignKey(name = "FK_STATEPROVINCE_COUNTRYID"), nullable = false)
    CountryMaster countryMaster;

    /*Country Code*/
    @Column(name = "country_code", length = 10, nullable = false)
    String countryCode;

    public StateMaster() {
    }

    public StateMaster(Long id) {
        this.id = id;
    }

    public StateMaster(Long id, String stateName) {
        this.id = id;
        this.stateName = stateName;
    }

    public StateMaster(String stateCode, String stateName, LovStatus status, CountryMaster countryMaster, String countryCode, SystemTrack systemTrack) {
        this.systemTrack = systemTrack;
        this.stateCode = stateCode;
        this.stateName = stateName;
        this.status = status;
        this.countryMaster = countryMaster;
        this.countryCode = countryCode;
    }
}
