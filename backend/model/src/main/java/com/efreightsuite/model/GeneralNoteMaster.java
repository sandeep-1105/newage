package com.efreightsuite.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Version;

import com.efreightsuite.enumeration.GeneralNoteCategory;
import com.efreightsuite.enumeration.GeneralNoteSubCategory;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;


@Entity
@Getter
@Setter
@Table(name = "efs_general_note_master")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class GeneralNoteMaster implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    @Version
    @Column(name = "version_lock")
    long versionLock;

    /* Create/Last User and Data Info */
    @Embedded
    @JsonIgnore
    SystemTrack systemTrack;

    /*category */
    @Column(name = "category", length = 50)
    @Enumerated(EnumType.STRING)
    GeneralNoteCategory category;

    /*category */
    @Column(name = "sub_category", length = 50)
    @Enumerated(EnumType.STRING)
    GeneralNoteSubCategory subCategory;

    @ManyToOne
    @JoinColumn(name = "location_id", foreignKey = @ForeignKey(name = "FK_GENNOTEMAS_LOCATIONID"))
    LocationMaster locationMaster;

    @Column(name = "location_code", length = 30)
    String locationCode;

    @ManyToOne
    @JoinColumn(name = "service_master_id", foreignKey = @ForeignKey(name = "FK_GENNOTEMAS_SERVICEID"))
    ServiceMaster serviceMaster;

    @Column(name = "service_code", length = 30)
    String serviceCode;

    /*notes */
    @Column(name = "note", length = 4000)
    String note;

}
