package com.efreightsuite.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.UniqueConstraint;
import javax.persistence.Version;

import com.efreightsuite.enumeration.LovStatus;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;


@Entity
@Getter
@Setter
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Table(name = "efs_comment_master", uniqueConstraints = {
        @UniqueConstraint(columnNames = {"comment_code"}, name = "UK_COMMENT_COMMENTCODE"),
        @UniqueConstraint(columnNames = {"comment_name"}, name = "UK_COMMENT_COMMENTNAME")})
public class CommentMaster implements Serializable {
    private static final long serialVersionUID = 1L;

    /* Auto Generation Id */
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    /* Version Lock */
    @Version
    @Column(name = "version_lock")
    long versionLock;

    /* Created / Last Updated User and Date Info */
    @Embedded
    SystemTrack systemTrack;

    /* Charter Code */
    @Column(name = "comment_code", nullable = false, length = 10)
    String commentCode;

    /* Charter Name */
    @Column(name = "comment_name", nullable = false, length = 100)
    String commentName;

    /* Display Status */
    @Column(name = "status", nullable = false, length = 10)
    @Enumerated(EnumType.STRING)
    LovStatus status;

    /* Description*/
    @Column(name = "description", nullable = false, length = 2000)
    String description;
}
