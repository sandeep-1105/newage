package com.efreightsuite.model;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.MappedSuperclass;
import javax.persistence.TableGenerator;
import javax.persistence.Version;

import com.efreightsuite.enumeration.YesNo;
import com.efreightsuite.util.JsonYesNoDeserializer;
import com.efreightsuite.util.JsonYesNoSerializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
@MappedSuperclass
public class SuperAttachment {

    /* Auto Generated ID */
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    public Long id;

    /* Version Lock */
    @Version
    @Column(name = "version_lock")
    public long versionLock;

    /* System Track Information */
    @Embedded
    public SystemTrack systemTrack;

    /* Reference Number */
    @Column(name = "ref_no", length = 100)
    public String refNo;

    /* Customs */
    @Column(name = "is_customs")
    @Enumerated(EnumType.STRING)
    @JsonSerialize(using = JsonYesNoSerializer.class)
    @JsonDeserialize(using = JsonYesNoDeserializer.class)
    public YesNo isCustoms = YesNo.No;

    /* Is Protected */
    @Column(name = "is_protected")
    @Enumerated(EnumType.STRING)
    @JsonSerialize(using = JsonYesNoSerializer.class)
    @JsonDeserialize(using = JsonYesNoDeserializer.class)
    public YesNo isProtected = YesNo.No;

    /* File Name */
    @Column(name = "file_name", length = 100)
    String fileName;

    /* File Content Type */
    @Column(name = "file_content_type", length = 100)
    String fileContentType;

    /* File */
    @Column(name = "file_content")
    @Lob
    byte[] file;

}
