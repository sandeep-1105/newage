package com.efreightsuite.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.UniqueConstraint;
import javax.persistence.Version;

import com.efreightsuite.enumeration.LovStatus;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;


@Entity
@Getter
@Setter
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Table(name = "efs_city_master",
        uniqueConstraints = {@UniqueConstraint(columnNames = {"city_code"}, name = "UK_CITY_CITYCODE"),
                @UniqueConstraint(columnNames = {"city_name", "state_id", "country_id"}, name = "UK_CITY_CITYNAME")}
)
public class CityMaster implements Serializable {
    private static final long serialVersionUID = 1L;

    /*Auto Generation Id*/
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    @Version
    @Column(name = "version_lock")
    long versionLock;

    /*Create/Last User and Data Info*/
    @Embedded
    SystemTrack systemTrack;

    /*Country Id*/
    @ManyToOne
    @JoinColumn(name = "country_id", foreignKey = @ForeignKey(name = "FK_CITY_COUNTRYID"))
    CountryMaster countryMaster;

    /*Country Code*/
    @Column(name = "country_code", length = 10)
    String countryCode;

    /*Country Id*/
    @ManyToOne
    @JoinColumn(name = "state_id", foreignKey = @ForeignKey(name = "FK_CITY_STATEID"))
    StateMaster stateMaster;

    /*Country Code*/
    @Column(name = "state_code", length = 10)
    String stateCode;

    /*City Code*/
    @Column(name = "city_code", nullable = false, length = 10)
    String cityCode;

    /*City Name*/
    @Column(name = "city_name", nullable = false, length = 100)
    String cityName;

    /* Lov Status */
    @Column(name = "status", nullable = false, length = 10)
    @Enumerated(EnumType.STRING)
    LovStatus status = LovStatus.Active;

    public CityMaster() {
    }

    public CityMaster(Long id) {

        this.id = id;
    }

    public CityMaster(Long id, String cityName) {
        this(id);
        this.cityName = cityName;
    }

    public CityMaster(String cityCode, String cityName, LovStatus status, SystemTrack systemTrack) {
        this.systemTrack = systemTrack;
        this.cityCode = cityCode;
        this.cityName = cityName;
        this.status = status;
    }

    public static CityMaster createProbe() {

        CityMaster cityMaster = new CityMaster();
        cityMaster.setStatus(null);
        return cityMaster;
    }
}
