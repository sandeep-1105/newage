package com.efreightsuite.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;
import javax.persistence.Version;

import com.efreightsuite.enumeration.LovStatus;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;


@Entity
@Getter
@Setter
@Table(name = "efs_logo_master",
        uniqueConstraints = {
                @UniqueConstraint(columnNames = {"location_id"}, name = "UK_LOGO_LOCATION")
        }
)
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class LogoMaster implements Serializable {
    private static final long serialVersionUID = 1L;

    /*Auto Generation Id*/
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    @Version
    @Column(name = "version_lock")
    long versionLock;

    /*Create/Last User and Data Info*/
    @Embedded
    SystemTrack systemTrack;

    /*Company id*/
    @ManyToOne
    @JoinColumn(name = "company_id", foreignKey = @ForeignKey(name = "FK_LOGO_COMPANYID"))
    CompanyMaster companyMaster;

    /*Company Code*/
    @Column(name = "company_code", length = 30)
    String companyCode;

    /*Country id*/
    @ManyToOne
    @JoinColumn(name = "country_id", foreignKey = @ForeignKey(name = "FK_LOGO_COUNTRYID"))
    CountryMaster countryMaster;

    /*Country Code*/
    @Column(name = "country_code", length = 30)
    String countryCode;


    /*Location id*/
    @ManyToOne
    @JoinColumn(name = "location_id", foreignKey = @ForeignKey(name = "FK_LOGO_LOCATIONID"))
    LocationMaster locationMaster;

    /*Location Code*/
    @Column(name = "location_code", length = 30)
    String locationCode;


    /*Lov Status*/
    @Column(name = "status", nullable = false, length = 10)
    @Enumerated(EnumType.STRING)
    LovStatus status;

    /*Logo*/
    @Column(name = "logo")
    @Lob
    byte[] logo;

    /* encoded logo for showing in ui */
    /* Note : this is not a column*/
    @Getter
    @Setter
    @Transient
    String encodedLogo;

}
