package com.efreightsuite.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.*;

import com.efreightsuite.enumeration.Approved;
import com.efreightsuite.enumeration.QuoteType;
import com.efreightsuite.enumeration.ReportName;
import com.efreightsuite.enumeration.WhoRouted;
import com.efreightsuite.enumeration.YesNo;
import com.efreightsuite.util.JsonDateDeserializer;
import com.efreightsuite.util.JsonDateSerializer;
import com.efreightsuite.util.JsonYesNoDeserializer;
import com.efreightsuite.util.JsonYesNoSerializer;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Type;

/**
 * @author Sathish
 */

@Entity
@Getter
@Setter
@Table(name = "efs_quotation", uniqueConstraints = {
        @UniqueConstraint(columnNames = {"quotation_no"}, name = "UK_QUOTATION_NO")})
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Quotation implements Serializable {

    private static final long serialVersionUID = 1L;
    /* isPrintCarrier */
    @Column(name = "is_print_carrier")
    @Enumerated(EnumType.STRING)
    @JsonSerialize(using = JsonYesNoSerializer.class)
    @JsonDeserialize(using = JsonYesNoDeserializer.class)
    public YesNo isPrintCarrier = YesNo.No;
    /* Auto Generation Id */
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;
    @Version
    @Column(name = "version_lock")
    long versionLock;
    /* Create/Last User and Data Info */
    @Embedded
    SystemTrack systemTrack;
    /* Quotation No */
    @Column(name = "quotation_no", length = 100, nullable = false)
    String quotationNo;
    @Column(name = "copy_quotation_no", length = 100)
    String copyQuotationNo;
    /* enquiry No */
    @Column(name = "enquiry_no", length = 100)
    String enquiryNo;
    /* Shipment No */
    @Column(name = "booking_no", length = 100)
    String bookingNo;
    /* Location */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "location_id", foreignKey = @ForeignKey(name = "FK_QUOTATION_LOCATIONID"))
    LocationMaster locationMaster;
    @Column(name = "location_code", length = 30)
    String locationCode;
    /* Company */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "company_id", foreignKey = @ForeignKey(name = "FK_QUOTATION_COMPANYID"))
    CompanyMaster companyMaster;
    @Column(name = "company_code", length = 30)
    String companyCode;
    /* Country */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "country_id", foreignKey = @ForeignKey(name = "FK_QUOTATION_COUNTRYID"))
    CountryMaster countryMaster;
    @Column(name = "country_code", length = 30)
    String countryCode;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "local_currency_id", foreignKey = @ForeignKey(name = "FK_QUOTATION_LOCCURID"))
    CurrencyMaster localCurrency;
    @Column(name = "local_currency_code", length = 100)
    String localCurrencyCode;
    /* Customer */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "customer_id", foreignKey = @ForeignKey(name = "FK_QUOTATION_CUSTOMERID"))
    PartyMaster customer;
    @Column(name = "customer_code", length = 100)
    String customerCode;
    /* Shipper */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "shipper_id", foreignKey = @ForeignKey(name = "FK_QUOTATION_SHIPPERID"))
    PartyMaster shipper;
    @Column(name = "shipper_code", length = 100)
    String shipperCode;
    /* valid_from Date */
    @Column(name = "valid_from")
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date validFrom;
    /* valid_to */
    @Column(name = "valid_to")
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date validTo;
    /* Sales Coordinator Id */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "sales_co_ordinator_id", foreignKey = @ForeignKey(name = "FK_QUOTATION_SALESCOORID"))
    EmployeeMaster salesCoordinator;
    @Column(name = "sales_co_ordinator_code", length = 100)
    String salesCoordinatorCode;
    /* Who Routed */
    @Column(name = "who_routed", nullable = false)
    @Enumerated(EnumType.STRING)
    WhoRouted whoRouted;
    /* Agent */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "agent_id", foreignKey = @ForeignKey(name = "FK_QUOTATION_AGENTID"))
    PartyMaster agent;
    @Column(name = "agent_code", length = 100)
    String agentCode;
    /* Salesman */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "salesman_id", foreignKey = @ForeignKey(name = "FK_QUOTATION_SALESMANID"))
    EmployeeMaster salesman;
    @Column(name = "salesman_code", length = 100)
    String salesmanCode;
    /* attention */
    @Column(name = "attention", length = 100)
    String attention;
    /* Logged On */
    @Column(name = "logged_on")
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date loggedOn;
    /* Logged By */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "logged_by", foreignKey = @ForeignKey(name = "FK_QUOTATION_LOGGEDBY"))
    EmployeeMaster loggedBy;
    @Column(name = "logged_by_code", length = 100)
    String loggedByCode;
    // Quotation Type
    @Column(name = "quote_type")
    @Enumerated(EnumType.STRING)
    QuoteType quoteType;
    // Approved Type
    @Column(name = "approved")
    @Enumerated(EnumType.STRING)
    Approved approved;
    /* Logged By */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "approved_by", foreignKey = @ForeignKey(name = "FK_QUOTATION_APPROVEDBY"))
    EmployeeMaster approvedBy;
    @Column(name = "approved_by_code", length = 100)
    String approvedByCode;
    // Approved Date
    @Column(name = "approved_date")
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date approvedDate;
    /* customerAddress */
    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "customer_address_id", foreignKey = @ForeignKey(name = "FK_QUOTATION_CUSTOMERADD"))
    AddressMaster customerAddress;
    /* shipperAddress */
    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "shipper_address_id", foreignKey = @ForeignKey(name = "FK_QUOTATION_SHIPPERADD"))
    AddressMaster shipperAddress;
    /* HeaderNote  */
    @Column(name = "header_note", nullable = false)
    @Lob
    @Type(type = "org.hibernate.type.MaterializedClobType")
    String headerNote;
    /* HeaderNote  */
    @Column(name = "footer_note", nullable = false)
    @Lob
    @Type(type = "org.hibernate.type.MaterializedClobType")
    String footerNote;
    // Approved Date
    @Column(name = "customer_approved_date")
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date customerApprovedDate;
    /* Source IP */
    @Column(name = "customer_approved_ip", length = 20)
    String customerApprovedIp;
    /* Reject Reason  */
    @Column(name = "reject_reason")
    @Lob
    @Type(type = "org.hibernate.type.MaterializedClobType")
    String rejectReason;
    @Transient
    ReportName reportName;
    /* EnquiryDetails */
    @OneToMany(mappedBy = "quotation", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<QuotationDetail> quotationDetailList = new ArrayList<>();
    /* general Note List */
    @OneToMany(mappedBy = "quotation", cascade = CascadeType.ALL, orphanRemoval = true)
    @JsonManagedReference
    @OrderBy("id ASC")
    private List<QuotationGeneralNote> generalNoteList = new ArrayList<>();
    @OneToMany(mappedBy = "quotation", cascade = CascadeType.ALL, orphanRemoval = true)
    @JsonManagedReference
    private List<QuotationAttachment> quotationAttachementList = new ArrayList<>();

    public Quotation() {
    }

    public Quotation(Long id, String quotationNo) {
        this.id = id;
        this.quotationNo = quotationNo;
    }

    private Quotation(Builder builder) {
        setSystemTrack(builder.systemTrack);
        setLocationMaster(builder.locationMaster);
        setCompanyMaster(builder.companyMaster);
        setCountryMaster(builder.countryMaster);
        setCustomer(builder.customer);
        setValidFrom(builder.validFrom);
        setValidTo(builder.validTo);
        setSalesCoordinator(builder.salesCoordinator);
        setWhoRouted(builder.whoRouted);
        setLoggedBy(builder.loggedBy);
        setApproved(builder.approved);
        setCustomerAddress(builder.customerAddress);
        setShipperAddress(builder.shipperAddress);
        setHeaderNote(builder.headerNote);
        setFooterNote(builder.footerNote);
        setQuotationDetailList(builder.quotationDetailList);
    }


    public boolean isNew() {
        return this.id == null;
    }

    public static final class Builder {
        private SystemTrack systemTrack;
        private LocationMaster locationMaster;
        private CompanyMaster companyMaster;
        private CountryMaster countryMaster;
        private PartyMaster customer;
        private Date validFrom;
        private Date validTo;
        private EmployeeMaster salesCoordinator;
        private WhoRouted whoRouted;
        private EmployeeMaster loggedBy;
        private Approved approved;
        private AddressMaster customerAddress;
        private AddressMaster shipperAddress;
        private String headerNote;
        private String footerNote;
        private List<QuotationDetail> quotationDetailList;

        public Builder() {
        }

        public Builder(WhoRouted whoRouted, String headerNote, String footerNote, Date validFrom, Date validTo) {
            this.whoRouted = whoRouted;
            this.headerNote = headerNote;
            this.footerNote = footerNote;
            this.validFrom = validFrom;
            this.validTo = validTo;
        }

        public Builder withSystemTrack(SystemTrack val) {
            systemTrack = val;
            return this;
        }

        public Builder withLocationMaster(LocationMaster val) {
            locationMaster = val;
            return this;
        }

        public Builder withCompanyMaster(CompanyMaster val) {
            companyMaster = val;
            return this;
        }

        public Builder withCountryMaster(CountryMaster val) {
            countryMaster = val;
            return this;
        }

        public Builder withCustomer(PartyMaster val) {
            customer = val;
            return this;
        }

        public Builder withValidFrom(Date val) {
            validFrom = val;
            return this;
        }

        public Builder withValidTo(Date val) {
            validTo = val;
            return this;
        }

        public Builder withSalesCoordinator(EmployeeMaster val) {
            salesCoordinator = val;
            return this;
        }

        public Builder withWhoRouted(WhoRouted val) {
            whoRouted = val;
            return this;
        }

        public Builder withLoggedBy(EmployeeMaster val) {
            loggedBy = val;
            return this;
        }

        public Builder withApproved(Approved val) {
            approved = val;
            return this;
        }

        public Builder withCustomerAddress(AddressMaster val) {
            customerAddress = val;
            return this;
        }

        public Builder withShipperAddress(AddressMaster val) {
            shipperAddress = val;
            return this;
        }

        public Builder withHeaderNote(String val) {
            headerNote = val;
            return this;
        }

        public Builder withFooterNote(String val) {
            footerNote = val;
            return this;
        }

        public Builder withQuotationDetailList(List<QuotationDetail> val) {
            quotationDetailList = val;
            return this;
        }

        public Quotation build() {
            return new Quotation(this);
        }
    }
}
