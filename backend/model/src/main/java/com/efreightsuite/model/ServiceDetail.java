package com.efreightsuite.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Version;

import com.efreightsuite.enumeration.BooleanType;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;


@Entity
@Getter
@Setter
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Table(name = "efs_service_detail")
public class ServiceDetail implements Serializable {
    private static final long serialVersionUID = 1L;

    /*Auto Generation Id*/
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    @Version
    @Column(name = "version_lock")
    long versionLock;

    /*Create/Last User and Data Info*/
    @Embedded
    @JsonIgnore
    SystemTrack systemTrack;

    /*Origin Clearance*/
    @Column(name = "origin_clearance", length = 5)
    @Enumerated(EnumType.STRING)
    BooleanType originClearance;

    /*Origin Trucking*/
    @Column(name = "origin_trucking", length = 5)
    @Enumerated(EnumType.STRING)
    BooleanType originTrucking;

    /*Destination Clearance*/
    @Column(name = "destination_clearance", length = 5)
    @Enumerated(EnumType.STRING)
    BooleanType destinationClearance;

    @Column(name = "destination_trucking", length = 5)
    @Enumerated(EnumType.STRING)
    BooleanType destinationTrucking;


}
