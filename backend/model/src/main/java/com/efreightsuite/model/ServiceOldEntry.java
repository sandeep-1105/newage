package com.efreightsuite.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Version;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Getter;
import lombok.Setter;


@Entity
@Table(name = "efs_service_old_entry")
@Getter
@Setter
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class ServiceOldEntry implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /**
     * serialVersionUID
     */

	
	/* Auto Generation Id */
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    @Version
    @Column(name = "version_lock")
    long versionLock;

    /* Create/Last User and Data Info */
    @Embedded
    SystemTrack systemTrack;

    /* Service UID */
    @Column(name = "service_uid", nullable = false)
    String serviceUid;

    /* Shipment UID */
    @Column(name = "shipment_uid", nullable = false)
    String shipmentUid;


    /* Auto Generated CFS Receipt Number */
    @Column(name = "cfs_receipt_no", length = 30, nullable = false)
    String cfsReceiptNumber;


    /* Booked Pieces */
    @Column(name = "booked_pieces")
    Long bookedPieces = 0L;

    /* Booked Gross Weight Unit KG */
    @Column(name = "booked_gross_wt_unit_kg")
    Double bookedGrossWeightUnitKg = 0.0;

    /* Booked Volume Weight Unit KG */
    @Column(name = "booked_volume_wt_unit_kg")
    Double bookedVolumeWeightUnitKg = 0.0;

    /* Booked Chargeable Unit */
    @Column(name = "booked_chargeable_unit")
    Double bookedChargeableUnit = 0.0;

    /* Booked Gross Weight Unit KG */
    @Column(name = "booked_gross_wt_unit_pound")
    Double bookedGrossWeightUnitPound = 0.0;

    /* Booked Volume Weight Unit KG */
    @Column(name = "booked_volume_wt_unit_pound")
    Double bookedVolumeWeightUnitPound = 0.0;


    /* ServiceEntryDimension  List */
    @OneToMany(mappedBy = "serviceEntry", cascade = CascadeType.ALL, orphanRemoval = true)
    @JsonManagedReference
    List<DocumentOldEntry> documentOldList = new ArrayList<>();


}
