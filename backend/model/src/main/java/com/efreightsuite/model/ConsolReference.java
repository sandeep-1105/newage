package com.efreightsuite.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Version;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;


@Entity
@Getter
@Setter
@Table(name = "efs_consol_reference")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class ConsolReference implements Serializable {

    private static final long serialVersionUID = 1L;

    /* Auto Generation Id */
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    @Version
    @Column(name = "version_lock")
    long versionLock;

    /* Create/Last User and Date Info */
    @Embedded
    SystemTrack systemTrack;

    /* Consol */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "consol_id", nullable = false, foreignKey = @ForeignKey(name = "FK_CONSOLREF_CONSOLID"))
    @JsonBackReference
    Consol consol;

    /* Reference Type Master ID*/
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "reference_type_id", nullable = false, foreignKey = @ForeignKey(name = "FK_CONSOLREF_REFTYPE_ID"))
    ReferenceTypeMaster referenceTypeMaster;

    @Column(name = "reference_type_code", length = 100)
    String referenceTypeCode;

    /*Reference Number*/
    @Column(name = "reference_number", nullable = false, length = 30)
    String referenceNumber;

    /*Notes*/
    @Column(name = "reference_notes", length = 4000)
    String referenceNotes;
}
