package com.efreightsuite.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Transient;
import javax.persistence.Version;

import com.efreightsuite.enumeration.CRN;
import com.efreightsuite.enumeration.DebitCredit;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Getter;
import lombok.Setter;


@Entity
@Getter
@Setter
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Table(name = "efs_invoice_crn_detail")
public class InvoiceCreditNoteDetail implements Serializable {
    private static final long serialVersionUID = 1L;

    /* Auto Generation Id */
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    @Version
    @Column(name = "version_lock")
    long versionLock;

    /* Create/Last User and Data Info */
    @Embedded
    SystemTrack systemTrack;

    /* Invoice id */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "invoice_credit_note_id", foreignKey = @ForeignKey(name = "FK_INV_CREDET_INVOICEID"), nullable = false)
    @JsonBackReference
    InvoiceCreditNote invoiceCreditNote;

    @Column(name = "sub_job_sequence")
    Integer subJobSequence;

    /* shipmentServiceDetail Id */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "shipment_service_detail_id", foreignKey = @ForeignKey(name = "FK_INV_CREDET_SERVICEDETAILID"))
    ShipmentServiceDetail shipmentServiceDetail;

    @Column(name = "service_uid")
    String serviceUid;

	/*@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "gl_charge_mapping_id", foreignKey = @ForeignKey(name = "FK_INV_CREDET_GLCHRMAPPID"))
	GeneralLedgerChargeMappingMaster glChargeMapping;
*/

    @Column(name = "gl_charge_mapping_id")
    Long glChargeMappingId;

    /* Charge Id */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "charge_id", foreignKey = @ForeignKey(name = "FK_INV_CREDET_CHARGEID"), nullable = false)
    ChargeMaster chargeMaster;

    /* Charge Code */
    @Column(name = "charge_code", nullable = false)
    String chargeCode;

    /* Charge Name */
    @Column(name = "charge_name", nullable = false)
    String chargeName;

    /* C/R/N */
    @Column(name = "crn", length = 10)
    @Enumerated(EnumType.STRING)
    CRN crn;

    /* Currency Id */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "currency_id", foreignKey = @ForeignKey(name = "FK_INV_CREDET_CURRENCYID"))
    CurrencyMaster currencyMaster;

    @Column(name = "currency_code")
    String currencyCode;

    @Column(name = "provisional_item_id")
    Long provisionalItemId;

    @Column(name = "shipment_rate_id")
    Long shipmentRateId;

    /* ROE (Rate Of Exchange) */
    @Column(name = "roe")
    Double roe;

    /* Unit */
    @Column(name = "unit")
    Double unit;

    /* AmountPerUnit */
    @Column(name = "amount_per_unit")
    Double amountPerUnit;

    /* Amount */
    @Column(name = "amount", nullable = false)
    Double amount;

    /* Local Amount */
    @Column(name = "local_amount")
    Double localAmount;


    /* Currency Amount */
    @Column(name = "currency_amount")
    Double currencyAmount;


    /* DebitCredit  */
    @Column(name = "debit_credit", nullable = false, length = 10)
    @Enumerated(EnumType.STRING)
    DebitCredit debitCredit;

    /* taxable */
    @Column(name = "is_taxable", length = 10)
    Boolean isTaxable;

    /* account Code */
    @Column(name = "account_code")
    String accountCode;


    /* subledger Code */
    @Column(name = "subledger_code")
    String subledgerCode;


    @OneToMany(mappedBy = "invoiceCreditNoteDetail", cascade = CascadeType.ALL, orphanRemoval = true)
    @JsonManagedReference
    List<InvoiceCreditNoteTax> invoiceCreditNoteTaxList = new ArrayList<>();

    /* invoiceNo */
    @Transient
    String invoiceNo;

}
