package com.efreightsuite.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.UniqueConstraint;
import javax.persistence.Version;

import com.efreightsuite.enumeration.LovStatus;
import com.efreightsuite.enumeration.SequenceFormat;
import com.efreightsuite.enumeration.YesNo;
import com.efreightsuite.util.JsonDateDeserializer;
import com.efreightsuite.util.JsonDateSerializer;
import com.efreightsuite.util.JsonYesNoDeserializer;
import com.efreightsuite.util.JsonYesNoSerializer;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Getter;
import lombok.Setter;


@Entity
@Getter
@Setter
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Table(name = "efs_daybook_master",
        uniqueConstraints = {@UniqueConstraint(columnNames = {"daybook_code"}, name = "UK_DAYBOOK_DAYBOOKCODE"),
                @UniqueConstraint(columnNames = {"daybook_name"}, name = "UK_DAYBOOK_DAYBOOKNAME")}

)
public class DaybookMaster implements Serializable {


    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /*Auto Generation Id*/
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    @Version
    @Column(name = "version_lock")
    long versionLock;

    /*Create/Last User and Data Info*/
    @Embedded
    SystemTrack systemTrack;

    @Column(name = "daybook_code", nullable = false, length = 10)
    String daybookCode;

    @Column(name = "daybook_name", nullable = false, length = 100)
    String daybookName;

    /*Lov Status*/
    @Column(name = "status", nullable = false, length = 10)
    @Enumerated(EnumType.STRING)
    LovStatus status;

    @ManyToOne
    @JoinColumn(name = "document_type_id", foreignKey = @ForeignKey(name = "FK_DAYBOOK_DOCUMENTTYPEID"))
    DocumentTypeMaster documentTypeMaster;

    @ManyToOne
    @JoinColumn(name = "location_id", foreignKey = @ForeignKey(name = "FK_DAYBOOK_LOCATIONID"))
    LocationMaster locationMaster;

    /*Block Date*/
    @Column(name = "block_date")
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date blockDate;

    //Bank Account
    @ManyToOne
    @JoinColumn(name = "bank_account", foreignKey = @ForeignKey(name = "FK_DAYBOOK_BANK_ACC"))
    GeneralLedgerAccount bankAccount;

    //PDC Account
    @ManyToOne
    @JoinColumn(name = "pdc_account", foreignKey = @ForeignKey(name = "FK_DAYBOOK_PDS_ACC"))
    GeneralLedgerAccount pdcAccount;

    //Cash Account
    @ManyToOne
    @JoinColumn(name = "cash_account", foreignKey = @ForeignKey(name = "FK_DAYBOOK_CASH_ACC"))
    GeneralLedgerAccount cashAccount;

    //Exchange Gain Account
    @ManyToOne
    @JoinColumn(name = "exchange_gain_account", foreignKey = @ForeignKey(name = "FK_DAYBOOK_EXC_GAIN_ACC"))
    GeneralLedgerAccount exchangeGainAccount;

    //Exchange Loss Account
    @ManyToOne
    @JoinColumn(name = "exchange_loss_account", foreignKey = @ForeignKey(name = "FK_DAYBOOK_EXC_LOSS_ACC"))
    GeneralLedgerAccount exchangeLossAccount;

    @Column(name = "cheque_no_from")
    String chequeNoFrom;

    @Column(name = "cheque_no_to")
    String chequeNoTo;

    @Column(name = "cheque_report_name")
    String chequeReportName;

    @Column(name = "numeric_daybook_id")
    Long numericDaybookId;

    @Column(name = "numeric_daybook_code")
    String numericDaybookCode;

    @Column(name = "current_sequence_value")
    long currentSequenceValue;

    @Column(name = "prefix", length = 5)
    String prefix;

    @Column(name = "format")
    @Enumerated(EnumType.STRING)
    SequenceFormat format;

    @Column(name = "is_year", length = 10)
    @Enumerated(EnumType.STRING)
    @JsonSerialize(using = JsonYesNoSerializer.class)
    @JsonDeserialize(using = JsonYesNoDeserializer.class)
    YesNo isYear;

    @Column(name = "is_month", length = 10)
    @Enumerated(EnumType.STRING)
    @JsonSerialize(using = JsonYesNoSerializer.class)
    @JsonDeserialize(using = JsonYesNoDeserializer.class)
    YesNo isMonth;

    @Column(name = "suffix", length = 5)
    String suffix;

    @Column(name = "separator", length = 1)
    String separator;

    public DaybookMaster() {

    }

    public DaybookMaster(DaybookMaster obj) {
        this.id = obj.getId();
        this.daybookCode = obj.getDaybookCode();
        this.daybookName = obj.getDaybookName();
        this.status = obj.getStatus();
    }
}
