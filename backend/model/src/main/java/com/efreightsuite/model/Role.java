package com.efreightsuite.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.UniqueConstraint;
import javax.persistence.Version;

import com.efreightsuite.dto.FeatureDTO;
import com.efreightsuite.dto.RoleDTO;
import com.efreightsuite.enumeration.LovStatus;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
@Table(name = "efs_role", uniqueConstraints = {
        @UniqueConstraint(columnNames = {"role_name"}, name = "UK_ROLE_ROLENAME")})
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Role implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    @Version
    @Column(name = "version_lock")
    long versionLock;

    /* Create/Last User and Data Info */
    @Embedded
    SystemTrack systemTrack;

    @Column(name = "role_name", nullable = false, length = 100)
    String roleName;


    /* description */
    @Column(name = "description", length = 4000)
    String description;

    /* Status */
    @Column(name = "status", nullable = false, length = 10)
    @Enumerated(EnumType.STRING)
    LovStatus status;


    @OneToMany(mappedBy = "role", cascade = CascadeType.ALL, orphanRemoval = true)
    @JsonManagedReference
    private List<RoleHasFeature> featureList = new ArrayList<>();


    public Role() {

    }

    public Role(Long id) {
        this.id = id;
    }

    public Role(RoleDTO dto) {
        this.id = dto.getId();
        this.systemTrack = dto.getSystemTrack();
        //this.versionLock=dto.getV
        this.roleName = dto.getRoleName();
        this.description = dto.getDescription();
        this.status = dto.getStatus();
        if (dto.getFeaturesList() != null) {
            this.setFeatureList(new ArrayList<>());
            for (FeatureDTO rhf : dto.getFeaturesList()) {
                this.getFeatureList().add(new RoleHasFeature(rhf));
            }
        }
    }


}
