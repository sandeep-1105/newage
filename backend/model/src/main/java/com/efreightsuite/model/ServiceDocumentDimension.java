package com.efreightsuite.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Version;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;


@Entity
@Getter
@Setter
@Table(name = "efs_service_doc_dimen")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class ServiceDocumentDimension implements Serializable {

    private static final long serialVersionUID = 1L;

    /* Auto Generation Id */
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    @Version
    @Column(name = "version_lock")
    long versionLock;

    /* Create/Last User and Data Info */
    @Embedded
    SystemTrack systemTrack;

    /* Shipment Service Detail */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "document_id", foreignKey = @ForeignKey(name = "FK_SERDOCDIM_DOCID"))
    @JsonBackReference
    DocumentDetail documentDetail;


    /* No. of Pieces */
    @Column(name = "no_of_pieces")
    int noOfPiece;


    /*length*/
    @Column(name = "length")
    int length;

    /*width*/
    @Column(name = "width")
    int width;

    /*height*/
    @Column(name = "height")
    int height;

    /*Volume Weight(kg) */
    @Column(name = "volume_weight")
    double volWeight;

    /* Gross Weight (kg)*/
    @Column(name = "gross_weight")
    double grossWeight;

    @Column(name = "gross_weight_in_kg")
    double grossWeightKg = 0.0;

    public ServiceDocumentDimension() {
    }


    public ServiceDocumentDimension(int noOfPiece, int length, int width, int height, double volWeight, double grossWeight, double grossWeightKg) {
        this.noOfPiece = noOfPiece;
        this.length = length;
        this.width = width;
        this.height = height;
        this.volWeight = volWeight;
        this.grossWeight = grossWeight;
        this.grossWeightKg = grossWeightKg;
    }
}
