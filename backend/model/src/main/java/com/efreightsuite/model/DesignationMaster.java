package com.efreightsuite.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.UniqueConstraint;
import javax.persistence.Version;

import com.efreightsuite.enumeration.LovStatus;
import com.efreightsuite.enumeration.YesNo;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Type;


@Entity
@Getter
@Setter
@Table(name = "efs_designation_master", uniqueConstraints = {
        @UniqueConstraint(columnNames = {"designation_code"}, name = "UK_DESIGNATION_DESIG_CODE"),
        @UniqueConstraint(columnNames = {"designation_name"}, name = "UK_DESIGNATION_DESIG_NAME")}

)
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class DesignationMaster implements Serializable {
    private static final long serialVersionUID = 1L;

    /* Auto Generation Id */
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    /* Version Lock */
    @Version
    @Column(name = "version_lock")
    long versionLock;

    /* Created / Last Updated User and Date Info */
    @Embedded
    SystemTrack systemTrack;

    /* designation Code */
    @Column(name = "designation_code", nullable = false, length = 100)
    String designationCode;

    /* designation Name */
    @Column(name = "designation_name", nullable = false, length = 100)
    String designationName;

    /*Profile */
    @Column(name = "profile")
    @Lob
    @Type(type = "org.hibernate.type.MaterializedClobType")
    String profile;

    /* Manager */
    @Column(name = "manager", length = 10)
    @Enumerated(EnumType.STRING)
    YesNo manager;

    /* Lov Status */
    @Column(name = "status", nullable = false, length = 10)
    @Enumerated(EnumType.STRING)
    LovStatus status;
}
