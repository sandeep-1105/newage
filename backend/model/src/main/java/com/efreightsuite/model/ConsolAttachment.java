package com.efreightsuite.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;


@Entity
@Getter
@Setter
@Table(name = "efs_consol_attachment", uniqueConstraints = {
        @UniqueConstraint(columnNames = {"ref_no", "consol_id"}, name = "UK_CONSLATTCH_REF")})
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class ConsolAttachment extends SuperAttachment implements Serializable {
    private static final long serialVersionUID = 1L;

    /* Document ID */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "document_id", foreignKey = @ForeignKey(name = "FK_CONSOL_ATT_DOCUMENTID"))
    DocumentMaster documentMaster;

    /* Master ( Consol ) id */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "consol_id", foreignKey = @ForeignKey(name = "FK_CONSOL_ATT_CONSOLID"))
    @JsonBackReference
    Consol consol;
}
