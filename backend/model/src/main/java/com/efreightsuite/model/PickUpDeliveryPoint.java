package com.efreightsuite.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Version;

import com.efreightsuite.enumeration.YesNo;
import com.efreightsuite.util.JsonDateDeserializer;
import com.efreightsuite.util.JsonDateSerializer;
import com.efreightsuite.util.JsonYesNoDeserializer;
import com.efreightsuite.util.JsonYesNoSerializer;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Getter;
import lombok.Setter;


@Entity
@Table(name = "efs_pickup_delivery_point")
@Getter
@Setter
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class PickUpDeliveryPoint implements Serializable {
    private static final long serialVersionUID = 1L;

    /* Auto Generation Id */
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    @Version
    @Column(name = "version_lock")
    long versionLock;

    /* Create/Last User and Data Info */
    @Embedded
    SystemTrack systemTrack;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "transporter_id", foreignKey = @ForeignKey(name = "FK_PICKDLI_TRANSPORTERID"))
    PartyMaster transporter;

    @Column(name = "transporter_code")
    String transporterCode;

    @Column(name = "transporter_manifest_name")
    String transportermManifestName;

    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "transporter_address_id", foreignKey = @ForeignKey(name = "FK_PICKDLI_TRANSPORTERADD"))
    AddressMaster transportAddress;

    @Column(name = "transporter_address")
    String transporterAddress;

    @Column(name = "is_our_pick_up")
    @Enumerated(EnumType.STRING)
    @JsonSerialize(using = JsonYesNoSerializer.class)
    @JsonDeserialize(using = JsonYesNoDeserializer.class)
    YesNo isOurPickUp;

    @Column(name = "pickup_client_ref_no", length = 255)
    String pickUpClientRefNo;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "pickup_point_id", foreignKey = @ForeignKey(name = "FK_PICKDLI_PICKPOINT"))
    PartyMaster pickupPoint;

    @Column(name = "pickup_point_code")
    String pickupPointCode;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "pickup_from_id", foreignKey = @ForeignKey(name = "FK_PICKDLI_PICKFROM"))
    PortMaster pickupFrom;

    @Column(name = "pickup_from_code")
    String pickupFromCode;

	/*@Column(name = "pickup_place",length=500)
    String pickUpPlace;*/


    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "pickup_place", foreignKey = @ForeignKey(name = "FK_PICKUP_PLACE"))
    AddressMaster pickUpPlace;

    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "delivery_place", foreignKey = @ForeignKey(name = "FK_PICKUP_DELIVERY_PLACE"))
    AddressMaster deliveryPlace;

    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "door_delivery_place", foreignKey = @ForeignKey(name = "FK_PICKUP_DOOR_DELIVERY_PLACE"))
    AddressMaster doorDeliveryPlace;


    @Column(name = "pickup_contact_person")
    String pickUpContactPerson;

    @Column(name = "pickup_mobile_no")
    String pickUpMobileNo;

    @Column(name = "pickup_phone_no")
    String pickUpPhoneNo;

    @Column(name = "pickup_email", length = 500)
    String pickUpEmail;

    @Column(name = "pickup_fax")
    String pickUpFax;

    @Column(name = "pickup_follow_up")
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date pickUpFollowUp;

    @Column(name = "pickup_planned")
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date pickUpPlanned;

    @Column(name = "pickup_actual")
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date pickUpActual;


    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "delivery_point_id", foreignKey = @ForeignKey(name = "FK_PICKDLI_DELVPOINT"))
    CFSMaster deliveryPoint;

    @Column(name = "delivery_point_code")
    String deliveryPointCode;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "delivery_from_id", foreignKey = @ForeignKey(name = "FK_PICKDLI_DELVFROM"))
    PortMaster deliveryFrom;

    @Column(name = "delivery_from_code")
    String deliveryFromCode;
	
/*	@Column(name = "delivery_place",length=500)
	String deliveryPlace;*/

    @Column(name = "delivery_contact_person")
    String deliveryContactPerson;

    @Column(name = "delivery_mobile_no")
    String deliveryMobileNo;

    @Column(name = "delivery_phone_no")
    String deliveryPhoneNo;

    @Column(name = "delivery_email", length = 500)
    String deliveryEmail;

    @Column(name = "delivery_expected")
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date deliveryExpected;

    @Column(name = "delivery_actual")
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date deliveryActual;


    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "door_delivery_point_id", foreignKey = @ForeignKey(name = "FK_PICKDLI_DORDELVPOINT"))
    PortMaster doorDeliveryPoint;

    @Column(name = "door_delivery_point_code")
    String doorDeliveryPointCode;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "door_delivery_from_id", foreignKey = @ForeignKey(name = "FK_PICKDLI_DORDELVFROM"))
    PortMaster doorDeliveryFrom;

    @Column(name = "door_delivery_from_code")
    String doorDeliveryFromCode;
	
	/*@Column(name = "door_delivery_place",length=500)
	String doorDeliveryPlace;*/

    @Column(name = "door_delivery_contact_person")
    String doorDeliveryContactPerson;

    @Column(name = "door_delivery_mobile_no")
    String doorDeliveryMobileNo;

    @Column(name = "door_delivery_phone_no")
    String doorDeliveryPhoneNo;

    @Column(name = "door_delivery_email", length = 500)
    String doorDeliveryEmail;

    @Column(name = "door_delivery_expected")
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date doorDeliveryExpected;

    @Column(name = "door_delivery_actual")
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date doorDeliveryActual;

}
