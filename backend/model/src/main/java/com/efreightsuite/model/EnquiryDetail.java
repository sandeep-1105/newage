package com.efreightsuite.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;

import com.efreightsuite.dto.EnquiryLogDto;
import com.efreightsuite.enumeration.DimensionUnit;
import com.efreightsuite.enumeration.ImportExport;
import com.efreightsuite.enumeration.Service;
import com.efreightsuite.enumeration.YesNo;
import com.efreightsuite.util.JsonDimensionUnitDeserializer;
import com.efreightsuite.util.JsonDimensionUnitSerializer;
import com.efreightsuite.util.JsonYesNoDeserializer;
import com.efreightsuite.util.JsonYesNoSerializer;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Getter;
import lombok.Setter;


@Entity
@Getter
@Setter
@Table(name = "efs_enquiry_detail",
        uniqueConstraints = {
                @UniqueConstraint(columnNames = {
                        "enquiry_log_id",
                        "service_id",
                        "from_port_id",
                        "to_port_id"},
                        name = "UK_ENQDET_SERVICE")
        })
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class EnquiryDetail implements Serializable {
    private static final long serialVersionUID = 1L;

    /* Auto Generation Id */
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    @Version
    @Column(name = "version_lock")
    long versionLock;

    /* Create/Last User and Data Info */
    @Embedded
    SystemTrack systemTrack;

    /* Enquiry Log */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "enquiry_log_id", foreignKey = @ForeignKey(name = "FK_ENQDET_ENQUIRYLOGID"))
    @JsonBackReference
    EnquiryLog enquiryLog;

    /* Enquiry No */
    @Column(name = "enquiry_no", length = 100)
    String enquiryNo;

    /* Service */
    @Column(name = "service")
    @Enumerated(EnumType.STRING)
    Service service;

    /* Import or Export */
    @Column(name = "importExport")
    @Enumerated(EnumType.STRING)
    ImportExport importExport;

    /* Service */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "service_id", foreignKey = @ForeignKey(name = "FK_ENQDET_SERVICEID"))
    ServiceMaster serviceMaster;

    @Column(name = "service_code", length = 100)
    String serviceCode;


    /* Service */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "tos_id", foreignKey = @ForeignKey(name = "FK_ENQUIRYDETAIL_TOSID"))
    TosMaster tosMaster;

    @Column(name = "tos_code", length = 100)
    String tosCode;

    /* Service */
    @Column(name = "clearance")
    @Enumerated(EnumType.STRING)
    @JsonSerialize(using = JsonYesNoSerializer.class)
    @JsonDeserialize(using = JsonYesNoDeserializer.class)
    YesNo clearance;

    /* Source */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "from_port_id", foreignKey = @ForeignKey(name = "FK_ENQDET_FROMPORTID"))
    PortMaster fromPortMaster;

    @Column(name = "from_port_code", length = 100)
    String fromPortMasterCode;

    /* Destination */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "to_port_id", foreignKey = @ForeignKey(name = "FK_ENQUIRYDETAIL_TOPORTID"))
    PortMaster toPortMaster;

    @Column(name = "to_port_code", length = 100)
    String toPortMasterCode;

    /* Commodity */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "commodity_id", foreignKey = @ForeignKey(name = "FK_ENQDET_COMMODITYID"))
    CommodityMaster commodityMaster;

    @Column(name = "commodity_code", length = 100)
    String commodityMasterCode;

    /* Dimension Unit */
    @Column(name = "dimension_unit", nullable = false, length = 50)
    @JsonSerialize(using = JsonDimensionUnitSerializer.class)
    @JsonDeserialize(using = JsonDimensionUnitDeserializer.class)
    @Enumerated(EnumType.STRING)
    DimensionUnit dimensionUnit;

    /* Pick Up Address */
    @Column(name = "dimension_unit_value", length = 10)
    double dimensionUnitValue;

    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "pickup_address_id", foreignKey = @ForeignKey(name = "FK_ENQUIRYDETAIL_PICKUP"))
    AddressMaster pickupAddress;

    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "delivery_address_id", foreignKey = @ForeignKey(name = "FK_ENQUIRYDETAIL_DELIVERY"))
    AddressMaster deliveryAddress;

    /* hazardous */
    @Column(name = "hazardous")
    @Enumerated(EnumType.STRING)
    @JsonSerialize(using = JsonYesNoSerializer.class)
    @JsonDeserialize(using = JsonYesNoDeserializer.class)
    YesNo hazardous;

    /* Notes */
    @Column(name = "notes", length = 500)
    String notes;

    /* Vol Weight(kg) */
    @Column(name = "vol_weight")
    double volWeight;

    /* Gross Weight (kg) */
    @Column(name = "gross_weight")
    double grossWeight;

    /* Dimension Vol Weight(kg) */
    @Column(name = "dimension_vol_weight")
    double dimensionVolWeight;

    /* Dimension Gross Weight (kg) */
    @Column(name = "dimension_gross_weight")
    double dimensionGrossWeight;

    /* Dimension Gross Weight (kg) */
    @Column(name = "dimension_gross_weight_in_kg")
    double dimensionGrossWeightInKg = 0.0;

    /* Dimension Gross Weight (kg) */
    @Column(name = "dimension_total_pieces")
    double dimensionTotalPieces;

    @Column(name = "over_dimension", length = 5)
    @Enumerated(EnumType.STRING)
    @JsonSerialize(using = JsonYesNoSerializer.class)
    @JsonDeserialize(using = JsonYesNoDeserializer.class)
    YesNo overDimension;


    @Column(name = "process_instance_id")
    String processInstanceId;

    @Column(name = "task_id")
    String taskId;

    @Column(name = "workflow_completed", length = 5)
    @Enumerated(EnumType.STRING)
    @JsonSerialize(using = JsonYesNoSerializer.class)
    @JsonDeserialize(using = JsonYesNoDeserializer.class)
    YesNo workflowCompleted = YesNo.No;

    @Transient
    EnquiryLogDto tmpEnquiryLogDto;

    @OneToMany(mappedBy = "enquiryDetail", cascade = CascadeType.ALL, orphanRemoval = true)
    @JsonManagedReference
    private List<EnquiryDimension> enquiryDimensionList = new ArrayList<>();

    /* EnquiryContainer */
    @OneToMany(mappedBy = "enquiryDetail", cascade = CascadeType.ALL, orphanRemoval = true)
    @JsonManagedReference
    private List<EnquiryContainer> enquiryContainerList = new ArrayList<>();

    // EnquiryAttachment
    @OneToMany(mappedBy = "enquiryDetail", cascade = CascadeType.ALL, orphanRemoval = true)
    @JsonManagedReference
    private List<EnquiryAttachment> enquiryAttachmentList = new ArrayList<>();

    @OneToMany(mappedBy = "enquiryDetail", cascade = CascadeType.ALL, orphanRemoval = true)
    @JsonManagedReference
    private List<RateRequest> rateRequestList = new ArrayList<>();

    @OneToMany(mappedBy = "enquiryDetail", cascade = CascadeType.ALL, orphanRemoval = true)
    @JsonManagedReference
    private List<EnquiryValueAddedService> enquiryValueAddedServiceList = new ArrayList<>();

}
