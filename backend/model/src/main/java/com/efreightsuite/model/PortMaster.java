package com.efreightsuite.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.UniqueConstraint;
import javax.persistence.Version;

import com.efreightsuite.enumeration.LovStatus;
import com.efreightsuite.enumeration.TransportMode;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Getter;
import lombok.Setter;


@Entity
@Getter
@Setter
@Table(name = "efs_port_master", uniqueConstraints = {
        @UniqueConstraint(columnNames = {"port_code"}, name = "UK_PORT_PORTCODE"),
        @UniqueConstraint(columnNames = {"port_name", "transport_mode"}, name = "UK_PORT_PORTNAME")}

)
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class PortMaster implements Serializable {
    private static final long serialVersionUID = 1L;

    /* Auto Generation Id */
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    /* Version Lock */
    @Version
    @Column(name = "version_lock")
    long versionLock;

    /* Created / Last Updated User and Date Info */
    @Embedded
    SystemTrack systemTrack;

    /* Port Group id */
    @ManyToOne
    @JoinColumn(name = "port_group_id", foreignKey = @ForeignKey(name = "FK_PORT_PORTGROUPID"))
    PortGroupMaster portGroupMaster;

    /* Port Group Code */
    @Column(name = "port_group_code", nullable = false, length = 10)
    String portGroupCode;

    /* Transport Mode */
    @Column(name = "transport_mode", nullable = false, length = 10)
    @Enumerated(EnumType.STRING)
    TransportMode transportMode;

    /* Port Code */
    @Column(name = "port_code", nullable = false, length = 10)
    String portCode;

    /* Port Name */
    @Column(name = "port_name", nullable = false, length = 100)
    String portName;

    /* Country Code */
    @Column(name = "country_code", nullable = false, length = 10)
    String countryCode;

    /* Lov Status */
    @Column(name = "status", nullable = false, length = 10)
    @Enumerated(EnumType.STRING)
    LovStatus status;

    @JsonManagedReference
    @OneToMany(mappedBy = "portMaster", cascade = CascadeType.ALL, orphanRemoval = true)
    List<PortEdiMapping> ediList = new ArrayList<>();

    public PortMaster() {
    }

    public PortMaster(Long id, LovStatus status) {
        this.id = id;
        this.status = status;
    }

    public PortMaster(String portGroupCode, String portCode, String portName, String countryCode, LovStatus status, SystemTrack systemTrack, TransportMode transportMode) {
        this.systemTrack = systemTrack;
        this.portGroupCode = portGroupCode;
        this.transportMode = transportMode;
        this.portCode = portCode;
        this.portName = portName;
        this.countryCode = countryCode;
        this.status = status;
    }

    public void setEdiList(List<PortEdiMapping> ediList) {
        this.ediList.clear();
        if (ediList != null) {
            this.ediList.addAll(ediList);
        }
    }
}
