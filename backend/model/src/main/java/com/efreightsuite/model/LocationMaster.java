package com.efreightsuite.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;
import javax.persistence.Version;

import com.efreightsuite.enumeration.BooleanType;
import com.efreightsuite.enumeration.LovStatus;
import com.efreightsuite.enumeration.Measurement;
import com.efreightsuite.enumeration.SalutationType;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;


@Entity
@Getter
@Setter
@Table(name = "efs_location_master",
        uniqueConstraints = {
                @UniqueConstraint(columnNames = {"location_code"}, name = "UK_LOCATION_LOCATIONCODE"),
                @UniqueConstraint(columnNames = {"location_name"}, name = "UK_LOCATION_LOCATIONNAME")
        }
)
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class LocationMaster implements Serializable {
    private static final long serialVersionUID = 1L;


    /*Auto Generation Id*/
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    @Version
    @Column(name = "version_lock")
    long versionLock;

    /*Create/Last User and Data Info*/
    @Embedded
    SystemTrack systemTrack;

    /*Location Code*/
    @Column(name = "location_code", nullable = false, length = 10)
    String locationCode;

    /*Location Name*/
    @Column(name = "location_name", nullable = false, length = 100)
    String locationName;

    /*Branch Name*/
    @Column(name = "branch_name", length = 100)
    String branchName;

    /*Time Zone*/
    @Column(name = "time_zone", nullable = false)
    String timeZone;

    /*is our own office*/
    @Column(name = "is_owr_own_office", length = 5)
    @Enumerated(EnumType.STRING)
    BooleanType isOurOwnOffice;

    /*is owr own headoffice*/
    @Column(name = "is_head_office", length = 5)
    @Enumerated(EnumType.STRING)
    BooleanType isHeadOffice;

    /*Address Line 1*/
    @Column(name = "address_line_1", length = 255)
    String addressLine1;

    /*Address Line 2*/
    @Column(name = "address_line_2", length = 255)
    String addressLine2;

    /*Address Line 3*/
    @Column(name = "address_line_3", length = 255)
    String addressLine3;

    /*Address Line 4*/
    @Column(name = "address_line_4", length = 255)
    String addressLine4;


    /*Country id*/
    @ManyToOne
    @JoinColumn(name = "country_id", foreignKey = @ForeignKey(name = "FK_LOCATION_COUNTRYID"))
    CountryMaster countryMaster;

    /*Country Code*/
    @Column(name = "country_code", length = 10)
    String countryCode;

    /*Company Master id*/
    @ManyToOne
    @JoinColumn(name = "company_id", foreignKey = @ForeignKey(name = "FK_LOCATION_COMPANYID"))
    CompanyMaster companyMaster;

    /*Company Code*/
    @Column(name = "company_code", length = 10)
    String companyCode;

    /*country no*/
    @Column(name = "country_no")
    String countryNo;

    /*city no*/
    @Column(name = "city_no")
    String cityNo;

    /*Region Master id*/
    @ManyToOne
    @JoinColumn(name = "region_id", foreignKey = @ForeignKey(name = "FK_LOCATION_REGIONID"))
    RegionMaster regionMaster;

    /*Division Master id*/
    @ManyToOne
    @JoinColumn(name = "division_id", foreignKey = @ForeignKey(name = "FK_LOCATION_DIVISIONID"))
    DivisionMaster divisionMaster;

    /*Zone id*/
    @ManyToOne
    @JoinColumn(name = "zone_id", foreignKey = @ForeignKey(name = "FK_LOCATION_ZONEID"))
    ZoneMaster zoneMaster;

    /*City id*/
    @ManyToOne
    @JoinColumn(name = "city_id", foreignKey = @ForeignKey(name = "FK_LOCATION_CITYID"))
    CityMaster cityMaster;

    /*Agent id*/
    @ManyToOne
    @JoinColumn(name = "agent_id", foreignKey = @ForeignKey(name = "FK_LOCATION_AGENTID"))
    @Setter
    @JsonIgnoreProperties("partyAccountList")
    PartyMaster partyMaster;


    /*Cost Center id*/
    @ManyToOne
    @JoinColumn(name = "cost_center_id", foreignKey = @ForeignKey(name = "FK_LOCATION_COSTCENTERID"))
    CostCenterMaster costCenterMaster;

    /*Cost Center Code*/
    @Column(name = "cost_center_code", length = 10)
    String costCenterCode;

    /* Lov Status */
    @Column(name = "status", nullable = false, length = 10)
    @Enumerated(EnumType.STRING)
    LovStatus status;

    @Column(name = "date_format", nullable = false, length = 30)
    String dateFormat;

    @Column(name = "date_time_format", nullable = false, length = 30)
    String dateTimeFormat;

    @Column(name = "bank_name", length = 50)
    String bankName;

    @Column(name = "bank_address", length = 255)
    String bankAddress;

    @Column(name = "bank_account_type", length = 20)
    String bankAccountType;

    @Column(name = "bank_account_name", length = 50)
    String bankAccountName;

    @Column(name = "bank_account_number", length = 25)
    String bankAccountNumber;

    @Transient
    PartyMaster tmpPartyMaster;

    //Added as per Robert and Testing team FB
    /*Currency id*/
    @ManyToOne
    @JoinColumn(name = "currency_id", foreignKey = @ForeignKey(name = "FK_LOC_CURRENCYID"))
    CurrencyMaster currencyMaster;
	
	
	/* Location Setup Wizard */

    @Column(name = "salutation", length = 10)
    @Enumerated(EnumType.STRING)
    SalutationType salutation;

    @Column(name = "first_name")
    String firstName;

    @Column(name = "last_name")
    String lastName;

    @Column(name = "email", length = 500)
    String email;

    @Column(name = "phone", length = 100)
    String phone;

    @Transient
    String companyRegisteredName;

    @ManyToOne
    @JoinColumn(name = "state_id", foreignKey = @ForeignKey(name = "FK_LOCATION_STATE"))
    StateMaster stateMaster;

    @Column(name = "zip_code", length = 10)
    String zipCode;

    @Column(name = "is_approved", length = 5)
    @Enumerated(EnumType.STRING)
    BooleanType isApproved;

    @Column(name = "is_measurement", length = 20)
    @Enumerated(EnumType.STRING)
    Measurement measurement;

    @Transient
    String timeZoneAbb;

    public LocationMaster() {
    }

    public LocationMaster(String locationCode, String locationName, String timeZone, LovStatus status, String dateFormat, String dateTimeFormat) {
        this.locationCode = locationCode;
        this.locationName = locationName;
        this.timeZone = timeZone;
        this.status = status;
        this.dateFormat = dateFormat;
        this.dateTimeFormat = dateTimeFormat;

    }

    public LocationMaster(Long id) {
        this.id = id;
    }

    public LocationMaster(Long id, String locationCode) {
        this.id = id;
        this.locationCode = locationCode;
    }

    private LocationMaster(Builder builder) {
        setId(builder.id);
        setVersionLock(builder.versionLock);
        setSystemTrack(builder.systemTrack);
        setLocationCode(builder.locationCode);
        setLocationName(builder.locationName);
        setBranchName(builder.branchName);
        setTimeZone(builder.timeZone);
        setIsOurOwnOffice(builder.isOurOwnOffice);
        setIsHeadOffice(builder.isHeadOffice);
        setAddressLine1(builder.addressLine1);
        setAddressLine2(builder.addressLine2);
        setAddressLine3(builder.addressLine3);
        setAddressLine4(builder.addressLine4);
        setCountryMaster(builder.countryMaster);
        setCountryCode(builder.countryCode);
        setCompanyMaster(builder.companyMaster);
        setCompanyCode(builder.companyCode);
        setCountryNo(builder.countryNo);
        setCityNo(builder.cityNo);
        setRegionMaster(builder.regionMaster);
        setDivisionMaster(builder.divisionMaster);
        setZoneMaster(builder.zoneMaster);
        setCityMaster(builder.cityMaster);
        setPartyMaster(builder.partyMaster);
        setCostCenterMaster(builder.costCenterMaster);
        setCostCenterCode(builder.costCenterCode);
        setStatus(builder.status);
        setDateFormat(builder.dateFormat);
        setDateTimeFormat(builder.dateTimeFormat);
        setBankName(builder.bankName);
        setBankAddress(builder.bankAddress);
        setBankAccountType(builder.bankAccountType);
        setBankAccountName(builder.bankAccountName);
        setBankAccountNumber(builder.bankAccountNumber);
        setTmpPartyMaster(builder.tmpPartyMaster);
        setCurrencyMaster(builder.currencyMaster);
        setSalutation(builder.salutation);
        setFirstName(builder.firstName);
        setLastName(builder.lastName);
        setEmail(builder.email);
        setPhone(builder.phone);
        setCompanyRegisteredName(builder.companyRegisteredName);
        setStateMaster(builder.stateMaster);
        setZipCode(builder.zipCode);
        setIsApproved(builder.isApproved);
        setMeasurement(builder.measurement);
        setTimeZoneAbb(builder.timeZoneAbb);
    }

    @Transient
    public String getJavaDateFormat() {
        String format = null;

        if (dateFormat != null && dateFormat.trim().length() != 0) {
            format = dateFormat.replace("D", "d");
            format = format.replace("Y", "y");
        }
        return format;
    }


    public static final class Builder {
        private Long id;
        private long versionLock;
        private SystemTrack systemTrack;
        private String locationCode;
        private String locationName;
        private String branchName;
        private String timeZone;
        private BooleanType isOurOwnOffice;
        private BooleanType isHeadOffice;
        private String addressLine1;
        private String addressLine2;
        private String addressLine3;
        private String addressLine4;
        private CountryMaster countryMaster;
        private String countryCode;
        private CompanyMaster companyMaster;
        private String companyCode;
        private String countryNo;
        private String cityNo;
        private RegionMaster regionMaster;
        private DivisionMaster divisionMaster;
        private ZoneMaster zoneMaster;
        private CityMaster cityMaster;
        private PartyMaster partyMaster;
        private CostCenterMaster costCenterMaster;
        private String costCenterCode;
        private LovStatus status;
        private String dateFormat;
        private String dateTimeFormat;
        private String bankName;
        private String bankAddress;
        private String bankAccountType;
        private String bankAccountName;
        private String bankAccountNumber;
        private PartyMaster tmpPartyMaster;
        private CurrencyMaster currencyMaster;
        private SalutationType salutation;
        private String firstName;
        private String lastName;
        private String email;
        private String phone;
        private String companyRegisteredName;
        private StateMaster stateMaster;
        private String zipCode;
        private BooleanType isApproved;
        private Measurement measurement;
        private String timeZoneAbb;

        public Builder() {
        }

        public Builder(String locationCode, String locationName, String timeZone, LovStatus status, String dateFormat, String dateTimeFormat){
            this.locationCode = locationCode;
            this.locationName = locationName;
            this.timeZone = timeZone;
            this.status = status;
            this.dateFormat = dateFormat;
            this.dateTimeFormat = dateTimeFormat;
        }

        public Builder withId(Long val) {
            id = val;
            return this;
        }

        public Builder withVersionLock(long val) {
            versionLock = val;
            return this;
        }

        public Builder withSystemTrack(SystemTrack val) {
            systemTrack = val;
            return this;
        }

        public Builder withLocationCode(String val) {
            locationCode = val;
            return this;
        }

        public Builder withLocationName(String val) {
            locationName = val;
            return this;
        }

        public Builder withBranchName(String val) {
            branchName = val;
            return this;
        }

        public Builder withTimeZone(String val) {
            timeZone = val;
            return this;
        }

        public Builder withIsOurOwnOffice(BooleanType val) {
            isOurOwnOffice = val;
            return this;
        }

        public Builder withIsHeadOffice(BooleanType val) {
            isHeadOffice = val;
            return this;
        }

        public Builder withAddressLine1(String val) {
            addressLine1 = val;
            return this;
        }

        public Builder withAddressLine2(String val) {
            addressLine2 = val;
            return this;
        }

        public Builder withAddressLine3(String val) {
            addressLine3 = val;
            return this;
        }

        public Builder withAddressLine4(String val) {
            addressLine4 = val;
            return this;
        }

        public Builder withCountryMaster(CountryMaster val) {
            countryMaster = val;
            return this;
        }

        public Builder withCountryCode(String val) {
            countryCode = val;
            return this;
        }

        public Builder withCompanyMaster(CompanyMaster val) {
            companyMaster = val;
            return this;
        }

        public Builder withCompanyCode(String val) {
            companyCode = val;
            return this;
        }

        public Builder withCountryNo(String val) {
            countryNo = val;
            return this;
        }

        public Builder withCityNo(String val) {
            cityNo = val;
            return this;
        }

        public Builder withRegionMaster(RegionMaster val) {
            regionMaster = val;
            return this;
        }

        public Builder withDivisionMaster(DivisionMaster val) {
            divisionMaster = val;
            return this;
        }

        public Builder withZoneMaster(ZoneMaster val) {
            zoneMaster = val;
            return this;
        }

        public Builder withCityMaster(CityMaster val) {
            cityMaster = val;
            return this;
        }

        public Builder withPartyMaster(PartyMaster val) {
            partyMaster = val;
            return this;
        }

        public Builder withCostCenterMaster(CostCenterMaster val) {
            costCenterMaster = val;
            return this;
        }

        public Builder withCostCenterCode(String val) {
            costCenterCode = val;
            return this;
        }

        public Builder withStatus(LovStatus val) {
            status = val;
            return this;
        }

        public Builder withDateFormat(String val) {
            dateFormat = val;
            return this;
        }

        public Builder withDateTimeFormat(String val) {
            dateTimeFormat = val;
            return this;
        }

        public Builder withBankName(String val) {
            bankName = val;
            return this;
        }

        public Builder withBankAddress(String val) {
            bankAddress = val;
            return this;
        }

        public Builder withBankAccountType(String val) {
            bankAccountType = val;
            return this;
        }

        public Builder withBankAccountName(String val) {
            bankAccountName = val;
            return this;
        }

        public Builder withBankAccountNumber(String val) {
            bankAccountNumber = val;
            return this;
        }

        public Builder withTmpPartyMaster(PartyMaster val) {
            tmpPartyMaster = val;
            return this;
        }

        public Builder withCurrencyMaster(CurrencyMaster val) {
            currencyMaster = val;
            return this;
        }

        public Builder withSalutation(SalutationType val) {
            salutation = val;
            return this;
        }

        public Builder withFirstName(String val) {
            firstName = val;
            return this;
        }

        public Builder withLastName(String val) {
            lastName = val;
            return this;
        }

        public Builder withEmail(String val) {
            email = val;
            return this;
        }

        public Builder withPhone(String val) {
            phone = val;
            return this;
        }

        public Builder withCompanyRegisteredName(String val) {
            companyRegisteredName = val;
            return this;
        }

        public Builder withStateMaster(StateMaster val) {
            stateMaster = val;
            return this;
        }

        public Builder withZipCode(String val) {
            zipCode = val;
            return this;
        }

        public Builder withIsApproved(BooleanType val) {
            isApproved = val;
            return this;
        }

        public Builder withMeasurement(Measurement val) {
            measurement = val;
            return this;
        }

        public Builder withTimeZoneAbb(String val) {
            timeZoneAbb = val;
            return this;
        }

        public LocationMaster build() {
            return new LocationMaster(this);
        }
    }
}
