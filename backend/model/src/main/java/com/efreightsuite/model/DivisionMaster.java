package com.efreightsuite.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.UniqueConstraint;
import javax.persistence.Version;

import com.efreightsuite.enumeration.LovStatus;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;


@Entity
@Getter
@Setter
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Table(name = "efs_division_master",
        uniqueConstraints = {@UniqueConstraint(columnNames = {"division_code", "company_id"}, name = "UK_DIVISION_DIVISIONCODE"),
                @UniqueConstraint(columnNames = {"division_name", "company_id"}, name = "UK_DIVISION_DIVISIONNAME")}
)
public class DivisionMaster implements Serializable {
    private static final long serialVersionUID = 1L;

    /*Auto Generation Id*/
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    @Version
    @Column(name = "version_lock")
    long versionLock;

    /*Create/Last User and Data Info*/
    @Embedded
    SystemTrack systemTrack;

    /*Company id*/
    @ManyToOne
    @JoinColumn(name = "company_id", foreignKey = @ForeignKey(name = "FK_DIVISION_COMPANYID"))
    CompanyMaster companyMaster;

    /*Division Code*/
    @Column(name = "division_code", nullable = false, length = 10)
    String divisionCode;

    /*Division Name*/
    @Column(name = "division_name", nullable = false, length = 100)
    String divisionName;

    /*Lov Status*/
    @Column(name = "status", nullable = false, length = 10)
    @Enumerated(EnumType.STRING)
    LovStatus status = LovStatus.Active;

    public DivisionMaster() {
        // TODO Auto-generated constructor stub
    }

    public DivisionMaster(Long id) {
        this.id = id;
    }

}
