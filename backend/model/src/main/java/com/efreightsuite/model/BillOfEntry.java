package com.efreightsuite.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Version;

import com.efreightsuite.enumeration.TransactionType;
import com.efreightsuite.util.JsonDateDeserializer;
import com.efreightsuite.util.JsonDateSerializer;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Type;


@Entity
@Getter
@Setter
@Table(name = "efs_bill_of_entry")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class BillOfEntry implements Serializable {
    private static final long serialVersionUID = 1L;

    /* Auto Generation Id */
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    @Version
    @Column(name = "version_lock")
    long versionLock;

    /* Create/Last User and Data Info */
    @Embedded
    SystemTrack systemTrack;

    /* Shipment Service Detail */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "service_id", nullable = false, foreignKey = @ForeignKey(name = "FK_BOE_SERVIDETID"))
    @JsonBackReference
    ShipmentServiceDetail shipmentServiceDetail;

    /* Bill of Entry */
    @Column(name = "bill_of_entry", length = 13)
    Integer billOfEntry;

    /* Decleartion No */
    @Column(name = "decleartion_no", length = 13)
    Integer decleartionNo;

    /* Bill Of Entry Date */
    @Column(name = "boe_date")
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date boeDate;

    /* Bill Of Entry Value */
    @Column(name = "boe_value")
    Double boeValue;

    /* Bill Of Entry Invoice No */
    @Column(name = "boe_invoice_no", length = 13)
    String boeInvoiceNo;

    // Transaction Type
    @Column(name = "transaction_type")
    @Enumerated(EnumType.STRING)
    TransactionType transactionType;

    /* amount */
    @Column(name = "amount")
    Double amount;

    /* Process Date */
    @Column(name = "process_date")
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date processDate;

    /* Received Date */
    @Column(name = "received_date")
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date receivedDate;

    /* Decleartion No */
    @Column(name = "ack_no", length = 100)
    Integer ackNo;

    /* Ack Date */
    @Column(name = "ack_date")
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date ackDate;

    /* Note */
    @Column(name = "note")
    @Lob
    @Type(type = "org.hibernate.type.MaterializedClobType")
    String note;


}
