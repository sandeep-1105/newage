package com.efreightsuite.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Version;

import com.efreightsuite.enumeration.EnquiryCustomerStatus;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;


@Entity
@Table(name = "efs_enquiry_customer")
@Getter
@Setter
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class EnquiryCustomer implements Serializable {
    private static final long serialVersionUID = 1L;

    /* Auto Generation Id */
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    @Version
    @Column(name = "version_lock")
    long versionLock;

    /* Create/Last User and Data Info */
    @Embedded
    SystemTrack systemTrack;

    @Column(name = "name", nullable = true, length = 100)
    String name;

    /*First Name */
    @Column(name = "first_name")
    String firstName;

    /*Last Name */
    @Column(name = "last_name")
    String lastName;


    @Column(name = "address_line_1", length = 100)
    String addressLine1;

    /*Address Line 2 */
    @Column(name = "address_line_2", length = 100)
    String addressLine2;

    /*Address Line 3 */
    @Column(name = "address_line_3", length = 100)
    String addressLine3;

    /*PO Box */
    @Column(name = "po_box", length = 20)
    String poBox;

    /*State or Province*/
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "state_province", foreignKey = @ForeignKey(name = "FK_ENQ_CUST_STATEID"))
    StateMaster stateMaster;

    @Column(name = "state_province_code", length = 100)
    String stateMasterCode;

    /*City Id*/
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "city_id", foreignKey = @ForeignKey(name = "FK_ENQ_CUST_CITYID"))
    CityMaster cityMaster;

    @Column(name = "city_code", length = 100)
    String cityMasterCode;

    /* Country Id */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "country_id", foreignKey = @ForeignKey(name = "FK_ENQ_CUST_COUNTRYID"))
    CountryMaster countryMaster;

    /* Country Code*/
    @Column(name = "country_code", length = 10)
    String countryCode;

    /*Zip Code */
    @Column(name = "zip_code", length = 10)
    String zipCode;


    /*Contact Person */
    @Column(name = "contact_person")
    String contactPerson;

    /*Phone */
    @Column(name = "phone", length = 100)
    String phone;

    /*Fax */
    @Column(name = "fax", length = 100)
    String fax;

    /*Mobile No */
    @Column(name = "mobile_no", length = 100)
    String mobileNo;

    /*Email */
    @Column(name = "email", length = 500)
    String email;

    @Column(name = "status", nullable = false, length = 20)
    @Enumerated(EnumType.STRING)
    EnquiryCustomerStatus enquiryCustomerStatus = EnquiryCustomerStatus.Pending;

    public EnquiryCustomer() {
    }
}
