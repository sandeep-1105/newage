package com.efreightsuite.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Version;

import com.efreightsuite.enumeration.ChargeType;
import com.efreightsuite.enumeration.ForwarderDirect;
import com.efreightsuite.enumeration.PPCC;
import com.efreightsuite.enumeration.WhoRouted;
import com.efreightsuite.enumeration.YesNo;
import com.efreightsuite.util.JsonDateDeserializer;
import com.efreightsuite.util.JsonDateSerializer;
import com.efreightsuite.util.JsonForwardDirectDeSerializer;
import com.efreightsuite.util.JsonForwardDirectSerializer;
import com.efreightsuite.util.JsonYesNoDeserializer;
import com.efreightsuite.util.JsonYesNoSerializer;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Getter;
import lombok.Setter;


@Entity
@Getter
@Setter
@Table(name = "efs_pricing_port_pair")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class PricingPortPair implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    @Version
    @Column(name = "version_lock")
    long versionLock;

    @Embedded
    SystemTrack systemTrack;

    @ManyToOne
    @JoinColumn(name = "pricing_master_id", foreignKey = @ForeignKey(name = "FK_PRICPORT_PRICID"))
    @JsonBackReference
    PricingMaster pricingMaster;

    @Column(name = "hazardous")
    @Enumerated(EnumType.STRING)
    @JsonSerialize(using = JsonYesNoSerializer.class)
    @JsonDeserialize(using = JsonYesNoDeserializer.class)
    YesNo hazardous;

    @Column(name = "charge_type")
    @Enumerated(EnumType.STRING)
    ChargeType chargeType;

    @ManyToOne
    @JoinColumn(name = "company_id", nullable = false, foreignKey = @ForeignKey(name = "FK_PRICPORT_COMPID"))
    CompanyMaster companyMaster;

    @Column(name = "company_code", nullable = false, length = 30)
    String companyCode;

    @ManyToOne
    @JoinColumn(name = "location_id", nullable = false, foreignKey = @ForeignKey(name = "FK_PRICPORT_LOCTID"))
    LocationMaster locationMaster;

    @Column(name = "location_code", nullable = false, length = 30)
    String locationCode;

    @ManyToOne
    @JoinColumn(name = "country_id", nullable = false, foreignKey = @ForeignKey(name = "FK_PRICPORT_COUNTID"))
    CountryMaster countryMaster;

    @Column(name = "country_code", nullable = false, length = 30)
    String countryCode;

    @ManyToOne
    @JoinColumn(name = "service_id", foreignKey = @ForeignKey(name = "FK_PPP_SERVICE_ID"))
    ServiceMaster serviceMaster;


    @ManyToOne
    @JoinColumn(name = "charge_id", foreignKey = @ForeignKey(name = "FK_PRICPORT_CHARGID"))
    ChargeMaster chargeMaster;

    @Column(name = "charge_code", length = 30)
    String chargeCode;

    @ManyToOne
    @JoinColumn(name = "unit_id", foreignKey = @ForeignKey(name = "FK_PRICPORT_UNITID"))
    UnitMaster unitMaster;

    @Column(name = "unit_code", length = 30)
    String unitCode;

    @ManyToOne
    @JoinColumn(name = "currency_id", foreignKey = @ForeignKey(name = "FK_PRICPORT_CURREID"))
    CurrencyMaster currencyMaster;

    @Column(name = "currency_code", length = 30)
    String currencyCode;

    @Column(name = "valid_from_date", nullable = false)
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date validFromDate;

    @Column(name = "valid_to_date", nullable = false)
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date validToDate;

    @Column(name = "min_sel_in_minimum")
    Double minSelInMinimum;

    @Column(name = "min_sel_in_amount")
    Double minSelInAmount;

    @Column(name = "std_sel_in_minimum")
    Double stdSelInMinimum;

    @Column(name = "std_sel_in_amount")
    Double stdSelInAmount;

    @Column(name = "cost_in_minimum")
    Double costInMinimum;

    @Column(name = "cost_in_amount")
    Double costInAmount;


    //Default Charge Master Columns

    /* Division Id */
    @ManyToOne
    @JoinColumn(name = "division_id", foreignKey = @ForeignKey(name = "FK_PRICPORT_DIVMASID"))
    DivisionMaster divisionMaster;


    /*Clearance*/
    @Column(name = "is_clearance", length = 5)
    @Enumerated(EnumType.STRING)
    @JsonSerialize(using = JsonYesNoSerializer.class)
    @JsonDeserialize(using = JsonYesNoDeserializer.class)
    YesNo isClearance;


    /* forwarderOrDirect*/
    @Column(name = "is_forwarder_direct", length = 15)
    @Enumerated(EnumType.STRING)
    @JsonSerialize(using = JsonForwardDirectSerializer.class)
    @JsonDeserialize(using = JsonForwardDirectDeSerializer.class)
    ForwarderDirect isForwarderOrDirect;


    /* Agent Id */
    @ManyToOne
    @JoinColumn(name = "agent_id", foreignKey = @ForeignKey(name = "FK_PRICPORT_AGTSID"))
    PartyMaster agent;


    /* ourTransport*/
    @Column(name = "is_our_transport", length = 5)
    @Enumerated(EnumType.STRING)
    @JsonSerialize(using = JsonYesNoSerializer.class)
    @JsonDeserialize(using = JsonYesNoDeserializer.class)
    YesNo isOurTransport;

    /* personalEffect*/
    @Column(name = "is_personal_effect", length = 5)
    @Enumerated(EnumType.STRING)
    @JsonSerialize(using = JsonYesNoSerializer.class)
    @JsonDeserialize(using = JsonYesNoDeserializer.class)
    YesNo isPersonalEffect;


    /* coload*/
    @Column(name = "is_coload", length = 5)
    @Enumerated(EnumType.STRING)
    @JsonSerialize(using = JsonYesNoSerializer.class)
    @JsonDeserialize(using = JsonYesNoDeserializer.class)
    YesNo isCoload;


    /* transit*/
    @Column(name = "is_transit", length = 5)
    @Enumerated(EnumType.STRING)
    @JsonSerialize(using = JsonYesNoSerializer.class)
    @JsonDeserialize(using = JsonYesNoDeserializer.class)
    YesNo isTransit;

    /* Commodity */
    @ManyToOne
    @JoinColumn(name = "commodity_id", foreignKey = @ForeignKey(name = "FK_PRICPORT_CMDTID"))
    CommodityMaster commodityMaster;

    /* Carrier Id */
    @ManyToOne
    @JoinColumn(name = "carrier_id", foreignKey = @ForeignKey(name = "FK_PRICPORT_CARID"))
    CarrierMaster carrierMaster;

    /*TOS Id*/
    @ManyToOne
    @JoinColumn(name = "tos_id", foreignKey = @ForeignKey(name = "FK_PRICPORT_TOSID"))
    TosMaster tosMaster;

    /* Prepaid / Collect */
    @Column(name = "ppcc")
    @Enumerated(EnumType.STRING)
    PPCC ppcc;

    /* Who Routed */
    @Column(name = "who_routed")
    @Enumerated(EnumType.STRING)
    WhoRouted whoRouted;

    /* Shipper */
    @ManyToOne
    @JoinColumn(name = "shipper_id", foreignKey = @ForeignKey(name = "FK_PRICPORT_SHPID"))
    PartyMaster shipper;

    /* Shipper */
    @ManyToOne
    @JoinColumn(name = "consignee_id", foreignKey = @ForeignKey(name = "FK_PRICPORT_CONSID"))
    PartyMaster consignee;


}
