package com.efreightsuite.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Version;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;


@Entity
@Getter
@Setter
@Table(name = "efs_iata_rate_charge")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class IataRateCharge implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    @Version
    @Column(name = "version_lock")
    long versionLock;

    @Embedded
    SystemTrack systemTrack;

    @ManyToOne
    @JoinColumn(name = "Iata_rate_id", foreignKey = @ForeignKey(name = "FK_IATARATECHARGE_IATARATEID"))
    @JsonBackReference
    IataRateMaster iataRateMaster;

    @ManyToOne
    @JoinColumn(name = "company_id", nullable = false, foreignKey = @ForeignKey(name = "FK_IATARATECHARGE_COMPID"))
    CompanyMaster companyMaster;

    @Column(name = "company_code", nullable = false, length = 30)
    String companyCode;

    @ManyToOne
    @JoinColumn(name = "location_id", nullable = false, foreignKey = @ForeignKey(name = "FK_IATARATECHARGE_LOCTID"))
    LocationMaster locationMaster;

    @Column(name = "location_code", nullable = false, length = 30)
    String locationCode;

    @ManyToOne
    @JoinColumn(name = "country_id", nullable = false, foreignKey = @ForeignKey(name = "FK_IATARATECHARGE_COUNTID"))
    CountryMaster countryMaster;

    @Column(name = "country_code", nullable = false, length = 30)
    String countryCode;

    @ManyToOne
    @JoinColumn(name = "pod", nullable = false, foreignKey = @ForeignKey(name = "FK_IATARATECHARGE_PODID"))
    PortMaster pod;

    @Column(name = "amount")
    Double amount;

    @Column(name = "min_amount")
    Double minAmount;

    @Column(name = "normal")
    Double normal;

    @Column(name = "plus_45")
    Double plus45;

    @Column(name = "plus_100")
    Double plus100;

    @Column(name = "plus_300")
    Double plus300;

    @Column(name = "plus_500")
    Double plus500;

    @Column(name = "plus_1000")
    Double plus1000;

}
