package com.efreightsuite.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.UniqueConstraint;
import javax.persistence.Version;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;


@Entity
@Getter
@Setter
@Table(name = "efs_edi_configuration_master", uniqueConstraints = {
        @UniqueConstraint(columnNames = {"key", "location_id"}, name = "UK_EDI_CONFIGURATION_KEY")})
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class EdiConfigurationMaster implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    @Version
    @Column(name = "version_lock")
    long versionLock;

    @Embedded
    SystemTrack systemTrack;

    @Column(name = "key", nullable = false)
    String ediConfigurationKey;

    @Column(name = "value", nullable = false)
    String ediConfigurationValue;

    @Column(name = "description", length = 4000)
    String description;

    /*Location id*/
    @ManyToOne
    @JoinColumn(name = "location_id", foreignKey = @ForeignKey(name = "FK_EDICOMLOC_LOCATIONID"))
    LocationMaster locationMaster;

}
