package com.efreightsuite.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;


@Entity
@Getter
@Setter
@Table(name = "efs_shipment_attachment", uniqueConstraints = {
        @UniqueConstraint(columnNames = {"ref_no", "shipment_service_id"}, name = "UK_SHIPMENTATT_REF")})
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class ShipmentAttachment extends SuperAttachment implements Serializable {
    private static final long serialVersionUID = 1L;


    /* Document ID */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "document_id", foreignKey = @ForeignKey(name = "FK_SHIPMENT_DOCUMENTID"))
    public DocumentMaster documentMaster;


    /* Shipment Service ID */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "shipment_service_id", foreignKey = @ForeignKey(name = "FK_SHIPATTCH_SERVICEID"))
    @JsonBackReference
    public ShipmentServiceDetail shipmentServiceDetail;

}
