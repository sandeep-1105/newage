package com.efreightsuite.model.common;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.UniqueConstraint;

import com.efreightsuite.enumeration.LovStatus;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;


@Entity
@Getter
@Setter
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Table(name = "efs_common_state_province",
        uniqueConstraints = {
                @UniqueConstraint(columnNames = {"state_code", "country_id"}, name = "UK_CMSTATEPROVINCE_STATECODE"),
                @UniqueConstraint(columnNames = {"state_name", "country_id"}, name = "UK_CMSTATEPROVINCE_STATENAME")
        },
        indexes = {
                @Index(name = "IDX_STATE_NAME", columnList = "state_name"),
                @Index(name = "IDX_STATE_CODE", columnList = "state_code")
        }
)
public class CommonStateMaster implements Serializable {


    private static final long serialVersionUID = 1L;

    /*Auto Generation Id*/
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    /*State Code*/
    @Column(name = "state_code", nullable = false, length = 10)
    String stateCode;

    /*State Name*/
    @Column(name = "state_name", nullable = false, length = 100)
    String stateName;

    /*Lov Status*/
    @Column(name = "status", nullable = false, length = 10)
    @Enumerated(EnumType.STRING)
    LovStatus status;

    /*Country Id*/
    @ManyToOne
    @JoinColumn(name = "country_id", foreignKey = @ForeignKey(name = "FK_CMSTATEPROVINCE_COUNTRYID"), nullable = false)
    CommonCountryMaster countryMaster;

    /*Country Code*/
    @Column(name = "country_code", length = 10, nullable = false)
    String countryCode;

    public CommonStateMaster() {
    }

    public CommonStateMaster(CommonStateMaster stateMaster) {
        this.id = stateMaster.getId();
        this.stateName = stateMaster.getStateName();
        this.stateCode = stateMaster.getStateCode();
        this.status = stateMaster.getStatus();
    }

    public CommonStateMaster(String stateCode, String stateName, String countryCode, CommonCountryMaster commonCountryMaster, LovStatus status) {
        this.stateCode = stateCode;
        this.stateName = stateName;
        this.status = status;
        this.countryCode = countryCode;
        this.countryMaster = commonCountryMaster;
    }
}
