package com.efreightsuite.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.UniqueConstraint;
import javax.persistence.Version;

import com.efreightsuite.enumeration.LovStatus;
import com.efreightsuite.enumeration.PPCC;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;


@Entity
@Getter
@Setter
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Table(name = "efs_tos_master", uniqueConstraints = {
        @UniqueConstraint(columnNames = {"tos_code"}, name = "UK_TOSMASTER_TOSCODE"),
        @UniqueConstraint(columnNames = {"tos_name"}, name = "UK_TOSMASTER_TOSNAME")})
public class TosMaster implements Serializable {
    private static final long serialVersionUID = 1L;

    /* Auto Generation Id */
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    @Version
    @Column(name = "version_lock")
    long versionLock;

    /* Create/Last User and Date Info */
    @Embedded
    SystemTrack systemTrack;

    /* TOS Code */
    @Column(name = "tos_code", nullable = false, length = 10)
    String tosCode;

    /* TOS Name */
    @Column(name = "tos_name", nullable = false, length = 100)
    String tosName;

    /* Description */
    @Column(name = "description", length = 4000)
    String description;

    /* Freight PPCC */
    @Enumerated(EnumType.STRING)
    @Column(name = "freight_ppcc", length = 10)
    PPCC freightPPCC;

    /* Lov Status */
    @Column(name = "status", nullable = false, length = 10)
    @Enumerated(EnumType.STRING)
    LovStatus status;

    public TosMaster() {
    }

    public TosMaster(String tosName, String tosCode, LovStatus status, SystemTrack systemTrack) {
        this.systemTrack = systemTrack;
        this.tosCode = tosCode;
        this.tosName = tosName;
        this.status = status;
    }
}
