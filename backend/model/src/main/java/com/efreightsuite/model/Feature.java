package com.efreightsuite.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;

import com.efreightsuite.enumeration.YesNo;
import com.efreightsuite.util.JsonYesNoDeserializer;
import com.efreightsuite.util.JsonYesNoSerializer;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Getter;
import lombok.Setter;


@Entity
@Getter
@Setter
@Table(name = "efs_feature", uniqueConstraints = {
        @UniqueConstraint(columnNames = {"feature_value"}, name = "UK_FEATURE_FEATUREVALUE")})
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})

public class Feature implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    Long id;

    @Column(name = "feature_name", nullable = false, length = 100)
    String featureName; //Name of the feature Used for UI

    @Column(name = "feature_value", nullable = false, length = 100)
    String featureValue; //Feature Value

    /* Status */
    @Column(name = "is_applicable", nullable = false, length = 10)
    @Enumerated(EnumType.STRING)
    @JsonSerialize(using = JsonYesNoSerializer.class)
    @JsonDeserialize(using = JsonYesNoDeserializer.class)
    YesNo isApplicable;

    @ManyToOne
    Feature parent;

    @OneToMany(mappedBy = "parent")
    @OrderBy("feature_name ASC")
    List<Feature> features; //Is the feature is parent or Child

    @Column(name = "is_list", length = 10, nullable = false)
    @Enumerated(EnumType.STRING)
    @JsonSerialize(using = JsonYesNoSerializer.class)
    @JsonDeserialize(using = JsonYesNoDeserializer.class)
    YesNo list;

    @Transient
    String valList;

    @Column(name = "is_create", length = 10, nullable = false)
    @Enumerated(EnumType.STRING)
    @JsonSerialize(using = JsonYesNoSerializer.class)
    @JsonDeserialize(using = JsonYesNoDeserializer.class)
    YesNo create;

    @Transient
    String valCreate;

    @Column(name = "is_modify", length = 10, nullable = false)
    @Enumerated(EnumType.STRING)
    @JsonSerialize(using = JsonYesNoSerializer.class)
    @JsonDeserialize(using = JsonYesNoDeserializer.class)
    YesNo modify;

    @Transient
    String valModify;

    @Column(name = "is_view", length = 10, nullable = false)
    @Enumerated(EnumType.STRING)
    @JsonSerialize(using = JsonYesNoSerializer.class)
    @JsonDeserialize(using = JsonYesNoDeserializer.class)
    YesNo view;

    @Transient
    String valView;

    @Column(name = "is_delete", length = 10, nullable = false)
    @Enumerated(EnumType.STRING)
    @JsonSerialize(using = JsonYesNoSerializer.class)
    @JsonDeserialize(using = JsonYesNoDeserializer.class)
    YesNo delete;

    @Transient
    String valDelete;

    @Column(name = "is_download", length = 10, nullable = false)
    @Enumerated(EnumType.STRING)
    @JsonSerialize(using = JsonYesNoSerializer.class)
    @JsonDeserialize(using = JsonYesNoDeserializer.class)
    YesNo download;

    @Transient
    String valDownload;

    public Feature() {

    }

    public Feature(Long id) {
        this.id = id;
    }
}
