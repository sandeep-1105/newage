package com.efreightsuite.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.UniqueConstraint;
import javax.persistence.Version;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;


@Entity
@Table(name = "efs_page_master", uniqueConstraints = {
        @UniqueConstraint(columnNames = {"page_name", "page_country_id"}, name = "UK_PAGE_NAME"),
        @UniqueConstraint(columnNames = {"page_code", "page_country_id"}, name = "UK_PAGE_CODE")})
@Setter
@Getter
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class PageMaster implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /* Auto Generation Id */
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    /* Create/Last User and Date Info */
    @Embedded
    SystemTrack systemTrack;

    @Version
    @Column(name = "version_lock")
    long versionLock;

    @Column(name = "page_code", nullable = false)
    String pageCode;

    @Column(name = "page_name", nullable = false)
    String pageName;

    @ManyToOne
    @JoinColumn(name = "page_country_id", nullable = false, foreignKey = @ForeignKey(name = "PAGE_COUNTRY_MASTER_FK"))
    @JsonBackReference
    CountryReportConfigMaster pageCountryMaster;

}
