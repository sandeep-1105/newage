package com.efreightsuite.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Transient;
import javax.persistence.Version;

import com.efreightsuite.enumeration.CRN;
import com.efreightsuite.enumeration.DUE;
import com.efreightsuite.enumeration.PPCC;
import com.efreightsuite.util.JsonDateDeserializer;
import com.efreightsuite.util.JsonDateSerializer;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.envers.Audited;
import org.hibernate.envers.RelationTargetAuditMode;


@Entity
@Getter
@Setter
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Table(name = "efs_consol_rate")
@Audited
public class ConsolCharge implements Serializable {
    private static final long serialVersionUID = 1L;

    /* Auto Generation Id */
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    @Version
    @Column(name = "version_lock")
    long versionLock;

    /* Create/Last User and Data Info */
    @Embedded
    SystemTrack systemTrack;

    /* Location Id */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "location_id", foreignKey = @ForeignKey(name = "FK_CONSRATE_LOCATIONID"))
    @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
    LocationMaster location;

    /* Location Code */
    @Column(name = "location_code", length = 10)
    String locationCode;

    /* Company Id */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "company_id", foreignKey = @ForeignKey(name = "FK_CONSRATE_COMPANYID"))
    @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
    CompanyMaster company;

    @Column(name = "company_code", length = 100)
    String companyCode;

    /* Country Id */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "country_id", foreignKey = @ForeignKey(name = "FK_CONSRATE_COUNTRYID"))
    @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
    CountryMaster country;

    /* Country Code */
    @Column(name = "country_code", length = 10)
    String countryCode;


    /* Consol UID */
    @Column(name = "consol_uid")
    String consolUid;

    /* Service id */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "consol_id", foreignKey = @ForeignKey(name = "FK_CONSRATE_SERVICEID"))
    @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
    @JsonBackReference
    Consol consol;


    /* Charge Id */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "charge_id", foreignKey = @ForeignKey(name = "FK_CONSRATE_CHARGEID"))
    @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
    ChargeMaster chargeMaster;

    /* Charge Code */
    @Column(name = "charge_code")
    String chargeCode;

    /* Charge Name */
    @Column(name = "charge_name")
    String chargeName;

    /* Cost/Revenue */
    @Column(name = "crn")
    @Enumerated(EnumType.STRING)
    CRN crn;

    /* Freight Term */
    @Column(name = "ppcc")
    @Enumerated(EnumType.STRING)
    PPCC ppcc;

    /* Currency */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "currency_id", foreignKey = @ForeignKey(name = "FK_CONSRATE_CURRENCYID"))
    @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
    CurrencyMaster currency;

    /* Currency Code */
    @Column(name = "currency_code")
    String currencyCode;

    /* ROE */
    @Column(name = "currency_roe")
    Double currencyRoe;

    /* Unit */
    @Column(name = "unit")
    Double unit;

    /* Amount per unit*/
    @Column(name = "amount_per_unit")
    Double amountPerUnit;

    /* Amount */
    @Column(name = "amount")
    Double amount;

    /* Local Amount */
    @Column(name = "local_amount")
    Double localAmount;

    /* Due */
    @Column(name = "due")
    @Enumerated(EnumType.STRING)
    DUE due;

    /*revisionType*/
    @Transient
    String revisionType;

    @Transient
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date auditDate;

}
