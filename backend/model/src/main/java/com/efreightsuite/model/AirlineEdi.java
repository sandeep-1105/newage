package com.efreightsuite.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

import com.efreightsuite.enumeration.DimensionUnit;
import com.efreightsuite.enumeration.EdiStatus;
import com.efreightsuite.enumeration.PPCC;
import com.efreightsuite.enumeration.RateClass;
import com.efreightsuite.enumeration.YesNo;
import com.efreightsuite.util.JsonDateDeserializer;
import com.efreightsuite.util.JsonDateSerializer;
import com.efreightsuite.util.JsonYesNoDeserializer;
import com.efreightsuite.util.JsonYesNoSerializer;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Getter;
import lombok.Setter;


@Entity
@Getter
@Setter
@Table(name = "efs_airline_edi")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class AirlineEdi implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    @Embedded
    SystemTrack systemTrack;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "location_id", foreignKey = @ForeignKey(name = "FK_AIR_EDI_LOCATIONID"))
    LocationMaster locationMaster;


    @Column(name = "location_code", length = 30)
    String locationCode;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "company_id", foreignKey = @ForeignKey(name = "FK_AIR_EDI_COMPANYID"))
    CompanyMaster companyMaster;

    @Column(name = "company_code", length = 30)
    String companyCode;

    /* Country */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "country_id", foreignKey = @ForeignKey(name = "FK_AIR_EDI_COUNTRYID"))
    CountryMaster countryMaster;

    @Column(name = "country_code", length = 30)
    String countryCode;

    @Column(name = "edi_generate_date")
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date ediGenerateDate;

    @Column(name = "eawb", length = 10, nullable = false)
    @Enumerated(EnumType.STRING)
    @JsonSerialize(using = JsonYesNoSerializer.class)
    @JsonDeserialize(using = JsonYesNoDeserializer.class)
    YesNo eawb = YesNo.No;

    @Column(name = "paper", length = 10, nullable = false)
    @Enumerated(EnumType.STRING)
    @JsonSerialize(using = JsonYesNoSerializer.class)
    @JsonDeserialize(using = JsonYesNoDeserializer.class)
    YesNo paper = YesNo.No;

    @Column(name = "mawb_no", length = 30)
    String mawbNo;

    @Column(name = "awb_date")
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date awbDate;

    @Column(name = "master_date")
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date masterDate;

    /* Reference Id */
    @Column(name = "service_uid", length = 30)
    String serviceUid;

    /* Master Shipment Uid */
    @Column(name = "master_shipment_uid", length = 30)
    String masterShipmentUid;

    @Column(name = "direct_sipment", length = 10, nullable = false)
    @Enumerated(EnumType.STRING)
    @JsonSerialize(using = JsonYesNoSerializer.class)
    @JsonDeserialize(using = JsonYesNoDeserializer.class)
    YesNo directShipment;

	/*
     * MAWB
	 */

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "shipper_id", foreignKey = @ForeignKey(name = "FK_AIR_EDI_SHIPPER_ID"))
    PartyMaster shipper;

    @Column(name = "shipper_code", length = 100)
    String shipperCode;

    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "shipper_address_id", foreignKey = @ForeignKey(name = "FK_AIR_EDI_SHIPPERADDID"))
    AddressMaster shipperAddress;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "consignee_id", foreignKey = @ForeignKey(name = "FK_AIR_EDI_CONSIGNEE_ID"))
    PartyMaster consignee;

    @Column(name = "consignee_code", length = 100)
    String consigneeCode;

    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "consignee_address_id", foreignKey = @ForeignKey(name = "FK_AIR_EDI_AGENTDESTADDID"))
    AddressMaster consigneeAddress;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "agent_id", foreignKey = @ForeignKey(name = "FK_AIR_EDI_AGENT_ID"))
    PartyMaster agent;

    @Column(name = "agent_code", length = 100)
    String agentCode;

    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "agent_address_id", foreignKey = @ForeignKey(name = "FK_AIR_EDI_AGENTADDID"))
    AddressMaster agentAddress;

    @Column(name = "agent_iata_code", length = 30)
    String agentIataCode;


    /* Commodity */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "commodity_id", foreignKey = @ForeignKey(name = "FK_AIR_EDI_COMMODITY_ID"))
    CommodityMaster commodityMaster;

    @Column(name = "commodity_code", length = 100)
    String commodityCode;

    /* Currency */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "currency_id", foreignKey = @ForeignKey(name = "FK_AIR_EDI_CURRENCY_ID"))
    CurrencyMaster currencyMaster;

    @Column(name = "currency_code", length = 100)
    String currencyCode;
	
	/*
	 * Routing and carrier info
	 */

    /* Origin */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "origin_id", foreignKey = @ForeignKey(name = "FK_AIR_EDI_ORIGIN_ID"))
    PortMaster origin;

    @Column(name = "origin_code", length = 100)
    String originCode;

    /* Airport of Loading */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "pol_id", foreignKey = @ForeignKey(name = "FK_AIR_EDI_POL_ID"))
    PortMaster pol;

    @Column(name = "pol_code", length = 100)
    String polCode;

    /* Airport of Discharge */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "pod_id", foreignKey = @ForeignKey(name = "FK_AIR_EDI_POD_ID"))
    PortMaster pod;

    @Column(name = "pod_code", length = 100)
    String podCode;

    /* Destination */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "dest_id", foreignKey = @ForeignKey(name = "FK_AIR_EDI_DEST_ID"))
    PortMaster destination;

    @Column(name = "dest_code", length = 100)
    String destCode;

    /* Carrier */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "carrier_id", foreignKey = @ForeignKey(name = "FK_AIR_EDI_CARRIER_ID"))
    CarrierMaster carrierMaster;

    @Column(name = "carrier_code", length = 100)
    String carrierCode;

    /* Flight Number */
    @Column(name = "flight_no", length = 30)
    String flightNumber;

    /* List of Connection */
    @OneToMany(mappedBy = "airlineEdi", cascade = CascadeType.ALL, orphanRemoval = true)
    @JsonManagedReference
    List<AirlineEdiConnection> connectionList = new ArrayList<>();

	/*
	 * Pack and Dimension
	 */

    /* Booking No Of Pack */
    @Column(name = "no_of_pieces")
    Long noOfPieces;

    /* gross weight */
    @Column(name = "gross_weight")
    Double grossWeight;

    /* volume weight */
    @Column(name = "volume_weight")
    Double volumeWeight;

    /* chargeble weight */
    @Column(name = "chargeble_weight")
    Double chargebleWeight;

    /* Dimension Unit */
    @Column(name = "dimension_unit", nullable = false, length = 50)
    @Enumerated(EnumType.STRING)
    DimensionUnit dimensionUnit;

    /* Dimension Unit Value */
    @Column(name = "dimension_unit_value")
    Double dimensionUnitValue;

    @Column(name = "dim_no_of_pieces")
    Long dimNoOfPieces;

    @Column(name = "dim_gross_weight")
    Double dimGrossWeight;

    @Column(name = "dim_gross_weight_in_pound")
    Double dimGrossWeightInPound;

    @Column(name = "dim_volume_weight")
    Double dimVolumeWeight;

    @Column(name = "dim_volume_weight_in_pound")
    Double dimVolumeWeightInPound;

    /* List of Dimension */
    @OneToMany(mappedBy = "airlineEdi", cascade = CascadeType.ALL, orphanRemoval = true)
    @JsonManagedReference
    List<AirlineEdiDimension> dimensionList = new ArrayList<>();

    /*
     * HAWB
     */
	/* List of Houses */
    @OneToMany(mappedBy = "airlineEdi", cascade = CascadeType.ALL, orphanRemoval = true)
    @JsonManagedReference
    List<AirlineEdiHouse> houseList = new ArrayList<>();

    /* Freight Term */
    @Column(name = "freight_term", length = 10, nullable = false)
    @Enumerated(EnumType.STRING)
    PPCC ppcc;

    /* Rate Class */
    @Column(name = "rate_class", length = 2)
    @Enumerated(EnumType.STRING)
    RateClass rateClass;

    /* Commodity Description */
    @Column(name = "commodity_description", length = 4000)
    String commodityDescription;

    /* List of status */
    @OneToMany(mappedBy = "airlineEdi", cascade = CascadeType.ALL, orphanRemoval = true)
    @JsonManagedReference
    List<AirlineEdiStatus> statusList = new ArrayList<>();

    /* Status Message */
    @Column(name = "status_messsage", length = 4000, nullable = true)
    String statusMessage;

    @Column(name = "status", length = 30)
    @Enumerated(EnumType.STRING)
    EdiStatus status;

    @Column(name = "value_for_carriage", length = 10)
    String valueForCarriage;

    @Column(name = "value_for_custom", length = 10)
    String valueForCustomDeclarations;

    @Column(name = "value_for_insurance", length = 10)
    String valueForInsurance;

    @Column(name = "handling_information", length = 100)
    String handlingInformation;

    public AirlineEdi() {
    }

    public AirlineEdi(Long id) {
        this.id = id;
    }

    public LocationMaster getLocationMaster() {
        if (locationMaster != null) {
            if (locationMaster.getPartyMaster() != null) {
                locationMaster.setPartyMaster(null);
            }
        }

        return locationMaster;
    }

}
