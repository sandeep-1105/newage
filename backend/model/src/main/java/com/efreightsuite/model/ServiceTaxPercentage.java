package com.efreightsuite.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Version;

import com.efreightsuite.enumeration.LovStatus;
import com.efreightsuite.util.JsonDateDeserializer;
import com.efreightsuite.util.JsonDateSerializer;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Getter;
import lombok.Setter;


@Entity
@Getter
@Setter
@Table(name = "efs_service_tax_percentage")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class ServiceTaxPercentage implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    @Version
    @Column(name = "version_lock")
    long versionLock;

    /* Create/Last User and Data Info */
    @Embedded
    SystemTrack systemTrack;

    @Column(name = "tax_code", nullable = false, length = 30)
    String taxCode;

    @Column(name = "tax_name", nullable = false, length = 100)
    String taxName;

    /* Lov Status */
    @Column(name = "status", length = 10)
    @Enumerated(EnumType.STRING)
    LovStatus status;

    @ManyToOne
    @JoinColumn(name = "currency_id", nullable = false, foreignKey = @ForeignKey(name = "FK_SERVTAXPERC_CURRENCYID"))
    CurrencyMaster currencyMaster;

	/*@ManyToOne
	@JoinColumn(name = "charge_id", nullable=false, foreignKey = @ForeignKey(name = "FK_CHARGEID"))
	ChargeMaster chargeMaster;*/

    @ManyToOne
    @JoinColumn(name = "sub_ldgr_id", nullable = false, foreignKey = @ForeignKey(name = "FK_SLEDGERID"))
    SubLedgerMaster subLedgerMaster;


    @ManyToOne
    @JoinColumn(name = "gen_ldgr_id", nullable = false, foreignKey = @ForeignKey(name = "FK_SGENLEDGERID"))
    GeneralLedgerAccount generalLedgerMaster;

    @Column(name = "percentage", nullable = false, length = 100)
    Double percentage;

    @ManyToOne
    @JoinColumn(name = "service_tax_category_id", nullable = false, foreignKey = @ForeignKey(name = "FK_SERVTAXPERC_STCID"))
    @JsonBackReference
    ServiceTaxCategoryMaster serviceTaxCategory;

    @Column(name = "valid_from", nullable = false)
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date validFrom;

    @ManyToOne
    @JoinColumn(name = "company_id", nullable = false, foreignKey = @ForeignKey(name = "FK_SERVTAXPERC_COMPID"))
    CompanyMaster companyMaster;

    @Column(name = "company_code", nullable = false, length = 30)
    String companyCode;

    @ManyToOne
    @JoinColumn(name = "country_id", nullable = false, foreignKey = @ForeignKey(name = "FK_SERVTAXPERC_COUNTID"))
    CountryMaster countryMaster;

    @Column(name = "country_code", nullable = false, length = 30)
    String countryCode;

    @Column(name = "note", length = 200)
    String note;

}
