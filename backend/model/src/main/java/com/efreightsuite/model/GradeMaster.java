package com.efreightsuite.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;
import javax.persistence.Version;

import com.efreightsuite.enumeration.Priority;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;


@Entity
@Getter
@Setter
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Table(name = "efs_grade_master",
        uniqueConstraints = {
                @UniqueConstraint(columnNames = {"grade_name"}, name = "UK_GRADE_GRADENAME"),
                @UniqueConstraint(columnNames = {"priority"}, name = "UK_GRADE_GRADEPRIORITY")
        }
)

public class GradeMaster implements Serializable {
    private static final long serialVersionUID = 1L;

    /*Auto Generation Id*/
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    @Version
    @Column(name = "version_lock")
    long versionLock;

    @Embedded
    SystemTrack systemTrack;

    @Column(name = "grade_name", nullable = false, length = 100)
    String gradeName;

    @Transient
    Priority priority;


    @Column(name = "priority", nullable = true)
    Integer priorityValue;


    @Column(name = "color_code", length = 100)
    String colorCode;

}
