package com.efreightsuite.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.UniqueConstraint;
import javax.persistence.Version;

import com.efreightsuite.enumeration.LovStatus;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;


@Entity
@Getter
@Setter
@Table(name = "efs_party_group_master", uniqueConstraints = {
        @UniqueConstraint(columnNames = {"party_group_code"}, name = "UK_PARTYGRP_PARTYGROUPCODE"),
        @UniqueConstraint(columnNames = {"party_group_name"}, name = "UK_PARTYGRP_PARTYGROUPNAME")}

)
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class PartyGroupMaster implements Serializable {
    private static final long serialVersionUID = 1L;

    /* Auto Generation Id */
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    @Version
    @Column(name = "version_lock")
    long versionLock;

    /* Create/LastUser and Date Info */
    @Embedded
    SystemTrack systemTrack;

    /* Party Group Code */
    @Column(name = "party_group_code", nullable = false, length = 10)
    String partyGroupCode;

    /* Party Group Name */
    @Column(name = "party_group_name", nullable = false, length = 100)
    String partyGroupName;

    /* Status */
    @Column(name = "status", nullable = false, length = 10)
    @Enumerated(EnumType.STRING)
    LovStatus status;

    /* Sales Lead Location id */
    @ManyToOne
    @JoinColumn(name = "sales_lead_location_id", foreignKey = @ForeignKey(name = "FK_PARTYGRP_SALESLOCID"))
    LocationMaster salesLeadLocation;

    /* List of Party */
    @ManyToMany
    @JoinTable(name = "efs_party_group_has_party",
            joinColumns = {@JoinColumn(name = "party_group_id", foreignKey = @ForeignKey(name = "FK_PARTYGRP_PARTY_1"))},
            inverseJoinColumns = {@JoinColumn(name = "party_id", foreignKey = @ForeignKey(name = "FK_PARTYGRP_PARTY_2"))},
            uniqueConstraints = {@UniqueConstraint(columnNames = {"party_group_id", "party_id"}, name = "UK_PARTYGROUP_PARTY")})
    List<PartyMaster> partyMasterList;

}
