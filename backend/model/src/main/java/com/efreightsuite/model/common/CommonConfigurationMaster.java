package com.efreightsuite.model.common;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.UniqueConstraint;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;


@Entity
@Getter
@Setter
@Table(name = "efs_common_config_master", uniqueConstraints = {
        @UniqueConstraint(columnNames = {"code"}, name = "UK_COM_CONFIGURATION_CODE")})
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class CommonConfigurationMaster implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    @Column(name = "code", nullable = false, length = 255)
    String code;

    @Column(name = "value", nullable = false, length = 4000)
    String value;

    @Column(name = "description", length = 4000)
    String description;

}
