package com.efreightsuite.model.common;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.UniqueConstraint;

import com.efreightsuite.enumeration.LovStatus;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Table(name = "efs_common_currency_master", uniqueConstraints = {
        @UniqueConstraint(columnNames = {"currency_code"}, name = "UK_CMCURRENCY_CURRENCYCODE"),
        @UniqueConstraint(columnNames = {"currency_name"}, name = "UK_CMCURRENCY_CURRENCYNAME")})
public class CommonCurrencyMaster implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    @Column(name = "currency_code", nullable = false, length = 10)
    String currencyCode;

    @Column(name = "currency_name", nullable = false, length = 100)
    String currencyName;

    @Column(name = "decimal_point", nullable = false, length = 10)
    String decimalPoint;

    @Column(name = "status", nullable = false, length = 10)
    @Enumerated(EnumType.STRING)
    LovStatus status;

    /* Country Code */
    @ManyToOne
    @JoinColumn(name = "country_id", foreignKey = @ForeignKey(name = "FK_COM_CURRENCY_COUNTRYID"))
    CommonCountryMaster countryMaster;

    /* Country Code */
    @Column(name = "country_code", length = 10)
    String countryCode;

    /* Currency Prefix */
    @Column(name = "prefix", length = 30)
    String prefix;

    /* Currency Suffix */
    @Column(name = "suffix", length = 30)
    String suffix;

    public CommonCurrencyMaster() {
    }

    public CommonCurrencyMaster(String currencyCode, String currencyName, String decimalPoint, LovStatus status) {
        this.currencyCode = currencyCode;
        this.currencyName = currencyName;
        this.decimalPoint = decimalPoint;
        this.status = status;
    }
}
