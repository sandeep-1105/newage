package com.efreightsuite.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.UniqueConstraint;
import javax.persistence.Version;

import com.efreightsuite.enumeration.FullGroupage;
import com.efreightsuite.enumeration.ImportExport;
import com.efreightsuite.enumeration.LovStatus;
import com.efreightsuite.enumeration.TransportMode;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Table(name = "efs_service_master", uniqueConstraints = {
        @UniqueConstraint(columnNames = {"service_code"}, name = "UK_SERVICE_SERVICECODE"),
        @UniqueConstraint(columnNames = {"service_name"}, name = "UK_SERVICE_SERVICENAME")}

)
public class ServiceMaster implements Serializable {
    private static final long serialVersionUID = 1L;

    /* Auto Generation Id */
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    @Version
    @Column(name = "version_lock")
    long versionLock;

    /* Create/Last User and Data Info */
    @Embedded
    SystemTrack systemTrack;

    /* Service Code */
    @Column(name = "service_code", nullable = false, length = 10)
    String serviceCode;

    /* Service Name */
    @Column(name = "service_name", nullable = false, length = 100)
    String serviceName;

    /* Transport Mode */
    @Column(name = "transport_mode", nullable = false, length = 10)
    @Enumerated(EnumType.STRING)
    TransportMode transportMode;

    /* Import Export */
    @Column(name = "import_export", nullable = false, length = 10)
    @Enumerated(EnumType.STRING)
    ImportExport importExport;

    /* Full Groupage */
    @Column(name = "full_groupage", length = 10)
    @Enumerated(EnumType.STRING)
    FullGroupage fullGroupage;

    @ManyToOne
    @JoinColumn(name = "cost_center_id", foreignKey = @ForeignKey(name = "FK_SERVICE_COSTCENTERID"))
    CostCenterMaster costCenter;

    @Column(name = "cost_center_code", length = 30)
    String costCenterCode;

    @ManyToOne
    @JoinColumn(name = "service_type_master", foreignKey = @ForeignKey(name = "FK_SERVICE_SERVICETYPEID"))
    ServiceTypeMaster serviceType;

    /* Lov Status */
    @Column(name = "status", nullable = false, length = 10)
    @Enumerated(EnumType.STRING)
    LovStatus status;

	/*@OneToMany(cascade = CascadeType.ALL, orphanRemoval=true)
    @JoinColumn(name="row_id", referencedColumnName="id")
	@Where(clause="flex_column_master_id IN (SELECT f.id from flex_column_master f WHERE f.flex_table_name='ServiceMaster')")
	List<FlexColumnValue> flexs = new ArrayList<FlexColumnValue>();*/

    public ServiceMaster() {
    }

    public ServiceMaster(Long id) {
        this.id = id;
    }

    public ServiceMaster(Long id, LovStatus status) {
        this.id = id;
        this.status = status;
    }

    public ServiceMaster(String serviceName, String serviceCode, TransportMode transportMode, ImportExport importExport, SystemTrack systemTrack, LovStatus status) {
        this.systemTrack = systemTrack;
        this.serviceCode = serviceCode;
        this.serviceName = serviceName;
        this.transportMode = transportMode;
        this.importExport = importExport;
        this.status = status;
    }
}
