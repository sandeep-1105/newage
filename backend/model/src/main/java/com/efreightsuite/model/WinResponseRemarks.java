package com.efreightsuite.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "efs_win_response_remarks")
public class WinResponseRemarks implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /* Auto Generation Id */
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    /* win_web_id */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "win_web_id", foreignKey = @ForeignKey(name = "WIN_WEB_RESNE_ID"))
    @JsonBackReference
    WinWebConnectResponse winWebResponse;

    @Column(name = "reference_number")
    String referenceNumber;

    @Column(name = "identifier")
    String identifier;

    @Column(name = "error_code")
    String errorCode;

    @Column(name = "error_message", length = 4000)
    String errorMessage;

}
