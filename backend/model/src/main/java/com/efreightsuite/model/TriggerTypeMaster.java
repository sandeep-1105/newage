package com.efreightsuite.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.UniqueConstraint;
import javax.persistence.Version;

import com.efreightsuite.enumeration.LovStatus;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;


@Entity
@Getter
@Setter
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Table(name = "efs_trigger_type_master",
        uniqueConstraints = {@UniqueConstraint(columnNames = {"trigger_type_code"}, name = "UK_TRIGGERTYPE_TRIGTYPECODE"),
                @UniqueConstraint(columnNames = {"trigger_type_name"}, name = "UK_TRIGGERTYPE_TRIGTYPENAME")}

)
public class TriggerTypeMaster implements Serializable {


    private static final long serialVersionUID = 1L;

    /*Auto Generation Id*/
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    @Version
    @Column(name = "version_lock")
    long versionLock;

    /*Create/Last User and Date Info*/
    @Embedded
    SystemTrack systemTrack;

    /*Trigger Type Code*/
    @Column(name = "trigger_type_code", nullable = false, length = 10)
    String triggerTypeCode;

    /*Trigger Type Name*/
    @Column(name = "trigger_type_name", nullable = false, length = 100)
    String triggerTypeName;


    /*Status*/
    @Column(name = "status", nullable = false, length = 10)
    @Enumerated(EnumType.STRING)
    LovStatus status;


}
