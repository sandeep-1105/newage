package com.efreightsuite.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.persistence.TableGenerator;
import javax.persistence.Version;

import com.efreightsuite.enumeration.ForwarderDirect;
import com.efreightsuite.enumeration.YesNo;
import com.efreightsuite.util.JsonYesNoDeserializer;
import com.efreightsuite.util.JsonYesNoSerializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
@MappedSuperclass
public class DefaultChargePricingAir implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    public Long id;

    @Version
    @Column(name = "version_lock")
    public long versionLock;

    @Embedded
    public SystemTrack systemTrack;


    /* Division Id */
    @ManyToOne
    @JoinColumn(name = "division_id", foreignKey = @ForeignKey(name = "FK_DEFCHG_PRICING_DIVMASID"))
    DivisionMaster divisionMaster;


    /*Clearance*/
    @Column(name = "is_clearance", length = 5)
    @Enumerated(EnumType.STRING)
    @JsonSerialize(using = JsonYesNoSerializer.class)
    @JsonDeserialize(using = JsonYesNoDeserializer.class)
    YesNo isClearance;


    /* forwarderOrDirect*/
    @Column(name = "is_forwarder_direct", length = 15)
    @Enumerated(EnumType.STRING)
    ForwarderDirect isForwarderOrDirect;


    /* Agent Id */
    @ManyToOne
    @JoinColumn(name = "agent_id", foreignKey = @ForeignKey(name = "FK_DEFCHG_PRICING_AGTSID"))
    PartyMaster agent;


    /* ourTransport*/
    @Column(name = "is_our_transport", length = 5)
    @Enumerated(EnumType.STRING)
    @JsonSerialize(using = JsonYesNoSerializer.class)
    @JsonDeserialize(using = JsonYesNoDeserializer.class)
    YesNo isOurTransport;

    /* personalEffect*/
    @Column(name = "is_personal_effect", length = 5)
    @Enumerated(EnumType.STRING)
    @JsonSerialize(using = JsonYesNoSerializer.class)
    @JsonDeserialize(using = JsonYesNoDeserializer.class)
    YesNo isPersonalEffect;


    /* coload*/
    @Column(name = "is_coload", length = 5)
    @Enumerated(EnumType.STRING)
    @JsonSerialize(using = JsonYesNoSerializer.class)
    @JsonDeserialize(using = JsonYesNoDeserializer.class)
    YesNo isCoload;


    /* transit*/
    @Column(name = "is_transit", length = 5)
    @Enumerated(EnumType.STRING)
    @JsonSerialize(using = JsonYesNoSerializer.class)
    @JsonDeserialize(using = JsonYesNoDeserializer.class)
    YesNo isTransit;

    /* Commodity */
    @ManyToOne
    @JoinColumn(name = "commodity_id", foreignKey = @ForeignKey(name = "FK_DEFCHG_PRICING_CMDTID"))
    CommodityMaster commodityMaster;

    /* Carrier Id */
    @ManyToOne
    @JoinColumn(name = "carrier_id", foreignKey = @ForeignKey(name = "FK_DEFCHG_PRICING_CARID"))
    CarrierMaster carrierMaster;

    /*TOS Id*/
    @ManyToOne
    @JoinColumn(name = "tos_id", foreignKey = @ForeignKey(name = "FK_DEFCHG_PRICING_TOSID"))
    TosMaster tosMaster;


}
