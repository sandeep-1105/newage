package com.efreightsuite.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.UniqueConstraint;
import javax.persistence.Version;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;


@Entity
@Getter
@Setter
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Table(name = "efs_shipment_link", uniqueConstraints = {
        @UniqueConstraint(columnNames = {"consol_id", "sub_job_sequence"}, name = "UK_CONSOLLINK_SUBJOBID")}
)

public class ShipmentLink implements Serializable {
    private static final long serialVersionUID = 1L;

    /*Auto Generation Id*/
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    @Version
    @Column(name = "version_lock")
    long versionLock;

    /*Create/Last User and Data Info*/
    @Embedded
    SystemTrack systemTrack;

    @Column(name = "sub_job_sequence")
    Integer subJobSequence;

    /*Consol id*/
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "consol_id", foreignKey = @ForeignKey(name = "FK_SHIPLINK_CONSOLID"))
    @JsonBackReference
    Consol consol;

    /*Consol UID*/
    @Column(name = "consol_uid")
    String consolUid;

    /*Company Id*/
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "company_id", foreignKey = @ForeignKey(name = "FK_SHIPLINK_COMPANYID"))
    CompanyMaster company;

    /*Company Code*/
    @Column(name = "company_code", length = 10)
    String companyCode;

    /*Country Id*/
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "country_id", foreignKey = @ForeignKey(name = "FK_SHIPLINK_COUNTRYID"))
    CountryMaster country;

    /*Country Code*/
    @Column(name = "country_code", length = 10)
    String countryCode;

    /*Location Id*/
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "location_id", foreignKey = @ForeignKey(name = "FK_SHIPLINK_LOCATIONID"))
    LocationMaster location;

    /*Location Code*/
    @Column(name = "location_code", length = 10)
    String locationCode;

    /*Shipment UID*/
    @Column(name = "shipment_uid")
    String shipmentUid;

    /*Service id*/
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "service_id", foreignKey = @ForeignKey(name = "FK_SHIPLINK_SERVICEID"))
    ShipmentServiceDetail service;

    /*Service UID*/
    @Column(name = "service_uid")
    String serviceUid;


}
