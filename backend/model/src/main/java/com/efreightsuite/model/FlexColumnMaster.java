package com.efreightsuite.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.UniqueConstraint;
import javax.persistence.Version;

import com.efreightsuite.enumeration.FlexDataType;
import com.efreightsuite.enumeration.YesNo;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;


@Entity
@Getter
@Setter
@Table(name = "efs_flex_column_master", uniqueConstraints = {
        @UniqueConstraint(columnNames = {"flex_table_name", "flex_key"}, name = "UK_FLXCOLMS_TBLKEY")})
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class FlexColumnMaster implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    @Version
    @Column(name = "version_lock")
    long versionLock;

    @Embedded
    SystemTrack systemTrack;

    @Column(name = "flex_table_name")
    String flexTableName;

    @Column(name = "flex_key")
    String flexKey;

    @Column(name = "flex_label")
    String flexLabel;

    @Column(name = "flex_data_type")
    @Enumerated(EnumType.STRING)
    FlexDataType flexDataType;

    @Column(name = "is_required")
    @Enumerated(EnumType.STRING)
    YesNo isRequired;

    @Column(name = "default_value")
    String defaultValue;

    @Column(name = "drop_down_list")
    String dropDownlist;

}
