package com.efreightsuite.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.UniqueConstraint;
import javax.persistence.Version;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Getter;
import lombok.Setter;


@Entity
@Getter
@Setter
@Table(name = "efs_value_added_service", uniqueConstraints = {@UniqueConstraint(columnNames = {"value_added_name"}, name = "UK_VAS_VASNAME"),
        @UniqueConstraint(columnNames = {"value_added_code"}, name = "UK_VAS_VASCODE")
})
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class ValueAddedServices implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    @Version
    @Column(name = "version_lock")
    long versionLock;

    @Embedded
    SystemTrack systemTrack;

    @Column(name = "value_added_name", nullable = false)
    String valueAddedSevicesName;

    @Column(name = "value_added_code", nullable = false)
    String valueAddedSevicesCode;

    @OneToMany(mappedBy = "valueAddedServices", cascade = CascadeType.ALL, orphanRemoval = true)
    @JsonManagedReference
    List<ValueAddedCharge> valueAddedChargeList = new ArrayList<>();

}
