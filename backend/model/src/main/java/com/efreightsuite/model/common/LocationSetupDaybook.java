package com.efreightsuite.model.common;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.UniqueConstraint;

import com.efreightsuite.enumeration.SequenceFormat;
import com.efreightsuite.enumeration.YesNo;
import com.efreightsuite.util.JsonYesNoDeserializer;
import com.efreightsuite.util.JsonYesNoSerializer;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Getter;
import lombok.Setter;


@Entity
@Getter
@Setter
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Table(name = "efs_location_setup_daybook", uniqueConstraints = {
        @UniqueConstraint(columnNames = {"daybook_code", "location_setup_id"}, name = "UK_LOC_DAYBOOK_DAYBOOKCODE"),
        @UniqueConstraint(columnNames = {"daybook_name", "location_setup_id"}, name = "UK_LOC_DAYBOOK_DAYBOOKNAME")}

)
public class LocationSetupDaybook implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    @ManyToOne
    @JoinColumn(name = "location_setup_id", foreignKey = @ForeignKey(name = "FK_LOCSETDAYBOOK_LOCSETUPID"))
    @JsonBackReference
    LocationSetup locationSetup;

    @Column(name = "daybook_code", nullable = false, length = 10)
    String daybookCode;

    @Column(name = "daybook_name", nullable = false, length = 100)
    String daybookName;

    @Column(name = "format")
    @Enumerated(EnumType.STRING)
    SequenceFormat format;

    @Column(name = "prefix", length = 5)
    String prefix;

    @Column(name = "is_year", length = 10)
    @Enumerated(EnumType.STRING)
    @JsonSerialize(using = JsonYesNoSerializer.class)
    @JsonDeserialize(using = JsonYesNoDeserializer.class)
    YesNo isYear = YesNo.No;
    ;

    @Column(name = "is_month", length = 10)
    @Enumerated(EnumType.STRING)
    @JsonSerialize(using = JsonYesNoSerializer.class)
    @JsonDeserialize(using = JsonYesNoDeserializer.class)
    YesNo isMonth = YesNo.No;
    ;

    @Column(name = "current_sequence_value")
    Long currentSequenceValue;

    @Column(name = "suffix", length = 5)
    String suffix;

    @Column(name = "separator", length = 1)
    String separator;

    @ManyToOne
    @JoinColumn(name = "document_type_id", foreignKey = @ForeignKey(name = "FK_LOCSETDAYBOOK_DOCTYPEID"))
    CommonDocumentTypeMaster documentTypeMaster;
}
