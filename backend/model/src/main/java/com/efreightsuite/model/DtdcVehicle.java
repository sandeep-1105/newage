package com.efreightsuite.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Version;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;


@Entity
@Getter
@Setter
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Table(name = "efs_dtdc_vehicle")
public class DtdcVehicle implements Serializable {


    /**
     *
     */
    private static final long serialVersionUID = 1L;


    /* Auto Generation Id */
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    @Version
    @Column(name = "version_lock")
    long versionLock;

    /* Create/Last User and Date Info */
    @Embedded
    SystemTrack systemTrack;


    /*Location id*/
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "aes_commodity_id", foreignKey = @ForeignKey(name = "FK_VEHI_DTDC_ID"))
    @JsonBackReference
    AesCommodity aesCommodit;

    @Column(name = "vin_product_id", length = 25)
    String vinProductId;

    @Column(name = "vin_id_type", length = 25)
    String vinIdType;

    @Column(name = "vehicle_title_no", length = 15)
    String vehicleTitleNo;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "title_state_id", foreignKey = @ForeignKey(name = "FK_VEHI_DTDC_TITLE_ID"))
    StateMaster vehicleTitleState;

    @Column(name = "title_state_code", length = 100)
    String vehicleTitleStateCode;

}
