package com.efreightsuite.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.UniqueConstraint;
import javax.persistence.Version;

import com.efreightsuite.enumeration.YesNo;
import com.efreightsuite.util.JsonYesNoDeserializer;
import com.efreightsuite.util.JsonYesNoSerializer;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Getter;
import lombok.Setter;


@Entity
@Getter
@Setter
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Table(name = "efs_document_iss_restri",
        uniqueConstraints = @UniqueConstraint(name = "UK_DOC_ISS_RES_COM_LOC_SER",
                columnNames = {"company_id", "location_id", "service_id"}))
public class DocumentIssueRestriction implements Serializable {
    private static final long serialVersionUID = 1L;

    /*Auto Generation Id*/
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    /*Company Id*/
    @ManyToOne
    @JoinColumn(name = "company_id", foreignKey = @ForeignKey(name = "FK_DOC_ISSUE_RES_COMPANY_ID"))
    CompanyMaster companyMaster;

    /* Location Id */
    @ManyToOne
    @JoinColumn(name = "location_id", foreignKey = @ForeignKey(name = "FK_DOC_ISSUE_RES_LOCATION_ID"))
    LocationMaster locationMaster;

    /* Location Id */
    @ManyToOne
    @JoinColumn(name = "service_id", foreignKey = @ForeignKey(name = "FK_DOC_ISSUE_RES_SERVICE_ID"))
    ServiceMaster serviceMaster;

    /*Invoice Checking- Is Enabled*/
    @Column(name = "enable_invoice_checking", length = 5, nullable = false)
    @Enumerated(EnumType.STRING)
    @JsonSerialize(using = JsonYesNoSerializer.class)
    @JsonDeserialize(using = JsonYesNoDeserializer.class)
    YesNo enableInvoiceChecking;

    /*Issue Restriction - Is Enabled*/
    @Column(name = "enable_issue_restriction", length = 5, nullable = false)
    @Enumerated(EnumType.STRING)
    @JsonSerialize(using = JsonYesNoSerializer.class)
    @JsonDeserialize(using = JsonYesNoDeserializer.class)
    YesNo enableIssueRestriction;

    /*Credit Limit Checking - Is Enabled */
    @Column(name = "enable_credit_limit_checking", length = 5, nullable = false)
    @Enumerated(EnumType.STRING)
    @JsonSerialize(using = JsonYesNoSerializer.class)
    @JsonDeserialize(using = JsonYesNoDeserializer.class)
    YesNo enableCreditLimitChecking;
    /*
	
	@Column(name="exceptional_terms", nullable=true)
	@JsonSerialize(using = JsonStringToArrSerializer.class)
	@JsonDeserialize(using = JsonStringToArrDeSerializer.class)
	String exceptionalTerms;
	
*/

    @ManyToMany
    @JoinTable(name = "efs_document_issue_res_tos",
            joinColumns = {@JoinColumn(name = "doc_issue_res_id", foreignKey = @ForeignKey(name = "FK_DOC_ISS_RES_DOC_ID"))},
            inverseJoinColumns = {@JoinColumn(name = "tos_id", foreignKey = @ForeignKey(name = "FK_DOC_ISS_RES_TOS_ID"))},
            uniqueConstraints = {@UniqueConstraint(columnNames = {"doc_issue_res_id", "tos_id"}, name = "UK_DOC_ISS_RES_DOC_TOS")})
    List<TosMaster> tosList;

    /*Create/Last User and Data Info*/
    @Embedded
    //@JsonIgnore
            SystemTrack systemTrack;


    @Version
    @Column(name = "version_lock")
    long versionLock;

}
