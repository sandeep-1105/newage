package com.efreightsuite.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "efs_hawb_win_response")
public class HawbWinResponse implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 1L;


    /* Auto Generation Id */
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    /* win_web_id */
    @ManyToOne
    @JoinColumn(name = "win_status_id", foreignKey = @ForeignKey(name = "WIN_WEB_STATUS_ID"))
    @JsonBackReference
    WinStatusResponse winStatusResponse;

    @Column(name = "hawb_id")
    int hawbId;

    @Column(name = "hawb_number")
    String hawbNumber;

    @Column(name = "status")
    String status;

}
