package com.efreightsuite.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.UniqueConstraint;
import javax.persistence.Version;

import com.efreightsuite.enumeration.EventMasterType;
import com.efreightsuite.enumeration.LovStatus;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;


@Entity
@Getter
@Setter
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Table(name = "efs_event_master",
        uniqueConstraints = {@UniqueConstraint(columnNames = {"event_code"}, name = "UK_EVENT_EVENTCODE"),
                @UniqueConstraint(columnNames = {"event_name"}, name = "UK_EVENT_EVENTNAME")}

)
public class EventMaster implements Serializable {

    private static final long serialVersionUID = 1L;

    /*Auto Generation Id*/
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    @Version
    @Column(name = "version_lock")
    long versionLock;

    /*Create/Last User and Date Info*/
    @Embedded
    SystemTrack systemTrack;

    /*Trigger Code*/
    @Column(name = "event_code", nullable = false, length = 10)
    String eventCode;

    /*Trigger Name*/
    @Column(name = "event_name", nullable = false, length = 100)
    String eventName;

    /*Event Type*/
    @Column(name = "event_master_type", length = 30)
    @Enumerated(EnumType.STRING)
    EventMasterType eventMasterType;

    /*Country Master*/
    @ManyToOne
    @JoinColumn(name = "country_id", foreignKey = @ForeignKey(name = "FK_COUNTRY_ID"))
    CountryMaster countryMaster;

    /*Status*/
    @Column(name = "status", nullable = false, length = 10)
    @Enumerated(EnumType.STRING)
    LovStatus status;


}
