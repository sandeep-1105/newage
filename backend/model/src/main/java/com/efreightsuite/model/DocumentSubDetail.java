package com.efreightsuite.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Version;

import com.efreightsuite.enumeration.BooleanType;
import com.efreightsuite.util.JsonDateDeserializer;
import com.efreightsuite.util.JsonDateSerializer;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Getter;
import lombok.Setter;


@Entity
@Getter
@Setter
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Table(name = "efs_document_sub_detail")
public class DocumentSubDetail implements Serializable {
    private static final long serialVersionUID = 1L;

    /*Auto Generation Id*/
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    @Version
    @Column(name = "version_lock")
    long versionLock;

    /*Create/Last User and Data Info*/
    @Embedded
    SystemTrack systemTrack;

    /*Expected CFS Arrival Code*/
    @Column(name = "expected_cfs_arrival_code")
    String expectedCfsArrivalCode;

    /*IMO Temperature*/
    @Column(name = "imo_temperature")
    String imoTemperature;

    /*Unique Consignment No*/
    @Column(name = "unique_consignment_no")
    String uniqueConsignmentNo;

    /*CD No*/
    @Column(name = "cd_no")
    String cdNo;

    /*CCN No*/
    @Column(name = "ccn_no")
    String ccnNo;

    /*CCN Date*/
    @Column(name = "ccn_date")
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date ccnDate;

    /*Item No*/
    @Column(name = "item_no")
    String itemNo;

    /*LIGM No*/
    @Column(name = "ligm_no")
    String ligmNo;

    /*LIGM Date*/
    @Column(name = "ligm_date")
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date ligmDate;

    /*Cargo Movement*/
    @Column(name = "cargo_movement")
    String cargoMovement;

    /*Badge No*/
    @Column(name = "badge_no")
    String badgeNo;

    /*Tran CCN No*/
    @Column(name = "tran_ccn_no")
    String tranCcnNo;

    /*Tran CCN Date*/
    @Column(name = "tran_ccn_date")
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date tranCcnDate;

    /*Unstuff Code*/
    @Column(name = "unstuff_code")
    String unstuffCode;

    /*Unstuff Date*/
    @Column(name = "unstuff_date")
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date unstuffDate;

    /*HBL Type*/
    @Column(name = "hbl_type")
    String hblType;

    /*Demurrage Calculate Date*/
    @Column(name = "demurrage_calc_date")
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date demurrageCalcDate;

    /*Demurrage Date*/
    @Column(name = "demurrage_date")
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date demurrageDate;

    /*CC Percentage*/
    @Column(name = "cc_percentage")
    Double ccPercentage;

    /*CAF Percentage*/
    @Column(name = "caf_percentage")
    Double cafPercentage;

    /*Clearance E-Mail*/
    @Column(name = "clearance_email")
    String clearanceEmail;

    /*Actual Sale kg*/
    @Column(name = "actual_sale_kg")
    Double actualSaleKg;

    /*Declared Sale kg*/
    @Column(name = "declared_sale_kg")
    Double declaredSaleKg;

    /*Routed by Salesman Email*/
    @Column(name = "routed_by_salesman_email")
    String routedBySalesmanEmail;

    /*Release Code*/
    @Column(name = "release_code")
    String releaseCode;

    /*Release Date*/
    @Column(name = "release_date")
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date releaseDate;

    /*Imported Code*/
    @Column(name = "imported_code")
    String importedCode;

    /*Expected DO Date*/
    @Column(name = "expected_do_date")
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date expectedDoDate;

    /*Expected DOC Received Date*/
    @Column(name = "expected_doc_received_date")
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date expectedDocReceivedDate;

    /*Expected DOC send Date*/
    @Column(name = "expected_doc_send_date")
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date expectedDocSendDate;

    /*Truck Handover to Transporter Date*/
    @Column(name = "truck_handover_to_transporter")
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date truckHandoverToTransporter;

    /*Booking Received Date*/
    @Column(name = "booking_received_date")
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date bookingReceivedDate;

    /*Known - UnKnown Text*/
    @Column(name = "known_unknown_text")
    String knownUnknownText;

    /*Optional Shipping Info 1*/
    @Column(name = "optional_shipping_info_1")
    String optionalShippingInfo1;

    /*Optional Shipping Info 2*/
    @Column(name = "optional_shipping_info_2")
    String optionalShippingInfo2;

    /*AMS House No*/
    @Column(name = "ams_house_no")
    String amsHouseNo;

    /*IT No*/
    @Column(name = "it_no")
    String itNo;

    /*IT Submitted Date*/
    @Column(name = "it_submitted_date")
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date itSubmittedDate;

    /*IT Approved Port*/
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "it_approved_port_id", foreignKey = @ForeignKey(name = "FK_DOCSUBDETAIL_APPROVEPORTID"))
    PortMaster itApprovedPort;

    @Column(name = "it_approved_port")
    String itApprovedPortCode;


    /*Co-Loader booking no*/
    @Column(name = "co_loader_booking_no")
    String coLoaderBookingNo;

    /*Shiprite Carrier id*/
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "shiprite_carrier_id", foreignKey = @ForeignKey(name = "FK_DOCSUBDETAIL_SHIPCARRIEID"))
    CarrierMaster shipriteCarrier;

    /*Shiprite Carrier Name*/
    @Column(name = "shiprite_carrier_name")
    String shipriteCarrierName;

    /*Shiprite Carrier SCAC*/
    @Column(name = "shiprite_carrier_scac")
    String shipriteCarrierScac;

    /*Shiprite Final Rate*/
    @Column(name = "shiprite_final_rate")
    Double shipriteFinalRate;

    /*Shiprite Special Note*/
    @Column(name = "shiprite_special_note")
    String shipriteSpecialNote;

    /*Shiprite Total Charge*/
    @Column(name = "shiprite_total_charge")
    Double shipriteTotalCharge;

    /*Shiprite FSC*/
    @Column(name = "shiprite_fsc")
    Double shipriteFsc;

    /*Shiprite Extra Charge*/
    @Column(name = "shiprite_extra_charge")
    Double shipriteExtraCharge;

    /*Shiprite ASC Name*/
    @Column(name = "shiprite_asc_name")
    String shipriteAscName;

    /*Shiprite Pickup Address Line 1*/
    @Column(name = "shiprite_pickup_address_line_1", length = 255)
    String shipritePickupAddress1;

    /*Shiprite Pickup Address Line 2*/
    @Column(name = "shiprite_pickup_address_line_2", length = 255)
    String shipritePickupAddress2;

    /*Shiprite Pickup Address Line 3*/
    @Column(name = "shiprite_pickup_address_line_3", length = 255)
    String shipritePickupAddress3;

    /*EP Copy Received Date*/
    @Column(name = "ep_copy_received_date")
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date ep_copy_received_date;

    /*Is Customs Cleared By us*/
    @Column(name = "is_customs_cleared_by_us", length = 5)
    @Enumerated(EnumType.STRING)
    BooleanType isCustomsClearedByUs;

    /*EP Copy Dispatch Date*/
    @Column(name = "ep_copy_dispatch_date")
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date ep_copy_dispatch_date;

    /*DUCR No*/
    @Column(name = "ducr_no")
    String ducrNo;

    /*Is All Prepaid*/
    @Column(name = "is_all_prepaid", length = 5)
    @Enumerated(EnumType.STRING)
    BooleanType isAllPrepaid;

    /*Is Brokerage Hold*/
    @Column(name = "is_brokerage_hold", length = 5)
    @Enumerated(EnumType.STRING)
    BooleanType isBrokerageHold;

    /*Brokerage Hold Note*/
    @Column(name = "brokerage_hold_note")
    String brokerageHoldNote;

    /*Is FSC Hold*/
    @Column(name = "is_fsc_hold", length = 5)
    @Enumerated(EnumType.STRING)
    BooleanType isFscHold;

    /*FSC Hold Note*/
    @Column(name = "fsc_hold_note")
    String fscHoldNote;

    /*WWA Booking No*/
    @Column(name = "wwa_booking_no")
    String wwaBookingNo;

    /*WWA Customer Control Code*/
    @Column(name = "wwa_customer_control_code")
    String wwaCustomerControlCode;


}
