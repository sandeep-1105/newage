package com.efreightsuite.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.UniqueConstraint;
import javax.persistence.Version;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Getter;
import lombok.Setter;


@Entity
@Setter
@Getter
@Table(name = "efs_party_company_associate", uniqueConstraints = {
        @UniqueConstraint(columnNames = {"party_id", "company_id", "division_id"}, name = "UK_PARTYCOMPDIV_PARTYCOMPDIV")}
)
public class PartyCompanyAssociate implements Serializable {
    private static final long serialVersionUID = 1L;

    /* Auto Generation Id */
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    @Version
    @Column(name = "version_lock")
    long versionLock;

    /* Party id */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "party_id", foreignKey = @ForeignKey(name = "FK_PARTYASSCOMP_PARTYID"))
    @JsonBackReference
    PartyMaster partyMaster;

    /* Company Id */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "company_id", foreignKey = @ForeignKey(name = "FK_PARTYASSCOMP_COMPID"))
    CompanyMaster companyMaster;

    /*Company Code*/
    @Column(name = "company_code", length = 20)
    String companyCode;

    /* Division id */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "division_id", foreignKey = @ForeignKey(name = "FK_PARTYASSCOMP_DIVISIONID"))
    DivisionMaster divisionMaster;

    /*Division Code*/
    @Column(name = "division_code", length = 20)
    String divisionCode;

    public PartyMaster getPartyMaster() {

        if (this.partyMaster == null)
            return null;

        PartyMaster tm = new PartyMaster();
        tm.setId(partyMaster.getId());
        tm.setPartyCode(partyMaster.getPartyCode());
        tm.setPartyName(partyMaster.getPartyName());
        return tm;
    }
}
