package com.efreightsuite.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Version;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;


@Entity
@Getter
@Setter
@Table(name = "efs_service_tax_chr_map")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class ServiceChargeTaxCategoryMapping implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    @Version
    @Column(name = "version_lock")
    long versionLock;

    /* Create/Last User and Data Info */
    @Embedded
    SystemTrack systemTrack;

    @ManyToOne
    @JoinColumn(name = "service_id", nullable = false, foreignKey = @ForeignKey(name = "FK_SERVTAXCHAM_SERVIID"))
    ServiceMaster serviceMaster;

    @Column(name = "service_code", nullable = false, length = 30)
    String serviceCode;

    @ManyToOne
    @JoinColumn(name = "charge_id", nullable = false, foreignKey = @ForeignKey(name = "FK_SERVTAXCHAM_CHARFID"))
    ChargeMaster chargeMaster;

    @Column(name = "charge_code", nullable = false, length = 30)
    String chargeCode;

    @ManyToOne
    @JoinColumn(name = "service_tax_category_id", nullable = false, foreignKey = @ForeignKey(name = "FK_SERVTAXCHAM_TAXCATID"))
    ServiceTaxCategoryMaster serviceTaxCategoryMaster;

    @Column(name = "service_tax_category_code", nullable = false, length = 30)
    String serviceTaxCategoryCode;

}
