package com.efreightsuite.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.UniqueConstraint;
import javax.persistence.Version;

import com.efreightsuite.enumeration.LovStatus;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;


@Entity
@Getter
@Setter
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Table(name = "efs_hs_code_master",
        uniqueConstraints = {@UniqueConstraint(columnNames = {"hs_code"}, name = "UK_COMMODITY_HSCODE"),
                @UniqueConstraint(columnNames = {"hs_name"}, name = "UK_COMMODITY_HSNAME")}
)
public class CommodityMaster implements Serializable {
    private static final long serialVersionUID = 1L;

    /*Auto Generation Id*/
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    @Version
    @Column(name = "version_lock")
    long versionLock;

    /*Create/Last User and Data Info*/
    @Embedded
    //@JsonIgnore
            SystemTrack systemTrack;

    /*HS Code*/
    @Column(name = "hs_code", nullable = false, length = 10)
    String hsCode;

    /*HS Name*/
    @Column(name = "hs_name", nullable = false, length = 500)
    String hsName;

    /*Description*/
    @Column(name = "description", length = 4000)
    String description;

    /*Lov Status*/
    @Column(name = "status", nullable = false, length = 10)
    @Enumerated(EnumType.STRING)
    LovStatus status;


}

