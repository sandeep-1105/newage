package com.efreightsuite.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Version;

import com.efreightsuite.enumeration.EdiResponseType;
import com.efreightsuite.enumeration.EdiStatusType;
import com.efreightsuite.util.JsonDateDeserializer;
import com.efreightsuite.util.JsonDateSerializer;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;


@Entity
@Getter
@Setter
@ToString
@Table(name = "efs_edi_response_status")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class EdiResponseStatus implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    @Version
    @Column(name = "version_lock")
    long versionLock;

    @Column(name = "master_number", nullable = false, length = 30)
    String masterNumber;

    /* Lov Status */
    @Column(name = "edi_type", nullable = false, length = 10)
    @Enumerated(EnumType.STRING)
    EdiResponseType ediType;

    /* Lov Status */
    @Column(name = "edi_status_type", nullable = false, length = 10)
    @Enumerated(EnumType.STRING)
    EdiStatusType ediStatusType;

    @Column(name = "status_name", nullable = false, length = 100)
    String statusName;

    @Column(name = "status_date", nullable = false)
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date statusDate;

    @Column(name = "carrier_code", length = 100)
    String carrierCode;

    @Column(name = "flight_number", length = 100)
    String flightNumber;

    /* Shipper id */
    @ManyToOne
    @JoinColumn(name = "consol_id", foreignKey = @ForeignKey(name = "FK_EDIRESP_CONSOLID"))
    Consol consol;

    public EdiResponseStatus(Long id, String masterNumber, EdiResponseType ediType, EdiStatusType ediStatusType,
                             String statusName, Date statusDate, String carrierCode, String flightNumber) {
        super();
        this.id = id;
        this.masterNumber = masterNumber;
        this.ediType = ediType;
        this.ediStatusType = ediStatusType;
        this.statusName = statusName;
        this.statusDate = statusDate;
        this.carrierCode = carrierCode;
        this.flightNumber = flightNumber;
    }

    public EdiResponseStatus() {
    }


}
