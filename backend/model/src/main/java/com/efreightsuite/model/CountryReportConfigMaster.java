package com.efreightsuite.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.UniqueConstraint;
import javax.persistence.Version;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Getter;
import lombok.Setter;


@Entity
@Table(name = "efs_country_report_config", uniqueConstraints = {
        @UniqueConstraint(columnNames = {"report_name", "country_id"}, name = "UK_PAGECOUNTRY_KEY")})
@Setter
@Getter
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class CountryReportConfigMaster implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /* Auto Generation Id */
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    @Column(name = "report_name")
    String reportName;

    @ManyToOne
    @JoinColumn(name = "country_id", foreignKey = @ForeignKey(name = "FK_COUNTRY"))
    CountryMaster countryMaster;

    @OneToMany(mappedBy = "pageCountryMaster", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.EAGER)
    @JsonManagedReference
    List<PageMaster> pageMasterList;

    @Column(name = "copies", nullable = false)
    Long copies;

    /* Create/Last User and Date Info */
    @Embedded
    SystemTrack systemTrack;

    @Version
    @Column(name = "version_lock")
    long versionLock;

}
