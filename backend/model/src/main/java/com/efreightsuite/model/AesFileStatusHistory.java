package com.efreightsuite.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

import com.efreightsuite.enumeration.MQBoundType;
import com.efreightsuite.util.JsonDateDeserializer;
import com.efreightsuite.util.JsonDateSerializer;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Type;


@Entity
@Getter
@Setter
@Table(name = "efs_aes_file_status_history")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class AesFileStatusHistory implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /**
     *
     */

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    /* Created Date */
    @Column(name = "create_date", nullable = false)
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date createDate;

    /* AirlineEdi */
    @ManyToOne
    @JoinColumn(name = "aes_file_id", foreignKey = @ForeignKey(name = "FK_AES_FILE_HIS_STATUS_EDI_ID"))
    AesFile aesFile;

    String company_code;
    String branch_code;
    String location_code;
    String user_id;
    String segment_code;
    String job_uid;
    String subjob_uid;
    String job_no;
    String subjob_no;
    String batch_no;
    String sl_no;
    String message_type;
    Date acr_date;
    String acr_time;
    Date location_acr_date;
    String location_acr_time;
    String customs_response_date;
    String house_no;
    String response_code;
    String response_description;
    String disposition_code;
    String severity_level;
    String itn_no;
    String status;
    String remarks;

    @Column(name = "bound_type")
    @Enumerated(EnumType.STRING)
    MQBoundType boundType;

    @Column(name = "message")
    @Lob
    @Type(type = "org.hibernate.type.MaterializedClobType")
    String message;


}
