package com.efreightsuite.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;
import javax.persistence.Version;

import com.efreightsuite.enumeration.EdiMessagingFor;
import com.efreightsuite.enumeration.LovStatus;
import com.efreightsuite.enumeration.TransportMode;
import com.efreightsuite.enumeration.YesNo;
import com.efreightsuite.util.JsonYesNoDeserializer;
import com.efreightsuite.util.JsonYesNoSerializer;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Getter;
import lombok.Setter;


@Entity
@Getter
@Setter
@Table(name = "efs_carrier_master", uniqueConstraints = {
        @UniqueConstraint(columnNames = {"carrier_code"}, name = "UK_CARRIER_CARRIERCODE"),
        @UniqueConstraint(columnNames = {"carrier_name"}, name = "UK_CARRIER_CARRIERNAME"),
        @UniqueConstraint(columnNames = {"carrier_no"}, name = "UK_CARRIER_CARRIERNO"),
        @UniqueConstraint(columnNames = {"scac_code"}, name = "UK_CARRIER_SCACCODE"),
        @UniqueConstraint(columnNames = {"iata_code"}, name = "UK_CARRIER_IATACODE")
})
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class CarrierMaster implements Serializable {
    private static final long serialVersionUID = 1L;

    /* Auto Generation Id */
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    @Version
    @Column(name = "version_lock")
    long versionLock;

    /* Create/Last User and Data Info */
    @Embedded
    SystemTrack systemTrack;

    /* Carrier Code */
    @Column(name = "carrier_code", nullable = false, length = 10)
    String carrierCode;

    /* Carrier Name */
    @Column(name = "carrier_name", nullable = false, length = 100)
    String carrierName;

    /* Transport Mode */
    @Column(name = "transport_mode", nullable = false, length = 10)
    @Enumerated(EnumType.STRING)
    TransportMode transportMode;

    /* Carrier Number */
    @Column(name = "carrier_no", length = 10)
    String carrierNo;

    /* SCAC Code only used sea */
    @Column(name = "scac_code", length = 10)
    String scacCode;

    /* IATA Code only used air */
    @Column(name = "iata_code", length = 10)
    String iataCode;

    /* Available for Pricing Slab (Only for Air Carriers) */
    @Column(name = "is_plus_300", length = 10)
    @Enumerated(EnumType.STRING)
    @JsonSerialize(using = JsonYesNoSerializer.class)
    @JsonDeserialize(using = JsonYesNoDeserializer.class)
    YesNo isPlus300 = YesNo.No;

    @Column(name = "messaging_type", length = 10)
    @Enumerated(EnumType.STRING)
    EdiMessagingFor messagingType;


    /* Carrier LOGO */
    @Column(name = "image")
    @Lob
    byte[] image;

    /* Encoded Image for showing in ui */
    /* Note : this is not a column */
    @Getter
    @Setter
    @Transient
    String encodedImage;


    /* Lov Status */
    @Column(name = "status", nullable = false, length = 10)
    @Enumerated(EnumType.STRING)
    LovStatus status;


    @OneToMany(mappedBy = "carrierMaster", cascade = CascadeType.ALL, orphanRemoval = true)
    @JsonManagedReference
    List<CarrierEdiMapping> ediList = new ArrayList<>();

    @OneToMany(mappedBy = "carrierMaster", cascade = CascadeType.ALL, orphanRemoval = true)
    @JsonManagedReference
    List<CarrierAddressMapping> addressList = new ArrayList<>();


}
