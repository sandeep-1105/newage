package com.efreightsuite.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.UniqueConstraint;
import javax.persistence.Version;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;


@Entity
@Getter
@Setter
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Table(name = "efs_time_zone_master",
        uniqueConstraints = {@UniqueConstraint(columnNames = {"display_timezone"}, name = "UK_TIMEZONE_DISPLAY"),
                @UniqueConstraint(columnNames = {"actual_timezone"}, name = "UK_TIMEZONE_ACTUAL")}

)
public class TimeZoneMaster implements Serializable {
    private static final long serialVersionUID = 1L;

    /*Auto Generation Id*/
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    @Version
    @Column(name = "version_lock")
    long versionLock;

    /* Create/Last User and Data Info */
    @Embedded
    SystemTrack systemTrack;

    @Column(name = "display_timezone", nullable = false, length = 100)
    String displayTimeZone;

    @Column(name = "actual_timezone", nullable = false, length = 100)
    String actualTimeZone;

}
