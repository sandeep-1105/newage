package com.efreightsuite.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;
import javax.persistence.Version;

import com.efreightsuite.enumeration.LovStatus;
import com.efreightsuite.enumeration.TermCode;
import com.efreightsuite.enumeration.YesNo;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;


@Entity
@Getter
@Setter
@Table(name = "efs_party_account_master", uniqueConstraints = {
        @UniqueConstraint(columnNames = {"party_id", "location_id", "account_master_id", "currency_id"}, name = "UK_PARTY_ACCOUNT")
})
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class PartyAccountMaster implements Serializable {
    private static final long serialVersionUID = 1L;

    /*Auto Generation Id*/
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    @Version
    @Column(name = "version_lock")
    long versionLock;

    /*Create/Last User and Data Info*/
    @Embedded
    SystemTrack systemTrack;

    /*Party id*/
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "party_id", foreignKey = @ForeignKey(name = "FK_PARTYACCOUNT_PARTYID"))
    @JsonBackReference
    PartyMaster partyMaster;

    @Transient
    PartyMaster tmpPartyMaster;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "account_master_id", foreignKey = @ForeignKey(name = "FK_PARTYACCOUNT_ACCOUNTID"))
    GeneralLedgerAccount accountMaster;

    /*Account Code*/
    @Column(name = "account_code", nullable = false, length = 30)
    String accountCode;

    /*Company Id*/
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "company_id", foreignKey = @ForeignKey(name = "FK_PARTYACCOUNT_COMPANYID"))
    CompanyMaster companyMaster;

    @Column(name = "company_code", length = 100)
    String companyCode;

    /*Country id*/
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "country_id", foreignKey = @ForeignKey(name = "FK_PARTYACCOUNT_COUNTRYID"))
    CountryMaster countryMaster;

    /*Country Code*/
    @Column(name = "country_code", length = 10)
    String countryCode;

    /*Location id*/
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "location_id", foreignKey = @ForeignKey(name = "FK_PARTYACCOUNT_LOCATIONID"))
    LocationMaster locationMaster;

    @Column(name = "location_code", length = 100)
    String locationCode;

    /*Currency id*/
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "currency_id", foreignKey = @ForeignKey(name = "FK_PARTYACCOUNT_CURRENCYID"))
    CurrencyMaster currencyMaster;

    /*Currency Code*/
    @Column(name = "currency_code", length = 10)
    String currencyCode;


    /*Location id*/
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "bill_party_account_id", foreignKey = @ForeignKey(name = "FK_PARTYACC_BILLPARTYACC"))
    PartyAccountMaster billingAccountName;

    /*Location id*/
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "bill_party_id", foreignKey = @ForeignKey(name = "FK_PARTYACC_BILLINGPARTY"))
    PartyMaster billingSubLedgerCode;

    @Column(name = "bill_party_code", length = 100)
    String billingSubLedger;

    /*Is Tax Exempted*/
    @Column(name = "is_tax_exempted", length = 10)
    @Enumerated(EnumType.STRING)
    YesNo isTaxExempted;

    /*Term Code*/
    @Column(name = "term_code")
    @Enumerated(EnumType.STRING)
    TermCode termCode;

    /*Lov Status*/
    @Column(name = "status", nullable = false, length = 10)
    @Enumerated(EnumType.STRING)
    LovStatus status;


    public PartyMaster getPartyMaster() {

        if (this.partyMaster == null)
            return null;

        PartyMaster tm = new PartyMaster();
        tm.setId(partyMaster.getId());
        tm.setPartyCode(partyMaster.getPartyCode());
        tm.setPartyName(partyMaster.getPartyName());
        tm.setStatus(partyMaster.getStatus());
        return tm;
    }
}
