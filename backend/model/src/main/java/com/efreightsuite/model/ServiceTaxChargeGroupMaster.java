package com.efreightsuite.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.UniqueConstraint;
import javax.persistence.Version;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;


@Entity
@Table(name = "efs_srvc_tax_chrg_grp_mstr", uniqueConstraints = {@UniqueConstraint(
        columnNames = {"service_master_id", "charge_master_id", "category_master_id"}, name = "UK_SVC_CHRG_CAT")})

@Setter
@Getter
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class ServiceTaxChargeGroupMaster implements Serializable {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 1L;

    /* Auto Generation Id */
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    /* note */
    @Column(name = "note", length = 2000)
    String note;


    /* Create/Last User and Date Info */
    @Embedded
    SystemTrack systemTrack;

    @Version
    @Column(name = "version_lock")
    long versionLock;

    /* service_master */
    @ManyToOne
    @JoinColumn(name = "service_master_id", foreignKey = @ForeignKey(name = "FK_STCG_SRVC"))
    ServiceMaster serviceMaster;


    /* charge_master */
    @ManyToOne
    @JoinColumn(name = "charge_master_id", foreignKey = @ForeignKey(name = "FK_STCG_CHRG"))
    ChargeMaster chargeMaster;
    /* category_master */
    @ManyToOne
    @JoinColumn(name = "category_master_id", foreignKey = @ForeignKey(name = "FK_STCG_CATG"))
    CategoryMaster categoryMaster;


}
