package com.efreightsuite.model.common;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.UniqueConstraint;

import com.efreightsuite.enumeration.LovStatus;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;


@Entity
@Getter
@Setter
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Table(name = "efs_common_region_master", uniqueConstraints = {
        @UniqueConstraint(columnNames = {"region_code"}, name = "UK_CMREGION_REGIONCODE"),
        @UniqueConstraint(columnNames = {"region_name"}, name = "UK_CMREGION_REGIONNAME")}

)
public class CommonRegionMaster implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /**
     *
     */

	/* Auto Generation Id */
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    /* Region Code */
    @Column(name = "region_code", nullable = false, length = 10)
    String regionCode;

    /* Region Name */
    @Column(name = "region_name", nullable = false, length = 100)
    String regionName;

    /* Lov Status */
    @Column(name = "status", nullable = false, length = 10)
    @Enumerated(EnumType.STRING)
    LovStatus status;

    public CommonRegionMaster() {
    }

    public CommonRegionMaster(CommonRegionMaster regionMaster) {
        this.id = regionMaster.getId();
        this.regionName = regionMaster.getRegionName();
        this.regionCode = regionMaster.getRegionCode();
        this.status = regionMaster.getStatus();
    }

    public CommonRegionMaster(String regionCode, String regionName, LovStatus status) {
        this.regionCode = regionCode;
        this.regionName = regionName;
        this.status = status;
    }
}
