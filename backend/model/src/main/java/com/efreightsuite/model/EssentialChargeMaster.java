package com.efreightsuite.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.UniqueConstraint;
import javax.persistence.Version;

import com.efreightsuite.enumeration.CRN;
import com.efreightsuite.enumeration.PPCC;
import com.efreightsuite.enumeration.WhoRouted;
import com.efreightsuite.enumeration.YesNo;
import com.efreightsuite.util.JsonYesNoDeserializer;
import com.efreightsuite.util.JsonYesNoSerializer;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Getter;
import lombok.Setter;


@Entity
@Getter
@Setter
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Table(name = "efs_essential_charge_master", uniqueConstraints = {
        @UniqueConstraint(columnNames = {"service_id", "charge_id", "crn", "origin_id", "destination__id", "tos_id",
                "ppcc", "routed", "hazardous"}, name = "UK_ESSENTIAL_CHARGE_MASTER")})
public class EssentialChargeMaster implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    @Version
    @Column(name = "version_lock")
    long versionLock;

    /* Create/Last User and Data Info */
    @Embedded
    SystemTrack systemTrack;

    @ManyToOne
    @JoinColumn(name = "company_id", nullable = false, foreignKey = @ForeignKey(name = "FK_ESSENTIALCM_COMPID"))
    CompanyMaster companyMaster;

    @Column(name = "company_code", nullable = false, length = 30)
    String companyCode;

    @ManyToOne
    @JoinColumn(name = "location_id", nullable = false, foreignKey = @ForeignKey(name = "FK_ESSENTIALCM_LOCID"))
    LocationMaster locationMaster;

    @Column(name = "location_code", nullable = false, length = 30)
    String locationCode;

    @ManyToOne
    @JoinColumn(name = "country_id", nullable = false, foreignKey = @ForeignKey(name = "FK_ESSENTIALCM_COUNTID"))
    CountryMaster countryMaster;

    @Column(name = "country_code", nullable = false, length = 30)
    String countryCode;

    /* Charge */
    @ManyToOne
    @JoinColumn(name = "charge_id", foreignKey = @ForeignKey(name = "FK_ESSENTIALCM_CHARGEID"))
    ChargeMaster chargeMaster;

    /* Service */
    @ManyToOne
    @JoinColumn(name = "service_id", foreignKey = @ForeignKey(name = "FK_ESSENTIALCM_SERVICEID"))
    ServiceMaster serviceMaster;

    @ManyToOne
    @JoinColumn(name = "origin_id", foreignKey = @ForeignKey(name = "FK_ESSENTIALCM_ORIGINID"))
    PortMaster origin;

    @Column(name = "origin_code", length = 30)
    String originPortCode;

    @ManyToOne
    @JoinColumn(name = "destination__id", foreignKey = @ForeignKey(name = "FK_ESSENTIALCM_DESTID"))
    PortMaster destination;

    @Column(name = "destination_code", length = 30)
    String destinationCode;

    /* TOS */
    @ManyToOne
    @JoinColumn(name = "tos_id", foreignKey = @ForeignKey(name = "FK_ESSENTIALCM_TOSID"))
    TosMaster tosMaster;

    /* TOS Code */
    @Column(name = "tos_code")
    String tosCode;

    /* Cost/Revenue */
    @Column(name = "crn")
    @Enumerated(EnumType.STRING)
    CRN crn;

    /* Prepaid / Collect */
    @Column(name = "ppcc", length = 10)
    @Enumerated(EnumType.STRING)
    PPCC ppcc;

    /* Who Routed */
    @Column(name = "routed", length = 10)
    @Enumerated(EnumType.STRING)
    WhoRouted routed;

    /* Hazardous */
    @Column(name = "hazardous", length = 10)
    @Enumerated(EnumType.STRING)
    @JsonSerialize(using = JsonYesNoSerializer.class)
    @JsonDeserialize(using = JsonYesNoDeserializer.class)
    YesNo hazardous;

}
