package com.efreightsuite.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.UniqueConstraint;
import javax.persistence.Version;

import com.efreightsuite.enumeration.LovStatus;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;


@Entity
@Getter
@Setter
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Table(name = "efs_company_master",
        uniqueConstraints = {@UniqueConstraint(columnNames = {"company_code"}, name = "UK_COMPANY_COMPANYCODE"),
                @UniqueConstraint(columnNames = {"company_name"}, name = "UK_COMPANY_COMPANYNAME")}
)
public class CompanyMaster implements Serializable {
    private static final long serialVersionUID = 1L;

    /*Auto Generation Id*/
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    @Version
    @Column(name = "version_lock")
    long versionLock;

    /*Create/Last User and Data Info*/
    @Embedded
    SystemTrack systemTrack;

    /*Company Code*/
    @Column(name = "company_code", nullable = false, length = 10)
    String companyCode;

    /*Company Name*/
    @Column(name = "company_name", nullable = false, length = 100)
    String companyName;

    /* Lov Status */
    @Column(name = "status", nullable = false, length = 10)
    @Enumerated(EnumType.STRING)
    LovStatus status = LovStatus.Active;

    public CompanyMaster() {
    }

    public CompanyMaster(Long id) {
        this.id = id;
    }

    public CompanyMaster(Long id, String companyCode, String companyName) {
        this.id = id;
        this.companyCode = companyCode;
        this.companyName = companyName;
    }

    public CompanyMaster(String companyName, String companyCode, LovStatus status, SystemTrack systemTrack) {
        this.companyName = companyName;
        this.companyCode = companyCode;
        this.status = status;
        this.systemTrack = systemTrack;
    }

    public static CompanyMaster createProbe() {

        CompanyMaster companyMaster = new CompanyMaster();
        companyMaster.setStatus(null);

        return companyMaster;
    }

}
