package com.efreightsuite.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

import com.efreightsuite.enumeration.ProbationStatus;
import com.efreightsuite.util.JsonDateDeserializer;
import com.efreightsuite.util.JsonDateSerializer;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Getter;
import lombok.Setter;


@Entity
@Getter
@Setter
@Table(name = "efs_employee_profess_detail")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class EmployeeProfessionalDetail implements Serializable {
    private static final long serialVersionUID = 1L;

    /* Auto Generation Id */
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    /* Date of Joining */
    @Column(name = "date_of_joining")
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date dateOfJoining;

    /* Probation Status */
    @Column(name = "joining_status", length = 15)
    @Enumerated(EnumType.STRING)
    ProbationStatus probationStatus;

    /* Probation Status */
    @Column(name = "probation_period")
    Long probationPeriod;

    /* National ID */
    @Column(name = "national_id", length = 100)
    String nationalId;

    /* Transfer Date */
    @Column(name = "transfer_date")
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date transferDate;

    /* Transfer Date */
    @Column(name = "employment_end_date")
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date employmentEndDate;

    /* Department Master */
    @ManyToOne
    @JoinColumn(name = "department_id", foreignKey = @ForeignKey(name = "FK_EMPLOYEE_DEPTID"))
    DepartmentMaster departmentMaster;

    /* Employee - Department Head */
    @ManyToOne
    @JoinColumn(name = "department_head_id", foreignKey = @ForeignKey(name = "FK_EMPLOYEE_DEPT_HEAD_ID"))
    EmployeeMaster departmentHead;

    /* Employee - Reporting To */
    @ManyToOne
    @JoinColumn(name = "reporting_to_id", foreignKey = @ForeignKey(name = "FK_EMPLOYEE_REPORTING_TO_ID"))
    EmployeeMaster reportingTo;

    /* Designation Start Date */
    @Column(name = "designation_start_date")
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date designationStartDate;

    /* Payroll Employee ID */
    @Column(name = "payroll_emp_id", length = 100)
    String payrollEmployeeId;

    /* Designation Master */
    @ManyToOne
    @JoinColumn(name = "designation_id", foreignKey = @ForeignKey(name = "FK_EMPLOYEE_DESIGNATION_ID"))
    DesignationMaster designationMaster;

    /* Designation Qualifier */
    @Column(name = "designation_qualifier", length = 100)
    String designationQualifier;

    /* Job Description */
    @Column(name = "job_description", length = 4000)
    String jobDescription;

    @ManyToOne
    @JoinColumn(name = "grade_id", foreignKey = @ForeignKey(name = "FK_GRADE_ID"))
    GradeMaster gradeMaster;
}
