package com.efreightsuite.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Version;

import com.efreightsuite.enumeration.ForwarderDirect;
import com.efreightsuite.enumeration.PPCC;
import com.efreightsuite.enumeration.WhoRouted;
import com.efreightsuite.enumeration.YesNo;
import com.efreightsuite.util.JsonDateDeserializer;
import com.efreightsuite.util.JsonDateSerializer;
import com.efreightsuite.util.JsonYesNoDeserializer;
import com.efreightsuite.util.JsonYesNoSerializer;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Getter;
import lombok.Setter;


@Entity
@Table(name = "efs_default_charge")
@Getter
@Setter
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class DefaultChargeMaster implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /* Auto Generation Id */
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    @Version
    @Column(name = "version_lock")
    long versionLock;

    /* Create/Last User and Date Info */
    @Embedded
    SystemTrack systemTrack;

    /* Service */
    @ManyToOne
    @JoinColumn(name = "service_master_id", foreignKey = @ForeignKey(name = "FK_DEFCHG_SVCMASID"), nullable = false)
    ServiceMaster serviceMaster;

    @Column(name = "service_code")
    String serviceCode;


    /* Charge Id */
    @ManyToOne
    @JoinColumn(name = "charge_master_id", foreignKey = @ForeignKey(name = "FK_DEFCHG_CHGMASID"), nullable = false)
    ChargeMaster chargeMaster;

    @Column(name = "charge_code", nullable = false)
    String chargeCode;

    @Column(name = "charge_desc")
    String chargeDescription;

    /* Currency */
    @ManyToOne
    @JoinColumn(name = "currency_id", foreignKey = @ForeignKey(name = "FK_DEFCHG_CURMASID"), nullable = false)
    CurrencyMaster currencyMaster;

    /*  Currency Code */
    @Column(name = "currency_code", nullable = false)
    String currencyCode;


    /* Unit id */
    @ManyToOne
    @JoinColumn(name = "unit_id", foreignKey = @ForeignKey(name = "FK_DEFCHG_UNTMASID"), nullable = false)
    UnitMaster unitMaster;


    /*  Unit Code */
    @Column(name = "unit_code", nullable = false)
    String unitCode;

    /*Amount  per unit*/
    @Column(name = "amount_per_unit", nullable = false)
    Double amountPerUnit;

    /*Min Amount*/
    @Column(name = "min_amount")
    Double minAmount;

    @Column(name = "cost_in_minimum")
    Double costInMinimum;

    @Column(name = "cost_in_amount")
    Double costInAmount;


    /* Division Id */
    @ManyToOne
    @JoinColumn(name = "division_id", foreignKey = @ForeignKey(name = "FK_DEFCHG_DIVMASID"))
    DivisionMaster divisionMaster;

	
/*	Country id
    @ManyToOne
	@JoinColumn(name="country_id", foreignKey = @ForeignKey(name = "FK_DEFCHG_ORGCONMASID"))
	CountryMaster originCountry;*/


    /* Origin */
    @ManyToOne
    @JoinColumn(name = "origin_id", foreignKey = @ForeignKey(name = "FK_DEFCHG_ORGMASID"))
    PortMaster origin;
	
	/*Country id
	@ManyToOne
	@JoinColumn(name="country_id", foreignKey = @ForeignKey(name = "FK_DEFCHG_DSTCONMASID"))
	CountryMaster destinationCountry;*/


    /* destination */
    @ManyToOne
    @JoinColumn(name = "destination_id", foreignKey = @ForeignKey(name = "FK_DEFCHG_DSTMASID"))
    PortMaster destination;

    /* VIA Route */
    @ManyToOne
    @JoinColumn(name = "transit_port_id", foreignKey = @ForeignKey(name = "FK_DEFCHG_TSTMASID"))
    PortMaster transitPort;

    /*TOS Id*/
    @ManyToOne
    @JoinColumn(name = "tos_id", foreignKey = @ForeignKey(name = "FK_DEFCHG_TOSID"))
    TosMaster tosMaster;

    /* Prepaid / Collect */
    @Column(name = "ppcc")
    @Enumerated(EnumType.STRING)
    PPCC ppcc;

    /* Who Routed */
    @Column(name = "who_routed")
    @Enumerated(EnumType.STRING)
    WhoRouted whoRouted;


    /* valid_from Date */
    @Column(name = "valid_from")
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date validFrom;

    /* valid_to */
    @Column(name = "valid_to")
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date validTo;


    /* Shipper */
    @ManyToOne
    @JoinColumn(name = "shipper_id", foreignKey = @ForeignKey(name = "FK_DEFCHG_SHPID"))
    PartyMaster shipper;


    /* Shipper */
    @ManyToOne
    @JoinColumn(name = "consignee_id", foreignKey = @ForeignKey(name = "FK_DEFCHG_CONSID"))
    PartyMaster consignee;


    /* imco */
    @Column(name = "is_imco")
    @Enumerated(EnumType.STRING)
    @JsonSerialize(using = JsonYesNoSerializer.class)
    @JsonDeserialize(using = JsonYesNoDeserializer.class)
    YesNo isImco;


    /*Clearance*/
    @Column(name = "is_clearance", length = 5)
    @Enumerated(EnumType.STRING)
    @JsonSerialize(using = JsonYesNoSerializer.class)
    @JsonDeserialize(using = JsonYesNoDeserializer.class)
    YesNo isClearance;


    /* forwarderOrDirect*/
    @Column(name = "is_forwarder_direct", length = 15)
    @Enumerated(EnumType.STRING)
    ForwarderDirect isForwarderOrDirect;


    /* Agent Id */
    @ManyToOne
    @JoinColumn(name = "agent_id", foreignKey = @ForeignKey(name = "FK_DEFCHG_AGTSID"))
    PartyMaster agent;


    /* ourTransport*/
    @Column(name = "is_our_transport", length = 5)
    @Enumerated(EnumType.STRING)
    @JsonSerialize(using = JsonYesNoSerializer.class)
    @JsonDeserialize(using = JsonYesNoDeserializer.class)
    YesNo isOurTransport;

    /* personalEffect*/
    @Column(name = "is_personal_effect", length = 5)
    @Enumerated(EnumType.STRING)
    @JsonSerialize(using = JsonYesNoSerializer.class)
    @JsonDeserialize(using = JsonYesNoDeserializer.class)
    YesNo isPersonalEffect;


    /* coload*/
    @Column(name = "is_coload", length = 5)
    @Enumerated(EnumType.STRING)
    @JsonSerialize(using = JsonYesNoSerializer.class)
    @JsonDeserialize(using = JsonYesNoDeserializer.class)
    YesNo isCoload;


    /* transit*/
    @Column(name = "is_transit", length = 5)
    @Enumerated(EnumType.STRING)
    @JsonSerialize(using = JsonYesNoSerializer.class)
    @JsonDeserialize(using = JsonYesNoDeserializer.class)
    YesNo isTransit;

    /* Commodity */
    @ManyToOne
    @JoinColumn(name = "commodity_id", foreignKey = @ForeignKey(name = "FK_DEFCHG_CMDTID"))
    CommodityMaster commodityMaster;

    /* Carrier Id */
    @ManyToOne
    @JoinColumn(name = "carrier_id", foreignKey = @ForeignKey(name = "FK_DEFCHG_CARID"))
    CarrierMaster carrierMaster;


}
