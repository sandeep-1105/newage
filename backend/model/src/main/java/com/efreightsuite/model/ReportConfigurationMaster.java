package com.efreightsuite.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.UniqueConstraint;
import javax.persistence.Version;

import com.efreightsuite.enumeration.ReportConfig;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;


@Entity
@Getter
@Setter
@Table(name = "efs_report_config_master", uniqueConstraints = {
        @UniqueConstraint(columnNames = {"report_key", "location_id"}, name = "UK_REPORT_CONF_KEY")})
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class ReportConfigurationMaster implements Serializable {
    private static final long serialVersionUID = 1L;

    /* Auto Generation Id */
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    @Version
    @Column(name = "version_lock")
    long versionLock;

    /* Create/Last User and Date Info */
    @Embedded
    SystemTrack systemTrack;

    @ManyToOne
    @JoinColumn(name = "location_id", foreignKey = @ForeignKey(name = "FK_REPORT_CONF_LOCID"))
    LocationMaster location;

    @Column(name = "location_code", length = 50)
    String locationCode;

    /* Report Key - REPORTNAME_KEYNAME */
    @Column(name = "report_key", nullable = false, length = 250)
    @Enumerated(EnumType.STRING)
    ReportConfig reportKey;

    /* Remark */
    @Column(name = "report_value", length = 4000)
    String reportValue;


}
