package com.efreightsuite.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.UniqueConstraint;
import javax.persistence.Version;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;


@Entity
@Getter
@Setter
@Table(name = "efs_agent_port_master", uniqueConstraints = {@UniqueConstraint(columnNames = {"service_master_id", "party_master_id", "port_master_id"}, name = "UK_SVC_PORT_AGENTID")}
)
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class AgentPortMaster implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /*Auto Generation Id*/
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    @Version
    @Column(name = "version_lock")
    long versionLock;

    /*Create/Last User and Data Info*/
    @Embedded
    SystemTrack systemTrack;

    /* Service */
    @ManyToOne
    @JoinColumn(name = "service_master_id", foreignKey = @ForeignKey(name = "FK_AGETNPORT_SERVICEID"), nullable = false)
    ServiceMaster serviceMaster;

    /* Service Code */
    @Column(name = "service_code", nullable = false)
    String serviceCode;

    @ManyToOne
    @JoinColumn(name = "party_master_id", foreignKey = @ForeignKey(name = "FK_AGETNPORT_AGENTID"), nullable = false)
    PartyMaster agent;


    @ManyToOne
    @JoinColumn(name = "port_master_id", foreignKey = @ForeignKey(name = "FK_AGETNPORT_PORTID"), nullable = false)
    PortMaster port;

    @ManyToOne
    @JoinColumn(name = "country_id", foreignKey = @ForeignKey(name = "FK_AGETNPORT_COUNTRYID"))
    CountryMaster countryMaster;

    @ManyToOne
    @JoinColumn(name = "location_id", foreignKey = @ForeignKey(name = "FK_AGETNPORT_LOCID"))
    LocationMaster locationMaster;


}


