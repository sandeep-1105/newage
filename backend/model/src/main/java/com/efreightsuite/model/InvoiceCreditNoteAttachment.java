package com.efreightsuite.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Version;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;


@Entity
@Getter
@Setter
@Table(name = "efs_invoice_cdt_note_attach")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class InvoiceCreditNoteAttachment implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    @Version
    @Column(name = "version_lock")
    long versionLock;

    @Embedded
    SystemTrack systemTrack;

    /* Invoice id */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "invoice_credit_note_id", foreignKey = @ForeignKey(name = "FK_INV_CRE_ATTC_INVOICEID"), nullable = false)
    @JsonBackReference
    InvoiceCreditNote invoiceCreditNote;

    /* Reference Number */
    @Column(name = "ref_no", length = 100, nullable = false)
    String refNo;

    /* File Name */
    @Column(name = "file_name", length = 100, nullable = false)
    String fileName;

    /* File Content Type */
    @Column(name = "file_content_type", length = 100, nullable = false)
    String fileContentType;

    /* File */
    @Column(name = "file_attached", nullable = false)
    @Lob
    byte[] file;

}
