package com.efreightsuite.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import com.efreightsuite.util.JsonDateDeserializer;
import com.efreightsuite.util.JsonDateSerializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Getter;
import lombok.Setter;

@Embeddable
@Getter
@Setter
public class SystemTrackManual implements Serializable {
    private static final long serialVersionUID = 1L;

    /*Created User Name */
    @Column(name = "create_user", length = 255, nullable = false)
    String createUser;

    /*Created Date */
    @Column(name = "create_date", nullable = false)
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date createDate;

    /*Last Updated User Name */
    @Column(name = "last_updated_user", length = 255, nullable = false)
    String lastUpdatedUser;

    /*Last Updated Date */
    @Column(name = "last_updated_date", nullable = false)
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date lastUpdatedDate;

    public SystemTrackManual() {
    }

    public SystemTrackManual(String createUser, Date createDate, String lastUpdatedUser, Date lastUpdatedDate) {
        this.createUser = createUser;
        this.createDate = createDate;
        this.lastUpdatedUser = lastUpdatedUser;
        this.lastUpdatedDate = lastUpdatedDate;
    }
}
