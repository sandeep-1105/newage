package com.efreightsuite.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Version;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;


@Entity
@Table(name = "efs_invoice_crn_receipt")
@Getter
@Setter
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class InvoiceCreditNoteReceipt implements Serializable {
    private static final long serialVersionUID = 1L;

    /* Auto Generation Id */
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    @Version
    @Column(name = "version_lock")
    long versionLock;

    /* Create/Last User and Data Info */
    @Embedded
    SystemTrack systemTrack;

    /* Invoice id */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "invoice_credit_note_id", foreignKey = @ForeignKey(name = "FK_INV_REC_INVOICEID"))
    @JsonBackReference
    InvoiceCreditNote invoiceCreditNote;

    /* Daybook id */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "daybook_id", foreignKey = @ForeignKey(name = "FK_INV_REC_DAYBOOKID"), nullable = false)
    DaybookMaster DaybookMaster;

    /* Daybook code */
    @Column(name = "daybook_code", nullable = false, length = 100)
    String daybookCode;

    /* Document number*/
    @Column(name = "document_number")
    String documentNumber;


    /* Currency */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "currency_id", foreignKey = @ForeignKey(name = "FK_INV_REC_CURRENCYID"))
    CurrencyMaster currencyMaster;

    @Column(name = "currency_code", length = 100)
    String currencyCode;

    /* Amount */
    @Column(name = "amount")
    Double amount;
}

