package com.efreightsuite.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Version;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;


@Entity
@Getter
@Setter
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Table(name = "efs_invoice_credit_note_tax")
public class InvoiceCreditNoteTax implements Serializable {
    private static final long serialVersionUID = 1L;

    /* Auto Generation Id */
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    /* version lock */
    @Version
    @Column(name = "version_lock")
    long versionLock;

    /* Create/Last User and Date Info */
    @Embedded
    SystemTrack systemTrack;


    /* tax code */
    @Column(name = "tax_code", nullable = false, length = 30)
    String taxCode;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "service_tax_percentage_id", foreignKey = @ForeignKey(name = "FK_INV_SER_TAX_ID"), nullable = false)
    ServiceTaxPercentage serviceTaxPercentage;

    /* amount */
    @Column(name = "amount")
    Double amount;


    /* invoiceCreditNoteDetail id */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "inv_crn_detail_id", foreignKey = @ForeignKey(name = "FK_INV_CRED_DETAILS_TAX_INVID"), nullable = false)
    @JsonBackReference
    InvoiceCreditNoteDetail invoiceCreditNoteDetail;


}
