package com.efreightsuite.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.UniqueConstraint;
import javax.persistence.Version;

import com.efreightsuite.enumeration.LovStatus;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;


@Entity
@Getter
@Setter
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Table(name = "efs_buyer_consol", uniqueConstraints = {
        @UniqueConstraint(columnNames = {"bc_code"}, name = "UK_BUYERS_CONSOLD_BCCODE"),
        @UniqueConstraint(columnNames = {"bc_name"}, name = "UK_BUYERS_CONSOLD_BCNAME")})
public class BuyerConsolidationMaster implements Serializable {
    private static final long serialVersionUID = 1L;

    /* Auto Generation Id */
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    @Version
    @Column(name = "version_lock")
    long versionLock;

    /* Create/Last User and Date Info */
    @Embedded
    SystemTrack systemTrack;

    /* BC Code */
    @Column(name = "bc_code", nullable = false, length = 100)
    String bcCode;

    /* BC Name */
    @Column(name = "bc_name", nullable = false, length = 100)
    String bcName;

    @Column(name = "ref1", length = 200)
    String ref1;

    @Column(name = "ref2", length = 200)
    String ref2;

    @Column(name = "ref3", length = 200)
    String ref3;

    @Column(name = "ref4", length = 200)
    String ref4;

    @Column(name = "ref5", length = 200)
    String ref5;

    /* Lov Status */
    @Column(name = "status", nullable = false, length = 10)
    @Enumerated(EnumType.STRING)
    LovStatus status;


}
