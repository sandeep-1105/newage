package com.efreightsuite.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Version;

import com.efreightsuite.enumeration.Status;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

/**
 * @author Devendrachary M
 */


@Entity
@Getter
@Setter
@Table(name = "efs_party_email_msg_map")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class PartyEmailMessageMapping implements Serializable {
    private static final long serialVersionUID = 1L;

    /* Auto Generation Id */
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    @Version
    @Column(name = "version_lock")
    long versionLock;

    /* Create/Last User and Data Info */
    @Embedded
    SystemTrack systemTrack;

    /* email mapping id */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "email_mapping_id", foreignKey = @ForeignKey(name = "FK_PARTYEMAILMSG_PAREMAMAPID"))
    @JsonBackReference
    PartyEmailMapping partyEmailMapping;

    /* message id */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "message_id", foreignKey = @ForeignKey(name = "FK_PARTYEMAILMSG_ORIGINID"))
    AutoMailMaster autoMailMaster;

    @Column(name = "message_code", length = 100)
    String autoMailMasterCode;

    /* message group id */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "message_group_id", foreignKey = @ForeignKey(name = "FK_PARTYEMAILMSG_AUTMAGRPID"))
    AutoMailGroupMaster autoMailGroupMaster;

    @Column(name = "message_group_code", length = 100)
    String autoMailGroupMasterCode;

    /* Status (enable/disable)  */
    @Column(name = "status")
    @Enumerated(EnumType.STRING)
    Status autoMailStatus;


}
