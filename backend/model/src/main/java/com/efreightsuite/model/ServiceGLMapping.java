package com.efreightsuite.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Version;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Type;


@Entity
@Getter
@Setter
@Table(name = "efs_service_account_mapping")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class ServiceGLMapping implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    @Version
    @Column(name = "version_lock")
    long versionLock;

    /* Create/Last User and Data Info */
    @Embedded
    SystemTrack systemTrack;

    @ManyToOne
    @JoinColumn(name = "service_id", nullable = false, foreignKey = @ForeignKey(name = "FK_SERACCMAPP_SERVIID"))
    ServiceMaster serviceMaster;

    @Column(name = "service_code", nullable = false, length = 30)
    String serviceCode;

    @ManyToOne
    @JoinColumn(name = "account_id", nullable = false, foreignKey = @ForeignKey(name = "FK_SERACCMAPP_ACCOUNT"))
    GeneralLedgerAccount account;

    @Column(name = "note", nullable = false)
    @Lob
    @Type(type = "org.hibernate.type.MaterializedClobType")
    String note;

}
