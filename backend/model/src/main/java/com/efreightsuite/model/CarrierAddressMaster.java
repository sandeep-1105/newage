package com.efreightsuite.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.UniqueConstraint;
import javax.persistence.Version;

import lombok.Getter;
import lombok.Setter;


@Entity
@Getter
@Setter
@Table(name = "efs_carrier_address_master",
        uniqueConstraints = {@UniqueConstraint(columnNames = {"carrier_id", "location_id"}, name = "UK_CARRIERADD_CARRIER_LOCATION")}
)
public class CarrierAddressMaster implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    @Version
    @Column(name = "version_lock")
    long versionLock;

    /*Create/Last User and Data Info*/
    @Embedded
    SystemTrack systemTrack;

    /*Carrier id*/
    @ManyToOne
    @JoinColumn(name = "carrier_id", foreignKey = @ForeignKey(name = "FK_CARRIER_CARRIERID"))
    CarrierMaster carrierMaster;

    /*Location id*/
    @ManyToOne
    @JoinColumn(name = "location_id", foreignKey = @ForeignKey(name = "FK_CARRIER_LOCATIONID"))
    LocationMaster locationMaster;

    /*Carrier Agent id*/
    @ManyToOne
    @JoinColumn(name = "carrier_agent_id", foreignKey = @ForeignKey(name = "FK_CARRIER_CARRIERAGENTID"))
    PartyMaster partyMaster;

    /*Our Account Number*/
    @Column(name = "our_account_no", length = 40)
    String ourAccountNumber;

}
