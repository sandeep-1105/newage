package com.efreightsuite.model.common;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;

import com.efreightsuite.enumeration.BooleanType;
import com.efreightsuite.enumeration.DateLogic;
import com.efreightsuite.enumeration.Measurement;
import com.efreightsuite.enumeration.SalutationType;
import com.efreightsuite.enumeration.YesNo;
import com.efreightsuite.enumeration.common.LocationSetupSection;
import com.efreightsuite.model.SystemTrackManual;
import com.efreightsuite.util.JsonBooleanTypeDeSerializer;
import com.efreightsuite.util.JsonBooleanTypeSerializer;
import com.efreightsuite.util.JsonDateMonthDeserializer;
import com.efreightsuite.util.JsonDateMonthSerializer;
import com.efreightsuite.util.JsonMeasurementDeserializer;
import com.efreightsuite.util.JsonMeasurementSerializer;
import com.efreightsuite.util.JsonYesNoDeserializer;
import com.efreightsuite.util.JsonYesNoSerializer;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;


@Entity
@Getter
@Setter
@Table(name = "efs_location_setup", uniqueConstraints = {
        @UniqueConstraint(columnNames = {"company_reg_name"}, name = "UK_LOCATION_SETUP_REG_NAME"),
        @UniqueConstraint(columnNames = {"saas_id"}, name = "UK_LOCATION_SETUP_SAAS_ID")},
        indexes = {
                @Index(name = "IDX_COMPANY_REGISTERED_NAME", columnList = "company_reg_name")
        }
)
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@ToString
public class LocationSetup implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    @Embedded
    SystemTrackManual systemTrackManual;

    @Column(name = "current_section", length = 15)
    @Enumerated(EnumType.STRING)
    LocationSetupSection currentSection;

	/* Login tab starts */

    @Column(name = "position")
    String position;

    @Column(name = "user_name")
    String userName;

    @Column(name = "password")
    String password;

    @OneToMany(mappedBy = "locationSetup", cascade = CascadeType.ALL, orphanRemoval = true)
    @JsonManagedReference
    List<LocationSetupService> serviceList = new ArrayList<>();

	/* Login tab ends */

    /* company tab starts */
    @Column(name = "salutation", length = 10)
    @Enumerated(EnumType.STRING)
    SalutationType salutation;

    @Column(name = "first_name")
    String firstName;

    @Column(name = "last_name")
    String lastName;

    @Column(name = "email", length = 500)
    String email;

    @ManyToOne
    @JoinColumn(name = "country_id", foreignKey = @ForeignKey(name = "FK_LOCATION_SETUP_COUNTRY"))
    CommonCountryMaster countryMaster;

    @Column(name = "country_code", length = 10)
    String countryCode;

    @Column(name = "dial_code", length = 10)
    String dialCode;

    @Column(name = "phone", length = 100)
    String phone;

    @Column(name = "mobile", length = 100)
    String mobile;

    @Column(name = "company_reg_name", length = 100)
    String companyRegisteredName;

    @Column(name = "address_line_1", length = 255)
    String addressLine1;

    @Column(name = "address_line_2", length = 255)
    String addressLine2;

    @Column(name = "address_line_3", length = 255)
    String addressLine3;

    @Column(name = "area", length = 255)
    String area;

    @ManyToOne
    @JoinColumn(name = "state_id", foreignKey = @ForeignKey(name = "FK_LOCATION_SETUP_STATE"))
    CommonStateMaster stateMaster;

    @Column(name = "state_code", length = 10)
    String stateCode;

    @ManyToOne
    @JoinColumn(name = "city_id", foreignKey = @ForeignKey(name = "FK_LOCATION_SETUP_CITY"))
    CommonCityMaster cityMaster;

    @Column(name = "city_code", length = 10)
    String cityCode;

    @Column(name = "zip_code", length = 10)
    String zipCode;

    @ManyToOne
    @JoinColumn(name = "region_id", foreignKey = @ForeignKey(name = "FK_LOCATION_SETUP_REGION"))
    CommonRegionMaster regionMaster;

    @Column(name = "region_code", length = 10)
    String regionCode;

    @Column(name = "is_head_office", length = 5)
    @Enumerated(EnumType.STRING)
    @JsonSerialize(using = JsonBooleanTypeSerializer.class)
    @JsonDeserialize(using = JsonBooleanTypeDeSerializer.class)
    BooleanType isHeadOffice = BooleanType.FALSE;

    @Column(name = "logo")
    @Lob
    byte[] logo;

    /* encoded logo for showing in ui */
    /* Note : this is not a column */
    @Getter
    @Setter
    @Transient
    String encodedLogo;

	/* company tab ends */

    @Column(name = "activation_key", length = 10)
    String activationKey;

    @Column(name = "is_user_activated", length = 5)
    @Enumerated(EnumType.STRING)
    BooleanType isUserActivated = BooleanType.FALSE;

    @Transient
    String recaptchaResponse;

    @Column(name = "saas_id", length = 10)
    String saasId;

    @Column(name = "date_format", length = 30)
    String dateFormat;

    @Column(name = "time_zone", length = 100)
    String timeZone;


    @ManyToOne
    @JoinColumn(name = "timezone_id", foreignKey = @ForeignKey(name = "FK_LOCATION_SETUP_TZ_ID"))
    CommonTimeZoneMaster timeZoneMaster;

    @ManyToOne
    @JoinColumn(name = "currency_id", foreignKey = @ForeignKey(name = "FK_LOCATION_SETUP_CURR_ID"))
    CommonCurrencyMaster currencyMaster;


    @Column(name = "fin_start_date", nullable = true)
    @JsonSerialize(using = JsonDateMonthSerializer.class)
    @JsonDeserialize(using = JsonDateMonthDeserializer.class)
    Date financialStartDate;

    @Column(name = "fin_end_date", nullable = true)
    @JsonSerialize(using = JsonDateMonthSerializer.class)
    @JsonDeserialize(using = JsonDateMonthDeserializer.class)
    Date financialEndDate;


    @Column(name = "measurement", length = 10)
    @Enumerated(EnumType.STRING)
    @JsonSerialize(using = JsonMeasurementSerializer.class)
    @JsonDeserialize(using = JsonMeasurementDeserializer.class)
    Measurement measurement = Measurement.Imperial;

    @ManyToOne
    @JoinColumn(name = "language_id", foreignKey = @ForeignKey(name = "FK_LOCATION_SETUP_LANGUAGE_ID"))
    CommonLanguageMaster languageMaster;

    /* File */
    @Column(name = "customer_uploaded_file")
    @Lob
    byte[] customerUploadedFile;

    /* File */
    @Column(name = "agent_uploaded_file")
    @Lob
    byte[] agentUploadedFile;

    /* File */
    @Column(name = "agent_port_uploaded_file")
    @Lob
    byte[] agentPortUploadedFile;

    @Column(name = "quote_need_approval", length = 10)
    @Enumerated(EnumType.STRING)
    @JsonSerialize(using = JsonYesNoSerializer.class)
    @JsonDeserialize(using = JsonYesNoDeserializer.class)
    YesNo quoteNeedApproval = YesNo.No;

    @Column(name = "is_provisional_cost", length = 10)
    @Enumerated(EnumType.STRING)
    @JsonSerialize(using = JsonYesNoSerializer.class)
    @JsonDeserialize(using = JsonYesNoDeserializer.class)
    YesNo isProvisionalCost = YesNo.No;

    @Column(name = "invoice_header", length = 4000)
    String invoiceHeaderNote;

    @Column(name = "invoice_footer", length = 4000)
    String invoiceFooterNote;

    @OneToMany(mappedBy = "locationSetup", cascade = CascadeType.ALL, orphanRemoval = true)
    @JsonManagedReference
    List<LocationSetupCharge> chargeList = new ArrayList<>();

    @OneToMany(mappedBy = "locationSetup", cascade = CascadeType.ALL, orphanRemoval = true)
    @JsonManagedReference
    List<LocationSetupSequence> sequenceList = new ArrayList<>();

    @OneToMany(mappedBy = "locationSetup", cascade = CascadeType.ALL, orphanRemoval = true)
    @JsonManagedReference
    List<LocationSetupDaybook> daybookMasterList = new ArrayList<>();

    @Column(name = "import_job_date")
    @Enumerated(EnumType.STRING)
    DateLogic importJobDate;

    @Column(name = "export_job_date")
    @Enumerated(EnumType.STRING)
    DateLogic exportJobDate;

    @Column(name = "customer_file_name", length = 100)
    String customerFileName;

    @Column(name = "agent_file_name", length = 100)
    String agentFileName;

    @Column(name = "agent_port_file_Name", length = 100)
    String agentPortFileName;

    public LocationSetup() {
    }

    public LocationSetup(Long id) {
        this.id = id;
    }

    public LocationSetup(String saasId, String user, Date creationDate) {
        this.saasId = saasId;
        this.systemTrackManual = new SystemTrackManual(user, creationDate, user, creationDate);
    }

    public LocationSetup(String saasId, String companyRegisteredName, String user, Date creationDate) {
        this(saasId, user, creationDate);
        this.companyRegisteredName = companyRegisteredName;
    }

    public LocationSetup(Long id, String saasId) {
        this.id = id;
        this.saasId = saasId;
    }

    public boolean isTab(LocationSetupSection section) {
        return this.getCurrentSection() != null && this.getCurrentSection().equals(section);
    }
}
