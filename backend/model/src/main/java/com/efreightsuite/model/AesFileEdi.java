package com.efreightsuite.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Version;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;


@Entity
@Table(name = "efs_aes_file_edi")
@Getter
@Setter
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class AesFileEdi implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    @Version
    @Column(name = "version_lock")
    long versionLock;

    /* Create/Last User and Data Info */
    @Embedded
    SystemTrack systemTrack;


    /* AirlineEdi */
    @ManyToOne
    @JoinColumn(name = "aes_file_id", foreignKey = @ForeignKey(name = "FK_AIR_AES_EDI_ID"))
    AesFile aesFile;


    /* Location */
    @ManyToOne
    @JoinColumn(name = "location_id", foreignKey = @ForeignKey(name = "FK_AIR_AES_EDI_LOCATIONID"))
    LocationMaster locationMaster;

    @Column(name = "location_code", length = 30)
    String locationCode;

    /* Company */
    @ManyToOne
    @JoinColumn(name = "company_id", foreignKey = @ForeignKey(name = "FK_AIR_AES_EDI_COMPANYID"))
    CompanyMaster companyMaster;

    @Column(name = "company_code", length = 30)
    String companyCode;

    /* Country */
    @ManyToOne
    @JoinColumn(name = "country_id", foreignKey = @ForeignKey(name = "FK_AIR_AES_EDI_COUNTRYID"))
    CountryMaster countryMaster;

    @Column(name = "country_code", length = 30)
    String countryCode;

    @Column(name = "messaging_status", length = 15, nullable = true)
    // MQ Message Sent, Mq Connection Failure, In Progress, Ftp Connection
            // Failure, Win Connection Failure, Completed
            String messagingStatus;

    @Column(name = "messaging_error", length = 4000, nullable = true)
    String messagingError;

}
