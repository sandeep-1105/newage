package com.efreightsuite.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Version;

import com.efreightsuite.enumeration.AesAction;
import com.efreightsuite.enumeration.FillingType;
import com.efreightsuite.enumeration.YesNo;
import com.efreightsuite.util.JsonDateDeserializer;
import com.efreightsuite.util.JsonDateSerializer;
import com.efreightsuite.util.JsonFillingTypeDeserializer;
import com.efreightsuite.util.JsonFillingTypeSerializer;
import com.efreightsuite.util.JsonYesNoDeserializer;
import com.efreightsuite.util.JsonYesNoSerializer;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Type;


@Entity
@Table(name = "efs_aes_file")
@Getter
@Setter
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class AesFile implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    @Version
    @Column(name = "version_lock")
    long versionLock;

    /* Create/Last User and Data Info */
    @Embedded
    SystemTrack systemTrack;

    /* Shipment UID */
    @Column(name = "shipment_uid", nullable = false)
    String shipmentUid;

    /* Shipment UID */
    @Column(name = "service_uid", nullable = false)
    String serviceUid;

    /* Master UID */
    @Column(name = "master_uid")
    String masterUid;

    /* HAWB */
    @Column(name = "hawb_no", length = 100)
    String hawbNo;

    @Column(name = "iata_scac_code", length = 100)
    String iataOrScacCode;

    /* MAWB */
    @Column(name = "mawb_no", length = 30)
    String mawbNo;

    /* AES Filing Type */
    @Column(name = "aes_filing_type", nullable = false)
    @Enumerated(EnumType.STRING)
    @JsonSerialize(using = JsonFillingTypeSerializer.class)
    @JsonDeserialize(using = JsonFillingTypeDeserializer.class)
    FillingType aesFilingType;

    /* pol */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "aes_transport", foreignKey = @ForeignKey(name = "FK_AES_TRANSPORT_ID"), nullable = false)
    AesTransportMode aesTransportMode;

    @Column(name = "aes_transport_name", length = 255)
    String aesTransportModeName;

    /* AES ITN No */
    @Column(name = "aes_itn_no", length = 50)
    String aesItnNo;

    /* Lov Status */
    @Column(name = "aes_status", length = 150)
    String aesStatus;

    /* Action */
    @Column(name = "aes_action", length = 30)
    @Enumerated(EnumType.STRING)
    AesAction aesAction;

    /* Origin State */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "origin_state_id", foreignKey = @ForeignKey(name = "FK_AES_ORIGIN_STATE_ID"), nullable = false)
    AesUsStateCodeMaster originState;

    @Column(name = "origin_state_code", length = 100)
    String originStateCode;

    /* pol */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "pol", foreignKey = @ForeignKey(name = "FK_AES_POL_ID"), nullable = false)
    AesUsPortMaster pol;

    @Column(name = "pol_code", length = 100)
    String polCode;

    /* pod */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "pod", foreignKey = @ForeignKey(name = "FK_AES_POD_ID"), nullable = false)
    AesUsForeignPortMaster pod;

    @Column(name = "pod_code", length = 100)
    String podCode;

    /* Destination Country */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "destination_country_id", foreignKey = @ForeignKey(name = "FK_AES_DESTINATION_COUNTRY_ID"), nullable = false)
    CountryMaster destinationCountry;

    @Column(name = "destination_country_code", length = 100)
    String destinationCountryCode;


    /* Country Id */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "country_id", foreignKey = @ForeignKey(name = "FK_AES_COUNTRYID"))
    CountryMaster country;

    @Column(name = "country_code", length = 100)
    String countryCode;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "company_id", foreignKey = @ForeignKey(name = "FK_AES_COMPANYID"))
    CompanyMaster companyMaster;

    @Column(name = "company_code", length = 100)
    String companyCode;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "location_id", foreignKey = @ForeignKey(name = "FK_AES_LOCATIONID"))
    LocationMaster location;

    @Column(name = "location_code", length = 100)
    String locationCode;


    /* Carrier master */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "carrier_id", foreignKey = @ForeignKey(name = "FK_AES_CARRIER_ID"))
    CarrierMaster carrier;

    @Column(name = "carrier_code", length = 100)
    String carrierCode;

    /* Iata master */
    @ManyToOne
    @JoinColumn(name = "iata_id", foreignKey = @ForeignKey(name = "FK_AES_IATA_ID"))
    IataMaster iata;

    /* IATA */
    @Column(name = "iata_code", length = 30)
    String iataCode;

    /* Iata master */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "scac_id", foreignKey = @ForeignKey(name = "FK_AES_SCAC_ID"))
    ScacMaster scac;

    @Column(name = "scac_code", length = 100)
    String scacCode;

    /* ETD */
    @Column(name = "etd")
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date etd;

    /* Routed Transaction */
    @Column(name = "routed_transaction", nullable = false)
    @Enumerated(EnumType.STRING)
    @JsonSerialize(using = JsonYesNoSerializer.class)
    @JsonDeserialize(using = JsonYesNoDeserializer.class)
    YesNo routedTransaction;

    /* USPPI and Ultimate Consignee Related */
    @Column(name = "usspi_ultimate_consignee", nullable = false)
    @Enumerated(EnumType.STRING)
    @JsonSerialize(using = JsonYesNoSerializer.class)
    @JsonDeserialize(using = JsonYesNoDeserializer.class)
    YesNo usspiUltimateConsignee;

    /* Hazardous Cargo */
    @Column(name = "hazardous_cargo", nullable = false)
    @Enumerated(EnumType.STRING)
    @JsonSerialize(using = JsonYesNoSerializer.class)
    @JsonDeserialize(using = JsonYesNoDeserializer.class)
    YesNo hazardousCargo;

    /* InBondTypeMaster */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "inbond_id", foreignKey = @ForeignKey(name = "FK_AES_INBOND_ID"))
    InBondTypeMaster InbondType;

    @Column(name = "inbond_type", length = 255)
    String InbondTypeCode;

    /* Import Entry No */
    @Column(name = "import_entry_no", length = 50)
    String importEntryNo;

    /* Foreign Trade Zone */
    @Column(name = "foreign_trade_zone", length = 50)
    String foreignTradeZone;

    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "aes_usppi_id", foreignKey = @ForeignKey(name = "FK_AES_FILE_USPPI_ID"))
    AesUsppi aesUsppi;

    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "aes_ultimate_consignee_id", foreignKey = @ForeignKey(name = "FK_AES_FILE_ULTI_CONSIG_ID"))
    AesUltimateConsignee aesUltimateConsignee;

    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "aes_intermediate_consignee_id", foreignKey = @ForeignKey(name = "FK_AES_FILE_INTER_CONSIG_ID"))
    AesIntermediateConsignee aesIntermediateConsignee;

    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "aes_freight_forwarder_id", foreignKey = @ForeignKey(name = "FK_AES_FILE_FRE_FOR_ID"))
    AesFreightForwarder aesFreightForwarder;


    @OneToMany(mappedBy = "aesfile", cascade = CascadeType.ALL, orphanRemoval = true)
    @JsonManagedReference
    List<AesCommodity> aesCommodityList = new ArrayList<>();

    // TODO
    @Column(name = "status", length = 50)
    String status;

    @Column(name = "batch_no")
    String batchNo;

    @Column(name = "segment_code")
    String segmentCode;

    @Column(name = "filing_option")
    String filingOption;

    @Column(name = "aes_queue_msg")
    @Lob
    @Type(type = "org.hibernate.type.MaterializedClobType")
    String aesQueueMessage;

//	JOBNO - masterUid
//	SUBJOBNO - serviceUid
//	MASTER_NO - mawbNo
//	HOUSE_NO - hawbNo
//	BOOKING_NO - shipmentUid

    public AesFile() {
        // TODO Auto-generated constructor stub
    }

    public AesFile(Long id) {
        this.id = id;
    }

}
