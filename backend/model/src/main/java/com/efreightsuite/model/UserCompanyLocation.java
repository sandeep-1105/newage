package com.efreightsuite.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.UniqueConstraint;
import javax.persistence.Version;

import com.efreightsuite.dto.UserCompanyLocationDTO;
import com.efreightsuite.enumeration.YesNo;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Type;


@Entity
@Getter
@Setter
@Table(name = "efs_user_company_location",
        uniqueConstraints = {@UniqueConstraint(columnNames = {"location_id", "user_company_id"}, name = "UK_USERCOMLOC_USERCOMLOCID")}

)
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class UserCompanyLocation implements Serializable {
    private static final long serialVersionUID = 1L;

    /*Auto Generation Id*/
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    @Version
    @Column(name = "version_lock")
    long versionLock;

    /*Create/Last User and Data Info*/
    @Embedded
    SystemTrack systemTrack;

    /*Location id*/
    @ManyToOne
    @JoinColumn(name = "user_company_id", foreignKey = @ForeignKey(name = "FK_USERCOMLOC_USERID"))
    @JsonBackReference
    UserCompany userCompany;

    /* Default Company */
    @Column(name = "default_location", nullable = false, length = 20)
    @Enumerated(EnumType.STRING)
    YesNo yesNo;

    /*Location id*/
    @ManyToOne
    @JoinColumn(name = "location_id", foreignKey = @ForeignKey(name = "FK_USERCOMLOC_LOCATIONID"))
    LocationMaster locationMaster;

    /* Note */
    @Column(name = "note")
    @Lob
    @Type(type = "org.hibernate.type.MaterializedClobType")
    String note;

    public UserCompanyLocation(UserCompanyLocationDTO dto) {
        this.id = dto.getId();
        this.versionLock = dto.getVersionLock();
        this.systemTrack = dto.getSystemTrack();
        //this.userCompany=new UserCompany(dto.getUserCompany());
        this.locationMaster = dto.getLocationMaster();
        this.yesNo = dto.getYesNo();
        this.note = dto.getNote();
    }

    public UserCompanyLocation() {
    }

}
