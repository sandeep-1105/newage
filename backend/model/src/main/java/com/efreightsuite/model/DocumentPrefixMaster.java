package com.efreightsuite.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.UniqueConstraint;
import javax.persistence.Version;

import com.efreightsuite.enumeration.BooleanType;
import com.efreightsuite.enumeration.LovStatus;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Getter;
import lombok.Setter;


@Entity
@Setter
@Getter
@Table(name = "efs_doc_prefix_master",
        uniqueConstraints = {
                @UniqueConstraint(columnNames = {"doc_prefix_code"}, name = "UK_DOCPREFIIX_DOCPREFIXCODE"),
                @UniqueConstraint(columnNames = {"doc_prefix_name"}, name = "UK_DOCPREFIIX_DOCPREFIXNAME")

        }
)
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class DocumentPrefixMaster implements Serializable {
    private static final long serialVersionUID = 1L;

	/*Auto Generation Id*/

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    @Version
    @Column(name = "version_lock")
    long versionLock;

    /*Create/Last User and Data Info*/
    @Embedded
    SystemTrack systemTrack;

    /*Doc Prefix Code*/
    @Column(name = "doc_prefix_code", nullable = false, length = 10)
    String documentCode;

    /*Doc Prefix Name*/
    @Column(name = "doc_prefix_name", nullable = false, length = 100)
    String documentName;

    /*Doc Prefix*/
    @Column(name = "doc_prefix", length = 20)
    String docPrefix;

    /*doc_suffix*/
    @Column(name = "doc_suffix", length = 20)
    String docSuffix;

    /*Add POR to Doc*/
    @Column(name = "add_por_to_doc", length = 5)
    @Enumerated(EnumType.STRING)
    BooleanType addPOR;

    /*Add FDC to Doc*/
    @Column(name = "add_fdc_to_doc", length = 5)
    @Enumerated(EnumType.STRING)
    BooleanType addFDC;

    /*Formula*/
    @Column(name = "formula")
    String formula;

    /* Lov Status */
    @Column(name = "status", nullable = false, length = 10)
    @Enumerated(EnumType.STRING)
    LovStatus status;

    /* List of Document prefix type */
    @OneToMany(mappedBy = "documentPrefixMaster", cascade = CascadeType.ALL, orphanRemoval = true)
    @JsonManagedReference
    List<DocPrefixTypeMapping> docPrefixTypeMappingList = new ArrayList<>();

}
