package com.efreightsuite.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.UniqueConstraint;
import javax.persistence.Version;

import com.efreightsuite.enumeration.ShipmentMovement;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;


@Entity
@Getter
@Setter
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Table(name = "efs_agent_rate_req_chr", uniqueConstraints = {
        @UniqueConstraint(columnNames = {"rate_request_id", "charge_id"}, name = "UK_RATECHARGE")}

)
public class RateRequestCharge implements Serializable {
    private static final long serialVersionUID = 1L;

    /* Auto Generation Id */
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    @Version
    @Column(name = "version_lock")
    long versionLock;

    /* Create/Last User and Data Info */
    @Embedded
    SystemTrackManual systemTrackManual;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "rate_request_id", nullable = false, foreignKey = @ForeignKey(name = "FK_RATEREQCHA_RATRQID"))
    @JsonBackReference
    RateRequest rateRequest;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "charge_id", nullable = false, foreignKey = @ForeignKey(name = "FK_RATEREQCHA_CHAID"))
    ChargeMaster chargeMaster;

    @Column(name = "charge_name", length = 100)
    String chargeName;

    @Column(name = "charge_code", length = 100)
    String chargeCode;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "unit_id", nullable = false, foreignKey = @ForeignKey(name = "FK_RATEREQCHA_UNITID"))
    UnitMaster unitMaster;

    @Column(name = "unit_code", length = 100)
    String unitCode;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "currency_id", nullable = false, foreignKey = @ForeignKey(name = "FK_RATEREQCHA_CURRID"))
    CurrencyMaster currencyMaster;

    @Column(name = "currency_code", length = 100)
    String currencyCode;

    @Column(name = "buy_rate")
    Double buyRate;

    @Column(name = "buy_rate_min_cost")
    Double buyRateMinCost;


    @Column(name = "unit_slap", length = 10)
    String unitSlap;

    @Column(name = "shipment_movement", length = 20)
    @Enumerated(EnumType.STRING)
    ShipmentMovement shipmentMovement;

    @Column(name = "notes", length = 500)
    String notes;

}
