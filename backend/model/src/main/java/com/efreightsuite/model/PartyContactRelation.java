package com.efreightsuite.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Version;

import com.efreightsuite.enumeration.PersonalEvent;
import com.efreightsuite.enumeration.RelationShip;
import com.efreightsuite.enumeration.SalutationType;
import com.efreightsuite.util.JsonDateDeserializer;
import com.efreightsuite.util.JsonDateSerializer;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Getter;
import lombok.Setter;


@Entity
@Setter
@Getter
@Table(name = "efs_party_contact_relation")
public class PartyContactRelation implements Serializable {
    private static final long serialVersionUID = 1L;

    /* Auto Generation Id */
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    @Version
    @Column(name = "version_lock")
    long versionLock;

    @Embedded
    SystemTrack systemTrack;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "party_contact_id", foreignKey = @ForeignKey(name = "FK_PARTYCONTRE_PARTYID"))
    @JsonBackReference
    PartyContact partyContact;
    @Column(name = "salutation", length = 10)
    @Enumerated(EnumType.STRING)
    SalutationType salutationType;
    @Column(name = "first_name", length = 100)
    String firstName;
    @Column(name = "last_name", length = 100)
    String lastName;
    @Column(name = "relation_ship", length = 20)
    @Enumerated(EnumType.STRING)
    RelationShip relationShip;
    @Column(name = "personal_event", length = 20)
    @Enumerated(EnumType.STRING)
    PersonalEvent personalEvent;
    @Column(name = "personal_event_data")
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date personalEventDate;
    @Column(name = "note", length = 400)
    String note;

    public PartyContact getPartyContact() {

        if (this.partyContact == null)
            return null;

        PartyContact tm = new PartyContact();
        tm.setId(partyContact.getId());
        return tm;
    }
}
