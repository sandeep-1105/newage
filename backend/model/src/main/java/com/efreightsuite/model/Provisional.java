package com.efreightsuite.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Version;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Getter;
import lombok.Setter;


@Entity
@Getter
@Setter
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Table(name = "efs_provisional")
public class Provisional implements Serializable {
    private static final long serialVersionUID = 1L;

    /* Auto Generation Id */
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    @Version
    @Column(name = "version_lock")
    long versionLock;

    /* Create/Last User and Date Info */
    @Embedded
    SystemTrack systemTrack;

    /* Master UID */
    @Column(name = "master_uid")
    String masterUid;

    /* Shipment UID */
    @Column(name = "shipment_uid")
    String shipmentUid;


    /* Shipment UID */
    @Column(name = "service_uid")
    String serviceUid;

    /* serviceSName */
    @Column(name = "service_name")
    String serviceName;

    @OneToMany(mappedBy = "provisional", cascade = CascadeType.ALL, orphanRemoval = true)
    @JsonManagedReference
    List<ProvisionItem> provisionalItemList = new ArrayList<>();

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "local_currency_id", foreignKey = @ForeignKey(name = "FK_PROVISIONAL_LOCCURID"))
    CurrencyMaster localCurrency;

    @Column(name = "local_currency_code", length = 100)
    String localCurrencyCode;

    /*Total Revenue */
    @Column(name = "total_revenue")
    Double totalRevenue;

    /* Total Cost */
    @Column(name = "total_cost")
    Double totalCost;

    /* Net */
    @Column(name = "net_amount")
    Double netAmount;

}
