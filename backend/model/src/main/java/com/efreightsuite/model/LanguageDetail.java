package com.efreightsuite.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.UniqueConstraint;
import javax.persistence.Version;

import com.efreightsuite.enumeration.YesNo;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;


@Entity
@Getter
@Setter
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Table(name = "efs_language_detail", uniqueConstraints = {
        @UniqueConstraint(columnNames = {"language_id", "language_code"}, name = "UK_LANGDETAIL_CODE")})
public class LanguageDetail implements Serializable {
    private static final long serialVersionUID = 1L;

    /* Auto Generation Id */
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    @Version
    @Column(name = "version_lock")
    long versionLock;

    /* systemTrack */
    @Embedded
    SystemTrack systemTrack;

    /* Language id */
    @ManyToOne
    @JoinColumn(name = "language_id", foreignKey = @ForeignKey(name = "FK_LANGUGE_LANGUAGEID"))
    Language language;

    /* code */
    @Column(name = "language_code", nullable = false, length = 20)
    String languageCode;

    /* description */
    @Column(name = "language_desc", nullable = false, length = 1000)
    String languageDesc;

    /* Group Name */
    @Column(name = "group_name", nullable = false, length = 30)
    String groupName;

    /* QC Verify */
    @Column(name = "flagged", length = 10)
    @Enumerated(EnumType.STRING)
    YesNo flagged;

    public LanguageDetail(){}

    public LanguageDetail(Language language, String languageCode, String languageDesc) {
        this.language = language;
        this.languageCode = languageCode;
        this.languageDesc = languageDesc;
    }

    public LanguageDetail(Language language, String languageCode, String languageDesc, String groupName, YesNo flagged, SystemTrack systemTrack) {
        this(language,languageCode,languageDesc);
        this.groupName = groupName;
        this.flagged=flagged;
        this.systemTrack=systemTrack;
    }
}
