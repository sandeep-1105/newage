package com.efreightsuite.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.UniqueConstraint;
import javax.persistence.Version;

import com.efreightsuite.enumeration.LovStatus;
import com.efreightsuite.enumeration.MappingUnit;
import com.efreightsuite.enumeration.UnitCalculationType;
import com.efreightsuite.enumeration.UnitDecimals;
import com.efreightsuite.enumeration.UnitType;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;


@Entity
@Getter
@Setter
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Table(name = "efs_unit_master",
        uniqueConstraints = {
                @UniqueConstraint(columnNames = {"unit_code"}, name = "UK_UNIT_UNITCODE"),
                @UniqueConstraint(columnNames = {"unit_name"}, name = "UK_UNIT_UNITNAME")
        }
)
public class UnitMaster implements Serializable {
    private static final long serialVersionUID = 1L;

    /*Auto Generation Id*/
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    @Version
    @Column(name = "version_lock")
    long versionLock;

    /*Create/Last User and Date Info*/
    @Embedded
    SystemTrack systemTrack;

    /*Unit Code*/
    @Column(name = "unit_code", nullable = false, length = 10)
    String unitCode;

    /*Unit Name*/
    @Column(name = "unit_name", nullable = false, length = 100)
    String unitName;

    /*Unit Type*/
    @Column(name = "unit_type", nullable = false, length = 20)
    @Enumerated(EnumType.STRING)
    UnitType unitType;


    /*Mapping Unit 1*/
    @Column(name = "mapping_unit1", length = 20)
    @Enumerated(EnumType.STRING)
    MappingUnit mappingUnit1;

    /*Calculation Type 1*/
    @Column(name = "calc_type1", length = 20)
    @Enumerated(EnumType.STRING)
    UnitCalculationType calcType1;

    /*Calculation Value 1*/
    @Column(name = "calc_value1")
    Long calcValue1;

    /*Mapping Unit 2*/
    @Column(name = "mapping_unit2", length = 20)
    @Enumerated(EnumType.STRING)
    MappingUnit mappingUnit2;

    /*Calculation Type 2*/
    @Column(name = "calc_type2", length = 20)
    @Enumerated(EnumType.STRING)
    UnitCalculationType calcType2;

    /*Calculation Value 2*/
    @Column(name = "calc_value2")
    Long calcValue2;

    /*Round level*/
    @Column(name = "decimals", length = 10)
    @Enumerated(EnumType.STRING)
    UnitDecimals decimals;

    /*Lov Status*/
    @Column(name = "status", nullable = false, length = 10)
    @Enumerated(EnumType.STRING)
    LovStatus status;


} 
