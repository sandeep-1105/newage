package com.efreightsuite.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Transient;
import javax.persistence.Version;

import com.efreightsuite.enumeration.ActualChargeable;
import com.efreightsuite.enumeration.SurchargeType;
import com.efreightsuite.enumeration.YesNo;
import com.efreightsuite.util.JsonDateDeserializer;
import com.efreightsuite.util.JsonDateSerializer;
import com.efreightsuite.util.JsonYesNoDeserializer;
import com.efreightsuite.util.JsonYesNoSerializer;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.envers.Audited;
import org.hibernate.envers.RelationTargetAuditMode;


@Entity
@Getter
@Setter
@Table(name = "efs_quotation_charge")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Audited
public class QuotationCharge implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /* Auto Generation Id */
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    @Version
    @Column(name = "version_lock")
    long versionLock;

    /*Tracking informations*/
    @Embedded
    SystemTrack systemTrack;

    @Column(name = "value_added_id")
    Long valueAddedId;

    @Column(name = "pricing_port_pair_id")
    Long pricingPortPairId;

    @Column(name = "pricing_carrier_id")
    Long pricingCarrierId;

    @Column(name = "agent_rate_request_charge_id")
    Long agentRateRequestChargeId;

    @Column(name = "surcharge_type", length = 50)
    @Enumerated(EnumType.STRING)
    SurchargeType surchargeType;

    /* EnquiryDetail */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "quotation_carrier_id", foreignKey = @ForeignKey(name = "FK_QUOTECARRR_CARRID"))
    @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
    @JsonBackReference
    QuotationCarrier quotationCarrier;

    /* Charge */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "charge_id", foreignKey = @ForeignKey(name = "FK_QUOTECHAR_CHARGEID"))
    @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
    ChargeMaster chargeMaster;

    /* Charge Name */
    @Column(name = "charge_code", nullable = false, length = 20)
    String chargeCode;

    /* Charge Name */
    @Column(name = "charge_name", nullable = false, length = 100)
    String chargeName;

    /* UnitMaster */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "currency_id", foreignKey = @ForeignKey(name = "FK_QUOTECHAR_CURRENCYID"))
    @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
    CurrencyMaster currencyMaster;

    /* Charge Name */
    @Column(name = "currency_code", nullable = false, length = 20)
    String currencyCode;

    @Column(name = "roe")
    Double roe;

    /* UnitMaster */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "unit_id", foreignKey = @ForeignKey(name = "FK_QUOTECHAR_UNITID"))
    @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
    UnitMaster unitMaster;

    /* Charge Name */
    @Column(name = "unit_code", nullable = false, length = 20)
    String unitCode;

    /* Number of Units */
    @Column(name = "no_of_unit")
    String numberOfUnit;

    /* amountPerUnit */
    @Column(name = "sell_rate_amount_per_unit")
    Double sellRateAmountPerUnit;

    @Column(name = "estimated_unit")
    Double estimatedUnit;

    /* no of minAmount */
    @Column(name = "sell_rate_min_amount")
    Double sellRateMinAmount;

    @Column(name = "sell_rate_amount")
    Double sellRateAmount;

    @Column(name = "sell_rate_local_amount")
    Double sellRateLocalAmount;

    /* costPerUnit */
    @Column(name = "buy_rate_cost_per_unit")
    Double buyRateCostPerUnit;

    /* no of minimumCost */
    @Column(name = "buy_rate_min_cost")
    Double buyRateMinCost;

    @Column(name = "buy_rate_amount")
    Double buyRateCostAmount;

    @Column(name = "buy_rate_local_amount")
    Double buyRateCostLocalAmount;

    // actual_chargeable
    @Column(name = "actual_chargeable")
    @Enumerated(EnumType.STRING)
    ActualChargeable actualchargeable;

    @Column(name = "is_group", length = 10)
    @Enumerated(EnumType.STRING)
    @JsonSerialize(using = JsonYesNoSerializer.class)
    @JsonDeserialize(using = JsonYesNoDeserializer.class)
    YesNo isGroup;

    @Column(name = "groupName", length = 255)
    String groupName;

    /*revisionType*/
    @Transient
    String revisionType;

    @Transient
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date auditDate;
}
