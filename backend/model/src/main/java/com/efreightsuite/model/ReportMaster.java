package com.efreightsuite.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;
import javax.persistence.Version;

import com.efreightsuite.enumeration.ReportName;
import com.efreightsuite.enumeration.YesNo;
import com.efreightsuite.util.JsonYesNoDeserializer;
import com.efreightsuite.util.JsonYesNoSerializer;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Getter;
import lombok.Setter;


@Entity
@Getter
@Setter
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Table(name = "efs_report_master",
        uniqueConstraints = {@UniqueConstraint(columnNames = {"report_master_name"}, name = "UK_REPORT_REPORTCODE"),
                @UniqueConstraint(columnNames = {"report_master_code"}, name = "UK_REPORT_REPORTNAME"),
                @UniqueConstraint(columnNames = {"report_key"}, name = "UK_REPORT_REPORTKEY")}

)
public class ReportMaster implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;


    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    @Version
    @Column(name = "version_lock")
    long versionLock;

    /*Create/Last User and Data Info*/
    @Embedded
    //@JsonIgnore
            SystemTrack systemTrack;

    /*Report Code*/
    @Column(name = "report_master_code", nullable = false, length = 30)
    String reportMasterCode;

    /*Report Name*/
    @Column(name = "report_master_name", nullable = false, length = 100)
    String reportMasterName;

    @Column(name = "is_report_enable", length = 5)
    @Enumerated(EnumType.STRING)
    @JsonSerialize(using = JsonYesNoSerializer.class)
    @JsonDeserialize(using = JsonYesNoDeserializer.class)
    YesNo isReportEnable = YesNo.No;

    /* Freight Term */
    @Column(name = "report_key", length = 100, nullable = false)
    @Enumerated(EnumType.STRING)
    ReportName reportKey;

    /* Document Masster */
    @ManyToOne
    @JoinColumn(name = "docmaster_id", foreignKey = @ForeignKey(name = "FK_REPORT_DOCUMENT_ID"))
    DocumentMaster documentMaster;


    @Transient
    List tmpReportMasterList;

}
