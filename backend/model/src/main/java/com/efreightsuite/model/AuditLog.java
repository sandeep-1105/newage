package com.efreightsuite.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Version;

import com.efreightsuite.util.JsonDateSerializer;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Getter;
import lombok.Setter;


@Entity
@Getter
@Setter
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Table(name = "efs_audit_log")
public class AuditLog implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    @Version
    @Column(name = "version_lock")
    long versionLock;

    /*Created User Name */
    @Column(name = "user_name", length = 30)
    String userName;

    /*Created Date */
    @Column(name = "create_date")
    @JsonSerialize(using = JsonDateSerializer.class)
    Date createDate;

    /*Created Method Name */
    @Column(name = "method_name", length = 40)
    String methodName;

    /*Created Method Name */
    @Column(name = "class_name", length = 70)
    String className;

    /*Created Status Code */
    @Column(name = "status_code", length = 30)
    int statusCode;

    /*Created Http Response Status Code */
    @Column(name = "http_response_code", length = 30)
    int httpResponseCode;

    /*Last Updated User Name */
    @Column(name = "last_updated_user_name", length = 30)
    String lastUpdatedUserName;

    /*Last Updated Date */
    @Column(name = "last_updated_date")
    @JsonSerialize(using = JsonDateSerializer.class)
    Date lastUpdatedDate;

}
