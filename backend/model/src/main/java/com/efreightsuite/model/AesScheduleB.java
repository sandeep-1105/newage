package com.efreightsuite.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Version;

import com.efreightsuite.enumeration.LovStatus;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;


@Entity
@Getter
@Setter
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Table(name = "efs_aes_schedule_b")
public class AesScheduleB implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    @Version
    @Column(name = "version_lock")
    long versionLock;

    /* Create/Last User and Data Info */
    @Embedded
    SystemTrack systemTrack;

    /* Action */
    @Column(name = "schedule_code", length = 50, nullable = false)
    String scheduleCode;

    @Column(name = "schedule_name", length = 100, nullable = false)
    String scheduleName;

    @ManyToOne
    @JoinColumn(name = "firs_unit_id", foreignKey = @ForeignKey(name = "FK_AESSCHEDULE_FUNIT_ID"), nullable = false)
    UnitMaster firstUnit;

    @ManyToOne
    @JoinColumn(name = "secon_unit_id", foreignKey = @ForeignKey(name = "FK_AESSCHEDULE_SUNIT_ID"), nullable = false)
    UnitMaster secondUnit;

    /* Commodity */
    @ManyToOne
    @JoinColumn(name = "commodity_id", foreignKey = @ForeignKey(name = "FK_AESSCHEDULE_COMMODITY_ID"), nullable = false)
    CommodityMaster commodityMaster;

    /* Lov Status */
    @Column(name = "status", nullable = false, length = 10)
    @Enumerated(EnumType.STRING)
    LovStatus status;

}
