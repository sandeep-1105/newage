package com.efreightsuite.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;
import javax.persistence.Version;

import com.efreightsuite.enumeration.ActualChargeable;
import com.efreightsuite.util.JsonDateDeserializer;
import com.efreightsuite.util.JsonDateSerializer;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Getter;
import lombok.Setter;


@Entity
@Setter
@Getter
@Table(name = "efs_carrier_rate_master", uniqueConstraints = {
        @UniqueConstraint(columnNames = {"carrier_master_id", "charge_master_id", "unit_master_id", "origin_id", "destination_id"},
                name = "UK_CARRIER_RATE")
})

@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class CarrierRateMaster implements Serializable {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    @Version
    @Column(name = "version_lock")
    long versionLock;

    /* Create/Last User and Data Info */
    @Embedded
    SystemTrack systemTrack;


    /* Service */
    @ManyToOne
    @JoinColumn(name = "carrier_master_id", foreignKey = @ForeignKey(name = "FK_CARRATE_CARRIERMASID"), nullable = false)
    CarrierMaster carrierMaster;

    /* serviceName */
    @Column(name = "carrier_code", nullable = false)
    String carrierCode;

    /* chargeMaster */
    @ManyToOne
    @JoinColumn(name = "charge_master_id", foreignKey = @ForeignKey(name = "FK_CARRATE_CHARGEMASID"), nullable = false)
    ChargeMaster chargeMaster;

    /* chargeName */
    @Column(name = "charge_code", nullable = false)
    String chargeCode;

    /* unitMaster */
    @ManyToOne
    @JoinColumn(name = "unit_master_id", foreignKey = @ForeignKey(name = "FK_CARRATE_UNITMASID"), nullable = false)
    UnitMaster unitMaster;

    /* unitCode */
    @Column(name = "unit_code", nullable = false)
    String unitCode;

    // actual_chargeable
    @Column(name = "actual_chargeable")
    @Enumerated(EnumType.STRING)
    ActualChargeable actualChargeable;

    /* Origin Id */
    @ManyToOne
    @JoinColumn(name = "origin_id", foreignKey = @ForeignKey(name = "FK_CARRATE_ORIGINID"))
    PortMaster origin;

    /* originCode */
    @Column(name = "origin_code")
    String originCode;


    /* destinationCode */
    @Column(name = "destination_code")
    String destinationCode;

    /* destination Id */
    @ManyToOne
    @JoinColumn(name = "destination_id", foreignKey = @ForeignKey(name = "FK_CARRATE_DESTID"))
    PortMaster destination;

    /*First effective from Date*/
    @Column(name = "frsteff_frm_date")
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date firstFromDate;

    /* frstAmount */
    @Column(name = "frsteff_amount")
    Double firstAmount;

    /* frstAmount */
    @Column(name = "frsteff_min_amount")
    Double firstMinAmount;


    /*secondeff_frm_date*/
    @Column(name = "secondeff_frm_date")
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date secondFromDate;

    /* frstAmount */
    @Column(name = "secondeff_amount")
    Double secondAmount;

    /* frstAmount */
    @Column(name = "secondeff_min_amount")
    Double secondMinAmount;

    @Transient
    List carrierRateDataList;

}
