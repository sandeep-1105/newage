package com.efreightsuite.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.UniqueConstraint;
import javax.persistence.Version;

import com.efreightsuite.enumeration.LovStatus;
import com.efreightsuite.enumeration.ReferenceType;
import com.efreightsuite.enumeration.ReferenceTypeKey;
import com.efreightsuite.enumeration.YesNo;
import com.efreightsuite.util.JsonYesNoDeserializer;
import com.efreightsuite.util.JsonYesNoSerializer;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Getter;
import lombok.Setter;


@Entity
@Getter
@Setter
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Table(name = "efs_referencetype_master",
        uniqueConstraints = {
                @UniqueConstraint(columnNames = {"referencetype_code"}, name = "UK_REFERENCE_REFERENCECODE"),
                @UniqueConstraint(columnNames = {"referencetype_name"}, name = "UK_REFERENCE_REFERENCENAME"),
                @UniqueConstraint(columnNames = {"reference_type"}, name = "UK_REFERENCE_REFERENCETYPE")}

)
public class ReferenceTypeMaster implements Serializable {


    private static final long serialVersionUID = 1L;

    /*Auto Generation Id*/
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    @Version
    @Column(name = "version_lock")
    long versionLock;

    /*Create/Last User and Date Info*/
    @Embedded
    SystemTrack systemTrack;

    /*ReferenceType Code*/
    @Column(name = "referencetype_code", nullable = false, length = 10)
    String referenceTypeCode;

    /*ReferenceType Name*/
    @Column(name = "referencetype_name", nullable = false, length = 100)
    String referenceTypeName;

    @Column(name = "reference_type", length = 50)
    @Enumerated(EnumType.STRING)
    ReferenceType referenceType;

    @Column(name = "reference_type_key", length = 50)
    @Enumerated(EnumType.STRING)
    ReferenceTypeKey referenceTypeKey;

    /* Is Mandatory */
    @Column(name = "is_mandatory")
    @Enumerated(EnumType.STRING)
    @JsonSerialize(using = JsonYesNoSerializer.class)
    @JsonDeserialize(using = JsonYesNoDeserializer.class)
    YesNo isMandatory = YesNo.No;

    /*Display Status*/
    @Column(name = "status", nullable = false, length = 10)
    @Enumerated(EnumType.STRING)
    LovStatus status;


}
