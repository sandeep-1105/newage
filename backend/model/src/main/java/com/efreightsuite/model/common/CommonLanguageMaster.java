package com.efreightsuite.model.common;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.UniqueConstraint;

import com.efreightsuite.enumeration.YesNo;
import com.efreightsuite.util.JsonYesNoDeserializer;
import com.efreightsuite.util.JsonYesNoSerializer;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Getter;
import lombok.Setter;


@Entity
@Getter
@Setter
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Table(name = "efs_common_language_master", uniqueConstraints = {
        @UniqueConstraint(columnNames = {"language_code"}, name = "UK_CMLANG_LANGCODE"),
        @UniqueConstraint(columnNames = {"language_name"}, name = "UK_CMLANG_LANGNAME")})
public class CommonLanguageMaster implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    @Column(name = "language_code", nullable = false, length = 20, unique = true)
    String languageCode;

    @Column(name = "language_name", nullable = false, length = 20, unique = true)
    String languageName;

    @Column(name = "status", nullable = false, length = 10)
    @Enumerated(EnumType.STRING)
    @JsonSerialize(using = JsonYesNoSerializer.class)
    @JsonDeserialize(using = JsonYesNoDeserializer.class)
    YesNo status;

    public CommonLanguageMaster() {
    }

    public CommonLanguageMaster(String languageCode, String languageName, YesNo status) {
        this.languageCode = languageCode;
        this.languageName = languageName;
        this.status = status;
    }
}
