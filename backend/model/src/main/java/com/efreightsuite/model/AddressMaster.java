package com.efreightsuite.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Version;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;


@Entity
@Getter
@Setter
@Table(name = "efs_address_master")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class AddressMaster implements Serializable {
    private static final long serialVersionUID = 1L;

    /*Auto Generation Id*/
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    @Version
    @Column(name = "version_lock")
    long versionLock;

    /*Address Line 1 */
    @Column(name = "address_line_1", length = 100)
    String addressLine1;

    /*Address Line 2 */
    @Column(name = "address_line_2", length = 100)
    String addressLine2;

    /*Address Line 3 */
    @Column(name = "address_line_3", length = 100)
    String addressLine3;

    /*Address Line 4 */
    @Column(name = "address_line_4", length = 150)
    String addressLine4;

    /* Contact */
    @Column(name = "contact", length = 100)
    String contact;

    /*Phone */
    @Column(name = "phone", length = 500)
    String phone;

    /*Phone */
    @Column(name = "fax", length = 500)
    String fax;

    /*Mobile */
    @Column(name = "mobile", length = 500)
    String mobile;

    /*Email */
    @Column(name = "email", length = 500)
    String email;

    /*PO Box */
    @Column(name = "po_box", length = 20)
    String poBox;

    /*Zip Code */
    @Column(name = "zip_code", length = 10)
    String zipCode;

	/*Phone 
	@Column(name="city", length=500)
	String city;
	
	Phone 
	@Column(name="state", length=500)
	String state;*/

    /*State or Province*/
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "state", foreignKey = @ForeignKey(name = "FK_ADDR_STAT"))
    StateMaster state;

    @Column(name = "state_code", length = 100)
    String stateCode;

    /*City Id*/
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "city", foreignKey = @ForeignKey(name = "FK_ADDR_CTY"))
    CityMaster city;

    @Column(name = "city_code", length = 100)
    String cityCode;


    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "country", foreignKey = @ForeignKey(name = "FK_ADDR_CTRY"))
    CountryMaster country;

    @Column(name = "country_code", length = 100)
    String countryCode;

    public AddressMaster() {
    }

    public AddressMaster(Long id, String addressLine1, String addressLine2, String addressLine3) {
        this.id = id;
        this.addressLine1 = addressLine1;
        this.addressLine2 = addressLine2;
        this.addressLine3 = addressLine3;
    }

    public AddressMaster(String addressLine1, StateMaster state, CityMaster city) {
        this.addressLine1 = addressLine1;
        this.state = state;
        this.city = city;
    }

    public String getFullAddressInLine() {

        String fullAddress = "";

        if (addressLine1 != null && addressLine1.trim().length() != 0) {
            if (fullAddress.trim().length() != 0) {
                fullAddress = fullAddress + ", ";
            }
            fullAddress = fullAddress + addressLine1;
        }

        if (addressLine2 != null && addressLine2.trim().length() != 0) {
            if (fullAddress.trim().length() != 0) {
                fullAddress = fullAddress + ", ";
            }
            fullAddress = fullAddress + addressLine2;
        }

        if (addressLine3 != null && addressLine3.trim().length() != 0) {
            if (fullAddress.trim().length() != 0) {
                fullAddress = fullAddress + ", ";
            }
            fullAddress = fullAddress + addressLine3;
        }

        if (addressLine4 != null && addressLine4.trim().length() != 0) {
            if (fullAddress.trim().length() != 0) {
                fullAddress = fullAddress + ", ";
            }
            fullAddress = fullAddress + addressLine4;
        }

        if (city != null) {
            if (fullAddress.trim().length() != 0) {
                fullAddress = fullAddress + ", ";
            }
            fullAddress = fullAddress + city.getCityName();
        }

        if (state != null) {
            if (fullAddress.trim().length() != 0) {
                fullAddress = fullAddress + ", ";
            }
            fullAddress = fullAddress + state.getStateCode();
        }

        if (zipCode != null && zipCode.trim().length() != 0) {
            if (fullAddress.trim().length() != 0) {
                fullAddress = fullAddress + ", ";
            }
            fullAddress = fullAddress + zipCode;
        }

        if (country != null) {
            if (fullAddress.trim().length() != 0) {
                fullAddress = fullAddress + ", ";
            }
            fullAddress = fullAddress + country.getCountryName();
        }

        return fullAddress;
    }

}
