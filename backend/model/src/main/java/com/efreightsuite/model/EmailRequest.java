package com.efreightsuite.model;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Version;

import com.efreightsuite.enumeration.EmailStatus;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Type;


@Entity
@Getter
@Setter
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Table(name = "efs_email_request")
public class EmailRequest implements Serializable {

    private static final long serialVersionUID = 1L;

    /* Auto Generation Id */
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    @Version
    @Column(name = "version_lock")
    long versionLock;

    /* Create/Last User and Data Info */
    @Embedded
    SystemTrackManual manualSystemTrack;

    /* From Email Address */
    @Column(name = "email_from", nullable = false, length = 100)
    String fromEmailId;

    /* From Email Address */
    @Column(name = "email_from_password", nullable = false, length = 100)
    String fromEmailIdPassword;

    /* List of email ID of receivers */
    @Column(name = "email_to", nullable = false, length = 500)
    String toEmailIdList;

    /* List of email ID of cc */
    @Column(name = "email_cc", length = 500)
    String ccEmailIdList;

    /* List of email ID of BCC */
    @Column(name = "email_bcc", length = 500)
    String bccEmailIdList;

    /* subject of email */
    @Column(name = "email_subject", nullable = false, length = 255)
    String subject;

    /* email body content to send */
    @Column(name = "email_body", nullable = false)
    @Lob
    @Type(type = "org.hibernate.type.MaterializedClobType")
    String emailBody;

    /* List of Attachment for this mail */
    @OneToMany(mappedBy = "emailRequest", cascade = CascadeType.ALL)
    @JsonBackReference
    Set<EmailRequestAttachment> attachmentList = new HashSet<>();

    /* email type to send */
    @Column(name = "email_type", nullable = false, length = 30)
    String emailType;

    /* Email status */
    @Column(name = "email_status", nullable = false, length = 20)
    @Enumerated(EnumType.STRING)
    EmailStatus emailStatus;

    /* errorDes */
    @Column(name = "error_desc", length = 255)
    String errorDesc;

    @Column(name = "server_port")
    Integer port;

    //E-mail Settings
    @Column(name = "smtp_server_address", length = 255)
    String smtpServerAddress;

}
