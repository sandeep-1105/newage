package com.efreightsuite.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.UniqueConstraint;

import com.efreightsuite.dto.FeatureDTO;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;


@Entity
@Getter
@Setter
@Table(name = "efs_role_has_feature", uniqueConstraints = {
        @UniqueConstraint(columnNames = {"role_id", "feature_id"}, name = "UK_ROLEHASFEATURE_UNQ")})
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})

public class RoleHasFeature implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    @ManyToOne
    @JoinColumn(name = "role_id", foreignKey = @ForeignKey(name = "FK_ROLEHASFEATURE_ROLE"))
    @JsonBackReference
    Role role;

    @ManyToOne
    @JoinColumn(name = "feature_id", foreignKey = @ForeignKey(name = "FK_ROLEHASFEATURE_FEATURE"))
    @JsonBackReference
    Feature feature;

    @Column(name = "val_list", length = 255)
    String valList;

    @Column(name = "val_create", length = 255)
    String valCreate;

    @Column(name = "val_modify", length = 255)
    String valModify;

    @Column(name = "val_view", length = 255)
    String valView;

    @Column(name = "val_delete", length = 255)
    String valDelete;

    @Column(name = "val_download", length = 255)
    String valDownload;

    public RoleHasFeature() {
    }

    public RoleHasFeature(FeatureDTO dto) {
        this.id = dto.getId();
        //this.role=dto.get
        this.valList = dto.getValList();
        this.valCreate = dto.getValCreate();
        this.valModify = dto.getValModify();
        this.valView = dto.getValView();
        this.valDelete = dto.getValDelete();
        this.valDownload = dto.getValDownload();
    }

}
