package com.efreightsuite.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Version;

import com.efreightsuite.enumeration.OnlineOffline;
import com.efreightsuite.util.JsonDateDeserializer;
import com.efreightsuite.util.JsonDateSerializer;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Getter;
import lombok.Setter;


@Entity
@Getter
@Setter
@Table(name = "efs_iata_rate_master")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class IataRateMaster implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "autogen")
    @TableGenerator(name = "autogen", initialValue = 0, allocationSize = 1)
    Long id;

    @Version
    @Column(name = "version_lock")
    long versionLock;

    @Embedded
    SystemTrack systemTrack;

    @ManyToOne
    @JoinColumn(name = "company_id", nullable = false, foreignKey = @ForeignKey(name = "FK_IATARATE_COMPID"))
    CompanyMaster companyMaster;

    @Column(name = "company_code", nullable = false, length = 30)
    String companyCode;

    @ManyToOne
    @JoinColumn(name = "location_id", nullable = false, foreignKey = @ForeignKey(name = "FK_IATARATE_LOCTID"))
    LocationMaster locationMaster;

    @Column(name = "location_code", nullable = false, length = 30)
    String locationCode;

    @ManyToOne
    @JoinColumn(name = "country_id", nullable = false, foreignKey = @ForeignKey(name = "FK_IATARATE_COUNTID"))
    CountryMaster countryMaster;

    @Column(name = "country_code", nullable = false, length = 30)
    String countryCode;

    @ManyToOne
    @JoinColumn(name = "carrier_id", nullable = false, foreignKey = @ForeignKey(name = "FK_IATARATE_CARRIERID"))
    CarrierMaster carrierMaster;

    @ManyToOne
    @JoinColumn(name = "pol", nullable = false, foreignKey = @ForeignKey(name = "FK_IATARATE_POLID"))
    PortMaster pol;

    @Column(name = "valid_from_date", nullable = false)
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date validFromDate;

    @Column(name = "valid_to_date", nullable = false)
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date validToDate;

    @ManyToOne
    @JoinColumn(name = "currency_id", foreignKey = @ForeignKey(name = "FK_IATARATE_CURREID"))
    CurrencyMaster currencyMaster;

    @Column(name = "currency_code", length = 30)
    String currencyCode;

    /* Online Offline */
    @Column(name = "online_offline", length = 30)
    @Enumerated(EnumType.STRING)
    OnlineOffline onlineOffline;

    @Column(name = "remark", length = 4000)
    String remark;

    @OneToMany(mappedBy = "iataRateMaster", cascade = CascadeType.ALL, orphanRemoval = true)
    @JsonManagedReference
    List<IataRateCharge> iataRateChargeList = new ArrayList<>();

    @OneToMany(mappedBy = "iataRateMaster", cascade = CascadeType.ALL, orphanRemoval = true)
    @JsonManagedReference
    List<IataRateAttachment> attachmentList = new ArrayList<>();

}
