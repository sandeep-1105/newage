package com.efreightsuite.exception;

public class EDIReaderException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    private String message;

    public EDIReaderException(String message) {
        super(message);
        this.message = message;
    }

    @Override
    public String getMessage() {
        return message;
    }

    @Override
    public String toString() {
        return "EDIReaderException [ message=" + message + "]";
    }
}
