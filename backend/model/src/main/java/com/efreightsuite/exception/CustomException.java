package com.efreightsuite.exception;


import com.efreightsuite.dto.BaseDto;
import lombok.Data;
import lombok.EqualsAndHashCode;


@Data
@EqualsAndHashCode(callSuper = false)
public class CustomException extends RuntimeException {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    BaseDto baseDto;
}
