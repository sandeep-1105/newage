package com.efreightsuite.exception;

public class UniqueKey {
    public static final String UK_LANGDETAIL_CODE = "UK_LANGDETAIL_CODE";

    public static final String UK_REFERENCE_REFERENCECODE = "UK_REFERENCE_REFERENCECODE";
    public static final String UK_REFERENCE_REFERENCENAME = "UK_REFERENCE_REFERENCENAME";
    public static final String UK_ACCOUNTMASTER_ACCOUNTCODE = "UK_ACCOUNTMASTER_ACCOUNTCODE";
    public static final String UK_ACCOUNTMASTER_ACCOUNTNAME = "UK_ACCOUNTMASTER_ACCOUNTNAME";
    public static final String UK_SERVICETYPE_SERVICECODE = "UK_SERVICETYPE_SERVICECODE";
    public static final String UK_SERVICETYPE_SERVICENAME = "UK_SERVICETYPE_SERVICENAME";
    public static final String UK_SERVICETYPE_SERVICETYPE = "UK_SERVICETYPE_SERVICETYPE";

    public static final String UK_SERVICE_SERVICECODE = "UK_SERVICE_SERVICECODE";
    public static final String UK_SERVICE_SERVICENAME = "UK_SERVICE_SERVICENAME";

    public static final String UK_CARRIER_CARRIERCODE = "UK_CARRIER_CARRIERCODE";
    public static final String UK_CARRIER_CARRIERNAME = "UK_CARRIER_CARRIERNAME";
    public static final String UK_CARRIER_CARRIERNO = "UK_CARRIER_CARRIERNO";
    public static final String UK_CARRIER_SCACCODE = "UK_CARRIER_SCACCODE";
    public static final String UK_CARRIER_IATACODE = "UK_CARRIER_IATACODE";
    public static final String UK_CARRIEREDI_EDITYPE = "UK_CARRIEREDI_EDITYPE";


    public static final String UK_TOSMASTER_TOSCODE = "UK_TOSMASTER_TOSCODE";
    public static final String UK_TOSMASTER_TOSNAME = "UK_TOSMASTER_TOSNAME";


    public static final String UK_COMPANY_COMPANYCODE = "UK_COMPANY_COMPANYCODE";
    public static final String UK_COMPANY_COMPANYNAME = "UK_COMPANY_COMPANYNAME";

    public static final String UK_EVENT_EVENTCODE = "UK_EVENT_EVENTCODE";
    public static final String UK_EVENT_EVENTNAME = "UK_EVENT_EVENTNAME";

    public static final String UK_UNIT_UNITCODE = "UK_UNIT_UNITCODE";
    public static final String UK_UNIT_UNITNAME = "UK_UNIT_UNITNAME";

    public static final String UK_CFSMAS_CFSCODE = "UK_CFSMAS_CFSCODE";
    public static final String UK_CFSMAS_CFSNAME = "UK_CFSMAS_CFSNAME";

    public static final String UK_CURRENCY_CURRENCYCODE = "UK_CURRENCY_CURRENCYCODE";
    public static final String UK_CURRENCY_CURRENCYNAME = "UK_CURRENCY_CURRENCYNAME";

    public static final String UK_CHARGE_CHARGECODE = "UK_CHARGE_CHARGECODE";
    public static final String UK_CHARGE_CHARGENAME = "UK_CHARGE_CHARGENAME";
    public static final String UK_EDICHARGE_EDITYPE = "UK_EDICHARGE_EDITYPE";

    public static final String UK_COMMODITY_HSCODE = "UK_COMMODITY_HSCODE";
    public static final String UK_COMMODITY_HSNAME = "UK_COMMODITY_HSNAME";

    public static final String UK_PACK_PACKCODE = "UK_PACK_PACKCODE";
    public static final String UK_PACK_PACKNAME = "UK_PACK_PACKNAME";

    public static final String UK_CATEGORY_CATEGORYCODE = "UK_CATEGORY_CATEGORYCODE";
    public static final String UK_CATEGORY_CATEGORYNAME = "UK_CATEGORY_CATEGORYNAME";

    public static final String UK_CURRENCY_FROMTODATE = "UK_CURRENCY_FROMTODATE";


    public static final String UK_DOCPREFIIX_DOCPREFIXCODE = "UK_DOCPREFIIX_DOCPREFIXCODE";

    public static final String UK_DOCPREFIIX_DOCPREFIXNAME = "UK_DOCPREFIIX_DOCPREFIXNAME";


    public static final String UK_DOCPREFIIXTYPE_LOCDOCTYPE = "UK_DOCPRETYPE_LOCDOC";

    public static final String UK_DOCPREFIIXTYPE_LOCREPORTTYPE = "UK_DOCPRETYPE_LOCREPORT";


    public static final String UK_PORT_PORTCODE = "UK_PORT_PORTCODE";

    public static final String UK_PORT_PORTNAME = "UK_PORT_PORTNAME";

    public static final String UK_EDIPORT_EDITYPE = "UK_EDIPORT_EDITYPE";

    public static final String UK_PORTGROUP_PORTGROUPCODE = "UK_PORTGROUP_PORTGROUPCODE";

    public static final String UK_PORTGROUP_PORTGROUPNAME = "UK_PORTGROUP_PORTGROUPNAME";

    public static final String UK_CITY_CITYCODE = "UK_CITY_CITYCODE";

    public static final String UK_CITY_CITYNAME = "UK_CITY_CITYNAME";

    public static final String UK_STATE_STATECODE = "UK_STATEPROVINCE_STATECODE";

    public static final String UK_STATE_STATENAME = "UK_STATEPROVINCE_STATENAME";

    public static final String UK_ZONE_ZONECODE = "UK_ZONE_ZONECODE";

    public static final String UK_SVC_PORT_AGENTID = "UK_SVC_PORT_AGENTID";

    public static final String UK_ZONE_ZONENAME = "UK_ZONE_ZONENAME";

    public static final String UK_EMPLOYEE_EMPLOYEECODE = "UK_EMPLOYEE_EMPLOYEECODE";

    public static final String UK_DIVISION_DIVISIONCODE = "UK_DIVISION_DIVISIONCODE";

    public static final String UK_DIVISION_DIVISIONNAME = "UK_DIVISION_DIVISIONNAME";

    public static final String UK_LOCATION_LOCATIONCODE = "UK_LOCATION_LOCATIONCODE";

    public static final String UK_LOCATION_LOCATIONNAME = "UK_LOCATION_LOCATIONNAME";


    public static final String UK_PARTYGRP_PARTYGROUPCODE = "UK_PARTYGRP_PARTYGROUPCODE";
    public static final String UK_PARTYGRP_PARTYGROUPNAME = "UK_PARTYGRP_PARTYGROUPNAME";
    public static final String UK_PARTYGROUP_PARTY = "UK_PARTYGROUP_PARTY";


    public static final String UK_PARTY_PARTYCODE = "UK_PARTY_PARTYCODE";
    public static final String UK_PARTY_PARTYNAME = "UK_PARTY_PARTYNAME";
    public static final String UK_PARTYDETAIL_VALUEADDEDTAX = "UK_PARTYDETAIL_VALUEADDEDTAX";
    public static final String UK_PARTYDETAIL_PAN = "UK_PARTYDETAIL_PAN";
    public static final String UK_PARTYDETAIL_RACNO = "UK_PARTYDETAIL_RACNO";
    public static final String UK_PARTYDETAIL_CSTNO = "UK_PARTYDETAIL_CSTNO";
    public static final String UK_PARTYDETAIL_SVTNO = "UK_PARTYDETAIL_SVTNO";
    public static final String UK_PARTYDETAIL_SVATNO = "UK_PARTYDETAIL_SVATNO";
    public static final String UK_PARTYDETAIL_TINNO = "UK_PARTYDETAIL_TINNO";
    public static final String UK_PARTYDETAIL_IATA_CODE = "UK_PARTYDETAIL_IATA_CODE";
    public static final String UK_PARTYSERVIE_PARTYSERVICE = "UK_PARTYSERVIE_PARTYSERVICE";
    public static final String UK_PARTYCOMPDIV_PARTYCOMPDIV = "UK_PARTYCOMPDIV_PARTYCOMPDIV";
    public static final String UK_PARTYEMAIL_PARTY = "UK_PARTYEMAIL_PARTY";


    public static final String UK_AUTOMAIL_MESSAGECODE = "UK_AUTOMAIL_MESSAGECODE";

    public static final String UK_AUTOMAIL_MESSAGENAME = "UK_AUTOMAIL_MESSAGENAME";

    public static final String UK_AUTOMAIL_GROUP_MSGGRPCODE = "UK_AUTOMAIL_GROUP_MSGGRPCODE";

    public static final String UK_AUTOMAIL_GROUP_MSGGRPNAME = "UK_AUTOMAIL_GROUP_MSGGRPNAME";

    public static final String UK_COSTCENTER_COSTCENTERCODE = "UK_COSTCENTER_COSTCENTERCODE";
    public static final String UK_COSTCENTER_COSTCENTERNAME = "UK_COSTCENTER_COSTCENTERNAME";

    public static final String UK_RATECHARGE = "UK_RATECHARGE";
    public static final String UK_ENQUIRYATT_REF = "UK_ENQUIRYATT_REF";
    public static final String UK_ENQDET_SERVICE = "UK_ENQDET_SERVICE";
    public static final String UK_LOGO_LOCATION = "UK_LOGO_LOCATION";

    public static final String UK_PARTYTYPE_PARTYTYPECODE = "UK_PARTYTYPE_PARTYTYPECODE";
    public static final String UK_PARTYTYPE_PARTYTYPENAME = "UK_PARTYTYPE_PARTYTYPENAME";
    public static final String UK_PARTYTYPE_PARTYTYPE = "UK_PARTYTYPE_PARTYTYPE";

    public static final String UK_PARTY_CREDIT = "UK_PARTY_CREDIT";

    public static final String UK_PARTY_ACCOUNT = "UK_PARTY_ACCOUNT";

    // Period Master Unique Keys
    public static final String UK_PERIOD_FREQUENCY_CODE = "UK_PERIOD_FREQUENCYCODE";
    public static final String UK_PERIOD_FREQUENCY_NAME = "UK_PERIOD_FREQUENCYNAME";
    // Project Master Unique Keys
    public static final String UK_PROJECT_PROJECT_CODE = "UK_PROJECT_PROJECTCODE";
    public static final String UK_PROJECT_PROJECT_NAME = "UK_PROJECT_PROJECTNAME";

    public static final String UK_QUOTATION_NO = "UK_QUOTATION_NO";
    public static final String UK_QUOTATION_PARTYVAILDTO = "UK_QUOTATION_PARTYVAILDTO";
    public static final String UK_QUOTATIONATT_REF = "UK_QUOTATIONATT_REF";

    public static final String UK_TRIGGERTYPE_TRIGGERTYPECODE = "UK_TRIGGERTYPE_TRIGTYPECODE";
    public static final String UK_TRIGGERTYPE_TRIGGERTYPENAME = "UK_TRIGGERTYPE_TRIGTYPENAME";

    public static final String UK_TRIGGER_TRIGGERCODE = "UK_TRIGGER_TRIGGERCODE";
    public static final String UK_TRIGGER_TRIGGERNAME = "UK_TRIGGER_TRIGGERNAME";

    public static final String UK_DOCUMENT_DOCUMENTCODE = "UK_DOCUMENT_DOCUMENTCODE";
    public static final String UK_DOCUMENT_DOCUMENTNAME = "UK_DOCUMENT_DOCUMENTNAME";

    public static final String UK_SHIPMENTATT_REF = "UK_SHIPMENTATT_REF";
    public static final String UK_SERVICE_DUPLICATE = "UK_SERVICE_DUPLICATE";
    public static final String UK_CONSOL_CONSOLUID = "UK_CONSOL_CONSOLUID";
    public static final String UK_CONSLATTCH_REF = "UK_CONSLATTCH_REF";

    public static final String UK_USERPROFILE_USERNAME = "UK_USERPROFILE_USERNAME";
    public static final String UK_ROLE_ROLENAME = "UK_ROLE_ROLENAME";

    public static final String UK_FLIGHTP_SCHEDULEID = "UK_FLIGHTP_SCHEDULEID";
    public static final String UK_FLIGHTP_FLIGHTCON = "UK_FLIGHTP_FLIGHTCON";

    public static final String UK_STOCK_UNIQUE = "UK_STOCK_UNIQUE";
    public static final String UK_STOCK_MAWB_UNIQUE = "UK_STOCK_MAWB_UNIQUE";


    public static final String UK_USERCOM_USERCOMID = "UK_USERCOM_USERCOMID";
    public static final String UK_USERCOMLOC_USERCOMLOCID = "UK_USERCOMLOC_USERCOMLOCID";

    public static final String UK_USER_ROLE = "UK_USER_ROLE";

    public static final String UK_ROLEHASFEATURE_UNQ = "UK_ROLEHASFEATURE_UNQ";

    public static final String UK_SHIPMENT_SHIPMENTUID = "UK_SHIPMENT_SHIPMENTUID";

    public static final String UK_DOCUMENT_DOCUMENTTYPENAME = "UK_DOCUMENT_DOCUMENTTYPENAME";
    public static final String UK_DOCUMENT_DOCUMENTTYPECODE = "UK_DOCUMENT_DOCUMENTTYPECODE";

    public static final String UK_DAYBOOK_DAYBOOKCODE = "UK_DAYBOOK_DAYBOOKCODE";
    public static final String UK_DAYBOOK_DAYBOOKNAME = "UK_DAYBOOK_DAYBOOKNAME";

    public static final String UK_REASON_REASONNAME = "UK_REASON_REASONNAME";
    public static final String UK_REASON_REASONCODE = "UK_REASON_REASONCODE";

    public static final String UK_PROVSNL_MASTERUID = "UK_PROVSNL_MASTERUID";
    public static final String UK_PROVSNL_SERVICEUID = "UK_PROVSNL_SERVICEUID";

    public static final String UK_CARRIER_RATE = "UK_CARRIER_RATE";

    public static final String UK_DOCUMENTDETA_DOCUMENTNO = "UK_DOCUMENTDETA_DOCUMENTNO";
    public static final String UK_DOCUMENTDETA_HAWBNO = "UK_DOCUMENTDETA_HAWBNO";


    public static final String CFS_RECEIVE_ENTRY_ATTACHMENT_REFERENCE_NUMBER = "UK_CFSATTACH_REF";

    //Purchase Order - Starts Here
    public static final String UK_PURCHASE_ORDER_NO = "UK_PURCHASE_ORDER_NO";

    public static final String FK_PURCHASE_ORDER_BUYER = "FK_PURCHASE_ORDER_BUYER";

    public static final String FK_PURCHASE_ORDER_SUPPLIER = "FK_PURCHASE_ORDER_SUPPLIER";

    public static final String FK_PURCHASE_ORDER_ORIGIN = "FK_PURCHASE_ORDER_ORIGIN";

    public static final String FK_PURCHASE_ORDER_DESTINATION = "FK_PURCHASE_ORDER_DESTINATION";

    public static final String FK_PURCHASE_ORDER_ORIGIN_AGENT = "FK_PURCHASE_ORDER_ORIGIN_AGENT";

    public static final String FK_PURCHASE_ORDER_DEST_AGENT = "FK_PURCHASE_ORDER_DEST_AGENT";

    public static final String FK_PURCHASE_ORDER_TOS = "FK_PURCHASE_ORDER_TOS";

    public static final String UK_PO_ATTACHMENT_REF = "UK_PO_ATTACHMENT_REF";

    public static final String FK_PO_ATTACHMENT_PO = "FK_PO_ATTACHMENT_PO";

    public static final String FK_PO_ITEM_PO = "FK_PO_ITEM_PO";

    public static final String FK_PO_REMARK_PO = "FK_PO_REMARK_PO";

    public static final String FK_PO_VEHICLE_INFO_PO = "FK_PO_VEHICLE_INFO_PO";

    public static final CharSequence UK_BUYERS_CONSOLD_BCNAME = "UK_BUYERS_CONSOLD_BCNAME";

    public static final CharSequence UK_BUYERS_CONSOLD_BCCODE = "UK_BUYERS_CONSOLD_BCCODE";

    public static final CharSequence UK_DATECONFI_DATE = "UK_DATECONFI_DATE";

    public static final CharSequence UK_ENQUIRY_SERVICE_MAPPING = "UK_ENQUIRY_SERVICE_MAPPING";

    public static final CharSequence UK_ENQSERVICE_SERVICEID = "UK_ENQSERVICE_SERVICEID";


    public static final String UK_SERMAP_IMPID = "UK_SERMAP_IMPID";
    public static final String UK_SERMAP_EXPID = "UK_SERMAP_EXPID";


    //Purchase Order - Ends Here

    public static final String UK_DEFAULT_MASTER_DATA_CODE = "UK_DEFAULT_MASTER_DATA_CODE";

    public static final String UK_SOFTWARE_SOFTWARECODE = "UK_SOFTWARE_SOFTWARECODE";

    public static final String UK_SOFTWARE_SOFTWARENAME = "UK_SOFTWARE_SOFTWARENAME";

    public static final String UK_OBJECTGROUP_OBJECTGROUPCODE = "UK_OBJECTGROUP_OBJECTGROUPCODE";

    public static final String UK_OBJECTGROUP_OBJECTGROUPNAME = "UK_OBJECTGROUP_OBJECTGROUPNAME";

    public static final String UK_OBJECTSUBGROUP_OBJECTSUBGROUPCODE = "UK_OBJECTSUBGROUP_OBJECTSUBGROUPCODE";

    public static final String UK_OBJECTSUBGROUP_OBJECTSUBGROUPNAME = "UK_OBJECTSUBGROUP_OBJECTSUBGROUPNAME";

    public static final String UK_VAS_VASNAME = "UK_VAS_VASNAME";

    public static final String UK_VAS_VASCODE = "UK_VAS_VASCODE";


    public static final String UK_GRADE_GRADENAME = "UK_GRADE_GRADENAME";

    public static final String UK_GRADE_GRADEPRIORITY = "UK_GRADE_GRADEPRIORITY";


    //BANK MASTER
    public static final String UK_BANK_BANKCODE = "UK_BANK_BANKCODE";
    public static final String UK_BANK_BANKNAME = "UK_BANK_BANKNAME";


    //BANK MASTER
    public static final String UK_REGION_REGIONCODE = "UK_REGION_REGIONCODE";
    public static final String UK_REGION_REGIONNAME = "UK_REGION_REGIONNAME";


    public static final String UK_ESSENTIAL_CHARGE_MASTER = "UK_ESSENTIAL_CHARGE_MASTER";


    // Department Master
    public static final String UK_DEPARTMENT_DEPTCODE = "UK_DEPARTMENT_DEPTCODE";
    public static final String UK_DEPARTMENT_DEPTNAME = "UK_DEPARTMENT_DEPTNAME";

    // Designation Master
    public static final String UK_DESIGNATION_DESIGNATION_CODE = "UK_DESIGNATION_DESIG_CODE";
    public static final String UK_DESIGNATION_DESIGNATION_NAME = "UK_DESIGNATION_DESIG_NAME";

    //STCG Master
    public static final String UK_SVC_CHRG_CAT = "UK_SVC_CHRG_CAT";

    // service tax category
    public static final String UK_SERVTAXCATE_CODE = "UK_SERVTAXCATE_CODE";
    public static final String UK_SERVTAXCATE_NAME = "UK_SERVTAXCATE_NAME";

    //Report Master

    public static final String UK_REPORT_REPORTNAME = "UK_REPORT_REPORTCODE";
    public static final String UK_REPORT_REPORTCODE = "UK_REPORT_REPORTNAME";
    public static final String UK_REPORT_REPORTKEY = "UK_REPORT_REPORTKEY";

    //CountryReportConfig
    public static final String UK_PAGECOUNTRY_KEY = "UK_PAGECOUNTRY_KEY";

    public static final String UK_SURCHARGE_CHARGEID = "UK_SURCHARGE_CHARGEID";
    public static final String UK_SURCHARGE_SURCHARGE = "UK_SURCHARGE_SURCHARGE";


    public static final String UK_CHARTER_CHARTERCODE = "UK_CHARTER_CHARTERCODE";
    public static final String UK_CHARTER_CHARTERNAME = "UK_CHARTER_CHARTERNAME";


    public static final String UK_COMMENT_COMMENTNAME = "UK_COMMENT_COMMENTNAME";
    public static final String UK_COMMENT_COMMENTCODE = "UK_COMMENT_COMMENTCODE";
    public static final String UK_RAL_NAME = "UK_RAL_NAME";

    public static final String UK_SEQUENCE_TYPE = "UK_SEQUENCE_TYPE";
    public static final String UK_SEQUENCE_NAME = "UK_SEQUENCE_NAME";
    public static final String UK_SEQUENCE_PREFIX = "UK_SEQUENCE_PREFIX";
    public static final String UK_EDI_CONFIGURATION_KEY = "UK_EDI_CONFIGURATION_KEY";


    //GROUP AND Sub Group Master

    public static final String UK_GLGRP_CODE = "UK_GLGRP_CODE";
    public static final String UK_GLGRP_NAME = "UK_GLGRP_NAME";
    public static final String UK_SGRP_CODE = "UK_SGRP_CODE";
    public static final String UK_SGRP_NAME = "UK_SGRP_NAME";


    //Report Template

    public static final String COMP_UNQ_R_TEMPLATE_CAT = "COMP_UNQ_R_TEMPLATE_CAT";

    public static final String UK_PAGE_CODE = "UK_PAGE_CODE";
    public static final String UK_PAGE_NAME = "UK_PAGE_NAME";


    //Pricing POrt Pair
    public static final String UK_PRICE_CHARGE = "UK_PRICE_CHARGE";
    public static final String CHARGE_ALREADY_EXISTS = "CHARGE_ALREADY_EXISTS";


    public static final String UK_COM_CHARGE_CHARGECODE = "UK_COM_CHARGE_CHARGECODE";
    public static final String UK_COM_CHARGE_CHARGENAME = "UK_COM_CHARGE_CHARGENAME";
    public static final String UK_COM_SEQUENCE_TYPE = "UK_COM_SEQUENCE_TYPE";
    public static final String UK_COM_SEQUENCE_PREFIX = "UK_COM_SEQUENCE_PREFIX";
    public static final String UK_LOC_DAYBOOK_DAYBOOKCODE = "UK_LOC_DAYBOOK_DAYBOOKCODE";
    public static final String UK_LOC_DAYBOOK_DAYBOOKNAME = "UK_LOC_DAYBOOK_DAYBOOKNAME";
}

