package com.efreightsuite.exception;

public class EDIGenerationException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    private String message;

    public EDIGenerationException(String message) {
        super(message);
        this.message = message;
    }

    @Override
    public String getMessage() {
        return message;
    }

    @Override
    public String toString() {
        return "EDIGenerationException [ message=" + message + "]";
    }
}
