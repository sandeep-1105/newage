package com.efreightsuite.exception;

public class ErrorCode {

    public static final String SUCCESS = "ERR0";
    public static final String FAILED = "ERR1";
    public static final String UNAUTHORIZED_USER = "ERR2";
    public static final String SAAS_USER_EMPTY = "ERR3";
    public static final String SAAS_INVAILD_USER = "ERR4";
    public static final String ERROR_GENERIC = "ERR5";
    public static final String ERROR_FK_CONSTRAINT = "ERR6";
    public static final String INVALID_PARAMETER = "ERR7";
    public static final String INVALID_PARAMETER_FORMAT = "ERR8";
    public static final String DB_ERROR = "ERR9";


    public static final String SEARCH_KEYWORD_MISSING = "ERR11";
    public static final String SEARCH_RECORD_PER_PAGE_MISSING = "ERR12";
    public static final String SEARCH_SELECTED_PAGE_NUMBER_MISSING = "ERR13";
    public static final String SEQUENCE_TYPE_ALREADY_EXIST = "ERR14";
    public static final String SEQUENCE_NAME_ALREADY_EXIST = "ERR15";
    public static final String SEQUENCE_TYPE_NOT_NULL = "ERR16";
    public static final String SEQUENCE_FORMAT_NOT_NULL = "ERR17";
    public static final String SEQUENCE_PREFIX_NOT_NULL = "ERR18";
    public static final String SEQUENCE_PREFIX_LENGTH = "ERR19";
    public static final String CANNOT_UPDATE_LOCKED_RECORD = "ERR20";
    public static final String CANNOT_UPDATE_DELETED_RECORD = "ERR21";
    public static final String DIVISION_STATUS_BLOCK = "ERR22";
    public static final String DIVISION_STATUS_HIDE = "ERR23";

    public static final String SERVICE_STATUS_BLOCK = "ERR24";
    public static final String SERVICE_STATUS_HIDE = "ERR25";

    public static final String TOS_STATUS_BLOCK = "ERR26";
    public static final String TOS_STATUS_HIDE = "ERR27";
    public static final String SALESMAN_STATUS_BLOCK = "ERR28";
    public static final String SALESMAN_STATUS_HIDE = "ERR29";
    public static final String CUSTOMERSERVICE_STATUS_BLOCK = "ERR30";
    public static final String CUSTOMERSERVICE_STATUS_HIDE = "ERR31";
    public static final String PARTY_ACCOUNT_STATUS_BLOCKED = "ERR32";
    public static final String PARTY_ACCOUNT_STATUS_HIDDEN = "ERR33";
    public static final String PARTY_CURRENCY_STATUS_BLOCK = "ERR34";
    public static final String PARTY_CURRENCY_STATUS_HIDE = "ERR35";
    public static final String COUNTRY_NOTNULL = "ERR36";
    public static final String COUNTRY_STATUS_BLOCK = "ERR37";
    public static final String COUNTRY_STATUS_HIDE = "ERR06406";
    public static final String LOCATION_STATUS_BLOCK = "ERR38";
    public static final String ORIGIN_STATUS_BLOCK = "ERR39";
    public static final String DESTINATION_STATUS_BLOCK = "ERR40";
    public static final String DESTINATION_ORIGIN_DIFFERENT = "ERR41";
    public static final String NOTES_INVALID = "ERR42";
    public static final String COMMODITY_STATUS_BLOCK = "ERR43";
    public static final String LOCATION_IS_NOTNULL = "ERR44";
    public static final String CATEGORY_IS_NOTNULL = "ERR45";
    public static final String USER_LOCATION_NOT_ASSOCIATE = "ERR46";
    public static final String SEQUENCE_NOT_PRESENT = "ERR47";
    public static final String SEQUENCE_PREFIX_ALREADY_EXIST = "ERR48";
    public static final String SAVE_UPDATE_FK_RECORD = "ERR99";
    public static final String CANNOT_DELETE_FK_RECORD = "ERR100";

    public static final String CSV_FILE_INVALID = "ERR110";
    public static final String EXCEL_FILE_INVALID = "ERR111";
    public static final String CSV_MISMATCH_RECORD_INVALID = "ERR112";


    public static final String SAVE_CHANGES = "ERR200";
    public static final String UPDATE_CHANGES = "ERR201";
    public static final String DELETE_SELECTED_ADDRESS = "ERR202";

    public static final String PARTY_SAVED_SUCCESSFULLY = "ERR210";
    public static final String PARTY_UPDATED_SUCCESSFULLY = "ERR211";

    public static final String ENQUIRY_SAVED_SUCCESSFULLY = "ERR212";
    public static final String ENQUIRY_UPDATED_SUCCESSFULLY = "ERR213";

    public static final String SELECTED_SERVICE_DELETED = "ERR214";

    public static final String QUOTATION_SAVED_SUCCESSFULLY = "ERR215";
    public static final String QUOTATION_UPDATED_SUCCESSFULLY = "ERR216";


    public static final String SHIPMENT_SAVED_SUCCESSFULLY = "ERR217";
    public static final String SHIPMENT_UPDATED_SUCCESSFULLY = "ERR218";

    public static final String MASTER_SHIPMENT_SAVED_SUCCESSFULLY = "ERR219";
    public static final String MASTER_SHIPMENT_UPDATED_SUCCESSFULLY = "ERR220";
    public static final String MASTER_SHIPMENT_CANCEL = "ERR221";

    public static final String COMPLETE_EVENT_AUTOMAIL = "ERR222";
    public static final String COMPLETE_IMPORT_PROCESS = "ERR223";
    public static final String QUOTATION_APPROVED = "ERR224";

    public static final String QUOTATION_APPROVAL_PIN_INVALID = "ERR255";
    public static final String QUOTATION_APPROVAL_QUOTATION_NO_REQUIRED = "ERR256";
    public static final String QUOTATION_APPROVAL_CUSTOMER_REQUIRED = "ERR257";

    public static final String QUOTATION_APPROVAL_PIN_REQUIRED = "ERR258";
    public static final String QUOTATION_ALREADY_GAINED = "ERR259";
    public static final String QUOTATION_ALREADY_REJECTED = "ERR260";
    public static final String QUOTATION_APPROVE_SCUCCESS = "ERR261";
    public static final String QUOTATION_REJECTED = "ERR262";
    public static final String QUOTATION_APPROVAL_INVALID = "ERR263";
    public static final String CANNOT_COMPLETE_IMPORT_PROCESS_EDI_REQUIRED = "ERR264";
    public static final String QUOTATION_NOT_APPROVED_MANAGER = "ERR265";


    public static final String ATTACHMENT_IS_INVALID = "ERR250";
    public static final String SAVED_SUCCESSFULLY = "ERR400";
    public static final String UPDATED_SUCCESSFULLY = "ERR401";
    public static final String EDI_GENERATED_SUCCESSFULLY = "ERR500";

    public static final String EDI_CONFIG_KEY_DUPLICATE = "ERR501";


    public static final String COMPANY_LIST_EMPTY = "ERR1001";
    public static final String COMPANY_CODE_ALREADY_EXIST = "ERR1002";
    public static final String COMPANY_NAME_ALREADY_EXIST = "ERR1003";
    public static final String COMPANY_CODE_INVALID = "ERR1004";
    public static final String COMPANY_NAME_INVALID = "ERR1005";
    public static final String COMPANY_ID_INVALID = "ERR1006";
    public static final String COMPANY_CODE_NOT_NULL = "ERR1007";
    public static final String COMPANY_NAME_NOT_NULL = "ERR1008";
    public static final String COMPANY_STATUS_NOT_NULL = "ERR1009";
    public static final String COMPANY_NOT_NULL = "ERR1010";
    public static final String COMPANY_ID_NOT_FOUND = "ERR1011";
    public static final String COMPANY_ID_NOT_NULL = "ERR1012";

    public static final String COSTCENTER_LIST_EMPTY = "ERR1101";
    public static final String COSTCENTER_CODE_NOTFOUND = "ERR1102";
    public static final String COSTCENTER_NAME_NOTFOUND = "ERR1103";
    public static final String COSTCENTER_CODE_ALREADY_EXIST = "ERR1104";
    public static final String COSTCENTER_NAME_ALREADY_EXIST = "ERR1105";
    public static final String COSTCENTER_CODE_INVALID = "ERR1106";
    public static final String COSTCENTER_NAME_INVALID = "ERR1107";
    public static final String COSTCENTER_CODE_NOT_NULL = "ERR1108";
    public static final String COSTCENTER_NAME_NOT_NULL = "ERR1109";
    public static final String COSTCENTER_ID_INVALID = "ERR1110";
    public static final String COSTCENTER_COMPANY__NOT_NULL = "ERR1111";

    public static final String CARRIER_LIST_EMPTY = "ERR1201";
    public static final String CARRIER_CODE_ALREADY_EXIST = "ERR1202";
    public static final String CARRIER_NAME_ALREADY_EXIST = "ERR1203";
    public static final String CARRIER_CODE_INVALID = "ERR1204";
    public static final String CARRIER_NAME_INVALID = "ERR1205";
    public static final String CARRIER_ID_NOT_FOUND = "ERR1206";
    public static final String CARRIER_CODE_NOT_NULL = "ERR1207";
    public static final String CARRIER_NAME_NOT_NULL = "ERR1208";
    public static final String CARRIER_TRANSPORT_MODE_NOTNULL = "ERR1209";
    public static final String CARRIER_LOV_STATUS_NOTNULL = "ERR1210";
    public static final String CARRIER_NAME_LENGTH_EXCEED = "ERR1211";
    public static final String CARRIER_CODE_LENGTH_EXCEED = "ERR1212";
    public static final String CARRIER_NUM_INVALID = "ERR1213";
    public static final String CARRIER_NUM_MODEAIR_NOT_NULL = "ERR1214";
    public static final String CARRIER_SCAC_CODE_INVALID = "ERR1215";
    public static final String CARRIER_NUM_ALREADY_EXIST = "ERR1216";
    public static final String CARRIER_SCAC_CODE_ALREADY_EXIST = "ERR1217";
    public static final String CARRIER__CODE_MODE_AIR_INVALID = "ERR1218";
    public static final String CARRIER_NUM_NOT_NULL = "ERR1219";
    public static final String CARRIER_NUM__AIR_MODE_INVALID = "ERR1220";
    public static final String CARRIER_AIR_CODE_INVALID = "ERR1221";
    public static final String CARRIER_CODE_NOTFOUND = "ERR1222";
    public static final String CARRIER_NAME_NOTFOUND = "ERR1223";
    public static final String CARRIER_IATA_CODE_ALREADY_EXIST = "ERR1224";
    public static final String CARRIER_ID_NOT_NULL = "ERR1225";
    public static final String EDI_TYPE = "ERR1226";
    public static final String EDI_SUBTYPE = "ERR1227";
    public static final String EDI_VALUE = "ERR1228";
    public static final String EDI_TYPE_ALREDY_EXIST = "ERR1229";
    public static final String CARRIER_ADDRESS_LOCATION = "ERR1230";
    public static final String CARRIER_ADDRESS_PARTY = "ERR1231";
    public static final String CARRIER_ADDRESS_ACCOUNTNUMBER = "ERR1232";

    public static final String CARRIER_CODE_MUST_BE_2_CHR = "ERR1233";
    public static final String PROVIDE_ONLY_THREE_NUMERIC_DGTS = "ERR1234";
    public static final String CARRIER_ICAO_CODE_INVALID = "ERR1235";


    public static final String CARRIER_ADDRESS_INVALID = "ERR1314";
    public static final String Account_number_mandatory = "ERR1236";
    public static final String Carrier_agent_mandatory = "ERR1237";
    public static final String CARRIER_AGENT_HIDDEN = "ERR1238";
    public static final String CARRIER_AGENT_BLOCKED = "ERR1239";
    public static final String ACCOUNT_NUMBER_INVALID = "ERR1240";


    public static final String CATEGORY_CODE_ALREADY_EXIST = "ERR1301";
    public static final String CATEGORY_NAME_ALREADY_EXIST = "ERR1302";
    public static final String CATEGORY_CODE_INVALID = "ERR1303";
    public static final String CATEGORY_ID_NOT_NULL = "ERR1304";
    public static final String CATEGORY_NAME_INVALID = "ERR1305";
    public static final String CATEGORY_CODE_NOT_NULL = "ERR1306";
    public static final String CATEGORY_NAME_NOT_NULL = "ERR1307";
    public static final String CATEGORY_ID_NOT_FOUND = "ERR1308";
    public static final String CATEGORY_NOT_NULL = "ERR1309";
    public static final String CATEGORY_STATUS_NOT_NULL = "ERR1310";
    public static final String CATEGORY_CODE_LENGTH_EXCEED = "ERR1311";
    public static final String CATEGORY_NAME_LENGTH_EXCEED = "ERR1312";
    public static final String CATEGORY_LIST_EMPTY = "ERR1313";
    //check top i created the ERR1314 for carrier address

    public static final String SERVICE_LIST_EMPTY = "ERR1401";
    public static final String SERVICE_CODE_ALREADY_EXIST = "ERR1402";
    public static final String SERVICE_NAME_ALREADY_EXIST = "ERR1403";
    public static final String SERVICE_CODE_INVALID = "ERR1404";
    public static final String SERVICE_NAME_INVALID = "ERR1405";
    public static final String SERVICE_ID_NOT_FOUND = "ERR1406";
    public static final String SERVICE_CODE_NOT_NULL = "ERR1407";
    public static final String SERVICE_NAME_NOT_NULL = "ERR1408";
    public static final String SERVICE_TRANSPORT_MODE_NOTNULL = "ERR1409";
    public static final String SERVICE_IMPORT_EXPORT__MODE_NOTNULL = "ERR1410";
    public static final String SERVICE_FULL_GROUPAGE_NOTNULL = "ERR1411";
    public static final String SERVICE_LOV_STATUS_NOTNULL = "ERR1412";
    public static final String SERVICE_CODE_LENGTH_EXCEED = "ERR1413";
    public static final String SERVICE_NAME_LENGTH_EXCEED = "ERR1414";
    public static final String SERVICE_NOT_FOUND = "ERR1415";
    public static final String SERVICE_CODE_NOTFOUND = "ERR1416";
    public static final String SERVICE_NAME_NOTFOUND = "ERR1417";
    public static final String SERVICE_ID_NOT_NULL = "ERR1418";
    public static final String SERVICE_COST_CENTER = "ERR1419";
    public static final String SERVICE_COST_CENTER_BLOCK = "ERR1420";
    public static final String SERVICE_COST_CENTER_HIDE = "ERR1421";
    public static final String SERVICE_TYPE_BLOCK = "ERR1422";
    public static final String SERVICE_TYPE_HIDE = "ERR1423";
    public static final String SERVICE_TYPE_CODE_ALREADY_EXIST = "ERR1424";
    public static final String SERVICE_TYPE_NAME_ALREADY_EXIST = "ERR1425";
    public static final String SERVICE_TYPE_TYPE_ALREADY_EXIST = "ERR1426";
    public static final String SERVICE_TYPE_ID_NOT_NULL = "ERR1427";
    public static final String SERVICE_TYPE_ID_NOT_FOUND = "ERR1428";

    public static final String SERVICETYPE_CODE_NOT_NULL = "ERR1429";
    public static final String SERVICETYPE_NAME_NOT_NULL = "ERR1430";
    public static final String SERVICETYPE_CODE_INVALID = "ERR1431";
    public static final String SERVICETYPE_NAME_INVALID = "ERR1432";

    public static final String COST_CENTER_NAME_INVALID = "ERR1433";
    public static final String COST_CENTER_CODE_INVALID = "ERR1434";
    public static final String SERVICE_TYPE_MANDATORY = "ERR1435";


    public static final String PORT_LIST_EMPTY = "ERR1501";
    public static final String PORT_CODE_NOT_FOUND = "ERR1502";
    public static final String PORT_NAME_NOT_FOUND = "ERR1503";
    public static final String PORT_CODE_ALREADY_EXIST = "ERR1504";
    public static final String PORT_NAME_ALREADY_EXIST = "ERR1505";
    public static final String PORT_CODE_INVALID = "ERR1506";
    public static final String PORT_NAME_INVALID = "ERR1507";
    public static final String PORT_CODE_NOT_NULL = "ERR1508";
    public static final String PORT_NAME_NOT_NULL = "ERR1509";
    public static final String PORT_ASS_GROUP_NOT_NULL = "ERR1510";
    public static final String PORT_TRANSPORT_MODE_NOT_NULL = "ERR1511";
    public static final String PORT_STATUS_NOT_NULL = "ERR1512";
    public static final String PORT_ID_NOT_FOUND = "ERR1513";
    public static final String PORT_CODE_FOR_SEA_INVALID = "ERR1514";
    public static final String PORT_CODE_FOR_AIR_INVALID = "ERR1515";
    public static final String PORT_CODE_FOR_OTHER_INVALID = "ERR1516";
    public static final String PORT_CODE_FOR_SEA_COUNTRY_INVALID = "ERR1517";
    public static final String PORT_CODE_FOR_SEA_LENGTH_INVALID = "ERR1518";
    public static final String PORT_CODE_FOR_OTHER_LENGTH_INVALID = "ERR1519";
    public static final String PORT_NAME_LENGTH_INVALID = "ERR1520";
    public static final String PORT_GROUP_LIST_EMPTY = "ERR1521";
    public static final String PORT_GROUP_CODE_NOT_FOUND = "ERR1522";
    public static final String PORT_GROUP_NAME_NOT_FOUND = "ERR1523";
    public static final String PORT_GROUP_CODE_ALREADY_EXIST = "ERR1524";
    public static final String PORT_GROUP_NAME_ALREADY_EXIST = "ERR1525";
    public static final String PORT_GROUP_CODE_INVALID = "ERR1526";
    public static final String PORT_GROUP_NAME_INVALID = "ERR1527";
    public static final String PORT_GROUP_ID_INVALID = "ERR1528";
    public static final String PORT_GROUP_CODE_NOT_NULL = "ERR1529";
    public static final String PORT_GROUP_NAME_NOT_NULL = "ERR1530";
    public static final String PORT_GROUP_COUNTRY_NOT_NULL = "ERR1531";
    public static final String PORT_GROUP_COUNTRY_INVALID = "ERR1532";
    public static final String PORT_GROUP_ID_NOT_NULL = "ERR1533";
    public static final String PORT_GROUP_ID_NOT_FOUND = "ERR1534";
    public static final String PORT_GROUP_NOT_NULL = "ERR1535";
    public static final String PORT_GROUP_CODE_LENGTH_INVALID = "ERR1536";
    public static final String PORT_CODE_FOR_AIR_LENGTH_INVALID = "ERR1537";
    public static final String PORT_ID_INVALID = "ERR1538";
    public static final String PORT_ID_NOT_NULL = "ERR1539";

    public static final String COUNTRY_LIST_EMPTY = "ERR1601";
    public static final String COUNTRY_CODE_NOT_FOUND = "ERR1602";
    public static final String COUNTRY_NAME_NOT_FOUND = "ERR1603";
    public static final String COUNTRY_CODE_INVALID = "ERR1604";
    public static final String COUNTRY_CODE_NOT_NULL = "ERR1605";
    public static final String COUNTRY_NAME_INVALID = "ERR1606";
    public static final String COUNTRY_NAME_NOT_NULL = "ERR1607";
    public static final String COUNTRY_CODE_ALREADY_EXIST = "ERR1608";
    public static final String COUNTRY_NAME_ALREADY_EXIST = "ERR1609";
    public static final String COUNTRY_ID_NOT_NULL = "ERR1610";
    public static final String COUNTRY_ID_NOT_FOUND = "ERR1611";
    public static final String COUNTRY_CODE_NOT_UPDATABLE = "ERR1612";
    public static final String COUNTRY_NOT_NULL = "ERR1613";
    public static final String COUNTRY_CURRENCY_NOT_NULL = "ERR1614";
    public static final String UNIT_LIST_EMPTY = "ERR1701";
    public static final String UNIT_CODE_NOT_FOUND = "ERR1702";
    public static final String UNIT_NAME_NOT_FOUND = "ERR1703";
    public static final String UNIT_CODE_INVALID = "ERR1704";
    public static final String UNIT_CODE_NOT_NULL = "ERR1705";
    public static final String UNIT_NAME_INVALID = "ERR1706";
    public static final String UNIT_NAME_NOT_NULL = "ERR1707";
    public static final String UNIT_CODE_ALREADY_EXIST = "ERR1708";
    public static final String UNIT_NAME_ALREADY_EXIST = "ERR1709";
    public static final String UNIT_ID_NOT_FOUND = "ERR1710";
    public static final String UNIT_LOV_STATUS_NOT_NULL = "ERR1711";
    public static final String UNIT_MAPPING_1_INVALID = "ERR1712";
    public static final String UNIT_MAPPING_1_NOT_NULL = "ERR1713";
    public static final String UNIT_MAPPING_1_LENGTH_EXCEED = "ERR1714";
    public static final String UNIT_CALC_TYPE_1_INVALID = "ERR1715";
    public static final String UNIT_CALC_TYPE_1_NOT_NULL = "ERR1716";
    public static final String UNIT_CALC_TYPE_1_LENGTH_EXCEED = "ERR1717";
    public static final String UNIT_CALC_VALUE_1_NOT_NULL = "ERR1718";
    public static final String UNIT_MAPPING_2_LENGTH_EXCEED = "ERR1719";
    public static final String UNIT_CALC_TYPE_2_LENGTH_EXCEED = "ERR1720";
    public static final String UNIT_LOV_STATUS_INVALID = "ERR1721";
    public static final String UNIT_LOV_STATUS_LENGTH_EXCEED = "ERR1722";
    public static final String UNIT_ID_NOT_NULL = "ERR1723";
    public static final String UNIT_CALC_VALUE_1_INVALID = "ERR1724";
    public static final String UNIT_MAPPING_2_INVALID = "ERR1725";
    public static final String UNIT_MAPPING_2_NOT_NULL = "ERR1726";
    public static final String UNIT_CALC_TYPE_2_NOT_NULL = "ERR1727";
    public static final String UNIT_CALC_TYPE_2_INVALID = "ERR1728";
    public static final String UNIT_CALC_VALUE_2_INVALID = "ERR1729";
    public static final String UNIT_CODE_NOT_UPDATABLE = "ERR1730";
    public static final String UNIT_CALC_VALUE_2_NOT_NULL = "ERR1731";
    public static final String UNIT_TYPE_NOT_NULL = "ERR1732";
    public static final String CITY_LIST_EMPTY = "ERR1801";
    public static final String CITY_CODE_NOT_FOUND = "ERR1802";
    public static final String CITY_NAME_NOT_FOUND = "ERR1803";
    public static final String CITY_CODE_INVALID = "ERR1804";
    public static final String CITY_CODE_NOT_NULL = "ERR1805";
    public static final String CITY_NAME_INVALID = "ERR1806";
    public static final String CITY_NAME_NOT_NULL = "ERR1807";
    public static final String CITY_CODE_ALREADY_EXIST = "ERR1808";
    public static final String CITY_NAME_ALREADY_EXIST = "ERR1809";
    public static final String CITY_ID_NOT_FOUND = "ERR1810";
    public static final String CITY_COUNTRY_NOT_NULL = "ERR1811";
    public static final String CITY_COUNTRY_INVALID = "ERR1812";
    public static final String CITY_NOT_NULL = "ERR1813";
    public static final String CITY_ID_NOT_NULL = "ERR1814";
    public static final String CITY_COUNTRY_CODE_NULL = "ERR1815";
    public static final String CITY_STATE_NOT_NULL = "ERR1816";
    public static final String CITY_STATE_BLOCKED = "ERR1817";
    public static final String CITY_STATE_HIDDEN = "ERR1818";

    public static final String PERIOD_LIST_EMPTY = "ERR1850";
    public static final String PERIOD_FREQUENCY_CODE_ALREADY_EXIST = "ERR1851";
    public static final String PERIOD_FREQUENCY_NAME_ALREADY_EXIST = "ERR1852";
    public static final String PERIOD_FREQUENCY_CODE_REQUIRED = "ERR1853";
    public static final String PERIOD_FREQUENCY_NAME_REQUIRED = "ERR1854";
    public static final String PERIOD_FREQUENCY_CODE_INVALID = "ERR1855";
    public static final String PERIOD_FREQUENCY_NAME_INVALID = "ERR1856";

    public static final String PROJECT_LIST_EMPTY = "ERR1875";
    public static final String PROJECT_CODE_ALREADY_EXIST = "ERR1876";
    public static final String PROJECT_NAME_ALREADY_EXIST = "ERR1877";
    public static final String PROJECT_CODE_REQUIRED = "ERR1878";
    public static final String PROJECT_NAME_REQUIRED = "ERR1879";
    public static final String PROJECT_CODE_INVALID = "ERR1880";
    public static final String PROJECT_NAME_INVALID = "ERR1881";

    public static final String PACK_LIST_EMPTY = "ERR1901";
    public static final String PACK_NOT_FOUND = "ERR1902";
    public static final String PACK_CODE_NOT_NULL = "ERR1903";
    public static final String PACK_CODE_INVALID = "ERR1904";
    public static final String PACK_NAME_NOT_NULL = "ERR1905";
    public static final String PACK_NAME_INVALID = "ERR1906";
    public static final String PACK_LOV_STATUS_NOT_NULL = "ERR1907";
    public static final String PACK_LOV_STATUS_INVALID = "ERR1908";
    public static final String PACK_LOV_STATUS_LENGTH_EXCEED = "ERR1909";
    public static final String PACK_CODE_ALREADY_EXIST = "ERR1910";
    public static final String PACK_NAME_ALREADY_EXIST = "ERR1911";
    public static final String PACK_ID_NOT_NULL = "ERR1912";
    public static final String PACK_ID_NOT_FOUND = "ERR1913";
    public static final String PACK_CODE_NOT_FOUND = "ERR1914";


    public static final String REFERENCE_TYPE_CODE_NOT_NULL = "ERR1925";
    public static final String REFERENCE_TYPE_CODE_INVALID = "ERR1926";
    public static final String REFERENCE_TYPE_NAME_NOT_NULL = "ERR1927";
    public static final String REFERENCE_TYPE_NAME_INVALID = "ERR1928";
    public static final String REFERENCE_TYPE_CODE_ALREADY_EXIST = "ERR1929";
    public static final String REFERENCE_TYPE_NAME_ALREADY_EXIST = "ERR1930";
    public static final String REFERENCE_TYPE_NOTES_INVALID = "ERR1931";
    public static final String REFERENCE_TYPE_REFERNE_TYPE_REQ = "ERR1932";


    public static final String DOCUMENT_LIST_EMPTY = "ERR1951";
    public static final String DOCUMENT_NOT_FOUND = "ERR1952";
    public static final String DOCUMENT_CODE_NOT_NULL = "ERR1953";
    public static final String DOCUMENT_CODE_INVALID = "ERR1954";
    public static final String DOCUMENT_NAME_NOT_NULL = "ERR1955";
    public static final String DOCUMENT_NAME_INVALID = "ERR1956";
    public static final String DOCUMENT_LOV_STATUS_NOT_NULL = "ERR1957";
    public static final String DOCUMENT_LOV_STATUS_INVALID = "ERR1958";
    public static final String DOCUMENT_LOV_STATUS_LENGTH_EXCEED = "ERR1959";
    public static final String DOCUMENT_CODE_ALREADY_EXIST = "ERR1960";
    public static final String DOCUMENT_NAME_ALREADY_EXIST = "ERR1961";
    public static final String DOCUMENT_ID_NOT_NULL = "ERR1962";
    public static final String DOCUMENT_ID_NOT_FOUND = "ERR1963";
    public static final String DOCUMENT_CODE_NOT_FOUND = "ERR1964";
    public static final String DOCUMENT_ISPROTECTED_NOT_NULL = "ERR1965";


    public static final String TOS_ID_NOT_NULL = "ERR2001";
    public static final String TOS_ID_NOT_FOUND = "ERR2002";
    public static final String TOS_CODE_ALREADY_EXIST = "ERR2003";
    public static final String TOS_NAME_ALREADY_EXIST = "ERR2004";
    public static final String TOS_NOT_NULL = "ERR2005";
    public static final String TOS_CODE_NOT_NULL = "ERR2006";
    public static final String TOS_NAME_NOT_NULL = "ERR2007";
    public static final String TOS_PP_CC_NOT_NULL = "ERR2008";
    public static final String TOS_STATUS_NOT_NULL = "ERR2009";
    public static final String TOS_DESCRIPTION_NOT_NULL = "ERR2010";
    public static final String TOS_CODE_INVALID = "ERR2011";
    public static final String TOS_NAME_INVALID = "ERR2012";
    public static final String TOS_DESCRIPTION_INVALID = "ERR2013";
    public static final String TOS_LIST_EMPTY = "ERR2014";
    public static final String CURRENCY_ID_NOT_NULL = "ERR2101";
    public static final String CURRENCY_ID_NOT_FOUND = "ERR2102";
    public static final String CURRENCY_CODE_ALREADY_EXIST = "ERR2103";
    public static final String CURRENCY_NAME_ALREADY_EXIST = "ERR2104";
    public static final String CURRENCY_NOT_NULL = "ERR2105";
    public static final String CURRENCY_CODE_NOT_NULL_OR_EMPTY = "ERR2106";
    public static final String CURRENCY_NAME_NOT_NULL_OR_EMPTY = "ERR2107";
    public static final String CURRENCY_CODE_INVALID = "ERR2108";
    public static final String CURRENCY_NAME_INVALID = "ERR2109";
    public static final String CURRENCY_DECIMAL_POINT_INVALID = "ERR2110";
    public static final String CURRENCY_STATUS_NOT_NULL = "ERR2111";
    public static final String CURRENCY_PREFIX_INVALID = "ERR2112";
    public static final String CURRENCY_SUFFIX_INVALID = "ERR2113";
    public static final String CURRENCY_PREFIX_NOT_NULL_OR_EMPTY = "ERR2114";
    public static final String CURRENCY_SUFFIX_NOT_NULL_OR_EMPTY = "ERR2115";
    public static final String CURRENCY_RATE_ID_NOT_NULL = "ERR2116";
    public static final String CURRENCY_RATE_ID_NOT_FOUND = "ERR2117";
    public static final String CURRENCY_RATE_NOT_NULL = "ERR2118";
    public static final String CURRENCY_RATE_DATE_NOT_NULL = "ERR2119";
    public static final String CURRENCY_RATE_TODAY_DATE = "ERR2120";
    public static final String CURRENCY_RATE_DATE_NOT_EMPTY = "ERR2121";
    public static final String CURRENCY_RATE_BUY_RATE_NOT_NULL = "ERR2122";
    public static final String CURRENCY_RATE_SELL_RATE_NOT_NULL = "ERR2123";
    public static final String CURRENCY_RATE_BUYRATE_INVALID = "ERR2124";
    public static final String CURRENCY_RATE_BUYRATE_NOTZERO = "ERR2125";
    public static final String CURRENCY_RATE_SELLRATE_INVALID = "ERR2126";
    public static final String CURRENCY_RATE_BUY_RATE_LENGTH_INVALID = "ERR2127";
    public static final String CURRENCY_RATE_SELL_RATE_LENGTH_INVALID = "ERR2128";
    public static final String CURRENCY_RATE_FROM_CURRENCY_CODE_ALREADY_EXIST = "ERR2129";
    public static final String CURRENCY_RATE_TO_CURRENCY_CODE_ALREADY_EXIST = "ERR2130";
    public static final String CURRENCY_RATE_CURRENCY_DATE_ALREADY_EXIST = "ERR2131";
    public static final String CURRENCY_RATE_BUY_RATE_NON_ZERO = "ERR2132";
    public static final String CURRENCY_RATE_SELLRATE_GREATER_BUYRATE = "ERR2133";
    public static final String CURRENCY_RATE_SELLRATE_BUYRATE_NOT_EQUAL = "ERR2134";
    public static final String CURRENCY_RATE_COUNTRY_CODE_NOT_NULL = "ERR2135";
    public static final String CURRENCY_CODE_LENGTH_INVALID = "ERR2136";
    public static final String CURRENCY_NAME_LENGTH_INVALID = "ERR2137";
    public static final String CURRENCY_PREFIX_LENGTH_INVALID = "ERR2138";
    public static final String CURRENCY_SUFFIX_LENGTH_INVALID = "ERR2139";
    public static final String CURRENCY_LIST_EMPTY = "ERR2140";
    public static final String CURRENCYRATE_TOCURRENCY_BLOCK = "ERR2141";
    public static final String CURRENCYRATE_TOCURRENCY_HIDE = "ERR2142";
    public static final String CURRENCY_COUNTRY_BLOCK = "ERR2143";
    public static final String CURRENCY_COUNTRY_HIDE = "ERR2144";
    public static final String CURRENCY_RATE_TOCURRENCY_NOTNULL = "ERR2145";
    public static final String CURRENCY_COUNTRY_NOT_NULL = "ERR2146";
    public static final String CURRENCY_DECIMAL_REQUIRED = "ERR2147";
    public static final String DOCUMENT_PREFIXM_ID_NOT_FOUND = "ERR2201";
    public static final String DOCUMENT_PREFIXM_CODE_ALREADY_EXIST = "ERR2202";
    public static final String DOCUMENT_PREFIXM_NAME_ALREADY_EXIST = "ERR2203";
    public static final String DOCUMENT_PREFIXM_NOT_NULL = "ERR2204";
    public static final String DOCUMENT_PREFIXM_CODE_NOT_NULL_OR_EMPTY = "ERR2205";
    public static final String DOCUMENT_PREFIXM_NAME_NOT_NULL_OR_EMPTY = "ERR2206";
    public static final String DOCUMENT_PREFIXM_CODE_INVALID = "ERR2207";
    public static final String DOCUMENT_PREFIXM_NAME_INVALID = "ERR2208";
    public static final String DOCUMENT_SUFFIX_NOT_NULL = "ERR2209";
    public static final String DOCUMENT_PREFIX_NOT_NULL = "ERR2210";
    public static final String DOCUMENT_PREFIXM_CODE_LENGTH_EXCEED = "ERR2211";
    public static final String DOCUMENT_PREFIXM_NAME__LENGTH_EXCEED = "ERR2212";
    public static final String DOCUMENT_PREFIX_NAME__LENGTH_EXCEED = "ERR2213";
    public static final String DOCUMENT_SUFIX_NAME__LENGTH_EXCEED = "ERR2214";
    public static final String DOCUMENT_PREFIXM_ADDFDC_NOT_NULL = "ERR2215";
    public static final String DOCUMENT_PREFIXM_ADDPOR_NOT_NULL = "ERR2216";
    public static final String DOCUMENT_FORMULAE_NOT_NULL = "ERR2217";
    public static final String DOCUMENT_PREFIX_NON_SELECT = "ERR2218";
    public static final String DOCUMENT_DOCPREFIX_ALREADY_EXIST = "ERR2219";
    public static final String DOCUMENT_DOCSUFFIX_ALREADY_EXIST = "ERR2220";
    public static final String DOCUMENT_PREFIX__INVALID = "ERR2221";
    public static final String DOCUMENT_SUFFIX_INVALID = "ERR2222";
    public static final String DOCUMENT_PREFIX_MAPPING_NOT_NULL = "ERR2223";
    public static final String DOCUMENT_PREFIXM_DOCTYPE_NOT_NULL_OR_EMPTY = "ERR2224";
    public static final String DOCUMENT_PREFIXM_DOCTYPE_LENGTH_INVALID = "ERR2225";
    public static final String DOCUMENT_PREFIXM_REPORT_NOT_NULL_OR_EMPTY = "ERR2226";
    public static final String DOCUMENT_PREFIXM_REPORT_LENGTH_INVALID = "ERR2227";
    public static final String DOCUMENT_PREFIXM_LOGO_NOT_NULL_OR_EMPTY = "ERR2228";
    public static final String DOCUMENT_DOCTYPE_ALREADY_EXIST = "ERR2229";
    public static final String DOCUMENT_REPORTNAME_ALREADY_EXIST = "ERR2230";
    public static final String DOCUMENT_PREFIX_MAPPING__ID_NOT_FOUND = "ERR2231";
    public static final String LOCATION_CODE_NOT_NULL_OR_EMPTY = "ERR2232";
    public static final String LOCATION_BLOCK = "ERR2233";
    public static final String LOCATION_HIDE = "ERR2234";
    public static final String DOCUMENT_PREFIX_OR_SUFFIX_REQUIRED = "ERR2235";
    public static final String DOCUMENT_PREFIX_IMAGE_FORMAT = "ERR2236";

    public static final String CHARGE_LIST_EMPTY = "ERR2301";
    public static final String CHARGE_CODE_NOTFOUND = "ERR2302";
    public static final String CHARGE_NAME_NOTFOUND = "ERR2303";
    public static final String CHARGE_CODE_ALREADY_EXIST = "ERR2304";
    public static final String CHARGE_NAME_ALREADY_EXIST = "ERR2305";
    public static final String CHARGE_CODE_INVALID = "ERR2306";
    public static final String CHARGE_NAME_INVALID = "ERR2307";
    public static final String CHARGE_ID_INVALID = "ERR2308";
    public static final String CHARGE_CODE_NOT_NULL = "ERR2309";
    public static final String CHARGE_NAME_NOT_NULL = "ERR2310";
    public static final String CHARGE_CHARGE_TYPE_NOT_NULL = "ERR2311";
    public static final String CHARGE_CALUCULATION_TYPE_NOT_NULL = "ERR2312";
    public static final String CHARGE_LOV_STATUS_NOT_NULL = "ERR2313";
    public static final String CHARGE_ID_NOT_NULL = "ERR2314";
    public static final String CHARGE_CODE_NOT_UPDATABLE = "ERR2315";
    public static final String CHARGE_ID_NOT_FOUND = "ERR2316";
    public static final String CHARGE_ID_ASSOCIATED_FOUND = "ERR2317";
    public static final String CHARGE_CODE_LENGTH_INVALID = "ERR2318";
    public static final String CHARGE_NAME_LENGTH_INVALID = "ERR2319";

    public static final String CHARGE_EDI_TYPE_NOT_NULL = "ERR2320";
    public static final String CHARGE_EDI_VALUE_NOT_NULL = "ERR2321";

    public static final String CHARGE_CHARGE_TYPE_INVALID = "ERR2322";


    public static final String COMMODITY_CODE_ALREADY_EXIST = "ERR2401";
    public static final String COMMODITY_NAME_ALREADY_EXIST = "ERR2402";
    public static final String COMMODITY_CODE_INVALID = "ERR2403";
    public static final String COMMODITY_NAME_INVALID = "ERR2404";
    public static final String COMMODITY_ID_NOT_FOUND = "ERR2405";
    public static final String COMMODITY_CODE_NOT_NULL = "ERR2406";
    public static final String COMMODITY_NAME_NOT_NULL = "ERR2407";
    public static final String COMMODITY_CODE_LENGTH_EXCEED = "ERR2408";
    public static final String COMMODITY_NAME_LENGTH_EXCEED = "ERR2409";
    public static final String COMMODITY_NOT_FOUND = "ERR2410";
    public static final String COMMODITY_LOV_STATUS_NOTNULL = "ERR2411";
    public static final String COMMODITY_NOT_NULL = "ERR2412";


    public static final String DEPARTMENT_CODE_DUPLICATED = "ERR2420";
    public static final String DEPARTMENT_NAME_DUPLICATED = "ERR2421";
    public static final String DEPARTMENT_CODE_REQUIRED = "ERR2422";
    public static final String DEPARTMENT_CODE_INVALID = "ERR2423";
    public static final String DEPARTMENT_NAME_REQUIRED = "ERR2424";
    public static final String DEPARTMENT_NAME_INVALID = "ERR2425";
    public static final String DEPARTMENT_DEPARTMENTHEAD_MANDATORY = "ERR2426";
    public static final String DEPARTMENT_DEPARTMENTHEAD_RESIGNED = "ERR2427";
    public static final String DEPARTMENT_DEPARTMENTHEAD_TERMINATED = "ERR2428";

    public static final String DESIGNATION_CODE_DUPLICATED = "ERR2430";
    public static final String DESIGNATION_NAME_DUPLICATED = "ERR2431";
    public static final String DESIGNATION_CODE_REQUIRED = "ERR2432";
    public static final String DESIGNATION_CODE_INVALID = "ERR2433";
    public static final String DESIGNATION_NAME_REQUIRED = "ERR2434";
    public static final String DESIGNATION_NAME_INVALID = "ERR2435";


    public static final String LANGUAGE_NAME_INVALID = "ERR2501";
    public static final String LANGUAGE_CODE_INVALID = "ERR2502";
    public static final String LANGUAGE_DESC_INVALID = "ERR2503";
    public static final String LANGUAGE_CODE_EXIT = "ERR2504";
    public static final String LANGUAGE_CODE_NOT_NULL = "ERR2505";
    public static final String LANGUAGE_DESC_NOT_NULL = "ERR2506";
    public static final String LANGUAGE_GROUP_NAME_NOT_NULL = "ERR2507";
    public static final String LANGUAGE_GROUP_INVALID = "ERR2508";
    public static final String LANGUAGE_NOT_NULL = "ERR2509";

    public static final String EMAIL_FROM_NOTNULL = "ERR2601";
    public static final String EMAIL_SUBJECT_NOTNULL = "ERR2602";
    public static final String EMAIL_BODY_NOTNULL = "ERR2603";
    public static final String EMAIL_TO_NOTNULL = "ERR2604";
    public static final String EMAIL_CC_NOTNULL = "ERR2605";
    public static final String EMAIL_BCC_NOTNULL = "ERR2606";
    public static final String ERROR_IN_EMAIL_SENDING = "ERR2607";
    public static final String EMAILID_NOT_PRESENT = "ERR2608";
    public static final String EMAILID_NOT_NULL = "ERR2609";
    public static final String INVALID_EMAILID = "ERR2610";
    public static final String FORGOT_PASSWORD_EMAIL_SENT_SUCCESS = "ERR2611";
    public static final String FORGOT_PASSWORD_EMAIL_SENDING = "ERR2612";
    public static final String PASSWORD_NOT_NULL = "ERR2613";
    public static final String RESET_EMAIL_EXPIRED = "ERR2614";
    public static final String EMAIL_SERVER_ADDRESS_REQUIRED = "ERR2615";
    public static final String EMAIL_SERVER_PORT_REQUIRED = "ERR2616";

    public static final String PARTY_TYPE_ID_NOT_NULL = "ERR2701";
    public static final String PARTY_TYPE_ID_NOT_FOUND = "ERR2702";
    public static final String PARTY_TYPE_CODE_ALREADY_EXIST = "ERR2703";
    public static final String PARTY_TYPE_NAME_ALREADY_EXIST = "ERR2704";
    public static final String PARTY_TYPE_CODE_NOT_NULL = "ERR2705";
    public static final String PARTY_TYPE_NAME_NOT_NULL = "ERR2706";
    public static final String PARTY_TYPE_NOT_NULL = "ERR2707";
    public static final String PARTY_TYPE_LOV_STATUS_NOT_NULL = "ERR2708";
    public static final String PARTY_TYPE_NAME_INVALID = "ERR2709";
    public static final String PARTY_TYPE_BLOCK = "ERR2710";
    public static final String PARTY_TYPE_HIDE = "ERR2711";
    public static final String PARTY_TYPE_TYPE_ALREADY_EXIST = "ERR2712";
    public static final String PARTY_TYPE_CODE_INVALID = "ERR2713";

    public static final String STATE_LIST_EMPTY = "ERR3001";
    public static final String STATE_CODE_NOT_FOUND = "ERR3002";
    public static final String STATE_NAME_NOT_FOUND = "ERR3003";
    public static final String STATE_CODE_INVALID = "ERR3004";
    public static final String STATE_CODE_NOT_NULL = "ERR3005";
    public static final String STATE_NAME_INVALID = "ERR3006";
    public static final String STATE_NAME_NOT_NULL = "ERR3007";
    public static final String STATE_CODE_ALREADY_EXIST = "ERR3008";
    public static final String STATE_NAME_ALREADY_EXIST = "ERR3009";
    public static final String STATE_ID_NOT_FOUND = "ERR3010";
    public static final String STATE_COUNTRY_NOT_NULL = "ERR3011";
    public static final String STATE_COUNTRY_INVALID = "ERR3012";
    public static final String STATE_NOT_NULL = "ERR3013";
    public static final String STATE_ID_NOT_NULL = "ERR3014";
    public static final String STATE_LOV_STATUS_NOT_NULL = "ERR3015";
    public static final String STATE_LOV_STATUS_INVALID = "ERR3016";

    public static final String ZONE_LIST_EMPTY = "ERR3101";
    public static final String ZONE_CODE_NOT_FOUND = "ERR3102";
    public static final String ZONE_NAME_NOT_FOUND = "ERR3103";
    public static final String ZONE_CODE_INVALID = "ERR3104";
    public static final String ZONE_CODE_NOT_NULL = "ERR3105";
    public static final String ZONE_NAME_INVALID = "ERR3106";
    public static final String ZONE_NAME_NOT_NULL = "ERR3107";
    public static final String ZONE_CODE_ALREADY_EXIST = "ERR3108";
    public static final String ZONE_NAME_ALREADY_EXIST = "ERR3109";
    public static final String ZONE_ID_NOT_FOUND = "ERR3110";
    public static final String ZONE_COUNTRY_NOT_NULL = "ERR3111";
    public static final String ZONE_COUNTRY_INVALID = "ERR3112";
    public static final String ZONE_NOT_NULL = "ERR3113";
    public static final String ZONE_ID_NOT_NULL = "ERR3114";
    public static final String ZONE_COUNTRY_CODE_NULL = "ERR3115";


    public static final String EMPLOYEE_CODE_ALREADY_EXIST = "ERR3201";
    public static final String EMPLOYEE_CODE_REQUIRED = "ERR3202";
    public static final String EMPLOYEE_CODE_INVALID = "ERR3203";
    public static final String EMPLOYEE_FIRST_NAME_REQUIRED = "ERR3204";
    public static final String EMPLOYEE_MIDDLE_NAME_REQUIRED = "ERR3205";
    public static final String EMPLOYEE_LAST_NAME_REQUIRED = "ERR3206";
    public static final String EMPLOYEE_FIRST_NAME_INVALID = "ERR3207";
    public static final String EMPLOYEE_MIDDLE_NAME_INVALID = "ERR3208";
    public static final String EMPLOYEE_LAST_NAME_INVALID = "ERR3209";
    public static final String EMPLOYEE_EMPLOYMENT_STATUS_REQUIRED = "ERR3210";
    public static final String EMPLOYEE_EMAIL_ADDRESS_INVALID = "ERR3211";
    public static final String EMPLOYEE_PHONE_NUMBER_INVALID = "ERR3212";
    public static final String EMPLOYEE_DOB_REQUIRED = "ERR3213";
    public static final String EMPLOYEE_GENDER_REQUIRED = "ERR3214";
    public static final String EMPLOYEE_NATIONALITY_REQUIRED = "ERR3215";
    public static final String EMPLOYEE_MOBILE_NUMBER_INVALID = "ERR3216";
    public static final String EMPLOYEE_LOCATION_BLOCKED = "ERR3217";
    public static final String EMPLOYEE_LOCATION_HIDDEN = "ERR3218";
    public static final String EMPLOYEE_DEPARTMENT_BLOCKED = "ERR3219";
    public static final String EMPLOYEE_DEPARTMENT_HIDDEN = "ERR3220";
    public static final String EMPLOYEE_DEPARTMENT_HEAD_RESIGNED = "ERR3221";
    public static final String EMPLOYEE_DEPARTMENT_HEAD_TERMINATED = "ERR3222";
    public static final String EMPLOYEE_REPORTING_TO_RESIGNED = "ERR3223";
    public static final String EMPLOYEE_REPORTING_TO_TERMINATED = "ERR3224";
    public static final String EMPLOYEE_DESIGNATION_BLOCKED = "ERR3225";
    public static final String EMPLOYEE_DESIGNATION_HIDDEN = "ERR3226";
    public static final String EMPLOYEE_ALIAS_NAME_REQUIRED = "ERR3227";
    public static final String EMPLOYEE_ALIAS_NAME_INVALID = "ERR3228";
    public static final String EMPLOYEE_EMAIL_REQUIRED = "ERR3229";
    public static final String EMPLOYEE_NATIONALITY_BLOCKED = "ERR3230";
    public static final String EMPLOYEE_NATIONALITY_HIDDEN = "ERR3231";
    public static final String EMPLOYEE_PROBATION_PERIOD = "ERR3232";
    public static final String EMPLOYEE_LOCATION_MANDATORY = "ERR3233";
    public static final String EMPLOYEE_DEPARTMENT_MANDATORY = "ERR3234";
    public static final String EMPLOYEE_DESIGNATION_MANDATORY = "ERR3235";


    public static final String DIVISION_LIST_EMPTY = "ERR3301";
    public static final String DIVISION_ID_NOT_FOUND = "ERR3302";
    public static final String DIVISION_CODE_ALREADY_EXIST = "ERR3303";
    public static final String DIVISION_NAME_ALREADY_EXIST = "ERR3304";
    public static final String DIVISION_ID_NOT_NULL = "ERR3305";
    public static final String DIVISION_COMPANY_INVALID = "ERR3306";
    public static final String DIVISION_CODE_NOT_NULL = "ERR3307";
    public static final String DIVISION_NAME_NOT_NULL = "ERR3308";
    public static final String DIVISION_LOV_STATUS_NOT_NULL = "ERR3309";
    public static final String DIVISION_CODE_INVALID = "ERR3310";
    public static final String DIVISION_NAME_INVALID = "ERR3311";
    public static final String DIVISION_CODE_LENGTH_EXCEED = "ERR3312";
    public static final String DIVISION_NAME_LENGTH_EXCEED = "ERR3313";
    public static final String DIVISION_COMPANY__NOT_NULL = "ERR3314";
    public static final String COMPANY_STATUS_BLOCKED = "ERR3316";
    public static final String COMPANY_STATUS_HIDDEN = "ERR3317";


    public static final String LOCATION_LIST_EMPTY = "ERR3401";
    public static final String LOCATION_ID_NOT_FOUND = "ERR3402";
    public static final String LOCATION_CODE_ALREADY_EXIST = "ERR3403";
    public static final String LOCATION_NAME_ALREADY_EXIST = "ERR3404";
    public static final String LOCATION_ID_NOT_NULL = "ERR3405";
    public static final String LOCATION_COMPANY_INVALID = "ERR3406";
    public static final String LOCATION_CODE_NOT_NULL = "ERR3407";
    public static final String LOCATION_NAME_NOT_NULL = "ERR3408";
    public static final String LOCATION_LOV_STATUS_NOT_NULL = "ERR3409";
    public static final String LOCATION_CODE_INVALID = "ERR3410";
    public static final String LOCATION_NAME_INVALID = "ERR3411";
    public static final String LOCATION_COUNTRY__NOT_NULL = "ERR3412";
    public static final String LOCATION_CITY__NOT_NULL = "ERR3413";
    public static final String LOCATION_ZONE__NOT_NULL = "ERR3414";
    public static final String LOCATION_COMPANY__NOT_NULL = "ERR3415";
    public static final String LOCATION_PARTY__NOT_NULL = "ERR3416";
    public static final String LOCATION_CURRENCY__NOT_NULL = "ERR3417";
    public static final String LOCATION_COSTCENTER__NOT_NULL = "ERR3418";
    public static final String LOCATION_COMPANY_BLOCKED = "ERR3419";
    public static final String LOCATION_COMPANY_HIDDEN = "ERR3420";
    public static final String LOCATION_COUNTRY_BLOCKED = "ERR3421";
    public static final String LOCATION_COUNTRY_HIDDEN = "ERR3422";
    public static final String LOCATION_CITY_BLOCKED = "ERR3423";
    public static final String LOCATION_CITY_HIDDEN = "ERR3424";
    public static final String LOCATION_REGION_BLOCKED = "ERR3425";
    public static final String LOCATION_REGION_HIDDEN = "ERR3426";
    public static final String LOCATION_ZONE_BLOCKED = "ERR3427";
    public static final String LOCATION_ZONE_HIDDEN = "ERR3428";
    public static final String LOCATION_DIVISION_BLOCKED = "ERR3429";
    public static final String LOCATION_DIVISION_HIDDEN = "ERR3430";
    public static final String LOCATION_PARTY_BLOCKED = "ERR3431";
    public static final String LOCATION_PARTY_HIDDEN = "ERR3432";
    public static final String LOCATION_BRANCH_NAME = "ERR3433";

    public static final String LOCATION_ADDRESSLINE1 = "ERR3434";
    public static final String LOCATION_ADDRESSLINE2 = "ERR3435";
    public static final String LOCATION_ADDRESSLINE3 = "ERR3436";
    public static final String LOCATION_TIMEZONE = "ERR3437";
    public static final String LOCATION_DELETE_MESSAGE = "ERR3438";
    public static final String LOCATION_DATE_FORMAT = "ERR3439";
    public static final String LOCATION_DATE_TIME_FORMAT = "ERR3440";

    public static final String LOCATION_CURRENCY_NOT_NULL = "ERR3441";
    public static final String LOCATION_CURRENCY_BLOCKED = "ERR3442";
    public static final String LOCATION_CURRENCY_HIDDEN = "ERR3443";

    public static final String LOCALTION_COUNTRYNO_NUMERIC = "ERR3444";
    public static final String LOCALTION_CITYNO_NUMERIC = "ERR3445";


    public static final String PARTY_CODE_ALREADY_EXIST = "ERR3501";
    public static final String PARTY_CODE_NOT_NULL_OR_EMPTY = "ERR3502";
    public static final String PARTY_CODE_INVALID = "ERR3503";
    public static final String PARTY_CODE_CANNOT_UPDATE = "ERR3504";
    public static final String PARTY_NAME_ALREADY_EXIST = "ERR3505";
    public static final String PARTY_NAME_NOT_NULL_OR_EMPTY = "ERR3506";
    public static final String PARTY_NAME_INVALID = "ERR3507";
    public static final String PARTY_STATUS_INVALID = "ERR3508";
    public static final String PARTY_FIRST_NAME_IS_REQUIRED = "ERR3509";
    public static final String PARTY_LAST_NAME_IS_REQUIRED = "ERR3510";
    public static final String PARTY_FIRST_NAME_INVALID = "ERR3511";
    public static final String PARTY_LAST_NAME_INVALID = "ERR3512";
    public static final String PARTY_SALUTATION_REQUIRED = "ERR3513";
    public static final String PARTY_COUNTRY_REQUIRED = "ERR3514";
    public static final String PARTY_CATEGORY_REQUIRED = "ERR3515";
    public static final String PARTY_BLOCKED_CATEGORY_SELECTED = "ERR3516";
    public static final String PARTY_HIDDEN_CATEGORY_SELECTED = "ERR3517";
    public static final String PARTY_LOV_STATUS_REQUIRED = "ERR3518";
    public static final String PARTY_PARTY_TYPE_NOT_NULL = "ERR3519";
    public static final String PARTY_PARTY_TYPE_BLOCKED_SELECTED = "ERR3520";
    public static final String PARTY_PARTY_TYPE_HIDDEN_SELECTED = "ERR3521";
    public static final String PARTY_COUNTRY_BLOCKED = "ERR3522";
    public static final String PARTY_COUNTRY_HIDDEN = "ERR3523";
    public static final String PARTY_CREDIT_ALREADY_EXIST = "ERR3524";
    public static final String PARTY_NAME_REGIONAL_INVALID = "ERR3525";
    public static final String PARTY_NAME_REGIONAL_REQUIRED = "ERR3526";

    public static final String PARTY_ADDRESS_REQUIRED = "ERR3700";
    public static final String PARTY_ADDRESS_LINE1_REQUIRED = "ERR3701";
    public static final String PARTY_ADDRESS_LINE2_REQUIRED = "ERR3702";
    public static final String PARTY_ADDRESS_LINE1_INVALID = "ERR3703";
    public static final String PARTY_ADDRESS_LINE2_INVALID = "ERR3704";
    public static final String PARTY_ADDRESS_LINE3_INVALID = "ERR3705";
    public static final String PARTY_STATE_REQUIRED = "ERR3706";
    public static final String PARTY_HIDDEN_STATE_SELECTED = "ERR3707";
    public static final String PARTY_BLOCKED_STATE_SELECTED = "ERR3708";
    public static final String PARTY_ZIP_CODE_INVALID = "ERR3709";
    public static final String PARTY_PO_BOX_INVALID = "ERR3710";
    public static final String PARTY_PHONE_NUMBER_INVALID = "ERR3711";
    public static final String PARTY_MOBILE_NUMBER_INVALID = "ERR3712";
    public static final String PARTY_FAX_INVALID = "ERR3713";
    public static final String PARTY_EMAIL_INVALID = "ERR3714";
    public static final String PARTY_CONTACT_PERSON_INVALID = "ERR3715";
    public static final String PARTY_IS_CORPORATE_OFFICE = "ERR3716";
    public static final String PARTY_CITY_REQUIRED = "ERR3717";
    public static final String PARTY_HIDDEN_CITY_SELECTED = "ERR3718";
    public static final String PARTY_BLOCKED_CITY_SELECTED = "ERR3719";

    public static final String PARTY_BANK_DLR_CODE_INVALID = "ERR3720";
    public static final String PARTY_KNOWN_SHIPPER_NUMBER_INVALID = "ERR3721";
    public static final String PARTY_ID_NUMBER_INVALID = "ERR3722";
    public static final String PARTY_ID_NUMBER_REQUIRED = "ERR3723";
    public static final String PARTY_BRANCH_SERIAL_NO_INVALID = "ERR3724";
    public static final String PARTY_WAREHOUSE_PROVIDER_NAME_INVALID = "ERR3725";
    public static final String PARTY_BOND_HOLDER_INVALID = "ERR3726";
    public static final String PARTY_BOND_NUMBER_INVALID = "ERR3727";
    public static final String PARTY_BOND_SURETY_CODE_INVALID = "ERR3728";
    public static final String PARTY_COUNTRY_OF_ISSUANCE_REQUIRED = "ERR3729";
    public static final String PARTY_BLOCKED_COUNTRY_OF_ISSUANCE = "ERR3730";
    public static final String PARTY_HIDDEN_COUNTRY_OF_ISSUANCE = "ERR3731";
    public static final String PARTY_CONTACT_PERSON_REQUIRED = "ERR3732";
    public static final String PARTY_PICK_HOUR_TEXT_INVALID = "ERR3733";
    public static final String PARTY_CONSIGNEE_TYPE_REQUIRED = "ERR3734";

    public static final String PARTY_DOB_REQUIRED = "ERR3735";

    public static final String PARTY_VAT_NUMBER_ALREADY_EXIST = "ERR3741";
    public static final String PARTY_PAN_NUMBER_ALREADY_EXIST = "ERR3742";
    public static final String PARTY_RAC_NUMBER_ALREADY_EXIST = "ERR3743";
    public static final String PARTY_CST_NUMBER_ALREADY_EXIST = "ERR3744";
    public static final String PARTY_SVT_NUMBER_ALREADY_EXIST = "ERR3745";
    public static final String PARTY_SVAT_NUMBER_ALREADY_EXIST = "ERR3746";
    public static final String PARTY_TIN_NUMBER_ALREADY_EXIST = "ERR3747";
    public static final String PARTY_IATA_CODE_ALREADY_EXIST = "ERR3748";
    public static final String PARTY_VAT_NUMBER_INVALID = "ERR3749";
    public static final String PARTY_PAN_NUMBER_INVALID = "ERR3750";
    public static final String PARTY_CST_NUMBER_INVALID = "ERR3751";
    public static final String PARTY_SVT_NUMBER_INVALID = "ERR3752";
    public static final String PARTY_SVAT_NUMBER_INVALID = "ERR3753";
    public static final String PARTY_RAC_NUMBER_INVALID = "ERR3754";
    public static final String PARTY_TIN_NUMBER_INVALID = "ERR3755";
    public static final String PARTY_IATA_CODE_INVALID = "ERR3756";
    public static final String PARTY_NLS_CUSTOMER_NAME_REQUIRED = "ERR3757";
    public static final String PARTY_NLS_CUSTOMER_NAME_INVALID = "ERR3758";
    public static final String PARTY_PARTY_SERVICE_ALREADY_EXIST = "ERR3759";
    public static final String PARTY_PARTY_COMPANY_ALREADY_EXIST = "ERR3760";
    public static final String PARTY_ADDRESS_TYPE_ALREADY_EXIST = "ERR3761";
    public static final String PARTY_TDSPERCENTAGE_INVALID = "ERR3762";
    public static final String PARTY_CCPERCENTAGE_INVALID = "ERR3763";
    public static final String PARTY_CAFPERCENTAGE_INVALID = "ERR3764";
    public static final String PARTY_PAYMENT_SCHEDULE_BLOCKED = "ERR3765";
    public static final String PARTY_PAYMENT_SCHEDULE_HIDDEN = "ERR3766";
    public static final String PARTY_GST_NUMBER_INVALID = "ERR3767";


    public static final String TRIGGER_NAME_NOT_NULL = "ERR06609";
    public static final String TRIGGER_NAME_LENGTH_EXCEED = "ERR06607";
    public static final String TRIGGER_NAME_INVALID = "ERR06602";
    public static final String TRIGGER_CODE_NOT_NULL = "ERR06609";
    public static final String TRIGGER_CODE_LENGTH_EXCEED = "ERR06608";
    public static final String TRIGGER_CODE_INVALID = "ERR06603";
    public static final String TRIGGER_MAILSUBJECT_NOT_NULL = "ERR06603";
    public static final String TRIGGER_ID_NOT_NULL = "ERR06611";
    public static final String TRIGGER_TYPE_STATUS_BLOCK = "ERR06612";
    public static final String TRIGGER_TYPE_STATUS_HIDDEN = "ERR06613";
    public static final String TRIGGER_CODE_ALREADY_EXIST = "ERR06605";
    public static final String TRIGGER_NAME_ALREADY_EXIST = "ERR06606";


    public static final String TRIGGER_TYPE_CODE_ALREADY_EXIST = "ERR06505";
    public static final String TRIGGER_TYPE_NAME_ALREADY_EXIST = "ERR06506";
    public static final String TRIGGER_TYPE_CODE_INVALID = "ERR06503";
    public static final String TRIGGER_TYPE_ID_NOT_NULL = "ERR3604";
    public static final String TRIGGER_TYPE_NAME_INVALID = "ERR06502";
    public static final String TRIGGER_TYPE_CODE_NOT_NULL = "ERR3606";
    public static final String TRIGGER_TYPE_NAME_NOT_NULL = "ERR06500";
    public static final String TRIGGER_TYPE_ID_NOT_FOUND = "ERR3608";
    public static final String TRIGGER_TYPE_NOT_NULL = "ERR3609";
    public static final String TRIGGER_TYPE_STATUS_NOT_NULL = "ERR3610";
    public static final String TRIGGER_TYPE_CODE_LENGTH_EXCEED = "ERR3611";
    public static final String TRIGGER_TYPE_NAME_LENGTH_EXCEED = "ERR3612";
    public static final String TRIGGER_TYPE_LIST_EMPTY = "ERR3613";

    public static final String MASTER_EVENT_NOT_FOUND = "ERR3701-2";
    public static final String MASTER_EVENT_CODE = "ERR3702-2";
    public static final String MASTER_EVENT_NAME = "ERR3703-2";
    public static final String MASTER_EVENT_ALREADY_CODE = "ERR3704-2";
    public static final String MASTER_EVENT_ALREADY_NAME = "ERR3705-2";
    public static final String MASTER_EVENT_CODE_INVALID = "ERR3706-2";
    public static final String MASTER_EVENT_NAME_INVALID = "ERR3707-2";
    public static final String MASTER_EVENT_STATUS = "ERR3708-2";
    public static final String MASTER_EVENT_TYPE = "ERR3709-2";


    public static final String PARTYSERVICE_ID_INVALID = "ERR6001";
    public static final String PARTYSERVICE_CODE_NOT_NULL = "ERR6002";
    public static final String PARTYSERVICE_NAME_NOT_NULL = "ERR6003";
    public static final String PARTYSERVICE_CODE_ALREADY_EXIST = "ERR6004";
    public static final String PARTYSERVICE_NAME_ALREADY_EXIST = "ERR6005";
    public static final String PARTYSERVICE_LIST_EMPTY = "ERR6006";
    public static final String PARTYSERVICE_ID_NOT_NULL = "ERR6007";
    public static final String PARTY_SERVICE_REQUIRED = "ERR6008";
    public static final String PARTY_SERVICE_LOCATION_REQUIRED = "ERR6009";
    public static final String PARTY_SERVICE_SALESMAN_REQUIRED = "ERR6010";
    public static final String PARTY_SERVICE_CUSTOMERSERVICE_REQUIRED = "ERR6011";
    public static final String PARTY_COMPANY_REQUIRED = "ERR6012";
    public static final String ACCOUNT_LIST_EMPTY = "ERR6013";

    public static final String PARTY_ACCOUNT_CURRENCY_REQUIRED = "ERR6014";
    public static final String PARTY_ACCOUNT_LOCATION_REQUIRED = "ERR6015";
    public static final String PARTY_ACCOUNT_REQUIRED = "ERR6016";
    public static final String PARTY_ACCOUNT_LOV_STATUS_NOT_NULL = "ERR6017";
    public static final String PARTY_BILLING_SUBLEGER_REQUIRED = "ERR6018";
    public static final String PARTY_BILLING_SUBLEGER_ACCOUNTCODE_REQUIRED = "ERR6019";

    public static final String PARTY_CREDITLIMIT_PUBLICCREDITDAYS_NOT_ZERO = "ERR6020";
    public static final String PARTY_CREDITLIMIT_CREDITAMOUNT_NOT_ZERO = "ERR6021";
    public static final String PARTY_CREDIT_CREDITDAYS_INVALID = "ERR6022";
    public static final String PARTY_CREDIT_PUBLICCREDITDAYS_INVALID = "ERR6023";
    public static final String PARTY_CREDIT_CREDITAMOUNT_INVALID = "ERR6024";
    public static final String PARTY_CREDITLIMIT_REQUIRED = "ERR6025";
    public static final String PARTY_CREDITLIMIT_CREDITDAYS_NOT_ZERO = "ERR6026";
    public static final String PARTY_CREDITLIMIT_OR_CREDITDAYS_REQUIRED = "ERR6027";

    public static final String PARTY_ACCOUNT_LOCATION_BLOCKED = "ERR6028";
    public static final String PARTY_ACCOUNT_LOCATION_HIDDEN = "ERR6029";
    public static final String PARTY_BILLING_SUBLEGER_HIDDEN = "ERR6030";
    public static final String PARTY_BILLING_SUBLEGER_BLOCKED = "ERR6031";
    public static final String PARTY_BILLING_ACCOUNT_BLOCKED = "ERR6032";
    public static final String PARTY_BILLING_ACCOUNT_HIDDEN = "ERR6033";
    public static final String PARTY_CREDIT_SERVICE_OR_DIVISION_REQUIRED = "ERR6034";

    public static final String PARTY_SERVICE_AND_LOCATION_ALREADY_EXIST = "ERR6035";
    public static final String PARTY_COMPANY_AND_DIVISION_ALREADY_EXIST = "ERR6036";
    public static final String PARTY_CREDIT_LIMIT_ALREADY_EXIST = "ERR6037";
    public static final String PARTY_DUPLICATES_CANNOT_BE_ALLOWED = "ERR6038";
    public static final String PARTY_TERMCODE_IS_NOTNULL = "ERR6039";
    public static final String PARTY_CAF_PERCENTAGE_NOTNULL = "ERR6040";
    public static final String PARTY_CAF_PERCENTAGE_INVALID = "ERR6041";
    public static final String PARTY_CC_PERCENTAGE_NOTNULL = "ERR6042";
    public static final String PARTY_CC_PERCENTAGE_INVALID = "ERR6043";
    public static final String PARTY_TDS_PERCENTAGE_NOTNULL = "ERR6044";
    public static final String PARTY_TDS_PERCENTAGE_INVALID = "ERR6045";
    public static final String PARTY_ADDRESS_TYPE = "ERR6046";
    public static final String PARTY_WAREHOUSE_NAME_IS_INVALID = "ERR6047";
    public static final String PARTY_PARTY_CONTACT_NAME_NOTNULL = "ERR6048";
    public static final String PARTY_SHIPPER_VALIDATION_NUMBER_IS_INVALID = "ERR6049";
    public static final String PARTY_DUPLICATE_ADDRESS_TYPE = "ERR6050";
    public static final String PARTY_SALESMAN_IS_NOTNULL = "ERR6051";
    public static final String PARTY_SERVICE_OR_DIVISION_REQUIRED = "ERR6052";
    public static final String ORIGIN_COUNTRY_REQUIRED = "ERR6053";
    public static final String ORIGIN_COUNTRY_STATUS_BLOCK = "ERR6054";
    public static final String ORIGIN_COUNTRY_STATUS_HIDDEN = "ERR6055";
    public static final String DESTINATION_COUNTRY_REQUIRED = "ERR6056";
    public static final String DESTINATION_COUNTRY_STATUS_BLOCK = "ERR6057";
    public static final String DESTINATION_COUNTRY_STATUS_HIDDEN = "ERR6058";
    public static final String DUPLICATE_EMAIL = "ERR6059";
    public static final String PARTY_ACCOUNT_ALREADY_EXIST = "ERR6060";

    public static final String PARTY_BUSINESS_SERVICE_BLOCKED = "ERR6061";
    public static final String PARTY_BUSINESS_ORIGIN_PORT_BLOCKED = "ERR6062";
    public static final String PARTY_BUSINESS_ORIGIN_DESTINATION_DIFFERENT = "ERR6063";
    public static final String PARTY_BUSINESS_DESTINATION_BLOCKED = "ERR6064";
    public static final String PARTY_BUSINESS_NOTES_INVALID = "ERR6065";
    public static final String PARTY_BUSINESS_COMMODITY_BLOCKED = "ERR6066";
    public static final String PARTY_BUSINESS_TOS_BLOCKED = "ERR6067";
    public static final String PARTY_BUSINESS_PERIOD_BLOCKED = "ERR6068";
    public static final String PARTY_CORPORATE_ADDRESS_ALREADY_EXIST = "ERR6069";
    public static final String PARTY_BUSINESS_SERVICE_HIDE = "ERR6070";
    public static final String PARTY_BUSINESS_TOS_HIDE = "ERR6071";
    public static final String PARTY_BUSINESS_ORIGIN_PORT_HIDE = "ERR6072";
    public static final String PARTY_BUSINESS_DESTINATION_HIDE = "ERR6073";
    public static final String PARTY_BUSINESS_COMMODITY_HIDE = "ERR6074";
    public static final String PARTY_BUSINESS_PERIOD_HIDE = "ERR6075";
    public static final String PARTY_BUSINESS_INVALID = "ERR6076";
    public static final String PARTY_SERVICE_INVALID = "ERR6082";
    public static final String PARTY_DIVISION_INVALID = "ERR6078";
    public static final String PARTY_ACCOUNTS_INVALID = "ERR6079";
    public static final String PARTY_CREDIT_INVALID = "ERR6080";
    public static final String PARTY_ACCOUNT_STATUS_REQUIRED = "ERR6081";
    public static final String PARTY_EMAIL_DUPLICATE_IMPORT = "ERR6077";
    public static final String PARTY_ACCOUNT_GL_SUB_LEDGER_NOT_ENABLED = "ERR6104";

    public static final String PARTY_BUSINESS_SERVICE_REQUIRED = "ERR6100";
    public static final String ESTIMATED_REVENUE_INVALID = "ERR6101";
    public static final String BUSINESS_NOOFUNITS_INVALID = "ERR6102";
    public static final String BUSINESS_NOOFSHIPMENTS_INVALID = "ERR6103";
    public static final String PARTY_BUSINESS_DUPLICATE = "ERR6083";
    public static final String PARTY_CREDIT_DUPLICATE = "ERR6084";

    public static final String PARTY_CONTCT_OFFICE_EMAIL = "ERR6085";
    public static final String PARTY_CONTCT_PERSONAL_EMAIL = "ERR6086";
    public static final String PARTY_EMAIL_ALREADY_EXIST = "ERR6087";
    public static final String PARTY_ADDRESS_COUNT = "ERR6088";
    public static final String PARTY_DEFAULTER = "ERR6089";
    public static final String PARTY_BUSINESS_UNIT_BLOCKED = "ERR6090";
    public static final String PARTY_BUSINESS_UNIT_HIDDEN = "ERR6091";

    public static final String PARTY_ACC_NUM_INVALID = "ERR6092";//added new column in acc number in party detail
    public static final String PARTY_PIN_INVALID = "ERR6093";
    public static final String PARTY_TSANO_INVALID = "ERR6094";
    public static final String PARTY_SPOTNO_INVALID = "ERR6095";
    public static final String PARTY_CUSTOMER_NOT_PROVIDED = "ERR6097";
    public static final String ENQUIRY_CUSTOMER_NOT_PROVIDED = "ERR6098";


    public static final String ENQUIRYLOG_NOT_NULL = "ERR7000";
    public static final String ENQUIRYLOG_ID_NOT_NULL = "ERR7001";
    public static final String ENQUIRYLOG_ID_INVALID = "ERR7002";
    public static final String ENQUIRYLOG_PARTY_NOT_NULL = "ERR7003";
    public static final String ENQUIRYLOG_ENQUIRYNO_NOT_NULL = "ERR7004";
    public static final String ENQUIRYLOG_QUOTEDATE_NOT_NULL = "ERR7005";
    public static final String ENQUIRYLOG_RECEIVEDON_NOT_NULL = "ERR7006";
    public static final String ENQUIRYLOG_STATUS_NOT_NULL = "ERR7007";
    public static final String ENQUIRYLOG_QUOTATION_NO_NOT_NULL = "ERR7008";
    public static final String ENQUIRYLOG_SALES_CO_ORDINATOR_NOT_NULL = "ERR7009";
    public static final String ENQUIRYLOG_CREATEDON_NOT_NULL = "ERR7011";
    public static final String ENQUIRYLOG_CREATED_BY_NOT_NULL = "ERR7012";
    public static final String ENQUIRYLOG_LIST_EMPTY = "ERR7013";
    public static final String ENQUIRYLOG_DETAIL_LIST_EMPTY = "ERR7014";
    public static final String ENQUIRYLOG_COMMODITYGROUPCODE_LIST_EMPTY = "ERR7015";

    public static final String SELECTED_PARTY_IS_BLOCKED = "ERR7016";
    public static final String LOGGED_ON_DATE_IS_MANDATORY = "ERR7017";
    public static final String LOGGED_BY_EMPLYOEE_NAME_IS_MANDATORY = "ERR7018";
    public static final String SELECTED_LOGGED_ON_WAS_BLOCKED = "ERR7019";
    public static final String SELECTED_SALES_COORDINATOR_IS_BLOCKED = "ERR7020";
    public static final String SELECTED_SALESMAN_IS_BLOCKED = "ERR7021";
    public static final String QUOTE_BY_DATE_IS_MANDATORY = "ERR7022";
    public static final String SELECTED_SERVICE_BLOCKED = "ERR7023";
    public static final String NOTES_IS_INVALID = "ERR7024";

    public static final String ATLEAST_ONE_DIMENSION = "ERR7999";
    public static final String ENQUIRYLOG_UID_INVALID = "ERR8000";
    public static final String ENQUIRYLOG_UID_NOT_NULL = "ERR8001";
    public static final String ENQUIRYLOG_UID_ALREADY_EXIST = "ERR8002";
    public static final String ENQUIRYLOG_UID_NOT_EMPTY = "ERR8003";

    public static final String ENQUIRYLOG_DATE_INVALID = "ERR8020";
    public static final String ENQUIRYLOG_DATE_NOT_NULL = "ERR8021";
    public static final String ENQUIRYLOG_DATE_ALREADY_EXIST = "ERR8022";
    public static final String ENQUIRYLOG_DATE_NOT_EMPTY = "ERR8023";

    public static final String ENQUIRYLOG_LOGGEDBY_INVALID = "ERR8040";
    public static final String ENQUIRYLOG_LOGGEDBY_NOT_NULL = "ERR8041";
    public static final String ENQUIRYLOG_LOGGEDBY_ALREADY_EXIST = "ERR8042";
    public static final String ENQUIRYLOG_LOGGEDBY_NOT_EMPTY = "ERR8043";

    public static final String ENQUIRYLOG_PARTYCODE_INVALID = "ERR8060";
    public static final String ENQUIRYLOG_PARTYCODE_NOT_NULL = "ERR8061";
    public static final String ENQUIRYLOG_PARTYCODE_ALREADY_EXIST = "ERR8062";
    public static final String ENQUIRYLOG_PARTYCODE_NOT_EMPTY = "ERR8063";

    public static final String ENQUIRYLOG_SALESCOORDINATOR_INVALID = "ERR8080";
    public static final String ENQUIRYLOG_SALESCOORDINATOR_NOT_NULL = "ERR8081";
    public static final String ENQUIRYLOG_SALESCOORDINATOR_ALREADY_EXIST = "ERR8082";
    public static final String ENQUIRYLOG_SALESCOORDINATOR_NOT_EMPTY = "ERR8083";

    public static final String ENQUIRYLOG_SALESMAN_INVALID = "ERR8100";
    public static final String ENQUIRYLOG_SALESMAN_NOT_NULL = "ERR8101";
    public static final String ENQUIRYLOG_SALESMAN_ALREADY_EXIST = "ERR8102";
    public static final String ENQUIRYLOG_SALESMAN_NOT_EMPTY = "ERR8103";

    public static final String ENQUIRYLOG_RECEIVEDDATE_INVALID = "ERR8120";
    public static final String ENQUIRYLOG_RECEIVEDDATE_NOT_NULL = "ERR8121";
    public static final String ENQUIRYLOG_RECEIVEDDATE_ALREADY_EXIST = "ERR8122";
    public static final String ENQUIRYLOG_RECEIVEDDATE_NOT_EMPTY = "ERR8123";

    public static final String ENQUIRYLOG_SERVICECODE_INVALID = "ERR8140";
    public static final String ENQUIRYLOG_SERVICECODE_NOT_NULL = "ERR8141";
    public static final String ENQUIRYLOG_SERVICECODE_ALREADY_EXIST = "ERR8142";
    public static final String ENQUIRYLOG_SERVICECODE_NOT_EMPTY = "ERR8143";

    public static final String ENQUIRYLOG_TOSCODE_INVALID = "ERR8160";
    public static final String ENQUIRYLOG_TOSCODE_NOT_NULL = "ERR8161";
    public static final String ENQUIRYLOG_TOSCODE_ALREADY_EXIST = "ERR8162";
    public static final String ENQUIRYLOG_TOSCODE_NOT_EMPTY = "ERR8163";

    public static final String ENQUIRYLOG_PORCODE_INVALID = "ERR8180";
    public static final String ENQUIRYLOG_PORCODE_NOT_NULL = "ERR8181";
    public static final String ENQUIRYLOG_PORCODE_ALREADY_EXIST = "ERR8182";
    public static final String ENQUIRYLOG_PORCODE_NOT_EMPTY = "ERR8183";

    public static final String ENQUIRYLOG_FDCCODE_INVALID = "ERR8200";
    public static final String ENQUIRYLOG_FDCCODE_NOT_NULL = "ERR8201";
    public static final String ENQUIRYLOG_FDCCODE_ALREADY_EXIST = "ERR8202";
    public static final String ENQUIRYLOG_FDCCODE_NOT_EMPTY = "ERR8203";

    public static final String ENQUIRYLOG_COMMODITYGROUPCODE_INVALID = "ERR8220";
    public static final String ENQUIRYLOG_COMMODITYGROUPCODE_NOT_NULL = "ERR8221";
    public static final String ENQUIRYLOG_COMMODITYGROUPCODE_ALREADY_EXIST = "ERR8222";
    public static final String ENQUIRYLOG_COMMODITYGROUPCODE_NOT_EMPTY = "ERR8223";

    public static final String ENQUIRYLOG_CLEARANCE_INVALID = "ERR8240";
    public static final String ENQUIRYLOG_CLEARANCE_NOT_NULL = "ERR8241";
    public static final String ENQUIRYLOG_CLEARANCE_ALREADY_EXIST = "ERR8242";
    public static final String ENQUIRYLOG_CLEARANCE_NOT_EMPTY = "ERR8243";

    public static final String ENQUIRYLOG_DELIVERY_INVALID = "ERR8260";
    public static final String ENQUIRYLOG_DELIVERY_NOT_NULL = "ERR8261";
    public static final String ENQUIRYLOG_DELIVERY_ALREADY_EXIST = "ERR8262";
    public static final String ENQUIRYLOG_DELIVERY_NOT_EMPTY = "ERR8263";

    public static final String ENQUIRYLOG_PICKUPPLACE_INVALID = "ERR8280";
    public static final String ENQUIRYLOG_PICKUPPLACE_NOT_NULL = "ERR8281";
    public static final String ENQUIRYLOG_PICKUPPLACE_ALREADY_EXIST = "ERR8282";
    public static final String ENQUIRYLOG_PICKUPPLACE_NOT_EMPTY = "ERR8283";

    public static final String ENQUIRYLOG_DELIVERYPLACE_INVALID = "ERR8300";
    public static final String ENQUIRYLOG_DELIVERYPLACE_NOT_NULL = "ERR8301";
    public static final String ENQUIRYLOG_DELIVERYPLACE_ALREADY_EXIST = "ERR8302";
    public static final String ENQUIRYLOG_DELIVERYPLACE_NOT_EMPTY = "ERR8303";

    public static final String ENQUIRYLOG_HAZARDOUS_INVALID = "ERR8320";
    public static final String ENQUIRYLOG_HAZARDOUS_NOT_NULL = "ERR8321";
    public static final String ENQUIRYLOG_HAZARDOUS_ALREADY_EXIST = "ERR8322";
    public static final String ENQUIRYLOG_HAZARDOUS_NOT_EMPTY = "ERR8323";

    public static final String ENQUIRYLOG_DIMENSION_INVALID = "ERR8340";
    public static final String ENQUIRYLOG_DIMENSION_NOT_NULL = "ERR8341";
    public static final String ENQUIRYLOG_DIMENSION_ALREADY_EXIST = "ERR8342";
    public static final String ENQUIRYLOG_DIMENSION_NOT_EMPTY = "ERR8343";

    public static final String ENQUIRYLOG_GROSSWEIGHT_INVALID = "ERR8360";
    public static final String ENQUIRYLOG_GROSSWEIGHT_NOT_NULL = "ERR8361";
    public static final String ENQUIRYLOG_GROSSWEIGHT_ALREADY_EXIST = "ERR8362";
    public static final String ENQUIRYLOG_GROSSWEIGHT_NOT_EMPTY = "ERR8363";
    public static final String ENQUIRYLOG_VOLUMEWEIGHT_INVALID = "ERR8364";

    public static final String ENQUIRYLOG_PIECES_INVALID = "ERR8380";
    public static final String ENQUIRYLOG_PIECES_NOT_NULL = "ERR8381";
    public static final String ENQUIRYLOG_PIECES_ALREADY_EXIST = "ERR8382";
    public static final String ENQUIRYLOG_PIECES_NOT_EMPTY = "ERR8383";

    public static final String ENQUIRYLOG_LENGTH_INVALID = "ERR8400";
    public static final String ENQUIRYLOG_LENGTH_NOT_NULL = "ERR8401";
    public static final String ENQUIRYLOG_LENGTH_ALREADY_EXIST = "ERR8402";
    public static final String ENQUIRYLOG_LENGTH_NOT_EMPTY = "ERR8403";

    public static final String ENQUIRYLOG_WIDTH_INVALID = "ERR8420";
    public static final String ENQUIRYLOG_WIDTH_NOT_NULL = "ERR8421";
    public static final String ENQUIRYLOG_WIDTH_ALREADY_EXIST = "ERR8422";
    public static final String ENQUIRYLOG_WIDTH_NOT_EMPTY = "ERR8423";

    public static final String ENQUIRYLOG_HEIGHT_INVALID = "ERR8460";
    public static final String ENQUIRYLOG_HEIGHT_NOT_NULL = "ERR8461";
    public static final String ENQUIRYLOG_HEIGHT_ALREADY_EXIST = "ERR8462";
    public static final String ENQUIRYLOG_HEIGHT_NOT_EMPTY = "ERR8463";

    public static final String ENQUIRYLOG_DIMENSIONCOLUMN_INVALID = "ERR8480";
    public static final String ENQUIRYLOG_DIMENSIONCOLUMN_NOT_NULL = "ERR8481";
    public static final String ENQUIRYLOG_DIMENSIONCOLUMN_ALREADY_EXIST = "ERR8482";
    public static final String ENQUIRYLOG_DIMENSIONCOLUMN_NOT_EMPTY = "ERR8483";

    public static final String ENQUIRYLOG_DIMENSIONVALUE_INVALID = "ERR8500";
    public static final String ENQUIRYLOG_DIMENSIONVALUE_NOT_NULL = "ERR8501";
    public static final String ENQUIRYLOG_DIMENSIONVALUE_ALREADY_EXIST = "ERR8502";
    public static final String ENQUIRYLOG_DIMENSIONVALUE_NOT_EMPTY = "ERR8503";

    public static final String ENQUIRYLOG_CONTAINER_INVALID = "ERR8520";
    public static final String ENQUIRYLOG_CONTAINER_NOT_NULL = "ERR8501";
    public static final String ENQUIRYLOG_CONTAINER_ALREADY_EXIST = "ERR8522";
    public static final String ENQUIRYLOG_CONTAINER_NOT_EMPTY = "ERR8523";

    public static final String ENQUIRYLOG_CONTAINER_NAME_INVALID = "ERR8525";
    public static final String ENQUIRYLOG_CONTAINER_NAME_NOT_NULL = "ERR826";
    public static final String ENQUIRYLOG_CONTAINER_NAME_ALREADY_EXIST = "ERR8527";
    public static final String ENQUIRYLOG_CONTAINER_NAME_NOT_EMPTY = "ERR8528";

    public static final String ENQUIRYLOG_CONTAINER_CODE_INVALID = "ERR8530";
    public static final String ENQUIRYLOG_CONTAINER_CODE_NOT_NULL = "ERR831";
    public static final String ENQUIRYLOG_CONTAINER_CODE_ALREADY_EXIST = "ERR8532";
    public static final String ENQUIRYLOG_CONTAINER_CODE_NOT_EMPTY = "ERR8533";

    public static final String ENQUIRYLOG_CONTAINER_NOOFCONTAINER_INVALID = "ERR8535";
    public static final String ENQUIRYLOG_CONTAINER_NOOFCONTAINER_NOT_NULL = "ERR836";
    public static final String ENQUIRYLOG_CONTAINER_NOOFCONTAINER_ALREADY_EXIST = "ERR8537";
    public static final String ENQUIRYLOG_CONTAINER_NOOFCONTAINER_NOT_EMPTY = "ERR8538";

    public static final String ENQUIRYLOG_AUTOMAIL_INVALID = "ERR8560";
    public static final String ENQUIRYLOG_AUTOMAIL_NOT_NULL = "ERR8561";
    public static final String ENQUIRYLOG_AUTOMAIL_ALREADY_EXIST = "ERR8562";
    public static final String ENQUIRYLOG_AUTOMAIL_NOT_EMPTY = "ERR8563";

    public static final String ENQUIRYLOG_REMARKS_INVALID = "ERR8580";
    public static final String ENQUIRYLOG_REMARKS_NOT_NULL = "ERR8581";
    public static final String ENQUIRYLOG_REMARKS_ALREADY_EXIST = "ERR8582";
    public static final String ENQUIRYLOG_REMARKS_NOT_EMPTY = "ERR8583";

    public static final String ENQUIRYLOG_NO_INVALID = "ERR8600";
    public static final String ENQUIRYLOG_NO_NOT_NULL = "ERR8601";
    public static final String ENQUIRYLOG_NO_ALREADY_EXIST = "ERR8602";
    public static final String ENQUIRYLOG_NO_NOT_EMPTY = "ERR8603";


    public static final String ENQUIRYLOG_QUOTATIONNO_INVALID = "ERR8740";
    public static final String ENQUIRYLOG_QUOTATIONNO_NOT_NULL = "ERR8741";
    public static final String ENQUIRYLOG_QUOTATIONNO_ALREADY_EXIST = "ERR8742";
    public static final String ENQUIRYLOG_QUOTATIONNO_NOT_EMPTY = "ERR8743";

    public static final String ENQUIRYLOG_VOLUME_INVALID = "ERR8760";
    public static final String ENQUIRYLOG_VOLUME_NOT_NULL = "ERR8761";
    public static final String ENQUIRYLOG_VOLUME_ALREADY_EXIST = "ERR8762";
    public static final String ENQUIRYLOG_VOLUME_NOT_EMPTY = "ERR8763";

    public static final String ENQUIRYLOG_QUOTEBY_INVALID = "ERR8780";
    public static final String ENQUIRYLOG_QUOTEBY_NOT_NULL = "ERR8781";
    public static final String ENQUIRYLOG_QUOTEBY_ALREADY_EXIST = "ERR8782";
    public static final String ENQUIRYLOG_QUOTEBY_NOT_EMPTY = "ERR8783";
    public static final String ENQUIRY_DIMENSION_PIECES_INVALID = "ERR8784";
    public static final String ENQUIRY_DIMENSION_LENGTH_INVALID = "ERR8785";
    public static final String ENQUIRY_DIMENSION_GROSSWEIGHT_INVALID = "ERR8786";
    public static final String ENQUIRY_DIMENSION_WIDTH_INVALID = "ERR8787";
    public static final String ENQUIRY_DIMENSION_HEIGHT_INVALID = "ERR8788";
    public static final String ENQUIRY_DIMENSION_VOLWEIGHT_INVALID = "ERR8789";

    public static final String ENQUIRYLOG_SERVICE_NOT_NULL = "ERR8790";
    public static final String ENQUIRYLOG_TOS_NOT_NULL = "ERR8791";

    public static final String ENQUIRYLOG_FROMPORT_NOT_NULL = "ERR8792";
    public static final String ENQUIRYLOG_TOPORT_NOT_NULL = "ERR8793";
    public static final String ENQUIRYLOG_COMMODITY_NOT_NULL = "ERR8794";
    public static final String ENQUIRYLOG_PICKUP_ADDRESS_INVALID = "ERR8795";
    public static final String ENQUIRYLOG_DELIVERY_ADDRESS_INVALID = "ERR8796";
    public static final String UK_ENQDET_SERVICE = "ERR8797";
    public static final String ENQUIRYLOG_ORIGIN_PORT_IS_HIDDEN = "ERR8798";
    public static final String ENQUIRYLOG_ORIGIN_PORT_IS_BLOCKED = "ERR8799";
    public static final String ENQUIRYLOG_DESTINATION_PORT_IS_HIDDEN = "ERR8800";
    public static final String ENQUIRYLOG_DESTINATION_PORT_IS_BLOCKED = "ERR8801";
    public static final String ENQUIRYLOG_TOS_IS_BLOCKED = "ERR8802";
    public static final String ENQUIRYLOG_ORIGIN_DESTINATION_PORT_DIFFERENT = "ERR8803";
    public static final String ENQUIRYLOG_COMMODITY_IS_BLOCKED = "ERR8804";
    public static final String ENQUIRYLOG_TOS_HIDDEN = "ERR8805";
    public static final String ENQUIRYLOG_LOGGEDON_RECEIVEDON = "ERR8806";
    public static final String ENQUIRYLOG_QUOTEBY_RECEIVEDON = "ERR8807";
    public static final String ENQUIRYLOG_DUPLICATE_SERVICE = "ERR8808";
    public static final String ENQUIRYLOG_DELIVERY_ADDRESS_REQUIRED = "ERR8809";
    public static final String ENQUIRYLOG_TOS_REQUIRED = "ERR8810";
    public static final String ENQUIRYLOG_QUOTEBY_BEFORE_LOGGED_ON = "ERR8811";
    public static final String ENQUIRYLOG_QUOTEBY_BEFORE_RECEIVED_ON = "ERR8812";
    public static final String ENQUIRYLOG_LOCATION = "ERR8813";
    public static final String ENQUIRYLOG_COMPANY = "ERR8814";
    public static final String ENQUIRYLOG_COUNTRY = "ERR8815";


    public static final String ENQUIRYLOG_VENDOR_PARTY_MANDATORY = "ERR8816";
    public static final String ENQUIRYLOG_VENDOR_PARTY_BLOCK = "ERR8817";
    public static final String ENQUIRYLOG_VENDOR_PARTY_HIDDEN = "ERR8818";
    public static final String ENQUIRYLOG_VENDOR_EMAIL_INVALID = "ERR8819";
    public static final String ENQUIRYLOG_VENDOR_REQDATE_INVALID = "ERR8820";
    public static final String ENQUIRYLOG_VENDOR_RECEIVED_ON_INVALID = "ERR8821";
    public static final String ENQUIRYLOG_VENDOR_RATE_ACCEPTED_DUPLICATE = "ERR8822";

    public static final String ENQUIRYLOG_VENDOR_CHARGE_MANDATORY = "ERR8823";
    public static final String ENQUIRYLOG_VENDOR_CHARGE_BLOCK = "ERR8824";
    public static final String ENQUIRYLOG_VENDOR_CHARGE_HIDDEN = "ERR8825";
    public static final String ENQUIRYLOG_VENDOR_CHARGE_NAME_MANDATORY = "ERR8826";

    public static final String ENQUIRYLOG_VENDOR_UNIT_MANDATORY = "ERR8827";
    public static final String ENQUIRYLOG_VENDOR_UNIT_BLOCK = "ERR8828";
    public static final String ENQUIRYLOG_VENDOR_UNIT_HIDDEN = "ERR8829";
    public static final String ENQUIRYLOG_VENDOR_BUYRATE_INVALID = "ERR8830";


    public static final String ENQUIRYLOG_VENDOR_RATE_CHARGE_INVALID = "ERR8831";
    public static final String ENQUIRYLOG_VENDOR_REQUEST_RATE_INVALID = "ERR8832";


    public static final String ENQUIRYLOG_VENDOR_CURRENCY_MANDATORY = "ERR8833";
    public static final String ENQUIRYLOG_VENDOR_CURRENCY_BLOCK = "ERR8834";
    public static final String ENQUIRYLOG_VENDOR_CURRENCY_HIDDEN = "ERR8835";

    public static final String ENQUIRYLOG_QUOTE_CANNOT_CREATE = "ERR8836";
    public static final String PIN_IS_MANDATORY = "ERR8837";
    public static final String PIN_IS_INVALID = "ERR8838";

    public static final String CHARGE_LIST_IS_MANDATORY = "ERR8839";
    public static final String SOME_THING_WENT_WRONG = "ERR8840";
    public static final String CHARGE_SAVED_SUCCESSFULLY = "ERR8841";
    public static final String VENDOR_EMAIL_MANDATORY = "ERR8842";


    /* Airline Edi Error Messages */
    public static final String AIRLINE_EDI_MAWB_NO_REQUIRED = "ERR9000";
    public static final String AIRLINE_EDI_MAWB_DATE_REQUIRED = "ERR9001";
    public static final String AIRLINE_EDI_ORIGIN_REQUIRED = "ERR9002";
    public static final String AIRLINE_EDI_DESTINATION_REQUIRED = "ERR9003";
    public static final String AIRLINE_EDI_POL_REQUIRED = "ERR9004";
    public static final String AIRLINE_EDI_POD_REQUIRED = "ERR9005";
    public static final String AIRLINE_EDI_CARRIER_REQUIRED = "ERR9006";
    public static final String AIRLINE_EDI_FLIGHT_NO_REQUIRED = "ERR9007";
    public static final String AIRLINE_EDI_FLIGHT_NO_INVALID = "ERR9008";
    public static final String AIRLINE_EDI_NO_OF_PIECES_REQUIRED = "ERR9009";
    public static final String AIRLINE_EDI_VOLUME_WEIGHT_REQUIRED = "ERR9010";
    public static final String AIRLINE_EDI_GROSS_WEIGHT_REQUIRED = "ERR9011";
    public static final String AIRLINE_EDI_CHARGEABLE_WEIGHT_REQUIRED = "ERR9012";
    public static final String AIRLINE_EDI_SHIPPER_REQUIRED = "ERR9013";
    public static final String AIRLINE_EDI_SHIPPER_ADDRESS_REQUIRED = "ERR9014";
    public static final String AIRLINE_EDI_CONSIGNEE_REQUIRED = "ERR9015";
    public static final String AIRLINE_EDI_CONSIGNEE_ADDRESS_REQUIRED = "ERR9016";
    public static final String AIRLINE_EDI_COMMODITY_DESCRIPTION_REQUIRED = "ERR9017";
    public static final String AIRLINE_EDI_IS_DIRECT_CONFIRMATION_MESSAGE = "ERR9018";
    public static final String AIRLINE_EDI_AGENT_REQUIRED = "ERR9019";
    public static final String AIRLINE_EDI_AGENT_ADDRESS_REQUIRED = "ERR9020";

    public static final String AIRLINE_EDI_SHIPPER_CITY_REQUIRED = "ERR9022";
    public static final String AIRLINE_EDI_AGENT_CITY_REQUIRED = "ERR9023";
    public static final String AIRLINE_EDI_CONSIGNEE_CITY_REQUIRED = "ERR9024";
    public static final String AIRLINE_EDI_HOUSE_COMMODITY_REQUIRED = "ERR9025";
    public static final String AIRLINE_EDI_HOUSE_SHIPPER_COUNTRY_REQUIRED = "ERR9026";
    public static final String AIRLINE_EDI_HOUSE_CONSIGNEE_COUNTRY_REQUIRED = "ERR9027";
    public static final String AIRLINE_EDI_AGENT_ACCOUNT_REQUIRED = "ERR9028";

    public static final String AIRLINE_EDI_ISSAGENT_REQUIRED = "ERR9029";
    public static final String AIRLINE_EDI_IATA_CONS_REQUIRED = "ERR9030";
    public static final String AIRLINE_EDI_IATA_AGENT_REQUIRED = "ERR9031";
    public static final String AIRLINE_EDI_IATA_ISSAGENT_REQUIRED = "ERR9032";
    public static final String AIRLINE_EDI_INFO_SAVE = "ERR9033";
    public static final String AIRLINE_EDI_HOUSE_SHIPPER_ADDRESS_REQUIRED = "ERR9034";
    public static final String AIRLINE_EDI_HOUSE_SHIPPER_CITY_REQUIRED = "ERR9035";
    public static final String AIRLINE_EDI_HOUSE_CONSIGNEE_ADDRESS_REQUIRED = "ERR9036";
    public static final String AIRLINE_EDI_HOUSE_CONSIGNEE_CITY_REQUIRED = "ERR9037";
    public static final String AIRLINE_EDI_HOUSE_AGENT_ADDRESS_REQUIRED = "ERR9038";
    public static final String AIRLINE_EDI_HOUSE_AGENT_CITY_REQUIRED = "ERR9039";
    public static final String AIRLINE_EDI_HOUSE_AGENT_COUNTRY_REQUIRED = "ERR9040";
    public static final String AIRLINE_EDI_HOUSE_SHIPPER_REQUIRED = "ERR9041";
    public static final String AIRLINE_EDI_HOUSE_CONSIGNEE_REQUIRED = "ERR9042";
    public static final String AIRLINE_EDI_HOUSE_AGENT_REQUIRED = "ERR9043";
    public static final String AIRLINE_EDI_CARRIER_NOT_ALLOWED = "ERR9044";


    /*EDI Response Messages*/
    public static final String EDI_STATUS_PROCESS_INITIATED = "ERR9991";
    public static final String EDI_STATUS_PROCESS_INITIATED_FAILED = "ERR9992";
    public static final String EDI_STATUS_SENT_TO_QUEUE = "ERR9993";
    public static final String EDI_STATUS_QUEUE_FAILURE = "ERR9994";
    public static final String EDI_STATUS_LOCAL_FILE_CONFIG_MISSING = "ERR9995";
    public static final String EDI_STATUS_SUCCESS = "ERR9996";
    public static final String EDI_STATUS_FTP_UPLOAD_FAILURE = "ERR9997";
    public static final String EDI_STATUS_FTP_CONFIGURATION_MISSING = "ERR9998";


    public static final String AUTOMAIL_NOT_NULL = "ERR10000";

    public static final String AUTOMAIL_ID_INVALID = "ERR10001";
    public static final String AUTOMAIL_ID_NOT_NULL = "ERR10002";
    public static final String AUTOMAIL_ID_ALREADY_EXIST = "ERR10003";
    public static final String AUTOMAIL_ID_NOT_EMPTY = "ERR10004";

    public static final String AUTOMAIL_MESSAGENAME_INVALID = "ERR10010";
    public static final String AUTOMAIL_MESSAGENAME_NOT_NULL = "ERR10011";
    public static final String AUTOMAIL_MESSAGENAME_ALREADY_EXIST = "ERR10012";
    public static final String AUTOMAIL_MESSAGENAME_NOT_EMPTY = "ERR10013";

    public static final String AUTOMAIL_MESSAGECODE_INVALID = "ERR10020";
    public static final String AUTOMAIL_MESSAGECODE_NOT_NULL = "ERR10021";

    public static final String AUTOMAIL_MESSAGECODE_ALREADY_EXIST = "ERR10022";
    public static final String AUTOMAIL_MESSAGECODE_NOT_EMPTY = "ERR10023";

    public static final String AUTOMAIL_MESSAGESENDTYPE_INVALID = "ERR10030";
    public static final String AUTOMAIL_MESSAGESENDTYPE_NOT_NULL = "ERR10031";
    public static final String AUTOMAIL_MESSAGESENDTYPE_ALREADY_EXIST = "ERR10032";
    public static final String AUTOMAIL_MESSAGESENDTYPE_NOT_EMPTY = "ERR10033";

    public static final String AUTOMAIL_STATUS_INVALID = "ERR10040";
    public static final String AUTOMAIL_STATUS_NOT_NULL = "ERR10041";
    public static final String AUTOMAIL_STATUS_ALREADY_EXIST = "ERR10042";
    public static final String AUTOMAIL_STATUS_NOT_EMPTY = "ERR10043";

    public static final String AUTOMAIL_MESSAGEGROUPID_INVALID = "ERR10050";
    public static final String AUTOMAIL_MESSAGEGROUPID_NOT_NULL = "ERR10051";
    public static final String AUTOMAIL_MESSAGEGROUPID_ALREADY_EXIST = "ERR10052";
    public static final String AUTOMAIL_MESSAGEGROUPID_NOT_EMPTY = "ERR10053";

    public static final String AUTOMAIL_EVENTTYPE_NOT_NULL = "ERR10054";
    public static final String AUTOMAIL_EVENTTYPE_INVALID = "ERR10055";

    public static final String AUTOMAILGROUP_NOT_NULL = "ERR11000";

    public static final String AUTOMAILGROUP_ID_INVALID = "ERR11001";
    public static final String AUTOMAILGROUP_ID_NOT_NULL = "ERR11002";
    public static final String AUTOMAILGROUP_ID_ALREADY_EXIST = "ERR11003";
    public static final String AUTOMAILGROUP_ID_NOT_EMPTY = "ERR11004";

    public static final String AUTOMAILGROUP_MESSAGEGROUPCODE_INVALID = "ERR11020";
    public static final String AUTOMAILGROUP_MESSAGEGROUPCODE_NOT_NULL = "ERR11021";
    public static final String AUTOMAILGROUP_MESSAGEGROUPCODE_ALREADY_EXIST = "ERR11022";
    public static final String AUTOMAILGROUP_MESSAGEGROUPCODE_NOT_EMPTY = "ERR11023";

    public static final String AUTOMAILGROUP_MESSAGEGROUPNAME_INVALID = "ERR11040";
    public static final String AUTOMAILGROUP_MESSAGEGROUPNAME_NOT_NULL = "ERR11041";
    public static final String AUTOMAILGROUP_MESSAGEGROUPNAME_ALREADY_EXIST = "ERR11042";
    public static final String AUTOMAILGROUP_MESSAGEGROUPNAME_NOT_EMPTY = "ERR11043";
    public static final String AUTOMAILGROUP_AUTOMAIL_MAPPING_INVALID = "ERR11044";

    public static final String QUOTATION_LIST_EMPTY = "ERR22000";
    public static final String QUOTATION_ID_INVALID = "ERR22001";
    public static final String QUOTATION_ID_NOT_NULL = "ERR22002";
    public static final String QUOTATION_DATE_NOT_NULL = "ERR22003";
    public static final String QUOTATION_VALID_FROM_NOT_NULL = "ERR22004";
    public static final String QUOTATION_VALID_TO_NOT_NULL = "ERR22005";
    public static final String QUOTATION_CUSTOMER_NOT_NULL = "ERR22006";
    public static final String QUOTATION_CUSTOMER_DEFAULTER = "ERR22161";
    public static final String QUOTATION_SHIPPER_DEFAULTER = "ERR22162";
    public static final String QUOTATION_ATLEAST_ONE_CHARGE_REQUIRED = "ERR22163";
    public static final String QUOTATION_CUSTOMER_BLOCKED = "ERR22007";
    public static final String QUOTATION_CUSTOMER_HIDE = "ERR22008";
    public static final String QUOTATION_SHIPPER_NOT_NULL = "ERR22009";
    public static final String QUOTATION_SHIPPER_BLOCKED = "ERR22010";
    public static final String QUOTATION_SHIPPER_HIDE = "ERR22011";

    public static final String QUOTATION_SALESMAN_BLOCKED = "ERR22012";
    public static final String QUOTATION_SALESMAN_HIDE = "ERR22013";
    public static final String QUOTATION_SALESMAN_INVALID = "ERR22014";
    public static final String QUOTATION_SALESMAN_RESIGNED = "ERR22015";
    public static final String QUOTATION_SALESMAN_TERMINATED = "ERR22016";

    public static final String QUOTATION_SALES_COORDINATOR_REQUIRED = "ERR22017";
    public static final String QUOTATION_SALESCOORDINATOR_BLOCKED = "ERR22018";
    public static final String QUOTATION_SALESCOORDINATOR_HIDE = "ERR22019";
    public static final String QUOTATION_SALES_COORDINATOR_RESIGNED = "ERR22020";
    public static final String QUOTATION_SALES_COORDINATOR_TERMINATED = "ERR22021";

    public static final String QUOTATION_NUMBER_ALREADY_EXIST = "ERR22022";
    public static final String QUOTATION_APPROVED_BY_NOT_NULL = "ERR22023";
    public static final String QUOTATION_APPROVED_DATE_NOT_NULL = "ERR22024";
    public static final String QUOTATION_HEADER_NOTE_INVALID = "ERR22025";
    public static final String QUOTATION_FOOTER_NOTE_INVALID = "ERR22026";

    public static final String QUOTATION_CUSTOMER_ADDRESS_LINE1_REQUIRED = "ERR22027";
    public static final String QUOTATION_CUSTOMER_ADDRESS_LINE2_REQUIRED = "ERR22028";
    public static final String QUOTATION_CUSTOMER_CITY_STATE_REQUIRED = "ERR22029";

    public static final String QUOTATION_SHIPPER_ADDRESS_LINE1_REQUIRED = "ERR22030";
    public static final String QUOTATION_SHIPPER_ADDRESS_LINE2_REQUIRED = "ERR22031";
    public static final String QUOTATION_SHIPPER_CITY_STATE_REQUIRED = "ERR22032";

    public static final String QUOTATION_LOGGED_BY_USER_BLOCKED = "ERR22033";
    public static final String QUOTATION_LOGGED_BY_USER_HIDE = "ERR22034";
    public static final String QUOTATION_LOGGED_BY_DATE_REQUIRED = "ERR22035";
    public static final String QUOTATION_LOGGED_BY_USER_RESIGNED = "ERR22036";
    public static final String QUOTATION_LOGGED_BY_USER_TERMINATED = "ERR22037";

    public static final String QUOTATION_ATTENTION_REQUIRED = "ERR22038";
    public static final String QUOTATION_ATTENTION_INVALID = "ERR22039";

    public static final String QUOTATION_SERVICE_BLOCKED = "ERR22040";
    public static final String QUOTATION_SERVICE_HIDDEN = "ERR22041";

    public static final String QUOTATION_PORT_OF_ORIGIN_REQUIRED = "ERR22042";
    public static final String QUOTATION_PORT_OF_ORIGIN_BLOCKED = "ERR22043";
    public static final String QUOTATION_PORT_OF_ORIGIN_HIDDEN = "ERR22044";
    public static final String QUOTATION_PORT_OF_DESTINATION_REQUIRED = "ERR22045";
    public static final String QUOTATION_PORT_OF_DESTINATION_BLOCKED = "ERR22046";
    public static final String QUOTATION_PORT_OF_DESTINATION_HIDDEN = "ERR22047";
    public static final String QUOTATION_ORIGIN_DESTINATION_SAME = "ERR22048";

    public static final String QUOTATION_CARRIER_BLOCKED = "ERR22049";
    public static final String QUOTATION_CARRIER_HIDDEN = "ERR22050";

    public static final String QUOTATION_TOS_REQUIRED = "ERR22051";
    public static final String QUOTATION_TOS_BLOCKED = "ERR22052";
    public static final String QUOTATION_TOS_HIDDEN = "ERR22053";

    public static final String QUOTATION_COMMODITY_BLOCKED = "ERR22054";
    public static final String QUOTATION_COMMODITY_HIDDEN = "ERR22055";

    public static final String QUOTATION_CHARGEABLE_WEIGHT_REQUIRED = "ERR22056";
    public static final String QUOTATION_CHARGEABLE_WEIGHT_NEGATIVE = "ERR22057";

    public static final String QUOTATION_GROSS_WEIGHT_REQUIRED = "ERR22058";
    public static final String QUOTATION_GROSS_WEIGHT_NEGATIVE = "ERR22059";

    public static final String QUOTATION_VOLUME_WEIGHT_REQUIRED = "ERR22060";
    public static final String QUOTATION_VOLUME_WEIGHT_NEGATIVE = "ERR22061";

    public static final String QUOTATION_DIMENSION_PIECES_REQUIRED = "ERR22062";
    public static final String QUOTATION_DIMENSION_PIECES_NEGATIVE = "ERR22063";

    public static final String QUOTATION_DIMENSION_LENGTH_REQUIRED = "ERR22064";
    public static final String QUOTATION_DIMENSION_LENGTH_NEGATIVE = "ERR22065";

    public static final String QUOTATION_DIMENSION_WIDTH_REQUIRED = "ERR22066";
    public static final String QUOTATION_DIMENSION_WIDTH_NEGATIVE = "ERR22067";

    public static final String QUOTATION_DIMENSION_HEIGHT_REQUIRED = "ERR22068";
    public static final String QUOTATION_DIMENSION_HEIGHT_NEGATIVE = "ERR22069";

    public static final String QUOTATION_DIMENSION_GROSS_WEIGHT_REQUIRED = "ERR22070";
    public static final String QUOTATION_DIMENSION_GROSS_WEIGHT_NEGATIVE = "ERR22071";

    public static final String QUOTATION_CHARGE_REQUIRED = "ERR22072";
    public static final String QUOTATION_CHARGE_BLOCKED = "ERR22073";
    public static final String QUOTATION_CHARGE_HIDDEN = "ERR22074";

    public static final String QUOTATION_CHARGE_NAME_REQUIRED = "ERR22075";
    public static final String QUOTATION_CHARGE_NAME_INVALID = "ERR22076";

    public static final String QUOTATION_CURRENCY_REQUIRED = "ERR22077";
    public static final String QUOTATION_CURRENCY_BLOCKED = "ERR22078";
    public static final String QUOTATION_CURRENCY_HIDDEN = "ERR22079";

    public static final String QUOTATION_UNIT_REQUIRED = "ERR22080";
    public static final String QUOTATION_UNIT_BLOCKED = "ERR22081";
    public static final String QUOTATION_UNIT_HIDDEN = "ERR22082";

    public static final String QUOTATION_CHARGE_MAX_VALUE_REQUIRED = "ERR22083";
    public static final String QUOTATION_CHARGE_MIN_VALUE_REQUIRED = "ERR22084";
    public static final String QUOTATION_CHARGE_MIN_MAX_MISMATCH = "ERR22085";
    public static final String QUOTATION_CHARGE_MIN_VALUE_ZERO = "ERR22086";
    public static final String QUOTATION_CHARGE_MAX_VALUE_ZERO = "ERR22087";
    public static final String QUOTATION_CHARGE_MIN_VALUE_NEGATIVE = "ERR22088";
    public static final String QUOTATION_CHARGE_MAX_VALUE_NEGATIVE = "ERR22089";

    public static final String QUOTATION_LOGGED_BY_USER_REQUIRED = "ERR22090";
    public static final String QUOTATION_CUSTOMER_ADDRESS_LINE_1_IS_INVALID = "ERR22091";
    public static final String QUOTATION_CUSTOMER_ADDRESS_LINE_2_IS_INVALID = "ERR22092";
    public static final String QUOTATION_CUSTOMER_ADDRESS_LINE_3_IS_INVALID = "ERR22093";
    public static final String QUOTATION_CUSTOMER_ADDRESS_LINE_4_IS_INVALID = "ERR22094";
    public static final String QUOTATION_CUSTOMER_ADDRESS_LINE_4_IS_MANDATORY = "ERR22095";

    public static final String QUOTATION_SHIPER_ADDRESS_LINE_1_IS_INVALID = "ERR22096";
    public static final String QUOTATION_SHIPPER_ADDRESS_LINE_2_IS_INVALID = "ERR22097";
    public static final String QUOTATION_SHIPPER_ADDRESS_LINE_3_IS_INVALID = "ERR22098";
    public static final String QUOTATION_SHIPER_ADDRESS_LINE_4_IS_INVALID = "ERR22099";
    public static final String QUOTATION_SHIPER_ADDRESS_LINE_4_IS_MANDATORY = "ERR22100";
    public static final String QUOTATION_LOGGED_ON_IS_MANDATORY = "ERR22101";
    public static final String QUOTATION_HEADER_NOTE_IS_MANDATORY = "ERR22102";
    public static final String QUOTATION_FOOTER_NOTE_IS_MANDATORY = "ERR22103";
    public static final String QUOTATION_GENERAL_NOTE_IS_MANDATORY = "ERR22104";

    public static final String QUOTATION_PICECES_IS_INVALID = "ERR22105";
    public static final String QUOTATION_LENGTH_IS_INVALID = "ERR22106";
    public static final String QUOTATION_WIDTH_IS_INVALID = "ERR22107";
    public static final String QUOTATION_HEIGHT_IS_INVALID = "ERR22108";
    public static final String QUOTATION_GROSSWEIGHT_IS_INVALID = "ERR22109";
    public static final String QUOTATION_TRANSIT_DAY_IS_INVALID = "ERR22110";
    public static final String QUOTATION_CHARGEBLE_WEIGHT_IS_INVALID = "ERR22111";
    public static final String QUOTATION_VOLUME_WEIGHT_IS_INVALID = "ERR22112";
    public static final String QUOTATION_CHARGE_MINIMUM_UNIT_CANNOT_ZERO = "ERR22113";
    public static final String QUOTATION_CHARGE_MINIMUM_UNIT_INVALID = "ERR22114";
    public static final String QUOTATION_CHARGE_MAXIMUM_UNIT_CANNOT_ZERO = "ERR22115";
    public static final String QUOTATION_CHARGE_MAXIMUM_UNIT_INVALID = "ERR22116";
    public static final String QUOTATION_SELL_RATE_AMOUNT_PER_UNIT_CANNOT_ZERO = "ERR22117";
    public static final String QUOTATION_SELL_RATE_AMOUNT_PER_UNIT_INVALID = "ERR22118";
    public static final String QUOTATION_SELL_RATE_MINIMUM_AMOUNT_CANNOT_ZERO = "ERR22119";
    public static final String QUOTATION_SELL_RATE_MINIMUM_AMOUNT_INVALID = "ERR22120";

    public static final String QUOTATION_BUY_RATE_COST_PER_UNIT_CANNOT_ZERO = "ERR22121";
    public static final String QUOTATION_BUY_RATE_COST_PER_UNIT_INVALID = "ERR22122";
    public static final String QUOTATION_BUY_RATE_MINIMUM_COST_CANNOT_ZERO = "ERR22123";
    public static final String QUOTATION_BUY_RATE_MINIMUM_COST_INVALID = "ERR22124";
    public static final String QUOTATION_NOTES_INVALID = "ERR22125";
    public static final String QUOTATION_HEADER_NOTES_INVALID = "ERR22126";
    public static final String QUOTATION_FOOTER_NOTES_INVALID = "ERR22127";
    public static final String QUOTATION_GENERAL_NOTES_INVALID = "ERR22128";
    public static final String QUOTATION_AIR_NOTES_INVALID = "ERR22129";
    public static final String QUOTATION_FROMDATE_LOGGEDONDATE = "ERR22130";
    public static final String QUOTATION_FROMDATE_TODATE = "ERR22131";

    public static final String QUOTATION_SELL_RATE_AMOUNT_PER_UNIT_REQUIRED = "ERR22132";
    public static final String QUOTATION_SELL_RATE_MINIMUM_AMOUNT_REQUIRED = "ERR22133";
    public static final String QUOTATION_BUY_RATE_COST_PER_UNIT_REQUIRED = "ERR22134";
    public static final String QUOTATION_BUY_RATE_MINIMUM_COST_REQUIRED = "ERR22135";
    public static final String QUOTATION_FROM_TO_INVALD_COPY_QOUTE = "ERR22136";

    public static final String QUOTATION_ALREADY_EXISTS = "ERR22137";

    public static final String QUOTATION_TRANSIT_DAYS_ABOVE_RANGE = "ERR22138";
    public static final String QUOTATION_TRANSIT_DAYS_NEGATIVE = "ERR22139";

    public static final String QUOTATION_FREQUENCY_BLOCKED = "ERR22140";
    public static final String QUOTATION_FREQUENCY_HIDDEN = "ERR22141";


    // EnquiryAttachment Error Codes and QuotationAttachment Error Codes are same....
    public static final String ENQUIRY_ATTACHMENT_REFNO_NOT_NULL = "ERR22142";
    public static final String ENQUIRY_ATTACHMENT_FILE_NAME_NOT_NULL = "ERR22143";
    public static final String ENQUIRY_ATTACHMENT_FILE_NOT_NULL = "ERR22144";
    public static final String ENQUIRY_REFERENCE_ALREADY_EXIST = "ERR22145";
    public static final String ENQUIRY_ATTACHMENT_FILE_EMPTY = "ERR22148";
    public static final String ENQUIRY_ATTACHMENT_FILE_SIZE_EXCEED = "ERR22146";
    public static final String ENQUIRY_ATTACHMENT_REF_NO_INVALID = "ERR22147";


    public static final String QUOTATION_REF_NO_NOT_NULL = "ERR22142";
    public static final String QUOTATION_FILE_NAME_NOT_NULL = "ERR22143";
    public static final String QUOTATION_FILE_NOT_NULL = "ERR22144";
    public static final String QUOTATION_REFERENCE_ALREADY_EXIST = "ERR22145";
    public static final String QUOTATION_FILE_SIZE_EXCEED = "ERR22146";
    public static final String QUOTATION_ATTACHMENT_REF_NO_INVALID = "ERR22147";
    public static final String QUOTATION_FILE_EMPTY = "ERR22148";
    public static final String QUOTATION_PARTY_VALID_FROM_TO = "ERR22149";

    public static final String QUOTATION_LOCATION = "ERR22152";
    public static final String QUOTATION_COMPANY = "ERR22153";
    public static final String QUOTATION_COUNTRY = "ERR22154";
    public static final String QUOTATION_LOGGED_ON_LESS_THAN_VALID_FROM = "ERR22155";
    public static final String QUOTATION_NUMBER_REQUIRED = "ERR22156";
    public static final String QUOTATION_IS_NOT_AVAILABLE = "ERR22157";

    public static final String QUOTATION_CUSTOMER_EMAIL_REQUIRED = "ERR22159";
    public static final String QUOTATION_EMPLOYEE_EMAIL_REQUIRED = "ERR22160";
    public static final String QUOTATION_ALREADY_CREATED_FROM_ENQUIRY = "ERR23101";
    public static final String QUOTATION_GET_DEFAULT_AIR_NOTES = "ERR23102";


    public static final String LEDGER_LIST_EMPTY = "ERR30000";

    public static final String PARTYEMAILMESSAGEMAPPING_NOT_NULL = "ERR50000";

    public static final String PARTYEMAILMESSAGEMAPPING_ID_INVALID = "ERR50001";
    public static final String PARTYEMAILMESSAGEMAPPING_ID_NOT_NULL = "ERR50002";
    public static final String PARTYEMAILMESSAGEMAPPING_ID_ALREADY_EXIST = "ERR50003";
    public static final String PARTYEMAILMESSAGEMAPPING_ID_NOT_EMPTY = "ERR50004";

    public static final String PARTYEMAILMAPPING_NOT_NULL = "ERR60000";

    public static final String PARTYEMAILMAPPING_ID_INVALID = "ERR60001";
    public static final String PARTYEMAILMAPPING_ID_NOT_NULL = "ERR60002";
    public static final String PARTYEMAILMAPPING_ID_ALREADY_EXIST = "ERR60003";
    public static final String PARTYEMAILMAPPING_ID_NOT_EMPTY = "ERR60004";
    public static final String PARTYEMAILMESSAGEMAPPING_LIST_EMPTY = "ERR60005";

    public static final String PARTYEMAILMAPPING_EMAIL_INVALID = "ERR60020";
    public static final String PARTYEMAILMAPPING_EMAIL_NOT_NULL = "ERR60021";
    public static final String PARTYEMAILMAPPING_EMAIL_ALREADY_EXIST = "ERR60022";
    public static final String PARTYEMAILMAPPING_EMAIL_NOT_EMPTY = "ERR60023";

    public static final String EMAILMAPPING_EMAIL_INVALID = "ERR60024";
    public static final String EMAILMAPPING_EMAIL_NOT_NULL = "ERR60025";
    public static final String EMAILMAPPING_EMPTY_MAILGROUP = "ERR60026";

    public static final String PARTY_EMAIL_DESTINATION_COUNTRY_BLOCKED = "ERR60027";
    public static final String PARTY_EMAIL_DESTINATION_COUNTRY_HIDDEN = "ERR60028";
    public static final String PARTY_EMAIL_ORIGIN_COUNTRY_BLOCKED = "ERR60029";
    public static final String PARTY_EMAIL_ORIGIN_COUNTRY_HIDDEN = "ERR60030";
    public static final String PARTY_EMAIL_LOCATION_BLOCKED = "ERR60031";
    public static final String PARTY_EMAIL_LOCATION_HIDDEN = "ERR60032";

    public static final String LOGOMASTER_NOT_NULL = "ERR80001";
    ;
    public static final String LOGOMASTER_LOCATION = "ERR80002";
    public static final String LOGOMASTER_LOGO = "ERR80003";
    public static final String UK_LOGO_LOCATION = "ERR80004";
    public static final String ENQUIRY_RATEREQUEST_CHARGE_UNIQUE = "ERR80005";


    public static final String SHIPMENT_SHIPMENT_REQ_DATE = "ERR90001";
    public static final String SHIPMENT_PARTY = "ERR90002";
    public static final String SHIPMENT_PARTY_DEFAULTER = "ERR90584";
    public static final String SHIPMENT_PARTY_BLOCK = "ERR90003";
    public static final String SHIPMENT_PARTY_HIDE = "ERR90004";
    public static final String SHIPMENT_PARTY_ADDRESS_1 = "ERR90005";
    public static final String SHIPMENT_PARTY_ADDRESS_1_LENGTH = "ERR90006";
    public static final String SHIPMENT_PARTY_ADDRESS_2 = "ERR90007";
    public static final String SHIPMENT_PARTY_ADDRESS_2_LENGTH = "ERR90008";
    public static final String SHIPMENT_PARTY_ADDRESS_3_LENGTH = "ERR90009";
    public static final String SHIPMENT_PARTY_ADDRESS_4 = "ERR90010";
    public static final String SHIPMENT_PARTY_ADDRESS_4_LENGTH = "ERR90011";
    public static final String SHIPMENT_PARTY_ADDRESS = "ERR90012";
    public static final String SHIPMENT_ORIGIN = "ERR90013";
    public static final String SHIPMENT_DESTINATION = "ERR90014";
    public static final String SHIPMENT_TOS = "ERR90015";
    public static final String SHIPMENT_TOS_BLOCK = "ERR90016";
    public static final String SHIPMENT_TOS_HIDE = "ERR90017";
    public static final String SHIPMENT_PROJECT_BLOCK = "ERR90018";
    public static final String SHIPMENT_PROJECT_HIDE = "ERR90019";
    public static final String SHIPMENT_PROJECT = "ERR90020";
    public static final String SHIPMENT_COMMODITY = "ERR90021";
    public static final String SHIPMENT_COMMODITY_BLOCK = "ERR90022";
    public static final String SHIPMENT_COMMODITY_HIDE = "ERR90023";
    public static final String SHIPMENT_SALESMAN = "ERR90024";
    public static final String SHIPMENT_SALESMAN_RESIGNED = "ERR90025";
    public static final String SHIPMENT_SALESMAN_TERMINATED = "ERR90026";
    public static final String SHIPMENT_SALESMAN_ISSALESMAN = "ERR90027";
    public static final String SHIPMENT_AGENT = "ERR90028";
    public static final String SHIPMENT_AGENT_BLOCK = "ERR90029";
    public static final String SHIPMENT_AGENT_HIDE = "ERR90030";
    public static final String SHIPMENT_SERVICE = "ERR90031";
    public static final String SHIPMENT_SERVICE_BLOCK = "ERR90032";
    public static final String SHIPMENT_SERVICE_HIDE = "ERR90033";
    public static final String SHIPMENT_CUSTOMER_SERVICE = "ERR90034";
    public static final String SHIPMENT_CUSTOMER_SERVICE_RESIGNED = "ERR90035";
    public static final String SHIPMENT_CUSTOMER_SERVICE_TERMINATED = "ERR90036";
    public static final String SHIPMENT_ORIGIN_BLOCK = "ERR90037";
    public static final String SHIPMENT_ORIGIN_HIDE = "ERR90038";
    public static final String SHIPMENT_DESTINATION_BLOCK = "ERR90039";
    public static final String SHIPMENT_DESTINATION_HIDE = "ERR90040";
    public static final String SHIPMENT_POL = "ERR90041";
    public static final String SHIPMENT_POL_BLOCK = "ERR90042";
    public static final String SHIPMENT_POL_HIDE = "ERR90043";
    public static final String SHIPMENT_POD = "ERR90044";
    public static final String SHIPMENT_POD_BLOCK = "ERR90045";
    public static final String SHIPMENT_POD_HIDE = "ERR90046";
    public static final String SHIPMENT_COLOADER = "ERR90047";
    public static final String SHIPMENT_COLOADER_BLOCK = "ERR90048";
    public static final String SHIPMENT_COLOADER_HIDE = "ERR90049";
    public static final String SHIPMENT_COMPANY = "ERR90050";
    public static final String SHIPMENT_COMPANY_BLOCK = "ERR90051";
    public static final String SHIPMENT_COMPANY_HIDE = "ERR90052";
    public static final String SHIPMENT_DIVISION = "ERR90053";
    public static final String SHIPMENT_DIVISION_BLOCK = "ERR90054";
    public static final String SHIPMENT_DIVISION_HIDE = "ERR90055";
    public static final String SHIPMENT_PACK = "ERR90056";
    public static final String SHIPMENT_PACK_BLOCK = "ERR90057";
    public static final String SHIPMENT_PACK_HIDE = "ERR90058";
    public static final String SHIPMENT_NO_OF_PIECES = "ERR90059";
    public static final String SHIPMENT_GROSS_WEIGHT = "ERR90060";
    public static final String SHIPMENT_VOLUME_WEIGHT = "ERR90061";
    public static final String SHIPMENT_CHARGEABLE_WEIGHT = "ERR90062";
    public static final String SHIPMENT_CARRIER = "ERR90063";
    public static final String SHIPMENT_CARRIER_BLOCK = "ERR90064";
    public static final String SHIPMENT_CARRIER_HIDE = "ERR90065";
    public static final String SHIPMENT_FLIGHT_NO = "ERR90066";
    public static final String SHIPMENT_ROUTE_NO = "ERR90067";
    public static final String SHIPMENT_MAWB_NO = "ERR90068";

    public static final String SHIPMENT_CREATED_BY = "ERR90069";
    public static final String SHIPMENT_CREATED_BY_RESIGED = "ERR90070";
    public static final String SHIPMENT_CREATED_BY_TERMINATED = "ERR90071";

    public static final String SHIPMENT_ORIGIN_AIR_MODE = "ERR90072";
    public static final String SHIPMENT_ORIGIN_OCEAN_MODE = "ERR90073";
    public static final String SHIPMENT_ORIGIN_RAIL_MODE = "ERR90074";
    public static final String SHIPMENT_ORIGIN_ROAD_MODE = "ERR90075";

    public static final String SHIPMENT_DESTINATION_AIR_MODE = "ERR90076";
    public static final String SHIPMENT_DESTINATION_OCEAN_MODE = "ERR90077";
    public static final String SHIPMENT_DESTINATION_RAIL_MODE = "ERR90078";
    public static final String SHIPMENT_DESTINATION_ROAD_MODE = "ERR90079";

    public static final String SHIPMENT_POL_AIR_MODE = "ERR90080";
    public static final String SHIPMENT_POL_OCEAN_MODE = "ERR90081";
    public static final String SHIPMENT_POL_RAIL_MODE = "ERR90082";
    public static final String SHIPMENT_POL_ROAD_MODE = "ERR90083";

    public static final String SHIPMENT_POD_AIR_MODE = "ERR90084";
    public static final String SHIPMENT_POD_OCEAN_MODE = "ERR90085";
    public static final String SHIPMENT_POD_RAIL_MODE = "ERR90086";
    public static final String SHIPMENT_POD_ROAD_MODE = "ERR90087";

    public static final String SHIPMENT_ORIGIN_DESTINATION_EQUAL = "ERR90088";

    public static final String SHIPMENT_SERVICE_ORIGIN_MODE = "ERR90089";
    public static final String SHIPMENT_SERVICE_DESTINATION_MODE = "ERR90090";
    public static final String SHIPMENT_SERVICE_POL_MODE = "ERR90091";
    public static final String SHIPMENT_SERVICE_POD_MODE = "ERR90092";

    public static final String SHIPMENT_POD_POL_EQUAL = "ERR90093";

    public static final String SHIPMENT_ETD_NOT_NULL = "ERR90094";
    public static final String SHIPMENT_ETA_NOT_NULL = "ERR90095";
    public static final String SHIPMENT_ETD_BEFORE_ETA = "ERR90096";
    public static final String SHIPMENT_PIECES_LESS_ZERO = "ERR90097";
    public static final String SHIPMENT_GROSS_LESS_ZERO = "ERR90098";
    public static final String SHIPMENT_VOLUME_LESS_ZERO = "ERR90099";
    public static final String SHIPMENT_CHARGE_LESS_ZERO = "ERR90100";

    public static final String SHIPMENT_LOCATION = "ERR90101";
    public static final String SHIPMENT_LOCATION_BLOCK = "ERR90102";
    public static final String SHIPMENT_LOCATION_HIDE = "ERR90103";

    public static final String SHIPMENT_SERVICE_REQ = "ERR90104";

    public static final String SHIPMENT_DOCUMENT_DATEREQ = "ERR90105";

    public static final String SHIPMENT_DOCUMENT_SHIPPER = "ERR90106";
    public static final String SHIPMENT_DOCUMENT_SHIPPER_BLOCK = "ERR90107";
    public static final String SHIPMENT_DOCUMENT_SHIPPER_HIDE = "ERR90108";
    public static final String SHIPMENT_DOCUMENT_SHIPPER_ADDRESS = "ERR90109";
    public static final String SHIPMENT_DOCUMENT_SHIPPER_ADDRESS_1 = "ERR90110";
    public static final String SHIPMENT_DOCUMENT_SHIPPER_ADDRESS_1_LENGTH = "ERR90111";
    public static final String SHIPMENT_DOCUMENT_SHIPPER_ADDRESS_2 = "ERR90112";
    public static final String SHIPMENT_DOCUMENT_SHIPPER_ADDRESS_2_LENGTH = "ERR90113";
    public static final String SHIPMENT_DOCUMENT_SHIPPER_ADDRESS_3_LENGTH = "ERR90114";
    public static final String SHIPMENT_DOCUMENT_SHIPPER_ADDRESS_4 = "ERR90115";
    public static final String SHIPMENT_DOCUMENT_SHIPPER_ADDRESS_4_LENGTH = "ERR90116";
    public static final String SHIPMENT_DOCUMENT_SHIPPER_EMAIL = "ERR90117";
    public static final String SHIPMENT_DOCUMENT_SHIPPER_EMAIL_LENGTH = "ERR90118";

    public static final String SHIPMENT_DOCUMENT_FORWARDER = "ERR90119";
    public static final String SHIPMENT_DOCUMENT_FORWARDER_BLOCK = "ERR90120";
    public static final String SHIPMENT_DOCUMENT_FORWARDER_HIDE = "ERR90121";
    public static final String SHIPMENT_DOCUMENT_FORWARDER_ADDRESS = "ERR90122";
    public static final String SHIPMENT_DOCUMENT_FORWARDER_ADDRESS_1 = "ERR90123";
    public static final String SHIPMENT_DOCUMENT_FORWARDER_ADDRESS_1_LENGTH = "ERR90124";
    public static final String SHIPMENT_DOCUMENT_FORWARDER_ADDRESS_2 = "ERR90125";
    public static final String SHIPMENT_DOCUMENT_FORWARDER_ADDRESS_2_LENGTH = "ERR90126";
    public static final String SHIPMENT_DOCUMENT_FORWARDER_ADDRESS_3_LENGTH = "ERR90127";
    public static final String SHIPMENT_DOCUMENT_FORWARDER_ADDRESS_4 = "ERR90128";
    public static final String SHIPMENT_DOCUMENT_FORWARDER_ADDRESS_4_LENGTH = "ERR90129";
    public static final String SHIPMENT_DOCUMENT_FORWARDER_EMAIL = "ERR90130";
    public static final String SHIPMENT_DOCUMENT_FORWARDER_EMAIL_LENGTH = "ERR90131";

    public static final String SHIPMENT_DOCUMENT_CONSIGNEE = "ERR90132";
    public static final String SHIPMENT_DOCUMENT_CONSIGNEE_BLOCK = "ERR90133";
    public static final String SHIPMENT_DOCUMENT_CONSIGNEE_HIDE = "ERR90134";
    public static final String SHIPMENT_DOCUMENT_CONSIGNEE_ADDRESS = "ERR90135";
    public static final String SHIPMENT_DOCUMENT_CONSIGNEE_ADDRESS_1 = "ERR90136";
    public static final String SHIPMENT_DOCUMENT_CONSIGNEE_ADDRESS_1_LENGTH = "ERR90137";
    public static final String SHIPMENT_DOCUMENT_CONSIGNEE_ADDRESS_2 = "ERR90138";
    public static final String SHIPMENT_DOCUMENT_CONSIGNEE_ADDRESS_2_LENGTH = "ERR90139";
    public static final String SHIPMENT_DOCUMENT_CONSIGNEE_ADDRESS_3_LENGTH = "ERR90140";
    public static final String SHIPMENT_DOCUMENT_CONSIGNEE_ADDRESS_4 = "ERR90141";
    public static final String SHIPMENT_DOCUMENT_CONSIGNEE_ADDRESS_4_LENGTH = "ERR90142";
    public static final String SHIPMENT_DOCUMENT_CONSIGNEE_EMAIL = "ERR90143";
    public static final String SHIPMENT_DOCUMENT_CONSIGNEE_EMAIL_LENGTH = "ERR90144";

    public static final String SHIPMENT_DOCUMENT_FIRST_NOTIFY = "ERR90145";
    public static final String SHIPMENT_DOCUMENT_FIRST_NOTIFY_BLOCK = "ERR90146";
    public static final String SHIPMENT_DOCUMENT_FIRST_NOTIFY_HIDE = "ERR90147";
    public static final String SHIPMENT_DOCUMENT_FIRST_NOTIFY_ADDRESS = "ERR90148";
    public static final String SHIPMENT_DOCUMENT_FIRST_NOTIFY_ADDRESS_1 = "ERR90149";
    public static final String SHIPMENT_DOCUMENT_FIRST_NOTIFY_ADDRESS_1_LENGTH = "ERR90150";
    public static final String SHIPMENT_DOCUMENT_FIRST_NOTIFY_ADDRESS_2 = "ERR90151";
    public static final String SHIPMENT_DOCUMENT_FIRST_NOTIFY_ADDRESS_2_LENGTH = "ERR90152";
    public static final String SHIPMENT_DOCUMENT_FIRST_NOTIFY_ADDRESS_3_LENGTH = "ERR90153";
    public static final String SHIPMENT_DOCUMENT_FIRST_NOTIFY_ADDRESS_4 = "ERR90154";
    public static final String SHIPMENT_DOCUMENT_FIRST_NOTIFY_ADDRESS_4_LENGTH = "ERR90155";
    public static final String SHIPMENT_DOCUMENT_FIRST_NOTIFY_EMAIL = "ERR90156";
    public static final String SHIPMENT_DOCUMENT_FIRST_NOTIFY_EMAIL_LENGTH = "ERR90157";

    public static final String SHIPMENT_DOCUMENT_SECOND_NOTIFY = "ERR90158";
    public static final String SHIPMENT_DOCUMENT_SECOND_NOTIFY_BLOCK = "ERR90159";
    public static final String SHIPMENT_DOCUMENT_SECOND_NOTIFY_HIDE = "ERR90160";
    public static final String SHIPMENT_DOCUMENT_SECOND_NOTIFY_ADDRESS = "ERR90161";
    public static final String SHIPMENT_DOCUMENT_SECOND_NOTIFY_ADDRESS_1 = "ERR90162";
    public static final String SHIPMENT_DOCUMENT_SECOND_NOTIFY_ADDRESS_1_LENGTH = "ERR90163";
    public static final String SHIPMENT_DOCUMENT_SECOND_NOTIFY_ADDRESS_2 = "ERR90164";
    public static final String SHIPMENT_DOCUMENT_SECOND_NOTIFY_ADDRESS_2_LENGTH = "ERR90165";
    public static final String SHIPMENT_DOCUMENT_SECOND_NOTIFY_ADDRESS_3_LENGTH = "ERR90166";
    public static final String SHIPMENT_DOCUMENT_SECOND_NOTIFY_ADDRESS_4 = "ERR90167";
    public static final String SHIPMENT_DOCUMENT_SECOND_NOTIFY_ADDRESS_4_LENGTH = "ERR90168";
    public static final String SHIPMENT_DOCUMENT_SECOND_NOTIFY_EMAIL = "ERR90169";
    public static final String SHIPMENT_DOCUMENT_SECOND_NOTIFY_EMAIL_LENGTH = "ERR90170";

    public static final String SHIPMENT_DOCUMENT_AGENT = "ERR90171";
    public static final String SHIPMENT_DOCUMENT_AGENT_BLOCK = "ERR90172";
    public static final String SHIPMENT_DOCUMENT_AGENT_HIDE = "ERR90173";
    public static final String SHIPMENT_DOCUMENT_AGENT_ADDRESS = "ERR90174";
    public static final String SHIPMENT_DOCUMENT_AGENT_ADDRESS_1 = "ERR90175";
    public static final String SHIPMENT_DOCUMENT_AGENT_ADDRESS_1_LENGTH = "ERR90176";
    public static final String SHIPMENT_DOCUMENT_AGENT_ADDRESS_2 = "ERR90177";
    public static final String SHIPMENT_DOCUMENT_AGENT_ADDRESS_2_LENGTH = "ERR90178";
    public static final String SHIPMENT_DOCUMENT_AGENT_ADDRESS_3_LENGTH = "ERR90179";
    public static final String SHIPMENT_DOCUMENT_AGENT_ADDRESS_4 = "ERR90180";
    public static final String SHIPMENT_DOCUMENT_AGENT_ADDRESS_4_LENGTH = "ERR90181";
    public static final String SHIPMENT_DOCUMENT_AGENT_EMAIL = "ERR90182";
    public static final String SHIPMENT_DOCUMENT_AGENT_EMAIL_LENGTH = "ERR90183";

    public static final String SHIPMENT_DOCUMENT_ISSUING_AGENT = "ERR90184";
    public static final String SHIPMENT_DOCUMENT_ISSUING_AGENT_BLOCK = "ERR90185";
    public static final String SHIPMENT_DOCUMENT_ISSUING_AGENT_HIDE = "ERR90186";
    public static final String SHIPMENT_DOCUMENT_ISSUING_AGENT_ADDRESS = "ERR90187";
    public static final String SHIPMENT_DOCUMENT_ISSUING_AGENT_ADDRESS_1 = "ERR90188";
    public static final String SHIPMENT_DOCUMENT_ISSUING_AGENT_ADDRESS_1_LENGTH = "ERR90189";
    public static final String SHIPMENT_DOCUMENT_ISSUING_AGENT_ADDRESS_2 = "ERR90190";
    public static final String SHIPMENT_DOCUMENT_ISSUING_AGENT_ADDRESS_2_LENGTH = "ERR90191";
    public static final String SHIPMENT_DOCUMENT_ISSUING_AGENT_ADDRESS_3_LENGTH = "ERR90192";
    public static final String SHIPMENT_DOCUMENT_ISSUING_AGENT_ADDRESS_4 = "ERR90193";
    public static final String SHIPMENT_DOCUMENT_ISSUING_AGENT_ADDRESS_4_LENGTH = "ERR90194";
    public static final String SHIPMENT_DOCUMENT_ISSUING_AGENT_EMAIL = "ERR90195";
    public static final String SHIPMENT_DOCUMENT_ISSUING_AGENT_EMAIL_LENGTH = "ERR90196";


    public static final String SHIPMENT_DOCUMENT_CHA_AGENT = "ERR90707";
    public static final String SHIPMENT_DOCUMENT_CHA_AGENT_BLOCK = "ERR90703";
    public static final String SHIPMENT_DOCUMENT_CHA_AGENT_HIDE = "ERR90704";
    public static final String SHIPMENT_DOCUMENT_CHA_AGENT_ADDRESS = "ERR90708";
    public static final String SHIPMENT_DOCUMENT_CHA_AGENT_ADDRESS_1 = "ERR90705";
    public static final String SHIPMENT_DOCUMENT_CHA_AGENT_ADDRESS_1_LENGTH = "ERR90709";
    public static final String SHIPMENT_DOCUMENT_CHA_AGENT_ADDRESS_2 = "ERR90710";
    public static final String SHIPMENT_DOCUMENT_CHA_AGENT_ADDRESS_2_LENGTH = "ERR90711";
    public static final String SHIPMENT_DOCUMENT_CHA_AGENT_ADDRESS_3_LENGTH = "ERR90702";
    public static final String SHIPMENT_DOCUMENT_CHA_AGENT_ADDRESS_4 = "ERR90712";
    public static final String SHIPMENT_DOCUMENT_CHA_AGENT_ADDRESS_4_LENGTH = "ERR90713";
    public static final String SHIPMENT_DOCUMENT_CHA_AGENT_EMAIL = "ERR90714";
    public static final String SHIPMENT_DOCUMENT_CHA_AGENT_EMAIL_LENGTH = "ERR90706";
    public static final String SHIPMENT_DOC_CHAAGENT_DEFAULTER = "ERR90702";

    public static final String SHIPMENT_GROSS_WEIGHT_POUND = "ERR90197";
    public static final String SHIPMENT_GROSS_WEIGHT_POUND_LESS_ZERO = "ERR90198";
    public static final String SHIPMENT_VOLUME_WEIGHT_POUND_LESS_ZERO = "ERR90199";

    public static final String SHIPMENT_DOCUMENT_HANDLING_INFO = "ERR90200";
    public static final String SHIPMENT_DOCUMENT_ACCOUNTING_INFO = "ERR90201";
    public static final String SHIPMENT_DOCUMENT_COMMODITY_DESCRIPTION = "ERR90202";
    public static final String SHIPMENT_DOCUMENT_MARKS_AND_NO = "ERR90203";
    public static final String SHIPMENT_DOCUMENT_RATE_CLASS = "ERR90204";

    public static final String SHIPMENT_DOCUMENT_BL_EMP = "ERR90205";
    public static final String SHIPMENT_DOCUMENT_BL_EMP_RESIGNED = "ERR90206";
    public static final String SHIPMENT_DOCUMENT_BL_EMP_TERMINATED = "ERR90207";
    public static final String SHIPMENT_DOCUMENT_BL_RECEIVED_DATE = "ERR90208";

    public static final String SHIPMENT_SERVICE_DIMENSION_PIECES_REQUIRED = "ERR90209";
    public static final String SHIPMENT_SERVICE_DIMENSION_LENGTH_REQUIRED = "ERR90210";
    public static final String SHIPMENT_SERVICE_DIMENSION_WIDTH_REQUIRED = "ERR90211";
    public static final String SHIPMENT_SERVICE_DIMENSION_HEIGHT_REQUIRED = "ERR90212";
    public static final String SHIPMENT_SERVICE_DIMENSION_VOLUME_WEIGHT_REQUIRED = "ERR90213";
    public static final String SHIPMENT_SERVICE_DIMENSION_GROSS_WEIGHT_REQUIRED = "ERR90214";

    public static final String SHIPMENT_SERVICE_PPCC = "ERR90215";
    public static final String SHIPMENT_SERVICE_INVALID_CHECK_DIGIT = "ERR90216";
    public static final String SHIPMENT_SERVICE_HOLD_NOTE = "ERR90217";
    public static final String SHIPMENT_SERVICE_HOLD_RELEASE_NOTE = "ERR90218";

    public static final String SHIPMENT_CHARGE_REQUIRED = "ERR90300";
    public static final String SHIPMENT_CHARGE_BLOCKED = "ERR90301";
    public static final String SHIPMENT_CHARGE_HIDDEN = "ERR90302";
    public static final String SHIPMENT_CHARGE_NAME_REQUIRED = "ERR90303";
    public static final String SHIPMENT_CHARGE_NAME_INVALID = "ERR90304";
    public static final String SHIPMENT_CHARGE_UNIT_REQUIRED = "ERR90305";
    public static final String SHIPMENT_CHARGE_UNIT_BLOCKED = "ERR90306";
    public static final String SHIPMENT_CHARGE_UNIT_HIDDEN = "ERR90307";
    public static final String SHIPMENT_CHARGE_MIN_VALUE_ZERO = "ERR90308";
    public static final String SHIPMENT_CHARGE_MAX_VALUE_ZERO = "ERR90309";
    public static final String SHIPMENT_CHARGE_MIN_VALUE_INVALID = "ERR90310";
    public static final String SHIPMENT_CHARGE_MAX_VALUE_INVALID = "ERR90311";
    public static final String SHIPMENT_CHARGE_MIN_MAX_MISMATCH = "ERR90312";
    public static final String SHIPMENT_CHARGE_GROSS_CURRENCY_BLOCKED = "ERR90313";
    public static final String SHIPMENT_CHARGE_GROSS_CURRENCY_HIDDEN = "ERR90314";
    public static final String SHIPMENT_CHARGE_GROSS_AMOUNT_ZERO = "ERR90315";
    public static final String SHIPMENT_CHARGE_GROSS_AMOUNT_INVALID = "ERR90316";
    public static final String SHIPMENT_CHARGE_GROSS_MINIMUM_ZERO = "ERR90317";
    public static final String SHIPMENT_CHARGE_GROSS_MINIMUM_INVALID = "ERR90318";
    public static final String SHIPMENT_CHARGE_NET_CURRENCY_BLOCKED = "ERR90319";
    public static final String SHIPMENT_CHARGE_NET_CURRENCY_HIDDEN = "ERR90320";
    public static final String SHIPMENT_CHARGE_NET_AMOUNT_ZERO = "ERR90321";
    public static final String SHIPMENT_CHARGE_NET_AMOUNT_INVALID = "ERR90322";
    public static final String SHIPMENT_CHARGE_NET_MINIMUM_ZERO = "ERR90323";
    public static final String SHIPMENT_CHARGE_NET_MINIMUM_INVALID = "ERR90324";
    public static final String SHIPMENT_CHARGE_DECLARED_CURRENCY_BLOCKED = "ERR90325";
    public static final String SHIPMENT_CHARGE_DECLARED_CURRENCY_HIDDEN = "ERR90326";
    public static final String SHIPMENT_CHARGE_DECLARED_AMOUNT_ZERO = "ERR90327";
    public static final String SHIPMENT_CHARGE_DECLARED_AMOUNT_INVALID = "ERR90328";
    public static final String SHIPMENT_CHARGE_DECLARED_MINIMUM_ZERO = "ERR90329";
    public static final String SHIPMENT_CHARGE_DECLARED_MINIMUM_INVALID = "ERR90330";
    public static final String SHIPMENT_CHARGE_RATE_CURRENCY_BLOCKED = "ERR90331";
    public static final String SHIPMENT_CHARGE_RATE_CURRENCY_HIDDEN = "ERR90332";
    public static final String SHIPMENT_CHARGE_RATE_AMOUNT_ZERO = "ERR90333";
    public static final String SHIPMENT_CHARGE_RATE_AMOUNT_INVALID = "ERR90334";
    public static final String SHIPMENT_CHARGE_RATE_MINIMUM_ZERO = "ERR90335";
    public static final String SHIPMENT_CHARGE_RATE_MINIMUM_INVALID = "ERR90336";
    public static final String SHIPMENT_CHARGE_COST_CURRENCY_BLOCKED = "ERR90337";
    public static final String SHIPMENT_CHARGE_COST_CURRENCY_HIDDEN = "ERR90338";
    public static final String SHIPMENT_CHARGE_COST_AMOUNT_ZERO = "ERR90339";
    public static final String SHIPMENT_CHARGE_COST_AMOUNT_INVALID = "ERR90340";
    public static final String SHIPMENT_CHARGE_COST_MINIMUM_ZERO = "ERR90341";
    public static final String SHIPMENT_CHARGE_COST_MINIMUM_INVALID = "ERR90342";
    public static final String SHIPMENT_CHARGE_VENDOR_BLOCKED = "ERR90343";
    public static final String SHIPMENT_CHARGE_VENDOR_HIDDEN = "ERR90344";
    public static final String SHIPMENT_CHARGE_EXCHANGE_RATE_1 = "ERR90345-1";
    public static final String SHIPMENT_CHARGE_EXCHANGE_RATE_2 = "ERR90345-2";
    public static final String SHIPMENT_CHARGE_EXCHANGE_RATE_3 = "ERR90345-3";
    public static final String SHIPMENT_CHARGE_EXCHANGE_RATE_4 = "ERR90345-4";
    public static final String SHIPMENT_CHARGE_EXCHANGE_RATE_5 = "ERR90345-5";

    public static final String SHIPMENT_MOVE_NOTNULL = "ERR90346";
    public static final String SHIPMENT_CONNECTION_STATUS = "ERR90347";
    public static final String SHIPMENT_CONNECTION_POL_MANDATORY = "ERR90348";
    public static final String SHIPMENT_CONNECTION_POL_BLOCKED = "ERR90349";
    public static final String SHIPMENT_CONNECTION_POL_HIDDEN = "ERR90350";
    public static final String SHIPMENT_CONNECTION_POD_MANDATORY = "ERR90351";
    public static final String SHIPMENT_CONNECTION_POD_BLOCKED = "ERR90352";
    public static final String SHIPMENT_CONNECTION_POD_HIDDEN = "ERR90353";
    public static final String SHIPMENT_CONNECTION_ETA_MANDATORY = "ERR90354";
    public static final String SHIPMENT_CONNECTION_ETD_MANDATORY = "ERR90355";
    public static final String SHIPMENT_CONNECTION_ETD_ETA_GREATER = "ERR90356";
    public static final String SHIPMENT_CONNECTION_CARRIER_MANDATORY = "ERR90357";
    public static final String SHIPMENT_CONNECTION_CARRIER_BLOCKED = "ERR90358";
    public static final String SHIPMENT_CONNECTION_CARRIER_HIDDEN = "ERR90359";
    public static final String SHIPMENT_FLIGHTVOYAGE_NOT_NULL = "ERR90360";
    public static final String SHIPMENT_FLIGHTVOYAGE_NO_INVALID = "ERR90361";
    public static final String SHIPMENT_CONNECTION_POD_POL_EQUAL = "ERR90362";
    public static final String SHIPMENT_CONNECTION_ORIGIN_DESTINATION_EQUAL = "ERR90363";
    public static final String CONNECTION_LIST_REQUIRED = "ERR90364";
    public static final String TRANSHIPMENT_CONNECTION_LIST_REQUIRED = "ERR90365";

    public static final String ATTACHMENT_DOCUMENT_BLOCKED = "ERR90600";
    public static final String ATTACHMENT_DOCUMENT_HIDDEN = "ERR90601";
    public static final String ATTACHMENT_REFERENCE_NUMBER_REQUIRED = "ERR90602";
    public static final String ATTACHMENT_REFERENCE_NUMBER_INVALID = "ERR90603";
    public static final String ATTACHMENT_REFERENCE_NUMBER_DUPLICATED = "ERR90604";
    public static final String ATTACHMENT_PROTECTED_FILE_EMPTY = "ERR90605";
    public static final String ATTACHMENT_PROTECTED_FILE_SIZE_EXCEED = "ERR90606";
    public static final String ATTACHMENT_UNPROTECTED_FILE_EMPTY = "ERR90607";
    public static final String ATTACHMENT_UNPROTECTED_FILE_SIZE_EXCEED = "ERR90608";
    public static final String ATTACHMENT_UNPROTECTED_OR_PROTECTED_FILE_REQUIRED = "ERR90609";

    public static final String SHIPMENT_STATUS_TYPE_REQUIRED = "ERR90400";
    public static final String SHIPMENT_STATUS_TYPE_BLOCKED = "ERR90401";
    public static final String SHIPMENT_STATUS_TYPE_HIDDEN = "ERR90402";

    public static final String SHIPMENT_STATUS_NAME_REQUIRED = "ERR90410";
    public static final String SHIPMENT_STATUS_NAME_BLOCKED = "ERR90411";
    public static final String SHIPMENT_STATUS_NAME_HIDDEN = "ERR90412";
    public static final String SHIPMENT_STATUS_DATE_REQUIRED = "ERR90413";
    public static final String SHIPMENT_STATUS_ASSIGNED_TO_REQUIRED = "ERR90420";
    public static final String SHIPMENT_STATUS_ASSIGNED_TO_RESIGNED = "ERR90421";
    public static final String SHIPMENT_STATUS_ASSIGNED_TO_TERMINATED = "ERR90422";

    public static final String SHIPMENT_STATUS_FOLLOW_UP_DATE_REQUIRED = "ERR90430";
    public static final String SHIPMENT_STATUS_FOLLOW_UP_DATE_INVALID = "ERR90431";

    public static final String SHIPMENT_STATUS_NOTE_REQUIRED = "ERR90440";
    public static final String SHIPMENT_STATUS_NOTE_INVALID = "ERR90441";

    public static final String SHIPMENT_STATUS_PROJECT_REQUIRED = "ERR90450";
    public static final String SHIPMENT_STATUS_PROJECT_INVALID = "ERR90451";
    public static final String SHIPMENT_STATUS_FOLLOW_UP_REQUIRED = "ERR90452";
    public static final String SHIPMENT_STATUS_PROTECT_REQUIRED = "ERR90453";


    public static final String SHIPMENT_TRANSPORTER = "ERR90454";
    public static final String SHIPMENT_TRANSPORTER_BLOCK = "ERR90455";
    public static final String SHIPMENT_TRANSPORTER_HIDE = "ERR90456";
    public static final String SHIPMENT_TRANSPORTER_ADDRESS = "ERR90457";
    public static final String SHIPMENT_TRANSPORTER_ADDRESS_1 = "ERR90458";
    public static final String SHIPMENT_TRANSPORTER_ADDRESS_1_LENGTH = "ERR90459";
    public static final String SHIPMENT_TRANSPORTER_ADDRESS_2 = "ERR90460";
    public static final String SHIPMENT_TRANSPORTER_ADDRESS_2_LENGTH = "ERR90461";
    public static final String SHIPMENT_TRANSPORTER_ADDRESS_3_LENGTH = "ERR90462";
    public static final String SHIPMENT_TRANSPORTER_ADDRESS_4_LENGTH = "ERR90463";
    public static final String SHIPMENT_TRANSPORTER_EMAIL_LENGTH = "ERR90464";


    public static final String SHIPMENT_EVENT = "ERR90465";
    public static final String SHIPMENT_EVENT_BLOCK = "ERR90466";
    public static final String SHIPMENT_EVENT_HIDE = "ERR90467";
    public static final String SHIPMENT_EVENT_DATE = "ERR90468";
    public static final String SHIPMENT_EVENT_ASSINGED = "ERR90469";
    public static final String SHIPMENT_EVENT_ASSINGED_BLOCK = "ERR90470";
    public static final String SHIPMENT_EVENT_ASSINGED_HIDE = "ERR90471";
    public static final String SHIPMENT_EVENT_FOLLOW_DATE = "ERR90472";

    public static final String SHIPMENT_DOC_PIECES_MANDATORY = "ERR90473";
    public static final String SHIPMENT_DOC_PIECES_INVALID = "ERR90474";

    public static final String SHIPMENT_DOC_GROSS_WEIGHT_MANDATORY = "ERR90475";
    public static final String SHIPMENT_DOC_GROSS_WEIGHT_INVALID = "ERR90476";


    public static final String SHIPMENT_DOC_GROSS_POUND_MANDATORY = "ERR90477";
    public static final String SHIPMENT_DOC_GROSS_POUND_INVALID = "ERR90478";


    public static final String SHIPMENT_DOC_VOLUME_WEIGHT_MANDATORY = "ERR90479";
    public static final String SHIPMENT_DOC_VOLUME_WEIGHT_INVALID = "ERR90480";


    public static final String SHIPMENT_DOC_VOLUME_POUND_MANDATORY = "ERR90481";
    public static final String SHIPMENT_DOC_VOLUME_POUND_INVALID = "ERR90482";


    public static final String SHIPMENT_SERVICE_PICKUP_POINT = "ERR90483";
    public static final String SHIPMENT_SERVICE_PICKUP_POINT_BLOCK = "ERR90484";
    public static final String SHIPMENT_SERVICE_PICKUP_POINT_HIDE = "ERR90485";
    public static final String SHIPMENT_SERVICE_PICKUP_FROM = "ERR90486";
    public static final String SHIPMENT_SERVICE_PICKUP_FROM_BLOCK = "ERR90487";
    public static final String SHIPMENT_SERVICE_PICKUP_FROM_HIDE = "ERR90488";
    public static final String SHIPMENT_SERVICE_PICKUP_PLACE = "ERR90489";
    public static final String SHIPMENT_SERVICE_PICKUP_CONTACT_PERSON = "ERR90490";
    public static final String SHIPMENT_SERVICE_PICKUP_MOBILE = "ERR90491";
    public static final String SHIPMENT_SERVICE_PICKUP_PHONE = "ERR90492";
    public static final String SHIPMENT_SERVICE_PICKUP_EMAIL_LENGTH = "ERR90493";
    public static final String SHIPMENT_SERVICE_PICKUP_EMAIL = "ERR90494";

    public static final String SHIPMENT_SERVICE_DELIVERY_POINT = "ERR90495";
    public static final String SHIPMENT_SERVICE_DELIVERY_POINT_BLOCK = "ERR90496";
    public static final String SHIPMENT_SERVICE_DELIVERY_POINT_HIDE = "ERR90497";
    public static final String SHIPMENT_SERVICE_DELIVERY_FROM = "ERR90498";
    public static final String SHIPMENT_SERVICE_DELIVERY_FROM_BLOCK = "ERR90499";
    public static final String SHIPMENT_SERVICE_DELIVERY_FROM_HIDE = "ERR90500";
    public static final String SHIPMENT_SERVICE_DELIVERY_PLACE = "ERR90501";
    public static final String SHIPMENT_SERVICE_DELIVERY_CONTACT_PERSON = "ERR90502";
    public static final String SHIPMENT_SERVICE_DELIVERY_MOBILE = "ERR90503";
    public static final String SHIPMENT_SERVICE_DELIVERY_PHONE = "ERR90504";
    public static final String SHIPMENT_SERVICE_DELIVERY_EMAIL_LENGTH = "ERR90505";
    public static final String SHIPMENT_SERVICE_DELIVERY_EMAIL = "ERR90506";

    public static final String SHIPMENT_SERVICE_DOOR_DELIVERY_POINT = "ERR90507";
    public static final String SHIPMENT_SERVICE_DOOR_DELIVERY_POINT_BLOCK = "ERR90508";
    public static final String SHIPMENT_SERVICE_DOOR_DELIVERY_POINT_HIDE = "ERR90509";
    public static final String SHIPMENT_SERVICE_DOOR_DELIVERY_FROM = "ERR90510";
    public static final String SHIPMENT_SERVICE_DOOR_DELIVERY_FROM_BLOCK = "ERR90511";
    public static final String SHIPMENT_SERVICE_DOOR_DELIVERY_FROM_HIDE = "ERR90512";
    public static final String SHIPMENT_SERVICE_DOOR_DELIVERY_PLACE = "ERR90513";
    public static final String SHIPMENT_SERVICE_DOOR_DELIVERY_CONTACT_PERSON = "ERR90514";
    public static final String SHIPMENT_SERVICE_DOOR_DELIVERY_MOBILE = "ERR90515";
    public static final String SHIPMENT_SERVICE_DOOR_DELIVERY_PHONE = "ERR90516";
    public static final String SHIPMENT_SERVICE_DOOR_DELIVERY_EMAIL_LENGTH = "ERR90517";
    public static final String SHIPMENT_SERVICE_DOOR_DELIVERY_EMAIL = "ERR90518";


    public static final String SHIPMENT_REFERENCE_NOT_NULL = "ERR90519";
    public static final String SHIPMENT_REFERENCE_BLOCKED = "ERR90520";
    public static final String SHIPMENT_REFERENCE_HIDDEN = "ERR90521";
    public static final String SHIPMENT_REFERENCE_NUMBER_EMPTY = "ERR90522";
    public static final String SHIPMENT_REFERENCE_NUMBER_INVALID = "ERR90523";
    public static final String SHIPMENT_REFERENCE_NOTES_INVALID = "ERR90524";
    public static final String SHIPMENT_REFERENCE_MANDATORY = "ERR90525";
    public static final String SHIPMENT_REFERENCE_MANDATORY_WITH_PARAM = "ERR90526";


    public static final String SHIPMENT_VOLUME_WEIGHT_MISMATCHED = "ERR90550";
    public static final String SHIPMENT_GROSS_WEIGHT_MISMATCHED = "ERR90551";
    public static final String SHIPMENT_VOLUME_POUND_MISMATCHED = "ERR90552";
    public static final String SHIPMENT_GROSSS_POUND_MISMATCHED = "ERR90553";
    public static final String SHIPMENT_CHARGEBLE_WEIGHT_MISMATCHED = "ERR90554";
    public static final String SHIPMENT_TOTAL_PIECES_MISMATCHED = "ERR90555";
    public static final String SHIPMENT_RATE_PER_CHARGE_INVALID = "ERR90556";
    public static final String SHIPMENT_RATE_PER_CHARGE = "ERR90557";
    public static final String SHIPMENT_SELECT_DOCUMENT = "ERR90558";
    public static final String SHIPMENT_VOLUMECPM_INVALID = "ERR90559";
    public static final String SHIPMENT_CONNECTION_INVALID = "ERR90560";
    public static final String SHIPMENT_RATES_INVALID = "ERR90561";
    public static final String SHIPMENT_STATUS_UPDATE_INVALID = "ERR90562";
    public static final String SHIPMENT_EVENTS_INVALID = "ERR90563";
    public static final String SHIPMENT_BROKERAGE_PARTY_BLOCK = "ERR90564";
    public static final String SHIPMENT_BROKERAGE_PARTY_HIDE = "ERR90565";
    public static final String SHIPMENT_BROKERAGE_INVALID = "ERR90566";
    public static final String SHIPMENT_BROKERAGE_PARTY_INVALID = "ERR90567";
    public static final String SHIPMENT_BROKERAGE_PARTY_DEFAULTER = "ERR90585";
    public static final String SHIPMENT_EVENTS_GOODS_NOT_RECEIVED = "ERR90568";
    public static final String SHIPMENT_MAWB_MANDATORY = "ERR90569";
    public static final String SHIPMENT_SERVICE_DUPLICATE = "ERR90570";
    public static final String SHIPMENT_UK_SHIPMENT_SHIPMENTUID = "ERR90571";
    public static final String IN_SERVICE = "ERR90572";
    public static final String SHIPMENT_GROSS_KG_MISMATCH = "ERR90577";
    public static final String SHIPMENT_VOLUME_KG_MISMATCH = "ERR90578";
    public static final String SHIPMENT_CHARGE_KG_MISMATCH = "ERR90579";
    public static final String SHIPMENT_GROSS_POUND_MISMATCH = "ERR90580";
    public static final String SHIPMENT_VOLUME_POUND_MISMATCH = "ERR90581";
    public static final String SHIPMENT_VOLUME_CBM_MISMATCH = "ERR90582";
    public static final String SHIPMENT_PIECES_MISMATCH = "ERR90583";
    public static final String SHIPMENT_DOC_SHIPPER_DEFAULTER = "ERR90586";
    public static final String SHIPMENT_DOC_CONSIGNEE_DEFAULTER = "ERR90587";
    public static final String SHIPMENT_DOC_FNOTI_DEFAULTER = "ERR90588";
    public static final String SHIPMENT_DOC_SNOTI_DEFAULTER = "ERR90589";
    public static final String SHIPMENT_DOC_AGENT_DEFAULTER = "ERR90590";
    public static final String SHIPMENT_DOC_FORW_DEFAULTER = "ERR90591";
    public static final String SHIPMENT_DOC_ISSAGENT_DEFAULTER = "ERR90592";
    public static final String SHIPMENT_TRANS_TRANS_DEFAULTER = "ERR90593";
    public static final String SHIPMENT_TRANS_PICKUP_DEFAULTER = "ERR90594";
    public static final String SHIPMENT_MINIMUM_WEIGHT = "ERR90595";
    public static final String DOCUMENT_NO_DUPLICATED = "ERR90596";
    public static final String HAWB_NO_DUPLICATED = "ERR90597";
    public static final String SHIPMENT_MINIMUM_WEIGHT_RESPONSE = "ERR90598";

    public static final String SHIPMENT_COPY_SERVICE_REQUIRED = "ERR90599-1";
    public static final String SHIPMENT_COPY_SERVICE_BLOCKED = "ERR90599-2";
    public static final String SHIPMENT_COPY_SERVICE_HIDDEN = "ERR90599-3";
    public static final String SHIPMENT_COPY_PARTY_REQUIRED = "ERR90599-4";
    public static final String SHIPMENT_COPY_PARTY_BLOCKED = "ERR90599-5";
    public static final String SHIPMENT_COPY_PARTY_HIDDEN = "ERR90599-6";

    public static final String SHIPMENT_ROUT_AGENT_DEFAULTER = "ERR90610";
    public static final String SHIPMENT_CO_LOADER_DEFAULTER = "ERR90611";
    public static final String SHIPMENT_STATUS = "ERR90612";
    public static final String SHIPMENT_ISSUING_AGENT_DONT_IATACODE = "ERR90613";
    public static final String SHIPMENT_HOLD = "ERR90614";

    public static final String HAWB_CUSTOMER_CONFORMATION_DOCUMENT_REQUIRED = "ERR90617";
    public static final String HAWB_CUSTOMER_CONFORMATION_PARTY_REQUIRED = "ERR90618";
    public static final String HAWB_CUSTOMER_CONFORMATION_PIN_INVALID = "ERR90619";

    public static final String HAWB_CUSTOMER_CONFORMATION_PIN_MANDATORY = "ERR90620";
    public static final String HAWB_CUSTOMER_CONFORMATION_FAILED = "ERR90621";
    public static final String HAWB_CUSTOMER_CONFORMATION_CONFIRMED = "ERR90622";
    public static final String HAWB_CUSTOMER_CONFORMATION_CANCELED = "ERR90623";
    public static final String HAWB_CUSTOMER_CONFORMATION_ALREADY_CONFIRMED = "ERR90624";

    public static final String SHIPMENT_CITY_BLOCKED = "ERR90624";
    public static final String SHIPMENT_CITY_HIDDEN = "ERR90625";

    public static final String SHIPMENT_SIGN_OFF_SUCCESS = "ERR90627";
    public static final String SHIPMENT_SIGN_OFF_ALREADY = "ERR90628";
    public static final String SHIPMENT_UISIGN_OFF_SUCCESS = "ERR90629";
    public static final String SHIPMENT_UISIGN_OFF_ALREADY = "ERR90630";
    public static final String SHIPMENT_SIGN_OFF_INVALID_USER = "ERR90631";
    public static final String SHIPMENT_SIGN_OFF_PVOS_NOT_FOUND = "ERR90632";
    public static final String SHIPMENT_SIGN_OFF_PVOS_NOT_MODIFY = "ERR90633";
    public static final String SHIPMENT_UNSIGN_OFF_INVALID_USER = "ERR90634";
    public static final String SHIPMENT_SIGN_OFF_FAILURE = "ERR90635";
    public static final String SHIPMENT_SIGN_OFF_NOT_ENABLED = "ERR90636";
    public static final String SHIPMENT_SIGN_OFF_INVALID_SUPER_USER_PROFIT = "ERR90637";
    public static final String SHIPMENT_SIGN_OFF_CHARGE_NOT_FOUND = "ERR90638";
    public static final String SHIPMENT_SIGN_OFF_INVALID_SUPER_USER_LOSS = "ERR90639";
    public static final String SHIPMENT_PICKUP_DELIVERY_PICKUP_FROM = "ERR90640";
    public static final String SHIPMENT_PICKUP_DELIVERY_PICKUP_POINT = "ERR90641";
    public static final String SHIPMENT_CONNECTION_AND_ROUTE_ETD = "ERR2700";
    public static final String SHIPMENT_ALREADY_CREATED_FROM_QUOTATION = "ERR90700";
    public static final String SHIPMENT_NOT_FOUND = "ERR90701";
    public static final String TRACK_SHIPMENT_EXPORT_SERVICE_EMPTY = "ERR90715";


    public static final String CONSOL_UNIQUE = "ERR95001";
    public static final String CONSOL_CREATED_BY = "ERR95002";
    public static final String CONSOL_CREATED_BY_RESIGED = "ERR95003";
    public static final String CONSOL_CREATED_BY_TERMINATED = "ERR95004";
    public static final String CONSOL_COMPANY = "ERR95005";
    public static final String CONSOL_COMPANY_BLOCK = "ERR95006";
    public static final String CONSOL_COMPANY_HIDE = "ERR95007";
    public static final String CONSOL_LOCATION = "ERR95008";
    public static final String CONSOL_LOCATION_BLOCK = "ERR95009";
    public static final String CONSOL_LOCATION_HIDE = "ERR95010";
    public static final String CONSOL_COUNTRY = "ERR95011";
    public static final String CONSOL_COUNTRY_BLOCK = "ERR95012";
    public static final String CONSOL_COUNTRY_HIDE = "ERR95013";
    public static final String CONSOL_REQ_DATE = "ERR95014";
    public static final String CONSOL_SERVICE = "ERR95015";
    public static final String CONSOL_SERVICE_BLOCK = "ERR95016";
    public static final String CONSOL_SERVICE_HIDE = "ERR95017";

    public static final String CONSOL_AGENT = "ERR95018";
    public static final String CONSOL_AGENT_BLOCK = "ERR95019";
    public static final String CONSOL_AGENT_HIDE = "ERR95020";
    public static final String CONSOL_AGENT_ADDRESS = "ERR95021";
    public static final String CONSOL_AGENT_ADDRESS_1 = "ERR95022";
    public static final String CONSOL_AGENT_ADDRESS_1_LENGTH = "ERR95023";
    public static final String CONSOL_AGENT_ADDRESS_2 = "ERR95024";
    public static final String CONSOL_AGENT_ADDRESS_2_LENGTH = "ERR95025";
    public static final String CONSOL_AGENT_ADDRESS_3_LENGTH = "ERR95026";
    public static final String CONSOL_AGENT_ADDRESS_4 = "ERR95027";
    public static final String CONSOL_AGENT_ADDRESS_4_LENGTH = "ERR95028";
    public static final String CONSOL_ORIGIN = "ERR95029";
    public static final String CONSOL_ORIGIN_BLOCK = "ERR95030";
    public static final String CONSOL_ORIGIN_HIDE = "ERR95031";
    public static final String CONSOL_ORIGIN_SERVICE_TRANSPORT_INVALID = "ERR95032";
    public static final String CONSOL_DESTINATION = "ERR95033";
    public static final String CONSOL_DESTINATION_BLOCK = "ERR95034";
    public static final String CONSOL_DESTINATION_HIDE = "ERR95035";
    public static final String CONSOL_DESTINATION_SERVICE_TRANSPORT_INVALID = "ERR95036";
    public static final String CONSOL_ORIGIN_DESTINATION_EQUAL = "ERR95037";
    public static final String CONSOL_POL = "ERR95038";
    public static final String CONSOL_POL_BLOCK = "ERR95039";
    public static final String CONSOL_POL_HIDE = "ERR95040";
    public static final String CONSOL_POL_SERVICE_TRANSPORT_INVALID = "ERR95041";
    public static final String CONSOL_POD = "ERR95042";
    public static final String CONSOL_POD_BLOCK = "ERR95043";
    public static final String CONSOL_POD_HIDE = "ERR95044";
    public static final String CONSOL_POD_SERVICE_TRANSPORT_INVALID = "ERR95045";
    public static final String CONSOL_POL_POD_EQUAL = "ERR95046";
    public static final String CONSOL_CURRENCY = "ERR95047";
    public static final String CONSOL_CURRENCY_BLOCK = "ERR95048";
    public static final String CONSOL_CURRENCY_HIDE = "ERR95049";
    public static final String CONSOL_CURRENCY_CURRENT_RATE_INVALID = "ERR95050";

    public static final String CONSOL_NO_OF_PACK_INVALID = "ERR95051";
    public static final String CONSOL_VOLUME_INVALID = "ERR95052";
    public static final String CONSOL_GROSS_WEIGHT_INVALID = "ERR95053";
    public static final String CONSOL_VOLUME_WEIGHT_INVALID = "ERR95054";
    public static final String CONSOL_CHARGEABLE_WEIGHT_INVALID = "ERR95055";
    public static final String CONSOL_GROSS_WEIGHT_POUND_INVALID = "ERR95056";
    public static final String CONSOL_VOLUME_WEIGHT_POUND_INVALID = "ERR95057";

    public static final String CONSOL_DIMENSION_PIECES_REQUIRED = "ERR95500";
    public static final String CONSOL_DIMENSION_PIECES_INVALID = "ERR95501";
    public static final String CONSOL_DIMENSION_PIECES_NEGATIVE = "ERR95502";

    public static final String CONSOL_DIMENSION_LENGTH_REQUIRED = "ERR95510";
    public static final String CONSOL_DIMENSION_LENGTH_INVALID = "ERR95511";
    public static final String CONSOL_DIMENSION_LENGTH_NEGATIVE = "ERR95512";

    public static final String CONSOL_DIMENSION_WIDTH_REQUIRED = "ERR95520";
    public static final String CONSOL_DIMENSION_WIDTH_INVALID = "ERR95521";
    public static final String CONSOL_DIMENSION_WIDTH_NEGATIVE = "ERR95522";

    public static final String CONSOL_DIMENSION_HEIGHT_REQUIRED = "ERR95530";
    public static final String CONSOL_DIMENSION_HEIGHT_INVALID = "ERR95531";
    public static final String CONSOL_DIMENSION_HEIGHT_NEGATIVE = "ERR95532";

    public static final String CONSOL_DIMENSION_VOLUME_WEIGHT_REQUIRED = "ERR95540";
    public static final String CONSOL_DIMENSION_VOLUME_WEIGHT_INVALID = "ERR95541";
    public static final String CONSOL_DIMENSION_VOLUME_WEIGHT_NEGATIVE = "ERR95542";

    public static final String CONSOL_DIMENSION_GROSS_WEIGHT_REQUIRED = "ERR95550";
    public static final String CONSOL_DIMENSION_GROSS_WEIGHT_INVALID = "ERR95551";
    public static final String CONSOL_DIMENSION_GROSS_WEIGHT_NEGATIVE = "ERR95552";

    public static final String CONSOL_MOVE_NOTNULL = "ERR95600";
    public static final String CONSOL_CONNECTION_STATUS = "ERR95601";
    public static final String CONSOL_CONNECTION_POL_MANDATORY = "ERR95602";
    public static final String CONSOL_CONNECTION_POL_BLOCKED = "ERR95603";
    public static final String CONSOL_CONNECTION_POL_HIDDEN = "ERR95604";
    public static final String CONSOL_CONNECTION_POD_MANDATORY = "ERR95605";
    public static final String CONSOL_CONNECTION_POD_BLOCKED = "ERR95606";
    public static final String CONSOL_CONNECTION_POD_HIDDEN = "ERR95607";
    public static final String CONSOL_CONNECTION_ETA_MANDATORY = "ERR95608";
    public static final String CONSOL_CONNECTION_ETD_MANDATORY = "ERR95609";
    public static final String CONSOL_CONNECTION_ETD_ETA_GREATER = "ERR95610";
    public static final String CONSOL_CONNECTION_CARRIER_MANDATORY = "ERR95611";
    public static final String CONSOL_CONNECTION_CARRIER_BLOCKED = "ERR95612";
    public static final String CONSOL_CONNECTION_CARRIER_HIDDEN = "ERR95613";
    public static final String CONSOL_FLIGHTVOYAGE_NOT_NULL = "ERR95614";
    public static final String CONSOL_FLIGHTVOYAGE_NO_INVALID = "ERR95615";

    public static final String CONSOL_POL_MODE = "ERR95616";
    public static final String CONSOL_POD_MODE = "ERR95617";

    public static final String CONSOL_POD_POL_EQUAL = "ERR95618";
    public static final String CONSOL_VOLUME_IS_REQUIRED = "ERR95619";

    public static final String CONSOL_CHARGE_REQUIRED = "ERR95620";
    public static final String CONSOL_CHARGE_BLOCKED = "ERR95621";
    public static final String CONSOL_CHARGE_HIDDEN = "ERR95622";
    public static final String CONSOL_CHARGE_NAME_REQUIRED = "ERR95623";
    public static final String CONSOL_CHARGE_NAME_INVALID = "ERR95624";

    public static final String CONSOL_CHARGE_CURRENCY = "ERR95625";
    public static final String CONSOL_CHARGE_CURRENCY_BLOCKED = "ERR95626";
    public static final String CONSOL_CHARGE_CURRENCY_HIDDEN = "ERR95627";

    public static final String CONSOL_CHARGE_UNIT = "ERR95628";

    public static final String CONSOL_CHARGE_ROE = "ERR95629";

    public static final String CONSOL_CHARGE_AMOUNT_PER_UNIT = "ERR95630";
    public static final String CONSOL_CHARGE_AMOUNT = "ERR95631";
    public static final String CONSOL_CHARGE_LOCAL_AMOUNT = "ERR95632";
    public static final String CONSOL_CHARGE_CRN_REQUIRED = "ERR95633";
    public static final String CONSOL_CHARGE_CRN_INVALID = "ERR95634";

    // Consol Document

    public static final String CONSOL_PACK = "ERR95701";
    public static final String CONSOL_PACK_BLOCK = "ERR95702";
    public static final String CONSOL_PACK_HIDE = "ERR95703";
    public static final String CONSOL_NO_OF_PIECES = "ERR95704";
    public static final String CONSOL_GROSS_WEIGHT = "ERR95705";

    public static final String CONSOL_CHARGEABLE_WEIGHT = "ERR95706";

    public static final String CONSOL_ORIGIN_AIR_MODE = "ERR95707";

    public static final String CONSOL_DESTINATION_AIR_MODE = "ERR95708";
    public static final String CONSOL_DESTINATION_OCEAN_MODE = "ERR95709";

    public static final String CONSOL_SERVICE_ORIGIN_MODE = "ERR95710";
    public static final String CONSOL_SERVICE_DESTINATION_MODE = "ERR95711";
    public static final String CONSOL_SERVICE_POL_MODE = "ERR95712";
    public static final String CONSOL_SERVICE_POD_MODE = "ERR95713";

    public static final String CONSOL_PIECES_LESS_ZERO = "ERR95714";
    public static final String CONSOL_GROSS_LESS_ZERO = "ERR95715";
    public static final String CONSOL_VOLUME_LESS_ZERO = "ERR95716";
    public static final String CONSOL_CHARGE_LESS_ZERO = "ERR95717";

    public static final String CONSOL_DOCUMENT_DATEREQ = "ERR95805";

    public static final String CONSOL_DOCUMENT_SHIPPER = "ERR95806";
    public static final String CONSOL_DOCUMENT_SHIPPER_BLOCK = "ERR95807";
    public static final String CONSOL_DOCUMENT_SHIPPER_HIDE = "ERR95808";
    public static final String CONSOL_DOCUMENT_SHIPPER_ADDRESS = "ERR95809";
    public static final String CONSOL_DOCUMENT_SHIPPER_ADDRESS_1 = "ERR95810";
    public static final String CONSOL_DOCUMENT_SHIPPER_ADDRESS_1_LENGTH = "ERR95811";
    public static final String CONSOL_DOCUMENT_SHIPPER_ADDRESS_2 = "ERR95812";
    public static final String CONSOL_DOCUMENT_SHIPPER_ADDRESS_2_LENGTH = "ERR95813";
    public static final String CONSOL_DOCUMENT_SHIPPER_ADDRESS_3_LENGTH = "ERR95814";
    public static final String CONSOL_DOCUMENT_SHIPPER_ADDRESS_4 = "ERR95815";
    public static final String CONSOL_DOCUMENT_SHIPPER_ADDRESS_4_LENGTH = "ERR95816";
    public static final String CONSOL_DOCUMENT_SHIPPER_EMAIL = "ERR95817";
    public static final String CONSOL_DOCUMENT_SHIPPER_EMAIL_LENGTH = "ERR95818";

    public static final String CONSOL_DOCUMENT_FORWARDER = "ERR95819";
    public static final String CONSOL_DOCUMENT_FORWARDER_BLOCK = "ERR95820";
    public static final String CONSOL_DOCUMENT_FORWARDER_HIDE = "ERR95821";
    public static final String CONSOL_DOCUMENT_FORWARDER_ADDRESS = "ERR95822";
    public static final String CONSOL_DOCUMENT_FORWARDER_ADDRESS_1 = "ERR95823";
    public static final String CONSOL_DOCUMENT_FORWARDER_ADDRESS_1_LENGTH = "ERR95824";
    public static final String CONSOL_DOCUMENT_FORWARDER_ADDRESS_2 = "ERR95825";
    public static final String CONSOL_DOCUMENT_FORWARDER_ADDRESS_2_LENGTH = "ERR95826";
    public static final String CONSOL_DOCUMENT_FORWARDER_ADDRESS_3_LENGTH = "ERR95827";
    public static final String CONSOL_DOCUMENT_FORWARDER_ADDRESS_4 = "ERR95828";
    public static final String CONSOL_DOCUMENT_FORWARDER_ADDRESS_4_LENGTH = "ERR95829";
    public static final String CONSOL_DOCUMENT_FORWARDER_EMAIL = "ERR95830";
    public static final String CONSOL_DOCUMENT_FORWARDER_EMAIL_LENGTH = "ERR95831";

    public static final String CONSOL_DOCUMENT_CONSIGNEE = "ERR95832";
    public static final String CONSOL_DOCUMENT_CONSIGNEE_BLOCK = "ERR95833";
    public static final String CONSOL_DOCUMENT_CONSIGNEE_HIDE = "ERR95834";
    public static final String CONSOL_DOCUMENT_CONSIGNEE_ADDRESS = "ERR95835";
    public static final String CONSOL_DOCUMENT_CONSIGNEE_ADDRESS_1 = "ERR95836";
    public static final String CONSOL_DOCUMENT_CONSIGNEE_ADDRESS_1_LENGTH = "ERR95837";
    public static final String CONSOL_DOCUMENT_CONSIGNEE_ADDRESS_2 = "ERR95838";
    public static final String CONSOL_DOCUMENT_CONSIGNEE_ADDRESS_2_LENGTH = "ERR95839";
    public static final String CONSOL_DOCUMENT_CONSIGNEE_ADDRESS_3_LENGTH = "ERR95840";
    public static final String CONSOL_DOCUMENT_CONSIGNEE_ADDRESS_4 = "ERR95841";
    public static final String CONSOL_DOCUMENT_CONSIGNEE_ADDRESS_4_LENGTH = "ERR95842";
    public static final String CONSOL_DOCUMENT_CONSIGNEE_EMAIL = "ERR95843";
    public static final String CONSOL_DOCUMENT_CONSIGNEE_EMAIL_LENGTH = "ERR95844";

    public static final String CONSOL_DOCUMENT_FIRST_NOTIFY = "ERR95845";
    public static final String CONSOL_DOCUMENT_FIRST_NOTIFY_BLOCK = "ERR95846";
    public static final String CONSOL_DOCUMENT_FIRST_NOTIFY_HIDE = "ERR95847";
    public static final String CONSOL_DOCUMENT_FIRST_NOTIFY_ADDRESS = "ERR95848";
    public static final String CONSOL_DOCUMENT_FIRST_NOTIFY_ADDRESS_1 = "ERR95849";
    public static final String CONSOL_DOCUMENT_FIRST_NOTIFY_ADDRESS_1_LENGTH = "ERR95850";
    public static final String CONSOL_DOCUMENT_FIRST_NOTIFY_ADDRESS_2 = "ERR95851";
    public static final String CONSOL_DOCUMENT_FIRST_NOTIFY_ADDRESS_2_LENGTH = "ERR95852";
    public static final String CONSOL_DOCUMENT_FIRST_NOTIFY_ADDRESS_3_LENGTH = "ERR95853";
    public static final String CONSOL_DOCUMENT_FIRST_NOTIFY_ADDRESS_4 = "ERR95854";
    public static final String CONSOL_DOCUMENT_FIRST_NOTIFY_ADDRESS_4_LENGTH = "ERR95855";
    public static final String CONSOL_DOCUMENT_FIRST_NOTIFY_EMAIL = "ERR95856";
    public static final String CONSOL_DOCUMENT_FIRST_NOTIFY_EMAIL_LENGTH = "ERR95857";

    public static final String CONSOL_DOCUMENT_SECOND_NOTIFY = "ERR95858";
    public static final String CONSOL_DOCUMENT_SECOND_NOTIFY_BLOCK = "ERR95859";
    public static final String CONSOL_DOCUMENT_SECOND_NOTIFY_HIDE = "ERR95860";
    public static final String CONSOL_DOCUMENT_SECOND_NOTIFY_ADDRESS = "ERR95861";
    public static final String CONSOL_DOCUMENT_SECOND_NOTIFY_ADDRESS_1 = "ERR95862";
    public static final String CONSOL_DOCUMENT_SECOND_NOTIFY_ADDRESS_1_LENGTH = "ERR95863";
    public static final String CONSOL_DOCUMENT_SECOND_NOTIFY_ADDRESS_2 = "ERR95864";
    public static final String CONSOL_DOCUMENT_SECOND_NOTIFY_ADDRESS_2_LENGTH = "ERR95865";
    public static final String CONSOL_DOCUMENT_SECOND_NOTIFY_ADDRESS_3_LENGTH = "ERR95866";
    public static final String CONSOL_DOCUMENT_SECOND_NOTIFY_ADDRESS_4 = "ERR95867";
    public static final String CONSOL_DOCUMENT_SECOND_NOTIFY_ADDRESS_4_LENGTH = "ERR95868";
    public static final String CONSOL_DOCUMENT_SECOND_NOTIFY_EMAIL = "ERR95869";
    public static final String CONSOL_DOCUMENT_SECOND_NOTIFY_EMAIL_LENGTH = "ERR95870";

    public static final String CONSOL_DOCUMENT_AGENT = "ERR95871";
    public static final String CONSOL_DOCUMENT_AGENT_BLOCK = "ERR95872";
    public static final String CONSOL_DOCUMENT_AGENT_HIDE = "ERR95873";
    public static final String CONSOL_DOCUMENT_AGENT_ADDRESS = "ERR95874";
    public static final String CONSOL_DOCUMENT_AGENT_ADDRESS_1 = "ERR95875";
    public static final String CONSOL_DOCUMENT_AGENT_ADDRESS_1_LENGTH = "ERR95876";
    public static final String CONSOL_DOCUMENT_AGENT_ADDRESS_2 = "ERR95877";
    public static final String CONSOL_DOCUMENT_AGENT_ADDRESS_2_LENGTH = "ERR95878";
    public static final String CONSOL_DOCUMENT_AGENT_ADDRESS_3_LENGTH = "ERR95879";
    public static final String CONSOL_DOCUMENT_AGENT_ADDRESS_4 = "ERR95880";
    public static final String CONSOL_DOCUMENT_AGENT_ADDRESS_4_LENGTH = "ERR95881";
    public static final String CONSOL_DOCUMENT_AGENT_EMAIL = "ERR95882";
    public static final String CONSOL_DOCUMENT_AGENT_EMAIL_LENGTH = "ERR95883";

    public static final String CONSOL_DOCUMENT_ISSUING_AGENT = "ERR95884";
    public static final String CONSOL_DOCUMENT_ISSUING_AGENT_BLOCK = "ERR95885";
    public static final String CONSOL_DOCUMENT_ISSUING_AGENT_HIDE = "ERR95886";
    public static final String CONSOL_DOCUMENT_ISSUING_AGENT_ADDRESS = "ERR95887";
    public static final String CONSOL_DOCUMENT_ISSUING_AGENT_ADDRESS_1 = "ERR95888";
    public static final String CONSOL_DOCUMENT_ISSUING_AGENT_ADDRESS_1_LENGTH = "ERR95889";
    public static final String CONSOL_DOCUMENT_ISSUING_AGENT_ADDRESS_2 = "ERR95890";
    public static final String CONSOL_DOCUMENT_ISSUING_AGENT_ADDRESS_2_LENGTH = "ERR95891";
    public static final String CONSOL_DOCUMENT_ISSUING_AGENT_ADDRESS_3_LENGTH = "ERR95892";
    public static final String CONSOL_DOCUMENT_ISSUING_AGENT_ADDRESS_4 = "ERR95893";
    public static final String CONSOL_DOCUMENT_ISSUING_AGENT_ADDRESS_4_LENGTH = "ERR95894";
    public static final String CONSOL_DOCUMENT_ISSUING_AGENT_EMAIL = "ERR95895";
    public static final String CONSOL_DOCUMENT_ISSUING_AGENT_EMAIL_LENGTH = "ERR95896";

    public static final String CONSOL_GROSS_WEIGHT_POUND = "ERR95897";
    public static final String CONSOL_GROSS_WEIGHT_POUND_LESS_ZERO = "ERR95898";
    public static final String CONSOL_VOLUME_WEIGHT_POUND_LESS_ZERO = "ERR95899";

    public static final String CONSOL_DOCUMENT_HANDLING_INFO = "ERR95900";
    public static final String CONSOL_DOCUMENT_ACCOUNTING_INFO = "ERR95901";
    public static final String CONSOL_DOCUMENT_COMMODITY_DESCRIPTION = "ERR95902";
    public static final String CONSOL_DOCUMENT_MARKS_AND_NO = "ERR95903";
    public static final String CONSOL_DOCUMENT_RATE_CLASS = "ERR95904";

    public static final String CONSOL_DOCUMENT_BL_EMP = "ERR95905";
    public static final String CONSOL_DOCUMENT_BL_EMP_RESIGNED = "ERR95906";
    public static final String CONSOL_DOCUMENT_BL_EMP_TERMINATED = "ERR95907";
    public static final String CONSOL_DOCUMENT_BL_RECEIVED_DATE = "ERR95908";

    //DELIVERY ORDER MASTER DO

    public static final String CONSOL_DOCUMENT_MASTER_DO_REQUIRED = "ERR95915";
    public static final String CONSOL_DOCUMENT_MASTER_DO_DATE_REQUIRED = "ERR95916";
    public static final String CONSOL_DOCUMENT_MASTER_DO_DATE_INVALID = "ERR95917";
    public static final String CONSOL_DOCUMENT_MASTER_DO_IGM_REQUIRED = "ERR95918";
    public static final String CONSOL_DOCUMENT_MASTER_DO_REFNO_REQUIRED = "ERR95919";
    public static final String CONSOL_DOCUMENT_MASTER_DO_ROUTE_REQUIRED = "ERR95920";


    //Consol Status Update
    public static final String CONSOL_STATUS_TYPE_REQUIRED = "ERR96001";
    public static final String CONSOL_STATUS_TYPE_BLOCKED = "ERR96002";
    public static final String CONSOL_STATUS_TYPE_HIDDEN = "ERR96003";

    public static final String CONSOL_STATUS_NAME_REQUIRED = "ERR96010";
    public static final String CONSOL_STATUS_NAME_BLOCKED = "ERR96011";
    public static final String CONSOL_STATUS_NAME_HIDDEN = "ERR96012";

    public static final String CONSOL_STATUS_ASSIGNED_TO_REQUIRED = "ERR96020";
    public static final String CONSOL_STATUS_ASSIGNED_TO_RESIGNED = "ERR96021";
    public static final String CONSOL_STATUS_ASSIGNED_TO_TERMINATED = "ERR96022";

    public static final String CONSOL_STATUS_FOLLOW_UP_DATE_REQUIRED = "ERR96030";
    public static final String CONSOL_STATUS_FOLLOW_UP_DATE_INVALID = "ERR96031";

    public static final String CONSOL_STATUS_NOTE_REQUIRED = "ERR96040";
    public static final String CONSOL_STATUS_NOTE_INVALID = "ERR96041";

    public static final String CONSOL_STATUS_PROJECT_REQUIRED = "ERR96050";
    public static final String CONSOL_STATUS_PROJECT_INVALID = "ERR96051";

    public static final String CONSOL_STATUS_FOLLOW_UP_REQUIRED = "ERR96060";
    public static final String CONSOL_STATUS_PROTECT_REQUIRED = "ERR96061";

    public static final String CONSOL_ATT_DOC_BLOCKED = "ERR96101";
    public static final String CONSOL_ATT_DOC_HIDDEN = "ERR96102";
    public static final String CONSOL_ATT_REF_NO_REQUIRED = "ERR96103";
    public static final String CONSOL_ATT_REF_NO_INVALID = "ERR96104";
    public static final String CONSOL_REFERENCE_ALREADY_EXIST = "ERR96105";
    public static final String CONSOL_ATT_PROTECTED_FILE_EMPTY = "ERR96106";
    public static final String CONSOL_ATT_PROTECTED_FILE_SIZE_EXCEED = "ERR96107";
    public static final String CONSOL_ATT_UNPROTECTED_FILE_EMPTY = "ERR96108";
    public static final String CONSOL_ATT_UNPROTECTED_FILE_SIZE_EXCEED = "ERR96109";

    public static final String ATA_NOT_LESS_THAN_ATD = "ERR96110";
    public static final String ATD_NOT_GREATER_THAN_ATA = "ERR96111";

    public static final String ETA_NOT_LESS_THAN_ETD = "ERR96112";
    public static final String ETD_NOT_GREATER_THAN_ETA = "ERR96113";

    public static final String CONSOL_GROSS_INVALID = "ERR96114";
    public static final String CONSOL_DOCVOLUME_INVALID = "ERR96115";
    public static final String CONSOL_DOCGROSS_WEIGHT_POUND_INVALID = "ERR96116";
    public static final String CONSOL_DOCVOLUME_WEIGHT_POUND_INVALID = "ERR96117";
    public static final String CONSOL_DOCPIECES_INVALID = "ERR96118";
    public static final String CONSOL_DOCVOLUME_WEIGHT_POUND = "ERR96119";


    public static final String CONSOL_VOLUME_WEIGHT_MISMATCHED = "ERR96120";
    public static final String CONSOL_GROSS_WEIGHT_MISMATCHED = "ERR96121";
    public static final String CONSOL_VOLUME_POUND_MISMATCHED = "ERR96122";
    public static final String CONSOL_GROSSS_POUND_MISMATCHED = "ERR96123";
    public static final String CONSOL_CHARGEBLE_WEIGHT_MISMATCHED = "ERR96124";
    public static final String CONSOL_TOTAL_PIECES_MISMATCHED = "ERR96125";

    // Consol event

    public static final String CONSOL_EVENT = "ERR96500";
    public static final String CONSOL_EVENT_BLOCK = "ERR96501";
    public static final String CONSOL_EVENT_HIDE = "ERR96502";
    public static final String CONSOL_EVENT_DATE = "ERR96503";
    public static final String CONSOL_EVENT_ASSINGED = "ERR96504";
    public static final String CONSOL_EVENT_ASSINGED_BLOCK = "ERR96505";
    public static final String CONSOL_EVENT_ASSINGED_HIDE = "ERR96506";
    public static final String CONSOL_EVENT_FOLLOW_DATE = "ERR96507";
    public static final String CONSOL_RATE_PER_CHARGE = "ERR96514";
    public static final String CONSOL_RATE_PER_CHARGE_INVAILD = "ERR96515";

    public static final String CONSOL_TOTAL_AMOUNT_INVALID = "ERR96516";

    public static final String CONSOL_ETD_MANDATORY = "ERR96517";
    public static final String CONSOL_ETA_MANDATORY = "	ERR96518";
    public static final String CONSOL_CARRIER_MANDATORY = "ERR96519";
    public static final String CONSOL_FLIGHT_NO_MANDATORY = "ERR96520";
    public static final String CONSOL_CARRIER_BLOCK = "ERR96521";
    public static final String CONSOL_CARRIER_HIDE = "ERR96522";
    public static final String CONSOL_MAWB_INVALID = "ERR96523";
    public static final String CONSOL_MAWB_REQUIRED = "ERR96525";
    public static final String LOCATION_NOT_FOR_THE_AGENT = "ERR96529";

    public static final String CONSOL_PARTY_DEFAULTER = "ERR96530";
    public static final String CONSOL_TRAS_DEFAULTER = "ERR96531";
    public static final String CONSOL_PIC_DEFAULTER = "ERR96532";
    public static final String CONSOL_SHIP_DEFAULTER = "ERR96533";
    public static final String CONSOL_CONSIG_DEFAULTER = "ERR96534";
    public static final String CONSOL_FOR_DEFAULTER = "ERR96535";
    public static final String CONSOL_FNOTI_DEFAULTER = "ERR96536";
    public static final String CONSOL_SNOTI_DEFAULTER = "ERR96537";
    public static final String CONSOL_DAGE_DEFAULTER = "ERR96538";
    public static final String CONSOL_ISSAGE_DEFAULTER = "ERR96539";
    public static final String CONSOL_HAVING_INVOICE = "ERR96540";
    public static final String CONNECTION_ETA_CONSOL_ETA_SAME = "ERR96541";

    public static final String REFERNCE_NO_MANDATORY = "ERR96542";
    public static final String CARRIER_DO_MANDATORY = "ERR96543";
    public static final String IGM_NO_MANDATORY = "ERR96544";
    public static final String CONSOL_ALREADY_CREATED_FROM_SHIPMENT = "ERR96560";
    public static final String DO_CANT_RELEASE = "ERR96561";
    public static final String ISSUE_CAN_FIRST = "ERR96562";
    public static final String IATA_MANDATORY = "ERR96563";
    public static final String AGENT_MANDATORY_CONSOL = "ERR96564";


    //roles and privilages
    public static final String ROLE_NAME_REQUIRED = "ERR98000";
    public static final String ROLE_NAME_INVALID = "ERR98001";
    public static final String ROLE_NAME_ALREADY_EXIST = "ERR98002";

    public static final String ROLE_USER_REQUIRED = "ERR98003";
    public static final String ROLE_USER_BLOCKED = "ERR98004";
    public static final String ROLE_USER_ALREADY_EXIST = "ERR98005";
    public static final String ROLE_FEATURE_ALREADY_EXIST = "ERR98006";

    //user profile
    public static final String USER_NAME_ALREADY_EXIST = "ERR99000";
    public static final String USERPROFILE_NOT_NULL = "ERR99001";
    public static final String USERPROFILE_STATUS_NOT_NULL = "ERR99002";
    public static final String USERPROFILE_PASSWORD_NOT_NULL = "ERR99003";
    public static final String USERPROFILE_PASSWORD_LENGTH_EXCEED = "ERR99004";
    public static final String USERPROFILE_NAME_NOT_NULL = "ERR99005";
    public static final String USERPROFILE_NAME_LENGTH_EXCEED = "ERR99006";
    public static final String USER_ID_NOT_NULL = "ERR99007";
    public static final String USER_ID_NOT_FOUND = "ERR99008";
    public static final String USERPROFILE_LIST_EMPTY = "ERR99009";
    public static final String PASSWORD_EXPIRESON_DATE = "ERR99010";
    public static final String USER_COMPANY_ALREADY_EXIST = "ERR99011";
    public static final String USER_COMPANY_LOCATION_ALREADY_EXIST = "ERR99012";
    public static final String USER_COMPANY_LOCATION_REQUIRED = "ERR99013";
    public static final String USER_COMPANY_LOCATION_BLOCKED = "ERR99014";
    public static final String USER_COMPANY_LOCATION_HIDDEN = "ERR99015";

    public static final String USER_ROLE_REQUIRED = "ERR99016";
    public static final String USER_ROLE_BLOCKED = "ERR99017";
    public static final String USER_ROLE_HIDDEN = "ERR99018";


    public static final String USER_ROLE_ALREADY_EXIST = "ERR99019";

    public static final String USER_COMPANY_REQUIRED = "ERR99020";
    public static final String USER_COMPANY_EMPLOYEE_REQUIRED = "ERR99021";
    public static final String USER_COMPANY_AND_ROLE_MANDATORY = "ERR99022";

    public static final String PASSWORD_RULE_INVALIED = "ERR99024";


    // FlightPlan

    public static final String FLIGHTPLAN_CONNECTION_LIST_EMPTY = "ERR99500";
    public static final String FLIGHTPLAN_CONNECTION_CARRIER_MANDATORY = "ERR99501";
    public static final String FLIGHTPLAN_CONNECTION_CARRIER_BLOCKED = "ERR99502";
    public static final String FLIGHTPLAN_CONNECTION_CARRIER_HIDDEN = "ERR99503";
    public static final String FLIGHTPLAN_CONNECTION_FLIGHT_NO_REQUIRED = "ERR99504";
    public static final String FLIGHTPLAN_CONNECTION_FLIGHT_NO_INVALID = "ERR99505";
    public static final String FLIGHTPLAN_CONNECTION_POL_MANDATORY = "ERR99506";
    public static final String FLIGHTPLAN_CONNECTION_POL_BLOCKED = "ERR99507";
    public static final String FLIGHTPLAN_CONNECTION_POL_HIDDEN = "ERR99508";
    public static final String FLIGHTPLAN_CONNECTION_POD_MANDATORY = "ERR99509";
    public static final String FLIGHTPLAN_CONNECTION_POD_BLOCKED = "ERR99510";
    public static final String FLIGHTPLAN_CONNECTION_POD_HIDDEN = "ERR99511";
    public static final String FLIGHTPLAN_CONNECTION_POL_POD_SAME = "ERR99512";
    public static final String FLIGHTPLAN_CONNECTION_ETD_MANDATORY = "ERR99513";
    public static final String FLIGHTPLAN_CONNECTION_ETA_MANDATORY = "ERR99514";
    public static final String FLIGHTPLAN_CONNECTION_ETD_ETA_GREATER = "ERR99515";
    public static final String FLIGHTPLAN_CAPACITY_RESERVED_INVALID = "ERR99516";
    public static final String FLIGHTPLAN_CAPACITY_RESERVED_NOT_NULL = "ERR99517";
    public static final String FLIGHTPLAN_FLIGHT_FREQUENCY_INVALID = "ERR99518";
    public static final String FLIGHTPLAN_FLIGHT_FREQUENCY_NOT_NULL = "ERR99519";
    public static final String FLIGHTPLAN_EFFECTIVE_UPTO_INVALID = "ERR99520";
    public static final String FLIGHTPLAN_EFFECTIVE_UPTO_NOT_NULL = "ERR99521";
    public static final String FLIGHTPLAN_STATUS_INVALID = "ERR99522";
    public static final String FLIGHTPLAN_STATUS_NOT_NULL = "ERR99523";
    public static final String SCHEDULE_ID_ALREADY_EXIST = "ERR99524";
    public static final String FLIGHTPLAN_DUPLICATED = "ERR99525";
    public static final String FLIGHTPLAN_CONNECTIONS_INVALID = "ERR99526";
    public static final String ETD_UPTO_GREATER_THAN_ETD = "ERR99529";
    public static final String ETD_UPTO_SHOULD_NOT_LT_ETA = "ERR99530";
    public static final String ETA_ETD_UPTO_SAME = "ERR99531";


    public static final String STOCK_GENERATION_MANDATORY = "ERR100000";
    public static final String STOCK_CARRIER_NAME_MANDATORY = "ERR100001";
    public static final String STOCK_CARRIER_NAME_BLOCK = "ERR100002";
    public static final String STOCK_CARRIER_NAME_HIDE = "ERR100003";
    public static final String STOCK_REC_DATE_MANDATORY = "ERR100004";
    public static final String STOCK_POR_CODE_MANDATORY = "ERR100005";
    public static final String STOCK_POR_CODE_BLOCK = "ERR100006";
    public static final String STOCK_POR_CODE_HIDE = "ERR100007";
    public static final String STOCK_ENDING_NO_NONZERO = "ERR100008";
    public static final String STOCK_ENDING_NO_GREATER = "ERR100009";
    public static final String STOCK_STARTING_NO_NONZERO = "ERR100010";
    public static final String STOCK_LIST_EMPTY = "ERR100011";
    public static final String STOCK_REMAIND_NO_INVALID = "ERR100012";
    public static final String STOCK_REMAIND_NO_RANGE_INVALID = "ERR100013";

    public static final String STOCK_STARTING_NO_INVALID = "ERR100014";
    public static final String STOCK_ENDING_NO_INVALID = "ERR100015";
    public static final String STATUS_ALREADY_AVAILABLE = "ERR100016";
    public static final String STATUS_ALREADY_CANCELLED = "ERR100017";
    public static final String STOCK_ALREADY_CREATED = "ERR100018";
    public static final String STOCK_MAWB_ALREADY_CREATED = "ERR100019";
    public static final String STOCK_CARRIER_REFNO_INVALID = "ERR100020";
    public static final String STOCK_NO_OF_DAYS_INVALID = "ERR100021";
    public static final String STOCK_NOT_AVIALABLE = "ERR100022";
    public static final String STOCK_ARE_OVER = "ERR100023";
    public static final String STATUS_ALREADY_USING = "ERR100024";
    public static final String STOCK_TIME_OUT = "ERR100025";
    public static final String STOCK_ALREADY_FILLED = "ERR100026";


    public static final String MASTER_CFS_NOT_FOUND = "ERR37066";
    public static final String MASTER_CFS_CODE = "ERR37077";
    public static final String MASTER_CFS_NAME = "ERR37088";
    public static final String MASTER_CFS_CONTACT = "ERR37089";
    public static final String MASTER_CFS_PHONE = "ERR37090";
    public static final String MASTER_CFS_MOBILE = "ERR37091";
    public static final String MASTER_CFS_EMAIL = "ERR37092";
    public static final String MASTER_CFS_ALREADY_CODE = "ERR37099";
    public static final String MASTER_CFS_ALREADY_NAME = "ERR37100";
    public static final String MASTER_CFS_PORT = "ERR37111";
    public static final String MASTER_CFS_PORT_BLOCK = "ERR37122";
    public static final String MASTER_CFS_PORT_HIDE = "ERR37133";
    public static final String MASTER_CFS_DELETE_MESSAGE = "ERR37134";


    /* Air CFS Receive Entry */
    public static final String AIR_CFS_RECEIVE_CFS_REQUIRED = "ERR37200";
    public static final String AIR_CFS_RECEIVE_CFS_BLOCKED = "ERR37201";
    public static final String AIR_CFS_RECEIVE_CFS_HIDDEN = "ERR37202";
    public static final String AIR_CFS_RECEIVE_RECEIPT_DATE_REQUIRED = "ERR37203";
    public static final String AIR_CFS_RECEIVE_RECEIPT_DATE_INVALID = "ERR37204";
    public static final String AIR_CFS_RECEIVE_SERVICE_REQUIRED = "ERR37205";
    public static final String AIR_CFS_RECEIVE_SERVICE_BLOCKED = "ERR37206";
    public static final String AIR_CFS_RECEIVE_SERVICE_HIDDEN = "ERR37207";
    public static final String AIR_CFS_RECEIVE_PRO_NUMBER_REQUIRED = "ERR37208";
    public static final String AIR_CFS_RECEIVE_PRO_NUMBER_INVALID = "ERR37209";
    public static final String AIR_CFS_RECEIVE_PRO_DATE_REQUIRED = "ERR37210";
    public static final String AIR_CFS_RECEIVE_PRO_DATE_INVALID = "ERR37211";
    public static final String AIR_CFS_RECEIVE_SHIPPER_REQUIRED = "ERR37212";
    public static final String AIR_CFS_RECEIVE_SHIPPER_BLOCKED = "ERR37213";
    public static final String AIR_CFS_RECEIVE_SHIPPER_HIDDEN = "ERR37214";
    public static final String AIR_CFS_RECEIVE_CONSIGNEE_REQUIRED = "ERR37215";
    public static final String AIR_CFS_RECEIVE_CONSIGNEE_BLOCKED = "ERR37216";
    public static final String AIR_CFS_RECEIVE_CONSIGNEE_HIDDEN = "ERR37217";
    public static final String AIR_CFS_RECEIVE_TRANSPORTER_REQUIRED = "ERR37218";
    public static final String AIR_CFS_RECEIVE_TRANSPORTER_BLOCKED = "ERR37219";
    public static final String AIR_CFS_RECEIVE_TRANSPORTER_HIDDEN = "ERR37220";
    public static final String AIR_CFS_RECEIVE_WAREHOUSE_LOCATION_REQUIRED = "ERR37221";
    public static final String AIR_CFS_RECEIVE_WAREHOUSE_LOCATION_INVALID = "ERR37222";
    public static final String AIR_CFS_RECEIVE_SHIPPER_REFERENCE_REQUIRED = "ERR37223";
    public static final String AIR_CFS_RECEIVE_SHIPPER_REFERENCE_INVALID = "ERR37224";
    public static final String AIR_CFS_RECEIVE_ON_HAND_REQUIRED = "ERR37225";
    public static final String AIR_CFS_RECEIVE_ON_HAND_INVALID = "ERR37226";
    public static final String AIR_CFS_RECEIVE_NOTES_INVALID = "ERR37227";
    public static final String AIR_CFS_RECEIVE_RECEIPT_DATE_CANT_FUTURE = "ERR37228";


    //	invoice error codes

    public static final String INVOICE_DAYBOOK = "ERR38000";
    public static final String INVOICE_DAYBOOK_BLOCK = "ERR38001";
    public static final String INVOICE_DAYBOOK_HIDE = "ERR38002";
    public static final String INVOICE_AGENT_CUSTOMER = "ERR38003";
    public static final String INVOICE_PARTY_ADDRESS = "ERR38004";
    public static final String INVOICE_PARTY_ADDRESS_1 = "ERR38005";
    public static final String INVOICE_PARTY_ADDRESS_1_LENGTH = "ERR38006";
    public static final String INVOICE_PARTY_ADDRESS_2 = "ERR38007";
    public static final String INVOICE_PARTY_ADDRESS_2_LENGTH = "ERR38008";
    public static final String INVOICE_PARTY_ADDRESS_3_LENGTH = "ERR38009";
    public static final String INVOICE_PARTY_ADDRESS_4 = "ERR38010";
    public static final String INVOICE_PARTY_ADDRESS_4_LENGTH = "ERR38011";
    public static final String INVOICE_PARTY = "ERR38012";
    public static final String INVOICE_PARTY_BLOCK = "ERR38013";
    public static final String INVOICE_PARTY_HIDE = "ERR38014";
    public static final String INVOICE_DATE = "ERR38015";
    public static final String INVOICE_DUE_DATE = "ERR38016";
    public static final String INVOICE_STATUS = "ERR38017";
    public static final String INVOICE_BILLING_CURRENCY = "ERR38018";
    public static final String INVOICE_BILLING_CURRENCY_BLOCK = "ERR38019";
    public static final String INVOICE_BILLING_CURRENCY_HIDE = "ERR38020";
    public static final String INVOICE_ROE = "ERR38021";
    public static final String INVOICE_PARTY_ACCOUNT = "ERR38022";
    public static final String INVOICE_PARTY_ACCOUNT_HIDE = "ERR38023";
    public static final String INVOICE_PARTY_ACCOUNT_BLOCK = "ERR38024";
    public static final String INVOICE_ROE_CANNOT_ZERO_OR_NEGATIVE = "ERR38025";
    public static final String INVOICE_ROE_IS_INVALID = "ERR38026";
    // invoice attachment error codes
    public static final String INVOICE_ATTACHMENT_REFERENCE_NUMBER_REQUIRED = "ERR39000";
    public static final String INVOICE_ATTACHMENT_REFERENCE_NUMBER_INVALID = "ERR39001";
    public static final String INVOICE_ATTACHMENT_REFERENCE_NUMBER_DUPLICATED = "ERR39002";
    public static final String INVOICE_ATTACHMENT_FILE_EMPTY = "ERR39003";
    public static final String INVOICE_ATTACHMENT_FILE_SIZE_EXCEED = "ERR39004";
    public static final String INVOICE_ATTACHMENT_FILE_REQUIRED = "ERR39005";

    // invoice charge error codes
    public static final String INVOICE_CHARGE_SERVICE = "ERR39050";
    public static final String INVOICE_CHARGE_SERVICE_BLOCK = "ERR39051";
    public static final String INVOICE_CHARGE_SERVICE_HIDE = "ERR39052";
    public static final String INVOICE_CHARGE_CHARGE = "ERR39053";
    public static final String INVOICE_CHARGE_CHARGE_BLOCK = "ERR39054";
    public static final String INVOICE_CHARGE_CHARGE_HIDE = "ERR39055";
    public static final String INVOICE_CHARGE_TAXABLE = "ERR39056";
    public static final String INVOICE_CHARGE_DEBIT_CREDIT = "ERR39057";
    public static final String INVOICE_CHARGE_BILLING_AMOUNT = "ERR39058";
    public static final String INVOICE_CHARGE_LOCAL_AMOUNT = "ERR39059";
    public static final String INVOICE_CHARGE_AMOUNT = "ERR39060";
    public static final String INVOICE_CHARGE_AMOUNT_PER_UNIT = "ERR39061";
    public static final String INVOICE_CHARGE_UNIT = "ERR39062";
    public static final String INVOICE_CHARGE_ROE = "ERR39063";
    public static final String INVOICE_CHARGE_CRN = "ERR39064";
    public static final String INVOICE_CHARGE_CURRENCY = "ERR39065";
    public static final String INVOICE_CHARGE_CURRENCY_BLOCK = "ERR39066";
    public static final String INVOICE_CHARGE_CURRENCY_HIDE = "ERR39067";
    public static final String INVOICE_ROE_INVALID = "ERR39068";
    public static final String INVOICE_UNIT_CANNOT_ZERO = "ERR39069";
    public static final String INVOICE_UNIT_INVALID = "ERR39070";

    public static final String INVOICE_AMOUNTPERUNIT_CANNOT_ZERO = "ERR39071";
    public static final String INVOICE_AMOUNTPERUNIT_INVALID = "ERR39072";

    public static final String INVOICE_AMOUNT_CANNOT_ZERO = "ERR39073";
    public static final String INVOICE_AMOUNT_INVALID = "ERR39074";

    public static final String INVOICE_LOCALAMOUNT_CANNOT_ZERO = "ERR39075";
    public static final String INVOICE_LOCALAMOUNT_INVALID = "ERR39076";

    public static final String INVOICE_CURRENCYAMOUNT_CANNOT_ZERO = "ERR39077";
    public static final String INVOICE_CURRENCYAMOUNT_INVALID = "ERR39078";

    public static final String INVOICE_CHARGE_NAME_REQUIRED = "ERR39079";
    public static final String INVOICE_CHARGE_NAME_INVALID = "ERR39080";


    //invoice receipt error codes
    public static final String INVOICE_RECEIPT_DAYBOOK = "ERR40000";
    public static final String INVOICE_RECEIPT_DAYBOOK_BLOCK = "ERR40001";
    public static final String INVOICE_RECEIPT_DAYBOOK_HIDE = "ERR40002";
    public static final String INVOICE_RECEIPT_DAYBOOK_CODE = "ERR40003";
    public static final String INVOICE_RECEIPT_DOCUMNET = "ERR40004";
    public static final String INVOICE_RECEIPT_CURRENCY = "ERR40005";
    public static final String INVOICE_RECEIPT_CURRENCY_HIDE = "ERR40006";
    public static final String INVOICE_RECEIPT_CURRENCY_BLOCK = "ERR40007";
    public static final String INVOICE_RECEIPT_AMOUNT = "ERR40008";
    public static final String INVOICE_PARTY_ACCOUNT_NOT_PRESENT = "ERR40009";
    public static final String INVOICE_SERVICE_MANDATORY = "ERR40010";


    public static final String INVOICE_CHARGE_INVALID = "ERR40011";
    public static final String CREDIT_NOTE_COST_CHARGE_INVALID = "ERR40012";
    public static final String CREDIT_NOTE_REVENUE_CHARGE_INVALID = "ERR40013";

    public static final String ADJ_INVOICE_NO_MANDATORY = "ERR40014";

    public static final String CANNOT_CREATE_INVOICE = "ERR40015";
    public static final String CANNOT_CREATE_CREDITNOTE_COST = "ERR40016";
    public static final String ROE_LOCAL_CURRENCY_CANNOT_MORE_THAN_ONE = "ERR40017";
    public static final String ROE_FOREIGN_CURRENCY_CANNOT_EQUAL_TO_ONE = "ERR40018";

    public static final String ROE_SHOULD_EQUAL_TO_HEADER = "ERR40019";

    public static final String NET_AMOUNT_SHOULD_POSITIVE = "ERR40020";
    public static final String NET_AMOUNT_SHOULD_NEGATIVE = "ERR40021";
    public static final String ONE_LINK_SERVICE_REQUIRED = "ERR40022";
    public static final String REVENUE_NOT_GREATER_THAN_INVOICE = "ERR40023";

    public static final String INVOICE_SUCCESS_MESSAGE = "ERR40024";
    public static final String CREDITNOTE_COST_SUCCESS_MESSAGE = "ERR40025";
    public static final String CREDITNOTE_REVENUE_SUCCESS_MESSAGE = "ERR40026";

    public static final String INVOICE_SERVICE_SIGNOFF_REQ = "ERR40027";
    public static final String INVOICE_MASTER_SIGNOFF_REQ = "ERR40028";

    public static final String INVOICE_SERVICE_ACCOUNTING_NOT_DONE = "ERR40029";
    public static final String INVOICE_MASTER_ACCOUNTING_NOT_DONE = "ERR40030";

    // Auto import process
    public static final String EXPORT_SERVICE_CODE_NOT_NULL = "ERR110010";
    public static final String IMPORT_SERVICE_MASTER_NOT_NULL = "ERR110011";
    public static final String IS_PROCESS_COMPLETED_NOT_NULL = "ERR110012";
    public static final String IS_PROCESS_COMPLETED_NOT_EQUAL = "ERR110013";
    public static final String IMPORT_EXPORT_NOT_EQUAL = "ERR110014";
    public static final String IMPORT_EXPORT_NOT_NULL = "ERR110015";

    public static final String SERVICEDETAIL_EXOPRT_OLD_NOT_NULL = "ERR110016";
    public static final String SERVICEDETAIL_IMPORT_NEW_NOT_NULL = "ERR110017";

    public static final String IMPORT_SERVICE_UID_TO_LINK_NOT_NULL = "ERR110018";

    public static final String CONSOL_OLD_NOT_NULL = "ERR110019";
    public static final String CONSOL_NEW_NOT_NULL = "ERR110020";

    public static final String AUTO_IMPORT_SUCCESS_WITH_CONSOL_LINK = "ERR110021";
    public static final String AUTO_IMPORT_SUCCESS_WITHOUT_CONSOL_LINK = "ERR110022";

    public static final String CONSOL_SERVICE_DOCUMENT_MANDATORY = "ERR96524";
    public static final String INVOICE_CHARGE_NOT_ASSOSIATE_WITH_GL_CHARGE = "ERR97000";

    public static final String CURRENCY_NOT_MAPPED_FOR_COUNTRY = "ERR97001";

    // Document type master error code
    public static final String DOCUMENT_TYPE_LIST_EMPTY = "ERR120000";
    public static final String DOCUMENT_TYPE_NOT_FOUND = "ERR120001";
    public static final String DOCUMENT_TYPE_CODE_NOT_NULL = "ERR120002";
    public static final String DOCUMENT_TYPE_CODE_INVALID = "ERR120003";
    public static final String DOCUMENT_TYPE_NAME_NOT_NULL = "ERR120004";
    public static final String DOCUMENT_TYPE_NAME_INVALID = "ERR120005";
    public static final String DOCUMENT_TYPE_LOV_STATUS_NOT_NULL = "ERR120006";
    public static final String DOCUMENT_TYPE_LOV_STATUS_INVALID = "ERR120007";
    public static final String DOCUMENT_TYPE_LOV_STATUS_LENGTH_EXCEED = "ERR120008";
    public static final String DOCUMENT_TYPE_CODE_ALREADY_EXIST = "ERR120009";
    public static final String DOCUMENT_TYPE_NAME_ALREADY_EXIST = "ERR120010";
    public static final String DOCUMENT_TYPE_ID_NOT_NULL = "ERR120011";
    public static final String DOCUMENT_TYPE_ID_NOT_FOUND = "ERR120012";
    public static final String DOCUMENT_TYPE_CODE_NOT_FOUND = "ERR120013";
    public static final String DOCUMENT_TYPE_DELETE_MESSAGE = "ERR120014";

    // Provision

    public static final String PROVISION_MASTER_ID = "ERR96601";
    public static final String PROVISION_MASTER_ID_INVALID = "ERR96602";

    public static final String PROVISION_SHIPMENT_ID = "ERR96603";
    public static final String PROVISION_SHIPMENT_ID_INVALID = "ERR96604";

    public static final String PROVISION_SERVICE_NAME = "ERR96605";
    public static final String PROVISION_SERVICE_NAME_INVALID = "ERR96606";

    public static final String PROVISION_CHARGE = "ERR96607";
    public static final String PROVISION_CHARGE_BLOCK = "ERR96608";
    public static final String PROVISION_CHARGE_HIDE = "ERR96609";

    public static final String PROVISION_UNIT = "ERR96610";
    public static final String PROVISION_UNIT_BLOCK = "ERR96611";
    public static final String PROVISION_UNIT_HIDE = "ERR96612";

    public static final String PROVISION_TOTAL_REVENUE = "ERR96613";
    public static final String PROVISION_TOTAL_REVENUE_CANNOT_ZERO = "ERR96614";
    public static final String PROVISION_TOTAL_REVENUE_INVALID = "ERR96615";

    public static final String PROVISION_TOTAL_COST = "ERR96616";
    public static final String PROVISION_TOTAL_COST_CANNOT_ZERO = "ERR96617";
    public static final String PROVISION_TOTAL_COST_INVALID = "ERR96618";

    public static final String PROVISION_NET = "ERR96619";
    public static final String PROVISION_NET_CANNOT_ZERO = "ERR96620";
    public static final String PROVISION_NET_INVALID = "ERR96621";


    public static final String PROVISION_SELL_CURRENCY = "ERR96622";
    public static final String PROVISION_SELL_CURRENCY_BLOCK = "ERR96623";
    public static final String PROVISION_SELL_CURRENCY_HIDE = "ERR96624";

    public static final String PROVISION_SELL_ROE = "ERR96625";
    public static final String PROVISION_SELL_ROE_CANNOT_ZERO = "ERR96626";
    public static final String PROVISION_SELL_ROE_INVALID = "ERR96627";

    public static final String PROVISION_SELL_AMOUNT_PER_UNIT = "ERR96628";
    public static final String PROVISION_SELL_AMOUNT_PER_UNIT_CANNOT_ZERO = "ERR96629";
    public static final String PROVISION_SELL_AMOUNT_PER_UNIT_INVALID = "ERR96630";

    public static final String PROVISION_SELL_AMOUNT = "ERR96631";
    public static final String PROVISION_SELL_AMOUNT_CANNOT_ZERO = "ERR96632";
    public static final String PROVISION_SELL_AMOUNT_INVALID = "ERR96633";

    public static final String PROVISION_SELL_LOCAL_AMOUNT = "ERR96634";
    public static final String PROVISION_SELL_LOCAL_AMOUNT_CANNOT_ZERO = "ERR96635";
    public static final String PROVISION_SELL_LOCAL_AMOUNT_INVALID = "ERR96636";

    public static final String PROVISION_SELL_TAX = "ERR96637";
    public static final String PROVISION_SELL_Tax_INVALID = "ERR96638";

    public static final String PROVISION_BUY_CURRENCY = "ERR96639";
    public static final String PROVISION_BUY_CURRENCY_BLOCK = "ERR96640";
    public static final String PROVISION_BUY_CURRENCY_HIDE = "ERR96641";

    public static final String PROVISION_BUY_ROE = "ERR96642";
    public static final String PROVISION_BUY_ROE_CANNOT_ZERO = "ERR96643";
    public static final String PROVISION_BUY_ROE_INVALID = "ERR96644";

    public static final String PROVISION_BUY_AMOUNT_PER_UNIT = "ERR96645";
    public static final String PROVISION_BUY_AMOUNT_PER_UNIT_CANNOT_ZERO = "ERR96646";
    public static final String PROVISION_BUY_AMOUNT_PER_UNIT_INVALID = "ERR96647";

    public static final String PROVISION_BUY_AMOUNT = "ERR96648";
    public static final String PROVISION_BUY_AMOUNT_CANNOT_ZERO = "ERR96649";
    public static final String PROVISION_BUY_AMOUNT_INVALID = "ERR96650";

    public static final String PROVISION_BUY_LOCAL_AMOUNT = "ERR96651";
    public static final String PROVISION_BUY_LOCAL_AMOUNT_CANNOT_ZERO = "ERR96652";
    public static final String PROVISION_BUY_LOCAL_AMOUNT_INVALID = "ERR96653";

    public static final String PROVISION_BILL_TO_PARTY = "ERR96654";
    public static final String PROVISION_BILL_TO_PARTY_INVALID = "ERR96655";

    public static final String PROVISION_VENDOR_SUBLEDGER = "ERR96656";
    public static final String PROVISION_VENDOR_SUBLEDGER_INVALID = "ERR96657";

    public static final String PROVISION_SELL_DESCRIPTION = "ERR96658";
    public static final String PROVISION_SELL_DESCRIPTION_INVALID = "ERR96659";

    public static final String PROVISION_BUY_DESCRIPTION = "ERR96660";
    public static final String PROVISION_BUY_DESCRIPTION_INVALID = "ERR96661";

    public static final String PROVISION_REFERENCE_TYPE = "ERR96662";
    public static final String PROVISION_REFERENCE_TYPE_BLOCK = "ERR96663";
    public static final String PROVISION_REFERENCE_TYPE_HIDE = "ERR96664";

    public static final String PROVISION_NO_OF_UNITS = "ERR96665";
    public static final String PROVISION_NO_OF_UNITS_CANNOT_ZERO = "ERR96666";
    public static final String PROVISION_NO_OF_UNITS_INVALID = "ERR96667";

    public static final String PROVISION_SERVICE_ID = "ERR96668";
    public static final String PROVISION_SERVICE_ID_INVALID = "ERR96669";

    public static final String PROVISION_REFERENCE = "ERR96670";
    public static final String PROVISION_REFERENCE_INVALID = "ERR96671";

    public static final String PROVISION_BILLTOPARTY_BLOCK = "ERR96672";
    public static final String PROVISION_BILLTOPARTY_HIDE = "ERR96673";

    public static final String PROVISION_VENDOR_BLOCK = "ERR96674";
    public static final String PROVISION_VENDOR_HIDE = "ERR96675";
    public static final String CANNOT_CREATE_PROVISION = "ERR96676";

    public static final String PROVISIONAL_CHARGES_NOT_CREATED = "ERR96678";


    //daybook master error codes
    public static final String DAYBOOK_LIST_EMPTY = "ERR200000";
    public static final String DAYBOOK_NOT_FOUND = "ERR200001";
    public static final String DAYBOOK_CODE_NOT_NULL = "ERR200002";
    public static final String DAYBOOK_CODE_INVALID = "ERR200003";
    public static final String DAYBOOK_NAME_NOT_NULL = "ERR200004";
    public static final String DAYBOOK_NAME_INVALID = "ERR200005";
    public static final String DAYBOOK_LOV_STATUS_NOT_NULL = "ERR200006";
    public static final String DAYBOOK_LOV_STATUS_INVALID = "ERR200007";
    public static final String DAYBOOK_LOV_STATUS_LENGTH_EXCEED = "ERR200008";
    public static final String DAYBOOK_CODE_ALREADY_EXIST = "ERR200009";
    public static final String DAYBOOK_NAME_ALREADY_EXIST = "ERR200010";
    public static final String DAYBOOK_ID_NOT_NULL = "ERR200011";
    public static final String DAYBOOK_ID_NOT_FOUND = "ERR200012";
    public static final String DAYBOOK_CODE_NOT_FOUND = "ERR200013";
    public static final String DAYBOOK_LOCATION_NOT_NULL = "ERR200027";
    public static final String DAYBOOK_LOCATION_BLOCKED = "ERR200014";
    public static final String DAYBOOK_LOCATION_HIDDEN = "ERR200015";
    public static final String DAYBOOK_BLOCKED = "ERR200018";
    public static final String DAYBOOK_HIDDEN = "ERR200019";
    public static final String DAYBOOK_DOCUMENTTYPE_BLOCKED = "ERR200020";
    public static final String DAYBOOK_DOCUMENTTYPE_HIDDEN = "ERR200021";
    public static final String DAYBOOK_DOCUMENTTYPE_NULL = "ERR200022";
    public static final String DAYBOOK_DELETE_MESSAGE = "ERR200023";
    public static final String DAYBOOK_CHEQUENOFROM_INVALID = "ERR200024";
    public static final String DAYBOOK_CHEQUENOTO_INVALID = "ERR200025";
    public static final String DAYBOOK_CHEQUEREPORTNAME_INVALID = "ERR200026";
    public static final String DAYBOOK_CASH_ACCOUNT_NOT_NULL = "ERR200028";
    public static final String DAYBOOK_CASH_ACCOUNT_BLOCKED = "ERR200016";
    public static final String DAYBOOK_CASH_ACCOUNT_HIDDEN = "ERR200017";
    public static final String DAYBOOK_BANK_ACCOUNT_NOT_NULL = "ERR200029";
    public static final String DAYBOOK_BANK_ACCOUNT_BLOCKED = "ERR200030";
    public static final String DAYBOOK_BANK_ACCOUNT_HIDDEN = "ERR200031";
    public static final String DAYBOOK_PDC_ACCOUNT_NOT_NULL = "ERR200032";
    public static final String DAYBOOK_PDC_ACCOUNT_BLOCKED = "ERR200033";
    public static final String DAYBOOK_PDC_ACCOUNT_HIDDEN = "ERR200034";
    public static final String DAYBOOK_EXCHANGE_GAIN_ACCOUNT_BLOCKED = "ERR200035";
    public static final String DAYBOOK_EXCHANGE_GAIN_PDC_ACCOUNT_HIDDEN = "ERR200036";
    public static final String DAYBOOK_EXCHANGE_LOSS_ACCOUNT_BLOCKED = "ERR200037";
    public static final String DAYBOOK_EXCHANGE_LOSS_ACCOUNT_HIDDEN = "ERR200038";
    public static final String ONE_BANK_ACCOUNT_MANDATORY = "ERR200039";


    public static final String REASON_LIST_EMPTY = "ERR201000";
    public static final String REASON_NOT_FOUND = "ERR201001";
    public static final String REASON_CODE_NOT_NULL = "ERR201002";
    public static final String REASON_CODE_INVALID = "ERR201003";
    public static final String REASON_NAME_NOT_NULL = "ERR201004";
    public static final String REASON_NAME_INVALID = "ERR201005";
    public static final String REASON_LOV_STATUS_NOT_NULL = "ERR201006";
    public static final String REASON_LOV_STATUS_INVALID = "ERR201007";
    public static final String REASON_LOV_STATUS_LENGTH_EXCEED = "ERR201008";
    public static final String REASON_CODE_ALREADY_EXIST = "ERR201009";
    public static final String REASON_NAME_ALREADY_EXIST = "ERR201010";
    public static final String REASON_ID_NOT_NULL = "ERR201011";
    public static final String REASON_ID_NOT_FOUND = "ERR201012";
    public static final String REASON_CODE_NOT_FOUND = "ERR201013";

    //ERR201015 used for  aes file


    public static final String PARTY_GROUP_CODE_ALREADY_EXIST = "ERR5001";
    public static final String PARTY_GROUP_NAME_ALREADY_EXIST = "ERR5002";
    public static final String PARTY_GROUP_PARTY_ALREADY_EXIST = "ERR5003";
    public static final String PARTY_GROUP_CODE_INVALID = "ERR5004";
    public static final String PARTY_GROUP_NAME_INVALID = "ERR5005";
    public static final String PARTY_GROUP_CODE_REQUIRED = "ERR5006";
    public static final String PARTY_GROUP_NAME_REQUIRED = "ERR5007";
    public static final String PARTY_GROUP_LOCATION_BLOCK = "ERR5008";
    public static final String PARTY_GROUP_LOCATION_HIDDEN = "ERR5009";
    public static final String PARTY_GROUP_PARTY_BLOCK = "ERR5010";
    public static final String PARTY_GROUP_PARTY_HIDDEN = "ERR5011";
    public static final String PARTY_GROUP_PARTY_REQUIRED = "ERR5012";


    public static final String CARRIER_RATE_REQUIRED = "ERR1260";
    public static final String DESTINATION_STATUS_HIDDEN = "ERR1261";
    public static final String ORIGIN_STATUS_HIDDEN = "ERR1262";
    public static final String CARRIER_RATE_UNIT_REQUIRED = "ERR1263";
    public static final String CARRIER_RATE_UNIT_BLOCKED = "ERR1264";
    public static final String CARRIER_RATE_UNIT_HIDDEN = "ERR1265";
    public static final String CARRIER_RATE_CARRIER_REQUIRED = "ERR1266";
    public static final String CARRIER_RATE_CARRIER_BLOCKED = "ERR1267";
    public static final String CARRIER_RATE_CARRIER_HIDDEN = "ERR1268";
    public static final String CARRIER_RATE_CHARGE_REQUIRED = "ERR1269";
    public static final String CARRIER_RATE_CHARGE_BLOCKED = "ERR1270";
    public static final String CARRIER_RATE_CHARGE_HIDDEN = "ERR1271";
    public static final String FIRST_AMOUNT_NOT_ZERO = "ERR1272";
    public static final String FIRST_MIN_AMOUNT_NOT_ZERO = "ERR1273";
    public static final String SECOND_AMOUNT_NOT_ZERO = "ERR1274";
    public static final String SECOND_MIN_AMOUNT_NOT_ZERO = "ERR1275";
    public static final String DOUBLE_VALUE_INVALID = "ERR1276";
    public static final String FIRST_AMOUNT_NOT_NULL = "ERR1292";


    public static final String PARTY_CONTACT_SALUTATION_REQUIRED = "ERR5100";
    public static final String PARTY_CONTACT_FIRST_NAME_REQUIRED = "ERR5101";
    public static final String PARTY_CONTACT_LAST_NAME_REQUIRED = "ERR5102";
    public static final String PARTY_CONTACT_FIRST_NAME_INVALID = "ERR5103";
    public static final String PARTY_CONTACT_LAST_NAME_INVALID = "ERR5104";

    public static final String PARTY_CONTACT_OFFICIAL_DESIGNATION_INVALID = "ERR5105";
    public static final String PARTY_CONTACT_OFFICIAL_DEPARTMENT_INVALID = "ERR5106";
    public static final String PARTY_CONTACT_OFFICIAL_ADDRESS_INVALID = "ERR5107";
    public static final String PARTY_CONTACT_OFFICIAL_PHONE_NO_INVALID = "ERR5108";
    public static final String PARTY_CONTACT_OFFICIAL_MOBILE_NO_INVALID = "ERR5109";
    public static final String PARTY_CONTACT_OFFICIAL_FAX_INVALID = "ERR5110";
    public static final String PARTY_CONTACT_OFFICIAL_EMAIL_INVALID = "ERR5111";
    public static final String PARTY_CONTACT_OFFICIAL_ASSISTENT_NAME_INVALID = "ERR5112";
    public static final String PARTY_CONTACT_OFFICIAL_ASSISTENT_PHONE_INVALID = "ERR5113";
    public static final String PARTY_CONTACT_OFFICIAL_PREFERRED_CALL_TIME_INVALID = "ERR5114";
    public static final String PARTY_CONTACT_OFFICIAL_REPORTING_TO_INVALID = "ERR5115";


    public static final String PARTY_CONTACT_PERSONAL_ADDRESS_INVALID = "ERR5118";
    public static final String PARTY_CONTACT_PERSONAL_PHONE_NO_INVALID = "ERR5119";
    public static final String PARTY_CONTACT_PERSONAL_MOBILE_NO_INVALID = "ERR5120";
    public static final String PARTY_CONTACT_PERSONAL_FAX_INVALID = "ERR5121";
    public static final String PARTY_CONTACT_PERSONAL_EMAIL_INVALID = "ERR5122";
    public static final String PARTY_CONTACT_FORMER_COMPANY_INVALID = "ERR5123";
    public static final String PARTY_CONTACT_PERSONAL_QUALIFICATION_INVALID = "ERR5124";
    public static final String PARTY_CONTACT_PERSONAL_CHILDREN_INVALID = "ERR5125";

    public static final String PARTY_CONTACT_REMARKS_NOTES_INVALID = "ERR5126";


    public static final String PARTY_CONTACT_RELATION_SALUTATION_INVALID = "ERR5127";
    public static final String PARTY_CONTACT_RELATION_FIRST_NAME_INVALID = "ERR5128";
    public static final String PARTY_CONTACT_RELATION_LAST_NAME_INVALID = "ERR5129";
    public static final String PARTY_CONTACT_RELATION_NOTES_INVALID = "ERR5130";

    public static final String CARRIER_RATE_ALREADY_EXIST = "ERR1296";
    public static final String CARRIER_RATE_CSV_FILE_REQUIRED = "ERR1297";
    public static final String CARRIER_RATE_CSV_FILE_FORMAT = "ERR1298";
    public static final String CARRIER_RATE_CSV_FILE_EMPTY = "ERR1299";
    public static final String CARRIER_RATE_CSV_FILE_LENGTH_EXCEED = "ERR1300";


    /*Document Issue Restriction Codes Starts Here*/
    public static final String DOCUMENT_ISSUE_RESTRICTION_COMPANY_REQUIRED = "ERR0131";
    public static final String DOCUMENT_ISSUE_RESTRICTION_COMPANY_BLOCKED = "ERR0132";
    public static final String DOCUMENT_ISSUE_RESTRICTION_COMPANY_HIDDEN = "ERR0133";
    public static final String DOCUMENT_ISSUE_RESTRICTION_COMPANY_INVALID = "ERR0134";

    public static final String DOCUMENT_ISSUE_RESTRICTION_LOCATION_REQUIRED = "ERR0135";
    public static final String DOCUMENT_ISSUE_RESTRICTION_LOCATION_BLOCKED = "ERR0136";
    public static final String DOCUMENT_ISSUE_RESTRICTION_LOCATION_HIDDEN = "ERR0137";
    public static final String DOCUMENT_ISSUE_RESTRICTION_LOCATION_INVALID = "ERR0138";

    public static final String DOCUMENT_ISSUE_RESTRICTION_SERVICE_REQUIRED = "ERR0139";
    public static final String DOCUMENT_ISSUE_RESTRICTION_SERVICE_BLOCKED = "ERR0140";
    public static final String DOCUMENT_ISSUE_RESTRICTION_SERVICE_HIDDEN = "ERR0141";
    public static final String DOCUMENT_ISSUE_RESTRICTION_SERVICE_INVALID = "ERR0142";
    public static final String DOCUMENT_ISSUE_RESTRICTION_SERVICE_COMP_DUPLICATE = "ERR0143";

    public static final String SHIPMENT_INVOICE_CHECKING_INVALID_SHIPMENT_REQUESTED = "ERR0144";
    public static final String SHIPMENT_INVOICE_CHECKING_INVALID_SHIPMENT_LOCATION = "ERR0145";
    public static final String SHIPMENT_INVOICE_CHECKING_INVALID_SHIPMENT_COMPANY = "ERR0146";
    public static final String SHIPMENT_INVOICE_CHECKING_INVALID_SHIPMENT_SERVICE = "ERR0147";
    public static final String SHIPMENT_INVOICE_CHECKING_INVALID_SHIPMENT_TERM = "ERR0148";
    public static final String SHIPMENT_INVOICE_CHECKING_INVOICE_NOT_CREATED = "ERR0149";
    public static final String COMP_UK_DOC_ISSUE_RES_COMPANY_lOC_SERVICE = "ERR0150";
    public static final String FK_DOC_ISS_RES_DOC_ID = "ERR0151";
    public static final String FK_DOC_ISS_RES_TOS_ID = "ERR0152";
    public static final String UK_DOC_ISS_RES_DOC_TOS = "ERR0153";

		/*Document Issue Restriction Codes Ends Here*/

    /*Dynamic Fields Codes Starts Here*/
    public static final String DYNAMIC_FIELD_DATA_IS_INVALID = "ERR0170";
    public static final String DYNAMIC_FIELD_NAME_IS_REQUIRED = "ERR0171";
    public static final String DYNAMIC_FIELD_NAME_IS_INVALID = "ERR0172";
    public static final String UK_DYNAMIC_FIELD_NAME = "ERR0173";

    public static final String DYNAMIC_FIELD_ID_IS_REQUIRED = "ERR0174";
    public static final String DYNAMIC_FIELD_ID_IS_INVALID = "ERR0175";
    public static final String UK_DYNAMIC_FIELD_ID = "ERR0176";
    public static final String DYNAMIC_FIELD_SCREEN_IS_INVALID = "ERR0177";
    public static final String DYNAMIC_FIELD_SECTION_OR_TAB_IS_INVALID = "ERR0178";
    public static final String DYNAMIC_FIELD_REMARK = "ERR0179";

    public static final String COUNTRY_DYNAMIC_FIELD_COUNTRY_IS_REQUIRED = "ERR0180";
    public static final String COUNTRY_UK_DYNAMIC_FIELD = "ERR0181";
    public static final String COUNTRY_DYNAMIC_FIELD_COUNTRY_IS_BLOCKED = "ERR0182";
    public static final String COUNTRY_DYNAMIC_FIELD_COUNTRY_IS_HIDDEN = "ERR0183";

    //Import To Export

    public static final String IMPORTTOEXPORT_AGENT_NOT_NULL = "ERR0200";
    public static final String IMPORTTOEXPORT_AGENT_BLOCKED = "ERR0201";
    public static final String IMPORTTOEXPORT_AGENT_HIDE = "ERR0202";
    public static final String IMPORTTOEXPORT_AGENT_DEFAULTER = "ER0214";

    public static final String IMPORTTOEXPORT_AGENT_ADDRESS_NOT_NULL = "ERR0216";
    public static final String IMPORTTOEXPORT_AGENT_ADDRESS_LINE1_REQUIRED = "ERR0203";
    public static final String IMPORTTOEXPORT_AGENT_ADDRESS_LINE1_INVALID = "ERR0204";

    public static final String IMPORTTOEXPORT_AGENT_ADDRESS_LINE2_REQUIRED = "ERR0205";
    public static final String IMPORTTOEXPORT_AGENT_ADDRESS_LINE2_INVALID = "ERR0206";

    public static final String IMPORTTOEXPORT_AGENT_ADDRESS_LINE3_REQUIRED = "ERR0207";
    public static final String IMPORTTOEXPORT_AGENT_ADDRESS_LINE3_INVALID = "ERR0208";

    public static final String IMPORTTOEXPORT_AGENT_ADDRESS_LINE4_REQUIRED = "ERR0209";
    public static final String IMPORTTOEXPORT_AGENT_ADDRESS_LINE4_INVALID = "ERR0210";

    public static final String IMPORTTOEXPORT_SERVICE = "ERR0211";
    public static final String IMPORTTOEXPORT_SERVICE_BLOCK = "ERR0212";
    public static final String IMPORTTOEXPORT_SERVICE_HIDE = "ER0213";

    public static final String IMPORTTOEXPORT_GENERATED_SUCCESSFULLY = "ERR0215";


    // AES ERROR CODES

    public static final String SERVICE_UID_CANNOT_NULL = "ERR201015";
    public static final String SHIPMENT_UID_CANNOT_NULL = "ERR201016";
    public static final String MASTER_UID_CANNOT_NULL = "ERR201017";
    public static final String AES_CANNOT_NULL = "ERR201018";
    public static final String AES_FILLING_CANNOT_NULL = "ERR201019";
    public static final String AES_TRANSPORT_CANNOT_NULL = "ERR201020";
    public static final String AES_ROUTED_CANNOT_NULL = "ERR201021";
    public static final String AES_ULTIMATE_CONSIGNEE_CANNOT_NULL = "ERR201022";
    public static final String AES_IN_BOND_CANNOT_NULL = "ERR201023";
    public static final String AES_ORIGIN_STATE_CANNOT_NULL = "ERR201024";
    public static final String AES_ORIGIN_STATE_STATUS_BLOCK = "ERR201025";
    public static final String AES_ORIGIN_STATE_STATUS_HIDE = "ERR2010256";

    public static final String AES_DESTINATION_COUNTRY_CANNOT_NULL = "ERR201027";
    public static final String AES_DESTINATION_COUNTRY_STATUS_BLOCK = "ERR201028";
    public static final String AES_DESTINATION_COUNTRY_STATUS_HIDE = "ERR2010229";

    public static final String AES_CARRIER_CANNOT_NULL = "ERR201030";
    public static final String AES_CARRIER_STATUS_BLOCK = "ERR201031";
    public static final String AES_CARRIER_STATUS_HIDE = "ERR201032";

    public static final String AES_POD_CANNOT_NULL = "ERR201033";
    public static final String AES_POD_STATUS_BLOCK = "ERR201034";
    public static final String AES_POD_STATUS_HIDE = "ERR201035";

    public static final String AES_POL_CANNOT_NULL = "ERR201036";
    public static final String AES_POL_STATUS_BLOCK = "ERR201037";
    public static final String AES_POL_STATUS_HIDE = "ERR201038";

    public static final String AES_TRANSPORT_BLOCK = "ERR201039";
    public static final String AES_TRANSPORT_HIDE = "ERR201040";
    public static final String HAWB_NO_MANDATORY = "ERR201041";
    public static final String MAWB_NO_MANDATORY = "ERR201042";
    public static final String AES_ACTION_MANDATORY = "ERR201043";
    public static final String AES_FILLING_TYPE_MANDATORY = "ERR201044";
    public static final String AES_ETD_MANDATORY = "ERR201045";
    public static final String AES_IATA_CODE_MANDATORY = "ERR201046";
    public static final String AES_IATA_BLOCK = "ERR201047";
    public static final String AES_IATA_HIDE = "ERR201048";
    public static final String AES_HAZARDOUS_CARGO_MANDATORY = "ERR201049";
    public static final String AES_IN_BOND_MANDATORY = "ERR201050";
    public static final String AES_IN_BOND_BLOCK = "ERR201051";
    public static final String AES_IN_BOND_HIDE = "ERR201052";
    public static final String AES_IMPORT_ENTRY_NO_INVALID = "ERR201053";

    public static final String AES_USPPI_MANDATORY = "ERR201054";
    public static final String AES_USPPI_SHIPPER_MANDATORY = "ERR201055";
    public static final String AES_USPPI_SHIPPER_BLOCK = "ERR201056";
    public static final String AES_USPPI_SHIPPER_HIDE = "ERR201057";
    public static final String AES_USPPI_SHIPPER_DEFAULTER = "ERR201058";
    public static final String AES_USPPI_SHIPPER_ADDRESS_MANDATORY = "ERR201059";
    public static final String AES_USPPI_SHIPPER_CONTACT_NO_MANDATORY = "ERR201060";

    public static final String AES_USPPI_SHIPPER_CITY_MANDATORY = "ERR201061";
    public static final String AES_USPPI_SHIPPER_STATE_MANDATORY = "ERR201062";
    public static final String AES_USPPI_SHIPPER_ZIP_MANDATORY = "ERR201063";
    public static final String AES_USPPI_SHIPPER_ID_TYPE_MANDATORY = "ERR201064";

    public static final String AES_ORIGIN_STATE_MANDATORY = "ERR201065";
    public static final String AES_ORIGIN_STATE_BLOCK = "ERR201066";
    public static final String AES_ORIGIN_STATE_HIDE = "ERR201067";

    public static final String AES_COMMODITY_ACTION_MANDATORY = "ERR201068";
    public static final String AES_COMMODITY_GOODS_MANDATORY = "ERR201069";
    public static final String AES_COMMODITY_LICENSE_MANDATORY = "ERR201070";

    public static final String AES_COMMODITY_LICENSE_BLOCK = "ERR201071";
    public static final String AES_COMMODITY_LICENSE_HIDE = "ERR201072";

    public static final String AES_COMMODITY_SCHEDULAR_MANDATORY = "ERR201073";
    public static final String AES_SCHEDULAR_BLOCK = "ERR201074";
    public static final String AES_SCHEDULAR_HIDE = "ERR201075";

    public static final String AES_COMMODITY_DESC_MANDATORY = "ERR201076";
    public static final String AES_COMMODITY_DESC_BLOCK = "ERR201077";
    public static final String AES_COMMODITY_DESC_HIDE = "ERR201078";
    public static final String AES_COMMODITY_FRSTPIECES_MANDATORY = "ERR201079";
    public static final String AES_COMMODITY_SECONDPIECES_MANDATORY = "ERR201080";

    public static final String AES_GROSS_WEIGHT_MANDATORY = "ERR201081";
    public static final String AES_COMMODITY_ITAR_BLOCK = "ERR201082";
    public static final String AES_COMMODITY_ITAR_HIDE = "ERR201083";
    public static final String AES_DTDC_LENGTH_INVALID = "ERR201084";

    public static final String AES_SME_INDICATOR_MANDATORY = "ERR201085";
    public static final String AES_CATEGORY_MANDATORY = "ERR201086";
    public static final String AES_CATEGORY_BLOCK = "ERR201087";
    public static final String AES_CATEGORY_HIDE = "ERR201088";

    public static final String AES_UNIT_CODE_MANDATORY = "ERR201089";
    public static final String AES_UNIT_CODE_BLOCK = "ERR201090";
    public static final String AES_UNIT_CODE_HIDE = "ERR201091";

    public static final String AES_QUANTITY_MANDATORY = "ERR201092";
    public static final String AES_QUANTITY_INVALID = "ERR201093";

    public static final String AES_EXPORT_CODE_MANDATORY = "ERR201094";
    public static final String AES_EXPORT_CODE_BLOCK = "ERR201095";
    public static final String AES_EXPORT_CODE_HIDE = "ERR201096";

    public static final String AES_ECCN_LENGTH_INVALID = "ERR201097";

    public static final String AES_EXPORT_LICENSE_LENGTH_INVALID = "ERR201098";
    public static final String AES_LICENSE_VALUE_MANDATORY = "ERR201099";

    public static final String AES_LICENSE_VALUE_LENGTH_INVALID = "ERR201100";
    public static final String AES_USED_VEHICLE_VALUE_MANDATORY = "ERR201101";
    public static final String AES_VIN_PRODUCT_LENGTH_INVALID = "ERR201102";

    public static final String AES_VEHICLE_TITLE_LENGTH_INVALID = "ERR201103";
    public static final String AES_VEHICLE_TITLE_BLOCK = "ERR201104";
    public static final String AES_VEHICLE_TITLE_HIDE = "ERR201105";

    public static final String AES_FREIGHT_FORWARDER_MANDATORY = "ERR201106";
    public static final String AES_FREIGHT_FORWARDER_BLOCK = "ERR201107";
    public static final String AES_FREIGHT_FORWARDER_HIDE = "ERR201108";
    public static final String AES_FREIGHT_FORWARDER_DEFAULTER = "ERR201112";
    public static final String AES_FREIGHT_FORWARDER_ADDRESS = "ERR201113";
    public static final String AES_FREIGHT_FORWARDER_CITY = "ERR201114";
    public static final String AES_FREIGHT_FORWARDER_COUNTRY = "ERR201115";

    public static final String AES_FORWARDER_FRST_NAME_INVALID = "ERR201192";
    public static final String AES_FORWARDER_SECND_NAME_INVALID = "ERR201193";

    public static final String AES_FORWARDER_FIRST_NAME_INVALID = "ERR201192";
    public static final String AES_FORWARDER_SECOND_NAME_INVALID = "ERR201193";

    public static final String AES_FREIGHT_FORWARDER_STATE_MANDATORY = "ERR201194";

    public static final String AES_FREIGHT_FORWARDER_STATE_BLOCK = "ERR201195";
    public static final String AES_FREIGHT_FORWARDER_STATE_HIDE = "ERR201196";

    public static final String AES_FREIGHT_FORWARDER_CITY_BLOCK = "ERR201197";
    public static final String AES_FREIGHT_FORWARDER_CITY_HIDE = "ERR201198";

    public static final String AES_FREIGHT_FORWARDER_ID_NO_INVALID = "ERR2011989";
    public static final String AES_FORWARDER_FRST_NAME_MANDATORY = "ERR2011990";
    public static final String AES_FORWARDER_SECND_NAME_MANDATORY = "ERR2011991";

    public static final String AES_CONTACT_NO_INVALID = "ERR2011992";
    public static final String AES_ZIP_CODE_INVALID = "ERR2011993";

    public static final String AES_FREIGHT_FORWARDER_COUNTRY_BLOCK = "ERR2011994";
    public static final String AES_FREIGHT_FORWARDER_COUNTRY_HIDE = "ERR2011995";

    public static final String AES_INTERMEDIATE_CONSIGNEE_BLOCK = "ERR201109";
    public static final String AES_ULTIMATE_CONSIGNEE_MANDATORY = "ERR201110";
    public static final String AES_ULTIMATE_CONSIGNEE_BLOCK = "ERR201111";

    public static final String AES_INTERMEDIATE_CONSIGNEE_HIDE = "ERR201118";
    public static final String AES_INTERMEDIATE_CONSIGNEE_DEFAULTER = "ERR201119";

    public static final String AES_ULTIMATE_CONSIGNEE_HIDE = "ERR201120";

    public static final String AES_ULTIMATE_CONSIGNEE_DEFAULTER = "ERR201121";
    public static final String AES_ULTIMATE_CONSIGNEE_ADDRESS = "ERR201122";
    public static final String AES_ULTIMATE_CONSIGNEE_CITY = "ERR201123";
    public static final String AES_ULTIMATE_CONSIGNEE_COUNTRY = "ERR201124";
    public static final String AES_SELECTED_POL_NOT_IN_COUNTRY = "ERR201125";

    public static final String AES_CONSIGNEE_ID_NO_INVALID = "ERR201126";
    public static final String AES_CONSIGNEE_FIRST_NAME_INVALID = "ERR201127";
    public static final String AES_CONSIGNEE_SECOND_NAME_INVALID = "ERR201128";

    public static final String AES_PIECES_NAME_INVALID = "ERR201129";

    public static final String AES_TRANSPORT_ID_MANDATORY = "ERR201130";

    public static final String AES_SCAC_MANDATORY = "ERR201131";

    public static final String AES_IATA_ID_MANDATORY = "ERR201132";

    public static final String AES_INBOND_TYPE_ID_MANDATORY = "ERR201133";

    public static final String AES_COMMODITY_MANDATORY = "ERR201134";

    public static final String AES_USPPI_CARGO_MANDATORY = "ERR201143";
    public static final String AES_USPPI_CARGO_BLOCK = "ERR201144";
    public static final String AES_USPPI_CARGO_HIDE = "ERR201145";
    public static final String AES_USPPI_CARGO_DEFAULTER = "ERR201146";

    public static final String AES_ULTIMATE_CONSIGNEE_COMPANY_MANDATORY = "ERR201147";
    public static final String AES_ULTIMATE_CONSIGNEE_COMPANY_BLOCK = "ERR201148";
    public static final String AES_ULTIMATE_CONSIGNEE_COMPANY_HIDE = "ERR201149";
    public static final String AES_ULTIMATE_CONSIGNEE_COMPANY_DEFAULTER = "ERR201150";

    public static final String AES_ULTIMATE_CONSIGNEE_COMPANY_ADDRESS_1_MANDATORY = "ERR201151";
    public static final String AES_ULTIMATE_CONSIGNEE_COMPANY_ADDRESS_1_LENGTH = "ERR201152";
    public static final String AES_ULTIMATE_CONSIGNEE_COMPANY_ADDRESS_2_MANDATORY = "ERR201153";
    public static final String AES_ULTIMATE_CONSIGNEE_COMPANY_ADDRESS_2_LENGTH = "ERR201154";
    public static final String AES_ULTIMATE_CONSIGNEE_COMPANY_ADDRESS_3_LENGTH = "ERR201155";
    public static final String AES_ULTIMATE_CONSIGNEE_COMPANY_ADDRESS_4_MANDATORY = "ERR201156";
    public static final String AES_ULTIMATE_CONSIGNEE_COMPANY_ADDRESS_4_LENGTH = "ERR201157";

    public static final String AES_USSPI_STATE_BLOCK = "ERR201135";
    public static final String AES_USSPI_STATE_HIDE = "ERR201136";
    public static final String AES_USSPI_CITY_BLOCK = "ERR201137";
    public static final String AES_USSPI_CITY_HIDE = "ERR201138";
    public static final String AES_USSPI_COUNTRY_BLOCK = "ERR201139";
    public static final String AES_USSPI_COUNTRY_HIDE = "ERR201140";
    public static final String AES_USSPI_CONTACT_NO_INVALID = "ERR201141";
    public static final String AES_USSPI_ZIP_CODE_INVALID = "ERR201142";
    public static final String AES_USSPI_STATE_MANDATORY = "ERR201158";
    public static final String AES_USSPI_CITY_MANDATORY = "ERR201159";
    public static final String AES_USSPI_COUNTRY_MANDATORY = "ERR201160";
    public static final String AES_USSPI_CONTACT_NO_MANDATORY = "ERR201161";
    public static final String AES_USSPI_ZIP_CODE_MANDATORY = "ERR201162";

    public static final String AES_ULCONS_STATE_BLOCK = "ERR201163";
    public static final String AES_ULCONS_STATE_HIDE = "ERR201164";
    public static final String AES_ULCONS_CITY_BLOCK = "ERR201165";
    public static final String AES_ULCONS_CITY_HIDE = "ERR201166";
    public static final String AES_ULCONS_COUNTRY_BLOCK = "ERR201167";
    public static final String AES_ULCONS_COUNTRY_HIDE = "ERR201168";
    public static final String AES_ULCONS_CONTACT_NO_INVALID = "ERR201169";
    public static final String AES_ULCONS_ZIP_CODE_INVALID = "ERR201170";
    public static final String AES_ULCONS_STATE_MANDATORY = "ERR201171";
    public static final String AES_ULCONS_CITY_MANDATORY = "ERR201172";
    public static final String AES_ULCONS_COUNTRY_MANDATORY = "ERR201173";
    public static final String AES_ULCONS_CONTACT_NO_MANDATORY = "ERR201174";
    public static final String AES_ULCONS_ZIP_CODE_MANDATORY = "ERR201175";
    public static final String AES_ULTIMATE_CONSIGNEE_TYPE_MANDATORY = "ERR201176";

    public static final String AES_INTERCONS_CITY_BLOCK = "ERR201177";
    public static final String AES_INTERCONS_CITY_HIDE = "ERR201178";
    public static final String AES_INTERCONS_STATE_BLOCK = "ERR201179";
    public static final String AES_INTERCONS_STATE_HIDE = "ERR201180";
    public static final String AES_INTERCONS_COUNTRY_BLOCK = "ERR201181";
    public static final String AES_INTERCONS_COUNTRY_HIDE = "ERR201182";
    public static final String AES_INTERCONS_ZIP_INVALID = "ERR201183";
    public static final String AES_INTERMEDIATE_CONSIGNEE_COMPANY_BLOCK = "ERR201184";
    public static final String AES_INTERMEDIATE_CONSIGNEE_COMPANY_HIDE = "ERR201185";
    public static final String AES_INTERMEDIATE_CONSIGNEE_COMPANY_DEFAULTER = "ERR201186";

    public static final String AES_INT_CONSIGNEE_FIRST_NAME_INVALID = "ERR201187";
    public static final String AES_INT_CONSIGNEE_SECOND_NAME_INVALID = "ERR201188";

    public static final String AES_INT_CONSIGNEE_CONTACT_NO_INVALID = "ERR201189";
    public static final String AES_INT_CONSIGNEE_ZIP_CODE_INVALID = "ERR201190";
    public static final String AES_INT_CONSIGNEE_ID_NO_INVALID = "ERR201191";

    public static final String AES_SAVED_SUCCESSFULLY = "ERR2011996";


    public static final String AES_USPPI_COUNTRY = "ERR2011997";
    public static final String AES_USPPI_COUNTRY_BLOCK = "ERR2011998";
    public static final String AES_USPPI_COUNTRY_HIDE = "ERR2011999";
    public static final String AES_USPPI_SHIPPER_ID_NO_MANDATORY = "ERR2012027";

    public static final String CARGO_ORIGIN_DEFUALTER = "ERR2012000";
    public static final String CARGO_ORIGIN_BLOCKED = "ERR2012001";
    public static final String CARGO_ORIGIN_HIDDEN = "ERR2012002";

    public static final String AES_USPPI_LAST_NAME_MANDATORY = "ERR2012003";


    public static final String AES_FREIGHT_COMPANY_MANDATORY = "ERR2012004";
    public static final String AES_FREIGHT_COMPANY_BLOCK = "ERR2012005";
    public static final String AES_FREIGHT_COMPANY_HIDE = "ERR2012006";
    public static final String AES_FREIGHT_COMPANY_DEFAULTER = "ERR2012007";

    public static final String AES_CANNOT_CREATE_FOR_CANCEL_CONSOL = "ERR2012008";
    public static final String AES_CANNOT_CREATE_FOR_CANCEL_SERVICE = "ERR2012009";

    public static final String AES_NO_INVALID = "ERR2012010";

    public static final String AES_VALUE_IN_USD_REQUIRED = "ERR201201";
    public static final String AES_VALUE_IN_USD_INVALID = "ERR201202";

    public static final String AES_COMMODITY_UNIT1_REQUIRED = "ERR201203";
    public static final String AES_COMMODITY_UNIT1_BLOCKED = "ERR201204";
    public static final String AES_COMMODITY_UNIT1_HIDDEN = "ERR201205";

    public static final String AES_COMMODITY_UNIT2_REQUIRED = "ERR201206";
    public static final String AES_COMMODITY_UNIT2_BLOCKED = "ERR201207";
    public static final String AES_COMMODITY_UNIT2_HIDDEN = "ERR201208";

    public static final String AES_COMMODITY_PIECES1_REQUIRED = "ERR201209";
    public static final String AES_COMMODITY_PIECES2_REQUIRED = "ERR201210";

    public static final String AES_FORWARDER_ID_TYPE_REQUIRED = "ERR201211";
    public static final String AES_FORWARDER_ID_NO_REQUIRED = "ERR201212";

    public static final String AES_ID_SUFFIX_ADD = "ERR201213";

    public static final String AES_SUBMIT_BUTTON_MESSAGE = "ERR201214";


    //till ERR2011996 codes are used plz use after


    //aes


    //Purchase Order Error Codes - Starts WIth here [Code Range ERR03001 - ERR03100]
    public static final String PURCHASE_UK_PURCHASE_ORDER_NO = "ERR03001";
    public static final String PURCHASE_FK_PURCHASE_ORDER_BUYER = "ERR03002";
    public static final String PURCHASE_FK_PURCHASE_ORDER_SUPPLIER = "ERR03003";
    public static final String PURCHASE_FK_PURCHASE_ORDER_ORIGIN = "ERR03004";
    public static final String PURCHASE_FK_PURCHASE_ORDER_DESTINATION = "ERR03005";
    public static final String PURCHASE_FK_PURCHASE_ORDER_ORIGIN_AGENT = "ERR03006";
    public static final String PURCHASE_FK_PURCHASE_ORDER_DEST_AGENT = "ERR03007";
    public static final String PURCHASE_FK_PURCHASE_ORDER_TOS = "ERR03008";
    public static final String PO_DATE = "ERR03009";
    public static final String PO_NUMBER = "ERR03010";
    public static final String PO_TOS = "ERR03011";
    public static final String PO_TOS_BLOCK = "ERR03012";
    public static final String PO_TOS_HIDE = "ERR03013";
    public static final String PO_DESTINATION = "ERR03014";
    public static final String PO_DESTINATION_BLOCK = "ERR03015";
    public static final String PO_DESTINATION_HIDE = "ERR03016";
    public static final String PO_ORIGIN = "ERR03017";
    public static final String PO_ORIGIN_BLOCK = "ERR03018";
    public static final String PO_ORIGIN_HIDE = "ERR03019";
    public static final String PO_BUYER = "ERR03020";
    public static final String PO_BUYER_BLOCK = "ERR03021";
    public static final String PO_BUYER_HIDE = "ERR03022";
    public static final String PO_SUPPLIER = "ERR03023";
    public static final String PO_SUPPLIER_BLOCK = "ERR03024";
    public static final String PO_SUPPLIER_HIDE = "ERR03025";
    public static final String PO_ORIGIN_AGENT = "ERR03026";
    public static final String PO_ORIGIN_AGENT_BLOCK = "ERR03027";
    public static final String PO_ORIGIN_AGENT_HIDE = "ERR03028";
    public static final String PO_DESTINATION_AGENT = "ERR03029";
    public static final String PO_DESTINATION_AGENT_BLOCK = "ERR03030";
    public static final String PO_DESTINATION_AGENT_HIDE = "ERR03031";
    public static final String PO_DIRECT_OR_CONSOL_REQUIRED = "ERR03032";
    public static final String PO_TRANSPORT_MODE_REQUIRED = "ERR03033";
    public static final String PO_STATUS = "ERR03034";
    public static final String PO_UK_PO_ATTACHMENT_REF = "ERR03035";
    public static final String PO_FK_PO_ATTACHMENT_PO = "ERR03036";
    public static final String PO_FK_PO_ITEM_PO = "ERR03037";
    public static final String PO_FK_PO_REMARK_PO = "ERR03038";
    public static final String PO_FK_PO_VEHICLE_INFO_PO = "ERR03039";
    public static final String PO_FK_PO_COMPANY = "ERR03040";
    public static final String PO_FK_PO_LOCATION = "ERR03041";
    public static final String PO_SIMILAR_PORTS = "ERR03042";
    public static final String PO_ITEM_NO = "ERR03043";
    public static final String PO_ITEM_DESCRIPTION = "ERR03044";
    public static final String PO_ITEM_NO_OF_PIECES = "ERR03045";
    public static final String PO_ITEM_PACK_BLOCK = "ERR03046";
    public static final String PO_ITEM_PCSANDCNT = "ERR03047";
    public static final String PO_ITEM_PIECES = "ERR03048";
    public static final String PO_ITEM_VOLUME = "ERR03049";
    public static final String PO_ITEM_WEIGHT = "ERR03050";
    public static final String PO_ITEM_NETWEIGHT = "ERR03051";
    public static final String PO_ITEM_CURRENCY = "ERR03052";
    public static final String PO_ITEM_AMT_PER_UNTI = "ERR03053";
    public static final String PO_ITEM_TOTAL_AMNT = "ERR03054";
    public static final String PO_ITEM_PICKUP_INVALID_RANGE = "ERR03055";
    public static final String PO_ITEM_DELIVERY_INVALID_RANGE = "ERR03056";


    public static final String PO_VEHICLE_NO = "ERR03057";
    public static final String PO_VEHICLE_TYPE = "ERR03058";
    public static final String PO_VEHICLE_DRIVER_NAME = "ERR03059";
    public static final String PO_VEHICLE_INVALID_PICKUP_DATE = "ERR03060";
    public static final String PO_VEHICLE_INVALID_DELIVERY_DATE = "ERR03061";
    public static final String PO_VEHICLE_INVALID_PICKUP_DELIVERY_DATE_RANGE = "ERR03062";
    public static final String PO_VEHICLE_NO_OF_PIECES = "ERR03063";
    public static final String PO_VEHICLE_VOLUME = "ERR03064";
    public static final String PO_VEHICLE_NETWEIGHT = "ERR03065";

    public static final String PO_REMARK_TYPE = "ERR03066";
    public static final String PO_REMARK_DATE = "ERR03067";
    public static final String PO_REMARK_FOLLOW_UP_DATE = "ERR03068";
    public static final String PO_REMARK = "ERR03069";


    public static final String PO_NO_REQUIRED = "ERR03070";
    public static final String PO_NO_INVALID = "ERR03071";
    public static final String PO_VEHICLE_EMPTY_LIST = "ERR03072";
    public static final String PO_REMARK_EMPTY_LIST = "ERR03073";
    public static final String PO_ITEM_NO_INVALID = "ERR03074";
    public static final String PO_ITEM_DESCRIPTION_INVALID = "ERR03075";
    public static final String PO_TRANSPORT_MODE = "ERR03076";


    //Purchase Order Error Codes - Ends Here[Code Range ERR03001 - ERR03100]


    //Buyer COnsolidation Master Error Codes - Starts Here[Code Range ERR03201 - ERR03300]
    public static final String BUYER_CONSOLIDATION_ID_NOT_NULL = "ERR03201";
    public static final String BUYER_CONSOLIDATION_ID_NOT_FOUND = "ERR03202";
    public static final String BUYER_CONSOLIDATION_CODE_ALREADY_EXIST = "ERR03203";
    public static final String BUYER_CONSOLIDATION_NAME_ALREADY_EXIST = "ERR03204";
    public static final String BUYER_CONSOLIDATION_NOT_NULL = "ERR03205";
    public static final String BUYER_CONSOLIDATION_CODE_NOT_NULL = "ERR03206";
    public static final String BUYER_CONSOLIDATION_NAME_NOT_NULL = "ERR03207";
    public static final String BUYER_CONSOLIDATION_PP_CC_NOT_NULL = "ERR03208";
    public static final String BUYER_CONSOLIDATION_STATUS_NOT_NULL = "ERR03209";
    public static final String BUYER_CONSOLIDATION_DESCRIPTION_NOT_NULL = "ERR03210";
    public static final String BUYER_CONSOLIDATION_CODE_INVALID = "ERR03211";
    public static final String BUYER_CONSOLIDATION_NAME_INVALID = "ERR03212";
    public static final String BUYER_CONSOLIDATION_DESCRIPTION_INVALID = "ERR03213";
    public static final String BUYER_CONSOLIDATION_LIST_EMPTY = "ERR03214";

    //Buyer COnsolidation Master Error Codes - Starts Here[Code Range ERR03201 - ERR03225]

    //Pricing Master Error code-
    public static final String PRICING_MASTER_ORIGIN_REQUIRED = "ERR4000";
    public static final String PRICING_MASTER_ORIGIN_BLOCKED = "ERR4001";
    public static final String PRICING_MASTER_ORIGIN_HIDDEN = "ERR4002";

    public static final String PRICING_MASTER_DESTINATION_REQUIRED = "ERR4003";
    public static final String PRICING_MASTER_DESTINATION_BLOCKED = "ERR4004";
    public static final String PRICING_MASTER_DESTINATION_HIDDEN = "ERR4005";

    public static final String PRICING_MASTER_TRANSIT_REQUIRED = "ERR4006";
    public static final String PRICING_MASTER_TRANSIT_BLOCKED = "ERR4007";
    public static final String PRICING_MASTER_TRANSIT_HIDDEN = "ERR4008";

    public static final String PRICING_MASTER_ORIGIN_AND_DESTINATION_SAME = "ERR4009";
    public static final String PRICING_MASTER_ORIGIN_AND_TRANSIT_SAME = "ERR4010";
    public static final String PRICING_MASTER_DESTINATION_AND_TRANSIT_SAME = "ERR4011";

    public static final String PRICING_MASTER_CHARGE_REQUIRED = "ERR4012";
    public static final String PRICING_MASTER_CHARGE_BLOCKED = "ERR4013";
    public static final String PRICING_MASTER_CHARGE_HIDDEN = "ERR4014";

    public static final String PRICING_MASTER_UNIT_REQUIRED = "ERR4015";
    public static final String PRICING_MASTER_UNIT_BLOCKED = "ERR4016";
    public static final String PRICING_MASTER_UNIT_HIDDEN = "ERR4017";

    public static final String PRICING_MASTER_CURRENCY_REQUIRED = "ERR4018";
    public static final String PRICING_MASTER_CURRENCY_BLOCKED = "ERR4019";
    public static final String PRICING_MASTER_CURRENCY_HIDDEN = "ERR4020";

    public static final String PRICING_MASTER_CARRIER_REQUIRED = "ERR4021";
    public static final String PRICING_MASTER_CARRIER_BLOCKED = "ERR4022";
    public static final String PRICING_MASTER_CARRIER_HIDDEN = "ERR4023";

    public static final String PRICING_MASTER_FROMDATE_REQUIRED = "ERR4024";
    public static final String PRICING_MASTER_TODATE_REQUIRED = "ERR4025";
    public static final String PRICING_MASTER_MINSELAMOUNT_REQUIRED = "ERR4026";
    public static final String PRICING_MASTER_STDSELLAMOUNT_REQUIRED = "ERR4027";
    public static final String PRICING_MASTER_COSTAMOUNT_REQUIRED = "ERR4028";

    public static final String PRICING_MASTER_MINUS45_REQUIRED = "ERR4029";
    public static final String PRICING_MASTER_PLUS45_REQUIRED = "ERR4030";
    public static final String PRICING_MASTER_PLUS100_REQUIRED = "ERR4031";
    public static final String PRICING_MASTER_PLUS250_REQUIRED = "ERR4032";
    public static final String PRICING_MASTER_PLUS500_REQUIRED = "ERR4033";
    public static final String PRICING_MASTER_PLUS1000_REQUIRED = "ERR4034";


    public static final String PRICING_MASTER_FUELSURCHARGE_REQUIRED = "ERR4035";
    public static final String PRICING_MASTER_SECURITY_SURCHARGE_REQUIRED = "ERR4036";


    public static final String PRICING_MASTER_MINUS45_INVALID = "ERR4037";
    public static final String PRICING_MASTER_PLUS45_INVALID = "ERR4038";
    public static final String PRICING_MASTER_PLUS100_INVALID = "ERR4039";
    public static final String PRICING_MASTER_PLUS250_INVALID = "ERR4040";
    public static final String PRICING_MASTER_PLUS300_INVALID = "ERR4149";
    public static final String PRICING_MASTER_PLUS500_INVALID = "ERR4041";
    public static final String PRICING_MASTER_PLUS1000_INVALID = "ERR4042";

    public static final String PRICING_MASTER_MINUS45_NOT_ZERO = "ERR4043";
    public static final String PRICING_MASTER_PLUS45_NOT_ZERO = "ERR4044";
    public static final String PRICING_MASTER_PLUS100_NOT_ZERO = "ERR4045";
    public static final String PRICING_MASTER_PLUS250_NOT_ZERO = "ERR4046";
    public static final String PRICING_MASTER_PLUS300_NOT_ZERO = "ERR4150";
    public static final String PRICING_MASTER_PLUS500_NOT_ZERO = "ERR4047";
    public static final String PRICING_MASTER_PLUS1000_NOT_ZERO = "ERR4048";


    public static final String PRICING_MASTER_MINSELIN_MINIMUM_NOT_ZERO = "ERR4049";
    public static final String PRICING_MASTER_MINSELAMOUNT_NOT_ZERO = "ERR4050";
    public static final String PRICING_MASTER_STDSELIN_MINIMUM_NOT_ZERO = "ERR4051";
    public static final String PRICING_MASTER_STDSEL_AMOUNT_NOT_ZERO = "ERR4052";
    public static final String PRICING_MASTER_COST_MINIMUM_NOT_ZERO = "ERR4053";
    public static final String PRICING_MASTER_COST_AMOUNT_NOT_ZERO = "ERR4054";

    public static final String PRICING_MASTER_MINSELIN_MINIMUM_INVALID = "ERR4055";
    public static final String PRICING_MASTER_MINSELAMOUNT_INVALID = "ERR4056";

    public static final String PRICING_MASTER_MINSEL_MINUS45_INVALID = "ERR4122";
    public static final String PRICING_MASTER_MINSEL_PLUS45_INVALID = "ERR4123";
    public static final String PRICING_MASTER_MINSEL_PLUS100_INVALID = "ERR4124";
    public static final String PRICING_MASTER_MINSEL_PLUS250_INVALID = "ERR4147";
    public static final String PRICING_MASTER_MINSEL_PLUS300_INVALID = "ERR4125";
    public static final String PRICING_MASTER_MINSEL_PLUS500_INVALID = "ERR4126";
    public static final String PRICING_MASTER_MINSEL_PLUS1000_INVALID = "ERR4127";


    public static final String PRICING_MASTER_STDSELIN_MINIMUM_INVALID = "ERR4057";
    public static final String PRICING_MASTER_STDSEL_AMOUNT_INVALID = "ERR4058";

    public static final String PRICING_MASTER_STDSEL_MINUS45_INVALID = "ERR4110";
    public static final String PRICING_MASTER_STDSEL_PLUS45_INVALID = "ERR4111";
    public static final String PRICING_MASTER_STDSEL_PLUS100_INVALID = "ERR4112";
    public static final String PRICING_MASTER_STDSEL_PLUS250_INVALID = "ERR4146";
    public static final String PRICING_MASTER_STDSEL_PLUS300_INVALID = "ERR4113";
    public static final String PRICING_MASTER_STDSEL_PLUS500_INVALID = "ERR4114";
    public static final String PRICING_MASTER_STDSEL_PLUS1000_INVALID = "ERR4115";


    public static final String PRICING_MASTER_COST_MINIMUM_INVALID = "ERR4059";
    public static final String PRICING_MASTER_COST_AMOUNT_INVALID = "ERR4060";

    public static final String PRICING_MASTER_COST_MINUS45_INVALID = "ERR4134";
    public static final String PRICING_MASTER_COST_PLUS45_INVALID = "ERR4135";
    public static final String PRICING_MASTER_COST_PLUS100_INVALID = "ERR4136";
    public static final String PRICING_MASTER_COST_PLUS250_INVALID = "ERR4148";
    public static final String PRICING_MASTER_COST_PLUS300_INVALID = "ERR4137";
    public static final String PRICING_MASTER_COST_PLUS500_INVALID = "ERR4138";
    public static final String PRICING_MASTER_COST_PLUS1000_INVALID = "ERR4139";


    public static final String PRICING_MASTER_MINIMUM_NOT_ZERO = "ERR4098";
    public static final String PRICING_MASTER_MINIMUM_INVALID = "ERR4099";

    public static final String UK_PRICING_ALREADY_EXIST = "ERR4100";

    public static final String PRICING_MASTER_FUEL_VALIDFROM_REQUIRED = "ERR4101";
    public static final String PRICING_MASTER_FUEL_VALIDTO_REQUIRED = "ERR4102";
    public static final String PRICING_MASTER_SECURITY_VALIDFROM_REQUIRED = "ERR4103";
    public static final String PRICING_MASTER_SECURITY_VALIDTO_REQUIRED = "ERR4104";

    public static final String PRICING_MASTER_STDSEL_MINIMUM_REQUIRED = "ERR4105";
    public static final String PRICING_MASTER_CSV_FILE_REQUIRED = "ERR4106";
    public static final String PRICING_MASTER_CSV_FILE_EMPTY = "ERR4107";
    public static final String PRICING_MASTER_CSV_FILE_LENGTH_EXCEED = "ERR4108";

    public static final String PRICING_MASTER_FUEL_SURCHARGE_INVALID = "ERR4062";
    public static final String PRICING_MASTER_SECURITY_SURCHARGE_INVALID = "ERR4063";

    public static final String PRICING_MASTER_FUEL_SURCHARGE_NOT_ZERO = "ERR4064";
    public static final String PRICING_MASTER_SECURITY_SURCHARGE_NOT_ZERO = "ERR4065";


    //Starts Iata rate master
    public static final String VALID_FROM_LESSTHAN_VALID_TO = "ERR05000";
    public static final String VALID_FROM_EMPTY = "ERR05001";
    public static final String VALID_TO_EMPTY = "ERR05002";
    public static final String POL_IS_EMPTY = "ERR05003";
    public static final String POL_IS_BLOCKED = "ERR05004";
    public static final String POL_IS_HIDDEN = "ERR05005";
    public static final String CARRIER_IS_EMPTY = "ERR05006";
    public static final String CARRIER_IS_BLOCKED = "ERR05007";
    public static final String CARRIER_IS_HIDDEN = "ERR05008";
    public static final String ATTACHMENT_IS_EMPTY = "ERR05009";
    public static final String CHARGE_IS_EMPTY = "ERR05010";
    public static final String POL_SATUS_IS_EMPTY = "ERR05011";
    public static final String CARRIER_STATUS_IS_EMPTY = "ERR05012";
    public static final String POD_IS_EMPTY = "ERR05013";
    public static final String POD_IS_BLOCKED = "ERR05014";
    public static final String POD_IS_HIDDEN = "ERR05015";
    public static final String MIN_AMOUNT_INVALID = "ERR05016";
    public static final String NORMAL_INVALID = "ERR05017";
    public static final String PLUS45_INVALID = "ERR05018";
    public static final String PLUS45_NOT_ZERO = "ERR05019";
    public static final String PLUS100_INVALID = "ERR05020";
    public static final String PLUS100_NOT_ZERO = "ERR05021";
    public static final String PLUS300_INVALID = "ERR05022";
    public static final String PLUS300_NOT_ZERO = "ERR05023";
    public static final String PLUS500_INVALID = "ERR05024";
    public static final String PLUS500_NOT_ZERO = "ERR05025";
    public static final String PLUS1000_INVALID = "ERR05026";
    public static final String PLUS1000_NOT_ZERO = "ERR05027";
    public static final String MINAMOUNT_NOT_ZERO = "ERR05028";
    public static final String NORMAL_NOT_ZERO = "ERR05029";
    public static final String POD_STATUS_EMPTY = "ERR05030";
    public static final String IATA_RATES_ALREADY_EXISTS = "ERR05036";
    //Ends Iata rate master

    // Essential charge master

    public static final String ESSENTIAL_SERVICE = "ERR05100";
    public static final String ESSENTIAL_SERVICE_BLOCK = "ERR05101";
    public static final String ESSENTIAL_SERVICE_HIDE = "ERR05102";

    public static final String ESSENTIAL_CHARGE = "ERR05103";
    public static final String ESSENTIAL_CHARGE_BLOCK = "ERR05104";
    public static final String ESSENTIAL_CHARGE_HIDE = "ERR05105";

    public static final String ESSENTIAL_CRN_REQUIRED = "ERR05106";
    public static final String ESSENTIAL_CRN_INVALIID = "ERR05107";

    public static final String ESSENTIAL_ID_NOT_NULL = "ERR05108";

    public static final String ESSENTIAL_TOS_BLOCK = "ERR05109";
    public static final String ESSENTIAL_TOS_HIDE = "ERR05110";

    public static final String ESSENTIAL_ORIGIN_BLOCK = "ERR05111";
    public static final String ESSENTIAL_ORIGIN_HIDE = "ERR05112";

    public static final String ESSENTIAL_DESTINATION_BLOCK = "ERR05113";
    public static final String ESSENTIAL_DESTINATION_HIDE = "ERR05114";

    public static final String ESSENTIAL_CHARGE_MISMATCHED = "ERR05115";
    public static final String CHARGE_IS_MANDATORY_FOR_SERVICE = "ERR05116";
    public static final String ESSENTIAL_CHARGE_NOT_EXIST = "ERR05117";

    public static final String ESSENTIAL_ORGIN_DESTINATION_SAME = "ERR05118";
    public static final String ESSENTIAL_ALREADY_EXIST = "ERR05119";

    //Authenticated docs
    public static final String AUTH_DOCS_DOCUMNENT_NO_MANDATORY = "ERR05500";
    public static final String AUTH_DOCS_GROSSWEIGHT_INVALID = "ERR05501";
    public static final String AUTH_DOCS_VOLUMEWEIGHT_INVALID = "ERR05502";
    public static final String AUTH_DOCS_DATE_MANDATORY = "ERR05503";
    public static final String AUTH_DOCS_ORIGIN = "ERR05504";
    public static final String AUTH_DOCS_ORIGIN_BLOCK = "ERR05505";
    public static final String AUTH_DOCS_ORIGIN_HIDE = "ERR05506";
    public static final String AUTH_DOCS_VOLUME_WEIGHT_NOT_ZERO = "ERR05507";
    public static final String AUTH_DOCS_GROSS_WEIGHT_NOT_ZERO = "ERR05508";
    public static final String AUTH_DOCS_DESTINATION = "ERR05509";
    public static final String AUTH_DOCS_DESTINATION_BLOCK = "ERR05510";
    public static final String AUTH_DOCS_DESTINATION_HIDE = "ERR05511";
    public static final String AUTH_DOCS_NOOFPIECES_NOT_ZERO = "ERR05512";
    public static final String AUTH_DOCS_NOOFPIECES_INVALID = "ERR05513";
    public static final String AUTH_DOCS_MARKS_AND_NO_LENGTH_INVALID = "ERR05514";
    public static final String AUTH_DOCS_COMMODITY_INVALID_LENGTH_INVALID = "ERR05515";

    public static final String AUTH_DOCS_SHIPPER = "ERR05516";
    public static final String AUTH_DOCS_SHIPPER_BLOCK = "ERR05517";
    public static final String AUTH_DOCS_SHIPPER_HIDE = "ERR05518";
    public static final String AUTH_DOCS_SHIPPER_ADDRESS = "ERR05519";
    public static final String AUTH_DOCS_SHIPPER_ADDRESS_1 = "ERR05520";
    public static final String AUTH_DOCS_SHIPPER_ADDRESS_1_LENGTH = "ERR05521";
    public static final String AUTH_DOCS_SHIPPER_ADDRESS_2 = "ERR05522";
    public static final String AUTH_DOCS_SHIPPER_ADDRESS_2_LENGTH = "ERR05523";
    public static final String AUTH_DOCS_SHIPPER_ADDRESS_3_LENGTH = "ERR05524";
    public static final String AUTH_DOCS_SHIPPER_ADDRESS_4 = "ERR05525";
    public static final String AUTH_DOCS_SHIPPER_ADDRESS_4_LENGTH = "ERR05526";
    public static final String AUTH_DOCS_SHIPPER_EMAIL = "ERR05527";
    public static final String AUTH_DOCS_SHIPPER_EMAIL_LENGTH = "ERR05528";

    public static final String AUTH_DOCS_FORWARDER = "ERR05529";
    public static final String AUTH_DOCS_FORWARDER_BLOCK = "ERR05530";
    public static final String AUTH_DOCS_FORWARDER_HIDE = "ERR05531";
    public static final String AUTH_DOCS_FORWARDER_ADDRESS = "ERR05532";
    public static final String AUTH_DOCS_FORWARDER_ADDRESS_1 = "ERR05533";
    public static final String AUTH_DOCS_FORWARDER_ADDRESS_1_LENGTH = "ERR05534";
    public static final String AUTH_DOCS_FORWARDER_ADDRESS_2 = "ERR05535";
    public static final String AUTH_DOCS_FORWARDER_ADDRESS_2_LENGTH = "ERR05536";
    public static final String AUTH_DOCS_FORWARDER_ADDRESS_3_LENGTH = "ERR05537";
    public static final String AUTH_DOCS_FORWARDER_ADDRESS_4 = "ERR05538";
    public static final String AUTH_DOCS_FORWARDER_ADDRESS_4_LENGTH = "ERR05539";
    public static final String AUTH_DOCS_FORWARDER_EMAIL = "ERR05540";
    public static final String AUTH_DOCS_FORWARDER_EMAIL_LENGTH = "ERR05541";

    public static final String AUTH_DOCS_CONSIGNEE = "ERR05542";
    public static final String AUTH_DOCS_CONSIGNEE_BLOCK = "ERR05543";
    public static final String AUTH_DOCS_CONSIGNEE_HIDE = "ERR05544";
    public static final String AUTH_DOCS_CONSIGNEE_ADDRESS = "ERR05545";
    public static final String AUTH_DOCS_CONSIGNEE_ADDRESS_1 = "ERR05546";
    public static final String AUTH_DOCS_CONSIGNEE_ADDRESS_1_LENGTH = "ERR05547";
    public static final String AUTH_DOCS_CONSIGNEE_ADDRESS_2 = "ERR05548";
    public static final String AUTH_DOCS_CONSIGNEE_ADDRESS_2_LENGTH = "ERR05549";
    public static final String AUTH_DOCS_CONSIGNEE_ADDRESS_3_LENGTH = "ERR05550";
    public static final String AUTH_DOCS_CONSIGNEE_ADDRESS_4 = "ERR05551";
    public static final String AUTH_DOCS_CONSIGNEE_ADDRESS_4_LENGTH = "ERR05552";
    public static final String AUTH_DOCS_CONSIGNEE_EMAIL = "ERR05553";
    public static final String AUTH_DOCS_CONSIGNEE_EMAIL_LENGTH = "ERR05554";

    public static final String AUTH_DOCS_FIRST_NOTIFY = "ERR05555";
    public static final String AUTH_DOCS_FIRST_NOTIFY_BLOCK = "ERR05556";
    public static final String AUTH_DOCS_FIRST_NOTIFY_HIDE = "ERR05557";
    public static final String AUTH_DOCS_FIRST_NOTIFY_ADDRESS = "ERR05558";
    public static final String AUTH_DOCS_FIRST_NOTIFY_ADDRESS_1 = "ERR05559";
    public static final String AUTH_DOCS_FIRST_NOTIFY_ADDRESS_1_LENGTH = "ERR05560";
    public static final String AUTH_DOCS_FIRST_NOTIFY_ADDRESS_2 = "ERR05561";
    public static final String AUTH_DOCS_FIRST_NOTIFY_ADDRESS_2_LENGTH = "ERR05562";
    public static final String AUTH_DOCS_FIRST_NOTIFY_ADDRESS_3_LENGTH = "ERR05563";
    public static final String AUTH_DOCS_FIRST_NOTIFY_ADDRESS_4 = "ERR05564";
    public static final String AUTH_DOCS_FIRST_NOTIFY_ADDRESS_4_LENGTH = "ERR05565";
    public static final String AUTH_DOCS_FIRST_NOTIFY_EMAIL = "ERR05566";
    public static final String AUTH_DOCS_FIRST_NOTIFY_EMAIL_LENGTH = "ERR05567";

    public static final String AUTH_DOCS_GROSS_WEIGHT_REQUIRED = "ERR05568";
    public static final String AUTH_DOCS_VOLUME_WEIGHT_REQUIRED = "ERR05569";
    public static final String AUTH_DOCS_NO_OF_PIECES_REQUIRED = "ERR05570";


    public static final String DATE_CONFIG_UNIQUE = "ERR05201";
    public static final String DATE_CONFIG_SERVICE = "ERR05202";
    public static final String DATE_CONFIG_BLOCK = "ERR05203";
    public static final String DATE_CONFIG_HIDE = "ERR05204";
    public static final String DATE_CONFIG_WHICH_OPERATION = "ERR05205";
    public static final String DATE_CONFIG_LOGIC_DATE = "ERR05206";
    public static final String ENQUIRY_SERVICE_MAPPING_ALREADY_EXIST = "ERR05219";
    public static final String TRANSPORT_ALREADY_EXIST = "ERR05220";

    public static final String SERVICE_MAPPING_ALREADY_EXIST = "ERR05221";

    public static final String SERVICE_IMPORT_NOT_NULL = "ERR05225";
    public static final String SERVICE_EXPORT_NOT_NULL = "ERR05226";
    public static final String SERVICE_IMPORT_BLOCK = "ERR05227";
    public static final String SERVICE_IMPORT_HIDE = "ERR05228";
    public static final String SERVICE_EXPORT_BLOCK = "ERR05229";
    public static final String SERVICE_EXPORT_HIDE = "ERR05230";


    public static final String EDI_KEY_MANDATORY = "ERR05231";
    public static final String EDI_VALUE_MANDATORY = "ERR05232";


    // Default Charge Code
    public static final String DEFAULT_CHARGE_DATA_MANDATORY = "ERR2000200";
    public static final String DEFAULT_CHARGE_SERVICE_MANDATORY = "ERR2000201";
    public static final String DEFAULT_CHARGE_SERVICE_BLOCKED = "ERR2000202";
    public static final String DEFAULT_CHARGE_SERVICE_HIDDEN = "ERR2000203";
    public static final String DEFAULT_CHARGE_CHARGE_MANDATORY = "ERR2000204";
    public static final String DEFAULT_CHARGE_CHARGE_BLOCKED = "ERR2000205";
    public static final String DEFAULT_CHARGE_CHARGE_HIDDEN = "ERR2000206";
    public static final String DEFAULT_CHARGE_CURRENCY_MANDATORY = "ERR2000207";
    public static final String DEFAULT_CHARGE_CURRENCY_BLOCKED = "ERR2000208";
    public static final String DEFAULT_CHARGE_CURRENCY_HIDDEN = "ERR2000209";
    public static final String DEFAULT_CHARGE_UNIT_MANDATORY = "ERR2000210";
    public static final String DEFAULT_CHARGE_UNIT_BLOCKED = "ERR2000211";
    public static final String DEFAULT_CHARGE_UNIT_HIDDEN = "ERR2000212";
    public static final String DEFAULT_CHARGE_AMOUNTUNIT_MANDATORY = "ERR2000213";
    public static final String DEFAULT_CHARGE_AMOUNTUNIT_INVALID = "ERR2000214";
    public static final String DEFAULT_CHARGE_MINAMOUNT_INVALID = "ERR2000215";
    public static final String DEFAULT_CHARGE_DIVISION_BLOCKED = "ERR2000216";
    public static final String DEFAULT_CHARGE_DIVISION_HIDDEN = "ERR2000217";

    public static final String DEFAULT_CHARGE_ORIGIN_BLOCKED = "ERR2000218";
    public static final String DEFAULT_CHARGE_ORIGIN_HIDDEN = "ERR2000219";

    public static final String DEFAULT_CHARGE_DESTINATION_BLOCKED = "ERR2000220";
    public static final String DEFAULT_CHARGE_DESTINATION_HIDDEN = "ERR2000221";


    public static final String DEFAULT_CHARGE_TRANSITPORT_BLOCKED = "ERR2000222";
    public static final String DEFAULT_CHARGE_TRANSITPORT_HIDDEN = "ERR2000223";

    public static final String DEFAULT_CHARGE_SHIPPER_DEFAULTER = "ERR2000224";

    public static final String DEFAULT_CHARGE_SHIPPER_BLOCKED = "ERR2000225";

    public static final String DEFAULT_CHARGE_SHIPPER_HIDDEN = "ERR2000226";

    public static final String DEFAULT_CHARGE_CONSIGNEE_DEFAULTER = "ERR2000227";

    public static final String DEFAULT_CHARGE_CONSIGNEE_BLOCKED = "ERR2000228";

    public static final String DEFAULT_CHARGE_CONSIGNEE_HIDDEN = "ERR2000229";

    public static final String DEFAULT_CHARGE_AGENT_DEFAULTER = "ERR2000230";

    public static final String DEFAULT_CHARGE_AGENT_BLOCKED = "ERR2000231";

    public static final String DEFAULT_CHARGE_AGENT_HIDDEN = "ERR2000232";

    public static final String DEFAULT_CHARGE_COMMODITY_BLOCKED = "ERR2000233";
    public static final String DEFAULT_CHARGE_COMMODITY_HIDDEN = "ERR2000234";

    public static final String DEFAULT_CHARGE_CARRIER_BLOCKED = "ERR2000235";
    public static final String DEFAULT_CHARGE_CARRIER_HIDDEN = "ERR2000236";

    public static final String DEFAULT_CHARGE_LIST_IS_INVALID = "ERR2000237";
    public static final String SELECT_SERVICE_FIRST = "ERR2000238";

    public static final String DEFAULT_CHARGE_TOS_BLOCKED = "ERR2000239";
    public static final String DEFAULT_CHARGE_TOS_HIDDEN = "ERR2000240";

    public static final String DEFAULT_CHARGE_DATE_INVALID = "ERR2000241";


    public static final String DEFAULT_CHARGE_ATLEAST_ONE_ROW = "ERR2000242";
    public static final String PLEASE_ENTER_VALID_DATA = "ERR2000243";
    public static final String COST_AMOUNT_REQUIRED = "ERR2000244";
    public static final String COST_AMOUNT_INVALID = "ERR2000245";
    public static final String COST_MIN_AMOUNT_INVALID = "ERR2000246";
    public static final String ORIGIN_DESTINATION_REQUIRED = "ERR2000249";
    public static final String DEFAUT_CHARGE_DUPLICATE = "ERR2000250";


    public static final String CSV_PORT_BLOCKED = "ERR05301";
    public static final String CSV_PORT_HIDDEN = "ERR05302";
    public static final String CSV_PORT_IS_NOT_FOUND = "ERR05303";

    public static final String CSV_CHARGE_IS_NOT_FOUND = "ERR05304";
    public static final String CSV_CHARGE_BLOCKED = "ERR05305";
    public static final String CSV_CHARGE_HIDDEN = "ERR05306";

    public static final String CSV_UNIT_IS_NOT_FOUND = "ERR05307";
    public static final String CSV_UNIT_BLOCKED = "ERR05308";
    public static final String CSV_UNIT_HIDDEN = "ERR05309";

    public static final String CSV_CURRENCY_IS_NOT_FOUND = "ERR05310";
    public static final String CSV_CURRENCY_BLOCKED = "ERR05311";
    public static final String CSV_CURRENCY_HIDDEN = "ERR05312";

    public static final String CSV_CARRIER_IS_NOT_FOUND = "ERR05313";
    public static final String CSV_CARRIER_BLOCKED = "ERR05314";
    public static final String CSV_CARRIER_HIDDEN = "ERR05315";

    public static final String CSV_DOUBLE_VAL_CONVERSION = "ERR05316";
    public static final String CSV_DATE_CONVERSION = "ERR05317";

    public static final String CSV_DATA_WIHT_IN_DATE_RANGE = "ERR05318";
    public static final String CSV_DATA_SUCCESSFULLY_UPLOADED = "ERR05319";
    public static final String CSV_DATA_INVAILD_PRICING = "ERR05320";

    public static final String DEFAULTMASTER_CODE_NOT_NULL = "ERR06000";
    public static final String DEFAULTMASTER_CODE_INVALID = "ERR06001";
    public static final String DEFAULTMASTER_ID_NOT_FOUND = "ERR06002";
    public static final String DEFAULTMASTER_CODE_ALREADY_EXIST = "ERR06003";
    public static final String DEFAULTMASTER_NAME_NOT_NULL = "ERR06004";
    public static final String DEFAULTMASTER_VALUE_NOT_NULL = "ERR06006";
    public static final String DEFAULTMASTER_LOCATION_NOT_NULL = "ERR06010";
    public static final String DEFAULTMASTER_LOCATION_BLOCK = "ERR06011";
    public static final String DEFAULTMASTER_LOCATION_HIDE = "ERR06012";

    public static final String SOFTWARE_CODE_NOT_NULL = "ERR06800";
    public static final String SOFTWARE_NAME_NOT_NULL = "ERR06801";
    public static final String SOFTWARE_CODE_ALREADY_EXIST = "ERR06804";
    public static final String SOFTWARE_NAME_ALREADY_EXIST = "ERR06805";
    public static final String SOFTWARE_ID_NOT_FOUND = "ERR06806";
    public static final String SOFTWARE_STATUS_BLOCKED = "ERR06808";
    public static final String SOFTWARE_STATUS_HIDDEN = "ERR06809";

    public static final String OBJECTGROUP_CODE_NOT_NULL = "ERR06900";
    public static final String OBJECTGROUP_NAME_NOT_NULL = "ERR06901";
    public static final String OBJECTGROUP_CODE_ALREADY_EXIST = "ERR06904";
    public static final String OBJECTGROUP_NAME_ALREADY_EXIST = "ERR06905";
    public static final String OBJECTGROUP_ID_NOT_FOUND = "ERR06906";
    public static final String OBJECTGROUP_STATUS_BLOCK = "ERR06117";
    public static final String OBJECTGROUP_STATUS_HIDE = "ERR06117";

    public static final String OBJECTSUBGROUP_CODE_NOT_NULL = "ERR06110";
    public static final String OBJECTSUBGROUP_NAME_NOT_NULL = "ERR06112";
    public static final String OBJECTSUBGROUP_CODE_ALREADY_EXIST = "ERR06115";
    public static final String OBJECTSUBGROUP_NAME_ALREADY_EXIST = "ERR06116";
    public static final String OBJECTSUBGROUP_ID_NOT_FOUND = "ERR06117";
    public static final String OBJECTSUBGROUP_STATUS_BLOCKED = "ERR06120";
    public static final String OBJECTSUBGROUP_STATUS_HIDDEN = "ERR06121";

    public static final String VAS_CODE_NOT_NULL = "ERR08000";
    public static final String VAS_NAME_NOT_NULL = "ERR08001";
    public static final String VAS_CODE_ALREADY_EXIST = "ERR08002";
    public static final String VAS_NAME_ALREADY_EXIST = "ERR08003";
    public static final String CHARGE_STATUS_BLOCKED = "ERR08004";
    public static final String CHARGE_STATUS_HIDDEN = "ERR08005";
    public static final String VAS_CHARGE_ID_ALREADY_EXIST = "ERR08010";
    public static final String CHARGE_NOT_NULL = "ERR08011";

    public static final String GRADE_ID_NOT_FOUND = "ERR07005";
    public static final String GRADE_ALREADY_EXIST = "ERR07004";
    public static final String GRADE_PRIORITY_ALREADY_EXIST = "ERR07007";

    public static final String SERVICE_TAX_PERCENTAGE_LIST_EMPTY = "ERR06100";
    public static final String SERVICE_TAX_PERCENTAGE_ID_NOT_NULL = "ERR06101";
    public static final String SERVICE_TAX_PERCENTAGE_ID_NOT_FOUND = "ERR06102";


    //Agent Port Code
    public static final String PORT_AGENT_ALREADY_EXIST = "ERR05208";
    public static final String AGENT_ID_NOT_NULL = "ERR05216";
    public static final String PARTY_ID_NOT_NULL = "ERR05217";
    public static final String AGENT_STATUS_BLOCK = "ERR05210";
    public static final String AGENT_STATUS_HIDE = "ERR05211";
    public static final String PORT_STATUS_BLOCK = "ERR05213";
    public static final String PORT_STATUS_HIDE = "ERR05214";


    public static final String SERVICE_TAX_CHARGE_LIST_EMPTY = "ERR06200";
    public static final String SERVICE_TAX_CHARGE_ID_NOT_NULL = "ERR06201";
    public static final String SERVICE_TAX_CHARGE_ID_NOT_FOUND = "ERR06202";


    //Bank Error code
    public static final String BANK_CODE_NOT_NULL = "ERR06250";
    public static final String BANK_NAME_NOT_NULL = "ERR06251";
    public static final String BANK_STATUS_NOT_NULL = "ERR06252";
    public static final String BANK_CODE_INVALID = "ERR06253";
    public static final String BANK_NAME_INVALID = "ERR06254";
    public static final String BANK_ID_NOT_FOUND = "ERR06255";
    public static final String BANK_CODE_ALREADY_EXIST = "ERR06256";
    public static final String BANK_NAME_ALREADY_EXIST = "ERR06257";
    public static final String BANK_DELETE_MESSAGE = "ERR06258";
    public static final String BANK_SMART_BANK_CODE_INVALID = "ERR06259";

    //Region Error code
    public static final String REGION_CODE_NOT_NULL = "ERR06260";
    public static final String REGION_NAME_NOT_NULL = "ERR06261";
    public static final String REGION_STATUS_NOT_NULL = "ERR06262";
    public static final String REGION_CODE_INVALID = "ERR06263";
    public static final String REGION_NAME_INVALID = "ERR06264";
    public static final String REGION_ID_NOT_FOUND = "ERR06265";
    public static final String REGION_CODE_ALREADY_EXIST = "ERR06266";
    public static final String REGION_NAME_ALREADY_EXIST = "ERR06267";

    /*Aes Filer master*/
    public static final String AESFILER_LIST_EMPTY = "ERR2012050";
    public static final String AESFILER_NOT_FOUND = "ERR2012051";
    public static final String AESFILER_CODE_NOT_NULL = "ERR2012052";
    public static final String AESFILER_CODE_INVALID = "ERR2012053";
    public static final String AESFILER_TRANSMITTER_CODE_NOT_NULL = "ERR2012054";
    public static final String AESFILER_TRANSMITTER_CODE_INVALID = "ERR2012055";
    public static final String AESFILER_LOV_STATUS_INVALID = "ERR2012056";

    public static final String AESFILER_COMPANY_NOT_NULL = "ERR2012057";
    public static final String AESFILER_COMPANY_BLOCKED = "ERR2012058";
    public static final String AESFILER_COMPANY_HIDDEN = "ERR2012059";
    public static final String AESFILER_COUNTRY_NOT_NULL = "ERR2012060";
    public static final String AESFILER_COUNTRY_BLOCKED = "ERR2012061";
    public static final String AESFILER_COUNTRY_HIDDEN = "ERR2012062";
    public static final String AESFILER_LOCATION_NOT_NULL = "ERR2012063";
    public static final String AESFILER_LOCATION_BLOCKED = "ERR2012064";
    public static final String AESFILER_LOCATION_HIDDEN = "ERR2012065";
    public static final String AESFILER_DELETE_MESSAGE = "ERR2012066";
    public static final String AESFILER_CODE_ALREADY_EXIST = "ERR2012067";
    public static final String AESFILER_LOV_STATUS_NOT_NULL = "ERR2012068";
    public static final String AESFILER_SCHEMA_NAME_NOT_NULL = "ERR2012069";


    public static final String AESFILE_ID_INVALID = "ERR2012031";
    public static final String AESFILE_TRANSMITTER_ID_MANDATORY = "ERR2012032";
    public static final String AESFILE_TRANSMITTER_ID_INVALID = "ERR2012033";
    public static final String AESFILE_TRANSMITTER_ID_INVALID2 = "ERR2012034";
    public static final String AESFILE_DETAILS_MANDATORY = "ERR2012035";

    public static final String AES_GENERATION_FAILED = "ERR2012036";
    //ServiceTaxChargeGroup Error code

    public static final String STCGROUP_ID_NOT_FOUND = "ERR06270";

    public static final String STCGROUP_ALREADY_EXIST = "ERR06271";


    public static final String SERVICETAXCATEGORY_CODE_INVALID = "ERR06370";
    public static final String SERVICETAXCATEGORY_NAME_INVALID = "ERR06371";

    public static final String SERVICETAXCATEGORY_CODE_NOT_NULL = "ERR06372";
    public static final String SERVICETAXCATEGORY_NAME_NOT_NULL = "ERR06373";
    public static final String SERVICETAXCATEGORY_LOV_STATUS_NOT_NULL = "ERR06374";
    public static final String SERVICETAXCATEGORY_CODE_ALREADY_EXIST = "ERR06375";
    public static final String SERVICETAXCATEGORY_NAME_ALREADY_EXIST = "ERR06376";
    public static final String SERVICETAXCATEGORY_ID_NOT_NULL = "ERR06377";
    public static final String SERVICETAXCATEGORY_ID_NOT_FOUND = "ERR06378";


    // ServiceTaxPercentage Error code

    public static final String SERVICETAXPERCENTAGE_CODE_NOT_NULL = "ERR06380";
    public static final String SERVICETAXPERCENTAGE_NAME_NOT_NULL = "ERR06381";
    public static final String SERVICETAXPERCENTAGE_VALIDFROM_NOT_NULL = "ERR06382";
    public static final String SERVICETAXPERCENTAGE_PERCENTAGE_NOT_NULL = "ERR06383";
    public static final String SERVICETAXPERCENTAGE_COMPANY_NOT_NULL = "ERR06384";
    public static final String SERVICETAXPERCENTAGE_COUNTRY_NOT_NULL = "ERR06385";
    public static final String SERVICETAXPERCENTAGE_STATUS_NOT_NULL = "ERR06386";

    public static final String SUBLEGDER_CODE_NOT_NULL = "ERR06387";
    public static final String GENERALEDGER_CODE_NOT_NULL = "ERR06388";
    public static final String CURRENCYMASTER_CODE_NOT_NULL = "ERR06389";


    public static final String SUBLEGDER_STATUS_BLOCKED = "ERR06390";
    public static final String GENERALEDGER_STATUS_BLOCKED = "ERR06391";
    public static final String CURRENCYMASTER_STATUS_BLOCKED = "ERR06392";


    public static final String REPORT_LIST_EMPTY = "ERR06353";
    public static final String REPORT_NOT_FOUND = "ERR06354";
    public static final String REPORT_CODE_NOT_NULL = "ERR06355";
    public static final String REPORT_CODE_INVALID = "ERR06356";
    public static final String REPORT_NAME_NOT_NULL = "ERR06357";
    public static final String REPORT_NAME_INVALID = "ERR06358";
    public static final String REPORT_CODE_ALREADY_EXIST = "ERR06362";
    public static final String REPORT_NAME_ALREADY_EXIST = "ERR06363";
    public static final String REPORT_KEY_ALREADY_EXIST = "ERR06369";
    public static final String REPORT_ID_NOT_NULL = "ERR06364";
    public static final String REPORT_ID_NOT_FOUND = "ERR06365";
    public static final String REPORT_CODE_NOT_FOUND = "ERR06366";
    public static final String REPORT_KEY_NOT_NULL = "ERR06367";
    public static final String REPORT_KEY_INVALID = "ERR06368";
    public static final String REPORT_KEY_DOCUMENT_BLOCKED = "ERR06399";
    public static final String REPORT_ISREPORTENABLE_NOTNULL = "ERR06350";
    public static final String REPORT_ISREPORTENABLE_INVALID = "ERR06351";


    //CountryReportConfig
    public static final String COUNTRYREPORT_PAGE_NAME_ALREADY_EXIST = "ERR06278";
    public static final String COUNTRYREPORT_PAGE_CODE_ALREADY_EXIST = "ERR06279";
    public static final String COUNTRYREPORT_ID_NOT_FOUND = "ERR06280";
    public static final String COUNTRYREPORT_ALREADY_EXIST = "ERR06281";
    public static final String COUNTRYREPORT_NAME_NOT_NULL = "ERR06282";
    public static final String COUNTRYREPORT_COUNTRY_NOT_NULL = "ERR06283";
    public static final String COUNTRYREPORT_COPIES_NOT_NULL = "ERR06284";

    //Page Master
    public static final String PAGE_NAME_NOT_NULL = "ERR06285";
    public static final String PAGE_CODE_NOT_NULL = "ERR06286";

    //Surcharge mapping
    public static final String SURCHARGE_MAPPING_ID_NOT_FOUND = "ERR06287";
    public static final String SURCHARGE_MAPPING_DELETE_MESSAGE = "ERR06288";

    public static final String SURCHARGE_MAPPING_CHARGE_MANDATORY = "ERR06289";
    public static final String SURCHARGE_MAPPING_CHARGE_BLOCK = "ERR06290";
    public static final String SURCHARGE_MAPPING_CHARGE_HIDDEN = "ERR06291";
    public static final String SURCHARGE_MAPPING_ALREADY_EXIST = "ERR06292";


    //Dynamic Reports mapping PickupSheet,CargoSals,CaarierVolume,VolumeReportByCountry
    public static final String SERVICE_NAME_MANDATORY = "ERR070001";
    public static final String FROM_DATE_MANDATORY = "ERR070002";
    public static final String TO_DATE_MANDATORY = "ERR070003";
    public static final String FROM_DATE_LESS_THAN_ON_TODATE = "ERR070004";
    public static final String ORIGIN_MANDATORY = "ERR070005";
    public static final String DESTINATION_MANDATORY = "ERR070006";
    public static final String PORT_MANDATORY = "ERR070007";
    public static final String COUNTRY_MANDATORY = "ERR070008";
    public static final String CARRIER_MANDATORY = "ERR070009";
    public static final String SELCT_MANDATORY_FIELDS = "ERR0700010";
    public static final String NO_REPORT_FOUND = "ERR0700011";


    public static final String SERVICE_MAPPING_NOT_DONE = "ERR0700012";
    public static final String NO_AGENT_FOUND = "ERR0700013";
    public static final String IMPORT_TO_EXPORT_SERVICE_EXIST = "ERR0700014";
    public static final String SHIPMENT_SERVICE_EXIST = "ERR0700015";
    public static final String REPORT_SAVE_ALERT = "ERR0700016";


    public static final String CHARTER_NOT_FOUND = "ERR16162";
    public static final String CHARTER_CODE_NOT_NULL = "ERR16163";
    public static final String CHARTER_CODE_INVALID = "ERR16164";
    public static final String CHARTER_NAME_NOT_NULL = "ERR16165";
    public static final String CHARTER_NAME_INVALID = "ERR16166";
    public static final String CHARTER_LOV_STATUS_NOT_NULL = "ERR16167";
    public static final String CHARTER_LOV_STATUS_INVALID = "ERR16168";
    public static final String CHARTER_LOV_STATUS_LENGTH_EXCEED = "ERR16169";
    public static final String CHARTER_CODE_ALREADY_EXIST = "ERR161610";
    public static final String CHARTER_NAME_ALREADY_EXIST = "ERR161611";
    public static final String CHARTER_ID_NOT_NULL = "161612";
    public static final String CHARTER_ID_NOT_FOUND = "ERR161613";
    public static final String CHARTER_CODE_NOT_FOUND = "ERR161614";
    public static final String CHARTER_DELETE_NOTIFICATION = "ERR161615";


    //Account Master
    public static final String ACCOUNT_ID_NOT_NULL = "ERR1015";
    public static final String ACCOUNT_ID_NOT_FOUND = "ERR1016";
    public static final String ACCOUNT_CODE_ALREADY_EXIST = "ERR1017";
    public static final String ACCOUNT_NAME_ALREADY_EXIST = "ERR1018";
    public static final String ACCOUNT_NOT_NULL = "ERR1019";
    public static final String ACCOUNT_CODE_NOT_NULL = "ERR1020";
    public static final String ACCOUNT_NAME_NOT_NULL = "ERR1021";
    public static final String ACCOUNT_STATUS_NOT_NULL = "ERR1022";
    public static final String ACCOUNT_CODE_INVALID = "ERR1023";
    public static final String ACCOUNT_NAME_INVALID = "ERR1024";
    public static final String ACCOUNTMASTER_LIST_EMPTY = "ERR1025";
    public static final String ACCOUNT_ID_INVALID = "ERR1026";


    public static final String NO_AUDIT_HISTORY_FOUND = "ERR0700012";

    //Comment Master
    public static final String COMMENT_LIST_EMPTY = "ERR1027";
    public static final String COMMENT_NOT_FOUND = "ERR1028";
    public static final String COMMENT_CODE_NOT_NULL = "ERR1029";
    public static final String COMMENT_CODE_INVALID = "ERR1030";
    public static final String COMMENT_NAME_NOT_NULL = "ERR1031";
    public static final String COMMENT_NAME_INVALID = "ERR1032";
    public static final String COMMENT_LOV_STATUS_NOT_NULL = "ERR1033";
    public static final String COMMENT_LOV_STATUS_INVALID = "ERR1034";
    public static final String COMMENT_LOV_STATUS_LENGTH_EXCEED = "ERR1035";
    public static final String COMMENT_CODE_ALREADY_EXIST = "ERR1036";
    public static final String COMMENT_NAME_ALREADY_EXIST = "ERR1037";
    public static final String COMMENT_ID_NOT_NULL = "ERR1038";
    public static final String COMMENT_ID_NOT_FOUND = "ERR1039";
    public static final String COMMENT_CODE_NOT_FOUND = "ERR1040";

    //WIN WEB CONNECT EXCEPTIONS
    public static final String AGENT_IS_NOT_MAPPED_FOR_WIN_WEB_CONNECT = "ERR8900";
    public static final String GROSS_WEIGHT_IS_INVALID = "ERR8901";
    public static final String WEIGHT_CHARGE_MANDATORY = "ERR8902";
    public static final String TOKENINVALID = "ERR8903";
    public static final String CARRIER_IS_NOT_MAPPED_FOR_WIN_WEB_CONNECT = "ERR8904";
    public static final String WIN_WB_PROCESS_SUCCESS_TO_QUEUE = "ERR8905";
    public static final String WIN_WB_PROCESS_FAILED_TO_QUEUE = "ERR8906";
    public static final String EDI_CONFIGURATION_NOT_CONFIGURED = "ERR8907";


    public static final String RAL_NAME_ALREADY_EXIST = "ERR4200";
    public static final String RAL_NAME_EMPTY = "ERR4201";
    public static final String RAL_COUNTRY_REQUIRED = "ERR4202";
    public static final String RAL_COUNTRY_BLOCKED = "ERR4203";
    public static final String RAL_LOCATION_REQUIRED = "ERR4204";
    public static final String RAL_LOCATION_BLOCKED = "ERR4205";
    public static final String RAL_DIVISION_REQUIRED = "ERR4206";
    public static final String RAL_DIVISION_BLOCKED = "ERR4207";
    public static final String RAL_SERVICE_REQUIRED = "ERR4208";
    public static final String RAL_SERVICE_BLOCKED = "ERR4209";
    public static final String RAL_SALESMAN_REQUIRED = "ERR4210";
    public static final String RAL_SALESMAN_TERMINATED = "ERR4211";
    public static final String RAL_SALESMAN_RESIGNED = "ERR4212";
    public static final String RAL_CUSTOMER_REQUIRED = "ERR4213";
    public static final String RAL_CUSTOMER_BLOCKED = "ERR4214";
    public static final String RAL_USER_REQUIRED = "ERR4215";
    public static final String RAL_USER_BLOCKED = "ERR4216";
    public static final String RAL_DELETE_CONFIRM_MESSAGE = "ERR4217";


    public static final String GENERALNOTE_CATEGORY_NOT_NULL = "ERR4218";
    public static final String GENERALNOTE_SUB_CATEGORY_NOT_NULL = "ERR4219";
    public static final String GENERALNOTE_SERVICE_NOT_NULL = "ERR4220";
    public static final String GENERALNOTE_LOCATION_NOT_NULL = "ERR4221";
    public static final String GENERALNOTE_ID_NOT_NULL = "ERR4222";
    public static final String GENERALNOTE_INVALID_NOTE_LENGTH = "ERR4223";

    //Meta configuration error codes
    public static final String META_CONF_DATA_BLOCKED = "ERR05324";
    public static final String META_CONF_DATA_HIDDEN = "ERR05325";
    public static final String META_CONF_DATA_IS_NOT_FOUND = "ERR05326";
    public static final String META_CONF_DATA_IS_MANDATORY = "ERR05327";
    public static final String META_CONF_STATE_IS_NOT_IN_COUNTRY = "ERR05333";
    public static final String META_CONF_CITY_IS_NOT_IN_STATE_COUNTRY = "ERR05334";
    public static final String META_CONF_SELECT_ONLY_EXCEL_FILES = "ERR05400";
    public static final String META_CONF_FILE_INVALID = "ERR05401";
    public static final String META_CONF_FILE_DUPLICATE_FOUND = "ERR05402";
    public static final String META_CONF_XLSX_FILE_REQUIRED = "ERR05414";
    public static final String META_CONF_XLSX_FILE_EMPTY = "ERR05415";
    public static final String META_CONF_XLSX_FILE_LENGTH_EXCEED = "ERR05416";
    public static final String META_CONF_CUSTOMER_TYPE_FORMAT = "ERR05417";
    public static final String META_CONF_AGENT_DATA_UPLOADED = "ERR05418";
    public static final String META_CONF_CUSTOMER_DATA_UPLOADED = "ERR05419";
    public static final String META_CONF_CHARGE_DATA_UPLOADED = "ERR05420";
    //Meta config ends here

    //tax/vat setup error codes starts here
    public static final String TAXORVAT_SETUP_SERVICE = "ERR05440";
    public static final String TAXORVAT_SETUP_SERVICE_BLOCK = "ERR05441";
    public static final String TAXORVAT_SETUP_SERVICE_HIDE = "ERR05442";

    public static final String TAXORVAT_SETUP_CHARGE = "ERR05443";
    public static final String TAXORVAT_SETUP_CHARGE_BLOCK = "ERR05444";
    public static final String TAXORVAT_SETUP_CHARGE_HIDE = "ERR05445";

    public static final String TAXORVAT_SETUP_CATEGORY = "ERR05446";
    public static final String TAXORVAT_SETUP_CATEGORY_BLOCK = "ERR05447";
    public static final String TAXORVAT_SETUP_CATEGORY_HIDE = "ERR05448";
    //tax/vat setup error codes ends here


    public static final String UR_SUCCESS = "ERRC0001";
    public static final String UR_FAILURE = "ERRC0002";
    public static final String UR_FAILURE_REQUIRED_DATA_MISSING = "ERRC0003";

    public static final String UR_COUNTRY_CODE_INVALID = "ERRC0006";
    public static final String UR_PHONE_NUMBER_VALID = "ERRC0007";
    public static final String UR_PHONE_NUMBER_INVALID = "ERRC0008";

    public static final String UR_COMPANY_NAME_UNIQUE = "ERRC0009";
    public static final String UR_COMPANY_NAME_DUPLICATED = "ERRC0010";

    public static final String UR_SALUTATION_REQUIRED = "ERRC0101";
    public static final String UR_FIRST_NAME_REQUIRED = "ERRC0102";
    public static final String UR_LAST_NAME_REQUIRED = "ERRC0103";
    public static final String UR_EMAIL_REQUIRED = "ERRC0104";
    public static final String UR_COUNTRY_REQUIRED = "ERRC0105";
    public static final String UR_PHONE_REQUIRED = "ERRC0106";
    public static final String UR_REG_NAME_REQUIRED = "ERRC0107";
    public static final String UR_ADDRESS_LINE1_REQUIRED = "ERRC0108";
    public static final String UR_ADDRESS_LINE2_REQUIRED = "ERRC0109";
    public static final String UR_ADDRESS_LINE3_REQUIRED = "ERRC0110";
    public static final String UR_AREA_REQUIRED = "ERRC0111";

    public static final String UR_POSITION_REQUIRED = "ERRC0113";
    public static final String UR_USERNAME_REQUIRED = "ERRC0114";
    public static final String UR_PASSWORD_REQUIRED = "ERRC0115";
    public static final String UR_SERVICE_REQUIRED = "ERRC0118";
    public static final String UR_RECAPTCHA_INVALID = "ERRC0119";

    public static final String UR_SAAS_DB_CREATION_FAILED = "ERRC0120";
    public static final String UR_SAAS_DB_DATASOURCE_WRITE_FILE_FAILED = "ERRC0121";
    public static final String UR_SAAS_DB_TABLE_CREATION_FAILED = "ERRC0122";
    public static final String UR_SAAS_DB_ACTIVITY_1_TABLE_CREATION_FAILED = "ERRC0123";
    public static final String UR_SAAS_DB_ACTIVITY_2_TABLE_CREATION_FAILED = "ERRC0124";
    public static final String UR_SAAS_DB_ACTIVITY_3_TABLE_CREATION_FAILED = "ERRC0125";

    public static final String UR_TIME_ZONE_REQUIRED = "ERRC0126";
    public static final String UR_DATE_FORMAT_REQUIRED = "ERRC0127";

    public static final String UR_USER_PROFILE_NOT_FOUND = "ERRC0200";
    public static final String UR_LOCATION_NOT_FOUND = "ERRC0200";

    public static final String UR_FIRSTNAME_REQUIRED = "ERRC0202";
    public static final String UR_LASTNAME_REQUIRED = "ERRC0203";

    public static final String UR_EMAIL_INVALID = "ERRC0204";
    public static final String UR_PASSWORD_INVALID = "ERRC0205";

    public static final String UR_FIRST_NAME_INVALID = "ERRC0208";
    public static final String UR_LAST_NAME_INVALID = "ERRC0209";
    public static final String UR_CITY_REQUIRED = "ERRC0210";
    public static final String UR_UNABLE_TO_COMMUNICATE_REPORT_APP = "ERRC0211";

    public static final String UR_COMMON_TIME_ZONE_ACTUAL_DUPLICATED = "ERRC0213";
    public static final String UR_COMMON_TIME_ZONE_DISPLAY_DUPLICATED = "ERRC0214";


    public static final String GROUP_CODE_ALREADY_EXIST = "ERR4217";
    public static final String GROUP_NAME_ALREADY_EXIST = "ERR4217";
    public static final String SUBGROUP_CODE_ALREADY_EXIST = "ERR4217";
    public static final String SUBGROUP_NAME_ALREADY_EXIST = "ERR4217";


    //Report Template
    public static final String REPORT_TEMPLATE_CATEGORY_NOT_NULL_OR_EMPTY = "ERRRT001";
    public static final String REPORT_TEMPLATE_NAME_NOT_NULL_OR_EMPTY = "ERRRT002";
    public static final String REPORT_TEMPLATE_COMP_UK_CATEGORY_AND_NAME = "ERRRT003";


    //Air line prebooking error codes
    public static final String AIRLINE_PREBOOKING_MAWB_MANDATORY = "ERR70000";

    public static final String AIRLINE_PREBOOKING_CARRIER_MANDATORY = "ERR70001";
    public static final String AIRLINE_PREBOOKING_CARRIER_BLOCKED = "ERR70002";
    public static final String AIRLINE_PREBOOKING_CARRIER_HIDDDEN = "ERR70003";

    public static final String AIRLINE_PREBOOKING_DATE_MANDATORY = "ERR70004";
    public static final String AIRLINE_PREBOOKING_DATE_NOT_OLD_DATE = "ERR70005";

    public static final String AIRLINE_PREBOOKING_SERVICE_MANDATORY = "ERR70006";
    public static final String AIRLINE_PREBOOKING_SERVICE_BLOCKED = "ERR70007";
    public static final String AIRLINE_PREBOOKING_SERVICE_HIDDDEN = "ERR70008";

    public static final String AIRLINE_PREBOOKING_POR_MANDATORY = "ERR70009";
    public static final String AIRLINE_PREBOOKING_POR_BLOCKED = "ERR70010";
    public static final String AIRLINE_PREBOOKING_POR_HIDDDEN = "ERR70011";

    public static final String AIRLINE_PREBOOKING_CUSTOMER_MANDATORY = "ERR70012";
    public static final String AIRLINE_PREBOOKING_CUSTOMER_BLOCKED = "ERR70013";
    public static final String AIRLINE_PREBOOKING_CUSTOMER_HIDDDEN = "ERR70014";

    public static final String AIRLINE_PREBOOKING_NO_OF_MAWBS_INVALID = "ERR70015";
    public static final String AIRLINE_PREBOOKING_NO_OF_MAWBS_NEGATIVE = "ERR70016";

    public static final String AIRLINE_PREBOOKING_NO_OF_PIECES_INVALID = "ERR70017";
    public static final String AIRLINE_PREBOOKING_NO_OF_PIECES_NEGATIVE = "ERR70018";

    public static final String AIRLINE_PREBOOKING_CHARTER_BLOCKED = "ERR70019";
    public static final String AIRLINE_PREBOOKING_CHARTER_HIDDDEN = "ERR70020";

    public static final String AIRLINE_PREBOOKING_VOLUME_CBM_INVALID = "ERR70021";
    public static final String AIRLINE_PREBOOKING_VOLUME_CBM_NEGATIVE = "ERR70022";

    public static final String AIRLINE_PREBOOKING_GROSS_WEIGHT_INVALID = "ERR70023";
    public static final String AIRLINE_PREBOOKING_GROSS_WEIGHT_NEGATIVE = "ERR70024";

    public static final String AIRLINE_PREBOOKING_VOLUME_WEIGHT_INVALID = "ERR70025";
    public static final String AIRLINE_PREBOOKING_VOLUME_WEIGHT_NEGATIVE = "ERR70026";

    public static final String AIRLINE_PREBOOKING_SCHEDULELIST_NOT_EMPTY = "ERR70027";

    //Error codes for bill of entry
    public static final String BILL_OF_ENTRY_REQUIRED = "ERR70100";
    public static final String BILL_OF_ENTRY_DECLARATION_NO_INVALID = "ERR70101";
    public static final String BILL_OF_ENTRY_BOE_DATE_INVALID = "ERR70102";
    public static final String BILL_OF_ENTRY_BOE_VALUE_INVLAID = "ERR70103";
    public static final String BILL_OF_ENTRY_BOE_INVOICENO_INVALID = "ERR70104";
    public static final String BILL_OF_ENTRY_TRANSACTION_TYPE_REQUIRED = "ERR70105";
    public static final String BILL_OF_ENTRY_AMOUNT_INVALID = "ERR70106";
    public static final String BILL_OF_ENTRY_PROCESS_DATE_INVALID = "ERR70107";
    public static final String BILL_OF_ENTRY_ACKNO_INVALID = "ERR70108";
    public static final String BILL_OF_ENTRY_ACK_DATE_INVALID = "ERR70109";
    public static final String BILL_OF_ENTRY_NOTE_INVALID = "ERR70110";
    public static final String BILL_OF_ENTRY_INVALID = "ERR70111";


    public static final String EMAIL_TEMPALTE_EVENT_TYPE_REQUIRED = "ERR2000300";
    public static final String EMAIL_TEMPALTE_SUBJECT_REQUIRED = "ERR2000301";
    public static final String EMAIL_TEMPALTE_SUBJECT_INVALID = "ERR2000302";
    public static final String EMAIL_TEMPALTE_SENDER_EMAIL_REQUIRED = "ERR2000303";
    public static final String EMAIL_TEMPALTE_SENDER_PASSWORD_REQUIRED = "ERR2000304";
    public static final String EMAIL_TEMPALTE_SENDER_PASSWORD_INVALID = "ERR2000305";
    public static final String EMAIL_TEMPALTE_TYPE_REQUIRED = "ERR2000306";
    public static final String EMAIL_TEMPALTE_SMTP_SERVER_REQUIRED = "ERR2000307";
    public static final String EMAIL_TEMPALTE_SMTP_SERVER_INVALID = "ERR2000308";
    public static final String EMAIL_TEMPALTE_SMTP_PORT_REQUIRED = "ERR2000309";
    public static final String EMAIL_TEMPALTE_SMTP_PORT_LESS_INVALID = "ERR2000310";
    public static final String EMAIL_TEMPALTE_SMTP_PORT_GREATER_INVALID = "ERR2000311";

//Job ledger

    public static final String JOB_LEDGER_SUCCESS = "ERR2000400";
    public static final String JOB_LEDGER_FAILURE = "ERR2000401";


}
