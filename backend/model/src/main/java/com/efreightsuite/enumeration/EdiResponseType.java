package com.efreightsuite.enumeration;

public enum EdiResponseType {

    DLV, BKD, RCS, MAN, DEP, TFD, RCT, RCF, NFD, AWD, TRM, CCD, DIS

}
