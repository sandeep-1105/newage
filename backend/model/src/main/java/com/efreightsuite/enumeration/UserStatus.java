package com.efreightsuite.enumeration;

public enum UserStatus {
    To_be_activated, Active, Deactivate
}
