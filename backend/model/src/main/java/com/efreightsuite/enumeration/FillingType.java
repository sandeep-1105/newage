package com.efreightsuite.enumeration;

public enum FillingType {
    PRE_SHIPMENT, POST_SHIPMENT
}
