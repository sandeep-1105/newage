package com.efreightsuite.enumeration;

public enum RemarkType {
    External, Internal;
}
