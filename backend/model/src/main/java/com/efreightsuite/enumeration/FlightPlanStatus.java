package com.efreightsuite.enumeration;

public enum FlightPlanStatus {

    Planned, Completed

}
