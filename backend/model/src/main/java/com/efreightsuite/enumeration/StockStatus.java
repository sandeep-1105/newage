package com.efreightsuite.enumeration;

public enum StockStatus {

    Available, Cancelled, Generated, Reserved;

}
