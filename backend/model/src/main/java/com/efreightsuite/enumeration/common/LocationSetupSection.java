package com.efreightsuite.enumeration.common;

public enum LocationSetupSection {
    Company, Login, Configuration, MetaConfig, Finance, Charge, Other, Trail, Completed
}
