package com.efreightsuite.enumeration;

public enum UnitType {
    Container, Shipment, Unit
}
