package com.efreightsuite.enumeration;

public enum DocumentType {
    INV, CRN, PMT, RPT
}
