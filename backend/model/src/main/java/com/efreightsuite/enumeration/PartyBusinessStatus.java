package com.efreightsuite.enumeration;

public enum PartyBusinessStatus {

    Active,
    Gained,
    Lost,
    Cancel,
    Hold

}
