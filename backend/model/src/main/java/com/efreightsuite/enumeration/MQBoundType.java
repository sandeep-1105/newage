package com.efreightsuite.enumeration;

public enum MQBoundType {
    INBOUND, OUTBOUND
}
