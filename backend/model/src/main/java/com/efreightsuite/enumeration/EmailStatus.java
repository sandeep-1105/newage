package com.efreightsuite.enumeration;

public enum EmailStatus {
    PENDING, PROGRESSING, SUCCESS, FAILURE;
}
