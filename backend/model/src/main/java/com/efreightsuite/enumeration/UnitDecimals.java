package com.efreightsuite.enumeration;

public enum UnitDecimals {
    NA, ZERO, ONE, TWO, THREE
}
