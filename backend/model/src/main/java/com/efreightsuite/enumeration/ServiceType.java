package com.efreightsuite.enumeration;

public enum ServiceType {
    Clearance, Haulage, Project, RoRo, Transportation
}
