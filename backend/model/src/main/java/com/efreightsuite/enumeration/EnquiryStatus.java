package com.efreightsuite.enumeration;

/**
 * @author Devendrachary M
 */
public enum EnquiryStatus {
    Active, Gained, Lost, Cancel;
}
