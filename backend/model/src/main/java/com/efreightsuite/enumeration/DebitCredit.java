package com.efreightsuite.enumeration;

public enum DebitCredit {
    Debit, Credit
}
