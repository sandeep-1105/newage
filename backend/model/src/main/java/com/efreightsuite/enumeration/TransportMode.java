package com.efreightsuite.enumeration;

public enum TransportMode {
    Air, Ocean, Rail, Road;
}
