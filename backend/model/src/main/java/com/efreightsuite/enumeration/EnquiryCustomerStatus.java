package com.efreightsuite.enumeration;

public enum EnquiryCustomerStatus {
    Pending, Approved, Rejected, Duplicated;
}
