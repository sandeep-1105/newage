package com.efreightsuite.enumeration;

public enum FlexDataType {
    String, Boolean, Integer, Double, Date, Time, DateAndTime, Dropdown
}
