package com.efreightsuite.enumeration;

public enum RateClass {
    C, M, N, Q, U;
}
