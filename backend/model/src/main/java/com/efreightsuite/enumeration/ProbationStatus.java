package com.efreightsuite.enumeration;

public enum ProbationStatus {
    Confirmed, Contractual, Probation, Temporary, Trainee
}
