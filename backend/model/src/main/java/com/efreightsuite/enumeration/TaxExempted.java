package com.efreightsuite.enumeration;

public enum TaxExempted {

    SERVICETAX, VAT
}
