package com.efreightsuite.enumeration;

public enum ScopeFlag {
    Module, Global, Company, Country, Zone, Branch
}
