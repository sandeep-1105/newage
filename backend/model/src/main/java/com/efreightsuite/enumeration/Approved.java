package com.efreightsuite.enumeration;

public enum Approved {
    Pending, Approved, Rejected, Gained, ClientApproved
}
