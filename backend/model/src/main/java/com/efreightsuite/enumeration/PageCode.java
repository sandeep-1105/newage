package com.efreightsuite.enumeration;

public enum PageCode {

    PAGE_1, PAGE_2, PAGE_5, PAGE_3, PAGE_4,

    PAGE_6, PAGE_7, PAGE_8, PAGE_9, PAGE_10,

    PAGE_11, PAGE_12, PAGE_15, PAGE_13, PAGE_14;
}
