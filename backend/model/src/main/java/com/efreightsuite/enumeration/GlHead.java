package com.efreightsuite.enumeration;

public enum GlHead {
    Assets, Liabilities, Incomes, Expenditures, Equity
}
