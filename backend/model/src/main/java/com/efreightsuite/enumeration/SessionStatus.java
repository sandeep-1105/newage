package com.efreightsuite.enumeration;

public enum SessionStatus {
    CREATED, INVALIDATED, EXPIRED
}
