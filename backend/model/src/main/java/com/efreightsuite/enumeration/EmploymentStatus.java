package com.efreightsuite.enumeration;

public enum EmploymentStatus {
    EMPLOYED, REJOINED, RESIGNED, TERMINATED, TRANSFERRED
}
