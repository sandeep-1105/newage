package com.efreightsuite.enumeration;

public enum Reason {

    Rates, Services, Others
}
