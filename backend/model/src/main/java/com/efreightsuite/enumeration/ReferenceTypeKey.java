package com.efreightsuite.enumeration;

public enum ReferenceTypeKey {
    Enquiry, Quotation, Shipment, Master;
}
