package com.efreightsuite.enumeration;

public enum IdType {
    EIN, SSN, DUNS, SCAC, CBP
}
