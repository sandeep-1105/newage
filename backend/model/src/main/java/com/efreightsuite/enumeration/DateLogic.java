package com.efreightsuite.enumeration;

public enum DateLogic {
    ETD, ETA, ATD, ATA, Date;
}
