package com.efreightsuite.enumeration;

public enum BondActivityCode {
    TISF, IMPORTER_OR_BROKER, INTERNATIONAL_CARRIER, CUSTODIAN_OF_BOND, FOREIGN_TRADE_ZONE_OPERATOR
}
