package com.efreightsuite.enumeration;

public enum ActivitiGroup {
    Enquiry, Quotation, Shipment, Pickup, DeliveryToCfs, LinkToMaster, AesOrCustoms, SignOff, AirLineSubmission,
    JobCompletion, Invoice, AtdConfirmation, AtaConfirmation, DeliveryOrder, Clearance, Pod, CanAndInvoice, Customs
}
