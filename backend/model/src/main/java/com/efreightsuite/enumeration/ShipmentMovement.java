package com.efreightsuite.enumeration;


public enum ShipmentMovement {
    General, SOC, COC
}
