package com.efreightsuite.enumeration;

public enum MaritalStatus {
    Single, Married, Widowed, Divorced
}
