package com.efreightsuite.enumeration;

public enum Measurement {
    Imperial, Metric
}
