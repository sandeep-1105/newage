package com.efreightsuite.enumeration;

public enum ConsigneeType {
    DIRECT_CUSTOMER, GOVT_ENTITY, RESELLER, OTHERS
}
