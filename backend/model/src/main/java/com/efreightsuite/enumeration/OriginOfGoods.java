package com.efreightsuite.enumeration;

public enum OriginOfGoods {
    Domestic, Foreign
}
