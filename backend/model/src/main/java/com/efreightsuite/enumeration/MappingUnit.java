package com.efreightsuite.enumeration;

public enum MappingUnit {
    GROSS_WEIGHT, PIECES, VOLUME, VOLUME_WEIGHT, DAY, DOC, TEU, FEU, CHARGEABLE_UNIT, POUND, CFT;
}
