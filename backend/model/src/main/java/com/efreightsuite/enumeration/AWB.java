package com.efreightsuite.enumeration;

public enum AWB {
    Original, Express, OriginalSurrendered, No
}
