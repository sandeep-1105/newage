package com.efreightsuite.enumeration;

public enum ConnectionStatus {

    Planned, Completed

}
