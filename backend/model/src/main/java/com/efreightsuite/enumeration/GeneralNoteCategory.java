package com.efreightsuite.enumeration;

public enum GeneralNoteCategory {
    Enquiry, Quotation, Shipment, Consol, Invoice
}
