package com.efreightsuite.dto;

import lombok.Data;

@Data
public class CfsReceiveSearchRequestDto {

    Integer id;
    String searchReceiptNumber;
    DateRange searchReceiptDate;
    String searchCSF;
    String searchShipmentUid;
    String searchProNumber;
    DateRange searchProDate;
    String searchShipper;
    String searchConsignee;
    String searchShipperRefNumber;
    String searchOnHand;
    String searchTrucker;

    String searchName;
    String searchValue;

    String sortByColumn;
    String orderByType;

    Integer selectedPageNumber = 0;
    Integer recordPerPage = 10;
}
