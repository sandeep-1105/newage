package com.efreightsuite.dto;

import lombok.Data;

@Data
public class ReferenceTypeSearchDto {

    String searchReferenceTypeCode;
    String searchReferenceTypeName;
    String searchStatus;

    String sortByColumn;
    String orderByType;

    Integer selectedPageNumber = 0;

    Integer recordPerPage = 5;

}
