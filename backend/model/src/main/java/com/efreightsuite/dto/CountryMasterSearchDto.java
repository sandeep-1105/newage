package com.efreightsuite.dto;

import com.efreightsuite.enumeration.LovStatus;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class CountryMasterSearchDto extends  AbstractSearchDto {

    String searchCountryName;
    String searchCountryCode;
    String searchNationality;
    String searchWeightOrCbm;
    LovStatus searchStatus;
    String searchCurrencyCode;

    public CountryMasterSearchDto(){}

    public CountryMasterSearchDto(String searchCountryCode) {
        this.searchCountryCode = searchCountryCode;
    }

    public CountryMasterSearchDto(String searchCountryName, String searchCountryCode) {
        this.searchCountryName = searchCountryName;
        this.searchCountryCode = searchCountryCode;
    }

    public CountryMasterSearchDto(String searchCountryName, String searchCountryCode, String searchCurrencyCode) {
        this.searchCountryName = searchCountryName;
        this.searchCountryCode = searchCountryCode;
        this.searchCurrencyCode = searchCurrencyCode;
    }

    private CountryMasterSearchDto(Builder builder) {
        setSearchCountryName(builder.searchCountryName);
        setSearchCountryCode(builder.searchCountryCode);
        setSearchNationality(builder.searchNationality);
        setSearchWeightOrCbm(builder.searchWeightOrCbm);
        setSearchStatus(builder.searchStatus);
        setSearchCurrencyCode(builder.searchCurrencyCode);
        setSortByColumn(builder.sortByColumn);
        setOrderByType(builder.orderByType);
        setSelectedPageNumber(builder.selectedPageNumber);
        setRecordPerPage(builder.recordPerPage);
    }


    public static final class Builder {
        private String searchCountryName;
        private String searchCountryCode;
        private String searchNationality;
        private String searchWeightOrCbm;
        private LovStatus searchStatus;
        private String searchCurrencyCode;
        private String sortByColumn;
        private String orderByType;
        private Integer selectedPageNumber;
        private Integer recordPerPage;

        public Builder() {
        }

        public Builder(String countryName, String countryCode, String currencyCode) {
            this.searchCountryName = countryName;
            this.searchCountryCode = countryCode;
            this.searchCurrencyCode = currencyCode;

        }

        public Builder withSearchCountryName(String val) {
            searchCountryName = val;
            return this;
        }

        public Builder withSearchCountryCode(String val) {
            searchCountryCode = val;
            return this;
        }

        public Builder withSearchNationality(String val) {
            searchNationality = val;
            return this;
        }

        public Builder withSearchWeightOrCbm(String val) {
            searchWeightOrCbm = val;
            return this;
        }

        public Builder withSearchStatus(LovStatus val) {
            searchStatus = val;
            return this;
        }

        public Builder withSearchCurrencyCode(String val) {
            searchCurrencyCode = val;
            return this;
        }

        public Builder withSortByColumn(String val) {
            sortByColumn = val;
            return this;
        }

        public Builder withOrderByType(String val) {
            orderByType = val;
            return this;
        }

        public Builder withSelectedPageNumber(Integer val) {
            selectedPageNumber = val;
            return this;
        }

        public Builder withRecordPerPage(Integer val) {
            recordPerPage = val;
            return this;
        }

        public CountryMasterSearchDto build() {
            return new CountryMasterSearchDto(this);
        }
    }
}
