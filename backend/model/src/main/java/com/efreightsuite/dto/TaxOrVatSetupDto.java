package com.efreightsuite.dto;

import java.util.Date;

import com.efreightsuite.model.CategoryMaster;
import com.efreightsuite.model.ChargeMaster;
import com.efreightsuite.model.ServiceMaster;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TaxOrVatSetupDto {

    ServiceMaster serviceMaster;

    ChargeMaster chargeMaster;

    CategoryMaster categoryMaster;

    Integer percentage;

    Date validFrom;

}
