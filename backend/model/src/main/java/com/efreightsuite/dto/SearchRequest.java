package com.efreightsuite.dto;

import lombok.Data;
import org.apache.commons.lang3.StringUtils;

@Data
public class SearchRequest {

    String keyword = StringUtils.EMPTY;

    String id = StringUtils.EMPTY;

    Integer selectedPageNumber = 0;

    Integer recordPerPage = 20;

    String status;
    String param1;
    String param2;
    String param3;
    String param4;

    String ids[];

    public SearchRequest() {
    }

    public SearchRequest(String keyword) {
        this.keyword = keyword;
    }

    public SearchRequest(String keyword, Integer selectedPageNumber) {
        this(keyword);
        this.selectedPageNumber = selectedPageNumber;

    }

    public SearchRequest(String keyword, Integer selectedPageNumber, Integer recordPerPage) {
        this(keyword, selectedPageNumber);
        this.recordPerPage = recordPerPage;
    }

    public String getKeyword() {
        return this.keyword == null ? StringUtils.EMPTY : this.keyword.trim();
    }

    public String getKeywordNullable() {
        return this.keyword == null ? null : this.keyword.trim();
    }
}
