package com.efreightsuite.dto;

import com.efreightsuite.enumeration.LovStatus;
import lombok.Data;

@Data
public class ChargeMasterSearchResponseDto {

    Long id;

    String chargeCode;

    String chargeName;

    LovStatus status;

    public ChargeMasterSearchResponseDto() {

    }

    public ChargeMasterSearchResponseDto(Long id, String chargeCode, String chargeName, LovStatus status) {
        super();
        this.id = id;
        this.chargeCode = chargeCode;
        this.chargeName = chargeName;
        this.status = status;
    }

}
