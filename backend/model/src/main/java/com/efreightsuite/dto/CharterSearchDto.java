package com.efreightsuite.dto;

import lombok.Data;

@Data
public class CharterSearchDto {

    String searchCharterCode;
    String searchCharterName;
    String searchStatus;

    String sortByColumn;
    String orderByType;

    Integer selectedPageNumber = 0;

    Integer recordPerPage = 5;

}


