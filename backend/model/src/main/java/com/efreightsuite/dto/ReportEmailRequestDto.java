package com.efreightsuite.dto;

import java.util.List;

import javax.persistence.Lob;

import com.efreightsuite.enumeration.ReportName;
import com.efreightsuite.model.EmailTemplate;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class ReportEmailRequestDto extends EmailTemplate {

    /**
     *
     */
    private static final long serialVersionUID = 1L;


    /* List of email ID of receivers */
    String toEmailIdList;

    /* List of email ID of cc */
    String ccEmailIdList;

    /* List of email ID of BCC */
    String bccEmailIdList;

    /* email body content to send */
    @Lob
    String emailBody;

    @Lob
    String template;


    List<ReportName> reportNamesList;

    String consolUid;

    List<String> reportLength;

}
