package com.efreightsuite.dto;

import lombok.Data;

@Data
public class ServiceSearchDto {

    String searchServiceCode;
    String searchServiceName;
    String searchTransportMode;
    String searchImportExport;
    String searchFullGroupage;
    String searchCostCenter;
    String searchStatus;

    String sortByColumn;
    String orderByType;

    Integer selectedPageNumber = 0;

    Integer recordPerPage = 10;

}
