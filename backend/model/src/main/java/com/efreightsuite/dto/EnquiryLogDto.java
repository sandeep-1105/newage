package com.efreightsuite.dto;

import java.io.Serializable;
import java.util.Date;

import com.efreightsuite.util.JsonDateDeserializer;
import com.efreightsuite.util.JsonDateSerializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class EnquiryLogDto implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    Long id;

    String enquiryNo;

    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date receivedOn;

    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date quoteBy;

    String quotationNo;

    PartyDto partyDto;

}
