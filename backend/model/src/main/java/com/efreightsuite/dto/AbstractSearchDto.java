package com.efreightsuite.dto;

import lombok.Data;
import org.springframework.data.domain.Sort;

@Data
public class AbstractSearchDto {

    String sortByColumn;
    String orderByType;
    Integer selectedPageNumber = 0;
    Integer recordPerPage = 10;

    public Sort getSortCriteria(String sortByColumn){
        Sort sortCriteria = this.getSortByColumn() != null && this.getOrderByType() != null
                ? new Sort(Sort.Direction.valueOf(this.getOrderByType()), this.getSortByColumn())
                : new Sort(Sort.Direction.ASC, sortByColumn);

        return sortCriteria;
    }

}
