package com.efreightsuite.dto;

import lombok.Data;

@Data
public class ForgotPasswordDto {

    String emailId;

    String password;

    String resetPasswordToken;

    String resetPasswordId;

    String resetPasswordSid;

}
