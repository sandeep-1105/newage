package com.efreightsuite.dto;


import lombok.Data;

@Data
public class ServiceMappingSearchDto {

    String searchImport;
    String searchExport;

    String sortByColumn;
    String orderByType;
    Integer selectedPageNumber = 0;
    Integer recordPerPage = 10;
}
