package com.efreightsuite.dto;

import lombok.Data;

@Data
public class DocumentPrefixSearchDto {


    String searchDocumentName;
    String searchDocumentCode;
    String searchDocumentPrefix;
    String searchDocumentSuffix;
    String searchStatus;

    String sortByColumn;
    String orderByType;

    Integer selectedPageNumber = 0;
    Integer recordPerPage = 5;

}
