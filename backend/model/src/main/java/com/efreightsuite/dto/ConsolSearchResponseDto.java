package com.efreightsuite.dto;

import java.io.Serializable;
import java.util.Date;

import com.efreightsuite.enumeration.JobStatus;
import com.efreightsuite.enumeration.PPCC;
import com.efreightsuite.util.JsonDateDeserializer;
import com.efreightsuite.util.JsonDateSerializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Data;

@Data
public class ConsolSearchResponseDto implements Serializable {

    private static final long serialVersionUID = 1L;

    Long id;

    String serviceName;

    String colorCode;

    String masterId;

    String agentName;

    String mawbNumber;

    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date mawbDate;

    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date masterDate;

    String originName;

    String destinationName;

    PPCC frieght;

    String carrierName;

    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date etd;

    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date eta;

    String createdBy;

    JobStatus jobStatus;

    //String directShipment;

    public ConsolSearchResponseDto(Long id, String serviceName, String masterId, String agentName, String colorCode, String mawbNumber,
                                   Date mawbDate, Date masterDate, String originName, String destinationName, PPCC frieght,
                                   String carrierName, Date etd, Date eta, String createdBy, JobStatus jobStatus) {
        super();
        this.id = id;
        this.serviceName = serviceName;
        this.masterId = masterId;
        this.agentName = agentName;
        this.colorCode = colorCode;
        this.mawbNumber = mawbNumber;
        this.mawbDate = mawbDate;
        this.masterDate = masterDate;
        this.originName = originName;
        this.destinationName = destinationName;
        this.frieght = frieght;
        this.carrierName = carrierName;
        this.etd = etd;
        this.eta = eta;
        this.createdBy = createdBy;
        this.jobStatus = jobStatus;
    }


    public ConsolSearchResponseDto() {
    }
}
