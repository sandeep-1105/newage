package com.efreightsuite.dto;

import com.efreightsuite.enumeration.WhichTransactionDate;
import lombok.Data;

@Data
public class DateConfigurationDto {

    /* Its two column is used for get Method -- Start here*/
    Long serviceId;
    WhichTransactionDate whichTransactionDate;
    /* Its two column is used for get Method -- End here*/

    String serviceName;
    String whichTransaction;
    String dateLogic;

    String sortByColumn;
    String orderByType;

    Integer selectedPageNumber = 0;

    Integer recordPerPage = 10;

}
