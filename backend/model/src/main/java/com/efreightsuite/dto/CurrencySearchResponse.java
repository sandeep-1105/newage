package com.efreightsuite.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CurrencySearchResponse {

    Long cFrom;
    Long cTo;
    String cCodeFrom;
    String cCodeTo;
    Double cBuy;
    Double cSell;
}
