package com.efreightsuite.dto;

import lombok.Data;

@Data
public class FlightPlanMawbSearchDto {

    Long carrierId;
    Long porId;
    Long[] ids;
}


