package com.efreightsuite.dto;

import lombok.Data;

@Data
public class SequenceGeneratorSearchDto {
    String searchSequenceName;

    String searchSequenceType;

    String searchCurrentSequenceValue;

    String searchPrefix;

    String searchFormat;

    String sortByColumn;
    String orderByType;

    Integer selectedPageNumber = 0;

    Integer recordPerPage = 5;


}
