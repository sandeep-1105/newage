package com.efreightsuite.dto;

import com.efreightsuite.enumeration.Priority;
import lombok.Data;

@Data
public class GradeMasterSearchDto {

    String searchGradeName;
    Priority searchPriority;
    String searchColorCode;


    String sortByColumn;
    String orderByType;
    Integer selectedPageNumber = 0;
    Integer recordPerPage = 10;
}
