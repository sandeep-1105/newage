package com.efreightsuite.dto;

import java.util.Date;

import com.efreightsuite.util.JsonDateDeserializer;
import com.efreightsuite.util.JsonDateSerializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Data;

@Data
public class JobLedgerResDocumentDto {

    Long invoiceCreditNoteId;

    String serviceCode, masterUid, shipmentUid, serviceUid, dayBookCode, dayBookName, documentType, documentNumber, adjustmentInvoiceNo, documentTypeCode;

    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date documentDate;

    Double amountDebit, amountCredit;

    Double actualRevenue, actualCost, actualNeutral;

}
