package com.efreightsuite.dto;

import lombok.Data;

@Data
public class TrackingShipmentRequest {

    String shipmentUid;

    String mailData;
}
