package com.efreightsuite.dto;

import java.util.Date;

import com.efreightsuite.util.JsonDateDeserializer;
import com.efreightsuite.util.JsonDateSerializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PickupSheetCarrierVolumeReoprtSearchDto {

    String carrierName;
    String serviceCode;
    String origin;
    String destination;
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date fromDate;
    Date toDate;

}
