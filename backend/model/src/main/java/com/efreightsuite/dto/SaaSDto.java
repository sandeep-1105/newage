package com.efreightsuite.dto;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class SaaSDto {

    String saasId;
    String companyName;
    String driverName;
    String url;
    String userName;
    String password;

    public SaaSDto() {
    }

    public SaaSDto(String saasId, String companyName, String driverName, String url, String userName, String password) {
        this.saasId = saasId;
        this.companyName = companyName;
        this.driverName = driverName;
        this.url = url;
        this.userName = userName;
        this.password = password;
    }

}
