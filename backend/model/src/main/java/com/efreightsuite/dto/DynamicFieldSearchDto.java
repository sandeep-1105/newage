package com.efreightsuite.dto;

import java.io.Serializable;

import lombok.Data;

@Data
public class DynamicFieldSearchDto implements Serializable {


    /**
     *
     */
    private static final long serialVersionUID = 1L;

    Long countryId;

    String fieldName;

    String screen;

    String section;

    String sortByColumn;

    String orderByType;

    Integer selectedPageNumber = 0;
    Integer recordPerPage = 10;
}
