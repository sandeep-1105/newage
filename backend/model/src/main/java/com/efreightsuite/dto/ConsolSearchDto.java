package com.efreightsuite.dto;

import lombok.Data;

@Data
public class ConsolSearchDto {

    String importExport;
    String serviceName;
    String consolUid;
    String partyName;
    String masterNo;
    DateRange shipmentDate;
    DateRange mawbDate;
    String origin;
    String destination;
    String freightTerm;
    String carrier;
    DateRange etd;
    DateRange eta;
    String createdBy;
    String status;
    String directShipment;

    String sortByColumn;
    String orderByType;

    String searchName;
    String searchValue;

    Integer selectedPageNumber = 0;
    Integer recordPerPage = 10;
}
