package com.efreightsuite.dto;

import com.efreightsuite.enumeration.StatusOfShipment;
import lombok.Data;

@Data
public class ShipmentLovResponseDto {
    Long id;
    String shipmentUid;
    StatusOfShipment shipmentStatus;

    public ShipmentLovResponseDto(Long id, String shipmentUid, StatusOfShipment shipmentStatus) {
        this.id = id;
        this.shipmentUid = shipmentUid;
        this.shipmentStatus = shipmentStatus;
    }

    public ShipmentLovResponseDto() {
    }
}
