package com.efreightsuite.dto;


import com.efreightsuite.enumeration.ImportExport;
import com.efreightsuite.enumeration.Service;
import lombok.Data;

@Data
public class EnquiryServiceMappingSearchDto {

    Service searchService;
    ImportExport searchImportExport;
    String searchTransport;


    String sortByColumn;
    String orderByType;
    Integer selectedPageNumber = 0;
    Integer recordPerPage = 10;
}
