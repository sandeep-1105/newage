package com.efreightsuite.dto;

import com.efreightsuite.enumeration.LovStatus;
import lombok.Data;

@Data
public class SoftwareMasterSearchDto {

    String searchSoftwareCode;
    String searchSoftwareName;
    LovStatus searchStatus;


    String sortByColumn;
    String orderByType;
    Integer selectedPageNumber = 0;
    Integer recordPerPage = 10;
}
