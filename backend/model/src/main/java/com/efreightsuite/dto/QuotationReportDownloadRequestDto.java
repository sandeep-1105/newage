package com.efreightsuite.dto;

import com.efreightsuite.enumeration.ReportDownloadFileType;
import com.efreightsuite.enumeration.ReportDownloadOption;
import lombok.Data;

@Data
public class QuotationReportDownloadRequestDto {

    ReportDownloadFileType downloadFileType;
    ReportDownloadOption downloadOption;
    Long quotationId;

}
