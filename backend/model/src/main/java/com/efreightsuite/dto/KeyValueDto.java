package com.efreightsuite.dto;

import java.io.Serializable;

import lombok.Data;

@Data
public class KeyValueDto implements Serializable {


    /**
     *
     */
    private static final long serialVersionUID = 1L;

    String key;

    String value;

    public KeyValueDto() {
    }

    public KeyValueDto(String key, String value) {
        this.key = key;
        this.value = value;
    }
}
