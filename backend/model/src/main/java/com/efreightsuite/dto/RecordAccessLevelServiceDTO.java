package com.efreightsuite.dto;

import com.efreightsuite.model.RecordAccessLevelService;
import com.efreightsuite.model.ServiceMaster;
import com.efreightsuite.model.SystemTrack;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RecordAccessLevelServiceDTO {

    Long id;

    long versionLock;

    SystemTrack systemTrack;

    ServiceMaster serviceMaster;

    public RecordAccessLevelServiceDTO() {
        // TODO Auto-generated constructor stub
    }

    public RecordAccessLevelServiceDTO(RecordAccessLevelService entity) {
        this.id = entity.getId();
        this.versionLock = entity.getVersionLock();
        this.systemTrack = entity.getSystemTrack();
        this.serviceMaster = entity.getServiceMaster();
    }

}
