package com.efreightsuite.dto;

import com.efreightsuite.model.DivisionMaster;
import com.efreightsuite.model.RecordAccessLevelDivision;
import com.efreightsuite.model.SystemTrack;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RecordAccessLevelDivisionDTO {

    Long id;

    long versionLock;

    SystemTrack systemTrack;

    DivisionMaster divisionMaster;

    public RecordAccessLevelDivisionDTO() {
        // TODO Auto-generated constructor stub
    }

    public RecordAccessLevelDivisionDTO(RecordAccessLevelDivision entity) {
        this.id = entity.getId();
        this.versionLock = entity.getVersionLock();
        this.systemTrack = entity.getSystemTrack();
        this.divisionMaster = entity.getDivisionMaster();
    }
}
