package com.efreightsuite.dto;

import lombok.Data;

@Data
public class UnitSearchDto {
    String searchUnitCode;
    String searchUnitName;
    String searchUnitType;
    String searchStatus;

    String sortByColumn;
    String orderByType;

    Integer selectedPageNumber = 0;
    Integer recordPerPage = 10;
}
