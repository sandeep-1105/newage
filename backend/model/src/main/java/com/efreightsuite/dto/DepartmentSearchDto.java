package com.efreightsuite.dto;

import lombok.Data;

@Data
public class DepartmentSearchDto {


    String searchDepartmentCode;
    String searchDepartmentName;

    String searchDepartmentHead;
    String searchStatus;

    String sortByColumn;
    String orderByType;

    Integer selectedPageNumber = 0;
    Integer recordPerPage = 10;

}
