package com.efreightsuite.dto;

import lombok.Data;

@Data
public class ShipmentSearchDto {

    String partyName;
    String shipmentUid;

    DateRange shipmentDate;
    String origin;
    String destination;
    String tos;
    String createdBy;
    String status;
    String routed;
    String routedBy;


    DateRange etd;//for aes usage

    String serviceUid;//for aes usage
    String consolUid;//for aes usage
    String mawbNo;//for aes usage


    String sortByColumn;
    String orderByType;
    String shipmentStatus;

    String searchName;
    String searchValue;

    Integer selectedPageNumber = 0;
    Integer recordPerPage = 10;
}
