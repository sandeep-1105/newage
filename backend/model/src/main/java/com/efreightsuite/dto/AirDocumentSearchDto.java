package com.efreightsuite.dto;

import lombok.Data;

@Data
public class AirDocumentSearchDto {

    String whichReport;
    String serviceUid;
    String shipmentUid;
    String masterUid;
    String serviceName;
    String shipperName;
    String consigneeName;
    String notifyCustomer1;
    String originName;
    String destinationName;
    String mawb;
    String hawb;
    String doNumber;
    Double chargeableWeight;

    String sortByColumn;
    String orderByType;

    Integer selectedPageNumber = 0;

    Integer recordPerPage = 5;

}
