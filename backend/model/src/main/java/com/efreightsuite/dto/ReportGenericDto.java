package com.efreightsuite.dto;

import java.io.Serializable;

import lombok.Data;

@Data
public class ReportGenericDto implements Serializable {


    /**
     *
     */
    private static final long serialVersionUID = 1L;
    String address;

}
