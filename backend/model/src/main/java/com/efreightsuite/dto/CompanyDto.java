package com.efreightsuite.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.efreightsuite.enumeration.LovStatus;
import com.efreightsuite.model.CompanyMaster;
import com.efreightsuite.model.SystemTrack;
import lombok.Getter;
import lombok.Setter;


@Getter
@Setter

public class CompanyDto implements Serializable {
    private static final long serialVersionUID = 1L;

    Long id;

    long versionLock;

    SystemTrack systemTrack;

    String companyCode;

    String companyName;

    LovStatus status;

    public CompanyDto() {
    }

    public CompanyDto(CompanyMaster entity) {
        if (entity != null) {
            this.id = entity.getId();
            this.companyName = entity.getCompanyName();
            this.companyCode = entity.getCompanyCode();
            this.status = entity.getStatus();
            this.systemTrack = entity.getSystemTrack();

        }
    }

    public static List<CompanyDto> toDTOList(List<CompanyMaster> entityList) {
        List<CompanyDto> dataList = new ArrayList<>();
        CompanyDto data = null;
        for (CompanyMaster entity : entityList) {
            data = new CompanyDto(entity);
            if (data != null) {
                dataList.add(data);
            }
        }
        return dataList;
    }
}
