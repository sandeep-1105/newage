package com.efreightsuite.dto;

import lombok.Data;

@Data
public class DefaultChargeRequestDto {

    String searchServiceCode;
    String searchChargeCode;
    String searchCurrencyCode;
    String searchUnitCode;
    String searchAmountPerUnit;
    String searchMinAmount;
    String searchOriginCode;
    String searchTransitPortCode;
    String searchDestinationCode;
    String searchTosCode;
    String searchPpcc;
    String searchWhoRouted;
    DateRange searchValidFrom;
    DateRange searchValidTo;

    String sortByColumn;
    String orderByType;
    Integer selectedPageNumber = 0;
    Integer recordPerPage = 10;


}
