package com.efreightsuite.dto;

import java.util.Date;

import com.efreightsuite.enumeration.PPCC;
import com.efreightsuite.enumeration.WhoRouted;
import com.efreightsuite.util.JsonDateDeserializer;
import com.efreightsuite.util.JsonDateSerializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Data;

@Data
public class DefaultChargeResponseDto {

    Long id;

    String serviceCode;
    String chargeCode;
    String currencyCode;
    String unitCode;
    Double amountPerUnit;
    Double minAmount;
    String originCode;
    String transitPortCode;
    String destinationCode;
    String tosCode;
    PPCC ppcc;
    WhoRouted whoRouted;
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date validFrom;
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date validTo;


    public DefaultChargeResponseDto(Long id, String serviceCode,
                                    String chargeCode, String currencyCode, String unitCode,
                                    Double amountPerUnit, Double minAmount, String originCode,
                                    String transitPortCode, String destinationCode, String tosCode,
                                    PPCC ppcc, WhoRouted whoRouted, Date validFrom, Date validTo) {
        this.id = id;
        this.serviceCode = serviceCode;
        this.chargeCode = chargeCode;
        this.currencyCode = currencyCode;
        this.unitCode = unitCode;
        this.amountPerUnit = amountPerUnit;
        this.minAmount = minAmount;
        this.originCode = originCode;
        this.transitPortCode = transitPortCode;
        this.destinationCode = destinationCode;
        this.tosCode = tosCode;
        this.ppcc = ppcc;
        this.whoRouted = whoRouted;
        this.validFrom = validFrom;
        this.validTo = validTo;
    }

    public DefaultChargeResponseDto() {
    }

}
