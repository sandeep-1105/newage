package com.efreightsuite.dto;

import java.io.Serializable;
import java.util.Set;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RecordAccessLevelProcessDTO implements Serializable {
    private static final long serialVersionUID = 1L;

    Long userId;
    Set<Long> countryList;
    Set<Long> locationList;
    Set<Long> divisionList;
    Set<Long> serviceList;
    Set<Long> salesmanList;
    Set<Long> customerList;

    public RecordAccessLevelProcessDTO() {
        // TODO Auto-generated constructor stub
    }

    public RecordAccessLevelProcessDTO(Long userId, Set<Long> countryList, Set<Long> locationList,
                                       Set<Long> divisionList, Set<Long> serviceList, Set<Long> salesmanList, Set<Long> customerList) {
        super();
        this.userId = userId;
        this.countryList = countryList;
        this.locationList = locationList;
        this.divisionList = divisionList;
        this.serviceList = serviceList;
        this.salesmanList = salesmanList;
        this.customerList = customerList;
    }


}
