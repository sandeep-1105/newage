package com.efreightsuite.dto;

import lombok.Data;

@Data
public class PartySearchDto {


    String searchPartyCode;
    String searchPartyName;
    String searchCountryName;
    String searchStatus;

    String sortByColumn;
    String orderByType;
    Integer selectedPageNumber = 0;
    Integer recordPerPage = 10;

}

