package com.efreightsuite.dto;


import lombok.Data;

@Data
public class ValueAddedServicesSearchDto {

    String searchValueAddedSevicesName;
    String searchValueAddedSevicesCode;
    String searchChargeName;


    String sortByColumn;
    String orderByType;
    Integer selectedPageNumber = 0;
    Integer recordPerPage = 10;
}
