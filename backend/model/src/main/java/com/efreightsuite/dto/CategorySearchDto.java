package com.efreightsuite.dto;

import lombok.Data;

@Data
public class CategorySearchDto {

    String searchCategoryCode;
    String searchCategoryName;
    String searchStatus;

    String sortByColumn;
    String orderByType;

    Integer selectedPageNumber = 0;

    Integer recordPerPage = 10;

}
