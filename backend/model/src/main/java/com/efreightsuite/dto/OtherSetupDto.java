package com.efreightsuite.dto;

import java.util.List;

import com.efreightsuite.model.DaybookMaster;
import com.efreightsuite.model.SequenceGenerator;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class OtherSetupDto {

    String exportTransportMode;

    String importTransportMode;

    Long userProfileId;

    List<TaxOrVatSetupDto> taxOrVatSetupDtoList;

    List<SequenceGenerator> sequenceGeneratorList;

    List<DaybookMaster> daybookMasterList;

}
