package com.efreightsuite.dto;

import com.efreightsuite.model.LocationMaster;
import com.efreightsuite.model.RecordAccessLevelLocation;
import com.efreightsuite.model.SystemTrack;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RecordAccessLevelLocationDTO {

    Long id;

    long versionLock;

    SystemTrack systemTrack;

    LocationMaster locationMaster;


    public RecordAccessLevelLocationDTO() {
        // TODO Auto-generated constructor stub
    }

    public RecordAccessLevelLocationDTO(RecordAccessLevelLocation entity) {
        this.id = entity.getId();
        this.versionLock = entity.getVersionLock();
        this.systemTrack = entity.getSystemTrack();
        this.locationMaster = entity.getLocationMaster();
    }
}
