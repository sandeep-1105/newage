package com.efreightsuite.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DummyUserDTO {

    Long id;

    String userName;

    public DummyUserDTO() {
        // TODO Auto-generated constructor stub
    }

    public DummyUserDTO(Long id, String userName) {
        this.id = id;
        this.userName = userName;
    }
}
