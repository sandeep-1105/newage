package com.efreightsuite.dto;

import com.efreightsuite.enumeration.LovStatus;
import lombok.Data;

@Data
public class ServiceMasterSearchResponseDto {

    Long id;

    String serviceCode;

    String serviceName;

    LovStatus status;

    public ServiceMasterSearchResponseDto() {

    }

    public ServiceMasterSearchResponseDto(Long id, String serviceCode, String serviceName, LovStatus status) {
        super();
        this.id = id;
        this.serviceCode = serviceCode;
        this.serviceName = serviceName;
        this.status = status;
    }

}
