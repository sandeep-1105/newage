package com.efreightsuite.dto;

import com.efreightsuite.model.Consol;
import com.efreightsuite.model.DocumentDetail;
import lombok.Data;

@Data
public class DeliveryOrderDto {

    String isDownloadPrintFor;
    Long sid;
    String serviceUid;
    String hawbNo;
    String consolUid;
    private Consol consolCloneDO;
    private DocumentDetail documentDetailForDO;

}
