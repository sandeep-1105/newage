package com.efreightsuite.dto;

import java.util.ArrayList;
import java.util.List;

import com.efreightsuite.enumeration.YesNo;
import com.efreightsuite.model.CompanyMaster;
import com.efreightsuite.model.EmployeeMaster;
import com.efreightsuite.model.SystemTrack;
import com.efreightsuite.model.UserCompany;
import com.efreightsuite.model.UserCompanyLocation;
import com.efreightsuite.util.JsonYesNoDeserializer;
import com.efreightsuite.util.JsonYesNoSerializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Data;

@Data
public class UserCompanyDTO {

    Long id;

    long versionLock;

    SystemTrack systemTrack;

    DummyUserDTO userProfile;

    EmployeeMaster employee;

    CompanyMaster companyMaster;

    @JsonSerialize(using = JsonYesNoSerializer.class)
    @JsonDeserialize(using = JsonYesNoDeserializer.class)
    YesNo yesNo;

    List<UserCompanyLocationDTO> userCompanyLocationList = new ArrayList<>();

    public UserCompanyDTO(UserCompany userCompany) {
        this.id = userCompany.getId();
        this.versionLock = userCompany.getVersionLock();
        this.systemTrack = userCompany.getSystemTrack();
        //this.userProfile=userCompany.getUserProfile();
        this.employee = userCompany.getEmployee();
        this.companyMaster = userCompany.getCompanyMaster();
        this.yesNo = userCompany.getYesNo();
        if (userCompany.getUserCompanyLocationList() != null) {
            this.setUserCompanyLocationList(new ArrayList());
            for (UserCompanyLocation loc : userCompany.getUserCompanyLocationList()) {
                this.getUserCompanyLocationList().add(new UserCompanyLocationDTO(loc));
            }
        }
        if (userCompany.getUserProfile() != null) {
            this.userProfile = new DummyUserDTO(userCompany.getUserProfile().getId(), userCompany.getUserProfile().getUserName());
        }
    }

    public UserCompanyDTO() {
    }

}
