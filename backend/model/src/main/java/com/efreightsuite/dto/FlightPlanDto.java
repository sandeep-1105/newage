package com.efreightsuite.dto;

import lombok.Data;

@Data
public class FlightPlanDto {

    Long id;
    String scheduleID;
    String carrier;
    String flight;
    String pol;
    String pod;
    String etd;
    String eta;
    String capacityFilled;
    String status;

}
