package com.efreightsuite.dto;

import lombok.Data;

@Data
public class ReportTemplateSearchDto {


    String searchReportName;
    String searchReportCategory;
    String searchStatus;

    String sortByColumn;
    String orderByType;
    Integer selectedPageNumber = 0;
    Integer recordPerPage = 10;

}

