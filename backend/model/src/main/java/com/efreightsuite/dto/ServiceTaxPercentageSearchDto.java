package com.efreightsuite.dto;

import lombok.Data;

@Data
public class ServiceTaxPercentageSearchDto {

    String searchTaxCode;
    String searchTaxName;
    String searchStatus;


    String sortByColumn;
    String orderByType;

    Integer selectedPageNumber = 0;

    Integer recordPerPage = 10;

}
