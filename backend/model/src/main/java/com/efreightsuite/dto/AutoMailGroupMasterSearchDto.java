package com.efreightsuite.dto;

import lombok.Data;

/**
 * @author Devendrachary M
 */
@Data
public class AutoMailGroupMasterSearchDto {

    String searchMessageGroupCode;
    String searchMessageGroupName;

    String sortByColumn;
    String orderByType;

    Integer selectedPageNumber = 0;
    Integer recordPerPage = 10;

    public AutoMailGroupMasterSearchDto() {
    }

    public AutoMailGroupMasterSearchDto(String searchMessageGroupName, String searchMessageGroupCode) {
        this.searchMessageGroupName = searchMessageGroupName;
        this.searchMessageGroupCode = searchMessageGroupCode;
    }
}
