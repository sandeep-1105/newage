package com.efreightsuite.dto;

import com.efreightsuite.enumeration.LovStatus;
import lombok.Data;

@Data
public class DivisionMasterSearchResponseDto {

    Long id;

    String divisionCode;

    String divisionName;

    LovStatus status;

    public DivisionMasterSearchResponseDto() {

    }

    public DivisionMasterSearchResponseDto(Long id, String divisionCode, String divisionName, LovStatus status) {
        super();
        this.id = id;
        this.divisionCode = divisionCode;
        this.divisionName = divisionName;
        this.status = status;
    }

}
