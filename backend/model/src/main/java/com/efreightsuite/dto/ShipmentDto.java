package com.efreightsuite.dto;

import lombok.Data;

@Data
public class ShipmentDto {

    Long shipmentId;
    String shipmentStatus;
    Long serviceId;
    String serviceStatus;
}
