package com.efreightsuite.dto;

import lombok.Data;

@Data
public class ServiceTypeSearchDto {

    String searchServiceTypeCode;
    String searchServiceTypeName;
    String searchServiceTransportMode;
    String searchStatus;
    String searchServiceType;

    String sortByColumn;
    String orderByType;

    Integer selectedPageNumber = 0;

    Integer recordPerPage = 5;

}


