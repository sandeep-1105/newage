package com.efreightsuite.dto;

import com.efreightsuite.enumeration.ReportDownloadFileType;
import com.efreightsuite.enumeration.ReportDownloadOption;
import com.efreightsuite.enumeration.ReportName;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class EmailReport {

    ReportDownloadOption downloadOption;

    Long resourceId;

    ReportName reportName;

    ReportDownloadFileType downloadFileType;


}
