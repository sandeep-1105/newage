package com.efreightsuite.dto;

import java.util.Date;

import com.efreightsuite.util.JsonDateDeserializer;
import com.efreightsuite.util.JsonDateSerializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class StockGenerationSearchDto {

    String carrierName;
    String portCode;
    String carrierNo;
    int awbNo;
    int checkDigit;
    String tos;
    String mawbNo;
    String stockStatus;
    String serviceCode;
    String serviceId;
    String consolUid;
    String receivedOn;
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date receivedDate;
    Date lastUsedMawbDate;
    String priorDate;
    String sortByColumn;
    long endingNo;
    String orderByType;

    Integer selectedPageNumber = 0;
    Integer recordPerPage = 10;

}
