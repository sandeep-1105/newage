package com.efreightsuite.dto;

import lombok.Data;

@Data
public class CommentSearchDto {


    String searchCommentCode;
    String searchCommentName;
    String searchStatus;

    String sortByColumn;
    String orderByType;

    Integer selectedPageNumber = 0;


    Integer recordPerPage = 10;

}

