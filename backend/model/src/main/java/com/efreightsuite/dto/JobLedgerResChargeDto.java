package com.efreightsuite.dto;

import lombok.Data;

@Data
public class JobLedgerResChargeDto {

    String chargeName, chargeCode, chargeType;


    Double amountDebit, amountCredit;

    Double provisionalRevenue, provisionalCost, provisionalNeutral;

    Double actualRevenue, actualCost, actualNeutral;

    public JobLedgerResChargeDto() {
        amountDebit = 0.0;
        amountCredit = 0.0;
        provisionalRevenue = 0.0;
        provisionalCost = 0.0;
        provisionalNeutral = 0.0;
        actualRevenue = 0.0;
        actualCost = 0.0;
        actualNeutral = 0.0;
    }
}
