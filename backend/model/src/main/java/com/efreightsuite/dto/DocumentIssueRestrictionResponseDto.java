package com.efreightsuite.dto;

import java.io.Serializable;

import com.efreightsuite.enumeration.YesNo;
import com.efreightsuite.util.JsonStringToArrDeSerializer;
import com.efreightsuite.util.JsonStringToArrSerializer;
import com.efreightsuite.util.JsonYesNoDeserializer;
import com.efreightsuite.util.JsonYesNoSerializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Data;

@Data
public class DocumentIssueRestrictionResponseDto implements Serializable {
    private static final long serialVersionUID = 1L;

    Long id;

    String companyMaster;

    String locationMaster;

    String serviceMaster;

    @JsonSerialize(using = JsonYesNoSerializer.class)
    @JsonDeserialize(using = JsonYesNoDeserializer.class)
    YesNo enableInvoiceChecking;

    /*Issue Restriction - Is Enabled*/
    @JsonSerialize(using = JsonYesNoSerializer.class)
    @JsonDeserialize(using = JsonYesNoDeserializer.class)
    YesNo enableIssueRestriction;

    /*Credit Limit Checking - Is Enabled */
    @JsonSerialize(using = JsonYesNoSerializer.class)
    @JsonDeserialize(using = JsonYesNoDeserializer.class)
    YesNo enableCreditLimitChecking;

    @JsonSerialize(using = JsonStringToArrSerializer.class)
    @JsonDeserialize(using = JsonStringToArrDeSerializer.class)
    String exceptionalTerms;

    public DocumentIssueRestrictionResponseDto() {
    }


    public DocumentIssueRestrictionResponseDto(Long id, String companyMaster, String locationMaster, String serviceMaster,
                                               YesNo enableInvoiceChecking, YesNo enableIssueRestriction, YesNo enableCreditLimitChecking, String exceptionalTerms) {


        this.id = id;
        this.companyMaster = companyMaster;
        this.locationMaster = locationMaster;
        this.serviceMaster = serviceMaster;
        this.enableInvoiceChecking = enableInvoiceChecking;
        this.enableIssueRestriction = enableIssueRestriction;
        this.enableCreditLimitChecking = enableCreditLimitChecking;
        this.exceptionalTerms = exceptionalTerms;
    }

}
