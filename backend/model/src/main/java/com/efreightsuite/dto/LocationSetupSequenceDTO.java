package com.efreightsuite.dto;

import java.io.Serializable;

import com.efreightsuite.enumeration.SequenceFormat;
import com.efreightsuite.enumeration.SequenceType;
import com.efreightsuite.enumeration.YesNo;
import com.efreightsuite.model.common.LocationSetupSequence;
import com.efreightsuite.util.JsonYesNoDeserializer;
import com.efreightsuite.util.JsonYesNoSerializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LocationSetupSequenceDTO implements Serializable {
    private static final long serialVersionUID = 1L;

    Long id;

    String sequenceName;

    SequenceType sequenceType;

    Long currentSequenceValue;

    String prefix;

    SequenceFormat format;

    @JsonSerialize(using = JsonYesNoSerializer.class)
    @JsonDeserialize(using = JsonYesNoDeserializer.class)
    YesNo isYear;

    @JsonSerialize(using = JsonYesNoSerializer.class)
    @JsonDeserialize(using = JsonYesNoDeserializer.class)
    YesNo isMonth;

    String suffix;

    String separator;

    public LocationSetupSequenceDTO() {
        // TODO Auto-generated constructor stub
    }

    public LocationSetupSequenceDTO(LocationSetupSequence locationSetupSequence) {

        this.id = locationSetupSequence.getId();
        this.prefix = locationSetupSequence.getPrefix();
        this.suffix = locationSetupSequence.getSuffix();
        this.isMonth = locationSetupSequence.getIsMonth();
        this.isYear = locationSetupSequence.getIsYear();
        this.separator = locationSetupSequence.getSeparator();
        this.currentSequenceValue = locationSetupSequence.getCurrentSequenceValue();
        this.format = locationSetupSequence.getFormat();
        this.sequenceName = locationSetupSequence.getSequenceName();
        this.sequenceType = locationSetupSequence.getSequenceType();

    }
}
