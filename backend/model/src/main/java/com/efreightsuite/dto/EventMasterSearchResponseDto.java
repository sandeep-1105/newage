package com.efreightsuite.dto;


import com.efreightsuite.enumeration.EventMasterType;
import com.efreightsuite.enumeration.LovStatus;
import lombok.Data;

@Data
public class EventMasterSearchResponseDto {


    Long id;

    String eventCode;

    String eventName;

    EventMasterType eventMasterType;

    LovStatus status;

    public EventMasterSearchResponseDto() {

    }

    public EventMasterSearchResponseDto(Long id, String eventCode, String eventName, EventMasterType eventMasterType, LovStatus status) {
        super();
        this.id = id;
        this.eventCode = eventCode;
        this.eventName = eventName;
        this.eventMasterType = eventMasterType;
        this.status = status;
    }

}
