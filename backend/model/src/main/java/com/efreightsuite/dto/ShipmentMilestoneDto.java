package com.efreightsuite.dto;

import java.util.Date;

import com.efreightsuite.util.JsonDateDeserializer;
import com.efreightsuite.util.JsonDateSerializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Data;

@Data
public class ShipmentMilestoneDto {
    Integer serialNo;
    Boolean flightStatus = false;
    String labelName;
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date date;

    String customerName;
    String serviceName;
    String pickUpPoint;
    String recievedPoint;
    String carrierNo;
    String carrierName;
    String portName;
    String portCode;
    String noticeSentTo;
    String deliveryPoint;

    Integer currentStatus;


}


