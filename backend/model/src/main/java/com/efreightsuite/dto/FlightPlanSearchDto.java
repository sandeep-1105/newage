package com.efreightsuite.dto;

import java.util.Date;

import lombok.Data;

@Data
public class FlightPlanSearchDto {

    Long carrierId;
    Long porId;
    String ids[];
    Date date;

    String scheduleId;
    String carrier;
    String flightNo;

    String pol;
    String pod;
    DateRange etd;
    DateRange eta;
    String capacityReserved;
    String status;
    String searchType;

    String sortByColumn;
    String orderByType;
    String directStopOver;

    Integer selectedPageNumber = 0;
    Integer recordPerPage = 10;
}
