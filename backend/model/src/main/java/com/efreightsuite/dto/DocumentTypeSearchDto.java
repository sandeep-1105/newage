package com.efreightsuite.dto;

import lombok.Data;

@Data
public class DocumentTypeSearchDto {

    String searchDocumentTypeCode;
    String searchDocumentTypeName;
    String searchStatus;


    String sortByColumn;
    String orderByType;

    Integer selectedPageNumber = 0;

    Integer recordPerPage = 5;

}


