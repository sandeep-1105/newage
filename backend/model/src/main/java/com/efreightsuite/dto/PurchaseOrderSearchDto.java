package com.efreightsuite.dto;

import java.io.Serializable;

import com.efreightsuite.enumeration.TransportMode;
import lombok.Data;

@Data
public class PurchaseOrderSearchDto implements Serializable {


    /**
     *
     */
    private static final long serialVersionUID = 1L;
    String buyerName;
    String supplierName;
    String poNo;
    DateRange poDate;
    String origin;
    String destination;
    TransportMode transportMode;
    String tos;
    String status;

    String sortByColumn;
    String orderByType;

    Integer selectedPageNumber = 0;
    Integer recordPerPage = 10;
}
