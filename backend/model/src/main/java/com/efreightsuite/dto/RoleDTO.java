package com.efreightsuite.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.efreightsuite.enumeration.LovStatus;
import com.efreightsuite.model.Role;
import com.efreightsuite.model.SystemTrack;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class RoleDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    Long id;

    String roleName;

    String description;

    LovStatus status;

    List<FeatureDTO> featuresList;

    List<UserDto> userList;

    SystemTrack systemTrack;

    public RoleDTO() {
    }

    public RoleDTO(Role entity) {
        if (entity != null) {
            this.id = entity.getId();
            this.roleName = entity.getRoleName();
            this.description = entity.getDescription();
            this.status = entity.getStatus();
            this.systemTrack = entity.getSystemTrack();

        }
    }

    public RoleDTO(String roleName) {
        this.roleName = roleName;
    }

    public static List<RoleDTO> toDTOList(List<Role> entityList) {
        List<RoleDTO> dataList = new ArrayList<>();
        RoleDTO data = null;
        for (Role entity : entityList) {
            data = new RoleDTO(entity);
            if (data != null) {
                dataList.add(data);
            }
        }
        return dataList;
    }

    public List<FeatureDTO> getFeaturesList() {
        return featuresList == null ? Collections.emptyList() : featuresList;
    }
}
