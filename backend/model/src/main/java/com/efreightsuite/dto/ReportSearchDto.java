package com.efreightsuite.dto;

import lombok.Data;

@Data
public class ReportSearchDto {

    String serviceCode;

    String origin;
    String destination;
    String carrier;

    String country;
    String port;

    String sortByColumn;
    String orderByType;
    String shipmentStatus;

    Integer selectedPageNumber = 0;
    Integer recordPerPage = 10;

    String fromDate;
    String toDate;

    DateRange reportDate = new DateRange();

    String reportName;
    String reportKey;
    String reportShortName;

    // report master feilds

    String searchReportCode;
    String searchIsReportEnable;
    String searchReportName;

}
