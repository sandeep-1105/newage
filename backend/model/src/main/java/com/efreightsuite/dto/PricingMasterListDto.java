package com.efreightsuite.dto;

import java.util.List;

import lombok.Data;

@Data
public class PricingMasterListDto {

    List<PricingMasterDto> pricingMasterList;

}
