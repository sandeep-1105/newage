package com.efreightsuite.dto;

import lombok.Data;

@Data
public class DocumentDetailSearchDto {
    Long documentId;
    String saasId;
    String pin;
    boolean isCustomerConform;

    String sortByColumn;
    String orderByType;

    Integer selectedPageNumber = 0;

    Integer recordPerPage = 5;

}
