package com.efreightsuite.dto;

import com.efreightsuite.enumeration.LovStatus;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;

@Data
public class AesFilerSearchDto {
    String searchAesFilerCode;
    String searchTrasmitterCode;
    String searchCountryCode;
    String searchLocationName;
    String searchStatus;
    String searchSchemaName;

    String sortByColumn;
    String orderByType;
    Integer selectedPageNumber = 0;
    Integer recordPerPage = 10;

    public AesFilerSearchDto() {
    }

    public AesFilerSearchDto(String searchSchemaName) {
        this.searchSchemaName = searchSchemaName;
    }

    public AesFilerSearchDto(String searchSchemaName, String searchCountryCode) {
        this.searchCountryCode = searchCountryCode;
        this.searchSchemaName = searchSchemaName;
    }

    public AesFilerSearchDto(String searchSchemaName, String searchCountryCode, String searchLocationName) {
        this.searchCountryCode = searchCountryCode;
        this.searchLocationName = searchLocationName;
        this.searchSchemaName = searchSchemaName;
    }

    public String getSearchAesFilerCodeNullable() {
        return StringUtils.isBlank(this.searchAesFilerCode) ? null : this.searchAesFilerCode.trim();
    }

    public String getSearchTrasmitterCodeNullable() {
        return StringUtils.isBlank(this.searchTrasmitterCode) ? null : this.searchTrasmitterCode;
    }

    public String getSearchSchemaNameNullable() {
        return StringUtils.isBlank(this.searchSchemaName) ? null : this.searchSchemaName;
    }

    public LovStatus getSearchStatusNullable() {
        return StringUtils.isBlank(this.searchStatus) ? null : LovStatus.valueOf(this.searchStatus);
    }

    public String getSearchLocationName() {
        return this.searchLocationName == null ? null : this.searchLocationName.trim();
    }

    public String getSortByColumn() {
        return this.sortByColumn == null ? null : this.sortByColumn.trim();
    }

    public String getOrderByType() {
        return this.orderByType == null ? null : this.orderByType.trim().toUpperCase();
    }
}

