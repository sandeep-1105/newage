package com.efreightsuite.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class JobNoSetupDto {

    String sequence;

    Integer noOfDigits;

    String prefix;

    Integer year;

    Integer month;

    String startingJobNo;

    String suffix;

}
