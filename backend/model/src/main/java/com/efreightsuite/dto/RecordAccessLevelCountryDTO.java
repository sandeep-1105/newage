package com.efreightsuite.dto;

import com.efreightsuite.model.CountryMaster;
import com.efreightsuite.model.RecordAccessLevelCountry;
import com.efreightsuite.model.SystemTrack;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RecordAccessLevelCountryDTO {

    Long id;

    long versionLock;

    SystemTrack systemTrack;

    CountryMaster countryMaster;

    public RecordAccessLevelCountryDTO() {
        // TODO Auto-generated constructor stub
    }

    public RecordAccessLevelCountryDTO(RecordAccessLevelCountry entity) {
        this.id = entity.getId();
        this.versionLock = entity.getVersionLock();
        this.systemTrack = entity.getSystemTrack();
        this.countryMaster = entity.getCountryMaster();
    }

}
