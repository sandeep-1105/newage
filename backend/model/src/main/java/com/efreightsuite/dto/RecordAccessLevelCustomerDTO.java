package com.efreightsuite.dto;

import com.efreightsuite.model.PartyMaster;
import com.efreightsuite.model.RecordAccessLevelCustomer;
import com.efreightsuite.model.SystemTrack;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RecordAccessLevelCustomerDTO {

    Long id;

    long versionLock;

    SystemTrack systemTrack;

    PartyMaster partyMaster;

    public RecordAccessLevelCustomerDTO() {

    }

    public RecordAccessLevelCustomerDTO(RecordAccessLevelCustomer entity) {
        this.id = entity.getId();
        this.versionLock = entity.getVersionLock();
        this.systemTrack = entity.getSystemTrack();
        this.partyMaster = entity.getPartyMaster();
    }

}
