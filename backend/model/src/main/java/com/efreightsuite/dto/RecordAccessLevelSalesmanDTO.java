package com.efreightsuite.dto;

import com.efreightsuite.model.EmployeeMaster;
import com.efreightsuite.model.RecordAccessLevelSalesman;
import com.efreightsuite.model.SystemTrack;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RecordAccessLevelSalesmanDTO {
    Long id;

    long versionLock;

    SystemTrack systemTrack;

    EmployeeMaster employeeMaster;

    public RecordAccessLevelSalesmanDTO() {
        // TODO Auto-generated constructor stub
    }

    public RecordAccessLevelSalesmanDTO(RecordAccessLevelSalesman entity) {
        this.id = entity.getId();
        this.versionLock = entity.getVersionLock();
        this.systemTrack = entity.getSystemTrack();
        this.employeeMaster = entity.getEmployeeMaster();
    }
}
