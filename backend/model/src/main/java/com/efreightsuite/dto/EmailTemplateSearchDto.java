package com.efreightsuite.dto;

import lombok.Data;

@Data
public class EmailTemplateSearchDto {

    String searchFromEmailId;
    String searchSubject;
    String searchTemplateType;

    String sortByColumn;
    String orderByType;

    Integer selectedPageNumber = 0;

    Integer recordPerPage = 5;

}
