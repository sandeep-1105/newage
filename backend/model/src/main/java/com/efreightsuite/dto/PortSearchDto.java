package com.efreightsuite.dto;

import lombok.Data;

@Data
public class PortSearchDto {


    String searchPortCode;
    String searchPortName;
    String searchTransportMode;
    String searchGroupName;
    String searchGroupCode;
    String searchCountryName;
    String searchStatus;

    String sortByColumn;
    String orderByType;

    Integer selectedPageNumber = 0;
    Integer recordPerPage = 10;

}
