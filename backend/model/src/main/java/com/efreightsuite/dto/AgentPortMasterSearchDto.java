package com.efreightsuite.dto;


import lombok.Data;

@Data
public class AgentPortMasterSearchDto {

    String searchService;
    String searchAgent;
    String searchPort;


    String sortByColumn;
    String orderByType;
    Integer selectedPageNumber = 0;
    Integer recordPerPage = 10;
}
