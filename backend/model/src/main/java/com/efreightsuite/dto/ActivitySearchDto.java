package com.efreightsuite.dto;

import lombok.Data;

@Data
public class ActivitySearchDto {

    String partyName;
    String quotationNo;
    String quotationValidity;
    String salesman;
    String service;
    String origin;
    String destination;

    String sortByColumn;
    String orderByType;

    Integer selectedPageNumber = 0;

    Integer recordPerPage = 10;

}
