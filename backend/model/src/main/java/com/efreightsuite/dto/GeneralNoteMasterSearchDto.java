package com.efreightsuite.dto;

import lombok.Data;

@Data
public class GeneralNoteMasterSearchDto {
    String searchCategory;

    String searchSubCategory;

    String searchServiceName;

    String searchLocationName;


    String sortByColumn;
    String orderByType;

    Integer selectedPageNumber = 0;

    Integer recordPerPage = 5;

}
