package com.efreightsuite.dto;

import lombok.Data;

@Data
public class EssentialChargeSearchDto {

    Long serviceId;
    Long originId;
    Long destinationId;
    Long tosId;

    String chargeName;
    String serviceName;
    String originName;
    String destinationName;
    String tosName;
    String costOrRevenue;
    String ppcc;
    String routed;
    String hazardous;


    String sortByColumn;
    String orderByType;

    Integer selectedPageNumber = 0;

    Integer recordPerPage = 10;

}
