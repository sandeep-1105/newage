package com.efreightsuite.dto;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;
import org.apache.log4j.MDC;

@Log4j2
public class BaseDto {

    @Getter
    @Setter
    String trackId;

    @Getter
    @Setter
    String responseCode;

    @Getter
    @Setter
    String responseDescription;

    @Getter
    @Setter
    Object responseObject;

    @Getter
    @Setter
    List<String> params = new ArrayList<>();

    public BaseDto() {
        trackId = (String) MDC.get("TrackId");
        log.info("BaseDto trackId:" + trackId);
    }

    public BaseDto(String responseCode, String responseDescription) {
        this();
        this.responseCode = responseCode;
        this.responseDescription = responseDescription;
    }

    public BaseDto(String responseCode, String responseDescription, Object responseObject) {
        this(responseCode, responseDescription);
        this.responseObject = responseObject;
    }

    public BaseDto(String responseCode, Object responseObject) {
        this(responseCode, null, responseObject);
    }

    public BaseDto(String responseCode) {
        this(responseCode, null);
    }
}
