package com.efreightsuite.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.efreightsuite.enumeration.UserStatus;
import com.efreightsuite.model.CompanyMaster;
import com.efreightsuite.model.EmployeeMaster;
import com.efreightsuite.model.LocationMaster;
import com.efreightsuite.model.Role;
import com.efreightsuite.model.SystemTrack;
import com.efreightsuite.model.UserCompany;
import com.efreightsuite.model.UserProfile;
import com.efreightsuite.model.WorkFlowGroup;
import com.efreightsuite.util.JsonDateDeserializer;
import com.efreightsuite.util.JsonDateSerializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Data;

@Data
public class UserProfileDto implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    Long id;

    long versionLock;

    SystemTrack systemTrack;

    String userName;

    String password;

    String resetTokenKey;

    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date resetTokenDate;

    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date passwordExpiresOn;

    String note;

    String activatedUser;

    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date activatedDate;

    String deactivatedUser;

    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date deactivatedDate;

    String sessionId;

    String reportServerBasePath;

    EmployeeMaster employee;

    List<RoleDTO> roleList;

    CompanyMaster selectedCompany;

    LocationMaster selectedUserLocation;

    UserStatus status;

    Map<String, Object> appMasterData = new HashMap<>();

    List<WorkFlowGroupDTO> workFlowList= new ArrayList<>();

    Map<String, Boolean> featureMap = new HashMap<>();

    List<UserCompanyDTO> userCompanyList = new ArrayList<>();

    //RecordAccessLevelProcessDTO recordAccessLevelProcessDTO = null;


    public UserProfileDto(UserProfile entity) {
        this.id = entity.getId();
        this.versionLock = entity.getVersionLock();
        this.systemTrack = entity.getSystemTrack();
        this.userName = entity.getUserName();
        // We can't send password back to user as it is hashed
//        this.password = entity.getPassword();
        this.resetTokenKey = entity.getResetTokenKey();
        this.resetTokenDate = entity.getResetTokenDate();
        this.employee = entity.getEmployee();
        this.passwordExpiresOn = entity.getPasswordExpiresOn();
        this.activatedUser = entity.getActivatedUser();
        this.activatedDate = entity.getActivatedDate();
        this.deactivatedDate = entity.getDeactivatedDate();
        this.deactivatedUser = entity.getDeactivatedUser();
        this.note = entity.getNote();
        this.sessionId = entity.getSessionId();
        this.reportServerBasePath = entity.getReportServerBasePath();
        this.status = entity.getStatus();
        this.selectedCompany = entity.getSelectedCompany();
        this.selectedUserLocation = entity.getSelectedUserLocation();


        this.appMasterData = entity.getAppMasterData();
        this.featureMap = entity.getFeatureMap();


        if (entity.getRoleList() != null) {
            this.setRoleList(new ArrayList<>());
            for (Role role : entity.getRoleList()) {
                this.getRoleList().add(new RoleDTO(role));
            }
        }

        if (entity.getUserCompanyList() != null) {
            this.setUserCompanyList(new ArrayList<>());
            for (UserCompany us : entity.getUserCompanyList()) {
                this.getUserCompanyList().add(new UserCompanyDTO(us));
            }
        }

        if (entity.getWorkFlowList() != null) {
            //this.setWorkFlowList(new ArrayList<>());
            for (WorkFlowGroup wfg : entity.getWorkFlowList()) {
                this.getWorkFlowList().add(new WorkFlowGroupDTO(wfg));
            }
        }
    }

    public UserProfileDto() {
    }

}
