package com.efreightsuite.dto;

import java.util.List;

import com.efreightsuite.enumeration.LovStatus;
import com.efreightsuite.model.PartyAddressMaster;
import lombok.Data;

@Data
public class PartyMasterSearchResponseDto {


    Long id;
    String partyName;
    String partyCode;
    LovStatus status;
    List<PartyAddressMaster> partyAddressList;

    public PartyMasterSearchResponseDto(Long id, String partyName, String partyCode, LovStatus status) {
        this.id = id;
        this.partyName = partyName;
        this.partyCode = partyCode;
        this.status = status;
    }

    public PartyMasterSearchResponseDto() {
    }


}


