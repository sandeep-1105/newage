package com.efreightsuite.dto;

import lombok.Data;

@Data
public class JobLedgerReqDto {

    String shipmentUid;
    String serviceUid;
    String documentUid;
    String consolUid;
    String serviceName;

}
