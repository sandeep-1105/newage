package com.efreightsuite.dto;

import com.efreightsuite.enumeration.LovStatus;
import lombok.Data;

@Data
public class PartySearchResponseDto {

    Long id;

    String partyCode;

    String partyName;

    LovStatus status;

    public PartySearchResponseDto() {

    }

    public PartySearchResponseDto(Long id, String partyCode, String partyName, LovStatus status) {
        super();
        this.id = id;
        this.partyCode = partyCode;
        this.partyName = partyName;
        this.status = status;
    }

}
