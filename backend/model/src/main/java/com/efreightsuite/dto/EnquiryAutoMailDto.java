package com.efreightsuite.dto;

import lombok.Data;

@Data
public class EnquiryAutoMailDto {

    String bkngNo;
    String bkngMsg;
    String logo;
    String poweredByImg;
    String customerName;
    String messageDescription;
    String messageGreetings;
    String statusRemark;
    String bookingNo;
    String segment;
    String hbl;
    String carrierName;
    String noOfPcs;
    String weightInKgs;
    String volumeInCbm;
    String termsOfShipment;
    String origin;
    String finalDestination;
    String shipper;
    String consignee;
    String notify;
    String commodityDescription;
    String freightPpCc;
    String pickupDate;
    String stuffingDate;
    String deliverTo;

    String portOfLoadingEtd;
    String portOfDischargeEta;
    String vesselVoyage;
    String status;

    String thankYouMsg;
    String empName;
    String empEmail;
    String telNo;
    String mode;

}
