package com.efreightsuite.dto;

import lombok.Data;

@Data
public class GroupSubGroupDto {


    String searchGroupCode;
    String searchGroupName;
    String searchSubGroupCode;
    String searchSubGroupName;
    String searchGlHeadName;


    String searchFreightPPCC;
    String searchStatus;

    String sortByColumn;
    String orderByType;


    String keyword = "";

    String glHead;

    String id = "";

    Integer selectedPageNumber = 0;

    Integer recordPerPage = 20;


}
