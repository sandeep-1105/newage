package com.efreightsuite.dto;

import java.util.List;

import com.efreightsuite.enumeration.QuotationReportType;
import com.efreightsuite.enumeration.ReportDownloadFileType;
import com.efreightsuite.enumeration.ReportDownloadOption;
import lombok.Data;

@Data
public class QuotationReportEmailRequestDto {
    List<QuotationReportType> typeList;
    ReportDownloadFileType downloadFileType;
    ReportDownloadOption downloadOption;
    Long quotationId;

    String mailSubject;

    String mailBodyPart;

    String mailTO;
    String mailCC;
    String mailBCC;

}
