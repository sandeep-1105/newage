package com.efreightsuite.dto;

import java.util.Date;

import com.efreightsuite.enumeration.AesAction;
import com.efreightsuite.util.JsonDateDeserializer;
import com.efreightsuite.util.JsonDateSerializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Data;


/**
 * @author AesFileSearchResponseDto
 */
@Data
public class AesFileSearchResponseDto {

    Long id;

    String serviceUid;

    String shipmentUid;

    String masterUid;

    String shipperName;

    String consigneeName;

    String originStateName;

    String destinationCountryName;

    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date etd;

    String status;

    AesAction aesAction;

    String aesItnNo;

    String aesStatus;

    public AesFileSearchResponseDto(Long id, String serviceUid,
                                    String shipmentUid, String masterUid, String shipperName,
                                    String consigneeName, String originStateName,
                                    String destinationCountryName, Date etd, String status,
                                    AesAction aesAction, String aesItnNo, String aesStatus) {
        this.id = id;
        this.serviceUid = serviceUid;
        this.shipmentUid = shipmentUid;
        this.masterUid = masterUid;
        this.shipperName = shipperName;
        this.consigneeName = consigneeName;
        this.originStateName = originStateName;
        this.destinationCountryName = destinationCountryName;
        this.etd = etd;
        this.status = status;
        this.aesAction = aesAction;
        this.aesItnNo = aesItnNo;
        this.aesStatus = aesStatus;
    }

    public AesFileSearchResponseDto() {

    }

}
