package com.efreightsuite.dto;


import com.efreightsuite.enumeration.ScopeFlag;
import lombok.Data;

@Data
public class DefaultMasterSearchDto {

    String searchDefaultCode;
    String searchDefaultName;
    String searchValue;
    String searchSoftware;
    String searchGroup;
    String searchSubGroup;
    ScopeFlag searchScopeFlag;


    String sortByColumn;
    String orderByType;
    Integer selectedPageNumber = 0;
    Integer recordPerPage = 10;
}
