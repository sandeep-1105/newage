package com.efreightsuite.dto;


import java.util.Date;

import com.efreightsuite.util.JsonDateDeserializer;
import com.efreightsuite.util.JsonDateSerializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Data;

@Data
public class ShipmentServiceResponseDto {

    Long id;

    String serviceUid;

    String shipmentUid;

    String consolUid;

    String mawbNo;

    String origin;

    String destination;


    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date etd;

    String hawbNo;


    public ShipmentServiceResponseDto(Long id, String serviceUid,
                                      String shipmentUid, String consolUid, String mawbNo, String origin,
                                      String destination, Date etd, String hawbNo) {
        super();
        this.id = id;
        this.serviceUid = serviceUid;
        this.shipmentUid = shipmentUid;
        this.consolUid = consolUid;
        this.mawbNo = mawbNo;
        this.origin = origin;
        this.destination = destination;
        this.etd = etd;
        this.hawbNo = hawbNo;
    }


}
