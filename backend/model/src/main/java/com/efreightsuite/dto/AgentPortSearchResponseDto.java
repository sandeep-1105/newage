package com.efreightsuite.dto;

import lombok.Data;

@Data
public class AgentPortSearchResponseDto {

    Long id;
    String service;
    String agent;
    String port;

    public AgentPortSearchResponseDto(Long id, String service, String agent, String port) {
        this.id = id;
        this.service = service;
        this.agent = agent;
        this.port = port;
    }

    public AgentPortSearchResponseDto() {
    }

}
