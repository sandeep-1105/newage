package com.efreightsuite.dto;

import lombok.Data;

@Data
public class ReasonSearchDto {

    String searchReasonCode;
    String searchReasonName;
    String searchStatus;

    String sortByColumn;
    String orderByType;

    Integer selectedPageNumber = 0;

    Integer recordPerPage = 5;

}


