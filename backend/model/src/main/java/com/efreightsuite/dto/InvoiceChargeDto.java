package com.efreightsuite.dto;

import java.io.Serializable;
import java.util.List;

import com.efreightsuite.model.InvoiceCreditNoteDetail;
import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class InvoiceChargeDto implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    Long serviceId;
    String consolUid;
    List<InvoiceCreditNoteDetail> invoiceCreditNoteDetailList;
}
