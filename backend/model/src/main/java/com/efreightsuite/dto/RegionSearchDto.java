package com.efreightsuite.dto;

import lombok.Data;

@Data
public class RegionSearchDto {

    String searchRegionCode;
    String searchRegionName;
    String searchStatus;

    String sortByColumn;
    String orderByType;

    Integer selectedPageNumber = 0;

    Integer recordPerPage = 5;

}
