package com.efreightsuite.dto;

import lombok.Data;

@Data
public class QuotationSearchDto {
    String searchServiceName;
    String searchCustomerName;
    String searchQuotationNo;
    String searchPortOrigin;
    String searchPortDestination;

    DateRange searchValidFrom;
    DateRange searchValidTo;
    String searchShipper;
    String searchSalesCordinator;
    String searchSalesman;
    DateRange searchLoggedOn;
    String searchLoggedBy;

    String transportMode;
    String status;
    String importExport;
    String searchRefNo;
    String shipmentId;


    String searchName;
    String searchValue;

    String sortByColumn;
    String orderByType;
    Integer selectedPageNumber = 0;
    Integer recordPerPage = 10;
}
