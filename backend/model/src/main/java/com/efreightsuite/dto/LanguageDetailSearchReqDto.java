package com.efreightsuite.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class LanguageDetailSearchReqDto extends AbstractSearchDto {

    String searchLanguageCode;
    String searchLanguageDesc;
    String searchLanguageName;
    String searchQcVerify;
    String searchGroupName;
    DateRange lastUpdatedOn;

    public LanguageDetailSearchReqDto() {
        this.recordPerPage = 5;
    }

    public LanguageDetailSearchReqDto(String searchLanguageName) {
        this.searchLanguageName = searchLanguageName;
    }

    public LanguageDetailSearchReqDto(String searchLanguageCode, String searchLanguageName) {
        this(searchLanguageName);
        this.searchLanguageCode = searchLanguageCode;
    }

    private LanguageDetailSearchReqDto(Builder builder) {
        setSearchLanguageCode(builder.searchLanguageCode);
        setSearchLanguageDesc(builder.searchLanguageDesc);
        setSearchLanguageName(builder.searchLanguageName);
        setSearchQcVerify(builder.searchQcVerify);
        setSearchGroupName(builder.searchGroupName);
        setLastUpdatedOn(builder.lastUpdatedOn);
    }

    public static final class Builder {
        private String searchLanguageCode;
        private String searchLanguageDesc;
        private String searchLanguageName;
        private String searchQcVerify;
        private String searchGroupName;
        private DateRange lastUpdatedOn;

        public Builder() {
        }

        public Builder(String searchLanguageCode, String searchLanguageDesc, String searchLanguageName) {
            this.searchLanguageCode = searchLanguageCode;
            this.searchLanguageDesc = searchLanguageDesc;
            this.searchLanguageName = searchLanguageName;
        }

        public Builder withSearchLanguageCode(String val) {
            searchLanguageCode = val;
            return this;
        }

        public Builder withSearchLanguageDesc(String val) {
            searchLanguageDesc = val;
            return this;
        }

        public Builder withSearchLanguageName(String val) {
            searchLanguageName = val;
            return this;
        }

        public Builder withSearchQcVerify(String val) {
            searchQcVerify = val;
            return this;
        }

        public Builder withSearchGroupName(String val) {
            searchGroupName = val;
            return this;
        }

        public Builder withLastUpdatedOn(DateRange val) {
            lastUpdatedOn = val;
            return this;
        }

        public LanguageDetailSearchReqDto build() {
            return new LanguageDetailSearchReqDto(this);
        }
    }
}
