package com.efreightsuite.dto;

import java.util.Date;

import com.efreightsuite.enumeration.EnquiryStatus;
import com.efreightsuite.util.JsonDateDeserializer;
import com.efreightsuite.util.JsonDateSerializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Data;

/**
 * @author Achyutananda P
 */

@Data
public class EnquiryLogResponseDto {

    Long id;
    String customerName;
    String customerColorCode;
    String enquiryCustomerName;
    String enquiryNo;

    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date quoteBy;

    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date receivedOn;

    String quotationNo;
    String salesCoOrdinatorName;
    String salesmanName;

    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date loggedOnDate;

    String loggedByName;
    EnquiryStatus status;

    String originName;
    String destinationName;
    String serviceName;

    public EnquiryLogResponseDto(Long id, String customerName, String customerColorCode, String enquiryCustomerName, String enquiryNo, Date quoteBy, Date receivedOn,
                                 String quotationNo, String salesCoOrdinatorName, String salesmanName, Date loggedOnDate,
                                 String loggedByName, EnquiryStatus status) {
        this.id = id;
        this.customerColorCode = customerColorCode;
        if (customerName == null || customerName.trim().length() == 0) {
            this.customerName = enquiryCustomerName;
        } else {
            this.customerName = customerName;
        }
        this.enquiryNo = enquiryNo;
        this.quoteBy = quoteBy;
        this.receivedOn = receivedOn;
        this.quotationNo = quotationNo;
        this.salesCoOrdinatorName = salesCoOrdinatorName;
        this.salesmanName = salesmanName;
        this.loggedOnDate = loggedOnDate;
        this.loggedByName = loggedByName;
        this.status = status;
    }

    public EnquiryLogResponseDto() {
    }

}
