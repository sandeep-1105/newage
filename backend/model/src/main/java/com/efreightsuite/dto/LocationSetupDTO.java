package com.efreightsuite.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.efreightsuite.enumeration.BooleanType;
import com.efreightsuite.enumeration.DateLogic;
import com.efreightsuite.enumeration.Measurement;
import com.efreightsuite.enumeration.SalutationType;
import com.efreightsuite.enumeration.YesNo;
import com.efreightsuite.model.common.CommonCityMaster;
import com.efreightsuite.model.common.CommonCountryMaster;
import com.efreightsuite.model.common.CommonCurrencyMaster;
import com.efreightsuite.model.common.CommonLanguageMaster;
import com.efreightsuite.model.common.CommonRegionMaster;
import com.efreightsuite.model.common.CommonStateMaster;
import com.efreightsuite.model.common.LocationSetup;
import com.efreightsuite.model.common.LocationSetupCharge;
import com.efreightsuite.model.common.LocationSetupDaybook;
import com.efreightsuite.model.common.LocationSetupSequence;
import com.efreightsuite.model.common.LocationSetupService;
import com.efreightsuite.util.JsonDateDeserializer;
import com.efreightsuite.util.JsonDateSerializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.apache.commons.lang3.StringUtils;

import static com.efreightsuite.constant.Constants.COMMA;
import static com.efreightsuite.constant.Constants.SEMI_COLON;

@Getter
@Setter
@ToString
public class LocationSetupDTO implements Serializable {


    /**
     *
     */
    private static final long serialVersionUID = 1L;

    Long id;

    String saasId;

    SalutationType salutation;

    String firstName;

    String lastName;

    String username;

    String password;

    String position;

    String email;

    CommonCountryMaster countryMaster;

    String countryCode;

    String phone;

    String mobile;

    String dialCode;

    String companyRegisteredName;

    String addressLine1;

    String addressLine2;

    String addressLine3;

    String area;

    CommonStateMaster stateMaster;

    String stateCode;

    CommonCityMaster cityMaster;

    String cityCode;

    String zipCode;

    CommonRegionMaster regionMaster;

    BooleanType isApproved;

    BooleanType isHeadOffice;

    byte[] logo;

    String encodedLogo;

    String dateFormat;

    String timeZone;

    CommonCurrencyMaster currencyMaster;

    Measurement measurement;

    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date financialStartDate;

    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date financialEndDate;

    CommonLanguageMaster languageMaster;

    byte[] customerUploadedFile;

    byte[] agentUploadedFile;

    byte[] agentPortUploadedFile;

    YesNo quoteNeedApproval;

    YesNo isProvisionalCost;

    String invoiceHeaderNote;

    String invoiceFooterNote;

    DateLogic importJobDate;

    DateLogic exportJobDate;

    String customerFileName;

    String agentFileName;

    List<LocationSetupDaybookDTO> daybookMasterList = new ArrayList<>();

    List<LocationSetupChargeDTO> chargeList = new ArrayList<>();

    List<LocationSetupSequenceDTO> sequenceList = new ArrayList<>();

    List<LocationSetupServiceDTO> serviceList = new ArrayList<>();

    public LocationSetupDTO() {
    }

    public LocationSetupDTO(LocationSetup locationSetup) {

        this.id = locationSetup.getId();

        this.salutation = locationSetup.getSalutation();

        this.firstName = locationSetup.getFirstName();

        this.lastName = locationSetup.getLastName();

        this.email = locationSetup.getEmail();

        if (locationSetup.getCountryMaster() != null) {
            this.countryMaster = new CommonCountryMaster(locationSetup.getCountryMaster());
        }

        if (locationSetup.getStateMaster() != null) {
            this.stateMaster = new CommonStateMaster(locationSetup.getStateMaster());
        }

        if (locationSetup.getCityMaster() != null) {
            this.cityMaster = new CommonCityMaster(locationSetup.getCityMaster());
        }

        if (locationSetup.getRegionMaster() != null) {
            this.regionMaster = new CommonRegionMaster(locationSetup.getRegionMaster());
        }

        this.phone = locationSetup.getPhone();

        this.mobile = locationSetup.getMobile();

        this.companyRegisteredName = locationSetup.getCompanyRegisteredName();

        this.addressLine1 = locationSetup.getAddressLine1();

        this.addressLine2 = locationSetup.getAddressLine2();

        this.addressLine3 = locationSetup.getAddressLine3();

        this.area = locationSetup.getArea();

        this.zipCode = locationSetup.getZipCode();

        this.isHeadOffice = locationSetup.getIsHeadOffice();

        this.logo = locationSetup.getLogo();

        this.encodedLogo = locationSetup.getEncodedLogo();

        this.dateFormat = locationSetup.getDateFormat();

        this.timeZone = locationSetup.getTimeZone();

        this.saasId = locationSetup.getSaasId();

        this.username = locationSetup.getUserName();

        this.password = locationSetup.getPassword();

        this.position = locationSetup.getPosition();

        this.currencyMaster = locationSetup.getCurrencyMaster();

        this.measurement = locationSetup.getMeasurement();

		/*this.financialStartDate = locationSetup.getFinancialStartDate();
		
		this.financialEndDate = locationSetup.getFinancialEndDate();*/

        this.languageMaster = locationSetup.getLanguageMaster();

        this.customerUploadedFile = locationSetup.getCustomerUploadedFile();

        this.agentUploadedFile = locationSetup.getAgentUploadedFile();

        this.agentPortUploadedFile = locationSetup.getAgentPortUploadedFile();

        this.quoteNeedApproval = locationSetup.getQuoteNeedApproval();

        this.isProvisionalCost = locationSetup.getIsProvisionalCost();

        this.invoiceHeaderNote = locationSetup.getInvoiceHeaderNote();

        this.invoiceFooterNote = locationSetup.getInvoiceFooterNote();

        this.importJobDate = locationSetup.getImportJobDate();

        this.exportJobDate = locationSetup.getExportJobDate();

        this.customerFileName = locationSetup.getCustomerFileName();

        this.agentFileName = locationSetup.getAgentFileName();


        if (locationSetup.getDaybookMasterList() != null && !locationSetup.getDaybookMasterList().isEmpty()) {
            for (LocationSetupDaybook daybook : locationSetup.getDaybookMasterList()) {
                LocationSetupDaybookDTO daybookDTO = new LocationSetupDaybookDTO(daybook);
                this.getDaybookMasterList().add(daybookDTO);
            }
        }

        if (locationSetup.getChargeList() != null && !locationSetup.getChargeList().isEmpty()) {
            for (LocationSetupCharge charge : locationSetup.getChargeList()) {
                LocationSetupChargeDTO chargeDTO = new LocationSetupChargeDTO(charge);
                this.getChargeList().add(chargeDTO);
            }
        }


        if (locationSetup.getSequenceList() != null && !locationSetup.getSequenceList().isEmpty()) {
            for (LocationSetupSequence sequence : locationSetup.getSequenceList()) {
                LocationSetupSequenceDTO sequenceDTO = new LocationSetupSequenceDTO(sequence);
                this.getSequenceList().add(sequenceDTO);
            }
        }

        if (locationSetup.getServiceList() != null && locationSetup.getServiceList().size() != 0) {
            for (LocationSetupService service : locationSetup.getServiceList()) {
                LocationSetupServiceDTO serviceDTO = new LocationSetupServiceDTO(service);
                this.getServiceList().add(serviceDTO);
            }
        }


    }
    public String getCommaSeperatedEmails(){
        return (this.getEmail() != null)? this.getEmail().trim().replace(SEMI_COLON,COMMA): StringUtils.EMPTY;
    }

}
