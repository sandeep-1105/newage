package com.efreightsuite.dto;

import java.util.Date;

import com.efreightsuite.util.JsonDateDeserializer;
import com.efreightsuite.util.JsonDateSerializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Data;

@Data
public class DefaultChargeSearchResponseDTo {


    Long id;

    String serviceName;
    String chargeName;
    String currencyName;
    String unitName;
    Double stdSelInMinimum;
    Double stdSelInAmount;
    Double costInMinimum;
    Double costInAmount;
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date validFrom;
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date validTo;

    Long pricingMasterId;
    String originName;
    String destinationName;
    String transitName;

    String chargeType;


    public DefaultChargeSearchResponseDTo(Long id, String serviceName, String chargeName,
                                          String currencyName, String unitName, Double stdSelInMinimum,
                                          Double stdSelInAmount, Double costInMinimum, Double costInAmount, Date validFrom, Date validTo, Long pricingMasterId,
                                          String originName, String destinationName, String transitName) {
        super();
        this.id = id;
        this.serviceName = serviceName;
        this.chargeName = chargeName;
        this.currencyName = currencyName;
        this.unitName = unitName;
        this.stdSelInMinimum = stdSelInMinimum;
        this.stdSelInAmount = stdSelInAmount;
        this.costInMinimum = costInMinimum;
        this.costInAmount = costInAmount;
        this.validFrom = validFrom;
        this.validTo = validTo;
        this.pricingMasterId = pricingMasterId;
        this.originName = originName;
        this.destinationName = destinationName;
        this.transitName = transitName;
    }


}
