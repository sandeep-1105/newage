package com.efreightsuite.dto;

import com.efreightsuite.enumeration.LovStatus;
import lombok.Data;

@Data
public class LocationMasterSearchResponseDto {

    Long id;

    String locationCode;

    String locationName;

    LovStatus status;

    public LocationMasterSearchResponseDto() {

    }

    public LocationMasterSearchResponseDto(Long id, String locationCode, String locationName, LovStatus status) {
        super();
        this.id = id;
        this.locationCode = locationCode;
        this.locationName = locationName;
        this.status = status;
    }

}
