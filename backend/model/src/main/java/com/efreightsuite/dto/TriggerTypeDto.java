package com.efreightsuite.dto;

import lombok.Data;

@Data
public class TriggerTypeDto {

    String searchTriggerTypeCode;
    String searchTriggerTypeName;
    String searchStatus;

    String sortByColumn;
    String orderByType;

    Integer selectedPageNumber = 0;
    Integer recordPerPage = 10;

}

