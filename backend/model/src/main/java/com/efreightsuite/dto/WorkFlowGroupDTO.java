package com.efreightsuite.dto;

import java.io.Serializable;

import com.efreightsuite.model.WorkFlowGroup;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class WorkFlowGroupDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    Long id;

    String workFlowName;

    public WorkFlowGroupDTO() {
    }

    public WorkFlowGroupDTO(WorkFlowGroup entity) {
        if (entity != null) {
            this.id = entity.getId();
            this.workFlowName = entity.getWorkFlowName();

        }
    }

}
