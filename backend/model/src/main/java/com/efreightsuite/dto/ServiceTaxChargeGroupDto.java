package com.efreightsuite.dto;

import lombok.Data;

@Data
public class ServiceTaxChargeGroupDto {
    String searchServiceName;

    String searchChargeName;

    String searchCategoryName;


    String sortByColumn;
    String orderByType;

    Integer selectedPageNumber = 0;

    Integer recordPerPage = 5;

}
