package com.efreightsuite.dto;

import java.io.Serializable;

import com.efreightsuite.enumeration.ChargeCalculationType;
import com.efreightsuite.enumeration.ChargeType;
import com.efreightsuite.enumeration.LovStatus;
import com.efreightsuite.model.common.LocationSetupCharge;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class LocationSetupChargeDTO implements Serializable {
    private static final long serialVersionUID = 1L;

    Long id;

    String chargeCode;

    String chargeName;

    ChargeType chargeType;

    ChargeCalculationType calculationType;

    LovStatus status;

    public LocationSetupChargeDTO() {
        // TODO Auto-generated constructor stub
    }

    public LocationSetupChargeDTO(LocationSetupCharge locationSetupCharge) {
        this.id = locationSetupCharge.getId();
        this.chargeCode = locationSetupCharge.getChargeCode();
        this.chargeName = locationSetupCharge.getChargeName();
        this.calculationType = locationSetupCharge.getCalculationType();
        this.chargeType = locationSetupCharge.getChargeType();
        this.status = locationSetupCharge.getStatus();
    }
}
