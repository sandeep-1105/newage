package com.efreightsuite.dto;

import java.io.Serializable;

import lombok.Data;

@Data
public class AirDocumentSearchResponseDto implements Serializable {
    private static final long serialVersionUID = 1L;

    Long id;
    String serviceUid;
    String shipmentUid;
    String masterUid;
    String serviceName;
    String shipperName;
    String consigneeName;
    String notifyCustomer1Name;
    String originName;
    String destinationName;
    String mawb;
    String hawb;
    String doNumber;
    Double chargeableWeight;


    public AirDocumentSearchResponseDto(Long id, String hawb, String shipmentUid, String shipperName, String consigneeName, String originName, String destinationName, Double chargeableWeight) {
        /*This constructor used for HAWB*/
        this.id = id;
        this.shipmentUid = shipmentUid;
        this.shipperName = shipperName;
        this.consigneeName = consigneeName;
        this.originName = originName;
        this.destinationName = destinationName;
        this.hawb = hawb;
        this.chargeableWeight = chargeableWeight;
    }

    public AirDocumentSearchResponseDto(String doNumber, Long id, String shipmentUid, String shipperName, String consigneeName, String originName, String destinationName, Double chargeableWeight) {
		/*This constructor used for Delivery Order*/
        this.id = id;
        this.shipmentUid = shipmentUid;
        this.shipperName = shipperName;
        this.consigneeName = consigneeName;
        this.originName = originName;
        this.destinationName = destinationName;
        this.doNumber = doNumber;
        this.chargeableWeight = chargeableWeight;
    }

    public AirDocumentSearchResponseDto(String consolUid, String mawbNo, Long id, String shipperName, String consigneeName, String originName, String destinationName, Double chargeableWeight) {
		/*This constructor used for MAWB*/
        this.id = id;
        this.masterUid = consolUid;
        this.shipperName = shipperName;
        this.consigneeName = consigneeName;
        this.originName = originName;
        this.destinationName = destinationName;
        this.mawb = mawbNo;
        this.chargeableWeight = chargeableWeight;
    }


}
