package com.efreightsuite.dto;

import lombok.Data;

@Data
public class ShipmentSearchRequestDto {

    String shipperName;
    String consigneeName;
    String shipmentUid;
    DateRange shipmentDate;
    String originName;
    String destinationName;
    String tosName;
    String createdBy;
    String employeeName;
    String customerOrderNumber;
    String shipperReferenceNumber;
    String hawbNo;
    String customerService;

    String sortByColumn;
    String orderByType;

    String shipmentStatus;
    String serviceStatus;

    String importExport;

    String searchName;
    String searchValue;

    Integer selectedPageNumber = 0;
    Integer recordPerPage = 10;
}
