package com.efreightsuite.dto;

import java.io.Serializable;

import com.efreightsuite.enumeration.EdiMessagingFor;
import com.efreightsuite.enumeration.YesNo;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class MessagingDto implements Serializable {
    private static final long serialVersionUID = 1L;

    Long ediId;

    Long userProfileId;

    YesNo isDirect;

    EdiMessagingFor messagingFor;

    boolean isAirlineEDI;

    Long selectedCurrencyId;

    Long selectedLocationId;

    Long selectedCompanyId;

    Long selectedCountryId;

    String saasId;

    String consolUid;

    boolean isResponse;

    public MessagingDto() {

    }


}
