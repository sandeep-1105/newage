package com.efreightsuite.dto;

import java.io.Serializable;
import java.util.Date;

import com.efreightsuite.enumeration.LovStatus;
import com.efreightsuite.enumeration.TransportMode;
import lombok.Data;

@Data
public class PurchaseOrderListResponseDto implements Serializable {
    private static final long serialVersionUID = 1L;

    Long id;

    String buyer;

    String supplier;

    String poNo;

    Date poDate;

    String origin;

    String destination;

    TransportMode transportMode;

    String tos;

    LovStatus status;

    public PurchaseOrderListResponseDto() {
    }


    public PurchaseOrderListResponseDto(Long id, String buyer, String supplier, String poNo, Date poDate,
                                        String origin, String destination, TransportMode transportMode, String tos, LovStatus status) {
        this.id = id;
        this.buyer = buyer;
        this.supplier = supplier;
        this.poNo = poNo;
        this.poDate = poDate;
        this.origin = origin;
        this.destination = destination;
        this.transportMode = transportMode;
        this.tos = tos;
        this.status = status;
    }

}
