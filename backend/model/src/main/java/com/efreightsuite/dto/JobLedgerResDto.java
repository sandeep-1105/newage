package com.efreightsuite.dto;

import java.util.ArrayList;
import java.util.List;

import lombok.Data;

@Data
public class JobLedgerResDto {


    String docCurrencyCode;
    String docLocale;
    String docDecimalPoint;

    Double docTotalAmountDebit;
    Double docTotalAmountCredit;
    Double docNetProfitAmount;

    Double docTotalAcutalRevenue;
    Double docTotalAcutalCost;
    Double docTotalAcutalNeutral;
    Double docNetProfitActual;

    String chargeCurrencyCode;
    String chargeLocale;
    String chargeDecimalPoint;

    Double chargeTotalAmountDebit;
    Double chargeTotalAmountCredit;
    Double chargeProfitAmount;

    Double chargeTotalProvisionalRevenue;
    Double chargeTotalProvisionalCost;
    Double chargeTotalProvisionalNeutral;
    Double chargeProfitProvisional;

    Double chargeTotalActualRevenue;
    Double chargeTotalActualCost;
    Double chargeTotalActualNeutral;
    Double chargeProfitActual;

    Double chargeGrossProfit;

    List<JobLedgerResDocumentDto> documentWiseList = new ArrayList<>();
    List<JobLedgerResChargeDto> chargeWiseList = new ArrayList<>();
}
