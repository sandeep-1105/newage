package com.efreightsuite.dto;

import com.efreightsuite.enumeration.LovStatus;
import lombok.Data;

@Data
public class CountryMasterSearchResponseDto {

    Long id;

    String countryCode;

    String countryName;

    LovStatus status;

    byte[] image;

    public CountryMasterSearchResponseDto() {

    }

    public CountryMasterSearchResponseDto(Long id, String countryCode, String countryName, byte[] image, LovStatus status) {
        super();
        this.id = id;
        this.countryCode = countryCode;
        this.countryName = countryName;
        this.image = image;
        this.status = status;
    }

}
