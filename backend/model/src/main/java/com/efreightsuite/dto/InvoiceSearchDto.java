package com.efreightsuite.dto;

import lombok.Data;

@Data
public class InvoiceSearchDto {

    Long[] serviceUidList;
    String daybookCode;
    String daybookName;
    String invoiceCreditNoteNo;
    DateRange invoiceDate;
    DateRange dueDate;
    String masterUid;
    String shipmentUid;
    String serviceId;
    Long shipmentServiceId;
    String customerAgent;
    String partyName;
    String partyAccountCode;
    String subledger;
    String currencyCode;
    String amount;
    String outstandingAmount;
    String localAmount;
    String status;

    String creditNoteType;

    String adjDaybookCode;
    String adjustmentInvoiceNo;

    String documentTypeCode;

    Boolean isServiceNull;

    String sortByColumn;
    String orderByType;


    Integer selectedPageNumber = 0;
    Integer recordPerPage = 10;
    String searchStatus;
}
