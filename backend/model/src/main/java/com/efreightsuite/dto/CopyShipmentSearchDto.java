package com.efreightsuite.dto;

import lombok.Data;

@Data
public class CopyShipmentSearchDto {
    Integer serviceId;
    Integer partyId;


    String originName;
    String destinationName;
    String shipperName;
    String consigneeName;
    String tosName;
    String shipmentUid;
    DateRange shipmentDate;
    String sortByColumn;
    String orderByType;
    Integer selectedPageNumber = 0;
    Integer recordPerPage = 10;

}
