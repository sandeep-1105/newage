package com.efreightsuite.dto;

import java.io.Serializable;
import java.util.Date;

import com.efreightsuite.util.JsonDateDeserializer;
import com.efreightsuite.util.JsonDateSerializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Data;

@Data
public class ConsolDto implements Serializable {

    Long consolId;
    String consolStatus;


    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date eta;

    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date etd;


    Long serviceId;
    Long locationId;
    Long polId;
    Long podId;
    Long carrierId;
    String consolUid;
    String mawbNo;
}
