package com.efreightsuite.dto;

import com.efreightsuite.enumeration.LovStatus;
import lombok.Data;

@Data
public class ObjectGroupMasterSearchDto {

    String searchObjectGroupName;
    String searchObjectGroupCode;
    LovStatus searchStatus;


    String sortByColumn;
    String orderByType;
    Integer selectedPageNumber = 0;
    Integer recordPerPage = 10;
}
