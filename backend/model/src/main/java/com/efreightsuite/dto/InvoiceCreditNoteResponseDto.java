package com.efreightsuite.dto;

import java.util.Date;

import com.efreightsuite.enumeration.CustomerAgent;
import com.efreightsuite.enumeration.InvoiceCreditNoteStatus;
import com.efreightsuite.util.JsonDateDeserializer;
import com.efreightsuite.util.JsonDateSerializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Data;

/**
 * @author Achyutananda P
 */

@Data
public class InvoiceCreditNoteResponseDto {

    Long id;

    String daybookCode;

    String daybookName;

    String adjDaybookCode;

    String invoiceCreditNoteNo;

    String adjustmentInvoiceNo;

    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date invoiceCreditNoteDate;

    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date dueDate;

    String masterUid;

    String shipmentUid;

    String serviceUid;

    CustomerAgent customerAgent;

    String partyName;

    String accountCode;

    String subledger;

    String currencyCode;

    String decimalPoint;

    String locale;

    Double currencyAmount;

    Double outstandingAmount;

    Double localAmount;

    String locCurrencyCode;

    String locCurDecimalPoint;

    Double roe;

    String adjDaybookName;

    String documentTypeCode;

    String locCurrLocale;

    InvoiceCreditNoteStatus invoiceCreditNoteStatus;

    public InvoiceCreditNoteResponseDto(Long id, String daybookCode, String daybookName, String adjDaybookCode, String invoiceCreditNoteNo,
                                        String adjustmentInvoiceNo, Date invoiceCreditNoteDate, Date dueDate, String masterUid, String shipmentUid, String serviceUid,
                                        CustomerAgent customerAgent, String partyName, String accountCode, String subledger, String currencyCode, String decimalPoint, String locale,
                                        Double currencyAmount, Double outstandingAmount, Double localAmount, String locCurrencyCode,
                                        String locCurDecimalPoint, Double roe, String adjDaybookName, String documentTypeCode,
                                        String locCurrLocale, InvoiceCreditNoteStatus invoiceCreditNoteStatus) {

        this.id = id;

        this.daybookCode = daybookCode;

        this.daybookName = daybookName;

        this.adjDaybookCode = adjDaybookCode;

        this.invoiceCreditNoteNo = invoiceCreditNoteNo;

        this.adjustmentInvoiceNo = adjustmentInvoiceNo;

        this.invoiceCreditNoteDate = invoiceCreditNoteDate;

        this.dueDate = dueDate;

        this.masterUid = masterUid;

        this.shipmentUid = shipmentUid;

        this.serviceUid = serviceUid;

        this.customerAgent = customerAgent;

        this.partyName = partyName;

        this.accountCode = accountCode;

        this.subledger = subledger;

        this.currencyCode = currencyCode;

        this.decimalPoint = decimalPoint;

        this.locale = locale;

        this.currencyAmount = currencyAmount;

        this.outstandingAmount = outstandingAmount;

        this.localAmount = localAmount;

        this.locCurrencyCode = locCurrencyCode;

        this.locCurDecimalPoint = locCurDecimalPoint;

        this.roe = roe;

        this.adjDaybookName = adjDaybookName;

        this.documentTypeCode = documentTypeCode;

        this.locCurrLocale = locCurrLocale;

        this.invoiceCreditNoteStatus = invoiceCreditNoteStatus;

    }

    public InvoiceCreditNoteResponseDto() {
    }

}
