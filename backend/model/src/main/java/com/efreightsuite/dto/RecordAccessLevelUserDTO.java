package com.efreightsuite.dto;

import com.efreightsuite.model.RecordAccessLevelUser;
import com.efreightsuite.model.SystemTrack;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RecordAccessLevelUserDTO {

    Long id;

    long versionLock;

    SystemTrack systemTrack;

    DummyUserDTO userProfile;

    public RecordAccessLevelUserDTO() {
        // TODO Auto-generated constructor stub
    }

    public RecordAccessLevelUserDTO(RecordAccessLevelUser entity) {
        this.id = entity.getId();
        this.versionLock = entity.getVersionLock();
        this.systemTrack = entity.getSystemTrack();
        if (entity.getUserProfile() != null) {
            this.userProfile = new DummyUserDTO(entity.getUserProfile().getId(), entity.getUserProfile().getUserName());
        }
    }

}
