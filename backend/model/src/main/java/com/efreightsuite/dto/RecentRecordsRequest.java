package com.efreightsuite.dto;

import lombok.Data;

@Data
public class RecentRecordsRequest {

    Long partyId;
    Integer selectedPageNumber = 0;
    Integer recordPerPage = 5;

}
