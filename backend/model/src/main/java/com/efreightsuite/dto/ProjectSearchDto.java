package com.efreightsuite.dto;

import lombok.Data;

@Data
public class ProjectSearchDto {

    String searchProjectCode;
    String searchProjectName;
    String searchStatus;

    String sortByColumn;
    String orderByType;

    Integer selectedPageNumber = 0;

    Integer recordPerPage = 10;

}
