package com.efreightsuite.dto;

import com.efreightsuite.enumeration.LovStatus;
import lombok.Data;

@Data
public class CommonSearchResponseDto {


    Long id;

    String code;

    String name;

    LovStatus status;


    public CommonSearchResponseDto() {

    }

    public CommonSearchResponseDto(Long id, String code, String name, LovStatus status) {
        super();
        this.id = id;
        this.code = code;
        this.name = name;
        this.status = status;
    }


}
