package com.efreightsuite.dto;

import java.util.Date;

import com.efreightsuite.enumeration.EnquiryStatus;
import lombok.Data;

@Data
public class RecentRecordsResponse {

    Long id;

    String locationName;

    Date loggedOnDate;

    String loggedByEmployee;

    String partyName;

    String enquiryNo;

    EnquiryStatus status;

    String quotationNo;

    String serviceName;

    String serviceCode;

    String originPortName;

    String originPortCode;

    String destinationPortName;

    String destinationPortCode;

    String quoteStatus;

    public RecentRecordsResponse(Long id, String locationName, Date loggedOnDate, String loggedByEmployee,
                                 String partyName, String enquiryNo, EnquiryStatus status, String quotationNo, String serviceName,
                                 String serviceCode, String originPortName, String originPortCode, String destinationPortName,
                                 String destinationPortCode) {
        this.id = id;
        this.locationName = locationName;
        this.loggedOnDate = loggedOnDate;
        this.loggedByEmployee = loggedByEmployee;
        this.partyName = partyName;
        this.enquiryNo = enquiryNo;
        this.status = status;
        this.quotationNo = quotationNo;
        this.serviceName = serviceName;
        this.serviceCode = serviceCode;
        this.originPortName = originPortName;
        this.originPortCode = originPortCode;
        this.destinationPortName = destinationPortName;
        this.destinationPortCode = destinationPortCode;
    }

    public RecentRecordsResponse() {
    }


}
