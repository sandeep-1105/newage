package com.efreightsuite.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;

import com.efreightsuite.enumeration.ReportDownloadFileType;
import com.efreightsuite.enumeration.ReportDownloadOption;
import com.efreightsuite.enumeration.ReportName;
import com.efreightsuite.enumeration.YesNo;
import com.efreightsuite.model.CarrierMaster;
import com.efreightsuite.model.CountryMaster;
import com.efreightsuite.model.PortMaster;
import com.efreightsuite.model.ServiceMaster;
import com.efreightsuite.util.JsonDateDeserializer;
import com.efreightsuite.util.JsonDateSerializer;
import com.efreightsuite.util.JsonYesNoDeserializer;
import com.efreightsuite.util.JsonYesNoSerializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Data;

@Data
public class ReportDownloadRequestDto implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    ReportName reportName;
    ReportDownloadFileType downloadFileType;
    ReportDownloadOption downloadOption;
    Long resourceId;
    String masterUid;

    Long count;
    String documentNo;

    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date dateData;

    //Title
    String parameter1;
    //Sign & Signature
    String parameter2;

    //String serviceCode;
    ServiceMaster serviceCode;
    ServiceMaster serviceName;

    //fromDate
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date fromDate;
    //toDate	
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date toDate;
    //apiFromDate
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date apiFromDate;
    //apiToDate
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date apiToDate;

    CountryMaster country;
    CarrierMaster carrier;
    PortMaster port;
    PortMaster origin;
    PortMaster destination;

    @Enumerated(EnumType.STRING)
    @JsonSerialize(using = JsonYesNoSerializer.class)
    @JsonDeserialize(using = JsonYesNoDeserializer.class)
    YesNo isSingle;

    @Enumerated(EnumType.STRING)
    YesNo showVolume;

    @Enumerated(EnumType.STRING)
    YesNo showCfs;

    @Enumerated(EnumType.STRING)
    YesNo showChargeWeight;

    @Enumerated(EnumType.STRING)
    YesNo showShipperName;

    @Enumerated(EnumType.STRING)
    YesNo showShipperAddress;

    @Enumerated(EnumType.STRING)
    YesNo showNote;

    @Enumerated(EnumType.STRING)
    YesNo showCharge;

    @Enumerated(EnumType.STRING)
    YesNo showLocalAmount;

    @Enumerated(EnumType.STRING)
    YesNo showLogo;

    @Enumerated(EnumType.STRING)
    YesNo showDimensionValue;

    Boolean isMultiple;
    Boolean single;

    String screenType;

    //used for do download for preview reports
    Long doId;


    DynamicConstraintDto dynamicConstraintDto;

    List<EmailReport> emailReportList;

    JobLedgerResDto jobLedgerResDto;

    public ReportDownloadRequestDto(){

    }

    public ReportDownloadRequestDto(ReportName reportName, ReportDownloadFileType downloadFileType, ReportDownloadOption downloadOption, Long resourceId, Boolean single) {
        this.reportName = reportName;
        this.downloadFileType = downloadFileType;
        this.downloadOption = downloadOption;
        this.resourceId = resourceId;
        this.single = single;
    }
}
