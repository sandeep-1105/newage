package com.efreightsuite.dto;


import com.efreightsuite.enumeration.ReportName;
import lombok.Data;

@Data
public class AutoAttachmentDto {


    Long parentId;
    Long childId;
    Long documentId;
    Long invoiceId;

    String documentName;
    String masterUid;
    String shipmentUid;

    //format of file
    String format;


    //format of file
    String transactionFor;
    ReportName reportName;


    byte[] documentObj;
    Boolean isAttach;

}


