package com.efreightsuite.dto;

import java.util.Date;

import com.efreightsuite.util.JsonDateDeserializer;
import com.efreightsuite.util.JsonDateSerializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Data;

/**
 * @author Achyutananda P
 */

@Data
public class provisionalCostResponseDto {

    Long id;

    String decimalPoint;

    String locale;

    String masterUid;

    String shipmentUid;

    String serviceName;

    Double totalCost;

    Double totalRevenue;

    Double netAmount;

    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date lastUpdatedDate;

    public provisionalCostResponseDto(Long id, String decimalPoint, String locale, String masterUid, String shipmentUid, String serviceName,
                                      Double totalCost, Double totalRevenue, Double netAmount, Date lastUpdatedDate) {

        this.id = id;

        this.decimalPoint = decimalPoint;

        this.locale = locale;

        this.masterUid = masterUid;

        this.shipmentUid = shipmentUid;

        this.serviceName = serviceName;

        this.totalCost = totalCost;

        this.totalRevenue = totalRevenue;

        this.netAmount = netAmount;

        this.lastUpdatedDate = lastUpdatedDate;

    }

    public provisionalCostResponseDto() {
    }

}
