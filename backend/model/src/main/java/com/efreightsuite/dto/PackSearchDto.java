package com.efreightsuite.dto;

import lombok.Data;

@Data
public class PackSearchDto {

    String searchPackCode;
    String searchPackName;
    String searchStatus;

    String sortByColumn;
    String orderByType;

    Integer selectedPageNumber = 0;

    Integer recordPerPage = 5;

}


