package com.efreightsuite.dto;

import lombok.Data;

@Data
public class UserProfileSearchDto {

    String searchUserName;
    String searchStatus;

    String sortByColumn;
    String orderByType;

    Integer selectedPageNumber = 0;

    Integer recordPerPage = 10;

}
