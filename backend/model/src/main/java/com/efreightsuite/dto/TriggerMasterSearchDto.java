package com.efreightsuite.dto;

import com.efreightsuite.enumeration.YesNo;
import lombok.Data;

@Data
public class TriggerMasterSearchDto {

    String searchTriggerCode;
    String searchTriggerName;
    String searchTriggerType;
    String searchMailSubject;
    YesNo searchUseForFinance;
    String searchStatus;

    String sortByColumn;
    String orderByType;

    Integer selectedPageNumber = 0;
    Integer recordPerPage = 10;

}

