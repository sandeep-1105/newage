package com.efreightsuite.dto;

import java.util.ArrayList;
import java.util.List;

import lombok.Data;

@Data
public class AesFileSubmissionResponseDto {

    String serviceId;
    String aesMsg;

    Long aesId;

    boolean error;

    List<FieldErrorDto> fieldErrorList = new ArrayList<>();
}
