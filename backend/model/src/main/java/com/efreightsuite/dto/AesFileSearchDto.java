package com.efreightsuite.dto;

import lombok.Data;

@Data
public class AesFileSearchDto {

    String serviceUid;
    String shipmentUid;
    String masterUid;

    String shipperName;
    String consigneeName;

    String originStateName;
    String destinationCountryName;


    DateRange etd;

    String status;
    String aesItnNo;
    String aesStatus;
    String aesAction;


    String sortByColumn;
    String orderByType;

    Integer selectedPageNumber = 0;

    Integer recordPerPage = 10;

}
