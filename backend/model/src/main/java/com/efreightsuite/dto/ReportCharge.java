package com.efreightsuite.dto;

import java.util.List;

import com.efreightsuite.model.QuotationCharge;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ReportCharge {

    String keyName;

    List<QuotationCharge> chargeList;

    List<ReportCurrency> currencyList;

}
