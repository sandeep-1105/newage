package com.efreightsuite.dto;

import lombok.Data;

@Data
public class ChargeSearchDto {


    String searchChargeCode;
    String searchChargeName;
    String searchChargeType;
    String searchCalculationType;
    String searchStatus;

    String sortByColumn;
    String orderByType;

    Integer selectedPageNumber = 0;


    Integer recordPerPage = 10;

}

