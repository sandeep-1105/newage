package com.efreightsuite.dto;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CountryDynamicFieldDto implements Serializable {

    String fieldName;

    /*Issue Restriction - Is Enabled*/
    /*@JsonSerialize(using = JsonYesNoSerializer.class)
	@JsonDeserialize(using = JsonYesNoDeserializer.class)
	*/ String showField;

    public CountryDynamicFieldDto() {
    }


    public CountryDynamicFieldDto(String fieldName, String showField) {
        this.fieldName = fieldName;
        this.showField = showField;

    }


}
