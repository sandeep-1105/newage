package com.efreightsuite.dto;

import com.efreightsuite.enumeration.SearchType;
import lombok.Data;

@Data
public class ImportExportSearchDto {

    String location;
    String shipmentUid;
    String service;
    String serviceUid;
    String hawbNo;
    DateRange nominationDate;
    String shipperName;
    String consigneeName, exportRef;

    String origin;
    String destination;
    DateRange etd;
    DateRange eta;

    SearchType searchType;


    String sortByColumn;
    String orderByType;
    String shipmentStatus;

    Integer selectedPageNumber = 0;
    Integer recordPerPage = 10;
}
