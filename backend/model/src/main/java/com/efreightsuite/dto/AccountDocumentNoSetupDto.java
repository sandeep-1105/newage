package com.efreightsuite.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AccountDocumentNoSetupDto {

    String daybookName;

    Integer noOfDigits;

    String prefix;

    Integer year;

    Integer month;

    String startingJobNo;

    String suffix;


}
