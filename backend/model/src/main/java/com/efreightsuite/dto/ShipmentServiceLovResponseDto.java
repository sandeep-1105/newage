package com.efreightsuite.dto;


import lombok.Data;

@Data
public class ShipmentServiceLovResponseDto {

    Long id;
    String shipmentUid;
    Long serviceId;
    String serviceUid;
    String originCode;
    String destinationCode;
    String serviceName;


    public ShipmentServiceLovResponseDto(Long id, String shipmentUid, Long serviceId, String serviceUid, String originCode, String destinationCode,
                                         String serviceName) {
        this.id = id;
        this.serviceId = serviceId;
        this.shipmentUid = shipmentUid;
        this.serviceUid = serviceUid;

        this.originCode = originCode;
        this.destinationCode = destinationCode;
        this.serviceName = serviceName;
    }

    public ShipmentServiceLovResponseDto() {
    }
}
