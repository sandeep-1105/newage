package com.efreightsuite.dto;

import lombok.Data;

@Data
public class IataRateMasterSearchDto {

    String searchCarrier;
    String searchPol;
    DateRange searchValidFromDate;
    DateRange searchValidToDate;


    String sortByColumn;
    String orderByType;
    Integer selectedPageNumber = 0;
    Integer recordPerPage = 10;


}
