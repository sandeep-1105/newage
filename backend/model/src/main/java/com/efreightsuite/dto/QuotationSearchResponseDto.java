package com.efreightsuite.dto;

import java.io.Serializable;
import java.util.Date;

import com.efreightsuite.enumeration.Approved;
import com.efreightsuite.enumeration.QuoteType;
import com.efreightsuite.enumeration.WhoRouted;
import com.efreightsuite.util.JsonDateDeserializer;
import com.efreightsuite.util.JsonDateSerializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Data;

@Data
public class QuotationSearchResponseDto implements Serializable {

    private static final long serialVersionUID = 1L;


    Long id;

    String serviceCode;

    String serviceName;

    String customerName;

    String customerColorCode;

    String quotationNo;

    String originCode;

    String originName;

    String destinationCode;

    String destinationName;

    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date validFrom;

    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date expiresOn;

    String referenceNumber;

    String shipmentUid;

    String shipperName;

    String shipperColorCode;

    String salesCoOrdinator;

    String salesman;

    WhoRouted whoRouted;

    String agentName;

    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date createdOn;

    String createdBy;

    Approved approved;

    QuoteType quoteType;

    public QuotationSearchResponseDto() {
    }


    public QuotationSearchResponseDto(Long id, String serviceCode, String serviceName, String customerName, String customerColorCode,
                                      String quotationNo, String originCode, String originName, String destinationCode, String destinationName, Date validFrom, Date expiresOn,
                                      String referenceNumber, String shipmentUid, String shipperName, String shipperColorCode,
                                      String salesCoOrdinator, String salesman, WhoRouted whoRouted, String agentName, Date createdOn,
                                      String createdBy, Approved approved) {
        super();
        this.id = id;
        this.serviceCode = serviceCode;
        this.serviceName = serviceName;
        this.customerName = customerName;
        this.customerColorCode = customerColorCode;
        this.quotationNo = quotationNo;
        this.originCode = originCode;
        this.originName = originName;
        this.destinationCode = destinationCode;
        this.destinationName = destinationName;
        this.validFrom = validFrom;
        this.expiresOn = expiresOn;
        this.referenceNumber = referenceNumber;
        this.shipmentUid = shipmentUid;
        this.shipperName = shipperName;
        this.shipperColorCode = shipperColorCode;
        this.salesCoOrdinator = salesCoOrdinator;
        this.salesman = salesman;
        this.whoRouted = whoRouted;
        this.agentName = agentName;
        this.createdOn = createdOn;
        this.createdBy = createdBy;
        this.approved = approved;
    }
}
