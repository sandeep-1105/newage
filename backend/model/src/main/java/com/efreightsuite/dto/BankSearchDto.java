package com.efreightsuite.dto;

import com.efreightsuite.enumeration.LovStatus;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;

@Data
public class BankSearchDto {

    String searchBankCode;
    String searchBankName;
    String searchStatus;

    String searchSmartBusinessBankCode;
    String sortByColumn;
    String orderByType;

    Integer selectedPageNumber = 0;

    Integer recordPerPage = 5;

    public BankSearchDto() {
    }

    public BankSearchDto(String searchBankName) {
        this.searchBankName = searchBankName;
    }

    public BankSearchDto(String searchBankName, String searchBankCode, String searchStatus) {
        this.searchBankCode = searchBankCode;
        this.searchBankName = searchBankName;
        this.searchStatus = searchStatus;
    }

    public LovStatus getSearchStatusNullable() {
        return StringUtils.isBlank(this.searchStatus)
                ? null
                : LovStatus.valueOf(StringUtils.capitalize(this.searchStatus.trim().toLowerCase()));
    }

    public String getSearchBankNameNullable() {
        return StringUtils.isBlank(this.searchBankName)
                ? null
                : this.searchBankName.trim();
    }

    public String getSearchBankCodeNullable() {
        return StringUtils.isBlank(this.searchBankCode)
                ? null
                : this.searchBankCode.trim();
    }

    public String getSearchSmartBusinessBankCodeNullable() {
        return StringUtils.isBlank(this.searchSmartBusinessBankCode)
                ? null
                : this.searchSmartBusinessBankCode.trim();
    }
}
