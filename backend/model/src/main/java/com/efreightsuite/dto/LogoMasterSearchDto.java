package com.efreightsuite.dto;

import lombok.Data;

@Data
public class LogoMasterSearchDto {

    String searchLocationName;
    String searchStatus;

    String sortByColumn;
    String orderByType;

    Integer selectedPageNumber = 0;
    Integer recordPerPage = 5;


}
