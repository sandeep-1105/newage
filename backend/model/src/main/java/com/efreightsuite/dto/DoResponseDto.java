package com.efreightsuite.dto;

import java.util.Date;

import com.efreightsuite.util.JsonDateDeserializer;
import com.efreightsuite.util.JsonDateSerializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Data;

@Data
public class DoResponseDto {

    Long id;
    Long totalPieces;
    Double totalGrossWt;
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date eta;
    String flightNo;
    String referenceNo;
    String igmNo;
    String mawbNo;
    String hawbNo;
    String descriptionOfGoods;
    String remark;
    String doNumber;
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date doDate;
    String carrierDo;
    String companyName;
    byte[] logo;
    String address;
    String companyAddress;

}
