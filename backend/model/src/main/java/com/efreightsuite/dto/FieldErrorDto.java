package com.efreightsuite.dto;

import lombok.Data;

@Data
public class FieldErrorDto {

    String fieldId;

    String errorMsg;

    int errIdx;

    public FieldErrorDto() {
    }

    public FieldErrorDto(String fieldId, String errorMsg, int errIdx) {
        this.fieldId = fieldId;
        this.errorMsg = errorMsg;
        this.errIdx = errIdx;
    }
}
