package com.efreightsuite.dto;

import lombok.Data;

@Data
public class CommoditySearchDto {

    String searchCommodityCode;
    String searchCommodityName;
    String searchStatus;

    String sortByColumn;
    String orderByType;

    Integer selectedPageNumber = 0;

    Integer recordPerPage = 10;

}
