package com.efreightsuite.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.efreightsuite.enumeration.YesNo;
import com.efreightsuite.model.Feature;
import com.efreightsuite.util.JsonYesNoDeserializer;
import com.efreightsuite.util.JsonYesNoSerializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Data;

@Data
public class FeatureDTO implements Serializable {
    private static final long serialVersionUID = 1L;

    Long id;

    String featureName, featureValue;

    @JsonSerialize(using = JsonYesNoSerializer.class)
    @JsonDeserialize(using = JsonYesNoDeserializer.class)
    YesNo list;

    String valList;

    @JsonSerialize(using = JsonYesNoSerializer.class)
    @JsonDeserialize(using = JsonYesNoDeserializer.class)
    YesNo create;

    String valCreate;

    @JsonSerialize(using = JsonYesNoSerializer.class)
    @JsonDeserialize(using = JsonYesNoDeserializer.class)
    YesNo modify;

    String valModify;

    @JsonSerialize(using = JsonYesNoSerializer.class)
    @JsonDeserialize(using = JsonYesNoDeserializer.class)
    YesNo view;

    String valView;

    @JsonSerialize(using = JsonYesNoSerializer.class)
    @JsonDeserialize(using = JsonYesNoDeserializer.class)
    YesNo delete;

    String valDelete;

    @JsonSerialize(using = JsonYesNoSerializer.class)
    @JsonDeserialize(using = JsonYesNoDeserializer.class)
    YesNo download;

    String valDownload;


    Long parentId;

    public FeatureDTO() {
    }

    public FeatureDTO(Feature feature) {
        this.id = feature.getId();
        this.featureName = feature.getFeatureName();
        this.featureValue = feature.getFeatureValue();


        this.list = feature.getList();
        this.valList = feature.getValList();

        this.create = feature.getCreate();
        this.valCreate = feature.getValCreate();

        this.modify = feature.getModify();
        this.valModify = feature.getValModify();

        this.view = feature.getView();
        this.valView = feature.getValView();

        this.delete = feature.getDelete();
        this.valDelete = feature.getValDelete();

        this.download = feature.getDownload();
        this.valDownload = feature.getValDownload();
    }
    
/*
    public static List<FeatureDTO> entityListToDataList(List<Feature> entityList, List<Long> availFeature) {
		List<FeatureDTO> dataList = new ArrayList<FeatureDTO>();

		for(Feature entity :entityList){
			dataList.add(entityToData(entity, availFeature));
		}
		return dataList;
	}*/
	/*public static FeatureDTO entityToData(Feature entity, List<Long> availFeature) {
		FeatureDTO data= new FeatureDTO();
		data.setId( entity.getId());
		data.setFeatureName(entity.getFeatureName());
		
		if(availFeature != null && availFeature.size() >0 && availFeature.contains(entity.getId())) {
			data.setChk(true);
		}else {
			data.setChk( false);
		}
		if(entity.getParent()!=null){
			data.setParentId(entity.getParent().getId()); 
		}
		if(entity.getFeatures()!= null && entity.getFeatures().size()>0)
			data.setChildren(entityListToDataList(entity.getFeatures(), availFeature));
		else 
			data.setChildren(null);
		
		data.setList(entity.getList());
		data.setValList(entity.getValList());
    	
		data.setCreate(entity.getCreate());
    	data.setValCreate(entity.getValCreate());
    	
    	data.setModify(entity.getModify());
    	data.setValModify(entity.getValModify());
    	
    	data.setView(entity.getView());
    	data.setValView(entity.getValView());
    	
    	data.setDelete(entity.getDelete());
    	data.setValDelete(entity.getValDelete());
    	
    	data.setDownload(entity.getDownload());
    	data.setValDownload(entity.getValDownload());
    	
    	
		return data;
	}*/

    public static List<FeatureDTO> entityListToDataList(List<Feature> entityList) {
        List<FeatureDTO> dataList = new ArrayList<>();

        for (Feature entity : entityList) {
            dataList.add(entityToData(entity));
        }
        return dataList;
    }

    public static FeatureDTO entityToData(Feature entity) {
        FeatureDTO data = new FeatureDTO();
        data.setId(entity.getId());
        data.setFeatureName(entity.getFeatureName());
        data.setFeatureValue(entity.getFeatureValue());
        if (entity.getParent() != null) {
            data.setParentId(entity.getParent().getId());
        }

        data.setList(entity.getList());
        data.setValList(entity.getValList());

        data.setCreate(entity.getCreate());
        data.setValCreate(entity.getValCreate());

        data.setModify(entity.getModify());
        data.setValModify(entity.getValModify());

        data.setView(entity.getView());
        data.setValView(entity.getValView());

        data.setDelete(entity.getDelete());
        data.setValDelete(entity.getValDelete());

        data.setDownload(entity.getDownload());
        data.setValDownload(entity.getValDownload());


        return data;
    }
}
