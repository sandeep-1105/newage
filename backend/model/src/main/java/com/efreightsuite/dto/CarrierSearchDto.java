package com.efreightsuite.dto;

import lombok.Data;

@Data
public class CarrierSearchDto {

    String searchCarrierCode;
    String searchCarrierName;
    String searchTransportMode;
    String searchCarrierNo;
    String searchScacCode;
    String searchStatus;
    String searchIataCode;

    String sortByColumn;
    String orderByType;

    Integer selectedPageNumber = 0;

    Integer recordPerPage = 10;

}
