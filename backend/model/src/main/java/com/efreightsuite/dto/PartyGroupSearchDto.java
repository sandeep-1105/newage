package com.efreightsuite.dto;

import lombok.Data;

@Data
public class PartyGroupSearchDto {

    String searchPartyGroupName;
    String searchPartyGroupCode;
    String searchSalesLeadLocation;
    String searchStatus;

    String sortByColumn;
    String orderByType;
    Integer selectedPageNumber = 0;
    Integer recordPerPage = 5;
}
