package com.efreightsuite.dto;

import com.efreightsuite.enumeration.LovStatus;
import lombok.Data;

@Data
public class BuyerConsolidationSearchDto {

    String keyword;
    String bcCode;
    String bcName;
    String ref1;
    String ref2;
    String ref3;
    String ref4;
    String ref5;

    LovStatus status;

    String sortByColumn;
    String orderByType;

    Integer selectedPageNumber = 0;

    Integer recordPerPage = 10;

}
