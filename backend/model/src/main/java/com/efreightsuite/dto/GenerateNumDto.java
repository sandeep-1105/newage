package com.efreightsuite.dto;

import com.efreightsuite.enumeration.ReportName;
import lombok.Data;

@Data
public class GenerateNumDto {

    Long id;

    ReportName reportName;

    Long consolId;


    Boolean isMultiple;

}
