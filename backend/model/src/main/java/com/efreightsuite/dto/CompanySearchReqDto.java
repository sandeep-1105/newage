package com.efreightsuite.dto;

import lombok.Data;

@Data
public class CompanySearchReqDto {

    String searchCompanyCode;
    String searchCompanyName;

/*	String orderCompanyCode;
    String orderCompanyName;*/

    String sortByColumn;
    String orderByType;


    Integer selectedPageNumber = 0;

    Integer recordPerPage = 10;
    String searchStatus;

}
