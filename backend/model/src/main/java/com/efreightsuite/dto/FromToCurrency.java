package com.efreightsuite.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FromToCurrency {

    Long cFrom;
    Long cTo;

    Double cBuy;
    Double cSell;
}
