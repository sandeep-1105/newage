package com.efreightsuite.dto;

import lombok.Data;

@Data
public class WinConfigurationRequestDto {

    //unique WIN
    int awbID;

    int transactionID;

    int mawbId;

    int agentId;

    String awbNumber;

    String winStatus;

    String errorCode;

    String errorMessage;

    String consolUid;


    String sortByColumn;
    String orderByType;
    Integer selectedPageNumber = 0;
    Integer recordPerPage = 10;

}
