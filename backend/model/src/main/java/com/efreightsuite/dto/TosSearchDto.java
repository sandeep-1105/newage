package com.efreightsuite.dto;

import lombok.Data;

@Data
public class TosSearchDto {

    String searchTosCode;
    String searchTosName;
    String searchFreightPPCC;
    String searchStatus;

    String sortByColumn;
    String orderByType;

    Integer selectedPageNumber = 0;

    Integer recordPerPage = 10;

}
