package com.efreightsuite.dto;

import lombok.Data;

@Data
public class ProvisionalSearchDto {


    String masterUid;
    String shipmentUid;
    String serviceName;
    String totalCost;
    String totalRevenue;
    String netAmount;
    DateRange lastUpdatedOn;

    String sortByColumn;
    String orderByType;

    Integer selectedPageNumber = 0;

    Integer recordPerPage = 10;

}
