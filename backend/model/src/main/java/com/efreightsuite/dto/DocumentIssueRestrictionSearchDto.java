package com.efreightsuite.dto;

import java.io.Serializable;

import lombok.Data;

@Data
public class DocumentIssueRestrictionSearchDto implements Serializable {


    /**
     *
     */
    private static final long serialVersionUID = 1L;

    String locationMaster;

    String serviceMaster;

    String sortByColumn;

    String orderByType;

    Integer selectedPageNumber = 0;
    Integer recordPerPage = 10;
}
