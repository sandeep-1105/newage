package com.efreightsuite.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.efreightsuite.enumeration.LovStatus;
import com.efreightsuite.model.RecordAccessLevel;
import com.efreightsuite.model.RecordAccessLevelCountry;
import com.efreightsuite.model.RecordAccessLevelCustomer;
import com.efreightsuite.model.RecordAccessLevelDivision;
import com.efreightsuite.model.RecordAccessLevelLocation;
import com.efreightsuite.model.RecordAccessLevelSalesman;
import com.efreightsuite.model.RecordAccessLevelService;
import com.efreightsuite.model.RecordAccessLevelUser;
import com.efreightsuite.model.SystemTrack;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RecordAccessLevelDTO implements Serializable {
    private static final long serialVersionUID = 1L;

    Long id;

    long versionLock;

    SystemTrack systemTrack;

    String recordAccessLevelName;

    LovStatus status;

    String description;

    List<RecordAccessLevelUserDTO> userList;

    List<RecordAccessLevelCountryDTO> countryList;

    List<RecordAccessLevelLocationDTO> locationList;

    List<RecordAccessLevelDivisionDTO> divisionList;

    List<RecordAccessLevelServiceDTO> serviceList;

    List<RecordAccessLevelSalesmanDTO> salesmanList;

    List<RecordAccessLevelCustomerDTO> customerList;

    public RecordAccessLevelDTO() {
        // TODO Auto-generated constructor stub
    }

    public RecordAccessLevelDTO(RecordAccessLevel entity) {

        this.id = entity.getId();

        this.versionLock = entity.getVersionLock();

        this.systemTrack = entity.getSystemTrack();

        this.recordAccessLevelName = entity.getRecordAccessLevelName();

        this.status = entity.getStatus();

        this.description = entity.getDescription();

        if (entity.getCountryList() != null) {
            this.setCountryList(new ArrayList<>());
            for (RecordAccessLevelCountry e : entity.getCountryList()) {
                this.getCountryList().add(new RecordAccessLevelCountryDTO(e));
            }
        }

        if (entity.getLocationList() != null) {
            this.setLocationList(new ArrayList<>());
            for (RecordAccessLevelLocation e : entity.getLocationList()) {
                this.getLocationList().add(new RecordAccessLevelLocationDTO(e));
            }
        }

        if (entity.getDivisionList() != null) {
            this.setDivisionList(new ArrayList<>());
            for (RecordAccessLevelDivision e : entity.getDivisionList()) {
                this.getDivisionList().add(new RecordAccessLevelDivisionDTO(e));
            }
        }

        if (entity.getServiceList() != null) {
            this.setServiceList(new ArrayList<>());
            for (RecordAccessLevelService e : entity.getServiceList()) {
                this.getServiceList().add(new RecordAccessLevelServiceDTO(e));
            }
        }

        if (entity.getSalesmanList() != null) {
            this.setSalesmanList(new ArrayList<>());
            for (RecordAccessLevelSalesman e : entity.getSalesmanList()) {
                this.getSalesmanList().add(new RecordAccessLevelSalesmanDTO(e));
            }
        }

        if (entity.getCustomerList() != null) {
            this.setCustomerList(new ArrayList<>());
            for (RecordAccessLevelCustomer e : entity.getCustomerList()) {
                this.getCustomerList().add(new RecordAccessLevelCustomerDTO(e));
            }
        }

        if (entity.getUserList() != null) {
            this.setUserList(new ArrayList<>());
            for (RecordAccessLevelUser e : entity.getUserList()) {
                this.getUserList().add(new RecordAccessLevelUserDTO(e));
            }
        }

    }

}
