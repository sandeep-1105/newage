package com.efreightsuite.dto;

import lombok.Data;

@Data
public class CurrencySearchDto {


    String searchCurrencyCode;
    String searchCurrencyName;
    String searchCountryCode;
    String searchCountryName;
    String searchPrefix;
    String searchSuffix;
    String searchStatus;

    String sortByColumn;
    String orderByType;

    Integer selectedPageNumber = 0;

    Integer recordPerPage = 10;

}

