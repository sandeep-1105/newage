package com.efreightsuite.dto;

import lombok.Data;

/**
 * @author Devendrachary M
 */
@Data
public class AutoMailMasterSearchDto {

    String searchMessageName;
    String searchMessageCode;
    String searchMessageSendType;
    String searchStatus;
    String searchAutoMailGroupMaster;
    String searchMessageGroupName;
    String sortByColumn;
    String orderByType;

    Integer selectedPageNumber = 0;

    Integer recordPerPage = 10;

}
