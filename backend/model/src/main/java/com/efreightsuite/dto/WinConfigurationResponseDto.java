package com.efreightsuite.dto;

import lombok.Data;

@Data
public class WinConfigurationResponseDto {


    //unique WIN

    Long id;
    String winErrorCode;
    String winErrorMessage;
    Long winAwbID;
    String winStatus;
    String consolUid;

    public WinConfigurationResponseDto(Long id, String winErrorCode,
                                       String winErrorMessage, Long winAwbID,
                                       String winStatus, String consolUid) {
        super();
        this.id = id;
        this.winErrorCode = winErrorCode;
        this.winErrorMessage = winErrorMessage;
        this.winAwbID = winAwbID;
        this.winStatus = winStatus;
        this.consolUid = consolUid;
    }


}
