package com.efreightsuite.dto;

import java.io.Serializable;

import com.efreightsuite.enumeration.SequenceFormat;
import com.efreightsuite.enumeration.YesNo;
import com.efreightsuite.model.common.CommonDocumentTypeMaster;
import com.efreightsuite.model.common.LocationSetupDaybook;
import com.efreightsuite.util.JsonYesNoDeserializer;
import com.efreightsuite.util.JsonYesNoSerializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class LocationSetupDaybookDTO implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    Long id;

    String daybookCode;

    String daybookName;

    SequenceFormat format;

    String prefix;

    @JsonSerialize(using = JsonYesNoSerializer.class)
    @JsonDeserialize(using = JsonYesNoDeserializer.class)
    YesNo isYear;

    @JsonSerialize(using = JsonYesNoSerializer.class)
    @JsonDeserialize(using = JsonYesNoDeserializer.class)
    YesNo isMonth;

    Long currentSequenceValue;

    String suffix;

    String separator;

    CommonDocumentTypeMaster documentTypeMaster;

    public LocationSetupDaybookDTO() {
        // TODO Auto-generated constructor stub
    }

    public LocationSetupDaybookDTO(LocationSetupDaybook locationSetupDaybook) {

        this.id = locationSetupDaybook.getId();
        this.daybookCode = locationSetupDaybook.getDaybookCode();
        this.daybookName = locationSetupDaybook.getDaybookName();
        this.currentSequenceValue = locationSetupDaybook.getCurrentSequenceValue();
        this.isMonth = locationSetupDaybook.getIsMonth();
        this.isYear = locationSetupDaybook.getIsYear();
        this.prefix = locationSetupDaybook.getPrefix();
        this.suffix = locationSetupDaybook.getSuffix();
        this.separator = locationSetupDaybook.getSeparator();
        this.format = locationSetupDaybook.getFormat();
        this.documentTypeMaster = locationSetupDaybook.getDocumentTypeMaster();

    }
}
