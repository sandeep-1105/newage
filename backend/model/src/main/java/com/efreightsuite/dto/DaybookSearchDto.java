package com.efreightsuite.dto;

import lombok.Data;

@Data
public class DaybookSearchDto {

    String searchDaybookCode;
    String searchDaybookName;
    String searchLocationName;
    String searcDocumentTypeName;

    String searchCashAccountCode;
    String searchCashAccountName;

    String searchBankAccountCode;
    String searchBankAccountName;
    DateRange searchBlockDate;
    String searchStatus;

    String sortByColumn;
    String orderByType;

    Integer selectedPageNumber = 0;

    Integer recordPerPage = 5;

}


