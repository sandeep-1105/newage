package com.efreightsuite.dto;

import lombok.Data;

@Data
public class PartyTypeSearchDto {
    String searchPartyTypeCode;
    String searchPartyTypeName;
    String searchStatus;

    String sortByColumn;
    String orderByType;

    Integer selectedPageNumber = 0;

    Integer recordPerPage = 5;


}


