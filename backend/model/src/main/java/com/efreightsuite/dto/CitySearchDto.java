package com.efreightsuite.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class CitySearchDto extends AbstractSearchDto {

    String searchCityName;
    String searchCityCode;
    String searchStateName;
    String searchCountryName;
    String searchStatus;

    public CitySearchDto() {

    }

    public CitySearchDto(String searchCityName) {

        this.searchCityName = searchCityName;
    }

    public CitySearchDto(String searchCityName, String searchCityCode) {
        this(searchCityName);
        this.searchCityCode = searchCityCode;
    }

    public CitySearchDto(String searchCityName, String searchCityCode, String searchStateName) {
        this(searchCityName, searchCityCode);
        this.searchStateName = searchStateName;
    }

    public CitySearchDto(String searchCityName, String searchCityCode, String searchStateName, String searchCountryName) {
        this(searchCityName, searchCityCode, searchStateName);
        this.searchCountryName = searchCountryName;
    }

    public CitySearchDto(String searchCityName, String searchCityCode, String searchStateName, String searchCountryName, String status) {
        this(searchCityName, searchCityCode, searchStateName, searchCountryName);
        this.searchStatus = status;
    }
}
