package com.efreightsuite.dto;

import com.efreightsuite.enumeration.LovStatus;
import lombok.Data;

@Data
public class DivisionMasterSearchDto {

    String searchDivisionName;
    String searchDivisionCode;
    String searchCompanyName;
    LovStatus searchStatus;


    String sortByColumn;
    String orderByType;
    Integer selectedPageNumber = 0;
    Integer recordPerPage = 10;
}
