package com.efreightsuite.dto;

import lombok.Data;

@Data
public class PreBookingMawbSearchDto {

    Long carrierId;
    Long porId;
    String[] mawbList;

    int count = 5;

}


