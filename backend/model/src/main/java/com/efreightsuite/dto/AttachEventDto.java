package com.efreightsuite.dto;

import lombok.Data;

@Data
public class AttachEventDto {


    Long sid;

    String serviceUid;

    String hawbNo;

    String consolUid;

    byte[] documentObj;

}
