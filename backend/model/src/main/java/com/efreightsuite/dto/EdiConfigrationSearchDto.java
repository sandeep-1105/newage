package com.efreightsuite.dto;

import lombok.Data;

@Data
public class EdiConfigrationSearchDto {

    String searchEdiKey;
    String searchEdiValue;
    String searchLocationName;
    String searchDescription;

    String sortByColumn;
    String orderByType;

    Integer selectedPageNumber = 0;

    Integer recordPerPage = 5;

}
