package com.efreightsuite.dto;

import lombok.Data;

@Data
public class ReportMasterSearchDto {


    String searchreportMasterName;
    String searchreportMasterCode;
    String searchreportMasterKey;
    String searchDocument;
    String isReportEnable;

    String sortByColumn;
    String orderByType;

    Integer selectedPageNumber = 0;

    Integer recordPerPage = 10;
}
