package com.efreightsuite.dto;

import com.efreightsuite.enumeration.Measurement;
import com.efreightsuite.model.CurrencyMaster;
import com.efreightsuite.model.Language;
import lombok.Data;
import lombok.ToString;

@Data
@ToString

public class LocationConfigurationDto {

    CurrencyMaster currencyMaster;

    String decimalPoint;

    Measurement measurement;

    String financialStratDate;

    String financialEndDate;

    Language language;

    Long userProfileId;

}
