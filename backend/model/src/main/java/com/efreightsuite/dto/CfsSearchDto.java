package com.efreightsuite.dto;

import lombok.Data;

@Data
public class CfsSearchDto {

    String searchCode;
    String searchName;
    String searchPortName;
    String searchStatus;

    String sortByColumn;
    String orderByType;

    Integer selectedPageNumber = 0;

    Integer recordPerPage = 10;

}
