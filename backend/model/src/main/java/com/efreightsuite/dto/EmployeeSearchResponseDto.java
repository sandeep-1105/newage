package com.efreightsuite.dto;

import java.io.Serializable;

import com.efreightsuite.enumeration.EmploymentStatus;
import com.efreightsuite.enumeration.YesNo;
import lombok.Data;

@Data
public class EmployeeSearchResponseDto implements Serializable {
    private static final long serialVersionUID = 1L;

    Long id;

    String employeeCode;

    String employeeName;

    String aliasName;

    EmploymentStatus employementStatus;

    YesNo isSalesman;

    String email;

    String employeePhoneNo;

    public EmployeeSearchResponseDto() {

    }

    public EmployeeSearchResponseDto(Long id, String employeeCode, String employeeName, String aliasName,
                                     EmploymentStatus employementStatus, YesNo isSalesman, String email, String employeePhoneNo) {
        super();
        this.id = id;
        this.employeeCode = employeeCode;
        this.employeeName = employeeName;
        this.aliasName = aliasName;
        this.employementStatus = employementStatus;
        this.isSalesman = isSalesman;
        this.email = email;
        this.employeePhoneNo = employeePhoneNo;
    }
}
