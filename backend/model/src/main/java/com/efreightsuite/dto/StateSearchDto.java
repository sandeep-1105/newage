package com.efreightsuite.dto;

import lombok.Data;

@Data
public class StateSearchDto {

    String searchStateName;
    String searchStateCode;
    String searchCountryName;
    String searchStatus;

    String sortByColumn;
    String orderByType;

    Integer selectedPageNumber = 0;

    Integer recordPerPage = 10;

}
