package com.efreightsuite.dto;

import lombok.Data;

@Data
public class CurrencyRateSearchDto {

	/*@JsonSerialize(using=JsonDateSerializer.class)
    @JsonDeserialize(using=JsonDateDeserializer.class)
	Date searchCurrencyDate;*/

    DateRange searchCurrencyDate;

    String searchToCurrencyCode;

    String searchFromCurrencyCode;

    String searchBuyRate;

    String searchSellRate;

    String sortByColumn;
    String orderByType;

    Integer selectedPageNumber = 0;

    Integer recordPerPage = 10;

}
