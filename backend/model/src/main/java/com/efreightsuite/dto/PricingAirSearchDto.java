package com.efreightsuite.dto;

import lombok.Data;

@Data
public class PricingAirSearchDto {


    String searchPortOrigin;
    String searchPortDestination;
    String searchTransitPort;
    String searchWhoRouted;

    String searchServiceName;
    String searchCharge;
    String searchCurrency;
    String searchUnit;

    String searchMinAmount;
    String searchAmountPerUnit;
    String searchCostInMinimum;
    String searchCostInAmount;
    String searchImportExport;


    DateRange searchValidFrom;
    DateRange searchValidTo;

    String searchChargeType;


    String sortByColumn;
    String orderByType;
    Integer selectedPageNumber = 0;
    Integer recordPerPage = 10;

}
