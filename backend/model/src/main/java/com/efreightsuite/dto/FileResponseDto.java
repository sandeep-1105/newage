package com.efreightsuite.dto;

import java.io.Serializable;

import com.efreightsuite.enumeration.ReportDownloadFileType;
import lombok.Data;

@Data
public class FileResponseDto implements Serializable {

    private static final long serialVersionUID = 1L;

    private byte[] byteArray;

    private String fileName;

    private String fileReponseContentType;

    private ReportDownloadFileType fileType;

}
