package com.efreightsuite.dto;

import lombok.Data;

@Data
public class AirlinePrebookingSearchRequestDto {

    String prebookingNumber;
    DateRange prebookingDate;

    String carrierName;
    String serviceName;
    String porName;
    String partyName;

    Long noOfPieces;

    Double volume;
    Double grossWeight;
    Double volumeWeight;

    String noOfMawb;

    String sortByColumn;
    String orderByType;

    Integer selectedPageNumber = 0;

    Integer recordPerPage = 10;
}
