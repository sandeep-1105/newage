package com.efreightsuite.dto;

import java.io.Serializable;
import java.util.Date;

import com.efreightsuite.enumeration.ServiceStatus;
import com.efreightsuite.enumeration.YesNo;
import com.efreightsuite.util.JsonDateDeserializer;
import com.efreightsuite.util.JsonDateSerializer;
import com.efreightsuite.util.JsonYesNoDeserializer;
import com.efreightsuite.util.JsonYesNoSerializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Data;

@Data
public class ShipmentSearchResponseDto implements Serializable {

    private static final long serialVersionUID = 1L;


    Long id;

    String shipperName;

    String shipperColorCode;

    String consigneeName;

    String consigneeColorCode;

    String shipmentUid;

    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date shipmentReqDate;

    String originName;

    String destinationName;

    String tosName;

    String employeeName;

    ServiceStatus serviceStatus;

    String hawbNo;

    String referenceNumber;

    String customerService;

    @JsonSerialize(using = JsonYesNoSerializer.class)
    @JsonDeserialize(using = JsonYesNoDeserializer.class)
    YesNo isOurPickUp;

    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date pickUpFollowUp;

    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date pickUpActualUp;


    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date etd;

    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date eta;

    String carrierName;

    String flightNumber;

    String mawb;

    String consolUid;


    public ShipmentSearchResponseDto(Long id, String shipperName, String shipperColorCode, String consigneeName, String consigneeColorCode, String shipmentUid, Date shipmentReqDate,
                                     String originName, String destinationName, String tosName, String employeeName, ServiceStatus serviceStatus,
                                     String hawbNo, String referenceNumber, String customerService) {
        this.id = id;
        this.shipperName = shipperName;
        this.shipperColorCode = shipperColorCode;

        this.consigneeName = consigneeName;
        this.consigneeColorCode = consigneeColorCode;

        this.shipmentUid = shipmentUid;
        this.shipmentReqDate = shipmentReqDate;
        this.originName = originName;
        this.destinationName = destinationName;
        this.tosName = tosName;
        this.employeeName = employeeName;
        this.serviceStatus = serviceStatus;
        this.hawbNo = hawbNo;
        this.referenceNumber = referenceNumber;
        this.customerService = customerService;
    }


    public ShipmentSearchResponseDto() {
    }
}
