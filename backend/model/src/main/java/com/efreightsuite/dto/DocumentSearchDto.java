package com.efreightsuite.dto;

import lombok.Data;

@Data
public class DocumentSearchDto {


    String searchDocumentCode;
    String searchDocumentName;
    String searchStatus;
    String isProtected;

    String sortByColumn;
    String orderByType;

    Integer selectedPageNumber = 0;

    Integer recordPerPage = 5;

}
