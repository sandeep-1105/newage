package com.efreightsuite.dto;

import lombok.Data;

@Data
public class FileUploadDto {

    private String fileName;

    private String fileType;

    private byte[] file;

    private Long pricingMasterId;

    private Long userId;

}
