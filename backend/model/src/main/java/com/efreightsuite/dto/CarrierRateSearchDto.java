package com.efreightsuite.dto;

import lombok.Data;

@Data
public class CarrierRateSearchDto {


    String searchCarrierName;
    String searchChargeName;
    String searchUnitName;
    String searchorigin;
    String searchDestination;
    DateRange searchFirstFromDate;
    String searchFirstAmount;
    String searchFirstMinAmount;
    DateRange searchSecondFromDate;
    String searchSecondAmount;
    String searchSecondMinAmount;
    String searchActualChargeble;


    String sortByColumn;
    String orderByType;

    Integer selectedPageNumber = 0;

    Integer recordPerPage = 10;


}
