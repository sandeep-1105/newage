package com.efreightsuite.dto;

import java.util.Date;

import com.efreightsuite.util.JsonDateDeserializer;
import com.efreightsuite.util.JsonDateSerializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Data;

@Data
public class AirlinePrebookingSearchResponseDto {

    Long id;


    String prebookingNumber;

    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date prebookingDate;

    String carrierName;

    String serviceName;

    String porName;

    String partyName;

    String locale;

    Long noOfPieces;

    Double volume;

    Double grossWeight;

    Double volumeWeight;

    Integer noOfMawb;

    public AirlinePrebookingSearchResponseDto(Long id, String prebookingNumber, Date prebookingDate, String carrierName, String serviceName,
                                              String porName, String partyName, Long noOfPieces, Double volume, Double grossWeight, Double volumeWeight,
                                              Integer noOfMawb, String locale) {
        this.id = id;
        this.prebookingNumber = prebookingNumber;
        this.carrierName = carrierName;
        this.prebookingDate = prebookingDate;
        this.serviceName = serviceName;
        this.porName = porName;
        this.partyName = partyName;
        this.noOfPieces = noOfPieces;
        this.noOfMawb = noOfMawb;
        this.volume = volume;
        this.grossWeight = grossWeight;
        this.volumeWeight = volumeWeight;
        this.locale = locale;
    }

    public AirlinePrebookingSearchResponseDto() {

    }

}
