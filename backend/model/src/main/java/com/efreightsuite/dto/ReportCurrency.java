package com.efreightsuite.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ReportCurrency {

    String currencyCode;
    Double roe;
    String localAmount;

    public ReportCurrency() {

    }

    public ReportCurrency(String currencyCode, Double roe, String localAmount) {
        this.currencyCode = currencyCode;
        this.roe = roe;
        this.localAmount = localAmount;
    }
}
