package com.efreightsuite.dto;

import java.util.Date;

import com.efreightsuite.enumeration.Approved;
import com.efreightsuite.util.JsonDateDeserializer;
import com.efreightsuite.util.JsonDateSerializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Data;

@Data
public class QuotationApproveDto {

    Long quotationId;

    Approved approved;

    Long employeeId;

    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date approvedDate;

    // Quotation customer approval
    Long customerId;
    String quotationNo;
    String saasId;
    String pin;
    Long quotationCarrierId;
    String rejectReason;

    public QuotationApproveDto() {
    }

    public QuotationApproveDto(Long quotationId, Approved approved, Long employeeId) {
        this.quotationId = quotationId;
        this.approved = approved;
        this.employeeId = employeeId;
    }
}
