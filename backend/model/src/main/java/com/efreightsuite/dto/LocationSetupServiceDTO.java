package com.efreightsuite.dto;

import java.io.Serializable;

import com.efreightsuite.enumeration.FullGroupage;
import com.efreightsuite.enumeration.ImportExport;
import com.efreightsuite.enumeration.LovStatus;
import com.efreightsuite.enumeration.TransportMode;
import com.efreightsuite.model.common.LocationSetupService;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LocationSetupServiceDTO implements Serializable {
    private static final long serialVersionUID = 1L;

    Long id;

    String serviceCode;

    String serviceName;

    TransportMode transportMode;

    ImportExport importExport;

    FullGroupage fullGroupage;

    LovStatus status;

    public LocationSetupServiceDTO(LocationSetupService serviceMaster) {

        id = serviceMaster.getServiceMaster().getId();
        serviceCode = serviceMaster.getServiceMaster().getServiceCode();
        serviceName = serviceMaster.getServiceMaster().getServiceName();

    }
}
