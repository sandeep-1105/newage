package com.efreightsuite.dto;

import lombok.Data;

@Data
public class PortGroupSearchDto {


    String searchPortGroupCode;
    String searchPortGroupName;
    String searchCountryName;
    String searchStatus;

    String sortByColumn;
    String orderByType;

    Integer selectedPageNumber = 0;
    Integer recordPerPage = 10;

}
