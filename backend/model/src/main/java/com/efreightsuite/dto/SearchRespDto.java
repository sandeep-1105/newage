package com.efreightsuite.dto;

import lombok.Data;

@Data
public class SearchRespDto {

    String keyword;

    Integer selectedPageNumber;

    Integer recordPerPage;

    Long totalRecord;

    Object searchResult;

    public SearchRespDto() {
    }

    public SearchRespDto(SearchRequest searchRequest, Long totalRecord, Object searchResult) {
        this.keyword = searchRequest.getKeyword();
        this.selectedPageNumber = searchRequest.getSelectedPageNumber();
        this.recordPerPage = searchRequest.getRecordPerPage();
        this.totalRecord = totalRecord;
        this.searchResult = searchResult;
    }

    public SearchRespDto(Long totalRecord, Object searchResult) {
        this.totalRecord = totalRecord;
        this.searchResult = searchResult;
    }


    public SearchRespDto(Object searchResult) {
        this.searchResult = searchResult;
    }
}
