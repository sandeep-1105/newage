package com.efreightsuite.dto;

import com.efreightsuite.enumeration.LovStatus;
import lombok.Data;

@Data
public class RecordAccessLevelSearchDto {

    Long id;
    String name;
    String description;
    String status;

    LovStatus lovStatus;
    String sortByColumn;
    String orderByType;
    Integer selectedPageNumber = 0;
    Integer recordPerPage = 10;

    public RecordAccessLevelSearchDto() {

    }

    public RecordAccessLevelSearchDto(Long id, String name, String description, LovStatus lovStatus) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.lovStatus = lovStatus;
    }

}
