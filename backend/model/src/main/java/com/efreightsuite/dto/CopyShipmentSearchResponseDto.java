package com.efreightsuite.dto;

import java.io.Serializable;
import java.util.Date;

import com.efreightsuite.util.JsonDateDeserializer;
import com.efreightsuite.util.JsonDateSerializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Data;

@Data
public class CopyShipmentSearchResponseDto implements Serializable {
    private static final long serialVersionUID = 1L;

    Long id;

    String originName;

    String destinationName;

    String shipperName;

    String consigneeName;

    String tosName;

    String shipmentUid;

    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date shipmentDate;

    Long shipmentServiceDetailId;

    public CopyShipmentSearchResponseDto(Long id, String originName, String destinationName, String shipperName, String consigneeName, String tosName, String shipmentUid, Date shipmentDate, Long shipmentServiceDetailId) {
        this.id = id;
        this.originName = originName;
        this.destinationName = destinationName;
        this.shipperName = shipperName;
        this.consigneeName = consigneeName;
        this.tosName = tosName;
        this.shipmentUid = shipmentUid;
        this.shipmentDate = shipmentDate;
        this.shipmentServiceDetailId = shipmentServiceDetailId;
    }

}

