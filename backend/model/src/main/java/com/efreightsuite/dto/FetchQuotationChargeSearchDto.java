package com.efreightsuite.dto;

import java.io.Serializable;

import com.efreightsuite.enumeration.TransportMode;
import com.efreightsuite.enumeration.WhoRouted;
import lombok.Data;

@Data
public class FetchQuotationChargeSearchDto implements Serializable {

    private static final long serialVersionUID = 1L;

    boolean hazardous = false;

    Long originId;

    Long destinationId;

    Long carrierId;

    double chargable;

    WhoRouted whoRouted;

    TransportMode transportMode;
}
