package com.efreightsuite.dto;

import lombok.Data;

@Data
public class RoleSearchDto {

    String searchDescription;
    String searchRoleName;
    String searchStatus;

    String sortByColumn;
    String orderByType;

    Integer selectedPageNumber = 0;

    Integer recordPerPage = 10;

}
