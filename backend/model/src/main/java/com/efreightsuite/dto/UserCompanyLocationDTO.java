package com.efreightsuite.dto;

import com.efreightsuite.enumeration.YesNo;
import com.efreightsuite.model.LocationMaster;
import com.efreightsuite.model.SystemTrack;
import com.efreightsuite.model.UserCompanyLocation;
import lombok.Data;

@Data
public class UserCompanyLocationDTO {


    Long id;

    long versionLock;

    SystemTrack systemTrack;

    UserCompanyDTO userCompany;

    YesNo yesNo;

    LocationMaster locationMaster;

    String note;

    public UserCompanyLocationDTO(UserCompanyLocation entity) {
        this.id = entity.getId();
        this.versionLock = entity.getVersionLock();
        this.systemTrack = entity.getSystemTrack();
        this.locationMaster = entity.getLocationMaster();
        //this.userCompany=new UserCompanyDTO(entity.getUserCompany());
        this.yesNo = entity.getYesNo();
        this.note = entity.getNote();
    }

    public UserCompanyLocationDTO() {
    }
}
