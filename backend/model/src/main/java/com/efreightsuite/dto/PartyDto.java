package com.efreightsuite.dto;

import java.io.Serializable;

import com.efreightsuite.enumeration.LovStatus;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PartyDto implements Serializable {

    private static final long serialVersionUID = 1L;

    Long id;

    String partyName;

    String partyCode;

    LovStatus status;


}
