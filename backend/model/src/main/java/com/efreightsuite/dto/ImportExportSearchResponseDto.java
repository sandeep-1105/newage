package com.efreightsuite.dto;

import java.io.Serializable;
import java.util.Date;

import com.efreightsuite.util.JsonDateDeserializer;
import com.efreightsuite.util.JsonDateSerializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Data;

@Data
public class ImportExportSearchResponseDto implements Serializable {
    private static final long serialVersionUID = 1L;

    Long id;

    String locationName;

    String shipmentUid;

    String serviceName;

    String serviceUid;

    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date serviceReqDate;

    String shipperName;

    String colorCode;

    String consigneeName;

    String consigneeColorCode;

    String originName;
    String destinationName;

    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date eta;

    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date etd;


    String hawbNo;
    String exportRef;

    public ImportExportSearchResponseDto(Long id, String locationName, String shipmentUid, String serviceName, String serviceUid, Date serviceReqDate, String shipperName, String colorCode, String consigneeName, String consigneeColorCode, String originName, String destinationName, Date eta, Date etd, String hawbNo, String exportRef) {
        this.id = id;
        this.locationName = locationName;
        this.shipmentUid = shipmentUid;
        this.serviceName = serviceName;
        this.serviceUid = serviceUid;
        this.serviceReqDate = serviceReqDate;
        this.shipperName = shipperName;
        this.colorCode = colorCode;
        this.consigneeName = consigneeName;
        this.consigneeColorCode = consigneeColorCode;
        this.originName = originName;
        this.destinationName = destinationName;
        this.eta = eta;
        this.etd = etd;
        this.hawbNo = hawbNo;
        this.exportRef = exportRef;
    }

}
