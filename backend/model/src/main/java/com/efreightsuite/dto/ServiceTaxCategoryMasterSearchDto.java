package com.efreightsuite.dto;

import lombok.Data;

@Data
public class ServiceTaxCategoryMasterSearchDto {


    String code;
    String name;
    String status;


    String sortByColumn;
    String orderByType;


    Integer selectedPageNumber = 0;
    Integer recordPerPage = 10;
}
