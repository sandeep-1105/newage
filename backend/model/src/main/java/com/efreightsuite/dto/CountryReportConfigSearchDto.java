package com.efreightsuite.dto;

import lombok.Data;

@Data
public class CountryReportConfigSearchDto {

    String searchReportName;

    String searchCountryName;

    String searchCopies;


    String sortByColumn;
    String orderByType;

    Integer selectedPageNumber = 0;

    Integer recordPerPage = 5;


}
