package com.efreightsuite.dto;

import lombok.Data;

/**
 * @author Devendrachary M
 */

@Data
public class EnquiryLogSearchDto {

    String searchCustomerName;
    String searchEnquiryNo;
    DateRange searchQuoteBy;
    DateRange searchReceivedOn;
    String searchQuotationNo;
    String searchSalesCoOrdinatorName;
    String searchSalesmanName;
    DateRange searchLoggedOnDate;
    String searchLoggedByName;

    String searchStatus;

    String sortByColumn;
    String orderByType;

    Integer selectedPageNumber = 0;
    Integer recordPerPage = 5;

    public EnquiryLogSearchDto() {
    }

    private EnquiryLogSearchDto(Builder builder) {
        setSearchCustomerName(builder.searchCustomerName);
        setSearchEnquiryNo(builder.searchEnquiryNo);
        setSearchQuoteBy(builder.searchQuoteBy);
        setSearchReceivedOn(builder.searchReceivedOn);
        setSearchQuotationNo(builder.searchQuotationNo);
        setSearchSalesCoOrdinatorName(builder.searchSalesCoOrdinatorName);
        setSearchSalesmanName(builder.searchSalesmanName);
        setSearchLoggedOnDate(builder.searchLoggedOnDate);
        setSearchLoggedByName(builder.searchLoggedByName);
        setSearchStatus(builder.searchStatus);
        setSortByColumn(builder.sortByColumn);
        setOrderByType(builder.orderByType);
        setSelectedPageNumber(builder.selectedPageNumber);
        setRecordPerPage(builder.recordPerPage);
    }


    public static final class Builder {
        private String searchCustomerName;
        private String searchEnquiryNo;
        private DateRange searchQuoteBy;
        private DateRange searchReceivedOn;
        private String searchQuotationNo;
        private String searchSalesCoOrdinatorName;
        private String searchSalesmanName;
        private DateRange searchLoggedOnDate;
        private String searchLoggedByName;
        private String searchStatus;
        private String sortByColumn;
        private String orderByType;
        private Integer selectedPageNumber;
        private Integer recordPerPage;

        public Builder() {
        }

        public Builder withSearchCustomerName(String val) {
            searchCustomerName = val;
            return this;
        }

        public Builder withSearchEnquiryNo(String val) {
            searchEnquiryNo = val;
            return this;
        }

        public Builder withSearchQuoteBy(DateRange val) {
            searchQuoteBy = val;
            return this;
        }

        public Builder withSearchReceivedOn(DateRange val) {
            searchReceivedOn = val;
            return this;
        }

        public Builder withSearchQuotationNo(String val) {
            searchQuotationNo = val;
            return this;
        }

        public Builder withSearchSalesCoOrdinatorName(String val) {
            searchSalesCoOrdinatorName = val;
            return this;
        }

        public Builder withSearchSalesmanName(String val) {
            searchSalesmanName = val;
            return this;
        }

        public Builder withSearchLoggedOnDate(DateRange val) {
            searchLoggedOnDate = val;
            return this;
        }

        public Builder withSearchLoggedByName(String val) {
            searchLoggedByName = val;
            return this;
        }

        public Builder withSearchStatus(String val) {
            searchStatus = val;
            return this;
        }

        public Builder withSortByColumn(String val) {
            sortByColumn = val;
            return this;
        }

        public Builder withOrderByType(String val) {
            orderByType = val;
            return this;
        }

        public Builder withSelectedPageNumber(Integer val) {
            selectedPageNumber = val;
            return this;
        }

        public Builder withRecordPerPage(Integer val) {
            recordPerPage = val;
            return this;
        }

        public EnquiryLogSearchDto build() {
            return new EnquiryLogSearchDto(this);
        }
    }
}
