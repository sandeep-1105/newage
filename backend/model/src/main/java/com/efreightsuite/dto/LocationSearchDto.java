package com.efreightsuite.dto;

import lombok.Data;

@Data
public class LocationSearchDto extends AbstractSearchDto{

    String searchCode;
    String searchName;
    String searchCompanyName;
    String searchBranchName;
    String searchCountryName;
    String searchStatus;

    public LocationSearchDto() {}

    public LocationSearchDto(String searchCode) {
        this.searchCode = searchCode;
    }

    public LocationSearchDto(String searchCode, String searchName) {
        this(searchCode);
        this.searchName = searchName;
    }

    public LocationSearchDto(String searchCode, String searchName,String searchBranchName ) {
        this(searchCode,searchName);
        this.searchBranchName = searchBranchName;
    }

    public LocationSearchDto(String searchCode, String searchName, String searchBranchName,String searchCompanyName ) {
        this(searchCode,searchName,searchBranchName);
        this.searchCompanyName = searchCompanyName;
    }

    public LocationSearchDto(String searchCode, String searchName, String searchBranchName, String searchCompanyName, String searchCountryName, String searchStatus) {
        this(searchCode,searchName,searchBranchName,searchCompanyName);
        this.searchCountryName = searchCountryName;
        this.searchStatus = searchStatus;
    }
}
