package com.efreightsuite.dto;

import com.efreightsuite.enumeration.LovStatus;
import lombok.Data;

@Data
public class ZoneMasterSearchDto {

    String searchZoneName;
    String searchZoneCode;
    String searchCountryName;
    String searchCountryCode;
    LovStatus searchStatus;


    String sortByColumn;
    String orderByType;
    Integer selectedPageNumber = 0;
    Integer recordPerPage = 10;
}
