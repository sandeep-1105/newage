package com.efreightsuite.dto;

import java.util.Date;

import com.efreightsuite.model.FlightPlan;
import com.efreightsuite.util.JsonDateDeserializer;
import com.efreightsuite.util.JsonDateSerializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FlightPlanCopyDto {

    FlightPlan flightPlan;

    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date effectiveUpto;

    /* Flight Frequency (in Days) */
    Integer flightFrequency;

    String copyFrom;

}
