package com.efreightsuite.dto;

import java.io.Serializable;
import java.util.Date;

import com.efreightsuite.util.JsonDateDeserializer;
import com.efreightsuite.util.JsonDateSerializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Data;

@Data
public class CfsReceiveSearchResponseDto implements Serializable {

    private static final long serialVersionUID = 1L;

    Long id;

    String receiptNumber;

    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date receiptDate;

    String cfsMasterName;

    String shipmentUid;

    String proNumber;

    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date proDate;

    String shipperName;

    String consigneeName;

    String shipperRefNumber;

    String onHand;

    String truckerName;

    public CfsReceiveSearchResponseDto(Long id, String receiptNumber, Date receiptDate, String cfsMasterName,
                                       String shipmentUid, String proNumber, Date proDate, String shipperName, String consigneeName,
                                       String shipperRefNumber, String onHand, String truckerName) {
        this.id = id;
        this.receiptNumber = receiptNumber;
        this.receiptDate = receiptDate;
        this.cfsMasterName = cfsMasterName;
        this.shipmentUid = shipmentUid;
        this.proNumber = proNumber;
        this.proDate = proDate;
        this.shipperName = shipperName;
        this.consigneeName = consigneeName;
        this.shipperRefNumber = shipperRefNumber;
        this.onHand = onHand;
        this.truckerName = truckerName;
    }

    public CfsReceiveSearchResponseDto() {
    }
}
