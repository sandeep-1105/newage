package com.efreightsuite.dto;

import java.util.List;

import com.efreightsuite.enumeration.TransportMode;
import com.efreightsuite.enumeration.WhoRouted;
import com.efreightsuite.model.CompanyMaster;
import com.efreightsuite.model.CountryMaster;
import com.efreightsuite.model.LocationMaster;
import com.efreightsuite.model.PortMaster;
import com.efreightsuite.model.PricingPortPair;
import com.efreightsuite.model.ServiceMaster;
import lombok.Data;

@Data
public class PricingMasterDto {


    List<PricingPortPair> pricingPortPairList;

    TransportMode transportMode;

    PortMaster origin;

    PortMaster destination;

    PortMaster transit;

    WhoRouted whoRouted;

    LocationMaster locationMaster;

    CompanyMaster companyMaster;

    CountryMaster countryMaster;

    ServiceMaster serviceMaster;

}
