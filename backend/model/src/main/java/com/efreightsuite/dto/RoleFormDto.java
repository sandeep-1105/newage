package com.efreightsuite.dto;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class RoleFormDto implements Serializable {

    private static final long serialVersionUID = 1L;

    RoleDTO roleDto;

    List<RoleDTO> selectedList;
}
