package com.efreightsuite.dto;

import lombok.Data;

@Data
public class DesignationSearchDto {


    String searchDesignationCode;
    String searchDesignationName;
    String searchManager;
    String searchStatus;

    String sortByColumn;
    String orderByType;

    Integer selectedPageNumber = 0;
    Integer recordPerPage = 10;

}
