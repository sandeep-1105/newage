package com.efreightsuite.dto;

import lombok.Data;

@Data
public class EventSearchDto {

    String searchCode;
    String searchName;
    String searchStatus;
    String searchCountryName;
    String searchType;

    String sortByColumn;
    String orderByType;

    Integer selectedPageNumber = 0;

    Integer recordPerPage = 10;

}
