package com.efreightsuite.dto;

import java.io.Serializable;

import com.efreightsuite.enumeration.UserStatus;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class UserDto implements Serializable {

    private static final long serialVersionUID = 1L;

    Long id;

    String userName;

    UserStatus status;

    public UserDto() {
        // TODO Auto-generated constructor stub
    }


    public UserDto(Long id, String userName, UserStatus status) {
        this.id = id;
        this.userName = userName;
        this.status = status;
    }

}
