package com.efreightsuite.dto;

import com.efreightsuite.model.AddressMaster;
import com.efreightsuite.model.PartyMaster;
import com.efreightsuite.model.ShipmentServiceDetail;
import lombok.Data;

@Data
public class ImportToExportDto {

    ShipmentServiceDetail importService;

    ShipmentServiceDetail exportService;

    String serviceUid;

    PartyMaster agent;

    AddressMaster agentAddress;

    String notes;

    String shipmentUid;
}
