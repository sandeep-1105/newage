package com.efreightsuite.dto;

import lombok.Data;

@Data
public class EmployeeSearchRequestDto {

    String searchEmployeeCode;
    String searchEmployeeName;
    String searchAliasName;
    String searchEmploymentStatus;
    String searchIsSalesman;
    String searchEmail;
    String searchPhoneNumber;


    String sortByColumn;
    String orderByType;

    Integer selectedPageNumber = 0;
    Integer recordPerPage = 10;
}
