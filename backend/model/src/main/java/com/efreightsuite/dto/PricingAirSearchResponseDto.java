package com.efreightsuite.dto;

import com.efreightsuite.enumeration.WhoRouted;
import lombok.Data;

@Data
public class PricingAirSearchResponseDto {

    Long id;
    String originName;
    String destinationName;
    String transitName;
    WhoRouted whoRouted;

    public PricingAirSearchResponseDto(Long id, String originName, String destinationName, String transitName, WhoRouted whoRouted) {
        this.id = id;
        this.originName = originName;
        this.destinationName = destinationName;
        this.transitName = transitName;
        this.whoRouted = whoRouted;
    }

    public PricingAirSearchResponseDto() {
    }

}
