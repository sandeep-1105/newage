package com.efreightsuite.dto;

import com.efreightsuite.enumeration.LovStatus;
import lombok.Data;

@Data
public class ServiceTaxPercentageSearchResponseDto {


    Long id;
    String taxCode;
    String taxName;
    LovStatus status;

    public ServiceTaxPercentageSearchResponseDto(Long id, String taxCode, String taxName, LovStatus status) {
        this.id = id;
        this.taxCode = taxCode;
        this.taxName = taxName;
        this.status = status;
    }

    public ServiceTaxPercentageSearchResponseDto() {
    }


}
