/* 
Description : This file contains all the re-usable functions required 
*/

var action = function () {

    var expectedCondition = protractor.ExpectedConditions;
    var path = require('path');
    var fs = require('fs');
    var longWait = 30000;
    var shortWait = 10000;
    var avgWait = 20000;
    this.linkCountBeforeAdd;
    this.linkCountAfterAdd;

    this.menuTasks = by.xpath("//a[@data-title='Tasks']");
    this.subMenuMyTasks = by.className("col-xs-9 ptb14 pl10 ng-binding");
    this.menuSales = by.xpath("//a[@data-title='Sales']");
    this.subMenuEnquiry = by.xpath("//b[@class='col-xs-9 ptb14 pl10 ng-binding' and text()='Enquiry']");
    this.subMenuQuotation = by.xpath("//b[@class='col-xs-9 ptb14 pl10 ng-binding' and text()='Quotation']");
    this.menuCrm = by.xpath("//a[@data-title='CRM']");
    this.subMenuShipment = by.xpath("//b[@class='col-xs-9 ptb14 pl10 ng-binding' and text()='Shipment']");
    this.menuAir = by.xpath("//a[@data-title='Air']");
    this.subMenuMasterShipment = by.xpath("//b[@class='col-xs-9 ptb14 pl10 ng-binding' and text()='Master Shipment']");
    this.menuSetup = by.css(".icon-setup");
    this.subMenuAppConfiguration = by.xpath("//b[@class='col-xs-9 ptb14 pl10 ng-binding' and text()='App Configuration']");

    var openApplication = function (strUrl) {
        browser.get(strUrl);
    }

    this.setApplicationUrl = function (loginPage, testSpecName) {
        var appIp = process.env.APP_IP || 'localhost';
        var appUrl = "http://" + appIp + ":9090/";
        console.log('Running ' + testSpecName + ' test on: ', appUrl);
        openApplication(appUrl);
        browser.getCurrentUrl().then(function (url) {
            if (url.indexOf("#/login") == -1) {
                console.log("Executed beforeAll function logout...");
                loginPage.appLogout();
                openApplication(appUrl);
            }
        });
        this.browserRefresh();
    }

    this.setUrlAndLoginApp = function (loginPage, loginData, testSpecName) {
        this.setApplicationUrl(loginPage, testSpecName);
        this.verifyElementPresent(loginPage.textboxSaasid);
        loginPage.appLogin(loginData.saasId, loginData.userName, loginData.password);
    }

    this.setText = function (strLocator, strInput) {
        browser.wait(expectedCondition.presenceOf(element(strLocator)), avgWait);
        var reqElement = element(strLocator);
        reqElement.clear();
        reqElement.sendKeys(strInput);
    };

    this.click = function (strLocator) {
        browser.wait(expectedCondition.presenceOf(element(strLocator)), longWait);
        element(strLocator).click();
    };

    this.getPageTitle = function () {
        return browser.getTitle();
    };

    this.dragAndDrop = function (strLocatorDrag, strLocatorDrop) {
        var dragElement = element(strLocatorDrag);
        var dropElement = element(strLocatorDrop);
        browser.actions().
            dragAndDrop(dragElement, dropElement).
            perform();
    };

    this.switchToFrame = function (strLocator) {
        var reqElement = element(strLocator);
        browser.switchTo().frame((reqElement).getWebElement());
    };

    this.handleAlert = function (strOperation) {
        // Waits for an alert pops up.
        browser.wait(expectedCondition.alertIsPresent(), shortWait);
        var alertWindow = browser.switchTo().alert();
        if (strOperation.toLowerCase() == "ok") {
            alertWindow.accept();
        } else if (strOperation.toLowerCase() == "cancel") {
            alertWindow.dismiss();
        } else {
            // added for future perspective
        }
    };

    this.verifyElementPresent = function (strLocator) {
        var reqElement = element(strLocator);
        browser.wait(expectedCondition.presenceOf(reqElement), longWait);
        expect(reqElement.isDisplayed()).toBe(true);
    };

    //objElement = "none" for locator
    //strLocator = "none" for element
    this.verifyElementText = function (strLocator, objElement, strText) {
        var reqElement;
        if (objElement === "none") {
            browser.wait(expectedCondition.presenceOf(element(strLocator)), avgWait);
            reqElement = element(strLocator);
        }
        else if (strLocator === "none") {
            reqElement = objElement;
        }
        expect(reqElement.getText()).toEqual(strText);
    };

    this.verifyElementPartialText = function (strLocator, strText) {
        browser.wait(expectedCondition.presenceOf(element(strLocator)), avgWait);
        var reqElement = element(strLocator);
        expect(reqElement.getText()).toContain(strText);
    };

    this.compareString = function (strExpected, strActual) {
        expect(strExpected).toContain(strActual);
    };

    this.switchToWindow = function (windowHandleNum) {
        var countFlag = false;
        browser.wait(function () {
            browser.getAllWindowHandles().then(function (handles) {
                if (handles[windowHandleNum]) {
                    browser.switchTo().window(handles[windowHandleNum]);
                    countFlag = true;
                    return true;
                }
                else {
                    return false;
                }
            });
            return countFlag;
        }, 10000)
    }

    this.performMouseHover = function (strLocator) {
        var reqElement = element(strLocator);
        browser.actions().mouseMove(reqElement).perform();
    };

    this.getTextByLocator = function (strLocator) {
        var reqElement = element(strLocator);
        return reqElement.getText().then(function (text) {
            return text;
        });
    };

    this.getAttributeValue = function (strLocator, strAttribute) {
        var reqElement = element(strLocator);
        return reqElement.getAttribute(strAttribute);
    }

    //-- getText() gives promise so to verify the incremented value than the previous
    //first call with second attribute as none, then add enquiry/quotation etc and again call 
    // function to get updated count and to verify the same.
    this.verifySplitCountOfLocator = function (strLocator, strAttribute) {
        var reqElement = element(strLocator);
        reqElement.getText().then(function (text) {
            var splitText = text.split("(")[1].split(")");
            if (strAttribute == "none") {
                this.linkCountBeforeAdd = parseInt(splitText[0]);
            }
            else {
                this.linkCountAfterAdd = splitText[0];
                expect(this.linkCountAfterAdd).toEqual((this.linkCountBeforeAdd + 1).toString());
            }
        });
    };

    this.selectDropdown = function (strLocator, strText) {
        browser.wait(expectedCondition.presenceOf(element(strLocator)), avgWait);
        reqElement = element(strLocator).element(by.xpath("option[@label='" + strText + "']"));
        reqElement.click();
    };

    // intIndex = 1 for first element
    // intIndex = 0 for last element
    // intIndex = 3 for element indexed at 3
    this.getRequiredElement = function (strLocator, intIndex) {

        var reqElement;
        if (typeof (intIndex) == 'number') {
            browser.wait(expectedCondition.presenceOf(element(strLocator)), avgWait);
            var reqElements = element.all(strLocator);
        }
        if (intIndex == 0) {
            reqElement = reqElements.last();
        }
        else if (intIndex == 1) {
            reqElement = reqElements.first();
        }
        else {
            reqElement = reqElements.get(intIndex);
        }
        return reqElement;
    };

    this.getTodayTime = function () {
        var todayDate = new Date();
        var todayHour = todayDate.getHours();
        var todayMin = todayDate.getMinutes();
        var todaySec = todayDate.getSeconds();
        var strTodayTime = todayHour.toString() + todayMin.toString() + todaySec.toString();
        return strTodayTime;
    };

    this.getChainedElement = function (strParentLocator, strChildLocator) {
        var reqElement;
        if (strParentLocator != "" && strChildLocator != "") {
            browser.wait(expectedCondition.presenceOf(element(strParentLocator).element(strChildLocator)), avgWait);
            reqElement = element(strParentLocator).element(strChildLocator);
        }
        else {
            reqElement = "";
        }
        return reqElement;
    };

    this.uploadFile = function (strLocator, strFileRelativePath) {
        var strFileAbsPath = path.resolve(__dirname, strFileRelativePath);
        browser.wait(expectedCondition.presenceOf(element(strLocator)), longWait);
        var reqElement = element(strLocator);
        reqElement.sendKeys(strFileAbsPath)
    };

    this.verifyCssProperty = function (strLocator, strPropertyName, strExpectedPropertyValue) {
        var reqElement;
        browser.wait(expectedCondition.presenceOf(element(strLocator)), avgWait);
        reqElement = element(strLocator);
        expect(element(reqElement).getCssValue(strPropertyName)).toBe(strExpectedPropertyValue);
    };

    this.explicitWait = function (strWait) {
        browser.sleep(strWait);
    }

    this.scrollToElement = function (strLocator) {
        var reqElement;
        reqElement = element(strLocator);
        browser.actions().mouseMove(reqElement).perform();
    }

    this.getTodayDate = function () {
        var strTodayDate;
        var objTodayDate = new Date();
        var todayDate = objTodayDate.getDate();
        var todayMonth = objTodayDate.getMonth() + 1; //January is 0 so add 1
        var todayYear = objTodayDate.getFullYear();
        if (todayDate < 10) {
            todayDate = '0' + todayDate;
        }
        if (todayMonth < 10) {
            todayMonth = '0' + todayMonth;
        }
        strTodayDate = todayDate + '-' + todayMonth + '-' + todayYear;
        return strTodayDate;
    }

    this.getFutureDate = function(intFutureDays){
        var strTodayDate;
        var objTodayDate = new Date();
        objTodayDate.setDate(objTodayDate.getDate() + intFutureDays);
        var todayDate = objTodayDate.getDate();
        var todayMonth = objTodayDate.getMonth() + 1; //January is 0 so add 1
        var todayYear = objTodayDate.getFullYear();
        if (todayDate < 10) {
            todayDate = '0' + todayDate;
        }
        if (todayMonth < 10) {
            todayMonth = '0' + todayMonth;
        }
        strTodayDate = todayDate + '-' + todayMonth + '-' + todayYear;
        return strTodayDate;
    }

    //-- enter text in table specific column 
    this.fillTextInTableColumn = function (strDataElement, intRowNum, intColumnNum, strText) {
        var reqElement = by.xpath("//super-table[@data = '" + strDataElement + "']//thead/tr[" + intRowNum + "]/th[" + intColumnNum + "]//input");
        return this.setText(reqElement, strText);
    }

    //-- verify text in table specific row column 
    this.verifySearchTextInTableRowCol = function (strDataElement, intRowNum, intColumnNum, strText) {
        var reqElement = element(by.xpath("//super-table[@data = '" + strDataElement + "']//tbody/tr[" + intRowNum + "]/td[" + intColumnNum + "]/span[@class='ng-binding']"));
        expect(reqElement.getAttribute('textContent')).toBe(strText);
    }

    this.clickRowInAirExportTable = function (strCustomerName, strOtherColumnVal) {
        var reqElement = by.xpath("//td[@title='" + strCustomerName + "']/../td[@title='" + strOtherColumnVal + "']");
        this.click(reqElement);
    }

    this.clickAddCustomerTable = function (strCustomerName) {
        var customerName = by.xpath("//input[@id='partyMaster']/../..//span");
        this.click(customerName);
        var addCustomer = by.xpath("//*[contains(text(),'Add New')]");
        this.click(addCustomer);
    }

    this.browserRefresh = function () {
        browser.refresh();
    }

    this.installNpmPackage = function (packageName) {
        const execSync = require('child_process').execSync;
        code = execSync('npm install --no-save ' + packageName);
    }

    // Description-: To verify report file created on ftp server
    // Parameters -:
    // Param -: Host,Username, passwd in key value pair
    // filename -: Name of file to verify on ftp server

    this.checkPlatform = function () {
        this.installNpmPackage('os');
        var os = require('os');
        if (os.platform() === 'win32') {
            this.installNpmPackage('chilkat_node8_win32');
        }
        else if (os.platform() == 'linux') {
            if (os.arch() === 'arm') {
                this.installNpmPackage('chilkat_node8_arm');
            }
            else if (os.arch() === 'x86') {
                this.installNpmPackage('chilkat_node8_linux32');
            }
            else {
                this.installNpmPackage('chilkat_node8_linux64');
            }
        }
        else if (os.platform() === 'darwin') {
            this.installNpmPackage('chilkat_node8_macosx');
        }
    }

    this.ftpLogin = function (param, fileName) {
        var os = require('os');
        var chilkat;
        if (os.platform() === 'win32') {
            chilkat = require('chilkat_node8_win32');
        }
        else if (os.platform() === 'linux') {
            if (os.arch() === 'arm') {
                chilkat = require('chilkat_node8_arm');
            }
            else if (os.arch() === 'x86') {
                chilkat = require('chilkat_node8_linux32');
            }
            else {
                chilkat = require('chilkat_node8_linux64');
            }
        }
        else if (os.platform() === 'darwin') {
            var chilkat = require('chilkat_node8_macosx');
        }
        var ftp = new chilkat.Ftp2();
        var success;
        success = ftp.UnlockComponent("Success");
        if (success !== true) {
            console.log(ftp.LastErrorText);
            return 0;
        }
        ftp.Hostname = param.hostName;
        ftp.Username = param.userName;
        ftp.Password = param.passwd;

        //  Connect and login to the FTP server.
        success = ftp.Connect();
        if (success !== true) {
            console.log(ftp.LastErrorText);
            return 0;
        }

        //  Set the current remote directory to where the file is located:
        success = ftp.ChangeRemoteDir("/NEW_EFS");
        if (success !== true) {
            console.log(ftp.LastErrorText);
            return 0;
        }

        //  Test to see if the file exists by getting the file size by name.
        var fileSize = ftp.GetSizeByName(fileName);
        console.log(fileName + " file size ------>" + fileSize);
        if (fileSize > 0) {
            return fileSize;
        }
        else {
            return 0;
        }
        success = ftp.Disconnect();
    }

    this.verifyDownloadedFile = function (fileName) {
        browser.wait(function () {
            return fs.existsSync(fileName);
        }, 50000)
    }

    this.verifyAndDeleteDownloadedFile = function (dir, fileName) {
        var status = false;
        var filesList;

        browser.wait(function () {
            fs.readdir(dir, (err, files) => {
                if (files.length > 0) {
                    filesList = fs.readdirSync(dir);
                    status = true;
                    for (file of filesList) {
                        expect(file).toContain(fileName);
                        fs.unlink((dir + file), function (err) {
                            if (err != null) {
                                throw err;
                            }
                        });
                    }
                }
            });
            return status;
        }, 50000)
    }

    this.deleteDownloadedFile = function (fileName) {
        browser.wait(function () {
            return fs.existsSync(fileName);
        }, 50000).then(function () {
            if (fs.existsSync(fileName)) {
                fs.unlink(fileName, function (err) {
                    if (err != null) {
                        throw err;
                    }
                });
            }
        });
    }
};
module.exports = new action();
