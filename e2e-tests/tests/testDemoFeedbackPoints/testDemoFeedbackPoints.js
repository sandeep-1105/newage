/* 
Description : This file testcases for Demo Feedback Point BRD document
*/

var loginPage = require('../../pages/pageLogin/pageLogin.js');
var demoFeedbackPointsPage = require('../../pages/pageDemoFeedbackPoints/pageDemoFeedbackPoints.js');
var loginData = require('../../testdata/dataLogin/dataLogin.json');
var globalData = require('../../testdata/dataGlobal/dataGlobal.json');
var actionLib = require('../../library/action.js');
var demoFeedbackPointsData = require('../../testdata/dataDemoFeedbackPoints/dataDemoFeedbackPoints.json');

describe('test provided demo feedback points BRD document of newage', function () {
    var reqElement;
    var labelsArray;
    var exportAgentName;
    var importAgentName;
    var customerDetails = {
        "agentName":"agentName",
        "country":"India",
        "type":"Primary",
        "address1":"Address 1",
        "address2":"Address 2",
        "city":"Delhi",
        "state":"Delhi",
        "zipCode":"111222",
        "disableCustomerType":"CS",
        "customerType":"AG",
        "iataCode":"1234"
    }

    beforeAll(function () {
        var testSpecName = "TestDemoFeedbackPoints";
        actionLib.setUrlAndLoginApp(loginPage, loginData, testSpecName);
    });

    it('test_demofeedbackpointsbrd_point1_&_point2 : should be able to verify labels on add pricing air page', function() {
      
        actionLib.verifyElementPresent(demoFeedbackPointsPage.menuCrm);
        actionLib.click(demoFeedbackPointsPage.menuCrm);
        actionLib.verifyElementPresent(demoFeedbackPointsPage.subMenuPricingAir);
        actionLib.click(demoFeedbackPointsPage.subMenuPricingAir);
        actionLib.verifyElementPresent(demoFeedbackPointsPage.btnAddPricingAir);
        actionLib.click(demoFeedbackPointsPage.btnAddPricingAir);

        labelsArray = ["Validity", "MSR", "SSR", "Cost"];
        demoFeedbackPointsPage.verifyAirPricingLabels("addGeneralOriginCharge()", labelsArray);

        labelsArray = ["Validity", "MSR", "SSR", "Cost"];
        actionLib.verifyElementPresent(demoFeedbackPointsPage.tabDestinationCharges);
        actionLib.click(demoFeedbackPointsPage.tabDestinationCharges);
        demoFeedbackPointsPage.verifyAirPricingLabels("addGeneralDestinationCharge()", labelsArray);

        actionLib.verifyElementPresent(demoFeedbackPointsPage.tabFreightCharges);
        actionLib.click(demoFeedbackPointsPage.tabFreightCharges);
        labelsArray = ["Validity", "SSR"];
        demoFeedbackPointsPage.verifyAirPricingLabels("addGeneralFreightCharge()", labelsArray);
        actionLib.verifyElementPresent(demoFeedbackPointsPage.btnFrightChargeMoreInfo);
        actionLib.click(demoFeedbackPointsPage.btnFrightChargeMoreInfo);
        labelsArray = ["SSR", "MSR", "Cost"];
        demoFeedbackPointsPage.verifyFreightChargeMoreInfoLabels(labelsArray);
        actionLib.verifyElementPresent(demoFeedbackPointsPage.btnMoreInfoCancel);
        actionLib.click(demoFeedbackPointsPage.btnMoreInfoCancel);
    });

    it('test_demofeedbackpointsbrd_point3_1 : should be able to verify that ssr is greater or equal to msr and cost on origin charge tab', function() {

        actionLib.verifyElementPresent(demoFeedbackPointsPage.menuCrm);
        actionLib.click(demoFeedbackPointsPage.menuCrm);
        actionLib.verifyElementPresent(demoFeedbackPointsPage.subMenuPricingAir);
        actionLib.click(demoFeedbackPointsPage.subMenuPricingAir);
        actionLib.verifyElementPresent(demoFeedbackPointsPage.btnAddPricingAir);
        actionLib.click(demoFeedbackPointsPage.btnAddPricingAir);
        
        actionLib.verifyElementPresent(demoFeedbackPointsPage.dropDownOrigin);
        actionLib.setText(demoFeedbackPointsPage.dropDownOrigin, demoFeedbackPointsData.origin);
        actionLib.setText(demoFeedbackPointsPage.dropDownDestination, demoFeedbackPointsData.destination);
        
        actionLib.verifyElementPresent(demoFeedbackPointsPage.dropDownOriginChargesCharge);
        actionLib.setText(demoFeedbackPointsPage.dropDownOriginChargesCharge, "Advance Filing Rules");
        actionLib.setText(demoFeedbackPointsPage.dropDownOriginChargesUnit, "Kilograms");

        actionLib.setText(demoFeedbackPointsPage.calenderOriginChargesValidityFrom, actionLib.getFutureDate(0));
        actionLib.setText(demoFeedbackPointsPage.calenderOriginChargesValidityTo, actionLib.getFutureDate(10));

        // SSR value is less than both MSR and Cost value
        actionLib.click(demoFeedbackPointsPage.tabOriginCharges);
        actionLib.verifyElementPresent(demoFeedbackPointsPage.textBoxOriginChargeMsrMinimum);
        actionLib.setText(demoFeedbackPointsPage.textBoxOriginChargeMsrMinimum, demoFeedbackPointsData.msrMinimum);
        actionLib.setText(demoFeedbackPointsPage.textBoxOriginChargeMsrAmount, demoFeedbackPointsData.msrAmount);
        actionLib.setText(demoFeedbackPointsPage.textBoxOriginChargeSsrMinimum, demoFeedbackPointsData.ssrMinimum);
        actionLib.setText(demoFeedbackPointsPage.textBoxOriginChargeSsrAmount, demoFeedbackPointsData.ssrAmountLess);
        actionLib.setText(demoFeedbackPointsPage.textBoxOriginChargeCostMinimum, demoFeedbackPointsData.costMinimum);
        actionLib.setText(demoFeedbackPointsPage.textBoxOriginChargeCostAmount, demoFeedbackPointsData.costAmount);
        actionLib.click(demoFeedbackPointsPage.btnSaveAddPricingAir);

        actionLib.click(demoFeedbackPointsPage.btnSaveAddPricingAir);
        actionLib.verifyElementPresent(demoFeedbackPointsPage.msgSsrMsrCostValidation);
        actionLib.verifyElementText(demoFeedbackPointsPage.msgSsrMsrCostValidation, "none",
                                    demoFeedbackPointsData.ssrMsrCostValidationMsg);
        
        // SSR value is less than Cost value but more than MSR value
        actionLib.setText(demoFeedbackPointsPage.textBoxOriginChargeMsrAmount, demoFeedbackPointsData.msrAmountLess);
        actionLib.click(demoFeedbackPointsPage.btnSaveAddPricingAir);
        actionLib.verifyElementPresent(demoFeedbackPointsPage.msgSsrMsrCostValidation);
        actionLib.verifyElementText(demoFeedbackPointsPage.msgSsrMsrCostValidation, "none",
                                    demoFeedbackPointsData.ssrMsrCostValidationMsg);

        // SSR value is less than MSR value but more than Cost value
        actionLib.setText(demoFeedbackPointsPage.textBoxOriginChargeMsrAmount, demoFeedbackPointsData.msrAmount);
        actionLib.setText(demoFeedbackPointsPage.textBoxOriginChargeCostAmount, demoFeedbackPointsData.costAmountLess);       
        actionLib.click(demoFeedbackPointsPage.btnSaveAddPricingAir);
        actionLib.verifyElementPresent(demoFeedbackPointsPage.msgSsrMsrCostValidation);
        actionLib.verifyElementText(demoFeedbackPointsPage.msgSsrMsrCostValidation, "none",
                                    demoFeedbackPointsData.ssrMsrCostValidationMsg);
        actionLib.setText(demoFeedbackPointsPage.textBoxOriginChargeSsrAmount, demoFeedbackPointsData.ssrAmount);
        actionLib.setText(demoFeedbackPointsPage.textBoxOriginChargeCostAmount, demoFeedbackPointsData.costAmount);
    });

    it('test_demofeedbackpointsbrd_point3_2 : should be able to verify that ssr is greater or equal to msr and cost on destination charge tab', function() {

        // SSR value is less than both MSR and Cost value
        actionLib.verifyElementPresent(demoFeedbackPointsPage.tabDestinationCharges);
        actionLib.click(demoFeedbackPointsPage.tabDestinationCharges);
        actionLib.verifyElementPresent(demoFeedbackPointsPage.textBoxDestChargeMsrMinimum);
        actionLib.setText(demoFeedbackPointsPage.textBoxDestChargeMsrMinimum, demoFeedbackPointsData.msrMinimum);
        actionLib.setText(demoFeedbackPointsPage.textBoxDestChargeMsrAmount, demoFeedbackPointsData.msrAmount);
        actionLib.setText(demoFeedbackPointsPage.textBoxDestChargeSsrMinimum, demoFeedbackPointsData.ssrMinimum);
        actionLib.setText(demoFeedbackPointsPage.textBoxDestChargeSsrAmount, demoFeedbackPointsData.ssrAmountLess);
        actionLib.setText(demoFeedbackPointsPage.textBoxDestChargeCostMinimum, demoFeedbackPointsData.costMinimum);
        actionLib.setText(demoFeedbackPointsPage.textBoxDestChargeCostAmount, demoFeedbackPointsData.costAmount);
        actionLib.click(demoFeedbackPointsPage.btnSaveAddPricingAir);

        actionLib.click(demoFeedbackPointsPage.btnSaveAddPricingAir);
        actionLib.verifyElementPresent(demoFeedbackPointsPage.msgSsrMsrCostValidation);
        actionLib.verifyElementText(demoFeedbackPointsPage.msgSsrMsrCostValidation, "none",
                                    demoFeedbackPointsData.ssrMsrCostValidationMsg);
        
        // SSR value is less than Cost value but more than MSR value
        actionLib.setText(demoFeedbackPointsPage.textBoxDestChargeMsrAmount, demoFeedbackPointsData.msrAmountLess);
        actionLib.click(demoFeedbackPointsPage.btnSaveAddPricingAir);
        actionLib.verifyElementPresent(demoFeedbackPointsPage.msgSsrMsrCostValidation);
        actionLib.verifyElementText(demoFeedbackPointsPage.msgSsrMsrCostValidation, "none",
                                    demoFeedbackPointsData.ssrMsrCostValidationMsg);

        // SSR value is less than MSR value but more than Cost value
        actionLib.setText(demoFeedbackPointsPage.textBoxDestChargeMsrAmount, demoFeedbackPointsData.msrAmount);
        actionLib.setText(demoFeedbackPointsPage.textBoxDestChargeCostAmount, demoFeedbackPointsData.costAmountLess);       
        actionLib.click(demoFeedbackPointsPage.btnSaveAddPricingAir);
        actionLib.verifyElementPresent(demoFeedbackPointsPage.msgSsrMsrCostValidation);
        actionLib.verifyElementText(demoFeedbackPointsPage.msgSsrMsrCostValidation, "none",
                                    demoFeedbackPointsData.ssrMsrCostValidationMsg);
    });

    // it('test_demofeedbackpointsbrd_point4a : currency against charge should display as per origin on Origin charges tab', function() {

    //     actionLib.verifyElementPresent(demoFeedbackPointsPage.dropDownOrigin);
    //     actionLib.setText(demoFeedbackPointsPage.dropDownOrigin, demoFeedbackPointsData.originNewYork);
    //     actionLib.click(demoFeedbackPointsPage.activeElementLocator);
    //     actionLib.verifyElementPresent(demoFeedbackPointsPage.tabOriginCharges);
    //     actionLib.click(demoFeedbackPointsPage.tabOriginCharges);
    //     actionLib.verifyElementPresent(demoFeedbackPointsPage.dropDownOriginChargesCharge); 
    //     actionLib.setText(demoFeedbackPointsPage.dropDownOriginChargesCharge, demoFeedbackPointsData.originCharge);
    //     actionLib.setText(demoFeedbackPointsPage.dropDownOriginChargesUnit, demoFeedbackPointsData.originUnit);
    //     actionLib.setText(demoFeedbackPointsPage.dropDownOriginChargesCurrency, "INR");
    //     reqElement = actionLib.getAttributeValue(demoFeedbackPointsPage.dropDownOriginChargesCurrency, 'value');
    //     reqElement.then(function(val){
    //         expect(val).toEqual(demoFeedbackPointsData.currencyNewYork);
    //     });
    // });

    // it('test_demofeedbackpointsbrd_point4b : currency against charge should display as per destination on destination charges tab', function() {

    //     actionLib.verifyElementPresent(demoFeedbackPointsPage.dropDownDestination);
    //     actionLib.setText(demoFeedbackPointsPage.dropDownDestination, demoFeedbackPointsData.destinationDubai);
    //     actionLib.verifyElementPresent(demoFeedbackPointsPage.tabDestinationCharges);
    //     actionLib.click(demoFeedbackPointsPage.tabDestinationCharges);
    //     actionLib.verifyElementPresent(demoFeedbackPointsPage.dropDownDestChargesCharge); 
    //     actionLib.setText(demoFeedbackPointsPage.dropDownDestChargesCharge, demoFeedbackPointsData.destinationCharge);
    //     actionLib.setText(demoFeedbackPointsPage.dropDownDestChargesUnit, demoFeedbackPointsData.destinationUnit);
    //     actionLib.setText(demoFeedbackPointsPage.dropDownDestChargesCurrency, "INR");
    //     reqElement = actionLib.getAttributeValue(demoFeedbackPointsPage.dropDownDestChargesCurrency, 'value');
    //     reqElement.then(function(val){
    //         expect(val).toEqual(demoFeedbackPointsData.currencyDubai);
    //     });
    // });

    // it('test_demofeedbackpointsbrd_point4c : currency against charge should display as per origin/destination on freight charges tab', function() {

    //     actionLib.verifyElementPresent(demoFeedbackPointsPage.dropDownDestination);
    //     actionLib.setText(demoFeedbackPointsPage.dropDownOrigin, demoFeedbackPointsData.originNewYork);
    //     actionLib.click(demoFeedbackPointsPage.activeElementLocator);
    //     actionLib.setText(demoFeedbackPointsPage.dropDownDestination, demoFeedbackPointsData.destinationDubai);
    //     actionLib.verifyElementPresent(demoFeedbackPointsPage.tabFreightCharges);
    //     actionLib.click(demoFeedbackPointsPage.tabFreightCharges);

    //     actionLib.verifyElementPresent(demoFeedbackPointsPage.dropDownFreightChargesCharge); 
    //     actionLib.setText(demoFeedbackPointsPage.dropDownFreightChargesCharge, demoFeedbackPointsData.freightCharge);
    //     actionLib.setText(demoFeedbackPointsPage.dropDownFreightChargesCurrency, "INR");
    //     reqElement = actionLib.getAttributeValue(demoFeedbackPointsPage.dropDownFreightChargesCurrency, 'value');
    //     reqElement.then(function(val){
    //         expect(val).toEqual(demoFeedbackPointsData.currencyDubai);
    //     });
    // });

    // it('test_demofeedbackpointsbrd_point19_1 : verify that count of records display in all the tabs on enquiry page', function() {

    //     actionLib.verifyElementPresent(actionLib.menuSales);
    //     actionLib.click(actionLib.menuSales);
    //     actionLib.verifyElementPresent(actionLib.subMenuEnquiry);
    //     actionLib.click(actionLib.subMenuEnquiry);
    //     actionLib.verifyElementPresent(demoFeedbackPointsPage.linkActiveEnquiry);
    //     demoFeedbackPointsPage.verifyLinksCount(demoFeedbackPointsPage.textRecordsCount,
    //                                             demoFeedbackPointsPage.linkActiveEnquiry);
    //     actionLib.verifyElementPresent(demoFeedbackPointsPage.linkQuoteCreatedEnquiry);
    //     actionLib.click(demoFeedbackPointsPage.linkQuoteCreatedEnquiry);
    //     demoFeedbackPointsPage.verifyLinksCount(demoFeedbackPointsPage.textRecordsCount,
    //                                             demoFeedbackPointsPage.linkQuoteCreatedEnquiry);
    //     actionLib.verifyElementPresent(demoFeedbackPointsPage.linkAllEnquiry);
    //     actionLib.click(demoFeedbackPointsPage.linkAllEnquiry);
    //     demoFeedbackPointsPage.verifyLinksCount(demoFeedbackPointsPage.textRecordsCount,
    //                                             demoFeedbackPointsPage.linkAllEnquiry);
    // });

    // it('test_demofeedbackpointsbrd_point19_2 : verify that count of records display in all the tabs on quotation page', function() {

    //     actionLib.verifyElementPresent(actionLib.menuSales);
    //     actionLib.click(actionLib.menuSales);
    //     actionLib.verifyElementPresent(actionLib.subMenuQuotation);
    //     actionLib.click(actionLib.subMenuQuotation);
    //     actionLib.verifyElementPresent(demoFeedbackPointsPage.linkWaitingForManagerApproval);
    //     demoFeedbackPointsPage.verifyLinksCount(demoFeedbackPointsPage.textRecordsCount,
    //                                             demoFeedbackPointsPage.linkWaitingForManagerApproval);
    //     actionLib.verifyElementPresent(demoFeedbackPointsPage.linkWaitingForCustomerApproval);
    //     actionLib.click(demoFeedbackPointsPage.linkWaitingForCustomerApproval);
    //     demoFeedbackPointsPage.verifyLinksCount(demoFeedbackPointsPage.textRecordsCount,
    //                                             demoFeedbackPointsPage.linkWaitingForCustomerApproval);
    //     actionLib.verifyElementPresent(demoFeedbackPointsPage.linkReadyForShipment);
    //     actionLib.click(demoFeedbackPointsPage.linkReadyForShipment);
    //     demoFeedbackPointsPage.verifyLinksCount(demoFeedbackPointsPage.textRecordsCount,
    //                                             demoFeedbackPointsPage.linkReadyForShipment);
    //     actionLib.verifyElementPresent(demoFeedbackPointsPage.linkRejectedQuotation);
    //     actionLib.click(demoFeedbackPointsPage.linkRejectedQuotation);
    //     demoFeedbackPointsPage.verifyLinksCount(demoFeedbackPointsPage.textRecordsCount,
    //                                             demoFeedbackPointsPage.linkRejectedQuotation);
    //     actionLib.verifyElementPresent(demoFeedbackPointsPage.linkAllQuotation);
    //     actionLib.click(demoFeedbackPointsPage.linkAllQuotation);                                                
    //     demoFeedbackPointsPage.verifyLinksCount(demoFeedbackPointsPage.textRecordsCount,
    //                                             demoFeedbackPointsPage.linkAllQuotation);
    // });

    // it('test_demofeedbackpointsbrd_point19_3 : verify that count of records display in all the tabs on shipment page', function () {

    //     actionLib.verifyElementPresent(actionLib.menuCrm);
    //     actionLib.click(actionLib.menuCrm);
    //     actionLib.verifyElementPresent(actionLib.subMenuShipment);
    //     actionLib.click(actionLib.subMenuShipment);

    //     var subTabsButtons = {
    //         "btnExportShipments":demoFeedbackPointsPage.btnExportShipments,
    //         "btnImportShipments":demoFeedbackPointsPage.btnImportShipments,
    //         "btnBothShipments":demoFeedbackPointsPage.btnBothShipments
    //     }

    //     demoFeedbackPointsPage.matchRecordsAndLinkCount(subTabsButtons,
    //         demoFeedbackPointsPage.textRecordsCount, demoFeedbackPointsPage.linkBookedShipment);
    //     demoFeedbackPointsPage.matchRecordsAndLinkCount(subTabsButtons,
    //         demoFeedbackPointsPage.textRecordsCount, demoFeedbackPointsPage.linkReceivedShipment);
    //     demoFeedbackPointsPage.matchRecordsAndLinkCount(subTabsButtons,
    //         demoFeedbackPointsPage.textRecordsCount, demoFeedbackPointsPage.linkGeneratedShipment);
    //     demoFeedbackPointsPage.matchRecordsAndLinkCount(subTabsButtons,
    //         demoFeedbackPointsPage.textRecordsCount, demoFeedbackPointsPage.linkClosedShipment);
    //     demoFeedbackPointsPage.matchRecordsAndLinkCount(subTabsButtons,
    //         demoFeedbackPointsPage.textRecordsCount, demoFeedbackPointsPage.linkCancelledShipment);
    //     demoFeedbackPointsPage.matchRecordsAndLinkCount(subTabsButtons,
    //         demoFeedbackPointsPage.textRecordsCount, demoFeedbackPointsPage.linkAllShipment);
    // });

    // it('test_demofeedbackpointsbrd_point19_4 : verify that count of records display in all the tabs on console page', function () {

    //     actionLib.verifyElementPresent(actionLib.menuAir);
    //     actionLib.click(actionLib.menuAir);
    //     actionLib.verifyElementPresent(actionLib.subMenuMasterShipment);
    //     actionLib.click(actionLib.subMenuMasterShipment);

    //     var subTabsButtons = {
    //         "btnExportShipments":demoFeedbackPointsPage.btnExportShipments,
    //         "btnImportShipments":demoFeedbackPointsPage.btnImportShipments,
    //         "btnBothShipments":demoFeedbackPointsPage.btnBothShipments
    //     }

    //     demoFeedbackPointsPage.matchRecordsAndLinkCount(subTabsButtons,
    //         demoFeedbackPointsPage.textRecordsCount, demoFeedbackPointsPage.linkActiveMasterShipments);
    //     demoFeedbackPointsPage.matchRecordsAndLinkCount(subTabsButtons,
    //         demoFeedbackPointsPage.textRecordsCount, demoFeedbackPointsPage.linkClosedMasterShipments);
    //     demoFeedbackPointsPage.matchRecordsAndLinkCount(subTabsButtons,
    //         demoFeedbackPointsPage.textRecordsCount, demoFeedbackPointsPage.linkCancelledMasterShipments);
    //     demoFeedbackPointsPage.matchRecordsAndLinkCount(subTabsButtons,
    //         demoFeedbackPointsPage.textRecordsCount, demoFeedbackPointsPage.linkAllMasterShipments);
    // });

    // it('test_demofeedbackpointsbrd_point8 : verify that vendor in rate request display as per export destination', function () {
    //     // agent for export destination
    //     exportAgentName = "exportAgentName" + actionLib.getTodayTime();
    //     // customerDetails.agentName = exportAgentName;
    //     // customerDetails.iataCode = "1" + actionLib.getTodayTime();
    //     // demoFeedbackPointsPage.addAgentCustomer(customerDetails);
    //     // agent for import destination
    //     importAgentName = "importAgentName" + actionLib.getTodayTime(); 
    //     // customerDetails.agentName = importAgentName;
    //     // customerDetails.iataCode = "2" + actionLib.getTodayTime();
    //     // demoFeedbackPointsPage.addAgentCustomer(customerDetails);

    //     actionLib.verifyElementPresent(actionLib.menuSetup);
    //     actionLib.click(actionLib.menuSetup);
    //     actionLib.verifyElementPresent(actionLib.subMenuAppConfiguration);
    //     actionLib.click(actionLib.subMenuAppConfiguration);
    //     actionLib.verifyElementPresent(demoFeedbackPointsPage.linkAgentPort);
    //     actionLib.click(demoFeedbackPointsPage.linkAgentPort);

    //     var agentPortDetails = {
    //         "agentPortService":"Air Export",
    //         "agentName":exportAgentName,
    //         "agentPort":"Neyveli"
    //     }
    //     demoFeedbackPointsPage.addAgentPort(agentPortDetails);
        
    //     agentPortDetails.agentPortService="Air Import",
    //     agentPortDetails.agentName=importAgentName,
    //     agentPortDetails.agentPort="Aalborg"
        
    //     demoFeedbackPointsPage.addAgentPort(agentPortDetails);
    // });

    afterAll(function () {
        console.log('Logout action called.....');
        loginPage.appLogout();
        console.log('Logout action finished.....');
    });
});