var loginPage = require('../../../pages/pageLogin/pageLogin.js');
var shipmentServiceRoutingPage = require('../../../pages/pageAirExport/pageShipment/pageShipmentServiceRouting.js');
var pickUpPage = require('../../../pages/pageAirExport/pagePickUp/pagePickUp.js');
var deliveryToCfsPage = require('../../../pages/pageAirExport/pageDeliveryToCfs/pageDeliveryToCfs.js');
var loginData = require('../../../testdata/dataLogin/dataLogin.json');
var globalData = require('../../../testdata/dataGlobal/dataGlobal.json');
var actionLib = require('../../../library/action.js');
var enquiryData = require('../../../testdata/dataAirExport/dataEnquiry/dataEnquiry.json');
var shipmentHawbData = require('../../../testdata/dataAirExport/dataShipment/dataShipmentHawb.json');
var pickUpData = require('../../../testdata/dataAirExport/dataPickUp/dataPickUp.json');
var deliveryToCfsData = require('../../../testdata/dataAirExport/dataDeliveryToCfs/dataDeliveryToCfs.json');

describe('test export add delivery to cfs functionality of newage', function() {
    var strExpectedDeliveryDate;
    var strDeliveryDate;
    var strExpectedDoorDeliveryDate;
    var strDoorDeliveryDate;
    var deliveryToCfsVariables;

    beforeAll(function(){
	    var testSpecName = "TestAddDeliveryToCfs";
        actionLib.setUrlAndLoginApp(loginPage, loginData, testSpecName);
    });
    
    it('test_addexportdeliverytocfs_1 : should be able to navigate to add delivery to cfs page', function() {

        var cfsDetails = {
            "cfsName":"cfsPoint",
            "cfsCode":"CFF",
            "cfsPort":"Chennai"
        }
        deliveryToCfsPage.addDeliveryToCfsPoint(cfsDetails);
        actionLib.verifyElementPresent(deliveryToCfsPage.menuTasks);
        actionLib.click(deliveryToCfsPage.menuTasks);
        actionLib.verifyElementPresent(deliveryToCfsPage.subMenuMyTasks);
        actionLib.click(deliveryToCfsPage.subMenuMyTasks);
    });

    it('test_addexportdeliverytocfs_2 : should be able to navigate to add delivery to cfs page', function() {
        
        actionLib.verifyElementPresent(deliveryToCfsPage.linkDeliveryToCfs);
        actionLib.click(deliveryToCfsPage.linkDeliveryToCfs);
        actionLib.fillTextInTableColumn(deliveryToCfsData.deliveryToCfsTableDataXpathTag, 2, 2, 
                                        globalData.customerName);
        actionLib.clickRowInAirExportTable(globalData.customerName, enquiryData.origin);
    });
    
    it('test_addexportdeliverytocfs_3 : should be able to add delivery to cfs data', function() {

        strExpectedDeliveryDate = actionLib.getTodayDate();
        strDeliveryDate = actionLib.getTodayDate();
        deliveryToCfsVariables = {
            "deliveryPoint":"addDeliveryPoint",
            "expectedDeliveryDate":strExpectedDeliveryDate,
            "deliveryDate":strDeliveryDate
        }
        actionLib.verifyElementPresent(deliveryToCfsPage.btnEdit);
        actionLib.click(deliveryToCfsPage.btnEdit);
        actionLib.verifyElementPresent(pickUpPage.subMenuPickUpDelivery);
        actionLib.click(pickUpPage.subMenuPickUpDelivery);
        
        actionLib.click(deliveryToCfsPage.expandDeliveryToCfs);
        deliveryToCfsPage.addDeliveryToCfs(deliveryToCfsData, deliveryToCfsVariables);
        actionLib.click(deliveryToCfsPage.expandDeliveryToCfs);
    });

    it('test_addexportdeliverytocfs_4 : should be able to save delivery to cfs successfully', function() {

        actionLib.verifyElementPresent(deliveryToCfsPage.btnUpdate);
        actionLib.click(deliveryToCfsPage.btnUpdate);
        actionLib.verifyElementPresent(deliveryToCfsPage.btnPopUpOk);
        actionLib.click(deliveryToCfsPage.btnPopUpOk);

        actionLib.click(actionLib.menuTasks);
        actionLib.verifyElementPresent(actionLib.subMenuMyTasks);
        actionLib.click(actionLib.subMenuMyTasks);

        actionLib.click(deliveryToCfsPage.linkDeliveryToCfs);
        actionLib.fillTextInTableColumn(deliveryToCfsData.deliveryToCfsTableDataXpathTag, 2, 2, 
                                        globalData.customerName);
        actionLib.verifySearchTextInTableRowCol(deliveryToCfsData.deliveryToCfsTableDataXpathTag, 1, 2, 
                                                globalData.customerName);
    });
	
    afterAll(function(){
        console.log('Logout action called.....');
        loginPage.appLogout();
        console.log('Logout action finished.....');
    });
});

