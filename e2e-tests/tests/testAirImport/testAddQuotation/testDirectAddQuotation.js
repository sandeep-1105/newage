var loginPage = require('../../../pages/pageLogin/pageLogin.js');
var quotationPage = require('../../../pages/pageAirImport/pageQuotation/pageQuotation.js');
var loginData = require('../../../testdata/dataLogin/dataLogin.json');
var globalData = require('../../../testdata/dataGlobal/dataGlobal.json');
var actionLib = require('../../../library/action.js');
var customerCommonElementPage = require('../../../pages/pageAddCustomer/pageAddCustomer.js');
var customerData = require('../../../testdata/dataAddCustomer/dataCustomer.json');
var customeAddress = require('../../../pages/pageAddCustomer/pageAddCustomerAddress.js');
var enquiryPage = require('../../../pages/pageGlobal/pageGlobal.js');
var enquiryData = require('../../../testdata/dataGlobal/quotationGlobalData.json');;

describe('test import direct add quotation functionality of newage', function () {
    var strEnquiryReceived;
    var strQuoteByDate;
    var attachmentUpload = "../testdata/dataFiles/agentTemplate.xlsx";
    var directCustomerName;

    beforeAll(function () {
        var testSpecName = "Import Direct Add Quotation";
        actionLib.setUrlAndLoginApp(loginPage, loginData, testSpecName);
    });

    it('test_directAddimportquotation_1 : should be able to navigate to add quotation page from Direct link', function () {

        actionLib.verifyElementPresent(enquiryPage.directQuotationLink);
        //----second attribute value none - get the quotation count on the my task page        
        actionLib.click(enquiryPage.directQuotationLink);
        directCustomerName = "AddCustomer" + actionLib.getTodayTime();
        actionLib.clickAddCustomerTable(directCustomerName);
        customerCommonElementPage.addCustomer(directCustomerName, "none", customerData.country)
        actionLib.setText(customeAddress.textBoxAddress1, "India");
        actionLib.setText(customeAddress.textBoxAddress2, "India");
        actionLib.setText(customeAddress.dropDownCity, "delhi");
        actionLib.setText(customeAddress.textBoxZipCode, "1100011");
        actionLib.scrollToElement(customeAddress.dropDownStateProvince);
        actionLib.setText(customeAddress.dropDownStateProvince, "delhi");
        actionLib.verifyElementPresent(customerCommonElementPage.btnSave);
        actionLib.click(customerCommonElementPage.btnSave);
        actionLib.verifyElementPresent(customerCommonElementPage.btnPopUpOk);
        actionLib.click(customerCommonElementPage.btnPopUpOk);

    });

    it('test_addDirectimportquotation_2 : should be able to add data on quotation page', function () {
        var serviceName = "Import";
        actionLib.verifyElementPresent(enquiryPage.dropDownCustomer);
        enquiryPage.addDirectQuotation(directCustomerName, enquiryData, "yes",
            attachmentUpload, "39", serviceName);

        actionLib.click(quotationPage.btnSubmit);
    });

    it('test_addDirectimportquotation_3 : should be able to add data on quotation charge carrier section', function () {

        actionLib.click(enquiryPage.expandCarrierCharge);
        actionLib.verifyElementPresent(enquiryPage.dropDownCarrier);
        enquiryPage.addDirectQuotationCarrierCharge(enquiryData);
    });

    it('test_addDirectimportQuotationDimension_4 : should be able to add Quotation dimensions section', function () {
        actionLib.click(enquiryPage.expandDimensionsSection);
        actionLib.verifyElementPresent(enquiryPage.textBoxNoOfPiecesQuotation);
        enquiryPage.addDirectQuotationDimensions(enquiryData);
        actionLib.click(enquiryPage.expandDimensionsSection);
    });

    it('test_addDirectimportQuotation_5 : should be able to add Quotation pick up / delivery section', function () {
        actionLib.click(enquiryPage.expandPickUpDeliverySectionQuotation);
        actionLib.verifyElementPresent(enquiryPage.textBoxPickUpAddress1Quotation);
        enquiryPage.addDirectImportQuotationPickUpDelivery(enquiryData);
        actionLib.click(enquiryPage.expandPickUpDeliverySectionQuotation);
    });
    it('test_addDirectImportquotation_6 : should be able to add data on quotation air notes section', function () {

        actionLib.verifyElementPresent(quotationPage.expandAirNotes);
        actionLib.click(quotationPage.expandAirNotes);
        actionLib.verifyElementPresent(quotationPage.textAreaNotes);
        quotationPage.addQuotationAirNotes(enquiryData.notes);
        actionLib.click(quotationPage.expandAirNotes);
    });

    it('test_addDirectImportquotation_7 : should be able to save quotation successfully', function () {

        actionLib.verifyElementPresent(quotationPage.btnSave);
        actionLib.click(quotationPage.btnSave);
        actionLib.verifyElementPresent(quotationPage.btnPopUpOk);
        actionLib.click(quotationPage.btnPopUpOk);

        actionLib.click(actionLib.menuTasks);
        actionLib.verifyElementPresent(actionLib.subMenuMyTasks);
        actionLib.click(actionLib.subMenuMyTasks);

    });


    afterAll(function () {
        console.log('Logout action called.....');
        loginPage.appLogout();
        console.log('Logout action finished.....');
    });
});

