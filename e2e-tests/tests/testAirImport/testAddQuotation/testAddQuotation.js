var loginPage = require('../../../pages/pageLogin/pageLogin.js');
var enquiryPage = require('../../../pages/pageAirImport/pageEnquiry/pageEnquiry.js');
var quotationPage = require('../../../pages/pageAirImport/pageQuotation/pageQuotation.js');
var loginData = require('../../../testdata/dataLogin/dataLogin.json');
var globalData = require('../../../testdata/dataGlobal/dataGlobal.json');
var actionLib = require('../../../library/action.js');
var enquiryData = require('../../../testdata/dataAirImport/dataEnquiry/dataEnquiry.json');
var quotationData = require('../../../testdata/dataAirImport/dataQuotation/dataQuotation.json');

describe('test import add quotation functionality of newage', function() {
    var strEnquiryReceived;
    var strQuoteByDate;
    var attachmentUpload = "../../testdata/dataFiles/agentTemplate.xlsx";

    beforeAll(function(){
        var testSpecName = "Import TestAddQuotation";
        actionLib.setUrlAndLoginApp(loginPage, loginData, testSpecName);
    });
	
    it('test_addimportquotation_1 : should be able to navigate to add quotation page', function() {
        
        actionLib.verifyElementPresent(quotationPage.linkQuotation);
        //----second attribute value none - get the quotation count on the my task page        
        actionLib.verifySplitCountOfLocator(quotationPage.linkQuotation, "none");        
        
        actionLib.verifyElementPresent(enquiryPage.linkEnquiry);
        actionLib.click(enquiryPage.linkEnquiry);
        actionLib.fillTextInTableColumn(enquiryData.enquiryTableDataXpathTag, 2, 2, globalData.importCustomerName);
        expect(element(by.xpath("//input[@data-id='getEnquiryHeadArray_Import_customerName']")).getAttribute('data-id')).toEqual(browser.driver.switchTo().activeElement().getAttribute('data-id'));
        actionLib.clickRowInAirExportTable(globalData.importCustomerName, enquiryData.origin);
        actionLib.verifyElementPresent(quotationPage.btnCreateQuotation);
    });
	
    it('test_addimportquotation_2 : should be able to add data on quotation page', function() {

        actionLib.click(quotationPage.btnCreateQuotation);
        actionLib.verifyElementPresent(quotationPage.dropDownCustomer);
        quotationPage.addQuotation(
                                globalData.importCustomerName,
                                quotationData.referenceNo,
                                "yes",
                                globalData.importCustomerName,
                                quotationData.attention ,
                                quotationData.generalNotes);

        actionLib.click(quotationPage.btnSubmit);
    });

    it('test_addimportquotation_3 : should be able to add data on quotation charge carrier section', function() {
    
        actionLib.click(quotationPage.expandCarrierCharge);
        actionLib.verifyElementPresent(quotationPage.dropDownCarrier);
        quotationPage.addQuotationCarrierCharge(quotationData.carrier, quotationData.transitDays, 
            quotationData.frequency, quotationData.amtPerUnit, quotationData.minAmount);
        actionLib.verifyElementPresent(quotationPage.dropDownCarrier);
        actionLib.click(quotationPage.expandCarrierCharge);
    });
    
    it('test_addimportquotation_4 : should be able to add data on quotation air notes section', function() {
	
        actionLib.verifyElementPresent(quotationPage.expandAirNotes);
        actionLib.click(quotationPage.expandAirNotes);
        actionLib.verifyElementPresent(quotationPage.textAreaNotes);
        quotationPage.addQuotationAirNotes(quotationData.notes);
        actionLib.click(quotationPage.expandAirNotes);
    });

    it('test_addimportquotation_5 : should be able to save quotation successfully', function() {
        
        actionLib.verifyElementPresent(quotationPage.btnSave);
        actionLib.click(quotationPage.btnSave);
        actionLib.verifyElementPresent(quotationPage.btnPopUpOk);
        actionLib.click(quotationPage.btnPopUpOk);

        actionLib.click(actionLib.menuTasks);
        actionLib.verifyElementPresent(actionLib.subMenuMyTasks);
        actionLib.click(actionLib.subMenuMyTasks);

        actionLib.verifyElementPresent(quotationPage.linkQuotation);
        //--second attribute value other than none - get the quotation count on the my task page
        // and verify it is 1 more than the previous shipment count.
        actionLib.verifySplitCountOfLocator(quotationPage.linkQuotation, "Yes");
    });

    it('test_addimportquotation_6 : should be able to approve quotation', function() {
        
        actionLib.click(quotationPage.linkQuotation);
        actionLib.fillTextInTableColumn(quotationData.quotationTableDataXpathTag, 2, 2, globalData.importCustomerName);
        actionLib.verifySearchTextInTableRowCol(quotationData.quotationTableDataXpathTag, 1, 2, globalData.importCustomerName);
        actionLib.clickRowInAirExportTable(globalData.importCustomerName, enquiryData.origin);
        actionLib.verifyElementPresent(quotationPage.btnApprove);
        actionLib.click(quotationPage.btnApprove);
        actionLib.verifyElementPresent(quotationPage.btnApproveYes);
        actionLib.click(quotationPage.btnApproveYes);
        actionLib.verifyElementPresent(quotationPage.imageApprovedQuotation);
    });

    afterAll(function(){
        console.log('Logout action called.....');
        loginPage.appLogout();
        console.log('Logout action finished.....');
    });
});

