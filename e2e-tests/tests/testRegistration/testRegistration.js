var companyRegistrationPage = require('../../pages/pageRegistration/pageCompanyRegistration.js');
var loginCreationRegistrationPage = require('../../pages/pageRegistration/pageLoginCreationRegistration.js');
var configurationRegistrationPage = require('../../pages/pageRegistration/pageConfigurationRegistration.js');
var customerAgentRegistrationPage = require('../../pages/pageRegistration/pageCustAgentUploadRegistration.js');
var financeSetupRegistrationPage = require('../../pages/pageRegistration/pageFinanceSetupRegistration.js');
var chargeSetupRegistrationPage = require('../../pages/pageRegistration/pageChargeSetupRegistration.js');
var othersRegistrationPage = require('../../pages/pageRegistration/pageOthersRegistration.js');
var companyData = require('../../testdata/dataRegistration/dataCompanyRegistration.json');
var loginCreationData = require('../../testdata/dataRegistration/dataLoginCreationRegistration.json');
var configurationData = require('../../testdata/dataRegistration/dataConfigurationRegistration.json');
var financeSetupData = require('../../testdata/dataRegistration/dataFinanceSetupRegistration.json');
var chargeSetupData = require('../../testdata/dataRegistration/dataChargeSetupRegistration.json');
var othersTabData = require('../../testdata/dataRegistration/dataOthersRegistration.json');
var globalData = require('../../testdata/dataGlobal/dataGlobal.json');
var actionLib = require('../../library/action.js');
var loginPage = require('../../pages/pageLogin/pageLogin.js');

describe('test signup functionality of newage', function() {
  
    var fileUploadLogoPath = '../testdata/dataFiles/companyLogo.jpg';
    var fileUploadCustomerPath = '../testdata/dataFiles/customerTemplate.xlsx';
    var fileUploadAgentPath = '../testdata/dataFiles/agentTemplate.xlsx';
    var fileUploadChargeSetupPath = '../testdata/dataFiles/charge_template.xlsx';
    var strCompanyNameData;
    var strUserName;
    var reqElement;

    beforeAll(function(){
        var testSpecName = "TestRegistration";
        actionLib.setApplicationUrl(loginPage, testSpecName);
    });

    it('test_signup_1 : Clicking back on  Others tab should take to Charge Setup', function() {

        actionLib.browserRefresh();
        actionLib.verifyElementPresent(companyRegistrationPage.linkSignUp);
        actionLib.click(companyRegistrationPage.linkSignUp);

        strCompanyNameData = companyData.companyName + actionLib.getTodayTime();
        companyRegistrationPage.signUpCompanyTab(companyData.salutation, companyData.firstName, 
            companyData.lastName, companyData.email, companyData.countryName, companyData.phoneNumber,
            companyData.mobileNumber, strCompanyNameData, companyData.address1, companyData.address2,
            companyData.address3, companyData.area, companyData.cityName, companyData.stateProvince,
            companyData.timeZone, companyData.dateFormat, companyData.zipCode, fileUploadLogoPath);
        actionLib.verifyElementPresent(companyRegistrationPage.iconLoginCreation);
        strUserName = companyData.firstName + companyData.lastName ;
        //when want to click on "check all" link under "Select service" dropdown then pass any string 
        //other than "none" in fifth attribute below
        loginCreationRegistrationPage.signUpLoginCreationTab(loginCreationData.position, strUserName, 
            loginCreationData.password, loginCreationData.confirmPassword, "selectAllSelectService");
        actionLib.verifyElementPresent(loginCreationRegistrationPage.iconConfiguration);



        //when want to click uiSwitch then pass any string other than "none" in third attribute below
        configurationRegistrationPage.signUpConfigurationTab(configurationData.currency, configurationData.language,
            "Yes", configurationRegistrationPage.getCurrentMonthYear());
        actionLib.verifyElementPresent(configurationRegistrationPage.iconCustomerAgent);

        actionLib.click(customerAgentRegistrationPage.buttonNext);
        actionLib.verifyElementPresent(customerAgentRegistrationPage.iconFinanceSetup);

        actionLib.click(financeSetupRegistrationPage.buttonNext);
        actionLib.verifyElementPresent(financeSetupRegistrationPage.iconChargeSetup);

        actionLib.click(chargeSetupRegistrationPage.buttonNext);
        actionLib.verifyElementPresent(chargeSetupRegistrationPage.iconOthers);


        actionLib.verifyElementPresent(othersRegistrationPage.buttonBack);
        actionLib.click(othersRegistrationPage.buttonBack);
        actionLib.verifyElementPresent(financeSetupRegistrationPage.iconChargeSetup);

        actionLib.explicitWait(3000);


    });

    it('test_signup_2 : Clicking back on  Charge Setup tab should take to Finance Setup', function() {
        
        actionLib.verifyElementPresent(chargeSetupRegistrationPage.buttonBack);
        actionLib.click(chargeSetupRegistrationPage.buttonBack);
        actionLib.verifyElementPresent(customerAgentRegistrationPage.iconFinanceSetup);
        actionLib.explicitWait(3000);


    });

    it('test_signup_3 : Clicking back on  Finance Setup tab should take to CustomerAgent Setup', function() {
        
        actionLib.verifyElementPresent(financeSetupRegistrationPage.buttonBack);
        actionLib.click(financeSetupRegistrationPage.buttonBack);
        actionLib.verifyElementPresent(configurationRegistrationPage.iconCustomerAgent);
        actionLib.explicitWait(3000);
    });

    it('test_signup_4 : Clicking back on  CustomerAgent Setup tab should take to Configuration Setup', function() {
        
        actionLib.verifyElementPresent(customerAgentRegistrationPage.buttonBack);
        actionLib.click(customerAgentRegistrationPage.buttonBack);
        actionLib.verifyElementPresent(loginCreationRegistrationPage.iconConfiguration);
        actionLib.explicitWait(3000);
    });

    it('test_signup_5 : Clicking back on  Configuration Setup tab should take to Login Setup', function() {
        
        actionLib.verifyElementPresent(configurationRegistrationPage.buttonBack);
        actionLib.click(configurationRegistrationPage.buttonBack);
        actionLib.verifyElementPresent(companyRegistrationPage.iconLoginCreation);
        actionLib.explicitWait(3000);
    });

    it('test_signup_6 : Clicking back on  Login Setup tab should take to Company Setup', function() {
        
        actionLib.verifyElementPresent(loginCreationRegistrationPage.buttonBack);
        actionLib.click(loginCreationRegistrationPage.buttonBack);

        actionLib.verifyElementPresent(loginCreationRegistrationPage.passwordMandate);
        
        actionLib.setText(loginCreationRegistrationPage.textBoxPassword, loginCreationData.password);
        actionLib.setText(loginCreationRegistrationPage.textBoxconfirmPassword, loginCreationData.confirmPassword);

        actionLib.explicitWait(2000);

        actionLib.click(loginCreationRegistrationPage.buttonBack);
        actionLib.verifyElementPresent(companyRegistrationPage.iconCompany);
        actionLib.explicitWait(3000);
    });



    it('test_signup_7 : should be able to provide details in company tab & navigate to next tab', function() {
    
        // actionLib.browserRefresh();
        // actionLib.verifyElementPresent(companyRegistrationPage.linkSignUp);
        // actionLib.click(companyRegistrationPage.linkSignUp);

        strCompanyNameData = companyData.companyName + actionLib.getTodayTime();
        companyRegistrationPage.signUpCompanyTab(companyData.salutation, companyData.firstName, 
            companyData.lastName, companyData.email, companyData.countryName, companyData.phoneNumber,
            companyData.mobileNumber, strCompanyNameData, companyData.address1, companyData.address2,
            companyData.address3, companyData.area, companyData.cityName, companyData.stateProvince,
            companyData.timeZone, companyData.dateFormat, companyData.zipCode, fileUploadLogoPath);
        actionLib.verifyElementPresent(companyRegistrationPage.iconLoginCreation);
    });

    it('test_signup_8 : should be able to provide details in login creation tab & navigate to next tab', 
    function() {

        strUserName = companyData.firstName + companyData.lastName ;
        //when want to click on "check all" link under "Select service" dropdown then pass any string 
        //other than "none" in fifth attribute below
        loginCreationRegistrationPage.signUpLoginCreationTab(loginCreationData.position, strUserName, 
            loginCreationData.password, loginCreationData.confirmPassword, "selectAllSelectService");
        actionLib.verifyElementPresent(loginCreationRegistrationPage.iconConfiguration);
    });

    it('test_signup_9 : should be able to provide details in configuration tab  & navigate to next tab', 
    function() {
    
        //when want to click uiSwitch then pass any string other than "none" in third attribute below
        configurationRegistrationPage.signUpConfigurationTab(configurationData.currency, configurationData.language,
            "Yes", configurationRegistrationPage.getCurrentMonthYear());
        actionLib.verifyElementPresent(configurationRegistrationPage.iconCustomerAgent);
    });

    it('test_signup_10 : should be able to provide details in customer Agent tab & navigate to next tab', 
    function() {

        customerAgentRegistrationPage.signUpCustomerAgentUploadTab(fileUploadCustomerPath, "none");
        actionLib.explicitWait(5000);
        reqElement = actionLib.getChainedElement(customerAgentRegistrationPage.msgSuccessfulFileUpload, 
            customerAgentRegistrationPage.strChildLocator);
        actionLib.verifyElementText("none", reqElement, "Customer data validated successfully");
        customerAgentRegistrationPage.signUpCustomerAgentUploadTab("none", fileUploadAgentPath);
        actionLib.explicitWait(5000);
        reqElement = actionLib.getChainedElement(customerAgentRegistrationPage.msgSuccessfulFileUpload,
            customerAgentRegistrationPage.strChildLocator);
        actionLib.verifyElementText("none", reqElement, "Agent data validated successfully");
        actionLib.click(customerAgentRegistrationPage.buttonNext);
        actionLib.verifyElementPresent(customerAgentRegistrationPage.iconFinanceSetup);
    });

    it('test_signup_11 : should be able to provide details in finance setup tab  & navigate to next tab', 
    function() {
    
        //when want to click uiSwitch then pass any string other than "none" in first two attributes below
        financeSetupRegistrationPage.signUpFinanceSetupTab("Yes", "Yes", financeSetupData.invoiceHeaderNote,
            financeSetupData.invoiceFooterNote);
        actionLib.click(financeSetupRegistrationPage.buttonNext);
        actionLib.verifyElementPresent(financeSetupRegistrationPage.iconChargeSetup);
    });

    it('test_signup_12 : should be able to provide details in charge setup tab & navigate to next tab', 
    function() {

        chargeSetupRegistrationPage.signUpChargeSetupTab(chargeSetupData.chargeName, chargeSetupData.chargeCode,
            chargeSetupData.chargeType, chargeSetupData.calculationType, fileUploadChargeSetupPath);
        reqElement = actionLib.getChainedElement(chargeSetupRegistrationPage.msgSuccessfulFileUpload,
            chargeSetupRegistrationPage.strChildLocator);
        actionLib.verifyElementText("none", reqElement, "Charge data uploaded successfully");
        actionLib.click(chargeSetupRegistrationPage.buttonNext);
        actionLib.verifyElementPresent(chargeSetupRegistrationPage.iconOthers);
    });

    it('test_signup_13 : should be able to provide details in Others tab', function() {

        actionLib.click(othersRegistrationPage.tabJobDateSetup);
        othersRegistrationPage.signUpJobDateSetupTab(othersTabData.jobDateSetupExport, othersTabData.jobDateSetupImport);
        actionLib.click(othersRegistrationPage.tabAccountDocNumberSetup);
        othersRegistrationPage.signUpAccountingDocNumTab(othersTabData.daybookCode, othersTabData.daybookName,
            othersTabData.documentType, othersTabData.noOfDigits, othersTabData.prefix, othersTabData.yearOption, 
            othersTabData.monthOption, othersTabData.startingNumber, othersTabData.suffix, othersTabData.separator);
        actionLib.click(othersRegistrationPage.buttonFinish);
        actionLib.verifyElementText(othersRegistrationPage.textSignUpSuccessfulSubmission, "none", 
            "Your information has been submitted successfully");
        actionLib.verifyElementText(othersRegistrationPage.textSignUpEmailActivation, "none", 
            "Please check your email to activate your account");
    });
});

