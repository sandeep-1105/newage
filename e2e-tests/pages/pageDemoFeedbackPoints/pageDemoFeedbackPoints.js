/* 
Description : This file contains locators and functions required for Demo feedback points BRD document testcases 
*/

var actionLib = require('../../library/action.js');
var customerCommonElementPage = require('../../pages/pageAddCustomer/pageAddCustomer.js');
var customerAddressPage = require('../../pages/pageAddCustomer/pageAddCustomerAddress.js');
var customerMoreInfoPage = require('../../pages/pageAddCustomer/pageAddCustomerMoreInfo.js');

var pageDemoFeedBackPoints = function () {

    this.menuCrm = by.xpath("//a[@data-title='CRM']");
    this.subMenuPricingAir = by.xpath("//b[@class='col-xs-9 ptb14 pl10 ng-binding'][text()='Pricing Air']");
    this.btnAddPricingAir = by.className("btn btn-primary btn-xs btn-property accent-btn");
    this.tabOriginCharges = by.xpath("//button[@ng-click=\"clickOnTab('Origin','generalChargeName0')\"]");
    this.tabDestinationCharges = by.xpath("//button[@ng-click=\" clickOnTab('Destination','genFreightDestination0')\"]");
    this.tabFreightCharges = by.xpath("//button[@ng-click=\" clickOnTab('Freight','genFreight0')\"]");
    this.btnFrightChargeMoreInfo = by.xpath("//table[@tab-row='addGeneralFreightCharge()']//tbody//td/a[@ng-click='editmorepopup(pricingCarrier)']");
    this.btnMoreInfoCancel = by.className("btn btn-primary btn-xs btn-property cancel-btn mr0");
    this.dropDownOrigin = by.xpath("//input[@ng-model='pricingMaster.origin' and @aria-invalid='false']");
    this.dropDownDestination = by.id("destination");
    this.activeElementLocator = by.css(".uib-typeahead-match.ng-scope.active");
    this.btnSaveAddPricingAir = by.css(".btn.btn-primary.btn-xs.btn-property.accent-btn.ng-binding");
    //--Origin charges tab
    this.dropDownOriginChargesCharge = by.id("generalChargeName0");
    this.dropDownOriginChargesUnit = by.id("unit0");
    this.dropDownOriginChargesCurrency = by.id("currency0");
    this.textBoxOriginChargeMsrMinimum = by.id("minsellPrice0");
    this.textBoxOriginChargeMsrAmount = by.xpath("//input[@ng-class=\"{'error-control':errorGenOrgArray[$index].genOrgMinSelInAmount}\"]");
    this.textBoxOriginChargeSsrMinimum = by.xpath("//input[@ng-class=\"{'error-control':errorGenOrgArray[$index].genOrgStdSelInMinimum}\"]");
    this.textBoxOriginChargeSsrAmount = by.xpath("//input[@ng-class=\"{'error-control':errorGenOrgArray[$index].genOrgStdSelInAmount}\"]");
    this.textBoxOriginChargeCostMinimum = by.xpath("//input[@ng-class=\"{'error-control':errorGenOrgArray[$index].genOrgCostInMinimum}\"]");
    this.textBoxOriginChargeCostAmount = by.xpath("//input[@ng-class=\"{'error-control':errorGenOrgArray[$index].genOrgCostInAmount}\"]");
    this.calenderOriginChargesValidityFrom = by.id("validFromDate0");
    this.calenderOriginChargesValidityTo = by.id("validToDate0");
    //Destination charges tab
    this.dropDownDestChargesCharge = by.id("genFreightDestination0");
    this.dropDownDestChargesUnit = by.id("genDestunit0");
    this.dropDownDestChargesCurrency = by.id("genDestCurrency0");
    this.textBoxDestChargeMsrMinimum = by.id("genDestMinSell0");
    this.textBoxDestChargeMsrAmount = by.xpath("//input[@ng-class=\"{'error-control':errorGenDestArray[$index].genDestMinSelInAmount}\"]");
    this.textBoxDestChargeSsrMinimum = by.xpath("//input[@ng-class=\"{'error-control':errorGenDestArray[$index].genDestStdSelInMinimum}\"]");
    this.textBoxDestChargeSsrAmount = by.xpath("//input[@ng-class=\"{'error-control':errorGenDestArray[$index].genDestStdSelInAmount}\"]");
    this.textBoxDestChargeCostMinimum = by.xpath("//input[@ng-class=\"{'error-control':errorGenDestArray[$index].genDestCostInMinimum}\"]");
    this.textBoxDestChargeCostAmount = by.xpath("//input[@ng-class=\"{'error-control':errorGenDestArray[$index].genDestCostInAmount}\"]");

    //Freight charges tab
    this.dropDownFreightChargesCharge = by.id("genFreight0");
    this.dropDownFreightChargesCurrency = by.id("genCurrency0");

    //Navigate from left side menu tabs to Sales -> Enquiry
    this.textRecordsCount = by.className("meta-data ng-binding");
    this.linkActiveEnquiry = by.xpath("//li[@ng-click=\"setSearch('active'); detailTab='active'\"]");
    this.linkQuoteCreatedEnquiry = by.xpath("//li[@ng-click=\"setSearch('quote'); detailTab='quoteCreated'\"]");
    this.linkAllEnquiry = by.xpath("//li[@ng-click=\"setSearch('all'); detailTab='all'\"]");

    //Navigate from left side menu tabs to Sales -> Quotation
    this.linkWaitingForManagerApproval = by.xpath("//li[@ng-click='tabChange(tabObj.tab)' and contains(text(),'Waiting for Manager Approval')]");
    this.linkWaitingForCustomerApproval = by.xpath("//li[@ng-click='tabChange(tabObj.tab)' and contains(text(),'Waiting for Customer Approval')]");
    this.linkReadyForShipment = by.xpath("//li[@ng-click='tabChange(tabObj.tab)' and contains(text(),'Ready for Shipment')]");
    this.linkRejectedQuotation = by.xpath("//li[@ng-click='tabChange(tabObj.tab)' and contains(text(),'Rejected')]");
    this.linkAllQuotation = by.xpath("//li[@ng-click='tabChange(tabObj.tab)' and contains(text(),'All')]");

    //Navigate from left side menu tabs to CRM -> Shipment
    this.linkBookedShipment = by.xpath("//li[@ng-click='tabChange(tab)' and contains(text(),'Booked')]");
    this.linkReceivedShipment = by.xpath("//li[@ng-click='tabChange(tab)' and contains(text(),'Received')]");
    this.linkGeneratedShipment = by.xpath("//li[@ng-click='tabChange(tab)' and contains(text(),'Generated')]");
    this.linkClosedShipment = by.xpath("//li[@ng-click='tabChange(tab)' and contains(text(),'Closed')]");
    this.linkCancelledShipment = by.xpath("//li[@ng-click='tabChange(tab)' and contains(text(),'Cancelled')]");
    this.linkAllShipment = by.xpath("//li[@ng-click='tabChange(tab)' and contains(text(),'All')]");
    this.btnExportShipments = by.xpath("//button[@data-state='Export']");
    this.btnImportShipments = by.xpath("//button[@data-state='Import']");
    this.btnBothShipments = by.xpath("//button[@data-state='Both']");

    //Navigate from left side menu tabs to Air -> Master Shipment
    this.linkActiveMasterShipments = by.xpath("//li[@ng-click='tabChange(tab)' and contains(text(),'Active')]");
    this.linkClosedMasterShipments = by.xpath("//li[@ng-click='tabChange(tab)' and contains(text(),'Closed')]");
    this.linkCancelledMasterShipments = by.xpath("//li[@ng-click='tabChange(tab)' and contains(text(),'Cancelled')]");
    this.linkAllMasterShipments = by.xpath("//li[@ng-click='tabChange(tab)' and contains(text(),'All')]");

    //-- demo feedback point BRD point 7
    this.textBoxOverDimension = by.xpath("//label[text()='Over Dimension']/..//*[@class='nomargin ng-binding']");
    this.textOverDimensionDetailScreen = by.xpath("//span[text()='Over Dimension']/..//*[@class='bold ng-binding']");
    this.uiSwitchDimensionUnit = by.xpath("//span[@id='enabled']");

    //-- demo feedback point BRD point 3
    this.msgSsrMsrCostValidation = by.className("message ng-binding");
    
    //-- demo feedback point BRD point 8
    this.linkAgentPort = by.xpath("//li[@ng-click=\"clickOnTab('agentPort')\"]");
    this.btnAddAgentPort = by.css(".btn.btn-property.accent-btn");
    this.dropDownAgentPortService = by.xpath("//input[contains(@class,'ng-valid-editable ng-empty') and @ng-model='agentPortMaster.serviceMaster']");
    this.dropDownAgentPortAgent = by.id("agentName0");
    this.dropDownAgentPort = by.id("portName0");
    this.btnAgentPortSave = by.css(".btn.btn-primary.btn-xs.btn-property.accent-btn");

    // demo feedback point BRD point 1 and 2
    this.verifyAirPricingLabels = function (tableRow, labelsArray) {
        reqElement = by.xpath("//table[@tab-row='" + tableRow + "']//tr");
        var row = element.all(reqElement);
        var cells = row.all(by.xpath("th[@class='text-center']"));
        var count = 0;
        cells.map(function (elm) {
            elm.getText().then(function (val) {
                expect(val).toEqual(labelsArray[count]);
                count += 1;
            });
        });
    }

    // demo feedback point BRD point 1 and 2
    this.verifyFreightChargeMoreInfoLabels = function (labelsArray) {
        reqElement = by.xpath("//div[@class='table-responsive']//tbody");
        var row = element.all(reqElement);
        var cells = row.all(by.tagName('tr'));
        var count = 0;
        cells.map(function (elm) {
            elm.getText().then(function (val) {
                expect(val).toEqual(labelsArray[count]);
                count += 1;
            });
        });
    }

    // demo feedback point BRD point 19
    this.verifyLinksCount = function (strRecordsLocator, strLinkLocator) {
        var recordsCount;
        var linkCount;
        element(strRecordsLocator).isPresent().then(function (isPresent) {
            if (isPresent) {
                reqElement = actionLib.getTextByLocator(strRecordsLocator);
                reqElement.then(function (val) {
                    var splitText = val.split(" ");
                    recordsCount = splitText[splitText.length - 1];
                });
            }
            else {
                recordsCount = 0;
            }
            reqElement = actionLib.getTextByLocator(strLinkLocator);
            reqElement.then(function (val) {
                var splitText = val.split("(")[1].split(")");
                linkCount = splitText[0];
                console.log("*** "+strLinkLocator+" ****Link: "+linkCount+" ****Records: "+recordsCount);
                expect(parseInt(linkCount)).toEqual(parseInt(recordsCount));
            });
        });
    }

    // demo feedback point BRD point 19
    // --  strTextRecordsCountLocator for showing 1-10 of 30, subTabsButtons for Export/Import/Both
    // strLinkLocator for Active/Cancelled/Closed etc.
    this.matchRecordsAndLinkCount = function (subTabsButtons, strTextRecordsCountLocator, strLinkLocator) {
        
        actionLib.click(strLinkLocator);
        actionLib.click(subTabsButtons.btnExportShipments);
        this.verifyLinksCount(strTextRecordsCountLocator, strLinkLocator);
        actionLib.click(subTabsButtons.btnImportShipments);
        this.verifyLinksCount(strTextRecordsCountLocator, strLinkLocator);
        actionLib.click(subTabsButtons.btnBothShipments);
        this.verifyLinksCount(strTextRecordsCountLocator, strLinkLocator);
        
        // actionLib.verifyElementPresent(shipmentStatusLocator);
        // actionLib.click(shipmentStatusLocator);
        // actionLib.click(shipmentTypeLocator);
        // this.verifyLinksCount(recordsCountLocator, shipmentStatusLocator);
    }

    // demo feedback point BRD point 8
    this.addAgentCustomer = function(customerDetails){
        customerCommonElementPage.navigateToAddCustomerPage();
        customerCommonElementPage.addCustomer(customerDetails.agentName, "none", customerDetails.country);
        actionLib.selectDropdown(customerAddressPage.dropDownType, customerDetails.type);
        actionLib.setText(customerAddressPage.textBoxAddress1, customerDetails.address1);
        actionLib.setText(customerAddressPage.textBoxAddress2, customerDetails.address2);
        actionLib.setText(customerAddressPage.dropDownCity, customerDetails.city);
        actionLib.setText(customerAddressPage.textBoxZipCode, customerDetails.zipCode);
        actionLib.scrollToElement(customerAddressPage.dropDownStateProvince);
        actionLib.setText(customerAddressPage.dropDownStateProvince, customerDetails.state);
        actionLib.click(customerMoreInfoPage.tabMoreInfo);
        customerMoreInfoPage.fillCustomerType(customerDetails.disableCustomerType);
        customerMoreInfoPage.fillCustomerType(customerDetails.customerType);
        actionLib.scrollToElement(customerMoreInfoPage.textBoxIataCode);
        actionLib.setText(customerMoreInfoPage.textBoxIataCode, customerDetails.iataCode);
        actionLib.verifyElementPresent(customerCommonElementPage.btnSave);
        actionLib.click(customerCommonElementPage.btnSave);
        actionLib.verifyElementPresent(customerCommonElementPage.btnPopUpOk);
        actionLib.click(customerCommonElementPage.btnPopUpOk);
    }

    // demo feedback point BRD point 8
    this.addAgentPort = function(agentPortDetails){
        actionLib.verifyElementPresent(this.btnAddAgentPort);
        actionLib.click(this.btnAddAgentPort);
        actionLib.verifyElementPresent(this.dropDownAgentPortService);
        actionLib.setText(this.dropDownAgentPortService, agentPortDetails.agentPortService);
        actionLib.click(this.activeElementLocator);
        actionLib.setText(this.dropDownAgentPortAgent, agentPortDetails.agentName);
        actionLib.setText(this.dropDownAgentPort, agentPortDetails.agentPort);
        actionLib.click(this.btnAgentPortSave);
    }
};
module.exports = new pageDemoFeedBackPoints();
