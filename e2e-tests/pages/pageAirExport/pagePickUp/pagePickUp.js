var actionLib = require('../../../library/action.js');

var pageAddPickUp = function () {
    var reqElement;

    this.linkPickUp = by.xpath("//a[@href='#/myAirExportTask?activeTab=Pickup']");
    this.btnEdit = by.className("btn-icon ng-scope");
    this.subMenuPickUpDelivery = by.xpath("//span[@title='PickUp/Delivery']");
    this.textBoxProTrackingNo = by.id("shipment.serviceDetail.pickUpDeliveryPoint.proTracking.0");
    this.expandPickUp = by.xpath("//div[@ng-click='picksInfo=!picksInfo']");
    this.checkBoxOurPickUp = by.id("ourPickUp0");
    this.textBoxAddress1 = by.id("shipment.serviceDetail.pickUpDeliveryPoint.pickUpPlace.addressLine1.0");
    this.textBoxAddress2 = by.id("shipment.serviceDetail.pickUpDeliveryPoint.pickUpPlace.addressLine2.0");
    this.textBoxAddress3 = by.id("shipment.serviceDetail.pickUpDeliveryPoint.pickUpPlace.addressLine3.0");
    this.dropDownState = by.id("shipment.serviceDetail.pickUpDeliveryPoint.pickUpPlace.state.0");
    this.dropDownCity = by.id("shipment.serviceDetail.pickUpDeliveryPoint.pickUpPlace.city.0");
    this.textBoxZipCode = by.id("shipment.serviceDetail.pickUpDeliveryPoint.pickUpPlace.zipCode.0");
    this.textBoxContactDetails = by.id("shipmentServiceDetail.pickUpDeliveryPoint.pickUpContactPerson.0");
    this.textBoxPickUpEmail = by.id("shipmentServiceDetail.pickUpDeliveryPoint.pickUpEmail.0");
    this.textBoxMobileNo = by.id("shipmentServiceDetail.pickUpDeliveryPoint.pickUpMobileNo.0");
    this.textBoxPhoneNo = by.id("shipmentServiceDetail.pickUpDeliveryPoint.pickUpPhoneNo.0");
    this.calenderPickUpFollowUpDate = by.id("shipmentServiceDetail.pickUpDeliveryPoint.pickUpFollowUp.0");
    this.calenderPlannedPickUpDate = by.id("shipmentServiceDetail.pickUpDeliveryPoint.pickUpPlanned.0");
    this.calenderActualPickUpDate = by.id("shipmentServiceDetail.pickUpDeliveryPoint.pickUpActual.0");
    this.btnUpdate = by.xpath("//input[@ng-click='saveShipment()']");
    this.btnPopUpOk = by.xpath("//button[@ng-click='confirm(1)']");

    //specify "none" for parameters which you don't want to fill
    this.addPickUp = function (pickUpData, pickUpCalenderVars) {

        if (pickUpData.pickUpAddress1 != "none") {
            actionLib.setText(this.textBoxAddress1, pickUpData.pickUpAddress1);
        }
        if (pickUpData.pickUpAddress2 != "none") {
            actionLib.setText(this.textBoxAddress2, pickUpData.pickUpAddress2);
        }
        if (pickUpData.pickUpAddress3 != "none") {
            actionLib.setText(this.textBoxAddress3, pickUpData.pickUpAddress3);
        }
        if (pickUpData.pickUpState != "none") {
            actionLib.setText(this.dropDownState, pickUpData.pickUpState);
        }
        if (pickUpData.pickUpCity != "none") {
            actionLib.setText(this.dropDownCity, pickUpData.pickUpCity);
        }
        if (pickUpData.pickUpZipCode != "none") {
            actionLib.setText(this.textBoxZipCode, pickUpData.pickUpZipCode);
        }
        if (pickUpData.pickUpContactDetails != "none") {
            actionLib.setText(this.textBoxContactDetails, pickUpData.pickUpContactDetails);
        }
        if (pickUpData.pickUpEmail != "none") {
            actionLib.setText(this.textBoxPickUpEmail, pickUpData.pickUpEmail);
        }
        if (pickUpData.pickUpMobileNo != "none") {
            actionLib.setText(this.textBoxMobileNo, pickUpData.pickUpMobileNo);
        }
        if (pickUpData.pickUpPhoneNo != "none") {
            actionLib.setText(this.textBoxPhoneNo, pickUpData.pickUpPhoneNo);
        }
        if (pickUpCalenderVars.strPickUpFollowUpDate != "none") {
            actionLib.setText(this.calenderPickUpFollowUpDate, pickUpCalenderVars.strPickUpFollowUpDate);
        }
        if (pickUpCalenderVars.strPlannedPickUpDate != "none") {
            actionLib.setText(this.calenderPlannedPickUpDate, pickUpCalenderVars.strPlannedPickUpDate);
        }
        if (pickUpCalenderVars.strActualPickUpDate != "none") {
            actionLib.setText(this.calenderActualPickUpDate, pickUpCalenderVars.strActualPickUpDate);
        }
    }
};
module.exports = new pageAddPickUp();
