package com.efreightsuite.controller;

import com.efreightsuite.enumeration.DimensionUnit;
import com.efreightsuite.enumeration.LovStatus;
import com.efreightsuite.model.*;
import com.efreightsuite.service.SequenceGeneratorService;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

@RunWith(SpringRunner.class)
@ActiveProfiles("test")
@SpringBootTest
@AutoConfigureMockMvc
abstract class BaseReportRestApiTest {

    @Autowired
    MockMvc mockMvc;

    @MockBean
    SequenceGeneratorService sequenceGeneratorService;

    CountryMaster country;
    LocationMaster location;
    CompanyMaster company;
    EmployeeMaster employee;
    PartyMaster partyMaster;
    AddressMaster addressMaster;
    QuotationDetail quotationDetail;
    QuotationCarrier quotationCarrier;
    SystemTrack systemTrack;

   final Gson gson = new GsonBuilder().create();

    @Before
    public void setUp() throws Exception {

        systemTrack = new SystemTrack("test_user", new Date(), "test_user", new Date());

        partyMaster = new PartyMaster("Party 1", "p1", LovStatus.Active, systemTrack);
        company =new CompanyMaster("Xebia", "XI", LovStatus.Active, systemTrack);
        country = new CountryMaster("IN", "India","INR", LovStatus.Active, systemTrack);
        location = new LocationMaster(
                "GGN",
                "Gurgaon",
                "IST",
                LovStatus.Active,
                "ddMMyyy",
                "ddMMyyy hh:mm:ss"

        );
        employee = new EmployeeMaster("Employee 1", "E01", "emp1@test.com", systemTrack);
        quotationDetail = new QuotationDetail();
        quotationCarrier = new QuotationCarrier();


    }

    protected String toJson(Object object) {

        return gson.toJson(object);
    }

    public LocationMaster partialLocationMaster() {
        location.setSystemTrack(systemTrack);
        location.setCountryMaster(country);
        return location;
    }

    public CountryMaster partialCountryMaster() {
        country.setLocale("en-IN");
        return country;
    }

    public QuotationDetail partialQuotationDetail(){
        List<QuotationCarrier> quotationCarriers = new ArrayList<>();
        quotationCarriers.add(quotationCarrier);
        quotationDetail.setQuotationCarrierList(quotationCarriers);
        quotationDetail.setDimensionUnit(DimensionUnit.CENTIMETERORKILOS);
        return quotationDetail;
    }

}
