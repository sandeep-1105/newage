package com.efreightsuite.controller;

import com.efreightsuite.aspect.LoggingUserActivityAspect;
import com.efreightsuite.dto.ReportDownloadRequestDto;
import com.efreightsuite.enumeration.*;
import com.efreightsuite.model.Quotation;
import com.efreightsuite.model.QuotationDetail;
import com.efreightsuite.repository.EnquiryDetailRepository;
import com.efreightsuite.repository.QuotationRepository;
import com.efreightsuite.repository.ReportConfigurationMasterRepository;
import com.efreightsuite.repository.ReportMasterRepository;
import org.junit.Test;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class ReportControllerTest extends BaseReportRestApiTest {


    @MockBean
    LoggingUserActivityAspect loggingUserActivityAspect;
    
    @MockBean
    QuotationRepository quotationRepository;

    @MockBean
    EnquiryDetailRepository enquiryDetailRepository;

    @MockBean
    ReportConfigurationMasterRepository reportConfigurationMasterRepository;

    @MockBean
    ReportMasterRepository reportMasterRepository;


    @Test
    public void should_process_pdf_reports_download_request_for_quotation_without_estimation() throws Exception {

        ReportDownloadRequestDto requestForPdfDownload = getPartialRequestForReportDownload();
        given(sequenceGeneratorService.getSequenceValueByType(SequenceType.QUOTATION)).willReturn("Q001");
        Quotation quotation = createQuotation();
        when(quotationRepository.findById(anyLong())).thenReturn(quotation);
        MvcResult result = this.mockMvc.perform(
                post("/api/v1/report/download")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(toJson(requestForPdfDownload)))
                .andExpect(status().isOk())
                .andReturn();
        verify(reportConfigurationMasterRepository, atLeastOnce()).getByKeyAndLocation(anyObject(), anyLong());
        verify(enquiryDetailRepository, atLeastOnce()).findByEnquiryNo(anyString());
        verify(reportMasterRepository, atLeastOnce()).findByReportKeyAndIsReportEnable(anyObject(), anyObject());
        String contentTypePdfDownload = result.getResponse().getContentType();
        int lengthPdfDownload = result.getResponse().getContentLength();
        assertThat(contentTypePdfDownload).isEqualToIgnoringCase("application/pdf");
        assertThat(lengthPdfDownload).isGreaterThan(1);
    }

    @Test
    public void should_process_pdf_reports_preview_request_for_quotation_without_estimation() throws Exception {

        ReportDownloadRequestDto requestForPdfPreview = getPartialRequestForReportDownload();
        requestForPdfPreview.setDownloadOption(ReportDownloadOption.Preview);
        Quotation quotation = createQuotation();
        when(quotationRepository.findById(anyLong())).thenReturn(quotation);
        MvcResult resultPdfPreview = this.mockMvc.perform(
                post("/api/v1/report/download")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(toJson(requestForPdfPreview)))
                .andExpect(status().isOk())
                .andReturn();
        verify(reportConfigurationMasterRepository, atLeastOnce()).getByKeyAndLocation(anyObject(), anyLong());
        verify(enquiryDetailRepository, atLeastOnce()).findByEnquiryNo(anyString());
        String contentTypePdfPreview = resultPdfPreview.getResponse().getContentType();
        int lengthPdfPreview = resultPdfPreview.getResponse().getContentLength();
        assertThat(contentTypePdfPreview).isEqualToIgnoringCase("application/pdf");
        assertThat(lengthPdfPreview).isGreaterThan(1);

    }

    @Test
    public void should_process_pdf_reports_print_request_for_quotation_without_estimation() throws Exception {
        ReportDownloadRequestDto requestForPdfPrint = getPartialRequestForReportDownload();
        requestForPdfPrint.setDownloadOption(ReportDownloadOption.Print);
        Quotation quotation = createQuotation();
        when(quotationRepository.findById(anyLong())).thenReturn(quotation);
        MvcResult resultPdfPrint = this.mockMvc.perform(
                post("/api/v1/report/download")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(toJson(requestForPdfPrint)))
                .andExpect(status().isOk())
                .andReturn();
        verify(reportConfigurationMasterRepository, atLeastOnce()).getByKeyAndLocation(anyObject(), anyLong());
        verify(enquiryDetailRepository, atLeastOnce()).findByEnquiryNo(anyString());
        String contentTypePdfPrint = resultPdfPrint.getResponse().getContentType();
        int lengthPdfPrint = resultPdfPrint.getResponse().getContentLength();
        assertThat(contentTypePdfPrint).isEqualToIgnoringCase("application/pdf");
        assertThat(lengthPdfPrint).isGreaterThan(1);

    }

    @Test
    public void should_process_xls_reports_download_request_for_quotation_without_estimation() throws Exception {
        ReportDownloadRequestDto requestFoxXlsDownload = getPartialRequestForReportDownload();
        requestFoxXlsDownload.setDownloadOption(ReportDownloadOption.Download);
        requestFoxXlsDownload.setDownloadFileType(ReportDownloadFileType.XLS);
        Quotation quotation = createQuotation();
        when(quotationRepository.findById(anyLong())).thenReturn(quotation);
        MvcResult resultXlsDownload = this.mockMvc.perform(
                post("/api/v1/report/download")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(toJson(requestFoxXlsDownload)))
                .andExpect(status().isOk())
                .andReturn();
        verify(reportConfigurationMasterRepository, atLeastOnce()).getByKeyAndLocation(anyObject(), anyLong());
        verify(enquiryDetailRepository, atLeastOnce()).findByEnquiryNo(anyString());
        verify(reportMasterRepository, atLeastOnce()).findByReportKeyAndIsReportEnable(anyObject(), anyObject());
        String contentTypeXlsDownload = resultXlsDownload.getResponse().getContentType();
        int lengthXlsDownload = resultXlsDownload.getResponse().getContentLength();
        assertThat(contentTypeXlsDownload).isEqualToIgnoringCase("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        assertThat(lengthXlsDownload).isGreaterThanOrEqualTo(0);

    }

    @Test
    public void should_process_rtf_reports_download_request_for_quotation_without_estimation() throws Exception {
        ReportDownloadRequestDto requestForRtfDownload = getPartialRequestForReportDownload();
        requestForRtfDownload.setDownloadOption(ReportDownloadOption.Download);
        requestForRtfDownload.setDownloadFileType(ReportDownloadFileType.RTF);
        Quotation quotation = createQuotation();
        when(quotationRepository.findById(anyLong())).thenReturn(quotation);
        MvcResult resultRtfDownload = this.mockMvc.perform(
                post("/api/v1/report/download")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(toJson(requestForRtfDownload)))
                .andExpect(status().isOk())
                .andReturn();
        verify(reportConfigurationMasterRepository, atLeastOnce()).getByKeyAndLocation(anyObject(), anyLong());
        verify(enquiryDetailRepository, atLeastOnce()).findByEnquiryNo(anyString());
        verify(reportMasterRepository, atLeastOnce()).findByReportKeyAndIsReportEnable(anyObject(), anyObject());
        String contentTypeRtfDownload = resultRtfDownload.getResponse().getContentType();
        int lengthRtfDownload = resultRtfDownload.getResponse().getContentLength();
        assertThat(contentTypeRtfDownload).isEqualToIgnoringCase("application/rtf");
        assertThat(lengthRtfDownload).isGreaterThan(1);
    }

    private ReportDownloadRequestDto getPartialRequestForReportDownload() {
        ReportDownloadRequestDto reportDownloadRequestDto = new ReportDownloadRequestDto(ReportName.QUOTATION_WITH_ESTIMATION, ReportDownloadFileType.PDF, ReportDownloadOption.Download, 1l, true);
        return reportDownloadRequestDto;
    }

    private Quotation createQuotation() {

        List<QuotationDetail> quotationDetails = new ArrayList<>();
        quotationDetails.add(partialQuotationDetail());
        Quotation quotation = new Quotation.Builder(WhoRouted.Agent, "Header Note", "Footer Note", new Date(), new Date())
                .withSystemTrack(systemTrack)
                .withLocationMaster(partialLocationMaster())
                .withCompanyMaster(company)
                .withCountryMaster(partialCountryMaster())
                .withCustomer(partyMaster)
                .withShipperAddress(addressMaster)
                .withCustomerAddress(addressMaster)
                .withApproved(Approved.Approved)
                .withSalesCoordinator(employee)
                .withLoggedBy(employee)
                .withQuotationDetailList(quotationDetails)
                .build();
        return quotation;
    }


}