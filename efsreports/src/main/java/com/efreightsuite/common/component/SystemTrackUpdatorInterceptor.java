package com.efreightsuite.common.component;

import java.io.Serializable;
import java.util.Date;
import java.util.Iterator;

import com.efreightsuite.model.SystemTrack;
import com.efreightsuite.service.AuthService;
import com.efreightsuite.util.TimeUtil;
import org.hibernate.EmptyInterceptor;
import org.hibernate.type.Type;

public class SystemTrackUpdatorInterceptor extends EmptyInterceptor {

    private static final long serialVersionUID = 1L;

    @Override
    public boolean onSave(Object entity, Serializable id, Object[] state, String[] propertyNames, Type[] types) {

        for (int i = 0; i < state.length; i++) {
            if (propertyNames[i].equals("systemTrack")) {
                SystemTrack systemTrack = new SystemTrack();
                Date currentLocationTime = TimeUtil.getCurrentLocationTime();
                systemTrack.setCreateDate(currentLocationTime);
                systemTrack.setCreateUser(AuthService.getCurrentUser().getUserName());
                systemTrack.setLastUpdatedDate(currentLocationTime);
                systemTrack.setLastUpdatedUser(AuthService.getCurrentUser().getUserName());
                state[i] = systemTrack;
                return true;
            }
        }

        return false;
    }

    @Override
    public boolean onFlushDirty(Object entity, Serializable id, Object[] currentState, Object[] previousState,
                                String[] propertyNames, Type[] types) {

        for (int i = 0; i < currentState.length; i++) {
            if (propertyNames[i].equals("systemTrack")) {
                Date currentLocationTime = TimeUtil.getCurrentLocationTime();

                SystemTrack systemTrack = (SystemTrack) previousState[i];
                /*
				 * systemTrack.setCreateDate(currentLocationTime);
				 * systemTrack.setCreateUser("ADMIN");
				 */
                systemTrack.setLastUpdatedDate(currentLocationTime);
                systemTrack.setLastUpdatedUser(AuthService.getCurrentUser().getUserName());
                currentState[i] = systemTrack;
                return true;
            }
        }

        return false;

    }

    @Override
    public void preFlush(Iterator entities) {
        super.preFlush(entities);
    }

    @Override
    public void postFlush(Iterator entities) {
        super.postFlush(entities);
    }

    @Override
    public void onDelete(Object entity, Serializable id, Object[] state, String[] propertyNames, Type[] types) {
        super.onDelete(entity, id, state, propertyNames, types);
    }
}
