package com.efreightsuite.search;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import com.efreightsuite.dto.ReportMasterSearchDto;
import com.efreightsuite.dto.SearchRespDto;
import com.efreightsuite.model.ReportMaster;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ReportMasterSearchImpl {

    @Autowired
    EntityManager em;

    public SearchRespDto search(ReportMasterSearchDto dto) {

        String hql = "";

        Map<String, Object> params = new HashMap<String, Object>();


        if (dto.getSearchreportMasterCode() != null && dto.getSearchreportMasterCode().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }
            hql = hql + " UPPER(reportMasterCode) like '" + dto.getSearchreportMasterCode().trim().toUpperCase() + "%' ";

        }

        if (dto.getSearchreportMasterName() != null && dto.getSearchreportMasterName().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }
            hql = hql + " UPPER(reportMasterName) like '" + dto.getSearchreportMasterName().trim().toUpperCase() + "%' ";

        }

        if (dto.getSearchreportMasterKey() != null && dto.getSearchreportMasterKey().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }
            hql = hql + " UPPER(reportKey) like '" + dto.getSearchreportMasterKey().trim().toUpperCase() + "%' ";

        }

        if (hql.trim().length() != 0) {
            hql = " where " + hql;
        }

        hql = " from ReportMaster " + hql;


        if (dto.getSortByColumn() != null && dto.getOrderByType() != null && dto.getSortByColumn().trim().length() != 0
                && dto.getOrderByType().trim().length() != 0) {
            hql = hql + " order by " + dto.getSortByColumn().trim() + " " + dto.getOrderByType().trim();
        } else {
            hql = hql + " order by reportMasterName ASC";
        }


        String countQ = "Select count (id) " + hql;

        Query countQuery = em.createQuery(countQ);
        for (Entry<String, Object> e : params.entrySet()) {
            countQuery.setParameter(e.getKey(), e.getValue());
        }
        Long countTotal = (Long) countQuery.getSingleResult();


        TypedQuery<ReportMaster> query = em.createQuery(hql, ReportMaster.class);

        for (Entry<String, Object> e : params.entrySet()) {
            query.setParameter(e.getKey(), e.getValue());
        }

        query.setFirstResult(dto.getSelectedPageNumber() * dto.getRecordPerPage());

        query.setMaxResults(dto.getRecordPerPage());

        List<ReportMaster> resultlist = query.getResultList();


        SearchRespDto searchRespDto = new SearchRespDto(countTotal, resultlist);

        return searchRespDto;

    }

    public SearchRespDto search(int selectedPageNumber, int recordPerPage, String hql) {

        String countQ = "Select count (id) " + hql;

        Query countQuery = em.createQuery(countQ);

        Long countTotal = (Long) countQuery.getSingleResult();

        TypedQuery<ReportMaster> query = em.createQuery(hql, ReportMaster.class);

        query.setFirstResult(selectedPageNumber * recordPerPage);

        query.setMaxResults(recordPerPage);

        List<ReportMaster> resultlist = query.getResultList();

        SearchRespDto searchRespDto = new SearchRespDto(countTotal, resultlist);


        return searchRespDto;

    }

}
