package com.efreightsuite.search;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import com.efreightsuite.dto.LanguageDetailSearchReqDto;
import com.efreightsuite.dto.SearchRespDto;
import com.efreightsuite.model.LanguageDetail;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class LanguageDetailImpl {

    @Autowired
    EntityManager em;

    public SearchRespDto search(LanguageDetailSearchReqDto dto) {

        Map<String, Object> params = new HashMap<String, Object>();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

        String hql = "";

        if (dto.getSearchLanguageCode() != null && dto.getSearchLanguageCode().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }

            hql = hql + " Upper(languageCode) like '" + dto.getSearchLanguageCode().trim().toUpperCase() + "%' ";
        }

        if (dto.getSearchLanguageDesc() != null && dto.getSearchLanguageDesc().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }

            hql = hql + " Upper(languageDesc) like '" + dto.getSearchLanguageDesc().trim().toUpperCase() + "%' ";
        }

        if (dto.getSearchQcVerify() != null && dto.getSearchQcVerify().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }

            hql = hql + " flagged = '" + dto.getSearchQcVerify() + "'";
        }

        if (dto.getSearchGroupName() != null && dto.getSearchGroupName().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }

            hql = hql + " Upper(groupName) like '" + dto.getSearchGroupName().trim().toUpperCase() + "%' ";
        }


        if (dto.getSearchLanguageName() != null && dto.getSearchLanguageName().trim().length() != 0) {

            if (hql.trim().length() != 0) {
                hql = hql + " AND ";
            }

            hql = hql + " language.id IN (select l.id from Language l where Upper(l.languageName) like '" + dto.getSearchLanguageName().trim().toUpperCase() + "%') ";
        }


        if (dto.getLastUpdatedOn() != null && dto.getLastUpdatedOn().getStartDate() != null
                && dto.getLastUpdatedOn().getEndDate() != null) {

            try {
                Date dt1 = sdf.parse(dto.getLastUpdatedOn().getStartDate());
                Date dt2 = sdf.parse(dto.getLastUpdatedOn().getEndDate());

                if (hql.trim().length() != 0) {
                    hql = hql + " AND ";
                }
                params.put("lastUpdatedStartDate", dt1);
                params.put("lastUpdatedEndDate", dt2);

                log.info("dt1 " + dt1 + " dt2 " + dt2);
                hql = hql
                        + " ( systemTrack.lastUpdatedDate >= :lastUpdatedStartDate AND systemTrack.lastUpdatedDate  <= :lastUpdatedEndDate ) ";

            } catch (ParseException e1) {
                log.error("Date Conversion error : ", e1);
            }

        }


        if (hql.trim().length() != 0) {
            hql = " where " + hql;
        }

        hql = " from LanguageDetail " + hql;


        if (dto.getSortByColumn() != null && dto.getOrderByType() != null && dto.getSortByColumn().trim().length() != 0 && dto.getOrderByType().trim().length() != 0) {
            hql = hql + " order by " + dto.getSortByColumn().trim() + " " + dto.getOrderByType().trim();
        } else {
            hql = hql + " order by id";
        }

        log.info("Language Detail Search hql:[" + hql + "]");

        return search(dto.getSelectedPageNumber(), dto.getRecordPerPage(), hql, params);
    }

    private SearchRespDto search(int selectedPageNumber, int recordPerPage, String hql, Map<String, Object> params) {

        String countQ = "Select count (id) " + hql;

        Query countQuery = em.createQuery(countQ);

        for (Entry<String, Object> e : params.entrySet()) {
            countQuery.setParameter(e.getKey(), e.getValue());
        }

        Long countTotal = (Long) countQuery.getSingleResult();

        TypedQuery<LanguageDetail> query = em.createQuery(hql, LanguageDetail.class);

        for (Entry<String, Object> e : params.entrySet()) {
            query.setParameter(e.getKey(), e.getValue());
        }

        query.setFirstResult(selectedPageNumber * recordPerPage);

        query.setMaxResults(recordPerPage);

        List<LanguageDetail> resultlist = query.getResultList();

        SearchRespDto searchRespDto = new SearchRespDto(countTotal, resultlist);

        log.info(resultlist.size() + " number of rows are selected.");

        return searchRespDto;

    }
}
