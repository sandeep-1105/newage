package com.efreightsuite.util;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletResponse;

import com.efreightsuite.dto.AutoAttachmentDto;
import com.efreightsuite.dto.ReportGenericDto;
import com.efreightsuite.enumeration.MappingUnit;
import com.efreightsuite.enumeration.ReportDownloadFileType;
import com.efreightsuite.enumeration.ReportDownloadOption;
import com.efreightsuite.enumeration.ReportName;
import com.efreightsuite.enumeration.UnitCalculationType;
import com.efreightsuite.enumeration.UnitType;
import com.efreightsuite.enumeration.YesNo;
import com.efreightsuite.model.AddressMaster;
import com.efreightsuite.model.LocationMaster;
import com.efreightsuite.model.UnitMaster;
import com.efreightsuite.report.service.AutoAttachService;
import com.efreightsuite.repository.ReportMasterRepository;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.log4j.Log4j2;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.export.JRRtfExporter;
import net.sf.jasperreports.engine.export.ooxml.JRXlsxExporter;
import net.sf.jasperreports.export.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class ReportUtil {

    @Autowired
    AutoAttachService as;
    @Autowired
    ReportMasterRepository reportMasterRepository;

    public byte[] japerPrintToResponseOutputStream(ReportDownloadFileType downloadFileType,
                                                   JasperPrint jasperPrint, String reportName, HttpServletResponse response, boolean b, AutoAttachmentDto ad) {
        log.info("ReportDownloadFileType : " + downloadFileType + " ;; Report Name : " + reportName);
        byte[] byteArray = null;
        try {

            ByteArrayOutputStream baoStream = new ByteArrayOutputStream();
            switch (downloadFileType) {
                case PDF:

                    if (response == null) {
                        byteArray = JasperExportManager.exportReportToPdf(jasperPrint);
                    } else {
                        byteArray = JasperExportManager.exportReportToPdf(jasperPrint);
                        response.setContentType("application/pdf");
                        response.setHeader("Content-Disposition", "attachment; filename=" + reportName + ".pdf");
                        response.setContentLength(byteArray.length);
                        if (ad != null) {
                            ad.setFormat("application/pdf");
                            ad.setDocumentObj(byteArray);
                            as.attach(ad);
                            log.info("attached successfully:====");
                        } else {
                            response.setHeader("Content-Disposition", "inline; filename=" + reportName + ".pdf");
                        }
                        if (!b) {
                            response.getOutputStream().write(byteArray);
                            response.getOutputStream().flush();
                            response.getOutputStream().close();
                        }
                    }

                    break;

                case eMail:

                    byteArray = JasperExportManager.exportReportToPdf(jasperPrint);
                    break;

                case RTF:
                    JRRtfExporter rtfExporter = new JRRtfExporter();
                    rtfExporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
                    rtfExporter.setParameter(JRExporterParameter.OUTPUT_STREAM, baoStream);
                    rtfExporter.setParameter(JRExporterParameter.CHARACTER_ENCODING, "UTF-8");
                    rtfExporter.exportReport();
                    byteArray = baoStream.toByteArray();
                    if (ad != null) {
                        ad.setDocumentObj(byteArray);
                        ad.setFormat("application/rtf");
                        as.attach(ad);
                    }
                    response.setContentType("application/rtf");
                    response.setHeader("Content-Disposition", "attachment; filename=" + reportName + ".rtf");
                    response.setContentLength(byteArray.length);
                    response.getOutputStream().write(byteArray);
                    response.getOutputStream().flush();
                    response.getOutputStream().close();
                    break;
                case XLS:
                    if (ad != null) {
                        ad.setFormat("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
                        as.attach(ad);
                    }
                    JRXlsxExporter exporter = new JRXlsxExporter();
                    exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
                    SimpleXlsxReportConfiguration configuration = new SimpleXlsxReportConfiguration();
                    configuration.setDetectCellType(true);// Set configuration
                    // as you like it!!
                    configuration.setCollapseRowSpan(false);
                    configuration.setIgnoreGraphics(false);
                    exporter.setConfiguration(configuration);
                    response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
                    response.setHeader("Content-Disposition", "attachment; filename=" + reportName + ".xlsx");
                    OutputStream out = response.getOutputStream();
                    OutputStreamExporterOutput output = new SimpleOutputStreamExporterOutput(out);
                    exporter.setExporterOutput(output);
                    exporter.exportReport();
                    out.flush();
                    out.close();
                    break;
                default:
                    break;
            }
        } catch (JRException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return byteArray;
    }


    //Date format Util Func
    public String dateToReportDate(Date date, String dateFormatString) {
        log.info("In dateToReportDate FUN input parameter date - " + date + " ; DateFormat -- " + dateFormatString);
        DateFormat dateFormat = null;
        if (date != null) {
            try {
                if (dateFormatString == null || dateFormatString.length() == 0) {
                    log.info("Request Date format is null So default date format 'dd-MMM-yy' is assigned");
                    dateFormat = new SimpleDateFormat("dd-MMM-yy");
                } else {
                    dateFormat = new SimpleDateFormat(dateFormatString);
                }
                return dateFormat.format(date).toString().toUpperCase();
            } catch (Exception ex) {
                log.error("Exception on converting date --- ", ex);
                ex.printStackTrace();
            }
        }
        return "";
    }

    public List<String> addressMasterToGenericStringList(AddressMaster addressMaster) {
        List<String> addressList = null;
        if (addressMaster != null) {
            addressList = new ArrayList<>();
            if (addressMaster.getAddressLine1() != null) {
                addressList.add(addressMaster.getAddressLine1());
            }
            if (addressMaster.getAddressLine2() != null) {
                addressList.add(addressMaster.getAddressLine2());
            }
            if (addressMaster.getAddressLine3() != null) {
                addressList.add(addressMaster.getAddressLine3());
            }

            String addrLine4 = "";
            if (addressMaster.getAddressLine4() != null) {
                addressList.add(addressMaster.getAddressLine4());
            }

            if (addressMaster.getPhone() != null) {
                addrLine4 = "Ph:" + addressMaster.getPhone();
            }
            if (addressMaster.getMobile() != null) {
                addrLine4 = addrLine4 + " " + "Ph:" + addressMaster.getMobile();
            }
            if (!addrLine4.equals("")) {
                addressList.add(addrLine4);
            }
        }
        return addressList;
    }


    public List<ReportGenericDto> addressMasterToGenericList(AddressMaster addressMaster) {
        List<ReportGenericDto> addressList = new ArrayList<>();
        if (addressMaster != null) {
            ReportGenericDto address = new ReportGenericDto();
            if (addressMaster.getAddressLine1() != null) {
                address.setAddress(addressMaster.getAddressLine1());
                addressList.add(address);
            }
            address = new ReportGenericDto();
            if (addressMaster.getAddressLine2() != null) {
                address.setAddress(addressMaster.getAddressLine2());
                addressList.add(address);
            }
            address = new ReportGenericDto();
            if (addressMaster.getAddressLine3() != null) {
                address.setAddress(addressMaster.getAddressLine3());
                addressList.add(address);
            }
            address = new ReportGenericDto();

            String addrLine4 = "";
            String phone = "";
            String mobile = "";

            if (addressMaster.getPhone() != null) {
                phone = "Ph: " + addressMaster.getPhone() + " ";
            }
            if (addressMaster.getMobile() != null) {
                mobile = "Mobile: " + addressMaster.getMobile() + " ";
            }
            if (addressMaster.getAddressLine4() != null) {
                addrLine4 = addressMaster.getAddressLine4() != null ? addressMaster.getAddressLine4() : "";

            }
            address.setAddress(addrLine4 + " " + phone + mobile);
            addressList.add(address);
            /*if(addressMaster.getAddressLine4() != null) {
                addrLine4 = addressMaster.getAddressLine4()!=null ? addressMaster.getAddressLine4():"";
			}
			if(addressMaster.getPhone() != null) {
				addrLine4 = "Ph:"+ addressMaster.getPhone();
			}
			if(addressMaster.getMobile() != null) {
				addrLine4 = addrLine4+" "+"Mobile:"+ addressMaster.getMobile();
			}
			address = new ReportGenericDto();
			address.setAddress(addrLine4);
			addressList.add(address);*/
        }
        return addressList;
    }

    public List<ReportGenericDto> locationMasterAddressList(LocationMaster addressMaster) {
        List<ReportGenericDto> addressList = new ArrayList<>();
        if (addressMaster != null) {
            ReportGenericDto address = new ReportGenericDto();
            if (addressMaster.getAddressLine1() != null) {
                address.setAddress(addressMaster.getAddressLine1());
                addressList.add(address);
            }
            address = new ReportGenericDto();
            if (addressMaster.getAddressLine2() != null) {
                address.setAddress(addressMaster.getAddressLine2());
                addressList.add(address);
            }
            address = new ReportGenericDto();
            if (addressMaster.getAddressLine3() != null) {
                address.setAddress(addressMaster.getAddressLine3());
                addressList.add(address);
            }
            String addrLine4 = "";
            if (addressMaster.getAddressLine4() != null) {
                addrLine4 = addressMaster.getAddressLine4() != null ? addressMaster.getAddressLine4() + " " : "";

            }
            address = new ReportGenericDto();


            String phone = "";
            String email = "";

            if (addressMaster.getPhone() != null) {
                phone = " Ph: " + addressMaster.getPhone() + " ";
            }
            if (addressMaster.getEmail() != null) {
                email = " Email : " + addressMaster.getEmail() + " ";
            }
            address.setAddress(addrLine4 + phone + email);
            addressList.add(address);

        }
        return addressList;
    }

    public BufferedImage getLogo(byte[] logo) {
        InputStream in = new ByteArrayInputStream(logo);
        BufferedImage bImageFromConvert = null;
        try {
            bImageFromConvert = ImageIO.read(in);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return bImageFromConvert;

    }

    public List<ReportGenericDto> shipperConsigneeAddressList(AddressMaster addressMaster) {
        List<ReportGenericDto> addressList = new ArrayList<>();
        if (addressMaster != null) {
            ReportGenericDto address = new ReportGenericDto();
            if (addressMaster.getAddressLine1() != null) {
                address.setAddress(addressMaster.getAddressLine1());
                addressList.add(address);
            }
            address = new ReportGenericDto();
            if (addressMaster.getAddressLine2() != null) {
                address.setAddress(addressMaster.getAddressLine2());
                addressList.add(address);
            }
            address = new ReportGenericDto();
            if (addressMaster.getAddressLine3() != null) {
                address.setAddress(addressMaster.getAddressLine3());
                addressList.add(address);
            }
            address = new ReportGenericDto();

            String addrLine4 = "";
            String phone = "";
            String mobile = "";
            String fax = "";
            String eMail = "";

            if (addressMaster.getPhone() != null) {
                phone = "Ph: " + addressMaster.getPhone() + " ";
            }
            if (addressMaster.getMobile() != null) {
                mobile = "Mobile: " + addressMaster.getMobile() + " ";
            }
            if (addressMaster.getAddressLine4() != null) {
                addrLine4 = addressMaster.getAddressLine4() != null ? addressMaster.getAddressLine4() + " " : "";

            }
            if (addressMaster.getFax() != null) {
                fax = "Fax: " + addressMaster.getFax() + " ";
            }
            if (addressMaster.getEmail() != null) {
                eMail = "Email: " + addressMaster.getEmail() + " ";
            }
            address.setAddress(addrLine4.replace("\\s", " ") + phone.replace("\\s", " ") + mobile.replace("\\s", " "));
            //address.setAddress(phone+mobile);

            addressList.add(address);

            address = new ReportGenericDto();
            address.setAddress(fax + eMail);
            addressList.add(address);

			/*if(addressMaster.getAddressLine4() != null) {
				addrLine4 = addressMaster.getAddressLine4()!=null ? addressMaster.getAddressLine4():"";
			}
			if(addressMaster.getPhone() != null) {
				addrLine4 = "Ph:"+ addressMaster.getPhone();
			}
			if(addressMaster.getMobile() != null) {
				addrLine4 = addrLine4+" "+"Mobile:"+ addressMaster.getMobile();
			}
			address = new ReportGenericDto();
			address.setAddress(addrLine4);
			addressList.add(address);*/
        }
        return addressList;
    }

    @SuppressWarnings("static-access")
    public Double mappingUnitCalculate(UnitCalculationType calculationType, Long calculationValue, Double actualValue) {

        Double retvalue = 1.0;

        if (calculationType.equals(calculationType.ADDITION)) {
            retvalue = actualValue + calculationValue;
        } else if (calculationType.equals(calculationType.SUBTRACTION)) {
            retvalue = actualValue - calculationValue;
        } else if (calculationType.equals(calculationType.MULTIPLICATION)) {
            retvalue = actualValue * calculationValue;
        } else if (calculationType.equals(calculationType.DIVISION)) {
            retvalue = actualValue / calculationValue;
        }
        return retvalue;

    }

    public Double unitCalc(UnitMaster unit, Double grossWeight, Double volumeWeight) {
        log.info("Unit Master UnitCode :: " + unit.getUnitCode() + " || UnitType :: " + unit.getUnitType() + " | Gross Weight : " + grossWeight + " | volumeWeight : " + volumeWeight);
        Double retValue = 1.0;
        Double mappingValue1 = 0.0;
        Double mappingValue2 = 0.0;

        if (unit.getUnitType().equals(UnitType.Unit)) {
            log.info("Unit Type " + unit.getUnitType());
            if (unit.getMappingUnit1() != null && unit.getMappingUnit1().equals(MappingUnit.GROSS_WEIGHT)) {
                mappingValue1 = mappingUnitCalculate(unit.getCalcType1(), unit.getCalcValue1(), grossWeight);
                log.info("GROSS WEIGHT Mapping Value 1 :: " + mappingValue1);
            } else if (unit.getMappingUnit1() != null && unit.getMappingUnit1().equals(MappingUnit.VOLUME_WEIGHT)) {
                mappingValue1 = mappingUnitCalculate(unit.getCalcType1(), unit.getCalcValue1(), volumeWeight);
                log.info("Volume WEIGHT Mapping Value 1 :: " + mappingValue1);
            }

            if (unit.getMappingUnit2() != null && unit.getMappingUnit2().equals(MappingUnit.GROSS_WEIGHT)) {
                mappingValue2 = mappingUnitCalculate(unit.getCalcType2(), unit.getCalcValue2(), grossWeight);
                log.info("GROSS WEIGHT Mapping Value 2 :: " + mappingValue2);
            } else if (unit.getMappingUnit2() != null && unit.getMappingUnit2().equals(MappingUnit.VOLUME_WEIGHT)) {
                mappingValue2 = mappingUnitCalculate(unit.getCalcType2(), unit.getCalcValue2(), volumeWeight);
                log.info("Volume WEIGHT Mapping Value 2 :: " + mappingValue2);
            }

            if (mappingValue2 != null) {
                if (mappingValue1 > mappingValue2)
                    retValue = mappingValue1;
                else
                    retValue = mappingValue2;
            } else {
                retValue = mappingValue1;
            }

        } else {
            retValue = 1.0;
        }
        log.info("Return Value :: " + retValue);
        return retValue;
    }

    public String threeDecimalDoubleValueFormat(Double value) {
        DecimalFormat threeDecFormat = new DecimalFormat("#.000#");
        return threeDecFormat.format(value);
    }

    public String twoDecimalDoubleValueFormat(Double value) {
        DecimalFormat twoDecFormat = new DecimalFormat("#0.00#");
        return twoDecFormat.format(value);
    }

    public void logObjectToJSON(Object data) {

        ObjectMapper objectMapper = new ObjectMapper();
        try {
            String json = objectMapper.writeValueAsString(data);
            log.info("Json Request :: " + json);
        } catch (JsonProcessingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    // Removing the data inside special characters like  (),{},[]
    public String removeSpecialCharacters(String input, ReportDownloadOption data) {
        if (input == null) {
            return "";
        }
        String beforeRemove = "";
        if (data != null && data.equals(ReportDownloadOption.Print) || data.equals(ReportDownloadOption.Preview) || data.equals(ReportDownloadOption.Download)) {
            char[] ch = input.toCharArray();
            for (int i = 0; i < ch.length; i++) {
                if (ch[i] == '[' || ch[i] == ']' || ch[i] == '{' || ch[i] == '}' || ch[i] == '(' || ch[i] == ')') {
                    ch[i] = ' ';
                    //break;
                }
                beforeRemove = beforeRemove + ch[i];
            }
        } else {
            return input;
        }
        return beforeRemove;
    }


    //For Reports AutoAttach
    public AutoAttachmentDto autoAttachment(ReportName reportName, YesNo yes, Long resourceID, String screenName) {
        //ReportMaster reportMaster=  reportMasterRepository.findByReportKeyAndIsReportEnable(reportName, yes);
        log.info("ReportNames::::::::::" + reportName);
        AutoAttachmentDto autoAttachmentDto = new AutoAttachmentDto();
        autoAttachmentDto.setDocumentName(reportName.name()); //Reportname
        autoAttachmentDto.setTransactionFor(screenName); //Invoice,MasterShipment,Shipment,Quotation
        autoAttachmentDto.setDocumentId(resourceID); //shipmetId or DocumentID
        autoAttachmentDto.setParentId(resourceID);  // serviceId or consolId
        autoAttachmentDto.setIsAttach(true);
        return autoAttachmentDto;
    }

}
