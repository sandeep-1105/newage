package com.efreightsuite.util;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import com.efreightsuite.exception.EDIGenerationException;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;

@Log4j2
public class EdiWriter {


    private StringBuffer message;

    private String separator;

    private String fileName;

    private String fileLocation;


    public EdiWriter() {
        this.message = new StringBuffer();
        this.separator = "/";
    }

    public String getSeparator() {
        return separator;
    }

    public void setSeparator(String separator) {
        this.separator = separator;
    }

    public StringBuffer getMessage() {
        return message;
    }

    public void write(String text, Integer maxLength) {
        if (text.length() > maxLength) {
            text = text.substring(0, maxLength);
        }
        message.append(text);
    }

    public void write(String text) {
        message.append(text);
    }

    public void addLine() {
        message.append("\n");
    }

    public void addSeparator() {
        message.append(getSeparator());
    }

    public void createEdiFile() {

        if (getFileLocation() == null || StringUtils.isEmpty(getFileLocation())) {
            throw new EDIGenerationException("Location not supplied " + getFileLocation());
        }

        if (getFileName() == null || StringUtils.isEmpty(getFileName())) {
            throw new EDIGenerationException("File name not supplied " + getFileName());
        }

        File dir = new File(getFileLocation() + "edi");
        if (!dir.exists()) {
            if (dir.mkdir()) {
                log.info("Directory is created!");
            } else {
                log.info("Failed to create directory!");
                throw new EDIGenerationException("Unable to create a directory on " + getFileLocation());
            }
        }


        File file = new File(dir + "/" + getFileName());

        try {

            FileWriter fileWriter = new FileWriter(file);

            fileWriter.write(this.message.toString());

            fileWriter.close();

            log.info("EDI file successfully created.....");

        } catch (IOException e) {
            throw new EDIGenerationException("EDI file not created " + getFileLocation());
        }
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFileLocation() {
        return fileLocation;
    }

    public void setFileLocation(String fileLocation) {
        this.fileLocation = fileLocation;
    }

}
