package com.efreightsuite.util;

import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.efreightsuite.annotation.Email;
import com.efreightsuite.annotation.Length;
import com.efreightsuite.annotation.Max;
import com.efreightsuite.annotation.Min;
import com.efreightsuite.annotation.NotNull;
import com.efreightsuite.annotation.NotNullOrEmpty;
import com.efreightsuite.annotation.Range;
import com.efreightsuite.annotation.Validate;
import com.efreightsuite.enumeration.EmploymentStatus;
import com.efreightsuite.enumeration.LovStatus;
import com.efreightsuite.enumeration.TransportMode;
import com.efreightsuite.enumeration.YesNo;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.model.PartyMaster;
import lombok.extern.log4j.Log4j2;

/**
 * The Class Validate.
 */

@Log4j2
public class ValidateUtil {

    public static String regularExp = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$";

    public static <E> E checkPattern(E object, String parameterName, boolean checkPattern) throws RestException {
        if (checkPattern) {
            checkPattern(object, parameterName);
        }
        return object;
    }

    public static void notNull(Object object, String errorCode) throws RestException {
        if (object == null) {
            throw new RestException(errorCode);
        }
    }

    public static void isStringEquals(String value1, String value2, String errorCode) throws RestException {
        if (value1 == null || value2 == null || !value1.trim().equals(value2.trim())) {
            throw new RestException(errorCode);
        }
    }

    public static void notEquals(Object src, Object target, String errorCode) throws RestException {
        if (!src.equals(target)) {
            throw new RestException(errorCode);
        }
    }

    public static <E> E checkPattern(E object, String parameterName) throws RestException {
        if (object == null) {
            return null;
        }
        String[] patternAndErrorMessage = ParameterFormat.getPatternAndErrorMessage(parameterName);

        if (patternAndErrorMessage == null) {
            return object;
        }
        String parameterPattern = patternAndErrorMessage[0];
        String errorMessage = patternAndErrorMessage[1];
        Pattern pattern = Pattern.compile(parameterPattern);
        Matcher matcher = pattern.matcher(object.toString());
        boolean matchFound = matcher.matches();
        if (!matchFound) {
            RestException ex = new RestException(errorMessage);
            throw ex;
        }
        return object;
    }

    public static <E> E checkPattern(E object, String regExp, String errorCode) throws RestException {
        if (object == null) {
            return null;
        }

        if (regExp == null) {
            return null;
        }

        Pattern pattern = Pattern.compile(regExp);
        Matcher matcher = pattern.matcher(object.toString());
        boolean matchFound = matcher.matches();
        if (!matchFound) {
            RestException ex = new RestException(errorCode.toString());
            throw ex;
        }
        return object;
    }

    public static void notEmpty(@SuppressWarnings("rawtypes") Collection list, String errorCode) throws RestException {
        if (list == null || list.size() == 0) {
            throw new RestException(errorCode);
        }
    }

    public static void notNullOrEmpty(String object, String errorCode) throws RestException {
        if (object == null || object.trim().length() == 0) {
            throw new RestException(errorCode);
        }
    }


    public static void validateLength(Object object, int min, int max, String errorCode) throws RestException {

        if (object != null) {
            if (object.toString().length() > max) {
                throw new RestException(errorCode);
            }

            if (object.toString().length() < min) {
                throw new RestException(errorCode);
            }
        }
    }

    public static void maxAndmin(double d, double e, String errorCode) throws RestException {

        if (d > e) {

            throw new RestException(errorCode);

        }
    }


    public static void validateEnum(Class<?> enumType, Object object, String errorCode) throws RestException {
        boolean flag = false;

        if (object != null) {
            for (Object obj : enumType.getEnumConstants()) {
                if (obj.equals(object)) {
                    flag = true;
                    break;
                }
            }
        }
        if (flag == false) {
            throw new RestException(errorCode);
        }
    }

    public static void checkDate(Date input, String errorCode) throws RestException {
        Date date = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        if (!sdf.format(date).equals(sdf.format(input))) {
            throw new RestException(errorCode);
        }

    }

    public static void dateNotNull(Date date, String errorCode) throws RestException {
        if (date == null) {
            throw new RestException(errorCode);
        }
    }

    public static void dateEmpty(Date date, String errorCode) throws RestException {
        if (date.equals("")) {
            throw new RestException(errorCode);
        }
    }

    public static void checkDecimalLength(Double value, String errorCode) {
        String text = Double.toString(Math.abs(value));
        int integerPlaces = text.indexOf('.');
        int decimalPlaces = text.length() - integerPlaces - 1;
        if (integerPlaces > 5 || decimalPlaces > 6) {
            throw new RestException(errorCode);
        }
    }

    public static void nonZero(Double value, String errorCode) throws RestException {
        if (value == 0) {
            throw new RestException(errorCode);
        }
    }

    public static void nonZero(Long value, String errorCode) throws RestException {
        if (value == 0) {
            throw new RestException(errorCode);
        }
    }

    public static void nonZero(int value, String errorCode) throws RestException {
        if (value == 0) {
            throw new RestException(errorCode);
        }
    }

    public static void isValidEmail(String enteredEmail, String errorCode) throws RestException {
        log.info("enteredEmail:" + enteredEmail);
        String EMAIL_REGIX = "^[\\\\w!#$%&’*+/=?`{|}~^-]+(?:\\\\.[\\\\w!#$%&’*+/=?`{|}~^-]+)*@(?:[a-zA-Z0-9-]+\\\\.)+[a-zA-Z]{2,6}$";
        Pattern pattern = Pattern.compile(EMAIL_REGIX);
        Matcher matcher = pattern.matcher(enteredEmail);
        if (!enteredEmail.isEmpty() && enteredEmail != null && matcher.matches()) {
            throw new RestException(errorCode);
        }
    }


    public static void isDefaulter(PartyMaster partyMaster, String errorCode) {
        if (partyMaster.getIsDefaulter() != null && partyMaster.getIsDefaulter().equals(YesNo.Yes)) {
            throw new RestException(errorCode);
        }
    }

    public static void isStatusBlocked(LovStatus status, String errorCode) {
        if (status.equals(LovStatus.Block)) {
            log.info("Status is Blocked. Throwing error code : " + errorCode);
            throw new RestException(errorCode);
        }
    }

    public static void isStatusHidden(LovStatus status, String errorCode) {
        if (status.equals(LovStatus.Hide)) {
            log.info("Status is Hidden. Throwing error code : " + errorCode);
            throw new RestException(errorCode);
        }
    }

    public static void isAirTransportMode(TransportMode transportMode, String errorCode) {

        log.info(" Air Transport. transportMode : " + transportMode);
        if (!transportMode.equals(TransportMode.Air)) {
            log.info("Not Air Transport. Throwing error code : " + errorCode);
            throw new RestException(errorCode);
        }
    }

    public static void isOceanTransportMode(TransportMode transportMode, String errorCode) {
        if (!transportMode.equals(TransportMode.Ocean)) {
            log.info("Not OCEAN Transport. Throwing error code : " + errorCode);
            throw new RestException(errorCode);
        }
    }

    public static void isRailTransportMode(TransportMode transportMode, String errorCode) {
        if (!transportMode.equals(TransportMode.Rail)) {
            log.info("Not RAIL Transport. Throwing error code : " + errorCode);
            throw new RestException(errorCode);
        }
    }

    public static void isRoadTransportMode(TransportMode transportMode, String errorCode) {
        if (!transportMode.equals(TransportMode.Road)) {
            log.info("Not Road Transport. Throwing error code : " + errorCode);
            throw new RestException(errorCode);
        }
    }

    public static void isNotEqualTransportMode(TransportMode transportMode, TransportMode serviceTransportMode, String errorCode) {

        if (!transportMode.equals(serviceTransportMode)) {
            log.info("Invaild isNotEqualTransportMode. Throwing error code : " + errorCode);
            throw new RestException(errorCode);
        }
    }


    public static void isEmployeeResigned(EmploymentStatus employementStatus, String errorCode) {
        if (employementStatus.equals(EmploymentStatus.RESIGNED)) {
            throw new RestException(errorCode);
        }
    }

    public static void isEmployeeTerminated(EmploymentStatus employementStatus, String errorCode) {
        if (employementStatus.equals(EmploymentStatus.TERMINATED)) {
            throw new RestException(errorCode);
        }
    }

    public static void isSalesman(YesNo isSalesman, String errorCode) {
        if (isSalesman.equals(YesNo.No)) {
            throw new RestException(errorCode);
        }
    }

    public static void validate(Object obj) {

        log.info("Validate Process..");
        try {
            if (obj.getClass().isAnnotationPresent(Validate.class)) {

                Validate validate = obj.getClass().getAnnotation(Validate.class);
                if (validate.value()) {
                    log.info(obj.getClass().getSimpleName() + " validation is set to true........");
                    log.info(obj.getClass().getSimpleName() + " is being validated.....");

                    Field[] fields = obj.getClass().getDeclaredFields();

                    for (Field f : fields) {

                        // Not Null
                        if (f.isAnnotationPresent(NotNull.class)) {
                            log.info(f.getName() + " is annotated with NotNull Annotation....................");
                            try {
                                f.setAccessible(true);
                                log.info(f.getName() + " is accessible.........................");
                                NotNull notNull = f.getAnnotation(NotNull.class);
                                Object value = f.get(obj);
                                log.info(f.getName() + " contains " + f.get(obj));
                                if (value == null) {
                                    log.info("RestException is being thrown........................");
                                    throw new RestException(notNull.errorCode());
                                }
                                log.info(f.getName() + " contains " + f.get(obj));
                            } catch (IllegalArgumentException | IllegalAccessException e) {
                                e.printStackTrace();
                            }
                        }

                        // NotNullOrEmpty
                        if (f.isAnnotationPresent(NotNullOrEmpty.class)) {
                            log.info(f.getName() + " is annotated with NotNullOrEmpty Annotation....................");
                            try {
                                f.setAccessible(true);
                                log.info(f.getName() + " is accessible.........................");

                                NotNullOrEmpty notNullOrEmpty = f.getAnnotation(NotNullOrEmpty.class);

                                String value = (String) f.get(obj);

                                log.info(f.getName() + " contains " + f.get(obj));

                                if (value == null || value.length() == 0) {
                                    log.info("RestException is being thrown........................");
                                    throw new RestException(notNullOrEmpty.errorCode());
                                }
                                log.info(f.getName() + " contains " + f.get(obj));
                            } catch (IllegalArgumentException | IllegalAccessException e) {
                                e.printStackTrace();
                            }
                        }

                        // Pattern
                        if (f.isAnnotationPresent(com.efreightsuite.annotation.Pattern.class)) {
                            log.info(f.getName() + " is annotated with Pattern Annotation....................");

                            try {
                                f.setAccessible(true);
                                log.info(f.getName() + " is accessible...........................");
                                com.efreightsuite.annotation.Pattern pattern = f
                                        .getAnnotation(com.efreightsuite.annotation.Pattern.class);
                                String value = (String) f.get(obj);
                                log.info(f.getName() + " contains " + f.get(obj) + "..................");
                                if (value != null) {
                                    String parameterPattern = pattern.regExp();
                                    log.info("given regExp : " + parameterPattern);
                                    String errorCode = pattern.errorCode();
                                    log.info("given errorCode : " + errorCode);
                                    Matcher matcher = Pattern.compile(parameterPattern)
                                            .matcher(value.toString());
                                    boolean matchFound = matcher.matches();
                                    if (!matchFound) {
                                        throw new RestException(errorCode);
                                    }
                                }
                            } catch (IllegalArgumentException | IllegalAccessException e) {
                                e.printStackTrace();
                            }
                        }

                        // Min
                        if (f.isAnnotationPresent(Min.class)) {
                            log.info(f.getName() + " is annotated with Min Annotation....................");
                            try {
                                f.setAccessible(true);
                                log.info(f.getName() + " is accessible.........................");

                                Min min = f.getAnnotation(Min.class);
                                int value = (int) f.get(obj);

                                log.info(f.getName() + " contains " + f.get(obj));

                                if (value < min.value()) {
                                    log.info("RestException is being thrown........................");
                                    throw new RestException(min.errorCode());
                                }
                                log.info(f.getName() + " contains " + f.get(obj));
                            } catch (IllegalArgumentException | IllegalAccessException e) {
                                e.printStackTrace();
                            }
                        }

                        // Max
                        if (f.isAnnotationPresent(Max.class)) {
                            log.info(f.getName() + " is annotated with Max Annotation....................");
                            try {
                                f.setAccessible(true);
                                log.info(f.getName() + " is accessible.........................");

                                Max max = f.getAnnotation(Max.class);
                                int value = (int) f.get(obj);

                                log.info(f.getName() + " contains " + f.get(obj));

                                if (value > max.value()) {
                                    log.info("RestException is being thrown........................");
                                    throw new RestException(max.errorCode());
                                }
                                log.info(f.getName() + " contains " + f.get(obj));
                            } catch (IllegalArgumentException | IllegalAccessException e) {
                                e.printStackTrace();
                            }
                        }

                        // Range
                        if (f.isAnnotationPresent(Range.class)) {
                            log.info(f.getName() + " is annotated with Range Annotation....................");
                            try {
                                f.setAccessible(true);
                                log.info(f.getName() + " is accessible.........................");

                                Range range = f.getAnnotation(Range.class);
                                int value = (int) f.get(obj);

                                log.info(f.getName() + " contains " + f.get(obj));

                                if (value < range.minValue() || value > range.maxValue()) {
                                    log.info("RestException is being thrown........................");
                                    throw new RestException(range.errorCode());
                                }
                                log.info(f.getName() + " contains " + f.get(obj));
                            } catch (IllegalArgumentException | IllegalAccessException e) {
                                e.printStackTrace();
                            }
                        }

                        // Length
                        if (f.isAnnotationPresent(Length.class)) {
                            log.info(f.getName() + " is annotated with Length Annotation....................");
                            try {
                                f.setAccessible(true);
                                log.info(f.getName() + " is accessible.........................");

                                Length length = f.getAnnotation(Length.class);
                                String value = (String) f.get(obj);

                                log.info(f.getName() + " contains " + f.get(obj));

                                if (value.length() < length.minValue() || value.length() > length.maxValue()) {
                                    log.info("RestException is being thrown........................");
                                    throw new RestException(length.errorCode());
                                }
                                log.info(f.getName() + " contains " + f.get(obj));
                            } catch (IllegalArgumentException | IllegalAccessException e) {
                                e.printStackTrace();
                            }
                        }

                        // Email
                        if (f.isAnnotationPresent(Email.class)) {
                            log.info(f.getName() + " is annotated with Email Annotation....................");

                            try {
                                f.setAccessible(true);
                                log.info(f.getName() + " is accessible...........................");
                                Email email = f.getAnnotation(Email.class);
                                String value = (String) f.get(obj);
                                log.info(f.getName() + " contains " + f.get(obj) + "..................");
                                if (value != null) {
                                    String parameterPattern = email.regExp();
                                    log.info("given regExp : " + parameterPattern);
                                    String errorCode = email.errorCode();
                                    log.info("given errorCode : " + errorCode);
                                    Matcher matcher = Pattern.compile(parameterPattern)
                                            .matcher(value.toString());
                                    boolean matchFound = matcher.matches();
                                    if (!matchFound) {
                                        throw new RestException(errorCode);
                                    }
                                }
                            } catch (IllegalArgumentException | IllegalAccessException e) {
                                e.printStackTrace();
                            }
                        }

                    }

                } else {
                    log.info(obj.getClass().getName() + " validation is set to false.....");
                }
            }
        } catch (Exception exception) {
            log.error(exception);
        }
    }

    public static void belowRange(double value, double minValue, String errorCode) {
        if (value < minValue) {
            throw new RestException(errorCode);
        }
    }

    public static void aboveRange(double value, double maxValue, String errorCode) {
        if (value > maxValue) {
            throw new RestException(errorCode);
        }
    }

    public static void decimalLength(double object, int min, int max, String errorCode) {

        if (object > max) {

            throw new RestException(errorCode);
        }
        if (object < min) {

            throw new RestException(errorCode);
        }

    }


    public static void isPortInCountry(String countryCode, String isCountry, String errorCode) {

        if (!countryCode.equals(isCountry)) {
            throw new RestException(errorCode);
        }

    }

	

	/*public static void checkDecimalPattern(double object, int intvalue, int decimalvalue,String errorCode) {
	
		int integerLength = (int) Math.log10((int)object) + 1; 
		
		if(integerLength>intvalue){
			
			throw new RestException(errorCode);
		}
		
		String temp = "." + String.valueOf(object).split( "\\." )[1];
		double decimalPart = Double.parseDouble( temp );
		
		int decimalLength = (int) Math.log10((int)decimalPart) + 1; 
		
      if(decimalLength>decimalvalue){
			
			throw new RestException(errorCode);
		}
		
	}*/

}
