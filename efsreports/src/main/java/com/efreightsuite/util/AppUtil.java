package com.efreightsuite.util;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import com.efreightsuite.common.component.CacheRepository;
import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.enumeration.*;
import com.efreightsuite.model.DefaultMasterData;
import com.efreightsuite.model.LocationMaster;
import com.efreightsuite.repository.DefaultMasterDataRepository;
import com.ibm.icu.text.DecimalFormat;
import com.ibm.icu.text.DecimalFormatSymbols;
import com.ibm.icu.text.NumberFormat;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class AppUtil {

    private static final double BASE = 1024, KB = BASE, MB = KB * BASE, GB = MB * BASE;
    @Autowired
    CacheRepository cacheRepository;
    @Autowired
    DefaultMasterDataRepository defaultMasterDataRepository;

    public static String getCurrencyFormat(Double amount, String locale, String decimalPoint) {
        if (amount == null) {
            return "";
        }
        if (locale == null) {
            return "";
        }
        Locale loc = new Locale(locale.split("_")[0], locale.split("_")[0]);
        NumberFormat format = com.ibm.icu.text.NumberFormat.getCurrencyInstance(loc);
        DecimalFormatSymbols dfs = ((DecimalFormat) format).getDecimalFormatSymbols();
        dfs.setCurrencySymbol("");
        DecimalFormat df = (DecimalFormat) NumberFormat.getCurrencyInstance(loc);
        df.setDecimalFormatSymbols(dfs);
        df.setMaximumFractionDigits(Integer.parseInt(decimalPoint));
        df.setMinimumFractionDigits(Integer.parseInt(decimalPoint));
        return df.format(amount);
    }

    public static String getWeightFormat(Double number, String locale) {
        if (number == null) {
            return "";
        }
        Locale loc = new Locale(locale.split("_")[0], locale.split("_")[0]);
        NumberFormat format = com.ibm.icu.text.NumberFormat.getCurrencyInstance(loc);
        DecimalFormatSymbols dfs = ((DecimalFormat) format).getDecimalFormatSymbols();
        dfs.setCurrencySymbol("");
        DecimalFormat df = (DecimalFormat) NumberFormat.getCurrencyInstance(loc);
        df.setDecimalFormatSymbols(dfs);
        df.setMaximumFractionDigits(2);
        df.setMinimumFractionDigits(2);
        return df.format(number);
    }

    public static String getNumberFormat(Long number, String locale) {
        if (number == null) {
            return "";
        }
        Locale loc = new Locale(locale.split("_")[0], locale.split("_")[0]);
        NumberFormat format = com.ibm.icu.text.NumberFormat.getCurrencyInstance(loc);
        DecimalFormatSymbols dfs = ((DecimalFormat) format).getDecimalFormatSymbols();
        dfs.setCurrencySymbol("");
        DecimalFormat df = (DecimalFormat) NumberFormat.getCurrencyInstance(loc);
        df.setDecimalFormatSymbols(dfs);
        df.setMaximumFractionDigits(0);
        df.setMinimumFractionDigits(0);
        return df.format(number);
    }

    public static String getNumberFormat(Integer number, String locale) {
        if (number == null) {
            return "";
        }
        Locale loc = new Locale(locale.split("_")[0], locale.split("_")[0]);
        NumberFormat format = com.ibm.icu.text.NumberFormat.getCurrencyInstance(loc);
        DecimalFormatSymbols dfs = ((DecimalFormat) format).getDecimalFormatSymbols();
        dfs.setCurrencySymbol("");
        DecimalFormat df = (DecimalFormat) NumberFormat.getCurrencyInstance(loc);
        df.setDecimalFormatSymbols(dfs);
        df.setMaximumFractionDigits(0);
        df.setMinimumFractionDigits(0);
        return df.format(number);
    }

    public static String formatSize(double size) {
        DecimalFormat df = new DecimalFormat("#.##");
        if (size >= GB) {
            return df.format(size / GB) + " GB";
        }
        if (size >= MB) {
            return df.format(size / MB) + " MB";
        }
        if (size >= KB) {
            return df.format(size / KB) + " KB";
        }
        return "" + (int) size + " bytes";
    }

    public String getLocationConfig(String key, LocationMaster location, boolean upperCase) {

        DefaultMasterData data = defaultMasterDataRepository.getCodeAndLocation(key, location.getId());

        if (data == null || data.getValue() == null) {
            return "";
        }

        if (upperCase) {
            return data.getValue().toUpperCase().trim();
        } else {
            return data.getValue().trim();
        }

    }

    public BaseDto setDesc(BaseDto dto) {
        log.debug("Response Message being sent to the client : " + dto.getResponseCode() + " --> " + getDesc(dto.getResponseCode()));
        dto.setResponseDescription(getDesc(dto.getResponseCode()));
        return dto;
    }

    public String getDesc(String code) {
        Map<String, String> codeMap = cacheRepository.getNLS(SaaSUtil.getSaaSId(), "English");
        return codeMap.get(code);
    }

    public List<Class> getAllClasses(String pckgname) {
        try {
            List<Class> classes = new ArrayList();
            /*
			 * // Get a File object for the package File directory = null; try {
			 * directory = new File(URLDecoder.decode(Thread.currentThread().
			 * getContextClassLoader() .getResource(pckgname.replace('.',
			 * '/')).getFile(), "UTF-8"));
			 *
			 * } catch (NullPointerException x) { log.error("Nullpointer :", x);
			 * throw new ClassNotFoundException(pckgname +
			 * " does not appear to be a valid package"); }
			 *
			 * log.info("ENUM PATH :" + directory.getAbsolutePath());
			 *
			 * if (directory.exists()) { // Get the list of the files contained
			 * in the package String[] files = directory.list(); for (int i = 0;
			 * i < files.length; i++) { // we are only interested in .class
			 * files if (files[i].endsWith(".class")) { // removes the .class
			 * extension classes.add(Class.forName(pckgname + '.' +
			 * files[i].substring(0, files[i].length() - 6))); } } } else {
			 * log.error("Directory does not exist"); throw new
			 * ClassNotFoundException(pckgname +
			 * " does not appear to be a valid package"); }
			 */

            classes.add(AddressType.class);
            classes.add(BillToType.class);
            classes.add(BooleanType.class);
            classes.add(ChargeCalculationType.class);
            classes.add(ChargeType.class);
            classes.add(CorporateNonCorporate.class);
            classes.add(EmailStatus.class);
            classes.add(FullGroupage.class);
            classes.add(ActualChargeable.class);
            classes.add(ImportExport.class);
            classes.add(JobStatus.class);
            classes.add(LovStatus.class);
            classes.add(MessageSendType.class);
            classes.add(PPCC.class);
            classes.add(ServiceCode.class);
            classes.add(ServiceStatus.class);
            classes.add(EmploymentStatus.class);

            classes.add(Status.class);
            classes.add(StatusOfShipment.class);
            classes.add(TermCode.class);
            classes.add(TradeCode.class);
            classes.add(TransitMode.class);
            classes.add(TransportMode.class);
            classes.add(UnitCalculationType.class);
            classes.add(WhoRouted.class);
            classes.add(ReferenceType.class);
            classes.add(SessionStatus.class);
            classes.add(UnitType.class);
            classes.add(MappingUnit.class);
            classes.add(UnitDecimals.class);
            classes.add(SequenceType.class);
            classes.add(SequenceFormat.class);
            classes.add(SalutationType.class);
            classes.add(IdType.class);
            classes.add(BondType.class);
            classes.add(FillingType.class);
            classes.add(FillingBy.class);
            classes.add(ConsigneeType.class);
            classes.add(BondActivityCode.class);
            classes.add(QuoteType.class);
            classes.add(Approved.class);
            classes.add(Frequency.class);
            classes.add(MailEventType.class);
            classes.add(TermCode.class);
            classes.add(CurrentPotential.class);
            classes.add(PartyBusinessStatus.class);
            classes.add(TaxExempted.class);
            classes.add(Reason.class);
            classes.add(ConnectionStatus.class);
            classes.add(DUE.class);
            classes.add(CRN.class);
            classes.add(UserStatus.class);
            classes.add(FlightPlanStatus.class);
            classes.add(StockStatus.class);
            classes.add(CustomerAgent.class);
            classes.add(InvoiceCreditNoteStatus.class);
            classes.add(DebitCredit.class);
            classes.add(RateClass.class);
            classes.add(EventMasterType.class);
            classes.add(DocumentType.class);
            classes.add(PersonalEvent.class);
            classes.add(RelationShip.class);
            classes.add(SignOffUnsignOff.class);
            classes.add(DocPrefixDocumentType.class);
            classes.add(AesAction.class);
            classes.add(AesInBondType.class);
            classes.add(OriginOfGoods.class);
            classes.add(RemarkType.class);
            classes.add(YesNo.class);
            classes.add(DateLogic.class);
            classes.add(WhichTransactionDate.class);
            classes.add(EdiMessagingFor.class);
            classes.add(ServiceType.class);
            classes.add(Gender.class);
            classes.add(MaritalStatus.class);
            classes.add(ProbationStatus.class);
            classes.add(ScopeFlag.class);
            classes.add(Priority.class);
            classes.add(ReportName.class);
            classes.add(PageCode.class);
            classes.add(ShipmentMovement.class);
            classes.add(com.efreightsuite.enumeration.Service.class);

            return classes;

        } catch (Exception e) {
            log.error("Exception getting all enum class:", e);
            return null;
        }
    }

    public Date removeTime(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return cal.getTime();
    }

}
