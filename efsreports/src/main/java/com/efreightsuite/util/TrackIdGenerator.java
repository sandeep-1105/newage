package com.efreightsuite.util;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import com.efreightsuite.service.AuthService;
import lombok.extern.log4j.Log4j2;

import org.springframework.stereotype.Service;

@Service
@Log4j2
public class TrackIdGenerator {

    private String ipAddress = null;

    public String getTrackId(HttpServletRequest httpRequest) {
        StringBuilder sb = new StringBuilder(8);

        String saas = httpRequest.getHeader("SAAS_ID");

        if (saas != null && saas.trim().length() != 0) {
            sb.append("SAAS#" + saas + "#-");
        }

        if (ipAddress == null) {
            getIP();
        }

        sb.append(ipAddress);

        sb.append("-");

        sb.append(UUID.randomUUID().toString());
        sb.append("-");

        sb.append(getServerDateTime());

        try {
            if (AuthService.getCurrentUser() != null) {
                sb.append("-");
                sb.append(AuthService.getCurrentUser().getUserName());
            }
        } catch (Exception e) {
            log.error("User Id not found when login api called", e);
        }


        return sb.toString();
    }

    private String getIP() {
        try {
            InetAddress ip = InetAddress.getLocalHost();
            ipAddress = ip.getHostAddress();
        } catch (UnknownHostException e) {
            log.warn("UnknownHostException : ", e);
            ipAddress = "";
        }
        return ipAddress;
    }

    private String getServerDateTime() {

        return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
    }
}
