package com.efreightsuite.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.ReportDownloadRequestDto;
import com.efreightsuite.dto.SaaSDto;
import com.efreightsuite.report.service.GenerateReportService;
import com.efreightsuite.util.SaaSUtil;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Log4j2
@RequestMapping(value = "/api/v1/report")
public class ReportController {

    @Autowired
    GenerateReportService generateReportService;

    @RequestMapping(value = "/download", method = RequestMethod.POST)
    public void process(@RequestBody ReportDownloadRequestDto data, HttpServletResponse response) {

        log.info("ReportController Class -> process Mtd single " + SaaSUtil.getSaaSId());

        generateReportService.process(data, response);
    }

    @RequestMapping(value = "/download/multiple", method = RequestMethod.POST)
    public void processMultiple(@RequestBody ReportDownloadRequestDto data)
            throws IOException {
        log.info("ReportController  -> process Multiple Controller called");
        if (data.getIsMultiple() != null && data.getIsMultiple().equals(true)) {
            generateReportService.processMultipleReport(data);
        }

    }

    @RequestMapping(value = "/saas/added", method = RequestMethod.POST)
    public BaseDto process(@RequestBody SaaSDto data) {
        log.info("ReportController Class -> process Mtd single ");
        return generateReportService.saasAdded(data);
    }
}
