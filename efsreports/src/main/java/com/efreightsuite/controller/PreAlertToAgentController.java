package com.efreightsuite.controller;

import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.ReportEmailRequestDto;
import com.efreightsuite.report.service.PreAlerttoAgentReportService;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Log4j2
@RequestMapping(value = "/api/v1/preAlert")
public class PreAlertToAgentController {

    @Autowired
    PreAlerttoAgentReportService preAlerttoAgentReportService;


    @RequestMapping(value = "/reportlength", method = RequestMethod.GET)
    public ReportEmailRequestDto reportLength(String consolUid) {
        log.info("PreAlert Agent Controller ----->Individual report byte array length");

        return preAlerttoAgentReportService.getLengthOfBytes(consolUid);
    }

    @RequestMapping(value = "/mailtoagent", method = RequestMethod.POST)
    public BaseDto sendMailToAgent(@RequestBody ReportEmailRequestDto reportEmailRequestDto) {
        log.info("PreAlert Agent Controller ----->Send a Mail to...");
        return preAlerttoAgentReportService.sendMailToAgent(reportEmailRequestDto);

    }

}
