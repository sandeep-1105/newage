package com.efreightsuite.configuration;

import java.util.HashMap;
import java.util.Map;

import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.exception.ErrorCode;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.dbcp2.BasicDataSource;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class TenantService {

    @Value("${newage.saas.jdbc.maxtotal}")
    int maxtotal;
    @Value("${newage.saas.jdbc.maxidle}")
    int maxidle;
    @Value("${newage.saas.jdbc.minidle}")
    int minidle;
    @Value("${newage.saas.jdbc.validationquery}")
    String validationquery;
    @Value("${newage.saas.jdbc.validationquerytimeout}")
    int validationquerytimeout;
    @Getter
    @Setter
    private SessionRoutingDataSource sessionRoutingDataSource;
    private Map<String, String> saasMap;

    private Object tempdefaultDataSources;

    private HashMap<Object, Object> tempTargetDataSources = new HashMap<>();

    public Map<String, String> getSaasMap() {
        return saasMap;
    }

    public void setSaasMap(Map<String, String> saasMap) {
        this.saasMap = saasMap;
    }

    public HashMap<Object, Object> getTempTargetDataSources() {
        return tempTargetDataSources;
    }

    public void setTempTargetDataSources(HashMap<Object, Object> tempTargetDataSources) {
        this.tempTargetDataSources = tempTargetDataSources;
    }

    public Object getTempdefaultDataSources() {
        return tempdefaultDataSources;
    }

    public void setTempdefaultDataSources(Object tempdefaultDataSources) {
        this.tempdefaultDataSources = tempdefaultDataSources;
    }

    public BaseDto loadNewDataSource(String saasId, String companyName, String driverName, String url, String userName, String password) {
        BasicDataSource ds = new BasicDataSource();
        ds.setDriverClassName(driverName);
        ds.setUrl(url);
        ds.setUsername(userName);
        ds.setPassword(password);
        ds.setMaxTotal(maxtotal);
        ds.setMaxIdle(maxidle);
        ds.setMinIdle(minidle);
        ds.setValidationQuery(validationquery);
        ds.setValidationQueryTimeout(validationquerytimeout);

        getTempTargetDataSources().put(saasId, ds);

        getSaasMap().put(saasId, companyName);

        sessionRoutingDataSource.setDataSources(getTempTargetDataSources());

        BaseDto dto = new BaseDto();
        dto.setResponseCode(ErrorCode.SUCCESS);

        return dto;
    }
}
