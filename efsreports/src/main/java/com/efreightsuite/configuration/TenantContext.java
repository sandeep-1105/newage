package com.efreightsuite.configuration;

public class TenantContext {
    private static InheritableThreadLocal<Object> currentTenant = new InheritableThreadLocal<>();

    public static Object getCurrentTenant() {
        return currentTenant.get();
    }

    public static void setCurrentTenant(Object tenant) {
        currentTenant.set(tenant);
    }
}
