package com.efreightsuite.service;

import java.util.Date;

import javax.transaction.Transactional;
import javax.transaction.Transactional.TxType;

import com.efreightsuite.common.component.CacheRepository;
import com.efreightsuite.enumeration.SequenceType;
import com.efreightsuite.enumeration.YesNo;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.model.LocationMaster;
import com.efreightsuite.model.SequenceGenerator;
import com.efreightsuite.repository.SequenceGeneratorRepository;
import com.efreightsuite.util.AppUtil;
import com.efreightsuite.util.TimeUtil;
import com.efreightsuite.validation.SequenceGeneratorValidator;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class SequenceGeneratorService {

    @Autowired
    SequenceGeneratorRepository sequenceGeneratorRepository;

    @Autowired
    CacheRepository cacheRepository;

    @Autowired
    SequenceGeneratorValidator sequenceGeneratorValidator;

    @Autowired
    AppUtil appUtil;

    @Transactional(value = TxType.REQUIRES_NEW)
    public String getSequenceValueByType(SequenceType sequenceType, LocationMaster locationMaster) {

        return getSequence(sequenceType, locationMaster);
    }

    @Transactional(value = TxType.REQUIRES_NEW)
    public String getSequenceValueByType(SequenceType sequenceType) {

        return getSequence(sequenceType);
    }

    public String getSequence(SequenceType sequenceType, LocationMaster locationMaster) {
        log.info("SequenceGeneratorService.getCurrentSequenceValueByType start");

        String genaratedSequence = nextSequence(sequenceType, locationMaster);

        log.info("Current Sequence Value : " + genaratedSequence);

        return genaratedSequence;

    }

    public String getSequence(SequenceType sequenceType) {
        log.info("SequenceGeneratorService.getCurrentSequenceValueByType start");

        String genaratedSequence = nextSequence(sequenceType, null);

        log.info("Current Sequence Value : " + genaratedSequence);

        return genaratedSequence;

    }

    private String nextSequence(SequenceType sequenceType, LocationMaster locationMaster) {

        SequenceGenerator sequence = sequenceGeneratorRepository.findBySequenceType(sequenceType);

        if (sequence == null) {
            throw new RestException(ErrorCode.SEQUENCE_NOT_PRESENT);
        }

        String genaratedSequence = "";

        String separator = "";

        if (sequence.getSeparator() != null && sequence.getSeparator().trim().length() != 0) {
            separator = sequence.getSeparator().trim();
        }

        if (sequence.getPrefix() != null && sequence.getPrefix().trim().length() != 0) {
            if (genaratedSequence.trim().length() == 0) {
                genaratedSequence = genaratedSequence + sequence.getPrefix().trim();
            } else {
                genaratedSequence = genaratedSequence + separator + sequence.getPrefix().trim();
            }
        }

        if (sequence.getIsYear() != null && sequence.getIsYear() == YesNo.Yes) {

            Date dt = null;

            if (locationMaster == null) {
                dt = TimeUtil.getCurrentLocationTime();
            } else {
                dt = TimeUtil.getCurrentLocationTime(locationMaster);
            }


            if (genaratedSequence.trim().length() == 0) {
                genaratedSequence = genaratedSequence + dt.getYear();
            } else {
                genaratedSequence = genaratedSequence + separator + dt.getYear();
            }
        }

        if (sequence.getIsMonth() != null && sequence.getIsMonth() == YesNo.Yes) {

            Date dt = null;

            if (locationMaster == null) {
                dt = TimeUtil.getCurrentLocationTime();
            } else {
                dt = TimeUtil.getCurrentLocationTime(locationMaster);
            }

            if (genaratedSequence.trim().length() == 0) {
                genaratedSequence = genaratedSequence + dt.getMonth();
            } else {
                genaratedSequence = genaratedSequence + separator + dt.getMonth();
            }
        }

        if (genaratedSequence.trim().length() == 0) {
            genaratedSequence = genaratedSequence + String.format(sequence.getFormat().getFormat(), sequence.getCurrentSequenceValue());
        } else {
            genaratedSequence = genaratedSequence + separator + String.format(sequence.getFormat().getFormat(), sequence.getCurrentSequenceValue());
        }

        if (sequence.getSuffix() != null && sequence.getSuffix().trim().length() != 0) {
            if (genaratedSequence.trim().length() == 0) {
                genaratedSequence = genaratedSequence + sequence.getSuffix().trim();
            } else {
                genaratedSequence = genaratedSequence + separator + sequence.getSuffix().trim();
            }
        }

        sequence.setCurrentSequenceValue(sequence.getCurrentSequenceValue() + 1);
        sequenceGeneratorRepository.save(sequence);

        return genaratedSequence;
    }


}
