package com.efreightsuite.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.efreightsuite.common.component.EmailRequestService;
import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.LoginRequest;
import com.efreightsuite.enumeration.LoginStatus;
import com.efreightsuite.enumeration.SessionStatus;
import com.efreightsuite.enumeration.YesNo;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.model.EmailRequest;
import com.efreightsuite.model.LocationMaster;
import com.efreightsuite.model.LoginHistory;
import com.efreightsuite.model.Role;
import com.efreightsuite.model.RoleHasFeature;
import com.efreightsuite.model.UserCompany;
import com.efreightsuite.model.UserCompanyLocation;
import com.efreightsuite.model.UserProfile;
import com.efreightsuite.repository.EmployeeMasterRepository;
import com.efreightsuite.repository.LocationMasterRepository;
import com.efreightsuite.repository.LoginHistoryRepository;
import com.efreightsuite.repository.LogoMasterRepository;
import com.efreightsuite.repository.ResetPasswordHistoryRepository;
import com.efreightsuite.repository.UserProfileRepository;
import com.efreightsuite.service.mailer.MailerUtil;
import com.efreightsuite.util.AppUtil;
import com.efreightsuite.util.SaaSUtil;
import com.efreightsuite.util.ValidateUtil;
import eu.bitwalker.useragentutils.UserAgent;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class AuthService {

    @Value("${session.timeout}")
    int sessionTimeout;

    @Autowired
    MailerUtil mailerUtil;

    @Autowired
    LoginHistoryRepository loginHistoryRepo;

    @Autowired
    EmailRequestService emailRequestService;

    @Autowired
    AppUtil appUtil;

    @Autowired
    UserProfileRepository userProfileRepository;

    @Autowired
    EmployeeMasterRepository employeeRepository;

    @Autowired
    ResetPasswordHistoryRepository resetPasswordHistoryRepository;

    @Autowired
    LogoMasterRepository logoMasterRepository;

    @Autowired
    LocationMasterRepository locationMasterRepository;

    @Autowired
    ReportMasterService reportMasterService;

    public static UserProfile getCurrentUser() {

        Object authObject = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (authObject == null || authObject.equals("anonymousUser")) {
            return null;
        } else {
            return (UserProfile) authObject;
        }
    }

    public BaseDto login(LoginRequest loginRequest, HttpServletRequest request) {

        log.debug("Login Method is Invoked");

        BaseDto respDto = new BaseDto();

        try {

            ValidateUtil.notNull(loginRequest.getUserName(), ErrorCode.UNAUTHORIZED_USER);

            ValidateUtil.notNull(loginRequest.getPassword(), ErrorCode.UNAUTHORIZED_USER);

            UserProfile userProfile = userProfileRepository.findByUserName(loginRequest.getUserName());
            log.info(userProfile);
            if (userProfile == null) {
                throw new RestException(ErrorCode.UNAUTHORIZED_USER);
            }

            if (userProfile.getUserCompanyList() == null || userProfile.getUserCompanyList().size() == 0) {
                throw new RestException(ErrorCode.USER_LOCATION_NOT_ASSOCIATE);
            } else {
                if (userProfile.getUserCompanyList().size() == 1) {
                    userProfile.setEmployee(userProfile.getUserCompanyList().get(0).getEmployee());
                    userProfile.setSelectedCompany(userProfile.getUserCompanyList().get(0).getCompanyMaster());
                    if (userProfile.getUserCompanyList().get(0).getUserCompanyLocationList() == null ||
                            userProfile.getUserCompanyList().get(0).getUserCompanyLocationList().size() == 0) {
                        throw new RestException(ErrorCode.USER_LOCATION_NOT_ASSOCIATE);
                    }

                    if (userProfile.getUserCompanyList().get(0).getUserCompanyLocationList().size() == 1) {
                        userProfile.setSelectedUserLocation(userProfile.getUserCompanyList().get(0).getUserCompanyLocationList().get(0).getLocationMaster());
                    } else {
                        boolean isFound = false;
                        for (UserCompanyLocation ucl : userProfile.getUserCompanyList().get(0).getUserCompanyLocationList()) {
                            if (ucl.getYesNo() == YesNo.Yes || ucl.getYesNo().equals(YesNo.Yes)) {
                                userProfile.setSelectedUserLocation(ucl.getLocationMaster());
                                isFound = true;
                                break;
                            }
                        }
                        if (!isFound) {
                            throw new RestException(ErrorCode.USER_LOCATION_NOT_ASSOCIATE);
                        }
                    }


                } else {
                    boolean isFound = false;
                    for (UserCompany uc : userProfile.getUserCompanyList()) {
                        if (uc.getYesNo() == YesNo.Yes || uc.getYesNo().equals(YesNo.Yes)) {
                            userProfile.setEmployee(uc.getEmployee());
                            userProfile.setSelectedCompany(uc.getCompanyMaster());
                            if (uc.getUserCompanyLocationList() == null || uc.getUserCompanyLocationList().size() == 0) {
                                throw new RestException(ErrorCode.USER_LOCATION_NOT_ASSOCIATE);
                            }

                            if (uc.getUserCompanyLocationList().size() == 1) {
                                userProfile.setSelectedUserLocation(uc.getUserCompanyLocationList().get(0).getLocationMaster());
                            } else {

                                for (UserCompanyLocation ucl : uc.getUserCompanyLocationList()) {
                                    if (ucl.getYesNo() == YesNo.Yes || ucl.getYesNo().equals(YesNo.Yes)) {
                                        userProfile.setSelectedUserLocation(ucl.getLocationMaster());
                                        isFound = true;
                                        break;
                                    }
                                }

                            }
                            break;
                        }
                    }

                    if (!isFound) {
                        throw new RestException(ErrorCode.USER_LOCATION_NOT_ASSOCIATE);
                    }
                }

            }


            if (userProfile != null && loginRequest.getUserName().equals(userProfile.getUserName())
                    && userProfile.getPassword().equals(loginRequest.getPassword())
                    && userProfile.getResetTokenKey() == null) {

                respDto.setResponseCode(ErrorCode.SUCCESS);

                createLoginHistory(userProfile, request, LoginStatus.SUCCESS, SessionStatus.CREATED);

                userProfile.setPassword(null);

                LocationMaster location = locationMasterRepository.findById(userProfile.getSelectedUserLocation().getId());
                userProfile.setSelectedUserLocation(location);
                userProfile.getSelectedUserLocation().setTmpPartyMaster(userProfile.getSelectedUserLocation().getPartyMaster());

                getFeatureList(userProfile);

                respDto.setResponseObject(userProfile);

                // Create a session id if login is successful
                HttpSession session = request.getSession(true);
                session.setMaxInactiveInterval(sessionTimeout);

                log.debug("Session timeout interval " + session.getMaxInactiveInterval());
                log.debug("Session created " + session.getId());

                session.setAttribute("userProfile", userProfile);

                List<GrantedAuthority> authorities = new ArrayList<>();

                for (String key : userProfile.getFeatureMap().keySet()) {
                    authorities.add(new SimpleGrantedAuthority("ROLE_" + key));
                    log.info(key);
                }

                Authentication authentication = new UsernamePasswordAuthenticationToken(userProfile,
                        SaaSUtil.getSaaSId(), authorities);

                SecurityContextHolder.getContext().setAuthentication(authentication);

                log.info("Login success.");


            } else {

                log.info("Login failed.");

                respDto.setResponseCode(ErrorCode.UNAUTHORIZED_USER);

                if (userProfile != null && userProfile.getEmployee() != null) {
                    createLoginHistory(userProfile, request, LoginStatus.FAILED, null);
                }
            }
        } catch (RestException exception) {
            log.error("Exception Occured with error code ", exception);
            respDto.setResponseCode(exception.getMessage());
        } catch (Exception e) {
            log.error("Exception ", e);
            respDto.setResponseCode(ErrorCode.ERROR_GENERIC);
        }

        return appUtil.setDesc(respDto);

    }

    private void getFeatureList(UserProfile userProfile) {

        Map<String, Boolean> map = new HashMap<>();

        for (Role role : userProfile.getRoleList()) {

            if (role.getFeatureList() == null || role.getFeatureList().size() == 0) {
                continue;
            }

            for (RoleHasFeature rhf : role.getFeatureList()) {

                if (rhf.getValList() != null) {
                    map.put(rhf.getValList(), true);
                }

                if (rhf.getValCreate() != null) {
                    map.put(rhf.getValCreate(), true);
                }

                if (rhf.getValModify() != null) {
                    map.put(rhf.getValModify(), true);
                }

                if (rhf.getValView() != null) {
                    map.put(rhf.getValView(), true);
                }

                if (rhf.getValDelete() != null) {
                    map.put(rhf.getValDelete(), true);
                }

                if (rhf.getValDownload() != null) {
                    map.put(rhf.getValDownload(), true);
                }
            }
        }
        userProfile.setFeatureMap(map);
        userProfile.setRoleList(null);
    }

    public void logout(HttpServletRequest request) {

        log.debug("Logout Method is Invoked");

        HttpSession session = request.getSession(false);

        if (session != null) {

            List<LoginHistory> loginHistoryList = loginHistoryRepo
                    .findBySessionIdAndSessionStatus(request.getSession().getId(), SessionStatus.CREATED);

            if (loginHistoryList != null && loginHistoryList.size() != 0) {

                for (LoginHistory loginHistory : loginHistoryList) {

                    loginHistory.setSessionStatus(SessionStatus.INVALIDATED);

                    loginHistory.setLogoutTime(new Date());

                }

                loginHistoryRepo.save(loginHistoryList);
            }

            session.setAttribute("LOGOUTSTATUS", "USER_LOGOUT");

            session.invalidate();

        } else {

            log.debug("Session is already invalidated");
        }
    }

    private LoginHistory createLoginHistory(UserProfile userProfile, HttpServletRequest request,
                                            LoginStatus loginStatus, SessionStatus sessionStatus) {

        LoginHistory loginHistory = new LoginHistory();

        loginHistory.setUserProfile(userProfile);
        loginHistory.setUserName(userProfile.getUserName());
        loginHistory.setEmployee(userProfile.getEmployee());
        loginHistory.setEmployeeCode(userProfile.getEmployee().getEmployeeCode());
        loginHistory.setSessionId(request.getSession().getId());
        loginHistory.setLoginTime(new Date());

        UserAgent userAgent = UserAgent.parseUserAgentString(request.getHeader("User-Agent"));
        if (userAgent != null) {
            if (userAgent.getBrowser() != null) {
                loginHistory.setBrowser(userAgent.getBrowser().getName());
            }

            if (userAgent.getBrowserVersion() != null) {
                loginHistory.setBrowserVersion(userAgent.getBrowserVersion().getVersion());
            }

            if (userAgent.getOperatingSystem() != null) {
                loginHistory.setPlatform(userAgent.getOperatingSystem().getName());
            }

            if (userAgent.getOperatingSystem().getDeviceType() != null) {
                loginHistory.setDeviceName(userAgent.getOperatingSystem().getDeviceType().getName());
            }
        }

        String ipAddress = request.getHeader("X-FORWARDED-FOR");

        if (ipAddress == null) {
            ipAddress = request.getRemoteAddr();
        }

        loginHistory.setSourceIp(ipAddress);
        loginHistory
                .setSaasId(SaaSUtil.getSaaSId() == "" || SaaSUtil.getSaaSId() == null ? null : SaaSUtil.getSaaSId());
        loginHistory.setSessionStatus(sessionStatus);
        loginHistory.setLoginStatus(loginStatus);

        loginHistoryRepo.save(loginHistory);

        log.debug("Login History Created [ " + userProfile.getUserName() + " ]");

        return loginHistory;
    }

    @Async
    public void processEmail(EmailRequest emailRequest, UserProfile userProfile) {
        emailRequestService.saveAndSendEmail(emailRequest, userProfile, mailerUtil.getLogo(userProfile.getSelectedUserLocation()));
    }
}
