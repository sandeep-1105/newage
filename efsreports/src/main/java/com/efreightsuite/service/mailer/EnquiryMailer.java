package com.efreightsuite.service.mailer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.efreightsuite.common.component.EmailRequestService;
import com.efreightsuite.enumeration.TemplateType;
import com.efreightsuite.enumeration.TransportMode;
import com.efreightsuite.model.CompanyMaster;
import com.efreightsuite.model.EmailRequest;
import com.efreightsuite.model.EmailTemplate;
import com.efreightsuite.model.EnquiryDetail;
import com.efreightsuite.model.EnquiryLog;
import com.efreightsuite.model.ServiceMaster;
import com.efreightsuite.model.UserProfile;
import com.efreightsuite.repository.EmailTemplateRepository;
import com.efreightsuite.repository.LocationMasterRepository;
import com.efreightsuite.repository.LogoMasterRepository;
import com.efreightsuite.util.ReportUtil;
import com.efreightsuite.util.SaaSUtil;
import com.efreightsuite.util.TimeUtil;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class EnquiryMailer {

    @Value("${efs.application.base.url}")
    String applicationBaseUrl;

    @Autowired
    MailerUtil mailerUtil;

    @Autowired
    ReportUtil reportUtil;

    @Autowired
    EmailRequestService emailRequestService;

    @Autowired
    EmailTemplateRepository emailTemplateRepository;

    @Autowired
    LogoMasterRepository logoMasterRepository;

    @Autowired
    LocationMasterRepository locationMasterRepository;

    @Async
    public void sendEnquiryMailToSalesDepo(EnquiryLog enquiryLog, UserProfile userProfile) {
        log.info("sendEnquiryAutoMail start: ");

        Map<Long, List<EnquiryDetail>> groupService = edListGroupByServiceName(enquiryLog.getEnquiryDetailList());

        if (groupService == null || groupService.size() == 0) {
            log.info("New Service is not available");
            return;
        }

        EmailTemplate emailTemplate = emailTemplateRepository.findByTemplateType(TemplateType.ENQUIRYCONFTOSALSE);

        if (emailTemplate == null) {
            log.error("Enquiry confirmation to sales man mail is not available");
            return;
        }

        Map<String, Object> mapSubject = new HashMap<>();
        mapSubject.put("#ENQUIRY_NO#", enquiryLog.getEnquiryNo());

        Map<String, Object> mapBody = new HashMap<>();

        mapBody.put("#COMPANY_NAME#", getCompanyName(enquiryLog.getCompanyMaster()));
        mapBody.put("#ENQUIRY_NO#", enquiryLog.getEnquiryNo());
        mapBody.put("#CUST_NAME#", enquiryLog.getPartyMaster().getPartyName());
        mapBody.put("#QUOTE_BY#", TimeUtil.getLocationDate(enquiryLog.getLocationMaster(), enquiryLog.getQuoteBy()));
        mapBody.put("#SERVICES_TABLES#", airServiceTemplate(groupService));
        mapBody.put("#EMP_NAME#", enquiryLog.getLoggedBy().getEmployeeName());
        mapBody.put("#EMP_EMAIL#", enquiryLog.getLoggedBy().getEmail());
        mapBody.put("#EMP_TELNO#", enquiryLog.getLoggedBy().getEmployeePhoneNo());

        // sending email
        EmailRequest emailRequest = new EmailRequest();
        emailRequest.setFromEmailId(emailTemplate.getFromEmailId());
        emailRequest.setFromEmailIdPassword(emailTemplate.getFromEmailPassword());
        emailRequest.setToEmailIdList(enquiryLog.getSalesCoOrdinator().getEmail());
        emailRequest.setCcEmailIdList(enquiryLog.getSalesman().getEmail());
        emailRequest.setEmailType(emailTemplate.getEmailType());
        emailRequest.setPort(emailTemplate.getPort());
        emailRequest.setSmtpServerAddress(emailTemplate.getSmtpServerAddress());

        emailRequest.setSubject(emailTemplate.getSubject());
        emailRequest.setEmailBody(emailTemplate.getTemplate());

        for (String key : mapSubject.keySet()) {
            emailRequest.setSubject(emailRequest.getSubject().replaceAll(key, mapSubject.get(key) == null ? "" : mapSubject.get(key).toString()));
        }

        for (String key : mapBody.keySet()) {
            emailRequest.setEmailBody(emailRequest.getEmailBody().replaceAll(key, mapBody.get(key) == null ? "" : mapBody.get(key).toString()));
        }

        emailRequestService.saveAndSendEmail(emailRequest, userProfile, mailerUtil.getLogo(enquiryLog.getSalesCoOrdinator().getLocationMaster()));


        log.info("sendEnquiryAutoMail  end");
    }

    private String airServiceTemplate(Map<Long, List<EnquiryDetail>> groupService) {

        String allServicesTables = "";
        String cssClass = "border: 1px solid #ba68c8;padding: 4px;border-collapse:collapse;";
        String cssClassDouble = "border: 1px solid #ba68c8;padding: 4px;border-collapse:collapse;text-align: right;";


        for (Long serviceId : groupService.keySet()) {
            List<EnquiryDetail> arrayList = groupService.get(serviceId);
            String tableHeading = getTableHdrHtmlString(groupService.get(serviceId).get(0).getServiceMaster());

            int srNo = 1;
            String sNoTd = "";
            String tosTd = "";
            String commo_groupTd = "";
            String originTd = "";
            String destinationTd = "";
            String grossWtTd = "";
            String volumeWtTd = "";
            String remarksTd = "";

            String row = "";

            for (EnquiryDetail ed : arrayList) {

                sNoTd = "<td style=" + "'" + cssClass + "'" + ">" + srNo + "</td>";

                if (ed.getTosMaster() != null) {
                    tosTd = "<td style=" + "'" + cssClass + "'" + ">" + ed.getTosMaster().getTosName() + "</td>";
                } else {
                    tosTd = "<td style=" + "'" + cssClass + "'" + ">-</td>";
                }

                if (ed.getCommodityMaster() != null) {
                    commo_groupTd = "<td style=" + "'" + cssClass + "'" + ">" + ed.getCommodityMaster().getHsName() + "</td>";
                } else {
                    commo_groupTd = "<td style=" + "'" + cssClass + "'" + ">-</td>";
                }
                if (ed.getFromPortMaster() != null) {
                    originTd = "<td style=" + "'" + cssClass + "'" + ">" + ed.getFromPortMaster().getPortName() + "</td>";
                } else {
                    originTd = "<td style=" + "'" + cssClass + "'" + ">-</td>";
                }

                if (ed.getGrossWeight() == 0) {
                    grossWtTd = "<td style=" + "'" + cssClassDouble + "'" + ">0.00</td>";
                } else {
                    grossWtTd = "<td style=" + "'" + cssClassDouble + "'" + ">" + reportUtil.twoDecimalDoubleValueFormat(ed.getGrossWeight()) + "</td>";
                }

                if (ed.getVolWeight() == 0) {
                    volumeWtTd = "<td style=" + "'" + cssClassDouble + "'" + ">0.00</td>";
                } else {
                    volumeWtTd = "<td style=" + "'" + cssClassDouble + "'" + ">" + reportUtil.twoDecimalDoubleValueFormat(ed.getVolWeight()) + "</td>";
                }

                if (ed.getToPortMaster() != null) {
                    destinationTd = "<td style=" + "'" + cssClass + "'" + ">" + ed.getToPortMaster().getPortName() + "</td>";
                } else {
                    destinationTd = "<td style=" + "'" + cssClass + "'" + ">-</td>";
                }

                if (ed.getNotes() != null) {
                    remarksTd = "<td style=" + "'" + cssClass + "'" + ">" + ed.getNotes() + "</td>";
                } else {
                    remarksTd = "<td style=" + "'" + cssClass + "'" + ">-</td>";
                }

                String tr = "<tr style=" + "'border-collapse:collapse;" + "'>\n" + sNoTd + "\n" + originTd + "\n" + destinationTd + "\n" + tosTd + "\n" + commo_groupTd + "\n" + grossWtTd + "\n" + volumeWtTd + "\n" + remarksTd + "\n" + "</tr>";

                row += tr + "\n";
                srNo++;
            }

            tableHeading += row + "</table>\n";
            allServicesTables += tableHeading;

        }

        return allServicesTables;
    }

    private Map<Long, List<EnquiryDetail>> edListGroupByServiceName(List<EnquiryDetail> detailList) {
        Map<Long, List<EnquiryDetail>> groupService = new HashMap<>();

        for (EnquiryDetail ed : detailList) {

            if (ed.getServiceMaster().getTransportMode() != TransportMode.Air
                    || !ed.getServiceMaster().getTransportMode().equals(TransportMode.Air) || ed.getVersionLock() != 0) {
                continue;
            }

            if (groupService.get(ed.getServiceMaster().getId()) == null) {
                List<EnquiryDetail> tmpList = new ArrayList<>();
                tmpList.add(ed);
                groupService.put(ed.getServiceMaster().getId(), tmpList);
            } else {
                List<EnquiryDetail> tmpList = groupService.get(ed.getServiceMaster().getId());
                tmpList.add(ed);
                groupService.put(ed.getServiceMaster().getId(), tmpList);
            }
        }

        return groupService;
    }

    private String getTableHdrHtmlString(ServiceMaster serviceMaster) {

        String tableCss = "font-weight:normal;padding:40px;border: 1px #ba68c8;margin-left:20px;width: 750px;margin-top: 20px;text-align: left;font-size: 11px;font-family:Helvetica,arial,sans-serif; border-collapse:collapse;";

        String thHdrCss = "font-weight:normal;border: 1px solid #ba68c8;width: 24px;padding: 4px;border-collapse:collapse;";
        String thHdrCssOrigin = "font-weight:normal;border: 1px solid #ba68c8;width: 120px;padding: 4px;border-collapse:collapse;";
        String thHdrCssDestination = "font-weight:normal;border: 1px solid #ba68c8;width: 120px;padding: 4px;border-collapse:collapse;";
        String thHdrCssTos = "font-weight:normal;border: 1px solid #ba68c8;width: 40px;padding: 4px;border-collapse:collapse;";
        String thHdrCssCommodity = "font-weight:normal;border: 1px solid #ba68c8;width: 140px;padding: 4px;border-collapse:collapse;";
        String thHdrCssGrossWt = "font-weight:normal;border: 1px solid #ba68c8;width: 90px;padding: 4px;border-collapse:collapse;text-align: right;";
        String thHdrCssVolumeWt = "font-weight:normal;border: 1px solid #ba68c8;width: 90px;padding: 4px;border-collapse:collapse;text-align: right;";
        String thHdrCssNoOfContainers = "font-weight:normal;border: 1px solid #ba68c8;width: 180px;padding: 4px;border-collapse:collapse;";


        String thHdrRemarksCss = "font-weight:normal;border: 1px solid #ba68c8;padding: 4px;border-collapse:collapse;width: 88px;";

        String trHdrCss = "background: #d68ee2;color: white;border-collapse:collapse;";

        String serviceTr = "background-color: #d68ee2;color: white;font-weight: 400; ";
        String serviceTh = "padding: -10px;border: 1px solid #ba68c8;border-collapse:collapse;";
        String serviceCol = "";


        if (serviceMaster.getTransportMode().equals(TransportMode.Ocean)) {
            serviceCol = "colspan='7'";
        } else {
            serviceCol = "colspan='8'";
        }


        String serviceP = "font-weight: 500;margin-left: 4px;font-size: 12px;font-family:'Helvetica Neue',Helvetica,arial,sans-serif;";

        String serviceSpan = "font-weight: 600;font-size: 12px;font-family:'Helvetica Neue',Helvetica,arial,sans-serif;";


        String table = "<table style=" + "'" + tableCss + "'" + ">\n";


        table += "<tr style=" + "'" + serviceTr + "'" + ">\n";
        table += "<th " + serviceCol + " style=" + "'" + serviceTh + "'" + ">\n";
        table += "<p style=" + "'" + serviceP + "'" + ">Service :\n";
        table += "<span style=" + "'" + serviceSpan + "'> " + serviceMaster.getServiceName() + " </span></p></th></tr>\n";

        table += "<tr style=" + "'" + trHdrCss + "'" + ">\n";
        table += "<th style=" + "'" + thHdrCss + "'" + ">No</th>\n";
        table += "<th style=" + "'" + thHdrCssOrigin + "'" + ">Origin</th>\n";
        table += "<th style=" + "'" + thHdrCssDestination + "'" + ">Destination</th>\n";
        table += "<th style=" + "'" + thHdrCssTos + "'" + ">TOS</th>\n";
        table += "<th style=" + "'" + thHdrCssCommodity + "'" + ">Commodity Group</th>\n";

        // No. of Containers & Container Type
        if (serviceMaster.getTransportMode().equals(TransportMode.Ocean)) {

            table += "<th style=" + "'" + thHdrCssNoOfContainers + "'" + ">No. of Containers & Container Type</th>\n";

        } else {

            table += "<th style=" + "'" + thHdrCssGrossWt + "'" + ">Gross Wt.(KG)</th>\n";
            table += "<th style=" + "'" + thHdrCssVolumeWt + "'" + ">Volume Wt.(KG)</th>\n";

        }

        table += "<th style=" + "'" + thHdrRemarksCss + "'" + ">Remarks</th>\n<tr/>";
        return table;
    }

    private String getCompanyName(CompanyMaster companyMaster) {
        if (companyMaster == null || companyMaster.getCompanyName() == null) {
            return "";
        }
        return companyMaster.getCompanyName().trim();
    }


    @Async
    public void sendEnquiryMailToRateRequest(EnquiryLog enquiryLog,
                                             UserProfile userProfile) {

        EmailTemplate emailTemplate = emailTemplateRepository.findByTemplateType(TemplateType.ENQUIRY_RATE_REQUEST);

        if (emailTemplate == null) {
            log.error("Enquiry confirmation to sales man mail is not available");
            return;
        }

        if (enquiryLog != null && enquiryLog.getEnquiryDetailList() != null && enquiryLog.getEnquiryDetailList().size() > 0) {

            for (int i = 0; i < enquiryLog.getEnquiryDetailList().size(); i++) {

                if (enquiryLog.getEnquiryDetailList().get(i).getRateRequestList() != null && enquiryLog.getEnquiryDetailList().get(i).getRateRequestList().size() > 0) {

                    for (int j = 0; j < enquiryLog.getEnquiryDetailList().get(i).getRateRequestList().size(); j++) {

                        Map<String, Object> mapSubject = new HashMap<>();
                        mapSubject.put("#ENQUIRYNO#", enquiryLog.getEnquiryNo());
                        Map<String, Object> mapBody = new HashMap<>();
                        log.info("Date" + enquiryLog.getLoggedOn());
                        mapBody.put("#DATE#", TimeUtil.getLocationDate(enquiryLog.getLocationMaster(), enquiryLog.getLoggedOn()));
                        mapBody.put("#VENDORNAME#", enquiryLog.getEnquiryDetailList().get(i).getRateRequestList().get(j).getVendor().getPartyName());
                        mapBody.put("#ENQUIRYNO#", enquiryLog.getEnquiryNo());
                        mapBody.put("#ORIGIN#", enquiryLog.getEnquiryDetailList().get(i).getFromPortMaster().getPortName());
                        mapBody.put("#DESTINATION#", enquiryLog.getEnquiryDetailList().get(i).getToPortMaster().getPortName());
                        mapBody.put("#COMMODITY#", enquiryLog.getEnquiryDetailList().get(i).getCommodityMaster() != null ? enquiryLog.getEnquiryDetailList().get(i).getCommodityMaster().getHsName() : null);
                        mapBody.put("#TOS#", enquiryLog.getEnquiryDetailList().get(i).getTosMaster() != null ? enquiryLog.getEnquiryDetailList().get(i).getTosMaster().getTosCode() : null);
                        mapBody.put("#REMARKS#", enquiryLog.getEnquiryDetailList().get(i).getNotes());
                        mapBody.put("#PICKUPPLACE#", (enquiryLog.getEnquiryDetailList().get(i).getPickupAddress() != null ?
                                enquiryLog.getEnquiryDetailList().get(i).getPickupAddress().getAddressLine1() + "," + enquiryLog.getEnquiryDetailList().get(i).getPickupAddress().getAddressLine2() + "," + enquiryLog.getEnquiryDetailList().get(i).getPickupAddress().getAddressLine3() + "," + enquiryLog.getEnquiryDetailList().get(i).getPickupAddress().getCity().getCityName() + "," + enquiryLog.getEnquiryDetailList().get(i).getPickupAddress().getState() + "," + enquiryLog.getEnquiryDetailList().get(i).getPickupAddress().getPoBox() : null));
                        mapBody.put("#DELIVERYPLACE#", (enquiryLog.getEnquiryDetailList().get(i).getDeliveryAddress() != null ?
                                enquiryLog.getEnquiryDetailList().get(i).getDeliveryAddress().getAddressLine1() + "," + enquiryLog.getEnquiryDetailList().get(i).getDeliveryAddress().getAddressLine2() + "," + enquiryLog.getEnquiryDetailList().get(i).getDeliveryAddress().getAddressLine3() + "," + enquiryLog.getEnquiryDetailList().get(i).getDeliveryAddress().getCity() + "," + enquiryLog.getEnquiryDetailList().get(i).getDeliveryAddress().getState() + "," + enquiryLog.getEnquiryDetailList().get(i).getDeliveryAddress().getPoBox() : null));
                        mapBody.put("#PHONENO#", enquiryLog.getSalesCoOrdinator().getEmployeePhoneNo());
                        mapBody.put("#SENDEREMAIL#", enquiryLog.getSalesCoOrdinator().getEmail());
                        mapBody.put("#SALESCORDINATOR#", enquiryLog.getSalesCoOrdinator().getEmployeeName());
                        mapBody.put("#LOGINCOMPANY#", enquiryLog.getSalesCoOrdinator().getCompanyMaster().getCompanyName());
                        mapBody.put("#URLLINK#", applicationBaseUrl + "/public/enquiryRateRequest/enquiry_rate_request.html?saasId=" + SaaSUtil.getSaaSId().toUpperCase() + "&rateRequestId=" + enquiryLog.getEnquiryDetailList().get(i).getRateRequestList().get(j).getId());

                        // sending email
                        EmailRequest emailRequest = new EmailRequest();
                        emailRequest.setFromEmailId(emailTemplate.getFromEmailId());
                        emailRequest.setFromEmailIdPassword(emailTemplate.getFromEmailPassword());
                        emailRequest.setToEmailIdList(enquiryLog.getEnquiryDetailList().get(i).getRateRequestList().get(j).getEmailId());
                        emailRequest.setCcEmailIdList(emailTemplate.getCcEmailAddress());
                        emailRequest.setBccEmailIdList(emailTemplate.getBccEmailAddress());
                        emailRequest.setEmailType(emailTemplate.getEmailType());
                        emailRequest.setPort(emailTemplate.getPort());
                        emailRequest.setSmtpServerAddress(emailTemplate.getSmtpServerAddress());
                        emailRequest.setSubject(emailTemplate.getSubject());
                        emailRequest.setEmailBody(emailTemplate.getTemplate());

                        for (String key : mapSubject.keySet()) {
                            emailRequest.setSubject(emailRequest.getSubject().replaceAll(key, mapSubject.get(key) == null ? "" : mapSubject.get(key).toString()));
                        }

                        for (String key : mapBody.keySet()) {
                            emailRequest.setEmailBody(emailRequest.getEmailBody().replaceAll(key, mapBody.get(key) == null ? "" : mapBody.get(key).toString()));
                        }
                        emailRequestService.saveAndSendEmail(emailRequest, userProfile, mailerUtil.getLogo(enquiryLog.getSalesCoOrdinator().getLocationMaster()));
                    }

                }
            }
        }
    }

}//last
