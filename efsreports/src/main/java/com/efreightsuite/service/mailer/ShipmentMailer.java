package com.efreightsuite.service.mailer;

import java.text.DecimalFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.efreightsuite.common.component.CacheRepository;
import com.efreightsuite.common.component.EmailRequestService;
import com.efreightsuite.enumeration.AddressType;
import com.efreightsuite.enumeration.DimensionUnit;
import com.efreightsuite.enumeration.TemplateType;
import com.efreightsuite.enumeration.WhoRouted;
import com.efreightsuite.enumeration.YesNo;
import com.efreightsuite.model.*;
import com.efreightsuite.repository.ConsolRepository;
import com.efreightsuite.repository.EmailTemplateRepository;
import com.efreightsuite.repository.LogoMasterRepository;
import com.efreightsuite.repository.ShipmentRepository;
import com.efreightsuite.repository.UserProfileRepository;
import com.efreightsuite.util.AppUtil;
import com.efreightsuite.util.ReportUtil;
import com.efreightsuite.util.SaaSUtil;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class ShipmentMailer {

    @Autowired
    ReportUtil reportUtil;

    @Autowired
    AppUtil appUtil;

    @Autowired
    MailerUtil mailerUtil;

    @Autowired
    ShipmentRepository shipmentRepository;

    @Autowired
    ConsolRepository consolRepository;

    @Autowired
    EmailTemplateRepository emailTemplateRepository;

    @Autowired
    CacheRepository cacheRepository;

    @Autowired
    LogoMasterRepository logoMasterRepository;

    @Autowired
    EmailRequestService emailRequestService;

    @Autowired
    UserProfileRepository userProfileRepository;

    @Async
    public void shipmentMailTrigger(Shipment shipment, UserProfile userProfile, Map<String, String> newServiceMap) {
        log.info("shipmentMailTrigger started...");

        for (ShipmentServiceDetail service : shipment.getShipmentServiceList()) {

            if (newServiceMap.get(service.getServiceUid()) != null) {
                serviceBookingMail(service, userProfile);
            }
            if (service.getShipmentServiceTriggerList() != null) {
                for (ShipmentServiceTrigger trigger : service.getShipmentServiceTriggerList()) {
                    if (trigger.getVersionLock() == 0) {
                        serviceStatusMail(service, trigger, userProfile);
                    }
                }
            } else {
                log.info("ShipmentServiceTriggerList is empty");
            }

            if (service.getEventList() != null) {
                for (ShipmentServiceEvent event : service.getEventList()) {
                    if (event.getVersionLock() == 0) {
                        serviceEventMail(service, event, userProfile);
                    }
                }
            } else {
                log.info("EventList is empty");
            }

        }
    }

    private void serviceBookingMail(ShipmentServiceDetail service, UserProfile userProfile) {
        log.info("serviceBookingMail started...");

        switch (service.getServiceMaster().getTransportMode()) {
            case Air:
                switch (service.getServiceMaster().getImportExport()) {
                    case Export:
                        bookingConfimationMail(service, userProfile);
                        break;
                    case Import:
                        bookingConfimationMail(service, userProfile);
                        break;
                    case NA:
                        log.info("Selected service importExport is NA");
                        break;
                    default:
                        log.info("Selected service importExport is null");
                        break;
                }
                break;
            case Ocean:
                switch (service.getServiceMaster().getImportExport()) {
                    case Export:
                        log.info("Selected service importExport is Export");
                        break;
                    case Import:
                        log.info("Selected service importExport is Import");
                        break;
                    case NA:
                        log.info("Selected service importExport is NA");
                        break;
                    default:
                        log.info("Selected service importExport is null");
                        break;
                }
                log.info("Selected service is Ocean booking confirmation not enable");
                break;
            case Rail:
                switch (service.getServiceMaster().getImportExport()) {
                    case Export:
                        log.info("Selected service importExport is Export");
                        break;
                    case Import:
                        log.info("Selected service importExport is Import");
                        break;
                    case NA:
                        log.info("Selected service importExport is NA");
                        break;
                    default:
                        log.info("Selected service importExport is null");
                        break;
                }
                log.info("Selected service is Rail booking confirmation not enable");
                break;
            case Road:
                switch (service.getServiceMaster().getImportExport()) {
                    case Export:
                        log.info("Selected service importExport is Export");
                        break;
                    case Import:
                        log.info("Selected service importExport is Import");
                        break;
                    case NA:
                        log.info("Selected service importExport is NA");
                        break;
                    default:
                        log.info("Selected service importExport is null");
                        break;
                }
                log.info("Selected service is Road booking confirmation not enable");
                break;
            default:
                log.info("Selected service transport mode is null");
        }


    }

    private void serviceStatusMail(ShipmentServiceDetail service, ShipmentServiceTrigger trigger, UserProfile userProfile) {
        log.info("serviceStatusMail started...");

        String toEmailList = null;

        if (service.getWhoRouted() != null && service.getWhoRouted().equals(WhoRouted.Agent)) {
            toEmailList = getPartyEmailList(service.getAgent());
        } else {
            toEmailList = getPartyEmailList(service.getParty());
        }

        if (toEmailList == null) {
            log.warn("Selected party or Agent primary email is not found");
            return;
        }

        Map<String, Object> mapSubject = new HashMap<>();
        Map<String, Object> mapBody = new HashMap<>();

        mapBody.put("#NOTE#", trigger.getNote());
        mapBody.put("#CARRIER#", service.getCarrier().getCarrierName());
        mapBody.put("#FLIGHTNO#", service.getRouteNo());
        mapBody.put("#ETD#", reportUtil.dateToReportDate(service.getEtd(), getLocationDateFormat(service.getLocation())));
        mapBody.put("#ETA#", reportUtil.dateToReportDate(service.getEta(), getLocationDateFormat(service.getLocation())));

        mapBody.putAll(getBasicInfo(service));

        mapSubject.put("#BOOKING_NO#", trigger.getShipmentServiceDetail().getShipment().getShipmentUid());

        sendMail(SaaSUtil.getSaaSId(), TemplateType.SHIPMENT_STATUS, toEmailList, mapSubject, mapBody, userProfile, service.getLocation());

    }

    private void serviceEventMail(ShipmentServiceDetail service, ShipmentServiceEvent event, UserProfile userProfile) {

        log.info("serviceEventMail started...");

        String toEmailList = null;

        if (event.getShipmentServiceDetail().getWhoRouted() != null && event.getShipmentServiceDetail().getWhoRouted().equals(WhoRouted.Agent)) {
            toEmailList = getPartyEmailList(event.getShipmentServiceDetail().getAgent());
        } else {
            toEmailList = getPartyEmailList(event.getShipmentServiceDetail().getParty());
        }

        if (toEmailList == null) {
            log.warn("Selected party or Agent primary email is not found");
            return;
        }

        Map<String, Object> mapSubject = new HashMap<>();
        Map<String, Object> mapBody = new HashMap<>();

        mapBody.put("#EVENT_NOTE#", event.getEventMaster().getEventName());
        mapBody.put("#EVENT_DATE#", reportUtil.dateToReportDate(event.getEventDate(), getLocationDateFormat(event.getShipmentServiceDetail().getLocation())));
        mapBody.putAll(getBasicInfo(event.getShipmentServiceDetail()));

        mapSubject.put("#BOOKING_NO#", event.getShipmentServiceDetail().getShipment().getShipmentUid());

        sendMail(SaaSUtil.getSaaSId(), TemplateType.SHIPMENT_EVENT, toEmailList, mapSubject, mapBody, userProfile, service.getLocation());

    }

    @Async
    public void cargoLoadingConfirmationMail(Consol consol, UserProfile userProfile) {
        log.info("cargoLoadingConfirmationMail started...");

        if (consol.getServiceMailTrigger() != null && (consol.getServiceMailTrigger().getIsCargoLoadedMailTrigger() == null || consol.getServiceMailTrigger().getIsCargoLoadedMailTrigger() == YesNo.No)) {
            if (consol.getShipmentLinkList() != null && consol.getShipmentLinkList().size() != 0) {
                for (ShipmentLink sd : consol.getShipmentLinkList()) {
                    sendCargoLoadedConfimration(consol, sd.getService(), userProfile);
                }
                consolRepository.updateServiceMailTrigger(consol.getServiceMailTrigger().getId());
            }
        }
    }

    private void sendCargoLoadedConfimration(Consol consol, ShipmentServiceDetail service, UserProfile userProfile) {
        String toEmailList = null;

        if (service.getWhoRouted() != null && service.getWhoRouted().equals(WhoRouted.Agent)) {
            toEmailList = getPartyEmailList(service.getAgent());
        } else {
            toEmailList = getPartyEmailList(service.getParty());
        }

        if (toEmailList == null) {
            log.warn("Selected party or Agent primary email is not found");
            return;
        }

        Map<String, Object> mapSubject = new HashMap<>();
        Map<String, Object> mapBody = new HashMap<>();

        mapBody.put("#CARRIER#", consol.getCarrier().getCarrierName());
        mapBody.put("#FLIGHTNO#", consol.getConsolDocument().getRouteNo());
        mapBody.put("#ATD#", reportUtil.dateToReportDate(consol.getAtd(), getLocationDateFormat(consol.getLocation())));
        mapBody.put("#ATA#", reportUtil.dateToReportDate(consol.getAta(), getLocationDateFormat(consol.getLocation())));

        mapBody.putAll(getBasicInfo(consol, service));

        mapSubject.put("#BOOKING_NO#", service.getShipmentUid());

        sendMail(SaaSUtil.getSaaSId(), TemplateType.SHIPMENT_CARGO_LOADED, toEmailList, mapSubject, mapBody, userProfile, service.getLocation());

    }

    private void bookingConfimationMail(ShipmentServiceDetail service, UserProfile userProfile) {
        log.info("bookingConfimationMail started...");


        String toEmailList = null;

        if (service.getWhoRouted() != null
                && service.getWhoRouted().equals(WhoRouted.Agent)) {
            toEmailList = getPartyEmailList(service.getAgent());
        } else {
            toEmailList = getPartyEmailList(service.getParty());
        }

        if (toEmailList == null) {
            log.warn("Selected party or Agent primary email is not found");
            return;
        }

        Map<String, Object> mapSubject = new HashMap<>();
        Map<String, Object> mapObj = new HashMap<>();

//		//NEW SHIPMENT COMMENTS
//		mapObj.put("#TERMS_OF_SHIPMENT#", getTOSName(service.getShipment().getTosMaster()));
//		mapObj.put("#COMMODITY#", getCommodityName(service.getShipment().getCommodity()));
        mapObj.put("#FREIGHT_TERMS#", service.getPpcc());
        mapObj.put("#PICK_UP_PLACE#", null);
        mapObj.put("#PICK_UP_DATE#", null);

        if (service.getPickUpDeliveryPoint() != null) {
            if (service.getPickUpDeliveryPoint().getPickUpPlace() != null) {
                mapObj.put("#PICK_UP_PLACE#", service.getPickUpDeliveryPoint().getPickUpPlace());
            }

            if (service.getPickUpDeliveryPoint().getPickUpActual() != null) {
                mapObj.put("#PICK_UP_DATE#", reportUtil.dateToReportDate(service.getPickUpDeliveryPoint().getPickUpActual(), getLocationDateFormat(service.getLocation())));
            }
        }

        mapObj.put("#IMPORT_EXPORT#", service.getServiceMaster().getImportExport().toString());
        mapObj.put("#TRANSPORT_MODE#", service.getServiceMaster().getTransportMode().toString());

        mapObj.put("#BOOKING_PERSION#", service.getCustomerService().getEmployeeName());
        mapObj.put("#BOOKING_PERSION_EMAIL#", service.getCustomerService().getEmail());


        if (service.getDocumentList() != null && !service.getDocumentList().isEmpty()) {
            for (DocumentDetail documentDetail : service.getDocumentList()) {

                mapSubject.put("#SHIPPER_NAME#", getPartyName(documentDetail.getShipper()));
                mapSubject.put("#CONSIGNEE_NAME#", getPartyName(documentDetail.getConsignee()));
                mapSubject.put("#BOOKING_NO#", service.getShipment().getShipmentUid());

                mapObj.put("#BOOKING_NO#", service.getShipment().getShipmentUid());
                mapObj.put("#HAWB_NO#", documentDetail.getHawbNo() != null ? documentDetail.getHawbNo() : "");
                mapObj.put("#MAWB_NO#", service.getMawbNo() != null ? service.getMawbNo() : "");
                mapObj.put("#EBOOKING_NO#", documentDetail.getDocumentNo());
                mapObj.put("#CUSTOMER_REF_NO#", documentDetail.getDocumentNo());
                mapObj.put("#AIRPORT_OF_LOADING#", getPortName(documentDetail.getPol()));
                mapObj.put("#AIRPORT_OF_DISCHARGE#", getPortName(documentDetail.getPod()));
                mapObj.put("#SHIPPER_NAME#", getPartyName(documentDetail.getShipper()));
                mapObj.put("#CONSIGNEE_NAME#", getPartyName(documentDetail.getConsignee()));
                mapObj.put("#NOTIFY_PARTY_NAME#", getPartyName(documentDetail.getFirstNotify()));
                mapObj.put("#ORIGIN#", getPortName(documentDetail.getOrigin()));
                mapObj.put("#DESTINATION#", getPortName(documentDetail.getDestination()));
                mapObj.put("#ETA#", reportUtil.dateToReportDate(documentDetail.getEta(), getLocationDateFormat(documentDetail.getLocation())));
                mapObj.put("#ETD#", reportUtil.dateToReportDate(documentDetail.getEtd(), getLocationDateFormat(documentDetail.getLocation())));
                mapObj.put("#AIRLINE#", getCarrierName(documentDetail.getCarrier()));
                mapObj.put("#FLIGHT_NO#", documentDetail.getRouteNo() != null ? documentDetail.getRouteNo() : "");
                mapObj.put("#FLIGHT_STATUS#", "Planned");
                mapObj.put("#DIMENSION_UNIT#", service.getDimensionUnit() == DimensionUnit.CENTIMETERORKILOS ? "cm" : "in");
                mapObj.put("#EXTERNAL_PACK#", documentDetail.getNoOfPieces() + " " + getPackName(documentDetail.getPackMaster()));
                mapObj.put("#INTERNAL_PACK#", getPackName(documentDetail.getPackMaster()));
                mapObj.put("#GROSS_WEIGHT#", getWeight(documentDetail.getGrossWeight()));
                mapObj.put("#CHARGABLE_WEIGHT#", getWeight(documentDetail.getChargebleWeight()));
                mapObj.put("#EMPLOYEE_NAME#", getEmployeeName(userProfile));
                mapObj.put("#BOOKING_DATE#", reportUtil.dateToReportDate(documentDetail.getDocumentReqDate(), getLocationDateFormat(documentDetail.getLocation())));
                mapObj.put("#DATE#", reportUtil.dateToReportDate(new Date(), getLocationDateFormat(documentDetail.getLocation())));
                mapObj.put("#COMPANY_NAME#", getCompanyName(service.getCompany()));
                mapObj.put("#DIMENSION_TEMPLATE#", prepereDimensionListTemplete(documentDetail));

                mapObj.putAll(getPartyAddressList(documentDetail.getShipperAddress(), "SHIPPER"));
                mapObj.putAll(getPartyAddressList(documentDetail.getConsigneeAddress(), "CONSIGNEE"));
                mapObj.putAll(getPartyAddressList(documentDetail.getFirstNotifyAddress(), "NOTIFY_PARTY"));

                sendMail(SaaSUtil.getSaaSId(), TemplateType.SHIPMENT_BOOKING_CONFIRMATION, toEmailList, mapSubject, mapObj, userProfile, service.getLocation());

            }
        }


    }


    private Map<String, Object> getBasicInfo(ShipmentServiceDetail service) {
        Map<String, Object> map = new HashMap<>();

        StringBuilder documentNos = new StringBuilder();
        for (int i = 0; i < service.getDocumentList().size(); i++) {
            if (documentNos.length() == 0)
                documentNos.append(service.getDocumentList().get(i).getDocumentNo());
            else
                documentNos.append(", " + service.getDocumentList().get(i).getDocumentNo());
        }

        map.put("#EBOOKING_NO#", documentNos);
        map.put("#CUSTOMER_REF_NO#", documentNos);
        map.put("#BOOKING_DATE#", reportUtil.dateToReportDate(service.getServiceReqDate(), getLocationDateFormat(service.getLocation())));
        map.put("#BOOKING_NO#", service.getShipment().getShipmentUid());
        map.put("#HAWB_NO#", service.getHawbNo());
        map.put("#MAWB_NO#", service.getMawbNo());
        map.put("#BOOKING_PERSION#", service.getCustomerService().getEmployeeName());
        map.put("#BOOKING_PERSION_EMAIL#", service.getCustomerService().getEmail());
        map.put("#ORIGIN#", getPortName(service.getOrigin()));
        map.put("#DESTINATION#", getPortName(service.getDestination()));
        map.put("#COMPANY_NAME#", getCompanyName(service.getCompany()));
        map.put("#IMPORT_EXPORT#", service.getServiceMaster().getImportExport().toString());
        map.put("#TRANSPORT_MODE#", service.getServiceMaster().getTransportMode().toString());
        return map;
    }

    private Map<String, Object> getBasicInfo(Consol consol, ShipmentServiceDetail service) {
        Map<String, Object> map = new HashMap<>();

        StringBuilder documentNos = new StringBuilder();
        for (int i = 0; i < service.getDocumentList().size(); i++) {
            if (documentNos.length() == 0)
                documentNos.append(service.getDocumentList().get(i).getDocumentNo());
            else
                documentNos.append(", " + service.getDocumentList().get(i).getDocumentNo());
        }

        map.put("#EBOOKING_NO#", documentNos);
        map.put("#CUSTOMER_REF_NO#", documentNos);
        map.put("#BOOKING_DATE#", reportUtil.dateToReportDate(service.getServiceReqDate(), getLocationDateFormat(service.getLocation())));
        map.put("#BOOKING_NO#", service.getShipmentUid());
        map.put("#HAWB_NO#", service.getHawbNo());
        map.put("#MAWB_NO#", consol.getConsolDocument().getMawbNo());
        map.put("#BOOKING_PERSION#", service.getCustomerService().getEmployeeName());
        map.put("#BOOKING_PERSION_EMAIL#", service.getCustomerService().getEmail());
        map.put("#ORIGIN#", getPortName(consol.getOrigin()));
        map.put("#DESTINATION#", getPortName(consol.getDestination()));
        map.put("#COMPANY_NAME#", getCompanyName(consol.getCompany()));
        map.put("#IMPORT_EXPORT#", consol.getServiceMaster().getImportExport().toString());
        map.put("#TRANSPORT_MODE#", consol.getServiceMaster().getTransportMode().toString());

        return map;
    }

    @Async
    public void sendMail(String saasId, TemplateType templateType, String toEmail, Map<String, Object> subject, Map<String, Object> body, UserProfile userProfile, LocationMaster location) {

        try {

            EmailTemplate emailTemplate = cacheRepository.get(saasId, EmailTemplate.class.getName(), templateType, emailTemplateRepository);

            EmailRequest emailRequest = new EmailRequest();
            emailRequest.setEmailType(emailTemplate.getEmailType());
            emailRequest.setFromEmailId(emailTemplate.getFromEmailId());
            emailRequest.setFromEmailIdPassword(emailTemplate.getFromEmailPassword());
            emailRequest.setToEmailIdList(toEmail);
            emailRequest.setSubject(emailTemplate.getSubject());
            emailRequest.setEmailBody(emailTemplate.getTemplate());

            emailRequest.setPort(emailTemplate.getPort());
            emailRequest.setSmtpServerAddress(emailTemplate.getSmtpServerAddress());


            for (String key : subject.keySet()) {
                emailRequest.setSubject(emailRequest.getSubject().replaceAll(key, subject.get(key) == null ? "" : subject.get(key).toString()));
            }

            for (String key : body.keySet()) {
                emailRequest.setEmailBody(emailRequest.getEmailBody().replaceAll(key, body.get(key) == null ? "" : body.get(key).toString()));
            }

            emailRequestService.saveAndSendEmail(emailRequest, userProfile, mailerUtil.getLogo(location));

        } catch (Exception e) {
            log.warn("Mail is not trigger :", e);
        }
    }

    private String getLocationDateFormat(LocationMaster kocationMaster) {
        if (kocationMaster == null || kocationMaster.getJavaDateFormat() == null) {
            return "";
        }
        return kocationMaster.getJavaDateFormat();
    }

    private String getCompanyName(CompanyMaster companyMaster) {
        if (companyMaster == null || companyMaster.getCompanyName() == null) {
            return "";
        }
        return companyMaster.getCompanyName().trim();
    }

    private String getEmployeeName(UserProfile user) {
        if (user == null || user.getEmployee() == null || user.getEmployee().getEmployeeName() == null) {
            return "";
        }
        return user.getEmployee().getEmployeeName().trim();
    }


    private String withoutDecimalFormat(Double value) {
        if (value == null) {
            return "";
        }
        return new DecimalFormat("#").format(value);
    }

    private String getCommodityName(CommodityMaster data) {
        if (data == null || data.getId() == null) {
            return "";
        }
        log.info("data.getHsName() : " + data.getHsName());
        return data.getHsName();
    }

    private String getCarrierName(CarrierMaster data) {
        if (data == null || data.getId() == null) {
            return "";
        }
        log.info("data.getCarrierName() : " + data.getCarrierName());
        return data.getCarrierName();
    }

    private String getPackName(PackMaster data) {
        if (data == null || data.getId() == null) {
            return "";
        }
        log.info("data.getPackName() : " + data.getPackName());
        return data.getPackName();
    }

    private String getPartyName(PartyMaster party) {
        if (party == null || party.getId() == null) {
            return "";
        }
        log.info("party.getPartyName() : " + party.getPartyName());
        return party.getPartyName();
    }

    private String getTOSName(TosMaster tos) {
        if (tos == null || tos.getId() == null) {
            return "";
        }
        log.info("tos.getTosName() : " + tos.getTosName());
        return tos.getTosName();
    }

    private String getPortName(PortMaster port) {
        if (port == null || port.getId() == null) {
            return "";
        }
        log.info("port.getPortName() : " + port.getPortName());
        return port.getPortName();
    }

    private Map<String, Object> getPartyAddressList(AddressMaster address, String whichParty) {

        Map<String, Object> tmpMap = new HashMap<>();

        if (whichParty.equals("SHIPPER") || whichParty == "SHIPPER") {
            tmpMap.put("#SHIPPER_ADDRESS_1#", null);
            tmpMap.put("#SHIPPER_ADDRESS_2#", null);
            tmpMap.put("#SHIPPER_ADDRESS_3#", null);
            tmpMap.put("#SHIPPER_ADDRESS_4#", null);
            tmpMap.put("#SHIPPER_ADDRESS_PHONE#", null);
            tmpMap.put("#SHIPPER_ADDRESS_MOBILE#", null);
            tmpMap.put("#SHIPPER_ADDRESS_FAX#", null);
            tmpMap.put("#SHIPPER_ADDRESS_EMAIL#", null);

            if (address != null && address.getId() != null) {
                tmpMap.put("#SHIPPER_ADDRESS_1#", address.getAddressLine1());
                tmpMap.put("#SHIPPER_ADDRESS_2#", address.getAddressLine2());
                tmpMap.put("#SHIPPER_ADDRESS_3#", address.getAddressLine3() == null ? null : address.getAddressLine3());
                tmpMap.put("#SHIPPER_ADDRESS_4#", address.getAddressLine4());
                tmpMap.put("#SHIPPER_ADDRESS_PHONE#", address.getPhone() == null ? null : ("Ph: " + address.getPhone()));
                tmpMap.put("#SHIPPER_ADDRESS_MOBILE#", address.getMobile() == null ? null : ("Mobile: " + address.getMobile()));
                tmpMap.put("#SHIPPER_ADDRESS_FAX#", address.getFax() == null ? null : ("Fax: " + address.getFax()));
                tmpMap.put("#SHIPPER_ADDRESS_EMAIL#", address.getEmail() == null ? null : ("E-Mail: " + address.getEmail()));
            }

        } else if (whichParty.equals("CONSIGNEE") || whichParty == "CONSIGNEE") {
            tmpMap.put("#CONSIGNEE_ADDRESS_1#", null);
            tmpMap.put("#CONSIGNEE_ADDRESS_2#", null);
            tmpMap.put("#CONSIGNEE_ADDRESS_3#", null);
            tmpMap.put("#CONSIGNEE_ADDRESS_4#", null);
            tmpMap.put("#CONSIGNEE_ADDRESS_PHONE#", null);
            tmpMap.put("#CONSIGNEE_ADDRESS_MOBILE#", null);
            tmpMap.put("#CONSIGNEE_ADDRESS_FAX#", null);
            tmpMap.put("#CONSIGNEE_ADDRESS_EMAIL#", null);

            if (address != null && address.getId() != null) {
                tmpMap.put("#CONSIGNEE_ADDRESS_1#", address.getAddressLine1());
                tmpMap.put("#CONSIGNEE_ADDRESS_2#", address.getAddressLine2());
                tmpMap.put("#CONSIGNEE_ADDRESS_3#", address.getAddressLine3() == null ? null : address.getAddressLine3());
                tmpMap.put("#CONSIGNEE_ADDRESS_4#", address.getAddressLine4());
                tmpMap.put("#CONSIGNEE_ADDRESS_PHONE#", address.getPhone() == null ? null : ("Ph: " + address.getPhone()));
                tmpMap.put("#CONSIGNEE_ADDRESS_MOBILE#", address.getMobile() == null ? null : ("Mobile: " + address.getMobile()));
                tmpMap.put("#CONSIGNEE_ADDRESS_FAX#", address.getFax() == null ? null : ("Fax: " + address.getFax()));
                tmpMap.put("#CONSIGNEE_ADDRESS_EMAIL#", address.getEmail() == null ? null : ("E-Mail: " + address.getEmail()));
            }

        } else if (whichParty.equals("NOTIFY_PARTY") || whichParty == "NOTIFY_PARTY") {
            tmpMap.put("#NOTIFY_PARTY_ADDRESS_1#", null);
            tmpMap.put("#NOTIFY_PARTY_ADDRESS_2#", null);
            tmpMap.put("#NOTIFY_PARTY_ADDRESS_3#", null);
            tmpMap.put("#NOTIFY_PARTY_ADDRESS_4#", null);
            tmpMap.put("#NOTIFY_PARTY_ADDRESS_PHONE#", null);
            tmpMap.put("#NOTIFY_PARTY_ADDRESS_MOBILE#", null);
            tmpMap.put("#NOTIFY_PARTY_ADDRESS_FAX#", null);
            tmpMap.put("#NOTIFY_PARTY_ADDRESS_EMAIL#", null);

            if (address != null && address.getId() != null) {
                tmpMap.put("#NOTIFY_PARTY_ADDRESS_1#", address.getAddressLine1());
                tmpMap.put("#NOTIFY_PARTY_ADDRESS_2#", address.getAddressLine2());
                tmpMap.put("#NOTIFY_PARTY_ADDRESS_3#", address.getAddressLine3() == null ? null : address.getAddressLine3());
                tmpMap.put("#NOTIFY_PARTY_ADDRESS_4#", address.getAddressLine4());
                tmpMap.put("#NOTIFY_PARTY_ADDRESS_PHONE#", address.getPhone() == null ? null : ("Ph: " + address.getPhone()));
                tmpMap.put("#NOTIFY_PARTY_ADDRESS_MOBILE#", address.getMobile() == null ? null : ("Mobile: " + address.getMobile()));
                tmpMap.put("#NOTIFY_PARTY_ADDRESS_FAX#", address.getFax() == null ? null : ("Fax: " + address.getFax()));
                tmpMap.put("#NOTIFY_PARTY_ADDRESS_EMAIL#", address.getEmail() == null ? null : ("E-Mail: " + address.getEmail()));
            }


        }

        return tmpMap;
    }

    private String getPartyEmailList(PartyMaster partyMaster) {

        String emailList = null;
        if (partyMaster == null || partyMaster.getId() == null) {
            return null;
        } else {
            if (partyMaster.getPartyAddressList() != null && partyMaster.getPartyAddressList().size() > 0) {
                for (int i = 0; i < partyMaster.getPartyAddressList().size(); i++) {
                    if (partyMaster.getPartyAddressList().get(i).getAddressType().equals(AddressType.Primary)) {
                        emailList = partyMaster.getPartyAddressList().get(i).getEmail();
                    }
                }
            }
        }
        return emailList;
    }

    private String prepereDimensionListTemplete(DocumentDetail documentDetail) {

        String allDimensionTable = "";

        if (documentDetail.getDimensionList() != null && documentDetail.getDimensionList().size() != 0) {
            int srNo = 1;

            for (ServiceDocumentDimension serviceDocumentDimension : documentDetail.getDimensionList()) {
                String sNoTd = "";
                String noOfPieces = "";
                String length = "";
                String width = "";
                String height = "";
                String volumeWeight = "";
                String grossWeight = "";

                String dimensionTrs = "";

                String cssClass = "border: 1px solid #ba68c8;padding: 4px;border-collapse:collapse;text-align: right;";
                String leftAlignCssClass = "border: 1px solid #ba68c8;padding: 4px;border-collapse:collapse;text-align: left;";
                sNoTd = "<td style=" + "'" + leftAlignCssClass + "'" + ">" + srNo + "</td>";

                if (serviceDocumentDimension.getNoOfPiece() != 0) {
                    noOfPieces = "<td style=" + "'" + cssClass + "'" + ">" + serviceDocumentDimension.getNoOfPiece()
                            + "</td>";
                } else {
                    noOfPieces = "<td style=" + "'" + cssClass + "'" + ">-</td>";
                }

                if (serviceDocumentDimension.getLength() != 0) {
                    length = "<td style=" + "'" + cssClass + "'" + ">" + serviceDocumentDimension.getLength() + "</td>";
                } else {
                    length = "<td style=" + "'" + cssClass + "'" + ">-</td>";
                }

                if (serviceDocumentDimension.getWidth() != 0) {
                    width = "<td style=" + "'" + cssClass + "'" + ">" + serviceDocumentDimension.getWidth() + "</td>";
                } else {
                    width = "<td style=" + "'" + cssClass + "'" + ">-</td>";
                }

                if (serviceDocumentDimension.getHeight() != 0) {
                    height = "<td style=" + "'" + cssClass + "'" + ">" + serviceDocumentDimension.getHeight() + "</td>";
                } else {
                    height = "<td style=" + "'" + cssClass + "'" + ">-</td>";
                }

                if (serviceDocumentDimension.getVolWeight() != 0) {
                    volumeWeight = "<td style=" + "'" + cssClass + "'" + ">" + getWeight(serviceDocumentDimension.getVolWeight())
                            + "</td>";
                } else {
                    volumeWeight = "<td style=" + "'" + cssClass + "'" + ">-</td>";
                }

                if (serviceDocumentDimension.getGrossWeight() != 0) {
                    grossWeight = "<td style=" + "'" + cssClass + "'" + ">" + getWeight(serviceDocumentDimension.getGrossWeight())
                            + "</td>";
                } else {
                    grossWeight = "<td style=" + "'" + cssClass + "'" + ">-</td>";
                }

                String tr = "<tr style=" + "'border-collapse:collapse;" + "'>\n" + sNoTd + "\n" + noOfPieces + "\n"
                        + length + "\n" + width + "\n" + height + "\n" + volumeWeight + "\n" + grossWeight + "\n"
                        + "</tr>";
                srNo++;
                dimensionTrs += tr + "\n";

                allDimensionTable += dimensionTrs + "\n";

            }

        }
        return allDimensionTable;
    }

    private String getWeight(Double val) {
        DecimalFormat df = new DecimalFormat("#0.00");
        if (val == null) {
            return null;
        }
        return df.format(val);

    }

}
