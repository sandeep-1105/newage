package com.efreightsuite.service.mailer;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import com.efreightsuite.common.component.CacheRepository;
import com.efreightsuite.common.component.EmailRequestService;
import com.efreightsuite.enumeration.ReportName;
import com.efreightsuite.enumeration.TemplateType;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.model.EmailRequest;
import com.efreightsuite.model.EmailRequestAttachment;
import com.efreightsuite.model.EmailTemplate;
import com.efreightsuite.model.ShipmentServiceDetail;
import com.efreightsuite.model.UserProfile;
import com.efreightsuite.repository.DocumentDetailRepository;
import com.efreightsuite.repository.EmailTemplateRepository;
import com.efreightsuite.util.ReportUtil;
import com.efreightsuite.util.TimeUtil;
import com.efreightsuite.util.ValidateUtil;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service
@Log4j2
public class ShipmentReportMailer {

    @Autowired
    MailerUtil mailerUtil;

    @Autowired
    PartyEmailUtil partyEmailUtil;

    @Autowired
    EmailTemplateRepository emailTemplateRepository;

    @Autowired
    EmailRequestService emailRequestService;

    @Autowired
    CacheRepository cacheRepository;

    @Autowired
    DocumentDetailRepository documentDetailRepository;

    @Autowired
    ReportUtil reportUtil;


    @Async
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void sendShipmentMailToCustomer(Long shipmentId, UserProfile userProfile, ReportName reportName, byte[] a) {

        log.info("sendShipmentMailer start: ");
        //EmailTemplate emailTemplate = cacheRepository.get(SaaSUtil.getSaaSId(), EmailTemplate.class.getName(), TemplateType.SHIPMENT_REPORT_ATTACHMENT, emailTemplateRepository);
        EmailTemplate emailTemplate = emailTemplateRepository.findByTemplateType(TemplateType.SHIPMENT_REPORT_ATTACHMENT);
        if (emailTemplate == null) {
            log.error("Shipment Mailer Template is not available");
            return;
        }

        log.info("Getting Service Detail basesed on DocumentID");
        ShipmentServiceDetail service = documentDetailRepository.findByService(shipmentId);
        ValidateUtil.notNull(partyEmailUtil.getPartyEmailList(service.getParty()), ErrorCode.QUOTATION_CUSTOMER_EMAIL_REQUIRED);
        log.info("PartyMaster Email Getting");
        byte[] attachmentArr = a;
        if (attachmentArr == null) {
            log.error("Shipment generation failed");
            return;
        }

        try {
            Map<String, Object> mapSubject = new HashMap<>();
            mapSubject.put("#SHIPMENT_UID#", service.getShipmentUid());
            Map<String, Object> mapBody = new HashMap<>();
            mapBody.put("#EMP_NAME#", service.getCustomerService().getEmployeeName());
            mapBody.put("#EMP_EMAIL#", service.getCustomerService().getEmail());
            mapBody.put("#EMP_TELNO#", service.getCustomerService().getEmployeePhoneNo());
            // sending email

            EmailRequest emailRequest = new EmailRequest();
            emailRequest.setFromEmailId(emailTemplate.getFromEmailId());
            emailRequest.setFromEmailIdPassword(emailTemplate.getFromEmailPassword());
            emailRequest.setToEmailIdList(partyEmailUtil.getPartyEmailList(service.getParty()));
            emailRequest.setCcEmailIdList(service.getCustomerService().getEmail());
            emailRequest.setEmailType(emailTemplate.getEmailType());
            emailRequest.setPort(emailTemplate.getPort());
            emailRequest.setSmtpServerAddress(emailTemplate.getSmtpServerAddress());

            emailRequest.setSubject(emailTemplate.getSubject());
            emailRequest.setEmailBody(emailTemplate.getTemplate());

            for (String key : mapSubject.keySet()) {
                emailRequest.setSubject(emailRequest.getSubject().replaceAll(key, mapSubject.get(key) == null ? "" : mapSubject.get(key).toString()));
            }

            for (String key : mapBody.keySet()) {
                emailRequest.setEmailBody(emailRequest.getEmailBody().replaceAll(key, mapBody.get(key) == null ? "" : mapBody.get(key).toString()));
            }

            // Shipment reports pdf setting to attachment
            Set<EmailRequestAttachment> attachmentList = new HashSet<>();
            if (attachmentArr != null) {
                EmailRequestAttachment emailRequestAttachment = new EmailRequestAttachment();
                emailRequestAttachment.setAttachement(attachmentArr);
                emailRequestAttachment.setFileName(reportName + "_" + service.getShipmentUid() + "." + "pdf");
                emailRequestAttachment.setMimeType("application/pdf");
                emailRequestAttachment.setManualSystemTrack(TimeUtil.getCreateSystemTrack(userProfile));
                attachmentList.add(emailRequestAttachment);
            }
            emailRequest.setAttachmentList(attachmentList);
            emailRequestService.saveAndSendEmail(emailRequest, userProfile, mailerUtil.getLogo(service.getLocation()));
        } catch (RestException re) {
            log.error("RestException in EmailAsyncService.sendShipmentMailtoCustomer: ", re);

        } catch (Exception e) {
            log.error("Exception in EmailAsyncService.sendShipmentMailtoCustomer : ", e);
        }

    }
}

