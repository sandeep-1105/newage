package com.efreightsuite.service.mailer;

import com.efreightsuite.enumeration.AddressType;
import com.efreightsuite.model.PartyMaster;
import lombok.extern.log4j.Log4j2;

import org.springframework.stereotype.Service;

@Service
@Log4j2
public class PartyEmailUtil {

    public String getPartyEmailList(PartyMaster partyMaster) {

        String emailList = null;
        if (partyMaster == null || partyMaster.getId() == null) {
            return null;
        } else {
            if (partyMaster.getPartyAddressList() != null && partyMaster.getPartyAddressList().size() > 0) {
                for (int i = 0; i < partyMaster.getPartyAddressList().size(); i++) {
                    if (partyMaster.getPartyAddressList().get(i).getAddressType().equals(AddressType.Primary)) {
                        emailList = partyMaster.getPartyAddressList().get(i).getEmail();
                        log.info("PartyMaster Email::::::" + emailList);
                    }
                }
            }
        }
        return emailList;
    }

}
