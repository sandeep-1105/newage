package com.efreightsuite.service.mailer;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import com.efreightsuite.common.component.CacheRepository;
import com.efreightsuite.common.component.EmailRequestService;
import com.efreightsuite.enumeration.ReportName;
import com.efreightsuite.enumeration.TemplateType;
import com.efreightsuite.enumeration.YesNo;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.model.Consol;
import com.efreightsuite.model.EmailRequest;
import com.efreightsuite.model.EmailRequestAttachment;
import com.efreightsuite.model.EmailTemplate;
import com.efreightsuite.model.UserProfile;
import com.efreightsuite.repository.ConsolRepository;
import com.efreightsuite.repository.EmailTemplateRepository;
import com.efreightsuite.util.TimeUtil;
import com.efreightsuite.util.ValidateUtil;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service
@Log4j2
public class ConsolReportMailer {

    @Autowired
    MailerUtil mailerUtil;

    @Autowired
    PartyEmailUtil partyEmailUtil;

    @Autowired
    EmailTemplateRepository emailTemplateRepository;

    @Autowired
    EmailRequestService emailRequestService;

    @Autowired
    CacheRepository cacheRepository;

    @Autowired
    ConsolRepository consolRepository;

    @Async
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void sendConsolMailToCustomer(Long consolId, UserProfile userProfile, ReportName reportName, byte[] a) {
        log.info("Consol Single Report Mailer  start:..... ");

        //EmailTemplate emailTemplate = cacheRepository.get(SaaSUtil.getSaaSId(), EmailTemplate.class.getName(), TemplateType.CONSOL_REPORT_ATTACHMENT, emailTemplateRepository);
        EmailTemplate emailTemplate = emailTemplateRepository.findByTemplateType(TemplateType.CONSOL_REPORT_ATTACHMENT);

        if (emailTemplate == null) {
            log.error("Consol Approval Template is not available");
            return;
        }

        Consol consol = consolRepository.findById(consolId);
        if (consol != null && consol.getConsolDocument() != null && consol.getConsolDocument().getHoldNote() != null && consol.getConsolDocument().getHoldNote().equals(YesNo.Yes)) {
            if (reportName.equals(ReportName.MASTER_DO)) {
                return;
            }
        }
        ValidateUtil.notNull(partyEmailUtil.getPartyEmailList(consol.getAgent()), ErrorCode.QUOTATION_CUSTOMER_EMAIL_REQUIRED);

        byte[] attachmentArr = a;
        if (attachmentArr == null) {
            log.error("Consol generation failed");
            return;
        }
        try {

            Map<String, Object> mapSubject = new HashMap<>();
            mapSubject.put("#QUOTATION_NO#", consol.getConsolUid());
            mapSubject.put("#CONSOL_UID#", consol.getConsolUid());

            Map<String, Object> mapBody = new HashMap<>();

            mapBody.put("#EMP_NAME#", consol.getCreatedBy().getEmployeeName());
            mapBody.put("#EMP_EMAIL#", consol.getCreatedBy().getEmail());
            mapBody.put("#EMP_TELNO#", consol.getCreatedBy().getEmployeePhoneNo());
            // sending email

            EmailRequest emailRequest = new EmailRequest();
            emailRequest.setFromEmailId(emailTemplate.getFromEmailId());
            emailRequest.setFromEmailIdPassword(emailTemplate.getFromEmailPassword());
            emailRequest.setToEmailIdList(partyEmailUtil.getPartyEmailList(consol.getAgent()));
            emailRequest.setCcEmailIdList(consol.getCreatedBy().getEmail());
            emailRequest.setEmailType(emailTemplate.getEmailType());
            emailRequest.setPort(emailTemplate.getPort());
            emailRequest.setSmtpServerAddress(emailTemplate.getSmtpServerAddress());

            emailRequest.setSubject(emailTemplate.getSubject());
            emailRequest.setEmailBody(emailTemplate.getTemplate());

            for (String key : mapSubject.keySet()) {
                emailRequest.setSubject(emailRequest.getSubject().replaceAll(key, mapSubject.get(key) == null ? "" : mapSubject.get(key).toString()));
            }

            for (String key : mapBody.keySet()) {
                emailRequest.setEmailBody(emailRequest.getEmailBody().replaceAll(key, mapBody.get(key) == null ? "" : mapBody.get(key).toString()));
            }

            // consol report pdf setting to attachment
            Set<EmailRequestAttachment> attachmentList = new HashSet<>();
            if (attachmentArr != null) {
                EmailRequestAttachment emailRequestAttachment = new EmailRequestAttachment();
                emailRequestAttachment.setAttachement(attachmentArr);
                emailRequestAttachment.setFileName(reportName + "_" + consol.getConsolUid() + "." + "pdf");
                emailRequestAttachment.setMimeType("application/pdf");
                emailRequestAttachment.setManualSystemTrack(TimeUtil.getCreateSystemTrack(userProfile));
                attachmentList.add(emailRequestAttachment);
            }
            emailRequest.setAttachmentList(attachmentList);
            emailRequestService.saveAndSendEmail(emailRequest, userProfile, mailerUtil.getLogo(consol.getLocation()));
        } catch (RestException re) {
            log.error("RestException in EmailAsyncService.sendConsolMailToCustomer : ", re);

        } catch (Exception e) {
            log.error("Exception in EmailAsyncService.sendConsolMailToCustomer : ", e);
        }

    }
}

