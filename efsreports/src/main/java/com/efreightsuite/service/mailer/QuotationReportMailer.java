package com.efreightsuite.service.mailer;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import com.efreightsuite.common.component.CacheRepository;
import com.efreightsuite.common.component.EmailRequestService;
import com.efreightsuite.enumeration.ReportName;
import com.efreightsuite.enumeration.TemplateType;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.model.EmailRequest;
import com.efreightsuite.model.EmailRequestAttachment;
import com.efreightsuite.model.EmailTemplate;
import com.efreightsuite.model.Quotation;
import com.efreightsuite.model.UserProfile;
import com.efreightsuite.repository.EmailTemplateRepository;
import com.efreightsuite.repository.QuotationRepository;
import com.efreightsuite.util.SaaSUtil;
import com.efreightsuite.util.TimeUtil;
import com.efreightsuite.util.ValidateUtil;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service
@Log4j2
public class QuotationReportMailer {

    @Autowired
    MailerUtil mailerUtil;

    @Autowired
    PartyEmailUtil partyEmailUtil;

    @Autowired
    EmailTemplateRepository emailTemplateRepository;

    @Autowired
    EmailRequestService emailRequestService;

    @Autowired
    QuotationRepository quotationRepository;

    @Autowired
    CacheRepository cacheRepository;

    @Async
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void sendQuotationMailToCustomer(Long quotationId, UserProfile userProfile, ReportName reportName, byte[] a) {

        log.info("sendQuotationMailToCustomer start: ");

        EmailTemplate emailTemplate = cacheRepository.get(SaaSUtil.getSaaSId(), EmailTemplate.class.getName(), TemplateType.QUOTATION_REPORT_ATTACHMENT, emailTemplateRepository);

        if (emailTemplate == null) {
            log.error("Quotation Approval Template is not available");
            return;
        }

        Quotation quotation = quotationRepository.findById(quotationId);

        ValidateUtil.notNull(partyEmailUtil.getPartyEmailList(quotation.getCustomer()), ErrorCode.QUOTATION_CUSTOMER_EMAIL_REQUIRED);
        byte[] attachmentArr = a;

        if (attachmentArr == null) {
            log.error("Quotation generation failed");
            return;
        }

        try {
            Map<String, Object> mapSubject = new HashMap<>();
            mapSubject.put("#QUOTATION_NO#", quotation.getQuotationNo());

            Map<String, Object> mapBody = new HashMap<>();

            mapBody.put("#COMPANY_NAME#", quotation.getCompanyMaster().getCompanyName());
            mapBody.put("#EMP_NAME#", quotation.getLoggedBy().getEmployeeName());
            mapBody.put("#EMP_EMAIL#", quotation.getLoggedBy().getEmail());
            mapBody.put("#EMP_TELNO#", quotation.getLoggedBy().getEmployeePhoneNo());
            // sending email

            EmailRequest emailRequest = new EmailRequest();
            emailRequest.setFromEmailId(emailTemplate.getFromEmailId());
            emailRequest.setFromEmailIdPassword(emailTemplate.getFromEmailPassword());
            emailRequest.setToEmailIdList(partyEmailUtil.getPartyEmailList(quotation.getCustomer()));
            emailRequest.setCcEmailIdList(quotation.getLoggedBy().getEmail());
            emailRequest.setEmailType(emailTemplate.getEmailType());
            emailRequest.setPort(emailTemplate.getPort());
            emailRequest.setSmtpServerAddress(emailTemplate.getSmtpServerAddress());

            emailRequest.setSubject(emailTemplate.getSubject());
            emailRequest.setEmailBody(emailTemplate.getTemplate());

            for (String key : mapSubject.keySet()) {
                emailRequest.setSubject(emailRequest.getSubject().replaceAll(key, mapSubject.get(key) == null ? "" : mapSubject.get(key).toString()));
            }

            for (String key : mapBody.keySet()) {
                emailRequest.setEmailBody(emailRequest.getEmailBody().replaceAll(key, mapBody.get(key) == null ? "" : mapBody.get(key).toString()));
            }

            // quotation report pdf setting to attachment
            Set<EmailRequestAttachment> attachmentList = new HashSet<>();
            if (attachmentArr != null) {

                EmailRequestAttachment emailRequestAttachment = new EmailRequestAttachment();
                emailRequestAttachment.setAttachement(attachmentArr);
                emailRequestAttachment.setFileName(reportName + "_" + quotation.getQuotationNo() + "." + "pdf");
                log.info("EMAIL_REQUEST_ATTACHMENT FILE_NAME::::" + emailRequestAttachment.getFileName());
                emailRequestAttachment.setMimeType("application/pdf");
                emailRequestAttachment.setManualSystemTrack(TimeUtil.getCreateSystemTrack(userProfile));
                attachmentList.add(emailRequestAttachment);
            }

            emailRequest.setAttachmentList(attachmentList);

            emailRequestService.saveAndSendEmail(emailRequest, userProfile, mailerUtil.getLogo(quotation.getLocationMaster()));
        } catch (RestException re) {
            log.error("RestException in EmailAsyncService.sendQuotationMailToCustomer : ", re);

        } catch (Exception e) {
            log.error("Exception in EmailAsyncService.sendQuotationMailToCustomer : ", e);
        }

    }
}

