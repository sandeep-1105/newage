package com.efreightsuite.service.mailer;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import com.efreightsuite.common.component.CacheRepository;
import com.efreightsuite.common.component.EmailRequestService;
import com.efreightsuite.enumeration.ReportName;
import com.efreightsuite.enumeration.TemplateType;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.model.EmailRequest;
import com.efreightsuite.model.EmailRequestAttachment;
import com.efreightsuite.model.EmailTemplate;
import com.efreightsuite.model.InvoiceCreditNote;
import com.efreightsuite.model.UserProfile;
import com.efreightsuite.repository.EmailTemplateRepository;
import com.efreightsuite.repository.InvoiceCreditNoteRepository;
import com.efreightsuite.util.TimeUtil;
import com.efreightsuite.util.ValidateUtil;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service
@Log4j2
public class CreditNoteSingleReportMailer {

    @Autowired
    MailerUtil mailerUtil;

    @Autowired
    PartyEmailUtil partyEmailUtil;

    @Autowired
    EmailTemplateRepository emailTemplateRepository;

    @Autowired
    EmailRequestService emailRequestService;

    @Autowired
    CacheRepository cacheRepository;

    @Autowired
    InvoiceCreditNoteRepository invoiceCreditNoteRepository;

    @Async
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void sendCreditNoteMailCustomer(Long invoiceId, UserProfile userProfile, ReportName reportName, byte[] a) {
        log.info("Credit Note Single Report Mailer  start:..... ");

        EmailTemplate emailTemplate = emailTemplateRepository.findByTemplateType(TemplateType.CREDIT_NOTE_REPORT_ATTACHMENT);
        if (emailTemplate == null) {
            log.error("Credit Note Template is not available");
            return;
        }

        InvoiceCreditNote invoice = invoiceCreditNoteRepository.findById(invoiceId);

        ValidateUtil.notNull(partyEmailUtil.getPartyEmailList(invoice.getParty()), ErrorCode.QUOTATION_CUSTOMER_EMAIL_REQUIRED);

        byte[] attachmentArr = a;
        if (attachmentArr == null) {
            log.error("Credit Note generation failed");
            return;
        }
        try {
            Map<String, Object> mapSubject = new HashMap<>();
            mapSubject.put("#CREDIT_NOTE_NO#", invoice.getInvoiceCreditNoteNo());

            Map<String, Object> mapBody = new HashMap<>();

            mapBody.put("#EMP_NAME#", userProfile.getEmployee().getEmployeeName());
            // sending email

            EmailRequest emailRequest = new EmailRequest();
            emailRequest.setFromEmailId(emailTemplate.getFromEmailId());
            emailRequest.setFromEmailIdPassword(emailTemplate.getFromEmailPassword());
            emailRequest.setToEmailIdList(partyEmailUtil.getPartyEmailList(invoice.getParty()));
            emailRequest.setCcEmailIdList(userProfile.getEmployee().getEmail());
            emailRequest.setEmailType(emailTemplate.getEmailType());
            emailRequest.setPort(emailTemplate.getPort());
            emailRequest.setSmtpServerAddress(emailTemplate.getSmtpServerAddress());

            emailRequest.setSubject(emailTemplate.getSubject());
            emailRequest.setEmailBody(emailTemplate.getTemplate());

            for (String key : mapSubject.keySet()) {
                emailRequest.setSubject(emailRequest.getSubject().replaceAll(key, mapSubject.get(key) == null ? "" : mapSubject.get(key).toString()));
            }

            for (String key : mapBody.keySet()) {
                emailRequest.setEmailBody(emailRequest.getEmailBody().replaceAll(key, mapBody.get(key) == null ? "" : mapBody.get(key).toString()));
            }

            //Credit Note report pdf setting to attachment
            Set<EmailRequestAttachment> attachmentList = new HashSet<>();
            if (attachmentArr != null) {
                EmailRequestAttachment emailRequestAttachment = new EmailRequestAttachment();
                emailRequestAttachment.setAttachement(attachmentArr);
                emailRequestAttachment.setFileName(reportName + "." + "pdf");
                emailRequestAttachment.setMimeType("application/pdf");
                emailRequestAttachment.setManualSystemTrack(TimeUtil.getCreateSystemTrack(userProfile));
                attachmentList.add(emailRequestAttachment);
            }
            emailRequest.setAttachmentList(attachmentList);
            emailRequestService.saveAndSendEmail(emailRequest, userProfile, mailerUtil.getLogo(invoice.getLocation()));
        } catch (RestException re) {
            log.error("RestException in EmailAsyncService.sendCredit Note MailToCustomer : ", re);

        } catch (Exception e) {
            log.error("Exception in EmailAsyncService.sendCredit Note MailToCustomer : ", e);
        }

    }
}

