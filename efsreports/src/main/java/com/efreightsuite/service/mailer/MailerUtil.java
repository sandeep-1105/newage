package com.efreightsuite.service.mailer;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.HashMap;
import java.util.Map;

import javax.imageio.ImageIO;

import com.efreightsuite.common.component.CacheRepository;
import com.efreightsuite.model.LocationMaster;
import com.efreightsuite.model.LogoMaster;
import com.efreightsuite.repository.LogoMasterRepository;
import lombok.extern.log4j.Log4j2;
import org.jfree.util.Log;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class MailerUtil {

    @Value("${efs.resource.path}")
    String appResourcePath;

    @Autowired
    LogoMasterRepository logoMasterRepository;

    @Autowired
    CacheRepository cacheRepository;

    public Map<String, byte[]> getLogo(LocationMaster location) {
        Map<String, byte[]> logoMap = new HashMap<String, byte[]>();
        logoMap.put("#COMPANY_LOGO#", null);
        logoMap.put("#POWERED_BY#", null);

        try {

            LogoMaster logoMaster = logoMasterRepository.findByLocationMaster(location);

            if (logoMaster != null && logoMaster.getLogo() != null) {
                logoMap.put("#COMPANY_LOGO#", logoMaster.getLogo());
            } else {
                logoMap.put("#COMPANY_LOGO#", null);
            }

            ByteArrayOutputStream baos = new ByteArrayOutputStream(1000);
            BufferedImage img = ImageIO.read(new File(appResourcePath, "master-data/newage_small.png"));
            ImageIO.write(img, "png", baos);
            baos.flush();
            baos.close();

            logoMap.put("#POWERED_BY#", baos.toByteArray());

        } catch (Exception e) {
            Log.error("Not able to read powered by image in resource path ", e);
            logoMap.put("#POWERED_BY#", null);
        }

        return logoMap;
    }

}
