package com.efreightsuite.security;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;

import com.efreightsuite.util.TrackIdGenerator;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;
import org.apache.log4j.MDC;

import org.springframework.stereotype.Service;

@Log4j2
@WebFilter("/*")
@Service
public class NewAgeFilter implements Filter {

    @Getter
    @Setter
    TrackIdGenerator trackIdGenerator;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        log.debug("NewAgeFilter init");
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {

        if (trackIdGenerator != null) {
            String trackId = trackIdGenerator.getTrackId((HttpServletRequest) request);
            MDC.put("TrackId", trackId);
        }
        chain.doFilter(request, response);
        MDC.clear();
    }

    @Override
    public void destroy() {
        log.debug("NewAgeFilter destroy");

    }

}
