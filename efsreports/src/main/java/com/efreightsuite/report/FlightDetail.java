package com.efreightsuite.report;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class FlightDetail {
    private String destinationName;
    private String flightNo;
    private String etdDate;
    private String carrierName;
}
