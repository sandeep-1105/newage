package com.efreightsuite.report;

import lombok.Data;

@Data
public class DimensionDto {

    private String length;
    private String width;
    private String height;
    private String marksNumbers;
    private String packages;
    private String pieces;
}
