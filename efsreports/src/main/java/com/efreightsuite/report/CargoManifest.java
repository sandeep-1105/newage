package com.efreightsuite.report;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class CargoManifest {
    private String hawbNumber;
    private String originName;
    private String destinationName;
    private String noOfPieces;
    private String grossWeight;
    private String volume;
    private String frieghtName;
    private String chargeableWeight;
    private String totalGrossWeight = "";
    private String totalChargeableWeight = "";
    private String totalVolume = "";
    private String totalPieces = "";
    private String commodityDescription;
    private String shipperDetails;
    private String consigneeDetails;
    private String shipperName;
    private String consigneeName;

    private String asAgreed;
    private String aesNumber;

    //For Air Cargo Load Plan
    private String onHandNo;
    private String warehouseLocation;
    private String packs;
    private String volumeWeight;
    private String haz;
    private String proNo;
    private String warehouseRemarks;
    private String totalVolumeWeight = "";

    private String chargeName;
    private String currencyCode;
    private String currencyAmount;

}
