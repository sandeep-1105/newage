package com.efreightsuite.report;

import lombok.Data;

@Data
public class InvoiceDetail {
    private String chargeName;
    private String exchangeRate;
    private String amount = "";
    private String multiplyLocalAmount = "";
    private String currencyCode;
}
