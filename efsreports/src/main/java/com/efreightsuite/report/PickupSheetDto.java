package com.efreightsuite.report;

import lombok.Data;

@Data
public class PickupSheetDto {
    private String shipperName;
    private String consigneeName;
    private String shipperId;
    private String plannedPickupDate;
    private String actualPickupDate;
    private String cargoReceivedDate;
    private String origin;
    private String destination;
    private String trucker;
    private String remarks;
    private String carrierName;
    private String portCountry;
    private String volumeInKg;
    private String cost;
    private String shipmentAndSeviceId;

}
