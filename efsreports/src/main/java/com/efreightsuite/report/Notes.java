package com.efreightsuite.report;

import lombok.Data;

@Data
public class Notes {

    String reportNote;

}
