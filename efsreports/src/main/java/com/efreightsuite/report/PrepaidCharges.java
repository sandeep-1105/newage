package com.efreightsuite.report;

import lombok.Data;

@Data
public class PrepaidCharges {
    private String chargeName;
    private String units;
    private String exRate;
    private String amount = "";
    private String totalLocalAmount = "";
    private String currency;

}
