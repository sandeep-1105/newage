package com.efreightsuite.report;

import lombok.Data;

@Data
public class VolumeReportCountryDto {

    private String country;
    private String portName;
    private String agentName;
    private String shipements;
    private String volumeInKgs;
    private String shipementsAgent;
    private String volumeInKgsAgent;
    private String totalShipments;
    private String totalVolumeKgs;

}
