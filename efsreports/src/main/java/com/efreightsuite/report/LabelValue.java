package com.efreightsuite.report;

import lombok.Data;

@Data
public class LabelValue {

    String label;
    String value;

}
