package com.efreightsuite.report;

import lombok.Data;

@Data
public class JobLedgerReportDto {
    String originName;
    String fdc;
    String ppcc;
    String hawb;
    String customer;
    String salesman;
    String grossWeight;
    String volumeWeight;
    String revenue;
    String cost;
    String neutral;
    String gp;
    String actualRevenue;
    String actualCost;
    String actualNeutral;
    String actualGp;
    String gpPercentage;
    String gpUnit;
    Integer index;

}
