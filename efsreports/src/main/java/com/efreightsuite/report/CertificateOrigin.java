package com.efreightsuite.report;

import lombok.Data;

@Data
public class CertificateOrigin {
    private String referenceNumber;
    private String bookingNumber;
    private String consigneeName;
    private String taxIdNumber;
    private String carrierName;
    private String flightNo;
    private String quantity;
    private String volumeCBM;
    private String grossWeight;
    private String currentDate;
    private String currentPlace;
    private String consigneeAddressLine1;
    private String consigneeAddressLine2;
    private String consigneeAddressLine3;
    private String consigneeAddressLine4;
    private String refPlace;
    private String exportImportName;
    private String exportImportAddress1;
    private String exportImportAddress2;
    private String exportImportPhone;
    private String marksAndNo;
    private String commodityGroup;

    private String exportImportAddress3;
    private String exportImportAddress4;
    private String exporterImporterName;

    private String packages;
    private String pieces;
    private String bothCode;

}
