package com.efreightsuite.report;

import lombok.Data;

@Data
public class ExportJobCard {
    private String hawbNumber;
    private String ppcc;
    private String tosCode;
    private String destination;
    private String shipper;
    private String qty;
    private String grossWeight;
    private String chargeableWeight;
    private String totalGrossWeight = "";
    private String totalChargeableWeight = "";
    private String totalQty = "";
}
