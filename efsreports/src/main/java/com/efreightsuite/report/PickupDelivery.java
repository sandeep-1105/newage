package com.efreightsuite.report;

import lombok.Data;

@Data
public class PickupDelivery {

    private String kgsLbsValue;
    private String description;
    private String cftcbmValue;
    private String pieces;
}
