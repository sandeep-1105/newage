package com.efreightsuite.report;

import lombok.Data;

@Data
public class TruckingInstruction {
    private String phoneNumber;
    private String faxNumber;
    private String pol;
    private String pod;
    private String refDate;
    private String flightName;
    private String volumeGoods;
    private String carrierBookingNo;
    private String packages;
    private String flightNo;
    private String referenceNo;
    private String grossWeight;
    private String refPlace;

}
