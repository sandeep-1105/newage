package com.efreightsuite.report;

import com.efreightsuite.dto.JobLedgerResDto;
import lombok.Data;

@Data
public class JobCostSheet {
    String chargeCode;
    String chargeName;
    String revenue;
    String cost;
    String neutral;
    String gp;
    String originName;
    String fdc;
    String ppcc;
    String hawb;
    String customer;
    String salesman;
    String grossWeight;
    String volumeWeight;


    Integer index;

    String actualRevenue;
    String actualCost;
    String actualNeutral;
    String actualGp;
    String gpPercentage;
    String gpUnit;
    //JobLedgerResDto
    /*JobLedgerResChargeDto jobLedgerResChargeDto;
	
	public JobCostSheet(){
		
	}
	public JobCostSheet(JobLedgerResChargeDto jobLedgerResChargeDto1){
		this.jobLedgerResChargeDto=jobLedgerResChargeDto1;
	}*/

    JobLedgerResDto jobLedgerResDto;

    public JobCostSheet() {

    }

    public JobCostSheet(JobLedgerResDto jobLedgerResDto1) {
        this.jobLedgerResDto = jobLedgerResDto1;
    }
}
