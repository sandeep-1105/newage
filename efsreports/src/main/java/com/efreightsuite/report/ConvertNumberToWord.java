package com.efreightsuite.report;

import org.springframework.stereotype.Service;

@Service
public class ConvertNumberToWord {
    String string;

    String a[] = {"", "One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine",};

    String b[] = {"Hundred", "Thousand", "Lakh", "Crore"};

    String c[] = {"Ten", "Eleven", "Twelve", "Thirteen", "Fourteen", "Fifteen", "Sixteen", "Seventeen", "Eighteen", "Ninteen",};

    String d[] = {"Twenty", "Thirty", "Fourty", "Fifty", "Sixty", "Seventy", "Eighty", "Ninty"};

    public String convertNumToWord(int number) {

        if (number == 0) {
            return "Zero";
        }
        int c = 1;
        int rm;
        string = "";
        while (number != 0) {
            switch (c) {
                case 1:
                    rm = number % 100;
                    pass(rm);
                    if (number > 100 && number % 100 != 0) {
                        display("and ");
                    }
                    number /= 100;

                    break;

                case 2:
                    rm = number % 10;
                    if (rm != 0) {
                        display(" ");
                        display(b[0]);
                        display(" ");
                        pass(rm);
                    }
                    number /= 10;
                    break;

                case 3:
                    rm = number % 100;
                    if (rm != 0) {
                        display(" ");
                        display(b[1]);
                        display(" ");
                        pass(rm);
                    }
                    number /= 100;
                    break;

                case 4:
                    rm = number % 100;
                    if (rm != 0) {
                        display(" ");
                        display(b[2]);
                        display(" ");
                        pass(rm);
                    }
                    number /= 100;
                    break;

                case 5:
                    rm = number % 100;
                    if (rm != 0) {
                        display(" ");
                        display(b[3]);
                        display(" ");
                        pass(rm);
                    }
                    number /= 100;
                    break;

            }
            c++;
        }

        return string;
    }

    public void pass(int number) {
        int rm, q;
        if (number < 10) {
            display(a[number]);
        }

        if (number > 9 && number < 20) {
            display(c[number - 10]);
        }

        if (number > 19) {
            rm = number % 10;
            if (rm == 0) {
                q = number / 10;
                display(d[q - 2]);
            } else {
                q = number / 10;
                display(a[rm]);
                display(" ");
                display(d[q - 2]);
            }
        }
    }

    public void display(String s) {
        String t;
        t = string;
        string = s;
        string += t;
    }

    public String getNumbersInWord(String strValue) {
        String rtnStr = "";
        int intValue = 0;
        boolean validInput = true;
        boolean decimalPresent = false;
        String paisaStr = "";
        try {
            // double dblValue = Double.parseDouble(strValue);
            try {
                intValue = Integer.parseInt(strValue);
            } catch (NumberFormatException nfe) {
                validInput = false;
                // Check if . is presnet like 2000.00
                try {
                    int idx = strValue.indexOf(".");

                    if (idx > -1) {
                        decimalPresent = true;
                        String tmpStr1 = strValue.substring(0, idx);
                        // log.info("tmpStr1 " + tmpStr1);
                        intValue = Integer.parseInt(tmpStr1);
                        paisaStr = strValue.substring(idx + 1);
                        validInput = true;
                    }
                } catch (NumberFormatException nfe2) {
                    //log.info("nfe2 1 " + nfe2.toString());
                    validInput = false;
                } catch (Exception e) {
                    //log.info("nfe2 2 " + e.toString());
                }
            }
            if (validInput) {
                // DecimalFormat twoPlaces = new DecimalFormat("#,##0.00");
                // String output = twoPlaces.format(dblValue);
                ConvertNumberToWord nw = new ConvertNumberToWord();
                String paisa = "";
                if (decimalPresent) {
                    paisa = nw.convertNumToWord(Integer.parseInt(paisaStr));
                    rtnStr = nw.convertNumToWord(intValue) + " RUPEES" + " And " + paisa
                            + " Paisa Only";
                } else {
                    rtnStr = nw.convertNumToWord(intValue);
                }
            } else {
                rtnStr = strValue;
            }
        } catch (Exception e) {
            //logger.fatal("formatAndNumWord() Exception " + e.toString());
            rtnStr = strValue;
        }
        return rtnStr;
    }

}
