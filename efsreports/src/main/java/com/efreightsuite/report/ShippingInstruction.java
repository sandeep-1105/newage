package com.efreightsuite.report;

import lombok.Data;

@Data
public class ShippingInstruction {
    private String pieces;
    private String description;
    private String cfsReceiptNo;
    private String grossWeight;
    private String volume;
    private String documentNo;
    private String length;
    private String width;
    private String height;
    private String marksNumbers;
    private String packages;

}
