package com.efreightsuite.report;

import java.awt.image.BufferedImage;

import lombok.Data;

@Data
public class CustomerInvoiceDetail {

    private String chargeName;
    private String currencyCode;
    private String perUnit;
    private String unit;
    private String amount;
    private String roe;
    private String localAmount;
    private String tax;
    private String taxName;
    private String taxAmount;
    private BufferedImage tickMark;
}
