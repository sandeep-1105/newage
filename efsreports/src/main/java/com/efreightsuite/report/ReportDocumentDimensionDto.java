package com.efreightsuite.report;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ReportDocumentDimensionDto implements Serializable {


    /**
     *
     */
    private static final long serialVersionUID = 1L;

    int noOfPiece;

    int length;
    int width;
    int height;
}
