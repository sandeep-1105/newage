package com.efreightsuite.report;

import lombok.Data;

@Data
public class HawbReportRider {
    String hawbNo;
    String commodityDescription;
    String marksAndNo;


}
