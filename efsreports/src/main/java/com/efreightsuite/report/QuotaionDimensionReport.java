package com.efreightsuite.report;

import lombok.Data;

@Data
public class QuotaionDimensionReport {
//dimensionList

    private String grossWeight;

    private String grossWeightLB;

    private String dimensionPiece;

    private String dimensionLength;

    private String dimensionWidth;

    private String dimensionHeight;

    private String dimensionVolume;

}
