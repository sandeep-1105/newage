package com.efreightsuite.report;

import lombok.Data;

@Data
public class FCRChargesData {

    private String chargeName;
    private String prepaidCharge;
    private String collectCharge;
}
