package com.efreightsuite.report;

import lombok.Data;

@Data
public class AirRates {

    private String chargeName;

    private String chargeCurrency;

    private String chargeDescription;

    private String minCharge;

    private String chargeUnits;

    private String estimatedUnit = "";

    private String totalAmount;

}
