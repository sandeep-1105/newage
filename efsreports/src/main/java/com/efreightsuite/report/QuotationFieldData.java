package com.efreightsuite.report;

import com.efreightsuite.model.Quotation;
import lombok.Data;

@Data
public class QuotationFieldData {

    String referenceNo;
    String refDate;
    String refPlace;
    String salesManName;
    String salesManDesignation;
    String salesManCompany;
    String salesManTel;
    String salesManEmail;
    String companyName;
    String companyAddressLine1;
    String companyAddressLine2;
    String companyLocation;
    Quotation quotation;

    public QuotationFieldData() {

    }

    public QuotationFieldData(Quotation quotation1) {
        this.quotation = quotation1;
    }


}
