package com.efreightsuite.report;

import lombok.Data;

@Data
public class CollectCharges {
    private String sNo;
    private String chargeName;
    private String units;
    private String exRate;
    private String amount;
    private String localAmount;
    private String currency;

}
