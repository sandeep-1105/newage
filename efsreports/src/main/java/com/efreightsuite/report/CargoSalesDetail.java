package com.efreightsuite.report;

import lombok.Data;

@Data
public class CargoSalesDetail {
    private String carrierCode;
    private String carrierName;
    private String mawbNo;
    private String jobDate;
    private String pod;
    private String iataRate;
    private String amount;
    private String fuelCharge;
    private String warRiskSurchage;
    private String awbFee;
    private String commission;
    private String grossWeight;
    private String chargeableWeight;

}
