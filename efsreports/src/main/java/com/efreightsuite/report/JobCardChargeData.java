package com.efreightsuite.report;

import lombok.Data;

@Data
public class JobCardChargeData {
    private String noOfpacks;
    private String grossWeight;
    private String volumeWeight;
    private String chargeableWeight;

}
