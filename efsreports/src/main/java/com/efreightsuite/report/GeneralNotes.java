package com.efreightsuite.report;

import lombok.Data;

@Data
public class GeneralNotes {

    String generalNote;

}
