package com.efreightsuite.report.service;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletResponse;

import com.efreightsuite.dto.AutoAttachmentDto;
import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.ReportDownloadRequestDto;
import com.efreightsuite.enumeration.AddressType;
import com.efreightsuite.enumeration.ReportDownloadOption;
import com.efreightsuite.enumeration.ReportName;
import com.efreightsuite.enumeration.YesNo;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.model.CfsReceiveEntry;
import com.efreightsuite.model.DocumentDetail;
import com.efreightsuite.model.LocationMaster;
import com.efreightsuite.model.LogoMaster;
import com.efreightsuite.model.PartyAddressMaster;
import com.efreightsuite.model.ServiceDocumentDimension;
import com.efreightsuite.model.ShipmentServiceDetail;
import com.efreightsuite.model.UserProfile;
import com.efreightsuite.report.ShippingInstruction;
import com.efreightsuite.service.AuthService;
import com.efreightsuite.util.AppUtil;
import lombok.extern.log4j.Log4j2;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

import org.springframework.stereotype.Service;

@Service
@Log4j2
public class ShippingInstructionReportService extends AbstractReportService {
    public ReportName getReportType() {
        return ReportName.SHIPPING_INSTRUCTION;
    }

    public byte[] processReport(ReportDownloadRequestDto data, HttpServletResponse response, boolean isMutiple) {
        log.info("I am in ShippingInstructionReportService......");
        BaseDto baseDto = new BaseDto();
        UserProfile logInUSer = AuthService.getCurrentUser();
        byte[] a = null;
        JasperPrint jasperPrint = getReport(baseDto, data, AuthService.getCurrentUser());
        AutoAttachmentDto autoAttachmentDto = reportUtil.autoAttachment(ReportName.SHIPPING_INSTRUCTION, YesNo.Yes, data.getResourceId(), "Shipment");
        if (data.getSingle() != null && data.getSingle() == true) {
            if (data.getDownloadOption() == ReportDownloadOption.Preview || data.getDownloadOption() == ReportDownloadOption.Print) {
                reportUtil.japerPrintToResponseOutputStream(data.getDownloadFileType(), jasperPrint, "SHIPPING_INSTRUCTION", response, false, null);
            } else if (autoAttachmentDto != null && autoAttachmentDto.getIsAttach() != null || data.getDownloadOption() == ReportDownloadOption.Download) {
                reportUtil.japerPrintToResponseOutputStream(data.getDownloadFileType(), jasperPrint, "SHIPPING_INSTRUCTION", response, false, autoAttachmentDto);
            }
        }
        if (data.getSingle() != null && data.getSingle() == true && data.getDownloadOption() == ReportDownloadOption.eMail) {
            a = reportUtil.japerPrintToResponseOutputStream(data.getDownloadFileType(), jasperPrint, "SHIPPING_INSTRUCTION", response, false, null);
            shipmentReportMailer.sendShipmentMailToCustomer(data.getResourceId(), logInUSer, ReportName.SHIPPING_INSTRUCTION, a);
        }
        if (isMutiple == true && data.getDownloadOption() == ReportDownloadOption.eMail) {
            a = reportUtil.japerPrintToResponseOutputStream(data.getDownloadFileType(), jasperPrint, "SHIPPING_INSTRUCTION", response, true, null);
            return a;
        }
        return null;
    }

    private JasperPrint getReport(BaseDto baseDto, ReportDownloadRequestDto data, UserProfile loggedInUser) {
        log.info("Shipping Instruction Report with Id::: [" + data.getResourceId() + "] called");

        Map<String, Object> parameters = new HashMap<>();
        try {
            DocumentDetail documentDetail = documentDetailRepository.findById(data.getResourceId());
            if (documentDetail != null) {

                parameters.put("hawbNo", documentDetail != null ? documentDetail.getDocumentNo() : "");
                if (documentDetail.getShipper() != null && documentDetail.getShipper().getPartyAddressList() != null && !documentDetail.getShipper().getPartyAddressList().isEmpty()) {
                    for (PartyAddressMaster partyAddressMaster : documentDetail.getShipper().getPartyAddressList()) {
                        PartyAddressMaster partyAddressMasterObj = partyAddressMaster;
                        if (partyAddressMasterObj.getAddressType().equals(AddressType.Primary)) {
                            parameters.put("shipperFaxNumber", partyAddressMasterObj.getFax());
                            parameters.put("shipperPhoneNumber", partyAddressMasterObj.getPhone());
                        } else if (partyAddressMasterObj.getAddressType().equals(AddressType.Billing)) {
                            parameters.put("shipperFaxNumber", partyAddressMasterObj.getFax());
                            parameters.put("shipperPhoneNumber", partyAddressMasterObj.getPhone());
                        } else if (partyAddressMasterObj.getAddressType().equals(AddressType.Other)) {
                            parameters.put("shipperFaxNumber", partyAddressMasterObj.getFax());
                            parameters.put("shipperPhoneNumber", partyAddressMasterObj.getPhone());
                        }
                    }
                }

                if (documentDetail.getConsignee() != null && documentDetail.getConsignee().getPartyAddressList() != null && !documentDetail.getConsignee().getPartyAddressList().isEmpty()) {
                    for (PartyAddressMaster partyAddressMaster : documentDetail.getConsignee().getPartyAddressList()) {
                        PartyAddressMaster partyAddressMasterObj = partyAddressMaster;
                        if (partyAddressMasterObj.getAddressType().equals(AddressType.Primary)) {
                            parameters.put("consigneeFaxNumber", partyAddressMasterObj.getFax());
                            parameters.put("consigneePhoneNumber", partyAddressMasterObj.getPhone());
                        } else if (partyAddressMasterObj.getAddressType().equals(AddressType.Billing)) {
                            parameters.put("consigneeFaxNumber", partyAddressMasterObj.getFax());
                            parameters.put("consigneePhoneNumber", partyAddressMasterObj.getPhone());
                        } else if (partyAddressMasterObj.getAddressType().equals(AddressType.Other)) {
                            parameters.put("consigneeFaxNumber", partyAddressMasterObj.getFax());
                            parameters.put("consigneePhoneNumber", partyAddressMasterObj.getPhone());
                        }
                    }

                }

                if (documentDetail.getShipper() != null && documentDetail.getShipper().getPartyName() != null) {
                    parameters.put("shipperName", reportUtil.removeSpecialCharacters(documentDetail.getShipper().getPartyName(), data.getDownloadOption()));
                }
                if (documentDetail.getConsignee() != null && documentDetail.getConsignee().getPartyName() != null) {
                    parameters.put("consigneeName", reportUtil.removeSpecialCharacters(documentDetail.getConsignee().getPartyName(), data.getDownloadOption()));
                }


                List<String> shipperAddressList = reportUtil.addressMasterToGenericStringList(documentDetail.getShipperAddress());
                int shipperAddrIndex = 0;
                if (shipperAddressList != null && !shipperAddressList.isEmpty()) {
                    for (String shipAddr : shipperAddressList) {
                        shipperAddrIndex++;
                        if (shipperAddrIndex > 4) {
                            parameters.put("shipperAddressLine4", parameters.get("shipperAddressLine4") + " ");
                        } else {
                            parameters.put("shipperAddressLine" + shipperAddrIndex, shipAddr);
                        }
                    }
                }
                if (shipperAddrIndex < 4) {
                    for (int i = shipperAddrIndex; i < 4; i++) {
                        shipperAddrIndex++;
                        parameters.put("shipperAddressLine" + shipperAddrIndex, "");
                    }
                }
                List<String> consigneeAddressList = reportUtil.addressMasterToGenericStringList(documentDetail.getConsigneeAddress());
                int consigneeAddrIndex = 0;
                if (consigneeAddressList != null && !consigneeAddressList.isEmpty()) {
                    for (String shipAddr : consigneeAddressList) {
                        consigneeAddrIndex++;
                        if (consigneeAddrIndex > 4) {
                            parameters.put("consigneeAddressLine4", parameters.get("consigneeAddressLine4") + " ");
                        } else {
                            parameters.put("consigneeAddressLine" + consigneeAddrIndex, shipAddr);
                        }
                    }
                }
                if (consigneeAddrIndex < 4) {
                    for (int i = consigneeAddrIndex; i < 4; i++) {
                        consigneeAddrIndex++;
                        parameters.put("consigneeAddressLine" + consigneeAddrIndex, "");
                    }

                }


                String cfsReceiptNo = "";

                ShipmentServiceDetail shipmentServiceDetail = shipmentServiceDetailRepository.findByIdForShipmentServiceDetail(documentDetail.getId());
                if (shipmentServiceDetail != null) {

                    if (shipmentServiceDetail.getPod() != null) {
                        parameters.put("podName", shipmentServiceDetail.getPod().getPortName() != null ? shipmentServiceDetail.getPod().getPortName() : "");
                    }
                    List<CfsReceiveEntry> cfs = cfsReceiveEntryRepository.findByShipmentServiceUID(shipmentServiceDetail.getServiceUid());
                    if (cfs != null && cfs.size() > 0 && cfs.get(0) != null) {
                        cfsReceiptNo = cfs.get(0).getCfsReceiptNumber() != null ? cfs.get(0).getCfsReceiptNumber() : "";
                    }

                    List<ShippingInstruction> dimensionList = new ArrayList<>();
                    Double grossTotal = 0.0;
                    Long totalPiecs = 0L;
                    Double totalVolume = 0.0;
                    boolean flag = true;
                    if (shipmentServiceDetail.getDocumentList() != null && !shipmentServiceDetail.getDocumentList().isEmpty()) {
                        for (DocumentDetail document : shipmentServiceDetail.getDocumentList()) {
                            if (document.getDimensionList() != null && !document.getDimensionList().isEmpty()) {
                                for (ServiceDocumentDimension dimesionList : document.getDimensionList()) {
                                    ShippingInstruction shippingInstruction = new ShippingInstruction();
                                    if (flag) {
                                        if (document != null) {
                                            shippingInstruction.setDescription(document.getCommodityDescription() != null ? document.getCommodityDescription() : "");
                                        }
                                    }

                                    if (dimesionList != null) {
                                        shippingInstruction.setHeight(AppUtil.getNumberFormat(dimesionList.getHeight(), shipmentServiceDetail.getCountry().getLocale()));
                                        shippingInstruction.setLength(AppUtil.getNumberFormat(dimesionList.getLength(), shipmentServiceDetail.getCountry().getLocale()));
                                        shippingInstruction.setWidth(AppUtil.getNumberFormat(dimesionList.getWidth(), shipmentServiceDetail.getCountry().getLocale()));
                                        Long piece = Long.valueOf(dimesionList.getNoOfPiece());
                                        shippingInstruction.setPieces(AppUtil.getNumberFormat(dimesionList.getNoOfPiece(), shipmentServiceDetail.getCountry().getLocale()));
                                        shippingInstruction.setCfsReceiptNo(cfsReceiptNo);
                                        totalPiecs = totalPiecs + piece;
                                        Double dimensionGross = 0.0;
                                        dimensionGross = dimesionList.getGrossWeight();
                                        grossTotal += dimensionGross;
                                        Double dimensionVolume = 0.0;
                                        shippingInstruction.setGrossWeight(AppUtil.getWeightFormat(dimensionGross, shipmentServiceDetail.getCountry().getLocale()));
                                        dimensionVolume = dimesionList.getVolWeight();
                                        totalVolume = totalVolume + dimensionVolume;
                                        shippingInstruction.setVolume(AppUtil.getWeightFormat(dimensionVolume, shipmentServiceDetail.getCountry().getLocale()));
                                    }
                                    dimensionList.add(shippingInstruction);
                                    flag = false;
                                }

                            }
                        }
                    }

                    parameters.put("totalGross", AppUtil.getWeightFormat(grossTotal, shipmentServiceDetail.getCountry().getLocale()));
                    parameters.put("totalPieces", AppUtil.getNumberFormat(totalPiecs, shipmentServiceDetail.getCountry().getLocale()));
                    parameters.put("totalVolume", AppUtil.getWeightFormat(totalVolume, shipmentServiceDetail.getCountry().getLocale()));

                    parameters.put("shippingInstructionList", dimensionList);

                }
            }
            String loggedInUserName = "";
            String loggedInLocation = "";
            String companyName = "";
            if (loggedInUser != null) {
                if (loggedInUser.getEmployee() != null && loggedInUser.getEmployee().getEmployeeName() != null) {
                    loggedInUserName = loggedInUser.getEmployee().getEmployeeName();
                } else if (loggedInUser.getUserName() != null) {
                    loggedInUserName = loggedInUser.getUserName();
                } else {
                    loggedInUserName = "";
                }
                if (loggedInUser.getSelectedUserLocation() != null) {

                    parameters.put("refDate", reportUtil.dateToReportDate(new Date(), loggedInUser.getSelectedUserLocation().getJavaDateFormat()));
                    loggedInLocation = loggedInUser.getSelectedUserLocation().getLocationName() != null
                            ? loggedInUser.getSelectedUserLocation().getLocationName() : "";
                    LocationMaster locationMaster = locationMasterRepository.findByLocationCode(loggedInUser.getSelectedUserLocation().getLocationCode());
                    if (locationMaster != null) {
                        parameters.put("companyAddressLine1", locationMaster.getAddressLine1() != null ? locationMaster.getAddressLine1() : "");
                        parameters.put("companyAddressLine2", locationMaster.getAddressLine2() != null ? locationMaster.getAddressLine2() : "");
                        parameters.put("companyAddressLine3", locationMaster.getAddressLine3() != null ? locationMaster.getAddressLine3() : "");
                        companyName = locationMaster.getBranchName() != null ? locationMaster.getBranchName() : "";
                        parameters.put("companyName", reportUtil.removeSpecialCharacters(companyName, data.getDownloadOption()));
                    }
                    LogoMaster logoMaster = logoMasterRepository.findByLocationMaster(loggedInUser.getSelectedUserLocation());
                    try {
                        if (logoMaster != null && logoMaster.getLogo() != null) {
                            InputStream in = new ByteArrayInputStream(logoMaster.getLogo());
                            BufferedImage bImageFromConvert = ImageIO.read(in);
                            parameters.put("logo", bImageFromConvert);
                            log.info("Image getting success::");
                        }
                    } catch (Exception ex) {
                        log.error("Image getting failed:::", ex);
                    }

                } else {
                    loggedInLocation = "";
                }
            }

            parameters.put("refPlace", loggedInUserName + "-" + loggedInLocation);
            parameters.put("reportName", "SHIPPING INSTRUCTION FOR VEHICLES");
            parameters.put("additionalInstruction", "ADDITIONAL INSTRUCTION");
            JasperReport jasperReport = JasperCompileManager.compileReport(appResourcePath + "/efs-report/shipping_instruction.jrxml");
            List<Object> fieldCollection1 = new ArrayList<>();
            Object obj1 = new Object();
            fieldCollection1.add(obj1);
            JRDataSource dataSource = new JRBeanCollectionDataSource(fieldCollection1, false);
            return JasperFillManager.fillReport(jasperReport, parameters, dataSource);


        } catch (RestException re) {
            log.error("RestException in Shipping Instruction : ", re);
        } catch (Exception exception) {
            log.error("RestException in Shipping Instruction:: ", exception);
            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        return null;
    }
}
