package com.efreightsuite.report.service;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletResponse;

import com.efreightsuite.dto.AutoAttachmentDto;
import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.ReportDownloadRequestDto;
import com.efreightsuite.enumeration.Approved;
import com.efreightsuite.enumeration.ReportConfig;
import com.efreightsuite.enumeration.ReportDownloadOption;
import com.efreightsuite.enumeration.ReportName;
import com.efreightsuite.enumeration.YesNo;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.model.CurrencyMaster;
import com.efreightsuite.model.EnquiryLog;
import com.efreightsuite.model.LocationMaster;
import com.efreightsuite.model.LogoMaster;
import com.efreightsuite.model.Quotation;
import com.efreightsuite.model.QuotationCarrier;
import com.efreightsuite.model.QuotationCharge;
import com.efreightsuite.model.QuotationDetail;
import com.efreightsuite.model.QuotationDetailGeneralNote;
import com.efreightsuite.model.QuotationGeneralNote;
import com.efreightsuite.model.ReportConfigurationMaster;
import com.efreightsuite.model.UserProfile;
import com.efreightsuite.report.AirRates;
import com.efreightsuite.report.GeneralNotes;
import com.efreightsuite.report.Notes;
import com.efreightsuite.report.QuotationFieldData;
import com.efreightsuite.service.AuthService;
import com.efreightsuite.util.AppUtil;
import com.efreightsuite.util.SaaSUtil;
import lombok.extern.log4j.Log4j2;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

import org.springframework.stereotype.Service;

@Service
@Log4j2
public class QuotationForPerKGReportService extends AbstractReportService {
    public ReportName getReportType() {
        return ReportName.QUOTATION_FOR_PERKG;
    }

    public byte[] processReport(ReportDownloadRequestDto data, HttpServletResponse response, boolean isMultiple) {
        log.info("I am in QuotationForPerKGReportService..");
        BaseDto baseDto = new BaseDto();
        UserProfile logInUSer = AuthService.getCurrentUser();
        JasperPrint jasperPrint = getReport(baseDto, data, AuthService.getCurrentUser());

        byte[] a = null;
        AutoAttachmentDto autoAttachmentDto = reportUtil.autoAttachment(ReportName.QUOTATION_WITHOUT_ESTIMATION, YesNo.Yes, data.getResourceId(), "Quotation");
        if (data.getSingle() != null && data.getSingle() == true) {
            if (data.getDownloadOption() == ReportDownloadOption.Preview || data.getDownloadOption() == ReportDownloadOption.Print) {
                reportUtil.japerPrintToResponseOutputStream(data.getDownloadFileType(), jasperPrint, "QUOTATION_WITHOUT_ESTIMATION", response, false, null);
            } else if (autoAttachmentDto != null && autoAttachmentDto.getIsAttach() != null || data.getDownloadOption() == ReportDownloadOption.Download) {
                reportUtil.japerPrintToResponseOutputStream(data.getDownloadFileType(), jasperPrint, "QUOTATION_WITHOUT_ESTIMATION", response, false, autoAttachmentDto);
            }
        }
        if (data.getSingle() != null && data.getSingle() == true && data.getDownloadOption() == ReportDownloadOption.eMail) {
            a = reportUtil.japerPrintToResponseOutputStream(data.getDownloadFileType(), jasperPrint, "QUOTATION_FOR_PERKG", response, false, null);
            quotationReportMailer.sendQuotationMailToCustomer(data.getResourceId(), logInUSer, ReportName.QUOTATION_FOR_PERKG, a);
        }

        if (isMultiple == true && data.getDownloadOption() == ReportDownloadOption.eMail) {
            a = reportUtil.japerPrintToResponseOutputStream(data.getDownloadFileType(), jasperPrint, "QUOTATION_WITHOUT_ESTIMATION", response, true, null);
            return a;
        }
        return null;
    }

    private JasperPrint getReport(BaseDto baseDto, ReportDownloadRequestDto data, UserProfile loggedInUser) {
        log.info("I am in QuotationForPerKGReportService..");
        // Report Parameters Data Preparation
        Map<String, Object> parameters = new HashMap<>();
        try {
            Quotation quotation = quotationRepository.findById(data.getResourceId());

            {
                ReportConfigurationMaster tmpObj = reportConfigurationMasterRepository.getByKeyAndLocation(ReportConfig.QUOTATION_WITHOUT_ESTIMATION_REPORT_HEADER, quotation.getLocationMaster().getId());
                if (tmpObj != null && tmpObj.getReportValue() != null) {
                    parameters.put("quotationHeaderNote", tmpObj.getReportValue());
                }
            }
            {
                ReportConfigurationMaster tmpObj = reportConfigurationMasterRepository.getByKeyAndLocation(ReportConfig.QUOTATION_WITHOUT_ESTIMATION_REPORT_FOOTER, quotation.getLocationMaster().getId());
                if (tmpObj != null && tmpObj.getReportValue() != null) {
                    parameters.put("quotationFooterNote", tmpObj.getReportValue());
                }
            }

            String urlPath = applicationBaseUrl + "/public/qutation-approval/customer_approveLink.html?saasId=" + SaaSUtil.getSaaSId().toUpperCase() + "&quotationId=" + quotation.getId();
            log.info("Quotation Approve Link:::" + urlPath);
            parameters.put("quotationApproveLink", urlPath);//for Testing approveLink

            List<QuotationFieldData> quotationFieldCollection = new ArrayList<>();
            QuotationFieldData quotationField = new QuotationFieldData();
            for (QuotationDetail qd : quotation.getQuotationDetailList()) {
                for (QuotationCarrier qc : qd.getQuotationCarrierList()) {
                    qc.setChargeList("");
                    for (QuotationCharge qcc : qc.getQuotationChargeList()) {
                        if (qc.getChargeList() == null || qc.getChargeList().trim().length() == 0) {
                            qc.setChargeList(qcc.getChargeName());
                        } else {
                            qc.setChargeList(qc.getChargeList() + ", " + qcc.getChargeName());
                        }
                    }
                }
            }

            quotationField.setQuotation(quotation);
            BufferedImage image = null;
            try {
                image = ImageIO.read(new File(appResourcePath + "/efs-report/NotApproveWaterMark.png"));
            } catch (IOException e) {
                e.printStackTrace();
            }
            byte[] imageInByte = null;
            try {
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                ImageIO.write(image, "png", baos);
                baos.flush();
                imageInByte = baos.toByteArray();
                baos.close();
            } catch (Exception e) {
                log.info(e);
            }
            InputStream isImage = new ByteArrayInputStream(imageInByte);
            BufferedImage bImage = null;
            try {
                bImage = ImageIO.read(isImage);
            } catch (IOException e) {
                e.printStackTrace();
            }

            parameters.put("waterMark", bImage);


            if (quotation.getApproved().equals(Approved.Approved)) {
                parameters.put("approvedLink", true);
            }


            if (quotation.getApproved().equals(Approved.Pending) || quotation.getApproved().equals(Approved.Rejected)) {
                parameters.put("approved", true);
            } else {
                parameters.put("approved", false);
            }
            // SalesMan Details
            if (quotation.getSalesman() != null) {
                if (quotation.getSalesman().getEmployeeProfessionalDetail() != null) {
                    if (quotation.getSalesman().getEmployeeProfessionalDetail().getDesignationMaster() != null) {
                        quotationField.setSalesManDesignation(quotation.getSalesman().getEmployeeProfessionalDetail().getDesignationMaster().getDesignationName() != null
                                ? quotation.getSalesman().getEmployeeProfessionalDetail().getDesignationMaster().getDesignationName() : "");
                    }
                }
                quotationField.setSalesManEmail(
                        quotation.getSalesman().getEmail() != null ? quotation.getSalesman().getEmail() : "");
                quotationField.setSalesManName(quotation.getSalesman().getEmployeeName() != null
                        ? quotation.getSalesman().getEmployeeName() : "");
                quotationField.setSalesManTel(quotation.getSalesman().getEmployeePhoneNo() != null
                        ? quotation.getSalesman().getEmployeePhoneNo() : "");

                if (quotation.getSalesman().getLocationMaster() != null) {
                    log.info("Date Format ::: " + quotation.getSalesman().getLocationMaster().getJavaDateFormat());
                    // Page Footer Details
                    quotationField.setReferenceNo("");
                }
            } else {
                quotationField.setSalesManDesignation("");
                quotationField.setSalesManEmail("");
                quotationField.setSalesManName("");
                quotationField.setSalesManTel("");
            }
            if (quotation.getCustomer() != null && quotation.getCustomer().getPartyName() != null) {
                log.info("quotation.getCustomer()---------- " + quotation.getCustomer().getFirstName());
                parameters.put("customerName", reportUtil.removeSpecialCharacters(quotation.getCustomer().getPartyName(), data.getDownloadOption()));
            } else {
                parameters.put("customerName", "");
            }
            if (quotation.getCustomerAddress() != null) {
                parameters.put("customerPhone", quotation.getCustomerAddress().getMobile() != null ? quotation.getCustomerAddress().getMobile() : "");
                parameters.put("customerFax", quotation.getCustomerAddress().getPhone() != null ? quotation.getCustomerAddress().getPhone() : "");
            } else {
                parameters.put("customerPhone", "");
                parameters.put("customerFax", "");
            }
            if (quotation.getCustomerAddress() != null) {
                parameters.put("customerAddressLine1", quotation.getCustomerAddress().getAddressLine1() != null
                        ? quotation.getCustomerAddress().getAddressLine1() : "");
                parameters.put("customerAddressLine2", quotation.getCustomerAddress().getAddressLine2() != null
                        ? quotation.getCustomerAddress().getAddressLine2() : "");
                parameters.put("customerAddressLine3", quotation.getCustomerAddress().getAddressLine3() != null
                        ? quotation.getCustomerAddress().getAddressLine3() : "");
                parameters.put("customerAddressLine4", quotation.getCustomerAddress().getAddressLine4() != null
                        ? quotation.getCustomerAddress().getAddressLine4() : "");
            }
            parameters.put("customerAttention", quotation.getAttention());
            parameters.put("quotationNo", quotation.getQuotationNo());

            CurrencyMaster currencyMaster = new CurrencyMaster();
            String loggedInUserName = "";
            String loggedInLocation = "";
            String companyName = "";

            quotationField.setReferenceNo("");
            if (quotation.getLocationMaster() != null) {
                currencyMaster = quotation.getCountryMaster().getCurrencyMaster();
                log.info("Date Format ::: " + quotation.getLocationMaster().getJavaDateFormat());
                // Page Footer Details
                quotationField.setRefDate(reportUtil.dateToReportDate(new Date(), quotation.getLocationMaster().getJavaDateFormat()));
                parameters.put("quotationDate", reportUtil.dateToReportDate(quotation.getSystemTrack().getCreateDate(), quotation.getLocationMaster().getJavaDateFormat()));
                parameters.put("validityFrom", reportUtil.dateToReportDate(quotation.getValidFrom(), quotation.getLocationMaster().getJavaDateFormat()));
                parameters.put("validityTo", reportUtil.dateToReportDate(quotation.getValidTo(), quotation.getLocationMaster().getJavaDateFormat()));
            } else {
                quotationField.setRefDate("");
                parameters.put("quotationDate", "");
                parameters.put("validityFrom", "");
                parameters.put("validityTo", "");
            }

            if (loggedInUser != null) {
                if (loggedInUser.getEmployee() != null && loggedInUser.getEmployee().getEmployeeName() != null) {
                    loggedInUserName = loggedInUser.getEmployee().getEmployeeName();
                } else if (loggedInUser.getUserName() != null) {
                    loggedInUserName = loggedInUser.getUserName();
                } else {
                    loggedInUserName = "";
                }
                //Address from Location Master -> Default Data Master
                if (loggedInUser.getSelectedUserLocation() != null) {
                    LogoMaster logoMaster = logoMasterRepository.findByLocationMaster(loggedInUser.getSelectedUserLocation());
                    if (logoMaster != null && logoMaster.getLogo() != null) {
                        log.info("Logo For Quotation With Estiamtion Report :: " + logoMaster.getId() + " --- Logo : " + logoMaster.getLogo());
                        InputStream in = new ByteArrayInputStream(logoMaster.getLogo());
                        BufferedImage bImageFromConvert = ImageIO.read(in);
                        parameters.put("logo", bImageFromConvert);
                    }
                    loggedInLocation = loggedInUser.getSelectedUserLocation().getLocationName() != null ? loggedInUser.getSelectedUserLocation().getLocationName() : "";
                    LocationMaster locationMaster = locationMasterRepository.findByLocationCode(loggedInUser.getSelectedUserLocation().getLocationCode());
                    if (locationMaster != null) {
                        if (locationMaster.getBranchName() != null) {
                            companyName = locationMaster.getBranchName() != null ? locationMaster.getBranchName() : "";
                            quotationField.setCompanyName(reportUtil.removeSpecialCharacters(companyName, data.getDownloadOption()));
                            quotationField.setSalesManCompany(reportUtil.removeSpecialCharacters(companyName, data.getDownloadOption()));
                            quotationField.setCompanyAddressLine1(locationMaster.getAddressLine1() != null ? locationMaster.getAddressLine1() : "");
                            quotationField.setCompanyAddressLine2(locationMaster.getAddressLine2() != null ? locationMaster.getAddressLine2() : "");
                            quotationField.setCompanyLocation(locationMaster.getAddressLine3() != null ? locationMaster.getAddressLine3() : "");
                        }
                    }
                } else {
                    loggedInLocation = "";
                }
            }
            quotationField.setRefPlace(loggedInUserName + "-" + loggedInLocation);
            quotationFieldCollection.add(quotationField);
            String originName = "";
            String destinationName = "";
            String carrierName = "";
            // Notes Parameter
            List<Notes> noteList = new ArrayList<>();
            if (quotation.getQuotationDetailList() != null && !quotation.getQuotationDetailList().isEmpty()) {
                QuotationDetail qd = quotation.getQuotationDetailList().iterator().next();
                if (qd.getOrigin() != null) {
                    originName = qd.getOrigin().getPortName();
                }
                if (qd.getDestination() != null) {
                    destinationName = qd.getDestination().getPortName();
                    if (qd.getTosCode() != null) {
                        destinationName = destinationName + "(" + qd.getTosCode() + ")";
                    }
                }
                if (currencyMaster != null && currencyMaster.getCurrencyCode() != null) {
                    parameters.put("currencyCode", currencyMaster.getCurrencyCode());
                }
                if (qd != null) {
                    parameters.put("quoteTypeAndKG", "Per KG(" + AppUtil.getCurrencyFormat(qd.getChargebleWeight(), currencyMaster.getCountryMaster().getLocale(), currencyMaster.getDecimalPoint()) + " KG)");
                }

                if (qd.getServiceNoteList() != null && !qd.getServiceNoteList().isEmpty()) {
                    for (QuotationDetailGeneralNote serviceNote : qd.getServiceNoteList()) {
                        Notes note = new Notes();
                        note.setReportNote(serviceNote.getNote() != null ? serviceNote.getNote() : "");
                        noteList.add(note);
                    }
                }


                //for Grouping charges names:;;;;;;;;;;
                List<QuotationCharge> ungroupList = new ArrayList<>();
                List<QuotationCharge> groupList = new ArrayList<>();
                List<AirRates> groupChargeList = new ArrayList<>();
                Double sellRateLocal = 0.0;

                String chargeNameDetails = "";
                for (QuotationCharge qc : ungroupList) {
                    if (qc.getChargeName() != null) {
                        chargeNameDetails = chargeNameDetails + qc.getChargeName();
                        if (qc.getSellRateLocalAmount() != null) {
                            sellRateLocal = sellRateLocal + qc.getSellRateLocalAmount();
                        }
                    }
                }
                parameters.put("chargeDetails", chargeNameDetails);

                Map<String, Double> chargeMap = new HashMap<>();
                Double totalAmount = 0.0;
                for (QuotationCharge group : groupList) {
                    if (chargeMap.containsKey(group.getGroupName())) {
                        totalAmount = chargeMap.get(group.getGroupName()) + group.getSellRateLocalAmount();
                        chargeMap.put(group.getGroupName(), totalAmount);
                    } else {
                        totalAmount = group.getSellRateLocalAmount();
                        chargeMap.put(group.getGroupName(), totalAmount);
                    }
                }

                log.info("totol Group localAmount:::::::" + totalAmount);
                // log.info("Get total LumSum value::::::::::"+qd.getPerKg());
                Double groupTotal = 0.0;
                for (Map.Entry<String, Double> entry : chargeMap.entrySet()) {
                    AirRates rate = new AirRates();
                    rate.setChargeCurrency(currencyMaster.getCurrencyCode());
                    rate.setChargeName(entry.getKey());
                    Double value = entry.getValue() / qd.getChargebleWeight();
                    groupTotal = groupTotal + value;
                    rate.setMinCharge(AppUtil.getCurrencyFormat(value, currencyMaster.getCountryMaster().getLocale(), currencyMaster.getDecimalPoint()));
                    log.info(" Group Name :::::" + entry.getKey() + " Group Total Value:::::" + groupTotal);
                    groupChargeList.add(rate);
                }
                log.info("Group Total Amount:::::::::::" + groupTotal);
                parameters.put("groupChargeList", groupChargeList);

                if (sellRateLocal != null) {
                    parameters.put("chargeAmount", AppUtil.getCurrencyFormat(sellRateLocal / qd.getChargebleWeight(), currencyMaster.getCountryMaster().getLocale(), currencyMaster.getDecimalPoint()));
                }

                Double totalPerKg = sellRateLocal / qd.getChargebleWeight() + groupTotal;

                parameters.put("totalPerKg", AppUtil.getCurrencyFormat(totalPerKg, currencyMaster.getCountryMaster().getLocale(), currencyMaster.getDecimalPoint()));


            }

            parameters.put("originName", originName);
            parameters.put("destinationName", destinationName);
            parameters.put("carrierName", carrierName);
            // General Notes Parameter
            List<GeneralNotes> generalNoteList = new ArrayList<>();
            if (!quotation.getGeneralNoteList().isEmpty()) {
                for (QuotationGeneralNote gnData : quotation.getGeneralNoteList()) {
                    GeneralNotes generalNote = new GeneralNotes();
                    generalNote.setGeneralNote(gnData.getNote());
                    generalNoteList.add(generalNote);

                }
            }
            parameters.put("generalNoteList", generalNoteList);
            parameters.put("noteList", noteList);

            EnquiryLog el = null;
            if (quotation != null) {
                String enquiryNo = quotation.getEnquiryNo();   //Getting enquiryNo from Enquiry
                el = enquiryDetailRepository.findByEnquiryNo(enquiryNo);  //Getting EnquiryLog Object
            }
            if (el != null && el.getEnquiryDetailList() != null && !el.getEnquiryDetailList().isEmpty()) {
                for (int i = 0; i < el.getEnquiryDetailList().size(); i++) {
                    for (int j = i; j < quotation.getQuotationDetailList().size(); j++) {
                        if (el.getEnquiryDetailList().get(i).getCommodityMaster() != null) {
                            quotation.getQuotationDetailList().get(i).setCommodityMaster(el.getEnquiryDetailList().get(i).getCommodityMaster());  //Setting the commodityHsName to Quotation
                            parameters.put("commodityGroupName", quotation.getQuotationDetailList().get(i).getCommodityMaster().getHsName());   //Getting the commodityHsName from Quotation and show the report
                        } else {
                            quotation.getQuotationDetailList().get(i).setCommodityMaster(null);
                            parameters.put("commodityGroupName", "");
                        }

                    }
                }

            }
            JasperReport jasperReport = JasperCompileManager.compileReport(appResourcePath + "/efs-report/quotation_perkg_new_format.jrxml");
            JRDataSource dataSource = new JRBeanCollectionDataSource(quotationFieldCollection, false);
            return JasperFillManager.fillReport(jasperReport, parameters, dataSource);

        } catch (RestException re) {
            log.error("RestException in method while getting the quotation : " + re);
            baseDto.setResponseCode(ErrorCode.QUOTATION_ID_INVALID);
        } catch (Exception e) {
            log.error("Exception in method while getting the quotation : ", e);
            baseDto.setResponseCode(ErrorCode.FAILED);
        }
        log.info("Quotation For PerKG For Resource [" + data.getResourceId() + "] function ended");
        return null;
    }
}
