package com.efreightsuite.report.service;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletResponse;

import com.efreightsuite.dto.AutoAttachmentDto;
import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.ReportDownloadRequestDto;
import com.efreightsuite.enumeration.ReportDownloadOption;
import com.efreightsuite.enumeration.ReportName;
import com.efreightsuite.enumeration.YesNo;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.model.AddressMaster;
import com.efreightsuite.model.DocumentDetail;
import com.efreightsuite.model.LocationMaster;
import com.efreightsuite.model.LogoMaster;
import com.efreightsuite.model.ShipmentCharge;
import com.efreightsuite.model.ShipmentServiceDetail;
import com.efreightsuite.model.UserProfile;
import com.efreightsuite.report.InvoiceDetail;
import com.efreightsuite.service.AuthService;
import com.efreightsuite.util.AppUtil;
import lombok.extern.log4j.Log4j2;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

import org.springframework.stereotype.Service;

@Service
@Log4j2
public class ExportJobCardHAWBReportService extends AbstractReportService {
    public ReportName getReportType() {
        return ReportName.EXPORT_JOB_CARD_HAWB;
    }

    public byte[] processReport(ReportDownloadRequestDto data, HttpServletResponse response, boolean isMultiple) {
        log.info("I am in ExportJobCardHAWBReportService..");
        BaseDto baseDto = new BaseDto();
        UserProfile logInUSer = AuthService.getCurrentUser();
        JasperPrint jasperPrint = getReport(baseDto, data, AuthService.getCurrentUser());

        byte[] a = null;
        AutoAttachmentDto autoAttachmentDto = reportUtil.autoAttachment(ReportName.EXPORT_JOB_CARD_HAWB, YesNo.Yes, data.getResourceId(), "Shipment");
        if (data.getSingle() != null && data.getSingle() == true) {
            if (data.getDownloadOption() == ReportDownloadOption.Preview || data.getDownloadOption() == ReportDownloadOption.Print) {
                reportUtil.japerPrintToResponseOutputStream(data.getDownloadFileType(), jasperPrint, "EXPORT_JOB_CARD_HAWB", response, false, null);
            }
            if (autoAttachmentDto != null && autoAttachmentDto.getIsAttach() != null || data.getDownloadOption() == ReportDownloadOption.Download) {
                reportUtil.japerPrintToResponseOutputStream(data.getDownloadFileType(), jasperPrint, "EXPORT_JOB_CARD_HAWB", response, false, autoAttachmentDto);
            }
        }
        if (data.getSingle() != null && data.getSingle() == true && data.getDownloadOption() == ReportDownloadOption.eMail) {
            a = reportUtil.japerPrintToResponseOutputStream(data.getDownloadFileType(), jasperPrint, "EXPORT_JOB_CARD_HAWB", response, false, null);
            shipmentReportMailer.sendShipmentMailToCustomer(data.getResourceId(), logInUSer, ReportName.EXPORT_JOB_CARD_HAWB, a);
        }
        if (isMultiple == true && data.getDownloadOption() == ReportDownloadOption.eMail) {
            a = reportUtil.japerPrintToResponseOutputStream(data.getDownloadFileType(), jasperPrint, "EXPORT_JOB_CARD_HAWB", response, true, null);
            return a;
        }
        return null;
    }

    private JasperPrint getReport(BaseDto baseDto, ReportDownloadRequestDto data, UserProfile loggedInUser) {
        log.info(" Entering Export Job Card(HAWB)  ReportService.." + data.getResourceId() + "function called");
        DocumentDetail document = documentDetailRepository.findById(data.getResourceId());
        Map<String, Object> parameters = new HashMap<>();
        if (document != null) {
            try {
                ShipmentServiceDetail service = documentDetailRepository.findByIdForShipmentServiceDetail(data.getResourceId());
                if (service != null) {
                    parameters.put("mawbNo", service.getMawbNo() != null ? service.getMawbNo() : "");
                }
                String flightNo = "";
                if (document != null) {
                    parameters.put("hawbNo", document.getDocumentNo() != null ? document.getDocumentNo() : "");
                    if (document.getConsignee() != null && document.getConsignee().getPartyName() != null) {
                        parameters.put("consigneeName", reportUtil.removeSpecialCharacters(document.getConsignee().getPartyName(), data.getDownloadOption()));
                    }
                    if (document.getShipper() != null && document.getShipper().getPartyName() != null) {
                        parameters.put("shipperName", reportUtil.removeSpecialCharacters(document.getShipper().getPartyName(), data.getDownloadOption()));
                    }
                    if (document.getShipperAddress() != null) {
                        parameters.putAll(AddressMasterToMap("shipperAddressLine", document.getShipperAddress()));
                    }
                    if (document.getConsigneeAddress() != null) {
                        parameters.put("consigneeAddressLine1", document.getConsigneeAddress().getAddressLine1() != null ? document.getConsigneeAddress().getAddressLine1() : "");
                        parameters.put("consigneeAddressLine2", document.getConsigneeAddress().getAddressLine2() != null ? document.getConsigneeAddress().getAddressLine2() : "");
                        if (document.getConsigneeAddress().getAddressLine3() != null) {
                            parameters.put("consigneeAddressLine3", document.getConsigneeAddress().getAddressLine3());
                        } else {
                            parameters.put("consigneeAddressLine3", "");
                        }
                        if (document.getConsigneeAddress().getAddressLine4() != null) {
                            parameters.put("consigneeAddressLine4", document.getConsigneeAddress().getAddressLine4());
                        }
                    }
                    parameters.put("noOfPieces", document.getNoOfPieces() != null ? AppUtil.getNumberFormat(document.getNoOfPieces(), service.getCountry().getLocale()) : "");
                    parameters.put("grossWeight", document.getGrossWeight() != null ? AppUtil.getWeightFormat(document.getGrossWeight(), service.getCountry().getLocale()) + " KG " : "");
                    parameters.put("chargeableWeight", document.getChargebleWeight() != null ? AppUtil.getWeightFormat(document.getChargebleWeight(), service.getCountry().getLocale()) + " KG" : "");
                    parameters.put("volume", document.getVolumeWeight() != null ? AppUtil.getWeightFormat(document.getVolumeWeight(), service.getCountry().getLocale()) + " KG" : "");
                    parameters.put("lcNumber", "");

                    flightNo = document.getRouteNo() != null ? document.getRouteNo() : "";

                }
                log.info("Flight No:::[" + flightNo + "] and FlightNo Size:::[" + flightNo.length() + "]");
                parameters.put("commodityName", document.getCommodityDescription() != null ? document.getCommodityDescription() : "");

                if (document.getOrigin() != null) {
                    parameters.put("originName", document.getOrigin().getPortName() != null ? document.getOrigin().getPortName() : "");
                }
                if (document.getDestination() != null) {
                    parameters.put("destination", document.getDestination().getPortName() != null ? document.getDestination().getPortName() : "");
                }
                //Shipment shipment = shipmentRepository.findByShipmentUid(document.getShipmentUid());
                if (service != null) {

                    if (service.getTosMaster() != null) {
                        parameters.put("tosCode", service.getTosMaster().getTosName());
                    }
                    parameters.put("jobNumber", service.getShipmentUid());
                }
                if (loggedInUser != null && loggedInUser.getSelectedUserLocation() != null) {
                    parameters.put("reportDate", reportUtil.dateToReportDate(document.getSystemTrack().getCreateDate(), loggedInUser.getSelectedUserLocation().getJavaDateFormat()));
                    String todayDate = "";
                    todayDate = reportUtil.dateToReportDate(new Date(), loggedInUser.getSelectedUserLocation().getJavaDateFormat());
                    parameters.put("refDate", todayDate);

                    log.info("Address ------------------------------- "
                            + loggedInUser.getSelectedUserLocation().getAddressLine1());
                    String loggedInUserName = "";
                    String loggedInLocation = "";
                    String companyName = "";
                    if (loggedInUser != null) {
                        if (loggedInUser.getEmployee() != null && loggedInUser.getEmployee().getEmployeeName() != null) {
                            loggedInUserName = loggedInUser.getEmployee().getEmployeeName();
                        } else if (loggedInUser.getUserName() != null) {
                            loggedInUserName = loggedInUser.getUserName();
                        } else {
                            loggedInUserName = "";
                        }

                        if (loggedInUser.getSelectedUserLocation() != null) {
                            loggedInLocation = loggedInUser.getSelectedUserLocation().getLocationName() != null
                                    ? loggedInUser.getSelectedUserLocation().getLocationName() : "";

                            LocationMaster locationMaster = locationMasterRepository.findByLocationCode(loggedInUser.getSelectedUserLocation().getLocationCode());
                            if (locationMaster != null) {
                                parameters.put("companyAddressLine1", locationMaster.getAddressLine1() != null ? locationMaster.getAddressLine1() : "");
                                parameters.put("companyAddressLine2", locationMaster.getAddressLine2() != null ? locationMaster.getAddressLine2() : "");
                                parameters.put("companyAddressLine3", locationMaster.getAddressLine3() != null ? locationMaster.getAddressLine3() : "");
                                companyName = locationMaster.getBranchName() != null ? locationMaster.getBranchName() : "";
                                parameters.put("companyName", reportUtil.removeSpecialCharacters(companyName, data.getDownloadOption()));
                            }
                            LogoMaster logoMaster = logoMasterRepository.findByLocationMaster(loggedInUser.getSelectedUserLocation());
                            try {
                                if (logoMaster != null && logoMaster.getLogo() != null) {
                                    InputStream in = new ByteArrayInputStream(logoMaster.getLogo());
                                    BufferedImage bImageFromConvert = ImageIO.read(in);
                                    parameters.put("logo", bImageFromConvert);
                                    log.info("Image getting success::");
                                }
                            } catch (Exception ex) {
                                log.error("Image getting failed:::", ex);
                            }
                        } else {
                            loggedInLocation = "";
                        }
                    }

                    String etd = "";
                    etd = reportUtil.dateToReportDate(document.getEtd(), loggedInUser.getSelectedUserLocation().getJavaDateFormat());
                    if (flightNo.length() > 0 && etd.length() > 0) {
                        parameters.put("flightDate", flightNo + "/" + etd);
                    } else {
                        parameters.put("flightDate", flightNo + " " + etd);
                    }

                    parameters.put("refPlace", loggedInUserName + "-" + loggedInLocation);
                    parameters.put("salesmanName", loggedInUserName);
                    parameters.put("phoneNumber", "");
                    parameters.put("reportName", "EXPORT JOB CARD (HAWB)");

                }
                List<InvoiceDetail> exportjobCardHawbList = new ArrayList<>();

                String rateMerged = appUtil.getLocationConfig("booking.rates.merged", service.getLocation(), true);

                if (rateMerged != null) {
                    if (rateMerged.equals("FALSE") || rateMerged == "FALSE") {
                        if (service.getShipmentChargeList() != null && !service.getShipmentChargeList().isEmpty()) {
                            for (ShipmentCharge shipmentCharge : service.getShipmentChargeList()) {
                                InvoiceDetail invoice = new InvoiceDetail();
                                invoice.setChargeName(shipmentCharge.getChargeName() != null ? shipmentCharge.getChargeName() : "");
                                invoice.setCurrencyCode(shipmentCharge.getNetSaleCurrency().getCurrencyCode() != null ? shipmentCharge.getNetSaleCurrency().getCurrencyCode() : "");

                                if (shipmentCharge.getLocalNetSaleRoe() != null) {
                                    invoice.setExchangeRate(AppUtil.getCurrencyFormat(shipmentCharge.getLocalNetSaleRoe(), shipmentCharge.getNetSaleCurrency().getCountryMaster().getLocale(), shipmentCharge.getNetSaleCurrency().getDecimalPoint()));
                                }
                                if (shipmentCharge.getNetSaleAmount() != null && shipmentCharge.getNetSaleCurrency().getCountryMaster().getLocale() != null) {
                                    invoice.setAmount(AppUtil.getCurrencyFormat(shipmentCharge.getNetSaleAmount(), shipmentCharge.getNetSaleCurrency().getCountryMaster().getLocale(), shipmentCharge.getNetSaleCurrency().getDecimalPoint()));
                                }
                                if (shipmentCharge.getLocalTotalNetSale() != null) {
                                    invoice.setMultiplyLocalAmount(AppUtil.getCurrencyFormat(shipmentCharge.getLocalTotalNetSale(), service.getCountry().getLocale(), service.getLocalCurrency().getDecimalPoint()));
                                }

                                exportjobCardHawbList.add(invoice);

                                if (service.getClientNetRate() != null) {
                                    parameters.put("totalLocalAmount", AppUtil.getCurrencyFormat(service.getClientNetRate(), service.getCountry().getLocale(), service.getLocalCurrency().getDecimalPoint()));
                                }
                            }
                        }
                    } else {
                        if (service.getShipmentChargeList() != null && !service.getShipmentChargeList().isEmpty()) {
                            for (ShipmentCharge shipmentCharge : service.getShipmentChargeList()) {
                                InvoiceDetail invoice = new InvoiceDetail();
                                invoice.setChargeName(shipmentCharge.getChargeName() != null ? shipmentCharge.getChargeName() : "");
                                invoice.setCurrencyCode(shipmentCharge.getRateCurrency().getCurrencyCode() != null ? shipmentCharge.getRateCurrency().getCurrencyCode() : "");

                                if (shipmentCharge.getLocalRateAmountRoe() != null) {
                                    invoice.setExchangeRate(AppUtil.getCurrencyFormat(shipmentCharge.getLocalRateAmountRoe(), shipmentCharge.getRateCurrency().getCountryMaster().getLocale(), shipmentCharge.getRateCurrency().getDecimalPoint()));
                                }
                                if (shipmentCharge.getRateAmount() != null) {
                                    invoice.setAmount(AppUtil.getCurrencyFormat(shipmentCharge.getRateAmount(), shipmentCharge.getRateCurrency().getCountryMaster().getLocale(), shipmentCharge.getRateCurrency().getDecimalPoint()));
                                }
                                if (shipmentCharge.getLocalTotalRateAmount() != null) {
                                    invoice.setMultiplyLocalAmount(AppUtil.getCurrencyFormat(shipmentCharge.getLocalTotalRateAmount(), service.getCountry().getLocale(), service.getLocalCurrency().getDecimalPoint()));
                                }

                                exportjobCardHawbList.add(invoice);

                                if (service.getRate() != null) {
                                    parameters.put("totalLocalAmount", AppUtil.getCurrencyFormat(service.getRate(), service.getCountry().getLocale(), service.getLocalCurrency().getDecimalPoint()));
                                }
                            }
                        }
                    }

                }

                parameters.put("exportjobCardHawbList", exportjobCardHawbList);

                JasperReport jasperReport = JasperCompileManager.compileReport(appResourcePath + "/efs-report/export_job_card_hawb.jrxml");
                List<Object> fieldCollection1 = new ArrayList<>();
                Object obj1 = new Object();
                fieldCollection1.add(obj1);

                JRDataSource dataSource = new JRBeanCollectionDataSource(fieldCollection1, false);
                return JasperFillManager.fillReport(jasperReport, parameters, dataSource);
            } catch (RestException re) {
                log.error("RestException occures in Export Job Card(HAWB) method  : ", re);
            } catch (Exception exception) {
                log.error("RestException occures  in Export Job Card(HAWB)   :: ", exception);
                baseDto.setResponseCode(ErrorCode.FAILED);
            }
            log.info("Export Job Card(HAWB)   [" + data.getResourceId() + "] ended");
        }
        return null;
    }

    public Map<String, String> AddressMasterToMap(String parameterName, AddressMaster addressMaster) {
        Map<String, String> parameter = new HashMap<>();
        int index = 1;
        if (addressMaster != null) {
            if (addressMaster.getAddressLine1() != null && addressMaster.getAddressLine1().trim().length() > 0) {
                parameter.put(parameterName + index, addressMaster.getAddressLine1());
                index++;
            }
            if (addressMaster.getAddressLine2() != null && addressMaster.getAddressLine2().trim().length() > 0) {
                parameter.put(parameterName + index, addressMaster.getAddressLine2());
                index++;
            }
            if (addressMaster.getAddressLine3() != null && addressMaster.getAddressLine3().trim().length() > 0) {
                parameter.put(parameterName + index, addressMaster.getAddressLine3());
                index++;
            }
            if (addressMaster.getAddressLine4() != null && addressMaster.getAddressLine4().trim().length() > 0) {
                parameter.put(parameterName + index, addressMaster.getAddressLine4());
                index++;
            }
        }
        return parameter;
    }
}
