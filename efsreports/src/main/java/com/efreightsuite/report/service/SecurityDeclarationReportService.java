package com.efreightsuite.report.service;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletResponse;

import com.efreightsuite.dto.AutoAttachmentDto;
import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.ReportDownloadRequestDto;
import com.efreightsuite.enumeration.ReportConfig;
import com.efreightsuite.enumeration.ReportDownloadOption;
import com.efreightsuite.enumeration.ReportName;
import com.efreightsuite.enumeration.YesNo;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.model.Consol;
import com.efreightsuite.model.LocationMaster;
import com.efreightsuite.model.LogoMaster;
import com.efreightsuite.model.ReportConfigurationMaster;
import com.efreightsuite.model.UserProfile;
import com.efreightsuite.service.AuthService;
import lombok.extern.log4j.Log4j2;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

import org.springframework.stereotype.Service;

@Service
@Log4j2
public class SecurityDeclarationReportService extends AbstractReportService {
    public ReportName getReportType() {
        return ReportName.SECURITY_DECLARATION;
    }

    public byte[] processReport(ReportDownloadRequestDto data, HttpServletResponse response, boolean isMultiple) {
        log.info("I am in SecurityDeclarationReportService..");
        BaseDto baseDto = new BaseDto();
        UserProfile loggedInUser = AuthService.getCurrentUser();
        JasperPrint jasperPrint = getReport(baseDto, data, AuthService.getCurrentUser());
        byte[] a = null;
        AutoAttachmentDto autoAttachmentDto = reportUtil.autoAttachment(ReportName.SECURITY_DECLARATION, YesNo.Yes, data.getResourceId(), "MasterShipment");
        if (data.getSingle() != null && data.getSingle() == true) {
            if (data.getDownloadOption() == ReportDownloadOption.Preview || data.getDownloadOption() == ReportDownloadOption.Print) {
                reportUtil.japerPrintToResponseOutputStream(data.getDownloadFileType(), jasperPrint, "SECURITY_DECLARATION", response, false, null);
            } else if (autoAttachmentDto != null && autoAttachmentDto.getIsAttach() != null || data.getDownloadOption() == ReportDownloadOption.Download) {
                a = reportUtil.japerPrintToResponseOutputStream(data.getDownloadFileType(), jasperPrint, "SECURITY_DECLARATION", response, false, autoAttachmentDto);
            }
        }
        if (data.getSingle() != null && data.getSingle() == true && data.getDownloadOption() == ReportDownloadOption.eMail) {
            a = reportUtil.japerPrintToResponseOutputStream(data.getDownloadFileType(), jasperPrint, "SECURITY_DECLARATION", response, false, null);
            consolReportMailer.sendConsolMailToCustomer(data.getResourceId(), loggedInUser, ReportName.SECURITY_DECLARATION, a);
        }
        if (isMultiple == true && data.getDownloadOption() == ReportDownloadOption.eMail) {
            a = reportUtil.japerPrintToResponseOutputStream(data.getDownloadFileType(), jasperPrint, "SECURITY_DECLARATION", response, true, null);
            return a;
        }
        return null;
    }

    private JasperPrint getReport(BaseDto baseDto, ReportDownloadRequestDto data, UserProfile loggedInUser) {
        log.info("Security Declaration  Report with Id::: [" + data.getResourceId() + "] called");
        Consol consol = consolRepository.findById(data.getResourceId());
        if (consol != null) {
            Map<String, Object> parameters = new HashMap<>();
            try {
                if (consol.getConsolDocument() != null) {
                    parameters.put("airwayBillNo", consol.getConsolDocument().getMawbNo() != null ? consol.getConsolDocument().getMawbNo() : "");
                }
                if (consol.getPod() != null) {
                    parameters.put("destination", consol.getPod().getPortName() != null ? consol.getPod().getPortName() : "");
                }

                {
                    ReportConfigurationMaster tmpObj = reportConfigurationMasterRepository.getByKeyAndLocation(ReportConfig.SECURITY_DECLARION_HEADER_ONE, consol.getLocation().getId());
                    if (tmpObj != null && tmpObj.getReportValue() != null) {
                        parameters.put("securityDeclarionHeaderOne", tmpObj.getReportValue());
                    }
                }
                {
                    ReportConfigurationMaster tmpObj = reportConfigurationMasterRepository.getByKeyAndLocation(ReportConfig.SECURITY_DECLARION_HEADER_TWO, consol.getLocation().getId());
                    if (tmpObj != null && tmpObj.getReportValue() != null) {
                        parameters.put("securityDeclarionHeaderTwo", tmpObj.getReportValue());
                    }
                }
                {
                    ReportConfigurationMaster tmpObj = reportConfigurationMasterRepository.getByKeyAndLocation(ReportConfig.SECURITY_DECLARION_HEADER_THREE, consol.getLocation().getId());
                    if (tmpObj != null && tmpObj.getReportValue() != null) {
                        parameters.put("securityDeclarionHeaderThree", tmpObj.getReportValue());
                    }
                }
                {
                    ReportConfigurationMaster tmpObj = reportConfigurationMasterRepository.getByKeyAndLocation(ReportConfig.SECURITY_DECLARION_TERM_ONE, consol.getLocation().getId());
                    if (tmpObj != null && tmpObj.getReportValue() != null) {
                        parameters.put("securityDeclarionTermOne", tmpObj.getReportValue());
                    }
                }
                {
                    ReportConfigurationMaster tmpObj = reportConfigurationMasterRepository.getByKeyAndLocation(ReportConfig.SECURITY_DECLARION_TERM_TWO, consol.getLocation().getId());
                    if (tmpObj != null && tmpObj.getReportValue() != null) {
                        parameters.put("securityDeclarionTermTwo", tmpObj.getReportValue());
                    }
                }
                {
                    ReportConfigurationMaster tmpObj = reportConfigurationMasterRepository.getByKeyAndLocation(ReportConfig.SECURITY_DECLARION_TERM_THREE, consol.getLocation().getId());
                    if (tmpObj != null && tmpObj.getReportValue() != null) {
                        parameters.put("securityDeclarionTermThree", tmpObj.getReportValue());
                    }
                }


                String loggedInUserName = "";
                String loggedInLocation = "";
                String companyName = "";
                if (loggedInUser != null) {
                    if (loggedInUser.getEmployee() != null && loggedInUser.getEmployee().getEmployeeName() != null) {
                        loggedInUserName = loggedInUser.getEmployee().getEmployeeName();
                    } else if (loggedInUser.getUserName() != null) {
                        loggedInUserName = loggedInUser.getUserName();
                    } else {
                        loggedInUserName = "";
                    }
                    if (loggedInUser.getSelectedUserLocation() != null) {
                        String todayDate = "";
                        todayDate = reportUtil.dateToReportDate(new Date(), loggedInUser.getSelectedUserLocation().getJavaDateFormat());
                        parameters.put("refDate", todayDate);
                        parameters.put("toDayDate", todayDate);

                        loggedInLocation = loggedInUser.getSelectedUserLocation().getLocationName() != null
                                ? loggedInUser.getSelectedUserLocation().getLocationName() : "";

                        if (loggedInUser.getSelectedUserLocation() != null) {
                            LocationMaster locationMaster = locationMasterRepository.findById(loggedInUser.getSelectedUserLocation().getId());
                            parameters.put("companyAddressLine1", locationMaster.getAddressLine1() != null ? locationMaster.getAddressLine1() : "");
                            parameters.put("companyAddressLine2", locationMaster.getAddressLine2() != null ? locationMaster.getAddressLine2() : "");
                            parameters.put("companyAddressLine3", locationMaster.getAddressLine3() != null ? locationMaster.getAddressLine3() : "");
                            companyName = locationMaster.getBranchName() != null ? locationMaster.getBranchName() : "";
                        }
                        LogoMaster logoMaster = logoMasterRepository.findByLocationMaster(loggedInUser.getSelectedUserLocation());
                        try {
                            if (logoMaster != null && logoMaster.getLogo() != null) {
                                InputStream in = new ByteArrayInputStream(logoMaster.getLogo());
                                BufferedImage bImageFromConvert = ImageIO.read(in);
                                parameters.put("logo", bImageFromConvert);
                                log.info("Image getting success::");
                            }
                        } catch (Exception ex) {
                            log.error("Image getting failed:::", ex);
                        }

                    } else {
                        loggedInLocation = "";
                    }
                }
                parameters.put("companyName", reportUtil.removeSpecialCharacters(companyName, data.getDownloadOption()));
                parameters.put("refPlace", loggedInUserName + "-" + loggedInLocation);
                parameters.put("signatureName", loggedInUserName);
                parameters.put("reportName", "SECURITY DECLARATION");
                JasperReport jasperReport = JasperCompileManager.compileReport(appResourcePath + "/efs-report/security_declaration.jrxml");
                List<Object> fieldCollection1 = new ArrayList<>();
                Object obj1 = new Object();
                fieldCollection1.add(obj1);

                JRDataSource dataSource = new JRBeanCollectionDataSource(fieldCollection1, false);
                return JasperFillManager.fillReport(jasperReport, parameters, dataSource);

            } catch (RestException re) {
                log.error("RestException in Security Declaration : ", re);
            } catch (Exception exception) {
                log.error("RestException in Shipping InstructionSecurity Declaration :: ", exception);
                baseDto.setResponseCode(ErrorCode.FAILED);
            }
            log.info("Security Declaration  [" + data.getResourceId() + "] function ended");
        }
        return null;
    }
}
