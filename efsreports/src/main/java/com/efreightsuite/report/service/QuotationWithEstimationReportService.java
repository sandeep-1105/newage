package com.efreightsuite.report.service;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletResponse;

import com.efreightsuite.dto.AutoAttachmentDto;
import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.CurrencySearchResponse;
import com.efreightsuite.dto.ReportCharge;
import com.efreightsuite.dto.ReportCurrency;
import com.efreightsuite.dto.ReportDownloadRequestDto;
import com.efreightsuite.dto.SearchRespDto;
import com.efreightsuite.enumeration.Approved;
import com.efreightsuite.enumeration.ChargeType;
import com.efreightsuite.enumeration.DimensionUnit;
import com.efreightsuite.enumeration.QuoteType;
import com.efreightsuite.enumeration.ReportConfig;
import com.efreightsuite.enumeration.ReportDownloadOption;
import com.efreightsuite.enumeration.ReportName;
import com.efreightsuite.enumeration.YesNo;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.model.CurrencyMaster;
import com.efreightsuite.model.EnquiryLog;
import com.efreightsuite.model.LocationMaster;
import com.efreightsuite.model.LogoMaster;
import com.efreightsuite.model.Quotation;
import com.efreightsuite.model.QuotationCarrier;
import com.efreightsuite.model.QuotationCharge;
import com.efreightsuite.model.QuotationDetail;
import com.efreightsuite.model.QuotationDetailGeneralNote;
import com.efreightsuite.model.QuotationDimension;
import com.efreightsuite.model.QuotationGeneralNote;
import com.efreightsuite.model.ReportConfigurationMaster;
import com.efreightsuite.model.UserProfile;
import com.efreightsuite.report.GeneralNotes;
import com.efreightsuite.report.LabelValue;
import com.efreightsuite.report.Notes;
import com.efreightsuite.report.QuotaionDimensionReport;
import com.efreightsuite.report.QuotationFieldData;
import com.efreightsuite.service.AuthService;
import com.efreightsuite.util.AppUtil;
import com.efreightsuite.util.SaaSUtil;
import lombok.extern.log4j.Log4j2;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

import org.springframework.stereotype.Service;

@Service
@Log4j2
public class QuotationWithEstimationReportService extends AbstractReportService {
    @Override
    public ReportName getReportType() {
        return ReportName.QUOTATION_WITH_ESTIMATION;
    }

    public byte[] processReport(ReportDownloadRequestDto data, HttpServletResponse response, boolean isMultiple) {
        log.info("I am in QuotationWithEstimationReportService..");
        BaseDto baseDto = new BaseDto();
        UserProfile logInUSer = AuthService.getCurrentUser();
        JasperPrint jasperPrint = getReport(baseDto, data, AuthService.getCurrentUser());
        byte[] a = null;
        AutoAttachmentDto autoAttachmentDto = reportUtil.autoAttachment(ReportName.QUOTATION_WITH_ESTIMATION, YesNo.Yes, data.getResourceId(), "Quotation");
        if (data.getSingle() != null && data.getSingle() == true) {
            if (data.getDownloadOption() == ReportDownloadOption.Preview || data.getDownloadOption() == ReportDownloadOption.Print) {
                reportUtil.japerPrintToResponseOutputStream(data.getDownloadFileType(), jasperPrint, "QUOTATION_WITH_ESTIMATION", response, false, null);
            } else if (autoAttachmentDto != null && autoAttachmentDto.getIsAttach() != null || data.getDownloadOption() == ReportDownloadOption.Download) {
                reportUtil.japerPrintToResponseOutputStream(data.getDownloadFileType(), jasperPrint, "QUOTATION_WITH_ESTIMATION", response, false, autoAttachmentDto);
            }
        }
        if (data.getSingle() != null && data.getSingle() == true && data.getDownloadOption() == ReportDownloadOption.eMail) {
            a = reportUtil.japerPrintToResponseOutputStream(data.getDownloadFileType(), jasperPrint, "QUOTATION_WITH_ESTIMATION", response, true, null);
            quotationReportMailer.sendQuotationMailToCustomer(data.getResourceId(), logInUSer, ReportName.QUOTATION_WITH_ESTIMATION, a);
        }
        if (isMultiple == true && data.getDownloadOption() == ReportDownloadOption.eMail) {
            a = reportUtil.japerPrintToResponseOutputStream(data.getDownloadFileType(), jasperPrint, "QUOTATION_WITH_ESTIMATION", response, true, null);
            return a;
        }
        return null;
    }


    private JasperPrint getReport(BaseDto baseDto, ReportDownloadRequestDto data, UserProfile loggedInUser) {
        log.info("I am in QuotationWithEstimationReportService..");
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("dimensionType", "WEIGHT");
        try {
            String companyName = "";
            Quotation quotation = quotationRepository.findById(data.getResourceId());
            {
                ReportConfigurationMaster tmpObj = reportConfigurationMasterRepository.getByKeyAndLocation(ReportConfig.QUOTATION_WITHOUT_ESTIMATION_REPORT_HEADER, quotation.getLocationMaster().getId());
                if (tmpObj != null && tmpObj.getReportValue() != null) {
                    parameters.put("quotationHeaderNote", tmpObj.getReportValue());
                }
            }
            {
                ReportConfigurationMaster tmpObj = reportConfigurationMasterRepository.getByKeyAndLocation(ReportConfig.QUOTATION_WITHOUT_ESTIMATION_REPORT_FOOTER, quotation.getLocationMaster().getId());
                if (tmpObj != null && tmpObj.getReportValue() != null) {
                    parameters.put("quotationFooterNote", tmpObj.getReportValue());
                }
            }
            String urlPath = applicationBaseUrl + "/public/qutation-approval/customer_approveLink.html?saasId=" + SaaSUtil.getSaaSId().toUpperCase() + "&quotationId=" + quotation.getId();
            log.info("Quotation Approve Link:::" + urlPath);
            parameters.put("quotationApproveLink", urlPath);//for Testing approveLink
            if (quotation.getApproved().equals(Approved.Approved)) {
                parameters.put("approvedLink", true);
            }

            BufferedImage image = null;
            try {
                image = ImageIO.read(new File(appResourcePath + "/efs-report/NotApproveWaterMark.png"));
            } catch (IOException e) {
                e.printStackTrace();
            }
            byte[] imageInByte = null;
            try {
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                ImageIO.write(image, "png", baos);
                baos.flush();
                imageInByte = baos.toByteArray();
                baos.close();
            } catch (Exception e) {
                log.info(e);
            }
            InputStream isImage = new ByteArrayInputStream(imageInByte);
            BufferedImage bImage = null;
            try {
                bImage = ImageIO.read(isImage);
            } catch (IOException e) {
                e.printStackTrace();
            }

            parameters.put("waterMark", bImage);


            List<QuotationFieldData> quotationFieldCollection = new ArrayList<>();
            QuotationFieldData quotationField = new QuotationFieldData();
            for (QuotationDetail qd : quotation.getQuotationDetailList()) {
                for (QuotationCarrier qc : qd.getQuotationCarrierList()) {
                    Map<String, List<QuotationCharge>> mapChargeList = new HashMap<>(); //carrierName and corresponding charges
                    Map<String, Map<Double, Double>> amountMap = new HashMap<>();
                    qc.setChargeList("");
                    List<String> groupNameList = new ArrayList<>();
                    for (QuotationCharge qcc : qc.getQuotationChargeList()) {
                        if (qc.getChargeList() == null || qc.getChargeList().trim().length() == 0) {
                            qc.setChargeList(qcc.getChargeName());
                        } else {
                            qc.setChargeList(qc.getChargeList() + ", " + qcc.getChargeName());
                        }
                        if (quotation.getQuoteType() != QuoteType.GENERAL && qcc.getIsGroup() != null && qcc.getIsGroup() == YesNo.Yes && qcc.getGroupName() != null && qcc.getGroupName().trim().length() != 0) {

                            if (mapChargeList.get(qcc.getGroupName()) == null) {
                                List<QuotationCharge> tmpList = new ArrayList<>();
                                tmpList.add(qcc);
                                mapChargeList.put(qcc.getGroupName(), tmpList);
                            } else {
                                mapChargeList.get(qcc.getGroupName()).add(qcc);
                            }

                            boolean flag = true;
                            for (String st : groupNameList) {
                                if (st.trim().toUpperCase().equals(qcc.getGroupName().toUpperCase())) {
                                    flag = false;
                                    break;
                                }
                            }

                            if (flag) {
                                groupNameList.add(qcc.getGroupName());
                            }

                        } else {
                            if (mapChargeList.get(qcc.getChargeMaster().getChargeType().toString()) == null) {
                                List<QuotationCharge> tmpList = new ArrayList<>();
                                tmpList.add(qcc);

                                mapChargeList.put(qcc.getChargeMaster().getChargeType().toString(), tmpList);
                            } else {
                                mapChargeList.get(qcc.getChargeMaster().getChargeType().toString()).add(qcc);
                            }
                        }

                        String currencyCode = qcc.getCurrencyMaster().getCurrencyCode();

                        if (amountMap.get(currencyCode) == null) {
                            amountMap.put(currencyCode, new HashMap<>());
                        }

                        Double roe = qcc.getRoe();

                        if (amountMap.get(currencyCode).get(roe) == null) {
                            amountMap.get(currencyCode).put(roe, 0.0);
                        }
                        amountMap.get(currencyCode).put(roe, (amountMap.get(currencyCode).get(roe) + qcc.getSellRateAmount()));
                    }
                    List<ReportCurrency> currencyList = new ArrayList<>();
                    for (String key : amountMap.keySet()) {
                        for (Double roe : amountMap.get(key).keySet()) {
                            ReportCharge rc = new ReportCharge();
                            ReportCurrency rcc = new ReportCurrency();
                            rcc.setCurrencyCode(key);
                            rcc.setRoe(roe);
                            rcc.setLocalAmount(AppUtil.getCurrencyFormat(amountMap.get(key).get(roe), quotation.getLocalCurrency().getCountryMaster().getLocale(), quotation.getLocalCurrency().getDecimalPoint()));
                            currencyList.add(rcc);
                            rc.setCurrencyList(currencyList);
                            qc.getCurrencyList().add(new ReportCurrency(key, roe, rcc.getLocalAmount()));

                        }
                    }

                    List<QuotationCharge> originList = new ArrayList<>();
                    List<QuotationCharge> otherList = new ArrayList<>();
                    List<QuotationCharge> freightList = new ArrayList<>();
                    List<QuotationCharge> destinationList = new ArrayList<>();

                    for (String key : mapChargeList.keySet()) {
                        if (key == "Origin") {
                            originList.addAll(mapChargeList.get(key));
                        } else if (key == "Freight") {
                            freightList.addAll(mapChargeList.get(key));
                        } else if (key == "Other") {
                            otherList.addAll(mapChargeList.get(key));
                        } else if (key == "Destination") {
                            destinationList.addAll(mapChargeList.get(key));
                        }
                    }

                    for (String st : groupNameList) {
                        for (String key : mapChargeList.keySet()) {

                            if (st.trim().toUpperCase().equals(key.trim().toUpperCase())) {
                                if (key == "Origin" || key == "Freight" || key == "Other" || key == "Destination") {
                                    continue;
                                }
                                ReportCharge dto = new ReportCharge();
                                dto.setKeyName(key);
                                dto.setChargeList(mapChargeList.get(key));
                                qc.getMapChargeList().add(dto);
                                break;
                            }

                        }
                    }

                    if (originList.size() > 0 && originList != null) {
                        ReportCharge dto = new ReportCharge();
                        dto.setKeyName(ChargeType.Origin.name());
                        dto.setChargeList(originList);
                        qc.getMapChargeList().add(dto);
                    }
                    if (freightList.size() > 0 && freightList != null) {
                        ReportCharge dto = new ReportCharge();
                        dto.setKeyName(ChargeType.Freight.name());
                        dto.setChargeList(freightList);
                        qc.getMapChargeList().add(dto);
                    }
                    if (otherList.size() > 0 && otherList != null) {
                        ReportCharge dto = new ReportCharge();
                        dto.setKeyName(ChargeType.Other.name());
                        dto.setChargeList(otherList);
                        qc.getMapChargeList().add(dto);
                    }
                    if (destinationList.size() > 0 && destinationList != null) {
                        ReportCharge dto = new ReportCharge();
                        dto.setKeyName(ChargeType.Destination.name());
                        dto.setChargeList(destinationList);
                        qc.getMapChargeList().add(dto);
                    }

                }
            }
            quotationField.setQuotation(quotation);

            if (quotation.getApproved().equals(Approved.Pending) || quotation.getApproved().equals(Approved.Rejected)) {
                parameters.put("approved", true);
            } else {
                parameters.put("approved", false);
            }
            // SalesMan Details

            if (quotation.getSalesman() != null) {
                if (quotation.getSalesman().getEmployeeProfessionalDetail() != null) {
                    if (quotation.getSalesman().getEmployeeProfessionalDetail().getDesignationMaster() != null) {
                        quotationField.setSalesManDesignation(quotation.getSalesman().getEmployeeProfessionalDetail().getDesignationMaster().getDesignationName() != null
                                ? quotation.getSalesman().getEmployeeProfessionalDetail().getDesignationMaster().getDesignationName() : "");
                    }
                }

                quotationField.setSalesManEmail(
                        quotation.getSalesman().getEmail() != null ? quotation.getSalesman().getEmail() : "");
                quotationField.setSalesManName(quotation.getSalesman().getEmployeeName() != null
                        ? quotation.getSalesman().getEmployeeName() : "");
                quotationField.setSalesManTel(quotation.getSalesman().getEmployeePhoneNo() != null
                        ? quotation.getSalesman().getEmployeePhoneNo() : "");

            } else {
                quotationField.setSalesManDesignation("");
                quotationField.setSalesManEmail("");
                quotationField.setSalesManName("");
                quotationField.setSalesManTel("");
            }
            if (quotation.getCustomer() != null && quotation.getCustomer().getPartyName() != null) {
                log.info("quotation.getCustomer()---------- " + quotation.getCustomer().getFirstName());

                parameters.put("customerName", reportUtil.removeSpecialCharacters(quotation.getCustomer().getPartyName(), data.getDownloadOption()));
            } else {
                parameters.put("customerName", "");
            }
            if (quotation.getCustomerAddress() != null) {
                parameters.put("customerPhone", quotation.getCustomerAddress().getMobile() != null ? quotation.getCustomerAddress().getMobile() : "");
                parameters.put("customerFax", quotation.getCustomerAddress().getPhone() != null ? quotation.getCustomerAddress().getPhone() : "");
            } else {
                parameters.put("customerPhone", "");
                parameters.put("customerFax", "");
            }
            if (quotation.getCustomerAddress() != null) {
                parameters.put("customerAddressLine1", quotation.getCustomerAddress().getAddressLine1() != null
                        ? quotation.getCustomerAddress().getAddressLine1() : "");
                parameters.put("customerAddressLine2", quotation.getCustomerAddress().getAddressLine2() != null
                        ? quotation.getCustomerAddress().getAddressLine2() : "");
                parameters.put("customerAddressLine3", quotation.getCustomerAddress().getAddressLine3() != null
                        ? quotation.getCustomerAddress().getAddressLine3() : "");
                parameters.put("customerAddressLine4", quotation.getCustomerAddress().getAddressLine4() != null
                        ? quotation.getCustomerAddress().getAddressLine4() : "");
            }
            parameters.put("customerAttention", quotation.getAttention());
            parameters.put("quotationNo", quotation.getQuotationNo());

            CurrencyMaster currencyMaster = new CurrencyMaster();
            String loggedInUserName = "";
            String loggedInLocation = "";
            if (quotation != null) {
                currencyMaster = quotation.getLocalCurrency();
                log.info("Date Format ::: " + quotation.getLocationMaster().getJavaDateFormat());

                // Page Footer Details
                quotationField.setRefDate(reportUtil.dateToReportDate(new Date(), quotation.getLocationMaster().getJavaDateFormat()));
                quotationField.setReferenceNo("");
                parameters.put("quotationDate", reportUtil.dateToReportDate(quotation.getSystemTrack().getCreateDate(), quotation.getLocationMaster().getJavaDateFormat()));
                parameters.put("validityFrom", reportUtil.dateToReportDate(quotation.getValidFrom(), quotation.getLocationMaster().getJavaDateFormat()));
                parameters.put("validityTo", reportUtil.dateToReportDate(quotation.getValidTo(), quotation.getLocationMaster().getJavaDateFormat()));
            } else {
                quotationField.setRefDate("");
                parameters.put("quotationDate", "");
                parameters.put("validityFrom", "");
                parameters.put("validityTo", "");
            }

            if (loggedInUser != null) {
                if (loggedInUser.getEmployee() != null && loggedInUser.getEmployee().getEmployeeName() != null) {
                    loggedInUserName = loggedInUser.getEmployee().getEmployeeName();
                } else if (loggedInUser.getUserName() != null) {
                    loggedInUserName = loggedInUser.getUserName();
                } else {
                    loggedInUserName = "";
                }

                if (loggedInUser.getSelectedUserLocation() != null) {
                    LogoMaster logoMaster = logoMasterRepository.findByLocationMaster(loggedInUser.getSelectedUserLocation());
                    if (logoMaster != null && logoMaster.getLogo() != null) {
                        log.info("Logo For Quotation With Estiamtion Report :: " + logoMaster.getId() + " --- Logo : " + logoMaster.getLogo());
                        InputStream in = new ByteArrayInputStream(logoMaster.getLogo());
                        BufferedImage bImageFromConvert = ImageIO.read(in);
                        parameters.put("logo", bImageFromConvert);
                    }

                    loggedInLocation = loggedInUser.getSelectedUserLocation().getLocationName() != null ? loggedInUser.getSelectedUserLocation().getLocationName() : "";
                    LocationMaster locationMaster = locationMasterRepository.findByLocationCode(loggedInUser.getSelectedUserLocation().getLocationCode());
                    if (locationMaster != null) {
                        if (locationMaster.getBranchName() != null) {
                            companyName = locationMaster.getBranchName() != null ? locationMaster.getBranchName() : "";
                            quotationField.setCompanyName(reportUtil.removeSpecialCharacters(companyName, data.getDownloadOption()));
                            quotationField.setSalesManCompany(reportUtil.removeSpecialCharacters(companyName, data.getDownloadOption()));
                            quotationField.setCompanyAddressLine1(locationMaster.getAddressLine1() != null ? locationMaster.getAddressLine1() : "");
                            quotationField.setCompanyAddressLine2(locationMaster.getAddressLine2() != null ? locationMaster.getAddressLine2() : "");
                            quotationField.setCompanyLocation(locationMaster.getAddressLine3() != null ? locationMaster.getAddressLine3() : "");
                        }
                    }

                } else {
                    loggedInLocation = "";
                }

            }
            quotationField.setRefPlace(loggedInUserName + "-" + loggedInLocation);
            quotationFieldCollection.add(quotationField);
            // Dimension Calculation
            List<QuotaionDimensionReport> dimensionList = new ArrayList<>();
            // Carrier Charges
            String originName = "";
            String destinationName = "";
            String carrierName = "";
            // Notes Parameter
            List<Notes> noteList = new ArrayList<>();
            String exchangeRates = " Ex-Rate :";
            if (quotation.getQuotationDetailList() != null && !quotation.getQuotationDetailList().isEmpty()) {
                QuotationDetail qd = quotation.getQuotationDetailList().iterator().next();
                if (qd.getOrigin() != null) {
                    originName = qd.getOrigin().getPortName();
                }
                if (qd.getDestination() != null) {
                    destinationName = qd.getDestination().getPortName();
                    if (qd.getTosCode() != null) {
                        destinationName = destinationName + "(" + qd.getTosCode() + ")";
                    }
                }
                parameters.put("chargeableWeight", AppUtil.getWeightFormat(qd.getChargebleWeight(), quotation.getCountryMaster().getLocale()));
                parameters.put("grossWeight", AppUtil.getWeightFormat(qd.getGrossWeight(), quotation.getCountryMaster().getLocale()));
                parameters.put("volumeWeight", AppUtil.getWeightFormat(qd.getVolumeWeight(), quotation.getCountryMaster().getLocale()));

                // Other Charges
                Map<String, CurrencySearchResponse> currentCurrencyRateValueMap = null;
                if (currencyMaster != null && currencyMaster.getCurrencyCode() != null) {
                    //lv.setLabel("Net Amount in "+currencyMaster.getCurrencyCode());
                    SearchRespDto currrencyDetailList = currencyRateSearchImpl.searchByIdResponseWithCode(currencyMaster.getId());
                    if (currrencyDetailList != null && currrencyDetailList.getSearchResult() != null) {
                        try {
                            currentCurrencyRateValueMap = (Map<String, CurrencySearchResponse>) currrencyDetailList.getSearchResult();
                        } catch (Exception ex) {
                            log.error("Exception on Type casting Currency Reponse Map : ", ex);
                            ex.printStackTrace();
                        }
                    }
                }

				/*if(currentCurrencyRateValueMap != null) {
					Set<String> mapSet = currentCurrencyRateValueMap.keySet();
					for(String ms : mapSet) {
						log.info("Key :: "+ms +"; Sell Rate :: "+currentCurrencyRateValueMap.get(ms).getCSell()  );
					}
				}*/


                DecimalFormat decimalFormat = new DecimalFormat("#0.00");

                Map<String, Double> currencyTotalMap = new HashMap<>();

                //Start for GroupList  Data
                if (qd.getQuotationCarrierList() != null && !qd.getQuotationCarrierList().isEmpty()) {
                    for (QuotationCarrier qcc : qd.getQuotationCarrierList()) {
                        for (QuotationCharge qc : qcc.getQuotationChargeList()) {
                            if (qc.getSellRateLocalAmount() != null) {
                                if (qc.getSellRateLocalAmount() > 0.0 && currentCurrencyRateValueMap != null && currentCurrencyRateValueMap.get(qc.getCurrencyMaster().getCurrencyCode()) != null) {
                                    if (currencyTotalMap.get(qc.getCurrencyMaster().getCurrencyCode()) != null) {
                                        currencyTotalMap.put(qc.getCurrencyMaster().getCurrencyCode(), currencyTotalMap.get(qc.getCurrencyMaster().getCurrencyCode()) + qc.getSellRateLocalAmount());
                                    } else {
                                        currencyTotalMap.put(qc.getCurrencyMaster().getCurrencyCode(), qc.getSellRateLocalAmount());
                                    }
                                } else if (qc.getSellRateLocalAmount() > 0.0 && qc.getCurrencyMaster().getCurrencyCode() != null) {
                                    log.info("I am In ELSE IF CURRENCY CODE " + qc.getCurrencyMaster().getCurrencyCode());
                                    if (currencyTotalMap.get(qc.getCurrencyMaster().getCurrencyCode()) != null) {
                                        log.info("I am In IF CURRENCY CODE ");
                                        currencyTotalMap.put(qc.getCurrencyMaster().getCurrencyCode(), currencyTotalMap.get(qc.getCurrencyMaster().getCurrencyCode()) + qc.getSellRateLocalAmount());
                                    } else {
                                        log.info("I am In ELSE CURRENCY CODE ");
                                        currencyTotalMap.put(qc.getCurrencyMaster().getCurrencyCode(), qc.getSellRateLocalAmount());
                                    }
                                }
                            }
                        }
                    }
                }


                //End for Group List Data


                log.info("Currency Total Map :: " + currencyTotalMap);
                List<LabelValue> lvList = new ArrayList<>();
                Set<String> lvCurrencySet = currencyTotalMap.keySet();
                Double totalNetAmount = 0.0;

                Set<String> currencySet = new HashSet<>();
                if (lvCurrencySet != null) {
                    for (String lvalue : lvCurrencySet) {
                        LabelValue lv = new LabelValue();
                        lv.setLabel("Total Local Amount for " + lvalue);
                        log.info("total local amount currencyCode::" + lvalue);
                        if (currentCurrencyRateValueMap != null && currentCurrencyRateValueMap.get(lvalue) != null) {
                            Double tempAmount = 0.0;
                            tempAmount = (currentCurrencyRateValueMap.get(lvalue).getCSell() * currencyTotalMap.get(lvalue));
                            log.info("Currency Code :: " + lvalue + " ; Sell rate :: " + currentCurrencyRateValueMap.get(lvalue).getCSell() + " ; Amount :: " + currencyTotalMap.get(lvalue));
                            totalNetAmount = totalNetAmount + tempAmount;
                            log.info("total local amount ===:" + totalNetAmount);
                            currencySet.add(lvalue);
                            log.info(currencyTotalMap.get(lvalue));
                            CurrencyMaster cm = currencyMasterRepository.findByCurrencyCode(lvalue);
                            if (cm != null) {
                                lv.setValue(AppUtil.getCurrencyFormat(currencyTotalMap.get(lvalue), cm.getCountryMaster().getLocale(), cm.getDecimalPoint()));
                                lvList.add(lv);
                            }
                        } else {
                            Double tempAmount = 0.0;
                            tempAmount = currencyTotalMap.get(lvalue);
                            totalNetAmount = totalNetAmount + tempAmount;
                            log.info(currencyTotalMap.get(lvalue));
                            CurrencyMaster cm = currencyMasterRepository.findByCurrencyCode(lvalue);
                            if (cm != null) {
                                lv.setValue(AppUtil.getCurrencyFormat(currencyTotalMap.get(lvalue), cm.getCountryMaster().getLocale(), cm.getDecimalPoint()));
                                lvList.add(lv);
                            }
                        }

                        log.info("Total :: " + totalNetAmount);
                    }
                }
                if (currencySet != null && !currencySet.isEmpty()) {
                    for (String notesCur : currencySet) {
                        if (currencyMaster.getCurrencyCode() != null && currentCurrencyRateValueMap.get(notesCur) != null && currentCurrencyRateValueMap.get(notesCur).getCSell() != null) {
                            exchangeRates = exchangeRates + " " + currencyMaster.getCurrencyCode() + " 1 = " + notesCur + " " + decimalFormat.format(currentCurrencyRateValueMap.get(notesCur).getCSell()) + ", ";
                        }
                    }
                }
                if (totalNetAmount > 0.0 && currencyMaster != null && currencyMaster.getCurrencyCode() != null) {
                    LabelValue lv = new LabelValue();
                    lv.setLabel("Net Amount in " + currencyMaster.getCurrencyCode());
                    lv.setValue(AppUtil.getCurrencyFormat(totalNetAmount, currencyMaster.getCountryMaster().getLocale(), currencyMaster.getDecimalPoint()));
                    lvList.add(lv);
                }

                if (qd.getDimensionUnit() != null) {
                    if (qd.getDimensionUnit().equals(DimensionUnit.INCHORPOUNDS)) {
                        parameters.put("dimensionType", "INCH");
                    }
                }

                if (qd.getDimensionUnit().equals(DimensionUnit.CENTIMETERORKILOS) || qd.getDimensionUnit().equals("CENTIMETERORKILOS")) {
                    String weight = "KG";
                    String dimension = "cm";
                    String volumeKg = "KG";
                    parameters.put("volumeKg", "(" + volumeKg + ")");
                    parameters.put("dimension", "(" + dimension + ")");
                    parameters.put("dimensionKg", "(" + weight + ")");
                } else {
                    String weight = "LBS";
                    String dimension = "in";
                    String volumeKg = "KG";
                    parameters.put("volumeKg", "(" + volumeKg + ")");
                    parameters.put("dimension", "(" + dimension + ")");
                    parameters.put("dimensionKg", "(" + weight + ")");
                }

                if (qd.getQuotationDimensionList() != null && !qd.getQuotationDimensionList().isEmpty()) {
                    for (QuotationDimension qDim : qd.getQuotationDimensionList()) {
                        QuotaionDimensionReport dimension = new QuotaionDimensionReport();
                        dimension.setDimensionPiece(AppUtil.getNumberFormat(qDim.getNoOfPiece(), quotation.getCountryMaster().getLocale()));
                        dimension.setDimensionLength(AppUtil.getNumberFormat(qDim.getLength(), quotation.getCountryMaster().getLocale()));
                        dimension.setDimensionHeight(AppUtil.getNumberFormat(qDim.getHeight(), quotation.getCountryMaster().getLocale()));
                        dimension.setDimensionWidth(AppUtil.getNumberFormat(qDim.getWidth(), quotation.getCountryMaster().getLocale()));

                        dimension.setGrossWeight(AppUtil.getWeightFormat(qDim.getGrossWeight(), quotation.getCountryMaster().getLocale()));
                        dimension.setDimensionVolume(AppUtil.getWeightFormat(qDim.getVolWeight(), quotation.getCountryMaster().getLocale()));
                        dimension.setGrossWeightLB(AppUtil.getWeightFormat(qDim.getGrossWeight(), quotation.getCountryMaster().getLocale()));

                        dimensionList.add(dimension);
                        parameters.put("showDimension", "SHOW");
                    }
                }
                if (qd.getServiceNoteList() != null && !qd.getServiceNoteList().isEmpty()) {
                    for (QuotationDetailGeneralNote serviceNote : qd.getServiceNoteList()) {
                        Notes note = new Notes();
                        note.setReportNote(serviceNote.getNote() != null ? serviceNote.getNote() : "");
                        noteList.add(note);
                    }
                }
                EnquiryLog el = null;
                if (quotation != null) {
                    String enquiryNo = quotation.getEnquiryNo();   //Getting enquiryNo from Enquiry
                    el = enquiryDetailRepository.findByEnquiryNo(enquiryNo);  //Getting EnquiryLog Object
                }
                if (el != null && el.getEnquiryDetailList() != null && !el.getEnquiryDetailList().isEmpty()) {
                    for (int i = 0; i < el.getEnquiryDetailList().size(); i++) {
                        for (int j = i; j < quotation.getQuotationDetailList().size(); j++) {
                            if (el.getEnquiryDetailList().get(i).getCommodityMaster() != null) {
                                quotation.getQuotationDetailList().get(i).setCommodityMaster(el.getEnquiryDetailList().get(i).getCommodityMaster());  //Setting the commodityHsName to Quotation
                                parameters.put("commodityGroupName", quotation.getQuotationDetailList().get(i).getCommodityMaster().getHsName());   //Getting the commodityHsName from Quotation and show the report
                            } else {
                                quotation.getQuotationDetailList().get(i).setCommodityMaster(null);
                                parameters.put("commodityGroupName", "");
                            }

                        }
                    }
                }
                parameters.put("repGrossWeight", AppUtil.getWeightFormat(qd.getDimensionGrossWeight(), quotation.getCountryMaster().getLocale()));
                parameters.put("repPiece", AppUtil.getNumberFormat(qd.getTotalNoOfPieces(), quotation.getCountryMaster().getLocale()));
                parameters.put("repVolume", AppUtil.getWeightFormat(qd.getDimensionVolWeight(), quotation.getCountryMaster().getLocale()));

            } else {

                parameters.put("repGrossWeight", "");
                parameters.put("repPiece", "");
                parameters.put("repVolume", "");
            }
            parameters.put("dimensionList", dimensionList);
            parameters.put("originName", originName);
            parameters.put("destinationName", destinationName);
            parameters.put("carrierName", carrierName);
            // General Notes Parameter
            List<GeneralNotes> generalNoteList = new ArrayList<>();
            log.info("Exchange Rates : " + exchangeRates);
            if (!exchangeRates.equals(" Ex-Rate :")) {
                exchangeRates = exchangeRates.substring(0, exchangeRates.length() - 2);
                GeneralNotes generalNote = new GeneralNotes();
                generalNote.setGeneralNote(exchangeRates);
                generalNoteList.add(generalNote);
            }

            if (!quotation.getGeneralNoteList().isEmpty()) {
                for (QuotationGeneralNote gnData : quotation.getGeneralNoteList()) {
                    GeneralNotes generalNote = new GeneralNotes();
                    generalNote.setGeneralNote(gnData.getNote());
                    generalNoteList.add(generalNote);

                }
            }

            parameters.put("generalNoteList", generalNoteList);
            parameters.put("noteList", noteList);

            JasperReport jasperReport = JasperCompileManager.compileReport(appResourcePath + "/efs-report/quotation_with_estimation_new_format.jrxml");
            JRDataSource dataSource = new JRBeanCollectionDataSource(quotationFieldCollection, false);
            return JasperFillManager.fillReport(jasperReport, parameters, dataSource);
        } catch (RestException re) {
            log.error("RestException in Quotation.get method while getting the quotation : " + re);
            baseDto.setResponseCode(ErrorCode.QUOTATION_ID_INVALID);
        } catch (Exception e) {
            log.error("Exception in quotation.get method while getting the quotation : ", e);
            baseDto.setResponseCode(ErrorCode.FAILED);
        }
        log.info("Quotation With Estimation For Resource [" + data.getResourceId() + "] function ended");
        return null;
    }

}
