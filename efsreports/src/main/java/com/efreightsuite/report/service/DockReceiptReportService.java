package com.efreightsuite.report.service;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletResponse;

import com.efreightsuite.dto.AutoAttachmentDto;
import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.ReportDownloadRequestDto;
import com.efreightsuite.enumeration.DimensionUnit;
import com.efreightsuite.enumeration.ReportConfig;
import com.efreightsuite.enumeration.ReportDownloadOption;
import com.efreightsuite.enumeration.ReportName;
import com.efreightsuite.enumeration.YesNo;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.model.CfsReceiveEntry;
import com.efreightsuite.model.DocumentDetail;
import com.efreightsuite.model.LocationMaster;
import com.efreightsuite.model.LogoMaster;
import com.efreightsuite.model.ReportConfigurationMaster;
import com.efreightsuite.model.ServiceDocumentDimension;
import com.efreightsuite.model.ShipmentServiceDetail;
import com.efreightsuite.model.ShipmentServiceReference;
import com.efreightsuite.model.UserProfile;
import com.efreightsuite.report.DimensionDto;
import com.efreightsuite.service.AuthService;
import com.efreightsuite.util.AppUtil;
import lombok.extern.log4j.Log4j2;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

import org.springframework.stereotype.Service;

@Service
@Log4j2
public class DockReceiptReportService extends AbstractReportService {
    public ReportName getReportType() {
        return ReportName.DOCK_RECEIPT;
    }

    public byte[] processReport(ReportDownloadRequestDto data, HttpServletResponse response, boolean isMultiple) {
        log.info("I am in DockReceiptReportService..");
        BaseDto baseDto = new BaseDto();
        UserProfile logInUSer = AuthService.getCurrentUser();
        JasperPrint jasperPrint = getReport(baseDto, data, AuthService.getCurrentUser());

        byte[] a = null;
        AutoAttachmentDto autoAttachmentDto = reportUtil.autoAttachment(ReportName.DOCK_RECEIPT, YesNo.Yes, data.getResourceId(), "Shipment");
        if (data.getSingle() != null && data.getSingle() == true) {
            if (data.getDownloadOption() == ReportDownloadOption.Preview || data.getDownloadOption() == ReportDownloadOption.Print) {
                reportUtil.japerPrintToResponseOutputStream(data.getDownloadFileType(), jasperPrint, "DOCK_RECEIPT", response, false, null);
            } else if (autoAttachmentDto != null && autoAttachmentDto.getIsAttach() != null || data.getDownloadOption() == ReportDownloadOption.Download) {
                reportUtil.japerPrintToResponseOutputStream(data.getDownloadFileType(), jasperPrint, "DOCK_RECEIPT", response, false, autoAttachmentDto);
            }
        }
        if (data.getSingle() != null && data.getSingle() == true && data.getDownloadOption() == ReportDownloadOption.eMail) {
            a = reportUtil.japerPrintToResponseOutputStream(data.getDownloadFileType(), jasperPrint, "DOCK_RECEIPT", response, false, null);
            shipmentReportMailer.sendShipmentMailToCustomer(data.getResourceId(), logInUSer, ReportName.DOCK_RECEIPT, a);
        }
        if (isMultiple == true && data.getDownloadOption() == ReportDownloadOption.eMail) {
            a = reportUtil.japerPrintToResponseOutputStream(data.getDownloadFileType(), jasperPrint, "DOCK_RECEIPT", response, true, null);
            return a;
        }
        return null;
    }

    private JasperPrint getReport(BaseDto baseDto, ReportDownloadRequestDto data, UserProfile loggedInUser) {
        DocumentDetail document = documentDetailRepository.findById(data.getResourceId());
        Map<String, Object> parameters = new HashMap<>();
        if (document != null) {
            try {
                String loggedInUserName = "";
                String loggedInLocation = "";
                String companyName = "";
                if (loggedInUser != null) {
                    if (loggedInUser.getEmployee() != null && loggedInUser.getEmployee().getEmployeeName() != null) {
                        loggedInUserName = loggedInUser.getEmployee().getEmployeeName();
                    } else if (loggedInUser.getUserName() != null) {
                        loggedInUserName = loggedInUser.getUserName();
                    } else {
                        loggedInUserName = "";
                    }
                    if (loggedInUser.getSelectedUserLocation() != null) {
                        LogoMaster logoMaster = logoMasterRepository.findByLocationMaster(loggedInUser.getSelectedUserLocation()); // getting logo
                        try {
                            if (logoMaster != null && logoMaster.getLogo() != null) {
                                InputStream in = new ByteArrayInputStream(logoMaster.getLogo());
                                BufferedImage bImageFromConvert = ImageIO.read(in);
                                parameters.put("logo", bImageFromConvert);
                                log.info("Image getting success::");
                            }
                        } catch (Exception ex) {
                            log.error("Image getting failed:::", ex);
                        }
                        loggedInLocation = loggedInUser.getSelectedUserLocation().getLocationName() != null ? loggedInUser.getSelectedUserLocation().getLocationName() : "";
                        LocationMaster locationMaster = locationMasterRepository.findByLocationCode(loggedInUser.getSelectedUserLocation().getLocationCode());
                        if (locationMaster != null) {
                            parameters.put("companyAddressLine1", locationMaster.getAddressLine1());
                            parameters.put("companyAddressLine2", locationMaster.getAddressLine2());
                            parameters.put("companyAddressLine3", locationMaster.getAddressLine3());
                            companyName = locationMaster.getBranchName() != null ? locationMaster.getBranchName() : "";
                            parameters.put("companyName", reportUtil.removeSpecialCharacters(companyName, data.getDownloadOption()));
                        }
                        parameters.put("refDate", reportUtil.dateToReportDate(new Date(), loggedInUser.getSelectedUserLocation().getJavaDateFormat())); // today
                        parameters.put("fromDate", reportUtil.dateToReportDate(new Date(), loggedInUser.getSelectedUserLocation().getJavaDateFormat())); // today
                    } else {
                        loggedInLocation = "";
                    }
                    parameters.put("refPlace", loggedInUserName + "-" + loggedInLocation);
                    parameters.put("fromName", loggedInUserName);//

                    if (document.getDestination() != null) {
                        parameters.put("destination", document.getDestination().getPortName() != null ? document.getDestination().getPortName() : "");
                    }

                    String carrierName = "";
                    if (document.getCarrier() != null) {
                        carrierName = document.getCarrier().getCarrierName() != null
                                ? document.getCarrier().getCarrierName() : "";
                    }
                    String flightNo = "";
                    if (document.getRouteNo() != null) {
                        flightNo = document.getRouteNo();
                    }

                    if (document.getShipper() != null && document.getShipper().getPartyName() != null) {
                        parameters.put("toName", reportUtil.removeSpecialCharacters(document.getShipper().getPartyName(), data.getDownloadOption()));
                    }
                    String address3 = "";
                    String address4 = "";
                    String phoneNumber = "";
                    String mobileNumber = "";

                    if (document.getShipperAddress() != null) {
                        parameters.put("shipperAddressLine1", document.getShipperAddress().getAddressLine1() + "," + document.getShipperAddress().getAddressLine2());
                        address3 = document.getShipperAddress().getAddressLine3() != null ? document.getShipperAddress().getAddressLine3() : "";
                        address4 = document.getShipperAddress().getAddressLine4() != null ? document.getShipperAddress().getAddressLine4() : "";
                        phoneNumber = document.getShipperAddress().getPhone() != null ? document.getShipperAddress().getPhone() : "";
                        mobileNumber = document.getShipperAddress().getMobile() != null ? document.getShipperAddress().getMobile() : "";
                    }
                    if (address3.length() > 0 && address4.length() > 0) {
                        parameters.put("shipperAddressLine2", address3 + "," + address4);
                    } else if (address3.length() > 0) {
                        parameters.put("shipperAddressLine2", address3);
                    } else if (address4.length() > 0) {
                        parameters.put("shipperAddressLine2", address4);
                    }

                    if (phoneNumber.length() > 0 && mobileNumber.length() > 0) {
                        parameters.put("shipperAddressLine3", phoneNumber + "," + mobileNumber);
                    } else if (phoneNumber.length() > 0) {
                        parameters.put("shipperAddressLine3", phoneNumber);
                    } else if (mobileNumber.length() > 0) {
                        parameters.put("shipperAddressLine3", mobileNumber);
                    }


                    if (carrierName.length() > 0 && flightNo.length() > 0) {
                        parameters.put("carrierFlightNo", carrierName + " / " + flightNo);
                    } else {
                        parameters.put("carrierFlightNo", carrierName + "  " + flightNo);
                    }

                    if (document.getOrigin() != null) {
                        parameters.put("receivingTerminal", document.getOrigin().getPortName() != null ? document.getOrigin().getPortName() : "");
                    }
                    if (document.getMarksAndNo() != null) {
                        parameters.put("marksNumbers", document.getMarksAndNo());
                    }

                    String etdDate = "";
                    String etaDate = "";

                    etdDate = reportUtil.dateToReportDate(document.getEtd(), loggedInUser.getSelectedUserLocation().getJavaDateFormat());
                    etaDate = reportUtil.dateToReportDate(document.getEta(), loggedInUser.getSelectedUserLocation().getJavaDateFormat());
                    parameters.put("etdDate", etdDate);
                    parameters.put("etaDate", etaDate);
                }

                ShipmentServiceDetail shipmentServie = documentDetailRepository.findByIdForShipmentServiceDetail(data.getResourceId()); // Getting Document Object Id from ID																					// ID
                if (shipmentServie != null) {
                    log.info("ServiceID::::::::::" + shipmentServie.getId());
                    ShipmentServiceReference reference = referenceTypeMasterRepository.findByReferenceNumber(shipmentServie.getId());
                    if (reference != null) {
                        parameters.put("customerOrderNo", reference.getReferenceNumber());
                    }
                }

                List<DimensionDto> dimensionList = new ArrayList<>();
                if (shipmentServie != null) {

                    if (shipmentServie.getHazardous() != null) {
                        if (shipmentServie.getHazardous().equals(YesNo.Yes)) {
                            parameters.put("hazardousName", "Yes");
                        } else {
                            parameters.put("hazardousName", "No");
                        }
                    }
                    String packName = "";
                    if (shipmentServie.getPackMaster() != null) {
                        packName = shipmentServie.getPackMaster().getPackName();
                    }

                    List<CfsReceiveEntry> cfs = cfsReceiveEntryRepository.findByShipmentService(shipmentServie.getServiceUid());

                    if (shipmentServie.getBookedPieces() != null) {
                        parameters.put("bookedPieces", AppUtil.getNumberFormat(shipmentServie.getBookedPieces(), shipmentServie.getCountry().getLocale()));
                        if (cfs == null || cfs.size() == 0) {
                            parameters.put("receivedPieces", AppUtil.getNumberFormat(shipmentServie.getBookedPieces(), shipmentServie.getCountry().getLocale()));
                        }
                    }
                    if (shipmentServie.getBookedChargeableUnit() != null) {
                        parameters.put("totalBookedChargeWeight", AppUtil.getWeightFormat(shipmentServie.getBookedChargeableUnit(), shipmentServie.getCountry().getLocale()));
                        if (cfs == null || cfs.size() == 0) {
                            parameters.put("totalRecChargeWeight", AppUtil.getWeightFormat(shipmentServie.getBookedChargeableUnit(), shipmentServie.getCountry().getLocale()));
                        }
                    }
                    if (shipmentServie.getBookedVolumeWeightUnitKg() != null) {
                        parameters.put("totalBookedVolume", AppUtil.getWeightFormat(shipmentServie.getBookedVolumeWeightUnitKg(), shipmentServie.getCountry().getLocale()));
                        if (cfs == null || cfs.size() == 0) {
                            parameters.put("totalRecVolume", AppUtil.getWeightFormat(shipmentServie.getBookedVolumeWeightUnitKg(), shipmentServie.getCountry().getLocale()));
                        }
                    }
                    if (shipmentServie.getBookedGrossWeightUnitKg() != null) {
                        parameters.put("totalBookedKgs", AppUtil.getWeightFormat(shipmentServie.getBookedGrossWeightUnitKg(), shipmentServie.getCountry().getLocale()));
                        if (cfs == null || cfs.size() == 0) {
                            parameters.put("totalRecKgs", AppUtil.getWeightFormat(shipmentServie.getBookedGrossWeightUnitKg(), shipmentServie.getCountry().getLocale()));
                        }
                    }
                    if (shipmentServie.getBookedGrossWeightUnitPound() != null) {
                        parameters.put("totalBookedLbs", AppUtil.getWeightFormat(shipmentServie.getBookedGrossWeightUnitPound(), shipmentServie.getCountry().getLocale()));
                        if (cfs == null || cfs.size() == 0) {
                            parameters.put("totalRecLbs", AppUtil.getWeightFormat(shipmentServie.getBookedGrossWeightUnitPound(), shipmentServie.getCountry().getLocale()));
                        }
                    }


                    if (cfs != null) {
                        Double totalChargeWeight = 0.0;
                        Double totalVolumeWeight = 0.0;
                        Double totalRecKgs = 0.0;
                        Double totalRecLbs = 0.0;
                        Long totalpieeces = 0L;

                        for (CfsReceiveEntry cf : cfs) {
                            parameters.put("truckingCompanyProName", cf.getProNumber());
                            parameters.put("dockReceiptNo", cf.getCfsReceiptNumber());
                            if (cf != null) {
                                if (cf.getChargebleWeight() != null) {
                                    totalChargeWeight = totalChargeWeight + cf.getChargebleWeight();
                                }
                                if (cf.getVolumeWeight() != null) {
                                    totalVolumeWeight = totalVolumeWeight + cf.getVolumeWeight();
                                }
                                if (cf.getGrossWeight() != null) {
                                    totalRecKgs = totalRecKgs + cf.getGrossWeight();
                                }
                                if (cf.getGrossWeightInPound() != null) {
                                    totalRecLbs = totalRecLbs + cf.getGrossWeightInPound();
                                }
                                if (cf.getNoOfPieces() != null) {
                                    totalpieeces = totalpieeces + cf.getNoOfPieces();
                                }

                            }
                        }

                        if (totalpieeces != null && totalpieeces > 0) {
                            parameters.put("receivedPieces", AppUtil.getNumberFormat(totalpieeces, shipmentServie.getCountry().getLocale()));
                        }
                        if (totalChargeWeight != null && totalChargeWeight > 0) {
                            parameters.put("totalRecChargeWeight", AppUtil.getWeightFormat(totalChargeWeight, shipmentServie.getCountry().getLocale()));
                        }
                        if (totalVolumeWeight != null && totalVolumeWeight > 0) {
                            parameters.put("totalRecVolume", AppUtil.getWeightFormat(totalVolumeWeight, shipmentServie.getCountry().getLocale()));
                        }
                        if (totalRecKgs != null && totalRecKgs > 0) {
                            parameters.put("totalRecKgs", AppUtil.getWeightFormat(totalRecKgs,
                                    shipmentServie.getCountry().getLocale()));
                        }
                        if (totalRecLbs != null && totalRecLbs > 0) {
                            parameters.put("totalRecLbs", AppUtil.getWeightFormat(totalRecLbs,
                                    shipmentServie.getCountry().getLocale()));
                        }
                    }

                    parameters.put("bookingNumber", shipmentServie.getShipmentUid());
                    if (shipmentServie.getDocumentList() != null && !shipmentServie.getDocumentList().isEmpty()) {
                        for (DocumentDetail documentDetail : shipmentServie.getDocumentList()) {
                            if (documentDetail.getDimensionList() != null && !documentDetail.getDimensionList().isEmpty()) {
                                for (ServiceDocumentDimension dimesionList : document.getDimensionList()) {
                                    DimensionDto dimension = new DimensionDto();
                                    if (dimesionList != null) {
                                        dimension.setHeight(AppUtil.getNumberFormat(dimesionList.getHeight(), shipmentServie.getCountry().getLocale()));
                                        dimension.setLength(AppUtil.getNumberFormat(dimesionList.getLength(), shipmentServie.getCountry().getLocale()));
                                        dimension.setWidth(AppUtil.getNumberFormat(dimesionList.getWidth(), shipmentServie.getCountry().getLocale()));
                                        dimension.setPieces(AppUtil.getNumberFormat(dimesionList.getNoOfPiece(), shipmentServie.getCountry().getLocale()));
                                    }
                                    dimension.setPackages(packName);
                                    dimensionList.add(dimension);    //Adding dimension Object
                                }
                            }
                        }
                    }

                    parameters.put("dimensionList", dimensionList);

                    if (shipmentServie.getDimensionUnit() != null) {
                        if (shipmentServie.getDimensionUnit().equals(DimensionUnit.CENTIMETERORKILOS)) {
                            parameters.put("inchCentLength", "cm");
                        } else {
                            parameters.put("inchCentLength", "in");
                        }
                    } else {
                        parameters.put("inchCentLength", "");
                    }


                    {
                        ReportConfigurationMaster tmpObj = reportConfigurationMasterRepository.getByKeyAndLocation(ReportConfig.DOCKRECEIPT_FOOTER_ONE, shipmentServie.getLocation().getId());
                        if (tmpObj != null && tmpObj.getReportValue() != null) {
                            parameters.put("dockReceiptFooterInformation1", tmpObj.getReportValue());
                        }
                    }

                    {
                        ReportConfigurationMaster tmpObj = reportConfigurationMasterRepository.getByKeyAndLocation(ReportConfig.DOCKRECEIPT_FOOTER_TWO, shipmentServie.getLocation().getId());
                        if (tmpObj != null && tmpObj.getReportValue() != null) {
                            parameters.put("dockReceiptFooterInformation2", tmpObj.getReportValue());
                        }
                    }

                    {
                        ReportConfigurationMaster tmpObj = reportConfigurationMasterRepository.getByKeyAndLocation(ReportConfig.DOCKRECEIPT_FOOTER_THREE, shipmentServie.getLocation().getId());
                        if (tmpObj != null && tmpObj.getReportValue() != null) {
                            parameters.put("dockReceiptFooterInformation3", tmpObj.getReportValue());
                        }
                    }

                    {
                        ReportConfigurationMaster tmpObj = reportConfigurationMasterRepository.getByKeyAndLocation(ReportConfig.DOCKRECEIPT_FOOTER, shipmentServie.getLocation().getId());
                        if (tmpObj != null && tmpObj.getReportValue() != null) {
                            parameters.put("dockReceiptFooter", tmpObj.getReportValue());
                        }
                    }


                }
                parameters.put("reportName", "Dock Receipt");


                JasperReport jasperReport = JasperCompileManager.compileReport(appResourcePath + "/efs-report/dock_receipt_air.jrxml");
                List<Object> fieldCollection1 = new ArrayList<>();
                Object obj1 = new Object();
                fieldCollection1.add(obj1);
                JRDataSource dataSource = new JRBeanCollectionDataSource(fieldCollection1, false);
                return JasperFillManager.fillReport(jasperReport, parameters, dataSource);

            } catch (RestException re) {
                log.error("RestException Occures in DockReceipt:: ", re);
            } catch (Exception exception) {
                log.error("RestException Occures in DockReceipt::   :: ", exception);
                baseDto.setResponseCode(ErrorCode.FAILED);
            }
        }
        return null;
    }
}
