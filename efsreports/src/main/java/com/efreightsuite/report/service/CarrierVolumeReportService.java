package com.efreightsuite.report.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import com.efreightsuite.dto.ReportDownloadRequestDto;
import com.efreightsuite.enumeration.ReportName;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.model.DocumentDetail;
import com.efreightsuite.model.LocationMaster;
import com.efreightsuite.model.ShipmentServiceDetail;
import com.efreightsuite.model.UserProfile;
import com.efreightsuite.report.PickupSheetDto;
import com.efreightsuite.service.AuthService;
import lombok.extern.log4j.Log4j;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

import org.springframework.stereotype.Service;

@Service
@Log4j
public class CarrierVolumeReportService extends AbstractReportService {

    public ReportName getReportType() {
        return ReportName.CARRIER_VOLUME;
    }

    public byte[] processReport(ReportDownloadRequestDto data, HttpServletResponse response, boolean isMultiple) {
        log.info("Enter Carrier Volume Report Serive.....");
        Map<String, Object> parameters = new HashMap<>();
        List<PickupSheetDto> carrierSummaryList = new ArrayList<>();
        try {
            String portName = "";
            UserProfile loggedInUser = AuthService.getCurrentUser();
            SimpleDateFormat dt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String fromDate = dt.format(data.getApiFromDate());
            String toDate = dt.format(data.getApiToDate());
            List<ShipmentServiceDetail> serviceList = shipmentServiceDetailRepository.findByCarrier(data.getServiceCode().getServiceCode(), data.getOrigin().getId(), data.getDestination().getId(), data.getCarrier().getId(), fromDate, toDate);
            log.info("Carrier Volume Report service Object::::::" + serviceList);
            if (serviceList != null && serviceList.size() > 0) {
                for (ShipmentServiceDetail serviceListAll : serviceList) {
                    List<DocumentDetail> documentList = serviceListAll.getDocumentList();
                    if (documentList != null && documentList.size() > 0) {
                        for (DocumentDetail document : documentList) {
                            PickupSheetDto pickupSheetDto = new PickupSheetDto();
                            if (document != null) {
                                if (document.getCarrier() != null) {
                                    pickupSheetDto.setCarrierName(document.getCarrier().getCarrierName() != null ? document.getCarrier().getCarrierName() : "");
                                }

                                if (document.getVolumeWeight() != null) {
                                    pickupSheetDto.setVolumeInKg(reportUtil.threeDecimalDoubleValueFormat(document.getVolumeWeight()));
                                }
                                String countryName = "";

                                if (document.getCountry() != null) {
                                    countryName = document.getCountry().getCountryName() != null ? document.getCountry().getCountryName() : "";
                                }
                                if (document.getTransitPort() != null) {
                                    portName = document.getTransitPort().getPortName() != null ? document.getTransitPort().getPortName() : "";
                                }
                                pickupSheetDto.setPortCountry(portName + " - " + countryName);
                            }
                            carrierSummaryList.add(pickupSheetDto);
                            if (loggedInUser != null && loggedInUser.getSelectedUserLocation() != null && document.getSystemTrack() != null && document.getSystemTrack().getCreateDate() != null) {
                                parameters.put("reportDate", reportUtil.dateToReportDate(document.getSystemTrack().getCreateDate(), loggedInUser.getSelectedUserLocation().getJavaDateFormat()));
                                String todayDate = "";
                                todayDate = reportUtil.dateToReportDate(new Date(), loggedInUser.getSelectedUserLocation().getJavaDateFormat());
                                parameters.put("pickupFromDate", reportUtil.dateToReportDate(data.getApiFromDate(), loggedInUser.getSelectedUserLocation().getJavaDateFormat()));
                                parameters.put("pickupToDate", reportUtil.dateToReportDate(data.getApiToDate(), loggedInUser.getSelectedUserLocation().getJavaDateFormat()));
                                parameters.put("refDate", todayDate);
                            }
                        }
                    }
                }
            }

            log.info("Address ------------------------------- " + loggedInUser.getSelectedUserLocation().getAddressLine1());
            String loggedInUserName = "";
            String loggedInLocation = "";
            if (loggedInUser != null) {
                if (loggedInUser.getEmployee() != null
                        && loggedInUser.getEmployee().getEmployeeName() != null) {
                    loggedInUserName = loggedInUser.getEmployee().getEmployeeName();
                } else if (loggedInUser.getUserName() != null) {
                    loggedInUserName = loggedInUser.getUserName();
                } else {
                    loggedInUserName = "";
                }

                if (loggedInUser.getSelectedUserLocation() != null) {
                    loggedInLocation = loggedInUser.getSelectedUserLocation()
                            .getLocationName() != null ? loggedInUser.getSelectedUserLocation().getLocationName() : "";
                    LocationMaster locationMaster = locationMasterRepository.findByLocationCode(loggedInUser.getSelectedUserLocation().getLocationCode());
                    if (locationMaster != null) {
                        parameters.put("companyAddressLine1", locationMaster.getAddressLine1() != null ? locationMaster.getAddressLine1() : "");
                        parameters.put("companyAddressLine2", locationMaster.getAddressLine2() != null ? locationMaster.getAddressLine2() : "");
                        parameters.put("companyAddressLine3", locationMaster.getAddressLine3() != null ? locationMaster.getAddressLine3() : "");
                        parameters.put("companyName", locationMaster.getBranchName() != null ? locationMaster.getBranchName() : "");
                    }


                } else {
                    loggedInLocation = "";
                }
            }
            parameters.put("refPlace", loggedInUserName + "-" + loggedInLocation);
            parameters.put("locationName", loggedInLocation);
            parameters.put("serviceCode", data.getServiceCode().getServiceCode().toString());

            parameters.put("carrierSummaryList", carrierSummaryList);
            parameters.put("reportName", "carrier volume report");
            JasperReport jasperReport = JasperCompileManager.compileReport(appResourcePath + "/efs-report/carrier_volume_summary.jrxml");
            List<Object> fieldCollection1 = new ArrayList<>();
            Object obj1 = new Object();
            fieldCollection1.add(obj1);

            JRDataSource dataSource = new JRBeanCollectionDataSource(fieldCollection1, false);
            JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, dataSource);
            reportUtil.japerPrintToResponseOutputStream(data.getDownloadFileType(), jasperPrint, "CARRIER_VOLUME", response, false, null);

        } catch (RestException e) {
            e.printStackTrace();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        log.info("CarrierVoluume Report Service Completed..... ");
        return null;
    }
}
