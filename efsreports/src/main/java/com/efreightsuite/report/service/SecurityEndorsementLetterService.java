package com.efreightsuite.report.service;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletResponse;

import com.efreightsuite.dto.AutoAttachmentDto;
import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.ReportDownloadRequestDto;
import com.efreightsuite.enumeration.ReportConfig;
import com.efreightsuite.enumeration.ReportDownloadOption;
import com.efreightsuite.enumeration.ReportName;
import com.efreightsuite.enumeration.YesNo;
import com.efreightsuite.model.Consol;
import com.efreightsuite.model.DocumentDetail;
import com.efreightsuite.model.LocationMaster;
import com.efreightsuite.model.LogoMaster;
import com.efreightsuite.model.PartyAddressMaster;
import com.efreightsuite.model.ReportConfigurationMaster;
import com.efreightsuite.model.ShipmentLink;
import com.efreightsuite.model.ShipmentServiceDetail;
import com.efreightsuite.model.UserProfile;
import com.efreightsuite.report.Notes;
import com.efreightsuite.service.AuthService;
import lombok.extern.log4j.Log4j2;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

import org.springframework.stereotype.Service;

@Service
@Log4j2
public class SecurityEndorsementLetterService extends AbstractReportService {
    @Override
    public ReportName getReportType() {
        return ReportName.SECURITY_ENDORSEMENT_LETTER;
    }

    public byte[] processReport(ReportDownloadRequestDto data, HttpServletResponse response, boolean isMultiple) {
        log.info("I am in SecurityEndorsementLetterService..");
        BaseDto baseDto = new BaseDto();
        UserProfile logInUSer = AuthService.getCurrentUser();
        JasperPrint jasperPrint = getReport(baseDto, data, AuthService.getCurrentUser());

        byte[] a = null;
        AutoAttachmentDto autoAttachmentDto = reportUtil.autoAttachment(ReportName.SECURITY_ENDORSEMENT_LETTER, YesNo.Yes, data.getResourceId(), "MasterShipment");
        if (data.getSingle() != null && data.getSingle() == true) {
            if (data.getDownloadOption() == ReportDownloadOption.Preview || data.getDownloadOption() == ReportDownloadOption.Print) {
                reportUtil.japerPrintToResponseOutputStream(data.getDownloadFileType(), jasperPrint, "SECURITY_ENDORSEMENT_LETTER", response, false, null);
            } else if (autoAttachmentDto != null && autoAttachmentDto.getIsAttach() != null || data.getDownloadOption() == ReportDownloadOption.Download) {
                reportUtil.japerPrintToResponseOutputStream(data.getDownloadFileType(), jasperPrint, "SECURITY_ENDORSEMENT_LETTER", response, false, autoAttachmentDto);
            }
        }
        if (data.getSingle() != null && data.getSingle() == true && data.getDownloadOption() == ReportDownloadOption.eMail) {
            a = reportUtil.japerPrintToResponseOutputStream(data.getDownloadFileType(), jasperPrint, "SECURITY_ENDORSEMENT_LETTER", response, false, null);
            consolReportMailer.sendConsolMailToCustomer(data.getResourceId(), logInUSer, ReportName.SECURITY_ENDORSEMENT_LETTER, a);
        }
        if (isMultiple == true && data.getDownloadOption() == ReportDownloadOption.eMail) {
            a = reportUtil.japerPrintToResponseOutputStream(data.getDownloadFileType(), jasperPrint, "SECURITY_ENDORSEMENT_LETTER", response, true, null);
            return a;
        }
        return null;
    }

    private JasperPrint getReport(BaseDto baseDto, ReportDownloadRequestDto data, UserProfile loggedInUser) {
        log.info("I am in SecurityEndorsementLetterService..");
        //TSA_NO,ISSUING_AGENT
        Consol consolEntity = consolRepository.findById(data.getResourceId());
        JasperReport jasperReport;
        try {
            jasperReport = JasperCompileManager.compileReport(appResourcePath + "/efs-report/security_endorsement_letter.jrxml");
            Map<String, Object> parameters = new HashMap<>();
            String knownShipper = "";
            String unKnownShipper = "";
            String securityInfo = "";
            //substitutionList
            List<Notes> substitutionList = new ArrayList<>();


            {
                ReportConfigurationMaster tmpObj = reportConfigurationMasterRepository.getByKeyAndLocation(ReportConfig.SECURITY_ENDORSEMENT_LETTER_KNOWN_SHIPPER, consolEntity.getLocation().getId());
                if (tmpObj != null && tmpObj.getReportValue() != null) {
                    knownShipper = tmpObj.getReportValue();
                }
            }

            {
                ReportConfigurationMaster tmpObj = reportConfigurationMasterRepository.getByKeyAndLocation(ReportConfig.SECURITY_ENDORSEMENT_LETTER_UNKNOWN_SHIPPER, consolEntity.getLocation().getId());
                if (tmpObj != null && tmpObj.getReportValue() != null) {
                    unKnownShipper = tmpObj.getReportValue();
                }
            }

            {
                ReportConfigurationMaster tmpObj = reportConfigurationMasterRepository.getByKeyAndLocation(ReportConfig.SECURITY_ENDORSEMENT_LETTER_SENSITIVE_SECURITY_INFO, consolEntity.getLocation().getId());
                if (tmpObj != null && tmpObj.getReportValue() != null) {
                    securityInfo = tmpObj.getReportValue();
                }
            }

            {
                ReportConfigurationMaster tmpObj = reportConfigurationMasterRepository.getByKeyAndLocation(ReportConfig.SECURITY_ENDORSEMENT_LETTER_SUBSTITUTION_1, consolEntity.getLocation().getId());
                if (tmpObj != null && tmpObj.getReportValue() != null) {
                    Notes substitution = new Notes();
                    substitution.setReportNote(tmpObj.getReportValue());
                    substitutionList.add(substitution);
                }
            }

            {
                ReportConfigurationMaster tmpObj = reportConfigurationMasterRepository.getByKeyAndLocation(ReportConfig.SECURITY_ENDORSEMENT_LETTER_SUBSTITUTION_2, consolEntity.getLocation().getId());
                if (tmpObj != null && tmpObj.getReportValue() != null) {
                    Notes substitution = new Notes();
                    substitution.setReportNote(tmpObj.getReportValue());
                    substitutionList.add(substitution);
                }
            }

            {
                ReportConfigurationMaster tmpObj = reportConfigurationMasterRepository.getByKeyAndLocation(ReportConfig.SECURITY_ENDORSEMENT_LETTER_SUBSTITUTION_3, consolEntity.getLocation().getId());
                if (tmpObj != null && tmpObj.getReportValue() != null) {
                    Notes substitution = new Notes();
                    substitution.setReportNote(tmpObj.getReportValue());
                    substitutionList.add(substitution);
                }
            }

            {
                ReportConfigurationMaster tmpObj = reportConfigurationMasterRepository.getByKeyAndLocation(ReportConfig.SECURITY_ENDORSEMENT_LETTER_SUBSTITUTION_4, consolEntity.getLocation().getId());
                if (tmpObj != null && tmpObj.getReportValue() != null) {
                    Notes substitution = new Notes();
                    substitution.setReportNote(tmpObj.getReportValue());
                    substitutionList.add(substitution);
                }
            }

            {
                ReportConfigurationMaster tmpObj = reportConfigurationMasterRepository.getByKeyAndLocation(ReportConfig.SECURITY_ENDORSEMENT_LETTER_SUBSTITUTION_5, consolEntity.getLocation().getId());
                if (tmpObj != null && tmpObj.getReportValue() != null) {
                    Notes substitution = new Notes();
                    substitution.setReportNote(tmpObj.getReportValue());
                    substitutionList.add(substitution);
                }
            }

            {
                ReportConfigurationMaster tmpObj = reportConfigurationMasterRepository.getByKeyAndLocation(ReportConfig.SECURITY_ENDORSEMENT_LETTER_SUBSTITUTION_6, consolEntity.getLocation().getId());
                if (tmpObj != null && tmpObj.getReportValue() != null) {
                    Notes substitution = new Notes();
                    substitution.setReportNote(tmpObj.getReportValue());
                    substitutionList.add(substitution);
                }
            }

            List<Object> fieldCollection1 = new ArrayList<>();
            Object obj1 = new Object();
            fieldCollection1.add(obj1);
            JRDataSource dataSource = new JRBeanCollectionDataSource(fieldCollection1, false);
            boolean knownShipperVarNA = true;
            if (consolEntity != null) {
                parameters.put("warningContent", securityInfo);
                String loggedInUserName = "";
                String loggedInLocation = "";
                String companyName = "";
                if (loggedInUser != null) {
                    if (loggedInUser.getEmployee() != null && loggedInUser.getEmployee().getEmployeeName() != null) {
                        loggedInUserName = loggedInUser.getEmployee().getEmployeeName();
                    } else if (loggedInUser.getUserName() != null) {
                        loggedInUserName = loggedInUser.getUserName();
                    } else {
                        loggedInUserName = "";
                    }

                    if (loggedInUser.getSelectedUserLocation() != null) {
                        parameters.put("refDate", reportUtil.dateToReportDate(new Date(), loggedInUser.getSelectedUserLocation().getJavaDateFormat()));
                        parameters.put("headerDate", reportUtil.dateToReportDate(new Date(), loggedInUser.getSelectedUserLocation().getJavaDateFormat()));
                        loggedInLocation = loggedInUser.getSelectedUserLocation().getLocationName() != null ? loggedInUser.getSelectedUserLocation().getLocationName() : "";
                        if (loggedInUser.getSelectedUserLocation() != null) {
                            LocationMaster locationMaster = locationMasterRepository.findByLocationCode(loggedInUser.getSelectedUserLocation().getLocationCode());
                            if (locationMaster != null) {
                                parameters.put("companyAddressLine1", locationMaster.getAddressLine1() != null ? locationMaster.getAddressLine1() : "");
                                parameters.put("companyAddressLine2", locationMaster.getAddressLine2() != null ? locationMaster.getAddressLine2() : "");
                                parameters.put("companyAddressLine3", locationMaster.getAddressLine3() != null ? locationMaster.getAddressLine3() : "");
                                companyName = locationMaster.getBranchName() != null ? reportUtil.removeSpecialCharacters(locationMaster.getBranchName(), data.getDownloadOption()) : "";
                                parameters.put("companyName", reportUtil.removeSpecialCharacters(companyName, data.getDownloadOption()));
                            }
                            LogoMaster logoMaster = logoMasterRepository.findByLocationMaster(loggedInUser.getSelectedUserLocation());
                            try {
                                if (logoMaster != null && logoMaster.getLogo() != null) {
                                    InputStream in = new ByteArrayInputStream(logoMaster.getLogo());
                                    BufferedImage bImageFromConvert = ImageIO.read(in);
                                    parameters.put("logo", bImageFromConvert);
                                    log.info("Image getting success::");
                                }
                            } catch (Exception ex) {
                                log.error("Image getting failed:::", ex);
                            }
                        }
                    } else {
                        loggedInLocation = "";
                    }
                }

                parameters.put("issuingAgent", companyName);
                if (companyName.length() > 0) {
                    parameters.put("issuingAgent", companyName);
                } else {
                    parameters.put("issuingAgent", "");
                }

                if (consolEntity.getShipmentLinkList() != null && !consolEntity.getShipmentLinkList().isEmpty()) {
                    for (ShipmentLink shipmentLink : consolEntity.getShipmentLinkList()) {       //Getting all ShipmentLinks from Consol
                        ShipmentServiceDetail serviceDetail = shipmentLink.getService();    //Getting one by one
                        if (serviceDetail != null) {
                            if (serviceDetail.getDocumentList() != null && !serviceDetail.getDocumentList().isEmpty()) {  //Getting all Documets List from Service
                                for (DocumentDetail document : serviceDetail.getDocumentList()) {       //Getting one by one document
                                    if (document != null) {
                                        if (document.getShipper() != null) {
                                            boolean isKnownShipper = true;
                                            List<PartyAddressMaster> pdm = partyAddressMasterRepository.getAll(document.getShipper().getId());
                                            for (PartyAddressMaster pd : pdm) {
                                                if (pd.getPartyCountryField() != null && pd.getPartyCountryField().getKnownShipperValidationNo() != null &&
                                                        pd.getPartyCountryField().getKnownShipperValidationDate() != null) {

                                                    Date dt1 = pd.getPartyCountryField().getKnownShipperValidationDate();
                                                    log.info("ShipperValidatorDate:::" + dt1);
                                                    Date dt2 = com.efreightsuite.util.TimeUtil.getCurrentLocationTime(serviceDetail.getLocation());
                                                    log.info("ShipperValidatorDate:::" + dt2);
                                                    if (dt1.compareTo(dt2) > 0) {
                                                        isKnownShipper = false;
                                                        String tsaNo = pd.getPartyCountryField().getKnownShipperValidationNo();
                                                        knownShipper = knownShipper.replace("ISSUING_AGENT", companyName);
                                                        knownShipper = knownShipper.replace("TSA_NO", tsaNo);
                                                        parameters.put("securityProgramContent", knownShipper);
                                                        parameters.put("substitutionList", substitutionList);

                                                    } else {
                                                        unKnownShipper = unKnownShipper.replace("ISSUING_AGENT", companyName);
                                                        log.info("unKnownShipper" + unKnownShipper);
                                                        unKnownShipper = unKnownShipper.replace("TSA_NO", "");
                                                        parameters.put("securityProgramContent", unKnownShipper);
                                                        //knownShipperVarNA = false;
                                                        break;
                                                    }
                                                } else {
                                                    isKnownShipper = false;
                                                    unKnownShipper = unKnownShipper.replace("ISSUING_AGENT", companyName);
                                                    log.info("unKnownShipper" + unKnownShipper);
                                                    unKnownShipper = unKnownShipper.replace("TSA_NO", "");
                                                    parameters.put("securityProgramContent", unKnownShipper);
                                                    //knownShipperVarNA = false;
                                                    break;
                                                }
                                            }
                                        }

                                    }
                                }
                            }
                        }
                    }
                }

                parameters.put("refPlace", loggedInUserName + " -" + loggedInLocation);
                if (consolEntity.getConsolDocument() != null && consolEntity.getConsolDocument().getMawbNo() != null) {
                    String mawbTemp = consolEntity.getConsolDocument().getMawbNo();
                    if (mawbTemp != null && mawbTemp.trim().length() > 0) {
                        String mawbFirst3Chars = "";
                        mawbTemp = mawbTemp.trim();
                        if (mawbTemp.length() >= 3) {
                            mawbFirst3Chars = mawbTemp.substring(0, 3);
                            mawbTemp = mawbTemp.substring(3, mawbTemp.length());
                        }
                        if (mawbTemp.length() >= 4) {
                            mawbTemp = mawbTemp.substring(0, 4) + " " + mawbTemp.substring(4, mawbTemp.length());
                        }

                        parameters.put("mawbNo", mawbFirst3Chars + "-" + mawbTemp);
                    }
                }

                if (consolEntity.getCarrier() != null) {
                    parameters.put("carrierName", consolEntity.getCarrier().getCarrierName() != null ? consolEntity.getCarrier().getCarrierName() : "");
                }

                if (!knownShipperVarNA) {
                    parameters.put("securityProgramContent", knownShipper);
                }
            }

            return JasperFillManager.fillReport(jasperReport, parameters, dataSource);


        } catch (JRException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
