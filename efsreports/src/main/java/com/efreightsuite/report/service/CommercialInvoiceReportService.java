package com.efreightsuite.report.service;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletResponse;

import com.efreightsuite.dto.AutoAttachmentDto;
import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.ReportDownloadRequestDto;
import com.efreightsuite.enumeration.ReportDownloadOption;
import com.efreightsuite.enumeration.ReportName;
import com.efreightsuite.enumeration.YesNo;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.model.AddressMaster;
import com.efreightsuite.model.DocumentDetail;
import com.efreightsuite.model.LocationMaster;
import com.efreightsuite.model.LogoMaster;
import com.efreightsuite.model.PartyMaster;
import com.efreightsuite.model.PortMaster;
import com.efreightsuite.model.ShipmentServiceDetail;
import com.efreightsuite.model.UserProfile;
import com.efreightsuite.service.AuthService;
import com.efreightsuite.util.AppUtil;
import lombok.extern.log4j.Log4j2;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

import org.springframework.stereotype.Service;

@Service
@Log4j2
public class CommercialInvoiceReportService extends AbstractReportService {
    public ReportName getReportType() {
        return ReportName.COMMERCIAL_INVOICE;
    }

    public byte[] processReport(ReportDownloadRequestDto data, HttpServletResponse response, boolean isMultiple) {
        log.info("I am in CommercialInvoiceReportService..");
        BaseDto baseDto = new BaseDto();
        UserProfile logInUSer = AuthService.getCurrentUser();
        JasperPrint jasperPrint = getReport(baseDto, data, AuthService.getCurrentUser());

        byte[] a = null;
        AutoAttachmentDto autoAttachmentDto = reportUtil.autoAttachment(ReportName.COMMERCIAL_INVOICE, YesNo.Yes, data.getResourceId(), "Shipment");
        if (data.getSingle() != null && data.getSingle() == true) {
            if (data.getDownloadOption() == ReportDownloadOption.Preview || data.getDownloadOption() == ReportDownloadOption.Print) {
                reportUtil.japerPrintToResponseOutputStream(data.getDownloadFileType(), jasperPrint, "COMMERCIAL_INVOICE", response, false, null);
            } else if (autoAttachmentDto != null && autoAttachmentDto.getIsAttach() != null || data.getDownloadOption() == ReportDownloadOption.Download) {
                reportUtil.japerPrintToResponseOutputStream(data.getDownloadFileType(), jasperPrint, "COMMERCIAL_INVOICE", response, false, autoAttachmentDto);
            }
        }

        if (data.getSingle() != null && data.getSingle() == true && data.getDownloadOption() == ReportDownloadOption.eMail) {
            a = reportUtil.japerPrintToResponseOutputStream(data.getDownloadFileType(), jasperPrint, "COMMERCIAL_INVOICE", response, false, null);
            shipmentReportMailer.sendShipmentMailToCustomer(data.getResourceId(), logInUSer, ReportName.COMMERCIAL_INVOICE, a);
        }
        if (isMultiple == true && data.getDownloadOption() == ReportDownloadOption.eMail) {
            a = reportUtil.japerPrintToResponseOutputStream(data.getDownloadFileType(), jasperPrint, "COMMERCIAL_INVOICE", response, true, null);
            return a;
        }
        return null;
    }

    private JasperPrint getReport(BaseDto baseDto, ReportDownloadRequestDto data, UserProfile loggedInUser) {
        log.info("I am in CommercialInvoiceReportService..");
        ShipmentServiceDetail shipmentServiceDetailEntity = shipmentServiceDetailRepository.findById(data.getResourceId());
        if (shipmentServiceDetailEntity != null) {
            // Report Parameters Data Preparation
            Map<String, Object> parameters = new HashMap<>();
            String todayDate = "";
            parameters.put("totalAmount", "");
            parameters.put("inputName", data.getParameter2() != null ? data.getParameter2() : "");
            parameters.put("reportName", data.getParameter1() != null ? data.getParameter1() : "");
            try {

                String loggedInUserName = "";
                String loggedInLocation = "";
                String companyName = "";
                if (loggedInUser != null) {
                    if (loggedInUser.getEmployee() != null && loggedInUser.getEmployee().getEmployeeName() != null) {
                        loggedInUserName = loggedInUser.getEmployee().getEmployeeName();
                    } else if (loggedInUser.getUserName() != null) {
                        loggedInUserName = loggedInUser.getUserName();
                    } else {
                        loggedInUserName = "";
                    }

                    if (loggedInUser.getSelectedUserLocation() != null) {

                        todayDate = reportUtil.dateToReportDate(new Date(), loggedInUser.getSelectedUserLocation().getJavaDateFormat());

                        parameters.put("refDate", todayDate != null ? todayDate : "");
                        parameters.put("reportDate", todayDate != null ? todayDate : "");
                        loggedInLocation = loggedInUser.getSelectedUserLocation().getLocationName() != null ? loggedInUser.getSelectedUserLocation().getLocationName() : "";

                        if (loggedInUser.getSelectedUserLocation() != null) {
                            LocationMaster locationMaster = locationMasterRepository.findByLocationCode(loggedInUser.getSelectedUserLocation().getLocationCode());
                            if (locationMaster != null) {
                                parameters.put("companyAddressLine1", locationMaster.getAddressLine1() != null ? locationMaster.getAddressLine1() : "");
                                parameters.put("companyAddressLine2", locationMaster.getAddressLine2() != null ? locationMaster.getAddressLine2() : "");
                                parameters.put("companyAddressLine3", locationMaster.getAddressLine3() != null ? locationMaster.getAddressLine3() : "");
                                companyName = locationMaster.getBranchName() != null ? locationMaster.getBranchName() : "";
                                parameters.put("companyName", reportUtil.removeSpecialCharacters(companyName, data.getDownloadOption()));
                            }
                        }
                        LogoMaster logoMaster = logoMasterRepository.findByLocationMaster(loggedInUser.getSelectedUserLocation());
                        try {
                            if (logoMaster != null && logoMaster.getLogo() != null) {
                                InputStream in = new ByteArrayInputStream(logoMaster.getLogo());
                                BufferedImage bImageFromConvert = ImageIO.read(in);
                                parameters.put("logo", bImageFromConvert);
                                log.info("Image getting success::");
                            }
                        } catch (Exception ex) {
                            log.error("Image getting failed:::", ex);
                        }

                    } else {
                        loggedInLocation = "";
                    }
                }

                parameters.put("refPlace", loggedInUserName + "-" + loggedInLocation);
                if (shipmentServiceDetailEntity.getShipment() != null) {
                    parameters.put("bookingShipmentID", shipmentServiceDetailEntity.getShipment().getShipmentUid());
                }

                PartyMaster shipper = null;
                AddressMaster shipperAddress = null;
                PartyMaster consignee = null;
                AddressMaster consigneeAddress = null;
                String commodityDescription = "";
                if (shipmentServiceDetailEntity.getDocumentList() != null && !shipmentServiceDetailEntity.getDocumentList().isEmpty()) {
                    DocumentDetail documentDetail = shipmentServiceDetailEntity.getDocumentList().get(0);
                    if (documentDetail != null) {
                        shipper = documentDetail.getShipper();
                        shipperAddress = documentDetail.getShipperAddress();
                        consignee = documentDetail.getConsignee();
                        consigneeAddress = documentDetail.getConsigneeAddress();
                        commodityDescription = documentDetail.getCommodityDescription();
                    }
                }
                if (shipper != null) {
                    parameters.put("shipperName", shipper.getPartyName() != null ? shipper.getPartyName() : "");
                }
                if (shipper != null) {
                    parameters.put("shipmentOrForwardedName", shipper.getPartyName() != null ? shipper.getPartyName() : "");
                }
                List<String> shipperAddressList = reportUtil.addressMasterToGenericStringList(shipperAddress);
                int shipperAddrIndex = 0;
                if (shipperAddressList != null && !shipperAddressList.isEmpty()) {
                    for (String shipAddr : shipperAddressList) {
                        shipperAddrIndex++;
                        if (shipperAddrIndex > 3) {
                            parameters.put("shipperAddressLine3", parameters.get("shipperAddressLine3") + " " + shipAddr);
                        } else {
                            parameters.put("shipperAddressLine" + shipperAddrIndex, shipAddr);
                        }
                    }
                }
                if (shipperAddrIndex < 3) {
                    for (int i = shipperAddrIndex; i < 3; i++) {
                        shipperAddrIndex++;
                        parameters.put("shipperAddressLine" + shipperAddrIndex, "");
                    }
                }

                if (consignee != null) {
                    parameters.put("consigneeName", consignee.getPartyName() != null ? consignee.getPartyName() : "");
                }
                List<String> consigneeAddressList = reportUtil.addressMasterToGenericStringList(consigneeAddress);
                int consigneeAddrIndex = 0;
                if (consigneeAddressList != null && !consigneeAddressList.isEmpty()) {
                    for (String shipAddr : consigneeAddressList) {
                        consigneeAddrIndex++;
                        if (consigneeAddrIndex > 3) {
                            parameters.put("consigneeAddressLine3", parameters.get("consigneeAddressLine3") + " " + shipAddr);
                        } else {
                            parameters.put("consigneeAddressLine" + consigneeAddrIndex, shipAddr);
                        }
                    }
                }
                if (consigneeAddrIndex < 3) {
                    for (int i = consigneeAddrIndex; i < 3; i++) {
                        consigneeAddrIndex++;
                        parameters.put("consigneeAddressLine" + consigneeAddrIndex, "");
                    }
                }
                List<String> forwarderAddressList = reportUtil.addressMasterToGenericStringList(shipperAddress);
                int shipmentOrForwardedAddrIndex = 0;
                if (forwarderAddressList != null && !forwarderAddressList.isEmpty()) {
                    for (String shipAddr : forwarderAddressList) {
                        shipmentOrForwardedAddrIndex++;
                        if (shipmentOrForwardedAddrIndex > 3) {
                            parameters.put("shipmentOrForwardedAddressLine3", parameters.get("shipmentOrForwardedAddressLine3") + " " + shipAddr);
                        } else {
                            parameters.put("shipmentOrForwardedAddressLine" + shipmentOrForwardedAddrIndex, shipAddr);
                        }
                    }
                }
                if (shipmentOrForwardedAddrIndex < 3) {
                    for (int i = shipmentOrForwardedAddrIndex; i < 3; i++) {
                        shipmentOrForwardedAddrIndex++;
                        parameters.put("shipmentOrForwardedAddressLine" + shipmentOrForwardedAddrIndex, "");
                    }
                }
                PortMaster destination = shipmentServiceDetailEntity.getDestination();
                if (destination != null) {
                    parameters.put("destination", destination.getPortName() != null ? destination.getPortName() : "");
                }
                parameters.put("currency", shipmentServiceDetailEntity.getLocalCurrencyCode() != null ? shipmentServiceDetailEntity.getLocalCurrencyCode() : "");

                List<CommercialInvoice> invoiceList = new ArrayList<>();

                CommercialInvoice cInvoice = new CommercialInvoice();


                cInvoice.setDescription(commodityDescription);
                String packName = "";
                String qty = "";
                if (shipmentServiceDetailEntity.getPackMaster() != null) {
                    packName = shipmentServiceDetailEntity.getPackMaster().getPackCode();
                }
                if (shipmentServiceDetailEntity.getBookedPieces() != null) {
                    qty = shipmentServiceDetailEntity.getBookedPieces().toString();
                }
                cInvoice.setQty(qty + " " + packName);

                if (shipmentServiceDetailEntity.getBookedVolumeWeightUnitKg() != null) {
                    cInvoice.setVolume(AppUtil.getWeightFormat(shipmentServiceDetailEntity.getBookedVolumeWeightUnitKg(), shipmentServiceDetailEntity.getCountry().getLocale()));
                }
                if (shipmentServiceDetailEntity.getBookedGrossWeightUnitKg() != null) {
                    cInvoice.setWeight(AppUtil.getWeightFormat(shipmentServiceDetailEntity.getBookedGrossWeightUnitKg(), shipmentServiceDetailEntity.getCountry().getLocale()));
                }
                invoiceList.add(cInvoice);

                parameters.put("commodityDimList", invoiceList);

                JasperReport jasperReport = JasperCompileManager.compileReport(appResourcePath + "/efs-report/commercial_invoice.jrxml");

                List<Object> fieldCollection1 = new ArrayList<>();
                Object obj1 = new Object();
                fieldCollection1.add(obj1);
                JRDataSource dataSource = new JRBeanCollectionDataSource(fieldCollection1, false);
                return JasperFillManager.fillReport(jasperReport, parameters, dataSource);

            } catch (Exception e) {
                log.error("Exception inCommercial Invoice : ", e);
                baseDto.setResponseCode(ErrorCode.FAILED);
            }
        }
        return null;
    }
}

