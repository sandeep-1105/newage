package com.efreightsuite.report.service;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletResponse;

import com.efreightsuite.dto.AutoAttachmentDto;
import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.ReportDownloadRequestDto;
import com.efreightsuite.enumeration.PPCC;
import com.efreightsuite.enumeration.ReportDownloadOption;
import com.efreightsuite.enumeration.ReportName;
import com.efreightsuite.enumeration.YesNo;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.model.AddressMaster;
import com.efreightsuite.model.Consol;
import com.efreightsuite.model.DocumentDetail;
import com.efreightsuite.model.LocationMaster;
import com.efreightsuite.model.LogoMaster;
import com.efreightsuite.model.ShipmentLink;
import com.efreightsuite.model.ShipmentServiceDetail;
import com.efreightsuite.model.UserProfile;
import com.efreightsuite.report.ExportJobCard;
import com.efreightsuite.service.AuthService;
import com.efreightsuite.util.AppUtil;
import lombok.extern.log4j.Log4j2;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

import org.springframework.stereotype.Service;

@Service
@Log4j2
public class ExportJobCardMawbReportService extends AbstractReportService {
    public ReportName getReportType() {
        return ReportName.EXPORT_JOB_CARD;
    }

    public byte[] processReport(ReportDownloadRequestDto data, HttpServletResponse response, boolean isMultiple) {
        log.info("I am in ExportJobCardMawbReportService..");
        BaseDto baseDto = new BaseDto();
        UserProfile logInUSer = AuthService.getCurrentUser();
        JasperPrint jasperPrint = getReport(baseDto, data, AuthService.getCurrentUser());

        byte[] a = null;
        AutoAttachmentDto autoAttachmentDto = reportUtil.autoAttachment(ReportName.EXPORT_JOB_CARD, YesNo.Yes, data.getResourceId(), "MasterShipment");
        if (data.getSingle() != null && data.getSingle() == true) {
            if (data.getDownloadOption() == ReportDownloadOption.Preview || data.getDownloadOption() == ReportDownloadOption.Print) {
                reportUtil.japerPrintToResponseOutputStream(data.getDownloadFileType(), jasperPrint, "EXPORT_JOB_CARD", response, false, null);
            } else if (autoAttachmentDto != null && autoAttachmentDto.getIsAttach() != null || data.getDownloadOption() == ReportDownloadOption.Download) {
                reportUtil.japerPrintToResponseOutputStream(data.getDownloadFileType(), jasperPrint, "EXPORT_JOB_CARD", response, false, autoAttachmentDto);

            }
        }
        if (data.getSingle() != null && data.getSingle() == true && data.getDownloadOption() == ReportDownloadOption.eMail) {
            a = reportUtil.japerPrintToResponseOutputStream(data.getDownloadFileType(), jasperPrint, "EXPORT_JOB_CARD", response, false, null);
            consolReportMailer.sendConsolMailToCustomer(data.getResourceId(), logInUSer, ReportName.EXPORT_JOB_CARD, a);
        }
        if (isMultiple == true && data.getDownloadOption() == ReportDownloadOption.eMail) {
            a = reportUtil.japerPrintToResponseOutputStream(data.getDownloadFileType(), jasperPrint, "EXPORT_JOB_CARD", response, true, null);
            return a;
        }
        return null;
    }

    private JasperPrint getReport(BaseDto baseDto, ReportDownloadRequestDto data, UserProfile loggedInUser) {
        log.info("I am in Export Job Card ReportService.." + data.getResourceId() + "function called");
        Consol consolEntity = consolRepository.findById(data.getResourceId());
        Map<String, Object> parameters = new HashMap<>();
        if (consolEntity != null) {
            try {
                if (consolEntity.getConsolDocument() != null) {
                    parameters.put("mawbNo", consolEntity.getConsolDocument().getMawbNo() != null ? consolEntity.getConsolDocument().getMawbNo() : "");
                    if (consolEntity.getConsolDocument().getConsignee() != null && consolEntity.getConsolDocument().getConsignee().getPartyName() != null) {
                        parameters.put("consigneeName", reportUtil.removeSpecialCharacters(consolEntity.getConsolDocument().getConsignee().getPartyName(), data.getDownloadOption()));
                    }
                    if (consolEntity.getConsolDocument().getShipper() != null && consolEntity.getConsolDocument().getShipper().getPartyName() != null) {
                        parameters.put("shipperName", reportUtil.removeSpecialCharacters(consolEntity.getConsolDocument().getShipper().getPartyName(), data.getDownloadOption()));
                    }
                    if (consolEntity.getConsolDocument().getShipperAddress() != null) {
                        parameters.putAll(AddressMasterToMap("shipperAddressLine", consolEntity.getConsolDocument().getShipperAddress()));
                    }
                    if (consolEntity.getConsolDocument().getConsigneeAddress() != null) {
                        parameters.putAll(AddressMasterToMap("consigneeAddressLine", consolEntity.getConsolDocument().getConsigneeAddress()));
                    }

                    parameters.put("noOfPieces", consolEntity.getConsolDocument().getNoOfPieces() != null ? AppUtil.getNumberFormat(consolEntity.getConsolDocument().getNoOfPieces(), consolEntity.getCountry().getLocale()) : "");
                    parameters.put("grossWeight", consolEntity.getConsolDocument().getGrossWeight() != null ? AppUtil.getWeightFormat(consolEntity.getConsolDocument().getGrossWeight(), consolEntity.getCountry().getLocale()) + " KG " : "");
                    parameters.put("chargeableWeight", consolEntity.getConsolDocument().getChargebleWeight() != null ? AppUtil.getWeightFormat(consolEntity.getConsolDocument().getChargebleWeight(), consolEntity.getCountry().getLocale()) + " KG" : "");
                    parameters.put("volume", consolEntity.getConsolDocument().getVolumeWeight() != null ? AppUtil.getWeightFormat(consolEntity.getConsolDocument().getVolumeWeight(), consolEntity.getCountry().getLocale()) + " KG" : "");
                }
                parameters.put("jobNumber", consolEntity.getConsolUid() != null ? consolEntity.getConsolUid() : "");
                if (consolEntity.getConsolDocument() != null) {
                    parameters.put("commodityName", consolEntity.getConsolDocument().getCommodityDescription() != null ? consolEntity.getConsolDocument().getCommodityDescription() : "");
                }
                if (consolEntity.getOrigin() != null) {
                    parameters.put("originName", consolEntity.getOrigin().getPortName() != null ? consolEntity.getOrigin().getPortName() : "");
                }
                if (consolEntity.getDestination() != null && consolEntity.getDestination().getPortName() != null) {
                    parameters.put("destination", consolEntity.getDestination().getPortName());
                }

                String todayDate = "";
                String flightDate = "";
                String flightNo = "";
                if (loggedInUser != null && loggedInUser.getSelectedUserLocation() != null
                        && consolEntity.getSystemTrack() != null
                        && consolEntity.getSystemTrack().getCreateDate() != null) {
                    parameters.put("reportDate", reportUtil.dateToReportDate(consolEntity.getSystemTrack().getCreateDate(), loggedInUser.getSelectedUserLocation().getJavaDateFormat()));

                    todayDate = reportUtil.dateToReportDate(new Date(), loggedInUser.getSelectedUserLocation().getJavaDateFormat());
                    if (todayDate != null) {
                        parameters.put("refDate", todayDate);
                    } else {
                        parameters.put("refDate", "");
                    }

                    flightDate = reportUtil.dateToReportDate(consolEntity.getSystemTrack().getCreateDate(), loggedInUser.getSelectedUserLocation().getJavaDateFormat());

                }
                if (consolEntity.getConsolDocument() != null) {
                    flightNo = consolEntity.getConsolDocument().getRouteNo() != null ? consolEntity.getConsolDocument().getRouteNo() : "";
                }
                if (flightNo.length() > 0 && flightDate.length() > 0) {
                    parameters.put("flightDate", flightNo + "/" + flightDate);
                } else if (flightNo.length() > 0) {
                    parameters.put("flightDate", flightNo);

                } else if (flightDate.length() > 0) {
                    parameters.put("flightDate", flightDate);
                }

                String loggedInUserName = "";
                String loggedInLocation = "";
                String companyName = "";

                if (loggedInUser != null) {
                    if (loggedInUser.getEmployee() != null && loggedInUser.getEmployee().getEmployeeName() != null) {
                        loggedInUserName = loggedInUser.getEmployee().getEmployeeName();
                    } else if (loggedInUser.getUserName() != null) {
                        loggedInUserName = loggedInUser.getUserName();
                    } else {
                        loggedInUserName = "";
                    }

                    if (loggedInUser.getSelectedUserLocation() != null) {
                        loggedInLocation = loggedInUser.getSelectedUserLocation().getLocationName() != null
                                ? loggedInUser.getSelectedUserLocation().getLocationName() : "";
                        LocationMaster locationMaster = locationMasterRepository.findByLocationCode(loggedInUser.getSelectedUserLocation().getLocationCode());
                        if (locationMaster != null) {
                            parameters.put("companyAddressLine1", locationMaster.getAddressLine1() != null ? locationMaster.getAddressLine1() : "");
                            parameters.put("companyAddressLine2", locationMaster.getAddressLine2() != null ? locationMaster.getAddressLine2() : "");
                            parameters.put("companyAddressLine3", locationMaster.getAddressLine3() != null ? locationMaster.getAddressLine3() : "");
                            companyName = locationMaster.getBranchName() != null ? locationMaster.getBranchName() : "";
                            parameters.put("companyName", reportUtil.removeSpecialCharacters(companyName, data.getDownloadOption()));
                        }

                        LogoMaster logoMaster = logoMasterRepository.findByLocationMaster(loggedInUser.getSelectedUserLocation());
                        try {
                            if (logoMaster != null && logoMaster.getLogo() != null) {
                                InputStream in = new ByteArrayInputStream(logoMaster.getLogo());
                                BufferedImage bImageFromConvert = ImageIO.read(in);
                                parameters.put("logo", bImageFromConvert);
                                log.info("Image getting success::");
                            }
                        } catch (Exception ex) {
                            log.error("Image getting failed:::", ex);
                        }

                    } else {
                        loggedInLocation = "";
                    }
                }

                parameters.put("refPlace", loggedInUserName + "-" + loggedInLocation);
                parameters.put("phoneNumber", "");
                parameters.put("reportName", "EXPORT JOB CARD (MAWB)");

                String destinationName = "";
                String shipperName = "";
                List<ExportJobCard> exportjobCardList = new ArrayList<>();

                double totalGrossWeight = 0.0;
                double totalChargeableWeight = 0.0;
                long totalQty = 0;
                if (consolEntity.getShipmentLinkList() != null && !consolEntity.getShipmentLinkList().isEmpty()) {
                    for (ShipmentLink shipmentLink : consolEntity.getShipmentLinkList()) {
                        ShipmentServiceDetail service = shipmentLink.getService();
                        for (DocumentDetail document : service.getDocumentList()) {
                            ExportJobCard exportJobCard = new ExportJobCard();
                            if (document != null && document.getDestination() != null) {

                                destinationName = document.getDestination().getPortName() != null ? consolEntity.getDestination().getPortName() : "";
                                exportJobCard.setDestination(destinationName);
                            }
                            if (document != null && document.getShipper() != null) {
                                shipperName = document.getShipper().getPartyName() != null ? document.getShipper().getPartyName() : "";
                                exportJobCard.setShipper(shipperName);
                            }
                            double grossWeight = 0.0;
                            if (document != null && document.getGrossWeight() != null) {
                                grossWeight = document.getGrossWeight();
                                //exportJobCard.setGrossWeight( reportUtil.twoDecimalDoubleValueFormat(grossWeight));
                                exportJobCard.setGrossWeight(AppUtil.getWeightFormat(grossWeight, service.getCountry().getLocale()));
                                totalGrossWeight += grossWeight;
                            }

                            double chargeWeight = 0.0;
                            if (document != null && document.getChargebleWeight() != null) {
                                chargeWeight = document.getChargebleWeight();
                                exportJobCard.setChargeableWeight(AppUtil.getWeightFormat(chargeWeight, service.getCountry().getLocale()));
                                totalChargeableWeight += chargeWeight;
                            }
                            long qty = 0;
                            if (document != null && document.getNoOfPieces() != null) {
                                exportJobCard.setQty(AppUtil.getNumberFormat(document.getNoOfPieces(), service.getCountry().getLocale()));
                                qty = document.getNoOfPieces();
                                totalQty += qty;
                            }
                            if (document != null) {
                                exportJobCard.setHawbNumber(document.getDocumentNo() != null ? document.getDocumentNo() : "");
                            }
                            if (service.getPpcc().equals(PPCC.Collect)) {
                                exportJobCard.setPpcc("Collect");
                            } else {
                                exportJobCard.setPpcc("Prepaid");
                            }
                            exportjobCardList.add(exportJobCard);
                        }
                        parameters.put("totalChargeableWeight", AppUtil.getWeightFormat(totalChargeableWeight, service.getCountry().getLocale()));
                        parameters.put("totalGrossWeight", AppUtil.getWeightFormat(totalGrossWeight, service.getCountry().getLocale()));
                        parameters.put("totalQty", (AppUtil.getNumberFormat(totalQty, service.getCountry().getLocale())));
                    }

                }
                parameters.put("exportjobCardList", exportjobCardList);

                JasperReport jasperReport = JasperCompileManager.compileReport(appResourcePath + "/efs-report/export_job_card_mawb.jrxml");
                List<Object> fieldCollection1 = new ArrayList<>();
                Object obj1 = new Object();
                fieldCollection1.add(obj1);
                JRDataSource dataSource = new JRBeanCollectionDataSource(fieldCollection1, false);
                return JasperFillManager.fillReport(jasperReport, parameters, dataSource);


            } catch (RestException re) {
                log.error("RestException occures in CExport Job card  method  : ", re);
            } catch (Exception exception) {
                log.error("RestException occures in Export Job card  :: ", exception);
                baseDto.setResponseCode(ErrorCode.FAILED);
            }
            log.info("Export Job card   [" + data.getResourceId() + "] method ended");
        }
        return null;

    }

    public Map<String, String> AddressMasterToMap(String parameterName, AddressMaster addressMaster) {
        Map<String, String> parameter = new HashMap<>();
        int index = 1;
        if (addressMaster != null) {
            if (addressMaster.getAddressLine1() != null && addressMaster.getAddressLine1().trim().length() > 0) {
                parameter.put(parameterName + index, addressMaster.getAddressLine1());
                index++;
            }
            if (addressMaster.getAddressLine2() != null && addressMaster.getAddressLine2().trim().length() > 0) {
                parameter.put(parameterName + index, addressMaster.getAddressLine2());
                index++;
            }
            if (addressMaster.getAddressLine3() != null && addressMaster.getAddressLine3().trim().length() > 0) {
                parameter.put(parameterName + index, addressMaster.getAddressLine3());
                index++;
            }
            if (addressMaster.getAddressLine4() != null && addressMaster.getAddressLine4().trim().length() > 0) {
                parameter.put(parameterName + index, addressMaster.getAddressLine4());
                index++;
            }
        }
        return parameter;
    }
}
