package com.efreightsuite.report.service;

import javax.servlet.http.HttpServletResponse;

import com.efreightsuite.dto.ReportDownloadRequestDto;
import com.efreightsuite.enumeration.ReportName;
import com.efreightsuite.report.ConvertNumberToWord;
import com.efreightsuite.repository.*;
import com.efreightsuite.search.CurrencyRateSearchImpl;
import com.efreightsuite.service.AuthService;
import com.efreightsuite.service.ReportMasterService;
import com.efreightsuite.service.SequenceGeneratorService;
import com.efreightsuite.service.mailer.ConsolReportMailer;
import com.efreightsuite.service.mailer.CreditNoteSingleReportMailer;
import com.efreightsuite.service.mailer.InvoiceSingleReportMailer;
import com.efreightsuite.service.mailer.QuotationReportMailer;
import com.efreightsuite.service.mailer.ShipmentReportMailer;
import com.efreightsuite.util.AppUtil;
import com.efreightsuite.util.ReportUtil;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public abstract class AbstractReportService {

    @Autowired
    AppUtil appUtil;

    @Autowired
    QuotationRepository quotationRepository;

    @Autowired
    LogoMasterRepository logoMasterRepository;

    @Autowired
    DocumentDetailRepository documentDetailRepository;

    @Autowired
    DefaultMasterDataRepository defaultMasterDataRepository;

    @Autowired
    ShipmentServiceDetailRepository shipmentServiceDetailRepository;

    @Autowired
    ShipmentRepository shipmentRepository;

    @Autowired
    ReportConfigurationMasterRepository reportConfigurationMasterRepository;

    @Autowired
    ConsolDocumentRepository consolDocumentRepository;

    @Autowired
    ShipmentChargeRepository shipmentChargeRepository;

    @Autowired
    EnquiryDetailRepository enquiryDetailRepository;

    @Value("${efs.resource.path}")
    String appResourcePath;

    @Value("${efs.application.base.url}")         //for ApproveLink in Quotations
            String applicationBaseUrl;


    @Autowired
    ReportUtil reportUtil;

    @Autowired
    CurrencyRateSearchImpl currencyRateSearchImpl;

    @Autowired
    CurrencyMasterRepository currencyMasterRepository;

    @Autowired
    ConvertNumberToWord convertNumberToWord;


    @Autowired
    CurrencyRateMasterRepository currencyRateMasterRepository;

    @Autowired
    ConsolRepository consolRepository;

    @Autowired
    ServiceDocumentDimensionRepository serviceDocumentDimensionRepository;

    @Autowired
    InvoiceCreditNoteRepository invoiceCreditNoteRepository;

    @Autowired
    LocationMasterRepository locationMasterRepository;

    @Autowired
    CfsReceiveEntryRepository cfsReceiveEntryRepository;

    @Autowired
    ProvisionalRepository provisionalRepository;

    @Autowired
    ReferenceTypeMasterRepository referenceTypeMasterRepository;

    @Autowired
    PartyAddressMasterRepository partyAddressMasterRepository;

    @Autowired
    SequenceGeneratorService sequenceGeneratorService;

    @Autowired
    ReportMasterService reportMasterService;

    @Autowired
    ConsolDocumentDimensionRepository consolDocumentDimensionRepository;

    @Autowired
    ReportMasterRepository reportMasterRepository;

    @Autowired
    AuthService authService;

    @Autowired
    @org.springframework.context.annotation.Lazy
    ShipmentReportMailer shipmentReportMailer;

    @Autowired
    @org.springframework.context.annotation.Lazy
    ConsolReportMailer consolReportMailer;

    @Autowired
    @org.springframework.context.annotation.Lazy
    QuotationReportMailer quotationReportMailer;

    @Autowired
    @org.springframework.context.annotation.Lazy
    InvoiceSingleReportMailer invoiceSingleReportMailer;

    @Autowired
    @org.springframework.context.annotation.Lazy
    CreditNoteSingleReportMailer creditNoteSingleReportMailer;

    @Autowired
    DynamicFieldRepository dynamicFieldRepository;

    @Autowired
    QuotationDetailRepository quotationDetailRepository;
    @Autowired
    CountryReportConfigRepository countryReportConfigRepository;      //for country config pages dynamically

    public abstract byte[] processReport(ReportDownloadRequestDto data, HttpServletResponse response, boolean value);

    //public abstract byte[] processReport(ReportDownloadRequestDto data, boolean value);
    public abstract ReportName getReportType();


}
