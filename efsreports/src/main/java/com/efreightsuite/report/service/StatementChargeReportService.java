package com.efreightsuite.report.service;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletResponse;

import com.efreightsuite.dto.AutoAttachmentDto;
import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.ReportDownloadRequestDto;
import com.efreightsuite.enumeration.ImportExport;
import com.efreightsuite.enumeration.PPCC;
import com.efreightsuite.enumeration.ReportDownloadOption;
import com.efreightsuite.enumeration.ReportName;
import com.efreightsuite.enumeration.YesNo;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.model.AddressMaster;
import com.efreightsuite.model.DocumentDetail;
import com.efreightsuite.model.LocationMaster;
import com.efreightsuite.model.LogoMaster;
import com.efreightsuite.model.PartyMaster;
import com.efreightsuite.model.PortMaster;
import com.efreightsuite.model.ProvisionItem;
import com.efreightsuite.model.Provisional;
import com.efreightsuite.model.ServiceMaster;
import com.efreightsuite.model.ShipmentCharge;
import com.efreightsuite.model.ShipmentServiceDetail;
import com.efreightsuite.model.UserProfile;
import com.efreightsuite.report.PrepaidCharges;
import com.efreightsuite.service.AuthService;
import com.efreightsuite.util.AppUtil;
import com.efreightsuite.util.NumberToWords;
import lombok.extern.log4j.Log4j2;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

import org.springframework.stereotype.Service;

@Service
@Log4j2
public class StatementChargeReportService extends AbstractReportService {
    public ReportName getReportType() {
        return ReportName.STATEMENT_OF_CHARGE;
    }

    public byte[] processReport(ReportDownloadRequestDto data, HttpServletResponse response, boolean isMultiple) {
        log.info("I am in StatementChargeReportService......");
        BaseDto baseDto = new BaseDto();
        UserProfile logInUSer = AuthService.getCurrentUser();
        JasperPrint jasperPrint = getReport(baseDto, data, AuthService.getCurrentUser());

        byte[] a = null;
        AutoAttachmentDto autoAttachmentDto = reportUtil.autoAttachment(ReportName.STATEMENT_OF_CHARGE, YesNo.Yes, data.getResourceId(), "Shipment");
        if (data.getSingle() != null && data.getSingle() == true) {
            if (data.getDownloadOption() == ReportDownloadOption.Preview || data.getDownloadOption() == ReportDownloadOption.Print) {
                reportUtil.japerPrintToResponseOutputStream(data.getDownloadFileType(), jasperPrint, "STATEMENT_OF_CHARGE", response, false, null);
            } else if (autoAttachmentDto != null && autoAttachmentDto.getIsAttach() != null || data.getDownloadOption() == ReportDownloadOption.Download) {
                reportUtil.japerPrintToResponseOutputStream(data.getDownloadFileType(), jasperPrint, "STATEMENT_OF_CHARGE", response, false, autoAttachmentDto);
            }
        }
        if (data.getSingle() != null && data.getSingle() == true && data.getDownloadOption() == ReportDownloadOption.eMail) {
            a = reportUtil.japerPrintToResponseOutputStream(data.getDownloadFileType(), jasperPrint, "STATEMENT_OF_CHARGE", response, false, null);
            shipmentReportMailer.sendShipmentMailToCustomer(data.getResourceId(), logInUSer, ReportName.STATEMENT_OF_CHARGE, a);
        }
        if (isMultiple == true && data.getDownloadOption() == ReportDownloadOption.eMail) {
            a = reportUtil.japerPrintToResponseOutputStream(data.getDownloadFileType(), jasperPrint, "STATEMENT_OF_CHARGE", response, true, null);
            return a;
        }
        return null;
    }

    private JasperPrint getReport(BaseDto baseDto, ReportDownloadRequestDto data, UserProfile loggedInUser) {
        log.info("Statement of Charge with Id::: [" + data.getResourceId() + "] called");
        ShipmentServiceDetail shipmentServiceDetail = shipmentServiceDetailRepository.findByIdForShipmentServiceDetail(data.getResourceId());
        if (shipmentServiceDetail != null) {
            Map<String, Object> parameters = new HashMap<>();
            try {
                PortMaster podName = shipmentServiceDetail.getPod();
                if (podName != null) {
                    parameters.put("pod", podName.getPortName());
                }
                PortMaster polName = shipmentServiceDetail.getPol();
                if (podName != null) {
                    parameters.put("pol", polName.getPortName());
                }
                PortMaster destination = shipmentServiceDetail.getDestination();
                if (destination != null) {
                    parameters.put("fdc", destination.getPortName());
                }
                PartyMaster shipper = null;
                AddressMaster shipperAddress = null;
                PartyMaster consignee = null;
                AddressMaster forwarderAddress = null;
                PartyMaster forwarder = null;
                if (shipmentServiceDetail.getDocumentList() != null
                        && !shipmentServiceDetail.getDocumentList().isEmpty()) {
                    DocumentDetail documentDetail = shipmentServiceDetail.getDocumentList().get(0);
                    if (documentDetail != null) {
                        shipper = documentDetail.getShipper();
                        shipperAddress = documentDetail.getShipperAddress();
                        consignee = documentDetail.getConsignee();
                        forwarderAddress = documentDetail.getForwarderAddress();
                        forwarder = documentDetail.getShipper();

                    }
                }

                //NEW SHIPMENT COMMENTS
                if (shipmentServiceDetail.getShipment() != null && shipmentServiceDetail.getCommodity() != null) {
                    parameters.put("commodityDescription", shipmentServiceDetail.getCommodity().getHsName() != null ? shipmentServiceDetail.getCommodity().getHsName() : "");
                }
                if (shipmentServiceDetail.getShipment() != null) {
                    parameters.put("bookingNo", shipmentServiceDetail.getShipment().getShipmentUid() != null ? shipmentServiceDetail.getShipment().getShipmentUid() : "");
                }
                if (shipmentServiceDetail.getCountry() != null) {
                    if (shipmentServiceDetail != null) {
                        parameters.put("noOfPieces", AppUtil.getNumberFormat(shipmentServiceDetail.getBookedPieces(), shipmentServiceDetail.getCountry().getLocale()));
                    }
                    if (shipmentServiceDetail.getBookedGrossWeightUnitKg() != null) {
                        parameters.put("weight", AppUtil.getWeightFormat(shipmentServiceDetail.getBookedGrossWeightUnitKg(), shipmentServiceDetail.getCountry().getLocale()) + " KG");
                    }
                    if (shipmentServiceDetail.getBookedVolumeWeightUnitKg() != null) {
                        parameters.put("volume", AppUtil.getWeightFormat(shipmentServiceDetail.getBookedVolumeWeightUnitKg(), shipmentServiceDetail.getCountry().getLocale()) + " KG");
                    }
                }

                ServiceMaster serviceMaster = shipmentServiceDetail.getServiceMaster();
                if (serviceMaster != null && serviceMaster.getImportExport().equals(ImportExport.Export)) {
                    if (forwarder != null && forwarder.getPartyName() != null) {
                        parameters.put("forwarderName", reportUtil.removeSpecialCharacters(shipper.getPartyName(), data.getDownloadOption()));
                    }
                    List<String> shipperAddressList = reportUtil.addressMasterToGenericStringList(shipperAddress);
                    int shipperAddrIndex = 0;
                    if (shipperAddressList != null && !shipperAddressList.isEmpty()) {
                        for (String shipAddr : shipperAddressList) {
                            shipperAddrIndex++;
                            if (shipperAddrIndex > 2) {
                                parameters.put("forwarderAddressLine2", parameters.get("forwarderAddressLine2") + " " + shipAddr);
                                log.info(" If Forwarder Addrss Line:::::::" + shipAddr);
                            } else {
                                parameters.put("forwarderAddressLine" + shipperAddrIndex, shipAddr);
                                log.info("Forwarder Addrss Line:::::::" + shipAddr);
                            }
                        }
                    }
                    if (shipperAddrIndex < 2) {
                        for (int i = shipperAddrIndex; i < 2; i++) {
                            shipperAddrIndex++;
                            parameters.put("forwarderAddressLine" + shipperAddrIndex, "");
                        }
                    }
                } else if (serviceMaster != null && serviceMaster.getImportExport().equals(ImportExport.Import)) {
                    if (forwarder != null && forwarder.getPartyName() != null) {
                        parameters.put("forwarderName", reportUtil.removeSpecialCharacters(forwarder.getPartyName(), data.getDownloadOption()));
                    }
                    List<String> shipperAddressList = reportUtil.addressMasterToGenericStringList(forwarderAddress);
                    int shipperAddrIndex = 0;
                    if (shipperAddressList != null && !shipperAddressList.isEmpty()) {
                        for (String shipAddr : shipperAddressList) {
                            shipperAddrIndex++;
                            if (shipperAddrIndex > 3) {
                                parameters.put("forwarderAddressLine2", parameters.get("forwarderAddressLine2") + " " + shipAddr);
                            } else {
                                parameters.put("forwarderAddressLine" + shipperAddrIndex, shipAddr);
                            }
                        }
                    }
                    if (shipperAddrIndex < 3) {
                        for (int i = shipperAddrIndex; i < 3; i++) {
                            shipperAddrIndex++;
                            parameters.put("forwarderAddressLine" + shipperAddrIndex, "");
                        }
                    }
                }

                if (consignee != null && consignee.getPartyName() != null) {
                    parameters.put("consigneeName", reportUtil.removeSpecialCharacters(consignee.getPartyName(), data.getDownloadOption()));
                }
                if (shipper != null && shipper.getPartyName() != null) {
                    parameters.put("shipperName", reportUtil.removeSpecialCharacters(shipper.getPartyName(), data.getDownloadOption()));
                }
                parameters.put("flightName", shipmentServiceDetail.getCarrier() != null ? shipmentServiceDetail.getCarrier().getCarrierName() : "");
                parameters.put("flightNo", shipmentServiceDetail.getRouteNo() != null ? shipmentServiceDetail.getRouteNo() : "");
                parameters.put("hblNumber", shipmentServiceDetail.getShipment().getShipmentUid() != null ? shipmentServiceDetail.getShipment().getShipmentUid() : "");

                String loggedInUserName = "";
                String loggedInLocation = "";
                String companyName = "";
                if (loggedInUser != null) {
                    if (loggedInUser.getEmployee() != null && loggedInUser.getEmployee().getEmployeeName() != null) {
                        loggedInUserName = loggedInUser.getEmployee().getEmployeeName();

                    } else if (loggedInUser.getUserName() != null) {
                        loggedInUserName = loggedInUser.getUserName();
                    } else {
                        loggedInUserName = "";
                    }

                    if (loggedInUser.getSelectedUserLocation() != null) {
                        String toDayDate = "";
                        toDayDate = reportUtil.dateToReportDate(new Date(), loggedInUser.getSelectedUserLocation().getJavaDateFormat());
                        parameters.put("shipmentDate", toDayDate);
                        parameters.put("refDate", toDayDate);

                        loggedInLocation = loggedInUser.getSelectedUserLocation().getLocationName() != null
                                ? loggedInUser.getSelectedUserLocation().getLocationName() : "";

                        if (loggedInUser.getSelectedUserLocation() != null) {
                            LocationMaster locationMaster = locationMasterRepository.findByLocationCode(loggedInUser.getSelectedUserLocation().getLocationCode());
                            if (locationMaster != null) {
                                parameters.put("companyAddressLine1", locationMaster.getAddressLine1() != null ? locationMaster.getAddressLine1() : "");
                                parameters.put("companyAddressLine2", locationMaster.getAddressLine2() != null ? locationMaster.getAddressLine2() : "");
                                parameters.put("companyAddressLine3", locationMaster.getAddressLine3() != null ? locationMaster.getAddressLine3() : "");
                                companyName = locationMaster.getBranchName() != null ? locationMaster.getBranchName() : "";
                                parameters.put("companyName", reportUtil.removeSpecialCharacters(companyName, data.getDownloadOption()));
                            }
                            LogoMaster logoMaster = logoMasterRepository.findByLocationMaster(loggedInUser.getSelectedUserLocation());
                            try {
                                if (logoMaster != null && logoMaster.getLogo() != null) {
                                    InputStream in = new ByteArrayInputStream(logoMaster.getLogo());
                                    BufferedImage bImageFromConvert = ImageIO.read(in);
                                    parameters.put("logo", bImageFromConvert);
                                    log.info("Image getting success::");
                                }
                            } catch (Exception ex) {
                                log.error("Image getting failed:::", ex);
                            }

                        }
                    } else {
                        loggedInLocation = "";
                    }
                }
                parameters.put("refPlace", loggedInUserName + "-" + loggedInLocation);
                parameters.put("salesManName", loggedInUserName);
                parameters.put("reportName", "STATEMENT OF CHARGES / PRO-FORMA INVOICE (Shipment)");
                DecimalFormat twoDecFormat = new DecimalFormat("#0.00#");
                parameters.put("companyName", reportUtil.removeSpecialCharacters(companyName, data.getDownloadOption()));
                List<PrepaidCharges> prepaidChargesList = new ArrayList<>();
                List<PrepaidCharges> collectList = new ArrayList<>();
                PrepaidCharges prepaidCharge = new PrepaidCharges();
                Double totalPrepaidAmount = 0.0;
                Double totalCollectAmount = 0.0;
                String rateMerged = appUtil.getLocationConfig("booking.rates.merged", shipmentServiceDetail.getLocation(), true);
                if (rateMerged != null) {
                    if (rateMerged.equals("FALSE") || rateMerged == "FALSE") {
                        if (shipmentServiceDetail.getShipmentChargeList() != null && !shipmentServiceDetail.getShipmentChargeList().isEmpty()) {
                            for (ShipmentCharge shipmentCharge : shipmentServiceDetail.getShipmentChargeList()) {

                                if (shipmentCharge != null) {
                                    Double totalLocalAmount = 0.0;
                                    prepaidCharge = new PrepaidCharges();
                                    prepaidCharge.setChargeName(shipmentCharge.getChargeName() != null ? shipmentCharge.getChargeName() : "");
                                    if (shipmentCharge.getNetSaleAmount() != null) {
                                        prepaidCharge.setUnits(AppUtil.getCurrencyFormat(shipmentCharge.getNetSaleAmount(), shipmentCharge.getNetSaleCurrency().getCountryMaster().getLocale(), shipmentCharge.getNetSaleCurrency().getDecimalPoint()));
                                    }
                                    if (shipmentCharge.getNetSaleCurrency() != null) {
                                        prepaidCharge.setCurrency(shipmentCharge.getNetSaleCurrency().getCurrencyCode() != null ? shipmentCharge.getNetSaleCurrency().getCurrencyCode() : "");
                                    }
                                    if (shipmentCharge.getLocalNetSaleRoe() != null) {
                                        prepaidCharge.setExRate(AppUtil.getCurrencyFormat(shipmentCharge.getLocalNetSaleRoe(), shipmentCharge.getNetSaleCurrency().getCountryMaster().getLocale(), shipmentCharge.getNetSaleCurrency().getDecimalPoint()));
                                    }

                                    if (shipmentCharge.getTotalNetSale() != null) {
                                        prepaidCharge.setAmount(AppUtil.getCurrencyFormat(shipmentCharge.getTotalNetSale(), shipmentCharge.getNetSaleCurrency().getCountryMaster().getLocale(), shipmentCharge.getNetSaleCurrency().getDecimalPoint()));
                                    }
                                    if (shipmentCharge.getLocalTotalNetSale() != null) {
                                        totalLocalAmount = shipmentCharge.getLocalTotalNetSale();
                                        prepaidCharge.setTotalLocalAmount(AppUtil.getCurrencyFormat(shipmentCharge.getLocalTotalNetSale(), shipmentServiceDetail.getCountry().getLocale(), shipmentServiceDetail.getLocalCurrency().getDecimalPoint()));
                                    }
                                    if (shipmentCharge.getPpcc() != null) {
                                        if (shipmentCharge.getPpcc().equals(PPCC.Collect)) {
                                            collectList.add(prepaidCharge);
                                            totalCollectAmount = totalCollectAmount + totalLocalAmount;
                                        }
                                        if (shipmentCharge.getPpcc().equals(PPCC.Prepaid)) {
                                            prepaidChargesList.add(prepaidCharge);
                                            totalPrepaidAmount = totalPrepaidAmount + totalLocalAmount;
                                        }

                                    }
                                }
                            }
                        }
                    } else {
                        if (shipmentServiceDetail.getShipmentChargeList() != null && !shipmentServiceDetail.getShipmentChargeList().isEmpty()) {
                            for (ShipmentCharge shipmentCharge : shipmentServiceDetail.getShipmentChargeList()) {


                                if (shipmentCharge != null) {
                                    Double totalLocalAmount = 0.0;
                                    prepaidCharge = new PrepaidCharges();
                                    prepaidCharge.setChargeName(shipmentCharge.getChargeName() != null ? shipmentCharge.getChargeName() : "");
                                    if (shipmentCharge.getRateAmount() != null) {
                                        prepaidCharge.setUnits(AppUtil.getCurrencyFormat(shipmentCharge.getRateAmount(), shipmentCharge.getRateCurrency().getCountryMaster().getLocale(), shipmentCharge.getRateCurrency().getDecimalPoint()));
                                    }
                                    if (shipmentCharge.getRateCurrency() != null) {
                                        prepaidCharge.setCurrency(shipmentCharge.getRateCurrency().getCurrencyCode() != null ? shipmentCharge.getRateCurrency().getCurrencyCode() : "");
                                    }

                                    if (shipmentCharge.getLocalRateAmountRoe() != null) {
                                        prepaidCharge.setExRate(AppUtil.getCurrencyFormat(shipmentCharge.getLocalRateAmountRoe(), shipmentCharge.getRateCurrency().getCountryMaster().getLocale(), shipmentCharge.getRateCurrency().getDecimalPoint()));
                                    }
                                    prepaidCharge.setExRate(shipmentCharge.getLocalRateAmountRoe().toString());

                                    if (shipmentCharge.getTotalRateAmount() != null) {
                                        prepaidCharge.setAmount(AppUtil.getCurrencyFormat(shipmentCharge.getTotalRateAmount(), shipmentCharge.getRateCurrency().getCountryMaster().getLocale(), shipmentCharge.getRateCurrency().getDecimalPoint()));
                                    }
                                    if (shipmentCharge.getLocalTotalRateAmount() != null) {
                                        totalLocalAmount = shipmentCharge.getLocalTotalRateAmount();
                                        prepaidCharge.setTotalLocalAmount(AppUtil.getCurrencyFormat(shipmentCharge.getLocalTotalRateAmount(), shipmentServiceDetail.getCountry().getLocale(), shipmentServiceDetail.getLocalCurrency().getDecimalPoint()));
                                    }
                                    if (shipmentCharge.getPpcc() != null) {
                                        if (shipmentCharge.getPpcc().equals(PPCC.Collect)) {
                                            collectList.add(prepaidCharge);
                                            totalCollectAmount = totalCollectAmount + totalLocalAmount;
                                        }
                                        if (shipmentCharge.getPpcc().equals(PPCC.Prepaid)) {
                                            prepaidChargesList.add(prepaidCharge);
                                            totalPrepaidAmount = totalPrepaidAmount + totalLocalAmount;
                                        }

                                    }
                                }
                            }
                        }

                    }

                }


                //Provisional charge start here.............
                Provisional provisional = provisionalRepository.findByServiceUid(shipmentServiceDetail.getServiceUid());
                log.info("Inside provisional cost report::::::");
                List<PrepaidCharges> provisionalList = new ArrayList<>();
                if (provisional != null) {
                    if (provisional.getProvisionalItemList() != null && !provisional.getProvisionalItemList().isEmpty()) {

                        for (ProvisionItem provisionItem : provisional.getProvisionalItemList()) {
                            PrepaidCharges pCharge = new PrepaidCharges();
                            if (provisionItem != null) {
                                if (provisionItem.getChargeMaster() != null) {
                                    if (provisionItem.getChargeMaster().getChargeName() != null) {
                                        pCharge.setChargeName(provisionItem.getChargeMaster().getChargeName()); //
                                    }
                                }
                                pCharge.setExRate(provisionItem.getSellRoe() != null
                                        ? AppUtil.getCurrencyFormat(provisionItem.getSellRoe(), provisionItem.getSellCurrency().getCountryMaster().getLocale(),
                                        provisionItem.getSellCurrency().getDecimalPoint()) : "");//
                                pCharge.setAmount(provisionItem.getSellAmountPerUnit() != null
                                        ? AppUtil.getCurrencyFormat(provisionItem.getSellAmountPerUnit(), provisionItem.getSellCurrency().getCountryMaster().getLocale(),
                                        provisionItem.getSellCurrency().getDecimalPoint()) : "");
                                pCharge.setUnits(AppUtil.getCurrencyFormat(provisionItem.getNoOfUnits(),
                                        provisionItem.getSellCurrency().getCountryMaster().getLocale(), provisionItem.getSellCurrency().getDecimalPoint()));
                                if (provisionItem.getSellCurrency() != null) {
                                    pCharge.setCurrency(provisionItem.getSellCurrency().getCurrencyCode());
                                }
                                if (provisionItem.getSellLocalAmount() != null) {
                                    pCharge.setAmount(provisionItem.getSellLocalAmount() != null
                                            ? AppUtil.getCurrencyFormat(provisionItem.getSellLocalAmount(), provisionItem.getSellCurrency().getCountryMaster().getLocale(),
                                            provisionItem.getSellCurrency().getDecimalPoint()) : "");

                                }
                                if (provisionItem.getSellAmount() != null) {
                                    pCharge.setTotalLocalAmount(provisionItem.getSellAmount() != null
                                            ? AppUtil.getCurrencyFormat(provisionItem.getSellAmount(), provisionItem.getSellCurrency().getCountryMaster().getLocale(),
                                            provisionItem.getSellCurrency().getDecimalPoint()) : "");

                                }
                                provisionalList.add(pCharge);
                            }
                        }
                    }

                }

                if (provisionalList.size() > 0) {
                    parameters.put("provisionalList", provisionalList);
                } else {
                    parameters.put("prepaidChargeList", prepaidChargesList);
                    parameters.put("totalPrepaidAmount", AppUtil.getCurrencyFormat(totalPrepaidAmount, shipmentServiceDetail.getCountry().getLocale(), shipmentServiceDetail.getLocalCurrency().getDecimalPoint()));
                    String[] input = twoDecFormat.format(totalPrepaidAmount).toString().split("\\.");
                    String mainValue = input[0];
                    String decimalValue = input[1];
                    if (totalPrepaidAmount > 0.0 && !decimalValue.equals("00")) {
                        String convertedNumbers = NumberToWords.convertPriceIntoString(mainValue, decimalValue, shipmentServiceDetail.getLocalCurrency().getPrefix(), shipmentServiceDetail.getLocalCurrency().getSuffix());
                        parameters.put("convertToequilentReadableWord", convertedNumbers);
                    } else {
                        String convertedNumbers = NumberToWords.convertPriceIntoString(mainValue, "", shipmentServiceDetail.getLocalCurrency().getPrefix(), "");
                        parameters.put("convertToequilentReadableWord", convertedNumbers);
                    }
                    parameters.put("collectList", collectList);
                    parameters.put("totalCollectCharge", AppUtil.getCurrencyFormat(totalCollectAmount, shipmentServiceDetail.getCountry().getLocale(), shipmentServiceDetail.getLocalCurrency().getDecimalPoint()));
                    String[] inputCollect = twoDecFormat.format(totalCollectAmount).toString().split("\\.");
                    log.info("inputCollect String length::::" + inputCollect.length);
                    String mainValueCollect = inputCollect[0];
                    String decimalValueCollect = inputCollect[1];

                    if (totalCollectAmount > 0.0 && !decimalValueCollect.equals("00")) {
                        String convertedNumbersCollect = NumberToWords.convertPriceIntoString(mainValueCollect, decimalValueCollect, shipmentServiceDetail.getLocalCurrency().getPrefix(), shipmentServiceDetail.getLocalCurrency().getSuffix());
                        parameters.put("convertToequilentReadbleCollectChargeWord", convertedNumbersCollect);
                    } else {
                        String convertedNumbersCollect = NumberToWords.convertPriceIntoString(mainValueCollect, "", shipmentServiceDetail.getLocalCurrency().getPrefix(), "");
                        parameters.put("convertToequilentReadbleCollectChargeWord", convertedNumbersCollect);
                    }

                }

                JasperReport jasperReport = JasperCompileManager.compileReport(appResourcePath + "/efs-report/statement_of_charges.jrxml");
                List<Object> fieldCollection1 = new ArrayList<>();
                Object obj1 = new Object();
                fieldCollection1.add(obj1);
                JRDataSource dataSource = new JRBeanCollectionDataSource(fieldCollection1, false);
                return JasperFillManager.fillReport(jasperReport, parameters, dataSource);

            } catch (RestException re) {
                log.error("RestException in Statement of Charge : ", re);
            } catch (Exception exception) {
                log.error("RestException in Statement of Charge :: ", exception);
                baseDto.setResponseCode(ErrorCode.FAILED);
            }
            log.info("Statement of Charge   [" + data.getResourceId() + "] function ended");
        }
        return null;
    }
}
