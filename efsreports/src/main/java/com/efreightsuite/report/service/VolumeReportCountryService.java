package com.efreightsuite.report.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import com.efreightsuite.dto.ReportDownloadRequestDto;
import com.efreightsuite.enumeration.ImportExport;
import com.efreightsuite.enumeration.ReportName;
import com.efreightsuite.enumeration.WhoRouted;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.model.LocationMaster;
import com.efreightsuite.model.ShipmentServiceDetail;
import com.efreightsuite.model.UserProfile;
import com.efreightsuite.report.VolumeReportCountryDto;
import com.efreightsuite.service.AuthService;
import lombok.extern.log4j.Log4j;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

import org.springframework.stereotype.Service;

@Service
@Log4j
public class VolumeReportCountryService extends AbstractReportService {
    public ReportName getReportType() {
        return ReportName.VOLUME_REPORT_BY_COUNTRY;
    }

    public byte[] processReport(ReportDownloadRequestDto data, HttpServletResponse response, boolean isMultiple) {
        log.info("Enter Volume Report By Country Serive.....");
        SimpleDateFormat dt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String fromDate = dt.format(data.getApiFromDate());
        String toDate = dt.format(data.getApiToDate());
        List<ShipmentServiceDetail> serviceList = shipmentServiceDetailRepository.findByAllShipment(data.getServiceCode().getServiceCode(), data.getCountry().getCountryCode(), data.getPort().getPortCode(), fromDate, toDate);
        log.info("ServiceList Object::::::::::;:::" + serviceList);
        Map<String, Object> parameters = new HashMap<>();
        List<VolumeReportCountryDto> volumeReportCountryList = new ArrayList<>();
        try {
            UserProfile loggedInUser = AuthService.getCurrentUser();
            if (serviceList != null && !serviceList.isEmpty()) {
                int inc = 1;
                for (ShipmentServiceDetail service : serviceList) {
                    VolumeReportCountryDto volume = new VolumeReportCountryDto();
                    if (service != null) {
                        Integer totalShipments = 0;
                        Double weights = 0.0;
                        Double weight = 0.0;
                        Double totalWeight = 0.0;
                        String count = "";
                        String totalCount = "";
                        if (service.getWhoRouted().equals(WhoRouted.Self)) {
                            log.info("WhoRouted Name::::::Self Detatils");
                            count = inc + "";
                            Integer shipment = Integer.valueOf(count);
                            totalShipments = totalShipments + shipment;
                            volume.setShipements(shipment.toString());
                            weight = service.getBookedVolumeWeightUnitKg();
                            //weights = weights + weight;
                            weights = weight;
                            volume.setVolumeInKgs(weights.toString());
                            totalWeight = totalWeight + weights;

                        } else if (service.getWhoRouted().equals(WhoRouted.Agent)) {
                            log.info("WhoRouted Name::::::Agent Detatils");
                            count = inc + "";
                            Integer shipments = Integer.valueOf(count);
                            weight = service.getBookedVolumeWeightUnitKg();
                            totalShipments = totalShipments + shipments;
                            volume.setShipementsAgent(shipments.toString());
                            volume.setTotalShipments(totalShipments.toString());
                            volume.setVolumeInKgsAgent(weight.toString());
                            //weights = weights + weight;
                            weights = weight;
                            totalWeight = totalWeight + weights;
                        }
                        // volume.setTotalShipments(totalShipments.toString());
                        totalCount = inc + "";
                        log.info("Total Shipements::::::" + totalCount);
                        volume.setTotalShipments(totalCount);
                        //totalWeight = totalWeight + totalWeight;
                        if (totalWeight != null) {
                            volume.setTotalVolumeKgs(Double.toString(totalWeight));
                        }
                        if (service.getAgent() != null && service.getAgent().getPartyName() != null) {
                            volume.setAgentName(service.getAgent().getPartyName() != null ? service.getAgent().getPartyName() : "");

                        }
                        if (service.getServiceMaster() != null && service.getServiceMaster().getImportExport().equals(ImportExport.Export)) {
                            if (service.getCountry() != null) {
                                volume.setCountry(service.getCountry().getCountryName() != null ? service.getCountry().getCountryName() : "");
                            }
                            if (service.getTransitPort() != null) {
                                volume.setPortName(service.getTransitPort().getPortName() != null ? service.getTransitPort().getPortName() : "");
                            }

                        } else if (service.getServiceMaster() != null & service.getServiceMaster().getImportExport().equals(ImportExport.Import)) {

                            if (service.getCountry() != null) {
                                volume.setCountry(service.getCountry().getCountryName() != null ? service.getCountry().getCountryName() : "");
                            }
                            if (service.getTransitPort() != null) {
                                volume.setPortName(service.getTransitPort().getPortName() != null ? service.getTransitPort().getPortName() : "");
                            }
                        }
                    }

                    volumeReportCountryList.add(volume);

                    if (loggedInUser != null && loggedInUser.getSelectedUserLocation() != null
                            && service.getSystemTrack() != null && service.getSystemTrack().getCreateDate() != null) {

                        parameters.put("reportDate", reportUtil.dateToReportDate(service.getSystemTrack().getCreateDate(), loggedInUser.getSelectedUserLocation().getJavaDateFormat()));
                        String todayDate = "";
                        todayDate = reportUtil.dateToReportDate(new Date(), loggedInUser.getSelectedUserLocation().getJavaDateFormat());
                        parameters.put("fromDate", reportUtil.dateToReportDate(data.getApiFromDate(), loggedInUser.getSelectedUserLocation().getJavaDateFormat()));
                        parameters.put("toDate", reportUtil.dateToReportDate(data.getApiToDate(), loggedInUser.getSelectedUserLocation().getJavaDateFormat()));
                        parameters.put("refDate", todayDate);
                    }
                }
                inc++;
            }

            log.info("Address ------------------------------- "
                    + loggedInUser.getSelectedUserLocation().getAddressLine1());
            String loggedInUserName = "";
            String loggedInLocation = "";
            if (loggedInUser != null) {
                if (loggedInUser.getEmployee() != null
                        && loggedInUser.getEmployee().getEmployeeName() != null) {
                    loggedInUserName = loggedInUser.getEmployee().getEmployeeName();
                } else if (loggedInUser.getUserName() != null) {
                    loggedInUserName = loggedInUser.getUserName();
                } else {
                    loggedInUserName = "";
                }

                if (loggedInUser.getSelectedUserLocation() != null) {
                    loggedInLocation = loggedInUser.getSelectedUserLocation().getLocationName() != null
                            ? loggedInUser.getSelectedUserLocation().getLocationName() : "";

                    if (loggedInUser.getSelectedUserLocation() != null) {
                        LocationMaster locationMaster = locationMasterRepository.findByLocationCode(loggedInUser.getSelectedUserLocation().getLocationCode());
                        if (locationMaster != null) {
                            parameters.put("companyAddressLine1", locationMaster.getAddressLine1() != null ? locationMaster.getAddressLine1() : "");
                            parameters.put("companyAddressLine2", locationMaster.getAddressLine2() != null ? locationMaster.getAddressLine2() : "");
                            parameters.put("companyAddressLine3", locationMaster.getAddressLine3() != null ? locationMaster.getAddressLine3() : "");
                            parameters.put("companyName", locationMaster.getBranchName() != null ? locationMaster.getBranchName() : "");
                        }
                    } else {
                        loggedInLocation = "";
                    }
                }
            }

            parameters.put("locationName", loggedInLocation);
            parameters.put("refPlace", loggedInUserName + "-" + loggedInLocation);
            parameters.put("volumeReportCountryList", volumeReportCountryList);
            parameters.put("reportName", "VOLUME REPORT BY COUNTRY");

            JasperReport jasperReport = JasperCompileManager.compileReport(appResourcePath + "/efs-report/volume_report_by_country.jrxml");
            List<Object> fieldCollection1 = new ArrayList<>();
            Object obj1 = new Object();
            fieldCollection1.add(obj1);

            JRDataSource dataSource = new JRBeanCollectionDataSource(fieldCollection1, false);
            JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, dataSource);
            reportUtil.japerPrintToResponseOutputStream(data.getDownloadFileType(), jasperPrint, "VOLUME_REPORT_BY_COUNTRY", response, false, null);

        } catch (RestException e) {
            e.printStackTrace();

        } catch (Exception e) {
            e.printStackTrace();
        }
        log.info("Volume Report By Country Service Completed..... ");
        return null;

    }
}
