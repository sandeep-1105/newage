package com.efreightsuite.report.service;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringJoiner;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletResponse;

import com.efreightsuite.dto.AutoAttachmentDto;
import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.ReportDownloadRequestDto;
import com.efreightsuite.enumeration.ReportDownloadOption;
import com.efreightsuite.enumeration.ReportName;
import com.efreightsuite.enumeration.SequenceType;
import com.efreightsuite.enumeration.TransportMode;
import com.efreightsuite.enumeration.YesNo;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.model.Consol;
import com.efreightsuite.model.DocumentDetail;
import com.efreightsuite.model.LocationMaster;
import com.efreightsuite.model.LogoMaster;
import com.efreightsuite.model.ShipmentLink;
import com.efreightsuite.model.ShipmentServiceDetail;
import com.efreightsuite.model.UserProfile;
import com.efreightsuite.service.AuthService;
import com.efreightsuite.util.AppUtil;
import lombok.extern.log4j.Log4j2;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import org.apache.commons.lang3.StringUtils;

import org.springframework.stereotype.Service;

@Service
@Log4j2
public class MasterDoReportService extends AbstractReportService {
    public ReportName getReportType() {
        return ReportName.MASTER_DO;
    }

    public byte[] processReport(ReportDownloadRequestDto data, HttpServletResponse response, boolean isMultiple) {
        log.info("I am in MasterDoReportService..");
        BaseDto baseDto = new BaseDto();
        UserProfile logInUSer = AuthService.getCurrentUser();
        SimpleDateFormat formatter = new SimpleDateFormat(logInUSer != null ? logInUSer.getSelectedUserLocation().getJavaDateFormat() : "yyyy-MM-dd HH:mm:ss");
        JasperPrint jasperPrint = getReport(baseDto, data, AuthService.getCurrentUser(), formatter);
        byte[] a = null;
        AutoAttachmentDto autoAttachmentDto;
        if (data.getDoId() != null) {
            autoAttachmentDto = reportUtil.autoAttachment(ReportName.MASTER_DO, YesNo.Yes, data.getDoId(), "Shipment");
        } else {
            autoAttachmentDto = reportUtil.autoAttachment(ReportName.MASTER_DO, YesNo.Yes, data.getResourceId(), "MasterShipment");
        }

        if (data.getSingle() != null && data.getSingle() == true) {
            if (data.getDownloadOption() == ReportDownloadOption.Preview) {
                reportUtil.japerPrintToResponseOutputStream(data.getDownloadFileType(), jasperPrint, "MASTER_DO", response, false, autoAttachmentDto);
            } else if (autoAttachmentDto != null && autoAttachmentDto.getIsAttach() != null || data.getDownloadOption() == ReportDownloadOption.Download) {
                reportUtil.japerPrintToResponseOutputStream(data.getDownloadFileType(), jasperPrint, "MASTER_DO", response, false, autoAttachmentDto);
            } else if (data.getDownloadOption() == ReportDownloadOption.Print) {
                reportUtil.japerPrintToResponseOutputStream(data.getDownloadFileType(), jasperPrint, "MASTER_DO", response, false, autoAttachmentDto);
            }
        }

        if (data.getSingle() != null && data.getSingle() == true && data.getDownloadOption() == ReportDownloadOption.eMail) {
            a = reportUtil.japerPrintToResponseOutputStream(data.getDownloadFileType(), jasperPrint, "MASTER_DO", response, false, autoAttachmentDto);
            consolReportMailer.sendConsolMailToCustomer(data.getResourceId(), logInUSer, ReportName.MASTER_DO, a);
        }
        if (isMultiple == true && data.getDownloadOption() == ReportDownloadOption.eMail) {
            a = reportUtil.japerPrintToResponseOutputStream(data.getDownloadFileType(), jasperPrint, "MASTER_DO", response, true, autoAttachmentDto);
            return a;
        }
        return null;
    }

    private void checkDoNumberAndUpdate(Consol consol, Long documentId, UserProfile logInUSer, SimpleDateFormat formatter) {
        if (consol != null && documentId != null) {
            String doNumber = null;
            Date issuedDate = null;
            boolean isSaved = false;
            if (consol.getShipmentLinkList() != null && consol.getShipmentLinkList().size() > 0) {

                for (ShipmentLink sl : consol.getShipmentLinkList()) {

                    for (DocumentDetail d : sl.getService().getDocumentList()) {

                        if (d.getId().equals(documentId)) {
                            if (d != null && d.getDoNumber() == null) {
                                doNumber = sequenceGeneratorService.getSequenceValueByType(SequenceType.DO);
                                try {
                                    issuedDate = formatter.parse(reportUtil.dateToReportDate(new Date(), logInUSer.getSelectedUserLocation().getJavaDateFormat()));
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }
                            } else if (d != null) {
                                try {
                                    doNumber = d.getDoNumber();
                                    issuedDate = formatter.parse(reportUtil.dateToReportDate(new Date(), logInUSer.getSelectedUserLocation().getJavaDateFormat()));
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }
                            }
                            d.setDoNumber(doNumber);
                            d.setDoIssuedDate(issuedDate);
                            consolRepository.save(consol);
                            isSaved = true;
                            break;
                        }
                    }
                    if (isSaved) {
                        break;
                    }
                }
            }
        } else {
            boolean doNumFound = false;
            String doNum = "";
            if (consol.getId() != null) {
                if (consol.getShipmentLinkList() != null && consol.getShipmentLinkList().size() > 0) {
                    for (ShipmentLink sl : consol.getShipmentLinkList()) {

                        if (sl.getService() != null && sl.getService().getId() != null && sl.getService().getDocumentList() != null) {

                            for (DocumentDetail dd : sl.getService().getDocumentList()) {

                                if (dd.getDoNumber() == null) {


                                } else {
                                    doNumFound = true;
                                    doNum = dd.getDoNumber();
                                    break;
                                }
                            }
                            if (doNumFound) {
                                break;
                            }
                        }
                    }
                    //do not for all services
                    if (!doNumFound && doNum.trim() != " ") {
                        String doNumber = sequenceGeneratorService.getSequenceValueByType(SequenceType.DO);
                        updateDoForDocumentsEqually(doNumber, consol, logInUSer, formatter);
                    } else {
                        updateDoForDocumentsEqually(doNum, consol, logInUSer, formatter);
                    }
                }

            }
        }
    }

    public void updateDoForDocumentsEqually(String doNumber, Consol c, UserProfile logInUSer, SimpleDateFormat formatter) {

        if (c.getShipmentLinkList() != null && c.getShipmentLinkList().size() > 0) {
            System.out.println("version lock.." + c.getVersionLock());
            for (ShipmentLink sl : c.getShipmentLinkList()) {
                if (sl.getService() != null && sl.getService().getId() != null && sl.getService().getDocumentList() != null) {
                    for (DocumentDetail dd : sl.getService().getDocumentList()) {
                        Date issuedDate = null;
                        try {
                            issuedDate = formatter.parse(reportUtil.dateToReportDate(new Date(), logInUSer.getSelectedUserLocation().getJavaDateFormat()));
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        dd.setDoNumber(doNumber);
                        dd.setDoIssuedDate(issuedDate);
                    }
                }
            }
            consolRepository.save(c);
        }
    }

    private JasperPrint getReport(BaseDto baseDto, ReportDownloadRequestDto data, UserProfile loggedInUser, SimpleDateFormat formatter) {
        Consol consolEntity = consolRepository.findById(data.getResourceId());
        checkDoNumberAndUpdate(consolEntity, data.getDoId(), loggedInUser, formatter);
        log.info("Entering  MasterDoReportService.......");
        Map<String, Object> parameters = new HashMap<>();
        if (consolEntity != null) {
            try {
                if (consolEntity.getConsolDocument() != null) {
                    parameters.put("remark", consolEntity.getConsolDocument().getMarksAndNo());
                    parameters.put("mawbNumber", consolEntity.getConsolDocument().getMawbNo());
                    parameters.put("igmNumber", consolEntity.getConsolDocument().getIgmNo());
                    parameters.put("carrierDoNumber", consolEntity.getConsolDocument().getCarrierDoNo());
                    parameters.put("descriptionGoods", consolEntity.getConsolDocument().getCommodityDescription());

                }

                StringJoiner joiner = new StringJoiner(",");
                if (consolEntity.getAgentAddress() != null) {
                    String addressLine1 = consolEntity.getAgentAddress().getAddressLine1() != null ? consolEntity.getAgentAddress().getAddressLine1() : "";
                    String addressLine4 = consolEntity.getAgentAddress().getAddressLine4() != null ? consolEntity.getAgentAddress().getAddressLine4() : "";
                    parameters.put("deliveryAddress", addressLine1);
                    String address = joiner.add(consolEntity.getAgentAddress().getAddressLine2() != null ? consolEntity.getAgentAddress().getAddressLine2() : "").add(consolEntity.getAgentAddress().getAddressLine3() != null ? consolEntity.getAgentAddress().getAddressLine3() : "").toString();
                    if (address != null && address.trim().length() > 0) {
                        parameters.put("deliveryAddress1", (address).substring(0, address.length() - 1));
                    }
                    parameters.put("deliveryAddress2", addressLine4);
                }


                if (consolEntity.getServiceMaster() != null && consolEntity.getServiceMaster().getTransportMode() != null && consolEntity.getServiceMaster().getTransportMode().equals(TransportMode.Air)) {
                    parameters.put("reportName", "Delivery Advice");
                } else if (consolEntity.getServiceMaster() != null && consolEntity.getServiceMaster().getTransportMode() != null && consolEntity.getServiceMaster().getTransportMode().equals(TransportMode.Ocean)) {
                    parameters.put("reportName", "Delivery Order");
                }

                Date date = null;
                Double grossWeight = 0.0;
                Long pieces = 0L;
                String refenceNumber = "";
                String hawbNumber = "";


                if (consolEntity.getShipmentLinkList() != null && consolEntity.getShipmentLinkList().size() > 0) {
                    for (ShipmentLink shipmentLink : consolEntity.getShipmentLinkList()) {

                        ShipmentServiceDetail service = shipmentLink.getService();

                        if (data.getDoId() != null && !service.getDocumentList().get(0).getId().equals(data.getDoId())) {
                            continue;
                        }

                        if (service != null) {
                            for (DocumentDetail document : service.getDocumentList()) {
                                if (document.getReferenceNo() != null) {
                                    refenceNumber = refenceNumber + document.getReferenceNo() + ",";
                                }
                                if (document.getDocumentNo() != null) {
                                    hawbNumber = hawbNumber + document.getDocumentNo() + ",";
                                }
                                parameters.put("doNumber", document.getDoNumber());
                            }
                            parameters.put("referenceNumber", StringUtils.stripEnd(refenceNumber, ","));
                            parameters.put("hawbNumber", StringUtils.stripEnd(hawbNumber, ","));
                            if (service.getEta() != null) {
                                date = service.getEta();
                            }
                            if (service.getBookedGrossWeightUnitKg() != null) {
                                grossWeight = grossWeight + service.getBookedGrossWeightUnitKg();
                                parameters.put("grossWeight", AppUtil.getWeightFormat(grossWeight, service.getCountry().getLocale()));
                            }
                            parameters.put("flightNo", service.getRouteNo());
                            if (service.getBookedPieces() != null) {
                                pieces = pieces + service.getBookedPieces();
                                parameters.put("numberOfPieces", AppUtil.getNumberFormat(pieces, service.getCountry().getLocale()));
                            }
                        }

                    }
                }

                String companyName = "";
                if (loggedInUser != null) {
                    if (loggedInUser.getSelectedUserLocation() != null) {
                        LocationMaster locationMaster = locationMasterRepository.findByLocationCode(loggedInUser.getSelectedUserLocation().getLocationCode());
                        if (locationMaster != null) {
                            parameters.put("companyAddressLine1", locationMaster.getAddressLine1() != null ? locationMaster.getAddressLine1() : "");
                            parameters.put("companyAddressLine2", locationMaster.getAddressLine2() != null ? locationMaster.getAddressLine2() : "");
                            parameters.put("companyAddressLine3", locationMaster.getAddressLine3() != null ? locationMaster.getAddressLine3() : "");
                            companyName = locationMaster.getBranchName() != null ? locationMaster.getBranchName() : "";
                        }
                        LogoMaster logoMaster = logoMasterRepository.findByLocationMaster(loggedInUser.getSelectedUserLocation());
                        if (logoMaster != null && logoMaster.getLogo() != null) {
                            InputStream in = new ByteArrayInputStream(logoMaster.getLogo());
                            BufferedImage bImageFromConvert = ImageIO.read(in);
                            parameters.put("logo", bImageFromConvert);
                        }
                        String todayDate = "";
                        todayDate = reportUtil.dateToReportDate(new Date(), loggedInUser.getSelectedUserLocation().getJavaDateFormat());
                        parameters.put("doDate", todayDate);
                        parameters.put("carrierDoDate", todayDate);
                    }
                }
                parameters.put("companyName", reportUtil.removeSpecialCharacters(companyName, data.getDownloadOption()));
                parameters.put("etaDate", reportUtil.dateToReportDate(date, loggedInUser.getSelectedUserLocation().getJavaDateFormat()));


                JasperReport jasperReport = JasperCompileManager.compileReport(appResourcePath + "/efs-report/master_do.jrxml");
                List<Object> fieldCollection1 = new ArrayList<>();
                Object obj1 = new Object();
                fieldCollection1.add(obj1);
                JRDataSource dataSource = new JRBeanCollectionDataSource(fieldCollection1, false);
                JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, dataSource);
                return jasperPrint;

            } catch (RestException re) {
                log.error("RestException occures in MasterDoReportService......  : ", re);
            } catch (Exception exception) {
                log.error("RestException occures inMasterDoReportService..........  :: ", exception);
                baseDto.setResponseCode(ErrorCode.FAILED);
            }
        }
        return null;
    }
}
