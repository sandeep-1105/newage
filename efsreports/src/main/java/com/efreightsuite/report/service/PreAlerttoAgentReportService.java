package com.efreightsuite.report.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletResponse;

import com.efreightsuite.common.component.EmailRequestService;
import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.ReportDownloadRequestDto;
import com.efreightsuite.dto.ReportEmailRequestDto;
import com.efreightsuite.enumeration.AddressType;
import com.efreightsuite.enumeration.ReportDownloadFileType;
import com.efreightsuite.enumeration.ReportDownloadOption;
import com.efreightsuite.enumeration.ReportName;
import com.efreightsuite.enumeration.TemplateType;
import com.efreightsuite.enumeration.YesNo;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.model.Consol;
import com.efreightsuite.model.EmailRequest;
import com.efreightsuite.model.EmailRequestAttachment;
import com.efreightsuite.model.EmailTemplate;
import com.efreightsuite.model.ShipmentLink;
import com.efreightsuite.model.UserProfile;
import com.efreightsuite.repository.ConsolRepository;
import com.efreightsuite.repository.EmailTemplateRepository;
import com.efreightsuite.service.AuthService;
import com.efreightsuite.service.mailer.MailerUtil;
import com.efreightsuite.util.AppUtil;
import com.efreightsuite.util.TimeUtil;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class PreAlerttoAgentReportService {

    @Autowired
    CargoManifestReportService cargoManifestReportService;

    @Autowired
    HawbReportService hawbReportService;

    @Autowired
    MawbReportService mawbReportService;

    @Autowired
    ConsolRepository consolRepository;

    @Autowired
    EmailRequestService emailRequestService;

    @Autowired
    MailerUtil mailerUtil;

    @Autowired
    AppUtil appUtil;

    @Autowired
    EmailTemplateRepository emailTemplateRepository;

    public BaseDto sendMailToAgent(ReportEmailRequestDto reportEmailRequestDto) {

        log.info("Prealert mail called.....");
        EmailTemplate emailTemplate = emailTemplateRepository.findByTemplateType(TemplateType.PRE_ALRETTO_AGENT);

        HttpServletResponse response = null;
        HashMap<String, byte[]> attachMentArray = new HashMap<>();
        BaseDto baseDto = new BaseDto();
        Consol consol = null;
        UserProfile userProfile = AuthService.getCurrentUser();

        if (reportEmailRequestDto != null && reportEmailRequestDto.getConsolUid() != null) {

            consol = consolRepository.findByConsolUid(reportEmailRequestDto.getConsolUid());

        } else {
            throw new RestException(ErrorCode.FAILED);
        }

        if (consol != null) {
            ReportDownloadRequestDto data = new ReportDownloadRequestDto();
            data.setIsSingle(YesNo.Yes);
            data.setResourceId(consol.getId());
            data.setDownloadOption(ReportDownloadOption.eMail);
            data.setDownloadFileType(ReportDownloadFileType.PDF);
            data.setReportName(ReportName.SHIPMENT_MAWB);

            byte b[] = mawbReportService.processReport(data, response, true);
            attachMentArray.put("MAWB", b);

            if (consol != null && consol.getDirectShipment() != null && consol.getDirectShipment().equals(YesNo.No)) {

                data.setReportName(ReportName.CARGO_MANIFEST);
                byte a[] = cargoManifestReportService.processReport(data, response, true);
                attachMentArray.put("Cargo Manifest ", a);
                if (consol.getShipmentLinkList() != null && consol.getShipmentLinkList().size() > 0) {
                    for (ShipmentLink sd : consol.getShipmentLinkList()) {
                        if (sd.getService() != null && sd.getService().getId() != null) {
                            data.setResourceId(sd.getService().getDocumentList().get(0).getId());
                            data.setDownloadOption(ReportDownloadOption.eMail);
                            data.setDownloadFileType(ReportDownloadFileType.PDF);
                            data.setReportName(ReportName.HAWB);
                            a = hawbReportService.processReport(data, response, true);
                            attachMentArray.put(ReportName.HAWB.name() + "_" + sd.getService().getShipmentUid(), a);
                        }
                    }
                }
            }
            EmailRequest emailRequest = new EmailRequest();


            String emailSeprate = appUtil.getLocationConfig("single.text.split.by.multiple.email", consol.getLocation(), false);

            if (reportEmailRequestDto.getToEmailIdList() != null) {
                String toEmail = null;
                if (emailSeprate != null) {
                    toEmail = (reportEmailRequestDto.getToEmailIdList().replaceAll(emailSeprate, ","));
                } else {
                    toEmail = (reportEmailRequestDto.getToEmailIdList().replaceAll(";", ","));
                }
                emailRequest.setToEmailIdList(toEmail);
            }
            if (reportEmailRequestDto.getCcEmailIdList() != null) {
                String toEmail;
                if (emailSeprate != null) {
                    toEmail = (reportEmailRequestDto.getCcEmailIdList().replaceAll(emailSeprate, ","));
                } else {
                    toEmail = (reportEmailRequestDto.getCcEmailIdList().replaceAll(";", ","));
                }
                emailRequest.setCcEmailIdList(toEmail);
            }

            emailRequest.setFromEmailIdPassword(emailTemplate.getFromEmailPassword());
            emailRequest.setFromEmailId(emailTemplate.getFromEmailId());
            emailRequest.setPort(emailTemplate.getPort());
            emailRequest.setEmailType(emailTemplate.getEmailType());
            emailRequest.setSmtpServerAddress(emailTemplate.getSmtpServerAddress());
            emailRequest.setSubject(reportEmailRequestDto.getSubject());
            emailRequest.setEmailBody(reportEmailRequestDto.getTemplate());

            Set<EmailRequestAttachment> attachmentList = new HashSet<>();
            for (Map.Entry<String, byte[]> byteArray : attachMentArray.entrySet()) {
                EmailRequestAttachment emailRequestAttachment = new EmailRequestAttachment();
                emailRequestAttachment.setAttachement(byteArray.getValue());
                emailRequestAttachment.setFileName(byteArray.getKey() + "." + "Pdf");
                emailRequestAttachment.setMimeType("application/pdf");
                emailRequestAttachment.setManualSystemTrack(TimeUtil.getCreateSystemTrack(userProfile));
                attachmentList.add(emailRequestAttachment);
            }
            emailRequest.setAttachmentList(attachmentList);
            emailRequestService.saveAndSendEmail(emailRequest, userProfile, mailerUtil.getLogo(consol.getLocation()));
            baseDto.setResponseCode(ErrorCode.SUCCESS);
        }
        return baseDto;
    }

    public ReportEmailRequestDto getLengthOfBytes(String consolUid) {
        EmailTemplate emailTemplate = emailTemplateRepository.findByTemplateType(TemplateType.PRE_ALRETTO_AGENT);
        Consol consol = null;
        HttpServletResponse response = null;
        List<String> reportLength = new ArrayList<>();
        ReportEmailRequestDto requestDto = new ReportEmailRequestDto();
        ReportDownloadRequestDto data = new ReportDownloadRequestDto();
        data.setIsSingle(YesNo.Yes);
        byte a[] = null;
        consol = consolRepository.findByConsolUid(consolUid);
        String emailList = null;
        if (consol != null && consol.getAgent() != null) {
            if (consol.getAgent().getPartyAddressList() != null && consol.getAgent().getPartyAddressList().size() > 0) {
                for (int i = 0; i < consol.getAgent().getPartyAddressList().size(); i++) {
                    if (consol.getAgent().getPartyAddressList().get(i).getAddressType().equals(AddressType.Primary)) {
                        emailList = consol.getAgent().getPartyAddressList().get(i).getEmail();
                    }
                }
            }

        }
        //Pre Alert To Agent  : #CONSOL_UID# /MAWB : #MAWB_NO#

        if (consol != null) {
            emailTemplate.setSubject(emailTemplate.getSubject().replace("#CONSOL_UID#", consol.getConsolUid()));
            if (consol.getServiceMaster() != null && consol.getServiceMaster().getServiceCode() != null) {
                emailTemplate.setSubject(emailTemplate.getSubject().replace("#SERVICE_CODE#", consol.getServiceMaster().getServiceCode()));
            }
            if (consol.getConsolDocument() != null) {
                if (consol.getConsolDocument().getMawbNo() != null) {
                    emailTemplate.setSubject(emailTemplate.getSubject().replace("#MAWB_NO#", consol.getConsolDocument().getMawbNo()));
                }
            }
        }
        requestDto.setToEmailIdList(emailList);
        if (consol != null && consol.getCompany() != null) {
            emailTemplate.setTemplate(emailTemplate.getTemplate().replace("#COMPANY_NAME#", consol.getCompany().getCompanyName()));
        }

        requestDto.setTemplate(emailTemplate.getTemplate());
        requestDto.setSubject(emailTemplate.getSubject());
        if (consol != null && consol.getId() != null) {
            data.setReportName(ReportName.SHIPMENT_MAWB);
            if (consol.getConsolDocument() != null) {
                data.setResourceId(consol.getId());
            }
            data.setDownloadOption(ReportDownloadOption.eMail);
            data.setDownloadFileType(ReportDownloadFileType.PDF);

            a = mawbReportService.processReport(data, response, true);
            if (a != null)
                reportLength.add(ReportName.MAWB.name() + ".Pdf" + "(" + appUtil.formatSize(a.length) + ")");
            requestDto.setReportLength(reportLength);

            if (consol != null && consol.getDirectShipment() != null && consol.getDirectShipment().equals(YesNo.No)) {
                if (consol.getShipmentLinkList() != null && consol.getShipmentLinkList().size() > 0) {
                    for (ShipmentLink sd : consol.getShipmentLinkList()) {
                        if (sd.getService() != null && sd.getService().getId() != null) {
                            data.setResourceId(sd.getService().getDocumentList().get(0).getId());
                            data.setDownloadOption(ReportDownloadOption.eMail);
                            data.setDownloadFileType(ReportDownloadFileType.PDF);
                            a = hawbReportService.processReport(data, response, true);
                            if (a != null)
                                reportLength.add(ReportName.HAWB.name() + ".Pdf" + "_" + sd.getService().getShipmentUid() + "(" + appUtil.formatSize(a.length) + ")");
                            requestDto.setReportLength(reportLength);
                        }
                    }
                }
                data.setReportName(ReportName.CARGO_MANIFEST);
                data.setResourceId(consol.getId());
                data.setDownloadOption(ReportDownloadOption.eMail);
                data.setDownloadFileType(ReportDownloadFileType.PDF);

                a = cargoManifestReportService.processReport(data, response, true);
                if (a != null)
                    reportLength.add("Cargo Manifest" + ".Pdf" + "(" + appUtil.formatSize(a.length) + ")");
                requestDto.setReportLength(reportLength);
            }
        } else {
            throw new RestException(ErrorCode.FAILED);
        }
        return requestDto;
    }

}
