package com.efreightsuite.report.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Objects;

import com.efreightsuite.enumeration.ReportName;
import com.efreightsuite.model.Consol;
import com.efreightsuite.model.ConsolEvent;
import com.efreightsuite.model.EventMaster;
import com.efreightsuite.model.ShipmentServiceDetail;
import com.efreightsuite.model.ShipmentServiceEvent;
import com.efreightsuite.model.SystemTrack;
import com.efreightsuite.model.UserProfile;
import com.efreightsuite.util.ReportUtil;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class AutoUpdateEventService {


    @Autowired
    ReportUtil reportUtil;

    public ShipmentServiceDetail updateEventService(ReportName hawb,
                                                    ShipmentServiceDetail sd, UserProfile logInUSer,
                                                    EventMaster ev, SimpleDateFormat formatter) {

        if (sd.getEventList() != null && sd.getEventList().size() > 0) {
            boolean isFound = false;
            for (ShipmentServiceEvent se : sd.getEventList()) {
                if (Objects.equals(se.getEventMaster().getId(), ev.getId())) {
                    isFound = true;
                    se.getSystemTrack().setLastUpdatedUser(logInUSer != null ? logInUSer.getEmployee().getEmployeeName() : null);
                    try {
                        se.setEventDate(formatter.parse(reportUtil.dateToReportDate(new Date(), (logInUSer != null ? logInUSer.getSelectedUserLocation().getJavaDateFormat() : "yyyy-MM-dd HH:mm:ss"))));
                        se.getSystemTrack().setLastUpdatedDate(formatter.parse(reportUtil.dateToReportDate(new Date(), (logInUSer != null ? logInUSer.getSelectedUserLocation().getJavaDateFormat() : "yyyy-MM-dd HH:mm:ss"))));
                    } catch (java.text.ParseException p) {
                        log.info("exception while parsing", p);
                    }
                }
            }
            if (!isFound) {
                if (sd.getEventList() == null || sd.getEventList().size() == 0) {
                    sd.setEventList(new ArrayList<>());
                }
                addNewEventService(logInUSer, ev, formatter, sd);
            }
        } else {
            sd.setEventList(new ArrayList<>());
            addNewEventService(logInUSer, ev, formatter, sd);
        }
        return sd;
    }

    private void addNewEventService(UserProfile logInUSer, EventMaster ev, SimpleDateFormat formatter, ShipmentServiceDetail sd) {
        // TODO Auto-generated method stub
        ShipmentServiceEvent se = new ShipmentServiceEvent();
        //se.setAssignedTo(logInUSer!=null?logInUSer.getEmployee():null);
        se.setEventMaster(ev);
        se.setShipmentServiceDetail(sd);
        SystemTrack sr = new SystemTrack();
        try {
            se.setEventDate(formatter.parse(reportUtil.dateToReportDate(new Date(), (logInUSer != null ? logInUSer.getSelectedUserLocation().getJavaDateFormat() : "yyyy-MM-dd HH:mm:ss"))));
            sr.setCreateDate(formatter.parse(reportUtil.dateToReportDate(new Date(), (logInUSer != null ? logInUSer.getSelectedUserLocation().getJavaDateFormat() : "yyyy-MM-dd HH:mm:ss"))));
            sr.setCreateUser(logInUSer != null ? logInUSer.getEmployee().getEmployeeName() : null);
            sr.setLastUpdatedUser(logInUSer != null ? logInUSer.getEmployee().getEmployeeName() : null);
            sr.setLastUpdatedDate(formatter.parse(reportUtil.dateToReportDate(new Date(), (logInUSer != null ? logInUSer.getSelectedUserLocation().getJavaDateFormat() : "yyyy-MM-dd HH:mm:ss"))));
            se.setSystemTrack(sr);
        } catch (java.text.ParseException p) {
            log.info("exception while parsing", p);
        }
        sd.getEventList().add(se);
    }


    public Consol updateEventConsolMAWB(ReportName mawb, Consol c,
                                        UserProfile logInUSer, EventMaster ev, SimpleDateFormat formatter) {
        String isFor = ReportName.MAWB.name();
        if (c.getEventList() != null && c.getEventList().size() > 0) {
            boolean isFound = false;
            for (ConsolEvent ce : c.getEventList()) {
                if (Objects.equals(ce.getEventMaster().getId(), ev.getId())) {
                    isFound = true;
                    //ce.setAssignedTo(logInUSer!=null?logInUSer.getEmployee():null);
                    ce.getSystemTrack().setLastUpdatedUser(logInUSer != null ? logInUSer.getEmployee().getEmployeeName() : null);
                    try {
                        ce.setEventDate(formatter.parse(reportUtil.dateToReportDate(new Date(), (logInUSer != null ? logInUSer.getSelectedUserLocation().getJavaDateFormat() : "yyyy-MM-dd HH:mm:ss"))));
                        ce.getSystemTrack().setLastUpdatedDate(formatter.parse(reportUtil.dateToReportDate(new Date(), (logInUSer != null ? logInUSer.getSelectedUserLocation().getJavaDateFormat() : "yyyy-MM-dd HH:mm:ss"))));
                    } catch (java.text.ParseException p) {
                        log.info("exception while parsing", p);
                    }
                }
            }
            if (!isFound) {
                addNewEventConsol(logInUSer, ev, formatter, c, isFor);
            }
        } else {
            c.setEventList(new ArrayList<>());
            addNewEventConsol(logInUSer, ev, formatter, c, isFor);
        }
        return c;
    }


    //for adding event in consol
    private void addNewEventConsol(UserProfile logInUSer, EventMaster ev,
                                   SimpleDateFormat formatter, Consol c, String isFor) {
        // TODO Auto-generated method stub
        ConsolEvent ce = new ConsolEvent();
        //ce.setAssignedTo(logInUSer!=null?logInUSer.getEmployee():null);
        ce.setEventMaster(ev);
        ce.setConsol(c);
        SystemTrack sr = new SystemTrack();
        sr.setLastUpdatedUser(logInUSer != null ? logInUSer.getEmployee().getEmployeeName() : null);
        try {
            ce.setEventDate(formatter.parse(reportUtil.dateToReportDate(new Date(), (logInUSer != null ? logInUSer.getSelectedUserLocation().getJavaDateFormat() : "yyyy-MM-dd HH:mm:ss"))));
            sr.setCreateDate(formatter.parse(reportUtil.dateToReportDate(new Date(), (logInUSer != null ? logInUSer.getSelectedUserLocation().getJavaDateFormat() : "yyyy-MM-dd HH:mm:ss"))));
            sr.setLastUpdatedDate(formatter.parse(reportUtil.dateToReportDate(new Date(), (logInUSer != null ? logInUSer.getSelectedUserLocation().getJavaDateFormat() : "yyyy-MM-dd HH:mm:ss"))));
            sr.setCreateUser(logInUSer != null ? logInUSer.getEmployee().getEmployeeName() : null);
            ce.setSystemTrack(sr);
        } catch (java.text.ParseException p) {
            log.info("exception while parsing", p);
        }
        c.getEventList().add(ce);
    }

    public Consol updateEventConsolDO(ReportName do1, Consol c,
                                      UserProfile logInUSer, EventMaster ev, SimpleDateFormat formatter) {

        String isFor = ReportName.DO.name();
        if (c.getEventList() != null && c.getEventList().size() > 0) {
            boolean isFound = false;
            for (ConsolEvent se : c.getEventList()) {
                if (Objects.equals(se.getEventMaster().getId(), ev.getId())) {
                    isFound = true;
                    se.setAssignedTo(logInUSer != null ? logInUSer.getEmployee() : null);
                    se.getSystemTrack().setLastUpdatedUser(logInUSer != null ? logInUSer.getEmployee().getEmployeeName() : null);
                    try {
                        se.setEventDate(formatter.parse(reportUtil.dateToReportDate(new Date(), (logInUSer != null ? logInUSer.getSelectedUserLocation().getJavaDateFormat() : "yyyy-MM-dd HH:mm:ss"))));
                        se.getSystemTrack().setLastUpdatedDate(formatter.parse(reportUtil.dateToReportDate(new Date(), (logInUSer != null ? logInUSer.getSelectedUserLocation().getJavaDateFormat() : "yyyy-MM-dd HH:mm:ss"))));
                    } catch (java.text.ParseException p) {
                        log.info("exception while parsing", p);
                    }
                }
            }
            if (!isFound) {
                addNewEventConsol(logInUSer, ev, formatter, c, isFor);
            }
        } else {
            if (c.getEventList() != null) {
                c.getEventList().clear();
            } else {
                c.setEventList(new ArrayList<>());
            }
            addNewEventConsol(logInUSer, ev, formatter, c, isFor);
        }
        return c;
    }

}


