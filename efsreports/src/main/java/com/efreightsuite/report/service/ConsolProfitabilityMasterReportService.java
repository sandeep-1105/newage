package com.efreightsuite.report.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import com.efreightsuite.dto.AutoAttachmentDto;
import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.ReportDownloadRequestDto;
import com.efreightsuite.enumeration.PPCC;
import com.efreightsuite.enumeration.ReportDownloadOption;
import com.efreightsuite.enumeration.ReportName;
import com.efreightsuite.enumeration.YesNo;
import com.efreightsuite.model.Consol;
import com.efreightsuite.model.DocumentDetail;
import com.efreightsuite.model.InvoiceCreditNote;
import com.efreightsuite.model.InvoiceCreditNoteDetail;
import com.efreightsuite.model.LocationMaster;
import com.efreightsuite.model.ProvisionItem;
import com.efreightsuite.model.Provisional;
import com.efreightsuite.model.ShipmentLink;
import com.efreightsuite.model.ShipmentServiceDetail;
import com.efreightsuite.model.UserProfile;
import com.efreightsuite.report.JobCostSheet;
import com.efreightsuite.service.AuthService;
import com.efreightsuite.util.AppUtil;
import lombok.extern.log4j.Log4j2;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

import org.springframework.stereotype.Service;

@Service
@Log4j2
public class ConsolProfitabilityMasterReportService extends AbstractReportService {
    @Override
    public ReportName getReportType() {
        return ReportName.JOBCOST_SHEET;
    }

    public byte[] processReport(ReportDownloadRequestDto data, HttpServletResponse response, boolean isMultiple) {
        log.info("I am in ConsolProfitabilityMasterReportService.....");
        BaseDto baseDto = new BaseDto();
        UserProfile logInUSer = AuthService.getCurrentUser();
        JasperPrint jasperPrint = getReport(baseDto, data, AuthService.getCurrentUser());
        byte[] a = null;
        AutoAttachmentDto autoAttachmentDto = reportUtil.autoAttachment(ReportName.JOBCOST_SHEET, YesNo.Yes, data.getResourceId(), "MasterShipment");
        if (data.getSingle() != null && data.getSingle() == true) {
            if (data.getDownloadOption() == ReportDownloadOption.Preview || data.getDownloadOption() == ReportDownloadOption.Print) {
                reportUtil.japerPrintToResponseOutputStream(data.getDownloadFileType(), jasperPrint, "JOBCOST_SHEET", response, false, null);
            } else if (autoAttachmentDto != null && autoAttachmentDto.getIsAttach() != null || data.getDownloadOption() == ReportDownloadOption.Download) {
                reportUtil.japerPrintToResponseOutputStream(data.getDownloadFileType(), jasperPrint, "JOBCOST_SHEET", response, false, autoAttachmentDto);
            }
        }
        if (data.getSingle() != null && data.getSingle() == true && data.getDownloadOption() == ReportDownloadOption.eMail) {
            a = reportUtil.japerPrintToResponseOutputStream(data.getDownloadFileType(), jasperPrint, "JOBCOST_SHEET", response, false, null);
            consolReportMailer.sendConsolMailToCustomer(data.getResourceId(), logInUSer, ReportName.JOBCOST_SHEET, a);
        }
        if (isMultiple == true && data.getDownloadOption() == ReportDownloadOption.eMail) {
            a = reportUtil.japerPrintToResponseOutputStream(data.getDownloadFileType(), jasperPrint, "JOBCOST_SHEET", response, true, null);
            return a;
        }
        return null;
    }

    private JasperPrint getReport(BaseDto baseDto, ReportDownloadRequestDto data, UserProfile loggedInUser) {
        log.info("ConsolProfitabilityMasterReportService  Resource[" + data.getResourceId() + "] function called");
        JasperReport jasperReport;
        try {
            jasperReport = JasperCompileManager.compileReport(appResourcePath + "/efs-report/Job_cost_sheet.jrxml");
            List<Object> fieldCollection1 = new ArrayList<>();
            Object obj1 = new Object();
            fieldCollection1.add(obj1);
            JRDataSource dataSource = new JRBeanCollectionDataSource(fieldCollection1, false);
            Map<String, Object> parameters = new HashMap<>();
            List<JobCostSheet> provisionList = new ArrayList<>();
            List<JobCostSheet> provisionChargeList = new ArrayList<>();
            Consol consol = consolRepository.findById(data.getResourceId());
            Double totalVolumeWeight = 0.0;
            Double totalGrossWeight = 0.0;
            if (consol != null) {
                String pol = "";
                String pod = "";
                if (consol.getPol() != null) {
                    pol = consol.getPol().getPortName() != null ? consol.getPol().getPortName() : "";
                }
                if (consol.getPod() != null) {
                    pod = consol.getPod().getPortName() != null ? consol.getPod().getPortName() : "";
                }
                if (consol.getCarrier() != null) {
                    parameters.put("carrier", consol.getCarrier().getCarrierName() != null ? consol.getCarrier().getCarrierName() : "");
                }
                if (consol.getPpcc().equals(PPCC.Prepaid) || consol.getPpcc().equals("Prepaid")) {
                    parameters.put("masterPpCc", "PREPAID");
                } else {
                    parameters.put("masterPpCc", "COLLECT");
                }
                if (consol.getAgent() != null && consol.getAgent().getPartyName() != null) {
                    parameters.put("agentName", reportUtil.removeSpecialCharacters(consol.getAgent().getPartyName(), data.getDownloadOption()));
                }
                if (consol.getVesselCode() != null) {
                    parameters.put("vesselAndVoy", consol.getVesselCode());
                }
                if (consol.getConsolUid() != null) {
                    parameters.put("jobNumber", consol.getConsolUid());
                }
                if (consol.getConsolDocument() != null) {
                    parameters.put("volumeWeight", consol.getConsolDocument().getVolumeWeight() != null ? AppUtil.getWeightFormat(consol.getConsolDocument().getVolumeWeight(), consol.getCountry().getLocale()) + " KG" : "");
                    parameters.put("grossWeight", consol.getConsolDocument().getGrossWeight() != null ? AppUtil.getWeightFormat(consol.getConsolDocument().getGrossWeight(), consol.getCountry().getLocale()) + " KG" : "");
                    parameters.put("chargeableWeight", consol.getConsolDocument().getChargebleWeight() != null ? AppUtil.getWeightFormat(consol.getConsolDocument().getChargebleWeight(), consol.getCountry().getLocale()) + " KG" : "");
                    parameters.put("oblAndMawb", consol.getConsolDocument().getMawbNo() != null ? consol.getConsolDocument().getMawbNo() : "");
                }

                String loggedInUserName = "";
                String companyName = "";
                if (loggedInUser != null && loggedInUser.getSelectedUserLocation() != null) {
                    if (loggedInUser.getEmployee() != null && loggedInUser.getEmployee().getEmployeeName() != null) {
                        loggedInUserName = loggedInUser.getEmployee().getEmployeeName();
                    } else if (loggedInUser.getUserName() != null) {
                        loggedInUserName = loggedInUser.getUserName();
                    } else {
                        loggedInUserName = "";
                    }
                    parameters.put("refName", loggedInUserName);
                    parameters.put("refPlace", loggedInUserName + " - " + loggedInUser.getSelectedUserLocation().getLocationName());
                    String polDate = reportUtil.dateToReportDate(consol.getEta(), loggedInUser.getSelectedUserLocation().getJavaDateFormat());
                    String refDate = "";
                    String podDate = reportUtil.dateToReportDate(consol.getEtd(), loggedInUser.getSelectedUserLocation().getJavaDateFormat());
                    refDate = reportUtil.dateToReportDate(new Date(), loggedInUser.getSelectedUserLocation().getJavaDateFormat());
                    parameters.put("refDate", refDate);

                    if (pol.length() > 0 && polDate.length() > 0) {
                        parameters.put("polAndDate", pol + " / " + polDate);
                    } else {
                        parameters.put("polAndDate", pol + "  " + polDate);
                    }
                    if (pod.length() > 0 && podDate.length() > 0) {
                        parameters.put("podAndDate", pod + " / " + podDate);
                    } else {
                        parameters.put("podAndDate", pod + "  " + podDate);
                    }

                    if (loggedInUser.getSelectedUserLocation().getCurrencyMaster() != null && loggedInUser.getSelectedUserLocation().getCurrencyMaster().getCurrencyCode() != null) {
                        parameters.put("localCurrencyCode", loggedInUser.getSelectedUserLocation().getCurrencyMaster().getCurrencyCode());
                    } else {
                        parameters.put("localCurrencyCode", "");
                    }
                    if (loggedInUser.getSelectedUserLocation() != null) {
                        LocationMaster locationMaster = locationMasterRepository.findByLocationCode(loggedInUser.getSelectedUserLocation().getLocationCode());
                        if (locationMaster != null) {
                            parameters.put("companyAddressLine1", locationMaster.getAddressLine1() != null ? locationMaster.getAddressLine1() : "");
                            parameters.put("companyAddressLine2", locationMaster.getAddressLine2() != null ? locationMaster.getAddressLine2() : "");
                            parameters.put("companyAddressLine3", locationMaster.getAddressLine3() != null ? locationMaster.getAddressLine3() : "");
                            companyName = locationMaster.getBranchName() != null ? locationMaster.getBranchName() : "";
                            parameters.put("companyName", reportUtil.removeSpecialCharacters(companyName, data.getDownloadOption()));
                        }
                    }
                }
                Double totalAmountRevenue = 0.0;
                Double totalAmountCost = 0.0;
                Double totalAmountNet = 0.0;
                if (consol.getShipmentLinkList() != null && !consol.getShipmentLinkList().isEmpty()) {
                    int index = 0;
                    for (ShipmentLink shipmentLink : consol.getShipmentLinkList()) {
                        JobCostSheet jobCost = new JobCostSheet();
                        index = index + 1;
                        if (shipmentLink != null) {
                            Double volumeWeight = 0.0;
                            Double grossWeight = 0.0;
                            Double invoiceAmount = 0.0;
                             /*Double totalAmountRevenue =0.0;
							 Double totalAmountCost =0.0;
							 Double totalAmountNet =0.0;*/
                            ShipmentServiceDetail service = shipmentLink.getService();
							/* Boolean flag=true;
							 Boolean flagOne=true;
*/

                            if (service != null) {
                                if (service.getOrigin() != null) {
                                    jobCost.setOriginName(service.getOrigin().getPortName() != null ? service.getOrigin().getPortName() : "");
                                }
                                if (service.getDestination() != null) {
                                    jobCost.setFdc(service.getDestination().getPortName() != null ? service.getDestination().getPortName() : "");
                                }
                                if (service.getPpcc() != null) {
                                    if (service.getPpcc().equals(PPCC.Prepaid)) {
                                        jobCost.setPpcc("PREPAID");
                                    } else {
                                        jobCost.setPpcc("COLLECT");
                                    }
                                }
                                if (service.getSalesman() != null) {
                                    jobCost.setSalesman(service.getSalesman().getEmployeeName() != null ? service.getSalesman().getEmployeeName() : "");
                                }
                                if (service.getParty() != null) {
                                    jobCost.setCustomer(service.getParty().getPartyName() != null ? service.getParty().getPartyName() : "");
                                }
                                if (service.getBookedVolumeWeightUnitKg() != null) {
                                    volumeWeight = service.getBookedVolumeWeightUnitKg();
                                    totalVolumeWeight = totalVolumeWeight + volumeWeight;
                                    jobCost.setVolumeWeight(AppUtil.getWeightFormat(volumeWeight, service.getCountry().getLocale()));
                                }
                                if (service.getBookedGrossWeightUnitKg() != null) {
                                    grossWeight = service.getBookedGrossWeightUnitKg();
                                    totalGrossWeight = totalGrossWeight + grossWeight;
                                    jobCost.setGrossWeight(AppUtil.getWeightFormat(grossWeight, service.getCountry().getLocale()));
                                }
                                if (service.getDocumentList() != null && !service.getDocumentList().isEmpty()) {
                                    DocumentDetail document = service.getDocumentList().get(0);
                                    if (document != null) {
                                        jobCost.setHawb(document.getDocumentNo() != null ? document.getDocumentNo() : "");
                                    }
                                }
                                log.info("Service UID using Shipment link:::" + shipmentLink.getServiceUid());
									 /*if(flagOne){
										 jobCost.setIndex(index);	 
										 flagOne=false; 
									 }else{
										 jobCost.setIndex(null);
									 }*/


                                List<InvoiceCreditNote> invoiceList = invoiceCreditNoteRepository.findByServiceUid(shipmentLink.getServiceUid());

                                if (invoiceList != null && !invoiceList.isEmpty()) {

                                    for (InvoiceCreditNote invoice : invoiceList) {
                                        for (InvoiceCreditNoteDetail invoiceDetail : invoice.getInvoiceCreditNoteDetailList()) {

                                            if (invoiceDetail.getLocalAmount() != null) {
                                                invoiceAmount = invoiceAmount + invoiceDetail.getLocalAmount();
                                                log.info("LocalAmount For Report::::" + invoiceDetail.getLocalAmount());
                                            }
                                        }
                                    }
                                }

                                Provisional provisional = provisionalRepository.findByServiceUid(shipmentLink.getServiceUid());

                                Double totalRevenue = 0.0;
                                Double totalCost = 0.0;
                                Double totalNet = 0.0;
                                if (provisional != null) {
                                    if (provisional.getTotalRevenue() != null) {
                                        totalRevenue = provisional.getTotalRevenue();
                                    }
                                    if (provisional.getTotalCost() != null) {
                                        totalCost = provisional.getTotalCost();
                                    }
                                    if (provisional.getNetAmount() != null) {
                                        totalNet = provisional.getNetAmount();
                                    }

                                }

                                if (invoiceList != null && invoiceList.size() > 0) {
                                    totalAmountRevenue = totalAmountRevenue + invoiceAmount;
                                    jobCost.setRevenue(AppUtil.getCurrencyFormat(invoiceAmount, loggedInUser.getSelectedUserLocation().getCountryMaster().getLocale(), loggedInUser.getSelectedUserLocation().getCurrencyMaster().getDecimalPoint()));
                                    jobCost.setCost(AppUtil.getCurrencyFormat(totalCost, loggedInUser.getSelectedUserLocation().getCountryMaster().getLocale(), loggedInUser.getSelectedUserLocation().getCurrencyMaster().getDecimalPoint()));
                                    totalAmountCost = totalAmountCost + totalCost;
                                    Double totalGp = invoiceAmount - totalCost;
                                    totalAmountNet = totalAmountRevenue - totalAmountCost;
                                    jobCost.setGp(AppUtil.getCurrencyFormat(totalGp, loggedInUser.getSelectedUserLocation().getCountryMaster().getLocale(), loggedInUser.getSelectedUserLocation().getCurrencyMaster().getDecimalPoint()));

                                } else {
                                    totalAmountRevenue = invoiceAmount + totalRevenue;
                                    totalAmountCost = totalAmountCost + totalCost;
                                    totalAmountNet = totalAmountRevenue - totalNet;
                                    jobCost.setRevenue(AppUtil.getCurrencyFormat(totalRevenue, loggedInUser.getSelectedUserLocation().getCountryMaster().getLocale(), loggedInUser.getSelectedUserLocation().getCurrencyMaster().getDecimalPoint()));
                                    jobCost.setCost(AppUtil.getCurrencyFormat(totalCost, loggedInUser.getSelectedUserLocation().getCountryMaster().getLocale(), loggedInUser.getSelectedUserLocation().getCurrencyMaster().getDecimalPoint()));
                                    jobCost.setGp(AppUtil.getCurrencyFormat(totalNet, loggedInUser.getSelectedUserLocation().getCountryMaster().getLocale(), loggedInUser.getSelectedUserLocation().getCurrencyMaster().getDecimalPoint()));

                                }


                            }
                            parameters.put("totalVolumeWeight", AppUtil.getWeightFormat(totalVolumeWeight, service.getCountry().getLocale()));
                            parameters.put("totalGrossWeight", AppUtil.getWeightFormat(totalGrossWeight, service.getCountry().getLocale()));
                        }
                        provisionList.add(jobCost);
                    }
                }

                parameters.put("provisionList", provisionList);

                Double ta = 0.0;
                Double tr = 0.0;
                Double tc = 0.0;

                for (JobCostSheet jss : provisionList) {
                    try {

                        ta = ta + Double.valueOf(jss.getGp());
                        tr = tr + Double.valueOf(jss.getRevenue());
                        tc = tc + Double.valueOf(jss.getCost());

                    } catch (NumberFormatException ne) {

                        log.info("Exception cause" + ne.getStackTrace());

                    }

                }
                parameters.put("stotalGp", AppUtil.getCurrencyFormat(ta, loggedInUser.getSelectedUserLocation().getCountryMaster().getLocale(), loggedInUser.getSelectedUserLocation().getCurrencyMaster().getDecimalPoint()));
                parameters.put("stotalRevenue", AppUtil.getCurrencyFormat(tr, loggedInUser.getSelectedUserLocation().getCountryMaster().getLocale(), loggedInUser.getSelectedUserLocation().getCurrencyMaster().getDecimalPoint()));
                parameters.put("stotalCost", AppUtil.getCurrencyFormat(tc, loggedInUser.getSelectedUserLocation().getCountryMaster().getLocale(), loggedInUser.getSelectedUserLocation().getCurrencyMaster().getDecimalPoint()));


                Provisional provisional = provisionalRepository.findByMasterUid(consol.getConsolUid());

                log.info("inside provisionl line nmber====================301");
                if (provisional != null) {
                    Double totalRevenue = 0.0;
                    Double totalCost = 0.0;
                    Double totalGp = 0.0;
                    Double totalNeutral = 0.0;
                    if (provisional.getProvisionalItemList() != null && !provisional.getProvisionalItemList().isEmpty()) {
                        for (ProvisionItem provisionItem : provisional.getProvisionalItemList()) {
                            JobCostSheet jobCostSheet = new JobCostSheet();
                            Double revenue = 0.0;
                            Double cost = 0.0;
                            Double gp = 0.0;
                            if (provisionItem != null) {
                                if (provisionItem.getChargeMaster() != null) {
                                    if (provisionItem.getChargeMaster().getChargeCode() != null) {
                                        jobCostSheet.setChargeCode(provisionItem.getChargeMaster().getChargeCode());
                                    }
                                    if (provisionItem.getChargeMaster().getChargeName() != null) {
                                        jobCostSheet.setChargeName(provisionItem.getChargeMaster().getChargeName());
                                    }
                                }
                                if (provisionItem.getSellLocalAmount() != null) {
                                    revenue = provisionItem.getSellLocalAmount();
                                    totalRevenue = totalRevenue + revenue;
                                    jobCostSheet.setRevenue(AppUtil.getCurrencyFormat(provisionItem.getSellLocalAmount(), provisionItem.getSellCurrency().getCountryMaster().getLocale(), provisionItem.getSellCurrency().getDecimalPoint()));
                                }
                                if (provisionItem.getBuyLocalAmount() != null) {
                                    cost = provisionItem.getBuyLocalAmount();
                                    totalCost = totalCost + cost;
                                    jobCostSheet.setCost(AppUtil.getCurrencyFormat(provisionItem.getBuyLocalAmount(), provisionItem.getSellCurrency().getCountryMaster().getLocale(), provisionItem.getSellCurrency().getDecimalPoint()));
                                }
                                if (provisionItem.getIsNeutral() != null && provisionItem.getIsNeutral().equals(YesNo.Yes)) {
                                    if (provisionItem.getSellLocalAmount() != null) {
                                        totalNeutral = totalNeutral + provisionItem.getSellLocalAmount();
                                        jobCostSheet.setNeutral(AppUtil.getCurrencyFormat(provisionItem.getSellLocalAmount(), provisionItem.getSellCurrency().getCountryMaster().getLocale(), provisionItem.getSellCurrency().getDecimalPoint()));
                                    }
                                }
                                if (cost != null && cost > 0.0 && revenue != null && revenue > 0.0) {
                                    gp = revenue - cost;
                                    totalGp = totalGp + gp;
                                    jobCostSheet.setGp(AppUtil.getCurrencyFormat(gp, provisionItem.getSellCurrency().getCountryMaster().getLocale(), provisionItem.getSellCurrency().getDecimalPoint()));
                                }
                                provisionChargeList.add(jobCostSheet);
                            }
                            parameters.put("provisionChargeList", provisionChargeList);
                        }
                    }


                    if (provisional.getTotalRevenue() != null) {
                        parameters.put("totalRevenue", AppUtil.getCurrencyFormat(provisional.getTotalRevenue(), loggedInUser.getSelectedUserLocation().getCountryMaster().getLocale(), loggedInUser.getSelectedUserLocation().getCurrencyMaster().getDecimalPoint()));
                    }
                    if (provisional.getTotalCost() != null) {
                        parameters.put("totalCost", AppUtil.getCurrencyFormat(provisional.getTotalCost(), loggedInUser.getSelectedUserLocation().getCountryMaster().getLocale(), loggedInUser.getSelectedUserLocation().getCurrencyMaster().getDecimalPoint()));
                    }

                    if (provisional.getNetAmount() != null) {
                        totalGp = totalRevenue - totalCost;
                        parameters.put("totalGp", AppUtil.getCurrencyFormat(provisional.getNetAmount(), loggedInUser.getSelectedUserLocation().getCountryMaster().getLocale(), loggedInUser.getSelectedUserLocation().getCurrencyMaster().getDecimalPoint()));
                    }
                    if (totalNeutral != null && totalNeutral > 0.0) {
                        parameters.put("totalNeutral", AppUtil.getCurrencyFormat(totalNeutral, loggedInUser.getSelectedUserLocation().getCountryMaster().getLocale(), loggedInUser.getSelectedUserLocation().getCurrencyMaster().getDecimalPoint()));
                    } else {
                        parameters.put("totalNeutral", "");
                    }
                }

                String defaultMasterData = appUtil.getLocationConfig("job.cost.sheet.note", consol.getLocation(), false);
                parameters.put("note", defaultMasterData);


                return JasperFillManager.fillReport(jasperReport, parameters, dataSource);

            }

        } catch (JRException e) {
            e.printStackTrace();
        }
        return null;
    }
}
