package com.efreightsuite.report.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import com.efreightsuite.dto.AutoAttachmentDto;
import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.ReportDownloadRequestDto;
import com.efreightsuite.enumeration.ReportDownloadOption;
import com.efreightsuite.enumeration.ReportName;
import com.efreightsuite.enumeration.YesNo;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.model.Consol;
import com.efreightsuite.model.UserProfile;
import com.efreightsuite.service.AuthService;
import lombok.extern.log4j.Log4j2;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRPrintPage;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

import org.springframework.stereotype.Service;

@Service
@Log4j2
public class MawbLabelReportService extends AbstractReportService {
    public ReportName getReportType() {
        return ReportName.MAWB_LABEL;
    }

    public byte[] processReport(ReportDownloadRequestDto data, HttpServletResponse response, boolean isMultiple) {

        log.info("I am in MawbLabelReportService..");
        BaseDto baseDto = new BaseDto();
        UserProfile logInUSer = AuthService.getCurrentUser();
        log.info("Logged in User :: " + logInUSer.getId());
        JasperPrint jasperPrint = getReport(baseDto, data, AuthService.getCurrentUser());
        byte[] a = null;
        AutoAttachmentDto autoAttachmentDto = reportUtil.autoAttachment(ReportName.MAWB_LABEL, YesNo.Yes, data.getResourceId(), "MasterShipment");
        if (data.getSingle() != null && data.getSingle() == true) {
            if (data.getDownloadOption() == ReportDownloadOption.Preview || data.getDownloadOption() == ReportDownloadOption.Print) {
                reportUtil.japerPrintToResponseOutputStream(data.getDownloadFileType(), jasperPrint, "MAWB_LABEL", response, false, null);
            } else if (autoAttachmentDto != null && autoAttachmentDto.getIsAttach() != null || data.getDownloadOption() == ReportDownloadOption.Download) {
                reportUtil.japerPrintToResponseOutputStream(data.getDownloadFileType(), jasperPrint, "MAWB_LABEL", response, false, autoAttachmentDto);
            }
        }
        if (data.getSingle() != null && data.getSingle() == true && data.getDownloadOption() == ReportDownloadOption.eMail) {
            a = reportUtil.japerPrintToResponseOutputStream(data.getDownloadFileType(), jasperPrint, "MAWB_LABEL", response, false, null);
            consolReportMailer.sendConsolMailToCustomer(data.getResourceId(), logInUSer, ReportName.MAWB_LABEL, a);
        }
        if (isMultiple == true && data.getDownloadOption() == ReportDownloadOption.eMail) {
            a = reportUtil.japerPrintToResponseOutputStream(data.getDownloadFileType(), jasperPrint, "MAWB_LABEL", response, true, null);
            return a;
        }
        return null;
    }

    private JasperPrint getReport(BaseDto baseDto, ReportDownloadRequestDto data, UserProfile loggedInUser) {
        log.info("I am in MawbLabelReportService..");
        Map<String, Object> parameters = new HashMap<>();
        JasperReport jasperReport = null;
        List<Object> fieldCollection1 = new ArrayList<>();
        Object obj1 = new Object();
        fieldCollection1.add(obj1);
        JRDataSource dataSource = new JRBeanCollectionDataSource(fieldCollection1, false);
        try {
            Integer noOfPages = 0;
            Consol consolEntity = consolRepository.findById(data.getResourceId());
            if (consolEntity != null) {
                if (consolEntity.getConsolDocument() != null && consolEntity.getConsolDocument().getMawbNo() != null) {
                    parameters.put("barcodeTemplate", consolEntity.getConsolDocument().getMawbNo());

                    String mawbTemp = consolEntity.getConsolDocument().getMawbNo();
                    if (mawbTemp != null && mawbTemp.trim().length() > 0) {
                        String mawbFirst3Chars = "";
                        mawbTemp = mawbTemp.trim();
                        if (mawbTemp.length() >= 3) {
                            mawbFirst3Chars = mawbTemp.substring(0, 3);
                            mawbTemp = mawbTemp.substring(3, mawbTemp.length());
                        }
                        if (mawbTemp.length() >= 4) {
                            mawbTemp = mawbTemp.substring(0, 4) + " " + mawbTemp.substring(4, mawbTemp.length());
                        }

                        parameters.put("airwayBillNo", mawbFirst3Chars + "-" + mawbTemp);
                    }
                } else {
                    parameters.put("barcodeTemplate", "mawbnoNA");
                }
                if (consolEntity.getDestinationCode() != null) {
                    parameters.put("destination", consolEntity.getDestinationCode());
                }
                if (consolEntity.getCarrier() != null) {
                    parameters.put("airlineName", consolEntity.getCarrier().getCarrierName());
                }
                parameters.put("departure", consolEntity.getOriginCode());
            }
            parameters.put("mawbHawbAgentAirline", "AIRLINE");
            Integer hawbLabel = 0;

            String defaultMasterData = appUtil.getLocationConfig("hawb.mawb.label.config", consolEntity.getLocation(), false);

            if (defaultMasterData != null && defaultMasterData.trim().length() != 0) {
                hawbLabel = Integer.valueOf(defaultMasterData);
                try {
                    if (hawbLabel == 1 && data.getDownloadFileType().equals(data.getDownloadFileType().PDF)) {
                        jasperReport = JasperCompileManager.compileReport(appResourcePath + "/efs-report/mawb_label_one_pdf.jrxml");  //only One label per each PDF
                    } else if (hawbLabel == 1 && data.getDownloadFileType().equals(data.getDownloadFileType().XLS) || data.getDownloadFileType().equals(data.getDownloadFileType().RTF)) {
                        jasperReport = JasperCompileManager.compileReport(appResourcePath + "/efs-report/mawb_label_one_xls_rtf.jrxml");  //only One labels per Xls and RTF
                    } else if (hawbLabel == 2 && data.getDownloadFileType().equals(data.getDownloadFileType().XLS) || data.getDownloadFileType().equals(data.getDownloadFileType().RTF)) {
                        jasperReport = JasperCompileManager.compileReport(appResourcePath + "/efs-report/mawb_label_two_xls_rtf.jrxml");  //only Two labels per Xls and RTF
                    } else if (hawbLabel == 2 && data.getDownloadFileType().equals(data.getDownloadFileType().PDF)) {
                        jasperReport = JasperCompileManager.compileReport(appResourcePath + "/efs-report/mawb_label_two_pdf.jrxml");  //only Two labels per each PDF
                    } else {
                        throw new RestException("No Jrxml file for corresponding configurable Value");
                    }
                } catch (RestException exception) {
                    log.error("There is no Jrxml File::::::::" + exception);
                }

            }

            if (consolEntity != null && consolEntity.getConsolDocument() != null && consolEntity.getConsolDocument().getNoOfPieces() != null) {
                parameters.put("totalNoOfPieces", consolEntity.getConsolDocument().getNoOfPieces().toString());
                Integer tempPages = consolEntity.getConsolDocument().getNoOfPieces().intValue();
                if (hawbLabel == 1) {
                    noOfPages = tempPages;
                    noOfPages++;
                } else if (hawbLabel == 2) {
                    noOfPages = tempPages / 2;
                    noOfPages++;
                } else if (hawbLabel == 4) {
                    noOfPages = tempPages / 4;
                    noOfPages++;
                }

            }
            JasperPrint jasperPrint = null;
            try {
                jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, dataSource);

                List<JRPrintPage> pages = jasperPrint.getPages();
                log.info("Get Second Page Size :: " + noOfPages);
                noOfPages--;
                for (int j = 1; j < noOfPages; j++) {
                    log.info("Get Second Page No :: " + j);
                    parameters.put("pageNumber", String.valueOf(j + 1));
                    log.info("NoOfPages::::::" + Integer.valueOf(j));
                    JRPrintPage object = pages.get(0);
                    jasperPrint.addPage(object);
                }
            } catch (RestException exception) {
                log.info("There is no corresponding Jrxml file::" + exception);
            }
            return jasperPrint;
        } catch (JRException e) {
            e.printStackTrace();
        } catch (Exception e) {
            //e.printStackTrace();
            baseDto.setResponseCode(ErrorCode.FAILED);
        }
        return null;

    }
}
