package com.efreightsuite.report.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import com.efreightsuite.dto.AutoAttachmentDto;
import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.enumeration.EventMasterType;
import com.efreightsuite.enumeration.ReportName;
import com.efreightsuite.enumeration.SequenceType;
import com.efreightsuite.enumeration.YesNo;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.model.Consol;
import com.efreightsuite.model.ConsolAttachment;
import com.efreightsuite.model.ConsolDocument;
import com.efreightsuite.model.DocumentDetail;
import com.efreightsuite.model.DocumentMaster;
import com.efreightsuite.model.EnquiryAttachment;
import com.efreightsuite.model.EnquiryDetail;
import com.efreightsuite.model.EventMaster;
import com.efreightsuite.model.InvoiceCreditNote;
import com.efreightsuite.model.InvoiceCreditNoteAttachment;
import com.efreightsuite.model.Quotation;
import com.efreightsuite.model.QuotationAttachment;
import com.efreightsuite.model.QuotationDetail;
import com.efreightsuite.model.ReportMaster;
import com.efreightsuite.model.ShipmentAttachment;
import com.efreightsuite.model.ShipmentLink;
import com.efreightsuite.model.ShipmentServiceDetail;
import com.efreightsuite.model.UserProfile;
import com.efreightsuite.repository.ConsolDocumentRepository;
import com.efreightsuite.repository.ConsolEventRepository;
import com.efreightsuite.repository.ConsolRepository;
import com.efreightsuite.repository.DefaultMasterDataRepository;
import com.efreightsuite.repository.DocumentDetailRepository;
import com.efreightsuite.repository.DocumentMasterRepository;
import com.efreightsuite.repository.EnquiryDetailRepository;
import com.efreightsuite.repository.EnquiryLogRepository;
import com.efreightsuite.repository.EventMasterRepository;
import com.efreightsuite.repository.InvoiceCreditNoteRepository;
import com.efreightsuite.repository.QuotationDetailRepository;
import com.efreightsuite.repository.QuotationRepository;
import com.efreightsuite.repository.ReportMasterRepository;
import com.efreightsuite.repository.ShipmentRepository;
import com.efreightsuite.repository.ShipmentServiceDetailRepository;
import com.efreightsuite.repository.ShipmentServiceEventRepository;
import com.efreightsuite.service.AuthService;
import com.efreightsuite.service.SequenceGeneratorService;
import com.efreightsuite.util.AppUtil;
import com.efreightsuite.util.CryptoException;
import com.efreightsuite.util.CryptoUtils;
import com.efreightsuite.util.ReportUtil;
import groovy.util.logging.Log4j;
import org.jfree.util.Log;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Log4j
public class AutoAttachService {

    @Autowired
    AppUtil appUtil;

    @Autowired
    DocumentMasterRepository documentMasterRepository;

    @Autowired
    SequenceGeneratorService sequenceGeneratorService;

    @Autowired
    EnquiryLogRepository enquiryLogRepository;

    @Autowired
    EnquiryDetailRepository enquiryDetailRepository;


    @Autowired
    QuotationRepository quotationRepository;

    @Autowired
    QuotationDetailRepository quotationDetailRepository;


    @Autowired
    ShipmentRepository shipmentRepository;

    @Autowired
    ShipmentServiceDetailRepository shipmentServiceDetailRepository;

    @Autowired
    DocumentDetailRepository documentDetailRepository;

    @Autowired
    ConsolDocumentRepository consolDocumentRepository;

    @Autowired
    ReportMasterRepository reportMasterRepository;

    @Autowired
    ConsolRepository consolRepository;

    @Autowired
    InvoiceCreditNoteRepository invoiceCreditNoteRepository;

    @Autowired
    DefaultMasterDataRepository defaultMasterDataRepository;

    @Autowired
    ShipmentServiceEventRepository shipmentServiceEventRepository;

    @Autowired
    EventMasterRepository eventMasterRepository;

    @Autowired
    ConsolEventRepository consolEventRepository;

    @Autowired
    AutoUpdateEventService autoUpdateEventService;

    @org.springframework.context.annotation.Lazy
    @Autowired
    ReportUtil reportUtil;


    public BaseDto attach(AutoAttachmentDto autoAttachmentDto) throws CryptoException, ParseException {

        UserProfile logInUSer = AuthService.getCurrentUser();
        SimpleDateFormat formatter = new SimpleDateFormat(logInUSer != null ? logInUSer.getSelectedUserLocation().getJavaDateFormat() : "yyyy-MM-dd HH:mm:ss");

        String referenceNo = sequenceGeneratorService.getSequenceValueByType(SequenceType.REFERENCE_NO);
        ReportMaster rm = reportMasterRepository.findByReportKeyAndIsReportEnable(ReportName.valueOf(autoAttachmentDto.getDocumentName()), YesNo.Yes);
        BaseDto bd = new BaseDto();
        DocumentMaster dm = null;
        if (rm != null && rm.getDocumentMaster() != null) {
            dm = documentMasterRepository.findById(rm.getDocumentMaster().getId());
        } else {
            //Attachment will never done but checking for event will be continue..
        }
        if (autoAttachmentDto != null && autoAttachmentDto.getTransactionFor() != null) {

            switch (autoAttachmentDto.getTransactionFor()) {
                case "Enquiry":
                    bd = setAttachmentEnquiry(autoAttachmentDto, bd, referenceNo, dm);
                    break;
                case "Quotation":
                    bd = setAttachmentQuotation(autoAttachmentDto, bd, referenceNo, dm);
                    break;
                case "Shipment":
                    bd = setAttachmentShipmentService(autoAttachmentDto, bd, referenceNo, dm, logInUSer, formatter);
                    break;
                case "MasterShipment":
                    bd = setAttachmentConsol(autoAttachmentDto, bd, referenceNo, dm, logInUSer, formatter);
                    break;
                case "Invoice":
                    bd = setAttachmentInvoice(autoAttachmentDto, bd, referenceNo, dm);
                    break;
                default:
            }
        }
        return bd;
    }


    private BaseDto setAttachmentInvoice(AutoAttachmentDto autoAttachmentDto, BaseDto bd, String referenceNo, DocumentMaster dm) {

        InvoiceCreditNote invoice = null;

        if (autoAttachmentDto.getParentId() != null) {
            invoice = invoiceCreditNoteRepository.findById(autoAttachmentDto.getParentId());
        } else {
            Log.info("Nothing Selected");
        }
        if (invoice != null) {
            InvoiceCreditNoteAttachment attachment = new InvoiceCreditNoteAttachment();
            attachment.setInvoiceCreditNote(invoice);
            attachment.setRefNo(referenceNo);
            attachment.setFile(autoAttachmentDto.getDocumentObj());
            attachment.setFileContentType(autoAttachmentDto.getFormat());
            attachment.setFileName(autoAttachmentDto.getDocumentName());
            if (invoice.getInvoiceCreditNoteAttachmentList() != null && invoice.getInvoiceCreditNoteAttachmentList().size() > 0) {
                invoice.getInvoiceCreditNoteAttachmentList().add(attachment);
            } else {
                invoice.getInvoiceCreditNoteAttachmentList().add(attachment);
            }
            invoiceCreditNoteRepository.save(invoice);
            Log.info("Attachement successfully");

        }
        return bd;
    }

    private BaseDto setAttachmentConsol(AutoAttachmentDto autoAttachmentDto,
                                        BaseDto bd, String referenceNo, DocumentMaster dm, UserProfile logInUSer, SimpleDateFormat formatter) throws CryptoException, ParseException {

        ShipmentServiceDetail sd = null;
        ConsolDocument cd = null;
        Consol c = null;
        if (autoAttachmentDto.getParentId() != null) {
            c = consolRepository.findById(autoAttachmentDto.getParentId());
            cd = consolDocumentRepository.findById(c.getConsolDocument().getId());
        } else if (autoAttachmentDto.getChildId() != null) {
            sd = shipmentServiceDetailRepository.findById(autoAttachmentDto.getChildId());
            c = consolRepository.findByConsolUid(sd.getConsolUid() != null ? sd.getConsolUid() : null);
        } else if (autoAttachmentDto.getDocumentId() != null) {
            cd = consolDocumentRepository.findById(autoAttachmentDto.getDocumentId());
            c = consolRepository.findByConsolUid(cd.getConsolUid() != null ? cd.getConsolUid() : null);
        } else {
            Log.info("nothing selected");
        }
        if (c != null) {
            if (dm != null) {
                ConsolAttachment ca = new ConsolAttachment();
                ca.setDocumentMaster(dm);
                ca.setIsCustoms(YesNo.No);
                ca.setIsProtected(dm.getIsProtected() != null ? dm.getIsProtected() : null);
                ca.setConsol(c);
                ca.setRefNo(referenceNo);
                if (dm.getIsProtected() != null && dm.getIsProtected().equals(YesNo.Yes)) {
                    String KEY = appUtil.getLocationConfig("shipment.attach.file.encrypt.key", c.getLocation(), false);
                    ca.setFile(CryptoUtils.encrypt(KEY, autoAttachmentDto.getDocumentObj()));
                } else {
                    ca.setFile(autoAttachmentDto.getDocumentObj());
                }
                ca.setFileContentType(autoAttachmentDto.getFormat());
                ca.setFileName(autoAttachmentDto.getDocumentName());
                if (c.getConsolAttachmentList() != null) {
                    c.getConsolAttachmentList().add(ca);
                } else {
                    c.setConsolAttachmentList(new ArrayList<>());
                    c.getConsolAttachmentList().add(ca);
                }
            }
            if (autoAttachmentDto.getDocumentName() != null && (autoAttachmentDto.getDocumentName().equals(ReportName.SHIPMENT_MAWB.name()) || autoAttachmentDto.getDocumentName().equals(ReportName.MASTER_DO.name()))) {
                EventMaster ev;
                if (autoAttachmentDto.getDocumentName().equals(ReportName.SHIPMENT_MAWB.name())) {
                    ev = eventMasterRepository.findByEventMasterType(EventMasterType.MAWB_RECEIVED);
                    if (ev != null && ev.getId() != null) {
                        c = autoUpdateEventService.updateEventConsolMAWB(ReportName.MAWB, c, logInUSer, ev, formatter);
                    }
                    try {
                        c.getConsolDocument().setMawbIssueDate(formatter.parse(reportUtil.dateToReportDate(new Date(), (logInUSer != null ? logInUSer.getSelectedUserLocation().getJavaDateFormat() : "yyyy-MM-dd HH:mm:ss"))));
                        c.getConsolDocument().setMawbGeneratedBy(logInUSer.getEmployee());
                    } catch (ParseException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                } else {
                    ev = eventMasterRepository.findByEventMasterType(EventMasterType.DO_RECEIVED);
                    if (ev != null && ev.getId() != null) {
                        c = autoUpdateEventService.updateEventConsolDO(ReportName.DO, c, logInUSer, ev, formatter);

                        if (c.getShipmentLinkList() != null && c.getShipmentLinkList().size() > 0) {
                            for (ShipmentLink sl : c.getShipmentLinkList()) {
                                if (sl.getService() != null && sl.getService().getDocumentList() != null) {
                                    sl.getService().getDocumentList().get(0).setDoIssuedDate((formatter.parse(reportUtil.dateToReportDate(new Date(), (logInUSer != null ? logInUSer.getSelectedUserLocation().getJavaDateFormat() : "yyyy-MM-dd HH:mm:ss")))));
                                    sl.getService().getDocumentList().get(0).setDoIssuedBy(logInUSer.getEmployee());
                                }
                            }
                        }
                    }
                }
            }
            consolRepository.save(c);
            bd.setResponseCode(ErrorCode.SUCCESS);
        } else {
            bd.setResponseCode(ErrorCode.FAILED);
        }

        return appUtil.setDesc(bd);

    }


    //* For AUto attachment for enquiry*//
    private BaseDto setAttachmentEnquiry(AutoAttachmentDto autoAttachmentDto, BaseDto bd, String referenceNo, DocumentMaster dm) {

        EnquiryDetail ed = enquiryDetailRepository.findById(autoAttachmentDto.getChildId());
        if (ed != null) {
            EnquiryAttachment eq = new EnquiryAttachment();
            eq.setEnquiryDetail(ed);
            eq.setRefNo(referenceNo);
            eq.setFile(autoAttachmentDto.getDocumentObj());
            eq.setFileContentType(autoAttachmentDto.getFormat());
            eq.setFileName(autoAttachmentDto.getDocumentName());
            if (ed.getEnquiryAttachmentList() != null) {
                ed.getEnquiryAttachmentList().add(eq);
            } else {
                ed.setEnquiryAttachmentList(new ArrayList<>());
                ed.getEnquiryAttachmentList().add(eq);
            }
            enquiryDetailRepository.save(ed);
        }
        return bd;


    }


    private BaseDto setAttachmentShipmentService(AutoAttachmentDto autoAttachmentDto, BaseDto bd, String referenceNo, DocumentMaster dm, UserProfile logInUSer, SimpleDateFormat formatter) throws CryptoException {

        ShipmentServiceDetail sd = null;
        DocumentDetail dd = null;
        if (autoAttachmentDto.getChildId() != null) {
            sd = shipmentServiceDetailRepository.findById(autoAttachmentDto.getChildId());
        } else if (autoAttachmentDto.getDocumentId() != null) {
            dd = documentDetailRepository.findById(autoAttachmentDto.getDocumentId());
            sd = shipmentServiceDetailRepository.findByServiceUid(dd != null ? dd.getServiceUid() : null);
        } else {
            Log.info("nothing selected");
        }
        if (sd != null) {
            if (dm != null) {
                ShipmentAttachment sa = new ShipmentAttachment();
                sa.setDocumentMaster(dm);
                sa.setIsCustoms(YesNo.No);
                sa.setIsProtected(dm.getIsProtected() != null ? dm.getIsProtected() : null);
                sa.setShipmentServiceDetail(sd);
                sa.setRefNo(referenceNo);
                if (dm.getIsProtected() != null && dm.getIsProtected().equals(YesNo.Yes)) {
                    if (autoAttachmentDto.getDocumentObj() != null) {
                        String KEY = appUtil.getLocationConfig("shipment.attach.file.encrypt.key", sd.getLocation(), false);
                        sa.setFile(CryptoUtils.encrypt(KEY, autoAttachmentDto.getDocumentObj()));
                    }
                } else {
                    sa.setFile(autoAttachmentDto.getDocumentObj());
                }
                sa.setFileContentType(autoAttachmentDto.getFormat());
                sa.setFileName(autoAttachmentDto.getDocumentName());
                if (sd.getShipmentAttachmentList() != null) {
                    sd.getShipmentAttachmentList().add(sa);
                } else {
                    sd.setShipmentAttachmentList(new ArrayList<>());
                    sd.getShipmentAttachmentList().add(sa);
                }
            }

            if (autoAttachmentDto.getDocumentName() != null && autoAttachmentDto.getDocumentName().equals(ReportName.HAWB.name()) || autoAttachmentDto.getDocumentName().equals(ReportName.HAWB_DRAFT.name())) {

                EventMaster ev = eventMasterRepository.findByEventMasterType(EventMasterType.HAWB_RECEIVED);
                if (ev != null && ev.getId() != null) {
                    sd = autoUpdateEventService.updateEventService(ReportName.HAWB, sd, logInUSer, ev, formatter);
                }
                sd.getDocumentList().get(0).setHawbReceivedBy(logInUSer.getEmployee());
                try {
                    sd.getDocumentList().get(0).setHawbReceivedOn(formatter.parse(reportUtil.dateToReportDate(new Date(), (logInUSer != null ? logInUSer.getSelectedUserLocation().getJavaDateFormat() : "yyyy-MM-dd HH:mm:ss"))));
                } catch (ParseException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
            shipmentServiceDetailRepository.save(sd);
            bd.setResponseCode(ErrorCode.SUCCESS);
        } else {
            bd.setResponseCode(ErrorCode.FAILED);
        }
        return appUtil.setDesc(bd);
    }


    private BaseDto setAttachmentQuotation(AutoAttachmentDto autoAttachmentDto, BaseDto bd, String referenceNo, DocumentMaster dm) {

        Quotation q = null;
        QuotationDetail qd = null;
        if (autoAttachmentDto.getParentId() != null) {
            q = quotationRepository.findById(autoAttachmentDto.getParentId());
        } else if (autoAttachmentDto.getChildId() != null) {
            qd = quotationDetailRepository.findById(autoAttachmentDto.getChildId());
            q = quotationRepository.findById(qd.getQuotation().getId());
        } else {
            Log.info("nothing selected");
        }
        if (q != null) {
            QuotationAttachment qa = new QuotationAttachment();
            qa.setQuotation(q);
            qa.setRefNo(referenceNo);
            qa.setFile(autoAttachmentDto.getDocumentObj());
            qa.setFileContentType(autoAttachmentDto.getFormat());
            qa.setFileName(autoAttachmentDto.getDocumentName());
            if (q.getQuotationAttachementList() != null && q.getQuotationAttachementList().size() > 0) {
                q.getQuotationAttachementList().add(qa);
            } else {
                //q.setQuotationAttachementList(new HashSet<QuotationAttachment>());
                q.getQuotationAttachementList().add(qa);
            }
            quotationRepository.save(q);
        }
        return bd;
    }


}

