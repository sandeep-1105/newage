package com.efreightsuite.report.service;

import lombok.Data;

@Data
public class CommercialInvoice {
    private String description;
    private String qty;
    private String weight;
    private String volume;
    private String unitPrice;
    private String amount;
    private String totalAmount;

}
