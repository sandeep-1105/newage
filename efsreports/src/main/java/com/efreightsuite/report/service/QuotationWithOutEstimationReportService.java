package com.efreightsuite.report.service;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletResponse;

import com.efreightsuite.dto.AutoAttachmentDto;
import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.ReportCharge;
import com.efreightsuite.dto.ReportDownloadRequestDto;
import com.efreightsuite.enumeration.Approved;
import com.efreightsuite.enumeration.ChargeType;
import com.efreightsuite.enumeration.DimensionUnit;
import com.efreightsuite.enumeration.QuoteType;
import com.efreightsuite.enumeration.ReportConfig;
import com.efreightsuite.enumeration.ReportDownloadOption;
import com.efreightsuite.enumeration.ReportName;
import com.efreightsuite.enumeration.YesNo;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.model.EnquiryLog;
import com.efreightsuite.model.LocationMaster;
import com.efreightsuite.model.LogoMaster;
import com.efreightsuite.model.Quotation;
import com.efreightsuite.model.QuotationCarrier;
import com.efreightsuite.model.QuotationCharge;
import com.efreightsuite.model.QuotationDetail;
import com.efreightsuite.model.QuotationDetailGeneralNote;
import com.efreightsuite.model.QuotationDimension;
import com.efreightsuite.model.QuotationGeneralNote;
import com.efreightsuite.model.ReportConfigurationMaster;
import com.efreightsuite.model.UserProfile;
import com.efreightsuite.report.GeneralNotes;
import com.efreightsuite.report.Notes;
import com.efreightsuite.report.QuotaionDimensionReport;
import com.efreightsuite.report.QuotationFieldData;
import com.efreightsuite.service.AuthService;
import com.efreightsuite.util.AppUtil;
import com.efreightsuite.util.SaaSUtil;
import lombok.extern.log4j.Log4j2;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

import org.springframework.stereotype.Service;

@Service
@Log4j2
public class QuotationWithOutEstimationReportService extends AbstractReportService {
    public ReportName getReportType() {
        return ReportName.QUOTATION_WITHOUT_ESTIMATION;
    }

    public byte[] processReport(ReportDownloadRequestDto data, HttpServletResponse response, boolean isMultiple) {
        log.info("I am in QuotationWithOutEstimationReportService..");
        BaseDto baseDto = new BaseDto();
        UserProfile logInUSer = AuthService.getCurrentUser();
        JasperPrint jasperPrint = getReport(baseDto, data, AuthService.getCurrentUser());

        byte[] a = null;
        AutoAttachmentDto autoAttachmentDto = reportUtil.autoAttachment(ReportName.QUOTATION_WITHOUT_ESTIMATION, YesNo.Yes, data.getResourceId(), "Quotation");
        if (data.getSingle() != null && data.getSingle() == true) {
            if (data.getDownloadOption() == ReportDownloadOption.Preview || data.getDownloadOption() == ReportDownloadOption.Print) {
                reportUtil.japerPrintToResponseOutputStream(data.getDownloadFileType(), jasperPrint, "QUOTATION_WITHOUT_ESTIMATION", response, false, null);
            } else if (autoAttachmentDto != null && autoAttachmentDto.getIsAttach() != null || data.getDownloadOption() == ReportDownloadOption.Download) {
                reportUtil.japerPrintToResponseOutputStream(data.getDownloadFileType(), jasperPrint, "QUOTATION_WITHOUT_ESTIMATION", response, false, autoAttachmentDto);
            }

        }
        if (data.getSingle() != null && data.getSingle() == true && data.getDownloadOption() == ReportDownloadOption.eMail) {
            a = reportUtil.japerPrintToResponseOutputStream(data.getDownloadFileType(), jasperPrint, "QUOTATION_WITHOUT_ESTIMATION", response, false, null);
            quotationReportMailer.sendQuotationMailToCustomer(data.getResourceId(), logInUSer, ReportName.QUOTATION_WITHOUT_ESTIMATION, a);
        }
        if (isMultiple == true && data.getDownloadOption() == ReportDownloadOption.eMail) {
            a = reportUtil.japerPrintToResponseOutputStream(data.getDownloadFileType(), jasperPrint, "QUOTATION_WITHOUT_ESTIMATION", response, true, null);
            return a;
        }
        if (data != null && data.getDownloadOption() != null && data.getDownloadFileType() != null) {

        } else {
            a = reportUtil.japerPrintToResponseOutputStream(data.getDownloadFileType(), jasperPrint, "QUOTATION_WITHOUT_ESTIMATION", response, false, null);
            return a;
        }
        return null;
    }

    public byte[] getPdfReportByteArray(ReportDownloadRequestDto data, UserProfile userProfile) {
        BaseDto baseDto = new BaseDto();
        JasperPrint jasperPrint = getReport(baseDto, data, userProfile);
        try {
            return JasperExportManager.exportReportToPdf(jasperPrint);
        } catch (JRException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return null;
    }

    private JasperPrint getReport(BaseDto baseDto, ReportDownloadRequestDto data, UserProfile loggedInUser) {
        log.info("I am in QuotationWithOutEstimationReportService..");
        // Report Parameters Data Preparation
        Map<String, Object> parameters = new HashMap<>();
        try {
            String companyName = "";
            Quotation quotation = quotationRepository.findById(data.getResourceId());
            {
                ReportConfigurationMaster tmpObj = reportConfigurationMasterRepository.getByKeyAndLocation(ReportConfig.QUOTATION_WITHOUT_ESTIMATION_REPORT_HEADER, quotation.getLocationMaster().getId());
                if (tmpObj != null && tmpObj.getReportValue() != null) {
                    parameters.put("quotationHeaderNote", tmpObj.getReportValue());
                }
            }
            {
                ReportConfigurationMaster tmpObj = reportConfigurationMasterRepository.getByKeyAndLocation(ReportConfig.QUOTATION_WITHOUT_ESTIMATION_REPORT_FOOTER, quotation.getLocationMaster().getId());
                if (tmpObj != null && tmpObj.getReportValue() != null) {
                    parameters.put("quotationFooterNote", tmpObj.getReportValue());
                }
            }
            String urlPath = applicationBaseUrl + "/public/qutation-approval/customer_approveLink.html?saasId=" + SaaSUtil.getSaaSId().toUpperCase() + "&quotationId=" + quotation.getId();
            log.info("Quotation Approve Link:::" + urlPath);
            parameters.put("quotationApproveLink", urlPath);//for Testing approveLink

            List<QuotationFieldData> quotationFieldCollection = new ArrayList<>();
            QuotationFieldData quotationField = new QuotationFieldData();


            for (QuotationDetail qd : quotation.getQuotationDetailList()) {
                for (QuotationCarrier qc : qd.getQuotationCarrierList()) {
                    Map<String, List<QuotationCharge>> mapChargeList = new HashMap<>();

                    for (QuotationCharge qcc : qc.getQuotationChargeList()) {

                        if (quotation.getQuoteType() != QuoteType.GENERAL && qcc.getIsGroup() != null && qcc.getIsGroup() == YesNo.Yes && qcc.getGroupName() != null && qcc.getGroupName().trim().length() != 0) {

                            if (mapChargeList.get(qcc.getGroupName()) == null) {
                                List<QuotationCharge> tmpList = new ArrayList<>();
                                tmpList.add(qcc);
                                mapChargeList.put(qcc.getGroupName(), tmpList);
                            } else {
                                mapChargeList.get(qcc.getGroupName()).add(qcc);
                            }
                        } else {
                            if (mapChargeList.get(qcc.getChargeMaster().getChargeType().toString()) == null) {
                                List<QuotationCharge> tmpList = new ArrayList<>();
                                tmpList.add(qcc);
                                mapChargeList.put(qcc.getChargeMaster().getChargeType().toString(), tmpList);
                            } else {
                                mapChargeList.get(qcc.getChargeMaster().getChargeType().toString()).add(qcc);
                            }
                        }

                    }

                    List<QuotationCharge> originList = new ArrayList<>();
                    List<QuotationCharge> otherList = new ArrayList<>();
                    List<QuotationCharge> freightList = new ArrayList<>();
                    List<QuotationCharge> destinationList = new ArrayList<>();

                    for (String key : mapChargeList.keySet()) {
                        if (key == "Origin") {
                            originList.addAll(mapChargeList.get(key));
                        } else if (key == "Freight") {
                            freightList.addAll(mapChargeList.get(key));
                        } else if (key == "Other") {
                            otherList.addAll(mapChargeList.get(key));
                        } else if (key == "Destination") {
                            destinationList.addAll(mapChargeList.get(key));
                        }
                    }
                    ReportCharge dto = null;
                    dto = new ReportCharge();
                    if (originList.size() > 0 && originList != null) {
                        dto = new ReportCharge();
                        dto.setKeyName(ChargeType.Origin.name());
                        dto.setChargeList(originList);
                        qc.getMapChargeList().add(dto);
                    }
                    if (freightList.size() > 0 && freightList != null) {
                        dto = new ReportCharge();
                        dto.setKeyName(ChargeType.Freight.name());
                        dto.setChargeList(freightList);
                        qc.getMapChargeList().add(dto);
                    }
                    if (otherList.size() > 0 && otherList != null) {
                        dto = new ReportCharge();
                        dto.setKeyName(ChargeType.Other.name());
                        dto.setChargeList(otherList);
                        qc.getMapChargeList().add(dto);
                    }
                    if (destinationList.size() > 0 && destinationList != null) {
                        dto = new ReportCharge();
                        dto.setKeyName(ChargeType.Destination.name());
                        dto.setChargeList(destinationList);
                        qc.getMapChargeList().add(dto);
                    }
                }
            }
            quotationField.setQuotation(quotation);

            BufferedImage image = null;
            try {
                image = ImageIO.read(new File(appResourcePath + "/efs-report/NotApproveWaterMark.png"));
            } catch (IOException e) {
                e.printStackTrace();
            }
            byte[] imageInByte = null;
            try {
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                ImageIO.write(image, "png", baos);
                baos.flush();
                imageInByte = baos.toByteArray();
                baos.close();
            } catch (Exception e) {
                log.info(e);
            }
            InputStream isImage = new ByteArrayInputStream(imageInByte);
            BufferedImage bImage = null;
            try {
                bImage = ImageIO.read(isImage);
            } catch (IOException e) {
                e.printStackTrace();
            }

            parameters.put("waterMark", bImage);
            if (quotation != null) {
                if (quotation.getApproved() != null && quotation.getApproved().equals(Approved.Approved)) {
                    parameters.put("approvedLink", true);
                }

                if (quotation.getApproved().equals(Approved.Pending) || quotation.getApproved().equals(Approved.Rejected)) {
                    parameters.put("approved", true);
                } else {
                    parameters.put("approved", false);
                }


                // SalesMan Details
                if (quotation.getSalesman() != null) {
                    if (quotation.getSalesman().getEmployeeProfessionalDetail() != null) {
                        if (quotation.getSalesman().getEmployeeProfessionalDetail().getDesignationMaster() != null) {
                            quotationField.setSalesManDesignation(quotation.getSalesman().getEmployeeProfessionalDetail().getDesignationMaster().getDesignationName() != null
                                    ? quotation.getSalesman().getEmployeeProfessionalDetail().getDesignationMaster().getDesignationName() : "");
                        }
                    }
                    quotationField.setSalesManEmail(
                            quotation.getSalesman().getEmail() != null ? quotation.getSalesman().getEmail() : "");
                    quotationField.setSalesManName(quotation.getSalesman().getEmployeeName() != null
                            ? quotation.getSalesman().getEmployeeName() : "");
                    quotationField.setSalesManTel(quotation.getSalesman().getEmployeePhoneNo() != null
                            ? quotation.getSalesman().getEmployeePhoneNo() : "");

                    if (quotation.getSalesman().getLocationMaster() != null) {
                        log.info("Date Format ::: " + quotation.getSalesman().getLocationMaster().getJavaDateFormat());
                        // Page Footer Details
                        quotationField.setReferenceNo("");
                    }
                } else {
                    quotationField.setSalesManDesignation("");
                    quotationField.setSalesManEmail("");
                    quotationField.setSalesManName("");
                    quotationField.setSalesManTel("");
                }
                if (quotation.getCustomer() != null && quotation.getCustomer().getPartyName() != null && data != null && data.getDownloadOption() != null) {
                    log.info("quotation.getCustomer()---------- " + quotation.getCustomer().getPartyName());
                    parameters.put("customerName", reportUtil.removeSpecialCharacters(quotation.getCustomer().getPartyName(), data.getDownloadOption()));
                } else {
                    parameters.put("customerName", quotation.getCustomer().getPartyName());
                }
                if (quotation.getCustomerAddress() != null) {
                    parameters.put("customerPhone", quotation.getCustomerAddress().getMobile() != null ? quotation.getCustomerAddress().getMobile() : "");
                    parameters.put("customerFax", quotation.getCustomerAddress().getPhone() != null ? quotation.getCustomerAddress().getPhone() : "");
                } else {
                    parameters.put("customerPhone", "");
                    parameters.put("customerFax", "");
                }
                parameters.put("customerAddressLine1", quotation.getCustomerAddress().getAddressLine1() != null
                        ? quotation.getCustomerAddress().getAddressLine1() : "");
                parameters.put("customerAddressLine2", quotation.getCustomerAddress().getAddressLine2() != null
                        ? quotation.getCustomerAddress().getAddressLine2() : "");
                parameters.put("customerAddressLine3", quotation.getCustomerAddress().getAddressLine3() != null
                        ? quotation.getCustomerAddress().getAddressLine3() : "");
                parameters.put("customerAddressLine4", quotation.getCustomerAddress().getAddressLine4() != null
                        ? quotation.getCustomerAddress().getAddressLine4() : "");

                parameters.put("customerAttention", quotation.getAttention());
                parameters.put("quotationNo", quotation.getQuotationNo());
            }

            String loggedInUserName = "";
            String loggedInLocation = "";
            quotationField.setReferenceNo("");
            if (quotation != null && quotation.getLocationMaster() != null) {
                log.info("Date Format ::: " + quotation.getLocationMaster().getJavaDateFormat());

                // Page Footer Details
                quotationField.setRefDate(reportUtil.dateToReportDate(new Date(), quotation.getLocationMaster().getJavaDateFormat()));
                parameters.put("quotationDate", reportUtil.dateToReportDate(quotation.getSystemTrack().getCreateDate(), quotation.getLocationMaster().getJavaDateFormat()));
                parameters.put("validityFrom", reportUtil.dateToReportDate(quotation.getValidFrom(), quotation.getLocationMaster().getJavaDateFormat()));
                parameters.put("validityTo", reportUtil.dateToReportDate(quotation.getValidTo(), quotation.getLocationMaster().getJavaDateFormat()));
            } else {
                quotationField.setRefDate("");
                parameters.put("quotationDate", "");
                parameters.put("validityFrom", "");
                parameters.put("validityTo", "");
            }

            if (loggedInUser != null) {
                if (loggedInUser.getEmployee() != null && loggedInUser.getEmployee().getEmployeeName() != null) {
                    loggedInUserName = loggedInUser.getEmployee().getEmployeeName();
                } else if (loggedInUser.getUserName() != null) {
                    loggedInUserName = loggedInUser.getUserName();
                } else {
                    loggedInUserName = "";
                }
                //Address from Location Master -> Default Data Master
                if (loggedInUser.getSelectedUserLocation() != null) {
                    LogoMaster logoMaster = logoMasterRepository.findByLocationMaster(loggedInUser.getSelectedUserLocation());
                    if (logoMaster != null && logoMaster.getLogo() != null) {
                        log.info("Logo For Quotation With Estiamtion Report :: " + logoMaster.getId() + " --- Logo : " + logoMaster.getLogo());
                        InputStream in = new ByteArrayInputStream(logoMaster.getLogo());
                        BufferedImage bImageFromConvert = ImageIO.read(in);
                        parameters.put("logo", bImageFromConvert);
                    }
                    loggedInLocation = loggedInUser.getSelectedUserLocation().getLocationName() != null ? loggedInUser.getSelectedUserLocation().getLocationName() : "";
                    LocationMaster locationMaster = locationMasterRepository.findByLocationCode(loggedInUser.getSelectedUserLocation().getLocationCode());
                    if (locationMaster != null) {
                        if (locationMaster.getBranchName() != null) {
                            companyName = locationMaster.getBranchName() != null ? locationMaster.getBranchName() : "";
                            if (data != null && data.getDownloadOption() != null) {
                                quotationField.setCompanyName(reportUtil.removeSpecialCharacters(companyName, data.getDownloadOption()));
                                quotationField.setSalesManCompany(reportUtil.removeSpecialCharacters(companyName, data.getDownloadOption()));
                            } else {
                                quotationField.setCompanyName(companyName);
                                quotationField.setSalesManCompany(companyName);
                            }

                            quotationField.setCompanyAddressLine1(locationMaster.getAddressLine1() != null ? locationMaster.getAddressLine1() : "");
                            quotationField.setCompanyAddressLine2(locationMaster.getAddressLine2() != null ? locationMaster.getAddressLine2() : "");
                            quotationField.setCompanyLocation(locationMaster.getAddressLine3() != null ? locationMaster.getAddressLine3() : "");
                        }
                    }

                } else {
                    loggedInLocation = "";
                }
            }

            List<QuotaionDimensionReport> dimensionList = new ArrayList<>();

            quotationField.setRefPlace(loggedInUserName + "-" + loggedInLocation);
            quotationFieldCollection.add(quotationField);
            // Carrier Charges
            String originName = "";
            String destinationName = "";
            String carrierName = "";
            // Notes Parameter
            List<Notes> noteList = new ArrayList<>();


            if (quotation != null && quotation.getQuotationDetailList() != null && !quotation.getQuotationDetailList().isEmpty()) {
                QuotationDetail qd = quotation.getQuotationDetailList().iterator().next();
                if (qd.getOrigin() != null) {
                    originName = qd.getOrigin().getPortName();
                }
                if (qd.getDestination() != null) {
                    destinationName = qd.getDestination().getPortName();
                    if (qd.getTosCode() != null) {
                        destinationName = destinationName + "(" + qd.getTosCode() + ")";
                    }
                }

                if (qd.getServiceNoteList() != null && !qd.getServiceNoteList().isEmpty()) {
                    for (QuotationDetailGeneralNote serviceNote : qd.getServiceNoteList()) {
                        Notes note = new Notes();
                        note.setReportNote(serviceNote.getNote() != null ? serviceNote.getNote() : "");
                        noteList.add(note);
                    }
                }

                if (qd.getDimensionUnit().equals(DimensionUnit.CENTIMETERORKILOS) || qd.getDimensionUnit().equals("CENTIMETERORKILOS")) {
                    String weight = "KG";
                    String dimension = "cm";
                    String volumeKg = "KG";
                    parameters.put("volumeKg", "(" + volumeKg + ")");
                    parameters.put("dimension", "(" + dimension + ")");
                    parameters.put("dimensionKg", "(" + weight + ")");
                } else {
                    String weight = "LBS";
                    String dimension = "in";
                    String volumeKg = "KG";
                    parameters.put("volumeKg", "(" + volumeKg + ")");
                    parameters.put("dimension", "(" + dimension + ")");
                    parameters.put("dimensionKg", "(" + weight + ")");
                }
                if (qd.getQuotationDimensionList() != null && !qd.getQuotationDimensionList().isEmpty()) {
                    for (QuotationDimension qDim : qd.getQuotationDimensionList()) {
                        QuotaionDimensionReport dimension = new QuotaionDimensionReport();
                        dimension.setDimensionPiece(AppUtil.getNumberFormat(qDim.getNoOfPiece(), quotation.getCountryMaster().getLocale()));
                        dimension.setDimensionLength(AppUtil.getNumberFormat(qDim.getLength(), quotation.getCountryMaster().getLocale()));
                        dimension.setDimensionHeight(AppUtil.getNumberFormat(qDim.getHeight(), quotation.getCountryMaster().getLocale()));
                        dimension.setDimensionWidth(AppUtil.getNumberFormat(qDim.getWidth(), quotation.getCountryMaster().getLocale()));

                        dimension.setGrossWeight(AppUtil.getWeightFormat(qDim.getGrossWeight(), quotation.getCountryMaster().getLocale()));
                        dimension.setDimensionVolume(AppUtil.getWeightFormat(qDim.getVolWeight(), quotation.getCountryMaster().getLocale()));
                        dimension.setGrossWeightLB(AppUtil.getWeightFormat(qDim.getGrossWeight(), quotation.getCountryMaster().getLocale()));

                        dimensionList.add(dimension);
                        parameters.put("showDimension", "SHOW");
                    }
                }
                EnquiryLog el = null;
                if (quotation != null) {
                    String enquiryNo = quotation.getEnquiryNo();   //Getting enquiryNo from Enquiry
                    el = enquiryDetailRepository.findByEnquiryNo(enquiryNo);  //Getting EnquiryLog Object
                }
                if (el != null && el.getEnquiryDetailList() != null && !el.getEnquiryDetailList().isEmpty()) {
                    for (int i = 0; i < el.getEnquiryDetailList().size(); i++) {
                        for (int j = i; j < quotation.getQuotationDetailList().size(); j++) {
                            if (el.getEnquiryDetailList().get(i).getCommodityMaster() != null) {
                                quotation.getQuotationDetailList().get(i).setCommodityMaster(el.getEnquiryDetailList().get(i).getCommodityMaster());  //Setting the commodityHsName to Quotation
                                parameters.put("commodityGroupName", quotation.getQuotationDetailList().get(i).getCommodityMaster().getHsName());   //Getting the commodityHsName from Quotation and show the report
                            } else {
                                quotation.getQuotationDetailList().get(i).setCommodityMaster(null);
                                parameters.put("commodityGroupName", "");
                            }
                        }
                    }
                }
                parameters.put("repGrossWeight", AppUtil.getWeightFormat(qd.getDimensionGrossWeight(), quotation.getCountryMaster().getLocale()));
                parameters.put("repPiece", AppUtil.getNumberFormat(qd.getTotalNoOfPieces(), quotation.getCountryMaster().getLocale()));
                parameters.put("repVolume", AppUtil.getWeightFormat(qd.getDimensionVolWeight(), quotation.getCountryMaster().getLocale()));

                parameters.put("chargeableWeight", AppUtil.getWeightFormat(qd.getChargebleWeight(), quotation.getCountryMaster().getLocale()));
                parameters.put("grossWeight", AppUtil.getWeightFormat(qd.getGrossWeight(), quotation.getCountryMaster().getLocale()));
                parameters.put("volumeWeight", AppUtil.getWeightFormat(qd.getVolumeWeight(), quotation.getCountryMaster().getLocale()));
            }
            parameters.put("dimensionList", dimensionList);
            parameters.put("originName", originName);
            parameters.put("destinationName", destinationName);
            parameters.put("carrierName", carrierName);
            // General Notes Parameter
            List<GeneralNotes> generalNoteList = new ArrayList<>();
            if (!quotation.getGeneralNoteList().isEmpty()) {
                for (QuotationGeneralNote gnData : quotation.getGeneralNoteList()) {
                    GeneralNotes generalNote = new GeneralNotes();
                    generalNote.setGeneralNote(gnData.getNote());
                    generalNoteList.add(generalNote);
                }
            }
            parameters.put("generalNoteList", generalNoteList);
            parameters.put("noteList", noteList);
            JasperReport jasperReport = JasperCompileManager.compileReport(appResourcePath + "/efs-report/quotation_without_estimation_new_format.jrxml");
            JRDataSource dataSource = new JRBeanCollectionDataSource(quotationFieldCollection, false);
            return JasperFillManager.fillReport(jasperReport, parameters, dataSource);

        } catch (RestException re) {
            log.error("RestException in method while getting the quotation : " + re);
            baseDto.setResponseCode(ErrorCode.QUOTATION_ID_INVALID);
        } catch (Exception e) {
            log.error("Exception in method while getting the quotation : ", e);
            baseDto.setResponseCode(ErrorCode.FAILED);
        }
        log.info("Quotation Without Estimation For Resource [" + data.getResourceId() + "] function ended");
        return null;
    }
}
