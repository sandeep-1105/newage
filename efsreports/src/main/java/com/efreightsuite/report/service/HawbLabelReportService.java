package com.efreightsuite.report.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import com.efreightsuite.dto.AutoAttachmentDto;
import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.ReportDownloadRequestDto;
import com.efreightsuite.enumeration.ReportDownloadOption;
import com.efreightsuite.enumeration.ReportName;
import com.efreightsuite.enumeration.YesNo;
import com.efreightsuite.model.DocumentDetail;
import com.efreightsuite.model.PartyMaster;
import com.efreightsuite.model.ShipmentServiceDetail;
import com.efreightsuite.model.UserProfile;
import com.efreightsuite.service.AuthService;
import lombok.extern.log4j.Log4j2;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRPrintPage;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

import org.springframework.stereotype.Service;

@Service
@Log4j2
public class HawbLabelReportService extends AbstractReportService {
    public ReportName getReportType() {
        return ReportName.HAWB_LABEL;
    }

    public byte[] processReport(ReportDownloadRequestDto data, HttpServletResponse response, boolean isMultiple) {
        log.info("I am in HawbLabelReportService..");
        BaseDto baseDto = new BaseDto();
        UserProfile logInUSer = AuthService.getCurrentUser();
        JasperPrint jasperPrint = getReport(baseDto, data, AuthService.getCurrentUser());

        byte[] a = null;
        AutoAttachmentDto autoAttachmentDto = reportUtil.autoAttachment(ReportName.HAWB_LABEL, YesNo.Yes, data.getResourceId(), "Shipment");
        if (data.getSingle() != null && data.getSingle() == true) {
            if (data.getDownloadOption() == ReportDownloadOption.Preview || data.getDownloadOption() == ReportDownloadOption.Print) {
                reportUtil.japerPrintToResponseOutputStream(data.getDownloadFileType(), jasperPrint, "HAWB_LABEL", response, false, null);
            } else if (autoAttachmentDto != null && autoAttachmentDto.getIsAttach() != null || data.getDownloadOption() == ReportDownloadOption.Download) {
                reportUtil.japerPrintToResponseOutputStream(data.getDownloadFileType(), jasperPrint, "HAWB_LABEL", response, false, autoAttachmentDto);
            }
        }
        if (data.getSingle() != null && data.getSingle() == true && data.getDownloadOption() == ReportDownloadOption.eMail) {
            a = reportUtil.japerPrintToResponseOutputStream(data.getDownloadFileType(), jasperPrint, "HAWB_LABEL", response, false, null);
            shipmentReportMailer.sendShipmentMailToCustomer(data.getResourceId(), logInUSer, ReportName.HAWB_LABEL, a);
        }
        if (isMultiple == true && data.getDownloadOption() == ReportDownloadOption.eMail) {
            a = reportUtil.japerPrintToResponseOutputStream(data.getDownloadFileType(), jasperPrint, "HAWB_LABEL", response, true, null);
            return a;
        }
        return null;
    }

    private JasperPrint getReport(BaseDto baseDto, ReportDownloadRequestDto data, UserProfile loggedInUser) {
        log.info("I am in HawbLabelReportService..");
        Map<String, Object> parameters = new HashMap<>();
        JasperReport jasperReport = null;
        List<Object> fieldCollection1 = new ArrayList<>();
        Object obj1 = new Object();
        fieldCollection1.add(obj1);
        JRDataSource dataSource = new JRBeanCollectionDataSource(fieldCollection1, false);
        try {
            if (data.getDownloadFileType().equals(data.getDownloadFileType().PDF)) {
                jasperReport = JasperCompileManager.compileReport(appResourcePath + "/efs-report/hawb_mawb_label_pdf.jrxml");
            } else if (data.getDownloadFileType().equals(data.getDownloadFileType().XLS) || data.getDownloadFileType().equals(data.getDownloadFileType().RTF)) {
                jasperReport = JasperCompileManager.compileReport(appResourcePath + "/efs-report/hawb_mawb_label_xls_rtf.jrxml");
            }

            parameters.put("mawbHawbAgentAirline", "AGENT");
            Integer noOfPages = 0;
            DocumentDetail documentDetail = documentDetailRepository.findById(data.getResourceId());
            if (documentDetail != null && documentDetail.getId() != null) {
                PartyMaster partyMaster = documentDetailRepository.findByIdForIssuingAgent(documentDetail.getId());
                if (partyMaster != null && partyMaster.getPartyName() != null) {
                    parameters.put("airlineName", reportUtil.removeSpecialCharacters(partyMaster.getPartyName(), data.getDownloadOption()));
                }
            }

            if (documentDetail != null) {
                parameters.put("destination", documentDetail.getDestinationCode() != null ? documentDetail.getDestinationCode() : "");
                parameters.put("totalNoOfPieces", documentDetail.getNoOfPieces() != null ? documentDetail.getNoOfPieces().toString() : "");
                parameters.put("departure", documentDetail.getOriginCode() != null ? documentDetail.getOriginCode() : "");

                if (documentDetail.getNoOfPieces() != null) {
                    Integer tempPages = documentDetail.getNoOfPieces().intValue();
                    noOfPages = tempPages / 4;
                    if (tempPages % 4 > 0) {
                        noOfPages++;
                    }
                }
                ShipmentServiceDetail shipmentServiceDetail = documentDetailRepository.findByIdForShipmentServiceDetail(documentDetail.getId());
                log.info("shipmentServiceDetail.getMawbNo() -- " + shipmentServiceDetail.getMawbNo());
                if (shipmentServiceDetail != null && shipmentServiceDetail.getMawbNo() != null) {
                    parameters.put("barcodeTemplate", shipmentServiceDetail.getMawbNo());
                    parameters.put("airwayBillNo", shipmentServiceDetail.getMawbNo());
                } else {
                    parameters.put("barcodeTemplate", "mawbnoNA");
                }
            }
            JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, dataSource);

            List<JRPrintPage> pages = jasperPrint.getPages();
            log.info("Get Second Page Size :: " + noOfPages);
            noOfPages--;
            for (int j = 0; j < noOfPages; j++) {
                log.info("Get Second Page No :: " + j);
                JRPrintPage object = pages.get(0);
                jasperPrint.addPage(object);
            }

            return jasperPrint;
        } catch (JRException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
