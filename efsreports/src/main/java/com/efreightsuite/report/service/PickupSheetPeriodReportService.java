package com.efreightsuite.report.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import com.efreightsuite.dto.ReportDownloadRequestDto;
import com.efreightsuite.enumeration.ReportName;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.model.DocumentDetail;
import com.efreightsuite.model.LocationMaster;
import com.efreightsuite.model.ShipmentServiceDetail;
import com.efreightsuite.model.ShipmentServiceEvent;
import com.efreightsuite.model.UserProfile;
import com.efreightsuite.report.PickupSheetDto;
import com.efreightsuite.service.AuthService;
import lombok.extern.log4j.Log4j2;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

import org.springframework.stereotype.Service;

@Service
@Log4j2
public class PickupSheetPeriodReportService extends AbstractReportService {
    public ReportName getReportType() {
        return ReportName.PICKUP_SHEET_FOR_THE_PERIOD;
    }

    public byte[] processReport(ReportDownloadRequestDto data, HttpServletResponse response, boolean isMultple) {
        log.info("PickupSheet Report Service method is called .............");
        List<ShipmentServiceDetail> serviceList = shipmentServiceDetailRepository.findByServiceDetail(data.getServiceCode().getServiceCode(), data.getApiFromDate(), data.getApiToDate());
        log.info("Service List Object:::[" + serviceList + "] AND  Seze::: [" + serviceList.size() + "]");
        Map<String, Object> parameters = new HashMap<>();
        List<PickupSheetDto> pickupSheetList = new ArrayList<>();
        try {
            UserProfile loggedInUser = AuthService.getCurrentUser();
            if (serviceList != null && serviceList.size() > 0) {
                for (ShipmentServiceDetail service : serviceList) {
                    if (service != null) {
                        if (loggedInUser != null && loggedInUser.getSelectedUserLocation() != null
                                && service.getSystemTrack() != null && service.getSystemTrack().getCreateDate() != null) {
                            parameters.put("reportDate", reportUtil.dateToReportDate(service.getSystemTrack().getCreateDate(), loggedInUser.getSelectedUserLocation().getJavaDateFormat()));
                            String todayDate = "";
                            todayDate = reportUtil.dateToReportDate(new Date(), loggedInUser.getSelectedUserLocation().getJavaDateFormat());
                            parameters.put("pickupFromDate", reportUtil.dateToReportDate(data.getApiFromDate(), loggedInUser.getSelectedUserLocation().getJavaDateFormat()));
                            parameters.put("pickupToDate", reportUtil.dateToReportDate(data.getApiToDate(), loggedInUser.getSelectedUserLocation().getJavaDateFormat()));

                            parameters.put("serviceCode", data.getServiceCode().getServiceCode());
                            log.info("Service Code from The Object::::" + data.getServiceCode().getServiceCode());
                            if (service != null && service.getDocumentList() != null
                                    && service.getDocumentList().size() > 0) {
                                for (DocumentDetail document : service.getDocumentList()) {
                                    PickupSheetDto pickupSheetDto = new PickupSheetDto();

                                    if (service.getEventList() != null && !service.getEventList().isEmpty()) {
                                        for (ShipmentServiceEvent eventMaster : service.getEventList()) {
                                            pickupSheetDto.setCargoReceivedDate(reportUtil.dateToReportDate(eventMaster.getEventDate(), loggedInUser.getSelectedUserLocation().getJavaDateFormat()));
                                        }
                                    }
                                    if (document != null) {
                                        if (document.getShipper() != null) {
                                            pickupSheetDto.setShipperName(document.getShipper().getPartyName() != null ? document.getShipper().getPartyName() : "");
                                        }
                                        if (document.getConsignee() != null) {
                                            pickupSheetDto.setConsigneeName(document.getConsignee().getPartyName() != null ? document.getConsignee().getPartyName() : "");
                                        }
                                        if (document.getOrigin() != null) {
                                            pickupSheetDto.setOrigin(document.getOrigin().getPortName() != null ? document.getOrigin().getPortName() : "");
                                        }
                                        if (document.getDestination() != null) {
                                            pickupSheetDto.setDestination(document.getDestination().getPortName() != null ? document.getDestination().getPortName() : "");
                                        }
                                        if (document.getPickUpDeliveryPoint() != null && document.getPickUpDeliveryPoint().getPickUpActual() != null) {
                                            String actulPickUpDate = null;
                                            actulPickUpDate = reportUtil.dateToReportDate(document.getPickUpDeliveryPoint().getPickUpActual(), loggedInUser.getSelectedUserLocation().getJavaDateFormat());
                                            pickupSheetDto.setActualPickupDate(actulPickUpDate);
                                        }
                                        if (document.getPickUpDeliveryPoint() != null && document.getPickUpDeliveryPoint().getPickUpPlanned() != null) {
                                            String planedPickUpDate = null;
                                            planedPickUpDate = reportUtil.dateToReportDate(document.getPickUpDeliveryPoint().getPickUpPlanned(), loggedInUser.getSelectedUserLocation().getJavaDateFormat());
                                            pickupSheetDto.setPlannedPickupDate(planedPickUpDate);
                                        }
                                        if (document.getPickUpDeliveryPoint() != null && document.getPickUpDeliveryPoint().getTransporter() != null) {
                                            pickupSheetDto.setTrucker(document.getPickUpDeliveryPoint().getTransporter().getPartyName() != null ? document.getPickUpDeliveryPoint().getTransporter().getPartyName() : "");
                                        }
                                        if (document.getShipmentServiceDetail() != null) {
                                            pickupSheetDto.setShipperId(document.getShipmentServiceDetail().getShipment().getId().toString());
                                        }
                                        pickupSheetDto.setRemarks("");
                                    }
                                    pickupSheetList.add(pickupSheetDto);
                                }
                                parameters.put("refDate", todayDate);
                            }
                        }

                    }
                }
                log.info("Address ------------------------------- "
                        + loggedInUser.getSelectedUserLocation().getAddressLine1());
                String loggedInUserName = "";
                String loggedInLocation = "";
                if (loggedInUser != null) {
                    if (loggedInUser.getEmployee() != null && loggedInUser.getEmployee().getEmployeeName() != null) {
                        loggedInUserName = loggedInUser.getEmployee().getEmployeeName();
                    } else if (loggedInUser.getUserName() != null) {
                        loggedInUserName = loggedInUser.getUserName();
                    } else {
                        loggedInUserName = "";
                    }

                    if (loggedInUser.getSelectedUserLocation() != null) {
                        loggedInLocation = loggedInUser.getSelectedUserLocation().getLocationName() != null
                                ? loggedInUser.getSelectedUserLocation().getLocationName() : "";
                        if (loggedInUser.getSelectedUserLocation() != null) {
                            LocationMaster locationMaster = locationMasterRepository.findByLocationCode(loggedInUser.getSelectedUserLocation().getLocationCode());
                            if (locationMaster != null) {
                                parameters.put("companyAddressLine1", locationMaster.getAddressLine1() != null ? locationMaster.getAddressLine1() : "");
                                parameters.put("companyAddressLine2", locationMaster.getAddressLine2() != null ? locationMaster.getAddressLine2() : "");
                                parameters.put("companyAddressLine3", locationMaster.getAddressLine3() != null ? locationMaster.getAddressLine3() : "");
                                parameters.put("companyName", locationMaster.getBranchName() != null ? locationMaster.getBranchName() : "");
                            }
                        } else {
                            loggedInLocation = "";
                        }
                    }
                }
                parameters.put("refPlace", loggedInUserName + "-" + loggedInLocation);
                parameters.put("locationName", loggedInLocation);

            }
            parameters.put("pickupSheetList", pickupSheetList);
            parameters.put("reportName", "PICKUP SHEET FOR THE PERIOD");
            JasperReport jasperReport = JasperCompileManager.compileReport(appResourcePath + "/efs-report/pickup_sheet_for_the_period.jrxml");
            List<Object> fieldCollection1 = new ArrayList<>();
            Object obj1 = new Object();
            fieldCollection1.add(obj1);

            JRDataSource dataSource = new JRBeanCollectionDataSource(fieldCollection1, false);
            JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, dataSource);
            reportUtil.japerPrintToResponseOutputStream(data.getDownloadFileType(), jasperPrint, "PICKUP_SHEET_FOR_THE_PERIOD", response, false, null);
        } catch (RestException e) {
            e.printStackTrace();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        log.info("Pickup Sheet Report Service Completed..... ");
        return null;
    }
}
