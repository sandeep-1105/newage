package com.efreightsuite.report.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import com.efreightsuite.dto.AutoAttachmentDto;
import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.ReportDownloadRequestDto;
import com.efreightsuite.enumeration.ImportExport;
import com.efreightsuite.enumeration.PPCC;
import com.efreightsuite.enumeration.ReportDownloadOption;
import com.efreightsuite.enumeration.ReportName;
import com.efreightsuite.enumeration.WhoRouted;
import com.efreightsuite.enumeration.YesNo;
import com.efreightsuite.model.LocationMaster;
import com.efreightsuite.model.ProvisionItem;
import com.efreightsuite.model.Provisional;
import com.efreightsuite.model.ShipmentServiceDetail;
import com.efreightsuite.model.UserProfile;
import com.efreightsuite.report.JobCostSheet;
import com.efreightsuite.service.AuthService;
import com.efreightsuite.util.AppUtil;
import lombok.extern.log4j.Log4j2;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

import org.springframework.stereotype.Service;

@Service
@Log4j2
public class ConsolProfitabilityShipmentReportService extends AbstractReportService {
    @Override
    public ReportName getReportType() {
        return ReportName.SUBJOB_COST_SHEET;
    }

    public byte[] processReport(ReportDownloadRequestDto data, HttpServletResponse response, boolean isMultiple) {
        log.info("I am in ConsolProfitabilityShipmentReportService.....");
        BaseDto baseDto = new BaseDto();
        UserProfile logInUSer = AuthService.getCurrentUser();
        JasperPrint jasperPrint = getReport(baseDto, data, AuthService.getCurrentUser());

        byte[] a = null;
        AutoAttachmentDto autoAttachmentDto = reportUtil.autoAttachment(ReportName.SUBJOB_COST_SHEET, YesNo.Yes, data.getResourceId(), "Shipment");
        if (data.getSingle() != null && data.getSingle() == true) {
            if (data.getDownloadOption() == ReportDownloadOption.Preview || data.getDownloadOption() == ReportDownloadOption.Print) {
                reportUtil.japerPrintToResponseOutputStream(data.getDownloadFileType(), jasperPrint, "SUBJOB_COST_SHEET", response, false, null);
            } else if (autoAttachmentDto != null && autoAttachmentDto.getIsAttach() != null || data.getDownloadOption() == ReportDownloadOption.Download) {
                reportUtil.japerPrintToResponseOutputStream(data.getDownloadFileType(), jasperPrint, "SUBJOB_COST_SHEET", response, false, autoAttachmentDto);
            }
        }
        if (data.getSingle() != null && data.getSingle() == true && data.getDownloadOption() == ReportDownloadOption.eMail) {
            a = reportUtil.japerPrintToResponseOutputStream(data.getDownloadFileType(), jasperPrint, "SUBJOB_COST_SHEET", response, false, null);
            shipmentReportMailer.sendShipmentMailToCustomer(data.getResourceId(), logInUSer, ReportName.SUBJOB_COST_SHEET, a);
        }
        if (isMultiple == true && data.getDownloadOption() == ReportDownloadOption.eMail) {
            a = reportUtil.japerPrintToResponseOutputStream(data.getDownloadFileType(), jasperPrint, "SUBJOB_COST_SHEET", response, true, null);
            return a;
        }
        return null;
    }

    private JasperPrint getReport(BaseDto baseDto, ReportDownloadRequestDto data, UserProfile loggedInUser) {
        log.info("ResourceId:::::" + data.getResourceId() + "");
        JasperReport jasperReport;
        try {
            jasperReport = JasperCompileManager.compileReport(appResourcePath + "/efs-report/subJob_cost_sheet.jrxml");
            List<Object> fieldCollection1 = new ArrayList<>();
            Object obj1 = new Object();
            fieldCollection1.add(obj1);
            JRDataSource dataSource = new JRBeanCollectionDataSource(fieldCollection1, false);
            Map<String, Object> parameters = new HashMap<>();
            log.info("Sub job cost enter.............");

            ShipmentServiceDetail service = documentDetailRepository.findByIdForShipmentServiceDetail(data.getResourceId());
            if (service != null) {
                log.info("ShipementUID:::::" + service.getShipmentUid());

                String loggedInUserName = "";
                String companyName = "";
                if (loggedInUser != null && loggedInUser.getSelectedUserLocation() != null) {
                    if (loggedInUser.getEmployee() != null
                            && loggedInUser.getEmployee().getEmployeeName() != null) {
                        loggedInUserName = loggedInUser.getEmployee().getEmployeeName();
                    } else if (loggedInUser.getUserName() != null) {
                        loggedInUserName = loggedInUser.getUserName();
                    } else {
                        loggedInUserName = "";
                    }
                    parameters.put("refName", loggedInUserName);
                    parameters.put("refPlace", loggedInUserName + " - " + loggedInUser.getSelectedUserLocation().getLocationName());
                    parameters.put("refDate", reportUtil.dateToReportDate(new Date(), loggedInUser.getSelectedUserLocation().getJavaDateFormat()));
                    String polDate = reportUtil.dateToReportDate(service.getEta(), loggedInUser.getSelectedUserLocation().getJavaDateFormat());
                    String podDate = reportUtil.dateToReportDate(service.getEta(), loggedInUser.getSelectedUserLocation().getJavaDateFormat());

                    String pol = "";
                    String pod = "";
                    if (service != null) {
                        if (service.getPol() != null) {
                            pol = service.getPol().getPortName() != null ? service.getPol().getPortName() : "";
                        }
                        if (service.getPod() != null) {
                            pod = service.getPod().getPortName() != null ? service.getPod().getPortName() : "";
                        }
                        if (service.getPpcc() != null && service.getPpcc().equals(PPCC.Prepaid) || service.getPpcc().equals("Prepaid")) {
                            parameters.put("frieghtName", "PREPAID");
                        } else {
                            parameters.put("frieghtName", "COLLECT");
                        }
                        if (service.getDocumentList() != null) {
                            if (service.getDocumentList().get(0).getAgent() != null && service.getDocumentList().get(0).getAgent().getPartyName() != null) {
                                parameters.put("agentName", service.getDocumentList().get(0).getAgent().getPartyName());
                            }
                        }
                        if (service.getParty() != null) {
                            parameters.put("billingPartyname", reportUtil.removeSpecialCharacters(service.getParty().getPartyName(), data.getDownloadOption()));

                        }
                        if (service.getConsolUid() != null) {
                            parameters.put("jobNumber", service.getConsolUid());
                        }
                        if (service.getServiceMaster() != null && service.getServiceMaster().getImportExport() != null) {
                            if (service.getServiceMaster().getImportExport().equals(ImportExport.Export) || service.getServiceMaster().getImportExport().equals("Export")) {
                                parameters.put("tradeCode", "EXPORT");
                            } else {
                                parameters.put("tradeCode", "IMPORT");
                            }
                        }
                        if (service.getHawbNo() != null) {
                            parameters.put("hblAndHawb", service.getHawbNo());
                        }
                        if (service.getMawbNo() != null) {
                            parameters.put("oblAndMawb", service.getMawbNo());
                        }
                        parameters.put("bookingNo", service.getShipmentUid());
                        if (service.getSalesman() != null) {
                            parameters.put("salesmanName", service.getSalesman().getEmployeeName() != null ? service.getSalesman().getEmployeeName() : "");
                        }
                        if (service.getShipper() != null && service.getShipper().getPartyName() != null) {
                            parameters.put("shipperName", reportUtil.removeSpecialCharacters(service.getShipper().getPartyName(), data.getDownloadOption()));
                        }
                        if (service.getConsignee() != null && service.getConsignee().getPartyName() != null) {
                            parameters.put("consigneeName", reportUtil.removeSpecialCharacters(service.getConsignee().getPartyName(), data.getDownloadOption()));
                        }
                        if (service.getOrigin() != null) {
                            parameters.put("origin", service.getOrigin().getPortName() != null ? service.getOrigin().getPortName() : "");
                        }
                        if (service.getPol() != null) {
                            pol = service.getPol().getPortName() != null ? service.getPol().getPortName() : "";
                        }
                        if (service.getPod() != null) {
                            pod = service.getPod().getPortName() != null ? service.getPod().getPortName() : "";
                        }
                        if (service.getDestination() != null) {
                            parameters.put("fdc", service.getDestination().getPortName() != null ? service.getDestination().getPortName() : "");
                        }
                        if (service.getServiceMaster() != null) {
                            if (service.getServiceMaster().getCostCenter() != null) {
                                parameters.put("costCenter", service.getServiceMaster().getCostCenter().getCostCenterName() != null ? service.getServiceMaster().getCostCenter().getCostCenterName() : "");
                            }
                        }
                        if (service.getBookedGrossWeightUnitKg() != null && service.getCountry() != null) {
                            parameters.put("grossWeight", AppUtil.getWeightFormat(service.getBookedGrossWeightUnitKg(), service.getCountry().getLocale()) + " KG");
                        }
                        if (service.getBookedVolumeWeightUnitKg() != null && service.getCountry() != null) {
                            parameters.put("volumeWeight", AppUtil.getWeightFormat(service.getBookedVolumeWeightUnitKg(), service.getCountry().getLocale()) + " KG");
                        }
                        if (service.getBookedChargeableUnit() != null && service.getCountry() != null) {
                            parameters.put("chargeableWeight", AppUtil.getWeightFormat(service.getBookedChargeableUnit(), service.getCountry().getLocale()) + " KG");
                        }
                        if (service.getWhoRouted() != null && service.getWhoRouted().equals(WhoRouted.Self) || service.getWhoRouted() != null && service.getWhoRouted().equals("self")) {
                            parameters.put("routedBy", "SELF");
                        } else {
                            parameters.put("routedBy", "AGENT");
                        }

                    }

                    if (pol.length() > 0 && polDate.length() > 0) {
                        parameters.put("polAndDate", pol + " / " + polDate);
                    } else {
                        parameters.put("polAndDate", pol + "  " + polDate);
                    }
                    if (pod.length() > 0 && podDate.length() > 0) {
                        parameters.put("podAndDate", pod + " / " + podDate);
                    } else {
                        parameters.put("podAndDate", pod + "  " + podDate);
                    }

                    if (loggedInUser.getSelectedUserLocation().getCurrencyMaster() != null
                            && loggedInUser.getSelectedUserLocation().getCurrencyMaster().getCurrencyCode() != null) {
                        parameters.put("localCurrencyCode", loggedInUser.getSelectedUserLocation().getCurrencyMaster().getCurrencyCode());
                    } else {
                        parameters.put("localCurrencyCode", "");
                    }
                    if (loggedInUser.getSelectedUserLocation() != null) {
                        LocationMaster locationMaster = locationMasterRepository
                                .findByLocationCode(loggedInUser.getSelectedUserLocation().getLocationCode());
                        if (locationMaster != null) {
                            parameters.put("companyAddressLine1", locationMaster.getAddressLine1() != null ? locationMaster.getAddressLine1() : "");
                            parameters.put("companyAddressLine2", locationMaster.getAddressLine2() != null ? locationMaster.getAddressLine2() : "");
                            parameters.put("companyAddressLine3", locationMaster.getAddressLine3() != null ? locationMaster.getAddressLine3() : "");
                            companyName = locationMaster.getBranchName() != null ? locationMaster.getBranchName() : "";
                            parameters.put("companyName", reportUtil.removeSpecialCharacters(companyName, data.getDownloadOption()));
                        }
                    }
                }


                Provisional provisional = provisionalRepository.findByServiceUid(service.getServiceUid());
                log.info("Inside provisional cost report::::::");
                List<JobCostSheet> provisionchargeList = new ArrayList<>();
                Double totalRevenue = 0.0;
                Double totalCost = 0.0;
                Double totalGp = 0.0;
                Double totalNeutral = 0.0;

                if (provisional != null) {
                    if (provisional.getProvisionalItemList() != null && !provisional.getProvisionalItemList().isEmpty()) {

                        for (ProvisionItem provisionItem : provisional.getProvisionalItemList()) {
                            JobCostSheet jobCostSheet = new JobCostSheet();
                            Double revenue = 0.0;
                            Double cost = 0.0;
                            Double gp = 0.0;
                            Double neutral = 0.0;
                            if (provisionItem != null) {
                                if (provisionItem.getChargeMaster() != null) {
                                    if (provisionItem.getChargeMaster().getChargeName() != null) {
                                        jobCostSheet.setChargeName(provisionItem.getChargeMaster().getChargeName());
                                    }
                                }
                                if (provisionItem.getSellLocalAmount() != null) {
                                    revenue = provisionItem.getSellLocalAmount();
                                    totalRevenue = totalRevenue + revenue;
                                    jobCostSheet.setRevenue(AppUtil.getCurrencyFormat(revenue, provisionItem.getSellCurrency().getCountryMaster().getLocale(), provisionItem.getSellCurrency().getDecimalPoint()));
                                }
                                if (provisionItem.getBuyLocalAmount() != null) {
                                    cost = provisionItem.getBuyLocalAmount();
                                    totalCost = totalCost + cost;
                                    jobCostSheet.setCost(AppUtil.getCurrencyFormat(cost, provisionItem.getBuyCurrency().getCountryMaster().getLocale(), provisionItem.getBuyCurrency().getDecimalPoint()));
                                }
                                if (provisionItem.getIsNeutral() != null && provisionItem.getIsNeutral().equals(YesNo.Yes)) {
                                    if (provisionItem.getSellLocalAmount() != null) {
                                        totalNeutral = totalNeutral + provisionItem.getSellLocalAmount();
                                        jobCostSheet.setNeutral(AppUtil.getCurrencyFormat(provisionItem.getSellLocalAmount(), provisionItem.getSellCurrency().getCountryMaster().getLocale(), provisionItem.getSellCurrency().getDecimalPoint()));
                                    }
                                }

                                if (cost != null && cost > 0.0 || revenue != null && revenue > 0.0) {
                                    gp = revenue - cost;
                                    totalGp = totalGp + gp;
                                    if (provisional != null && provisional.getLocalCurrency() != null) {
                                        jobCostSheet.setGp(AppUtil.getCurrencyFormat(gp, loggedInUser.getSelectedUserLocation().getCountryMaster().getLocale(), loggedInUser.getSelectedUserLocation().getCurrencyMaster().getDecimalPoint()));
                                    }
                                }

                            }
                            provisionchargeList.add(jobCostSheet);
                            parameters.put("provisionchargeList", provisionchargeList);
                        }

                    }

                    if (totalRevenue != null && totalRevenue > 0.0) {
                        parameters.put("totalRevenue", AppUtil.getCurrencyFormat(totalRevenue, loggedInUser.getSelectedUserLocation().getCountryMaster().getLocale(), loggedInUser.getSelectedUserLocation().getCurrencyMaster().getDecimalPoint()));
                    }
                    if (totalCost != null && totalCost > 0.0) {
                        parameters.put("totalCost", AppUtil.getCurrencyFormat(totalCost, loggedInUser.getSelectedUserLocation().getCountryMaster().getLocale(), loggedInUser.getSelectedUserLocation().getCurrencyMaster().getDecimalPoint()));
                    }
                    if (totalGp != null && totalGp > 0.0) {
                        totalGp = totalRevenue - totalCost;
                        parameters.put("totalGp", AppUtil.getCurrencyFormat(totalGp, loggedInUser.getSelectedUserLocation().getCountryMaster().getLocale(), loggedInUser.getSelectedUserLocation().getCurrencyMaster().getDecimalPoint()));
                    } else {
                        parameters.put("totalGp", AppUtil.getCurrencyFormat(totalGp, loggedInUser.getSelectedUserLocation().getCountryMaster().getLocale(), loggedInUser.getSelectedUserLocation().getCurrencyMaster().getDecimalPoint()));
                    }
                    if (totalNeutral != null && totalNeutral > 0.0) {
                        parameters.put("totalNeutral", AppUtil.getCurrencyFormat(totalNeutral, loggedInUser.getSelectedUserLocation().getCountryMaster().getLocale(), loggedInUser.getSelectedUserLocation().getCurrencyMaster().getDecimalPoint()));
                    } else {
                        parameters.put("totalNeutral", "");
                    }

                }

                String defaultMasterData = appUtil.getLocationConfig("job.cost.sheet.note", service.getLocation(), false);
                parameters.put("note", defaultMasterData);

                return JasperFillManager.fillReport(jasperReport, parameters, dataSource);
            }
        } catch (JRException e) {
            e.printStackTrace();
        }
        return null;
    }
}
