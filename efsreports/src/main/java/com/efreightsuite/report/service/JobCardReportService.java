package com.efreightsuite.report.service;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletResponse;

import com.efreightsuite.dto.AutoAttachmentDto;
import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.ReportDownloadRequestDto;
import com.efreightsuite.enumeration.DimensionUnit;
import com.efreightsuite.enumeration.ImportExport;
import com.efreightsuite.enumeration.PPCC;
import com.efreightsuite.enumeration.ReportDownloadOption;
import com.efreightsuite.enumeration.ReportName;
import com.efreightsuite.enumeration.ServiceType;
import com.efreightsuite.enumeration.TransportMode;
import com.efreightsuite.enumeration.YesNo;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.model.Consol;
import com.efreightsuite.model.ConsolCharge;
import com.efreightsuite.model.ConsolDocumentDimension;
import com.efreightsuite.model.ConsolReference;
import com.efreightsuite.model.LocationMaster;
import com.efreightsuite.model.LogoMaster;
import com.efreightsuite.model.ShipmentLink;
import com.efreightsuite.model.ShipmentServiceDetail;
import com.efreightsuite.model.UserProfile;
import com.efreightsuite.report.InvoiceDetail;
import com.efreightsuite.report.JobCardChargeData;
import com.efreightsuite.report.ShippingInstruction;
import com.efreightsuite.service.AuthService;
import com.efreightsuite.util.AppUtil;
import lombok.extern.log4j.Log4j2;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

import org.springframework.stereotype.Service;

@Service
@Log4j2
public class JobCardReportService extends AbstractReportService {
    public ReportName getReportType() {
        return ReportName.JOB_CARD;
    }

    public byte[] processReport(ReportDownloadRequestDto data, HttpServletResponse response, boolean isMultiple) {
        log.info("I am in JobCardReportService..");
        BaseDto baseDto = new BaseDto();
        UserProfile logInUSer = AuthService.getCurrentUser();
        JasperPrint jasperPrint = getReport(baseDto, data, AuthService.getCurrentUser());

        byte[] a = null;
        AutoAttachmentDto autoAttachmentDto = reportUtil.autoAttachment(ReportName.JOB_CARD, YesNo.Yes, data.getResourceId(), "MasterShipment");
        if (data.getSingle() != null && data.getSingle() == true) {
            if (data.getDownloadOption() == ReportDownloadOption.Preview || data.getDownloadOption() == ReportDownloadOption.Print) {
                reportUtil.japerPrintToResponseOutputStream(data.getDownloadFileType(), jasperPrint, "JOB_CARD", response, false, null);
            } else if (autoAttachmentDto != null && autoAttachmentDto.getIsAttach() != null || data.getDownloadOption() == ReportDownloadOption.Download) {
                reportUtil.japerPrintToResponseOutputStream(data.getDownloadFileType(), jasperPrint, "JOB_CARD", response, false, autoAttachmentDto);
            }
        }
        if (data.getSingle() != null && data.getSingle() == true && data.getDownloadOption() == ReportDownloadOption.eMail) {
            a = reportUtil.japerPrintToResponseOutputStream(data.getDownloadFileType(), jasperPrint, "JOB_CARD", response, false, null);
            consolReportMailer.sendConsolMailToCustomer(data.getResourceId(), logInUSer, ReportName.JOB_CARD, a);
        }
        if (isMultiple == true && data.getDownloadOption() == ReportDownloadOption.eMail) {
            a = reportUtil.japerPrintToResponseOutputStream(data.getDownloadFileType(), jasperPrint, "JOB_CARD", response, true, null);
            return a;
        }
        return null;
    }

    private JasperPrint getReport(BaseDto baseDto, ReportDownloadRequestDto data, UserProfile loggedInUser) {
        log.info("Entering  Job Card ReportService..");
        log.info("Job Card[" + data.getResourceId() + "] function called");
        Consol consolEntity = consolRepository.findById(data.getResourceId());
        Map<String, Object> parameters = new HashMap<>();
        if (consolEntity != null) {
            try {
                if (consolEntity.getConsolDocument() != null) {
                    parameters.put("mawbNumber", consolEntity.getConsolDocument().getMawbNo() != null ? consolEntity.getConsolDocument().getMawbNo() : "");
                    parameters.put("hawbNumber", consolEntity.getConsolDocument().getDocumentNo() != null ? consolEntity.getConsolDocument().getDocumentNo() : "");
                    if (consolEntity.getConsolDocument().getConsignee() != null && consolEntity.getConsolDocument().getConsignee().getPartyName() != null) {
                        parameters.put("consigneeName", reportUtil.removeSpecialCharacters(consolEntity.getConsolDocument().getConsignee().getPartyName(), data.getDownloadOption()));
                    }
                    if (consolEntity.getConsolDocument().getConsigneeAddress() != null) {
                        parameters.put("consigneeAddressLine1", consolEntity.getConsolDocument().getConsigneeAddress().getAddressLine1() != null ? consolEntity.getConsolDocument().getConsigneeAddress().getAddressLine1() : "");
                        parameters.put("consigneeAddressLine2", consolEntity.getConsolDocument().getConsigneeAddress().getAddressLine2() != null ? consolEntity.getConsolDocument().getConsigneeAddress().getAddressLine2() : "");
                    }
                    if (consolEntity.getConsolDocument().getConsigneeAddress() != null && consolEntity.getConsolDocument().getConsigneeAddress().getAddressLine3() != null) {
                        parameters.put("consigneeAddressLine3", consolEntity.getConsolDocument().getConsigneeAddress().getAddressLine3());
                    } else {
                        parameters.put("consigneeAddressLine3", "");
                    }
                    if (consolEntity.getConsolDocument().getShipper() != null && consolEntity.getConsolDocument().getShipper().getPartyName() != null) {
                        parameters.put("shipperName", reportUtil.removeSpecialCharacters(consolEntity.getConsolDocument().getShipper().getPartyName(), data.getDownloadOption()));
                    }
                    if (consolEntity.getConsolDocument().getShipperAddress() != null) {
                        parameters.put("shipperAddressLine1", consolEntity.getConsolDocument().getShipperAddress().getAddressLine1() != null ? consolEntity.getConsolDocument().getShipperAddress().getAddressLine1() : "");
                        parameters.put("shipperAddressLine2", consolEntity.getConsolDocument().getShipperAddress().getAddressLine2() != null ? consolEntity.getConsolDocument().getShipperAddress().getAddressLine2() : "");
                    }
                    String address3 = "";
                    String address4 = "";
                    if (consolEntity.getConsolDocument().getShipperAddress() != null && consolEntity.getConsolDocument().getShipperAddress().getAddressLine3() != null) {
                        address3 = consolEntity.getConsolDocument().getShipperAddress().getAddressLine3();
                    } else {
                        address3 = "";
                    }
                    if (consolEntity.getConsolDocument().getShipperAddress() != null && consolEntity.getConsolDocument().getShipperAddress().getAddressLine4() != null) {
                        address4 = consolEntity.getConsolDocument().getShipperAddress().getAddressLine4();
                    } else {
                        address4 = "";
                    }
                    parameters.put("shipperAddressLine3", address3 + " " + address4);

                    if (consolEntity.getConsolDocument().getFirstNotify() != null &&
                            consolEntity.getConsolDocument().getFirstNotify().getPartyName() != null) {
                        parameters.put("firstNotifyName", reportUtil.removeSpecialCharacters(consolEntity.getConsolDocument().getFirstNotify().getPartyName(), data.getDownloadOption()));
                    }
                    if (consolEntity.getConsolDocument().getFirstNotifyAddress() != null) {
                        parameters.put("firstNotifyAddressLine1", consolEntity.getConsolDocument().getFirstNotifyAddress().getAddressLine1() != null ? consolEntity.getConsolDocument().getFirstNotifyAddress().getAddressLine1() : "");
                        parameters.put("firstNotifyAddressLine2", consolEntity.getConsolDocument().getFirstNotifyAddress().getAddressLine2() != null ? consolEntity.getConsolDocument().getFirstNotifyAddress().getAddressLine2() : "");
                        parameters.put("firstNotifyAddressLine3", consolEntity.getConsolDocument().getFirstNotifyAddress().getAddressLine3() != null ? consolEntity.getConsolDocument().getFirstNotifyAddress().getAddressLine3() : "");
                        parameters.put("firstNotifyAddressLine4", consolEntity.getConsolDocument().getFirstNotifyAddress().getAddressLine4() != null ? consolEntity.getConsolDocument().getFirstNotifyAddress().getAddressLine4() : "");
                    }
                    if (consolEntity.getConsolDocument().getCommodityDescription() != null) {
                        parameters.put("commodityDescription", consolEntity.getConsolDocument().getCommodityDescription());
                    }
                    if (consolEntity.getConsolDocument().getDocumentNo() != null) {
                        parameters.put("customerOrderNo", consolEntity.getConsolDocument().getDocumentNo());
                    }


                    if (consolEntity.getConsolDocument().getDimensionUnit() != null) {
                        if (consolEntity.getConsolDocument().getDimensionUnit().equals(DimensionUnit.CENTIMETERORKILOS)) {
                            parameters.put("kgLB", "KG");
                            parameters.put("centimeterOrInch", "cm");
                        } else {
                            parameters.put("kgLB", "KG");
                            parameters.put("centimeterOrInch", "in");
                        }
                    } else {
                        parameters.put("kgLB", "");
                        parameters.put("centimeterOrInch", "");

                    }

                    List<ShippingInstruction> jobCardDimensionList = new ArrayList<>();
                    if (consolEntity.getConsolDocument().getDimensionList() != null && !consolEntity.getConsolDocument().getDimensionList().isEmpty()) {
                        for (ConsolDocumentDimension dimesionList : consolEntity.getConsolDocument().getDimensionList()) {
                            ShippingInstruction dimensionDto = new ShippingInstruction();
                            if (dimesionList != null) {
                                dimensionDto.setHeight(AppUtil.getNumberFormat(dimesionList.getHeight(), consolEntity.getCountry().getLocale()));
                                dimensionDto.setLength(AppUtil.getNumberFormat(dimesionList.getLength(), consolEntity.getCountry().getLocale()));
                                dimensionDto.setWidth(AppUtil.getNumberFormat(dimesionList.getWidth(), consolEntity.getCountry().getLocale()));
                                dimensionDto.setPieces(AppUtil.getNumberFormat(dimesionList.getNoOfPiece(), consolEntity.getCountry().getLocale()));
                                dimensionDto.setVolume(AppUtil.getWeightFormat(dimesionList.getVolWeight(), consolEntity.getCountry().getLocale()));
                            }
                            jobCardDimensionList.add(dimensionDto);    //Adding dimension Object
                        }
                    }
                    parameters.put("jobCardDimensionList", jobCardDimensionList);    //pass the dimensionList
                }
                //if size equal to One then only print billing customer name otherwise no need to show
                if (consolEntity.getShipmentLinkList() != null && consolEntity.getShipmentLinkList().size() == 1) {
                    for (ShipmentLink shipmentLink : consolEntity.getShipmentLinkList()) {
                        ShipmentServiceDetail serviceDetail = shipmentLink.getService();
                        if (serviceDetail != null) {
                            if (serviceDetail.getParty() != null) {
                                parameters.put("billingCustomerName", serviceDetail.getParty().getPartyName() != null ? serviceDetail.getParty().getPartyName() : "");
                                //parameters.put("billingCustomerAddressLine1","");
                            }


                        }
                        //If below three condition are satisfy then only pass parameter values

                        if (serviceDetail.getShipment() != null) {
                            for (ShipmentServiceDetail sd : serviceDetail.getShipment().getShipmentServiceList()) {

                                if (sd.getServiceMaster() != null && sd.getServiceMaster().getServiceType() != null && sd.getServiceMaster().getServiceType().equals(ServiceType.Clearance) &&
                                        sd.getServiceMaster() != null && sd.getServiceMaster().getImportExport() != null && sd.getServiceMaster().getImportExport().equals(ImportExport.Import) &&
                                        sd.getServiceMaster() != null && sd.getServiceMaster().getTransportMode() != null && sd.getServiceMaster().getTransportMode().equals(TransportMode.Air)) {
                                    if (sd.getConsolUid() != null) {
                                        parameters.put("clearanceJobNo", sd.getConsolUid()); //Consol UID
                                        parameters.put("clearanceSegmentCode", sd.getServiceCode());  //Service Code
                                        parameters.put("clearanceSubJobNo", sd.getServiceUid());
                                    }
                                }
                            }
                        }
                    }

                }

                parameters.put("jobNumber", consolEntity.getConsolUid() != null ? consolEntity.getConsolUid() : "");

                if (consolEntity.getCarrier() != null) {
                    parameters.put("carrierName", consolEntity.getCarrier().getCarrierName() != null ? consolEntity.getCarrier().getCarrierName() : "");
                }
                if (consolEntity.getCarrier() != null) {
                    parameters.put("carrierNumber", consolEntity.getCarrier().getCarrierNo() != null ? consolEntity.getCarrier().getCarrierNo() : "");
                }
                if (consolEntity.getReferenceList() != null && !consolEntity.getReferenceList().isEmpty()) {
                    ConsolReference consolReference = consolEntity.getReferenceList().get(0);
                    if (consolReference != null) {
                        parameters.put("customerRefNo", consolReference.getReferenceNumber() != null ? consolReference.getReferenceNumber() : "");
                    }
                }

                if (consolEntity.getPpcc().equals(PPCC.Prepaid) || consolEntity.getPpcc().equals("Prepaid")) {
                    parameters.put("frieghtName", " FREIGHT PREPAID");
                } else {
                    parameters.put("frieghtName", "FREIGHT COLLECT");
                }
                String origin = "";
                String destination = "";
                if (consolEntity.getOrigin() != null) {
                    origin = consolEntity.getOrigin().getPortName() != null ? consolEntity.getOrigin().getPortName() : "";
                } else {
                    origin = "";
                }
                if (consolEntity.getDestination() != null) {
                    destination = consolEntity.getDestination().getPortName() != null ? consolEntity.getDestination().getPortName() : "";
                }
                parameters.put("porAndFdc", origin + " / " + destination);
                if (consolEntity.getOrigin() != null && consolEntity.getAgent().getPartyName() != null) {
                    parameters.put("originAgentName", reportUtil.removeSpecialCharacters(consolEntity.getAgent().getPartyName(), data.getDownloadOption()));
                }


                Double totalLocalAmount = 0.0;
                List<InvoiceDetail> jobCardChargeList = new ArrayList<>();
                if (consolEntity.getChargeList() != null && !consolEntity.getChargeList().isEmpty()) {
                    for (ConsolCharge consolCharge : consolEntity.getChargeList()) {
                        InvoiceDetail invoice = new InvoiceDetail();
                        invoice.setChargeName(consolCharge.getChargeName() != null ? consolCharge.getChargeName() : "");
                        invoice.setCurrencyCode(consolCharge.getCurrency().getCurrencyCode() != null ? consolCharge.getCurrency().getCurrencyCode() : "");
                        if (consolCharge.getCurrencyRoe() != null) {
                            invoice.setExchangeRate(AppUtil.getCurrencyFormat(consolCharge.getCurrencyRoe(), consolCharge.getCurrency().getCountryMaster().getLocale(), consolCharge.getCurrency().getDecimalPoint()));
                        }
                        if (consolCharge.getAmount() != null) {
                            invoice.setAmount(AppUtil.getCurrencyFormat(consolCharge.getAmount(), consolCharge.getCurrency().getCountryMaster().getLocale(), consolCharge.getCurrency().getDecimalPoint()));
                        }
                        if (consolCharge.getLocalAmount() != null) {
                            totalLocalAmount += consolCharge.getLocalAmount();
                            invoice.setMultiplyLocalAmount(AppUtil.getCurrencyFormat(consolCharge.getLocalAmount(), consolCharge.getCurrency().getCountryMaster().getLocale(), consolCharge.getCurrency().getDecimalPoint()));
                        }
                        jobCardChargeList.add(invoice);
                    }
                }

                if (data.getShowCharge() != null && data.getShowCharge().equals(YesNo.Yes)) {
                    parameters.put("jobCardChargeList", jobCardChargeList);
                    if (totalLocalAmount != null && totalLocalAmount > 0.0) {
                        parameters.put("totalLocalAmount", AppUtil.getCurrencyFormat(totalLocalAmount, consolEntity.getCurrency().getCountryMaster().getLocale(), consolEntity.getCurrency().getDecimalPoint()));
                    }
                }
                List<JobCardChargeData> totalChargeList = new ArrayList<>();
                if (consolEntity != null && consolEntity.getConsolDocument() != null) {
                    JobCardChargeData jobCardChargeData = new JobCardChargeData();
                    jobCardChargeData.setNoOfpacks(consolEntity.getConsolDocument().getNoOfPieces() != null ? AppUtil.getNumberFormat(consolEntity.getConsolDocument().getNoOfPieces(), consolEntity.getCountry().getLocale()) : "");
                    jobCardChargeData.setGrossWeight(consolEntity.getConsolDocument().getGrossWeight() != null ? AppUtil.getWeightFormat(consolEntity.getConsolDocument().getGrossWeight(), consolEntity.getCountry().getLocale()) + " KG " : "");
                    jobCardChargeData.setVolumeWeight(consolEntity.getConsolDocument().getVolumeWeight() != null ? AppUtil.getWeightFormat(consolEntity.getConsolDocument().getVolumeWeight(), consolEntity.getCountry().getLocale()) + " KG" : "");
                    jobCardChargeData.setChargeableWeight(consolEntity.getConsolDocument().getChargebleWeight() != null ? AppUtil.getWeightFormat(consolEntity.getConsolDocument().getChargebleWeight(), consolEntity.getCountry().getLocale()) + " KG" : "");
                    totalChargeList.add(jobCardChargeData);
                }
                parameters.put("totalChargeList", totalChargeList);

                if (loggedInUser != null && loggedInUser.getSelectedUserLocation() != null) {
                    String todayDate = "";
                    String jobHeaderDate = "";
                    jobHeaderDate = reportUtil.dateToReportDate(new Date(), loggedInUser.getSelectedUserLocation().getJavaDateFormat());
                    todayDate = reportUtil.dateToReportDate(new Date(), loggedInUser.getSelectedUserLocation().getJavaDateFormat());
                    parameters.put("refDate", todayDate);
                    parameters.put("jobHeaderDate", jobHeaderDate);
                    String loggedInUserName = "";
                    String loggedInLocation = "";
                    String companyName = "";
                    if (loggedInUser.getEmployee() != null && loggedInUser.getEmployee().getEmployeeName() != null) {
                        loggedInUserName = loggedInUser.getEmployee().getEmployeeName();
                    } else if (loggedInUser.getUserName() != null) {
                        loggedInUserName = loggedInUser.getUserName();
                    } else {
                        loggedInUserName = "";
                    }

                    if (loggedInUser.getSelectedUserLocation() != null) {
                        loggedInLocation = loggedInUser.getSelectedUserLocation().getLocationName() != null
                                ? loggedInUser.getSelectedUserLocation().getLocationName() : "";
                        LocationMaster locationMaster = locationMasterRepository.findByLocationCode(loggedInUser.getSelectedUserLocation().getLocationCode());
                        if (locationMaster != null) {
                            parameters.put("companyAddressLine1", locationMaster.getAddressLine1() != null ? locationMaster.getAddressLine1() : "");
                            parameters.put("companyAddressLine2", locationMaster.getAddressLine2() != null ? locationMaster.getAddressLine2() : "");
                            parameters.put("companyAddressLine3", locationMaster.getAddressLine3() != null ? locationMaster.getAddressLine3() : "");
                            companyName = locationMaster.getBranchName() != null ? locationMaster.getBranchName() : "";
                        }
                        LogoMaster logoMaster = logoMasterRepository.findByLocationMaster(loggedInUser.getSelectedUserLocation());
                        if (logoMaster != null && logoMaster.getLogo() != null) {
                            InputStream in = new ByteArrayInputStream(logoMaster.getLogo());
                            BufferedImage bImageFromConvert = ImageIO.read(in);
                            parameters.put("logo", bImageFromConvert);
                        }
                    } else {
                        loggedInLocation = "";
                    }
                    parameters.put("refPlace", loggedInUserName + "-" + loggedInLocation);
                    parameters.put("companyName", reportUtil.removeSpecialCharacters(companyName, data.getDownloadOption()));
                    parameters.put("salesmanName", loggedInUserName);
                    if (consolEntity.getEtd() != null) {
                        parameters.put("etdDate", reportUtil.dateToReportDate(consolEntity.getEtd(), loggedInUser.getSelectedUserLocation().getJavaDateFormat()));
                    }
                    if (consolEntity.getEta() != null) {
                        parameters.put("etaDate", reportUtil.dateToReportDate(consolEntity.getEta(), loggedInUser.getSelectedUserLocation().getJavaDateFormat()));
                    }
                    if (consolEntity.getConsolDocument() != null) {
                        if (consolEntity.getConsolDocument().getMawbGeneratedDate() != null) {
                            parameters.put("mawbDate", reportUtil.dateToReportDate(consolEntity.getConsolDocument().getMawbGeneratedDate(), loggedInUser.getSelectedUserLocation().getJavaDateFormat()));
                        } else {
                            parameters.put("mawbDate", "");
                        }
                        String flighNumber = "";
                        flighNumber = consolEntity.getConsolDocument().getRouteNo() != null ? consolEntity.getConsolDocument().getRouteNo() : "";
                        //parameters.put("flightNoDate", flighNumber+"/"+reportUtil.dateToReportDate(consolEntity.getEtd(), loggedInUser.getSelectedUserLocation().getJavaDateFormat()));

                        String etaDte = reportUtil.dateToReportDate(consolEntity.getEta(), loggedInUser.getSelectedUserLocation().getJavaDateFormat());

                        if (flighNumber.length() > 0 && etaDte.length() > 0) {
                            parameters.put("flightNoDate", flighNumber + " / " + etaDte);
                        } else {
                            parameters.put("flightNoDate", flighNumber + " " + etaDte);
                        }


                    }
                }
                parameters.put("reportName", "Job Card");

                JasperReport jasperReport = JasperCompileManager.compileReport(appResourcePath + "/efs-report/job_card.jrxml");
                List<Object> fieldCollection1 = new ArrayList<>();
                Object obj1 = new Object();
                fieldCollection1.add(obj1);
                JRDataSource dataSource = new JRBeanCollectionDataSource(fieldCollection1, false);
                JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, dataSource);
                return jasperPrint;
            } catch (RestException re) {
                log.error("RestException occures in Export Job card  method  : ", re);
            } catch (Exception exception) {
                log.error("RestException occures in Export Job card  :: ", exception);
                baseDto.setResponseCode(ErrorCode.FAILED);
            }
            log.info("Job card   [" + data.getResourceId() + "] method ended");
        }
        return null;
    }
}
