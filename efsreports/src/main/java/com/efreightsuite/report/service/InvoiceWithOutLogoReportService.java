package com.efreightsuite.report.service;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletResponse;

import com.efreightsuite.dto.AutoAttachmentDto;
import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.ReportDownloadRequestDto;
import com.efreightsuite.enumeration.CustomerAgent;
import com.efreightsuite.enumeration.ImportExport;
import com.efreightsuite.enumeration.QuoteType;
import com.efreightsuite.enumeration.ReportDownloadOption;
import com.efreightsuite.enumeration.ReportName;
import com.efreightsuite.enumeration.YesNo;
import com.efreightsuite.model.*;
import com.efreightsuite.report.CustomerInvoiceDetail;
import com.efreightsuite.service.AuthService;
import com.efreightsuite.util.AppUtil;
import com.efreightsuite.util.NumberToWords;
import lombok.extern.log4j.Log4j2;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

import org.springframework.stereotype.Service;

@Service
@Log4j2
public class InvoiceWithOutLogoReportService extends AbstractReportService {
    @Override
    public ReportName getReportType() {
        return ReportName.INVOICE_WITH_OUT_LOGO;
    }

    public byte[] processReport(ReportDownloadRequestDto data, HttpServletResponse response, boolean isMultiple) {
        log.info("I am in InvoiceWithOutLogoReportService.....");
        BaseDto baseDto = new BaseDto();
        UserProfile logInUser = AuthService.getCurrentUser();
        JasperPrint jasperPrint = getReport(baseDto, data, AuthService.getCurrentUser());
        byte[] a = null;
        AutoAttachmentDto autoAttachmentDto = reportUtil.autoAttachment(ReportName.INVOICE_WITH_OUT_LOGO, YesNo.Yes, data.getResourceId(), "Invoice");
        if (data.getSingle() != null && data.getSingle() == true) {
            if (data.getDownloadOption() == ReportDownloadOption.Preview || data.getDownloadOption() == ReportDownloadOption.Print) {
                reportUtil.japerPrintToResponseOutputStream(data.getDownloadFileType(), jasperPrint, "INVOICE_WITH_OUT_LOGO", response, false, null);
            } else if (autoAttachmentDto != null && autoAttachmentDto.getIsAttach() != null || data.getDownloadOption() == ReportDownloadOption.Download) {
                reportUtil.japerPrintToResponseOutputStream(data.getDownloadFileType(), jasperPrint, "INVOICE_WITH_OUT_LOGO", response, false, autoAttachmentDto);
            }
        }
        if (data.getSingle() != null && data.getSingle() == true && data.getDownloadOption() == ReportDownloadOption.eMail) {
            a = reportUtil.japerPrintToResponseOutputStream(data.getDownloadFileType(), jasperPrint, "INVOICE_WITH_OUT_LOGO", response, false, null);
            invoiceSingleReportMailer.sendInvoiceMailCustomer(data.getResourceId(), logInUser, ReportName.INVOICE_WITH_OUT_LOGO, a);
        }
        if (isMultiple == true && data.getDownloadOption() == ReportDownloadOption.eMail) {
            a = reportUtil.japerPrintToResponseOutputStream(data.getDownloadFileType(), jasperPrint, "INVOICE_WITH_OUT_LOGO", response, true, null);
            return a;
        }
        return null;
    }

    private JasperPrint getReport(BaseDto baseDto, ReportDownloadRequestDto data, UserProfile loggedInUser) {
        JasperReport jasperReport;
        try {
            jasperReport = JasperCompileManager.compileReport(appResourcePath + "/efs-report/invoice_with_logo.jrxml");
            List<Object> fieldCollection1 = new ArrayList<>();
            Object obj1 = new Object();
            fieldCollection1.add(obj1);
            JRDataSource dataSource = new JRBeanCollectionDataSource(fieldCollection1, false);
            Map<String, Object> parameters = new HashMap<>();

            String prefix = "";
            String suffix = "";
            String locale = "";
            String companyName = "";
            if (loggedInUser != null && loggedInUser.getSelectedUserLocation() != null) {
                CurrencyMaster cm = loggedInUser.getSelectedUserLocation().getCurrencyMaster();
                if(cm != null) {
                    prefix = cm.getPrefix() != null ? cm.getPrefix() : prefix;
                    suffix = cm.getSuffix() != null ? cm.getSuffix() : suffix;
                }
                LocationMaster locationMaster = locationMasterRepository.findById(loggedInUser.getSelectedUserLocation().getId());
                if (locationMaster != null) {
                    if (locationMaster.getBankAccountName() != null) {
                        parameters.put("acName", locationMaster.getBankAccountName());
                    }
                    if (locationMaster.getBankAccountNumber() != null) {
                        parameters.put("acNo", locationMaster.getBankAccountNumber());
                    }
                    if (locationMaster.getBankName() != null) {
                        parameters.put("bankName", locationMaster.getBankName());
                    }
                    if (locationMaster.getBankAddress() != null) {
                        parameters.put("addressLine1", locationMaster.getBankAddress());
                    }
                    parameters.put("companyAddressLine1", locationMaster.getAddressLine1() != null ? locationMaster.getAddressLine1() : "");
                    parameters.put("companyAddressLine2", locationMaster.getAddressLine2() != null ? locationMaster.getAddressLine2() : "");
                    parameters.put("companyAddressLine3", locationMaster.getAddressLine3() != null ? locationMaster.getAddressLine3() : "");
                    parameters.put("companyAddressLine4", locationMaster.getAddressLine4() != null ? locationMaster.getAddressLine4() : "");
                    companyName = locationMaster.getBranchName() != null ? locationMaster.getBranchName() : "";
                    parameters.put("companyName", reportUtil.removeSpecialCharacters(companyName, data.getDownloadOption()));
                }
                String loggedInUserName = "";
                if (loggedInUser != null) {
                    if (loggedInUser.getEmployee() != null && loggedInUser.getEmployee().getEmployeeName() != null) {
                        loggedInUserName = loggedInUser.getEmployee().getEmployeeName();
                    } else if (loggedInUser.getUserName() != null) {
                        loggedInUserName = loggedInUser.getUserName();
                    } else {
                        loggedInUserName = "";
                    }
                }
                parameters.put("refName", loggedInUserName);
                parameters.put("refPlace", loggedInUser.getSelectedUserLocation().getLocationName());
                parameters.put("refDate", reportUtil.dateToReportDate(new Date(), loggedInUser.getSelectedUserLocation().getJavaDateFormat()));

                BufferedImage image = null;
                try {
                    image = ImageIO.read(new File(appResourcePath + "/efs-report/check.png"));
                } catch (IOException e) {
                    e.printStackTrace();
                }
                byte[] imageInByte = null;

                try {
                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    ImageIO.write(image, "png", baos);
                    baos.flush();
                    imageInByte = baos.toByteArray();
                    baos.close();
                } catch (Exception e) {
                    log.info(e);
                }
                InputStream isImage = new ByteArrayInputStream(imageInByte);
                BufferedImage bImage = null;
                try {
                    bImage = ImageIO.read(isImage);
                } catch (IOException e) {
                    e.printStackTrace();
                }

                List<CustomerInvoiceDetail> chargeList = new ArrayList<>();
                List<CustomerInvoiceDetail> taxList = new ArrayList<>();
                Double totalLocalAmount = 0.0;
                Double totalAmount = 0.0;
                InvoiceCreditNote invoice = invoiceCreditNoteRepository.findById(data.getResourceId());
                if (invoice != null && invoice.getLocation() != null && invoice.getLocation().getCountryMaster().getLocale() != null) {
                    locale = invoice.getLocation().getCountryMaster().getLocale();
                }
                String billingCurrency = "";

                if (invoice != null) {
                    if (invoice.getBillingCurrency() != null) {
                        billingCurrency = invoice.getBillingCurrency().getCurrencyCode() != null ? invoice.getBillingCurrency().getCurrencyCode() : "";
                        parameters.put("localCurrencyCode", billingCurrency);
                    }
                    parameters.put("notes", invoice.getNote() != null ? invoice.getNote() : "");
                    if (invoice.getCustomerAgent() != null && invoice.getCustomerAgent().equals(CustomerAgent.Customer) || invoice.getCustomerAgent().equals("Customer")) {
                        parameters.put("customerLabel", "Customer");
                        String partyAccount = "";
                        partyAccount = invoice.getInvoiceCreditNoteNo() != null ? invoice.getInvoiceCreditNoteNo() : "";
                        String dayBookNo = "";
                        if (invoice.getDaybookMaster() != null) {
                            dayBookNo = invoice.getDaybookMaster().getDaybookCode() != null ? invoice.getDaybookMaster().getDaybookCode() : "";
                        }
                        if (invoice.getParty() != null && invoice.getParty().getPartyName() != null) {
                            parameters.put("customerPartyName", reportUtil.removeSpecialCharacters(invoice.getParty().getPartyName(), data.getDownloadOption()));
                        }
                        parameters.put("invoiceFaturaNo", dayBookNo + " - " + partyAccount);
                        if (invoice.getPartyAddress() != null) {
                            String addressLine4 = "";
                            parameters.put("customerAddressLine1", invoice.getPartyAddress().getAddressLine1() != null ? invoice.getPartyAddress().getAddressLine1() : "");
                            parameters.put("customerAddressLine2", invoice.getPartyAddress().getAddressLine2() != null ? invoice.getPartyAddress().getAddressLine2() : "");
                            parameters.put("customerAddressLine3", invoice.getPartyAddress().getAddressLine3() != null ? invoice.getPartyAddress().getAddressLine3() : "");
                            addressLine4 = invoice.getPartyAddress().getAddressLine4() != null ? invoice.getPartyAddress().getAddressLine4() : "";
                            String phoneNumber = "";
                            String faxNumber = "";
                            phoneNumber = invoice.getPartyAddress().getPhone() != null ? invoice.getPartyAddress().getPhone() : "";
                            faxNumber = invoice.getPartyAddress().getFax() != null ? invoice.getPartyAddress().getFax() : "";
                            if (phoneNumber.length() > 0 && faxNumber.length() > 0 && addressLine4.length() > 0) {
                                parameters.put("customerAddressLine4", addressLine4 + " PH : " + phoneNumber + "  " + "FAX : " + faxNumber);
                            } else if (phoneNumber.length() > 0) {
                                parameters.put("customerAddressLine4", addressLine4 + " PH : " + phoneNumber);
                            } else if (phoneNumber.length() > 0) {
                                parameters.put("customerAddressLine4", addressLine4 + " FAX : " + faxNumber);
                            } else {
                                parameters.put("customerAddressLine4", addressLine4);
                            }
                        }
                    } else if (invoice.getCustomerAgent() != null
                            && invoice.getCustomerAgent().equals(CustomerAgent.Agent) || invoice.getCustomerAgent().equals("Agent")) {
                        parameters.put("customerLabel", "Agent");

                        String partyAccount = "";
                        partyAccount = invoice.getInvoiceCreditNoteNo() != null ? invoice.getInvoiceCreditNoteNo() : "";
                        parameters.put("invoiceFaturaNo", partyAccount);
                        String dayBookNo = "";
                        if (invoice.getDaybookMaster() != null) {
                            dayBookNo = invoice.getDaybookMaster().getDaybookCode() != null ? invoice.getDaybookMaster().getDaybookCode() : "";
                        }
                        parameters.put("invoiceFaturaNo", dayBookNo + " - " + partyAccount);
                        String subLedger = invoice.getSubledger() != null ? invoice.getSubledger() : "";
                        parameters.put("customerPartyAccAndSubLedger", partyAccount + "  -  " + subLedger);
                        if (invoice.getParty() != null && invoice.getParty().getPartyName() != null) {
                            parameters.put("customerPartyName", reportUtil.removeSpecialCharacters(invoice.getParty().getPartyName(), data.getDownloadOption()));
                        }
                        if (invoice.getPartyAddress() != null) {
                            String addressLine4 = "";
                            parameters.put("customerAddressLine1", invoice.getPartyAddress().getAddressLine1() != null
                                    ? invoice.getPartyAddress().getAddressLine1() : "");
                            parameters.put("customerAddressLine2", invoice.getPartyAddress().getAddressLine2() != null
                                    ? invoice.getPartyAddress().getAddressLine2() : "");
                            parameters.put("customerAddressLine3", invoice.getPartyAddress().getAddressLine3() != null
                                    ? invoice.getPartyAddress().getAddressLine3() : "");
                            addressLine4 = invoice.getPartyAddress().getAddressLine4() != null ? invoice.getPartyAddress().getAddressLine4() : "";
                            String phoneNumber = "";
                            String faxNumber = "";
                            phoneNumber = invoice.getPartyAddress().getPhone() != null ? invoice.getPartyAddress().getPhone() : "";
                            faxNumber = invoice.getPartyAddress().getFax() != null ? invoice.getPartyAddress().getFax() : "";
                            if (phoneNumber.length() > 0 && faxNumber.length() > 0 && addressLine4.length() > 0) {
                                parameters.put("customerAddressLine4", addressLine4 + " PH : " + phoneNumber + "  " + "FAX : " + faxNumber);
                            } else if (phoneNumber.length() > 0) {
                                parameters.put("customerAddressLine4", addressLine4 + " PH : " + phoneNumber);
                            } else if (phoneNumber.length() > 0) {
                                parameters.put("customerAddressLine4", addressLine4 + " FAX : " + faxNumber);
                            } else {
                                parameters.put("customerAddressLine4", addressLine4);
                            }
                        }
                    }
                    if (invoice.getDueDate() != null) {
                        parameters.put("paymentDueDate", reportUtil.dateToReportDate(invoice.getDueDate(),
                                loggedInUser.getSelectedUserLocation().getJavaDateFormat()));
                    }
                    if (invoice.getInvoiceCreditNoteDate() != null) {
                        parameters.put("invoiceFaturaDate", reportUtil.dateToReportDate(invoice.getInvoiceCreditNoteDate(),
                                loggedInUser.getSelectedUserLocation().getJavaDateFormat()));
                    }
                    if (invoice.getInvoiceCreditNoteDetailList() != null && !invoice.getInvoiceCreditNoteDetailList().isEmpty()) {
                        for (InvoiceCreditNoteDetail invoiceDetail : invoice.getInvoiceCreditNoteDetailList()) {
                            CustomerInvoiceDetail customerInvoice = new CustomerInvoiceDetail();
                            if (invoiceDetail != null) {
                                if (invoiceDetail.getChargeMaster() != null) {
                                    customerInvoice.setChargeName(invoiceDetail.getChargeMaster().getChargeName() != null ? invoiceDetail.getChargeMaster().getChargeName() : "");
                                }
                                if (invoiceDetail.getCurrencyMaster() != null) {
                                    customerInvoice.setCurrencyCode(invoiceDetail.getCurrencyMaster().getCurrencyCode() != null ? invoiceDetail.getCurrencyMaster().getCurrencyCode() : "");
                                }
                                if (invoiceDetail.getUnit() != null) {
                                    customerInvoice.setUnit(AppUtil.getCurrencyFormat(invoiceDetail.getUnit(), invoiceDetail.getCurrencyMaster().getCountryMaster().getLocale(), invoiceDetail.getCurrencyMaster().getDecimalPoint()));
                                }
                                if (invoiceDetail.getAmountPerUnit() != null) {
                                    customerInvoice.setPerUnit(AppUtil.getCurrencyFormat(invoiceDetail.getAmountPerUnit(), invoiceDetail.getCurrencyMaster().getCountryMaster().getLocale(), invoiceDetail.getCurrencyMaster().getDecimalPoint()));
                                }
                                if (invoiceDetail.getCurrencyAmount() != null) {
                                    customerInvoice.setAmount(AppUtil.getCurrencyFormat(invoiceDetail.getCurrencyAmount(), invoiceDetail.getCurrencyMaster().getCountryMaster().getLocale(), invoiceDetail.getCurrencyMaster().getDecimalPoint()));
                                }
                                if (invoiceDetail.getCurrencyAmount() != null) {
                                    customerInvoice.setAmount(AppUtil.getCurrencyFormat(invoiceDetail.getCurrencyAmount(), invoiceDetail.getCurrencyMaster().getCountryMaster().getLocale(), invoiceDetail.getCurrencyMaster().getDecimalPoint()));
                                }
                                if (invoiceDetail.getRoe() != null) {
                                    log.info("GetROE::::" + invoiceDetail.getRoe());
                                    customerInvoice.setRoe(AppUtil.getCurrencyFormat(invoiceDetail.getRoe(), invoiceDetail.getCurrencyMaster().getCountryMaster().getLocale(), invoiceDetail.getCurrencyMaster().getDecimalPoint()));
                                }
                                if (invoiceDetail.getLocalAmount() != null) {
                                    log.info("LocalAmount::::" + invoiceDetail.getLocalAmount());
                                    customerInvoice.setLocalAmount(AppUtil.getCurrencyFormat(invoiceDetail.getLocalAmount(), invoiceDetail.getCurrencyMaster().getCountryMaster().getLocale(), invoiceDetail.getCurrencyMaster().getDecimalPoint()));
                                    totalLocalAmount = totalLocalAmount + invoiceDetail.getLocalAmount();
                                }
                                if (invoiceDetail.getIsTaxable().equals(true)) {
                                    customerInvoice.setTickMark(bImage);
                                }
                            }
                            chargeList.add(customerInvoice);
                        }
                    }
                    if (totalLocalAmount != null) {
                        parameters.put("totalLocalAmount", AppUtil.getCurrencyFormat(totalLocalAmount, invoice.getBillingCurrency().getCountryMaster().getLocale(), invoice.getBillingCurrency().getDecimalPoint()));
                    }
                    // For Service Tax Code start here............
                    Double taxAmount = 0.0;
                    if (invoice.getInvoiceCreditNoteDetailList() != null && !invoice.getInvoiceCreditNoteDetailList().isEmpty()) {
                        for (InvoiceCreditNoteDetail invoiceCreditNoteDetail : invoice.getInvoiceCreditNoteDetailList()) {
                            if (invoiceCreditNoteDetail != null && !invoiceCreditNoteDetail.getInvoiceCreditNoteTaxList().isEmpty()) {
                                for (InvoiceCreditNoteTax invoiceTax : invoiceCreditNoteDetail.getInvoiceCreditNoteTaxList()) {
                                    CustomerInvoiceDetail customerDetailTax = new CustomerInvoiceDetail();
                                    Double amount = 0.0;
                                    if (invoiceTax != null) {
                                        if (invoiceTax.getServiceTaxPercentage() != null && invoiceTax.getServiceTaxPercentage().getTaxName() != null) {
                                            customerDetailTax.setChargeName(invoiceTax.getServiceTaxPercentage().getTaxName());
                                            log.info("Taxname:::::::::::::" + invoiceTax.getServiceTaxPercentage().getTaxName());
                                        }
                                        if (invoiceTax.getAmount() != null) {
                                            amount = invoiceTax.getAmount();
                                            taxAmount = taxAmount + amount;
                                            customerDetailTax.setLocalAmount(AppUtil.getCurrencyFormat(invoiceTax.getAmount(), invoice.getLocalCurrency().getCountryMaster().getLocale(), invoice.getLocalCurrency().getDecimalPoint()));
                                        }
                                    }
                                    taxList.add(customerDetailTax);
                                }
                                parameters.put("taxList", taxList);
                            }
                        }
                    }
                    if (taxAmount != null) {
                        parameters.put("totalTax", AppUtil.getCurrencyFormat(taxAmount, invoice.getBillingCurrency().getCountryMaster().getLocale(), invoice.getBillingCurrency().getDecimalPoint()));
                    }
                    if (totalLocalAmount != null && totalLocalAmount > 0) {
                        totalAmount = totalLocalAmount + taxAmount;
                        parameters.put("totalTaxAndAmount", AppUtil.getCurrencyFormat(totalAmount, invoice.getBillingCurrency().getCountryMaster().getLocale(), invoice.getBillingCurrency().getDecimalPoint()));
                    }
                    DecimalFormat twoDecFormat = new DecimalFormat("#0.00#");
                    String[] input = twoDecFormat.format(totalAmount).toString().split("\\.");
                    log.info("Input String length::::" + input.length);
                    String mainValue = input[0];
                    String decimalValue = input[1];
                    if (totalAmount > 0.0 && !decimalValue.equals("00")) {
                        log.info("If Inside  Main Value:::[" + mainValue + "] == and DecimalValue::::[" + decimalValue + "]:::Prefix::[" + prefix + "] Suffix:::[" + suffix + "]");
                        String convertedNumbers = NumberToWords.convertPriceIntoString(mainValue, decimalValue, prefix, suffix);
                        parameters.put("convertAmountIntoWords", convertedNumbers);
                    } else {
                        String convertedNumbers = NumberToWords.convertPriceIntoString(mainValue, "", prefix, "");
                        parameters.put("convertAmountIntoWords", convertedNumbers);
                    }

                    if (invoice.getShipmentUid() != null && invoice.getShipmentServiceDetail() != null) {
                        parameters.put("invoiceShipmentUid", invoice.getShipmentUid());
                        ShipmentServiceDetail service = shipmentServiceDetailRepository.findById(invoice.getShipmentServiceDetail().getId());
                        if (service != null) {
                            if (service.getServiceMaster() != null && service.getServiceMaster().getImportExport().equals(ImportExport.Export)) {
                                if (service.getDocumentList() != null && !service.getDocumentList().isEmpty()) {
                                    for (DocumentDetail document : service.getDocumentList()) {
                                        if (document != null) {
                                            if (document.getShipper() != null && document.getShipper().getPartyName() != null) {
                                                parameters.put("shipperName", reportUtil.removeSpecialCharacters(document.getShipper().getPartyName(), data.getDownloadOption()));
                                            }
                                            if (document.getConsignee() != null && document.getConsignee().getPartyName() != null) {
                                                parameters.put("consigneeLabel", "Consignee");
                                                parameters.put("consigneeName", reportUtil.removeSpecialCharacters(document.getConsignee().getPartyName(), data.getDownloadOption()));
                                            }
                                            parameters.put("hawbNo", document.getDocumentNo() != null ? document.getDocumentNo() : "");
                                        }
                                    }
                                }

                            } else if (service.getServiceMaster() != null && service.getServiceMaster().getImportExport().equals(ImportExport.Import)) {
                                if (service.getDocumentList() != null && !service.getDocumentList().isEmpty()) {
                                    for (DocumentDetail document : service.getDocumentList()) {
                                        if (document != null) {
                                            if (document.getShipper() != null && document.getShipper().getPartyName() != null) {
                                                parameters.put("shipperName", reportUtil.removeSpecialCharacters(document.getShipper().getPartyName(), data.getDownloadOption()));
                                            }
                                            parameters.put("hawbNo", document.getDocumentNo() != null ? document.getDocumentNo() : "");
                                        }
                                    }
                                }

                            }
                            if (service.getPol() != null && service.getPol().getPortName() != null) {
                                parameters.put("originPort", service.getPol().getPortName());
                            }
                            if (service.getPod() != null && service.getPod().getPortName() != null) {
                                parameters.put("finalDestination", service.getPod().getPortName());
                            }
                            if (service.getCarrier() != null) {
                                parameters.put("carrierName", service.getCarrier().getCarrierName() != null ? service.getCarrier().getCarrierName() : "");
                            }
                            if (service.getRouteNo() != null) {
                                parameters.put("flightNumber", service.getRouteNo());
                            }

                            parameters.put("volumeWeightPerkg", service.getBookedVolumeWeightUnitKg() != null ? AppUtil.getWeightFormat(service.getBookedVolumeWeightUnitKg(), invoice.getLocation().getCountryMaster().getLocale()) : "");
                            parameters.put("grossWeightPerkg", service.getBookedGrossWeightUnitKg() != null ? AppUtil.getWeightFormat(service.getBookedGrossWeightUnitKg(), invoice.getLocation().getCountryMaster().getLocale()) : "");
                            parameters.put("masterNumber", service.getMawbNo() != null ? service.getMawbNo() : "");
                            if (service.getBookedPieces() != null) {
                                parameters.put("numberOfPacks", service.getBookedPieces());
                            }
                            parameters.put("etaDate", reportUtil.dateToReportDate(service.getEta(), loggedInUser.getSelectedUserLocation().getJavaDateFormat()));
                            parameters.put("etdDate", reportUtil.dateToReportDate(service.getEtd(), loggedInUser.getSelectedUserLocation().getJavaDateFormat()));
                            if (service.getShipment() != null && service.getShipment().getShipmentReqDate() != null) {
                                parameters.put("jobReferenceDate", reportUtil.dateToReportDate(service.getShipment().getShipmentReqDate(),
                                        loggedInUser.getSelectedUserLocation().getJavaDateFormat()));
                            }
                        }
                        parameters.put("shipperReferenceNo", "");

                    } else if (invoice.getMasterUid() != null) {
                        parameters.put("jobReferenceNo", invoice.getMasterUid());
                        Consol consol = consolRepository.findByConsolUid(invoice.getMasterUid());
                        if (consol != null) {

                            if (consol.getShipmentLinkList() != null && !consol.getShipmentLinkList().isEmpty() &&
                                    consol.getShipmentLinkList().size() == 1) {
                                for (ShipmentLink shipmentLink : consol.getShipmentLinkList()) {
                                    ShipmentServiceDetail service = shipmentLink.getService();
                                    if (service != null) {
                                        if (service.getShipper() != null) {
                                            parameters.put("shipperName", reportUtil.removeSpecialCharacters(service.getShipper().getPartyName(), data.getDownloadOption()));
                                        }
                                        if (service.getConsignee() != null) {
                                            parameters.put("consigneeLabel", "Consignee");
                                            parameters.put("consigneeName", reportUtil.removeSpecialCharacters(service.getConsignee().getPartyName(), data.getDownloadOption()));
                                        }
                                    }

                                }
                            } else {
                                parameters.put("shipperName", "");
                                parameters.put("consigneeLabel", "Consignee");
                                parameters.put("consigneeName", "");
                            }

                            if (consol.getConsolDocument() != null) {
                                    /*if (consol.getServiceMaster() != null && consol.getServiceMaster().getImportExport() != null
											&& consol.getServiceMaster().getImportExport().equals(ImportExport.Export)) {
										if (consol.getConsolDocument().getShipper() != null && consol.getConsolDocument().getShipper().getPartyName() != null) {
											parameters.put("shipperName",reportUtil.removeSpecialCharacters(consol.getConsolDocument().getShipper().getPartyName(), data.getDownloadOption()));
										}
										if (consol.getConsolDocument().getConsignee() != null && consol.getConsolDocument().getConsignee().getPartyName() != null) {
											parameters.put("consigneeLabel", "Consignee");
											parameters.put("consigneeName",reportUtil.removeSpecialCharacters(consol.getConsolDocument().getConsignee().getPartyName(), data.getDownloadOption()));
										}
									} else if (consol.getServiceMaster() != null && consol.getServiceMaster().getImportExport() != null
											&& consol.getServiceMaster().getImportExport().equals(ImportExport.Import)) {
										if (consol.getConsolDocument().getShipper() != null && consol.getConsolDocument().getShipper().getPartyName() != null) {
											parameters.put("shipperName",reportUtil.removeSpecialCharacters(consol.getConsolDocument().getShipper().getPartyName(), data.getDownloadOption()));
										}
									}*/
                                if (consol.getPol() != null && consol.getPol().getPortName() != null) {
                                    parameters.put("originPort", consol.getPol().getPortName());
                                }
                                if (consol.getPod() != null && consol.getPod().getPortName() != null) {
                                    parameters.put("finalDestination", consol.getPod().getPortName());
                                }
                                if (consol.getCarrier() != null) {
                                    parameters.put("carrierName", consol.getCarrier().getCarrierName() != null ? consol.getCarrier().getCarrierName() : "");
                                }
                                if (consol.getConsolDocument() != null && consol.getConsolDocument().getRouteNo() != null) {
                                    parameters.put("flightNumber", consol.getConsolDocument().getRouteNo());
                                }
                                parameters.put("volumeWeightPerkg", consol.getConsolDocument().getVolumeWeight() != null ? AppUtil.getWeightFormat(consol.getConsolDocument().getVolumeWeight(), invoice.getLocation().getCountryMaster().getLocale()) : "");
                                parameters.put("grossWeightPerkg", consol.getConsolDocument().getGrossWeight() != null ? AppUtil.getWeightFormat(consol.getConsolDocument().getGrossWeight(), invoice.getLocation().getCountryMaster().getLocale()) : "");
                                parameters.put("masterNumber", consol.getConsolDocument().getMawbNo() != null ? consol.getConsolDocument().getMawbNo() : "");
                                if (consol.getConsolDocument().getNoOfPieces() != null) {
                                    parameters.put("numberOfPacks", consol.getConsolDocument().getNoOfPieces());
                                }
                            }
                            parameters.put("etaDate", reportUtil.dateToReportDate(consol.getEta(),
                                    loggedInUser.getSelectedUserLocation().getJavaDateFormat()));
                            parameters.put("etdDate", reportUtil.dateToReportDate(consol.getEtd(),
                                    loggedInUser.getSelectedUserLocation().getJavaDateFormat()));
                            if (consol.getMasterDate() != null) {
                                parameters.put("jobReferenceDate", reportUtil.dateToReportDate(consol.getMasterDate(),
                                        loggedInUser.getSelectedUserLocation().getJavaDateFormat()));
                            }
                        }
                        parameters.put("shipperReferenceNo", "");
                    }
                    if (invoice.getMasterUid() != null) {
                        parameters.put("jobReferenceNo", invoice.getMasterUid());
                    }
                }

                String quoteType = "";
                Double localAmount = 0.0;
                if (invoice != null && invoice.getShipmentServiceDetail() != null) {
                    ShipmentServiceDetail service = shipmentServiceDetailRepository.findById(invoice.getShipmentServiceDetail().getId());
                    Quotation quotation = quotationRepository.findByQuotationNo(service.getQuotationUid());
                    if (quotation != null && quotation.getQuoteType() != null) {
                        if (quotation.getQuoteType().equals(QuoteType.LUMPSUM)) {
                            quoteType = QuoteType.LUMPSUM.name();
                        } else if (quotation.getQuoteType().equals(QuoteType.PERKG)) {
                            quoteType = QuoteType.PERKG.name();
                        }
                    }
                    String rateMerged = appUtil.getLocationConfig("booking.rates.merged", service.getLocation(), true);
                    if (rateMerged != null) {
                        if (rateMerged.equals("FALSE") || rateMerged == "FALSE") {
                            if (service.getShipmentChargeList() != null && !service.getShipmentChargeList().isEmpty()) {
                                for (ShipmentCharge shipmentCharge : service.getShipmentChargeList()) {

                                    if (shipmentCharge != null) {
                                        if (shipmentCharge.getLocalTotalNetSale() != null) {
                                            localAmount = localAmount + shipmentCharge.getLocalTotalNetSale();
                                        }
                                    }
                                }
                            }
                        } else {
                            if (service.getShipmentChargeList() != null && !service.getShipmentChargeList().isEmpty()) {
                                for (ShipmentCharge shipmentCharge : service.getShipmentChargeList()) {

                                    if (shipmentCharge != null) {
                                        if (shipmentCharge.getLocalTotalRateAmount() != null) {
                                            localAmount = localAmount + shipmentCharge.getLocalTotalRateAmount();
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                if (quoteType == "LUMPSUM" || quoteType == "PERKG") {
                    parameters.put("quoteTypeAmount", AppUtil.getCurrencyFormat(localAmount, invoice.getLocation().getCountryMaster().getLocale(), invoice.getLocation().getCountryMaster().getCurrencyMaster().getDecimalPoint()));
                    parameters.put("quoteTypeName", quoteType);
                } else {
                    parameters.put("chargeList", chargeList);
                }
                LocationMaster lMaster = locationMasterRepository.findByLocationCode(loggedInUser.getSelectedUserLocation().getLocationCode());
                if (lMaster != null && lMaster.getCountryMaster() != null) {

                    DynamicFields tmp = dynamicFieldRepository.findByFieldId("Invoice->Report->ServiceTaxNo");
                    if (tmp != null && tmp.getId() != null && tmp.getCountryMasterList() != null && tmp.getCountryMasterList().size() > 0) {
                        boolean isSvtAvailable = false;
                        for (CountryMaster c : tmp.getCountryMasterList()) {
                            if (Objects.equals(c.getId(), lMaster.getCountryMaster().getId())) {
                                isSvtAvailable = true;
                                break;
                            }
                        }
                        if (isSvtAvailable) {

                            String invoiceServiceTax = appUtil.getLocationConfig("invoice.service.tax", invoice.getLocation(), false);
                            String invoiceFooterTwo = appUtil.getLocationConfig("invoice.footer.two.service.taxno", invoice.getLocation(), false);

                            parameters.put("invoiceServiceTax", invoiceServiceTax);
                            parameters.put("invoiceFooterTwo", invoiceFooterTwo);

                        }
                    } else {
                        parameters.put("invoiceServiceTax", "");
                        parameters.put("invoiceFooterTwo", "");
                    }

                    DynamicFields pan = dynamicFieldRepository.findByFieldId("Invoice->Report->PANno");
                    if (pan != null && pan.getId() != null && pan.getCountryMasterList() != null && pan.getCountryMasterList().size() > 0) {
                        boolean isPanAvialbale = false;
                        for (CountryMaster c : pan.getCountryMasterList()) {
                            if (Objects.equals(c.getId(), lMaster.getCountryMaster().getId())) {
                                isPanAvialbale = true;
                                break;
                            }
                        }
                        if (isPanAvialbale) {

                            String keyValue = appUtil.getLocationConfig("invoice.footer.three.service.pan.cin", invoice.getLocation(), false);

                            parameters.put("invoiceFooterThree", keyValue);

                        }
                    } else {
                        parameters.put("invoiceFooterThree", "");
                    }

                } else {
                    parameters.put("invoiceFooterThree", "");
                    parameters.put("invoiceServiceTax", "");
                    parameters.put("invoiceFooterTwo", "");
                }

                if (invoice != null && invoice.getLocation() != null) {
                    String invoiceTermOne = appUtil.getLocationConfig("invoice.terms.one", invoice.getLocation(), false);

                    parameters.put("invoiceTermOne", invoiceTermOne);

                    String invoiceTermTwo = appUtil.getLocationConfig("invoice.terms.two", invoice.getLocation(), false);

                    parameters.put("invoiceTermTwo", invoiceTermTwo);

                    String invoiceTermThree = appUtil.getLocationConfig("invoice.terms.three", invoice.getLocation(), false);

                    parameters.put("invoiceTermThree", invoiceTermThree);

                    String invoiceFooterOne = appUtil.getLocationConfig("invoice.footer.one", invoice.getLocation(), false);

                    parameters.put("invoiceFooterOne", invoiceFooterOne);
                }

                return JasperFillManager.fillReport(jasperReport, parameters, dataSource);

            }
        } catch (JRException e) {
            e.printStackTrace();
        }
        return null;
    }
}
