package com.efreightsuite.report.service;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletResponse;

import com.efreightsuite.dto.AutoAttachmentDto;
import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.ReportDownloadRequestDto;
import com.efreightsuite.enumeration.AddressType;
import com.efreightsuite.enumeration.ImportExport;
import com.efreightsuite.enumeration.ReportConfig;
import com.efreightsuite.enumeration.ReportDownloadOption;
import com.efreightsuite.enumeration.ReportName;
import com.efreightsuite.enumeration.TransportMode;
import com.efreightsuite.enumeration.YesNo;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.model.DocumentDetail;
import com.efreightsuite.model.LocationMaster;
import com.efreightsuite.model.LogoMaster;
import com.efreightsuite.model.PartyAddressMaster;
import com.efreightsuite.model.ReportConfigurationMaster;
import com.efreightsuite.model.ServiceMaster;
import com.efreightsuite.model.ShipmentServiceDetail;
import com.efreightsuite.model.UserProfile;
import com.efreightsuite.report.BookingConfirmation;
import com.efreightsuite.service.AuthService;
import com.efreightsuite.util.AppUtil;
import lombok.extern.log4j.Log4j2;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

import org.springframework.stereotype.Service;

@Service
@Log4j2
public class BookingConfirmationReportService extends AbstractReportService {
    public ReportName getReportType() {
        return ReportName.BOOKING_CONFIRMATION;
    }

    public byte[] processReport(ReportDownloadRequestDto data, HttpServletResponse response, boolean isMultiple) {
        log.info("I am in BookingConfirmationReportService..");
        BaseDto baseDto = new BaseDto();
        UserProfile logInUSer = AuthService.getCurrentUser();
        JasperPrint jasperPrint = getReport(baseDto, data, AuthService.getCurrentUser());
        byte[] a = null;
        AutoAttachmentDto autoAttachmentDto = reportUtil.autoAttachment(ReportName.BOOKING_CONFIRMATION, YesNo.Yes, data.getResourceId(), "Shipment");
        if (data.getSingle() != null && data.getSingle() == true) {
            if (data.getDownloadOption() == ReportDownloadOption.Preview || data.getDownloadOption() == ReportDownloadOption.Print) {
                reportUtil.japerPrintToResponseOutputStream(data.getDownloadFileType(), jasperPrint, "BOOKING_CONFIRMATION", response, false, null);
            } else if (autoAttachmentDto != null && autoAttachmentDto.getIsAttach() != null || data.getDownloadOption() == ReportDownloadOption.Download) {
                reportUtil.japerPrintToResponseOutputStream(data.getDownloadFileType(), jasperPrint, "BOOKING_CONFIRMATION", response, false, autoAttachmentDto);
            }
        }
        if (data.getSingle() != null && data.getSingle() == true && data.getDownloadOption() == ReportDownloadOption.eMail) {
            a = reportUtil.japerPrintToResponseOutputStream(data.getDownloadFileType(), jasperPrint, "BOOKING_CONFIRMATION", response, false, null);
            shipmentReportMailer.sendShipmentMailToCustomer(data.getResourceId(), logInUSer, ReportName.BOOKING_CONFIRMATION, a);
        }

        if (isMultiple == true && data.getDownloadOption() == ReportDownloadOption.eMail) {
            a = reportUtil.japerPrintToResponseOutputStream(data.getDownloadFileType(), jasperPrint, "BOOKING_CONFIRMATION", response, true, null);
            return a;
        }
        return null;
    }

    private JasperPrint getReport(BaseDto baseDto, ReportDownloadRequestDto data, UserProfile loggedInUser) {
        log.info("I am in BookingConfirmationReportService..");

        BookingConfirmation bookingConfirmation = new BookingConfirmation();
        Map<String, Object> parameters = new HashMap<>();
        try {
            DocumentDetail document = documentDetailRepository.findById(data.getResourceId());
            if (document != null) {
                List<BookingConfirmation> bookingCollection = new ArrayList<>();
                if (document.getShipmentServiceDetail() != null && document.getShipmentServiceDetail().getServiceMaster() != null) {
                    ServiceMaster serviceMaster = document.getShipmentServiceDetail().getServiceMaster();
                    if (serviceMaster != null && serviceMaster.getImportExport().equals(ImportExport.Export)) {
                        if (document.getShipper() != null) {
                            String shippName = document.getShipper().getPartyName() != null ? document.getShipper().getPartyName() : "";
                            bookingConfirmation.setForwarderName(reportUtil.removeSpecialCharacters(shippName, data.getDownloadOption()));
                        }

                        if (document.getShipperAddress() != null) {
                            bookingConfirmation.setForwarderAddressLine1(document.getShipperAddress().getAddressLine1());
                            bookingConfirmation.setForwarderAddressLine2(document.getShipperAddress().getAddressLine2());
                            bookingConfirmation.setForwarderAddressLine3(document.getShipperAddress().getAddressLine3());
                            bookingConfirmation.setForwarderAddressLine4(document.getShipperAddress().getAddressLine4());
                        }
                        if (document.getShipper() != null && document.getShipper().getPartyAddressList() != null && !document.getShipper().getPartyAddressList().isEmpty()) {
                            for (PartyAddressMaster partyAddressMaster : document.getShipper().getPartyAddressList()) {
                                PartyAddressMaster partyAddressMasterObj = partyAddressMaster;
                                if (partyAddressMasterObj.getAddressType() == AddressType.Primary) {
                                    bookingConfirmation.setPhone(partyAddressMasterObj.getPhone());
                                    bookingConfirmation.setFax(partyAddressMasterObj.getFax());
                                    bookingConfirmation.setAttnNumber(partyAddressMasterObj.getContactPerson());
                                }
                            }
                        }

                    } else if (serviceMaster != null && serviceMaster.getImportExport().equals(ImportExport.Import)) {
                        if (document.getShipper() != null) {
                            String shippName = document.getShipper().getPartyName() != null ? document.getShipper().getPartyName() : "";
                            bookingConfirmation.setForwarderName(reportUtil.removeSpecialCharacters(shippName, data.getDownloadOption()));
                        }
                        if (document.getForwarderAddress() != null) {
                            bookingConfirmation.setForwarderAddressLine1(document.getForwarderAddress().getAddressLine1());
                            bookingConfirmation.setForwarderAddressLine2(document.getForwarderAddress().getAddressLine2());
                            bookingConfirmation.setForwarderAddressLine3(document.getForwarderAddress().getAddressLine3());
                            bookingConfirmation.setForwarderAddressLine4(document.getForwarderAddress().getAddressLine4());
                        }
                        if (document.getShipper() != null && document.getShipper().getPartyAddressList() != null && !document.getShipper().getPartyAddressList().isEmpty()) {
                            for (PartyAddressMaster partyAddressMaster : document.getShipper().getPartyAddressList()) {
                                PartyAddressMaster partyAddressMasterObj = partyAddressMaster;
                                if (partyAddressMasterObj.getAddressType() == AddressType.Primary) {
                                    bookingConfirmation.setPhone(partyAddressMasterObj.getPhone());
                                    bookingConfirmation.setFax(partyAddressMasterObj.getFax());
                                    bookingConfirmation.setAttnNumber(partyAddressMasterObj.getContactPerson());
                                }
                            }
                        }

                    }
                }

                if (document.getRouteNo() != null) {
                    bookingConfirmation.setFlightNo(document.getRouteNo());
                }
                if (document.getShipper() != null && document.getShipper().getPartyName() != null) {
                    parameters.put("shipperName", reportUtil.removeSpecialCharacters(document.getShipper().getPartyName(), data.getDownloadOption()));
                }
                if (document.getConsignee() != null && document.getConsignee().getPartyName() != null) {
                    parameters.put("consigneeName", reportUtil.removeSpecialCharacters(document.getConsignee().getPartyName(), data.getDownloadOption()));
                }

                bookingConfirmation.setPortOrigin(document.getOrigin() != null ? document.getOrigin().getPortName() : "");
                bookingConfirmation.setDestination(document.getDestination() != null ? document.getDestination().getPortName() : "");
                String pol = document.getPol() != null ? document.getPol().getPortName() : " ";
                String pod = document.getPod() != null ? document.getPod().getPortName() : " ";
                bookingConfirmation.setPol(pol);
                bookingConfirmation.setPod(pod);

                if (document.getShipmentServiceDetail() != null && document.getShipmentServiceDetail().getServiceMaster() != null
                        && document.getShipmentServiceDetail().getServiceMaster().getTransportMode() != null
                        && document.getShipmentServiceDetail().getServiceMaster().getTransportMode().equals(TransportMode.Air)
                        || document != null && document.getShipmentServiceDetail() != null && document.getShipmentServiceDetail().getServiceMaster() != null
                        && document.getShipmentServiceDetail().getServiceMaster().getTransportMode() != null
                        && document.getShipmentServiceDetail().getServiceMaster().getTransportMode().equals("Air")) {
                    parameters.put("loadingPoint", "Airport of Loading");
                    parameters.put("dischargePoint", "Airport of Discharge");
                } else {
                    parameters.put("loadingPoint", "Port of Loading");
                    parameters.put("dischargePoint", "Port of Discharge");
                }

                if (document.getCarrier() != null) {
                    parameters.put("carrierName", document.getCarrier().getCarrierName());
                }

                if (document.getShipmentServiceDetail() != null) {
                    parameters.put("carrierBookingNo", document.getShipmentServiceDetail().getCarrierBookingNumber());
                }

                ShipmentServiceDetail service = documentDetailRepository.findByIdForShipmentServiceDetail(document.getId());
                if (service != null) {
                    if (service.getShipment() != null && service.getShipment().getCreatedBy() != null) {
                        parameters.put("bookingPerson", service.getShipment().getCreatedBy().getEmployeeName() != null ? service.getShipment().getCreatedBy().getEmployeeName() : "");
                    }
                    if (service.getDirectShipment() != null && service.getDirectShipment().equals(YesNo.Yes)) {
                        bookingConfirmation.setHawbNo(service.getMawbNo()); // Replace with HAWBNO
                    } else {
                        bookingConfirmation.setHawbNo(""); // Replace with HAWBNO
                    }
                    parameters.put("termsShipment", service.getTosMaster() != null ? service.getTosMaster().getTosName() : "");

                    parameters.put("bookingNo", service.getShipmentUid() != null ? service.getShipmentUid() : "");


                    if (service.getPickUpDeliveryPoint() != null && service.getPickUpDeliveryPoint().getDeliveryPoint() != null) {
                        parameters.put("cfsDeliveryPoint", reportUtil.removeSpecialCharacters(service.getPickUpDeliveryPoint().getDeliveryPoint().getCfsName(), data.getDownloadOption()));     //delivery name and corresponding details
                    }
                    if (service.getPickUpDeliveryPoint() != null && service.getPickUpDeliveryPoint().getDeliveryPlace() != null) {
                        String addressLine1 = service.getPickUpDeliveryPoint().getDeliveryPlace().getAddressLine1() != null ? service.getPickUpDeliveryPoint().getDeliveryPlace().getAddressLine1() : "";
                        String addressLine2 = service.getPickUpDeliveryPoint().getDeliveryPlace().getAddressLine2() != null ? service.getPickUpDeliveryPoint().getDeliveryPlace().getAddressLine2() : "";
                        String addressLine3 = service.getPickUpDeliveryPoint().getDeliveryPlace().getAddressLine3() != null ? service.getPickUpDeliveryPoint().getDeliveryPlace().getAddressLine3() : "";
                        String zipCode = service.getPickUpDeliveryPoint().getDeliveryPlace().getZipCode() != null ? service.getPickUpDeliveryPoint().getDeliveryPlace().getZipCode() : "";
                        parameters.put("deliveryToAddressLine1", addressLine1);
                        parameters.put("deliveryToAddressLine2", addressLine2);
                        parameters.put("deliveryToAddressLine3", addressLine3 + "," + zipCode);
                        String state = "";
                        String city = "";
                        if (service.getPickUpDeliveryPoint().getDeliveryPlace().getState() != null) {
                            state = service.getPickUpDeliveryPoint().getDeliveryPlace().getState().getStateName() != null ? service.getPickUpDeliveryPoint().getDeliveryPlace().getState().getStateName() : "";
                        }
                        if (service.getPickUpDeliveryPoint().getDeliveryPlace().getCity() != null) {
                            city = service.getPickUpDeliveryPoint().getDeliveryPlace().getCity().getCityName() != null ? service.getPickUpDeliveryPoint().getDeliveryPlace().getCity().getCityName() : "";
                        }
                        parameters.put("deliveryToAddressLine4", state);
                        parameters.put("deliveryToAddressLine5", city);

                    }

                    String pack = AppUtil.getNumberFormat(document.getNoOfPieces(), service.getCountry().getLocale());
                    String packCode = "";
                    if (document.getPackMaster() != null && document.getPackMaster().getPackName() != null) {
                        packCode = document.getPackMaster().getPackName();
                    }
                    String bothCode = pack + " " + packCode;
                    parameters.put("packages", bothCode);

                    if (document.getVolumeWeight() != null && service.getCountry() != null) {
                        bookingConfirmation.setVolumeWeight(AppUtil.getWeightFormat(document.getVolumeWeight(), service.getCountry().getLocale()) + " KG"); // volumeWeight

                    }
                    if (document.getGrossWeight() != null && service.getCountry() != null) {
                        bookingConfirmation.setWeight(AppUtil.getWeightFormat(document.getGrossWeight(), service.getCountry().getLocale()) + " KG");        // Gross weight
                    }
                    if (document.getChargebleWeight() != null && service.getCountry() != null) {
                        parameters.put("chargeableWeight", AppUtil.getWeightFormat(document.getChargebleWeight(), service.getCountry().getLocale()) + " KG"); // chargeable weight
                    }


                }


                String etaDate = "";
                String etdDate = "";
                String documentCommodity = "";
                if (document.getCommodityDescription() != null) {
                    documentCommodity = document.getCommodityDescription();
                }
                String shipmentCommodity = "";
                if (service != null) {
                    if (service.getCommodity() != null && service.getCommodity().getHsName() != null) {
                        shipmentCommodity = service.getCommodity().getHsName();         //shipment commodityHsName
                    }
                }
                if (documentCommodity.length() > 0) {
                    bookingConfirmation.setCommodity(documentCommodity); //Document Commodity Description
                } else {
                    bookingConfirmation.setCommodity(shipmentCommodity); //Commodity GroupHsName
                }

                ReportConfigurationMaster tmpObj = reportConfigurationMasterRepository.getByKeyAndLocation(ReportConfig.BOOKING_CONFIRMATION_REPORT_REMARKS, service.getLocation().getId());
                if (tmpObj != null && tmpObj.getReportValue() != null) {
                    parameters.put("remarks", tmpObj.getReportValue());
                }

                //UserProfile loggedInUser = AuthService.getCurrentUser();
                String loggedInUserName = "";
                String loggedInLocation = "";


                if (loggedInUser != null) {
                    if (loggedInUser.getEmployee() != null && loggedInUser.getEmployee().getEmployeeName() != null) {
                        loggedInUserName = loggedInUser.getEmployee().getEmployeeName();
                    } else if (loggedInUser.getUserName() != null) {
                        loggedInUserName = loggedInUser.getUserName();
                    } else {
                        loggedInUserName = "";
                    }

                    if (loggedInUser.getSelectedUserLocation() != null) {
                        if (document.getEta() != null) {
                            etaDate = reportUtil.dateToReportDate(document.getEta(), loggedInUser.getSelectedUserLocation().getJavaDateFormat());
                        }
                        if (document.getEtd() != null) {
                            etdDate = reportUtil.dateToReportDate(document.getEtd(), loggedInUser.getSelectedUserLocation().getJavaDateFormat());
                        }
                        parameters.put("etaDate", etaDate + "(" + pod + ")");    //Eta Date and port of loading or origin
                        parameters.put("etdDate", etdDate + "(" + pol + ")");    //Etd Date and port of discharge or destination

                        bookingConfirmation.setRefDate(reportUtil.dateToReportDate(new Date(), loggedInUser.getSelectedUserLocation().getJavaDateFormat()));
                        bookingConfirmation.setCurrentDate(reportUtil.dateToReportDate(new Date(), loggedInUser.getSelectedUserLocation().getJavaDateFormat()));
                        loggedInLocation = loggedInUser.getSelectedUserLocation().getLocationName() != null ? loggedInUser.getSelectedUserLocation().getLocationName() : "";
                        LocationMaster locationMaster = locationMasterRepository.findByLocationCode(loggedInUser.getSelectedUserLocation().getLocationCode());
                        if (locationMaster != null) {
                            bookingConfirmation.setCompanyAddressLine1(locationMaster.getAddressLine1());
                            bookingConfirmation.setCompanyAddressLine2(locationMaster.getAddressLine2());
                            bookingConfirmation.setCompanyAddressLine3(locationMaster.getAddressLine3());
                            if (locationMaster.getBranchName() != null) {
                                String name = locationMaster.getBranchName();
                                bookingConfirmation.setCompanyName(reportUtil.removeSpecialCharacters(name, data.getDownloadOption()));
                                parameters.put("defaultCompanyName", reportUtil.removeSpecialCharacters(locationMaster.getBranchName(), data.getDownloadOption()));
                            }
                        }

                        LogoMaster logoMaster = logoMasterRepository.findByLocationMaster(loggedInUser.getSelectedUserLocation());
                        try {
                            if (logoMaster != null && logoMaster.getLogo() != null) {
                                InputStream in = new ByteArrayInputStream(logoMaster.getLogo());
                                BufferedImage bImageFromConvert = ImageIO.read(in);
                                parameters.put("logo", bImageFromConvert);
                                log.info("Image getting success::");
                            }
                        } catch (Exception ex) {
                            log.error("Image getting failed:::", ex);
                        }
                    } else {
                        loggedInLocation = "";
                    }
                }

                bookingConfirmation.setRefPlace(loggedInUserName + "-" + loggedInLocation);
                parameters.put("salesmanName", loggedInUserName);
                bookingCollection.add(bookingConfirmation);

                JasperReport jasperReport = JasperCompileManager.compileReport(appResourcePath + "/efs-report/booking_confirmation_new.jrxml");
                JRDataSource dataSource = new JRBeanCollectionDataSource(bookingCollection, false);
                return JasperFillManager.fillReport(jasperReport, parameters, dataSource);
            }

        } catch (RestException re) {
            log.error("RestException in BookingConfirmation method while getting the : ", re);

        } catch (Exception exception) {
            log.error("RestException in BookingConfirmation while getting the :: ", exception);
            baseDto.setResponseCode(ErrorCode.FAILED);
        }

        log.info("Booking Confirmation Resource  [" + data.getResourceId() + "] function ended");
        return null;
    }
}
