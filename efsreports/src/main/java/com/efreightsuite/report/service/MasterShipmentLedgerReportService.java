package com.efreightsuite.report.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.JobLedgerResChargeDto;
import com.efreightsuite.dto.JobLedgerResDocumentDto;
import com.efreightsuite.dto.JobLedgerResDto;
import com.efreightsuite.dto.ReportDownloadRequestDto;
import com.efreightsuite.enumeration.PPCC;
import com.efreightsuite.enumeration.ReportDownloadOption;
import com.efreightsuite.enumeration.ReportName;
import com.efreightsuite.enumeration.TransportMode;
import com.efreightsuite.model.Consol;
import com.efreightsuite.model.DocumentDetail;
import com.efreightsuite.model.LocationMaster;
import com.efreightsuite.model.Provisional;
import com.efreightsuite.model.ShipmentServiceDetail;
import com.efreightsuite.model.UserProfile;
import com.efreightsuite.report.JobCostSheet;
import com.efreightsuite.report.JobLedgerReportDto;
import com.efreightsuite.service.AuthService;
import com.efreightsuite.util.AppUtil;
import lombok.extern.log4j.Log4j2;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

import org.springframework.stereotype.Service;

@Service
@Log4j2
public class MasterShipmentLedgerReportService extends AbstractReportService {
    @Override
    public ReportName getReportType() {
        return ReportName.MASTER_COST_SHEET;
    }

    public byte[] processReport(ReportDownloadRequestDto data, HttpServletResponse response, boolean isMultiple) {
        log.info("I am in maaster_shipmentrReportService.....");
        BaseDto baseDto = new BaseDto();
        JasperPrint jasperPrint = getReport(baseDto, data, AuthService.getCurrentUser());
        if (data.getDownloadOption() == ReportDownloadOption.Preview || data.getDownloadOption() == ReportDownloadOption.Download || data.getDownloadOption() == ReportDownloadOption.Print) {
            reportUtil.japerPrintToResponseOutputStream(data.getDownloadFileType(), jasperPrint, "MASTER_COST_SHEET", response, false, null);
        }
        return null;
    }

    private JasperPrint getReport(BaseDto baseDto, ReportDownloadRequestDto data, UserProfile loggedInUser) {
        log.info("MasterShipmentReportService  Resource[" + data.getJobLedgerResDto() + "] function called");
        try {
            Map<String, Object> parameters = new HashMap<>();

            String loggedInUserName = "";
            String companyName = "";
            String loggedInLocation = "";
            String pol = "";
            String pod = "";
            if (loggedInUser != null && loggedInUser.getSelectedUserLocation() != null) {
                if (loggedInUser.getEmployee() != null && loggedInUser.getEmployee().getEmployeeName() != null) {
                    loggedInUserName = loggedInUser.getEmployee().getEmployeeName();
                } else if (loggedInUser.getUserName() != null) {
                    loggedInUserName = loggedInUser.getUserName();
                } else {
                    loggedInUserName = "";
                }

                parameters.put("refPlace", loggedInUserName + "-" + loggedInUser.getSelectedUserLocation());
                String refDate = reportUtil.dateToReportDate(new Date(), loggedInUser.getSelectedUserLocation().getJavaDateFormat());
                parameters.put("refDate", refDate);

                if (loggedInUser.getSelectedUserLocation() != null) {
                    loggedInLocation = loggedInUser.getSelectedUserLocation().getLocationName() != null
                            ? loggedInUser.getSelectedUserLocation().getLocationName() : "";
                    LocationMaster locationMaster = locationMasterRepository.findByLocationCode(loggedInUser.getSelectedUserLocation().getLocationCode());
                    parameters.put("locationMasterAddressList", reportUtil.locationMasterAddressList(locationMaster));
                    if (locationMaster != null) {
                        companyName = locationMaster.getBranchName() != null ? locationMaster.getBranchName() : "";
                        parameters.put("companyName", reportUtil.removeSpecialCharacters(companyName, data.getDownloadOption()));
                    }
                }

            }
            parameters.put("refPlace", loggedInUserName + "-" + loggedInLocation);
            Consol consol = null;
            JobLedgerResDto jobLedgerResDto = data.getJobLedgerResDto();

            List<JobCostSheet> jobCostSheetList = new ArrayList<>();
            List<JobCostSheet> provisionchargeList = new ArrayList<>();
            List<JobLedgerReportDto> provisionList = new ArrayList<>();

            JobCostSheet jobCostSheet = new JobCostSheet();

            Double provisionalRevenue = 0.0, provisionalCost = 0.0, provisionalGp = 0.0;
            Double totalGrossWeight = 0.0, totalVolumeWeight = 0.0;

            if (jobLedgerResDto != null) {
                List<JobLedgerResDocumentDto> jobLedgerResDocumentDto = jobLedgerResDto.getDocumentWiseList();
                Map<String, JobLedgerReportDto> documentConsol = new HashMap<>();
                Map<String, JobLedgerReportDto> documentShipment = new HashMap<>();

                if (jobLedgerResDocumentDto != null && jobLedgerResDocumentDto.size() > 0) {
                    Double grossWeight = 0.0;
                    Double volumeWeight = 0.0;
                    Double shiGrossWeight = 0.0;
                    Double shiVolumeWeight = 0.0;
                    for (JobLedgerResDocumentDto jlr : jobLedgerResDto.getDocumentWiseList()) {

                        if (jlr != null) {
                            consol = consolRepository.findByConsolUid(jlr.getMasterUid());

                            if (consol != null) {
                                String polEta = "";
                                String podEtd = "";
                                if (consol.getEta() != null) {
                                    polEta = reportUtil.dateToReportDate(consol.getEta(), loggedInUser.getSelectedUserLocation().getJavaDateFormat());
                                }
                                if (consol.getEtd() != null) {
                                    podEtd = reportUtil.dateToReportDate(consol.getEtd(), loggedInUser.getSelectedUserLocation().getJavaDateFormat());
                                }

                                if (pol.length() > 0 && polEta.length() > 0) {
                                    parameters.put("polAndDate", pol + " / " + polEta);
                                } else {
                                    parameters.put("polAndDate", pol + "  " + polEta);
                                }
                                if (pod.length() > 0 && podEtd.length() > 0) {
                                    parameters.put("podAndDate", pod + " / " + podEtd);
                                } else {
                                    parameters.put("podAndDate", pod + "  " + podEtd);
                                }

                                if (consol.getServiceMaster() != null) {
                                    if (consol.getServiceMaster().getTransportMode() != null && consol.getServiceMaster().getTransportMode().equals(TransportMode.Ocean)) {
                                        parameters.put("containerLabel", "Container No");
                                        parameters.put("containerTypeLabel", "Type");
                                    }
                                }
                                if (consol.getPol() != null) {
                                    pol = consol.getPol().getPortName() != null ? consol.getPol().getPortName() : "";
                                }
                                if (consol.getPod() != null) {
                                    pod = consol.getPod().getPortName() != null ? consol.getPod().getPortName() : "";
                                }
                                if (consol.getCarrier() != null) {
                                    parameters.put("carrier", consol.getCarrier().getCarrierName() != null ? consol.getCarrier().getCarrierName() : "");
                                }
                                if (consol.getPpcc().equals(PPCC.Prepaid) || consol.getPpcc().equals("Prepaid")) {
                                    parameters.put("masterPpCc", "PREPAID");
                                } else {
                                    parameters.put("masterPpCc", "COLLECT");
                                }
                                if (consol.getAgent() != null && consol.getAgent().getPartyName() != null) {
                                    parameters.put("agentName", reportUtil.removeSpecialCharacters(consol.getAgent().getPartyName(), data.getDownloadOption()));
                                }
                                if (consol.getVesselCode() != null) {
                                    parameters.put("vesselAndVoy", consol.getVesselCode());
                                }
                                if (consol.getConsolUid() != null) {
                                    parameters.put("jobNumber", consol.getConsolUid());
                                }
                                if (consol.getConsolDocument() != null) {
                                    parameters.put("volumeWeight", consol.getConsolDocument().getVolumeWeight() != null ? AppUtil.getWeightFormat(consol.getConsolDocument().getVolumeWeight(), consol.getCountry().getLocale()) + " KG" : "");
                                    parameters.put("grossWeight", consol.getConsolDocument().getGrossWeight() != null ? AppUtil.getWeightFormat(consol.getConsolDocument().getGrossWeight(), consol.getCountry().getLocale()) + " KG" : "");
                                    parameters.put("chargeableWeight", consol.getConsolDocument().getChargebleWeight() != null ? AppUtil.getWeightFormat(consol.getConsolDocument().getChargebleWeight(), consol.getCountry().getLocale()) + " KG" : "");
                                    parameters.put("oblAndMawb", consol.getConsolDocument().getMawbNo() != null ? consol.getConsolDocument().getMawbNo() : "");
                                }
                            }

                        }

                        if (jlr.getMasterUid() != null && jlr.getShipmentUid() == null) {
                            //consol level
                            Consol consolDetail = consolRepository.findByConsolUid(jlr.getMasterUid());

                            if (documentConsol.get(jlr.getMasterUid()) == null) {
                                JobLedgerReportDto newjrdto = new JobLedgerReportDto();
                                if (consolDetail != null) {

                                    Provisional provisional = provisionalRepository.findByMasterUid(consolDetail.getConsolUid());
                                    if (provisional != null) {
                                        if (provisional.getTotalRevenue() != null) {
                                            provisionalRevenue = provisionalRevenue + provisional.getTotalRevenue();
                                            newjrdto.setRevenue(AppUtil.getCurrencyFormat(provisional.getTotalRevenue(), consolDetail.getLocalCurrency().getCountryMaster().getLocale(), consolDetail.getLocalCurrency().getDecimalPoint()));
                                        }
                                        if (provisional.getTotalCost() != null) {
                                            provisionalCost = provisionalCost + provisional.getTotalCost();
                                            newjrdto.setCost(AppUtil.getCurrencyFormat(provisional.getTotalCost(), consolDetail.getLocalCurrency().getCountryMaster().getLocale(), consolDetail.getLocalCurrency().getDecimalPoint()));
                                        }
                                        if (provisional.getNetAmount() != null) {
                                            provisionalGp = provisionalGp + provisional.getNetAmount();
                                            newjrdto.setGp(AppUtil.getCurrencyFormat(provisional.getNetAmount(), consolDetail.getLocalCurrency().getCountryMaster().getLocale(), consolDetail.getLocalCurrency().getDecimalPoint()));
                                        }
                                    }

                                    if (consolDetail.getOrigin() != null) {
                                        newjrdto.setOriginName(consolDetail.getOrigin().getPortName());
                                    }
                                    if (consolDetail.getDestination() != null) {
                                        newjrdto.setFdc(consolDetail.getDestination().getPortName());
                                    }
                                    if (consolDetail.getPpcc() != null) {
                                        if (consolDetail.getPpcc().equals(PPCC.Prepaid)) {
                                            newjrdto.setPpcc("PREPAID");
                                        } else {
                                            newjrdto.setPpcc("COLLECT");
                                        }
                                    }
                                    if (consolDetail.getConsolDocument() != null) {
                                        newjrdto.setHawb(consolDetail.getConsolDocument().getDocumentNo() != null ? consolDetail.getConsolDocument().getDocumentNo() : "");
                                        if (consolDetail.getConsolDocument().getGrossWeight() != null) {
                                            grossWeight = grossWeight + consolDetail.getConsolDocument().getGrossWeight();
                                            newjrdto.setGrossWeight(AppUtil.getWeightFormat(consolDetail.getConsolDocument().getGrossWeight(), consolDetail.getCountry().getLocale()));
                                        }
                                        if (consolDetail.getConsolDocument().getVolumeWeight() != null) {
                                            volumeWeight = volumeWeight + consolDetail.getConsolDocument().getVolumeWeight();
                                            newjrdto.setVolumeWeight(AppUtil.getWeightFormat(consolDetail.getConsolDocument().getVolumeWeight(), consolDetail.getCountry().getLocale()));
                                        }
                                    }
                                    if (consolDetail.getRoutedSalesman() != null) {
                                        newjrdto.setSalesman(consolDetail.getRoutedSalesman().getEmployeeName());
                                    }

                                }
                                if (jlr.getActualCost() != null) {
                                    newjrdto.setActualCost(AppUtil.getCurrencyFormat(jlr.getActualCost(), consolDetail.getLocalCurrency().getCountryMaster().getLocale(), consolDetail.getLocalCurrency().getDecimalPoint()));
                                }
                                if (jlr.getActualRevenue() != null) {
                                    newjrdto.setActualRevenue(AppUtil.getCurrencyFormat(jlr.getActualRevenue(), consolDetail.getLocalCurrency().getCountryMaster().getLocale(), consolDetail.getLocalCurrency().getDecimalPoint()));
                                }
                                if (jlr.getActualNeutral() != null) {
                                    newjrdto.setActualNeutral(AppUtil.getCurrencyFormat(jlr.getActualNeutral(), consolDetail.getLocalCurrency().getCountryMaster().getLocale(), consolDetail.getLocalCurrency().getDecimalPoint()));
                                }
                                documentConsol.put(jlr.getMasterUid(), newjrdto);
                            } else {
                                JobLedgerReportDto exjrdto = documentConsol.get(jlr.getMasterUid());

                                exjrdto.setActualRevenue(AppUtil.getCurrencyFormat(Double.valueOf(exjrdto.getActualRevenue() == null ? "0.0" : exjrdto.getActualRevenue()) + (jlr.getActualRevenue() == null ? 0.0 : jlr.getActualRevenue()), consolDetail.getLocalCurrency().getCountryMaster().getLocale(), consolDetail.getLocalCurrency().getDecimalPoint()));

                                exjrdto.setActualCost(AppUtil.getCurrencyFormat(Double.valueOf(exjrdto.getActualCost() == null ? "0.0" : exjrdto.getActualCost()) + (jlr.getActualCost() == null ? 0.0 : jlr.getActualCost()), consolDetail.getLocalCurrency().getCountryMaster().getLocale(), consolDetail.getLocalCurrency().getDecimalPoint()));

                                exjrdto.setActualNeutral(AppUtil.getCurrencyFormat(Double.valueOf(exjrdto.getActualNeutral() == null ? "0.0" : exjrdto.getActualNeutral()) + (jlr.getActualNeutral() == null ? 0.0 : jlr.getActualNeutral()), consolDetail.getLocalCurrency().getCountryMaster().getLocale(), consolDetail.getLocalCurrency().getDecimalPoint()));

                                documentConsol.put(jlr.getMasterUid(), exjrdto);
                            }

                        } else if (jlr.getMasterUid() != null && jlr.getShipmentUid() != null) {
                            //shipment service level
                            ShipmentServiceDetail serviceDetail = shipmentServiceDetailRepository.findByServiceUid(jlr.getServiceUid());

                            if (documentShipment.get(jlr.getShipmentUid()) == null) {
                                JobLedgerReportDto newShipjrdto = new JobLedgerReportDto();
                                if (serviceDetail != null) {
                                    Provisional provisional = provisionalRepository.findByServiceUid(serviceDetail.getServiceUid());
                                    if (provisional != null) {
                                        if (provisional.getTotalRevenue() != null) {
                                            provisionalRevenue = provisionalRevenue + provisional.getTotalRevenue();
                                            newShipjrdto.setRevenue(AppUtil.getCurrencyFormat(provisional.getTotalRevenue(), serviceDetail.getLocalCurrency().getCountryMaster().getLocale(), serviceDetail.getLocalCurrency().getDecimalPoint()));
                                        }
                                        if (provisional.getTotalCost() != null) {
                                            provisionalCost = provisionalCost + provisional.getTotalCost();
                                            newShipjrdto.setCost(AppUtil.getCurrencyFormat(provisional.getTotalCost(), serviceDetail.getLocalCurrency().getCountryMaster().getLocale(), serviceDetail.getLocalCurrency().getDecimalPoint()));
                                        }
                                        if (provisional.getNetAmount() != null) {
                                            provisionalGp = provisionalGp + provisional.getNetAmount();
                                            newShipjrdto.setGp(AppUtil.getCurrencyFormat(provisional.getNetAmount(), serviceDetail.getLocalCurrency().getCountryMaster().getLocale(), serviceDetail.getLocalCurrency().getDecimalPoint()));
                                        }

                                    } else {
                                        if (serviceDetail.getRate() != null) {
                                            provisionalRevenue = provisionalRevenue + serviceDetail.getRate();
                                            newShipjrdto.setRevenue(AppUtil.getCurrencyFormat(serviceDetail.getRate(), serviceDetail.getLocalCurrency().getCountryMaster().getLocale(), serviceDetail.getLocalCurrency().getDecimalPoint()));
                                        }
                                        if (serviceDetail.getCost() != null) {
                                            provisionalCost = provisionalCost + serviceDetail.getCost();
                                            newShipjrdto.setCost(AppUtil.getCurrencyFormat(serviceDetail.getCost(), serviceDetail.getLocalCurrency().getCountryMaster().getLocale(), serviceDetail.getLocalCurrency().getDecimalPoint()));
                                        }

                                        provisionalGp = provisionalRevenue - provisionalCost;
                                        newShipjrdto.setGp(AppUtil.getCurrencyFormat(provisionalGp, serviceDetail.getLocalCurrency().getCountryMaster().getLocale(), serviceDetail.getLocalCurrency().getDecimalPoint()));
                                    }

                                    if (serviceDetail.getOrigin() != null) {
                                        newShipjrdto.setOriginName(serviceDetail.getOrigin().getPortName());
                                    }
                                    if (serviceDetail.getDestination() != null) {
                                        newShipjrdto.setFdc(serviceDetail.getDestination().getPortName());
                                    }
                                    if (serviceDetail.getParty() != null) {
                                        newShipjrdto.setCustomer(serviceDetail.getParty().getPartyName());
                                    }
                                    if (serviceDetail.getSalesman() != null) {
                                        newShipjrdto.setSalesman(serviceDetail.getSalesman().getEmployeeName());
                                    }
                                    if (serviceDetail.getPpcc() != null) {
                                        if (serviceDetail.getPpcc().equals(PPCC.Prepaid)) {
                                            newShipjrdto.setPpcc("PREPAID");
                                        } else {
                                            newShipjrdto.setPpcc("COLLECT");
                                        }
                                    }
                                    if (serviceDetail.getDocumentList() != null) {
                                        for (DocumentDetail documentDetail : serviceDetail.getDocumentList()) {
                                            newShipjrdto.setHawb(documentDetail.getHawbNo() != null ? documentDetail.getHawbNo() : "");
                                        }
                                    }

                                    if (serviceDetail.getBookedGrossWeightUnitKg() != null) {
                                        shiGrossWeight = shiGrossWeight + serviceDetail.getBookedGrossWeightUnitKg();
                                        newShipjrdto.setGrossWeight(AppUtil.getWeightFormat(serviceDetail.getBookedGrossWeightUnitKg(), serviceDetail.getCountry().getLocale()));
                                    }
                                    if (serviceDetail.getBookedVolumeWeightUnitKg() != null) {
                                        shiVolumeWeight = shiVolumeWeight + serviceDetail.getBookedVolumeWeightUnitKg();
                                        newShipjrdto.setVolumeWeight(AppUtil.getWeightFormat(serviceDetail.getBookedVolumeWeightUnitKg(), serviceDetail.getCountry().getLocale()));
                                    }
                                }
                                if (jlr.getActualCost() != null) {
                                    newShipjrdto.setActualCost(AppUtil.getCurrencyFormat(jlr.getActualCost(), serviceDetail.getCountry().getLocale(), serviceDetail.getLocalCurrency().getDecimalPoint()));
                                }
                                if (jlr.getActualRevenue() != null) {
                                    newShipjrdto.setActualRevenue(AppUtil.getCurrencyFormat(jlr.getActualRevenue(), serviceDetail.getCountry().getLocale(), serviceDetail.getLocalCurrency().getDecimalPoint()));
                                }
                                if (jlr.getActualNeutral() != null) {
                                    newShipjrdto.setActualNeutral(AppUtil.getCurrencyFormat(jlr.getActualNeutral(), serviceDetail.getCountry().getLocale(), serviceDetail.getLocalCurrency().getDecimalPoint()));
                                }

                                documentShipment.put(jlr.getShipmentUid(), newShipjrdto);
                            } else {
                                JobLedgerReportDto exShipjrdto = documentShipment.get(jlr.getShipmentUid());

                                exShipjrdto.setActualRevenue(AppUtil.getCurrencyFormat(Double.valueOf(exShipjrdto.getActualRevenue() == null ? "0.0" : exShipjrdto.getActualRevenue()) + (jlr.getActualRevenue() == null ? 0.0 : jlr.getActualRevenue()), serviceDetail.getLocalCurrency().getCountryMaster().getLocale(), serviceDetail.getLocalCurrency().getDecimalPoint()));

                                exShipjrdto.setActualCost(AppUtil.getCurrencyFormat(Double.valueOf(exShipjrdto.getActualCost() == null ? "0.0" : exShipjrdto.getActualCost()) + (jlr.getActualCost() == null ? 0.0 : jlr.getActualCost()), serviceDetail.getLocalCurrency().getCountryMaster().getLocale(), serviceDetail.getLocalCurrency().getDecimalPoint()));

                                exShipjrdto.setActualNeutral(AppUtil.getCurrencyFormat(Double.valueOf(exShipjrdto.getActualNeutral() == null ? "0.0" : exShipjrdto.getActualNeutral()) + (jlr.getActualNeutral() == null ? 0.0 : jlr.getActualNeutral()), serviceDetail.getLocalCurrency().getCountryMaster().getLocale(), serviceDetail.getLocalCurrency().getDecimalPoint()));

                                documentShipment.put(jlr.getShipmentUid(), exShipjrdto);
                            }
                        }

                        totalGrossWeight = grossWeight + shiGrossWeight;
                        totalVolumeWeight = volumeWeight + shiVolumeWeight;

                        parameters.put("sActualCost", AppUtil.getCurrencyFormat(jobLedgerResDto.getDocTotalAcutalCost(), loggedInUser.getSelectedUserLocation().getCountryMaster().getLocale(), loggedInUser.getSelectedUserLocation().getCurrencyMaster().getDecimalPoint()));
                        parameters.put("sActualRevenue", AppUtil.getCurrencyFormat(jobLedgerResDto.getDocTotalAcutalRevenue(), loggedInUser.getSelectedUserLocation().getCountryMaster().getLocale(), loggedInUser.getSelectedUserLocation().getCurrencyMaster().getDecimalPoint()));
                        parameters.put("sActualGp", AppUtil.getCurrencyFormat(jobLedgerResDto.getDocNetProfitAmount(), loggedInUser.getSelectedUserLocation().getCountryMaster().getLocale(), loggedInUser.getSelectedUserLocation().getCurrencyMaster().getDecimalPoint()));

                        parameters.put("stotalRevenue", AppUtil.getCurrencyFormat(provisionalRevenue, loggedInUser.getSelectedUserLocation().getCountryMaster().getLocale(), loggedInUser.getSelectedUserLocation().getCurrencyMaster().getDecimalPoint()));
                        parameters.put("stotalCost", AppUtil.getCurrencyFormat(provisionalCost, loggedInUser.getSelectedUserLocation().getCountryMaster().getLocale(), loggedInUser.getSelectedUserLocation().getCurrencyMaster().getDecimalPoint()));
                        parameters.put("stotalGp", AppUtil.getCurrencyFormat(provisionalGp, loggedInUser.getSelectedUserLocation().getCountryMaster().getLocale(), loggedInUser.getSelectedUserLocation().getCurrencyMaster().getDecimalPoint()));

                        parameters.put("totalGrossWeight", AppUtil.getWeightFormat(totalGrossWeight, loggedInUser.getSelectedUserLocation().getCountryMaster().getLocale()));
                        parameters.put("totalVolumeWeight", AppUtil.getWeightFormat(totalVolumeWeight, loggedInUser.getSelectedUserLocation().getCountryMaster().getLocale()));
                    }
                    for (String dto : documentConsol.keySet()) {
                        JobLedgerReportDto value = documentConsol.get(dto);
                        provisionList.add(value);
                    }
                    for (String dto : documentShipment.keySet()) {
                        JobLedgerReportDto value = documentShipment.get(dto);
                        provisionList.add(value);
                    }

                    parameters.put("provisionList", provisionList);

                    // Charge Detail start here***********

                    Double revenue = 0.0, cost = 0.0, gp = 0.0, neutral = 0.0, actualRevenue = 0.0, actualCost = 0.0, actualGp = 0.0, actualNeutral = 0.0, totalRevenue = 0.0;
                    Double totalCost = 0.0, totalActualRevenue = 0.0, totalActualCost = 0.0;

                    List<JobLedgerResChargeDto> jobLedgerCharge = jobLedgerResDto.getChargeWiseList();
                    if (jobLedgerCharge != null) {
                        for (JobLedgerResChargeDto jlrc : jobLedgerCharge) {
                            JobCostSheet jcs = new JobCostSheet();
                            if (jlrc != null) {
                                if (jlrc.getChargeCode() != null) {
                                    jcs.setChargeCode(jlrc.getChargeCode());
                                }
                                if (jlrc.getChargeName() != null) {
                                    jcs.setChargeName(jlrc.getChargeName());
                                }
                                if (jlrc.getProvisionalRevenue() != null) {
                                    revenue = jlrc.getProvisionalRevenue();
                                    totalRevenue = totalRevenue + revenue;
                                    jcs.setRevenue(AppUtil.getCurrencyFormat(jlrc.getProvisionalRevenue(), loggedInUser.getSelectedUserLocation().getCountryMaster().getLocale(), loggedInUser.getSelectedUserLocation().getCurrencyMaster().getDecimalPoint()));
                                }
                                if (jlrc.getProvisionalCost() != null) {
                                    cost = jlrc.getProvisionalCost();
                                    totalCost = totalCost + cost;

                                    jcs.setCost(AppUtil.getCurrencyFormat(jlrc.getProvisionalCost(), loggedInUser.getSelectedUserLocation().getCountryMaster().getLocale(), loggedInUser.getSelectedUserLocation().getCurrencyMaster().getDecimalPoint()));
                                }
                                if (jlrc.getProvisionalNeutral() != null) {
                                    neutral = neutral + jlrc.getProvisionalNeutral();
                                    jcs.setNeutral(AppUtil.getCurrencyFormat(jlrc.getProvisionalNeutral(), loggedInUser.getSelectedUserLocation().getCountryMaster().getLocale(), loggedInUser.getSelectedUserLocation().getCurrencyMaster().getDecimalPoint()));
                                }
                                if (jlrc.getActualRevenue() != null) {
                                    actualRevenue = jlrc.getActualRevenue();
                                    totalActualRevenue = totalActualRevenue + actualRevenue;
                                    jcs.setActualRevenue(AppUtil.getCurrencyFormat(jlrc.getActualRevenue(), loggedInUser.getSelectedUserLocation().getCountryMaster().getLocale(), loggedInUser.getSelectedUserLocation().getCurrencyMaster().getDecimalPoint()));
                                }
                                if (jlrc.getActualCost() != null) {
                                    actualCost = jlrc.getActualCost();
                                    totalActualCost = totalActualCost + actualCost;
                                    jcs.setActualCost(AppUtil.getCurrencyFormat(jlrc.getActualCost(), loggedInUser.getSelectedUserLocation().getCountryMaster().getLocale(), loggedInUser.getSelectedUserLocation().getCurrencyMaster().getDecimalPoint()));
                                }
                                if (jlrc.getActualNeutral() != null) {
                                    actualNeutral = actualNeutral + jlrc.getActualNeutral();
                                    jcs.setActualNeutral(AppUtil.getCurrencyFormat(jlrc.getActualNeutral(), loggedInUser.getSelectedUserLocation().getCountryMaster().getLocale(), loggedInUser.getSelectedUserLocation().getCurrencyMaster().getDecimalPoint()));
                                }
                                gp = revenue - cost;
                                actualGp = actualRevenue - actualCost;
                                jcs.setGp(AppUtil.getCurrencyFormat(gp, loggedInUser.getSelectedUserLocation().getCountryMaster().getLocale(), loggedInUser.getSelectedUserLocation().getCurrencyMaster().getDecimalPoint()));
                                jcs.setActualGp(AppUtil.getCurrencyFormat(actualGp, loggedInUser.getSelectedUserLocation().getCountryMaster().getLocale(), loggedInUser.getSelectedUserLocation().getCurrencyMaster().getDecimalPoint()));

                                jcs.setGpPercentage("");

                            }
                            provisionchargeList.add(jcs);
                            parameters.put("provisionChargeList", provisionchargeList);
                        }
                        //Charge details end Here............

                        parameters.put("totalGp", AppUtil.getCurrencyFormat(jobLedgerResDto.getChargeProfitProvisional(), loggedInUser.getSelectedUserLocation().getCountryMaster().getLocale(), loggedInUser.getSelectedUserLocation().getCurrencyMaster().getDecimalPoint()));
                        parameters.put("totalCost", AppUtil.getCurrencyFormat(jobLedgerResDto.getChargeTotalProvisionalCost(), loggedInUser.getSelectedUserLocation().getCountryMaster().getLocale(), loggedInUser.getSelectedUserLocation().getCurrencyMaster().getDecimalPoint()));
                        parameters.put("totalRevenue", AppUtil.getCurrencyFormat(jobLedgerResDto.getChargeTotalProvisionalRevenue(), loggedInUser.getSelectedUserLocation().getCountryMaster().getLocale(), loggedInUser.getSelectedUserLocation().getCurrencyMaster().getDecimalPoint()));
                        parameters.put("totalNeutral", AppUtil.getCurrencyFormat(jobLedgerResDto.getChargeTotalProvisionalNeutral(), loggedInUser.getSelectedUserLocation().getCountryMaster().getLocale(), loggedInUser.getSelectedUserLocation().getCurrencyMaster().getDecimalPoint()));

                        parameters.put("actualGp", AppUtil.getCurrencyFormat(jobLedgerResDto.getChargeProfitActual(), loggedInUser.getSelectedUserLocation().getCountryMaster().getLocale(), loggedInUser.getSelectedUserLocation().getCurrencyMaster().getDecimalPoint()));
                        parameters.put("actualCost", AppUtil.getCurrencyFormat(jobLedgerResDto.getChargeTotalActualCost(), loggedInUser.getSelectedUserLocation().getCountryMaster().getLocale(), loggedInUser.getSelectedUserLocation().getCurrencyMaster().getDecimalPoint()));
                        parameters.put("actualRevenue", AppUtil.getCurrencyFormat(jobLedgerResDto.getChargeTotalActualRevenue(), loggedInUser.getSelectedUserLocation().getCountryMaster().getLocale(), loggedInUser.getSelectedUserLocation().getCurrencyMaster().getDecimalPoint()));
                        parameters.put("actualNeutral", AppUtil.getCurrencyFormat(jobLedgerResDto.getChargeTotalActualNeutral(), loggedInUser.getSelectedUserLocation().getCountryMaster().getLocale(), loggedInUser.getSelectedUserLocation().getCurrencyMaster().getDecimalPoint()));

                    }

                }

            }

            jobCostSheet.setJobLedgerResDto(jobLedgerResDto);
            jobCostSheetList.add(jobCostSheet);

            String defaultMasterData = appUtil.getLocationConfig("master.cost.sheet", loggedInUser.getSelectedUserLocation(), false);
            parameters.put("note", defaultMasterData);

            JasperReport jasperReport = JasperCompileManager.compileReport(appResourcePath + "/efs-report/master_shipment.jrxml");
            JRDataSource dataSource = new JRBeanCollectionDataSource(jobCostSheetList, false);
            return JasperFillManager.fillReport(jasperReport, parameters, dataSource);

        } catch (JRException e) {
            e.printStackTrace();
        }
        return null;
    }
}
