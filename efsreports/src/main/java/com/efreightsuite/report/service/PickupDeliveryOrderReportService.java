package com.efreightsuite.report.service;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringJoiner;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletResponse;

import com.efreightsuite.dto.AutoAttachmentDto;
import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.ReportDownloadRequestDto;
import com.efreightsuite.enumeration.AddressType;
import com.efreightsuite.enumeration.DimensionUnit;
import com.efreightsuite.enumeration.ReportConfig;
import com.efreightsuite.enumeration.ReportDownloadOption;
import com.efreightsuite.enumeration.ReportName;
import com.efreightsuite.enumeration.YesNo;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.model.Consol;
import com.efreightsuite.model.ConsolDocumentDimension;
import com.efreightsuite.model.LocationMaster;
import com.efreightsuite.model.LogoMaster;
import com.efreightsuite.model.PartyAddressMaster;
import com.efreightsuite.model.PickUpDeliveryPoint;
import com.efreightsuite.model.ReportConfigurationMaster;
import com.efreightsuite.model.UserProfile;
import com.efreightsuite.report.ShippingInstruction;
import com.efreightsuite.service.AuthService;
import com.efreightsuite.util.AppUtil;
import lombok.extern.log4j.Log4j2;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

import org.springframework.stereotype.Service;

@Service
@Log4j2
public class PickupDeliveryOrderReportService extends AbstractReportService {
    public ReportName getReportType() {
        return ReportName.PICKUP_DELIVERY_ORDER;
    }

    public byte[] processReport(ReportDownloadRequestDto data, HttpServletResponse response, boolean isMultiple) {
        log.info("I am in PickupDeliveryOrderReportService..");
        BaseDto baseDto = new BaseDto();
        UserProfile logInUSer = AuthService.getCurrentUser();
        JasperPrint jasperPrint = getReport(baseDto, data, AuthService.getCurrentUser());

        byte[] a = null;
        AutoAttachmentDto autoAttachmentDto = reportUtil.autoAttachment(ReportName.PICKUP_DELIVERY_ORDER, YesNo.Yes, data.getResourceId(), "Shipment");
        if (data.getSingle() != null && data.getSingle() == true) {
            if (data.getDownloadOption() == ReportDownloadOption.Preview || data.getDownloadOption() == ReportDownloadOption.Print) {
                reportUtil.japerPrintToResponseOutputStream(data.getDownloadFileType(), jasperPrint, "PICKUP_DELIVERY_ORDER", response, false, null);
            } else if (autoAttachmentDto != null && autoAttachmentDto.getIsAttach() != null || data.getDownloadOption() == ReportDownloadOption.Download) {
                reportUtil.japerPrintToResponseOutputStream(data.getDownloadFileType(), jasperPrint, "PICKUP_DELIVERY_ORDER", response, false, autoAttachmentDto);
            }
        }
        if (data.getSingle() != null && data.getSingle() == true && data.getDownloadOption() == ReportDownloadOption.eMail) {
            a = reportUtil.japerPrintToResponseOutputStream(data.getDownloadFileType(), jasperPrint, "PICKUP_DELIVERY_ORDER", response, false, null);
            shipmentReportMailer.sendShipmentMailToCustomer(data.getResourceId(), logInUSer, ReportName.PICKUP_DELIVERY_ORDER, a);
        }
        if (isMultiple == true && data.getDownloadOption() == ReportDownloadOption.eMail) {
            a = reportUtil.japerPrintToResponseOutputStream(data.getDownloadFileType(), jasperPrint, "PICKUP_DELIVERY_ORDER", response, true, null);
            return a;
        }
        return null;
    }

    private JasperPrint getReport(BaseDto baseDto, ReportDownloadRequestDto data, UserProfile loggedInUser) {
        Consol consolEntity = consolRepository.findById(data.getResourceId());
        Map<String, Object> parameters = new HashMap<>();
        if (consolEntity != null) {
            try {
                String loggedInUserName = "";
                String loggedInLocation = "";
                String companyName = "";
                if (loggedInUser != null) {
                    if (loggedInUser.getEmployee() != null && loggedInUser.getEmployee().getEmployeeName() != null) {
                        loggedInUserName = loggedInUser.getEmployee().getEmployeeName();
                    } else if (loggedInUser.getUserName() != null) {
                        loggedInUserName = loggedInUser.getUserName();
                    } else {
                        loggedInUserName = "";
                    }
                    if (loggedInUser.getSelectedUserLocation() != null) {
                        LogoMaster logoMaster = logoMasterRepository.findByLocationMaster(loggedInUser.getSelectedUserLocation());   //getting logo
                        try {
                            if (logoMaster != null && logoMaster.getLogo() != null) {
                                InputStream in = new ByteArrayInputStream(logoMaster.getLogo());
                                BufferedImage bImageFromConvert = ImageIO.read(in);
                                parameters.put("logo", bImageFromConvert);
                                log.info("Image getting success::");
                            }
                        } catch (Exception ex) {
                            log.error("Image getting failed:::", ex);
                        }
                        loggedInLocation = loggedInUser.getSelectedUserLocation().getLocationName() != null
                                ? loggedInUser.getSelectedUserLocation().getLocationName() : "";
                        LocationMaster locationMaster = locationMasterRepository.findByLocationCode(loggedInUser.getSelectedUserLocation().getLocationCode());
                        if (locationMaster != null) {
                            parameters.put("companyAddressLine1", locationMaster.getAddressLine1());
                            parameters.put("companyAddressLine2", locationMaster.getAddressLine2());
                            parameters.put("companyAddressLine3", locationMaster.getAddressLine3());
                            companyName = locationMaster.getBranchName() != null ? locationMaster.getBranchName() : "";
                            parameters.put("companyName", reportUtil.removeSpecialCharacters(companyName, data.getDownloadOption()));
                        }
                        parameters.put("refDate", reportUtil.dateToReportDate(new Date(), loggedInUser.getSelectedUserLocation().getJavaDateFormat()));  //today date
                        parameters.put("generateDate", reportUtil.dateToReportDate(new Date(), loggedInUser.getSelectedUserLocation().getJavaDateFormat())); // today date it is also report generated date

                    } else {
                        loggedInLocation = "";
                    }
                    parameters.put("refPlace", loggedInUserName + "-" + loggedInLocation);

                    PickUpDeliveryPoint pickUpDeliveryPoint = consolEntity.getPickUpDeliveryPoint();
                    if (pickUpDeliveryPoint != null) {
                        if (pickUpDeliveryPoint.getTransporter() != null) {
                            parameters.put("transporterName", reportUtil.removeSpecialCharacters(pickUpDeliveryPoint.getTransporter().getPartyName(), data.getDownloadOption()));  //transporter name or partyname
                            if (pickUpDeliveryPoint.getTransporter() != null && pickUpDeliveryPoint.getTransporter().getPartyAddressList() != null && !pickUpDeliveryPoint.getTransporter().getPartyAddressList().isEmpty()) {
                                for (PartyAddressMaster partyAddressMaster : pickUpDeliveryPoint.getTransporter().getPartyAddressList()) {
                                    PartyAddressMaster partyAddressMasterObj = partyAddressMaster;                //getting corresponding partyaddress
                                    if (partyAddressMasterObj.getAddressType().equals(AddressType.Primary)) {       //from primary address
                                        parameters.put("faxNumber", partyAddressMasterObj.getFax());
                                        parameters.put("telephoneNumber", partyAddressMasterObj.getPhone());
                                        parameters.put("attentionName", partyAddressMasterObj.getContactPerson());

                                        parameters.put("transportAddressLine1", partyAddressMasterObj.getAddressLine1());
                                        parameters.put("transportAddressLine2", partyAddressMasterObj.getAddressLine2());
                                        parameters.put("transportAddressLine3", partyAddressMasterObj.getAddressLine3());
                                    } else if (partyAddressMasterObj.getAddressType().equals(AddressType.Billing)) {    //from billing address
                                        parameters.put("faxNumber", partyAddressMasterObj.getFax());
                                        parameters.put("telephoneNumber", partyAddressMasterObj.getPhone());
                                        parameters.put("attentionName", partyAddressMasterObj.getContactPerson());
                                        parameters.put("transportAddressLine1", partyAddressMasterObj.getAddressLine1());
                                        parameters.put("transportAddressLine2", partyAddressMasterObj.getAddressLine2());
                                        parameters.put("transportAddressLine3", partyAddressMasterObj.getAddressLine3());
                                    } else if (partyAddressMasterObj.getAddressType().equals(AddressType.Other)) {     // from other address
                                        parameters.put("faxNumber", partyAddressMasterObj.getFax());
                                        parameters.put("telephoneNumber", partyAddressMasterObj.getPhone());
                                        parameters.put("attentionName", partyAddressMasterObj.getContactPerson());
                                        parameters.put("transportAddressLine1", partyAddressMasterObj.getAddressLine1());
                                        parameters.put("transportAddressLine2", partyAddressMasterObj.getAddressLine2());
                                        parameters.put("transportAddressLine3", partyAddressMasterObj.getAddressLine3());
                                    }
                                }
                            }

                        }

                        if (pickUpDeliveryPoint.getPickupPoint() != null) {
                            parameters.put("pickUpFromName", reportUtil.removeSpecialCharacters(pickUpDeliveryPoint.getPickupPoint().getPartyName(), data.getDownloadOption()));       // pickupname and corresponding details
                        }
                        if (pickUpDeliveryPoint.getPickUpPlace() != null) {
                            String addressLine1 = pickUpDeliveryPoint.getPickUpPlace().getAddressLine1() != null ? pickUpDeliveryPoint.getPickUpPlace().getAddressLine1() : "";
                            String addressLine2 = pickUpDeliveryPoint.getPickUpPlace().getAddressLine2() != null ? pickUpDeliveryPoint.getPickUpPlace().getAddressLine2() : "";
                            String addressLine3 = pickUpDeliveryPoint.getPickUpPlace().getAddressLine3() != null ? pickUpDeliveryPoint.getPickUpPlace().getAddressLine3() : "";
                            String zipCode = pickUpDeliveryPoint.getPickUpPlace().getZipCode() != null ? pickUpDeliveryPoint.getPickUpPlace().getZipCode() : "";

                            String state = "";
                            String city = "";
                            if (pickUpDeliveryPoint.getPickUpPlace().getState() != null) {
                                state = pickUpDeliveryPoint.getPickUpPlace().getState().getStateName() != null ? pickUpDeliveryPoint.getPickUpPlace().getState().getStateName() : "";
                            }
                            if (pickUpDeliveryPoint.getPickUpPlace().getCity() != null) {
                                city = pickUpDeliveryPoint.getPickUpPlace().getCity().getCityName() != null ? pickUpDeliveryPoint.getPickUpPlace().getCity().getCityName() : "";
                            }
                            StringJoiner joiner = new StringJoiner(",");
                            String address = joiner.add(addressLine1).add(addressLine2).add(addressLine3).add(state).add(city).add(zipCode).toString();
                        /*String  sb =new StringBuilder().append(addressLine1).append(",").append(addressLine2).append(",").append(addressLine3)
								.append("\n").append(state).append(",").append(city).append(",").append(zipCode).toString();*/
                            parameters.put("pickUpAddressLine1", address);
                        }

                        parameters.put("pickUpContactName", pickUpDeliveryPoint.getPickUpContactPerson() != null ? pickUpDeliveryPoint.getPickUpContactPerson() : "");
                        parameters.put("pickUpTeleNumber", pickUpDeliveryPoint.getPickUpPhoneNo() != null ? pickUpDeliveryPoint.getPickUpPhoneNo() : "");
                        parameters.put("pickUpMobileNumber", pickUpDeliveryPoint.getPickUpMobileNo() != null ? pickUpDeliveryPoint.getPickUpMobileNo() : "");
                        parameters.put("pickUpEmail", pickUpDeliveryPoint.getPickUpEmail() != null ? pickUpDeliveryPoint.getPickUpEmail() : "");

                        if (pickUpDeliveryPoint.getDeliveryPoint() != null) {
                            parameters.put("deliveryToName", reportUtil.removeSpecialCharacters(pickUpDeliveryPoint.getDeliveryPoint().getCfsName(), data.getDownloadOption()));     //delivery name and corresponding details
                        }
                        if (pickUpDeliveryPoint.getDeliveryPlace() != null) {
                            String addressLine1 = pickUpDeliveryPoint.getDeliveryPlace().getAddressLine1() != null ? pickUpDeliveryPoint.getDeliveryPlace().getAddressLine1() : "";
                            String addressLine2 = pickUpDeliveryPoint.getDeliveryPlace().getAddressLine2() != null ? pickUpDeliveryPoint.getDeliveryPlace().getAddressLine2() : "";
                            String addressLine3 = pickUpDeliveryPoint.getDeliveryPlace().getAddressLine3() != null ? pickUpDeliveryPoint.getDeliveryPlace().getAddressLine3() : "";
                            String zipCode = pickUpDeliveryPoint.getDeliveryPlace().getZipCode() != null ? pickUpDeliveryPoint.getDeliveryPlace().getZipCode() : "";
                            String state = "";
                            String city = "";
                            if (pickUpDeliveryPoint.getDeliveryPlace().getState() != null) {
                                state = pickUpDeliveryPoint.getDeliveryPlace().getState().getStateName() != null ? pickUpDeliveryPoint.getDeliveryPlace().getState().getStateName() : "";
                            }
                            if (pickUpDeliveryPoint.getDeliveryPlace().getCity() != null) {
                                city = pickUpDeliveryPoint.getDeliveryPlace().getCity().getCityName() != null ? pickUpDeliveryPoint.getDeliveryPlace().getCity().getCityName() : "";
                            }
                            String sb = new StringBuilder().append(addressLine1).append(",").append(addressLine2).append(",").append(addressLine3)
                                    .append("\n").append(state).append(",").append(city).append(",").append(zipCode).toString();
                            parameters.put("deliveryToAddressLine1", sb);
                        }

                        parameters.put("deliveryToContactName", pickUpDeliveryPoint.getDeliveryContactPerson() != null ? pickUpDeliveryPoint.getDeliveryContactPerson() : "");
                        parameters.put("deliveryToTeleNumber", pickUpDeliveryPoint.getDeliveryPhoneNo() != null ? pickUpDeliveryPoint.getDeliveryPhoneNo() : "");
                        parameters.put("deliveryToMobileNumber", pickUpDeliveryPoint.getDeliveryMobileNo() != null ? pickUpDeliveryPoint.getDeliveryMobileNo() : "");
                        parameters.put("deliveryToEmail", pickUpDeliveryPoint.getDeliveryEmail() != null ? pickUpDeliveryPoint.getDeliveryEmail() : "");

                        if (pickUpDeliveryPoint.getPickUpActual() != null) {
                            parameters.put("loadingDate", reportUtil.dateToReportDate(pickUpDeliveryPoint.getPickUpActual(), loggedInUser.getSelectedUserLocation().getJavaDateFormat())); //Pickup actuall date is loading date
                        }
                        if (pickUpDeliveryPoint.getDeliveryActual() != null) {
                            parameters.put("portCutOffDate", reportUtil.dateToReportDate(pickUpDeliveryPoint.getDeliveryActual(), loggedInUser.getSelectedUserLocation().getJavaDateFormat())); //delivery Actuall date is portutoff date
                        }
                    }
                    if (consolEntity.getCarrier() != null) {
                        parameters.put("carrierName", consolEntity.getCarrier().getCarrierName());
                        if (consolEntity.getConsolDocument() != null) {
                            parameters.put("flightNo", consolEntity.getConsolDocument().getRouteNo()); // flightNo or routeNo
                            parameters.put("referenceNumber", consolEntity.getConsolUid()); // reference Number

                            String pack = AppUtil.getNumberFormat(consolEntity.getConsolDocument().getNoOfPieces(), consolEntity.getCountry().getLocale()); //noOfpieces
                            String packCode = "";
                            if (consolEntity.getConsolDocument().getPackMaster() != null) {
                                packCode = consolEntity.getConsolDocument().getPackMaster().getPackName();      //packName
                            }
                            String bothCode = pack + " " + packCode;
                            parameters.put("noOfpieces", bothCode);
                            if (consolEntity.getConsolDocument().getCommodityDescription() != null) {
                                parameters.put("commodityDescription", consolEntity.getConsolDocument().getCommodityDescription());
                            }
                            if (consolEntity.getConsolDocument().getVolumeWeight() != null) {
                                parameters.put("cftcbmValue", AppUtil.getWeightFormat(consolEntity.getConsolDocument().getVolumeWeight(), consolEntity.getCountry().getLocale()));
                            }

                            if (consolEntity.getConsolDocument().getBookingGrossWeight() != null) {
                                parameters.put("kgsLbsValue", AppUtil.getWeightFormat(consolEntity.getConsolDocument().getBookingGrossWeight(), consolEntity.getCountry().getLocale()));
                            }


                            if (consolEntity.getConsolDocument().getDimensionUnit() != null) {
                                if (consolEntity.getConsolDocument().getDimensionUnit().equals(DimensionUnit.CENTIMETERORKILOS)) {
                                    parameters.put("kgLB", "KG");
                                    parameters.put("centimeterOrInch", "cm");
                                } else {
                                    parameters.put("kgLB", "KG");
                                    parameters.put("centimeterOrInch", "in");
                                }
                            } else {
                                parameters.put("kgLB", "");
                                parameters.put("centimeterOrInch", "");

                            }

                            List<ShippingInstruction> dimensionList = new ArrayList<>();
                            Long totalNoOfPieces = 0L;
                            Double totalVolumeWeight = 0.0;
                            //Getting all dimension details from ConsolDocument
                            if (consolEntity.getConsolDocument().getDimensionList() != null && !consolEntity.getConsolDocument().getDimensionList().isEmpty()) {
                                for (ConsolDocumentDimension dimesionList : consolEntity.getConsolDocument().getDimensionList()) {
                                    ShippingInstruction dimensionDto = new ShippingInstruction();
                                    if (dimesionList != null) {
                                        dimensionDto.setHeight(AppUtil.getNumberFormat(dimesionList.getHeight(), consolEntity.getCountry().getLocale()));
                                        dimensionDto.setLength(AppUtil.getNumberFormat(dimesionList.getLength(), consolEntity.getCountry().getLocale()));
                                        dimensionDto.setWidth(AppUtil.getNumberFormat(dimesionList.getWidth(), consolEntity.getCountry().getLocale()));
                                        Long piece = Long.valueOf(dimesionList.getNoOfPiece());
                                        dimensionDto.setPieces(AppUtil.getNumberFormat(dimesionList.getNoOfPiece(), consolEntity.getCountry().getLocale()));
                                        totalNoOfPieces = totalNoOfPieces + piece;
                                        Double dimensionvolume = 0.0;
                                        dimensionvolume = dimesionList.getVolWeight();
                                        totalVolumeWeight = totalVolumeWeight + dimensionvolume;
                                        dimensionDto.setVolume(AppUtil.getWeightFormat(dimensionvolume, consolEntity.getCountry().getLocale()));
                                    }


                                    dimensionList.add(dimensionDto);    //Adding dimension Object
                                }
                            }
                            parameters.put("totalNoOfPieces", AppUtil.getNumberFormat(totalNoOfPieces, consolEntity.getCountry().getLocale()));
                            parameters.put("totalVolumeWeight", AppUtil.getWeightFormat(totalVolumeWeight, consolEntity.getCountry().getLocale()));
                            parameters.put("dimensionList", dimensionList);    //showing Dimension list valuess in jasper report
                        }
                    }


                    ReportConfigurationMaster tmpObj = reportConfigurationMasterRepository.getByKeyAndLocation(ReportConfig.PICKUP_DELIVERY, consolEntity.getLocation().getId());
                    if (tmpObj != null && tmpObj.getReportValue() != null) {
                        parameters.put("pickUpDeliveryFooter", tmpObj.getReportValue());
                    }

                    parameters.put("reportName", "Pickup/Delivery Order");
                    JasperReport jasperReport = JasperCompileManager.compileReport(appResourcePath + "/efs-report/pickup_delivery_order.jrxml");
                    List<Object> fieldCollection1 = new ArrayList<>();
                    Object obj1 = new Object();
                    fieldCollection1.add(obj1);
                    JRDataSource dataSource = new JRBeanCollectionDataSource(fieldCollection1, false);

                    return JasperFillManager.fillReport(jasperReport, parameters, dataSource);

                }
            } catch (RestException re) {
                log.error("RestException  Occures in Pickup delivery:: ", re);
            } catch (Exception exception) {
                exception.printStackTrace();
            }
        }
        return null;
    }
}
