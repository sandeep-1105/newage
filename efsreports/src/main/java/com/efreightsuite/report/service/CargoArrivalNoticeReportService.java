package com.efreightsuite.report.service;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletResponse;

import com.efreightsuite.dto.AutoAttachmentDto;
import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.ReportDownloadRequestDto;
import com.efreightsuite.enumeration.AddressType;
import com.efreightsuite.enumeration.ReportConfig;
import com.efreightsuite.enumeration.ReportDownloadOption;
import com.efreightsuite.enumeration.ReportName;
import com.efreightsuite.enumeration.YesNo;
import com.efreightsuite.model.DocumentDetail;
import com.efreightsuite.model.InvoiceCreditNote;
import com.efreightsuite.model.InvoiceCreditNoteDetail;
import com.efreightsuite.model.LocationMaster;
import com.efreightsuite.model.LogoMaster;
import com.efreightsuite.model.PartyAddressMaster;
import com.efreightsuite.model.ReportConfigurationMaster;
import com.efreightsuite.model.ShipmentCharge;
import com.efreightsuite.model.ShipmentServiceDetail;
import com.efreightsuite.model.UserProfile;
import com.efreightsuite.report.CargoCharges;
import com.efreightsuite.service.AuthService;
import com.efreightsuite.util.AppUtil;
import lombok.extern.log4j.Log4j2;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

import org.springframework.stereotype.Service;

@Service
@Log4j2
public class CargoArrivalNoticeReportService extends AbstractReportService {
    @Override
    public ReportName getReportType() {
        return ReportName.CARGO_ARRICAL_NOTICE;
    }

    public byte[] processReport(ReportDownloadRequestDto data, HttpServletResponse response, boolean isMultiple) {
        log.info("I am in CargoArrivalNoticeReportService..");
        BaseDto baseDto = new BaseDto();
        UserProfile logInUSer = AuthService.getCurrentUser();
        JasperPrint jasperPrint = getReport(baseDto, data, AuthService.getCurrentUser());

        byte[] a = null;
        AutoAttachmentDto autoAttachmentDto = reportUtil.autoAttachment(ReportName.CARGO_ARRICAL_NOTICE, YesNo.Yes, data.getResourceId(), "Shipment");
        if (data.getSingle() != null && data.getSingle() == true) {
            if (data.getDownloadOption() == ReportDownloadOption.Preview || data.getDownloadOption() == ReportDownloadOption.Print) {
                a = reportUtil.japerPrintToResponseOutputStream(data.getDownloadFileType(), jasperPrint, "CARGO_ARRICAL_NOTICE", response, false, autoAttachmentDto);
                shipmentReportMailer.sendShipmentMailToCustomer(data.getResourceId(), logInUSer, ReportName.CARGO_ARRICAL_NOTICE, a);
            } else if (autoAttachmentDto != null && autoAttachmentDto.getIsAttach() != null || data.getDownloadOption() == ReportDownloadOption.Download) {
                a = reportUtil.japerPrintToResponseOutputStream(data.getDownloadFileType(), jasperPrint, "CARGO_ARRICAL_NOTICE", response, false, autoAttachmentDto);
                shipmentReportMailer.sendShipmentMailToCustomer(data.getResourceId(), logInUSer, ReportName.CARGO_ARRICAL_NOTICE, a);
            }
        }
        if (data.getSingle() != null && data.getSingle() == true && data.getDownloadOption() == ReportDownloadOption.eMail) {
            a = reportUtil.japerPrintToResponseOutputStream(data.getDownloadFileType(), jasperPrint, "CARGO_ARRICAL_NOTICE", response, false, null);
            shipmentReportMailer.sendShipmentMailToCustomer(data.getResourceId(), logInUSer, ReportName.CARGO_ARRICAL_NOTICE, a);
        }
        if (isMultiple == true && data.getDownloadOption() == ReportDownloadOption.eMail) {
            a = reportUtil.japerPrintToResponseOutputStream(data.getDownloadFileType(), jasperPrint, "CARGO_ARRICAL_NOTICE", response, true, null);
            return a;
        }
        return null;
    }

    private JasperPrint getReport(BaseDto baseDto, ReportDownloadRequestDto data, UserProfile loggedInUser) {
        log.info("CargoArrivalNoticeReport.. " + data.getResourceId() + " function called");
        DocumentDetail document = documentDetailRepository.findById(data.getResourceId());
        ShipmentServiceDetail sd = null;
        sd = shipmentServiceDetailRepository.findByServiceUid(document.getServiceUid());

		/*if (sd != null && sd.getDocumentList() != null && sd.getDocumentList().size() > 0) {
			for (int i = 0; i < sd.getDocumentList().size(); i++) {
				if (sd.getDocumentList().get(i).getId() == document.getId()) {
					document = documentDetailRepository.findById(sd.getDocumentList().get(i).getId());
					break;
				}
			}
		}*/

        try {
            JasperReport jasperReport = null;
            Map<String, Object> parameters = new HashMap<>();
            jasperReport = JasperCompileManager.compileReport(appResourcePath + "/efs-report/cargo_arival_notice.jrxml");
            String loggedInUserName = "";
            String loggedInLocation = "";
            String companyName = "";

            if (loggedInUser != null) {
                if (loggedInUser.getEmployee() != null && loggedInUser.getEmployee().getEmployeeName() != null) {
                    loggedInUserName = loggedInUser.getEmployee().getEmployeeName();
                } else if (loggedInUser.getUserName() != null) {
                    loggedInUserName = loggedInUser.getUserName();
                } else {
                    loggedInUserName = "";
                }

                if (loggedInUser.getSelectedUserLocation() != null) {
                    loggedInLocation = loggedInUser.getSelectedUserLocation().getLocationName() != null ? loggedInUser.getSelectedUserLocation().getLocationName() : "";
                    LocationMaster locationMaster = locationMasterRepository.findByLocationCode(loggedInUser.getSelectedUserLocation().getLocationCode());
                    if (locationMaster != null) {
                        parameters.put("companyAddressLine1", locationMaster.getAddressLine1() != null ? locationMaster.getAddressLine1() : "");
                        parameters.put("companyAddressLine2", locationMaster.getAddressLine2() != null ? locationMaster.getAddressLine2() : "");
                        parameters.put("companyAddressLine3", locationMaster.getAddressLine3() != null ? locationMaster.getAddressLine3() : "");
                        companyName = locationMaster.getBranchName() != null ? locationMaster.getBranchName() : "";
                        parameters.put("companyName", reportUtil.removeSpecialCharacters(companyName, data.getDownloadOption()));

                        ReportConfigurationMaster tmpObj = reportConfigurationMasterRepository.getByKeyAndLocation(ReportConfig.CARGO_ARRIVAL_NOTICE_HEADER, locationMaster.getId());
                        if (tmpObj != null && tmpObj.getReportValue() != null) {
                            parameters.put("cargoArrivalNoticeHeader", tmpObj.getReportValue());
                        }

                    }
                    LogoMaster logoMaster = logoMasterRepository.findByLocationMaster(loggedInUser.getSelectedUserLocation());
                    try {
                        if (logoMaster != null && logoMaster.getLogo() != null) {
                            InputStream in = new ByteArrayInputStream(logoMaster.getLogo());
                            BufferedImage bImageFromConvert = ImageIO.read(in);
                            parameters.put("logo", bImageFromConvert);
                            log.info("Image getting success::");
                        }
                    } catch (Exception ex) {
                        log.error("Image getting failed:::", ex);
                    }

                } else {
                    loggedInLocation = "";
                }
            }

            parameters.put("refPlace", loggedInLocation + "-" + loggedInUserName);
            parameters.put("fromName", loggedInUserName);
            parameters.put("refDate", reportUtil.dateToReportDate(new Date(), loggedInUser.getSelectedUserLocation().getJavaDateFormat()));
            parameters.put("jobReferenceDate", reportUtil.dateToReportDate(new Date(), loggedInUser.getSelectedUserLocation().getJavaDateFormat()));


            if (sd != null) {
                List<InvoiceCreditNote> invoiceList = invoiceCreditNoteRepository.findByServiceUid(sd.getServiceUid());
                if (invoiceList != null && !invoiceList.isEmpty()) {
                    for (InvoiceCreditNote invoice : invoiceList) {
                        for (InvoiceCreditNoteDetail invoiceDetail : invoice.getInvoiceCreditNoteDetailList()) {
                            if (invoiceDetail != null) {
                                parameters.put("invoiceReference", invoiceDetail.getInvoiceNo() != null ? invoiceDetail.getInvoiceNo() : "");
                            }
                        }
                    }
                }

                parameters.put("poNO", sd.getPoNo() != null ? sd.getPoNo() : "");

                if (document != null) {
                    parameters.put("canNumber", document.getCanNumber() != null ? document.getCanNumber() : "");

                    if (document.getCanIssuedDate() != null) {
                        parameters.put("canDate", reportUtil.dateToReportDate(document.getCanIssuedDate(), loggedInUser.getSelectedUserLocation().getJavaDateFormat()));
                    } else {
                        if (document.getCanNumber() != null) {
                            parameters.put("canDate", reportUtil.dateToReportDate(new Date(), loggedInUser.getSelectedUserLocation().getJavaDateFormat()));
                        }
                    }

                    int shipperAddressIndex = 1;
                    int consigneeAddressIndex = 1;

                    int notifyIndex = 1;
                    if (document.getFirstNotify() != null) {
                        parameters.put("notifyName", document.getFirstNotify().getPartyName() != null ? document.getFirstNotify().getPartyName() : "");
                    }
                    parameters.put("jobReferenceNo", document.getShipmentUid());
                    parameters.put("canLabel", "Job Ref No");
                    if (document.getFirstNotifyAddress() != null) {
                        parameters.put("notifyAddressLine" + notifyIndex, document.getFirstNotifyAddress().getAddressLine1());
                        notifyIndex++;
                        parameters.put("notifyAddressLine" + notifyIndex, document.getFirstNotifyAddress().getAddressLine2());
                        notifyIndex++;
                        parameters.put("notifyAddressLine" + notifyIndex, document.getFirstNotifyAddress().getAddressLine3());
                        notifyIndex++;
                        parameters.put("notifyAddressLine" + notifyIndex, document.getFirstNotifyAddress().getAddressLine4());
                        notifyIndex++;
                    }
                    if (document.getShipper() != null && document.getShipper().getPartyName() != null) {
                        parameters.put("shipperName", reportUtil.removeSpecialCharacters(document.getShipper().getPartyName(), data.getDownloadOption()));
                    }
                    if (document.getConsignee() != null && document.getConsignee().getPartyName() != null) {
                        parameters.put("consigneeName", reportUtil.removeSpecialCharacters(document.getConsignee().getPartyName(), data.getDownloadOption()));
                    }
                    if (document.getShipperAddress() != null) {
                        parameters.put("shipperAddressLine" + shipperAddressIndex, document.getShipperAddress().getAddressLine1());
                        shipperAddressIndex++;
                        parameters.put("shipperAddressLine" + shipperAddressIndex, document.getShipperAddress().getAddressLine2());
                        shipperAddressIndex++;
                        parameters.put("shipperAddressLine" + shipperAddressIndex, document.getShipperAddress().getAddressLine3());
                        shipperAddressIndex++;
                        parameters.put("shipperAddressLine" + shipperAddressIndex, document.getShipperAddress().getAddressLine4());
                        shipperAddressIndex++;
                    }
                    if (document.getConsigneeAddress() != null) {
                        parameters.put("consigneeAddressLine" + consigneeAddressIndex, document.getConsigneeAddress().getAddressLine1());
                        consigneeAddressIndex++;
                        parameters.put("consigneeAddressLine" + consigneeAddressIndex, document.getConsigneeAddress().getAddressLine2());
                        consigneeAddressIndex++;
                        parameters.put("consigneeAddressLine" + consigneeAddressIndex, document.getConsigneeAddress().getAddressLine3());
                        consigneeAddressIndex++;
                        parameters.put("consigneeAddressLine" + consigneeAddressIndex, document.getConsigneeAddress().getAddressLine4());
                        consigneeAddressIndex++;
                    }
                    if (document.getConsignee() != null && document.getConsignee().getPartyAddressList() != null && !document.getConsignee().getPartyAddressList().isEmpty()) {
                        for (PartyAddressMaster partyAddressMaster : document.getConsignee().getPartyAddressList()) {
                            PartyAddressMaster partyAddressMasterObj = partyAddressMaster;
                            if (partyAddressMasterObj.getAddressType() == AddressType.Primary) {
                                parameters.put("attentionName", partyAddressMasterObj.getContactPerson() != null ? partyAddressMasterObj.getContactPerson() : "");
                            } else if (partyAddressMasterObj.getAddressType() == AddressType.Billing) {
                                parameters.put("attentionName", partyAddressMasterObj.getContactPerson() != null ? partyAddressMasterObj.getContactPerson() : "");
                            } else if (partyAddressMasterObj.getAddressType() == AddressType.Other) {
                                parameters.put("attentionName", partyAddressMasterObj.getContactPerson() != null ? partyAddressMasterObj.getContactPerson() : "");
                            }
                        }
                    }


                    if (document.getNoOfPieces() != null) {
                        parameters.put("noOfPcs", AppUtil.getNumberFormat(document.getNoOfPieces(), sd.getCountry().getLocale()));
                    }
                    if (document.getChargebleWeight() != null) {
                        parameters.put("chargableWeight", AppUtil.getWeightFormat(document.getChargebleWeight(), sd.getCountry().getLocale()));
                    }
                    if (document.getGrossWeight() != null) {
                        parameters.put("grossWeight", AppUtil.getWeightFormat(document.getGrossWeight(), sd.getCountry().getLocale()));
                    }

                    parameters.put("commodityDescription", document.getCommodityDescription() != null ? document.getCommodityDescription() : "");

                    if (document.getCarrier() != null) {
                        parameters.put("carrierName", document.getCarrier().getCarrierName() != null ? document.getCarrier().getCarrierName() : "");
                    }
                    if (document.getPol() != null) {
                        parameters.put("pol", document.getPol().getPortName() != null ? document.getPol().getPortName() : "");
                    }
                    if (document.getPod() != null) {
                        parameters.put("pod", document.getPod().getPortName() != null ? document.getPod().getPortName() : "");
                        parameters.put("finalDestination", document.getPod().getPortName() != null ? document.getPod().getPortName() : "");
                    }
                    if (sd != null && sd.getMawbNo() != null) {
                        parameters.put("mawbNo", sd.getMawbNo());
                    }
                    if (document != null) {
                        parameters.put("hawbNo", document.getHawbNo());
                        parameters.put("bookingNo", document.getShipmentUid());
                    }
                    if (document.getEta() != null) {
                        parameters.put("arrivalDate", reportUtil.dateToReportDate(document.getEta(), loggedInUser.getSelectedUserLocation().getJavaDateFormat()));
                    }
                }
            }
            List<CargoCharges> cargoChargeList = new ArrayList<>();

            String rateMerged = appUtil.getLocationConfig("booking.rates.merged", sd.getLocation(), true);

            if (rateMerged != null) {
                if (rateMerged.equals("FALSE") || rateMerged == "FALSE") {
                    if (sd.getShipmentChargeList() != null && !sd.getShipmentChargeList().isEmpty()) {
                        for (ShipmentCharge shipmentCharge : sd.getShipmentChargeList()) {
                            CargoCharges charges = new CargoCharges();
                            charges.setChargeName(shipmentCharge.getChargeName() != null ? shipmentCharge.getChargeName() : "");
                            charges.setCurrencyCode(shipmentCharge.getNetSaleCurrency().getCurrencyCode() != null ? shipmentCharge.getNetSaleCurrency().getCurrencyCode() : "");

                            if (shipmentCharge.getLocalNetSaleRoe() != null) {
                                if (shipmentCharge.getLocalNetSaleRoe() != null) {
                                    charges.setRoe(AppUtil.getCurrencyFormat(shipmentCharge.getLocalNetSaleRoe(), shipmentCharge.getNetSaleCurrency().getCountryMaster().getLocale(), shipmentCharge.getNetSaleCurrency().getDecimalPoint()));
                                }
                            }
                            if (shipmentCharge.getNetSaleAmount() != null) {
                                charges.setAmount(AppUtil.getCurrencyFormat(shipmentCharge.getNetSaleAmount(), shipmentCharge.getNetSaleCurrency().getCountryMaster().getLocale(), shipmentCharge.getNetSaleCurrency().getDecimalPoint()));
                            }
                            if (shipmentCharge.getLocalTotalNetSale() != null) {
                                charges.setLocalAmount(AppUtil.getCurrencyFormat(shipmentCharge.getLocalTotalNetSale(), shipmentCharge.getNetSaleCurrency().getCountryMaster().getLocale(), shipmentCharge.getNetSaleCurrency().getDecimalPoint()));
                            }

                            cargoChargeList.add(charges);

                            if (sd.getClientNetRate() != null) {
                                parameters.put("totalLocalAmount", AppUtil.getCurrencyFormat(sd.getClientNetRate(), sd.getLocalCurrency().getCountryMaster().getLocale(), sd.getLocalCurrency().getDecimalPoint()));
                            }
                        }
                    }
                } else {
                    if (sd.getShipmentChargeList() != null && !sd.getShipmentChargeList().isEmpty()) {
                        for (ShipmentCharge shipmentCharge : sd.getShipmentChargeList()) {
                            CargoCharges charges = new CargoCharges();
                            charges.setChargeName(shipmentCharge.getChargeName() != null ? shipmentCharge.getChargeName() : "");
                            charges.setCurrencyCode(shipmentCharge.getRateCurrency().getCurrencyCode() != null ? shipmentCharge.getRateCurrency().getCurrencyCode() : "");

                            if (shipmentCharge.getLocalRateAmountRoe() != null) {
                                charges.setRoe(AppUtil.getCurrencyFormat(shipmentCharge.getLocalRateAmountRoe(), shipmentCharge.getRateCurrency().getCountryMaster().getLocale(), shipmentCharge.getRateCurrency().getDecimalPoint()));
                            }
                            if (shipmentCharge.getRateAmount() != null) {
                                charges.setAmount(AppUtil.getCurrencyFormat(shipmentCharge.getRateAmount(), shipmentCharge.getRateCurrency().getCountryMaster().getLocale(), shipmentCharge.getRateCurrency().getDecimalPoint()));
                            }
                            if (shipmentCharge.getLocalTotalRateAmount() != null) {
                                charges.setLocalAmount(AppUtil.getCurrencyFormat(shipmentCharge.getLocalTotalRateAmount(), shipmentCharge.getRateCurrency().getCountryMaster().getLocale(), shipmentCharge.getRateCurrency().getDecimalPoint()));
                            }

                            cargoChargeList.add(charges);

                            if (sd.getRate() != null) {
                                parameters.put("totalLocalAmount", AppUtil.getCurrencyFormat(sd.getRate(), sd.getLocalCurrency().getCountryMaster().getLocale(), sd.getLocalCurrency().getDecimalPoint()));
                            }
                        }
                    }
                }

            }

            parameters.put("totalAmountList", cargoChargeList);
            if (sd != null && sd.getLocalCurrencyCode() != null) {
                parameters.put("localCurrencyCode", sd.getLocalCurrencyCode());
            } else {
                parameters.put("localCurrencyCode", "");
            }
            // parameters.put("totalLocalAmount",AppUtil.getCurrencyFormat(totalLocalAmount, document.getCurrency().getCountryMaster().getLocale(),document.getCurrency().getDecimalPoint()));
            List<Object> fieldCollection1 = new ArrayList<>();
            Object obj1 = new Object();
            fieldCollection1.add(obj1);
            JRDataSource dataSource = new JRBeanCollectionDataSource(fieldCollection1, false);
            return JasperFillManager.fillReport(jasperReport, parameters, dataSource);

        } catch (JRException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
