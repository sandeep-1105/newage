package com.efreightsuite.report.service;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletResponse;

import com.efreightsuite.dto.AutoAttachmentDto;
import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.ReportDownloadRequestDto;
import com.efreightsuite.enumeration.PPCC;
import com.efreightsuite.enumeration.ReportDownloadOption;
import com.efreightsuite.enumeration.ReportName;
import com.efreightsuite.enumeration.YesNo;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.model.AddressMaster;
import com.efreightsuite.model.Consol;
import com.efreightsuite.model.ConsolCharge;
import com.efreightsuite.model.ConsolConnection;
import com.efreightsuite.model.DocumentDetail;
import com.efreightsuite.model.LocationMaster;
import com.efreightsuite.model.LogoMaster;
import com.efreightsuite.model.ShipmentLink;
import com.efreightsuite.model.ShipmentServiceDetail;
import com.efreightsuite.model.UserProfile;
import com.efreightsuite.report.CargoManifest;
import com.efreightsuite.report.FlightDetail;
import com.efreightsuite.service.AuthService;
import com.efreightsuite.util.AppUtil;
import lombok.extern.log4j.Log4j2;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

import org.springframework.stereotype.Service;

@Service
@Log4j2
public class CargoManifestReportService extends AbstractReportService {
    public ReportName getReportType() {
        return ReportName.CARGO_MANIFEST;
    }

    public byte[] processReport(ReportDownloadRequestDto data, HttpServletResponse response, boolean isMultiple) {
        log.info("I am in CargoManifestReportService..");
        BaseDto baseDto = new BaseDto();
        UserProfile logInUSer = AuthService.getCurrentUser();
        JasperPrint jasperPrint = getReport(baseDto, data, AuthService.getCurrentUser());

        byte[] a = null;
        AutoAttachmentDto autoAttachmentDto = reportUtil.autoAttachment(ReportName.CARGO_MANIFEST, YesNo.Yes, data.getResourceId(), "MasterShipment");
        if (data.getSingle() != null && data.getSingle() == true) {
            if (data.getDownloadOption() == ReportDownloadOption.Preview || data.getDownloadOption() == ReportDownloadOption.Print) {
                reportUtil.japerPrintToResponseOutputStream(data.getDownloadFileType(), jasperPrint, "CARGO_MANIFEST", response, false, null);
            } else if (autoAttachmentDto != null && autoAttachmentDto.getIsAttach() != null || data.getDownloadOption() == ReportDownloadOption.Download) {
                reportUtil.japerPrintToResponseOutputStream(data.getDownloadFileType(), jasperPrint, "CARGO_MANIFEST", response, false, autoAttachmentDto);
            }
        }
        if (data.getSingle() != null && data.getSingle() == true && data.getDownloadOption() == ReportDownloadOption.eMail) {
            a = reportUtil.japerPrintToResponseOutputStream(data.getDownloadFileType(), jasperPrint, "CARGO_MANIFEST", response, false, null);
            consolReportMailer.sendConsolMailToCustomer(data.getResourceId(), logInUSer, ReportName.CARGO_MANIFEST, a);
        }
        if (isMultiple == true && data.getDownloadOption() == ReportDownloadOption.eMail) {
            a = reportUtil.japerPrintToResponseOutputStream(data.getDownloadFileType(), jasperPrint, "CARGO_MANIFEST", response, true, null);
            return a;
        }
        return null;
    }

    private JasperPrint getReport(BaseDto baseDto, ReportDownloadRequestDto data, UserProfile loggedInUser) {
        log.info("Cargo Manifest[" + data.getResourceId() + "] function called");
        Consol consolEntity = consolRepository.findById(data.getResourceId());
        if (consolEntity != null) {
            Map<String, Object> parameters = new HashMap<>();
            try {
                String jobNumber = consolEntity.getConsolUid() != null ? consolEntity.getConsolUid() : "";
                String serviceCode = "";

                if (consolEntity.getServiceMaster() != null) {
                    serviceCode = consolEntity.getServiceMaster().getServiceCode();
                } else {
                    serviceCode = "";
                }
                parameters.put("jobNumber", serviceCode + "-" + jobNumber);
                if (consolEntity.getOrigin() != null && consolEntity.getOrigin().getPortName() != null) {
                    parameters.put("originName", consolEntity.getOrigin().getPortName());
                }
                if (consolEntity.getDestination() != null && consolEntity.getDestination().getPortName() != null) {
                    parameters.put("destinationName", consolEntity.getDestination().getPortName());
                }
                if (consolEntity.getCarrier() != null) {
                    parameters.put("carrierName", consolEntity.getCarrier().getCarrierName() != null ? consolEntity.getCarrier().getCarrierName() : "");
                }
                if (consolEntity.getConsolDocument() != null) {
                    if (consolEntity.getConsolDocument().getRouteNo() != null) {
                        parameters.put("flightNo", consolEntity.getConsolDocument().getRouteNo());
                    }
                    if (consolEntity.getConsolDocument().getConsignee() != null && consolEntity.getConsolDocument().getConsignee().getPartyName() != null) {
                        parameters.put("consigneeName", reportUtil.removeSpecialCharacters(consolEntity.getConsolDocument().getConsignee().getPartyName(), data.getDownloadOption()));
                    }
                    if (consolEntity.getConsolDocument().getMawbNo() != null) {
                        parameters.put("mawbNumber", consolEntity.getConsolDocument().getMawbNo());
                    }
                    if (consolEntity.getNotes() != null) {
                        if (data.getShowNote() != null && data.getShowNote().equals(YesNo.Yes) && (data.getDownloadOption().equals(ReportDownloadOption.Preview) || data.getDownloadOption().equals(ReportDownloadOption.Print))) {
                            parameters.put("remarks", consolEntity.getNotes());
                        } else if (data.getShowNote() != null && data.getShowNote().equals(YesNo.No) && (data.getDownloadOption().equals(ReportDownloadOption.Preview) || data.getDownloadOption().equals(ReportDownloadOption.Print))) {
                            parameters.put("remarks", "");
                        } else {
                            parameters.put("remarks", consolEntity.getNotes());
                        }
                    }
                }
                if (consolEntity.getAgent() != null && consolEntity.getAgent().getPartyName() != null) {
                    String agentName = reportUtil.removeSpecialCharacters(consolEntity.getAgent().getPartyName(), data.getDownloadOption());
                    String agentCode = consolEntity.getAgentCode();
                    parameters.put("destinationAgent", agentName + "- " + agentCode);
                }
                if (consolEntity.getAgentAddress() != null) {
                    parameters.put("destinationAddressLine1", consolEntity.getAgentAddress().getAddressLine1() != null ? consolEntity.getAgentAddress().getAddressLine1() : "");
                    parameters.put("destinationAddressLine2", consolEntity.getAgentAddress().getAddressLine2() != null ? consolEntity.getAgentAddress().getAddressLine2() : "");
                }
                String address3 = "";
                String address4 = "";
                if (consolEntity.getAgentAddress() != null && consolEntity.getAgentAddress().getAddressLine3() != null) {
                    address3 = consolEntity.getAgentAddress().getAddressLine3();
                } else {
                    address3 = "";
                }
                if (consolEntity.getAgentAddress() != null) {
                    address4 = consolEntity.getAgentAddress().getAddressLine4() != null ? consolEntity.getAgentAddress().getAddressLine4() : "";
                } else {
                    address4 = "";
                }
                parameters.put("destinationAddressLine3", address3 + " " + address4);
                if (consolEntity.getConsolDocument() != null) {
                    if (consolEntity.getConsolDocument().getIssuingAgent() != null && consolEntity.getConsolDocument().getIssuingAgent().getPartyName() != null) {
                        parameters.put("originAgent", reportUtil.removeSpecialCharacters(consolEntity.getConsolDocument().getIssuingAgent().getPartyName(), data.getDownloadOption()));
                    }
                    if (consolEntity.getConsolDocument().getIssuingAgentAddress() != null) {
                        parameters.put("originAgentAddressLine1", consolEntity.getConsolDocument().getIssuingAgentAddress().getAddressLine1() != null ? consolEntity.getConsolDocument().getIssuingAgentAddress().getAddressLine1() : "");
                        parameters.put("originAgentAddressLine2", consolEntity.getConsolDocument().getIssuingAgentAddress().getAddressLine2() != null ? consolEntity.getConsolDocument().getIssuingAgentAddress().getAddressLine2() : "");
                        parameters.put("originAgentAddressLine3", consolEntity.getConsolDocument().getIssuingAgentAddress().getAddressLine3() != null ? consolEntity.getConsolDocument().getIssuingAgentAddress().getAddressLine3() : "");
                        parameters.put("originAgentAddressLine4", consolEntity.getConsolDocument().getIssuingAgentAddress().getAddressLine4() != null ? consolEntity.getConsolDocument().getIssuingAgentAddress().getAddressLine4() : "");
                    }
                }

                Double totalGrossWeight = 0.0;
                Double totalchargeWeight = 0.0;
                Double totalVoluemWeight = 0.0;
                Long totalPieces = 0L;
                List<CargoManifest> cargoManifestList = new ArrayList<>();
                if (consolEntity.getShipmentLinkList() != null && consolEntity.getShipmentLinkList().size() > 0) {
                    for (ShipmentLink shipmentLink : consolEntity.getShipmentLinkList()) {
                        ShipmentServiceDetail service = shipmentLink.getService();
                        log.info("ServiceId:::::::::" + service.getServiceUid());
                        for (DocumentDetail document : service.getDocumentList()) {
                            log.info("DocumentID:::::::::" + document.getDocumentNo());
                            CargoManifest cargoManifest = new CargoManifest();
                            if (document != null) {
                                cargoManifest.setHawbNumber(document.getDocumentNo() != null ? document.getDocumentNo() : "");
                                if (document.getOrigin() != null) {
                                    cargoManifest.setOriginName(document.getOrigin().getPortName());
                                }
                                if (document.getDestination() != null) {
                                    cargoManifest.setDestinationName(document.getDestination().getPortName());
                                }
                                if (document != null) {
                                    Double grossWeight = document.getGrossWeight();
                                    Double voluemWeight = document.getVolumeWeight();
                                    Double chargeWeight = document.getChargebleWeight();
                                    Long pieces = document.getNoOfPieces();

                                    if (grossWeight != null) {
                                        cargoManifest.setGrossWeight(AppUtil.getWeightFormat(grossWeight, service.getCountry().getLocale()));
                                    }
                                    if (document.getChargebleWeight() != null) {
                                        if (data.getShowChargeWeight() != null && data.getShowChargeWeight().equals(YesNo.Yes) && (data.getDownloadOption().equals(ReportDownloadOption.Preview) || data.getDownloadOption().equals(ReportDownloadOption.Print))) {
                                            cargoManifest.setChargeableWeight(AppUtil.getWeightFormat(document.getChargebleWeight(), service.getCountry().getLocale()));
                                        } else if (data.getShowChargeWeight() != null && data.getShowChargeWeight().equals(YesNo.No) && (data.getDownloadOption().equals(ReportDownloadOption.Preview) || data.getDownloadOption().equals(ReportDownloadOption.Print))) {
                                            cargoManifest.setChargeableWeight("");
                                        } else {
                                            cargoManifest.setChargeableWeight(AppUtil.getWeightFormat(document.getChargebleWeight(), service.getCountry().getLocale()));
                                        }
                                    }

                                    if (document.getVolumeWeight() != null) {
                                        if (data.getShowVolume() != null && data.getShowVolume().equals(YesNo.Yes) && (data.getDownloadOption().equals(ReportDownloadOption.Preview) || data.getDownloadOption().equals(ReportDownloadOption.Print))) {
                                            cargoManifest.setVolume(AppUtil.getWeightFormat(document.getVolumeWeight(), service.getCountry().getLocale()));
                                        } else if (data.getShowVolume() != null && data.getShowVolume().equals(YesNo.No) && (data.getDownloadOption().equals(ReportDownloadOption.Preview) || data.getDownloadOption().equals(ReportDownloadOption.Print))) {
                                            cargoManifest.setVolume("");
                                        } else {
                                            cargoManifest.setVolume(AppUtil.getWeightFormat(document.getVolumeWeight(), service.getCountry().getLocale()));
                                        }
                                    }

                                    cargoManifest.setNoOfPieces(AppUtil.getNumberFormat(pieces, service.getCountry().getLocale()));
                                    if (totalGrossWeight != null && grossWeight != null) {
                                        totalGrossWeight = totalGrossWeight + grossWeight;
                                    }

                                    if (totalchargeWeight != null && chargeWeight != null) {
                                        totalchargeWeight = totalchargeWeight + chargeWeight;
                                    }

                                    if (totalVoluemWeight != null && voluemWeight != null) {
                                        totalVoluemWeight = totalVoluemWeight + voluemWeight;
                                    }
                                    if (totalPieces != null && pieces != null)
                                        totalPieces = totalPieces + pieces;
                                }
                                if (service.getPpcc().equals(PPCC.Prepaid)) {
                                    cargoManifest.setFrieghtName("Prepaid");
                                } else if (service.getPpcc().equals(PPCC.Collect)) {
                                    cargoManifest.setFrieghtName("Collect");
                                }
                                String shipperAddressLine3 = "";
                                String shipperAddressLine4 = "";
                                String consigneeAddressLine3 = "";
                                String consigneeAddressLine4 = "";
                                if (document.getShipperAddress() != null && document.getShipperAddress().getAddressLine3() != null) {
                                    shipperAddressLine3 = document.getShipperAddress().getAddressLine3();
                                } else {
                                    shipperAddressLine3 = "";
                                }
                                if (document.getShipperAddress() != null && document.getShipperAddress().getAddressLine4() != null) {
                                    shipperAddressLine4 = document.getShipperAddress().getAddressLine4();
                                } else {
                                    shipperAddressLine4 = "";
                                }

                                if (document.getShipper() != null) {
                                    String shipper = document.getShipper().getPartyName() != null ? document.getShipper().getPartyName() : "";
                                    String removeShipper = reportUtil.removeSpecialCharacters(shipper, data.getDownloadOption());
                                    log.info("ShipperName:::::::" + removeShipper);
                                    if (data.getShowShipperName() != null && data.getShowShipperName().equals(YesNo.Yes) && (data.getDownloadOption().equals(ReportDownloadOption.Preview) || data.getDownloadOption().equals(ReportDownloadOption.Print))) {
                                        cargoManifest.setShipperName(removeShipper);
                                    } else if (data.getShowShipperName() != null && data.getShowShipperName().equals(YesNo.No) && (data.getDownloadOption().equals(ReportDownloadOption.Preview) || data.getDownloadOption().equals(ReportDownloadOption.Print))) {
                                        cargoManifest.setShipperName("");
                                    } else {
                                        cargoManifest.setShipperName(removeShipper);
                                    }
                                }
                                if (document.getConsignee() != null) {
                                    String consignee = document.getConsignee().getPartyName() != null ? document.getConsignee().getPartyName() : "";
                                    log.info("ConsigneeName:::::::" + consignee);
                                    String removeConsignee = reportUtil.removeSpecialCharacters(consignee, data.getDownloadOption());
                                    if (data.getShowShipperName() != null && data.getShowShipperName().equals(YesNo.Yes) && (data.getDownloadOption().equals(ReportDownloadOption.Preview) || data.getDownloadOption().equals(ReportDownloadOption.Print))) {
                                        log.info("From ShipperYesNo Value::::::" + data.getShowShipperName());
                                        cargoManifest.setConsigneeName(removeConsignee);
                                    } else if (data.getShowShipperName() != null && data.getShowShipperName().equals(YesNo.No) && (data.getDownloadOption().equals(ReportDownloadOption.Preview) || data.getDownloadOption().equals(ReportDownloadOption.Print))) {
                                        log.info("From ShipperYesNo Value::::::" + data.getShowShipperName());
                                        cargoManifest.setConsigneeName("");
                                    } else {
                                        cargoManifest.setConsigneeName(removeConsignee);
                                    }
                                }

                                if (document.getShipperAddress() != null) {
                                    if (data.getShowShipperAddress() != null && data.getShowShipperAddress().equals(YesNo.Yes) && (data.getDownloadOption().equals(ReportDownloadOption.Preview) || data.getDownloadOption().equals(ReportDownloadOption.Print))) {
                                        cargoManifest.setShipperDetails(document.getShipperAddress().getAddressLine1()
                                                + "\n" + document.getShipperAddress().getAddressLine2() + "\n"
                                                + shipperAddressLine3 + "\n" + shipperAddressLine4);
                                    } else if (data.getShowShipperAddress() != null
                                            && data.getShowShipperAddress().equals(YesNo.No) && (data.getDownloadOption().equals(ReportDownloadOption.Preview) || data.getDownloadOption().equals(ReportDownloadOption.Print))) {
                                        cargoManifest.setShipperDetails("");
                                    } else {
                                        cargoManifest.setShipperDetails(document.getShipperAddress().getAddressLine1()
                                                + "\n" + document.getShipperAddress().getAddressLine2() + "\n"
                                                + shipperAddressLine3 + "\n" + shipperAddressLine4);
                                    }

                                }

                                if (document.getConsigneeAddress() != null
                                        && document.getConsigneeAddress().getAddressLine3() != null) {
                                    consigneeAddressLine3 = document.getConsigneeAddress().getAddressLine3();
                                } else {
                                    consigneeAddressLine3 = "";
                                }
                                if (document.getConsigneeAddress() != null
                                        && document.getConsigneeAddress().getAddressLine4() != null) {
                                    consigneeAddressLine4 = document.getConsigneeAddress().getAddressLine4();
                                } else {
                                    consigneeAddressLine4 = "";
                                }


                                if (document.getConsigneeAddress() != null) {
                                    if (data.getShowShipperAddress() != null && data.getShowShipperAddress().equals(YesNo.Yes) && (data.getDownloadOption().equals(ReportDownloadOption.Preview) || data.getDownloadOption().equals(ReportDownloadOption.Print))) {
                                        cargoManifest.setConsigneeDetails((document.getConsigneeAddress().getAddressLine1() + "\n"
                                                + document.getConsigneeAddress().getAddressLine2() + "\n"
                                                + consigneeAddressLine3 + "\n" + consigneeAddressLine4));
                                    } else if (data.getShowShipperAddress() != null && data.getShowShipperAddress().equals(YesNo.No) && (data.getDownloadOption().equals(ReportDownloadOption.Preview) || data.getDownloadOption().equals(ReportDownloadOption.Print))) {
                                        cargoManifest.setConsigneeDetails("");
                                    } else {
                                        cargoManifest.setConsigneeDetails((document.getConsigneeAddress().getAddressLine1() + "\n"
                                                + document.getConsigneeAddress().getAddressLine2() + "\n"
                                                + consigneeAddressLine3 + "\n" + consigneeAddressLine4));
                                    }
                                }


                                if (consolEntity.getKnownShipper() != null) {
                                    if (consolEntity.getKnownShipper().equals(YesNo.Yes)) {
                                        cargoManifest.setAsAgreed("AS AGREED");
                                    } else if (consolEntity.getKnownShipper().equals(YesNo.No)) {
                                        cargoManifest.setAsAgreed("NO AGREED");
                                    }
                                } else {
                                    cargoManifest.setAsAgreed("");
                                }
                                if (document.getCommodity() != null && document.getCommodity().getDescription() != null) {
                                    cargoManifest.setCommodityDescription(document.getCommodity().getDescription());
                                }
                                if (service != null) {

                                    cargoManifest.setAesNumber(service.getAesNo());
                                }

                            }
                            cargoManifestList.add(cargoManifest);
                        }
                        if (totalGrossWeight != null) {
                            parameters.put("totalGrossWeight", AppUtil.getWeightFormat(totalGrossWeight, service.getCountry().getLocale()));
                        }

                        if (totalchargeWeight != null) {
                            if (data.getShowChargeWeight() != null && data.getShowChargeWeight().equals(YesNo.Yes) && (data.getDownloadOption().equals(ReportDownloadOption.Preview) || data.getDownloadOption().equals(ReportDownloadOption.Print))) {
                                parameters.put("totalchargeWeight", AppUtil.getWeightFormat(totalchargeWeight, service.getCountry().getLocale()));
                            } else if (data.getShowChargeWeight() != null && data.getShowChargeWeight().equals(YesNo.No) && (data.getDownloadOption().equals(ReportDownloadOption.Preview) || data.getDownloadOption().equals(ReportDownloadOption.Print))) {
                                parameters.put("totalchargeWeight", "");
                            } else {
                                parameters.put("totalchargeWeight", AppUtil.getWeightFormat(totalchargeWeight, service.getCountry().getLocale()));
                            }
                        }


                        if (totalVoluemWeight != null) {
                            if (data.getShowVolume() != null && data.getShowVolume().equals(YesNo.Yes) && (data.getDownloadOption().equals(ReportDownloadOption.Preview) || data.getDownloadOption().equals(ReportDownloadOption.Print))) {
                                parameters.put("totalVoluemFreight", AppUtil.getWeightFormat(totalVoluemWeight, service.getCountry().getLocale()));
                            } else if (data.getShowVolume() != null && data.getShowVolume().equals(YesNo.No) && (data.getDownloadOption().equals(ReportDownloadOption.Preview) || data.getDownloadOption().equals(ReportDownloadOption.Print))) {
                                parameters.put("totalVoluemFreight", "");
                            } else {
                                parameters.put("totalVoluemFreight", AppUtil.getWeightFormat(totalVoluemWeight, service.getCountry().getLocale()));
                            }
                        }

                        parameters.put("totalPieces", AppUtil.getNumberFormat(totalPieces, service.getCountry().getLocale()));
                    }
                }

                parameters.put("cargoManifestList", cargoManifestList);
                if (loggedInUser != null && loggedInUser.getSelectedUserLocation() != null
                        && consolEntity.getSystemTrack() != null
                        && consolEntity.getSystemTrack().getCreateDate() != null) {
                    String dateAndEtd = "";
                    dateAndEtd = reportUtil.dateToReportDate(consolEntity.getEtd(),
                            loggedInUser.getSelectedUserLocation().getJavaDateFormat());
                    parameters.put("refDate", reportUtil.dateToReportDate(new Date(),
                            loggedInUser.getSelectedUserLocation().getJavaDateFormat()));
                    parameters.put("etd", dateAndEtd);
                    parameters.put("date", reportUtil.dateToReportDate(new Date(),
                            loggedInUser.getSelectedUserLocation().getJavaDateFormat()));
                    parameters.put("flightDate", dateAndEtd);

                    String loggedInUserName = "";
                    String loggedInLocation = "";
                    String companyName = "";
                    List<String> addressStringList = new ArrayList<>();
                    if (loggedInUser != null) {
                        if (loggedInUser.getEmployee() != null && loggedInUser.getEmployee().getEmployeeName() != null) {
                            loggedInUserName = loggedInUser.getEmployee().getEmployeeName();
                        } else if (loggedInUser.getUserName() != null) {
                            loggedInUserName = loggedInUser.getUserName();
                        } else {
                            loggedInUserName = "";
                        }
                        if (loggedInUser.getSelectedUserLocation() != null) {
                            LocationMaster locationMaster = locationMasterRepository.findById(loggedInUser.getSelectedUserLocation().getId());

                            if (locationMaster != null) {
                                addressStringList.add(locationMaster.getAddressLine1() != null ? locationMaster.getAddressLine1() : "");
                                addressStringList.add(locationMaster.getAddressLine2() != null ? locationMaster.getAddressLine2() : "");
                                addressStringList.add(locationMaster.getAddressLine3() != null ? locationMaster.getAddressLine3() : "");
                                companyName = locationMaster.getBranchName() != null ? locationMaster.getBranchName() : "";
                            }
                            loggedInLocation = loggedInUser.getSelectedUserLocation().getLocationName() != null
                                    ? loggedInUser.getSelectedUserLocation().getLocationName() : "";
                            LogoMaster logoMaster = logoMasterRepository.findByLocationMaster(loggedInUser.getSelectedUserLocation());
                            try {
                                if (logoMaster != null && logoMaster.getLogo() != null) {
                                    InputStream in = new ByteArrayInputStream(logoMaster.getLogo());
                                    BufferedImage bImageFromConvert = ImageIO.read(in);
                                    if (data.getShowLogo() != null && data.getShowLogo().equals(YesNo.Yes) && (data.getDownloadOption().equals(ReportDownloadOption.Preview) || data.getDownloadOption().equals(ReportDownloadOption.Print))) {
                                        parameters.put("logo", bImageFromConvert);
                                    } else if (data.getShowLogo() != null && data.getShowLogo().equals(YesNo.No) && (data.getDownloadOption().equals(ReportDownloadOption.Preview) || data.getDownloadOption().equals(ReportDownloadOption.Print))) {
                                        //parameters.put("logo","");
                                    } else {
                                        parameters.put("logo", bImageFromConvert);
                                    }
                                    log.info("Image getting success::");
                                }
                            } catch (Exception ex) {
                                log.error("Image getting failed:::", ex);
                            }

                        } else {
                            loggedInLocation = "";
                        }
                    }

                    parameters.put("refPlace", loggedInUserName + "-" + loggedInLocation);
                    parameters.put("companyName", reportUtil.removeSpecialCharacters(companyName, data.getDownloadOption()));
                    int noOfAddrLine = 0;
                    for (String strAddr : addressStringList) {
                        noOfAddrLine++;
                        parameters.put("companyAddressLine" + noOfAddrLine, strAddr != null ? strAddr : "");
                    }
                    if (noOfAddrLine < 3) {
                        for (int i = noOfAddrLine; i < 3; i++) {
                            noOfAddrLine++;
                            parameters.put("companyAddressLine" + noOfAddrLine, "");
                        }
                    }
                    parameters.put("reportName", "CARGO MANIFEST (AIR EXPORT)");

                    List<FlightDetail> cargoManifestConnectionList = new ArrayList<>();
                    log.info("ConsolConnectionList  Before Object Size:" + consolEntity.getConnectionList().size());
                    if (consolEntity.getConnectionList() != null && !consolEntity.getConnectionList().isEmpty()) {
                        log.info("ConsolConnectionList  Afert Object Size:" + consolEntity.getConnectionList().size());
                        for (ConsolConnection consolConnection : consolEntity.getConnectionList()) {
                            FlightDetail flightDetail = new FlightDetail();
                            if (consolConnection != null) {
                                if (consolConnection.getPod() != null) {
                                    flightDetail.setDestinationName(consolConnection.getPod().getPortName());
                                }
                                if (consolConnection.getEtd() != null) {
                                    flightDetail.setEtdDate(reportUtil.dateToReportDate(consolConnection.getEtd(), loggedInUser.getSelectedUserLocation().getJavaDateFormat()));
                                }
                                if (consolConnection.getCarrierMaster() != null) {
                                    flightDetail.setCarrierName(consolConnection.getCarrierMaster().getCarrierName());
                                }
                                flightDetail.setFlightNo(consolConnection.getFlightVoyageNo());
                            }
                            cargoManifestConnectionList.add(flightDetail);
                        }
                    }
                    parameters.put("cargoManifestConnectionList", cargoManifestConnectionList);

                }


                Double currencyTotal = 0.0;
                List<CargoManifest> cargoManifestChargeList = new ArrayList<>();
                if (consolEntity.getChargeList() != null && !consolEntity.getChargeList().isEmpty()) {
                    log.info("ConsolChargeList  Before Object Size:" + consolEntity.getChargeList().size());
                    for (ConsolCharge consolCharge : consolEntity.getChargeList()) {
                        CargoManifest charge = new CargoManifest();
                        if (consolCharge != null) {
                            if (consolCharge.getChargeMaster() != null) {
                                charge.setChargeName(consolCharge.getChargeMaster().getChargeName());
                            }
                            if (consolCharge.getCurrency() != null) {
                                log.info("get consol CurrencyCode=====" + consolCharge.getCurrency().getCurrencyCode());
                                charge.setCurrencyCode(consolCharge.getCurrency().getCurrencyCode());
                            }

                            if (consolCharge.getPpcc() != null) {
                                if (consolCharge.getPpcc().equals(consolEntity.getPpcc())) {
                                    if (consolCharge.getAmount() != null) {
                                        log.info("get consolcharge Collect Amount=====" + consolCharge.getAmount());
                                        currencyTotal = currencyTotal + consolCharge.getAmount();
                                        if (consolCharge.getCurrency() != null && consolCharge.getCurrency().getCountryMaster() != null) {
                                            charge.setCurrencyAmount(AppUtil.getCurrencyFormat(consolCharge.getAmount(), consolCharge.getCurrency().getCountryMaster().getLocale(), consolCharge.getCurrency().getDecimalPoint()));
                                        }
                                    }
                                    cargoManifestChargeList.add(charge);

                                }
                            }
                        }
                    }
                }


                if (data.getShowCharge() != null && data.getShowCharge().equals(YesNo.Yes) && (data.getDownloadOption().equals(ReportDownloadOption.Preview) || data.getDownloadOption().equals(ReportDownloadOption.Print))) {
                    parameters.put("cargoManifestChargeList", cargoManifestChargeList);
                    if (consolEntity.getCountry() != null && consolEntity.getCurrency() != null) {
                        parameters.put("currencyTotal", AppUtil.getCurrencyFormat(currencyTotal, consolEntity.getCountry().getLocale(), consolEntity.getCurrency().getDecimalPoint()));
                    }
                } else {
                    parameters.put("cargoManifestChargeList", null);
                    parameters.put("currencyTotal", "");
                }
                JasperReport jasperReport = JasperCompileManager.compileReport(appResourcePath + "/efs-report/cargo_manifest_air_export.jrxml");
                List<Object> fieldCollection1 = new ArrayList<>();
                Object obj1 = new Object();
                fieldCollection1.add(obj1);
                JRDataSource dataSource = new JRBeanCollectionDataSource(fieldCollection1, false);
                JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, dataSource);
                return jasperPrint;

            } catch (RestException re) {
                log.error("RestException occures in Cargo Manifest method  : ", re);
            } catch (Exception exception) {
                log.error("RestException occures in Cargo Manifest :: ", exception);
                baseDto.setResponseCode(ErrorCode.FAILED);
            }
            log.info("Cargo Manifest   [" + data.getResourceId() + "] method ended");
        }
        return null;
    }

    public Map<String, String> AddressMasterToMap(String parameterName, AddressMaster addressMaster) {
        Map<String, String> parameter = new HashMap<>();
        int index = 1;
        if (addressMaster != null) {
            if (addressMaster.getAddressLine1() != null && addressMaster.getAddressLine1().trim().length() > 0) {
                parameter.put(parameterName + index, addressMaster.getAddressLine1());
                index++;
            }
            if (addressMaster.getAddressLine2() != null && addressMaster.getAddressLine2().trim().length() > 0) {
                parameter.put(parameterName + index, addressMaster.getAddressLine2());
                index++;
            }
            if (addressMaster.getAddressLine3() != null && addressMaster.getAddressLine3().trim().length() > 0) {
                parameter.put(parameterName + index, addressMaster.getAddressLine3());
                index++;
            }
            if (addressMaster.getAddressLine4() != null && addressMaster.getAddressLine4().trim().length() > 0) {
                parameter.put(parameterName + index, addressMaster.getAddressLine4());
                index++;
            }
        }
        return parameter;
    }
}
