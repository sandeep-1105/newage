package com.efreightsuite.report.service;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletResponse;

import com.efreightsuite.dto.AutoAttachmentDto;
import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.ReportDownloadRequestDto;
import com.efreightsuite.enumeration.ReportDownloadOption;
import com.efreightsuite.enumeration.ReportName;
import com.efreightsuite.enumeration.YesNo;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.model.CfsReceiveEntry;
import com.efreightsuite.model.Consol;
import com.efreightsuite.model.DocumentDetail;
import com.efreightsuite.model.LocationMaster;
import com.efreightsuite.model.LogoMaster;
import com.efreightsuite.model.ShipmentLink;
import com.efreightsuite.model.ShipmentServiceDetail;
import com.efreightsuite.model.UserProfile;
import com.efreightsuite.report.CargoManifest;
import com.efreightsuite.service.AuthService;
import com.efreightsuite.util.AppUtil;
import lombok.extern.log4j.Log4j2;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

import org.springframework.stereotype.Service;

@Service
@Log4j2
public class AirCargoLoadPlanReportService extends AbstractReportService {
    public ReportName getReportType() {
        return ReportName.AIR_CARGO_LOAD_PLAN;
    }

    public byte[] processReport(ReportDownloadRequestDto data, HttpServletResponse response, boolean isMultliple) {
        log.info("I am in AirCargoLoadPlanReportService..");
        BaseDto baseDto = new BaseDto();
        UserProfile logInUSer = AuthService.getCurrentUser();
        JasperPrint jasperPrint = getReport(baseDto, data, AuthService.getCurrentUser());

        byte[] a = null;
        AutoAttachmentDto autoAttachmentDto = reportUtil.autoAttachment(ReportName.AIR_CARGO_LOAD_PLAN, YesNo.Yes, data.getResourceId(), "MasterShipment");
        if (data.getSingle() != null && data.getSingle() == true) {
            if (data.getDownloadOption() == ReportDownloadOption.Preview || data.getDownloadOption() == ReportDownloadOption.Print) {
                reportUtil.japerPrintToResponseOutputStream(data.getDownloadFileType(), jasperPrint, "AIR_CARGO_LOAD_PLAN", response, false, null);
            }
            if (autoAttachmentDto != null && autoAttachmentDto.getIsAttach() != null || data.getDownloadOption() == ReportDownloadOption.Download) {
                a = reportUtil.japerPrintToResponseOutputStream(data.getDownloadFileType(), jasperPrint, "AIR_CARGO_LOAD_PLAN", response, false, autoAttachmentDto);
            }
        }
        if (data.getSingle() != null && data.getSingle() == true && data.getDownloadOption() == ReportDownloadOption.eMail) {
            a = reportUtil.japerPrintToResponseOutputStream(data.getDownloadFileType(), jasperPrint, "AIR_CARGO_LOAD_PLAN", response, false, null);
            consolReportMailer.sendConsolMailToCustomer(data.getResourceId(), logInUSer, ReportName.AIR_CARGO_LOAD_PLAN, a);
        }
        if (isMultliple == true && data.getDownloadOption() == ReportDownloadOption.eMail) {
            a = reportUtil.japerPrintToResponseOutputStream(data.getDownloadFileType(), jasperPrint, "AIR_CARGO_LOAD_PLAN", response, true, null);
            return a;
        }
        return null;
    }

    private JasperPrint getReport(BaseDto baseDto, ReportDownloadRequestDto data, UserProfile loggedInUser) {
        log.info("Air Cargo Load Plan  Report with Id::: [" + data.getResourceId() + "] called");
        Consol consol = consolRepository.findById(data.getResourceId());
        log.info("consolID::::::::::" + consol.getId());
        if (consol != null) {
            Map<String, Object> parameters = new HashMap<>();
            try {
                Double totalGrossWeight = 0.0;
                Double totalVolumeWeight = 0.0;
                Long totalPieces = 0L;
                List<CargoManifest> shipmentLinkList = new ArrayList<>();
                String filghtNo = "";
                Date date = null;

                parameters.put("carrierName", consol.getCarrier().getCarrierName() != null ? consol.getCarrier().getCarrierName() : "");
                if (consol.getConsolDocument() != null) {
                    parameters.put("mawbNumber", consol.getConsolDocument().getMawbNo());
                    filghtNo = consol.getConsolDocument().getRouteNo() != null ? consol.getConsolDocument().getRouteNo() : "";
                    parameters.put("destination", consol.getPod().getPortName() != null ? consol.getPod().getPortName() : "");
                    if (consol.getEtd() != null) {
                        date = consol.getEtd();
                    }
                }

                if (consol.getShipmentLinkList() != null && !consol.getShipmentLinkList().isEmpty()) {
                    for (ShipmentLink shipmentLink : consol.getShipmentLinkList()) {
                        CargoManifest airCargo = new CargoManifest();
                        if (shipmentLink != null) {
                            ShipmentServiceDetail service = shipmentLink.getService();
                            if (service != null && service.getDocumentList() != null && !service.getDocumentList().isEmpty()) {
                                for (DocumentDetail document : service.getDocumentList()) {
                                    if (document != null) {
                                        airCargo.setHawbNumber(document.getHawbNo() != null ? document.getHawbNo() : "");
                                        if (document.getShipper() != null && document.getShipper().getPartyName() != null) {
                                            airCargo.setShipperName(reportUtil.removeSpecialCharacters(document.getShipper().getPartyName(), data.getDownloadOption()));
                                        }
                                    }
                                }
                                if (service.getPackMaster() != null) {
                                    airCargo.setPacks(service.getPackMaster().getPackName() != null ? service.getPackMaster().getPackName() : "");
                                }
                                airCargo.setProNo(service.getProTrackingNo() != null ? service.getProTrackingNo() : "");
                                if (service.getBookedPieces() != null) {
                                    Long piecs = 0L;
                                    piecs = service.getBookedPieces();
                                    totalPieces = totalPieces + piecs;
                                    airCargo.setNoOfPieces(AppUtil.getNumberFormat(service.getBookedPieces(), service.getCountry().getLocale()));
                                }
                                if (service.getBookedVolumeWeightUnitKg() != null) {
                                    Double volume = 0.0;
                                    volume = service.getBookedVolumeWeightUnitKg();
                                    totalVolumeWeight = totalVolumeWeight + volume;
                                    airCargo.setVolumeWeight(AppUtil.getWeightFormat(service.getBookedVolumeWeightUnitKg(), service.getCountry().getLocale()));

                                }
                                if (service.getBookedGrossWeightUnitKg() != null) {
                                    Double gross = 0.0;
                                    gross = service.getBookedGrossWeightUnitKg();
                                    totalGrossWeight = totalGrossWeight + gross;
                                    airCargo.setGrossWeight(AppUtil.getWeightFormat(service.getBookedGrossWeightUnitKg(), service.getCountry().getLocale()));

                                }
                                if (service.getHazardous() != null) {
                                    if (service.getHazardous().equals(YesNo.Yes)) {
                                        airCargo.setHaz("Yes");
                                    } else {
                                        airCargo.setHaz("No");
                                    }
                                }


                            }
                            if (service != null) {
                                List<CfsReceiveEntry> cfs = cfsReceiveEntryRepository.findByShipmentServiceUID(service.getServiceUid());
                                if (cfs != null && cfs.size() > 0 && cfs.get(0) != null) {
                                    if (cfs.get(0).getOnHand() != null) {
                                        airCargo.setOnHandNo(cfs.get(0).getOnHand());
                                    }
                                    if (cfs.get(0).getWarehouseLocation() != null) {
                                        airCargo.setWarehouseLocation(cfs.get(0).getWarehouseLocation());
                                    }
                                    if (cfs.get(0).getNotes() != null) {
                                        airCargo.setWarehouseRemarks(cfs.get(0).getNotes());
                                    }
                                }

						 
						/*
						 if(cfs!=null && !cfs.getCfsDimensionList().isEmpty()){
							 for (CfsDimension dimensionUnit : cfs.getCfsDimensionList()) {
								 
								 if(dimensionUnit!=null){
										if(dimensionUnit.getGrossWeight() != null) {
											Double gross=0.0;
											gross=dimensionUnit.getGrossWeight();
											totalGrossWeight= totalGrossWeight + gross;
											airCargo.setGrossWeight(AppUtil.getWeightFormat(gross, service.getCountry().getLocale()));
										}
										if(dimensionUnit.getVolWeight() != null) {
											Double volume=0.0;
											volume=dimensionUnit.getVolWeight();
											totalVolumeWeight = totalVolumeWeight + volume;
											airCargo.setVolumeWeight(AppUtil.getWeightFormat(volume, service.getCountry().getLocale()));
										}
										if(dimensionUnit.getNoOfPiece()!=null){
											Integer piecs=0;
											piecs=dimensionUnit.getNoOfPiece();
											totalPieces = totalPieces + piecs;
											airCargo.setNoOfPieces(AppUtil.getNumberFormat(piecs, service.getCountry().getLocale()));
										}
								 }
								 if(cfs.getShipper()!=null && cfs.getShipper().getPartyName()!=null){
									 airCargo.setShipperName(reportUtil.removeSpecialCharacters(cfs.getShipper().getPartyName(), data.getDownloadOption())); 
								 }
								 if(cfs.getProNumber()!=null){
									 log.info("CFS  pro Number::::;;;;"+cfs.getProNumber());
									 airCargo.setProNo(cfs.getProNumber());
								 }
								 
								 airCargo.setHawbNumber(hawbNumber);
								 airCargo.setHaz("");
								 airCargo.setPacks("");	
								 airCargo.setWarehouseRemarks("");
								 shipmentLinkList.add(airCargo);
				         	}						
							 if(cfs!=null){
								 parameters.put("note", cfs.getNotes()!=null ? cfs.getNotes():"");
						      }
						}*/

                            }

                            if (totalGrossWeight != null && totalGrossWeight > 0.0) {
                                parameters.put("totalGrossWeight", AppUtil.getWeightFormat(totalGrossWeight, service.getCountry().getLocale()));
                            }
                            if (totalPieces != null && totalPieces > 0) {
                                parameters.put("totalPieces", AppUtil.getNumberFormat(totalPieces, service.getCountry().getLocale()));
                            }
                            if (totalVolumeWeight != null && totalVolumeWeight > 0.0) {
                                parameters.put("totalVolumeWeight", AppUtil.getWeightFormat(totalVolumeWeight, service.getCountry().getLocale()));
                            }
                        }

                        shipmentLinkList.add(airCargo);
                    }
                    parameters.put("shipmentLinkList", shipmentLinkList);

                    String loggedInUserName = "";
                    String loggedInLocation = "";
                    String companyName = "";
                    if (loggedInUser != null) {
                        if (loggedInUser.getEmployee() != null && loggedInUser.getEmployee().getEmployeeName() != null) {
                            loggedInUserName = loggedInUser.getEmployee().getEmployeeName();
                        } else if (loggedInUser.getUserName() != null) {
                            loggedInUserName = loggedInUser.getUserName();
                        } else {
                            loggedInUserName = "";
                        }
                        if (loggedInUser.getSelectedUserLocation() != null) {
                            String todayDate = "";
                            todayDate = reportUtil.dateToReportDate(new Date(), loggedInUser.getSelectedUserLocation().getJavaDateFormat());
                            parameters.put("refDate", todayDate);
                            String flightDate = reportUtil.dateToReportDate(date, loggedInUser.getSelectedUserLocation().getJavaDateFormat());
                            parameters.put("flightNoAndDate", filghtNo + " / " + flightDate);

                            loggedInLocation = loggedInUser.getSelectedUserLocation().getLocationName() != null
                                    ? loggedInUser.getSelectedUserLocation().getLocationName() : "";

                            LocationMaster locationMaster = locationMasterRepository.findById(loggedInUser.getSelectedUserLocation().getId());
                            if (locationMaster != null) {
                                parameters.put("companyAddressLine1", locationMaster.getAddressLine1() != null ? locationMaster.getAddressLine1() : "");
                                parameters.put("companyAddressLine2", locationMaster.getAddressLine2() != null ? locationMaster.getAddressLine2() : "");
                                parameters.put("companyAddressLine3", locationMaster.getAddressLine3() != null ? locationMaster.getAddressLine3() : "");
                                companyName = locationMaster.getBranchName() != null ? locationMaster.getBranchName() : "";

                            }
                            LogoMaster logoMaster = logoMasterRepository.findByLocationMaster(loggedInUser.getSelectedUserLocation());
                            try {
                                if (logoMaster != null && logoMaster.getLogo() != null) {
                                    InputStream in = new ByteArrayInputStream(logoMaster.getLogo());
                                    BufferedImage bImageFromConvert = ImageIO.read(in);
                                    parameters.put("logo", bImageFromConvert);
                                    log.info("Image getting success::");
                                }
                            } catch (Exception ex) {
                                log.error("Image getting failed:::", ex);
                            }

                        } else {
                            loggedInLocation = "";
                        }
                    }
                    parameters.put("companyName", reportUtil.removeSpecialCharacters(companyName, data.getDownloadOption()));
                    parameters.put("refPlace", loggedInUserName + "-" + loggedInLocation);

                }

                parameters.put("reportName", "AIR CARGO LOAD PLAN");
                JasperReport jasperReport = JasperCompileManager.compileReport(appResourcePath + "/efs-report/air_cargo_load_plan.jrxml");
                List<Object> fieldCollection1 = new ArrayList<>();
                Object obj1 = new Object();
                fieldCollection1.add(obj1);

                JRDataSource dataSource = new JRBeanCollectionDataSource(fieldCollection1, false);
                JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, dataSource);
                return jasperPrint;

            } catch (RestException re) {
                log.error("RestException in Air Cargo Load Plan : ", re);
            } catch (Exception exception) {
                log.error("RestException in Air Cargo Load Plan:: ", exception);
                baseDto.setResponseCode(ErrorCode.FAILED);
            }
            log.info("Air Cargo Load Plan  [" + data.getResourceId() + "] function ended");
        }
        return null;
    }

}
