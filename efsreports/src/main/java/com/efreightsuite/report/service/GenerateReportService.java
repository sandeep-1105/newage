package com.efreightsuite.report.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletResponse;

import com.efreightsuite.configuration.TenantService;
import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.ReportDownloadRequestDto;
import com.efreightsuite.dto.SaaSDto;
import com.efreightsuite.enumeration.ReportDownloadOption;
import com.efreightsuite.enumeration.ReportName;
import com.efreightsuite.model.UserProfile;
import com.efreightsuite.service.AuthService;
import com.efreightsuite.service.mailer.ConsolMultipleReportMailer;
import com.efreightsuite.service.mailer.InvoiceMultipleReportMailer;
import com.efreightsuite.service.mailer.QuotationMailer;
import com.efreightsuite.service.mailer.ShipmentMultipleReportMailer;
import com.efreightsuite.util.ReportUtil;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class GenerateReportService {

    @Autowired
    TenantService tenantService;

    @Autowired
    List<AbstractReportService> abstractReportServiceList;

    @Autowired
    QuotationMailer quotationMailer;

    @Autowired
    ShipmentMultipleReportMailer shipmentMultipleReportMailer;

    @Autowired
    ConsolMultipleReportMailer consolMultipleReportMailer;

    @Autowired
    InvoiceMultipleReportMailer invoiceCreditNoteMultipleReportMailer;

    @Autowired
    ReportUtil reportUtil;

    @Value("${efs.resource.path}")
    String appResourcePath;

    Map<ReportName, AbstractReportService> abstractReportServiceMap = new HashMap<>();

    @PostConstruct
    public void init() {
        for (AbstractReportService ars : abstractReportServiceList) {
            abstractReportServiceMap.put(ars.getReportType(), ars);
        }
    }

    public BaseDto saasAdded(SaaSDto saasDto) {
        return tenantService.loadNewDataSource(saasDto.getSaasId(), saasDto.getCompanyName(), saasDto.getDriverName(), saasDto.getUrl(), saasDto.getUserName(), saasDto.getPassword());
    }

    public void process(ReportDownloadRequestDto data, HttpServletResponse response) {
        log.info("GenericReportService Class -> process Mtd ");
        reportUtil.logObjectToJSON(data);
        abstractReportServiceMap.get(data.getReportName()).processReport(data, response, false);
    }

    /*@Async
    @Transactional(propagation=Propagation.REQUIRES_NEW)*/
    public void processMultipleReport(ReportDownloadRequestDto data) {
        log.info("GenericReportService Class -> process multiple reports ");
        byte[] array = null;
        UserProfile userProfile = AuthService.getCurrentUser();
        Map<String, byte[]> report = new HashMap<>();
        if (data.getDownloadOption() != null && data.getDownloadOption().equals(ReportDownloadOption.eMail)) {
            if (data.getEmailReportList() != null && data.getEmailReportList().size() > 0) {
                for (int i = 0; i < data.getEmailReportList().size(); i++) {
                    ReportDownloadRequestDto rd = new ReportDownloadRequestDto();
                    rd.setResourceId(data.getEmailReportList().get(i).getResourceId());
                    rd.setReportName(data.getEmailReportList().get(i).getReportName());
                    array = abstractReportServiceMap.get(data.getEmailReportList().get(i).getReportName()).processReport(data, null, data.getIsMultiple());
                    if (array != null) {
                        report.put(data.getEmailReportList().get(i).getReportName().name(), array);
                    }
                }

                if (report != null && report.size() > 0) {
                    log.info("Report values after calling reportutil=====" + report);
                    if (data.getScreenType() != null && data.getScreenType().equals("Quotation") && (data.getDownloadOption() != null && data.getDownloadOption().equals(ReportDownloadOption.eMail))) {
                        quotationMailer.sendAllReportToCustomer(data.getResourceId(), userProfile, report);
                    } else if (data.getScreenType() != null && data.getScreenType().equals("Shipment") && (data.getDownloadOption() != null && data.getDownloadOption().equals(ReportDownloadOption.eMail))) {
                        shipmentMultipleReportMailer.sendMailToCustomer(data.getResourceId(), userProfile, report);
                    } else if (data.getScreenType() != null && data.getScreenType().equals("Consol") && (data.getDownloadOption() != null && data.getDownloadOption().equals(ReportDownloadOption.eMail))) {
                        consolMultipleReportMailer.sendMailToCustomer(data.getResourceId(), userProfile, report);
                    } else if (data.getScreenType() != null && data.getScreenType().equals("Invoice") && (data.getDownloadOption() != null && data.getDownloadOption().equals(ReportDownloadOption.eMail))) {
                        invoiceCreditNoteMultipleReportMailer.sendAllReportsToCustomer(data.getResourceId(), userProfile, report);

                    }
                }

            }

        }
    }
}
