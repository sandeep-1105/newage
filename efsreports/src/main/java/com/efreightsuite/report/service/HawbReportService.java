package com.efreightsuite.report.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import com.efreightsuite.dto.AutoAttachmentDto;
import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.ReportDownloadRequestDto;
import com.efreightsuite.dto.ReportGenericDto;
import com.efreightsuite.enumeration.DUE;
import com.efreightsuite.enumeration.DimensionUnit;
import com.efreightsuite.enumeration.PPCC;
import com.efreightsuite.enumeration.ReportConfig;
import com.efreightsuite.enumeration.ReportDownloadOption;
import com.efreightsuite.enumeration.ReportName;
import com.efreightsuite.enumeration.YesNo;
import com.efreightsuite.model.AddressMaster;
import com.efreightsuite.model.CarrierMaster;
import com.efreightsuite.model.CountryReportConfigMaster;
import com.efreightsuite.model.DocumentDetail;
import com.efreightsuite.model.PageMaster;
import com.efreightsuite.model.PartyMaster;
import com.efreightsuite.model.PortMaster;
import com.efreightsuite.model.ReportConfigurationMaster;
import com.efreightsuite.model.ServiceConnection;
import com.efreightsuite.model.ServiceDocumentDimension;
import com.efreightsuite.model.ShipmentCharge;
import com.efreightsuite.model.ShipmentServiceDetail;
import com.efreightsuite.model.UserProfile;
import com.efreightsuite.report.HawbReportRider;
import com.efreightsuite.report.PrepaidCharges;
import com.efreightsuite.report.ReportDocumentDimensionDto;
import com.efreightsuite.service.AuthService;
import com.efreightsuite.util.AppUtil;
import lombok.extern.log4j.Log4j2;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRPrintPage;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

import org.springframework.stereotype.Service;

@Service
@Log4j2
public class HawbReportService extends AbstractReportService {
    public ReportName getReportType() {
        return ReportName.HAWB;
    }

    public byte[] processReport(ReportDownloadRequestDto requestData, HttpServletResponse response, boolean isMultiple) {
        log.info("I am in HawbReportService.....");
        BaseDto baseDto = new BaseDto();
        UserProfile logInUSer = AuthService.getCurrentUser();
        JasperPrint jasperPrint = getReport(baseDto, requestData, AuthService.getCurrentUser());
        log.error("jasperPrint Data In HAWB :: ", jasperPrint);
        byte[] a = null;
        AutoAttachmentDto autoAttachmentDto = reportUtil.autoAttachment(ReportName.HAWB, YesNo.Yes, requestData.getResourceId(), "Shipment");
        if (requestData.getSingle() != null && requestData.getSingle() == true) {
            if (requestData.getDownloadOption() == ReportDownloadOption.Preview) {
                reportUtil.japerPrintToResponseOutputStream(requestData.getDownloadFileType(), jasperPrint, "HAWB", response, false, autoAttachmentDto);
            } else if (autoAttachmentDto != null && autoAttachmentDto.getIsAttach() != null || requestData.getDownloadOption() == ReportDownloadOption.Download) {
                reportUtil.japerPrintToResponseOutputStream(requestData.getDownloadFileType(), jasperPrint, "HAWB", response, false, autoAttachmentDto);
            } else if (requestData.getDownloadOption() == ReportDownloadOption.Print) {
                reportUtil.japerPrintToResponseOutputStream(requestData.getDownloadFileType(), jasperPrint, "HAWB", response, false, autoAttachmentDto);
            }
        }
        if (requestData.getSingle() != null && requestData.getSingle() == true && requestData.getDownloadOption() == ReportDownloadOption.eMail) {
            a = reportUtil.japerPrintToResponseOutputStream(requestData.getDownloadFileType(), jasperPrint, "HAWB", response, false, autoAttachmentDto);
            shipmentReportMailer.sendShipmentMailToCustomer(requestData.getResourceId(), logInUSer, ReportName.HAWB, a);
        }
        if (isMultiple == true && requestData.getDownloadOption() == ReportDownloadOption.eMail) {
            a = reportUtil.japerPrintToResponseOutputStream(requestData.getDownloadFileType(), jasperPrint, "HAWB", response, true, autoAttachmentDto);
            return a;
        }
        return null;
    }

    private JasperPrint getReport(BaseDto baseDto, ReportDownloadRequestDto requestData, UserProfile loggedInUser) {
        log.info("I am in HawbReportService..");
        // Report Parameters Data Preparation
        Map<String, Object> parameters = new HashMap<>();
        Map<String, Object> parameters1 = new HashMap<>();
        String marksParam1 = "";
        String marksParam2 = "";
        int commoditylengthLine = 0;
        boolean secondPageNeeded = false;
        try {
            DocumentDetail documentDetail = documentDetailRepository.findByIdForNonMappingObjects(requestData.getResourceId());

            if (documentDetail != null) {


                {
                    ReportConfigurationMaster tmpObj = reportConfigurationMasterRepository.getByKeyAndLocation(ReportConfig.HAWB_COPIES_DECLARATION, documentDetail.getLocation().getId());
                    if (tmpObj != null && tmpObj.getReportValue() != null) {
                        parameters.put("hawbCopiesDeclaration", tmpObj.getReportValue());
                    }
                }

                {
                    ReportConfigurationMaster tmpObj = reportConfigurationMasterRepository.getByKeyAndLocation(ReportConfig.HAWB_AGREED_GOOD_ORDER_CONDITION, documentDetail.getLocation().getId());
                    if (tmpObj != null && tmpObj.getReportValue() != null) {
                        parameters.put("hawbAgreedGoodOrderCondition", tmpObj.getReportValue());
                    }
                }

                {
                    ReportConfigurationMaster tmpObj = reportConfigurationMasterRepository.getByKeyAndLocation(ReportConfig.HAWB_INSURANCE, documentDetail.getLocation().getId());
                    if (tmpObj != null && tmpObj.getReportValue() != null) {
                        parameters.put("hawbInsurance", tmpObj.getReportValue());
                    }
                }

                {
                    ReportConfigurationMaster tmpObj = reportConfigurationMasterRepository.getByKeyAndLocation(ReportConfig.HAWB_SHIPPER_CERTIFIES_CONSIGNMENT, documentDetail.getLocation().getId());
                    if (tmpObj != null && tmpObj.getReportValue() != null) {
                        parameters.put("hawbShipperCertifiesConsignment", tmpObj.getReportValue());
                    }
                }

                PartyMaster shipper = documentDetailRepository.findByIdForShipper(documentDetail.getId());
                if (shipper != null && shipper.getPartyName() != null) {
                    parameters.put("shipperName", reportUtil.removeSpecialCharacters(shipper.getPartyName(), requestData.getDownloadOption()));
                } else {
                    parameters.put("shipperName", "");
                }

                parameters.put("shipperAddressList", reportUtil.addressMasterToGenericList(documentDetailRepository.findByIdForShipperAddress(documentDetail.getId())));
                List<ReportGenericDto> reportGenericDtoList = reportUtil.addressMasterToGenericList(documentDetailRepository.findByIdForIssuingAgentAddress(documentDetail.getId()));
                if (reportGenericDtoList != null && !reportGenericDtoList.isEmpty() && reportGenericDtoList.size() > 4) {
                    ReportGenericDto line5 = reportGenericDtoList.get(4);
                    reportGenericDtoList.remove(4);
                    ReportGenericDto line4 = reportGenericDtoList.get(3);
                    reportGenericDtoList.remove(3);
                    ReportGenericDto newLine4 = new ReportGenericDto();
                    newLine4.setAddress(line4.getAddress() + " " + line5.getAddress());
                    reportGenericDtoList.add(newLine4);
                }
                parameters.put("issuingCarrierAgentAddressList", reportGenericDtoList);
                parameters.put("carrierAddressList", reportUtil.addressMasterToGenericList(documentDetailRepository.findByIdForIssuingAgentAddress(documentDetail.getId())));

                CarrierMaster carrier = documentDetailRepository.findByIdForCarrier(documentDetail.getId());
                if (carrier != null) {
                    parameters.put("carrierName", carrier.getCarrierName() != null ? carrier.getCarrierName() : "");
                } else {
                    parameters.put("carrierName", "");
                }
                PartyMaster consignee = documentDetailRepository.findByIdForConsignee(documentDetail.getId());
                if (consignee != null && consignee.getPartyName() != null) {
                    parameters.put("consigneeName", reportUtil.removeSpecialCharacters(consignee.getPartyName(), requestData.getDownloadOption()));
                } else {
                    parameters.put("consigneeName", "");
                }

                if (loggedInUser != null) {
                    if (loggedInUser.getEmployee() != null && loggedInUser.getEmployee().getEmployeeName() != null) {
                        parameters.put("loginUserName", loggedInUser.getEmployee().getEmployeeName());
                    } else if (loggedInUser.getUserName() != null) {
                        parameters.put("loginUserName", loggedInUser.getUserName());
                    } else {
                        parameters.put("loginUserName", "");
                    }
                    if (loggedInUser.getSelectedUserLocation() != null) {
                        parameters.put("issuedDate", reportUtil.dateToReportDate(new Date(), loggedInUser.getSelectedUserLocation().getJavaDateFormat()));
                    } else {
                        parameters.put("issuedDate", "");
                    }
                } else {
                    parameters.put("loginUserName", "");
                    parameters.put("issuedDate", "");
                }
                parameters.put("consigneeAddressList", reportUtil.addressMasterToGenericList(documentDetailRepository.findByIdForConsigneeAddress(documentDetail.getId())));
            /*First Notify Account INfo starts Here*/
                ShipmentServiceDetail shipmentServiceDetail = documentDetailRepository.findByIdForShipmentServiceDetail(documentDetail.getId());
                PartyMaster firstNotify = documentDetailRepository.findByIdForFirstNotify(documentDetail.getId());
                AddressMaster firstNotifyAddressMaster = documentDetailRepository.findByIdForFirstNotifyAddress(documentDetail.getId());

                ReportGenericDto acName = new ReportGenericDto();
                List<HawbReportRider> hawbRiderList = new ArrayList<>();
                HawbReportRider rider = new HawbReportRider();
                if (documentDetail != null) {
                    parameters.put("hawbNo", documentDetail.getHawbNo());
                    parameters1.put("hawbNo", documentDetail.getHawbNo());
                }

                if (documentDetail != null && documentDetail.getCommodityDescription() != null) {
                    String commodityDescription = documentDetail.getCommodityDescription();
                    commoditylengthLine = commodityDescription.length();
                    log.info("Commodity Size:::::::" + commoditylengthLine);
				/*if(commoditylengthLine > 0) {
					commoditylengthLine = commoditylengthLine /38;
					log.info("Commodity Size After :::::::"+commoditylengthLine);
				}*/
                    if (commoditylengthLine > 130) {
                        parameters.put("commodityDescription", documentDetail.getCommodityDescription().substring(0, 130));
                        rider.setCommodityDescription(documentDetail.getCommodityDescription().substring(130, commodityDescription.length()));
                        secondPageNeeded = true;
                    } else {
                        parameters.put("commodityDescription", documentDetail.getCommodityDescription());
                    }
                }
                List<ReportGenericDto> firstNotifyAddressList = new ArrayList<>();
                if (shipmentServiceDetail != null) {
                    acName = new ReportGenericDto();
                    acName.setAddress(shipmentServiceDetail.getPpcc() != null ? "** FREIGHT " + shipmentServiceDetail.getPpcc().toString().toUpperCase() + " **" : "");
                    firstNotifyAddressList.add(acName);
                    parameters.put("currencyCode", shipmentServiceDetail.getLocalCurrency() != null ? shipmentServiceDetail.getLocalCurrency().getCurrencyCode() : "");

                    if (shipmentServiceDetail.getPpcc() != null) {
                        if (shipmentServiceDetail.getPpcc().equals(PPCC.Collect)) {
                            parameters.put("prepaidOrCollectChargesFlag", "C");
                            parameters.put("collectChargesFlag", "C");
                            parameters.put("prepaidChargesFlag", "");
                        } else {
                            parameters.put("prepaidOrCollectChargesFlag", "P");
                            parameters.put("prepaidChargesFlag", "P");
                            parameters.put("collectChargesFlag", "");
                        }
                    } else {
                        parameters.put("prepaidOrCollectChargesFlag", "");
                        parameters.put("prepaidChargesFlag", "");
                        parameters.put("collectChargesFlag", "");
                    }
                    List<ReportDocumentDimensionDto> docDimensionList = new ArrayList<>();
                    int list1Size = 0;
                    List<ServiceDocumentDimension> documentDimensionList = serviceDocumentDimensionRepository.findAllDimensionsByDocument(documentDetail.getId());
                    int noOfDimensionRows = 7;
                    if (documentDimensionList != null && !documentDimensionList.isEmpty()) {
                        for (ServiceDocumentDimension ssd : documentDimensionList) {
                            String dim = ssd.getNoOfPiece() + "   -    " + ssd.getLength() + "    x    " + ssd.getWidth() + "    x    " + ssd.getHeight();
                            log.info("list1Size ------- " + list1Size + "  ;; noOfDimensionRows ----------- " + noOfDimensionRows);
                            if (list1Size < noOfDimensionRows) {
                                switch (list1Size) {
                                    case 0:
                                        parameters.put("dimension1", dim);
                                        break;
                                    case 1:
                                        parameters.put("dimension2", dim);
                                        break;
                                    case 2:
                                        parameters.put("dimension3", dim);
                                        break;
                                    case 3:
                                        parameters.put("dimension4", dim);
                                        break;
                                    case 4:
                                        parameters.put("dimension5", dim);
                                        break;
                                    case 5:
                                        parameters.put("dimension6", dim);
                                        break;
                                    case 6:
                                        parameters.put("dimension7", dim);
                                        break;
                                    case 7:
                                        parameters.put("dimension8", dim);
                                        break;
                                }

                            } else {
                                ReportDocumentDimensionDto docDimension = new ReportDocumentDimensionDto();
                                docDimension.setHeight(ssd.getHeight());
                                docDimension.setLength(ssd.getLength());
                                docDimension.setNoOfPiece(ssd.getNoOfPiece());
                                docDimension.setWidth(ssd.getWidth());
                                docDimensionList.add(docDimension);
                                secondPageNeeded = true;
                            }
                            list1Size++;
                        }
                    }
                    if (list1Size <= 9) {
                        log.info("In if Loop::::::" + list1Size++);
                        list1Size++;
                        for (int i = list1Size; i <= 8; i++) {
                            parameters.put("dimension" + i, "");
                        }
                    }
                    log.info("noOfDimensionRows  -- " + noOfDimensionRows);
                    if (noOfDimensionRows < 9) {
                        log.info("noOfDimensionRows  #1-- " + noOfDimensionRows);
                        for (int i = (noOfDimensionRows + 1); i <= 9; i++) {
                            parameters.put("dimension" + i, "");
                        }
                    } else {
                        log.info("noOfDimensionRows  #2-- " + noOfDimensionRows);
                        for (int i = 3; i <= 9; i++) {
                            parameters.put("dimension" + i, "");
                        }
                    }

                    if (shipmentServiceDetail.getIsAgreed() != null && shipmentServiceDetail.getIsAgreed().equals(YesNo.Yes)) {
                        if (shipmentServiceDetail.getPpcc().equals(PPCC.Collect)) {
                            parameters.put("weightChargeAsAgreed", "AS AGREED");
                            parameters.put("dueAsAgreed", "");
                            parameters.put("dueCarrierAgent", "");
                            parameters.put("totalCollectAsAgreed", "AS AGREED");
                        } else {
                            parameters.put("weightChargeTotalAmount", "AS AGREED");
                            parameters.put("dueAgent", "");
                            parameters.put("dueCarrier", "");
                            parameters.put("totalPrepaidAmount", "AS AGREED");
                        }
                        parameters.put("total", "AS AGREED");
                        parameters.put("ratePerCharge", "");
                    } else {
                        if (documentDetail != null && documentDetail.getRatePerCharge() != null && documentDetail.getChargebleWeight() != null) {
                            Double totalChargeableWeight = documentDetail.getChargebleWeight() * documentDetail.getRatePerCharge();
                            parameters.put("ratePerCharge", AppUtil.getWeightFormat(documentDetail.getRatePerCharge(), shipmentServiceDetail.getLocation().getCountryMaster().getLocale()));
                            parameters.put("total", AppUtil.getWeightFormat(totalChargeableWeight, shipmentServiceDetail.getLocation().getCountryMaster().getLocale()));

                            if (shipmentServiceDetail.getPpcc().equals(PPCC.Collect)) {
                                parameters.put("weightChargeAsAgreed", AppUtil.getWeightFormat(totalChargeableWeight, shipmentServiceDetail.getLocation().getCountryMaster().getLocale()));
                                parameters.put("totalCollectAsAgreed", AppUtil.getWeightFormat(totalChargeableWeight, shipmentServiceDetail.getLocation().getCountryMaster().getLocale()));
                            } else {
                                parameters.put("weightChargeTotalAmount", AppUtil.getWeightFormat(totalChargeableWeight, shipmentServiceDetail.getLocation().getCountryMaster().getLocale()));
                                parameters.put("totalPrepaidAmount", AppUtil.getWeightFormat(totalChargeableWeight, shipmentServiceDetail.getLocation().getCountryMaster().getLocale()));
                            }
                            Double totalPrepaidAmount = 0.0;
                            Double preapidDueAmount = 0.0;
                            Double preapidCarrierAmount = 0.0;
                            Double collectDueAmount = 0.0;
                            Double collectCarrierAmount = 0.0;

                            String rateMerged = appUtil.getLocationConfig("booking.rates.merged", shipmentServiceDetail.getLocation(), true);
                            if (rateMerged != null) {
                                if (rateMerged.equals("FALSE") || rateMerged == "FALSE") {
                                    if (shipmentServiceDetail.getShipmentChargeList() != null && !shipmentServiceDetail.getShipmentChargeList().isEmpty()) {
                                        for (ShipmentCharge scharge : shipmentServiceDetail.getShipmentChargeList()) {

                                            if (scharge.getPpcc() != null && scharge.getPpcc().equals(PPCC.Collect)) {

                                                if (scharge.getDue() != null && (scharge.getDue().equals(DUE.Agent))) {

                                                    collectDueAmount = collectDueAmount + scharge.getLocalTotalNetSale();

                                                } else if (scharge.getDue() != null && (scharge.getDue().equals(DUE.Carrier))) {

                                                    collectCarrierAmount = collectCarrierAmount + scharge.getLocalTotalNetSale();
                                                }

                                            } else if (scharge.getPpcc() != null && scharge.getPpcc().equals(PPCC.Prepaid)) {
                                                if (scharge.getDue() != null && (scharge.getDue().equals(DUE.Agent))) {

                                                    preapidDueAmount = preapidDueAmount + scharge.getLocalTotalNetSale();

                                                } else if (scharge.getDue() != null && (scharge.getDue().equals(DUE.Carrier))) {

                                                    preapidCarrierAmount = preapidCarrierAmount + scharge.getLocalTotalNetSale();

                                                }
                                            }
                                        }
                                        parameters.put("dueAgent", AppUtil.getCurrencyFormat(preapidDueAmount, shipmentServiceDetail.getLocation().getCountryMaster().getLocale(),
                                                shipmentServiceDetail.getLocalCurrency().getDecimalPoint()));
                                        parameters.put("dueAsAgreed", AppUtil.getCurrencyFormat(collectDueAmount, shipmentServiceDetail.getCountry().getLocale(),
                                                shipmentServiceDetail.getLocalCurrency().getDecimalPoint()));
                                        parameters.put("dueCarrier", AppUtil.getCurrencyFormat(preapidCarrierAmount, shipmentServiceDetail.getCountry().getLocale(),
                                                shipmentServiceDetail.getLocalCurrency().getDecimalPoint()));
                                        parameters.put("dueCarrierAgent", AppUtil.getCurrencyFormat(collectCarrierAmount, shipmentServiceDetail.getCountry().getLocale(),
                                                shipmentServiceDetail.getLocalCurrency().getDecimalPoint()));

                                        Double totalCollectAsAgreed = 0.0;
                                        // collect deue agent and carrier

                                        if (shipmentServiceDetail.getPpcc().equals(PPCC.Collect)) {
                                            totalCollectAsAgreed = collectDueAmount + collectCarrierAmount + totalChargeableWeight;
                                            totalPrepaidAmount = preapidDueAmount + preapidCarrierAmount;
                                        } else {
                                            totalPrepaidAmount = preapidDueAmount + preapidCarrierAmount + totalChargeableWeight;
                                            totalCollectAsAgreed = collectDueAmount + collectCarrierAmount;
                                        }
                                        parameters.put("totalCollectAsAgreed", AppUtil.getCurrencyFormat(totalCollectAsAgreed, shipmentServiceDetail.getCountry().getLocale(),
                                                shipmentServiceDetail.getLocalCurrency().getDecimalPoint()));
                                        parameters.put("totalPrepaidAmount", AppUtil.getCurrencyFormat(totalPrepaidAmount, shipmentServiceDetail.getCountry().getLocale(),
                                                shipmentServiceDetail.getLocalCurrency().getDecimalPoint()));

                                    }
                                } else {

                                    for (ShipmentCharge scharge : shipmentServiceDetail.getShipmentChargeList()) {

                                        if (scharge.getPpcc() != null && scharge.getPpcc().equals(PPCC.Collect)) {

                                            if (scharge.getDue() != null && (scharge.getDue().equals(DUE.Agent))) {

                                                collectDueAmount = collectDueAmount + scharge.getLocalTotalRateAmount();

                                            } else if (scharge.getDue() != null && (scharge.getDue().equals(DUE.Carrier))) {

                                                collectCarrierAmount = collectCarrierAmount + scharge.getLocalTotalRateAmount();
                                            }

                                        } else if (scharge.getPpcc() != null && scharge.getPpcc().equals(PPCC.Prepaid)) {
                                            if (scharge.getDue() != null && (scharge.getDue().equals(DUE.Agent))) {

                                                preapidDueAmount = preapidDueAmount + scharge.getLocalTotalRateAmount();

                                            } else if (scharge.getDue() != null && (scharge.getDue().equals(DUE.Carrier))) {

                                                preapidCarrierAmount = preapidCarrierAmount + scharge.getLocalTotalRateAmount();

                                            }
                                        }
                                    }
                                    parameters.put("dueAgent", AppUtil.getCurrencyFormat(preapidDueAmount, shipmentServiceDetail.getLocation().getCountryMaster().getLocale(),
                                            shipmentServiceDetail.getLocalCurrency().getDecimalPoint()));
                                    parameters.put("dueAsAgreed", AppUtil.getCurrencyFormat(collectDueAmount, shipmentServiceDetail.getCountry().getLocale(),
                                            shipmentServiceDetail.getLocalCurrency().getDecimalPoint()));
                                    parameters.put("dueCarrier", AppUtil.getCurrencyFormat(preapidCarrierAmount, shipmentServiceDetail.getCountry().getLocale(),
                                            shipmentServiceDetail.getLocalCurrency().getDecimalPoint()));
                                    parameters.put("dueCarrierAgent", AppUtil.getCurrencyFormat(collectCarrierAmount, shipmentServiceDetail.getCountry().getLocale(),
                                            shipmentServiceDetail.getLocalCurrency().getDecimalPoint()));

                                    Double totalCollectAsAgreed = 0.0;
                                    // collect deue agent and carrier

                                    if (shipmentServiceDetail.getPpcc().equals(PPCC.Collect)) {
                                        totalCollectAsAgreed = collectDueAmount + collectCarrierAmount + totalChargeableWeight;
                                        totalPrepaidAmount = preapidDueAmount + preapidCarrierAmount;
                                    } else {
                                        totalPrepaidAmount = preapidDueAmount + preapidCarrierAmount + totalChargeableWeight;
                                        totalCollectAsAgreed = collectDueAmount + collectCarrierAmount;
                                    }
                                    parameters.put("totalCollectAsAgreed", AppUtil.getCurrencyFormat(totalCollectAsAgreed, shipmentServiceDetail.getCountry().getLocale(),
                                            shipmentServiceDetail.getLocalCurrency().getDecimalPoint()));
                                    parameters.put("totalPrepaidAmount", AppUtil.getCurrencyFormat(totalPrepaidAmount, shipmentServiceDetail.getCountry().getLocale(),
                                            shipmentServiceDetail.getLocalCurrency().getDecimalPoint()));

                                }
                            }

                        }
                    }

                    parameters1.put("dimensionList", docDimensionList);
                    log.info("Parameters DimenSIon List :" + parameters1.get("dimensionList"));

                    String formatDate = null;
                    if (loggedInUser != null && loggedInUser.getSelectedUserLocation() != null) {
                        formatDate = loggedInUser.getSelectedUserLocation().getJavaDateFormat();
                    }
                    if (shipmentServiceDetail != null) {
                        String flightNO = shipmentServiceDetail.getRouteNo() != null ? shipmentServiceDetail.getRouteNo() : "";
                        String etdDate = reportUtil.dateToReportDate(shipmentServiceDetail.getEtd(), formatDate);
                        if (etdDate != null && !etdDate.equals("")) {
                            flightNO = flightNO + "/" + etdDate;
                        }
                        parameters.put("firstFlightNoAndDate", flightNO);
                    }
                    List<StringBuilder> extraConList = new ArrayList<>();
                    if (shipmentServiceDetail.getConnectionList() != null && !shipmentServiceDetail.getConnectionList().isEmpty()) {
                        int noOfFlights = 0;
                        for (ServiceConnection serviceConnection : shipmentServiceDetail.getConnectionList()) {
                            int len = shipmentServiceDetail.getConnectionList().size();
                            log.info("ServiceConnection Id :: ###################### :: " + serviceConnection.getId());
                            serviceConnection.getEtd();
                            String flightNO = serviceConnection.getFlightVoyageNo() != null ? serviceConnection.getFlightVoyageNo() : "";
                            String etdDate = reportUtil.dateToReportDate(serviceConnection.getEtd(), formatDate);

                            log.info("Flight No :----->> " + flightNO);
						  if(noOfFlights == 0) {
                              if (etdDate != null && !etdDate.equals("")) {
                                  flightNO = flightNO + "/" + etdDate;
                              }
							parameters.put("firstFlightNoAndDate", flightNO);
							parameters.put("connection1Pod", serviceConnection.getPodCode());
							if(serviceConnection.getCarrierMaster() != null) {
								parameters.put("connection1Carrier", serviceConnection.getCarrierMaster().getCarrierCode());
							}
						}
                            if (noOfFlights == 1) {
                                if (etdDate != null && !etdDate.equals("")) {
                                    flightNO = flightNO + "/" + etdDate;
                                }
                                parameters.put("secondFlightNoAndDate", flightNO);
                                parameters.put("connection2Pod", serviceConnection.getPodCode());
                                if (serviceConnection.getCarrierMaster() != null) {
                                    parameters.put("connection2Carrier", serviceConnection.getCarrierMaster().getCarrierCode());
                                }
                            }

                            if (noOfFlights >1) {
                                if(len>2){
                                    extraConList.add(new StringBuilder(" "+ flightNO).append(" | ").
                                            append(etdDate).append(" | ").
                                            append(serviceConnection.getPodCode()).append(" | ").
                                            append(serviceConnection.getCarrierMaster().getCarrierCode()).append(" "));
                                }
                            }
                            noOfFlights++;
                        }
                    }
                    String extraConnections = String.join(",", extraConList);
                    parameters.put("handlingInformation", documentDetail.getHandlingInformation() != null ? documentDetail.getHandlingInformation() : "");
                    if(shipmentServiceDetail.getConnectionList().size() >2){
                        parameters.put("handlingInformation", extraConnections);
                    }
                }
                if (firstNotify != null) {
                    acName = new ReportGenericDto();
                    acName.setAddress(firstNotify.getPartyName() != null ? firstNotify.getPartyName() : "");
                    firstNotifyAddressList.add(acName);
                }
                List<ReportGenericDto> tempAddressList = reportUtil.addressMasterToGenericList(firstNotifyAddressMaster);
                if (tempAddressList != null && !tempAddressList.isEmpty()) {
                    firstNotifyAddressList.addAll(tempAddressList);
                }
                parameters.put("accountingInformationList", firstNotifyAddressList);

                PartyMaster issuingAgent = documentDetailRepository.findByIdForIssuingAgent(documentDetail.getId());
                if (issuingAgent != null) {
                    if (issuingAgent.getPartyDetail() != null) {
                        parameters.put("iataCode", issuingAgent.getPartyDetail().getIataCode() != null ? issuingAgent.getPartyDetail().getIataCode() : "");
                        parameters.put("accountNumber", issuingAgent.getPartyDetail().getAccountNumber() != null ? issuingAgent.getPartyDetail().getAccountNumber() : "");
                    }
                    if (issuingAgent.getPartyName() != null) {
                        parameters.put("issuingCarrierAgentName", reportUtil.removeSpecialCharacters(issuingAgent.getPartyName(), requestData.getDownloadOption()));
                        parameters.put("issuingCarrierName", reportUtil.removeSpecialCharacters(issuingAgent.getPartyName(), requestData.getDownloadOption()));
                    } else {
                        parameters.put("issuingCarrierAgentName", "");
                        parameters.put("issuingCarrierName", "");
                    }

                } else {
                    parameters.put("issuingCarrierAgentName", "");
                    parameters.put("issuingCarrierName", "");
                }


                String polCode = "";
                if (documentDetail != null) {
                    parameters.put("decValueOfCarriage", documentDetail.getDeclaredValueForCarriage());
                    parameters.put("decValueOfCustoms", documentDetail.getDeclaredValueForCustoms());
                    parameters.put("amountOfInsurance", documentDetail.getAmountOfInsurance());
                    parameters.put("shipmentId", shipmentServiceDetail.getServiceUid() != null ? shipmentServiceDetail.getServiceUid() : "");
                    parameters.put("podCode", documentDetail.getPodCode() != null ? documentDetail.getPodCode() : "");
                    parameters.put("accountingInformation", documentDetail.getAccoutingInformation() != null ? documentDetail.getAccoutingInformation() : "");
                    parameters.put("noOfPieces", documentDetail.getNoOfPieces() != null ? documentDetail.getNoOfPieces().toString() : "");
                    parameters.put("grossWeight", documentDetail.getGrossWeight() != null ? AppUtil.getWeightFormat(documentDetail.getGrossWeight(), shipmentServiceDetail.getLocation().getCountryMaster().getLocale()) : "");
                    if (documentDetail.getDimensionUnit() != null) {
                        if (documentDetail.getDimensionUnit().equals(DimensionUnit.CENTIMETERORKILOS)) {
                            parameters.put("kgLB", "KG");
                            parameters.put("centimeterOrInch", "Kilo");
                            parameters1.put("centimeterOrInch", "Kilo");
                        } else {
                            parameters.put("kgLB", "KG");
                            parameters.put("centimeterOrInch", "Inch");
                            parameters1.put("centimeterOrInch", "Inch");
                        }
                    } else {
                        parameters.put("centimeterOrInch", "");
                    }
                    parameters.put("rateClass", documentDetail.getRateClass() != null ? documentDetail.getRateClass().toString() : "");
                    parameters.put("chargeableWeight", documentDetail.getChargebleWeight() != null ? AppUtil.getWeightFormat(documentDetail.getChargebleWeight(), shipmentServiceDetail.getLocation().getCountryMaster().getLocale()) : "");

                    String mawbTemp = "";
                    if (shipmentServiceDetail != null) {
                        mawbTemp = shipmentServiceDetail.getMawbNo() != null ? shipmentServiceDetail.getMawbNo() : "";
                    }
                    String mawbFirst3Chars = "";
                    if (mawbTemp != null) {
                        if (mawbTemp.length() >= 3) {
                            mawbFirst3Chars = mawbTemp.substring(0, 3);
                            mawbTemp = mawbTemp.substring(3, mawbTemp.length());
                        }
                        if (mawbTemp.length() >= 4) {
                            mawbTemp = mawbTemp.substring(0, 4) + " " + mawbTemp.substring(4, mawbTemp.length());
                        } else {
                            mawbTemp = "";
                        }
                    } else {
                        mawbFirst3Chars = "";
                        mawbTemp = "";
                    }
                    parameters.put("mawbNo", mawbTemp);
                    parameters.put("mawbFirst3Chars", mawbFirst3Chars);
                    if (documentDetail.getPol() != null) {
                        polCode = documentDetail.getPol().getPortCode() != null ? documentDetail.getPol().getPortCode() : "";
                        parameters.put("polCode", polCode);
                    }

                    if (documentDetail.getMarksAndNo() != null) {
                        int markLength = documentDetail.getMarksAndNo().length();
                        log.info("Marks and No  Total length::::::" + markLength);
                        if (markLength > 700) {
                            marksParam1 = documentDetail.getMarksAndNo().substring(0, 700);
                            marksParam2 = documentDetail.getMarksAndNo().substring(700, documentDetail.getMarksAndNo().length());
                            secondPageNeeded = true;
                        } else {
                            marksParam1 = documentDetail.getMarksAndNo();
                        }
                    }
                    log.info("Document Detail Id :: " + documentDetail.getId() + "; Marks And No :: " + documentDetail.getMarksAndNo());
                }
                parameters.put("marksAndNo", marksParam1);
                rider.setMarksAndNo(marksParam2);
                hawbRiderList.add(rider);
                PortMaster pol = documentDetailRepository.findByIdForPol(documentDetail.getId());
                if (pol != null && pol.getPortName() != null) {
                    polCode = "(" + polCode + ")";
                    parameters.put("polName", polCode + " " + pol.getPortName());
                } else {
                    parameters.put("polName", "");
                }
                PortMaster destination = documentDetailRepository.findByIdForDestination(documentDetail.getId());
                if (destination != null && destination.getPortName() != null) {
                    String poldestination = destination.getPortCode() != null ? "(" + destination.getPortCode() + ")" : "";
                    parameters.put("fdestinationName", poldestination + " " + destination.getPortName());
                } else {
                    parameters.put("fdestinationName", "");
                }

                List<PrepaidCharges> chargesList = new ArrayList<>();
                List<PrepaidCharges> chargeListRider = new ArrayList<>();
                List<PrepaidCharges> chargeListRiderAll = new ArrayList<>();
                List<PrepaidCharges> secondList = new ArrayList<>();

                String rateMerged = appUtil.getLocationConfig("booking.rates.merged", shipmentServiceDetail.getLocation(), true);

                if (rateMerged != null) {
                    if (rateMerged.equals("FALSE") || rateMerged == "FALSE") {
                        if (shipmentServiceDetail.getShipmentChargeList() != null && !shipmentServiceDetail.getShipmentChargeList().isEmpty()) {
                            for (ShipmentCharge shipmentCharge : shipmentServiceDetail.getShipmentChargeList()) {
                                PrepaidCharges netSaleCharges = new PrepaidCharges();
                                if (shipmentCharge != null) {
                                    if (shipmentCharge.getChargeMaster() != null) {
                                        netSaleCharges.setChargeName(shipmentCharge.getChargeMaster().getChargeCode());
                                    }
                                    if (shipmentCharge.getLocalTotalNetSale() != null) {
                                        netSaleCharges.setTotalLocalAmount(AppUtil.getCurrencyFormat(shipmentCharge.getLocalTotalNetSale(),
                                                shipmentServiceDetail.getCountry().getLocale(), shipmentServiceDetail.getLocalCurrency().getDecimalPoint()));
                                    }
                                    chargesList.add(netSaleCharges);
                                }
                            }
                        }
                    } else {
                        if (shipmentServiceDetail.getShipmentChargeList() != null && !shipmentServiceDetail.getShipmentChargeList().isEmpty()) {
                            for (ShipmentCharge shipmentCharge : shipmentServiceDetail.getShipmentChargeList()) {
                                PrepaidCharges rateSaleCharges = new PrepaidCharges();
                                if (shipmentCharge != null) {
                                    if (shipmentCharge.getChargeMaster() != null) {
                                        rateSaleCharges.setChargeName(shipmentCharge.getChargeMaster().getChargeCode());
                                    }
                                    if (shipmentCharge.getLocalTotalRateAmount() != null) {
                                        rateSaleCharges.setTotalLocalAmount(AppUtil.getCurrencyFormat(shipmentCharge.getLocalTotalRateAmount(), shipmentServiceDetail.getCountry().getLocale(), shipmentServiceDetail.getLocalCurrency().getDecimalPoint()));
                                    }
                                    chargesList.add(rateSaleCharges);
                                }
                            }
                        }
                    }
                }
                if (chargesList != null && chargesList.size() > 0) {
                    for (int j = 0; j < chargesList.size(); j++) {
                        if (j <= 2) {
                            chargeListRiderAll.add(chargesList.get(j));
                        } else if (j >= 3 && j <= 5) {
                            secondList.add(chargesList.get(j));
                        } else {
                            chargeListRider.add(chargesList.get(j));
                            if (shipmentServiceDetail != null && shipmentServiceDetail.getIsAgreed() != null && shipmentServiceDetail.getIsAgreed().equals(YesNo.Yes)) {
                                secondPageNeeded = false;
                                parameters1.put("riderChargesList", null);
                            } else {
                                secondPageNeeded = true;
                                parameters1.put("riderChargesList", chargeListRider);
                            }

                        }
                    }
                }

                if (shipmentServiceDetail != null && shipmentServiceDetail.getIsAgreed() != null && shipmentServiceDetail.getIsAgreed().equals(YesNo.Yes)) {
                    parameters.put("chargesSecondList", null);
                    parameters.put("chargesList", null);
                } else {
                    parameters.put("chargesSecondList", secondList);
                    parameters.put("chargesList", chargeListRiderAll);
                }


                JasperPrint jasperPrint1 = null;
                JasperPrint jasperPrintRider = null;
                JasperPrint jasperPrint = null;

                CountryReportConfigMaster crcf = countryReportConfigRepository.findbyReportName(requestData.getReportName().HAWB.name(), shipmentServiceDetail.getLocation().getCountryMaster().getId());
                if (crcf != null) {
                    if (crcf.getPageMasterList() != null && crcf.getPageMasterList().size() > 0) {
                        PageMaster pageMaster = crcf.getPageMasterList().get(0);
                        if (pageMaster != null) {
                            String pageName = pageMaster.getPageName();
                            parameters.put("pageNameDetails", pageName);
                            JasperReport jasperReport = JasperCompileManager.compileReport(appResourcePath + "/efs-report/hawb.jrxml");
                            List<Object> fieldCollection = new ArrayList<>();
                            Object obj = new Object();
                            fieldCollection.add(obj);
                            JRDataSource dataSource = new JRBeanCollectionDataSource(fieldCollection, false);
                            jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, dataSource);
                        }
                    }
                    if (crcf.getPageMasterList().size() > 1) {

                    } else {
                        if (secondPageNeeded) {
                            JasperReport jasperReport1 = JasperCompileManager.compileReport(appResourcePath + "/efs-report/hawb_rider.jrxml");
                            List<Object> fieldCollection1 = new ArrayList<>();
                            Object obj1 = new Object();
                            fieldCollection1.add(obj1);
                            JRDataSource dataSource1 = new JRBeanCollectionDataSource(hawbRiderList, false);
                            jasperPrint1 = JasperFillManager.fillReport(jasperReport1, parameters1, dataSource1);
                            List<JRPrintPage> pages = jasperPrint1.getPages();
                            for (int count = 0; count < pages.size(); count++) {
                                jasperPrint.addPage(pages.get(count));
                            }
                        }
                    }
                } else {
                    JasperReport jasperReport = JasperCompileManager.compileReport(appResourcePath + "/efs-report/hawb.jrxml");
                    List<Object> fieldCollection = new ArrayList<>();
                    Object obj = new Object();
                    fieldCollection.add(obj);
                    JRDataSource dataSource = new JRBeanCollectionDataSource(fieldCollection, false);
                    jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, dataSource);
                    if (secondPageNeeded) {
                        JasperReport jasperReport1 = JasperCompileManager.compileReport(appResourcePath + "/efs-report/hawb_rider.jrxml");
                        List<Object> fieldCollection1 = new ArrayList<>();
                        Object obj1 = new Object();
                        fieldCollection1.add(obj1);
                        JRDataSource dataSource1 = new JRBeanCollectionDataSource(hawbRiderList, false);
                        jasperPrint1 = JasperFillManager.fillReport(jasperReport1, parameters1, dataSource1);
                        List<JRPrintPage> pages = jasperPrint1.getPages();
                        for (int count = 0; count < pages.size(); count++) {
                            jasperPrint.addPage(pages.get(count));
                        }
                    }
                }

                if (crcf != null) {
                    if (crcf.getPageMasterList() != null && crcf.getPageMasterList().size() > 1) {
                        for (int i = 1; i < crcf.getPageMasterList().size(); i++) {
                            PageMaster pageMaster = crcf.getPageMasterList().get(i);
                            if (pageMaster != null) {
                                String pageNo = pageMaster.getPageCode();
                                log.info("PageNumber:" + pageNo);
                                String pageName = pageMaster.getPageName();
                                log.info("PageName:" + pageName);
                                parameters.put("pageNameDetails", pageName);
                                JasperReport jasperReport1 = JasperCompileManager.compileReport(appResourcePath + "/efs-report/hawb.jrxml");
                                List<Object> fieldCollection1 = new ArrayList<>();
                                Object obj1 = new Object();
                                fieldCollection1.add(obj1);
                                JRDataSource dataSource1 = new JRBeanCollectionDataSource(fieldCollection1, false);
                                jasperPrint1 = JasperFillManager.fillReport(jasperReport1, parameters, dataSource1);
                                List<JRPrintPage> pages = jasperPrint1.getPages();
                                for (int count = 0; count < pages.size(); count++) {
                                    jasperPrint.addPage(pages.get(count));
                                }
                            }
                        }
                    }

                    if (crcf.getPageMasterList().size() > 1) {
                        if (secondPageNeeded) {
                            JasperReport jasperReportRider = JasperCompileManager.compileReport(appResourcePath + "/efs-report/hawb_rider.jrxml");
                            JRDataSource dataSourceRider = new JRBeanCollectionDataSource(hawbRiderList, false);
                            jasperPrintRider = JasperFillManager.fillReport(jasperReportRider, parameters1, dataSourceRider);
                            List<JRPrintPage> pagesRider = jasperPrintRider.getPages();
                            for (int count = 0; count < pagesRider.size(); count++) {
                                jasperPrint.addPage(pagesRider.get(count));
                            }
                        }
                    }
                }
                return jasperPrint;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }
}
