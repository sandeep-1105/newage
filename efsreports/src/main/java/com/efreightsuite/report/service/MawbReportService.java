package com.efreightsuite.report.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import com.efreightsuite.dto.AutoAttachmentDto;
import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.ReportDownloadRequestDto;
import com.efreightsuite.dto.ReportGenericDto;
import com.efreightsuite.enumeration.DUE;
import com.efreightsuite.enumeration.DimensionUnit;
import com.efreightsuite.enumeration.PPCC;
import com.efreightsuite.enumeration.ReportConfig;
import com.efreightsuite.enumeration.ReportDownloadOption;
import com.efreightsuite.enumeration.ReportName;
import com.efreightsuite.enumeration.YesNo;
import com.efreightsuite.model.AddressMaster;
import com.efreightsuite.model.Consol;
import com.efreightsuite.model.ConsolCharge;
import com.efreightsuite.model.ConsolConnection;
import com.efreightsuite.model.ConsolDocument;
import com.efreightsuite.model.ConsolDocumentDimension;
import com.efreightsuite.model.PartyAddressMaster;
import com.efreightsuite.model.ReportConfigurationMaster;
import com.efreightsuite.model.UserProfile;
import com.efreightsuite.report.HawbReportRider;
import com.efreightsuite.report.PrepaidCharges;
import com.efreightsuite.report.ReportDocumentDimensionDto;
import com.efreightsuite.service.AuthService;
import com.efreightsuite.util.AppUtil;
import lombok.extern.log4j.Log4j2;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRPrintPage;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

import org.springframework.stereotype.Service;

@Service
@Log4j2
public class MawbReportService extends AbstractReportService {
    public ReportName getReportType() {
        return ReportName.SHIPMENT_MAWB;
    }

    public byte[] processReport(ReportDownloadRequestDto requestData, HttpServletResponse response, boolean isMultiple) {
        log.info("I am in MawbReportService.....");
        BaseDto baseDto = new BaseDto();
        UserProfile logInUSer = AuthService.getCurrentUser();
        JasperPrint jasperPrint = getReport(baseDto, requestData, AuthService.getCurrentUser());

        byte[] a = null;
        AutoAttachmentDto autoAttachmentDto = reportUtil.autoAttachment(ReportName.SHIPMENT_MAWB, YesNo.Yes, requestData.getResourceId(), "MasterShipment");
        if (requestData.getSingle() != null && requestData.getSingle() == true) {
            if (requestData.getDownloadOption() == ReportDownloadOption.Preview) {
                reportUtil.japerPrintToResponseOutputStream(requestData.getDownloadFileType(), jasperPrint, "SHIPMENT_MAWB", response, false, autoAttachmentDto);
            }
            if (requestData.getDownloadOption() == ReportDownloadOption.Print) {
                reportUtil.japerPrintToResponseOutputStream(requestData.getDownloadFileType(), jasperPrint, "SHIPMENT_MAWB", response, false, autoAttachmentDto);
            }
            if (autoAttachmentDto != null && autoAttachmentDto.getIsAttach() != null || requestData.getDownloadOption() == ReportDownloadOption.Download) {
                reportUtil.japerPrintToResponseOutputStream(requestData.getDownloadFileType(), jasperPrint, "SHIPMENT_MAWB", response, false, autoAttachmentDto);
            }
        }
        if (requestData.getSingle() != null && requestData.getSingle() == true && requestData.getDownloadOption() == ReportDownloadOption.eMail) {
            a = reportUtil.japerPrintToResponseOutputStream(requestData.getDownloadFileType(), jasperPrint, "SHIPMENT_MAWB", response, false, autoAttachmentDto);
            consolReportMailer.sendConsolMailToCustomer(requestData.getResourceId(), logInUSer, ReportName.SHIPMENT_MAWB, a);
        }
        if (isMultiple == true && requestData.getDownloadOption() == ReportDownloadOption.eMail) {
            a = reportUtil.japerPrintToResponseOutputStream(requestData.getDownloadFileType(), jasperPrint, "SHIPMENT_MAWB", response, true, autoAttachmentDto);
            return a;
        }
        return null;
    }

    private JasperPrint getReport(BaseDto baseDto, ReportDownloadRequestDto data, UserProfile loggedInUser) {
        try {
            Map<String, Object> parameters = new HashMap<>();
            Map<String, Object> parameters1 = new HashMap<>();
            Consol consolEntity = consolRepository.findById(data.getResourceId());
            if (consolEntity != null) {
                ConsolDocument consolDocumentEntity = consolDocumentRepository.findById(consolEntity.getConsolDocument().getId());
                {
                    ReportConfigurationMaster tmpObj = reportConfigurationMasterRepository.getByKeyAndLocation(ReportConfig.MAWB_COPIES_DECLARATION, consolEntity.getLocation().getId());
                    if (tmpObj != null && tmpObj.getReportValue() != null) {
                        parameters.put("mawbCopiesDeclaration", tmpObj.getReportValue());
                    }
                }

                {
                    ReportConfigurationMaster tmpObj = reportConfigurationMasterRepository.getByKeyAndLocation(ReportConfig.MAWB_AGREED_GOOD_ORDER_CONDITION, consolEntity.getLocation().getId());
                    if (tmpObj != null && tmpObj.getReportValue() != null) {
                        parameters.put("mawbAgreedGoodOrderCondition", tmpObj.getReportValue());
                    }
                }

                {
                    ReportConfigurationMaster tmpObj = reportConfigurationMasterRepository.getByKeyAndLocation(ReportConfig.MAWB_INSURANCE, consolEntity.getLocation().getId());
                    if (tmpObj != null && tmpObj.getReportValue() != null) {
                        parameters.put("mawbInsurance", tmpObj.getReportValue());
                    }
                }

                {
                    ReportConfigurationMaster tmpObj = reportConfigurationMasterRepository.getByKeyAndLocation(ReportConfig.MAWB_SHIPPER_CERTIFIES_CONSIGNMENT, consolEntity.getLocation().getId());
                    if (tmpObj != null && tmpObj.getReportValue() != null) {
                        parameters.put("mawbShipperCertifiesConsignment", tmpObj.getReportValue());
                    }
                }


                log.info("I am in SHIPMENT MAWB ReportService :: " + consolDocumentEntity);
                if (consolDocumentEntity != null) {
                    parameters.put("decValueOfCarriage", consolDocumentEntity.getDeclaredValueForCarriage());
                    parameters.put("decValueOfCustoms", consolDocumentEntity.getDeclaredValueForCustoms());
                    parameters.put("amountOfInsurance", consolDocumentEntity.getAmountOfInsurance());
                }
                parameters.put("carrierCode", consolEntity.getCarrierCode());

                List<ReportGenericDto> reportGenericDtoList = reportUtil.addressMasterToGenericList(consolDocumentRepository.findByIdForIssuingAgentAddress(consolEntity.getConsolDocument().getId()));
                if (reportGenericDtoList != null && !reportGenericDtoList.isEmpty() && reportGenericDtoList.size() > 4) {
                    ReportGenericDto line5 = reportGenericDtoList.get(4);
                    reportGenericDtoList.remove(4);
                    ReportGenericDto line4 = reportGenericDtoList.get(3);
                    reportGenericDtoList.remove(3);
                    ReportGenericDto newLine4 = new ReportGenericDto();
                    newLine4.setAddress(line4.getAddress() + " " + line5.getAddress());
                    reportGenericDtoList.add(newLine4);
                }
                parameters.put("issuingCarrierAgentAddressList", reportGenericDtoList);
                if (consolDocumentEntity != null) {
                    if (consolDocumentEntity.getShipper() != null) {
                        parameters.put("shipperName", consolDocumentEntity.getShipper().getPartyName() != null ? consolDocumentEntity.getShipper().getPartyName() : "");
                    }
                    parameters.putAll(AddressMasterToMap("shipperAddressLine", consolDocumentEntity.getShipperAddress()));

                    if (consolDocumentEntity.getConsignee() != null) {
                        parameters.put("consigneeName", consolDocumentEntity.getConsignee().getPartyName() != null ? consolDocumentEntity.getConsignee().getPartyName() : "");
                    }
                    parameters.putAll(AddressMasterToMap("consigneeAddressLine", consolDocumentEntity.getConsigneeAddress()));

                    if (consolDocumentEntity.getIssuingAgent() != null) {
                        parameters.put("issuingCarrierAgentName", consolDocumentEntity.getIssuingAgent().getPartyName() != null ? consolDocumentEntity.getIssuingAgent().getPartyName() : "");
                        parameters.put("issuingCarrierName", consolDocumentEntity.getIssuingAgent().getPartyName() != null ? consolDocumentEntity.getIssuingAgent().getPartyName() : "");
                        if (consolDocumentEntity.getIssuingAgent().getPartyDetail() != null) {
                            parameters.put("iataCode", consolDocumentEntity.getIssuingAgent().getPartyDetail().getIataCode() != null ? consolDocumentEntity.getIssuingAgent().getPartyDetail().getIataCode() : "");
                            parameters.put("accountNumber", consolDocumentEntity.getIssuingAgent().getPartyDetail().getAccountNumber() != null ? consolDocumentEntity.getIssuingAgent().getPartyDetail().getAccountNumber() : "");
                        }
                    }
                    parameters.putAll(AddressMasterToMap("issuingCarrierAgentAddressLine", consolDocumentEntity.getIssuingAgentAddress()));

                }

                if (consolEntity != null && consolEntity.getCarrier() != null) {
                    String carrierName = consolEntity.getCarrier().getCarrierName();
                    parameters.put("carrierName", carrierName);
                }
                String loggedInUserName = "";
                if (loggedInUser != null) {
                    if (loggedInUser.getEmployee() != null && loggedInUser.getEmployee().getEmployeeName() != null) {
                        loggedInUserName = loggedInUser.getEmployee().getEmployeeName();
                    } else if (loggedInUser.getUserName() != null) {
                        loggedInUserName = loggedInUser.getUserName();
                    } else {
                        loggedInUserName = "";
                    }
                    if (loggedInUser.getSelectedUserLocation() != null) {
                        parameters.put("issuedDate", reportUtil.dateToReportDate(new Date(), loggedInUser.getSelectedUserLocation().getJavaDateFormat()));
                    }

                }
                parameters.put("loginUserName", loggedInUserName);


                String formatDate = null;
                if (loggedInUser != null && loggedInUser.getSelectedUserLocation() != null) {
                    formatDate = loggedInUser.getSelectedUserLocation().getJavaDateFormat();
                }
                if (consolDocumentEntity != null) {
                    String flightNO = consolDocumentEntity.getRouteNo() != null ? consolDocumentEntity.getRouteNo() : "";
                    String etdDate = reportUtil.dateToReportDate(consolEntity.getEtd(), formatDate);
                    if (etdDate != null && !etdDate.equals("")) {
                        flightNO = flightNO + "/" + etdDate;
                    }
                    parameters.put("firstFlightNoAndDate", flightNO);
                }
                List<StringBuilder> extraConList = new ArrayList<>();
                if (consolEntity != null && consolEntity.getConnectionList() != null && !consolEntity.getConnectionList().isEmpty()) {

                    int noOfFlights = 0;
                    for (ConsolConnection serviceConnection : consolEntity.getConnectionList()) {
                        int len = consolEntity.getConnectionList().size();
                        log.info("ServiceConnection Id :: ###################### :: " + serviceConnection.getId());
                        serviceConnection.getEtd();
                        String flightNO = serviceConnection.getFlightVoyageNo() != null ? serviceConnection.getFlightVoyageNo() : "";
                        String etdDate = reportUtil.dateToReportDate(serviceConnection.getEtd(), formatDate);

                        log.info("Flight No :----->> " + flightNO);
                        if(noOfFlights == 0) {
                            if (etdDate != null && !etdDate.equals("")) {
                                flightNO = flightNO + "/" + etdDate;
                            }
                            parameters.put("firstFlightNoAndDate", flightNO);
                            parameters.put("connection1Pod", serviceConnection.getPodCode());
                            if(serviceConnection.getCarrierMaster() != null) {
                                parameters.put("connection1Carrier", serviceConnection.getCarrierMaster().getCarrierCode());
                            }
                        }
                        if (noOfFlights == 1) {
                            if (etdDate != null && !etdDate.equals("")) {
                                flightNO = flightNO + "/" + etdDate;
                            }
                            parameters.put("secondFlightNoAndDate", flightNO);
                            parameters.put("connection2Pod", serviceConnection.getPodCode());
                            if (serviceConnection.getCarrierMaster() != null) {
                                parameters.put("connection2Carrier", serviceConnection.getCarrierMaster().getCarrierCode());
                            }
                        }

                        if (noOfFlights >1) {
                            if(len>2){
                                extraConList.add(new StringBuilder(" "+ flightNO).append(" | ").
                                        append(etdDate).append(" | ").
                                        append(serviceConnection.getPodCode()).append(" | ").
                                        append(serviceConnection.getCarrierMaster().getCarrierCode()).append(" "));
                            }
                        }
                        noOfFlights++;
                    }
                }
                String extraConnections = String.join(",", extraConList);

                parameters.put("handlingInformation", consolDocumentEntity.getHandlingInformation() != null ? consolDocumentEntity.getHandlingInformation() : "");
                if(consolEntity.getConnectionList().size() >2){
                    parameters.put("handlingInformation", extraConnections);
                }
                String polCode = "";

                parameters.put("podCode", consolEntity.getPodCode() != null ? consolEntity.getPodCode() : "");
                parameters.put("otherCharges", "");
                parameters.put("noOfPieces",
                        consolDocumentEntity.getNoOfPieces() != null ? consolDocumentEntity.getNoOfPieces().toString() : "");
                if (consolDocumentEntity.getGrossWeight() != null) {
                    parameters.put("grossWeight", AppUtil.getWeightFormat(consolDocumentEntity.getGrossWeight(), consolEntity.getCountry().getLocale()));
                    parameters.put("grossWeightLBS", AppUtil.getWeightFormat(consolDocumentEntity.getGrossWeight(), consolEntity.getCountry().getLocale()));
                }
                if (consolDocumentEntity.getChargebleWeight() != null) {
                    parameters.put("chargeableWeight", AppUtil.getWeightFormat(consolDocumentEntity.getChargebleWeight(), consolEntity.getCountry().getLocale()));
                }

                parameters.put("kgLB", "KG");

                if (consolDocumentEntity.getDimensionUnit() != null) {
                    if (consolDocumentEntity.getDimensionUnit().getDimensionUnit().equals(DimensionUnit.CENTIMETERORKILOS)) {
                        parameters.put("centimeterOrInch", "Kilo");
                        parameters1.put("centimeterOrInch", "Kilo");
                    } else {
                        parameters.put("centimeterOrInch", "Inch");
                        parameters1.put("centimeterOrInch", "Inch");
                    }
                }
                parameters.put("rateClass", consolDocumentEntity.getRateClass() != null ? consolDocumentEntity.getRateClass().toString() : "");

                String mawbTemp = consolDocumentEntity.getMawbNo();
                parameters1.put("mawbNumber", mawbTemp);
                String mawbFirst3Chars = "";
                if (mawbTemp != null) {
                    if (mawbTemp.length() >= 3) {
                        mawbFirst3Chars = mawbTemp.substring(0, 3);
                        mawbTemp = mawbTemp.substring(3, mawbTemp.length());
                    }
                    System.out.println(mawbTemp);
                    if (mawbTemp.length() >= 4) {
                        mawbTemp = mawbTemp.substring(0, 4) + " " + mawbTemp.substring(4, mawbTemp.length());
                        parameters.put("mawbLastSequence", mawbTemp);
                    } else {
                        parameters.put("mawbLastSequence", "");
                    }
                } else {
                    parameters.put("mawbFirst3Chars", "");
                    parameters.put("mawbLastSequence", "");
                }

                parameters.put("mawbFirst3Chars", mawbFirst3Chars);
                if (consolEntity.getPol() != null) {
                    polCode = consolEntity.getPol().getPortCode() != null ? consolEntity.getPol().getPortCode() : "";
                    parameters.put("polCode", polCode);
                }

                List<HawbReportRider> hawbRiderList = new ArrayList<>();
                HawbReportRider rider = new HawbReportRider();
                String marksParam1 = "";
                String marksParam2 = "";
                boolean secondPageNeeded = false;
                int commoditylengthLine = 0;
                //parameters.put("commodityDescription", consolDocumentEntity.getCommodityDescription() != null ? consolDocumentEntity.getCommodityDescription() : "");

                if (consolDocumentEntity != null && consolDocumentEntity.getCommodityDescription() != null) {
                    String commodityDescription = consolDocumentEntity.getCommodityDescription();
                    commoditylengthLine = commodityDescription.length();
                    if (commoditylengthLine > 0) {
                        commoditylengthLine = commoditylengthLine / 38;
                    }
                    if (commoditylengthLine > 10) {
                        parameters.put("commodityDescription", consolDocumentEntity.getCommodityDescription().substring(0, 250));
                        rider.setCommodityDescription(consolDocumentEntity.getCommodityDescription().substring(250, commodityDescription.length()));
                        secondPageNeeded = true;
                    } else {
                        parameters.put("commodityDescription", consolDocumentEntity.getCommodityDescription());
                    }
                }
                if (consolDocumentEntity.getMarksAndNo() != null) {
                    int markLength = consolDocumentEntity.getMarksAndNo().length();
                    if (markLength > 700) {
                        marksParam1 = consolDocumentEntity.getMarksAndNo().substring(0, 700);
                        marksParam2 = consolDocumentEntity.getMarksAndNo().substring(700, consolDocumentEntity.getMarksAndNo().length());
                        secondPageNeeded = true;
                    } else {
                        marksParam1 = consolDocumentEntity.getMarksAndNo();
                    }
                }
                if (consolDocumentEntity.getMarksAndNo() != null) {
                    int markLength = consolDocumentEntity.getMarksAndNo().length();
                    if (markLength > 700) {
                        marksParam1 = consolDocumentEntity.getMarksAndNo().substring(0, 700);
                        marksParam2 = consolDocumentEntity.getMarksAndNo().substring(700, markLength - 1);
                        secondPageNeeded = true;
                    } else {
                        marksParam1 = consolDocumentEntity.getMarksAndNo();
                    }
                }

                parameters.put("marksAndNo", marksParam1);
                log.info("Marks and NO::::::::::" + marksParam2);
                rider.setMarksAndNo(marksParam2);
                hawbRiderList.add(rider);
                parameters.put("marksAndNo", marksParam1);

                if (consolEntity.getPol() != null && consolEntity.getPol().getPortName() != null) {
                    polCode = "(" + polCode + ")";
                    if (consolEntity.getCarrier() != null) {
                        parameters.put("polName", polCode + " " + consolEntity.getPol().getPortName() + "(" + consolEntity.getCarrier().getCarrierCode() + ")");
                    } else {
                        parameters.put("polName", polCode + " " + consolEntity.getPol().getPortName());
                    }

                } else {
                    parameters.put("polName", "");
                }

                if (consolEntity.getDestination() != null && consolEntity.getDestination().getPortName() != null) {
                    String poldestination = consolEntity.getDestination().getPortCode() != null ? "(" + consolEntity.getDestination().getPortCode() + ")" : "";
                    parameters.put("destinationName", poldestination + " " + consolEntity.getDestination().getPortName());
                } else {
                    parameters.put("destinationName", "");
                }

                parameters.put("referenceNumber", consolEntity.getConsolUid());
                parameters.put("currencyCode", consolEntity.getCurrencyCode());
                String freightName = "";
                String spotNo = "";
                if (consolEntity.getPpcc() != null) {
                    freightName = "** FREIGHT " + consolEntity.getPpcc().toString().toUpperCase() + " **";
                    if (consolEntity.getPpcc().equals(PPCC.Collect)) {
                        parameters.put("prepaidOrCollectChargesFlag", "C");
                        parameters.put("collectChargesFlag", "C");
                        parameters.put("prepaidChargesFlag", "");
                    } else {
                        parameters.put("prepaidOrCollectChargesFlag", "P");
                        parameters.put("prepaidChargesFlag", "P");
                        parameters.put("collectChargesFlag", "");
                    }
                } else {
                    parameters.put("prepaidOrCollectChargesFlag", "");
                    parameters.put("prepaidChargesFlag", "");
                    parameters.put("collectChargesFlag", "");
                }

                if (consolDocumentEntity != null) {
                    AddressMaster firstnotifyAddress = null;
                    String acc = "";
                    if (consolDocumentEntity.getAccountingInformation() != null) {
                        acc = consolDocumentEntity.getAccountingInformation();
                        parameters.put("accountingInformation", acc);
                    }
                    if (consolDocumentEntity.getFirstNotify() != null) {
                        firstnotifyAddress = consolDocumentEntity.getFirstNotifyAddress();
                        String address1 = firstnotifyAddress.getAddressLine1() != null ? firstnotifyAddress.getAddressLine1() : "";
                        String address2 = firstnotifyAddress.getAddressLine2() != null ? firstnotifyAddress.getAddressLine2() : "";
                        String address3 = firstnotifyAddress.getAddressLine3() != null ? firstnotifyAddress.getAddressLine3() : "";
                        String address4 = firstnotifyAddress.getAddressLine4() != null ? firstnotifyAddress.getAddressLine4() : "";
                        String phone = firstnotifyAddress.getPhone() != null ? firstnotifyAddress.getPhone() : "";
                        String mobile = firstnotifyAddress.getMobile() != null ? firstnotifyAddress.getMobile() : "";
                        parameters.put("firstNotifyName", consolDocumentEntity.getFirstNotify().getPartyName());
                        if (address1.length() > 0 && address2.length() > 0) {
                            parameters.put("accountingInfoLine1", address1 + "," + address2);
                        } else if (address1.length() > 0) {
                            parameters.put("accountingInfoLine1", address1);
                        } else if (address2.length() > 0) {
                            parameters.put("accountingInfoLine1", address2);
                        }
                        if (address3.length() > 0 && address4.length() > 0) {
                            parameters.put("accountingInfoLine2", address3 + "," + address4);
                        } else if (address3.length() > 0) {
                            parameters.put("accountingInfoLine2", address3);
                        } else if (address4.length() > 0) {
                            parameters.put("accountingInfoLine2", address4);
                        }
                        if (phone.length() > 0 && mobile.length() > 0) {
                            parameters.put("accountingInfoLine3", "Ph:" + phone + "," + "Mobile:" + mobile);
                        } else if (phone != null && phone.length() > 0) {
                            parameters.put("accountingInfoLine3", "Ph:" + phone);
                        } else if (mobile != null && mobile.length() > 0) {
                            parameters.put("accountingInfoLine3", "Mobile:" + mobile);
                        } else {
                            parameters.put("accountingInfoLine3", "");
                        }
                    }

                    if (consolEntity.getDirectShipment() != null && (consolEntity.getDirectShipment().equals(YesNo.Yes))) {
                        if (consolDocumentEntity.getShipper() != null && consolDocumentEntity.getShipper().getPartyDetail() != null) {
                            if (consolDocumentEntity.getShipper().getPartyDetail().getSpotNo() != null) {
                                spotNo = "Spot No:" + consolDocumentEntity.getShipper().getPartyDetail().getSpotNo();
                            }
                        }
                    } else if (consolEntity.getDirectShipment() != null && (consolEntity.getDirectShipment().equals(YesNo.No))) {
                        if (consolDocumentEntity.getAgent() != null && consolDocumentEntity.getAgent().getPartyDetail() != null) {
                            if (consolDocumentEntity.getAgent().getPartyDetail().getSpotNo() != null) {
                                spotNo = "Spot No:" + consolDocumentEntity.getAgent().getPartyDetail().getSpotNo();
                            }
                        }
                    }

                    String tsaNo = "";
                    if (consolEntity.getConsolDocument() != null) {
                        if (consolEntity.getConsolDocument().getIssuingAgent() != null) {
                            List<PartyAddressMaster> pdm = partyAddressMasterRepository.getAll(consolEntity.getConsolDocument().getIssuingAgent().getId());
                            for (PartyAddressMaster pd : pdm) {
                                if (pd.getPartyCountryField() != null && pd.getPartyCountryField().getKnownShipperValidationNo() != null &&
                                        pd.getPartyCountryField().getKnownShipperValidationDate() != null) {

                                    Date dt1 = pd.getPartyCountryField().getKnownShipperValidationDate();
                                    log.info("ShipperValidatorDate:::" + dt1);
                                    Date dt2 = com.efreightsuite.util.TimeUtil.getCurrentLocationTime(consolEntity.getLocation());
                                    log.info("ShipperValidatorDate:::" + dt2);
                                    if (dt1.compareTo(dt2) > 0) {
                                        tsaNo = "TSA No:" + pd.getPartyCountryField().getKnownShipperValidationNo();
                                    }
                                }
                            }
                        }
                    }

                    parameters.put("frieghtTsa", freightName + "\t" + spotNo + "\t" + tsaNo);

                    if (consolEntity.getConsolDocument() != null && consolEntity.getConsolDocument().getIsAgreed() != null &&
                            consolEntity.getConsolDocument().getIsAgreed().equals(YesNo.Yes)) {
                        if (consolEntity.getPpcc().equals(PPCC.Collect)) {
                            parameters.put("weightChargeAsAgreed", "AS AGREED");
                            parameters.put("dueAsAgreed", "");
                            parameters.put("dueCarrierAgent", "");
                            parameters.put("totalCollectAsAgreed", "AS AGREED");
                        } else {
                            parameters.put("weightChargeTotalAmount", "AS AGREED");
                            parameters.put("dueAgent", "");
                            parameters.put("dueCarrier", "");
                            parameters.put("totalPrepaidAmount", "AS AGREED");
                        }
                        parameters.put("total", "AS AGREED");
                        parameters.put("ratePerCharge", "");
                    } else {
                        if (consolEntity.getIataRate() != null && consolDocumentEntity.getChargebleWeight() != null) {
                            Double ratePerCharge = consolEntity.getIataRate();
                            Double totalChargeableWeight = consolDocumentEntity.getChargebleWeight() * ratePerCharge;
                            parameters.put("ratePerCharge", AppUtil.getCurrencyFormat(ratePerCharge, consolEntity.getCountry().getLocale(), consolEntity.getCurrency().getDecimalPoint()));
                            parameters.put("total", AppUtil.getCurrencyFormat(totalChargeableWeight, consolEntity.getCountry().getLocale(), consolEntity.getCurrency().getDecimalPoint()));

                            if (consolEntity.getPpcc().equals(PPCC.Collect)) {
                                parameters.put("weightChargeAsAgreed", AppUtil.getCurrencyFormat(totalChargeableWeight, consolEntity.getCountry().getLocale(), consolEntity.getCurrency().getDecimalPoint()));
                            } else {
                                parameters.put("weightChargeTotalAmount", AppUtil.getCurrencyFormat(totalChargeableWeight, consolEntity.getCountry().getLocale(), consolEntity.getCurrency().getDecimalPoint()));
                            }

                            if (consolEntity.getChargeList() != null && !consolEntity.getChargeList().isEmpty()) {
                                Double totalPrepaidAmount = 0.0;

                                Double preapidDueAmount = 0.0;
                                Double preapidCarrierAmount = 0.0;
                                Double collectDueAmount = 0.0;
                                Double collectCarrierAmount = 0.0;

                                for (ConsolCharge scharge : consolEntity.getChargeList()) {
                                    if (scharge.getPpcc() != null && scharge.getPpcc().equals(PPCC.Collect)) {

                                        if (scharge.getDue() != null && (scharge.getDue().equals(DUE.Agent))) {

                                            collectDueAmount = collectDueAmount + scharge.getLocalAmount();

                                        } else if (scharge.getDue() != null && (scharge.getDue().equals(DUE.Carrier))) {

                                            collectCarrierAmount = collectCarrierAmount + scharge.getLocalAmount();
                                        }

                                    } else if (scharge.getPpcc() != null && scharge.getPpcc().equals(PPCC.Prepaid)) {
                                        if (scharge.getDue() != null && (scharge.getDue().equals(DUE.Agent))) {

                                            preapidDueAmount = preapidDueAmount + scharge.getLocalAmount();

                                        } else if (scharge.getDue() != null && (scharge.getDue().equals(DUE.Carrier))) {

                                            preapidCarrierAmount = preapidCarrierAmount + scharge.getLocalAmount();

                                        }

                                    }
                                }

                                parameters.put("dueAgent", AppUtil.getCurrencyFormat(preapidDueAmount, consolEntity.getLocation().getCountryMaster().getLocale(), consolEntity.getCurrency().getDecimalPoint()));
                                parameters.put("dueAsAgreed", AppUtil.getCurrencyFormat(collectDueAmount, consolEntity.getCountry().getLocale(), consolEntity.getCurrency().getDecimalPoint()));
                                parameters.put("dueCarrier", AppUtil.getCurrencyFormat(preapidCarrierAmount, consolEntity.getCountry().getLocale(), consolEntity.getCurrency().getDecimalPoint()));
                                parameters.put("dueCarrierAgent", AppUtil.getCurrencyFormat(collectCarrierAmount, consolEntity.getCountry().getLocale(), consolEntity.getCurrency().getDecimalPoint()));


                                Double totalCollectAsAgreed = 0.0;
                                //collect deue agent and carrier

                                if (consolEntity.getPpcc().equals(PPCC.Collect)) {
                                    totalCollectAsAgreed = collectDueAmount + collectCarrierAmount + totalChargeableWeight;
                                    totalPrepaidAmount = preapidDueAmount + preapidCarrierAmount;
                                } else {
                                    totalPrepaidAmount = preapidDueAmount + preapidCarrierAmount + totalChargeableWeight;
                                    totalCollectAsAgreed = collectDueAmount + collectCarrierAmount;
                                }
                                parameters.put("totalCollectAsAgreed", AppUtil.getCurrencyFormat(totalCollectAsAgreed, consolEntity.getCountry().getLocale(), consolEntity.getCurrency().getDecimalPoint()));
                                parameters.put("totalPrepaidAmount", AppUtil.getCurrencyFormat(totalPrepaidAmount, consolEntity.getCountry().getLocale(), consolEntity.getCurrency().getDecimalPoint()));
                            }

                        }
                    }
                    List<ReportDocumentDimensionDto> docDimensionList = new ArrayList<>();

                    int list1Size = 0;
                    if (consolDocumentEntity.getDimensionList() != null && !consolDocumentEntity.getDimensionList().isEmpty()) {
                        for (ConsolDocumentDimension ssd : consolDocumentEntity.getDimensionList()) {
                            String dim = ssd.getNoOfPiece() + "   -    " + ssd.getLength() + "    x    " + ssd.getWidth() + "    x    " + ssd.getHeight();
                            switch (list1Size) {
                                case 0:
                                    parameters.put("dimension1", dim);
                                    break;
                                case 1:
                                    parameters.put("dimension2", dim);
                                    break;
                                case 2:
                                    parameters.put("dimension3", dim);
                                    break;
                                case 3:
                                    parameters.put("dimension4", dim);
                                    break;
                                case 4:
                                    parameters.put("dimension5", dim);
                                    break;
                                case 5:
                                    parameters.put("dimension6", dim);
                                    break;
                                case 6:
                                    parameters.put("dimension7", dim);
                                    break;
                                case 7:
                            }
                            if (list1Size >= 7) {
                                ReportDocumentDimensionDto docDimension = new ReportDocumentDimensionDto();
                                docDimension.setHeight(ssd.getHeight());
                                docDimension.setLength(ssd.getLength());
                                docDimension.setNoOfPiece(ssd.getNoOfPiece());
                                docDimension.setWidth(ssd.getWidth());
                                docDimensionList.add(docDimension);
                                secondPageNeeded = true;
                            }
                            list1Size++;
                        }

                    }
                    List<PrepaidCharges> chargesList = new ArrayList<>();
                    List<PrepaidCharges> secondList = new ArrayList<>();
                    List<PrepaidCharges> chargeListRider = new ArrayList<>();
                    List<PrepaidCharges> chargeListRiderAll = new ArrayList<>();

                    String rateMerged = appUtil.getLocationConfig("booking.rates.merged", consolEntity.getLocation(), true);

                    if (rateMerged != null) {
                        if (rateMerged.equals("FALSE") || rateMerged == "FALSE") {
                            if (consolEntity.getChargeList() != null && !consolEntity.getChargeList().isEmpty()) {
                                for (ConsolCharge consolCharge : consolEntity.getChargeList()) {
                                    PrepaidCharges rateSaleCharges = new PrepaidCharges();
                                    if (consolCharge != null) {
                                        if (consolCharge.getChargeMaster() != null) {
                                            rateSaleCharges.setChargeName(consolCharge.getChargeMaster().getChargeCode());
                                        }
                                        if (consolCharge.getLocalAmount() != null) {
                                            rateSaleCharges.setTotalLocalAmount(AppUtil.getCurrencyFormat(consolCharge.getLocalAmount(), consolEntity.getCountry().getLocale(), consolEntity.getCurrency().getDecimalPoint()));
                                        }

                                        chargesList.add(rateSaleCharges);
                                    }
                                }
                            }
                        } else {
                            if (consolEntity.getChargeList() != null && !consolEntity.getChargeList().isEmpty()) {
                                for (ConsolCharge consolCharge : consolEntity.getChargeList()) {
                                    PrepaidCharges rateSaleCharges = new PrepaidCharges();
                                    if (consolCharge != null) {
                                        if (consolCharge.getChargeMaster() != null) {
                                            rateSaleCharges.setChargeName(consolCharge.getChargeMaster().getChargeCode());
                                        }
                                        if (consolCharge.getLocalAmount() != null) {
                                            rateSaleCharges.setTotalLocalAmount(AppUtil.getCurrencyFormat(consolCharge.getLocalAmount(), consolEntity.getCountry().getLocale(), consolEntity.getCurrency().getDecimalPoint()));
                                        }

                                        chargesList.add(rateSaleCharges);
                                    }
                                }
                            }
                        }
                    }
                    if (chargesList != null && chargesList.size() > 0) {
                        for (int j = 0; j < chargesList.size(); j++) {
                            if (j <= 3) {
                                chargeListRiderAll.add(chargesList.get(j));
                            } else if (j >= 4 && j <= 7) {
                                secondList.add(chargesList.get(j));
                            } else {
                                chargeListRider.add(chargesList.get(j));
                                if (consolEntity.getConsolDocument() != null && consolEntity.getConsolDocument().getIsAgreed() != null &&
                                        consolEntity.getConsolDocument().getIsAgreed().equals(YesNo.Yes)) {
                                    secondPageNeeded = false;
                                    parameters1.put("riderChargesList", null);
                                } else {
                                    secondPageNeeded = true;
                                    parameters1.put("riderChargesList", chargeListRider);
                                }
                            }
                        }
                    }

                    if (consolEntity.getConsolDocument() != null && consolEntity.getConsolDocument().getIsAgreed() != null &&
                            consolEntity.getConsolDocument().getIsAgreed().equals(YesNo.Yes)) {
                        parameters.put("chargesSecondList", null);
                        parameters.put("chargesList", null);
                    } else {
                        parameters.put("chargesSecondList", secondList);
                        parameters.put("chargesList", chargeListRiderAll);
                    }

                    parameters1.put("dimensionList", docDimensionList);


                    JasperReport jasperReport = JasperCompileManager.compileReport(appResourcePath + "/efs-report/mawb.jrxml");

                    List<Object> fieldCollection = new ArrayList<>();
                    Object obj = new Object();
                    fieldCollection.add(obj);
                    JRDataSource dataSource = new JRBeanCollectionDataSource(fieldCollection, false);
                    JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, dataSource);
                    log.info("Rider Page is available ?? " + secondPageNeeded);
                    if (secondPageNeeded) {
                        JasperReport jasperReport1 = JasperCompileManager.compileReport(appResourcePath + "/efs-report/mawb_rider.jrxml");
                        List<Object> fieldCollection1 = new ArrayList<>();
                        Object obj1 = new Object();
                        fieldCollection1.add(obj1);
                        JRDataSource dataSource1 = new JRBeanCollectionDataSource(hawbRiderList, false);
                        JasperPrint jasperPrint1 = JasperFillManager.fillReport(jasperReport1, parameters1, dataSource1);
                        List<JRPrintPage> pages = jasperPrint1.getPages();
                        for (int count = 0; count <
                                pages.size(); count++) {
                            jasperPrint.addPage(pages.get(count));
                        }
                    }

                    return jasperPrint;
                }
            }
        } catch (JRException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;

    }

    public Map<String, String> AddressMasterToMap(String parameterName, AddressMaster addressMaster) {
        Map<String, String> parameter = new HashMap<>();
        int index = 1;
        if (addressMaster != null) {
            if (addressMaster.getAddressLine1() != null && addressMaster.getAddressLine1().trim().length() > 0) {
                parameter.put(parameterName + index, addressMaster.getAddressLine1());
                index++;
            }
            if (addressMaster.getAddressLine2() != null && addressMaster.getAddressLine2().trim().length() > 0) {
                parameter.put(parameterName + index, addressMaster.getAddressLine2());
                index++;
            }
            if (addressMaster.getAddressLine3() != null && addressMaster.getAddressLine3().trim().length() > 0) {
                parameter.put(parameterName + index, addressMaster.getAddressLine3());
                index++;
            }
            if (addressMaster.getAddressLine4() != null && addressMaster.getAddressLine4().trim().length() > 0) {
                parameter.put(parameterName + index, addressMaster.getAddressLine4());
                index++;
            }
        }
        return parameter;
    }
}
