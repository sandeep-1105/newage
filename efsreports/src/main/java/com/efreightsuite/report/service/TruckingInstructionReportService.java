package com.efreightsuite.report.service;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletResponse;

import com.efreightsuite.dto.AutoAttachmentDto;
import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.ReportDownloadRequestDto;
import com.efreightsuite.enumeration.AddressType;
import com.efreightsuite.enumeration.ReportConfig;
import com.efreightsuite.enumeration.ReportDownloadOption;
import com.efreightsuite.enumeration.ReportName;
import com.efreightsuite.enumeration.YesNo;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.model.DocumentDetail;
import com.efreightsuite.model.LocationMaster;
import com.efreightsuite.model.LogoMaster;
import com.efreightsuite.model.PartyAddressMaster;
import com.efreightsuite.model.PickUpDeliveryPoint;
import com.efreightsuite.model.ReportConfigurationMaster;
import com.efreightsuite.model.ServiceDocumentDimension;
import com.efreightsuite.model.ShipmentServiceDetail;
import com.efreightsuite.model.UserProfile;
import com.efreightsuite.report.ShippingInstruction;
import com.efreightsuite.service.AuthService;
import com.efreightsuite.util.AppUtil;
import lombok.extern.log4j.Log4j2;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

import org.springframework.stereotype.Service;

@Service
@Log4j2
public class TruckingInstructionReportService extends AbstractReportService {
    public ReportName getReportType() {
        return ReportName.TRUCKING_INSTRUCTION;
    }

    public byte[] processReport(ReportDownloadRequestDto data, HttpServletResponse response, boolean isMultiple) {
        log.info("I am in TruckingInstructionReportService..");
        BaseDto baseDto = new BaseDto();
        UserProfile logInUSer = AuthService.getCurrentUser();
        byte[] a = null;
        JasperPrint jasperPrint = getReport(baseDto, data, AuthService.getCurrentUser());
        AutoAttachmentDto autoAttachmentDto = reportUtil.autoAttachment(ReportName.TRUCKING_INSTRUCTION, YesNo.Yes, data.getResourceId(), "Shipment");
        if (data.getSingle() != null && data.getSingle() == true) {
            if (data.getDownloadOption() == ReportDownloadOption.Preview || data.getDownloadOption() == ReportDownloadOption.Print) {
                reportUtil.japerPrintToResponseOutputStream(data.getDownloadFileType(), jasperPrint, "TRUCKING_INSTRUCTION", response, false, null);
            } else if (autoAttachmentDto != null && autoAttachmentDto.getIsAttach() != null || data.getDownloadOption() == ReportDownloadOption.Download) {
                reportUtil.japerPrintToResponseOutputStream(data.getDownloadFileType(), jasperPrint, "TRUCKING_INSTRUCTION", response, false, autoAttachmentDto);
            }
        }
        if (data.getSingle() != null && data.getSingle() == true && data.getDownloadOption() == ReportDownloadOption.eMail) {
            a = reportUtil.japerPrintToResponseOutputStream(data.getDownloadFileType(), jasperPrint, "TRUCKING_INSTRUCTION", response, false, null);
            shipmentReportMailer.sendShipmentMailToCustomer(data.getResourceId(), logInUSer, ReportName.TRUCKING_INSTRUCTION, a);
        }
        if (isMultiple == true && data.getDownloadOption() == ReportDownloadOption.eMail) {
            a = reportUtil.japerPrintToResponseOutputStream(data.getDownloadFileType(), jasperPrint, "TRUCKING_INSTRUCTION", response, true, null);
            return a;
        }
        return null;
    }

    private JasperPrint getReport(BaseDto baseDto, ReportDownloadRequestDto data, UserProfile loggedInUser) {
        log.info("I am in TruckingInstructionReportService..");
        DocumentDetail document = documentDetailRepository.findById(data.getResourceId());
        Map<String, Object> parameters = new HashMap<>();
        try {
            String loggedInUserName = "";
            String loggedInLocation = "";
            String companyName = "";
            if (loggedInUser != null) {
                if (loggedInUser.getEmployee() != null && loggedInUser.getEmployee().getEmployeeName() != null) {
                    loggedInUserName = loggedInUser.getEmployee().getEmployeeName();
                } else if (loggedInUser.getUserName() != null) {
                    loggedInUserName = loggedInUser.getUserName();
                } else {
                    loggedInUserName = "";
                }
                if (loggedInUser.getSelectedUserLocation() != null) {
                    LogoMaster logoMaster = logoMasterRepository.findByLocationMaster(loggedInUser.getSelectedUserLocation());   //getting logo
                    try {
                        if (logoMaster != null && logoMaster.getLogo() != null) {
                            InputStream in = new ByteArrayInputStream(logoMaster.getLogo());
                            BufferedImage bImageFromConvert = ImageIO.read(in);
                            parameters.put("logo", bImageFromConvert);
                            log.info("Image getting success::");
                        }
                    } catch (Exception ex) {
                        log.error("Image getting failed:::", ex);
                    }
                    loggedInLocation = loggedInUser.getSelectedUserLocation().getLocationName() != null
                            ? loggedInUser.getSelectedUserLocation().getLocationName() : "";
                    LocationMaster locationMaster = locationMasterRepository.findByLocationCode(loggedInUser.getSelectedUserLocation().getLocationCode());
                    if (locationMaster != null) {
                        parameters.put("companyAddressLine1", locationMaster.getAddressLine1());
                        parameters.put("companyAddressLine2", locationMaster.getAddressLine2());
                        parameters.put("companyAddressLine3", locationMaster.getAddressLine3());

                        if (locationMaster.getBranchName() != null) {
                            companyName = locationMaster.getBranchName();
                            parameters.put("companyName", reportUtil.removeSpecialCharacters(companyName, data.getDownloadOption()));
                        }
                    }
                    parameters.put("refDate", reportUtil.dateToReportDate(new Date(), loggedInUser.getSelectedUserLocation().getJavaDateFormat()));  //today date
                    parameters.put("generateDate", reportUtil.dateToReportDate(new Date(), loggedInUser.getSelectedUserLocation().getJavaDateFormat())); // today date it is also report generated date

                } else {
                    loggedInLocation = "";
                }
            }
            parameters.put("refPlace", loggedInUserName + "-" + loggedInLocation);

            if (document != null) {
                ShipmentServiceDetail shipmentServie = documentDetailRepository.findByIdForShipmentServiceDetail(document.getId());  //Getting shipmentService Object from document ID

                if (shipmentServie != null) {
                    String pack = AppUtil.getNumberFormat(document.getNoOfPieces(), shipmentServie.getCountry().getLocale());  //noOfPieces
                    String packCode = "";
                    if (document.getPackMaster() != null && document.getPackMaster().getPackName() != null) {
                        packCode = document.getPackMaster().getPackName();        //packName
                    }
                    String bothCode = pack + " " + packCode;
                    parameters.put("noOfpieces", bothCode);
                    if (document.getCommodityDescription() != null) {
                        parameters.put("commodityDescription", document.getCommodityDescription());
                    }
                    if (shipmentServie.getBookedVolumeUnitCbm() != null) {
                        parameters.put("cftcbmValue", AppUtil.getWeightFormat(shipmentServie.getBookedVolumeUnitCbm(), shipmentServie.getCountry().getLocale()));
                    }
                    if (shipmentServie.getBookedGrossWeightUnitPound() != null && shipmentServie.getBookedGrossWeightUnitKg() != null) {
                        String bookedGrossWeightUnitPound = AppUtil.getWeightFormat(shipmentServie.getBookedGrossWeightUnitPound(), shipmentServie.getCountry().getLocale());
                        String bookedGrossWeightUnitKg = AppUtil.getWeightFormat(shipmentServie.getBookedGrossWeightUnitKg(), shipmentServie.getCountry().getLocale());
                        parameters.put("kgsLbsValue", bookedGrossWeightUnitKg + " / " + bookedGrossWeightUnitPound);
                    }

                    if (shipmentServie.getCarrier() != null) {
                        parameters.put("carrierName", shipmentServie.getCarrier().getCarrierName());
                    }
                    parameters.put("flightNo", shipmentServie.getRouteNo()); // flightNo or routeNo
                    parameters.put("referenceNumber", shipmentServie.getShipmentUid()); // reference Number


                    List<ShippingInstruction> dimensionList = new ArrayList<>();
                    Long totalNoOfPieces = 0L;
                    Double totalVolumeWeight = 0.0;
                    //Getting all dimension details from Shipment
                    if (shipmentServie.getDocumentList() != null && !shipmentServie.getDocumentList().isEmpty()) {
                        for (DocumentDetail documentDetail : shipmentServie.getDocumentList()) {
                            if (documentDetail.getDimensionList() != null && !documentDetail.getDimensionList().isEmpty()) {
                                for (ServiceDocumentDimension dimesionList : document.getDimensionList()) {
                                    ShippingInstruction dimensionDto = new ShippingInstruction();
                                    if (dimesionList != null) {
                                        dimensionDto.setHeight(AppUtil.getNumberFormat(dimesionList.getHeight(), shipmentServie.getCountry().getLocale()));
                                        dimensionDto.setLength(AppUtil.getNumberFormat(dimesionList.getLength(), shipmentServie.getCountry().getLocale()));
                                        dimensionDto.setWidth(AppUtil.getNumberFormat(dimesionList.getWidth(), shipmentServie.getCountry().getLocale()));
                                        Long piece = Long.valueOf(dimesionList.getNoOfPiece());
                                        dimensionDto.setPieces(AppUtil.getNumberFormat(dimesionList.getNoOfPiece(), shipmentServie.getCountry().getLocale()));
                                        totalNoOfPieces = totalNoOfPieces + piece;
                                        Double dimensionVolume = 0.0;
                                        dimensionVolume = dimesionList.getVolWeight();
                                        totalVolumeWeight = totalVolumeWeight + dimensionVolume;
                                        dimensionDto.setVolume(AppUtil.getWeightFormat(dimensionVolume, shipmentServie.getCountry().getLocale()));
                                    }
                                    dimensionList.add(dimensionDto);    //Adding dimension Object
                                }
                            }
                        }
                    }
                    parameters.put("totalNoOfPieces", AppUtil.getNumberFormat(totalNoOfPieces, shipmentServie.getCountry().getLocale()));
                    parameters.put("totalVolumeWeight", AppUtil.getWeightFormat(totalVolumeWeight, shipmentServie.getCountry().getLocale()));
                    parameters.put("dimensionList", dimensionList);    //showing Dimension list valuess in jasper report


                    if (shipmentServie.getPickUpDeliveryPoint() != null) {
                        PickUpDeliveryPoint pickUpDeliveryPoint = shipmentServie.getPickUpDeliveryPoint();
                        if (pickUpDeliveryPoint != null) {
                            if (pickUpDeliveryPoint.getTransporter() != null) {
                                parameters.put("transporterName", reportUtil.removeSpecialCharacters(pickUpDeliveryPoint.getTransporter().getPartyName(), data.getDownloadOption()));  //transporter name or partyname
                                for (PartyAddressMaster partyAddressMaster : pickUpDeliveryPoint.getTransporter().getPartyAddressList()) {
                                    PartyAddressMaster partyAddressMasterObj = partyAddressMaster;                //getting corresponding partyaddress
                                    if (partyAddressMasterObj.getAddressType().equals(AddressType.Primary)) {  //primary Address
                                        parameters.put("faxNumber", partyAddressMasterObj.getFax());
                                        parameters.put("telephoneNumber", partyAddressMasterObj.getPhone());
                                        parameters.put("attentionName", partyAddressMasterObj.getContactPerson());

                                        parameters.put("transportAddressLine1", partyAddressMasterObj.getAddressLine1());
                                        parameters.put("transportAddressLine2", partyAddressMasterObj.getAddressLine2());
                                        parameters.put("transportAddressLine3", partyAddressMasterObj.getAddressLine3());
                                    } else if (partyAddressMasterObj.getAddressType().equals(AddressType.Billing)) {  //billing Address
                                        parameters.put("faxNumber", partyAddressMasterObj.getFax());
                                        parameters.put("telephoneNumber", partyAddressMasterObj.getPhone());
                                        parameters.put("attentionName", partyAddressMasterObj.getContactPerson());
                                        parameters.put("transportAddressLine1", partyAddressMasterObj.getAddressLine1());
                                        parameters.put("transportAddressLine2", partyAddressMasterObj.getAddressLine2());
                                        parameters.put("transportAddressLine3", partyAddressMasterObj.getAddressLine3());
                                    } else if (partyAddressMasterObj.getAddressType().equals(AddressType.Other)) {     //other Address
                                        parameters.put("faxNumber", partyAddressMasterObj.getFax());
                                        parameters.put("telephoneNumber", partyAddressMasterObj.getPhone());
                                        parameters.put("attentionName", partyAddressMasterObj.getContactPerson());
                                        parameters.put("transportAddressLine1", partyAddressMasterObj.getAddressLine1());
                                        parameters.put("transportAddressLine2", partyAddressMasterObj.getAddressLine2());
                                        parameters.put("transportAddressLine3", partyAddressMasterObj.getAddressLine3());
                                    }
                                }
                            }

                            if (pickUpDeliveryPoint.getPickupPoint() != null) {
                                parameters.put("pickUpFromName", reportUtil.removeSpecialCharacters(pickUpDeliveryPoint.getPickupPoint().getPartyName(), data.getDownloadOption()));        // pickupname and corresponding details
                            }
                            if (pickUpDeliveryPoint.getPickUpPlace() != null) {
                                String addressLine1 = pickUpDeliveryPoint.getPickUpPlace().getAddressLine1() != null ? pickUpDeliveryPoint.getPickUpPlace().getAddressLine1() : "";
                                String addressLine2 = pickUpDeliveryPoint.getPickUpPlace().getAddressLine2() != null ? pickUpDeliveryPoint.getPickUpPlace().getAddressLine2() : "";
                                String addressLine3 = pickUpDeliveryPoint.getPickUpPlace().getAddressLine3() != null ? pickUpDeliveryPoint.getPickUpPlace().getAddressLine3() : "";
                                String zipCode = pickUpDeliveryPoint.getPickUpPlace().getZipCode() != null ? pickUpDeliveryPoint.getPickUpPlace().getZipCode() : "";

                                String state = "";
                                String city = "";
                                if (pickUpDeliveryPoint.getPickUpPlace().getState() != null) {
                                    state = pickUpDeliveryPoint.getPickUpPlace().getState().getStateName() != null ? pickUpDeliveryPoint.getPickUpPlace().getState().getStateName() : "";
                                }
                                if (pickUpDeliveryPoint.getPickUpPlace().getCity() != null) {
                                    city = pickUpDeliveryPoint.getPickUpPlace().getCity().getCityName() != null ? pickUpDeliveryPoint.getPickUpPlace().getCity().getCityName() : "";
                                }
                                String sb = new StringBuilder().append(addressLine1).append(",").append(addressLine2).append(",").append(addressLine3)
                                        .append("\n").append(state).append(",").append(city).append(",").append(zipCode).toString();
                                parameters.put("pickUpAddressLine1", sb);
                            }
                            parameters.put("pickUpContactName", pickUpDeliveryPoint.getPickUpContactPerson() != null ? pickUpDeliveryPoint.getPickUpContactPerson() : "");
                            parameters.put("pickUpTeleNumber", pickUpDeliveryPoint.getPickUpPhoneNo() != null ? pickUpDeliveryPoint.getPickUpPhoneNo() : "");
                            parameters.put("pickUpMobileNumber", pickUpDeliveryPoint.getPickUpMobileNo() != null ? pickUpDeliveryPoint.getPickUpMobileNo() : "");
                            parameters.put("pickUpEmail", pickUpDeliveryPoint.getPickUpEmail() != null ? pickUpDeliveryPoint.getPickUpEmail() : "");

                            if (pickUpDeliveryPoint.getDeliveryPoint() != null) {
                                parameters.put("deliveryToName", reportUtil.removeSpecialCharacters(pickUpDeliveryPoint.getDeliveryPoint().getCfsName(), data.getDownloadOption()));     //delivery name and corresponding details
                            }

                            if (pickUpDeliveryPoint.getDeliveryPlace() != null) {
                                String addressLine1 = pickUpDeliveryPoint.getDeliveryPlace().getAddressLine1() != null ? pickUpDeliveryPoint.getDeliveryPlace().getAddressLine1() : "";
                                String addressLine2 = pickUpDeliveryPoint.getDeliveryPlace().getAddressLine2() != null ? pickUpDeliveryPoint.getDeliveryPlace().getAddressLine2() : "";
                                String addressLine3 = pickUpDeliveryPoint.getDeliveryPlace().getAddressLine3() != null ? pickUpDeliveryPoint.getDeliveryPlace().getAddressLine3() : "";
                                String zipCode = pickUpDeliveryPoint.getDeliveryPlace().getZipCode() != null ? pickUpDeliveryPoint.getDeliveryPlace().getZipCode() : "";

                                String state = "";
                                String city = "";
                                if (pickUpDeliveryPoint.getDeliveryPlace().getState() != null) {
                                    state = pickUpDeliveryPoint.getDeliveryPlace().getState().getStateName() != null ? pickUpDeliveryPoint.getDeliveryPlace().getState().getStateName() : "";
                                }
                                if (pickUpDeliveryPoint.getDeliveryPlace().getCity() != null) {
                                    city = pickUpDeliveryPoint.getDeliveryPlace().getCity().getCityName() != null ? pickUpDeliveryPoint.getDeliveryPlace().getCity().getCityName() : "";
                                }
                                String sb = new StringBuilder().append(addressLine1).append(",").append(addressLine2).append(",").append(addressLine3)
                                        .append("\n").append(state).append(",").append(city).append(",").append(zipCode).toString();
                                parameters.put("deliveryToAddressLine1", sb);
                            }
                            parameters.put("deliveryToContactName", pickUpDeliveryPoint.getDeliveryContactPerson() != null ? pickUpDeliveryPoint.getDeliveryContactPerson() : "");
                            parameters.put("deliveryToTeleNumber", pickUpDeliveryPoint.getDeliveryPhoneNo() != null ? pickUpDeliveryPoint.getDeliveryPhoneNo() : "");
                            parameters.put("deliveryToMobileNumber", pickUpDeliveryPoint.getDeliveryMobileNo() != null ? pickUpDeliveryPoint.getDeliveryMobileNo() : "");
                            parameters.put("deliveryToEmail", pickUpDeliveryPoint.getDeliveryEmail() != null ? pickUpDeliveryPoint.getDeliveryEmail() : "");

                            if (pickUpDeliveryPoint.getPickUpActual() != null) {
                                parameters.put("loadingDate", reportUtil.dateToReportDate(pickUpDeliveryPoint.getPickUpActual(), loggedInUser.getSelectedUserLocation().getJavaDateFormat())); //Pickup actuall date is loading date
                            }
                            if (pickUpDeliveryPoint.getDeliveryActual() != null) {
                                parameters.put("portCutOffDate", reportUtil.dateToReportDate(pickUpDeliveryPoint.getDeliveryActual(), loggedInUser.getSelectedUserLocation().getJavaDateFormat())); //delivery Actuall date is portutoff date
                            }
                        }
                    }
                }

                ReportConfigurationMaster tmpObj = reportConfigurationMasterRepository.getByKeyAndLocation(ReportConfig.PICKUP_DELIVERY_FOOTER, shipmentServie.getLocation().getId());
                if (tmpObj != null && tmpObj.getReportValue() != null) {
                    parameters.put("pickUpDeliveryFooter", tmpObj.getReportValue());
                }
            }

            parameters.put("reportName", "TRUCKING INSTRUCTION");          //reportName
            JasperReport jasperReport = JasperCompileManager.compileReport(appResourcePath + "/efs-report/trucking_instruction_new_format.jrxml");
            List<Object> fieldCollection = new ArrayList<>();
            Object obj1 = new Object();
            fieldCollection.add(obj1);
            JRDataSource dataSource = new JRBeanCollectionDataSource(fieldCollection, false);
            return JasperFillManager.fillReport(jasperReport, parameters, dataSource);

        } catch (RestException re) {
            log.error("RestException occures in Trucking Instruction  : ", re);

        } catch (Exception exception) {
            log.error("RestException occures in Trucking Instruction :: ", exception);
            baseDto.setResponseCode(ErrorCode.FAILED);
        }
        return null;

    }
}
