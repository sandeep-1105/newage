package com.efreightsuite.report.service;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletResponse;

import com.efreightsuite.dto.AutoAttachmentDto;
import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.ReportDownloadRequestDto;
import com.efreightsuite.enumeration.PPCC;
import com.efreightsuite.enumeration.ReportConfig;
import com.efreightsuite.enumeration.ReportDownloadOption;
import com.efreightsuite.enumeration.ReportName;
import com.efreightsuite.enumeration.YesNo;
import com.efreightsuite.model.AddressMaster;
import com.efreightsuite.model.DocumentDetail;
import com.efreightsuite.model.LocationMaster;
import com.efreightsuite.model.LogoMaster;
import com.efreightsuite.model.ReportConfigurationMaster;
import com.efreightsuite.model.ShipmentCharge;
import com.efreightsuite.model.ShipmentServiceDetail;
import com.efreightsuite.model.UserProfile;
import com.efreightsuite.report.CertificateOrigin;
import com.efreightsuite.report.FCRChargesData;
import com.efreightsuite.service.AuthService;
import com.efreightsuite.util.AppUtil;
import lombok.extern.log4j.Log4j2;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRPrintPage;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

import org.springframework.stereotype.Service;

@Service
@Log4j2
public class ForwardersCargoReceiptReportService extends AbstractReportService {
    public ReportName getReportType() {
        return ReportName.FORWARDERS_CARGO_RECEIPT;
    }

    public byte[] processReport(ReportDownloadRequestDto data, HttpServletResponse response, boolean isMultiple) {
        log.info("I am in ForwardersCargoReceiptReportService..");
        BaseDto baseDto = new BaseDto();
        UserProfile logInUSer = AuthService.getCurrentUser();
        JasperPrint jasperPrint = getReport(baseDto, data, AuthService.getCurrentUser());

        byte[] a = null;
        AutoAttachmentDto autoAttachmentDto = reportUtil.autoAttachment(ReportName.FORWARDERS_CARGO_RECEIPT, YesNo.Yes, data.getResourceId(), "Shipment");
        if (data.getSingle() != null && data.getSingle() == true) {
            if (data.getDownloadOption() == ReportDownloadOption.Preview || data.getDownloadOption() == ReportDownloadOption.Print) {
                reportUtil.japerPrintToResponseOutputStream(data.getDownloadFileType(), jasperPrint, "FORWARDERS_CARGO_RECEIPT", response, false, null);
            } else if (autoAttachmentDto != null && autoAttachmentDto.getIsAttach() != null || data.getDownloadOption() == ReportDownloadOption.Download) {
                reportUtil.japerPrintToResponseOutputStream(data.getDownloadFileType(), jasperPrint, "FORWARDERS_CARGO_RECEIPT", response, false, autoAttachmentDto);
            }
        }
        if (data.getSingle() != null && data.getSingle() == true && data.getDownloadOption() == ReportDownloadOption.eMail) {
            a = reportUtil.japerPrintToResponseOutputStream(data.getDownloadFileType(), jasperPrint, "FORWARDERS_CARGO_RECEIPT", response, false, null);
            shipmentReportMailer.sendShipmentMailToCustomer(data.getResourceId(), logInUSer, ReportName.FORWARDERS_CARGO_RECEIPT, a);
        }
        if (isMultiple == true && data.getDownloadOption() == ReportDownloadOption.eMail) {
            a = reportUtil.japerPrintToResponseOutputStream(data.getDownloadFileType(), jasperPrint, "FORWARDERS_CARGO_RECEIPT", response, true, null);
            return a;
        }
        return null;
    }

    private JasperPrint getReport(BaseDto baseDto, ReportDownloadRequestDto data, UserProfile loggedInUser) {
        log.info("DownloadFileType : [" + data.getDownloadFileType() + "]; DownloadOption : [" + data.getDownloadOption() + "] ; ResourceId : [" + data.getResourceId() + "ReportName" + data.getReportName() + "]");
        String marksParam1 = "";
        String marksParam2 = "";
        int commoditylengthLine = 0;
        boolean secondPageNeeded = false;
        JasperReport jasperReport;
        try {
            Map<String, Object> parameters = new HashMap<>();
            Map<String, Object> parameter1 = new HashMap<>();


            String companyDetail1 = "";
            if (loggedInUser != null && loggedInUser.getSelectedUserLocation() != null) {
                if (loggedInUser.getSelectedUserLocation() != null) {
                    parameters.put("issuePlace", loggedInUser.getSelectedUserLocation().getLocationName() != null ? loggedInUser.getSelectedUserLocation().getLocationName() : "");
                    LocationMaster locationMaster = locationMasterRepository.findByLocationCode(loggedInUser.getSelectedUserLocation().getLocationCode());
                    if (locationMaster != null) {
                        parameters.put("companyDetail2", locationMaster.getAddressLine1() != null ? locationMaster.getAddressLine1() : "");
                        parameters.put("companyDetail3", locationMaster.getAddressLine2() != null ? locationMaster.getAddressLine2() : "");
                        parameters.put("companyDetail4", locationMaster.getAddressLine3() != null ? locationMaster.getAddressLine3() : "");
                        companyDetail1 = locationMaster.getBranchName() != null ? locationMaster.getBranchName() : "";
                        parameters.put("companyDetail1", reportUtil.removeSpecialCharacters(companyDetail1, data.getDownloadOption()));


                        {
                            ReportConfigurationMaster tmpObj = reportConfigurationMasterRepository.getByKeyAndLocation(ReportConfig.MAWB_COPIES_DECLARATION, locationMaster.getId());
                            if (tmpObj != null && tmpObj.getReportValue() != null) {
                                parameters.put("mawbCopiesDeclaration", tmpObj.getReportValue());
                            }
                        }

                        {
                            ReportConfigurationMaster tmpObj = reportConfigurationMasterRepository.getByKeyAndLocation(ReportConfig.MAWB_AGREED_GOOD_ORDER_CONDITION, locationMaster.getId());
                            if (tmpObj != null && tmpObj.getReportValue() != null) {
                                parameters.put("mawbAgreedGoodOrderCondition", tmpObj.getReportValue());
                            }
                        }

                        {
                            ReportConfigurationMaster tmpObj = reportConfigurationMasterRepository.getByKeyAndLocation(ReportConfig.MAWB_INSURANCE, locationMaster.getId());
                            if (tmpObj != null && tmpObj.getReportValue() != null) {
                                parameters.put("mawbInsurance", tmpObj.getReportValue());
                            }
                        }

                        {
                            ReportConfigurationMaster tmpObj = reportConfigurationMasterRepository.getByKeyAndLocation(ReportConfig.MAWB_SHIPPER_CERTIFIES_CONSIGNMENT, locationMaster.getId());
                            if (tmpObj != null && tmpObj.getReportValue() != null) {
                                parameters.put("mawbShipperCertifiesConsignment", tmpObj.getReportValue());
                            }
                        }

                    }
                    LogoMaster logoMaster = logoMasterRepository.findByLocationMaster(loggedInUser.getSelectedUserLocation());
                    try {
                        if (logoMaster != null && logoMaster.getLogo() != null) {
                            InputStream in = new ByteArrayInputStream(logoMaster.getLogo());
                            BufferedImage bImageFromConvert = ImageIO.read(in);
                            parameters.put("logo", bImageFromConvert);
                            log.info("Image getting success::");
                        }
                    } catch (Exception ex) {
                        log.error("Image getting failed:::", ex);
                    }
                }
            }


            DocumentDetail document = documentDetailRepository.findById(data.getResourceId());
            List<CertificateOrigin> forwarderCargoList = new ArrayList<>();
            CertificateOrigin forwarderCargorceipt = new CertificateOrigin();
            if (document != null) {
                if (document.getShipper() != null && document.getShipper().getPartyName() != null) {
                    parameters.put("shipperDetail1", reportUtil.removeSpecialCharacters(document.getShipper().getPartyName(), data.getDownloadOption()));
                }
                if (document.getConsignee() != null && document.getConsignee().getPartyName() != null) {
                    parameters.put("consigneeOrOrderDetail1", reportUtil.removeSpecialCharacters(document.getConsignee().getPartyName(), data.getDownloadOption()));
                }
                if (document.getFirstNotify() != null && document.getFirstNotify().getPartyName() != null) {
                    parameters.put("notifyAddressDetail1", reportUtil.removeSpecialCharacters(document.getFirstNotify().getPartyName(), data.getDownloadOption()));
                }
                parameters.putAll(AddressMasterToMap("shipperDetail", document.getShipperAddress(), 2));
                parameters.putAll(AddressMasterToMap("consigneeOrOrderDetail", document.getConsigneeAddress(), 2));
                parameters.putAll(AddressMasterToMap("notifyAddressDetail", document.getFirstNotifyAddress(), 2));
                parameters.put("fcrNo", document.getDocumentNo());
                //parameters.put("marksAndNo", consolDocumentEntity.getConsolDocument().getMarksAndNo());
                parameters.put("flightNo", document.getRouteNo());

                if (document.getMarksAndNo() != null) {
                    int markLength = document.getMarksAndNo().length();
                    if (markLength > 700) {
                        marksParam1 = document.getMarksAndNo().substring(0, 700);
                        marksParam2 = document.getMarksAndNo().substring(700, document.getMarksAndNo().length());
                        secondPageNeeded = true;
                    } else {
                        marksParam1 = document.getMarksAndNo();
                    }
                }
                parameters.put("marksAndNo", marksParam1);
                forwarderCargorceipt.setMarksAndNo(marksParam2);

                if (document.getAgent() != null && document.getAgent().getPartyName() != null) {
                    parameters.put("agentAtDestinationDetail1", reportUtil.removeSpecialCharacters(document.getAgent().getPartyName(), data.getDownloadOption()));
                }
                parameters.putAll(AddressMasterToMap("agentAtDestinationDetail", document.getAgentAddress(), 2));

                if (document.getOrigin() != null) {
                    parameters.put("placeOfReceipt", document.getOrigin().getPortName());
                }
                if (document.getCarrier() != null) {
                    parameters.put("carrierName", document.getCarrier().getCarrierName());
                }
                if (document.getDestination() != null) {
                    parameters.put("placeOfDelivery", document.getDestination().getPortName());
                }
                if (document.getCommodityDescription() != null) {
                    String commodityDescription = document.getCommodityDescription();
                    commoditylengthLine = commodityDescription.length();
                    log.info("Commodity Size:::::::" + commoditylengthLine);
                    if (commoditylengthLine > 0) {
                        commoditylengthLine = commoditylengthLine / 38;
                        log.info("Commodity Size After :::::::" + commoditylengthLine);
                    }
                    if (commoditylengthLine > 10) {
                        parameters.put("commodityDescription", document.getCommodityDescription().substring(0, 380));
                        forwarderCargorceipt.setCommodityGroup(document.getCommodityDescription().substring(380, commodityDescription.length()));
                        //parameters1.put("commodityDescription", documentDetail.getCommodityDescription().substring(200, commodityDescription.length()));
                        secondPageNeeded = true;
                    } else {
                        parameters.put("commodityDescription", document.getCommodityDescription());
                    }
                }
                forwarderCargoList.add(forwarderCargorceipt);
                if (document.getPol() != null) {
                    parameters.put("portOfLoading", document.getPol().getPortName());
                }
                if (document.getPod() != null) {
                    parameters.put("portOfDischarge", document.getPod().getPortName());
                }
                parameters.put("issueDate", reportUtil.dateToReportDate(document.getSystemTrack().getCreateDate(), loggedInUser.getSelectedUserLocation().getJavaDateFormat()));
            }

            ShipmentServiceDetail service = documentDetailRepository.findByIdForShipmentServiceDetail(data.getResourceId());
            if (service != null) {
                String packCode = "";
                if (document != null && document.getPackMaster() != null) {
                    if (document.getPackMaster().getPackName() != null) {
                        packCode = document.getPackMaster().getPackName();
                    }
                }

                if (service.getPpcc() != null && service.getPpcc().equals(PPCC.Prepaid)) {
                    parameters.put("freightCharges", "PREPAID");
                } else {
                    parameters.put("freightCharges", "COLLECT");
                }

                String pieces = "";
                if (document != null) {
                    pieces = document.getNoOfPieces() != null ? AppUtil.getNumberFormat(document.getNoOfPieces(), service.getCountry().getLocale()) : ""; // change noOfPack to noOfPieces
                    String bothCode = pieces + " " + packCode;
                    parameters.put("noOfPieces", bothCode);
                    if (document.getGrossWeight() != null && service.getCountry() != null) {
                        parameters.put("grossWeightKG", AppUtil.getWeightFormat(Double.valueOf(document.getGrossWeight()), service.getCountry().getLocale()));
                    }

                    if (document.getVolumeWeight() != null && service.getCountry() != null) {
                        parameters.put("volumeWeight", AppUtil.getWeightFormat(Double.valueOf(document.getVolumeWeight()), service.getCountry().getLocale()));
                    }
                }

                List<FCRChargesData> chargeList = new ArrayList<>();
                List<FCRChargesData> chargeListRider = new ArrayList<>();
                List<FCRChargesData> chargeListRiderAll = new ArrayList<>();
                if (service.getShipmentChargeList() != null && !service.getShipmentChargeList().isEmpty()) {
                    for (ShipmentCharge sc : service.getShipmentChargeList()) {
                        FCRChargesData fcd = new FCRChargesData();
                        if (sc != null) {
                            if (service.getPpcc().equals(PPCC.Prepaid)) {
                                if (sc.getPpcc().equals(PPCC.Prepaid)) {
                                    fcd.setChargeName(sc.getChargeName());
                                    if(sc.getRateAmount() != null) {
                                        fcd.setPrepaidCharge(String.valueOf(sc.getRateAmount()));
                                    }
                                }
                                if (sc.getPpcc().equals(PPCC.Collect)) {
                                    fcd.setChargeName(sc.getChargeName());
                                    if(sc.getRateAmount() != null) {
                                        fcd.setCollectCharge(String.valueOf(sc.getRateAmount()));
                                    }
                                }
                            }
                        }
                        chargeList.add(fcd);
                    }
                    if (chargeList != null && chargeList.size() > 0) {
                        for (int j = 0; j < chargeList.size(); j++) {
                            if (j <= 9) {
                                chargeListRiderAll.add(chargeList.get(j));
                            } else {
                                chargeListRider.add(chargeList.get(j));
                                secondPageNeeded = true;
                                parameter1.put("chargesListRider", chargeListRider);
                            }

                        }
                    }
                    parameters.put("chargesList", chargeListRiderAll);
                }

            }

            jasperReport = JasperCompileManager.compileReport(appResourcePath + "/efs-report/forwarders_cargo_receipt.jrxml");

            List<Object> fieldCollection1 = new ArrayList<>();
            Object obj1 = new Object();
            fieldCollection1.add(obj1);
            JRDataSource dataSource = new JRBeanCollectionDataSource(fieldCollection1, false);
            log.info("Report Going to fill");
            JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, dataSource);
            log.info("Rider Page is available ?? " + secondPageNeeded);
            if (secondPageNeeded) {
                log.info("Rider Page is available 1111111 ");
                JasperReport jasperReport1 = JasperCompileManager.compileReport(appResourcePath + "/efs-report/forwarders_cargo_receipt_rider.jrxml");
                List<Object> fieldCollection = new ArrayList<>();
                Object obj = new Object();
                fieldCollection.add(obj);
                log.info("Rider Page is available 2222222");
                JRDataSource dataSource1 = new JRBeanCollectionDataSource(forwarderCargoList, false);
                log.info("Rider Page is available 33333333");

                JasperPrint jasperPrint1 = JasperFillManager.fillReport(jasperReport1, parameter1, dataSource1);
                List<JRPrintPage> pages = jasperPrint1.getPages();
                for (int count = 0; count <
                        pages.size(); count++) {
                    jasperPrint.addPage(pages.get(count));
                }
            }
            return jasperPrint;

        } catch (JRException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public Map<String, String> AddressMasterToMap(String parameterName, AddressMaster addressMaster, int index) {
        Map<String, String> parameter = new HashMap<>();

        if (addressMaster != null) {
            if (addressMaster.getAddressLine1() != null && addressMaster.getAddressLine1().trim().length() > 0) {
                parameter.put(parameterName + index, addressMaster.getAddressLine1());
                index++;
            }
            if (addressMaster.getAddressLine2() != null && addressMaster.getAddressLine2().trim().length() > 0) {
                parameter.put(parameterName + index, addressMaster.getAddressLine2());
                index++;
            }
            if (addressMaster.getAddressLine3() != null && addressMaster.getAddressLine3().trim().length() > 0) {
                parameter.put(parameterName + index, addressMaster.getAddressLine3());
                index++;
            }
            if (addressMaster.getAddressLine4() != null && addressMaster.getAddressLine4().trim().length() > 0) {
                parameter.put(parameterName + index, addressMaster.getAddressLine4());
                index++;
            }
        }
        return parameter;
    }
}

