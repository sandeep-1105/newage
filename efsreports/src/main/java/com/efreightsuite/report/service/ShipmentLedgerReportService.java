package com.efreightsuite.report.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.JobLedgerResChargeDto;
import com.efreightsuite.dto.JobLedgerResDocumentDto;
import com.efreightsuite.dto.JobLedgerResDto;
import com.efreightsuite.dto.ReportDownloadRequestDto;
import com.efreightsuite.enumeration.ChargeType;
import com.efreightsuite.enumeration.ImportExport;
import com.efreightsuite.enumeration.PPCC;
import com.efreightsuite.enumeration.ReportDownloadOption;
import com.efreightsuite.enumeration.ReportName;
import com.efreightsuite.enumeration.TransportMode;
import com.efreightsuite.enumeration.WhoRouted;
import com.efreightsuite.model.Consol;
import com.efreightsuite.model.LocationMaster;
import com.efreightsuite.model.Shipment;
import com.efreightsuite.model.ShipmentServiceDetail;
import com.efreightsuite.model.UserProfile;
import com.efreightsuite.report.JobCostSheet;
import com.efreightsuite.service.AuthService;
import com.efreightsuite.util.AppUtil;
import lombok.extern.log4j.Log4j2;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class ShipmentLedgerReportService extends AbstractReportService {
    @Override
    public ReportName getReportType() {
        return ReportName.SHIPMENT_COST_SHEET;
    }

    public byte[] processReport(ReportDownloadRequestDto data, HttpServletResponse response, boolean isMultiple) {
        log.info("I am in maaster_shipmentrReportService.....");
        byte[] reportContent = null;
        JasperPrint jasperPrint = getReport(data, AuthService.getCurrentUser());
        if (data.getDownloadOption() != ReportDownloadOption.eMail) {
            reportContent = reportUtil.japerPrintToResponseOutputStream(data.getDownloadFileType(), jasperPrint, ReportName.SHIPMENT_COST_SHEET.toString(), response, false, null);
        }
        return reportContent;
    }

    private JasperPrint getReport(ReportDownloadRequestDto data, UserProfile loggedInUser) {
        log.info("MasterShipmentReportService  Resource[" + data.getJobLedgerResDto() + "] function called");
        try {
            Map<String, Object> parameters = new HashMap<>();
            String loggedInUserName = StringUtils.EMPTY;
            String loggedInLocation = StringUtils.EMPTY;
            if (loggedInUser != null && loggedInUser.getSelectedUserLocation() != null) {
                if (loggedInUser.getEmployee() != null && loggedInUser.getEmployee().getEmployeeName() != null) {
                    loggedInUserName = loggedInUser.getEmployee().getEmployeeName();
                } else if (loggedInUser.getUserName() != null) {
                    loggedInUserName = loggedInUser.getUserName();
                } else {
                    loggedInUserName = StringUtils.EMPTY;
                }
                parameters.put("refDate", reportUtil.dateToReportDate(new java.util.Date(), loggedInUser.getSelectedUserLocation().getJavaDateFormat()));
                if (loggedInUser.getSelectedUserLocation() != null) {
                    loggedInLocation = loggedInUser.getSelectedUserLocation().getLocationName() != null
                            ? loggedInUser.getSelectedUserLocation().getLocationName() : StringUtils.EMPTY;
                    LocationMaster locationMaster = locationMasterRepository.findByLocationCode(loggedInUser.getSelectedUserLocation().getLocationCode());
                    parameters.put("locationMasterAddressList", reportUtil.locationMasterAddressList(locationMaster));
                    if (locationMaster != null) {
                        String companyName = locationMaster.getBranchName() != null ? locationMaster.getBranchName() : StringUtils.EMPTY;
                        parameters.put("companyName", reportUtil.removeSpecialCharacters(companyName, data.getDownloadOption()));
                    }
                }
            }

            parameters.put("refPlace", loggedInUserName + "-" + loggedInLocation);

            String pol = StringUtils.EMPTY;
            String pod = StringUtils.EMPTY;
            ShipmentServiceDetail serviceDetail = null;
            JobLedgerResDto jobLedgerResDto = data.getJobLedgerResDto();

            List<JobCostSheet> jobCostSheetList = new ArrayList<>();
            List<JobCostSheet> provisionchargeList = new ArrayList<>();
            List<JobCostSheet> otherChargeList = new ArrayList<>();
            JobCostSheet jobCostSheet = new JobCostSheet();
            if (jobLedgerResDto != null) {
                List<JobLedgerResDocumentDto> jobLedgerResDocumentDto = jobLedgerResDto.getDocumentWiseList();
                if (jobLedgerResDocumentDto != null) {
                    for (JobLedgerResDocumentDto jlr : jobLedgerResDto.getDocumentWiseList()) {


                        if (jlr != null) {

                            if (jlr.getMasterUid() != null) {
                                Consol consolDetail = consolRepository.findByConsolUid(jlr.getMasterUid());
                                if (consolDetail != null) {
                                    int size = consolDetail.getShipmentLinkList().size();
                                    parameters.put("noOfSubjob", Integer.valueOf(size));
                                }

                                List<ShipmentServiceDetail> serviceDetailList = shipmentServiceDetailRepository.getByServiceByConsol(jlr.getMasterUid());
                                serviceDetail = serviceDetailList.get(0);
                            } else {
                                Shipment shipment = shipmentRepository.findByShipmentUid(jlr.getShipmentUid());
                                if (shipment != null) {
                                    parameters.put("noOfSubjob", 1);
                                    serviceDetail = shipmentServiceDetailRepository.findById(shipment.getShipmentDetail().getId());
                                }

                            }

                            if (serviceDetail != null) {
                                String polDate = reportUtil.dateToReportDate(serviceDetail.getEta(), loggedInUser.getSelectedUserLocation().getJavaDateFormat());
                                String podDate = reportUtil.dateToReportDate(serviceDetail.getEta(), loggedInUser.getSelectedUserLocation().getJavaDateFormat());

                                if (pol.length() > 0 && polDate.length() > 0) {
                                    parameters.put("polAndDate", pol + " / " + polDate);
                                } else {
                                    parameters.put("polAndDate", pol + "  " + polDate);
                                }
                                if (pod.length() > 0 && podDate.length() > 0) {
                                    parameters.put("podAndDate", pod + " / " + podDate);
                                } else {
                                    parameters.put("podAndDate", pod + "  " + podDate);
                                }

                                parameters.put("jobNumber", jlr.getShipmentUid());
                                parameters.put("bookingNo", jlr.getShipmentUid());

                                if (serviceDetail.getServiceMaster() != null) {
                                    if (serviceDetail.getServiceMaster().getTransportMode() != null && serviceDetail.getServiceMaster().getTransportMode().equals(TransportMode.Ocean)) {
                                        parameters.put("containerLabel", "Container");
                                        parameters.put("costCenterLabel", "Cost Center");
                                    }
                                    if (serviceDetail.getServiceMaster().getCostCenter() != null) {
                                        parameters.put("costCenter", serviceDetail.getServiceMaster().getCostCenter().getCostCenterName() != null ? serviceDetail.getServiceMaster().getCostCenter().getCostCenterName() : StringUtils.EMPTY);
                                    }
                                }

                                if (serviceDetail.getServiceMaster() != null && serviceDetail.getServiceMaster().getImportExport() != null) {
                                    if (serviceDetail.getServiceMaster().getImportExport().equals(ImportExport.Export) || serviceDetail.getServiceMaster().getImportExport().equals("Export")) {
                                        parameters.put("tradeCode", "EXPORT");
                                    } else {
                                        parameters.put("tradeCode", "IMPORT");
                                    }
                                }
                                if (serviceDetail.getHawbNo() != null) {
                                    parameters.put("hblAndHawb", serviceDetail.getHawbNo());
                                }
                                if (serviceDetail.getMawbNo() != null) {
                                    parameters.put("oblAndMawb", serviceDetail.getMawbNo());
                                }
                                if (serviceDetail.getParty() != null) {
                                    parameters.put("billingPartyname", reportUtil.removeSpecialCharacters(serviceDetail.getParty().getPartyName(), data.getDownloadOption()));

                                }
                                if (serviceDetail.getDocumentList() != null) {
                                    if (serviceDetail.getDocumentList().get(0).getAgent() != null && serviceDetail.getDocumentList().get(0).getAgent().getPartyName() != null) {
                                        parameters.put("accountName", serviceDetail.getDocumentList().get(0).getAgent().getPartyName());
                                        parameters.put("agentName", serviceDetail.getDocumentList().get(0).getAgent().getPartyName());
                                    }
                                }
                                if (serviceDetail.getPpcc() != null && serviceDetail.getPpcc().equals(PPCC.Prepaid) || serviceDetail.getPpcc().equals("Prepaid")) {
                                    parameters.put("frieghtName", "PREPAID");
                                } else {
                                    parameters.put("frieghtName", "COLLECT");
                                }

                                if (serviceDetail.getSalesman() != null) {
                                    parameters.put("salesmanName", serviceDetail.getSalesman().getEmployeeName() != null ? serviceDetail.getSalesman().getEmployeeName() : StringUtils.EMPTY);
                                }
                                if (serviceDetail.getShipper() != null && serviceDetail.getShipper().getPartyName() != null) {
                                    parameters.put("shipperName", reportUtil.removeSpecialCharacters(serviceDetail.getShipper().getPartyName(), data.getDownloadOption()));
                                }
                                if (serviceDetail.getConsignee() != null && serviceDetail.getConsignee().getPartyName() != null) {
                                    parameters.put("consigneeName", reportUtil.removeSpecialCharacters(serviceDetail.getConsignee().getPartyName(), data.getDownloadOption()));
                                }
                                if (serviceDetail.getOrigin() != null) {
                                    parameters.put("origin", serviceDetail.getOrigin().getPortName() != null ? serviceDetail.getOrigin().getPortName() : StringUtils.EMPTY);
                                }
                                if (serviceDetail.getDestination() != null) {
                                    parameters.put("fdc", serviceDetail.getDestination().getPortName() != null ? serviceDetail.getDestination().getPortName() : StringUtils.EMPTY);
                                }

                                if (serviceDetail.getBookedGrossWeightUnitKg() != null && serviceDetail.getCountry() != null) {
                                    parameters.put("grossWeight", AppUtil.getWeightFormat(serviceDetail.getBookedGrossWeightUnitKg(), serviceDetail.getCountry().getLocale()) + " KG");
                                }
                                if (serviceDetail.getBookedVolumeWeightUnitKg() != null && serviceDetail.getCountry() != null) {
                                    parameters.put("volumeWeight", AppUtil.getWeightFormat(serviceDetail.getBookedVolumeWeightUnitKg(), serviceDetail.getCountry().getLocale()) + " KG");
                                }
                                if (serviceDetail.getBookedChargeableUnit() != null && serviceDetail.getCountry() != null) {
                                    parameters.put("chargeableWeight", AppUtil.getWeightFormat(serviceDetail.getBookedChargeableUnit(), serviceDetail.getCountry().getLocale()) + " KG");
                                }
                                if (serviceDetail.getWhoRouted() != null && serviceDetail.getWhoRouted().equals(WhoRouted.Self) || serviceDetail.getWhoRouted() != null && serviceDetail.getWhoRouted().equals("self")) {
                                    parameters.put("routedBy", "SELF");
                                } else {
                                    parameters.put("routedBy", "AGENT");
                                }

                                if (serviceDetail.getPol() != null) {
                                    pol = serviceDetail.getPol().getPortName() != null ? serviceDetail.getPol().getPortName() : StringUtils.EMPTY;
                                }
                                if (serviceDetail.getPod() != null) {
                                    pod = serviceDetail.getPod().getPortName() != null ? serviceDetail.getPod().getPortName() : StringUtils.EMPTY;
                                }

                            }

                        }

                    }

                }

                jobCostSheet.setJobLedgerResDto(jobLedgerResDto);
                jobCostSheetList.add(jobCostSheet);

                Double revenue = 0.0, cost = 0.0, gp = 0.0, neutral = 0.0, actualRevenue = 0.0, actualCost = 0.0, actualGp = 0.0, actualNeutral = 0.0, totalRevenue = 0.0;
                Double totalCost = 0.0, totalGp = 0.0, totalActualRevenue = 0.0, totalActualCost = 0.0, totalActualGp = 0.0;

                Double otherRevenue = 0.0, otherCost = 0.0, otherGp = 0.0, otherNeutral = 0.0, otherActualRevenue = 0.0, otherActualCost = 0.0, otherActualGp = 0.0;
                Double otherActualNeutral = 0.0, otherTotalRevenue = 0.0, otherTotalCost = 0.0, otherTotalGp = 0.0, otherTotalActualRevenue = 0.0, otherTotalActualCost = 0.0,
                        otherTotalActualGp = 0.0;

                Double grandActualTotalGp = 0.0;
                Double grandTotalRevenue = 0.0, grandTotalCost = 0.0, grandTotalGp = 0.0, grandTotalNeutral = 0.0;
                ;
                Double otherGrandActualTotalRevenue = 0.0, otherGrandActualTotalCost = 0.0, otherGrandActualTotalGp = 0.0, otherGrandTotalActualNetral = 0.0;

                List<JobLedgerResChargeDto> jobLedgerCharge = jobLedgerResDto.getChargeWiseList();
                if (jobLedgerCharge != null) {
                    for (JobLedgerResChargeDto jlrc : jobLedgerCharge) {
                        JobCostSheet jcs = new JobCostSheet();
                        if (jlrc != null && jlrc.getChargeType() != null && jlrc.getChargeType().equals(ChargeType.Freight.name())) {
                            if (jlrc.getChargeName() != null) {
                                jcs.setChargeName(jlrc.getChargeName());
                            }
                            if (jlrc.getProvisionalRevenue() != null) {
                                revenue = jlrc.getProvisionalRevenue();
                                totalRevenue = totalRevenue + revenue;
                                jcs.setRevenue(AppUtil.getCurrencyFormat(jlrc.getProvisionalRevenue(), loggedInUser.getSelectedUserLocation().getCountryMaster().getLocale(), loggedInUser.getSelectedUserLocation().getCurrencyMaster().getDecimalPoint()));
                            }
                            if (jlrc.getProvisionalCost() != null) {
                                cost = jlrc.getProvisionalCost();
                                totalCost = totalCost + cost;

                                jcs.setCost(AppUtil.getCurrencyFormat(jlrc.getProvisionalCost(), loggedInUser.getSelectedUserLocation().getCountryMaster().getLocale(), loggedInUser.getSelectedUserLocation().getCurrencyMaster().getDecimalPoint()));
                            }
                            if (jlrc.getProvisionalNeutral() != null) {
                                neutral = neutral + jlrc.getProvisionalNeutral();
                                jcs.setNeutral(AppUtil.getCurrencyFormat(jlrc.getProvisionalNeutral(), loggedInUser.getSelectedUserLocation().getCountryMaster().getLocale(), loggedInUser.getSelectedUserLocation().getCurrencyMaster().getDecimalPoint()));
                            }
                            if (jlrc.getActualRevenue() != null) {
                                actualRevenue = jlrc.getActualRevenue();
                                totalActualRevenue = totalActualRevenue + actualRevenue;
                                jcs.setActualRevenue(AppUtil.getCurrencyFormat(jlrc.getActualRevenue(), loggedInUser.getSelectedUserLocation().getCountryMaster().getLocale(), loggedInUser.getSelectedUserLocation().getCurrencyMaster().getDecimalPoint()));
                            }
                            if (jlrc.getActualCost() != null) {
                                actualCost = jlrc.getActualCost();
                                totalActualCost = totalActualCost + actualCost;
                                jcs.setActualCost(AppUtil.getCurrencyFormat(jlrc.getActualCost(), loggedInUser.getSelectedUserLocation().getCountryMaster().getLocale(), loggedInUser.getSelectedUserLocation().getCurrencyMaster().getDecimalPoint()));
                            }
                            if (jlrc.getActualNeutral() != null) {
                                actualNeutral = actualNeutral + jlrc.getActualNeutral();
                                jcs.setActualNeutral(AppUtil.getCurrencyFormat(jlrc.getActualNeutral(), loggedInUser.getSelectedUserLocation().getCountryMaster().getLocale(), loggedInUser.getSelectedUserLocation().getCurrencyMaster().getDecimalPoint()));
                            }
                            gp = revenue - cost;
                            actualGp = actualRevenue - actualCost;

                            jcs.setGp(AppUtil.getCurrencyFormat(gp, loggedInUser.getSelectedUserLocation().getCountryMaster().getLocale(), loggedInUser.getSelectedUserLocation().getCurrencyMaster().getDecimalPoint()));
                            jcs.setActualGp(AppUtil.getCurrencyFormat(actualGp, loggedInUser.getSelectedUserLocation().getCountryMaster().getLocale(), loggedInUser.getSelectedUserLocation().getCurrencyMaster().getDecimalPoint()));

                            totalGp = totalRevenue - totalCost;
                            grandTotalGp = grandTotalGp + totalGp;

                            totalActualGp = totalActualRevenue - totalActualCost;
                            grandActualTotalGp = grandActualTotalGp + totalActualGp;


                            jcs.setGpPercentage(StringUtils.EMPTY);

                        } else if (jlrc != null && jlrc.getChargeType() != null && jlrc.getChargeType().equals(ChargeType.Other.name())) {
                            if (jlrc.getChargeName() != null) {
                                jcs.setChargeName(jlrc.getChargeName());
                            }
                            if (jlrc.getProvisionalRevenue() != null) {
                                otherRevenue = jlrc.getProvisionalRevenue();
                                otherTotalRevenue = otherTotalRevenue + otherRevenue;
                                jcs.setRevenue(AppUtil.getCurrencyFormat(jlrc.getProvisionalRevenue(), loggedInUser.getSelectedUserLocation().getCountryMaster().getLocale(), loggedInUser.getSelectedUserLocation().getCurrencyMaster().getDecimalPoint()));
                            }
                            if (jlrc.getProvisionalCost() != null) {
                                otherCost = jlrc.getProvisionalCost();
                                otherTotalCost = otherTotalCost + otherCost;

                                jcs.setCost(AppUtil.getCurrencyFormat(jlrc.getProvisionalCost(), loggedInUser.getSelectedUserLocation().getCountryMaster().getLocale(), loggedInUser.getSelectedUserLocation().getCurrencyMaster().getDecimalPoint()));
                            }
                            if (jlrc.getProvisionalNeutral() != null) {
                                otherNeutral = otherNeutral + jlrc.getProvisionalNeutral();
                                jcs.setNeutral(AppUtil.getCurrencyFormat(jlrc.getProvisionalNeutral(), loggedInUser.getSelectedUserLocation().getCountryMaster().getLocale(), loggedInUser.getSelectedUserLocation().getCurrencyMaster().getDecimalPoint()));
                            }
                            if (jlrc.getActualRevenue() != null) {
                                otherActualRevenue = jlrc.getActualRevenue();
                                otherTotalActualRevenue = otherTotalActualRevenue + otherActualRevenue;
                                jcs.setActualRevenue(AppUtil.getCurrencyFormat(jlrc.getActualRevenue(), loggedInUser.getSelectedUserLocation().getCountryMaster().getLocale(), loggedInUser.getSelectedUserLocation().getCurrencyMaster().getDecimalPoint()));
                            }
                            if (jlrc.getActualCost() != null) {
                                otherActualCost = jlrc.getActualCost();
                                otherTotalActualCost = otherTotalActualCost + otherActualCost;
                                jcs.setActualCost(AppUtil.getCurrencyFormat(jlrc.getActualCost(), loggedInUser.getSelectedUserLocation().getCountryMaster().getLocale(), loggedInUser.getSelectedUserLocation().getCurrencyMaster().getDecimalPoint()));
                            }
                            if (jlrc.getActualNeutral() != null) {
                                otherActualNeutral = otherActualNeutral + jlrc.getActualNeutral();
                                jcs.setActualNeutral(AppUtil.getCurrencyFormat(jlrc.getActualNeutral(), loggedInUser.getSelectedUserLocation().getCountryMaster().getLocale(), loggedInUser.getSelectedUserLocation().getCurrencyMaster().getDecimalPoint()));
                            }
                            otherGp = otherRevenue - otherCost;
                            otherActualGp = otherActualRevenue - otherActualCost;
                            jcs.setGp(AppUtil.getCurrencyFormat(otherGp, loggedInUser.getSelectedUserLocation().getCountryMaster().getLocale(), loggedInUser.getSelectedUserLocation().getCurrencyMaster().getDecimalPoint()));
                            jcs.setActualGp(AppUtil.getCurrencyFormat(otherActualGp, loggedInUser.getSelectedUserLocation().getCountryMaster().getLocale(), loggedInUser.getSelectedUserLocation().getCurrencyMaster().getDecimalPoint()));

                            otherTotalGp = otherTotalRevenue - otherTotalCost;
                            otherTotalActualGp = otherTotalActualRevenue - otherTotalActualCost;

                            otherGrandActualTotalGp = otherGrandActualTotalGp + otherTotalActualGp;

                            jcs.setGpPercentage(StringUtils.EMPTY);

                        }

                        //Grand total for Actual both other and Freight Details
                        otherGrandActualTotalRevenue = totalActualRevenue + otherTotalActualRevenue;
                        otherGrandActualTotalCost = totalActualCost + otherTotalActualCost;
                        otherGrandActualTotalGp = otherGrandActualTotalRevenue - otherGrandActualTotalCost;

                        grandTotalNeutral = neutral + otherNeutral;
                        otherGrandTotalActualNetral = actualNeutral + otherNeutral;

                        //Grand total for  both other and Freight Provisional Details
                        grandTotalRevenue = totalRevenue + otherTotalRevenue;
                        grandTotalCost = totalCost + otherTotalCost;
                        grandTotalGp = grandTotalRevenue - grandTotalCost;

                        if (jlrc.getChargeType() != null && jlrc.getChargeType().equals(ChargeType.Freight.name())) {
                            provisionchargeList.add(jcs);
                            parameters.put("provisionchargeList", provisionchargeList); //Freight  Charge Details
                        } else if (jlrc.getChargeType() != null && jlrc.getChargeType().equals(ChargeType.Other.name())) {
                            otherChargeList.add(jcs);
                            parameters.put("otherChargeList", otherChargeList); //Other Charge Details
                        }
                    }
                    //Freight and Other Charge Details End Here.........

                    parameters.put("totalGp", AppUtil.getCurrencyFormat(totalGp, loggedInUser.getSelectedUserLocation().getCountryMaster().getLocale(), loggedInUser.getSelectedUserLocation().getCurrencyMaster().getDecimalPoint()));
                    parameters.put("totalCost", AppUtil.getCurrencyFormat(totalCost, loggedInUser.getSelectedUserLocation().getCountryMaster().getLocale(), loggedInUser.getSelectedUserLocation().getCurrencyMaster().getDecimalPoint()));
                    parameters.put("totalRevenue", AppUtil.getCurrencyFormat(totalRevenue, loggedInUser.getSelectedUserLocation().getCountryMaster().getLocale(), loggedInUser.getSelectedUserLocation().getCurrencyMaster().getDecimalPoint()));
                    parameters.put("totalNeutral", AppUtil.getCurrencyFormat(neutral, loggedInUser.getSelectedUserLocation().getCountryMaster().getLocale(), loggedInUser.getSelectedUserLocation().getCurrencyMaster().getDecimalPoint()));

                    parameters.put("totalActualGp", AppUtil.getCurrencyFormat(totalActualGp, loggedInUser.getSelectedUserLocation().getCountryMaster().getLocale(), loggedInUser.getSelectedUserLocation().getCurrencyMaster().getDecimalPoint()));
                    parameters.put("totalActualCost", AppUtil.getCurrencyFormat(totalActualCost, loggedInUser.getSelectedUserLocation().getCountryMaster().getLocale(), loggedInUser.getSelectedUserLocation().getCurrencyMaster().getDecimalPoint()));
                    parameters.put("totalActualRevenue", AppUtil.getCurrencyFormat(totalActualRevenue, loggedInUser.getSelectedUserLocation().getCountryMaster().getLocale(), loggedInUser.getSelectedUserLocation().getCurrencyMaster().getDecimalPoint()));
                    parameters.put("totalActualNeutral", AppUtil.getCurrencyFormat(actualNeutral, loggedInUser.getSelectedUserLocation().getCountryMaster().getLocale(), loggedInUser.getSelectedUserLocation().getCurrencyMaster().getDecimalPoint()));

                    parameters.put("otherTotalGp", AppUtil.getCurrencyFormat(otherTotalGp, loggedInUser.getSelectedUserLocation().getCountryMaster().getLocale(), loggedInUser.getSelectedUserLocation().getCurrencyMaster().getDecimalPoint()));
                    parameters.put("otherTotalCost", AppUtil.getCurrencyFormat(otherTotalCost, loggedInUser.getSelectedUserLocation().getCountryMaster().getLocale(), loggedInUser.getSelectedUserLocation().getCurrencyMaster().getDecimalPoint()));
                    parameters.put("otherTotalRevenue", AppUtil.getCurrencyFormat(otherTotalRevenue, loggedInUser.getSelectedUserLocation().getCountryMaster().getLocale(), loggedInUser.getSelectedUserLocation().getCurrencyMaster().getDecimalPoint()));
                    parameters.put("otherTotalNeutral", AppUtil.getCurrencyFormat(otherNeutral, loggedInUser.getSelectedUserLocation().getCountryMaster().getLocale(), loggedInUser.getSelectedUserLocation().getCurrencyMaster().getDecimalPoint()));

                    parameters.put("otherTotalActualGp", AppUtil.getCurrencyFormat(otherTotalActualGp, loggedInUser.getSelectedUserLocation().getCountryMaster().getLocale(), loggedInUser.getSelectedUserLocation().getCurrencyMaster().getDecimalPoint()));
                    parameters.put("otherTotalActualCost", AppUtil.getCurrencyFormat(otherTotalActualCost, loggedInUser.getSelectedUserLocation().getCountryMaster().getLocale(), loggedInUser.getSelectedUserLocation().getCurrencyMaster().getDecimalPoint()));
                    parameters.put("otherTotalActualRevenue", AppUtil.getCurrencyFormat(otherTotalActualRevenue, loggedInUser.getSelectedUserLocation().getCountryMaster().getLocale(), loggedInUser.getSelectedUserLocation().getCurrencyMaster().getDecimalPoint()));
                    parameters.put("otherTotalActualNeutral", AppUtil.getCurrencyFormat(otherActualNeutral, loggedInUser.getSelectedUserLocation().getCountryMaster().getLocale(), loggedInUser.getSelectedUserLocation().getCurrencyMaster().getDecimalPoint()));

                    parameters.put("grandTotalGp", AppUtil.getCurrencyFormat(grandTotalGp, loggedInUser.getSelectedUserLocation().getCountryMaster().getLocale(), loggedInUser.getSelectedUserLocation().getCurrencyMaster().getDecimalPoint()));
                    parameters.put("grandActualTotalGp", AppUtil.getCurrencyFormat(grandActualTotalGp, loggedInUser.getSelectedUserLocation().getCountryMaster().getLocale(), loggedInUser.getSelectedUserLocation().getCurrencyMaster().getDecimalPoint()));
                    parameters.put("otherGrandActualTotalGp", AppUtil.getCurrencyFormat(otherGrandActualTotalGp, loggedInUser.getSelectedUserLocation().getCountryMaster().getLocale(), loggedInUser.getSelectedUserLocation().getCurrencyMaster().getDecimalPoint()));

                    parameters.put("grandActualTotalRevenue", AppUtil.getCurrencyFormat(otherGrandActualTotalRevenue, loggedInUser.getSelectedUserLocation().getCountryMaster().getLocale(), loggedInUser.getSelectedUserLocation().getCurrencyMaster().getDecimalPoint()));
                    parameters.put("grandActualTotalNeutral", AppUtil.getCurrencyFormat(otherGrandTotalActualNetral, loggedInUser.getSelectedUserLocation().getCountryMaster().getLocale(), loggedInUser.getSelectedUserLocation().getCurrencyMaster().getDecimalPoint()));
                    parameters.put("grandActualTotalCost", AppUtil.getCurrencyFormat(otherGrandActualTotalCost, loggedInUser.getSelectedUserLocation().getCountryMaster().getLocale(), loggedInUser.getSelectedUserLocation().getCurrencyMaster().getDecimalPoint()));
                    parameters.put("grandActualTotalGp", AppUtil.getCurrencyFormat(otherGrandActualTotalGp, loggedInUser.getSelectedUserLocation().getCountryMaster().getLocale(), loggedInUser.getSelectedUserLocation().getCurrencyMaster().getDecimalPoint()));

                    parameters.put("grandTotalRevenue", AppUtil.getCurrencyFormat(grandTotalRevenue, loggedInUser.getSelectedUserLocation().getCountryMaster().getLocale(), loggedInUser.getSelectedUserLocation().getCurrencyMaster().getDecimalPoint()));
                    parameters.put("grandTotalCost", AppUtil.getCurrencyFormat(grandTotalCost, loggedInUser.getSelectedUserLocation().getCountryMaster().getLocale(), loggedInUser.getSelectedUserLocation().getCurrencyMaster().getDecimalPoint()));
                    parameters.put("grandTotalNeutral", AppUtil.getCurrencyFormat(grandTotalNeutral, loggedInUser.getSelectedUserLocation().getCountryMaster().getLocale(), loggedInUser.getSelectedUserLocation().getCurrencyMaster().getDecimalPoint()));
                }
            } else {
                jobCostSheetList.add(jobCostSheet);
            }
            String defaultMasterData = appUtil.getLocationConfig("shipment.sub.cost.sheet", loggedInUser.getSelectedUserLocation(), false);
            parameters.put("note", defaultMasterData);

            JasperReport jasperReport = JasperCompileManager.compileReport(appResourcePath + "/efs-report/shipment_ledger.jrxml");
            JRDataSource dataSource = new JRBeanCollectionDataSource(jobCostSheetList, false);
            return JasperFillManager.fillReport(jasperReport, parameters, dataSource);

        } catch (JRException e) {
            e.printStackTrace();
        }
        return null;
    }
}
