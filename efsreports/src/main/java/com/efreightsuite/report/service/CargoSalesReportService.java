package com.efreightsuite.report.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import com.efreightsuite.dto.ReportDownloadRequestDto;
import com.efreightsuite.enumeration.ReportConfig;
import com.efreightsuite.enumeration.ReportName;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.model.Consol;
import com.efreightsuite.model.ReportConfigurationMaster;
import com.efreightsuite.model.ShipmentCharge;
import com.efreightsuite.model.ShipmentServiceDetail;
import com.efreightsuite.model.UserProfile;
import com.efreightsuite.report.CargoSalesDetail;
import com.efreightsuite.service.AuthService;
import com.efreightsuite.util.AppUtil;
import lombok.extern.log4j.Log4j2;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

import org.springframework.stereotype.Service;

@Service
@Log4j2

public class CargoSalesReportService extends AbstractReportService {
    public ReportName getReportType() {
        return ReportName.CARGO_SALES;
    }

    public byte[] processReport(ReportDownloadRequestDto data, HttpServletResponse response, boolean isMultiple) {
        log.info("Cargo Sales Service method is called .............");
        List<ShipmentServiceDetail> serviceList = shipmentServiceDetailRepository.findByServiceByDetail(data.getServiceName().getServiceName(), data.getApiFromDate(), data.getApiToDate());
        log.info("Service List Object:::[" + serviceList + "] AND  Size::: [" + serviceList.size() + "]");
        Map<String, Object> parameters = new HashMap<>();
        List<CargoSalesDetail> cargoSalesList = new ArrayList<>();
        if (serviceList != null && serviceList.size() > 0) {
            try {
                for (ShipmentServiceDetail service : serviceList) {
                    CargoSalesDetail cargoSalesDetail = new CargoSalesDetail();
                    if (service != null) {
                        if (service.getCarrier() != null) {
                            cargoSalesDetail.setCarrierCode(service.getCarrier().getCarrierCode() != null ? service.getCarrier().getCarrierCode() : "");
                            cargoSalesDetail.setCarrierName(service.getCarrier().getCarrierName() != null ? service.getCarrier().getCarrierName() : "");
                        }
                        if (service.getMawbNo() != null) {
                            cargoSalesDetail.setMawbNo(service.getMawbNo());
                        }
                        if (service.getPod() != null) {
                            log.info("Cargo Sales Report POD:::::::::::" + service.getPod().getPortName() + "::::::");
                            cargoSalesDetail.setPod(service.getPod().getPortName() != null ? service.getPod().getPortName() : "");
                        }
                        cargoSalesDetail.setGrossWeight(service.getBookedGrossWeightUnitKg() != null && service.getBookedGrossWeightUnitKg() > 0 ? AppUtil.getWeightFormat(service.getBookedGrossWeightUnitKg(), service.getCountry().getLocale()) : "");
                        cargoSalesDetail.setChargeableWeight(service.getBookedChargeableUnit() != null && service.getBookedChargeableUnit() > 0 ? AppUtil.getWeightFormat(service.getBookedChargeableUnit(), service.getCountry().getLocale()) : "");
                        if (service.getConsolUid() != null) {
                            Consol consol = consolRepository.findByConsolUid(service.getConsolUid());
                            if (consol != null) {
                                Double iataRate = 0.0;
                                Double grossWeight = 0.0;
                                Double amount = 0.0;
                                if (consol.getIataRate() != null) {
                                    log.info("IATA Rate:::::::" + consol.getIataRate());
                                    iataRate = consol.getIataRate();
                                }
                                cargoSalesDetail.setIataRate((iataRate != null && iataRate > 0) ? AppUtil.getCurrencyFormat(iataRate, consol.getCurrency().getCountryMaster().getLocale(), consol.getCurrency().getDecimalPoint()) : "");
                                if (consol.getConsolDocument() != null) {
                                    if (consol.getConsolDocument().getGrossWeight() != null) {
                                        grossWeight = consol.getConsolDocument().getGrossWeight();
                                    }
                                }
                                amount = iataRate * grossWeight;
                                cargoSalesDetail.setAmount((amount != null && amount > 0) ? AppUtil.getCurrencyFormat(amount, consol.getCurrency().getCountryMaster().getLocale(), consol.getCurrency().getDecimalPoint()) : "");
                            }
                        }
                        List<ShipmentCharge> chargeList = shipmentChargeRepository.findByCharge(service.getServiceUid());
                        log.info("ChargeList Object:::::::::::" + chargeList);
                        String fuelCharge = "";
                        String warRiskCharge = "";
                        String awbFeeCharge = "";

                        ReportConfigurationMaster tmpObj = reportConfigurationMasterRepository.getByKeyAndLocation(ReportConfig.SURCHARGE_FUEL, service.getId());
                        if (tmpObj != null && tmpObj.getReportValue() != null) {
                            fuelCharge = tmpObj.getReportValue();
                        }

                        ReportConfigurationMaster tmpObj1 = reportConfigurationMasterRepository.getByKeyAndLocation(ReportConfig.SURCHARGE_WAR_RISK, service.getId());
                        if (tmpObj1 != null && tmpObj1.getReportValue() != null) {
                            warRiskCharge = tmpObj1.getReportValue();
                        }

                        ReportConfigurationMaster tmpObj2 = reportConfigurationMasterRepository.getByKeyAndLocation(ReportConfig.SURCHARGE_AWB_FEE, service.getId());
                        if (tmpObj2 != null && tmpObj2.getReportValue() != null) {
                            awbFeeCharge = tmpObj2.getReportValue();
                        }


                        if (chargeList != null && chargeList.size() > 0) {
                            //ShipmentCharge shipmentCharge= chargeList.get(0);
                            for (ShipmentCharge shipmentCharge : chargeList) {
                                if (shipmentCharge != null) {
                                    if (fuelCharge.equals(shipmentCharge.getChargeName()) || fuelCharge.equals("Fuel Surcharge")) {
                                        cargoSalesDetail.setFuelCharge((shipmentCharge.getLocalAmount() != null && shipmentCharge.getLocalAmount() > 0) ? reportUtil.twoDecimalDoubleValueFormat(shipmentCharge.getLocalAmount()) : "");
                                    }
                                    if (warRiskCharge.equals(shipmentCharge.getChargeName()) || fuelCharge.equals("War Risk Surcharge")) {
                                        cargoSalesDetail.setWarRiskSurchage((shipmentCharge.getLocalAmount() != null && shipmentCharge.getLocalAmount() > 0) ? reportUtil.twoDecimalDoubleValueFormat(shipmentCharge.getLocalAmount()) : "");
                                    }
                                    if (awbFeeCharge.equals(shipmentCharge.getChargeName()) || fuelCharge.equals("Air Waybill Fee")) {
                                        cargoSalesDetail.setAwbFee((shipmentCharge.getLocalAmount() != null && shipmentCharge.getLocalAmount() > 0) ? reportUtil.twoDecimalDoubleValueFormat(shipmentCharge.getLocalAmount()) : "");
                                    }
                                    if (shipmentCharge.getLocalAmount() != null) {
                                        cargoSalesDetail.setCommission((shipmentCharge.getLocalAmount() != null && shipmentCharge.getLocalAmount() > 0) ? reportUtil.twoDecimalDoubleValueFormat(shipmentCharge.getLocalAmount()) : "");
                                    }
                                }
                            }
                        }

                        UserProfile loggedInUser = AuthService.getCurrentUser();
                        if (loggedInUser != null && loggedInUser.getSelectedUserLocation() != null
                                && service.getSystemTrack() != null
                                && service.getSystemTrack().getCreateDate() != null) {
                            String todayDate = "";
                            todayDate = reportUtil.dateToReportDate(new Date(), loggedInUser.getSelectedUserLocation().getJavaDateFormat());
                            log.info("Cargo Sales Report JoBDate:::::::::::" + service.getSystemTrack().getCreateDate() + "::::::");
                            cargoSalesDetail.setJobDate(reportUtil.dateToReportDate(service.getSystemTrack().getCreateDate(), loggedInUser.getSelectedUserLocation().getJavaDateFormat()));
                            log.info("Address ------------------------------- " + loggedInUser.getSelectedUserLocation().getAddressLine1());
                            String loggedInUserName = "";
                            String loggedInLocation = "";
                            if (loggedInUser != null) {
                                if (loggedInUser.getEmployee() != null && loggedInUser.getEmployee().getEmployeeName() != null) {
                                    loggedInUserName = loggedInUser.getEmployee().getEmployeeName();
                                } else if (loggedInUser.getUserName() != null) {
                                    loggedInUserName = loggedInUser.getUserName();
                                } else {
                                    loggedInUserName = "";
                                }
                                if (loggedInUser.getSelectedUserLocation() != null) {
                                    loggedInLocation = loggedInUser.getSelectedUserLocation().getLocationName() != null
                                            ? loggedInUser.getSelectedUserLocation().getLocationName() : "";
                                }
                            }
                            parameters.put("refPlace", loggedInUserName + "-" + loggedInLocation);
                            parameters.put("refDate", todayDate);
                        }
                    }
                    cargoSalesList.add(cargoSalesDetail);
                }
                parameters.put("cargoSalesList", cargoSalesList);
                parameters.put("reportName", "CARGO SALES REPORT");

                JasperReport jasperReport = JasperCompileManager.compileReport(appResourcePath + "/efs-report/cargo_sales.jrxml");
                List<Object> fieldCollection1 = new ArrayList<>();
                Object obj1 = new Object();
                fieldCollection1.add(obj1);
                JRDataSource dataSource = new JRBeanCollectionDataSource(fieldCollection1, false);
                JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, dataSource);
                reportUtil.japerPrintToResponseOutputStream(data.getDownloadFileType(), jasperPrint, "CARGO_SALES", response, false, null);
            } catch (RestException e) {
                e.printStackTrace();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            log.info("Cargo Sales  Report Service Completed..... ");
        }
        return null;
    }
}
