package com.efreightsuite.report.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import com.efreightsuite.dto.AutoAttachmentDto;
import com.efreightsuite.dto.BaseDto;
import com.efreightsuite.dto.ReportDownloadRequestDto;
import com.efreightsuite.enumeration.ImportExport;
import com.efreightsuite.enumeration.ReportConfig;
import com.efreightsuite.enumeration.ReportDownloadOption;
import com.efreightsuite.enumeration.ReportName;
import com.efreightsuite.enumeration.YesNo;
import com.efreightsuite.exception.ErrorCode;
import com.efreightsuite.exception.RestException;
import com.efreightsuite.model.DocumentDetail;
import com.efreightsuite.model.ReportConfigurationMaster;
import com.efreightsuite.model.ServiceMaster;
import com.efreightsuite.model.ShipmentServiceDetail;
import com.efreightsuite.model.UserProfile;
import com.efreightsuite.report.CertificateOrigin;
import com.efreightsuite.service.AuthService;
import com.efreightsuite.util.AppUtil;
import lombok.extern.log4j.Log4j2;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRPrintPage;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

import org.springframework.stereotype.Service;

@Service
@Log4j2
public class CertificateOfOriginReportService extends AbstractReportService {

    public ReportName getReportType() {
        return ReportName.CERTIFICATE_OF_ORIGIN;
    }

    public byte[] processReport(ReportDownloadRequestDto data, HttpServletResponse response, boolean isMutiple) {
        log.info("I am in CertificateOfOriginReportService..");
        BaseDto baseDto = new BaseDto();
        UserProfile logInUSer = AuthService.getCurrentUser();
        JasperPrint jasperPrint = getReport(baseDto, data, AuthService.getCurrentUser());

        byte[] a = null;
        AutoAttachmentDto autoAttachmentDto = reportUtil.autoAttachment(ReportName.CERTIFICATE_OF_ORIGIN, YesNo.Yes, data.getResourceId(), "Shipment");
        if (data.getSingle() != null && data.getSingle() == true) {
            if (data.getDownloadOption() == ReportDownloadOption.Preview || data.getDownloadOption() == ReportDownloadOption.Print) {
                reportUtil.japerPrintToResponseOutputStream(data.getDownloadFileType(), jasperPrint, "CERTIFICATE_OF_ORIGIN", response, false, null);
            } else if (autoAttachmentDto != null && autoAttachmentDto.getIsAttach() != null || data.getDownloadOption() == ReportDownloadOption.Download) {
                reportUtil.japerPrintToResponseOutputStream(data.getDownloadFileType(), jasperPrint, "CERTIFICATE_OF_ORIGIN", response, false, autoAttachmentDto);
            }
        }
        if (data.getDownloadOption() == ReportDownloadOption.eMail) {
            a = reportUtil.japerPrintToResponseOutputStream(data.getDownloadFileType(), jasperPrint, "CERTIFICATE_OF_ORIGIN", response, false, null);
            shipmentReportMailer.sendShipmentMailToCustomer(data.getResourceId(), logInUSer, ReportName.CERTIFICATE_OF_ORIGIN, a);
        }
        if (isMutiple == true && data.getDownloadOption() == ReportDownloadOption.eMail) {
            a = reportUtil.japerPrintToResponseOutputStream(data.getDownloadFileType(), jasperPrint, "CERTIFICATE_OF_ORIGIN", response, true, null);
            return a;
        }
        return null;
    }

    private JasperPrint getReport(BaseDto baseDto, ReportDownloadRequestDto data, UserProfile loggedInUser) {
        log.info("I am in CertificateOfOriginReportService....." + data.getResourceId());
        DocumentDetail document = documentDetailRepository.findById(data.getResourceId());
        Map<String, Object> parameters = new HashMap<>();
        Map<String, Object> parameter1 = new HashMap<>();
        boolean secondPageNeeded = false;
        try {
            List<CertificateOrigin> originCollection = new ArrayList<>();
            CertificateOrigin origin = new CertificateOrigin();
            List<CertificateOrigin> certificateOriginRiderList = new ArrayList<>();
            if (document != null) {
                if (document.getShipmentServiceDetail() != null) {
                    if (document.getShipmentServiceDetail().getServiceMaster() != null) {
                        ServiceMaster serviceMaster = document.getShipmentServiceDetail().getServiceMaster();
                        if (serviceMaster != null && serviceMaster.getImportExport().equals(ImportExport.Export) || serviceMaster.getImportExport().equals("Export")) {
                            origin.setExporterImporterName("Exporter");
                            if (document.getShipper() != null && document.getShipper().getPartyName() != null) {
                                origin.setExportImportName(reportUtil.removeSpecialCharacters(document.getShipper().getPartyName(), data.getDownloadOption()));
                            }
                            if (document.getShipperAddress() != null) {
                                origin.setExportImportAddress1(document.getShipperAddress().getAddressLine1());
                                origin.setExportImportAddress2(document.getShipperAddress().getAddressLine2());
                                origin.setExportImportAddress3(document.getShipperAddress().getAddressLine3());
                                origin.setExportImportAddress4(document.getShipperAddress().getAddressLine4());
                                origin.setExportImportPhone(document.getShipperAddress().getPhone());
                            }
                        } else if (serviceMaster != null && serviceMaster.getImportExport().equals(ImportExport.Import) || serviceMaster.getImportExport().equals("Import")) {

                            origin.setExporterImporterName("Impoter");
                            if (document.getShipper() != null && document.getShipper().getPartyName() != null) {
                                origin.setExportImportName(reportUtil.removeSpecialCharacters(document.getShipper().getPartyName(), data.getDownloadOption()));
                            }
                            if (document.getForwarderAddress() != null) {
                                origin.setExportImportAddress1(document.getForwarderAddress().getAddressLine1());
                                origin.setExportImportAddress2(document.getForwarderAddress().getAddressLine2());
                                origin.setExportImportAddress3(document.getForwarderAddress().getAddressLine3());
                                origin.setExportImportAddress4(document.getForwarderAddress().getAddressLine4());
                                origin.setExportImportPhone(document.getForwarderAddress().getPhone());
                            }
                        }
                    }

                }


                origin.setBookingNumber(document.getShipmentUid() != null ? document.getShipmentUid() : "");
                origin.setReferenceNumber(document.getDocumentNo() != null ? document.getDocumentNo() : "");
                origin.setCarrierName(document.getCarrier() != null ? document.getCarrier().getCarrierName() : "");
                origin.setFlightNo(document.getRouteNo());

                if (document.getConsignee() != null && document.getConsignee().getPartyName() != null) {
                    origin.setConsigneeName(reportUtil.removeSpecialCharacters(document.getConsignee().getPartyName(), data.getDownloadOption()));
                }
                ShipmentServiceDetail service = documentDetailRepository.findByIdForShipmentServiceDetail(document.getId());
                if (service != null) {
                    origin.setQuantity(AppUtil.getNumberFormat(document.getNoOfPieces(), service.getCountry().getLocale()));
                    origin.setGrossWeight((AppUtil.getWeightFormat(document.getGrossWeight(), service.getCountry().getLocale())) + " KG");

                    String pack = AppUtil.getNumberFormat(document.getNoOfPieces(), service.getCountry().getLocale());
                    ;
                    String packCode = "";
                    if (document.getPackMaster() != null && document.getPackMaster().getPackName() != null) {
                        packCode = document.getPackMaster().getPackName();
                    }
                    String bothCode = pack + " " + packCode;
                    origin.setBothCode(bothCode);
                }
                CertificateOrigin certificateOrigin = new CertificateOrigin();

                int commoditylengthLine = 0;
                log.info("Enter into loop:::::::::::::");
                if (document.getCommodityDescription() != null) {
                    String commodityDescription = document.getCommodityDescription();
                    commoditylengthLine = commodityDescription.length();
                    if (commoditylengthLine > 0) {
                        commoditylengthLine = commoditylengthLine / 38;
                        log.info("Commodity Size After :::::::" + commoditylengthLine);
                    }
                    if (commoditylengthLine > 10) {
                        origin.setCommodityGroup(document.getCommodityDescription().substring(0, 420));
                        certificateOrigin.setCommodityGroup(document.getCommodityDescription().substring(420, commodityDescription.length()));
                        secondPageNeeded = true;
                    } else {
                        origin.setCommodityGroup(document.getCommodityDescription());
                    }
                }
                String marksParam1 = "";
                String marksParam2 = "";
                if (document.getMarksAndNo() != null) {
                    int markLength = document.getMarksAndNo().length();
                    log.info("Marks and No  Total length::::::" + markLength);
                    if (markLength > 700) {
                        marksParam1 = document.getMarksAndNo().substring(0, 800);
                        log.info(" First Page Marks and No  Total length::::::" + marksParam1.length());
                        origin.setMarksAndNo(marksParam1);
                        marksParam2 = document.getMarksAndNo().substring(800, document.getMarksAndNo().length());
                        log.info(" Second page Marks and No  Total length::::::" + marksParam2.length());
                        certificateOrigin.setMarksAndNo(marksParam2);
                        secondPageNeeded = true;
                    } else {
                        marksParam1 = document.getMarksAndNo();
                        origin.setMarksAndNo(marksParam1);
                    }
                }
                certificateOriginRiderList.add(certificateOrigin);

                origin.setConsigneeAddressLine1(document.getConsigneeAddress() != null ? document.getConsigneeAddress().getAddressLine1() : "");
                origin.setConsigneeAddressLine2(document.getConsigneeAddress() != null ? document.getConsigneeAddress().getAddressLine2() : "");
                if (document.getConsigneeAddress() != null) {
                    origin.setConsigneeAddressLine3(document.getConsigneeAddress().getAddressLine3() != null ? document.getConsigneeAddress().getAddressLine3() : "");
                } else {
                    origin.setConsigneeAddressLine3("");
                }
                origin.setConsigneeAddressLine4(document.getConsigneeAddress() != null ? document.getConsigneeAddress().getAddressLine4() : "");


                {
                    ReportConfigurationMaster tmpObj = reportConfigurationMasterRepository.getByKeyAndLocation(ReportConfig.CERTIFICATE_OF_ORIGIN_REPORT_ADDITIONAL_INFORMATION, service.getLocation().getId());
                    if (tmpObj != null && tmpObj.getReportValue() != null) {
                        parameters.put("additionalInformation", tmpObj.getReportValue());
                    }
                }

                {
                    ReportConfigurationMaster tmpObj = reportConfigurationMasterRepository.getByKeyAndLocation(ReportConfig.CERTIFICATE_OF_ORIGIN_REPORT_FOOTER, service.getLocation().getId());
                    if (tmpObj != null && tmpObj.getReportValue() != null) {
                        parameters.put("cooFooter", tmpObj.getReportValue());
                    }
                }

                {
                    ReportConfigurationMaster tmpObj = reportConfigurationMasterRepository.getByKeyAndLocation(ReportConfig.CERTIFICATE_OF_ORIGIN_REPORT_FOOTER_SECOND, service.getLocation().getId());
                    if (tmpObj != null && tmpObj.getReportValue() != null) {
                        parameters.put("cooFooterSecond", tmpObj.getReportValue());
                    }
                }

                {
                    ReportConfigurationMaster tmpObj = reportConfigurationMasterRepository.getByKeyAndLocation(ReportConfig.CERTIFICATE_OF_ORIGIN_REPORT_DECLARATION_BY_AUTHORIZED_PERSON, service.getLocation().getId());
                    if (tmpObj != null && tmpObj.getReportValue() != null) {
                        parameters.put("declarationByCountry", tmpObj.getReportValue());
                    }
                }

                {
                    ReportConfigurationMaster tmpObj = reportConfigurationMasterRepository.getByKeyAndLocation(ReportConfig.CERTIFICATE_OF_ORIGIN_REPORT_DECLARATION_BY_COUNTRY, service.getLocation().getId());
                    if (tmpObj != null && tmpObj.getReportValue() != null) {
                        parameters.put("declarationByAuthorizedPerson", tmpObj.getReportValue());
                    }
                }


            }


            String loggedInUserName = "";
            String loggedInLocation = "";

            if (loggedInUser != null) {
                if (loggedInUser.getEmployee() != null && loggedInUser.getEmployee().getEmployeeName() != null) {
                    loggedInUserName = loggedInUser.getEmployee().getEmployeeName();
                } else if (loggedInUser.getUserName() != null) {
                    loggedInUserName = loggedInUser.getUserName();
                } else {
                    loggedInUserName = "";
                }

                if (loggedInUser.getSelectedUserLocation() != null) {
                    origin.setCurrentDate(reportUtil.dateToReportDate(new Date(), loggedInUser.getSelectedUserLocation().getJavaDateFormat()));
                    loggedInLocation = loggedInUser.getSelectedUserLocation().getLocationName() != null ? loggedInUser.getSelectedUserLocation().getLocationName() : "";


                } else {
                    loggedInLocation = "";
                }
            }

            origin.setRefPlace(loggedInUserName + "-" + loggedInLocation);
            origin.setCurrentPlace(loggedInLocation);
            originCollection.add(origin);
            parameters.put("usName", loggedInLocation);
            JasperReport jasperReport = JasperCompileManager.compileReport(appResourcePath + "/efs-report/certificate_of_origin.jrxml");
            JRDataSource dataSource = new JRBeanCollectionDataSource(originCollection, false);
            JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, dataSource);
            log.info("Rider Page is available ?? " + secondPageNeeded);
            if (secondPageNeeded) {
                JasperReport jasperReport1 = JasperCompileManager.compileReport(appResourcePath + "/efs-report/certificate_of_origin_rider.jrxml");
                List<Object> fieldCollection1 = new ArrayList<>();
                Object obj1 = new Object();
                fieldCollection1.add(obj1);
                JRDataSource dataSource1 = new JRBeanCollectionDataSource(certificateOriginRiderList, false);
                JasperPrint jasperPrint1 = JasperFillManager.fillReport(jasperReport1, parameter1, dataSource1);

                List<JRPrintPage> pages = jasperPrint1.getPages();
                int count = 0;
                for (count = 0; count <
                        pages.size(); count++) {
                    jasperPrint.addPage(pages.get(count));
                }
                parameter1.put("pageCount", count);
            }

            return jasperPrint;
        } catch (RestException re) {
            log.error("RestException occures in Certification of orgin method  : ", re);
        } catch (Exception exception) {
            log.error("RestException occures in Certification of orgin :: ", exception);
            baseDto.setResponseCode(ErrorCode.FAILED);
        }
        log.info("Certification of Origin   [" + data.getResourceId() + "] method ended");
        return null;

    }

}
