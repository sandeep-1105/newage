package com.efreightsuite.report;

import lombok.Data;

@Data
public class CargoCharges {
    private String chargeName;
    private String currencyCode;
    private String amount;
    private String roe;
    private String localAmount;
}
