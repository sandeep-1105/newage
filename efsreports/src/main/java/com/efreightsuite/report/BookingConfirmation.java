package com.efreightsuite.report;

import lombok.Data;

@Data
public class BookingConfirmation {

    String companyName;
    String companyAddressLine1;
    String refNumber;
    String refPlace;
    String refDate;
    String fax;
    String phone;
    String currentDate;
    String forwarderName;
    String attnNumber;
    String portOrigin;
    String pol;
    String pod;
    String destination;
    String commodity;
    String flightNo;
    String hawbNo;
    String weight;
    String volumeWeight;
    String companyAddressLine2;
    String companyAddressLine3;
    String forwarderAddressLine1;
    String forwarderAddressLine2;
    String forwarderAddressLine3;
    String forwarderAddressLine4;

}
