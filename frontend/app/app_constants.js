app.constant("appConstant",{
	appName : "My App",
	 
    weightValExpr : "^[+]?([0-9]{0,11}(?:[\.][0-9]{0,2})?|\.[0-9]+)$",
    piecesValExpr : "^[+]?([0-9]{0,11}(?:[\.][0-9]{0,0})?|\.[0-9]+)$",
    freeText100CharsExpr : "[ A-Za-z0-9\n_@.#&+-]{0,100}",
    freeText250CharsExpr : "[ A-Za-z0-9\n_@.#&+-]{0,250}",
    freeText4000CharsExpr : "[ A-Za-z0-9\n_@.#&+-]{0,4000}",
    alphaNumeric10CharsExpr : "^[ A-Za-z0-9]{0,10}$",
    alphaNumeric30CharsExpr : "^[ A-Za-z0-9]{0,30}$",
    alphaNumeric50CharsExpr : "^[ A-Za-z0-9]{0,50}$",
    alphaNumeric100CharsExpr : "^[ A-Za-z0-9]{0,100}$",
    alphaNumeric250CharsExpr : "^[ A-Za-z0-9]{0,250}$",
    percentageExpr : "^[0-9.]{0,100}$",
    chargeNumberOfUnitList : ["-45", "+45", "+100", "+250", "+300", "+500", "+1000"],
    multipleEmailExpr : /^(([^<>()\[\]\\.,,:\s@"]+(\.[^<>()\[\]\\.,,:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
    testExpr : "sandi",
    
    
    /*Regular Expression - Start Here*/
    REG_EXP_FLIGHT_NUMBER : "flight.number",
    REG_EXP_MOBILE_NUMBER : "mobile.number",
  	REG_EXP_PHONE_NUMBER : "phone.number",
  	REG_EXP_FAX_NUMBER : "fax.number",
  	REG_EXP_SINGLE_EMAIL : "single.email",
  	REG_EXP_MULTIPLE_EMAIL : "multiple.email",
  	Reg_Exp_AMOUNT_TEN_SIX : "amount.ten.six",
  	
    Reg_Exp_Master_Service_Code : "master.service.code",
    Reg_Exp_Master_Service_Name : "master.service.name",
    
    Reg_Exp_Master_Company_Name : "master.company.name",
    Reg_Exp_Master_Company_Code : "master.company.code",
    
    
    Reg_Exp_Master_Carrier_Code : "master.carrier.code",
    Reg_Exp_Master_Carrier_Name : "master.carrier.name",
    Reg_Exp_Master_Air_Carrier_Code : "master.air.carrier.code",
	Reg_Exp_Master_Air_Carrier_No : "master.air.carrier.no",
	Reg_Exp_Master_Air_Carrier_Iata_No : "master.air.carrier.iata.no",
	Reg_Exp_Master_Carrier_No : "master.carrier.no",
	Reg_Exp_Master_Carrier_Scac_Code : "master.carrier.scac.code",

	Reg_Exp_Master_Edi_Type : "master.edi.type",
	Reg_Exp_Master_Edi_Subtype : "master.edi.subtype",
	Reg_Exp_Master_Edi_Value : "master.edi.value",

	Reg_Exp_Master_Carrier_Rate_or_amount : "master.carrier.rate.or.amount",
	
	Reg_Exp_Master_Unit_Code : "master.unit.code",
	Reg_Exp_Master_Unit_Name : "master.unit.name",
	Reg_Exp_Master_Unit_Calculation_1 : "master.master.unit.calculation.1",
	Reg_Exp_Master_Unit_Calculation_2 : "master.master.unit.calculation.2",
		
	Reg_Exp_Master_Tos_Code : "master.tos.code",
	Reg_Exp_Master_Tos_Name : "master.tos.name",
	
	Reg_Exp_Master_Pack_Code : "master.pack.code",
	Reg_Exp_Master_Pack_Name : "master.pack.name",

	Reg_Exp_Master_Charge_Code : "master.charge.code",
	Reg_Exp_Master_Charge_Name : "master.charge.name",
	
	Reg_Exp_Master_HS_CODE : "master.hs.code",
	Reg_Exp_Master_HS_NAME : "master.hs.name",

	Reg_Exp_Master_CATEGORY_CODE : "master.category.code",
	Reg_Exp_Master_CATEGORY_NAME : "master.category.name",
	
	Reg_Exp_Master_CURRENCY_BUY_RATE : "master.currency.buy.rate",
	Reg_Exp_Master_CURRENCY_SELL_RATE : "master.currency.sell.rate",

		
	Reg_Exp_Master_DOCUMENT_PREFIX_CODE : "master.document.prefix.code",
	Reg_Exp_Master_DOCUMENT_PREFIX_NAME : "master.document.prefix.name",
	Reg_Exp_Master_DOCUMENT_PREFIX_PREFIX_VALUE : "master.document.prefix.prefix.value",
	Reg_Exp_Master_DOCUMENT_PREFIX_SUFFIX_VALUE : "master.document.prefix.suffix.value",
		
	Reg_Exp_Master_CURRENCY_CODE : "master.currency.code",
    Reg_Exp_Master_CURRENCY_NAME : "master.currency.name",
    Reg_Exp_Master_CURRENCY_PREFIX : "master.currency.prefix",
    Reg_Exp_Master_CURRENCY_SUFFFIX  : "master.currency.sufffix",
    

	Reg_Exp_Master_PARTY_GROUP_CODE : "master.party.group.code",
	Reg_Exp_Master_PARTY_GROUP_NAME : "master.party.group.name",

	Reg_Exp_Master_PARTY_TYPE_CODE : "master.party.type.code",
	Reg_Exp_Master_PARTY_TYPE_NAME : "master.party.type.name",

	Reg_Exp_Master_REFERENCE_TYPE_CODE : "master.reference.type.code",
	Reg_Exp_Master_REFERENCE_TYPE_NAME : "master.reference.type.name",
		
	Reg_Exp_Master_COST_CENTER_NAME : "master.cost.center.name",
	Reg_Exp_Master_COST_CENTER_CODE : "master.cost.center.code",
		
	Reg_Exp_Master_SERVICE_TYPE_NAME : "master.service.type.name",
    Reg_Exp_Master_SERVICE_TYPE_CODE : "master.service.type.code",

    	
    Reg_Exp_Master_AUTO_MAIL_NAME : "master.auto.mail.name",
    Reg_Exp_Master_AUTO_MAIL_CODE : "master.auto.mail.code",
    
    Reg_Exp_Master_AUTO_MAIL_GROUP_NAME : "master.auto.mail.group.name",
    Reg_Exp_Master_AUTO_MAIL_GROUP_CODE : "master.auto.mail.group.code",
    
    Reg_Exp_Master_PORT_GROUP_CODE : "master.port.group.code",
    Reg_Exp_Master_PORT_GROUP_NAME : "master.port.group.name",

    Reg_Exp_Master_PORT_CODE : "master.port.code",
    Reg_Exp_Master_PORT_NAME : "master.port.name",

    Reg_Exp_Master_PORT_CODE_OTHER : "master.port.code.other",
    Reg_Exp_Master_PORT_CODE_AIR : "master.port.code.air",
    Reg_Exp_Master_PORT_CODE_SEA : "master.port.code.sea",
		
    Reg_Exp_Master_PARTY_NAME : "master.party.name",
	Reg_Exp_Master_PARTY_FIRST_NAME : "master.party.first.name",
	Reg_Exp_Master_PARTY_LAST_NAME : "master.party.last.name",
	
	
	Reg_Exp_Master_PARTY_VAT_NO : "master.party.vat.number",
	Reg_Exp_Master_PARTY_PAN_NO : "master.party.pan.number",
	Reg_Exp_Master_PARTY_CST_NO : "master.party.cst.number",
	Reg_Exp_Master_PARTY_SVT_NO : "master.party.svt.number",
	Reg_Exp_Master_PARTY_SVAT_NO : "master.party.svat.number",
	Reg_Exp_Master_PARTY_RAC_NO : "master.party.rac.number",
	Reg_Exp_Master_PARTY_TIN_NO : "master.party.tin.number",
	Reg_Exp_Master_PARTY_IATA_CODE : "master.party.iata.code",
	Reg_Exp_Master_PARTY_REGIONAL_NAME : "master.party.regional.name",
	Reg_Exp_Master_PARTY_BRANCH_SLNO : "master.party.branch.slno",
	Reg_Exp_Master_PARTY_BANK_DLR_CODE : "master.party.bank.drl.code",
	Reg_Exp_Master_PARTY_SHIPPER_VALIDATION_NO : "master.party.shipper.validation.no",
	Reg_Exp_Master_PARTY_ID_NUMBER : "master.party.id.number",
	Reg_Exp_Master_PARTY_BOND_HOLDER : "master.party.bond.holder",
	Reg_Exp_Master_PARTY_BOND_NUMBER : "master.party.bond.no",
	Reg_Exp_Master_PARTY_BOND_SECURITY_CODE : "master.party.bond.security.code",
	Reg_Exp_Master_PARTY_ESTIMATED_REVENUE : "master.party.estimated.revenue",
    Reg_Exp_Master_PARTY_NO_OF_SHIPMENTS : "master.party.no.of.shipments",
    Reg_Exp_Master_PARTY_NO_OF_UNITS : "master.party.no.of.units",
    Reg_Exp_Master_PARTY_WAREHOUSE_PROVIDER_NAME : "master.party.warehouse.provider.name",
    Reg_Exp_Master_PARTY_ZIP_CODE : "master.party.zip.code",
    Reg_Exp_Master_PARTY_CONTACT_PERSON : "master.party.contact.person",
    	
	Reg_Exp_Master_PARTY_CREDIT_DAYS : "master.party.credit.days",
	Reg_Exp_Master_PARTY_CREDIT_AMOUNT : "master.party.credit.amount",
	Reg_Exp_Master_PARTY_PUBLISHED_CREDIT_DAYS : "master.party.published.credit.days",
	Reg_Exp_Master_PARTY_ACC_NO : "master.party.acc.number",
	Reg_Exp_Master_PARTY_PIN : "master.party.pin",
	Reg_Exp_Master_PARTY_TSANO : "master.party.tsano",
	Reg_Exp_Master_PARTY_SPOTNO : "master.party.spotno",
	
	REG_EXP_GROSS_WEIGHT : "gross.weight",
	REG_EXP_VOLUME_WEIGHT : "volume.weight",
	REG_EXP_NO_OF_PIECES : "no.of.pieces",
	REG_EXP_GROSS_WEIGHT_POUND : "gross.weight.pound",
	REG_EXP_VOLUME_WEIGHT_POUND : "volume.weight.pound",
	REG_EXP_VOLUME_WEIGHT_CBM : "volume.weight.cbm",
	REG_EXP_GROSS_WEIGHT_KG : "gross.weight.kg",
	REG_EXP_VOLUME_WEIGHT_KG : "volume.weight.kg",
	REG_EXP_RATE_CLASS : "rate.class",
	REG_EXP_RATE_PER_CHARGE : "rate.per.charge",
	REG_EXP_MAWB_NUMBER : "mawb.number",
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	REG_EXP_DIMENSION_NO_OF_PIECES : "dimension.no.of.pieces",
	REG_EXP_DIMENSION_LENGTH : "dimension.length",
	REG_EXP_DIMENSION_WIDTH : "dimension.width",
	REG_EXP_DIMENSION_HEIGHT : "dimension.height",
	REG_EXP_DIMENSION_GROSS_WEIGHT : "dimension.gross.weight",
	REG_EXP_SHIPMENT_BROKERAGE_PERCENTAGE : "shipment.brokerage.percentage",
	
    Reg_Exp_Master_PARTY_CONTACT_PERSONAl_CHILDREN  : "party.contact.personal.children",
    
    Reg_Exp_Master_PARTY_ADD_PICKUP_HOURS : "party.address.pickup.hours",
    Reg_Exp_Master_PARTY_DETAIL_CC_PERCENTAGE : "party.detail.cc.percentage",
    Reg_Exp_Master_PARTY_DETAIL_TDS_PERCENTAGE : "party.detail.tds.percentage",
    Reg_Exp_Master_CURRENCY_RATE : "currency.rate",

    Reg_Exp_RATE_TWO_DECIMAL : "rate.twodecimal",
    Reg_Exp_RATE_THREE_DECIMAL : "rate.threedecimal",
    Reg_Exp_FINANCE_ROE : "finance.roe",
    Reg_Exp_FINANCE_UNIT : "finance.unit",
    Reg_Exp_FINANCE_REFERENCE_NO :"finance.reference" ,
    
    Reg_Exp_Cfs_Receive_Code : "cfs.receive.code",
    Reg_Exp_Cfs_Receive_Name : "cfs.receive.name",

    Reg_Exp_Cfs_Receive_Pro_Number : "cfs.receive.pro.number",
    Reg_Exp_Cfs_Receive_Warehouse_location : "cfs.receive.warehouse.location",
    Reg_Exp_Cfs_Receive_Shipper_Ref_No : "cfs.receive.shipper.reference.number",
    Reg_Exp_Cfs_Receive_On_Hand : "cfs.receive.on.hand",
    Reg_Exp_Cfs_Receive_Entry_Notes : "cfs.receive.entry.notes",
    	
    Reg_Exp_TRIGGER_TYPE_CODE : "trigger_type.code",
    Reg_Exp_TRIGGER_TYPE_NAME : "trigger_type.name",
    
    Reg_Exp_AES_NO : "aes.no",
    Reg_Exp_AES_ENTRY_NO : "aes.entry.no",
    Reg_Exp_AES_ID_NO : "aes.id.no",
    Reg_Exp_AES_NAME : "aes.name",
    Reg_Exp_AES_PIECES : "aes.pieces",
    Reg_Exp_AES_FOREIGN_TRADE_ZONE : "aes.foreign.trade.zone",
    Reg_Exp_AES_ADDRESS : "aes.address",
    Reg_Exp_AES_CONTACT_NO : "aes.contact.no",
    Reg_Exp_AES_ZIP_CODE : "aes.zip.code",
    Reg_Exp_AES_VALUE_USD : "aes.value.usd",
    Reg_Exp_AES_GROSS_WEIGHT : "aes.gross.weight",

   //work on me dec-29th
    	
    	   Reg_Exp_STOCK_CARRIER_REF_NO : "stock.carrierref.no",
    	
    		   Reg_Exp_FLIGHT_PLAN_CAPACITYRESERVED : "flightplan.capacityReserved",
    			Reg_Exp_FLIGHT_PLAN_FLIGHTFREQUENCY : "flightplan.flightFrequency",
    			Reg_Exp_FLIGHT_PLAN_FLIGHTNUMBER : "flightplan.flightNumber",

    				
    Reg_Exp_PO_ALPHA_TEN:"po.alphalengthten",
    Reg_Exp_PO_ALPHA_THIRTY:"po.alphalengththirty",
    Reg_Exp_PO_ALPHA_FIFTY:"po.alphalengthfifty",
    Reg_Exp_PO_ALPHA_HUNDRED:"po.alphalengthhundred",
    Reg_Exp_PO_ALPHA_TWOFIFTY:"po.alphalengthtwofifty",
    	 Reg_Exp_PO_ALPHA_FVEHND:"po.alphalengthfivehnd",
    Reg_Exp_PO_PERC:"po.percentage",
    	
    	
    //Regular expression for BankMaster	
    	Reg_Exp_Master_Bank_Name:"bank.name",
    	Reg_Exp_Master_Bank_Code:"bank.code",
    	Reg_Exp_Master_Bank_Smart_Bank_Code:"bank.smart.bank.code",
	
   //Reg for Event Master
    		Reg_Exp_MASTER_EVENT_NAME:"event.name",
    		Reg_Exp_MASTER_EVENT_CODE:"event.code",
    				
    		   	
    		    //Regular expression for Region	
    			Reg_Exp_Master_Region_Name:"region.name",
    			Reg_Exp_Master_Region_Code:"region.code",
    			
    			// Regular expression for Bill Of Entry
    			Reg_Exp_BOE_BILL_OF_ENTRY:"billofentry.billofentry",
    				
    //Regular expression for aes filer
    REG_EXP_MASTER_AES_FILER_CODE:"aesfiler.code",
    REG_EXP_MASTER_AES_FILER_TRANSMITTER_CODE:"aesfiler.transmitterCode",
    	
    	// Reason Master
    	Reg_Exp_Master_Reason_Name:"reason.name",
		Reg_Exp_Master_Reason_Code:"reason.code",
		
		
		Reg_Exp_Master_ServiceTaxCategory_Name:"servicetaxcategory.name",
		Reg_Exp_Master_ServiceTaxCategory_Code:"servicetaxcategory.code",
		
		
		Reg_Exp_Master_Charter_Name :"master.charter.name",
	    Reg_Exp_Master_Charter_Code :"master.charter.code",
	    Reg_Exp_Master_Account_Code : "master.account.code",
		Reg_Exp_Master_Account_Name : "master.account.name",
			
			Reg_Exp_Master_Comment_Name :"master.comment.name",
			Reg_Exp_Master_Comment_Code :"master.comment.code",
				
				Reg_Exp_Master_Daybook_Name :"master.Daybook.name",
				Reg_Exp_Master_Daybook_Code :"master.Daybook.code",
				Reg_Exp_Master_Daybook_Cheque_From :"master.Daybook.cheque.from",
				Reg_Exp_Master_Daybook_Cheque_To :"master.Daybook.cheque.to",
				Reg_Exp_Master_Daybook_Report_Name :"master.Daybook.report.name",
					
					Reg_Exp_Master_Location_Name :"master.location.name",
					Reg_Exp_Master_Location_Code :"master.location.code",
					
					Reg_Exp_Master_Buyer_Consolidation_Name:"buyer.bcname",
	    			Reg_Exp_Master_Buyer_Consolidation_Code:"buyer.bccode"
    			
    /*Regular Expression - End Here*/
});
