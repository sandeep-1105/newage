angular.module('NewAge')
	.factory('appMetaFactory',['$resource', function($resource) {
        return {
            country: $resource('/api/v1/dynamicfield/country/:countryId', {}, {
                dynamicFields: { method : 'GET', params: { countryId : '' }, isArray: false }
            }),
            tos: $resource('/api/v1/tosmaster/getall', {}, {
                findall: { method : 'GET', isArray: false }
            }),

            location: $resource('/api/v1/locationmaster/get/searchByCompany/keyword/:companyId', {}, {
                search: { method : 'POST', params: { companyId : '' }, isArray: false }
            }),
            
            pricing : $resource('/api/v1/pricing/getcharge/:enquiryId/:originId/:destinationId', {}, {
                findByOriginAndDestination: { method : 'GET', params: { enquiryId: '', originId : '',destinationId :'' }, isArray: false }
            }),
            reportInvoiceValidation : $resource('/api/v1/shipment/invoicechecking/:serviceId', {}, {
                findByServiceId: { method : 'GET', params: { serviceId: ''}, isArray: false }
            }) ,
            fetchQuotationCharge : $resource('/api/v1/pricing/getquotationcharge', {}, {
                findByOD: { method : 'POST', isArray: false }
            }),
            fetchRecentHistoryGroup : $resource('/api/v1/recenthistory/group', {}, {
                get: { method : 'GET', isArray: false }
            }) ,
            fetchFightPlanSchedule : $resource('/api/v1/flightplan/getschedule', {}, {
                get: { method : 'POST', isArray: false }
            }) ,
            fetchStockMawb : $resource('/api/v1/stock/mawblist', {}, {
                get: { method : 'POST', isArray: false }
            }) ,
            shipmentService : $resource('/api/v1/shipment/service/signoff', {}, {
                signOff: { method : 'POST', isArray: false }
            }),
            shipmentServiceUnSignOff : $resource('/api/v1/shipment/service/unsignoff', {}, {
                unSignOff: { method : 'POST', isArray: false }
            }),
            consol : $resource('/api/v1/consol/signoff', {}, {
                signOff: { method : 'POST', isArray: false }
            }),
            consolUnSignOff : $resource('/api/v1/consol/unsignoff', {}, {
                unSignOff: { method : 'POST', isArray: false }
            }),
            isClaimed : $resource('/api/v1/activity/iscliamed/:taskId', {}, {
                workFlow: { method : 'GET', params: { taskId: ''}, isArray: false }
            }),
            claim : $resource('/api/v1/activity/cliam/:taskId/:userId', {}, {
                workFlow: { method : 'GET', params: { taskId: '', userId: ''}, isArray: false }
            }),
            myTaskList : $resource('/api/v1/activity/task', {}, {
                workFlow: { method : 'POST', isArray: false }
            }) ,
            airExportFlow : $resource('/api/v1/activiti/airexport/group/:groupName', {}, {
                workFlow: { method : 'POST', params: { groupName: ''}, isArray: false }
            }),
            airImportFlow : $resource('/api/v1/activiti/airimport/group/:groupName', {}, {
                workFlow: { method : 'POST', params: { groupName: ''}, isArray: false }
            }),
            DocumentAttach : $resource('/api/v1/attachment/auto', {}, {
                auto: { method : 'POST', isArray: false }
            }) ,
            saveUnsavedForm : $resource('/api/v1/history/save', {}, {
                save: { method : 'POST', isArray: false }
            }),
            getUnsavedForm : $resource('/api/v1/history/get', {}, {
                query: { method : 'POST', isArray: false }
            }),
            fetchJobLedger : $resource('/api/v1/finance/jobledger', {}, {
            	query: { method : 'POST', isArray: false }
            }) ,
            generateNum: $resource('/api/v1/consol/gennum', {}, {
            	 cangenerate: { method : 'POST', isArray: false }
            }),
            airImportExportCount : $resource('/api/v1/activiti/airexportimport/getcount', {}, {
                workFlow: { method : 'GET', isArray: false }
            })
        };
    }])