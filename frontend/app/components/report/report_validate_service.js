/**
 *    @author            Created Date                 Description                             Version
 * K.SATHISH KUMAR        09-08-2017        Used to validation/business logic             0.1
 *  
 */
app.service('reportValidation', ['$rootScope', 'Notification', 'ValidateUtil',
    function($rootScope, Notification, ValidateUtil) {
        //can received or not
        this.checkIsCanReceived = function(consol, document, Screen) {

            if (Screen == 'MasterShipment') {

                var isNumGenerate = true;
                if (consol != undefined && consol.shipmentLinkList != undefined && consol.shipmentLinkList != null && consol.shipmentLinkList.length != undefined && consol.shipmentLinkList.length != 0) {
                    for (var i = 0; i < consol.shipmentLinkList.length; i++) {
                        if (consol.shipmentLinkList[i].service != undefined && consol.shipmentLinkList[i].service.documentList != null) {
                            for (var j = 0; j < consol.shipmentLinkList[i].service.documentList.length; j++) {
                                if (consol.shipmentLinkList[i].service.documentList[j] != undefined && (consol.shipmentLinkList[i].service.documentList[j].canIssuedDate == undefined || consol.shipmentLinkList[i].service.documentList[j].canIssuedDate == null ||
                                        consol.shipmentLinkList[i].service.documentList[j].canIssuedBy == undefined ||
                                        consol.shipmentLinkList[i].service.documentList[j].canIssuedBy == null || consol.shipmentLinkList[i].service.documentList[j].canIssuedBy.id == undefined)) {
                                    isNumGenerate = false;
                                    Notification.error($rootScope.nls["ERR96562"] + " - " + consol.shipmentLinkList[i].service.shipmentUid);
                                    break;
                                }
                            }

                        }
                    }
                }
                return !isNumGenerate;

            } else {
                if (document != undefined && (document.canIssuedDate == undefined || document.canIssuedDate == null || document.canIssuedBy == undefined ||
                        document.canIssuedBy == null || document.canIssuedBy.id == undefined)) {
                    Notification.error($rootScope.nls["ERR96562"]);
                    return;
                }

            }
        }

        this.doValidation = function(consol) {

            var isvalid = true;
            if (consol != undefined && consol.shipmentLinkList != undefined && consol.shipmentLinkList != null && consol.shipmentLinkList.length != undefined && consol.shipmentLinkList.length != 0) {

                if (ValidateUtil.isNotNull(consol.consolDocument.carrierDoNo)) {

                    Notification.error($rootScope.nls["ERR95915"]);
                    return !isvalid;
                } else if (ValidateUtil.isNotNull(consol.consolDocument.igmNo)) {
                    Notification.error($rootScope.nls["ERR95918"]);
                    return !isvalid;
                }

                for (var i = 0; i < consol.shipmentLinkList.length; i++) {

                    if (consol.shipmentLinkList[i].service.documentList != undefined &&
                        consol.shipmentLinkList[i].service.documentList != null && consol.shipmentLinkList[i].service.documentList.length > 0) {
                        for (var j = 0; j < consol.shipmentLinkList[i].service.documentList.length; j++) {
                            if (ValidateUtil.isNotNull(consol.shipmentLinkList[i].service.documentList[j].referenceNo)) {
                                Notification.error($rootScope.nls["ERR95919"] + " in shipment -" + consol.shipmentLinkList[i].service.shipmentUid);
                                isvalid = false;
                                break;
                            } else if (ValidateUtil.isNotNull(consol.shipmentLinkList[i].service.documentList[j].routeNo)) {
                                Notification.error($rootScope.nls["ERR95920"] + " in shipment -" + consol.shipmentLinkList[i].service.shipmentUid);
                                isvalid = false;
                                break;
                            } else if (ValidateUtil.isNotNull(consol.shipmentLinkList[i].service.documentList[j].grossWeight)) {
                                Notification.error($rootScope.nls["ERR90475"] + " in shipment -" + consol.shipmentLinkList[i].service.shipmentUid);
                                isvalid = false;
                                break;
                            } else if (ValidateUtil.isNotNull(consol.shipmentLinkList[i].service.documentList[j].mawbNo)) {
                                Notification.error($rootScope.nls["ERR96118"] + "in shipment -" + consol.shipmentLinkList[i].service.shipmentUid);
                                isvalid = false;
                                break;
                            }
                        }
                        if (!isvalid) {
                            return isvalid;
                        }
                    } else {
                        return !isvalid;
                    }
                }
                return isvalid
            } else {
                return !isvalid;
            }

        }

    }
]);