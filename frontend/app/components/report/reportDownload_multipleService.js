app.factory('downloadMultipleFactory', [ '$q', '$timeout', '$window', '$http', '$sce', '$filter', '$rootScope',
	function($q, $timeout, $window, $http, $sce, $filter, $rootScope) {
		return {
			download : function(reportDownloadRequest) {
				console.log("Ref Id : "+reportDownloadRequest.resourceId+" ; Down File Type : "+reportDownloadRequest.downloadFileType+" ; Preview Option : "+reportDownloadRequest.downloadOption+" ; Report Name : "+reportDownloadRequest.reportName);
				var defer = $q.defer();
				var reqConfig = {
					url : $rootScope.userProfile.reportServerBasePath+'/api/v1/report/download/multiple',
					method : "POST",
					responseType : 'arraybuffer',
					data : reportDownloadRequest,
					headers: { 
						'SAAS_ID':localStorage.saasId,
						'Cookie':'SESSION='+$rootScope.userProfile.sessionId
					}
				};
				$http(reqConfig).success(function(data, status, headers, config) {
					console.log("Rest Api Called but there is no response:: ", data);
				});
				return defer.promise;
			},
		};
	} ]);
