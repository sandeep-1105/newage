app.factory('downloadFactory', ['$q', '$timeout', '$window', '$http', '$sce', '$filter', '$rootScope', 'Notification',
    function($q, $timeout, $window, $http, $sce, $filter, $rootScope, Notification) {
        return {
            download: function(reportDownloadRequest) {
                if (reportDownloadRequest.downloadOption == 'eMail') {
                    Notification.success("Your " + reportDownloadRequest.downloadOption + " is initiated ");
                } else {
                    Notification.success("Your " + reportDownloadRequest.downloadOption + " is in progress ");
                }
                console.log("Ref Id : " + reportDownloadRequest.resourceId + " ; Down File Type : " + reportDownloadRequest.downloadFileType + " ; Preview Option : " + reportDownloadRequest.downloadOption + " ; Report Name : " + reportDownloadRequest.reportName);
                var defer = $q.defer();
                var reqConfig = {
                    url: $rootScope.userProfile.reportServerBasePath + '/api/v1/report/download',
                    method: "POST",
                    responseType: 'arraybuffer',
                    data: reportDownloadRequest,
                    headers: {
                        'SAAS_ID': localStorage.saasId,
                        'Cookie': 'SESSION=' + $rootScope.userProfile.sessionId
                    }
                };
                $http(reqConfig).success(function(data, status, headers, config) {
                    $rootScope.clientMessage = null;
                    if (data.byteLength == 0 || data.byteLength > 0) {
                        var blobResponseType = 'application/pdf';
                        var _date = $filter('date')(new Date(), 'dd-MM-yyyy');
                        console.log("CUrrent Date :: " + _date);
                        var filePrefixName = reportDownloadRequest.reportName + '_' + reportDownloadRequest.resourceId + "_" + _date;
                        var downloadReportFileName = filePrefixName + ".pdf";
                        if (reportDownloadRequest.downloadFileType === "PDF") {
                            blobResponseType = 'application/pdf';
                            downloadReportFileName = filePrefixName + ".pdf";
                        } else if (reportDownloadRequest.downloadFileType === "XLS") {
                            blobResponseType = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
                            downloadReportFileName = filePrefixName + ".xlsx";
                        } else if (reportDownloadRequest.downloadFileType === "RTF") {
                            blobResponseType = 'application/rtf';
                            downloadReportFileName = filePrefixName + ".rtf";
                        }

                        if (reportDownloadRequest.downloadOption === "Download") {
                            console.log("Hey Iam IN Download");
                            var blob = new Blob([data], {});
                            saveAs(blob, downloadReportFileName);
                            defer.resolve(data)
                        } else if (reportDownloadRequest.downloadOption === "eMail") {
                            console.log("Email option called");
                            /*var blob = new Blob([data], {});
								saveAs(blob, downloadReportFileName);
							*/
                        } else if (reportDownloadRequest.downloadOption === "Print") {
                            console.log("PDf Print..");
                            var file = new Blob([data], {
                                type: blobResponseType
                            });
                            var fileURL = URL.createObjectURL(file);
                            var contentFileURL = $sce.trustAsResourceUrl(fileURL);
                            var winparams = 'dependent=yes,locationbar=no,scrollbars=yes,menubar=yes,' +
                                'resizable,screenX=50,screenY=50,width=850,height=1050';
                            var printWindow = window.open(contentFileURL, "_blank", "PDF", winparams);
                            printWindow.focus();
                            printWindow.print();
                            console.log("Focused ... ");
                            defer.resolve(data)
                        } else {
                            console.log("Hey Iam IN Download");
                            var file = new Blob([data], {
                                type: blobResponseType
                            });
                            var fileURL = URL.createObjectURL(file);
                            var contentFileURL = $sce.trustAsResourceUrl(fileURL);
                            $window.open(contentFileURL);
                            defer.resolve(data)
                            console.log("Hey Iam IN Download");
                        }
                    } else {
                        $rootScope.clientMessage = $rootScope.nls["ERR0700011"];
                    }
                    $rootScope.mainpreloder = false;
                }).error(function(data, status, headers, config) {
                    console.error("Problem while downloading..... ", data);
                    $rootScope.mainpreloder = false;
                    defer.reject(null);
                });
                return defer.promise;
            },
            downloadAttachment: function(downloadUrl, id, fileName) {
                $http({
                    url: $rootScope.baseURL + downloadUrl + id,
                    method: "GET",
                    responseType: 'arraybuffer'
                }).success(function(data, status, headers, config) {
                    var blob = new Blob([data], {});
                    saveAs(blob, fileName);
                }).error(function(data, status, headers, config) {
                    console.error("Problem while downloading..... ", data);
                });
            }
        };
    }
]);