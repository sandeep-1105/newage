/**
 * bankMasterFactory-used to connect with server
 */

app.factory('documentMasterFactory', function($resource) {
	return {
		findById : $resource('/api/v1/documentmaster/get/id/:id', {}, {
			 query : {method : 'GET', params : {id : ''}, isArray : false}
		}),
	   create:$resource('/api/v1/documentmaster/create', {}, {
			save : {method : 'POST'}
		}),
		update : $resource('/api/v1/documentmaster/update', {}, {
			query : {method : 'POST'}
		}),
		remove : $resource('/api/v1/documentmaster/delete/:id', {}, {
			query : {method : 'DELETE', params : {id : ''}}
		}),
		search : $resource('/api/v1/documentmaster/get/search', {}, {
			query : {method : 'POST'}
		})
	};
})
