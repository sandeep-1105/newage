app.controller("document_master_ctrl", function($scope, $rootScope,$modal,ngDialog,$stateParams, $state, Notification,cloneService,documentMasterFactory, roleConstant) {
    
	
	
	$scope.YesNo = ["Yes",'No'];
	
	
	$scope.init = function() {
		$scope.documentMasterSearchDto = {};
		$scope.limitArr = [10,15,20];
		$scope.page = 0;
		$scope.limit = 10;
		$scope.totalRecord = 0;
		$scope.search();
		
	};
	
	
    $scope.initAdd=function(){
		
		if($scope.documentMasterList==undefined || $scope.documentMasterList==null){
			$scope.documentMasterList=[];
			$scope.documentMasterList.push({'isProtected':false});
		}
		$scope.firstFocus = true;
		$scope.errorArray = [];
		
	}
    
    var myOtherModal123 = $modal({scope:$scope,templateUrl: 'errorPopUp.html', show: false,keyboard:true});
    var errorOnRowIndex = null;
    
    $scope.errorShow = function(errorObj,index){
        $scope.errList = errorObj.errTextArr;
        // Show when some event occurs (use $promise property to ensure the template has been loaded)
        errorOnRowIndex = index;
        myOtherModal123.$promise.then(myOtherModal123.show);
    };
		
	$scope.clickOnTab=function(tab){
		if(tab=='documentmaster'){
			$scope.detailTab='documentmaster';	
			$scope.init();
     }
	}
	
	
	
	$scope.search = function() {
		console.log("Search is called....");
		$scope.documentMasterSearchDto.selectedPageNumber = $scope.page;
		$scope.documentMasterSearchDto.recordPerPage = $scope.limit;
		documentMasterFactory.search.query($scope.documentMasterSearchDto).$promise.then(function(data, status) {
			$scope.totalRecord = data.responseObject.totalRecord;
			var tempArr = [];
			var resultArr = [];
			tempArr = data.responseObject.searchResult;
			console.log("tempArr", tempArr);
			var tempObj = {};
			angular.forEach(tempArr,function(item,index) {
				tempObj = item;
				tempObj.no = (index+1)+($scope.page*$scope.limit);
				tempObj.isProtected=tempObj.isProtected?'Yes':'No';
				resultArr.push(tempObj);
				tempObj = {};
			});
			$scope.documentArray = resultArr;
			console.log("$scope.appConfigArray ", $scope.appConfigArray);
		});
	}

	

	$scope.documentConfigHead=[
		{
			"name":"#",
			"width":"w25px",
			"model":"no",
			"search":false
		},{
			"name": "Name",
			"model": "documentName",
			"search": true,
			"wrap_cell": true,
			"type": "input",
			"sort": true,
			"width": "w300px",
			"prefWidth": "300",
			"key": "searchDocumentName"
		},
		{
			"name": "Code",
			"search": true,
			"model": "documentCode",
			"wrap_cell": true,
			"type": "input",
			"sort": true,
			"width": "w150px",
			"prefWidth": "150",
			"key": "searchDocumentCode"
		},{
			"name": "Is Protected",
			"model": "isProtected",
			"search": true,
			"wrap_cell": true,
			"type": "select",
			"sort": true,
			"data":$scope.YesNo,
			"width": "w100px",
			"prefWidth": "100",
			"key": "isProtected"
		},{
			"name":"Status",
			"width":"w150px",
			"model":"status",
			"search":true,
			"type":"select",
			"data":$rootScope.enum['LovStatus'],
			"key":"searchStatus",
			"prefWidth": "150",
			"sort":true
		},
		{
			"name": "Action",
			"model": "",
			"search": false,
			"wrap_cell": true,
			"type": "action",
			"actype": "action",
			"sort": false,
			"width": "w100px",
			"prefWidth": "100",
			"key": ""
		}
	];

	$scope.sortSelection = {
			sortKey:"documentName",
			sortOrder:"asc"
	}
	
	$scope.limitChange = function(item){
		console.log("Limit Change is called...", item);
		$scope.page = 0;
		$scope.limit = item;
		$scope.search();
	}
	
	$scope.changeSearch = function(param) {
		
		console.log("Change Search", param);
		
		for (var attrname in param) {
			$scope.documentMasterSearchDto[attrname] = param[attrname];
		}
		
		$scope.page = 0;
		console.log("change search",$scope.documentMasterSearchDto);

		$scope.search();
	}
	
	$scope.sortChange = function(param){
		console.log("Sort Change Called.", param);
		
		$scope.documentMasterSearchDto.orderByType = param.sortOrder.toUpperCase();
		$scope.documentMasterSearchDto.sortByColumn = param.sortKey;

		$scope.search();
	}
	
	$scope.changepage = function(param){
		console.log("Change Page Called.", param);
		$scope.page = param.page;
		$scope.limit = param.size;
		$scope.search();
	}
	
	  $scope.isEmptyRow=function(obj){
          //return (Object.getOwnPropertyNames(obj).length === 0);
          var isempty = true; //  empty
          if (!obj) {
              return isempty;
          }
          var k = Object.getOwnPropertyNames(obj);
          for (var i = 0; i < k.length; i++){
              if (obj[k[i]]) {
                  isempty = false; // not empty
                  break;
              }
          }
          return isempty;
  }
	
	$scope.addRow=function(){
		$scope.firstFocus = true;
    	$scope.errorArray = [];
    	var listErrorFlag = false;
    	if($scope.documentMasterList != undefined && $scope.documentMasterList!= null && $scope.documentMasterList.length >0) {
    		for(var index = 0; index < $scope.documentMasterList.length; index++) {
    			if($scope.validation(index)) {
    				listErrorFlag = true;
    			}
    		}
    		if(listErrorFlag) {
    			return;
    		}  else {
    			$scope.documentMasterList.push({'isProtected':false});
        	}
    	} else {
    		$scope.documentMasterList.push({'isProtected':false});
    	}
	}
	
	$scope.deleteRow=function(index){
		$scope.documentMasterList.splice(index,1);	
	}
	
	$scope.cancel=function(){
		$state.go('layout.documentMaster');
	}

	 $scope.remove = function(param) {
		 if($rootScope.roleAccess(roleConstant.MASTER_FINANCE_DOCUMENT_MASTER_DELETE)){
	    	console.log("Remove ", param);
	    	var newScope = $scope.$new();
			newScope.errorMessage = $rootScope.nls["ERR1966"];
	    	ngDialog.openConfirm( {
	                    template : '<p>{{errorMessage}}</p>'
	                    + '<div class="ngdialog-footer">'
	                    + '<button type="button" class="btn btn-default btn-property cancel-btn" ng-click="closeThisDialog(0)">Cancel'
	                    + '<button type="button" class="btn btn-primary btn-property accent-btn" ng-click="confirm(1)">Yes'
	                    + '</button></div>',
	                    plain : true,
	                    scope : newScope,
	                    className : 'ngdialog-theme-default'
	                }).then( function(value) {
	                	documentMasterFactory.remove.query({ id : param.item.id }, function(data) {
	            			if (data.responseCode == 'ERR0') {
	        	        		Notification.success($rootScope.nls["ERR402"]);
	            				$scope.init();
	            			} else {
	            			}
	            		}, function(error) {
	            		});
	                }, function(value) {
	                });
		  }
	    }
	 
	 
	 $scope.validation = function(index){
		 $scope.errorArray[index] = {};
			$scope.errorArray[index].errTextArr = [];
			var errorFound = false;
			if($scope.documentMasterList[index].documentCode == undefined || $scope.documentMasterList[index].documentCode == null ||
					$scope.documentMasterList[index].documentCode.length == 0) {
				$scope.errorArray[index].errRow = true;
				$scope.errorArray[index].documentCode = true;
				
	            $scope.errorArray[index].errTextArr.push($rootScope.nls["ERR1953"]);	
	            errorFound = true;
			} else {
				$scope.errorArray[index].documentCode = false;
			}
			
			if($scope.documentMasterList[index].documentName == undefined || $scope.documentMasterList[index].documentName == null ||
					$scope.documentMasterList[index].documentName.length == 0) {
				$scope.errorArray[index].errRow = true;
				$scope.errorArray[index].documentName = true;
				
	            $scope.errorArray[index].errTextArr.push($rootScope.nls["ERR1955"]);	
	            errorFound = true;
			} else {
				$scope.errorArray[index].documentName = false;
			}
			if($scope.documentMasterList[index].isProtected == undefined || $scope.documentMasterList[index].isProtected == null ||
					$scope.documentMasterList[index].isProtected.length == 0) {
				$scope.errorArray[index].errRow = true;
				$scope.errorArray[index].isProtected = true;
				
	            $scope.errorArray[index].errTextArr.push($rootScope.nls["ERR1965"]);	
	            errorFound = true;
			} else {
				$scope.errorArray[index].isProtected = false;
			}
	 	return errorFound;		
	 }

    var checkAndUpdate = function(param) {
		console.log("checkAndUpdate ", param);
			$scope.update(param);
    }
    
    $scope.saveDocument=function(){
    	  $scope.errorArray = [];
		  var listErrorFlag = false;
		  if($scope.documentMasterList != undefined && $scope.documentMasterList!= null && $scope.documentMasterList.length >0) {
			  for(var index = 0; index < $scope.documentMasterList.length; index++) {
					  if(index==0){
						  if($scope.validation(index)) {
							  listErrorFlag = true;
						   }
						  }else{
							  if($scope.isEmptyRow($scope.documentMasterList[index])){
								  $scope.documentMasterList.splice(index,1);
								  listErrorFlag = false;
							  }else{
								  if($scope.validation(index)) {
									  listErrorFlag = true;
								  }
							  }
						  }
				  }
			  if(listErrorFlag) {
				  return;
			  }  else {
				  $scope.tmpDocumentMasterList = cloneService.clone($scope.documentMasterList);
			  }
			  documentMasterFactory.create.save($scope.tmpDocumentMasterList).$promise.then(function(data) {
					  if (data.responseCode == 'ERR0') {
						  console.log("CarrierRate Saved successfully..");
						  var params = {};
						  params.submitAction = 'Saved';
						  $rootScope.successDesc=$rootScope.nls["ERR400"];
						  $state.go("layout.documentMaster",params);
					  } else {
					  }
				  }, function(error) {
					   });
		    		
		        }  
    }
	
    
    $scope.update = function(param) {
    	console.log("Update ", param);
    	if(param.item.documentCode==undefined || param.item.documentCode==null || param.item.documentCode=="" ){
    		Notification.error($rootScope.nls["ERR1953"]+"at line no: "+(parseInt(param.index)+1));
    		return ;
    	}
    	if(param.item.documentName==undefined || param.item.documentName==null || param.item.documentName=="" ){
    		Notification.error($rootScope.nls["ERR1955"]+"at line no: "+(parseInt(param.index)+1));
    		return ;
    	}
    	if(param.item.status==undefined || param.item.status==null || param.item.status=="" ){
    		Notification.error($rootScope.nls["ERR1957"]+"at line no: "+(parseInt(param.index)+1));
    		return ;
    	}
    	documentMasterFactory.update.query(param.item).$promise.then(function(data) {
	        	if (data.responseCode == 'ERR0') {
	        		Notification.success($rootScope.nls["ERR401"]);
	        	} else {
	        	}
	        }, function(error) {
	        });
    }
    
    
    

   $scope.actionClick = function(param){
	   if($rootScope.roleAccess(roleConstant.MASTER_FINANCE_DOCUMENT_MASTER_VIEW)){
	        console.log(param);
	      if(param.type === 'SAVE') {
	        	checkAndUpdate(param);
	      }
	      if(param.type === 'DELETE') {
	    	  $scope.remove(param);
	       }
	   }
	 };

    
    
    $scope.addPage = function() {
    	if($rootScope.roleAccess(roleConstant.MASTER_FINANCE_DOCUMENT_MASTER_CREATE)){
    		  $state.go("layout.addDocumentMaster");
    	}
	};


	switch ($stateParams.action) {
	case "SEARCH":
		$scope.init();
		$rootScope.breadcrumbArr = [
		                  	      {
		                  	        label: "Master",
		                  	        state: "layout.documentMaster"
		                  	      },
		                  	      {
		                  	        label: "Document",
		                  	        state: "layout.documentMaster"
		                  	      }
		                  	    ];
		break;
	case "ADD":
		$rootScope.breadcrumbArr = [  { label: "Master",state: "layout.documentMaster"},
			                  	      { label: "Document",state: "layout.documentMaster"},
			                  	      {label: "Add Document ",state: "layout.addDocumentMaster" }
			                  	    ];
		
		
		$scope.initAdd();
		break;
		
		
		
}		
 });