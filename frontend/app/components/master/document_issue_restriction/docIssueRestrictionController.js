'use strict';

angular.module('NewAge')
	.controller('DocIssueRestrictionCtrl',
		function($http, $scope, $rootScope, ngTableParams, $stateParams, $state, $filter, $timeout, 
				docIssueRestrictionFactory, CompanyWiseLocation) {
			$scope.action = "GETALL";
			$scope.docIssueRestriction  = {};
			$scope.pagination = {};			
			$scope.filter = {
					location: "",
			        service: ""
			    };


			$scope.submit = function() {
				
				if($stateParams.action == "ADD" || $stateParams.action == "EDIT") {
					docIssueRestrictionFactory.query($scope.docIssueRestriction).$promise.then(function(data){
						if (data.responseCode == "ERR0") {
							console.log("Enquiry  updated Successfully")
							$state.go('layout.emailTemplate');
						} else {
							console.log("added Failed " + data.responseDescription)
						}
					},
					function(error){
						console.log("Error", error);
					});	
				}
				
			};
			
			
			$scope.rowSelect = function(selectedObj){
					console.log("rowSelect selectedObj :: ", selectedObj);
			}
			if($stateParams.action == "ADD") {
				$scope.action = "ADD";
				$scope.docIssueRestriction={};
			} else if($stateParams.action == "EDIT") {
				
				$scope.action = $stateParams.action;
				console.log("I am In EDIT , VIEW Block",$stateParams.emailTemplateId);
				$scope.docIssueRestriction={}; 
				docIssueRestrictionFactory.query({id:$stateParams.id},function(data) {
					//console.log("REsponse -- ",JSON.stringify(data));
					$scope.docIssueRestriction=data.responseObject;
					
				},
				function(error){ 
					console.log("Error");
				});	
			}
			
			
			if($stateParams.action == "GETALL") {
				
				$scope.pagination ={};
				$scope.tableParams = new ngTableParams({
					page: 1, 
					count: 10,
					filter: $scope.filter
				}, {
				    getData: function($defer, params) {
				    	$scope.pagination.selectedPageNumber = params.page() -1;
				    	$scope.pagination.recordPerPage = params.count(); 	
				    	$scope.pagination.location = $scope.filter.location;
				    	$scope.pagination.service = $scope.filter.service;
				    	console.log("Table Params :: ", params.filter());
				    	console.log("Table Params $scope.filter:: ", $scope.filter);
				    	docIssueRestrictionFactory.search.query($scope.pagination).$promise.then(function(data){
				    		$scope.totalElements = data.responseObject.totalRecord;
							$scope.serialNumber = ($scope.pagination.selectedPageNumber * $scope.pagination.recordPerPage);
							$scope.start = ($scope.pagination.selectedPageNumber * $scope.pagination.recordPerPage)+1;
							$scope.last = ((($scope.pagination.selectedPageNumber + 1) * $scope.pagination.recordPerPage) > $scope.totalElements) ?  $scope.totalElements :  (($scope.pagination.selectedPageNumber+1) * $scope.pagination.recordPerPage);
							
							params.total($scope.totalElements);
							
							params.settings({ counts: [] });
							//params.settings({ counts: data.response.last ? [] : [10, 25, 50] });
							
			                $defer.resolve(data.responseObject.searchResult);


							
				    	});
				    }
				});
			}
			
			
		});
