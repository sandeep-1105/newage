app.controller('sub_ledgerCtrl',function($rootScope,$state,$scope,$location,$window, ngDialog) {

	/************************************************************
	 * ux - by Muthu
	 * reason - ipad compatibility fix
	 *
	 * *********************************************************/
	$scope.deskTopView = true;

    $scope.addSubledger = function() {
  		console.log("add new screen");
  	    $state.go("layout.addEditSubLedgerCA");
  	};
  	
  	 $scope.backSubledger = function() {
   		console.log("back to list page");
   	    $state.go("layout.subLedgerCA");
   	};


	/********************* ux fix ends here *****************************/

					$scope.subledgerHeadArr = [ {
						"name" : "#",
						"width" : "col-xs-0half",
						"model" : "no",
						"search" : false
					
					},{
						"name" : "Name",
						"width" : "col-xs-4",
						"model" : "subledgerName",
						"search" : true,
						"type" : "text",
						"sort" : true,
						"key" : "SubledgerName"
						
					},{
						"name" : "Code",
						"width" : "col-xs-3",
						"model" : "subledgerCode",
						"search" : true,
						"wrap_cell" : true,
						"type" : "text",
						"sort" : true,
						"key" : "SubledgerCode"
						

					},{
						"name" : "Job Account",
						"width" : "col-xs-2",
						"model" : "accountType",
						"search" : true,
						"type" : "drop",
						"data":$rootScope.enum['YesNo'],
						"sort" : true,
						"key" : "AccountType"
						
					},
					{
						"name" : "Status",
						"width" : "col-xs-3",
						"model" : "status",
						"search" : true,
						"type" : "drop",
						"sort" : true,
						"data" : $rootScope.enum['LovStatus'],
						"key" : "Status"
						
						
					}
					];
	                $scope.subledgerArr = [
						{
							"no" : 1,
							
							"subledgerName" : "Pooja Expenses",
							"subledgerCode" : "S000080",
							
							"accountType" : "Yes",
							"status": "Active"
					    },
						{
							"no" : 2,
							
							"subledgerName" : "Proprietor Capital",
							"subledgerCode" : "S000118",
							
							"accountType" : "Yes",
							"status": "Active"
						},
						{
							"no" : 3,
							
							"subledgerName" : "Santha Capital",
							"subledgerCode" : "S000280",
							
							"accountType" : "Yes",
							"status": "Active"
						}
						,
						{
							"no" : 4,
							
							"subledgerName" : "Nantha Pvt Ltd",
							"subledgerCode" : "S000680",
							
							"accountType" : "Yes",
							"status": "Active"
						},
						{
							"no" : 5,
							
							"subledgerName" : "Import and Export  Corporation",
							"subledgerCode" : "S000780",
							
							"accountType" : "Yes",
							"status": "Active"
						}
						,
						{
							"no" : 6,
							
							"subledgerName" : "AirShipping Services",
							"subledgerCode" : "S000180",
							
							"accountType" : "Yes",
							"status": "Active"
						}
						,
						{
							"no" : 7,
							
							"subledgerName" : "Shipping Transporters",
							"subledgerCode" : "S000422",
							
							"accountType" : "Yes",
							"status": "Active"
						}
						,
						{
							"no" : 8,
							
							"subledgerName" : "Cargo Services",
							"subledgerCode" : "S000222",
							
							"accountType" : "Yes",
							"status": "Active"
						}
						,
						{
							"no" : 9,
							
							"subledgerName" : "Tarsport Corportation",
							"subledgerCode" : "S000580",
							
							"accountType" : "Yes",
							"status": "Active"
						}
						,
						{
							"no" : 10,
							
							"subledgerName" : "Cargo Import and Export Corporation",
							"subledgerCode" : "S000480",
							
							"accountType" : "Yes",
							"status": "Active"
						}
					];
	                $scope.dropValue="S00012XXX";
					$scope.sortSelection = {
						sortKey : "chargeName",
						sortOrder : "asc"
					};
					
				    $scope.setStatus = function(data){
			    		if(data=='Active'){
			    			return 'activetype';
			    		} else if(data=='Block'){
			    			return 'blockedtype';
			    		}else if(data=='Hide'){
			    			return 'hiddentype';
			    		}
			    	}
			        
					$scope.page=0;
					$scope.limit=10;
					 $limitArr = [10, 15, 20];
				      
				      $totalRecord = 100;
					$scope.rowSelect = function(){
						$scope.showDetail = true;

						/************************************************************
						 * ux - by Muthu
						 * reason - ipad compatibility fix
						 *
						 * *********************************************************/

						var windowInner=$window.innerWidth;
						if(windowInner<=1199){
							$scope.deskTopView = false;

						}
						else{
							$scope.deskTopView = true;
						}

						/********************* ux fix ends here *****************************/

					};
					$scope.cancel = function() {
						$scope.showDetail = false;
						$scope.showLogs = false;
						$scope.showHistory = false;
						$scope.chargeMaster = {};

						/************************************************************
						 * ux - by Muthu
						 * reason - ipad compatibility fix
						 *
						 * *********************************************************/
						$scope.deskTopView = true;

						/********************* ux fix ends here *****************************/
					};

					$scope.changepage = function(param) {
						$scope.page = param.page;
						$scope.limit = param.size;
						$scope.search();
					}

					$scope.currentTab = "address";
					$scope.activeTab = function(tab) {
						$scope.currentTab = tab;
					}
					$scope.view = function(selectedMaster) {
						console.log("View Charge Called");
						$scope.chargeMaster = selectedMaster;
					};
					$scope.limitChange = function(item) {
						$scope.page = 0;
						$scope.limit = [10, 15, 20];
						$scope.cancel();
						$scope.search();
					}
	$scope.add = function(){
		$location.path('/sub_ledger_master/sub_ledger_master_operation')
	};
	$scope.edit = function(){
		$location.path('/sub_ledger_master/sub_ledger_master_operation')
	};

					$scope.sortChange = function(param) {
						$scope.searchDto.orderByType = param.sortOrder
								.toUpperCase();
						$scope.searchDto.sortByColumn = param.sortKey;
						$scope.cancel();
						$scope.search();
					};

					$scope.deleteMaster = function() {
						ngDialog
								.openConfirm(
										{
											template : '<p>Are you sure you want to delete selected charge?</p>'
													+ '<div class="ngdialog-footer">'
													+ '<button type="button" class="btn btn-default btn-property cancel-btn" ng-click="closeThisDialog(0)">Cancel'
													+ '<button type="button" class="btn btn-primary btn-property accent-btn" ng-click="confirm(1)">Yes'
													+ '</button></div>',
											plain : true,
											className : 'ngdialog-theme-default'
										})
								.then(
										function(value) {
											ChargeRemove
													.remove(
															{
																id : $scope.chargeMaster.id
															},
															function(data) {
																if (data.responseCode == 'ERR0') {
																	console
																			.log("Charge deleted Successfully")
																	$scope
																			.init();
																} else {
																	console
																			.log("Charge deleted Failed "
																					+ data.responseDescription)
																}
															},
															function(error) {
																console
																		.log("Charge deleted Failed : "
																				+ error)
															});
										}, function(value) {
											console.log("deleted cancelled");

										});
					}
					$scope.changeSearch = function(param) {
						for ( var attrname in param) {
							$scope.searchDto[attrname] = param[attrname];
						}
						$scope.page = 0;
						$scope.cancel();
						$scope.search();

					}

					$scope.mouseOver = function(column) {

						if ($scope.searchDto.sortByColumn != column) {
							if ($scope.carrierSortIconClass == "icon-uparrow") {
								$scope.carrierSortIconClass = "icon-uparrow";
							} else {
								$scope.carrierSortIconClass = "icon-downarrow";
							}
						}
						$scope.searchDto.sortByColumn = column;

					}

					$scope.changeSorting = function(column) {
						$scope.sortIconCarrierName = " ";
						if ($scope.searchDto.sortByColumn != column) {
							$scope.carrierSortIconClass = "icon-downarrow";
							$scope.searchDto.orderByType = "ASC";
							$scope.search();

						} else {
							$scope.searchDto.sortByColumn = column;
							if ($scope.searchDto.orderByType == "ASC") {
								$scope.search();
								$scope.searchDto.orderByType = "DESC";
								$scope.carrierSortIconClass = "icon-uparrow";
							} else {
								$scope.searchDto.orderByType = "DESC";
								$scope.carrierSortIconClass = "icon-uparrow";
								$scope.search();
								$scope.searchDto.orderByType = "ASC";
								$scope.carrierSortIconClass = "icon-downarrow";
							}

						}
						$scope.searchDto.sortByColumn = column;

					}

					$scope.search = function() {
						$scope.chargeMaster = {};

						$scope.searchDto.selectedPageNumber = $scope.page;
						$scope.searchDto.recordPerPage = $scope.limit;
						console.log($scope.searchDto);
						$scope.chargeArr = [];
						$scope.searchDto.selectedPageNumber = $scope.page;
						$scope.searchDto.recordPerPage = $scope.limit;
						ChargeSearch.query($scope.searchDto).$promise
								.then(function(data, status) {
									$scope.totalRecord = data.responseObject.totalRecord;
									var tempArr = [];
									var resultArr = [];
									tempArr = data.responseObject.searchResult;
									var tempObj = {};
									angular.forEach(tempArr, function(item,
											index) {
										tempObj = item;
										tempObj.no = (index + 1)
												+ ($scope.page * $scope.limit);
										resultArr.push(tempObj);
										tempObj = {};
									});
									$scope.chargeArr = resultArr;
								});
						

					}

					$scope.serialNo = function(index) {
						index = ($scope.searchDto.selectedPageNumber * $scope.searchDto.recordPerPage)
								+ (index + 1);
						return index;
					}
	$scope.sublMaster = {};

					

});
