/**
 * 
 */

 (function() {


 	app.factory("DocumentMasterList",['$resource', function($resource) {

 		return $resource("/api/v1/documentmaster/get/search/keyword", {}, {
 			query : {
 				method : 'POST',
 			}
 		});
 	}]);


 })();
