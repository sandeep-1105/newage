app.service('triggerTypeMasterDataService', function($rootScope) {
    this.getHeadArray = function() {
	   return [
			  {
					"name":"#",
					"width":"col-xs-0half",
					"model":"no",
					"search":false
			  },{
					"name":"Name",
					"width":"col-md-1half",
					"model":"triggerTypeName",
					"search":true,
					"wrap_cell":true,
					"type":"text",
					"sort":true,
					"key":"searchTriggerTypeName"
				},{
					"name":"Code",
					"width":"col-md-1half",
					"model":"triggerTypeCode",
					"search":true,
					"type":"text",
					"sort":true,
					"key":"searchTriggerTypeCode"
				},{
					"name":"Status",
					"width":"col-md-2",
					"model":"status",
					"search":true,
					"type":"drop",
					"data":$rootScope.enum['LovStatus'],
					"key":"searchStatus",
					"sort":true
				}];
          };

    
    this.getListBreadCrumb = function() {
        return [{
                label: "Master",
                state: "layout.triggerTypeMaster"
            },
            {
                label: "Trigger Type",
                state: "layout.triggerTypeMaster"
            }
        ];
    }

    this.getAddBreadCrumb = function() {
        return [{
                label: "Master",
                state: "layout.triggerTypeMaster"
            },
            {
                label: "Trigger Type",
                state: "layout.triggerTypeMaster"
            },
            {
                label: "Add Trigger Type",
                state: null
            }
        ];
    };
    this.getEditBreadCrumb = function() {
        return [{
                label: "Master",
                state: "layout.triggerTypeMaster"
            },
            {
                label: "Trigger Type",
                state: "layout.triggerTypeMaster"
            },
            {
                label: "Edit Trigger Type",
                state: null
            }
        ];
    };
   

});