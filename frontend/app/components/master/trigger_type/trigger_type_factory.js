(function() {

	app.factory("TriggerTypeSearch",['$resource', function($resource) {
		return $resource("/api/v1/triggertypemaster/get/search", {}, {
			query : {
				method : 'POST'
			}
		});
	}]);
	
	app.factory("TriggerTypeSearchKeyword",['$resource', function($resource) {
		return $resource("/api/v1/triggertypemaster/get/search/keyword", {}, {
			fetch : {
				method : 'POST'
			}
		});
	}]);


})();
