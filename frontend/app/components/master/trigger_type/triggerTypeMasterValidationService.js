app.service('triggerTypeMasterValidationService', ['$rootScope','ValidateUtil', 'CommonValidationService', 'appConstant', function($rootScope, ValidateUtil, CommonValidationService, appConstant) {
	this.validate = function(triggerType, code) {
		//Validate triggerType name
		if(code == 0 || code == 1) {
			if(triggerType.triggerTypeName == undefined || triggerType.triggerTypeName == null || triggerType.triggerTypeName =="" ){
				return this.validationResponse(true, "triggerTypeName", $rootScope.nls["ERR06500"], triggerType);
			}
		}
		//Validate triggerType code
		if(code == 0 || code == 2) {
			if(triggerType.triggerTypeCode == undefined || triggerType.triggerTypeCode == null || triggerType.triggerTypeCode =="" ){
				return this.validationResponse(true, "triggerTypeCode", $rootScope.nls["ERR06501"], triggerType);
			}
		}
		
		
		
		return this.validationSuccesResponse(triggerType);
	}
 
	this.validationSuccesResponse = function(triggerType) {
	    return {error : false, obj : triggerType};
	}
	
	this.validationResponse = function(err, elem, message, triggerType) {
		return {error : err, errElement : elem, errMessage : message, obj : triggerType};
	}
	
}]);