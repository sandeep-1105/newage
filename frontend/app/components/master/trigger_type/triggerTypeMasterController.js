(function(){
app.controller("TriggerTypeMasterCtrl", ['$scope', '$rootScope', '$window', '$modal', '$state', '$stateParams', '$log', 'ngDialog', 'triggerTypeMasterDataService', 'triggerTypeMasterFactory', 'RecentHistorySaveService', 'triggerTypeMasterValidationService', 'Notification','roleConstant',
    function($scope, $rootScope, $window, $modal, $state, $stateParams, $log, ngDialog, triggerTypeMasterDataService, triggerTypeMasterFactory, RecentHistorySaveService, triggerTypeMasterValidationService, Notification,roleConstant ) {

        var vm = this;
        vm.tableHeadArr = triggerTypeMasterDataService.getHeadArray();
        vm.dataArr = [];
        vm.triggerTypeMaster = {};
        vm.showHistory = false;
        vm.deskTopView=true;
        vm.errorMap = new Map();
      
        /**
         * TriggerType Master List Page Search Data  initializing and calling search Mtd.
         */
        vm.searchInit = function() {
            vm.searchData = {};
            vm.limitArr = [10, 15, 20];
            vm.page = 0;
            vm.limit = 10;
            vm.totalRecord = 10;
            vm.searchData.orderByType = vm.sortSelection.sortOrder.toUpperCase();
            vm.searchData.sortByColumn = vm.sortSelection.sortKey;
            vm.search();
        };
        
        vm.sortSelection = {
            sortKey: "triggerTypeName",
            sortOrder: "asc"
        }
        
        vm.changeSearch = function(param) {
            $log.debug("Change Search is called." + param);
            for (var attrname in param) {
                vm.searchData[attrname] = param[attrname];
            }
            vm.page = 0;
            vm.search();
        }

        vm.sortChange = function(param) {
            $log.debug("sortChange is called. " + param);
            vm.searchData.orderByType = param.sortOrder.toUpperCase();
            vm.searchData.sortByColumn = param.sortKey;
            vm.search();
        }
        
        vm.setStatus = function(data){
    		if(data=='Active'){
    			return 'activetype';
    		} else if(data=='Block'){
    			return 'blockedtype';
    		}else if(data=='Hide'){
    			return 'hiddentype';
    		}
    	}
        
        /**
         * TriggerType Master List Navigate to selected page.
         * @param {Object} param - The Selected page option.
         */
        vm.changepage = function(param) {
            $log.debug("change Page..." + param.page);
            vm.page = param.page;
            vm.limit = param.size;
            vm.search();
        }

        /**
         * TriggerType Master List Page Limit Data change.
         * @param {int} nLimit - The Selected limit option from Limit array.
         */
        vm.limitChange = function(nLimit) {
            $log.debug("Limit Change..." + nLimit);
            vm.page = 0;
            vm.limit = nLimit;
            vm.search();
        }

        /**
         * TriggerType Master List Page Search Data  initializing and calling search Mtd.
         * @param {Object} data - The Selected TriggerType Master from List page.
         * @param {string} data.id - The id of the Selected TriggerType Master.
         * @param {int} index - The Selected row index from List page.
         */
        vm.rowSelect = function(data) {
        	if($rootScope.roleAccess(roleConstant.MASTER_CRM_TRIGGER_TYPE_VIEW)){
        		vm.triggerTypeMasteView=true;
    			vm.triggerTypeMaster = data;
    			var windowInner=$window.innerWidth;
    			if(windowInner<=1199){
    				vm.deskTopView = false;
    			}
    			else{
    				vm.deskTopView = true;
    			}
    			angular.element($window).bind('resize', function(){
    				if($window.innerWidth>=1200){
    					vm.deskTopView = true;
    				}
    				else if($window.innerWidth<=1199){
    					if(vm.triggerTypeMaster.id!=null){
    						vm.deskTopView = false;
    					}
    					else {
    						vm.deskTopView = true;
    					}
    				}
    				$scope.$digest();
    			});
        	}
		}

       
        /**
         * Go Back to TriggerType Master List Page.
         */
        vm.backToList = function() {
        	vm.triggerTypeMasteView = false;
        	vm.triggerTypeMaster = {};
        vm.deskTopView = true;
        }
        

       
        /**
         * Get TriggerType Master's Based on Search Data.
         */
        vm.search = function() {
        	vm.triggerTypeMasteView=false;
            vm.searchData.selectedPageNumber = vm.page;
            vm.searchData.recordPerPage = vm.limit;
            vm.dataArr = [];
            triggerTypeMasterFactory.search.fetch(vm.searchData).$promise.then(
                function(data) {
                    if (data.responseCode == "ERR0") {
                        vm.totalRecord = data.responseObject.totalRecord;
                        vm.dataArr = data.responseObject.searchResult;
                    }
                },
                function(errResponse) {
                	 $log.error('Error while fetching TriggerType');
                });
           }

        /**
         * Get TriggerType Master By id.
         * @param {int} id - The id of a TriggerType.
         * @param {boolean} isView - TO identify purpose of calling is for View or Edit a TriggerType.
         */
        vm.view = function(id, isView) {
        	triggerTypeMasterFactory.findById.query({
                id: id
            }).$promise.then(function(data) {
                if (data.responseCode == 'ERR0') {
                    vm.triggerTypeMaster = data.responseObject;
                    var rHistoryObj = {}

                    if (vm.triggerTypeMaster != undefined && vm.triggerTypeMaster != null && vm.triggerTypeMaster.id != undefined && vm.triggerTypeMaster.id != null) {
                        $rootScope.subTitle = vm.triggerTypeMaster.triggerTypeName;
                    } else {
                        $rootScope.subTitle = "Unknown TriggerType"
                    }
                    $rootScope.unfinishedFormTitle = "TriggerType  # " + vm.triggerTypeMaster.triggerTypeCode;
                    if (isView) {
                        rHistoryObj = {
                            'title': "TriggerType #" + vm.triggerTypeMaster.triggerTypeCode,
                            'subTitle': $rootScope.subTitle,
                            'stateName': $state.current.name,
                            'stateParam': JSON.stringify($stateParams),
                            'stateCategory': 'TriggerType Master',
                            'serviceType': undefined,
                            'showService': false
                        }

                    } else {

                        rHistoryObj = {
                            'title': 'Edit TriggerType # ' + $stateParams.serviceId,
                            'subTitle': $rootScope.subTitle,
                            'stateName': $state.current.name,
                            'stateParam': JSON.stringify($stateParams),
                            'stateCategory': 'TriggerType Master',
                            'serviceType': 'MASTER',
                            'serviceCode': ' ',
                            'orginAndDestination': ' '
                        }
                    }
                    RecentHistorySaveService.form(rHistoryObj);
                } else {
                    $log.debug("Exception while getting TriggerType by Id ", data.responseDescription);
                }
            }, function(error) {
                $log.debug("Error ", error);
            });
        };

        /**
         * Go To Add TriggerType Master.
         */
        vm.addNew = function() {
        	if($rootScope.roleAccess(roleConstant.MASTER_CRM_TRIGGER_TYPE_CREATE)){
        		$state.go("layout.triggerTypeMasterAdd");
        	}
        }

        /**
         * Validating field of TriggerType Master while instant change.
         * @param {errorElement,errorCode}
         */
        vm.validate = function(valElement, code) {
            var validationResponse = triggerTypeMasterValidationService.validate(vm.triggerTypeMaster,code);
            if(validationResponse.error == true) {
				$scope.validationErrorAndFocus(validationResponse, validationResponse.errElement);
			} else {
				$scope.errorMap.put(valElement, null);
            	validationResponse.error = false;
            }
        }
        
        /**
         * Cancel Add or Edit TriggerType Master.
         * @param {int} id - The id of a TriggerType
         */
       
        vm.cancel = function(objId) {
            if(vm.triggerTypeMasterForm!=undefined){
		    	vm.goTocancel(vm.triggerTypeMasterForm.$dirty,objId);
            }else{
            	vm.goTocancel(false,objId);
            }
        }
       
       vm.goTocancel = function(isDirty,objId){
     		 if (isDirty) {
     		      var newScope = $scope.$new();
     		      newScope.errorMessage = $rootScope.nls["ERR200"];
     		      ngDialog.openConfirm({
     		        template: '<p>{{errorMessage}}</p>' +
     		          '<div class="ngdialog-footer">' +
     		          ' <button type="button" class="btn btn-default btn-property cancel-btn" ng-click="confirm(2)">No</button>' +
     		          '<button type="button" class="btn btn-default btn-property accent-btn " ng-click="confirm(1)">Yes</button>' +
     		          '<button type="button" class="btn btn-default btn-property pull-left grey-cancel-btn" ng-click="confirm(3)">Cancel</button>' +
     		          '</div>',
     		        plain: true,
     		        closeByDocument: false,
     		        scope: newScope,
     		        className: 'ngdialog-theme-default'
     		      }).then(
     		        function(value) {
     		          if (value == 1) {
     		            if(objId!=null && objId != undefined){
     		            	vm.update();
     		            }else{
     		            	vm.create();
     		            }
     		          } else if (value == 2) {
     		        	var params = {};
     					params.submitAction = 'Cancelled';
     		        	 $state.go("layout.triggerTypeMaster", params);
     		          } else {
     		            $log.debug("cancelled");
     		          }
     		        });
     		    } else {
     		    	var params = {};
     		        params.submitAction = 'Cancelled';
	        	    $state.go("layout.triggerTypeMaster", params);
	        	}
     	    }

        /**
         * Modify Existing TriggerType Master.
         * @param {int} id - The id of a Country.
         */
        vm.edit = function(id) {
        	if($rootScope.roleAccess(roleConstant.MASTER_CRM_TRIGGER_TYPE_MODIFY)){
        		$state.go("layout.triggerTypeMasterEdit", {
                    id: id
                });        		
        	}
        }

        /**
         * Delete Country Master.
         * @param {int} id - The id of a Country.
         */
        vm.delete = function(id) {
        	if($rootScope.roleAccess(roleConstant.MASTER_CRM_TRIGGER_TYPE_DELETE)){
        		var newScope = $scope.$new();
    			newScope.errorMessage = $rootScope.nls["ERR06504"];
    	    	ngDialog.openConfirm( {
    	                    template : '<p>{{errorMessage}}</p>'
    	                    + '<div class="ngdialog-footer">'
    	                    + '<button type="button" class="btn btn-default btn-property cancel-btn" ng-click="closeThisDialog(0)">Cancel'
    	                    + '<button type="button" class="btn btn-primary btn-property accent-btn" ng-click="confirm(1)">Yes'
    	                    + '</button></div>',
    	                    plain : true,
    	                    scope : newScope,
    	                    closeByDocument: false,
    	                    className : 'ngdialog-theme-default'
    	                }).then( function(value) {
    	                	triggerTypeMasterFactory.delete.query({
    	                        id: id
    	                    }).$promise.then(function(data) {
    	                        if (data.responseCode == 'ERR0') {
    	                        	vm.triggerTypeMasteView=false;
    	                        	Notification.success($rootScope.nls["ERR402"]);
    	                        	vm.deskTopView=true;
    	                            vm.searchInit();
    	                        }
    	                    }, function(error) {
    	                        $log.debug("Error ", error);
    	                    });
    	                }, function(value) {
    	                    $log.debug("delete Cancelled");
    	            });
        	}
        }

        /**
         * Create New TriggerType Master.
         */
        vm.create = function() {
            var validationResponse = triggerTypeMasterValidationService.validate(vm.triggerTypeMaster,0);
            if(validationResponse.error == true) {
            	$log.debug("Validation Response -- ", validationResponse);
            	$log.debug('Validation Failed to create TriggerType..')
				$scope.validationErrorAndFocus(validationResponse, validationResponse.errElement);
			}
            else {
            	triggerTypeMasterFactory.create.query(vm.triggerTypeMaster).$promise.then(function(data) {
		            if (data.responseCode == 'ERR0') {
		            	$log.debug("TriggerType Saved Successfully Response :: ", data);
		            	Notification.success($rootScope.nls["ERR400"]);
		            	var params = {};
     					params.submitAction = 'Saved';
		            	$state.go('layout.triggerTypeMaster',params);
		            } else {
		                $log.debug("Create TriggerType Failed :" + data.responseDescription)
		            }

		        }, function(error) {
		        	 $log.debug("Create TriggerType Failed : " + error)
		        });
            } 
        };

        
      /**
       * Common Validation Focus Functionality - starts here" 
       */
		$scope.validationErrorAndFocus = function(valResponse, elemId){
			$scope.errorMap = new Map();
			if(valResponse.error == true){
				$scope.errorMap.put(valResponse.errElement, valResponse.errMessage);
			}
				$scope.navigateToNextField(elemId);
		}
        
        //Update Existing Country Master
        vm.update = function() {
        	 var validationResponse = triggerTypeMasterValidationService.validate(vm.triggerTypeMaster,0);
             if(validationResponse.error == true) {
             	$log.debug("Validation Response -- ", validationResponse);
             	$log.debug('Validation Failed while Updating the TriggerType..')
 				$scope.validationErrorAndFocus(validationResponse, validationResponse.errElement);
 			}
             else {
             	triggerTypeMasterFactory.update.query(vm.triggerTypeMaster).$promise.then(function(data) {
 		            if (data.responseCode == 'ERR0') {
 		            	$log.debug("TriggerType Updated Successfully Response :: ", data);
 		            	Notification.success($rootScope.nls["ERR401"]);
 		            	var params = {};
     					params.submitAction = 'Saved';
 		            	$state.go('layout.triggerTypeMaster',params);
 		            } else {
 		                $log.debug("Updating TriggerType Failed :" + data.responseDescription)
 		            }
 		        }, function(error) {
 		        	 $log.debug("Updating TriggerType Failed : " + error)
 		        });
             } 
        };
        
       
        /**
         * Recent History - starts here
         */
        
        $scope.$on('addTriggerTypeMasterEvent', function(events, args){
        	$rootScope.unfinishedData = vm.triggerTypeMaster;
			$rootScope.category = "Trigger Type Master";
			$rootScope.unfinishedFormTitle = "Trigger Type (New)";
        	if(vm.triggerTypeMaster != undefined && vm.triggerTypeMaster != null && vm.triggerTypeMaster.triggerTypeName != undefined && vm.triggerTypeMaster.triggerTypeName != null) {
        		$rootScope.subTitle = vm.triggerTypeMaster.triggerTypeName;
        	} else {
        		$rootScope.subTitle = "Unknown Trigger Type "
        	}
        })
      
	    $scope.$on('editTriggerTypeMasterEvent', function(events, args){
	    	$rootScope.unfinishedData = vm.triggerTypeMaster;
			$rootScope.category = "Trigger Type Master";
			$rootScope.unfinishedFormTitle = "Trigger Type Edit (" + vm.triggerTypeMaster.triggerTypeCode+")";
			if(vm.triggerTypeMaster != undefined && vm.triggerTypeMaster != null && vm.triggerTypeMaster.triggerTypeName != undefined && vm.triggerTypeMaster.triggerTypeName != null) {
        		$rootScope.subTitle = vm.triggerTypeMaster.triggerTypeName;
        	} else {
        		$rootScope.subTitle = "Unknown Trigger Type "
        	}
	    })
	      
	    $scope.$on('addTriggerTypeMasterEventReload', function(e, confirmation){
	    	    confirmation.message = "";
	    	    localStorage.reloadFormData = JSON.stringify(vm.triggerTypeMaster) ;
	    	    localStorage.isTriggerTypeMasterReloaded = "YES";
	            e.preventDefault();
	    })
	    
	    $scope.$on('editTriggerTypeMasterEventReload', function(e, confirmation){
	    	    confirmation.message = "";
	    	    localStorage.reloadFormData = JSON.stringify(vm.triggerTypeMaster) ;
	    	    localStorage.isTriggerTypeMasterReloaded = "YES";
	            e.preventDefault();
	    	  
	    })
	      


        /**
         * State Identification Handling Here.
         * @param {String} $stateParams.action - VIEW, ADD, EDIT, SEARCH.
         */
        //
	    vm.isReloaded = localStorage.isTriggerTypeMasterReloaded;
        switch ($stateParams.action) {
            case "ADD":
            	 if ($stateParams.fromHistory === 'Yes') {
                     if (vm.isReloaded == "YES") {
                      vm.isReloaded = "NO";
                      localStorage.isTriggerTypeMasterReloaded = "NO";
                       vm.triggerTypeMaster = JSON.parse(localStorage.reloadFormData);
                     } else {
                       vm.triggerTypeMaster = $rootScope.selectedUnfilledFormData;
                     }
                   } else {
                     if (vm.isReloaded == "YES") {
                       vm.isReloaded = "NO";
                       localStorage.isTriggerTypeMasterReloaded = "NO";
                       vm.triggerTypeMaster = JSON.parse(localStorage.reloadFormData);
                     } else {
                         $log.debug("Add NotHistory NotReload...")
                     }
                   }
                $rootScope.breadcrumbArr = triggerTypeMasterDataService.getAddBreadCrumb();
                break;
            case "EDIT":
            	 if (vm.fromHistory === 'Yes') {
                     if (vm.isReloaded == "YES") {
                       vm.isReloaded = "NO";
                       localStorage.isTriggerTypeMasterReloaded = "NO";
                       vm.triggerTypeMaster = JSON.parse(localStorage.reloadFormData);
                     } else {
                       vm.triggerTypeMaster = $rootScope.selectedUnfilledFormData;
                     }
                   } else {
                     if (vm.isReloaded == "YES") {
                       vm.isReloaded = "NO";
                       localStorage.isTriggerTypeMasterReloaded = "NO";
                       vm.triggerTypeMaster = JSON.parse(localStorage.reloadFormData);
                     } else {
                    	 vm.view($stateParams.id, false);
                     }
                   }
                $rootScope.breadcrumbArr = triggerTypeMasterDataService.getEditBreadCrumb();
                break;
            default:
                $rootScope.breadcrumbArr = triggerTypeMasterDataService.getListBreadCrumb();
                vm.searchInit();
        }

    }
]);
}) ();