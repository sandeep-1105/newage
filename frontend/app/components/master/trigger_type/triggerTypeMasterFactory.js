app.factory('triggerTypeMasterFactory', function($resource) {
    return {
        findById: $resource('/api/v1/triggertypemaster/get/id/:id', {}, {
            query: { method: 'GET', params: { id: '' }, isArray: false }
        }),
        create: $resource('/api/v1/triggertypemaster/create', {}, {
            query: { method: 'POST' }
        }),
        update: $resource('/api/v1/triggertypemaster/update', {}, {
            query: { method: 'POST' }
        }),
        delete: $resource('/api/v1/triggertypemaster/delete/:id', {}, {
            query: { method: 'DELETE', params: { id: '' } }
        }),
        search: $resource('/api/v1/triggertypemaster/get/search', {}, {
            fetch: { method: 'POST' }
        })
    };
})