app.controller('serviceTypeOperationCtrl',['$rootScope', '$state', '$stateParams', 'serviceTypeView',
        '$scope', 'serviceTypeService', 'serviceTypeUpdate',
        'serviceTypeSave', 'ngProgressFactory', 'ngDialog', 'appConstant', 'CommonValidationService',
    function($rootScope, $state, $stateParams, serviceTypeView,
        $scope, serviceTypeService, serviceTypeUpdate,
        serviceTypeSave, ngProgressFactory, ngDialog, appConstant, CommonValidationService) {

        $scope.init = function(acFlag) {

            if ($scope.serviceTypeMaster == undefined ||
                $scope.serviceTypeMaster == null) {
                $scope.serviceTypeMaster = {};
                $scope.serviceTypeMaster.status = 'Active';
                $rootScope.setNavigate3("Add Service Type");
            } else {
                $rootScope.setNavigate3("Edit Service Type");
            }
            $scope.contained_progressbar = ngProgressFactory
                .createInstance();
            $scope.contained_progressbar.setParent(document
                .getElementById('serviceType-panel'));
            $scope.contained_progressbar.setAbsolute();

            if (acFlag != undefined && acFlag != null && acFlag === 0) {
                $scope.setOldDataVal();
            }
        };


        $scope.setOldDataVal = function() {
            $scope.oldData = JSON.stringify($scope.serviceTypeMaster);
            console.log("serviceTypeMaster Data Loaded");
        }

        $scope.cancel = function() {
            var tempObj = $scope.serviceTypeMasterForm;
            if ($scope.oldData != JSON.stringify($scope.serviceTypeMaster)) {
                if (Object.keys(tempObj).length) {

                    var newScope = $scope.$new();
                    newScope.errorMessage = $rootScope.nls["ERR200"];
                    ngDialog
                        .openConfirm({
                            template: '<p>{{errorMessage}}</p>' +
                                '<div class="ngdialog-footer">' +
                                ' <button type="button" class="btn btn-default btn-property cancel-btn" ng-click="confirm(2)">No</button>' +
                                '<button type="button" class="btn btn-default btn-property accent-btn " ng-click="confirm(1)">Yes</button>' +
                                '<button type="button" class="btn btn-default btn-property pull-left grey-cancel-btn" ng-click="confirm(3)">Cancel</button>' +
                                '</div>',
                            plain: true,
                            scope: newScope,
                            className: 'ngdialog-theme-default'
                        })
                        .then(
                            function(value) {
                                if (value == 1 &&
                                    $scope.serviceTypeMaster.id == null) {
                                    $scope.save();
                                } else if (value == 1 &&
                                    $scope.serviceTypeMaster.id != null) {
                                    $scope.update();
                                } else if (value == 2) {
                                    var params = {}
                                    params.submitAction = 'Cancelled';
                                    $state
                                        .go(
                                            "layout.serviceType",
                                            params);
                                } else {
                                    console
                                        .log("cancelled");
                                }
                            });
                } else {
                    var params = {}
                    params.submitAction = 'Cancelled';
                    $state.go("layout.serviceType", params);
                }
            } else {
                var params = {}
                params.submitAction = 'Cancelled';
                $state.go("layout.serviceType", params);
            }
        }

        $scope.save = function() {
            console.log("Save Method is called.");

            if ($scope.validateServiceTypeMaster(0)) {

                $scope.contained_progressbar.start();

                serviceTypeSave.save($scope.serviceTypeMaster).$promise
                    .then(
                        function(data) {
                            if (data.responseCode == 'ERR0') {
                                var params = {}
                                params.submitAction = 'Saved';
                                $rootScope.successDesc = $rootScope.nls["ERR400"];
                                $state
                                    .go(
                                        "layout.serviceType",
                                        params);
                            } else {
                                console
                                    .log("Service added Failed " +
                                        data.responseDescription)
                            }

                            $scope.contained_progressbar
                                .complete();

                            angular.element(".panel-body")
                                .animate({
                                    scrollTop: 0
                                }, "slow");

                        },
                        function(error) {
                            console
                                .log("Service added Failed : " +
                                    error)
                        });
            } else {
                console.log("Invalid Form Submission.");
            }
        }

        $scope.update = function() {

            console.log("Update Method is called.");

            if ($scope.validateServiceTypeMaster(0)) {

                $scope.contained_progressbar.start();

                serviceTypeUpdate.update($scope.serviceTypeMaster).$promise
                    .then(
                        function(data) {
                            if (data.responseCode == 'ERR0') {
                                console
                                    .log("Cost Center updated Successfully")
                                var params = {}
                                params.submitAction = 'Saved';
                                $rootScope.successDesc = $rootScope.nls["ERR401"];
                                $state
                                    .go(
                                        "layout.serviceType",
                                        params);
                            } else {
                                console
                                    .log(
                                        "Service Type updated Failed ",
                                        data.responseDescription)
                            }

                            $scope.contained_progressbar
                                .complete();
                            angular.element(".panel-body")
                                .animate({
                                    scrollTop: 0
                                }, "slow");
                        },
                        function(error) {
                            console
                                .log(
                                    "Service Type updated Failed : ",
                                    error)
                        });
            } else {
                console.log("Invalid Form Submission.");
            }
        }

        // Service Type Master Validation started

        $scope.validateServiceTypeMaster = function(validateCode) {

            $scope.errorMap = new Map();

            if (validateCode == 0 || validateCode == 1) {
                if ($scope.serviceTypeMaster.serviceTypeName == null ||
                    $scope.serviceTypeMaster.serviceTypeName == undefined ||
                    $scope.serviceTypeMaster.serviceTypeName == "") {
                    $scope.errorMap.put("serviceTypeName",
                        $rootScope.nls["ERR1430"]);
                    return false
                }
                if (!CommonValidationService.checkRegExp(appConstant.Reg_Exp_Master_SERVICE_TYPE_NAME, $scope.serviceTypeMaster.serviceTypeName)) {
                    $scope.errorMap.put("serviceTypeName", $rootScope.nls["ERR1432"]);
                    return false;
                }
            }
            if (validateCode == 0 || validateCode == 2) {
                if ($scope.serviceTypeMaster.serviceTypeCode == null ||
                    $scope.serviceTypeMaster.serviceTypeCode == undefined ||
                    $scope.serviceTypeMaster.serviceTypeCode == "") {
                    $scope.errorMap.put("serviceTypeCode",
                        $rootScope.nls["ERR1429"]);
                    return false
                }

                if (!CommonValidationService.checkRegExp(appConstant.Reg_Exp_Master_SERVICE_TYPE_CODE, $scope.serviceTypeMaster.serviceTypeCode)) {
                    $scope.errorMap.put("serviceTypeCode", $rootScope.nls["ERR1431"]);
                    return false;
                }

            }

            if (validateCode == 0 || validateCode == 3) {
                if ($scope.serviceTypeMaster.serviceType == null ||
                    $scope.serviceTypeMaster.serviceType == undefined ||
                    $scope.serviceTypeMaster.serviceType == "") {
                    $scope.errorMap.put("serviceType",
                        $rootScope.nls["ERR1435"]);
                    return false
                }

            }
            return true;
        }

        $scope.setStatus = function(data) {
            if (data == 'Active') {
                return 'activetype';
            } else if (data == 'Block') {
                return 'blockedtype';
            } else if (data == 'Hide') {
                return 'hiddentype';
            }
        }

        // History Start

        $scope.getServiceTypeById = function(serviceTypeId) {
            console.log("getServiceTypeById ", serviceTypeId);

            serviceTypeView
                .get({
                        id: serviceTypeId
                    },
                    function(data) {
                        if (data.responseCode == "ERR0") {
                            console
                                .log(
                                    "Successful while getting service type.",
                                    data)
                            $scope.serviceTypeMaster = data.responseObject;
                        }
                        $scope.setOldDataVal();
                    },
                    function(error) {
                        console
                            .log(
                                "Error while getting service type.",
                                error)
                    });

        }

        // On leave the Unfilled service type Form Broadcast then
        // event will happen in
        // StateChangeListener(BroadCaster)-app_state_route.js

        $scope
            .$on(
                'addServiceTypeEvent',
                function(events, args) {

                    $rootScope.unfinishedData = $scope.serviceTypeMaster;
                    $rootScope.category = "Master";
                    $rootScope.unfinishedFormTitle = "Service Type (New)";
                    if ($scope.serviceTypeMaster != undefined &&
                        $scope.serviceTypeMaster != null &&
                        $scope.serviceTypeMaster.serviceTypeName != undefined &&
                        $scope.serviceTypeMaster.serviceTypeName != null &&
                        $scope.serviceTypeMaster.serviceTypeName != "") {
                        $rootScope.subTitle = $scope.serviceTypeMaster.serviceTypeName;
                    } else {
                        $rootScope.subTitle = "Unknown ServiceType"
                    }
                })

        $scope
            .$on(
                'editServiceTypeEvent',
                function(events, args) {
                    $rootScope.unfinishedData = $scope.serviceTypeMaster;
                    $rootScope.category = "Master";
                    $rootScope.unfinishedFormTitle = "Service Type Edit";
                    if ($scope.serviceTypeMaster != undefined &&
                        $scope.serviceTypeMaster != null &&
                        $scope.serviceTypeMaster.serviceTypeName != undefined &&
                        $scope.serviceTypeMaster.serviceTypeName != null &&
                        $scope.serviceTypeMaster.serviceTypeName != "") {
                        $rootScope.subTitle = $scope.serviceTypeMaster.serviceTypeName;
                    } else {
                        $rootScope.subTitle = "Unknown ServiceType"
                    }
                })

        $scope.$on('addServiceTypeEventReload', function(e,
            confirmation) {
            console.log("addServiceTypeEventReload is called ",
                $scope.serviceTypeMaster);
            confirmation.message = "";
            localStorage.reloadFormData = JSON
                .stringify($scope.serviceTypeMaster);
            localStorage.isServiceTypeReloaded = "YES";
            e.preventDefault();
        });

        $scope.$on('editServiceTypeEventReload', function(e,
            confirmation) {
            confirmation.message = "";
            localStorage.reloadFormData = JSON
                .stringify($scope.serviceTypeMaster);
            localStorage.isServiceTypeReloaded = "YES";
            e.preventDefault();
        });

        $scope.isReloaded = localStorage.isServiceTypeReloaded;

        if ($stateParams.action == "ADD") {
            if ($stateParams.fromHistory === 'Yes') {
                if ($scope.isReloaded == "YES") {
                    console.log("Add History Reload...")
                    $scope.isReloaded = "NO";
                    localStorage.isServiceTypeReloaded = "NO";
                    $scope.serviceTypeMaster = JSON
                        .parse(localStorage.reloadFormData);
                    $scope.init();
                } else {
                    console.log("Add History NotReload...")
                    $scope.serviceTypeMaster = $rootScope.selectedUnfilledFormData;
                    $scope.init();
                }
                $scope.oldData = "";
            } else {
                if ($scope.isReloaded == "YES") {
                    $scope.isReloaded = "NO";
                    localStorage.isServiceTypeReloaded = "NO";
                    $scope.serviceTypeMaster = JSON
                        .parse(localStorage.reloadFormData);
                    console.log("Add NotHistory Reload...");
                    $scope.init();
                    $scope.oldData = "";
                } else {
                    console.log("Add NotHistory NotReload...")
                    $scope.init(0);
                }
            }

            $rootScope.breadcrumbArr = [{
                    label: "Master",
                    state: "layout.serviceType"
                },
                {
                    label: "Service Type",
                    state: "layout.serviceType"
                },
                {
                    label: "Add Service Type",
                    state: null
                }
            ];
        } else {
            if ($stateParams.fromHistory === 'Yes') {
                if ($scope.isReloaded == "YES") {
                    $scope.isReloaded = "NO";
                    localStorage.isServiceTypeReloaded = "NO";
                    $scope.serviceTypeMaster = JSON
                        .parse(localStorage.reloadFormData);
                    $scope.init();
                } else {
                    $scope.serviceTypeMaster = $rootScope.selectedUnfilledFormData;
                    $scope.init();
                }
                $scope.oldData = "";
            } else {
                if ($scope.isReloaded == "YES") {
                    $scope.isReloaded = "NO";
                    localStorage.isServiceTypeReloaded = "NO";
                    $scope.serviceTypeMaster = JSON
                        .parse(localStorage.reloadFormData);
                    $scope.init();
                    $scope.oldData = "";
                } else {
                    $scope
                        .getServiceTypeById($stateParams.serviceTypeId);
                    $scope.init();
                }
            }
            $rootScope.breadcrumbArr = [{
                    label: "Master",
                    state: "layout.serviceType"
                },
                {
                    label: "Service Type",
                    state: "layout.serviceType"
                },
                {
                    label: "Edit Service Type",
                    state: null
                }
            ];
        }

        // History End

    }]);