app.controller('serviceTypeCtrl',['$rootScope', '$stateParams', '$state', '$scope', '$window', '$modal', 'serviceTypeRemove', 
		'serviceTypeSearch', 'ngTableParams', 'ngDialog', 'roleConstant',
	function($rootScope, $stateParams, $state, $scope, $window, $modal, serviceTypeRemove, 
		serviceTypeSearch, ngTableParams, ngDialog, roleConstant) {

    /************************************************************
     * ux - by Muthu
     * reason - ipad compatibility fix
     *
     * *********************************************************/
    $scope.deskTopView = true;

    /********************* ux fix ends here *****************************/
    $scope.serviceTypeHeadArr = [{
            "name": "#",
            "width": "col-xs-0half",
            "model": "no",
            "search": false,
        },
        {
            "name": "Name",
            "width": "w200px",
            "model": "serviceTypeName",
            "search": true,
            "wrap_cell": true,
            "type": "text",
            "sort": true,
            "key": "searchServiceTypeName"
        },
        {
            "name": "Code",
            "width": "w90px",
            "model": "serviceTypeCode",
            "search": true,
            "type": "text",
            "wrap_cell": true,
            "sort": true,
            "key": "searchServiceTypeCode"
        },
        {
            "name": "Service Type",
            "width": "w150px",
            "model": "serviceType",
            "search": true,
            "type": "drop",
            "sort": true,
            "data": $rootScope.enum['ServiceType'],
            "key": "searchServiceType"
        }, {
            "name": "Transport Mode",
            "width": "w150px",
            "model": "transportMode",
            "search": true,
            "type": "drop",
            "sort": true,
            "data": $rootScope.enum['TransportMode'],
            "key": "searchServiceTransportMode"
        },
        {
            "name": "Status",
            "width": "w150px",
            "model": "status",
            "search": true,
            "type": "drop",
            "sort": true,
            "data": $rootScope.enum['LovStatus'],
            "key": "searchStatus"
        }
    ]




    $scope.sortSelection = {
        sortKey: "serviceTypeName",
        sortOrder: "asc"
    }



    $scope.init = function() {
        $rootScope.setNavigate1("Master");
        $rootScope.setNavigate2("Service Type");

        $scope.serviceTypeMaster = {};
        $scope.searchDto = {};

        $scope.limitArr = [10, 15, 20];
        $scope.page = 0;
        $scope.limit = 10;

        $scope.search();
    };


    $scope.limitChange = function(item) {
        console.log("Limit Change is called...", item);
        $scope.page = 0;
        $scope.limit = item;
        $scope.cancel();
        $scope.search();
    }

    $scope.rowSelect = function(data) {
        if ($rootScope.roleAccess(roleConstant.MASTER_GENERAL_SERVICE_TYPE_VIEW)) {
            $scope.showLogs = true;
            $scope.serviceTypeMaster = data;
            console.log("data", data);
            /************************************************************
             * ux - by Muthu
             * reason - ipad compatibility fix
             *
             * *********************************************************/

            var windowInner = $window.innerWidth;
            if (windowInner <= 1199) {
                $scope.deskTopView = false;

            } else {
                $scope.deskTopView = true;
            }
            angular.element($window).bind('resize', function() {
                if ($window.innerWidth >= 1200) {
                    $scope.deskTopView = true;
                } else if ($window.innerWidth <= 1199) {
                    if ($scope.serviceTypeMaster.id != null) {
                        $scope.deskTopView = false;
                    } else {
                        $scope.deskTopView = true;
                    }
                }

                console.log("window resizing..." + $window.innerWidth);
                $scope.$digest();
            });

            /********************* ux fix ends here *****************************/
        }

    }

    $scope.changeSearch = function(param) {
        console.log("Change Search", param)
        for (var attrname in param) {
            $scope.searchDto[attrname] = param[attrname];
        }
        $scope.page = 0;
        $scope.cancel();
        $scope.search();
        console.log("change search", $scope.searchDto);
    }

    $scope.sortChange = function(param) {
        console.log("Sort Change Called.", param);
        $scope.searchDto.orderByType = param.sortOrder.toUpperCase();
        $scope.searchDto.sortByColumn = param.sortKey;
        $scope.cancel();
        $scope.search();
    }

    $scope.changepage = function(param) {
        console.log("Change Page Called.", param);
        $scope.page = param.page;
        $scope.limit = param.size;
        $scope.search();
    }



    $scope.addNewServiceType = function() {
        if ($rootScope.roleAccess(roleConstant.MASTER_GENERAL_SERVICE_TYPE_CREATE)) {
            console.log("Add Button is Pressed.")
            $state.go("layout.addServiceType");
        }
    }


    $scope.cancel = function() {
        $scope.showLogs = false;
        $scope.serviceTypeMaster = {};
        /************************************************************
         * ux - by Muthu
         * reason - ipad compatibility fix
         *
         * *********************************************************/
        $scope.deskTopView = true;

        /********************* ux fix ends here *****************************/
    }

    $scope.search = function() {
        $scope.searchDto.selectedPageNumber = $scope.page;
        $scope.searchDto.recordPerPage = $scope.limit;
        $scope.serviceTypeArr = [];
        serviceTypeSearch.query($scope.searchDto).$promise.then(function(data, status) {
            $scope.totalRecord = data.responseObject.totalRecord;
            var tempArr = [];
            var resultArr = [];
            tempArr = data.responseObject.searchResult;
            console.log("tempArr", tempArr);
            var tempObj = {};
            angular.forEach(tempArr, function(item, index) {
                tempObj = item;
                tempObj.no = (index + 1) + ($scope.page * $scope.limit);
                resultArr.push(tempObj);
                tempObj = {};
            });
            $scope.serviceTypeArr = resultArr;
        });
    }


    $scope.editServiceType = function() {
        if ($rootScope.roleAccess(roleConstant.MASTER_GENERAL_SERVICE_TYPE_MODIFY)) {
            $state.go("layout.editServiceType", {
                serviceTypeId: $scope.serviceTypeMaster.id
            });
        }
    }
    $scope.deleteServiceType = function() {
        if ($rootScope.roleAccess(roleConstant.MASTER_GENERAL_SERVICE_TYPE_DELETE)) {
            var newScope = $scope.$new();
            newScope.errorMessage = $rootScope.nls["ERR243"];
            ngDialog.openConfirm({
                template: '<p>{{errorMessage}}</p>' +
                    '<div class="ngdialog-footer">' +
                    '<button type="button" class="btn btn-default btn-property cancel-btn" ng-click="closeThisDialog(0)">Cancel' +
                    '<button type="button" class="btn btn-primary btn-property accent-btn" ng-click="confirm(1)">Yes' +
                    '</button></div>',
                plain: true,
                scope: newScope,
                className: 'ngdialog-theme-default'
            }).then(function(value) {
                serviceTypeRemove.remove({
                    id: $scope.serviceTypeMaster.id
                }, function(data) {
                    if (data.responseCode == 'ERR0') {
                        console.log("Service Type deleted Successfully")
                        $scope.init();
                        $scope.deskTopView = true;
                    } else {
                        console.log("Service Type deleted Failed " + data.responseDescription)
                    }
                }, function(error) {
                    console.log("Service Type deleted Failed : " + error)
                });
            }, function(value) {
                console.log("Service Type deleted cancelled");
            });
        }
    }


    // Routing start


    switch ($stateParams.action) {
        case "SEARCH":
            $scope.init();
            $rootScope.breadcrumbArr = [{
                    label: "Master",
                    state: "layout.serviceType"
                },
                {
                    label: "Service Type",
                    state: "layout.serviceType"
                }
            ];
            break;
    }

    // Routing end


}]);