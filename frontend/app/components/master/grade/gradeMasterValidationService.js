app.service('gradeMasterValidationService', ['$rootScope','ValidateUtil', 'CommonValidationService', 'appConstant', function($rootScope, ValidateUtil, CommonValidationService, appConstant) {
	this.validate = function(grade, code) {
		
		if(code ==0 || code ==1){
			if(grade.gradeName == undefined || grade.gradeName == null || grade.gradeName == ""){
				return this.validationResponse(true, "gradeName", $rootScope.nls["ERR07000"], grade);
			}
		}
		
		if(code ==0 || code ==2){
			if(grade.priority == undefined || grade.priority == null || grade.priority == ""){
				return this.validationResponse(true, "priority", $rootScope.nls["ERR07001"], grade);
			}
		}
		
		
		return this.validationSuccesResponse(grade);
	}
 
	this.validationSuccesResponse = function(grade) {
	    return {error : false, obj : grade};
	}
	
	this.validationResponse = function(err, elem, message, grade) {
		return {error : err, errElement : elem, errMessage : message, obj : grade};
	}
	
}]);