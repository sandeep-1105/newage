app.service('gradeMasterDataService', function($rootScope) {
    this.getHeadArray = function() {
	   return [
			  {
					"name":"#",
					"width":"col-xs-0half",
					"model":"no",
					"search":false
			  },{
					"name":"Name",
					"width":"col-md-3",
					"model":"gradeName",
					"search":true,
					"wrap_cell":true,
					"type":"text",
					"sort":true,
					"key":"searchGradeName"
				},{
					"name":"Color Code",
					"width":"col-md-3",
					"model":"colorCode",
					"search":true,
					"type":"text",
					"key":"searchColorCode",
					"sort":true
				},{
					"name":"Priority",
					"width":"col-md-2",
					"model":"priority",
					"search":true,
					"type":"drop",
					"data":$rootScope.enum['Priority'],
					"key":"searchPriority",
					"sort":true
				}];
          };

    
    this.getListBreadCrumb = function() {
        return [{
                label: "Master",
                state: "layout.gradeMaster"
            },
            {
                label: "Grade",
                state: "layout.gradeMaster"
            }
        ];
    }

    this.getAddBreadCrumb = function() {
        return [{
                label: "Master",
                state: "layout.gradeMaster"
            },
            {
                label: "Grade",
                state: "layout.gradeMaster"
            },
            {
                label: "Add Grade",
                state: null
            }
        ];
    };
    this.getEditBreadCrumb = function() {
        return [{
                label: "Master",
                state: "layout.gradeMaster"
            },
            {
                label: "Grade",
                state: "layout.gradeMaster"
            },
            {
                label: "Edit Grade",
                state: null
            }
        ];
    };
   

});