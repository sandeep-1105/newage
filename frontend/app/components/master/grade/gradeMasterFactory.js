app.factory('gradeMasterFactory',['$resource', function($resource) {
	return {
		findById : $resource('/api/v1/grademaster/get/id/:id', {}, {
			 query : {method : 'GET', params : {id : ''}, isArray : false}
		}),
		create : $resource('/api/v1/grademaster/save', {}, {
			query : {method : 'POST'}
		}),
		delete : $resource('/api/v1/grademaster/delete/:id', {}, {
			query : {method : 'DELETE', params : {id : ''}}
		}),
		search : $resource('/api/v1/grademaster/get/search/keyword', {}, {
			query : {method : 'POST'}
		}),
		getall : $resource('/api/v1/grademaster/get/search', {}, {
			fetch : {method : 'POST'}
		})
	};
}])