(function(){
app.controller("GradeMasterCtrl", ['$scope', '$rootScope', '$window', '$modal', '$state', '$stateParams', '$log', 'ngDialog', 'gradeMasterDataService', 'gradeMasterFactory', 'RecentHistorySaveService', 'gradeMasterValidationService', 'Notification','roleConstant',
    function($scope, $rootScope, $window, $modal, $state, $stateParams, $log, ngDialog, gradeMasterDataService, gradeMasterFactory, RecentHistorySaveService, gradeMasterValidationService, Notification,roleConstant) {

        var vm = this;
        vm.tableHeadArr = gradeMasterDataService.getHeadArray();
        vm.dataArr = [];
        vm.gradeMaster = {};
        vm.showHistory = false;
        vm.errorMap = new Map();
        vm.priorityArr = $rootScope.enum['Priority'];
        vm.deskTopView = true;

        vm.processColor = function(color){
        	vm.gradeMaster.colorCode = document.getElementById(color).value;
        }
        vm.sortSelection = {
            sortKey: "gradeName",
            sortOrder: "asc"
        }
        
        vm.changeSearch = function(param) {
            $log.debug("Change Search is called." + param);
            for (var attrname in param) {
                vm.searchData[attrname] = param[attrname];
            }
            vm.page = 0;
            vm.search();
        }

        vm.sortChange = function(param) {
            $log.debug("sortChange is called. " + param);
            vm.searchData.orderByType = param.sortOrder.toUpperCase();
            vm.searchData.sortByColumn = param.sortKey;
            vm.search();
        }
      
        
        /**
         * Grade Master List Navigate to selected page.
         * @param {Object} param - The Selected page option.
         */
        vm.changepage = function(param) {
            $log.debug("change Page..." + param.page);
            vm.page = param.page;
            vm.limit = param.size;
            vm.search();
        }

        /**
         * Grade Master List Page Limit Data change.
         * @param {int} nLimit - The Selected limit option from Limit array.
         */
        vm.limitChange = function(nLimit) {
            $log.debug("Limit Change..." + nLimit);
            vm.page = 0;
            vm.limit = nLimit;
            vm.search();
        }

        /**
         * Grade Master List Page Search Data  initializing and calling search Mtd.
         * @param {Object} data - The Selected Grade Master from List page.
         * @param {string} data.id - The id of the Selected Grade Master.
         * @param {int} index - The Selected row index from List page.
         */
        vm.rowSelect = function(data) {
        	if($rootScope.roleAccess(roleConstant.MASTER_GENERAL_GRADE_VIEW)){
        		vm.gradeMasteView=true;
    			vm.gradeMaster = data;
    			var windowInner=$window.innerWidth;
    			if(windowInner<=1199){
    				vm.deskTopView = false;
    			}
    			else{
    				vm.deskTopView = true;
    			}
    			angular.element($window).bind('resize', function(){
    				if($window.innerWidth>=1200){
    					vm.deskTopView = true;
    				}
    				else if($window.innerWidth<=1199){
    					if(vm.gradeMaster.id!=null){
    						vm.deskTopView = false;
    					}
    					else {
    						vm.deskTopView = true;
    					}
    				}
    				$scope.$digest();
    			});
        	}
		}

        /**
         * Grade Master List Page Search Data  initializing and calling search Mtd.
         */
        vm.searchInit = function() {
            vm.searchData = {};
            vm.limitArr = [10, 15, 20];
            vm.page = 0;
            vm.limit = 10;
            vm.totalRecord = 10;
            vm.searchData.orderByType = vm.sortSelection.sortOrder.toUpperCase();
            vm.searchData.sortByColumn = vm.sortSelection.sortKey;
            vm.search();
        };
        /**
         * Go Back to Grade Master List Page.
         */
        vm.backToList = function() {
        	vm.gradeMasteView = false;
        	vm.gradeMaster = {};
        	vm.deskTopView = true;
        }

       
        /**
         * Get Grade Master's Based on Search Data.
         */
        vm.search = function() {
        	vm.gradeMasteView=false;
            vm.searchData.selectedPageNumber = vm.page;
            vm.searchData.recordPerPage = vm.limit;
            vm.dataArr = [];
            gradeMasterFactory.search.query(vm.searchData).$promise.then(
                function(data) {
                    if (data.responseCode == "ERR0") {
                        vm.totalRecord = data.responseObject.totalRecord;
                        vm.dataArr = data.responseObject.searchResult;
                    }
                },
                function(errResponse) {
                	 $log.error('Error while fetching Grade');
                });
           }

        /**
         * Get Grade Master By id.
         * @param {int} id - The id of a Grade.
         * @param {boolean} isView - TO identify purpose of calling is for View or Edit a Grade.
         */
        vm.view = function(id, isView) {
        	gradeMasterFactory.findById.query({
                id: id
            }).$promise.then(function(data) {
                if (data.responseCode == 'ERR0') {
                    vm.gradeMaster = data.responseObject;
                    var rHistoryObj = {}

                    if (vm.gradeMaster != undefined && vm.gradeMaster != null && vm.gradeMaster.id != undefined && vm.gradeMaster.id != null) {
                        $rootScope.subTitle = vm.gradeMaster.gradeName;
                    } else {
                        $rootScope.subTitle = "Unknown Grade"
                    }
                    $rootScope.unfinishedFormTitle = "Grade # " + vm.gradeMaster.gradeCode;
                    if (isView) {
                        rHistoryObj = {
                            'title': "Grade #" + vm.gradeMaster.gradeCode,
                            'subTitle': $rootScope.subTitle,
                            'stateName': $state.current.name,
                            'stateParam': JSON.stringify($stateParams),
                            'stateCategory': 'Grade Master',
                            'serviceType': undefined,
                            'showService': false
                        }

                    } else {

                        rHistoryObj = {
                            'title': 'Edit Grade # ' + $stateParams.serviceId,
                            'subTitle': $rootScope.subTitle,
                            'stateName': $state.current.name,
                            'stateParam': JSON.stringify($stateParams),
                            'stateCategory': 'Grade Master',
                            'serviceType': 'MASTER',
                            'serviceCode': ' ',
                            'orginAndDestination': ' '
                        }
                    }
                    RecentHistorySaveService.form(rHistoryObj);
                } else {
                    $log.debug("Exception while getting Grade by Id ", data.responseDescription);
                }
            }, function(error) {
                $log.debug("Error ", error);
            });
        };

        /**
         * Go To Add Grade Master.
         */
        vm.addNew = function() {
        	if($rootScope.roleAccess(roleConstant.MASTER_GENERAL_GRADE_CREATE)){
            $state.go("layout.gradeMasterAdd");
        	}
        }

        
        /**
         * Cancel Add or Edit Grade Master.
         * @param {int} id - The id of a Grade
         */
       
        vm.cancel = function() {
            if(vm.gradeMasterForm!=undefined){
		    	vm.goTocancel(vm.gradeMasterForm.$dirty);
            }else{
            	vm.goTocancel(false);
            }
        }
       
       vm.goTocancel = function(isDirty){
     		 if (isDirty) {
     		      var newScope = $scope.$new();
     		      newScope.errorMessage = $rootScope.nls["ERR200"];
     		      ngDialog.openConfirm({
     		        template: '<p>{{errorMessage}}</p>' +
     		          '<div class="ngdialog-footer">' +
     		          ' <button type="button" class="btn btn-default btn-property cancel-btn" ng-click="confirm(2)">No</button>' +
     		          '<button type="button" class="btn btn-default btn-property accent-btn " ng-click="confirm(1)">Yes</button>' +
     		          '<button type="button" class="btn btn-default btn-property pull-left grey-cancel-btn" ng-click="confirm(3)">Cancel</button>' +
     		          '</div>',
     		        plain: true,
     		        closeByDocument: false,
     		        scope: newScope,
     		        className: 'ngdialog-theme-default'
     		      }).then(
     		        function(value) {
     		          if (value == 1) {
     		           vm.saveOrUpdate();
     		          } else if (value == 2) {
     		        	 $state.go("layout.gradeMaster", {submitAction: "Cancelled"});
     		          } else {
     		            $log.debug("cancelled");
     		          }
     		        });
     		    } else {
     		     $state.go("layout.gradeMaster", {
                   submitAction: "Cancelled"
               });
     		    }
     	    }

        /**
         * Modify Existing Grade Master.
         * @param {int} id - The id of a Country.
         */
        vm.edit = function(id) {
        	if($rootScope.roleAccess(roleConstant.MASTER_GENERAL_GRADE_MODIFY)){
            $state.go("layout.gradeMasterEdit", {
                id: id
            });
        	}
        }

        /**
         * Delete Country Master.
         * @param {int} id - The id of a Country.
         */
        vm.delete = function(id) {
        	if($rootScope.roleAccess(roleConstant.MASTER_GENERAL_GRADE_DELETE)){
        	var newScope = $scope.$new();
			newScope.errorMessage = $rootScope.nls["ERR07006"];
	    	ngDialog.openConfirm( {
	                    template : '<p>{{errorMessage}}</p>'
	                    + '<div class="ngdialog-footer">'
	                    + '<button type="button" class="btn btn-default btn-property cancel-btn" ng-click="closeThisDialog(0)">Cancel'
	                    + '<button type="button" class="btn btn-primary btn-property accent-btn" ng-click="confirm(1)">Yes'
	                    + '</button></div>',
	                    plain : true,
	                    scope : newScope,
	                    closeByDocument: false,
	                    className : 'ngdialog-theme-default'
	                }).then( function(value) {
	                	gradeMasterFactory.delete.query({
	                        id: id
	                    }).$promise.then(function(data) {
	                        if (data.responseCode == 'ERR0') {
	                        	vm.gradeMasteView=false;
	                        	Notification.success($rootScope.nls["ERR402"]);
	                        	vm.deskTopView = true;
	                            vm.search();
	                        }
	                    }, function(error) {
	                        $log.debug("Error ", error);
	                    });
	                }, function(value) {
	                    $log.debug("delete Cancelled");
	            });
        	}
        }

        /**
         * Create New Grade Master.
         */
        vm.saveOrUpdate = function() {
            var validationResponse = gradeMasterValidationService.validate(vm.gradeMaster,0);
            if(validationResponse.error == true) {
            	$log.debug("Validation Response -- ", validationResponse);
            	$log.debug('Validation Failed to create Grade..')
				$scope.validationErrorAndFocus(validationResponse, validationResponse.errElement);
			}
            else {
            	gradeMasterFactory.create.query(vm.gradeMaster).$promise.then(function(data) {
		            if (data.responseCode == 'ERR0') {
		            	$log.debug("Grade Saved Successfully Response :: ", data);
		            	Notification.success($rootScope.nls["ERR400"]);
		            	$state.go('layout.gradeMaster',{submitAction: "Saved"});
		            } else {
		                $log.debug("Create Grade Failed :" + data.responseDescription)
		            }

		        }, function(error) {
		        	 $log.debug("Create Grade Failed : " + error)
		        });
            } 
        };

        /**
         * Validating field of Grade Master while instant change.
         * @param {errorElement,errorCode}
         */
        vm.validate = function(valElement,code) {
            var validationResponse = defaultMasterValidationService.validate(vm.defaultMasterData,code);
            if(validationResponse.error == true) {
				$scope.validationErrorAndFocus(validationResponse, validationResponse.errElement);
			} else {
				vm.errorMap.put(valElement, null);
            	validationResponse.error = false;
            }
        }
        
        /**
         * Common Validation Focus Functionality - starts here" 
         */
  		$scope.validationErrorAndFocus = function(valResponse, elemId){
  			vm.errorMap = new Map();
  			if(valResponse.error == true){
  				vm.errorMap.put(valResponse.errElement, valResponse.errMessage);
  			}
  				$scope.navigateToNextField(elemId);
  		}
        
       
        /**
         * Recent History - starts here
         */
        vm.isReloaded = localStorage.isGradeMasterReloaded;
        $scope.$on('addGradeMasterEvent', function(events, args){
        	$rootScope.unfinishedData = vm.gradeMaster;
			$rootScope.category = "Grade";
			$rootScope.unfinishedFormTitle = "Grade (New)";
        	if(vm.gradeMaster != undefined && vm.gradeMaster != null && vm.gradeMaster.gradeName != undefined && vm.gradeMaster.gradeName != null) {
        		$rootScope.subTitle = vm.gradeMaster.gradeName;
        	} else {
        		$rootScope.subTitle = "Unknown Grade "
        	}
        })
      
	    $scope.$on('editGradeMasterEvent', function(events, args){
	    	$rootScope.unfinishedData = vm.gradeMaster;
			$rootScope.category = "Grade";
			$rootScope.unfinishedFormTitle = "Grade Edit # "+vm.gradeMaster.gradeName;
	    })
	      
	    $scope.$on('addGradeMasterEventReload', function(e, confirmation){
	    	    confirmation.message = "";
	    	    localStorage.reloadFormData = JSON.stringify(vm.gradeMaster) ;
	    	    localStorage.isGradeMasterReloaded = "YES";
	            e.preventDefault();
	    })
	    
	    $scope.$on('editGradeMasterEventReload', function(e, confirmation){
	    	    confirmation.message = "";
	    	    localStorage.reloadFormData = JSON.stringify(vm.gradeMaster) ;
	    	    localStorage.isGradeMasterReloaded = "YES";
	            e.preventDefault();
	    	  
	    })
	      


        /**
         * State Identification Handling Here.
         * @param {String} $stateParams.action - VIEW, ADD, EDIT, SEARCH.
         */
        //
        switch ($stateParams.action) {
            case "ADD":
            	 if ($stateParams.fromHistory === 'Yes') {
                     if (vm.isReloaded == "YES") {
                      vm.isReloaded = "NO";
                      localStorage.isGradeMasterReloaded = "NO";
                       vm.gradeMaster = JSON.parse(localStorage.reloadFormData);
                     } else {
                       vm.gradeMaster = $rootScope.selectedUnfilledFormData;
                     }
                   } else {
                     if (vm.isReloaded == "YES") {
                       vm.isReloaded = "NO";
                       localStorage.isGradeMasterReloaded = "NO";
                       vm.gradeMaster = JSON.parse(localStorage.reloadFormData);
                     } else {
                         $log.debug("Add NotHistory NotReload...")
                     }
                   }
                $rootScope.breadcrumbArr = gradeMasterDataService.getAddBreadCrumb();
                break;
            case "EDIT":
            	 if (vm.fromHistory === 'Yes') {
                     if (vm.isReloaded == "YES") {
                       vm.isReloaded = "NO";
                       localStorage.isGradeMasterReloaded = "NO";
                       vm.gradeMaster = JSON.parse(localStorage.reloadFormData);
                     } else {
                       vm.gradeMaster = $rootScope.selectedUnfilledFormData;
                     }
                   } else {
                     if (vm.isReloaded == "YES") {
                       vm.isReloaded = "NO";
                       localStorage.isGradeMasterReloaded = "NO";
                       vm.gradeMaster = JSON.parse(localStorage.reloadFormData);
                     } else {
                    	 vm.view($stateParams.id, false);
                     }
                   }
                $rootScope.breadcrumbArr = gradeMasterDataService.getEditBreadCrumb();
                break;
            default:
                $rootScope.breadcrumbArr = gradeMasterDataService.getListBreadCrumb();
                vm.searchInit();
        }

    }
]);
}) ();