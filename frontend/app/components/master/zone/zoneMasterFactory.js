app.factory('zoneMasterFactory', function($resource) {
	return {
		findById : $resource('/api/v1/zonemaster/get/id/:id', {}, {
			 query : {method : 'GET', params : {id : ''}, isArray : false}
		}),
		create : $resource('/api/v1/zonemaster/create', {}, {
			query : {method : 'POST'}
		}),
		update : $resource('/api/v1/zonemaster/update', {}, {
			query : {method : 'POST'}
		}),
		delete : $resource('/api/v1/zonemaster/delete/:id', {}, {
			query : {method : 'DELETE', params : {id : ''}}
		}),
		search : $resource('/api/v1/zonemaster/get/search/keyword', {}, {
			query : {method : 'POST'}
		})
	};
})