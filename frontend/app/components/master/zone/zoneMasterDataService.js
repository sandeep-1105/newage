app.service('zoneMasterDataService', function($rootScope) {
    this.getHeadArray = function() {
	   return [
			  {
					"name":"#",
					"width":"w25px",
					"model":"no",
					"search":false
			  },{
					"name":"Name",
					"width":"w200px",
					"model":"zoneName",
					"search":true,
					"wrap_cell":true,
					"type":"text",
					"sort":true,
					"key":"searchZoneName"
				},{
					"name":"Code",
					"width":"w100px",
					"model":"zoneCode",
					"search":true,
					"type":"text",
					"sort":true,
					"key":"searchZoneCode"
				}/*,{
					"name":"Country Code",
					"width":"col-md-1half",
					"model":"countryMaster.countryCode",
					"search":true,
					"type":"text",
					"sort":true,
					"key":"searchCountryCode"
				}*/,{
					"name":"Country",
					"width":"w175px",
					"model":"countryMaster.countryName",
					"search":true,
					"type":"text",
					"sort":true,
					"key":"searchCountryName"
				},{
					"name":"Status",
					"width":"w150px",
					"model":"status",
					"search":true,
					"type":"drop",
					"data":$rootScope.enum['LovStatus'],
					"key":"searchStatus",
					"sort":true
				}];
          };

    
    this.getListBreadCrumb = function() {
        return [{
                label: "Master",
                state: "layout.zoneMaster"
            },
            {
                label: "Zone",
                state: "layout.zoneMaster"
            }
        ];
    }

    this.getAddBreadCrumb = function() {
        return [{
                label: "Master",
                state: "layout.zoneMaster"
            },
            {
                label: "Zone",
                state: "layout.zoneMaster"
            },
            {
                label: "Add Zone",
                state: null
            }
        ];
    };
    this.getEditBreadCrumb = function() {
        return [{
                label: "Master",
                state: "layout.zoneMaster"
            },
            {
                label: "Zone",
                state: "layout.zoneMaster"
            },
            {
                label: "Edit Zone",
                state: null
            }
        ];
    };
   

});