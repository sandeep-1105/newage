app.service('zoneMasterValidationService', ['$rootScope','ValidateUtil', 'CommonValidationService', 'appConstant', function($rootScope, ValidateUtil, CommonValidationService, appConstant) {
	this.validate = function(zone, code) {
		//Validate zone name
		if(code == 0 || code == 1) {
			if(zone.zoneName == undefined || zone.zoneName == null || zone.zoneName =="" ){
				return this.validationResponse(true, "zoneName", $rootScope.nls["ERR06400"], zone);
			}
		}
		//Validate zone code
		if(code == 0 || code == 2) {
			if(zone.zoneCode == undefined || zone.zoneCode == null || zone.zoneCode =="" ){
				return this.validationResponse(true, "zoneCode", $rootScope.nls["ERR06401"], zone);
			}
		}
		
		//Validate country name
		if(code == 0 || code == 3) {
			if(zone.countryMaster == null || zone.countryMaster == undefined || zone.countryMaster== "" ){
				return this.validationResponse(true, "countryName", $rootScope.nls["ERR06300"], zone);	
			}
			else{
				if(zone.countryMaster.status=="Block"){
					return this.validationResponse(true, "countryName", $rootScope.nls["ERR06405"], zone);	
				}
				else if(zone.countryMaster.status=="Hide"){
					return this.validationResponse(true, "countryName", $rootScope.nls["ERR06406"], zone);	
				}
			}
		}
		
		return this.validationSuccesResponse(zone);
	}
 
	this.validationSuccesResponse = function(zone) {
	    return {error : false, obj : zone};
	}
	
	this.validationResponse = function(err, elem, message, zone) {
		return {error : err, errElement : elem, errMessage : message, obj : zone};
	}
	
}]);