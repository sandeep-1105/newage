(function() {
	app.factory("ZoneList",['$resource', function($resource) {
		return $resource("/api/v1/zonemaster/get/search/keyword", {}, {
			fetch : {
				method : 'POST'
			}
		});
	}]);

})();
