(function(){
app.controller("ZoneMasterCtrl", ['$scope', '$rootScope', '$window', '$modal', '$state', '$stateParams', '$log', 'ngDialog', 'zoneMasterDataService', 'zoneMasterFactory', 'RecentHistorySaveService', 'zoneMasterValidationService', 'Notification', 'CountryList', 'roleConstant',
    function($scope, $rootScope, $window, $modal, $state, $stateParams, $log, ngDialog, zoneMasterDataService, zoneMasterFactory, RecentHistorySaveService, zoneMasterValidationService, Notification, CountryList,roleConstant) {

        var vm = this;
        vm.tableHeadArr = zoneMasterDataService.getHeadArray();
        vm.dataArr = [];
        vm.searchData = {};
        vm.zoneMaster = {};
        vm.limitArr = [10, 15, 20];
        vm.page = 0;
        vm.limit = 10;
        vm.totalRecord = 100;
        vm.showHistory = false;
        vm.errorMap = new Map();
        vm.deskTopView = true;

        vm.sortSelection = {
            sortKey: "zoneName",
            sortOrder: "asc"
        }
        
        vm.changeSearch = function(param) {
            $log.debug("Change Search is called." + param);
            for (var attrname in param) {
                vm.searchData[attrname] = param[attrname];
            }
            vm.page = 0;
            vm.search();
        }

        vm.sortChange = function(param) {
            $log.debug("sortChange is called. " + param);
            vm.searchData.orderByType = param.sortOrder.toUpperCase();
            vm.searchData.sortByColumn = param.sortKey;
            vm.search();
        }
        
        vm.setStatus = function(data){
    		if(data=='Active'){
    			return 'activetype';
    		} else if(data=='Block'){
    			return 'blockedtype';
    		}else if(data=='Hide'){
    			return 'hiddentype';
    		}
    	}
        
        /**
         * Zone Master List Navigate to selected page.
         * @param {Object} param - The Selected page option.
         */
        vm.changepage = function(param) {
            $log.debug("change Page..." + param.page);
            vm.page = param.page;
            vm.limit = param.size;
            vm.search();
        }

        /**
         * Zone Master List Page Limit Data change.
         * @param {int} nLimit - The Selected limit option from Limit array.
         */
        vm.limitChange = function(nLimit) {
            $log.debug("Limit Change..." + nLimit);
            vm.page = 0;
            vm.limit = nLimit;
            vm.search();
        }

        /**
         * Zone Master List Page Search Data  initializing and calling search Mtd.
         * @param {Object} data - The Selected Zone Master from List page.
         * @param {string} data.id - The id of the Selected Zone Master.
         * @param {int} index - The Selected row index from List page.
         */
        vm.rowSelect = function(data) {
        	if($rootScope.roleAccess(roleConstant.MASTER_GENERAL_ZONE_VIEW)){
        	vm.zoneMasteView=true;
			vm.zoneMaster = data;
			var windowInner=$window.innerWidth;
			if(windowInner<=1199){
				vm.deskTopView = false;
			}
			else{
				vm.deskTopView = true;
			}
			angular.element($window).bind('resize', function(){
				if($window.innerWidth>=1200){
					vm.deskTopView = true;
				}
				else if($window.innerWidth<=1199){
					if(vm.zoneMaster.id!=null){
						vm.deskTopView = false;
					}
					else {
						vm.deskTopView = true;
					}
				}
				$scope.$digest();
			});
        }
		}

        /**
         * Zone Master List Page Search Data  initializing and calling search Mtd.
         */
        vm.searchInit = function() {
            vm.searchData = {};
            vm.searchData.orderByType = vm.sortSelection.sortOrder.toUpperCase();
            vm.searchData.sortByColumn = vm.sortSelection.sortKey;
            vm.search();
        };
        /**
         * Go Back to Zone Master List Page.
         */
        vm.backToList = function() {
        	vm.zoneMasteView = false;
        	vm.zoneMaster = {};
        	vm.deskTopView = true;
        }

       
        /**
         * Get Zone Master's Based on Search Data.
         */
        vm.search = function() {
        	vm.zoneMasteView=false;
            vm.searchData.selectedPageNumber = vm.page;
            vm.searchData.recordPerPage = vm.limit;
            vm.dataArr = [];
            zoneMasterFactory.search.query(vm.searchData).$promise.then(
                function(data) {
                    if (data.responseCode == "ERR0") {
                        vm.totalRecord = data.responseObject.totalRecord;
                        vm.dataArr = data.responseObject.searchResult;
                    }
                },
                function(errResponse) {
                	 $log.error('Error while fetching Zone');
                });
           }

        /**
         * Get Zone Master By id.
         * @param {int} id - The id of a Zone.
         * @param {boolean} isView - TO identify purpose of calling is for View or Edit a Zone.
         */
        vm.view = function(id, isView) {
        	zoneMasterFactory.findById.query({
                id: id
            }).$promise.then(function(data) {
                if (data.responseCode == 'ERR0') {
                    vm.zoneMaster = data.responseObject;
                    var rHistoryObj = {}

                    if (vm.zoneMaster != undefined && vm.zoneMaster != null && vm.zoneMaster.id != undefined && vm.zoneMaster.id != null) {
                        $rootScope.subTitle = vm.zoneMaster.zoneName;
                    } else {
                        $rootScope.subTitle = "Unknown Zone"
                    }
                    $rootScope.unfinishedFormTitle = "Zone # " + vm.zoneMaster.zoneCode;
                    if (isView) {
                        rHistoryObj = {
                            'title': "Zone #" + vm.zoneMaster.zoneCode,
                            'subTitle': $rootScope.subTitle,
                            'stateName': $state.current.name,
                            'stateParam': JSON.stringify($stateParams),
                            'stateCategory': 'Zone Master',
                            'serviceType': undefined,
                            'showService': false
                        }

                    } else {

                        rHistoryObj = {
                            'title': 'Edit Zone # ' + $stateParams.serviceId,
                            'subTitle': $rootScope.subTitle,
                            'stateName': $state.current.name,
                            'stateParam': JSON.stringify($stateParams),
                            'stateCategory': 'Zone Master',
                            'serviceType': 'MASTER',
                            'serviceCode': ' ',
                            'orginAndDestination': ' '
                        }
                    }
                    RecentHistorySaveService.form(rHistoryObj);
                } else {
                    $log.debug("Exception while getting Zone by Id ", data.responseDescription);
                }
            }, function(error) {
                $log.debug("Error ", error);
            });
        };

        /**
         * Go To Add Zone Master.
         */
        vm.addNew = function() {
        	if($rootScope.roleAccess(roleConstant.MASTER_GENERAL_ZONE_CREATE)){
            $state.go("layout.zoneMasterAdd");
        	}
        }

        /**
         * Validating field of Zone Master while instant change.
         * @param {errorElement,errorCode}
         */
        vm.validate = function(valElement, code) {
            var validationResponse = zoneMasterValidationService.validate(vm.zoneMaster,code);
            if(validationResponse.error == true) {
				$scope.validationErrorAndFocus(validationResponse, validationResponse.errElement);
			} else {
				$scope.errorMap.put(valElement, null);
            	validationResponse.error = false;
            }
        }
        
        /**
         * Cancel Add or Edit Zone Master.
         * @param {int} id - The id of a Zone
         */
       
        vm.cancel = function(objId) {
            if(vm.zoneMasterForm!=undefined){
		    	vm.goTocancel(vm.zoneMasterForm.$dirty,objId);
            }else{
            	vm.goTocancel(false,objId);
            }
        }
       
       vm.goTocancel = function(isDirty,objId){
     		 if (isDirty) {
     		      var newScope = $scope.$new();
     		      newScope.errorMessage = $rootScope.nls["ERR200"];
     		      ngDialog.openConfirm({
     		        template: '<p>{{errorMessage}}</p>' +
     		          '<div class="ngdialog-footer">' +
     		          ' <button type="button" class="btn btn-default btn-property cancel-btn" ng-click="confirm(2)">No</button>' +
     		          '<button type="button" class="btn btn-default btn-property accent-btn " ng-click="confirm(1)">Yes</button>' +
     		          '<button type="button" class="btn btn-default btn-property pull-left grey-cancel-btn" ng-click="confirm(3)">Cancel</button>' +
     		          '</div>',
     		        plain: true,
     		        closeByDocument: false,
     		        scope: newScope,
     		        className: 'ngdialog-theme-default'
     		      }).then(
     		        function(value) {
     		          if (value == 1) {
     		            if(objId!=null && objId != undefined){
     		            	vm.update();
     		            }else{
     		            	vm.create();
     		            }
     		          } else if (value == 2) {
     		        	 $state.go("layout.zoneMaster", {
     		                submitAction: "Cancelled"
     		            });
     		          } else {
     		            $log.debug("cancelled");
     		          }
     		        });
     		    } else {
     		     $state.go("layout.zoneMaster", {
                   submitAction: "Cancelled"
               });
     		    }
     	    }

        /**
         * Modify Existing Zone Master.
         * @param {int} id - The id of a Country.
         */
        vm.edit = function(id) {
        	if($rootScope.roleAccess(roleConstant.MASTER_GENERAL_ZONE_MODIFY)){
            $state.go("layout.zoneMasterEdit", {
                id: id
            });
        	}
        }

        /**
         * Delete Country Master.
         * @param {int} id - The id of a Country.
         */
        vm.delete = function(id) {
        	if($rootScope.roleAccess(roleConstant.MASTER_GENERAL_ZONE_DELETE)){
        	var newScope = $scope.$new();
			newScope.errorMessage = $rootScope.nls["ERR06404"];
	    	ngDialog.openConfirm( {
	                    template : '<p>{{errorMessage}}</p>'
	                    + '<div class="ngdialog-footer">'
	                    + '<button type="button" class="btn btn-default btn-property cancel-btn" ng-click="closeThisDialog(0)">Cancel'
	                    + '<button type="button" class="btn btn-primary btn-property accent-btn" ng-click="confirm(1)">Yes'
	                    + '</button></div>',
	                    plain : true,
	                    scope : newScope,
	                    closeByDocument: false,
	                    className : 'ngdialog-theme-default'
	                }).then( function(value) {
	                	zoneMasterFactory.delete.query({
	                        id: id
	                    }).$promise.then(function(data) {
	                        if (data.responseCode == 'ERR0') {
	                        	vm.zoneMasteView=false;
	                        	Notification.success($rootScope.nls["ERR402"]);
	                        	vm.deskTopView = true;
	                            vm.search();
	                        }
	                    }, function(error) {
	                        $log.debug("Error ", error);
	                    });
	                }, function(value) {
	                    $log.debug("delete Cancelled");
	            });
        	}
        }

        /**
         * Create New Zone Master.
         */
        vm.create = function() {
            var validationResponse = zoneMasterValidationService.validate(vm.zoneMaster,0);
            if(validationResponse.error == true) {
            	$log.debug("Validation Response -- ", validationResponse);
            	$log.debug('Validation Failed to create Zone..')
				$scope.validationErrorAndFocus(validationResponse, validationResponse.errElement);
			}
            else {
            	zoneMasterFactory.create.query(vm.zoneMaster).$promise.then(function(data) {
		            if (data.responseCode == 'ERR0') {
		            	$log.debug("Zone Saved Successfully Response :: ", data);
		            	Notification.success($rootScope.nls["ERR400"]);
		            	$state.go('layout.zoneMaster');
		            } else {
		                $log.debug("Create Zone Failed :" + data.responseDescription)
		            }

		        }, function(error) {
		        	 $log.debug("Create Zone Failed : " + error)
		        });
            } 
        };

        
      /**
       * Common Validation Focus Functionality - starts here" 
       */
		$scope.validationErrorAndFocus = function(valResponse, elemId){
			$scope.errorMap = new Map();
			if(valResponse.error == true){
				$scope.errorMap.put(valResponse.errElement, valResponse.errMessage);
			}
				$scope.navigateToNextField(elemId);
		}
        
        //Update Existing Country Master
        vm.update = function() {
        	 var validationResponse = zoneMasterValidationService.validate(vm.zoneMaster,0);
             if(validationResponse.error == true) {
             	$log.debug("Validation Response -- ", validationResponse);
             	$log.debug('Validation Failed while Updating the Zone..')
 				$scope.validationErrorAndFocus(validationResponse, validationResponse.errElement);
 			}
             else {
             	zoneMasterFactory.update.query(vm.zoneMaster).$promise.then(function(data) {
 		            if (data.responseCode == 'ERR0') {
 		            	$log.debug("Zone Updated Successfully Response :: ", data);
 		            	Notification.success($rootScope.nls["ERR401"]);
 		            	$state.go('layout.zoneMaster');
 		            } else {
 		                $log.debug("Updating Zone Failed :" + data.responseDescription)
 		            }
 		        }, function(error) {
 		        	 $log.debug("Updating Zone Failed : " + error)
 		        });
             } 
        };
        
        $scope.ajaxCountryEvent = function (object){
        	 vm.searchDto = {};
        	 vm.searchDto.keyword = object == null ? "" : object;
        	 vm.searchDto.selectedPageNumber = 0;
        	 vm.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

			return CountryList.fetch(vm.searchDto).$promise.then(
					function(data) {
						if (data.responseCode == "ERR0") {
							vm.zoneMasterList = data.responseObject.searchResult;
							return vm.zoneMasterList;
						}
					},
					function(errResponse) {
						$log.error('Error while fetching Countries');
					});

		}

        $scope.countryRender = function(item){
			return {
				label:item.countryName,
				item:item
			}
		}
        /**
         * Recent History - starts here
         */
        vm.isReloaded = localStorage.isZoneMasterReloaded;
        $scope.$on('addZoneMasterEvent', function(events, args){
        	$rootScope.unfinishedData = vm.zoneMaster;
			$rootScope.category = "Zone";
			$rootScope.unfinishedFormTitle = "Zone (New)";
        	if(vm.zoneMaster != undefined && vm.zoneMaster != null && vm.zoneMaster.zoneName != undefined && vm.zoneMaster.zoneName != null) {
        		$rootScope.subTitle = vm.zoneMaster.zoneName;
        	} else {
        		$rootScope.subTitle = "Unknown Zone "
        	}
        })
      
	    $scope.$on('editZoneMasterEvent', function(events, args){
	    	$rootScope.unfinishedData = vm.zoneMaster;
			$rootScope.category = "Zone";
			$rootScope.unfinishedFormTitle = "Zone Edit # "+vm.zoneMaster.zoneCode;
	    })
	      
	    $scope.$on('addZoneMasterEventReload', function(e, confirmation){
	    	    confirmation.message = "";
	    	    localStorage.reloadFormData = JSON.stringify(vm.zoneMaster) ;
	    	    localStorage.isZoneMasterReloaded = "YES";
	            e.preventDefault();
	    })
	    
	    $scope.$on('editZoneMasterEventReload', function(e, confirmation){
	    	    confirmation.message = "";
	    	    localStorage.reloadFormData = JSON.stringify(vm.zoneMaster) ;
	    	    localStorage.isZoneMasterReloaded = "YES";
	            e.preventDefault();
	    	  
	    })
	      


        /**
         * State Identification Handling Here.
         * @param {String} $stateParams.action - VIEW, ADD, EDIT, SEARCH.
         */
        //
        switch ($stateParams.action) {
            case "ADD":
            	 if ($stateParams.fromHistory === 'Yes') {
                     if (vm.isReloaded == "YES") {
                      vm.isReloaded = "NO";
                      localStorage.isZoneMasterReloaded = "NO";
                       vm.zoneMaster = JSON.parse(localStorage.reloadFormData);
                     } else {
                       vm.zoneMaster = $rootScope.selectedUnfilledFormData;
                     }
                   } else {
                     if (vm.isReloaded == "YES") {
                       vm.isReloaded = "NO";
                       localStorage.isZoneMasterReloaded = "NO";
                       vm.zoneMaster = JSON.parse(localStorage.reloadFormData);
                     } else {
                         $log.debug("Add NotHistory NotReload...")
                     }
                   }
                $rootScope.breadcrumbArr = zoneMasterDataService.getAddBreadCrumb();
                break;
            case "EDIT":
            	 if (vm.fromHistory === 'Yes') {
                     if (vm.isReloaded == "YES") {
                       vm.isReloaded = "NO";
                       localStorage.isZoneMasterReloaded = "NO";
                       vm.zoneMaster = JSON.parse(localStorage.reloadFormData);
                     } else {
                       vm.zoneMaster = $rootScope.selectedUnfilledFormData;
                     }
                   } else {
                     if (vm.isReloaded == "YES") {
                       vm.isReloaded = "NO";
                       localStorage.isZoneMasterReloaded = "NO";
                       vm.zoneMaster = JSON.parse(localStorage.reloadFormData);
                     } else {
                    	 vm.view($stateParams.id, false);
                     }
                   }
                $rootScope.breadcrumbArr = zoneMasterDataService.getEditBreadCrumb();
                break;
            default:
                $rootScope.breadcrumbArr = zoneMasterDataService.getListBreadCrumb();
                vm.searchInit();
        }

    }
]);
}) ();