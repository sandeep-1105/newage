app.controller('CarrierRateOperationController',['$rootScope', '$timeout', '$http', '$scope', '$state', '$modal', 'ngDialog', 
        'ngProgressFactory', 'discardService', '$stateParams',
    'CarrierRateSave', 'CarrierSearchKeyword', 'ChargeSearchKeyword', 'appConstant', 'CommonValidationService', 
    'UnitSearchKeyword', 'PortSearchKeyword', 'CarrierRateView', 'CarrierRateEdit', 'UnitMasterGetByCode',
    'cloneService', 'CarrierRateBulkFileUpload', 'PortByTransportMode', 'roleConstant',
    function($rootScope, $timeout, $http, $scope, $state, $modal, ngDialog, 
        ngProgressFactory, discardService, $stateParams,
    CarrierRateSave, CarrierSearchKeyword, ChargeSearchKeyword, appConstant, CommonValidationService, 
    UnitSearchKeyword, PortSearchKeyword, CarrierRateView, CarrierRateEdit, UnitMasterGetByCode,
    cloneService, CarrierRateBulkFileUpload, PortByTransportMode, roleConstant) {

    $scope.errorMap = new Map();
    $scope.chargeType = [{
        id: "CHARGEABLE",
        title: "Chargeable Weight"
    }, {
        id: "ACTUAL",
        title: "Actual"
    }];
    $scope.init = function() {
        $scope.firstFocus = true;
        $scope.errorArray = [];
        $scope.contained_progressbar = ngProgressFactory.createInstance();
        $scope.contained_progressbar.setParent(document.getElementById('carrier-rate-panel'));
        $scope.contained_progressbar.setAbsolute();
    };

    $scope.setOldDataVal = function(object, falg) {
        if (falg) {
            object.firstFromDate = $rootScope.dateToString(object.firstFromDate);
            object.secondFromDate = $rootScope.dateToString(object.secondFromDate);
            $scope.oldData = JSON.stringify(object);
        } else {
            $scope.oldData = JSON.stringify(object);
        }

    }


    $scope.ajaxCarrierDestinationEvent = function(object, carrierRatedata) {
        console.log("Ajax CarrierUnit Event method : ", object)
        $scope.searchDto = {};
        $scope.searchDto.keyword = object == null ? "" : object;
        $scope.searchDto.selectedPageNumber = 0;
        $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
        if (carrierRatedata.carrierMaster == null || carrierRatedata.carrierMaster == undefined ||
            carrierRatedata.carrierMaster == "" || carrierRatedata.carrierMaster.transportMode == null ||
            carrierRatedata.carrierMaster.transportMode == undefined ||
            carrierRatedata.carrierMaster.transportMode == "") {
            console.log('Please select carrier');
            return;
        }
        return PortByTransportMode.fetch({
            "transportMode": carrierRatedata.carrierMaster.transportMode
        }, $scope.searchDto).$promise.then(function(data, status) {
                if (data.responseCode == "ERR0") {
                    $scope.portList = data.responseObject.searchResult;
                    console.log("$scope.portList ", $scope.portList);
                    $scope.tempPortList = [];
                    if (carrierRatedata.origin != null && carrierRatedata.origin != undefined && carrierRatedata.origin != "") {
                        for (var i = 0; i < $scope.portList.length; i++) {
                            if ($scope.portList[i].id != carrierRatedata.origin.id) {
                                $scope.tempPortList.push($scope.portList[i]);
                            }
                        }
                    } else {
                        $scope.tempPortList = $scope.portList;
                    }
                    return $scope.tempPortList;
                }
            },
            function(errResponse) {
                console.error('Error while fetching Carrier Rate');
            });

    };


    $scope.ajaxCarrierOriginEvent = function(object, carrierRatedata) {
        console.log("Ajax CarrierUnit Event method : ", object)
        $scope.searchDto = {};
        $scope.searchDto.keyword = object == null ? "" : object;
        $scope.searchDto.selectedPageNumber = 0;
        //return PortSearchKeyword.fetch($scope.searchDto).$promise.then(function(data, status) {

        if (carrierRatedata.carrierMaster == null || carrierRatedata.carrierMaster == undefined ||
            carrierRatedata.carrierMaster == "" || carrierRatedata.carrierMaster.transportMode == null ||
            carrierRatedata.carrierMaster.transportMode == undefined ||
            carrierRatedata.carrierMaster.transportMode == "") {
            console.log('Please select carrier');
            return;
        }
        return PortByTransportMode.fetch({
            "transportMode": carrierRatedata.carrierMaster.transportMode
        }, $scope.searchDto).$promise.then(function(data, status) {

                if (data.responseCode == "ERR0") {
                    $scope.portList = data.responseObject.searchResult;
                    console.log("$scope.portList ", $scope.portList);
                    $scope.tempPortList = [];
                    if (carrierRatedata.destination != null && carrierRatedata.destination != undefined && carrierRatedata.destination != "") {
                        for (var i = 0; i < $scope.portList.length; i++) {
                            if ($scope.portList[i].id != carrierRatedata.destination.id) {
                                $scope.tempPortList.push($scope.portList[i]);
                            }
                        }
                    } else {
                        $scope.tempPortList = $scope.portList;
                    }
                    return $scope.tempPortList;
                }
            },
            function(errResponse) {
                console.error('Error while fetching Origin port');
            });

    };

    $scope.carrierPortRender = function(item) {
        return {
            label: item.portName,
            item: item
        }
    };

    $scope.carrierPortEditRender = function(item) {
        return {
            label: item.portName,
            item: item
        }
    };

    $scope.ajaxCarrierUnitEvent = function(object) {
        console.log("Ajax CarrierUnit Event method : ", object)
        $scope.searchDto = {};
        $scope.searchDto.keyword = object == null ? "" : object;
        $scope.searchDto.selectedPageNumber = 0;
        $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

        return UnitSearchKeyword.query($scope.searchDto).$promise.then(function(data, status) {
                if (data.responseCode == "ERR0") {
                    $scope.unitList = data.responseObject.searchResult;
                    console.log("$scope.unitList ", $scope.unitList);
                    return $scope.unitList;
                }
            },
            function(errResponse) {
                console.error('Error while fetching unit');
            });

    };

    $scope.carrierUnitRender = function(item) {
        return {
            label: item.unitName,
            item: item
        }
    };

    $scope.ajaxCarrierChargeEvent = function(object) {
        console.log("Ajax CarrierCharge Event method : ", object)
        $scope.searchDto = {};
        $scope.searchDto.keyword = object == null ? "" : object;
        $scope.searchDto.selectedPageNumber = 0;
        $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

        return ChargeSearchKeyword.query($scope.searchDto).$promise.then(function(data, status) {
                if (data.responseCode == "ERR0") {
                    $scope.chargeList = data.responseObject.searchResult;
                    console.log("$scope.chargeList ", $scope.chargeList);
                    return $scope.chargeList;
                }
            },
            function(errResponse) {
                console.error('Error while fetching Carrier Charge');
            });

    };

    $scope.carrierChargeRender = function(item) {
        return {
            label: item.chargeName,
            item: item
        }
    };

    $scope.ajaxCarrierRateEvent = function(object) {
        console.log("Ajax Carrier Rate Event method : ", object)
        $scope.searchDto = {};
        $scope.searchDto.keyword = object == null ? "" : object;
        $scope.searchDto.selectedPageNumber = 0;
        $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

        return CarrierSearchKeyword.fetch($scope.searchDto).$promise.then(function(data, status) {
                if (data.responseCode == "ERR0") {
                    $scope.carrierList = data.responseObject.searchResult;
                    console.log("$scope.carrierList ", $scope.carrierList);
                    return $scope.carrierList;
                }
            },
            function(errResponse) {
                console.error('Error while fetching Carrier');
            });
    };

    $scope.carrierRateRender = function(item) {
        return {
            label: item.carrierName,
            item: item
        }
    };


    $scope.removeCarrier = function(index) {
        if ($rootScope.roleAccess(roleConstant.MASTER_EAIR_CARRIER_RATE_RATE_DELETE)) {
            console.log("remove Carrier Rate is called ", index);
            //$scope.isDirty = $scope.carrierRateForm.$dirty;
            if (!$scope.isEmptyRow($scope.carrierRateDataList[index])) {
                var newScope = $scope.$new();
                newScope.errorMessage = $rootScope.nls["ERR203"];
                newScope.errorMessage = newScope.errorMessage.replace("%s", " the selected Carrier Rate");
                ngDialog.openConfirm({
                    template: '<p>{{errorMessage}}</p>' +
                        '<div class="ngdialog-footer">' +
                        ' <button type="button" class="btn btn-default btn-property cancel-btn" ng-click="confirm(2)">No</button>' +
                        '<button type="button" class="btn btn-default btn-property accent-btn " ng-click="confirm(1)">Yes</button>' +
                        '</div>',
                    plain: true,
                    closeByDocument: false,
                    scope: newScope,
                    className: 'ngdialog-theme-default'
                }).then(function(value) {
                    if (value == 1) {
                        $scope.carrierRateDataList.splice(index, 1);
                    } else if (value == 2) {
                        console.log("You selected No");
                    }
                });
            } else {
                $scope.carrierRateDataList.splice(index, 1);
            }
        }
    };



    //show errors

    var myOtherModal = $modal({
        scope: $scope,
        templateUrl: 'errorPopUp.html',
        show: false,
        keyboard: true
    });

    var errorOnRowIndex = null;

    $scope.errorShow = function(errorObj, index) {
        $scope.errList = errorObj.errTextArr;
        errorOnRowIndex = index;
        myOtherModal.$promise.then(myOtherModal.show);
    };

    $scope.validation = function(index) {
        $scope.errorArray[index] = {};
        $scope.errorArray[index].errTextArr = [];
        var errorFound = false;

        if ($scope.carrierRateDataList[index].carrierMaster == undefined ||
            $scope.carrierRateDataList[index].carrierMaster == null ||
            $scope.carrierRateDataList[index].carrierMaster.length == 0) {
            $scope.errorArray[index].errRow = true;
            $scope.errorArray[index].carrierCode = true;
            $scope.errorArray[index].errTextArr.push($rootScope.nls["ERR1266"]);
            errorFound = true;
        } else if ($scope.carrierRateDataList[index].carrierMaster.status == 'Block') {
            $scope.errorArray[index].errRow = true;
            $scope.errorArray[index].carrierCode = true;
            $scope.errorArray[index].errTextArr.push($rootScope.nls["ERR1267"]);
            errorFound = true;
        } else if ($scope.carrierRateDataList[index].carrierMaster.status == 'Hide') {
            $scope.errorArray[index].errRow = true;
            $scope.errorArray[index].carrierCode = true;
            $scope.errorArray[index].errTextArr.push($rootScope.nls["ERR1268"]);
            errorFound = true;
        } else {
            $scope.errorArray[index].carrierCode = false;
        }



        if ($scope.carrierRateDataList[index].chargeMaster == undefined ||
            $scope.carrierRateDataList[index].chargeMaster == null ||
            $scope.carrierRateDataList[index].chargeMaster.length == 0) {
            $scope.errorArray[index].errRow = true;
            $scope.errorArray[index].chargeCode = true;
            $scope.errorArray[index].errTextArr.push($rootScope.nls["ERR1269"]);
            errorFound = true;
        } else if ($scope.carrierRateDataList[index].chargeMaster.status == 'Block') {
            $scope.errorArray[index].errRow = true;
            $scope.errorArray[index].chargeCode = true;
            $scope.errorArray[index].errTextArr.push($rootScope.nls["ERR1270"]);
            errorFound = true;
        } else if ($scope.carrierRateDataList[index].chargeMaster.status == 'Hide') {
            $scope.errorArray[index].errRow = true;
            $scope.errorArray[index].chargeCode = true;
            $scope.errorArray[index].errTextArr.push($rootScope.nls["ERR1271"]);
            errorFound = true;
        } else {
            $scope.errorArray[index].chargeCode = false;
        }



        if ($scope.carrierRateDataList[index].unitMaster == undefined ||
            $scope.carrierRateDataList[index].unitMaster == null ||
            $scope.carrierRateDataList[index].unitMaster.length == 0) {
            $scope.errorArray[index].errRow = true;
            $scope.errorArray[index].unitCode = true;
            $scope.errorArray[index].errTextArr.push($rootScope.nls["ERR1263"]);
            errorFound = true;
        } else if ($scope.carrierRateDataList[index].unitMaster.status == 'Block') {
            $scope.errorArray[index].errRow = true;
            $scope.errorArray[index].unitCode = true;
            $scope.errorArray[index].errTextArr.push($rootScope.nls["ERR1264"]);
            errorFound = true;
        } else if ($scope.carrierRateDataList[index].unitMaster.status == 'Hide') {
            $scope.errorArray[index].errRow = true;
            $scope.errorArray[index].unitCode = true;
            $scope.errorArray[index].errTextArr.push($rootScope.nls["ERR1265"]);
            errorFound = true;
        } else {
            $scope.errorArray[index].unitCode = false;
        }


        if ($scope.carrierRateDataList[index].origin == undefined ||
            $scope.carrierRateDataList[index].origin == null ||
            $scope.carrierRateDataList[index].origin.length == 0) {
            $scope.errorArray[index].errRow = true;
            $scope.errorArray[index].origin = true;
            $scope.errorArray[index].errTextArr.push($rootScope.nls["ERR1281"]);
            errorFound = true;
        } else if ($scope.carrierRateDataList[index].origin.status == 'Block') {
            $scope.errorArray[index].errRow = true;
            $scope.errorArray[index].origin = true;
            $scope.errorArray[index].errTextArr.push($rootScope.nls["ERR1278"]);
            errorFound = true;
        } else if ($scope.carrierRateDataList[index].origin.status == 'Hide') {
            $scope.errorArray[index].errRow = true;
            $scope.errorArray[index].origin = true;
            $scope.errorArray[index].errTextArr.push($rootScope.nls["ERR1262"]);
            errorFound = true;
        } else {
            $scope.errorArray[index].origin = false;
        }



        if ($scope.carrierRateDataList[index].destination == undefined ||
            $scope.carrierRateDataList[index].destination == null ||
            $scope.carrierRateDataList[index].destination.length == 0) {
            $scope.errorArray[index].errRow = true;
            $scope.errorArray[index].destination = true;
            $scope.errorArray[index].errTextArr.push($rootScope.nls["ERR1280"]);
            errorFound = true;
        } else if ($scope.carrierRateDataList[index].destination.status == 'Block') {
            $scope.errorArray[index].errRow = true;
            $scope.errorArray[index].destination = true;
            $scope.errorArray[index].errTextArr.push($rootScope.nls["ERR1279"]);
            errorFound = true;
        } else if ($scope.carrierRateDataList[index].destination.status == 'Hide') {
            $scope.errorArray[index].errRow = true;
            $scope.errorArray[index].destination = true;
            $scope.errorArray[index].errTextArr.push($rootScope.nls["ERR1261"]);
            errorFound = true;
        } else if ($scope.carrierRateDataList[index].destination.portCode == $scope.carrierRateDataList[index].origin.portCode) {
            $scope.errorArray[index].errRow = true;
            $scope.errorArray[index].destination = true;
            $scope.errorArray[index].errTextArr.push($rootScope.nls["ERR1283"]);
            errorFound = true;
        } else {
            $scope.errorArray[index].destination = false;
        }



        if ($scope.carrierRateDataList[index].actualChargeable == undefined ||
            $scope.carrierRateDataList[index].actualChargeable == null) {
            $scope.errorArray[index].errRow = true;
            $scope.errorArray[index].actualChargeable = true;
            $scope.errorArray[index].errTextArr.push($rootScope.nls["ERR1282"]);
            errorFound = true;
        } else {
            $scope.errorArray[index].actualChargeable = false;
        }


        if ($scope.carrierRateDataList[index].firstFromDate == undefined ||
            $scope.carrierRateDataList[index].firstFromDate == null ||
            $scope.carrierRateDataList[index].firstFromDate == "") {

            $scope.errorArray[index].errRow = true;
            $scope.errorArray[index].firstFromDate = true;
            $scope.errorArray[index].errTextArr.push($rootScope.nls["ERR1284"]);
            errorFound = true;
        }

        /* if ($scope.carrierRateDataList[index].secondFromDate == undefined
				  || $scope.carrierRateDataList[index].secondFromDate == null
				  || $scope.carrierRateDataList[index].secondFromDate== "") {
				  
				  $scope.errorArray[index].errRow = true;
				   $scope.errorArray[index].secondFromDate = true;
				   $scope.errorArray[index].errTextArr.push($rootScope.nls["ERR1285"]);	
				   errorFound = true;
			  } */

        if ($scope.carrierRateDataList[index].firstFromDate != undefined &&
            $scope.carrierRateDataList[index].firstFromDate != null &&
            $scope.carrierRateDataList[index].firstFromDate != "" &&
            $scope.carrierRateDataList[index].secondFromDate != undefined &&
            $scope.carrierRateDataList[index].secondFromDate != null &&
            $scope.carrierRateDataList[index].secondFromDate != "") {

            var fDate = $rootScope.convertToDate($scope.carrierRateDataList[index].firstFromDate);
            var sDate = $rootScope.convertToDate($scope.carrierRateDataList[index].secondFromDate);
            if (sDate <= fDate) {
                $scope.errorArray[index].errRow = true;
                $scope.errorArray[index].secondFromDate = true;
                $scope.errorArray[index].errTextArr.push($rootScope.nls["ERR1287"]);
                errorFound = true;
            }

        } else {
            $scope.carrierRateDataList[index].secondFromDate = null;
        }



        //First Effective Amount validation
        var fa = $scope.carrierRateDataList[index].firstAmount;
        if (fa == 0) {
            $scope.errorArray[index].errRow = true;
            $scope.errorArray[index].firstAmount = true;
            $scope.errorArray[index].errTextArr.push($rootScope.nls["ERR1272"]);
            errorFound = true;
        } else {
            if ($scope.carrierRateDataList[index].firstAmount == null || $scope.carrierRateDataList[index].firstAmount == undefined || $scope.carrierRateDataList[index].firstAmount == "") {
                $scope.errorArray[index].errRow = true;
                $scope.errorArray[index].firstAmount = true;
                $scope.errorArray[index].errTextArr.push($rootScope.nls["ERR1292"]);
                errorFound = true;
            }
        }
        //First Effective MinAmount validation 
        var fma = $scope.carrierRateDataList[index].firstMinAmount;
        if (fma == 0) {
            $scope.errorArray[index].errRow = true;
            $scope.errorArray[index].firstMinAmount = true;
            $scope.errorArray[index].errTextArr.push($rootScope.nls["ERR1273"]);
            errorFound = true;
        } else {
            fma = null;
        }
        //Second Effective Amount validation 
        var sa = $scope.carrierRateDataList[index].secondAmount;
        if (sa == 0) {
            $scope.errorArray[index].errRow = true;
            $scope.errorArray[index].secondAmount = true;
            $scope.errorArray[index].errTextArr.push($rootScope.nls["ERR1274"]);
            errorFound = true;
        } else {
            sa = null;
        }
        //Second Effective MinAmount validation 
        var sma = $scope.carrierRateDataList[index].secondMinAmount;
        if (sma == 0) {
            $scope.errorArray[index].errRow = true;
            $scope.errorArray[index].secondMinAmount = true;
            $scope.errorArray[index].errTextArr.push($rootScope.nls["ERR1275"]);
            errorFound = true;
        } else {
            sma = null;
        }


        return errorFound;

    }

    $scope.validateCarrierRate = function(validateCode) {
        console.log("validateCarrierRate", validateCode);
        $scope.errorMap = new Map();
        if (validateCode == 0 || validateCode == 1) {
            //carrier validation
            if ($scope.carrierRateMaster.carrierMaster == undefined ||
                $scope.carrierRateMaster.carrierMaster == null ||
                $scope.carrierRateMaster.carrierMaster == "") {
                $scope.errorMap.put("carrierName", $rootScope.nls["ERR1266"]);
                return false;
            } else if ($scope.carrierRateMaster.carrierMaster.status == "Block") {
                $scope.errorMap.put("carrierName", $rootScope.nls["ERR1267"]);
                return false;
            } else if ($scope.carrierRateMaster.carrierMaster.status == "Hide") {
                $scope.errorMap.put("carrierName", $rootScope.nls["ERR1268"]);
                return false;
            }
            //charge validation
            if ($scope.carrierRateMaster.chargeMaster == undefined ||
                $scope.carrierRateMaster.chargeMaster == null ||
                $scope.carrierRateMaster.chargeMaster == "") {
                $scope.errorMap.put("chargeName", $rootScope.nls["ERR1269"]);
                return false;
            } else if ($scope.carrierRateMaster.chargeMaster.status == "Block") {
                $scope.errorMap.put("chargeName", $rootScope.nls["ERR1270"]);
                return false;
            } else if ($scope.carrierRateMaster.chargeMaster.status == "Hide") {
                $scope.errorMap.put("chargeName", $rootScope.nls["ERR1271"]);
                return false;
            }
            //unit validation
            if ($scope.carrierRateMaster.unitMaster == undefined ||
                $scope.carrierRateMaster.unitMaster == null ||
                $scope.carrierRateMaster.unitMaster == "") {
                $scope.errorMap.put("unitName", $rootScope.nls["ERR1263"]);
                return false;
            } else if ($scope.carrierRateMaster.unitMaster.status == "Block") {
                $scope.errorMap.put("unitName", $rootScope.nls["ERR1264"]);
                return false;
            } else if ($scope.carrierRateMaster.unitMaster.status == "Hide") {
                $scope.errorMap.put("unitName", $rootScope.nls["ERR1265"]);
                return false;
            }

        }


        if (validateCode == 0 || validateCode == 2) {
            if ($scope.carrierRateMaster.firstFromDate == undefined ||
                $scope.carrierRateMaster.firstFromDate == null ||
                $scope.carrierRateMaster.firstFromDate == "") {
                $scope.errorMap.put("firstFromDate", $rootScope.nls["ERR1284"]);
                return false;
            } else if ($scope.carrierRateMaster.secondFromDate == $scope.carrierRateMaster.firstFromDate) {
                $scope.errorMap.put("firstFromDate", $rootScope.nls["ERR1286"]);
                return false;
            }
            if ($scope.carrierRateMaster.secondFromDate != undefined &&
                $scope.carrierRateMaster.secondFromDate != null &&
                $scope.carrierRateMaster.secondFromDate != "") {
                if ($scope.carrierRateMaster.firstFromDate > $scope.carrierRateMaster.secondFromDate) {
                    $scope.errorMap.put("firstFromDate", $rootScope.nls["ERR1286"]);
                    return false;
                }
            }


        }

        if (validateCode == 0 || validateCode == 3) {
            /* if ($scope.carrierRateMaster.secondFromDate == undefined
					  || $scope.carrierRateMaster.secondFromDate == null
					  || $scope.carrierRateMaster.secondFromDate== "") {
					  $scope.errorMap.put("secondFromDate",$rootScope.nls["ERR1285"]);
					  return false;
				  } */

            if ($scope.carrierRateMaster != undefined && $scope.carrierRateMaster != null) {
                if ($scope.carrierRateMaster.firstFromDate != undefined && $scope.carrierRateMaster.firstFromDate != null && $scope.carrierRateMaster.firstFromDate != "" &&
                    $scope.carrierRateMaster.secondFromDate != undefined && $scope.carrierRateMaster.secondFromDate != null && $scope.carrierRateMaster.secondFromDate != "") {
                    if ($scope.carrierRateMaster.firstFromDate > $scope.carrierRateMaster.secondFromDate) {
                        $scope.errorMap.put("secondFromDate", $rootScope.nls["ERR1287"]);
                        return false;
                    }
                    if ($scope.carrierRateMaster.secondFromDate == $scope.carrierRateMaster.firstFromDate) {
                        $scope.errorMap.put("secondFromDate", $rootScope.nls["ERR1287"]);
                        return false;
                    }
                } else {
                    $scope.carrierRateMaster.secondFromDate = null;
                }
            }
        }

        if (validateCode == 0 || validateCode == 4) {
            //Amount validation
            if ($scope.carrierRateMaster != undefined && $scope.carrierRateMaster != null) {

                var a = $scope.carrierRateMaster.firstAmount;

                if (a == 0) {
                    $scope.errorMap.put("firstAmount", $rootScope.nls["ERR1272"]);
                    return false;
                }

                if (a !== a || $scope.carrierRateMaster.firstAmount == null || $scope.carrierRateMaster.firstAmount == undefined || $scope.carrierRateMaster.firstAmount == "") {
                    $scope.errorMap.put("firstAmount", $rootScope.nls["ERR1292"]);
                    return false;
                } else {
                    if ($scope.carrierRateMaster.firstAmount < 0) {
                        $scope.errorMap.put("firstAmount", $rootScope.nls["ERR1288"]);
                        return false;
                    }
                    /* if(!CommonValidationService.checkRegExp(appConstant.Reg_Exp_Master_Carrier_Rate_or_amount,$scope.carrierRateMaster.firstAmount)){
   						   $scope.errorMap.put("firstAmount",$rootScope.nls["ERR1288"]);
   							return false;
   						}*/
                }

                if ($scope.carrierRateMaster.firstMinAmount != null || $scope.carrierRateMaster.firstMinAmount != undefined || $scope.carrierRateMaster.firstMinAmount != "") {
                    if ($scope.carrierRateMaster.firstMinAmount < 0) {
                        $scope.errorMap.put("firstMinAmount", $rootScope.nls["ERR1289"]);
                        return false;
                    }
                    if ($scope.carrierRateMaster.firstMinAmount == 0) {
                        $scope.errorMap.put("firstMinAmount", $rootScope.nls["ERR1273"]);
                        return false;
                    }

                    /* if(!CommonValidationService.checkRegExp(appConstant.Reg_Exp_Master_Carrier_Rate_or_amount,$scope.carrierRateMaster.firstMinAmount)){
   						   $scope.errorMap.put("firstMinAmount",$rootScope.nls["ERR1289"]);
   							return false;
   						}*/

                } else {
                    $scope.carrierRateMaster.firstMinAmount = null;
                }

                /* if($scope.carrierRateMaster.secondAmount==null
   						  ||$scope.carrierRateMaster.secondAmount==undefined
   						  ||$scope.carrierRateMaster.secondAmount==""){ 
   					  $scope.errorMap.put("secondAmount",$rootScope.nls["ERR1294"]);

   						  return false;
                    }*/


                if ($scope.carrierRateMaster.secondAmount != null ||
                    $scope.carrierRateMaster.secondAmount != undefined ||
                    $scope.carrierRateMaster.secondAmount != "") {

                    if ($scope.carrierRateMaster.secondAmount < 0) {
                        $scope.errorMap.put("secondAmount", $rootScope.nls["ERR1290"]);
                        return false;
                    }
                    if ($scope.carrierRateMaster.secondAmount == 0) {
                        $scope.errorMap.put("secondAmount", $rootScope.nls["ERR1274"]);
                        return false;
                    }

                    /* if(!CommonValidationService.checkRegExp(appConstant.Reg_Exp_Master_Carrier_Rate_or_amount,$scope.carrierRateMaster.secondAmount)){
   						   $scope.errorMap.put("secondAmount",$rootScope.nls["ERR1290"]);
   							return false;
   						}*/

                } else {
                    $scope.carrierRateMaster.secondAmount = null;
                }

                if ($scope.carrierRateMaster.secondMinAmount != null ||
                    $scope.carrierRateMaster.secondMinAmount != undefined ||
                    $scope.carrierRateMaster.secondMinAmount != "") {

                    if ($scope.carrierRateMaster.secondMinAmount == 0) {
                        $scope.errorMap.put("secondMinAmount", $rootScope.nls["ERR1275"]);

                        return false;
                    }
                    if ($scope.carrierRateMaster.secondMinAmount < 0) {
                        $scope.errorMap.put("secondMinAmount", $rootScope.nls["ERR1291"]);

                        return false;
                    }
                    /*if(!CommonValidationService.checkRegExp(appConstant.Reg_Exp_Master_Carrier_Rate_or_amount,$scope.carrierRateMaster.secondMinAmount)){
   						  $scope.errorMap.put("secondMinAmount",$rootScope.nls["ERR1291"]);
   							return false;
   						}*/

                } else {
                    $scope.carrierRateMaster.secondMinAmount = null;
                }


            }


        }
        if (validateCode == 0 || validateCode == 5) {
            //origin validation
            if ($scope.carrierRateMaster != undefined && $scope.carrierRateMaster != null) {
                if ($scope.carrierRateMaster.origin == undefined ||
                    $scope.carrierRateMaster.origin == null ||
                    $scope.carrierRateMaster.origin == "") {
                    $scope.errorMap.put("originName", $rootScope.nls["ERR1281"]);
                    return false;
                } else if ($scope.carrierRateMaster.origin.status == "Block") {
                    $scope.errorMap.put("originName", $rootScope.nls["ERR1278"]);
                    return false;
                } else if ($scope.carrierRateMaster.origin.status == "Hide") {
                    $scope.errorMap.put("originName", $rootScope.nls["ERR1262"]);
                    return false;
                }

                if ($scope.carrierRateMaster.origin != undefined && $scope.carrierRateMaster.origin != null && $scope.carrierRateMaster.origin != "" &&
                    $scope.carrierRateMaster.destination != undefined && $scope.carrierRateMaster.destination != null && $scope.carrierRateMaster.destination != "") {
                    if ($scope.carrierRateMaster.origin.portCode == $scope.carrierRateMaster.destination.portCode) {
                        $scope.errorMap.put("originName", $rootScope.nls["ERR1283"]);
                        return false;
                    }
                }
            }
        }

        if (validateCode == 0 || validateCode == 6) {
            //Destination validation
            if ($scope.carrierRateMaster != undefined && $scope.carrierRateMaster != null) {
                if ($scope.carrierRateMaster.destination == undefined || $scope.carrierRateMaster.destination == null || $scope.carrierRateMaster.destination == "") {
                    $scope.errorMap.put("destinationName", $rootScope.nls["ERR1280"]);
                    return false;
                } else if ($scope.carrierRateMaster.destination.status == "Block") {
                    $scope.errorMap.put("destinationName", $rootScope.nls["ERR1279"]);
                    return false;
                } else if ($scope.carrierRateMaster.destination.status == "Hide") {
                    $scope.errorMap.put("destinationName", $rootScope.nls["ERR1261"]);
                    return false;
                }
                if ($scope.carrierRateMaster.origin != undefined && $scope.carrierRateMaster.origin != null && $scope.carrierRateMaster.origin != "" &&
                    $scope.carrierRateMaster.destination != undefined && $scope.carrierRateMaster.destination != null && $scope.carrierRateMaster.destination != "") {
                    if ($scope.carrierRateMaster.origin.portCode == $scope.carrierRateMaster.destination.portCode) {
                        $scope.errorMap.put("destinationName", $rootScope.nls["ERR1283"]);
                        return false;
                    }
                }
            }
        }




        return true;
    }



    $scope.addNewRate = function() {
        if ($rootScope.roleAccess(roleConstant.MASTER_EAIR_CARRIER_RATE_RATE_CREATE)) {
            console.log("Add Row is clicked");
            $scope.firstFocus = true;
            $scope.errorArray = [];
            var listErrorFlag = false;
            if ($scope.carrierRateDataList != undefined && $scope.carrierRateDataList != null && $scope.carrierRateDataList.length > 0) {
                for (var index = 0; index < $scope.carrierRateDataList.length; index++) {
                    if ($scope.validation(index)) {
                        listErrorFlag = true;
                    }
                }
                if (listErrorFlag) {
                    // $scope.carrierRateDataList.push({});
                } else {
                    $scope.carrierRateDataList.push({});
                }
            } else {
                $scope.carrierRateDataList.push({});
            }
        }
    };

    $scope.isEmptyRow = function(obj) {
        //return (Object.getOwnPropertyNames(obj).length === 0);
        var isempty = true; //  empty
        if (!obj) {
            return isempty;
        }
        var k = Object.getOwnPropertyNames(obj);
        for (var i = 0; i < k.length; i++) {
            if (obj[k[i]]) {
                isempty = false; // not empty
                break;
            }
        }
        return isempty;
    }


    $scope.saveCarrierRate = function() {
        console.log("Save CarrierRate method is called.", $scope.carrierRateDataList);
        $scope.errorArray = [];
        var listErrorFlag = false;

        if ($scope.carrierRateDataList != undefined && $scope.carrierRateDataList != null && $scope.carrierRateDataList.length > 0) {
            for (var index = 0; index < $scope.carrierRateDataList.length; index++) {
                if (index == 0) {
                    if ($scope.validation(index)) {
                        listErrorFlag = true;
                    }
                } else {
                    if ($scope.isEmptyRow($scope.carrierRateDataList[index])) {
                        $scope.carrierRateDataList.splice(index, 1);
                        listErrorFlag = false;
                    } else {
                        if ($scope.validation(index)) {
                            listErrorFlag = true;
                        }
                    }
                }
            }
            if (listErrorFlag) {
                return;
            } else {
                $scope.tmpCarrierRateDataList = cloneService.clone($scope.carrierRateDataList);
                for (var i = 0; i < $scope.tmpCarrierRateDataList.length; i++) {
                    $scope.tmpCarrierRateDataList[i].firstFromDate = $rootScope.sendApiStartDateTime($scope.tmpCarrierRateDataList[i].firstFromDate);
                    $scope.tmpCarrierRateDataList[i].secondFromDate = $rootScope.sendApiStartDateTime($scope.tmpCarrierRateDataList[i].secondFromDate);

                }
            }

            CarrierRateSave.save($scope.tmpCarrierRateDataList).$promise.then(function(data) {
                if (data.responseCode == 'ERR0') {
                    console.log("CarrierRate Saved successfully..");
                    var params = {};
                    params.submitAction = 'Saved';
                    $rootScope.successDesc = $rootScope.nls["ERR400"];
                    $state.go("layout.carrierRate", params);
                } else {
                    console.log("CarrierRate Saving Failed " + data.responseDescription)
                }
            }, function(error) {
                console.log("CarrierRateMaster Saving Failed : " + error)
            });

        }
    }

    /* $scope.dateConvertion = function() {
		  if($scope.carrierRateMaster.id != undefined && $scope.carrierRateMaster.id != null) {
				  $scope.carrierRateMaster.firstFromDate = $rootScope.sendApiDateAndTime($scope.carrierRateMaster.firstFromDate);
				  $scope.carrierRateMaster.secondFromDate = $rootScope.sendApiDateAndTime($scope.carrierRateMaster.secondFromDate);
		  }
	} */


    $scope.updateCarrierRate = function() {
        console.log(" CarrierRateMaster Update Method is called.");

        if ($scope.validateCarrierRate(0)) {

            $scope.tmpCarrierRateMaster = cloneService.clone($scope.carrierRateMaster);

            if (!$scope.tmpCarrierRateMaster.actualChargeable) {
                $scope.tmpCarrierRateMaster.actualChargeable = "CHARGEABLE";
            } else {
                $scope.tmpCarrierRateMaster.actualChargeable = "ACTUAL";
            }

            $scope.tmpCarrierRateMaster.firstFromDate = $rootScope.sendApiStartDateTime($scope.tmpCarrierRateMaster.firstFromDate);
            $scope.tmpCarrierRateMaster.secondFromDate = $rootScope.sendApiStartDateTime($scope.tmpCarrierRateMaster.secondFromDate);

            CarrierRateEdit.update($scope.tmpCarrierRateMaster).$promise.then(
                function(data) {
                    if (data.responseCode == 'ERR0') {
                        console.log("CarrierRateMaster updated Successfully")
                        $rootScope.successDesc = $rootScope.nls["ERR401"];
                        var params = {}
                        params.submitAction = 'Saved';
                        $state.go("layout.carrierRate", params);

                    } else {
                        console.log("CarrirRateMaster updated Failed ", data.responseDescription)
                    }
                },
                function(error) {
                    console.log("CarrirRateMaster updated Failed : " + error)
                });
        } else {
            console.log("Invalid Form Submission.");
        }

    }

    $scope.cancelEdit = function() {
        if ($scope.oldData != JSON.stringify($scope.carrierRateMaster)) {
            var newScope = $scope.$new();
            newScope.errorMessage = $rootScope.nls["ERR200"];
            ngDialog.openConfirm({
                template: '<p>{{errorMessage}}</p>' +
                    '<div class="ngdialog-footer">' +
                    ' <button type="button" class="btn btn-default btn-property cancel-btn" ng-click="confirm(2)">No</button>' +
                    '<button type="button" class="btn btn-default btn-property accent-btn " ng-click="confirm(1)">Yes</button>' +
                    '<button type="button" class="btn btn-default btn-property pull-left grey-cancel-btn" ng-click="confirm(3)">Cancel</button>' +
                    '</div>',
                plain: true,
                closeByDocument: false,
                scope: newScope,
                className: 'ngdialog-theme-default'
            }).then(
                function(value) {
                    if (value == 1) {
                        $scope.updateCarrierRate();
                    } else if (value == 2) {
                        var params = {};
                        params.submitAction = 'Cancelled';
                        $state.go("layout.carrierRate", params);
                    } else {
                        console.log("cancelled");
                    }
                });
        } else {
            var params = {};
            params.submitAction = 'Cancelled';
            $state.go("layout.carrierRate", params);
        }


    }

    $scope.cancel = function() {
        if ($scope.oldData != JSON.stringify($scope.carrierRateDataList)) {
            var newScope = $scope.$new();
            newScope.errorMessage = $rootScope.nls["ERR200"];
            ngDialog.openConfirm({
                template: '<p>{{errorMessage}}</p>' +
                    '<div class="ngdialog-footer">' +
                    ' <button type="button" class="btn btn-default btn-property cancel-btn" ng-click="confirm(2)">No</button>' +
                    '<button type="button" class="btn btn-default btn-property accent-btn " ng-click="confirm(1)">Yes</button>' +
                    '<button type="button" class="btn btn-default btn-property pull-left grey-cancel-btn" ng-click="confirm(3)">Cancel</button>' +
                    '</div>',
                plain: true,
                closeByDocument: false,
                scope: newScope,
                className: 'ngdialog-theme-default'
            }).then(
                function(value) {
                    if (value == 1) {
                        $scope.saveCarrierRate();
                    } else if (value == 2) {
                        var params = {};
                        params.submitAction = 'Cancelled';
                        $state.go("layout.carrierRate", params);
                    } else {
                        console.log("cancelled");
                    }
                });
        } else {
            var params = {};
            params.submitAction = 'Cancelled';
            $state.go("layout.carrierRate", params);
        }
    };




    $scope.selectDestination = function(nextId) {
        if ($scope.validateCarrierRate(6)) {
            $scope.navigateToNextField(nextId);
        }
    }
    $scope.selectAddDestination = function(nextId) {
        $scope.navigateToNextField(nextId);
    }

    $scope.selectOrigin = function(nextId) {
        if ($scope.validateCarrierRate(5)) {
            $scope.navigateToNextField(nextId);
        }
    }
    $scope.selectAddOrigin = function(nextId) {
        $scope.navigateToNextField(nextId);
    }
    $scope.selectActualCharge = function(nextId) {
        $scope.navigateToNextField(nextId);
    }
    $scope.selectAddActualCharge = function(nextId) {
        $scope.navigateToNextField(nextId);
    }

    $scope.selectUnitCode = function(nextId) {
        if ($scope.validateCarrierRate(1)) {
            $scope.navigateToNextField(nextId);
        }
    }
    $scope.selectAddUnit = function(nextId) {
        $scope.navigateToNextField(nextId);
    }

    $scope.selectChargeCode = function(nextId) {
        if ($scope.validateCarrierRate(1)) {
            $scope.navigateToNextField(nextId);
        }
    }
    $scope.selectAddCharge = function(nextId, data) {
        $scope.setDefaultUnit(data);
        $scope.navigateToNextField(nextId);
    }
    $scope.selectCarrierCode = function(nextId) {
        if ($scope.validateCarrierRate(1)) {
            $scope.navigateToNextField(nextId);
        }
    }
    $scope.selectAddCarrier = function(nextId) {
        $scope.navigateToNextField(nextId);
    }
    $scope.selectFirstFromDate = function(nextId) {
        if ($scope.validateCarrierRate(2)) {
            $scope.navigateToNextField(nextId);
        }
    }

    $scope.selectSecondFromDate = function(nextId) {
        if ($scope.validateCarrierRate(3)) {
            $scope.navigateToNextField(nextId);
        }
    }

    $scope.edit = function() {
        console.log("Edit CarrierRateMaster Called");
        $state.go("layout.carrierRateEdit", {
            id: $scope.carrierRateMaster.id
        });
    };

    $scope.setDefaultUnit = function(data) {
        if (data.chargeMaster.calculationType != undefined && data.chargeMaster.calculationType != null) {
            if (data.chargeMaster.calculationType == 'Percentage') {
                UnitMasterGetByCode.get({
                    unitCode: 'PER'
                }).$promise.then(function(resObj) {
                    if (resObj.responseCode == "ERR0") {
                        data.unitMaster = resObj.responseObject;
                    }
                }, function(error) {
                    console.log("unit Get Failed : " + error)
                });

            }
            if (data.chargeMaster.calculationType == 'Shipment') {
                UnitMasterGetByCode.get({
                    unitCode: 'SHPT'
                }).$promise.then(function(resObj) {
                    if (resObj.responseCode == "ERR0") {
                        data.unitMaster = resObj.responseObject;
                    }
                }, function(error) {
                    console.log("unit Get Failed : " + error)
                });

            }
            if (data.chargeMaster.calculationType == 'Unit') {
                UnitMasterGetByCode.get({
                    unitCode: 'KG'
                }).$promise.then(function(resObj) {
                    if (resObj.responseCode == "ERR0") {
                        data.unitMaster = resObj.responseObject;
                    }
                }, function(error) {
                    console.log("unit Get Failed : " + error)
                });

            }
            if (data.chargeMaster.calculationType == 'Document') {
                UnitMasterGetByCode.get({
                    unitCode: 'DOC'
                }).$promise.then(function(resObj) {
                    if (resObj.responseCode == "ERR0") {
                        data.unitMaster = resObj.responseObject;
                    }
                }, function(error) {
                    console.log("unit Get Failed : " + error)
                });
            }
        }
    }


    //Bulk upload
    var myTotalOtherModal = null;
    $scope.bulkuploadmodel = function() {
        if ($rootScope.roleAccess(roleConstant.MASTER_EAIR_CARRIER_RATE_BULK_UPLOAD_CREATE)) {
            console.log("BulkUpload Button is pressed....!");
            $scope.errorMap = new Map();
            var newScope = $scope.$new();
            $scope.uploadText = true;
            $scope.data = {};
            myTotalOtherModal = $modal({
                scope: newScope,
                templateUrl: '/app/components/master/carrier_rate/view/bulkupload.html',
                show: false,
                backdrop: 'static'
            });

            myTotalOtherModal.$promise.then(myTotalOtherModal.show);

        }
    };

    $scope.convertToByteArray = function(file) {

        if (file != null || file != undefined) {

            var reader = new FileReader();

            reader.onload = function(event) {

                var contents = event.target.result;

                var uploadedFile = btoa(contents);

                console.log("File contents: " + uploadedFile);

                $scope.data.file = uploadedFile;
            };

            reader.readAsBinaryString(file);
        }
    }
    $scope.uploadFile = function() {
        if ($scope.validateUploadFile($scope.data.file)) {
            $scope.spinner = true;
            $scope.data.fileName = $scope.data.file.name;

            var reader = new FileReader();

            reader.onload = function(event) {
                var contents = event.target.result;
                var uploadedFile = btoa(contents);
                $scope.file = uploadedFile;
            };
            reader.readAsBinaryString($scope.data.file);


            $timeout(function() {

                $scope.fileUploadDto = {};

                $scope.fileUploadDto.fileName = $scope.data.file.name;
                $scope.fileUploadDto.fileType = $scope.data.file.type;
                $scope.fileUploadDto.file = $scope.file;

                return CarrierRateBulkFileUpload.upload($scope.fileUploadDto).$promise.then(function(data, status) {
                        if (data.responseCode == "ERR0") {
                            console.log("upload successfully : ", $scope.fileUploadDto.fileName);
                            console.log("JSON : ", data.responseObject);
                            myTotalOtherModal.$promise.then(myTotalOtherModal.hide);
                            $scope.spinner = false;
                            $scope.carrierRateDataList = data.responseObject;

                        } else {
                            console.log('Error while uploading  Carrier', data.responseObject);
                            myTotalOtherModal.$promise.then(myTotalOtherModal.hide);
                            $scope.spinner = false;
                        }
                    },
                    function(errResponse) {
                        console.log('Error while uploading  Carrier', errResponse);
                        myTotalOtherModal.$promise.then(myTotalOtherModal.hide);
                        $scope.spinner = false;
                    });

            }, 5000);
        } else {
            console.log('invalid file format');
        }
    }

    $scope.enableUploadFn = function() {
        $timeout(function() {
            $scope.disableUploadBtn = false;
        }, 1000);
    }

    $scope.chooseFile = function() {
        $scope.disableUploadBtn = true;
        if ($scope.validateUploadFile($scope.data.file)) {
            $scope.uploadText = false;
            $scope.isBusy = true;
            $scope.data.fileName = $scope.data.file.name;
            $timeout(function() {
                $scope.isBusy = false;
                $scope.enableUploadFn();
            }, 5000);
        } else {
            $scope.enableUploadFn();
            console.log("Not valid file format", $scope.data.file.type);
        }

    };



    $scope.validateUploadFile = function(file) {
        console.log("validateUploadFile is running");
        $scope.errorMap = new Map();
        if (file == null && file == undefined) {
            $scope.errorMap.put('fileUpload', $rootScope.nls["ERR1297"]);
            return false;
        } else {
            if (file.type == "text/csv" || file.name.includes('.csv')) {
                console.log('selected file type is :', file.type);

                var allowedSize = $rootScope.appMasterData['file.size.carrier.rate.bulk.upload'];

                if (allowedSize != undefined) {

                    console.log("Allowed File Size : " + allowedSize + " MB");

                    console.log("Uploaded File Size : ", file.size);

                    allowedSize = allowedSize * 1024 * 1024;

                    if (file.size <= 0) {

                        console.log("Empty file uploaded...")

                        $scope.errorMap.put("fileUpload", $rootScope.nls["ERR1299"]);

                        return false;
                    } else if (file.size > allowedSize) {

                        console.log("Selected File is Larger than the allowed size")

                        $scope.errorMap.put("fileUpload", $rootScope.nls["ERR1300"]);

                        return false;
                    }

                }
            } else {
                $scope.errorMap.put('fileUpload', $rootScope.nls["ERR1298"]);
                return false;
            }
        }

        return true;
    }




    //History start here...........

    $scope.getCarrierRateMastrById = function(id) {
        console.log("getCarrierRateMasterById ", id);

        CarrierRateView.get({
            id: id
        }, function(data) {
            if (data.responseCode == "ERR0") {
                $scope.carrierRateMaster = data.responseObject;
                if ($scope.carrierRateMaster.actualChargeable == "CHARGEABLE") {
                    $scope.carrierRateMaster.actualChargeable = false;
                } else {
                    $scope.carrierRateMaster.actualChargeable = true;
                }
                if ($scope.carrierRateMaster.firstFromDate != undefined) {
                    $scope.carrierRateMaster.firstFromDate = $rootScope.dateToString($scope.carrierRateMaster.firstFromDate);
                }
                if ($scope.carrierRateMaster.secondFromDate != undefined) {
                    $scope.carrierRateMaster.secondFromDate = $rootScope.dateToString($scope.carrierRateMaster.secondFromDate);
                }
                $scope.setOldDataVal(angular.copy($scope.carrierRateMaster), true);

            }

        }, function(error) {
            console.log("Error while getting CarrierRateMaster.", error)
        });

    }


    $scope.$on('carrierRateAddEvent', function(events, args) {
        $rootScope.unfinishedData = $scope.carrierRateDataList;
        $rootScope.category = "Carrier Rate Master";
        $rootScope.unfinishedFormTitle = "Carrier Rate Master (New)";
        if ($scope.carrierRateDataList != undefined &&
            $scope.carrierRateDataList != null &&
            $scope.carrierRateDataList.length != 0) {

            if ($scope.carrierRateDataList[0].carrierMaster != undefined &&
                $scope.carrierRateDataList[0].carrierMaster != null &&
                $scope.carrierRateDataList[0].carrierMaster.id != undefined &&
                $scope.carrierRateDataList[0].carrierMaster.id != null) {
                $rootScope.subTitle = $scope.carrierRateDataList[0].carrierMaster.carrierName;
            } else {
                $rootScope.subTitle = "Unknown Carrier "
            }
        } else {
            $rootScope.subTitle = "Unknown Carrier Rate Master"
        }
    });


    $scope.$on('carrierRateEditEvent', function(events, args) {
        $rootScope.unfinishedData = $scope.carrierRateMaster;
        $rootScope.category = "Carrier Rate Master";
        $rootScope.unfinishedFormTitle = "Carrier Rate Master Edit";
        if ($scope.carrierRateMaster != undefined &&
            $scope.carrierRateMaster != null) {

            if ($scope.carrierRateMaster.carrierMaster != undefined &&
                $scope.carrierRateMaster.carrierMaster != null &&
                $scope.carrierRateMaster.carrierMaster.id != undefined &&
                $scope.carrierRateMaster.carrierMaster.id != null) {
                $rootScope.subTitle = $scope.carrierRateMaster.carrierMaster.carrierName;
            } else {
                $rootScope.subTitle = "Unknown Carrier "
            }
        } else {
            $rootScope.subTitle = "Unknown Carrier Rate Master"
        }
    });


    $scope.$on('carrierRateAddEventReload', function(e, confirmation) {
        confirmation.message = "";
        localStorage.reloadFormData = JSON.stringify($scope.carrierRateDataList);
        localStorage.isCarrierRateMasterReloaded = "YES";
    });

    $scope.$on('carrierRateEditEventReload', function(e, confirmation) {
        confirmation.message = "";
        localStorage.reloadFormData = JSON.stringify($scope.carrierRateMaster);
        localStorage.isCarrierRateMasterReloaded = "YES";
    });



    $scope.isReloaded = localStorage.isCarrierRateMasterReloaded;

    if ($stateParams.action == "ADD") {

        if ($stateParams.fromHistory === 'Yes') {

            if ($scope.isReloaded == "YES") {


                console.log("Add History Reload...")
                $scope.isReloaded = "NO";
                localStorage.isCarrierRateMasterReloaded = "NO";
                $scope.carrierRateDataList = JSON.parse(localStorage.reloadFormData);

            } else {

                console.log("Add History NotReload...")
                $scope.carrierRateDataList = $rootScope.selectedUnfilledFormData;

            }

        } else {

            if ($scope.isReloaded == "YES") {

                $scope.isReloaded = "NO";

                localStorage.isCarrierRateMasterReloaded = "NO";

                $scope.carrierRateDataList = JSON.parse(localStorage.reloadFormData);

                if ($scope.carrierRateDataList == undefined || $scope.carrierRateDataList == null || $scope.carrierRateDataList.length == 0) {
                    $scope.carrierRateDataList = [{}];
                }

            } else {

                $scope.carrierRateDataList = [{}];
                $scope.setOldDataVal($scope.carrierRateDataList, false);

            }

        }

        $rootScope.breadcrumbArr = [{
                label: "Master",
                state: "layout.carrierRate"
            },
            {
                label: "Carrier Rate",
                state: "layout.carrierRate"
            },
            {
                label: "Add Carrier Rate",
                state: null
            }
        ];
    } else {


        if ($stateParams.fromHistory === 'Yes') {

            if ($scope.isReloaded == "YES") {

                $scope.isReloaded = "NO";

                localStorage.isCarrierRateMasterReloaded = "NO";

                $scope.carrierRateMaster = JSON.parse(localStorage.reloadFormData);

            } else {
                $scope.carrierRateMaster = $rootScope.selectedUnfilledFormData;
            }
        } else {

            if ($scope.isReloaded == "YES") {

                $scope.isReloaded = "NO";

                sessionStorage.isCarrierRateMasterReloaded = "NO";

                $scope.carrierRateMaster = JSON.parse(localStorage.reloadFormData);

            } else {

                $scope.getCarrierRateMastrById($stateParams.id);

            }
        }
        $rootScope.breadcrumbArr = [{
                label: "Master",
                state: "layout.carrierRate"
            },
            {
                label: "Carrier Rate",
                state: "layout.carrierRate"
            },
            {
                label: "Edit Carrier Rate",
                state: null
            }
        ];
    }

    //History end

}]);