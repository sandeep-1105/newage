app.controller('CarrierRateController',['$rootScope', '$scope', '$state', '$window', '$stateParams', 'RecentHistorySaveService',
    'ngTableParams', 'ngDialog', 'CarrierRateSearch', 'CarrierRateView', 'roleConstant',
    function($rootScope, $scope, $state, $window, $stateParams, RecentHistorySaveService,
    ngTableParams, ngDialog, CarrierRateSearch, CarrierRateView, roleConstant) {

    $scope.directiveDatePickerOpts = angular.copy($rootScope.datePickerOptions);
    $scope.directiveDatePickerOpts.widgetParent = "body";

    /************************************************************
     * ux - by Muthu
     * reason - ipad compatibility fix
     *
     * *********************************************************/
    $scope.deskTopView = true;


    $scope.limitArr = [10, 15, 20];
    $scope.page = 0;
    $scope.limit = 10;

    /********************* ux fix ends here *****************************/

    $scope.carrierRateHeadArr = [{
            "name": "#",
            "width": "col-xs-0half",
            "prefWidth": "50",
            "model": "no",
            "search": false,
        },
        {
            "name": "Carrier",
            "width": "w150px",
            "prefWidth": "150",
            "model": "carrierMaster.carrierName",
            "search": true,
            "wrap_cell": true,
            "type": "text",
            "sort": true,
            "key": "searchCarrierName"
        },
        {
            "name": "Charge",
            "width": "w100px",
            "prefWidth": "100",
            "model": "chargeMaster.chargeName",
            "search": true,
            "type": "text",
            "sort": true,
            "key": "searchChargeName"
        },
        {
            "name": "Unit",
            "width": "w100px",
            "prefWidth": "100",
            "model": "unitMaster.unitName",
            "search": true,
            "type": "text",
            "sort": true,
            "key": "searchUnitName"
        },
        {
            "name": "Chargeable Weight / Actual",
            "width": "w150px ",
            "prefWidth": "100",
            "model": "actualChargeable",
            "search": true,
            "sort": true,
            "type": "drop",
            "key": "searchActualChargeble",
            "data": actualcharge()
        },
        {
            "name": "Origin",
            "width": "w100px ",
            "prefWidth": "100",
            "model": "origin.portName",
            "search": true,
            "sort": true,
            "type": "text",
            "key": "searchorigin"
        },
        {
            "name": "Destination",
            "width": "w100px",
            "prefWidth": "w100px",
            "model": "destination.portName",
            "search": true,
            "sort": true,
            "type": "text",
            "key": "searchDestination"
        },
        {
            "name": "First Effective Date",
            "width": "w100px input125res",
            "prefWidth": "100",
            "model": "firstFromDate",
            "search": true,
            "sort": true,
            "type": "date-range",
            "key": "searchFirstFromDate"
        },
        {
            "name": "Amount",
            "width": "w75px",
            "prefWidth": "75",
            "model": "firstAmount",
            "search": true,
            "sort": true,
            "type": "number",
            "key": "searchFirstAmount"
        },
        {
            "name": "Min Amount",
            "width": "w75px",
            "prefWidth": "75",
            "model": "firstMinAmount",
            "search": true,
            "sort": true,
            "type": "number",
            "key": "searchFirstMinAmount"
        },
        {
            "name": "Second Effective Date",
            "width": "w100px",
            "prefWidth": "150",
            "model": "secondFromDate",
            "search": true,
            "sort": true,
            "type": "date-range",
            "key": "searchSecondFromDate"
        },
        {
            "name": "Amount",
            "width": "w75px",
            "prefWidth": "75",
            "model": "secondAmount",
            "search": true,
            "sort": true,
            "type": "number",
            "key": "searchSecondAmount"
        }, {
            "name": "Min Amount",
            "width": "w75px",
            "prefWidth": "75",
            "model": "secondMinAmount",
            "search": true,
            "sort": true,
            "type": "number",
            "key": "searchSecondMinAmount"
        }

    ]

    $scope.init = function() {
        console.log("Carrier Rate Master Init method called.");
        $rootScope.setNavigate1("Master");
        $rootScope.setNavigate2("Carrier");
        $scope.searchDto = {};
        $scope.search();
        $scope.searchDto.orderByType = "ASC";
    };

    function actualcharge() {
        var actualchargeDat;
        actualchargeDat = $rootScope.enum['ActualChargeable'];
        actualchargeDat[1] = "Actual";
        actualchargeDat[0] = "Chargeable Weight";
        return actualchargeDat;
    }
    $scope.changePage = function(param) {
        console.log("called change page")
        $scope.page = param.page;
        $scope.limit = param.size;
        $scope.search();
    }

    $scope.currentTab = "address";
    $scope.activeTab = function(tab) {
        $scope.currentTab = tab;
    }

    $scope.add = function() {
        if ($rootScope.roleAccess(roleConstant.MASTER_EAIR_CARRIER_RATE_CREATE)) {
            $state.go('layout.carrierRateAdd');
        }
    };
    $scope.editCarrierRate = function() {
        if ($rootScope.roleAccess(roleConstant.MASTER_EAIR_CARRIER_RATE_MODIFY)) {
            console.log("Edit editCarrierRate Button is Pressed....")
            var param = {
                id: $scope.carrierRateMaster.id
            };
            console.log("State Parameters :: ", param);
            $state.go("layout.carrierRateEdit", param);
        }
    }

    $scope.limitChange = function(item) {
        $scope.page = 0;
        $scope.limit = item;
        $scope.cancel();
        $scope.search();
    }

    $scope.sortSelection = {
        sortKey: "carrierMaster.carrierName",
        sortOrder: "asc"
    }

    $scope.sortChange = function(param) {
        $scope.searchDto.orderByType = param.sortOrder.toUpperCase();
        $scope.searchDto.sortByColumn = param.sortKey;
        $scope.search();
    };

    $scope.changeSearch = function(param) {

        console.log("Change Search is called ", param);

        for (var attrname in param) {
            $scope.searchDto[attrname] = param[attrname];
        }

        $scope.page = 0;
        $scope.search();
    }

    $scope.rowSelect = function(data, ind) {
        if ($rootScope.roleAccess(roleConstant.MASTER_EAIR_CARRIER_RATE_VIEW)) {
            var param = {
                id: data.id,
                index: ind
            };
            $state.go("layout.viewCarrierRate", param);

            /************************************************************
             * ux - by Muthu
             * reason - ipad compatibility fix
             *
             * *********************************************************/

            var windowInner = $window.innerWidth;
            if (windowInner <= 1199) {
                $scope.deskTopView = false;
            } else {
                $scope.deskTopView = true;
            }
        }
    }

    $scope.search = function() {
        $scope.carrierRateMaster = {};
        $scope.searchDto.selectedPageNumber = $scope.page;
        $scope.searchDto.recordPerPage = $scope.limit;
        if ($scope.searchDto.searchActualChargeble == "Chargeable Weight") {
            $scope.searchDto.searchActualChargeble = "Chargeable";
        }
        $scope.carrierRateArr = [];
        CarrierRateSearch.query($scope.searchDto).$promise.then(function(
            data, status) {

            $scope.totalRecord = data.responseObject.totalRecord;
            console.log($scope.totalRecord);
            var tempArr = [];
            var resultArr = [];
            tempArr = data.responseObject.searchResult;
            console.log("tempArr", tempArr);
            var tempObj = {};
            angular.forEach(tempArr, function(item, index) {
                tempObj = item;
                if (tempObj != undefined && tempObj.actualChargeable == "ACTUAL") {
                    tempObj.actualChargeable = "Actual"
                } else {
                    tempObj.actualChargeable = "Chargeable Weight"
                }

                tempObj.firstAmount = $rootScope.currencyFormat(tempObj.origin.portGroupMaster.countryMaster.currencyMaster, tempObj.firstAmount);
                tempObj.firstMinAmount = $rootScope.currencyFormat(tempObj.origin.portGroupMaster.countryMaster.currencyMaster, tempObj.firstMinAmount);
                tempObj.secondAmount = $rootScope.currencyFormat(tempObj.origin.portGroupMaster.countryMaster.currencyMaster, tempObj.secondAmount);
                tempObj.secondMinAmount = $rootScope.currencyFormat(tempObj.origin.portGroupMaster.countryMaster.currencyMaster, tempObj.secondMinAmount);


                tempObj.no = (index + 1) + ($scope.page * $scope.limit);
                resultArr.push(tempObj);
                tempObj = {};
            });
            $scope.carrierRateArr = resultArr;
            console.log('carrierRateArr', $scope.carrierRateArr);
        });
    }

    $scope.cancel = function() {
        $scope.detailPageFlag = false;
        $scope.showHistory = false;
        $state.go('layout.carrierRate');

        /************************************************************
         * ux - by Muthu
         * reason - ipad compatibility fix
         *
         * *********************************************************/
        $scope.deskTopView = true;

        /********************* ux fix ends here *****************************/
    };

    $scope.view = function(carrierRateId) {
        console.log("View Carrier Rate Master is called ", carrierRateId);
        CarrierRateView.get({
            id: carrierRateId
        }, function(data) {
            if (data.responseCode == "ERR0") {
                console.log("Carrier Rate Successful");
                $scope.carrierRateMaster = data.responseObject;
                if ($scope.carrierRateMaster != undefined && $scope.carrierRateMaster != null && $scope.carrierRateMaster.carrierMaster != undefined && $scope.carrierRateMaster.carrierMaster.carrierName != null) {
                    $rootScope.subTitle = $scope.carrierRateMaster.carrierMaster.carrierName;
                }
                // To display the view page
                $scope.detailPageFlag = true;
                var rHistoryObj = {
                    'title': 'Carrier Rate Master View #' + $stateParams.id,
                    'subTitle': $rootScope.subTitle,
                    'stateName': $state.current.name,
                    'stateParam': JSON.stringify($stateParams),
                    'stateCategory': 'Carrier Rate Master',
                    'serviceType': 'AIR'
                }
                RecentHistorySaveService.form(rHistoryObj);
                console.log("Viewing Carrier Rate Master ", $scope.carrierRateMaster);
            } else {
                console.log("Carrier Rate Master View Failed ", data.responseDescription)
            }
        }, function(error) {
            console.log("Carrier Rate Master View Failed : ", error);
        });
    }

    switch ($stateParams.action) {
        case "VIEW":
            console.log("CarrierRateMaster View Page", $stateParams.id, $stateParams.index);
            $rootScope.unfinishedFormTitle = "Carrier Rate Master # " + $stateParams.id;
            $rootScope.unfinishedData = undefined;
            $scope.view($stateParams.id);
            $rootScope.breadcrumbArr = [{
                    label: "Master",
                    state: "layout.carrierRate"
                },
                {
                    label: "Carrier Rate",
                    state: "layout.carrierRate"
                },
                {
                    label: "View Carrier Rate",
                    state: null
                }
            ];
            break;
        case "SEARCH":
            console.log("CarrierRateMaster Search Page");
            $scope.init();
            $rootScope.breadcrumbArr = [{
                    label: "Master",
                    state: "layout.carrierRate"
                },
                {
                    label: "Carrier Rate",
                    state: "layout.carrierRate"
                }
            ];
            break;
        default:
    }
}]);