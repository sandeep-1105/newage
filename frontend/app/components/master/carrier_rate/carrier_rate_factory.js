(function() {

		app.factory("CarrierRateSearch",['$resource', function($resource) {
			return $resource("/api/v1/carrierratemaster/get/search/keyword", {}, {
				query : {
					method : 'POST'
				}
			});
		}]);
		
		app.factory("CarrierRateView",['$resource', function($resource) {
			return $resource("/api/v1/carrierratemaster/get/id/:id", {}, {
				get : {
					method : 'GET',
					params : {
						id : ''
					},
					isArray : false
				}
			});
		}]);
		
		app.factory("CarrierRateSave",['$resource', function($resource) {
			return $resource("/api/v1/carrierratemaster/create", {}, {
				save : {
					method : 'POST'
				}
			});
		}]);
		
		
		app.factory("CarrierRateEdit",['$resource', function($resource) {
			return $resource("/api/v1/carrierratemaster/update", {}, {
			update : {
					method : 'POST'
				}
			});
		}]);

		app.factory("CarrierRateBulkFileUpload",['$resource', function($resource) {
			return $resource("/api/v1/carrierratemaster/upload", {}, {
				upload : {
					method : 'POST'
				}
			});
		}]);
		
		
		
})();
