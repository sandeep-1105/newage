app.service('cfsMasterValidationService', ['$rootScope','ValidateUtil', 'CommonValidationService', 'appConstant', function($rootScope, ValidateUtil, CommonValidationService, appConstant) {
	this.validate = function(cfs, code) {
		//Validate cfs name
		if(code == 0 || code == 1) {
			if(cfs.cfsName == undefined || cfs.cfsName == null || cfs.cfsName =="" ){
				return this.validationResponse(true, "cfsName", $rootScope.nls["ERR37088"], cfs);
			}
		}
		//Validate cfs code
		if(code == 0 || code == 2) {
			if(cfs.cfsCode == undefined || cfs.cfsCode == null || cfs.cfsCode =="" ){
				return this.validationResponse(true, "cfsCode", $rootScope.nls["ERR37077"], cfs);
			}
		}
		
		
		 //Validate contact persion
		
		if(cfs.addressMaster != undefined && cfs.addressMaster != null) {
			 if (code == 0 || code == 4) {
		            if (cfs.addressMaster.contact != undefined &&
		            		cfs.addressMaster.contact != null && cfs.addressMaster.contact != "") {
		            	
		            	if(!CommonValidationService.checkRegExp(appConstant.Reg_Exp_Master_PARTY_CONTACT_PERSON,cfs.addressMaster.contact)){
		                    return this.validationResponse(true, "contact", $rootScope.nls["ERR37089"], cfs);
		    			}
		            }
		        }
			 
			 //Validate phone
			 if (code == 0 || code == 5) {
		            if (cfs.addressMaster.phone != undefined &&
		            		cfs.addressMaster.phone != null && cfs.addressMaster.phone != "") {
		            	
		            	if(!CommonValidationService.checkRegExp(appConstant.REG_EXP_PHONE_NUMBER,cfs.addressMaster.phone)){
		                    return this.validationResponse(true, "phone", $rootScope.nls["ERR37090"], cfs);
		    			}
		            }
		        }
			
			//Validate mobile
			 if (code == 0 || code == 6) {
		            if (cfs.addressMaster.mobile != undefined &&
		            		cfs.addressMaster.mobile != null && cfs.addressMaster.mobile != "") {
		            	
		            	if(!CommonValidationService.checkRegExp(appConstant.REG_EXP_MOBILE_NUMBER,cfs.addressMaster.mobile)){
		                    return this.validationResponse(true, "mobileNo",$rootScope.nls["ERR37091"], cfs);
		    			}
		            }
		        }
			//Validate email
			 if (code == 0 || code == 7) {
		            if (cfs.addressMaster.email != undefined &&
		            		cfs.addressMaster.email != null && cfs.addressMaster.email != "") {
		            	
		            	if(!CommonValidationService.checkMultipleMail(cfs.addressMaster.email)){
		                    return this.validationResponse(true, "email", $rootScope.nls["ERR37092"], cfs);
		    			}
		            }
		        }
		}
		
		
		
     
		 
		//Validate port master
			if(code == 0 || code == 8) {
				if(cfs.portMaster == null || cfs.portMaster == undefined || cfs.portMaster== "" ){
					//return this.validationResponse(true, "portName", $rootScope.nls["ERR37111"], cfs);	
				}
				else{
					if(cfs.portMaster.status=="Block"){
						return this.validationResponse(true, "portName", $rootScope.nls["ERR37122"], cfs);	
					}
					else if(cfs.portMaster.status=="Hide"){
						return this.validationResponse(true, "portName", $rootScope.nls["ERR37133"], cfs);	
					}
				}
			}
		
		
		return this.validationSuccesResponse(cfs);
	}
 
	this.validationSuccesResponse = function(cfs) {
	    return {error : false, obj : cfs};
	}
	
	this.validationResponse = function(err, elem, message, cfs) {
		return {error : err, errElement : elem, errMessage : message, obj : cfs};
	}
	
}]);