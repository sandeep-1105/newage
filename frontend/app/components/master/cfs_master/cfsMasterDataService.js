app.service('cfsMasterDataService', function($rootScope) {
    this.getHeadArray = function() {
	   return [
			  {
					"name":"#",
					"width":"col-xs-0half",
					"model":"no",
					"search":false
			  },{
					"name":"Name",
					"width":"col-xs-7",
					"model":"cfsName",
					"search":true,
					"wrap_cell":true,
					"type":"text",
					"sort":true,
					"key":"searchName"
				},{
					"name":"Code",
					"width":"col-xs-2half",
					"model":"cfsCode",
					"search":true,
					"type":"text",
					"sort":true,
					"key":"searchCode"
				},{
					"name":"Status",
					"width":"w120px",
					"model":"status",
					"search":true,
					"type":"drop",
					"data":$rootScope.enum['LovStatus'],
					"key":"searchStatus",
					"sort":true
				}];
          };

    
    this.getListBreadCrumb = function() {
        return [{
                label: "Master",
                state: "layout.cfsMaster"
            },
            {
                label: "CFS",
                state: "layout.cfsMaster"
            }
        ];
    }

    this.getAddBreadCrumb = function() {
        return [{
                label: "Master",
                state: "layout.cfsMaster"
            },
            {
                label: "CFS",
                state: "layout.cfsMaster"
            },
            {
                label: "Add CFS",
                state: null
            }
        ];
    };
    this.getEditBreadCrumb = function() {
        return [{
                label: "Master",
                state: "layout.cfsMaster"
            },
            {
                label: "CFS",
                state: "layout.cfsMaster"
            },
            {
                label: "Edit CFS",
                state: null
            }
        ];
    };
   

});