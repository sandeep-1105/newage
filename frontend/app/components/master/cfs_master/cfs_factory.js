(function() {

	app.factory("CFSSearch",['$resource', function($resource) {
		return $resource("/api/v1/cfsmaster/get/search", {}, {
			query : {
				method : 'POST'
			}
		});
	}]);
	
	app.factory("CFSSearchKeyword",['$resource', function($resource) {
		return $resource("/api/v1/cfsmaster/get/search/keyword", {}, {
			fetch : {
				method : 'POST'
			}
		});
	}]);


})();
