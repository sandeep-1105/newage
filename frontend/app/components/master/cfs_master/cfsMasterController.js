(function(){
app.controller("CFSMasterCtrl", ['$scope', '$rootScope', '$window', '$modal', '$state', '$stateParams', '$log', 'ngDialog', 'cfsMasterDataService', 'cfsMasterFactory', 'RecentHistorySaveService', 'cfsMasterValidationService', 'Notification','PortByTransportMode','AutoCompleteService','roleConstant',
    function($scope, $rootScope, $window, $modal, $state, $stateParams, $log, ngDialog, cfsMasterDataService, cfsMasterFactory, RecentHistorySaveService, cfsMasterValidationService, Notification,PortByTransportMode,AutoCompleteService, roleConstant ) {
	    $scope.$AutoCompleteService = AutoCompleteService;
        var vm = this;
        vm.tableHeadArr = cfsMasterDataService.getHeadArray();
        vm.dataArr = [];
        vm.searchData = {};
        vm.cfsMaster = {};
        vm.cfsMaster.status='Active';
        vm.limitArr = [10, 15, 20];
        vm.page = 0;
        vm.limit = 10;
        vm.totalRecord = 100;
        vm.showHistory = false;
        vm.errorMap = new Map();
        vm.deskTopView = true;

        vm.sortSelection = {
            sortKey: "cfsName",
            sortOrder: "asc"
        }
        
        $scope.portRender = function(item){
			return {
				label:item.portName,
				item:item
			}
		}
        
        $scope.ajaxPortEvent = function(object) {
			$scope.searchDto = {};
			$scope.searchDto.keyword = object == null ? "" : object;
			$scope.searchDto.selectedPageNumber = 0;
			$scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
			
			return PortByTransportMode.fetch({
				"transportMode" : 'Air'
			}, $scope.searchDto).$promise.then(function(data) {
				if (data.responseCode == "ERR0") {
					$scope.portList = [];
					var resultList = data.responseObject.searchResult;
					if (resultList != null && resultList.length != 0) {
						for (var i = 0; i < resultList.length; i++) {
							$scope.portList.push(resultList[i]);
						}
					}
					return $scope.portList;
				}
			},
			function(errResponse) {
			console.error('Error while fetching Port Master List');
			});
		}
        vm.changeSearch = function(param) {
            $log.debug("Change Search is called." + param);
            for (var attrname in param) {
                vm.searchData[attrname] = param[attrname];
            }
            vm.page = 0;
            vm.search();
        }

        vm.sortChange = function(param) {
            $log.debug("sortChange is called. " + param);
            vm.searchData.orderByType = param.sortOrder.toUpperCase();
            vm.searchData.sortByColumn = param.sortKey;
            vm.search();
        }
        
        vm.setStatus = function(data){
    		if(data=='Active'){
    			return 'activetype';
    		} else if(data=='Block'){
    			return 'blockedtype';
    		}else if(data=='Hide'){
    			return 'hiddentype';
    		}
    	}
        
        /**
         * CFS Master List Navigate to selected page.
         * @param {Object} param - The Selected page option.
         */
        vm.changepage = function(param) {
            $log.debug("change Page..." + param.page);
            vm.page = param.page;
            vm.limit = param.size;
            vm.search();
        }

        /**
         * CFS Master List Page Limit Data change.
         * @param {int} nLimit - The Selected limit option from Limit array.
         */
        vm.limitChange = function(nLimit) {
            $log.debug("Limit Change..." + nLimit);
            vm.page = 0;
            vm.limit = nLimit;
            vm.search();
        }

        /**
         * CFS Master List Page Search Data  initializing and calling search Mtd.
         * @param {Object} data - The Selected CFS Master from List page.
         * @param {string} data.id - The id of the Selected CFS Master.
         * @param {int} index - The Selected row index from List page.
         */
        vm.rowSelect = function(data) {
        	 if($rootScope.roleAccess(roleConstant.MASTER_OCEAN_CFS_VIEW)){
        		 vm.cfsMasteView=true;
     			vm.cfsMaster = data;
     			var windowInner=$window.innerWidth;
     			if(windowInner<=1199){
     				vm.deskTopView = false;
     			}
     			else{
     				vm.deskTopView = true;
     			}
     			angular.element($window).bind('resize', function(){
     				if($window.innerWidth>=1200){
     					vm.deskTopView = true;
     				}
     				else if($window.innerWidth<=1199){
     					if(vm.cfsMaster.id!=null){
     						vm.deskTopView = false;
     					}
     					else {
     						vm.deskTopView = true;
     					}
     				}
     				$scope.$digest();
     			}); 
        	 }
		}

        /**
         * CFS Master List Page Search Data  initializing and calling search Mtd.
         */
        vm.searchInit = function() {
            vm.searchData = {};
            vm.searchData.orderByType = vm.sortSelection.sortOrder.toUpperCase();
            vm.searchData.sortByColumn = vm.sortSelection.sortKey;
            vm.search();
        };
        /**
         * Go Back to CFS Master List Page.
         */
        vm.backToList = function() {
        	vm.cfsMasteView = false;
        	vm.cfsMaster = {};
        	vm.deskTopView = true;
        }

       
        /**
         * Get CFS Master's Based on Search Data.
         */
        vm.search = function() {
        	vm.cfsMasteView=false;
            vm.searchData.selectedPageNumber = vm.page;
            vm.searchData.recordPerPage = vm.limit;
            vm.dataArr = [];
            cfsMasterFactory.search.fetch(vm.searchData).$promise.then(
                function(data) {
                    if (data.responseCode == "ERR0") {
                        vm.totalRecord = data.responseObject.totalRecord;
                        vm.dataArr = data.responseObject.searchResult;
                    }
                },
                function(errResponse) {
                	 $log.error('Error while fetching CFS');
                });
           }

        /**
         * Get CFS Master By id.
         * @param {int} id - The id of a CFS.
         * @param {boolean} isView - TO identify purpose of calling is for View or Edit a CFS.
         */
        vm.view = function(id, isView) {
        	cfsMasterFactory.findById.query({
                id: id
            }).$promise.then(function(data) {
                if (data.responseCode == 'ERR0') {
                    vm.cfsMaster = data.responseObject;
                    var rHistoryObj = {}

                    if (vm.cfsMaster != undefined && vm.cfsMaster != null && vm.cfsMaster.id != undefined && vm.cfsMaster.id != null) {
                        $rootScope.subTitle = vm.cfsMaster.cfsName;
                    } else {
                        $rootScope.subTitle = "Unknown CFS"
                    }
                    $rootScope.unfinishedFormTitle = "CFS  # " + vm.cfsMaster.cfsCode;
                    if (isView) {
                        rHistoryObj = {
                            'title': "CFS #" + vm.cfsMaster.cfsCode,
                            'subTitle': $rootScope.subTitle,
                            'stateName': $state.current.name,
                            'stateParam': JSON.stringify($stateParams),
                            'stateCategory': 'CFS Master',
                            'serviceType': undefined,
                            'showService': false
                        }

                    } else {

                        rHistoryObj = {
                            'title': 'Edit CFS # ' + $stateParams.serviceId,
                            'subTitle': $rootScope.subTitle,
                            'stateName': $state.current.name,
                            'stateParam': JSON.stringify($stateParams),
                            'stateCategory': 'CFS Master',
                            'serviceType': 'MASTER',
                            'serviceCode': ' ',
                            'orginAndDestination': ' '
                        }
                    }
                    RecentHistorySaveService.form(rHistoryObj);
                } else {
                    $log.debug("Exception while getting CFS by Id ", data.responseDescription);
                }
            }, function(error) {
                $log.debug("Error ", error);
            });
        };

        /**
         * Go To Add CFS Master.
         */
        vm.addNew = function() {
        	 if($rootScope.roleAccess(roleConstant.MASTER_OCEAN_CFS_CREATE)){
        		   $state.go("layout.cfsMasterAdd"); 
        	 }
        }

        /**
         * Validating field of CFS Master while instant change.
         * @param {errorElement,errorCode}
         */
        vm.validate = function(valElement, code) {
            var validationResponse = cfsMasterValidationService.validate(vm.cfsMaster,code);
            if(validationResponse.error == true) {
				$scope.validationErrorAndFocus(validationResponse, validationResponse.errElement);
			} else {
				$scope.errorMap.put(valElement, null);
            	validationResponse.error = false;
            }
        }
        
        /**
         * Cancel Add or Edit CFS Master.
         * @param {int} id - The id of a CFS
         */
       
        vm.cancel = function(objId) {
            if(vm.cfsMasterForm!=undefined){
		    	vm.goTocancel(vm.cfsMasterForm.$dirty,objId);
            }else{
            	vm.goTocancel(false,objId);
            }
        }
       
       vm.goTocancel = function(isDirty,objId){
     		 if (isDirty) {
     		      var newScope = $scope.$new();
     		      newScope.errorMessage = $rootScope.nls["ERR200"];
     		      ngDialog.openConfirm({
     		        template: '<p>{{errorMessage}}</p>' +
     		          '<div class="ngdialog-footer">' +
     		          ' <button type="button" class="btn btn-default btn-property cancel-btn" ng-click="confirm(2)">No</button>' +
     		          '<button type="button" class="btn btn-default btn-property accent-btn " ng-click="confirm(1)">Yes</button>' +
     		          '<button type="button" class="btn btn-default btn-property pull-left grey-cancel-btn" ng-click="confirm(3)">Cancel</button>' +
     		          '</div>',
     		        plain: true,
     		        closeByDocument: false,
     		        scope: newScope,
     		        className: 'ngdialog-theme-default'
     		      }).then(
     		        function(value) {
     		          if (value == 1) {
     		            if(objId!=null && objId != undefined){
     		            	vm.update();
     		            }else{
     		            	vm.create();
     		            }
     		          } else if (value == 2) {
     		        	 $state.go("layout.cfsMaster", {
     		                submitAction: "Cancelled"
     		            });
     		          } else {
     		            $log.debug("cancelled");
     		          }
     		        });
     		    } else {
     		     $state.go("layout.cfsMaster", {
                   submitAction: "Cancelled"
               });
     		    }
     	    }

        /**
         * Modify Existing CFS Master.
         * @param {int} id - The id of a Country.
         */
        vm.edit = function(id) {
        	 if($rootScope.roleAccess(roleConstant.MASTER_OCEAN_CFS_MODIFY)){
        		 $state.go("layout.cfsMasterEdit", {
                     id: id
                 }); 
        	 }
        }

        /**
         * Delete Country Master.
         * @param {int} id - The id of a Country.
         */
        vm.delete = function(id) {
        	 if($rootScope.roleAccess(roleConstant.MASTER_OCEAN_CFS_DELETE)){
        		 var newScope = $scope.$new();
     			newScope.errorMessage = $rootScope.nls["ERR37134"];
     	    	ngDialog.openConfirm( {
     	                    template : '<p>{{errorMessage}}</p>'
     	                    + '<div class="ngdialog-footer">'
     	                    + '<button type="button" class="btn btn-default btn-property cancel-btn" ng-click="closeThisDialog(0)">Cancel'
     	                    + '<button type="button" class="btn btn-primary btn-property accent-btn" ng-click="confirm(1)">Yes'
     	                    + '</button></div>',
     	                    plain : true,
     	                    scope : newScope,
     	                    closeByDocument: false,
     	                    className : 'ngdialog-theme-default'
     	                }).then( function(value) {
     	                	cfsMasterFactory.delete.query({
     	                        id: id
     	                    }).$promise.then(function(data) {
     	                        if (data.responseCode == 'ERR0') {
     	                        	vm.cfsMasteView=false;
     	                        	Notification.success($rootScope.nls["ERR402"]);
     	                        	vm.deskTopView = true;
     	                            vm.search();
     	                        }
     	                    }, function(error) {
     	                        $log.debug("Error ", error);
     	                    });
     	                }, function(value) {
     	                    $log.debug("delete Cancelled");
     	            }); 
        	 }
        }

        /**
         * Create New CFS Master.
         */
        vm.create = function() {
            var validationResponse = cfsMasterValidationService.validate(vm.cfsMaster,0);
            if(validationResponse.error == true) {
            	$log.debug("Validation Response -- ", validationResponse);
            	$log.debug('Validation Failed to create CFS..')
				$scope.validationErrorAndFocus(validationResponse, validationResponse.errElement);
			}
            else {
            	cfsMasterFactory.create.query(vm.cfsMaster).$promise.then(function(data) {
		            if (data.responseCode == 'ERR0') {
		            	$log.debug("CFS Saved Successfully Response :: ", data);
		            	Notification.success($rootScope.nls["ERR400"]);
		            	$state.go('layout.cfsMaster');
		            } else {
		                $log.debug("Create CFS Failed :" + data.responseDescription)
		            }

		        }, function(error) {
		        	 $log.debug("Create CFS Failed : " + error)
		        });
            } 
        };

        
      /**
       * Common Validation Focus Functionality - starts here" 
       */
		$scope.validationErrorAndFocus = function(valResponse, elemId){
			$scope.errorMap = new Map();
			if(valResponse.error == true){
				$scope.errorMap.put(valResponse.errElement, valResponse.errMessage);
			}
				$scope.navigateToNextField(elemId);
		}
        
        //Update Existing Country Master
        vm.update = function() {
        	 var validationResponse = cfsMasterValidationService.validate(vm.cfsMaster,0);
             if(validationResponse.error == true) {
             	$log.debug("Validation Response -- ", validationResponse);
             	$log.debug('Validation Failed while Updating the CFS..')
 				$scope.validationErrorAndFocus(validationResponse, validationResponse.errElement);
 			}
             else {
             	cfsMasterFactory.update.query(vm.cfsMaster).$promise.then(function(data) {
 		            if (data.responseCode == 'ERR0') {
 		            	$log.debug("CFS Updated Successfully Response :: ", data);
 		            	Notification.success($rootScope.nls["ERR401"]);
 		            	$state.go('layout.cfsMaster');
 		            } else {
 		                $log.debug("Updating CFS Failed :" + data.responseDescription)
 		            }
 		        }, function(error) {
 		        	 $log.debug("Updating CFS Failed : " + error)
 		        });
             } 
        };
        
       $scope.selectedState=function(id){
    	   $rootScope.navigateToNextField(id);
       }
        
       $scope.selectedCity=function(id){
    	   $rootScope.navigateToNextField(id);
       }
        
        /**
         * Recent History - starts here
         */
        vm.isReloaded = localStorage.isCFSMasterReloaded;
        $scope.$on('addCFSMasterEvent', function(events, args){
        	$rootScope.unfinishedData = vm.cfsMaster;
			$rootScope.category = "CFS";
			$rootScope.unfinishedFormTitle = "CFS (New)";
        	if(vm.cfsMaster != undefined && vm.cfsMaster != null && vm.cfsMaster.cfsName != undefined && vm.cfsMaster.cfsName != null) {
        		$rootScope.subTitle = vm.cfsMaster.cfsName;
        	} else {
        		$rootScope.subTitle = "Unknown CFS "
        	}
        })
      
	    $scope.$on('editCFSMasterEvent', function(events, args){
	    	$rootScope.unfinishedData = vm.cfsMaster;
			$rootScope.category = "CFS";
			$rootScope.unfinishedFormTitle = "CFS Edit # "+vm.cfsMaster.cfsCode;
	    })
	      
	    $scope.$on('addCFSMasterEventReload', function(e, confirmation){
	    	    confirmation.message = "";
	    	    localStorage.reloadFormData = JSON.stringify(vm.cfsMaster) ;
	    	    localStorage.isCFSMasterReloaded = "YES";
	            e.preventDefault();
	    })
	    
	    $scope.$on('editCFSMasterEventReload', function(e, confirmation){
	    	    confirmation.message = "";
	    	    localStorage.reloadFormData = JSON.stringify(vm.cfsMaster) ;
	    	    localStorage.isCFSMasterReloaded = "YES";
	            e.preventDefault();
	    	  
	    })
	      


        /**
         * State Identification Handling Here.
         * @param {String} $stateParams.action - VIEW, ADD, EDIT, SEARCH.
         */
        //
        switch ($stateParams.action) {
            case "ADD":
            	 if ($stateParams.fromHistory === 'Yes') {
                     if (vm.isReloaded == "YES") {
                      vm.isReloaded = "NO";
                      localStorage.isCFSMasterReloaded = "NO";
                       vm.cfsMaster = JSON.parse(localStorage.reloadFormData);
                     } else {
                       vm.cfsMaster = $rootScope.selectedUnfilledFormData;
                     }
                   } else {
                     if (vm.isReloaded == "YES") {
                       vm.isReloaded = "NO";
                       localStorage.isCFSMasterReloaded = "NO";
                       vm.cfsMaster = JSON.parse(localStorage.reloadFormData);
                     } else {
                         $log.debug("Add NotHistory NotReload...")
                     }
                   }
                $rootScope.breadcrumbArr = cfsMasterDataService.getAddBreadCrumb();
                break;
            case "EDIT":
            	 if (vm.fromHistory === 'Yes') {
                     if (vm.isReloaded == "YES") {
                       vm.isReloaded = "NO";
                       localStorage.isCFSMasterReloaded = "NO";
                       vm.cfsMaster = JSON.parse(localStorage.reloadFormData);
                     } else {
                       vm.cfsMaster = $rootScope.selectedUnfilledFormData;
                     }
                   } else {
                     if (vm.isReloaded == "YES") {
                       vm.isReloaded = "NO";
                       localStorage.isCFSMasterReloaded = "NO";
                       vm.cfsMaster = JSON.parse(localStorage.reloadFormData);
                     } else {
                    	 vm.view($stateParams.id, false);
                     }
                   }
                $rootScope.breadcrumbArr = cfsMasterDataService.getEditBreadCrumb();
                break;
            default:
                $rootScope.breadcrumbArr = cfsMasterDataService.getListBreadCrumb();
                vm.searchInit();
        }

    }
]);
}) ();