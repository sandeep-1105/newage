app.factory('cfsMasterFactory', function($resource) {
	return {
		findById : $resource('/api/v1/cfsmaster/get/id/:id', {}, {
			 query : {method : 'GET', params : {id : ''}, isArray : false}
		}),
		create : $resource('/api/v1/cfsmaster/create', {}, {
			query : {method : 'POST'}
		}),
		update : $resource('/api/v1/cfsmaster/update', {}, {
			query : {method : 'POST'}
		}),
		delete : $resource('/api/v1/cfsmaster/delete/:id', {}, {
			query : {method : 'DELETE', params : {id : ''}}
		}),
		search : $resource('/api/v1/cfsmaster/get/search', {}, {
			fetch : {method : 'POST'}
		})
	};
})