app.controller('currencyRateOperationCtrl',['$rootScope', '$scope', '$http', '$location', 'CurrencyRateAdd', 'CurrencyRateView', '$timeout',
    'CurrencyRateEdit', 'currencyRateService', 'CurrencySearchExclude', 'ngProgressFactory', 'ngDialog', 
    '$state', '$stateParams', 'RecentHistorySaveService', 'appConstant', 'CommonValidationService', 
    function($rootScope, $scope, $http, $location, CurrencyRateAdd, CurrencyRateView, $timeout,
    CurrencyRateEdit, currencyRateService, CurrencySearchExclude, ngProgressFactory, ngDialog, 
    $state, $stateParams, RecentHistorySaveService, appConstant, CommonValidationService) {
    $scope.currencyRateMasterDataArr = [{}];
    $scope.pageAction = null;
    $scope.currencyDate = $rootScope.dateToString(new Date());
    $scope.selectedMaster = {};
    $scope.currencyList = [];
    $scope.selectedCurrencyRateMasterIndex = null;
    $scope.cancelShow = false;

    $scope.init = function() {
        $scope.contained_progressbar = ngProgressFactory.createInstance();
        $scope.contained_progressbar.setParent(document.getElementById('currencyrate-panel'));
        $scope.contained_progressbar.setAbsolute();
    };

    $scope.addCurrency = function() {
        if ($scope.validateCurrencyRateMaster(0)) {
            $scope.selectedMaster = {};
            $scope.selectedMaster.countryMaster = $rootScope.userProfile.selectedUserLocation.countryMaster;
            $scope.selectedMaster.fromCurrency = $rootScope.userProfile.selectedUserLocation.currencyMaster;
            $scope.currencyRateMasterDataArrr.push($scope.selectedMaster);
            $scope.cancelShow = true;
            console.log("Sie : " + $scope.currencyRateMasterDataArrr.length);
        } else {
            console.log("Validation failed");
        }
    }
    $scope.removeList = function(index) {
        $scope.currencyRateMasterDataArrr.splice(index, 1);
    }

    $scope.setOldDataVal = function() {

        $timeout(function() {
            $scope.oldData = JSON.stringify($scope.currencyRateMasterDataArrr);
            console.log("Currency Rate Data Loaded");
        }, 2000);
    }
    $scope.cancel = function() {
        var tempObj = $scope.currencyRateMasterDataArrr;
        if ($scope.oldData != JSON.stringify($scope.currencyRateMasterDataArrr)) {
            if (Object.keys(tempObj).length) {
                var newScope = $scope.$new();
                newScope.errorMessage = $rootScope.nls["ERR200"];
                ngDialog.openConfirm({
                        template: '<p>{{errorMessage}}</p>' +
                            '<div class="ngdialog-footer">' +
                            ' <button type="button" class="btn btn-default btn-property cancel-btn" ng-click="confirm(2)">No</button>' +
                            '<button type="button" class="btn btn-default btn-property accent-btn " ng-click="confirm(1)">Yes</button>' +
                            '<button type="button" class="btn btn-default btn-property pull-left grey-cancel-btn" ng-click="confirm(3)">Cancel</button>' +
                            '</div>',
                        plain: true,
                        scope: newScope,
                        className: 'ngdialog-theme-default'
                    })
                    .then(
                        function(value) {
                            if (value == 1 &&
                                $scope.currencyRateMasterDataArrr.id == null) {
                                $scope.save();
                            } else if (value == 1 &&
                                $scope.currencyRateMasterDataArrr.id != null) {
                                $scope.update();
                            } else if (value == 2) {

                                $state.go("layout.currencyRate", {
                                    submitAction: 'Cancelled'
                                });
                            } else {
                                console.log("cancelled");
                            }

                        });
            } else {

                $state.go("layout.currencyRate", {
                    submitAction: 'Cancelled'
                });
            }
        } else {

            $state.go("layout.currencyRate", {
                submitAction: 'Cancelled'
            });
        }
    }


    $scope.changeDateFn = function() {

        console.log("date Changed ");
        for (var i = 0; i < $scope.currencyRateMasterDataArrr.length; i++) {
            $scope.currencyRateMasterDataArrr[i].currencyDate = $rootScope.sendApiStartDateTime($scope.currencyDate);
        }
    }
    $scope.save = function() {
        console.log("Save method is called....", $scope.currencyDate);
        if ($scope.validateCurrencyRateMaster(0)) {
            for (var i = 0; i < $scope.currencyRateMasterDataArrr.length; i++) {
                $scope.currencyRateMasterDataArrr[i].currencyDate = $rootScope.sendApiStartDateTime($scope.currencyDate);
            }
            $scope.contained_progressbar.start();
            CurrencyRateAdd.save($scope.currencyRateMasterDataArrr).$promise.then(function(data) {
                    if (data.responseCode == "ERR0") {
                        currencyRateService.set([]);
                        currencyRateService.setPageAction(null);
                        currencyRateService.setDate(null);
                        $rootScope.successDesc = $rootScope.nls["ERR400"];
                        $state.go("layout.currencyRate", {
                            submitAction: 'Saved'
                        });
                    } else {
                        console.log("Currency Rate added Failed " + data.responseDescription)
                    }
                    $scope.contained_progressbar.complete();
                    angular.element(".panel-body").animate({
                        scrollTop: 0
                    }, "slow");
                },
                function(error) {
                    console.log("Currency Rate added Failed : " + error)
                });
        } else {
            console.log("Validation Failed");
        }
    }

    $scope.update = function() {
        console.log("update", $scope.currencyDate);
        if ($scope.validateCurrencyRateMaster(0)) {
            for (var i = 0; i < $scope.currencyRateMasterDataArrr.length; i++) {
                $scope.currencyRateMasterDataArrr[i].currencyDate = $rootScope.sendApiStartDateTime($scope.currencyDate);
            }

            $scope.contained_progressbar.start();

            CurrencyRateEdit.update($scope.currencyRateMasterDataArrr).$promise.then(function(data) {

                    if (data.responseCode == "ERR0") {
                        console.log("Currency Rate updated Successfully")
                        currencyRateService.set([]);
                        currencyRateService.setPageAction(null);
                        currencyRateService.setDate(null);
                        $rootScope.successDesc = $rootScope.nls["ERR401"];
                        $state.go("layout.currencyRate", {
                            submitAction: 'Saved'
                        });
                    } else {
                        console.log("Currency Rate updated Failed " + data.responseDescription)
                    }
                    $scope.contained_progressbar.complete();
                    angular.element(".panel-body").animate({
                        scrollTop: 0
                    }, "slow");
                },
                function(error) {
                    console.log("Currency Rate updated Failed : " + error)
                });
        } else {
            console.log("Validation Failed");
        }
    }

    $scope.listConfig = {
        search: true,
        showCode: true,
        ajax: true,
        columns: [{
                "title": "currencyName",
                seperator: false
            }, {
                "title": "currencyCode",
                seperator: true
            }

        ]
    }

    $scope.showCurrency = function(selectedCurrencyRateMasterIndex, currency) {
        $scope.selectedItem = -1;
        $scope.panelTitle = "Currency";
        $scope.selectedCurrencyRateMasterIndex = selectedCurrencyRateMasterIndex;
    }

    $scope.ajaxCurrencyEvent = function(object) {
        $scope.searchDto = {};
        $scope.searchDto.keyword = object == null ? "" : object;
        $scope.searchDto.selectedPageNumber = 0;
        $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

        return CurrencySearchExclude.fetch({
            "excludeCurrencyCode": $rootScope.userProfile.selectedUserLocation.currencyMaster.currencyCode
        }, $scope.searchDto).$promise.then(
            function(data) {
                if (data.responseCode == "ERR0") {
                    $scope.currencyList = data.responseObject.searchResult;
                    return $scope.currencyList;
                }
            },
            function(errResponse) {
                console.error('Error while fetching Currency');
            }
        );
    }



    $scope.selectedCurrency = function(nextIdValue, index) {
        console.log("Index :" + index);
        $scope.validateCurrencyRateMaster(1);
        $rootScope.navigateToNextField(nextIdValue + index);
    }


    $scope.validateCurrencyRateMaster = function(validateCode) {
        console.log("Validate Called");
        $scope.errorMap = new Map();

        if (validateCode == 0 || validateCode == 1) {

            for (var i = 0; i < $scope.currencyRateMasterDataArrr.length; i++) {
                if ($scope.currencyRateMasterDataArrr[i].toCurrency == undefined ||
                    $scope.currencyRateMasterDataArrr[i].toCurrency == null ||
                    $scope.currencyRateMasterDataArrr[i].toCurrency == "") {

                    var toCurrencyKey = "toCurrency";
                    $scope.toCurrencyMessageKey = toCurrencyKey;

                    $scope.errorMap.put(i + toCurrencyKey, $rootScope.nls["ERR2145"]);
                    console.log($rootScope.nls["ERR2145"]);
                    return false;
                }

                if ($scope.currencyRateMasterDataArrr[i].toCurrency.status == "Block") {
                    $scope.currencyRateMasterDataArrr[i].toCurrency = null;
                    var toCurrencyKey = "toCurrency";
                    $scope.toCurrencyMessageKey = toCurrencyKey;
                    $scope.errorMap.put(i + toCurrencyKey, $rootScope.nls["ERR2141"]);
                    console.log($rootScope.nls["ERR2141"]);
                    return false;

                }

                if ($scope.currencyRateMasterDataArrr[i].toCurrency.status == "Hide") {
                    $scope.currencyRateMasterDataArrr[i].toCurrency = null;
                    var toCurrencyKey = "toCurrency";
                    $scope.toCurrencyMessageKey = toCurrencyKey;
                    $scope.errorMap.put(i + toCurrencyKey, $rootScope.nls["ERR2142"]);
                    console.log($rootScope.nls["ERR2142"]);
                    return false;

                }
            }
        }



        if (validateCode == 0 || validateCode == 2) {

            for (var i = 0; i < $scope.currencyRateMasterDataArrr.length; i++) {
                if ($scope.currencyRateMasterDataArrr[i].buyRate == undefined ||
                    $scope.currencyRateMasterDataArrr[i].buyRate == null ||
                    $scope.currencyRateMasterDataArrr[i].buyRate == "") {
                    var buyRateKey = "buyRate";
                    $scope.buyRateMessageKey = buyRateKey;
                    console.log("Buy Rate is Mandatory");
                    $scope.errorMap.put(i + buyRateKey, $rootScope.nls["ERR2122"]);
                    console.log($rootScope.nls["ERR2122"]);
                    return false;
                } else {

                    if (!CommonValidationService.checkRegExp(appConstant.Reg_Exp_Master_CURRENCY_BUY_RATE, $scope.currencyRateMasterDataArrr[i].buyRate)) {
                        var buyRateKey = "buyRate";
                        $scope.buyRateMessageKey = buyRateKey;
                        $scope.errorMap.put(i + buyRateKey, $rootScope.nls["ERR2127"]);
                        console.log($rootScope.nls["ERR2127"]);
                        return false;
                    }


                }
            }
        }

        if (validateCode == 0 || validateCode == 3) {

            for (var i = 0; i < $scope.currencyRateMasterDataArrr.length; i++) {
                if ($scope.currencyRateMasterDataArrr[i].sellRate == undefined ||
                    $scope.currencyRateMasterDataArrr[i].sellRate == null ||
                    $scope.currencyRateMasterDataArrr[i].sellRate == "") {
                    var sellRateKey = "sellRate";
                    $scope.sellRateArrayIndex = i;
                    $scope.sellRateMessageKey = sellRateKey;
                    $scope.errorMap.put(i + sellRateKey, $rootScope.nls["ERR2123"]);
                    console.log($rootScope.nls["ERR2123"]);
                    return false;
                } else {

                    if (!CommonValidationService.checkRegExp(appConstant.Reg_Exp_Master_CURRENCY_SELL_RATE, $scope.currencyRateMasterDataArrr[i].sellRate)) {
                        var buyRateKey = "sellRate";
                        $scope.buyRateMessageKey = buyRateKey;
                        $scope.errorMap.put(i + buyRateKey, $rootScope.nls["ERR2128"]);
                        console.log($rootScope.nls["ERR2128"]);
                        return false;
                    }

                }
            }


        }
        if (validateCode == 0) {
            for (var i = 0; i < $scope.currencyRateMasterDataArrr.length; i++) {
                if (($scope.currencyRateMasterDataArrr[i].sellRate != undefined &&
                        $scope.currencyRateMasterDataArrr[i].sellRate != null &&
                        $scope.currencyRateMasterDataArrr[i].sellRate != "") &&
                    ($scope.currencyRateMasterDataArrr[i].buyRate != undefined &&
                        $scope.currencyRateMasterDataArrr[i].buyRate != null &&
                        $scope.currencyRateMasterDataArrr[i].buyRate != "")) {
                    /*if ($scope.currencyRateMasterDataArrr[i].sellRate == $scope.currencyRateMasterDataArrr[i].buyRate) {
                          var sellRateKey = "sellRate";
                          $scope.sellRateArrayIndex = i;
                          $scope.sellRateMessageKey = sellRateKey;
                          $scope.errorMap.put(i + sellRateKey, $rootScope.nls["ERR2134"]);
                          return false;
                      }*/
                }
            } //for end
        }
        return true;
    }
    $scope.currencyRender = function(item) {
        return {
            label: item.currencyName,
            item: item
        }
    }




    $scope.$on('addCurrencyRateEvent', function(events, args) {
        $rootScope.unfinishedData = $scope.currencyRateMasterDataArrr;
        $rootScope.category = "Currency Rate";
        $rootScope.unfinishedFormTitle = "Currency Rate (New)";
        if ($scope.currencyRateMasterDataArrr != undefined && $scope.currencyRateMasterDataArrr != null && $scope.currencyRateMasterDataArrr.length > 0) {
            $rootScope.subTitle = "";
        } else {
            $rootScope.subTitle = "Unknown"
        }
    })

    $scope.$on('editCurrencyRateEvent', function(events, args) {
        console.log("Scope currencyRateMasterDataArrr data :: ", $scope.currencyRateMasterDataArrr);
        $rootScope.unfinishedData = $scope.currencyRateMasterDataArrr;
        $rootScope.category = "Currency Rate";
        $rootScope.unfinishedFormTitle = "Currency Rate Edit # "; //+$scope.currencyrateMaster.id;
        if ($scope.currencyRateMasterDataArrr != undefined && $scope.currencyRateMasterDataArrr != null) {
            $rootScope.subTitle = "";
        } else {
            $rootScope.subTitle = "Unknown"
        }
    })
    $scope.$on('addCurrencyRateEventReload', function(e, confirmation) {
        confirmation.message = "All data willl be lost.";
        localStorage.reloadFormData = JSON.stringify($scope.currencyRateMasterDataArrr);
        localStorage.isCurrencyRateReloaded = "YES";
        e.preventDefault();
    });
    $scope.$on('editCurrencyRateEventReload', function(e, confirmation) {
        confirmation.message = "All data willl be lost.";
        localStorage.reloadFormData = JSON.stringify($scope.currencyRateMasterDataArrr);
        localStorage.isCurrencyRateReloaded = "YES";
        e.preventDefault();
    });

    $scope.isReloaded = localStorage.isCurrencyRateReloaded;

    if ($stateParams.action == "ADD") {
        $scope.pageAction = "Add";
        if ($stateParams.fromHistory === 'Yes') {
            if ($scope.isReloaded == "YES") {
                $scope.isReloaded = "NO";
                localStorage.isCurrencyRateReloaded = "NO";
                $scope.currencyRateMasterDataArrr = JSON.parse(localStorage.reloadFormData);
            } else {
                $scope.currencyRateMasterDataArrr = $rootScope.selectedUnfilledFormData;
            }
            $scope.oldData = "";
        } else {
            if ($scope.isReloaded == "YES") {
                $scope.isReloaded = "NO";
                localStorage.isCurrencyRateReloaded = "NO";
                $scope.currencyRateMasterDataArrr = [];
                $scope.currencyRateMasterDataArrr = JSON.parse(localStorage.reloadFormData);
                console.log("$scope.currencyRateMasterDataArrr :: ", $scope.currencyRateMasterDataArrr);
                $scope.oldData = "";
            } else {
                $scope.currencyRateMaster = {};
                $scope.currencyRateMaster.countryMaster = $rootScope.userProfile.selectedUserLocation.countryMaster;
                $scope.currencyRateMaster.fromCurrency = $rootScope.userProfile.selectedUserLocation.currencyMaster;
                $scope.currencyRateMasterDataArrr = [];
                $scope.currencyRateMasterDataArrr.push($scope.currencyRateMaster);
                $scope.setOldDataVal();
            }
        }
        $rootScope.breadcrumbArr = [{
                label: "Master",
                state: "layout.currencyRate"
            },
            {
                label: "Currency Rate",
                state: "layout.currencyRate"
            },
            {
                label: "Add Currency Rate",
                state: null
            }
        ];


    } else {
        $scope.pageAction = "Edit";
        if ($stateParams.fromHistory === 'Yes') {
            if ($scope.isReloaded == "YES") {
                $scope.isReloaded = "NO";
                localStorage.isCurrencyRateReloaded = "NO";
                $scope.currencyRateMasterDataArrr = JSON.parse(localStorage.reloadFormData);
            } else {
                $scope.currencyRateMasterDataArrr = $rootScope.selectedUnfilledFormData;

            }
            $scope.oldData = "";
        } else {
            if ($scope.isReloaded == "YES") {
                $scope.isReloaded = "NO";
                localStorage.isCurrencyRateReloaded = "NO";
                $scope.currencyRateMasterDataArrr = JSON.parse(localStorage.reloadFormData);
                $scope.oldData = "";
            } else {

                CurrencyRateView.get({
                    id: $stateParams.currencyRateId
                }, function(data) {
                    if (data.responseCode == "ERR0") {
                        $scope.currencyRateMasterDataArrr = [];
                        $scope.currencyrateMaster = data.responseObject;
                        $scope.currencyRateMasterDataArrr.push($scope.currencyrateMaster);

                        $scope.currencyDate = $rootScope.dateToString($scope.currencyrateMaster.currencyDate);
                        $rootScope.unfinishedFormTitle = "Currency Rate Edit # ";
                        var rHistoryObj = {
                            'title': 'Currency Rate Edit # ' + $scope.currencyDate,
                            'subTitle': "Currency Rate For " + $scope.currencyrateMaster.toCurrency.currencyCode,
                            'stateName': $state.current.name,
                            'stateParam': JSON.stringify($stateParams),
                            'stateCategory': 'Currency Rate',
                            'serviceType': 'AIR',
                            'serviceCode': "",
                            'orginAndDestination': "",
                            'showService': false
                        }
                        RecentHistorySaveService.form(rHistoryObj);
                        $scope.setOldDataVal();
                    } else {
                        console.log("CostCenter view Failed " + data.responseDescription)
                    }
                }, function(error) {
                    console.log("CostCenter view Failed : " + error)
                });
            }
        }
        $rootScope.breadcrumbArr = [{
                label: "Master",
                state: "layout.currencyRate"
            },
            {
                label: "Currency Rate",
                state: "layout.currencyRate"
            },
            {
                label: "Edit Currency Rate",
                state: null
            }
        ];

    }

    $scope.init();

}]);