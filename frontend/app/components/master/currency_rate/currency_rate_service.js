/**
 * 
 */
app.factory('currencyRateService',[ function() {
	var savedData = [];
	var pageAction="";
	function set(data) {
		savedData = data;
		localStorage.savedData=JSON.stringify(savedData);
		
	}
	function get() {
		savedData = localStorage.savedData==null?null:JSON.parse(localStorage.savedData);
		return savedData;
	}

	function setPageAction(data) {
		pageAction = data;
		localStorage.pageAction=pageAction;
		
	}
	function getPageAction() {
		pageAction = localStorage.pageAction;
		return pageAction;
	}
	var date="";
	function setDate(data) {
		date = data;
		localStorage.date=date;
		
	}
	function getDate() {
		date = localStorage.date;
		
		return date;
	}

	return {
		set : set,
		get : get,
		setPageAction : setPageAction,
		getPageAction : getPageAction,
		setDate : setDate,
		getDate : getDate
	}
	
}]);