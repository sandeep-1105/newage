(function() {

	app.factory("CurrencyRateSearch",['$resource', function($resource) {
		return $resource("/api/v1/currencyratemaster/get/search", {}, {
			query : {
				method : 'POST'
			}
		});
	}]);

	app.factory("CurrencyRateAdd", ['$resource', function($resource) {
		return $resource("/api/v1/currencyratemaster/create", {}, {
			save : {
				method : 'POST'
			}
		});
	}]);

	app.factory("CurrencyRateEdit", ['$resource', function($resource) {
		return $resource("/api/v1/currencyratemaster/update", {}, {
			update : {
				method : 'POST'
			}
		});
	}]);

	app.factory("CurrencyRateView", ['$resource', function($resource) {
		return $resource("/api/v1/currencyratemaster/get/id/:id", {}, {
			get : {
				method : 'GET',
				params : {
					id : ''
				},
				isArray : false
			}
		});
	}]);

	app.factory("CurrencyRateRemove", ['$resource', function($resource) {
		return $resource("/api/v1/currencyratemaster/delete/:id", {}, {
			remove : {
				method : 'DELETE',
				params : {
					id : ''
				},
				isArray : false
			}
		});
	}]);
	
	app.factory("CurrencySearchExclude", ['$resource', function($resource) {
		return $resource("/api/v1/currencymaster/get/search/exclude/:excludeCurrencyCode", {}, {
			fetch : {
				method : 'POST',
				params : {
					excludeCurrencyCode : ''
				},
				isArray : false
			}
		});
	}]);

	app.factory("CurrencyRateByBase", ['$resource', function($resource) {
		return $resource("/api/v1/currencyratemaster/get/fromCurrency/:fromCurrencyId", {}, {
			get : {
				method : 'GET',
				params : {
					fromCurrencyId : ''
				},
				isArray : false
			}
		});
	}]);
	
})();
