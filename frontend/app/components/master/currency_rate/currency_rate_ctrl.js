app.controller('currencyRateController', ['$rootScope', '$scope', '$http', '$location', '$window', '$state', '$stateParams', 'ngDialog',
    'ngTableParams', 'CurrencyRateSearch', 'CurrencyRateRemove', 'currencyRateService', 'roleConstant',
    function($rootScope, $scope, $http, $location, $window, $state, $stateParams, ngDialog,
        ngTableParams, CurrencyRateSearch, CurrencyRateRemove, currencyRateService, roleConstant) {

        console.log("CurrencyRate controller loaded..");
        /************************************************************
         * ux - by Muthu
         * reason - ipad compatibility fix
         *
         * *********************************************************/
        $scope.deskTopView = true;

        /********************* ux fix ends here *****************************/
        $scope.currencyRateHeadArr = [{
                "name": "#",
                "width": "col-xs-0half",
                "model": "no",
                "search": false,
            },
            {
                "name": "Date",
                "width": "col-xs-3",
                "model": "currencyDate",
                "search": true,
                "type": "date-range",
                "sort": true,
                "key": "searchCurrencyDate"

            },
            {
                "name": "Currency",
                "width": "col-xs-2",
                "model": "toCurrency.currencyCode",
                "search": true,
                "type": "text",
                "sort": true,
                "key": "searchToCurrencyCode"
            },
            {
                "name": "Buy Rate (" + $rootScope.userProfile.selectedUserLocation.currencyMaster.currencyCode + ")",
                "width": "col-xs-2 text-right",
                "model": "buyRate",
                "search": true,
                "type": "number",
                "sort": true,
                "key": "searchBuyRate"
            },
            {
                "name": "Sell Rate (" + $rootScope.userProfile.selectedUserLocation.currencyMaster.currencyCode + ")",
                "width": "col-xs-2 text-right",
                "model": "sellRate",
                "search": true,
                "type": "number",
                "sort": true,
                "key": "searchSellRate"
            }
        ]
        $scope.sortSelection = {
            sortKey: "currencyDate",
            sortOrder: "desc"
        }


        $scope.currencyRateMasterDataArr = [{}];
        $scope.currencyRateMaster = {};
        $scope.searchDto = {};
        //$scope.searchDto.currencyDate = {startDate: null, endDate: null};
        //$scope.datepickeropts = {singleDatePicker:true,
        //        				locale: {format:'YYYY-MM-DD'},
        //				       eventHandlers: {
        //				          'apply.daterangepicker': function(ev,picker){$scope.changeSearch();}
        //				       }
        //};

        currencyRateService.set({});

        $scope.limitArr = [10, 15, 20];
        $scope.page = 0;
        $scope.limit = 10;


        $scope.init = function() {
            console.log("CurrencyRate Init method called.");
            $rootScope.setNavigate1("Master");
            $rootScope.setNavigate2("Currency Rate");
            $scope.showHistory = false;
            currencyRateService.set({});
            $scope.searchDto = {};
            //$scope.searchDto.currencyDate = {startDate: null, endDate: null};
            $scope.currencyRateMasterDataArr = [{}];
            $scope.search();
            $scope.currencyRateSortIconClass = " ";
            $scope.sortIconCurrencyName = "fa fa-chevron-down";
        };

        $scope.changepage = function(param) {
            $scope.page = param.page;
            $scope.limit = param.size;
            $scope.search();
        }

        $scope.add = function() {
            if ($rootScope.roleAccess(roleConstant.MASTER_FINANCE_CURRENCY_RATE_CREATE)) {
                console.log("Add method called...");
                $scope.currencyRateMaster = {};
                $scope.currencyRateMaster.countryMaster = $rootScope.userProfile.selectedUserLocation.countryMaster;
                $scope.currencyRateMaster.fromCurrency = $rootScope.userProfile.selectedUserLocation.currencyMaster;
                $scope.currencyRateMasterDataArr = [$scope.currencyRateMaster];
                currencyRateService.set($scope.currencyRateMasterDataArr);
                currencyRateService.setPageAction("Add");
                currencyRateService.setDate($rootScope.sendApiStartDateTime(new Date()));
                //		$location.path("/master/currency_rate_operation");

                $state.go("layout.addCurrencyRate");
            }
        };


        $scope.cancel = function() {
            $scope.showHistory = false;
            $scope.currencyRateMaster = {};
            /************************************************************
             * ux - by Muthu
             * reason - ipad compatibility fix
             *
             * *********************************************************/
            $scope.deskTopView = true;

            /********************* ux fix ends here *****************************/
        };


        $scope.deleteCurrencyRate = function() {

            if ($rootScope.roleAccess(roleConstant.MASTER_FINANCE_CURRENCY_RATE_DELETE)) {
                var newScope = $scope.$new();
                newScope.errorMessage = $rootScope.nls["ERR237"];
                ngDialog
                    .openConfirm({
                        template: '<p>{{errorMessage}}</p>' +
                            '<div class="ngdialog-footer">' +
                            '<button type="button" class="btn btn-default btn-property cancel-btn" ng-click="closeThisDialog(0)">Cancel' +
                            '<button type="button" class="btn btn-primary btn-property accent-btn" ng-click="confirm(1)">Yes' +
                            '</button></div>',
                        plain: true,
                        scope: newScope,
                        className: 'ngdialog-theme-default'
                    })
                    .then(
                        function(value) {
                            CurrencyRateRemove
                                .remove({
                                        id: $scope.currencyRateMaster.id
                                    },
                                    function(data) {
                                        if (data.responseCode == 'ERR0') {
                                            console.log("CurrencyRate deleted Successfully")
                                            $scope.init();
                                            $scope.deskTopView = true;
                                        } else {
                                            console.log("CurrencyRate deleted Failed " + data.responseDescription)
                                        }
                                    },
                                    function(error) {
                                        console.log("CurrencyRate deleted Failed : " + error)
                                    });
                        },
                        function(value) {
                            console.log("deleted cancelled");
                        });
            }
        }




        $scope.editCurrencyRateMaster = function() {

            if ($rootScope.roleAccess(roleConstant.MASTER_FINANCE_CURRENCY_RATE_MODIFY)) {
                $scope.currencyRateMasterDataArr = [];
                $scope.currencyRateMasterDataArr.push($scope.currencyRateMaster);
                currencyRateService.set($scope.currencyRateMasterDataArr);
                currencyRateService.setPageAction("Edit");
                currencyRateService.setDate($scope.currencyRateMaster.currencyDate);

                $state.go("layout.editCurrencyRate", {
                    currencyRateId: $scope.currencyRateMaster.id
                });
            }
        }




        $scope.rowSelect = function(data) {

            if ($rootScope.roleAccess(roleConstant.MASTER_FINANCE_CURRENCY_RATE_VIEW)) {
                $scope.showHistory = false;
                $scope.currencyRateMaster = data;
                /************************************************************
                 * ux - by Muthu
                 * reason - ipad compatibility fix
                 *
                 * *********************************************************/

                var windowInner = $window.innerWidth;
                if (windowInner <= 1199) {
                    $scope.deskTopView = false;

                } else {
                    $scope.deskTopView = true;
                }
                angular.element($window).bind('resize', function() {
                    if ($window.innerWidth >= 1200) {
                        $scope.deskTopView = true;
                    } else if ($window.innerWidth <= 1199) {
                        if ($scope.currencyRateMaster.id != null) {
                            $scope.deskTopView = false;
                        } else {
                            $scope.deskTopView = true;
                        }
                    }

                    console.log("window resizing..." + $window.innerWidth);
                    $scope.$digest();
                });

                /********************* ux fix ends here *****************************/
            }
        }

        $scope.limitChange = function(item) {
            $scope.page = 0;
            $scope.limit = item;
            $scope.cancel();
            $scope.search();
        }

        $scope.sortChange = function(param) {
            $scope.searchDto.orderByType = param.sortOrder.toUpperCase();
            $scope.searchDto.sortByColumn = param.sortKey;
            $scope.cancel();
            $scope.search();
        }
        $scope.changeSearch = function(param) {
            for (var attrname in param) {
                $scope.searchDto[attrname] = param[attrname];
            }
            $scope.page = 0;
            $scope.cancel();
            $scope.search();

        }


        $scope.search = function() {
            $scope.currencyRateMaster = {};

            $scope.searchDto.searchFromCurrencyCode = $rootScope.userProfile.selectedUserLocation.currencyMaster.currencyCode;


            $scope.searchDto.selectedPageNumber = $scope.page;
            $scope.searchDto.recordPerPage = $scope.limit;
            console.log(JSON.stringify($scope.searchDto));
            $scope.currencyRateArr = [];
            $scope.searchDto.selectedPageNumber = $scope.page;
            $scope.searchDto.recordPerPage = $scope.limit;
            CurrencyRateSearch.query($scope.searchDto).$promise.then(function(
                data, status) {
                $scope.totalRecord = data.responseObject.totalRecord;
                var tempArr = [];
                var resultArr = [];
                tempArr = data.responseObject.searchResult;
                var tempObj = {};
                angular.forEach(tempArr, function(item, index) {
                    tempObj = item;

                    tempObj.no = (index + 1) + ($scope.page * $scope.limit);
                    resultArr.push(tempObj);
                    tempObj = {};
                });
                $scope.currencyRateArr = resultArr;
            });
            $rootScope.breadcrumbArr = [{
                    label: "Master",
                    state: "layout.currencyRate"
                },
                {
                    label: "Currency Rate",
                    state: "layout.currencyRate"
                }
            ];

        }


    }
]);