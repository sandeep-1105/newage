app.controller('portGroupOperationCtrl',['$rootScope', '$scope', '$state', '$stateParams', 'CountryList', 'PortGroupSaveOrUpdate', 'PortRemove',
    "ngProgressFactory", 'ngDialog', 'PortGroupView', 'appConstant', 'CommonValidationService', 'Notification',
	function($rootScope, $scope, $state, $stateParams, CountryList, PortGroupSaveOrUpdate, PortRemove,
    ngProgressFactory, ngDialog, PortGroupView, appConstant, CommonValidationService, Notification) {

    $scope.contained_progressbar = ngProgressFactory.createInstance();
    $scope.contained_progressbar.setParent(document.getElementById('group-panel'));
    $scope.contained_progressbar.setAbsolute();

    $scope.init = function() {

        console.log("Init is called...............");

        if ($scope.portGroupMaster == undefined) {
            $scope.portGroupMaster = {};
            $scope.portGroupMaster.status = 'Active';
        }


        // Setting it to compare while cancelling
        $scope.oldData = JSON.stringify($scope.portGroupMaster);
    }


    $scope.cancel = function() {

        if ($scope.oldData !== JSON.stringify($scope.portGroupMaster)) {

            var newScope = $scope.$new();
            newScope.errorMessage = $rootScope.nls["ERR200"];
            ngDialog.openConfirm({
                template: '<p>{{errorMessage}}</p>' +
                    '<div class="ngdialog-footer">' +
                    ' <button type="button" class="btn btn-default btn-property cancel-btn" ng-click="confirm(2)">No</button>' +
                    '<button type="button" class="btn btn-default btn-property accent-btn " ng-click="confirm(1)">Yes</button>' +
                    '<button type="button" class="btn btn-default btn-property pull-left grey-cancel-btn" ng-click="confirm(3)">Cancel</button>' +
                    '</div>',
                plain: true,
                scope: newScope,
                className: 'ngdialog-theme-default'
            }).then(function(value) {

                if (value == 1) {
                    $scope.saveOrUpdate();
                } else {

                    var params = {};
                    params.submitAction = 'Cancelled';
                    $state.go("layout.portGroup", params);
                }
            });
        } else {
            var params = {};
            params.submitAction = 'Cancelled';
            $state.go("layout.portGroup", params);
        }
    }

    $scope.ajaxCountryEvent = function(object) {

        $scope.searchDto = {};
        $scope.searchDto.keyword = object == null ? "" : object;
        $scope.searchDto.selectedPageNumber = 0;
        $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

        return CountryList.fetch($scope.searchDto).$promise.then(function(data) {
            if (data.responseCode == "ERR0") {
                $scope.contriesList = data.responseObject.searchResult;
                return $scope.contriesList;
            }
        }, function(errResponse) {
            console.error('Error while fetching Countries', errResponse);
        });

    }

    $scope.selectedCountry = function() {
        $scope.validatePortGroupMaster(3);
    };


    $scope.countryRender = function(item) {
        return {
            label: item.countryName,
            item: item
        }
    }


    $scope.saveOrUpdate = function() {

        console.log("SaveOrUpdate is called...");

        if ($scope.validatePortGroupMaster(0)) {
            $scope.contained_progressbar.start();

            PortGroupSaveOrUpdate.save($scope.portGroupMaster).$promise.then(function(data) {

                if (data.responseCode == "ERR0") {
                    Notification.success($rootScope.nls["ERR401"]);
                    console.log("Port Group updated Successfully");
                    $state.go("layout.portGroup", {
                        submitAction: 'Saved'
                    });
                } else {
                    console.log("Port Group updated Failed ", data.responseDescription)
                }
                $scope.contained_progressbar.complete();
                angular.element(".panel-body").animate({
                    scrollTop: 0
                }, "slow");
            }, function(error) {
                console.log("Group updated Failed : ", error);
            });
        } else {
            console.log("Form Invalid");
        }


    }



    $scope.validatePortGroupMaster = function(validateCode) {

        $scope.errorMap = new Map();


        if (validateCode == 0 || validateCode == 1) {

            if ($scope.portGroupMaster.portGroupName == undefined || $scope.portGroupMaster.portGroupName == null || $scope.portGroupMaster.portGroupName == "") {
                $scope.errorMap.put("portGroupName", $rootScope.nls["ERR1530"]);
                return false;
            } else {
                if (!CommonValidationService.checkRegExp(appConstant.Reg_Exp_Master_PORT_GROUP_NAME, $scope.portGroupMaster.portGroupName)) {
                    $scope.errorMap.put("portGroupName", $rootScope.nls["ERR1527"]);
                    return false;
                }
            }

        }

        if (validateCode == 0 || validateCode == 2) {

            if ($scope.portGroupMaster.portGroupCode == undefined || $scope.portGroupMaster.portGroupCode == null || $scope.portGroupMaster.portGroupCode == "") {
                $scope.errorMap.put("portGroupCode", $rootScope.nls["ERR1529"]);
                return false;
            } else {
                if (!CommonValidationService.checkRegExp(appConstant.Reg_Exp_Master_PORT_GROUP_CODE, $scope.portGroupMaster.portGroupCode)) {
                    $scope.errorMap.put("portGroupCode", $rootScope.nls["ERR1526"]);
                    return false;
                }
            }

        }

        if (validateCode == 0 || validateCode == 3) {

            if ($scope.portGroupMaster.countryMaster == undefined || $scope.portGroupMaster.countryMaster == null || $scope.portGroupMaster.countryMaster.id == null) {

                $scope.errorMap.put("country", $rootScope.nls["ERR1531"]);
                return false;

            } else {
                if ($scope.portGroupMaster.countryMaster.status == 'Block') {

                    $scope.portGroupMaster.countryMaster = null;
                    $scope.errorMap.put("country", $rootScope.nls["ERR1532"]);
                    return false;


                }
                if ($scope.portGroupMaster.countryMaster.status == 'Hide') {

                    $scope.portGroupMaster.countryMaster = null;
                    $scope.errorMap.put("country", $rootScope.nls["ERR1532"]);
                    return false;
                }
            }
        }
        return true;

    }

    $scope.setStatus = function(data) {
        if (data == 'Active') {
            return 'activetype';
        } else if (data == 'Block') {
            return 'blockedtype';
        } else if (data == 'Hide') {
            return 'hiddentype';
        }
    }




    $scope.getPortGroupById = function(portGroupId) {

        console.log("getPortGroupById ", portGroupId);

        PortGroupView.get({
            id: portGroupId
        }, function(data) {
            if (data.responseCode == "ERR0") {
                console.log("Successful while getting portGroupMaster.", data.responseObject);
                $scope.portGroupMaster = data.responseObject;
                console.log("Port Group Master ", $scope.portGroupMaster);
                $scope.init();
            }
        }, function(error) {
            console.log("Error while getting portGroupMaster.", error)
        });

    }



    // port group 


    $scope.$on('addPortGroupEvent', function(events, args) {
        $rootScope.unfinishedData = $scope.portGroupMaster;
        $rootScope.unfinishedFormTitle = "Port Group Master (New)";
        $rootScope.category = "Port Group Master";
        if ($scope.portGroupMaster != undefined && $scope.portGroupMaster != null && $scope.portGroupMaster.id != undefined &&
            $scope.portGroupMaster.portGroupName != null && $scope.portGroupMaster.portGroupName != "") {
            $rootScope.subTitle = $scope.portGroupMaster.portGroupName;
        } else {
            $rootScope.subTitle = "Unknown Port Group Master"
        }
    });


    $scope.$on('editPortGroupEvent', function(events, args) {
        $rootScope.unfinishedData = $scope.portGroupMaster;
        $rootScope.unfinishedFormTitle = "Port Group Master Edit (" + $scope.portGroupMaster.portGroupCode + ")";
        $rootScope.category = "Port Group Master";
        if ($scope.portGroupMaster != undefined && $scope.portGroupMaster != null && $scope.portGroupMaster.id != undefined &&
            $scope.portGroupMaster.portGroupName != null && $scope.portGroupMaster.portGroupName != "") {
            $rootScope.subTitle = $scope.portGroupMaster.portGroupName;
        } else {
            $rootScope.subTitle = "Unknown Port Group Master"
        }
    });


    $scope.$on('addPortGroupEventReload', function(e, confirmation) {
        confirmation.message = "";
        localStorage.reloadFormData = JSON.stringify($scope.portGroupMaster);
        localStorage.isPortGroupReloaded = "YES";
        e.preventDefault();
    });


    $scope.$on('editPortGroupEventReload', function(e, confirmation) {
        confirmation.message = "";
        localStorage.reloadFormData = JSON.stringify($scope.portGroupMaster);
        localStorage.isPortGroupReloaded = "YES";
        e.preventDefault();
    });



    $scope.isReloaded = localStorage.isPortGroupReloaded;
    if ($stateParams.action == "ADD") {
        if ($stateParams.fromHistory === 'Yes') {
            if ($scope.isReloaded == "YES") {
                console.log("Add History Reload...")
                $scope.isReloaded = "NO";
                localStorage.isPortGroupReloaded = "NO";
                $scope.portGroupMaster = JSON.parse(localStorage.reloadFormData);
            } else {
                console.log("Add History Not Reload...")
                $scope.portGroupMaster = $rootScope.selectedUnfilledFormData;
            }
        } else {
            if ($scope.isReloaded == "YES") {
                console.log("Add Not History Reload....");
                $scope.isReloaded = "NO";
                localStorage.isPortGroupReloaded = "NO";
                $scope.portGroupMaster = JSON.parse(localStorage.reloadFormData);
            } else {
                console.log("Add Not History Not Reload...");
                $scope.init();
            }
        }

        $rootScope.breadcrumbArr = [{
                label: "Master",
                state: "layout.portGroup"
            },
            {
                label: "Port Group",
                state: "layout.portGroup"
            },
            {
                label: "Add Port Group",
                state: null
            }
        ];

        $rootScope.navigateToNextField("portGroupName");

    } else {

        if ($stateParams.fromHistory === 'Yes') {

            if ($scope.isReloaded == "YES") {

                $scope.isReloaded = "NO";

                localStorage.isPortGroupReloaded = "NO";

                $scope.portGroupMaster = JSON.parse(localStorage.reloadFormData);

            } else {

                $scope.portGroupMaster = $rootScope.selectedUnfilledFormData;

            }

        } else {

            if ($scope.isReloaded == "YES") {
                $scope.isReloaded = "NO";
                localStorage.isPortReloaded = "NO";
                $scope.portGroupMaster = JSON.parse(localStorage.reloadFormData);
            } else {
                $scope.getPortGroupById($stateParams.portGroupId);

            }

        }

        $rootScope.breadcrumbArr = [{
                label: "Master",
                state: "layout.portGroup"
            },
            {
                label: "Port Group",
                state: "layout.portGroup"
            },
            {
                label: "Edit Port Group",
                state: null
            }
        ];

        $rootScope.navigateToNextField("portGroupName");
    }


}]);