app.controller('portGroupCtrl',['$rootScope', '$scope', '$window', '$state', '$stateParams', 'ngDialog', 
		'PortGroupSearch', 'PortGroupRemove', 'roleConstant', 
	function($rootScope, $scope, $window, $state, $stateParams, ngDialog, 
		PortGroupSearch, PortGroupRemove, roleConstant) {

    /************************************************************
     * ux - by Muthu
     * reason - ipad compatibility fix
     *
     * *********************************************************/
    $scope.deskTopView = true;

    /********************* ux fix ends here *****************************/

    $scope.limitArr = [10, 15, 20];
    $scope.page = 0;
    $scope.limit = 10;

    $scope.portGroupHeadArr = [{
        "name": "#",
        "width": "col-xs-0half",
        "model": "no",
        "search": false,
    }, {
        "name": "Name",
        "width": "col-xs-3",
        "model": "portGroupName",
        "search": true,
        "wrap_cell": true,
        "type": "text",
        "sort": true,
        "key": "searchPortGroupName"

    }, {
        "name": "Code",
        "width": "col-xs-1half",
        "model": "portGroupCode",
        "search": true,
        "type": "text",
        "sort": true,
        "key": "searchPortGroupCode"
    }, {
        "name": "Country",
        "width": "col-xs-2",
        "model": "countryMaster.countryName",
        "search": true,
        "wrap_cell": true,
        "type": "text",
        "sort": true,
        "key": "searchCountryName"
    }, {
        "name": "Status",
        "width": "col-xs-2",
        "model": "status",
        "search": true,
        "type": "drop",
        "sort": true,
        "data": $rootScope.enum['LovStatus'],
        "key": "searchStatus"
    }];




    $scope.sortSelection = {
        sortKey: "portGroupName",
        sortOrder: "asc"
    }

    $scope.init = function() {
        $scope.searchDto = {};
        $scope.detailView = false;
        $scope.search();
    };

    $scope.cancel = function() {
        $scope.portGroupMaster = {};
        $scope.showHistory = false;
        $scope.detailView = false;
        /************************************************************
         * ux - by Muthu
         * reason - ipad compatibility fix
         *
         * *********************************************************/
        $scope.deskTopView = true;

        /********************* ux fix ends here *****************************/
    }


    $scope.addPortGroupMaster = function() {
        if ($rootScope.roleAccess(roleConstant.MASTER_GENERAL_PORT_GROUP_CREATE)) {
            console.log("Add Port Group Button Pressed...");
            $state.go("layout.addPortGroup");
        }
    }


    $scope.editPortGroupMaster = function() {
        if ($rootScope.roleAccess(roleConstant.MASTER_GENERAL_PORT_GROUP_MODIFY)) {
            console.log("Edit Port Group Button Pressed...");
            $state.go("layout.editPortGroup", {
                portGroupId: $scope.portGroupMaster.id
            });
        }
    };


    $scope.deletePortGroupMaster = function() {
        if ($rootScope.roleAccess(roleConstant.MASTER_GENERAL_PORT_GROUP_DELETE)) {
            var newScope = $scope.$new();
            newScope.errorMessage = $rootScope.nls["ERR247"];
            ngDialog.openConfirm({
                template: '<p>{{errorMessage}}</p>' +
                    '<div class="ngdialog-footer">' +
                    '<button type="button" class="btn btn-default btn-property cancel-btn" ng-click="closeThisDialog(0)">No' +
                    '<button type="button" class="btn btn-primary btn-property accent-btn" ng-click="confirm(1)">Yes' +
                    '</button>' +
                    '</div>',
                plain: true,
                scope: newScope,
                className: 'ngdialog-theme-default'
            }).then(function(value) {

                PortGroupRemove.remove({
                    id: $scope.portGroupMaster.id
                }, function(data) {
                    if (data.responseCode == "ERR0") {
                        console.log("Port Group deleted Successfully")
                        $scope.cancel();
                        $scope.init();
                        $scope.deskTopView = true;
                    } else {
                        console.log("Port Group deletion Failed ", data.responseDescription)
                    }
                }, function(error) {
                    console.log("Port Group deleted Failed : ", error)
                });

            }, function(value) {
                console.log("Port Group deleted cancelled");
            });
        }
    }

    /* Custom Table related functions */

    $scope.rowSelect = function(portGroupMasterObj) {
        if ($rootScope.roleAccess(roleConstant.MASTER_GENERAL_PORT_GROUP_VIEW)) {
            console.log("rowSelect is called.");
            $scope.portGroupMaster = portGroupMasterObj;
            $scope.detailView = true;
            /************************************************************
             * ux - by Muthu
             * reason - ipad compatibility fix
             *
             * *********************************************************/

            var windowInner = $window.innerWidth;
            if (windowInner <= 1199) {
                $scope.deskTopView = false;
            } else {
                $scope.deskTopView = true;
            }

            angular.element($window).bind('resize', function() {
                if ($window.innerWidth >= 1200) {
                    $scope.deskTopView = true;
                } else if ($window.innerWidth <= 1199) {
                    if ($scope.portGroupMaster.id != null) {
                        $scope.deskTopView = false;
                    } else {
                        $scope.deskTopView = true;
                    }
                }
                console.log("window resizing..." + $window.innerWidth);
                $scope.$digest();
            });

            /********************* ux fix ends here *****************************/
        }
    }

    $scope.changepage = function(param) {
        console.log("ChangePage..." + param.page);
        $scope.page = param.page;
        $scope.limit = param.size;
        $scope.search();
    }

    $scope.limitChange = function(item) {
        console.log("LimitChange..." + item);
        $scope.page = 0;
        $scope.limit = item;
        $scope.search();
    }

    $scope.sortChange = function(param) {
        console.log("SortChange..." + param.sortKey);
        $scope.searchDto.orderByType = param.sortOrder.toUpperCase();
        $scope.searchDto.sortByColumn = param.sortKey;
        $scope.search();
    }

    $scope.changeSearch = function(param) {
        console.log("Change Search " + param);
        for (var attrname in param) {
            $scope.searchDto[attrname] = param[attrname];
        }
        $scope.page = 0;
        $scope.cancel();
        $scope.search();
    }


    $scope.search = function() {
        $scope.searchDto.selectedPageNumber = $scope.page;
        $scope.searchDto.recordPerPage = $scope.limit;

        PortGroupSearch.query($scope.searchDto).$promise.then(function(data, status) {
            $scope.totalRecord = data.responseObject.totalRecord;
            $scope.portGroupArr = data.responseObject.searchResult;
            console.log($scope.portGroupArr);
        });
    }


    if ($stateParams.action == "SEARCH") {
        console.log("Port Group Master Search Page....");
        $scope.init();

        $rootScope.breadcrumbArr = [{
                label: "Master",
                state: "layout.portGroup"
            },
            {
                label: "Port Group",
                state: "layout.portGroup"
            }
        ];
    }
}]);