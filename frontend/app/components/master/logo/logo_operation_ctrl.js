app.controller('logoOperationCtrl', ['$rootScope', '$state', '$stateParams', 'LogoView', '$scope', '$timeout', '$http', '$location',
    'ngProgressFactory', 'logoMasterService', 'ngDialog', 'LogoSave', 'LogoUpdate', 'CountryWiseLocation', 'roleConstant',
    function($rootScope, $state, $stateParams, LogoView, $scope, $timeout, $http, $location,
        ngProgressFactory, logoMasterService, ngDialog, LogoSave, LogoUpdate, CountryWiseLocation, roleConstant) {

        $scope.contained_progressbar = ngProgressFactory.createInstance();
        $scope.contained_progressbar.setParent(document.getElementById('logo-panel'));
        $scope.contained_progressbar.setAbsolute();

        $scope.init = function() {
            if ($scope.logoMaster == undefined || $scope.logoMaster == null || $scope.logoMaster.id == null) {
                $scope.logoMaster = {};
                $scope.uploadText = true;
                $scope.isBusy = false;
                $scope.logoMaster.status = 'Active';
            } else {
                $scope.uploadText = false;
                $scope.isBusy = false;
            }
            $scope.setOldDataVal();
        };

        $scope.setOldDataVal = function() {
            $scope.oldData = JSON.stringify($scope.logoMaster);
            console.log("logoMaster Data Loaded");
        }


        $scope.cancel = function() {
            var tempObj = $scope.logoMasterForm;

            if ($scope.oldData != JSON.stringify($scope.logoMaster)) {
                if (Object.keys(tempObj).length) {
                    var newScope = $scope.$new();
                    newScope.errorMessage = $rootScope.nls["ERR200"];
                    ngDialog.openConfirm({
                            template: '<p>{{errorMessage}}</p>' +
                                '<div class="ngdialog-footer">' +
                                ' <button type="button" class="btn btn-default btn-property cancel-btn" ng-click="confirm(2)">No</button>' +
                                '<button type="button" class="btn btn-default btn-property accent-btn " ng-click="confirm(1)">Yes</button>' +
                                '<button type="button" class="btn btn-default btn-property pull-left grey-cancel-btn" ng-click="confirm(3)">Cancel</button>' +
                                '</div>',
                            plain: true,
                            scope: newScope,
                            className: 'ngdialog-theme-default'
                        })
                        .then(
                            function(value) {
                                if (value == 1 &&
                                    $scope.logoMaster.id == null) {
                                    $scope.save();
                                } else if (value == 1 &&
                                    $scope.logoMaster.id != null) {
                                    $scope.update();
                                } else if (value == 2) {
                                    var params = {}
                                    params.submitAction = 'Cancelled';
                                    $state.go("layout.logo", params);
                                } else {
                                    console.log("cancelled");
                                }

                            });
                } else {
                    var params = {}
                    params.submitAction = 'Cancelled';
                    $state.go("layout.logo", params);
                }
            } else {
                var params = {}
                params.submitAction = 'Cancelled';
                $state.go("layout.logo", params);
            }
        }


        $scope.update = function() {
            if ($rootScope.roleAccess(roleConstant.SETUP_LOGO_MASTER_MODIFY)) {
                if ($scope.validateLogoMaster(0)) {
                    console.log("Update Method is called.");
                    $scope.contained_progressbar.start();
                    $scope.associateMapping();
                    LogoUpdate.update($scope.logoMaster).$promise.then(
                        function(data) {
                            if (data.responseCode == 'ERR0') {
                                console.log("Logo Master updated Successfully")
                                logoMasterService.set({});
                                var params = {}
                                params.submitAction = 'Saved';
                                $rootScope.successDesc = $rootScope.nls["ERR401"];
                                $state.go("layout.logo", params);

                            } else {
                                console.log("Logo Master updated Failed " +
                                    data.responseDescription)
                                $scope.revertAssociateMapping
                            }
                            $scope.contained_progressbar.complete();
                            angular.element(".panel-body").animate({
                                scrollTop: 0
                            }, "slow");
                        },
                        function(error) {
                            $scope.revertAssociateMapping
                            console.log("Cost updated Failed : " + error)
                        });
                }


            }

        }
        $scope.save = function() {
            if ($rootScope.roleAccess(roleConstant.SETUP_LOGO_MASTER_CREATE)) {
                console.log("Save Method is called.");
                if ($scope.validateLogoMaster(0)) {
                    $scope.contained_progressbar.start();
                    $scope.associateMapping();
                    LogoSave.save($scope.logoMaster).$promise.then(function(data) {
                        if (data.responseCode == 'ERR0') {
                            logoMasterService.set({});
                            var params = {}
                            params.submitAction = 'Saved';
                            $rootScope.successDesc = $rootScope.nls["ERR400"];
                            $state.go("layout.logo", params);
                        } else {
                            console.log("Tos added Failed " + data.responseDescription)
                            $scope.revertAssociateMapping
                        }
                        $scope.contained_progressbar.complete();
                        angular.element(".panel-body").animate({
                            scrollTop: 0
                        }, "slow");
                    }, function(error) {
                        $scope.revertAssociateMapping
                        console.log("Cost added Failed : " + error)
                    });
                }
            }
        }

        $scope.associateMapping = function() {
            $scope.logoMaster.logo = $scope.logoMaster.encodedLogo;
            $scope.logoMaster.encodedLogo = null;
        }

        $scope.revertAssociateMapping = function() {
            $scope.logoMaster.encodedLogo = $scope.logoMaster.logo;
            $scope.logoMaster.logo = null;
        }


        $scope.setStatus = function(data) {
            if (data == 'Active') {
                return 'activetype';
            } else if (data == 'Block') {
                return 'blockedtype';
            } else if (data == 'Hide') {
                return 'hiddentype';
            }
        }


        $scope.validateLogoMaster = function(validateCode) {

            $scope.errorMap = new Map();

            if (validateCode == 0 || validateCode == 1) {

                if ($scope.logoMaster.locationMaster == null || $scope.logoMaster.locationMaster == undefined || $scope.logoMaster.locationMaster.id.trim == "" || $scope.logoMaster.locationMaster.id == "") {
                    $scope.errorMap.put("locationMaster", $rootScope.nls["ERR80002"]);
                    return false
                }

            }

            if (validateCode == 0 || validateCode == 2) {

                if ($scope.logoMaster.encodedLogo == null || $scope.logoMaster.encodedLogo == undefined || $scope.logoMaster.encodedLogo.trim == "") {
                    $scope.errorMap.put("logo", $rootScope.nls["ERR80003"]);
                    return false
                }

            }


            return true;


        }


        $scope.ajaxLocationEvent = function(object) {
            $scope.searchDto = {};
            $scope.searchDto.keyword = object == null ? "" : object;
            $scope.searchDto.selectedPageNumber = 0;
            $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
            return CountryWiseLocation.fetch({
                "countryId": $rootScope.userProfile.selectedUserLocation.countryMaster.id
            }, $scope.searchDto).$promise.then(
                function(data) {
                    if (data.responseCode == "ERR0") {
                        $scope.locationList = data.responseObject.searchResult;
                        return $scope.locationList;
                    }
                },
                function(errResponse) {
                    console.error('Error while fetching  Locations');
                }
            );


        }

        $scope.selectLocation = function(locationObj) {

            $scope.errorMap = new Map();
            if (locationObj != null && locationObj.status == 'Block') {
                $scope.errorMap.put("locationMaster", "Location was blocked");
                return false
            } else if (locationObj != null && locationObj.status == 'Hide') {
                $scope.errorMap.put("locationMaster", "Location was hidden");
                return false
            } else {
                $scope.validateLogoMaster(1);
            }

        };

        $scope.uploadImage = function(file) {

            if (file != null || file != undefined) {
                var fileName = file.name;
                var extention = fileName.split('.')[fileName.split('.').length - 1].toLowerCase();
                console.log("file extention ", extention);
                var valid = false;
                if (extention == 'jpeg' || extention == 'jpg' || extention == 'png' || extention == 'gif') {
                    valid = true;
                }

                var reader = new FileReader();

                reader.onload = function(event) {
                    var contents = event.target.result;
                    var uploadedFile = btoa(contents);

                    //content = content.replace("data:image/png;base64,","");

                    console.log("File contents : " + uploadedFile);
                    $scope.logoMaster.encodedLogo = uploadedFile;
                    $scope.uploadText = false;
                    $scope.isBusy = true;
                    $timeout(function() {
                        $scope.isBusy = false;
                    }, 1000);

                };
                reader.readAsBinaryString(file);
            }


        }

        $scope.locationRender = function(item) {
            return {
                label: item.locationName,
                item: item
            }
        }


        // History Start

        $scope.getLogoById = function(logoId) {
            console.log("getLogoById ", logoId);

            LogoView.get({
                id: logoId
            }, function(data) {
                if (data.responseCode == "ERR0") {
                    console.log("Successful while getting logo.", data)
                    $scope.logoMaster = data.responseObject;
                    $scope.init();
                    $scope.setOldDataVal();
                }
            }, function(error) {
                console.log("Error while getting logo.", error)
            });

        }


        //On leave the Unfilled logo Form Broadcast then event will happen in StateChangeListener(BroadCaster)-app_state_route.js

        $scope.$on('addLogoEvent', function(events, args) {

            $rootScope.unfinishedData = $scope.logoMaster;
            $rootScope.category = "Master";
            $rootScope.unfinishedFormTitle = "Logo (New)";
            if ($scope.logoMaster != undefined && $scope.logoMaster != null && $scope.logoMaster.locationMaster != undefined &&
                $scope.logoMaster.locationMaster != null && $scope.logoMaster.locationMaster != "") {
                $rootScope.subTitle = $scope.logoMaster.locationMaster.locationName;
            } else {
                $rootScope.subTitle = "Unknown Logo"
            }
        })

        $scope.$on('editLogoEvent', function(events, args) {
            $rootScope.unfinishedData = $scope.logoMaster;
            $rootScope.category = "Master";
            $rootScope.unfinishedFormTitle = "Logo Edit";
            if ($scope.logoMaster != undefined && $scope.logoMaster != null && $scope.logoMaster.locationMaster != undefined &&
                $scope.logoMaster.locationMaster != null && $scope.logoMaster.locationMaster != "") {
                $rootScope.subTitle = $scope.logoMaster.locationMaster.locationName;
            } else {
                $rootScope.subTitle = "Unknown Logo"
            }
        })


        $scope.$on('addLogoEventReload', function(e, confirmation) {
            console.log("addLogoEventReload is called ", $scope.logoMaster);
            confirmation.message = "";
            localStorage.reloadFormData = JSON.stringify($scope.logoMaster);
            localStorage.isLogoReloaded = "YES";
            e.preventDefault();
        });


        $scope.$on('editLogoEventReload', function(e, confirmation) {
            confirmation.message = "";
            localStorage.reloadFormData = JSON.stringify($scope.logoMaster);
            localStorage.isLogoReloaded = "YES";
            e.preventDefault();
        });


        $scope.isReloaded = localStorage.isLogoReloaded;

        if ($stateParams.action == "ADD") {
            if ($stateParams.fromHistory === 'Yes') {
                if ($scope.isReloaded == "YES") {
                    console.log("Add History Reload...")
                    $scope.isReloaded = "NO";
                    localStorage.isLogoReloaded = "NO";
                    $scope.logoMaster = JSON.parse(localStorage.reloadFormData);
                    $scope.init();
                } else {
                    console.log("Add History NotReload...")
                    $scope.logoMaster = $rootScope.selectedUnfilledFormData;
                    $scope.init();
                }
                $scope.oldData = "";
            } else {
                if ($scope.isReloaded == "YES") {
                    $scope.isReloaded = "NO";
                    localStorage.isLogoReloaded = "NO";
                    $scope.logoMaster = JSON.parse(localStorage.reloadFormData);
                    console.log("Add NotHistory Reload...");
                    $scope.init();
                    $scope.oldData = "";
                } else {
                    console.log("Add NotHistory NotReload...")
                    $scope.init();
                }
            }
            $rootScope.breadcrumbArr = [{
                label: "Master",
                state: "layout.logo"
            }, {
                label: "Logo",
                state: "layout.logo"
            }, {
                label: "Add Logo",
                state: null
            }];

        } else {
            if ($stateParams.fromHistory === 'Yes') {
                if ($scope.isReloaded == "YES") {
                    $scope.isReloaded = "NO";
                    localStorage.isLogoReloaded = "NO";
                    $scope.logoMaster = JSON.parse(localStorage.reloadFormData);
                    $scope.init();
                } else {
                    $scope.logoMaster = $rootScope.selectedUnfilledFormData;
                    $scope.init();
                }
                $scope.oldData = "";
            } else {
                if ($scope.isReloaded == "YES") {
                    $scope.isReloaded = "NO";
                    localStorage.isLogoReloaded = "NO";
                    $scope.logoMaster = JSON.parse(localStorage.reloadFormData);
                    $scope.init();
                    $scope.oldData = "";
                } else {
                    $scope.getLogoById($stateParams.logoId);
                }
            }
            $rootScope.breadcrumbArr = [{
                label: "Master",
                state: "layout.logo"
            }, {
                label: "Logo",
                state: "layout.logo"
            }, {
                label: "Edit Logo",
                state: null
            }];
        }

        // History End

    }
]);