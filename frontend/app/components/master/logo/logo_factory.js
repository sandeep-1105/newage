(function() {

	app.factory("LogoSearch", ['$resource', function($resource) {
		return $resource("/api/v1/logomaster/get/search", {}, {
			query : {
				method : 'POST'
			}
		});
	}]);
	
	app.factory("LogoSearchKeyword", ['$resource', function($resource) {
		return $resource("/api/v1/logomaster/get/search/keyword", {}, {
			fetch : {
				method : 'POST'
			}
		});
	}]);

	app.factory("LogoSave", ['$resource', function($resource) {
		return $resource("/api/v1/logomaster/create", {}, {
			save : {
				method : 'POST'
			}
		});
	}]);

	app.factory("LogoUpdate", ['$resource', function($resource) {
		return $resource("/api/v1/logomaster/update", {}, {
			update : {
				method : 'POST'
			}
		});
	}]);

	app.factory("LogoView", ['$resource', function($resource) {
		return $resource("/api/v1/logomaster/get/id/:id", {}, {
			get : {
				method : 'GET',
				params : {
					id : ''
				},
				isArray : false
			}
		});
	}]);
	
	
	app.factory("LogoGetByLocation", ['$resource', function($resource) {
		return $resource("/api/v1/logomaster/get/bylocation/:locationid", {}, {
			get : {
				method : 'GET',
				params : {
					locationid : ''
				},
				isArray : false
			}
		});
	}]);
	
	
	app.factory("LogoRemove", ['$resource', function($resource) {
		return $resource("/api/v1/logomaster/delete/:id", {}, {
			remove : {
				method : 'DELETE',
				params : {
					id : ''
				},
				isArray : false
			}
		});
	}]);
	
})();
