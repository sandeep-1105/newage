app.controller('logoCtrl', ['$rootScope', '$scope', '$state', '$stateParams', '$window', 'ngDialog',
    'LogoSearch', 'LogoView', 'LogoRemove', 'RecentHistorySaveService', 'roleConstant',
    function($rootScope, $scope, $state, $stateParams, $window, ngDialog,
        LogoSearch, LogoView, LogoRemove, RecentHistorySaveService, roleConstant) {


        /************************************************************
         * ux - by Muthu
         * reason - ipad compatibility fix
         *
         * *********************************************************/
        $scope.deskTopView = true;

        /********************* ux fix ends here *****************************/

        $scope.logoHeadArr = [{
                "name": "#",
                "width": "col-xs-0half",
                "model": "no",
                "search": false,
            },
            {
                "name": "Location",
                "width": "col-xs-7",
                "model": "locationMaster.locationName",
                "search": true,
                "wrap_cell": true,
                "type": "text",
                "sort": true,
                "key": "searchLocationName"
            },
            {
                "name": "Status",
                "width": "col-xs-3",
                "model": "status",
                "search": true,
                "type": "drop",
                "sort": true,
                "data": $rootScope.enum['LovStatus'],
                "key": "searchStatus"
            }
        ]

        $scope.sortSelection = {
            sortKey: "locationMaster.locationName",
            sortOrder: "asc"
        }

        $scope.logoMaster = {};

        $scope.searchDto = {};
        $scope.limitArr = [10, 15, 20];
        $scope.page = 0;
        $scope.limit = 10;


        $scope.init = function() {
            console.log("Logo Master Init method called.");
            $scope.logoMaster = {};
            $scope.searchDto = {};
            $scope.search();
        };

        $scope.limitChange = function(item) {
            console.log("Limit Change is called...", item);
            $scope.page = 0;
            $scope.limit = item;
            $scope.cancel();
            $scope.search();
        }

        $scope.rowSelect = function(row) {
            if ($rootScope.roleAccess(roleConstant.SETUP_LOGO_MASTER_VIEW)) {
                $scope.Logs = false;
                $scope.showHistory = false;

                /************************************************************
                 * ux - by Muthu
                 * reason - ipad compatibility fix
                 *
                 * *********************************************************/

                var windowInner = $window.innerWidth;
                if (windowInner <= 1199) {
                    $scope.deskTopView = false;

                } else {
                    $scope.deskTopView = true;
                }
                angular.element($window).bind('resize', function() {
                    if ($window.innerWidth >= 1200) {
                        $scope.deskTopView = true;
                    } else if ($window.innerWidth <= 1199) {
                        if ($scope.logoMaster.id !== false) {
                            $scope.deskTopView = false;
                        } else {
                            $scope.deskTopView = true;
                        }
                    }

                    console.log("window resizing..." + $window.innerWidth);
                    $scope.$digest();
                });

                /********************* ux fix ends here *****************************/

                LogoView.get({
                    id: row.id
                }, function(data) {
                    if (data.responseCode == "ERR0") {
                        console.log("logoMasterMaster view Successfully")
                        $scope.logoMaster = data.responseObject;
                    } else {
                        console.log("logoMasterMaster view Failed ", data.responseDescription)
                    }
                }, function(error) {
                    console.log("logoMasterMaster view Failed : ", error)
                });

            }
        }

        $scope.addLogo = function() {
            if ($rootScope.roleAccess(roleConstant.SETUP_LOGO_MASTER_CREATE)) {
                $state.go("layout.addLogo");
            }
        }

        $scope.editLogo = function() {
            if ($rootScope.roleAccess(roleConstant.SETUP_LOGO_MASTER_MODIFY)) {
                $state.go("layout.editLogo", {
                    logoId: $scope.logoMaster.id
                });
            }
        }


        $scope.changeSearch = function(param) {
            console.log("Change Search", param)
            for (var attrname in param) {
                $scope.searchDto[attrname] = param[attrname];
            }
            $scope.page = 0;
            $scope.cancel();
            $scope.search();
            console.log("change search", $scope.searchDto);
        }

        $scope.sortChange = function(param) {
            $scope.searchDto.orderByType = param.sortOrder.toUpperCase();
            $scope.searchDto.sortByColumn = param.sortKey;
            $scope.cancel();
            $scope.search();
        }

        $scope.changepage = function(param) {
            console.log("Change Page Called.", param);
            $scope.page = param.page;
            $scope.limit = param.size;
            $scope.search();
        }


        $scope.cancel = function() {
            $scope.showLogs = false;
            $scope.logoMaster = {};

            /************************************************************
             * ux - by Muthu
             * reason - ipad compatibility fix
             *
             * *********************************************************/
            $scope.deskTopView = true;

            /********************* ux fix ends here *****************************/
        }


        $scope.deleteLogo = function() {
            if ($rootScope.roleAccess(roleConstant.SETUP_LOGO_MASTER_DELETE)) {
                var newScope = $scope.$new();
                newScope.errorMessage = $rootScope.nls["ERR244"];
                ngDialog
                    .openConfirm({
                        template: '<p>{{errorMessage}}</p>' +
                            '<div class="ngdialog-footer">' +
                            '<button type="button" class="btn btn-default btn-property cancel-btn" ng-click="closeThisDialog(0)">Cancel' +
                            '<button type="button" class="btn btn-primary btn-property accent-btn" ng-click="confirm(1)">Yes' +
                            '</button></div>',
                        plain: true,
                        scope: newScope,
                        className: 'ngdialog-theme-default'
                    })
                    .then(
                        function(value) {
                            LogoRemove
                                .remove({
                                        id: $scope.logoMaster.id
                                    },
                                    function(data) {
                                        if (data.responseCode == 'ERR0') {
                                            console.log("Logo Master deleted Successfully")
                                            $scope.init();
                                            $scope.deskTopView = true;
                                        } else {
                                            console.log("Logo Master deleted Failed " + data.responseDescription)
                                        }
                                    },
                                    function(error) {
                                        console.log("Logo Master deleted Failed : " + error)
                                    });
                        },
                        function(value) {
                            console.log("deleted cancelled");
                        });
            }
        }


        $scope.search = function() {
            $scope.logoMaster = {};
            $scope.searchDto.selectedPageNumber = $scope.page;
            $scope.searchDto.recordPerPage = $scope.limit;
            $scope.logoArr = [];
            LogoSearch.query($scope.searchDto).$promise.then(function(
                data, status) {

                $scope.totalRecord = data.responseObject.totalRecord;
                var tempArr = [];
                var resultArr = [];
                tempArr = data.responseObject.searchResult;
                console.log("tempArr", tempArr);
                var tempObj = {};
                angular.forEach(tempArr, function(item, index) {
                    tempObj = item;
                    tempObj.no = (index + 1) + ($scope.page * $scope.limit);
                    resultArr.push(tempObj);
                    tempObj = {};
                });
                $scope.logoArr = resultArr;
            });

        }
        switch ($stateParams.action) {
            case "SEARCH":
                $scope.init();
                $rootScope.breadcrumbArr = [{
                        label: "Master",
                        state: "layout.logo"
                    },
                    {
                        label: "Logo",
                        state: "layout.logo"
                    }
                ];
                break;
        }


    }
]);