app.controller('ReferenceTypeController',['$rootScope', '$scope', '$window', 'ngTableParams', 'ngDialog', '$stateParams', '$state',
    'ReferenceTypeSearch', 'ReferenceTypeRemove', 'roleConstant', 'Notification',
    function($rootScope, $scope, $window, ngTableParams, ngDialog, $stateParams, $state,
    ReferenceTypeSearch, ReferenceTypeRemove, roleConstant, Notification) {

    console.log("Reference Type controller loaded..");
    /************************************************************
     * ux - by Muthu
     * reason - ipad compatibility fix
     *
     * *********************************************************/
    $scope.deskTopView = true;

    /********************* ux fix ends here *****************************/


    $scope.init = function() {

        console.log("Reference Type Init method called.");

        $rootScope.setNavigate1("Master");
        $rootScope.setNavigate2("Reference Type");

        $scope.limitArr = [10, 15, 20];
        $scope.page = 0;
        $scope.limit = 10;
        $scope.showHistory = false;

        $scope.searchDto = {};
        $scope.search();
    };


    $scope.search = function() {
        console.log("Reference Type Search loaded.....");

        $scope.searchDto.selectedPageNumber = $scope.page;
        $scope.searchDto.recordPerPage = $scope.limit;

        $scope.referenceTypeArr = [];

        ReferenceTypeSearch.query($scope.searchDto).$promise.then(function(data, status) {

            $scope.totalRecord = data.responseObject.totalRecord;

            var tempArr = [];
            var resultArr = [];

            tempArr = data.responseObject.searchResult;

            console.log("tempArr", tempArr);
            var tempObj = {};

            angular.forEach(tempArr, function(item, index) {
                tempObj = item;
                tempObj.no = (index + 1) + ($scope.page * $scope.limit);
                resultArr.push(tempObj);
                tempObj = {};
            });

            $scope.referenceTypeArr = resultArr;
        });
    }



    $scope.referenceTypeHeadArr = [{
        "name": "#",
        "width": "col-xs-0half",
        "model": "no",
        "search": false,
    }, {
        "name": "Name",
        "width": "col-xs-7",
        "model": "referenceTypeName",
        "search": true,
        "wrap_cell": true,
        "type": "text",
        "sort": true,
        "key": "searchReferenceTypeName"
    }, {
        "name": "Code",
        "width": "col-xs-2half",
        "model": "referenceTypeCode",
        "search": true,
        "type": "text",
        "sort": true,
        "key": "searchReferenceTypeCode"
    }, {
        "name": "Status",
        "width": "w120px",
        "model": "status",
        "search": true,
        "sort": true,
        "type": "drop",
        "data": $rootScope.enum['LovStatus'],
        "key": "searchStatus"
    }];


    $scope.sortSelection = {
        sortKey: "referenceTypeName",
        sortOrder: "asc"
    }

    $scope.changeSearch = function(param) {
        console.log("Change Search", param);
        for (var attrname in param) {
            $scope.searchDto[attrname] = param[attrname];
        }
        $scope.page = 0;
        console.log("change search", $scope.searchDto);
        $scope.search();
    }

    $scope.sortChange = function(param) {
        console.log("Sort Change Called.", param);
        $scope.searchDto.orderByType = param.sortOrder.toUpperCase();
        $scope.searchDto.sortByColumn = param.sortKey;
        $scope.search();
    }

    $scope.changePage = function(param) {
        console.log("Change Page Called.", param);
        $scope.page = param.page;
        $scope.limit = param.size;
        $scope.search();
    }

    $scope.limitChange = function(item) {
        console.log("Limit Change is called...", item);
        $scope.page = 0;
        $scope.limit = item;
        $scope.search();
    }


    $scope.rowSelect = function(data) {
        if ($rootScope.roleAccess(roleConstant.MASTER_OCEAN_REFERENCE_TYPE_VIEW)) {
            $scope.referenceTypeMaster = data;
            $scope.showHistory = false;

            /************************************************************
             * ux - by Muthu
             * reason - ipad compatibility fix
             *
             * *********************************************************/

            var windowInner = $window.innerWidth;
            if (windowInner <= 1199) {
                $scope.deskTopView = false;

            } else {
                $scope.deskTopView = true;
            }
            angular.element($window).bind('resize', function() {
                if ($window.innerWidth >= 1200) {
                    $scope.deskTopView = true;
                } else if ($window.innerWidth <= 1199) {
                    if ($scope.referenceTypeMaster.id != null) {
                        $scope.deskTopView = false;
                    } else {
                        $scope.deskTopView = true;
                    }
                }

                console.log("window resizing..." + $window.innerWidth);
                $scope.$digest();
            });

            /********************* ux fix ends here *****************************/
        }
    }


    $scope.cancel = function() {

        console.log("Cancel Button Pressed.....");

        $scope.referenceTypeMaster = {};
        $scope.showHistory = false;
        /************************************************************
         * ux - by Muthu
         * reason - ipad compatibility fix
         *
         * *********************************************************/
        $scope.deskTopView = true;

        /********************* ux fix ends here *****************************/
    };


    $scope.addNewReferenceTypeMaster = function() {
        if ($rootScope.roleAccess(roleConstant.MASTER_OCEAN_REFERENCE_TYPE_CREATE)) {
            console.log("Add Button is Pressed.")
            $state.go("layout.addReferenceType");
        }
    }

    $scope.editReferenceTypeMaster = function() {
        if ($rootScope.roleAccess(roleConstant.MASTER_OCEAN_REFERENCE_TYPE_MODIFY)) {
            console.log("Edit Button is Pressed.");
            $state.go("layout.editReferenceType", {
                id: $scope.referenceTypeMaster.id
            });
        }
    };

    $scope.deleteReferenceTypeMaster = function() {
        if ($rootScope.roleAccess(roleConstant.MASTER_OCEAN_REFERENCE_TYPE_DELETE)) {
            ngDialog.openConfirm({
                template: '<p>Are you sure you want to delete selected reference type?</p>' +
                    '<div class="ngdialog-footer">' +
                    '<button type="button" class="btn btn-default btn-property cancel-btn" ng-click="closeThisDialog(0)">No' +
                    '<button type="button" class="btn btn-primary btn-property accent-btn" ng-click="confirm(1)">Yes' +
                    '</button>' +
                    '</div>',
                plain: true,
                className: 'ngdialog-theme-default'
            }).then(function(value) {
                ReferenceTypeRemove.remove({
                        id: $scope.referenceTypeMaster.id
                    }, function(data) {
                        if (data.responseCode == 'ERR0') {
                            Notification.success($rootScope.nls["ERR402"]);
                            console.log("Reference Type deleted Successfully");
                            $scope.init();
                            $scope.cancel();
                            $scope.deskTopView = true;
                        } else {
                            console.log("Reference Type deletion Failed ", data.responseDescription)
                        }
                    },
                    function(error) {
                        console.log("Reference Type deletion Failed : ", error);
                    });
            }, function(value) {
                console.log("deletion cancelled");
            });
        }
    }


    switch ($stateParams.action) {
        case "SEARCH":
            $scope.init();
            $rootScope.breadcrumbArr = [{
                    label: "Master",
                    state: "layout.referenceType"
                },
                {
                    label: "Reference",
                    state: null
                }
            ];
            break;
    }

}]);