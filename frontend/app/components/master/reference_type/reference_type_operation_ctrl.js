app.controller('ReferenceTypeEditController',['$rootScope', '$scope', 'ngProgressFactory', 'ngDialog',
    '$state', '$stateParams', 'ReferenceTypeView', 'ReferenceTypeSave', 'ReferenceTypeUpdate', 'appConstant', 'CommonValidationService', 
    function($rootScope, $scope, ngProgressFactory, ngDialog,
    $state, $stateParams, ReferenceTypeView, ReferenceTypeSave, ReferenceTypeUpdate, appConstant, CommonValidationService) {


    $scope.contained_progressbar = ngProgressFactory.createInstance();
    $scope.contained_progressbar.setParent(document.getElementById('reference-panel'));
    $scope.contained_progressbar.setAbsolute();


    $scope.init = function(acFlag) {

        if ($scope.referenceTypeMaster == undefined || $scope.referenceTypeMaster == null) {
            $scope.referenceTypeMaster = {};
        }

        if ($scope.referenceTypeMaster.id == null)
            $scope.referenceTypeMaster.status = 'Active';

        if (acFlag != undefined && acFlag != null && acFlag === 0) {
            $scope.setOldDataVal();
        }
    };


    $scope.setOldDataVal = function() {
        $scope.oldData = JSON.stringify($scope.referenceTypeMaster);
        console.log("partyTypeMaster Data Loaded");
    }

    $scope.setStatus = function(data) {
        if (data == 'Active') {
            return 'activetype';
        } else if (data == 'Block') {
            return 'blockedtype';
        } else if (data == 'Hide') {
            return 'hiddentype';
        }
    }



    $scope.cancel = function() {

        var tempObj = $scope.referenceTypeMaster;
        if ($scope.oldData != JSON.stringify($scope.referenceTypeMaster)) {
            if (Object.keys(tempObj).length) {
                var newScope = $scope.$new();
                newScope.errorMessage = $rootScope.nls["ERR200"];
                ngDialog.openConfirm({
                    template: '<p>{{errorMessage}}</p>' +
                        '<div class="ngdialog-footer">' +
                        ' <button type="button" class="btn btn-default btn-property cancel-btn" ng-click="confirm(2)">No</button>' +
                        '<button type="button" class="btn btn-default btn-property accent-btn " ng-click="confirm(1)">Yes</button>' +
                        '<button type="button" class="btn btn-default btn-property pull-left grey-cancel-btn" ng-click="confirm(3)">Cancel</button>' +
                        '</div>',
                    plain: true,
                    scope: newScope,
                    className: 'ngdialog-theme-default'
                }).then(function(value) {
                    if (value == 1 && $scope.referenceTypeMaster.id == null) {
                        $scope.save();
                    } else if (value == 1 && $scope.referenceTypeMaster.id != null) {
                        $scope.update();
                    } else if (value == 2) {
                        var params = {};
                        params.submitAction = 'Cancelled';
                        $state.go("layout.referenceType", params);
                    } else {
                        console.log("cancelled");
                    }
                });
            } else {
                var params = {};
                params.submitAction = 'Cancelled';
                $state.go("layout.referenceType", params);
            }
        } else {
            var params = {};
            params.submitAction = 'Cancelled';
            $state.go("layout.referenceType", params);
        }

    }



    $scope.save = function() {

        console.log("Save Method is called.");
        if ($scope.validate(0)) {
            $scope.contained_progressbar.start();

            ReferenceTypeSave.save($scope.referenceTypeMaster).$promise.then(function(data) {
                if (data.responseCode == 'ERR0') {
                    console.log("referenceTypeMaster saved Successfully")
                    $rootScope.successDesc = $rootScope.nls["ERR400"];
                    var params = {}
                    params.submitAction = 'Saved';
                    $state.go("layout.referenceType", params);
                } else {
                    console.log("referenceTypeMaster added Failed ", data.responseDescription)
                }
                $scope.contained_progressbar.complete();
                angular.element(".panel-body").animate({
                    scrollTop: 0
                }, "slow");
            }, function(error) {
                console.log("referenceTypeMaster added Failed : ", error)
            });
        } else {
            console.log("Invalid Form Submission.");
        }
    }




    $scope.update = function() {

        console.log("Update Method is called.");
        if ($scope.validate(0)) {
            $scope.contained_progressbar.start();
            ReferenceTypeUpdate.update($scope.referenceTypeMaster).$promise.then(function(data) {
                if (data.responseCode == 'ERR0') {
                    console.log("referenceTypeMaster updated Successfully")
                    $rootScope.successDesc = $rootScope.nls["ERR401"];
                    var params = {}
                    params.submitAction = 'Saved';
                    $state.go("layout.referenceType", params);
                } else {
                    console.log("referenceTypeMaster updated Failed ", data.responseDescription)
                }

                $scope.contained_progressbar.complete();
                angular.element(".panel-body").animate({
                    scrollTop: 0
                }, "slow");

            }, function(error) {
                console.log("referenceTypeMaster updated Failed : " + error)
            });
        } else {
            console.log("Invalid Form Submission.");
        }
    }




    $scope.getById = function(id) {

        console.log("getById ", id);

        ReferenceTypeView.get({
            id: id
        }, function(data) {
            if (data.responseCode == "ERR0") {
                console.log("Successful while getting referenceTypeMaster.", data)
                $scope.referenceTypeMaster = data.responseObject;
                $scope.init();

                $scope.setOldDataVal();
            }
        }, function(error) {
            console.log("Error while getting referenceTypeMaster.", error)
        });


    }



    $scope.validate = function(validateCode) {
        console.log("Validate Called " + validateCode);

        $scope.errorMap = new Map();

        if (validateCode == 0 || validateCode == 1) {

            if ($scope.referenceTypeMaster.referenceTypeName == undefined ||
                $scope.referenceTypeMaster.referenceTypeName == null ||
                $scope.referenceTypeMaster.referenceTypeName == "") {
                $scope.errorMap.put("referenceTypeName", $rootScope.nls["ERR1927"]);

                return false;
            } else {
                if (!CommonValidationService.checkRegExp(appConstant.Reg_Exp_Master_REFERENCE_TYPE_NAME, $scope.referenceTypeMaster.referenceTypeName)) {
                    $scope.errorMap.put("referenceTypeName", $rootScope.nls["ERR1928"]);
                    return false;
                }

            }
        }


        if (validateCode == 0 || validateCode == 2) {
            if ($scope.referenceTypeMaster.referenceTypeCode == undefined ||
                $scope.referenceTypeMaster.referenceTypeCode == null ||
                $scope.referenceTypeMaster.referenceTypeCode == "") {
                $scope.errorMap.put("referenceTypeCode", $rootScope.nls["ERR1925"]);
                return false;
            } else {
                if (!CommonValidationService.checkRegExp(appConstant.Reg_Exp_Master_REFERENCE_TYPE_CODE, $scope.referenceTypeMaster.referenceTypeCode)) {
                    $scope.errorMap.put("referenceTypeCode", $rootScope.nls["ERR1926"]);
                    return false;
                }
            }
        }

        /*	if (validateCode == 0 || validateCode == 3) {
        		if ($scope.referenceTypeMaster.referenceType == undefined
        				|| $scope.referenceTypeMaster.referenceType == null
        				|| $scope.referenceTypeMaster.referenceType == "") {
        			$scope.errorMap.put("referenceType",$rootScope.nls["ERR1932"]);
        			return false;
        		}
        	}
        */
        if (validateCode == 0 || validateCode == 3) {}

        return true;

    }



    $scope.$on('addReferenceTypeEvent', function(events, args) {

        $rootScope.unfinishedData = $scope.referenceTypeMaster;
        $rootScope.category = "Master";
        $rootScope.unfinishedFormTitle = "Reference Type (New)";
        if ($scope.referenceTypeMaster != undefined && $scope.referenceTypeMaster != null && $scope.referenceTypeMaster.referenceTypeName != undefined &&
            $scope.referenceTypeMaster.referenceTypeName != null && $scope.referenceTypeMaster.referenceTypeName != "") {
            $rootScope.subTitle = $scope.referenceTypeMaster.referenceTypeName;
        } else {
            $rootScope.subTitle = "Unknown Reference Type"
        }
    })

    $scope.$on('editReferenceTypeEvent', function(events, args) {

        $rootScope.unfinishedData = $scope.referenceTypeMaster;
        $rootScope.category = "Master";
        $rootScope.unfinishedFormTitle = "Reference Type Edit";
        if ($scope.referenceTypeMaster != undefined && $scope.referenceTypeMaster != null && $scope.referenceTypeMaster.referenceTypeName != undefined &&
            $scope.referenceTypeMaster.referenceTypeName != null && $scope.referenceTypeMaster.referenceTypeName != "") {
            $rootScope.subTitle = $scope.referenceTypeMaster.referenceTypeName;
        } else {
            $rootScope.subTitle = "Unknown Reference Type"
        }
    })


    $scope.$on('addReferenceTypeEventReload', function(e, confirmation) {
        confirmation.message = "";
        localStorage.reloadFormData = JSON.stringify($scope.referenceTypeMaster);
        localStorage.isReferenceTypeReloaded = "YES";
        e.preventDefault();
    });


    $scope.$on('editReferenceTypeEventReload', function(e, confirmation) {
        confirmation.message = "";
        localStorage.reloadFormData = JSON.stringify($scope.referenceTypeMaster);
        localStorage.isReferenceTypeReloaded = "YES";
        e.preventDefault();
    });


    $scope.isReloaded = localStorage.isReferenceTypeReloaded;

    if ($stateParams.action == "ADD") {
        if ($stateParams.fromHistory === 'Yes') {
            if ($scope.isReloaded == "YES") {
                console.log("Add History Reload...")
                $scope.isReloaded = "NO";
                localStorage.isReferenceTypeReloaded = "NO";
                $scope.referenceTypeMaster = JSON.parse(localStorage.reloadFormData);
                $scope.init();
            } else {
                console.log("Add History NotReload...")
                $scope.referenceTypeMaster = $rootScope.selectedUnfilledFormData;
                $scope.init();
            }
            $scope.oldData = "";
        } else {
            if ($scope.isReloaded == "YES") {
                $scope.isReloaded = "NO";
                localStorage.isReferenceTypeReloaded = "NO";
                $scope.referenceTypeMaster = JSON.parse(localStorage.reloadFormData);
                $scope.init();
                $scope.oldData = "";
                console.log("Add NotHistory Reload...")
            } else {
                console.log("Add NotHistory NotReload...")
                $scope.init(0);
            }
        }
        $rootScope.breadcrumbArr = [{
                label: "Master",
                state: "layout.referenceType"
            },
            {
                label: "Reference",
                state: "layout.referenceType"
            },
            {
                label: "Add Reference",
                state: null
            }
        ];

    } else {
        if ($stateParams.fromHistory === 'Yes') {
            if ($scope.isReloaded == "YES") {
                $scope.isReloaded = "NO";
                localStorage.isReferenceTypeReloaded = "NO";
                $scope.referenceTypeMaster = JSON.parse(localStorage.reloadFormData);
                $scope.init();
            } else {
                $scope.referenceTypeMaster = $rootScope.selectedUnfilledFormData;
                $scope.init();
            }
            $scope.oldData = "";
        } else {
            if ($scope.isReloaded == "YES") {
                $scope.isReloaded = "NO";
                localStorage.isReferenceTypeReloaded = "NO";
                $scope.referenceTypeMaster = JSON.parse(localStorage.reloadFormData);
                $scope.init();
                $scope.oldData = "";
            } else {
                $scope.getById($stateParams.id);

            }
        }
        $rootScope.breadcrumbArr = [{
                label: "Master",
                state: "layout.referenceType"
            },
            {
                label: "Reference",
                state: "layout.referenceType"
            },
            {
                label: "Edit Reference",
                state: null
            }
        ];
    }

}]);