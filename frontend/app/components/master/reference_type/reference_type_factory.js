/**
 * ReferenceTypeMaster 
 */

(function() {
	
	app.factory("ReferenceTypeSearchByKeyword",['$resource', function($resource) {
		return $resource("/api/v1/referencetype/get/search/keyword", {}, {
			fetch : {
				method : 'POST'
			}
		});
	}]);
	
	app.factory("ReferenceTypeSearch",['$resource', function($resource) {
		return $resource("/api/v1/referencetype/get/search", {}, {
			query : {
				method : 'POST'
			}
		});
	}]);
	
	app.factory("ReferenceTypeSave",['$resource', function($resource) {
		return $resource("/api/v1/referencetype/create", {}, {
			save : {
				method : 'POST'
			}
		});
	}]);

	app.factory("ReferenceTypeUpdate",['$resource', function($resource) {
		return $resource("/api/v1/referencetype/update", {}, {
			update : {
				method : 'POST'
			}
		});
	}]);

	app.factory("ReferenceTypeView",['$resource', function($resource) {
		return $resource("/api/v1/referencetype/get/id/:id", {}, {
			get : {
				method : 'GET',
				params : {
					id : ''
				},
				isArray : false
			}
		});
	}]);
	
	app.factory("MandatoryReferenceTypeByKey",['$resource', function($resource) {
		return $resource("/api/v1/referencetype/get/:key", {}, {
			get : { method : 'GET', params : { key : '' }, isArray : false }
		});
	}]);

	
	app.factory("ReferenceTypeRemove",['$resource', function($resource) {
		return $resource("/api/v1/referencetype/delete/:id", {}, {
			remove : {
				method : 'DELETE',
				params : {
					id : ''
				},
				isArray : false
			}
		});
	}]);
	
	
})();
