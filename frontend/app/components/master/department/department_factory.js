app.factory('DepartmentFactory',['$resource', function($resource) {
	return {
		findById : $resource('/api/v1/departmentmaster/get/id/:id', {}, {
			 query : {method : 'GET', params : {id : ''}, isArray : false}
		}),
		save : $resource('/api/v1/departmentmaster/create', {}, {
			query : {method : 'POST'}
		}),
		remove : $resource('/api/v1/departmentmaster/delete/:id', {}, {
			query : {method : 'DELETE', params : {id : ''}}
		}),
		getAll : $resource('/api/v1/departmentmaster/get/search/keyword', {}, {
			query : {method : 'POST'}
		}),
		search : $resource('/api/v1/departmentmaster/get/search', {}, {
			fetch : {method : 'POST'}
		})
	};
}])