app.controller('DepartmentMasterCtrl', ['$rootScope', '$scope', '$window', '$state', '$stateParams', 'ngDialog','DepartmentFactory', 'roleConstant','Notification',
     function($rootScope, $scope, $window, $state, $stateParams, ngDialog, DepartmentFactory,roleConstant,Notification) {

	/************************************************************
	 * ux - by Muthu
	 * reason - ipad compatibility fix
	 *
	 * *********************************************************/
	$scope.deskTopView = true;

	/********************* ux fix ends here *****************************/
	
	$scope.limitArr = [10,15,20];
	$scope.page = 0;
	$scope.limit = 10;
	
	$scope.departmentHeadArr=[ 
	                {
						"name" : "#",
						"width" : "col-xs-0half",
						"model" : "no",
						"search" : false,
					}, {
						"name" : "Name",
						"width" : "col-xs-3",
						"model" : "departmentName",
						"search" : true,
						"wrap_cell" : true,
						"type" : "text",
						"sort" : true,
						"key" : "searchDepartmentName"

					}, {
						"name" : "Code",
						"width" : "col-xs-1half",
						"model" : "departmentCode",
						"search" : true,
						"type" : "text",
						"sort" : true,
						"key" : "searchDepartmentCode"
					}, {
						"name" : "Department Head",
						"width" : "col-xs-1half",
						"model" : "departmentHead.employeeName",
						"search" : true,
						"type" : "text",
						"sort" : true,
						"key" : "searchDepartmentHead"
					}, {
						"name" : "Status",
						"width" : "col-xs-2",
						"model" : "status",
						"search" : true,
						"type" : "drop",
						"sort" : true,
						"data" : $rootScope.enum['LovStatus'],
						"key" : "searchStatus"
					}];
	
	

	
	$scope.sortSelection = {
		sortKey:"departmentName",
        sortOrder:"asc"
	}
	
	$scope.init = function () {
		$scope.searchDto = {};
		$scope.detailView = false;
		$scope.search();
	};

	$scope.cancel = function () {
		$scope.departmentMaster = {};
		$scope.showHistory = false;
		$scope.detailView = false;
		/************************************************************
		 * ux - by Muthu
		 * reason - ipad compatibility fix
		 *
		 * *********************************************************/
		$scope.deskTopView = true;

		/********************* ux fix ends here *****************************/
	}
	

	$scope.addDepartmentMaster = function() {
		 if($rootScope.roleAccess(roleConstant.MASTER_HRMS_DEPARTMENT_CREATE)){
			 console.log("Add Department Button Pressed...");
				$state.go("layout.addDepartment");
		 }
	}
	
	
	$scope.editDepartmentMaster = function () {
		 if($rootScope.roleAccess(roleConstant.MASTER_HRMS_DEPARTMENT_MODIFY)){
			 console.log("Edit Department Button Pressed...");
				$state.go("layout.editDepartment", {departmentId : $scope.departmentMaster.id}); 
		 }
	};
		

	$scope.deleteDepartmentMaster = function() {
		 if($rootScope.roleAccess(roleConstant.MASTER_HRMS_DEPARTMENT_DELETE)){
			 var newScope = $scope.$new();
				newScope.errorMessage = $rootScope.nls["ERR252"];
				ngDialog.openConfirm( 
				{ template : 	'<p>{{errorMessage}}</p>' +
								'<div class="ngdialog-footer">' +
								'<button type="button" class="btn btn-default btn-property cancel-btn" ng-click="closeThisDialog(0)">No' +
								'<button type="button" class="btn btn-primary btn-property accent-btn" ng-click="confirm(1)">Yes' +
								'</button>' +
								'</div>',
								plain: true,
								scope: newScope,
								className: 'ngdialog-theme-default'
				}).then(function (value) {

					DepartmentFactory.remove.query({ id : $scope.departmentMaster.id }, function(data) {
						if (data.responseCode =="ERR0"){
							Notification.success($rootScope.nls["ERR402"]);
							console.log("Department deleted Successfully")
							$scope.cancel();
							$scope.init();
						} else {
							console.log("Department deletion Failed ", data.responseDescription)
						}
					}, function(error) {
						console.log("Department deleted Failed : ", error)
					});
					
				}, function (value) {
					console.log("Department deleted cancelled");
				});
		 }
	}
    
	
	$scope.rowSelect = function(obj){
		 if($rootScope.roleAccess(roleConstant.MASTER_HRMS_DEPARTMENT_VIEW)){
			 console.log("rowSelect is called.");
				$scope.departmentMaster = obj;
				$scope.detailView = true;
				/************************************************************
				 * ux - by Muthu
				 * reason - ipad compatibility fix
				 *
				 * *********************************************************/

				var windowInner=$window.innerWidth;
				if(windowInner<=1199){
					$scope.deskTopView = false;
				}
				else{
					$scope.deskTopView = true;
				}
				
				angular.element($window).bind('resize', function(){
					if($window.innerWidth>=1200){
						$scope.deskTopView = true;
					}
					else if($window.innerWidth<=1199){
						if($scope.departmentMaster.id!=null){
							$scope.deskTopView = false;
						}
						else {
							$scope.deskTopView = true;
						}
					}
					console.log("window resizing..." + $window.innerWidth);
					$scope.$digest();
				});

				/********************* ux fix ends here *****************************/ 
		 }
	}
	
	$scope.changepage = function(param) {
		console.log("ChangePage..." , param.page);
		$scope.page = param.page;
		$scope.limit = param.size;
		$scope.search();
	}
	
	$scope.limitChange = function(item){
		console.log("LimitChange...", item);
		$scope.page = 0;
		$scope.limit = item;
		$scope.search();
	}

	$scope.sortChange = function(param){
		console.log("SortChange..." , param);
		$scope.searchDto.orderByType = param.sortOrder.toUpperCase();
		$scope.searchDto.sortByColumn = param.sortKey;
		$scope.search();
	}
	
	$scope.changeSearch = function(param){
		console.log("Change Search ", param);
		for (var attrname in param) {
			$scope.searchDto[attrname] = param[attrname];
		}
		$scope.page = 0;
		$scope.cancel();
		$scope.search();
	}
	
	
	$scope.search=  function(){
		$scope.searchDto.selectedPageNumber = $scope.page;
		$scope.searchDto.recordPerPage = $scope.limit;

		DepartmentFactory.search.fetch($scope.searchDto).$promise.then(function(data, status) {
			$scope.totalRecord = data.responseObject.totalRecord;
			$scope.departmentArr = data.responseObject.searchResult;
			console.log($scope.departmentArr);
		});
	}
	
	
	if($stateParams.action == "SEARCH") {
			console.log("department Master Search Page....");
			$scope.init();

			$rootScope.breadcrumbArr = [ {label:"Master", state:"layout.department"}, 
			                              {label:"Department",  state:"layout.department"}];
	}	
}]);

