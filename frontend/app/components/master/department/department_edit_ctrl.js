app.controller('DepartmentMasterEditCtrl', function($rootScope, $scope, $state, $stateParams, PortGroupSaveOrUpdate, PortRemove, 
						ngProgressFactory, ngDialog, PortGroupView, appConstant,CommonValidationService, Notification, DepartmentFactory,AutoCompleteService,ValidateUtil) {
				    
					$scope.contained_progressbar = ngProgressFactory.createInstance();
					$scope.contained_progressbar.setParent(document.getElementById('group-panel'));
					$scope.contained_progressbar.setAbsolute();
					 $scope.$AutoCompleteService = AutoCompleteService;
					$scope.init = function() {

						console.log("Init is called...............");
						
						if($scope.departmentMaster == undefined) {
							$scope.departmentMaster =  {};
							$scope.departmentMaster.status = 'Active';
						}
						
						
						// Setting it to compare while cancelling
						$scope.oldData = JSON.stringify($scope.departmentMaster);
					}
					
		                
					$scope.cancel = function() {

						if ($scope.oldData !== JSON.stringify($scope.departmentMaster)) {

							var newScope = $scope.$new();
							newScope.errorMessage = $rootScope.nls["ERR200"];
							ngDialog.openConfirm({
								template: '<p>{{errorMessage}}</p>' +
								'<div class="ngdialog-footer">' +
									' <button type="button" class="btn btn-default btn-property cancel-btn" ng-click="confirm(2)">No</button>' +
									'<button type="button" class="btn btn-default btn-property accent-btn " ng-click="confirm(1)">Yes</button>' +
									'<button type="button" class="btn btn-default btn-property pull-left grey-cancel-btn" ng-click="confirm(3)">Cancel</button>' +
								'</div>',
								plain: true,
								scope: newScope,
								className: 'ngdialog-theme-default'
							}).then(function(value) {

								if (value == 1) {
									$scope.saveOrUpdate();
								} else {
									
									var params = {};
									params.submitAction = 'Cancelled';
									$state.go("layout.department",params);
								}
							});
						} else {
							var params = {};
							params.submitAction = 'Cancelled';
							$state.go("layout.department",params);
						}
					}

					
					$scope.saveOrUpdate = function() {
					
						console.log("SaveOrUpdate is called...");
						
						if($scope.validateDepartmentMaster(0)) {
							$scope.contained_progressbar.start();
							
							
							if($scope.departmentMaster.id == null) {
								var successMessage = $rootScope.nls["ERR400"];	
							} else {
								var successMessage = $rootScope.nls["ERR401"];	
							}
							
							DepartmentFactory.save.query($scope.departmentMaster).$promise.then(function(data) {
								
								if (data.responseCode == "ERR0") {
									Notification.success(successMessage);
									console.log("Department updated Successfully");
									$state.go("layout.department", {submitAction : 'Saved'});
								} else {
									console.log("Department updated Failed ", data.responseDescription)
								}
								$scope.contained_progressbar.complete();
								angular.element(".panel-body").animate({ scrollTop : 0 }, "slow");
							},	function(error) {
								console.log("Department updated Failed : ", error);
							});
						} else {
							console.log("Form Invalid");
						}
						

					}



					$scope.validateDepartmentMaster = function(validateCode) {
						
						$scope.errorMap = new Map();
						

						if (validateCode == 0 || validateCode == 1) {
							
							if ($scope.departmentMaster.departmentName == undefined || $scope.departmentMaster.departmentName == null || $scope.departmentMaster.departmentName == "") {
								$scope.errorMap.put("departmentName", $rootScope.nls["ERR2424"]);
								return false;
							} 
						}
							
						/*if (validateCode == 0 || validateCode == 2) {
							if ($scope.departmentMaster.departmentCode == undefined || $scope.departmentMaster.departmentCode == null || $scope.departmentMaster.departmentCode == "") {
								$scope.errorMap.put("departmentCode", $rootScope.nls["ERR2422"]);
								return false;
							} 
						}*/
						
						if (validateCode == 0 || validateCode == 3) {
							if ($scope.departmentMaster.departmentHead==null || $scope.departmentMaster.departmentHead.id == undefined
									|| $scope.departmentMaster.departmentHead.id == null
									|| $scope.departmentMaster.departmentHead.id == "") {
								console.log($rootScope.nls["ERR2426"]);
								$scope.errorMap.put("departmentHead",$rootScope.nls["ERR2426"]);
								return false;
							}else if($scope.departmentMaster.departmentHead.employementStatus!=null){
							
								if(ValidateUtil.isEmployeeResigned($scope.departmentMaster.departmentHead.employementStatus)){
									$scope.departmentMaster.departmentHead=null;
									console.log($rootScope.nls["ERR2427"]);
									$scope.errorMap.put("departmentHead",$rootScope.nls["ERR2427"]);
									return false
									
								}
								if(ValidateUtil.isEmployeeTerminated($scope.departmentMaster.departmentHead.employementStatus)){
									$scope.departmentMaster.departmentHead=null;
									console.log($rootScope.nls["ERR2428"]);
									$scope.errorMap.put("departmentHead",$rootScope.nls["ERR2428"]);
									return false
									
								}
							}
						}

						return true;

					}

					$scope.setStatus = function(data) {
						if (data == 'Active') {
							return 'activetype';
						} else if (data == 'Block') {
							return 'blockedtype';
						} else if (data == 'Hide') {
							return 'hiddentype';
						}
					}

					$scope.selectedDepartmentHead=function(id){
						if($scope.validateDepartmentMaster(3))
							{
							$rootScope.navigateToNextField(id);
							}
					}



					
					
					
					
					

					$scope.getDepartmentById = function(departmentId) {

						console.log("getDepartmentById ", departmentId);
						
						DepartmentFactory.findById.query({ id : departmentId }, function(data) {
				   			if (data.responseCode =="ERR0") {
				   				console.log("Successful while getting Department Master.", data.responseObject);
				   				$scope.departmentMaster = data.responseObject;
				   				console.log("Department Master ", $scope.departmentMaster);
				   				$scope.init();
				   			}
				   		}, function(error) {
				   			console.log("Error while getting department Master.", error)
				   		});
					
					}
					
					

				$scope.$on('addDepartmentEvent', function(events, args){
					$rootScope.unfinishedData = $scope.departmentMaster;
					$rootScope.unfinishedFormTitle = "Department Master (New)";
					$rootScope.category = "Department Master";
					if($scope.departmentMaster != undefined && $scope.departmentMaster != null && $scope.departmentMaster.id != undefined 
							&& $scope.departmentMaster.departmentName != null && $scope.departmentMaster.departmentName != "") {
						$rootScope.subTitle = $scope.departmentMaster.departmentName;
					} else {
						$rootScope.subTitle = "Unknown Department Master"
					}
				});
			  
			
				$scope.$on('editDepartmentEvent', function(events, args) {
				 	$rootScope.unfinishedData = $scope.departmentMaster;
					$rootScope.unfinishedFormTitle = "Department Master Edit (" + $scope.departmentMaster.departmentCode + ")";
					$rootScope.category = "Department Master";
					if($scope.departmentMaster != undefined && $scope.departmentMaster != null && $scope.departmentMaster.id != undefined 
							&& $scope.departmentMaster.departmentName != null && $scope.departmentMaster.departmentName != "") {
						$rootScope.subTitle = $scope.departmentMaster.departmentName;
					} else {
						$rootScope.subTitle = "Unknown Department Master"
					}
				});
			

				$scope.$on('editDepartmentEventReload', function (e, confirmation) {
					confirmation.message = "";
					localStorage.reloadFormData = JSON.stringify($scope.departmentMaster);
					localStorage.isDepartmentReloaded = "YES";
					e.preventDefault();
				});


				$scope.$on('editDepartmentEventReload', function (e, confirmation) {
					confirmation.message = "";
					localStorage.reloadFormData = JSON.stringify($scope.departmentMaster);
					localStorage.isDepartmentReloaded = "YES";
					e.preventDefault();
				});
				
			
			
			$scope.isReloaded = localStorage.isDepartmentReloaded;
			if ($stateParams.action == "ADD") {
				if($stateParams.fromHistory === 'Yes') {
					if($scope.isReloaded == "YES") {
						console.log("Add History Reload...")
						$scope.isReloaded = "NO";
						localStorage.isDepartmentReloaded = "NO";
						$scope.departmentMaster  = JSON.parse(localStorage.reloadFormData);
					} else {
						console.log("Add History Not Reload...")
						$scope.departmentMaster = $rootScope.selectedUnfilledFormData;
					}
				} else {
					if($scope.isReloaded == "YES") {
						console.log("Add Not History Reload....");
						$scope.isReloaded = "NO";
						localStorage.isDepartmentReloaded = "NO";
						$scope.departmentMaster  = JSON.parse(localStorage.reloadFormData);
					} else {
						console.log("Add Not History Not Reload...");
						$scope.init();
					}	
				}
				
				$rootScope.breadcrumbArr = [ { label:"Master",  state:"layout.department"}, 
				                             { label:"Department",  state:"layout.department"}, 
				                             { label:"Add Department", state:null } ];
				
				$rootScope.navigateToNextField("departmentName");
						
			} else {
						
				if($stateParams.fromHistory === 'Yes') {
					if($scope.isReloaded == "YES") {
						$scope.isReloaded = "NO";
						localStorage.isDepartmentReloaded = "NO";
						$scope.departmentMaster  = JSON.parse(localStorage.reloadFormData);
					} else {
						$scope.departmentMaster = $rootScope.selectedUnfilledFormData;
					}
				} else {
					if($scope.isReloaded == "YES") {
						$scope.isReloaded = "NO";
						localStorage.isDepartmentReloaded = "NO";
						$scope.departmentMaster  = JSON.parse(localStorage.reloadFormData);
					} else {
						$scope.getDepartmentById($stateParams.departmentId);
					}
					
				}
						
				$rootScope.breadcrumbArr = [ { label:"Master", state:"layout.department"}, 
				                             { label:"Department",  state:"layout.department"}, 
				                             { label:"Edit Department", state:null } 
				                           ];
				
				$rootScope.navigateToNextField("departmentName");
			}
						

});
