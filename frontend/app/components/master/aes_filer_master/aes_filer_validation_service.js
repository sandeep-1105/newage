app.service('aesFilerMasterValidationService', ['$rootScope','ValidateUtil', 'CommonValidationService', 'appConstant', function($rootScope, ValidateUtil, CommonValidationService, appConstant) {
	this.validate = function(aes, code) {
		//Validate aes name
		if(code == 0 || code == 1) {
			if(aes.schemaName == undefined || aes.schemaName == null || aes.schemaName =="" ){
				return this.validationResponse(true, "schemaName", $rootScope.nls["ERR2012069"], aes);
			}
		}
		//Validate company master
		if(code == 0 || code == 2) {
			if(aes.companyMaster == null || aes.companyMaster == undefined || aes.companyMaster== "" ){
				return this.validationResponse(true, "companyName", $rootScope.nls["ERR2012057"], aes);	
			}
			else{
				if(aes.companyMaster.status=="Block"){
					return this.validationResponse(true, "companyName", $rootScope.nls["ERR2012058"], aes);	
				}
				else if(aes.companyMaster.status=="Hide"){
					return this.validationResponse(true, "companyName", $rootScope.nls["ERR2012059"], aes);	
				}
			}
		}
		//Validate country master
		if(code == 0 || code == 3) {
			if(aes.countryMaster == null || aes.countryMaster == undefined || aes.countryMaster== "" ){
				return this.validationResponse(true, "countryName", $rootScope.nls["ERR2012060"], aes);	
			}
			else{
				if(aes.countryMaster.status=="Block"){
					return this.validationResponse(true, "countryName", $rootScope.nls["ERR2012061"], aes);	
				}
				else if(aes.countryMaster.status=="Hide"){
					return this.validationResponse(true, "countryName", $rootScope.nls["ERR2012062"], aes);	
				}
				else if(aes.locationMaster!=undefined && aes.locationMaster.countryMaster.id!=aes.countryMaster.id){
					aes.locationMaster=undefined;
				}
			}
		}
		
		//Validate location master
		if(code == 0 || code == 4) {
			if(aes.locationMaster == null || aes.locationMaster == undefined || aes.locationMaster== "" ){
				return this.validationResponse(true, "locationName", $rootScope.nls["ERR2012063"], aes);	
			}
			else{
				if(aes.locationMaster.status=="Block"){
					return this.validationResponse(true, "locationName", $rootScope.nls["ERR2012064"], aes);	
				}
				else if(aes.locationMaster.status=="Hide"){
					return this.validationResponse(true, "locationName", $rootScope.nls["ERR2012065"], aes);	
				}
			}
		}
		
		//Validate aes filer code
		if(code == 0 || code == 5) {
			if(aes.aesFilerCode == undefined || aes.aesFilerCode == null || aes.aesFilerCode =="" ){
				return this.validationResponse(true, "aesFilerCode", $rootScope.nls["ERR2012052"], aes);
			}
			else {
				if(!CommonValidationService.checkRegExp(appConstant.REG_EXP_MASTER_AES_FILER_CODE,aes.aesFilerCode)){
                    return this.validationResponse(true, "aesFilerCode", $rootScope.nls["ERR2012053"], aes);
    			}
			}
				
		}
		//Validate aes transmitter Code
		if(code == 0 || code == 6) {
			if(aes.transmitterCode == undefined || aes.transmitterCode == null || aes.transmitterCode =="" ){
				return this.validationResponse(true, "transmitterCode", $rootScope.nls["ERR2012054"], aes);
			}
			else {
				if(!CommonValidationService.checkRegExp(appConstant.REG_EXP_MASTER_AES_FILER_TRANSMITTER_CODE,aes.transmitterCode)){
                    return this.validationResponse(true, "transmitterCode", $rootScope.nls["ERR2012055"], aes);
    			}
			}
		}
		return this.validationSuccesResponse(aes);
	}
 
	this.validationSuccesResponse = function(aes) {
	    return {error : false, obj : aes};
	}
	
	this.validationResponse = function(err, elem, message, aes) {
		return {error : err, errElement : elem, errMessage : message, obj : aes};
	}
	
}]);