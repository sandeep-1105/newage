app.service('aesFilerMasterDataService', function($rootScope) {
    this.getHeadArray = function() {
	   return [
               {
             	  "name" : "#",
             	  "width" : "col-xs-0half",
             	  "prefWidth" : "50",
             	  "model" : "no",
             	  "search" : false
             	
             	  
               },
               {
             	  "name" : "Schema Name",
             	  "width" : "col-xs-5",
             	  "prefWidth" : "50",
             	  "type": "text",
             	  "wrap_cell" : true,
             	  "sort" : true,
             	  "model" : "schemaName",
             	  "key" : "searchSchemaName",
             	  "search" : true
               },
          
              
               {
             	  "name" : "Country Code",
             	  "width" : "col-xs-2",
             	  "prefWidth" : "50",
             	  "wrap_cell" : true,
             	  "model" : "countryMaster.countryCode",
             	  "type":"text",
             	  "search" : true,
             	  "sort" : true,
             	  "key" : "searchCountryCode",
               },
               {
             	  "name" : "Location",
             	  "width" : "col-xs-2",
             	  "prefWidth" : "50",
             	  "wrap_cell" : true,
             	  "model" : "locationMaster.locationName",
             	  "type":"text",
             	  "search" : true,
             	  "sort" : true,
             	  "key" : "searchLocationName",
               },
               {
             	  "name" : "Status",
             	 "width" : "w120px",
            	  "wrap_cell" : true,
             	  "model" : "status",
             	  "sort" : true,
             	  "type":"drop",
             	  "search" : true,
             	  "data" : $rootScope.enum['LovStatus'],
             	  "key" : "searchStatus",
               }
               ];
          };

    
    this.getListBreadCrumb = function() {
        return [{
                label: "Master",
                state: "layout.aesFilerMaster"
            },
            {
                label: "AES Filer",
                state: "layout.aesFilerMaster"
            }
        ];
    }

    this.getAddBreadCrumb = function() {
        return [{
                label: "Master",
                state: "layout.aesFilerMaster"
            },
            {
                label: "AES Filer",
                state: "layout.aesFilerMaster"
            },
            {
                label: "Add AES Filer",
                state: null
            }
        ];
    };
    this.getEditBreadCrumb = function() {
        return [{
                label: "Master",
                state: "layout.aesFilerMaster"
            },
            {
                label: "AES Filer",
                state: "layout.aesFilerMaster"
            },
            {
                label: "Edit AES Filer",
                state: null
            }
        ];
    };
   

});