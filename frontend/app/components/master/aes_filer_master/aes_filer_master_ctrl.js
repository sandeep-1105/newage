(function(){
app.controller("AesFilerMasterCtrl", ['$scope', '$rootScope', '$window', '$modal', '$state', '$stateParams', '$log', 'ngDialog', 'aesFilerMasterDataService', 'aesFilerMasterFactory', 'RecentHistorySaveService', 'aesFilerMasterValidationService', 'Notification','CountryWiseLocation','CompanyList','CountryList', "roleConstant",
    function($scope, $rootScope, $window, $modal, $state, $stateParams, $log, ngDialog, aesFilerMasterDataService, aesFilerMasterFactory, RecentHistorySaveService, aesFilerMasterValidationService, Notification,CountryWiseLocation,CompanyList,CountryList,roleConstant ) {

        var afm = this;
        afm.tableHeadArr = aesFilerMasterDataService.getHeadArray();
        afm.dataArr = [];
        afm.searchData = {};
        afm.aesFilerMaster = {};
        afm.aesFilerMaster.status='Active';
        afm.limitArr = [10, 15, 20];
        afm.page = 0;
        afm.limit = 10;
        afm.totalRecord = 100;
        afm.showHistory = false;
        afm.errorMap = new Map();
        afm.deskTopView = true;

        afm.sortSelection = {
            sortKey: "schemaName",
            sortOrder: "asc"
        }
        
        /**
         * fetching company list start here
         */
        afm.ajaxCompanyEvent = function(object) {
            afm.searchDto = {};
            afm.searchDto.keyword = object == null ? "" : object;
            afm.searchDto.selectedPageNumber = 0;
            afm.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
            return CompanyList.fetch(afm.searchDto).$promise
                .then(
                    function(data) {
                        if (data.responseCode == "ERR0") {
                        	afm.companyList = data.responseObject.searchResult;
                        	return afm.companyList;
                            
                        }
                    },
                    function(errResponse) {
                        console
                            .error('Error while fetching Company');
                    });
        }
        afm.selectedCompany = function(object) {
              afm.validate('companyName', 2);
              $rootScope.navigateToNextField(object);
            
        };
        /**
         * fetching country list start here
         */
        afm.ajaxCountryEvent = function(object) {
    		afm.searchDto = {};
    		afm.searchDto.keyword = object == null ? "" : object;
    		afm.searchDto.selectedPageNumber = 0;
    		afm.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
    		return CountryList.fetch(afm.searchDto).$promise
    				.then(
    						function(data) {
    							if (data.responseCode == "ERR0") {
    								afm.countryList = data.responseObject.searchResult;
    								return afm.countryList;
    							}
    						},
    						function(errResponse) {
    							console
    									.error('Error while fetching Country');
    						});

    	}
        afm.selectedCountry = function(object) {
        	afm.validate('countryName', 3);
            $rootScope.navigateToNextField(object);
           
        };
        
        /**
         * fetching location list start here
         */
        afm.ajaxLocationEvent = function(object) {
    		afm.searchDto = {};
    		afm.searchDto.keyword = object == null ? "" : object;
    		afm.searchDto.selectedPageNumber = 0;
    		afm.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
    		if(afm.aesFilerMaster.countryMaster==undefined || afm.aesFilerMaster.countryMaster==null) {
    			return;
    			}
    		return CountryWiseLocation.fetch({"countryId":afm.aesFilerMaster.countryMaster.id},afm.searchDto).$promise.then(
					function(data) {
						if (data.responseCode == "ERR0") {
							$scope.locationList = data.responseObject.searchResult;
							return $scope.locationList;
						}
					},
					function(errResponse) {
						console.error('Error while fetching Locations');
					});

    	}
        afm.selectedLocation = function(object) {
        	afm.validate('locationName', 4);
            $rootScope.navigateToNextField(object);
           
        };
        
        afm.setStatus = function(data) {
			if (data == 'Active') {
				return 'activetype';
			} else if (data == 'Block') {
				return 'blockedtype';
			} else if (data == 'Hide') {
				return 'hiddentype';
			}
		}
        
        afm.changeSearch = function(param) {
            $log.debug("Change Search is called." + param);
            for (var attrname in param) {
                afm.searchData[attrname] = param[attrname];
            }
            afm.page = 0;
            afm.search();
        }

        afm.sortChange = function(param) {
            $log.debug("sortChange is called. " + param);
            afm.searchData.orderByType = param.sortOrder.toUpperCase();
            afm.searchData.sortByColumn = param.sortKey;
            afm.search();
        }
        
        afm.setStatus = function(data){
    		if(data=='Active'){
    			return 'activetype';
    		} else if(data=='Block'){
    			return 'blockedtype';
    		}else if(data=='Hide'){
    			return 'hiddentype';
    		}
    	}
        
        /**
         * AES Filer Master List Navigate to selected page.
         * @param {Object} param - The Selected page option.
         */
        afm.changepage = function(param) {
            $log.debug("change Page..." + param.page);
            afm.page = param.page;
            afm.limit = param.size;
            afm.search();
        }

        /**
         * AES Filer Master List Page Limit Data change.
         * @param {int} nLimit - The Selected limit option from Limit array.
         */
        afm.limitChange = function(nLimit) {
            $log.debug("Limit Change..." + nLimit);
            afm.page = 0;
            afm.limit = nLimit;
            afm.search();
        }

        /**
         * AES Filer Master List Page Search Data  initializing and calling search Mtd.
         * @param {Object} data - The Selected AES Filer Master from List page.
         * @param {string} data.id - The id of the Selected AES Filer Master.
         * @param {int} index - The Selected row index from List page.
         */
        afm.rowSelect = function(data) {
        	if($rootScope.roleAccess(roleConstant.MASTER_GENERAL_AES_FILER_VIEW)){
        	afm.aesMasteView=true;
			afm.aesFilerMaster = data;
			var windowInner=$window.innerWidth;
			if(windowInner<=1199){
				afm.deskTopView = false;
			}
			else{
				afm.deskTopView = true;
			}
			angular.element($window).bind('resize', function(){
				if($window.innerWidth>=1200){
					afm.deskTopView = true;
				}
				else if($window.innerWidth<=1199){
					if(afm.aesFilerMaster.id!=null){
						afm.deskTopView = false;
					}
					else {
						afm.deskTopView = true;
					}
				}
				$scope.$digest();
			});
        	}
		}

        /**
         * AES Filer Master List Page Search Data  initializing and calling search Mtd.
         */
        afm.searchInit = function() {
            afm.searchData = {};
            afm.searchData.orderByType = afm.sortSelection.sortOrder.toUpperCase();
            afm.searchData.sortByColumn = afm.sortSelection.sortKey;
            afm.search();
        };
        /**
         * Go Back to AES Filer Master List Page.
         */
        afm.backToList = function() {
        	afm.aesMasteView = false;
        	afm.aesFilerMaster ={};
        	afm.deskTopView = true;
        }

       
        /**
         * Get AES Filer Master's Based on Search Data.
         */
        afm.search = function() {
        	afm.aesMasteView=false;
            afm.searchData.selectedPageNumber = afm.page;
            afm.searchData.recordPerPage = afm.limit;
            afm.dataArr = [];
            aesFilerMasterFactory.search.fetch(afm.searchData).$promise.then(
                function(data) {
                    if (data.responseCode == "ERR0") {
                        afm.totalRecord = data.responseObject.totalRecord;
                        afm.dataArr = data.responseObject.searchResult;
                    }
                },
                function(errResponse) {
                	 $log.error('Error while fetching AES Filer');
                });
           }

        /**
         * Get AES Filer Master By id.
         * @param {int} id - The id of a AES Filer.
         * @param {boolean} isView - TO identify purpose of calling is for View or Edit a AES Filer.
         */
        afm.view = function(id, isView) {
        	aesFilerMasterFactory.findById.query({
                id: id
            }).$promise.then(function(data) {
                if (data.responseCode == 'ERR0') {
                    afm.aesFilerMaster = data.responseObject;
                    var rHistoryObj = {}

                    if (afm.aesFilerMaster != undefined && afm.aesFilerMaster != null && afm.aesFilerMaster.id != undefined && afm.aesFilerMaster.id != null) {
                        $rootScope.subTitle = afm.aesFilerMaster.aesName;
                    } else {
                        $rootScope.subTitle = "Unknown AES Filer"
                    }
                    $rootScope.unfinishedFormTitle = "AES Filer  # " + afm.aesFilerMaster.aesCode;
                    if (isView) {
                        rHistoryObj = {
                            'title': "AES Filer #" + afm.aesFilerMaster.aesCode,
                            'subTitle': $rootScope.subTitle,
                            'stateName': $state.current.name,
                            'stateParam': JSON.stringify($stateParams),
                            'stateCategory': 'AES Filer Master',
                            'serviceType': undefined,
                            'showService': false
                        }

                    } else {

                        rHistoryObj = {
                            'title': 'Edit AES Filer # ' + $stateParams.serviceId,
                            'subTitle': $rootScope.subTitle,
                            'stateName': $state.current.name,
                            'stateParam': JSON.stringify($stateParams),
                            'stateCategory': 'AES Filer Master',
                            'serviceType': 'MASTER',
                            'serviceCode': ' ',
                            'orginAndDestination': ' '
                        }
                    }
                    RecentHistorySaveService.form(rHistoryObj);
                } else {
                    $log.debug("Exception while getting AES Filer by Id ", data.responseDescription);
                }
            }, function(error) {
                $log.debug("Error ", error);
            });
        };

        /**
         * Go To Add AES Filer Master.
         */
        afm.addNew = function() {
        	if($rootScope.roleAccess(roleConstant.MASTER_GENERAL_AES_FILER_CREATE)){
            $state.go("layout.aesFilerMasterAdd");
        	}
        }

        /**
         * Validating field of AES Filer Master while instant change.
         * @param {errorElement,errorCode}
         */
        afm.validate = function(valElement, code) {
            var validationResponse = aesFilerMasterValidationService.validate(afm.aesFilerMaster,code);
            if(validationResponse.error == true) {
				$scope.validationErrorAndFocus(validationResponse, validationResponse.errElement);
			} else {
				afm.errorMap.put(valElement, null);
            	validationResponse.error = false;
            }
        }
        
        /**
         * Cancel Add or Edit AES Filer Master.
         * @param {int} id - The id of a AES Filer
         */
       
        afm.cancel = function(objId) {
            if(afm.aesFilerMasterForm!=undefined){
		    	afm.goTocancel(afm.aesFilerMasterForm.$dirty,objId);
            }else{
            	afm.goTocancel(false,objId);
            }
        }
       
       afm.goTocancel = function(isDirty,objId){
     		 if (isDirty) {
     		      var newScope = $scope.$new();
     		      newScope.errorMessage = $rootScope.nls["ERR200"];
     		      ngDialog.openConfirm({
     		        template: '<p>{{errorMessage}}</p>' +
     		          '<div class="ngdialog-footer">' +
     		          ' <button type="button" class="btn btn-default btn-property cancel-btn" ng-click="confirm(2)">No</button>' +
     		          '<button type="button" class="btn btn-default btn-property accent-btn " ng-click="confirm(1)">Yes</button>' +
     		          '<button type="button" class="btn btn-default btn-property pull-left grey-cancel-btn" ng-click="confirm(3)">Cancel</button>' +
     		          '</div>',
     		        plain: true,
     		        closeByDocument: false,
     		        scope: newScope,
     		        className: 'ngdialog-theme-default'
     		      }).then(
     		        function(value) {
     		          if (value == 1) {
     		            if(objId!=null && objId != undefined){
     		            	afm.update();
     		            }else{
     		            	afm.create();
     		            }
     		          } else if (value == 2) {
     		        	 $state.go("layout.aesFilerMaster", {submitAction: "Cancelled"});
     		          } else {
     		            $log.debug("cancelled");
     		          }
     		        });
     		    } else {
     		     $state.go("layout.aesFilerMaster", {submitAction: "Cancelled"});
     		    }
     	    }

        /**
         * Modify Existing AES Filer Master.
         * @param {int} id - The id of a Country.
         */
        afm.edit = function(id) {
        	if($rootScope.roleAccess(roleConstant.MASTER_GENERAL_AES_FILER_MODIFY)){
            $state.go("layout.aesFilerMasterEdit", {
                id: id
            });
        	}
        }

        /**
         * Delete Country Master.
         * @param {int} id - The id of a Country.
         */
        afm.delete = function(id) {
        	if($rootScope.roleAccess(roleConstant.MASTER_GENERAL_AES_FILER_DELETE)){
        	var newScope = $scope.$new();
			newScope.errorMessage = $rootScope.nls["ERR2012066"];
	    	ngDialog.openConfirm( {
	                    template : '<p>{{errorMessage}}</p>'
	                    + '<div class="ngdialog-footer">'
	                    + '<button type="button" class="btn btn-default btn-property cancel-btn" ng-click="closeThisDialog(0)">Cancel'
	                    + '<button type="button" class="btn btn-primary btn-property accent-btn" ng-click="confirm(1)">Yes'
	                    + '</button></div>',
	                    plain : true,
	                    scope : newScope,
	                    closeByDocument: false,
	                    className : 'ngdialog-theme-default'
	                }).then( function(value) {
	                	aesFilerMasterFactory.delete.query({
	                        id: id
	                    }).$promise.then(function(data) {
	                        if (data.responseCode == 'ERR0') {
	                        	afm.aesMasteView=false;
	                        	Notification.success($rootScope.nls["ERR402"]);
	                        	afm.deskTopView = true;	
	                            afm.search();
	                        }
	                    }, function(error) {
	                        $log.debug("Error ", error);
	                    });
	                }, function(value) {
	                    $log.debug("delete Cancelled");
	            });
        	}
        }

        /**
         * Create New AES Filer Master.
         */
        afm.create = function() {
            var validationResponse = aesFilerMasterValidationService.validate(afm.aesFilerMaster,0);
            if(validationResponse.error == true) {
            	$log.debug("Validation Response -- ", validationResponse);
            	$log.debug('Validation Failed to create AES Filer..')
				$scope.validationErrorAndFocus(validationResponse, validationResponse.errElement);
			}
            else {
            	aesFilerMasterFactory.create.query(afm.aesFilerMaster).$promise.then(function(data) {
		            if (data.responseCode == 'ERR0') {
		            	$log.debug("AES Filer Saved Successfully Response :: ", data);
		            	Notification.success($rootScope.nls["ERR400"]);
		            	$state.go('layout.aesFilerMaster');
		            } else {
		                $log.debug("Create AES Filer Failed :" + data.responseDescription)
		            }

		        }, function(error) {
		        	 $log.debug("Create AES Filer Failed : " + error)
		        });
            } 
        };

        
      /**
       * Common Validation Focus Functionality - starts here" 
       */
		$scope.validationErrorAndFocus = function(valResponse, elemId){
			afm.errorMap = new Map();
			if(valResponse.error == true){
				afm.errorMap.put(valResponse.errElement, valResponse.errMessage);
			}
				$scope.navigateToNextField(elemId);
		}
        
        //Update Existing Country Master
        afm.update = function() {
        	
        	 var validationResponse = aesFilerMasterValidationService.validate(afm.aesFilerMaster,0);
             if(validationResponse.error == true) {
             	$log.debug("Validation Response -- ", validationResponse);
             	$log.debug('Validation Failed while Updating the AES Filer..')
 				$scope.validationErrorAndFocus(validationResponse, validationResponse.errElement);
 			}
             else {
             	aesFilerMasterFactory.update.query(afm.aesFilerMaster).$promise.then(function(data) {
 		            if (data.responseCode == 'ERR0') {
 		            	$log.debug("AES Filer Updated Successfully Response :: ", data);
 		            	Notification.success($rootScope.nls["ERR401"]);
 		            	$state.go('layout.aesFilerMaster');
 		            } else {
 		                $log.debug("Updating AES Filer Failed :" + data.responseDescription)
 		            }
 		        }, function(error) {
 		        	 $log.debug("Updating AES Filer Failed : " + error)
 		        });
             } 
        	
        };
        
       
        /**
         * Recent History - starts here
         */
        afm.isReloaded = localStorage.isAESFilerMasterReloaded;
        $scope.$on('addAESFilerMasterEvent', function(events, args){
        	$rootScope.unfinishedData = afm.aesFilerMaster;
			$rootScope.category = "AES Filer";
			$rootScope.unfinishedFormTitle = "AES Filer (New)";
        	if(afm.aesFilerMaster != undefined && afm.aesFilerMaster != null && afm.aesFilerMaster.companyMaster.companyName != undefined && afm.aesFilerMaster.companyMaster.companyName  != null) {
        		$rootScope.subTitle = afm.aesFilerMaster.companyMaster.companyName;
        	} else {
        		$rootScope.subTitle = "Unknown AES Filer "
        	}
        })
      
	    $scope.$on('editAESFilerMasterEvent', function(events, args){
	    	$rootScope.unfinishedData = afm.aesFilerMaster;
			$rootScope.category = "AES Filer";
			$rootScope.unfinishedFormTitle = "AES Filer Edit # "+afm.aesFilerMaster.companyMaster.companyCode;
			if(afm.aesFilerMaster != undefined && afm.aesFilerMaster != null && afm.aesFilerMaster.companyMaster.companyName != undefined && afm.aesFilerMaster.companyMaster.companyName  != null) {
        		$rootScope.subTitle = afm.aesFilerMaster.companyMaster.companyName;
        	}
	    })
	      
	    $scope.$on('addAESFilerMasterEventReload', function(e, confirmation){
	    	    confirmation.message = "";
	    	    localStorage.reloadFormData = JSON.stringify(afm.aesFilerMaster) ;
	    	    localStorage.isAESFilerMasterReloaded = "YES";
	            e.preventDefault();
	    })
	    
	    $scope.$on('editAESFilerMasterEventReload', function(e, confirmation){
	    	    confirmation.message = "";
	    	    localStorage.reloadFormData = JSON.stringify(afm.aesFilerMaster) ;
	    	    localStorage.isAESFilerMasterReloaded = "YES";
	            e.preventDefault();
	    	  
	    })
	      


        /**
         * State Identification Handling Here.
         * @param {String} $stateParams.action - VIEW, ADD, EDIT, SEARCH.
         */
        //
        switch ($stateParams.action) {
            case "ADD":
            	 if ($stateParams.fromHistory === 'Yes') {
                     if (afm.isReloaded == "YES") {
                      afm.isReloaded = "NO";
                      localStorage.isAESFilerMasterReloaded = "NO";
                       afm.aesFilerMaster = JSON.parse(localStorage.reloadFormData);
                     } else {
                       afm.aesFilerMaster = $rootScope.selectedUnfilledFormData;
                     }
                   } else {
                     if (afm.isReloaded == "YES") {
                       afm.isReloaded = "NO";
                       localStorage.isAESFilerMasterReloaded = "NO";
                       afm.aesFilerMaster = JSON.parse(localStorage.reloadFormData);
                     } else {
                         $log.debug("Add NotHistory NotReload...")
                     }
                   }
                $rootScope.breadcrumbArr = aesFilerMasterDataService.getAddBreadCrumb();
                break;
            case "EDIT":
            	 if (afm.fromHistory === 'Yes') {
                     if (afm.isReloaded == "YES") {
                       afm.isReloaded = "NO";
                       localStorage.isAESFilerMasterReloaded = "NO";
                       afm.aesFilerMaster = JSON.parse(localStorage.reloadFormData);
                     } else {
                       afm.aesFilerMaster = $rootScope.selectedUnfilledFormData;
                     }
                   } else {
                     if (afm.isReloaded == "YES") {
                       afm.isReloaded = "NO";
                       localStorage.isAESFilerMasterReloaded = "NO";
                       afm.aesFilerMaster = JSON.parse(localStorage.reloadFormData);
                     } else {
                    	 afm.view($stateParams.id, false);
                     }
                   }
                $rootScope.breadcrumbArr = aesFilerMasterDataService.getEditBreadCrumb();
                break;
            default:
                $rootScope.breadcrumbArr = aesFilerMasterDataService.getListBreadCrumb();
                afm.searchInit();
        }

    }
]);
}) ();