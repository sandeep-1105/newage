(function() {

	app.factory("aesFilerSearch", function($resource) {
		return $resource("/api/v1/aesfilermaster/get/search", {}, {
			query : {
				method : 'POST'
			}
		});
	});
	
	app.factory("aesFilerSearchKeyword", function($resource) {
		return $resource("/api/v1/aesfilermaster/get/search/keyword", {}, {
			fetch : {
				method : 'POST'
			}
		});
	});


})();
app.factory('aesFilerMasterFactory', function($resource) {
	return {
		findById : $resource('/api/v1/aesfilermaster/get/id/:id', {}, {
			 query : {method : 'GET', params : {id : ''}, isArray : false}
		}),
		create : $resource('/api/v1/aesfilermaster/create', {}, {
			query : {method : 'POST'}
		}),
		update : $resource('/api/v1/aesfilermaster/update', {}, {
			query : {method : 'POST'}
		}),
		delete : $resource('/api/v1/aesfilermaster/delete/:id', {}, {
			query : {method : 'DELETE', params : {id : ''}}
		}),
		search : $resource('/api/v1/aesfilermaster/get/search', {}, {
			fetch : {method : 'POST'}
		})
	};
})


