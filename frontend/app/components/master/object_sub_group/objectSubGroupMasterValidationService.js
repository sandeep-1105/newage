app.service('objectSubGroupMasterValidationService', ['$rootScope','ValidateUtil', 'CommonValidationService', 'appConstant', function($rootScope, ValidateUtil, CommonValidationService, appConstant) {
	this.validate = function(objectSubGroup, code) {
		//Validate objectSubGroup name
		if(code == 0 || code == 1) {
			if(objectSubGroup.objectSubGroupName == undefined || objectSubGroup.objectSubGroupName == null || objectSubGroup.objectSubGroupName =="" ){
				return this.validationResponse(true, "objectSubGroupName", $rootScope.nls["ERR06111"], objectSubGroup);
			}
		}
		//Validate objectSubGroup code 
		if(code == 0 || code == 2) {
			if(objectSubGroup.objectSubGroupCode == undefined || objectSubGroup.objectSubGroupCode == null || objectSubGroup.objectSubGroupCode =="" ){
				return this.validationResponse(true, "objectSubGroupCode", $rootScope.nls["ERR06110"], objectSubGroup);
			}
		}
		//Validate objectGroup  
		if(code == 0 || code == 3) {
			if(objectSubGroup.objectGroupMaster == undefined || objectSubGroup.objectGroupMaster == null || objectSubGroup.objectGroupMaster =="" ){
				return this.validationResponse(true, "objectGroupName", $rootScope.nls["ERR06901"], objectSubGroup);
			}else{
				if(objectSubGroup.objectGroupMaster.status=='Block'){
					return this.validationResponse(true, "objectGroupName", $rootScope.nls["ERR06118"], objectSubGroup);
				}else if(objectSubGroup.objectGroupMaster.status=='Hide'){
					return this.validationResponse(true, "objectGroupName", $rootScope.nls["ERR06119"], objectSubGroup);
				}
			}
		}
		
		return this.validationSuccesResponse(objectSubGroup);
	}
 
	this.validationSuccesResponse = function(objectSubGroup) {
	    return {error : false, obj : objectSubGroup};
	}
	
	this.validationResponse = function(err, elem, message, objectSubGroup) {
		return {error : err, errElement : elem, errMessage : message, obj : objectSubGroup};
	}
	
}]);