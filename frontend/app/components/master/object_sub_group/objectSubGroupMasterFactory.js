app.factory('objectSubGroupMasterFactory', function($resource) {
	return {
		findById : $resource('/api/v1/objectsubgroupmaster/get/id/:id', {}, {
			 query : {method : 'GET', params : {id : ''}, isArray : false}
		}),
		create : $resource('/api/v1/objectsubgroupmaster/create', {}, {
			query : {method : 'POST'}
		}),
		delete : $resource('/api/v1/objectsubgroupmaster/delete/:id', {}, {
			query : {method : 'DELETE', params : {id : ''}}
		}),
		search : $resource('/api/v1/objectsubgroupmaster/get/search/keyword', {}, {
			query : {method : 'POST'}
		}),
		getall : $resource('/api/v1/objectsubgroupmaster/get/search/:groupId', {}, {
			fetch : {method : 'POST'}
		})
	};
})