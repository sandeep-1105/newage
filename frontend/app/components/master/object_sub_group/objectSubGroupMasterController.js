(function(){
app.controller("ObjectSubGroupMasterCtrl", ['$scope', '$rootScope', '$window', '$modal', '$state', '$stateParams', '$log', 'ngDialog', 'objectSubGroupMasterDataService', 'objectSubGroupMasterFactory', 'RecentHistorySaveService', 'objectSubGroupMasterValidationService', 'Notification','objectGroupMasterFactory','roleConstant',
    function($scope, $rootScope, $window, $modal, $state, $stateParams, $log, ngDialog, objectSubGroupMasterDataService, objectSubGroupMasterFactory, RecentHistorySaveService, objectSubGroupMasterValidationService, Notification,objectGroupMasterFactory,roleConstant) {

        var vm = this;
        vm.tableHeadArr = objectSubGroupMasterDataService.getHeadArray();
        vm.dataArr = [];
        vm.objectSubGroupMaster = {};
        vm.objectSubGroupMaster.status='Active';
        vm.showHistory = false;
        vm.errorMap = new Map();
        vm.deskTopView = true;

        vm.sortSelection = {
            sortKey: "objectSubGroupName",
            sortOrder: "asc"
        }
       
        vm.clear = function(){
        	vm.errorMap = new Map();
        }
        
        vm.changeSearch = function(param) {
            $log.debug("Change Search is called." + param);
            for (var attrname in param) {
                vm.searchData[attrname] = param[attrname];
            }
            vm.page = 0;
            vm.search();
        }

        vm.sortChange = function(param) {
            $log.debug("sortChange is called. " + param);
            vm.searchData.orderByType = param.sortOrder.toUpperCase();
            vm.searchData.sortByColumn = param.sortKey;
            vm.search();
        }
        
        vm.setStatus = function(data){
    		if(data=='Active'){
    			return 'activetype';
    		} else if(data=='Block'){
    			return 'blockedtype';
    		}else if(data=='Hide'){
    			return 'hiddentype';
    		}
    	}
        
        /**
         * ObjectSubGroup Master List Navigate to selected page.
         * @param {Object} param - The Selected page option.
         */
        vm.changepage = function(param) {
            $log.debug("change Page..." + param.page);
            vm.page = param.page;
            vm.limit = param.size;
            vm.search();
        }

        /**
         * ObjectSubGroup Master List Page Limit Data change.
         * @param {int} nLimit - The Selected limit option from Limit array.
         */
        vm.limitChange = function(nLimit) {
            $log.debug("Limit Change..." + nLimit);
            vm.page = 0;
            vm.limit = nLimit;
            vm.search();
        }

        /**
         * ObjectSubGroup Master List Page Search Data  initializing and calling search Mtd.
         * @param {Object} data - The Selected ObjectSubGroup Master from List page.
         * @param {string} data.id - The id of the Selected ObjectSubGroup Master.
         * @param {int} index - The Selected row index from List page.
         */
        vm.rowSelect = function(data) {
        	if($rootScope.roleAccess(roleConstant.MASTER_GENERAL_OBJECT_SUBGROUP_VIEW)){
        	vm.objectSubGroupMasteView=true;
			vm.objectSubGroupMaster = data;
			var windowInner=$window.innerWidth;
			if(windowInner<=1199){
				vm.deskTopView = false;
			}
			else{
				vm.deskTopView = true;
			}
			angular.element($window).bind('resize', function(){
				if($window.innerWidth>=1200){
					vm.deskTopView = true;
				}
				else if($window.innerWidth<=1199){
					if(vm.objectSubGroupMaster.id!=null){
						vm.deskTopView = false;
					}
					else {
						vm.deskTopView = true;
					}
				}
				$scope.$digest();
			});
		}
}

        /**
         * ObjectSubGroup Master List Page Search Data  initializing and calling search Mtd.
         */
        vm.searchInit = function() {
            vm.searchData = {};
            vm.limitArr = [10, 15, 20];
            vm.page = 0;
            vm.limit = 10;
            vm.totalRecord = 10;
            vm.searchData.orderByType = vm.sortSelection.sortOrder.toUpperCase();
            vm.searchData.sortByColumn = vm.sortSelection.sortKey;
            vm.search();
        };
        /**
         * Go Back to ObjectSubGroup Master List Page.
         */
        vm.backToList = function() {
        	vm.objectSubGroupMasteView = false;
        	vm.objectSubGroupMaster = {};
        	vm.deskTopView = true;
        }

       
        /**
         * Get ObjectSubGroup Master's Based on Search Data.
         */
        vm.search = function() {
        	vm.objectSubGroupMasteView=false;
            vm.searchData.selectedPageNumber = vm.page;
            vm.searchData.recordPerPage = vm.limit;
            vm.dataArr = [];
            objectSubGroupMasterFactory.search.query(vm.searchData).$promise.then(
                function(data) {
                    if (data.responseCode == "ERR0") {
                        vm.totalRecord = data.responseObject.totalRecord;
                        vm.dataArr = data.responseObject.searchResult;
                    }
                },
                function(errResponse) {
                	 $log.error('Error while fetching ObjectSubGroup');
                });
           }

        /**
         * Get ObjectSubGroup Master By id.
         * @param {int} id - The id of a ObjectSubGroup.
         * @param {boolean} isView - TO identify purpose of calling is for View or Edit a ObjectSubGroup.
         */
        vm.view = function(id, isView) {
        	objectSubGroupMasterFactory.findById.query({
                id: id
            }).$promise.then(function(data) {
                if (data.responseCode == 'ERR0') {
                    vm.objectSubGroupMaster = data.responseObject;
                    var rHistoryObj = {}

                    if (vm.objectSubGroupMaster != undefined && vm.objectSubGroupMaster != null && vm.objectSubGroupMaster.id != undefined && vm.objectSubGroupMaster.id != null) {
                        $rootScope.subTitle = vm.objectSubGroupMaster.objectGroupName;
                    } else {
                        $rootScope.subTitle = "Unknown ObjectSubGroup"
                    }
                    $rootScope.unfinishedFormTitle = "ObjectSubGroup # " + vm.objectSubGroupMaster.objectGroupCode;
                    if (isView) {
                        rHistoryObj = {
                            'title': "ObjectSubGroup #" + vm.objectSubGroupMaster.objectGroupCode,
                            'subTitle': $rootScope.subTitle,
                            'stateName': $state.current.name,
                            'stateParam': JSON.stringify($stateParams),
                            'stateCategory': 'ObjectSubGroup Master',
                            'serviceType': undefined,
                            'showService': false
                        }

                    } else {

                        rHistoryObj = {
                            'title': 'Edit ObjectSubGroup # ' + $stateParams.serviceId,
                            'subTitle': $rootScope.subTitle,
                            'stateName': $state.current.name,
                            'stateParam': JSON.stringify($stateParams),
                            'stateCategory': 'ObjectSubGroup Master',
                            'serviceType': 'MASTER',
                            'serviceCode': ' ',
                            'orginAndDestination': ' '
                        }
                    }
                    RecentHistorySaveService.form(rHistoryObj);
                } else {
                    $log.debug("Exception while getting ObjectSubGroup by Id ", data.responseDescription);
                }
            }, function(error) {
                $log.debug("Error ", error);
            });
        };

        
        vm.ajaxObjectGroupEvent = function(object){
        	 vm.searchDto = {};
     	 vm.searchDto.keyword = object == null ? "" : object;
     	 vm.searchDto.selectedPageNumber = 0;
     	 vm.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
 		return objectGroupMasterFactory.getall.fetch(vm.searchDto).$promise.then(
 				function(data) {
 					if (data.responseCode == "ERR0") {
 						vm.objectGroupMasterList = data.responseObject.searchResult;
 						return vm.objectGroupMasterList;
 					}
 				},
 				function(errResponse) {
 					$log.error('Error while fetching objectGroupMasterList');
 				});
 	    }
        
        /**
         * Go To Add ObjectSubGroup Master.
         */
        vm.addNew = function() {
        	if($rootScope.roleAccess(roleConstant.MASTER_GENERAL_OBJECT_SUBGROUP_CREATE)){
            $state.go("layout.objectSubGroupMasterAdd");
        	}
        }

        
        /**
         * Cancel Add or Edit ObjectSubGroup Master.
         * @param {int} id - The id of a ObjectSubGroup
         */
       
        vm.cancel = function(objId) {
            if(vm.objectSubGroupMasterForm!=undefined){
		    	vm.goTocancel(vm.objectSubGroupMasterForm.$dirty,objId);
            }else{
            	vm.goTocancel(false,objId);
            }
        }
       
       vm.goTocancel = function(isDirty,objId){
     		 if (isDirty) {
     		      var newScope = $scope.$new();
     		      newScope.errorMessage = $rootScope.nls["ERR200"];
     		      ngDialog.openConfirm({
     		        template: '<p>{{errorMessage}}</p>' +
     		          '<div class="ngdialog-footer">' +
     		          ' <button type="button" class="btn btn-default btn-property cancel-btn" ng-click="confirm(2)">No</button>' +
     		          '<button type="button" class="btn btn-default btn-property accent-btn " ng-click="confirm(1)">Yes</button>' +
     		          '<button type="button" class="btn btn-default btn-property pull-left grey-cancel-btn" ng-click="confirm(3)">Cancel</button>' +
     		          '</div>',
     		        plain: true,
     		        closeByDocument: false,
     		        scope: newScope,
     		        className: 'ngdialog-theme-default'
     		      }).then(
     		        function(value) {
     		          if (value == 1) {
     		        	 vm.saveOrUpdate();
     		          } else if (value == 2) {
     		        	 $state.go("layout.objectSubGroupMaster", {
     		                submitAction: "Cancelled"
     		            });
     		          } else {
     		            $log.debug("cancelled");
     		          }
     		        });
     		    } else {
     		     $state.go("layout.objectSubGroupMaster", {
                   submitAction: "Cancelled"
               });
     		    }
     	    }

        /**
         * Modify Existing ObjectSubGroup Master.
         * @param {int} id - The id of a Country.
         */
        vm.edit = function(id) {
        	if($rootScope.roleAccess(roleConstant.MASTER_GENERAL_OBJECT_SUBGROUP_MODIFY)){
            $state.go("layout.objectSubGroupMasterEdit", {
                id: id
            });
        	}
        }

        /**
         * Delete Country Master.
         * @param {int} id - The id of a Country.
         */
        vm.delete = function(id) {
        	if($rootScope.roleAccess(roleConstant.MASTER_GENERAL_OBJECT_SUBGROUP_DELETE)){
        	var newScope = $scope.$new();
			newScope.errorMessage = $rootScope.nls["ERR06117"];
	    	ngDialog.openConfirm( {
	                    template : '<p>{{errorMessage}}</p>'
	                    + '<div class="ngdialog-footer">'
	                    + '<button type="button" class="btn btn-default btn-property cancel-btn" ng-click="closeThisDialog(0)">Cancel'
	                    + '<button type="button" class="btn btn-primary btn-property accent-btn" ng-click="confirm(1)">Yes'
	                    + '</button></div>',
	                    plain : true,
	                    scope : newScope,
	                    closeByDocument: false,
	                    className : 'ngdialog-theme-default'
	                }).then( function(value) {
	                	objectSubGroupMasterFactory.delete.query({
	                        id: id
	                    }).$promise.then(function(data) {
	                        if (data.responseCode == 'ERR0') {
	                        	vm.objectSubGroupMasteView=false;
	                        	Notification.success($rootScope.nls["ERR402"]);
	                        	vm.deskTopView = true;
	                            vm.search();
	                        }
	                    }, function(error) {
	                        $log.debug("Error ", error);
	                    });
	                }, function(value) {
	                    $log.debug("delete Cancelled");
	            });
        }
        }
        
        /**
         * Validating field of ObjectSubGroup Master while instant change.
         * @param {errorElement,errorCode}
         */
        vm.validate = function(valElement, code) {
            var validationResponse = objectSubGroupMasterValidationService.validate(vm.objectSubGroupMaster,code);
            if(validationResponse.error == true) {
            	console.log("validationResponse::",validationResponse);
            	console.log("validationResponse.errElement::",validationResponse.errElement);
				$scope.validationErrorAndFocus(validationResponse, validationResponse.errElement);
			} else {
				vm.errorMap.put(valElement, null);
            	validationResponse.error = false;
            }
        }
        
        /**
         * Common Validation Focus Functionality - starts here" 
         */
  		$scope.validationErrorAndFocus = function(valResponse, elemId){
  			vm.errorMap = new Map();
  			if(valResponse.error == true){
  				vm.errorMap.put(valResponse.errElement, valResponse.errMessage);
  			}
  				$scope.navigateToNextField(elemId);
  		}

        /**
         * Create and Update ObjectSubGroup Master.
         */
        vm.saveOrUpdate = function() {
            var validationResponse = objectSubGroupMasterValidationService.validate(vm.objectSubGroupMaster,0);
            if(validationResponse.error == true) {
            	$log.debug("Validation Response -- ", validationResponse);
            	$log.debug('Validation Failed to create ObjectSubGroup..')
				$scope.validationErrorAndFocus(validationResponse, validationResponse.errElement);
			}
            else {
            	if(vm.objectSubGroupMaster.id == null) {
            		var successMessage = $rootScope.nls["ERR400"];
            	} else {
            		var successMessage = $rootScope.nls["ERR401"];
            	}
        		objectSubGroupMasterFactory.create.query(vm.objectSubGroupMaster).$promise.then(function(data) {
		            if (data.responseCode == 'ERR0') {
		            	Notification.success(successMessage);
		            	$state.go('layout.objectSubGroupMaster');
		            } else {
		                $log.debug("Object Sub Group Failed :" + data.responseDescription)
		            }
		        }, function(error) {
		        	 $log.debug("ObjectSubGroup Failed : " + error)
		        });	
            } 
        };
      
        /**
         * Recent History - starts here
         */
        vm.isReloaded = localStorage.isObjectSubGroupMasterReloaded;
        $scope.$on('addObjectSubGroupMasterEvent', function(events, args){
        	$rootScope.unfinishedData = vm.objectSubGroupMaster;
			$rootScope.category = "ObjectSubGroup";
			$rootScope.unfinishedFormTitle = "ObjectSubGroup (New)";
        	if(vm.objectSubGroupMaster != undefined && vm.objectSubGroupMaster != null && vm.objectSubGroupMaster.objectGroupName != undefined && vm.objectSubGroupMaster.objectGroupName != null) {
        		$rootScope.subTitle = vm.objectSubGroupMaster.objectGroupName;
        	} else {
        		$rootScope.subTitle = "Unknown ObjectSubGroup "
        	}
        })
      
	    $scope.$on('editObjectSubGroupMasterEvent', function(events, args){
	    	$rootScope.unfinishedData = vm.objectSubGroupMaster;
			$rootScope.category = "ObjectSubGroup";
			$rootScope.unfinishedFormTitle = "ObjectSubGroup Edit # "+vm.objectSubGroupMaster.objectGroupCode;
	    })
	      
	    $scope.$on('addObjectSubGroupMasterEventReload', function(e, confirmation){
	    	    confirmation.message = "";
	    	    localStorage.reloadFormData = JSON.stringify(vm.objectSubGroupMaster) ;
	    	    localStorage.isObjectSubGroupMasterReloaded = "YES";
	            e.preventDefault();
	    })
	    
	    $scope.$on('editObjectSubGroupMasterEventReload', function(e, confirmation){
	    	    confirmation.message = "";
	    	    localStorage.reloadFormData = JSON.stringify(vm.objectSubGroupMaster) ;
	    	    localStorage.isObjectSubGroupMasterReloaded = "YES";
	            e.preventDefault();
	    	  
	    })
	      


        /**
         * State Identification Handling Here.
         * @param {String} $stateParams.action - VIEW, ADD, EDIT, SEARCH.
         */
        //
        switch ($stateParams.action) {
            case "ADD":
            	 if ($stateParams.fromHistory === 'Yes') {
                     if (vm.isReloaded == "YES") {
                      vm.isReloaded = "NO";
                      localStorage.isObjectSubGroupMasterReloaded = "NO";
                       vm.objectSubGroupMaster = JSON.parse(localStorage.reloadFormData);
                     } else {
                       vm.objectSubGroupMaster = $rootScope.selectedUnfilledFormData;
                     }
                   } else {
                     if (vm.isReloaded == "YES") {
                       vm.isReloaded = "NO";
                       localStorage.isObjectSubGroupMasterReloaded = "NO";
                       vm.objectSubGroupMaster = JSON.parse(localStorage.reloadFormData);
                     } else {
                         $log.debug("Add NotHistory NotReload...")
                     }
                   }
                $rootScope.breadcrumbArr = objectSubGroupMasterDataService.getAddBreadCrumb();
                break;
            case "EDIT":
            	 if (vm.fromHistory === 'Yes') {
                     if (vm.isReloaded == "YES") {
                       vm.isReloaded = "NO";
                       localStorage.isObjectSubGroupMasterReloaded = "NO";
                       vm.objectSubGroupMaster = JSON.parse(localStorage.reloadFormData);
                     } else {
                       vm.objectSubGroupMaster = $rootScope.selectedUnfilledFormData;
                     }
                   } else {
                     if (vm.isReloaded == "YES") {
                       vm.isReloaded = "NO";
                       localStorage.isObjectSubGroupMasterReloaded = "NO";
                       vm.objectSubGroupMaster = JSON.parse(localStorage.reloadFormData);
                     } else {
                    	 vm.view($stateParams.id, false);
                     }
                   }
                $rootScope.breadcrumbArr = objectSubGroupMasterDataService.getEditBreadCrumb();
                break;
            default:
                $rootScope.breadcrumbArr = objectSubGroupMasterDataService.getListBreadCrumb();
                vm.searchInit();
        }

    }
]);
}) ();