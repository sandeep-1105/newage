app.service('objectSubGroupMasterDataService', function($rootScope) {
    this.getHeadArray = function() {
	   return [
			  {
					"name":"#",
					"width":"col-xs-0half",
					"model":"no",
					"search":false
			  },{
					"name":"Name",
					"width":"col-md-1half",
					"model":"objectSubGroupName",
					"search":true,
					"wrap_cell":true,
					"type":"text",
					"sort":true,
					"key":"searchObjectSubGroupName"
				},{
					"name":"Code",
					"width":"col-md-1",
					"model":"objectSubGroupCode",
					"search":true,
					"type":"text",
					"sort":true,
					"key":"searchObjectSubGroupCode"
				},{
					"name":"Group",
					"width":"col-md-1half",
					"model":"objectGroupMaster.objectGroupName",
					"search":true,
					"type":"text",
					"sort":true,
					"key":"searchObjectGroup"
				},{
					"name":"Status",
					"width":"w120px",
					"model":"status",
					"search":true,
					"type":"drop",
					"data":$rootScope.enum['LovStatus'],
					"key":"searchStatus",
					"sort":true
				}];
          };

    
    this.getListBreadCrumb = function() {
        return [{
                label: "Master",
                state: "layout.objectSubGroupMaster"
            },
            {
                label: "Object SubGroup",
                state: "layout.objectSubGroupMaster"
            }
        ];
    }

    this.getAddBreadCrumb = function() {
        return [{
                label: "Master",
                state: "layout.objectSubGroupMaster"
            },
            {
                label: "Object SubGroup",
                state: "layout.objectSubGroupMaster"
            },
            {
                label: "Add Object SubGroup",
                state: null
            }
        ];
    };
    this.getEditBreadCrumb = function() {
        return [{
                label: "Master",
                state: "layout.objectSubGroupMaster"
            },
            {
                label: "Object SubGroup",
                state: "layout.objectSubGroupMaster"
            },
            { 
                label: "Edit Object SubGroup",
                state: null
            }
        ];
    };
   

});