
(function() {

	app.factory("GLAccount",['$resource', function($resource) {
		return $resource("/api/v1/gl/account/get/search/keyword/all", {}, {
			fetch : {
				method : 'POST',
				isArray : false
			}
		});
	}]);

	app.factory("GLAccountOnlySubledgerYes",['$resource', function($resource) {
		return $resource("/api/v1/gl/account/get/search/keyword/Yes", {}, {
			fetch : {
				method : 'POST',
				isArray : false
			}
		});
	}]);

	app.factory("GLAccountOnlySubledgerNo",['$resource', function($resource) {
		return $resource("/api/v1/gl/account/get/search/keyword/No", {}, {
			fetch : {
				method : 'POST',
				isArray : false
			}
		});
	}]);
	
	app.factory("GLAccountOnlySubledgerNoWithBankAccount",['$resource', function($resource) {
		return $resource("/api/v1/gl/account/get/search/keyword/No/Yes", {}, {
			fetch : {
				method : 'POST',
				isArray : false
			}
		});
	}]);
	
	app.factory("GLAccountOnlySubledgerNoWithOutBankAccount",['$resource', function($resource) {
		return $resource("/api/v1/gl/account/get/search/keyword/No/No", {}, {
			fetch : {
				method : 'POST',
				isArray : false
			}
		});
	}]);

})();