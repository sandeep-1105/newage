app.controller('portCtrl',['$rootScope', '$scope', '$window', 'ngDialog', 'PortRemove', 'PortSearch', '$state', '$stateParams', 'roleConstant', 
	function($rootScope, $scope, $window, ngDialog, PortRemove, PortSearch, $state, $stateParams, roleConstant) {

    /************************************************************
     * ux - by Muthu
     * reason - ipad compatibility fix
     *
     * *********************************************************/
    $scope.deskTopView = true;

    /********************* ux fix ends here *****************************/
    $scope.portHeadArr = [{
        "name": "#",
        "width": "col-xs-0half",
        "model": "no",
        "search": false,
    }, {
        "name": "Name",
        "width": "w150px",
        "model": "portName",
        "search": true,
        "wrap_cell": true,
        "type": "text",
        "sort": true,
        "key": "searchPortName"

    }, {
        "name": "Code",
        "width": "w100px",
        "model": "portCode",
        "search": true,
        "type": "text",
        "sort": true,
        "key": "searchPortCode"
    }, {
        "name": "Transport Mode",
        "width": "w100px",
        "model": "transportMode",
        "search": true,
        "type": "drop",
        "data": $rootScope.enum['TransportMode'],
        "sort": true,
        "key": "searchTransportMode"
    }, {
        "name": "Group Code",
        "width": "w80px",
        "model": "portGroupMaster.portGroupCode",
        "search": true,
        "type": "text",
        "wrap_cell": true,
        "sort": true,
        "key": "searchGroupCode"
    }, {
        "name": "Country",
        "width": "w150px",
        "model": "portGroupMaster.countryMaster.countryName",
        "search": true,
        "wrap_cell": true,
        "type": "text",
        "sort": true,
        "key": "searchCountryName"
    }, {
        "name": "Status",
        "width": "w150px",
        "model": "status",
        "search": true,
        "type": "drop",
        "sort": true,
        "data": $rootScope.enum['LovStatus'],
        "key": "searchStatus"
    }]



    $scope.sortSelection = {
        sortKey: "portName",
        sortOrder: "asc"
    }

    $scope.limitArr = [10, 15, 20];
    $scope.page = 0;
    $scope.limit = 10;


    $scope.init = function() {
        $scope.searchDto = {};
        $scope.detailView = false;
        $scope.search();
    };

    $scope.cancel = function() {

        $scope.portMaster = {};

        $scope.showHistory = false;
        $scope.detailView = false;

        /***********************************************************************
         * ux - by Muthu reason - ipad compatibility fix
         * 
         **********************************************************************/
        $scope.deskTopView = true;

        /********************* ux fix ends here *****************************/
    }

    $scope.addPortMaster = function() {
        if ($rootScope.roleAccess(roleConstant.MASTER_GENERAL_PORT_CREATE)) {
            console.log("Add Port Button Pressed...");
            $state.go("layout.addPort");
        }
    }

    $scope.editPortMaster = function() {
        if ($rootScope.roleAccess(roleConstant.MASTER_GENERAL_PORT_MODIFY)) {
            console.log("Edit Port Button Pressed...");
            $state.go("layout.editPort", {
                portId: $scope.portMaster.id
            });
        }
    };



    $scope.deletePortMaster = function() {

        if ($rootScope.roleAccess(roleConstant.MASTER_GENERAL_PORT_DELETE)) {

            var newScope = $scope.$new();
            newScope.errorMessage = $rootScope.nls["ERR233"];
            ngDialog.openConfirm({
                template: '<p>{{errorMessage}}</p>' +
                    '<div class="ngdialog-footer">' +
                    '<button type="button" class="btn btn-default btn-property cancel-btn" ng-click="closeThisDialog(0)">No' +
                    '<button type="button" class="btn btn-primary btn-property accent-btn" ng-click="confirm(1)">Yes' +
                    '</button>' +
                    '</div>',
                plain: true,
                scope: newScope,
                className: 'ngdialog-theme-default'
            }).then(function(value) {

                PortRemove.remove({
                    id: $scope.portMaster.id
                }, function(data) {

                    if (data.responseCode == "ERR0") {

                        console.log("Port deleted Successfully")

                        $scope.cancel();

                        $scope.init();
                        $scope.deskTopView = true;

                    } else {
                        console.log("Port deleted Failed ", data.responseDescription)
                    }
                }, function(error) {
                    console.log("Port deleted Failed : ", error)
                });
            }, function(value) {
                console.log("Port deleted cancelled");
            });
        }
    }

    /* Custom Table related functions */

    $scope.rowSelect = function(portMasterObj) {

        if ($rootScope.roleAccess(roleConstant.MASTER_GENERAL_PORT_VIEW)) {

            console.log("rowSelect is called.");

            $scope.portMaster = portMasterObj;
            $scope.detailView = true;

            /************************************************************
             * ux - by Muthu
             * reason - ipad compatibility fix
             *
             * *********************************************************/

            var windowInner = $window.innerWidth;
            if (windowInner <= 1199) {
                $scope.deskTopView = false;

            } else {
                $scope.deskTopView = true;
            }
            angular.element($window).bind('resize', function() {
                if ($window.innerWidth >= 1200) {
                    $scope.deskTopView = true;
                } else if ($window.innerWidth <= 1199) {
                    if ($scope.portMaster.id != null) {
                        $scope.deskTopView = false;
                    } else {
                        $scope.deskTopView = true;
                    }
                }

                console.log("window resizing..." + $window.innerWidth);
                $scope.$digest();
            });

            /********************* ux fix ends here *****************************/
        }
    }

    $scope.changepage = function(param) {

        console.log("ChangePage...", param);

        $scope.page = param.page;
        $scope.limit = param.size;

        $scope.search();
    }
    $scope.limitChange = function(item) {

        console.log("LimitChange...", item);

        $scope.page = 0;
        $scope.limit = item;

        $scope.search();
    }

    $scope.sortChange = function(param) {

        console.log("SortChange...", param);

        $scope.searchDto.orderByType = param.sortOrder.toUpperCase();
        $scope.searchDto.sortByColumn = param.sortKey;

        $scope.search();
    }

    $scope.changeSearch = function(param) {
        console.log("Change Search ".param);
        for (var attrname in param) {
            $scope.searchDto[attrname] = param[attrname];
        }

        $scope.page = 0;
        $scope.cancel();
        $scope.search();
    }


    $scope.search = function() {

        $scope.searchDto.selectedPageNumber = $scope.page;
        $scope.searchDto.recordPerPage = $scope.limit;

        PortSearch.query($scope.searchDto).$promise.then(function(data, status) {

            $scope.totalRecord = data.responseObject.totalRecord;
            $scope.portArr = data.responseObject.searchResult;

            console.log($scope.portArr);
        });
    }


    if ($stateParams.action == "SEARCH") {

        console.log("Port Master Search Page....");
        $scope.init();
        $rootScope.breadcrumbArr = [{
                label: "Master",
                state: "layout.port"
            },
            {
                label: "Port",
                state: "layout.port"
            }
        ];

    }

}]);