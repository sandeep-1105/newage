(function() {

    app.factory("PortGroupList",['$resource', function($resource) {
        return $resource("/api/v1/portgroupmaster/get/search/keyword", {}, {
            fetch: {
                method: 'POST'
            }
        });
    }]);

    app.factory("PortSearch",['$resource', function($resource) {
        return $resource("/api/v1/portmaster/get/search", {}, {
            query: {
                method: 'POST'
            }
        });
    }]);


    app.factory("PortGroupSearch",['$resource', function($resource) {
        return $resource("/api/v1/portgroupmaster/get/search", {}, {
            query: {
                method: 'POST'
            }
        });
    }]);

    app.factory("PortSearchKeyword",['$resource', function($resource) {
        return $resource("/api/v1/portmaster/get/search/keyword", {}, {
            fetch: {
                method: 'POST'
            }
        });
    }]);


    app.factory("PortSaveOrUpdate",['$resource', function($resource) {
        return $resource("/api/v1/portmaster/create", {}, {
            save: {
                method: 'POST'
            }
        });
    }]);

    app.factory("PortView",['$resource', function($resource) {
        return $resource("/api/v1/portmaster/get/id/:id", {}, {
            get: {
                method: 'GET',
                params: {
                    id: ''
                },
                isArray: false
            }
        });
    }]);


    app.factory("PortRemove",['$resource', function($resource) {
        return $resource("/api/v1/portmaster/delete/:id", {}, {
            remove: {
                method: 'DELETE',
                params: {
                    id: ''
                },
                isArray: false
            }
        });
    }]);


    app.factory("PortGroupSaveOrUpdate",['$resource', function($resource) {
        return $resource("/api/v1/portgroupmaster/create", {}, {
            save: {
                method: 'POST'
            }
        });
    }]);

    app.factory("PortGroupView",['$resource', function($resource) {
        return $resource("/api/v1/portgroupmaster/get/id/:id", {}, {
            get: {
                method: 'GET',
                params: {
                    id: ''
                },
                isArray: false
            }
        });
    }]);


    app.factory("PortGroupRemove",['$resource', function($resource) {
        return $resource("/api/v1/portgroupmaster/delete/:id", {}, {
            remove: {
                method: 'DELETE',
                params: {
                    id: ''
                },
                isArray: false
            }
        });
    }]);


    app.factory("PortByTransportMode",['$resource', function($resource) {
        return $resource("/api/v1/portmaster/get/search/keyword/:transportMode", {}, {
            fetch: {
                method: 'POST',
                params: {
                    transportMode: ''
                },
                isArray: false
            }
        });
    }]);

    app.factory("PortByTransportModeCountry",['$resource', function($resource) {
        return $resource("/api/v1/portmaster/get/searchByCountryCode/keyword", {}, {
            fetch: {
                method: 'POST',

                isArray: false
            }
        });
    }])



    app.factory("PortByPortGroup",['$resource', function($resource) {
        return $resource("/api/v1/portmaster/get/search/:groupId/:transportMode", {}, {
            fetch: {
                method: 'POST',
                params: {
                    groupId: '',
                    transportMode: ''
                },
                isArray: false
            }
        });
    }]);


    app.service("PortService",['PortByTransportMode', '$rootScope', function(PortByTransportMode, $rootScope) {




        this.getPortListByTransportMode = function(keyword, transportMode) {

            searchDto = {};
            searchDto.keyword = keyword == null ? "" : keyword;
            searchDto.selectedPageNumber = 0;
            searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

            return PortByTransportMode.fetch({
                "transportMode": transportMode
            }, searchDto).$promise.then(function(data) {
                    if (data.responseCode == "ERR0") {
                        portList = data.responseObject.searchResult;
                        return portList;
                    }
                },
                function(errResponse) {
                    console.error('Error while fetching Port');
                });
        }



    }]);

})();