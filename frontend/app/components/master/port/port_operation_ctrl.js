app.controller('portOperationCtrl',['$rootScope', '$scope', 'PortGroupList',
    'PortSaveOrUpdate', 'PortView', 'ngProgressFactory', 'ngDialog', '$state',
    '$stateParams', 'appConstant', 'CommonValidationService', 'Notification',
	function($rootScope, $scope, PortGroupList,
    PortSaveOrUpdate, PortView, ngProgressFactory, ngDialog, $state,
    $stateParams, appConstant, CommonValidationService, Notification) {

    $scope.contained_progressbar = ngProgressFactory.createInstance();
    $scope.contained_progressbar.setParent(document.getElementById('port-panel'));
    $scope.contained_progressbar.setAbsolute();



    $scope.init = function() {

        if ($scope.portMaster == undefined) {

            $scope.portMaster = {};
            $scope.portMaster.status = "Active";
            $scope.portMaster.ediList = [{}];
        }

        // Setting to compare on cancel
        $scope.oldData = JSON.stringify($scope.portMaster);
    }


    $scope.cancel = function() {

        if ($scope.oldData !== JSON.stringify($scope.portMaster)) {
            var newScope = $scope.$new();
            newScope.errorMessage = $rootScope.nls["ERR200"];
            ngDialog.openConfirm({
                template: '<p>{{errorMessage}}</p>' +
                    '<div class="ngdialog-footer">' +
                    ' <button type="button" class="btn btn-default btn-property cancel-btn" ng-click="confirm(2)">No</button>' +
                    '<button type="button" class="btn btn-default btn-property accent-btn " ng-click="confirm(1)">Yes</button>' +
                    '<button type="button" class="btn btn-default btn-property pull-left grey-cancel-btn" ng-click="confirm(3)">Cancel</button>' +
                    '</div>',
                plain: true,
                scope: newScope,
                className: 'ngdialog-theme-default'
            }).then(function(value) {
                if (value == 1) {
                    $scope.saveOrUpdate();
                } else {
                    $state.go("layout.port", {
                        submitAction: 'Cancelled'
                    });
                }
            });
        } else {
            $state.go("layout.port", {
                submitAction: 'Cancelled'
            });
        }
    }




    $scope.groupRender = function(item) {
        return {
            label: item.portGroupName,
            item: item
        }
    }

    $scope.ajaxPortGroupEvent = function(object) {

        console.log("ajaxPortGroupEvent ", object);
        $scope.searchDto = {};
        $scope.searchDto.keyword = object == null ? "" : object;
        $scope.searchDto.selectedPageNumber = 0;
        $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
        return PortGroupList.fetch($scope.searchDto).$promise.then(function(data) {
            if (data.responseCode == "ERR0") {
                $scope.portGroupListArr = data.responseObject.searchResult;
                console.log($scope.portGroupListArr)
                return $scope.portGroupListArr;
            }
        }, function(errResponse) {
            console.error('Error while fetching Port Group', errResponse);
        });
    }

    $scope.selectedGroup = function(nextFocus) {
        if ($scope.validatePortMaster(2)) {
            $rootScope.navigateToNextField(nextFocus);
        }
    };


    $scope.saveOrUpdate = function() {


        if ($scope.validatePortMaster(0)) {

            if (!$scope.portOperation.ediIsValid()) {
                Notification.error($rootScope.nls["ERR111"]);
                $rootScope.navigateToNextField("ediType0");
                return false;
            }

            $scope.contained_progressbar.start();

            PortSaveOrUpdate.save($scope.portMaster).$promise.then(function(data) {

                if (data.responseCode == "ERR0") {
                    console.log("Port updated Successfully")
                    Notification.success($rootScope.nls["ERR401"]);
                    $state.go("layout.port", {
                        submitAction: 'Saved'
                    });
                } else {
                    console.log("Port updated Failed ", data.responseDescription);
                }

                $scope.contained_progressbar.complete();
                angular.element(".panel-body").animate({
                    scrollTop: 0
                }, "slow");

            }, function(error) {
                console.log("Port updated Failed : ", error);
            });
        } else {
            console.log("Validation Failed");
        }

    }




    $scope.validatePortMaster = function(validateCode) {

        $scope.errorMap = new Map();

        if (validateCode == 0 || validateCode == 1) {
            if ($scope.portMaster.transportMode == undefined ||
                $scope.portMaster.transportMode == null ||
                $scope.portMaster.transportMode == "") {
                $scope.errorMap.put("transportMode", $rootScope.nls["ERR1511"]);
                return false;
            }
        }

        if (validateCode == 0 || validateCode == 2) {
            if ($scope.portMaster.portName == undefined ||
                $scope.portMaster.portName == null ||
                $scope.portMaster.portName == "") {
                $scope.errorMap.put("portName", $rootScope.nls["ERR1509"]);
                return false;
            } else {
                if (!CommonValidationService.checkRegExp(appConstant.Reg_Exp_Master_PORT_NAME, $scope.portMaster.portName)) {
                    $scope.errorMap.put("portName", $rootScope.nls["ERR1507"]);
                    return false;
                }
            }
        }

        if (validateCode == 0 || validateCode == 3) {
            if ($scope.portMaster.portCode == undefined ||
                $scope.portMaster.portCode == null ||
                $scope.portMaster.portCode == "") {
                $scope.errorMap.put("portCode", $rootScope.nls["ERR1508"]);
                return false;
            } else if ($scope.portMaster.transportMode == 'Ocean' &&
                $scope.portMaster.portCode != null &&
                $scope.portMaster.portCode != undefined &&
                $scope.portMaster.portCode != "") {
                if ($scope.portMaster.portGroupMaster.countryMaster.countryCode != null ||
                    $scope.portMaster.portGroupMaster.countryMaster.countryCode != undefined) {
                    var CountryCode = $scope.portMaster.portGroupMaster.countryMaster.countryCode.substring(0, 2);
                    var portCode = $scope.portMaster.portCode.substring(0, 2);
                    if (CountryCode != portCode) {
                        $scope.errorMap.put("portCode", $rootScope.nls["ERR1517"]);
                        return false;
                    }
                }

                if (!CommonValidationService.checkRegExp(appConstant.Reg_Exp_Master_PORT_CODE_SEA, $scope.portMaster.portCode)) {
                    $scope.errorMap.put("portCode", $rootScope.nls["ERR1506"]);
                    return false;
                }
            } else if ($scope.portMaster.transportMode == 'Air' &&
                $scope.portMaster.portCode != null &&
                $scope.portMaster.portCode != undefined &&
                $scope.portMaster.portCode != "") {

                if (!CommonValidationService.checkRegExp(appConstant.Reg_Exp_Master_PORT_CODE_AIR, $scope.portMaster.portCode)) {
                    $scope.errorMap.put("portCode", $rootScope.nls["ERR1506"]);
                    return false;
                }

            }

        }

        if (validateCode == 0 || validateCode == 4) {

            if ($scope.portMaster.portGroupMaster == undefined ||
                $scope.portMaster.portGroupMaster == null ||
                $scope.portMaster.portGroupMaster.id == null) {
                $scope.errorMap.put("group", $rootScope.nls["ERR1510"]);
                return false;
            } else {


                if ($scope.portMaster.portGroupMaster.status == 'Block') {
                    $scope.errorMap.put("group", $rootScope.nls["ERR1528"]);
                    return false;
                }

                if ($scope.portMaster.portGroupMaster.status == 'Hide') {
                    $scope.errorMap.put("group", $rootScope.nls["ERR1528"]);
                    return false;
                }



            }
        }

        return true;
    }



    $scope.setStatus = function(data) {
        if (data == 'Active') {
            return 'activetype';
        } else if (data == 'Block') {
            return 'blockedtype';
        } else if (data == 'Hide') {
            return 'hiddentype';
        }
    }



    $scope.getPortById = function(portId) {
        console.log("getPortById ", portId);


        PortView.get({
            id: portId
        }, function(data) {
            if (data.responseCode == "ERR0") {
                console.log("Successful while getting port.", data.responseObject);
                $scope.portMaster = data.responseObject;

                if ($scope.portMaster.ediList == undefined || $scope.portMaster.ediList == null || $scope.portMaster.ediList.length == 0) {
                    $scope.portMaster.ediList = [{}];
                }
                $scope.init();
            }
        }, function(error) {
            console.log("Error while getting port.", error);
        });
    }

    $scope.$on('addPortEvent', function(events, args) {

        $rootScope.unfinishedData = $scope.portMaster;
        $rootScope.unfinishedFormTitle = "Port Master (New)";
        $rootScope.category = "Port Master";


        if ($scope.portMaster != undefined &&
            $scope.portMaster != null &&
            $scope.portMaster.portName != undefined &&
            $scope.portMaster.portName != null &&
            $scope.portMaster.portName != "") {
            $rootScope.subTitle = $scope.portMaster.portName;
        } else {
            $rootScope.subTitle = "Unknown Port Master"
        }
    });


    $scope.$on('editPortEvent', function(events, args) {
        $rootScope.unfinishedData = $scope.portMaster;
        $rootScope.unfinishedFormTitle = "Port Master Edit (" + $scope.portMaster.portCode + ")";
        $rootScope.category = "Port Master";

        if ($scope.portMaster != undefined &&
            $scope.portMaster != null &&
            $scope.portMaster.portName != undefined &&
            $scope.portMaster.portName != null &&
            $scope.portMaster.portName != "") {
            $rootScope.subTitle = $scope.portMaster.portName;
        } else {
            $rootScope.subTitle = "Unknown Port Master"
        }
    });



    $scope.$on('addPortEventReload', function(e, confirmation) {
        confirmation.message = "";
        localStorage.reloadFormData = JSON.stringify($scope.portMaster);
        localStorage.isPortReloaded = "YES";
        e.preventDefault();
    });

    $scope.$on('editPortEventReload', function(e, confirmation) {
        confirmation.message = "";
        localStorage.reloadFormData = JSON.stringify($scope.portMaster);
        localStorage.isPortReloaded = "YES";
        e.preventDefault();
    });

    $scope.isReloaded = localStorage.isPortReloaded;

    if ($stateParams.action == "ADD") {
        if ($stateParams.fromHistory === 'Yes') {
            if ($scope.isReloaded == "YES") {
                console.log("Add History Reload...")
                $scope.isReloaded = "NO";
                localStorage.isPortReloaded = "NO";
                $scope.portMaster = JSON.parse(localStorage.reloadFormData);
            } else {
                console.log("Add History Not Reload...")
                $scope.portMaster = $rootScope.selectedUnfilledFormData;
            }
        } else {
            if ($scope.isReloaded == "YES") {
                console.log("Add Not History Reload...");
                $scope.isReloaded = "NO";
                localStorage.isPortReloaded = "NO";
                $scope.portMaster = JSON.parse(localStorage.reloadFormData);
            } else {
                console.log("Add Not History Not Reload...");
                $scope.init();
            }
        }

        $rootScope.breadcrumbArr = [{
                label: "Master",
                state: "layout.port"
            },
            {
                label: "Port",
                state: "layout.port"
            },
            {
                label: "Add Port",
                state: null
            }
        ];

        $rootScope.navigateToNextField("transportMode");

    } else {

        if ($stateParams.fromHistory === 'Yes') {

            if ($scope.isReloaded == "YES") {

                console.log("Edit History Reload");
                $scope.isReloaded = "NO";
                sessionStorage.isPortReloaded = "NO";
                $scope.portMaster = JSON.parse(localStorage.reloadFormData);

            } else {

                console.log("Edit History Not Reload");
                $scope.portMaster = $rootScope.selectedUnfilledFormData;

            }



        } else {

            if ($scope.isReloaded == "YES") {

                console.log("Edit Not History Reload");
                $scope.isReloaded = "NO";
                localStorage.isPortReloaded = "NO";
                $scope.portMaster = JSON.parse(localStorage.reloadFormData);

            } else {

                console.log("Edit Not History Not Reload");
                $scope.getPortById($stateParams.portId);

            }

        }

        $rootScope.breadcrumbArr = [{
                label: "Master",
                state: "layout.port"
            },
            {
                label: "Port",
                state: "layout.port"
            },
            {
                label: "Edit Port",
                state: null
            }
        ];
        $rootScope.navigateToNextField("transportMode");
    }

}]);