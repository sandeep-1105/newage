(function() {
	
	
	app.factory("costCenterSearch",['$resource', function($resource) {
		return $resource("/api/v1/costcentermaster/get/search", {}, {
			query : {
				method : 'POST'
			}
		});
	}]);
	
	
	app.factory("CostCenterSearchKeyword",['$resource', function($resource) {
		return $resource("/api/v1/costcentermaster/get/search/keyword", {}, {
			fetch : {
				method : 'POST'
			}
		});
	}]);


	
	app.factory("CostCenterSave",['$resource', function($resource) {
		return $resource("/api/v1/costcentermaster/create", {}, {
			save : {
				method : 'POST'
			}
		});
	}]);

	app.factory("CostCenterUpdate",['$resource', function($resource) {
		return $resource("/api/v1/costcentermaster/update", {}, {
			update : {
				method : 'POST'
			}
		});
	}]);

	app.factory("costCenterView",['$resource', function($resource) {
		return $resource("/api/v1/costcentermaster/get/id/:id", {}, {
			get : {
				method : 'GET',
				params : {
					id : ''
				},
				isArray : false
			}
		});
	}]);
	
	app.factory("costCenterRemove",['$resource', function($resource) {
		return $resource("/api/v1/costcentermaster/delete/:id", {}, {
			remove : {
				method : 'DELETE',
				params : {
					id : ''
				},
				isArray : false
			}
		});
	}]);
	
	
})();
