app.controller('costCenterOperaionCtrl',['$rootScope', '$scope', '$http', 'costCenterService', 'CostCenterSave', 'CostCenterUpdate',
    '$location', 'ngProgressFactory', 'ngDialog', 'costCenterView', '$state', '$stateParams', 'RecentHistorySaveService', 
    'appConstant', 'CommonValidationService', 
	function($rootScope, $scope, $http, costCenterService, CostCenterSave, CostCenterUpdate,
    $location, ngProgressFactory, ngDialog, costCenterView, $state, $stateParams, RecentHistorySaveService, 
    appConstant, CommonValidationService) {
    $scope.costCenterMaster = {};

    $scope.init = function(acFlag) {

        $scope.contained_progressbar = ngProgressFactory.createInstance();
        $scope.contained_progressbar.setParent(document.getElementById('costCenter-panel'));
        $scope.contained_progressbar.setAbsolute();
        $rootScope.setNavigate3($scope.costCenterMaster.id != null ? "Edit Cost Center" : "Add Cost Center");

        if ($scope.costCenterMaster.id == null) {
            $scope.costCenterMaster.status = 'Active';
        } else {}

        if (acFlag != undefined && acFlag != null && acFlag === 0) {
            $scope.setOldDataVal();
        }
    };


    $scope.setOldDataVal = function() {
        $scope.oldData = JSON.stringify($scope.costCenterMaster);
        console.log("TosMaster Data Loaded");
    }
    $scope.cancel = function() {
        var tempObj = $scope.costCenterMasterForm;
        if ($scope.oldData != JSON.stringify($scope.costCenterMaster)) {
            if (Object.keys(tempObj).length) {
                var newScope = $scope.$new();
                newScope.errorMessage = $rootScope.nls["ERR200"];
                ngDialog.openConfirm({
                        template: '<p>{{errorMessage}}</p>' +
                            '<div class="ngdialog-footer">' +
                            ' <button type="button" class="btn btn-default btn-property cancel-btn" ng-click="confirm(2)">No</button>' +
                            '<button type="button" class="btn btn-default btn-property accent-btn " ng-click="confirm(1)">Yes</button>' +
                            '<button type="button" class="btn btn-default btn-property pull-left grey-cancel-btn" ng-click="confirm(3)">Cancel</button>' +
                            '</div>',
                        plain: true,
                        scope: newScope,
                        className: 'ngdialog-theme-default'
                    })
                    .then(
                        function(value) {
                            if (value == 1 &&
                                $scope.costCenterMaster.id == null) {
                                $scope.save();
                            } else if (value == 1 &&
                                $scope.costCenterMaster.id != null) {
                                $scope.update();
                            } else if (value == 2) {
                                var params = {};
                                params.submitAction = 'Cancelled';
                                $state.go("layout.costCenter", params);
                            } else {
                                console.log("cancelled");
                            }

                        });
            } else {
                $state.go("layout.costCenter", {
                    submitAction: "Saved"
                });
            }
        } else {
            $state.go("layout.costCenter", {
                submitAction: "Saved"
            });
        }
    }


    $scope.update = function() {
        console.log("Update Method is called.");
        if ($scope.validateCCMaster(0)) {
            $scope.contained_progressbar.start();

            CostCenterUpdate.update($scope.costCenterMaster).$promise.then(
                function(data) {
                    if (data.responseCode == 'ERR0') {
                        $rootScope.successDesc = $rootScope.nls["ERR401"];
                        console.log("Cost Center updated Successfully")
                        $state.go("layout.costCenter", {
                            submitAction: "Saved"
                        });

                    } else {
                        console.log("Cost Center updated Failed " +
                            data.responseDescription)
                    }
                    $scope.contained_progressbar.complete();
                    angular.element(".panel-body").animate({
                        scrollTop: 0
                    }, "slow");
                },
                function(error) {
                    console.log("Cost updated Failed : " + error)
                });
        } else {
            console.log("Invalid Form Submission.");
        }


    }
    $scope.save = function() {
        console.log("Save Method is called.");
        if ($scope.validateCCMaster(0)) {
            $scope.contained_progressbar.start();
            CostCenterSave.save($scope.costCenterMaster).$promise.then(function(data) {
                if (data.responseCode == 'ERR0') {
                    $rootScope.successDesc = $rootScope.nls["ERR400"];
                    $state.go("layout.costCenter", {
                        submitAction: "Saved"
                    });
                } else {
                    console.log("Tos added Failed " + data.responseDescription)
                }
                $scope.contained_progressbar.complete();
                angular.element(".panel-body").animate({
                    scrollTop: 0
                }, "slow");
            }, function(error) {
                console.log("Cost added Failed : " + error)
            });
        } else {
            console.log("Invalid Form Submission.");
        }

    }


    $scope.setStatus = function(data) {
        if (data == 'Active') {
            return 'activetype';
        } else if (data == 'Block') {
            return 'blockedtype';
        } else if (data == 'Hide') {
            return 'hiddentype';
        }
    }

    // cost center Validation started

    $scope.validateCCMaster = function(validateCode) {

        $scope.errorMap = new Map();

        if (validateCode == 0 || validateCode == 1) {

            if ($scope.costCenterMaster.costCenterName == null || $scope.costCenterMaster.costCenterName == undefined || $scope.costCenterMaster.costCenterName == "") {


                $scope.errorMap.put("costCenterName", $rootScope.nls["ERR1109"]);
                return false
            }

            if (!CommonValidationService.checkRegExp(appConstant.Reg_Exp_Master_COST_CENTER_NAME, $scope.costCenterMaster.costCenterName)) {
                $scope.errorMap.put("costCenterName", $rootScope.nls["ERR1107"]);
                return false;
            }

        }
        if (validateCode == 0 || validateCode == 2) {


            if ($scope.costCenterMaster.costCenterCode == null || $scope.costCenterMaster.costCenterCode == undefined || $scope.costCenterMaster.costCenterCode == "") {

                $scope.errorMap.put("costCenterCode", $rootScope.nls["ERR1108"]);
                return false
            }

            if (!CommonValidationService.checkRegExp(appConstant.Reg_Exp_Master_COST_CENTER_CODE, $scope.costCenterMaster.costCenterCode)) {
                $scope.errorMap.put("costCenterCode", $rootScope.nls["ERR1106"]);
                return false;
            }


        }

        return true;


    }




    //On leave the Unfilled Enquiry Form Broadcast then event will happen in StateChangeListener(BroadCaster)-app_state_route.js
    //This block is listener for Add Sales Enquiry Event
    $scope.$on('addCostCenterEvent', function(events, args) {
        $rootScope.unfinishedData = $scope.enquiryLog;
        $rootScope.category = "Cost Center";
        $rootScope.unfinishedFormTitle = "Cost Center (New)";
        if ($scope.costCenterMaster != undefined && $scope.costCenterMaster != null) {
            $rootScope.subTitle = "";
        } else {
            $rootScope.subTitle = "Unknown Party"
        }
    })

    $scope.$on('editCostCenterEvent', function(events, args) {
        $rootScope.unfinishedData = $scope.costCenterMaster;
        $rootScope.category = "Cost Center";
        $rootScope.unfinishedFormTitle = "Cost Center Edit # " + $scope.costCenterMaster;
        if ($scope.costCenterMaster != undefined && $scope.costCenterMaster != null) {
            $rootScope.subTitle = ""; //$scope.costCenterMaster;
        } else {
            $rootScope.subTitle = "Unknown Party"
        }
    })
    $scope.$on('addCostCenterEventReload', function(e, confirmation) {
        confirmation.message = "All data willl be lost.";
        localStorage.reloadFormData = JSON.stringify($scope.costCenterMaster);
        localStorage.isCCReloaded = "YES";
        e.preventDefault();
    });
    $scope.$on('editCostCenterEventReload', function(e, confirmation) {
        confirmation.message = "All data willl be lost.";
        localStorage.reloadFormData = JSON.stringify($scope.costCenterMaster);
        localStorage.isCCReloaded = "YES";
        e.preventDefault();
    });
    $scope.isReloaded = localStorage.isCCReloaded;
    if ($stateParams.action == "ADD") {
        $scope.costCenterMaster = {};
        if ($stateParams.fromHistory === 'Yes') {
            if ($scope.isReloaded == "YES") {
                $scope.isReloaded = "NO";
                localStorage.isCCReloaded = "NO";
                $scope.costCenterMaster = JSON.parse(localStorage.reloadFormData);
            } else {
                $scope.costCenterMaster = $rootScope.selectedUnfilledFormData;
            }

            $scope.oldData = "";
        } else {
            if ($scope.isReloaded == "YES") {
                $scope.isReloaded = "NO";
                localStorage.isCCReloaded = "NO";
                $scope.costCenterMaster = JSON.parse(localStorage.reloadFormData);
                $scope.init();
                $scope.oldData = "";
            } else {
                $scope.init(0);
            }
        }
        $rootScope.breadcrumbArr = [{
                label: "Master",
                state: "layout.costCenter"
            },
            {
                label: "Cost Center",
                state: "layout.costCenter"
            },
            {
                label: "Add Cost Center",
                state: null
            }
        ];
    } else {
        if ($stateParams.fromHistory === 'Yes') {
            if ($scope.isReloaded == "YES") {
                $scope.isReloaded = "NO";
                localStorage.isCCReloaded = "NO";
                $scope.costCenterMaster = JSON.parse(localStorage.reloadFormData);
            } else {
                $scope.costCenterMaster = $rootScope.selectedUnfilledFormData;
            }

            $scope.oldData = "";
        } else {
            if ($scope.isReloaded == "YES") {
                $scope.isReloaded = "NO";
                localStorage.isCCReloaded = "NO";
                $scope.costCenterMaster = JSON.parse(localStorage.reloadFormData);
                $scope.oldData = "";
            } else {
                costCenterView.get({
                    id: $stateParams.costCenterId
                }, function(data) {
                    if (data.responseCode == "ERR0") {
                        console.log("CostCenter view Successfully")
                        $scope.costCenterMaster = data.responseObject;

                        $rootScope.unfinishedFormTitle = "Cost Center Edit # " + $scope.costCenterMaster.costCenterCode;
                        var rHistoryObj = {
                            'title': 'Cost Center Edit # ' + $scope.costCenterMaster.costCenterCode,
                            'subTitle': $scope.costCenterMaster.costCenterCode,
                            'stateName': $state.current.name,
                            'stateParam': JSON.stringify($stateParams),
                            'stateCategory': 'CostCenter',
                            'serviceType': 'AIR',
                            'serviceCode': "",
                            'orginAndDestination': ""
                        }
                        $scope.setOldDataVal();
                        RecentHistorySaveService.form(rHistoryObj);
                    } else {
                        console.log("CostCenter view Failed " + data.responseDescription)
                    }
                }, function(error) {
                    console.log("CostCenter view Failed : " + error)
                });
            }
        }
        $scope.init();
        $rootScope.breadcrumbArr = [{
                label: "Master",
                state: "layout.costCenter"
            },
            {
                label: "Cost Center",
                state: "layout.costCenter"
            },
            {
                label: "Edit Cost Center",
                state: null
            }
        ];
    }

}]);