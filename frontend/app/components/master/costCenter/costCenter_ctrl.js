app.controller('costCenterCtrl',['$rootScope', '$scope', '$http', '$location', '$window', '$modal', 'costCenterService', 'costCenterRemove',
        'costCenterSearch', 'ngTableParams', 'ngDialog', '$state', '$stateParams', 'roleConstant',
    function($rootScope, $scope, $http, $location, $window, $modal, costCenterService, costCenterRemove,
        costCenterSearch, ngTableParams, ngDialog, $state, $stateParams, roleConstant) {


        /************************************************************
         * ux - by Muthu
         * reason - ipad compatibility fix
         *
         * *********************************************************/
        $scope.deskTopView = true;

        /********************* ux fix ends here *****************************/

        $scope.costCenterHeadArr = [{
                "name": "#",
                "width": "col-xs-0half",
                "model": "no",
                "search": false,
            },
            {
                "name": "Name",
                "width": "col-xs-6",
                "model": "costCenterName",
                "search": true,
                "wrap_cell": true,
                "type": "text",
                "sort": true,
                "key": "searchcostCenterName"
            },
            {
                "name": "Code",
                "width": "col-xs-2half",
                "model": "costCenterCode",
                "search": true,
                "type": "text",
                "wrap_cell": true,
                "sort": true,
                "key": "searchcostCenterCode"
            },
            {
                "name": "Status",
                "width": "col-xs-3",
                "model": "status",
                "search": true,
                "type": "drop",
                "sort": true,
                "data": $rootScope.enum['LovStatus'],
                "key": "searchStatus"
            }
        ]

        $scope.sortSelection = {
            sortKey: "costCenterName",
            sortOrder: "asc"
        }



        $scope.costCenterMaster = {};

        $scope.searchDto = {};
        $scope.limitArr = [10, 15, 20];
        $scope.page = 0;
        $scope.limit = 10;
        costCenterService.set({});


        $scope.init = function() {
            $rootScope.setNavigate1("Master");
            $rootScope.setNavigate2("Cost Center");
            $scope.search();
            $scope.searchDto = {};
            $scope.costCenterMaster = {};
            if ($scope.costCenterMaster.id == null)
                $scope.costCenterMaster.status = 'Active';
            $scope.searchDto.orderByType = $scope.sortSelection.sortOrder.toUpperCase();
            $scope.searchDto.sortByColumn = $scope.sortSelection.sortKey;

        };
        $scope.limitChange = function(item) {
            console.log("Limit Change is called...", item);
            $scope.page = 0;
            $scope.limit = item;
            $scope.cancel();
            $scope.search();
        }

        $scope.rowSelect = function(data) {
            if ($rootScope.roleAccess(roleConstant.MASTER_FINANCE_COST_CENTER_VIEW)) {
                $scope.showLogs = false;
                $scope.costCenterMaster = data;
                console.log("data", data);
                /************************************************************
                 * ux - by Muthu
                 * reason - ipad compatibility fix
                 *
                 * *********************************************************/

                var windowInner = $window.innerWidth;
                if (windowInner <= 1199) {
                    $scope.deskTopView = false;

                } else {
                    $scope.deskTopView = true;
                }
                angular.element($window).bind('resize', function() {
                    if ($window.innerWidth >= 1200) {
                        $scope.deskTopView = true;
                    } else if ($window.innerWidth <= 1199) {
                        if ($scope.costCenterMaster.id != null) {
                            $scope.deskTopView = false;
                        } else {
                            $scope.deskTopView = true;
                        }
                    }

                    console.log("window resizing..." + $window.innerWidth);
                    $scope.$digest();
                });

                /********************* ux fix ends here *****************************/

            }
        }

        $scope.changeSearch = function(param) {
            console.log("Change Search", param)
            for (var attrname in param) {
                $scope.searchDto[attrname] = param[attrname];
            }
            $scope.page = 0;
            $scope.cancel();
            $scope.search();
            console.log("change search", $scope.searchDto);
        }

        $scope.sortChange = function(param) {
            console.log("Sort Change Called.", param);
            $scope.searchDto.orderByType = param.sortOrder.toUpperCase();
            $scope.searchDto.sortByColumn = param.sortKey;
            $scope.cancel();
            $scope.search();
        }




        $scope.changepage = function(param) {
            console.log("Change Page Called.", param);
            $scope.page = param.page;
            $scope.limit = param.size;
            $scope.search();
        }


        $scope.addNewCC = function() {
            console.log("Add Button is Pressed.");
            if ($rootScope.roleAccess(roleConstant.MASTER_FINANCE_COST_CENTER_CREATE)) {
                costCenterService.set({});
                $state.go("layout.addCostCenter");
            }
        }

        $scope.serialNo = function(index) {
            index = ($scope.searchDto.selectedPageNumber * $scope.searchDto.recordPerPage) + (index + 1);
            return index;
        }


        $scope.cancel = function() {
            $scope.showLogs = false;
            $scope.costCenterMaster = {};
            /************************************************************
             * ux - by Muthu
             * reason - ipad compatibility fix
             *
             * *********************************************************/
            $scope.deskTopView = true;

            /********************* ux fix ends here *****************************/

        }


        $scope.search = function() {
            $scope.costCenterMaster = {};

            $scope.searchDto.selectedPageNumber = $scope.page;
            $scope.searchDto.recordPerPage = $scope.limit;
            $scope.costCenterArr = [];
            costCenterSearch.query($scope.searchDto).$promise.then(function(
                data, status) {
                $scope.totalRecord = data.responseObject.totalRecord;
                console.log($scope.totalRecord);
                var tempArr = [];
                var resultArr = [];
                tempArr = data.responseObject.searchResult;
                console.log("tempArr", tempArr);
                var tempObj = {};
                angular.forEach(tempArr, function(item, index) {
                    tempObj = item;
                    tempObj.no = (index + 1) + ($scope.page * $scope.limit);
                    resultArr.push(tempObj);
                    tempObj = {};
                });

                $scope.costCenterArr = resultArr;
            });

        }


        $scope.editCC = function() {
            if ($rootScope.roleAccess(roleConstant.MASTER_FINANCE_COST_CENTER_MODIFY)) {
                costCenterService.set($scope.costCenterMaster);
                $state.go("layout.editCostCenter", {
                    costCenterId: $scope.costCenterMaster.id
                });
                //SANDI
            }
        }

        $scope.deleteCC = function() {
            if ($rootScope.roleAccess(roleConstant.MASTER_FINANCE_COST_CENTER_DELETE)) {
                var newScope = $scope.$new();
                newScope.errorMessage = $rootScope.nls["ERR242"];
                ngDialog
                    .openConfirm({
                        template: '<p>{{errorMessage}}</p>' +
                            '<div class="ngdialog-footer">' +
                            '<button type="button" class="btn btn-default btn-property cancel-btn" ng-click="closeThisDialog(0)">Cancel' +
                            '<button type="button" class="btn btn-primary btn-property accent-btn" ng-click="confirm(1)">Yes' +
                            '</button></div>',
                        plain: true,
                        scope: newScope,
                        className: 'ngdialog-theme-default'
                    })
                    .then(
                        function(value) {
                            costCenterRemove
                                .remove({
                                        id: $scope.costCenterMaster.id
                                    },
                                    function(data) {
                                        if (data.responseCode == 'ERR0') {
                                            console.log("Cost deleted Successfully")
                                            $scope.init();
                                            $scope.deskTopView = true;
                                        } else {
                                            console.log("Cost deleted Failed " + data.responseDescription)
                                        }
                                    },
                                    function(error) {
                                        console.log("Cost deleted Failed : " + error)
                                    });
                        },
                        function(value) {
                            console.log("deleted cancelled");
                        });

            }
        }
        switch ($stateParams.action) {
            case "SEARCH":
                $scope.init();
                $rootScope.breadcrumbArr = [{
                        label: "Master",
                        state: "layout.costCenter"
                    },
                    {
                        label: "Cost Center",
                        state: "layout.costCenter"
                    }
                ];
                break;
        }

    }]);