app.controller('companyMasterOperationCtrl', ['$rootScope', '$scope', 'CompanyAdd', 'CompanyEdit', 'ngProgressFactory', 
		'ngDialog', '$state', '$stateParams', 'CommonValidationService', 'appConstant', 'CompanyView',
	function($rootScope, $scope, CompanyAdd, CompanyEdit, ngProgressFactory, 
		ngDialog, $state, $stateParams, CommonValidationService, appConstant, CompanyView) {

    $scope.contained_progressbar = ngProgressFactory.createInstance();
    $scope.contained_progressbar.setParent(document.getElementById('company-panel'));
    $scope.contained_progressbar.setAbsolute();

    $scope.init = function() {

        if ($scope.companyMaster == undefined || $scope.companyMaster == null) {
            $scope.companyMaster = {};
            $scope.companyMaster.status = 'Active';
        }

        $scope.setOldDataVal();
    };

    $scope.setOldDataVal = function() {
        $scope.oldData = JSON.stringify($scope.companyMaster);
    }


    $scope.cancel = function() {

        if ($scope.oldData != JSON.stringify($scope.companyMaster)) {
            var newScope = $scope.$new();
            newScope.errorMessage = $rootScope.nls["ERR200"];
            ngDialog.openConfirm({
                template: '<p>{{errorMessage}}</p>' +
                    '<div class="ngdialog-footer">' +
                    ' <button type="button" class="btn btn-default btn-property cancel-btn" ng-click="confirm(2)">No</button>' +
                    '<button type="button" class="btn btn-default btn-property accent-btn " ng-click="confirm(1)">Yes</button>' +
                    '<button type="button" class="btn btn-default btn-property pull-left grey-cancel-btn" ng-click="confirm(3)">Cancel</button>' +
                    '</div>',
                plain: true,
                scope: newScope,
                className: 'ngdialog-theme-default'
            }).then(function(value) {
                if (value == 1 && $scope.companyMaster.id == null) {
                    $scope.save();
                } else if (value == 1 && $scope.companyMaster.id != null) {
                    $scope.update();
                } else if (value == 2) {
                    var params = {};
                    params.submitAction = 'Cancelled';
                    $state.go("layout.company", params);
                } else {
                    console.log("cancelled");
                }
            });
        } else {
            var params = {};
            params.submitAction = 'Cancelled';
            $state.go("layout.company", params);
        }
    };

    $scope.update = function() {
        console.log("Update Method is called.");
        if ($scope.validateCompanyMaster(0)) {
            $scope.contained_progressbar.start();
            CompanyEdit.update($scope.companyMaster).$promise.then(function(data) {
                if (data.responseCode == 'ERR0') {
                    console.log("Company updated Successfully")
                    $rootScope.successDesc = $rootScope.nls["ERR401"];
                    var params = {}
                    params.submitAction = 'Saved';
                    $state.go("layout.company", params);
                } else {
                    console.log("Company updated Failed ", data.responseDescription)
                }
                $scope.contained_progressbar.complete();
                angular.element(".panel-body").animate({
                    scrollTop: 0
                }, "slow");
            }, function(error) {
                console.log("Company updated Failed : " + error)
            });
        } else {
            console.log("Invalid Form Submission.");
        }
    }

    $scope.save = function() {
        console.log("Save Method is called.");
        if ($scope.validateCompanyMaster(0)) {

            $scope.contained_progressbar.start();
            CompanyAdd.save($scope.companyMaster).$promise.then(function(data) {
                if (data.responseCode == 'ERR0') {
                    $rootScope.successDesc = $rootScope.nls["ERR400"];
                    var params = {}
                    params.submitAction = 'Saved';
                    $state.go("layout.company", params);
                } else {
                    console.log("Company added Failed " + data.responseDescription)
                }
                $scope.contained_progressbar.complete();
                angular.element(".panel-body").animate({
                    scrollTop: 0
                }, "slow");
            }, function(error) {
                console.log("Company added Failed : " + error)
            });
        } else {
            console.log("Invalid Form Submission.");
        }
    }


    $scope.validateCompanyMaster = function(validateCode) {

        console.log("Validate Called " + validateCode);

        $scope.errorMap = new Map();

        if (validateCode == 0 || validateCode == 1) {

            if ($scope.companyMaster.companyName == undefined ||
                $scope.companyMaster.companyName == null ||
                $scope.companyMaster.companyName == "") {
                $scope.errorMap.put("companyName", $rootScope.nls["ERR1008"]);

                return false;
            } else {

                if (!CommonValidationService.checkRegExp(appConstant.Reg_Exp_Master_Company_Name, $scope.companyMaster.companyName)) {
                    $scope.errorMap.put("companyName", $rootScope.nls["ERR1005"]);
                    return false;
                }
            }
        }


        if (validateCode == 0 || validateCode == 2) {

            if ($scope.companyMaster.companyCode == undefined ||
                $scope.companyMaster.companyCode == null ||
                $scope.companyMaster.companyCode == "") {
                $scope.errorMap.put("companyCode", $rootScope.nls["ERR1007"]);

                return false;
            } else {

                if (!CommonValidationService.checkRegExp(appConstant.Reg_Exp_Master_Company_Code, $scope.companyMaster.companyCode)) {
                    $scope.errorMap.put("companyCode", $rootScope.nls["ERR1004"]);
                    return false;
                }
            }
        }

        return true;

    }




    $scope.setStatus = function(data) {
        if (data == 'Active') {
            return 'activetype';
        } else if (data == 'Block') {
            return 'blockedtype';
        } else if (data == 'Hide') {
            return 'hiddentype';
        }
    }




    $scope.getCompanyById = function(companyId) {
        console.log("getCompanyById ", companyId);

        CompanyView.get({
            id: companyId
        }, function(data) {
            if (data.responseCode == "ERR0") {
                console.log("Successful while getting Company.", data)
                $scope.companyMaster = data.responseObject;
            }

            $scope.setOldDataVal();
        }, function(error) {
            console.log("Error while getting Company.", error)
        });

    }




    //On leave the Unfilled Company Form Broadcast then event will happen in StateChangeListener(BroadCaster)-app_state_route.js

    $scope.$on('addCompanyEvent', function(events, args) {

        $rootScope.unfinishedData = $scope.companyMaster;
        $rootScope.category = "Company";
        $rootScope.unfinishedFormTitle = "Company (New)";
        if ($scope.companyMaster != undefined && $scope.companyMaster != null && $scope.companyMaster.companyName != undefined &&
            $scope.companyMaster.companyName != null && $scope.companyMaster.companyName != "") {
            $rootScope.subTitle = $scope.companyMaster.companyName;
        } else {
            $rootScope.subTitle = "Unknown Company"
        }
    })

    $scope.$on('editCompanyEvent', function(events, args) {

        $rootScope.unfinishedData = $scope.companyMaster;
        $rootScope.category = "Company";
        $rootScope.unfinishedFormTitle = "Company Edit (" + $scope.companyMaster.companyCode + ")";
        if ($scope.companyMaster != undefined && $scope.companyMaster != null && $scope.companyMaster.companyName != undefined &&
            $scope.companyMaster.companyName != null && $scope.companyMaster.companyName != "") {
            $rootScope.subTitle = $scope.companyMaster.companyName;
        } else {
            $rootScope.subTitle = "Unknown Company"
        }
    })


    $scope.$on('addCompanyEventReload', function(e, confirmation) {
        confirmation.message = "";
        localStorage.reloadFormData = JSON.stringify($scope.companyMaster);
        localStorage.isCompanyReloaded = "YES";
        e.preventDefault();
    });


    $scope.$on('editCompanyEventReload', function(e, confirmation) {
        confirmation.message = "";
        localStorage.reloadFormData = JSON.stringify($scope.companyMaster);
        localStorage.isCompanyReloaded = "YES";
        e.preventDefault();
    });


    $scope.isReloaded = localStorage.isCompanyReloaded;

    if ($stateParams.action == "ADD") {
        if ($stateParams.fromHistory === 'Yes') {
            if ($scope.isReloaded == "YES") {
                console.log("Add History Reload...")
                $scope.isReloaded = "NO";
                localStorage.isCompanyReloaded = "NO";
                $scope.CompanyMaster = JSON.parse(localStorage.reloadFormData);
                $scope.init();
            } else {
                console.log("Add History NotReload...")
                $scope.companyMaster = $rootScope.selectedUnfilledFormData;
                $scope.init();
            }
            $scope.oldData = "";
        } else {
            if ($scope.isReloaded == "YES") {
                $scope.isReloaded = "NO";
                localStorage.isCompanyReloaded = "NO";
                $scope.companyMaster = JSON.parse(localStorage.reloadFormData);
                $scope.init();
                $scope.oldData = "";
                console.log("Add NotHistory Reload...")
            } else {
                console.log("Add NotHistory NotReload...")
                $scope.init(0);
            }
        }
        $rootScope.breadcrumbArr = [{
            label: "Master",
            state: "layout.company"
        }, {
            label: "Company",
            state: "layout.company"
        }, {
            label: "Add Company",
            state: null
        }];

    } else {
        if ($stateParams.fromHistory === 'Yes') {
            if ($scope.isReloaded == "YES") {
                $scope.isReloaded = "NO";
                localStorage.isCompanyReloaded = "NO";
                $scope.companyMaster = JSON.parse(localStorage.reloadFormData);
                $scope.init();
            } else {
                $scope.companyMaster = $rootScope.selectedUnfilledFormData;
                $scope.init();
            }
            $scope.oldData = "";
        } else {
            if ($scope.isReloaded == "YES") {
                $scope.isReloaded = "NO";
                localStorage.isCompanyReloaded = "NO";
                $scope.companyMaster = JSON.parse(localStorage.reloadFormData);
                $scope.init();
                $scope.oldData = "";
            } else {
                $scope.getCompanyById($stateParams.companyId);
                $scope.init();
            }
        }
        $rootScope.breadcrumbArr = [{
            label: "Master",
            state: "layout.company"
        }, {
            label: "Company",
            state: "layout.company"
        }, {
            label: "Edit Company",
            state: null
        }];
    }


}]);