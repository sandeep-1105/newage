
(function() {
	
	app.factory("CompanySearch",['$resource', function($resource) {
		return $resource("/api/v1/companymaster/get/search", {}, {
			query : {
				method : 'POST'
			}
		});
	}]);
	
	app.factory("CompanySearchKeyword",['$resource', function($resource) {
		return $resource("/api/v1/companymaster/get/search/keyword", {}, {
			fetch : {
				method : 'POST'
			}
		});
	}]);

	app.factory("CompanyAdd",['$resource', function($resource) {
		return $resource("/api/v1/companymaster/create", {}, {
			save : {
				method : 'POST'
			}
		});
	}]);

	app.factory("CompanyEdit",['$resource', function($resource) {
		return $resource("/api/v1/companymaster/update", {}, {
			update : {
				method : 'POST'
			}
		});
	}]);

	app.factory("CompanyView",['$resource', function($resource) {
		return $resource("/api/v1/companymaster/get/id/:id", {}, {
			get : {
				method : 'GET',
				params : {
					id : ''
				},
				isArray : false
			}
		});
	}]);

	app.factory("CompanyRemove",['$resource', function($resource) {
		return $resource("/api/v1/companymaster/delete/:id", {}, {
			remove : {
				method : 'DELETE',
				params : {
					id : ''
				},
				isArray : false
			}
		});
	}]);
	
	
	app.factory("CompanyList",['$resource', function($resource) {
		return $resource("/api/v1/companymaster/get/search/keyword", {}, {
			fetch : {
				method : 'POST'
			}
		});
	}]);
	
	app.factory("DivisionListByCompany",['$resource', function($resource) {
		return $resource("/api/v1/divisionmaster/get/getallcompanywise/:companyId", {}, {
			fetch : {
				method : 'POST',
				params : {
					companyId : ''
				},
				isArray : false
			}
		});
	}]);
	
	
	app.factory("DivisionList", ['$resource',function($resource) {
		return $resource("/api/v1/divisionmaster/get/search/keyword", {}, {
			fetch : {
				method : 'POST'
			}
		});
	}]);
	
})();
