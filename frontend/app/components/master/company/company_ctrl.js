app.controller('companyMasterController',['$rootScope', '$scope', '$window', 
	'ngTableParams', 'CompanySearch', 'ngDialog', 'CompanyRemove', '$stateParams', '$state',
    function($rootScope, $scope, $window, ngTableParams, 
    	CompanySearch, ngDialog, CompanyRemove, $stateParams, $state) {

        /************************************************************
         * ux - by Muthu
         * reason - ipad compatibility fix
         *
         * *********************************************************/
        $scope.deskTopView = true;

        /********************* ux fix ends here *****************************/
        $scope.companyHeadArr = [{
                "name": "#",
                "width": "col-xs-0half",
                "model": "no",
                "search": false,
            },
            {
                "name": "Name",
                "width": "col-xs-5",
                "model": "companyName",
                "search": true,
                "wrap_cell": true,
                "type": "text",
                "sort": true,
                "key": "searchCompanyName"

            },
            {
                "name": "Code",
                "width": "col-xs-2",
                "model": "companyCode",
                "search": true,
                "type": "text",
                "sort": true,
                "key": "searchCompanyCode"
            },
            {
                "name": "Status",
                "width": "col-xs-2half",
                "model": "status",
                "search": true,
                "sort": true,
                "type": "drop",
                "data": $rootScope.enum['LovStatus'],
                "key": "searchStatus"
            }
        ]

        $scope.sortSelection = {
            sortKey: "companyName",
            sortOrder: "asc"
        }

        $scope.companyMaster = {};
        $scope.searchDto = {};
        $scope.limitArr = [10, 15, 20];
        $scope.page = 0;
        $scope.limit = 10;


        $scope.init = function() {
            console.log("Company Init method called.");
            $rootScope.setNavigate1("Master");
            $rootScope.setNavigate2("Company");
            $scope.searchDto = {};
            $scope.search();
        };

        $scope.add = function() {
            $state.go("layout.addCompany");
        };

        $scope.limitChange = function(item) {
            $scope.page = 0;
            $scope.limit = item;
            $scope.cancel();
            $scope.search();
        }
        $scope.rowSelect = function(data) {
            $scope.companyMaster = data;

            /************************************************************
             * ux - by Muthu
             * reason - ipad compatibility fix
             *
             * *********************************************************/

            var windowInner = $window.innerWidth;
            if (windowInner <= 1199) {
                $scope.deskTopView = false;

            } else {
                $scope.deskTopView = true;
            }
            angular.element($window).bind('resize', function() {
                if ($window.innerWidth >= 1200) {
                    $scope.deskTopView = true;
                } else if ($window.innerWidth <= 1199) {
                    if ($scope.companyMaster.id != null) {
                        $scope.deskTopView = false;
                    } else {
                        $scope.deskTopView = true;
                    }
                }

                console.log("window resizing..." + $window.innerWidth);
                $scope.$digest();
            });

            /********************* ux fix ends here *****************************/
        }
        $scope.changeSearch = function(param) {
            for (var attrname in param) {
                $scope.searchDto[attrname] = param[attrname];
            }
            $scope.page = 0;
            $scope.cancel();
            $scope.search();
            console.log("change search", $scope.searchDto);
        }
        $scope.sortChange = function(param) {
            $scope.searchDto.orderByType = param.sortOrder.toUpperCase();
            $scope.searchDto.sortByColumn = param.sortKey;
            $scope.cancel();
            $scope.search();
        }
        $scope.changepage = function(param) {
            $scope.page = param.page;
            $scope.limit = param.size;
            $scope.cancel();
            $scope.search();
        }

        $scope.search = function() {
            $scope.companyMaster = {};
            $scope.searchDto.selectedPageNumber = $scope.page;
            $scope.searchDto.recordPerPage = $scope.limit;
            $scope.companyArr = [];
            CompanySearch.query($scope.searchDto).$promise.then(function(data, status) {

                $scope.totalRecord = data.responseObject.totalRecord;
                console.log($scope.totalRecord);
                var tempArr = [];
                var resultArr = [];
                tempArr = data.responseObject.searchResult;
                console.log("tempArr", tempArr);
                var tempObj = {};
                angular.forEach(tempArr, function(item, index) {
                    tempObj = item;
                    tempObj.no = (index + 1) + ($scope.page * $scope.limit);
                    resultArr.push(tempObj);
                    tempObj = {};
                });
                $scope.companyArr = resultArr;
            });

        }

        $scope.serialNo = function(index) {
            index = ($scope.searchDto.selectedPageNumber * $scope.searchDto.recordPerPage) +
                (index + 1);
            return index;
        }

        $scope.cancel = function() {
            $scope.showHistory = false;
            $scope.companyMaster = {};
            /************************************************************
             * ux - by Muthu
             * reason - ipad compatibility fix
             *
             * *********************************************************/
            $scope.deskTopView = true;

            /********************* ux fix ends here *****************************/
        };

        $scope.edit = function() {
            console.log("Edit Company Called");
            $state.go("layout.editCompany", {
                companyId: $scope.companyMaster.id
            });
        };


        $scope.deleteCompany = function() {
            ngDialog.openConfirm({
                    template: '<p>Are you sure you want to delete selected Company?</p>' +
                        '<div class="ngdialog-footer">' +
                        '<button type="button" class="btn btn-default btn-property cancel-btn" ng-click="closeThisDialog(0)">Cancel' +
                        '<button type="button" class="btn btn-primary btn-property accent-btn" ng-click="confirm(1)">Yes' +
                        '</button></div>',
                    plain: true,
                    className: 'ngdialog-theme-default'
                })
                .then(function(value) {
                    CompanyRemove.remove({
                        id: $scope.companyMaster.id
                    }, function(data) {
                        if (data.responseCode == 'ERR0') {
                            console.log("Company deleted Successfully")
                            $scope.init();
                            $scope.deskTopView = true;
                        } else {
                            console.log("Company deleted Failed " + data.responseDescription)
                        }
                    }, function(error) {
                        console.log("Company deleted Failed : " + error)
                    });
                }, function(value) {
                    console.log("deleted cancelled");
                });
        }


        // Routing

        switch ($stateParams.action) {
            case "SEARCH":
                $scope.init();
                $rootScope.breadcrumbArr = [{
                        label: "Master",
                        state: "layout.company"
                    },
                    {
                        label: "Company",
                        state: "layout.company"
                    }
                ];
                break;
            case "VIEW":
                break;
        }

    }]);