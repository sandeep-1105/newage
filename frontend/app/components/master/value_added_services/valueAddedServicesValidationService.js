app.service('valueAddedServicesValidationService', ['$rootScope','ValidateUtil', 'CommonValidationService', 'appConstant', function($rootScope, ValidateUtil, CommonValidationService, appConstant) {
	this.validate = function(valueAddedServices, code) {
		//Validate valueAddedServices name
		if(code == 0 || code == 1) {
			if(valueAddedServices.valueAddedSevicesName == undefined || valueAddedServices.valueAddedSevicesName == null || valueAddedServices.valueAddedSevicesName =="" ){
				return this.validationResponse(true, "valueAddedSevicesName", $rootScope.nls["ERR08001"], valueAddedServices);
			}
		}
		//Validate valueAddedServices code 
		if(code == 0 || code == 2) {
			if(valueAddedServices.valueAddedSevicesCode == undefined || valueAddedServices.valueAddedSevicesCode == null || valueAddedServices.valueAddedSevicesCode =="" ){
				return this.validationResponse(true, "valueAddedSevicesCode", $rootScope.nls["ERR08000"], valueAddedServices);
			}
		}
		
		return this.validationSuccesResponse(valueAddedServices);
	}
 
	this.validationSuccesResponse = function(valueAddedServices) {
	    return {error : false, obj : valueAddedServices};
	}
	
	this.validationResponse = function(err, elem, message, valueAddedServices) {
		return {error : err, errElement : elem, errMessage : message, obj : valueAddedServices};
	}
	
}]);