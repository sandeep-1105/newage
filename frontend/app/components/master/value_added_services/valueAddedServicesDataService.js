app.service('valueAddedServicesDataService', function($rootScope) {
    this.getHeadArray = function() {
	   return [
			  {
					"name":"#",
					"width":"w25px",
					"model":"no",
					"search":false
			  },{
					"name":"Name",
					"width":"w300px",
					"model":"valueAddedSevicesName",
					"search":true,
					"wrap_cell":true,
					"type":"text",
					"sort":true,
					"key":"searchValueAddedSevicesName"
				},{
					"name":"Code",
					"width":"w100px",
					"model":"valueAddedSevicesCode",
					"search":true,
					"type":"text",
					"sort":true,
					"key":"searchValueAddedSevicesCode"
				}];
          };

    
    this.getListBreadCrumb = function() {
        return [{
                label: "Master",
                state: "layout.valueAddedServices"
            },
            {
                label: "Value Added Service",
                state: "layout.valueAddedServices"
            }
        ];
    }

    this.getAddBreadCrumb = function() {
        return [{
                label: "Master",
                state: "layout.valueAddedServices"
            },
            {
                label: "Value Added Service",
                state: "layout.valueAddedServices"
            },
            {
                label: "Add Value Added Service",
                state: null
            }
        ];
    };
    this.getEditBreadCrumb = function() {
        return [{
                label: "Master",
                state: "layout.valueAddedServices"
            },
            {
                label: "Value Added Service",
                state: "layout.valueAddedServices"
            },
            { 
                label: "Edit Value Added Service",
                state: null
            }
        ];
    };
   

});