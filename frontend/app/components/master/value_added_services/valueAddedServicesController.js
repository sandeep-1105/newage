(function(){
app.controller("ValueAddedServicesCtrl", ['$scope', '$rootScope', '$window', '$modal', '$state', '$stateParams', '$log', 'ngDialog', 'valueAddedServicesDataService', 'valueAddedServicesFactory', 'RecentHistorySaveService', 'valueAddedServicesValidationService', 'Notification','serTaxCatMasterFactory', "roleConstant","AutoCompleteService",
    function($scope, $rootScope, $window, $modal, $state, $stateParams, $log, ngDialog, valueAddedServicesDataService, valueAddedServicesFactory, RecentHistorySaveService, valueAddedServicesValidationService, Notification,serTaxCatMasterFactory,roleConstant, AutoCompleteService) {

        $scope.tableHeadArr = valueAddedServicesDataService.getHeadArray();
        $scope.dataArr = [];
        $scope.valueAddedServices = {};
        $scope.valueAddedServices.valueAddedChargeList=[{}];
        $scope.showHistory = false;
        $scope.errorMap = new Map();
        $scope.deskTopView = true;
        
        $scope.sortSelection = {
            sortKey: "valueAddedSevicesName",
            sortOrder: "asc"
        }
        
        $scope.changeSearch = function(param) {
            $log.debug("Change Search is called." + param);
            for (var attrname in param) {
                $scope.searchData[attrname] = param[attrname];
            }
            $scope.page = 0;
            $scope.search();
        }

        $scope.sortChange = function(param) {
            $log.debug("sortChange is called. " + param);
            $scope.searchData.orderByType = param.sortOrder.toUpperCase();
            $scope.searchData.sortByColumn = param.sortKey;
            $scope.search();
        }
        
        /**
         * ValueAddedServices Master List Navigate to selected page.
         * @param {Object} param - The Selected page option.
         */
        $scope.changepage = function(param) {
            $log.debug("change Page..." + param.page);
            $scope.page = param.page;
            $scope.limit = param.size;
            $scope.search();
        }

        /**
         * ValueAddedServices Master List Page Limit Data change.
         * @param {int} nLimit - The Selected limit option from Limit array.
         */
        $scope.limitChange = function(nLimit) {
            $log.debug("Limit Change..." + nLimit);
            $scope.page = 0;
            $scope.limit = nLimit;
            $scope.search();
        }

        /**
         * ValueAddedServices Master List Page Search Data  initializing and calling search Mtd.
         * @param {Object} data - The Selected ValueAddedServices Master from List page.
         * @param {string} data.id - The id of the Selected ValueAddedServices Master.
         * @param {int} index - The Selected row index from List page.
         */
        $scope.rowSelect = function(data) {
        	if($rootScope.roleAccess(roleConstant.MASTER_GENERAL_VALUE_ADDED_SERVICE_VIEW)){
        	$scope.valueAddedServicesView=true;
			$scope.valueAddedServices = data;
			var windowInner=$window.innerWidth;
			if(windowInner<=1199){
				$scope.deskTopView = false;
			}
			else{
				$scope.deskTopView = true;
			}
			angular.element($window).bind('resize', function(){
				if($window.innerWidth>=1200){
					$scope.deskTopView = true;
				}
				else if($window.innerWidth<=1199){
					if($scope.valueAddedServices.id!=null){
						$scope.deskTopView = false;
					}
					else {
						$scope.deskTopView = true;
					}
				}
				$scope.$digest();
			});
        }
		}

        $scope.addNewcharge = function(){
        	$scope.errorArray = [];
        	var listErrorFlag = false;
        	if($scope.valueAddedServices.valueAddedChargeList != undefined && $scope.valueAddedServices.valueAddedChargeList != null && $scope.valueAddedServices.valueAddedChargeList.length >0) {
        		for(var index = 0; index < $scope.valueAddedServices.valueAddedChargeList.length; index++) {
        			if($scope.validateCharge(index)) {
        				listErrorFlag = true;
        			}
        		}
        		if(listErrorFlag) {
        			return;
        		}  else {
        			$scope.valueAddedServices.valueAddedChargeList.push({});
        			$rootScope.navigateToNextField('chargeName' + ($scope.valueAddedServices.valueAddedChargeList.length - 1));
            	}
        	} else {
        		$scope.valueAddedServices.valueAddedChargeList.push({});
        		$rootScope.navigateToNextField('chargeName' + ($scope.valueAddedServices.valueAddedChargeList.length - 1));
        	}
        }
        
        $scope.removeCharge = function(index){
        	$scope.valueAddedServices.valueAddedChargeList.splice(index,1);
        }
        
        /**
         * ValueAddedServices Master List Page Search Data  initializing and calling search Mtd.
         */
        $scope.searchInit = function() {
            $scope.searchData = {};
            $scope.limitArr = [10, 15, 20];
            $scope.page = 0;
            $scope.limit = 10;
            $scope.totalRecord = 10;
            $scope.searchData.orderByType = $scope.sortSelection.sortOrder.toUpperCase();
            $scope.searchData.sortByColumn = $scope.sortSelection.sortKey;
            $scope.search();
        };
        /**
         * Go Back to ValueAddedServices Master List Page.
         */
        $scope.backToList = function() {
        	$scope.valueAddedServicesView = false;
        	$scope.valueAddedServices = {};
        	$scope.deskTopView = true;
        	
        }

       
        /**
         * Get ValueAddedServices Master's Based on Search Data.
         */
        $scope.search = function() {
        	$scope.valueAddedServicesView=false;
            $scope.searchData.selectedPageNumber = $scope.page;
            $scope.searchData.recordPerPage = $scope.limit;
            $scope.dataArr = [];
            valueAddedServicesFactory.search.query($scope.searchData).$promise.then(
                function(data) {
                    if (data.responseCode == "ERR0") {
                        $scope.totalRecord = data.responseObject.totalRecord;
                        $scope.dataArr = data.responseObject.searchResult;
                    }
                },
                function(errResponse) {
                	 $log.error('Error while fetching ValueAddedServices');
                });
           }

        /**
         * Get ValueAddedServices Master By id.
         * @param {int} id - The id of a ValueAddedServices.
         * @param {boolean} isView - TO identify purpose of calling is for View or Edit a ValueAddedServices.
         */
        $scope.view = function(id, isView) {
        	valueAddedServicesFactory.findById.query({
                id: id
            }).$promise.then(function(data) {
                if (data.responseCode == 'ERR0') {
                    $scope.valueAddedServices = data.responseObject;
                    var rHistoryObj = {}

                    if ($scope.valueAddedServices != undefined && $scope.valueAddedServices != null && $scope.valueAddedServices.id != undefined && $scope.valueAddedServices.id != null) {
                        $rootScope.subTitle = $scope.valueAddedServices.valueAddedServicesName;
                    } else {
                        $rootScope.subTitle = "Unknown Value Added Services"
                    }
                    $rootScope.unfinishedFormTitle = "Value Added Services # " + $scope.valueAddedServices.valueAddedServicesCode;
                    if (isView) {
                        rHistoryObj = {
                            'title': "Value Added Services #" + $scope.valueAddedServices.valueAddedServicesCode,
                            'subTitle': $rootScope.subTitle,
                            'stateName': $state.current.name,
                            'stateParam': JSON.stringify($stateParams),
                            'stateCategory': 'Value Added Services',
                            'serviceType': undefined,
                            'showService': false
                        }

                    } else {

                        rHistoryObj = {
                            'title': 'Edit Value Added Services # ' + $stateParams.serviceId,
                            'subTitle': $rootScope.subTitle,
                            'stateName': $state.current.name,
                            'stateParam': JSON.stringify($stateParams),
                            'stateCategory': 'Value Added Services',
                            'serviceType': 'MASTER',
                            'serviceCode': ' ',
                            'orginAndDestination': ' '
                        }
                    }
                    RecentHistorySaveService.form(rHistoryObj);
                } else {
                    $log.debug("Exception while getting Value Added Services by Id ", data.responseDescription);
                }
            }, function(error) {
                $log.debug("Error ", error);
            });
        };

        $scope.getChargeNotInList = function(keyword) {
        	
        	
        	if($scope.valueAddedServices.valueAddedChargeList != null && $scope.valueAddedServices.valueAddedChargeList.length > 0) {
        		   var idList = [];

        		   for(var i = 0; i < $scope.valueAddedServices.valueAddedChargeList.length; i++) {
        			   if($scope.valueAddedServices.valueAddedChargeList[i] != null && 
        					   $scope.valueAddedServices.valueAddedChargeList[i].chargeMaster != null) {
        				   idList.push($scope.valueAddedServices.valueAddedChargeList[i].chargeMaster.id);
        			   }
        		   }
        	}
        	   
        	   return AutoCompleteService.getChargeNotInList(keyword, idList);
        	
        }
        
        $scope.selectedCharge = function() {
        	$scope.addNewcharge();
        }
        
       
        var errorOnRowIndex = null;

        $scope.errorShow = function(errorObj,index){
        	 var myOtherModal = $modal({scope:$scope,templateUrl: 'errorPopUp.html', show: false,keyboard:true});
        	$scope.errList = errorObj.errTextArr;
        	errorOnRowIndex = index;
        	myOtherModal.$promise.then(myOtherModal.show);
        };
 	   

        $scope.addNew = function() {
        	if($rootScope.roleAccess(roleConstant.MASTER_GENERAL_VALUE_ADDED_SERVICE_CREATE)){
        		$state.go("layout.valueAddedServicesAdd");
        	}
        }

       
        $scope.cancel = function() {
            if($scope.valueAddedServicesrForm!=undefined){
		    	$scope.goTocancel($scope.valueAddedServicesrForm.$dirty);
            }else{
            	$scope.goTocancel(false);
            }
        }
       
       $scope.goTocancel = function(isDirty){
     		 if (isDirty) {
     		      var newScope = $scope.$new();
     		      newScope.errorMessage = $rootScope.nls["ERR200"];
     		      ngDialog.openConfirm({
     		        template: '<p>{{errorMessage}}</p>' +
     		          '<div class="ngdialog-footer">' +
     		          ' <button type="button" class="btn btn-default btn-property cancel-btn" ng-click="confirm(2)">No</button>' +
     		          '<button type="button" class="btn btn-default btn-property accent-btn " ng-click="confirm(1)">Yes</button>' +
     		          '<button type="button" class="btn btn-default btn-property pull-left grey-cancel-btn" ng-click="confirm(3)">Cancel</button>' +
     		          '</div>',
     		        plain: true,
     		        closeByDocument: false,
     		        scope: newScope,
     		        className: 'ngdialog-theme-default'
     		      }).then(
     		        function(value) {
     		          if (value == 1) {
     		        	 $scope.saveOrUpdate();
     		          } else if (value == 2) {
     		        	 $state.go("layout.valueAddedServices", {submitAction: "Cancelled"});
     		          } else {
     		            $log.debug("cancelled");
     		          }
     		        });
     		    } else {
     		     $state.go("layout.valueAddedServices", {submitAction: "Cancelled"});
     		    }
     	    }

        /**
         * Modify Existing ValueAddedServices Master.
         * @param {int} id - The id of a Country.
         */
        $scope.vasEdit = function(id) {
        	if($rootScope.roleAccess(roleConstant.MASTER_GENERAL_VALUE_ADDED_SERVICE_MODIFY)){
        	if($scope.valueAddedServices.valueAddedChargeList == undefined || $scope.valueAddedServices.valueAddedChargeList == null || $scope.valueAddedServices.valueAddedChargeList.length ==0){
          	  $scope.valueAddedServices.valueAddedChargeList.push({})
            }
            $state.go("layout.valueAddedServicesEdit", {id: id});
        	}
        }

        /**
         * Delete Country Master.
         * @param {int} id - The id of a Country.
         */
        $scope.vasDelete = function(id) {
        	if($rootScope.roleAccess(roleConstant.MASTER_GENERAL_VALUE_ADDED_SERVICE_DELETE)){
        	var newScope = $scope.$new();
			newScope.errorMessage = $rootScope.nls["ERR08006"];
	    	ngDialog.openConfirm( {
	                    template : '<p>{{errorMessage}}</p>'
	                    + '<div class="ngdialog-footer">'
	                    + '<button type="button" class="btn btn-default btn-property cancel-btn" ng-click="closeThisDialog(0)">Cancel'
	                    + '<button type="button" class="btn btn-primary btn-property accent-btn" ng-click="confirm(1)">Yes'
	                    + '</button></div>',
	                    plain : true,
	                    scope : newScope,
	                    closeByDocument: false,
	                    className : 'ngdialog-theme-default'
	                }).then( function(value) {
	                	valueAddedServicesFactory.delete.query({
	                        id: id
	                    }).$promise.then(function(data) {
	                        if (data.responseCode == 'ERR0') {
	                        	$scope.valueAddedServicesView=false;
	                        	Notification.success($rootScope.nls["ERR402"]);
	                        	$scope.deskTopView = true;
	                            $scope.search();
	                        }
	                    }, function(error) {
	                        $log.debug("Error ", error);
	                    });
	                }, function(value) {
	                    $log.debug("delete Cancelled");
	            });
        }
        }
        
        $scope.isEmptyRow=function(obj){
            var isempty = true; //  empty
            if (!obj) {
                return isempty;
            }
            var k = Object.getOwnPropertyNames(obj);
            for (var i = 0; i < k.length; i++){
                if (obj[k[i]]) {
                    isempty = false; // not empty
                    break;
                }
            }
            return isempty;
    }
        
        /**
         * Validating field of ValueAddedServices Master while instant change.
         * @param {errorElement,errorCode}
         */
        $scope.validate = function(valElement, code) {
            var validationResponse = valueAddedServicesValidationService.validate($scope.valueAddedServices,code);
            if(validationResponse.error == true) {
				$scope.validationErrorAndFocus(validationResponse, validationResponse.errElement);
			} else {
				$scope.errorMap.put(valElement, null);
            	validationResponse.error = false;
            }
        }
        
        /**
         * Common Validation Focus Functionality - starts here" 
         */
  		$scope.validationErrorAndFocus = function(valResponse, elemId){
  			$scope.errorMap = new Map();
  			if(valResponse.error == true){
  				$scope.errorMap.put(valResponse.errElement, valResponse.errMessage);
  			}
  				$scope.navigateToNextField(elemId);
  		}
  		
  		$scope.validateCharge = function(index){
  			$scope.errorArray[index] = {};
  			$scope.errorArray[index].errTextArr = [];
  			var errorFound = false;
  			
  			
  			if($scope.valueAddedServices.valueAddedChargeList[index].chargeMaster == undefined
  					|| $scope.valueAddedServices.valueAddedChargeList[index].chargeMaster == null 
  					|| $scope.valueAddedServices.valueAddedChargeList[index].chargeMaster.chargeName == undefined 
  					|| $scope.valueAddedServices.valueAddedChargeList[index].chargeMaster.chargeName == null 
  					|| $scope.valueAddedServices.valueAddedChargeList[index].chargeMaster.chargeName == "") {
  				$scope.errorArray[index].errRow = true;
  				$scope.errorArray[index].chargeName = true;
  	            $scope.errorArray[index].errTextArr.push($rootScope.nls["ERR08008"]);	
  	            errorFound = true;
  			}else{
  				if($scope.valueAddedServices.valueAddedChargeList[index].chargeMaster.status=='Block') {
  	  				$scope.errorArray[index].errRow = true;
  	  				$scope.errorArray[index].chargeName = true;
  	  	            $scope.errorArray[index].errTextArr.push($rootScope.nls["ERR08004"]);	
  	  	            errorFound = true;
  	  			}
  			}
  			
  			 return errorFound;
  		}

        /**
         * Create and Update ValueAddedServices Master.
         */
        $scope.saveOrUpdate = function() {
        	$scope.errorArray = [];
        	var listErrorFlag = false;
        
            var validationResponse = valueAddedServicesValidationService.validate($scope.valueAddedServices,0);
            if(validationResponse.error == true) {
            	$log.debug("Validation Response -- ", validationResponse);
            	$log.debug('Validation Failed to create ValueAddedServices..')
				$scope.validationErrorAndFocus(validationResponse, validationResponse.errElement);
			}
        else if($scope.valueAddedServices.valueAddedChargeList == undefined || $scope.valueAddedServices.valueAddedChargeList == null || $scope.valueAddedServices.valueAddedChargeList.length ==0){
        	  Notification.error($rootScope.nls["ERR08009"]);
        	  $scope.valueAddedServices.valueAddedChargeList.push({})
        }
       else if($scope.valueAddedServices.valueAddedChargeList != undefined && $scope.valueAddedServices.valueAddedChargeList!= null && $scope.valueAddedServices.valueAddedChargeList.length >0) {
            for(var index = 0; index < $scope.valueAddedServices.valueAddedChargeList.length; index++) {
    			
            	 if(index==0){
					  if($scope.validateCharge(index)) {
						 // Notification.error($rootScope.nls["ERR08009"]);
						  listErrorFlag = true;
					   }
					  }else{
						  if($scope.isEmptyRow($scope.valueAddedServices.valueAddedChargeList[index])){
							  $scope.valueAddedServices.valueAddedChargeList.splice(index,1);
							  listErrorFlag = false;
						  }else{
							  if($scope.validateCharge(index)) {
								  listErrorFlag = true;
							  }
						  }
					  }
    		}
    		if(listErrorFlag) {
    			return;
    		}
    		else {
            	if($scope.valueAddedServices.id == null) {
            		var successMessage = $rootScope.nls["ERR400"];
            	} else {
            		var successMessage = $rootScope.nls["ERR401"];
            	}
        		valueAddedServicesFactory.create.query($scope.valueAddedServices).$promise.then(function(data) {
		            if (data.responseCode == 'ERR0') {
		            	Notification.success(successMessage);
		            	$state.go('layout.valueAddedServices');
		            } else {
		                $log.debug("Value Aadded Services Failed :" + data.responseDescription)
		            }
		        }, function(error) {
		        	 $log.debug("ValueAddedServices Failed : " + error)
		        });	
            }
          }
        };
      
        /**
         * Recent History - starts here
         */
        $scope.isReloaded = localStorage.isValueAddedServicesReloaded;
        $scope.$on('addValueAddedServicesEvent', function(events, args){
        	$rootScope.unfinishedData = $scope.valueAddedServices;
			$rootScope.category = "Value Added Service";
			$rootScope.unfinishedFormTitle = "Value Added Services (New)";
        	if($scope.valueAddedServices != undefined && $scope.valueAddedServices != null && $scope.valueAddedServices.valueAddedSevicesName != undefined && $scope.valueAddedServices.valueAddedSevicesName != null) {
        		$rootScope.subTitle = $scope.valueAddedServices.valueAddedSevicesName;
        	} else {
        		$rootScope.subTitle = "Unknown Value Added Services "
        	}
        })
      
	    $scope.$on('editValueAddedServicesEvent', function(events, args){
	    	$rootScope.unfinishedData = $scope.valueAddedServices;
			$rootScope.category = "Value Added Service";
			$rootScope.unfinishedFormTitle = "Value Added Services Edit # "+$scope.valueAddedServices.valueAddedServicesCode;
			if($scope.valueAddedServices != undefined && $scope.valueAddedServices != null && $scope.valueAddedServices.valueAddedSevicesName != undefined && $scope.valueAddedServices.valueAddedSevicesName != null) {
        		$rootScope.subTitle = $scope.valueAddedServices.valueAddedSevicesName;
        	} else {
        		$rootScope.subTitle = "Unknown Value Added Services "
        	}
	    })
	      
	    $scope.$on('addValueAddedServicesEventReload', function(e, confirmation){
	    	    confirmation.message = "";
	    	    localStorage.reloadFormData = JSON.stringify($scope.valueAddedServices) ;
	    	    localStorage.isValueAddedServicesReloaded = "YES";
	            e.preventDefault();
	    })
	    
	    $scope.$on('editValueAddedServicesEventReload', function(e, confirmation){
	    	    confirmation.message = "";
	    	    localStorage.reloadFormData = JSON.stringify($scope.valueAddedServices) ;
	    	    localStorage.isValueAddedServicesReloaded = "YES";
	            e.preventDefault();
	    	  
	    })
	      


        /**
         * State Identification Handling Here.
         * @param {String} $stateParams.action - VIEW, ADD, EDIT, SEARCH.
         */
        //
        switch ($stateParams.action) {
            case "ADD":
            	 if ($stateParams.fromHistory === 'Yes') {
                     if ($scope.isReloaded == "YES") {
                      $scope.isReloaded = "NO";
                      localStorage.isValueAddedServicesReloaded = "NO";
                       $scope.valueAddedServices = JSON.parse(localStorage.reloadFormData);
                     } else {
                       $scope.valueAddedServices = $rootScope.selectedUnfilledFormData;
                     }
                   } else {
                     if ($scope.isReloaded == "YES") {
                       $scope.isReloaded = "NO";
                       localStorage.isValueAddedServicesReloaded = "NO";
                       $scope.valueAddedServices = JSON.parse(localStorage.reloadFormData);
                     } else {
                         $log.debug("Add NotHistory NotReload...")
                     }
                   }
                $rootScope.breadcrumbArr = valueAddedServicesDataService.getAddBreadCrumb();
                break;
            case "EDIT":
            	 if ($scope.fromHistory === 'Yes') {
                     if ($scope.isReloaded == "YES") {
                       $scope.isReloaded = "NO";
                       localStorage.isValueAddedServicesReloaded = "NO";
                       $scope.valueAddedServices = JSON.parse(localStorage.reloadFormData);
                     } else {
                       $scope.valueAddedServices = $rootScope.selectedUnfilledFormData;
                     }
                   } else {
                     if ($scope.isReloaded == "YES") {
                       $scope.isReloaded = "NO";
                       localStorage.isValueAddedServicesReloaded = "NO";
                       $scope.valueAddedServices = JSON.parse(localStorage.reloadFormData);
                     } else {
                    	 $scope.view($stateParams.id, false);
                     }
                   }
                $rootScope.breadcrumbArr = valueAddedServicesDataService.getEditBreadCrumb();
                break;
            default:
                $rootScope.breadcrumbArr = valueAddedServicesDataService.getListBreadCrumb();
                $scope.searchInit();
        }

    }
]);
}) ();