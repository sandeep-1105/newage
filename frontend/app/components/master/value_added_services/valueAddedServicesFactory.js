app.factory('valueAddedServicesFactory',['$resource', function($resource) {
	return {
		findById : $resource('/api/v1/valueaddedservices/get/id/:id', {}, {
			 query : {method : 'GET', params : {id : ''}, isArray : false}
		}),
		getAllValue : $resource('/api/v1/valueaddedservices/get/all', {}, {
			 query : {method : 'GET', params : {id : ''}, isArray : false}
		}),
		create : $resource('/api/v1/valueaddedservices/save', {}, {
			query : {method : 'POST'}
		}),
		delete : $resource('/api/v1/valueaddedservices/delete/:id', {}, {
			query : {method : 'DELETE', params : {id : ''}}
		}),
		search : $resource('/api/v1/valueaddedservices/get/search/keyword', {}, {
			query : {method : 'POST'}
		}),
		getall : $resource('/api/v1/valueaddedservices/get/search/:groupId', {}, {
			fetch : {method : 'POST'}
		})
	};
}])