app.service('eventMasterService', function($rootScope) {
    this.getHeadArray = function() {
        return [{
                "name": "#",
                "width": "col-xs-0half",
                "model": "no",
                "search": false
            },
            {
                "name": "Name",
                "width": "col-xs-6",
                "model": "eventName",
                "search": true,
                "wrap_cell": true,
                "type": "text",
                "sort": true,
                "key": "searchName"
            },
            {
                "name": "Code",
                "width": "col-xs-2",
                "model": "eventCode",
                "search": true,
                "type": "text",
                "sort": true,
                "key": "searchCode"
            },
            {
                "name": "Country Name",
                "width": "col-xs-4",
                "model": "countryMaster.countryName",
                "search": true,
                "type": "text",
                "sort": true,
                "key": "searchCountryName"
            },
            {
                "name": "Status",
                "width": "w120px",
                "model": "status",
                "search": true,
                "type": "drop",
                "data": $rootScope.enum['LovStatus'],
                "key": "searchStatus",
                "sort": true
            }
        ];
    };

    
    this.getListBreadCrumb = function() {
        return [{
                label: "Master",
                state: "layout.eventMaster"
            },
            {
                label: "Event",
                state: "layout.eventMaster"
            }
        ];
    }

    this.getAddBreadCrumb = function() {
        return [{
                label: "Master",
                state: "layout.eventMaster"
            },
            {
                label: "Event",
                state: "layout.eventMaster"
            },
            {
                label: "Add Event",
                state: null
            }
        ];
    };
    this.getEditBreadCrumb = function() {
        return [{
                label: "Master",
                state: "layout.eventMaster"
            },
            {
                label: "Event",
                state: "layout.eventMaster"
            },
            {
                label: "Edit Event",
                state: null
            }
        ];
    };
    

});