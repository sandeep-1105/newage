app.service('eventMasterValidationService', ['$rootScope','ValidateUtil', 'CommonValidationService', 'appConstant', function($rootScope, ValidateUtil, CommonValidationService, appConstant) {
	this.validate = function(masterObject, code) {
		//Validate event Name
		if(code == 0 || code == 1) {
			if (masterObject.eventName == null ||masterObject.eventName == undefined  || masterObject.eventName == "") {
				return this.validationResponse(true, "eventName", $rootScope.nls["ERR3703-2"], masterObject);
			}else{
				if(!CommonValidationService.checkRegExp(appConstant.Reg_Exp_MASTER_EVENT_NAME,masterObject.eventName)){
	  				return this.validationResponse(true, "eventName", $rootScope.nls["ERR3707-2"], masterObject);
			   }
			}
		}
		//Validate event Code
		if(code == 0 || code == 2) {
			if (masterObject.eventCode == null ||masterObject.eventCode == undefined  || masterObject.eventCode == "") {
				return this.validationResponse(true, "eventCode", $rootScope.nls["ERR3702-2"], masterObject);
			}
			else{
				if(!CommonValidationService.checkRegExp(appConstant.Reg_Exp_MASTER_EVENT_CODE,masterObject.eventCode)){
	  				return this.validationResponse(true, "eventCode", $rootScope.nls["ERR3706-2"], masterObject);
			   }
			}
		}
		//Validate country master 
		if(code == 0 || code == 4) {
			if (masterObject.countryMaster == null ||masterObject.countryMaster == undefined  || masterObject.countryMaster == "") {
				return this.validationResponse(true, "country", $rootScope.nls["ERR36"], masterObject);
			}
			
			if (masterObject.countryMaster.status =='Block') {
				return this.validationResponse(true, "country", $rootScope.nls["ERR37"], masterObject);
			}
			
		}
		//Validate event Type drop down
		/*if(code == 0 || code == 3) {
			if (masterObject.eventMasterType == null ||masterObject.eventMasterType == undefined  || masterObject.eventMasterType == "") {
				return this.validationResponse(true, "eventMasterType", $rootScope.nls["ERR3709-2"], masterObject);
			}
		}*/
		return this.validationSuccesResponse(masterObject);
	}
 
	this.validationSuccesResponse = function(masterObject) {
	    return {error : false, obj : masterObject};
	}
	
	this.validationResponse = function(err, elem, message, masterObject) {
		return {error : err, errElement : elem, errMessage : message, obj : masterObject};
	}
	
}]);