(function(){
app.controller("eventMasterCtrl", ['$scope', '$rootScope', '$window', '$modal', '$state', '$stateParams', '$log', 'ngDialog','eventMasterService',
                                   'RecentHistorySaveService','eventMasterValidationService','Notification','EventAdd','EventView','EventSearch','EventEdit','EventRemove','roleConstant','CountryList',
    function($scope, $rootScope, $window, $modal, $state, $stateParams, $log, ngDialog, eventMasterService,RecentHistorySaveService,eventMasterValidationService,Notification,
    		EventAdd,EventView,EventSearch,EventEdit,EventRemove,roleConstant,CountryList
    		) {
	
        var vm = this;
        vm.deskTopView = true;
        vm.tableHeadArr = eventMasterService.getHeadArray();
        vm.dataArr = [];
        vm.searchData = {};
        vm.eventMaster = {};
        vm.limitArr = [10, 15, 20];
        vm.page = 0;
        vm.limit = 10;
        vm.totalRecord = 100;
        vm.showHistory = false;
        vm.errorMap = new Map();
        vm.showDetail=false;

        vm.sortSelection = {
            sortKey: "eventName",
            sortOrder: "asc"
        }
        
        vm.changeSearch = function(param) {
            $log.debug("Change Search is called." + param);
            for (var attrname in param) {
                vm.searchData[attrname] = param[attrname];
            }
            vm.page = 0;
            vm.search();
        }

        vm.sortChange = function(param) {
            $log.debug("sortChange is called. " + param);
            vm.searchData.orderByType = param.sortOrder.toUpperCase();
            vm.searchData.sortByColumn = param.sortKey;
            vm.search();
        }
        
        
        
        $scope.setStatus = function(data){
    		if(data=='Active'){
    			return 'activetype';
    		} else if(data=='Block'){
    			return 'blockedtype';
    		}else if(data=='Hide'){
    			return 'hiddentype';
    		}
    	}
        
        vm.changepage = function(param) {
            $log.debug("change Page..." + param.page);
            vm.page = param.page;
            vm.limit = param.size;
            vm.search();
        }

        vm.limitChange = function(nLimit) {
            $log.debug("Limit Change..." + nLimit);
            vm.page = 0;
            vm.limit = nLimit;
            vm.search();
        }
        
        vm.rowSelect = function(param) {
        	if($rootScope.roleAccess(roleConstant.MASTER_OCEAN_EVENT_VIEW)){
        		vm.eventMaster=param;
            	vm.showDetail=true;	
            	var windowInner=$window.innerWidth;
    			if(windowInner<=1199){
    				vm.deskTopView = false;
    			}
    			else{
    				vm.deskTopView = true;
    			}
    			angular.element($window).bind('resize', function(){
    				if($window.innerWidth>=1200){
    					vm.deskTopView = true;
    				}
    				else if($window.innerWidth<=1199){
    					if(vm.eventMaster.id!=null){
    						vm.deskTopView = false;
    					}
    					else {
    						vm.deskTopView = true;
    					}
    				}
    				$scope.$digest();
    			});
        	}
        }

        
        vm.editEvent=function(id){
        	if($rootScope.roleAccess(roleConstant.MASTER_OCEAN_EVENT_MODIFY)){
        		var params={};
                params.id=id;
                $state.go('layout.eventMasterEdit',params);
        	}
        }
        /**
         * Event Master List Page Search Data  initializing and calling search Mtd.
         */
        vm.searchInit = function() {
            vm.searchData = {};
            vm.searchData.orderByType = vm.sortSelection.sortOrder.toUpperCase();
            vm.searchData.sortByColumn = vm.sortSelection.sortKey;
            vm.search();
        };
        /**
         * Go Back to Event Master List Page.
         */
        vm.backToList = function() {
        	vm.eventMaster = {};
        	vm.deskTopView = true;	
        	vm.showDetail=false;
        	vm.showHistory=false;
            $state.go("layout.eventMaster", {
                submitAction: "Cancelled"
            });
        }

        /**
         * Get Event Master's Based on Search Data.
         */
        vm.search = function() {
            vm.searchData.selectedPageNumber = vm.page;
            vm.searchData.recordPerPage = vm.limit;
            vm.dataArr = [];
            EventSearch.query(vm.searchData).$promise.then(
                function(data) {
                    if (data.responseCode == "ERR0") {
                        vm.totalRecord = data.responseObject.totalRecord;
                        vm.eventArr = data.responseObject.searchResult;
                    }
                },
                function(errResponse) {
                    $log.error('Error while fetching events');
                });
        }

        /**
         * Get Event Master By id.
         * @param {int} id - The id of a Event.
         * @param {boolean} isView - TO identify purpose of calling is for View or Edit a Event.
         */
        vm.view = function(id) {
        	EventView.get({
                id: id
            }).$promise.then(function(data) {
                if (data.responseCode == 'ERR0') {
                	vm.eventMaster=data.responseObject;
                }
            }, function(error) {
                $log.debug("Error ", error);
            });
        };
        /**
         * Go To Add Event Master.
         */
        vm.addNew = function() {
        	if($rootScope.roleAccess(roleConstant.MASTER_OCEAN_EVENT_CREATE)){
        		 $state.go("layout.eventMasterAdd");
        	}
        }

        vm.validate = function(valElement, code) {
            var validationResponse = eventMasterValidationService.validate(vm.eventMaster,code);
            if(validationResponse.error == true) {
				vm.validationErrorAndFocus(validationResponse, validationResponse.errElement);
			} else {
				vm.errorMap.put(valElement, null);
            	validationResponse.error = false;
            }
        }
        /**
         * Cancel Add or Edit Event Master.
         */
       
        vm.cancel = function(objId) {
            if(vm.eventMasterform!=undefined){
		    	vm.goTocancel(vm.eventMasterform.$dirty,objId);
            }else{
            	vm.goTocancel(false,objId);
            }
        }
       
       vm.goTocancel = function(isDirty,objId){
     		 if (isDirty) {
     		      var newScope = $scope.$new();
     		      newScope.errorMessage = $rootScope.nls["ERR200"];
     		      ngDialog.openConfirm({
     		        template: '<p>{{errorMessage}}</p>' +
     		          '<div class="ngdialog-footer">' +
     		          ' <button type="button" class="btn btn-default btn-property cancel-btn" ng-click="confirm(2)">No</button>' +
     		          '<button type="button" class="btn btn-default btn-property accent-btn " ng-click="confirm(1)">Yes</button>' +
     		          '<button type="button" class="btn btn-default btn-property pull-left grey-cancel-btn" ng-click="confirm(3)">Cancel</button>' +
     		          '</div>',
     		        plain: true,
     		        closeByDocument: false,
     		        scope: newScope,
     		        className: 'ngdialog-theme-default'
     		      }).then(
     		        function(value) {
     		          if (value == 1) {
     		            if(objId!=null && objId != undefined){
     		            	vm.update();
     		            }else{
     		            	vm.save();
     		            }
     		          } else if (value == 2) {
     		        	 $state.go("layout.eventMaster", {
     		                submitAction: "Cancelled"
     		            });
     		          } else {
     		            $log.debug("cancelled");
     		          }
     		        });
     		    } else {
     		     $state.go("layout.eventMaster", {
                   submitAction: "Cancelled"
               });
     		    }
     	    }

        /**
         * Modify Existing Event Master.
         * @param {int} id - The id of a Event.
         */
        vm.edit = function(id) {

            $state.go("layout.eventMasterEdit", {
                id: id
            });
        }
        
        

        /**
         * Delete Event Master.
         * @param {int} id - The id of a Event.
         */
        vm.deleteEvent = function(id) {
        	if($rootScope.roleAccess(roleConstant.MASTER_OCEAN_EVENT_DELETE)){
        		var newScope = $scope.$new();
    			newScope.errorMessage =$rootScope.nls["ERR37010-2"];
    	    	ngDialog.openConfirm( {
    	                    template : '<p>{{errorMessage}}</p>'
    	                    + '<div class="ngdialog-footer">'
    	                    + '<button type="button" class="btn btn-default btn-property cancel-btn" ng-click="closeThisDialog(0)">Cancel'
    	                    + '<button type="button" class="btn btn-primary btn-property accent-btn" ng-click="confirm(1)">Yes'
    	                    + '</button></div>',
    	                    plain : true,
    	                    scope : newScope,
    	                    closeByDocument: false,
    	                    className : 'ngdialog-theme-default'
    	                }).then( function(value) {
    	                	EventRemove.remove({
    	                        id: id
    	                    }).$promise.then(function(data) {
    	                        if (data.responseCode == 'ERR0') {
    	                        	Notification.success($rootScope.nls["ERR402"]);
    	                           /* $state.go("layout.eventMaster", {
    	                                submitAction: "Cancelled"
    	                            });*/
    	                        	
    	                        	vm.showDetail=false;
    	                        	vm.showHistory=false;
    	                        	vm.deskTopView = true;
    	                        	vm.searchInit();
    	                        }
    	                    }, function(error) {
    	                        $log.debug("Error ", error);
    	                    });
    	                }, function(value) {
    	                    $log.debug("delete Cancelled");
    	            });
        	}
        }

        /**
         * Create New Event Master.
         */
        vm.save= function() {
            var validationResponse = eventMasterValidationService.validate(vm.eventMaster,0);
            if(validationResponse.error == true) {
				vm.validationErrorAndFocus(validationResponse, validationResponse.errElement);
			}
            else {
            	EventAdd.save(vm.eventMaster).$promise.then(function(data) {
		            if (data.responseCode == 'ERR0') {
		            	$log.debug("Event Saved Successfully Response :: ", data);
		            	Notification.success($rootScope.nls["ERR400"]);
		            	$state.go('layout.eventMaster');
		            } else {
		                $log.debug("Create eventMaster Failed :" + data.responseDescription)
		            }

		        }, function(error) {
		        	 $log.debug("Create eventMaster Failed : " + error)
		        });
            } 
        };

        
      /**
       * Common Validation Focus Functionality - starts here" 
       */
		vm.validationErrorAndFocus = function(valResponse, elemId){
			vm.errorMap = new Map();
			if(valResponse.error == true){
				vm.errorMap.put(valResponse.errElement, valResponse.errMessage);
			}
			$rootScope.navigateToNextField(elemId);
		}
        
        //Update Existing Event Master
        vm.update = function() {
        	 var validationResponse = eventMasterValidationService.validate(vm.eventMaster,0);
             if(validationResponse.error == true) {
             	$log.debug('Validation Failed while Updating the event')
 				vm.validationErrorAndFocus(validationResponse, validationResponse.errElement);
 			}
             else {
            	 EventEdit.update(vm.eventMaster).$promise.then(function(data) {
 		            if (data.responseCode == 'ERR0') {
 		            	$log.debug("Event Updated Successfully Response :: ", data);
 		            	Notification.success($rootScope.nls["ERR401"]);
 		            	$state.go('layout.eventMaster');
 		            } else {
 		                $log.debug("Updating eventMaster Failed :" + data.responseDescription)
 		            }

 		        }, function(error) {
 		        	 $log.debug("Updating eventMaster Failed : " + error)
 		        });
             } 
        };
        
        
        /* Master Select Picker */
        $scope.ajaxCountryEvent = function(object) {
            vm.searchData = {};
            vm.searchData.keyword = object == null ? "" : object;
            vm.searchData.selectedPageNumber = 0;
            vm.searchData.recordPerPage = $rootScope.totalRecordFetchPerRequest;
            return CountryList.fetch(vm.searchData).$promise
            .then(
                function(data) {
                    if (data.responseCode == "ERR0") {
                        vm.countryMasterList = data.responseObject.searchResult;
                        return vm.countryMasterList;
                    }
                },
                function(errResponse) {
                    console
                        .error('Error while fetching Country');
                });

        }
        
        $scope.validateEventOnChange=function(em,code){
       	vm.errorMap=new Map();
        var validationResponse=eventMasterValidationService.validate(em,code)
           if(validationResponse.error=true){
       	  vm.errorMap.put(validationResponse.errElement, validationResponse.errMessage);
          }
       }
        
        vm.moveToNextFeild = function(nextElemId,code){
        	vm.errorMap=new Map();
        	var validationResponse = eventMasterValidationService.validate(vm.eventMaster,code);
        	if(validationResponse.error == true) {
				vm.validationErrorAndFocus(validationResponse, validationResponse.errElement);
			}else{
        	$rootScope.navigateToNextField(nextElemId);
			}
        }
        
        
        
        
        /************************************************************
		* ux - by Muthu
		* reason - ipad compatibility fix
		*
		* *********************************************************/

		/********************* ux fix ends here *****************************/

		/**
         * Recent History - starts here
         */
vm.isReloaded= localStorage.isEventMasterReloaded;
        
        $scope.$on('addEventMasterEvent', function(events, args){
        	$rootScope.unfinishedData = vm.eventMaster;
			$rootScope.category = "Event";
			$rootScope.unfinishedFormTitle = "Event (New)";
        	if(vm.eventMaster != undefined && vm.eventMaster != null && vm.eventMaster.eventName != undefined && vm.eventMaster.eventName != null) {
        		$rootScope.subTitle = vm.eventMaster.eventName;
        	} else {
        		$rootScope.subTitle = "Unknown Event "
        	}
        })
      
	    $scope.$on('editEventMasterEvent', function(events, args){
	    	$rootScope.unfinishedData = vm.eventMaster;
			$rootScope.category = "Event";
			$rootScope.unfinishedFormTitle = "Event Edit # "+vm.eventMaster.eventNameCode;
	    })
	      
	    $scope.$on('addEventMasterEventReload', function(e, confirmation){
	    	    confirmation.message = "";
	    	    localStorage.reloadFormData = JSON.stringify(vm.eventMaster) ;
	    	    localStorage.isEventMasterReloaded = "YES";
	            e.preventDefault();
	    })
	    
	    $scope.$on('editEventMasterEventReload', function(e, confirmation){
	    	    confirmation.message = "";
	    	    localStorage.reloadFormData = JSON.stringify(vm.eventMaster) ;
	    	    localStorage.isEventMasterReloaded = "YES";
	            e.preventDefault();
	    	  
	    })

        /**
         * State Identification Handling Here.
         * @param {String} $stateParams.action - VIEW, ADD, EDIT, SEARCH.
         */
        //
        switch ($stateParams.action) {
            case "VIEW":
                vm.view($stateParams.id, true);
                $rootScope.breadcrumbArr = eventMasterService.getViewBreadCrumb();
                vm.selectedRecordIndex = parseInt($stateParams.selectedPageNumber);
                vm.totalRecord = parseInt($stateParams.totalRecord);
                break;
            case "ADD":
                if ($stateParams.fromHistory === 'Yes') {
                  if (vm.isReloaded == "YES") {
                   vm.isReloaded = "NO";
                   localStorage.isEventMasterReloaded = "NO";
                    vm.eventMaster = JSON.parse(localStorage.reloadFormData);
                  } else {
                    vm.eventMaster = $rootScope.selectedUnfilledFormData;
                  }
                } else {
                  if (vm.isReloaded == "YES") {
                    vm.isReloaded = "NO";
                    localStorage.isEventMasterReloaded = "NO";
                    vm.eventMaster = JSON.parse(localStorage.reloadFormData);
                  } else {
                	  vm.eventMaster={};
                  }
                }
                $rootScope.breadcrumbArr = eventMasterService.getAddBreadCrumb();
                break;
            case "EDIT":
                if (vm.fromHistory === 'Yes') {
                  if (vm.isReloaded == "YES") {
                    vm.isReloaded = "NO";
                    localStorage.isEventMasterReloaded = "NO";
                    vm.eventMaster = JSON.parse(localStorage.reloadFormData);
                  } else {
                    vm.eventMaster = $rootScope.selectedUnfilledFormData;
                  }
                } else {
                  if (vm.isReloaded == "YES") {
                    vm.isReloaded = "NO";
                    localStorage.isEventMasterReloaded = "NO";
                    vm.eventMaster = JSON.parse(localStorage.reloadFormData);
                  } else {
                      vm.view($stateParams.id, false);
                  }
                }
                $rootScope.breadcrumbArr = eventMasterService.getEditBreadCrumb();
                break;
            default:
                $rootScope.breadcrumbArr = eventMasterService.getListBreadCrumb();
                vm.searchInit();
        }

    }
]);
}) ();