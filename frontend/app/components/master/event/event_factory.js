	app.factory("EventSearch",['$resource', function($resource) {
		return $resource("/api/v1/eventmaster/get/search", {}, {
			query : {
				method : 'POST'
			}
		});
	}]);
	
	app.factory("EventSearchKeyword",['$resource', function($resource) {
		return $resource("/api/v1/eventmaster/get/search/keyword", {}, {
			fetch : {
				method : 'POST'
			}
		});
	}]);
	
	
	app.factory("SearchEventNotInList",['$resource', function($resource) {
		return $resource("/api/v1/eventmaster/get/search/exclude", {}, {
			fetch : {
				method : 'POST'
			}
		});
	}]);
	
	app.factory("EventAdd",['$resource', function($resource) {
		return $resource("/api/v1/eventmaster/create", {}, {
			save : {
				method : 'POST'
			}
		});
	}]);

	app.factory("EventEdit",['$resource', function($resource) {
		return $resource("/api/v1/eventmaster/update", {}, {
			update : {
				method : 'POST'
			}
		});
	}]);

	app.factory("EventView",['$resource', function($resource) {
		return $resource("/api/v1/eventmaster/get/id/:id", {}, {
			get : {
				method : 'GET',
				params : {
					id : ''
				},
				isArray : false
			}
		});
	}]);
	
	app.factory("EventRemove",['$resource', function($resource) {
		return $resource("/api/v1/eventmaster/delete/:id", {}, {
			remove : {
				method : 'DELETE',
				params : {
					id : ''
				},
				isArray : false
			}
		});
	}]);
