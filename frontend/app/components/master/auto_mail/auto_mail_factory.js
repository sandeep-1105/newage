(function() {

	app.factory("AutoMailList",['$resource', function($resource) {
		return $resource("/api/v1/automailmaster/get/search/keyword", {}, {
			fetch : {
				method : 'POST'
			}
		});
	}]);
	
	app.factory("ExternalAutoMailList",['$resource', function($resource) {
		return $resource("/api/v1/automailmaster/get/searchByExternal/keyword", {}, {
			fetch : {
				method : 'POST'
			}
		});
	}]);
	
	
	app.factory("AutoMailGroupList",['$resource', function($resource) {
		return $resource("/api/v1/automailgroupmaster/get/search/keyword", {}, {
			fetch : {
				method : 'POST'
			}
		});
	}]);
	
	app.factory("ExternalAutoMailGroupList",['$resource', function($resource) {
		return $resource("/api/v1/automailgroupmaster/get/searchByExternal/keyword", {}, {
			fetch : {
				method : 'POST'
			}
		});
	}]);
	
	app.factory("AutoMailSearch",['$resource', function($resource) {
		return $resource("/api/v1/automailmaster/get/search", {}, {
			query : {
				method : 'POST'
			}
		});
	}]);
	
	
	app.factory("AutoMailAdd",['$resource', function($resource) {
		return $resource("/api/v1/automailmaster/create", {}, {
			save : {
				method : 'POST'
			}
		});
	}]);

	app.factory("AutoMailEdit",['$resource', function($resource) {
		return $resource("/api/v1/automailmaster/update", {}, {
			update : {
				method : 'POST'
			}
		});
	}]);

	app.factory("AutoMailView",['$resource', function($resource) {
		return $resource("/api/v1/automailmaster/get/id/:id", {}, {
			get : {
				method : 'GET',
				params : {
					id : ''
				},
				isArray : false
			}
		});
	}]);
	
	
	app.factory("AutoMailRemove",['$resource', function($resource) {
		return $resource("/api/v1/automailmaster/delete/:id", {}, {
			remove : {
				method : 'DELETE',
				params : {
					id : ''
				},
				isArray : false
			}
		});
	}]);
	
	
	app.factory("AutoMailGroupAdd",['$resource', function($resource) {
		return $resource("/api/v1/automailgroupmaster/create", {}, {
			save : {
				method : 'POST'
			}
		});
	}]);

	app.factory("AutoMailGroupEdit",['$resource', function($resource) {
		return $resource("/api/v1/automailgroupmaster/update", {}, {
			update : {
				method : 'POST'
			}
		});
	}]);

	app.factory("AutoMailGroupView",['$resource', function($resource) {
		return $resource("/api/v1/automailgroupmaster/get/id/:id", {}, {
			get : {
				method : 'GET',
				params : {
					id : ''
				},
				isArray : false
			}
		});
	}]);
	
	
	app.factory("AutoMailGroupRemove",['$resource', function($resource) {
		return $resource("/api/v1/automailgroupmaster/delete/:id", {}, {
			remove : {
				method : 'DELETE',
				params : {
					id : ''
				},
				isArray : false
			}
		});
	}]);

})();
