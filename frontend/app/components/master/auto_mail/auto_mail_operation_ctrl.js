app.controller('autoMailOperationCtrl', ['$rootScope', '$scope', '$http', '$location', 'Upload', '$routeParams', 'cloneService',
    'autoMailService', 'AutoMailGroupList', 'AutoMailGroupAdd', 'AutoMailGroupEdit', 'AutoMailRemove', 'AutoMailView',
    'AutoMailAdd', 'AutoMailEdit', 'ngProgressFactory', 'ngDialog', 'discardService', '$timeout', '$state', '$stateParams',
    'RecentHistorySaveService', 'AutoMailGroupView', 'appConstant', 'CommonValidationService',
    function($rootScope, $scope, $http, $location, Upload, $routeParams, cloneService,
        autoMailService, AutoMailGroupList, AutoMailGroupAdd, AutoMailGroupEdit, AutoMailRemove, AutoMailView,
        AutoMailAdd, AutoMailEdit, ngProgressFactory, ngDialog, discardService, $timeout, $state, $stateParams,
        RecentHistorySaveService, AutoMailGroupView, appConstant, CommonValidationService) {



        $scope.pageAction = null;

        if ($stateParams.pageAction != undefined && $stateParams.pageAction != null) {
            $scope.pageAction = $stateParams.pageAction;
        }
        $scope.eventArr = $rootScope.enum['MailEventType'];
        $scope.searchText = "";
        $scope.showAutoMail = false;
        $scope.showGroupList = false;
        $scope.searchTextGroup = "";
        $scope.groupListConfig = {
            search: true,
            showCode: true,
            ajax: true,
            columns: [{
                    "title": "messageGroupCode",
                    seperator: false
                },
                {
                    "title": "messageGroupName",
                    seperator: true
                }
            ]
        }

        $scope.init = function() {
            if ($scope.pageAction == 'AUTOMAIL') {
                $scope.contained_progressbar = ngProgressFactory.createInstance();
                $scope.contained_progressbar.setParent(document.getElementById('autoMail-panel'));
                $scope.contained_progressbar.setAbsolute();
            } else if ($scope.pageAction == 'GROUP') {
                $scope.contained_progressbar = ngProgressFactory.createInstance();
                $scope.contained_progressbar.setParent(document.getElementById('group-panel'));
                $scope.contained_progressbar.setAbsolute();
            }
            if ($scope.pageAction == 'AUTOMAIL' && $scope.autoMailMaster != undefined && $scope.autoMailMaster.id != null) {
                $rootScope.breadcrumbArr = [{
                        label: "Master",
                        state: null
                    },
                    {
                        label: "Auto Mail",
                        state: null
                    }, {
                        label: "Edit Auto Mail",
                        state: null
                    },
                ];
            } else if ($scope.pageAction == 'AUTOMAIL' && $scope.autoMailMaster.id == null) {
                $rootScope.breadcrumbArr = [{
                        label: "Master",
                        state: null
                    },
                    {
                        label: "Auto Mail",
                        state: "layout.automail"
                    }, {
                        label: "Add Auto Mail",
                        state: null
                    },
                ];
            } else if ($scope.pageAction == 'GROUP' && $scope.autoMailGroupMaster.id == null) {

                $rootScope.breadcrumbArr = [{
                        label: "Master",
                        state: null
                    },
                    {
                        label: "Auto Mail",
                        state: "layout.automail"
                    }, {
                        label: "Add Group",
                        state: null
                    },
                ];
            } else if ($scope.pageAction == 'GROUP' && $scope.autoMailGroupMaster.id != null) {
                $rootScope.breadcrumbArr = [{
                        label: "Master",
                        state: null
                    },
                    {
                        label: "Auto Mail",
                        state: "layout.automail"
                    }, {
                        label: "Edit Group",
                        state: null
                    },
                ];
            }
        };

        $scope.cancel = function() {

            console.log("cancel method called for :", $scope.pageAction);
            var tempObj = $scope.autoMailMaster;
            if ($scope.pageAction == 'AUTOMAIL') {
                $scope.isDirty = !discardService.compare(tempObj);
            }
            if ($scope.pageAction == 'GROUP') {
                $scope.isDirty = !discardService.compare(tempObj);
            }
            if ($scope.isDirty) {
                if (Object.keys(tempObj).length) {
                    var newScope = $scope.$new();
                    newScope.errorMessage = $rootScope.nls["ERR200"];
                    ngDialog.openConfirm({
                            template: '<p>{{errorMessage}}</p>' +
                                '<div class="ngdialog-footer">' +
                                ' <button type="button" class="btn btn-default btn-property cancel-btn" ng-click="confirm(2)">No</button>' +
                                '<button type="button" class="btn btn-default btn-property accent-btn " ng-click="confirm(1)">Yes</button>' +
                                '<button type="button" class="btn btn-default btn-property pull-left grey-cancel-btn" ng-click="confirm(3)">Cancel</button>' +
                                '</div>',
                            plain: true,
                            scope: newScope,
                            className: 'ngdialog-theme-default'
                        })
                        .then(
                            function(value) {
                                if ($scope.pageAction == 'AUTOMAIL') {

                                    if (value == 1 &&
                                        $scope.autoMailMaster.id == null) {
                                        $scope.save();
                                    } else if (value == 1 &&
                                        $scope.autoMailMaster.id != null) {
                                        $scope.update();
                                    } else if (value == 2) {
                                        $state.go("layout.automail", {
                                            submitAction: "Cancelled"
                                        });
                                    } else {
                                        console.log("cancelled");
                                    }
                                } else if ($scope.pageAction == 'GROUP') {

                                    if (value == 1 &&
                                        $scope.autoMailMaster.autoMailGroupMaster.id == null) {
                                        $scope.save();
                                    } else if (value == 1 &&
                                        $scope.autoMailMaster.autoMailGroupMaster.id != null) {
                                        $scope.update();
                                    } else if (value == 2) {
                                        $state.go("layout.automail", {
                                            submitAction: "Cancelled"
                                        });
                                    } else {
                                        console.log("cancelled");
                                    }
                                }


                            });
                } else {
                    $state.go("layout.automail", {
                        submitAction: "Cancelled"
                    });
                }
            } else {

                $state.go("layout.automail", {
                    submitAction: "Cancelled"
                });
            }
        }

        $scope.addrow = function() {
            if ($scope.validateAutoMailMaster(0)) {
                $scope.selectedAutoMailMaster = {};
                $scope.selectedAutoMailMaster.autoMailStatus = 'Enable';
                $scope.showAutoMail = true;
                $scope.editIndex = null;
                $scope.addAutoMailMapping.show = true;
                discardService.set($scope.selectedAutoMailMaster);
            }
        }
        $scope.editIndex = null;
        $scope.editrow = function(index) {
            $scope.selectedAutoMailMaster = cloneService.clone($scope.autoMailMaster.autoMailGroupMaster.autoMailMasterList[index]);
            $scope.showAutoMail = true;
            $scope.editIndex = index;
            $scope.addAutoMailMapping.show = true;
            discardService.set($scope.selectedAutoMailMaster);
        }

        $scope.discardMapping = function() {
            if (!discardService.compare($scope.selectedAutoMailMaster)) {

                ngDialog.openConfirm({
                    template: '<p>Do you want to update your changes?</p>' +
                        '<div class="ngdialog-footer">' +
                        '<button type="button" class="btn btn-default btn-property cancel-btn" ng-click="confirm(2)">No</button>' +
                        '<button type="button" class="btn btn-primary btn-property accent-btn" ng-click="confirm(1)">Yes</button>' +
                        '</div>',
                    plain: true,
                    className: 'ngdialog-theme-default'
                }).then(function(value) {
                    if (value == 1) {
                        $scope.saveMapping();
                    } else {
                        $scope.cancelMapping();
                    }
                });

            } else {
                $scope.cancelMapping();
            }
        }


        $scope.deleterow = function(index) {
            ngDialog.openConfirm({
                template: '<p>Are you sure you want to delete selected auto mail ?</p>' +
                    '<div class="ngdialog-footer">' +
                    '<button type="button" class="btn btn-default btn-property cancel-btn" ng-click="closeThisDialog(0)">No' +
                    '<button type="button" class="btn btn-primary btn-property accent-btn" ng-click="confirm(1)">Yes' +
                    '</button>' +
                    '</div>',
                plain: true,
                className: 'ngdialog-theme-default'
            }).
            then(function(value) {
                $scope.autoMailMaster.autoMailGroupMaster.autoMailMasterList.splice(index, 1);
                console.log("auto mail Deleted");
            }, function(value) {
                console.log("auto mail Deletion cancelled");
            });

        }

        $scope.saveMapping = function() {
            console.log("Save Button is Pressed.");

            if ($scope.validateGroupAutoMailMaster(0)) {

                console.log("editIndex:" + $scope.editIndex);


                if ($scope.editIndex == null) {
                    if ($scope.autoMailMaster.autoMailGroupMaster.autoMailMasterList == null)
                        $scope.autoMailMaster.autoMailGroupMaster.autoMailMasterList = [];
                    $scope.autoMailMaster.autoMailGroupMaster.autoMailMasterList.push($scope.selectedAutoMailMaster);
                } else {
                    $scope.autoMailMaster.autoMailGroupMaster.autoMailMasterList.splice($scope.editIndex, 1, $scope.selectedAutoMailMaster);
                }
                $scope.cancelMapping();
            } else {
                console.log("Mapping added Failed with empty Objects")
            }
        }

        /* Cancel Mapping Form */
        $scope.cancelMapping = function() {
            console.log("Cancel Button is Pressed.");
            $scope.selectedAutoMailMaster = {};
            $scope.searchText = "";
            $scope.showAutoMail = false;
            $scope.addAutoMailMapping.show = false;
        }




        $scope.cancelGroup = function() {
            $scope.searchTextGroup = "";
            $scope.showGroupList = false;
        }


        $scope.showAutoMailGroups = function(group) {
            $scope.groupSelected = -1;
            $scope.groupPanelTitle = "Select Auto Mail Group";
            /*$scope.ajaxGroupEvent(null);*/
        };

        $scope.ajaxGroupEvent = function(object) {
            $scope.searchDto = {};
            $scope.searchDto.keyword = object == null ? "" : object;
            $scope.searchDto.selectedPageNumber = 0;
            $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
            return AutoMailGroupList.fetch($scope.searchDto).$promise.then(function(data) {
                if (data.responseCode == "ERR0") {
                    $scope.autoMailGroupList = data.responseObject.searchResult;
                    return $scope.autoMailGroupList;
                    /*$scope.showGroupList = true;*/

                }
            }, function(errResponse) {
                console.error('Error while fetching Auto Mail Group');
            });

        }
        //$scope.ajaxGroupEvent(null);

        $scope.selectedGroup = function(obj, key) {
            if (obj != undefined && obj != null && obj != "") {
                $rootScope.navigateToNextField(key);
            }

        };
        $scope.tableGroupState = function(param) {

            $scope.tableState = param;

        }

        $scope.update = function() {

            $scope.spinner = true;
            $scope.contained_progressbar = ngProgressFactory.createInstance();
            if ($scope.validateAutoMailMaster(0)) {
                if ($scope.pageAction == 'AUTOMAIL') {
                    $scope.contained_progressbar.start();
                    AutoMailAdd.save($scope.autoMailMaster).$promise.then(
                        function(data) {
                            if (data.responseCode == "ERR0") {
                                console.log("Auto Mail updated Successfully")
                                $rootScope.successDesc = $rootScope.nls["ERR401"];
                                $scope.listView('Saved');
                            } else {
                                console.log("Auto Mail updated Failed " +
                                    data.responseDescription)
                            }
                            $scope.contained_progressbar.complete();
                            angular.element(".panel-body").animate({
                                scrollTop: 0
                            }, "slow");
                        },
                        function(error) {
                            console.log("Auto Mail updated Failed : " + error)
                        });
                } else if ($scope.pageAction == 'GROUP') {
                    if (!$scope.isListValid()) {
                        console.log("table invalid")
                        $rootScope.clientMessage = $rootScope.nls["ERR11044"];
                        console.log($rootScope.nls["ERR11044"]);
                        return false;
                    }
                    if ($scope.autoMailGroupMaster.autoMailMasterList != null && $scope.autoMailGroupMaster.autoMailMasterList != undefined && $scope.autoMailGroupMaster.autoMailMasterList != '') {
                        /*  for(var i=0;i<$scope.autoMailGroupMaster.autoMailMasterList.length;i++)
                                {
                                if($scope.autoMailGroupMaster.autoMailMasterList[i].autoMailStatus==undefined || $scope.autoMailGroupMaster.autoMailMasterList[i].autoMailStatus==null)
                                $scope.autoMailGroupMaster.autoMailMasterList[i].autoMailStatus='Enable';
                                }*/
                    }
                    AutoMailGroupAdd.save($scope.autoMailGroupMaster).$promise.then(
                        function(data) {
                            if (data.responseCode == "ERR0") {
                                console.log("Group updated Successfully")
                                $rootScope.successDesc = $rootScope.nls["ERR401"];
                                $scope.listView(Saved);
                            } else {
                                console.log("Group updated Failed " +
                                    data.responseDescription)
                            }
                            $scope.contained_progressbar.complete();
                            angular.element(".panel-body").animate({
                                scrollTop: 0
                            }, "slow");
                        },
                        function(error) {
                            console.log("Group updated Failed : " + error)
                        });
                }
            } else {
                console.log("Validation Failed");
            }
        }
        $scope.listView = function(submitAction) {
            autoMailService.set({});
            autoMailService.setPageAction(null);
            $state.go("layout.automail", {
                submitAction: submitAction
            });
        }


        $scope.save = function() {

            $scope.spinner = true;
            if ($scope.validateAutoMailMaster(0)) {
                if ($scope.pageAction == 'AUTOMAIL') {
                    $scope.contained_progressbar.start();
                    AutoMailAdd.save($scope.autoMailMaster).$promise.then(function(data) {
                        if (data.responseCode == "ERR0") {
                            console.log("Auto Mail added Successfully")
                            if ($scope.autoMailMaster.id == undefined || $scope.autoMailMaster.id == null) {
                                $rootScope.successDesc = $rootScope.nls["ERR400"];
                            } else {
                                $rootScope.successDesc = $rootScope.nls["ERR401"];
                            }
                            $scope.listView('Saved');
                        } else {
                            console.log("Auto Mail added Failed " + data.responseDescription)
                        }
                        $scope.contained_progressbar.complete();
                        angular.element(".panel-body").animate({
                            scrollTop: 0
                        }, "slow");
                    }, function(error) {
                        console.log("Auto Mail added Failed : " + error)
                    });
                } else if ($scope.pageAction == 'GROUP') {
                    if (!$scope.isListValid()) {
                        $rootScope.clientMessage = $rootScope.nls["ERR11044"];
                        console.log($rootScope.nls["ERR11044"]);
                        return false;
                    }
                    if ($scope.autoMailGroupMaster.autoMailMasterList != null && $scope.autoMailGroupMaster.autoMailMasterList != undefined && $scope.autoMailGroupMaster.autoMailMasterList != '' && ($scope.autoMailGroupMaster.id == undefined || $scope.autoMailGroupMaster.id == null)) {
                        for (var i = 0; i < $scope.autoMailGroupMaster.autoMailMasterList.length; i++) {
                            $scope.autoMailGroupMaster.autoMailMasterList[i].autoMailStatus = 'Enable';
                        }
                    }
                    AutoMailGroupAdd.save($scope.autoMailGroupMaster).$promise.then(function(data) {
                        if (data.responseCode == "ERR0") {
                            console.log("Group added Successfully")
                            if ($scope.autoMailGroupMaster.id == undefined || $scope.autoMailGroupMaster.id == null) {
                                $rootScope.successDesc = $rootScope.nls["ERR400"];
                            } else {
                                $rootScope.successDesc = $rootScope.nls["ERR401"];
                            }
                            $scope.listView('Saved');
                        } else {
                            console.log("Group added Failed " + data.responseDescription)
                        }
                        $scope.contained_progressbar.complete();
                        angular.element(".panel-body").animate({
                            scrollTop: 0
                        }, "slow");
                    }, function(error) {
                        console.log("Group added Failed : " + error)
                    });
                }
            } else {
                console.log("Validation Failed");
            }

        }
        $scope.validateAutoMailMaster = function(validateCode) {

            console.log("Validate Called", $scope.pageAction);
            $scope.errorMap = new Map();


            if ($scope.pageAction == 'AUTOMAIL') {


                if (validateCode == 0 || validateCode == 1) {
                    if ($scope.autoMailMaster.autoMailGroupMaster == undefined || $scope.autoMailMaster.autoMailGroupMaster == null ||
                        $scope.autoMailMaster.autoMailGroupMaster.id == null || $scope.autoMailMaster.autoMailGroupMaster.id == "") {

                        $scope.errorMap.put("group", $scope.nls["ERR11000"]);
                        console.log($scope.nls["ERR11000"]);
                        return false;

                    }

                }



                if (validateCode == 0 || validateCode == 2) {

                    if ($scope.autoMailMaster.messageName == undefined ||
                        $scope.autoMailMaster.messageName == null ||
                        $scope.autoMailMaster.messageName == "") {

                        $scope.errorMap.put("messageName", $scope.nls["ERR10011"]);
                        return false;

                    } else {

                        if (!CommonValidationService.checkRegExp(appConstant.Reg_Exp_Master_AUTO_MAIL_NAME, $scope.autoMailMaster.messageName)) {
                            $scope.errorMap.put("messageName", $rootScope.nls["ERR10010"]);
                            return false;
                        }
                    }
                }


                if (validateCode == 0 || validateCode == 3) {

                    if ($scope.autoMailMaster.messageCode == undefined ||
                        $scope.autoMailMaster.messageCode == null ||
                        $scope.autoMailMaster.messageCode == "") {

                        $scope.errorMap.put("messageCode", $scope.nls["ERR10021"]);
                        return false;
                    } else {

                        if (!CommonValidationService.checkRegExp(appConstant.Reg_Exp_Master_AUTO_MAIL_CODE, $scope.autoMailMaster.messageCode)) {
                            $scope.errorMap.put("messageCode", $rootScope.nls["ERR10020"]);
                            return false;
                        }
                    }
                }

            } else if ($scope.pageAction == 'GROUP') {

                if (validateCode == 0 || validateCode == 1) {
                    if ($scope.autoMailGroupMaster.messageGroupName == undefined ||
                        $scope.autoMailGroupMaster.messageGroupName == null ||
                        $scope.autoMailGroupMaster.messageGroupName == "") {
                        $scope.errorMap.put("messageGroupName", $scope.nls["ERR11041"]);
                        return false;
                    } else {

                        if (!CommonValidationService.checkRegExp(appConstant.Reg_Exp_Master_AUTO_MAIL_GROUP_NAME, $scope.autoMailGroupMaster.messageGroupName)) {
                            $scope.errorMap.put("messageGroupName", $rootScope.nls["ERR11040"]);
                            return false;
                        }

                    }
                }
                if (validateCode == 0 || validateCode == 2) {
                    if ($scope.autoMailGroupMaster.messageGroupCode == undefined ||
                        $scope.autoMailGroupMaster.messageGroupCode == null ||
                        $scope.autoMailGroupMaster.messageGroupCode == "") {
                        $scope.errorMap.put("messageGroupCode", $scope.nls["ERR11021"]);
                        return false;
                    } else {
                        if (!CommonValidationService.checkRegExp(appConstant.Reg_Exp_Master_AUTO_MAIL_GROUP_CODE, $scope.autoMailGroupMaster.messageGroupCode)) {
                            $scope.errorMap.put("messageGroupCode", $rootScope.nls["ERR11020"]);
                            return false;
                        }
                    }
                }
            }
            return true;
        }

        $scope.removeEmptyObject = function() {
            var lastObject = $scope.autoMailGroupMaster.autoMailMasterList[$scope.autoMailGroupMaster.autoMailMasterList.length - 1];
            if (lastObject.id == undefined && lastObject.messageName == undefined && lastObject.messageCode == undefined) {
                $scope.autoMailGroupMaster.autoMailMasterList.splice($scope.autoMailGroupMaster.autoMailMasterList.length - 1, 1);
            }
        }
        $scope.validateGroupAutoMailMaster = function(validateCode) {
            $scope.errorMap = new Map();
            if ($scope.pageAction == 'GROUP') {
                console.log("inside autoMail Group");
                if (validateCode == 0 || validateCode == 1) {
                    if ($scope.selectedAutoMailMaster.messageName == undefined ||
                        $scope.selectedAutoMailMaster.messageName == null ||
                        $scope.selectedAutoMailMaster.messageName == "") {
                        $scope.errorMap.put("messageName", $scope.nls["ERR10011"]);
                        return false;
                    } else {

                        var regexp = new RegExp("^[0-9a-zA-Z ]{0,100}$");
                        if (!regexp
                            .test($scope.selectedAutoMailMaster.messageName)) {
                            $scope.errorMap.put("messageName", $scope.nls["ERR10010"]);
                            return false;
                        }

                    }
                }
                if (validateCode == 0 || validateCode == 2) {
                    if ($scope.selectedAutoMailMaster.messageCode == undefined ||
                        $scope.selectedAutoMailMaster.messageCode == null ||
                        $scope.selectedAutoMailMaster.messageCode == "") {
                        $scope.errorMap.put("messageCode", $scope.nls["ERR10021"]);
                        return false;
                    } else {

                        var regexp = new RegExp("^[0-9A-Z]{0,10}$");
                        if (!regexp
                            .test($scope.selectedAutoMailMaster.messageCode)) {
                            $scope.errorMap.put("messageCode", $scope.nls["ERR10020"]);
                            return false;
                        }

                    }
                }


            }

            return true;

        }

        $scope.setStatus = function(data) {
            if (data == 'Enable') {
                return 'activetype';
            } else if (data == 'Disable') {
                return 'blockedtype';
            }
        }

        $scope.mailGroupRender = function(item) {
            return {
                label: item.messageGroupName,
                item: item
            }
        }
        $scope.$on('addAutomailEvent', function(events, args) {
            $rootScope.unfinishedData = $scope.autoMailMaster;
            $rootScope.category = "Auto Mail";
            $rootScope.unfinishedFormTitle = "Auto Mail (New)";
            if ($scope.autoMailMaster != undefined && $scope.autoMailMaster != null) {
                $rootScope.subTitle = "";
            } else {
                $rootScope.subTitle = "Unknown AutoMail"
            }
        })

        $scope.$on('editAutomailEvent', function(events, args) {
            $rootScope.unfinishedData = $scope.autoMailMaster;
            $rootScope.category = "Auto Mail";

            $rootScope.unfinishedFormTitle = "Auto Mail Edit #";
            if ($scope.autoMailMaster != undefined && $scope.autoMailMaster != null) {
                $rootScope.subTitle = ""; //$scope.costCenterMaster;
            } else {
                $rootScope.subTitle = "Unknown AutoMail"
            }
        })
        $scope.$on('addAutomailEventReload', function(e, confirmation) {
            confirmation.message = "All data willl be lost.";
            localStorage.reloadFormData = JSON.stringify($scope.autoMailMaster);
            localStorage.isAutoMailReloaded = "YES";
            e.preventDefault();
        });
        $scope.$on('editAutomailEventReload', function(e, confirmation) {
            confirmation.message = "All data willl be lost.";
            localStorage.reloadFormData = JSON.stringify($scope.autoMailMaster);
            localStorage.isAutoMailReloaded = "YES";
            e.preventDefault();
        });
        $scope.isReloaded = localStorage.isAutoMailReloaded;
        if ($stateParams.pageAction == "AUTOMAIL") {
            $scope.pageAction = "AUTOMAIL";
            if ($stateParams.action == "ADD") {
                $scope.autoMailMaster = {};
                $scope.autoMailMaster.autoMailStatus = 'Enable';
                $scope.autoMailMaster.autoMailGroupMaster = {};
                if ($stateParams.fromHistory === 'Yes') {
                    if ($scope.isReloaded == "YES") {
                        $scope.isReloaded = "NO";
                        localStorage.isAutoMailReloaded = "NO";
                        try {
                            $scope.autoMailMaster = JSON.parse(localStorage.reloadFormData);
                            $scope.autoMailMaster.autoMailStatus = 'Enable';
                        } catch (e) {
                            console.log("error in auto group" + e);
                            $scope.autoMailMaster = {};
                            $scope.autoMailMaster.autoMailStatus = 'Enable';
                        }
                    } else {
                        $scope.autoMailMaster = $rootScope.selectedUnfilledFormData;
                        $scope.autoMailMaster.autoMailStatus = 'Enable';
                    }
                } else {
                    if ($scope.isReloaded == "YES") {
                        $scope.isReloaded = "NO";
                        localStorage.isAutoMailReloaded = "NO";
                        try {
                            $scope.autoMailMaster = JSON.parse(localStorage.reloadFormData);
                            $scope.autoMailMaster.autoMailStatus = 'Enable';
                        } catch (e) {
                            console.log("error in auto group" + e);
                            $scope.autoMailMaster = {};
                            $scope.autoMailMaster.autoMailStatus = 'Enable';
                        }
                    } else {
                        $scope.autoMailMaster = {};
                        $scope.autoMailMaster.autoMailStatus = 'Enable';
                        $scope.autoMailMaster.autoMailGroupMaster = {};
                    }
                }
                $scope.init();
            } else {
                if ($stateParams.fromHistory === 'Yes') {
                    if ($scope.isReloaded == "YES") {
                        $scope.isReloaded = "NO";
                        localStorage.isAutoMailReloaded = "NO";
                        try {
                            $scope.autoMailMaster = JSON.parse(localStorage.reloadFormData);
                        } catch (e) {
                            console.log("error in auto group" + e);
                            $scope.autoMailMaster = {};
                        }
                        $scope.init();
                    } else {
                        $scope.autoMailMaster = $rootScope.selectedUnfilledFormData;
                        $scope.init();
                    }

                } else {
                    if ($scope.isReloaded == "YES") {
                        $scope.isReloaded = "NO";
                        localStorage.isAutoMailReloaded = "NO";
                        try {
                            $scope.autoMailMaster = JSON.parse(localStorage.reloadFormData);
                        } catch (e) {
                            console.log("error in auto group" + e);
                            $scope.autoMailMaster = {};
                        }
                        $scope.init();
                    } else {
                        AutoMailView.get({
                            id: $stateParams.automailId
                        }, function(data) {
                            if (data.responseCode == "ERR0") {
                                console.log("CostCenter view Successfully")
                                $scope.autoMailMaster = data.responseObject;
                                $scope.autoMailMaster.autoMailGroupMaster = {};
                                $scope.autoMailMaster.autoMailGroupMaster = $scope.autoMailMaster.transAutoMailGroupMaster;
                                $scope.pageAction = 'AUTOMAIL';
                                $rootScope.unfinishedFormTitle = "Auto Mail Edit # " + $scope.autoMailMaster.messageCode;
                                var rHistoryObj = {
                                    'title': 'Auto Mail Edit # ' + $scope.autoMailMaster.messageCode,
                                    'subTitle': $scope.autoMailMaster.messageName,
                                    'stateName': $state.current.name,
                                    'stateParam': JSON.stringify($stateParams),
                                    'stateCategory': 'Auto Mail',
                                    'serviceType': 'AIR',
                                    'serviceCode': "",
                                    'orginAndDestination': ""
                                }
                                $scope.init();
                                RecentHistorySaveService.form(rHistoryObj);
                            } else {
                                console.log("CostCenter view Failed " + data.responseDescription)
                            }
                        }, function(error) {
                            console.log("CostCenter view Failed : " + error)
                        });
                    }
                }
            }
        } else {
            $scope.pageAction = "GROUP";
            if ($stateParams.action == "ADD") {
                $scope.autoMailGroupMaster = {};
                if ($stateParams.fromHistory === 'Yes') {
                    if ($scope.isReloaded == "YES") {
                        $scope.isReloaded = "NO";
                        localStorage.isAutoMailReloaded = "NO";
                        try {
                            $scope.autoMailGroupMaster = JSON.parse(localStorage.reloadFormData);
                        } catch (e) {
                            console.log("error in auto group" + e);
                            $scope.autoMailGroupMaster = {};
                        }
                    } else {
                        $scope.autoMailGroupMaster = $rootScope.selectedUnfilledFormData;
                    }
                } else {
                    if ($scope.isReloaded == "YES") {
                        $scope.isReloaded = "NO";
                        localStorage.isAutoMailReloaded = "NO";
                        try {
                            $scope.autoMailGroupMaster = JSON.parse(localStorage.reloadFormData);
                        } catch (e) {
                            console.log("error in auto group" + e);
                            $scope.autoMailGroupMaster = {};
                        }
                    } else {
                        $scope.autoMailGroupMaster = {};
                        //$scope.autoMailGroupMaster.autoMailMasterList = [];
                        //$scope.autoMailGroupMaster.autoMailMasterList.push({});
                    }
                }
                $scope.init();
            } else {
                if ($stateParams.fromHistory === 'Yes') {
                    if ($scope.isReloaded == "YES") {
                        $scope.isReloaded = "NO";
                        localStorage.isAutoMailReloaded = "NO";
                        try {
                            $scope.autoMailGroupMaster = JSON.parse(localStorage.reloadFormData);
                        } catch (e) {
                            console.log("error in auto group" + e);
                            $scope.autoMailGroupMaster = {};
                        }
                    } else {
                        $scope.autoMailGroupMaster = $rootScope.selectedUnfilledFormData;
                        $scope.init();
                    }
                } else {
                    if ($scope.isReloaded == "YES") {
                        $scope.isReloaded = "NO";
                        localStorage.isAutoMailReloaded = "NO";
                        try {
                            $scope.autoMailGroupMaster = JSON.parse(localStorage.reloadFormData);
                        } catch (e) {
                            console.log("error in auto group" + e);
                            $scope.autoMailGroupMaster = {};
                        }
                        $scope.init();
                    } else {
                        console.log("get by group");
                        if ($stateParams.groupId != undefined && $stateParams.groupId != null) {
                            AutoMailGroupView.get({
                                id: $stateParams.groupId
                            }, function(data) {
                                if (data.responseCode == "ERR0") {
                                    console.log("Auto Mail Group view Successfully")
                                    $scope.autoMailGroupMaster = data.responseObject;
                                    $scope.init();
                                } else {
                                    console.log("Auto Mail Group view Failed " +
                                        data.responseDescription)
                                }
                                $scope.showHistory = false;
                                $scope.groupSwitchType = false;
                            }, function(error) {
                                console.log("Auto Mail Group view Failed : " + error)
                            });
                        }
                    }
                }
            }
        }


        // History Start

        $scope.getAutoMailById = function(automailId) {
            console.log("getAutoMailById ", automailId);
            AutoMailView.get({
                id: automailId
            }, function(data) {
                if (data.responseCode == "ERR0") {
                    console.log("Successful while getting automail.", data)
                    $scope.autoMailMaster = data.responseObject;
                    $scope.autoMailMaster.autoMailGroupMaster = {};
                    $scope.autoMailMaster.autoMailGroupMaster = $scope.autoMailMaster.transAutoMailGroupMaster;
                }
            }, function(error) {
                console.log("Error while getting automail.", error)
            });

        }


        //On leave the Unfilled automail Form Broadcast then event will happen in StateChangeListener(BroadCaster)-app_state_route.js

        $scope.$on('addAutoMailEvent', function(events, args) {

            $rootScope.unfinishedData = $scope.autoMailMaster;
            $rootScope.automail = "Master";
            $rootScope.unfinishedFormTitle = "AutoMail (New)";
            if ($scope.autoMailMaster != undefined && $scope.autoMailMaster != null && $scope.autoMailMaster.messageName != undefined &&
                $scope.autoMailMaster.messageName != null && $scope.autoMailMaster.messageName != "") {
                $rootScope.subTitle = $scope.autoMailMaster.messageName;
            } else {
                $rootScope.subTitle = "Unknown AutoMail"
            }
        })

        $scope.$on('editAutoMailEvent', function(events, args) {
            $rootScope.unfinishedData = $scope.autoMailMaster;
            $rootScope.automail = "Master";
            $rootScope.unfinishedFormTitle = "AutoMail Edit";
            if ($scope.autoMailMaster != undefined && $scope.autoMailMaster != null && $scope.autoMailMaster.messageName != undefined &&
                $scope.autoMailMaster.messageName != null && $scope.autoMailMaster.messageName != "") {
                $rootScope.subTitle = $scope.autoMailMaster.messageName;
            } else {
                $rootScope.subTitle = "Unknown AutoMail"
            }
        })


        $scope.$on('addAutoMailEventReload', function(e, confirmation) {
            console.log("addAutoMailEventReload is called ", $scope.autoMailMaster);
            confirmation.message = "";
            localStorage.reloadFormData = JSON.stringify($scope.autoMailMaster);
            localStorage.isAutoMailReloaded = "YES";
            e.preventDefault();
        });


        $scope.$on('editAutoMailEventReload', function(e, confirmation) {
            confirmation.message = "";
            localStorage.reloadFormData = JSON.stringify($scope.autoMailMaster);
            localStorage.isAutoMailReloaded = "YES";
            e.preventDefault();
        });


        /*  $scope.isReloaded = localStorage.isAutoMailReloaded;
            
            if ($stateParams.action == "ADD") {
                if($stateParams.fromHistory === 'Yes') {
                    if($scope.isReloaded == "YES") {
                        console.log("Add History Reload...")
                        $scope.isReloaded = "NO";
                        localStorage.isAutoMailReloaded = "NO";
                        $scope.autoMailMaster  = JSON.parse(localStorage.reloadFormData);
                        $scope.init();
                    } else {
                        console.log("Add History NotReload...")
                        $scope.autoMailMaster = $rootScope.selectedUnfilledFormData;
                        $scope.init();          
                    }
                } else {
                    if($scope.isReloaded == "YES") {
                        $scope.isReloaded = "NO";
                        localStorage.isAutoMailReloaded = "NO";
                        $scope.autoMailMaster  = JSON.parse(localStorage.reloadFormData);
                        console.log("Add NotHistory Reload...");
                        $scope.init();              
                    } else {
                        console.log("Add NotHistory NotReload...")
                        $scope.init();
                    }   
                }
                $rootScope.breadcrumbArr = [
                                            { label:"Master", 
                                              state:"layout.automail"}, 
                                            { label:"AutoMail", 
                                              state:"layout.automail"}, 
                                            { label:"Add AutoMail", state:null}];   

                
            } else {
                if($stateParams.fromHistory === 'Yes') {
                    if($scope.isReloaded == "YES") {
                        $scope.isReloaded = "NO";
                        localStorage.isAutoMailReloaded = "NO";
                        $scope.autoMailMaster  = JSON.parse(localStorage.reloadFormData);
                        $scope.init();          } else {
                        $scope.autoMailMaster = $rootScope.selectedUnfilledFormData;
                        $scope.init();          
                        }
                } else {
                    if($scope.isReloaded == "YES") {
                        $scope.isReloaded = "NO";
                        localStorage.isAutoMailReloaded = "NO";
                        $scope.autoMailMaster  = JSON.parse(localStorage.reloadFormData);
                        $scope.init();
                    } else {
                        $scope.getAutoMailById($stateParams.automailId);
                        $scope.init();
                    }
                }
                $rootScope.breadcrumbArr = [
                                            { label:"Master", 
                                              state:null}, 
                                            { label:"AutoMail", 
                                              state:"layout.automail"}, 
                                            { label:"Edit AutoMail", state:null}];  
            }
            */

        // History End

    }
]); 