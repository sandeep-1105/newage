
app.factory('autoMailService',[ function() {
	var savedData = {};
	var pageAction="";
	function set(data) {
		savedData = data;
		localStorage.savedData=JSON.stringify(savedData);	
	};
	function get() {
		savedData = localStorage.savedData==null?null:JSON.parse(localStorage.savedData);
		return savedData;
	};

	function setPageAction(data) {
		pageAction = data;
		localStorage.pageAction=pageAction;
		
	};
	function getPageAction() {
		pageAction = localStorage.pageAction;
		return pageAction;
	};
	return {
		set : set,
		get : get,
		setPageAction : setPageAction,
		getPageAction : getPageAction
	}
	
}]);