app.controller('autoMailCtrl', ['$rootScope', '$scope', '$http', '$location', '$window',
    'ngTableParams', 'ngDialog', '$state', '$stateParams',
    'AutoMailRemove', 'AutoMailSearch', 'AutoMailGroupRemove', 'autoMailService', 'AutoMailGroupView', 'roleConstant',
    function($rootScope, $scope, $http, $location, $window,
        ngTableParams, ngDialog, $state, $stateParams,
        AutoMailRemove, AutoMailSearch, AutoMailGroupRemove, autoMailService, AutoMailGroupView, roleConstant) {

        /************************************************************
         * ux - by Muthu
         * reason - ipad compatibility fix
         *
         * *********************************************************/
        $scope.deskTopView = true;

        /********************* ux fix ends here *****************************/
        $scope.autoMailHeadArr = [{
                "name": "#",
                "width": "col-xs-0half",
                "model": "no",
                "search": false,
            },
            {
                "name": "Name",
                "width": "col-xs-3",
                "model": "messageName",
                "search": true,
                "wrap_cell": true,
                "type": "text",
                "sort": true,
                "key": "searchMessageName"

            },
            {
                "name": "Code",
                "width": "col-xs-2half",
                "model": "messageCode",
                "search": true,
                "type": "text",
                "sort": true,
                "key": "searchMessageCode"
            },
            {
                "name": "Internal / External",
                "width": "col-xs-2",
                "model": "messageSendType",
                "search": true,
                "type": "text",
                "sort": true,
                "key": "searchMessageSendType"
            },
            {
                "name": "Group Name",
                "width": "col-xs-2",
                "model": "transAutoMailGroupMaster.messageGroupName",
                "search": true,
                "type": "text",
                "sort": true,
                "key": "searchMessageGroupName"
            },
            {
                "name": "Status",
                "width": "w120px",
                "model": "autoMailStatus",
                "search": true,
                "type": "drop",
                "sort": true,
                "data": $rootScope.enum['Status'],
                "key": "searchStatus"
            }
        ]


        $scope.sortSelection = {
            sortKey: "messageName",
            sortOrder: "asc"
        }

        $scope.searchDto = {};
        $scope.autoMailMaster = {};
        $scope.autoMailGroupMaster = {};
        $scope.groupSwitchType = false;
        $scope.limitArr = [10, 15, 20];
        $scope.page = 0;
        $scope.limit = 10;


        $rootScope.breadcrumbArr = [{
                label: "Master",
                state: null
            },
            {
                label: "Auto Mail",
                state: null
            }
        ];

        $scope.init = function() {
            $rootScope.setNavigate1("Master");
            $rootScope.setNavigate2("Auto Mail");
            $scope.searchDto = {};
            $scope.search();
            $scope.autoMailMaster = {};

            $scope.searchDto.orderByType = "ASC";
            $scope.autoMailSortIconClass = " ";
            $scope.sortIconAutoMailName = "fa fa-chevron-down";
            $scope.searchDto.sortByColumn = "messageName";
            $scope.showHistory = false;

        };

        $scope.cancel = function() {
            $scope.autoMailMaster = {};
            $scope.showHistory = false;
            /************************************************************
             * ux - by Muthu
             * reason - ipad compatibility fix
             *
             * *********************************************************/
            $scope.deskTopView = true;

            /********************* ux fix ends here *****************************/
            ;
        }

        $scope.addAutoMail = function() {
            if ($rootScope.roleAccess(roleConstant.MASTER_GENERAL_AUTO_MAIL_CREATE)) {
                console.log("Add Auto Mail Button Pressed...");
                $scope.autoMailMaster = {};
                autoMailService.set($scope.autoMailMaster);
                autoMailService.setPageAction("AUTOMAIL");
                var params = {};
                params.pageAction = 'AUTOMAIL';
                $state.go('layout.addAutomail', params);
            }
        }

        $scope.addGroup = function() {
            if ($rootScope.roleAccess(roleConstant.MASTER_GENERAL_AUTO_MAIL_GROUP_CREATE)) {
                console.log("Add Auto Mail Group Button Pressed...");
                $scope.autoMailMaster = {};
                autoMailService.set($scope.autoMailMaster);
                autoMailService.setPageAction("GROUP");
                var params = {};
                params.pageAction = 'GROUP';
                $state.go('layout.addAutomail', params);
            }
        }




        $scope.editAutoMail = function(autoMailObj) {
            if ($rootScope.roleAccess(roleConstant.MASTER_GENERAL_AUTO_MAIL_MODIFY)) {
                autoMailService.set(autoMailObj);
                autoMailService.setPageAction("AUTOMAIL");
                var params = {};
                params.pageAction = 'AUTOMAIL';
                params.automailId = autoMailObj.id;
                $state.go('layout.editAutomail', params);
            }
        };

        $scope.editGroup = function(groupObj) {
            if ($rootScope.roleAccess(roleConstant.MASTER_GENERAL_AUTO_MAIL_GROUP_MODIFY)) {
                autoMailService.set(groupObj);
                autoMailService.setPageAction("GROUP");
                var params = {};
                params.groupId = groupObj.autoMailGroupMaster.id;
                params.pageAction = 'GROUP';
                $state.go('layout.editAutomail', params);
                //      $location.path("/master/auto_mail_operation");
            }
        };


        $scope.deleteAutoMail = function(autoMailObj) {
            if ($rootScope.roleAccess(roleConstant.MASTER_GENERAL_AUTO_MAIL_DELETE)) {
                var newScope = $scope.$new();
                newScope.errorMessage = $rootScope.nls["ERR240"];
                ngDialog.openConfirm({
                    template: '<p>{{errorMessage}}</p>' +
                        '<div class="ngdialog-footer">' +
                        '<button type="button" class="btn btn-default btn-property cancel-btn" ng-click="closeThisDialog(0)">No' +
                        '<button type="button" class="btn btn-primary btn-property accent-btn" ng-click="confirm(1)">Yes' +
                        '</button>' +
                        '</div>',
                    plain: true,
                    scope: newScope,
                    className: 'ngdialog-theme-default'
                }).then(function(value) {
                    AutoMailRemove.remove({
                        id: autoMailObj.id
                    }, function(data) {
                        if (data.responseCode == "ERR0") {
                            console.log("Auto Mail deleted Successfully")
                            $scope.cancel();
                            $scope.init();
                        } else {
                            console.log("Auto Mail deleted Failed " +
                                data.responseDescription)
                        }
                    }, function(error) {
                        console.log("Auto Mail deleted Failed : " + error)
                    });
                }, function(value) {
                    console.log("Auto Mail deleted cancelled");
                });
            }
        }

        $scope.deleteGroup = function(autoMailObj) {
            if ($rootScope.roleAccess(roleConstant.MASTER_GENERAL_AUTO_MAIL_GROUP_DELETE)) {
                var newScope = $scope.$new();
                newScope.errorMessage = $rootScope.nls["ERR241"];
                ngDialog.openConfirm({
                    template: '<p>{{errorMessage}}</p>' +
                        '<div class="ngdialog-footer">' +
                        '<button type="button" class="btn btn-default btn-property cancel-btn" ng-click="closeThisDialog(0)">No' +
                        '<button type="button" class="btn btn-primary btn-property accent-btn" ng-click="confirm(1)">Yes' +
                        '</button>' +
                        '</div>',
                    plain: true,
                    scope: newScope,
                    className: 'ngdialog-theme-default'
                }).then(function(value) {

                    AutoMailGroupRemove.remove({
                        id: autoMailObj.autoMailGroupMaster.id
                    }, function(data) {
                        if (data.responseCode == "ERR0") {
                            console.log("Auto Mail Group deleted Successfully")
                            $scope.cancel();
                            $scope.init();
                        } else {
                            console.log("Auto Mail Group deleted Failed " +
                                data.responseDescription)
                        }
                    }, function(error) {
                        console.log("Auto Mail Group deleted Failed : " + error)
                    });
                }, function(value) {
                    console.log("Auto Mail Group deleted cancelled");
                });
            }
        }




        /* Custom Table related functions */

        $scope.rowSelect = function(autoMailMasterObj) {
            if ($rootScope.roleAccess(roleConstant.MASTER_GENERAL_AUTO_MAIL_VIEW)) {
                AutoMailGroupView.get({
                    id: autoMailMasterObj.transAutoMailGroupMaster.id
                }, function(data) {
                    if (data.responseCode == "ERR0") {

                        console.log("Auto Mail Group view Successfully")
                        $scope.autoMailMaster = autoMailMasterObj;
                        $scope.autoMailMaster.autoMailGroupMaster = data.responseObject;
                    } else {
                        console.log("Auto Mail Group view Failed " +
                            data.responseDescription)
                    }

                    $scope.showHistory = false;
                    $scope.groupSwitchType = false;
                }, function(error) {
                    console.log("Auto Mail Group view Failed : " + error)
                });

                /************************************************************
                 * ux - by Muthu
                 * reason - ipad compatibility fix
                 *
                 * *********************************************************/

                var windowInner = $window.innerWidth;
                if (windowInner <= 1199) {
                    $scope.deskTopView = false;

                } else {
                    $scope.deskTopView = true;
                }
                angular.element($window).bind('resize', function() {
                    if ($window.innerWidth >= 1200) {
                        $scope.deskTopView = true;
                    } else if ($window.innerWidth <= 1199) {
                        if ($scope.autoMailMaster.id != null) {
                            $scope.deskTopView = false;
                        } else {
                            $scope.deskTopView = true;
                        }
                    }

                    $scope.$digest();
                });

                /********************* ux fix ends here *****************************/
            }

        }

        $scope.changepage = function(param) {
            console.log("ChangePage..." + param.page);
            $scope.page = param.page;
            $scope.limit = param.size;
            $scope.search();
        }
        $scope.limitChange = function(item) {
            console.log("LimitChange..." + item);
            $scope.page = 0;
            $scope.limit = item;
            $scope.search();
        }

        $scope.sortChange = function(param) {
            console.log("SortChange..." + param.sortKey);
            $scope.searchDto.orderByType = param.sortOrder.toUpperCase();
            $scope.searchDto.sortByColumn = param.sortKey;
            $scope.search();
        }

        $scope.changeSearch = function(param) {
            console.log("Change Search " + param);
            for (var attrname in param) {
                $scope.searchDto[attrname] = param[attrname];
            }
            $scope.page = 0;
            $scope.cancel();
            $scope.search();
        }


        $scope.search = function() {

            console.log("searched");
            $scope.autoMailMaster = {};
            $scope.searchDto.selectedPageNumber = $scope.page;
            $scope.searchDto.recordPerPage = $scope.limit;
            $scope.autoMailArr = [];
            $scope.searchDto.selectedPageNumber = $scope.page;
            $scope.searchDto.recordPerPage = $scope.limit;
            AutoMailSearch.query($scope.searchDto).$promise.then(function(
                data, autoMailStatus) {

                $scope.totalRecord = data.responseObject.totalRecord;
                var tempArr = [];
                var resultArr = [];
                tempArr = data.responseObject.searchResult;
                var tempObj = {};
                angular.forEach(tempArr, function(item, index) {

                    tempObj = item;
                    tempObj.no = (index + 1) + ($scope.page * $scope.limit);
                    resultArr.push(tempObj);
                    tempObj = {};
                });
                $scope.autoMailData = resultArr;

                console.log($scope.autoMailData);
            });
        }


    }
]);