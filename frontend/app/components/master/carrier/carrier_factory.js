(function() {

	app.factory("CarrierSearch",['$resource', function($resource) {
		return $resource("/api/v1/carriermaster/get/search", {}, {
			query : {
				method : 'POST'
			}
		});
	}]);
	
	app.factory("CarrierSearchKeyword", ['$resource', function($resource) {
		return $resource("/api/v1/carriermaster/get/search/keyword", {}, {
			fetch : {
				method : 'POST'
			}
		});
	}]);

	app.factory("CarrierAdd",['$resource', function($resource) {
		return $resource("/api/v1/carriermaster/create", {}, {
			save : {
				method : 'POST'
			}
		});
	}]);

	app.factory("CarrierEdit",['$resource', function($resource) {
		return $resource("/api/v1/carriermaster/update", {}, {
			update : {
				method : 'POST'
			}
		});
	}]);

	app.factory("CarrierView",['$resource', function($resource) {
		return $resource("/api/v1/carriermaster/get/id/:id", {}, {
			get : {
				method : 'GET',
				params : {
					id : ''
				},
				isArray : false
			}
		});
	}]);
	
	
	app.factory("GetCarrier",['$resource', function($resource) {
		return $resource("/api/v1/carriermaster/getCarrier/:carrierNo", {}, {
			fetch : {
				method : 'GET',
				params : {
					carrierNo : ''
				},
				isArray : false
			}
		});
	}]);
	
	

	
	
	app.factory("CarrierRemove",['$resource', function($resource) {
		return $resource("/api/v1/carriermaster/delete/:id", {}, {
			remove : {
				method : 'DELETE',
				params : {
					id : ''
				},
				isArray : false
			}
		});
	}]);
	
	

	app.factory("CarrierByTransportMode",['$resource', function($resource) {
		return $resource("/api/v1/carriermaster/get/search/keyword/:transportMode", {}, {
			fetch : {
				method : 'POST',
				params : {
					transportMode : ''
				},
				isArray : false
			}
		});
	}]);
	
	

})();
