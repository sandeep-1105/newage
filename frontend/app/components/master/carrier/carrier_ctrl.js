app.controller('CarrierCtrl',['$rootScope', '$state', '$scope', '$http', '$location', '$window',
    'ngTableParams', 'CarrierSearch', 'CarrierView', 'ngDialog',
    'CarrierRemove', 'carrierService', '$stateParams', 'roleConstant', 
	function($rootScope, $state, $scope, $http, $location, $window,
    ngTableParams, CarrierSearch, CarrierView, ngDialog,
    CarrierRemove, carrierService, $stateParams, roleConstant) {
    console.log("Carrier controller loaded..");
    /************************************************************
     * ux - by Muthu
     * reason - ipad compatibility fix
     *
     * *********************************************************/
    $scope.deskTopView = true;

    /********************* ux fix ends here *****************************/
    $scope.carrierHeadArr = [{
            "name": "#",
            "width": "w50px",
            "model": "no",
            "search": false,
        },
        {
            "name": "Name",
            "width": "w200px",
            "model": "carrierName",
            "search": true,
            "wrap_cell": true,
            "type": "text",
            "sort": true,
            "key": "searchCarrierName"

        },
        {
            "name": "Code",
            "width": "w100px",
            "model": "carrierCode",
            "search": true,
            "type": "text",
            "sort": true,
            "key": "searchCarrierCode"
        },
        {
            "name": "Transport Mode",
            "width": "w100px",
            "model": "transportMode",
            "search": true,
            "type": "drop",
            "data": $rootScope.enum['TransportMode'],
            "sort": true,
            "key": "searchTransportMode"
        },
        {
            "name": "ICAO Code",
            "width": "w100px",
            "model": "iataCode",
            "search": true,
            "type": "text",
            "sort": true,
            "key": "searchIataCode"
        },
        {
            "name": "SCAC Code",
            "width": "w100px",
            "model": "scacCode",
            "search": true,
            "type": "text",
            "sort": true,
            "key": "searchScacCode"
        },
        {
            "name": "Status",
            "width": "w120px",
            "model": "status",
            "search": true,
            "type": "drop",
            "sort": true,
            "data": $rootScope.enum['LovStatus'],
            "key": "searchStatus"
        }
    ]
    $scope.sortSelection = {
        sortKey: "carrierName",
        sortOrder: "asc"
    }



    $scope.carrierMaster = {};
    $scope.selectedMaster = {};
    $scope.searchDto = {};
    carrierService.set({});
    $scope.limitArr = [10, 15, 20];
    $scope.page = 0;
    $scope.limit = 10;

    $scope.rowSelect = function(data) {
        if ($rootScope.roleAccess(roleConstant.MASTER_GENERAL_CARRIER_VIEW)) {
            $scope.showLogs = false;
            $scope.carrierMaster = data;

            /************************************************************
             * ux - by Muthu
             * reason - ipad compatibility fix
             *
             * *********************************************************/
            var windowInner = $window.innerWidth;
            if (windowInner <= 1199) {
                $scope.deskTopView = false;

            } else {
                $scope.deskTopView = true;
            }
            angular.element($window).bind('resize', function() {
                if ($window.innerWidth >= 1200) {
                    $scope.deskTopView = true;
                } else if ($window.innerWidth <= 1199) {
                    if ($scope.carrierMaster.id != null) {
                        $scope.deskTopView = false;
                    } else {
                        $scope.deskTopView = true;
                    }
                }

                console.log("window resizing..." + $window.innerWidth);
                $scope.$digest();
            });

            /********************* ux fix ends here *****************************/
        }
    }


    $scope.init = function() {
        console.log("Carrier Init method called.");
        $rootScope.setNavigate1("Master");
        $rootScope.setNavigate2("Carrier");
        carrierService.set({});
        $scope.searchDto = {};
        $scope.carrierMaster = {};
        $scope.search();
        $scope.searchDto.orderByType = "ASC";

    };

    $scope.cancel = function() {

        $scope.carrierMaster = {};
        /************************************************************
         * ux - by Muthu
         * reason - ipad compatibility fix
         *
         * *********************************************************/
        $scope.deskTopView = true;

        /********************* ux fix ends here *****************************/
    };

    $scope.add = function() {
        if ($rootScope.roleAccess(roleConstant.MASTER_GENERAL_CARRIER_CREATE)) {
            carrierService.set({});
            $state.go("layout.addCarrier");
        }
    };

    $scope.edit = function() {
        if ($rootScope.roleAccess(roleConstant.MASTER_GENERAL_CARRIER_MODIFY)) {
            $state.go("layout.editCarrier", {
                carrierId: $scope.carrierMaster.id
            });
        }
    };

    $scope.changepage = function(param) {

        $scope.page = param.page;
        $scope.limit = param.size;
        $scope.cancel();
        $scope.search();
    }

    $scope.currentTab = "address";
    $scope.activeTab = function(tab) {
        $scope.currentTab = tab;
    }

    $scope.view = function(selectedMaster) {
        console.log("View Carrier Called");

        $scope.carrierMaster = selectedMaster;

    };

    $scope.limitChange = function(item) {
        $scope.page = 0;
        $scope.limit = item;
        $scope.cancel();
        $scope.search();
    }

    $scope.sortChange = function(param) {
        $scope.searchDto.orderByType = param.sortOrder.toUpperCase();
        $scope.searchDto.sortByColumn = param.sortKey;

        $scope.cancel();
        $scope.search();
    }



    $scope.deleteCarrier = function() {
        if ($rootScope.roleAccess(roleConstant.MASTER_GENERAL_CARRIER_DELETE)) {
            var newScope = $scope.$new();
            newScope.errorMessage = $rootScope.nls["ERR228"];
            ngDialog.openConfirm({
                template: '<p>{{errorMessage}}</p>' +
                    '<div class="ngdialog-footer">' +
                    '<button type="button" class="btn btn-default btn-property cancel-btn" ng-click="closeThisDialog(0)">No' +
                    '<button type="button" class="btn btn-primary btn-property accent-btn" ng-click="confirm(1)">Yes' +
                    '</button>' +
                    '</div>',
                plain: true,
                scope: newScope,
                className: 'ngdialog-theme-default'
            }).
            then(function(value) {
                CarrierRemove.remove({
                    id: $scope.carrierMaster.id
                }, function(data) {
                    if (data.responseCode == 'ERR0') {
                        console.log("Carrier deleted Successfully");
                        $scope.cancel();
                        $scope.init();
                    } else {
                        console.log("Carrier deleted Failed " +
                            data.responseDescription)
                    }
                }, function(error) {
                    console.log("Carrier deleted Failed : " + error)
                });
            }, function(value) {
                console.log("deleted cancelled");

            });
        }
    }



    $scope.changeSearch = function(param) {
        for (var attrname in param) {
            $scope.searchDto[attrname] = param[attrname];
        }
        $scope.page = 0;
        $scope.cancel();
        $scope.search();

    }




    $scope.search = function() {
        $scope.carrierMaster = {};

        $scope.searchDto.selectedPageNumber = $scope.page;
        $scope.searchDto.recordPerPage = $scope.limit;
        console.log($scope.searchDto);
        $scope.carrierArr = [];
        $scope.searchDto.selectedPageNumber = $scope.page;
        $scope.searchDto.recordPerPage = $scope.limit;
        CarrierSearch.query($scope.searchDto).$promise.then(function(
            data, status) {
            $scope.totalRecord = data.responseObject.totalRecord;
            var tempArr = [];
            var resultArr = [];
            tempArr = data.responseObject.searchResult;
            var tempObj = {};
            angular.forEach(tempArr, function(item, index) {
                tempObj = item;
                tempObj.no = (index + 1) + ($scope.page * $scope.limit);
                resultArr.push(tempObj);
                tempObj = {};
            });
            $scope.carrierArr = resultArr;
        });

    }


    $scope.clickOnTabView = function(tab) {
        if (tab == 'Address' && $rootScope.roleAccess(roleConstant.MASTER_GENERAL_CARRIER_ADDRESS_VIEW)) {
            $scope.detailTab = tab;
        }
        if (tab == 'Edi' && $rootScope.roleAccess(roleConstant.MASTER_GENERAL_CARRIER_EDI_VIEW)) {
            $scope.detailTab = tab;
        }
    }


    switch ($stateParams.action) {
        case "SEARCH":
            console.log("carrier Master Search Page....");
            $scope.init();
            $rootScope.breadcrumbArr = [{
                    label: "Master",
                    state: "layout.carrier"
                },
                {
                    label: "Carrier",
                    state: "layout.carrier"
                }
            ];
            break;
    }
}]);