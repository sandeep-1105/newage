app.factory('carrierService',[ function() {
	var savedData = {}
	function set(data) {
		savedData = data;
		localStorage.savedData=JSON.stringify(savedData);
		
	}
	function get() {
		savedData = localStorage.savedData==null?null:JSON.parse(localStorage.savedData);
		return savedData;
	}

	return {
		set : set,
		get : get
	}

}]);


app.service('carrierMasterService',['$rootScope','CarrierByTransportMode' ,
	function($rootScope,CarrierByTransportMode) {
	
	this.getCarrierList = function(object,mode) {
	        var searchDto = {};
	        searchDto.keyword = object == null ? "" : object;
	        searchDto.selectedPageNumber = 0;
	        searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

	        return CarrierByTransportMode.fetch({
	            "transportMode": mode
	        }, searchDto).$promise.then(
	            function(data) {
	                if (data.responseCode == "ERR0") {
	                    var carrierMasterList =[]
	                    carrierMasterList = data.responseObject.searchResult;
	                    return carrierMasterList;
	                }
	            },
	            function(errResponse) {
	                console.error('Error while fetching Carrier List Based on Air');
	            }
	        );

	    }
}]);


