app.factory('reasonMasterFactory', function($resource) {
	return {
		findById : $resource('/api/v1/reasonmaster/get/id/:id', {}, {
			 query : {method : 'GET', params : {id : ''}, isArray : false}
		})
		,
		search : $resource('/api/v1/reasonmaster/get/search', {}, {
			fetch : {method : 'POST'}
		})
		,
		update : $resource('/api/v1/reasonmaster/update', {}, {
			query : {method : 'POST'}
		})
		,
		delete : $resource('/api/v1/reasonmaster/delete/:id', {}, {
			query : {method : 'DELETE', params : {id : ''}}
		})
		,
		create : $resource('/api/v1/reasonmaster/create', {}, {
			query : {method : 'POST'}
		})
	};
})