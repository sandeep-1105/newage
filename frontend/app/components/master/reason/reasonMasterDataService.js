app.service('reasonMasterDataService', function($rootScope) {
    this.getHeadArray = function() {
	   return [
			  {
					"name":"#",
					"width":"col-xs-0half",
					"model":"no",
					"search":false
			  },{
					"name":"Name",
					"width":"col-xs-4half",
					"model":"reasonName",
					"search":true,
					"wrap_cell":true,
					"type":"text",
					"sort":true,
					"key":"searchReasonName"
				},{
					"name":"Code",
					"width":"col-xs-2",
					"model":"reasonCode",
					"search":true,
					"type":"text",
					"sort":true,
					"key":"searchReasonCode"
				},{
					"name":"Status",
					"width":"col-xs-3",
					"model":"status",
					"search":true,
					"type":"drop",
					"data":$rootScope.enum['LovStatus'],
					"key":"searchStatus",
					"sort":true
				}];
          };

    
    this.getListBreadCrumb = function() {
        return [{
                label: "Master",
                state: "layout.reasonMaster"
            },
            {
                label: "Reason",
                state: "layout.reasonMaster"
            }
        ];
    }

    this.getAddBreadCrumb = function() {
        return [{
                label: "Master",
                state: "layout.reasonMaster"
            },
            {
                label: "Reason",
                state: "layout.reasonMaster"
            },
            {
                label: "Add Reason",
                state: null
            }
        ];
    };
    this.getEditBreadCrumb = function() {
        return [{
                label: "Master",
                state: "layout.reasonMaster"
            },
            {
                label: "Reason",
                state: "layout.reasonMaster"
            },
            {
                label: "Edit Reason",
                state: null
            }
        ];
    };
   

});