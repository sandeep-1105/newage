(function(){
app.controller("ReasonMasterCtrl", ['$scope', '$rootScope', '$window', '$modal', '$state', '$stateParams', '$log', 'ngDialog', 'reasonMasterDataService', 'reasonMasterFactory', 'RecentHistorySaveService', 'reasonMasterValidationService', 'Notification','roleConstant',
    function($scope, $rootScope, $window, $modal, $state, $stateParams, $log, ngDialog, reasonMasterDataService, reasonMasterFactory, RecentHistorySaveService, reasonMasterValidationService, Notification, roleConstant ) {
	
	
	  var vm = this;
      vm.tableHeadArr = reasonMasterDataService.getHeadArray();
      vm.dataArr = [];
      vm.searchData = {};
      vm.reasonMaster = {};
      vm.limitArr = [10, 15, 20];
      vm.page = 0;
      vm.limit = 10;
      vm.totalRecord = 100;
      vm.showHistory = false;
      vm.errorMap = new Map();
      vm.deskTopView = true;

      vm.sortSelection = {
          sortKey: "reasonName",
          sortOrder: "asc"
      }
      
      vm.changeSearch = function(param) {
          $log.debug("Change Search is called." + param);
          for (var attrname in param) {
              vm.searchData[attrname] = param[attrname];
          }
          vm.page = 0;
          vm.search();
      }

      vm.sortChange = function(param) {
          $log.debug("sortChange is called. " + param);
          vm.searchData.orderByType = param.sortOrder.toUpperCase();
          vm.searchData.sortByColumn = param.sortKey;
          vm.search();
      }
      
      
      /**
       * Reason Master List Page Search Data  initializing and calling search Mtd.
       */
      vm.searchInit = function() {
          vm.searchData = {};
          vm.searchData.orderByType = vm.sortSelection.sortOrder.toUpperCase();
          vm.searchData.sortByColumn = vm.sortSelection.sortKey;
          vm.search();
      };
      

      /**
       * Get Reason Master's Based on Search Data.
       */
      vm.search = function() {
      	vm.reasonMasteView=false;
          vm.searchData.selectedPageNumber = vm.page;
          vm.searchData.recordPerPage = vm.limit;
          vm.dataArr = [];
          reasonMasterFactory.search.fetch(vm.searchData).$promise.then(
              function(data) {
                  if (data.responseCode == "ERR0") {
                      vm.totalRecord = data.responseObject.totalRecord;
                      vm.dataArr = data.responseObject.searchResult;
                  }
              },
              function(errResponse) {
              	 $log.error('Error while fetching ReasonMaster');
              });
         }
      
      
      /**
       * Reason Master List Page Search Data  initializing and calling search Mtd.
       * @param {Object} data - The Selected Reason Master from List page.
       * @param {string} data.id - The id of the Selected Reason Master.
       * @param {int} index - The Selected row index from List page.
       */
      vm.rowSelect = function(data) {
    	  if($rootScope.roleAccess(roleConstant.MASTER_FINANCE_REASON_VIEW)){
    		  vm.reasonMasteView=true;
  			vm.reasonMaster = data;
  			var windowInner=$window.innerWidth;
  			if(windowInner<=1199){
  				vm.deskTopView = false;
  			}
  			else{
  				vm.deskTopView = true;
  			}
  			angular.element($window).bind('resize', function(){
  				if($window.innerWidth>=1200){
  					vm.deskTopView = true;
  				}
  				else if($window.innerWidth<=1199){
  					if(vm.reasonMaster.id!=null){
  						vm.deskTopView = false;
  					}
  					else {
  						vm.deskTopView = true;
  					}
  				}
  				$scope.$digest();
  			});  
    	  }
		}
      
      
      /**
       * Modify Existing Reason Master.
       * @param {int} id - The id of a Country.
       */
      vm.edit = function(id) {
    	  if($rootScope.roleAccess(roleConstant.MASTER_FINANCE_REASON_CREATE)){
    		  $state.go("layout.reasonMasterEdit", {
                  id: id
              }); 
    	  }
      }
      
      // set status of reason master
      
      vm.setStatus = function(data){
  		if(data=='Active'){
  			return 'activetype';
  		} else if(data=='Block'){
  			return 'blockedtype';
  		}else if(data=='Hide'){
  			return 'hiddentype';
  		}
  	}
      
      
      /**
       * Go To Add Reason Master.
       */
      vm.addNew = function() {
    	  if($rootScope.roleAccess(roleConstant.MASTER_FINANCE_REASON_CREATE)){
    		  $state.go("layout.reasonMasterAdd");
    	  }
      }
      
      
      /**
       * Create New Reason Master.
       */
      vm.create = function() {
      	var validationResponse = reasonMasterValidationService.validate(vm.reasonMaster,0);
          if(validationResponse.error == true) {
          	$log.debug("Validation Response -- ", validationResponse);
          	$log.debug('Validation Failed to create ReasonMaster..')
				$scope.validationErrorAndFocus(validationResponse, validationResponse.errElement);
			}
          else {
          	reasonMasterFactory.create.query(vm.reasonMaster).$promise.then(function(data) {
		            if (data.responseCode == 'ERR0') {
		            	$log.debug("Reason Saved Successfully Response :: ", data);
		            	Notification.success($rootScope.nls["ERR400"]);
		            	$state.go('layout.reasonMaster');
		            } else {
		                $log.debug("Create Reason Failed :" + data.responseDescription)
		            }

		        }, function(error) {
		        	 $log.debug("Create Reason Failed : " + error)
		        });
          } 
      };
      
      
      //Update Existing Reason Master
      vm.update = function() {
      	 var validationResponse = reasonMasterValidationService.validate(vm.reasonMaster,0);
           if(validationResponse.error == true) {
           	$log.debug("Validation Response -- ", validationResponse);
           	$log.debug('Validation Failed while Updating the Reason..')
				$scope.validationErrorAndFocus(validationResponse, validationResponse.errElement);
			}
           else {
           	reasonMasterFactory.update.query(vm.reasonMaster).$promise.then(function(data) {
		            if (data.responseCode == 'ERR0') {
		            	$log.debug("Reason Updated Successfully Response :: ", data);
		            	Notification.success($rootScope.nls["ERR401"]);
		            	$state.go('layout.reasonMaster');
		            } else {
		                $log.debug("Updating Reason Failed :" + data.responseDescription)
		            }
		        }, function(error) {
		        	 $log.debug("Updating Reason Failed : " + error)
		        });
           } 
      };
      
      
      /**
       * Go Back to reason Master List Page.
       */
      vm.backToList = function() {
      	vm.reasonMasteView = false;
      	vm.reasonMaster = {};
      	vm.deskTopView = true;
      }
      
      
      vm.changepage = function(param) {
          $log.debug("change Page..." + param.page);
          vm.page = param.page;
          vm.limit = param.size;
          vm.search();
      }

      /**
       * Country Master List Page Limit Data change.
       * @param {int} nLimit - The Selected limit option from Limit array.
       */
      vm.limitChange = function(nLimit) {
          $log.debug("Limit Change..." + nLimit);
          vm.page = 0;
          vm.limit = nLimit;
          vm.search();
      }
      
      
      /**
       * Cancel Add or Edit Reason Master.
       * @param {int} id - The id of a Reason
       */
     
      vm.cancel = function(objId) {
          if(vm.reasonMasterForm!=undefined){
		    	vm.goTocancel(vm.reasonMasterForm.$dirty,objId);
          }else{
          	vm.goTocancel(false,objId);
          }
      }
     
     vm.goTocancel = function(isDirty,objId){
   		 if (isDirty) {
   		      var newScope = $scope.$new();
   		      newScope.errorMessage = $rootScope.nls["ERR200"];
   		      ngDialog.openConfirm({
   		        template: '<p>{{errorMessage}}</p>' +
   		          '<div class="ngdialog-footer">' +
   		          ' <button type="button" class="btn btn-default btn-property cancel-btn" ng-click="confirm(2)">No</button>' +
   		          '<button type="button" class="btn btn-default btn-property accent-btn " ng-click="confirm(1)">Yes</button>' +
   		          '<button type="button" class="btn btn-default btn-property pull-left grey-cancel-btn" ng-click="confirm(3)">Cancel</button>' +
   		          '</div>',
   		        plain: true,
   		        closeByDocument: false,
   		        scope: newScope,
   		        className: 'ngdialog-theme-default'
   		      }).then(
   		        function(value) {
   		          if (value == 1) {
   		            if(objId!=null && objId != undefined){
   		            	vm.update();
   		            }else{
   		            	vm.create();
   		            }
   		          } else if (value == 2) {
   		        	 $state.go("layout.reasonMaster", {
   		                submitAction: "Cancelled"
   		            });
   		          } else {
   		            $log.debug("cancelled");
   		          }
   		        });
   		    } else {
   		     $state.go("layout.reasonMaster", {
                 submitAction: "Cancelled"
             });
   		    }
   	    }

      
      
      /**
       * Get Reason Master By id.
       * @param {int} id - The id of a Reason.
       * @param {boolean} isView - TO identify purpose of calling is for View or Edit a Reason.
       */
      vm.view = function(id, isView) {
      	reasonMasterFactory.findById.query({
              id: id
          }).$promise.then(function(data) {
              if (data.responseCode == 'ERR0') {
                  vm.reasonMaster = data.responseObject;
                  var rHistoryObj = {}

                  if (vm.reasonMaster != undefined && vm.reasonMaster != null && vm.reasonMaster.id != undefined && vm.reasonMaster.id != null) {
                      $rootScope.subTitle = vm.reasonMaster.reasonName;
                  } else {
                      $rootScope.subTitle = "Unknown Reason"
                  }
                  $rootScope.unfinishedFormTitle = "Reason  # " + vm.reasonMaster.reasonCode;
                  if (isView) {
                      rHistoryObj = {
                          'title': "Reason #" + vm.reasonMaster.reasonCode,
                          'subTitle': $rootScope.subTitle,
                          'stateName': $state.current.name,
                          'stateParam': JSON.stringify($stateParams),
                          'stateCategory': 'Reason Master',
                          'serviceType': undefined,
                          'showService': false
                      }

                  } else {

                      rHistoryObj = {
                          'title': 'Edit Reason # ' + $stateParams.serviceId,
                          'subTitle': $rootScope.subTitle,
                          'stateName': $state.current.name,
                          'stateParam': JSON.stringify($stateParams),
                          'stateCategory': 'Reason Master',
                          'serviceType': 'MASTER',
                          'serviceCode': ' ',
                          'orginAndDestination': ' '
                      }
                  }
                  RecentHistorySaveService.form(rHistoryObj);
              } else {
                  $log.debug("Exception while getting Reason by Id ", data.responseDescription);
              }
          }, function(error) {
              $log.debug("Error ", error);
          });
      };
      
      /**
       * Delete Reason Master.
       * @param {int} id - The id of a Reason.
       */
      vm.delete = function(id) {
    	  if($rootScope.roleAccess(roleConstant.MASTER_FINANCE_REASON_DELETE)){
    			var newScope = $scope.$new();
    			newScope.errorMessage = $rootScope.nls["ERR201014"];
    	    	ngDialog.openConfirm( {
    	                    template : '<p>{{errorMessage}}</p>'
    	                    + '<div class="ngdialog-footer">'
    	                    + '<button type="button" class="btn btn-default btn-property cancel-btn" ng-click="closeThisDialog(0)">Cancel'
    	                    + '<button type="button" class="btn btn-primary btn-property accent-btn" ng-click="confirm(1)">Yes'
    	                    + '</button></div>',
    	                    plain : true,
    	                    scope : newScope,
    	                    closeByDocument: false,
    	                    className : 'ngdialog-theme-default'
    	                }).then( function(value) {
    	                	reasonMasterFactory.delete.query({
    	                        id: id
    	                    }).$promise.then(function(data) {
    	                        if (data.responseCode == 'ERR0') {
    	                        	vm.reasonMasteView=false;
    	                        	Notification.success($rootScope.nls["ERR402"]);
    	                        	vm.deskTopView = true;
    	                            vm.search();
    	                        }
    	                    }, function(error) {
    	                        $log.debug("Error ", error);
    	                    });
    	                }, function(value) {
    	                    $log.debug("delete Cancelled");
    	            });  
    	  }
      }
      

      
    /**
     * Common Validation Focus Functionality - starts here" 
     */
		$scope.validationErrorAndFocus = function(valResponse, elemId){
			$scope.errorMap = new Map();
			if(valResponse.error == true){
				$scope.errorMap.put(valResponse.errElement, valResponse.errMessage);
			}
				$scope.navigateToNextField(elemId);
		}
      
      
      /**
       * State Identification Handling Here.
       * @param {String} $stateParams.action - VIEW, ADD, EDIT, SEARCH.
       */
      //
      switch ($stateParams.action) {
          case "ADD":
          	 if ($stateParams.fromHistory === 'Yes') {
                   if (vm.isReloaded == "YES") {
                    vm.isReloaded = "NO";
                    localStorage.isReasonMasterReloaded = "NO";
                     vm.reasonMaster = JSON.parse(localStorage.reloadFormData);
                   } else {
                     vm.reasonMaster = $rootScope.selectedUnfilledFormData;
                   }
                 } else {
                   if (vm.isReloaded == "YES") {
                     vm.isReloaded = "NO";
                     localStorage.isReasonMasterReloaded = "NO";
                     vm.reasonMaster = JSON.parse(localStorage.reloadFormData);
                   } else {
                       $log.debug("Add NotHistory NotReload...")
                   }
                 }
              $rootScope.breadcrumbArr = reasonMasterDataService.getAddBreadCrumb();
              break;
          case "EDIT":
          	 if (vm.fromHistory === 'Yes') {
                   if (vm.isReloaded == "YES") {
                     vm.isReloaded = "NO";
                     localStorage.isReasonMasterReloaded = "NO";
                     vm.reasonMaster = JSON.parse(localStorage.reloadFormData);
                   } else {
                     vm.reasonMaster = $rootScope.selectedUnfilledFormData;
                   }
                 } else {
                   if (vm.isReloaded == "YES") {
                     vm.isReloaded = "NO";
                     localStorage.isReasonMasterReloaded = "NO";
                     vm.reasonMaster = JSON.parse(localStorage.reloadFormData);
                   } else {
                  	 vm.view($stateParams.id, false);
                   }
                 }
              $rootScope.breadcrumbArr = reasonMasterDataService.getEditBreadCrumb();
              break;
          default:
              $rootScope.breadcrumbArr = reasonMasterDataService.getListBreadCrumb();
              vm.searchInit();
      }
      
 
}
]);
}) ();