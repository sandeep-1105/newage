app.service('reasonMasterValidationService', ['$rootScope','ValidateUtil', 'CommonValidationService', 'appConstant', function($rootScope, ValidateUtil, CommonValidationService, appConstant) {
	this.validate = function(reason, code) {
		//Validate reason name
		if(code == 0 || code == 1) {
			if(reason.reasonName == undefined || reason.reasonName == null || reason.reasonName =="" ){
				return this.validationResponse(true, "reasonName", $rootScope.nls["ERR06700"], reason);
			}
		}
		//Validate reason code
		if(code == 0 || code == 2) {
			if(reason.reasonCode == undefined || reason.reasonCode == null || reason.reasonCode =="" ){
				return this.validationResponse(true, "reasonCode", $rootScope.nls["ERR06701"], reason);
			}
		}
		
		
		
		return this.validationSuccesResponse(reason);
	}
 
	this.validationSuccesResponse = function(reason) {
	    return {error : false, obj : reason};
	}
	
	this.validationResponse = function(err, elem, message, reason) {
		return {error : err, errElement : elem, errMessage : message, obj : reason};
	}
	
}]);