
(function() {

	app.factory("CityList",['$resource', function($resource) {
		return $resource("/api/v1/citymaster/get/search/keyword/:countryId", {}, {
			fetch : {
				method : 'POST',
				params : {
					countryId : ''
				},
				isArray : false
			}
		});
	}]);


	app.factory("CitySearch",['$resource',  function($resource) {
		return $resource("/api/v1/citymaster/get/search/keyword", {}, {
			query : {
				method : 'POST'
			}
		});
	}]);
	
	app.factory("CitySaveOrUpdate",['$resource',  function($resource) {
		return $resource("/api/v1/citymaster/create", {}, {
			save : {
				method : 'POST'
			}
		});
	}]);
	
	
	app.factory("CityView",['$resource',  function($resource) {
		return $resource("/api/v1/citymaster/get/id/:id", {}, {
			get : {
				method : 'GET',
				params : {
					id : ''
				},
				isArray : false
			}
		});
	}]);
	
	
	app.factory("CityRemove",['$resource',  function($resource) {
		return $resource("/api/v1/citymaster/delete/:id", {}, {
			remove : {
				method : 'DELETE',
				params : {
					id : ''
				},
				isArray : false
			}
		});
	}]);
	

})();