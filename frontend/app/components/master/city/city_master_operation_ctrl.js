app.controller('cityMasterOperationCtrl',['$rootScope', '$scope', 'ngProgressFactory', 'ngDialog', '$state', '$stateParams',
    'CityView', 'CitySaveOrUpdate', 'CountryList', 'StateList', 'Notification', 
	function($rootScope, $scope, ngProgressFactory, ngDialog, $state, $stateParams,
    CityView, CitySaveOrUpdate, CountryList, StateList, Notification) {

    $scope.contained_progressbar = ngProgressFactory.createInstance();
    $scope.contained_progressbar.setParent(document.getElementById('city-panel'));
    $scope.contained_progressbar.setAbsolute();


    $scope.init = function() {

        if ($scope.cityMaster == undefined || $scope.cityMaster == null) {
            $scope.cityMaster = {};
            $scope.cityMaster.status = 'Active';
        }

        // Setting to compare on cancel
        $scope.oldData = JSON.stringify($scope.cityMaster);
    };

    $scope.cancel = function() {

        if ($scope.oldData !== JSON.stringify($scope.cityMaster)) {
            var newScope = $scope.$new();
            newScope.errorMessage = $rootScope.nls["ERR200"];
            ngDialog.openConfirm({
                template: '<p>{{errorMessage}}</p>' +
                    '<div class="ngdialog-footer">' +
                    ' <button type="button" class="btn btn-default btn-property cancel-btn" ng-click="confirm(2)">No</button>' +
                    '<button type="button" class="btn btn-default btn-property accent-btn " ng-click="confirm(1)">Yes</button>' +
                    '<button type="button" class="btn btn-default btn-property pull-left grey-cancel-btn" ng-click="confirm(3)">Cancel</button>' +
                    '</div>',
                plain: true,
                scope: newScope,
                className: 'ngdialog-theme-default'
            }).then(function(value) {
                if (value == 1) {
                    $scope.saveOrUpdate();
                } else if (value == 2) {
                    $state.go("layout.city", {
                        submitAction: 'Cancelled'
                    });
                } else {
                    $log.debug("cancelled");
                }
            });
        } else {
            $state.go("layout.city", {
                submitAction: 'Cancelled'
            });
        }
    };


    $scope.saveOrUpdate = function() {


        if ($scope.validateCityMaster(0)) {

            $scope.contained_progressbar.start();

            if ($scope.cityMaster.id == null) {
                var successMessage = $rootScope.nls["ERR400"];
            } else {
                var successMessage = $rootScope.nls["ERR401"];
            }




            CitySaveOrUpdate.save($scope.cityMaster).$promise.then(function(data) {

                if (data.responseCode == "ERR0") {
                    console.log("City updated Successfully")
                    Notification.success(successMessage);
                    $state.go("layout.city", {
                        submitAction: 'Saved'
                    });
                } else {
                    console.log("City updated Failed ", data.responseDescription);
                }

                $scope.contained_progressbar.complete();
                angular.element(".panel-body").animate({
                    scrollTop: 0
                }, "slow");

            }, function(error) {
                console.log("City updated Failed : ", error);
            });
        } else {
            console.log("Validation Failed");
        }

    }




    $scope.ajaxCountryEvent = function(object) {

        console.log("Country Ajax Event.... ", object);

        $scope.searchDto = {};
        $scope.searchDto.keyword = object == null ? "" : object;
        $scope.searchDto.selectedPageNumber = 0;
        $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

        return CountryList.fetch($scope.searchDto).$promise.then(function(data) {
            if (data.responseCode == "ERR0") {
                $scope.countryList = data.responseObject.searchResult;
                return $scope.countryList;
            }
        }, function(errResponse) {
            console.error('Error while fetching Countries');
        });
    }

    $scope.selectedCountry = function(nextIdValue) {
        if ($scope.validateCityMaster(3)) {
            $rootScope.navigateToNextField(nextIdValue);
        }
    };

    $scope.countryRender = function(item) {
        return {
            label: item.countryName,
            item: item
        }
    }



    $scope.ajaxStateEvent = function(object) {

        $scope.searchDto = {};
        $scope.searchDto.keyword = object == null ? "" : object;
        $scope.searchDto.selectedPageNumber = 0;
        $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

        return StateList.fetch({
            "countryId": -1
        }, $scope.searchDto).$promise.then(function(data) {

            if (data.responseCode == "ERR0") {
                $scope.stateListArr = data.responseObject.searchResult;
                return $scope.stateListArr;
            } else {
                console.error('Error while fetching State List', data.responseDescription);
            }

        }, function(errResponse) {
            console.error('Error while fetching State List', errResponse);
        });
    }

    $scope.selectedState = function(nextFieldId) {
        if ($scope.validateCityMaster(4)) {
            $rootScope.navigateToNextField(nextFieldId);
        }

        $scope.cityMaster.countryMaster = $scope.cityMaster.stateMaster.countryMaster;
    };

    $scope.stateRender = function(item) {
        return {
            label: item.stateName,
            item: item
        }
    }




    $scope.validateCityMaster = function(validateCode) {

        console.log("Validate Called " + validateCode);

        $scope.errorMap = new Map();

        if (validateCode == 0 || validateCode == 1) {

            if ($scope.cityMaster.cityName == undefined ||
                $scope.cityMaster.cityName == null ||
                $scope.cityMaster.cityName == "") {
                $scope.errorMap.put("cityName", $rootScope.nls["ERR1807"]);
                return false;
            }

        }


        if (validateCode == 0 || validateCode == 2) {

            if ($scope.cityMaster.cityCode == undefined ||
                $scope.cityMaster.cityCode == null ||
                $scope.cityMaster.cityCode == "") {
                $scope.errorMap.put("cityCode", $rootScope.nls["ERR1805"]);
                return false;
            }

        }

        if (validateCode == 0 || validateCode == 3) {

            if ($scope.cityMaster.countryMaster == undefined ||
                $scope.cityMaster.countryMaster == null ||
                $scope.cityMaster.countryMaster.id == null) {
                $scope.errorMap.put("countryMaster", $rootScope.nls["ERR1811"]);
                return false;
            } else {

                if ($scope.cityMaster.countryMaster.status == 'Block') {
                    $scope.errorMap.put("countryMaster", $rootScope.nls["ERR1812"]);
                    return false;
                }


                if ($scope.cityMaster.countryMaster.status == 'Hide') {
                    $scope.errorMap.put("countryMaster", $rootScope.nls["ERR1812"]);
                    return false;
                }

            }

        }



        if (validateCode == 0 || validateCode == 4) {

            if ($scope.cityMaster.stateMaster == undefined ||
                $scope.cityMaster.stateMaster == null ||
                $scope.cityMaster.stateMaster.id == null) {
                $scope.errorMap.put("stateMaster", $rootScope.nls["ERR1816"]);
                return false;
            } else {

                if ($scope.cityMaster.stateMaster.status == 'Block') {
                    $scope.errorMap.put("stateMaster", $rootScope.nls["ERR1817"]);
                    return false;
                }


                if ($scope.cityMaster.stateMaster.status == 'Hide') {
                    $scope.errorMap.put("stateMaster", $rootScope.nls["ERR1818"]);
                    return false;
                }

            }

        }

        return true;

    }




    $scope.setStatus = function(data) {
        if (data == 'Active') {
            return 'activetype';
        } else if (data == 'Block') {
            return 'blockedtype';
        } else if (data == 'Hide') {
            return 'hiddentype';
        }
    }




    $scope.getCityById = function(cityId) {
        console.log("getCityById ", cityId);

        CityView.get({
            id: cityId
        }, function(data) {
            if (data.responseCode == "ERR0") {
                console.log("Successful while getting TOS.", data)
                $scope.cityMaster = data.responseObject;
            }
            $scope.init();
        }, function(error) {
            console.log("Error while getting City.", error)
        });
    }




    $scope.$on('addCityEvent', function(events, args) {

        $rootScope.unfinishedData = $scope.cityMaster;
        $rootScope.category = "City Master";
        $rootScope.unfinishedFormTitle = "City (New)";
        if ($scope.cityMaster != undefined && $scope.cityMaster != null && $scope.cityMaster.cityName != undefined &&
            $scope.cityMaster.cityName != null && $scope.cityMaster.cityName != "") {
            $rootScope.subTitle = $scope.cityMaster.cityName;
        } else {
            $rootScope.subTitle = "Unknown City"
        }
    })

    $scope.$on('editCityEvent', function(events, args) {

        $rootScope.unfinishedData = $scope.cityMaster;
        $rootScope.category = "City Master";
        $rootScope.unfinishedFormTitle = "City Edit (" + $scope.cityMaster.cityCode + ")";
        if ($scope.cityMaster != undefined && $scope.cityMaster != null && $scope.cityMaster.cityName != undefined &&
            $scope.cityMaster.cityName != null && $scope.cityMaster.cityName != "") {
            $rootScope.subTitle = $scope.cityMaster.cityName;
        } else {
            $rootScope.subTitle = "Unknown City"
        }
    })


    $scope.$on('addCityEventReload', function(e, confirmation) {
        confirmation.message = "";
        localStorage.reloadFormData = JSON.stringify($scope.cityMaster);
        localStorage.isCityReloaded = "YES";
        e.preventDefault();
    });


    $scope.$on('editCityEventReload', function(e, confirmation) {
        confirmation.message = "";
        localStorage.reloadFormData = JSON.stringify($scope.cityMaster);
        localStorage.isCityReloaded = "YES";
        e.preventDefault();
    });


    $scope.isReloaded = localStorage.isCityReloaded;

    if ($stateParams.action == "ADD") {
        if ($stateParams.fromHistory == 'Yes') {
            if ($scope.isReloaded == "YES") {
                console.log("Add History Reload...")
                $scope.isReloaded = "NO";
                localStorage.isCityReloaded = "NO";
                $scope.cityMaster = JSON.parse(localStorage.reloadFormData);
            } else {
                console.log("Add History Not Reload...")
                $scope.cityMaster = $rootScope.selectedUnfilledFormData;
            }
        } else {
            if ($scope.isReloaded == "YES") {
                console.log("Add Not History Reload...")
                $scope.isReloaded = "NO";
                localStorage.isCityReloaded = "NO";
                $scope.cityMaster = JSON.parse(localStorage.reloadFormData);
            } else {
                console.log("Add Not History Not Reload...")
                $scope.init();
            }
        }
        $rootScope.breadcrumbArr = [{
                label: "Master",
                state: "layout.city"
            },
            {
                label: "City ",
                state: "layout.city"
            },
            {
                label: "Add City",
                state: null
            }
        ];


        $rootScope.navigateToNextField("cityName");

    } else {
        if ($stateParams.fromHistory === 'Yes') {
            if ($scope.isReloaded == "YES") {
                $scope.isReloaded = "NO";
                localStorage.isCityReloaded = "NO";
                $scope.cityMaster = JSON.parse(localStorage.reloadFormData);
            } else {
                $scope.cityMaster = $rootScope.selectedUnfilledFormData;
            }
        } else {
            if ($scope.isReloaded == "YES") {
                $scope.isReloaded = "NO";
                localStorage.isCityReloaded = "NO";
                $scope.cityMaster = JSON.parse(localStorage.reloadFormData);
            } else {
                $scope.getCityById($stateParams.cityId);
            }
        }
        $rootScope.breadcrumbArr = [{
                label: "Master",
                state: "layout.city"
            },
            {
                label: "City ",
                state: "layout.city"
            },
            {
                label: "Edit City",
                state: null
            }
        ];

        $rootScope.navigateToNextField("cityName");
    }


}]);