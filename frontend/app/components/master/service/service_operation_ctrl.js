app.controller('ServiceMasterOperationCtrl', ['$rootScope', '$scope', '$http', '$location',
    'ServiceMasterAdd', 'ServiceMasterEdit',
    'ServiceMasterService', 'discardService',
    'ngDialog', 'ngProgressFactory', 'appConstant', 'CommonValidationService',
    'CostCenterSearchKeyword', 'ServiceTypeKeyword', '$state', '$stateParams',
    'RecentHistorySaveService', 'ServiceMasterView',
    function($rootScope, $scope, $http, $location,
        ServiceMasterAdd, ServiceMasterEdit,
        ServiceMasterService, discardService,
        ngDialog, ngProgressFactory, appConstant, CommonValidationService,
        CostCenterSearchKeyword, ServiceTypeKeyword, $state, $stateParams,
        RecentHistorySaveService, ServiceMasterView) {

        $scope.contained_progressbar = ngProgressFactory.createInstance();
        $scope.contained_progressbar.setParent(document
            .getElementById('service-panel'));
        $scope.contained_progressbar.setAbsolute();

        $scope.cancelTab = false;

        $scope.init = function(acFlag) {
            $scope.firstFocus = "true";
            $scope.errorMap = new Map();
            if ($scope.serviceMaster == undefined) {
                $scope.serviceMaster = {};
                $scope.serviceMaster.status = 'Active';
            }
            if ($scope.serviceMaster != undefined && $scope.serviceMaster.costCenter == undefined || $scope.serviceMaster.costCenter == null) {
                $scope.serviceMaster.costCenter = {};
            }
            if (acFlag != undefined && acFlag != null && acFlag === 0) {
                $scope.setOldDataVal();
            }
        };


        $scope.setOldDataVal = function() {

            $scope.oldData = JSON.stringify($scope.serviceMaster);
            console.log("Service Data Loaded");
        }

        // Added for the bug : NES-1361 start

        function checkTabPress(e) {
            'use strict';
            var ele = document.activeElement;
            if (e.which === 9 && e.shiftKey) {
                if ($scope.cancelTab) {
                    var tabIdValue = 'serviceTypeMaster';
                    $rootScope.navigateToNextField(tabIdValue);
                    $scope.cancelTab = false;
                }

                if (ele.id === 'cancel') {
                    $scope.cancelTab = true;
                }

            }
        }
        document.addEventListener('keyup', function(e) {
            checkTabPress(e);
        }, false);

        // Added for the bug : NES-1362 start


        $scope.cancel = function() {
            if ($scope.oldData != JSON.stringify($scope.serviceMaster)) {
                var newScope = $scope.$new();
                newScope.errorMessage = $rootScope.nls["ERR200"];
                ngDialog.openConfirm({
                        template: '<p>{{errorMessage}}</p>' +
                            '<div class="ngdialog-footer">' +
                            ' <button type="button" class="btn btn-default btn-property cancel-btn" ng-click="confirm(2)">No</button>' +
                            '<button type="button" class="btn btn-default btn-property accent-btn " ng-click="confirm(1)">Yes</button>' +
                            '<button type="button" class="btn btn-default btn-property pull-left grey-cancel-btn" ng-click="confirm(3)">Cancel</button>' +
                            '</div>',
                        plain: true,
                        scope: newScope,
                        className: 'ngdialog-theme-default'
                    })
                    .then(
                        function(value) {
                            if (value == 1 &&
                                $scope.serviceMaster.id == null) {
                                $scope.save();
                            } else if (value == 1 &&
                                $scope.serviceMaster.id != null) {
                                $scope.update();
                            } else if (value == 2) {
                                var params = {};
                                params.submitAction = 'Cancelled';
                                $state.go("layout.service", params);
                            } else {
                                console.log("canceleed")
                            }

                        });
            } else {
                var params = {};
                params.submitAction = 'Cancelled';
                $state.go("layout.service", params);
            }
        }

        $scope.save = function() {
            console.log("Save Method is called.",
                $scope.serviceMaster.id)

            if ($scope.validateServiceMaster(0)) {
                $scope.contained_progressbar.start();
                ServiceMasterAdd.save($scope.serviceMaster).$promise
                    .then(function(data) {
                            if (data.responseCode == 'ERR0') {
                                ServiceMasterService.set({});
                                $rootScope.successDesc = $rootScope.nls["ERR400"];
                                var params = {}
                                params.submitAction = 'Saved';
                                $state.go("layout.service", params);
                            } else {
                                console.log("Service Master added Failed ", data.responseDescription)
                            }
                            $scope.contained_progressbar.complete();
                        },
                        function(error) {
                            console.log("Service Master added Failed : ", error)
                        });
            } else {
                console.log("Validation Failed");
            }
        }

        $scope.update = function() {
            console.log("Update Method is called.")
            if ($scope.validateServiceMaster(0)) {
                $scope.contained_progressbar.start();
                ServiceMasterEdit.update($scope.serviceMaster).$promise
                    .then(
                        function(data) {
                            if (data.responseCode == 'ERR0') {
                                console.log("Service Master updated Successfully")
                                ServiceMasterService.set({});
                                $rootScope.successDesc = $rootScope.nls["ERR401"];
                                var params = {}
                                params.submitAction = 'Saved';
                                $state.go("layout.service", params);
                            } else {
                                console.log("Service Master updated Failed " +
                                    data.responseDescription)
                            }
                            $scope.contained_progressbar
                                .complete();
                        },
                        function(error) {
                            console
                                .log("Service Master updated Failed : ", error)
                        });

            } else {
                console.log("Validation Failed");
            }

        }
        $scope.editServiceGetdata = function(serviceId) {
            ServiceMasterView.get({
                id: serviceId
            }, function(data) {
                if (data.responseCode == "ERR0") {
                    console.log("Get Enquiry for Edit", data.responseObject.id);
                    $scope.serviceMaster = data.responseObject;
                } else {
                    console.log("enquiry view Failed " + data.responseDescription)
                }
                $scope.setOldDataVal();
            }, function(error) {
                console.log("enquiry view Failed : " + error)
                $scope.setOldDataVal();
            });
        }


        $scope.showCostCenterList = false;
        $scope.costCenterlistConfig = {
            search: true,
            showCode: true,
            ajax: true,
            columns: [{
                    "title": "costCenterName",
                    seperator: false
                },
                {
                    "title": "costCenterCode",
                    seperator: true
                }
            ]
        }
        $scope.showCostCenter = function() {
            $scope.selectedItem = -1;
            $scope.panelTitle = "Cost Center";
            /*$scope.ajaxCostCenterEvent(null);*/
        };

        $scope.ajaxCostCenterEvent = function(object) {
            $scope.searchDto = {};
            $scope.searchDto.selectedPageNumber = 0;
            $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
            $scope.searchDto.keyword = object == null ? "" : object;

            return CostCenterSearchKeyword.fetch($scope.searchDto).$promise.then(function(
                    data, status) {
                    if (data.responseCode == "ERR0") {
                        $scope.costCenterList = [];
                        $scope.costCenterList = data.responseObject.searchResult;
                        //$scope.showCostCenterList=true;
                        return $scope.costCenterList;
                    }
                },
                function(errResponse) {
                    console.error('Error while fetching CostCenter');
                }

            );
        };
        //$scope.ajaxCostCenterEvent(null);

        $scope.selectedCostCenter = function(param, nextIdField) {
            //$scope.serviceMaster.costCenter= obj;
            if ($scope.validateServiceMaster(6)) {
                if (nextIdField != undefined)
                    $rootScope.navigateToNextField(nextIdField);
            }
            //$scope.cancelCostCenter();


        };

        $scope.cancelCostCenter = function() {
            $scope.showCostCenterList = false;
        }


        $scope.showServiceTypeList = false;
        $scope.serviceTypelistConfig = {
            search: true,
            showCode: true,
            ajax: true,
            columns: [{
                    "title": "serviceTypeName",
                    seperator: false
                },
                {
                    "title": "serviceTypeCode",
                    seperator: true
                }
            ]
        }
        $scope.showServiceType = function() {
            $scope.selectedItem = -1;
            $scope.panelTitle = "Service Type";
            /*$scope.ajaxServiceTypeEvent(null);*/
        };

        $scope.ajaxServiceTypeEvent = function(object) {
            $scope.searchDto = {};
            $scope.searchDto.selectedPageNumber = 0;
            $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
            $scope.searchDto.keyword = object == null ? "" : object;
            if ($scope.serviceMaster != undefined && $scope.serviceMaster.transportMode == undefined || $scope.serviceMaster.transportMode == null) {
                $scope.serviceMaster.transportMode = null;
            }
            return ServiceTypeKeyword.fetch({
                "transportMode": $scope.serviceMaster.transportMode
            }, $scope.searchDto).$promise.then(function(
                    data, status) {
                    if (data.responseCode == "ERR0") {
                        $scope.serviceTypeList = [];
                        $scope.serviceTypeList = data.responseObject.searchResult;
                        return $scope.serviceTypeList;
                        /*$scope.showServiceTypeList=true;*/
                    }
                },
                function(errResponse) {
                    console.error('Error while fetching ServiceType');
                }
            );
        };
        //$scope.ajaxServiceTypeEvent(null);

        $scope.selectedServiceType = function(obj) {
            /*$scope.serviceMaster.serviceType= obj;*/
            $scope.validateServiceMaster(7);
            /*$scope.cancelServiceType();*/


        };

        $scope.cancelServiceType = function() {
            $scope.showServiceTypeList = false;
        }

        $scope.validateServiceMaster = function(validateCode) {
            console.log("Validate Called");
            $scope.firstFocus = "false";
            $scope.errorMap = new Map();

            if (validateCode == 0 || validateCode == 1) {
                if ($scope.serviceMaster.serviceName == undefined ||
                    $scope.serviceMaster.serviceName == null ||
                    $scope.serviceMaster.serviceName == "") {
                    $scope.errorMap.put("serviceName", $rootScope.nls["ERR1408"]);


                    return false;
                } else {
                    if (!CommonValidationService.checkRegExp(appConstant.Reg_Exp_Master_Service_Name, $scope.serviceMaster.serviceName)) {
                        $scope.errorMap.put("serviceName", $rootScope.nls["ERR1405"]);
                        return false;
                    }
                }
            }

            if (validateCode == 0 || validateCode == 2) {
                if ($scope.serviceMaster.serviceCode == undefined ||
                    $scope.serviceMaster.serviceCode == null ||
                    $scope.serviceMaster.serviceCode == "") {
                    $scope.errorMap.put("serviceCode", $rootScope.nls["ERR1407"]);
                    return false;
                } else {
                    if (!CommonValidationService.checkRegExp(appConstant.Reg_Exp_Master_Service_Code, $scope.serviceMaster.serviceCode)) {
                        $scope.errorMap.put("serviceCode", $rootScope.nls["ERR1404"]);
                        return false;
                    }
                }
            }

            if (validateCode == 0 || validateCode == 3) {
                if ($scope.serviceMaster.transportMode == undefined ||
                    $scope.serviceMaster.transportMode == null ||
                    $scope.serviceMaster.transportMode == "") {
                    $scope.errorMap.put("transportMode", $rootScope.nls["ERR1409"]);
                    return false;
                }
            }

            if (validateCode == 0 || validateCode == 4) {
                if ($scope.serviceMaster.transportMode == 'Ocean' &&
                    ($scope.serviceMaster.fullGroupage == undefined ||
                        $scope.serviceMaster.fullGroupage == null || $scope.serviceMaster.fullGroupage == "")) {
                    $scope.errorMap.put("fullGroupage", $rootScope.nls["ERR1411"]);
                    return false;

                }
            }

            if (validateCode == 0 || validateCode == 5) {
                if ($scope.serviceMaster.importExport == undefined ||
                    $scope.serviceMaster.importExport == null ||
                    $scope.serviceMaster.importExport == "") {
                    $scope.errorMap.put("importExport", $rootScope.nls["ERR1410"]);
                    return false;
                }
            }

            if (validateCode == 0 || validateCode == 6) {
                if ($scope.serviceMaster.costCenter == undefined ||
                    $scope.serviceMaster.costCenter == null ||
                    $scope.serviceMaster.costCenter.id == null ||
                    $scope.serviceMaster.costCenter.id.trim == "") {
                    /*$scope.errorMap.put("costCenter","Cost Center is Mandatory");
							return false;*/
                } else {
                    if ($scope.serviceMaster.costCenter.status == 'Block') {
                        $scope.serviceMaster.costCenter = null;
                        $scope.errorMap.put("costCenter", $rootScope.nls["ERR1420"]);
                        return false;
                    }
                    if ($scope.serviceMaster.costCenter.status == 'Hide') {
                        $scope.errorMap.put("costCenter", $rootScope.nls["ERR1421"]);
                        return false;
                    }
                }
            }

            if (validateCode == 0 || validateCode == 7) {
                if ($scope.serviceMaster.serviceType != null && $scope.serviceMaster.serviceType.id != null) {
                    if ($scope.serviceMaster.serviceType.status == 'Block') {
                        $scope.serviceMaster.serviceType = null;
                        $scope.errorMap.put("serviceType", $rootScope.nls["ERR1422"]);
                        return false;
                    }
                    if ($scope.serviceMaster.serviceType.status == 'Hide') {
                        $scope.errorMap.put("serviceType", $rootScope.nls["ERR1423"]);
                        return false;
                    }
                }
            }


            return true;

        }

        $scope.setStatus = function(data) {
            if (data == 'Active') {
                return 'activetype';
            } else if (data == 'Block') {
                return 'blockedtype';
            } else if (data == 'Hide') {
                return 'hiddentype';
            }
        }
        $scope.costCenterRender = function(item) {
            return {
                label: item.costCenterName,
                item: item
            }
        };
        $scope.serviceRender = function(item) {
            return {
                label: item.serviceTypeName,
                item: item
            }
        };

        //On leave the Unfilled Service Form Broadcast then event will happen in StateChangeListener(BroadCaster)-app_state_route.js
        //This block is listener for Add Service Event
        $scope.$on('addServiceEvent', function(events, args) {
            $rootScope.unfinishedData = $scope.serviceMaster;
            $rootScope.category = "Service Master";
            $rootScope.unfinishedFormTitle = "Service (New)";

            if ($scope.serviceMaster != undefined && $scope.serviceMaster != null &&
                $scope.serviceMaster.serviceName != undefined &&
                $scope.serviceMaster.serviceName != null &&
                $scope.serviceMaster.serviceName != "") {
                $rootScope.subTitle = $scope.serviceMaster.serviceName;
            } else {
                $rootScope.subTitle = "Unknown Service";
            }

        })

        $scope.$on('editServiceEvent', function(events, args) {
            $rootScope.unfinishedData = $scope.serviceMaster;
            $rootScope.category = "Service Master";
            $rootScope.unfinishedFormTitle = "Service Edit (" + $scope.serviceMaster.serviceCode + ")";

            if ($scope.serviceMaster != undefined && $scope.serviceMaster != null &&
                $scope.serviceMaster.serviceName != undefined &&
                $scope.serviceMaster.serviceName != null &&
                $scope.serviceMaster.serviceName != "") {
                $rootScope.subTitle = $scope.serviceMaster.serviceName;
            } else {
                $rootScope.subTitle = "Unknown Service";
            }

        })
        $scope.$on('addServiceEventReload', function(e, confirmation) {
            confirmation.message = "All data willl be lost.";
            localStorage.reloadFormData = JSON.stringify($scope.serviceMaster);
            localStorage.isSERVICEReloaded = "YES";
            e.preventDefault();
        });
        $scope.$on('editServiceEventReload', function(e, confirmation) {
            confirmation.message = "All data willl be lost.";
            localStorage.reloadFormData = JSON.stringify($scope.serviceMaster);
            localStorage.isSERVICEReloaded = "YES";
            e.preventDefault();
        });

        $scope.isReloaded = localStorage.isSERVICEReloaded;
        if ($stateParams.action == "ADD") {
            if ($stateParams.fromHistory === 'Yes') {
                if ($scope.isReloaded == "YES") {
                    $scope.isReloaded = "NO";
                    localStorage.isSERVICEReloaded = "NO";
                    $scope.serviceMaster = JSON.parse(localStorage.reloadFormData);
                } else {
                    $scope.serviceMaster = $rootScope.selectedUnfilledFormData;
                }
                $scope.oldData = "";
                $scope.init()
            } else {
                if ($scope.isReloaded == "YES") {
                    $scope.isReloaded = "NO";
                    localStorage.isSERVICEReloaded = "NO";
                    $scope.serviceMaster = JSON.parse(localStorage.reloadFormData);
                    $scope.oldData = "";
                } else {
                    $scope.init(0);
                }
            }
            $rootScope.breadcrumbArr = [{
                    label: "Master",
                    state: "layout.service"
                },
                {
                    label: "Service",
                    state: "layout.service"
                },
                {
                    label: "Add Service",
                    state: null
                }
            ];
        } else {
            if ($stateParams.fromHistory === 'Yes') {
                if ($scope.isReloaded == "YES") {
                    $scope.isReloaded = "NO";
                    localStorage.isSERVICEReloaded = "NO";
                    $scope.serviceMaster = JSON.parse(localStorage.reloadFormData);
                } else {
                    $scope.serviceMaster = $rootScope.selectedUnfilledFormData;
                    $scope.init()
                }
                $scope.oldData = "";
            } else {
                if ($scope.isReloaded == "YES") {
                    $scope.isReloaded = "NO";
                    localStorage.isSERVICEReloaded = "NO";
                    $scope.serviceMaster = JSON.parse(localStorage.reloadFormData);
                    $scope.init()
                    $scope.oldData = "";
                } else {
                    $scope.editServiceGetdata($stateParams.serviceId);
                }

            }
            $rootScope.breadcrumbArr = [{
                    label: "Master",
                    state: "layout.service"
                },
                {
                    label: "Service",
                    state: "layout.service"
                },
                {
                    label: "Edit Service",
                    state: null
                }
            ];
        }


    }
]);