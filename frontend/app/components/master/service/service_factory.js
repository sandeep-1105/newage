(function() {

    app.factory("ServiceMasterSearch",['$resource', function($resource) {
        return $resource("/api/v1/servicemaster/get/search", {}, {
            query: {
                method: 'POST'
            }
        });
    }]);



    app.factory("ServiceMasterAdd",['$resource', function($resource) {
        return $resource("/api/v1/servicemaster/create", {}, {
            save: {
                method: 'POST'
            }
        });
    }]);

    app.factory("ServiceMasterEdit",['$resource', function($resource) {
        return $resource("/api/v1/servicemaster/update", {}, {
            update: {
                method: 'POST'
            }
        });
    }]);

    app.factory("ServiceMasterView",['$resource', function($resource) {
        return $resource("/api/v1/servicemaster/get/id/:id", {}, {
            get: {
                method: 'GET',
                params: {
                    id: ''
                },
                isArray: false
            }
        });
    }]);

    app.factory("ServiceMasterRemove",['$resource', function($resource) {
        return $resource("/api/v1/servicemaster/delete/:id", {}, {
            remove: {
                method: 'DELETE',
                params: {
                    id: ''
                },
                isArray: false
            }
        });
    }]);

    app.factory("ServiceList",['$resource', function($resource) {
        return $resource("/api/v1/servicemaster/get/search/keyword", {}, {
            fetch: {
                method: 'POST'
            }
        });
    }]);

    app.factory("SearchServiceNotInList",['$resource', function($resource) {
        return $resource("/api/v1/servicemaster/get/search/exclude", {}, {
            fetch: {
                method: 'POST'
            }
        });
    }]);


    app.factory("AirExportService",['$resource', function($resource) {
        return $resource("/api/v1/servicemaster/get/airexportservice", {}, {
            fetch: {
                method: 'GET',
                params: {},
                isArray: false
            }
        });
    }]);

    app.factory("ServiceTypeKeyword",['$resource', function($resource) {
        return $resource("/api/v1/servicetypemaster/get/search/keyword/:transportMode", {}, {
            fetch: {
                method: 'POST',
                params: {
                    transportMode: ''
                },
                isArray: false
            }
        });
    }]);

    app.factory("serviceTypeSearch",['$resource', function($resource) {
        return $resource("/api/v1/servicetypemaster/get/search", {}, {
            query: {
                method: 'POST'
            }
        });
    }]);



    app.factory("serviceTypeSave",['$resource', function($resource) {
        return $resource("/api/v1/servicetypemaster/create", {}, {
            save: {
                method: 'POST'
            }
        });
    }]);

    app.factory("serviceTypeUpdate",['$resource', function($resource) {
        return $resource("/api/v1/servicetypemaster/update", {}, {
            update: {
                method: 'POST'
            }
        });
    }]);

    app.factory("serviceTypeView",['$resource', function($resource) {
        return $resource("/api/v1/servicetypemaster/get/id/:id", {}, {
            get: {
                method: 'GET',
                params: {
                    id: ''
                },
                isArray: false
            }
        });
    }]);

    app.factory("serviceTypeRemove",['$resource', function($resource) {
        return $resource("/api/v1/servicetypemaster/delete/:id", {}, {
            remove: {
                method: 'DELETE',
                params: {
                    id: ''
                },
                isArray: false
            }
        });
    }]);




    app.factory("ServiceByTransportMode",['$resource', function($resource) {
        return $resource("/api/v1/servicemaster/get/search/keyword/:transportMode", {}, {
            fetch: {
                method: 'POST',
                params: {
                    transportMode: ''
                },
                isArray: false
            }
        });
    }]);




})();