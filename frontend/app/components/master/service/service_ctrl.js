app.controller('ServiceMasterCtrl',['$rootScope','$state', '$stateParams', '$scope', '$http', '$location', '$window', 
		'ngTableParams', 'ServiceMasterSearch', 'ServiceMasterView','ngDialog',	
		'ServiceMasterRemove', 'ServiceMasterService','$timeout','roleConstant', 
	function($rootScope,$state, $stateParams, $scope, $http, $location, $window, 
		ngTableParams, ServiceMasterSearch, ServiceMasterView,ngDialog,	
		ServiceMasterRemove, ServiceMasterService,$timeout,roleConstant) {


	/************************************************************
	 * ux - by Muthu
	 * reason - ipad compatibility fix
	 *
	 * *********************************************************/
	 $scope.deskTopView = true;

	 /********************* ux fix ends here *****************************/
	 $scope.serviceHeadArr=[
	 {
	 	"name":"#",
	 	"width":"col-xs-0half",
	 	"model":"no",
	 	"search":false
	 },
	 {
	 	"name":"Name",
	 	"width":"w200px",
	 	"model":"serviceName",
	 	"search":true,
	 	"wrap_cell":true,
	 	"type":"text",
	 	"sort":true,
	 	"key":"searchServiceName"

	 },
	 {
	 	"name":"Code",
	 	"width":"w75px",
	 	"model":"serviceCode",
	 	"search":true,
	 	"type":"text",
	 	"sort":true,
	 	"key":"searchServiceCode"
	 },
	 {
	 	"name":"Transport Mode",
	 	"width":"w90px",
	 	"model":"transportMode",
	 	"search":true,
	 	"type":"drop",
	 	"data":$rootScope.enum['TransportMode'],
	 	"sort":true,
	 	"key":"searchTransportMode"
	 },
	 {
	 	"name":"Full / Groupage",
	 	"width":"w90px",
	 	"model":"fullGroupage",
	 	"search":true,
	 	"type":"drop",
	 	"data":$rootScope.enum['FullGroupage'],
	 	"sort":true,
	 	"key":"searchFullGroupage"
	 },
	 {
	 	"name":"Import / Export",
	 	"width":"w90px",
	 	"model":"importExport",
	 	"search":true,
	 	"type":"drop",
	 	"data":$rootScope.enum['ImportExport'],
	 	"sort":true,
	 	"key":"searchImportExport"
	 },
	 {
	 	"name":"Status",
	 	"width":"w150px",
	 	"model":"status",
	 	"search":true,
	 	"type":"drop",
	 	"data":$rootScope.enum['LovStatus'],
	 	"key":"searchStatus",
	 	"sort":true
	 }]

	 $scope.sortSelection = {
	 	sortKey:"serviceName",
	 	sortOrder:"asc"
	 }



	 $scope.changepage = function(param){
	 	$scope.page = param.page;
	 	$scope.limit = param.size;
	 	$scope.cancel();
	 	$scope.search();
	 }

	 $scope.serviceMaster = {};
	 $scope.selectedMaster= {};
	 $scope.searchDto = {};
	 $scope.limitArr = [10,15,20];
	 $scope.page = 0;
	 $scope.limit = 10;


	 ServiceMasterService.set({});

	 $scope.init = function () {
	 	console.log("ServiceMaster Init method called.");

	 	$rootScope.setNavigate1("Master");
	 	$rootScope.setNavigate2("Service");
	 	ServiceMasterService.set({});
	 	$scope.searchDto = {};
	 	$scope.serviceMaster = {};
	 	$scope.searchDto.orderByType=$scope.sortSelection.sortOrder.toUpperCase();
	 	$scope.searchDto.sortByColumn = $scope.sortSelection.sortKey;
	 	$scope.showHistory=false;
	 	$scope.search();


	 };
	 $scope.limitChange = function(item){
	 	$scope.page = 0;
	 	$scope.limit = item;
	 	$scope.cancel();
	 	$scope.search();
	 }
	 $scope.rowSelect = function(data){
	 	if($rootScope.roleAccess(roleConstant.MASTER_GENERAL_SERVICE_VIEW)){
		//$scope.serviceMaster.id=1;
		$scope.showHistory = false;
		$scope.serviceMaster=data;

		/************************************************************
		 * ux - by Muthu
		 * reason - ipad compatibility fix
		 *
		 * *********************************************************/

		 var windowInner=$window.innerWidth;
		 if(windowInner<=1199){
		 	$scope.deskTopView = false;

		 }
		 else{
		 	$scope.deskTopView = true;
		 }
		 angular.element($window).bind('resize', function(){
		 	if($window.innerWidth>=1200){
		 		$scope.deskTopView = true;
		 	}
		 	else if($window.innerWidth<=1199){
		 		if($scope.serviceMaster.id!=null){
		 			$scope.deskTopView = false;
		 		}
		 		else {
		 			$scope.deskTopView = true;
		 		}
		 	}

		 	console.log("window resizing..." + $window.innerWidth);
		 	$scope.$digest();
		 });

		 /********************* ux fix ends here *****************************/




		}

	}
	$scope.cancel = function () {
		$scope.showHistory = false;
		$scope.serviceMaster = {};

		/************************************************************
		 * ux - by Muthu
		 * reason - ipad compatibility fix
		 *
		 * *********************************************************/
		 $scope.deskTopView = true;

		 /********************* ux fix ends here *****************************/


		};

		$scope.add = function(){
			if($rootScope.roleAccess(roleConstant.MASTER_GENERAL_SERVICE_CREATE)){
				$state.go("layout.addService");
			}
		};

		$scope.edit = function () {
			if($rootScope.roleAccess(roleConstant.MASTER_GENERAL_SERVICE_MODIFY)){
				var localRefStateParam = {
					serviceId: $scope.serviceMaster.id
				};
				var ishistoryFounded = false;
				if($rootScope.unfinishedFormHistoryList != undefined && $rootScope.unfinishedFormHistoryList != null && $rootScope.unfinishedFormHistoryList.length > 0 ) {
					for(var iIndex = 0; iIndex < $rootScope.unfinishedFormHistoryList.length; iIndex++) {
						var tit = "ServiceMaster Edit # "+$scope.serviceMaster.id;
						if($rootScope.unfinishedFormHistoryList[iIndex].title == tit) {
							var unSavedData = $rootScope.unfinishedFormHistoryList[iIndex];
			                GoToUnfilledFormNavigationService.goToState($rootScope.unfinishedFormHistoryList[iIndex], undefined);// $rootScope.unfinishedFormHistoryList.push(stateDataObject);
			                ishistoryFounded = true;
			            } 
			            if(ishistoryFounded) {
			            	break;
			            }
			        }
			    } 
			    if(!ishistoryFounded){
			    	$state.go("layout.editService",localRefStateParam);
			    }
			}

		};

		$scope.view = function (selectedMaster) {
			console.log("View ServiceMaster Called");
			$scope.serviceMaster=selectedMaster;

		};

		$scope.changeSearch = function(param){
			for (var attrname in param)
			{
				$scope.searchDto[attrname] = param[attrname];
			}
			$scope.page = 0;
			$scope.cancel();
			$scope.search();
			console.log("change search",$scope.searchDto);
		}

		$scope.sortChange = function(param){
			$scope.searchDto.orderByType = param.sortOrder.toUpperCase();
			$scope.searchDto.sortByColumn = param.sortKey;
			$scope.cancel();
			$scope.search();
		}
		$scope.deleteMaster = function() {
			if($rootScope.roleAccess(roleConstant.MASTER_GENERAL_SERVICE_DELETE)){
				var newScope = $scope.$new();
				newScope.errorMessage = $rootScope.nls["ERR229"];
				ngDialog.openConfirm({
					template:
					'<p>{{errorMessage}}</p>' +
					'<div class="ngdialog-footer">' +
					'<button type="button" class="btn btn-default btn-property cancel-btn" ng-click="closeThisDialog(0)">No' +
					'<button type="button" class="btn btn-primary btn-property accent-btn" ng-click="confirm(1)">Yes' +
					'</button>' +
					'</div>',
					plain: true,
					scope: newScope,
					className: 'ngdialog-theme-default'
				}).
				then(function (value) {
					ServiceMasterRemove.remove({
						id : $scope.serviceMaster.id
					}, function(data) {
						if (data.responseCode == 'ERR0') {
							console.log("ServiceMaster deleted Successfully");
							$scope.cancel();
							$scope.init();
							$scope.deskTopView = true;
						} else {
							console.log("ServiceMaster deleted Failed "
								+ data.responseDescription)
						}
					}, function(error) {
						console.log("ServiceMaster deleted Failed : " + error)
					});
				}, function (value) {
					console.log("deleted cancelled");

				});
			}
		}

		$scope.search=function(){
			$scope.serviceMaster={};
			$scope.searchDto.selectedPageNumber = $scope.page;
			$scope.searchDto.recordPerPage = $scope.limit;
			$scope.serviceArr = [];
			ServiceMasterSearch.query($scope.searchDto).$promise.then(function(
				data, status) {

				$scope.totalRecord = data.responseObject.totalRecord;
				var tempArr = [];
				var resultArr = [];
				tempArr = data.responseObject.searchResult;
				var tempObj = {};
				angular.forEach(tempArr,function(item,index){
					tempObj = item;
					tempObj.no = (index+1)+($scope.page*$scope.limit);
					resultArr.push(tempObj);
					tempObj = {};
				});
				$scope.serviceArr = resultArr;
			});

		}


		$scope.serialNo=function(index){
			index=($scope.searchDto.selectedPageNumber * $scope.searchDto.recordPerPage) + (index+1);
			return index;
		}



		if($stateParams.action == 'SEARCH') {
			$scope.init();
			$rootScope.breadcrumbArr = [
			{ label:"Master", 
			state:"layout.service"}, 
			{ label:"Service", 
			state:"layout.service"}
			];
		}



	}]);
