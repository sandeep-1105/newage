app.controller('packMasterEditController',['$rootScope', '$scope', 'PackAdd', 'appConstant', 'CommonValidationService', 
		'PackEdit', 'ngProgressFactory', 'ngDialog', '$stateParams', '$state', 'PackView',
	function($rootScope, $scope, PackAdd, appConstant, CommonValidationService, 
		PackEdit, ngProgressFactory, ngDialog, $stateParams, $state, PackView) {

    $scope.contained_progressbar = ngProgressFactory.createInstance();
    $scope.contained_progressbar.setParent(document.getElementById('pack-panel'));
    $scope.contained_progressbar.setAbsolute();

    $scope.init = function() {

        if ($scope.packMaster == undefined || $scope.packMaster == null) {
            $scope.packMaster = {};
            $scope.packMaster.status = 'Active';
        }

        $scope.setOldDataVal();
    };

    $scope.setOldDataVal = function() {
        $scope.oldData = JSON.stringify($scope.packMaster);
    }

    $scope.cancel = function() {

        if ($scope.oldData != JSON.stringify($scope.packMaster)) {
            var newScope = $scope.$new();
            newScope.errorMessage = $rootScope.nls["ERR200"];
            ngDialog.openConfirm({
                template: '<p>{{errorMessage}}</p>' +
                    '<div class="ngdialog-footer">' +
                    ' <button type="button" class="btn btn-default btn-property cancel-btn" ng-click="confirm(2)">No</button>' +
                    '<button type="button" class="btn btn-default btn-property accent-btn " ng-click="confirm(1)">Yes</button>' +
                    '<button type="button" class="btn btn-default btn-property pull-left grey-cancel-btn" ng-click="confirm(3)">Cancel</button>' +
                    '</div>',
                plain: true,
                scope: newScope,
                className: 'ngdialog-theme-default'
            }).then(function(value) {
                if (value == 1 && $scope.packMaster.id == null) {
                    $scope.save();
                } else if (value == 1 && $scope.packMaster.id != null) {
                    $scope.update();
                } else if (value == 2) {
                    var params = {};
                    params.submitAction = 'Cancelled';
                    $state.go("layout.pack", params);
                } else {
                    console.log("cancelled");
                }
            });
        } else {
            var params = {};
            params.submitAction = 'Cancelled';
            $state.go("layout.pack", params);
        }
    }




    $scope.update = function() {
        console.log("Update Method is called.");

        if ($scope.validatePackMaster(0)) {

            $scope.contained_progressbar.start();

            PackEdit.update($scope.packMaster).$promise.then(function(data) {
                if (data.responseCode == 'ERR0') {
                    console.log("Pack updated Successfully")
                    var params = {}
                    $rootScope.successDesc = $rootScope.nls["ERR401"];
                    params.submitAction = 'Saved';
                    $state.go("layout.pack", params);
                } else {
                    console.log("Pack updated Failed ", data.responseDescription)
                }
                $scope.contained_progressbar.complete();
                angular.element(".panel-body").animate({
                    scrollTop: 0
                }, "slow");
            }, function(error) {
                console.log("Pack updation Failed : ", error);
            });
        } else {
            console.log("Invalid Form Submission.");
        }
    }

    $scope.save = function() {
        console.log("Save Method is called.");

        if ($scope.validatePackMaster(0)) {
            $scope.contained_progressbar.start();

            PackAdd.save($scope.packMaster).$promise.then(function(data) {
                if (data.responseCode == 'ERR0') {
                    console.log("Pack Saved Successfully")
                    var params = {}
                    $rootScope.successDesc = $rootScope.nls["ERR400"];
                    params.submitAction = 'Saved';
                    $state.go("layout.pack", params);
                } else {
                    console.log("Pack Saving Failed ", data.responseDescription)
                }
                $scope.contained_progressbar.complete();
                angular.element(".panel-body").animate({
                    scrollTop: 0
                }, "slow");
            }, function(error) {
                console.log("Pack Saving Failed : ", error)
            });
        } else {
            console.log("Invalid Form Submission.");
        }
    }


    $scope.validatePackMaster = function(validateCode) {
        console.log("Validate Called ", validateCode);

        $scope.errorMap = new Map();

        if (validateCode == 0 || validateCode == 1) {
            if ($scope.packMaster.packName == undefined || $scope.packMaster.packName == null || $scope.packMaster.packName == "") {
                $scope.errorMap.put("packName", $rootScope.nls["ERR1905"]);
                return false;
            } else {

                if (!CommonValidationService.checkRegExp(appConstant.Reg_Exp_Master_Pack_Name, $scope.packMaster.packName)) {
                    $scope.errorMap.put("packName", $rootScope.nls["ERR1906"]);
                    return false;
                }
            }
        }

        if (validateCode == 0 || validateCode == 2) {
            if ($scope.packMaster.packCode == undefined || $scope.packMaster.packCode == null || $scope.packMaster.packCode == "") {
                $scope.errorMap.put("packCode", $rootScope.nls["ERR1903"]);
                return false;
            } else {
                if (!CommonValidationService.checkRegExp(appConstant.Reg_Exp_Master_Pack_Name, $scope.packMaster.packCode)) {
                    $scope.errorMap.put("packCode", $rootScope.nls["ERR1904"]);
                    return false;
                }
            }
        }

        return true;
    }

    $scope.setStatus = function(data) {
        if (data == 'Active') {
            return 'activetype';
        } else if (data == 'Block') {
            return 'blockedtype';
        } else if (data == 'Hide') {
            return 'hiddentype';
        }
    }


    // History Start

    $scope.getPackById = function(packId) {
        console.log("getPackById ", packId);

        PackView.get({
            id: packId
        }, function(data) {
            if (data.responseCode == "ERR0") {
                console.log("Successful while getting pack.", data)
                $scope.packMaster = data.responseObject;
            }
            $scope.setOldDataVal();
        }, function(error) {
            console.log("Error while getting pack.", error)
        });

    }


    //On leave the Unfilled pack Form Broadcast then event will happen in StateChangeListener(BroadCaster)-app_state_route.js

    $scope.$on('addPackEvent', function(events, args) {

        $rootScope.unfinishedData = $scope.packMaster;
        $rootScope.category = "Pack Master";
        $rootScope.unfinishedFormTitle = "Pack (New)";
        if ($scope.packMaster != undefined && $scope.packMaster != null && $scope.packMaster.packName != undefined &&
            $scope.packMaster.packName != null && $scope.packMaster.packName != "") {
            $rootScope.subTitle = $scope.packMaster.packName;
        } else {
            $rootScope.subTitle = "Unknown Pack"
        }
    })

    $scope.$on('editPackEvent', function(events, args) {
        $rootScope.unfinishedData = $scope.packMaster;
        $rootScope.category = "Pack Master";
        $rootScope.unfinishedFormTitle = "Pack Edit (" + $scope.packMaster.packCode + ")";
        if ($scope.packMaster != undefined && $scope.packMaster != null && $scope.packMaster.packName != undefined &&
            $scope.packMaster.packName != null && $scope.packMaster.packName != "") {
            $rootScope.subTitle = $scope.packMaster.packName;
        } else {
            $rootScope.subTitle = "Unknown Pack"
        }
    })


    $scope.$on('addPackEventReload', function(e, confirmation) {
        console.log("addPackEventReload is called ", $scope.packMaster);
        confirmation.message = "";
        localStorage.reloadFormData = JSON.stringify($scope.packMaster);
        localStorage.isPackReloaded = "YES";
        e.preventDefault();
    });


    $scope.$on('editPackEventReload', function(e, confirmation) {
        confirmation.message = "";
        localStorage.reloadFormData = JSON.stringify($scope.packMaster);
        localStorage.isPackReloaded = "YES";
        e.preventDefault();
    });


    $scope.isReloaded = localStorage.isPackReloaded;

    if ($stateParams.action == "ADD") {
        if ($stateParams.fromHistory === 'Yes') {
            if ($scope.isReloaded == "YES") {
                console.log("Add History Reload...")
                $scope.isReloaded = "NO";
                localStorage.isPackReloaded = "NO";
                $scope.packMaster = JSON.parse(localStorage.reloadFormData);
                $scope.init();
            } else {
                console.log("Add History NotReload...")
                $scope.packMaster = $rootScope.selectedUnfilledFormData;
                $scope.init();
            }
            $scope.oldData = "";
        } else {
            if ($scope.isReloaded == "YES") {
                $scope.isReloaded = "NO";
                localStorage.isPackReloaded = "NO";
                $scope.packMaster = JSON.parse(localStorage.reloadFormData);
                console.log("Add NotHistory Reload...");
                $scope.init();
                $scope.oldData = "";
            } else {
                console.log("Add NotHistory NotReload...")
                $scope.init();
            }
        }
        $rootScope.breadcrumbArr = [{
                label: "Master",
                state: "layout.pack"
            },
            {
                label: "Pack",
                state: "layout.pack"
            },
            {
                label: "Add Pack",
                state: null
            }
        ];


    } else {
        if ($stateParams.fromHistory === 'Yes') {
            if ($scope.isReloaded == "YES") {
                $scope.isReloaded = "NO";
                localStorage.isPackReloaded = "NO";
                $scope.packMaster = JSON.parse(localStorage.reloadFormData);
                $scope.init();
            } else {
                $scope.packMaster = $rootScope.selectedUnfilledFormData;
                $scope.init();
            }
            $scope.oldData = "";
        } else {
            if ($scope.isReloaded == "YES") {
                $scope.isReloaded = "NO";
                localStorage.isPackReloaded = "NO";
                $scope.packMaster = JSON.parse(localStorage.reloadFormData);
                $scope.init();
                $scope.oldData = "";
            } else {
                $scope.getPackById($stateParams.packId);
                $scope.init();
            }
        }
        $rootScope.breadcrumbArr = [{
                label: "Master",
                state: "layout.pack"
            },
            {
                label: "Pack",
                state: "layout.pack"
            },
            {
                label: "Edit Pack",
                state: null
            }
        ];
    }


    // History End
}]);