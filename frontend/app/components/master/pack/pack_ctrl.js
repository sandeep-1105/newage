app.controller('packMasterController',['$rootScope', '$scope', '$state', '$stateParams', 
		'$window', 'ngDialog', 'PackSearch', 'PackRemove', 'roleConstant',
	function($rootScope, $scope, $state, $stateParams, 
		$window, ngDialog, PackSearch, PackRemove, roleConstant) {


    /************************************************************
     * ux - by Muthu
     * reason - ipad compatibility fix
     *
     * *********************************************************/
    $scope.deskTopView = true;

    /********************* ux fix ends here *****************************/
    $scope.packHeadArr = [{
            "name": "#",
            "width": "col-xs-0half",
            "model": "no",
            "search": false
        },
        {
            "name": "Name",
            "width": "col-xs-5",
            "model": "packName",
            "search": true,
            "wrap_cell": true,
            "type": "text",
            "sort": true,
            "key": "searchPackName"
        },
        {
            "name": "Code",
            "width": "col-xs-2half",
            "model": "packCode",
            "search": true,
            "type": "text",
            "sort": true,
            "key": "searchPackCode"
        },
        {
            "name": "Status",
            "width": "col-xs-2half",
            "model": "status",
            "search": true,
            "sort": true,
            "type": "drop",
            "data": $rootScope.enum['LovStatus'],
            "key": "searchStatus"
        }
    ];

    $scope.sortSelection = {
        sortKey: "packName",
        sortOrder: "asc"
    }

    $scope.limitArr = [10, 15, 20];
    $scope.page = 0;
    $scope.limit = 10;

    $scope.init = function() {
        console.log("pack Init method called.................");
        $scope.searchDto = {};
        $scope.packMaster = {};
        $scope.search();
    };


    $scope.limitChange = function(item) {
        console.log("Limit Change is called...", item);
        $scope.page = 0;
        $scope.limit = item;
        $scope.search();
    }




    $scope.rowSelect = function(data) {

        if ($rootScope.roleAccess(roleConstant.MASTER_GENERAL_PACK_VIEW)) {

            $scope.packMaster = data;

            /************************************************************
             * ux - by Muthu
             * reason - ipad compatibility fix
             *
             * *********************************************************/

            var windowInner = $window.innerWidth;
            if (windowInner <= 1199) {
                $scope.deskTopView = false;

            } else {
                $scope.deskTopView = true;
            }
            angular.element($window).bind('resize', function() {
                if ($window.innerWidth >= 1200) {
                    $scope.deskTopView = true;
                } else if ($window.innerWidth <= 1199) {
                    if ($scope.packMaster.id != null) {
                        $scope.deskTopView = false;
                    } else {
                        $scope.deskTopView = true;
                    }
                }

                console.log("window resizing..." + $window.innerWidth);
                $scope.$digest();
            });

            /********************* ux fix ends here *****************************/


            console.log("selected Pack Master ", $scope.packMaster);
        }
    }

    $scope.changeSearch = function(param) {

        console.log("Change Search", param);

        for (var attrname in param) {
            $scope.searchDto[attrname] = param[attrname];
        }

        $scope.page = 0;
        console.log("change search", $scope.searchDto);

        $scope.search();
    }

    $scope.sortChange = function(param) {
        console.log("Sort Change Called.", param);

        $scope.searchDto.orderByType = param.sortOrder.toUpperCase();
        $scope.searchDto.sortByColumn = param.sortKey;

        $scope.search();
    }

    $scope.changePage = function(param) {
        console.log("Change Page Called.", param);
        $scope.page = param.page;
        $scope.limit = param.size;
        $scope.search();
    }



    $scope.search = function() {
        console.log("Search method is called............................!");

        $scope.packMaster = {};
        $scope.searchDto.selectedPageNumber = $scope.page;
        $scope.searchDto.recordPerPage = $scope.limit;
        $scope.packArr = [];

        PackSearch.query($scope.searchDto).$promise.then(function(data, status) {

            $scope.totalRecord = data.responseObject.totalRecord;

            var tempArr = [];
            var resultArr = [];
            tempArr = data.responseObject.searchResult;

            var tempObj = {};
            angular.forEach(tempArr, function(item, index) {
                tempObj = item;
                tempObj.no = (index + 1) + ($scope.page * $scope.limit);
                resultArr.push(tempObj);
                tempObj = {};
            });

            $scope.packArr = resultArr;
        });

    }


    $scope.addNewPack = function() {
        if ($rootScope.roleAccess(roleConstant.MASTER_GENERAL_PACK_CREATE)) {
            console.log("Add Button is Pressed.")
            $state.go("layout.addPack");
        }
    }



    $scope.deletePack = function() {
        if ($rootScope.roleAccess(roleConstant.MASTER_GENERAL_PACK_DELETE)) {
            var newScope = $scope.$new();
            newScope.errorMessage = $rootScope.nls["ERR231"];
            ngDialog.openConfirm({
                    template: '<p>{{errorMessage}}</p>' +
                        '<div class="ngdialog-footer">' +
                        '<button type="button" class="btn btn-default btn-property cancel-btn" ng-click="closeThisDialog(0)">Cancel</button>' +
                        '<button type="button" class="btn btn-primary btn-property accent-btn" ng-click="confirm(1)">Yes</button></div>',
                    plain: true,
                    scope: newScope,
                    className: 'ngdialog-theme-default'
                })
                .then(function(value) {
                    PackRemove.remove({
                        id: $scope.packMaster.id
                    }, function(data) {
                        if (data.responseCode == 'ERR0') {
                            console.log("Pack deleted Successfully")
                            $scope.cancel();
                            $scope.init();
                            $scope.deskTopView = true;
                        } else {
                            console.log("Pack deleted Failed ", data.responseDescription);
                        }
                    }, function(error) {
                        console.log("Pack deleted Failed : ", error);
                    });
                }, function(value) {
                    console.log("deleted cancelled ", value);
                });
        }
    }

    $scope.editpack = function(data) {
        if ($rootScope.roleAccess(roleConstant.MASTER_GENERAL_PACK_MODIFY)) {
            console.log("Edit Button is Pressed.");
            $state.go("layout.editPack", {
                packId: $scope.packMaster.id
            });
        }
    }

    $scope.cancel = function() {

        $scope.packMaster = {};
        /************************************************************
         * ux - by Muthu
         * reason - ipad compatibility fix
         *
         * *********************************************************/
        $scope.deskTopView = true;

        /********************* ux fix ends here *****************************/
    }



    switch ($stateParams.action) {
        case "SEARCH":
            console.log("Pack Master Search Page....");
            $scope.init();
            $rootScope.breadcrumbArr = [{
                    label: "Master",
                    state: "layout.pack"
                },
                {
                    label: "Pack",
                    state: "layout.pack"
                }
            ];
            break;
    }



}]);