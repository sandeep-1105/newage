(function() {

	app.factory("PackSearch",['$resource', function($resource) {
		return $resource("/api/v1/packmaster/get/search", {}, {
			query : {
				method : 'POST'
			}
		});
	}]);
	
	app.factory("PackList",['$resource', function($resource) {
		return $resource("/api/v1/packmaster/get/search/keyword", {}, {
			fetch : {
				method : 'POST'
			}
		});
	}]);

	app.factory("PackAdd",['$resource', function($resource) {
		return $resource("/api/v1/packmaster/create", {}, {
			save : {
				method : 'POST'
			}
		});
	}]);

	app.factory("PackEdit",['$resource', function($resource) {
		return $resource("/api/v1/packmaster/update", {}, {
			update : {
				method : 'POST'
			}
		});
	}]);

	app.factory("PackView",['$resource', function($resource) {
		return $resource("/api/v1/packmaster/get/id/:id", {}, {
			get : {
				method : 'GET',
				params : {
					id : ''
				},
				isArray : false
			}
		});
	}]);

	app.factory("PackRemove",['$resource', function($resource) {
		return $resource("/api/v1/packmaster/delete/:id", {}, {
			remove : {
				method : 'DELETE',
				params : {
					id : ''
				},
				isArray : false
			}
		});
	}]);
	
	
	app.factory("PackGetByCode",['$resource', function($resource) {
		return $resource("/api/v1/packmaster/get/code/:code", {}, {
			get : {
				method : 'GET',
				params : {
					code : ''
				},
				isArray : false
			}
		});
	}]);
	
	
	
})();
