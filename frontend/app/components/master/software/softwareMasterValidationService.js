app.service('softwareMasterValidationService', ['$rootScope','ValidateUtil', 'CommonValidationService', 'appConstant', function($rootScope, ValidateUtil, CommonValidationService, appConstant) {
	this.validate = function(software, code) {
		//Validate software name
		if(code == 0 || code == 1) {
			if(software.softwareName == undefined || software.softwareName == null || software.softwareName =="" ){
				return this.validationResponse(true, "softwareName", $rootScope.nls["ERR06801"], software);
			}
		}
		//Validate software code
		if(code == 0 || code == 2) {
			if(software.softwareCode == undefined || software.softwareCode == null || software.softwareCode =="" ){
				return this.validationResponse(true, "softwareCode", $rootScope.nls["ERR06800"], software);
			}
		}
		
		return this.validationSuccesResponse(software);
	}
 
	this.validationSuccesResponse = function(software) {
	    return {error : false, obj : software};
	}
	
	this.validationResponse = function(err, elem, message, software) {
		return {error : err, errElement : elem, errMessage : message, obj : software};
	}
	
}]);