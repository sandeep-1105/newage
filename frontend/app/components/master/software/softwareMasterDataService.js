app.service('softwareMasterDataService', function($rootScope) {
    this.getHeadArray = function() {
	   return [
			  {
					"name":"#",
					"width":"col-xs-0half",
					"model":"no",
					"search":false
			  },{
					"name":"Name",
					"width":"col-xs-4half",
					"model":"softwareName",
					"search":true,
					"wrap_cell":true,
					"type":"text",
					"sort":true,
					"key":"searchSoftwareName"
				},{
					"name":"Code",
					"width":"col-xs-2",
					"model":"softwareCode",
					"search":true,
					"type":"text",
					"sort":true,
					"key":"searchSoftwareCode"
				},{
					"name":"Status",
					"width":"col-xs-3",
					"model":"status",
					"search":true,
					"type":"drop",
					"data":$rootScope.enum['LovStatus'],
					"key":"searchStatus",
					"sort":true
				}];
          };

    
    this.getListBreadCrumb = function() {
        return [{
                label: "Master",
                state: "layout.softwareMaster"
            },
            {
                label: "Software",
                state: "layout.softwareMaster"
            }
        ];
    }

    this.getAddBreadCrumb = function() {
        return [{
                label: "Master",
                state: "layout.softwareMaster"
            },
            {
                label: "Software",
                state: "layout.softwareMaster"
            },
            {
                label: "Add Software",
                state: null
            }
        ];
    };
    this.getEditBreadCrumb = function() {
        return [{
                label: "Master",
                state: "layout.softwareMaster"
            },
            {
                label: "Software",
                state: "layout.softwareMaster"
            },
            {
                label: "Edit Software",
                state: null
            }
        ];
    };
   

});