(function(){
app.controller("SoftwareMasterCtrl", ['$scope', '$rootScope', '$window', '$modal', '$state', '$stateParams', '$log', 'ngDialog', 'softwareMasterDataService', 'softwareMasterFactory', 'RecentHistorySaveService', 'softwareMasterValidationService', 'Notification', 'roleConstant',
    function($scope, $rootScope, $window, $modal, $state, $stateParams, $log, ngDialog, softwareMasterDataService, softwareMasterFactory, RecentHistorySaveService, softwareMasterValidationService, Notification,roleConstant) {

        var vm = this;
        vm.tableHeadArr = softwareMasterDataService.getHeadArray();
        vm.dataArr = [];
        vm.softwareMaster = {};
        vm.showHistory = false;
        vm.errorMap = new Map();
        vm.deskTopView = true;

        vm.sortSelection = {
            sortKey: "softwareName",
            sortOrder: "asc"
        }
        
        vm.changeSearch = function(param) {
            $log.debug("Change Search is called." + param);
            for (var attrname in param) {
                vm.searchData[attrname] = param[attrname];
            }
            vm.page = 0;
            vm.search();
        }

        vm.sortChange = function(param) {
            $log.debug("sortChange is called. " + param);
            vm.searchData.orderByType = param.sortOrder.toUpperCase();
            vm.searchData.sortByColumn = param.sortKey;
            vm.search();
        }
        
        vm.setStatus = function(data){
    		if(data=='Active'){
    			return 'activetype';
    		} else if(data=='Block'){
    			return 'blockedtype';
    		}else if(data=='Hide'){
    			return 'hiddentype';
    		}
    	}
        
        /**
         * Software Master List Navigate to selected page.
         * @param {Object} param - The Selected page option.
         */
        vm.changepage = function(param) {
            $log.debug("change Page..." + param.page);
            vm.page = param.page;
            vm.limit = param.size;
            vm.search();
        }

        /**
         * Software Master List Page Limit Data change.
         * @param {int} nLimit - The Selected limit option from Limit array.
         */
        vm.limitChange = function(nLimit) {
            $log.debug("Limit Change..." + nLimit);
            vm.page = 0;
            vm.limit = nLimit;
            vm.search();
        }

        /**
         * Software Master List Page Search Data  initializing and calling search Mtd.
         * @param {Object} data - The Selected Software Master from List page.
         * @param {string} data.id - The id of the Selected Software Master.
         * @param {int} index - The Selected row index from List page.
         */
        vm.rowSelect = function(data) {
        	if($rootScope.roleAccess(roleConstant.MASTER_GENERAL_SOFTWARE_VIEW)){
        	vm.softwareMasterView=true;
			vm.softwareMaster = data;
			var windowInner=$window.innerWidth;
			if(windowInner<=1199){
				vm.deskTopView = false;
			}
			else{
				vm.deskTopView = true;
			}
			angular.element($window).bind('resize', function(){
				if($window.innerWidth>=1200){
					vm.deskTopView = true;
				}
				else if($window.innerWidth<=1199){
					if(vm.softwareMaster.id!=null){
						vm.deskTopView = false;
					}
					else {
						vm.deskTopView = true;
					}
				}
				$scope.$digest();
			});
        }
		}

        /**
         * Software Master List Page Search Data  initializing and calling search Mtd.
         */
        vm.searchInit = function() {
            vm.searchData = {};
            vm.limitArr = [10, 15, 20];
            vm.page = 0;
            vm.limit = 10;
            vm.totalRecord = 10;
            vm.searchData.orderByType = vm.sortSelection.sortOrder.toUpperCase();
            vm.searchData.sortByColumn = vm.sortSelection.sortKey;
            vm.search();
        };
        /**
         * Go Back to Software Master List Page.
         */
        vm.backToList = function() {
        	vm.softwareMasterView = false;
        	vm.softwareMaster = {};
        	vm.deskTopView = true;
        }

       
        /**
         * Get Software Master's Based on Search Data.
         */
        vm.search = function() {
        	vm.softwareMasterView=false;
            vm.searchData.selectedPageNumber = vm.page;
            vm.searchData.recordPerPage = vm.limit;
            vm.dataArr = [];
            softwareMasterFactory.search.query(vm.searchData).$promise.then(
                function(data) {
                    if (data.responseCode == "ERR0") {
                        vm.totalRecord = data.responseObject.totalRecord;
                        vm.dataArr = data.responseObject.searchResult;
                    }
                },
                function(errResponse) {
                	 $log.error('Error while fetching Software');
                });
           }

        /**
         * Get Software Master By id.
         * @param {int} id - The id of a Software.
         * @param {boolean} isView - TO identify purpose of calling is for View or Edit a Software.
         */
        vm.view = function(id, isView) {
        	softwareMasterFactory.findById.query({
                id: id
            }).$promise.then(function(data) {
                if (data.responseCode == 'ERR0') {
                    vm.softwareMaster = data.responseObject;
                    var rHistoryObj = {}

                    if (vm.softwareMaster != undefined && vm.softwareMaster != null && vm.softwareMaster.id != undefined && vm.softwareMaster.id != null) {
                        $rootScope.subTitle = vm.softwareMaster.softwareName;
                    } else {
                        $rootScope.subTitle = "Unknown Software"
                    }
                    $rootScope.unfinishedFormTitle = "Software # " + vm.softwareMaster.softwareCode;
                    if (isView) {
                        rHistoryObj = {
                            'title': "Software #" + vm.softwareMaster.softwareCode,
                            'subTitle': $rootScope.subTitle,
                            'stateName': $state.current.name,
                            'stateParam': JSON.stringify($stateParams),
                            'stateCategory': 'Software Master',
                            'serviceType': undefined,
                            'showService': false
                        }

                    } else {

                        rHistoryObj = {
                            'title': 'Edit Software # ' + $stateParams.serviceId,
                            'subTitle': $rootScope.subTitle,
                            'stateName': $state.current.name,
                            'stateParam': JSON.stringify($stateParams),
                            'stateCategory': 'Software Master',
                            'serviceType': 'MASTER',
                            'serviceCode': ' ',
                            'orginAndDestination': ' '
                        }
                    }
                    RecentHistorySaveService.form(rHistoryObj);
                } else {
                    $log.debug("Exception while getting Software by Id ", data.responseDescription);
                }
            }, function(error) {
                $log.debug("Error ", error);
            });
        };

        /**
         * Go To Add Software Master.
         */
        vm.addNew = function() {
        	if($rootScope.roleAccess(roleConstant.MASTER_GENERAL_SOFTWARE_CREATE)){
            $state.go("layout.softwareMasterAdd");
        	}
        }

        /**
         * Cancel Add or Edit Software Master.
         * @param {int} id - The id of a Software
         */
       
        vm.cancel = function(objId) {
            if(vm.softwareMasterForm!=undefined){
		    	vm.goTocancel(vm.softwareMasterForm.$dirty,objId);
            }else{
            	vm.goTocancel(false,objId);
            }
        }
       
       vm.goTocancel = function(isDirty,objId){
     		 if (isDirty) {
     		      var newScope = $scope.$new();
     		      newScope.errorMessage = $rootScope.nls["ERR200"];
     		      ngDialog.openConfirm({
     		        template: '<p>{{errorMessage}}</p>' +
     		          '<div class="ngdialog-footer">' +
     		          ' <button type="button" class="btn btn-default btn-property cancel-btn" ng-click="confirm(2)">No</button>' +
     		          '<button type="button" class="btn btn-default btn-property accent-btn " ng-click="confirm(1)">Yes</button>' +
     		          '<button type="button" class="btn btn-default btn-property pull-left grey-cancel-btn" ng-click="confirm(3)">Cancel</button>' +
     		          '</div>',
     		        plain: true,
     		        closeByDocument: false,
     		        scope: newScope,
     		        className: 'ngdialog-theme-default'
     		      }).then(
     		        function(value) {
     		          if (value == 1) {
     		            if(objId!=null && objId != undefined){
     		            	vm.update();
     		            }else{
     		            	vm.create();
     		            }
     		          } else if (value == 2) {
     		        	 $state.go("layout.softwareMaster", {
     		                submitAction: "Cancelled"
     		            });
     		          } else {
     		            $log.debug("cancelled");
     		          }
     		        });
     		    } else {
     		     $state.go("layout.softwareMaster", {
                   submitAction: "Cancelled"
               });
     		    }
     	    }

        /**
         * Modify Existing Software Master.
         * @param {int} id - The id of a Country.
         */
        vm.edit = function(id) {
        	if($rootScope.roleAccess(roleConstant.MASTER_GENERAL_SOFTWARE_MODIFY)){
            $state.go("layout.softwareMasterEdit", {
                id: id
            });
        	}
        }

        /**
         * Delete Country Master.
         * @param {int} id - The id of a Country.
         */
        vm.delete = function(id) {
        	if($rootScope.roleAccess(roleConstant.MASTER_GENERAL_SOFTWARE_DELETE)){
        	var newScope = $scope.$new();
			newScope.errorMessage = $rootScope.nls["ERR06807"];
	    	ngDialog.openConfirm( {
	                    template : '<p>{{errorMessage}}</p>'
	                    + '<div class="ngdialog-footer">'
	                    + '<button type="button" class="btn btn-default btn-property cancel-btn" ng-click="closeThisDialog(0)">Cancel'
	                    + '<button type="button" class="btn btn-primary btn-property accent-btn" ng-click="confirm(1)">Yes'
	                    + '</button></div>',
	                    plain : true,
	                    scope : newScope,
	                    closeByDocument: false,
	                    className : 'ngdialog-theme-default'
	                }).then( function(value) {
	                	softwareMasterFactory.delete.query({
	                        id: id
	                    }).$promise.then(function(data) {
	                        if (data.responseCode == 'ERR0') {
	                        	vm.softwareMasterView=false;
	                        	Notification.success($rootScope.nls["ERR402"]);
	                        	vm.deskTopView = true;
	                            vm.search();
	                        }
	                    }, function(error) {
	                        $log.debug("Error ", error);
	                    });
	                }, function(value) {
	                    $log.debug("delete Cancelled");
	            });
        }
        }

        /**
         * Validating field of Software Master while instant change.
         * @param {errorElement,errorCode}
         */
        vm.validate = function(valElement, code) {
            var validationResponse = softwareMasterValidationService.validate(vm.softwareMaster,code);
            if(validationResponse.error == true) {
				$scope.validationErrorAndFocus(validationResponse, validationResponse.errElement);
			} else {
				vm.errorMap.put(valElement, null);
            	validationResponse.error = false;
            }
        }
        
        /**
         * Common Validation Focus Functionality - starts here" 
         */
  		$scope.validationErrorAndFocus = function(valResponse, elemId){
  			vm.errorMap = new Map();
  			if(valResponse.error == true){
  				vm.errorMap.put(valResponse.errElement, valResponse.errMessage);
  			}
  				$scope.navigateToNextField(elemId);
  		} 
  		
        /**
         * Create and Update New Software Master.
         */
        vm.saveOrUpdate = function() {
            var validationResponse = softwareMasterValidationService.validate(vm.softwareMaster,0);
            if(validationResponse.error == true) {
            	$log.debug("Validation Response -- ", validationResponse);
            	$log.debug('Validation Failed to create Software..')
				$scope.validationErrorAndFocus(validationResponse, validationResponse.errElement);
			}
            else {
            	if(vm.softwareMaster.id== undefined && vm.softwareMaster.id==null){
            		softwareMasterFactory.create.query(vm.softwareMaster).$promise.then(function(data) {
    		            if (data.responseCode == 'ERR0') {
    		            	$log.debug("Software Saved Successfully Response :: ", data);
    		            	Notification.success($rootScope.nls["ERR400"]);
    		            	$state.go('layout.softwareMaster');
    		            } else {
    		                $log.debug("Create Software Failed :" + data.responseDescription)
    		            }

    		        }, function(error) {
    		        	 $log.debug("Create Software Failed : " + error)
    		        });	
            	}else{
                 	softwareMasterFactory.create.query(vm.softwareMaster).$promise.then(function(data) {
     		            if (data.responseCode == 'ERR0') {
     		            	$log.debug("Software Updated Successfully Response :: ", data);
     		            	Notification.success($rootScope.nls["ERR401"]);
     		            	$state.go('layout.softwareMaster');
     		            } else {
     		                $log.debug("Updating Software Failed :" + data.responseDescription)
     		            }
     		        }, function(error) {
     		        	 $log.debug("Updating Software Failed : " + error)
     		        });
                 
            	}
            	
            	
            } 
        };

        /**
         * Recent History - starts here
         */
        vm.isReloaded = localStorage.isSoftwareMasterReloaded;
        $scope.$on('addSoftwareMasterEvent', function(events, args){
        	$rootScope.unfinishedData = vm.softwareMaster;
			$rootScope.category = "Software";
			$rootScope.unfinishedFormTitle = "Software (New)";
        	if(vm.softwareMaster != undefined && vm.softwareMaster != null && vm.softwareMaster.softwareName != undefined && vm.softwareMaster.softwareName != null) {
        		$rootScope.subTitle = vm.softwareMaster.softwareName;
        	} else {
        		$rootScope.subTitle = "Unknown Software "
        	}
        })
      
	    $scope.$on('editSoftwareMasterEvent', function(events, args){
	    	$rootScope.unfinishedData = vm.softwareMaster;
			$rootScope.category = "Software";
			$rootScope.unfinishedFormTitle = "Software Edit # "+vm.softwareMaster.softwareCode;
	    })
	      
	    $scope.$on('addSoftwareMasterEventReload', function(e, confirmation){
	    	    confirmation.message = "";
	    	    localStorage.reloadFormData = JSON.stringify(vm.softwareMaster) ;
	    	    localStorage.isSoftwareMasterReloaded = "YES";
	            e.preventDefault();
	    })
	    
	    $scope.$on('editSoftwareMasterEventReload', function(e, confirmation){
	    	    confirmation.message = "";
	    	    localStorage.reloadFormData = JSON.stringify(vm.softwareMaster) ;
	    	    localStorage.isSoftwareMasterReloaded = "YES";
	            e.preventDefault();
	    	  
	    })
	      


        /**
         * State Identification Handling Here.
         * @param {String} $stateParams.action - VIEW, ADD, EDIT, SEARCH.
         */
        //
        switch ($stateParams.action) {
            case "ADD":
            	 if ($stateParams.fromHistory === 'Yes') {
                     if (vm.isReloaded == "YES") {
                      vm.isReloaded = "NO";
                      localStorage.isSoftwareMasterReloaded = "NO";
                       vm.softwareMaster = JSON.parse(localStorage.reloadFormData);
                     } else {
                       vm.softwareMaster = $rootScope.selectedUnfilledFormData;
                     }
                   } else {
                     if (vm.isReloaded == "YES") {
                       vm.isReloaded = "NO";
                       localStorage.isSoftwareMasterReloaded = "NO";
                       vm.softwareMaster = JSON.parse(localStorage.reloadFormData);
                     } else {
                         $log.debug("Add NotHistory NotReload...")
                     }
                   }
                $rootScope.breadcrumbArr = softwareMasterDataService.getAddBreadCrumb();
                break;
            case "EDIT":
            	 if (vm.fromHistory === 'Yes') {
                     if (vm.isReloaded == "YES") {
                       vm.isReloaded = "NO";
                       localStorage.isSoftwareMasterReloaded = "NO";
                       vm.softwareMaster = JSON.parse(localStorage.reloadFormData);
                     } else {
                       vm.softwareMaster = $rootScope.selectedUnfilledFormData;
                     }
                   } else {
                     if (vm.isReloaded == "YES") {
                       vm.isReloaded = "NO";
                       localStorage.isSoftwareMasterReloaded = "NO";
                       vm.softwareMaster = JSON.parse(localStorage.reloadFormData);
                     } else {
                    	 vm.view($stateParams.id, false);
                     }
                   }
                $rootScope.breadcrumbArr = softwareMasterDataService.getEditBreadCrumb();
                break;
            default:
                $rootScope.breadcrumbArr = softwareMasterDataService.getListBreadCrumb();
                vm.searchInit();
        }

    }
]);
}) ();