app.factory('softwareMasterFactory', function($resource) {
	return {
		findById : $resource('/api/v1/softwaremaster/get/id/:id', {}, {
			 query : {method : 'GET', params : {id : ''}, isArray : false}
		}),
		create : $resource('/api/v1/softwaremaster/create', {}, {
			query : {method : 'POST'}
		}),
		delete : $resource('/api/v1/softwaremaster/delete/:id', {}, {
			query : {method : 'DELETE', params : {id : ''}}
		}),
		search : $resource('/api/v1/softwaremaster/get/search/keyword', {}, {
			query : {method : 'POST'}
		}),
		getall : $resource('/api/v1/softwaremaster/get/search', {}, {
			fetch : {method : 'POST'}
		})
	};
})