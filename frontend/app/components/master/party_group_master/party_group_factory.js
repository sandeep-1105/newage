/**
 * PartyGroupMaster 
 */

(function() {
	
	app.factory("PartyGroupSearchByKeyword",['$resource', function($resource) {
		return $resource("/api/v1/partygroupmaster/get/search/keyword", {}, {
			fetch : {
				method : 'POST'
			}
		});
	}]);
	
	app.factory("PartyGroupGetAll",['$resource', function($resource) {
		return $resource("/api/v1/partygroupmaster/get/all", {}, {
			get : {
				method : 'GET',
				params : {},
				isArray : false
			}
		});
	}]);
	
	
	app.factory("PartyGroupSearch",['$resource', function($resource) {
		return $resource("/api/v1/partygroupmaster/get/search", {}, {
			query : {
				method : 'POST'
			}
		});
	}]);
	
	
	
	app.factory("PartyGroupSave",['$resource', function($resource) {
		return $resource("/api/v1/partygroupmaster/create", {}, {
			save : {
				method : 'POST'
			}
		});
	}]);

	app.factory("PartyGroupUpdate",['$resource', function($resource) {
		return $resource("/api/v1/partygroupmaster/update", {}, {
			update : {
				method : 'POST'
			}
		});
	}]);

	app.factory("PartyGroupView",['$resource', function($resource) {
		return $resource("/api/v1/partygroupmaster/get/id/:id", {}, {
			get : {
				method : 'GET',
				params : {
					id : ''
				},
				isArray : false
			}
		});
	}]);
	
	app.factory("PartyGroupRemove",['$resource', function($resource) {
		return $resource("/api/v1/partygroupmaster/delete/:id", {}, {
			remove : {
				method : 'DELETE',
				params : {
					id : ''
				},
				isArray : false
			}
		});
	}]);
	
	
})();
