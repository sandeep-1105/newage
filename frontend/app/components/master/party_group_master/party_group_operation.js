/**
 * Created by saravanan on 20/9/16.
 */
app.controller('PartyGroupEditController',['$modal', '$rootScope', '$scope', 'ngProgressFactory', '$state', '$stateParams', 'RecentHistorySaveService',
    'PartyGroupView', 'CountryWiseLocation', 'PartyGroupSave', 'PartyGroupUpdate', 'PartiesList', 'discardService', 
    'ngDialog', 'appConstant', 'CommonValidationService', 'roleConstant',
    function($modal, $rootScope, $scope, ngProgressFactory, $state, $stateParams, RecentHistorySaveService,
    PartyGroupView, CountryWiseLocation, PartyGroupSave, PartyGroupUpdate, PartiesList, discardService, 
    ngDialog, appConstant, CommonValidationService, roleConstant) {


    console.log("$stateParams.action ", $stateParams.action);

    $scope.contained_progressbar = ngProgressFactory.createInstance();
    $scope.contained_progressbar.setParent(document.getElementById('partygroup-panel'));
    $scope.contained_progressbar.setAbsolute();

    $scope.initForAdd = function() {

        console.log("Add Page Init method is called...");

        $scope.partyGroupMaster = {};
        $scope.partyGroupMaster.status = 'Active';
        $scope.partyGroupMaster.partyMasterList = [];
        $scope.partyGroupMaster.partyMasterList.push({});

        discardService.set($scope.partyGroupMaster);
    };

    $scope.initForEdit = function() {
        console.log("Edit Page Init method is called...");


        if ($scope.partyGroupMaster.partyMasterList == undefined || $scope.partyGroupMaster.partyMasterList == null || $scope.partyGroupMaster.partyMasterList.length == 0) {
            $scope.partyGroupMaster.partyMasterList.push({});
        }

        if ($scope.partyGroupMaster != undefined && $scope.partyGroupMaster.id != undefined) {
            var editRole = $rootScope.roleAccess(roleConstant.MASTER_CRM_PARTY_GROUP_MAPPING_MODIFY);
            for (var i = 0; i < $scope.partyGroupMaster.partyMasterList.length; i++) {
                if ($scope.partyGroupMaster.partyMasterList[i].id != null) {
                    if (editRole) {
                        $scope.partyGroupMaster.partyMasterList[i].checkRolesToDisable = false;
                    } else {
                        $scope.partyGroupMaster.partyMasterList[i].checkRolesToDisable = true;
                    }
                }
            }
        }
        discardService.set($scope.partyGroupMaster);
    }




    $scope.removeEmptyObject = function() {
        var lastObject = $scope.partyGroupMaster.partyMasterList[$scope.partyGroupMaster.partyMasterList.length - 1];
        if (lastObject.id == undefined || lastObject.id == null) {
            $scope.partyGroupMaster.partyMasterList.splice($scope.partyGroupMaster.partyMasterList.length - 1, 1);
        }
    }

    $scope.savePartyGroup = function() {
        console.log("Save Party Group method is called.");


        if ($scope.validatePartyGroup(0)) {

            $scope.removeEmptyObject();

            $scope.contained_progressbar.start();

            PartyGroupSave.save($scope.partyGroupMaster).$promise.then(function(data) {
                if (data.responseCode == 'ERR0') {
                    console.log("PartyGroupMaster Saved successfully..");
                    var params = {};
                    params.submitAction = 'Saved';
                    $rootScope.successDesc = $rootScope.nls["ERR400"];
                    $state.go("layout.partyGroup", params);
                } else {
                    console.log("PartyGroupMaster Saving Failed " + data.responseDescription)
                }
                $scope.contained_progressbar.complete();
                angular.element(".panel-body").animate({
                    scrollTop: 0
                }, "slow");
            }, function(error) {
                console.log("PartyGroupMaster Saving Failed : " + error)
            });

        } else {
            console.log("Error while validating the party group..");
        }

    }



    $scope.updatePartyGroup = function() {
        console.log("Update Party Group method is called.");

        if ($scope.validatePartyGroup(0)) {
            $scope.contained_progressbar.start();

            $scope.removeEmptyObject();

            PartyGroupUpdate.update($scope.partyGroupMaster).$promise.then(function(data) {
                if (data.responseCode == 'ERR0') {
                    console.log("Party Group Master updated Successfully")
                    var params = {};
                    params.submitAction = 'Saved';
                    $rootScope.successDesc = $rootScope.nls["ERR401"];
                    $state.go("layout.partyGroup", params);
                } else {
                    console.log("PartyGroupMaster Updation Failed", data.responseDescription)
                }
                $scope.contained_progressbar.complete();
                angular.element(".panel-body").animate({
                    scrollTop: 0
                }, "slow");
            }, function(error) {
                console.log("PartyGroupMaster Updation Failed", error)
            });

        } else {
            console.log("Error while validating the party group..");
        }

    }


    $scope.setStatus = function(data) {
        if (data == 'Active') {
            return 'activetype';
        } else if (data == 'Block') {
            return 'blockedtype';
        } else if (data == 'Hide') {
            return 'hiddentype';
        }
    }

    $scope.cancel = function() {
        console.log("Cancel Button is Pressed....");


        if (!discardService.compare($scope.partyGroupMaster)) {
            ngDialog.openConfirm({
                template: '<p>Do you want to Save your changes?</p>' +
                    '<div class="ngdialog-footer">' +
                    '<button type="button" class="btn btn-default btn-property cancel-btn" ng-click="confirm(2)">No</button>' +
                    '<button type="button" class="btn btn-default btn-property accent-btn " ng-click="confirm(1)">Yes</button>' +
                    '<button type="button" class="btn btn-default btn-property pull-left grey-cancel-btn" ng-click="confirm(3)">Cancel</button>' +
                    '</div>',
                plain: true,
                className: 'ngdialog-theme-default'
            }).
            then(function(value) {
                if (value == 1 && $scope.partyGroupMaster.id == null) {
                    $scope.savePartyGroup();
                } else if (value == 1 && $scope.partyGroupMaster.id != null) {
                    $scope.updatePartyGroup();
                } else if (value == 2) {
                    var params = {};
                    params.submitAction = 'Cancelled';
                    $state.go("layout.partyGroup", params);
                } else {
                    console.log("Nothing selected..Cancel")
                }
            });

        } else {
            var params = {};
            params.submitAction = 'Cancelled';
            $state.go("layout.partyGroup", params);
        }

    };


    /* Location Lov */

    $scope.locationRender = function(item) {
        return {
            label: item.locationName,
            item: item
        }
    };

    $scope.ajaxLocationEvent = function(object) {

        console.log("ajaxLocationEvent is called ", object)

        $scope.searchDto = {};
        $scope.searchDto.keyword = object == null ? "" : object;
        $scope.searchDto.selectedPageNumber = 0;
        $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
        return CountryWiseLocation.fetch({
            "countryId": $rootScope.userProfile.selectedUserLocation.countryMaster.id
        }, $scope.searchDto).$promise.then(
            function(data) {
                if (data.responseCode == "ERR0") {
                    $scope.locationList = data.responseObject.searchResult;
                    console.log("$scope.locationList ", $scope.locationList);
                    return $scope.locationList;
                }
            },
            function(errResponse) {
                console.error('Error while fetching  Locations');
            }
        );

    }

    $scope.selectedLocation = function(object, nextIdValue) {
        console.log("Selected Location ", object);
        if ($scope.validatePartyGroup(3)) {
            if (nextIdValue != undefined)
                $rootScope.navigateToNextField(nextIdValue);
        }
    }



    $scope.ajaxPartyEvent = function(object) {
        console.log("Ajax Party Event method : ", object);

        $scope.searchDto = {};
        $scope.searchDto.keyword = object == null ? "" : object;
        $scope.searchDto.selectedPageNumber = 0;
        $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

        return PartiesList.query($scope.searchDto).$promise.then(function(data, status) {
                if (data.responseCode == "ERR0") {
                    $scope.partiesList = data.responseObject.searchResult;
                    console.log("$scope.partiesList ", $scope.partiesList);
                    return $scope.partiesList;
                }
            },
            function(errResponse) {
                console.error('Error while fetching Party');
            }
        );
    };

    $scope.partyRender = function(item) {
        return {
            label: item.partyName,
            item: item
        }
    };

    $scope.selectedParty = function(object) {
        console.log("Selected Party is Running....", object);

        console.log("$scope.partyGroupMaster.partyMasterList ", $scope.partyGroupMaster.partyMasterList);
    }




    $scope.validatePartyGroup = function(validateCode) {

        console.log("validatePartyGroup ", validateCode);

        $scope.errorMap = new Map();

        if (validateCode == 0 || validateCode == 1) {

            if ($scope.partyGroupMaster.partyGroupName == null ||
                $scope.partyGroupMaster.partyGroupName == undefined ||
                $scope.partyGroupMaster.partyGroupName == "") {
                console.log($rootScope.nls["ERR5007"]);
                $scope.errorMap.put("partyGroupName", $scope.nls["ERR5007"]);
                return false
            } else {
                if (!CommonValidationService.checkRegExp(appConstant.Reg_Exp_Master_PARTY_GROUP_NAME, $scope.partyGroupMaster.partyGroupName)) {
                    $scope.errorMap.put("partyGroupName", $rootScope.nls["ERR5005"]);
                    return false;
                }
            }
        }



        if (validateCode == 0 || validateCode == 2) {

            if ($scope.partyGroupMaster.partyGroupCode == null ||
                $scope.partyGroupMaster.partyGroupCode == undefined ||
                $scope.partyGroupMaster.partyGroupCode == "") {
                console.log($rootScope.nls["ERR5006"]);
                $scope.errorMap.put("partyGroupCode", $scope.nls["ERR5006"]);
                return false

            } else {

                if (!CommonValidationService.checkRegExp(appConstant.Reg_Exp_Master_PARTY_GROUP_CODE, $scope.partyGroupMaster.partyGroupCode)) {
                    $scope.errorMap.put("partyGroupCode", $rootScope.nls["ERR5004"]);
                    return false;
                }
            }
        }



        if (validateCode == 0 || validateCode == 2) {


            if ($scope.partyGroupMaster.salesLeadLocation == null ||
                $scope.partyGroupMaster.salesLeadLocation == undefined ||
                $scope.partyGroupMaster.salesLeadLocation == "") {
                // $scope.partyGroupMaster.salesLeadLocation is not mandatory
            } else {
                if ($scope.partyGroupMaster.salesLeadLocation.status != undefined && $scope.partyGroupMaster.salesLeadLocation.status != null) {
                    if ($scope.partyGroupMaster.salesLeadLocation.status == 'Block') {
                        $scope.partyGroupMaster.salesLeadLocation = null;
                        console.log($rootScope.nls["ERR5008"]);
                        $scope.errorMap.put("partyGroupLocation", $rootScope.nls["ERR5008"]);
                        return false;
                    }
                    if ($scope.partyGroupMaster.salesLeadLocation.status == 'Hide') {
                        $scope.partyGroupMaster.salesLeadLocation = null;
                        console.log($rootScope.nls["ERR5009"]);
                        $scope.errorMap.put("partyGroupLocation", $rootScope.nls["ERR5009"]);
                        return false;
                    }
                }
            }
        }


        return true;

    }




    //show errors

    var myOtherModal = $modal({
        scope: $scope,
        templateUrl: 'errorPopUp.html',
        show: false,
        keyboard: true
    });

    var errorOnRowIndex = null;

    $scope.errorShow = function(errorObj, index) {
        $scope.errList = errorObj.errTextArr;
        errorOnRowIndex = index;
        myOtherModal.$promise.then(myOtherModal.show);
    };


    $scope.addNewParty = function() {
        if ($rootScope.roleAccess(roleConstant.MASTER_CRM_PARTY_GROUP_MAPPING_CREATE)) {
            console.log("Add Link Party Button is pressed.....", $scope.partyGroupMaster.partyMasterList);

            var fineArr = [];
            $scope.secondArr = [];
            angular.forEach($scope.partyGroupMaster.partyMasterList, function(partyMaster, index) {

                $scope.secondArr[index] = {};

                var validResult = $scope.validatePartyMapping(partyMaster, index, "row");

                if (validResult) {
                    fineArr.push(1);
                    $scope.secondArr[index].errRow = false;
                } else {
                    $scope.secondArr[index].errRow = true;
                    fineArr.push(0);
                }

            });

            if (fineArr.indexOf(0) < 0) {

                $scope.partyGroupMaster.partyMasterList.push({});
                $scope.secondArr.push({
                    "errRow": false
                });

            }
        }
    }

    $scope.removeParty = function(index) {
        if ($rootScope.roleAccess(roleConstant.MASTER_CRM_PARTY_GROUP_MAPPING_DELETE)) {
            console.log("removeParty is called ", index);
            $scope.partyGroupMaster.partyMasterList.splice(index, 1);
        }
    }



    $scope.validatePartyMapping = function(dataObj, index, type) {
        console.log("ValidatePartyMapping ", dataObj, index, type);


        var errorArr = [];


        if (type == "row") {

            $scope.secondArr[index].errTextArr = [];

        }

        if (dataObj == null || dataObj.id == undefined || dataObj.id == null) {
            if (type == "row") {
                $scope.secondArr[index].partyName = true;
                $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR5012"]);
            }
            //return false;
            errorArr.push(0);
        } else {
            if (dataObj.status == 'Block') {
                if (type == "row") {
                    $scope.secondArr[index].partyName = true;
                    $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR5010"]);
                }
                //return false;
                errorArr.push(0);
            } else if (dataObj.status == 'Hide') {
                if (type == "row") {
                    $scope.secondArr[index].partyName = true;
                    $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR5011"]);
                }
                //return false;
                errorArr.push(0);
            } else {
                if (type == "row") {
                    $scope.secondArr[index].partyName = false;
                }
                //return false;
                errorArr.push(1);
            }

        }
        if (errorArr.indexOf(0) < 0) {
            return true;
        } else {
            return false;
        }
    }




    $scope.getPartyGroupMaster = function(partyGroupId) {
        console.log("getPartyGroupMaster is called ", partyGroupId);


        PartyGroupView.get({
            id: partyGroupId
        }, function(data) {
            if (data.responseCode == "ERR0") {

                $scope.partyGroupMaster = data.responseObject;

                $scope.initForEdit();

                if ($scope.partyGroupMaster != undefined && $scope.partyGroupMaster != null && $scope.partyGroupMaster.partyGroupName != undefined && $scope.partyGroupMaster.partyGroupName != null) {
                    $rootScope.subTitle = $scope.partyGroupMaster.partyGroupName;
                } else {
                    $rootScope.subTitle = "Unknown Party Group"
                }

                var rHistoryObj = {
                    'title': 'Party Group Edit #' + $stateParams.partyGroupId,
                    'subTitle': $rootScope.subTitle,
                    'stateName': $state.current.name,
                    'stateParam': JSON.stringify($stateParams),
                    'stateCategory': 'Party Group',
                    'serviceType': 'AIR'
                }
                RecentHistorySaveService.form(rHistoryObj);

            } else {
                console.log("Party Group Get Failed ", data.responseDescription)
            }
        }, function(error) {
            console.log("Party Group Get Failed : ", error)
        });
    };




    //On leave the Unfilled Form Broadcast then event will happen in StateChangeListener(BroadCaster)- app_state_route.js
    $scope.$on('addPartyGroupEvent', function(events, args) {

        $rootScope.unfinishedData = $scope.partyGroupMaster;
        $rootScope.category = "Party Group";
        $rootScope.unfinishedFormTitle = "Party Group (New)";

        if ($scope.partyGroupMaster != undefined &&
            $scope.partyGroupMaster != null &&
            $scope.partyGroupMaster.partyGroupCode != undefined &&
            $scope.partyGroupMaster.partyGroupCode != null &&
            $scope.partyGroupMaster.partyGroupCode != "") {

            $rootScope.subTitle = $scope.partyGroupMaster.partyGroupCode;
        } else {
            $rootScope.subTitle = "Unknown Party Group"
        }
    });

    $scope.$on('editPartyGroupEvent', function(events, args) {

        $rootScope.unfinishedData = $scope.partyGroupMaster;
        $rootScope.category = "Party Group";
        $rootScope.unfinishedFormTitle = "Party Group Edit # " + $scope.partyGroupMaster.id;

        if ($scope.partyGroupMaster != undefined &&
            $scope.partyGroupMaster != null &&
            $scope.partyGroupMaster.partyGroupCode != undefined &&
            $scope.partyGroupMaster.partyGroupCode != null &&
            $scope.partyGroupMaster.partyGroupCode != "") {

            $rootScope.subTitle = $scope.partyGroupMaster.partyGroupCode;

        } else {
            $rootScope.subTitle = "Unknown Party Group"
        }
    });

    $scope.$on('addPartyGroupEventReload', function(e, confirmation) {
        confirmation.message = "";
        localStorage.reloadFormData = JSON.stringify($scope.partyGroupMaster);
        localStorage.isPartyGroupReloaded = "YES";
        e.preventDefault();
    });

    $scope.$on('editPartyGroupEventReload', function(e, confirmation) {
        confirmation.message = "";
        localStorage.reloadFormData = JSON.stringify($scope.partyGroupMaster);
        localStorage.isPartyGroupReloaded = "YES";
        e.preventDefault();
    });




    // State Routing Configuration
    $scope.isReloaded = localStorage.isPartyGroupReloaded;
    if ($stateParams.action == "ADD") {

        console.log("Party Group Master Add Page.");

        if ($stateParams.fromHistory === 'Yes') {
            if ($scope.isReloaded == "YES") {
                $scope.isReloaded = "NO";
                localStorage.isPartyGroupReloaded = "NO";
                $scope.partyGroupMaster = JSON.parse(localStorage.reloadFormData);
            } else {
                $scope.partyGroupMaster = $rootScope.selectedUnfilledFormData;
            }
            $scope.initForEdit();
        } else {
            if ($scope.isReloaded == "YES") {
                $scope.isReloaded = "NO";
                localStorage.isPartyGroupReloaded = "NO";
                if (localStorage.reloadFormData != undefined) {
                    $scope.partyGroupMaster = JSON.parse(localStorage.reloadFormData);
                }
            } else {
                $scope.initForAdd();
            }
        }
        $rootScope.breadcrumbArr = [{
                label: "Master",
                state: "layout.partyGroup"
            },
            {
                label: $rootScope.appMasterData['Party_Label'] + ' Group',
                state: "layout.partyGroup"
            },
            {
                label: 'Add ' + $rootScope.appMasterData['Party_Label'] + ' Group',
                state: null
            }
        ];
    } else {

        console.log("Party Group Master Edit Page.")


        if ($stateParams.fromHistory === 'Yes') {
            if ($scope.isReloaded == "YES") {
                $scope.isReloaded = "NO";
                localStorage.isPartyGroupReloaded = "NO";
                $scope.partyGroupMaster = JSON.parse(localStorage.reloadFormData);
            } else {
                $scope.partyGroupMaster = $rootScope.selectedUnfilledFormData;
            }
            $scope.initForEdit();
        } else {
            if ($scope.isReloaded == "YES") {
                $scope.isReloaded = "NO";
                localStorage.isPartyGroupReloaded = "NO";
                $scope.partyGroupMaster = JSON.parse(localStorage.reloadFormData);
                $scope.initForEdit();
            } else {
                $scope.getPartyGroupMaster($stateParams.partyGroupId);
            }
        }
        $rootScope.breadcrumbArr = [{
                label: "Master",
                state: "layout.partyGroup"
            },
            {
                label: $rootScope.appMasterData['Party_Label'] + ' Group',
                state: "layout.partyGroup"
            },
            {
                label: 'Edit ' + $rootScope.appMasterData['Party_Label'] + ' Group',
                state: null
            }
        ];
    }

}]);