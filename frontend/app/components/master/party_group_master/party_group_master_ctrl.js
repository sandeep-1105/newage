app.controller('PartyGroupController',['$scope', '$location', '$window', 'ngDialog', '$state', '$modal', '$stateParams', '$rootScope', 
        'PartyGroupSearch', 'PartyGroupView', 'PartyGroupRemove', 'RecentHistorySaveService', 'roleConstant',
    function($scope, $location, $window, ngDialog, $state, $modal, $stateParams, $rootScope, 
        PartyGroupSearch, PartyGroupView, PartyGroupRemove, RecentHistorySaveService, roleConstant) {

        /************************************************************
         * ux - by Muthu
         * reason - ipad compatibility fix
         *
         * *********************************************************/
        $scope.deskTopView = true;

        /********************* ux fix ends here *****************************/


        $scope.limitArr = [10, 15, 20];
        $scope.page = 0;
        $scope.limit = 10;

        // Super Table Head Array
        $scope.partyGroupHeadArr = [{
            "name": "#",
            "width": "w50px",
            "model": "no",
            "search": false

        }, {
            "name": "Name",
            "width": "w277px",
            "model": "partyGroupName",
            "search": true,
            "wrap_cell": true,
            "type": "text",
            "sort": true,
            "key": "searchPartyGroupName"

        }, {
            "name": "Code",
            "width": "w100px",
            "model": "partyGroupCode",
            "search": true,
            "type": "text",
            "sort": true,
            "key": "searchPartyGroupCode"

        }, {
            "name": "Sales Lead Location",
            "width": "w175px",
            "model": "salesLeadLocation.locationName",
            "search": true,
            "type": "text",
            "sort": true,
            "key": "searchSalesLeadLocation"

        }, {
            "name": "Status",
            "width": "w175px",
            "model": "status",
            "search": true,
            "type": "drop",
            "sort": true,
            "key": "searchStatus",
            "data": $rootScope.enum['LovStatus']
        }];


        /*    $scope.partyGroupArr = [
						{
							"no" : 1,
							"partyGroupName" : "TVS Chennai",
							"partyGroupCode" : "C100101",
							"salesLeadCity" : "Chennai",
							"status": "Active"
					    },
						{
							"no" : 2,
							"partyGroupName" : "TechnoSoft Gadgets",
							"partyGroupCode" : "C100102",
							"salesLeadCity" : "Chennai",
							"status": "Active"
						},
						{
							"no" : 3,
							"partyGroupName" : "Raghuram R",
							"partyGroupCode" : "C200101",
							"salesLeadCity" : "Delhi",
							"status": "Active"
						},
						{
							"no" : 4,
							"partyGroupName" : "All Dubai ",
							"partyGroupCode" : "C100104",
							"salesLeadCity" : "Kolkatta",
							"status": "Active"
						}
					];*/


        $scope.init = function() {
            console.log("Init method is called......");

            $rootScope.setNavigate1("Master");
            $rootScope.setNavigate2("Party Group");

            $scope.searchDto = {};
            $scope.search();
        }



        // Super Table Sorting Default Configuration
        $scope.sortSelection = {
            sortKey: "partyGroupName",
            sortOrder: "asc"
        };



        $scope.changePage = function(param) {
            $scope.page = param.page;
            $scope.limit = param.size;
            $scope.search();
        }

        $scope.currentTab = "address";
        $scope.activeTab = function(tab) {
            $scope.currentTab = tab;
        }
        $scope.view = function(selectedMaster) {
            console.log("View Charge Called");
            $scope.chargeMaster = selectedMaster;
        };
        $scope.limitChange = function(item) {
            $scope.page = 0;
            $scope.limit = item;
            $scope.search();
        }



        $scope.sortChange = function(param) {
            $scope.searchDto.orderByType = param.sortOrder.toUpperCase();
            $scope.searchDto.sortByColumn = param.sortKey;
            $scope.search();
        };




        $scope.changeSearch = function(param) {

            console.log("Change Search is called ", param);

            for (var attrname in param) {
                $scope.searchDto[attrname] = param[attrname];
            }

            $scope.page = 0;
            $scope.search();
        }

        $scope.rowSelect = function(data, index) {
            if ($rootScope.roleAccess(roleConstant.MASTER_CRM_PARTY_GROUP_VIEW)) {
                $scope.selectedRecordIndex = index;

                var param = {
                    partyGroupId: data.id,
                    partyGroupIndex: index
                };

                $state.go("layout.viewPartyGroup", param);




                /************************************************************
                 * ux - by Muthu
                 * reason - ipad compatibility fix
                 *
                 * *********************************************************/

                var windowInner = $window.innerWidth;
                if (windowInner <= 1199) {
                    $scope.deskTopView = false;
                } else {
                    $scope.deskTopView = true;
                }
            }
        }

        $scope.clickOnTabView = function(tab) {
            if (tab == 'mapping') {
                if ($rootScope.roleAccess(roleConstant.MASTER_CRM_PARTY_GROUP_MAPPING_VIEW)) {
                    $scope.Tabs = tab;
                }
            }
            if (tab == 'business') {
                if ($rootScope.roleAccess(roleConstant.MASTER_CRM_PARTY_GROUP_BUSINESS_DETAILS_VIEW)) {
                    $scope.Tabs = tab;
                }
            }

        }

        $scope.cancel = function() {

            console.log("Cancel Button is pressed.....");

            $scope.showDetail = false;
            $scope.showHistory = false;

            $state.go("layout.partyGroup");

            /************************************************************
             * ux - by Muthu
             * reason - ipad compatibility fix
             *
             * *********************************************************/
            $scope.deskTopView = true;

            /********************* ux fix ends here *****************************/
        };

        $scope.search = function() {

            console.log("Search is called.......");

            $scope.searchDto.selectedPageNumber = $scope.page;
            $scope.searchDto.recordPerPage = $scope.limit;



            console.log("$scope.searchDto ", $scope.searchDto);

            PartyGroupSearch.query($scope.searchDto).$promise.then(
                function(data, status) {

                    $scope.totalRecord = data.responseObject.totalRecord;

                    var tempArr = [];
                    var resultArr = [];

                    tempArr = data.responseObject.searchResult;

                    console.log("tempArr", tempArr);

                    var tempObj = {};

                    angular.forEach(tempArr, function(item, index) {
                        tempObj = item;
                        tempObj.no = (index + 1) + ($scope.page * $scope.limit);
                        resultArr.push(tempObj);
                        tempObj = {};
                    });

                    $scope.partyGroupArr = resultArr;

                    console.log("$scope.partyGroupArr ", $scope.partyGroupArr);
                });
        }




        $scope.serialNo = function(index) {
            index = ($scope.searchDto.selectedPageNumber * $scope.searchDto.recordPerPage) +
                (index + 1);
            return index;
        }



        var myOtherModal = $modal({
            scope: $scope,
            templateUrl: 'app/components/master/party_group_master/views/viewShipmentPopup.html',
            show: false
        });

        $scope.viewShipment = function() {

            myOtherModal.$promise.then(myOtherModal.show);

        }


        $scope.view = function(partyGroupId) {

            console.log("View Party Group is called ", partyGroupId);


            PartyGroupView.get({
                id: partyGroupId
            }, function(data) {

                if (data.responseCode == "ERR0") {

                    console.log("Party Group Successful");

                    $scope.partyGroupMaster = data.responseObject;

                    if ($scope.partyGroupMaster != undefined && $scope.partyGroupMaster != null && $scope.partyGroupMaster.partyGroupName != undefined && $scope.partyGroupMaster.partyGroupName != null) {
                        $rootScope.subTitle = $scope.partyGroupMaster.partyGroupName;
                    }

                    // To display the view page
                    $scope.showDetail = true;


                    var rHistoryObj = {
                        'title': 'Party Group View #' + $stateParams.partyGroupId,
                        'subTitle': $rootScope.subTitle,
                        'stateName': $state.current.name,
                        'stateParam': JSON.stringify($stateParams),
                        'stateCategory': 'PartyGroup',
                        'serviceType': 'AIR'
                    }

                    RecentHistorySaveService.form(rHistoryObj);


                    console.log("Viewing Party Group Master ", $scope.partyGroupMaster);

                } else {
                    console.log("Party Group View Failed ", data.responseDescription)
                }
            }, function(error) {
                console.log("Party Group View Failed : ", error);
            });
        }

        $scope.addPartyGroup = function() {
            if ($rootScope.roleAccess(roleConstant.MASTER_CRM_PARTY_GROUP_CREATE)) {
                console.log("Add Party Group Button is pressed....");
                $state.go("layout.addPartyGroup");
            }
        };


        $scope.editPartyGroup = function() {
            if ($rootScope.roleAccess(roleConstant.MASTER_CRM_PARTY_GROUP_MODIFY)) {
                console.log("Edit Party Group Master Button is Pressed....")

                var param = {
                    partyGroupId: $scope.partyGroupMaster.id
                };
                console.log("State Parameters :: ", param);
                $state.go("layout.editPartyGroup", param);
            }
        }


        $scope.deletePartyGroup = function() {
            if ($rootScope.roleAccess(roleConstant.MASTER_CRM_PARTY_GROUP_DELETE)) {
                console.log("Delete Party Group Master Button is Pressed....")
                var newScope = $scope.$new();
                newScope.errorMessage = $rootScope.nls["ERR239"];
                ngDialog.openConfirm({
                    template: '<p>{{errorMessage}}</p>' +
                        '<div class="ngdialog-footer">' +
                        '<button type="button" class="btn btn-default btn-property cancel-btn" ng-click="closeThisDialog(0)">Cancel</button>' +
                        '<button type="button" class="btn btn-primary btn-property accent-btn" ng-click="confirm(1)">Yes</button></div>',
                    plain: true,
                    scope: newScope,
                    className: 'ngdialog-theme-default'
                }).then(
                    function(value) {
                        PartyGroupRemove.remove({
                                id: $scope.partyGroupMaster.id
                            }, function(data) {
                                if (data.responseCode == 'ERR0') {
                                    console.log("PartyGroupMaster Deleted Successfully")
                                    $state.go("layout.partyGroup");
                                } else {
                                    console.log("PartyGroupMaster deletion Failed ", data.responseDescription)
                                }
                            },
                            function(error) {
                                console.log("PartyGroupMaster deletion Failed ", data.responseDescription)
                            });
                    },
                    function(value) {
                        console.log("Deletion is Cancelled");
                    });
            }
        }

        switch ($stateParams.action) {
            case "VIEW":
                console.log("PartyGroupMaster View Page", $stateParams.partyGroupId, $stateParams.partyGroupIndex);

                $rootScope.unfinishedFormTitle = "Party Group # " + $stateParams.partyGroupId;
                $rootScope.unfinishedData = undefined;
                $scope.view($stateParams.partyGroupId);
                $rootScope.breadcrumbArr = [{
                        label: "Master",
                        state: "layout.partyGroup"
                    },
                    {
                        label: $rootScope.appMasterData['Party_Label'] + ' Group',
                        state: "layout.partyGroup"
                    },
                    {
                        label: 'View ' + $rootScope.appMasterData['Party_Label'] + ' Group',
                        state: null
                    }
                ];
                break;
            case "SEARCH":
                console.log("PartyGroupMaster Search Page");
                $scope.init();
                $rootScope.breadcrumbArr = [{
                        label: "Master",
                        state: "layout.partyGroup"
                    },
                    {
                        label: $rootScope.appMasterData['Party_Label'] + ' Group',
                        state: "layout.partyGroup"
                    }
                ];
                break;
            default:

        }

    }]);