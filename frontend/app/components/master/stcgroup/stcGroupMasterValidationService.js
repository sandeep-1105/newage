app.service('stcGroupMasterValidationService', ['$rootScope','ValidateUtil', 'CommonValidationService', 'appConstant', function($rootScope, ValidateUtil, CommonValidationService, appConstant) {
	this.validate = function(stcGroupMaster, code) {
		//Validate stcGroupMaster service code
		if(code == 0 || code == 1) {
			if(stcGroupMaster.serviceMaster == undefined || stcGroupMaster.serviceMaster == null || stcGroupMaster.serviceMaster =="" ){
				return this.validationResponse(true, "serviceName", $rootScope.nls["ERR1408"], stcGroupMaster);
			}
			else if(stcGroupMaster.serviceMaster.serviceCode == undefined || stcGroupMaster.serviceMaster.serviceCode == null || stcGroupMaster.serviceMaster.serviceCode =="" ){
				return this.validationResponse(true, "serviceName", $rootScope.nls["ERR1408"], stcGroupMaster);
			}else if(ValidateUtil.isStatusBlocked(stcGroupMaster.serviceMaster.status)){
	  				return this.validationResponse(true, "serviceName", $rootScope.nls["ERR1422"], stcGroupMaster);
			}
		}
		//Validate StcGroupMaster charge code
		if(code == 0 || code == 2) {
			if(stcGroupMaster.chargeMaster == undefined || stcGroupMaster.chargeMaster == null || stcGroupMaster.chargeMaster =="" ){
				return this.validationResponse(true, "chargeName", $rootScope.nls["ERR2310"], stcGroupMaster);
			}
			else if(stcGroupMaster.chargeMaster.chargeCode == undefined || stcGroupMaster.chargeMaster.chargeCode == null || stcGroupMaster.chargeMaster.chargeCode =="" ){
				return this.validationResponse(true, "chargeName", $rootScope.nls["ERR2310"], stcGroupMaster);
			}else if(ValidateUtil.isStatusBlocked(stcGroupMaster.chargeMaster.status)){
	  				return this.validationResponse(true, "chargeName", $rootScope.nls["ERR1422"], stcGroupMaster);
			   }
		}
		
		//Validate StcGroupMaster Category code
		if(code == 0 || code == 3) {
			if(stcGroupMaster.categoryMaster == undefined || stcGroupMaster.categoryMaster == null || stcGroupMaster.categoryMaster =="" ){
				return this.validationResponse(true, "categoryName", $rootScope.nls["ERR1307"], stcGroupMaster);
			}
			else if(stcGroupMaster.categoryMaster.categoryCode == undefined || stcGroupMaster.categoryMaster.categoryCode == null || stcGroupMaster.categoryMaster.categoryCode =="" ){
				return this.validationResponse(true, "categoryName", $rootScope.nls["ERR1307"], stcGroupMaster);
			}else if(ValidateUtil.isStatusBlocked(stcGroupMaster.categoryMaster.status)){
	  				return this.validationResponse(true, "categoryName", $rootScope.nls["ERR3516"], stcGroupMaster);
			   }
		}
		return this.validationSuccesResponse(stcGroupMaster);
	}
		
 
	this.validationSuccesResponse = function(stcGroupMaster) {
	    return {error : false, obj : stcGroupMaster};
	}
	
	this.validationResponse = function(err, elem, message, stcGroupMaster) {
		return {error : err, errElement : elem, errMessage : message, obj : stcGroupMaster};
	}
	
	
}]);