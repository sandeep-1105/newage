(function(){
app.controller("STCGroupMasterCtrl", ['$scope', '$rootScope', '$window', '$modal', '$state', '$stateParams', '$log', 'ngDialog', 'stcGroupMasterDataService', 'stcGroupMasterFactory', 'RecentHistorySaveService', 'stcGroupMasterValidationService', 'Notification','roleConstant',
    function($scope, $rootScope, $window, $modal, $state, $stateParams, $log, ngDialog, stcGroupMasterDataService, stcGroupMasterFactory, RecentHistorySaveService, stcGroupMasterValidationService, Notification, roleConstant ) {
	
	
	  var vm = this;
      vm.tableHeadArr = stcGroupMasterDataService.getHeadArray();
      vm.dataArr = [];
      vm.searchData = {};
      vm.stcGroupMaster = {};
      vm.limitArr = [10, 15, 20];
      vm.page = 0;
      vm.limit = 10;
      vm.totalRecord = 100;
      vm.showDetail=false;
      vm.showHistory = false;
      vm.errorMap = new Map();
      vm.deskTopView = true;

      vm.sortSelection = {
          sortKey: "serviceMaster.serviceName",
          sortOrder: "asc"
      }
      
      vm.changeSearch = function(param) {
          $log.debug("Change Search is called." + param);
          for (var attrname in param) {
              vm.searchData[attrname] = param[attrname];
          }
          vm.page = 0;
          vm.search();
      }

      vm.sortChange = function(param) {
          $log.debug("sortChange is called. " + param);
          vm.searchData.orderByType = param.sortOrder.toUpperCase();
          vm.searchData.sortByColumn = param.sortKey;
          vm.search();
      }
      
      
      /**
       * STCGroup Master List Page Search Data  initializing and calling search Mtd.
       */
      vm.searchInit = function() {
          vm.searchData = {};
          vm.searchData.orderByType = vm.sortSelection.sortOrder.toUpperCase();
          vm.searchData.sortByColumn = vm.sortSelection.sortKey;
          vm.search();
      };
      

      /**
       * Get servicetaxchargegroup Master's Based on Search Data.
       */
      vm.search = function() {
      	vm.stcGroupMasterView=false;
          vm.searchData.selectedPageNumber = vm.page;
          vm.searchData.recordPerPage = vm.limit;
          vm.dataArr = [];
          stcGroupMasterFactory.search.fetch(vm.searchData).$promise.then(
              function(data) {
                  if (data.responseCode == "ERR0") {
                      vm.totalRecord = data.responseObject.totalRecord;
                      vm.dataArr = data.responseObject.searchResult;
                  }
              },
              function(errResponse) {
              	 $log.error('Error while fetching ServiceTaxChargeGroupMaster');
              });
         }
      
      /* Master Select Picker */
      vm.ajaxServiceEvent = function(object) {
          vm.searchDto = {};
          vm.searchDto.keyword = object == null ? "" : object;
          vm.searchDto.selectedPageNumber = 0;
          vm.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
          return stcGroupMasterFactory.searchByService.query(vm.searchDto).$promise.then(function(data, status) {
                  if (data.responseCode == "ERR0") {
                      vm.serviceMasterList = data.responseObject.searchResult;
                      return vm.serviceMasterList;
                  }
              },
              function(errResponse) {
                  console.error('Error while fetching Service Master');
              }
          );

      }
      
      
      /* Master Select Picker */
      vm.ajaxChargeEvent = function(object) {
          vm.searchDto = {};
          vm.searchDto.keyword = object == null ? "" : object;
          vm.searchDto.selectedPageNumber = 0;
          vm.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
          return stcGroupMasterFactory.searchByCharge.query(vm.searchDto).$promise.then(function(data, status) {
                  if (data.responseCode == "ERR0") {
                      vm.chargeMasterList = data.responseObject.searchResult;
                      return vm.chargeMasterList;
                  }
              },
              function(errResponse) {
                  console.error('Error while fetching charge Master');
              }
          );

      }
      
      
      /* Master Select Picker */
      vm.ajaxCategoryEvent = function(object) {
          vm.searchDto = {};
          vm.searchDto.keyword = object == null ? "" : object;
          vm.searchDto.selectedPageNumber = 0;
          vm.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
          return stcGroupMasterFactory.searchByCategory.query(vm.searchDto).$promise.then(function(data, status) {
                  if (data.responseCode == "ERR0") {
                      vm.categoryMasterList = data.responseObject.searchResult;
                      return vm.categoryMasterList;
                  }
              },
              function(errResponse) {
                  console.error('Error while fetching category Master');
              }
          );

      }
      
      vm.selectedMaster = function(obj,master){
    	  
    	  var validationResponse=stcGroupMasterValidationService.validate(master);
    	  if(validationResponse.error==true){
    		  $scope.validationErrorAndFocus(validationResponse, validationResponse.errElement);
    	  }
    	  else{
    		  validationResponse=false;
    		  vm.errorMap.put(obj, null);
    		  $rootScope.navigateToNextField(obj);
    	  }
      }
      
     
      
      
      
     /* *//**
       * Reason Master List Page Search Data  initializing and calling search Mtd.
       * @param {Object} data - The Selected Reason Master from List page.
       * @param {string} data.id - The id of the Selected Reason Master.
       * @param {int} index - The Selected row index from List page.
       *//*
      
      
      *//**
       * Modify Existing Reason Master.
       * @param {int} id - The id of a Country.
       *//*
      vm.edit = function(id) {
          $state.go("layout.reasonMasterEdit", {
              id: id
          });
      }
      
      // set status of reason master
      
      vm.setStatus = function(data){
  		if(data=='Active'){
  			return 'activetype';
  		} else if(data=='Block'){
  			return 'blockedtype';
  		}else if(data=='Hide'){
  			return 'hiddentype';
  		}
  	}
  	*/
      
      
      /**
       * Go Back to reason Master List Page.
       */
      
      vm.backToList = function() {
    	  //$state.go("layout.stcgroupmaster");
    	  vm.stcGroupMasterView=false;
    	  vm.stcGroupMaster = {};
    	  vm.deskTopView = true;
        }
      
      
      /**
       * Go To Add ServiceTaxChargeGroupMaster.
       */
      
      vm.addNew = function() {
    	  if($rootScope.roleAccess(roleConstant.MASTER_FINANCE_SERVICE_CHARGE_MAPPING_CREATE)){
    		  $state.go("layout.stcgroupmasterAdd");
    	  }
      }
      
      
      /**
       * Cancel Add or Edit ServiceTaxChargeGroupMaster.
       * @param {int} id - The id of a ServiceTaxChargeGroupMaster
       */
     
      
      vm.edit = function(id) {
    	  if($rootScope.roleAccess(roleConstant.MASTER_FINANCE_SERVICE_CHARGE_MAPPING_MODIFY)){
    		  $state.go("layout.stcgroupmasterEdit", {
                  id: id
              });
    	  }
      }
      
      
      /**
       * Delete ServiceTaxChargeGroupMaster.
       * @param {int} id - The id of a ServiceTaxChargeGroupMaster.
       */
      
      vm.delete = function(id) {
    	  if($rootScope.roleAccess(roleConstant.MASTER_FINANCE_SERVICE_CHARGE_MAPPING_DELETE)){
    		  var newScope = $scope.$new();
    			newScope.errorMessage = $rootScope.nls["ERR06604"];
    	    	ngDialog.openConfirm( {
    	                    template : '<p>{{errorMessage}}</p>'
    	                    + '<div class="ngdialog-footer">'
    	                    + '<button type="button" class="btn btn-default btn-property cancel-btn" ng-click="closeThisDialog(0)">Cancel'
    	                    + '<button type="button" class="btn btn-primary btn-property accent-btn" ng-click="confirm(1)">Yes'
    	                    + '</button></div>',
    	                    plain : true,
    	                    scope : newScope,
    	                    closeByDocument: false,
    	                    className : 'ngdialog-theme-default'
    	                }).then( function(value) {
    	                	stcGroupMasterFactory.delete.query({
    	                        id: id
    	                    }).$promise.then(function(data) {
    	                        if (data.responseCode == 'ERR0') {
    	                        	Notification.success($rootScope.nls["ERR402"]);
    	                        	//$state.go("layout.stcgroupmaster");
    	                        	vm.deskTopView = true;
    	                        	vm.search();
    	                        }
    	                    }, function(error) {
    	                        $log.debug("Error ", error);
    	                    });
    	                }, function(value) {
    	                    $log.debug("delete Cancelled");
    	            });
    	  }    	  
        }
      
      
      //Update ServiceTaxChargeGroupMaster
      vm.update = function() {
    	  var validationResponse = stcGroupMasterValidationService.validate(vm.stcGroupMaster,0);
           if(validationResponse.error == true) {
           	$log.debug("Validation Response -- ", validationResponse);
           	$log.debug('Validation Failed while Updating the Reason..')
				$scope.validationErrorAndFocus(validationResponse, validationResponse.errElement);
			}
           else {
        	   stcGroupMasterFactory.update.query(vm.stcGroupMaster).$promise.then(function(data) {
		            if (data.responseCode == 'ERR0') {
		            	$log.debug("Reason Updated Successfully Response :: ", data);
		            	Notification.success($rootScope.nls["ERR401"]);
		            	$state.go('layout.stcgroupmaster');
		            } else {
		                $log.debug("Updating Reason Failed :" + data.responseDescription)
		            }
		        }, function(error) {
		        	 $log.debug("Updating Reason Failed : " + error)
		        });
           } 
      };
     
      
      /**
       * Create New ServiceTaxChargeGroupMaster.
       */
      vm.create = function() {
      	var validationResponse = stcGroupMasterValidationService.validate(vm.stcGroupMaster,0);
          if(validationResponse.error == true) {
          	$log.debug("Validation Response -- ", validationResponse);
          	$log.debug('Validation Failed to create ReasonMaster..')
				$scope.validationErrorAndFocus(validationResponse, validationResponse.errElement);
			}
          else {
          	stcGroupMasterFactory.create.query(vm.stcGroupMaster).$promise.then(function(data) {
		            if (data.responseCode == 'ERR0') {
		            	$log.debug("Reason Saved Successfully Response :: ", data);
		            	Notification.success($rootScope.nls["ERR400"]);
		            	$state.go('layout.stcgroupmaster');
		            } else {
		                $log.debug("Create Reason Failed :" + data.responseDescription)
		            }

		        }, function(error) {
		        	 $log.debug("Create Reason Failed : " + error)
		        });
          } 
      };
      
      

      /**
       * Cancel Add or Edit ServiceTaxChargeGroupMaster.
       * @param {int} id - The id of a ServiceTaxChargeGroupMaster
       */
      vm.cancel = function(objId) {
          if(vm.stcGroupMasterForm!=undefined){
		    	vm.goTocancel(vm.stcGroupMasterForm.$dirty,objId);
          }else{
          	vm.goTocancel(false,objId);
          }
      }
     
     vm.goTocancel = function(isDirty,objId){
   		 if (isDirty) {
   		      var newScope = $scope.$new();
   		      newScope.errorMessage = $rootScope.nls["ERR200"];
   		      ngDialog.openConfirm({
   		        template: '<p>{{errorMessage}}</p>' +
   		          '<div class="ngdialog-footer">' +
   		          ' <button type="button" class="btn btn-default btn-property cancel-btn" ng-click="confirm(2)">No</button>' +
   		          '<button type="button" class="btn btn-default btn-property accent-btn " ng-click="confirm(1)">Yes</button>' +
   		          '<button type="button" class="btn btn-default btn-property pull-left grey-cancel-btn" ng-click="confirm(3)">Cancel</button>' +
   		          '</div>',
   		        plain: true,
   		        closeByDocument: false,
   		        scope: newScope,
   		        className: 'ngdialog-theme-default'
   		      }).then(
   		        function(value) {
   		          if (value == 1) {
   		            if(objId!=null && objId != undefined){
   		            	vm.update();
   		            }else{
   		            	vm.create();
   		            }
   		          } else if (value == 2) {
   		        	 $state.go("layout.stcgroupmaster", {
   		                submitAction: "Cancelled"
   		            });
   		          } else {
   		            $log.debug("cancelled");
   		          }
   		        });
   		    } else {
   		     $state.go("layout.stcgroupmaster", {
                 submitAction: "Cancelled"
             });
   		    }
   	    }
      
     
     /**
      * Common Validation Focus Functionality - starts here" 
      */
    
		$scope.validationErrorAndFocus = function(valResponse, elemId){
			$scope.errorMap = new Map();
			if(valResponse.error == true){
				$scope.errorMap.put(valResponse.errElement, valResponse.errMessage);
			}
				$scope.navigateToNextField(elemId);
		}
		
		
		/**
	       * Get ServiceTaxChargeGroupMaster  By id.
	       * @param {int} id - The id of a ServiceTaxChargeGroupMaster.
	       * @param {boolean} isView - TO identify purpose of calling is for View or Edit a ServiceTaxChargeGroupMaster.
	       */
	     
	      vm.view = function(id, isView) {
	    	  vm.id=id;
	      	stcGroupMasterFactory.findById.query({
	              id: vm.id
	          }).$promise.then(function(data) {
	              if (data.responseCode == 'ERR0') {
	            	  $log.debug("response is sucess");
	                  vm.stcGroupMaster = data.responseObject;
	                  var rHistoryObj = {}

	                  if (vm.stcGroupMaster != undefined && vm.stcGroupMaster != null && vm.stcGroupMaster.id != undefined && vm.stcGroupMaster.id != null) {
	                      $rootScope.subTitle = "ServiceTaxChargeGroup";
	                  } else {
	                      $rootScope.subTitle = "Unknown ServiceTaxChargeGroup"
	                  }
	                  $rootScope.unfinishedFormTitle = "ServiceTaxChargeGroup";
	                  if (isView) {
	                      rHistoryObj = {
	                          'title': 'ServiceTaxChargeGroupMaster',
	                          'subTitle': $rootScope.subTitle,
	                          'stateName': $state.current.name,
	                          'stateParam': JSON.stringify($stateParams),
	                          'stateCategory': 'ServiceTaxChargeGroup Master',
	                          'serviceType': undefined,
	                          'showService': false
	                      }

	                  } else {

	                      rHistoryObj = {
	                          'title': 'ServiceTaxChargeGroupMaster',
	                          'subTitle': $rootScope.subTitle,
	                          'stateName': $state.current.name,
	                          'stateParam': JSON.stringify($stateParams),
	                          'stateCategory': 'ServiceTaxChargeGroup Master',
	                          'serviceType': 'MASTER',
	                          'serviceCode': ' ',
	                          'orginAndDestination': ' '
	                      }
	                  }
	                  RecentHistorySaveService.form(rHistoryObj);
	              } else {
	                  $log.debug("Exception while getting ServiceTaxChargeGroup by Id ", data.responseDescription);
	              }
	          }, function(error) {
	              $log.debug("Error ", error);
	          });
	      };
	      
	      /**
	       * ServiceTaxChargeGroupMaster  List Page Search Data  initializing and calling search Mtd.
	       * @param {Object} data - The Selected ServiceTaxChargeGroupMaster  from List page.
	       * @param {string} data.id - The id of the Selected ServiceTaxChargeGroupMaster.
	       * @param {int} index - The Selected row index from List page.
	       */
	      
	      vm.rowSelect = function(data) {
	    	  if($rootScope.roleAccess(roleConstant.MASTER_FINANCE_SERVICE_CHARGE_MAPPING_VIEW)){
	    		  vm.stcGroupMaster = data;
		  			vm.stcGroupMasterView=true;
		  			 vm.showDetail=true;
		  			/*vm.id=data.id;
		  			$state.go("layout.stcgroupmasterView",{id:data.id});*/
		  			var windowInner=$window.innerWidth;
		  			if(windowInner<=1199){
		  				vm.deskTopView = false;
		  			}
		  			else{
		  				vm.deskTopView = true;
		  			}
		  			angular.element($window).bind('resize', function(){
		  				if($window.innerWidth>=1200){
		  					vm.deskTopView = true;
		  				}
		  				else if($window.innerWidth<=1199){
		  					if(vm.stcGroupMaster.id!=null){
		  						vm.deskTopView = false;
		  					}
		  					else {
		  						vm.deskTopView = true;
		  					}
		  				}
		  				$scope.$digest();
		  			});
	    		  
	    	  }
	  		}
	     
	      
	      
      
      
      /**
       * State Identification Handling Here.
       * @param {String} $stateParams.action - VIEW, ADD, EDIT, SEARCH.
       */
      //
      switch ($stateParams.action) {
          case "ADD":
          	 if ($stateParams.fromHistory === 'Yes') {
                   if (vm.isReloaded == "YES") {
                    vm.isReloaded = "NO";
                    localStorage.isReasonMasterReloaded = "NO";
                     vm.reasonMaster = JSON.parse(localStorage.reloadFormData);
                   } else {
                     vm.reasonMaster = $rootScope.selectedUnfilledFormData;
                   }
                 } else {
                   if (vm.isReloaded == "YES") {
                     vm.isReloaded = "NO";
                     localStorage.isReasonMasterReloaded = "NO";
                     vm.reasonMaster = JSON.parse(localStorage.reloadFormData);
                   } else {
                       $log.debug("Add NotHistory NotReload...")
                   }
                 }
              $rootScope.breadcrumbArr = stcGroupMasterDataService.getAddBreadCrumb();
              break;
          case "EDIT":
          	 if (vm.fromHistory === 'Yes') {
                   if (vm.isReloaded == "YES") {
                     vm.isReloaded = "NO";
                     localStorage.isstcGroupMasterReloaded = "NO";
                     vm.reasonMaster = JSON.parse(localStorage.reloadFormData);
                   } else {
                     vm.reasonMaster = $rootScope.selectedUnfilledFormData;
                   }
                 } else {
                   if (vm.isReloaded == "YES") {
                     vm.isReloaded = "NO";
                     localStorage.isReasonMasterReloaded = "NO";
                     vm.reasonMaster = JSON.parse(localStorage.reloadFormData);
                   } else {
                  	 vm.view($stateParams.id, false);
                   }
                 }
              $rootScope.breadcrumbArr = stcGroupMasterDataService.getEditBreadCrumb();
              break;
          case "VIEW":
        	  vm.view($stateParams.id, false);
        	  $rootScope.breadcrumbArr = stcGroupMasterDataService.getViewBreadCrumb();
        	  break;
          default:
              $rootScope.breadcrumbArr = stcGroupMasterDataService.getListBreadCrumb();
              vm.searchInit();
      }
      
 
}
]);
}) ();