app.service('stcGroupMasterDataService', function($rootScope) {
    this.getHeadArray = function() {
	   return [
			  {
					"name":"#",
					"width":"col-xs-0half",
					"model":"no",
					"search":false
			  },{
					"name":"Service",
					"width":"col-md-1half",
					"model":"serviceMaster.serviceName",
					"search":true,
					"wrap_cell":true,
					"type":"text",
					"sort":true,
					"key":"searchServiceName"
				},{
					"name":"Charge",
					"width":"col-md-1half",
					"model":"chargeMaster.chargeName",
					"search":true,
					"wrap_cell":true,
					"type":"text",
					"sort":true,
					"key":"searchChargeName"
				},{
					"name":"Category",
					"width":"col-md-1half",
					"model":"categoryMaster.categoryName",
					"search":true,
					"wrap_cell":true,
					"type":"text",
					"sort":true,
					"key":"searchCategoryName"
				}];
          };

    
    this.getListBreadCrumb = function() {
        return [{
                label: "Master",
                state: "layout.stcgroupmaster"
            },
            {
                label: "Service Tax Charge Group",
                state: "layout.stcgroupmaster"
            }
        ];
    }

    this.getAddBreadCrumb = function() {
        return [{
                label: "Master",
                state: "layout.stcgroupmaster"
            },
            {
                label: "Service Tax Charge Group",
                state: "layout.stcgroupmaster"
            },
            {
                label: "Add Service Tax Charge Group",
                state: null
            }
        ];
    };
    this.getEditBreadCrumb = function() {
        return [{
                label: "Master",
                state: "layout.stcgroupmaster"
            },
            {
            	label: "Service Tax Charge Group",
                state: "layout.stcgroupmaster"
            },
            {
                label: "Edit Service Tax Charge Group",
                state: null
            }
        ];
    };
    
    this.getViewBreadCrumb = function() {
        return [{
                label: "Master",
                state: "layout.stcgroupmaster"
            },
            {
            	label: "Service Tax Charge Group",
                state: "layout.stcgroupmaster"
            },
            {
                label: "View Service Tax Charge Group",
                state: null
            }
        ];
    };
   

});