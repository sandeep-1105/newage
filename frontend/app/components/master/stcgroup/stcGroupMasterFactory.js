app.factory('stcGroupMasterFactory', function($resource) {
	return {
		findById : $resource('/api/v1/stcgroupmaster/get/id/:id', {}, {
			 query : {method : 'GET', params : {id : ''}, isArray : false}
		})
		,
		search : $resource('/api/v1/stcgroupmaster/get/search', {}, {
			fetch : {method : 'POST'}
		})
		,
		update : $resource('/api/v1/stcgroupmaster/update', {}, {
			query : {method : 'POST'}
		})
		,
		delete : $resource('/api/v1/stcgroupmaster/delete/:id', {}, {
			query : {method : 'DELETE', params : {id : ''}}
		})
		,
		create : $resource('/api/v1/stcgroupmaster/create', {}, {
			query : {method : 'POST'}
		})
		,
		searchByService : $resource("/api/v1/servicemaster/get/search/keyword", {}, {
			query : {
				method : 'POST',
			}
		})
		,
		searchByCharge : $resource("/api/v1/chargemaster/get/search/keyword", {}, {
			query : {
				method : 'POST',
			}
		})
		,
		searchByCategory : $resource("/api/v1/categorymaster/get/search/keyword", {}, {
			query : {
				method : 'POST',
			}
		})
	};
})