app.controller('aesmastercontroller', function($rootScope, $scope, $window, $stateParams, $state, ngTableParams, ngDialog, 
		CitySearch, CityRemove) {

	/************************************************************
	 * ux - by Muthu
	 * reason - ipad compatibility fix
	 *
	 * *********************************************************/
	$scope.deskTopView = true;

	/********************* ux fix ends here *****************************/
	
	$scope.cityHeadArr = [ {
		"name" : "#",
		"width" : "col-xs-0half",
		"model" : "no",
		"search" : false,
	}, {
		"name" : "Name",
		"width" : "col-xs-3",
		"model" : "cityName",
		"search" : true,
		"wrap_cell" : true,
		"type" : "text",
		"sort" : true,
		"key" : "searchCityName"
	}, {
		"name" : "Code",
		"width" : "col-xs-1",
		"model" : "cityCode",
		"search" : true,
		"type" : "text",
		"sort" : true,
		"key" : "searchCityCode"
	}, {
		"name" : "State",
		"width" : "col-xs-2",
		"model" : "stateMaster.stateName",
		"search" : true,
		"type" : "text",
		"sort" : true,
		"key" : "searchStateName"
	}, {
		"name" : "Country",
		"width" : "col-xs-3",
		"model" : "countryMaster.countryName",
		"search" : true,
		"type" : "text",
		"sort" : true,
		"key" : "searchCountryName"
	}, {
		"name" : "Status",
		"width" : "col-xs-2",
		"model" : "status",
		"search" : true,
		"sort" : true,
		"type" : "drop",
		"data" : $rootScope.enum['LovStatus'],
		"key" : "searchStatus"
	} ];

					
					
					
	$scope.sortSelection = {
			sortKey:"cityName", sortOrder:"asc"
	}

	$scope.limitArr = [10, 15, 20];
	$scope.page = 0;
	$scope.limit = 10;
					

	$scope.init = function() {
		$scope.searchDto = {};
		$scope.search();
	};

	$scope.limitChange = function(item) {
		$scope.page = 0;
		$scope.limit = item;
		$scope.cancel();
		$scope.search();
	}
	
	$scope.rowSelect = function(data){
		$scope.cityMaster = data;
		$scope.showHistory = false;
		$scope.detailView = true;
		
		/************************************************************
		* ux - by Muthu
		* reason - ipad compatibility fix
		*
		* *********************************************************/

		var windowInner=$window.innerWidth;
		if(windowInner<=1199){
			$scope.deskTopView = false;
		} else {
			$scope.deskTopView = true;
		}
		
		angular.element($window).bind('resize', function(){
			if($window.innerWidth>=1200){
				$scope.deskTopView = true;
			} else if($window.innerWidth<=1199){
				if($scope.tosMaster.id!==false){
					$scope.deskTopView = false;
				} else {
					$scope.deskTopView = true;
				}
			}
			
			console.log("window resizing..." + $window.innerWidth);
			$scope.$digest();
		});

		/********************* ux fix ends here *****************************/
	}
	
	$scope.changeSearch = function(param) {
						
		for (var attrname in param) {
			$scope.searchDto[attrname] = param[attrname];
		}
		$scope.page = 0;
		$scope.cancel();
		$scope.search();
		console.log("change search",$scope.searchDto);
	}

	$scope.sortChange = function(param) {
		$scope.searchDto.orderByType = param.sortOrder.toUpperCase();
		$scope.searchDto.sortByColumn = param.sortKey;
		$scope.cancel();
		$scope.search();
	}
	
	$scope.changepage = function(param) {
		$scope.page = param.page;
		$scope.limit = param.size;
		$scope.cancel();
		$scope.search();
	}
					
	$scope.search = function() {
		
		$scope.detailView = false;
		$scope.showHistory = false;
		
		$scope.searchDto.selectedPageNumber = $scope.page;
		$scope.searchDto.recordPerPage = $scope.limit;
		
		CitySearch.query($scope.searchDto).$promise.then(function(data, status) {
			$scope.totalRecord = data.responseObject.totalRecord;
			$scope.cityDataArr = data.responseObject.searchResult;
			console.log($scope.cityDataArr);
		});

	}

	$scope.cancel = function() {
		$scope.showHistory = false;
		$scope.cityMaster = {};
		$scope.detailView = false;
		/************************************************************
		 * ux - by Muthu
		 * reason - ipad compatibility fix
		 *
		 * *********************************************************/
		$scope.deskTopView = true;
		/********************* ux fix ends here *****************************/
	};

	$scope.addCityMaster = function() {
		$state.go("layout.addCity");
	};
	
	$scope.editCityMaster = function() {
		$state.go("layout.editCity", {cityId : $scope.cityMaster.id});
	};
	
	$scope.deleteCityMaster = function() {
		var newScope = $scope.$new();
		newScope.errorMessage = $rootScope.nls["ERR248"];
		ngDialog.openConfirm( 
		{ template : 	'<p>{{errorMessage}}</p>' +
						'<div class="ngdialog-footer">' +
						'<button type="button" class="btn btn-default btn-property cancel-btn" ng-click="closeThisDialog(0)">No' +
						'<button type="button" class="btn btn-primary btn-property accent-btn" ng-click="confirm(1)">Yes' +
						'</button>' +
						'</div>',
						plain: true,
						scope: newScope,
						className: 'ngdialog-theme-default'
		}).then(function (value) {
        
			CityRemove.remove({ id : $scope.cityMaster.id }, function(data) {
       			
				if (data.responseCode =="ERR0"){
					console.log("City deleted Successfully")
					$scope.cancel();
					$scope.init();
				} else {
					console.log("City deleted Failed " , data.responseDescription)
       			}
       		}, function(error) {
       			console.log("City deleted Failed : " , error)
       		});
           }, function (value) {
               console.log("City deleted cancelled");
           });
			
		
	}

					

	if($stateParams.action == "SEARCH") {
		$rootScope.breadcrumbArr = [ { label:"Master", state:"layout.city"}, 
		                             { label:"City", state:"layout.city"}];
		$scope.init();
	}					
});
