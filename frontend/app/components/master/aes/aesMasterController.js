(function(){ 
app.controller("aesMasterController",['$scope','$rootScope','$state','$stateParams','$modal','ngDialog','$log','Notification',
                                       function($scope,$rootScope,$state,$stateParams,$modal,ngDialog,$log, Notification){


	 //var $scope= this;
	 //$scope.bankHeadArr = bankMasterService.getHeadArray();
	 $scope.bankMaster={};
     $scope.showDetail=false;
     $scope.searchDto={};
     $scope.limitArr = [10,20,30];
     $scope.page = 0;
     $scope.limit =10;
     
     $scope.aesHeadArr = [
	                      {
	                    	  "name" : "#",
	                    	  "width" : "col-xs-0half",
	                    	  "prefWidth" : "50",
	                    	  "model" : "no",
	                    	  "search" : false
	                    	
	                    	  
	                      },
	                      {
	                    	  "name" : "Name",
	                    	  "width" : "col-xs-2",
	                    	  "prefWidth" : "50",
	                    	  "type": "text",
	                    	  "sort" : true,
	                    	  "model" : "aesName",
	                    	  "search" : true
	                    	  
	                      },
	                 
	                     
	                      {
	                    	  "name" : "Country Code",
	                    	  "width" : "col-xs-1",
	                    	  "prefWidth" : "50",
	                    	  "model" : "aescountry",
	                    	  "type":"text",
	                    	  "search" : true
	                      },
	                      {
	                    	  "name" : "Location ID",
	                    	  "width" : "col-xs-1",
	                    	  "prefWidth" : "50",
	                    	  "model" : "aeslocation",
	                    	  "type":"text",
	                    	  "search" : true
	                      },
	                      {
	                    	  "name" : "Status",
	                    	  "width" : "col-xs-2",
	                    	  "prefWidth" : "50",
	                    	  "model" : "status",
	                    	  "type":"drop",
	                    	  "search" : true
	                      }
	                      ];
	
	
	
	 $scope.tableValue = [
	                      {
	                    	  no : 1,
	                    	  aesName : 'Demo1',
	                    	  aesid : 'C00049',
	                    	  aesdate :'01-01-2017',
	                    	  aesrundate :'30-01-2017',
	                    	  aescountry :'00049',
	                    		  aeslocation:'CB00049',
	                    			  status: 'Active'
	                      },
	                      {
	                    	  no : 2,
	                    	  aesName : 'Demo2',
	                    	  aesid : 'C00048',
	                    	  aesdate :'01-01-2017',
	                    	  aesrundate :'30-01-2017',
	                    	  aescountry :'00049',
	                    		  aeslocation:'CB00049',
	                    			  status: 'Active'
	                      },
	                      {
	                    	  no : 3,
	                    	  aesName : 'Demo3',
	                    	  aesid : 'C00047',
	                    	  aesdate :'01-01-2017',
	                    	  aesrundate :'30-01-2017',
	                    	  aescountry :'00049',
	                    		  aeslocation:'CB00049',
	                    			  status: 'Active'
	                      },
	                      {
	                    	  no : 4,
	                    	  aesName : 'Demo4',
	                    	  aesid : 'C00046',
	                    	  aesdate :'01-01-2017',
	                    	  aesrundate :'30-01-2017',
	                    	  aescountry :'00049',
	                    		  aeslocation:'CB00049',
	                    			  status: 'Active'
	                      },
	                      {
	                    	  no : 5,
	                    	  aesName : 'Demo5',
	                    	  aesid : 'C00045',
	                    	  aesdate :'01-01-2017',
	                    	  aesrundate :'30-01-2017',
	                    	  aescountry :'00049',
	                    		  aeslocation:'CB00049',
	                    			  status: 'Active'
	                      },
	                      {
	                    	  no : 6,
	                    	  aesName : 'Demo6',
	                    	  aesid : 'C00044',
	                    	  aesdate :'01-01-2017',
	                    	  aesrundate :'30-01-2017',
	                    	  aescountry :'00049',
	                    		  aeslocation:'CB00049',
	                    			  status: 'Active'
	                      },
	                      {
	                    	  no : 7,
	                    	  aesName : 'Demo7',
	                    	  aesid : 'C00043',
	                    	  aesdate :'01-01-2017',
	                    	  aesrundate :'30-01-2017',
	                    	  aescountry :'00049',
	                    		  aeslocation:'CB00049',
	                    			  status: 'Active'
	                      },
	                      {
	                    	  no : 8,
	                    	  aesName : 'Demo8',
	                    	  aesid : 'C00042',
	                    	  aesdate :'01-01-2017',
	                    	  aesrundate :'30-01-2017',
	                    	  aescountry :'00049',
	                    		  aeslocation:'CB00049',
	                    			  status: 'Active'
	                      },
	                      {
	                    	  no : 9,
	                    	  aesName : 'Demo9',
	                    	  aesid : 'C00041',
	                    	  aesdate :'01-01-2017',
	                    	  aesrundate :'30-01-2017',
	                    	  aescountry :'00049',
	                    		  aeslocation:'CB00049',
	                    			  status: 'Active'
	                      },
	                      {
	                    	  no : 10,
	                    	  aesName : 'Demo10',
	                    	  aesid : 'C00040',
	                    	  aesdate :'01-01-2017',
	                    	  aesrundate :'30-01-2017',
	                    	  aescountry :'00049',
	                    		  aeslocation:'CB00049',
	                    			  status: 'Active'
	                      }                      
	                      ];
	//initialzation the values
	$scope.init=function(){
		//for- if Add/else edit
		$scope.search();
		if($scope.bankMaster==undefined || $scope.bankMaster==null || $scope.bankMaster.id==undefined || $scope.bankMaster.id==null){
			$rootScope.breadcrumbArr = [
	                                    { label:"Master", 
	                                      state:"layout.bankMaster"}, 
	                                    { label:"Bank", 
	                                      state:"layout.bankMaster"}, 
	                                    { label:"Add Bank", state:null}];	
		}else{
			$rootScope.breadcrumbArr = [
	                                    { label:"Master", 
	                                      state:"layout.bankMaster"}, 
	                                    { label:"Bank", 
	                                      state:"layout.bankMaster"}, 
	                                    { label:"Edit Bank", state:null}];	
		}
	}//init ended

	
	
	$scope.setBreadCumList=function(){
		
		$rootScope.breadcrumbArr = [
                                    { label:"Master", 
                                      state:"layout.bankMaster"}, 
                                    { label:"Bank", 
                                      state:null}
                                   ];	
		
	}
	
	
	
	
$scope.sortSelection = {
		sortKey:"bankName",
		sortOrder:"asc"
}



//add operation-
$scope.addNewBank=function(){
	var params={};
	params.Action='ADD';
	$state.go('layout.bankMasterAdd',params);
}

$scope.setStatus = function(data){
	if(data=='Active'){
		return 'activetype';
	} else if(data=='Block'){
		return 'blockedtype';
	}else if(data=='Hide'){
		return 'hiddentype';
	}
}

//based on param serach
$scope.changeSearch = function(param) {
    $log.debug("Change Search is called." + param);
    for (var attrname in param) {
        $scope.searchDto[attrname] = param[attrname];
    }
    $scope.page = 0;
    $scope.search();
}

//sort 
$scope.sortChange = function(param) {
    $log.debug("sortChange is called. " + param);
    $scope.searchDto.orderByType = param.sortOrder.toUpperCase();
    $scope.searchDto.sortByColumn = param.sortKey;
    $scope.search();
}

/**
 * Bank Master List Navigate to selected page.
 */
$scope.changepage = function(param) {
    $log.debug("change Page..." + param.page);
    $scope.page = param.page;
    $scope.limit = param.size;
    $scope.search();
}

/**
 * Bank Master List Page Limit Data change.
 */
$scope.limitChange = function(nLimit) {
    $log.debug("Limit Change..." + nLimit);
    $scope.page = 0;
    $scope.limit = nLimit;
    $scope.search();
}

//for view 
$scope.editBank=function(obj){
	bankService.set(obj);
$state.go('layout.bankMasterEdit');	
}

$scope.rowSelect=function(obj){
	$scope.bankMaster=obj;
	$scope.showDetail=true;
}
$scope.cancelHistory=function(){
	$scope.showDetail=false;
}
$scope.validateBankOnChange=function(bm,code){
	 $scope.errorMap=new Map();
 var validationResponse=bankMasterValidationService.validate(bm,code);
    if(validationResponse.error=true){
	  $scope.errorMap.put(validationResponse.errElement, validationResponse.errMessage);
   }
}


$scope.search = function() {
	$scope.showDetail=false;
	$scope.setBreadCumList();
	console.log("Search method is called............................!");
	$scope.searchDto.selectedPageNumber = $scope.page;
	$scope.searchDto.recordPerPage = $scope.limit;
	$scope.bankListArr = [];
	/*bankMasterFactory.search.query($scope.searchDto).$promise.then(function(data, status) {
		$scope.totalRecord = data.responseObject.totalRecord;
		var tempArr = [];
		var resultArr = [];
		tempArr = data.responseObject.searchResult;

		var tempObj = {};
		angular.forEach(tempArr,function(item,index) {
			tempObj = item;
			tempObj.no = (index+1)+($scope.page*$scope.limit);
			resultArr.push(tempObj);
			tempObj = {};
		});
		
		$scope.bankListArr = resultArr;
	});*/

}
/**
 * Delete Bank Master-calling service.
 * 
 */
$scope.deleteBank = function(id) {
	var newScope = $scope.$new();
	newScope.errorMessage =$rootScope.nls["ERR06258"];
	ngDialog.openConfirm({
                template : '<p>{{errorMessage}}</p>'
                + '<div class="ngdialog-footer">'
                + '<button type="button" class="btn btn-default btn-property cancel-btn" ng-click="closeThisDialog(0)">Cancel'
                + '<button type="button" class="btn btn-primary btn-property accent-btn" ng-click="confirm(1)">Yes'
                + '</button></div>',
                plain : true,
                scope : newScope,
                closeByDocument: false,
                className : 'ngdialog-theme-default'
            }).then(function(value) {
            	bankMasterFactory.delete.query({
                    id: id
                }).$promise.then(function(data) {
                    if (data.responseCode == 'ERR0') {
                    	Notification.success($rootScope.nls["ERR402"]);
                    	$scope.search();
                        $state.go("layout.bankMaster", {
                            submitAction: "Cancelled"
                        });
                    }
                }, function(error) {
                    $log.debug("Error ", error);
                });
            }, function(value) {
                $log.debug("delete Cancelled");
        });
}


/**
 * Create New bank Master.
 */
$scope.create = function() {
	$scope.errorMap=new Map();
    var validationResponse = bankMasterValidationService.validate($scope.bankMaster,0);
    if(validationResponse.error == true) {
    	$log.debug('Validation Failed to create bank..')
		$scope.validationErrorAndFocus(validationResponse, validationResponse.errElement);
	}
    else {
    	bankMasterFactory.create.query($scope.bankMaster).$promise.then(function(data) {
            if (data.responseCode == 'ERR0') {
            	$log.debug("bank Saved Successfully Response :: ", data);
            	Notification.success($rootScope.nls["ERR400"]);
            	$state.go('layout.bankMaster');
            } else {
                $log.debug("Create bank Failed :" + data.responseDescription)
            }

        }, function(error) {
        	 $log.debug("Create bank Failed : " + error)
        });
    } 
};



/**
* Common Validation Focus Functionality - starts here" 
*/
$scope.validationErrorAndFocus = function(valResponse, elemId){
	$scope.errorMap = new Map();
	if(valResponse.error == true){
		$scope.errorMap.put(valResponse.errElement, valResponse.errMessage);
	}
		$scope.navigateToNextField(elemId);
}

//Update Existing bank Master
$scope.update = function() {
	$scope.errorMap=new Map();
	 var validationResponse = bankMasterValidationService.validate($scope.bankMaster,0);
     if(validationResponse.error == true) {
     	$log.debug("Validation Response -- ", validationResponse);
     	$log.debug('Validation Failed while Updating the bank..')
			$scope.validationErrorAndFocus(validationResponse, validationResponse.errElement);
		}
     else {
     	bankMasterFactory.update.query($scope.bankMaster).$promise.then(function(data) {
	            if (data.responseCode == 'ERR0') {
	            	$log.debug("bank Updated Successfully Response :: ", data);
	            	Notification.success($rootScope.nls["ERR401"]);
	            	$state.go('layout.bankMaster');
	            } else {
	                $log.debug("Updating Bank Failed :" + data.responseDescription)
	            }

	        }, function(error) {
	        	 $log.debug("Updating bank Failed : " + error)
	        });
     } 
};


//cancel from add/edit page
$scope.cancelToList=function(){
	if(!$scope.bankform.$dirty){
		var params = {};
		params.submitAction = 'Cancelled';
		$state.go('layout.bankMaster',params);
	}else{
		var newScope = $scope.$new();
		newScope.errorMessage = $rootScope.nls["ERR200"];
		ngDialog.openConfirm({
			template: '<p>{{errorMessage}}</p>' +
			'<div class="ngdialog-footer">' +
				' <button type="button" class="btn btn-default btn-property cancel-btn" ng-click="confirm(2)">No</button>' +
				'<button type="button" class="btn btn-default btn-property accent-btn " ng-click="confirm(1)">Yes</button>' +
				'<button type="button" class="btn btn-default btn-property pull-left grey-cancel-btn" ng-click="confirm(3)">Cancel</button>' +
			'</div>',
			plain: true,
			scope: newScope,
			className: 'ngdialog-theme-default'
		})
				.then(
						function(value) {
							if (value == 1
									&& $scope.bankMaster.id == null) {
								$scope.create();
							} else if (value == 1
									&& $scope.bankMaster.id != null) {
								$scope.update();
							} else if (value == 2) {
								var params = {};
								params.submitAction = 'Cancelled';
								$state.go('layout.bankMaster',params);
							}
							else {
								console.log("cancelled");
							}
						});
	}
}





/**
 * Recent History - starts here
 */
$scope.isReloaded = localStorage.isBankMasterReloaded;
$scope.$on('addBankMasterEvent', function(events, args){
  try{
		$rootScope.unfinishedData = $scope.bankMaster;
		$rootScope.category = "Master";
		$rootScope.unfinishedFormTitle = "Bank (New)";
		if($scope.bankMaster != undefined && $scope.bankMaster != null && $scope.bankMaster.bankName != undefined && $scope.bankMaster.bankName != null && $scope.bankMaster.bankName != "") {
			$rootScope.subTitle = $scope.bankMaster.bankName;
		} else {
			$rootScope.subTitle = "Unknown Bank "
		}
	}catch(e){
		$log.error("session storage is exceed in bank  master")
	}
})

$scope.$on('editBankMasterEvent', function(events, args){
	try{
		$rootScope.unfinishedData = $scope.bankMaster;
		$rootScope.category = "Master";
		$rootScope.unfinishedFormTitle = "Bank Edit";
	    $rootScope.subTitle ="Bank"; 
	}catch(e){
		$log.error("session storage is exceed in bank master-edit")
	}
		 
})
  
$scope.$on('addBankMasterEventReload', function(e, confirmation){
	try{
		confirmation.message = "";
	    localStorage.reloadFormData = JSON.stringify($scope.bankMaster) ;
	    localStorage.isBankMasterReloaded = "YES";
        e.preventDefault();
	}catch(e){
		$log.error("session storage is exceed in bank master-add while reload")
	}
})

$scope.$on('editBankMasterEventReload', function(e, confirmation){
	try{
		confirmation.message = "";
	    localStorage.reloadFormData = JSON.stringify($scope.bankMaster) ;
	    localStorage.isBankMasterReloaded = "YES";
        e.preventDefault();
	}catch(e){
		$log.error("session storage is exceed in bank master-edit while reload")
	}
})

try{
	if($stateParams.action == "ADD") {
		if($stateParams.fromHistory === 'Yes') {
			if($scope.isReloaded == "YES") {
				console.log("Add History Reload...")
				$scope.isReloaded = "NO";
				localStorage.isBankMasterReloaded = "NO";
				$scope.bankMaster  = JSON.parse(localStorage.reloadFormData);
				$scope.init();
			} else {
				console.log("Add History NotReload...")
				$scope.bankMaster = $rootScope.selectedUnfilledFormData;
				$scope.init();			
			}
			$scope.oldData = "";
		} else {
			if($scope.isReloaded == "YES") {
				$scope.isReloaded = "NO";
				localStorage.isBankMasterReloaded = "NO";
				$scope.bankMaster  = JSON.parse(localStorage.reloadFormData);
				console.log("Add NotHistory Reload...");
				$scope.init();			
				$scope.oldData = "";
			} else {
				$scope.bankMaster={};
				console.log("Add NotHistory NotReload...")
				$scope.init();
			}	
		}
	}else if($stateParams.action == "EDIT"){
		if($stateParams.fromHistory === 'Yes') {
			if($scope.isReloaded == "YES") {
				$scope.isReloaded = "NO";
				localStorage.isBankMasterReloaded = "NO";
				$scope.bankMaster  = JSON.parse(localStorage.reloadFormData);
				$scope.init();	
				}else {
				$scope.bankMaster = $rootScope.selectedUnfilledFormData;
				$scope.init();			
				}
			$scope.oldData = "";
		} else {
			if($scope.isReloaded == "YES") {
				$scope.isReloaded = "NO";
				localStorage.isBankMasterReloaded = "NO";
				$scope.bankMaster  = JSON.parse(localStorage.reloadFormData);
				$scope.init();
				$scope.oldData = "";
			} else {
				$scope.bankMaster=bankService.get();
				$scope.init();
			}	
		}
	}else{
		$scope.search();
		$log.info('Search matched');
	}
}catch(e){
	$log.error("Exception in state "+e);
}

   }
]);
}) ();