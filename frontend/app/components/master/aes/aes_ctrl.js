app.controller("aesmasterListcontroller", function( $scope, $stateParams, $state, ngTableParams ){
	$scope.name = 'Sanjai';
	$scope.aesHeadArr = [
	                      {
	                    	  "name" : "#",
	                    	  "width" : "col-xs-0half",
	                    	  "prefWidth" : "50",
	                    	  "model" : "no",
	                    	  "search" : false
	                    	
	                    	  
	                      },
	                      {
	                    	  "name" : "Name",
	                    	  "width" : "col-xs-2",
	                    	  "prefWidth" : "50",
	                    	  "type": "text",
	                    	  "sort" : true,
	                    	  "model" : "aesName",
	                    	  "search" : true
	                    	  
	                      },
	                 
	                     
	                      {
	                    	  "name" : "Country Code",
	                    	  "width" : "col-xs-1",
	                    	  "prefWidth" : "50",
	                    	  "model" : "aescountry",
	                    	  "type":"text",
	                    	  "search" : true
	                      },
	                      {
	                    	  "name" : "Location ID",
	                    	  "width" : "col-xs-1",
	                    	  "prefWidth" : "50",
	                    	  "model" : "aeslocation",
	                    	  "type":"text",
	                    	  "search" : true
	                      },
	                      {
	                    	  "name" : "Status",
	                    	  "width" : "col-xs-2",
	                    	  "prefWidth" : "50",
	                    	  "model" : "status",
	                    	  "type":"drop",
	                    	  "search" : true
	                      }
	                      ];
	
	
	
	 $scope.tableValue = [
	                      {
	                    	  no : 1,
	                    	  aesName : 'Demo1',
	                    	  aesid : 'C00049',
	                    	  aesdate :'01-01-2017',
	                    	  aesrundate :'30-01-2017',
	                    	  aescountry :'00049',
	                    		  aeslocation:'CB00049',
	                    			  status: 'Active'
	                      },
	                      {
	                    	  no : 2,
	                    	  aesName : 'Demo2',
	                    	  aesid : 'C00048',
	                    	  aesdate :'01-01-2017',
	                    	  aesrundate :'30-01-2017',
	                    	  aescountry :'00049',
	                    		  aeslocation:'CB00049',
	                    			  status: 'Active'
	                      },
	                      {
	                    	  no : 3,
	                    	  aesName : 'Demo3',
	                    	  aesid : 'C00047',
	                    	  aesdate :'01-01-2017',
	                    	  aesrundate :'30-01-2017',
	                    	  aescountry :'00049',
	                    		  aeslocation:'CB00049',
	                    			  status: 'Active'
	                      },
	                      {
	                    	  no : 4,
	                    	  aesName : 'Demo4',
	                    	  aesid : 'C00046',
	                    	  aesdate :'01-01-2017',
	                    	  aesrundate :'30-01-2017',
	                    	  aescountry :'00049',
	                    		  aeslocation:'CB00049',
	                    			  status: 'Active'
	                      },
	                      {
	                    	  no : 5,
	                    	  aesName : 'Demo5',
	                    	  aesid : 'C00045',
	                    	  aesdate :'01-01-2017',
	                    	  aesrundate :'30-01-2017',
	                    	  aescountry :'00049',
	                    		  aeslocation:'CB00049',
	                    			  status: 'Active'
	                      },
	                      {
	                    	  no : 6,
	                    	  aesName : 'Demo6',
	                    	  aesid : 'C00044',
	                    	  aesdate :'01-01-2017',
	                    	  aesrundate :'30-01-2017',
	                    	  aescountry :'00049',
	                    		  aeslocation:'CB00049',
	                    			  status: 'Active'
	                      },
	                      {
	                    	  no : 7,
	                    	  aesName : 'Demo7',
	                    	  aesid : 'C00043',
	                    	  aesdate :'01-01-2017',
	                    	  aesrundate :'30-01-2017',
	                    	  aescountry :'00049',
	                    		  aeslocation:'CB00049',
	                    			  status: 'Active'
	                      },
	                      {
	                    	  no : 8,
	                    	  aesName : 'Demo8',
	                    	  aesid : 'C00042',
	                    	  aesdate :'01-01-2017',
	                    	  aesrundate :'30-01-2017',
	                    	  aescountry :'00049',
	                    		  aeslocation:'CB00049',
	                    			  status: 'Active'
	                      },
	                      {
	                    	  no : 9,
	                    	  aesName : 'Demo9',
	                    	  aesid : 'C00041',
	                    	  aesdate :'01-01-2017',
	                    	  aesrundate :'30-01-2017',
	                    	  aescountry :'00049',
	                    		  aeslocation:'CB00049',
	                    			  status: 'Active'
	                      },
	                      {
	                    	  no : 10,
	                    	  aesName : 'Demo10',
	                    	  aesid : 'C00040',
	                    	  aesdate :'01-01-2017',
	                    	  aesrundate :'30-01-2017',
	                    	  aescountry :'00049',
	                    		  aeslocation:'CB00049',
	                    			  status: 'Active'
	                      }                      
	                      ];
	 
	
	/* $scope.alldetails = [{id:1, name:'#', cname:'Sanjai', price:'285'},
	                     {id:2, name:'Apple', cname:'Krishnan', price:'70'},
	                     {id:3, name:'Orange', cname:'Mariyamman', price:'180'},
	                     {id:4, name:'Apple', cname:'Sudhanthini', price:'70'},
	                     {id:5, name:'Grape', cname:'Anantha Raja', price:'70'},
	                     
	                     ];
	                     
	                          
	*/                
	                     
	                     $scope.sortSelection = {
			sortKey:"aesName", sortOrder:"asc"
	}
	                     
	                     $scope.rowSelect = function(data, ind){
	 						
	 					    var param = {
	 					    		id   : data.id,
	 					    		index: ind
	 					    };
	 					    $state.go("layout.masteraesView", param);
	 						
	 						/************************************************************
	 						 * ux - by Muthu
	 						 * reason - ipad compatibility fix
	 						 *
	 						 * *********************************************************/

	 						var windowInner=$window.innerWidth;
	 						if(windowInner<=1199){
	 							$scope.deskTopView = false;
	 						}
	 						else{
	 							$scope.deskTopView = true;
	 						}
	 					}
       
	                     
	                     
	
	 $scope.limitArr = [10,20,30];
	 $scope.page = 0;
	 $scope.limit = 10;
	 $scope.totalRecord = 14;
	 

		$scope.limitArr = [10, 15, 20];
		$scope.page = 0;
		$scope.limit = 10;
						

		$scope.init = function() {
			$scope.searchDto = {};
			$scope.search();
		};

		$scope.limitChange = function(item) {
			$scope.page = 0;
			$scope.limit = item;
			$scope.cancel();
			$scope.search();
		}

});

