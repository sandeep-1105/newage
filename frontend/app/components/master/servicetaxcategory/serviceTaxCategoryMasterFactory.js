app.factory('serTaxCatMasterFactory', function($resource) {
	return {
		findById : $resource('/api/v1/sertaxcatmaster/get/id/:id', {}, {
			 query : {method : 'GET', params : {id : ''}, isArray : false}
		})
		,
		search : $resource('/api/v1/sertaxcatmaster/get/search', {}, {
			fetch : {method : 'POST'}
		})
		,
		update : $resource('/api/v1/sertaxcatmaster/update', {}, {
			query : {method : 'POST'}
		})
		,
		delete : $resource('/api/v1/sertaxcatmaster/delete/:id', {}, {
			query : {method : 'DELETE', params : {id : ''}}
		})
		,
		searchByCharge : $resource("/api/v1/chargemaster/get/search/keyword", {}, {
			query : {
				method : 'POST',
			}
		})
		,
		searchByGenLdr : $resource("/api/v1/generalledgermaster/get/search/keyword", {}, {
			query : {
				method : 'POST',
			}
		})
		,
		searchBySubLdr : $resource("/api/v1/subledgermaster/get/search/keyword", {}, {
			query : {
				method : 'POST',
			}
		})
		,
		searchByCurrency : $resource("/api/v1/currencymaster/get/search/keyword", {}, {
			query : {
				method : 'POST',
			}
		})
		,
		create : $resource('/api/v1/sertaxcatmaster/create', {}, {
			query : {method : 'POST'}
		})
	};
})