app.service('serTaxCatMasterValidationService', ['$rootScope','ValidateUtil', 'CommonValidationService', 'appConstant', function($rootScope, ValidateUtil, CommonValidationService, appConstant) {
	this.validate = function(serTaxCatMaster, code) {
		//Validate serTaxCatMaster name
		if(code == 0 || code == 1) {
			if(serTaxCatMaster.name == undefined || serTaxCatMaster.name == null || serTaxCatMaster.name =="" ){
				return this.validationResponse(true, "name", $rootScope.nls["ERR06373"], serTaxCatMaster);
			}else{
				if(!CommonValidationService.checkRegExp(appConstant.Reg_Exp_Master_ServiceTaxCategory_Name,serTaxCatMaster.name)){
	  				return this.validationResponse(true, "name", $rootScope.nls["ERR06371"], serTaxCatMaster);
			   }
			}
		}
		//Validate serTaxCatMaster code
		if(code == 0 || code == 2) {
			if(serTaxCatMaster.code == undefined || serTaxCatMaster.code == null || serTaxCatMaster.code =="" ){
				return this.validationResponse(true, "code", $rootScope.nls["ERR06372"], serTaxCatMaster);
			}
			else{
				if(!CommonValidationService.checkRegExp(appConstant.Reg_Exp_Master_ServiceTaxCategory_Code,serTaxCatMaster.code)){
	  				return this.validationResponse(true, "code", $rootScope.nls["ERR06370"], serTaxCatMaster);
			   }
			}
		}
		
		return this.validationSuccesResponse(serTaxCatMaster);
	}
 
	this.validationSuccesResponse = function(serTaxCatMaster) {
	    return {error : false, obj : serTaxCatMaster};
	}
	
	this.validationResponse = function(err, elem, message, serTaxCatMaster) {
		return {error : err, errElement : elem, errMessage : message, obj : serTaxCatMaster};
	}
	
}]);