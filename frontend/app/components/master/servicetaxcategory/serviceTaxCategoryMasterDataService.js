app.service('serTaxCatMasterDataService', function($rootScope) {
    this.getHeadArray = function() {
	   return [
			  {
					"name":"#",
					"width":"col-xs-0half",
					"model":"no",
					"search":false
			  },{
					"name":"Name",
					"width":"col-md-1half",
					"model":"name",
					"search":true,
					"wrap_cell":true,
					"type":"text",
					"sort":true,
					"key":"name"
				},{
					"name":"Code",
					"width":"col-md-1half",
					"model":"code",
					"search":true,
					"type":"text",
					"sort":true,
					"key":"code"
				},{
					"name":"Status",
					"width":"col-md-2 col-xs-3",
					"model":"status",
					"search":true,
					"type":"drop",
					"data":$rootScope.enum['LovStatus'],
					"key":"status",
					"sort":true
				}];
          };

    
    this.getListBreadCrumb = function() {
        return [{
                label: "Master",
                state: "layout.serTaxCatMaster"
            },
            {
                label: "Service Tax Category",
                state: "layout.serTaxCatMaster"
            }
        ];
    }

    this.getAddBreadCrumb = function() {
        return [{
                label: "Master",
                state: "layout.serTaxCatMaster"
            },
            {
                label: "Service Tax Category",
                state: "layout.serTaxCatMaster"
            },
            {
                label: "Add Service Tax Category",
                state: null
            }
        ];
    };
    this.getEditBreadCrumb = function() {
        return [{
                label: "Master",
                state: "layout.serTaxCatMaster"
            },
            {
                label: "Service Tax Category",
                state: "layout.serTaxCatMaster"
            },
            {
                label: "Edit Service Tax Category",
                state: null
            }
        ];
    };
   

});