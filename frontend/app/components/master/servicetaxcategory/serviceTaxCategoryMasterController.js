(function(){
app.controller("serTaxCatMasterCtrl", ['$scope', '$rootScope', '$window', '$modal', '$state', '$stateParams', '$log', 'ngDialog', 'serTaxCatMasterDataService', 'serTaxCatMasterFactory', 'RecentHistorySaveService', 'serTaxCatMasterValidationService','ValidateUtil', 'Notification','roleConstant',
    function($scope, $rootScope, $window, $modal, $state, $stateParams, $log, ngDialog, serTaxCatMasterDataService, serTaxCatMasterFactory, RecentHistorySaveService, serTaxCatMasterValidationService, ValidateUtil, Notification, roleConstant ) {
	
	
	  var vm = this;
      vm.tableHeadArr = serTaxCatMasterDataService.getHeadArray();
      vm.dataArr = [];
      vm.errorArray = [];
      vm.searchData = {};
      vm.serTaxCatMaster = {};
      vm.serTaxCatMaster.taxPercentageList=[];
      vm.serTaxCatMaster.taxPercentageList.push({});
      vm.limitArr = [10, 15, 20];
      vm.page = 0;
      vm.limit = 10;
      vm.totalRecord = 100;
      vm.showHistory = false;
      vm.errorMap = new Map();
      vm.firstFocus = false;
      vm.errorFlag=false;
      vm.editFormFlag=false;
      vm.deskTopView = true;
      
      
     
      var errorOnRowIndex = null;
      vm.myOtherModal = $modal({scope:$scope,templateUrl: 'errorPopUp.html', show: false,keyboard:true});

      vm.errorShow = function(errorObj,index){
    	  $scope.errList = errorObj.errTxtArr;
          // Show when some event occurs (use $promise property to ensure the template has been loaded)
          errorOnRowIndex = index;
          vm.myOtherModal.$promise.then(vm.myOtherModal.show);
      };

      vm.sortSelection = {
          sortKey: "name",
          sortOrder: "asc"
      }
      
      vm.changeSearch = function(param) {
          $log.debug("Change Search is called." + param);
          for (var attrname in param) {
              vm.searchData[attrname] = param[attrname];
          }
          vm.page = 0;
          vm.search();
      }

      vm.sortChange = function(param) {
          $log.debug("sortChange is called. " + param);
          vm.searchData.orderByType = param.sortOrder.toUpperCase();
          vm.searchData.sortByColumn = param.sortKey;
          vm.search();
      }
      
      
      /**
       * serTaxCat Master List Page Search Data  initializing and calling search Mtd.
       */
      vm.searchInit = function() {
          vm.searchData = {};
          vm.searchData.orderByType = vm.sortSelection.sortOrder.toUpperCase();
          vm.searchData.sortByColumn = vm.sortSelection.sortKey;
          vm.search();
      };
      

      /**
       * Get serTaxCat Master's Based on Search Data.
       */
      vm.search = function() {
      	vm.serTaxCatMasterView=false;
          vm.searchData.selectedPageNumber = vm.page;
          vm.searchData.recordPerPage = vm.limit;
          vm.dataArr = [];
          serTaxCatMasterFactory.search.fetch(vm.searchData).$promise.then(
              function(data) {
                  if (data.responseCode == "ERR0") {
                      vm.totalRecord = data.responseObject.totalRecord;
                      vm.dataArr = data.responseObject.searchResult;
                  }
              },
              function(errResponse) {
              	 $log.error('Error while fetching serTaxCatMaster');
              });
         }
      
      
      
      
      
      /**
       * Validating field of Trigger Master while instant change.
       * @param {errorElement,errorCode}
       */
      vm.validate = function(valElement, code) {
          var validationResponse = serTaxCatMasterValidationService.validate(vm.serTaxCatMaster,code);
          if(validationResponse.error == true) {
				$scope.validationErrorAndFocus(validationResponse, validationResponse.errElement);
			} else {
				vm.errorMap.put(valElement, null);
          	validationResponse.error = false;
          }
      }
      
      
      
      
      /**
       * serTaxCat Master List Page Search Data  initializing and calling search Mtd.
       * @param {Object} data - The Selected serTaxCat Master from List page.
       * @param {string} data.id - The id of the Selected serTaxCat Master.
       * @param {int} index - The Selected row index from List page.
       */
      vm.rowSelect = function(data) {
    	  if($rootScope.roleAccess(roleConstant.MASTER_FINANCE_SERVICE_TAX_CATEGORY_VIEW)){
    		  vm.serTaxCatMasterView=true;
  			vm.serTaxCatMaster = data;
  			var windowInner=$window.innerWidth;
  			if(windowInner<=1199){
  				vm.deskTopView = false;
  			}
  			else{
  				vm.deskTopView = true;
  			}
  			angular.element($window).bind('resize', function(){
  				if($window.innerWidth>=1200){
  					vm.deskTopView = true;
  				}
  				else if($window.innerWidth<=1199){
  					if(vm.serTaxCatMaster.id!=null){
  						vm.deskTopView = false;
  					}
  					else {
  						vm.deskTopView = true;
  					}
  				}
  				$scope.$digest();
  			});
    	  }
		}
      
      

      
      
      /* Master Select Picker */
      vm.ajaxChargeEvent = function(object) {
          vm.searchDto = {};
          vm.searchDto.keyword = object == null ? "" : object;
          vm.searchDto.selectedPageNumber = 0;
          vm.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
          return serTaxCatMasterFactory.searchByCharge.query(vm.searchDto).$promise.then(function(data, status) {
                  if (data.responseCode == "ERR0") {
                      vm.chargeMasterList = data.responseObject.searchResult;
                      return vm.chargeMasterList;
                  }
              },
              function(errResponse) {
                  console.error('Error while fetching charge Master');
              }
          );

      }
      
      /* Master Select Picker */
      vm.ajaxGenledgerEvent = function(object) {
          vm.searchDto = {};
          vm.searchDto.keyword = object == null ? "" : object;
          vm.searchDto.selectedPageNumber = 0;
          vm.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
          return serTaxCatMasterFactory.searchByGenLdr.query(vm.searchDto).$promise.then(function(data, status) {
                  if (data.responseCode == "ERR0") {
                      vm.chargeMasterList = data.responseObject.searchResult;
                      return vm.chargeMasterList;
                  }
              },
              function(errResponse) {
                  console.error('Error while fetching genledger  Master');
              }
          );

      }
      
      /* Master Select Picker */
      vm.ajaxSubledgerEvent = function(object) {
          vm.searchDto = {};
          vm.searchDto.keyword = object == null ? "" : object;
          vm.searchDto.selectedPageNumber = 0;
          vm.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
          return serTaxCatMasterFactory.searchBySubLdr.query(vm.searchDto).$promise.then(function(data, status) {
                  if (data.responseCode == "ERR0") {
                      vm.chargeMasterList = data.responseObject.searchResult;
                      return vm.chargeMasterList;
                  }
              },
              function(errResponse) {
                  console.error('Error while fetching Subledger Master');
              }
          );

      }
      
      /* Master Select Picker */
      vm.ajaxCurrencyEvent = function(object) {
          vm.searchDto = {};
          vm.searchDto.keyword = object == null ? "" : object;
          vm.searchDto.selectedPageNumber = 0;
          vm.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
          return serTaxCatMasterFactory.searchByCurrency.query(vm.searchDto).$promise.then(function(data, status) {
                  if (data.responseCode == "ERR0") {
                      vm.chargeMasterList = data.responseObject.searchResult;
                      return vm.chargeMasterList;
                  }
              },
              function(errResponse) {
                  console.error('Error while fetching Currency Master');
              }
          );

      }
      
      
      /**
       * Modify Existing serTaxCat Master.
       * @param {int} id - The id of a Country.
       */
      vm.edit = function(id) {
    	  if($rootScope.roleAccess(roleConstant.MASTER_FINANCE_SERVICE_TAX_CATEGORY_MODIFY)){
    		  $state.go("layout.serTaxCatMasterEdit", {
                  id: id
              });
    	  }
      }
      
      // set status of serTaxCat master
      
      vm.setStatus = function(data){
  		if(data=='Active'){
  			return 'activetype';
  		} else if(data=='Block'){
  			return 'blockedtype';
  		}else if(data=='Hide'){
  			return 'hiddentype';
  		}
  	}
      
      
      /**
       * Go To Add serTaxCat Master.
       */
      vm.addNew = function() {
    	  if($rootScope.roleAccess(roleConstant.MASTER_FINANCE_SERVICE_TAX_CATEGORY_CREATE)){
    		  $state.go("layout.serTaxCatMasterAdd");
    	  }
      }
      
      
      /**
       * Create New serTaxCat Master.
       */
      
      vm.create = function() {
    	var errorFlag=false;
      	var validationResponse = serTaxCatMasterValidationService.validate(vm.serTaxCatMaster,0);
          if(validationResponse.error == true) {
          	$log.debug("Validation Response -- ", validationResponse);
          	$log.debug('Validation Failed to create serTaxCatMaster..')
				$scope.validationErrorAndFocus(validationResponse, validationResponse.errElement);
			}
          else {
        	  for(var index=0; index<vm.serTaxCatMaster.taxPercentageList.length;index++){
  	    		if(vm.validateServiceTaxPercentage(index)){
  	    			vm.errorFlag=true;
  	    		}
  	    	}
        	  if(vm.errorFlag){
        		  return;
        	  }
        	  else{
        	  
        	  for(var i=0;i<vm.serTaxCatMaster.taxPercentageList.length;i++){
        		  vm.serTaxCatMaster.taxPercentageList[i].validFrom=$rootScope.sendApiStartDateTime(vm.serTaxCatMaster.taxPercentageList[i].validFrom);
        	  }
          	serTaxCatMasterFactory.create.query(vm.serTaxCatMaster).$promise.then(function(data) {
		            if (data.responseCode == 'ERR0') {
		            	$log.debug("serTaxCat Saved Successfully Response :: ", data);
		            	Notification.success($rootScope.nls["ERR400"]);
		            	$state.go('layout.serTaxCatMaster');
		            } else {
		                $log.debug("Create serTaxCat Failed :" + data.responseDescription)
		            }

		        }, function(error) {
		        	 $log.debug("Create serTaxCat Failed : " + error)
		        });
          } 
          }
      };
      
      
      //Update Existing serTaxCat Master
      vm.update = function() {
    	 var errorFlag=false;
      	 var validationResponse = serTaxCatMasterValidationService.validate(vm.serTaxCatMaster,0);
           if(validationResponse.error == true) {
           	$log.debug("Validation Response -- ", validationResponse);
           	$log.debug('Validation Failed while Updating the serTaxCat..')
				$scope.validationErrorAndFocus(validationResponse, validationResponse.errElement);
			}
           else {
        	   for(var index=0; index<vm.serTaxCatMaster.taxPercentageList.length;index++){
     	    		if(vm.validateServiceTaxPercentage(index)){
     	    			vm.errorFlag=true;
     	    		}
     	    	}
        	   if(vm.errorFlag){
         		  return;
         	  }
         	  else{
        	   
        	   for(var i=0;i<vm.serTaxCatMaster.taxPercentageList.length;i++){
         		  vm.serTaxCatMaster.taxPercentageList[i].validFrom=$rootScope.sendApiStartDateTime(vm.serTaxCatMaster.taxPercentageList[i].validFrom);
         	  }
           	serTaxCatMasterFactory.update.query(vm.serTaxCatMaster).$promise.then(function(data) {
		            if (data.responseCode == 'ERR0') {
		            	$log.debug("serTaxCat Updated Successfully Response :: ", data);
		            	Notification.success($rootScope.nls["ERR401"]);
		            	$state.go('layout.serTaxCatMaster');
		            } else {
		                $log.debug("Updating serTaxCat Failed :" + data.responseDescription)
		            }
		        }, function(error) {
		        	 $log.debug("Updating serTaxCat Failed : " + error)
		        });
         	   } 
           }
       };
      
      
      /**
       * Go Back to serTaxCat Master List Page.
       */
      vm.backToList = function() {
      	vm.serTaxCatMasterView = false;
      	vm.serTaxCatMaster = {};
      	 vm.deskTopView = true;
      }
      
      
      /**
       * Cancel Add or Edit serTaxCat Master.
       * @param {int} id - The id of a serTaxCat
       */
     
      vm.cancel = function(objId) {
          if(vm.serTaxCatMasterForm!=undefined){
		    	vm.goTocancel(vm.serTaxCatMasterForm.$dirty,objId);
          }else{
          	vm.goTocancel(false,objId);
          }
      }
     
     vm.goTocancel = function(isDirty,objId){
   		 if (isDirty) {
   		      var newScope = $scope.$new();
   		      newScope.errorMessage = $rootScope.nls["ERR200"];
   		      ngDialog.openConfirm({
   		        template: '<p>{{errorMessage}}</p>' +
   		          '<div class="ngdialog-footer">' +
   		          ' <button type="button" class="btn btn-default btn-property cancel-btn" ng-click="confirm(2)">No</button>' +
   		          '<button type="button" class="btn btn-default btn-property accent-btn " ng-click="confirm(1)">Yes</button>' +
   		          '<button type="button" class="btn btn-default btn-property pull-left grey-cancel-btn" ng-click="confirm(3)">Cancel</button>' +
   		          '</div>',
   		        plain: true,
   		        closeByDocument: false,
   		        scope: newScope,
   		        className: 'ngdialog-theme-default'
   		      }).then(
   		        function(value) {
   		          if (value == 1) {
   		            if(objId!=null && objId != undefined){
   		            	vm.update();
   		            }else{
   		            	vm.create();
   		            }
   		          } else if (value == 2) {
   		        	 $state.go("layout.serTaxCatMaster", {
   		                submitAction: "Cancelled"
   		            });
   		          } else {
   		            $log.debug("cancelled");
   		          }
   		        });
   		    } else {
   		     $state.go("layout.serTaxCatMaster", {
                 submitAction: "Cancelled"
             });
   		    }
   	    }

      
      
      /**
       * Get serTaxCat Master By id.
       * @param {int} id - The id of a serTaxCat.
       * @param {boolean} isView - TO identify purpose of calling is for View or Edit a serTaxCat.
       */
     
      vm.view = function(id, isView) {
      	serTaxCatMasterFactory.findById.query({
              id: id
          }).$promise.then(function(data) {
              if (data.responseCode == 'ERR0') {
                  vm.serTaxCatMaster = data.responseObject;
                  for(var i=0;i<vm.serTaxCatMaster.taxPercentageList.length;i++){
            		  vm.serTaxCatMaster.taxPercentageList[i].validFrom=$rootScope.dateAndTimeToString(vm.serTaxCatMaster.taxPercentageList[i].validFrom);
            	  }
                  var rHistoryObj = {}

                  if (vm.serTaxCatMaster != undefined && vm.serTaxCatMaster != null && vm.serTaxCatMaster.id != undefined && vm.serTaxCatMaster.id != null) {
                      $rootScope.subTitle = vm.serTaxCatMaster.serTaxCatName;
                  } else {
                      $rootScope.subTitle = "Unknown serTaxCat"
                  }
                  $rootScope.unfinishedFormTitle = "serTaxCat  # " + vm.serTaxCatMaster.serTaxCatCode;
                  if (isView) {
                      rHistoryObj = {
                          'title': "serTaxCat #" + vm.serTaxCatMaster.serTaxCatCode,
                          'subTitle': $rootScope.subTitle,
                          'stateName': $state.current.name,
                          'stateParam': JSON.stringify($stateParams),
                          'stateCategory': 'serTaxCat Master',
                          'serviceType': undefined,
                          'showService': false
                      }

                  } else {

                      rHistoryObj = {
                          'title': 'Edit serTaxCat # ' + $stateParams.serviceId,
                          'subTitle': $rootScope.subTitle,
                          'stateName': $state.current.name,
                          'stateParam': JSON.stringify($stateParams),
                          'stateCategory': 'serTaxCat Master',
                          'serviceType': 'MASTER',
                          'serviceCode': ' ',
                          'orginAndDestination': ' '
                      }
                  }
                  RecentHistorySaveService.form(rHistoryObj);
              } else {
                  $log.debug("Exception while getting serTaxCat by Id ", data.responseDescription);
              }
          }, function(error) {
              $log.debug("Error ", error);
          });
      };
      
      /**
       * Delete serTaxCat Master.
       * @param {int} id - The id of a serTaxCat.
       */
      vm.delete = function(id) {
    	  if($rootScope.roleAccess(roleConstant.MASTER_FINANCE_SERVICE_TAX_CATEGORY_DELETE)){
    		  var newScope = $scope.$new();
  			newScope.errorMessage = $rootScope.nls["ERR06379"];
  	    	ngDialog.openConfirm( {
  	                    template : '<p>{{errorMessage}}</p>'
  	                    + '<div class="ngdialog-footer">'
  	                    + '<button type="button" class="btn btn-default btn-property cancel-btn" ng-click="closeThisDialog(0)">Cancel'
  	                    + '<button type="button" class="btn btn-primary btn-property accent-btn" ng-click="confirm(1)">Yes'
  	                    + '</button></div>',
  	                    plain : true,
  	                    scope : newScope,
  	                    closeByDocument: false,
  	                    className : 'ngdialog-theme-default'
  	                }).then( function(value) {
  	                	serTaxCatMasterFactory.delete.query({
  	                        id: id
  	                    }).$promise.then(function(data) {
  	                        if (data.responseCode == 'ERR0') {
  	                        	vm.serTaxCatMasterView=false;
  	                        	Notification.success($rootScope.nls["ERR402"]);
  	                        	vm.deskTopView = true;
  	                            vm.search();
  	                        }
  	                    }, function(error) {
  	                        $log.debug("Error ", error);
  	                    });
  	                }, function(value) {
  	                    $log.debug("delete Cancelled");
  	            });
    	  }
      }
      
      
      vm.addServiceTaxPercentage = function() {
    	    console.log("adding Row is clicked");
    	    vm.firstFocus = true;
    	    vm.errorArray=[];
    	    vm.errorFlag=false;
    	    if(vm.serTaxCatMaster.taxPercentageList!= undefined && vm.serTaxCatMaster.taxPercentageList!=null && vm.serTaxCatMaster.taxPercentageList.length>0)
    	    	{
    	    	for(var index=0; index<vm.serTaxCatMaster.taxPercentageList.length;index++){
    	    		if(vm.validateServiceTaxPercentage(index)){
    	    			vm.errorFlag=true;
    	    		}
    	    	}
    	    	if(vm.errorFlag){
    	    		return;
    	    	}else
    	    		{
    	    		vm.serTaxCatMaster.taxPercentageList.push({}); 
    	    		}
    	    	}else{
    		   vm.serTaxCatMaster.taxPercentageList.push({}); 
    	    	}
    	   }
      
      vm.selectedMaster = function(id){
    	  $rootScope.navigateToNextField(id);
      }
      
      
      
      vm.validateServiceTaxPercentage=function(index){
    	  vm.errorArray[index]={};
    	  vm.errorArray[index].errTxtArr=[];
    	  var errorFound=false;
    	  
    	  if(vm.serTaxCatMaster.taxPercentageList[index].taxName==undefined || 
    			  vm.serTaxCatMaster.taxPercentageList[index].taxName==null || 
    			  vm.serTaxCatMaster.taxPercentageList[index].taxName.length==0){
    		  vm.errorFound=true;
    		  vm.errorArray[index].errRow=true;
    		  vm.errorArray[index].taxName=true;
    		  vm.errorArray[index].errTxtArr.push($rootScope.nls["ERR06381"]);
    		  errorFound=true;
    	  }
    	  else{
    		  vm.errorArray[index].taxName=false;
    	  }
    	  
    	  if(vm.serTaxCatMaster.taxPercentageList[index].taxCode==undefined || 
    			  vm.serTaxCatMaster.taxPercentageList[index].taxCode==null || 
    			  vm.serTaxCatMaster.taxPercentageList[index].taxCode.length==0){
    		  vm.errorFound=true;
    		  vm.errorArray[index].errRow=true;
    		  vm.errorArray[index].taxCode=true;
    		  vm.errorArray[index].errTxtArr.push($rootScope.nls["ERR06380"]);
    		  errorFound=true;
    	  }
    	  else{
    		  vm.errorArray[index].taxCode=false;
    	  }
    	  if(vm.serTaxCatMaster.taxPercentageList[index].currencyMaster==undefined || vm.serTaxCatMaster.taxPercentageList[index].currencyMaster==null){
    		  vm.errorFound=true;
    		  vm.errorArray[index].errRow=true;
    		  vm.errorArray[index].currencyCode=true;
    		  vm.errorArray[index].errTxtArr.push($rootScope.nls["ERR06389"]);
    		  errorFound=true;
    	  }
    	  else{
    		  
    		  if(ValidateUtil.isStatusBlocked(vm.serTaxCatMaster.taxPercentageList[index].currencyMaster.status)){
        		  vm.errorFound=true;
        		  vm.errorArray[index].errRow=true;
        		  vm.errorArray[index].currencyCode=true;
        		  vm.errorArray[index].errTxtArr.push("ERR06392");
        		  errorFound=true;
        	  }
        	  else{
        		  vm.errorArray[index].currencyCode=false;
        	  }
    	  }
    	  
    	  if(vm.serTaxCatMaster.taxPercentageList[index].percentage==undefined || 
    			  vm.serTaxCatMaster.taxPercentageList[index].percentage==null || 
    			  vm.serTaxCatMaster.taxPercentageList[index].percentage.length==0){
    		  vm.errorFound=true;
    		  vm.errorArray[index].errRow=true;
    		  vm.errorArray[index].percentage=true;
    		  vm.errorArray[index].errTxtArr.push($rootScope.nls["ERR06383"]);
    		  errorFound=true;
    	  }
    	  else{
    		  vm.errorArray[index].percentage=false;
    	  }
    	  if(vm.serTaxCatMaster.taxPercentageList[index].validFrom==undefined || 
    			  vm.serTaxCatMaster.taxPercentageList[index].validFrom==null || 
    			  vm.serTaxCatMaster.taxPercentageList[index].validFrom.lenght==0){
    		  vm.errorFound=true;
    		  vm.errorArray[index].errRow=true;
    		  vm.errorArray[index].validFrom=true;
    		  vm.errorArray[index].errTxtArr.push($rootScope.nls["ERR06382"]);
    		  errorFound=true;
    	  }
    	  else{
    		  vm.errorArray[index].validFrom=false;
    	  }
    	  if(vm.serTaxCatMaster.taxPercentageList[index].generalLedgerMaster==undefined || vm.serTaxCatMaster.taxPercentageList[index].generalLedgerMaster==null){
    		  vm.errorFound=true;
    		  vm.errorArray[index].errRow=true;
    		  vm.errorArray[index].accountCode=true;
    		  vm.errorArray[index].errTxtArr.push($rootScope.nls["ERR06388"]);
    		  errorFound=true;
    	  }
    	  else{
    		  if(ValidateUtil.isStatusBlocked(vm.serTaxCatMaster.taxPercentageList[index].generalLedgerMaster.status)){
        		  vm.errorFound=true;
        		  vm.errorArray[index].errRow=true;
        		  vm.errorArray[index].currencyCode=true;
        		  vm.errorArray[index].errTxtArr.push("ERR06391");
        		  errorFound=true;
        	  }
        	  else{
        		  vm.errorArray[index].currencyCode=false;
        	  }
    	  }
    	  
    	  if(vm.serTaxCatMaster.taxPercentageList[index].subLedgerMaster==undefined || vm.serTaxCatMaster.taxPercentageList[index].subLedgerMaster==null){
    		  vm.errorFound=true;
    		  vm.errorArray[index].errRow=true;
    		  vm.errorArray[index].subLedgerCode=true;
    		  vm.errorArray[index].errTxtArr.push($rootScope.nls["ERR06387"]);
    		  errorFound=true;
    	  }
    	  else{
    		  if(ValidateUtil.isStatusBlocked(vm.serTaxCatMaster.taxPercentageList[index].subLedgerMaster.status)){
        		  vm.errorFound=true;
        		  vm.errorArray[index].errRow=true;
        		  vm.errorArray[index].currencyCode=true;
        		  vm.errorArray[index].errTxtArr.push("ERR06392");
        		  errorFound=true;
        	  }
        	  else{
        		  vm.errorArray[index].currencyCode=false;
        	  }
    	  }
    	  return errorFound;
    	  
      }
    	  

		vm.removeServiceTaxPercentage = function(index) {
		    console.log("remove removeGeneralOriginCharge  is called ", index);
		    vm.serTaxCatMaster.taxPercentageList.splice(index, 1);
		
		  }
      

      
    /**
     * Common Validation Focus Functionality - starts here" 
     */
		$scope.validationErrorAndFocus = function(valResponse, elemId){
			vm.errorMap = new Map();
			if(valResponse.error == true){
				vm.errorMap.put(valResponse.errElement, valResponse.errMessage);
			}
				$scope.navigateToNextField(elemId);
		}
      
      
      /**
       * State Identification Handling Here.
       * @param {String} $stateParams.action - VIEW, ADD, EDIT, SEARCH.
       */
      //
      switch ($stateParams.action) {
          case "ADD":
          	 if ($stateParams.fromHistory === 'Yes') {
                   if (vm.isReloaded == "YES") {
                    vm.isReloaded = "NO";
                    localStorage.isserTaxCatMasterReloaded = "NO";
                     vm.serTaxCatMaster = JSON.parse(localStorage.reloadFormData);
                   } else {
                     vm.serTaxCatMaster = $rootScope.selectedUnfilledFormData;
                   }
                 } else {
                   if (vm.isReloaded == "YES") {
                     vm.isReloaded = "NO";
                     localStorage.isserTaxCatMasterReloaded = "NO";
                     vm.serTaxCatMaster = JSON.parse(localStorage.reloadFormData);
                   } else {
                       $log.debug("Add NotHistory NotReload...")
                   }
                 }
          	 	vm.editFormFlag=true;
              $rootScope.breadcrumbArr = serTaxCatMasterDataService.getAddBreadCrumb();
              break;
          case "EDIT":
          	 if (vm.fromHistory === 'Yes') {
                   if (vm.isReloaded == "YES") {
                     vm.isReloaded = "NO";
                     localStorage.isserTaxCatMasterReloaded = "NO";
                     vm.serTaxCatMaster = JSON.parse(localStorage.reloadFormData);
                   } else {
                     vm.serTaxCatMaster = $rootScope.selectedUnfilledFormData;
                   }
                 } else {
                   if (vm.isReloaded == "YES") {
                     vm.isReloaded = "NO";
                     localStorage.isserTaxCatMasterReloaded = "NO";
                     vm.serTaxCatMaster = JSON.parse(localStorage.reloadFormData);
                   } else {
                  	 vm.view($stateParams.id, false);
                   }
                 }
          	 vm.editFormFlag=false;
              $rootScope.breadcrumbArr = serTaxCatMasterDataService.getEditBreadCrumb();
              break;
          default:
              $rootScope.breadcrumbArr = serTaxCatMasterDataService.getListBreadCrumb();
              vm.searchInit();
      }
      
 
}
]);
}) ();