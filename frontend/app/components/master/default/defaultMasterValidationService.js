app.service('defaultMasterValidationService', ['$rootScope','ValidateUtil', 'CommonValidationService', 'appConstant', function($rootScope, ValidateUtil, CommonValidationService, appConstant) {
	this.validate = function(defaultObj, code) {
		
		//Validate  default Name
	    if(code == 0 || code == 1) {
			if (defaultObj.defaultName == null ||defaultObj.defaultName == undefined  || defaultObj.defaultName == "") {
				return this.validationResponse(true, "defaultName", $rootScope.nls["ERR06004"], defaultObj);
			}
		}
	    
	    if(code == 0 || code == 2) {
			if (defaultObj.code == null ||defaultObj.code == undefined  || defaultObj.code == "") {
				return this.validationResponse(true, "code", $rootScope.nls["ERR06000"], defaultObj);
			}
		}
	    
	    if(code == 0 || code == 3) {
			if (defaultObj.softwareMaster == null ||defaultObj.softwareMaster == undefined  || defaultObj.softwareMaster == "") {
				return this.validationResponse(true, "softwareName", $rootScope.nls["ERR06801"], defaultObj);
			}
			else{
				if(defaultObj.softwareMaster.status=="Block"){
					return this.validationResponse(true, "softwareName", $rootScope.nls["ERR06808"], defaultObj);	
				}
				else if(defaultObj.softwareMaster.status=="Hide"){
					return this.validationResponse(true, "softwareName", $rootScope.nls["ERR06809"], defaultObj);	
				}
			}
			
		}
	    
	    if(code == 0 || code == 4) {
			if (defaultObj.objectGroupMaster == null ||defaultObj.objectGroupMaster == undefined  || defaultObj.objectGroupMaster == "") {
				return this.validationResponse(true, "objectGroupName", $rootScope.nls["ERR06901"], defaultObj);
			}else{
				if(code == 4){
					defaultObj.objectSubGroupMaster = null;	
				}
				if(defaultObj.objectGroupMaster.status=="Block"){
					return this.validationResponse(true, "objectGroupName", $rootScope.nls["ERR06118"], defaultObj);	
				}
				else if(defaultObj.objectGroupMaster.status=="Hide"){
					return this.validationResponse(true, "objectGroupName", $rootScope.nls["ERR06119"], defaultObj);	
				}
			}
		}
	    
	    if(code == 0 || code == 5) {
			if (defaultObj.objectSubGroupMaster == null ||defaultObj.objectSubGroupMaster == undefined  || defaultObj.objectSubGroupMaster == "") {
				return this.validationResponse(true, "objectSubGroupName", $rootScope.nls["ERR06111"], defaultObj);
			}else{
				if(defaultObj.objectSubGroupMaster.status=="Block"){
					return this.validationResponse(true, "objectSubGroupName", $rootScope.nls["ERR06120"], defaultObj);	
				}
				else if(defaultObj.objectSubGroupMaster.status=="Hide"){
					return this.validationResponse(true, "objectSubGroupName", $rootScope.nls["ERR06121"], defaultObj);	
				}
			}
		}
	    
	    if(code == 0 || code == 6) {
			if (defaultObj.value == null ||defaultObj.value == undefined  || defaultObj.value == "") {
				return this.validationResponse(true, "value", $rootScope.nls["ERR06006"], defaultObj);
			}
		}
	    
	    if(code == 0 || code == 7) {
			
		}
	    
	    
		return this.validationSuccesResponse(defaultObj);
	}
 
	this.validationSuccesResponse = function(defaultObj) {
	    return {error : false, obj : defaultObj};
	}
	
	this.validationResponse = function(err, elem, message, defaultObj) {
		return {error : err, errElement : elem, errMessage : message, obj : defaultObj};
	}
	
}]);