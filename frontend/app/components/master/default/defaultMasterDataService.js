app.service('defaultMasterDataService', function($rootScope) {
    this.getHeadArray = function() {
        return [ {
    		"name" : "#",
    		"width" : "col-xs-0half",
    		"model" : "no",
    		"search" : false,
    	}, {
    		"name" : "Name",
    		"width" : "w150px ",
    		"model" : "defaultName",
    		"search" : true,
    		"wrap_cell" : true,
    		"type" : "text",
    		"sort" : true,
      		"key" : "searchDefaultName"
    	}, {
    		"name" : "Code",
    		"width" : "w100px",
    		"model" : "code",
    		"search" : true,
    		"type" : "text",
    		"sort" : true,
    		"key" : "searchDefaultCode"
    	},{
    		"name" : "Value",
    		"width" : "w100px",
    		"model" : "value",
    		"search" : true,
    		"sort" : true,
    		"type" : "text",
    		"key" : "searchValue"
    	}, {
    		"name" : "Software",
    		"width" : "w150px",
    		"model" : "softwareMaster.softwareName",
    		"search" : true,
    		"type" : "text",
    		"sort" : true,
    		"key" : "searchSoftware"
    	}, {
    		"name" : "Group",
    		"width" : "w150px",
    		"model" : "objectGroupMaster.objectGroupName",
    		"search" : true,
    		"type" : "text",
    		"sort" : true,
    		"key" : "searchGroup"
    	}, {
    		"name" : "Sub Group",
    		"width" : "w100px",
    		"model" : "objectSubGroupMaster.objectSubGroupName",
    		"search" : true,
    		"sort" : true,
    		"type" : "text",
    		"key" : "searchSubGroup"
    	}, {
    		"name" : "Scope Flag",
    		"width" : "w100px",
    		"model" : "scopeFlag",
    		"search" : true,
    		"sort" : true,
    		"type" : "drop",
    		"data" : $rootScope.enum['ScopeFlag'],
    		"key" : "searchScopeFlag"
    	}];
    };

    
    this.getListBreadCrumb = function() {
        return [{
                label: "Master",
                state: "layout.defaultMaster"
            },
            {
                label: "Default",
                state: "layout.defaultMaster"
            }
        ];
    }

    this.getAddBreadCrumb = function() {
        return [{
                label: "Master",
                state: "layout.defaultMaster"
            },
            {
                label: "Default",
                state: "layout.defaultMaster"
            },
            {
                label: "Add Default",
                state: null
            }
        ];
    };
    this.getEditBreadCrumb = function() {
        return [{
                label: "Master",
                state: "layout.defaultMaster"
            },
            {
                label: "Default",
                state: "layout.defaultMaster"
            },
            {
                label: "Edit Default",
                state: null
            }
        ];
    };
    this.getViewBreadCrumb = function() {
        return [{
                label: "Master",
                state: "layout.defaultMaster"
            },
            {
                label: "Default",
                state: "layout.defaultMaster"
            },
            {
                label: "View Default",
                state: null
            }
        ];
    };

});