(function(){
app.controller("DefaultMasterCtrl", ['$scope', '$rootScope', '$window', '$modal', '$state', '$stateParams', '$log', 'ngDialog', 'defaultMasterDataService', 'defaultMasterFactory', 'RecentHistorySaveService', 'defaultMasterValidationService', 'Notification','objectGroupMasterFactory','softwareMasterFactory','objectSubGroupMasterFactory','roleConstant',
    function($scope, $rootScope, $window, $modal, $state, $stateParams, $log, ngDialog, defaultMasterDataService, defaultMasterFactory, RecentHistorySaveService, defaultMasterValidationService, Notification,objectGroupMasterFactory, softwareMasterFactory, objectSubGroupMasterFactory,roleConstant) {

        var vm = this;
        vm.tableHeadArr = defaultMasterDataService.getHeadArray();
        vm.dataArr = [];
        vm.defaultMasterData = {};
        vm.showHistory = false;
        vm.errorMap = new Map();
        vm.scopeFlagArr = $rootScope.enum['ScopeFlag'];
        vm.sortSelection = {
            sortKey: "defaultName",
            sortOrder: "asc"
        }
        
        vm.changeSearch = function(param) {
            $log.debug("Change Search is called." + param);
            for (var attrname in param) {
                vm.searchData[attrname] = param[attrname];
            }
            vm.page = 0;
            vm.search();
        }

        vm.sortChange = function(param) {
            $log.debug("sortChange is called. " + param);
            vm.searchData.orderByType = param.sortOrder.toUpperCase();
            vm.searchData.sortByColumn = param.sortKey;
            vm.search();
        }
        
       
        
        /**
         * Default Master List Navigate to selected page.
         * @param {Object} param - The Selected page option.
         */
        vm.changepage = function(param) {
            $log.debug("change Page..." + param.page);
            vm.page = param.page;
            vm.limit = param.size;
            vm.search();
        }

        /**
         * Default Master List Page Limit Data change.
         * @param {int} nLimit - The Selected limit option from Limit array.
         */
        vm.limitChange = function(nLimit) {
            $log.debug("Limit Change..." + nLimit);
            vm.page = 0;
            vm.limit = nLimit;
            vm.search();
        }

        /**
         * Default Master List Page Search Data  initializing and calling search Mtd.
         * @param {Object} data - The Selected Default Master from List page.
         * @param {string} data.id - The id of the Selected Default Master.
         * @param {int} index - The Selected row index from List page.
         */
        vm.rowSelect = function(data, index) {
        	if($rootScope.roleAccess(roleConstant.MASTER_GENERAL_DEFAULT_VIEW)){

                vm.selectedRecordIndex = (vm.searchData.recordPerPage * vm.searchData.selectedPageNumber) + index;
                var param = {
                    id: data.id,
                    selectedPageNumber: vm.selectedRecordIndex,
                    totalRecord: vm.totalRecord
                }

                $state.go("layout.defaultMasterView", vm.searchDtoToStateParams(param));
            
        	}
        }

        /**
         * Default Master List Page Search Data  initializing and calling search Mtd.
         */
        vm.searchInit = function() {
            vm.searchData = {};
            vm.limitArr = [10, 15, 20];
            vm.page = 0;
            vm.limit = 10;
            vm.totalRecord = 10;
            vm.searchData.orderByType = vm.sortSelection.sortOrder.toUpperCase();
            vm.searchData.sortByColumn = vm.sortSelection.sortKey;
            vm.search();
        };
        
        /**
         * Get Default Master's Based on Search Data.
         */
        vm.search = function() {
            vm.searchData.selectedPageNumber = vm.page;
            vm.searchData.recordPerPage = vm.limit;

            vm.dataArr = [];
            defaultMasterFactory.search.query(vm.searchData).$promise.then(
                function(data) {
                    if (data.responseCode == "ERR0") {
                    	vm.totalRecord = data.responseObject.totalRecord;
                        vm.dataArr = data.responseObject.searchResult;
                    }
                },
                function(errResponse) {
                    $log.error('Error while fetching Countries');
                });
        }
        
        /**
         * Go Back to Default Master List Page.
         */
        vm.backToList = function() {
            $state.go("layout.defaultMaster", {submitAction: "Cancelled"});
        }

        /**
         * Get Next or Previous Default Master Based on id and index.
         * @param {int} nxtIdx - The nxtIdx(-1, or 1) for view page .
         */
        
        vm.singlePageNavigation = function(val) {
	    	if($stateParams != undefined && $stateParams != null) {
	            if ((parseInt($stateParams.selectedPageNumber) + val) < 0 || (parseInt($stateParams.selectedPageNumber) + val) >= parseInt($stateParams.totalRecord))
	                return;
	            var stateParameters = {};
	            vm.searchData = {};
	            vm.searchData.recordPerPage = 1;
	            vm.searchData.selectedPageNumber = parseInt($stateParams.selectedPageNumber) + val;
				if($stateParams.sortByColumn != undefined && $stateParams.sortByColumn != null) {
					stateParameters.sortByColumn = vm.searchData.sortByColumn = $stateParams.sortByColumn;
	        	}
				if($stateParams.orderByType != undefined && $stateParams.orderByType != null) {
					stateParameters.orderByType = vm.searchData.orderByType = $stateParams.orderByType;
	        	}
				if($stateParams.recordPerPage != undefined && $stateParams.recordPerPage != null) {
					stateParameters.recordPerPage = vm.searchData.recordPerPage = $stateParams.recordPerPage;
	        	}
				if($stateParams.searchDefaultName != undefined && $stateParams.searchDefaultName != null) {
					stateParameters.searchDefaultName = vm.searchData.searchDefaultName = $stateParams.searchDefaultName;
	        	}
				if($stateParams.searchDefaultCode != undefined && $stateParams.searchDefaultCode != null) {
					stateParameters.searchDefaultCode = vm.searchData.searchDefaultCode = $stateParams.searchDefaultCode;
	        	}
				if($stateParams.searchValue != undefined && $stateParams.searchValue != null) {
					stateParameters.searchValue = vm.searchData.searchValue = $stateParams.searchValue;
	        	}
				if($stateParams.searchSoftware != undefined && $stateParams.searchSoftware != null) {
					stateParameters.searchSoftware = vm.searchData.searchSoftware = $stateParams.searchSoftware;
	        	}
				if($stateParams.searchGroup != undefined && $stateParams.searchGroup != null) {
					stateParameters.searchGroup = vm.searchData.searchGroup = $stateParams.searchGroup;
	        	}
				if($stateParams.searchSubGroup != undefined && $stateParams.searchSubGroup != null) {
					stateParameters.searchSubGroup = vm.searchData.searchSubGroup = $stateParams.searchSubGroup;
	        	}
				if($stateParams.searchScopeFlag != undefined && $stateParams.searchScopeFlag != null) {
					stateParameters.searchScopeFlag = vm.searchData.searchScopeFlag = $stateParams.searchScopeFlag;
	        	}
				
	    	} else {
	    		return;
	    	}
	    	defaultMasterFactory.search.query(vm.searchData).$promise.then(function(data, status) {
	            vm.totalRecord = data.responseObject.totalRecord;
				stateParameters.selectedPageNumber = vm.searchData.selectedPageNumber;
	            stateParameters.id =  data.responseObject.searchResult[0].id;
	            stateParameters.totalRecord = vm.totalRecord;
	            $state.go("layout.defaultMasterView", stateParameters);
			});
	    }
        
        vm.searchDtoToStateParams = function(param) {
	    	if(param == undefined || param == null) {
	    		param = {};
	    	}
	    	if(vm.searchData != undefined && vm.searchData != null) {
	    		if(vm.searchData != undefined && vm.searchData != null) {
	        		param.recordPerPage = 1;
	    			if(vm.searchData.sortByColumn != undefined && vm.searchData.sortByColumn != null) {
	            		param.sortByColumn = vm.searchData.sortByColumn;
	            	}
	    			if(vm.searchData.orderByType != undefined && vm.searchData.orderByType != null) {
	            		param.orderByType = vm.searchData.orderByType;
	            	}
	    			if(vm.searchData.searchDefaultName != undefined && vm.searchData.searchDefaultName != null) {
	            		param.searchDefaultName = vm.searchData.searchDefaultName;
	            	}
	    			if(vm.searchData.searchDefaultCode != undefined && vm.searchData.searchDefaultCode != null) {
	            		param.searchDefaultCode = vm.searchData.searchDefaultCode;
	            	}
	    			if(vm.searchData.searchValue != undefined && vm.searchData.searchValue != null) {
	            		param.searchValue = vm.searchData.searchValue;
	            	}
	    			if(vm.searchData.searchSoftware != undefined && vm.searchData.searchSoftware != null) {
	            		param.searchSoftware = vm.searchData.searchSoftware;
	            	}
	    			if(vm.searchData.searchGroup != undefined && vm.searchData.searchGroup != null) {
	            		param.searchGroup = vm.searchData.searchGroup;
	            	}
	    			if(vm.searchData.searchSubGroup != undefined && vm.searchData.searchSubGroup != null) {
	            		param.searchSubGroup = vm.searchData.searchSubGroup;
	            	}
	    			if(vm.searchData.searchScopeFlag != undefined && vm.searchData.searchScopeFlag != null) {
	            		param.searchScopeFlag = vm.searchData.searchScopeFlag;
	            	}
	        	}
	    	}
	    	return param;
	    }


        /**
         * Get Default Master By id.
         * @param {int} id - The id of a Default.
         * @param {boolean} isView - TO identify purpose of calling is for View or Edit a Default.
         */
        vm.view = function(id, isView) {
            defaultMasterFactory.findById.query({
                id: id
            }).$promise.then(function(data) {
                if (data.responseCode == 'ERR0') {
                    vm.defaultMasterData = data.responseObject;
                    if (vm.defaultMasterData != undefined && vm.defaultMasterData != null && vm.defaultMasterData.id != undefined && vm.defaultMasterData.id != null) {
                        $rootScope.subTitle = vm.defaultMasterData.defaultName;
                    } else {
                        $rootScope.subTitle = "Unknown Default"
                    }
                    $rootScope.unfinishedFormTitle = "Default View (" + vm.defaultMasterData.code + ")";
                } else {
                    $log.debug("Exception while getting Default by Id ", data.responseDescription);
                }

            }, function(error) {
                $log.debug("Error ", error);
            });

        };

        /**
         * Go To Add Default Master.
         */
        vm.addNew = function() {
        	if($rootScope.roleAccess(roleConstant.MASTER_GENERAL_DEFAULT_CREATE)){
        		$state.go("layout.defaultMasterAdd");
        	}
        }
      
        /**
         * Cancel Add or Edit Default Master.
         */
       
        vm.cancel = function() {
            if(vm.defaultMasterDataForm!=undefined){
		    	vm.goTocancel(vm.defaultMasterDataForm.$dirty);
            }else{
            	vm.goTocancel(false);
            }
        }
       
       vm.goTocancel = function(isDirty){
     		 if (isDirty) {
     		      var newScope = $scope.$new();
     		      newScope.errorMessage = $rootScope.nls["ERR200"];
     		      ngDialog.openConfirm({
     		        template: '<p>{{errorMessage}}</p>' +
     		          '<div class="ngdialog-footer">' +
     		          ' <button type="button" class="btn btn-default btn-property cancel-btn" ng-click="confirm(2)">No</button>' +
     		          '<button type="button" class="btn btn-default btn-property accent-btn " ng-click="confirm(1)">Yes</button>' +
     		          '<button type="button" class="btn btn-default btn-property pull-left grey-cancel-btn" ng-click="confirm(3)">Cancel</button>' +
     		          '</div>',
     		        plain: true,
     		        closeByDocument: false,
     		        scope: newScope,
     		        className: 'ngdialog-theme-default'
     		      }).then(
     		        function(value) {
     		          if (value == 1) {
     		        	 vm.saveOrUpdate();
     		          } else if (value == 2) {
     		        	 var params = {};
      					params.submitAction = 'Cancelled';
     		        	 $state.go("layout.defaultMaster", params);
     		          } else {
     		            $log.debug("cancelled");
     		          }
     		        });
     		    } else {
     		         $state.go("layout.defaultMaster", {submitAction: "Cancelled"});
     		    }
     	    }

        /**
         * Modify Existing Default Master.
         * @param {int} id - The id of a Default.
         */
        vm.edit = function(id) {
        	if($rootScope.roleAccess(roleConstant.MASTER_GENERAL_DEFAULT_MODIFY)){
        		 $state.go("layout.defaultMasterEdit", {
                     id: id
                 });	
        	}
        }

        /**
         * Delete Default Master.
         * @param {int} id - The id of a Default.
         */
        vm.delete = function(id) {
        	if($rootScope.roleAccess(roleConstant.MASTER_GENERAL_DEFAULT_DELETE)){

            	var newScope = $scope.$new();
    			newScope.errorMessage =$rootScope.nls["ERR06008"];
    	    	ngDialog.openConfirm( {
    	                    template : '<p>{{errorMessage}}</p>'
    	                    + '<div class="ngdialog-footer">'
    	                    + '<button type="button" class="btn btn-default btn-property cancel-btn" ng-click="closeThisDialog(0)">Cancel'
    	                    + '<button type="button" class="btn btn-primary btn-property accent-btn" ng-click="confirm(1)">Yes'
    	                    + '</button></div>',
    	                    plain : true,
    	                    scope : newScope,
    	                    closeByDocument: false,
    	                    className : 'ngdialog-theme-default'
    	                }).then( function(value) {
    	                	defaultMasterFactory.delete.query({
    	                        id: id
    	                    }).$promise.then(function(data) {
    	                        if (data.responseCode == 'ERR0') {
    	                        	Notification.success($rootScope.nls["ERR402"]);
    	                            $state.go("layout.defaultMaster", {
    	                                submitAction: "Cancelled"
    	                            });
    	                        }
    	                    }, function(error) {
    	                        $log.debug("Error ", error);
    	                    });
    	                }, function(value) {
    	                    $log.debug("delete Cancelled");
    	            });
            
        	}
        }
        
        vm.validate = function(valElement,code,nextId) {
            var validationResponse = defaultMasterValidationService.validate(vm.defaultMasterData,code);
            if(validationResponse.error == true) {
				$scope.validationErrorAndFocus(validationResponse, validationResponse.errElement);
			} else {
				if(nextId!= undefined && nextId != null){
					$scope.navigateToNextField(nextId);
				}
				vm.errorMap.put(valElement, null);
            	validationResponse.error = false;
            }
        }
        
        /**
         * Common Validation Focus Functionality - starts here" 
         */
  		$scope.validationErrorAndFocus = function(valResponse, elemId){
  			vm.errorMap = new Map();
  			if(valResponse.error == true){
  				vm.errorMap.put(valResponse.errElement, valResponse.errMessage);
  			}
  				$scope.navigateToNextField(elemId);
  		}
          

        /**
         * save or update Default Master.
         */
        vm.saveOrUpdate = function() {
            var validationResponse = defaultMasterValidationService.validate(vm.defaultMasterData,0);
            if(validationResponse.error == true) {
            	$log.debug("Validation Response -- ", validationResponse);
				$scope.validationErrorAndFocus(validationResponse, validationResponse.errElement);
			}
            else {
            	if(vm.defaultMasterData.id == null) {
            		var successMessage = $rootScope.nls["ERR400"];
            		vm.defaultMasterData.location = $rootScope.userProfile.selectedUserLocation;
            	} else {
            		var successMessage = $rootScope.nls["ERR401"];
            	}
            	
             	defaultMasterFactory.save.query(vm.defaultMasterData).$promise.then(function(data) {
 		            if (data.responseCode == 'ERR0') {
 		            	$log.debug("Default Updated Successfully Response :: ", data);
 		            	Notification.success(successMessage);
 		            	var params = {};
     					params.submitAction = 'Saved';
 		            	$state.go("layout.defaultMaster",params);
 		            } else {
 		                $log.debug("default Master Failed :" + data.responseDescription)
 		            }

 		        }, function(error) {
 		        	 $log.debug(" default Master Failed : " + error)
 		        });
            } 
        };
        
       
        
        vm.ajaxSoftwareEvent = function(object){
        	 vm.searchDto = {};
     	 vm.searchDto.keyword = object == null ? "" : object;
     	 vm.searchDto.selectedPageNumber = 0;
     	 vm.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
 		return softwareMasterFactory.getall.fetch(vm.searchDto).$promise.then(
 				function(data) {
 					if (data.responseCode == "ERR0") {
 						vm.softwareList = data.responseObject.searchResult;
 						return vm.softwareList;
 					}
 				},
 				function(errResponse) {
 					$log.error('Error while fetching softwareList');
 				});
 	    }
         
         vm.ajaxObjectSubGroupEvent = function(object,obj){
        	 vm.searchDto = {};
     	 vm.searchDto.keyword = object == null ? "" : object;
     	 vm.searchDto.selectedPageNumber = 0;
     	 vm.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
     	 if(obj.objectGroupMaster == undefined && obj.objectGroupMaster == null){
     		 /*var errorMsg = $rootScope.nls["ERR06009"];
     		 Notification.error(errorMsg);*/
     		 
     		//vm.errorMap.put('objectGroupName', $rootScope.nls["ERR06009"]);
     	 }else{
     		return objectSubGroupMasterFactory.getall.fetch({"groupId" : parseInt(obj.objectGroupMaster.id)},vm.searchDto).$promise.then(
     				function(data) {
     					if (data.responseCode == "ERR0") {
     						vm.subGroupList = data.responseObject.searchResult;
     						return vm.subGroupList;
     					}
     				},
     				function(errResponse) {
     					$log.error('Error while fetching subGroupList');
     				}); 
     	 }
 	    }
         
        vm.ajaxObjectGroupEvent = function(object){
       	 vm.searchDto = {};
    	 vm.searchDto.keyword = object == null ? "" : object;
    	 vm.searchDto.selectedPageNumber = 0;
    	 vm.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
		return objectGroupMasterFactory.getall.fetch(vm.searchDto).$promise.then(
				function(data) {
					if (data.responseCode == "ERR0") {
						vm.objectGroupMasterList = data.responseObject.searchResult;
						return vm.objectGroupMasterList;
					}
				},
				function(errResponse) {
					$log.error('Error while fetching objectGroupMasterList');
				});
	    }
        
        
        /**
         * Recent History - starts here
         * 
         */
        $scope.$on('addDefaultMasterEvent', function(events, args){
        	$rootScope.unfinishedData = vm.defaultMasterData;
			$rootScope.category = "Default";
			$rootScope.unfinishedFormTitle = "Default (New)";
        	if(vm.defaultMasterData != undefined && vm.defaultMasterData != null && vm.defaultMasterData.defaultName != undefined && vm.defaultMasterData.defaultName != null) {
        		$rootScope.subTitle = vm.defaultMasterData.defaultName;
        	} else {
        		$rootScope.subTitle = "Unknown Default "
        	}
        })
      
	    $scope.$on('editDefaultMasterEvent', function(events, args){
	    	$rootScope.unfinishedData = vm.defaultMasterData;
			$rootScope.category = "Default";
			$rootScope.unfinishedFormTitle = "Default Edit (" +vm.defaultMasterData.code + ")";
			if(vm.defaultMasterData != undefined && vm.defaultMasterData != null && vm.defaultMasterData.defaultName != undefined && vm.defaultMasterData.defaultName != null) {
        		$rootScope.subTitle = vm.defaultMasterData.defaultName;
        	} else {
        		$rootScope.subTitle = "Unknown Default "
        	}
	    })
	      
	    $scope.$on('addDefaultMasterEventReload', function(e, confirmation){
	    	    confirmation.message = "";
	    	    localStorage.reloadFormData = JSON.stringify(vm.defaultMasterData) ;
	    	    localStorage.isDefaultMasterReloaded = "YES";
	            e.preventDefault();
	    })
	    
	    $scope.$on('editDefaultMasterEventReload', function(e, confirmation){
	    	    confirmation.message = "";
	    	    localStorage.reloadFormData = JSON.stringify(vm.defaultMasterData) ;
	    	    localStorage.isDefaultMasterReloaded = "YES";
	            e.preventDefault();
	    })
	      

        /**
         * State Identification Handling Here.
         * @param {String} $stateParams.action - VIEW, ADD, EDIT, SEARCH.
         */
        //
	    vm.isReloaded = localStorage.isDefaultMasterReloaded;
        switch ($stateParams.action) {
            case "VIEW":
                vm.view($stateParams.id, true);
                $rootScope.breadcrumbArr = defaultMasterDataService.getViewBreadCrumb();
                vm.selectedRecordIndex = parseInt($stateParams.selectedPageNumber);
                vm.totalRecord = parseInt($stateParams.totalRecord);
                break;
            case "ADD":
                if ($stateParams.fromHistory === 'Yes') {
                  if (vm.isReloaded == "YES") {
                   vm.isReloaded = "NO";
                   localStorage.isDefaultMasterReloaded = "NO";
                    vm.defaultMasterData = JSON.parse(localStorage.reloadFormData);
                  } else {
                    vm.defaultMasterData = $rootScope.selectedUnfilledFormData;
                  }
                } else {
                  if (vm.isReloaded == "YES") {
                    vm.isReloaded = "NO";
                    localStorage.isDefaultMasterReloaded = "NO";
                    vm.defaultMasterData = JSON.parse(localStorage.reloadFormData);
                  } else {
                      $log.debug("Add NotHistory NotReload...")
                  }
                }
                $rootScope.breadcrumbArr = defaultMasterDataService.getAddBreadCrumb();
                break;
            case "EDIT":
                if (vm.fromHistory === 'Yes') {
                  if (vm.isReloaded == "YES") {
                    vm.isReloaded = "NO";
                    localStorage.isDefaultMasterReloaded = "NO";
                    vm.defaultMasterData = JSON.parse(localStorage.reloadFormData);
                  } else {
                    vm.defaultMasterData = $rootScope.selectedUnfilledFormData;
                  }
                } else {
                  if (vm.isReloaded == "YES") {
                    vm.isReloaded = "NO";
                    localStorage.isDefaultMasterReloaded = "NO";
                    vm.defaultMasterData = JSON.parse(localStorage.reloadFormData);
                  } else {
                      vm.view($stateParams.id, false);
                  }
                }
                $rootScope.breadcrumbArr = defaultMasterDataService.getEditBreadCrumb();
                break;
            default:
                $rootScope.breadcrumbArr = defaultMasterDataService.getListBreadCrumb();
                vm.searchInit();
        }

    }
]);
}) ();