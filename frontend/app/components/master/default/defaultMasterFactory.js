app.factory('defaultMasterFactory', function($resource) {
	return {
		findById : $resource('/api/v1/defaultmaster/get/id/:id', {}, {
			 query : {method : 'GET', params : {id : ''}, isArray : false}
		}),
		save : $resource('/api/v1/defaultmaster/save', {}, {
			query : {method : 'POST'}
		}),
		delete : $resource('/api/v1/defaultmaster/delete/:id', {}, {
			query : {method : 'DELETE', params : {id : ''}}
		}),
		search : $resource('/api/v1/defaultmaster/get/search/keyword', {}, {
			query : {method : 'POST'}
		})
	};
})