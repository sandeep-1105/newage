app.service('buyerConsolidationDataService', function($rootScope) {
    this.getHeadArray = function() {
	   return [
				{
					"name":"#",
					"width":"col-xs-0half",
					"model":"no",
					"search":false
				},
				{
					"name":" Name",
					"width":"col-xs-6",
					"model":"bcName",
					"search":true,
					"wrap_cell":true,
					"type":"text",
					"sort":true,
					"key":"bcName"
				
				},
				{
					"name":"Code",
					"width":"col-xs-2half",
					"model":"bcCode",
					"search":true,
					"wrap_cell":true,
					"type":"text",
					"sort":true,
					"key":"bcCode"
				
				},
				{
					"name":"Status",
					"width":"col-xs-3",
					"model":"status",
					"search":true,
					"type":"drop",
					"data":$rootScope.enum['LovStatus'],
					"key":"status",
					"sort":true
				}
					];
          };

    
    this.getListBreadCrumb = function() {
        return [{
                label: "Master",
                state: "layout.buyerconsolidation"
            },
            {
                label: "Buyer Consolidation",
                state: "layout.buyerconsolidation"
            }
        ];
    }

    this.getAddBreadCrumb = function() {
        return [{
                label: "Master",
                state: "layout.buyerconsolidation"
            },
            {
                label: "Buyer Consolidation",
                state: "layout.buyerconsolidation"
            },
            {
                label: "Add Buyer Consolidation",
                state: null
            }
        ];
    };
    this.getEditBreadCrumb = function() {
        return [{
                label: "Master",
                state: "layout.buyerconsolidation"
            },
            {
            	label: "Buyer Consolidation",
                state: "layout.buyerconsolidation"
            },
            {
                label: "Edit Buyer Consolidation",
                state: null
            }
        ];
    };
    
    this.getViewBreadCrumb = function() {
        return [{
                label: "Master",
                state: "layout.buyerconsolidation"
            },
            {
            	label: "BuyerConsolidation",
                state: "layout.buyerconsolidation"
            },
            {
                label: "View BuyerConsolidation",
                state: null
            }
        ];
    };
   

});