(function(){
app.controller("buyerConsolidationCtrl", ['$scope', '$rootScope', '$window', '$modal', '$state', '$stateParams', '$log', 'ngDialog', 'buyerConsolidationDataService', 'buyerConsolidationFactory', 'RecentHistorySaveService', 'buyerConsolidationValidationService', 'Notification','roleConstant',
    function($scope, $rootScope, $window, $modal, $state, $stateParams, $log, ngDialog, buyerConsolidationDataService, buyerConsolidationFactory, RecentHistorySaveService, buyerConsolidationValidationService, Notification, roleConstant ) {
	
	
      $scope.tableHeadArr = buyerConsolidationDataService.getHeadArray();
      $scope.dataArr = [];
      $scope.searchData = {};
      $scope.buyerConsolidation = {};
      $scope.limitArr = [10, 15, 20];
      $scope.page = 0;
      $scope.limit = 10;
      $scope.totalRecord = 100;
      $scope.showDetail=false;
      $scope.showHistory = false;
      $scope.errorMap = new Map();
  	$scope.deskTopView = true;
      $scope.sortSelection = {
          sortKey: "bcName",
          sortOrder: "asc"
      }
      $scope.limitChange = function(nLimit) {
          $scope.page = 0;
          $scope.limit = nLimit;
          $scope.search();
      }
      
      $scope.changepage = function(param) {
          $scope.page = param.page;
          $scope.limit = param.size;
          $scope.search();
      }
      
      $scope.changeSearch = function(param) {
          $log.debug("Change Search is called." + param);
          for (var attrname in param) {
              $scope.searchData[attrname] = param[attrname];
          }
          $scope.page = 0;
          $scope.search();
      }

      $scope.sortChange = function(param) {
          $log.debug("sortChange is called. " + param);
          $scope.searchData.orderByType = param.sortOrder.toUpperCase();
          $scope.searchData.sortByColumn = param.sortKey;
          $scope.search();
      }
      
     
      $scope.searchInit = function() {
          $scope.searchData = {};
          $scope.searchData.orderByType = $scope.sortSelection.sortOrder.toUpperCase();
          $scope.searchData.sortByColumn = $scope.sortSelection.sortKey;
          $scope.search();
      };
      
      $scope.search = function() {
      	$scope.buyerConsolidationView=false;
          $scope.searchData.selectedPageNumber = $scope.page;
          $scope.searchData.recordPerPage = $scope.limit;
          $scope.dataArr = [];
          buyerConsolidationFactory.search.fetch($scope.searchData).$promise.then(
              function(data) {
                  if (data.responseCode == "ERR0") {
                      $scope.totalRecord = data.responseObject.totalRecord;
                      $scope.dataArr = data.responseObject.searchResult;
                  }
              },
              function(errResponse) {
              	 $log.error('Error while fetching ServiceTaxChargeGroupMaster');
              });
         }
      
   
   
      $scope.setStatus = function(data){
    		if(data=='Active'){
    			return 'activetype';
    		} else if(data=='Block'){
    			return 'blockedtype';
    		}else if(data=='Hide'){
    			return 'hiddentype';
    		}
    	}
      
      
     /* $scope.backToList = function() {
    	  $scope.buyerConsolidationView=false;
        }`*/
      
      
      $scope.addNew = function() {
    	  if($rootScope.roleAccess(roleConstant.MASTER_CRM_BUYER_CONSOLIDATION_CREATE)){
    		  $state.go("layout.buyerconsolidationAdd");
    	  }
      }
      
      
      
      $scope.editBC = function(id) {
    	  if($rootScope.roleAccess(roleConstant.MASTER_CRM_BUYER_CONSOLIDATION_MODIFY)){
    		  $state.go("layout.buyerconsolidationEdit", {id: id});
    	  }
      }
      
     
      
      $scope.deleteBC = function(id) {
    	  if($rootScope.roleAccess(roleConstant.MASTER_CRM_BUYER_CONSOLIDATION_DELETE)){
    		  var newScope = $scope.$new();
    			newScope.errorMessage = $rootScope.nls["ERR03215"];
    	    	ngDialog.openConfirm( {
    	                    template : '<p>{{errorMessage}}</p>'
    	                    + '<div class="ngdialog-footer">'
    	                    + '<button type="button" class="btn btn-default btn-property cancel-btn" ng-click="closeThisDialog(0)">Cancel'
    	                    + '<button type="button" class="btn btn-primary btn-property accent-btn" ng-click="confirm(1)">Yes'
    	                    + '</button></div>',
    	                    plain : true,
    	                    scope : newScope,
    	                    closeByDocument: false,
    	                    className : 'ngdialog-theme-default'
    	                }).then( function(value) {
    	                	buyerConsolidationFactory.delete.query({
    	                        id: id
    	                    }).$promise.then(function(data) {
    	                        if (data.responseCode == 'ERR0') {
    	                        	Notification.success($rootScope.nls["ERR402"]);
    	                        	$scope.deskTopView = true;
    	                        	$scope.search();
    	                        }
    	                    }, function(error) {
    	                        $log.debug("Error in Buyer Consolidation ", error);
    	                    });
    	                }, function(value) {
    	                    $log.debug("delete Buyer Consolidation Cancelled");
    	            });
    	  }    	  
        }
      
      $scope.validate = function(valElement, code) {
          var validationResponse = buyerConsolidationValidationService.validate($scope.buyerConsolidation,code);
          if(validationResponse.error == true) {
				$scope.validationErrorAndFocus(validationResponse, validationResponse.errElement);
			} else {
				$scope.errorMap.put(valElement, null);
          	validationResponse.error = false;
          }
      }

      
      $scope.update = function() {
    	  var validationResponse = buyerConsolidationValidationService.validate($scope.buyerConsolidation,0);
           if(validationResponse.error == true) {
           	$log.debug("Validation Response -- ", validationResponse);
           	$log.debug('Validation Failed while Updating the Buyer Consolidation..')
				$scope.validationErrorAndFocus(validationResponse, validationResponse.errElement);
			}
           else {
        	   buyerConsolidationFactory.update.query($scope.buyerConsolidation).$promise.then(function(data) {
		            if (data.responseCode == 'ERR0') {
		            	$log.debug("Buyer Consolidation Updated Successfully Response :: ", data);
		            	Notification.success($rootScope.nls["ERR401"]);
		            	$state.go('layout.buyerconsolidation');
		            } else {
		                $log.debug("Updating Buyer Consolidation Failed :" + data.responseDescription)
		            }
		        }, function(error) {
		        	 $log.debug("Updating Buyer Consolidation Failed : " + error)
		        });
           } 
      };
     
      
      
      $scope.create = function() {
      	var validationResponse = buyerConsolidationValidationService.validate($scope.buyerConsolidation,0);
          if(validationResponse.error == true) {
          	$log.debug("Validation Response -- ", validationResponse);
          	$log.debug('Validation Failed to create Buyer Consolidation..')
				$scope.validationErrorAndFocus(validationResponse, validationResponse.errElement);
			}
          else {
          	buyerConsolidationFactory.create.query($scope.buyerConsolidation).$promise.then(function(data) {
		            if (data.responseCode == 'ERR0') {
		            	$log.debug("Reason Saved Successfully Response :: ", data);
		            	Notification.success($rootScope.nls["ERR400"]);
		            	$state.go('layout.buyerconsolidation');
		            } else {
		                $log.debug("Create Reason Failed :" + data.responseDescription)
		            }

		        }, function(error) {
		        	 $log.debug("Create Reason Failed : " + error)
		        });
          } 
      };
      
      

      /**
       * Cancel Add or Edit ServiceTaxChargeGroupMaster.
       * @param {int} id - The id of a ServiceTaxChargeGroupMaster
       */
      $scope.cancel = function(objId) {
          if($scope.buyerConsolidationForm!=undefined){
		    	$scope.goTocancel($scope.buyerConsolidationForm.$dirty,objId);
          }else{
          	$scope.goTocancel(false,objId);
          }
          
      }
     
      $scope.cancelList = function() {
    	  
    	   $scope.showLogs = false;
		   $scope.showHistory = false;
		   $scope.buyerConsolidation = {};
    	   $scope.deskTopView = true;
    	   $scope.buyerConsolidationView=false;
    	  
      }
      
     $scope.goTocancel = function(isDirty,objId){
   		 if (isDirty) {
   		      var newScope = $scope.$new();
   		      newScope.errorMessage = $rootScope.nls["ERR200"];
   		      ngDialog.openConfirm({
   		        template: '<p>{{errorMessage}}</p>' +
   		          '<div class="ngdialog-footer">' +
   		          ' <button type="button" class="btn btn-default btn-property cancel-btn" ng-click="confirm(2)">No</button>' +
   		          '<button type="button" class="btn btn-default btn-property accent-btn " ng-click="confirm(1)">Yes</button>' +
   		          '<button type="button" class="btn btn-default btn-property pull-left grey-cancel-btn" ng-click="confirm(3)">Cancel</button>' +
   		          '</div>',
   		        plain: true,
   		        closeByDocument: false,
   		        scope: newScope,
   		        className: 'ngdialog-theme-default'
   		      }).then(
   		        function(value) {
   		          if (value == 1) {
   		            if(objId!=null && objId != undefined){
   		            	$scope.update();
   		            }else{
   		            	$scope.create();
   		            }
   		          } else if (value == 2) {
   		        	 $state.go("layout.buyerconsolidation", {
   		                submitAction: "Cancelled"
   		            });
   		          } else {
   		            $log.debug("cancelled");
   		          }
   		        });
   		    } else {
   		     $state.go("layout.buyerconsolidation", {
                 submitAction: "Cancelled"
             });
   		    }
   	    }
      
    
		$scope.validationErrorAndFocus = function(valResponse, elemId){
			$scope.errorMap = new Map();
			if(valResponse.error == true){
				$scope.errorMap.put(valResponse.errElement, valResponse.errMessage);
			}
				$scope.navigateToNextField(elemId);
		}
		
	      $scope.view = function(id) {
	    	  $scope.id=id;
	      	buyerConsolidationFactory.findById.query({
	              id: $scope.id
	          }).$promise.then(function(data) {

	                if (data.responseCode == 'ERR0') {
	                    $scope.buyerConsolidation = data.responseObject;
	                    var rHistoryObj = {}

	                    if ($scope.buyerConsolidation != undefined && $scope.buyerConsolidation != null && $scope.buyerConsolidation.id != undefined && $scope.buyerConsolidation.id != null) {
	                        $rootScope.subTitle = $scope.buyerConsolidation.bcName;
	                    } else {
	                        $rootScope.subTitle = "Unknown Buyer Consolidation"
	                    }
	                    $rootScope.unfinishedFormTitle = "Buyer Consolidation  # " + $scope.buyerConsolidation.bcCode;
	                    rHistoryObj = {
	                            'title': "Buyer Consolidation #" + $scope.buyerConsolidation.bcCode,
	                            'subTitle': $rootScope.subTitle,
	                            'stateName': $state.current.name,
	                            'stateParam': JSON.stringify($stateParams),
	                            'stateCategory': 'Buyer Consolidation',
	                            'serviceType': undefined,
	                            'showService': false
	                        }
	                    RecentHistorySaveService.form(rHistoryObj);
	                } else {
	                    $log.debug("Exception while getting Buyer Consolidation by Id ", data.responseDescription);
	                }
	            
	          }, function(error) {
	              $log.debug("Error in Buyer Consolidation ", error);
	          });
	      };
	      
	      $scope.rowSelect = function(data) {
	    	  if($rootScope.roleAccess(roleConstant.MASTER_CRM_BUYER_CONSOLIDATION_VIEW)){
	    		   $scope.buyerConsolidation = data;
		  		   $scope.buyerConsolidationView=true;
		  		   $scope.showDetail=true;
		  			var windowInner=$window.innerWidth;
		  			if(windowInner<=1199){
		  				$scope.deskTopView = false;
		  			}
		  			else{
		  				$scope.deskTopView = true;
		  			}
		  			angular.element($window).bind('resize', function(){
		  				if($window.innerWidth>=1200){
		  					$scope.deskTopView = true;
		  				}
		  				else if($window.innerWidth<=1199){
		  					if($scope.reasonMaster.id!=null){
		  						$scope.deskTopView = false;
		  					}
		  					else {
		  						$scope.deskTopView = true;
		  					}
		  				}
		  				$scope.$digest();
		  			});
	    		  
	    	  }
	  		}
	     
	 $scope.isReloaded = localStorage.isBuyerConsolidationReloaded;
	 
	 $scope.$on('addBuyerConsolidationEvent', function(events, args){
     	$rootScope.unfinishedData = $scope.buyerConsolidation;
			$rootScope.category = "Buyer Consolidation";
			$rootScope.unfinishedFormTitle = "Buyer Consolidation (New)";
     	if($scope.buyerConsolidation != undefined && $scope.buyerConsolidation != null && $scope.buyerConsolidation.bcName != undefined && $scope.buyerConsolidation.bcName != null) {
     		$rootScope.subTitle = $scope.buyerConsolidation.bcName;
     	} else {
     		$rootScope.subTitle = "Unknown Buyer Consolidation "
     	}
     })
   
	    $scope.$on('editBuyerConsolidationEvent', function(events, args){
	    	$rootScope.unfinishedData = $scope.buyerConsolidation;
			$rootScope.category = "Buyer Consolidation";
			$rootScope.unfinishedFormTitle = "Buyer Consolidation Edit # "+$scope.buyerConsolidation.bcCode;
	    })
	      
	    $scope.$on('addBuyerConsolidationEventReload', function(e, confirmation){
	    	    confirmation.message = "";
	    	    localStorage.reloadFormData = JSON.stringify($scope.buyerConsolidation) ;
	    	    localStorage.isBuyerConsolidationReloaded = "YES";
	            e.preventDefault();
	    })
	    
	    $scope.$on('editBuyerConsolidationEventReload', function(e, confirmation){
	    	    confirmation.message = "";
	    	    localStorage.reloadFormData = JSON.stringify($scope.buyerConsolidation) ;
	    	    localStorage.isBuyerConsolidationReloaded = "YES";
	            e.preventDefault();
	    	  
	    })
	 
      switch ($stateParams.action) {
          case "ADD":
          	 if ($stateParams.fromHistory === 'Yes') {
                   if ($scope.isReloaded == "YES") {
                    $scope.isReloaded = "NO";
                    localStorage.isBuyerConsolidationReloaded = "NO";
                     $scope.buyerConsolidation = JSON.parse(localStorage.reloadFormData);
                   } else {
                     $scope.buyerConsolidation = $rootScope.selectedUnfilledFormData;
                   }
                 } else {
                   if ($scope.isReloaded == "YES") {
                     $scope.isReloaded = "NO";
                     localStorage.isBuyerConsolidationReloaded = "NO";
                     $scope.buyerConsolidation = JSON.parse(localStorage.reloadFormData);
                   } else {
                       $log.debug("Add NotHistory NotReload...")
                   }
                 }
              $rootScope.breadcrumbArr = buyerConsolidationDataService.getAddBreadCrumb();
              break;
          case "EDIT":
          	 if ($scope.fromHistory === 'Yes') {
                   if ($scope.isReloaded == "YES") {
                     $scope.isReloaded = "NO";
                     localStorage.isbuyerConsolidationReloaded = "NO";
                     $scope.buyerConsolidation = JSON.parse(localStorage.reloadFormData);
                   } else {
                     $scope.buyerConsolidation = $rootScope.selectedUnfilledFormData;
                   }
                 } else {
                   if ($scope.isReloaded == "YES") {
                     $scope.isReloaded = "NO";
                     localStorage.isBuyerConsolidationReloaded = "NO";
                     $scope.buyerConsolidation = JSON.parse(localStorage.reloadFormData);
                   } else {
                  	 $scope.view($stateParams.id);
                   }
                 }
              $rootScope.breadcrumbArr = buyerConsolidationDataService.getEditBreadCrumb();
              break;
          case "VIEW":
        	  $scope.view($stateParams.id);
        	  $rootScope.breadcrumbArr = buyerConsolidationDataService.getViewBreadCrumb();
        	  break;
          default:
              $rootScope.breadcrumbArr = buyerConsolidationDataService.getListBreadCrumb();
              $scope.searchInit();
      }
      
 
}
]);
}) ();