app.factory('buyerConsolidationFactory', function($resource) {
	return {
		findById : $resource('/api/v1/bcmaster/:id', {}, {
			 query : {method : 'GET', params : {id : ''}, isArray : false}
		})
		,
		search : $resource('/api/v1/bcmaster/search', {}, {
			fetch : {method : 'POST'}
		})
		,
		update : $resource('/api/v1/bcmaster', {}, {
			query : {method : 'PUT'}
		})
		,
		delete : $resource('/api/v1/bcmaster/:id', {}, {
			query : {method : 'DELETE', params : {id : ''}}
		})
		,
		create : $resource('/api/v1/bcmaster', {}, {
			query : {method : 'POST'}
		})
	};
})