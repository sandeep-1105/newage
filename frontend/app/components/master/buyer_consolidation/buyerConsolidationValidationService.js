app.service('buyerConsolidationValidationService', ['$rootScope','ValidateUtil', 'CommonValidationService', 'appConstant', function($rootScope, ValidateUtil, CommonValidationService, appConstant) {
	this.validate = function(buyerConsolidation, code) {
		//Validate buyerConsolidation service code
		if(code == 0 || code == 1) {
			if(buyerConsolidation.bcName == undefined || buyerConsolidation.bcName == null || buyerConsolidation.bcName =="" ){
				return this.validationResponse(true, "bcname", $rootScope.nls["ERR03207"],buyerConsolidation);
			}else{
				if(!CommonValidationService.checkRegExp(appConstant.Reg_Exp_Master_Buyer_Consolidation_Name,buyerConsolidation.bcName)){
	  				return this.validationResponse(true, "bcname", $rootScope.nls["ERR03212"],buyerConsolidation);
			   }
			}
		}
		//Validate buyerConsolidation charge code
		if(code == 0 || code == 2) {
			if(buyerConsolidation.bcCode == undefined || buyerConsolidation.bcCode == null || buyerConsolidation.bcCode =="" ){
				return this.validationResponse(true, "bccode",$rootScope.nls["ERR03206"],buyerConsolidation);
			}else{
				if(!CommonValidationService.checkRegExp(appConstant.Reg_Exp_Master_Buyer_Consolidation_Code,buyerConsolidation.bcCode)){
	  				return this.validationResponse(true, "bccode", $rootScope.nls["ERR03211"],buyerConsolidation);
			   }
			}
		}
		return this.validationSuccesResponse(buyerConsolidation);
	}
		
 
	this.validationSuccesResponse = function(buyerConsolidation) {
	    return {error : false, obj : buyerConsolidation};
	}
	
	this.validationResponse = function(err, elem, message, buyerConsolidation) {
		return {error : err, errElement : elem, errMessage : message, obj : buyerConsolidation};
	}
	
	
}]);