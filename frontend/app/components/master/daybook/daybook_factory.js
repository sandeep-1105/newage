(function() {

	app.factory("DaybookSearch",['$resource', function($resource) {
		return $resource("/api/v1/daybookmaster/get/search", {}, {
			query : {
				method : 'POST'
			}
		});
	}]);
	
	app.factory("DaybookList",['$resource', function($resource) {
		return $resource("/api/v1/daybookmaster/get/search/keyword", {}, {
			fetch : {
				method : 'POST'
			}
		});
	}]);
	
	app.factory("DaybookListByDocumentType",['$resource', function($resource) {
		return $resource("/api/v1/daybookmaster/getDaybookListByDocumentType/search/keyword/:documentTypeId", {}, {
			fetch : {
				    method : 'POST',
					params : {
						documentTypeId : ''
					},
					isArray : false
			}
		});
	}]);

	app.factory("DaybookAdd",['$resource', function($resource) {
		return $resource("/api/v1/daybookmaster/create", {}, {
			save : {
				method : 'POST'
			}
		});
	}]);

	app.factory("DaybookEdit",['$resource', function($resource) {
		return $resource("/api/v1/daybookmaster/update", {}, {
			update : {
				method : 'POST'
			}
		});
	}]);

	app.factory("DaybookView",['$resource', function($resource) {
		return $resource("/api/v1/daybookmaster/get/id/:id", {}, {
			get : {
				method : 'GET',
				params : {
					id : ''
				},
				isArray : false
			}
		});
	}]);

	app.factory("DaybookRemove",['$resource', function($resource) {
		return $resource("/api/v1/daybookmaster/delete/:id", {}, {
			remove : {
				method : 'DELETE',
				params : {
					id : ''
				},
				isArray : false
			}
		});
	}]);
})();