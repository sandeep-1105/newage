(function(){
app.controller("DaybookMasterCtrl", ['$scope', '$rootScope', '$window', '$modal', '$state', '$stateParams', '$log', 'ngDialog', 
                                     'daybookMasterFactory', 'RecentHistorySaveService', 'daybookMasterValidationService',
                                     'Notification','AutoCompleteService',
                                     'cloneService','DaybookView','roleConstant',
    function($scope, $rootScope, $window, $modal, $state, $stateParams, $log, ngDialog, daybookMasterFactory, RecentHistorySaveService,
    		daybookMasterValidationService, Notification,
    		AutoCompleteService,cloneService,DaybookView,roleConstant) {

	    $scope.$AutoCompleteService = AutoCompleteService;
	    $scope.directiveDatePickerOpts = angular.copy($rootScope.datePickerOptions);
	    $scope.directiveDatePickerOpts.widgetParent = "body";
        var vm = this;
        vm.tableHeadArr =  [ {
			"name" : "#",
			"width" : "w50px",
			"model" : "no",
			"search" : false,
			"prefWidth" : "50"
		},{
			"name" : "Name",
			"width" : "150px",
			"model" : "daybookName",
			"search" : true,
			"wrap_cell" : true,
			"type" : "text",
			"sort" : true,
			"key" : "searchDaybookName",
			"prefWidth" : "150"

		},{
			"name" : "Code",
			"width" : "w80px",
			"model" : "daybookCode",
			"search" : true,
			"type" : "text",
			"sort" : true,
			"key" : "searchDaybookCode",
			"prefWidth" : "80"
		},{
			"name" : "Location",
			"width" : "w100px",
			"model" : "locationMaster.locationName",
			"search" : true,
			"type" : "text",
			"sort" : true,
			"key" : "searchLocationName",
			"prefWidth" : "100"
		},{
			"name" : "Document Type",
			"width" : "w120px",
			"model" : "documentTypeMaster.documentTypeName",
			"search" : true,
			"type" : "text",
			"sort" : true,
			"key" : "searcDocumentTypeName",
			"prefWidth": "100"
		},{
			"name" : "Cash Account code",
			"width" : "w100px",
			"model" : "cashAccount.accountCode",
			"search" : true,
			"type" : "text",
			"sort" : true,
			"key" : "searchCashAccountCode",
			"prefWidth" :"100"
		},{
			"name" : "Cash Account Name",
			"width" : "w140px",
			"model" : "cashAccount.accountName",
			"search" : true,
			"type" : "text",
			"sort" : true,
			"key" : "searchCashAccountName",
			"prefWidth": "100"
		},{
			"name" : "Bank Account code",
			"width" : "w100px",
			"model" : "bankAccount.accountCode",
			"search" : true,
			"type" : "text",
			"sort" : true,
			"key" : "searchBankAccountCode",
			"prefWidth" :"100"
		},{
			"name" : "Bank Account Name",
			"width" : "w140px",
			"model" : "bankAccount.accountName",
			"search" : true,
			"type" : "text",
			"sort" : true,
			"key" : "searchBankAccountName",
			"prefWidth": "100"
		},
		
		/*{
			"name" : "PDC GL Account",
			"width" : "w100px",
			"model" : "pdcAccountCode",
			"search" : true,
			"type" : "text",
			"sort" : true,
			"key" : "PdcAccountCode",
			"prefWidth" :"100"
		},{
			"name" : "PDC Account Name",
			"width" : "w100px",
			"model" : "pdcSubleger",
			"search" : true,
			"type" : "text",
			"sort" : true,
			"key" : "PdcSubleger",
			"prefWidth": "100"
		},*/
		
		
		{
			"name" : "Block Date",
			"width" : "w100px",
			"model" : "blockDate",
			"search" : true,
			"type" : "date-range",
			"sort" : true,
			"key" : "searchBlockDate",
			"prefWidth": "100"
		},
		{
			"name" : "Status",
			"width" : "w120px",
			"model" : "status",
			"data":$rootScope.enum['LovStatus'],
			"search" : true,
			"type" : "drop",
			"sort" : true,
			"key" : "searchStatus",
			"prefWidth":"100"
		}
		];
        vm.dataArr = [];
        vm.searchDto = {};
        vm.daybookMaster = {};
        vm.daybookMaster.status='Active';
        vm.limitArr = [10, 15, 20];
        vm.page = 0;
        vm.limit = 10;
        vm.totalRecord = 100;
        vm.showHistory = false;
        vm.errorMap = new Map();
        vm.searchDto = {};
        vm.sortSelection = {
            sortKey: "daybookName",
            sortOrder: "asc"
        }
        
        /**
         * singlePageNavigation method starts here
         */
        vm.singlePageNavigation = function(val) {
        	if($stateParams != undefined && $stateParams != null) {
                if ((parseInt($stateParams.index) + val) < 0 || (parseInt($stateParams.index) + val) >= parseInt($stateParams.totalRecord))
                    return;
                var stateParameters = {};
                vm.searchDto = {};
                vm.searchDto.recordPerPage = 1;
                vm.hidePage = true;
                vm.searchDto.selectedPageNumber = parseInt($stateParams.selectedPageNumber) + val;
    			
    			if($stateParams.searchStatus != undefined && $stateParams.searchStatus != null) {
    				stateParameters.searchStatus = vm.searchDto.searchStatus = $stateParams.searchStatus; 
            	}
    			if($stateParams.sortByColumn != undefined && $stateParams.sortByColumn != null) {
    				stateParameters.sortByColumn = vm.searchDto.sortByColumn = $stateParams.sortByColumn; 
            	}
    			if($stateParams.orderByType != undefined && $stateParams.orderByType != null) {
    				stateParameters.orderByType = vm.searchDto.orderByType = $stateParams.orderByType; 
            	}
    			if($stateParams.searchDaybookCode != undefined && $stateParams.searchDaybookCode != null) {
    				stateParameters.searchDaybookCode = vm.searchDto.searchDaybookCode = $stateParams.searchDaybookCode; 
            	}
    			if($stateParams.searchDaybookName != undefined && $stateParams.searchDaybookName != null) {
    				stateParameters.searchDaybookName = vm.searchDto.searchDaybookName = $stateParams.searchDaybookName; 
            	}
    			if($stateParams.searchLocationName != undefined && $stateParams.searchLocationName != null) {
    				stateParameters.searchLocationName = vm.searchDto.searchLocationName = $stateParams.searchLocationName; 
            	}
    			if($stateParams.searcDocumentTypeName != undefined && $stateParams.searcDocumentTypeName != null) {
    				stateParameters.searcDocumentTypeName = vm.searchDto.searcDocumentTypeName = $stateParams.searcDocumentTypeName; 
            	}
    			if($stateParams.searchCashAccountCode != undefined && $stateParams.searchCashAccountCode != null) {
    				stateParameters.searchCashAccountCode = vm.searchDto.searchCashAccountCode = $stateParams.searchCashAccountCode; 
            	}
    			if($stateParams.searchCashAccountName != undefined && $stateParams.searchCashAccountName != null) {
    				stateParameters.searchCashAccountName = vm.searchDto.searchCashAccountName = $stateParams.searchCashAccountName; 
            	}
    			if($stateParams.searchBankAccountCode != undefined && $stateParams.searchBankAccountCode != null) {
    				stateParameters.searchBankAccountCode = vm.searchDto.searchBankAccountCode = $stateParams.searchBankAccountCode; 
            	}
    			if($stateParams.searchBankAccountName != undefined && $stateParams.searchBankAccountName != null) {
    				stateParameters.searchBankAccountName = vm.searchDto.searchBankAccountName = $stateParams.searchBankAccountName; 
            	}
    			if($stateParams.searchBlockDate != undefined && $stateParams.searchBlockDate != null) {
    				stateParameters.searchBlockDate = vm.searchDto.searchBlockDate = $stateParams.searchBlockDate; 
            	}
    			if($stateParams.searchStatus != undefined && $stateParams.searchStatus != null) {
    				stateParameters.searchStatus = vm.searchDto.searchStatus = $stateParams.searchStatus; 
            	}
    			
    			
        	} else {
        		return;
        	}
        	daybookMasterFactory.search.fetch(vm.searchDto).$promise.then(
                    function(data) {
                    	 vm.totalRecord = data.responseObject.totalRecord;
                    	 stateParameters.selectedPageNumber = vm.searchDto.selectedPageNumber;
                         stateParameters.id = data.responseObject.searchResult[0].id;
                         $scope.hidePage = false;
                         $state.go("layout.daybookMasterView", stateParameters);
                    });
        }
        
        vm.selectedLocation = function(object) {
            vm.validate('locationMaster', 3)
            $rootScope.navigateToNextField(object);
            
        };
        
        vm.selectedDocumentType = function(object) {
           // vm.validate('documentTypeMaster', 4)
            $rootScope.navigateToNextField(object);
            
        };
       
        vm.selectedCashAccount = function(object) {
        	vm.validate('cashAccount', 5);
            $rootScope.navigateToNextField(object);
        };
        vm.selectedBankAccount = function(object) {
        	vm.validate('bankAccount', 6);
            $rootScope.navigateToNextField(object);
        };
        vm.selectedPdcAccount = function(object) {
        	vm.validate('pdcAccount', 7);
            $rootScope.navigateToNextField(object);
        };
        vm.selectedExcangeGainAccount = function(object) {
        	vm.validate('exchangeGainAccount', 8);
            $rootScope.navigateToNextField(object);
        };
        
        vm.selectedExcangeLossAccount = function(object) {
        	vm.validate('exchangeLossAccount', 9);
            $rootScope.navigateToNextField(object);
        };
        
        vm.selectedNumericDaybook = function() {
        	vm.validate('numericDaybook', 13);
        };
        
      
        vm.changeSearch = function(param) {
            $log.debug("Change Search is called." + param);
            for (var attrname in param) {
                vm.searchDto[attrname] = param[attrname];
            }
            vm.page = 0;
            vm.search();
        }

        vm.sortChange = function(param) {
            $log.debug("sortChange is called. " + param);
            vm.searchDto.orderByType = param.sortOrder.toUpperCase();
            vm.searchDto.sortByColumn = param.sortKey;
            vm.search();
        }
        
        vm.setStatus = function(data){
    		if(data=='Active'){
    			return 'activetype';
    		} else if(data=='Block'){
    			return 'blockedtype';
    		}else if(data=='Hide'){
    			return 'hiddentype';
    		}
    	}
        
        /**
         * Daybook Master List Navigate to selected page.
         * @param {Object} param - The Selected page option.
         */
        vm.changepage = function(param) {
            $log.debug("change Page..." + param.page);
            vm.page = param.page;
            vm.limit = param.size;
            vm.search();
        }

        /**
         * Daybook Master List Page Limit Data change.
         * @param {int} nLimit - The Selected limit option from Limit array.
         */
        vm.limitChange = function(nLimit) {
            $log.debug("Limit Change..." + nLimit);
            vm.page = 0;
            vm.limit = nLimit;
            vm.search();
        }

        /**
         * Daybook Master List Page Search Data  initializing and calling search Mtd.
         * @param {Object} data - The Selected Daybook Master from List page.
         * @param {string} data.id - The id of the Selected Daybook Master.
         * @param {int} index - The Selected row index from List page.
         */
        vm.rowSelect = function(data,index) {
        	if($rootScope.roleAccess(roleConstant.MASTER_FINANCE_DAY_BOOK_VIEW)){
                console.log("RowSelect ", data, index);
    			
    			vm.selectedRecordIndex = index;
    		    
    	
    			/*************************************************************
    			 * ux - by Muthu
    			 * reason - ipad compatibility fix
    			 *
    			 * **********************************************************/
    	
    			var windowInner=$window.innerWidth;
    			if(windowInner<=1199){
    				$scope.deskTopView = false;
    			}
    			else{
    				$scope.deskTopView = true;
    			}
    			
                vm.selectedRowIndex =  (vm.searchDto.recordPerPage * vm.searchDto.selectedPageNumber) + index;
    			
    			var param = {
    						id                      : data.id,
    			    		selectedPageNumber 		: vm.selectedRowIndex,
    			    		totalRecord 			: vm.totalRecord
    			    };
    			    
    			$state.go("layout.daybookMasterView", vm.searchDtoToStateParams(param));
    		
        	}
        }
        
        vm.searchDtoToStateParams = function(param) {
   		 if(param == undefined || param == null) {
   		    		param = {};
   		    	}
   		 if(vm.searchDto != undefined && vm.searchDto != null) {
       		if(vm.searchDto != undefined && vm.searchDto != null) {
           		param.recordPerPage = 1;
       			if(vm.searchDto.sortByColumn != undefined && vm.searchDto.sortByColumn != null) {
               		param.sortByColumn = vm.searchDto.sortByColumn; 
               	}
       			if(vm.searchDto.orderByType != undefined && vm.searchDto.orderByType != null) {
               		param.orderByType = vm.searchDto.orderByType; 
               	}
       			if(vm.searchDto.searchDaybookCode != undefined && vm.searchDto.searchDaybookCode != null) {
               		param.searchDaybookCode = vm.searchDto.searchDaybookCode; 
               	}
       			
       			if(vm.searchDto.searchDaybookName != undefined && vm.searchDto.searchDaybookName != null) {
               		param.searchDaybookName = vm.searchDto.searchDaybookName; 
               	}
       			
       			if(vm.searchDto.searchLocationName != undefined && vm.searchDto.searchLocationName != null) {
               		param.searchLocationName = vm.searchDto.searchLocationName; 
               	}
       			
       			if(vm.searchDto.searcDocumentTypeName != undefined && vm.searchDto.searcDocumentTypeName != null) {
               		param.searcDocumentTypeName = vm.searchDto.searcDocumentTypeName; 
               	}
       			
       			if(vm.searchDto.searchCashAccountCode != undefined && vm.searchDto.searchCashAccountCode != null) {
               		param.searchCashAccountCode = vm.searchDto.searchCashAccountCode; 
               	}
       			
       			if(vm.searchDto.searchCashAccountName != undefined && vm.searchDto.searchCashAccountName != null) {
               		param.searchCashAccountName = vm.searchDto.searchCashAccountName; 
               	}
       			if(vm.searchDto.searchBankAccountCode != undefined && vm.searchDto.searchBankAccountCode != null) {
               		param.searchBankAccountCode = vm.searchDto.searchBankAccountCode; 
               	}
       			
       			if(vm.searchDto.searchBankAccountName != undefined && vm.searchDto.searchBankAccountName != null) {
               		param.searchBankAccountName = vm.searchDto.searchBankAccountName; 
               	}
       			if(vm.searchDto.searchBlockDate != undefined && vm.searchDto.searchBlockDate != null) {
               		param.searchBlockDate = vm.searchDto.searchBlockDate; 
               	}
       			if(vm.searchDto.searchStatus != undefined && vm.searchDto.searchStatus != null) {
               		param.searchStatus = vm.searchDto.searchStatus; 
               	}
       		}
       		
   		}
   		return param;
   		}

        /**
         * Daybook Master List Page Search Data  initializing and calling search Mtd.
         */
        vm.searchInit = function() {
            vm.searchDto = {};
            vm.searchDto.orderByType = vm.sortSelection.sortOrder.toUpperCase();
            vm.searchDto.sortByColumn = vm.sortSelection.sortKey;
            vm.search();
        };
        /**
         * Go Back to Daybook Master List Page.
         */
        vm.backToList = function() {
        	$state.go('layout.daybookMaster');
        	vm.daybookMasteView = false;
        }
        
       
        /**
         * Get Daybook Master's Based on Search Data.
         */
        vm.search = function() {
            vm.searchDto.selectedPageNumber = vm.page;
            vm.searchDto.recordPerPage = vm.limit;
            if(vm.searchDto.blockDate!=null){
            	vm.searchDto.blockDate.startDate = $rootScope.sendApiStartDateTime(vm.searchDto.blockDate.startDate);
            	vm.searchDto.blockDate.endDate = $rootScope.sendApiEndDateTime(vm.searchDto.blockDate.endDate);
			}
            vm.dataArr = [];
            daybookMasterFactory.search.fetch(vm.searchDto).$promise.then(
                function(data) {
                    if (data.responseCode == "ERR0") {
                        vm.totalRecord = data.responseObject.totalRecord;
                        vm.dataArr = data.responseObject.searchResult;
                    }
                },
                function(errResponse) {
                	 $log.error('Error while fetching Daybook');
                });
           }

        /**
         * Get Daybook Master By id.
         * @param {int} id - The id of a Daybook.
         * @param {boolean} isView - TO identify purpose of calling is for View or Edit a Daybook.
         */
        vm.view = function(id, isView) {
        	daybookMasterFactory.findById.query({
                id: id
            }).$promise.then(function(data) {
                if (data.responseCode == 'ERR0') {
                    vm.daybookMaster = data.responseObject;
                    var rHistoryObj = {}

                    if (vm.daybookMaster != undefined && vm.daybookMaster != null && vm.daybookMaster.id != undefined && vm.daybookMaster.id != null) {
                        $rootScope.subTitle = vm.daybookMaster.daybookName;
                        vm.daybookMaster.blockDate=$rootScope.dateToString(vm.daybookMaster.blockDate);
                        if(vm.daybookMaster.numericDaybookId!=null){
                        DaybookView.get({
                   			id : vm.daybookMaster.numericDaybookId
                   		}, function(data) {
                   			if (data.responseCode =="ERR0"){
                   				vm.daybookMaster.numericDaybook = data.responseObject;
                   			}
                   		}, function(error) {
                   			console.log("Error while getting daybook.", error)
                   		});
                        }
                    } else {
                        $rootScope.subTitle = "Unknown Daybook"
                    }
                    $rootScope.unfinishedFormTitle = "Daybook  # " + vm.daybookMaster.daybookCode;
                    if (isView) {
                    	vm.selectedRowIndex=parseInt($stateParams.selectedPageNumber);
                    	vm.totalRecord=parseInt($stateParams.totalRecord);
                        rHistoryObj = {
                            'title': "Daybook #" + vm.daybookMaster.daybookCode,
                            'subTitle': $rootScope.subTitle,
                            'stateName': $state.current.name,
                            'stateParam': JSON.stringify($stateParams),
                            'stateCategory': 'Daybook Master',
                            'serviceType': undefined,
                            'showService': false
                        }

                    } else {

                        rHistoryObj = {
                            'title': 'Edit Daybook # ' + $stateParams.id,
                            'subTitle': $rootScope.subTitle,
                            'stateName': $state.current.name,
                            'stateParam': JSON.stringify($stateParams),
                            'stateCategory': 'Daybook Master',
                            'serviceType': 'MASTER',
                            'serviceCode': ' ',
                            'orginAndDestination': ' '
                        }
                    }
                    RecentHistorySaveService.form(rHistoryObj);
                } else {
                    $log.debug("Exception while getting Daybook by Id ", data.responseDescription);
                }
            }, function(error) {
                $log.debug("Error ", error);
            });
        };

        /**
         * Go To Add Daybook Master.
         */
        vm.addNew = function() {
        	if($rootScope.roleAccess(roleConstant.MASTER_FINANCE_DAY_BOOK_CREATE)){
        		 $state.go("layout.daybookMasterAdd");	
        	}
        }

        /**
         * Validating field of Daybook Master while instant change.
         * @param {errorElement,errorCode}
         */
        vm.validate = function(valElement, code) {
            var validationResponse = daybookMasterValidationService.validate(vm.daybookMaster,code);
            if(validationResponse.error == true) {
				$scope.validationErrorAndFocus(validationResponse, validationResponse.errElement);
			} else {
				vm.errorMap.put(valElement, null);
            	validationResponse.error = false;
            }
        }
        
        /**
         * Cancel Add or Edit Daybook Master.
         * @param {int} id - The id of a Daybook
         */
       
        vm.cancel = function(objId) {
            if(vm.daybookMasterForm!=undefined){
		    	vm.goTocancel(vm.daybookMasterForm.$dirty,objId);
            }else{
            	vm.goTocancel(false,objId);
            }
        }
       
       vm.goTocancel = function(isDirty,objId){
     		 if (isDirty) {
     		      var newScope = $scope.$new();
     		      newScope.errorMessage = $rootScope.nls["ERR200"];
     		      ngDialog.openConfirm({
     		        template: '<p>{{errorMessage}}</p>' +
     		          '<div class="ngdialog-footer">' +
     		          ' <button type="button" class="btn btn-default btn-property cancel-btn" ng-click="confirm(2)">No</button>' +
     		          '<button type="button" class="btn btn-default btn-property accent-btn " ng-click="confirm(1)">Yes</button>' +
     		          '<button type="button" class="btn btn-default btn-property pull-left grey-cancel-btn" ng-click="confirm(3)">Cancel</button>' +
     		          '</div>',
     		        plain: true,
     		        closeByDocument: false,
     		        scope: newScope,
     		        className: 'ngdialog-theme-default'
     		      }).then(
     		        function(value) {
     		          if (value == 1) {
     		            if(objId!=null && objId != undefined){
     		            	vm.update();
     		            }else{
     		            	vm.create();
     		            }
     		          } else if (value == 2) {
     		        	 $state.go("layout.daybookMaster", {
     		                submitAction: "Cancelled"
     		            });
     		          } else {
     		            $log.debug("cancelled");
     		          }
     		        });
     		    } else {
     		     $state.go("layout.daybookMaster", {
                   submitAction: "Cancelled"
               });
     		    }
     	    }

        /**
         * Modify Existing Daybook Master.
         * @param {int} id - The id of a Country.
         */
        vm.edit = function(id) {
        	if($rootScope.roleAccess(roleConstant.MASTER_FINANCE_DAY_BOOK_MODIFY)){
                $state.go("layout.daybookMasterEdit", {
                    id: id
                });
        	}
        }

        /**
         * Delete Country Master.
         * @param {int} id - The id of a Country.
         */
        vm.delete = function(id) {
		    if($rootScope.roleAccess(roleConstant.MASTER_FINANCE_DAY_BOOK_DELETE)){
			var newScope = $scope.$new();
			newScope.errorMessage = $rootScope.nls["ERR200023"];
			ngDialog.openConfirm( {
		                template : '<p>{{errorMessage}}</p>'
		                + '<div class="ngdialog-footer">'
		                + '<button type="button" class="btn btn-default btn-property cancel-btn" ng-click="closeThisDialog(0)">Cancel'
		                + '<button type="button" class="btn btn-primary btn-property accent-btn" ng-click="confirm(1)">Yes'
		                + '</button></div>',
		                plain : true,
		                scope : newScope,
		                closeByDocument: false,
		                className : 'ngdialog-theme-default'
		            }).then( function(value) {
		            	daybookMasterFactory.delete.query({
		                    id: id
		                }).$promise.then(function(data) {
		                    if (data.responseCode == 'ERR0') {
		                    	vm.daybookMasteView=false;
		                    	Notification.success($rootScope.nls["ERR402"]);
		                    	vm.backToList();
		                    }
		                }, function(error) {
		                    $log.debug("Error ", error);
		                });
		            }, function(value) {
		                $log.debug("delete Cancelled");
		        });	
}
        	
        }

        /**
         * Create New Daybook Master.
         */
        vm.create = function() {
            var validationResponse = daybookMasterValidationService.validate(vm.daybookMaster,0);
            if(validationResponse.error == true) {
            	$log.debug("Validation Response -- ", validationResponse);
            	$log.debug('Validation Failed to create Daybook..')
				$scope.validationErrorAndFocus(validationResponse, validationResponse.errElement);
			}
            else {
            	var isPayentOrReceipt = ((vm.daybookMaster.documentTypeMaster.documentTypeCode).toUpperCase() =='PMT' || (vm.daybookMaster.documentTypeMaster.documentTypeCode).toUpperCase() =='RPT');
            	if(isPayentOrReceipt){
            		if((vm.daybookMaster.bankAccount==undefined || vm.daybookMaster.bankAccount==null || vm.daybookMaster.bankAccount.id==null) 
        				&& (vm.daybookMaster.pdcAccount==undefined || vm.daybookMaster.pdcAccount==null || vm.daybookMaster.pdcAccount.id==null) 
        				&& (vm.daybookMaster.cashAccount==undefined || vm.daybookMaster.cashAccount==null || vm.daybookMaster.cashAccount.id==null)){
	            		Notification.error($rootScope.nls["ERR200039"]);
	            		return;
            		}
            	}
            	vm.tmpObj = cloneService.clone(vm.daybookMaster);
            	vm.tmpObj.blockDate = $rootScope.sendApiDateAndTime(vm.tmpObj.blockDate);
            	if(vm.tmpObj.numericDaybook!=null && vm.tmpObj.numericDaybook!=undefined){
            	vm.tmpObj.numericDaybookCode=vm.tmpObj.numericDaybook.daybookCode;
            	vm.tmpObj.numericDaybookId=vm.tmpObj.numericDaybook.id;
            	}
            	daybookMasterFactory.create.query(vm.tmpObj).$promise.then(function(data) {
		            if (data.responseCode == 'ERR0') {
		            	$log.debug("Daybook Saved Successfully Response :: ", data);
		            	Notification.success($rootScope.nls["ERR400"]);
		            	$state.go('layout.daybookMaster');
		            } else {
		                $log.debug("Create Daybook Failed :" + data.responseDescription)
		            }

		        }, function(error) {
		        	 $log.debug("Create Daybook Failed : " + error)
		        });
            } 
        };
      /**
       * Common Validation Focus Functionality - starts here" 
       */
		$scope.validationErrorAndFocus = function(valResponse, elemId){
			vm.errorMap = new Map();
			if(valResponse.error == true){
				vm.errorMap.put(valResponse.errElement, valResponse.errMessage);
			}
				$scope.navigateToNextField(elemId);
		}
        
		 /**
	       * Update Existing Country Master - starts here" 
	       */
        vm.update = function() {
        	 var validationResponse = daybookMasterValidationService.validate(vm.daybookMaster,0);
             if(validationResponse.error == true) {
             	$log.debug("Validation Response -- ", validationResponse);
             	$log.debug('Validation Failed while Updating the Daybook..')
 				$scope.validationErrorAndFocus(validationResponse, validationResponse.errElement);
 			}
             else {
            	var isPayentOrReceipt = ((vm.daybookMaster.documentTypeMaster.documentTypeCode).toUpperCase() =='PMT' || (vm.daybookMaster.documentTypeMaster.documentTypeCode).toUpperCase() =='RPT');
             	if(isPayentOrReceipt){
             		if((vm.daybookMaster.bankAccount==undefined || vm.daybookMaster.bankAccount==null || vm.daybookMaster.bankAccount.id==null) 
         				&& (vm.daybookMaster.pdcAccount==undefined || vm.daybookMaster.pdcAccount==null || vm.daybookMaster.pdcAccount.id==null) 
         				&& (vm.daybookMaster.cashAccount==undefined || vm.daybookMaster.cashAccount==null || vm.daybookMaster.cashAccount.id==null)){
 	            		Notification.error($rootScope.nls["ERR200039"]);
 	            		return;
             		}
             	}
            	vm.tmpObj = cloneService.clone(vm.daybookMaster);
             	vm.tmpObj.blockDate = $rootScope.sendApiDateAndTime(vm.tmpObj.blockDate);
             	if(vm.tmpObj.numericDaybook!=null && vm.tmpObj.numericDaybook!=undefined){
             	vm.tmpObj.numericDaybookCode=vm.tmpObj.numericDaybook.daybookCode;
            	vm.tmpObj.numericDaybookId=vm.tmpObj.numericDaybook.id;
             	}
            		if(vm.daybookMaster.id == null) {
        				var successMessage = $rootScope.nls["ERR400"];	
        			} else {
        				var successMessage = $rootScope.nls["ERR401"];	
        			}
             	daybookMasterFactory.update.query(vm.tmpObj).$promise.then(function(data) {
 		            if (data.responseCode == 'ERR0') {
 		            	$log.debug("Daybook Updated Successfully Response :: ", data);
 		            	Notification.success(successMessage);
 		            	$state.go('layout.daybookMaster');
 		            } else {
 		                $log.debug("Updating Daybook Failed :" + data.responseDescription)
 		            }
 		        }, function(error) {
 		        	 $log.debug("Updating Daybook Failed : " + error)
 		        });
             } 
        };
        
       
        /**
         * Recent History - starts here
         */
        vm.isReloaded = localStorage.isDaybookMasterReloaded;
        $scope.$on('addDaybookMasterEvent', function(events, args){
        	$rootScope.unfinishedData = vm.daybookMaster;
			$rootScope.category = "Daybook Master";
			$rootScope.unfinishedFormTitle = "Daybook (New)";
        	if(vm.daybookMaster != undefined && vm.daybookMaster != null && vm.daybookMaster.daybookName != undefined && vm.daybookMaster.daybookName != null) {
        		$rootScope.subTitle = vm.daybookMaster.daybookName;
        	} else {
        		$rootScope.subTitle = "Unknown Daybook "
        	}
        })
      
	    $scope.$on('editDaybookMasterEvent', function(events, args){
	    	$rootScope.unfinishedData = vm.daybookMaster;
			$rootScope.category = "Daybook Master";
			$rootScope.unfinishedFormTitle = "Daybook Edit # "+vm.daybookMaster.daybookCode;
	    })
	      
	    $scope.$on('addDaybookMasterEventReload', function(e, confirmation){
	    	    confirmation.message = "";
	    	    localStorage.reloadFormData = JSON.stringify(vm.daybookMaster) ;
	    	    localStorage.isDaybookMasterReloaded = "YES";
	            e.preventDefault();
	    })
	    
	    $scope.$on('editDaybookMasterEventReload', function(e, confirmation){
	    	    confirmation.message = "";
	    	    localStorage.reloadFormData = JSON.stringify(vm.daybookMaster) ;
	    	    localStorage.isDaybookMasterReloaded = "YES";
	            e.preventDefault();
	    	  
	    })
	      


        /**
         * State Identification Handling Here.
         * @param {String} $stateParams.action - VIEW, ADD, EDIT, SEARCH.
         */
        //
        switch ($stateParams.action) {
        case "VIEW":
            console.log("I am In Daybook View Page");
            $rootScope.unfinishedFormTitle = "Daybook View # " + $stateParams.id;
            $rootScope.unfinishedData = undefined;
            vm.selectedTabIndex=0;
            if($stateParams.selectedPageNumber != undefined && $stateParams.selectedPageNumber != null) {
				vm.selectedRowIndex = parseInt($stateParams.selectedPageNumber);
			}
			
    	    if($stateParams.totalRecord != undefined && $stateParams.totalRecord != null) {
				vm.totalRecord = parseInt($stateParams.totalRecord);
			}
            vm.view($stateParams.id,true);
            $rootScope.breadcrumbArr = [{label:"Master", state:"layout.daybookMaster"},
                                        {label:"Daybook", state:"layout.daybookMaster"}, {label:"View Daybook", state:null}];
            break;
            case "ADD":
            	 if ($stateParams.fromHistory === 'Yes') {
                     if (vm.isReloaded == "YES") {
                      vm.isReloaded = "NO";
                      localStorage.isDaybookMasterReloaded = "NO";
                       vm.daybookMaster = JSON.parse(localStorage.reloadFormData);
                     } else {
                       vm.daybookMaster = $rootScope.selectedUnfilledFormData;
                     }
                   } else {
                     if (vm.isReloaded == "YES") {
                       vm.isReloaded = "NO";
                       localStorage.isDaybookMasterReloaded = "NO";
                       vm.daybookMaster = JSON.parse(localStorage.reloadFormData);
                     } else {
                         $log.debug("Add NotHistory NotReload...")
                     }
                   }
                $rootScope.breadcrumbArr = [{label: "Master",state: "layout.daybookMaster"},{label: "Daybook",state: "layout.daybookMaster"},{label: "Add Daybook",state: null}];
                break;
            case "EDIT":
            	 if ($stateParams.fromHistory === 'Yes') {
                     if (vm.isReloaded == "YES") {
                       vm.isReloaded = "NO";
                       localStorage.isDaybookMasterReloaded = "NO";
                       vm.daybookMaster = JSON.parse(localStorage.reloadFormData);
                     } else {
                       vm.daybookMaster = $rootScope.selectedUnfilledFormData;
                     }
                   } else {
                     if (vm.isReloaded == "YES") {
                       vm.isReloaded = "NO";
                       localStorage.isDaybookMasterReloaded = "NO";
                       vm.daybookMaster = JSON.parse(localStorage.reloadFormData);
                     } else {
                    	 vm.view($stateParams.id, false);
                     }
                   }
                $rootScope.breadcrumbArr = [{label: "Master",state: "layout.daybookMaster"},{label: "Daybook",state: "layout.daybookMaster"},{label: "Edit Daybook",state: null}];
                break;
            default:
                $rootScope.breadcrumbArr = [{label: "Master",state: "layout.daybookMaster"},{label: "Daybook",state: "layout.daybookMaster"}];
                vm.searchInit();
        }

    }
]);
}) ();