app.factory('daybookMasterFactory', function($resource) {
	return {
		findById : $resource('/api/v1/daybookmaster/get/id/:id', {}, {
			 query : {method : 'GET', params : {id : ''}, isArray : false}
		}),
		create : $resource('/api/v1/daybookmaster/create', {}, {
			query : {method : 'POST'}
		}),
		update : $resource('/api/v1/daybookmaster/update', {}, {
			query : {method : 'POST'}
		}),
		delete : $resource('/api/v1/daybookmaster/delete/:id', {}, {
			query : {method : 'DELETE', params : {id : ''}}
		}),
		search : $resource('/api/v1/daybookmaster/get/search', {}, {
			fetch : {method : 'POST'}
		})
	};
})