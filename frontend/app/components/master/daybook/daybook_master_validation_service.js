app.service('daybookMasterValidationService', ['$rootScope','ValidateUtil', 'CommonValidationService', 'appConstant','Notification', function($rootScope, ValidateUtil, CommonValidationService, appConstant,Notification) {
	this.validate = function(daybook, code) {
		
		//Validate daybook name
		if(code == 0 || code == 1) {
			if(daybook.daybookName == undefined || daybook.daybookName == null || daybook.daybookName =="" ){
				return this.validationResponse(true, "daybookName", $rootScope.nls["ERR200004"], daybook);
			}
			else {
				if(!CommonValidationService.checkRegExp(appConstant.Reg_Exp_Master_Daybook_Name,daybook.daybookName)){
                    return this.validationResponse(true, "daybookName", $rootScope.nls["ERR200005"], daybook);
    			}
			}
		}
		//Validate daybook code
		if(code == 0 || code == 2) {
			if(daybook.daybookCode == undefined || daybook.daybookCode == null || daybook.daybookCode =="" ){
				return this.validationResponse(true, "daybookCode", $rootScope.nls["ERR200002"], daybook);
			}
			else {
				if(!CommonValidationService.checkRegExp(appConstant.Reg_Exp_Master_Daybook_Code,daybook.daybookCode)){
                    return this.validationResponse(true, "daybookCode", $rootScope.nls["ERR200003"], daybook);
    			}
			}
		}
		
		//Validate daybook master
		if(code == 0 || code == 3) {
			if(daybook.locationMaster == null || daybook.locationMaster == undefined || daybook.locationMaster== "" ){
				return this.validationResponse(true, "locationMaster", $rootScope.nls["ERR200027"], daybook);	
			}
			else{
				if(daybook.locationMaster.status=="Block"){
					return this.validationResponse(true, "locationMaster", $rootScope.nls["ERR200014"], daybook);	
				}
				else if(daybook.locationMaster.status=="Hide"){
					return this.validationResponse(true, "locationMaster", $rootScope.nls["ERR200015"], daybook);	
				}
			}
		}
		if(daybook.documentTypeMaster!=undefined && daybook.documentTypeMaster!=null){
			var isPayentOrReceipt = ((daybook.documentTypeMaster.documentTypeCode).toUpperCase() =='PMT' || (daybook.documentTypeMaster.documentTypeCode).toUpperCase() =='RPT');
		}
		//Validate document type master
		if(code == 0 || code == 4) {
			if(daybook.documentTypeMaster == null || daybook.documentTypeMaster == undefined || daybook.documentTypeMaster== "" ){
				return this.validationResponse(true, "documentTypeMaster", $rootScope.nls["ERR200022"], daybook);	
			}
			else{
				if(daybook.documentTypeMaster.status=="Block"){
					return this.validationResponse(true, "documentTypeMaster", $rootScope.nls["ERR200020"], daybook);	
				}
				else if(daybook.documentTypeMaster.status=="Hide"){
					return this.validationResponse(true, "documentTypeMaster", $rootScope.nls["ERR200021"], daybook);	
				}
			}
		}
		
		//Validate cash account master
		if(code == 0 || code == 5) {
			if(daybook.cashAccount!=undefined && daybook.cashAccount!=null) {
				if(daybook.cashAccount.status=="Block"){
					return this.validationResponse(true, "cashAccount", $rootScope.nls["ERR200016"], daybook);	
				}
				else if(daybook.cashAccount.status=="Hide"){
					return this.validationResponse(true, "cashAccount", $rootScope.nls["ERR200017"], daybook);	
				}
			}
		}

		
		//Validate bank account master
		if(code == 0 || code == 6) {
				if(daybook.bankAccount!=null && daybook.bankAccount!=undefined){
					if(daybook.bankAccount.status=="Block"){
						return this.validationResponse(true, "bankAccount", $rootScope.nls["ERR200030"], daybook);	
					}
					else if(daybook.bankAccount.status=="Hide"){
						return this.validationResponse(true, "bankAccount", $rootScope.nls["ERR200031"], daybook);	
					}
				}
		}
		
		//Validate Pdc account master
		if(code == 0 || code == 7) {
				if(daybook.pdcAccount !=null && daybook.pdcAccount !=undefined){
					if(daybook.pdcAccount.status=="Block"){
						return this.validationResponse(true, "pdcAccount", $rootScope.nls["ERR200033"], daybook);	
					}
					else if(daybook.pdcAccount.status=="Hide"){
						return this.validationResponse(true, "pdcAccount", $rootScope.nls["ERR200034"], daybook);	
					}
				}
		}
		
		//Validate exchange Gain account master
		if(code == 0 || code == 8) {
			if(daybook.exchangeGainAccount != null && (daybook.exchangeGainAccount != undefined && daybook.exchangeGainAccount!= "" )){
				if(daybook.exchangeGainAccount.status=="Block"){
					return this.validationResponse(true, "exchangeGainAccount", $rootScope.nls["ERR200035"], daybook);	
				}
				else if(daybook.exchangeGainAccount.status=="Hide"){
					return this.validationResponse(true, "exchangeGainAccount", $rootScope.nls["ERR200036"], daybook);	
				}
			}
		}
		//Validate exchange loss account master
		if(code == 0 || code == 9) {
			if(daybook.exchangeLossAccount != null && daybook.exchangeLossAccount != undefined && daybook.exchangeLossAccount!= "" ){
				if(daybook.exchangeLossAccount.status=="Block"){
					return this.validationResponse(true, "exchangeLossAccount", $rootScope.nls["ERR200037"], daybook);	
				}
				else if(daybook.exchangeLossAccount.status=="Hide"){
					return this.validationResponse(true, "exchangeLossAccount", $rootScope.nls["ERR200038"], daybook);	
				}
			}
		}
		
		//Validate chequeNoFrom
		if(code == 0 || code == 10) {
			if(daybook.chequeNoFrom != null && daybook.chequeNoFrom != undefined && daybook.chequeNoFrom!= "" ){
				if(!CommonValidationService.checkRegExp(appConstant.Reg_Exp_Master_Daybook_Cheque_From,daybook.chequeNoFrom)){
                    return this.validationResponse(true, "chequeNoFrom", $rootScope.nls["ERR200024"], daybook);
    			}
			}
		}
		
		//Validate chequeNoTo
		if(code == 0 || code == 11) {
			if(daybook.chequeNoTo != null && daybook.chequeNoTo != undefined && daybook.chequeNoTo!= "" ){
				if(!CommonValidationService.checkRegExp(appConstant.Reg_Exp_Master_Daybook_Cheque_To,daybook.chequeNoTo)){
                    return this.validationResponse(true, "chequeNoTo", $rootScope.nls["ERR200025"], daybook);
    			}
			}
		}

		//Validate chequeReportName
		if(code == 0 || code == 12) {
			if(daybook.chequeReportName != null && daybook.chequeReportName != undefined && daybook.chequeReportName!= "" ){
				if(!CommonValidationService.checkRegExp(appConstant.Reg_Exp_Master_Daybook_Report_Name,daybook.chequeReportName)){
                    return this.validationResponse(true, "chequeReportName", $rootScope.nls["ERR200026"], daybook);
    			}
			}
		}
		
		//Validate numeric Daybook
		if(code == 0 || code == 13) {
			if(daybook.numericDaybook != null && daybook.numericDaybook != undefined && daybook.numericDaybook!= "" ){
				if(daybook.numericDaybook.status=="Block"){
					return this.validationResponse(true, "numericDaybook", $rootScope.nls["ERR200016"], daybook);	
				}
				else if(daybook.numericDaybook.status=="Hide"){
					return this.validationResponse(true, "numericDaybook", $rootScope.nls["ERR200017"], daybook);	
				}
			}
		}
		
		return this.validationSuccesResponse(daybook);
	}
 
	this.validationSuccesResponse = function(daybook) {
	    return {error : false, obj : daybook};
	}
	
	this.validationResponse = function(err, elem, message, daybook) {
		return {error : err, errElement : elem, errMessage : message, obj : daybook};
	}
	
}]);