app.factory('generalNoteMasterFactory', function($resource) {
	return {
		findById : $resource('/api/v1/generalnotemaster/get/id/:id', {}, {
			 query : {method : 'GET', params : {id : ''}, isArray : false}
		})
		,
		search : $resource('/api/v1/generalnotemaster/get/search', {}, {
			fetch : {method : 'POST'}
		})
		,
		update : $resource('/api/v1/generalnotemaster/update', {}, {
			query : {method : 'POST'}
		})
		,
		delete : $resource('/api/v1/generalnotemaster/delete/:id', {}, {
			query : {method : 'DELETE', params : {id : ''}}
		})
		,
		create : $resource('/api/v1/generalnotemaster/create', {}, {
			query : {method : 'POST'}
		})
		,
		searchByService : $resource("/api/v1/servicemaster/get/search/keyword", {}, {
			query : {
				method : 'POST',
			}
		})
		,
		searchByLocation : $resource("/api/v1/locationmaster/get/search/keyword", {}, {
			query : {
				method : 'POST',
			}
		})
		,
		searchByCategory : $resource("/api/v1/categorymaster/get/search/keyword", {}, {
			query : {
				method : 'POST',
			}
		})
	};
})