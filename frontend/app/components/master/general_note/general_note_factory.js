/**
 * EmployeeMaster Factory which initiates all Rest calls using resource service
 */

(function() {
	

	app.factory("GeneralNote",['$resource', function($resource) {
		return $resource("/api/v1/generalnotemaster/get/generalnote/:locationId/:category/:subCategory/:serviceMasterId", {}, {
			fetch : {
				method : 'GET',
				params : {
					locationId : '',
					category : '',
					subCategory : '',
					serviceMasterId : ''
				},
				isArray : false
			}
		});
	}]);
	
	
	
})();
