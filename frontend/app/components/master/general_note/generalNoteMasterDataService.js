app.service('generalNoteMasterDataService', function($rootScope) {
    this.getHeadArray = function() {
	   return [
			  {
					"name":"#",
					"width":"col-xs-0half",
					"model":"no",
					"search":false
			  },{
					"name":"General Category",
					"width":"col-md-2",
					"model":"category",
					"search":true,
					"type":"drop",
					"data":$rootScope.enum['GeneralNoteCategory'],
					"key":"searchCategory",
					"sort":true
				},{
					"name":"Sub Category",
					"width":"col-md-2",
					"model":"subCategory",
					"search":true,
					"type":"drop",
					"data":$rootScope.enum['GeneralNoteSubCategory'],
					"key":"searchSubCategory",
					"sort":true
				},{
					"name":"Service",
					"width":"col-md-1half",
					"model":"serviceMaster.serviceName",
					"search":true,
					"wrap_cell":true,
					"type":"text",
					"sort":true,
					"key":"searchServiceName"
				},{
					"name":"Location",
					"width":"col-md-1half",
					"model":"locationMaster.locationName",
					"search":true,
					"wrap_cell":true,
					"type":"text",
					"sort":true,
					"key":"searchLocationName"
				}];
          };

    
    this.getListBreadCrumb = function() {
        return [{
                label: "Master",
                state: "layout.generalnotemaster"
            },
            {
                label: "General Note Master",
                state: "layout.generalnotemaster"
            }
        ];
    }

    this.getAddBreadCrumb = function() {
        return [{
                label: "Master",
                state: "layout.generalnotemaster"
            },
            {
                label: "General Note Master",
                state: "layout.generalnotemaster"
            },
            {
                label: "Add General Note Master",
                state: null
            }
        ];
    };
    this.getEditBreadCrumb = function() {
        return [{
                label: "Master",
                state: "layout.generalnotemaster"
            },
            {
            	label: "General Note Master",
                state: "layout.generalnotemaster"
            },
            {
                label: "Edit General Note Master",
                state: null
            }
        ];
    };
    
    this.getViewBreadCrumb = function() {
        return [{
                label: "Master",
                state: "layout.generalnotemaster"
            },
            {
            	label: "General Note Master",
                state: "layout.generalnotemaster"
            },
            {
                label: "View General Note Master",
                state: null
            }
        ];
    };
   

});