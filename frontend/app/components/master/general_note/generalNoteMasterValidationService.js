app.service('generalNoteMasterValidationService', ['$rootScope','ValidateUtil', 'CommonValidationService', 'appConstant', function($rootScope, ValidateUtil, CommonValidationService, appConstant) {
	this.validate = function(generalNoteMaster, code) {
		//Validate generalNoteMaster service code
		if(code == 0 || code == 1) {
			if(generalNoteMaster.category == undefined || generalNoteMaster.category == null || generalNoteMaster.category =="" ){
				return this.validationResponse(true, "category", $rootScope.nls["ERR4218"], generalNoteMaster);
			}
		}
		
		if(code == 0 || code == 2) {
			if(generalNoteMaster.subCategory == undefined || generalNoteMaster.subCategory == null || generalNoteMaster.subCategory =="" ){
				return this.validationResponse(true, "subCategory", $rootScope.nls["ERR4219"], generalNoteMaster);
			}else if(generalNoteMaster.subCategory==="Service"){
				if(generalNoteMaster.serviceMaster == undefined || generalNoteMaster.serviceMaster == null || generalNoteMaster.serviceMaster ==""){
					return this.validationResponse(true, "service", $rootScope.nls["ERR4220"], generalNoteMaster);
				}
			}
		}
		
		if(code == 0 || code == 3){
			if(ValidateUtil.isStatusBlocked(generalNoteMaster.serviceMaster.status)){
  				return this.validationResponse(true, "service", $rootScope.nls["ERR1422"], generalNoteMaster);
		   }
		}
		if(code == 0 || code == 4){
			if(generalNoteMaster.locationMaster == undefined || generalNoteMaster.locationMaster == null || generalNoteMaster.locationMaster =="" ){
				return this.validationResponse(true, "location", $rootScope.nls["ERR4221"], generalNoteMaster);
			}else if(ValidateUtil.isStatusBlocked(generalNoteMaster.locationMaster.status)){
  				return this.validationResponse(true, "location", $rootScope.nls["ERR2233"], generalNoteMaster);
			   }
		}
		
		return this.validationSuccesResponse(generalNoteMaster);
	}
		
 
	this.validationSuccesResponse = function(generalNoteMaster) {
	    return {error : false, obj : generalNoteMaster};
	}
	
	this.validationResponse = function(err, elem, message, generalNoteMaster) {
		return {error : err, errElement : elem, errMessage : message, obj : generalNoteMaster};
	}
	
	
}]);