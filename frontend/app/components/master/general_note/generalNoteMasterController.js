(function(){
app.controller("GeneralNoteMasterCtrl", ['$scope', '$rootScope', '$window', '$modal', '$state', '$stateParams', '$log', 'ngDialog', 'generalNoteMasterDataService', 'generalNoteMasterFactory', 'RecentHistorySaveService', 'generalNoteMasterValidationService', 'Notification','roleConstant',
    function($scope, $rootScope, $window, $modal, $state, $stateParams, $log, ngDialog, generalNoteMasterDataService, generalNoteMasterFactory, RecentHistorySaveService, generalNoteMasterValidationService, Notification, roleConstant ) {
	
	
	  var vm = this;
      vm.tableHeadArr = generalNoteMasterDataService.getHeadArray();
      vm.dataArr = [];
      vm.searchData = {};
      vm.categoryArr = $rootScope.enum['GeneralNoteCategory'];
      vm.subCategoryArr = $rootScope.enum['GeneralNoteSubCategory'];
      vm.generalNoteMaster = {};
      vm.limitArr = [10, 15, 20];
      vm.page = 0;
      vm.limit = 10;
      vm.totalRecord = 100;
      vm.showDetail=false;
      vm.showHistory = false;
      vm.errorMap = new Map();
      vm.deskTopView = true;

      vm.sortSelection = {
          sortKey: "category",
          sortOrder: "asc"
      }
      
      vm.changeSearch = function(param) {
          $log.debug("Change Search is called." + param);
          for (var attrname in param) {
              vm.searchData[attrname] = param[attrname];
          }
          vm.page = 0;
          vm.search();
      }

      vm.sortChange = function(param) {
          $log.debug("sortChange is called. " + param);
          vm.searchData.orderByType = param.sortOrder.toUpperCase();
          vm.searchData.sortByColumn = param.sortKey;
          vm.search();
      }
      
      
      /**
       * GeneralNote Master List Page Search Data  initializing and calling search Mtd.
       */
      vm.searchInit = function() {
          vm.searchData = {};
          vm.searchData.orderByType = vm.sortSelection.sortOrder.toUpperCase();
          vm.searchData.sortByColumn = vm.sortSelection.sortKey;
          vm.search();
      };
      

      /**
       * Get GeneralNote Master's Based on Search Data.
       */
      vm.search = function() {
      	vm.generalNoteMasterView=false;
          vm.searchData.selectedPageNumber = vm.page;
          vm.searchData.recordPerPage = vm.limit;
          vm.dataArr = [];
          generalNoteMasterFactory.search.fetch(vm.searchData).$promise.then(
              function(data) {
                  if (data.responseCode == "ERR0") {
                      vm.totalRecord = data.responseObject.totalRecord;
                      vm.dataArr = data.responseObject.searchResult;
                  }
              },
              function(errResponse) {
              	 $log.error('Error while fetching GeneralNoteMaster');
              });
         }
      
      /* Master Select Picker */
      vm.ajaxServiceEvent = function(object) {
          vm.searchDto = {};
          vm.searchDto.keyword = object == null ? "" : object;
          vm.searchDto.selectedPageNumber = 0;
          vm.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
          return generalNoteMasterFactory.searchByService.query(vm.searchDto).$promise.then(function(data, status) {
                  if (data.responseCode == "ERR0") {
                      vm.serviceMasterList = data.responseObject.searchResult;
                      return vm.serviceMasterList;
                  }
              },
              function(errResponse) {
                  console.error('Error while fetching Service Master');
              }
          );

      }
      
      
      /* Master Select Picker */
      vm.ajaxLocationEvent = function(object) {
          vm.searchDto = {};
          vm.searchDto.keyword = object == null ? "" : object;
          vm.searchDto.selectedPageNumber = 0;
          vm.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
          return generalNoteMasterFactory.searchByLocation.query(vm.searchDto).$promise.then(function(data, status) {
                  if (data.responseCode == "ERR0") {
                      vm.chargeMasterList = data.responseObject.searchResult;
                      return vm.chargeMasterList;
                  }
              },
              function(errResponse) {
                  console.error('Error while fetching Location Master');
              }
          );

      }
      
      
      /* Master Select Picker */
      vm.ajaxCategoryEvent = function(object) {
          vm.searchDto = {};
          vm.searchDto.keyword = object == null ? "" : object;
          vm.searchDto.selectedPageNumber = 0;
          vm.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
          return generalNoteMasterFactory.searchByCategory.query(vm.searchDto).$promise.then(function(data, status) {
                  if (data.responseCode == "ERR0") {
                      vm.categoryMasterList = data.responseObject.searchResult;
                      return vm.categoryMasterList;
                  }
              },
              function(errResponse) {
                  console.error('Error while fetching category Master');
              }
          );

      }
      
      vm.selectedMaster = function(obj,master,code){
    	  
    	  var validationResponse=generalNoteMasterValidationService.validate(master,code);
    	  if(validationResponse.error==true){
    		  $scope.validationErrorAndFocus(validationResponse, validationResponse.errElement);
    	  }
    	  else{
    		  validationResponse=false;
    		  $scope.errorMap = new Map();
    		  $rootScope.navigateToNextField(obj);
    	  }
      }
      
     
      
      
      
     /* *//**
       * Reason Master List Page Search Data  initializing and calling search Mtd.
       * @param {Object} data - The Selected Reason Master from List page.
       * @param {string} data.id - The id of the Selected Reason Master.
       * @param {int} index - The Selected row index from List page.
       *//*
      
      
      *//**
       * Modify Existing Reason Master.
       * @param {int} id - The id of a Country.
       *//*
      vm.edit = function(id) {
          $state.go("layout.reasonMasterEdit", {
              id: id
          });
      }
      
      // set status of reason master
      
      vm.setStatus = function(data){
  		if(data=='Active'){
  			return 'activetype';
  		} else if(data=='Block'){
  			return 'blockedtype';
  		}else if(data=='Hide'){
  			return 'hiddentype';
  		}
  	}
  	*/
      
      
      /**
       * Go Back to reason Master List Page.
       */
      
      vm.backToList = function() {
    	  //$state.go("layout.generalnotemaster");
    	  vm.generalNoteMasterView=false;
    	  vm.deskTopView = true;
        }
      
      
      /**
       * Go To Add GeneralNoteMaster.
       */
      
      vm.addNew = function() {
    	  if($rootScope.roleAccess(roleConstant.MASTER_FINANCE_SERVICE_CHARGE_MAPPING_CREATE)){
    		  $state.go("layout.generalnotemasterAdd");
    	  }
      }
      
      
      /**
       * Cancel Add or Edit GeneralNoteMaster.
       * @param {int} id - The id of a GeneralNoteMaster
       */
     
      
      vm.edit = function(id) {
    	  if($rootScope.roleAccess(roleConstant.MASTER_FINANCE_SERVICE_CHARGE_MAPPING_MODIFY)){
    		  $state.go("layout.generalnotemasterEdit", {
                  id: id
              });
    	  }
      }
      
      
      /**
       * Delete GeneralNoteMaster.
       * @param {int} id - The id of a GenearlNoteMaster.
       */
      
      vm.delete = function(id) {
    	  if($rootScope.roleAccess(roleConstant.MASTER_FINANCE_SERVICE_CHARGE_MAPPING_DELETE)){
    		  var newScope = $scope.$new();
    			newScope.errorMessage = $rootScope.nls["ERR06604"];
    	    	ngDialog.openConfirm( {
    	                    template : '<p>{{errorMessage}}</p>'
    	                    + '<div class="ngdialog-footer">'
    	                    + '<button type="button" class="btn btn-default btn-property cancel-btn" ng-click="closeThisDialog(0)">Cancel'
    	                    + '<button type="button" class="btn btn-primary btn-property accent-btn" ng-click="confirm(1)">Yes'
    	                    + '</button></div>',
    	                    plain : true,
    	                    scope : newScope,
    	                    closeByDocument: false,
    	                    className : 'ngdialog-theme-default'
    	                }).then( function(value) {
    	                	generalNoteMasterFactory.delete.query({
    	                        id: id
    	                    }).$promise.then(function(data) {
    	                        if (data.responseCode == 'ERR0') {
    	                        	Notification.success($rootScope.nls["ERR402"]);
    	                        	//$state.go("layout.generalnotemaster");
    	                        	vm.deskTopView = true;
    	                        	vm.search();
    	                        }
    	                    }, function(error) {
    	                        $log.debug("Error ", error);
    	                    });
    	                }, function(value) {
    	                    $log.debug("delete Cancelled");
    	            });
    	  }    	  
        }
      
      
      //Update GenearlNoteMaster
      vm.update = function() {
    	  var validationResponse = generalNoteMasterValidationService.validate(vm.generalNoteMaster,0);
           if(validationResponse.error == true) {
           	$log.debug("Validation Response -- ", validationResponse);
           	$log.debug('Validation Failed while Updating the Reason..')
				$scope.validationErrorAndFocus(validationResponse, validationResponse.errElement);
			}
           else {
        	   generalNoteMasterFactory.update.query(vm.generalNoteMaster).$promise.then(function(data) {
		            if (data.responseCode == 'ERR0') {
		            	$log.debug("Reason Updated Successfully Response :: ", data);
		            	Notification.success($rootScope.nls["ERR401"]);
		            	$state.go('layout.generalnotemaster');
		            } else {
		                $log.debug("Updating Reason Failed :" + data.responseDescription)
		            }
		        }, function(error) {
		        	 $log.debug("Updating Reason Failed : " + error)
		        });
           } 
      };
     
      
      /**
       * Create New GeneralNoteMaster.
       */
      vm.create = function() {
      	var validationResponse = generalNoteMasterValidationService.validate(vm.generalNoteMaster,0);
          if(validationResponse.error == true) {
          	$log.debug("Validation Response -- ", validationResponse);
          	$log.debug('Validation Failed to create ReasonMaster..')
				$scope.validationErrorAndFocus(validationResponse, validationResponse.errElement);
			}
          else {
          	generalNoteMasterFactory.create.query(vm.generalNoteMaster).$promise.then(function(data) {
		            if (data.responseCode == 'ERR0') {
		            	$log.debug("Reason Saved Successfully Response :: ", data);
		            	Notification.success($rootScope.nls["ERR400"]);
		            	$state.go('layout.generalnotemaster');
		            } else {
		                $log.debug("Create Reason Failed :" + data.responseDescription)
		            }

		        }, function(error) {
		        	 $log.debug("Create Reason Failed : " + error)
		        });
          } 
      };
      
      

      /**
       * Cancel Add or Edit GeneralNoteMaster.
       * @param {int} id - The id of a GeneralNoteMaster
       */
      vm.cancel = function(objId) {
          if(vm.generalNoteMasterForm!=undefined){
		    	vm.goTocancel(vm.generalNoteMasterForm.$dirty,objId);
          }else{
          	vm.goTocancel(false,objId);
          }
      }
     
     vm.goTocancel = function(isDirty,objId){
   		 if (isDirty) {
   		      var newScope = $scope.$new();
   		      newScope.errorMessage = $rootScope.nls["ERR200"];
   		      ngDialog.openConfirm({
   		        template: '<p>{{errorMessage}}</p>' +
   		          '<div class="ngdialog-footer">' +
   		          ' <button type="button" class="btn btn-default btn-property cancel-btn" ng-click="confirm(2)">No</button>' +
   		          '<button type="button" class="btn btn-default btn-property accent-btn " ng-click="confirm(1)">Yes</button>' +
   		          '<button type="button" class="btn btn-default btn-property pull-left grey-cancel-btn" ng-click="confirm(3)">Cancel</button>' +
   		          '</div>',
   		        plain: true,
   		        closeByDocument: false,
   		        scope: newScope,
   		        className: 'ngdialog-theme-default'
   		      }).then(
   		        function(value) {
   		          if (value == 1) {
   		            if(objId!=null && objId != undefined){
   		            	vm.update();
   		            }else{
   		            	vm.create();
   		            }
   		          } else if (value == 2) {
   		        	 $state.go("layout.generalnotemaster", {
   		                submitAction: "Cancelled"
   		            });
   		          } else {
   		            $log.debug("cancelled");
   		          }
   		        });
   		    } else {
   		     $state.go("layout.generalnotemaster", {
                 submitAction: "Cancelled"
             });
   		    }
   	    }
      
     
     /**
      * Common Validation Focus Functionality - starts here" 
      */
    
		$scope.validationErrorAndFocus = function(valResponse, elemId){
			$scope.errorMap = new Map();
			if(valResponse.error == true){
				$scope.errorMap.put(valResponse.errElement, valResponse.errMessage);
			}
				$scope.navigateToNextField(elemId);
		}
		
		
		/**
	       * Get GeneralNoteMaster  By id.
	       * @param {int} id - The id of a GeneralNoteMaster.
	       * @param {boolean} isView - TO identify purpose of calling is for View or Edit a GeneralNoteMaster.
	       */
	     
	      vm.view = function(id, isView) {
	    	  vm.id=id;
	      	generalNoteMasterFactory.findById.query({
	              id: vm.id
	          }).$promise.then(function(data) {
	              if (data.responseCode == 'ERR0') {
	            	  $log.debug("response is sucess");
	                  vm.generalNoteMaster = data.responseObject;
	                  var rHistoryObj = {}

	                  if (vm.generalNoteMaster != undefined && vm.generalNoteMaster != null && vm.generalNoteMaster.id != undefined && vm.generalNoteMaster.id != null) {
	                      $rootScope.subTitle = "ServiceTaxChargeGroup";
	                  } else {
	                      $rootScope.subTitle = "Unknown GeneralNote"
	                  }
	                  $rootScope.unfinishedFormTitle = "GeneralNote";
	                  if (isView) {
	                      rHistoryObj = {
	                          'title': 'GeneralNoteMaster',
	                          'subTitle': $rootScope.subTitle,
	                          'stateName': $state.current.name,
	                          'stateParam': JSON.stringify($stateParams),
	                          'stateCategory': 'GeneralNote Master',
	                          'serviceType': undefined,
	                          'showService': false
	                      }

	                  } else {

	                      rHistoryObj = {
	                          'title': 'GeneralNoteMaster',
	                          'subTitle': $rootScope.subTitle,
	                          'stateName': $state.current.name,
	                          'stateParam': JSON.stringify($stateParams),
	                          'stateCategory': 'GeneralNote Master',
	                          'serviceType': 'MASTER',
	                          'serviceCode': ' ',
	                          'orginAndDestination': ' '
	                      }
	                  }
	                  RecentHistorySaveService.form(rHistoryObj);
	              } else {
	                  $log.debug("Exception while getting GeneralNote by Id ", data.responseDescription);
	              }
	          }, function(error) {
	              $log.debug("Error ", error);
	          });
	      };
	      
	      /**
	       * GeneralNoteMaster  List Page Search Data  initializing and calling search Mtd.
	       * @param {Object} data - The Selected GeneralNoteMaster  from List page.
	       * @param {string} data.id - The id of the Selected GeneralNoteMaster.
	       * @param {int} index - The Selected row index from List page.
	       */
	      
	      vm.rowSelect = function(data) {
	    	  if($rootScope.roleAccess(roleConstant.MASTER_FINANCE_SERVICE_CHARGE_MAPPING_VIEW)){
	    		  vm.generalNoteMaster = data;
		  			vm.generalNoteMasterView=true;
		  			 vm.showDetail=true;
		  			/*vm.id=data.id;
		  			$state.go("layout.generalnotemasterView",{id:data.id});*/
		  			var windowInner=$window.innerWidth;
		  			if(windowInner<=1199){
		  				vm.deskTopView = false;
		  			}
		  			else{
		  				vm.deskTopView = true;
		  			}
		  			angular.element($window).bind('resize', function(){
		  				if($window.innerWidth>=1200){
		  					vm.deskTopView = true;
		  				}
		  				else if($window.innerWidth<=1199){
		  					if(vm.reasonMaster.id!==false){
		  						vm.deskTopView = false;
		  					}
		  					else {
		  						vm.deskTopView = true;
		  					}
		  				}
		  				$scope.$digest();
		  			});
	    		  
	    	  }
	  		}
	     
	      
	      
      
      
      /**
       * State Identification Handling Here.
       * @param {String} $stateParams.action - VIEW, ADD, EDIT, SEARCH.
       */
      //
      switch ($stateParams.action) {
          case "ADD":
          	 if ($stateParams.fromHistory === 'Yes') {
                   if (vm.isReloaded == "YES") {
                    vm.isReloaded = "NO";
                    localStorage.isReasonMasterReloaded = "NO";
                     vm.reasonMaster = JSON.parse(localStorage.reloadFormData);
                   } else {
                     vm.reasonMaster = $rootScope.selectedUnfilledFormData;
                   }
                 } else {
                   if (vm.isReloaded == "YES") {
                     vm.isReloaded = "NO";
                     localStorage.isReasonMasterReloaded = "NO";
                     vm.reasonMaster = JSON.parse(localStorage.reloadFormData);
                   } else {
                       $log.debug("Add NotHistory NotReload...")
                   }
                 }
              $rootScope.breadcrumbArr = generalNoteMasterDataService.getAddBreadCrumb();
              break;
          case "EDIT":
          	 if (vm.fromHistory === 'Yes') {
                   if (vm.isReloaded == "YES") {
                     vm.isReloaded = "NO";
                     localStorage.isgeneralNoteMasterReloaded = "NO";
                     vm.reasonMaster = JSON.parse(localStorage.reloadFormData);
                   } else {
                     vm.reasonMaster = $rootScope.selectedUnfilledFormData;
                   }
                 } else {
                   if (vm.isReloaded == "YES") {
                     vm.isReloaded = "NO";
                     localStorage.isReasonMasterReloaded = "NO";
                     vm.reasonMaster = JSON.parse(localStorage.reloadFormData);
                   } else {
                  	 vm.view($stateParams.id, false);
                   }
                 }
              $rootScope.breadcrumbArr = generalNoteMasterDataService.getEditBreadCrumb();
              break;
          case "VIEW":
        	  vm.view($stateParams.id, false);
        	  $rootScope.breadcrumbArr = generalNoteMasterDataService.getViewBreadCrumb();
        	  break;
          default:
              $rootScope.breadcrumbArr = generalNoteMasterDataService.getListBreadCrumb();
              vm.searchInit();
      }
      
 
}
]);
}) ();