app.controller('partyTypeOperationCtrl',['$rootScope', '$stateParams', 'partyTypeView', '$state', '$scope', 'partyTypeService', 'partyTypeUpdate', 
        'partyTypeSave', '$http', '$location', 'ngProgressFactory', 'ngDialog', 'appConstant', 'CommonValidationService',
    function($rootScope, $stateParams, partyTypeView, $state, $scope, partyTypeService, partyTypeUpdate, 
        partyTypeSave, $http, $location, ngProgressFactory, ngDialog, appConstant, CommonValidationService) {


    $scope.init = function(acFlag) {


        if ($scope.partyTypeMaster == undefined || $scope.partyTypeMaster == null) {
            $scope.partyTypeMaster = {};
        }


        $scope.contained_progressbar = ngProgressFactory.createInstance();
        $scope.contained_progressbar.setParent(document.getElementById('partyType-panel'));
        $scope.contained_progressbar.setAbsolute();

        if ($scope.partyTypeMaster.id == null)
            $scope.partyTypeMaster.status = 'Active';

        if (acFlag != undefined && acFlag != null && acFlag === 0) {
            $scope.setOldDataVal();
        }
    };


    $scope.setOldDataVal = function() {
        $scope.oldData = JSON.stringify($scope.partyTypeMaster);
        console.log("partyTypeMaster Data Loaded");
    }
    $scope.cancel = function() {
        var tempObj = $scope.partyTypeMasterForm;
        if ($scope.oldData != JSON.stringify($scope.partyTypeMaster)) {
            if (Object.keys(tempObj).length) {
                var newScope = $scope.$new();
                newScope.errorMessage = $rootScope.nls["ERR200"];
                ngDialog.openConfirm({
                        template: '<p>{{errorMessage}}</p>' +
                            '<div class="ngdialog-footer">' +
                            ' <button type="button" class="btn btn-default btn-property cancel-btn" ng-click="confirm(2)">No</button>' +
                            '<button type="button" class="btn btn-default btn-property accent-btn " ng-click="confirm(1)">Yes</button>' +
                            '<button type="button" class="btn btn-default btn-property pull-left grey-cancel-btn" ng-click="confirm(3)">Cancel</button>' +
                            '</div>',
                        plain: true,
                        scope: newScope,
                        className: 'ngdialog-theme-default'
                    })
                    .then(
                        function(value) {
                            if (value == 1 &&
                                $scope.partyTypeMaster.id == null) {
                                $scope.save();
                            } else if (value == 1 &&
                                $scope.partyTypeMaster.id != null) {
                                $scope.update();
                            } else if (value == 2) {
                                var params = {}
                                params.submitAction = 'Cancelled';
                                $state.go("layout.partyType", params);
                            } else {
                                console.log("cancelled");
                            }

                        });
            } else {
                var params = {}
                params.submitAction = 'Cancelled';
                $state.go("layout.partyType", params);
            }
        } else {
            var params = {}
            params.submitAction = 'Cancelled';
            $state.go("layout.partyType", params);
        }
    }


    $scope.save = function() {
        console.log("Save Method is called.");
        if ($scope.validatePartyTypeMaster(0)) {
            $scope.contained_progressbar.start();
            partyTypeSave.save($scope.partyTypeMaster).$promise.then(function(data) {
                if (data.responseCode == 'ERR0') {
                    partyTypeService.set({});
                    var params = {}
                    params.submitAction = 'Saved';
                    $rootScope.successDesc = $rootScope.nls["ERR400"];
                    $state.go("layout.partyType", params);
                } else {
                    console.log("Party added Failed " + data.responseDescription)
                }
                $scope.contained_progressbar.complete();
                angular.element(".panel-body").animate({
                    scrollTop: 0
                }, "slow");
            }, function(error) {
                console.log("Party added Failed : " + error)
            });
        } else {
            console.log("Invalid Form Submission.");
        }
    }

    $scope.update = function() {
        console.log("Update Method is called.");
        if ($scope.validatePartyTypeMaster(0)) {
            $scope.contained_progressbar.start();

            partyTypeUpdate.update($scope.partyTypeMaster).$promise.then(
                function(data) {
                    if (data.responseCode == 'ERR0') {
                        console.log("Cost Center updated Successfully")
                        partyTypeService.set({});
                        var params = {}
                        params.submitAction = 'Saved';
                        $rootScope.successDesc = $rootScope.nls["ERR401"];
                        $state.go("layout.partyType", params);

                    } else {
                        console.log("Party Type updated Failed " +
                            data.responseDescription)
                    }
                    $scope.contained_progressbar.complete();
                    angular.element(".panel-body").animate({
                        scrollTop: 0
                    }, "slow");
                },
                function(error) {
                    console.log("Party Type updated Failed : " + error)
                });
        } else {
            console.log("Invalid Form Submission.");
        }


    }



    // Party Type Master Validation started

    $scope.validatePartyTypeMaster = function(validateCode) {

        $scope.errorMap = new Map();

        if (validateCode == 0 || validateCode == 1) {

            if ($scope.partyTypeMaster.partyTypeName == null ||
                $scope.partyTypeMaster.partyTypeName == undefined ||
                $scope.partyTypeMaster.partyTypeName == "") {
                $scope.errorMap.put("partyTypeName", $scope.nls["ERR2706"]);
                return false
            } else {
                if (!CommonValidationService.checkRegExp(appConstant.Reg_Exp_Master_PARTY_TYPE_NAME, $scope.partyTypeMaster.partyTypeName)) {
                    $scope.errorMap.put("partyTypeName", $rootScope.nls["ERR2709"]);
                    return false;
                }
            }
        }

        if (validateCode == 0 || validateCode == 2) {

            if ($scope.partyTypeMaster.partyTypeCode == null ||
                $scope.partyTypeMaster.partyTypeCode == undefined ||
                $scope.partyTypeMaster.partyTypeCode == "") {
                $scope.errorMap.put("partyTypeCode", $scope.nls["ERR2705"]);
                return false
            } else {
                if (!CommonValidationService.checkRegExp(appConstant.Reg_Exp_Master_PARTY_TYPE_CODE, $scope.partyTypeMaster.partyTypeCode)) {
                    $scope.errorMap.put("partyTypeCode", $rootScope.nls["ERR2713"]);
                    return false;
                }
            }
        }

        return true;

    }
    $scope.setStatus = function(data) {
        if (data == 'Active') {
            return 'activetype';
        } else if (data == 'Block') {
            return 'blockedtype';
        } else if (data == 'Hide') {
            return 'hiddentype';
        }
    }


    // History Start

    $scope.getPartyTypeById = function(partyTypeId) {
        console.log("getPartyTypeById ", partyTypeId);

        partyTypeView.get({
            id: partyTypeId
        }, function(data) {
            if (data.responseCode == "ERR0") {
                console.log("Successful while getting party type.", data)
                $scope.partyTypeMaster = data.responseObject;
            }
            $scope.setOldDataVal();
        }, function(error) {
            console.log("Error while getting party type.", error)
        });

    }


    //On leave the Unfilled party type Form Broadcast then event will happen in StateChangeListener(BroadCaster)-app_state_route.js

    $scope.$on('addPartyTypeEvent', function(events, args) {

        $rootScope.unfinishedData = $scope.partyTypeMaster;
        $rootScope.category = "Master";
        $rootScope.unfinishedFormTitle = "PartyType (New)";
        if ($scope.partyTypeMaster != undefined && $scope.partyTypeMaster != null && $scope.partyTypeMaster.partyTypeName != undefined &&
            $scope.partyTypeMaster.partyTypeName != null && $scope.partyTypeMaster.partyTypeName != "") {
            $rootScope.subTitle = $scope.partyTypeMaster.partyTypeName;
        } else {
            $rootScope.subTitle = "Unknown PartyType"
        }
    })

    $scope.$on('editPartyTypeEvent', function(events, args) {
        $rootScope.unfinishedData = $scope.partyTypeMaster;
        $rootScope.category = "Master";
        $rootScope.unfinishedFormTitle = "PartyType Edit";
        if ($scope.partyTypeMaster != undefined && $scope.partyTypeMaster != null && $scope.partyTypeMaster.partyTypeName != undefined &&
            $scope.partyTypeMaster.partyTypeName != null && $scope.partyTypeMaster.partyTypeName != "") {
            $rootScope.subTitle = $scope.partyTypeMaster.partyTypeName;
        } else {
            $rootScope.subTitle = "Unknown PartyType"
        }
    })


    $scope.$on('addPartyTypeEventReload', function(e, confirmation) {
        console.log("addPartyTypeEventReload is called ", $scope.partyTypeMaster);
        confirmation.message = "";
        localStorage.reloadFormData = JSON.stringify($scope.partyTypeMaster);
        localStorage.isPartyTypeReloaded = "YES";
        e.preventDefault();
    });


    $scope.$on('editPartyTypeEventReload', function(e, confirmation) {
        confirmation.message = "";
        localStorage.reloadFormData = JSON.stringify($scope.partyTypeMaster);
        localStorage.isPartyTypeReloaded = "YES";
        e.preventDefault();
    });


    $scope.isReloaded = localStorage.isPartyTypeReloaded;

    if ($stateParams.action == "ADD") {
        if ($stateParams.fromHistory === 'Yes') {
            if ($scope.isReloaded == "YES") {
                console.log("Add History Reload...")
                $scope.isReloaded = "NO";
                localStorage.isPartyTypeReloaded = "NO";
                $scope.partyTypeMaster = JSON.parse(localStorage.reloadFormData);
                $scope.init();
            } else {
                console.log("Add History NotReload...")
                $scope.partyTypeMaster = $rootScope.selectedUnfilledFormData;
                $scope.init();
            }
            $scope.oldData = "";
        } else {
            if ($scope.isReloaded == "YES") {
                $scope.isReloaded = "NO";
                localStorage.isPartyTypeReloaded = "NO";
                $scope.partyTypeMaster = JSON.parse(localStorage.reloadFormData);
                console.log("Add NotHistory Reload...");
                $scope.init();
                $scope.oldData = "";
            } else {
                console.log("Add NotHistory NotReload...")
                $scope.init(0);
            }
        }
        $rootScope.breadcrumbArr = [{
                label: "Master",
                state: "layout.partyType"
            },
            {
                label: $rootScope.appMasterData['Party_Label'] + ' Type',
                state: "layout.partyType"
            },
            {
                label: 'Add ' + $rootScope.appMasterData['Party_Label'] + ' Type',
                state: null
            }
        ];

    } else {
        if ($stateParams.fromHistory === 'Yes') {
            if ($scope.isReloaded == "YES") {
                $scope.isReloaded = "NO";
                localStorage.isPartyTypeReloaded = "NO";
                $scope.partyTypeMaster = JSON.parse(localStorage.reloadFormData);
                $scope.init();
            } else {
                $scope.partyTypeMaster = $rootScope.selectedUnfilledFormData;
                $scope.init();
            }
            $scope.oldData = "";
        } else {
            if ($scope.isReloaded == "YES") {
                $scope.isReloaded = "NO";
                localStorage.isPartyTypeReloaded = "NO";
                $scope.partyTypeMaster = JSON.parse(localStorage.reloadFormData);
                $scope.init();
                $scope.oldData = "";
            } else {
                $scope.getPartyTypeById($stateParams.partyTypeId);
                $scope.init();
            }
        }

        $rootScope.breadcrumbArr = [{
                label: "Master",
                state: "layout.partyType"
            },
            {
                label: $rootScope.appMasterData['Party_Label'] + ' Type',
                state: "layout.partyType"
            },
            {
                label: 'Edit ' + $rootScope.appMasterData['Party_Label'] + ' Type',
                state: null
            }
        ];
    }

    // History End




}]);