/**
 * PartyTypeMaster Factory which initiates all Rest calls using resource party
 */

(function() {
	
	app.factory("PartyTypeKeyword",['$resource', function($resource) {
		return $resource("/api/v1/partytypemaster/get/search/keyword", {}, {
			fetch : {
				method : 'POST'
			}
		});
	}]);
	
	app.factory("PartyTypeGetAll",['$resource', function($resource) {
		return $resource("/api/v1/partytypemaster/get/all", {}, {
			get : {
				method : 'GET',
				params : {},
				isArray : false
			}
		});
	}]);
	
	
	app.factory("partyTypeSearch",['$resource', function($resource) {
		return $resource("/api/v1/partytypemaster/get/search", {}, {
			query : {
				method : 'POST'
			}
		});
	}]);
	
	
	
	app.factory("partyTypeSave",['$resource', function($resource) {
		return $resource("/api/v1/partytypemaster/create", {}, {
			save : {
				method : 'POST'
			}
		});
	}]);

	app.factory("partyTypeUpdate",['$resource', function($resource) {
		return $resource("/api/v1/partytypemaster/update", {}, {
			update : {
				method : 'POST'
			}
		});
	}]);

	app.factory("partyTypeView",['$resource', function($resource) {
		return $resource("/api/v1/partytypemaster/get/id/:id", {}, {
			get : {
				method : 'GET',
				params : {
					id : ''
				},
				isArray : false
			}
		});
	}]);
	
	app.factory("partyTypeRemove",['$resource', function($resource) {
		return $resource("/api/v1/partytypemaster/delete/:id", {}, {
			remove : {
				method : 'DELETE',
				params : {
					id : ''
				},
				isArray : false
			}
		});
	}]);
	
	
})();
