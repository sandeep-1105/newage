app.controller('partyTypeCtrl',['$rootScope', '$state', '$stateParams', '$scope', '$http', '$location', '$window', '$modal', 
    	'partyTypeService', 'partyTypeRemove', 'partyTypeSearch', 'ngTableParams', 'ngDialog', 'roleConstant',
    function($rootScope, $state, $stateParams, $scope, $http, $location, $window, $modal, 
    	partyTypeService, partyTypeRemove, partyTypeSearch, ngTableParams, ngDialog, roleConstant) {


        /************************************************************
         * ux - by Muthu
         * reason - ipad compatibility fix
         *
         * *********************************************************/
        $scope.deskTopView = true;

        /********************* ux fix ends here *****************************/
        $scope.partyTypeHeadArr = [{
                "name": "#",
                "width": "col-xs-0half",
                "model": "no",
                "search": false,
            },
            {
                "name": "Name",
                "width": "col-xs-6",
                "model": "partyTypeName",
                "search": true,
                "wrap_cell": true,
                "type": "text",
                "sort": true,
                "key": "searchPartyTypeName"
            },
            {
                "name": "Code",
                "width": "col-xs-2half",
                "model": "partyTypeCode",
                "search": true,
                "type": "text",
                "wrap_cell": true,
                "sort": true,
                "key": "searchPartyTypeCode"
            },
            {
                "name": "Status",
                "width": "col-xs-3",
                "model": "status",
                "search": true,
                "type": "drop",
                "sort": true,
                "data": $rootScope.enum['LovStatus'],
                "key": "searchStatus"
            }
        ]

        $scope.sortSelection = {
            sortKey: "partyTypeName",
            sortOrder: "asc"
        }

        $scope.partyTypeMaster = {};

        $scope.searchDto = {};
        $scope.limitArr = [10, 15, 20];
        $scope.page = 0;
        $scope.limit = 10;


        $scope.init = function() {
            $rootScope.setNavigate1("Master");
            $rootScope.setNavigate2("Party Type");
            $scope.searchDto = {};
            if ($scope.partyTypeMaster.id == null)
                $scope.partyTypeMaster.status = 'Active';
            $scope.search();

        };


        $scope.limitChange = function(item) {
            console.log("Limit Change is called...", item);
            $scope.page = 0;
            $scope.limit = item;
            $scope.cancel();
            $scope.search();
        }

        $scope.rowSelect = function(data) {
            if ($rootScope.roleAccess(roleConstant.MASTER_CRM_PARTY_TYPE_VIEW)) {
                $scope.showLogs = false;
                $scope.partyTypeMaster = data;

                console.log("data", data);
                /************************************************************
                 * ux - by Muthu
                 * reason - ipad compatibility fix
                 *
                 * *********************************************************/

                var windowInner = $window.innerWidth;
                if (windowInner <= 1199) {
                    $scope.deskTopView = false;

                } else {
                    $scope.deskTopView = true;
                }
                angular.element($window).bind('resize', function() {
                    if ($window.innerWidth >= 1200) {
                        $scope.deskTopView = true;
                    } else if ($window.innerWidth <= 1199) {
                        if ($scope.partyTypeMaster.id != null) {
                            $scope.deskTopView = false;
                        } else {
                            $scope.deskTopView = true;
                        }
                    }

                    console.log("window resizing..." + $window.innerWidth);
                    $scope.$digest();
                });

                /********************* ux fix ends here *****************************/
            }
        }

        $scope.changeSearch = function(param) {
            console.log("Change Search", param)
            for (var attrname in param) {
                $scope.searchDto[attrname] = param[attrname];
            }
            $scope.page = 0;
            $scope.cancel();
            $scope.search();
            console.log("change search", $scope.searchDto);
        }

        $scope.sortChange = function(param) {
            console.log("Sort Change Called.", param);
            $scope.searchDto.orderByType = param.sortOrder.toUpperCase();
            $scope.searchDto.sortByColumn = param.sortKey;
            $scope.cancel();
            $scope.search();
        }

        $scope.changepage = function(param) {
            console.log("Change Page Called.", param);
            $scope.page = param.page;
            $scope.limit = param.size;
            $scope.search();
        }



        $scope.addPartyTypeOperation = function() {
            if ($rootScope.roleAccess(roleConstant.MASTER_CRM_PARTY_TYPE_CREATE)) {
                console.log("Add Button is Pressed.")
                partyTypeService.set({});
                $state.go("layout.addPartyType");

            }
        }

        $scope.serialNo = function(index) {
            index = ($scope.searchDto.selectedPageNumber * $scope.searchDto.recordPerPage) + (index + 1);
            return index;
        }


        $scope.cancel = function() {
            $scope.showLogs = false;
            $scope.partyTypeMaster = {};
            /************************************************************
             * ux - by Muthu
             * reason - ipad compatibility fix
             *
             * *********************************************************/
            $scope.deskTopView = true;

            /********************* ux fix ends here *****************************/
        }

        $scope.search = function() {
            $scope.partyTypeMaster = {};

            $scope.searchDto.selectedPageNumber = $scope.page;
            $scope.searchDto.recordPerPage = $scope.limit;
            $scope.partyTypeArr = [];
            partyTypeSearch.query($scope.searchDto).$promise.then(function(
                data, status) {
                $scope.totalRecord = data.responseObject.totalRecord;
                var tempArr = [];
                var resultArr = [];
                tempArr = data.responseObject.searchResult;
                console.log("tempArr", tempArr);
                var tempObj = {};
                angular.forEach(tempArr, function(item, index) {
                    tempObj = item;
                    tempObj.no = (index + 1) + ($scope.page * $scope.limit);
                    resultArr.push(tempObj);
                    tempObj = {};
                });
                $scope.partyTypeArr = resultArr;
            });

        }


        $scope.editPartyType = function() {
            if ($rootScope.roleAccess(roleConstant.MASTER_CRM_PARTY_TYPE_MODIFY)) {
                $state.go("layout.editPartyType", {
                    partyTypeId: $scope.partyTypeMaster.id
                });
            }
        }
        $scope.deletePartyType = function() {
            if ($rootScope.roleAccess(roleConstant.MASTER_CRM_PARTY_TYPE_DELETE)) {
                var newScope = $scope.$new();
                newScope.errorMessage = $rootScope.nls["ERR245"];
                ngDialog
                    .openConfirm({
                        template: '<p>{{errorMessage}}</p>' +
                            '<div class="ngdialog-footer">' +
                            '<button type="button" class="btn btn-default btn-property cancel-btn" ng-click="closeThisDialog(0)">Cancel' +
                            '<button type="button" class="btn btn-primary btn-property accent-btn" ng-click="confirm(1)">Yes' +
                            '</button></div>',
                        plain: true,
                        scope: newScope,
                        className: 'ngdialog-theme-default'
                    })
                    .then(
                        function(value) {
                            partyTypeRemove
                                .remove({
                                        id: $scope.partyTypeMaster.id
                                    },
                                    function(data) {
                                        if (data.responseCode == 'ERR0') {
                                            console.log("Cost deleted Successfully")
                                            $scope.init();
                                        } else {
                                            console.log("Cost deleted Failed " + data.responseDescription)
                                        }
                                    },
                                    function(error) {
                                        console.log("Cost deleted Failed : " + error)
                                    });
                        },
                        function(value) {
                            console.log("deleted cancelled");
                        });
            }
        }


        // Routing start

        switch ($stateParams.action) {
            case "SEARCH":
                $scope.init();
                $rootScope.breadcrumbArr = [{
                        label: "Master",
                        state: "layout.partyType"
                    },
                    {
                        label: $rootScope.appMasterData['Party_Label'] + ' Type',
                        state: "layout.partyType"
                    }
                ];
                break;
            case "VIEW":
                break;
        }

        // Routing end



    }]);