/**
 * 
 */

(function() {
	app.factory("PeriodList",['$resource', function($resource) {
		return $resource("/api/v1/periodmaster/getall", {}, {
			get : {
				method : 'GET'
			}
		});
	}]);
})();
