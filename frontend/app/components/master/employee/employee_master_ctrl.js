app.controller('EmployeeController',['$rootScope', '$scope', '$state', '$stateParams', '$window', '$modal', 'ngDialog', 'EmployeeFactory','downloadFactory','Notification','roleConstant','$filter',
	                     function($rootScope, $scope, $state, $stateParams, $window, $modal, ngDialog, EmployeeFactory, downloadFactory, Notification, roleConstant,$filter) {
	
		/************************************************************
		 * ux - by Muthu
		 * reason - ipad compatibility fix
		 *
		 * *********************************************************/
		$scope.deskTopView = true;
	
		/********************* ux fix ends here *****************************/
		$scope.$roleConstant =roleConstant;
		$scope.limitArr = [10,15,20];
		$scope.page = 0;
		$scope.limit = 10;
		$scope.showHistory = false;

		
		
		$scope.toggleHistory = function() {
			console.log("toggleHistory.......................................")
			$scope.showHistory = !$scope.showHistory;
		}
		
		$scope.tempEmployeeStatus = [];
		for(var i = 0; i < $rootScope.enum['EmploymentStatus'].length; i++) {
			$scope.tempEmployeeStatus.push($filter('EmpStatus')($rootScope.enum['EmploymentStatus'][i]));
		}
		
		$scope.employeeHeadArray = [ {
			"name" : "#",
			"width" : "col-xs-0half",
			"model" : "no",
			"search" : false
		}, {
			"name" : "Code",
			"width" : "w90px",
			"model" : "employeeCode",
			"search" : true,
			"wrap_cell" : true,
			"type" : "text",
			"sort" : true,
			"key" : "searchEmployeeCode"
		}, {
			"name" : "Name",
			"width" : "w150px",
			"model" : "employeeName",
			"search" : true,
			"type" : "text",
			"sort" : true,
			"key" : "searchEmployeeName"
		}, {
			"name" : "Alias Name",
			"width" : "w90px",
			"model" : "aliasName",
			"search" : true,
			"type" : "text",
			"sort" : true,
			"key" : "searchAliasName"
		},{
			"name" : "Employee Status",
			"width" : "w75px",
			"model" : "employementStatus",
			"search" : true,
			"type" : "drop",
			"sort" : true,
			"key" : "searchEmploymentStatus",
			"data" : $scope.tempEmployeeStatus
		},{
			"name" : "Salesman",
			"width" : "w90px",
			"model" : "isSalesman",
			"search" : true,
			"type" : "drop",
			"sort" : true,
			"key" : "searchIsSalesman",
			"data" : $rootScope.enum['YesNo']
		},{
			"name" : "Email",
			"width" : "w120px",
			"model" : "email",
			"search" : true,
			"type" : "text",
			"sort" : true,
			"key" : "searchEmail"
		}, {
			"name" : "Phone",
			"width" : "w90px",
			"model" : "employeePhoneNo",
			"search" : true,
			"type" : "text",
			"sort" : true,
			"key" : "searchPhoneNumber"
		}];
		
		

		$scope.init = function() {
			console.log("Init method is called......");
			$scope.searchDto = {};
			$scope.search();
		}
	    
	    
	    $scope.sortSelection = { sortKey : "employeeCode", sortOrder : "asc" };
	
		$scope.changePage = function(param) {
			$scope.page = param.page;
			$scope.limit = param.size;
			$scope.search();
		}
	
		$scope.limitChange = function(item) {
			$scope.page = 0;
			$scope.limit = item;
			$scope.search();
		}
		
		$scope.sortChange = function(param) {
			$scope.searchDto.orderByType = param.sortOrder.toUpperCase();
			$scope.searchDto.sortByColumn = param.sortKey;
			$scope.search();
		};
	
		$scope.changeSearch = function(param) {
			console.log("Change Search is called ", param);
			for ( var attrname in param) {
				$scope.searchDto[attrname] = param[attrname];
			}
			$scope.page = 0;
			$scope.search();
		}
	
		$scope.rowSelect = function(data, index){
			if($rootScope.roleAccess(roleConstant.MASTER_HRMS_EMPLOYEE_VIEW)){
				console.log("RowSelect ", data, index);
				
				$scope.selectedRecordIndex = index;
			    
		
				/*************************************************************
				 * ux - by Muthu
				 * reason - ipad compatibility fix
				 *
				 * **********************************************************/
		
				var windowInner=$window.innerWidth;
				if(windowInner<=1199){
					$scope.deskTopView = false;
				}
				else{
					$scope.deskTopView = true;
				}
				
				$scope.selectedRowIndex =  ($scope.searchDto.recordPerPage * $scope.searchDto.selectedPageNumber) + index;
				
				var param = {
							employeeId 				: data.id,
				    		selectedPageNumber 		: $scope.selectedRowIndex,
				    		totalRecord 			: $scope.totalRecord
				    };
				    
				$state.go("layout.viewEmployee", $scope.searchDtoToStateParams(param));	
			}
		}
		
		
		 $scope.searchDtoToStateParams = function(param) {
		 if(param == undefined || param == null) {
		    		param = {};
		    	}
		 if($scope.searchDto != undefined && $scope.searchDto != null) {
    		if($scope.searchDto != undefined && $scope.searchDto != null) {
        		param.recordPerPage = 1;
    			if($scope.searchDto.sortByColumn != undefined && $scope.searchDto.sortByColumn != null) {
            		param.sortByColumn = $scope.searchDto.sortByColumn; 
            	}
    			if($scope.searchDto.orderByType != undefined && $scope.searchDto.orderByType != null) {
            		param.orderByType = $scope.searchDto.orderByType; 
            	}
    			if($scope.searchDto.searchEmployeeCode != undefined && $scope.searchDto.searchEmployeeCode != null) {
            		param.searchEmployeeCode = $scope.searchDto.searchEmployeeCode; 
            	}
    			
    			if($scope.searchDto.searchEmployeeName != undefined && $scope.searchDto.searchEmployeeName != null) {
            		param.searchEmployeeName = $scope.searchDto.searchEmployeeName; 
            	}
    			
    			if($scope.searchDto.searchAliasName != undefined && $scope.searchDto.searchAliasName != null) {
            		param.searchAliasName = $scope.searchDto.searchAliasName; 
            	}
    			
    			if($scope.searchDto.searchEmployeeStatus != undefined && $scope.searchDto.searchEmployeeStatus != null) {
            		param.searchEmployeeStatus = $scope.searchDto.searchEmployeeStatus; 
            	}
    			
    			if($scope.searchDto.searchIsSalesman != undefined && $scope.searchDto.searchIsSalesman != null) {
            		param.searchIsSalesman = $scope.searchDto.searchIsSalesman; 
            	}
    			if($scope.searchDto.searchEmail != undefined && $scope.searchDto.searchEmail != null) {
            		param.searchEmail = $scope.searchDto.searchEmail; 
            	}
    			if($scope.searchDto.searchPhoneNumber != undefined && $scope.searchDto.searchPhoneNumber != null) {
            		param.searchPhoneNumber = $scope.searchDto.searchPhoneNumber; 
            	}
    			
    		}
    		
		}
		return param;
		}
		
		 $scope.singlePageNavigation = function(val) { 
			 console.log("singlePageNavigation ", val);
			 
		    	if($stateParams != undefined && $stateParams != null) {
		    		
		            if ((parseInt($stateParams.selectedPageNumber) + val) < 0 || (parseInt($stateParams.selectedPageNumber) + val) >= parseInt($stateParams.totalRecord))
		                return;
		            
		            var stateParameters = {};
		            $scope.searchDto = {};
		            $scope.searchDto.recordPerPage = 1; 
		    		$scope.searchDto.selectedPageNumber = parseInt($stateParams.selectedPageNumber) + val;
		    		
		    		if($stateParams.sortByColumn != undefined && $stateParams.sortByColumn != null) {
						stateParameters.sortByColumn = $scope.searchDto.sortByColumn = $stateParams.sortByColumn; 
		        	}
					if($stateParams.orderByType != undefined && $stateParams.orderByType != null) {
						stateParameters.orderByType = $scope.searchDto.orderByType = $stateParams.orderByType; 
		        	}
					if($stateParams.recordPerPage != undefined && $stateParams.recordPerPage != null) {
						stateParameters.recordPerPage = $scope.searchDto.recordPerPage = $stateParams.recordPerPage; 
		        	}
					if($stateParams.searchEmployeeCode != undefined && $stateParams.searchEmployeeCode != null) {
						stateParameters.searchEmployeeCode = $scope.searchDto.searchEmployeeCode = $stateParams.searchEmployeeCode; 
		        	}
					if($stateParams.searchEmployeeName != undefined && $stateParams.searchEmployeeName != null) {
						stateParameters.searchEmployeeName = $scope.searchDto.searchEmployeeName = $stateParams.searchEmployeeName; 
		        	}
					if($stateParams.searchAliasName != undefined && $stateParams.searchAliasName != null) {
						stateParameters.searchAliasName = $scope.searchDto.searchAliasName = $stateParams.searchAliasName; 
		        	}
					if($stateParams.searchEmployeeStatus != undefined && $stateParams.searchEmployeeStatus != null) {
						stateParameters.searchEmployeeStatus = $scope.searchDto.searchEmployeeStatus = $stateParams.searchEmployeeStatus; 
		        	}
					if($stateParams.searchIsSalesman != undefined && $stateParams.searchIsSalesman != null) {
						stateParameters.searchIsSalesman = $scope.searchDto.searchIsSalesman = $stateParams.searchIsSalesman; 
		        	}
					if($stateParams.searchEmail != undefined && $stateParams.searchEmail != null) {
						stateParameters.searchEmail = $scope.searchDto.searchEmail = $stateParams.searchEmail; 
		        	}
					if($stateParams.searchPhoneNumber != undefined && $stateParams.searchPhoneNumber != null) {
						stateParameters.searchPhoneNumber = $scope.searchDto.searchPhoneNumber = $stateParams.searchPhoneNumber; 
		        	}
					
		    	}  else {
		    		return;
		    	}
		    	EmployeeFactory.search.query($scope.searchDto).$promise.then(function(data, status) {
					$scope.totalRecord = data.responseObject.totalRecord;
					stateParameters.selectedPageNumber = $scope.searchDto.selectedPageNumber;
		            stateParameters.employeeId = data.responseObject.searchResult[0].id;
		            stateParameters.totalRecord = $scope.totalRecord; 
		            $state.go("layout.viewEmployee", stateParameters);
				});
		 }
		
		$scope.search = function() {
			console.log("Search is called.......");

			$scope.searchDto.selectedPageNumber = $scope.page;
			$scope.searchDto.recordPerPage = $scope.limit;
			
			EmployeeFactory.search.query($scope.searchDto).$promise.then(function(data, status) {
						$scope.totalRecord = data.responseObject.totalRecord;
						$scope.employeeDataArray = data.responseObject.searchResult;
						angular.forEach($scope.employeeDataArray,function(item,index){
							$scope.employeeDataArray[index].employementStatus=$filter('EmpStatus')($scope.employeeDataArray[index].employementStatus)
						});
						
						
						console.log("Employee List ", $scope.employeeDataArray);
			});
		}
		
	$scope.clickOnTab = function(tab){
		if(tab=='personal' && $rootScope.roleAccess(roleConstant.MASTER_HRMS_EMPLOYEE_PERSONAL_VIEW)){
			$scope.Tabs=tab;
		}
		if(tab=='professional' && $rootScope.roleAccess(roleConstant.MASTER_HRMS_EMPLOYEE_PROFESSIONAL_VIEW)){
			$scope.Tabs=tab;
		}
		if(tab=='documents' && $rootScope.roleAccess(roleConstant.MASTER_HRMS_EMPLOYEE_DOCUMENTS_VIEW)){
			$scope.Tabs=tab;
		}
	}	
		
		
	$scope.addEmployeeMaster = function(){
		if($rootScope.roleAccess(roleConstant.MASTER_HRMS_EMPLOYEE_CREATE)){
			console.log("Add Employee Master Button is pressed....");
			$state.go("layout.addEmployee");
		}
	};
	
	$scope.editEmployeeMaster = function() {
		if($rootScope.roleAccess(roleConstant.MASTER_HRMS_EMPLOYEE_MODIFY)){
			console.log("Edit Employee Master Button is pressed.....");
			
		 	var param = {
	            employeeId : $scope.employeeMaster.id
	        };
		 	
	        $state.go("layout.editEmployee", param);
		}
	}
	
	$scope.deleteEmployeeMaster = function() {
		if($rootScope.roleAccess(roleConstant.MASTER_HRMS_EMPLOYEE_DELETE)){
			console.log("Delete Employee Master Button is pressed.....");
			var newScope = $scope.$new();
			newScope.errorMessage = $rootScope.nls["ERR251"];
			ngDialog.openConfirm(
					{ template : '<p>{{errorMessage}}</p>'
								+ '<div class="ngdialog-footer">'
								+ '<button type="button" class="btn btn-default btn-property cancel-btn" ng-click="closeThisDialog(0)">Cancel</button>'
								+ '<button type="button" class="btn btn-primary btn-property accent-btn" ng-click="confirm(1)">Yes</button></div>',
						plain : true,
						scope: newScope,
						className : 'ngdialog-theme-default'
					}).then(function(value) {
						EmployeeFactory.remove.query( { id : $scope.employeeMaster.id }, function(data) {

							if (data.responseCode == 'ERR0') {
								console.log("EmployeeMaster Deleted Successfully");
								Notification.success($rootScope.nls["ERR402"]);
								$state.go("layout.employee");
							} else {
								console.log("EmployeeMaster deletion Failed ", data.responseDescription)
							}
						}, function(error) {
								console.log("PartyGroupMaster deletion Failed ", data.responseDescription)
						});
					}, function(value) {
						console.log("Deletion is Cancelled");
					});
		}
	}
	
	$scope.backToListPage = function() {
		console.log("Cancel Button is pressed.....");
		
		$scope.showHistory = false;
		
		$state.go("layout.employee");
		
		/************************************************************
		 * ux - by Muthu
		 * reason - ipad compatibility fix
		 *
		 * *********************************************************/
	    	$scope.deskTopView = true;

		/********************* ux fix ends here *****************************/
	}
	
	$scope.attachConfig = {
			"isEdit":false,  
			"isDelete":false, 
			"columnDefs":[
				{"name":"Description", "model":"refNo","type":"text",width:"inl_l"},
				{"name":"File Name", "model":"fileName","type":"file"}
			]
	}

		
	$scope.downloadAttach = function(param){
		if(param.data.id!=null && param.data.file==null){
			console.log("API CALL")	
			downloadFactory.downloadAttachment('/api/v1/employeemaster/files/', param.data.id, param.data.fileName);
		}else if(param.data.id==null && param.data.file!=null){
			console.log("NO API CALL BUT Byte array found")	
			var blob = new Blob([param.data.file], {type: param.data.fileContentType+";charset=utf-8"});
			saveAs(blob, param.data.fileName);
		}else{
			console.log("NO API CALL")	
			saveAs(param.data.tmpFile, param.data.fileName);
		}
	}
	
	$scope.getEmployeeById = function(employeeId) {
		
		console.log("Get Employee By ID " , employeeId);
		
		EmployeeFactory.get.query({id : employeeId}).$promise.then(function(data) {
			if(data.responseCode == 'ERR0') {
				$scope.employeeMaster = data.responseObject;
				console.log("Employee Master " , $scope.employeeMaster);
			} else {
				console.log(data.responseDescription);
			}
		}, function(error) {
			console.log(error);
		})
	}
	

	console.log($stateParams);
	 switch ($stateParams.action) {
	    case "SEARCH":
	        console.log("Employee Search Page");
	        $scope.init();
	        $rootScope.breadcrumbArr = 	[ 
	                                   	  { label:"Master", state:"layout.employee"}, 
	                                   	  { label:"Employee",  state:"layout.employee"}
	        							];
	        break;
	        
	    case "VIEW":
	    	console.log("Employee View Page", $stateParams);
	    	
    	    if($stateParams.selectedPageNumber != undefined && $stateParams.selectedPageNumber != null) {
				$scope.selectedRowIndex = parseInt($stateParams.selectedPageNumber);
			}
			
    	    if($stateParams.totalRecord != undefined && $stateParams.totalRecord != null) {
				$scope.totalRecord = parseInt($stateParams.totalRecord);
			}

	    	$scope.getEmployeeById($stateParams.employeeId);
	    	
	    	$rootScope.breadcrumbArr = 	[ 
	                                   	  { label:"Master", state:"layout.employee"}, 
	                                   	  { label:"Employee",  state:"layout.employee"},
	                                 	  { label:"View Employee",  state: null}
	                                   	  
	        							];
	        break;
	}			
}]);
