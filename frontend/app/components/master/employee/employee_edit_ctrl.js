app.controller('EmployeeEditController',['$rootScope', '$scope', '$state', '$stateParams', '$timeout', 'ngProgressFactory', 'Notification', 'ngDialog', 'appConstant', 'CommonValidationService','EmployeeFactory','CompanyWiseLocation','CountryList','DepartmentFactory', 'DesignationFactory','EmployeeList','cloneService','roleConstant','AutoCompleteService','ValidateUtil','downloadFactory','$filter', 
                                         function($rootScope, $scope, $state, $stateParams, $timeout, ngProgressFactory, Notification, ngDialog, appConstant,CommonValidationService,EmployeeFactory, CompanyWiseLocation, CountryList, DepartmentFactory, DesignationFactory, EmployeeList,cloneService,roleConstant,AutoCompleteService,ValidateUtil,downloadFactory,$filter) {
	
	$scope.$AutoCompleteService = AutoCompleteService;
	$scope.$roleConstant =roleConstant;
	$scope.contained_progressbar = ngProgressFactory.createInstance();
	$scope.contained_progressbar.setParent(document.getElementById('employee-panel'));
	$scope.contained_progressbar.setAbsolute();
	
	$scope.init = function () {
		
		console.log("Init is called.............................................");
		
		if($scope.employeeMaster == undefined || $scope.employeeMaster == null) {
			$scope.employeeMaster = {};
			$scope.employeeMaster.employeePersonalDetail = {};
			$scope.employeeMaster.employeeProfessionalDetail = {};
			$scope.employeeMaster.employeePersonalDetail.gender = false;
			$scope.employeeMaster.attachmentList = [{}];
			$scope.uploadText = true;
			$scope.isBusy = false;
		} else {
			$scope.uploadText=false;
			$scope.isBusy = false;
		}
		
		$scope.clickOnTab('personal');
		$scope.setOldDataVal();	
	};

    $scope.setOldDataVal = function(){
        $scope.oldData = JSON.stringify($scope.employeeMaster);
    }
    
    
    var tempData = $rootScope.enum['EmploymentStatus'];
	$scope.mappingUnitData = [];
	for(var i = 0; i < tempData.length; i++) {
		$scope.mappingUnitData.push($filter('EmpStatus')(tempData[i]));
	}
	
	
    $scope.clickOnTab = function(tab,fieldId){
    	
		if($scope.employeeMaster!=undefined&&$scope.employeeMaster.id!=null){
			
			if(tab=='personal' && $rootScope.roleAccess(roleConstant.MASTER_HRMS_EMPLOYEE_PERSONAL_MODIFY)){
				$scope.Tabs=tab;
			}
			if(tab=='professional' && $rootScope.roleAccess(roleConstant.MASTER_HRMS_EMPLOYEE_PROFESSIONAL_MODIFY)){
				$scope.Tabs=tab;
			}
			if(tab=='documents'){
				$scope.Tabs=tab;
			}	
		}else{
			if(tab=='personal' && $rootScope.roleAccess(roleConstant.MASTER_HRMS_EMPLOYEE_PERSONAL_CREATE)){
				$scope.Tabs=tab;
			}
			if(tab=='professional' && $rootScope.roleAccess(roleConstant.MASTER_HRMS_EMPLOYEE_PROFESSIONAL_CREATE)){
				$scope.Tabs=tab;
			}
			if(tab=='documents'){
				$scope.Tabs=tab;
				$rootScope.navigateToNextField(fieldId);
			}
		}
		$rootScope.navigateToNextField(fieldId);
	}	
		
    
    $scope.cancel = function() {

		 if ($scope.oldData != JSON.stringify($scope.employeeMaster)) {
				var newScope = $scope.$new();
				newScope.errorMessage = $rootScope.nls["ERR200"];
				ngDialog.openConfirm({
					template: '<p>{{errorMessage}}</p>' +
					'<div class="ngdialog-footer">' +
						' <button type="button" class="btn btn-default btn-property cancel-btn" ng-click="confirm(2)">No</button>' +
						'<button type="button" class="btn btn-default btn-property accent-btn " ng-click="confirm(1)">Yes</button>' +
						'<button type="button" class="btn btn-default btn-property pull-left grey-cancel-btn" ng-click="confirm(3)">Cancel</button>' +
					'</div>',
					plain: true,
					scope: newScope,
					className: 'ngdialog-theme-default'
				}).then(function(value) {
					if (value == 1) {
						$scope.saveOrUpdateEmployee();
					} else if (value == 2) {
						$state.go("layout.employee", { submitAction : 'Cancelled'});
					} else {
						console.log("cancelled");
					}
				});
			} else {
				$state.go("layout.employee",{ submitAction : 'Cancelled'});
			}
		}
    
    $scope.selectDateOfJoining = function(nextField){
    	if(nextField!=undefined)
   		 $scope. navigateToNextField(nextField); 	
    	if($scope.employeeMaster.employeeProfessionalDetail.dateOfJoining !=undefined && ($scope.employeeMaster.employeeProfessionalDetail.designationStartDate==undefined || $scope.employeeMaster.employeeProfessionalDetail.designationStartDate==null || $scope.employeeMaster.employeeProfessionalDetail.designationStartDate=="")){
    		$scope.employeeMaster.employeeProfessionalDetail.designationStartDate = $scope.employeeMaster.employeeProfessionalDetail.dateOfJoining;
    	}
    	
    }
   
    
    $scope.createDefaultValue = function(){
    	if($scope.employeeMaster.id==undefined && ($scope.employeeMaster.firstName != undefined && $scope.employeeMaster.firstName!=null && $scope.employeeMaster.firstName!="")){
    		
    		if($scope.employeeMaster.employementStatus==undefined || $scope.employeeMaster.employementStatus ==null || $scope.employeeMaster.employementStatus == ""){
        		$scope.employeeMaster.employementStatus = "Employed";	
        	}	
    		if($scope.employeeMaster.employeeProfessionalDetail.probationStatus==undefined || $scope.employeeMaster.employeeProfessionalDetail.probationStatus ==null || $scope.employeeMaster.employeeProfessionalDetail.probationStatus == ""){
        		$scope.employeeMaster.employeeProfessionalDetail.probationStatus="Probation";	
        	}
    		if($scope.employeeMaster.employeeProfessionalDetail.probationPeriod==undefined || $scope.employeeMaster.employeeProfessionalDetail.probationPeriod ==null || $scope.employeeMaster.employeeProfessionalDetail.probationPeriod == ""){
        		$scope.employeeMaster.employeeProfessionalDetail.probationPeriod=180;
        	}    		
    	}
    }
    
    $scope.createAliasName =function(){
    	
    if($scope.employeeMaster.aliasName == null  ||  $scope.employeeMaster.aliasName == undefined  || $scope.employeeMaster.aliasName.trim().length==0){
    	
    	if($scope.employeeMaster.firstName != undefined && $scope.employeeMaster.firstName!=null && $scope.employeeMaster.firstName.trim().length!=0  &&
    	 $scope.employeeMaster.lastName != undefined && $scope.employeeMaster.lastName!=null && $scope.employeeMaster.lastName.trim().length!=0){
    					var aliasName = $scope.employeeMaster.firstName +' '+$scope.employeeMaster.lastName;
    					$scope.employeeMaster.aliasName =aliasName;
    		
    	}
    	
    }	
    	
    }
    
    $scope.uploadImage = function(file){
		if(file != null || file != undefined) {
			var fileName = file.name;
			var extention = fileName.split('.')[fileName.split('.').length - 1].toLowerCase();
			console.log("file extention ", extention);
			var valid = false;
			if(extention == 'jpeg' || extention == 'jpg' || extention == 'png' || extention == 'gif'){
				valid = true;
			}
			
			var reader = new FileReader();
			
			reader.onload = function(event){
				var contents = event.target.result;
				var uploadedFile = btoa(contents);
				console.log("File contents : " , uploadedFile);
				$scope.employeeMaster.image = uploadedFile;
				$scope.uploadText=false;
				$scope.isBusy = true;
				$timeout(function(){
					$scope.isBusy = false;
				},1000);

			};
			reader.readAsBinaryString(file);
		}
	}
	
    
    $scope.associateMapping = function (){
		 $scope.logoMaster.logo=$scope.logoMaster.encodedImage;
		 $scope.logoMaster.encodedLogo=null;
	}

	$scope.revertAssociateMapping = function (){
			 $scope.logoMaster.encodedLogo=$scope.logoMaster.logo;
			 $scope.logoMaster.logo=null;
	}

	$scope.copyAsPermanentAddrees = function(type){
		if(type =='temporaryAddress'){
			if($scope.checkTemp){
				$scope.employeeMaster.employeePersonalDetail.temporaryAddress = cloneService.clone($scope.employeeMaster.employeePersonalDetail.permanentAddress);
			}
			else{
				$scope.employeeMaster.employeePersonalDetail.temporaryAddress = null;
			}
		}else{
			if($scope.checkEmergency){
				$scope.employeeMaster.employeePersonalDetail.emergencyAddress= cloneService.clone($scope.employeeMaster.employeePersonalDetail.permanentAddress);
			}
			else{
				$scope.employeeMaster.employeePersonalDetail.emergencyAddress = null;
			}
		}
		
	}
	
	$scope.emailValidEmployeeMaster = function(){
		if($scope.employeeMaster.email==""){
			$scope.errorMap = new Map();
			return;
		}
		if($scope.employeeMaster.email!=undefined||$scope.employeeMaster.email!=null){
			$scope.validateEmployeeMaster(8);
		}
		
	}
	
	$scope.validateEmployeeMaster = function(validateCode){
		$scope.errorMap = new Map();
		
		if($scope.employeeMaster!= undefined && $scope.employeeMaster != null){
			
			//validation-employee code
			/*if(validateCode==0 || validateCode==1){
						if($scope.employeeMaster.employeeCode == undefined || $scope.employeeMaster.employeeCode == null || $scope.employeeMaster.employeeCode == ""){
							 $scope.errorMap.put("employeeCode",$rootScope.nls["ERR3202"]);
							  return false;
						}
			 }*/
			
			//removed for bug
			//validation-name
			/*if(validateCode==0 || validateCode==2){
				if($scope.employeeMaster.firstName == undefined || $scope.employeeMaster.firstName == null || $scope.employeeMaster.firstName == ""){
					 $scope.errorMap.put("firstName",$rootScope.nls["ERR3204"]);
					  return false;
				}
	         }*/
			
			/*if(validateCode==0 || validateCode==3){
				if($scope.employeeMaster.middleName == undefined || $scope.employeeMaster.middleName == null || $scope.employeeMaster.middleName == ""){
					 $scope.errorMap.put("middleName",$rootScope.nls["ERR3205"]);
					  return false;
				}
	         }*/
			
			/*if(validateCode==0 || validateCode==4){
				if($scope.employeeMaster.lastName == undefined || $scope.employeeMaster.lastName == null || $scope.employeeMaster.lastName == ""){
					 $scope.errorMap.put("lastName",$rootScope.nls["ERR3206"]);
					  return false;
				}
	         }*/
			
			
			if(validateCode==0 || validateCode==5){
				if($scope.employeeMaster.aliasName == undefined || $scope.employeeMaster.aliasName == null || $scope.employeeMaster.aliasName == ""){
					 $scope.errorMap.put("aliasName",$rootScope.nls["ERR3227"]);
					  return false;
				}
	         }
			if(validateCode==0 || validateCode==6){
				if($scope.employeeMaster.employementStatus == undefined || $scope.employeeMaster.employementStatus == null || $scope.employeeMaster.employementStatus == ""){
					 $scope.errorMap.put("employementStatus",$rootScope.nls["ERR3210"]);
					  return false;
				}else{
					if(($scope.employeeMaster.employementStatus=="Resigned" || $scope.employeeMaster.employementStatus=="Terminated") 
							&& ($scope.employeeMaster.employeeProfessionalDetail.employmentEndDate==undefined ||$scope.employeeMaster.employeeProfessionalDetail.employmentEndDate==null || $scope.employeeMaster.employeeProfessionalDetail.employmentEndDate=="" )){
						$scope.Tabs='professional';
						$scope.errorMap.put("employmentEndDate",$rootScope.nls["ERR3237"]);
						  return false;
					}
					
					if(($scope.employeeMaster.employementStatus=="Transferred") 
							&& ($scope.employeeMaster.employeeProfessionalDetail.transferDate==undefined ||$scope.employeeMaster.employeeProfessionalDetail.transferDate==null || $scope.employeeMaster.employeeProfessionalDetail.transferDate=="" )){
						$scope.Tabs='professional';
						$scope.errorMap.put("transferDate",$rootScope.nls["ERR3238"]);
						  return false;
					}
				}
	         }
			//validate-email
			if(validateCode==0 || validateCode==8){
				
				if($scope.employeeMaster.email == undefined || $scope.employeeMaster.email  == null || $scope.employeeMaster.email ==""){
					$scope.errorMap.put("email",$rootScope.nls["ERR3229"]);
					$scope.navigateToNextField('phone');
					return false;
				}
				else{
					if(!CommonValidationService.checkMultipleMail($scope.employeeMaster.email)){
	            		$scope.errorMap.put("email",$rootScope.nls["ERR3211"]);
	            		$scope.navigateToNextField('email');
	            		return false;
				   }
					
				}
	         }
			//validate-phone number
			if(validateCode==0 || validateCode==9){
				if($scope.employeeMaster.employeePhoneNo != undefined && $scope.employeeMaster.employeePhoneNo != null && $scope.employeeMaster.employeePhoneNo != ""){
					if(!CommonValidationService.checkRegExp(appConstant.REG_EXP_PHONE_NUMBER,$scope.employeeMaster.employeePhoneNo)){
	                     $scope.errorMap.put("employeePhoneNo",$rootScope.nls["ERR3212"]);
	                     return false;
					}
				}
	         }
			
			//validate-cc-email
			if(validateCode==0 || validateCode==10){
				if($scope.employeeMaster.cc_email != undefined && $scope.employeeMaster.cc_email != null && $scope.employeeMaster.cc_email != ""){
					if(!CommonValidationService.checkMultipleMail($scope.employeeMaster.cc_email)){
	            		$scope.errorMap.put("cc_email",$rootScope.nls["ERR3211"]);
	            		return false;
				   }
					
				}
	         }
			//validate-DOB
			if(validateCode==0 || validateCode==11){
				if($scope.employeeMaster.employeePersonalDetail.dateOfBirth == undefined || $scope.employeeMaster.employeePersonalDetail.dateOfBirth == null || $scope.employeeMaster.employeePersonalDetail.dateOfBirth == ""){
					$scope.Tabs='personal';
					$scope.errorMap.put("dateOfBirth",$rootScope.nls["ERR3213"]);
					  return false;
				}
	         }
			
			//validate-nationality
			if(validateCode==0 || validateCode==12){
					if($scope.employeeMaster.countryMaster == undefined || $scope.employeeMaster.countryMaster == null || $scope.employeeMaster.countryMaster == ""){
						$scope.Tabs='personal';
						$scope.errorMap.put("countryName",$rootScope.nls["ERR3215"]);
						  return false;
					}else{
						if($scope.employeeMaster.countryMaster.status=='Block'){
							$scope.errorMap.put("countryName",$rootScope.nls["ERR3230"]);
							  return false;
						}else if($scope.employeeMaster.countryMaster.status=='Hide'){
							$scope.errorMap.put("countryName",$rootScope.nls["ERR3230"]);
							  return false;
						}
					}
	         }
			
			if(validateCode==0 || validateCode==13){
				if($scope.employeeMaster.employeePersonalDetail != undefined && $scope.employeeMaster.employeePersonalDetail != null){
					if($scope.employeeMaster.employeePersonalDetail.phoneNumber != undefined && $scope.employeeMaster.employeePersonalDetail.phoneNumber != null && $scope.employeeMaster.employeePersonalDetail.phoneNumber != ""){
						if(!CommonValidationService.checkRegExp(appConstant.REG_EXP_PHONE_NUMBER,$scope.employeeMaster.employeePersonalDetail.phoneNumber)){
		                     $scope.errorMap.put("officialPhoneNo",$rootScope.nls["ERR3212"]);
		                     return false;
						}
					}
				}
	         }
			
			if(validateCode==0 || validateCode==14){
				if($scope.employeeMaster.employeePersonalDetail != undefined && $scope.employeeMaster.employeePersonalDetail != null){
					if($scope.employeeMaster.employeePersonalDetail.personalEmail != undefined && $scope.employeeMaster.employeePersonalDetail.personalEmail != null && $scope.employeeMaster.employeePersonalDetail.personalEmail != ""){
						if(!CommonValidationService.checkMultipleMail($scope.employeeMaster.employeePersonalDetail.personalEmail)){
		            		$scope.errorMap.put("personalEmail",$rootScope.nls["ERR3211"]);
		            		 $rootScope.navigateToNextField('personalEmail')
		            		return false;
					   }
						
					}
				}
	         }
			
			if(validateCode==0 || validateCode==15){
				if($scope.employeeMaster.employeePersonalDetail != undefined && $scope.employeeMaster.employeePersonalDetail != null){
					if($scope.employeeMaster.employeePersonalDetail.mobileNumber != undefined && $scope.employeeMaster.employeePersonalDetail.mobileNumber != null && $scope.employeeMaster.employeePersonalDetail.mobileNumber != ""){
						if(!CommonValidationService.checkRegExp(appConstant.REG_EXP_MOBILE_NUMBER,$scope.employeeMaster.employeePersonalDetail.mobileNumber)){
		                     $scope.errorMap.put("mobileNumber",$rootScope.nls["ERR3212"]);
		                     return false;
						}
					}
				}
	         }
			
			if(validateCode==0 || validateCode==16){
				if($scope.employeeMaster.employeePersonalDetail != undefined && $scope.employeeMaster.employeePersonalDetail != null){
					if($scope.employeeMaster.employeePersonalDetail.permanentPhoneNumber != undefined && $scope.employeeMaster.employeePersonalDetail.permanentPhoneNumber != null && $scope.employeeMaster.employeePersonalDetail.permanentPhoneNumber != ""){
						if(!CommonValidationService.checkRegExp(appConstant.REG_EXP_PHONE_NUMBER,$scope.employeeMaster.employeePersonalDetail.permanentPhoneNumber)){
		                     $scope.errorMap.put("permanentPhoneNumber",$rootScope.nls["ERR3212"]);
		                     return false;
						}
					}
				}
	         }
			
			if(validateCode==0 || validateCode==17){
				if($scope.employeeMaster.employeePersonalDetail != undefined && $scope.employeeMaster.employeePersonalDetail != null){
					if($scope.employeeMaster.employeePersonalDetail.temporaryPhoneNumber != undefined && $scope.employeeMaster.employeePersonalDetail.temporaryPhoneNumber != null && $scope.employeeMaster.employeePersonalDetail.temporaryPhoneNumber != ""){
						if(!CommonValidationService.checkRegExp(appConstant.REG_EXP_PHONE_NUMBER,$scope.employeeMaster.employeePersonalDetail.temporaryPhoneNumber)){
		                     $scope.errorMap.put("temporaryPhoneNumber",$rootScope.nls["ERR3212"]);
		                     return false;
						}
					}
				}
	         }
			
			if(validateCode==0 || validateCode==18){
				if($scope.employeeMaster.employeePersonalDetail != undefined && $scope.employeeMaster.employeePersonalDetail != null){
					if($scope.employeeMaster.employeePersonalDetail.emergencyPhoneNumber != undefined && $scope.employeeMaster.employeePersonalDetail.emergencyPhoneNumber != null && $scope.employeeMaster.employeePersonalDetail.emergencyPhoneNumber != ""){
						if(!CommonValidationService.checkRegExp(appConstant.REG_EXP_PHONE_NUMBER,$scope.employeeMaster.employeePersonalDetail.emergencyPhoneNumber)){
		                     $scope.errorMap.put("emergencyPhoneNumber",$rootScope.nls["ERR3212"]);
		                     return false;
						}
					}
				}
	         }
			
			
			
			if(validateCode==0 || validateCode==19){
				
					if($scope.employeeMaster.locationMaster==undefined || $scope.employeeMaster.locationMaster.locationName ==undefined 
							||  $scope.employeeMaster.locationMaster.locationName == null ||  $scope.employeeMaster.locationMaster.locationName == ""){
						$scope.Tabs='professional';
						$scope.errorMap.put("locationName", $rootScope.nls["ERR3233"]);
						  return false;
					}else{

						if (ValidateUtil.isStatusBlocked($scope.employeeMaster.locationMaster.status)) {
							$scope.employeeMaster.locationMaster = null;
							$scope.errorMap.put("locationName",$rootScope.nls["ERR3217"]);
		                     return false;
		                }
		                if (ValidateUtil.isStatusHidden($scope.employeeMaster.locationMaster.status)) {
		                	$scope.employeeMaster.locationMaster = null;
		                	$scope.errorMap.put("locationName",$rootScope.nls["ERR3218"]);
		                     return false;
		                }
					}
					
	         }
			
			
			if(validateCode==0 || validateCode==20){
				if($scope.employeeMaster.employeeProfessionalDetail != undefined && $scope.employeeMaster.employeeProfessionalDetail != null){
					if($scope.employeeMaster.employeeProfessionalDetail.departmentMaster!= undefined && $scope.employeeMaster.employeeProfessionalDetail.departmentMaster != null && $scope.employeeMaster.employeeProfessionalDetail.departmentMaster != ""){
						if (ValidateUtil.isStatusBlocked($scope.employeeMaster.employeeProfessionalDetail.departmentMaster.status)) {
							$scope.employeeMaster.employeeProfessionalDetail.departmentMaster = null;
							$scope.errorMap.put("departmentName",$rootScope.nls["ERR3219"]);
		                     return false;
		                }
		                if (ValidateUtil.isStatusHidden($scope.employeeMaster.employeeProfessionalDetail.departmentMaster.status)) {
		                	$scope.employeeMaster.employeeProfessionalDetail.departmentMaster = null;
		                	$scope.errorMap.put("departmentName",$rootScope.nls["ERR3220"]);
		                     return false;
		                }
						
					}else{
						if($scope.employeeMaster.employeeProfessionalDetail.departmentMaster==undefined || $scope.employeeMaster.employeeProfessionalDetail.departmentMaster.departmentName  ==undefined 
								||  $scope.employeeMaster.employeeProfessionalDetail.departmentMaster.departmentName  == null ||  $scope.employeeMaster.employeeProfessionalDetail.departmentMaster.departmentName  == ""){
							$scope.Tabs='professional';
							$scope.errorMap.put("departmentName",$rootScope.nls["ERR3234"]);
							  return false;
						}
					}
				}
	         }
		
			
			
			if(validateCode==0 || validateCode==21){
				if($scope.employeeMaster.employeeProfessionalDetail != undefined && $scope.employeeMaster.employeeProfessionalDetail != null){
					if($scope.employeeMaster.employeeProfessionalDetail.reportingTo!= undefined && $scope.employeeMaster.employeeProfessionalDetail.reportingTo != null && $scope.employeeMaster.employeeProfessionalDetail.reportingTo != ""){
						if (ValidateUtil.isEmployeeResigned($scope.employeeMaster.employeeProfessionalDetail.reportingTo.employementStatus)) {
							$scope.employeeMaster.employeeProfessionalDetail.reportingTo = null;
							$scope.errorMap.put("employeeName",$rootScope.nls["ERR3221"]);
		                     return false;
		                }
		                if (ValidateUtil.isEmployeeTerminated($scope.employeeMaster.employeeProfessionalDetail.reportingTo.employementStatus)) {
		                	$scope.employeeMaster.employeeProfessionalDetail.reportingTo = null;
		                	$scope.errorMap.put("employeeName",$rootScope.nls["ERR3222"]);
		                     return false;
		                }
						
					}
				}
	         }
			
			
			if(validateCode==0 || validateCode==22){
				if($scope.employeeMaster.employeeProfessionalDetail != undefined && $scope.employeeMaster.employeeProfessionalDetail != null){
					if($scope.employeeMaster.employeeProfessionalDetail.departmentHead!= undefined && $scope.employeeMaster.employeeProfessionalDetail.departmentHead != null && $scope.employeeMaster.employeeProfessionalDetail.departmentHead != ""){
						if(ValidateUtil.isEmployeeResigned($scope.employeeMaster.employeeProfessionalDetail.departmentHead.employementStatus)){
							$scope.employeeMaster.employeeProfessionalDetail.departmentHead = null;
							$scope.errorMap.put("employeeName",$rootScope.nls["ERR3223"]);
		                     return false;
		                }
						if(ValidateUtil.isEmployeeTerminated($scope.employeeMaster.employeeProfessionalDetail.departmentHead.employementStatus)){
		                	$scope.employeeMaster.employeeProfessionalDetail.departmentHead = null;
		                	$scope.errorMap.put("employeeName",$rootScope.nls["ERR3224"]);
		                     return false;
		                }
						
					}
				}
	         }
			
			
			if( validateCode==0 || validateCode==23) {
				if($scope.employeeMaster.employeeProfessionalDetail!=undefined && $scope.employeeMaster.employeeProfessionalDetail!=null){
					if($scope.employeeMaster.employeeProfessionalDetail.probationPeriod!=undefined && $scope.employeeMaster.employeeProfessionalDetail.probationPeriod!=null){
						if(!CommonValidationService.checkRegExp(appConstant.REG_EXP_NO_OF_PIECES,$scope.employeeMaster.employeeProfessionalDetail.probationPeriod)){
							$scope.errorMap.put("probationPeriod",$rootScope.nls["ERR3232"]);
							return false;
						}
					}
					
				}
			}
			if(validateCode==0 || validateCode==24){
				if($scope.employeeMaster.employeeProfessionalDetail.designationMaster == undefined || $scope.employeeMaster.employeeProfessionalDetail.designationMaster.designationName ==undefined 
						||  $scope.employeeMaster.employeeProfessionalDetail.designationMaster.designationName == null ||  $scope.employeeMaster.employeeProfessionalDetail.designationMaster.designationName == ""){
					$scope.Tabs='professional';
					$scope.errorMap.put("designation",$rootScope.nls["ERR3235"]);
					  return false;
				}else{
				    if($scope.employeeMaster.employeeProfessionalDetail.designationMaster.status=='Block'){
				    	$scope.errorMap.put("designation",$rootScope.nls["ERR3239"]);
						  return false;
				    }else if($scope.employeeMaster.employeeProfessionalDetail.designationMaster.status=='Hide'){
				    	$scope.errorMap.put("designation",$rootScope.nls["ERR3240"]);
						  return false;
				    }
				}
			}
			
			
			if(validateCode==0 || validateCode==25){
				var alphanum = new RegExp("^[a-zA-Z0-9]*$");
	            if (!alphanum.test($scope.employeeMaster.employeeProfessionalDetail.payrollEmployeeId)) {
	                $scope.errorMap.put("payrollEmployeeId",$rootScope.nls["ERR3236"]);
	                return false;
	            }
			}
			
			if(validateCode==0 || validateCode==26){
				
				if($scope.employeeMaster.employeeProfessionalDetail.designationStartDate !=undefined 
						&& $scope.employeeMaster.employeeProfessionalDetail.dateOfJoining != undefined)
				{
					var doj = $scope.employeeMaster.employeeProfessionalDetail.dateOfJoining;
					var dsd = $scope.employeeMaster.employeeProfessionalDetail.designationStartDate;
					if(doj>dsd){
						$scope.errorMap.put("dateOfJoining",$rootScope.nls["ERR3241"]);
		                return false;	
					}
					
					
				}
				
				
			}
		
		}
	return true;	
		
	}
	
    $scope.convertDateSend = function(employeeMasterCopy) {

		if(employeeMasterCopy.employeePersonalDetail.dateOfBirth != null) {
			employeeMasterCopy.employeePersonalDetail.dateOfBirth = $rootScope.sendApiStartDateTime(employeeMasterCopy.employeePersonalDetail.dateOfBirth);
		}
		
		if(employeeMasterCopy.employeePersonalDetail.weddingAnniversaryDate != null) {
			employeeMasterCopy.employeePersonalDetail.weddingAnniversaryDate = $rootScope.sendApiStartDateTime(employeeMasterCopy.employeePersonalDetail.weddingAnniversaryDate);
		}
		
		if(employeeMasterCopy.employeeProfessionalDetail.dateOfJoining != null) {
			employeeMasterCopy.employeeProfessionalDetail.dateOfJoining = $rootScope.sendApiStartDateTime(employeeMasterCopy.employeeProfessionalDetail.dateOfJoining);
		}
		
		if(employeeMasterCopy.employeeProfessionalDetail.designationStartDate != null) {
			employeeMasterCopy.employeeProfessionalDetail.designationStartDate = $rootScope.sendApiStartDateTime(employeeMasterCopy.employeeProfessionalDetail.designationStartDate);
		}
		
		if(employeeMasterCopy.employeeProfessionalDetail.transferDate!=null){
			employeeMasterCopy.employeeProfessionalDetail.transferDate = $rootScope.sendApiStartDateTime(employeeMasterCopy.employeeProfessionalDetail.transferDate);
		}
		if(employeeMasterCopy.employeeProfessionalDetail.employmentEndDate!=null){
			employeeMasterCopy.employeeProfessionalDetail.employmentEndDate = $rootScope.sendApiStartDateTime(employeeMasterCopy.employeeProfessionalDetail.employmentEndDate);
		}
		
		employeeMasterCopy.employementStatus=employeeMasterCopy.employementStatus.toUpperCase();
    }
    
    $scope.saveOrUpdateEmployee = function() {
    	console.log("Save Method is called.");
		
    	if($scope.validateEmployeeMaster(0)){
			
			employeeMasterCopy = JSON.parse(JSON.stringify($scope.employeeMaster));
			
			$scope.convertDateSend(employeeMasterCopy);
			
	    	console.log("employeeMasterCopy  ", employeeMasterCopy);
			
	    	
	    	if(employeeMasterCopy.id == null) {
	    		var successMessage = $rootScope.nls["ERR400"];	
	    	} else {
	    		var successMessage = $rootScope.nls["ERR401"];	
	    	}
	    	
			
			EmployeeFactory.save.query(employeeMasterCopy).$promise.then(function(data) {
			
				if (data.responseCode == 'ERR0') {
					var params = {}
					params.submitAction = 'Saved';
					Notification.success(successMessage);
					$state.go("layout.employee",params);
				} else {
					console.log("Employee Save Failed " , data.responseDescription)
					//$scope.revertAssociateMapping
				}
			
			angular.element(".panel-body").animate({scrollTop: 0}, "slow");
		
			}, function(error) {
			
				//$scope.revertAssociateMapping
			
				console.log("Employee added Failed : " , error)
		
			});
		}else{
			console.log("Employee validation  Failed..");
		}
    }
	
	

	/* Code For Populating Lov */
	
    $scope.ajaxLocationEvent = function(object) {
     
    	$scope.searchDto = {};
    	$scope.searchDto.keyword = object == null ? "" : object;
    	$scope.searchDto.selectedPageNumber = 0;
        $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

        return CompanyWiseLocation.fetch({"companyId": $rootScope.userProfile.selectedUserLocation.companyMaster.id}, $scope.searchDto)
        .$promise.then(function(data) {
        	if (data.responseCode == "ERR0") {
        		return data.responseObject.searchResult;
        	}
        }, function(errResponse) {
        	console.error('Error while fetching  Locations', errResponse);
        });
    }

    
    $scope.selectedLocation = function(fieldId) {
    	$rootScope.navigateToNextField(fieldId);
    }

    
    $scope.ajaxCountryEvent = function (object){
		
		$scope.searchDto={};
		$scope.searchDto.keyword=object==null?"":object;
		$scope.searchDto.selectedPageNumber = 0;
		$scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
		
		return CountryList.fetch($scope.searchDto).$promise.then(function(data) {
			if (data.responseCode == "ERR0") {
				$scope.contriesList = data.responseObject.searchResult;
				return $scope.contriesList ;
			}
		}, function(errResponse) {
			console.error('Error while fetching Countries', errResponse);
		});

	}

	$scope.selectedCountry = function(fieldId,code) {
		if($scope.validateEmployeeMaster(code)){
			$rootScope.navigateToNextField(fieldId);
		}
		else{
			
		}
	};
    
	
	 $scope.ajaxDepartmentEvent = function (object){
			$scope.searchDto={};
			$scope.searchDto.keyword=object==null?"":object;
			$scope.searchDto.selectedPageNumber = 0;
			$scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
			
			return DepartmentFactory.getAll.query($scope.searchDto).$promise.then(function(data) {
				if (data.responseCode == "ERR0") {
					return data.responseObject.searchResult;
				}
			}, function(errResponse) {
				console.error('Error while fetching Departments ', errResponse);
			});

	}

	$scope.selectedDepartment = function(fieldId) {
		$rootScope.navigateToNextField(fieldId);
	};

	
	
	$scope.ajaxDesignationEvent = function (object){
			$scope.searchDto={};
			$scope.searchDto.keyword=object==null?"":object;
			$scope.searchDto.selectedPageNumber = 0;
			$scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
			
			return DesignationFactory.getAll.query($scope.searchDto).$promise.then(function(data) {
				if (data.responseCode == "ERR0") {
					return data.responseObject.searchResult;
				}
			}, function(errResponse) {
				console.error('Error while fetching Designation', errResponse);
			});

	}

	$scope.selectedDesignation = function(fieldId) {
		$rootScope.navigateToNextField(fieldId);
	};
	   
	$scope.selectedGradeMaster= function(fieldId){
		if(fieldId!=undefined){
    		$rootScope.navigateToNextField(fieldId);
    	}
	}
    
	
	$scope.ajaxEmployeeEvent = function(object) {
		
		console.log("ajaxEmployeeEvent ", object);
		
		$scope.searchDto={};
		$scope.searchDto.keyword=object==null?"":object;
		$scope.searchDto.selectedPageNumber = 0;
		$scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
		
		
		return EmployeeList.fetch($scope.searchDto).$promise.then(function(data) {
			if (data.responseCode == "ERR0") {
				return data.responseObject.searchResult;
			}
		}, function(errResponse) {
			console.error('Error while fetching Employees', errResponse);
		});
	}
    
	$scope.selectedDepartmentHead = function(fieldId) {
		$rootScope.navigateToNextField(fieldId);
	};
	
	$scope.selectedReportingTo = function(fieldId) {
		$rootScope.navigateToNextField(fieldId);
	};
	
	 $scope.directiveDatePickerOpts = angular.copy($rootScope.datePickerOptions);
	 $scope.directiveDatePickerOpts.widgetParent = "body";
	 $scope.directiveDatePickerOpts.widgetPositioning = {
		        horizontal: 'right'
		    };
	 
	 $scope.dateTimePickerOptionsLeft = angular.copy($rootScope.datePickerOptions);
	 $scope.dateTimePickerOptionsLeft.widgetParent = "body";
	 $scope.dateTimePickerOptionsLeft.widgetPositioning = {
		        horizontal: 'left'
		    };
	
	 $scope.downloadAttach = function(param){
			if(param.data.id!=null && param.data.file==null){
				console.log("API CALL")	
				downloadFactory.downloadAttachment('/api/v1/employeemaster/files/', param.data.id, param.data.fileName);
			}else if(param.data.id==null && param.data.file!=null){
				console.log("NO API CALL BUT Byte array found")	
				var blob = new Blob([param.data.file], {type: param.data.fileContentType+";charset=utf-8"});
				saveAs(blob, param.data.fileName);
			}else{
				console.log("NO API CALL")	
				saveAs(param.data.tmpFile, param.data.fileName);
			}
		} 
    
    $scope.convertDateReceive = function(employeeMaster) {
    	
    	if(employeeMaster.employeePersonalDetail.dateOfBirth != null) {
			employeeMaster.employeePersonalDetail.dateOfBirth = $rootScope.dateToString(employeeMaster.employeePersonalDetail.dateOfBirth);
		}
		
		if(employeeMaster.employeePersonalDetail.weddingAnniversaryDate != null) {
			employeeMaster.employeePersonalDetail.weddingAnniversaryDate = $rootScope.dateToString(employeeMaster.employeePersonalDetail.weddingAnniversaryDate);
		}
		
		if(employeeMaster.employeeProfessionalDetail.dateOfJoining != null) {
			employeeMaster.employeeProfessionalDetail.dateOfJoining = $rootScope.dateToString(employeeMaster.employeeProfessionalDetail.dateOfJoining);
		}
		
		if(employeeMaster.employeeProfessionalDetail.dateOfJoining != null) {
			employeeMaster.employeeProfessionalDetail.transferDate = $rootScope.dateToString(employeeMaster.employeeProfessionalDetail.transferDate);
		}
		
		if(employeeMaster.employeeProfessionalDetail.dateOfJoining != null) {
			employeeMaster.employeeProfessionalDetail.employmentEndDate = $rootScope.dateToString(employeeMaster.employeeProfessionalDetail.employmentEndDate);
		}
		
		if(employeeMaster.employeeProfessionalDetail.dateOfJoining != null) {
			employeeMaster.employeeProfessionalDetail.designationStartDate = $rootScope.dateToString(employeeMaster.employeeProfessionalDetail.designationStartDate);
		}
		employeeMaster.employementStatus=$filter('EmpStatus')(employeeMaster.employementStatus);
		$scope.init();
    }
    
	 // History Start
	
	$scope.getEmployeeById = function(employeeId) {
		
		console.log("Get Employee By ID " , employeeId);
		
		EmployeeFactory.get.query({id : employeeId}).$promise.then(function(data) {
			if(data.responseCode == 'ERR0') {
				$scope.employeeMaster = data.responseObject;
				
				if($scope.employeeMaster.encodedImage != undefined && $scope.employeeMaster.encodedImage != null) {
					$scope.employeeMaster.image = $scope.employeeMaster.encodedImage;
				}
				
				if($scope.employeeMaster.employeePersonalDetail == undefined || $scope.employeeMaster.employeePersonalDetail == null) {
					$scope.employeeMaster.employeePersonalDetail = {};
				}
				
				if($scope.employeeMaster.employeeProfessionalDetail == undefined || $scope.employeeMaster.employeeProfessionalDetail == null) {
					$scope.employeeMaster.employeeProfessionalDetail = {};
				}
				
				if($scope.employeeMaster.attachmentList == undefined || $scope.employeeMaster.attachmentList == null || $scope.employeeMaster.attachmentList.length == 0) {
					$scope.employeeMaster.attachmentList = [{}];
				}
				
				
				$scope.convertDateReceive($scope.employeeMaster);
			} else {
				console.log(data.responseDescription);
			}
		}, function(error) {
			console.log(error);
		})
	}	
	
	//On leave the Unfilled pack Form Broadcast then event will happen in StateChangeListener(BroadCaster)-app_state_route.js

	$scope.$on('addEmployeeEvent', function(events, args){
		
			$rootScope.unfinishedData = $scope.employeeMaster;
			$rootScope.category = "Employee Master";
			$rootScope.unfinishedFormTitle = "Employee (New)";
			if($scope.employeeMaster != undefined && $scope.employeeMaster != null && $scope.employeeMaster.employeeCode != undefined 
					&& $scope.employeeMaster.employeeCode != null && $scope.employeeMaster.employeeCode != "") {
				$rootScope.subTitle = $scope.employeeMaster.employeeCode;
			} else {
				$rootScope.subTitle = "Unknown Employee"
			}
	});
	  
	 $scope.$on('editEmployeeEvent', function(events, args) {
		
		    $rootScope.unfinishedData = $scope.employeeMaster;
			$rootScope.category = "Employee Master";
			$rootScope.unfinishedFormTitle = "Employee Edit (" + $scope.employeeMaster.employeeCode + ")";
			if($scope.employeeMaster != undefined && $scope.employeeMaster != null && $scope.employeeMaster.employeeCode != undefined 
					&& $scope.employeeMaster.employeeCode != null && $scope.employeeMaster.employeeCode != "") {
				$rootScope.subTitle = $scope.employeeMaster.employeeCode;
			} else {
				$rootScope.subTitle = "Unknown Employee"
			}
	});
	
	
    $scope.$on('addEmployeeEventReload', function (e, confirmation) {
        confirmation.message = "";
        localStorage.reloadFormData = JSON.stringify($scope.employeeMaster) ;
        localStorage.isEmployeeReload = "YES";
        e.preventDefault();
    });

	
	$scope.$on('editEmployeeEventReload', function (e, confirmation) {
        confirmation.message = "";
        localStorage.reloadFormData = JSON.stringify($scope.employeeMaster) ;
        localStorage.isEmployeeReload = "YES";
        e.preventDefault();
    });
	
	
	
	$scope.isReloaded = localStorage.isEmployeeReload;
	
	if ($stateParams.action == "ADD") {
		if($stateParams.fromHistory === 'Yes') {
			if($scope.isReloaded == "YES") {
				console.log("Add History Reload...")
				$scope.isReloaded = "NO";
				localStorage.isEmployeeReload = "NO";
				$scope.employeeMaster  = JSON.parse(localStorage.reloadFormData);
			} else {
				console.log("Add History Not Reload...")
				$scope.employeeMaster = $rootScope.selectedUnfilledFormData;
			}
		} else {
			if($scope.isReloaded == "YES") {
				$scope.isReloaded = "NO";
				localStorage.isEmployeeReload = "NO";
				$scope.employeeMaster  = JSON.parse(localStorage.reloadFormData);
				console.log("Add Not History Reload...");
			} else {
				console.log("Add Not History Not Reload...")
				$scope.init();
			}	
		}
		$rootScope.breadcrumbArr = [{ label:"Master",state:"layout.employee"},{ label:"Employee",state:"layout.employee"},{ label:"Add Employee", state:null}];	
		
	} else {
		if($stateParams.fromHistory === 'Yes') {
			if($scope.isReloaded == "YES") {
					$scope.isReloaded = "NO";
					localStorage.isEmployeeReload = "NO";
					$scope.employeeMaster  = JSON.parse(localStorage.reloadFormData);
				} else {
					$scope.employeeMaster = $rootScope.selectedUnfilledFormData;
				}
			$scope.init();			
		} else {
			if($scope.isReloaded == "YES") {
				$scope.isReloaded = "NO";
				localStorage.isEmployeeReload = "NO";
				$scope.employeeMaster  = JSON.parse(localStorage.reloadFormData);
				$scope.init();
			} else {
				$scope.getEmployeeById($stateParams.employeeId);
			}
			
		}
		$rootScope.breadcrumbArr = [{ label:"Master",state:"layout.employee"},{ label:"Employee",state:"layout.employee"},{ label:"Edit Employee", state:null}];		
	}
	
		
	// History End
	
	
	
	

}]);