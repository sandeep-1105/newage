/**
 * EmployeeMaster Factory which initiates all Rest calls using resource service
 */
(function() {



    app.factory("EmployeeList",['$resource', function($resource) {
        return $resource("/api/v1/employeemaster/get/search/keyword", {}, {
            fetch: {
                method: 'POST'
            }
        });
    }]);

    app.factory("SalesmanList",['$resource', function($resource) {
        return $resource("/api/v1/employeemaster/get/search/keyword/salesman", {}, {
            fetch: {
                method: 'POST'
            }
        });
    }]);


    app.factory("EmployeeListOnLocation",['$resource', function($resource) {
        return $resource("/api/v1/employeemaster/get/search/keyword/:countryId", {}, {
            fetch: {
                method: 'POST',
                params: {
                    countryId: ''
                },
                isArray: false
            }
        });
    }]);


    app.factory("SalesmanListOnLocation",['$resource', function($resource) {
        return $resource("/api/v1/employeemaster/get/search/keyword/salesman/:countryId", {}, {
            fetch: {
                method: 'POST',
                params: {
                    countryId: ''
                },
                isArray: false
            }
        });
    }]);


    app.factory("CompanyWiseEmployee",['$resource', function($resource) {
        return $resource("/api/v1/employeemaster/get/searchByCompany/keyword/:companyId", {}, {
            fetch: {
                method: 'POST',
                params: {
                    companyId: ''
                },
                isArray: false
            }
        });
    }]);


    app.factory('EmployeeFactory',['$resource', function($resource) {
        return {
            search: $resource('/api/v1/employeemaster/get/search', {}, {
                query: {
                    method: 'POST'
                }
            }),
            get: $resource('/api/v1/employeemaster/get/id/:id', {}, {
                query: {
                    method: 'GET',
                    params: {
                        id: ''
                    },
                    isArray: false
                }
            }),
            remove: $resource('/api/v1/employeemaster/delete/:id', {}, {
                query: {
                    method: 'DELETE',
                    params: {
                        id: ''
                    }
                }
            }),
            save: $resource('/api/v1/employeemaster/create', {}, {
                query: {
                    method: 'POST'
                }
            })
        };
    }])


})();