app.controller('ChargeOperationCtrl',['$rootScope', '$stateParams', '$scope', '$state', 'ChargeAdd',
    'ChargeEdit', 'ChargeView', 'ngProgressFactory', 'ngDialog',
    'appConstant', 'CommonValidationService', 
	function($rootScope, $stateParams, $scope, $state, ChargeAdd,
    ChargeEdit, ChargeView, ngProgressFactory, ngDialog,
    appConstant, CommonValidationService) {

    $scope.chargeOperation = {};

    $scope.contained_progressbar = ngProgressFactory.createInstance();
    $scope.contained_progressbar.setParent(document.getElementById('charge-panel'));
    $scope.contained_progressbar.setAbsolute();


    $scope.init = function(acFlag) {

        $scope.firstFocus = true;
        $scope.showEdi = null;

        if ($scope.chargeMaster == undefined || $scope.chargeMaster == null) {
            $scope.chargeMaster = {};
            $scope.chargeMaster.status = 'Active';
        }

        if ($scope.chargeMaster.ediList == undefined || $scope.chargeMaster.ediList == null || $scope.chargeMaster.ediList.length == 0) {
            $scope.chargeMaster.ediList = [{}];
            $scope.chargeMaster.ediList.push()
        }

        $scope.setOldDataVal();
    };

    $scope.setOldDataVal = function() {
        $scope.oldData = JSON.stringify($scope.chargeMaster);
        console.log("Old Data Loaded");
    }

    $scope.clearOldDataVal = function() {
        $scope.oldData = "";
        console.log("Old Data Cleared");
    }

    $scope.cancel = function() {
        var isDirty = $scope.chargemasterform.$dirty
        if (isDirty) {
            var newScope = $scope.$new();
            newScope.errorMessage = $rootScope.nls["ERR200"];
            ngDialog.openConfirm({
                template: '<p>{{errorMessage}}</p>' +
                    '<div class="ngdialog-footer">' +
                    ' <button type="button" class="btn btn-default btn-property cancel-btn" ng-click="confirm(2)">No</button>' +
                    '<button type="button" class="btn btn-default btn-property accent-btn " ng-click="confirm(1)">Yes</button>' +
                    '<button type="button" class="btn btn-default btn-property pull-left grey-cancel-btn" ng-click="confirm(3)">Cancel</button>' +
                    '</div>',
                plain: true,
                scope: newScope,
                className: 'ngdialog-theme-default'
            }).then(function(value) {
                if (value == 1 && $scope.chargeMaster.id == null) {
                    $scope.save();
                } else if (value == 1 && $scope.chargeMaster.id != null) {
                    $scope.update();
                } else if (value == 2) {
                    $rootScope.clientMessage = null;
                    var params = {}
                    params.submitAction = 'Cancelled';
                    $state.go("layout.charge", params);
                } else {
                    console.log("cancelled");
                }
            });
        } else {
            var params = {}
            params.submitAction = 'Cancelled';
            $state.go("layout.charge", params);
        }
    }

    $scope.activeTab = function(tab) {
        $scope.currentTab = tab;
    }

    $scope.update = function() {

        $rootScope.clientMessage = null;
        if ($scope.validateChargeMaster(0)) {

            if ($scope.chargeOperation.ediIsValid()) {
                $scope.contained_progressbar.start();
                ChargeEdit.update($scope.chargeMaster).$promise.then(
                    function(data) {
                        if (data.responseCode == 'ERR0') {
                            console.log("Charge updated Successfully")
                            var params = {}
                            params.submitAction = 'Saved';
                            $rootScope.successDesc = $rootScope.nls["ERR401"];
                            $state.go("layout.charge", params);
                        } else {
                            console.log("Charge updated Failed " + data.responseDescription)
                        }
                        $scope.contained_progressbar.complete();
                        angular.element(".panel-body").animate({
                            scrollTop: 0
                        }, "slow");
                    },
                    function(error) {
                        console.log("Charge updated Failed : " + error)
                    });
            } else {
                $scope.EdiError = true;
                console.log("EDI List Invalid ", $rootScope.nls["ERR111"]);
                $rootScope.clientMessage = $rootScope.nls["ERR111"];
            }
        } else {
            console.log("invalid update failed")
        }
    }

    $scope.save = function() {

        $rootScope.clientMessage = null;
        if ($scope.validateChargeMaster(0)) {
            if ($scope.chargeOperation.ediIsValid()) {
                $scope.contained_progressbar.start();

                ChargeAdd.save($scope.chargeMaster).$promise.then(function(data) {
                    if (data.responseCode == 'ERR0') {
                        console.log("Charge added Successfully")
                        var params = {}
                        $rootScope.successDesc = $rootScope.nls["ERR400"];
                        params.submitAction = 'Saved';
                        $state.go("layout.charge", params);
                    } else {
                        console.log("Charge added Failed " + data.responseDescription)
                    }
                    $scope.contained_progressbar.complete();
                    angular.element(".panel-body").animate({
                        scrollTop: 0
                    }, "slow");

                }, function(error) {
                    console.log("Charge added Failed : " + error)
                });

            } else {
                $rootScope.clientMessage = $rootScope.nls["ERR111"];
                $scope.EdiError = true;
                console.log("EDI List Invalid ", $rootScope.nls["ERR111"]);
            }
        } else {
            console.log("Invalid Form")
        }
    }
    $scope.validateChargeMaster = function(validateCode) {

        console.log("Validate Called ", validateCode);

        $scope.errorMap = new Map();
        $scope.firstFocus = false;

        if (validateCode == 0 || validateCode == 1) {
            if ($scope.chargeMaster.chargeName == undefined ||
                $scope.chargeMaster.chargeName == null ||
                $scope.chargeMaster.chargeName == "") {
                $scope.errorMap.put("chargeName", $rootScope.nls["ERR2310"]);
                return false;
            } else {
                if (!CommonValidationService.checkRegExp(appConstant.Reg_Exp_Master_Charge_Name, $scope.chargeMaster.chargeName)) {
                    $scope.errorMap.put("chargeName", $rootScope.nls["ERR2307"]);
                    return false;
                }
            }
        }

        if (validateCode == 0 || validateCode == 2) {
            if ($scope.chargeMaster.chargeCode == undefined ||
                $scope.chargeMaster.chargeCode == null ||
                $scope.chargeMaster.chargeCode == "") {
                $scope.errorMap.put("chargeCode", $rootScope.nls["ERR2309"]);
                return false;
            } else {
                if (!CommonValidationService.checkRegExp(appConstant.Reg_Exp_Master_Charge_Code, $scope.chargeMaster.chargeCode)) {
                    $scope.errorMap.put("chargeCode", $rootScope.nls["ERR2306"]);
                    return false;
                }
            }
        }

        if (validateCode == 0 || validateCode == 3) {
            if ($scope.chargeMaster.chargeType == undefined ||
                $scope.chargeMaster.chargeType == null ||
                $scope.chargeMaster.chargeType == "") {
                $scope.errorMap.put("chargeType", $rootScope.nls["ERR2311"]);
                return false;
            }
        }

        if (validateCode == 0 || validateCode == 4) {
            if ($scope.chargeMaster.calculationType == undefined ||
                $scope.chargeMaster.calculationType == null ||
                $scope.chargeMaster.calculationType == "") {
                $scope.errorMap.put("calculationType", $rootScope.nls["ERR2312"]);
                return false;
            }
        }

        return true;
    }
    $scope.setStatus = function(data) {
        if (data == 'Active') {
            return 'activetype';
        } else if (data == 'Block') {
            return 'blockedtype';
        } else if (data == 'Hide') {
            return 'hiddentype';
        }
    }

    $scope.getChargeById = function(chargeId) {
        console.log("getChargeById ", chargeId);

        ChargeView.get({
            id: chargeId
        }, function(data) {
            if (data.responseCode == "ERR0") {
                console.log("Successful while getting charge.", data)
                $scope.chargeMaster = data.responseObject;
                $scope.init();
            }
        }, function(error) {
            console.log("Error while getting charge.", error)
        });

    }


    //On leave the Unfilled charge Form Broadcast then event will happen in StateChangeListener(BroadCaster)-app_state_route.js

    $scope.$on('addChargeEvent', function(events, args) {

        $rootScope.unfinishedData = $scope.chargeMaster;
        $rootScope.category = "Charge Master";
        $rootScope.unfinishedFormTitle = "Charge (New)";

        if ($scope.chargeMaster != undefined && $scope.chargeMaster != null && $scope.chargeMaster.chargeName != undefined &&
            $scope.chargeMaster.chargeName != null && $scope.chargeMaster.chargeName != "") {
            $rootScope.subTitle = $scope.chargeMaster.chargeName;
        } else {
            $rootScope.subTitle = "Unknown Charge"
        }
    })

    $scope.$on('editChargeEvent', function(events, args) {
        $rootScope.unfinishedData = $scope.chargeMaster;
        $rootScope.category = "Charge Master";
        $rootScope.unfinishedFormTitle = "Charge Edit (" + $scope.chargeMaster.chargeCode + ")";

        if ($scope.chargeMaster != undefined && $scope.chargeMaster != null && $scope.chargeMaster.chargeName != undefined &&
            $scope.chargeMaster.chargeName != null && $scope.chargeMaster.chargeName != "") {
            $rootScope.subTitle = $scope.chargeMaster.chargeName;
        } else {
            $rootScope.subTitle = "Unknown Charge"
        }
    })


    $scope.$on('addChargeEventReload', function(e, confirmation) {
        console.log("addChargeEventReload is called ", $scope.chargeMaster);
        confirmation.message = "";
        localStorage.reloadFormData = JSON.stringify($scope.chargeMaster);
        localStorage.isChargeReloaded = "YES";
        e.preventDefault();
    });


    $scope.$on('editChargeEventReload', function(e, confirmation) {
        confirmation.message = "";
        localStorage.reloadFormData = JSON.stringify($scope.chargeMaster);
        localStorage.isChargeReloaded = "YES";
        e.preventDefault();
    });


    $scope.isReloaded = localStorage.isChargeReloaded;

    if ($stateParams.action == "ADD") {
        if ($stateParams.fromHistory === 'Yes') {
            if ($scope.isReloaded == "YES") {
                console.log("Add History Reload...")
                $scope.isReloaded = "NO";
                localStorage.isChargeReloaded = "NO";
                $scope.chargeMaster = JSON.parse(localStorage.reloadFormData);
                $scope.init();
            } else {
                console.log("Add History NotReload...")
                $scope.chargeMaster = $rootScope.selectedUnfilledFormData;
                $scope.init();
            }
        } else {
            if ($scope.isReloaded == "YES") {
                $scope.isReloaded = "NO";
                localStorage.isChargeReloaded = "NO";
                $scope.chargeMaster = JSON.parse(localStorage.reloadFormData);
                console.log("Add NotHistory Reload...");
                $scope.init();
            } else {
                console.log("Add NotHistory NotReload...")
                $scope.init();
            }
        }
        $rootScope.breadcrumbArr = [{
                label: "Master",
                state: "layout.charge"
            },
            {
                label: "Charge",
                state: "layout.charge"
            },
            {
                label: "Add Charge",
                state: null
            }
        ];


    } else {
        if ($stateParams.fromHistory === 'Yes') {
            if ($scope.isReloaded == "YES") {
                $scope.isReloaded = "NO";
                localStorage.isChargeReloaded = "NO";
                $scope.chargeMaster = JSON.parse(localStorage.reloadFormData);
                $scope.init();
            } else {
                $scope.chargeMaster = $rootScope.selectedUnfilledFormData;
                $scope.init();
            }
        } else {
            if ($scope.isReloaded == "YES") {
                $scope.isReloaded = "NO";
                localStorage.isChargeReloaded = "NO";
                $scope.chargeMaster = JSON.parse(localStorage.reloadFormData);
                $scope.init();
            } else {
                $scope.getChargeById($stateParams.chargeId);
            }
        }
        $rootScope.breadcrumbArr = [{
                label: "Master",
                state: "layout.charge"
            },
            {
                label: "Charge",
                state: "layout.charge"
            },
            {
                label: "Edit Charge",
                state: null
            }
        ];
    }

    // History End
}]);