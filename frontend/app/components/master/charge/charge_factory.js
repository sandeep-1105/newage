(function() {

    app.factory("ChargeSearch",['$resource', function($resource) {
        return $resource("/api/v1/chargemaster/get/search", {}, {
            query: {
                method: 'POST'
            }
        });
    }]);

    app.factory("ChargeSearchKeyword",['$resource', function($resource) {
        return $resource("/api/v1/chargemaster/get/search/keyword", {}, {
            query: {
                method: 'POST'
            }
        });
    }]);

    app.factory("ChargeAdd",['$resource', function($resource) {
        return $resource("/api/v1/chargemaster/create", {}, {
            save: {
                method: 'POST'
            }
        });
    }]);

    app.factory("ChargeEdit",['$resource', function($resource) {
        return $resource("/api/v1/chargemaster/update", {}, {
            update: {
                method: 'POST'
            }
        });
    }]);

    /*app.factory("ChargeView", function($resource) {
    	return $resource("/api/v1/chargemaster/get/id/:id", {}, {
    		get : {
    			method : 'GET',
    			params : {
    				id : ''
    			},
    			isArray : false
    		}
    	});
    });*/

    app.factory("ChargeView",['$resource', function($resource) {
        return $resource("/api/v1/chargemaster/get/id/:id", {}, {
            get: {
                method: 'GET',
                params: {
                    id: ''
                },
                isArray: false
            }
        });
    }]);

    app.factory("ChargeRemove",['$resource', function($resource) {
        return $resource("/api/v1/chargemaster/delete/:id", {}, {
            remove: {
                method: 'DELETE',
                params: {
                    id: ''
                },
                isArray: false
            }
        });
    }]);


    app.factory("SearchChargeNotInList",['$resource', function($resource) {
        return $resource("/api/v1/chargemaster/get/search/exclude", {}, {
            fetch: {
                method: 'POST'
            }
        });
    }]);

})();