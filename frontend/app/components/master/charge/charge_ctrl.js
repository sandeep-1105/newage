app.controller('ChargeCtrl',['$rootScope', '$state', '$stateParams', '$scope', '$http', '$location', '$window',
    'ngTableParams', 'ChargeSearch', 'ChargeView', 'ngDialog',
    'ChargeRemove', 'roleConstant',
	function($rootScope, $state, $stateParams, $scope, $http, $location, $window,
    ngTableParams, ChargeSearch, ChargeView, ngDialog,
    ChargeRemove, roleConstant) {

    console.log("Charge controller loaded..");


    /************************************************************
     * ux - by Muthu
     * reason - ipad compatibility fix
     *
     * *********************************************************/
    $scope.deskTopView = true;

    /********************* ux fix ends here *****************************/
    $scope.chargeMaster = {};
    $scope.searchDto = {};
    $scope.limitArr = [10, 15, 20];
    $scope.page = 0;
    $scope.limit = 10;

    $scope.init = function() {
        console.log("Charge Init method called.");
        $scope.searchDto = {};
        $scope.search();
    };


    $scope.rowSelect = function(data) {
        if ($rootScope.roleAccess(roleConstant.MASTER_FINANCE_CHARGE_VIEW)) {

            $scope.chargeMaster = data;
            /************************************************************
             * ux - by Muthu
             * reason - ipad compatibility fix
             *
             * *********************************************************/

            var windowInner = $window.innerWidth;
            if (windowInner <= 1199) {
                $scope.deskTopView = false;

            } else {
                $scope.deskTopView = true;
            }
            angular.element($window).bind('resize', function() {
                if ($window.innerWidth >= 1200) {
                    $scope.deskTopView = true;
                } else if ($window.innerWidth <= 1199) {
                    if ($scope.chargeMaster.id != null) {
                        $scope.deskTopView = false;
                    } else {
                        $scope.deskTopView = true;
                    }
                }

                console.log("window resizing..." + $window.innerWidth);
                $scope.$digest();
            });

            /********************* ux fix ends here *****************************/

        }
    }

    $scope.chargeHeadArr = [{
        "name": "#",
        "width": "col-xs-0half",
        "model": "no",
        "search": false,
    }, {
        "name": "Name",
        "width": "inl_xl",
        "model": "chargeName",
        "search": true,
        "wrap_cell": true,
        "type": "text",
        "sort": true,
        "key": "searchChargeName"

    }, {
        "name": "Code",
        "width": "w100px",
        "model": "chargeCode",
        "search": true,
        "type": "text",
        "sort": true,
        "key": "searchChargeCode"
    }, {
        "name": "Charge Type",
        "width": "w100px",
        "model": "chargeType",
        "search": true,
        "type": "drop",
        "data": $rootScope.enum['ChargeType'],
        "sort": true,
        "key": "searchChargeType"
    }, {
        "name": "Calculation Type",
        "width": "w125px",
        "model": "calculationType",
        "search": true,
        "type": "drop",
        "data": $rootScope.enum['ChargeCalculationType'],
        "sort": true,
        "key": "searchCalculationType"
    }, {
        "name": "Status",
        "width": "w120px",
        "model": "status",
        "search": true,
        "type": "drop",
        "sort": true,
        "data": $rootScope.enum['LovStatus'],
        "key": "searchStatus"
    }]
    $scope.sortSelection = {
        sortKey: "chargeName",
        sortOrder: "asc"
    }
    $scope.cancel = function() {
        $scope.showLogs = false;
        $scope.showHistory = false;
        $scope.chargeMaster = {};
        /************************************************************
         * ux - by Muthu
         * reason - ipad compatibility fix
         *
         * *********************************************************/
        $scope.deskTopView = true;

        /********************* ux fix ends here *****************************/
    };
    $scope.add = function() {
        if ($rootScope.roleAccess(roleConstant.MASTER_FINANCE_CHARGE_CREATE)) {
            $state.go("layout.addCharge");
        }
    };
    $scope.edit = function() {
        if ($rootScope.roleAccess(roleConstant.MASTER_FINANCE_CHARGE_MODIFY)) {
            $state.go("layout.editCharge", {
                chargeId: $scope.chargeMaster.id
            });
        }
    };
    $scope.changepage = function(param) {
        $scope.page = param.page;
        $scope.limit = param.size;
        $scope.search();
    }

    $scope.currentTab = "address";
    $scope.activeTab = function(tab) {
        $scope.currentTab = tab;
    }
    $scope.view = function(selectedMaster) {
        console.log("View Charge Called");
        $scope.chargeMaster = selectedMaster;
    };
    $scope.limitChange = function(item) {
        $scope.page = 0;
        $scope.limit = item;
        $scope.cancel();
        $scope.search();
    }

    $scope.sortChange = function(param) {
        $scope.searchDto.orderByType = param.sortOrder
            .toUpperCase();
        $scope.searchDto.sortByColumn = param.sortKey;
        $scope.cancel();
        $scope.search();
    }
    $scope.deleteMaster = function() {
        if ($rootScope.roleAccess(roleConstant.MASTER_FINANCE_CHARGE_DELETE)) {
            var newScope = $scope.$new();
            newScope.errorMessage = $rootScope.nls["ERR234"];
            ngDialog
                .openConfirm({
                    template: '<p>{{errorMessage}}</p>' +
                        '<div class="ngdialog-footer">' +
                        '<button type="button" class="btn btn-default btn-property cancel-btn" ng-click="closeThisDialog(0)">Cancel' +
                        '<button type="button" class="btn btn-primary btn-property accent-btn" ng-click="confirm(1)">Yes' +
                        '</button></div>',
                    plain: true,
                    scope: newScope,
                    className: 'ngdialog-theme-default'
                })
                .then(
                    function(value) {
                        ChargeRemove
                            .remove({
                                    id: $scope.chargeMaster.id
                                },
                                function(data) {
                                    if (data.responseCode == 'ERR0') {
                                        console
                                            .log("Charge deleted Successfully")
                                        $scope
                                            .init();
                                        $scope.deskTopView = true;
                                    } else {
                                        console
                                            .log("Charge deleted Failed " +
                                                data.responseDescription)
                                    }
                                },

                                function(error) {
                                    console
                                        .log("Charge deleted Failed : " +
                                            error)
                                });
                    },
                    function(value) {
                        console.log("deleted cancelled");

                    });

        }
    }
    $scope.changeSearch = function(param) {
        for (var attrname in param) {
            $scope.searchDto[attrname] = param[attrname];
        }
        $scope.page = 0;
        $scope.cancel();
        $scope.search();

    }

    $scope.search = function() {
        $scope.chargeMaster = {};

        $scope.searchDto.selectedPageNumber = $scope.page;
        $scope.searchDto.recordPerPage = $scope.limit;
        console.log($scope.searchDto);
        $scope.chargeArr = [];
        ChargeSearch.query($scope.searchDto).$promise
            .then(function(data, status) {
                $scope.totalRecord = data.responseObject.totalRecord;
                var tempArr = [];
                var resultArr = [];
                tempArr = data.responseObject.searchResult;
                var tempObj = {};
                angular.forEach(tempArr, function(item,
                    index) {
                    tempObj = item;
                    tempObj.no = (index + 1) +
                        ($scope.page * $scope.limit);
                    resultArr.push(tempObj);
                    tempObj = {};
                });
                $scope.chargeArr = resultArr;
            });

    }

    $scope.serialNo = function(index) {
        index = ($scope.searchDto.selectedPageNumber * $scope.searchDto.recordPerPage) +
            (index + 1);
        return index;
    }

    // Routing start

    switch ($stateParams.action) {
        case "SEARCH":
            $scope.init();
            $rootScope.breadcrumbArr = [{
                    label: "Master",
                    state: "layout.charge"
                },
                {
                    label: "Charge",
                    state: "layout.charge"
                }
            ];
            break;
        case "VIEW":
            break;
    }

    // Routing end




}]);