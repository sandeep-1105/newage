app.factory('objectGroupMasterFactory', function($resource) {
	return {
		findById : $resource('/api/v1/objectgroupmaster/get/id/:id', {}, {
			 query : {method : 'GET', params : {id : ''}, isArray : false}
		}),
		create : $resource('/api/v1/objectgroupmaster/create', {}, {
			query : {method : 'POST'}
		}),
		delete : $resource('/api/v1/objectgroupmaster/delete/:id', {}, {
			query : {method : 'DELETE', params : {id : ''}}
		}),
		search : $resource('/api/v1/objectgroupmaster/get/search/keyword', {}, {
			query : {method : 'POST'}
		}),
		getall : $resource('/api/v1/objectgroupmaster/get/search', {}, {
			fetch : {method : 'POST'}
		})
	};
})