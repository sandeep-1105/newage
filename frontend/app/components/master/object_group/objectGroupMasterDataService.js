app.service('objectGroupMasterDataService', function($rootScope) {
    this.getHeadArray = function() {
	   return [
			  {
					"name":"#",
					"width":"col-xs-0half",
					"model":"no",
					"search":false
			  },{
					"name":"Name",
					"width":"col-xs-5",
					"model":"objectGroupName",
					"search":true,
					"wrap_cell":true,
					"type":"text",
					"sort":true,
					"key":"searchObjectGroupName"
				},{
					"name":"Code",
					"width":"col-xs-2half",
					"model":"objectGroupCode",
					"search":true,
					"type":"text",
					"sort":true,
					"key":"searchObjectGroupCode"
				},{
					"name":"Status",
					"width":"col-xs-3",
					"model":"status",
					"search":true,
					"type":"drop",
					"data":$rootScope.enum['LovStatus'],
					"key":"searchStatus",
					"sort":true
				}];
          };

    
    this.getListBreadCrumb = function() {
        return [{
                label: "Master",
                state: "layout.objectGroupMaster"
            },
            {
                label: "Object Group",
                state: "layout.objectGroupMaster"
            }
        ];
    }

    this.getAddBreadCrumb = function() {
        return [{
                label: "Master",
                state: "layout.objectGroupMaster"
            },
            {
                label: "Object Group",
                state: "layout.objectGroupMaster"
            },
            {
                label: "Add Object Group",
                state: null
            }
        ];
    };
    this.getEditBreadCrumb = function() {
        return [{
                label: "Master",
                state: "layout.objectGroupMaster"
            },
            {
                label: "Object Group",
                state: "layout.objectGroupMaster"
            },
            {
                label: "Edit Object Group",
                state: null
            }
        ];
    };
   

});