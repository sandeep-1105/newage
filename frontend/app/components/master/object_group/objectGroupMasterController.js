(function(){
app.controller("ObjectGroupMasterCtrl", ['$scope', '$rootScope', '$window', '$modal', '$state', '$stateParams', '$log', 'ngDialog', 'objectGroupMasterDataService', 'objectGroupMasterFactory', 'RecentHistorySaveService', 'objectGroupMasterValidationService', 'Notification', "roleConstant",
    function($scope, $rootScope, $window, $modal, $state, $stateParams, $log, ngDialog, objectGroupMasterDataService, objectGroupMasterFactory, RecentHistorySaveService, objectGroupMasterValidationService, Notification,roleConstant) {

        var vm = this;
        vm.tableHeadArr = objectGroupMasterDataService.getHeadArray();
        vm.dataArr = [];
        vm.objectGroupMaster = {};
        vm.showHistory = false;
        vm.errorMap = new Map();
        vm.deskTopView = true;

        vm.sortSelection = {
            sortKey: "objectGroupName",
            sortOrder: "asc"
        }
        
        vm.changeSearch = function(param) {
            $log.debug("Change Search is called." + param);
            for (var attrname in param) {
                vm.searchData[attrname] = param[attrname];
            }
            vm.page = 0;
            vm.search();
        }

        vm.sortChange = function(param) {
            $log.debug("sortChange is called. " + param);
            vm.searchData.orderByType = param.sortOrder.toUpperCase();
            vm.searchData.sortByColumn = param.sortKey;
            vm.search();
        }
        
        vm.setStatus = function(data){
    		if(data=='Active'){
    			return 'activetype';
    		} else if(data=='Block'){
    			return 'blockedtype';
    		}else if(data=='Hide'){
    			return 'hiddentype';
    		}
    	}
        
        /**
         * ObjectGroup Master List Navigate to selected page.
         * @param {Object} param - The Selected page option.
         */
        vm.changepage = function(param) {
            $log.debug("change Page..." + param.page);
            vm.page = param.page;
            vm.limit = param.size;
            vm.search();
        }

        /**
         * ObjectGroup Master List Page Limit Data change.
         * @param {int} nLimit - The Selected limit option from Limit array.
         */
        vm.limitChange = function(nLimit) {
            $log.debug("Limit Change..." + nLimit);
            vm.page = 0;
            vm.limit = nLimit;
            vm.search();
        }

        /**
         * ObjectGroup Master List Page Search Data  initializing and calling search Mtd.
         * @param {Object} data - The Selected ObjectGroup Master from List page.
         * @param {string} data.id - The id of the Selected ObjectGroup Master.
         * @param {int} index - The Selected row index from List page.
         */
        vm.rowSelect = function(data) {
        	if($rootScope.roleAccess(roleConstant.MASTER_GENERAL_OBJECT_GROUP_VIEW)){
        	vm.objectGroupMasteView=true;
			vm.objectGroupMaster = data;
			var windowInner=$window.innerWidth;
			if(windowInner<=1199){
				vm.deskTopView = false;
			}
			else{
				vm.deskTopView = true;
			}
			angular.element($window).bind('resize', function(){
				if($window.innerWidth>=1200){
					vm.deskTopView = true;
				}
				else if($window.innerWidth<=1199){
					if(vm.objectGroupMaster.id!=null){
						vm.deskTopView = false;
					}
					else {
						vm.deskTopView = true;
					}
				}
				$scope.$digest();
				
			});
        	}
		}

        /**
         * ObjectGroup Master List Page Search Data  initializing and calling search Mtd.
         */
        vm.searchInit = function() {
            vm.searchData = {};
            vm.limitArr = [10, 15, 20];
            vm.page = 0;
            vm.limit = 10;
            vm.totalRecord = 10;
            vm.searchData.orderByType = vm.sortSelection.sortOrder.toUpperCase();
            vm.searchData.sortByColumn = vm.sortSelection.sortKey;
            vm.search();
        };
        /**
         * Go Back to ObjectGroup Master List Page.
         */
        vm.backToList = function() {
        	vm.objectGroupMasteView = false;
        	vm.objectGroupMaster = {};
        	vm.deskTopView = true;
        }

       
        /**
         * Get ObjectGroup Master's Based on Search Data.
         */
        vm.search = function() {
        	vm.objectGroupMasteView=false;
            vm.searchData.selectedPageNumber = vm.page;
            vm.searchData.recordPerPage = vm.limit;
            vm.dataArr = [];
            objectGroupMasterFactory.search.query(vm.searchData).$promise.then(
                function(data) {
                    if (data.responseCode == "ERR0") {
                        vm.totalRecord = data.responseObject.totalRecord;
                        vm.dataArr = data.responseObject.searchResult;
                    }
                },
                function(errResponse) {
                	 $log.error('Error while fetching ObjectGroup');
                });
           }

        /**
         * Get ObjectGroup Master By id.
         * @param {int} id - The id of a ObjectGroup.
         * @param {boolean} isView - TO identify purpose of calling is for View or Edit a ObjectGroup.
         */
        vm.view = function(id, isView) {
        	objectGroupMasterFactory.findById.query({
                id: id
            }).$promise.then(function(data) {
                if (data.responseCode == 'ERR0') {
                    vm.objectGroupMaster = data.responseObject;
                    var rHistoryObj = {}
                    if (vm.objectGroupMaster != undefined && vm.objectGroupMaster != null && vm.objectGroupMaster.id != undefined && vm.objectGroupMaster.id != null) {
                        $rootScope.subTitle = vm.objectGroupMaster.objectGroupName;
                    } else {
                        $rootScope.subTitle = "Unknown ObjectGroup"
                    }
                    $rootScope.unfinishedFormTitle = "ObjectGroup # " + vm.objectGroupMaster.objectGroupCode;
                    if (isView) {
                        rHistoryObj = {
                            'title': "ObjectGroup #" + vm.objectGroupMaster.objectGroupCode,
                            'subTitle': $rootScope.subTitle,
                            'stateName': $state.current.name,
                            'stateParam': JSON.stringify($stateParams),
                            'stateCategory': 'ObjectGroup Master',
                            'serviceType': undefined,
                            'showService': false
                        }

                    } else {

                        rHistoryObj = {
                            'title': 'Edit ObjectGroup # ' + $stateParams.serviceId,
                            'subTitle': $rootScope.subTitle,
                            'stateName': $state.current.name,
                            'stateParam': JSON.stringify($stateParams),
                            'stateCategory': 'ObjectGroup Master',
                            'serviceType': 'MASTER',
                            'serviceCode': ' ',
                            'orginAndDestination': ' '
                        }
                    }
                    RecentHistorySaveService.form(rHistoryObj);
                } else {
                    $log.debug("Exception while getting ObjectGroup by Id ", data.responseDescription);
                }
            }, function(error) {
                $log.debug("Error ", error);
            });
        };

        /**
         * Go To Add ObjectGroup Master.
         */
        vm.addNew = function() {
        	if($rootScope.roleAccess(roleConstant.MASTER_GENERAL_OBJECT_GROUP_CREATE)){
            $state.go("layout.objectGroupMasterAdd");
        }
        	}
       
        
        /**
         * Cancel Add or Edit ObjectGroup Master.
         * @param {int} id - The id of a ObjectGroup
         */
       
        vm.cancel = function(objId) {
            if(vm.objectGroupMasterForm!=undefined){
		    	vm.goTocancel(vm.objectGroupMasterForm.$dirty,objId);
            }else{
            	vm.goTocancel(false,objId);
            }
        }
       
       vm.goTocancel = function(isDirty,objId){
     		 if (isDirty) {
     		      var newScope = $scope.$new();
     		      newScope.errorMessage = $rootScope.nls["ERR200"];
     		      ngDialog.openConfirm({
     		        template: '<p>{{errorMessage}}</p>' +
     		          '<div class="ngdialog-footer">' +
     		          ' <button type="button" class="btn btn-default btn-property cancel-btn" ng-click="confirm(2)">No</button>' +
     		          '<button type="button" class="btn btn-default btn-property accent-btn " ng-click="confirm(1)">Yes</button>' +
     		          '<button type="button" class="btn btn-default btn-property pull-left grey-cancel-btn" ng-click="confirm(3)">Cancel</button>' +
     		          '</div>',
     		        plain: true,
     		        closeByDocument: false,
     		        scope: newScope,
     		        className: 'ngdialog-theme-default'
     		      }).then(
     		        function(value) {
     		          if (value == 1) {
     		        	 vm.saveOrUpdate();
     		          } else if (value == 2) {
     		        	 $state.go("layout.objectGroupMaster", {
     		                submitAction: "Cancelled"
     		            });
     		          } else {
     		            $log.debug("cancelled");
     		          }
     		        });
     		    } else {
     		     $state.go("layout.objectGroupMaster", {
                   submitAction: "Cancelled"
               });
     		    }
     	    }

        /**
         * Modify Existing ObjectGroup Master.
         * @param {int} id - The id of a Country.
         */
        vm.edit = function(id) {
        	if($rootScope.roleAccess(roleConstant.MASTER_GENERAL_OBJECT_GROUP_MODIFY)){
            $state.go("layout.objectGroupMasterEdit", {
                id: id
            });
        	}
        }

        /**
         * Delete Country Master.
         * @param {int} id - The id of a Country.
         */
        vm.delete = function(id) {
        	if($rootScope.roleAccess(roleConstant.MASTER_GENERAL_OBJECT_GROUP_DELETE)){
        	var newScope = $scope.$new();
			newScope.errorMessage = $rootScope.nls["ERR06907"];
	    	ngDialog.openConfirm( {
	                    template : '<p>{{errorMessage}}</p>'
	                    + '<div class="ngdialog-footer">'
	                    + '<button type="button" class="btn btn-default btn-property cancel-btn" ng-click="closeThisDialog(0)">Cancel'
	                    + '<button type="button" class="btn btn-primary btn-property accent-btn" ng-click="confirm(1)">Yes'
	                    + '</button></div>',
	                    plain : true,
	                    scope : newScope,
	                    closeByDocument: false,
	                    className : 'ngdialog-theme-default'
	                }).then( function(value) {
	                	objectGroupMasterFactory.delete.query({
	                        id: id
	                    }).$promise.then(function(data) {
	                        if (data.responseCode == 'ERR0') {
	                        	vm.objectGroupMasteView=false;
	                        	Notification.success($rootScope.nls["ERR402"]);
	                        	vm.deskTopView = true;
	                        	vm.search();
	                        }
	                    }, function(error) {
	                        $log.debug("Error ", error);
	                    });
	                }, function(value) {
	                    $log.debug("delete Cancelled");
	            });
        }
        }
        
        /**
         * Validating field of ObjectGroup Master while instant change.
         * @param {errorElement,errorCode}
         */
        vm.validate = function(valElement, code) {
            var validationResponse = objectGroupMasterValidationService.validate(vm.objectGroupMaster,code);
            if(validationResponse.error == true) {
				$scope.validationErrorAndFocus(validationResponse, validationResponse.errElement);
			} else {
				vm.errorMap.put(valElement, null);
            	validationResponse.error = false;
            }
        }
        
        /**
         * Common Validation Focus Functionality - starts here" 
         */
  		$scope.validationErrorAndFocus = function(valResponse, elemId){
  			vm.errorMap = new Map();
  			if(valResponse.error == true){
  				vm.errorMap.put(valResponse.errElement, valResponse.errMessage);
  			}
  				$scope.navigateToNextField(elemId);
  		} 

        /**
         * Create New ObjectGroup Master.
         */
        vm.saveOrUpdate = function() {
            var validationResponse = objectGroupMasterValidationService.validate(vm.objectGroupMaster,0);
            if(validationResponse.error == true) {
            	$log.debug("Validation Response -- ", validationResponse);
            	$log.debug('Validation Failed to create ObjectGroup..')
				$scope.validationErrorAndFocus(validationResponse, validationResponse.errElement);
			}
            else {
            	if(vm.objectGroupMaster.id== undefined && vm.objectGroupMaster.id==null){
            		objectGroupMasterFactory.create.query(vm.objectGroupMaster).$promise.then(function(data) {
    		            if (data.responseCode == 'ERR0') {
    		            	$log.debug("ObjectGroup Saved Successfully Response :: ", data);
    		            	Notification.success($rootScope.nls["ERR400"]);
    		            	$state.go('layout.objectGroupMaster');
    		            } else {
    		                $log.debug("Create ObjectGroup Failed :" + data.responseDescription)
    		            }

    		        }, function(error) {
    		        	 $log.debug("Create ObjectGroup Failed : " + error)
    		        });	
            	}else{
                 	objectGroupMasterFactory.create.query(vm.objectGroupMaster).$promise.then(function(data) {
     		            if (data.responseCode == 'ERR0') {
     		            	$log.debug("ObjectGroup Updated Successfully Response :: ", data);
     		            	Notification.success($rootScope.nls["ERR401"]);
     		            	$state.go('layout.objectGroupMaster');
     		            } else {
     		                $log.debug("Updating ObjectGroup Failed :" + data.responseDescription)
     		            }
     		        }, function(error) {
     		        	 $log.debug("Updating ObjectGroup Failed : " + error)
     		        });
                 
            	}
            	
            	
            } 
        };

      
        /**
         * Recent History - starts here
         */
        vm.isReloaded = localStorage.isObjectGroupMasterReloaded;
        $scope.$on('addObjectGroupMasterEvent', function(events, args){
        	$rootScope.unfinishedData = vm.objectGroupMaster;
			$rootScope.category = "ObjectGroup";
			$rootScope.unfinishedFormTitle = "ObjectGroup (New)";
        	if(vm.objectGroupMaster != undefined && vm.objectGroupMaster != null && vm.objectGroupMaster.objectGroupName != undefined && vm.objectGroupMaster.objectGroupName != null) {
        		$rootScope.subTitle = vm.objectGroupMaster.objectGroupName;
        	} else {
        		$rootScope.subTitle = "Unknown ObjectGroup "
        	}
        })
      
	    $scope.$on('editObjectGroupMasterEvent', function(events, args){
	    	$rootScope.unfinishedData = vm.objectGroupMaster;
			$rootScope.category = "ObjectGroup";
			$rootScope.unfinishedFormTitle = "ObjectGroup Edit # "+vm.objectGroupMaster.objectGroupCode;
	    })
	      
	    $scope.$on('addObjectGroupMasterEventReload', function(e, confirmation){
	    	    confirmation.message = "";
	    	    localStorage.reloadFormData = JSON.stringify(vm.objectGroupMaster) ;
	    	    localStorage.isObjectGroupMasterReloaded = "YES";
	            e.preventDefault();
	    })
	    
	    $scope.$on('editObjectGroupMasterEventReload', function(e, confirmation){
	    	    confirmation.message = "";
	    	    localStorage.reloadFormData = JSON.stringify(vm.objectGroupMaster) ;
	    	    localStorage.isObjectGroupMasterReloaded = "YES";
	            e.preventDefault();
	    	  
	    })
	      


        /**
         * State Identification Handling Here.
         * @param {String} $stateParams.action - VIEW, ADD, EDIT, SEARCH.
         */
        //
        switch ($stateParams.action) {
            case "ADD":
            	 if ($stateParams.fromHistory === 'Yes') {
                     if (vm.isReloaded == "YES") {
                      vm.isReloaded = "NO";
                      localStorage.isObjectGroupMasterReloaded = "NO";
                       vm.objectGroupMaster = JSON.parse(localStorage.reloadFormData);
                     } else {
                       vm.objectGroupMaster = $rootScope.selectedUnfilledFormData;
                     }
                   } else {
                     if (vm.isReloaded == "YES") {
                       vm.isReloaded = "NO";
                       localStorage.isObjectGroupMasterReloaded = "NO";
                       vm.objectGroupMaster = JSON.parse(localStorage.reloadFormData);
                     } else {
                         $log.debug("Add NotHistory NotReload...")
                     }
                   }
                $rootScope.breadcrumbArr = objectGroupMasterDataService.getAddBreadCrumb();
                break;
            case "EDIT":
            	 if (vm.fromHistory === 'Yes') {
                     if (vm.isReloaded == "YES") {
                       vm.isReloaded = "NO";
                       localStorage.isObjectGroupMasterReloaded = "NO";
                       vm.objectGroupMaster = JSON.parse(localStorage.reloadFormData);
                     } else {
                       vm.objectGroupMaster = $rootScope.selectedUnfilledFormData;
                     }
                   } else {
                     if (vm.isReloaded == "YES") {
                       vm.isReloaded = "NO";
                       localStorage.isObjectGroupMasterReloaded = "NO";
                       vm.objectGroupMaster = JSON.parse(localStorage.reloadFormData);
                     } else {
                    	 vm.view($stateParams.id, false);
                     }
                   }
                $rootScope.breadcrumbArr = objectGroupMasterDataService.getEditBreadCrumb();
                break;
            default:
                $rootScope.breadcrumbArr = objectGroupMasterDataService.getListBreadCrumb();
                vm.searchInit();
        }

    }
]);
}) ();