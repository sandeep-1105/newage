app.service('objectGroupMasterValidationService', ['$rootScope','ValidateUtil', 'CommonValidationService', 'appConstant', function($rootScope, ValidateUtil, CommonValidationService, appConstant) {
	this.validate = function(objectGroup, code) {
		//Validate objectGroup name
		if(code == 0 || code == 1) {
			if(objectGroup.objectGroupName == undefined || objectGroup.objectGroupName == null || objectGroup.objectGroupName =="" ){
				return this.validationResponse(true, "objectGroupName", $rootScope.nls["ERR06901"], objectGroup);
			}
		}
		//Validate objectGroup code
		if(code == 0 || code == 2) {
			if(objectGroup.objectGroupCode == undefined || objectGroup.objectGroupCode == null || objectGroup.objectGroupCode =="" ){
				return this.validationResponse(true, "objectGroupCode", $rootScope.nls["ERR06900"], objectGroup);
			}
		}
		
		return this.validationSuccesResponse(objectGroup);
	}
 
	this.validationSuccesResponse = function(objectGroup) {
	    return {error : false, obj : objectGroup};
	}
	
	this.validationResponse = function(err, elem, message, objectGroup) {
		return {error : err, errElement : elem, errMessage : message, obj : objectGroup};
	}
	
}]);