/**
 * CurrencyMasterCtrl which list all the details from database 
 */
app.controller('CurrencyMasterCtrl',['$rootScope', '$scope', '$window', '$stateParams', '$state', 'CurrencyMasterSearch', 
		'ngDialog', 'CurrencyMasterRemove', 'roleConstant', 
	function($rootScope, $scope, $window, $stateParams, $state, CurrencyMasterSearch, 
		ngDialog, CurrencyMasterRemove, roleConstant) {

    /************************************************************
     * ux - by Muthu
     * reason - ipad compatibility fix
     *
     * *********************************************************/
    $scope.deskTopView = true;

    /********************* ux fix ends here *****************************/

    $scope.init = function() {

        console.log("CurrencyMaster Init method called.");

        $rootScope.setNavigate1("Master");
        $rootScope.setNavigate2("Currency");

        $scope.searchDto = {};
        $scope.limitArr = [10, 15, 20];
        $scope.page = 0;
        $scope.limit = 10;


        $scope.searchDto = {};

        $scope.search();
    };


    $scope.currencyHeadArr = [{
            "name": "#",
            "width": "col-xs-0half",
            "model": "no",
            "search": false,
        },
        {
            "name": "Name",
            "width": "w200px",
            "model": "currencyName",
            "search": true,
            "wrap_cell": true,
            "type": "text",
            "sort": true,
            "key": "searchCurrencyName"

        },
        {
            "name": "Code",
            "width": "w100px",
            "model": "currencyCode",
            "search": true,
            "type": "text",
            "sort": true,
            "key": "searchCurrencyCode"
        },
        {
            "name": "Country",
            "width": "w150px",
            "model": "countryMaster.countryName",
            "search": true,
            "type": "text",
            "sort": true,
            "key": "searchCountryName",
            "wrap_cell": true,
        },
        {
            "name": "Prefix",
            "width": "w80px",
            "model": "prefix",
            "search": true,
            "type": "text",
            "sort": true,
            "key": "searchPrefix",
            "wrap_cell": true
        },
        {
            "name": "Suffix",
            "width": "w80px",
            "model": "suffix",
            "search": true,
            "type": "text",
            "sort": true,
            "key": "searchSuffix",
            "wrap_cell": true,
        },
        {
            "name": "Status",
            "width": "w150px",
            "model": "status",
            "search": true,
            "type": "drop",
            "data": $rootScope.enum['LovStatus'],
            "key": "searchStatus",
            "sort": true,
        }
    ]

    $scope.sortSelection = {
        sortKey: "currencyName",
        sortOrder: "asc"
    }



    $scope.changepage = function(param) {
        $scope.page = param.page;
        $scope.limit = param.size;
        $scope.search();
    }


    $scope.limitChange = function(item) {
        $scope.page = 0;
        $scope.limit = item;
        $scope.search();
    }


    $scope.rowSelect = function(data) {
        if ($rootScope.roleAccess(roleConstant.MASTER_GENERAL_CURRENCY_VIEW)) {
            //$scope.CurrencyMaster.id=1;
            $scope.showHistory = false;
            $scope.currencyMaster = data;
            if ($scope.currencyMaster.decimalPoint == 2) {
                $scope.currencyMaster.denomination = 100;
            } else {
                $scope.currencyMaster.denomination = 1000;
            }
            /************************************************************
             * ux - by Muthu
             * reason - ipad compatibility fix
             *
             * *********************************************************/

            var windowInner = $window.innerWidth;
            if (windowInner <= 1199) {
                $scope.deskTopView = false;

            } else {
                $scope.deskTopView = true;
            }
            angular.element($window).bind('resize', function() {
                if ($window.innerWidth >= 1200) {
                    $scope.deskTopView = true;
                } else if ($window.innerWidth <= 1199) {
                    if ($scope.currencyMaster.id != null) {
                        $scope.deskTopView = false;
                    } else {
                        $scope.deskTopView = true;
                    }
                }

                console.log("window resizing..." + $window.innerWidth);
                $scope.$digest();
            });

            /********************* ux fix ends here *****************************/
        }
    }


    $scope.cancel = function() {

        $scope.currencyMaster = {};
        /************************************************************
         * ux - by Muthu
         * reason - ipad compatibility fix
         *
         * *********************************************************/
        $scope.deskTopView = true;

        /********************* ux fix ends here *****************************/
    };

    $scope.add = function() {
        if ($rootScope.roleAccess(roleConstant.MASTER_GENERAL_CURRENCY_CREATE)) {
            console.log("Currency Add Button is pressed....");
            $state.go("layout.addCurrency");
        }
    };

    $scope.editCurrency = function() {
        if ($rootScope.roleAccess(roleConstant.MASTER_GENERAL_CURRENCY_MODIFY)) {
            console.log("Currency Edit Button is pressed....");
            $state.go("layout.editCurrency", {
                currencyId: $scope.currencyMaster.id
            });
        }
    };

    $scope.changeSearch = function(param) {
        for (var attrname in param) {
            $scope.searchDto[attrname] = param[attrname];
        }
        $scope.page = 0;
        $scope.search();
        console.log("change search", $scope.searchDto);
    }

    $scope.sortChange = function(param) {
        $scope.searchDto.orderByType = param.sortOrder.toUpperCase();
        $scope.searchDto.sortByColumn = param.sortKey;
        $scope.search();
    }

    $scope.deleteCurrency = function() {
        if ($rootScope.roleAccess(roleConstant.MASTER_GENERAL_CURRENCY_DELETE)) {
            var newScope = $scope.$new();
            newScope.errorMessage = $rootScope.nls["ERR232"];
            ngDialog.openConfirm({
                template: '<p>{{errorMessage}}</p>' +
                    '<div class="ngdialog-footer">' +
                    '<button type="button" class="btn btn-default btn-property cancel-btn" ng-click="closeThisDialog(0)">Cancel' +
                    '<button type="button" class="btn btn-primary btn-property accent-btn" ng-click="confirm(1)">Yes' +
                    '</button></div>',
                plain: true,
                scope: newScope,
                className: 'ngdialog-theme-default'
            }).then(function(value) {
                CurrencyMasterRemove.remove({
                    id: $scope.currencyMaster.id
                }, function(data) {

                    if (data.responseCode == 'ERR0') {
                        console.log("CurrencyMaster deleted Successfully");
                        $scope.init();
                        $scope.deskTopView = true;
                    } else {
                        console.log("CurrencyMaster deleted Failed ", data.responseDescription)
                    }
                }, function(error) {
                    console.log("CurrencyMaster deleted Failed : " + error)
                });
            }, function(value) {
                console.log("deleted cancelled");
            });
        }
    }

    $scope.search = function() {
        $scope.searchDto.selectedPageNumber = $scope.page;
        $scope.searchDto.recordPerPage = $scope.limit;
        CurrencyMasterSearch.query($scope.searchDto).$promise.then(function(data, status) {
            $scope.totalRecord = data.responseObject.totalRecord;
            var tempArr = [];
            var resultArr = [];
            tempArr = data.responseObject.searchResult;

            var tempObj = {};
            angular.forEach(tempArr, function(item, index) {
                tempObj = item;
                tempObj.no = (index + 1) + ($scope.page * $scope.limit);
                resultArr.push(tempObj);
                tempObj = {};
            });

            $scope.currencyArr = resultArr;
            console.log("currencyArr", $scope.currencyArr);
        });
    }


    if ($stateParams.action == "SEARCH") {
        console.log("Currency Master Search Page......")
        $scope.init();
        $rootScope.breadcrumbArr = [{
                label: "Master",
                state: "layout.currency"
            },
            {
                label: "Currency",
                state: "layout.currency"
            }
        ];
    } else {
        console.log("Currency Master View Page......")
        $scope.init();
    }

}]);