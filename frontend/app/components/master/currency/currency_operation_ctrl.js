/**
 * ServiceMasterOperationCtrl--which is performed update and save operations
 * 
 */
app.controller('CurrencyOperationCtrl',['$rootScope', '$scope', 'CurrencyMasterAdd', 'CurrencyMasterEdit', 'CurrencyMasterView',
    '$stateParams', '$state', 'CountryList', 'ngProgressFactory', 'ngDialog', 'appConstant', 'CommonValidationService', 
	function($rootScope, $scope, CurrencyMasterAdd, CurrencyMasterEdit, CurrencyMasterView,
    $stateParams, $state, CountryList, ngProgressFactory, ngDialog, appConstant, CommonValidationService) {

    $scope.contained_progressbar = ngProgressFactory.createInstance();
    $scope.contained_progressbar.setParent(document.getElementById('currency-panel'));
    $scope.contained_progressbar.setAbsolute();

    $scope.init = function(acFlag) {

        if ($scope.currencyMaster == undefined && $scope.currencyMaster == null) {
            $scope.currencyMaster = {};
            $scope.currencyMaster.status = 'Active';
        }

        $scope.setOldDataVal();
    };

    $scope.setOldDataVal = function() {
        $scope.oldData = JSON.stringify($scope.currencyMaster);
    }

    $scope.cancel = function() {

        if ($scope.oldData != JSON.stringify($scope.currencyMaster)) {
            var newScope = $scope.$new();
            newScope.errorMessage = $rootScope.nls["ERR200"];
            ngDialog.openConfirm({
                template: '<p>{{errorMessage}}</p>' +
                    '<div class="ngdialog-footer">' +
                    ' <button type="button" class="btn btn-default btn-property cancel-btn" ng-click="confirm(2)">No</button>' +
                    '<button type="button" class="btn btn-default btn-property accent-btn " ng-click="confirm(1)">Yes</button>' +
                    '<button type="button" class="btn btn-default btn-property pull-left grey-cancel-btn" ng-click="confirm(3)">Cancel</button>' +
                    '</div>',
                plain: true,
                scope: newScope,
                className: 'ngdialog-theme-default'
            }).then(function(value) {
                if (value == 1 && $scope.currencyMaster.id == null) {
                    $scope.save();
                } else if (value == 1 && $scope.currencyMaster.id != null) {
                    $scope.update();
                } else if (value == 2) {
                    var param = {};
                    param.submitAction = "Cancelled";
                    $state.go("layout.currency", param);
                } else {
                    console.log("cancelled");
                }
            });

        } else {
            var param = {};
            param.submitAction = "Cancelled";
            $state.go("layout.currency", param);
        }
    }


    $scope.bindDenomination = function() {

        if ($scope.currencyMaster.decimalPoint == "2") {
            $scope.currencyMaster.denomination = 100;
        } else if ($scope.currencyMaster.decimalPoint == "3") {
            $scope.currencyMaster.denomination = 1000;
        } else {
            $scope.currencyMaster.denomination = "";
        }

    };




    $scope.update = function() {
        console.log("Update method is called....");

        if ($scope.validateCurrencyMaster(0)) {

            $scope.contained_progressbar.start();

            CurrencyMasterEdit.update($scope.currencyMaster).$promise.then(function(data) {

                if (data.responseCode == "ERR0") {
                    console.log("Currency updated Successfully")
                    var param = {};
                    param.submitAction = "Saved";
                    $rootScope.successDesc = $rootScope.nls["ERR401"];
                    $state.go("layout.currency", param);


                } else {
                    console.log("Currency updated Failed ", data.responseDescription)
                }

                $scope.contained_progressbar.complete();

                angular.element(".panel-body").animate({
                    scrollTop: 0
                }, "slow");
            }, function(error) {

                console.log("Currency updated Failed : ", error)

            });

        } else {
            console.log("Currency Form Invalid");
        }

    }


    $scope.save = function() {

        console.log("Save is called.....");

        if ($scope.validateCurrencyMaster(0)) {
            $scope.contained_progressbar.start();

            CurrencyMasterAdd.save($scope.currencyMaster).$promise.then(function(data) {

                if (data.responseCode == "ERR0") {
                    console.log("Currency added Successfully")
                    var param = {};
                    param.submitAction = "Saved";
                    $rootScope.successDesc = $rootScope.nls["ERR400"];
                    $state.go("layout.currency", param);
                } else {
                    console.log("Currency added Failed ", data.responseDescription)
                }
                $scope.contained_progressbar.complete();
                angular.element(".panel-body").animate({
                    scrollTop: 0
                }, "slow");
            }, function(error) {
                console.log("Currency added Failed : " + error)
            });
        } else {
            console.log("Currency Form Invalid");
        }
    }


    $scope.ajaxCountryEvent = function(object) {

        console.log("ajaxCountryEvent is called ", object);

        $scope.searchDto = {};
        $scope.searchDto.keyword = object == null ? "" : object;
        $scope.searchDto.selectedPageNumber = 0;
        $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

        return CountryList.fetch($scope.searchDto).$promise.then(function(data) {
                if (data.responseCode == "ERR0") {
                    $scope.countriesList = data.responseObject.searchResult;
                    return $scope.countriesList;
                }
            },
            function(errResponse) {
                console.error('Error while fetching Countries', errResponse);
            });
    }


    $scope.selectedCountry = function(countryObj) {
        $scope.validateCurrencyMaster(3);
    };

    $scope.validateCurrencyMaster = function(validateCode) {
        console.log("Validate Called");
        $scope.errorMap = new Map();

        if (validateCode == 0 || validateCode == 1) {
            if ($scope.currencyMaster.currencyName == undefined ||
                $scope.currencyMaster.currencyName == null ||
                $scope.currencyMaster.currencyName == "") {
                $scope.errorMap.put("currencyName", $rootScope.nls["ERR2107"]);
                return false;
            } else {
                if (!CommonValidationService.checkRegExp(appConstant.Reg_Exp_Master_CURRENCY_NAME, $scope.currencyMaster.currencyName)) {
                    $scope.errorMap.put("currencyName", $rootScope.nls["ERR2109"]);
                    return false;
                }
            }
        }

        if (validateCode == 0 || validateCode == 2) {
            if ($scope.currencyMaster.currencyCode == undefined ||
                $scope.currencyMaster.currencyCode == null ||
                $scope.currencyMaster.currencyCode == "") {
                $scope.errorMap.put("currencyCode", $rootScope.nls["ERR2106"]);
                return false;
            } else {
                if (!CommonValidationService.checkRegExp(appConstant.Reg_Exp_Master_CURRENCY_CODE, $scope.currencyMaster.currencyCode)) {
                    $scope.errorMap.put("currencyCode", $rootScope.nls["ERR2108"]);
                    return false;
                }
            }
        }


        if (validateCode == 0 || validateCode == 3) {
            if ($scope.currencyMaster.countryMaster == undefined ||
                $scope.currencyMaster.countryMaster == null ||
                $scope.currencyMaster.countryMaster.id == undefined ||
                $scope.currencyMaster.countryMaster.id == null) {
                $scope.errorMap.put("country", $rootScope.nls["ERR2146"]);
                return false;
            }
            if ($scope.currencyMaster.countryMaster.status == "Block") {
                $scope.currencyMaster.countryMaster = null;
                $scope.errorMap.put("country", $rootScope.nls["ERR2143"]);
                return false;
            }
            if ($scope.currencyMaster.countryMaster.status == "Hide") {
                $scope.currencyMaster.countryMaster = null;
                $scope.errorMap.put("country", $rootScope.nls["ERR2144"]);
                return false;
            }
        }

        if (validateCode == 0 || validateCode == 4) {
            if ($scope.currencyMaster.prefix == undefined ||
                $scope.currencyMaster.prefix == null ||
                $scope.currencyMaster.prefix == "") {
                $scope.errorMap.put("prefix", $rootScope.nls["ERR2114"]);
                return false;
            } else {
                if (!CommonValidationService.checkRegExp(appConstant.Reg_Exp_Master_CURRENCY_PREFIX, $scope.currencyMaster.prefix)) {
                    $scope.errorMap.put("prefix", $rootScope.nls["ERR2112"]);
                    return false;
                }
            }
        }

        if (validateCode == 0 || validateCode == 5) {
            if ($scope.currencyMaster.suffix == undefined ||
                $scope.currencyMaster.suffix == null ||
                $scope.currencyMaster.suffix == "") {
                $scope.errorMap.put("suffix", $rootScope.nls["ERR2115"]);
                return false;
            } else {
                if (!CommonValidationService.checkRegExp(appConstant.Reg_Exp_Master_CURRENCY_SUFFFIX, $scope.currencyMaster.suffix)) {
                    $scope.errorMap.put("suffix", $rootScope.nls["ERR2113"]);
                    return false;
                }

            }
        }



        if (validateCode == 0 || validateCode == 6) {

            if ($scope.currencyMaster.decimalPoint == undefined ||
                $scope.currencyMaster.decimalPoint == null ||
                $scope.currencyMaster.decimalPoint == "") {
                $scope.currencyMaster.denomination = "";
                $scope.errorMap.put("decimal", $rootScope.nls["ERR2147"]);
                console.log($rootScope.nls["ERR2147"]);
                return false;
            }
        }


        return true;

    };


    $scope.setStatus = function(data) {
        if (data == 'Active') {
            return 'activetype';
        } else if (data == 'Block') {
            return 'blockedtype';
        } else if (data == 'Hide') {
            return 'hiddentype';
        }
    }

    $scope.countryRender = function(item) {
        return {
            label: item.countryName,
            item: item
        }
    };


    $scope.getCurrencyById = function(currencyId) {
        console.log("getCurrencyById is called", currencyId);


        CurrencyMasterView.get({
            id: currencyId
        }, function(data) {
            if (data.responseCode == "ERR0") {
                console.log("Successful while getting currency.", data)
                $scope.currencyMaster = data.responseObject;
            }
            $scope.setOldDataVal();

        }, function(error) {
            console.log("Error while getting unit.", error)
        });
    }




    $scope.$on('addCurrencyEvent', function(events, args) {

        $rootScope.unfinishedData = $scope.currencyMaster;
        $rootScope.category = "Currency Master";
        $rootScope.unfinishedFormTitle = "Currency (New)";

        if ($scope.currencyMaster != undefined && $scope.currencyMaster != null && $scope.currencyMaster.currencyName != undefined &&
            $scope.currencyMaster.currencyName != null && $scope.currencyMaster.currencyName != "") {
            $rootScope.subTitle = $scope.currencyMaster.currencyName;
        } else {
            $rootScope.subTitle = "Unknown Currency"
        }
    });


    $scope.$on('editCurrencyEvent', function(events, args) {

        $rootScope.unfinishedData = $scope.currencyMaster;
        $rootScope.category = "Currency Master";
        $rootScope.unfinishedFormTitle = "Currency Edit (" + $scope.currencyMaster.currencyCode + ")";

        if ($scope.currencyMaster != undefined && $scope.currencyMaster != null && $scope.currencyMaster.currencyName != undefined &&
            $scope.currencyMaster.currencyName != null && $scope.currencyMaster.currencyName != "") {
            $rootScope.subTitle = $scope.currencyMaster.currencyName;
        } else {
            $rootScope.subTitle = "Unknown Currency"
        }
    });


    $scope.$on('addCurrencyEventReload', function(e, confirmation) {
        confirmation.message = "";
        localStorage.reloadFormData = JSON.stringify($scope.currencyMaster);
        localStorage.isCurrencyReloaded = "YES";
        e.preventDefault();
    });


    $scope.$on('editCurrencyEventReload', function(e, confirmation) {
        confirmation.message = "";
        localStorage.reloadFormData = JSON.stringify($scope.currencyMaster);
        localStorage.isCurrencyReloaded = "YES";
        e.preventDefault();
    });


    $scope.isReloaded = localStorage.isCurrencyReloaded;

    if ($stateParams.action == "ADD") {
        if ($stateParams.fromHistory === 'Yes') {
            if ($scope.isReloaded == "YES") {
                console.log("Add History Reload...")
                $scope.isReloaded = "NO";
                localStorage.isCurrencyReloaded = "NO";
                $scope.currencyMaster = JSON.parse(localStorage.reloadFormData);
            } else {
                console.log("Add History NotReload...")
                $scope.currencyMaster = $rootScope.selectedUnfilledFormData;
            }
            $scope.oldData = "";
            $scope.init();
        } else {
            if ($scope.isReloaded == "YES") {
                $scope.isReloaded = "NO";
                localStorage.isCurrencyReloaded = "NO";
                $scope.currencyMaster = JSON.parse(localStorage.reloadFormData);
                console.log("Add NotHistory Reload...")
                $scope.oldData = "";
                $scope.init();
            } else {
                console.log("Add NotHistory NotReload...")
                $scope.init();
            }
        }
        $rootScope.breadcrumbArr = [{
                label: "Master",
                state: "layout.currency"
            },
            {
                label: "Currency",
                state: "layout.currency"
            },
            {
                label: "Add Currency",
                state: null
            }
        ];


    } else {
        if ($stateParams.fromHistory === 'Yes') {
            if ($scope.isReloaded == "YES") {
                $scope.isReloaded = "NO";
                localStorage.isCurrencyReloaded = "NO";
                $scope.currencyMaster = JSON.parse(localStorage.reloadFormData);
            } else {
                $scope.currencyMaster = $rootScope.selectedUnfilledFormData;
            }
            $scope.init();
            $scope.oldData = "";
        } else {
            if ($scope.isReloaded == "YES") {
                $scope.isReloaded = "NO";
                localStorage.isCurrencyReloaded = "NO";
                $scope.currencyMaster = JSON.parse(localStorage.reloadFormData);
                $scope.oldData = "";
            } else {
                $scope.getCurrencyById($stateParams.currencyId);
            }
            $scope.init();
        }
        $rootScope.breadcrumbArr = [{
                label: "Master",
                state: "layout.currency"
            },
            {
                label: "Currency",
                state: "layout.currency"
            },
            {
                label: "Edit Currency",
                state: null
            }
        ];
    }




}]);