/**
 * CurrencyMaster Factory which initiates all Rest calls using resource service
 */

(function() {
	
	
	app.factory("CurrencyMasterSearch",['$resource', function($resource) {
		return $resource("/api/v1/currencymaster/get/search", {}, {
			query : {
				method : 'POST'
			}
		});
	}]);
	
	app.factory("CurrencyMasterSearchKeyword",['$resource', function($resource) {
		return $resource("/api/v1/currencymaster/get/search/keyword", {}, {
			query : {
				method : 'POST'
			}
		});
	}]);

	app.factory("CurrencyMasterAdd",['$resource', function($resource) {
		return $resource("/api/v1/currencymaster/create", {}, {
			save : {
				method : 'POST'
			}
		});
	}]);

	app.factory("CurrencyMasterEdit",['$resource', function($resource) {
		return $resource("/api/v1/currencymaster/update", {}, {
			update : {
				method : 'POST'
			}
		});
	}]);

	app.factory("CurrencyMasterView",['$resource', function($resource) {
		return $resource("/api/v1/currencymaster/get/id/:id", {}, {
			get : {
				method : 'GET',
				params : {
					id : ''
				},
				isArray : false
			}
		});
	}]);

	app.factory("CurrencyMasterRemove",['$resource', function($resource) {
		return $resource("/api/v1/currencymaster/delete/:id", {}, {
			remove : {
				method : 'DELETE',
				params : {
					id : ''
				},
				isArray : false
			}
		});
	}]);

})();
