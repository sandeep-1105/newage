/**
 * Created by hmspl on 8/7/16.
 */
app.controller("essential_edit_ctrl", ['$rootScope', 'ServiceList', 'PortByTransportMode', 'EssentialChargeView', 'EssentialChargeAdd', 'EssentialChargeEdit',
    'TosSearchKeyword', 'ChargeSearchKeyword', 'cloneService',
    'discardService', '$scope', '$location', 'ngDialog', '$http',
    '$modal', '$state', '$stateParams', 'Notification',
    function($rootScope, ServiceList, PortByTransportMode, EssentialChargeView, EssentialChargeAdd, EssentialChargeEdit,
        TosSearchKeyword, ChargeSearchKeyword, cloneService,
        discardService, $scope, $location, ngDialog, $http,
        $modal, $state, $stateParams, Notification) {

        $scope.init = function() {
            console.log("init method called.....");
            $scope.page = 0;
            $scope.limit = 10;
            if ($scope.essentialChargeMaster == undefined ||
                $scope.essentialChargeMaster == null) {
                $scope.essentialChargeDataList = [];
                $scope.essentialChargeDataList.push({});
            } else {

            }

        }

        $scope.cancel = function() {
            var isDirty = $scope.essentialChargeForm.$dirty;
            if (isDirty) {
                var newScope = $scope.$new();
                newScope.errorMessage = $rootScope.nls["ERR200"];
                ngDialog.openConfirm({
                    template: '<p>{{errorMessage}}</p>' +
                        '<div class="ngdialog-footer">' +
                        ' <button type="button" class="btn btn-default btn-property cancel-btn" ng-click="confirm(2)">No</button>' +
                        '<button type="button" class="btn btn-default btn-property accent-btn " ng-click="confirm(1)">Yes</button>' +
                        '<button type="button" class="btn btn-default btn-property pull-left grey-cancel-btn" ng-click="confirm(3)">Cancel</button>' +
                        '</div>',
                    plain: true,
                    scope: newScope,
                    className: 'ngdialog-theme-default'
                }).then(function(value) {
                    if (value == 1 && $scope.essentialChargeDataList[0].id == null) {
                        $scope.saveEssentialCharge();
                    } else if (value == 1 && $scope.essentialChargeMaster.id != null) {
                        $scope.updateEssentialCharge();
                    } else if (value == 2) {
                        var params = {};
                        params.submitAction = 'Cancelled';
                        $state.go('layout.essentialList', params);
                    } else {
                        console.log("cancelled");
                    }
                });
            } else {
                var params = {};
                params.submitAction = 'Cancelled';
                $state.go('layout.essentialList', params);
            }
        }




        $scope.addRow = function() {
            console.log("addRow is clicked");
            $scope.firstFocus = true;
            $scope.errorArray = [];
            var listErrorFlag = false;
            if ($scope.essentialChargeDataList != undefined && $scope.essentialChargeDataList != null && $scope.essentialChargeDataList.length > 0) {
                for (var index = 0; index < $scope.essentialChargeDataList.length; index++) {
                    if ($scope.validateEssential(index)) {
                        listErrorFlag = true;
                    }
                }
                if (listErrorFlag) {
                    // $scope.carrierRateDataList.push({});
                } else {
                    $scope.essentialChargeDataList.push({});
                }
            } else {
                $scope.essentialChargeDataList.push({});
            }
        };

        // Save


        $scope.saveEssentialCharge = function() {
            console.log("Save EssentialCharge method is called.", $scope.essentialChargeDataList);
            $scope.errorArray = [];
            var listErrorFlag = false;

            if ($scope.essentialChargeDataList != undefined && $scope.essentialChargeDataList != null && $scope.essentialChargeDataList.length > 0) {
                for (var index = 0; index < $scope.essentialChargeDataList.length; index++) {
                    if (index == 0) {
                        if ($scope.validateEssential(index)) {
                            listErrorFlag = true;
                        }
                    } else {
                        if ($scope.isEmptyRow($scope.essentialChargeDataList[index])) {
                            $scope.essentialChargeDataList.splice(index, 1);
                            listErrorFlag = false;
                        } else {
                            if ($scope.validateEssential(index)) {
                                listErrorFlag = true;
                            }
                        }
                    }
                }
                if (listErrorFlag) {
                    return;
                }
            }
            EssentialChargeAdd.save($scope.essentialChargeDataList).$promise.then(function(data) {
                if (data.responseCode == 'ERR0') {
                    console.log("EssentialCharge Saved successfully..");
                    var params = {};
                    params.submitAction = 'Saved';
                    Notification.success($rootScope.nls["ERR400"]);
                    $state.go("layout.essentialList", params);
                } else if (data.responseCode == 'ERR05119') {
                    console.log("EssentialCharge already exists");
                    Notification.error($rootScope.nls["ERR05119"]);
                } else {
                    console.log("EssentialCharge Saving Failed " + data.responseDescription)
                }
            }, function(error) {
                console.log("EssentialChargeMaster Saving Failed : " + error)
            });

        }

        // Update

        $scope.updateEssentialCharge = function() {
            console.log("Update Method is called.");
            $scope.essentialChargeMaster = $scope.essentialChargeDataList[0];

            $scope.errorArray = [];
            var listErrorFlag = false;

            if ($scope.essentialChargeDataList != undefined && $scope.essentialChargeDataList != null && $scope.essentialChargeDataList.length > 0) {
                for (var index = 0; index < $scope.essentialChargeDataList.length; index++) {
                    if (index == 0) {
                        if ($scope.validateEssential(index)) {
                            listErrorFlag = true;
                        }
                    } else {
                        if ($scope.isEmptyRow($scope.essentialChargeDataList[index])) {
                            $scope.essentialChargeDataList.splice(index, 1);
                            listErrorFlag = false;
                        } else {
                            if ($scope.validateEssential(index)) {
                                listErrorFlag = true;
                            }
                        }
                    }
                }
                if (listErrorFlag) {
                    return;
                }
            }

            EssentialChargeEdit
                .update($scope.essentialChargeMaster).$promise
                .then(
                    function(data) {
                        if (data.responseCode == 'ERR0') {
                            console
                                .log("EssentialCharge updated Successfully")

                            var params = {}
                            Notification.success($rootScope.nls["ERR401"]);
                            params.submitAction = 'Saved';
                            $state
                                .go(
                                    "layout.essentialList",
                                    params);
                        } else if (data.responseCode == 'ERR05119') {
                            console.log("EssentialCharge already exists");
                            Notification.error($rootScope.nls["ERR05119"]);
                        } else {
                            console
                                .log(
                                    "EssentialCharge updated Failed ",
                                    data.responseDescription)
                        }
                        angular.element(".panel-body")
                            .animate({
                                scrollTop: 0
                            }, "slow");
                    },
                    function(error) {
                        console
                            .log(
                                "EssentialCharge updation Failed : ",
                                error);
                    });

        }

        $scope.removeEssential = function(index) {
            console.log("remove Essential Rate is called ", index);
            //$scope.isDirty = $scope.essentialChargeForm.$dirty;
            // !$scope.isEmptyRow($scope.essentialChargeDataList[index])
            var newScope = $scope.$new();
            newScope.errorMessage = $rootScope.nls["ERR203"];
            newScope.errorMessage = newScope.errorMessage.replace("%s", " the selected essential charge");
            ngDialog.openConfirm({
                template: '<p>{{errorMessage}}</p>' +
                    '<div class="ngdialog-footer">' +
                    ' <button type="button" class="btn btn-default btn-property cancel-btn" ng-click="confirm(2)">No</button>' +
                    '<button type="button" class="btn btn-default btn-property accent-btn " ng-click="confirm(1)">Yes</button>' +
                    '</div>',
                plain: true,
                closeByDocument: false,
                scope: newScope,
                className: 'ngdialog-theme-default'
            }).then(function(value) {
                if (value == 1) {
                    $scope.essentialChargeDataList.splice(index, 1);
                } else if (value == 2) {
                    console.log("You selected No");
                }
            });
        };

        $scope.isEmptyRow = function(obj) {
            //return (Object.getOwnPropertyNames(obj).length === 0);
            var isempty = true; //  empty
            if (!obj) {
                return isempty;
            }
            var k = Object.getOwnPropertyNames(obj);
            for (var i = 0; i < k.length; i++) {
                if (obj[k[i]]) {
                    isempty = false; // not empty
                    break;
                }
            }
            return isempty;
        }



        // List Pickers

        // To display the pod
        $scope.ajaxOriginEvent = function(object) {
                console.log("Displaying List of Ports....");
                console.log("$scope.selectedPolName : " +
                    $scope.selectedPolName);
                $scope.searchDto = {};
                $scope.searchDto.keyword = object == null ? "" : object;
                $scope.searchDto.selectedPageNumber = 0;
                $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

                return PortByTransportMode.fetch({
                        "transportMode": 'Air'
                    }, $scope.searchDto).$promise
                    .then(
                        function(data) {
                            if (data.responseCode == "ERR0") {
                                var resultList = data.responseObject.searchResult;
                                console
                                    .log(
                                        "List of Ports before removing : ",
                                        resultList);
                                if (resultList != null &&
                                    resultList.length != 0) {

                                    /*
                                     * for (var int = 0; int <
                                     * resultList.length; int++) {
                                     * if
                                     * ($scope.selectedPolName !=
                                     * undefined &&
                                     * $scope.selectedPolName !=
                                     * null &&
                                     * $scope.selectedPolName !=
                                     * "") {
                                     * if(resultList[int].portName ==
                                     * $scope.selectedPolName){
                                     * 
                                     * var index = int;
                                     * resultList.splice(index,
                                     * 1);
                                     *  }
                                     *  }
                                     *  }
                                     */
                                    console
                                        .log(
                                            "List of Ports after removing : ",
                                            resultList);
                                    $scope.originList = resultList;
                                }
                                return $scope.originList;
                            }
                        },
                        function(errResponse) {
                            console
                                .error('Error while fetching origin List');
                        });

            }
            // pod end

        // To display the Destination
        $scope.ajaxDestinationEvent = function(object) {
                console.log("Displaying List of Ports....");
                console.log("$scope.selectedPolName : " +
                    $scope.selectedPolName);
                $scope.searchDto = {};
                $scope.searchDto.keyword = object == null ? "" : object;
                $scope.searchDto.selectedPageNumber = 0;
                $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

                return PortByTransportMode.fetch({
                        "transportMode": 'Air'
                    }, $scope.searchDto).$promise
                    .then(
                        function(data) {
                            if (data.responseCode == "ERR0") {
                                var resultList = data.responseObject.searchResult;
                                // console.log("List of Ports
                                // before removing : ",
                                // resultList);
                                if (resultList != null &&
                                    resultList.length != 0) {

                                    /*
                                     * for (var int = 0; int <
                                     * resultList.length; int++) {
                                     * if
                                     * ($scope.selectedPolName !=
                                     * undefined &&
                                     * $scope.selectedPolName !=
                                     * null &&
                                     * $scope.selectedPolName !=
                                     * "") {
                                     * if(resultList[int].portName ==
                                     * $scope.selectedPolName){
                                     * 
                                     * var index = int;
                                     * resultList.splice(index,
                                     * 1);
                                     *  }
                                     *  }
                                     *  }
                                     */
                                    console
                                        .log(
                                            "List of Ports after removing : ",
                                            resultList);
                                    $scope.destinationList = resultList;
                                }
                                return $scope.destinationList;
                            }
                        },
                        function(errResponse) {
                            console
                                .error('Error while fetching destinations List');
                        });

            }
            // pod end

        $scope.ajaxServiceEvent = function(object) {

            $scope.searchDto = {};
            $scope.searchDto.keyword = object == null ? "" : object;
            $scope.searchDto.selectedPageNumber = 0;
            $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
            return ServiceList.fetch($scope.searchDto).$promise
                .then(
                    function(data) {
                        if (data.responseCode == "ERR0") {
                            $scope.serviceList = data.responseObject.searchResult;
                            console.log("Service List ",
                                $scope.serviceList)
                            return $scope.serviceList;
                        }
                    },
                    function(errResponse) {
                        console
                            .error('Error while fetching  Services');
                    });

        }

        $scope.ajaxChargeEvent = function(object) {

            console.log("ajaxChargeEvent ", object);

            $scope.searchDto = {};
            $scope.searchDto.keyword = object == null ? "" : object;
            $scope.searchDto.selectedPageNumber = 0;
            $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

            return ChargeSearchKeyword.query($scope.searchDto).$promise
                .then(
                    function(data, status) {
                        if (data.responseCode == "ERR0") {
                            $scope.totalRecord = data.responseObject.totalRecord;
                            $scope.chargeMasterList = data.responseObject.searchResult;
                            console.log("", $scope.chargeMasterList);
                            return $scope.chargeMasterList;

                        }
                    },
                    function(errResponse) {
                        console
                            .error('Error while fetching Charge');
                    });
        }

        $scope.selectedChargeMaster = function(obj) {

        }

        $scope.ajaxTosEvent = function(object) {
            console.log("ajaxTosEvent ", object);

            $scope.searchDto = {};
            $scope.searchDto.keyword = object == null ? "" : object;
            $scope.searchDto.selectedPageNumber = 0;
            $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

            return TosSearchKeyword.fetch($scope.searchDto).$promise
                .then(
                    function(data, status) {
                        if (data.responseCode == "ERR0") {
                            $scope.tosList = data.responseObject.searchResult;
                            // $scope.showTosList=true;
                            return $scope.tosList;
                        }
                    },
                    function(errResponse) {
                        console
                            .error('Error while fetching Tos');
                    });

        }

        $scope.selectedService = function(navigateTo, service) {

            $rootScope.navigateToNextField(navigateTo)
        }

        $scope.selectedCharge = function(navigateTo, data) {

            $rootScope.navigateToNextField(navigateTo)
        }

        $scope.selectedCrn = function(navigateTo, data) {

            $rootScope.navigateToNextField(navigateTo)
        }

        $scope.selectedOrigin = function(navigateTo, data) {

            $rootScope.navigateToNextField(navigateTo)
        }

        $scope.selectedDestination = function(navigateTo, data) {
            $rootScope.navigateToNextField(navigateTo)

        }

        $scope.selectedTos = function(navigateTo, data) {
            $rootScope.navigateToNextField(navigateTo)
        }

        $scope.selectedPpcc = function(navigateTo, data) {
            $rootScope.navigateToNextField(navigateTo)
        }

        $scope.selectedRouted = function(navigateTo, data) {

            $rootScope.navigateToNextField(navigateTo)
        }

        $scope.selectedHazardous = function(navigateTo, data) {
            $scope.addRow();
            $rootScope.navigateToNextField(navigateTo)
        }


        $scope.chargeRender = function(item) {
            return {
                label: item.chargeName,
                item: item
            }
        }

        $scope.tosRender = function(item) {
            return {
                label: item.tosName,
                item: item
            }
        }

        $scope.destinationRender = function(item) {
            return {
                label: item.portName,
                item: item
            }
        }

        $scope.originRender = function(item) {
            return {
                label: item.portName,
                item: item
            }
        }

        $scope.serviceRender = function(item) {
            return {
                label: item.serviceName,
                item: item
            }
        }




        // Validations start

        //show errors

        var myOtherModal = $modal({
            scope: $scope,
            templateUrl: 'errorPopUp.html',
            show: false,
            keyboard: true
        });

        var errorOnRowIndex = null;

        $scope.errorShow = function(errorObj, index) {
            $scope.errList = errorObj.errTextArr;
            errorOnRowIndex = index;
            myOtherModal.$promise.then(myOtherModal.show);
        };


        $scope.validateEssential = function(index) {
            console.log("validateEssential", index);

            $scope.errorArray[index] = {};
            $scope.errorArray[index].errTextArr = [];
            var errorFound = false;


            // Service validation
            if ($scope.essentialChargeDataList[index].serviceMaster == undefined ||
                $scope.essentialChargeDataList[index].serviceMaster == null ||
                $scope.essentialChargeDataList[index].serviceMaster == "") {
                $scope.errorArray[index].errRow = true;
                $scope.errorArray[index].serviceName = true;
                $scope.errorArray[index].errTextArr.push($rootScope.nls["ERR05100"]);
                errorFound = true;

            } else if ($scope.essentialChargeDataList[index].serviceMaster.status == "Block") {
                $scope.errorArray[index].errRow = true;
                $scope.errorArray[index].serviceName = true;
                $scope.errorArray[index].errTextArr.push($rootScope.nls["ERR05101"]);
                errorFound = true;
            } else if ($scope.essentialChargeDataList[index].serviceMaster.status == "Hide") {
                $scope.errorArray[index].errRow = true;
                $scope.errorArray[index].serviceName = true;
                $scope.errorArray[index].errTextArr.push($rootScope.nls["ERR05102"]);
                errorFound = true;
            }

            // Crn validation
            if ($scope.essentialChargeDataList[index].crn == undefined ||
                $scope.essentialChargeDataList[index].crn == null ||
                $scope.essentialChargeDataList[index].crn == "") {
                $scope.errorArray[index].errRow = true;
                $scope.errorArray[index].crn = true;
                $scope.errorArray[index].errTextArr.push($rootScope.nls["ERR05106"]);
                errorFound = true;
            }

            // charge validation
            if ($scope.essentialChargeDataList[index].chargeMaster == undefined ||
                $scope.essentialChargeDataList[index].chargeMaster == null ||
                $scope.essentialChargeDataList[index].chargeMaster == "") {
                $scope.errorArray[index].errRow = true;
                $scope.errorArray[index].chargeName = true;
                $scope.errorArray[index].errTextArr.push($rootScope.nls["ERR05103"]);
                errorFound = true;
            } else if ($scope.essentialChargeDataList[index].chargeMaster.status == "Block") {
                $scope.errorArray[index].errRow = true;
                $scope.errorArray[index].chargeName = true;
                $scope.errorArray[index].errTextArr.push($rootScope.nls["ERR05104"]);
                errorFound = true;
            } else if ($scope.essentialChargeDataList[index].chargeMaster.status == "Hide") {
                $scope.errorArray[index].errRow = true;
                $scope.errorArray[index].chargeName = true;
                $scope.errorArray[index].errTextArr.push($rootScope.nls["ERR05105"]);
                errorFound = true;
            }


            // Origin validation
            if ($scope.essentialChargeDataList[index].origin != undefined &&
                $scope.essentialChargeDataList[index].origin != null &&
                $scope.essentialChargeDataList[index].origin != "") {

                if ($scope.essentialChargeDataList[index].origin.status == "Block") {
                    $scope.errorArray[index].errRow = true;
                    $scope.errorArray[index].origin = true;
                    $scope.errorArray[index].errTextArr.push($rootScope.nls["ERR05111"]);
                    errorFound = true;
                } else if ($scope.essentialChargeDataList[index].origin.status == "Hide") {
                    /*$scope.errorMap.put("origin",
                    		$rootScope.nls["ERR05112"]);*/
                    $scope.errorArray[index].errRow = true;
                    $scope.errorArray[index].origin = true;
                    $scope.errorArray[index].errTextArr.push($rootScope.nls["ERR05112"]);
                    errorFound = true;
                }


            }

            // Destination validation
            if ($scope.essentialChargeDataList[index].destination != undefined &&
                $scope.essentialChargeDataList[index].destination != null &&
                $scope.essentialChargeDataList[index].destination != "") {

                if ($scope.essentialChargeDataList[index].destination.status == "Block") {
                    $scope.errorArray[index].errRow = true;
                    $scope.errorArray[index].destination = true;
                    $scope.errorArray[index].errTextArr.push($rootScope.nls["ERR05113"]);
                    errorFound = true;
                } else if ($scope.essentialChargeDataList[index].destination.status == "Hide") {
                    $scope.errorArray[index].errRow = true;
                    $scope.errorArray[index].destination = true;
                    $scope.errorArray[index].errTextArr.push($rootScope.nls["ERR05114"]);
                    errorFound = true;
                } else if ($scope.essentialChargeDataList[index].destination.id === $scope.essentialChargeDataList[index].origin.id) {
                    $scope.errorArray[index].errRow = true;
                    $scope.errorArray[index].destination = true;
                    $scope.errorArray[index].origin = true;
                    $scope.errorArray[index].errTextArr.push($rootScope.nls["ERR05118"]);
                    errorFound = true;
                }

            }



            // tosMaster validation
            if ($scope.essentialChargeDataList[index].tosMaster != undefined &&
                $scope.essentialChargeDataList[index].tosMaster != null &&
                $scope.essentialChargeDataList[index].tosMaster != "") {

                if ($scope.essentialChargeDataList[index].tosMaster.status == "Block") {
                    $scope.errorArray[index].errRow = true;
                    $scope.errorArray[index].tosName = true;
                    $scope.errorArray[index].errTextArr.push($rootScope.nls["ERR05109"]);
                    errorFound = true;
                } else if ($scope.essentialChargeDataList[index].tosMaster.status == "Hide") {
                    $scope.errorArray[index].errRow = true;
                    $scope.errorArray[index].tosName = true;
                    $scope.errorArray[index].errTextArr.push($rootScope.nls["ERR05110"]);
                    errorFound = true;
                }
            }

            return errorFound;

        }

        $scope.validateEssentialCharge = function(validateCode) {
            console.log("validateEssentialCharge", validateCode);

            $scope.errorMap = new Map();

            if (validateCode == 0 || validateCode == 1) {

                // unit validation
                if ($scope.essentialChargeMaster.serviceMaster == undefined ||
                    $scope.essentialChargeMaster.serviceMaster == null ||
                    $scope.essentialChargeMaster.serviceMaster == "") {
                    $scope.errorMap.put("serviceName",
                        $rootScope.nls["ERR05100"]);
                    return false;
                } else if ($scope.essentialChargeMaster.serviceMaster.status == "Block") {
                    $scope.errorMap.put("serviceName",
                        $rootScope.nls["ERR05101"]);
                    return false;
                } else if ($scope.essentialChargeMaster.serviceMaster.status == "Hide") {
                    $scope.errorMap.put("serviceName",
                        $rootScope.nls["ERR05102"]);
                    return false;
                }

            }

            if (validateCode == 0 || validateCode == 2) {

                // charge validation
                if ($scope.essentialChargeMaster.chargeMaster == undefined ||
                    $scope.essentialChargeMaster.chargeMaster == null ||
                    $scope.essentialChargeMaster.chargeMaster == "") {
                    $scope.errorMap.put("chargeName",
                        $rootScope.nls["ERR05103"]);
                    return false;
                } else if ($scope.essentialChargeMaster.chargeMaster.status == "Block") {
                    $scope.errorMap.put("chargeName",
                        $rootScope.nls["ERR05104"]);
                    return false;
                } else if ($scope.essentialChargeMaster.chargeMaster.status == "Hide") {
                    $scope.errorMap.put("chargeName",
                        $rootScope.nls["ERR05105"]);
                    return false;
                }

            }


            if (validateCode == 0 || validateCode == 3) {

                // Crn validation
                if ($scope.essentialChargeMaster.crn == undefined ||
                    $scope.essentialChargeMaster.crn == null ||
                    $scope.essentialChargeMaster.crn == "") {
                    $scope.errorMap.put("crn",
                        $rootScope.nls["ERR05106"]);
                    return false;
                }

            }


            if (validateCode == 0 || validateCode == 4) {

                // unit validation
                if ($scope.essentialChargeMaster.origin != undefined &&
                    $scope.essentialChargeMaster.origin != null &&
                    $scope.essentialChargeMaster.origin != "") {

                    if ($scope.essentialChargeMaster.origin.status == "Block") {
                        $scope.errorMap.put("origin",
                            $rootScope.nls["ERR05111"]);
                        return false;
                    } else if ($scope.essentialChargeMaster.origin.status == "Hide") {
                        $scope.errorMap.put("origin",
                            $rootScope.nls["ERR05112"]);
                        return false;
                    }


                }

            }

            if (validateCode == 0 || validateCode == 5) {

                // unit validation
                if ($scope.essentialChargeMaster.destination != undefined &&
                    $scope.essentialChargeMaster.destination != null &&
                    $scope.essentialChargeMaster.destination != "") {

                    if ($scope.essentialChargeMaster.destination.status == "Block") {
                        $scope.errorMap.put("destination",
                            $rootScope.nls["ERR05113"]);
                        return false;
                    } else if ($scope.essentialChargeMaster.destination.status == "Hide") {
                        $scope.errorMap.put("destination",
                            $rootScope.nls["ERR05114"]);
                        return false;
                    }
                }

            }

            if (validateCode == 0 || validateCode == 6) {
                // unit validation
                if ($scope.essentialChargeMaster.tosMaster != undefined &&
                    $scope.essentialChargeMaster.tosMaster != null &&
                    $scope.essentialChargeMaster.tosMaster != "") {

                    if ($scope.essentialChargeMaster.destination.status == "Block") {
                        $scope.errorMap.put("tosName",
                            $rootScope.nls["ERR05109"]);
                        return false;
                    } else if ($scope.essentialChargeMaster.destination.status == "Hide") {
                        $scope.errorMap.put("tosName",
                            $rootScope.nls["ERR05110"]);
                        return false;
                    }
                }
            }
            return true;
        }


        // History start here...........

        $scope.getEssentialChargeMastrById = function(id) {
            console.log("getEssentialChargeMasterById ", id);
            EssentialChargeView.get({
                    id: id
                },
                function(data) {
                    if (data.responseCode == "ERR0") {
                        console.log("Successful while getting EssentialChargeMaster.", data)
                        $scope.essentialChargeMaster = data.responseObject;
                        $scope.essentialChargeDataList = [];
                        $scope.essentialChargeDataList.push($scope.essentialChargeMaster);
                    }
                },
                function(error) {
                    console.log("Error while getting EssentialChargeMaster.", error)
                });

        }

        $scope.$on('essentialChargeAddEvent', function(events, args) {
            $rootScope.unfinishedData = $scope.essentialChargeDataList;
            $rootScope.category = "Essential Charge";
            $rootScope.unfinishedFormTitle = "Essential Charge (New)";
            if ($scope.essentialChargeDataList != undefined &&
                $scope.essentialChargeDataList[0].serviceMaster != null &&
                $scope.essentialChargeDataList[0].serviceMaster.serviceName != undefined &&
                $scope.essentialChargeDataList[0].serviceMaster.serviceName != null &&
                $scope.essentialChargeDataList[0].serviceMaster.serviceName != "") {
                $rootScope.subTitle = $scope.essentialChargeDataList[0].serviceMaster.serviceName;
            } else {
                $rootScope.subTitle = "Unknown Essential Charge"
            }
        })

        $scope.$on('essentialChargeEditEvent', function(events, args) {
            $rootScope.unfinishedData = $scope.essentialChargeDataList;
            $rootScope.category = "Essential Charge";
            $rootScope.unfinishedFormTitle = "Essential Charge Edit (" + $scope.essentialChargeMaster.serviceMaster.serviceCode + ")";
            $rootScope.subTitle = $scope.essentialChargeMaster.serviceMaster.serviceName;
        })

        $scope.$on('essentialChargeAddEventReload', function(e, confirmation) {
            confirmation.message = "";
            localStorage.reloadFormData = JSON.stringify($scope.essentialChargeDataList);
            localStorage.isEssentialChargeMasterReloaded = "YES";
            e.preventDefault();
        });

        $scope.$on('essentialChargeEditEventReload', function(e, confirmation) {
            confirmation.message = "";
            localStorage.reloadFormData = JSON.stringify($scope.essentialChargeDataList);
            localStorage.isEssentialChargeMasterReloaded = "YES";
            e.preventDefault();
        });

        $scope.isReloaded = localStorage.isEssentialChargeMasterReloaded;

        if ($stateParams.action == "ADD") {
            if ($stateParams.fromHistory === 'Yes') {
                if ($scope.isReloaded == "YES") {
                    console.log("Add History Reload...")
                    $scope.isReloaded = "NO";
                    localStorage.isEssentialChargeMasterReloaded = "NO";
                    $scope.essentialChargeDataList = JSON.parse(localStorage.reloadFormData);
                } else {
                    console.log("Add History NotReload...")
                    $scope.essentialChargeDataList = $rootScope.selectedUnfilledFormData;
                }
            } else {
                if ($scope.isReloaded == "YES") {
                    $scope.isReloaded = "NO";
                    localStorage.isEssentialChargeMasterReloaded = "NO";
                    $scope.essentialChargeDataList = JSON.parse(localStorage.reloadFormData);
                    console.log("Add NotHistory Reload...")
                } else {
                    console.log("Add NotHistory NotReload...")
                    $scope.init();
                }
            }
            $rootScope.breadcrumbArr = [{
                label: "Master",
                state: "layout.essentialList"
            }, {
                label: "Essential Charge",
                state: "layout.essentialList"
            }, {
                label: "Add Essential Charge",
                state: null
            }];
        } else {
            if ($stateParams.fromHistory === 'Yes') {
                if ($scope.isReloaded == "YES") {
                    $scope.isReloaded = "NO";
                    localStorage.isEssentialChargeMasterReloaded = "NO";
                    $scope.essentialChargeDataList = JSON.parse(localStorage.reloadFormData);
                } else {
                    $scope.essentialChargeDataList = $rootScope.selectedUnfilledFormData;
                }
            } else {
                if ($scope.isReloaded == "YES") {
                    $scope.isReloaded = "NO";
                    localStorage.isEssentialChargeMasterReloaded = "NO";
                    $scope.essentialChargeDataList = JSON.parse(localStorage.reloadFormData);
                } else {
                    $scope.getEssentialChargeMastrById($stateParams.id);
                    $scope.init();
                }
            }
            $rootScope.breadcrumbArr = [{
                label: "Master",
                state: "layout.essentialList"
            }, {
                label: "Essential Charge",
                state: "layout.essentialList"
            }, {
                label: "Edit Essential Charge",
                state: null
            }];
        }

    }
]);