(function() {
	
		app.factory("EssentialChargeSearch",['$resource', function($resource) {
			return $resource("/api/v1/essentialcharge/get/search", {}, {
				query : {
					method : 'POST'
				}
			});
		}]);

		app.factory("EssentialChargeList",['$resource', function($resource) {
			return $resource("/api/v1/essentialcharge/get/search/keyword", {}, {
				query : {
					method : 'POST'
				}
			});
		}]);
		
		app.factory("EssentialChargeView",['$resource', function($resource) {
			return $resource("/api/v1/essentialcharge/get/id/:id", {}, {
				get : {
					method : 'GET',
					params : {
						id : ''
					},
					isArray : false
				}
			});
		}]);
		
		
		app.factory("EssentialChargesByService",['$resource', function($resource) {
			return $resource("/api/v1/essentialcharge/get/serviceId/:serviceId", {}, {
				get : {
					method : 'GET',
					params : {
						serviceId : ''
					},
					isArray : false
				}
			});
		}]);

		app.factory("EssentialChargeAdd",['$resource', function($resource) {
			return $resource("/api/v1/essentialcharge/create", {}, {
				save : {
					method : 'POST'
				}
			});
		}]);
		
		app.factory("EssentialChargeRemove",['$resource', function($resource) {
			return $resource("/api/v1/essentialcharge/delete/:id", {}, {
				remove : {
					method : 'DELETE',
					params : {
						id : ''
					},
					isArray : false
				}
			});
		}]);
		
		app.factory("EssentialChargeEdit",['$resource', function($resource) {
			return $resource("/api/v1/essentialcharge/update", {}, {
			update : {
					method : 'POST'
				}
			});
		}]);

		app.factory("DefaultEssentialCharge",['$resource', function($resource) {
			return $resource("/api/v1/essentialcharge/get/essentialcharges", {}, {
				query : {
					method : 'POST'
				}
			});
		}]);
		
		
})();
