app.controller('essentialCtrl',['$rootScope', '$stateParams', 'EssentialChargeRemove', 'EssentialChargeView', 
        'RecentHistorySaveService', '$scope', '$http', '$location', '$window', 'ngTableParams', 'ngDialog', 
        'FeatureList', '$state', '$modal', 'EssentialChargeSearch', 'roleConstant', 'Notification',
    function($rootScope, $stateParams, EssentialChargeRemove, EssentialChargeView, 
        RecentHistorySaveService, $scope, $http, $location, $window, ngTableParams, ngDialog, 
        FeatureList, $state, $modal, EssentialChargeSearch, roleConstant, Notification) {


    $scope.searchDto = {};
    $scope.limitArr = [10, 15, 20];
    $scope.page = 0;
    $scope.limit = 10;

    $scope.essentialChargesHeadArr = [{
            "name": "#",
            "width": "w15px",
            "prefWidth": "15",
            "model": "no",
            "search": false
        },
        {
            "name": "Service",
            "width": "w125px",
            "prefWidth": "125",
            "model": "serviceMaster.serviceName",
            "search": true,
            "wrap_cell": true,
            "type": "text",
            "sort": true,
            "key": "serviceName"

        }, {
            "name": "Charge",
            "width": "w125px",
            "prefWidth": "125",
            "model": "chargeMaster.chargeName",
            "search": true,
            "sort": true,
            "type": "text",
            "key": "chargeName"
        },
        {
            "name": "Cost / Revenue",
            "width": "w100px",
            "prefWidth": "100",
            "model": "crn",
            "search": true,
            "sort": true,
            "data": $rootScope.enum['CRN'],
            "type": "drop",
            "key": "costOrRevenue"
        }, {
            "name": "Origin",
            "width": "w100px",
            "prefWidth": "100",
            "model": "origin.portName",
            "search": true,
            "sort": true,
            "type": "text",
            "key": "originName"
        },
        {
            "name": "Destination",
            "width": "w100px",
            "prefWidth": "100",
            "model": "destination.portName",
            "search": true,
            "sort": true,
            "type": "text",
            "key": "destinationName"
        },
        {
            "name": "TOS",
            "width": "w75px",
            "prefWidth": "75",
            "model": "tosMaster.tosName",
            "search": true,
            "sort": true,
            "type": "text",
            "key": "tosName"
        }, {
            "name": "Prepaid / Collect",
            "width": "w125px",
            "prefWidth": "125",
            "model": "ppcc",
            "data": $rootScope.enum['PPCC'],
            "search": true,
            "sort": true,
            "type": "drop",
            "key": "ppcc"
        }, {
            "name": "Routed",
            "width": "w100px",
            "prefWidth": "100",
            "model": "routed",
            "data": $rootScope.enum['WhoRouted'],
            "search": true,
            "sort": true,
            "type": "drop",
            "key": "routed"
        }, {
            "name": "Hazardous",
            "width": "w100px",
            "prefWidth": "100",
            "model": "hazardous",
            "data": $rootScope.enum['YesNo'],
            "search": true,
            "sort": true,
            "type": "drop",
            "key": "hazardous"
        }
    ];


    $scope.sortSelection = {
        sortKey: "serviceMaster.serviceName",
        sortOrder: "asc"
    };

    /*$scope.dataArr=[
        {
            no:1,
            service:"Air Export",
            charge:"Administration Fee",
            cr:"Cost",
            origin:"Chennai",
            dest:"Dubai",
            tos:"FOB",
            ppcc:"Prepaid",
            routed:"Yes",
            hazardous:"Yes"
        }
    ];*/


    $scope.init = function() {

        //essentialChargeListArr
        console.log("init called");

        $scope.search();


    }

    $scope.add = function() {

        $state.go('layout.essentialAdd', {
            action: 'ADD'
        });

    };
    $scope.showDetail = false;


    $scope.changepage = function(param) {
        console.log("called change page")
        $scope.page = param.page;
        $scope.limit = param.size;
        $scope.search();
    }

    $scope.currentTab = "address";
    $scope.activeTab = function(tab) {
        $scope.currentTab = tab;
    }

    $scope.addEssentialCharges = function() {
        if ($rootScope.roleAccess(roleConstant.MASTER_GENERAL_ESSENTIAL_CHARGE_CREATE)) {
            $state.go('layout.essentialChargeAdd');
        }
    }
    $scope.editEssentialCharge = function() {
        if ($rootScope.roleAccess(roleConstant.MASTER_GENERAL_ESSENTIAL_CHARGE_MODIFY)) {
            console.log("Edit editEssentialCharge Button is Pressed....")
            var param = {
                id: $scope.essentialChargeMaster.id,
            };
            console.log("State Parameters :: ", param);
            $state.go("layout.essentialChargeEdit", param);
        }
    }

    $scope.limitChange = function(item) {
        $scope.page = 0;
        $scope.limit = item;
        $scope.search();
    }

    // Delete

    $scope.deleteEssentialCharge = function() {
        if ($rootScope.roleAccess(roleConstant.MASTER_GENERAL_ESSENTIAL_CHARGE_DELETE)) {
            ngDialog.openConfirm({
                    template: '<p>Are you sure you want to delete selected essential charge?</p>' +
                        '<div class="ngdialog-footer">' +
                        '<button type="button" class="btn btn-default btn-property cancel-btn" ng-click="closeThisDialog(0)">Cancel' +
                        '<button type="button" class="btn btn-primary btn-property accent-btn" ng-click="confirm(1)">Yes' +
                        '</button></div>',
                    plain: true,
                    className: 'ngdialog-theme-default'
                })
                .then(function(value) {
                    EssentialChargeRemove.remove({
                        id: $scope.essentialChargeMaster.id
                    }, function(data) {
                        if (data.responseCode == 'ERR0') {
                            console.log("EssentialCharge deleted Successfully")
                            Notification.success($rootScope.nls["ERR402"]);
                            $state.go('layout.essentialList');
                        } else {
                            console.log("EssentialCharge deleted Failed " + data.responseDescription)
                        }
                    }, function(error) {
                        console.log("EssentialCharge deleted Failed : " + error)
                    });
                }, function(value) {
                    console.log("deleted cancelled");
                });
        }
    }


    $scope.sortChange = function(param) {
        $scope.searchDto.orderByType = param.sortOrder.toUpperCase();
        $scope.searchDto.sortByColumn = param.sortKey;
        $scope.search();
    };

    $scope.changeSearch = function(param) {

        console.log("Change Search is called ", param);

        for (var attrname in param) {
            $scope.searchDto[attrname] = param[attrname];
        }

        $scope.page = 0;
        $scope.search();
    }

    /*$scope.rowSelect = function(data, ind){
        
        var param = {
                essentialChargeId   : data.id,
                index: ind
        };
        $state.go("layout.essentialChargeView", param);
        
        */
    /************************************************************
     * ux - by Muthu
     * reason - ipad compatibility fix
     *
     * *********************************************************/
    /*

            var windowInner=$window.innerWidth;
            if(windowInner<=1199){
                $scope.deskTopView = false;
            }
            else{
                $scope.deskTopView = true;
            }
        }*/

    $scope.rowSelect = function(data, index) {
        if ($rootScope.roleAccess(roleConstant.MASTER_GENERAL_ESSENTIAL_CHARGE_VIEW)) {
            $scope.selectedRecordIndex = ($scope.searchDto.recordPerPage * $scope.searchDto.selectedPageNumber) + index;
            var localRefStateParam = {
                essentialChargeId: data.id,
                essnIdx: $scope.selectedRecordIndex,
                totalRecord: $scope.totalRecord
            };

            $state.go("layout.essentialChargeView", $scope.searchDtoToStateParams(localRefStateParam));

        }
    }

    $scope.searchDtoToStateParams = function(param) {
        if (param == undefined || param == null) {
            param = {};
        }
        if ($scope.searchDto != undefined && $scope.searchDto != null) {
            if ($scope.searchDto != undefined && $scope.searchDto != null) {
                //Page Details
                param.selectedPageNumber = param.essnIdx;
                param.recordPerPage = 1;
                //Sorting Details
                if ($scope.searchDto.sortByColumn != undefined && $scope.searchDto.sortByColumn != null) {
                    param.sortByColumn = $scope.searchDto.sortByColumn;
                }
                if ($scope.searchDto.orderByType != undefined && $scope.searchDto.orderByType != null) {
                    param.orderByType = $scope.searchDto.orderByType;
                }

                // location
                if ($scope.searchDto.chargeName != undefined && $scope.searchDto.chargeName != null) {
                    param.chargeName = $scope.searchDto.chargeName;
                }
                // serviceName
                if ($scope.searchDto.serviceName != undefined && $scope.searchDto.serviceName != null) {
                    param.serviceName = $scope.searchDto.serviceName;
                }
                // originName
                if ($scope.searchDto.originName != undefined && $scope.searchDto.originName != null) {
                    param.originName = $scope.searchDto.originName;
                }
                // destinationName
                if ($scope.searchDto.destinationName != undefined && $scope.searchDto.destinationName != null) {
                    param.destinationName = $scope.searchDto.destinationName;
                }

                // tosName
                if ($scope.searchDto.tosName != undefined && $scope.searchDto.tosName != null) {
                    param.tosName = $scope.searchDto.tosName;
                }
                // crn
                if ($scope.searchDto.crn != undefined && $scope.searchDto.crn != null) {
                    param.crn = $scope.searchDto.crn;
                }
                // ppcc;
                if ($scope.searchDto.ppcc != undefined && $scope.searchDto.ppcc != null) {
                    param.ppcc = $scope.searchDto.ppcc;
                }

                // routed;
                if ($scope.searchDto.routed != undefined && $scope.searchDto.routed != null) {
                    param.routed = $scope.searchDto.routed;
                }
                // hazardous;
                if ($scope.searchDto.hazardous != undefined && $scope.searchDto.hazardous != null) {
                    param.hazardous = $scope.searchDto.hazardous;
                }

            }

        }
        return param;
    }




    $scope.search = function() {

        $scope.searchDto.selectedPageNumber = $scope.page;
        $scope.searchDto.recordPerPage = $scope.limit;

        $scope.essentialChargeListArr = [];
        EssentialChargeSearch.query($scope.searchDto).$promise.then(function(
            data, status) {
            $scope.totalRecord = data.responseObject.totalRecord;
            console.log($scope.totalRecord);
            var tempArr = [];
            var resultArr = [];
            tempArr = data.responseObject.searchResult;
            console.log("tempArr", tempArr);
            var tempObj = {};
            angular.forEach(tempArr, function(item, index) {
                tempObj = item;
                tempObj.hazardous = tempObj.hazardous ? 'Yes' : 'No';
                tempObj.no = (index + 1) + ($scope.page * $scope.limit);
                resultArr.push(tempObj);
                tempObj = {};
            });
            $scope.essentialChargeListArr = resultArr;
            console.log('essentialChargeListArr', $scope.essentialChargeListArr);
        });
    }

    // Start page navigation 

    $scope.singlePageNavigation = function(val) {

        if ($stateParams != undefined && $stateParams != null) {

            if ((parseInt($stateParams.selectedPageNumber) + val) < 0 || (parseInt($stateParams.selectedPageNumber) + val) >= parseInt($stateParams.totalRecord))
                return;
            var stateParameters = {};
            $scope.searchDto = {};
            $scope.searchDto.recordPerPage = 1;
            $scope.hidePage = true;

            $scope.searchDto.selectedPageNumber = parseInt($stateParams.selectedPageNumber) + val;

            /*
            // submitAction
            if($stateParams.submitAction != undefined && $stateParams.submitAction != null) {
                stateParameters.submitAction = $scope.searchDto.submitAction = $stateParams.submitAction; 
            }*/

            // chargeName
            if ($stateParams.chargeName != undefined && $stateParams.chargeName != null) {
                stateParameters.chargeName = $scope.searchDto.chargeName = $stateParams.chargeName;
            }
            // service
            if ($stateParams.serviceName != undefined && $stateParams.serviceName != null) {
                stateParameters.serviceName = $scope.searchDto.serviceName = $stateParams.serviceName;
            }
            // originName
            if ($stateParams.originName != undefined && $stateParams.originName != null) {
                stateParameters.originName = $scope.searchDto.originName = $stateParams.originName;
            }

            // destinationName
            if ($stateParams.destinationName != undefined && $stateParams.destinationName != null) {
                stateParameters.destinationName = $scope.searchDto.destinationName = $stateParams.destinationName;
            }

            // tosName
            if ($stateParams.tosName != undefined && $stateParams.tosName != null) {
                stateParameters.tosName = $scope.searchDto.tosName = $stateParams.tosName;
            }
            // crn
            if ($stateParams.crn != undefined && $stateParams.crn != null) {
                stateParameters.crn = $scope.searchDto.crn = $stateParams.crn;
            }
            // ppcc
            if ($stateParams.ppcc != undefined && $stateParams.ppcc != null) {
                stateParameters.ppcc = $scope.searchDto.ppcc = $stateParams.ppcc;
            }
            // routed
            if ($stateParams.routed != undefined && $stateParams.routed != null) {
                stateParameters.routed = $scope.searchDto.routed = $stateParams.routed;
            }

            // hazardous
            if ($stateParams.hazardous != undefined && $stateParams.hazardous != null) {
                stateParameters.hazardous = $scope.searchDto.hazardous = $stateParams.hazardous;
            }

            if ($stateParams.sortByColumn != undefined && $stateParams.sortByColumn != null) {
                stateParameters.sortByColumn = $scope.searchDto.sortByColumn = $stateParams.sortByColumn;
            }
            if ($stateParams.orderByType != undefined && $stateParams.orderByType != null) {
                stateParameters.orderByType = $scope.searchDto.orderByType = $stateParams.orderByType;
            }
            if ($stateParams.recordPerPage != undefined && $stateParams.recordPerPage != null) {
                stateParameters.recordPerPage = $scope.searchDto.recordPerPage = $stateParams.recordPerPage;
            }
            if ($stateParams.searchFlag != undefined && $stateParams.searchFlag != null) {
                stateParameters.searchFlag = $scope.searchFlag = $stateParams.searchFlag;
            } else {
                //stateParameters.searchFlag = $scope.searchFlag = "active";
            }
        } else {
            return;
        }

        //$scope.searchDto.searchType='All';

        EssentialChargeSearch.query($scope.searchDto).$promise.then(function(data, status) {
            $scope.totalRecord = data.responseObject.totalRecord;
            stateParameters.essnIdx = stateParameters.selectedPageNumber = $scope.searchDto.selectedPageNumber;
            stateParameters.essentialChargeId = data.responseObject.searchResult[0].id;
            $state.go("layout.essentialChargeView", stateParameters);
        });
    }


    // End Page navigation

    $scope.back = function() {
        var params = {};
        params.submitAction = 'Cancelled';
        $state.go('layout.essentialList', params);
    }


    $scope.cancel = function() {
        $scope.detailPageFlag = false;
        $scope.showHistory = false;
        $state.go('layout.essentialList');

        /************************************************************
         * ux - by Muthu
         * reason - ipad compatibility fix
         *
         * *********************************************************/

        /********************* ux fix ends here *****************************/
    };

    $scope.view = function(essentialChargeId) {
        console.log("View Essential Charge Master is called ", essentialChargeId);

        EssentialChargeView.get({
            id: essentialChargeId
        }, function(data) {
            if (data.responseCode == "ERR0") {
                console.log("Essential Charge Successful");
                $scope.essentialChargeMaster = data.responseObject;
                $scope.showDetail = true;
                $rootScope.unfinishedFormTitle = "Essential Charge View # " + essentialChargeId;
                if ($scope.essentialChargeMaster != undefined && $scope.essentialChargeMaster.serviceMaster != null) {
                    $rootScope.subTitle = $scope.essentialChargeMaster.serviceMaster.serviceName;
                }
                // To display the view page
                var rHistoryObj = {
                    'title': 'Essential Charge View #' + $stateParams.id,
                    'subTitle': $rootScope.subTitle,
                    'stateName': $state.current.name,
                    'stateParam': JSON.stringify($stateParams),
                    'category': 'Essential Charge',
                    'serviceType': null
                }
                RecentHistorySaveService.form(rHistoryObj);
                console.log("Viewing Essential Charge Master ", $scope.essentialChargeMaster);
            } else {
                console.log("Essential Charge Master View Failed ", data.responseDescription)
            }
        }, function(error) {
            console.log("Essential Charge Master View Failed : ", error);
        });
    }

    switch ($stateParams.action) {
        case "VIEW":
            console.log("EssentialChargeMaster View Page", $stateParams.id, $stateParams.index);
            $rootScope.unfinishedData = undefined;
            $scope.showDetail = false;
            $scope.totalRecord = $stateParams.totalRecord;
            $scope.selectedRecordIndex = parseInt($stateParams.essnIdx);
            $scope.view($stateParams.essentialChargeId);
            $rootScope.breadcrumbArr = [{
                label: "Master",
                state: "layout.essentialList"
            }, {
                label: "Essential Charge",
                state: "layout.essentialList"
            }, {
                label: "View Essential Charge",
                state: null
            }];
            break;
        case "SEARCH":
            $scope.init();
            $rootScope.breadcrumbArr = [{
                label: "Master",
                state: "layout.essentialList"
            }, {
                label: "Essential Charge",
                state: null
            }];
            break;
        default:
    }

}]);