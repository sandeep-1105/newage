/**
 * 
 */
app.service('divsionService',['$rootScope', 'DivisionList', 'CompanyList',
    function($rootScope, DivisionList, CompanyList) {


    this.ajaxDivisionEvent = function(object) {
        var searchDto = {};
        searchDto.keyword = object == null ? "" : object;
        searchDto.selectedPageNumber = 0;
        searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
        return DivisionList.fetch(searchDto).$promise
            .then(
                function(data) {
                    if (data.responseCode == "ERR0") {
                        var ajaxDivisionEvent = []
                        divisionList = data.responseObject.searchResult;
                        return divisionList;
                    }
                },
                function(error) {
                    console
                        .error('Error while fetching division');
                });

    }

    this.ajaxCompanyEvent = function(object) {
        var searchDto = {}
        searchDto.keyword = object == null ? "" : object;
        searchDto.selectedPageNumber = 0;
        searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
        return CompanyList.fetch(searchDto).$promise.then(
            function(data) {
                if (data.responseCode == "ERR0") {
                    var companyList = [];
                    companyList = data.responseObject.searchResult;
                    return companyList;
                }
            },
            function(errResponse) {
                console.error('Error while fetching Company');
            });
    }


}]);