app.controller('DivisionMasterCtrl', function($rootScope, $scope, $window, $stateParams, $state, ngTableParams, ngDialog, 
		DivisionSearch, DivisionRemove, Notification, roleConstant) {

	$scope.deskTopView = true;
	
	$scope.title = "Division";
	
	$scope.divisionHeadArr = [ {
		"name" : "#",
		"width" : "col-xs-0half",
		"model" : "no",
		"search" : false,
	}, {
		"name" : "Name",
		"width" : "col-xs-3",
		"model" : "divisionName",
		"search" : true,
		"wrap_cell" : true,
		"type" : "text",
		"sort" : true,
		"key" : "searchDivisionName"
	}, {
		"name" : "Code",
		"width" : "col-xs-1",
		"model" : "divisionCode",
		"search" : true,
		"type" : "text",
		"sort" : true,
		"key" : "searchDivisionCode"
	}, {
		"name" : "Company",
		"width" : "col-xs-2",
		"model" : "companyMaster.companyName",
		"search" : true,
		"type" : "text",
		"sort" : true,
		"key" : "searchCompanyName"},
	{
		"name" : "Status",
		"width" : "col-xs-2",
		"model" : "status",
		"search" : true,
		"sort" : true,
		"type" : "drop",
		"data" : $rootScope.enum['LovStatus'],
		"key" : "searchStatus"
		}
	];
					
					
	$scope.sortSelection = {
			sortKey:"divisionName", sortOrder:"asc"
	}

	$scope.limitArr = [10, 15, 20];
	$scope.page = 0;
	$scope.limit = 10;
					

	$scope.init = function() {
		$scope.searchDto = {};
		$scope.search();
	};

	$scope.limitChange = function(item) {
		$scope.page = 0;
		$scope.limit = item;
		$scope.cancel();
		$scope.search();
	}
	
	$scope.rowSelect = function(data){
		if($rootScope.roleAccess(roleConstant.MASTER_GENERAL_DIVISION_VIEW)){
		$scope.divisionMaster = data;
		$scope.showHistory = false;
		$scope.detailView = true;
		var windowInner=$window.innerWidth;
		if(windowInner<=1199){
			$scope.deskTopView = false;
		} else {
			$scope.deskTopView = true;
		}
		angular.element($window).bind('resize', function(){
			if($window.innerWidth>=1200){
				$scope.deskTopView = true;
			} else if($window.innerWidth<=1199){
				if($scope.tosMaster.id!=null){
					$scope.deskTopView = false;
				} else {
					$scope.deskTopView = true;
				}
			}
			console.log("window resizing..." + $window.innerWidth);
			$scope.$digest();
		});
		}
	}
	
	$scope.changeSearch = function(param) {
						
		for (var attrname in param) {
			$scope.searchDto[attrname] = param[attrname];
		}
		$scope.page = 0;
		$scope.cancel();
		$scope.search();
		console.log("change search",$scope.searchDto);
	}

	$scope.sortChange = function(param) {
		$scope.searchDto.orderByType = param.sortOrder.toUpperCase();
		$scope.searchDto.sortByColumn = param.sortKey;
		$scope.cancel();
		$scope.search();
	}
	
	$scope.changepage = function(param) {
		$scope.page = param.page;
		$scope.limit = param.size;
		$scope.cancel();
		$scope.search();
	}
					
	$scope.search = function() {
		
		$scope.detailView = false;
		$scope.showHistory = false;
		
		$scope.searchDto.selectedPageNumber = $scope.page;
		$scope.searchDto.recordPerPage = $scope.limit;
		
		DivisionSearch.query($scope.searchDto).$promise.then(function(data, status) {
			$scope.totalRecord = data.responseObject.totalRecord;
			$scope.divisionDataArr = data.responseObject.searchResult;
			console.log($scope.divisionDataArr);
		});

	}

	$scope.cancel = function() {
		$scope.showHistory = false;
		$scope.divisionMaster = {};
		$scope.detailView = false;
		$scope.deskTopView = true;
	};

	$scope.addDivisionMaster = function() {
		if($rootScope.roleAccess(roleConstant.MASTER_GENERAL_DIVISION_CREATE)){
		$state.go("layout.divisionMasterAdd");
		}
	};
	
	$scope.editDivisionMaster = function() {
		if($rootScope.roleAccess(roleConstant.MASTER_GENERAL_DIVISION_MODIFY)){
		$state.go("layout.divisionMasterEdit", {id : $scope.divisionMaster.id});
		}
	};
	
	$scope.deleteDivisionMaster = function() {
		if($rootScope.roleAccess(roleConstant.MASTER_GENERAL_DIVISION_DELETE)){
		var newScope = $scope.$new();
		newScope.errorMessage = $rootScope.nls["ERR3315"];
		ngDialog.openConfirm( 
		{ template : 	'<p>{{errorMessage}}</p>' +
						'<div class="ngdialog-footer">' +
						'<button type="button" class="btn btn-default btn-property cancel-btn" ng-click="closeThisDialog(0)">No' +
						'<button type="button" class="btn btn-primary btn-property accent-btn" ng-click="confirm(1)">Yes' +
						'</button>' +
						'</div>',
						plain: true,
						scope: newScope,
						className: 'ngdialog-theme-default'
		}).then(function (value) {
        
			DivisionRemove.remove({ id : $scope.divisionMaster.id }, function(data) {
       			
				if (data.responseCode =="ERR0"){
					console.log("Division deleted Successfully");
					Notification.success($rootScope.nls["ERR402"]);
					$scope.cancel();
					$scope.init();
				} else {
					console.log("Division deleted Failed " , data.responseDescription)
       			}
       		}, function(error) {
       			console.log("Division deleted Failed : " , error)
       		});
           }, function (value) {
               console.log("Division deleted cancelled");
           });
		}
			
	}

					

	if($stateParams.action == "SEARCH") {
		$rootScope.breadcrumbArr = [ { label:"Master", state:"layout.divisionMaster"}, 
		                             { label:"Division", state:"layout.divisionMaster"}];
		$scope.init();
	}					
});
