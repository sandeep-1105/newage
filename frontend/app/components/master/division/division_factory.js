
(function() {
	app.factory("DivisionSearch",['$resource', function($resource) {
		return $resource("/api/v1/divisionmaster/get/search", {}, {
			query : {
				method : 'POST'
			}
		});
	}]);
	
	app.factory("SearchDivisionNotInList",['$resource', function($resource) {
		return $resource("/api/v1/divisionmaster/get/search/exclude", {}, {
			fetch : {
				method : 'POST'
			}
		});
	}]);
	
	app.factory("DivisionSave",['$resource', function($resource) {
		return $resource("/api/v1/divisionmaster/create", {}, {
			save : {
				method : 'POST'
			}
		});
	}]);
	
	app.factory("DivisionUpdate",['$resource', function($resource) {
		return $resource("/api/v1/divisionmaster/update", {}, {
			update : {
				method : 'POST'
			}
		});
	}]);
	
	
	app.factory("DivisionView",['$resource', function($resource) {
		return $resource("/api/v1/divisionmaster/get/id/:id", {}, {
			get : {
				method : 'GET',
				params : {
					id : ''
				},
				isArray : false
			}
		});
	}]);
	
	
	app.factory("DivisionRemove",['$resource', function($resource) {
		return $resource("/api/v1/divisionmaster/delete/:id", {}, {
			remove : {
				method : 'DELETE',
				params : {
					id : ''
				},
				isArray : false
			}
		});
	}]);
	

})();