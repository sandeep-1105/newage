app.controller('DivisionMasterOperationCtrl', function($rootScope, $scope, ngProgressFactory, ngDialog, $state, $stateParams, 
		DivisionView, DivisionSave, DivisionUpdate, divsionService, Notification) {

	$scope.init = function () {
		if($scope.divisionMaster == undefined || $scope.divisionMaster == null) {
			$scope.divisionMaster = {};
			$scope.divisionMaster.status='Active';
		}
		$scope.oldData = JSON.stringify($scope.divisionMaster);
	};

	$scope.cancel = function(id) {
		if ($scope.oldData !== JSON.stringify($scope.divisionMaster)) {
			var newScope = $scope.$new();
			newScope.errorMessage = $rootScope.nls["ERR200"];
			ngDialog.openConfirm({
				template: '<p>{{errorMessage}}</p>' +
				'<div class="ngdialog-footer">' +
					' <button type="button" class="btn btn-default btn-property cancel-btn" ng-click="confirm(2)">No</button>' +
					'<button type="button" class="btn btn-default btn-property accent-btn " ng-click="confirm(1)">Yes</button>' +
					'<button type="button" class="btn btn-default btn-property pull-left grey-cancel-btn" ng-click="confirm(3)">Cancel</button>' +
				'</div>',
				plain: true,
				scope: newScope,
				className: 'ngdialog-theme-default'
			}).then(function(value) {
				if (value == 1) {
					if(id==undefined||id==null){
						$scope.saveDivision();	
					}else{
						$scope.updateDivision();
					}
				} else if(value == 2) {
					$state.go("layout.divisionMaster", {submitAction : 'Cancelled'});
				}else{
					$log.debug("cancelled");
				}
			});
		} else {
			$state.go("layout.divisionMaster", {submitAction : 'Cancelled'});
		}
	};
	
	
	
	$scope.saveDivision = function() {
		if ($scope.validateDivisionMaster(0)) {
			DivisionSave.save($scope.divisionMaster).$promise.then(function(data) {
				if (data.responseCode == "ERR0") {
					console.log("City updated Successfully")
					Notification.success($rootScope.nls["ERR400"]);
					$state.go("layout.divisionMaster", {submitAction : 'Saved'});
				} else {
					console.log("DivisionMaster save Failed " , data.responseDescription);
				}
									
			},function(error) {
				console.log("DivisionMaster save Failed : ", error);
			});
		}	else {
			console.log("DivisionMaster Validation Failed");
		}					
					
	}
	
	$scope.updateDivision = function(){
		if ($scope.validateDivisionMaster(0)) {
			DivisionUpdate.update($scope.divisionMaster).$promise.then(function(data) {
				if (data.responseCode == "ERR0") {
					console.log("City updated Successfully")
					Notification.success($rootScope.nls["ERR401"]);
					$state.go("layout.divisionMaster", {submitAction : 'Saved'});
				} else {
					console.log("DivisionMaster updated Failed " , data.responseDescription);
				}
									
			},function(error) {
				console.log("DivisionMaster updated Failed : ", error);
			});
		}	else {
			console.log("DivisionMaster Validation Failed");
		}					
					
	}
   
	$scope.companyCall=function(){
	  return divsionService.ajaxCompanyEvent();
	}
	
	$scope.errorFocus = function(id){
		if(id!= undefined && id!= null){
			$rootScope.navigateToNextField(id);
		}
	}
	
	$scope.validateDivisionMaster = function(validateCode) {
		console.log("Validate Called " + validateCode);
		$scope.errorMap = new Map();
		if (validateCode == 0 || validateCode == 1) {
			if ($scope.divisionMaster.divisionName == undefined
					|| $scope.divisionMaster.divisionName == null
					|| $scope.divisionMaster.divisionName == "") {
				$scope.errorFocus('divisionName');
				$scope.errorMap.put("divisionName",$rootScope.nls["ERR3308"]);
				return false;
			}
		}
		if(validateCode == 0 || validateCode == 2){
			if ($scope.divisionMaster.divisionCode == undefined
					|| $scope.divisionMaster.divisionCode == null
					|| $scope.divisionMaster.divisionCode == "") {
				$scope.errorFocus('divisionCode');
				$scope.errorMap.put("divisionCode",$rootScope.nls["ERR3307"]);
				return false;
			}
		}
		if(validateCode == 0 || validateCode == 3){
			if ($scope.divisionMaster.companyMaster == undefined
					|| $scope.divisionMaster.companyMaster == null
					|| $scope.divisionMaster.companyMaster.companyName == "") {
				$scope.errorFocus('companyName');
				$scope.errorMap.put("companyName",$rootScope.nls["ERR3314"]);
				return false;
			}else if ($scope.divisionMaster.companyMaster.status =='Block'){
				$scope.errorFocus('companyName');
				$scope.errorMap.put("companyName",$rootScope.nls["ERR3316"]);
				return false;
			}else if($scope.divisionMaster.companyMaster.status =='Hide'){
				$scope.errorFocus('companyName');
				$scope.errorMap.put("companyName",$rootScope.nls["ERR3317"]);
				return false;
			}
		}
		
		return true;
	}

	$scope.setStatus = function(data){
		if(data=='Active'){
			return 'activetype';
		} else if(data=='Block'){
			return 'blockedtype';
		}else if(data=='Hide'){
			return 'hiddentype';
		}
	}
	

	$scope.getDivisionById = function(id) {
		console.log("getDivisionById ", id);
		
		DivisionView.get({ id : id }, function(data) {
   			if (data.responseCode =="ERR0"){
   				console.log("Successful while getting Division.", data)
   				$scope.divisionMaster = data.responseObject;
   			}
   			$scope.init();
		}, function(error) {
   			console.log("Error while getting Division.", error)
   		});
	}

	$scope.$on('addDivisionMasterEvent', function(events, args){
		
			$rootScope.unfinishedData = $scope.divisionMaster;
			$rootScope.category = "Division Master";
			$rootScope.unfinishedFormTitle = "Division (New)";
			if($scope.divisionMaster != undefined && $scope.divisionMaster != null && $scope.divisionMaster.divisionName != undefined 
					&& $scope.divisionMaster.divisionName != null && $scope.divisionMaster.divisionName != "") {
				$rootScope.subTitle = $scope.divisionMaster.divisionName;
			} else {
				$rootScope.subTitle = "Unknown Division"
			}
	})
	  
	 $scope.$on('editDivisionMasterEvent', function(events, args) {
		 
		 $rootScope.unfinishedData = $scope.divisionMaster;
			$rootScope.category = "Division Master";
			$rootScope.unfinishedFormTitle = "Division Edit ("+  $scope.divisionMaster.divisionCode +")";
			if($scope.divisionMaster != undefined && $scope.divisionMaster != null && $scope.divisionMaster.divisionName != undefined 
					&& $scope.divisionMaster.divisionName != null && $scope.divisionMaster.divisionName != "") {
				$rootScope.subTitle = $scope.divisionMaster.divisionName;
			} else {
				$rootScope.subTitle = "Unknown Division"
			}
	})
	
	
    $scope.$on('addDivisionMasterEventReload', function (e, confirmation) {
        confirmation.message = "";
        localStorage.reloadFormData = JSON.stringify($scope.divisionMaster) ;
        localStorage.isDivisionReloaded = "YES";
        e.preventDefault();
    });

	
	$scope.$on('editDivisionMasterEventReload', function (e, confirmation) {
        confirmation.message = "";
        localStorage.reloadFormData = JSON.stringify($scope.divisionMaster) ;
        localStorage.isDivisionReloaded = "YES";
        e.preventDefault();
    });
	
	
	$scope.isReloaded = localStorage.isDivisionReloaded;
	
	if ($stateParams.action == "ADD") {
		if($stateParams.fromHistory == 'Yes') {
			if($scope.isReloaded == "YES") {
				console.log("Add History Reload...")
				$scope.isReloaded = "NO";
				localStorage.isDivisionReloaded = "NO";
				$scope.divisionMaster  = JSON.parse(localStorage.reloadFormData);
			} else {
				console.log("Add History Not Reload...")
				$scope.divisionMaster = $rootScope.selectedUnfilledFormData;
			}
		} else {
			if($scope.isReloaded == "YES") {
				console.log("Add Not History Reload...")
				$scope.isReloaded = "NO";
				localStorage.isDivisionReloaded = "NO";
				$scope.divisionMaster  = JSON.parse(localStorage.reloadFormData);
			} else {
				console.log("Add Not History Not Reload...")
				$scope.init();
			}	
		}
		$rootScope.breadcrumbArr = [ { label:"Master",  state:"layout.divisionMaster"}, 
		                             { label:"Division ",  state:"layout.divisionMaster"}, 
                                     { label:"Add Division", state:null }];
		
		
		
	} else {
		if($stateParams.fromHistory === 'Yes') {
			if($scope.isReloaded == "YES") {
				$scope.isReloaded = "NO";
				localStorage.isDivisionReloaded = "NO";
				$scope.divisionMaster  = JSON.parse(localStorage.reloadFormData);
			} else {
				$scope.divisionMaster = $rootScope.selectedUnfilledFormData;
			}
		} else {
			if($scope.isReloaded == "YES") {
				$scope.isReloaded = "NO";
				localStorage.isDivisionReloaded = "NO";
				$scope.divisionMaster  = JSON.parse(localStorage.reloadFormData);
			} else {
				$scope.getDivisionById($stateParams.id);
			}
		}
		$rootScope.breadcrumbArr = [ { label:"Master",  state:"layout.divisionMaster"}, 
		                             { label:"Division ",  state:"layout.divisionMaster"}, 
                                     { label:"Edit Division", state:null }];
		
	}
	
	
});