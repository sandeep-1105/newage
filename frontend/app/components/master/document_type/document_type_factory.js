(function() {

	app.factory("DocumentTypeSearch",['$resource', function($resource) {
		return $resource("/api/v1/documenttypemaster/get/search", {}, {
			query : {
				method : 'POST'
			}
		});
	}]);
	
	app.factory("DocumentTypeSearchKeyword",['$resource', function($resource) {
		return $resource("/api/v1/documenttypemaster/get/search/keyword", {}, {
			fetch : {
				method : 'POST'
			}
		});
	}]);


	app.factory('documentTypeMasterFactory',['$resource', function($resource) {
		return {
			findById : $resource('/api/v1/documenttypemaster/get/id/:id', {}, {
				 query : {method : 'GET', params : {id : ''}, isArray : false}
			}),
			findByCode : $resource('/api/v1/documenttypemaster/get/code/:documentTypeCode', {}, {
				 query : {method : 'GET', params : {documentTypeCode : ''}, isArray : false}
			}),
			create : $resource('/api/v1/documenttypemaster/create', {}, {
				query : {method : 'POST'}
			}),
			update : $resource('/api/v1/documenttypemaster/update', {}, {
				query : {method : 'POST'}
			}),
			delete : $resource('/api/v1/documenttypemaster/delete/:id', {}, {
				query : {method : 'DELETE', params : {id : ''}}
			}),
			
			search : $resource('/api/v1/documenttypemaster/get/search', {}, {
				fetch : {method : 'POST'}
			})
		};
	}])
})();

