app.service('documentTypeMasterValidationService', ['$rootScope','ValidateUtil', 'CommonValidationService', 'appConstant', function($rootScope, ValidateUtil, CommonValidationService, appConstant) {
	this.validate = function(documentType, code) {
		//Validate documentType name
		if(code == 0 || code == 1) {
			if(documentType.documentTypeName == undefined || documentType.documentTypeName == null || documentType.documentTypeName =="" ){
				return this.validationResponse(true, "documentTypeName", $rootScope.nls["ERR120004"], documentType);
			}
		}
		//Validate documentType code
		if(code == 0 || code == 2) {
			if(documentType.documentTypeCode == undefined || documentType.documentTypeCode == null || documentType.documentTypeCode =="" ){
				return this.validationResponse(true, "documentTypeCode", $rootScope.nls["ERR120002"], documentType);
			}
		}
		return this.validationSuccesResponse(documentType);
	}
 
	this.validationSuccesResponse = function(documentType) {
	    return {error : false, obj : documentType};
	}
	
	this.validationResponse = function(err, elem, message, documentType) {
		return {error : err, errElement : elem, errMessage : message, obj : documentType};
	}
	
}]);