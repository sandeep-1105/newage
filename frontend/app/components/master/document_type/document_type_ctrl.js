(function(){
app.controller("DocumentTypeMasterCtrl", ['$scope', '$rootScope', '$window', '$modal', '$state', '$stateParams', '$log', 'ngDialog', 'documentTypeMasterDataService', 'documentTypeMasterFactory', 'RecentHistorySaveService', 'documentTypeMasterValidationService', 'Notification','PortByTransportMode','roleConstant',
    function($scope, $rootScope, $window, $modal, $state, $stateParams, $log, ngDialog, documentTypeMasterDataService, documentTypeMasterFactory, RecentHistorySaveService, documentTypeMasterValidationService, Notification,PortByTransportMode, roleConstant ) {

        var vm = this;
        vm.tableHeadArr = documentTypeMasterDataService.getHeadArray();
        vm.dataArr = [];
        vm.searchData = {};
        vm.documentTypeMaster = {};
        vm.documentTypeMaster.status='Active';
        vm.limitArr = [10, 15, 20];
        vm.page = 0;
        vm.limit = 10;
        vm.totalRecord = 100;
        vm.showHistory = false;
        vm.errorMap = new Map();
        vm.deskTopView = true;

        vm.sortSelection = {
            sortKey: "documentTypeName",
            sortOrder: "asc"
        }
        
        $scope.portRender = function(item){
			return {
				label:item.portName,
				item:item
			}
		}
        
        $scope.ajaxPortEvent = function(object) {
			$scope.searchDto = {};
			$scope.searchDto.keyword = object == null ? "" : object;
			$scope.searchDto.selectedPageNumber = 0;
			$scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
			
			return PortByTransportMode.fetch({
				"transportMode" : 'Air'
			}, $scope.searchDto).$promise.then(function(data) {
				if (data.responseCode == "ERR0") {
					$scope.portList = [];
					var resultList = data.responseObject.searchResult;
					if (resultList != null && resultList.length != 0) {
						for (var i = 0; i < resultList.length; i++) {
							$scope.portList.push(resultList[i]);
						}
					}
					return $scope.portList;
				}
			},
			function(errResponse) {
			console.error('Error while fetching Port Master List');
			});
		}
        vm.changeSearch = function(param) {
            $log.debug("Change Search is called." + param);
            for (var attrname in param) {
                vm.searchData[attrname] = param[attrname];
            }
            vm.page = 0;
            vm.search();
        }

        vm.sortChange = function(param) {
            $log.debug("sortChange is called. " + param);
            vm.searchData.orderByType = param.sortOrder.toUpperCase();
            vm.searchData.sortByColumn = param.sortKey;
            vm.search();
        }
        
        vm.setStatus = function(data){
    		if(data=='Active'){
    			return 'activetype';
    		} else if(data=='Block'){
    			return 'blockedtype';
    		}else if(data=='Hide'){
    			return 'hiddentype';
    		}
    	}
        
        /**
         * DocumentType Master List Navigate to selected page.
         * @param {Object} param - The Selected page option.
         */
        vm.changepage = function(param) {
            $log.debug("change Page..." + param.page);
            vm.page = param.page;
            vm.limit = param.size;
            vm.search();
        }

        /**
         * DocumentType Master List Page Limit Data change.
         * @param {int} nLimit - The Selected limit option from Limit array.
         */
        vm.limitChange = function(nLimit) {
            $log.debug("Limit Change..." + nLimit);
            vm.page = 0;
            vm.limit = nLimit;
            vm.search();
        }

        /**
         * DocumentType Master List Page Search Data  initializing and calling search Mtd.
         * @param {Object} data - The Selected DocumentType Master from List page.
         * @param {string} data.id - The id of the Selected DocumentType Master.
         * @param {int} index - The Selected row index from List page.
         */
        vm.rowSelect = function(data) {
        	if($rootScope.roleAccess(roleConstant.MASTER_FINANCE_DOCUMENT_TYPE_VIEW)){
        		vm.documentTypeMasteView=true;
    			vm.documentTypeMaster = data;
    			var windowInner=$window.innerWidth;
    			if(windowInner<=1199){
    				vm.deskTopView = false;
    			}
    			else{
    				vm.deskTopView = true;
    			}
    			angular.element($window).bind('resize', function(){
    				if($window.innerWidth>=1200){
    					vm.deskTopView = true;
    				}
    				else if($window.innerWidth<=1199){
    					if(vm.documentTypeMaster.id!=null){
    						vm.deskTopView = false;
    					}
    					else {
    						vm.deskTopView = true;
    					}
    				}
    				$scope.$digest();
    			});
        	}
		}

        /**
         * DocumentType Master List Page Search Data  initializing and calling search Mtd.
         */
        vm.searchInit = function() {
            vm.searchData = {};
            vm.searchData.orderByType = vm.sortSelection.sortOrder.toUpperCase();
            vm.searchData.sortByColumn = vm.sortSelection.sortKey;
            vm.search();
        };
        /**
         * Go Back to DocumentType Master List Page.
         */
        vm.backToList = function() {
        	vm.documentTypeMasteView = false;
        	vm.documentTypeMaster = {};
        	vm.deskTopView = true;
        }

       
        /**
         * Get DocumentType Master's Based on Search Data.
         */
        vm.search = function() {
        	vm.documentTypeMasteView=false;
            vm.searchData.selectedPageNumber = vm.page;
            vm.searchData.recordPerPage = vm.limit;
            vm.dataArr = [];
            documentTypeMasterFactory.search.fetch(vm.searchData).$promise.then(
                function(data) {
                    if (data.responseCode == "ERR0") {
                        vm.totalRecord = data.responseObject.totalRecord;
                        vm.dataArr = data.responseObject.searchResult;
                    }
                },
                function(errResponse) {
                	 $log.error('Error while fetching DocumentType');
                });
           }

        /**
         * Get DocumentType Master By id.
         * @param {int} id - The id of a DocumentType.
         * @param {boolean} isView - TO identify purpose of calling is for View or Edit a DocumentType.
         */
        vm.view = function(id, isView) {
        	documentTypeMasterFactory.findById.query({
                id: id
            }).$promise.then(function(data) {
                if (data.responseCode == 'ERR0') {
                    vm.documentTypeMaster = data.responseObject;
                    var rHistoryObj = {}

                    if (vm.documentTypeMaster != undefined && vm.documentTypeMaster != null && vm.documentTypeMaster.id != undefined && vm.documentTypeMaster.id != null) {
                        $rootScope.subTitle = vm.documentTypeMaster.documentTypeName;
                    } else {
                        $rootScope.subTitle = "Unknown DocumentType"
                    }
                    $rootScope.unfinishedFormTitle = "DocumentType  # " + vm.documentTypeMaster.documentTypeCode;
                    if (isView) {
                        rHistoryObj = {
                            'title': "DocumentType #" + vm.documentTypeMaster.documentTypeCode,
                            'subTitle': $rootScope.subTitle,
                            'stateName': $state.current.name,
                            'stateParam': JSON.stringify($stateParams),
                            'stateCategory': 'DocumentType Master',
                            'serviceType': undefined,
                            'showService': false
                        }

                    } else {

                        rHistoryObj = {
                            'title': 'Edit DocumentType # ' + $stateParams.serviceId,
                            'subTitle': $rootScope.subTitle,
                            'stateName': $state.current.name,
                            'stateParam': JSON.stringify($stateParams),
                            'stateCategory': 'DocumentType Master',
                            'serviceType': 'MASTER',
                            'serviceCode': ' ',
                            'orginAndDestination': ' '
                        }
                    }
                    RecentHistorySaveService.form(rHistoryObj);
                } else {
                    $log.debug("Exception while getting DocumentType by Id ", data.responseDescription);
                }
            }, function(error) {
                $log.debug("Error ", error);
            });
        };

        /**
         * Go To Add DocumentType Master.
         */
        vm.addNew = function() {
        	if($rootScope.roleAccess(roleConstant.MASTER_FINANCE_DOCUMENT_TYPE_CREATE)){
        		 $state.go("layout.documentTypeMasterAdd");
        	}
        }

        /**
         * Validating field of DocumentType Master while instant change.
         * @param {errorElement,errorCode}
         */
        vm.validate = function(valElement, code) {
        	vm.errorMap = new Map();
            var validationResponse = documentTypeMasterValidationService.validate(vm.documentTypeMaster,code);
            if(validationResponse.error == true) {
				$scope.validationErrorAndFocus(validationResponse, validationResponse.errElement);
			} else {
				vm.errorMap.put(valElement, null);
            	validationResponse.error = false;
            }
        }
        
        /**
         * Cancel Add or Edit DocumentType Master.
         * @param {int} id - The id of a DocumentType
         */
       
        vm.cancel = function(objId) {
            if(vm.documentTypeMasterForm!=undefined){
		    	vm.goTocancel(vm.documentTypeMasterForm.$dirty,objId);
            }else{
            	vm.goTocancel(false,objId);
            }
        }
       
       vm.goTocancel = function(isDirty,objId){
     		 if (isDirty) {
     		      var newScope = $scope.$new();
     		      newScope.errorMessage = $rootScope.nls["ERR200"];
     		      ngDialog.openConfirm({
     		        template: '<p>{{errorMessage}}</p>' +
     		          '<div class="ngdialog-footer">' +
     		          ' <button type="button" class="btn btn-default btn-property cancel-btn" ng-click="confirm(2)">No</button>' +
     		          '<button type="button" class="btn btn-default btn-property accent-btn " ng-click="confirm(1)">Yes</button>' +
     		          '<button type="button" class="btn btn-default btn-property pull-left grey-cancel-btn" ng-click="confirm(3)">Cancel</button>' +
     		          '</div>',
     		        plain: true,
     		        closeByDocument: false,
     		        scope: newScope,
     		        className: 'ngdialog-theme-default'
     		      }).then(
     		        function(value) {
     		          if (value == 1) {
     		            if(objId!=null && objId != undefined){
     		            	vm.update();
     		            }else{
     		            	vm.create();
     		            }
     		          } else if (value == 2) {
     		        	 $state.go("layout.documentTypeMaster", {
     		                submitAction: "Cancelled"
     		            });
     		          } else {
     		            $log.debug("cancelled");
     		          }
     		        });
     		    } else {
     		     $state.go("layout.documentTypeMaster", {
                   submitAction: "Cancelled"
               });
     		    }
     	    }

        /**
         * Modify Existing DocumentType Master.
         * @param {int} id - The id of a Country.
         */
        vm.edit = function(id) {
        	if($rootScope.roleAccess(roleConstant.MASTER_FINANCE_DOCUMENT_TYPE_MODIFY)){
        		 $state.go("layout.documentTypeMasterEdit", {
                     id: id
                 });
        	}
        }

        /**
         * Delete Country Master.
         * @param {int} id - The id of a Country.
         */
        vm.delete = function(id) {
        	if($rootScope.roleAccess(roleConstant.MASTER_FINANCE_DOCUMENT_TYPE_DELETE)){
        		var newScope = $scope.$new();
    			newScope.errorMessage = $rootScope.nls["ERR120014"];
    	    	ngDialog.openConfirm( {
    	                    template : '<p>{{errorMessage}}</p>'
    	                    + '<div class="ngdialog-footer">'
    	                    + '<button type="button" class="btn btn-default btn-property cancel-btn" ng-click="closeThisDialog(0)">Cancel'
    	                    + '<button type="button" class="btn btn-primary btn-property accent-btn" ng-click="confirm(1)">Yes'
    	                    + '</button></div>',
    	                    plain : true,
    	                    scope : newScope,
    	                    closeByDocument: false,
    	                    className : 'ngdialog-theme-default'
    	                }).then( function(value) {
    	                	documentTypeMasterFactory.delete.query({
    	                        id: id
    	                    }).$promise.then(function(data) {
    	                        if (data.responseCode == 'ERR0') {
    	                        	vm.documentTypeMasteView=false;
    	                        	Notification.success($rootScope.nls["ERR402"]);
    	                            vm.search();
    	                        }
    	                    }, function(error) {
    	                        $log.debug("Error ", error);
    	                    });
    	                }, function(value) {
    	                    $log.debug("delete Cancelled");
    	            });
        	
        	}
        }

        /**
         * Create New DocumentType Master.
         */
        vm.create = function() {
            var validationResponse = documentTypeMasterValidationService.validate(vm.documentTypeMaster,0);
            if(validationResponse.error == true) {
            	$log.debug("Validation Response -- ", validationResponse);
            	$log.debug('Validation Failed to create DocumentType..')
				$scope.validationErrorAndFocus(validationResponse, validationResponse.errElement);
			}
            else {
            	documentTypeMasterFactory.create.query(vm.documentTypeMaster).$promise.then(function(data) {
		            if (data.responseCode == 'ERR0') {
		            	$log.debug("DocumentType Saved Successfully Response :: ", data);
		            	Notification.success($rootScope.nls["ERR400"]);
		            	$state.go('layout.documentTypeMaster');
		            } else {
		                $log.debug("Create DocumentType Failed :" + data.responseDescription)
		            }

		        }, function(error) {
		        	 $log.debug("Create DocumentType Failed : " + error)
		        });
            } 
        };

        
      /**
       * Common Validation Focus Functionality - starts here" 
       */
		$scope.validationErrorAndFocus = function(valResponse, elemId){
			vm.errorMap = new Map();
			if(valResponse.error == true){
				vm.errorMap.put(valResponse.errElement, valResponse.errMessage);
			}
				$scope.navigateToNextField(elemId);
		}
        
        //Update Existing Country Master
        vm.update = function() {
        	 var validationResponse = documentTypeMasterValidationService.validate(vm.documentTypeMaster,0);
             if(validationResponse.error == true) {
             	$log.debug("Validation Response -- ", validationResponse);
             	$log.debug('Validation Failed while Updating the DocumentType..')
 				$scope.validationErrorAndFocus(validationResponse, validationResponse.errElement);
 			}
             else {
             	documentTypeMasterFactory.update.query(vm.documentTypeMaster).$promise.then(function(data) {
 		            if (data.responseCode == 'ERR0') {
 		            	$log.debug("DocumentType Updated Successfully Response :: ", data);
 		            	Notification.success($rootScope.nls["ERR401"]);
 		            	$state.go('layout.documentTypeMaster');
 		            } else {
 		                $log.debug("Updating DocumentType Failed :" + data.responseDescription)
 		            }
 		        }, function(error) {
 		        	 $log.debug("Updating DocumentType Failed : " + error)
 		        });
             } 
        };
        
       
        /**
         * Recent History - starts here
         */
        vm.isReloaded = localStorage.isDocumentTypeMasterReloaded;
        $scope.$on('addDocumentTypeMasterEvent', function(events, args){
        	$rootScope.unfinishedData = vm.documentTypeMaster;
			$rootScope.category = "DocumentType";
			$rootScope.unfinishedFormTitle = "DocumentType (New)";
        	if(vm.documentTypeMaster != undefined && vm.documentTypeMaster != null && vm.documentTypeMaster.documentTypeName != undefined && vm.documentTypeMaster.documentTypeName != null) {
        		$rootScope.subTitle = vm.documentTypeMaster.documentTypeName;
        	} else {
        		$rootScope.subTitle = "Unknown DocumentType "
        	}
        })
      
	    $scope.$on('editDocumentTypeMasterEvent', function(events, args){
	    	$rootScope.unfinishedData = vm.documentTypeMaster;
			$rootScope.category = "DocumentType";
			$rootScope.unfinishedFormTitle = "DocumentType Edit # "+vm.documentTypeMaster.documentTypeCode;
	    })
	      
	    $scope.$on('addDocumentTypeMasterEventReload', function(e, confirmation){
	    	    confirmation.message = "";
	    	    localStorage.reloadFormData = JSON.stringify(vm.documentTypeMaster) ;
	    	    localStorage.isDocumentTypeMasterReloaded = "YES";
	            e.preventDefault();
	    })
	    
	    $scope.$on('editDocumentTypeMasterEventReload', function(e, confirmation){
	    	    confirmation.message = "";
	    	    localStorage.reloadFormData = JSON.stringify(vm.documentTypeMaster) ;
	    	    localStorage.isDocumentTypeMasterReloaded = "YES";
	            e.preventDefault();
	    	  
	    })
	      


        /**
         * State Identification Handling Here.
         * @param {String} $stateParams.action - VIEW, ADD, EDIT, SEARCH.
         */
        //
        switch ($stateParams.action) {
            case "ADD":
            	 if ($stateParams.fromHistory === 'Yes') {
                     if (vm.isReloaded == "YES") {
                      vm.isReloaded = "NO";
                      localStorage.isDocumentTypeMasterReloaded = "NO";
                       vm.documentTypeMaster = JSON.parse(localStorage.reloadFormData);
                     } else {
                       vm.documentTypeMaster = $rootScope.selectedUnfilledFormData;
                     }
                   } else {
                     if (vm.isReloaded == "YES") {
                       vm.isReloaded = "NO";
                       localStorage.isDocumentTypeMasterReloaded = "NO";
                       vm.documentTypeMaster = JSON.parse(localStorage.reloadFormData);
                     } else {
                         $log.debug("Add NotHistory NotReload...")
                     }
                   }
                $rootScope.breadcrumbArr = documentTypeMasterDataService.getAddBreadCrumb();
                break;
            case "EDIT":
            	 if (vm.fromHistory === 'Yes') {
                     if (vm.isReloaded == "YES") {
                       vm.isReloaded = "NO";
                       localStorage.isDocumentTypeMasterReloaded = "NO";
                       vm.documentTypeMaster = JSON.parse(localStorage.reloadFormData);
                     } else {
                       vm.documentTypeMaster = $rootScope.selectedUnfilledFormData;
                     }
                   } else {
                     if (vm.isReloaded == "YES") {
                       vm.isReloaded = "NO";
                       localStorage.isDocumentTypeMasterReloaded = "NO";
                       vm.documentTypeMaster = JSON.parse(localStorage.reloadFormData);
                     } else {
                    	 vm.view($stateParams.id, false);
                     }
                   }
                $rootScope.breadcrumbArr = documentTypeMasterDataService.getEditBreadCrumb();
                break;
            default:
                $rootScope.breadcrumbArr = documentTypeMasterDataService.getListBreadCrumb();
                vm.searchInit();
        }

    }
]);
}) ();