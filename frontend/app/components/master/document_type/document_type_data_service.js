app.service('documentTypeMasterDataService', function($rootScope) {
    this.getHeadArray = function() {
	   return [
			  {
					"name":"#",
					"width":"w25px",
					"model":"no",
					"prefWidth": "25",
					"search":false
			  },{
					"name":"Name",
					"width":"w200px",
					"model":"documentTypeName",
					"search":true,
					"wrap_cell":true,
					"type":"text",
					"sort":true,
					"prefWidth": "200",
					"key":"searchDocumentTypeName"
				},{
					"name":"Code",
					"width":"w75px",
					"model":"documentTypeCode",
					"search":true,
					"type":"text",
					"sort":true,
					"prefWidth": "75",
					"key":"searchDocumentTypeCode"
				},{
					"name":"Status",
					"width":"w100px",
					"model":"status",
					"search":true,
					"type":"drop",
					"data":$rootScope.enum['LovStatus'],
					"key":"searchStatus",
					"prefWidth": "100",
					"sort":true
				}];
          };

    
    this.getListBreadCrumb = function() {
        return [{
                label: "Master",
                state: "layout.documentTypeMaster"
            },
            {
                label: "Document Type",
                state: "layout.documentTypeMaster"
            }
        ];
    }

    this.getAddBreadCrumb = function() {
        return [{
                label: "Master",
                state: "layout.documentTypeMaster"
            },
            {
                label: "Document Type",
                state: "layout.documentTypeMaster"
            },
            {
                label: "Add Document Type",
                state: null
            }
        ];
    };
    this.getEditBreadCrumb = function() {
        return [{
                label: "Master",
                state: "layout.documentTypeMaster"
            },
            {
                label: "Document Type",
                state: "layout.documentTypeMaster"
            },
            {
                label: "Edit Document Type",
                state: null
            }
        ];
    };
   

});