(function() {

	app.factory("TosSearch",['$resource', function($resource) {
		return $resource("/api/v1/tosmaster/get/search", {}, {
			query : {
				method : 'POST'
			}
		});
	}]);
	
	app.factory("TosSearchKeyword",['$resource', function($resource) {
		return $resource("/api/v1/tosmaster/get/search/keyword", {}, {
			fetch : {
				method : 'POST'
			}
		});
	}]);

	app.factory("TosAdd",['$resource', function($resource) {
		return $resource("/api/v1/tosmaster/create", {}, {
			save : {
				method : 'POST'
			}
		});
	}]);

	app.factory("TosEdit",['$resource', function($resource) {
		return $resource("/api/v1/tosmaster/update", {}, {
			update : {
				method : 'POST'
			}
		});
	}]);

	app.factory("TosView",['$resource', function($resource) {
		return $resource("/api/v1/tosmaster/get/id/:id", {}, {
			get : {
				method : 'GET',
				params : {
					id : ''
				},
				isArray : false
			}
		});
	}]);

	app.factory("TosRemove",['$resource', function($resource) {
		return $resource("/api/v1/tosmaster/delete/:id", {}, {
			remove : {
				method : 'DELETE',
				params : {
					id : ''
				},
				isArray : false
			}
		});
	}]);
	

})();
