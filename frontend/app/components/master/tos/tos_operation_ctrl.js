app.controller('tosOperationCtrl',['$rootScope', '$scope', 'TosAdd', 'TosEdit', 'ngProgressFactory', 
		'ngDialog', '$state', '$stateParams', 'CommonValidationService', 'appConstant', 'TosView',
	function($rootScope, $scope, TosAdd, TosEdit, ngProgressFactory, 
		ngDialog, $state, $stateParams, CommonValidationService, appConstant, TosView) {

    $scope.contained_progressbar = ngProgressFactory.createInstance();
    $scope.contained_progressbar.setParent(document.getElementById('tos-panel'));
    $scope.contained_progressbar.setAbsolute();

    $scope.init = function() {

        if ($scope.tosMaster == undefined || $scope.tosMaster == null) {
            $scope.tosMaster = {};
            $scope.tosMaster.status = 'Active';
        }

        $scope.setOldDataVal();
    };

    $scope.setOldDataVal = function() {
        $scope.oldData = JSON.stringify($scope.tosMaster);
    }


    $scope.cancel = function() {

        if ($scope.oldData != JSON.stringify($scope.tosMaster)) {
            var newScope = $scope.$new();
            newScope.errorMessage = $rootScope.nls["ERR200"];
            ngDialog.openConfirm({
                template: '<p>{{errorMessage}}</p>' +
                    '<div class="ngdialog-footer">' +
                    ' <button type="button" class="btn btn-default btn-property cancel-btn" ng-click="confirm(2)">No</button>' +
                    '<button type="button" class="btn btn-default btn-property accent-btn " ng-click="confirm(1)">Yes</button>' +
                    '<button type="button" class="btn btn-default btn-property pull-left grey-cancel-btn" ng-click="confirm(3)">Cancel</button>' +
                    '</div>',
                plain: true,
                scope: newScope,
                className: 'ngdialog-theme-default'
            }).then(function(value) {
                if (value == 1 && $scope.tosMaster.id == null) {
                    $scope.save();
                } else if (value == 1 && $scope.tosMaster.id != null) {
                    $scope.update();
                } else if (value == 2) {
                    var params = {};
                    params.submitAction = 'Cancelled';
                    $state.go("layout.tos", params);
                } else {
                    console.log("cancelled");
                }
            });
        } else {
            var params = {};
            params.submitAction = 'Cancelled';
            $state.go("layout.tos", params);
        }
    };

    $scope.update = function() {
        console.log("Update Method is called.");
        if ($scope.validateTosMaster(0)) {
            $scope.contained_progressbar.start();
            TosEdit.update($scope.tosMaster).$promise.then(function(data) {
                if (data.responseCode == 'ERR0') {
                    console.log("TOS updated Successfully")
                    $rootScope.successDesc = $rootScope.nls["ERR401"];
                    var params = {}
                    params.submitAction = 'Saved';
                    $state.go("layout.tos", params);
                } else {
                    console.log("TOS updated Failed ", data.responseDescription)
                }
                $scope.contained_progressbar.complete();
                angular.element(".panel-body").animate({
                    scrollTop: 0
                }, "slow");
            }, function(error) {
                console.log("TOS updated Failed : " + error)
            });
        } else {
            console.log("Invalid Form Submission.");
        }
    }

    $scope.save = function() {
        console.log("Save Method is called.");
        if ($scope.validateTosMaster(0)) {

            $scope.contained_progressbar.start();
            TosAdd.save($scope.tosMaster).$promise.then(function(data) {
                if (data.responseCode == 'ERR0') {
                    $rootScope.successDesc = $rootScope.nls["ERR400"];
                    var params = {}
                    params.submitAction = 'Saved';
                    $state.go("layout.tos", params);
                } else {
                    console.log("Tos added Failed " + data.responseDescription)
                }
                $scope.contained_progressbar.complete();
                angular.element(".panel-body").animate({
                    scrollTop: 0
                }, "slow");
            }, function(error) {
                console.log("Tos added Failed : " + error)
            });
        } else {
            console.log("Invalid Form Submission.");
        }
    }


    $scope.validateTosMaster = function(validateCode) {

        console.log("Validate Called " + validateCode);

        $scope.errorMap = new Map();

        if (validateCode == 0 || validateCode == 1) {

            if ($scope.tosMaster.tosName == undefined ||
                $scope.tosMaster.tosName == null ||
                $scope.tosMaster.tosName == "") {
                $scope.errorMap.put("tosName", $rootScope.nls["ERR2007"]);

                return false;
            } else {

                if (!CommonValidationService.checkRegExp(appConstant.Reg_Exp_Master_Tos_Name, $scope.tosMaster.tosName)) {
                    $scope.errorMap.put("tosName", $rootScope.nls["ERR2012"]);
                    return false;
                }
            }
        }


        if (validateCode == 0 || validateCode == 2) {
            if ($scope.tosMaster.tosCode == undefined ||
                $scope.tosMaster.tosCode == null ||
                $scope.tosMaster.tosCode == "") {
                $scope.errorMap.put("tosCode", $rootScope.nls["ERR2006"]);
                return false;
            } else {

                if (!CommonValidationService.checkRegExp(appConstant.Reg_Exp_Master_Tos_Code, $scope.tosMaster.tosCode)) {
                    $scope.errorMap.put("tosCode", $rootScope.nls["ERR2011"]);
                    return false;
                }
            }
        }

        if (validateCode == 0 || validateCode == 3) {
            if ($scope.tosMaster.freightPPCC == undefined ||
                $scope.tosMaster.freightPPCC == null ||
                $scope.tosMaster.freightPPCC == "") {
                $scope.errorMap.put("freightPPCC", $rootScope.nls["ERR2008"]);
                return false;
            }
        }
        if (validateCode == 0 || validateCode == 4) {
            /* No Validations for Descriptions.. */
        }

        return true;

    }




    $scope.setStatus = function(data) {
        if (data == 'Active') {
            return 'activetype';
        } else if (data == 'Block') {
            return 'blockedtype';
        } else if (data == 'Hide') {
            return 'hiddentype';
        }
    }




    $scope.getTosById = function(tosId) {
        console.log("getTosById ", tosId);

        TosView.get({
            id: tosId
        }, function(data) {
            if (data.responseCode == "ERR0") {
                console.log("Successful while getting TOS.", data)
                $scope.tosMaster = data.responseObject;
            }

            $scope.setOldDataVal();
        }, function(error) {
            console.log("Error while getting TOS.", error)
        });

    }




    //On leave the Unfilled TOS Form Broadcast then event will happen in StateChangeListener(BroadCaster)-app_state_route.js

    $scope.$on('addTosEvent', function(events, args) {

        $rootScope.unfinishedData = $scope.tosMaster;
        $rootScope.category = "Terms of Shipment";
        $rootScope.unfinishedFormTitle = "Terms of Shipment (New)";
        if ($scope.tosMaster != undefined && $scope.tosMaster != null && $scope.tosMaster.tosName != undefined &&
            $scope.tosMaster.tosName != null && $scope.tosMaster.tosName != "") {
            $rootScope.subTitle = $scope.tosMaster.tosName;
        } else {
            $rootScope.subTitle = "Unknown Terms of Shipment"
        }
    })

    $scope.$on('editTosEvent', function(events, args) {

        $rootScope.unfinishedData = $scope.tosMaster;
        $rootScope.category = "Terms of Shipment";
        $rootScope.unfinishedFormTitle = "Terms of Shipment Edit (" + $scope.tosMaster.tosCode + ")";
        if ($scope.tosMaster != undefined && $scope.tosMaster != null && $scope.tosMaster.tosName != undefined &&
            $scope.tosMaster.tosName != null && $scope.tosMaster.tosName != "") {
            $rootScope.subTitle = $scope.tosMaster.tosName;
        } else {
            $rootScope.subTitle = "Unknown Terms of Shipment"
        }
    })


    $scope.$on('addTosEventReload', function(e, confirmation) {
        confirmation.message = "";
        localStorage.reloadFormData = JSON.stringify($scope.tosMaster);
        localStorage.isTosReloaded = "YES";
        e.preventDefault();
    });


    $scope.$on('editTosEventReload', function(e, confirmation) {
        confirmation.message = "";
        localStorage.reloadFormData = JSON.stringify($scope.tosMaster);
        localStorage.isTosReloaded = "YES";
        e.preventDefault();
    });


    $scope.isReloaded = localStorage.isTosReloaded;

    if ($stateParams.action == "ADD") {
        if ($stateParams.fromHistory === 'Yes') {
            if ($scope.isReloaded == "YES") {
                console.log("Add History Reload...")
                $scope.isReloaded = "NO";
                localStorage.isTosReloaded = "NO";
                $scope.tosMaster = JSON.parse(localStorage.reloadFormData);
                $scope.init();
            } else {
                console.log("Add History NotReload...")
                $scope.tosMaster = $rootScope.selectedUnfilledFormData;
                $scope.init();
            }
            $scope.oldData = "";
        } else {
            if ($scope.isReloaded == "YES") {
                $scope.isReloaded = "NO";
                localStorage.isTosReloaded = "NO";
                $scope.tosMaster = JSON.parse(localStorage.reloadFormData);
                $scope.init();
                $scope.oldData = "";
                console.log("Add NotHistory Reload...")
            } else {
                console.log("Add NotHistory NotReload...")
                $scope.init(0);
            }
        }
        $rootScope.breadcrumbArr = [{
                label: "Master",
                state: "layout.tos"
            },
            {
                label: "Terms Of Shipment",
                state: "layout.tos"
            },
            {
                label: "Add Terms Of Shipment",
                state: null
            }
        ];

    } else {
        if ($stateParams.fromHistory === 'Yes') {
            if ($scope.isReloaded == "YES") {
                $scope.isReloaded = "NO";
                localStorage.isTosReloaded = "NO";
                $scope.tosMaster = JSON.parse(localStorage.reloadFormData);
                $scope.init();
            } else {
                $scope.tosMaster = $rootScope.selectedUnfilledFormData;
                $scope.init();
            }
            $scope.oldData = "";
        } else {
            if ($scope.isReloaded == "YES") {
                $scope.isReloaded = "NO";
                localStorage.isTosReloaded = "NO";
                $scope.tosMaster = JSON.parse(localStorage.reloadFormData);
                $scope.init();
                $scope.oldData = "";
            } else {
                $scope.getTosById($stateParams.tosId);
                $scope.init();
            }
        }
        $rootScope.breadcrumbArr = [{
                label: "Master",
                state: "layout.tos"
            },
            {
                label: "Terms Of Shipment",
                state: "layout.tos"
            },
            {
                label: "Edit Terms Of Shipment",
                state: null
            }
        ];
    }


}]);