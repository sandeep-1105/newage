(function() {

    app.factory("StateSearch",['$resource', function($resource) {
        return $resource("/api/v1/statemaster/get/search", {}, {
            query: {
                method: 'POST'
            }
        });
    }]);

    app.factory("StateSaveOrUpdate",['$resource', function($resource) {
        return $resource("/api/v1/statemaster/create", {}, {
            save: {
                method: 'POST'
            }
        });
    }]);


    app.factory("StateView",['$resource', function($resource) {
        return $resource("/api/v1/statemaster/get/id/:id", {}, {
            get: {
                method: 'GET',
                params: {
                    id: ''
                },
                isArray: false
            }
        });
    }]);


    app.factory("StateRemove",['$resource', function($resource) {
        return $resource("/api/v1/statemaster/delete/:id", {}, {
            remove: {
                method: 'DELETE',
                params: {
                    id: ''
                },
                isArray: false
            }
        });
    }]);


    app.factory("StateList",['$resource', function($resource) {
        return $resource("/api/v1/statemaster/get/search/keyword/:countryId", {}, {
            fetch: {
                method: 'POST',
                params: {
                    countryId: ''
                },
                isArray: false
            }
        });
    }]);


    app.factory("StateListCode",['$resource', function($resource) {
        return $resource("/api/v1/statemaster/get/search/keyword/code/:countryCode", {}, {
            fetch: {
                method: 'POST',
                params: {
                    countryCode: ''
                },
                isArray: false
            }
        });
    }]);


})();