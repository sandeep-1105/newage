app.controller('stateMasterController',['$rootScope', '$scope', '$window', '$stateParams', '$state', 'ngTableParams', 'ngDialog',
    'StateSearch', 'StateRemove', 'roleConstant',
	function($rootScope, $scope, $window, $stateParams, $state, ngTableParams, ngDialog,
    StateSearch, StateRemove, roleConstant) {

    /************************************************************
     * ux - by Muthu
     * reason - ipad compatibility fix
     *
     * *********************************************************/
    $scope.deskTopView = true;

    /********************* ux fix ends here *****************************/

    $scope.stateHeadArr = [{
        "name": "#",
        "width": "col-xs-0half",
        "model": "no",
        "search": false,
    }, {
        "name": "Name",
        "width": "col-xs-3",
        "model": "stateName",
        "search": true,
        "wrap_cell": true,
        "type": "text",
        "sort": true,
        "key": "searchStateName"
    }, {
        "name": "Code",
        "width": "col-xs-1",
        "model": "stateCode",
        "search": true,
        "type": "text",
        "sort": true,
        "key": "searchStateCode"
    }, {
        "name": "Country",
        "width": "col-xs-3",
        "model": "countryMaster.countryName",
        "search": true,
        "type": "text",
        "sort": true,
        "key": "searchCountryName"
    }, {
        "name": "Status",
        "width": "col-xs-2half",
        "model": "status",
        "search": true,
        "sort": true,
        "type": "drop",
        "data": $rootScope.enum['LovStatus'],
        "key": "searchStatus"
    }];




    $scope.sortSelection = {
        sortKey: "stateName",
        sortOrder: "asc"
    }

    $scope.limitArr = [10, 15, 20];
    $scope.page = 0;
    $scope.limit = 10;


    $scope.init = function() {
        $scope.searchDto = {};
        $scope.search();
    };

    $scope.limitChange = function(item) {
        $scope.page = 0;
        $scope.limit = item;
        $scope.cancel();
        $scope.search();
    }

    $scope.rowSelect = function(data) {
        if ($rootScope.roleAccess(roleConstant.MASTER_GENERAL_STATE_PROVINCE_VIEW)) {
            $scope.stateMaster = data;
            $scope.showHistory = false;
            $scope.detailView = true;

            /************************************************************
             * ux - by Muthu
             * reason - ipad compatibility fix
             *
             * *********************************************************/

            var windowInner = $window.innerWidth;
            if (windowInner <= 1199) {
                $scope.deskTopView = false;
            } else {
                $scope.deskTopView = true;
            }

            angular.element($window).bind('resize', function() {
                if ($window.innerWidth >= 1200) {
                    $scope.deskTopView = true;
                } else if ($window.innerWidth <= 1199) {
                    if ($scope.tosMaster.id != null) {
                        $scope.deskTopView = false;
                    } else {
                        $scope.deskTopView = true;
                    }
                }

                console.log("window resizing..." + $window.innerWidth);
                $scope.$digest();
            });

            /********************* ux fix ends here *****************************/
        }
    }

    $scope.changeSearch = function(param) {

        for (var attrname in param) {
            $scope.searchDto[attrname] = param[attrname];
        }
        $scope.page = 0;
        $scope.cancel();
        $scope.search();
        console.log("change search", $scope.searchDto);
    }

    $scope.sortChange = function(param) {
        $scope.searchDto.orderByType = param.sortOrder.toUpperCase();
        $scope.searchDto.sortByColumn = param.sortKey;
        $scope.cancel();
        $scope.search();
    }

    $scope.changepage = function(param) {
        $scope.page = param.page;
        $scope.limit = param.size;
        $scope.cancel();
        $scope.search();
    }

    $scope.search = function() {

        $scope.detailView = false;
        $scope.showHistory = false;

        $scope.searchDto.selectedPageNumber = $scope.page;
        $scope.searchDto.recordPerPage = $scope.limit;

        StateSearch.query($scope.searchDto).$promise.then(function(data, status) {
            $scope.totalRecord = data.responseObject.totalRecord;
            $scope.stateDataArr = data.responseObject.searchResult;
            console.log($scope.stateDataArr);
        });

    }

    $scope.cancel = function() {
        $scope.showHistory = false;
        $scope.stateMaster = {};
        $scope.detailView = false;
        /************************************************************
         * ux - by Muthu
         * reason - ipad compatibility fix
         *
         * *********************************************************/
        $scope.deskTopView = true;
        /********************* ux fix ends here *****************************/
    };

    $scope.addStateMaster = function() {
        if ($rootScope.roleAccess(roleConstant.MASTER_GENERAL_STATE_PROVINCE_CREATE)) {
            $state.go("layout.addState");
        }
    };

    $scope.editStateMaster = function() {
        if ($rootScope.roleAccess(roleConstant.MASTER_GENERAL_STATE_PROVINCE_MODIFY)) {
            $state.go("layout.editState", {
                stateId: $scope.stateMaster.id
            });
        }
    };

    $scope.deleteStateMaster = function() {
        if ($rootScope.roleAccess(roleConstant.MASTER_GENERAL_STATE_PROVINCE_DELETE)) {
            var newScope = $scope.$new();
            newScope.errorMessage = $rootScope.nls["ERR249"];
            ngDialog.openConfirm({
                template: '<p>{{errorMessage}}</p>' +
                    '<div class="ngdialog-footer">' +
                    '<button type="button" class="btn btn-default btn-property cancel-btn" ng-click="closeThisDialog(0)">No' +
                    '<button type="button" class="btn btn-primary btn-property accent-btn" ng-click="confirm(1)">Yes' +
                    '</button>' +
                    '</div>',
                plain: true,
                scope: newScope,
                className: 'ngdialog-theme-default'
            }).then(function(value) {

                StateRemove.remove({
                    id: $scope.stateMaster.id
                }, function(data) {

                    if (data.responseCode == "ERR0") {
                        console.log("State deleted Successfully")
                        $scope.cancel();
                        $scope.init();
                        $scope.deskTopView = true;
                    } else {
                        console.log("State deleted Failed ", data.responseDescription)
                    }
                }, function(error) {
                    console.log("State deleted Failed : ", error)
                });
            }, function(value) {
                console.log("State deleted cancelled");
            });
        }

    }



    if ($stateParams.action == "SEARCH") {
        $rootScope.breadcrumbArr = [{
                label: "Master",
                state: "layout.state"
            },
            {
                label: "State",
                state: "layout.state"
            }
        ];
        $scope.init();
    }
}]);