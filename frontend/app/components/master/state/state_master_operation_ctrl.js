app.controller('stateMasterOperationCtrl',['$rootScope', '$scope', 'ngProgressFactory', 'ngDialog', '$state', '$stateParams',
    'StateView', 'StateSaveOrUpdate', 'CountryList', 'Notification', 
	function($rootScope, $scope, ngProgressFactory, ngDialog, $state, $stateParams,
    StateView, StateSaveOrUpdate, CountryList, Notification) {

    $scope.contained_progressbar = ngProgressFactory.createInstance();
    $scope.contained_progressbar.setParent(document.getElementById('state-panel'));
    $scope.contained_progressbar.setAbsolute();


    $scope.init = function() {

        if ($scope.stateMaster == undefined || $scope.stateMaster == null) {
            $scope.stateMaster = {};
            $scope.stateMaster.status = 'Active';
        }

        // Setting to compare on cancel
        $scope.oldData = JSON.stringify($scope.stateMaster);
    };

    $scope.cancel = function() {

        if ($scope.oldData !== JSON.stringify($scope.stateMaster)) {
            var newScope = $scope.$new();
            newScope.errorMessage = $rootScope.nls["ERR200"];
            ngDialog.openConfirm({
                template: '<p>{{errorMessage}}</p>' +
                    '<div class="ngdialog-footer">' +
                    ' <button type="button" class="btn btn-default btn-property cancel-btn" ng-click="confirm(2)">No</button>' +
                    '<button type="button" class="btn btn-default btn-property accent-btn " ng-click="confirm(1)">Yes</button>' +
                    '<button type="button" class="btn btn-default btn-property pull-left grey-cancel-btn" ng-click="confirm(3)">Cancel</button>' +
                    '</div>',
                plain: true,
                scope: newScope,
                className: 'ngdialog-theme-default'
            }).then(function(value) {
                if (value == 1) {
                    $scope.saveOrUpdate();
                } else if (value == 2) {
                    $state.go("layout.state", {
                        submitAction: 'Cancelled'
                    });
                } else {
                    $log.debug("cancelled");
                }
            });
        } else {
            $state.go("layout.state", {
                submitAction: 'Cancelled'
            });
        }
    };


    $scope.saveOrUpdate = function() {


        if ($scope.validateStateMaster(0)) {

            $scope.contained_progressbar.start();


            if ($scope.stateMaster.id == null) {
                var successMessage = $rootScope.nls["ERR400"];
            } else {
                var successMessage = $rootScope.nls["ERR401"];
            }

            StateSaveOrUpdate.save($scope.stateMaster).$promise.then(function(data) {

                if (data.responseCode == "ERR0") {
                    console.log("State updated Successfully")
                    Notification.success(successMessage);
                    $state.go("layout.state", {
                        submitAction: 'Saved'
                    });
                } else {
                    console.log("State updated Failed ", data.responseDescription);
                }

                $scope.contained_progressbar.complete();
                angular.element(".panel-body").animate({
                    scrollTop: 0
                }, "slow");

            }, function(error) {
                console.log("State updated Failed : ", error);
            });
        } else {
            console.log("Validation Failed");
        }

    }




    $scope.ajaxCountryEvent = function(object) {

        console.log("Country Ajax Event.... ", object);

        $scope.searchDto = {};
        $scope.searchDto.keyword = object == null ? "" : object;
        $scope.searchDto.selectedPageNumber = 0;
        $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

        return CountryList.fetch($scope.searchDto).$promise.then(function(data) {
            if (data.responseCode == "ERR0") {
                $scope.countryList = data.responseObject.searchResult;
                return $scope.countryList;
            }
        }, function(errResponse) {
            console.error('Error while fetching Countries');
        });
    }

    $scope.selectedCountry = function(nextIdValue) {
        if ($scope.validateStateMaster(3)) {
            $rootScope.navigateToNextField(nextIdValue);
        }
    };

    $scope.countryRender = function(item) {
        return {
            label: item.countryName,
            item: item
        }
    }




    $scope.validateStateMaster = function(validateCode) {

        console.log("Validate Called " + validateCode);

        $scope.errorMap = new Map();

        if (validateCode == 0 || validateCode == 1) {

            if ($scope.stateMaster.stateName == undefined ||
                $scope.stateMaster.stateName == null ||
                $scope.stateMaster.stateName == "") {
                $scope.errorMap.put("stateName", $rootScope.nls["ERR1807"]);
                return false;
            }

        }


        if (validateCode == 0 || validateCode == 2) {

            if ($scope.stateMaster.stateCode == undefined ||
                $scope.stateMaster.stateCode == null ||
                $scope.stateMaster.stateCode == "") {
                $scope.errorMap.put("stateCode", $rootScope.nls["ERR1805"]);
                return false;
            }

        }

        if (validateCode == 0 || validateCode == 3) {

            if ($scope.stateMaster.countryMaster == undefined ||
                $scope.stateMaster.countryMaster == null ||
                $scope.stateMaster.countryMaster.id == null) {
                $scope.errorMap.put("countryMaster", $rootScope.nls["ERR1811"]);
                return false;
            } else {

                if ($scope.stateMaster.countryMaster.status == 'Block') {
                    $scope.errorMap.put("countryMaster", $rootScope.nls["ERR1812"]);
                    return false;
                }


                if ($scope.stateMaster.countryMaster.status == 'Hide') {
                    $scope.errorMap.put("countryMaster", $rootScope.nls["ERR1812"]);
                    return false;
                }

            }

        }

        return true;

    }




    $scope.setStatus = function(data) {
        if (data == 'Active') {
            return 'activetype';
        } else if (data == 'Block') {
            return 'blockedtype';
        } else if (data == 'Hide') {
            return 'hiddentype';
        }
    }




    $scope.getStateById = function(stateId) {
        console.log("getStateById ", stateId);

        StateView.get({
            id: stateId
        }, function(data) {
            if (data.responseCode == "ERR0") {
                console.log("Successful while getting TOS.", data)
                $scope.stateMaster = data.responseObject;
            }
            $scope.init();
        }, function(error) {
            console.log("Error while getting State.", error)
        });
    }




    $scope.$on('addStateEvent', function(events, args) {

        $rootScope.unfinishedData = $scope.stateMaster;
        $rootScope.category = "State Master";
        $rootScope.unfinishedFormTitle = "State (New)";
        if ($scope.stateMaster != undefined && $scope.stateMaster != null && $scope.stateMaster.stateName != undefined &&
            $scope.stateMaster.stateName != null && $scope.stateMaster.stateName != "") {
            $rootScope.subTitle = $scope.stateMaster.stateName;
        } else {
            $rootScope.subTitle = "Unknown State"
        }
    })

    $scope.$on('editStateEvent', function(events, args) {

        $rootScope.unfinishedData = $scope.stateMaster;
        $rootScope.category = "State Master";
        $rootScope.unfinishedFormTitle = "State Edit (" + $scope.stateMaster.stateCode + ")";
        if ($scope.stateMaster != undefined && $scope.stateMaster != null && $scope.stateMaster.stateName != undefined &&
            $scope.stateMaster.stateName != null && $scope.stateMaster.stateName != "") {
            $rootScope.subTitle = $scope.stateMaster.stateName;
        } else {
            $rootScope.subTitle = "Unknown State"
        }
    })


    $scope.$on('addStateEventReload', function(e, confirmation) {
        confirmation.message = "";
        localStorage.reloadFormData = JSON.stringify($scope.stateMaster);
        localStorage.isStateReloaded = "YES";
        e.preventDefault();
    });


    $scope.$on('editStateEventReload', function(e, confirmation) {
        confirmation.message = "";
        localStorage.reloadFormData = JSON.stringify($scope.stateMaster);
        localStorage.isStateReloaded = "YES";
        e.preventDefault();
    });


    $scope.isReloaded = localStorage.isStateReloaded;

    if ($stateParams.action == "ADD") {
        if ($stateParams.fromHistory == 'Yes') {
            if ($scope.isReloaded == "YES") {
                console.log("Add History Reload...")
                $scope.isReloaded = "NO";
                localStorage.isStateReloaded = "NO";
                $scope.stateMaster = JSON.parse(localStorage.reloadFormData);
            } else {
                console.log("Add History Not Reload...")
                $scope.stateMaster = $rootScope.selectedUnfilledFormData;
            }
        } else {
            if ($scope.isReloaded == "YES") {
                console.log("Add Not History Reload...")
                $scope.isReloaded = "NO";
                localStorage.isStateReloaded = "NO";
                $scope.stateMaster = JSON.parse(localStorage.reloadFormData);
            } else {
                console.log("Add Not History Not Reload...")
                $scope.init();
            }
        }
        $rootScope.breadcrumbArr = [{
                label: "Master",
                state: "layout.state"
            },
            {
                label: "State ",
                state: "layout.state"
            },
            {
                label: "Add State",
                state: null
            }
        ];


        $rootScope.navigateToNextField("stateName");

    } else {
        if ($stateParams.fromHistory === 'Yes') {
            if ($scope.isReloaded == "YES") {
                $scope.isReloaded = "NO";
                localStorage.isStateReloaded = "NO";
                $scope.stateMaster = JSON.parse(localStorage.reloadFormData);
            } else {
                $scope.stateMaster = $rootScope.selectedUnfilledFormData;
            }
        } else {
            if ($scope.isReloaded == "YES") {
                $scope.isReloaded = "NO";
                localStorage.isStateReloaded = "NO";
                $scope.stateMaster = JSON.parse(localStorage.reloadFormData);
            } else {
                $scope.getStateById($stateParams.stateId);
            }
        }
        $rootScope.breadcrumbArr = [{
                label: "Master",
                state: "layout.state"
            },
            {
                label: "State ",
                state: "layout.state"
            },
            {
                label: "Edit State",
                state: null
            }
        ];

        $rootScope.navigateToNextField("stateName");
    }


}]);