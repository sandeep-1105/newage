(function() {

	app.factory("UnitSearch", ['$resource', function($resource) {
		return $resource("/api/v1/unitmaster/get/search", {}, {
			query : {
				method : 'POST'
			}
		});
	}]);
	
	app.factory("UnitSearchKeyword", ['$resource', function($resource) {
		return $resource("/api/v1/unitmaster/get/search/keyword", {}, {
			query : {
				method : 'POST'
			}
		});
	}]);

	app.factory("UnitAdd", ['$resource', function($resource) {
		return $resource("/api/v1/unitmaster/create", {}, {
			save : {
				method : 'POST'
			}
		});
	}]);

	app.factory("UnitEdit", ['$resource', function($resource) {
		return $resource("/api/v1/unitmaster/update", {}, {
			update : {
				method : 'POST'
			}
		});
	}]);

	app.factory("UnitView", ['$resource', function($resource) {
		return $resource("/api/v1/unitmaster/get/id/:id", {}, {
			get : {
				method : 'GET',
				params : {
					id : ''
				},
				isArray : false
			}
		});
	}]);
	
	
	app.factory("UnitMasterGetByCode", ['$resource', function($resource) {
		return $resource("/api/v1/unitmaster/get/code/:unitCode", {}, {
			get : {
				method : 'GET',
				params : {
					unitCode : ''
				},
				isArray : false
			}
		});
	}]);
	
	app.factory("UnitRemove", ['$resource', function($resource) {
		return $resource("/api/v1/unitmaster/delete/:id", {}, {
			remove : {
				method : 'DELETE',
				params : {
					id : ''
				},
				isArray : false
			}
		});
	}]);

})();
