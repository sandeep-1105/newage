/**
 * 
 */

app.filter('calcType',[ function() {
	
	return function(value) {
		
		if(value == 'ADDITION') {
			return '+';
		}
		else if(value == 'DIVISION') {
			return '/';
		}
		else if(value == 'MULTIPLICATION') {
			return '*';
		}
		else if(value == 'SUBTRACTION') {
			return '-';
		}
	}
	
}]);



app.filter('roundOf',[ function() {
	
	return function(value) {
	
	if(typeof value=='number'){
		var changedValue=value.toFixed(3);
		return changedValue;
	}
		
	if(typeof parsedate == "string"){
		var changedValue=parseFloat(value.replace(/,/g, ''));
		return changedValue;
	}	
	return value;
	}
	
}]);

app.filter('unitDecimals',[ function() {
	
	return function(value) {
		
		if(value == 'NA') {
			return 'NA';
		}
		else if(value == 'ZERO') {
			return '0';
		}
		else if(value == 'ONE') {
			return '1';
		}
		else if(value == 'TWO') {
			return '2';
		}
		else if(value == 'THREE') {
			return '3';
		}
	}
	
}]);

app.filter('quoteType',[ function() {
	
	return function(value) {
		
		if(value == 'GENERAL') {
			return 'General';
		}
		else if(value == 'LUMPSUM') {
			return 'Lump Sum';
		}
		else if(value == 'PERKG') {
			return 'Per KG';
		}
	}
	
}]);

app.filter('MappingUnit',[ function() {

	return function(value) {
		
		if(value == 'CHARGEABLE_UNIT') {
			return 'Chargeable Unit';
		}else if(value == 'DAY') {
			return 'Day';
		}else if(value == 'DOC') {
			return 'DOC';
		}else if(value == 'FEU') {
			return 'FEU';
		}else if(value == 'GROSS_WEIGHT') {
			return 'Gross Weight';
		}else if(value == 'PIECES') {
			return 'Pieces';
		}else if(value == 'TEU') {
			return 'TEU';
		}else if(value == 'VOLUME') {
			return 'Volume';
		}else if(value == 'VOLUME_WEIGHT') {
			return 'Volume Weight';
		}else if(value == 'POUND') {
			return 'Pound';
		}else if(value == 'CFT') {
			return 'CFT';
		}else {
			return value;
		}

	}
}]);


app.filter('EmpStatus',[ function() {

	return function(value) {
	  if(value!=undefined && value!=null){
		  return value.toLowerCase().charAt(0).toUpperCase()+ value.toLowerCase().slice(1)
    	}
	}

}]);
app.filter('MappingUnitReverse',[ function() {

	return function(value) {
		
		if(value == 'Chargeable Unit') {
			return 'CHARGEABLE_UNIT';
		}else if(value == 'Day') {
			return 'DAY';
		}else if(value == 'DOC') {
			return 'DOC';
		}else if(value == 'FEU') {
			return 'FEU';
		}else if(value == 'Gross Weight') {
			return 'GROSS_WEIGHT';
		}else if(value == 'Pieces') {
			return 'PIECES';
		}else if(value == 'TEU') {
			return 'TEU';
		}else if(value == 'Volume') {
			return 'VOLUME';
		}else if(value == 'Volume Weight') {
			return 'VOLUME_WEIGHT';
		}else if(value == 'Pound') {
			return 'POUND';
		}else if(value == 'CFT') {
			return 'CFT';
		} else {
			return value;
		}

	}
}]);


app.filter('bondActivityCode',[ function() {
	
	return function(value) {
		
		if(value == 'TISF') {
			return 'TISF';
		}
		else if(value == 'IMPORTER_OR_BROKER') {
			return 'Importer or Broker';
		}
		else if(value == 'INTERNATIONAL_CARRIER') {
			return 'International Carrier';
		}
		else if(value == 'CUSTODIAN_OF_BOND') {
			return 'Custodian of Bond';
		}
		else if(value == 'FOREIGN_TRADE_ZONE_OPERATOR') {
			return 'Foreign Trade Zone Operator';
		}
	}
}]);

app.filter('bondType',[ function() {
	
	return function(value) {
		
		if(value == 'CONTINIUOUS' || value == false) {
			return 'Continuous';
		}
		else if(value == 'SINGLE_TRANSACTION' || value == true) {
			return 'Single Transaction';
		}
	}
}]);

app.filter('consigneeType',[ function() {
	
	return function(value) {
		
		if(value == 'DIRECT_CUSTOMER') {
			return 'D – Direct Customer';
		}
		else if(value == 'GOVT_ENTITY') {
			return 'G – Government Entity';
		}
		else if(value == 'RESELLER') {
			return 'R – Re-Seller';
		}
		else if(value == 'OTHERS') {
			return 'O – Others';
		}
		
	}
}]);


app.filter('frequency',[ function() {
	
	return function(value) {
		
		if(value == 'DAILY') {
			return 'Daily';
		}
		else if(value == 'WEEKLY') {
			return 'Weekly';
		}
		else if(value == 'FORT_NIGHTLY') {
			return 'Fort Nightly';
		}
		else if(value == 'BI_WEEKLY') {
			return 'Bi Weekly';
		}
		else if(value == 'MONTHLY') {
			return 'Monthly';
		}
		else if(value == 'YEARLY') {
			return 'Yearly';
		}
		else if(value == 'QUARTERLY') {
			return 'Quarterly';
		}
		else if(value == 'HALF_YEARLY') {
			return 'Half Yearly';
		}
		else if(value == 'NOT_REGULAR') {
			return 'Not Regular';
		}
	}
}]);



app.filter('FillingType',[ function() {
	
	return function(value) {
		
		if(value == 'PRE_SHIPMENT' || value == false) {
			return 'Pre Shipment';
		}
		else if(value == 'POST_SHIPMENT' || value == true) {
			return 'Post Shipment';
		}
		
	}
}]);


app.filter('TermCode',[ function() {
	
	return function(value) {
		
		if(value == 'CASH') {
			return 'Cash';
		}
		else if(value == 'CREDIT') {
			return 'Credit';
		}
		else if(value == 'POSTDATEDCHEQUE') {
			return 'Post Dated Cheque';
		}
		
	}
}]);


app.filter('TaxExempted',[ function() {
	
	return function(value) {
		
		if(value == 'SERVICETAX') {
			return 'Service Tax';
		}
		else if(value == 'VAT') {
			return 'VAT';
		}
		
	}
}]);

app.filter('MailEventType',[ function() {
	
	return function(value) {
		
		if(value == 'BOOKING_CONFIRM') {
			return 'Booking Confirm';
		}
		else if(value == 'BOOKING_CANCEL') {
			return 'Booking Cancel';
		}
		
	}
}]);

app.filter('EventMasterType',[ function() {
	
	return function(value) {
		
		if(value == 'CARGO_RECEIVED') {
			return 'Cargo Received';
		}
		else if(value == 'CARGO_ARRIVAL') {
			return 'Cargo Arrival';
		}
		
	}
}]);

app.filter('MessageSendType',[ function() {
	
	return function(value) {
		
		if(value == 'INTERNAL' || value == 'Internal') {
			return 'Internal';
		}
		else if(value == 'EXTERNAL' || value == 'External') {
			return 'External';
		}
		
	}
}]);

app.filter('chargeType',[ function() {
	
	return function(value) {
		
		if(value == 'CHARGEABLE') {
			return 'No';
		}
		else if(value == 'ACTUAL') {
			return 'Yes';
		}
		
	}
}]);

app.filter('InvoiceCreditNoteStatusFilter',[ function() {
	
	return function(value) {
		
		if(value == 'Awaiting_Payment') {
			return 'Awaiting Payment';
		}
		
		
	}
}]);

app.filter('ReferenceType',[ function() {
	
	return function(value) {
		
		if(value == 'Customer_Order_No') {
			return 'Customer Order No';
		}else if(value == 'Shipper_Reference_No') {
			return 'Shipper Reference No';
		}
		
		
	}
}]);


app.filter('orderObjectBy',[ function() {
	  return function(items, field, reverse) {
	    var filtered = [];
	    angular.forEach(items, function(item) {
	      filtered.push(item);
	    });
	    filtered.sort(function (a, b) {
	      return ((a[field]).toLowerCase() > (b[field]).toLowerCase() ? -1 : 1);
	    });
	    if(reverse) 
	    filtered.reverse();
	    return filtered;
	  };
	}]);


app.filter('SingOffUnSingOff',[ function() {
	
	return function(value) {
		
		if(value == 'SignOff') {
			return 'Sign Off';
		}
		if(value == 'UnSignOff') {
			return 'UnSign Off';
		}
	}
	
}]);


app.filter('AuditFilter',[ function() {
	
	return function(value) {
		
		if(value == 'ADD') {
			return 'Inserted';
		}
		else if(value == 'MOD') {
			return 'Updated';
		}
		else if(value == 'DEL') {
			return 'Deleted';
		}
		else  {
			return value;
		}
	}
	
}]);

app.filter('hideUndefined',[ function() {
    return function(items) {
        var filtered_items = [];

        //iterate over all emails
        for(var i = 0; i < items.length; i++) {
            //if not undefined, add to filtered_items array
            if(items[i].locationMaster !== undefined) {
                filtered_items.push(items[i])
            }
        }
        return filtered_items;
    }
}])

app.filter('QuotationStatus',[ function() {
	
	return function(value) {
		
		if(value == 'Pending') {
			return 'Pending';
		}
		else if(value == 'Approved') {
			return 'Approved';
		}
		else if(value == 'ClientApproved') {
			return 'Client Approved';
		}
		else if(value == 'Rejected') {
			return 'Rejected';
		}
		else if(value == 'Gained') {
			return 'Gained';
		}
		else  {
			return value;
		}
	}
	
}]);
