app.controller('UnitController',['$rootScope', '$scope', '$stateParams', '$state', '$window', 'ngDialog', 
		'UnitSearch', 'UnitRemove', 'UnitService', 'roleConstant',
	function($rootScope, $scope, $stateParams, $state, $window, ngDialog, 
		UnitSearch, UnitRemove, UnitService, roleConstant) {

    /************************************************************
     * ux - by Muthu
     * reason - ipad compatibility fix
     *
     * *********************************************************/
    $scope.deskTopView = true;

    /********************* ux fix ends here *****************************/

    $scope.unitHeadArr = [{
        "name": "#",
        "width": "col-xs-0half",
        "model": "no",
        "search": false,
    }, {
        "name": "Name",
        "width": "col-xs-5",
        "model": "unitName",
        "search": true,
        "wrap_cell": true,
        "type": "text",
        "sort": true,
        "key": "searchUnitName"

    }, {
        "name": "Code",
        "width": "col-xs-1half",
        "model": "unitCode",
        "search": true,
        "type": "text",
        "sort": true,
        "key": "searchUnitCode"
    }, {
        "name": "Type",
        "width": "col-xs-2",
        "model": "unitType",
        "search": true,
        "type": "drop",
        "data": $rootScope.enum['UnitType'],
        "sort": true,
        "key": "searchUnitType"
    }, {
        "name": "Status",
        "width": "col-xs-3",
        "model": "status",
        "search": true,
        "type": "drop",
        "data": $rootScope.enum['LovStatus'],
        "sort": true,
        "key": "searchStatus"
    }]

    $scope.sortSelection = {
        sortKey: "unitName",
        sortOrder: "asc"
    }



    $scope.init = function() {

        console.log("Unit Init method called.");

        $rootScope.setNavigate1("Master");
        $rootScope.setNavigate2("Unit");

        UnitService.set({});
        $scope.limitArr = [10, 15, 20];
        $scope.page = 0;
        $scope.limit = 10;

        $scope.searchDto = {};
        $scope.search();
    };


    $scope.rowSelect = function(data) {
        if ($rootScope.roleAccess(roleConstant.MASTER_GENERAL_UNIT_VIEW)) {
            console.log("Selected Unit ", data);
            $scope.unitMaster = data;

            /************************************************************
             * ux - by Muthu
             * reason - ipad compatibility fix
             *
             * *********************************************************/

            var windowInner = $window.innerWidth;
            if (windowInner <= 1199) {
                $scope.deskTopView = false;
            } else {
                $scope.deskTopView = true;
            }
            angular.element($window).bind('resize', function() {
                if ($window.innerWidth >= 1200) {
                    $scope.deskTopView = true;
                } else if ($window.innerWidth <= 1199) {
                    if ($scope.unitMaster.id != null) {
                        $scope.deskTopView = false;
                    } else {
                        $scope.deskTopView = true;
                    }
                }

                console.log("window resizing..." + $window.innerWidth);
                $scope.$digest();
            });

            /********************* ux fix ends here *****************************/
        }
    }


    $scope.addUnit = function() {
        if ($rootScope.roleAccess(roleConstant.MASTER_GENERAL_UNIT_CREATE)) {
            console.log("Add method is called.");
            UnitService.set({});
            $state.go("layout.addUnit");
        }
    };

    $scope.changeSearch = function(param) {
        for (var attrname in param) {
            $scope.searchDto[attrname] = param[attrname];
        }
        $scope.page = 0;
        $scope.search();
        console.log("changeSearch called ", $scope.searchDto);
    }

    $scope.changepage = function(param) {
        $scope.page = param.page;
        $scope.limit = param.size;
        $scope.search();
        console.log("ChangePage called ", param.page);
    }

    $scope.limitChange = function(item) {
        $scope.page = 0;
        $scope.limit = item;
        $scope.search();
        console.log("LimitChange called ", item);
    }

    $scope.sortChange = function(param) {
        $scope.searchDto.orderByType = param.sortOrder.toUpperCase();
        $scope.searchDto.sortByColumn = param.sortKey;
        $scope.search();
        console.log("sortChange is called ", $scope.searchDto);
    }


    $scope.search = function() {

        console.log("Search is called.");

        $scope.searchDto.selectedPageNumber = $scope.page;
        $scope.searchDto.recordPerPage = $scope.limit;

        $scope.unitArr = [];
        UnitSearch.query($scope.searchDto).$promise.then(function(data, status) {
            $scope.totalRecord = data.responseObject.totalRecord;
            var tempArr = [];
            var resultArr = [];
            tempArr = data.responseObject.searchResult;
            var tempObj = {};
            angular.forEach(tempArr, function(item, index) {
                tempObj = item;
                tempObj.no = (index + 1) + ($scope.page * $scope.limit);
                resultArr.push(tempObj);
                tempObj = {};
            });
            $scope.unitArr = resultArr;
            console.log("$scope.unitArr ", $scope.unitArr);
        });

    }


    $scope.cancel = function() {
        console.log("Cancel method is called.");
        $scope.showHistory = false;
        $scope.unitMaster = {};
        /************************************************************
         * ux - by Muthu
         * reason - ipad compatibility fix
         *
         * *********************************************************/
        $scope.deskTopView = true;

        /********************* ux fix ends here *****************************/
    };

    $scope.edit = function() {
        if ($rootScope.roleAccess(roleConstant.MASTER_GENERAL_UNIT_MODIFY)) {
            console.log("Edit method is called.");
            $state.go("layout.editUnit", {
                unitId: $scope.unitMaster.id
            });
        }
    };


    $scope.deleteUnit = function() {
        if ($rootScope.roleAccess(roleConstant.MASTER_GENERAL_UNIT_DELETE)) {
            var newScope = $scope.$new();
            newScope.errorMessage = $rootScope.nls["ERR230"];
            ngDialog.openConfirm({
                template: '<p>{{errorMessage}}</p>' +
                    '<div class="ngdialog-footer">' +
                    '<button type="button" class="btn btn-default btn-property cancel-btn" ng-click="closeThisDialog(0)">No</button>' +
                    '<button type="button" class="btn btn-primary btn-property accent-btn" ng-click="confirm(1)">Yes</button>' +
                    '</div>',
                plain: true,
                scope: newScope,
                className: 'ngdialog-theme-default'
            }).then(function(value) {
                UnitRemove.remove({
                    id: $scope.unitMaster.id
                }, function(data) {
                    if (data.responseCode == 'ERR0') {
                        console.log("Unit deleted Successfully");
                        $scope.init();
                        $scope.deskTopView = true;
                    } else {
                        console.log("Unit deleted Failed ", data.responseDescription);
                    }
                }, function(error) {
                    console.log("Unit deleted Failed : ", error);
                });
            }, function(value) {
                console.log("deleted cancelled");
            });
        }
    }


    switch ($stateParams.action) {
        case "SEARCH":
            $scope.init();
            $rootScope.breadcrumbArr = [{
                    label: "Master",
                    state: "layout.unit"
                },
                {
                    label: "Unit",
                    state: "layout.unit"
                }
            ];
            break;
        case "VIEW":
            break;
    }
}]);