app.controller('UnitOperationController',['$rootScope', '$scope', '$http', '$stateParams', '$state', 'appConstant', 'CommonValidationService',
    '$location', 'UnitAdd', 'UnitEdit', 'UnitView', 'UnitService', 'ngProgressFactory', 
    'ngDialog', 'discardService', '$filter', 'cloneService',
	function($rootScope, $scope, $http, $stateParams, $state, appConstant, CommonValidationService,
    $location, UnitAdd, UnitEdit, UnitView, UnitService, ngProgressFactory, 
    ngDialog, discardService, $filter, cloneService) {


    $scope.unitMaster = {};

    $scope.cancelTab = false;

    $scope.initForAdd = function(acFlag) {

        $scope.firstFocus = true;
        if ($scope.unitMaster == undefined || $scope.unitMaster == null) {
            $scope.unitMaster = {};
        }
        $scope.showOptional = false;

        $rootScope.setNavigate3("Unit");
        $rootScope.setNavigate3("Add Unit");

        $scope.contained_progressbar = ngProgressFactory.createInstance();
        $scope.contained_progressbar.setParent(document.getElementById('unit-panel'));
        $scope.contained_progressbar.setAbsolute();

        $scope.unitMaster.status = 'Active';
        if ($scope.unitMaster.mappingUnit2 != null)
            $scope.showOptional = true;

        var tempData = $rootScope.enum['MappingUnit'];
        $scope.mappingUnitData = [];
        for (var i = 0; i < tempData.length; i++) {
            $scope.mappingUnitData.push($filter('MappingUnit')(tempData[i]));

        }

        console.log("mapping data", $scope.mappingUnitData);


        $scope.changeMappingUnit2();

        if (acFlag != undefined && acFlag != null && acFlag === 0) {
            $scope.setOldDataVal();
        }
    };

    $scope.setOldDataVal = function() {
        $scope.oldData = JSON.stringify($scope.unitMaster);
        console.log("UnitMaster Data Loaded");
    }


    $scope.initForEdit = function() {
        $scope.firstFocus = true;
        $scope.showOptional = false;

        $rootScope.setNavigate3("Unit");
        $rootScope.setNavigate3("Edit Unit");

        $scope.contained_progressbar = ngProgressFactory.createInstance();
        $scope.contained_progressbar.setParent(document.getElementById('unit-panel'));
        $scope.contained_progressbar.setAbsolute();

        if ($scope.unitMaster.mappingUnit2 != null)
            $scope.showOptional = true;

        var tempData = $rootScope.enum['MappingUnit'];
        $scope.mappingUnitData = [];
        for (var i = 0; i < tempData.length; i++) {
            $scope.mappingUnitData.push($filter('MappingUnit')(tempData[i]));

        }
        $scope.changeMappingUnit2();
    }


    $scope.cancel = function() {
        if ($scope.oldData != JSON.stringify($scope.unitMaster)) {
            var newScope = $scope.$new();
            newScope.errorMessage = $rootScope.nls["ERR200"];
            ngDialog.openConfirm({
                template: '<p>{{errorMessage}}</p>' +
                    '<div class="ngdialog-footer">' +
                    ' <button type="button" class="btn btn-default btn-property cancel-btn" ng-click="confirm(2)">No</button>' +
                    '<button type="button" class="btn btn-default btn-property accent-btn " ng-click="confirm(1)">Yes</button>' +
                    '<button type="button" class="btn btn-default btn-property pull-left grey-cancel-btn" ng-click="confirm(3)">Cancel</button>' +
                    '</div>',
                plain: true,
                scope: newScope,
                className: 'ngdialog-theme-default'
            }).then(function(value) {
                if (value == 1 && $scope.unitMaster.id == null) {
                    $scope.save();
                } else if (value == 1 && $scope.unitMaster.id != null) {
                    $scope.update();
                } else if (value == 2) {
                    var params = {};
                    params.submitAction = 'Cancelled';
                    $state.go("layout.unit", params);
                } else {
                    console.log("cancelled");
                }
            });
        } else {
            var params = {};
            params.submitAction = 'Cancelled';
            $state.go("layout.unit", params);
        }
        UnitService.set({});
    }



    $scope.update = function() {
        console.log("Update Method is called.");



        if ($scope.validateUnitMaster(0)) {


            $scope.unitMasterTemp = cloneService.clone($scope.unitMaster);

            if ($scope.unitMasterTemp.mappingUnit1 != null) {
                $scope.unitMasterTemp.mappingUnit1 = $filter('MappingUnitReverse')($scope.unitMasterTemp.mappingUnit1);
            }

            if ($scope.unitMasterTemp.mappingUnit2 != null) {
                $scope.unitMasterTemp.mappingUnit2 = $filter('MappingUnitReverse')($scope.unitMasterTemp.mappingUnit2);
            }


            $scope.contained_progressbar.start();

            UnitEdit.update($scope.unitMasterTemp).$promise.then(
                function(data) {
                    if (data.responseCode == 'ERR0') {
                        console.log("Unit updated Successfully")
                        UnitService.set({});
                        $rootScope.successDesc = $rootScope.nls["ERR401"];
                        var params = {}
                        params.submitAction = 'Saved';
                        $state.go("layout.unit", params);
                    } else {
                        console.log("Unit updated Failed ", data.responseDescription)
                    }
                    $scope.contained_progressbar.complete();
                    angular.element(".panel-body").animate({
                        scrollTop: 0
                    }, "slow");
                },
                function(error) {
                    console.log("Unit updated Failed : ", error)
                });
        } else {
            console.log("Invalid Form Submission.");
        }
    }

    $scope.tabNavigateTo = function(nextIdValue) {
        $rootScope.navigateToNextField(nextIdValue);
    }



    $scope.save = function() {
        console.log("Save Method is called.");




        if ($scope.validateUnitMaster(0)) {
            $scope.contained_progressbar.start();


            $scope.unitMasterTemp = cloneService.clone($scope.unitMaster);

            if ($scope.unitMasterTemp.mappingUnit1 != null) {
                $scope.unitMasterTemp.mappingUnit1 = $filter('MappingUnitReverse')($scope.unitMasterTemp.mappingUnit1);
            }

            if ($scope.unitMasterTemp.mappingUnit2 != null) {
                $scope.unitMasterTemp.mappingUnit2 = $filter('MappingUnitReverse')($scope.unitMasterTemp.mappingUnit2);
            }




            UnitAdd.save($scope.unitMasterTemp).$promise.then(function(data) {
                if (data.responseCode == 'ERR0') {
                    console.log("Unit Saved Successfully")
                    $rootScope.successDesc = $rootScope.nls["ERR400"];
                    UnitService.set({});
                    var params = {}
                    params.submitAction = 'Saved';
                    $state.go("layout.unit", params);
                } else {
                    console.log("Unit added Failed ", data.responseDescription)
                }
                $scope.contained_progressbar.complete();
                angular.element(".panel-body").animate({
                    scrollTop: 0
                }, "slow");
            }, function(error) {
                console.log("Unit added Failed : ", error)
            });
        } else {
            console.log("Invalid Form Submission.");
        }
    }


    $scope.changeMappingUnit2 = function(nextIdValue) {
        if ($scope.showOptional) {
            $rootScope.navigateToNextField(nextIdValue);
        }
        if (!$scope.showOptional) {
            $scope.showOptional = false;
            $scope.unitMaster.mappingUnit2 = null;
            $scope.unitMaster.calcType2 = null;
            $scope.unitMaster.calcValue2 = null;
        }
    }

    // Added for the bug : NES-1362 start

    function checkTabPress(e) {
        'use strict';
        var ele = document.activeElement;
        //var x = document.getElementById("item2").previousSibling.innerHTML;
        if (e.which === 9 && e.shiftKey) {
            if ($scope.cancelTab) {
                var tabIdValue = 'decimals';
                $rootScope.navigateToNextField(tabIdValue);
                $scope.cancelTab = false;
            }

            if (ele.id === 'cancel') {
                $scope.cancelTab = true;
            }

        }
    }
    document.addEventListener('keyup', function(e) {
        checkTabPress(e);
    }, false);

    // Added for the bug : NES-1362 start


    $scope.validateUnitMaster = function(validateCode, nextIdValue) {

        $scope.firstFocus = false;

        console.log("Validate Called", validateCode);

        $scope.errorMap = new Map();

        if (validateCode == 0 || validateCode == 1) {
            if ($scope.unitMaster.unitName == undefined ||
                $scope.unitMaster.unitName == null ||
                $scope.unitMaster.unitName == "") {
                $scope.errorMap.put("unitName", $rootScope.nls["ERR1707"]);
                var tabIdValue = 'unitName';
                $rootScope.navigateToNextField(tabIdValue);
                return false;
            } else {

                if (!CommonValidationService.checkRegExp(appConstant.Reg_Exp_Master_Unit_Name, $scope.unitMaster.unitName)) {
                    $scope.errorMap.put("unitName", $rootScope.nls["ERR1706"]);
                    var tabIdValue = 'unitName';
                    $rootScope.navigateToNextField(tabIdValue);
                    return false;
                }

            }
        }

        if (validateCode == 0 || validateCode == 2) {
            if ($scope.unitMaster.unitCode == undefined ||
                $scope.unitMaster.unitCode == null ||
                $scope.unitMaster.unitCode == "") {
                $scope.errorMap.put("unitCode", $rootScope.nls["ERR1705"]);
                var tabIdValue = 'unitCode';
                $rootScope.navigateToNextField(tabIdValue);
                return false;
            } else {

                if (!CommonValidationService.checkRegExp(appConstant.Reg_Exp_Master_Unit_Code, $scope.unitMaster.unitCode)) {
                    $scope.errorMap.put("unitCode", $rootScope.nls["ERR1704"]);
                    var tabIdValue = 'unitCode';
                    $rootScope.navigateToNextField(tabIdValue);
                    return false;
                }
            }
        }

        if (validateCode == 0 || validateCode == 3) {
            if ($scope.unitMaster.unitType == undefined ||
                $scope.unitMaster.unitType == null ||
                $scope.unitMaster.unitType == "") {
                $scope.errorMap.put("unitType", $rootScope.nls["ERR1732"]);
                var tabIdValue = 'unitType';
                $rootScope.navigateToNextField(tabIdValue);
                return false;
            }
        }

        if (validateCode == 0 || validateCode == 4) {
            if ($scope.unitMaster.unitType == 'Unit' &&
                ($scope.unitMaster.mappingUnit1 == undefined ||
                    $scope.unitMaster.mappingUnit1 == null || $scope.unitMaster.mappingUnit1 == "")) {
                $scope.errorMap.put("mappingUnit1", $rootScope.nls["ERR1713"]);
                var tabIdValue = 'mappingUnit1';
                $rootScope.navigateToNextField(tabIdValue);
                return false;

            }
        }

        if (validateCode == 0 || validateCode == 5) {
            if ($scope.unitMaster.unitType == 'Unit' &&
                ($scope.unitMaster.calcType1 == undefined ||
                    $scope.unitMaster.calcType1 == null || $scope.unitMaster.calcType1 == "")) {
                $scope.errorMap.put("calcType1", $rootScope.nls["ERR1716"]);
                var tabIdValue = 'calcType1';
                $rootScope.navigateToNextField(tabIdValue);
                return false;

            }
        }

        if (validateCode == 0 || validateCode == 6) {
            if ($scope.unitMaster.unitType == 'Unit' && ($scope.unitMaster.calcValue1 == undefined ||
                    $scope.unitMaster.calcValue1 == null ||
                    $scope.unitMaster.calcValue1 == "")) {
                $scope.errorMap.put("calcValue1", $rootScope.nls["ERR1718"]);
                var tabIdValue = 'calcValue1';
                $rootScope.navigateToNextField(tabIdValue);
                return false;
            } else if ($scope.unitMaster.unitType == 'Unit') {

                if (!CommonValidationService.checkRegExp(appConstant.Reg_Exp_Master_Unit_Calculation_1, $scope.unitMaster.calcValue1)) {
                    $scope.errorMap.put("calcValue1", $rootScope.nls["ERR1724"]);
                    var tabIdValue = 'calcValue1';
                    $rootScope.navigateToNextField(tabIdValue);
                    return false;
                }

            }
        }

        if (validateCode == 0 || validateCode == 7) {
            if ($scope.unitMaster.unitType == 'Unit' && $scope.showOptional == true &&
                ($scope.unitMaster.mappingUnit2 == undefined ||
                    $scope.unitMaster.mappingUnit2 == null || $scope.unitMaster.mappingUnit2 == "")) {
                $scope.errorMap.put("mappingUnit2", $rootScope.nls["ERR1726"]);
                var tabIdValue = 'mappingUnit2';
                $rootScope.navigateToNextField(tabIdValue);
                return false;

            }
        }


        if (validateCode == 0 || validateCode == 8) {
            if ($scope.unitMaster.unitType == 'Unit' && $scope.showOptional == true &&
                ($scope.unitMaster.calcType2 == undefined ||
                    $scope.unitMaster.calcType2 == null || $scope.unitMaster.calcType2 == "")) {
                $scope.errorMap.put("calcType2", $rootScope.nls["ERR1727"]);
                var tabIdValue = 'calcType2';
                $rootScope.navigateToNextField(tabIdValue);
                return false;

            }
        }


        if (validateCode == 0 || validateCode == 9) {
            if ($scope.unitMaster.unitType == 'Unit' && $scope.showOptional == true && ($scope.unitMaster.calcValue2 == undefined ||
                    $scope.unitMaster.calcValue2 == null ||
                    $scope.unitMaster.calcValue2 == "")) {
                $scope.errorMap.put("calcValue2", $rootScope.nls["ERR1731"]);
                var tabIdValue = 'calcType2';
                $rootScope.navigateToNextField(tabIdValue);
                return false;
            } else if ($scope.showOptional == true) {

                if (!CommonValidationService.checkRegExp(appConstant.Reg_Exp_Master_Unit_Calculation_2, $scope.unitMaster.calcValue2)) {
                    $scope.errorMap.put("calcValue2", $rootScope.nls["ERR1729"]);
                    var tabIdValue = 'calcType2';
                    $rootScope.navigateToNextField(tabIdValue);
                    return false;
                }
            }
        }


        return true;

    }
    $scope.setStatus = function(data) {
        if (data == 'Active') {
            return 'activetype';
        } else if (data == 'Block') {
            return 'blockedtype';
        } else if (data == 'Hide') {
            return 'hiddentype';
        }
    }


    $scope.getUnitById = function(unitId) {
        console.log("getUnitById ", unitId);

        UnitView.get({
            id: unitId
        }, function(data) {
            if (data.responseCode == "ERR0") {
                console.log("Successful while getting unit.", data)
                $scope.unitMasterTemp = data.responseObject;

                if ($scope.unitMasterTemp.mappingUnit1 != null) {
                    $scope.unitMasterTemp.mappingUnit1 = $filter('MappingUnit')($scope.unitMasterTemp.mappingUnit1);
                }

                if ($scope.unitMasterTemp.mappingUnit2 != null) {
                    $scope.unitMasterTemp.mappingUnit2 = $filter('MappingUnit')($scope.unitMasterTemp.mappingUnit2);
                }

                $scope.unitMaster = $scope.unitMasterTemp;

            }
            $scope.setOldDataVal();
        }, function(error) {
            console.log("Error while getting unit.", error)
        });

    }

    //On leave the Unfilled Unit Form Broadcast then event will happen in StateChangeListener(BroadCaster)-app_state_route.js

    $scope.$on('addUnitEvent', function(events, args) {

        $rootScope.unfinishedData = $scope.unitMaster;
        $rootScope.category = "Unit Master";
        $rootScope.unfinishedFormTitle = "Unit (New)";
        if ($scope.unitMaster != undefined && $scope.unitMaster != null && $scope.unitMaster.unitName != undefined &&
            $scope.unitMaster.unitName != null && $scope.unitMaster.unitName != "") {
            $rootScope.subTitle = $scope.unitMaster.unitName;
        } else {
            $rootScope.subTitle = "Unknown Unit"
        }
    })

    $scope.$on('editUnitEvent', function(events, args) {
        $rootScope.unfinishedData = $scope.unitMaster;
        $rootScope.category = "Unit Master";
        $rootScope.unfinishedFormTitle = "Unit Edit " + $scope.unitMaster.unitCode;
        if ($scope.unitMaster != undefined && $scope.unitMaster != null && $scope.unitMaster.unitName != undefined &&
            $scope.unitMaster.unitName != null && $scope.unitMaster.unitName != "") {
            $rootScope.subTitle = $scope.unitMaster.unitName;
        } else {
            $rootScope.subTitle = "Unknown Unit"
        }
    })


    $scope.$on('addUnitEventReload', function(e, confirmation) {
        confirmation.message = "";
        localStorage.reloadFormData = JSON.stringify($scope.unitMaster);
        localStorage.isUnitReloaded = "YES";
        e.preventDefault();
    });


    $scope.$on('editUnitEventReload', function(e, confirmation) {
        confirmation.message = "";
        localStorage.reloadFormData = JSON.stringify($scope.unitMaster);
        localStorage.isUnitReloaded = "YES";
        e.preventDefault();
    });


    $scope.isReloaded = localStorage.isUnitReloaded;

    if ($stateParams.action == "ADD") {
        if ($stateParams.fromHistory === 'Yes') {
            if ($scope.isReloaded == "YES") {
                console.log("Add History Reload...")
                $scope.isReloaded = "NO";
                localStorage.isUnitReloaded = "NO";
                $scope.unitMaster = JSON.parse(localStorage.reloadFormData);
                $scope.initForAdd();
            } else {
                console.log("Add History NotReload...")
                $scope.unitMaster = $rootScope.selectedUnfilledFormData;
                $scope.initForAdd();
            }
            $scope.oldData = "";
        } else {
            if ($scope.isReloaded == "YES") {
                $scope.isReloaded = "NO";
                localStorage.isUnitReloaded = "NO";
                $scope.unitMaster = JSON.parse(localStorage.reloadFormData);
                $scope.initForAdd();
                $scope.oldData = "";
                console.log("Add NotHistory Reload...")
            } else {
                console.log("Add NotHistory NotReload...")
                $scope.initForAdd(0);
            }
        }
        $rootScope.breadcrumbArr = [{
                label: "Master",
                state: "layout.unit"
            },
            {
                label: "Unit",
                state: "layout.unit"
            },
            {
                label: "Add Unit",
                state: null
            }
        ];


    } else {
        if ($stateParams.fromHistory === 'Yes') {
            if ($scope.isReloaded == "YES") {
                $scope.isReloaded = "NO";
                localStorage.isUnitReloaded = "NO";
                $scope.unitMaster = JSON.parse(localStorage.reloadFormData);
                $scope.initForEdit();
            } else {
                $scope.unitMaster = $rootScope.selectedUnfilledFormData;
                $scope.initForEdit();
            }
            $scope.oldData = "";
        } else {
            if ($scope.isReloaded == "YES") {
                $scope.isReloaded = "NO";
                localStorage.isUnitReloaded = "NO";
                $scope.unitMaster = JSON.parse(localStorage.reloadFormData);
                $scope.initForEdit();
                $scope.oldData = "";
            } else {
                $scope.getUnitById($stateParams.unitId);
                $scope.initForEdit();
            }
        }
        $rootScope.breadcrumbArr = [{
                label: "Master",
                state: "layout.unit"
            },
            {
                label: "Unit",
                state: "layout.unit"
            },
            {
                label: "Edit Unit",
                state: null
            }
        ];
    }

}]);