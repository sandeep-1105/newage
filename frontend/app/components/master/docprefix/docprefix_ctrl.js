app.controller('DocPrefixCtrl',['$rootScope', '$scope', '$http', '$location', '$window', 'Upload',
    'ngTableParams', 'DocumentSearch', 'DocumentPrefixView', 'discardService',
    'ngDialog', 'DocumentPrefixRemove', 'documentPrefixService', '$stateParams', '$state', 'roleConstant',
	function($rootScope, $scope, $http, $location, $window, Upload,
    ngTableParams, DocumentSearch, DocumentPrefixView, discardService,
    ngDialog, DocumentPrefixRemove, documentPrefixService, $stateParams, $state, roleConstant) {
    console.log("Carrier controller loaded..");
    /************************************************************
     * ux - by Muthu
     * reason - ipad compatibility fix
     *
     * *********************************************************/
    $scope.deskTopView = true;

    /********************* ux fix ends here *****************************/
    $scope.docPrefixHeadArr = [{
        "name": "#",
        "width": "col-xs-0half",
        "model": "no",
        "search": false
    }, {
        "name": "Name",
        "width": "col-xs-3",
        "model": "documentName",
        "search": true,
        "wrap_cell": true,
        "type": "text",
        "sort": true,
        "key": "searchDocumentName"

    }, {
        "name": "Code",
        "width": "col-xs-2half",
        "model": "documentCode",
        "search": true,
        "type": "text",
        "sort": true,
        "key": "searchDocumentCode"
    }, {
        "name": "Prefix",
        "width": "w80px",
        "model": "docPrefix",
        "search": true,
        "type": "text",
        "sort": true,
        "key": "searchDocumentPrefix"
    }, {
        "name": "Suffix",
        "width": "col-xs-2",
        "model": "docSuffix",
        "search": true,
        "type": "text",
        "sort": true,
        "key": "searchDocumentSuffix"
    }, {
        "name": "Status",
        "width": "col-xs-3",
        "model": "status",
        "search": true,
        "sort": true,
        "type": "drop",
        "data": $rootScope.enum['LovStatus'],
        "key": "searchStatus"
    }]
    $scope.sortSelection = {
        sortKey: "documentName",
        sortOrder: "asc"
    }

    $scope.docPrefixMaster = {};
    $scope.searchDto = {};
    documentPrefixService.set({});
    $scope.limitArr = [10, 15, 20];
    $scope.page = 0;
    $scope.limit = 10;

    $scope.rowSelect = function(data) {
        if ($rootScope.roleAccess(roleConstant.MASTER_GENERAL_DOCUMENT_PREFIX_VIEW)) {
            console.log(data.id)
            DocumentPrefixView.get({
                id: data.id
            }, function(data) {
                if (data.responseCode == "ERR0") {
                    console.log("docPrefixMaster view Successfully")
                    $scope.docPrefixMaster = data.responseObject;
                } else {
                    console.log("docPrefixMaster view Failed " +
                        data.responseDescription)
                }
            }, function(error) {
                console.log("docPrefixMaster view Failed : " + error)
            });
            /************************************************************
             * ux - by Muthu
             * reason - ipad compatibility fix
             *
             * *********************************************************/

            var windowInner = $window.innerWidth;
            if (windowInner <= 1199) {
                $scope.deskTopView = false;

            } else {
                $scope.deskTopView = true;
            }
            angular.element($window).bind('resize', function() {
                if ($window.innerWidth >= 1200) {
                    $scope.deskTopView = true;
                } else if ($window.innerWidth <= 1199) {
                    if ($scope.docPrefixMaster.id !== false) {
                        $scope.deskTopView = false;
                    } else {
                        $scope.deskTopView = true;
                    }
                }

                console.log("window resizing..." + $window.innerWidth);
                $scope.$digest();
            });

            /********************* ux fix ends here *****************************/
        }
    }

    $scope.init = function() {
        console.log("Document Prefix Init method called.");
        $rootScope.setNavigate1("Master");
        $rootScope.setNavigate2("Document Prefix");

        $scope.docPrefixMaster = {};
        documentPrefixService.set({});
        $scope.searchDto = {};

        $scope.documentSortIconClass = " ";
        $scope.sortIconDocumentName = "fa fa-chevron-down";
        $scope.searchDto.orderByType = "ASC";
        $scope.searchDto.sortByColumn = "documentName";

        $scope.search();

    };

    $scope.cancel = function() {
        $scope.docPrefixMaster = {};
        /************************************************************
         * ux - by Muthu
         * reason - ipad compatibility fix
         *
         * *********************************************************/
        $scope.deskTopView = true;

        /********************* ux fix ends here *****************************/
    };

    $scope.add = function() {
        if ($rootScope.roleAccess(roleConstant.MASTER_GENERAL_DOCUMENT_PREFIX_CREATE)) {
            $scope.docPrefixMaster = {};
            documentPrefixService.set({});
            $state.go("layout.addDocprefix");
        }
    };




    $scope.edit = function() {
        if ($rootScope.roleAccess(roleConstant.MASTER_GENERAL_DOCUMENT_PREFIX_MODIFY)) {
            documentPrefixService.set($scope.docPrefixMaster);
            $state.go("layout.editDocprefix", {
                docprefixId: $scope.docPrefixMaster.id
            });
        }
    };



    $scope.changepage = function(param) {
        $scope.page = param.page;
        $scope.limit = param.size;
        $scope.search();
    }

    $scope.view = function(selectedMaster) {
        console.log("View document Called");
        $scope.docPrefixMaster = selectedMaster;
    };
    $scope.limitChange = function(item) {
        $scope.page = 0;
        $scope.limit = item;
        $scope.search();
    }
    $scope.sortChange = function(param) {
        $scope.searchDto.orderByType = param.sortOrder
            .toUpperCase();
        $scope.searchDto.sortByColumn = param.sortKey;
        $scope.search();
    }
    $scope.deleteMaster = function() {
        if ($rootScope.roleAccess(roleConstant.MASTER_GENERAL_DOCUMENT_PREFIX_DELETE)) {
            var newScope = $scope.$new();
            newScope.errorMessage = $rootScope.nls["ERR238"];
            ngDialog.openConfirm({
                template: '<p>{{errorMessage}}</p>' +
                    '<div class="ngdialog-footer">' +
                    '<button type="button" class="btn btn-default btn-property cancel-btn" ng-click="closeThisDialog(0)">Cancel' +
                    '<button type="button" class="btn btn-primary btn-property accent-btn" ng-click="confirm(1)">Yes' +
                    '</button></div>',
                plain: true,
                scope: newScope,
                className: 'ngdialog-theme-default'
            }).then(
                function(value) {
                    DocumentPrefixRemove
                        .remove({
                                id: $scope.docPrefixMaster.id
                            },
                            function(data) {
                                if (data.responseCode == 'ERR0') {
                                    console
                                        .log("Document Prefix deleted Successfully")
                                    $scope
                                        .init();
                                    $scope.deskTopView = true;
                                } else {
                                    console
                                        .log("Document Prefix deleted Failed " +
                                            data.responseDescription)
                                }
                            },
                            function(error) {
                                console
                                    .log("Document Prefix deleted Failed : " +
                                        error)
                            });
                },
                function(value) {
                    console
                        .log("Document Prefix deleted cancelled");

                });
        }
    }

    $scope.changeSearch = function(param) {
        for (var attrname in param) {
            $scope.searchDto[attrname] = param[attrname];
        }
        $scope.page = 0;
        $scope.cancel();
        $scope.search();

    }

    $scope.mouseOver = function(column) {

        if ($scope.searchDto.sortByColumn != column) {
            if ($scope.documentSortIconClass == "icon-downarrow") {
                $scope.documentSortIconClass = "icon-uparrow";
            } else {
                $scope.documentSortIconClass = "icon-downarrow";
            }
        }
        $scope.searchDto.sortByColumn = column;

    }

    $scope.changeSorting = function(column) {
        $scope.sortIconDocumentName = " ";
        if ($scope.searchDto.sortByColumn != column) {
            $scope.documentSortIconClass = "icon-downarrow";
            $scope.searchDto.orderByType = "ASC";
            $scope.search();

        } else {
            $scope.searchDto.sortByColumn = column;
            if ($scope.searchDto.orderByType == "ASC") {
                $scope.search();
                $scope.searchDto.orderByType = "DESC";
                $scope.documentSortIconClass = "icon-uparrow";
            } else {
                $scope.searchDto.orderByType = "DESC";
                $scope.documentSortIconClass = "icon-uparrow";
                $scope.search();
                $scope.searchDto.orderByType = "ASC";
                $scope.documentSortIconClass = "icon-downarrow";
            }

        }
        $scope.searchDto.sortByColumn = column;
    }
    $scope.search = function() {
        $scope.documentPrefixMaster = {};
        $scope.searchDto.selectedPageNumber = $scope.page;
        $scope.searchDto.recordPerPage = $scope.limit;
        $scope.documentPrefixArr = [];
        $scope.searchDto.selectedPageNumber = $scope.page;
        $scope.searchDto.recordPerPage = $scope.limit;
        DocumentSearch.query($scope.searchDto).$promise
            .then(function(data, status) {
                $scope.totalRecord = data.responseObject.totalRecord;
                var tempArr = [];
                var resultArr = [];
                tempArr = data.responseObject.searchResult;
                var tempObj = {};
                angular.forEach(tempArr, function(item,
                    index) {

                    tempObj = item;
                    tempObj.no = (index + 1) +
                        ($scope.page * $scope.limit);

                    resultArr.push(tempObj);
                    tempObj = {};
                });
                $scope.documentPrefixArr = resultArr;
            });
    }

    switch ($stateParams.action) {
        case "SEARCH":
            console.log("Doc Prefix Master Search Page....");
            $scope.init();
            $rootScope.breadcrumbArr = [{
                    label: "Master",
                    state: "layout.docprefix"
                },
                {
                    label: "Document Prefix",
                    state: "layout.docprefix"
                }
            ];

            break;
    }


}]);