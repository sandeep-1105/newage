
(function() {
	
	app.factory("DocumentSearch",['$resource', function($resource) {
		return $resource("/api/v1/documentprefixmaster/get/search", {}, {
			query : {
				method : 'POST'
			}
		});
	}]);

	app.factory("DocumentPrefixAdd",['$resource', function($resource) {
		return $resource("/api/v1/documentprefixmaster/create", {}, {
			save : {
				method : 'POST'
			}
		});
	}]);

	app.factory("DocumentPrefixUpdate",['$resource', function($resource) {
		return $resource("/api/v1/documentprefixmaster/update", {}, {
			update : {
				method : 'POST'
			}
		});
	}]);

	app.factory("DocumentPrefixView",['$resource', function($resource) {
		return $resource("/api/v1/documentprefixmaster/get/id/:id", {}, {
			get : {
				method : 'GET',
				params : {
					id : ''
				},
				isArray : false
			}
		});
	}]);
	
	
	app.factory("FormulaByDocType",['$resource', function($resource) {
		return $resource("/api/v1/documentprefixmaster/get/formula/doctype/locationcode/:doctype/:locationcode", {}, {
			fetch : {
				method : 'GET',
				params : {
					doctype : '',
					locationcode : ''
				},
				isArray : false
			}
		});
	}]);
	
	app.factory("DocumentPrefixRemove",['$resource', function($resource) {
		return $resource("/api/v1/documentprefixmaster/delete/:id", {}, {
			remove : {
				method : 'DELETE',
				params : {
					id : ''
				},
				isArray : false
			}
		});
	}]);

	app.factory("DocumentTypePrefixRemove",['$resource', function($resource) {
		return $resource("/api/v1/docPrefixTypemappingmaster/delete/:id", {}, {
			remove : {
				method : 'DELETE',
				params : {
					id : ''
				},
				isArray : false
			}
		});
	}]);

})();
