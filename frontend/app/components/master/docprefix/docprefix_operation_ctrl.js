/**
 * 
 */
app.controller('docprefixOperationCtrl',['$rootScope', '$scope', '$timeout', '$http', '$location', 'Upload',
        'DocumentPrefixAdd', 'DocumentPrefixUpdate', 'discardService',
        'DocumentTypePrefixRemove', 'CountryWiseLocation', 'cloneService',
        'ngDialog', 'documentPrefixService', 'ngProgressFactory', 'DocumentPrefixView', 
        '$stateParams', '$state', '$modal', 'appConstant', 'CommonValidationService',
    function($rootScope, $scope, $timeout, $http, $location, Upload,
        DocumentPrefixAdd, DocumentPrefixUpdate, discardService,
        DocumentTypePrefixRemove, CountryWiseLocation, cloneService,
        ngDialog, documentPrefixService, ngProgressFactory, DocumentPrefixView, 
        $stateParams, $state, $modal, appConstant, CommonValidationService) {


        $scope.isFormValueDirty = false;
        $scope.isFormValueDirtyCount = 0;

        $scope.init = function() {
            if ($scope.docPrefixMaster == undefined || $scope.docPrefixMaster == null) {
                $scope.docPrefixMaster = {};
                $scope.docPrefixMaster.addPOR = false;
                $scope.docPrefixMaster.addFDC = false;
                $scope.docPrefixMaster.docPrefixTypeMappingList = [];
                $scope.docPrefixMaster.docPrefixTypeMappingList.push({});
                $scope.docPrefixMaster.status = 'Active';
            }
            if ($scope.docPrefixMaster.id != null) {
                if ($scope.docPrefixMaster.docPrefixTypeMappingList == undefined || $scope.docPrefixMaster.docPrefixTypeMappingList == null) {
                    $scope.docPrefixMaster.docPrefixTypeMappingList = [];
                }
                $scope.docPrefixMaster.docPrefixTypeMappingList.push({});
            }
            $scope.contained_progressbar = ngProgressFactory
                .createInstance();
            $scope.contained_progressbar.setParent(document
                .getElementById('docprefix-panel'));
            $scope.contained_progressbar.setAbsolute();

            $rootScope
                .setNavigate3($scope.docPrefixMaster.id != null ? "Update Document Prefix" :
                    "Add Document Prefix");
            console.log("docPrefixMaster value while loading" +
                $scope.docPrefixMaster)
            discardService.set($scope.docPrefixMaster);
        };

        $scope.$watch('docPrefixMaster', function(newVal, oldVal) {
            if (newVal != undefined && oldVal != undefined) {
                $scope.isFormValueDirtyCount++;
                if ($scope.isFormValueDirtyCount > 1) {
                    $scope.isFormValueDirty = true;
                }
            }
        }, true);

        $scope.cancel = function() {
            var tempObj = $scope.docPrefixMaster;
            //$scope.isDirty = $scope.documentprefixform.$dirty;
            //if ($scope.isDirty) {
            if ($scope.isFormValueDirty) {
                if (Object.keys(tempObj).length) {
                    var newScope = $scope.$new();
                    newScope.errorMessage = $rootScope.nls["ERR200"];
                    ngDialog.openConfirm({
                            template: '<p>{{errorMessage}}</p>' +
                                '<div class="ngdialog-footer">' +
                                ' <button type="button" class="btn btn-default btn-property cancel-btn" ng-click="confirm(2)">No</button>' +
                                '<button type="button" class="btn btn-default btn-property accent-btn " ng-click="confirm(1)">Yes</button>' +
                                '<button type="button" class="btn btn-default btn-property pull-left grey-cancel-btn" ng-click="confirm(3)">Cancel</button>' +
                                '</div>',
                            plain: true,
                            scope: newScope,
                            className: 'ngdialog-theme-default'
                        })
                        .then(
                            function(value) {
                                if (value == 1 &&
                                    $scope.docPrefixMaster.id == null) {
                                    $scope.save();
                                } else if (value == 1 &&
                                    $scope.docPrefixMaster.id != null) {
                                    $scope.update();
                                } else if (value == 2) {
                                    var params = {};
                                    params.submitAction = 'Cancelled';
                                    $state.go("layout.docprefix", params);
                                } else {
                                    console.log("cancelled");
                                }

                            });
                } else {
                    var params = {};
                    params.submitAction = 'Cancelled';
                    $state.go("layout.docprefix", params);
                }
            } else {
                var params = {};
                params.submitAction = 'Cancelled';
                $state.go("layout.docprefix", params);
            }
        }

        $scope.cancelMapping = function() {
            var tempObj = $scope.docPrefixMaster.relatedData.docPrefixTypeMapping;
            if (!discardService.compare(tempObj)) {
                if (Object.keys(tempObj).length) {
                    var newScope = $scope.$new();
                    newScope.errorMessage = $rootScope.nls["ERR200"];
                    ngDialog.openConfirm({
                            template: '<p>{{errorMessage}}</p>' +
                                '<div class="ngdialog-footer">' +
                                ' <button type="button" class="btn btn-default btn-property cancel-btn" ng-click="confirm(2)">No</button>' +
                                '<button type="button" class="btn btn-default btn-property accent-btn " ng-click="confirm(1)">Yes</button>' +
                                '<button type="button" class="btn btn-default btn-property pull-left grey-cancel-btn" ng-click="confirm(3)">Cancel</button>' +
                                '</div>',
                            plain: true,
                            scope: newScope,
                            className: 'ngdialog-theme-default'
                        })
                        .then(
                            function(value) {
                                if (value == 1 &&
                                    $scope.docPrefixMaster.relatedData.docPrefixTypeMapping.id == null) {
                                    $scope.saveMapping();
                                    $scope.showForm = false;
                                } else if (value == 2) {
                                    $scope.docPrefixMaster.relatedData.docPrefixTypeMapping = {};
                                    $scope.showForm = false;
                                } else {
                                    console.log("cancelled");
                                }
                            });
                } else {
                    $scope.showForm = false;
                    $scope.docPrefixMaster.relatedData.docPrefixTypeMapping = {};
                }
            } else {

                $scope.showForm = false;
                $scope.docPrefixMaster.relatedData.docPrefixTypeMapping = {};
            }
        }


        $scope.checkMapping = function() {

            if ($scope.docPrefixMaster.docPrefixTypeMappingList != undefined && $scope.docPrefixMaster.docPrefixTypeMappingList != null) {

                $scope.secondArr = [];
                var fineArr = [];
                angular.forEach($scope.docPrefixMaster.docPrefixTypeMappingList, function(docPrefixTypeMapping, index) {
                    //var curObj = $scope.tableData.data[index];
                    $scope.secondArr[index] = {};
                    if (isEmptyRow(docPrefixTypeMapping)) {
                        validResult = true;
                        $scope.docPrefixMaster.docPrefixTypeMappingList[index].isEmpty = true;
                    } else {
                        validResult = $scope.validateDocPrefixMasterMapping(docPrefixTypeMapping, index, "row");
                    }
                    if (validResult) {
                        fineArr.push(1);
                        $scope.secondArr[index].errRow = false;
                    } else {
                        $scope.secondArr[index].errRow = true;
                        fineArr.push(0);
                    }
                });
                angular.forEach($scope.docPrefixMaster.docPrefixTypeMappingList, function(docPrefixTypeMapping, index) {
                    if (docPrefixTypeMapping.isEmpty) {
                        $scope.docPrefixMaster.docPrefixTypeMappingList.splice(index, 1);
                        $scope.secondArr.splice(index, 1);
                    }
                });
                if (fineArr.indexOf(0) < 0) {
                    return true;
                } else {
                    return false;
                }
            }
        }

        $scope.errorShow = function(errorObj, index) {
            $scope.errList = errorObj.errTextArr;
            // Show when some event occurs (use $promise property to ensure the template has been loaded)
            errorOnRowIndex = index;
            myOtherModal.$promise.then(myOtherModal.show);
        };
        var myOtherModal = $modal({
            scope: $scope,
            templateUrl: 'errorPopUp.html',
            show: false,
            keyboard: true
        });

        $scope.addRow = function() {
            $rootScope.clientMessage = null;
            var fineArr = [];
            $scope.secondArr = [];
            angular.forEach($scope.docPrefixMaster.docPrefixTypeMappingList, function(docPrefixTypeMapping, index) {
                var validResult = $scope.validateDocPrefixMasterMapping(docPrefixTypeMapping, index, "row");
                if (validResult) {
                    fineArr.push(1);
                    $scope.secondArr[index].errRow = false;
                } else {
                    $scope.secondArr[index].errRow = true;
                    fineArr.push(0);
                }
            });
            if (fineArr.indexOf(0) < 0) {
                if ($scope.docPrefixMaster.docPrefixTypeMappingList == undefined || $scope.docPrefixMaster.docPrefixTypeMappingList == null) {
                    $scope.docPrefixMaster.docPrefixTypeMappingList = [];
                }
                $scope.docPrefixMaster.docPrefixTypeMappingList.push({});
                $scope.secondArr.push({
                    "errRow": false
                });
                var nextFocus = 'browse' + ($scope.docPrefixMaster.docPrefixTypeMappingList.length);
                $rootScope.navigateToNextField(nextFocus);
            } else {
                //$rootScope.clientMessage = "DocPrefix List is Invalid";
                var nextFocus = 'browse' + ($scope.docPrefixMaster.docPrefixTypeMappingList.length - 1);
                $rootScope.navigateToNextField(nextFocus);
            }
        }

        function isEmptyRow(obj) {
            //return (Object.getOwnPropertyNames(obj).length === 0);
            var isempty = true; //  empty
            if (!obj) {
                return isempty;
            }
            var k = Object.getOwnPropertyNames(obj);
            for (var i = 0; i < k.length; i++) {
                if (obj[k[i]]) {
                    isempty = false; // not empty
                    break;
                }
            }
            return isempty;
        }

        $scope.deleteMapping = function(index) {
            $scope.docPrefixMaster.docPrefixTypeMappingList
                .splice(index, 1);
        }
        $scope.saveMapping = function() {
            if ($scope.validateDocPrefixMasterMapping(0)) {

                if ($scope.editIndex == null) {

                    if ($scope.docPrefixMaster.docPrefixTypeMappingList == null)
                        $scope.docPrefixMaster.docPrefixTypeMappingList = [];
                    $scope.docPrefixMaster.docPrefixTypeMappingList
                        .push($scope.docPrefixMaster.relatedData.docPrefixTypeMapping);
                    $scope.showForm = false;

                } else {
                    $scope.docPrefixMaster.docPrefixTypeMappingList
                        .splice($scope.editIndex, 1, $scope.docPrefixMaster.relatedData.docPrefixTypeMapping);
                    $scope.showForm = false;
                }

                $scope.docPrefixMaster.relatedData.docPrefixTypeMapping = {};
            } else {
                console.log("Document Validation Invalid");
            }
        }

        $scope.showListFn = function(type) {
            if (type == "location") {
                $scope.listType = "Location"
                $scope.listArr = [{
                    "code": "AW",
                    "name": "Aruba"
                }, {
                    "code": "BH",
                    "name": "Bahrain"
                }, {
                    "code": "CN",
                    "name": "China"
                }, {
                    "code": "DK",
                    "name": "Denmark"
                }]
            } else if (type == "doctype") {
                $scope.listType = "Doc Type";
                $scope.listArr = [{
                    "code": "AW",
                    "name": "Aruba"
                }, {
                    "code": "BH",
                    "name": "Bahrain"
                }, {
                    "code": "CN",
                    "name": "China"
                }, {
                    "code": "DK",
                    "name": "Denmark"
                }]
            }

        }

        $scope.selectLocation = function(nextId) {

            $rootScope.navigateToNextField(nextId);
        }

        $scope.save = function() {

            if ($scope.validateDocPrefixMaster(0) && $scope.checkMapping()) {

                $scope.object = cloneService.clone($scope.docPrefixMaster);

                if ($scope.object.addFDC == true) {

                    $scope.object.addFDC = "TRUE";
                } else {

                    $scope.object.addFDC = "FALSE"

                }

                if ($scope.object.addPOR == true) {

                    $scope.object.addPOR = "TRUE";

                } else {

                    $scope.object.addPOR = "FALSE"

                }


                $scope.generateFormula();

                $scope.associateMapping();


                $scope.object.status = "Active";

                $scope.contained_progressbar.start();
                DocumentPrefixAdd.save($scope.object).$promise

                    .then(
                        function(data) {

                            if (data.responseCode == "ERR0") {

                                $rootScope.successDesc = $rootScope.nls["ERR400"];
                                console
                                    .log("Document Prefix added Successfully")
                                documentPrefixService
                                    .set({});
                                var params = {}
                                params.submitAction = 'Saved';
                                $state.go("layout.docprefix", params);
                            } else {
                                console
                                    .log("Document added Failed " +
                                        data.responseDescription);
                                $scope
                                    .revertAssociateMapping();
                            }
                            $scope.contained_progressbar
                                .complete();
                            angular.element(".panel-body")
                                .animate({
                                    scrollTop: 0
                                }, "slow");
                        },
                        function(error) {
                            console
                                .log("Document added Failed : " +
                                    error);
                            $scope.revertAssociateMapping();

                        });

            } else {

                console.log("Document Validation Invalid");

            }

        }

        $scope.associateMapping = function() {


            if ($scope.object.docPrefixTypeMappingList != null) {
                for (var j = 0; j < $scope.object.docPrefixTypeMappingList.length; j++) {
                    $scope.object.docPrefixTypeMappingList[j].logo = $scope.object.docPrefixTypeMappingList[j].encodedLogo;
                    $scope.object.docPrefixTypeMappingList[j].encodedLogo = null;
                }
            }
        }

        $scope.revertAssociateMapping = function() {

            if ($scope.object.docPrefixTypeMappingList != null) {
                for (var j = 0; j < $scope.object.docPrefixTypeMappingList.length; j++) {
                    $scope.object.docPrefixTypeMappingList[j].encodedLogo = $scope.object.docPrefixTypeMappingList[j].logo;
                    $scope.object.docPrefixTypeMappingList[j].logo = null;
                }
            }
        }



        $scope.generateFormula = function() {

            var result = "";

            if ($scope.object.docPrefix != null && $scope.object.docPrefix != undefined && $scope.object.docPrefix != "") {

                result = $scope.object.docPrefix + "/";

            }
            if ($scope.object.addPOR == "TRUE") {

                result += "POR" + "/";

            }
            if ($scope.object.addFDC == "TRUE") {

                result += "FDC" + "/";
            }
            if ($scope.object.docSuffix != null && $scope.object.docSuffix != undefined && $scope.object.docSuffix != "") {

                result += $scope.object.docSuffix + "/"

            }

            $scope.object.formula = $scope.stripTrailingSlash(result);
        }

        $scope.stripTrailingSlash = function(str) {
            if (str.endsWith('/')) {
                return str.slice(0, -1);
            }
            return str;
        }


        $scope.update = function() {

            if ($scope.validateDocPrefixMaster(0) && $scope.checkMapping()) {

                $scope.object = cloneService.clone($scope.docPrefixMaster);

                if ($scope.object.addFDC == true) {

                    $scope.object.addFDC = "TRUE";

                } else {

                    $scope.object.addFDC = "FALSE"

                }

                if ($scope.object.addPOR == true) {

                    $scope.object.addPOR = "TRUE";

                } else {


                    $scope.object.addPOR = "FALSE"

                }


                $scope.generateFormula();

                $scope.associateMapping();

                DocumentPrefixUpdate.update($scope.object).$promise
                    .then(
                        function(data) {
                            if (data.responseCode == "ERR0") {

                                $rootScope.successDesc = $rootScope.nls["ERR401"];
                                console
                                    .log("Document Prefix updated Successfully")
                                documentPrefixService
                                    .set({});
                                var params = {}
                                params.submitAction = 'Saved';
                                $state.go("layout.docprefix", params);
                            } else {
                                console
                                    .log("Document Prefix updated Failed " +
                                        data.responseDescription);
                                $scope
                                    .revertAssociateMapping();
                            }
                            angular.element(".panel-body")
                                .animate({
                                    scrollTop: 0
                                }, "slow");
                        },
                        function(error) {
                            console
                                .log("Document Prefix updated Failed : " +
                                    error);
                            $scope.revertAssociateMapping();

                        });

            } else {

                console.log("Document Form Validation Failed")
            }

        }




        function ValidateDocument(docPrefixMaster) {

            if ((docPrefixMaster.documentCode == undefined ||
                    docPrefixMaster.documentCode == "" || docPrefixMaster.documentCode == null)) {

                $rootScope.respCode = "ERR1";
                $rootScope.respDesc = "Doc Prefix Code is Mandatory";

                return false;

            }
            if ((docPrefixMaster.documentName == undefined ||
                    docPrefixMaster.documentName == "" || docPrefixMaster.documentName == null)) {

                $rootScope.respCode = "ERR1";
                $rootScope.respDesc = "Doc Prefix Name is Mandatory";
                return false;

            }

            if ((docPrefixMaster.docPrefix == undefined ||
                    docPrefixMaster.docPrefix == "" || docPrefixMaster.docPrefix == null) &&
                (docPrefixMaster.docSuffix == undefined ||
                    docPrefixMaster.docSuffix == "" || docPrefixMaster.docSuffix == null)

                &&
                (docPrefixMaster.addPOR == false || docPrefixMaster.addPOR == null) &&
                (docPrefixMaster.addFDC == null || docPrefixMaster.addFDC == false)) {

                $rootScope.respCode = "ERR1";
                $rootScope.respDesc = "Please Select Any one Below Prefix,Sufix,FDC or POR";

                return false;
            } else {
                return true;
            }

        };



        $scope.listConfig = {
            search: true,
            showCode: true,
            ajax: true,
            columns: [{
                "title": "locationName",
                seperator: false
            }, {
                "title": "locationCode",
                seperator: true
            }]
        }



        $scope.showLocation = function() {
            $scope.docPrefixMaster.relatedData.showMapping = false;
            $scope.panelTitle = "Location";
            /*$scope.ajaxLocationEvent(null);*/

        }
        $scope.ajaxLocationEvent = function(object) {
            $scope.searchDto = {};
            $scope.searchDto.keyword = object == null ? "" : object;
            $scope.searchDto.selectedPageNumber = 0;
            $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
            return CountryWiseLocation.fetch({
                "countryId": $rootScope.userProfile.selectedUserLocation.countryMaster.id
            }, $scope.searchDto).$promise.then(
                function(data) {
                    if (data.responseCode == "ERR0") {
                        $scope.locationList = data.responseObject.searchResult;
                        return $scope.locationList;
                        /*$scope.showLocationSelectList = true;*/
                    }
                },
                function(errResponse) {
                    console.error('Error while fetching Locations');
                });

        }
        //$scope.ajaxLocationEvent(null);
        $scope.selectedLocation = function(focus, index) {
            var focusnext = focus + index;
            $rootScope.navigateToNextField(focusnext);
        };
        $scope.cancelList = function() {
            $scope.showLocationSelectList = false;
            $scope.docPrefixMaster.relatedData.showMapping = true;
        }

        $scope.editMapping = function(index) {
            $scope.isBusy = true;
            $scope.docPrefixMaster.relatedData = {};
            $scope.docPrefixMaster.relatedData.docPrefixTypeMapping = cloneService.clone($scope.docPrefixMaster.docPrefixTypeMappingList[index]);
            $scope.logo = $scope.docPrefixMaster.relatedData.docPrefixTypeMapping.encodedLogo;
            $scope.uploadText = false;
            $scope.showForm = true;
            discardService.set($scope.docPrefixMaster.relatedData.docPrefixTypeMapping);
            $timeout(function() {
                $scope.isBusy = false;
                //$scope.convertToByteArray(file);
            }, 1000);
            $scope.editIndex = index;
        }

        $scope.uploadImage = function(file, docPrefixTypeMapping) {
            if (file != null || file != undefined) {
                $scope.fileName = file.name;
                var extention = $scope.fileName.split('.')[$scope.fileName
                    .split('.').length - 1].toLowerCase();
                console.log("file extention ", extention);
                var valid = false;
                if (extention == 'jpeg' || extention == 'jpg' ||
                    extention == 'png' || extention == 'gif') {
                    valid = true;
                }
                if (valid) {
                    $scope.isBusy = true;
                    var reader = new FileReader();
                    reader.onload = function(event) {
                        var contents = event.target.result;
                        var uploadedFile = btoa(contents);
                        // content =
                        // content.replace("data:image/png;base64,","");

                        console.log("File contents: " + uploadedFile);
                        docPrefixTypeMapping.encodedLogo = uploadedFile;
                        $scope.uploadText = false;
                        $timeout(function() {
                            $scope.isBusy = false;
                            //$scope.convertToByteArray(file);
                        }, 5000);
                    };
                    reader.readAsBinaryString(file);
                } else {
                    $scope.errorMap = new Map();
                    $scope.errorMap.put("logo", $rootScope.nls["ERR2236"]);
                }
            }

        }



        $scope.validateDocPrefixMaster = function(validateCode) {
            console.log("Validate Called");
            $scope.errorMap = new Map();

            if (validateCode == 0 || validateCode == 1) {
                if ($scope.docPrefixMaster.documentName == undefined || $scope.docPrefixMaster.documentName == null || $scope.docPrefixMaster.documentName == "") {
                    $scope.errorMap.put("documentName", $rootScope.nls["ERR2206"]);
                    return false;
                } else {
                    if (!CommonValidationService.checkRegExp(appConstant.Reg_Exp_Master_DOCUMENT_PREFIX_NAME, $scope.docPrefixMaster.documentName)) {
                        $scope.errorMap.put("documentName", $rootScope.nls["ERR2208"]);
                        return false;
                    }
                }
            }

            if (validateCode == 0 || validateCode == 2) {
                if ($scope.docPrefixMaster.documentCode == undefined ||
                    $scope.docPrefixMaster.documentCode == null ||
                    $scope.docPrefixMaster.documentCode == "") {
                    $scope.errorMap.put("documentCode", $rootScope.nls["ERR2205"]);
                    return false;
                } else {
                    if (!CommonValidationService.checkRegExp(appConstant.Reg_Exp_Master_DOCUMENT_PREFIX_CODE, $scope.docPrefixMaster.documentCode)) {
                        $scope.errorMap.put("documentCode", $rootScope.nls["ERR2207"]);
                        return false;
                    }
                }
            }

            if (validateCode == 0 || validateCode == 4) {

                if ($scope.docPrefixMaster.docSuffix == undefined ||
                    $scope.docPrefixMaster.docSuffix == null ||
                    $scope.docPrefixMaster.docSuffix == "")

                    if ($scope.docPrefixMaster.docPrefix == undefined ||
                        $scope.docPrefixMaster.docPrefix == null ||
                        $scope.docPrefixMaster.docPrefix == "") {
                        $scope.errorMap.put("docPrefix", $rootScope.nls["ERR2235"]);
                        var navigateTo = 'docPrefix';
                        $rootScope.navigateToNextField(navigateTo);
                        return false;
                    }
                if ($scope.docPrefixMaster.docPrefix != undefined ||
                    $scope.docPrefixMaster.docPrefix != null ||
                    $scope.docPrefixMaster.docPrefix != "") {
                    /*	var regexp = new RegExp("^[ 0-9a-zA-Z@-'.,&@:;?!()$#*_]{0,20}$");
									if (!regexp
											.test($scope.docPrefixMaster.docPrefix)) {
										$scope.errorMap.put("docPrefix",$rootScope.nls["ERR2221"]);
										return false;
									}
*/
                }
            }

            if (validateCode == 0 || validateCode == 5) {

                if ($scope.docPrefixMaster.docPrefix == "undefined" ||
                    $scope.docPrefixMaster.docPrefix == null ||
                    $scope.docPrefixMaster.docPrefix == "")
                    if ($scope.docPrefixMaster.docSuffix == "undefined" ||
                        $scope.docPrefixMaster.docSuffix == null ||
                        $scope.docPrefixMaster.docSuffix == "") {
                        $scope.errorMap.put("docSuffix",
                            $rootScope.nls["ERR2209"]);
                        var navigateTo = 'docSuffix';
                        $rootScope.navigateToNextField(navigateTo);
                        return false;
                    }
                if ($scope.docPrefixMaster.docSuffix != null) {
                    /*	var regexp = new RegExp("^[ 0-9a-zA-Z@-'.,&@:;?!()$#*_]{0,20}$");
                    	if (!regexp
                    			.test($scope.docPrefixMaster.docSuffix)) {
                    		$scope.errorMap.put("docSuffix",
                    				 $rootScope.nls["ERR2222"]);
                    		return false;
                    	}*/

                }
            }

            return true;

        };
        $scope.validateDocPrefixMasterMapping = function(
            dataObj, index, type) {
            $scope.secondArr[index] = {};
            var errorArr = [];
            $scope.secondArr[index].errTextArr = [];
            if (dataObj.encodedLogo == "undefined" ||
                dataObj.encodedLogo == null || dataObj.encodedLogo == "") {
                $scope.secondArr[index].encodedLogo = true;
                $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR2228"]);
                errorArr.push(0);
            }
            if (dataObj.locationMaster == undefined ||
                dataObj.locationMaster == null ||
                dataObj.locationMaster == "") {
                $scope.secondArr[index].locationMaster = true;
                $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR2232"]);
                errorArr.push(0);
            } else {

                if (dataObj.locationMaster.status == "Block" ||
                    dataObj.locationMaster.status == "Hide") {
                    dataObj.locationMaster = null;
                    $scope.secondArr[index].locationMaster = true;
                    $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR2233"]);
                    errorArr.push(0);
                }
            }
            if (dataObj.docType == undefined ||
                dataObj.docType == null ||
                dataObj.docType == "") {
                $scope.secondArr[index].docType = true;
                $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR2224"]);
                errorArr.push(0);
            }
            if (dataObj.reportName == undefined ||
                dataObj.reportName == null ||
                dataObj.reportName == "") {
                $scope.secondArr[index].docType = true;
                $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR2226"]);
                errorArr.push(0);
            } else {
                var regexp = new RegExp("^[0-9a-zA-Z]{0,100}$");
                if (!regexp
                    .test(dataObj.reportName)) {
                    $scope.secondArr[index].docType = true;
                    $scope.secondArr[index].errTextArr.push($rootScope.nls["ERR2227"]);
                    errorArr.push(0);
                }
            }
            if (errorArr.indexOf(0) < 0) {
                return true
            } else {
                return false;
            }

        };
        $scope.setStatus = function(data) {
            if (data == 'Active') {
                return 'activetype';
            } else if (data == 'Block') {
                return 'blockedtype';
            } else if (data == 'Hide') {
                return 'hiddentype';
            }
        }

        $scope.setDocStatus = function() {
            return 'activetype';
        }
        $scope.locationRender = function(item) {
            return {
                label: item.locationName,
                item: item
            }
        }

        // History Start

        reloadDateConversion = function(docPrefixMaster) {

            if (docPrefixMaster.addFDC == 'true' || docPrefixMaster.addFDC == 'TRUE' || docPrefixMaster.addFDC == true) {
                docPrefixMaster.addFDC = true;
            } else {
                docPrefixMaster.addFDC = false;
            }

            if (docPrefixMaster.addPOR == 'true' || docPrefixMaster.addPOR == 'TRUE' || docPrefixMaster.addPOR == true) {
                docPrefixMaster.addPOR = true;
            } else {
                docPrefixMaster.addPOR = false;
            }

            return docPrefixMaster;
        }

        $scope.getDocPrefixById = function(docprefixId) {
            console.log("getDocPrefixById ", docprefixId);

            DocumentPrefixView.get({
                id: docprefixId
            }, function(data) {
                if (data.responseCode == "ERR0") {
                    console.log("Successful while getting docPrefix.", data)
                    $scope.docPrefixMaster = data.responseObject;
                    $scope.init();
                    if ($scope.docPrefixMaster.addPOR == "FALSE" || $scope.docPrefixMaster.addPOR == "false" || $scope.docPrefixMaster.addPOR == false) {
                        $scope.docPrefixMaster.addPOR = false;
                    } else {
                        $scope.docPrefixMaster.addPOR = true;

                    }
                    if ($scope.docPrefixMaster.addFDC == "TRUE" || $scope.docPrefixMaster.addFDC == "true" || $scope.docPrefixMaster.addFDC == true) {
                        $scope.docPrefixMaster.addFDC = true;
                    } else {
                        $scope.docPrefixMaster.addFDC = false;
                    }
                    $rootScope
                        .setNavigate3($scope.docPrefixMaster.id != null ? "Update Document Prefix" :
                            "Add Document Prefix");
                }

            }, function(error) {
                console.log("Error while getting docPrefix.", error)
            });

        }


        //On leave the Unfilled docPrefix Form Broadcast then event will happen in StateChangeListener(BroadCaster)-app_state_route.js

        $scope.$on('addDocprefixEvent', function(events, args) {

            $rootScope.unfinishedData = $scope.docPrefixMaster;
            $rootScope.category = "Document Prefix Master";
            $rootScope.unfinishedFormTitle = "Document Prefix (New)";
            if ($scope.docPrefixMaster != undefined && $scope.docPrefixMaster != null && $scope.docPrefixMaster.docPrefixName != undefined &&
                $scope.docPrefixMaster.docPrefixName != null && $scope.docPrefixMaster.docPrefixName != "") {
                $rootScope.subTitle = $scope.docPrefixMaster.docPrefixName;
            } else {
                $rootScope.subTitle = "Unknown Document Prefix Master"
            }
        })

        $scope.$on('editDocprefixEvent', function(events, args) {
            $rootScope.unfinishedData = $scope.docPrefixMaster;
            $rootScope.category = "Document Prefix Master";
            $rootScope.unfinishedFormTitle = "Document Prefix Edit (" + $scope.docPrefixMaster.documentCode + ")";
            if ($scope.docPrefixMaster != undefined && $scope.docPrefixMaster != null && $scope.docPrefixMaster.docPrefixName != undefined &&
                $scope.docPrefixMaster.docPrefixName != null && $scope.docPrefixMaster.docPrefixName != "") {
                $rootScope.subTitle = $scope.docPrefixMaster.docPrefixName;
            } else {
                $rootScope.subTitle = "Unknown Document Prefix Master"
            }
        })


        $scope.$on('addDocprefixEventReload', function(e, confirmation) {
            console.log("addDocPrefixEventReload is called ", $scope.docPrefixMaster);
            confirmation.message = "";
            localStorage.reloadFormData = JSON.stringify($scope.docPrefixMaster);
            localStorage.isDocPrefixReloaded = "YES";
            e.preventDefault();
        });


        $scope.$on('editDocprefixEventReload', function(e, confirmation) {
            confirmation.message = "";
            localStorage.reloadFormData = JSON.stringify($scope.docPrefixMaster);
            localStorage.isDocPrefixReloaded = "YES";
            e.preventDefault();
        });


        $scope.isReloaded = localStorage.isDocPrefixReloaded;

        if ($stateParams.action == "ADD") {
            if ($stateParams.fromHistory === 'Yes') {
                if ($scope.isReloaded == "YES") {
                    console.log("Add History Reload...")
                    $scope.isReloaded = "NO";
                    localStorage.isDocPrefixReloaded = "NO";
                    $scope.docPrefixMaster = reloadDateConversion(JSON.parse(localStorage.reloadFormData));
                } else {
                    console.log("Add History NotReload...")
                    $scope.docPrefixMaster = $rootScope.selectedUnfilledFormData;
                }
            } else {
                if ($scope.isReloaded == "YES") {
                    $scope.isReloaded = "NO";
                    localStorage.isDocPrefixReloaded = "NO";
                    $scope.docPrefixMaster = reloadDateConversion(JSON.parse(localStorage.reloadFormData));
                    console.log("Add NotHistory Reload...");
                } else {
                    console.log("Add NotHistory NotReload...")
                    $scope.init();
                }
            }
            $rootScope.breadcrumbArr = [{
                    label: "Master",
                    state: "layout.docprefix"
                },
                {
                    label: "Document Prefix",
                    state: "layout.docprefix"
                },
                {
                    label: "Add Document Prefix",
                    state: null
                }
            ];

        } else {

            if ($stateParams.fromHistory === 'Yes') {

                if ($scope.isReloaded == "YES") {
                    $scope.isReloaded = "NO";
                    localStorage.isDocPrefixReloaded = "NO";
                    $scope.docPrefixMaster = reloadDateConversion(JSON.parse(localStorage.reloadFormData));
                } else {
                    $scope.docPrefixMaster = $rootScope.selectedUnfilledFormData;
                }
            } else {
                if ($scope.isReloaded == "YES") {
                    $scope.isReloaded = "NO";
                    localStorage.isDocPrefixReloaded = "NO";
                    $scope.docPrefixMaster = reloadDateConversion(JSON.parse(localStorage.reloadFormData));
                } else {
                    $scope.getDocPrefixById($stateParams.docprefixId);

                }
            }
            $rootScope.breadcrumbArr = [{
                    label: "Master",
                    state: "layout.docprefix"
                },
                {
                    label: "Document Prefix",
                    state: "layout.docprefix"
                },
                {
                    label: "Edit Document Prefix",
                    state: null
                }
            ];
        }


        // History End
    }]);