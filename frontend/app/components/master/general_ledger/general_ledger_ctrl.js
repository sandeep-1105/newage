app.controller('glCtrl',function($scope,$location,$window, ngDialog) {

					/************************************************************
					 * ux - by Muthu
					 * reason - ipad compatibility fix
					 *
					 * *********************************************************/
					$scope.deskTopView = true;

					/********************* ux fix ends here *****************************/
					$scope.glHeadArr = [ {
						"name" : "#",
						"width" : "w50px",
						"model" : "no",
						"search" : false,
						"prefWidth" : "50"
					},{
						"name" : "Account Code",
						"width" : "w200px",
						"model" : "accountCode",
						"search" : true,
						"wrap_cell" : true,
						"type" : "text",
						"sort" : true,
						"key" : "AccountCode",
						"prefWidth" : "200"

					},{
						"name" : "GL Account Name",
						"width" : "w200px",
						"model" : "accountName",
						"search" : true,
						"type" : "text",
						"sort" : true,
						"key" : "AccountName",
						"prefWidth" : "200"
					},{
						"name" : "GL Head",
						"width" : "w150px",
						"model" : "glHead",
						"search" : true,
						"type" : "drop",
						"sort" : true,
						"key" : "GLHead",
						"prefWidth" : "150"
					},{
						"name" : "Group",
						"width" : "w150px",
						"model" : "group",
						"search" : true,
						"type" : "text",
						"sort" : true,
						"key" : "Group",
						"prefWidth": "150"
					},{
						"name" : "Subgroup",
						"width" : "w200px",
						"model" : "subGroup",
						"search" : true,
						"type" : "text",
						"sort" : true,
						"key" : "Subgroup",
						"prefWidth" :"200"
					},
					{
						"name" : "Status",
						"width" : "w150px",
						"model" : "status",
						"search" : true,
						"type" : "drop",
						"sort" : true,
						"key" : "Status",
						"prefWidth":"150"
					}
					];
	                $scope.glArr = [
						{
							"no" : 1,
							"accountCode" : 1101090000,
							"accountName" : "Fixed Assets - Chennai",
							"glHead" : "Assets",
							"group" : "Fixed Assets",
							"subGroup" : "Chennai",
							"status": "active"
					    },
						{
							"no" : 2,
							"accountCode" : 1103020111,
							"accountName" : "ABN Bank Balance",
							"glHead" : "Assets",
							"group" : "Current Assets",
							"subGroup" : "Bank Balance",
							"status": "active"
						},
						{
							"no" : 3,
							"accountCode" : 1301010111,
							"accountName" : "AE Export Income",
							"glHead" : "Income",
							"group" : "Operating Income",
							"subGroup" : "Export",
							"status": "active"
						}
					]
					$scope.sortSelection = {
						sortKey : "chargeName",
						sortOrder : "asc"
					}

					$scope.page = 0;
					$scope.limit = 10;

					$scope.rowSelect = function(){
						$scope.showDetail = true;

						/************************************************************
						 * ux - by Muthu
						 * reason - ipad compatibility fix
						 *
						 * *********************************************************/

						var windowInner=$window.innerWidth;
						if(windowInner<=1199){
							$scope.deskTopView = false;
						} else{
							$scope.deskTopView = true;
						}

						/********************* ux fix ends here *****************************/
					};
					$scope.cancel = function() {
						$scope.showDetail = false;
						$scope.showLogs = false;
						$scope.showHistory = false;
						$scope.chargeMaster = {};
						/************************************************************
						 * ux - by Muthu
						 * reason - ipad compatibility fix
						 *
						 * *********************************************************/
						$scope.deskTopView = true;

						/********************* ux fix ends here *****************************/
					};
					$scope.add = function() {
						$location.path("/general_ledger/general_ledger_operation");
					};
					$scope.edit = function() {
						$location.path("/general_ledger/general_ledger_operation");
					};


					$scope.changepage = function(param) {
						$scope.page = param.page;
						$scope.limit = param.size;
						$scope.search();
					}

					$scope.currentTab = "address";
					$scope.activeTab = function(tab) {
						$scope.currentTab = tab;
					}
					$scope.view = function(selectedMaster) {
						console.log("View Charge Called");
						$scope.chargeMaster = selectedMaster;
					};
					$scope.limitChange = function(item) {
						$scope.page = 0;
						$scope.limit = item;
						$scope.cancel();
						$scope.search();
					}

					$scope.sortChange = function(param) {
						$scope.searchDto.orderByType = param.sortOrder
								.toUpperCase();
						$scope.searchDto.sortByColumn = param.sortKey;
						$scope.cancel();
						$scope.search();
					};

					$scope.deleteMaster = function() {
						ngDialog
								.openConfirm(
										{
											template : '<p>Are you sure you want to delete selected charge?</p>'
													+ '<div class="ngdialog-footer">'
													+ '<button type="button" class="btn btn-default btn-property cancel-btn" ng-click="closeThisDialog(0)">Cancel'
													+ '<button type="button" class="btn btn-primary btn-property accent-btn" ng-click="confirm(1)">Yes'
													+ '</button></div>',
											plain : true,
											className : 'ngdialog-theme-default'
										})
								.then(
										function(value) {
											ChargeRemove
													.remove(
															{
																id : $scope.chargeMaster.id
															},
															function(data) {
																if (data.responseCode == 'ERR0') {
																	console
																			.log("Charge deleted Successfully")
																	$scope
																			.init();
																} else {
																	console
																			.log("Charge deleted Failed "
																					+ data.responseDescription)
																}
															},
															function(error) {
																console
																		.log("Charge deleted Failed : "
																				+ error)
															});
										}, function(value) {
											console.log("deleted cancelled");

										});
					}
					$scope.changeSearch = function(param) {
						for ( var attrname in param) {
							$scope.searchDto[attrname] = param[attrname];
						}
						$scope.page = 0;
						$scope.cancel();
						$scope.search();

					}

					$scope.mouseOver = function(column) {

						if ($scope.searchDto.sortByColumn != column) {
							if ($scope.carrierSortIconClass == "icon-downarrow") {
								$scope.carrierSortIconClass = "icon-uparrow";
							} else {
								$scope.carrierSortIconClass = "icon-downarrow";
							}
						}
						$scope.searchDto.sortByColumn = column;

					}

					$scope.changeSorting = function(column) {
						$scope.sortIconCarrierName = " ";
						if ($scope.searchDto.sortByColumn != column) {
							$scope.carrierSortIconClass = "icon-downarrow";
							$scope.searchDto.orderByType = "ASC";
							$scope.search();

						} else {
							$scope.searchDto.sortByColumn = column;
							if ($scope.searchDto.orderByType == "ASC") {
								$scope.search();
								$scope.searchDto.orderByType = "DESC";
								$scope.carrierSortIconClass = "icon-uparrow";
							} else {
								$scope.searchDto.orderByType = "DESC";
								$scope.carrierSortIconClass = "icon-uparrow";
								$scope.search();
								$scope.searchDto.orderByType = "ASC";
								$scope.carrierSortIconClass = "icon-downarrow";
							}

						}
						$scope.searchDto.sortByColumn = column;

					}

					$scope.search = function() {
						$scope.chargeMaster = {};

						$scope.searchDto.selectedPageNumber = $scope.page;
						$scope.searchDto.recordPerPage = $scope.limit;
						console.log($scope.searchDto);
						$scope.chargeArr = [];
						$scope.searchDto.selectedPageNumber = $scope.page;
						$scope.searchDto.recordPerPage = $scope.limit;
						ChargeSearch.query($scope.searchDto).$promise
								.then(function(data, status) {
									$scope.totalRecord = data.responseObject.totalRecord;
									var tempArr = [];
									var resultArr = [];
									tempArr = data.responseObject.searchResult;
									var tempObj = {};
									angular.forEach(tempArr, function(item,
											index) {
										tempObj = item;
										tempObj.no = (index + 1)
												+ ($scope.page * $scope.limit);
										resultArr.push(tempObj);
										tempObj = {};
									});
									$scope.chargeArr = resultArr;
								});

					}

					$scope.serialNo = function(index) {
						index = ($scope.searchDto.selectedPageNumber * $scope.searchDto.recordPerPage)
								+ (index + 1);
						return index;
					}

					

});
