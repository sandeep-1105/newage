app.factory('countryReportConfigFactory',['$resource', function($resource) {
	return {
		findById : $resource('/api/v1/cntryrprtcnfg/get/id/:id', {}, {
			 query : {method : 'GET', params : {id : ''}, isArray : false}
		})
		,
		search : $resource('/api/v1/cntryrprtcnfg/get/search', {}, {
			fetch : {method : 'POST'}
		})
		,
		update : $resource('/api/v1/cntryrprtcnfg/update', {}, {
			query : {method : 'POST'}
		})
		,
		delete : $resource('/api/v1/cntryrprtcnfg/delete/:id', {}, {
			query : {method : 'DELETE', params : {id : ''}}
		})
		,
		searchByCharge : $resource("/api/v1/chargemaster/get/search/keyword", {}, {
			query : {
				method : 'POST',
			}
		})
		,
		searchByGenLdr : $resource("/api/v1/generalledgermaster/get/search/keyword", {}, {
			query : {
				method : 'POST',
			}
		})
		,
		searchBySubLdr : $resource("/api/v1/subledgermaster/get/search/keyword", {}, {
			query : {
				method : 'POST',
			}
		})
		,
		create : $resource('/api/v1/cntryrprtcnfg/create', {}, {
			query : {method : 'POST'}
		})
	};
}])