app.controller("countryReportConfigCtrl", ['$scope', '$rootScope', '$window', '$modal', '$state', '$stateParams', '$log', 'ngDialog', 'countryReportConfigDataService', 'countryReportConfigFactory', 'CountryList', 'RecentHistorySaveService', 'countryReportConfigValidationService', 'ValidateUtil', 'Notification', 'roleConstant',
    function($scope, $rootScope, $window, $modal, $state, $stateParams, $log, ngDialog, countryReportConfigDataService, countryReportConfigFactory, CountryList, RecentHistorySaveService, countryReportConfigValidationService, ValidateUtil, Notification, roleConstant) {



        $scope.tableHeadArr = countryReportConfigDataService.getHeadArray();
        $scope.dataArr = [];
        $scope.errorArray = [];
        $scope.searchData = {};
        $scope.countryReportConfig = {};
        $scope.countryReportConfig.pageMasterList = [];
        $scope.countryReportConfig.pageMasterList.push({});
        $scope.limitArr = [10, 15, 20];
        $scope.page = 0;
        $scope.limit = 10;
        $scope.totalRecord = 100;
        $scope.showHistory = false;
        $scope.errorMap = new Map();
        $scope.firstFocus = false;
        $scope.firstFocusReportCode = true;
        $scope.errorFlag = false;
        $scope.editFormFlag = false;
        $scope.deskTopView = true;


        var errorOnRowIndex = null;

        $scope.errorShow = function(errorObj, index) {
            $scope.errList = errorObj.errTxtArr;
            // Show when some event occurs (use $promise property to ensure the template has been loaded)
            errorOnRowIndex = index;
            var myOtherModal = $modal({
                scope: $scope,
                templateUrl: 'errorPopUp.html',
                show: false,
                keyboard: true
            });
            myOtherModal.$promise.then(myOtherModal.show);
        };


        $scope.sortSelection = {
            sortKey: "reportName",
            sortOrder: "asc"
        }

        $scope.changeSearch = function(param) {
            $log.debug("Change Search is called." + param);
            for (var attrname in param) {
                $scope.searchData[attrname] = param[attrname];
            }
            $scope.page = 0;
            $scope.search();
        }

        $scope.sortChange = function(param) {
            $log.debug("sortChange is called. " + param);
            $scope.searchData.orderByType = param.sortOrder.toUpperCase();
            $scope.searchData.sortByColumn = param.sortKey;
            $scope.search();
        }


        /**
         * CountryReportConfig Master List Page Search Data  initializing and calling search Mtd.
         */
        $scope.searchInit = function() {
            $scope.searchData = {};
            $scope.searchData.orderByType = $scope.sortSelection.sortOrder.toUpperCase();
            $scope.searchData.sortByColumn = $scope.sortSelection.sortKey;
            $scope.search();
            $scope.detailTab = "cntrycnfgmaster";
        };


        /**
         * Get CountryReportConfig Master's Based on Search Data.
         */
        $scope.search = function() {
            $scope.countryReportConfigView = false;
            $scope.searchData.selectedPageNumber = $scope.page;
            $scope.searchData.recordPerPage = $scope.limit;
            $scope.dataArr = [];
            countryReportConfigFactory.search.fetch($scope.searchData).$promise.then(
                function(data) {
                    if (data.responseCode == "ERR0") {
                        $scope.totalRecord = data.responseObject.totalRecord;
                        $scope.dataArr = data.responseObject.searchResult;
                    }
                },
                function(errResponse) {
                    $log.error('Error while fetching countryReportConfig');
                });
        }




        /**
         * Validating field of Trigger Master while instant change.
         * @param {errorElement,errorCode}
         */
        $scope.validate = function(valElement, code) {
            $scope.firstFocusReportCode = false;
            var validationResponse = countryReportConfigValidationService.validate($scope.countryReportConfig, code);
            if (validationResponse.error == true) {
                $scope.validationErrorAndFocus(validationResponse, validationResponse.errElement);
            } else {
                $scope.errorMap.put(valElement, null);
                validationResponse.error = false;
            }
            return validationResponse.error;
        }


        $scope.selectedCountry = function(id) {
            if (!$scope.validate('country', 3)) {
                $rootScope.navigateToNextField(id);
            }
        }

        /**
         * CountryReportConfig Master List Page Search Data  initializing and calling search Mtd.
         * @param {Object} data - The Selected serTaxCat Master from List page.
         * @param {string} data.id - The id of the Selected serTaxCat Master.
         * @param {int} index - The Selected row index from List page.
         */
        $scope.rowSelect = function(data) {
            if ($rootScope.roleAccess(roleConstant.SETUP_REPORT_CONFIGURATION_COUNTRYMASTER_REPORT_VIEW)) {
                $scope.countryReportConfigView = true;
                $scope.countryReportConfig = data;
                var windowInner = $window.innerWidth;
                if (windowInner <= 1199) {
                    $scope.deskTopView = false;
                } else {
                    $scope.deskTopView = true;
                }
                angular.element($window).bind('resize', function() {
                    if ($window.innerWidth >= 1200) {
                        $scope.deskTopView = true;
                    } else if ($window.innerWidth <= 1199) {
                        if ($scope.countryReportConfig.id !== false) {
                            $scope.deskTopView = false;
                        } else {
                            $scope.deskTopView = true;
                        }
                    }
                    $scope.$digest();
                });
            }

        }




        /* Master Select Picker */
        $scope.ajaxChargeEvent = function(object) {
            $scope.searchDto = {};
            $scope.searchDto.keyword = object == null ? "" : object;
            $scope.searchDto.selectedPageNumber = 0;
            $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
            return countryReportConfigFactory.searchByCharge.query($scope.searchDto).$promise.then(function(data, status) {
                    if (data.responseCode == "ERR0") {
                        $scope.chargeMasterList = data.responseObject.searchResult;
                        return $scope.chargeMasterList;
                    }
                },
                function(errResponse) {
                    console.error('Error while fetching charge Master');
                }
            );

        }

        /* Master Select Picker */
        $scope.ajaxGenledgerEvent = function(object) {
            $scope.searchDto = {};
            $scope.searchDto.keyword = object == null ? "" : object;
            $scope.searchDto.selectedPageNumber = 0;
            $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
            return countryReportConfigFactory.searchByGenLdr.query($scope.searchDto).$promise.then(function(data, status) {
                    if (data.responseCode == "ERR0") {
                        $scope.chargeMasterList = data.responseObject.searchResult;
                        return $scope.chargeMasterList;
                    }
                },
                function(errResponse) {
                    console.error('Error while fetching genledger  Master');
                }
            );

        }

        /* Master Select Picker */
        $scope.ajaxSubledgerEvent = function(object) {
            $scope.searchDto = {};
            $scope.searchDto.keyword = object == null ? "" : object;
            $scope.searchDto.selectedPageNumber = 0;
            $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
            return countryReportConfigFactory.searchBySubLdr.query($scope.searchDto).$promise.then(function(data, status) {
                    if (data.responseCode == "ERR0") {
                        $scope.chargeMasterList = data.responseObject.searchResult;
                        return $scope.chargeMasterList;
                    }
                },
                function(errResponse) {
                    console.error('Error while fetching Subledger Master');
                }
            );

        }

        /* Master Select Picker */
        $scope.ajaxCountryEvent = function(object) {
            $scope.searchDto = {};
            $scope.searchDto.keyword = object == null ? "" : object;
            $scope.searchDto.selectedPageNumber = 0;
            $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
            return CountryList.fetch($scope.searchDto).$promise
                .then(
                    function(data) {
                        if (data.responseCode == "ERR0") {
                            $scope.chargeMasterList = data.responseObject.searchResult;
                            return $scope.chargeMasterList;
                        }
                    },
                    function(errResponse) {
                        console
                            .error('Error while fetching Country');
                    });

        }


        /**
         * Modify Existing CountryReportConfig Master.
         * @param {int} id - The id of a Country.
         */
        $scope.editMaster = function(id) {
            if ($rootScope.roleAccess(roleConstant.SETUP_REPORT_CONFIGURATION_COUNTRYMASTER_REPORT_MODIFY)) {
                $state.go("layout.countryReportConfigEdit", {
                    id: id
                });
            }
        }

        // set status of CountryReportConfig master

        $scope.setStatus = function(data) {
            if (data == 'Active') {
                return 'activetype';
            } else if (data == 'Block') {
                return 'blockedtype';
            } else if (data == 'Hide') {
                return 'hiddentype';
            }
        }


        /**
         * Go To Add CountryReportConfig Master.
         */

        $scope.addNew = function() { // roles access added for adding new record
            if ($rootScope.roleAccess(roleConstant.SETUP_REPORT_CONFIGURATION_COUNTRYMASTER_REPORT_CREATE)) {
                $state.go("layout.countryReportConfigAdd");
            }
        }


        /**
         * Create New CountryReportConfig Master.
         */

        $scope.create = function() {
            $scope.errorFlag = false;
            var validationResponse = countryReportConfigValidationService.validate($scope.countryReportConfig, 0);
            $log.debug("Validation Response -- ", validationResponse);
            if (validationResponse.error == true) {
                $log.debug("Validation Response -- ", validationResponse);
                $log.debug('Validation Failed to create countryReportConfig..')
                $scope.validationErrorAndFocus(validationResponse, validationResponse.errElement);
            } else {
                for (var index = 0; index < $scope.countryReportConfig.pageMasterList.length; index++) {
                    if ($scope.validatePageMaster(index)) {
                        return;
                    }
                }

                countryReportConfigFactory.create.query($scope.countryReportConfig).$promise.then(function(data) {
                    if (data.responseCode == 'ERR0') {
                        $log.debug("CountryReportConfig Saved Successfully Response :: ", data);
                        Notification.success($rootScope.nls["ERR400"]);
                        $state.go('layout.reportMaster');
                    } else {
                        $log.debug("Create CountryReportConfig Failed :" + data.responseDescription)
                    }

                }, function(error) {
                    $log.debug("Create CountryReportConfig Failed : " + error)
                });

            }
        };


        //Update Existing serTaxCat Master
        $scope.update = function() {
            var errorFlag = false;
            var validationResponse = countryReportConfigValidationService.validate($scope.countryReportConfig, 0);
            if (validationResponse.error == true) {
                $log.debug("Validation Response -- ", validationResponse);
                $log.debug('Validation Failed while Updating the CountryReportConfig..')
                $scope.validationErrorAndFocus(validationResponse, validationResponse.errElement);
            } else {
                for (var index = 0; index < $scope.countryReportConfig.pageMasterList.length; index++) {
                    if ($scope.validatePageMaster(index)) {
                        return;
                    }
                }


                countryReportConfigFactory.update.query($scope.countryReportConfig).$promise.then(function(data) {
                    if (data.responseCode == 'ERR0') {
                        $log.debug("serTaxCat Updated Successfully Response :: ", data);
                        Notification.success($rootScope.nls["ERR401"]);
                        $state.go('layout.reportMaster');
                    } else {
                        $log.debug("Updating CountryReportConfig Failed :" + data.responseDescription)
                    }
                }, function(error) {
                    $log.debug("Updating CountryReportConfig Failed : " + error)
                });
            }

        };


        /**
         * Go Back to CountryReportConfig Master List Page.
         */
        $scope.backToList = function() {
            $scope.countryReportConfigView = false;
            $scope.deskTopView = true;
        }


        /**
         * Cancel Add or Edit CountryReportConfig Master.
         * @param {int} id - The id of a serTaxCat
         */

        $scope.cancel = function(objId) {
            if ($scope.countryReportConfigForm != undefined) {
                $scope.goTocancel($scope.countryReportConfigForm.$dirty, objId);
            } else {
                $scope.goTocancel(false, objId);
            }
        }

        $scope.goTocancel = function(isDirty, objId) {
            if (isDirty) {
                var newScope = $scope.$new();
                newScope.errorMessage = $rootScope.nls["ERR200"];
                ngDialog.openConfirm({
                    template: '<p>{{errorMessage}}</p>' +
                        '<div class="ngdialog-footer">' +
                        ' <button type="button" class="btn btn-default btn-property cancel-btn" ng-click="confirm(2)">No</button>' +
                        '<button type="button" class="btn btn-default btn-property accent-btn " ng-click="confirm(1)">Yes</button>' +
                        '<button type="button" class="btn btn-default btn-property pull-left grey-cancel-btn" ng-click="confirm(3)">Cancel</button>' +
                        '</div>',
                    plain: true,
                    closeByDocument: false,
                    scope: newScope,
                    className: 'ngdialog-theme-default'
                }).then(
                    function(value) {
                        if (value == 1) {
                            if (objId != null && objId != undefined) {
                                $scope.update();
                            } else {
                                $scope.create();
                            }
                        } else if (value == 2) {
                            $state.go("layout.reportMaster", {
                                submitAction: "Cancelled"
                            });
                        } else {
                            $log.debug("cancelled");
                        }
                    });
            } else {
                $state.go("layout.reportMaster", {
                    submitAction: "Cancelled"
                });
            }
        }



        /**
         * Get serTaxCat Master By id.
         * @param {int} id - The id of a serTaxCat.
         * @param {boolean} isView - TO identify purpose of calling is for View or Edit a serTaxCat.
         */

        $scope.view = function(id, isView) {
            countryReportConfigFactory.findById.query({
                id: id
            }).$promise.then(function(data) {
                if (data.responseCode == 'ERR0') {
                    $scope.countryReportConfig = data.responseObject;
                    var editAccess = $rootScope.roleAccess(roleConstant.SETUP_REPORT_CONFIGURATION_COUNTRYMASTER_REPORT_COUNTRY_PAGES_MODIFY);
                    for (var i = 0; i < $scope.countryReportConfig.pageMasterList.length; i++) {
                        if (editAccess) {
                            $scope.countryReportConfig.pageMasterList[i].check = false;
                        } else
                            $scope.countryReportConfig.pageMasterList[i].check = true;
                    }
                    var rHistoryObj = {}

                    if ($scope.countryReportConfig != undefined && $scope.countryReportConfig != null && $scope.countryReportConfig.id != undefined && $scope.countryReportConfig.id != null) {
                        $rootScope.subTitle = $scope.countryReportConfig.reportName;
                    } else {
                        $rootScope.subTitle = "Unknown countryReportConfig"
                    }
                    $rootScope.unfinishedFormTitle = "countryReportConfig  # " + $scope.countryReportConfig.copies;
                    if (isView) {
                        rHistoryObj = {
                            'title': "serTaxCat #" + $scope.countryReportConfig.serTaxCatCode,
                            'subTitle': $rootScope.subTitle,
                            'stateName': $state.current.name,
                            'stateParam': JSON.stringify($stateParams),
                            'stateCategory': 'serTaxCat Master',
                            'serviceType': undefined,
                            'showService': false
                        }

                    } else {

                        rHistoryObj = {
                            'title': 'Edit serTaxCat # ' + $stateParams.serviceId,
                            'subTitle': $rootScope.subTitle,
                            'stateName': $state.current.name,
                            'stateParam': JSON.stringify($stateParams),
                            'stateCategory': 'countryReportConfig',
                            'serviceType': 'MASTER',
                            'serviceCode': ' ',
                            'orginAndDestination': ' '
                        }
                    }
                    RecentHistorySaveService.form(rHistoryObj);
                } else {
                    $log.debug("Exception while getting serTaxCat by Id ", data.responseDescription);
                }
            }, function(error) {
                $log.debug("Error ", error);
            });
        };

        /**
         * Delete serTaxCat Master.
         * @param {int} id - The id of a serTaxCat.
         */
        $scope.deleteMaster = function(id) {
            if ($rootScope.roleAccess(roleConstant.SETUP_REPORT_CONFIGURATION_COUNTRYMASTER_REPORT_DELETE)) { //role access for delete record
                var newScope = $scope.$new();
                newScope.errorMessage = $rootScope.nls["ERR06604"];
                ngDialog.openConfirm({
                    template: '<p>{{errorMessage}}</p>' +
                        '<div class="ngdialog-footer">' +
                        '<button type="button" class="btn btn-default btn-property cancel-btn" ng-click="closeThisDialog(0)">Cancel' +
                        '<button type="button" class="btn btn-primary btn-property accent-btn" ng-click="confirm(1)">Yes' +
                        '</button></div>',
                    plain: true,
                    scope: newScope,
                    closeByDocument: false,
                    className: 'ngdialog-theme-default'
                }).then(function(value) {
                    countryReportConfigFactory.delete.query({
                        id: id
                    }).$promise.then(function(data) {
                        if (data.responseCode == 'ERR0') {
                            $scope.countryReportConfigView = false;
                            Notification.success($rootScope.nls["ERR402"]);
                            $scope.deskTopView = true;
                            $scope.search();
                        }
                    }, function(error) {
                        $log.debug("Error ", error);
                    });
                }, function(value) {
                    $log.debug("delete Cancelled");
                });
            }
        }


        $scope.addPageMaster = function() {
            if ($rootScope.roleAccess(roleConstant.SETUP_REPORT_CONFIGURATION_COUNTRYMASTER_REPORT_COUNTRY_PAGES_CREATE)) {
                console.log("adding Row is clicked");
                $scope.firstFocus = true;
                $scope.errorArray = [];
                $scope.errorFlag = false;
                if ($scope.countryReportConfig.pageMasterList != undefined && $scope.countryReportConfig.pageMasterList != null && $scope.countryReportConfig.pageMasterList.length > 0) {
                    for (var index = 0; index < $scope.countryReportConfig.pageMasterList.length; index++) {
                        if ($scope.validatePageMaster(index)) {
                            $scope.errorFlag = true;
                        }
                    }
                    if ($scope.errorFlag) {
                        return;
                    } else {
                        $scope.countryReportConfig.pageMasterList.push({});
                    }
                } else {
                    $scope.countryReportConfig.pageMasterList.push({});
                }
            }
            $rootScope.navigateToNextField('pageCode' + $scope.countryReportConfig.pageMasterList.length - 1);
        }


        $scope.validatePageMaster = function(index) {
            $scope.errorArray[index] = {};
            $scope.errorArray[index].errTxtArr = [];
            var errorFound = false;

            if ($scope.countryReportConfig.pageMasterList[index].pageCode == undefined ||
                $scope.countryReportConfig.pageMasterList[index].pageCode == null ||
                $scope.countryReportConfig.pageMasterList[index].pageCode.length == 0) {
                $scope.errorFound = true;
                $scope.errorArray[index].errRow = true;
                $scope.errorArray[index].pageCode = true;
                $scope.errorArray[index].errTxtArr.push($rootScope.nls["ERR06285"]);
                errorFound = true;
            } else {
                $scope.errorArray[index].pageCode = false;
            }

            if ($scope.countryReportConfig.pageMasterList[index].pageName == undefined ||
                $scope.countryReportConfig.pageMasterList[index].pageName == null ||
                $scope.countryReportConfig.pageMasterList[index].pageName.length == 0) {
                $scope.errorFound = true;
                $scope.errorArray[index].errRow = true;
                $scope.errorArray[index].pageName = true;
                $scope.errorArray[index].errTxtArr.push($rootScope.nls["ERR06286"]);
                errorFound = true;
            } else {
                $scope.errorArray[index].pageName = false;
            }

            return errorFound;

        }


        $scope.removePageMaster = function(index) {
            if ($rootScope.roleAccess(roleConstant.SETUP_REPORT_CONFIGURATION_COUNTRYMASTER_REPORT_COUNTRY_PAGES_DELETE)) {
                console.log("remove removeGeneralOriginCharge  is called ", index);
                $scope.countryReportConfig.pageMasterList.splice(index, 1);
            }
        }



        /**
         * Common Validation Focus Functionality - starts here" 
         */
        $scope.validationErrorAndFocus = function(valResponse, elemId) {
            $scope.errorMap = new Map();
            if (valResponse.error == true) {
                $scope.errorMap.put(valResponse.errElement, valResponse.errMessage);
            }
            $scope.navigateToNextField(elemId);
        }


        /**
         * State Identification Handling Here.
         * @param {String} $stateParams.action - VIEW, ADD, EDIT, SEARCH.
         */
        //
        switch ($stateParams.action) {
            case "ADD":
                if ($stateParams.fromHistory === 'Yes') {
                    if ($scope.isReloaded == "YES") {
                        $scope.isReloaded = "NO";
                        localStorage.iscountryReportConfigReloaded = "NO";
                        $scope.countryReportConfig = JSON.parse(localStorage.reloadFormData);
                    } else {
                        $scope.countryReportConfig = $rootScope.selectedUnfilledFormData;
                    }
                } else {
                    if ($scope.isReloaded == "YES") {
                        $scope.isReloaded = "NO";
                        localStorage.iscountryReportConfigReloaded = "NO";
                        $scope.countryReportConfig = JSON.parse(localStorage.reloadFormData);
                    } else {
                        $log.debug("Add NotHistory NotReload...")
                    }
                }
                $scope.editFormFlag = true;
                $rootScope.breadcrumbArr = countryReportConfigDataService.getAddBreadCrumb();
                break;
            case "EDIT":
                if ($scope.fromHistory === 'Yes') {
                    if ($scope.isReloaded == "YES") {
                        $scope.isReloaded = "NO";
                        localStorage.iscountryReportConfigReloaded = "NO";
                        $scope.countryReportConfig = JSON.parse(localStorage.reloadFormData);
                    } else {
                        $scope.countryReportConfig = $rootScope.selectedUnfilledFormData;
                    }
                } else {
                    if ($scope.isReloaded == "YES") {
                        $scope.isReloaded = "NO";
                        localStorage.iscountryReportConfigReloaded = "NO";
                        $scope.countryReportConfig = JSON.parse(localStorage.reloadFormData);
                    } else {
                        $scope.view($stateParams.id, false);
                    }
                }
                $scope.editFormFlag = false;
                $rootScope.breadcrumbArr = countryReportConfigDataService.getEditBreadCrumb();
                break;
            default:
                $rootScope.breadcrumbArr = countryReportConfigDataService.getListBreadCrumb();
                $scope.searchInit();
        }


    }
]);