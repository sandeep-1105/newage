app.service('countryReportConfigDataService',['$rootScope', function($rootScope) {
    this.getHeadArray = function() {
        return [{
            "name": "#",
            "width": "col-xs-0half",
            "model": "no",
            "search": false
        }, {
            "name": "Report Name",
            "width": "col-md-1half",
            "model": "reportName",
            "search": true,
            "wrap_cell": true,
            "type": "text",
            "sort": true,
            "key": "searchReportName"
        }, {
            "name": "Country Name",
            "width": "col-md-1half",
            "model": "countryMaster.countryName",
            "search": true,
            "type": "text",
            "sort": true,
            "key": "searchCountryName"
        }, {
            "name": "No Of Copies",
            "width": "col-md-1half",
            "model": "copies",
            "search": true,
            "wrap_cell": true,
            "type": "text",
            "sort": true,
            "key": "searchCopies"
        }];
    };


    this.getListBreadCrumb = function() {
        return [{
                label: "Setup",
                state: "layout.countryReportConfig"
            },
            {
                label: "Report Configuration",
                state: "layout.countryReportConfig"
            }
        ];
    }

    this.getAddBreadCrumb = function() {
        return [{
                label: "Setup",
                state: "layout.reportMaster"
            },
            {
                label: "Report Configuration",
                state: "layout.reportMaster"
            },
            {
                label: "Add Country Report Master",
                state: null
            }
        ];
    };
    this.getEditBreadCrumb = function() {
        return [{
                label: "Setup",
                state: "layout.reportMaster"
            },
            {
                label: "Report Configuration",
                state: "layout.reportMaster"
            },
            {
                label: "Edit Country Report Master",
                state: null
            }
        ];
    };


}]);