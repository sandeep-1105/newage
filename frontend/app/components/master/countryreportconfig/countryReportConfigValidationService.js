app.service('countryReportConfigValidationService', ['$rootScope', 'ValidateUtil', 'CommonValidationService', 'appConstant', 
    function($rootScope, ValidateUtil, CommonValidationService, appConstant) {
    this.validate = function(countryReportConfig, code) {
        //Validate countryReportConfig name

        //Validate countryReportConfig name

        if (code == 0 || code == 1) {
            if (countryReportConfig.reportName == undefined || countryReportConfig.reportName == null || countryReportConfig.reportName == "") {
                return this.validationResponse(true, "reportName", $rootScope.nls["ERR06282"], countryReportConfig);
            }
        }

        /*if(code == 0 || code == 2) {
			if(countryReportConfig.copies == undefined || countryReportConfig.copies == null || countryReportConfig.copies =="" ){
				return this.validationResponse(true, "copies", $rootScope.nls["ERR06372"], countryReportConfig);
			}
			else{
				if(!CommonValidationService.checkRegExp(appConstant.Reg_Exp_Master_ServiceTaxCategory_Code,countryReportConfig.copies)){
	  				return this.validationResponse(true, "copies", $rootScope.nls["ERR06370"], countryReportConfig);
			   }
			}
		}*/

        if (code == 0 || code == 3) {
            if (countryReportConfig.countryMaster == undefined || countryReportConfig.countryMaster == null || countryReportConfig.countryMaster == "") {
                return this.validationResponse(true, "country", $rootScope.nls["ERR06283"], countryReportConfig);
            } else {
                if (ValidateUtil.isStatusBlocked(countryReportConfig.countryMaster.status)) {
                    countryReportConfig.countryMaster = null;
                    return this.validationResponse(true, "country", $rootScope.nls["ERR06405"], countryReportConfig);
                }
                if (ValidateUtil.isStatusHidden(countryReportConfig.countryMaster.status)) {
                    countryReportConfig.countryMaster = null;
                    return this.validationResponse(true, "country", $rootScope.nls["ERR06406"], countryReportConfig);
                }
            }
        }

        return this.validationSuccesResponse(countryReportConfig);
    }

    this.validationSuccesResponse = function(countryReportConfig) {
        return {
            error: false,
            obj: countryReportConfig
        };
    }

    this.validationResponse = function(err, elem, message, countryReportConfig) {
        return {
            error: err,
            errElement: elem,
            errMessage: message,
            obj: countryReportConfig
        };
    }

}]);