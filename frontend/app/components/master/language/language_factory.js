/**
 * 
 */

 (function() {
 	app.factory("LanguageSearch",['$resource', function($resource) {
 		return $resource("/api/v1/language/get/search/keyword", {}, {
 			query : {
 				method : 'POST'
 			}
 		});
 	}]);

 })();


