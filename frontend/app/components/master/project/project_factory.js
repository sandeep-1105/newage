/**
 * 
 */

 (function() {


 	app.factory("ProjectList",['$resource', function($resource) {

 		return $resource("/api/v1/projectmaster/get/search/keyword", {}, {
 			query : {
 				method : 'POST',
 			}
 		});
 	}]);


 })();
