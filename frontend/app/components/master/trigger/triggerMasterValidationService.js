app.service('triggerMasterValidationService', ['$rootScope','ValidateUtil', 'CommonValidationService', 'appConstant', function($rootScope, ValidateUtil, CommonValidationService, appConstant) {
	this.validate = function(trigger, code) {
		//Validate trigger name
		if(code == 0 || code == 1) {
			if(trigger.triggerName == undefined || trigger.triggerName == null || trigger.triggerName =="" ){
				return this.validationResponse(true, "triggerName", $rootScope.nls["ERR06600"], trigger);
			}
		}
		//Validate trigger code
		if(code == 0 || code == 2) {
			if(trigger.triggerCode == undefined || trigger.triggerCode == null || trigger.triggerCode =="" ){
				return this.validationResponse(true, "triggerCode", $rootScope.nls["ERR06601"], trigger);
			}
		}
		//Validate trigger type 
		if(code == 0 || code == 3) {
			if(trigger.triggerTypeMaster== undefined || trigger.triggerTypeMaster == null || trigger.triggerTypeMaster =="" ){
				return this.validationResponse(true, "triggerTypeName", $rootScope.nls["ERR06609"], trigger);
			}else{
				if(trigger.triggerTypeMaster.status=='Block'){
					return this.validationResponse(true, "triggerTypeName", $rootScope.nls["ERR06612"], trigger);
				}
				if(trigger.triggerTypeMaster.status=='Hide'){
					return this.validationResponse(true, "triggerTypeName", $rootScope.nls["ERR06613"], trigger);
				}
				
			}	
			
		}
		//Validate airDays
		if(code == 0 || code == 4) {
				
				var ad = trigger.airDays;
	        	if(ad != undefined && ad != null && ad !=""){
	        		if(isNaN(ad)) {
	      				return this.validationResponse(true, "airDays", $rootScope.nls["ERR06614"], trigger);
	        		}
	        		if(parseInt(ad) <= 0 ){
	      				return this.validationResponse(true, "airDays", $rootScope.nls["ERR06614"], trigger);
	        		}
	        	}
	        	else{
	        		trigger.airDays = null;
	        	}
				
		}
		//Validate seaDays
		if(code == 0 || code == 5) {
			var ad = trigger.seaDays;
        	if(ad != undefined && ad != null && ad !=""){
        		if(isNaN(ad)) {
      				return this.validationResponse(true, "seaDays", $rootScope.nls["ERR06615"], trigger);
        		}
        		if(parseInt(ad) <= 0 ){
      				return this.validationResponse(true, "seaDays", $rootScope.nls["ERR06615"], trigger);
        		}
        	}
        	else{
        		trigger.seaDays = null;
        	}
		}
		//Validate landDays
		if(code == 0 || code == 6) {
			var ad = trigger.landDays;
        	if(ad != undefined && ad != null && ad !=""){
        		if(isNaN(ad)) {
      				return this.validationResponse(true, "landDays", $rootScope.nls["ERR06616"], trigger);
        		}
        		if(parseInt(ad) <= 0 ){
      				return this.validationResponse(true, "landDays", $rootScope.nls["ERR06616"], trigger);
        		}
        	}
        	else{
        		trigger.landDays = null;
        	}
		}
		
		
		return this.validationSuccesResponse(trigger);
	}
 
	this.validationSuccesResponse = function(trigger) {
	    return {error : false, obj : trigger};
	}
	
	this.validationResponse = function(err, elem, message, trigger) {
		return {error : err, errElement : elem, errMessage : message, obj : trigger};
	}
	
}]);