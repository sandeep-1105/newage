app.service('triggerMasterDataService', function($rootScope, $log, TriggerTypeSearchKeyword) {
    
	var triggerTypeMasterListName=[];
	this.getHeadArray = function() {
	   return [{
					"name":"#",
					"width":"col-xs-0half",
					"model":"no",
					"search":false
			  },{
					"name":"Name",
					"width":"w150px",
					"model":"triggerName",
					"search":true,
					"wrap_cell":true,
					"type":"text",
					"sort":true,
					"key":"searchTriggerName"
				},{
					"name":"Code",
					"width":"w100px",
					"model":"triggerCode",
					"search":true,
					"type":"text",
					"sort":true,
					"key":"searchTriggerCode"
				},{
					"name":"Trigger Type",
					"width":"w100px",
					"model":"triggerTypeMaster.triggerTypeCode",
					"search":true,
					"type":"drop",
					"sort":true,
					"data":triggerTypeMasterListName,
					"key":"searchTriggerType"
				},{
					"name":"Use for Finance",
					"width":"w100px",
					"model":"useForFinance",
					"search":true,
					"type":"drop",
					"data":$rootScope.enum['YesNo'],
					"key":"searchUseForFinance",
					"sort":true
				},{
					"name":"Status",
					"width":"w125px",
					"model":"status",
					"search":true,
					"type":"drop",
					"data":$rootScope.enum['LovStatus'],
					"key":"searchStatus",
					"sort":true
				}];
          };
    
    this.triggerTypeArr = function (){
    	this.searchDto={};
    	 return  TriggerTypeSearchKeyword.fetch(this.searchDto).$promise.then(
                 function(data) {
                     if (data.responseCode =="ERR0"){
                    	 this.triggerTypeMasterList = data.responseObject.searchResult;
                     	for(var i=0;i<this.triggerTypeMasterList.length;i++){
                     		triggerTypeMasterListName.push(this.triggerTypeMasterList[i].triggerTypeCode);
                     	}
                     }
                 },
                 function(errResponse){
                    $log.error('Error while fetching Customer Services');
                 }
             );
        };
          
    this.getListBreadCrumb = function() {
        return [{
                label: "Master",
                state: "layout.triggerMaster"
            },
            {
                label: "Trigger",
                state: "layout.triggerMaster"
            }
        ];
    }

    this.getAddBreadCrumb = function() {
        return [{
                label: "Master",
                state: "layout.triggerMaster"
            },
            {
                label: "Trigger ",
                state: "layout.triggerMaster"
            },
            {
                label: "Add Trigger ",
                state: null
            }
        ];
    };
    this.getEditBreadCrumb = function() {
        return [{
                label: "Master",
                state: "layout.triggerMaster"
            },
            {
                label: "Trigger ",
                state: "layout.triggerMaster"
            },
            {
                label: "Edit Trigger ",
                state: null
            }
        ];
    };
   

});