(function() {

	app.factory("TriggerSearch",['$resource', function($resource) {
		return $resource("/api/v1/triggermaster/get/search", {}, {
			query : {
				method : 'POST'
			}
		});
	}]);
	
	app.factory("TriggerSearchKeyword",['$resource', function($resource) {
		
		return $resource("/api/v1/triggermaster/get/search/keyword/:triggerType", {}, {
			fetch : {
				method : 'POST'
			}
		});
	}]);


})();
