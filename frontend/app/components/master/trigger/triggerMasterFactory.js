app.factory('triggerMasterFactory', function($resource) {
	return {
		findById : $resource('/api/v1/triggermaster/get/id/:id', {}, {
			 query : {method : 'GET', params : {id : ''}, isArray : false}
		}),
		create : $resource('/api/v1/triggermaster/create', {}, {
			query : {method : 'POST'}
		}),
		update : $resource('/api/v1/triggermaster/update', {}, {
			query : {method : 'POST'}
		}),
		delete : $resource('/api/v1/triggermaster/delete/:id', {}, {
			query : {method : 'DELETE', params : {id : ''}}
		}),
		search : $resource('/api/v1/triggermaster/get/search', {}, {
			fetch : {method : 'POST'}
		})
	};
})