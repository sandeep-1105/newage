(function() {
    app.controller("TriggerMasterCtrl", ['$scope', '$rootScope', '$window', '$modal', '$state', '$stateParams', '$log', '$timeout', 'ngDialog', 'triggerMasterDataService', 'triggerMasterFactory', 'RecentHistorySaveService', 'triggerMasterValidationService', 'Notification', 'TriggerTypeSearchKeyword', 'roleConstant',
        function($scope, $rootScope, $window, $modal, $state, $stateParams, $log, $timeout, ngDialog, triggerMasterDataService, triggerMasterFactory, RecentHistorySaveService, triggerMasterValidationService, Notification, TriggerTypeSearchKeyword, roleConstant) {

            triggerMasterDataService.triggerTypeArr();
            $scope.useForFinanceArr = $rootScope.enum['YesNo'];
            $scope.dataArr = [];
            $scope.triggerMaster = {};
            $scope.showHistory = false;
            $scope.triggerMasteView = false;
            $scope.errorMap = new Map();
            $scope.deskTopView = true;
            $timeout(function() {
                $scope.tableHeadArr = triggerMasterDataService.getHeadArray();
            }, 500);

            /**
             * TriggerType Master List Page Search Data  initializing and calling search Mtd.
             */
            $scope.searchInit = function() {
                $scope.searchData = {};
                $scope.limitArr = [10, 15, 20];
                $scope.page = 0;
                $scope.limit = 10;
                $scope.totalRecord = 10;
                $scope.searchData.orderByType = $scope.sortSelection.sortOrder.toUpperCase();
                $scope.searchData.sortByColumn = $scope.sortSelection.sortKey;
                $scope.search();
            };

            $scope.sortSelection = {
                sortKey: "triggerName",
                sortOrder: "asc"
            }

            $scope.changeSearch = function(param) {
                $log.debug("Change Search is called." + param);
                for (var attrname in param) {
                    $scope.searchData[attrname] = param[attrname];
                }
                $scope.page = 0;
                $scope.search();
            }

            $scope.sortChange = function(param) {
                $log.debug("sortChange is called. " + param);
                $scope.searchData.orderByType = param.sortOrder.toUpperCase();
                $scope.searchData.sortByColumn = param.sortKey;
                $scope.search();
            }

            $scope.setStatus = function(data) {
                if (data == 'Active') {
                    return 'activetype';
                } else if (data == 'Block') {
                    return 'blockedtype';
                } else if (data == 'Hide') {
                    return 'hiddentype';
                }
            }

            /**
             * Trigger Master List Navigate to selected page.
             * @param {Object} param - The Selected page option.
             */
            $scope.changepage = function(param) {
                $log.debug("change Page..." + param.page);
                $scope.page = param.page;
                $scope.limit = param.size;
                $scope.search();
            }

            /**
             * Trigger Master List Page Limit Data change.
             * @param {int} nLimit - The Selected limit option from Limit array.
             */
            $scope.limitChange = function(nLimit) {
                $log.debug("Limit Change..." + nLimit);
                $scope.page = 0;
                $scope.limit = nLimit;
                $scope.search();
            }

            /**
             * Trigger Master List Page Search Data  initializing and calling search Mtd.
             * @param {Object} data - The Selected Trigger Master from List page.
             * @param {string} data.id - The id of the Selected Trigger Master.
             * @param {int} index - The Selected row index from List page.
             */
            $scope.rowSelect = function(data) {
                if ($rootScope.roleAccess(roleConstant.MASTER_CRM_TRIGGER_VIEW)) {
                    $scope.triggerMasteView = true;
                    $scope.triggerMaster = data;
                    var windowInner = $window.innerWidth;
                    if (windowInner <= 1199) {
                        $scope.deskTopView = false;
                    } else {
                        $scope.deskTopView = true;
                    }
                    angular.element($window).bind('resize', function() {
                        if ($window.innerWidth >= 1200) {
                            $scope.deskTopView = true;
                        } else if ($window.innerWidth <= 1199) {
                            if ($scope.triggerMaster.id != null) {
                                $scope.deskTopView = false;
                            } else {
                                $scope.deskTopView = true;
                            }
                        }
                        $scope.$digest();
                    });
                }
            }

            /**
             * Go Back to Trigger Master List Page.
             */
            $scope.backToList = function() {
                $scope.triggerMasteView = false;
                $scope.triggerMaster = {};
                $scope.deskTopView = true;
            }


            /**
             * Get Trigger Master's Based on Search Data.
             */
            $scope.search = function() {
                $scope.triggerMasteView = false;
                $scope.searchData.selectedPageNumber = $scope.page;
                $scope.searchData.recordPerPage = $scope.limit;
                $scope.dataArr = [];
                triggerMasterFactory.search.fetch($scope.searchData).$promise.then(
                    function(data) {
                        if (data.responseCode == "ERR0") {
                            $scope.totalRecord = data.responseObject.totalRecord;
                            $scope.dataArr = data.responseObject.searchResult;
                        }
                    },
                    function(errResponse) {
                        $log.error('Error while fetching Trigger');
                    });
            }

            /**
             * Get Trigger Master By id.
             * @param {int} id - The id of a Trigger.
             * @param {boolean} isView - TO identify purpose of calling is for View or Edit a Trigger.
             */
            $scope.view = function(id, isView) {
                triggerMasterFactory.findById.query({
                    id: id
                }).$promise.then(function(data) {
                    if (data.responseCode == 'ERR0') {
                        $scope.triggerMaster = data.responseObject;
                        var rHistoryObj = {}

                        if ($scope.triggerMaster != undefined && $scope.triggerMaster != null && $scope.triggerMaster.id != undefined && $scope.triggerMaster.id != null) {
                            $rootScope.subTitle = $scope.triggerMaster.triggerName;
                        } else {
                            $rootScope.subTitle = "Unknown Trigger"
                        }
                        $rootScope.unfinishedFormTitle = "Trigger  # " + $scope.triggerMaster.triggerCode;
                        if (isView) {
                            rHistoryObj = {
                                'title': "Trigger #" + $scope.triggerMaster.triggerCode,
                                'subTitle': $rootScope.subTitle,
                                'stateName': $state.current.name,
                                'stateParam': JSON.stringify($stateParams),
                                'stateCategory': 'Trigger Master',
                                'serviceType': undefined,
                                'showService': false
                            }

                        } else {

                            rHistoryObj = {
                                'title': 'Edit Trigger # ' + $stateParams.serviceId,
                                'subTitle': $rootScope.subTitle,
                                'stateName': $state.current.name,
                                'stateParam': JSON.stringify($stateParams),
                                'stateCategory': 'Trigger Master',
                                'serviceType': 'MASTER',
                                'serviceCode': ' ',
                                'orginAndDestination': ' '
                            }
                        }
                        RecentHistorySaveService.form(rHistoryObj);
                    } else {
                        $log.debug("Exception while getting Trigger by Id ", data.responseDescription);
                    }
                }, function(error) {
                    $log.debug("Error ", error);
                });
            };

            $scope.ajaxTriggerTypeMasterEvent = function(object) {
                $scope.searchDto = {};
                $scope.searchDto.keyword = object == null ? "" : object;
                $scope.searchDto.selectedPageNumber = 0;
                $scope.searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;
                return TriggerTypeSearchKeyword.fetch($scope.searchDto).$promise.then(
                    function(data) {
                        if (data.responseCode == "ERR0") {
                            $scope.triggerTypeMasterList = data.responseObject.searchResult;
                            return $scope.triggerTypeMasterList;
                        }
                    },
                    function(errResponse) {
                        $log.error('Error while fetching Customer Services');
                    }
                );
            };

            $scope.triggerTypeRender = function(item) {
                return {
                    label: item.triggerTypeName,
                    item: item
                }
            }

            /**
             * Go To Add Trigger Master.
             */
            $scope.addNew = function() {
                if ($rootScope.roleAccess(roleConstant.MASTER_CRM_TRIGGER_CREATE)) {
                    $state.go("layout.triggerMasterAdd");
                }
            }

            /**
             * Validating field of Trigger Master while instant change.
             * @param {errorElement,errorCode}
             */
            $scope.validate = function(valElement, code) {
                var validationResponse = triggerMasterValidationService.validate($scope.triggerMaster, code);
                if (validationResponse.error == true) {
                    $scope.validationErrorAndFocus(validationResponse, validationResponse.errElement);
                } else {
                    $scope.errorMap.put(valElement, null);
                    validationResponse.error = false;
                }
            }

            /**
             * Cancel Add or Edit Trigger Master.
             * @param {int} id - The id of a Trigger
             */

            $scope.cancel = function(objId) {
                if ($scope.triggerMasterForm != undefined) {
                    $scope.goTocancel($scope.triggerMasterForm.$dirty, objId);
                } else {
                    $scope.goTocancel(false, objId);
                }
            }

            $scope.goTocancel = function(isDirty, objId) {
                if (isDirty) {
                    var newScope = $scope.$new();
                    newScope.errorMessage = $rootScope.nls["ERR200"];
                    ngDialog.openConfirm({
                        template: '<p>{{errorMessage}}</p>' +
                            '<div class="ngdialog-footer">' +
                            ' <button type="button" class="btn btn-default btn-property cancel-btn" ng-click="confirm(2)">No</button>' +
                            '<button type="button" class="btn btn-default btn-property accent-btn " ng-click="confirm(1)">Yes</button>' +
                            '<button type="button" class="btn btn-default btn-property pull-left grey-cancel-btn" ng-click="confirm(3)">Cancel</button>' +
                            '</div>',
                        plain: true,
                        closeByDocument: false,
                        scope: newScope,
                        className: 'ngdialog-theme-default'
                    }).then(
                        function(value) {
                            if (value == 1) {
                                if (objId != null && objId != undefined) {
                                    $scope.update();
                                } else {
                                    $scope.create();
                                }
                            } else if (value == 2) {
                                $state.go("layout.triggerMaster", {
                                    submitAction: "Cancelled"
                                });
                            } else {
                                $log.debug("cancelled");
                            }
                        });
                } else {
                    $state.go("layout.triggerMaster", {
                        submitAction: "Cancelled"
                    });
                }
            }

            /**
             * Modify Existing Trigger Master.
             * @param {int} id - The id of a Country.
             */
            $scope.editTrigger = function(id) {
                if ($rootScope.roleAccess(roleConstant.MASTER_CRM_TRIGGER_MODIFY)) {
                    $state.go("layout.triggerMasterEdit", {
                        id: id
                    });
                }
            }

            /**
             * Delete Country Master.
             * @param {int} id - The id of a Country.
             */
            $scope.deleteTrigger = function(id) {
                if ($rootScope.roleAccess(roleConstant.MASTER_CRM_TRIGGER_DELETE)) {
                    var newScope = $scope.$new();
                    newScope.errorMessage = $rootScope.nls["ERR06604"];
                    ngDialog.openConfirm({
                        template: '<p>{{errorMessage}}</p>' +
                            '<div class="ngdialog-footer">' +
                            '<button type="button" class="btn btn-default btn-property cancel-btn" ng-click="closeThisDialog(0)">Cancel' +
                            '<button type="button" class="btn btn-primary btn-property accent-btn" ng-click="confirm(1)">Yes' +
                            '</button></div>',
                        plain: true,
                        scope: newScope,
                        closeByDocument: false,
                        className: 'ngdialog-theme-default'
                    }).then(function(value) {
                        triggerMasterFactory.delete.query({
                            id: id
                        }).$promise.then(function(data) {
                            if (data.responseCode == 'ERR0') {
                                $scope.triggerMasteView = false;
                                Notification.success($rootScope.nls["ERR402"]);
                                $scope.deskTopView = true;
                                $scope.searchInit();
                            }
                        }, function(error) {
                            $log.debug("Error ", error);
                        });
                    }, function(value) {
                        $log.debug("delete Cancelled");
                    });
                }
            }

            /**
             * Create New Trigger Master.
             */
            $scope.create = function() {
                var validationResponse = triggerMasterValidationService.validate($scope.triggerMaster, 0);
                if (validationResponse.error == true) {
                    $log.debug("Validation Response -- ", validationResponse);
                    $log.debug('Validation Failed to create Trigger..')
                    $scope.validationErrorAndFocus(validationResponse, validationResponse.errElement);
                } else {
                    triggerMasterFactory.create.query($scope.triggerMaster).$promise.then(function(data) {
                        if (data.responseCode == 'ERR0') {
                            $log.debug("Trigger Saved Successfully Response :: ", data);
                            Notification.success($rootScope.nls["ERR400"]);
                            $state.go('layout.triggerMaster');
                        } else {
                            $log.debug("Create Trigger Failed :" + data.responseDescription)
                        }

                    }, function(error) {
                        $log.debug("Create Trigger Failed : " + error)
                    });
                }
            };


            /**
             * Common Validation Focus Functionality - starts here" 
             */
            $scope.validationErrorAndFocus = function(valResponse, elemId) {
                $scope.errorMap = new Map();
                if (valResponse.error == true) {
                    $scope.errorMap.put(valResponse.errElement, valResponse.errMessage);
                }
                $scope.navigateToNextField(elemId);
            }

            //Update Existing Country Master
            $scope.update = function() {
                var validationResponse = triggerMasterValidationService.validate($scope.triggerMaster, 0);
                if (validationResponse.error == true) {
                    $log.debug("Validation Response -- ", validationResponse);
                    $log.debug('Validation Failed while Updating the Trigger..')
                    $scope.validationErrorAndFocus(validationResponse, validationResponse.errElement);
                } else {
                    triggerMasterFactory.update.query($scope.triggerMaster).$promise.then(function(data) {
                        if (data.responseCode == 'ERR0') {
                            $log.debug("Trigger Updated Successfully Response :: ", data);
                            Notification.success($rootScope.nls["ERR401"]);
                            $state.go('layout.triggerMaster');
                        } else {
                            $log.debug("Updating Trigger Failed :" + data.responseDescription)
                        }
                    }, function(error) {
                        $log.debug("Updating Trigger Failed : " + error)
                    });
                }
            };


            /**
             * Recent History - starts here
             */
            $scope.isReloaded = localStorage.isTriggerMasterReloaded;
            $scope.$on('addTriggerMasterEvent', function(events, args) {
                $rootScope.unfinishedData = $scope.triggerMaster;
                $rootScope.category = "Trigger";
                $rootScope.unfinishedFormTitle = "Trigger (New)";
                if ($scope.triggerMaster != undefined && $scope.triggerMaster != null && $scope.triggerMaster.triggerName != undefined && $scope.triggerMaster.triggerName != null) {
                    $rootScope.subTitle = $scope.triggerMaster.triggerName;
                } else {
                    $rootScope.subTitle = "Unknown Trigger "
                }
            })

            $scope.$on('editTriggerMasterEvent', function(events, args) {
                $rootScope.unfinishedData = $scope.triggerMaster;
                $rootScope.category = "Trigger";
                $rootScope.unfinishedFormTitle = "Trigger Edit # " + $scope.triggerMaster.triggerCode;
            })

            $scope.$on('addTriggerMasterEventReload', function(e, confirmation) {
                confirmation.message = "";
                localStorage.reloadFormData = JSON.stringify($scope.triggerMaster);
                localStorage.isTriggerMasterReloaded = "YES";
                e.preventDefault();
            })

            $scope.$on('editTriggerMasterEventReload', function(e, confirmation) {
                confirmation.message = "";
                localStorage.reloadFormData = JSON.stringify($scope.triggerMaster);
                localStorage.isTriggerMasterReloaded = "YES";
                e.preventDefault();

            })



            /**
             * State Identification Handling Here.
             * @param {String} $stateParams.action - VIEW, ADD, EDIT, SEARCH.
             */
            //
            switch ($stateParams.action) {
                case "ADD":
                    if ($stateParams.fromHistory === 'Yes') {
                        if ($scope.isReloaded == "YES") {
                            $scope.isReloaded = "NO";
                            localStorage.isTriggerMasterReloaded = "NO";
                            $scope.triggerMaster = JSON.parse(localStorage.reloadFormData);
                        } else {
                            $scope.triggerMaster = $rootScope.selectedUnfilledFormData;
                        }
                    } else {
                        if ($scope.isReloaded == "YES") {
                            $scope.isReloaded = "NO";
                            localStorage.isTriggerMasterReloaded = "NO";
                            $scope.triggerMaster = JSON.parse(localStorage.reloadFormData);
                        } else {
                            $log.debug("Add NotHistory NotReload...")
                        }
                    }
                    $rootScope.breadcrumbArr = triggerMasterDataService.getAddBreadCrumb();
                    break;
                case "EDIT":
                    if ($scope.fromHistory === 'Yes') {
                        if ($scope.isReloaded == "YES") {
                            $scope.isReloaded = "NO";
                            localStorage.isTriggerMasterReloaded = "NO";
                            $scope.triggerMaster = JSON.parse(localStorage.reloadFormData);
                        } else {
                            $scope.triggerMaster = $rootScope.selectedUnfilledFormData;
                        }
                    } else {
                        if ($scope.isReloaded == "YES") {
                            $scope.isReloaded = "NO";
                            localStorage.isTriggerMasterReloaded = "NO";
                            $scope.triggerMaster = JSON.parse(localStorage.reloadFormData);
                        } else {
                            $scope.view($stateParams.id, false);
                        }
                    }
                    $rootScope.breadcrumbArr = triggerMasterDataService.getEditBreadCrumb();
                    break;
                default:
                    $rootScope.breadcrumbArr = triggerMasterDataService.getListBreadCrumb();
                    $scope.searchInit();
            }

        }
    ]);
})();