/**
 * 
 */

app.service('bankMasterValidationService', ['$rootScope','ValidateUtil', 'CommonValidationService',
                                               'appConstant', function($rootScope, ValidateUtil, CommonValidationService, appConstant) {	
	
this.validate = function(masterObject, code) {	
	//Validate Bank Name
	if(code == 0 || code == 1) {
		if (masterObject.bankName == null ||masterObject.bankName == undefined  || masterObject.bankName == "") {
			return this.validationResponse(true, "bankName", $rootScope.nls["ERR06251"], masterObject);
		}else{
			if(!CommonValidationService.checkRegExp(appConstant.Reg_Exp_Master_Bank_Name,masterObject.bankName)){
  				return this.validationResponse(true, "bankName", $rootScope.nls["ERR06254"], masterObject);
		   }
		}
	}
	
	//Validate Bank Code
	if(code == 0 || code == 2) {
		if (masterObject.bankCode == null ||masterObject.bankCode == undefined  || masterObject.bankCode == "") {
			return this.validationResponse(true, "bankCode", $rootScope.nls["ERR06250"], masterObject);
		}
		else{
			if(!CommonValidationService.checkRegExp(appConstant.Reg_Exp_Master_Bank_Code,masterObject.bankCode)){
  				return this.validationResponse(true, "bankCode", $rootScope.nls["ERR06253"], masterObject);
		   }
		}
	}
	
	//Validate Smart Business Bank Code
	if(code == 0 || code == 3) {
		if (masterObject.smartBusinessBankCode != null && masterObject.smartBusinessBankCode != undefined  &&  masterObject.smartBusinessBankCode != "") 
		{
			if(!CommonValidationService.checkRegExp(appConstant.Reg_Exp_Master_Bank_Smart_Bank_Code,masterObject.smartBusinessBankCode)){
  				return this.validationResponse(true, "smartbankCode", $rootScope.nls["ERR06259"], masterObject);
		   }
		}
	}
	return this.validationSuccesResponse(masterObject);
}


   this.validationSuccesResponse = function(masterObject) {
	    return {error : false, obj : masterObject};
	}
	
	this.validationResponse = function(err, elem, message, masterObject) {
		return {error : err, errElement : elem, errMessage : message, obj : masterObject};
	}
	
}]);

