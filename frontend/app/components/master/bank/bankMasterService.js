/**
 * @Author-K.Sathish Kumar
 * service which handles all the operations related with bank master
 */

app.service('bankMasterService',function($rootScope){
	
	this.getHeadArray = function() {
        return [{

    		"name":"#",
    		"width":"col-xs-0half",
    		"model":"no",
    		"search":false
    		},
    		{
    			"name":"Name",
    			"width":"col-md-2 col-xs-2 col-sm-2",
    			"model":"bankName",
    			"search":true,
    			"wrap_cell":true,
    			"type":"text",
    			"sort":true,
    			"key":"searchBankName"

    		},
    		{
    			"name":"Code",
    			"width":"col-md-2 col-xs-1 col-sm-2",
    			"model":"bankCode",
    			"search":true,
    			"type":"text",
    			"sort":true,
    			"key":"searchBankCode"
    		},
    		{
    			"name":"Smart Business bank code",
    			"width":"col-md-2 col-xs-2 col-sm-2",
    			"model":"smartBusinessBankCode",
    			"search":true,
    			"type":"text",
    			"sort":true,
    			"key":"searchSmartBusinessBankCode"
    		},
    		{
    			"name":"Status",
    			"width":"col-md-1 col-xs-2 col-sm-1",
    			"model":"status",
    			"search":true,
    			"type":"drop",
    			"data":$rootScope.enum['LovStatus'],
    			"key":"searchStatus",
    			"sort":true
    		}
        ];
    };
});



app.service('bankService', function() {
	var oldObject=null;
	    function set(data) {
	    	oldObject=JSON.stringify(data);
		}
	    function get() {
	    	oldObect=JSON.parse(oldObject);
	    	return oldObect
	    }
	    return {
			set : set,
			get:get
		}
});


