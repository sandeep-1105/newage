/**
 * bankMasterFactory-used to connect with server
 */

app.factory('bankMasterFactory', function($resource) {
	return {
		findById : $resource('/api/v1/bankmaster/get/id/:id', {}, {
			 query : {method : 'GET', params : {id : ''}, isArray : false}
		}),
		create : $resource('/api/v1/bankmaster/create', {}, {
			query : {method : 'POST'}
		}),
		update : $resource('/api/v1/bankmaster/update', {}, {
			query : {method : 'POST'}
		}),
		delete : $resource('/api/v1/bankmaster/delete/:id', {}, {
			query : {method : 'DELETE', params : {id : ''}}
		}),
		search : $resource('/api/v1/bankmaster/get/search', {}, {
			query : {method : 'POST'}
		})
	};
})
