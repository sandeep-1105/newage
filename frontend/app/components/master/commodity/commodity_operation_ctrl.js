app.controller('CommodityOperationCtrl',['$rootScope', '$state', '$scope', '$http', '$location',
    'CommodityAdd', 'CommodityView', 'CommodityEdit', 'ngProgressFactory', 'ngDialog',
    '$stateParams', 'appConstant', 'CommonValidationService', 
	function($rootScope, $state, $scope, $http, $location,
    CommodityAdd, CommodityView, CommodityEdit, ngProgressFactory, ngDialog,
    $stateParams, appConstant, CommonValidationService) {


    $scope.contained_progressbar = ngProgressFactory.createInstance();
    $scope.contained_progressbar.setParent(document.getElementById('commodity-panel'));
    $scope.contained_progressbar.setAbsolute();

    $scope.init = function() {

        if ($scope.commodityMaster == undefined || $scope.commodityMaster == null) {
            $scope.commodityMaster = {}
            $scope.commodityMaster.status = 'Active';
        }

        $scope.setOldDataVal();
    };

    $scope.setOldDataVal = function() {
        $scope.oldData = JSON.stringify($scope.commodityMaster);
        console.log("CommodityMaster Data Loaded");
    }

    $scope.cancel = function() {

        if ($scope.oldData != JSON.stringify($scope.commodityMaster)) {
            var newScope = $scope.$new();
            newScope.errorMessage = $rootScope.nls["ERR200"];
            ngDialog.openConfirm({
                    template: '<p>{{errorMessage}}</p>' +
                        '<div class="ngdialog-footer">' +
                        ' <button type="button" class="btn btn-default btn-property cancel-btn" ng-click="confirm(2)">No</button>' +
                        '<button type="button" class="btn btn-default btn-property accent-btn " ng-click="confirm(1)">Yes</button>' +
                        '<button type="button" class="btn btn-default btn-property pull-left grey-cancel-btn" ng-click="confirm(3)">Cancel</button>' +
                        '</div>',
                    plain: true,
                    scope: newScope,
                    className: 'ngdialog-theme-default'
                })
                .then(
                    function(value) {
                        if (value == 1 &&
                            $scope.commodityMaster.id == null) {
                            $scope.save();
                        } else if (value == 1 &&
                            $scope.commodityMaster.id != null) {
                            $scope.update();
                        } else if (value == 2) {
                            var params = {}
                            params.submitAction = 'Cancelled';
                            $state.go("layout.commodity", params);
                        } else {
                            console.log("cancelled");
                        }

                    });

        } else {
            var params = {}
            params.submitAction = 'Cancelled';
            $state.go("layout.commodity", params);
        }
    }



    $scope.update = function() {
        console.log("Update Method is called.");
        if ($scope.validateCommodityMaster(0)) {
            $scope.contained_progressbar.start();
            CommodityEdit.update($scope.commodityMaster).$promise.then(
                function(data) {
                    if (data.responseCode == 'ERR0') {
                        console.log("Commodity updated Successfully")
                        var params = {}
                        $rootScope.successDesc = $rootScope.nls["ERR401"];
                        params.submitAction = 'Saved';
                        $state.go("layout.commodity", params);

                    } else {
                        console.log("Commodity updated Failed ", data.responseDescription)
                    }
                    $scope.contained_progressbar.complete();
                    angular.element(".panel-body").animate({
                        scrollTop: 0
                    }, "slow");
                },
                function(error) {
                    console.log("Commodity updated Failed : ", error)
                });
        } else {
            console.log("Invalid Form Submission.");
        }

    }
    $scope.save = function() {
        console.log("Save Method is called.");
        if ($scope.validateCommodityMaster(0)) {
            $scope.contained_progressbar.start();
            CommodityAdd.save($scope.commodityMaster).$promise.then(function(data) {
                if (data.responseCode == 'ERR0') {
                    var params = {}
                    params.submitAction = 'Saved';
                    $rootScope.successDesc = $rootScope.nls["ERR400"];
                    $state.go("layout.commodity", params);
                } else {
                    console.log("Commodity added Failed " + data.responseDescription)
                }
                $scope.contained_progressbar.complete();
                angular.element(".panel-body").animate({
                    scrollTop: 0
                }, "slow");
            }, function(error) {
                console.log("Commodity added Failed : ", error)
            });
        } else {
            console.log("Invalid Form Submission.");
        }

    }
    $scope.validateCommodityMaster = function(validateCode) {

        console.log("Validate Called");
        $scope.errorMap = new Map();

        if (validateCode == 0 || validateCode == 1) {
            if ($scope.commodityMaster.hsName == undefined ||
                $scope.commodityMaster.hsName == null ||
                $scope.commodityMaster.hsName == "") {
                $scope.errorMap.put("hsName", $rootScope.nls["ERR2407"]);
                return false;
            } else {
                if (!CommonValidationService.checkRegExp(appConstant.Reg_Exp_Master_HS_NAME, $scope.commodityMaster.hsName)) {
                    $scope.errorMap.put("hsName", $rootScope.nls["ERR2404"]);
                    return false;
                }
            }
        }

        if (validateCode == 0 || validateCode == 2) {
            if ($scope.commodityMaster.hsCode == undefined ||
                $scope.commodityMaster.hsCode == null ||
                $scope.commodityMaster.hsCode == "") {
                $scope.errorMap.put("hsCode", $rootScope.nls["ERR2406"]);
                return false;
            } else {
                if (!CommonValidationService.checkRegExp(appConstant.Reg_Exp_Master_HS_CODE, $scope.commodityMaster.hsCode)) {
                    $scope.errorMap.put("hsCode", $rootScope.nls["ERR2403"]);
                    return false;
                }
            }
        }

        return true;

    }

    $scope.setStatus = function(data) {
        if (data == 'Active') {
            return 'activetype';
        } else if (data == 'Block') {
            return 'blockedtype';
        } else if (data == 'Hide') {
            return 'hiddentype';
        }
    }


    // History Start

    $scope.getCommodityById = function(commodityId) {
        console.log("getCommodityById ", commodityId);

        CommodityView.get({
            id: commodityId
        }, function(data) {
            if (data.responseCode == "ERR0") {
                console.log("Successful while getting commodity.", data)
                $scope.commodityMaster = data.responseObject;
                $scope.setOldDataVal();
            }
        }, function(error) {
            console.log("Error while getting commodity.", error)
        });

    }


    //On leave the Unfilled commodity Form Broadcast then event will happen in StateChangeListener(BroadCaster)-app_state_route.js

    $scope.$on('addCommodityEvent', function(events, args) {

        $rootScope.unfinishedData = $scope.commodityMaster;
        $rootScope.category = "Commodity Master";
        $rootScope.unfinishedFormTitle = "Commodity (New)";
        if ($scope.commodityMaster != undefined &&
            $scope.commodityMaster != null &&
            $scope.commodityMaster.hsName != undefined &&
            $scope.commodityMaster.hsName != null &&
            $scope.commodityMaster.hsName != "") {
            $rootScope.subTitle = $scope.commodityMaster.hsName;
        } else {
            $rootScope.subTitle = "Unknown Commodity"
        }
    })

    $scope.$on('editCommodityEvent', function(events, args) {
        $rootScope.unfinishedData = $scope.commodityMaster;
        $rootScope.category = "Commodity Master";
        $rootScope.unfinishedFormTitle = "Commodity Edit (" + $scope.commodityMaster.hsCode + ")";
        if ($scope.commodityMaster != undefined &&
            $scope.commodityMaster != null &&
            $scope.commodityMaster.hsName != undefined &&
            $scope.commodityMaster.hsName != null &&
            $scope.commodityMaster.hsName != "") {
            $rootScope.subTitle = $scope.commodityMaster.hsName;
        } else {
            $rootScope.subTitle = "Unknown Commodity"
        }
    })


    $scope.$on('addCommodityEventReload', function(e, confirmation) {
        confirmation.message = "";
        localStorage.reloadFormData = JSON.stringify($scope.commodityMaster);
        localStorage.isCommodityReloaded = "YES";
        e.preventDefault();
    });


    $scope.$on('editCommodityEventReload', function(e, confirmation) {
        confirmation.message = "";
        localStorage.reloadFormData = JSON.stringify($scope.commodityMaster);
        localStorage.isCommodityReloaded = "YES";
        e.preventDefault();
    });


    $scope.isReloaded = localStorage.isCommodityReloaded;

    if ($stateParams.action == "ADD") {
        if ($stateParams.fromHistory === 'Yes') {
            if ($scope.isReloaded == "YES") {
                console.log("Add History Reload...")
                $scope.isReloaded = "NO";
                localStorage.isCommodityReloaded = "NO";
                $scope.commodityMaster = JSON.parse(localStorage.reloadFormData);
                $scope.init();
            } else {
                console.log("Add History NotReload...")
                $scope.commodityMaster = $rootScope.selectedUnfilledFormData;
                $scope.init();
            }
            $scope.oldData = "";
        } else {
            if ($scope.isReloaded == "YES") {
                $scope.isReloaded = "NO";
                localStorage.isCommodityReloaded = "NO";
                $scope.commodityMaster = JSON.parse(localStorage.reloadFormData);
                console.log("Add NotHistory Reload...");
                $scope.init();
                $scope.oldData = "";
            } else {
                console.log("Add NotHistory NotReload...")
                $scope.init(0);
            }
        }
        $rootScope.breadcrumbArr = [{
                label: "Master",
                state: "layout.commodity"
            },
            {
                label: "Commodity",
                state: "layout.commodity"
            },
            {
                label: "Add Commodity",
                state: null
            }
        ];

    } else {
        if ($stateParams.fromHistory === 'Yes') {
            if ($scope.isReloaded == "YES") {
                $scope.isReloaded = "NO";
                localStorage.isCommodityReloaded = "NO";
                $scope.commodityMaster = JSON
                    .parse(localStorage.reloadFormData);
                $scope.init();
            } else {
                $scope.commodityMaster = $rootScope.selectedUnfilledFormData;
                $scope.init();
            }
            $scope.oldData = "";
        } else {
            if ($scope.isReloaded == "YES") {
                $scope.isReloaded = "NO";
                localStorage.isCommodityReloaded = "NO";
                $scope.commodityMaster = JSON.parse(localStorage.reloadFormData);
                $scope.init();
                $scope.oldData = "";
            } else {
                $scope.getCommodityById($stateParams.commodityId);
                $scope.init();
            }
        }
        $rootScope.breadcrumbArr = [{
                label: "Master",
                state: "layout.commodity"
            },
            {
                label: "Commodity",
                state: "layout.commodity"
            },
            {
                label: "Edit Commodity",
                state: null
            }
        ];
    }


    // History End




}]);