(function() {

    app.factory("CommoditySearch",['$resource', function($resource) {
        return $resource("/api/v1/commoditymaster/get/search", {}, {
            query: {
                method: 'POST'
            }
        });
    }]);

    app.factory("CommoditySearchKeyword",['$resource', function($resource) {
        return $resource("/api/v1/commoditymaster/get/search/keyword", {}, {
            fetch: {
                method: 'POST'
            }
        });
    }]);

    app.factory("CommodityAdd",['$resource', function($resource) {
        return $resource("/api/v1/commoditymaster/create", {}, {
            save: {
                method: 'POST'
            }
        });
    }]);

    app.factory("CommodityEdit",['$resource', function($resource) {
        return $resource("/api/v1/commoditymaster/update", {}, {
            update: {
                method: 'POST'
            }
        });
    }]);

    app.factory("CommodityList",['$resource', function($resource) {
        return $resource("/api/v1/commoditymaster/get/search/keyword", {}, {
            fetch: {
                method: 'POST'
            }
        });
    }]);


    app.factory("CommodityView",['$resource', function($resource) {
        return $resource("/api/v1/commoditymaster/get/id/:id", {}, {
            get: {
                method: 'GET',
                params: {
                    id: ''
                },
                isArray: false
            }
        });
    }]);

    app.factory("CommodityRemove",['$resource', function($resource) {
        return $resource("/api/v1/commoditymaster/delete/:id", {}, {
            remove: {
                method: 'DELETE',
                params: {
                    id: ''
                },
                isArray: false
            }
        });
    }]);



    /* Commodity Service */


    app.service("CommodityService",['$rootScope', 'CommodityList', function($rootScope, CommodityList) {




        this.getCommodityList = function(keyword) {
            searchDto = {};
            searchDto.keyword = keyword == null ? "" : keyword;
            searchDto.selectedPageNumber = 0;
            searchDto.recordPerPage = $rootScope.totalRecordFetchPerRequest;

            return CommodityList.fetch(searchDto).$promise.then(
                function(data) {
                    if (data.responseCode == "ERR0") {
                        var commodityList = data.responseObject.searchResult;
                        console.log("List of commodity fetched ", commodityList);
                        return commodityList;
                    }
                },
                function(errResponse) {
                    console.error('Error while fetching commodity List ', errResponse);
                }
            );
        }



    }]);




})();