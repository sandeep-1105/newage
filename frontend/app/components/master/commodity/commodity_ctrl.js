app.controller('CommodityCtrl',['$rootScope', '$state', '$scope', '$window', 'ngTableParams', 'CommoditySearch', 
    	'CommodityView', 'ngDialog', 'CommodityRemove', '$stateParams', 'roleConstant',
    function($rootScope, $state, $scope, $window, ngTableParams, CommoditySearch, 
    	CommodityView, ngDialog, CommodityRemove, $stateParams, roleConstant) {

        console.log("Commodity controller loaded..");
        /************************************************************
         * ux - by Muthu
         * reason - ipad compatibility fix
         *
         * *********************************************************/
        $scope.deskTopView = true;

        /********************* ux fix ends here *****************************/


        $scope.init = function() {
            console.log("Commodity Init method called.");
            $scope.searchDto = {};
            $scope.search();
        };

        $scope.commodityHeadArr = [{
                "name": "#",
                "width": "col-xs-0half",
                "model": "no",
                "search": false,
            },
            {
                "name": "Name",
                "width": "col-xs-3",
                "model": "hsName",
                "search": true,
                "wrap_cell": true,
                "type": "text",
                "sort": true,
                "key": "searchCommodityName"

            },
            {
                "name": "Code",
                "width": "col-xs-2",
                "model": "hsCode",
                "search": true,
                "type": "text",
                "sort": true,
                "key": "searchCommodityCode"
            },
            {
                "name": "Status",
                "width": "col-xs-2",
                "model": "status",
                "search": true,
                "sort": true,
                "type": "drop",
                "data": $rootScope.enum['LovStatus'],
                "key": "searchStatus"
            }
        ];

        $scope.sortSelection = {
            sortKey: "hsName",
            sortOrder: "asc"
        }

        $scope.commodityMaster = {};
        $scope.searchDto = {};
        $scope.limitArr = [10, 15, 20];
        $scope.page = 0;
        $scope.limit = 10;


        $scope.add = function() {
            if ($rootScope.roleAccess(roleConstant.MASTER_GENERAL_COMMODITY_CREATE)) {
                $state.go("layout.addCommodity");
            }
        };
        $scope.limitChange = function(item) {
            $scope.page = 0;
            $scope.limit = item;
            $scope.search();
        }
        $scope.rowSelect = function(data) {
            if ($rootScope.roleAccess(roleConstant.MASTER_GENERAL_COMMODITY_VIEW)) {
                //$scope.serviceMaster.id=1;
                $scope.commodityMaster = data;
                $scope.showHistory = false;
                /************************************************************
                 * ux - by Muthu
                 * reason - ipad compatibility fix
                 *
                 * *********************************************************/

                var windowInner = $window.innerWidth;
                if (windowInner <= 1199) {
                    $scope.deskTopView = false;

                } else {
                    $scope.deskTopView = true;
                }
                angular.element($window).bind('resize', function() {
                    if ($window.innerWidth >= 1200) {
                        $scope.deskTopView = true;
                    } else if ($window.innerWidth <= 1199) {
                        if ($scope.commodityMaster.id != null) {
                            $scope.deskTopView = false;
                        } else {
                            $scope.deskTopView = true;
                        }
                    }

                    console.log("window resizing..." + $window.innerWidth);
                    $scope.$digest();
                });

                /********************* ux fix ends here *****************************/
            }

        }

        $scope.changeSearch = function(param) {
            for (var attrname in param) {
                $scope.searchDto[attrname] = param[attrname];
            }
            $scope.page = 0;
            $scope.cancel();
            $scope.search();
            console.log("change search", $scope.searchDto);
        }
        $scope.sortChange = function(param) {
            $scope.searchDto.orderByType = param.sortOrder.toUpperCase();
            $scope.searchDto.sortByColumn = param.sortKey;
            $scope.search();
        }
        $scope.changepage = function(param) {
            $scope.page = param.page;
            $scope.limit = param.size;
            $scope.search();
        }


        $scope.search = function() {
            console.log("Commodity Search loaded..");
            $scope.commodityMaster = {};
            $scope.searchDto.selectedPageNumber = $scope.page;
            $scope.searchDto.recordPerPage = $scope.limit;
            $scope.commodityArr = [];
            CommoditySearch.query($scope.searchDto).$promise.then(function(
                data, status) {
                $scope.totalRecord = data.responseObject.totalRecord;
                console.log($scope.totalRecord);
                var tempArr = [];
                var resultArr = [];

                tempArr = data.responseObject.searchResult;

                console.log("tempArr", tempArr);
                var tempObj = {};
                angular.forEach(tempArr, function(item, index) {
                    tempObj = item;
                    tempObj.no = (index + 1) + ($scope.page * $scope.limit);
                    resultArr.push(tempObj);
                    tempObj = {};
                });
                $scope.commodityArr = resultArr;

            });

        }

        $scope.serialNo = function(index) {
            index = ($scope.searchDto.selectedPageNumber * $scope.searchDto.recordPerPage) +
                (index + 1);
            return index;
        }

        $scope.cancel = function() {
            $scope.commodityMaster = {};
            $scope.showHistory = false;
            /************************************************************
             * ux - by Muthu
             * reason - ipad compatibility fix
             *
             * *********************************************************/
            $scope.deskTopView = true;

            /********************* ux fix ends here *****************************/

        };

        $scope.edit = function() {
            if ($rootScope.roleAccess(roleConstant.MASTER_GENERAL_COMMODITY_MODIFY)) {
                console.log("Edit commodity Called");
                $state.go("layout.editCommodity", {
                    commodityId: $scope.commodityMaster.id
                });
            }
        };

        $scope.deleteCommodity = function() {
            if ($rootScope.roleAccess(roleConstant.MASTER_GENERAL_COMMODITY_DELETE)) {
                var newScope = $scope.$new();
                newScope.errorMessage = $rootScope.nls["ERR235"];
                ngDialog.openConfirm({
                    template: '<p>{{errorMessage}}</p>' +
                        '<div class="ngdialog-footer">' +
                        '<button type="button" class="btn btn-default btn-property cancel-btn" ng-click="closeThisDialog(0)">No' +
                        '<button type="button" class="btn btn-primary btn-property accent-btn" ng-click="confirm(1)">Yes' +
                        '</button>' +
                        '</div>',
                    plain: true,
                    scope: newScope,
                    className: 'ngdialog-theme-default'
                }).then(
                    function(value) {
                        CommodityRemove
                            .remove({
                                    id: $scope.commodityMaster.id
                                },
                                function(data) {
                                    if (data.responseCode == 'ERR0') {
                                        console.log("Commodity deleted Successfully")
                                        $scope.init();
                                        $scope.deskTopView = true;
                                    } else {
                                        console.log("Commodity deleted Failed " + data.responseDescription)
                                    }
                                },
                                function(error) {
                                    console.log("Commodity deleted Failed : " + error)
                                });
                    },
                    function(value) {
                        console.log("deleted cancelled");
                    });
            }
        }



        // Routing start

        switch ($stateParams.action) {
            case "SEARCH":
                $scope.init();
                $rootScope.breadcrumbArr = [{
                        label: "Master",
                        state: "layout.commodity"
                    },
                    {
                        label: "Commodity",
                        state: "layout.commodity"
                    }
                ];
                break;
            case "VIEW":
                break;
        }
        // Routing end




    }]);