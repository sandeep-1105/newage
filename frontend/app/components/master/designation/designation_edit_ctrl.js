app.controller('DesignationMasterEditCtrl', function($rootScope, $scope, $state, $stateParams, 
						ngProgressFactory, ngDialog, appConstant,CommonValidationService, Notification, DesignationFactory) {
				    
					$scope.contained_progressbar = ngProgressFactory.createInstance();
					$scope.contained_progressbar.setParent(document.getElementById('group-panel'));
					$scope.contained_progressbar.setAbsolute();
					
					$scope.init = function() {

						console.log("Init is called...............");
						
						if($scope.designationMaster == undefined) {
							$scope.designationMaster =  {};
							$scope.designationMaster.status = 'Active';
						}
						
						
						// Setting it to compare while cancelling
						$scope.oldData = JSON.stringify($scope.designationMaster);
					}
					
		                
					$scope.cancel = function() {

						if ($scope.oldData !== JSON.stringify($scope.designationMaster)) {

							var newScope = $scope.$new();
							newScope.errorMessage = $rootScope.nls["ERR200"];
							ngDialog.openConfirm({
								template: '<p>{{errorMessage}}</p>' +
								'<div class="ngdialog-footer">' +
									' <button type="button" class="btn btn-default btn-property cancel-btn" ng-click="confirm(2)">No</button>' +
									'<button type="button" class="btn btn-default btn-property accent-btn " ng-click="confirm(1)">Yes</button>' +
									'<button type="button" class="btn btn-default btn-property pull-left grey-cancel-btn" ng-click="confirm(3)">Cancel</button>' +
								'</div>',
								plain: true,
								scope: newScope,
								className: 'ngdialog-theme-default'
							}).then(function(value) {

								if (value == 1) {
									$scope.saveOrUpdate();
								} else {
									
									var params = {};
									params.submitAction = 'Cancelled';
									$state.go("layout.designation",params);
								}
							});
						} else {
							var params = {};
							params.submitAction = 'Cancelled';
							$state.go("layout.designation",params);
						}
					}

					
					$scope.saveOrUpdate = function() {
					
						console.log("SaveOrUpdate is called...");
						
						if($scope.validateDesignationMaster(0)) {
							$scope.contained_progressbar.start();
							
							if($scope.designationMaster.id == null) {
								var successMessage = $rootScope.nls["ERR400"];	
							} else {
								var successMessage = $rootScope.nls["ERR401"];	
							}
							
							DesignationFactory.save.query($scope.designationMaster).$promise.then(function(data) {
								
								if (data.responseCode == "ERR0") {
									Notification.success(successMessage);
									$state.go("layout.designation", {submitAction : 'Saved'});
								} else {
									console.log("Designation updated Failed ", data.responseDescription)
								}
								$scope.contained_progressbar.complete();
								angular.element(".panel-body").animate({ scrollTop : 0 }, "slow");
							},	function(error) {
								console.log("Designation updated Failed : ", error);
							});
						} else {
							console.log("Form Invalid");
						}
						

					}



					$scope.validateDesignationMaster = function(validateCode) {
						
						$scope.errorMap = new Map();
						

						if (validateCode == 0 || validateCode == 1) {
							
							if ($scope.designationMaster.designationName == undefined || $scope.designationMaster.designationName == null || $scope.designationMaster.designationName == "") {
								$scope.errorMap.put("designationName", $rootScope.nls["ERR2434"]);
								return false;
							} 
						}
							
						// Designation code is auto generated.so that validation is not required- refer bug id- NES-1632
						/*if (validateCode == 0 || validateCode == 2) {
							if ($scope.designationMaster.designationCode == undefined || $scope.designationMaster.designationCode == null || $scope.designationMaster.designationCode == "") {
								$scope.errorMap.put("designationCode", $rootScope.nls["ERR2432"]);
								return false;
							} 
						}*/

						return true;

					}

					$scope.setStatus = function(data) {
						if (data == 'Active') {
							return 'activetype';
						} else if (data == 'Block') {
							return 'blockedtype';
						} else if (data == 'Hide') {
							return 'hiddentype';
						}
					}





					
					
					
					
					

					$scope.getDesignationById = function(designationId) {

						console.log("getDesignationById ", designationId);
						
						DesignationFactory.findById.query({ id : designationId }, function(data) {
				   			if (data.responseCode =="ERR0") {
				   				console.log("Successful while getting Designation Master.", data.responseObject);
				   				$scope.designationMaster = data.responseObject;
				   				console.log("Designation Master ", $scope.designationMaster);
				   				$scope.init();
				   			}
				   		}, function(error) {
				   			console.log("Error while getting designation Master.", error)
				   		});
					
					}
					
					

				$scope.$on('addDesignationEvent', function(events, args){
					$rootScope.unfinishedData = $scope.designationMaster;
					$rootScope.unfinishedFormTitle = "Designation Master (New)";
					$rootScope.category = "Designation Master";
					if($scope.designationMaster != undefined && $scope.designationMaster != null && $scope.designationMaster.id != undefined 
							&& $scope.designationMaster.designationName != null && $scope.designationMaster.designationName != "") {
						$rootScope.subTitle = $scope.designationMaster.designationName;
					} else {
						$rootScope.subTitle = "Unknown Designation Master"
					}
				});
			  
			
				$scope.$on('editDesignationEvent', function(events, args) {
				 	$rootScope.unfinishedData = $scope.designationMaster;
					$rootScope.unfinishedFormTitle = "Designation Master Edit (" + $scope.designationMaster.designationCode + ")";
					$rootScope.category = "Designation Master";
					if($scope.designationMaster != undefined && $scope.designationMaster != null && $scope.designationMaster.id != undefined 
							&& $scope.designationMaster.designationName != null && $scope.designationMaster.designationName != "") {
						$rootScope.subTitle = $scope.designationMaster.designationName;
					} else {
						$rootScope.subTitle = "Unknown Designation Master"
					}
				});
			

				$scope.$on('editDesignationEventReload', function (e, confirmation) {
					confirmation.message = "";
					localStorage.reloadFormData = JSON.stringify($scope.designationMaster);
					localStorage.isDesignationReloaded = "YES";
					e.preventDefault();
				});


				$scope.$on('editDesignationEventReload', function (e, confirmation) {
					confirmation.message = "";
					localStorage.reloadFormData = JSON.stringify($scope.designationMaster);
					localStorage.isDesignationReloaded = "YES";
					e.preventDefault();
				});
				
			
			
			$scope.isReloaded = localStorage.isDesignationReloaded;
			if ($stateParams.action == "ADD") {
				if($stateParams.fromHistory === 'Yes') {
					if($scope.isReloaded == "YES") {
						console.log("Add History Reload...")
						$scope.isReloaded = "NO";
						localStorage.isDesignationReloaded = "NO";
						$scope.designationMaster  = JSON.parse(localStorage.reloadFormData);
					} else {
						console.log("Add History Not Reload...")
						$scope.designationMaster = $rootScope.selectedUnfilledFormData;
					}
				} else {
					if($scope.isReloaded == "YES") {
						console.log("Add Not History Reload....");
						$scope.isReloaded = "NO";
						localStorage.isDesignationReloaded = "NO";
						$scope.designationMaster  = JSON.parse(localStorage.reloadFormData);
					} else {
						console.log("Add Not History Not Reload...");
						$scope.init();
					}	
				}
				
				$rootScope.breadcrumbArr = [ { label:"Master",  state:"layout.designation"}, 
				                             { label:"Designation",  state:"layout.designation"}, 
				                             { label:"Add Designation", state:null } ];
				
				$rootScope.navigateToNextField("designationName");
						
			} else {
						
				if($stateParams.fromHistory === 'Yes') {
					if($scope.isReloaded == "YES") {
						$scope.isReloaded = "NO";
						localStorage.isDesignationReloaded = "NO";
						$scope.designationMaster  = JSON.parse(localStorage.reloadFormData);
					} else {
						$scope.designationMaster = $rootScope.selectedUnfilledFormData;
					}
				} else {
					if($scope.isReloaded == "YES") {
						$scope.isReloaded = "NO";
						localStorage.isDesignationReloaded = "NO";
						$scope.designationMaster  = JSON.parse(localStorage.reloadFormData);
					} else {
						$scope.getDesignationById($stateParams.designationId);
					}
					
				}
						
				$rootScope.breadcrumbArr = [ { label:"Master", state:"layout.designation"}, 
				                             { label:"Designation",  state:"layout.designation"}, 
				                             { label:"Edit Designation", state:null } 
				                           ];
				
				$rootScope.navigateToNextField("designationName");
			}
						

});
