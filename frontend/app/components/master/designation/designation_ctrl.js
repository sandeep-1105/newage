app.controller('DesignationMasterCtrl', ['$rootScope', '$scope', '$window', '$state', '$stateParams', 'ngDialog','DesignationFactory', 'roleConstant','Notification',
     function($rootScope, $scope, $window, $state, $stateParams, ngDialog, DesignationFactory, roleConstant,Notification) {

	/************************************************************
	 * ux - by Muthu
	 * reason - ipad compatibility fix
	 *
	 * *********************************************************/
	$scope.deskTopView = true;

	/********************* ux fix ends here *****************************/
	
	$scope.limitArr = [10,15,20];
	$scope.page = 0;
	$scope.limit = 10;
	
	$scope.designationHeadArr=[ 
	                {
						"name" : "#",
						"width" : "col-xs-0half",
						"model" : "no",
						"search" : false,
					}, {
						"name" : "Name",
						"width" : "col-xs-3",
						"model" : "designationName",
						"search" : true,
						"wrap_cell" : true,
						"type" : "text",
						"sort" : true,
						"key" : "searchDesignationName"

					}, {
						"name" : "Code",
						"width" : "col-xs-1half",
						"model" : "designationCode",
						"search" : true,
						"type" : "text",
						"sort" : true,
						"key" : "searchDesignationCode"
					}, {
						"name" : "Manager",
						"width" : "col-xs-2",
						"model" : "manager",
						"search" : true,
						"type" : "drop",
						"sort" : true,
						"data" : $rootScope.enum['YesNo'],
						"key" : "searchManager"
					}, {
						"name" : "Status",
						"width" : "col-xs-2",
						"model" : "status",
						"search" : true,
						"type" : "drop",
						"sort" : true,
						"data" : $rootScope.enum['LovStatus'],
						"key" : "searchStatus"
					}];
	
	

	
	$scope.sortSelection = {
		sortKey:"designationName",
        sortOrder:"asc"
	}
	
	$scope.init = function () {
		$scope.searchDto = {};
		$scope.detailView = false;
		$scope.search();
	};

	$scope.cancel = function () {
		$scope.designationMaster = {};
		$scope.showHistory = false;
		$scope.detailView = false;
		/************************************************************
		 * ux - by Muthu
		 * reason - ipad compatibility fix
		 *
		 * *********************************************************/
		$scope.deskTopView = true;

		/********************* ux fix ends here *****************************/
	}
	

	$scope.addDesignationMaster = function() { 
		if($rootScope.roleAccess(roleConstant.MASTER_HRMS_DESIGNATION_CREATE)){
			console.log("Add Designation Button Pressed...");
			$state.go("layout.addDesignation");
		}
	}
	
	
	$scope.editDesignationMaster = function () {
		if($rootScope.roleAccess(roleConstant.MASTER_HRMS_DESIGNATION_MODIFY)){
			console.log("Edit Designation Button Pressed...");
			$state.go("layout.editDesignation", {designationId : $scope.designationMaster.id});
		}
	};
		

	$scope.deleteDesignationMaster = function() {
		if($rootScope.roleAccess(roleConstant.MASTER_HRMS_DESIGNATION_DELETE)){
			var newScope = $scope.$new();
			newScope.errorMessage = $rootScope.nls["ERR253"];
			ngDialog.openConfirm( 
			{ template : 	'<p>{{errorMessage}}</p>' +
							'<div class="ngdialog-footer">' +
							'<button type="button" class="btn btn-default btn-property cancel-btn" ng-click="closeThisDialog(0)">No' +
							'<button type="button" class="btn btn-primary btn-property accent-btn" ng-click="confirm(1)">Yes' +
							'</button>' +
							'</div>',
							plain: true,
							scope: newScope,
							className: 'ngdialog-theme-default'
			}).then(function (value) {

				DesignationFactory.remove.query({ id : $scope.designationMaster.id }, function(data) {
					if (data.responseCode =="ERR0"){
						Notification.success($rootScope.nls["ERR402"]);
						console.log("Designation deleted Successfully")
						$scope.cancel();
						$scope.init();
					} else {
						console.log("Designation deletion Failed ", data.responseDescription)
					}
				}, function(error) {
					console.log("Designation deleted Failed : ", error)
				});
				
			}, function (value) {
				console.log("Designation deleted cancelled");
			});
		}
	}
    
	
	$scope.rowSelect = function(obj){
		if($rootScope.roleAccess(roleConstant.MASTER_HRMS_DESIGNATION_VIEW)){
			console.log("rowSelect is called.");
			$scope.designationMaster = obj;
			$scope.detailView = true;
			/************************************************************
			 * ux - by Muthu
			 * reason - ipad compatibility fix
			 *
			 * *********************************************************/

			var windowInner=$window.innerWidth;
			if(windowInner<=1199){
				$scope.deskTopView = false;
			}
			else{
				$scope.deskTopView = true;
			}
			
			angular.element($window).bind('resize', function(){
				if($window.innerWidth>=1200){
					$scope.deskTopView = true;
				}
				else if($window.innerWidth<=1199){
					if($scope.designationMaster.id!=null){
						$scope.deskTopView = false;
					}
					else {
						$scope.deskTopView = true;
					}
				}
				console.log("window resizing..." + $window.innerWidth);
				$scope.$digest();
			});

			/********************* ux fix ends here *****************************/
		}
	}
	
	$scope.changepage = function(param) {
		console.log("ChangePage..." , param.page);
		$scope.page = param.page;
		$scope.limit = param.size;
		$scope.search();
	}
	
	$scope.limitChange = function(item){
		console.log("LimitChange...", item);
		$scope.page = 0;
		$scope.limit = item;
		$scope.search();
	}

	$scope.sortChange = function(param){
		console.log("SortChange..." , param);
		$scope.searchDto.orderByType = param.sortOrder.toUpperCase();
		$scope.searchDto.sortByColumn = param.sortKey;
		$scope.search();
	}
	
	$scope.changeSearch = function(param){
		console.log("Change Search ", param);
		for (var attrname in param) {
			$scope.searchDto[attrname] = param[attrname];
		}
		$scope.page = 0;
		$scope.cancel();
		$scope.search();
	}
	
	
	$scope.search=  function(){
		$scope.searchDto.selectedPageNumber = $scope.page;
		$scope.searchDto.recordPerPage = $scope.limit;

		DesignationFactory.search.fetch($scope.searchDto).$promise.then(function(data, status) {
			$scope.totalRecord = data.responseObject.totalRecord;
			$scope.designationArr = data.responseObject.searchResult;
			console.log($scope.designationArr);
		});
	}
	
	
	if($stateParams.action == "SEARCH") {
			console.log("designation Master Search Page....");
			$scope.init();

			$rootScope.breadcrumbArr = [ {label:"Master", state:"layout.designation"}, 
			                              {label:"Designation",  state:"layout.designation"}];
	}	
}]);

