app.factory('DesignationFactory',['$resource', function($resource) {
	return {
		findById : $resource('/api/v1/designationmaster/get/id/:id', {}, {
			 query : {method : 'GET', params : {id : ''}, isArray : false}
		}),
		save : $resource('/api/v1/designationmaster/create', {}, {
			query : {method : 'POST'}
		}),
		remove : $resource('/api/v1/designationmaster/delete/:id', {}, {
			query : {method : 'DELETE', params : {id : ''}}
		}),
		getAll : $resource('/api/v1/designationmaster/get/search/keyword', {}, {
			query : {method : 'POST'}
		}),
		search : $resource('/api/v1/designationmaster/get/search', {}, {
			fetch : {method : 'POST'}
		})
	};
}])