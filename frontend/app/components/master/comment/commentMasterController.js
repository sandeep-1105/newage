(function(){
app.controller("CommentMasterCtrl", ['$scope', '$rootScope', '$window', '$modal', '$state', '$stateParams', '$log', 'ngDialog', 'commentMasterDataService', 'commentMasterFactory', 'RecentHistorySaveService', 'commentMasterValidationService', 'Notification','roleConstant',
    function($scope, $rootScope, $window, $modal, $state, $stateParams, $log, ngDialog, commentMasterDataService, commentMasterFactory, RecentHistorySaveService, commentMasterValidationService, Notification, roleConstant ) {
	
	
	  var vm = this;
      vm.tableHeadArr = commentMasterDataService.getHeadArray();
      vm.dataArr = [];
      vm.searchData = {};
      vm.commentMaster = {};
      vm.limitArr = [10, 15, 20];
      vm.page = 0;
      vm.limit = 10;
      vm.totalRecord = 100;
      vm.showHistory = false;
      vm.errorMap = new Map();
      vm.deskTopView = true;

      vm.sortSelection = {
          sortKey: "commentName",
          sortOrder: "asc"
      }
      
      vm.changeSearch = function(param) {
          $log.debug("Change Search is called." + param);
          for (var attrname in param) {
              vm.searchData[attrname] = param[attrname];
          }
          vm.page = 0;
          vm.search();
      }

      vm.sortChange = function(param) {
          $log.debug("sortChange is called. " + param);
          vm.searchData.orderByType = param.sortOrder.toUpperCase();
          vm.searchData.sortByColumn = param.sortKey;
          vm.search();
      }
      
      
      /**
       * comment Master List Page Search Data  initializing and calling search Mtd.
       */
      vm.searchInit = function() {
          vm.searchData = {};
          vm.searchData.orderByType = vm.sortSelection.sortOrder.toUpperCase();
          vm.searchData.sortByColumn = vm.sortSelection.sortKey;
          vm.search();
      };
      

      /**
       * Get comment Master's Based on Search Data.
       */
      vm.search = function() {
      	vm.commentMasteView=false;
          vm.searchData.selectedPageNumber = vm.page;
          vm.searchData.recordPerPage = vm.limit;
          vm.dataArr = [];
          commentMasterFactory.search.fetch(vm.searchData).$promise.then(
              function(data) {
                  if (data.responseCode == "ERR0") {
                      vm.totalRecord = data.responseObject.totalRecord;
                      vm.dataArr = data.responseObject.searchResult;
                  }
              },
              function(errResponse) {
              	 $log.error('Error while fetching commentMaster');
              });
         }
      
      
      /**
       * comment Master List Page Search Data  initializing and calling search Mtd.
       * @param {Object} data - The Selected comment Master from List page.
       * @param {string} data.id - The id of the Selected comment Master.
       * @param {int} index - The Selected row index from List page.
       */
      vm.rowSelect = function(data) {
    	  if($rootScope.roleAccess(roleConstant.MASTER_GENERAL_COMMENT_VIEW)){
    		  vm.commentMasteView=true;
  			vm.commentMaster = data;
  			var windowInner=$window.innerWidth;
  			if(windowInner<=1199){
  				vm.deskTopView = false;
  			}
  			else{
  				vm.deskTopView = true;
  			}
  			angular.element($window).bind('resize', function(){
  				if($window.innerWidth>=1200){
  					vm.deskTopView = true;
  				}
  				else if($window.innerWidth<=1199){
  					if(vm.commentMaster.id!=null){
  						vm.deskTopView = false;
  					}
  					else {
  						vm.deskTopView = true;
  					}
  				}
  				$scope.$digest();
  			});  
    	  }
		}
      
      
      /**
       * Modify Existing comment Master.
       * @param {int} id - The id of a Country.
       */
      vm.edit = function(id) {
    	  if($rootScope.roleAccess(roleConstant.MASTER_GENERAL_COMMENT_MODIFY)){
    		  $state.go("layout.commentMasterEdit", {
                  id: id
              }); 
    	  }
      }
      
      // set status of comment master
      
      vm.setStatus = function(data){
  		if(data=='Active'){
  			return 'activetype';
  		} else if(data=='Block'){
  			return 'blockedtype';
  		}else if(data=='Hide'){
  			return 'hiddentype';
  		}
  	}
      
      
      /**
       * Go To Add comment Master.
       */
      vm.addNew = function() {
    	  if($rootScope.roleAccess(roleConstant.MASTER_GENERAL_COMMENT_CREATE)){
    		  $state.go("layout.commentMasterAdd");
    	  }
      }
      
      
      /**
       * Create New comment Master.
       */
      vm.create = function() {
      	var validationResponse = commentMasterValidationService.validate(vm.commentMaster,0);
          if(validationResponse.error == true) {
          	$log.debug("Validation Response -- ", validationResponse);
          	$log.debug('Validation Failed to create commentMaster..')
				$scope.validationErrorAndFocus(validationResponse, validationResponse.errElement);
			}
          else {
          	commentMasterFactory.create.query(vm.commentMaster).$promise.then(function(data) {
		            if (data.responseCode == 'ERR0') {
		            	$log.debug("comment Saved Successfully Response :: ", data);
		            	Notification.success($rootScope.nls["ERR400"]);
		            	$state.go('layout.commentMaster');
		            } else {
		                $log.debug("Create comment Failed :" + data.responseDescription)
		            }

		        }, function(error) {
		        	 $log.debug("Create comment Failed : " + error)
		        });
          } 
      };
      
      
      //Update Existing comment Master
      vm.update = function() {
      	 var validationResponse = commentMasterValidationService.validate(vm.commentMaster,0);
           if(validationResponse.error == true) {
           	$log.debug("Validation Response -- ", validationResponse);
           	$log.debug('Validation Failed while Updating the comment..')
				$scope.validationErrorAndFocus(validationResponse, validationResponse.errElement);
			}
           else {
           	commentMasterFactory.update.query(vm.commentMaster).$promise.then(function(data) {
		            if (data.responseCode == 'ERR0') {
		            	$log.debug("comment Updated Successfully Response :: ", data);
		            	Notification.success($rootScope.nls["ERR401"]);
		            	$state.go('layout.commentMaster');
		            } else {
		                $log.debug("Updating comment Failed :" + data.responseDescription)
		            }
		        }, function(error) {
		        	 $log.debug("Updating comment Failed : " + error)
		        });
           } 
      };
      
      
      /**
       * Go Back to comment Master List Page.
       */
      vm.backToList = function() {
      	vm.commentMasteView = false;
      	vm.commentMaster = {};
      	vm.deskTopView = true;
      }
      
      
      vm.changepage = function(param) {
          $log.debug("change Page..." + param.page);
          vm.page = param.page;
          vm.limit = param.size;
          vm.search();
      }

      /**
       * Country Master List Page Limit Data change.
       * @param {int} nLimit - The Selected limit option from Limit array.
       */
      vm.limitChange = function(nLimit) {
          $log.debug("Limit Change..." + nLimit);
          vm.page = 0;
          vm.limit = nLimit;
          vm.search();
      }
      
      
      /**
       * Cancel Add or Edit comment Master.
       * @param {int} id - The id of a comment
       */
     
      vm.cancel = function(objId) {
          if(vm.commentMasterForm!=undefined){
		    	vm.goTocancel(vm.commentMasterForm.$dirty,objId);
          }else{
          	vm.goTocancel(false,objId);
          }
      }
     
     vm.goTocancel = function(isDirty,objId){
   		 if (isDirty) {
   		      var newScope = $scope.$new();
   		      newScope.errorMessage = $rootScope.nls["ERR200"];
   		      ngDialog.openConfirm({
   		        template: '<p>{{errorMessage}}</p>' +
   		          '<div class="ngdialog-footer">' +
   		          ' <button type="button" class="btn btn-default btn-property cancel-btn" ng-click="confirm(2)">No</button>' +
   		          '<button type="button" class="btn btn-default btn-property accent-btn " ng-click="confirm(1)">Yes</button>' +
   		          '<button type="button" class="btn btn-default btn-property pull-left grey-cancel-btn" ng-click="confirm(3)">Cancel</button>' +
   		          '</div>',
   		        plain: true,
   		        closeByDocument: false,
   		        scope: newScope,
   		        className: 'ngdialog-theme-default'
   		      }).then(
   		        function(value) {
   		          if (value == 1) {
   		            if(objId!=null && objId != undefined){
   		            	vm.update();
   		            }else{
   		            	vm.create();
   		            }
   		          } else if (value == 2) {
   		        	 $state.go("layout.commentMaster", {
   		                submitAction: "Cancelled"
   		            });
   		          } else {
   		            $log.debug("cancelled");
   		          }
   		        });
   		    } else {
   		     $state.go("layout.commentMaster", {
                 submitAction: "Cancelled"
             });
   		    }
   	    }

      
      
      /**
       * Get comment Master By id.
       * @param {int} id - The id of a comment.
       * @param {boolean} isView - TO identify purpose of calling is for View or Edit a comment.
       */
      vm.view = function(id, isView) {
      	commentMasterFactory.findById.query({
              id: id
          }).$promise.then(function(data) {
              if (data.responseCode == 'ERR0') {
                  vm.commentMaster = data.responseObject;
                  var rHistoryObj = {}

                  if (vm.commentMaster != undefined && vm.commentMaster != null && vm.commentMaster.id != undefined && vm.commentMaster.id != null) {
                      $rootScope.subTitle = vm.commentMaster.commentName;
                  } else {
                      $rootScope.subTitle = "Unknown comment"
                  }
                  $rootScope.unfinishedFormTitle = "comment  # " + vm.commentMaster.commentCode;
                  if (isView) {
                      rHistoryObj = {
                          'title': "comment #" + vm.commentMaster.commentCode,
                          'subTitle': $rootScope.subTitle,
                          'stateName': $state.current.name,
                          'stateParam': JSON.stringify($stateParams),
                          'stateCategory': 'comment Master',
                          'serviceType': undefined,
                          'showService': false
                      }

                  } else {

                      rHistoryObj = {
                          'title': 'Edit comment # ' + $stateParams.serviceId,
                          'subTitle': $rootScope.subTitle,
                          'stateName': $state.current.name,
                          'stateParam': JSON.stringify($stateParams),
                          'stateCategory': 'comment Master',
                          'serviceType': 'MASTER',
                          'serviceCode': ' ',
                          'orginAndDestination': ' '
                      }
                  }
                  RecentHistorySaveService.form(rHistoryObj);
              } else {
                  $log.debug("Exception while getting comment by Id ", data.responseDescription);
              }
          }, function(error) {
              $log.debug("Error ", error);
          });
      };
      
      /**
       * Delete comment Master.
       * @param {int} id - The id of a comment.
       */
      vm.delete = function(id) {
    	  if($rootScope.roleAccess(roleConstant.MASTER_GENERAL_COMMENT_DELETE)){
    			var newScope = $scope.$new();
    			newScope.errorMessage = $rootScope.nls["ERR1041"];
    	    	ngDialog.openConfirm( {
    	                    template : '<p>{{errorMessage}}</p>'
    	                    + '<div class="ngdialog-footer">'
    	                    + '<button type="button" class="btn btn-default btn-property cancel-btn" ng-click="closeThisDialog(0)">Cancel'
    	                    + '<button type="button" class="btn btn-primary btn-property accent-btn" ng-click="confirm(1)">Yes'
    	                    + '</button></div>',
    	                    plain : true,
    	                    scope : newScope,
    	                    closeByDocument: false,
    	                    className : 'ngdialog-theme-default'
    	                }).then( function(value) {
    	                	commentMasterFactory.delete.query({
    	                        id: id
    	                    }).$promise.then(function(data) {
    	                        if (data.responseCode == 'ERR0') {
    	                        	vm.commentMasteView=false;
    	                        	Notification.success($rootScope.nls["ERR402"]);
    	                        	vm.deskTopView = true;
    	                            vm.search();
    	                        }
    	                    }, function(error) {
    	                        $log.debug("Error ", error);
    	                    });
    	                }, function(value) {
    	                    $log.debug("delete Cancelled");
    	            });  
    	  }
      }
      

      
    /**
     * Common Validation Focus Functionality - starts here" 
     */
		$scope.validationErrorAndFocus = function(valResponse, elemId){
			$scope.errorMap = new Map();
			if(valResponse.error == true){
				$scope.errorMap.put(valResponse.errElement, valResponse.errMessage);
			}
				$scope.navigateToNextField(elemId);
		}
      
      
      /**
       * State Identification Handling Here.
       * @param {String} $stateParams.action - VIEW, ADD, EDIT, SEARCH.
       */
      //
      switch ($stateParams.action) {
          case "ADD":
          	 if ($stateParams.fromHistory === 'Yes') {
                   if (vm.isReloaded == "YES") {
                    vm.isReloaded = "NO";
                    localStorage.isCommentMasterReloaded = "NO";
                     vm.commentMaster = JSON.parse(localStorage.reloadFormData);
                   } else {
                     vm.commentMaster = $rootScope.selectedUnfilledFormData;
                   }
                 } else {
                   if (vm.isReloaded == "YES") {
                     vm.isReloaded = "NO";
                     localStorage.isCommentMasterReloaded = "NO";
                     vm.commentMaster = JSON.parse(localStorage.reloadFormData);
                   } else {
                       $log.debug("Add NotHistory NotReload...")
                   }
                 }
              $rootScope.breadcrumbArr = commentMasterDataService.getAddBreadCrumb();
              break;
          case "EDIT":
          	 if (vm.fromHistory === 'Yes') {
                   if (vm.isReloaded == "YES") {
                     vm.isReloaded = "NO";
                     localStorage.isCommentMasterReloaded = "NO";
                     vm.commentMaster = JSON.parse(localStorage.reloadFormData);
                   } else {
                     vm.commentMaster = $rootScope.selectedUnfilledFormData;
                   }
                 } else {
                   if (vm.isReloaded == "YES") {
                     vm.isReloaded = "NO";
                     localStorage.isCommentMasterReloaded = "NO";
                     vm.commentMaster = JSON.parse(localStorage.reloadFormData);
                   } else {
                  	 vm.view($stateParams.id, false);
                   }
                 }
              $rootScope.breadcrumbArr = commentMasterDataService.getEditBreadCrumb();
              break;
          default:
              $rootScope.breadcrumbArr = commentMasterDataService.getListBreadCrumb();
              vm.searchInit();
      }
      
}
]);
}) ();