
app.factory('CommentFactory',['$resource', function($resource) {
	return {
		search : $resource('/api/v1/commentmaster/get/search/keyword', {}, {
			query : {method : 'POST'}
		})
	};
}])
