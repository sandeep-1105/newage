app.factory('commentMasterFactory',['$resource', function($resource) {
	return {
		findById : $resource('/api/v1/commentmaster/get/id/:id', {}, {
			 query : {method : 'GET', params : {id : ''}, isArray : false}
		})
		,
		search : $resource('/api/v1/commentmaster/get/search', {}, {
			fetch : {method : 'POST'}
		})
		,
		keyword : $resource('/api/v1/commentmaster/get/search/keyword', {}, {
			fetch : {method : 'POST'}
		})
		,
		update : $resource('/api/v1/commentmaster/update', {}, {
			query : {method : 'POST'}
		})
		,
		delete : $resource('/api/v1/commentmaster/delete/:id', {}, {
			query : {method : 'DELETE', params : {id : ''}}
		})
		,
		create : $resource('/api/v1/commentmaster/create', {}, {
			query : {method : 'POST'}
		})
	};
}])