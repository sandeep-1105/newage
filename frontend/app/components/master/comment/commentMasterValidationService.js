app.service('commentMasterValidationService', ['$rootScope','ValidateUtil', 'CommonValidationService', 'appConstant', function($rootScope, ValidateUtil, CommonValidationService, appConstant) {
	this.validate = function(comment, code) {
		//Validate comment name
		if(code == 0 || code == 1) {
			if(comment.commentName == undefined || comment.commentName == null || comment.commentName =="" ){
				return this.validationResponse(true, "commentName", $rootScope.nls["ERR1031"], comment);
			}else if(!CommonValidationService.checkRegExp(appConstant.Reg_Exp_Master_Comment_Name,comment.commentName)){
				return this.validationResponse(true, "commentName", $rootScope.nls["ERR1032"], comment);
			}
		}
		//Validate comment code
		if(code == 0 || code == 2) {
			if(comment.commentCode == undefined || comment.commentCode == null || comment.commentCode =="" ){
				return this.validationResponse(true, "commentCode", $rootScope.nls["ERR1029"], comment);
			}else if(!CommonValidationService.checkRegExp(appConstant.Reg_Exp_Master_Comment_Code,comment.commentCode)){
				return this.validationResponse(true, "commentCode", $rootScope.nls["ERR1030"], comment);
			}
		}
		//Validate description
		if(code == 0 || code == 3) {
			if(comment.description == undefined || comment.description == null || comment.description =="" ){
				return this.validationResponse(true, "description", $rootScope.nls["ERR1042"], comment);
			}
		}
		return this.validationSuccesResponse(comment);
	}
	this.validationSuccesResponse = function(comment) {
	    return {error : false, obj : comment};
	}
	this.validationResponse = function(err, elem, message, comment) {
		return {error : err, errElement : elem, errMessage : message, obj : comment};
	}
	
}]);