app.service('commentMasterDataService', function($rootScope) {
    this.getHeadArray = function() {
	   return [
			  {
					"name":"#",
					"width":"col-xs-0half",
					"model":"no",
					"search":false
			  },{
					"name":"Name",
					"width":"col-xs-3",
					"model":"commentName",
					"search":true,
					"wrap_cell":true,
					"type":"text",
					"sort":true,
					"key":"searchCommentName"
				},{
					"name":"Code",
					"width":"col-xs-2",
					"model":"commentCode",
					"search":true,
					"type":"text",
					"sort":true,
					"key":"searchCommentCode"
				},{
					"name":"Status",
					"width":"col-xs-2",
					"model":"status",
					"search":true,
					"type":"drop",
					"data":$rootScope.enum['LovStatus'],
					"key":"searchStatus",
					"sort":true
				}];
          };

    
    this.getListBreadCrumb = function() {
        return [{
                label: "Master",
                state: "layout.commentMaster"
            },
            {
                label: "Comment",
                state: "layout.commentMaster"
            }
        ];
    }

    this.getAddBreadCrumb = function() {
        return [{
                label: "Master",
                state: "layout.commentMaster"
            },
            {
                label: "Comment",
                state: "layout.commentMaster"
            },
            {
                label: "Add Comment",
                state: null
            }
        ];
    };
    this.getEditBreadCrumb = function() {
        return [{
                label: "Master",
                state: "layout.commentMaster"
            },
            {
                label: "Comment",
                state: "layout.commentMaster"
            },
            {
                label: "Edit Comment",
                state: null
            }
        ];
    };
   

});