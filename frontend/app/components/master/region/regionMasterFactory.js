/**
 * regionMasterFactory-used to connect with server
 */

app.factory('regionMasterFactory', function($resource) {
	return {
		findById : $resource('/api/v1/regionmaster/get/id/:id', {}, {
			 query : {method : 'GET', params : {id : ''}, isArray : false}
		}),
		create : $resource('/api/v1/regionmaster/create', {}, {
			query : {method : 'POST'}
		}),
		update : $resource('/api/v1/regionmaster/update', {}, {
			query : {method : 'POST'}
		}),
		delete : $resource('/api/v1/regionmaster/delete/:id', {}, {
			query : {method : 'DELETE', params : {id : ''}}
		}),
		search : $resource('/api/v1/regionmaster/get/search', {}, {
			query : {method : 'POST'}
		})
	};
})
