(function() {
	app.factory("RegionList",['$resource', function($resource) {
		return $resource("/api/v1/regionmaster/get/search/keyword", {}, {
			fetch : {
				method : 'POST'
			}
		});
	}]);

})();
