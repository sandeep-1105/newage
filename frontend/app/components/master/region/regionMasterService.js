/**
 * @Author-K.Sathish Kumar
 * service which handles all the operations related with region master
 */

app.service('regionMasterService',function($rootScope){
	
	this.getHeadArray = function() {
        return [{

    		"name":"#",
    		"width":"col-xs-0half",
    		"model":"no",
    		"search":false
    		},
    		{
    			"name":"Name",
    			"width":"col-xs-4half",
    			"model":"regionName",
    			"search":true,
    			"wrap_cell":true,
    			"type":"text",
    			"sort":true,
    			"key":"searchRegionName"

    		},
    		{
    			"name":"Code",
    			"width":"col-xs-2",
    			"model":"regionCode",
    			"search":true,
    			"type":"text",
    			"sort":true,
    			"key":"searchRegionCode"
    		},
    		{
    			"name":"Status",
    			"width":"col-xs-3",
    			"model":"status",
    			"search":true,
    			"type":"drop",
    			"data":$rootScope.enum['LovStatus'],
    			"key":"searchStatus",
    			"sort":true
    		}
        ];
    };
});
