/**
 * 
 */

app.service('regionMasterValidationService', ['$rootScope','ValidateUtil', 'CommonValidationService',
                                               'appConstant', function($rootScope, ValidateUtil, CommonValidationService, appConstant) {	
	
this.validate = function(masterObject, code) {	
	//Validate Region Name
	if(code == 0 || code == 1) {
		if (masterObject.regionName == null ||masterObject.regionName == undefined  || masterObject.regionName == "") {
			return this.validationResponse(true, "regionName", $rootScope.nls["ERR06261"], masterObject);
		}else{
			if(!CommonValidationService.checkRegExp(appConstant.Reg_Exp_Master_Region_Name,masterObject.regionName)){
  				return this.validationResponse(true, "regionName", $rootScope.nls["ERR06264"], masterObject);
		   }
		}
	}
	
	//Validate Region Code
	if(code == 0 || code == 2) {
		if (masterObject.regionCode == null ||masterObject.regionCode == undefined  || masterObject.regionCode == "") {
			return this.validationResponse(true, "regionCode", $rootScope.nls["ERR06260"], masterObject);
		}
		else{
			if(!CommonValidationService.checkRegExp(appConstant.Reg_Exp_Master_Region_Code,masterObject.regionCode)){
  				return this.validationResponse(true, "regionCode", $rootScope.nls["ERR06263"], masterObject);
		   }
		}
	}
	return this.validationSuccesResponse(masterObject);
}


   this.validationSuccesResponse = function(masterObject) {
	    return {error : false, obj : masterObject};
	}
	
	this.validationResponse = function(err, elem, message, masterObject) {
		return {error : err, errElement : elem, errMessage : message, obj : masterObject};
	}
	
}]);

