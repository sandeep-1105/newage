(function(){ 
app.controller("regionMasterController",['$scope','$rootScope','$window', '$state','$stateParams','$modal','ngDialog','$log','regionMasterService','regionMasterValidationService','Notification','regionMasterFactory', "roleConstant",
                                       function($scope,$rootScope, $window, $state,$stateParams,$modal,ngDialog,$log,regionMasterService,regionMasterValidationService,Notification,regionMasterFactory,roleConstant){


	 //var $scope= this;
	 $scope.regionHeadArr = regionMasterService.getHeadArray();
	 $scope.regionMaster={};
     $scope.showDetail=false;
     $scope.searchDto={};
     $scope.limitArr = [10,15,20];
     $scope.page = 0;
     $scope.limit =10;
     $scope.deskTopView = true;
     
	//initialzation the values
	$scope.init=function(){
		//for- if Add/else edit
		if($scope.regionMaster==undefined || $scope.regionMaster==null || $scope.regionMaster.id==undefined || $scope.regionMaster.id==null){
			$rootScope.breadcrumbArr = [
	                                    { label:"Master", 
	                                      state:"layout.regionMaster"}, 
	                                    { label:"Region", 
	                                      state:"layout.regionMaster"}, 
	                                    { label:"Add Region", state:null}];	
		}else{
			$rootScope.breadcrumbArr = [
	                                    { label:"Master", 
	                                      state:"layout.regionMaster"}, 
	                                    { label:"Region", 
	                                      state:"layout.regionMaster"}, 
	                                    { label:"Edit Region", state:null}];	
		}
	}//init ended
	
	
$scope.setBreadCumList=function(){
		
		$rootScope.breadcrumbArr = [
                                    { label:"Master", 
                                      state:"layout.regionMaster"}, 
                                    { label:"Region", 
                                      state:"layout.regionMaster"}
                                   ];	
		
	}

$scope.sortSelection = {
		sortKey:"regionName",
		sortOrder:"asc"
}



//add operation-
$scope.addNewRegion=function(){
	if($rootScope.roleAccess(roleConstant.MASTER_GENERAL_REGION_SECTOR_CREATE)){
	var params={};
	params.Action='ADD';
	$state.go('layout.regionMasterAdd',params);
	}
}

$scope.setStatus = function(data){
	if(data=='Active'){
		return 'activetype';
	} else if(data=='Block'){
		return 'blockedtype';
	}else if(data=='Hide'){
		return 'hiddentype';
	}
}

//based on param serach
$scope.changeSearch = function(param) {
    $log.debug("Change Search is called." + param);
    for (var attrname in param) {
        $scope.searchDto[attrname] = param[attrname];
    }
    $scope.page = 0;
    $scope.search();
}

//sort 
$scope.sortChange = function(param) {
    $log.debug("sortChange is called. " + param);
    $scope.searchDto.orderByType = param.sortOrder.toUpperCase();
    $scope.searchDto.sortByColumn = param.sortKey;
    $scope.search();
}

/**
 * Region Master List Navigate to selected page.
 */
$scope.changepage = function(param) {
    $log.debug("change Page..." + param.page);
    $scope.page = param.page;
    $scope.limit = param.size;
    $scope.search();
}

/**
 * Region Master List Page Limit Data change.
 */
$scope.limitChange = function(nLimit) {
    $log.debug("Limit Change..." + nLimit);
    $scope.page = 0;
    $scope.limit = nLimit;
    $scope.search();
}

//for view 
$scope.editRegion=function(obj){
	if($rootScope.roleAccess(roleConstant.MASTER_GENERAL_REGION_SECTOR_MODIFY)){
		$state.go('layout.regionMasterEdit', {id : obj.id});	
	}
}

$scope.rowSelect=function(obj){
	if($rootScope.roleAccess(roleConstant.MASTER_GENERAL_REGION_SECTOR_VIEW)){
		$scope.regionMaster=obj;
		$scope.showDetail=true;
	     			var windowInner=$window.innerWidth;
	     			if(windowInner<=1199){
	     				$scope.deskTopView = false;
	     			}
	     			else{
	     				$scope.deskTopView = true;
	     			}
	     			angular.element($window).bind('resize', function(){
	     				if($window.innerWidth>=1200){
	     					$scope.deskTopView = true;
	     				}
	     				else if($window.innerWidth<=1199){
	     					if($scope.regionMaster.id!=null){
	     						$scope.deskTopView = false;
	     					}
	     					else {
	     						$scope.deskTopView = true;
	     					}
	     				}
	     				$scope.$digest();
	     			}); 
	        	 }
			}

$scope.cancelHistory=function(){
	$scope.showDetail=false;
	$scope.regionMaster = {};
	$scope.deskTopView = true;
}

$scope.validateRegionOnChange=function(bm,code){
	 $scope.errorMap=new Map();
 var validationResponse=regionMasterValidationService.validate(bm,code);
    if(validationResponse.error=true){
	  $scope.errorMap.put(validationResponse.errElement, validationResponse.errMessage);
   }
}


$scope.search = function() {
	console.log("Search method is called............................!");
	$scope.showDetail=false;
	$scope.setBreadCumList();
	$scope.searchDto.selectedPageNumber = $scope.page;
	$scope.searchDto.recordPerPage = $scope.limit;
	$scope.regionListArr = [];
	regionMasterFactory.search.query($scope.searchDto).$promise.then(function(data, status) {
		$scope.totalRecord = data.responseObject.totalRecord;
		var tempArr = [];
		var resultArr = [];
		tempArr = data.responseObject.searchResult;

		var tempObj = {};
		angular.forEach(tempArr,function(item,index) {
			tempObj = item;
			tempObj.no = (index+1)+($scope.page*$scope.limit);
			resultArr.push(tempObj);
			tempObj = {};
		});
		
		$scope.regionListArr = resultArr;
	});

}
/**
 * Delete Region Master-calling service.
 * 
 */
$scope.deleteRegion = function(id) {
	if($rootScope.roleAccess(roleConstant.MASTER_GENERAL_REGION_SECTOR_DELETE)){
	var newScope = $scope.$new();
	newScope.errorMessage =$rootScope.nls["ERR06268"];
	ngDialog.openConfirm({
                template : '<p>{{errorMessage}}</p>'
                + '<div class="ngdialog-footer">'
                + '<button type="button" class="btn btn-default btn-property cancel-btn" ng-click="closeThisDialog(0)">Cancel'
                + '<button type="button" class="btn btn-primary btn-property accent-btn" ng-click="confirm(1)">Yes'
                + '</button></div>',
                plain : true,
                scope : newScope,
                closeByDocument: false,
                className : 'ngdialog-theme-default'
            }).then(function(value) {
            	regionMasterFactory.delete.query({
                    id: id
                }).$promise.then(function(data) {
                    if (data.responseCode == 'ERR0') {
                    	Notification.success($rootScope.nls["ERR402"]);
                    	$scope.deskTopView = true;
                    	$scope.search();
                        $state.go("layout.regionMaster", {
                            submitAction: "Cancelled"
                        });
                    }
                }, function(error) {
                    $log.debug("Error ", error);
                });
            }, function(value) {
                $log.debug("delete Cancelled");
        });




}


}


/**
 * Create New region Master.
 */
$scope.create = function() {
	$scope.errorMap=new Map();
    var validationResponse = regionMasterValidationService.validate($scope.regionMaster,0);
    if(validationResponse.error == true) {
    	$log.debug('Validation Failed to create region..')
		$scope.validationErrorAndFocus(validationResponse, validationResponse.errElement);
	}
    else {
    	regionMasterFactory.create.query($scope.regionMaster).$promise.then(function(data) {
            if (data.responseCode == 'ERR0') {
            	$log.debug("region Saved Successfully Response :: ", data);
            	Notification.success($rootScope.nls["ERR400"]);
            	$state.go('layout.regionMaster', {submitAction : 'Cancelled'});
            } else {
                $log.debug("Create region Failed :" + data.responseDescription)
            }

        }, function(error) {
        	 $log.debug("Create region Failed : " + error)
        });
    } 
};



/**
* Common Validation Focus Functionality - starts here" 
*/
$scope.validationErrorAndFocus = function(valResponse, elemId){
	$scope.errorMap = new Map();
	if(valResponse.error == true){
		$scope.errorMap.put(valResponse.errElement, valResponse.errMessage);
	}
		$scope.navigateToNextField(elemId);
}

//Update Existing region Master
$scope.update = function() {
	$scope.errorMap=new Map();
	 var validationResponse = regionMasterValidationService.validate($scope.regionMaster,0);
     if(validationResponse.error == true) {
     	$log.debug("Validation Response -- ", validationResponse);
     	$log.debug('Validation Failed while Updating the region..')
			$scope.validationErrorAndFocus(validationResponse, validationResponse.errElement);
		}
     else {
     	regionMasterFactory.update.query($scope.regionMaster).$promise.then(function(data) {
	            if (data.responseCode == 'ERR0') {
	            	$log.debug("region Updated Successfully Response :: ", data);
	            	Notification.success($rootScope.nls["ERR401"]);
	            	$state.go('layout.regionMaster', {submitAction : 'Cancelled'});
	            } else {
	                $log.debug("Updating Region Failed :" + data.responseDescription)
	            }

	        }, function(error) {
	        	 $log.debug("Updating region Failed : " + error)
	        });
     } 
};


$scope.getRegionMasterById = function(id) {
	regionMasterFactory.findById.query({id : id}).$promise.then(function(data) {
        if (data.responseCode == 'ERR0') {
        	$scope.regionMaster = data.responseObject;
        } else {
        	console.log("Error while getting the Region Master");
        }
    }, function(error) {
    	console.log("Error while getting the Region Master", error);
    });
}


//cancel from add/edit page
$scope.cancelToList=function(){
	if(!$scope.regionform.$dirty){
		var params = {};
		params.submitAction = 'Cancelled';
		$state.go('layout.regionMaster',params);
	}else{
		var newScope = $scope.$new();
		newScope.errorMessage = $rootScope.nls["ERR200"];
		ngDialog.openConfirm({
			template: '<p>{{errorMessage}}</p>' +
			'<div class="ngdialog-footer">' +
				' <button type="button" class="btn btn-default btn-property cancel-btn" ng-click="confirm(2)">No</button>' +
				'<button type="button" class="btn btn-default btn-property accent-btn " ng-click="confirm(1)">Yes</button>' +
				'<button type="button" class="btn btn-default btn-property pull-left grey-cancel-btn" ng-click="confirm(3)">Cancel</button>' +
			'</div>',
			plain: true,
			scope: newScope,
			className: 'ngdialog-theme-default'
		})
				.then(
						function(value) {
							if (value == 1
									&& $scope.regionMaster.id == null) {
								$scope.create();
							} else if (value == 1
									&& $scope.regionMaster.id != null) {
								$scope.update();
							} else if (value == 2) {
								var params = {};
								params.submitAction = 'Cancelled';
								$state.go('layout.regionMaster',params);
							}
							else {
								console.log("cancelled");
							}
						});
	}
}





/**
 * Recent History - starts here
 */
$scope.isReloaded = localStorage.isRegionMasterReloaded;
$scope.$on('regionMasterAddEvent', function(events, args){
  try{
		$rootScope.unfinishedData = $scope.regionMaster;
		$rootScope.category = "Region";
		$rootScope.unfinishedFormTitle = "Region (New)";
		if($scope.regionMaster != undefined && $scope.regionMaster != null && $scope.regionMaster.regionName != undefined && $scope.regionMaster.regionName != null && $scope.regionMaster.regionName != "") {
			$rootScope.subTitle = $scope.regionMaster.regionName;
		} else {
			$rootScope.subTitle = "Unknown Region"
		}
	}catch(e){
		$log.error("session storage is exceed in region  master")
	}
})

$scope.$on('regionMasterEditEvent', function(events, args){
	try{
		
		$rootScope.unfinishedData = $scope.regionMaster;
		$rootScope.category = "Region";
		$rootScope.unfinishedFormTitle = "Region Edit (" + $scope.regionMaster.regionCode +")";
		if($scope.regionMaster != undefined && $scope.regionMaster != null && $scope.regionMaster.regionName != undefined && $scope.regionMaster.regionName != null && $scope.regionMaster.regionName != "") {
			$rootScope.subTitle = $scope.regionMaster.regionName;
		} else {
			$rootScope.subTitle = "Unknown Region"
		}
	}catch(e){
		$log.error("session storage is exceed in region master-edit")
	}
		 
})
  
$scope.$on('regionMasterAddEventReload', function(e, confirmation){
	try{
		confirmation.message = "";
	    localStorage.reloadFormData = JSON.stringify($scope.regionMaster) ;
	    localStorage.isRegionMasterReloaded = "YES";
        e.preventDefault();
	}catch(e){
		$log.error("session storage is exceed in region master-add while reload")
	}
})

$scope.$on('regionMasterEditEventReload', function(e, confirmation){
	try{
		confirmation.message = "";
	    localStorage.reloadFormData = JSON.stringify($scope.regionMaster) ;
	    localStorage.isRegionMasterReloaded = "YES";
        e.preventDefault();
	}catch(e){
		$log.error("session storage is exceed in region master-edit while reload")
	}
})

try{
	if($stateParams.action == "ADD") {
		if($stateParams.fromHistory === 'Yes') {
			if($scope.isReloaded == "YES") {
				console.log("Add History Reload...")
				$scope.isReloaded = "NO";
				localStorage.isRegionMasterReloaded = "NO";
				$scope.regionMaster  = JSON.parse(localStorage.reloadFormData);
				$scope.init();
			} else {
				console.log("Add History NotReload...")
				$scope.regionMaster = $rootScope.selectedUnfilledFormData;
				$scope.init();			
			}
			$scope.oldData = "";
			$rootScope.navigateToNextField("regionName");
		} else {
			if($scope.isReloaded == "YES") {
				$scope.isReloaded = "NO";
				localStorage.isRegionMasterReloaded = "NO";
				$scope.regionMaster  = JSON.parse(localStorage.reloadFormData);
				console.log("Add NotHistory Reload...");
				$scope.init();			
				$scope.oldData = "";
			} else {
				$scope.regionMaster={};
				console.log("Add NotHistory NotReload...")
				$scope.init();
			}	
			$rootScope.navigateToNextField("regionName");
		}
	}else if($stateParams.action == "EDIT"){
		if($stateParams.fromHistory === 'Yes') {
			if($scope.isReloaded == "YES") {
				$scope.isReloaded = "NO";
				localStorage.isRegionMasterReloaded = "NO";
				$scope.regionMaster  = JSON.parse(localStorage.reloadFormData);
				$scope.init();	
				}else {
				$scope.regionMaster = $rootScope.selectedUnfilledFormData;
				$scope.init();			
				}
			$scope.oldData = "";
			$rootScope.navigateToNextField("regionName");
		} else {
			if($scope.isReloaded == "YES") {
				$scope.isReloaded = "NO";
				localStorage.isRegionMasterReloaded = "NO";
				$scope.regionMaster  = JSON.parse(localStorage.reloadFormData);
				$scope.init();
				$scope.oldData = "";
			} else {
				$scope.getRegionMasterById($stateParams.id);
			}	
			$rootScope.navigateToNextField("regionName");
		}
	}else{
		$scope.search();
		$log.info('Search matched');
	}
}catch(e){
	$log.error("Exception in state "+e);
}

   }
]);
}) ();